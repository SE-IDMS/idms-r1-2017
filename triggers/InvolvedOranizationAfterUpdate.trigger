trigger InvolvedOranizationAfterUpdate on InvolvedOrganization__c (After Update)
{
     system.debug('1*******After trigger Before');
     if(Utils_SDF_Methodology.canTrigger('AP_IO_AU'))
       {
        List<InvolvedOrganization__c> iOrgListIOrg=new List<InvolvedOrganization__c>();
        List<InvolvedOrganization__c> iOrgListRep=new List<InvolvedOrganization__c>();
            for(InvolvedOrganization__c iOrg : Trigger.new){
            if(iOrg.Representative__c != trigger.oldMap.get(iOrg.Id).Representative__c){
                iOrgListRep.add(iOrg);
            }
       if(iOrg.Organization__c != trigger.oldMap.get(iOrg.Id).Organization__c){
                  // iOrg.TECH_FromBRE__c=false;
                iOrgListIOrg.add(iOrg);
            }
            }
           if(iOrgListRep.size()>0)
           {system.debug('2**********iOrgListRep'+iOrgListRep);
            AP_InvolvedOrganization.updateIORepresentative(iOrgListRep);  
            
           } 
        if(iOrgListIOrg.size()>0)
           {system.debug('3**********iOrgListIOrg'+iOrgListIOrg);
            AP_InvolvedOrganization.updateInvolvedOrgonBRE(iOrgListIOrg,'Yes');  
            AP_InvolvedOrganization.upDateInvolvedOrgIdsOnBRE(iOrgListIOrg);  
            }
}

}