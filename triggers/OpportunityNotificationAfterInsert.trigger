/**
* @author: Nabil ZEGHACHE
* Date Created (YYYY-MM-DD): 2016-08-25
* Test class: 
* Description: This is the afterInsert trigger of the Opportunity Notification object
* -----------------------------------------------------------------
*                     MODIFICATION HISTORY
* -----------------------------------------------------------------
* Modified By: Authors Name
* Modified Date: Date
* Description: Brief Description of Change + BR/Hotfix
* -----------------------------------------------------------------
*/
trigger OpportunityNotificationAfterInsert on OpportunityNotification__c (after insert) {
    if(Utils_SDF_Methodology.canTrigger('SRV_OppNotif_AI')) {
        
        set<Id> optNotId = New set<Id> ();
        for(OpportunityNotification__c oppNotif :trigger.New) {   
            
            if(oppNotif.Tech_CreatedFromIB__c == true || oppNotif.TechCreatedFromSC__c == true) {
                optNotId.add(oppNotif.Id);
            }            
        }
        
        if(optNotId.size()>0) {
            AP_OpptyCreationAfterInsert.CreatingOpportunity(optNotId);    
        }
    }
}