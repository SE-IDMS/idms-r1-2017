trigger RIAMBeforeInsert on ReturnItemsApprovalMatrix__c(before insert) {
  
  /*********************************************************************************
// Trigger Name     : RPDTBeforeInsert
// Purpose          : Uniqness of the records
// Created by       : SUKUMAR SALLA - Global Delivery Team - IPO
// Date created     : 14-MARCH-2013
// Modified by      :
// Date Modified    :
// Remarks          : MAY 2013 Release
///********************************************************************************/
if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_NewRIAM'))
{      
   AP_RIAM_CreateNewRIAM.createNewRIAM(trigger.new);
    
}

}