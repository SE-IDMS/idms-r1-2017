/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_X3rd_Party_CostTrigger on X3rd_Party_Cost__c
    (before delete, before insert, before update, after delete, after insert, after undelete, after update)
{
	if(Utils_SDF_Methodology.canTrigger('AP_FinancialForceTriggers'))
    dlrs.RollupService.triggerHandler();
}