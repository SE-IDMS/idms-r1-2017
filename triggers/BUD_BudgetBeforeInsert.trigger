/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 03/11/2010
    Description     : Budget Before Insert event trigger.
*/

trigger BUD_BudgetBeforeInsert on Budget__c (before insert) 
{
    System.Debug('****** BUD_BudgetBeforeInsert Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('BudgetTriggerInsert'))
    {
        System.Debug('Value New: ' + Trigger.new);
        System.Debug('Value NewMap: ' + Trigger.newMap);
        BUD_BudgetTriggers.budgetBeforeInsert(Trigger.new);
    }
    System.Debug('****** BUD_BudgetBeforeInsert Trigger End ****'); 
}