trigger SVMXC_BeforeInsertUpdate_Timesheet on SVMXC_Timesheet__c (before delete, before insert, before update) {

    // ensures that the related tech is the owner of the time entry record
    SVMXC_TimesheetBefore.ensureTechIsOwner(trigger.New, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
   
}