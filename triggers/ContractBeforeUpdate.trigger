/********************************************************************************************************************
    Author : Vimal K
    Created Date : 02-May-2016
    Description : For Q2 2016 Release:
                 1. Calculate the OwnerExpirationNotice Date for Email Notification

********************************************************************************************************************/

trigger ContractBeforeUpdate on Contract(before update) {
	if(Utils_SDF_Methodology.canTrigger('AP_ContractTriggerUtils'))
        AP_ContractTriggerUtils.calculateNotificationDates(Trigger.old,Trigger.new, Trigger.oldMap,Trigger.newMap);
}