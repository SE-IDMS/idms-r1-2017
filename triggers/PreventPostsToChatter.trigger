trigger PreventPostsToChatter on FeedItem (before Insert) {
    if(Utils_SDF_Methodology.canTrigger('PreventPostsToChatter'))
    {
        Utils_PreventUsageOfChatter.preventPosts(Trigger.New);
    } 
}