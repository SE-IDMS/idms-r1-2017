/*********************************************************************************
    // Trigger Name     : ServiceContractAfterUpdate
    // Purpose          : ServiceContract After Update event trigger
    // Created by       : Global Delivery Team
    // Date created     : 12th Feb 2013
    // Modified by      : Hari Krishna -->For May - 13 Release
    //Last Modified by  : Deepak Kumar-->For OCT - 13 Release 
    // Date Modified    :07-08-2013
    Modified By-        : Deepak       --> For APR-14-->For asset Categorisation Purpose.
                        : Suhail       --> For June-16-->For PM plan auto population.
    ********************************************************************************/

    trigger ServiceContractAfterUpdate on SVMXC__Service_Contract__c (After update)
    {
        //Trigger bypass
        if(Utils_SDF_Methodology.canTrigger('SVMX16'))
        {
            
            System.Debug('##>>> ServiceContract before update <<<run by ' + UserInfo.getName());
            String ServiceContractRT = System.Label.CLAPR15SRV01;
            String ServiceContractConnectedRT = System.Label.CLAPR15SRV04;
            String ServiceLineRT = System.Label.CLAPR15SRV02;
            String ServiceLineConnectedRT = System.Label.CLAPR15SRV03;
            String PCANC = System.Label.CLAPR15SRV24;
            String VAL = System.Label.CLAPR15SRV29;
            String CAN = System.Label.CLAPR15SRV25;
            String Amendment = System.Label.CLAPR15SRV26;
            String ETL = System.Label.CLAPR15SRV27;
            String Renewal = System.Label.CLAPR15SRV28;
            
            
            list<SVMXC__Service_Contract__c> sclist1 =  New List<SVMXC__Service_Contract__c>();
            list<SVMXC__Service_Contract_Services__c> inslist = new list<SVMXC__Service_Contract_Services__c>();
            list<SVMXC__Service_Contract__share > SCShareLst = new list<SVMXC__Service_Contract__share >(); 
            list<SVMXC__Service_Contract__c> PMPlanList =  New List<SVMXC__Service_Contract__c>();
            set<Id> scid1=New set<Id>();
            
            if(Test.isRunningTest() || Utils_SDF_Methodology.canTrigger('SVMX08'))
            {
                List<SVMXC__Service_Contract__c> scToprocessList = new List<SVMXC__Service_Contract__c>();
                List<SVMXC__Service_Contract__c> scToUpdateList = new List<SVMXC__Service_Contract__c>(); 
                List<SVMXC__Service_Contract__c> newPorcess= new List<SVMXC__Service_Contract__c>();
                list<SVMXC__Service_Contract__c> sclist2 =  New List<SVMXC__Service_Contract__c>();
                list<SVMXC__Service_Contract__c> sclist3 =  New List<SVMXC__Service_Contract__c>();
                list<SVMXC__Service_Contract__c> sclist5 =  New List<SVMXC__Service_Contract__c>();
                List<SVMXC__Service_Contract__c> SPlist = new List<SVMXC__Service_Contract__c>();
                list<SVMXC__Service_Contract__c> scprocesslist = new list<SVMXC__Service_Contract__c>(); 
                list<SVMXC__Service_Contract__c> Canlist= new list<SVMXC__Service_Contract__c>(); 
                list<SVMXC__Service_Contract__c>sllist= new list <SVMXC__Service_Contract__c>();
                list<SVMXC__Service_Contract__c> scOwnerlist= new list <SVMXC__Service_Contract__c>();
                list<SVMXC__Service_Contract__c> renewcontractlist= new list <SVMXC__Service_Contract__c>();
                set<id>slslaset = new set<id>();
                Set<id> accIdSet = new Set<id>();
                Set<id> scIdSet = new Set<id>();
                Set<Id> ServId = new Set<Id>(); 
                Set<Id> SPID=new Set<Id>();
                set<Id> scid = new set<Id>();
                set<Id> scid3 = new set<Id>();
                set<Id> scid4 = new set<Id>();
                set<Id> scid5 = new set<Id>();
                Set<Id> Cancelset = new Set<Id>(); 
                set<Id> Expset = new set<Id>();
                set<Id> CancelWOset = new set<Id>();
                Set<Id> OPPRenSet = new Set<Id>();
                Set<Id> ScSlset= new Set<Id>();
                set<Id> scid2=New set<Id>();
                set<id> scOwnerid =New set<id>();
                Set<id> oppidset = new Set<id>();
                
                set<id> scparIdset = new set<id>();//added for Price Field Update
                list<SVMXC__Service_Contract__c> ScParList = new list<SVMXC__Service_Contract__c>();//added for Price Field Update
                
                for(SVMXC__Service_Contract__c sc: Trigger.new){
                    scIdSet.add(sc.Id);
                    if((sc.SVMXC__Active__c == true && Trigger.oldMap.get(sc.id).SVMXC__Active__c == false) && sc.IsEvergreen__c == False && sc.BOContractType__c !=ETL && sc.LeadingBusinessBU__c != null && sc.LeadingBusinessBU__c!='' && sc.RecordTypeId != ServiceContractRT && sc.RecordTypeId != ServiceContractConnectedRT){
                        scToprocessList.add(sc);
                        if(sc.SoldtoAccount__c != null)
                            accIdSet.add(sc.SoldtoAccount__c);
                    }
                    // added by anand
                    
                    if((sc.RecordTypeid == ServiceLineRT ||sc.RecordTypeId ==ServiceLineConnectedRT)&& sc.SVMXC__Service_Plan__c!= null && sc.SVMXC__Service_Plan__c!= trigger.OldMap.get(sc.Id).SVMXC__Service_Plan__c){
                        SPID.add(sc.SVMXC__Service_Plan__c);
                        SPlist.add(sc);
                        // Added by Suhail for june release 2016 BR-8201 start
                          if(sc.SKUComRef__c != null && sc.SVMXC__Service_Plan__c != null){
                          PMPlanList.add(sc);              
                          }
                           
                         if(PMPlanList != null && PMPlanList.size()>0){
                         AP_ServiceContract.autoPopulatePMplan(PMPlanList);
                       }
                       // Added by Suhail for june release 2016 BR-8201 end
                    }
                    
                    /* SVMXC__Service_Contract__share SCShare= new SVMXC__Service_Contract__share();
                                    SCShare.ParentId = sc.id; 
                                    SCShare.UserOrGroupId = SC.SVMXC__Sales_Rep__c;
                                    SCShare.AccessLevel = 'Edit';
                                    
                                    SCShareLst.add(SCShare);*/
                        
                    
                    
                    if((sc.RecordTypeid == ServiceLineRT ||sc.RecordTypeId ==ServiceLineConnectedRT) && sc.SVMXC__Service_Level__c!= null && trigger.OldMap.get(sc.Id).SVMXC__Service_Level__c !=sc.SVMXC__Service_Level__c){
                        
                        slslaset.add(sc.id);
                        sllist.add(sc);
                    }
                    
                    
                    if((sc.RecordTypeId == ServiceContractRT ||sc.RecordTypeId == ServiceContractConnectedRT) && sc.status__c == 'PCANC' ) {
                        Cancelset.add(sc.id);
                    }
                    
                    if((sc.RecordTypeId == ServiceContractRT ||sc.RecordTypeId == ServiceContractConnectedRT) && sc.SVMXC__Active__c == FALSE && sc.Tech_ContractCancel__c == TRUE ) 
                    {
                        ScSlset.add(sc.id);
                    }
                    
                    if((sc.RecordTypeid == ServiceLineRT ||sc.RecordTypeId ==ServiceLineConnectedRT) && sc.SVMXC__End_Date__c!=trigger.oldmap.get(sc.Id).SVMXC__End_Date__c){
                        scid4.add(sc.ParentContract__c);
                    }
                    if((sc.RecordTypeId == ServiceContractRT ||  sc.RecordTypeId == ServiceContractConnectedRT ) && sc.status__c== 'PCANC' || sc.status__c== 'CAN' ){
                        if(sc.LatestRenewalOpportunity__c != null)  
                            OPPRenSet.add(sc.LatestRenewalOpportunity__c);
                    }   
                    if((sc.RecordTypeId == ServiceContractRT ||  sc.RecordTypeId == ServiceContractConnectedRT )  && sc.Tech_EndDate__c == true && sc.SVMXC__Active__c == FALSE && sc.status__c == 'EXP')
                        Expset.add(sc.id);
                    
                    if( (sc.RecordTypeId == ServiceContractRT ||sc.RecordTypeId == ServiceContractConnectedRT) && sc.Status__c == 'CAN'  )
                    {
                        CancelWOset.add(sc.id);
                        
                    }
                    if((sc.RecordTypeId == ServiceContractRT ||sc.RecordTypeId == ServiceContractConnectedRT) && sc.SVMXC__End_Date__c!=trigger.oldmap.get(sc.id).SVMXC__End_Date__c){
                        oppidset.add(sc.LatestRenewalOpportunity__c);
                        scprocesslist.add(sc); 
                    }
                    if((sc.RecordTypeId == ServiceContractRT ||sc.RecordTypeId == ServiceContractConnectedRT) && (sc.SVMXC__Company__c!=trigger.oldmap.get(sc.id).SVMXC__Company__c ||sc.SoldtoAccount__c!= trigger.oldmap.get(sc.id).SoldtoAccount__c ||sc.DefaultInstalledAtAccount__c!=trigger.oldmap.get(sc.id).DefaultInstalledAtAccount__c ||sc.MaininstalledatContact__c!=trigger.oldmap.get(sc.id).MaininstalledatContact__c|| sc.BillToAccount__c!=trigger.oldmap.get(sc.id).BillToAccount__c ||sc.LeadingBusinessBU__c!=trigger.oldmap.get(sc.id).LeadingBusinessBU__c)){        
                        scid3.add(sc.id);
                        sclist3.add(sc);
                    } 
                    //  Added by Deepak 
                    
                    if(sc.SVMXC__Contact__c!=null) 
                        ServId.add(sc.SVMXC__Contact__c);
                    
                    //Commented below by VISHNU C for Q3 2016 Release BR-10010 and Added IF condition for remivng the validation on Leading BU
                    //if((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT)&& ((sc.OpportunitySource__c ==Renewal ||sc.OpportunitySource__c ==Amendment) && Trigger.oldMap.get(sc.id).OpportunitySource__c != sc.OpportunitySource__c) 
                    //   && (sc.LeadingBusinessBU__c != null && sc.LeadingBusinessBU__c!='') && (userinfo.getuserid()==sc.ownerid || userinfo.getProfileId()== '00eA0000000uVHPIA2' || userinfo.getuserid()==sc.SVMXC__Sales_Rep__c))
                    
                      if((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT)&& ((sc.OpportunitySource__c ==Renewal ||sc.OpportunitySource__c ==Amendment) && Trigger.oldMap.get(sc.id).OpportunitySource__c != sc.OpportunitySource__c) 
                       && (userinfo.getuserid()==sc.ownerid || userinfo.getProfileId()== '00eA0000000uVHPIA2' || userinfo.getuserid()==sc.SVMXC__Sales_Rep__c))
                        
                    {
                        renewcontractlist.add(sc);
                    }  
                    else if((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT)&& ((sc.OpportunitySource__c ==Renewal ||sc.OpportunitySource__c ==Amendment) && Trigger.oldMap.get(sc.id).OpportunitySource__c != sc.OpportunitySource__c) 
                            && (sc.LeadingBusinessBU__c != null && sc.LeadingBusinessBU__c!='') && (userinfo.getuserid()!=sc.ownerid || userinfo.getProfileId()!= '00eA0000000uVHPIA2' || userinfo.getuserid()!=sc.SVMXC__Sales_Rep__c))
                    {
                        
                        
                        sc.addError('You can not create opportunity  as your not owner or sales Rep  for this contract or sysadmin');
                    } 
                    
                    if((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT) && 
                       (sc.Tech_RenewalOpportunityCreat__c == True && sc.Tech_RenewalOpportunityCreat__c!=Trigger.oldMap.get(sc.id).Tech_RenewalOpportunityCreat__c))
                    {
                        if(sc.LatestRenewalOpportunity__c == null)
                        {
                            newPorcess.add(sc);
                        }
                        else 
                        {
                            sc.addError('A Renewal Opportunity has already been generated for this Service Maintenance Contract.');
                        }
                    }
                    
                    
                    
                    if((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT) && (trigger.oldmap.get(sc.id).SVMXC__Active__c != sc.SVMXC__Active__c && sc.SVMXC__Active__c ==true) && (sc.IsEvergreen__c == true && sc.SendCustomerRenewalNotification__c == true) && (sc.LeadingBusinessBU__c != null && sc.LeadingBusinessBU__c!=''))
                    {
                        
                        if(sc.LatestRenewalOpportunity__c == null)
                        {
                            newPorcess.add(sc);
                        }
                        else 
                        {
                            sc.addError('A Renewal Opportunity has already been generated for this Service Maintenance Contract.');
                        }
                        
                    }
                    
                    
                    
                    
        // Added by Hari: New Renewal Oppty Generation
                /*  if((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT)&& (sc.OpportunitySource__c ==Amendment && Trigger.oldMap.get(sc.id).OpportunitySource__c != sc.OpportunitySource__c) 
                && (sc.LeadingBusinessBU__c != null && sc.LeadingBusinessBU__c!=''))
                {
                newPorcess.add(sc);           
                }
                else if(((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT) && sc.Tech_RenewalOpportunityCreat__c == true)
                ||((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT ) && (sc.OpportunitySource__c == Renewal && Trigger.oldMap.get(sc.id).OpportunitySource__c != sc.OpportunitySource__c && sc.IsEvergreen__c == False))
                || ((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT) && trigger.oldmap.get(sc.id).SVMXC__Active__c != sc.SVMXC__Active__c && sc.SVMXC__Active__c ==true && sc.IsEvergreen__c == true && (sc.SendCustomerRenewalNotification__c == true) && (sc.LeadingBusinessBU__c != null && sc.LeadingBusinessBU__c!='')))
                {
                if(sc.LatestRenewalOpportunity__c == null)
                {
                newPorcess.add(sc);
                }
                else if(sc.LatestRenewalOpportunity__c != null && trigger.oldmap.get(sc.id).LatestRenewalOpportunity__c != sc.LatestRenewalOpportunity__c)
                {
                sc.Name.addError('A Renewal Opportunity has already been generated for this Service Maintenance Contract.');
                }
                }*/
                    //Added by Deepak For April-2015 Release
                    if(trigger.oldmap.get(sc.id).SVMXC__Active__c != sc.SVMXC__Active__c && (sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId ==ServiceContractConnectedRT))
                        // if((trigger.oldmap.get(sc.id).SVMXC__Active__c != sc.SVMXC__Active__c || (sc.SVMXC__Start_Date__c<=System.Today() && sc.Status__c == 'VAL' && (sc.SVMXC__Start_Date__c !=trigger.OldMap.get(sc.Id).SVMXC__Start_Date__c || sc.Status__c !=trigger.OldMap.get(sc.Id).Status__c)))
                        //&& (sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId ==ServiceContractConnectedRT))
                    {
                        system.debug('\n %%%%%%%%%%%%%%%%%%%%%%%');
                        scid2.add(sc.Id);
                        sclist2.add(sc); 
                        
                    }
                    if(sc.ParentContract__c!=null && (sc.RecordTypeId == ServiceLineRT ||sc.RecordTypeId ==ServiceLineConnectedRT) && (sc.SVMXC__Active__c !=trigger.OldMap.get(sc.Id).SVMXC__Active__c || sc.SVMXC__Contract_Price2__c !=trigger.OldMap.get(sc.Id).SVMXC__Contract_Price2__c))
                    {
                        scid1.add(sc.ParentContract__c);
                        sclist1.add(sc);
                    }
                    /*
            //Addd on 13-05-2015
                    if((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT) 
                    && sc.Tech_PriceUpade__c ==true && trigger.OldMap.get(sc.Id).Tech_PriceUpade__c != true){

                    scparIdset.add(sc.Id);
                    ScParList.add(sc);
                    }*/
                    if((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId ==ServiceContractConnectedRT) && sc.OwnerId!= null && trigger.OldMap.get(sc.Id).OwnerId!=sc.OwnerId)
                    {
                        scOwnerid.add(sc.Id);
                        scOwnerlist.add(sc); 
                    }
                    
                    
                    
                }
                scToUpdateList =[Select Id,LatestOpportunityNotif__c From SVMXC__Service_Contract__c Where Id=:scIdSet];
                if(scToprocessList != null && scToprocessList.size()>0){
                    
                    if(accIdSet!= null && accIdSet.size()>0){
                        Map<id,Account> accMap = new Map<id,Account>();
                        List<Opportunity> opportunities = new List<Opportunity>();
                        List<OpportunityNotification__c> oppNotifications = new List<OpportunityNotification__c>();
                        
                        List<Account> accounts = [select id, Name, Country__c from Account where id in :accIdSet];
                        if(accounts != null && accounts.size()>0){
                            accMap.putAll(accounts);
                        }
                        for(SVMXC__Service_Contract__c sc:scToprocessList ){
                            if(accMap.containsKey(sc.SoldtoAccount__c)){
                                // Opportunity opp = AP38_ContractTrigger.createNewServiceContractOpportunity(sc, accMap.get(sc.SVMXC__Company__c ));
                                OpportunityNotification__c oppNotification = AP38_ContractTrigger.createOpportunityNotification(sc, accMap.get(sc.SoldtoAccount__c));
                                oppNotifications.add(oppNotification);
                                // opportunities.add(opp );
                            }
                        }
                                            /*
                        if(opportunities != null && opportunities .size()>0){
                        insert opportunities ;
                        }
                        */
                        if(oppNotifications != null && oppNotifications .size()>0){
                            Database.SaveResult[] sresults= Database.insert(oppNotifications , false);
                            //insert oppNotifications ;
                            
                                            /* for(Integer k=0;k<sresults.size();k++ )
                    {
                    Database.SaveResult sr =sresults[k];
                    if(sr.issuccess())
                    { 
                    */
                            for(OpportunityNotification__c oppNotif :oppNotifications)
                            {
                                if(scToUpdateList.size()>0)
                                {
                                    for(SVMXC__Service_Contract__c sc :scToUpdateList)
                                    {
                                        sc.LatestOpportunityNotif__c=oppNotif.Id;
                                        //database.saveResult updt = Database.Update(sc,false);
                                        update scToUpdateList;
                                    }
                                }
                            }
                            for(Integer k=0;k<sresults.size();k++ )
                            {
                                Database.SaveResult sr =sresults[k];
                                if(!sr.isSuccess())
                                {
                                    
                                    String Message ='';
                                    for(Database.Error err : sr.getErrors()) 
                                    {
                                        Message= ' '+err.getFields()+' Error : '+err.getMessage();
                                    }
                                    oppNotifications[k].addError(Message );
                                }
                            }   
                            
                        }
                        
                    }
                }
                
                /*
                Added by: Ramu Veligeti
                Date: 17/07/2013
                Update TECH_IsSVMXRecordPresent__c on Account object if the Account is used in 
                ServiceMax related objects like Work Order, Installed Product, Location, Service Contract etc..
                */
                List<SVMXC__Service_Contract__c> sclist = New List<SVMXC__Service_Contract__c>();//Added for April-2015 Release By Deepak
                Set<Id> AccId = new Set<Id>();
                for(SVMXC__Service_Contract__c a:trigger.new)
                {
                    
                    if(a.SVMXC__Company__c!=null && a.SVMXC__Company__c != trigger.OldMap.get(a.Id).SVMXC__Company__c ) 
                    {   
                        sclist.add(a);//Added for April-2015 Release By Deepak
                        AccId.add(a.SVMXC__Company__c);
                        
                    }
                }
                if(AccId !=null)
                    SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
                /*****************************
                    Modified by Deepak
                    April-14 Release(20/02/14) 
                    Purpose- For AP_IsSVMXRelatedContact Update
    ******************************/            
                if(ServId !=null)
                    AP_IsSVMXRelatedContact.UpdateContact(ServId );  
                
                /**************************************************************************
                Modified by Anand
                April-15 Release
                Purpose- generating included services and cancelling  PM plans, WorkOrders and Opportunities

                ****************************************************************************/
                        /*if(SPID!=null&&SPID.size()>0){
            AP_ServiceContract.IncludedServicesfromServicePlan(SPlist,SPID);
            }*/
                
                if(Expset!=null && Expset.size()>0){
                    AP_ServiceContract.ContractExpire(Expset);
                }
                
                if(OPPRenSet !=null && OPPRenSet.size()>0){
                    AP_ServiceContract.OpportunityUpdate(OPPRenSet);
                    
                }
                if(scid4!=null && scid4.size()>0)
                    AP_ServiceContract.LatestDateUpdate(scid4);
                
                
                if(slslaset!=null && slslaset.size()>0){
                    AP_ServiceContract.Coveredproductslaterm(slslaset,sllist);
                }
                
                if(oppidset!=null && oppidset.size()>0){
                    AP_ServiceContract.OPPUpdate(oppidset,scprocesslist);
                }
                
                if(ScSlset!=null && ScSlset.size()>0){
                    AP_ServiceContract.ServiceLineDeactive(ScSlset);
                }
                
                if(Cancelset !=null && Cancelset.size()>0){
                    AP_ServiceContract.ContractCancel(Cancelset);
                }
                if(CancelWOset !=null && CancelWOset.size()>0){
                    AP_ServiceContract.WorkOrderUpdate(CancelWOset);
                }
                
                if(scid3!=null && scid3.size()>0)
                    AP_ServiceContract.ServicelineaccountUpdate(scid3,sclist3); 
                
                
                
                /*****************************
                    Modified by Hari
                    April-14 Release(13/02/14) 
                    Purpose- Renewal Opportunity Generation
                    ******************************/
                if(newPorcess != null && newPorcess.size()>0){
                    AP_ServiceContract.RenewalOpportunity(newPorcess);
                    
                }
                
                /*****************************
                    Modified by Deepak
                    April-14 Release(15/12/14) 
                    Purpose- Contract Price and No of service line Update
                    ***********************************/
                if(scid1.size()>0) {
                   AP_ServiceContract.SericesContractPriceupdated(scid1);
               }
                  
              //  AP_ServiceContract.SericesContractPrice(sclist1,scid1); 
                if(scid2!=null && sclist2.size()>0){
                    AP_ServiceContract.SlUpdate(sclist2,scid2);
                    system.debug('!!!!!!');
                }
                if(scOwnerid!=null && scOwnerlist.size()>0){
                    AP_ServiceContract.slineOwnerUpdate(scOwnerlist,scOwnerid);
                }
              
                if(renewcontractlist!=null && renewcontractlist.size() > 0 )
                {   
                    AP_ServiceContract_RenewalOpportunity.RenewalOpportunity(renewcontractlist);
                }
                
                
                //Added on 13-05-2015
                /*if(scparIdset!=null && scparIdset.size()>0 && ScParList.size()>0)    
                    AP_ServiceContract.ContractPriceUpdate(scparIdset,ScParList);*/
            }
        }
    }