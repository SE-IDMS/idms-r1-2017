trigger CountryChannelsAfterInsert on CountryChannels__c (after insert) {
	if (Utils_SDF_Methodology.canTrigger('AP_CountryChannels_Records')) {
	    AP_CountryChannels_Records.createCountryChannelDefaultUserRegFields(trigger.new);
	    AP_CountryChannels_Records.createDefaultEmailConfigFields(trigger.new);
	}
}