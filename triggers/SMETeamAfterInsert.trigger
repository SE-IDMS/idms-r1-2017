/*
    Author          : Ankit Badhani (ACN) 
    Date Created    : 18/05/2011
    Description     : SME Team after insert event trigger.
    
      Modified by : Shruti Karn
    Description : To create a Task for each SME Team Member Added to the Quote Link
    Date Modified : 12 April 2012

*/

trigger SMETeamAfterInsert on RIT_SMETeam__c (after insert){
    if(Utils_SDF_Methodology.canTrigger('AP17'))
    {  
        System.Debug('**** SMETeamAfterInsert Trigger Start ****');
        //List<RIT_SMETeam__c> asd=[select QuoteLink__r.OpportunityName__r.id from RIT_SMETeam__c limit 1];
        
        System.debug('asd#'+trigger.new[0].QuoteLink__r.OpportunityName__r.accountid);
        Map<Id,RIT_SMETeam__c> SMETeamMap = new Map<Id,RIT_SMETeam__c>();
                
        for(RIT_SMETeam__c SMETeamObj:Trigger.new)
        {        
            SMETeamMap.put(SMETeamObj.id,SMETeamObj);
        }
    
        AP17_SharingAccessToSMETeam.insertSMETeamMember(SMETeamMap);
               
        
               
    }
    
   
    if(Utils_SDF_Methodology.canTrigger('AP51'))
    { 
         //Added by Shruti Karn to create tasks for each SME Member
        AP51_CreateTaskforSME.createTask(trigger.newMap);
        System.Debug('**** SMETeamAfterInsert Trigger End ****');
    }
    
}