trigger RPDTBeforeInsert on RMAShippingToAdmin__c (before insert) {
  
  /*********************************************************************************
// Trigger Name     : RPDTBeforeInsert
// Purpose          : Uniqness of the records
// Created by       : Global Delivery Team - IPO
// Date created     : 19-NOV-2012
// Modified by      :
// Date Modified    :
// Remarks          : December 2012 Release
///********************************************************************************/
if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_NewRPDT'))
{      
   AP_RPDT_CreateNewRPDT.createNewRPDT(trigger.new);
    
}

}