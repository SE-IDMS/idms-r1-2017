/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 03-Feb-2012
    Description     : Connect Team Members Before Update event trigger.
*/

trigger ConnectTeamMembersBeforeUpdate on Team_Members__c (before update) {

  System.Debug('****** ConnectTeamMembersBeforeUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('ConnectTeamMembersBeforeUpdate'))
    {
    
    
     if((Trigger.new[0].Entity__c != Trigger.old[0].Entity__c) || (Trigger.new[0].Team_Name__c != Trigger.old[0].Team_Name__c) || (Trigger.new[0].GSC_Region__c != Trigger.old[0].GSC_Region__c)|| (Trigger.new[0].Partner_Region__c != Trigger.old[0].Partner_Region__c)|| (Trigger.new[0].Smart_Cities_Division__c != Trigger.old[0].Smart_Cities_Division__c)){ 
        ConnectTeamMembersTriggers.TeamMembersBeforeUpdate(Trigger.new);
         ConnectTeamMembersTriggers.TeamMembersBeforeInsert_ValidateScopeEntity(Trigger.New);             
         ConnectNetworkTeamMembersProgramTriggers.ConnectNetworkTeamMembersBeforeUpdate(Trigger.New, Trigger.Old);
         
        if (Trigger.new[0].ValidateScopeonInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_Deployment_Network_Insert_ERR1);
      }         
        
 }
 System.Debug('****** ConnectTeamMembersBeforeUpdate Trigger End ****'); 
}