/* 
Trigger After Insert  

*/

trigger AnnouncementAfterInsert on Announcement__c (after Insert) {

   AP_COLA_Announcement.UpdateOfferDescription(trigger.new);
}