trigger OpportunityAssessmentResponse on OpportunityAssessmentResponse__c (after insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_ResponseHandler')) {
        AP_ResponseHandler.checkResponseAssessment(Trigger.new);
    }
}