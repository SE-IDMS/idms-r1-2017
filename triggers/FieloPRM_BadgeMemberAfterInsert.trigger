/********************************************************************
* Company: Fielo
* Developer: 
* Created Date: 30/03/2015
* Description: 
********************************************************************/

trigger FieloPRM_BadgeMemberAfterInsert on FieloEE__BadgeMember__c (after insert) {
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_BadgeMemberTriggers')){
        List<FieloEE__BadgeMember__c> triggerNew = FieloPRM_AP_BadgeMemberTriggers.filterTriggerNew(trigger.new);
        if(!triggerNew.isEmpty()){
            FieloPRM_AP_BadgeMemberTriggers.memberFeatureCreation(triggerNew, trigger.OldMap);
            FieloPRM_AP_BadgeMemberTriggers.resetLevel(triggerNew, trigger.OldMap);
            FieloPRM_AP_BadgeMemberTriggers.serializationOnMember(triggerNew, trigger.OldMap);
        }
    }
}