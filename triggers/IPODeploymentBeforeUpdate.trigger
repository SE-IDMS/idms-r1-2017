trigger IPODeploymentBeforeUpdate on IPO_Strategic_Deployment_Network__c (before update) {

  System.Debug('****** ConnectTeamMembersBeforeUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('IPODeploymentBeforeUpdate'))
    {
    
    
     if((Trigger.new[0].Entity__c != Trigger.old[0].Entity__c) || (Trigger.new[0].Scope__c != Trigger.old[0].Scope__c)){ 
        IPODeploymentNetworkTriggers.IPODeploymentNetworkBeforeUpdate(Trigger.new);
         IPODeploymentNetworkTriggers.IPODeploymentNetworkBeforeInsert_ValidateScopeEntity(Trigger.New);             
         IPOStrategicNetworkTriggers.IPOStrategicNetworkBeforeUpdate(Trigger.New,Trigger.Old);
         
        if (Trigger.new[0].ValidateScopeonInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_Deployment_Network_Insert_ERR1);
      }         
        
 }
 System.Debug('****** ConnectTeamMembersBeforeUpdate Trigger End ****'); 
}