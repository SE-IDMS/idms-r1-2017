/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 02-Oct-2012
    Description     : Connect Cascading KPI After Insert event trigger.
*/

trigger CascadingKPITargetAfterInsert on Cascading_KPI_Target__c (after insert) {

System.Debug('****** CascadingKPITargetAfterInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CascadingKPITargetAfterInsert'))
    {
     ConnectCascadingKPITargetTriggers.ConnectCascadingKPITargetAfterInsert(Trigger.new);   
        
    }
 System.Debug('****** CascadingKPITargetAfterUpdate Trigger End ****'); 
 }