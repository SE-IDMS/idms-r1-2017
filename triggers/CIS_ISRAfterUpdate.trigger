//@Author:- Ashish Sharma SESA340747
//@Date:- 22/09/2014
//@Description:- This trigger is written to share ISR once EU CIS correspondent team is assigned.
//@Events :- Before Insert
trigger CIS_ISRAfterUpdate  on CIS_ISR__c (after update) {
    if(Test.isRunningTest() || Utils_SDF_Methodology.canTrigger('AP_CIS_ISRHandler')){
    List<CIS_ISR__c> isrListToShare = new List<CIS_ISR__c>();
    List<CIS_ISR__c> isrListToPopulateQueueAndItsMemberEmails = new List<CIS_ISR__c>();
    for(CIS_ISR__c  isrInstance:trigger.new){
        if((isrInstance.CIS_EUCISCorrespondentsTeam__c!=null)&&(trigger.newMap.get(isrInstance.Id).CIS_EUCISCorrespondentsTeam__c!=trigger.oldMap.get(isrInstance.Id).CIS_EUCISCorrespondentsTeam__c)){
            isrListToShare.add(isrInstance);
        }   
    }
    
    if(!isrListToShare.isEmpty()){
        AP_CIS_ISRHandler.shareISRWithPublicGroup(isrListToShare);
    }
    
    List<CIS_ISR__c> reminderEmailForStatusSent = new List<CIS_ISR__c>();
    List<CIS_ISR__c> reminderEmailForStatusAcknowledged = new List<CIS_ISR__c>();
    List<CIS_ISR__c> reminderEmailForStatusReportValidated = new List<CIS_ISR__c>();
    List<CIS_ISR__c> sentStatusIsrEmail = new List<CIS_ISR__c>();
    List<CIS_ISR__c> acknowledgedStatusIsrEmail = new List<CIS_ISR__c>();
    List<CIS_ISR__c> reportedStatusISREmil = new List<CIS_ISR__c>();
    List<CIS_ISR__c> reportValidatedStatusISREmil = new List<CIS_ISR__c>();
    List<CIS_ISR__c> toBeClosedStatusISREmil = new List<CIS_ISR__c>();
    List<CIS_ISR__c> sendEmailOnEUTeamChange = new List<CIS_ISR__c>();
    for(CIS_ISR__c  isrInstance:trigger.new){
        if((trigger.newMap.get(isrInstance.Id).CIS_Status__c !=trigger.oldMap.get(isrInstance.Id).CIS_Status__c )&&(isrInstance.CIS_Status__c =='Sent')&&(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c)){
            sentStatusIsrEmail.add(isrInstance);
        } 
        if((trigger.newMap.get(isrInstance.Id).CIS_Status__c !=trigger.oldMap.get(isrInstance.Id).CIS_Status__c )&&(isrInstance.CIS_Status__c =='Acknowledged')&&(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c)){
            acknowledgedStatusIsrEmail.add(isrInstance);
        }
        if((trigger.newMap.get(isrInstance.Id).CIS_Status__c !=trigger.oldMap.get(isrInstance.Id).CIS_Status__c )&&(isrInstance.CIS_Status__c =='Report validated')&&(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c)){
            reportValidatedStatusISREmil.add(isrInstance);
        } 
        if((trigger.newMap.get(isrInstance.Id).CIS_Status__c !=trigger.oldMap.get(isrInstance.Id).CIS_Status__c )&&(isrInstance.CIS_Status__c =='Reported')&&(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c)){
            reportedStatusISREmil.add(isrInstance);
        }  
        if((trigger.newMap.get(isrInstance.Id).CIS_Status__c !=trigger.oldMap.get(isrInstance.Id).CIS_Status__c )&&(isrInstance.CIS_Status__c =='To be closed')&&(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c)){
            toBeClosedStatusISREmil.add(isrInstance);
        }
        
         if((trigger.newMap.get(isrInstance.Id).CIS_EUCISCorrespondentsTeam__c !=trigger.oldMap.get(isrInstance.Id).CIS_EUCISCorrespondentsTeam__c)&&(trigger.newMap.get(isrInstance.Id).CIS_Status__c ==trigger.oldMap.get(isrInstance.Id).CIS_Status__c)){
            sendEmailOnEUTeamChange.add(isrInstance);
        }
        
        if(trigger.newMap.get(isrInstance.Id).Sentmorethan2days__c !=trigger.oldMap.get(isrInstance.Id).Sentmorethan2days__c ){
            reminderEmailForStatusSent.add(isrInstance);
        }
        
        if(trigger.newMap.get(isrInstance.Id).Acknowledgedmorethan7days__c !=trigger.oldMap.get(isrInstance.Id).Acknowledgedmorethan7days__c ){
            reminderEmailForStatusAcknowledged.add(isrInstance);
        }
        
        if(trigger.newMap.get(isrInstance.Id).ReportValidatedmorethan7days__c !=trigger.oldMap.get(isrInstance.Id).ReportValidatedmorethan7days__c ){
            reminderEmailForStatusReportValidated.add(isrInstance);
        }
        
    }
    
    if(!sentStatusIsrEmail.isEmpty()){
        AP_CIS_ISRHandler.sendEmailToCISTeamsOnSentStatus(sentStatusIsrEmail);
    }
     if(!acknowledgedStatusIsrEmail.isEmpty()){
        AP_CIS_ISRHandler.sendEmailToCISTeamsOnAcknowledgedStatus(acknowledgedStatusIsrEmail);
    }
    if(!reportValidatedStatusISREmil.isEmpty()){
        AP_CIS_ISRHandler.sendEmailToCISTeamsOnReportValidatedStatus(reportValidatedStatusISREmil);
    }
    if(!reportedStatusISREmil.isEmpty()){
        AP_CIS_ISRHandler.sendEmailToCISTeamsOnReportedStatus(reportedStatusISREmil);
    }
    if(!toBeClosedStatusISREmil.isEmpty()){
        AP_CIS_ISRHandler.sendEmailToCISTeamsOnToBeClosedStatus(toBeClosedStatusISREmil);
    }
    if(!sendEmailOnEUTeamChange.isEmpty()){
        AP_CIS_ISRHandler.sendEmailOnEUTeamChange(sendEmailOnEUTeamChange);
    }
    if(!reminderEmailForStatusSent.isEmpty()){
        AP_CIS_ISRHandler.reminderEmailForStatusSent(reminderEmailForStatusSent);
    }
    if(!reminderEmailForStatusAcknowledged.isEmpty()){
        AP_CIS_ISRHandler.reminderEmailForStatusAcknowledged(reminderEmailForStatusAcknowledged);
    }
    if(!reminderEmailForStatusReportValidated.isEmpty()){
        AP_CIS_ISRHandler.reminderEmailForStatusReportValidated(reminderEmailForStatusReportValidated);
    }
  }
}