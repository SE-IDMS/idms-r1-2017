trigger ConnectMilestoneBeforeInsert on Connect_Milestones__c (before insert) {

/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 28-Feb-2012
    Description     : Connect Milestone Before Insert event trigger.
*/

System.Debug('****** ConnectMilestoneBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('ConnectMilestoneBeforeInsert'))
    {
        ConnectMilestoneTriggers.ConnectMilestoneBeforeInsertUpdate(Trigger.new);
         
        if (Trigger.new[0].Validate_Insert_Update__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_Milestone_Insert_ERR1);
 }
 System.Debug('****** ConnectMilestoneBeforeInsert Trigger End ****'); 
}