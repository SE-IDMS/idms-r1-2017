/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 8-June-2012
    Description     : Connect Entity Progress Update event trigger.
*/


trigger ConnectEntityBeforeUpdate on Connect_Entity_Progress__c (before update) {

  System.Debug('****** ConnectEntityBeforeUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('EntityBeforeUpdate'))
    {
        
        //ConnectCascadingMilestoneTriggers.CascadingMilestoneBeforeUpdate_AppendTextColoumns(Trigger.new);
     if((Trigger.new[0].Entity__c != Trigger.old[0].Entity__c) || (Trigger.new[0].Scope__c != Trigger.old[0].Scope__c) || (Trigger.new[0].GSC_Region__c != Trigger.old[0].GSC_Region__c)|| (Trigger.new[0].Partner_Region__c != Trigger.old[0].Partner_Region__c)|| (Trigger.new[0].Smart_Cities_Division__c != Trigger.old[0].Smart_Cities_Division__c)){
                  ConnectEntityTriggers.ConnectEntityBeforeInsertUpdate_ValidateEntityRecord(Trigger.new);
                  ConnectEntityTriggers.ConnectEntityBeforeInsertUpdate_ValidateScopeEntity(Trigger.new);
                  
                   if(Trigger.new[0].ValidateEntityInsertUpdate__c == true)
                
                    Trigger.new[0].Name.AddError(Label.EntityValidateERR);
                    
           if (Trigger.new[0].ValidateScopeInsertUpdate__c == True)
        
                   Trigger.new[0].Name.AddError(Label.Connect_EntityProgress_InsertUpdate_ERR1);
                   }
 }
 System.Debug('****** ConnectEntityBeforeUpdate Trigger End ****'); 
}