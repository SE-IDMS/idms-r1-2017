/*
    Author          : Srikant Joshi
    Date Created    : 11/07/2012
    Description     : DMT Funding Entity before delete event trigger.
*/
trigger FE_FundingEntityBeforeDelete on DMTFundingEntity__c (After Delete) 
{
    System.Debug('****** FE_FundingEntityBeforeDelete Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('FundingEntityTriggerbeforeDelete'))
    {
        System.Debug('Value New Update: ' + Trigger.new);
        System.Debug('Value NewMap Update: ' + Trigger.newMap);
        
        FE_FundingEntityTriggers.fundingentitybeforeDelete(Trigger.old);

    }
    System.Debug('****** FE_FundingEntityBeforeDelete Trigger End ****'); 
}