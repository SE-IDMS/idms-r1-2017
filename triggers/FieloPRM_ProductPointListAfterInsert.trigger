/******************************************
* Developer: Fielo Team                   *
*******************************************/
trigger FieloPRM_ProductPointListAfterInsert on FieloPRM_ProductPointList__c (After Insert) {


    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_ProductPointListTriggers')){
        FieloPRM_AP_ProductPointListTriggers.createPointValues();
    }
   
}