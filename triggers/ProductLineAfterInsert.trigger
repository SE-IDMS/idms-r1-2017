/*       Author: Pooja Gupta  
 Date Of Creation: 01/01/2013   
 Description : Product Line After Insert event trigger

 Modification History:
 Author: Siddharth N (GD solutions)         
 Description: Logic moved entirely to a handler
*/


trigger ProductLineAfterInsert on OPP_ProductLine__c (After Insert){
  System.Debug('********* ProductLineAfterInsert Trigger Start *********');
  //if(Utils_SDF_methodology.canTrigger('AP_ProductLineTrigger') && VFC61_OpptyClone.isOpptyClone == false)
  if(Utils_SDF_methodology.canTrigger('AP_ProductLineTrigger'))
  {    
    if(Label.CLOCT13SLS47 != 'UPDATE_OPPTY_WITH_MANY_LINES')
    {
         AP_ProductLineAfterInsertHandler handler = new AP_ProductLineAfterInsertHandler();
         handler.OnAfterInsert(Trigger.new);    
    }
    else
    {
        List<OPP_ProductLine__c> lstProductLinestobeProcessedInHandler = new List<OPP_ProductLine__c>();
        List<OPP_ProductLine__c> lstProductLinestobeProcessedInBatch = new List<OPP_ProductLine__c>();
        Map<Id, Integer> oppProdLineCount = new Map<Id, Integer>();
        Set<Opportunity> updOpps = new Set<Opportunity>();
        List<Opportunity> updateOpps = new List<Opportunity>();
        
        for(OPP_ProductLine__c newProductLineRecord:Trigger.New)
        {
            if(oppProdLineCount.get(newProductLineRecord.Opportunity__c)!=null)
            {
                oppProdLineCount.put(newProductLineRecord.Opportunity__c, oppProdLineCount.get(newProductLineRecord.Opportunity__c)+1);
            }
            else
                oppProdLineCount.put(newProductLineRecord.Opportunity__c, 1);
        }
        for(OPP_ProductLine__c newProductLineRecord:Trigger.New)
        {        
            //Checks if the number of Product Lines for Opportunity is <200
            if((newProductLineRecord.TECH_OpportunityLinesCount__c+oppProdLineCount.get(newProductLineRecord.Opportunity__c))>100  && (newProductLineRecord.TECH_OpportunityLinesCount__c+oppProdLineCount.get(newProductLineRecord.Opportunity__c))<1000)
            {
                updOpps.add(new Opportunity(TECH_IsOpportunityUpdate__c=true,Id=newProductLineRecord.Opportunity__c));
                lstProductLinestobeProcessedInBatch.add(newProductLineRecord);    
            }
            else
            {
               lstProductLinestobeProcessedInHandler.add(newProductLineRecord);    
           }
       }
    
       //Calls the Product Line after update 
       if(lstProductLinestobeProcessedInHandler!=null && !(lstProductLinestobeProcessedInHandler.isEmpty()))
       {
            AP_ProductLineAfterInsertHandler handler = new AP_ProductLineAfterInsertHandler();
            handler.OnAfterInsert(lstProductLinestobeProcessedInHandler);    
        }
        updateOpps.addall(updOpps);
        Database.update(updateOpps);    
    }
}

System.Debug('********* ProductLineAfterInsert Trigger End *********');
}