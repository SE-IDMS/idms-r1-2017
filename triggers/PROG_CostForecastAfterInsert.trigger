/*
    Author          : Ramakrishna Singara (Schneider Electric)
    Date Created    : 12/07/2012
    Description     : Program Costs Forecasts After Insert event trigger.
*/
trigger PROG_CostForecastAfterInsert on ProgramCostsForecasts__c (After Insert) {

    if(Utils_SDF_Methodology.canTrigger('ProgramCostForecastTriggerAfterInsert'))
    {
        PROG_CostForecastTriggers.ProgCostForecastAfterInsert(Trigger.new);
    }

}