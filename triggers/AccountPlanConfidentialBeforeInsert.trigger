trigger AccountPlanConfidentialBeforeInsert on AccountPlanConfidential__c (before insert) 
{
    AP_AccountPlanConfidential_RecordCount.checkAccessToAccount(Trigger.New);
    AP_AccountPlanConfidential_RecordCount.insertAPCIdInAP(Trigger.New);
}