trigger SupportRequestBeforeInsert on OPP_SupportRequest__c (Before Insert) 
{
    if(Utils_SDF_Methodology.canTrigger('AP22'))
    {
     System.Debug('****** SupportRequestBeforeInsert Trigger Start****');
     Map<ID,OPP_SupportRequest__c> supportRequest = new Map<Id,OPP_SupportRequest__c>();
     List<OPP_SupportRequest__c> updateAgreementIds = new List<OPP_SupportRequest__c>();
     Map<String,Set<String>> mapQLsharedToUsers = new Map<String,Set<String>>();
     Set<String> sharedToUsers;
     Map<Id,OPP_SupportRequest__c > mapQLIdSupportRequest= new Map<Id,OPP_SupportRequest__c>();
        
        for(OPP_SupportRequest__c supportReq:trigger.new)
        {
            //AddedforBR-7964 - Additional information on the support request - March 2016 release
            if(supportReq.QuoteLink__c!=NULL && supportReq.TypeOfOffer__c!=NULL )
            {
                if(!(mapQLIdSupportRequest.ContainsKey(supportReq.QuoteLink__c)))
                    mapQLIdSupportRequest.put(supportReq.QuoteLink__c,supportReq);
            }
            supportRequest.put(supportReq.opportunity__c,supportReq);  
            //Populates Agreement field with TECH_AgreementId__c - Modified for BR-6915
            if(supportReq.TECH_AgreementId__c!=null)
                updateAgreementIds.add(supportReq); 
                
            //BR-8607 Added by Uttara - Grant access to Quote Link
            sharedToUsers = new Set<String>();
            if(supportReq.QuoteLink__c != null && supportReq.PMTender__c != null) {
                if(mapQLsharedToUsers.containsKey(supportReq.QuoteLink__c))
                    mapQLsharedToUsers.get(supportReq.QuoteLink__c).add(supportReq.PMTender__c);
                else {         
                    sharedToUsers.add(supportReq.PMTender__c);  
                    mapQLsharedToUsers.put(supportReq.QuoteLink__c,sharedToUsers);
                } 
            }     
            if(supportReq.QuoteLink__c != null && supportReq.AssignedToUser__c != null) {
                if(mapQLsharedToUsers.containsKey(supportReq.QuoteLink__c))
                    mapQLsharedToUsers.get(supportReq.QuoteLink__c).add(supportReq.AssignedToUser__c);
                else {         
                    sharedToUsers.add(supportReq.AssignedToUser__c);  
                    mapQLsharedToUsers.put(supportReq.QuoteLink__c,sharedToUsers);
                }
            }   
                 
        }
        if(supportRequest.size()>0)
        {
            // AP22_SupportRequest.validateUser(supportRequest); test without validating user
            AP22_SupportRequest.updateEmail(supportRequest.values());
        }   
        //Modified for BR-6915
        if(updateAgreementIds.size()>0)
            AP22_SupportRequest.updateAgreement(updateAgreementIds);
        
        if(!mapQLsharedToUsers.isEmpty()) 
            AP16_GrantAcessToQteLink.SupportRequestSharing(mapQLsharedToUsers);
        
         if(!mapQLIdSupportRequest.isEmpty()) 
            AP22_SupportRequest.qlTypeOfOfferPopulation(mapQLIdSupportRequest);
            
     System.Debug('****** SupportRequestBeforeInsert Trigger End****');
    }
}