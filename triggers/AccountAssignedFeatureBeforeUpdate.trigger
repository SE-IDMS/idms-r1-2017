trigger AccountAssignedFeatureBeforeUpdate on AccountAssignedFeature__c (before update) {
    if(Utils_SDF_Methodology.canTrigger('AP_AAF'))
    {
        AP_AccountAssignedFeature.checkDuplicateAAF(trigger.new);
    }   
}