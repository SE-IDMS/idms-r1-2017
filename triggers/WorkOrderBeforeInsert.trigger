/*
@Author: Ramu Veligeti
Created Date: 22-07-2011
Description: This trigger fires on Work Order on Before Insert
**********
Scenarios:
**********
1.  On Insert of Child Work Order, update fields from the Parent Work Order.
2.  Update Work Order ownership to Service Center Queue[Primary Territory 's Owner]
3.  Update Customer location details on Workf Order from Customer Location lookup if customer location exists
4.  Update Case Owner on Work Order from Case lookup if Case exists
5.  Update Contact Phone on Work Order from Contact lookup if Contact exists
6.  Update Account Type on Work Order from Account lookup if Account exists
7.  Update Location details on Work Order from Location lookup if location exists
9.  Update Location and Included Service from PM Plan 
10. Update Owner Email Field From User.
11. update shipToAccount and BillToAccount from SOldToAccount.
11a. Inserting Preferred Technician From Location As Well As Account Location.
12. Create Work Order Group. Update Work Order Group lookup with the new Work Order Group (Incase of Parent WO).
    If the WO is a Child WO then Update WO with the parent's WOGroup value. Also maintain the status of the WOGroup object based on WO Status.
13. Consolidating Certifications (Habilitation Requirements) on the Work Order.

15. Update Service Contract Details from PM Plan 
16. Automatically calculate WO's Primary Territory using Territory Coverage definition

Date Modified       Modified By         Comments
------------------------------------------------------------------------
26-07-2011          Praveen Simha           Added Logic for Second scenario
03-08-2011          Anita D'Souza           Added Logic to update Customer Location details.    
05-11-2012          Jyotiranjan Singhlal    Added Logic to update Case Details
14-11-2012          Jyotiranjan Singhlal    Added Logic to update Contact Details
19-11-2012          Jyotiranjan Singhlal    Added Logic to update Account Details
20-11-2012          Jyotiranjan Singhlal    Added Time Zone in scenario 3
18-02-2013          Jyotiranjan Singhlal    Added Logic for scenario 7
26-03-2013          Chris Hurd              Added Logic for scenario 9
27-03-2013          Deepak Kumar            Added Logic for scenario 10
6-04-2013           Jyotiranjan Singhlal    Commented back office ref# valie for child order
6-04-2013           Jyotiranjan Singhlal    Added longitude and latitude for scenario 7
16-o7-2013          Deepak Kumar            Added the trigger part to update shipToAccount and BillToAccount
16-07-2013          Ramu Veligeti           Added the logic to handle Work Order Group functionality
30-07-2013          Ramu Veligeti           Added the logic to update skills on WO from Habilitation requirements object
05-08-2013          Deepak Kumar            Added Logic to Insert Preferred Technician from Location as well as From Account Location.
05-02-2015          Brahanadam Dhanthuluri  Added logic to populate Service Contract from PM Plan
03-02-2016      Adrian Modolea      Added logic to call ServiceMaxWOTerritoryAssignment apex class
*/
trigger WorkOrderBeforeInsert on SVMXC__Service_Order__c (before insert) 
{
    //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX06')){
    
      //Variablees related to Scenario 1  
      List<SVMXC__Service_Order__c> solst = new List<SVMXC__Service_Order__c>();
      //List<SVMXC__Service_Order__c> wolst = new List<SVMXC__Service_Order__c>();
      Set<Id> pwoId = new Set<Id>();
      
      //Variablees related to Scenario 2
      List<SVMXC__Service_Order__c> wolst3 = new List<SVMXC__Service_Order__c>();
      List<SVMXC__Service_Order__c> wolstnew3 = new List<SVMXC__Service_Order__c>();
      Set<Id> WoterId = new Set<Id>();
      Set<Id> woId3 = new Set<Id>();  
      List<SVMXC__Territory__c> terList = New List <SVMXC__Territory__c>();
      
      //Variablees related to Scenario 3
      //list<CustomerLocation__c> custloc= new list <CustomerLocation__c>();
      //Set<id> wocustloc = New Set<id>();
      //List<SVMXC__Service_Order__c> wolst4 = new List<SVMXC__Service_Order__c>();
      
      //Variablees related to Scenario 4
      List<case> caseown = new list <case>();
      Set<id> wocase = New Set<id>();
      List<SVMXC__Service_Order__c> wolst5 = new List<SVMXC__Service_Order__c>();
      
      //Variablees related to Scenario 5
      List<contact> contactphone = new list <contact>();
      Set<id> wocontact = New Set<id>();
      List<SVMXC__Service_Order__c> wolst6 = new List<SVMXC__Service_Order__c>();
      
      //Variablees related to Scenario 6
      List<account> accntype = new list <account>();
      Set<id> woaccnt = New Set<id>();
      List<SVMXC__Service_Order__c> wolst7 = new List<SVMXC__Service_Order__c>();
      
      //Variablees related to Scenario 7
      list<SVMXC__Site__C> loc= new list <SVMXC__Site__C>();
      Set<id> woloc = New Set<id>();
      List<SVMXC__Service_Order__c> wolst8 = new List<SVMXC__Service_Order__c>();
      
      //Variables related to Scenario 9
      List<SVMXC__Service_Contract_Products__c> covProdList = new List<SVMXC__Service_Contract_Products__c>();
      Set<Id> woPMIdSet = new Set<Id>();
      Set<Id> woIpIdSet = new Set<Id>();
      
      List<SVMXC__Service_Order__c> woList9 = new List<SVMXC__Service_Order__c>();
      
      //Variables related to Scenario 10
      
     //List<User> users = new list <User>();
     Set<id> woOwnerId = New Set<id>();
     List<SVMXC__Service_Order__c> wolst1 = new List<SVMXC__Service_Order__c>();
     Map<Id,User> user1;
     
     //Variable Related to Scenario 11
     list<SVMXC__Service_Order__c> wolist = New list<SVMXC__Service_Order__c>();
     
     //Variable Related to Scenario 15
     list<SVMXC__Service_Order__c> wolst15 = New list<SVMXC__Service_Order__c>();
      list<SVMXC__PM_Plan__c> Pmplanlist2= new  list<SVMXC__PM_Plan__c>();
     set<id>Pmplanset1= new set<id>();
     //variable related to scenario 16 
    set<Id> accid = New set<Id>();
    set<Id> accntid = New set<Id>();
    list<SVMXC__Service_Order__c> workOrderList = New list<SVMXC__Service_Order__c>();
    
    //Variablees related to Scenario 1
    set<Id> scid = New set<Id>();
    list<SVMXC__Service_Order__c> workOList = New list<SVMXC__Service_Order__c>();
    
    //Variable Related to scenario 17
     set<Id> caseid = New Set<Id>();
      //Condition for Scenario 1
      for(SVMXC__Service_Order__c a:trigger.new)
      {
      
          //Fetching only Child Work Orders
          if(a.Parent_Work_Order__c != null)
          {
              solst.add(a);
              pwoId.add(a.Parent_Work_Order__c);
          }
          system.debug('Test-A'+a.SVMXC__Primary_Territory__c);
          //Condition for Scenario 2    
          if(a.SVMXC__Primary_Territory__c!=null)
          {
              wolst3.add(a);
              WoterId.add(a.SVMXC__Primary_Territory__c);
          }
          
          //Condition for Scenario 3
         //if(a.Customer_Location__c != null)
          //{
            //  wolst4.add(a);
              //wocustloc.add(a.Customer_Location__c);
         // }
          
          //Condition for Scenario 4
         if(a.SVMXC__Case__c!= null)
          {
              wolst5.add(a);
              wocase.add(a.SVMXC__Case__c);
          }
          //Condition for Scenario 5
         if(a.SVMXC__Contact__c!= null)
          {
              wolst6.add(a);
              wocontact.add(a.SVMXC__Contact__c);
          }
          //Condition for Scenario 6
         if(a.SVMXC__Company__c!= null)
          {
              wolst7.add(a);
              woaccnt.add(a.SVMXC__Company__c);
          }
         //Condition for Scenario 7
         if(a.SVMXC__Site__c!= null)
          {
              wolst8.add(a);
              woloc.add(a.SVMXC__Site__c);
          }
          
          system.debug('Test-9: ' + a);
          //Condition for Scenario 9
          if (a.SVMXC__PM_Plan__c != null && a.SVMXC__Component__c != null)
          {
            woList9.add(a);
            woPMIdSet.add(a.SVMXC__PM_Plan__c);
            woIpIdSet.add(a.SVMXC__Component__c);
          }
          
         //Condition for Scenario 10
            if(a.OwnerId != null)
            {
              wolst1.add(a);
              woOwnerid.add(a.OwnerId);
            }
        //Condition for Scenario 15
        if(a.SVMXC__PM_Plan__c!=null)
        {
          wolst15.add(a);
          Pmplanset1.add(a.SVMXC__PM_Plan__c);
        }
        if(a.SVMXC__Company__c != null ){
            accid.add(a.SVMXC__Company__c);
            workOrderList.add(a);
        }
        if(a.SVMXC__Service_Contract__c != null ){
        workOList.add(a);
        }
        if(a.SVMXC__Case__c != null && a.SVMXC__Site__c == null)
        {
            caseid.add(a.SVMXC__Case__c);          
        }
        if(a.SVMXC__Case__c != null && a.SVMXC__Company__c != null && a.SVMXC__Site__c == null)
        {
            accntid.add(a.SVMXC__Company__c);          
        }
       }
       //Scenario 17
        //if(caseid != null && caseid.size()>0)//
            //Ap_WorkOrder.LocationFromCase(caseid,trigger.New);
         if(accntid!= null && accntid.size()>0 && userinfo.getProfileId() != System.label.CLDEC14SRV01 )// Hari added on 06-07-2015
            Ap_WorkOrder.LocationFromCase(accntid,trigger.New);

       /**********************************************************************************************************************************/
      // Scenario: 1
      // On Insert of Child Work Order, update fields from the Parent Work Order
      /***********************************************************************************************************************************/
      if(solst.size()>0 && pwoId.size()>0)
      {
          //Fetching Records from the Parent Work Order 
          Map<Id,SVMXC__Service_Order__c> womap = new Map<Id,SVMXC__Service_Order__c>([select Id,SVMXC__Company__c,AddressLine2__c,
                                                                                              Comments_to_Planner__c,Commercial_Reference__c,
                                                                                              SVMXC__Contact__c,Customer_Location__c,
                                                                                              Customer_Reference_Number__c,Estimated_Duration__c,
                                                                                              Is_Billable__c, SVMXC__Order_Type__c,
                                                                                              AdditionalCommercialReferences__c,SVMXC__Primary_Territory__c,
                                                                                              SVMXC__Preferred_Technician__c,SVMXC__Problem_Description__c,
                                                                                              SVMXC__Priority__c,WOPriority__c,Qualifications__c,Reason_for_Rejection__c,
                                                                                              Reports_Required__c,Report_Submission_Date__c,Report_Types__c,
                                                                                              Serial_Number__c,Service_Business_Unit__c,BackOfficeReference__c,
                                                                                              Service_Coordinator__c, SVMXC__Dispatch_Response__c,
                                                                                              SiteSpecificComments__c,CustomerTimeZone__c,
                                                                                              TechnicianType__c,Tools_Required__c,CustomerApproval__c,
                                                                                              Work_Order_Category__c,Work_Order_Notification__c,
                                                                                              WorkOrderSubType__c, SVMXC__City__c, SVMXC__Street__c ,SVMXC__Zip__c,
                                                                                              SVMXC__State__c,SVMXC__Billing_Type__c,SVMXC__Country__c,
                                                                                              ProductDescription__c,ProductLine__c,BusinessUnit__c,
                                                                                              Family__c,PlannerContact__c,ProductFamily__c
                                                                                         from SVMXC__Service_Order__c where Id IN :pwoId]);
          for(SVMXC__Service_Order__c a:solst)
          {
              if(womap.containsKey(a.Parent_Work_Order__c))
              {
                  //Assigning Parent Work Order values to the Child Work Order
                  SVMXC__Service_Order__c so = new SVMXC__Service_Order__c();
                  so = womap.get(a.Parent_Work_Order__c);
                  
                  a.Parent_Work_Order__c = so.Id;
                  a.SVMXC__Company__c = so.SVMXC__Company__c;
                  a.AddressLine2__c = so.AddressLine2__c;                
                  a.Commercial_Reference__c = so.Commercial_Reference__c;
                  a.ProductDescription__c = so.ProductDescription__c;
                  a.ProductFamily__c = so.ProductFamily__c;
                  a.ProductLine__c = so.ProductLine__c;
                  a.BusinessUnit__c = so.BusinessUnit__c;
                  a.Family__c = so.Family__c;
                  a.CustomerTimeZone__c = so.CustomerTimeZone__c;
                  a.SVMXC__Contact__c = so.SVMXC__Contact__c;
                  //a.PlannerContact__c = so.PlannerContact__c;
                  a.Customer_Location__c = so.Customer_Location__c;
                  a.SVMXC__City__c = so.SVMXC__City__c;
                  a.SVMXC__Street__c = so.SVMXC__Street__c;
                  a.SVMXC__Zip__c = so.SVMXC__Zip__c;
                  a.SVMXC__State__c =  so.SVMXC__State__c;
                  a.SVMXC__Dispatch_Response__c = null;
                  a.Reason_for_Rejection__c = null;
                  a.Customer_Reference_Number__c = so.Customer_Reference_Number__c;
                  //a.Estimated_Duration__c = so.Estimated_Duration__c;
                  a.Is_Billable__c = so.Is_Billable__c;
                  //a.SVMXC__Order_Type__c = so.SVMXC__Order_Type__c;
                  //a.AdditionalCommercialReferences__c = so.AdditionalCommercialReferences__c;
                  a.SVMXC__Billing_Type__c = so.SVMXC__Billing_Type__c;
                  a.SVMXC__Country__c = so.SVMXC__Country__c ;
                  a.SVMXC__Primary_Territory__c = so.SVMXC__Primary_Territory__c;
                  a.SVMXC__Preferred_Technician__c = so.SVMXC__Preferred_Technician__c;
                  //a.SVMXC__Problem_Description__c = so.SVMXC__Problem_Description__c;
                  //a.WOPriority__c = so.WOPriority__c;
                  //a.SVMXC__Priority__c = so.SVMXC__Priority__c;
                  a.Qualifications__c = so.Qualifications__c;
                  if(a.SiteSpecificComments__c == null)
                      a.SiteSpecificComments__c = so.SiteSpecificComments__c;                
                  a.Reports_Required__c = so.Reports_Required__c;
                  a.Report_Submission_Date__c = so.Report_Submission_Date__c;
                  a.Report_Types__c = so.Report_Types__c;
                  //a.BackOfficeReference__c= so.BackOfficeReference__c;
                  a.Serial_Number__c = so.Serial_Number__c;
                  //if(a.Service_Business_Unit__c == null)
                  a.Service_Business_Unit__c = so.Service_Business_Unit__c;
                  a.Service_Coordinator__c = so.Service_Coordinator__c;               
                 // a.CustomerApproval__c = so.CustomerApproval__c;
                  a.TechnicianType__c = so.TechnicianType__c;
                  //a.Tools_Required__c = so.Tools_Required__c;
                  a.Work_Order_Category__c = so.Work_Order_Category__c;
                  a.Work_Order_Notification__c = so.Work_Order_Notification__c;
                  //a.WorkOrderSubType__c = so.WorkOrderSubType__c;  
                  wolst3.add(a);
                  WoterId.add(a.SVMXC__Primary_Territory__c);              
              }                       
          }
      }
      
      /**********************************************************************************************************************************/
      // Scenario: 2
      // Update Work Order ownership to Service Center Queue[Primary Territory's Owner]
      /***********************************************************************************************************************************/
      if(wolst3.size()>0)
      {
          terList= [Select id,Name,OwnerId
                   From SVMXC__Territory__c
                   Where Id IN :woterId ];
                   for(SVMXC__Territory__c wtr:terList)   
          {
              for(SVMXC__Service_Order__c svo3:wolst3)
              {
                  if(svo3.SVMXC__Primary_Territory__c == wtr.Id)
                  {
                      svo3.OwnerId=wtr.OwnerId;     //Assignment of Primary Terrotiry's owner as Work Order owner
                      wolstnew3.add(svo3);
                  }
              }
              //if(wolstnew3.size()>0) Update wolstnew3 ;
          }                                       
      }

       /**********************************************************************************************************************************/
      // Scenario: 4
      // Update Case Owner on Work Order from Case lookup if Case exists
      /***********************************************************************************************************************************/
      if(wolst5.size()>0)
      {
           caseown = [select OwnerId,Owner.Name From case Where Id IN :wocase];
          for(case c :caseown)   
          {
              for(SVMXC__Service_Order__c svo5:wolst5)
              {
                  if(svo5.SVMXC__Case__c == c.Id)
                  {
                      svo5.CaseOwner__c = c.Owner.Name;
                  }   
              }   
          }  
      }
      /**********************************************************************************************************************************/
      // Scenario: 5
      // Update Contact Phone on Work Order from Contact lookup if Contact exists
      /***********************************************************************************************************************************/
      if(wolst6.size()>0)
      {
          contactphone = [select MobilePhone,WorkPhone__c  From contact Where Id IN :wocontact];
          for(contact cn :contactphone)   
          {
              for(SVMXC__Service_Order__c svo6:wolst6)
              {
                  if(svo6.SVMXC__Contact__c == cn.Id)
                  {
                       if(cn.MobilePhone != null )
                        {
                            svo6.Contact_Phone__c = cn.MobilePhone;
                        }
                        else if(cn.WorkPhone__c !=null)
                        {
                            svo6.Contact_Phone__c = cn.WorkPhone__c;
                        }
                  }   
              }   
          }  
        
      }
      /**********************************************************************************************************************************/
      // Scenario: 6
      // Update Account Type on Work Order from Account lookup if Account exists
      /***********************************************************************************************************************************/
      if(wolst7.size()>0)
      {
          accntype = [select Type From account Where Id IN :woaccnt];
          for(account ac :accntype)   
          {
              for(SVMXC__Service_Order__c svo7:wolst7)
              {
                  if(svo7.SVMXC__Company__c == ac.Id)
                  {
                      if(ac.Type== 'GSA')
                          svo7.AccountType__c = Label.CLDEC12SRV13;
                      else
                          svo7.AccountType__c = ''; 
                  }   
              }  
        
          }
          
      }
      
      /************************************************************
      // Scenario 9
      // Update Location and Included Services from PM
      /************************************************************/
      if (woList9.size() > 0)
      {
        system.debug('Scenario 9: ' + woList9);
        Map<Id, Id> contractPMMap = new Map<Id, Id>();
        for (SVMXC__PM_Plan__c pm : [SELECT Id, SVMXC__Service_Contract__c FROM SVMXC__PM_Plan__c WHERE Id IN :woPMIdSet])
        {
          contractPMMap.put(pm.Id, pm.SVMXC__Service_Contract__c);
        }
        covProdList = [SELECT Id, SVMXC__Installed_Product__r.SVMXC__Site__c, IncludedService__c, SVMXC__Service_Contract__c FROM SVMXC__Service_Contract_Products__c WHERE SVMXC__Service_Contract__c IN :contractPMMap.values() AND SVMXC__Installed_Product__c IN :woIpIdSet];
        
        for (SVMXC__Service_Order__c wo : woList9)
        {
          for (SVMXC__Service_Contract_Products__c covProd : covProdList)
          {
            if (covProd.SVMXC__Installed_Product__c == wo.SVMXC__Component__c && contractPMMap.get(wo.SVMXC__PM_Plan__c) == covProd.SVMXC__Service_Contract__c)
            {
              wo.SVMXC__Site__c = covProd.SVMXC__Installed_Product__r.SVMXC__Site__c;
              wo.IncludedService__c = covProd.IncludedService__c;
              wolst8.add(wo);
                        woloc.add(wo.SVMXC__Site__c);
            }
          }
        }
      }
       /**********************************************************************************************************************************/
      // Scenario: 7
      // Update Location details on Work Order from Location lookup if location exists
      /***********************************************************************************************************************************/
      if(wolst8.size()>0)
      {
          loc=[select SVMXC__City__c,LocationCountry__r.name,SVMXC__Zip__c,StateProvince__c,StateProvince__r.name,
          AddressLine2__c,SVMXC__Street__c, SiteSpecificComments__c,
          TimeZone__c,SVMXC__Account__r.Name,SVMXC__Longitude__c,SVMXC__Latitude__c From SVMXC__Site__c Where Id IN :woloc];
               
          for(SVMXC__Site__c l :loc )   
          {
              for(SVMXC__Service_Order__c svo8:wolst8)
              {
                  if(svo8.SVMXC__Site__c == l.Id)
                  {
                      svo8.SVMXC__Street__c = l.SVMXC__Street__c;
                      svo8.AddressLine2__c = l.AddressLine2__c;
                      svo8.SVMXC__City__c = l.SVMXC__City__c;
                      svo8.SVMXC__State__c = l.StateProvince__r.name;
                      svo8.SVMXC__Zip__c = l.SVMXC__Zip__c;
                      //svo8.SVMXC__Country__c = l.LocationCountry__c;
                      svo8.SVMXC__Country__c = l.LocationCountry__r.name;
                      svo8.SiteSpecificComments__c = l.SiteSpecificComments__c;
                      svo8.CustomerLocationAccount__c = l.SVMXC__Account__r.Name;
                      svo8.SVMXC__Latitude__c = l.SVMXC__Latitude__c;
                      svo8.SVMXC__Longitude__c = l.SVMXC__Longitude__c;
                      if(svo8.CustomerTimeZone__c == null)
                          svo8.CustomerTimeZone__c = l.TimeZone__c;
                  }   
              }   
          }       
      }
           
       
    /**********************************************************************************************************************************/
    // Scenario: 10
    // On Insert of  Work Order, update Tech Owner Email fields. 
    /***********************************************************************************************************************************/
      
      if(wolst1.size()>0)
        {
            //user1 = new Map<Id,User>([SELECT Id ,Email FROM User WHERE Id in :woOwnerid]);
            //users = [SELECT Id ,Email FROM User WHERE Id in :woOwnerid ];
            //for(User u1:user1.values())   
            for(User u1:[SELECT Id ,Email FROM User WHERE Id in :woOwnerid])
            {
                for(SVMXC__Service_Order__c  wo1 :wolst1)
                {
                    wo1.TechOwnerEmail__c=u1.Email; 
                } 
            }  
          
        } 
      
     /**********************************************************************************************************************************/
    // Scenario: 11
    // On Insert of  Work Order, update ShipToAccount and BillToAccount fields. 
    /***********************************************************************************************************************************/
       
        for(SVMXC__Service_Order__c wo:trigger.New)
        {
             if(wo.SoldToAccount__c !=null)
             {
             wolist.add(wo);
             //AP_WOBeforInsertUpdateClass.Ship_BillToAccountupdate(wolist); // commented for prod Defect# DEF-9154 Hari
             }
        }
        //added for prod defect# DEF-9154 Hari to resolve too many SOQL
        if(wolist!=null && wolist.size()>0)
            AP_WOBeforInsertUpdateClass.Ship_BillToAccountupdate(wolist); 
      
        /**********************************************************************************************************************************/
    // Scenario: 11a
    // Added by: Deepak
    // Date: 05/08/2013
    // Inserting Preferred Technician From Location As Well As Account Location.
/***********************************************************************************************************************************/       
    
    List<SVMXC__Service_Order__c> wopreferTechList  = new List<SVMXC__Service_Order__c>();
    
    for(SVMXC__Service_Order__c wo:trigger.New)
    {
        if(wo.SVMXC__Site__c != null)
        {
            wopreferTechList.add(wo);
        }
      /*  if(wo.SVMXC__Case__c != null)
        {
            caseid.add(wo.SVMXC__Case__c);          
        } */
    }
    if(wopreferTechList != null && wopreferTechList.size()>0)//LocationFromCase
    Ap_WorkOrder.WOPreferedTechnician(wopreferTechList);
    
   
        
      
    /**********************************************************************************************************************************/
    // Scenario: 12
    // Added by: Ramu Veligeti
    // Date: 16/07/2013
    // On Insert of  Work Order, Create Work Order Group. Update Work Order Group lookup with the new Work Order Group (Incase of Parent WO).
    // If the WO is a Child WO then Update WO with the parent's WOGroup value. Also maintain the status of the WOGroup object based on WO Status.
    /***********************************************************************************************************************************/
        ServiceMaxWOG.CreateWOG(trigger.New);
    
    /**********************************************************************************************************************************/
    // Scenario: 13
    // Added by: Ramu Veligeti
    // Date: 30/07/2013
    // Consolidating Certifications (Habilitation Requirements) on the Work Order.
    /***********************************************************************************************************************************/
        Set<Id> locId = new Set<Id>();
        Set<String> BU = new Set<String>();
        Set<String> CN = new Set<String>();
        List<SVMXC__Service_Order__c> wolst = new List<SVMXC__Service_Order__c>();
        for(SVMXC__Service_Order__c wo: trigger.new)
        {
            if(wo.SVMXC__Site__c!=null && wo.SVMXC__Country__c !=null)
            {
                locId.add(wo.SVMXC__Site__c);
                //BU.add(wo.Service_Business_Unit__c);
                CN.add(wo.SVMXC__Country__c);
                wolst.add(wo);
            }
        }
        
        //if(locId.size()>0 && BU.size()>0) ServiceMaxUpdateSkillsOnWO.HabilitationRequirement(locId,BU,wolst);
        if(locId.size()>0 && CN.size()>0) ServiceMaxUpdateSkillsOnWO.HabilitationRequirement(locId,CN,wolst);
        

    
    
   /**********************************************************************************************************************************/
    // Scenario: 15
    // Update Service Contract Details from PM Plan 
    /***********************************************************************************************************************************/
   
   
       if(wolst15.size()>0 && Pmplanset1!=null)
          Pmplanlist2= [select id, name,SVMXC__Service_Contract__c,SVMXC__Service_Contract__r.ParentContract__c from SVMXC__PM_Plan__c where id in:Pmplanset1];
          
          for(SVMXC__Service_Order__c svo15:wolst15)
          {
              for(SVMXC__PM_Plan__c pmp:Pmplanlist2)
              {
              
                  svo15.SVMXC__Service_Contract__c=pmp.SVMXC__Service_Contract__c;
                  svo15.ServiceLine__c=pmp.SVMXC__Service_Contract__r.ParentContract__c;
              }
          }


    /**************************************************************
       Added by Bala Candassamy
       Call to the apex class in charge of retrieving and adding the footer to be used by the Service Intervention Report
       **********************************************************/
       AP_WorkOrderHandler.fillInFooter(trigger.new);
   
   
       
       /**************************************************************
       Added by Deepak
       Calling Class and Method WO_OwnerEmailUpdate(Added in April-15)
       **********************************************************/
        if(accid!=null && workOrderList.size()>0 && accid.size()>0){
            Ap_WorkOrder.WO_OwnerEmailUpdate(workOrderList,accid);
        
        }
         /**************************************************************
        Added by Deepak
       to update Service Contract from Service Line (Added in April-15)
       **********************************************************/
        if(workOList.size()>0 ){
            Ap_WorkOrder.SerContFieldUpdate(workOList);
        }
        
      /**************************************************************
    * @author Adrian MODOLEA (adrian.modolea@servicemax.com)
    * @date 03/02/2016 (dd/mm/yyyy)
    * @description Apex class for automatically calculate work order's Primary Territory using Territory Coverage definition
    * @NOTE THIS FUNCTION REPLACES THE STANDARD SERVICE MAX FUNCTIONALITY !
      !!! Please ensure the ServiceMax setting SET049 is set to FALSE !!!
    **********************************************************/
    if(System.Label.SVMX_BypassTerritoryMatchRules.toLowerCase() == 'false')
    {
      System.debug('### AMO  ServiceMaxWOTerritoryAssignment on WorkOrderBeforeInsert');
      List<SVMXC__Service_Order__c> workOrders = new List<SVMXC__Service_Order__c>(); 
      
      for(SVMXC__Service_Order__c workOrder : Trigger.new)
      {  
        if(workOrder.SVMXEligibleforTerritoryRouting__c)
        {
          Boolean toAddWo = false;	
          System.debug('### AMO WO eligible for Territory Assignment');
          for(String fieldType : ServiceMaxWOTerritoryAssignment.attributesMap.keySet())
          {
          	String fieldName = ServiceMaxWOTerritoryAssignment.attributesMap.get(fieldType);
          	String fieldValue = (String) workOrder.get(fieldName);
            System.debug('### AMO fieldName ' + fieldName);
            System.debug('### AMO fieldType ' + fieldType);
            System.debug('### AMO fieldValue ' + fieldValue);          	
            if(fieldValue != null)
            {
              toAddWo = true;
              ServiceMaxWOTerritoryAssignment.fieldTypes.add(fieldType);
              ServiceMaxWOTerritoryAssignment.fieldValues.add(fieldValue);
            }  
          }
          if(toAddWo)
          {
              System.debug('### AMO WO added for Territory Assignment');
              workOrders.add(workOrder);
          }
        }
      }   
      
      System.debug('### AMO ' + workOrders.size() + ' WOs for Territory Assignment'); 
      
      if(workOrders.size() > 0)  
        ServiceMaxWOTerritoryAssignment.assignTerritory(workOrders);
      
    }//bypass
    }      
     
}