trigger SVMXC_AfterInsertUpdateDeleteTimesheet on SVMXC_Timesheet__c (after delete, after insert, after undelete, 
after update) {

	if (Trigger.isUpdate)
		SVMXC_TimesheetAfter.submitTimesheetForApproval(Trigger.new, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate);
}