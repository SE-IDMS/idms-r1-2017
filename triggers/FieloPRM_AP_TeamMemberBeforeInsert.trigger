trigger FieloPRM_AP_TeamMemberBeforeInsert on FieloCH__TeamMember__c (before insert) {
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_TeamMemberBeforeInsert')){
        FieloPRM_TeamMemberTrigger.validateTeamMember();
    }

}