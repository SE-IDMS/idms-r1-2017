/*
OCT 15 RELEASE
Divya M
BR-2496
*/
trigger CRAfterDelete on CommercialReference__c (after delete) {
    if(Utils_SDF_Methodology.canTrigger('CRAfterDelete')){   
        system.debug('Entered the trigger Affected Product after Insert');
        AP_AffectedProducts.updateProductLine(trigger.old);
    }
}