/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 02-Oct-2012
    Description     : Connect Entity KPI Contributro KPI After Insert event trigger.
*/

trigger EntityKPIContributorAfterInsert on Entity_KPI_Contributors__c (after insert) {

System.Debug('****** EntityKPIContributorAfterInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('EntityKPIContributorAfterInsert'))
    {
     ConnectEntityKPIContributorTriggers.EntityKPIContributorAfterInsert(Trigger.new);   
        
    }
 System.Debug('****** EntityKPIContributorAfterInsert Trigger End ****'); 
 }