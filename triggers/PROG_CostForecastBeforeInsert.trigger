/*
    Author          : Srikant Joshi (Schneider Electric)
    Date Created    : 11/07/2012
    Description     : Program Costs Forecasts Beforeupdate event trigger.
*/
trigger PROG_CostForecastBeforeInsert on ProgramCostsForecasts__c (before Insert) {

    if(Utils_SDF_Methodology.canTrigger('ProgramCostForecastTriggerBeforeInsert'))
    {
        PROG_CostForecastTriggers.ProgCostForecastbeforeInsert(Trigger.new);
    }

}