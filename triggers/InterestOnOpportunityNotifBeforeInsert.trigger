/*
@Author: Nabil ZEGHACHE
Created Date: 2016-06-16
Description: This trigger fires on Interest on Opportunity Notification on Before Insert
**********
Scenarios:
**********
1.  Update the amount of the interest

Date Modified       Modified By         Comments
------------------------------------------------------------------------

*/
trigger InterestOnOpportunityNotifBeforeInsert on InterestOnOpportunityNotif__c (before insert) {
    System.debug('#### Starting InterestOnOpportunityNotifBeforeUpdate trigger');
    if(Utils_SDF_Methodology.canTrigger('SRV_IntOnOppNotif_BI')) {
        List<InterestOnOpportunityNotif__c> oppNotifToBeProcessed = new List<InterestOnOpportunityNotif__c>();
        for (InterestOnOpportunityNotif__c newOppNotif : Trigger.new) {
            if (
                newOppNotif.Leading_Business_BU__c != null || newOppNotif.Type__c != null || newOppNotif.Interest__c != null
            ) {
                oppNotifToBeProcessed.add(newOppNotif);
            }
        }
        if (oppNotifToBeProcessed != null && oppNotifToBeProcessed.size() > 0) {
            AP_InterestOnOpportunityNotif.updateInterestAmount(oppNotifToBeProcessed);
        }       
    }
}