/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 07-Feb-2013
    Description     : Connect Country Cascading Milestones Before Insert event trigger.
*/

trigger ConnectCountryCascadingMilestoneBeforeInsert on Country_Cascading_Milestone__c (before insert) {
System.Debug('****** ConnectCountryCascadingMilestoneBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CountryCascadingMilestoneBeforeInsert'))
    {  
       ConnectCountryCascadingMilestoneTriggers.ConnectCountryCascadingMilestoneBeforeInsertUpdate(Trigger.new);
        if (Trigger.new[0].ValidateScopeonInsertUpdate__c == true)        
             Trigger.new[0].Name.AddError(Label.ConnectCountryInsertandUpdate);
        ConnectCountryCascadingMilestoneTriggers.ConnectCountryCMBeforeInsertUpdate(Trigger.new);
               
        
        
 }
 System.Debug('****** ConnectCountryCascadingMilestoneBeforeInsert Trigger End ****'); 

}