/*
    Author        : Christian Tran (ATS)
    Date Created  : 12/04/2011
    Description   : Event before Insert Trigger
*/

trigger EventBeforeInsert on Event (before insert) {
    if(Utils_SDF_Methodology.canTrigger('EVT_BEFOREINSERT')){
        System.debug('************** Log3 >> '+Limits.getQueries());
        List<Event> lstEvents = new List<Event>();
        set<id> setAccIds = new set<id>();
        List<Event> eventsRelTo = new List<Event>();
        for(Event event : trigger.new)
        {
             String srtWhoId = event.whoId;
             if(event.StartDateTime!=null)
                event.TECHMonthActivity__c = event.StartDateTime.month();
            
             String strWhatId = event.WhatId; 
             if(strWhatId != null && strWhatId.startsWith('001'))
             {
                 setAccIds.add(event.WhatId);
             }
             if(strWhatId != null && strWhatId.startsWith('001') && event.Location == null )
            {
                lstEvents.add(event);
            } 
            /* Modified for BR-5608 */
            if(event.whoId!=null && event.whatId==null && srtWhoId.startsWith(Schema.SObjectType.Contact.getKeyPrefix()) && event.RecordTypeId==Label.CLOCT14SLS22)
            {
                eventsRelTo.add(event);  
            }
            /* End of Modification for BR-5608 */
        }
        
        if(setAccIds.size() > 0)
        {
              set<id> setPerAccs = new set<id>();
              setPerAccs = AP25_Task.getPersonAccountIds(setAccIds);
              
              if(setPerAccs != null)
                  for(Event ev:Trigger.new)
                  {
                        if(setPerAccs.contains(ev.whatId))
                            ev.addError(System.Label.CLDEC12AC13);
                  }
        }
        /* Modified for BR-5608 */
        if(!(eventsRelTo.isEmpty()))
        {
            AP_EventRecordUpdate.populateRelatedTo(eventsRelTo);
        }
        /* End of Modification for BR-5608 */
        AP_EventRecordUpdate.updateEventLocation(lstEvents);
        
    }
}