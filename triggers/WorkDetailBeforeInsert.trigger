/**
* @author: Nabil ZEGHACHE
* Date Created (YYYY-MM-DD): 2016-08-30
* Test class: 
* Description: This is the beforeInsert trigger of the Work Detail object
* Screnarios:
* 1. Update the Technician/Equipment Field  with the logged in User when empty
* 2. For Estimate Work Details: set the Billable flag to False for non-billable WD for Estimate + calculate the end date/time based on start date/time and duration.
* 3. Inserting new part order line whenever workdetail is inserting and updating parts order line into work detail
*
* -----------------------------------------------------------------
*                     MODIFICATION HISTORY
* -----------------------------------------------------------------
* Modified By: Authors Name
* Modified Date: Date
* Description: Brief Description of Change + BR/Hotfix
* -----------------------------------------------------------------
*/
trigger WorkDetailBeforeInsert on SVMXC__Service_Order_Line__c (before insert) {
    System.debug(Logginglevel.INFO, '#### WorkDetailBeforeInsert start: '+trigger.new);
    if(Utils_SDF_Methodology.canTrigger('SRV05')) {
      
    Set<Id> woIdSet = new Set<Id>();
    // This set will contain all the WO IDs related to the WDs of the trigger
    Set<Id> allWOIDSet = new Set<Id>();
    // This map will contain the set of associated WO IDs as keys and the associated WOs as values. It is meant to be used one per call to the trigger to avoid multiple SOQL queries
    Map<Id, SVMXC__Service_Order__c> woMap;
    Set<Id> ipIdSet = new Set<Id>();
    Set<Id> partset1 = new Set<Id>();
    set<string> rmaset= new set<string>();
    set<string> rmaset1= new set<string>();
    String RMA_RT = System.Label.CLJUL15SRV03;
    String Parts_RT = System.Label.CLJUL15SRV04;
    String ServicedProduct_RT = System.Label.CLQ316SRV02; //Added by VISHNU C for Q3 2016 Release BR-9850 08/08/2016
    String Actuals_RT = Label.CLSSRV_WDD_RecordType;
    Set<String> nameset = new Set<String>();
    List<SVMXC__Service_Order_Line__c> wodlist = new List<SVMXC__Service_Order_Line__c>();
    List<SVMXC__Service_Order_Line__c> detailList = new List<SVMXC__Service_Order_Line__c>();
    //RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'SVMXC__Service_Order_Line__c' AND Name = 'Products Serviced' LIMIT 1];
    //string rt = System.Label.CLJUL16SRV01; // added by suhail for fixing too many SOQL queries error
    //RecordType rts = [SELECT Id FROM RecordType WHERE SobjectType = 'SVMXC__Service_Order_Line__c' AND Name = 'Labor' LIMIT 1];//added by suhail june release 2016 
    Set<id> fsrset = new Set<id>();
    Set<id> woidset1= new Set<id>();
    Map<id,Set<String>> woidPONameMap = new Map<id,Set<String>>();
    List <SVMXC__RMA_Shipment_Order__c> PartsOrderList= new List <SVMXC__RMA_Shipment_Order__c>();
    List<SVMXC__Service_Order_Line__c> toProcess = new List<SVMXC__Service_Order_Line__c>();
    Map<id,list<SVMXC__Service_Order_Line__c>> WoIdWDRecMap = new map<id,list<SVMXC__Service_Order_Line__c>>(); //Added by VISHNU C for Q3 2016 Release BR-9850 08/08/2016
    String MIPinSplit = '';
    string OriginalWDNumber = '';
    
    //Variablees related to Scenario 1 
    List<SVMXC__Service_Order__c> wo =  new List<SVMXC__Service_Order__c>();
    List<SVMXC__Service_Group_Members__c> tech =  new List<SVMXC__Service_Group_Members__c>();
    List<SVMXC__Service_Order_Line__c> wdlst = new List<SVMXC__Service_Order_Line__c>();
    List<SVMXC__Service_Order_Line__c> wdList2 = new List<SVMXC__Service_Order_Line__c>(); // Will be used to store the WD with RecordType Actual in order to calculate the start and end date/time in WO timezone
    Set<Id> woidToProcess = new Set<Id>();
    List<SVMXC__Service_Order_Line__c> wdlistTobeProcess =  new List<SVMXC__Service_Order_Line__c>();
    //Added for May 2015 Release bFS
    List<SVMXC__Service_Order_Line__c> woglist= new list<SVMXC__Service_Order_Line__c >();  
    
	for (SVMXC__Service_Order_Line__c detail : trigger.new) {
    
    	System.debug('#### Current workDetail: '+detail);
          
          // Put all the WO Ids in a set in order to make one single query to retrieve the related Work Orders
          // and avoid to have each method making its own query. This is giving 101 SOQL query error.
          allWOIDSet.add(detail.SVMXC__Service_Order__c);
          
      if (detail.RecordTypeId == ServicedProduct_RT && detail.SVMXC__Serial_Number__c != null)
          {
              woIdSet.add(detail.SVMXC__Service_Order__c);
              ipIdSet.add(detail.SVMXC__Serial_Number__c);
              detailList.add(detail);
          }
          if(detail.SVMXC__Group_Member__c !=null){
              fsrset.add(detail.SVMXC__Group_Member__c);
          }
        

            if(detail.RMANumber__c!=null && detail.recordtypeid==Parts_RT) 
            {
               rmaset1.add(detail.RMANumber__c);
               if(detail.SVMXC__Service_Order__c != null)
               {
                   woidset1.add(detail.SVMXC__Service_Order__c);
                   toProcess.add(detail);
               }
           
            }//End
            
            //Added by VISHNU C for BR-9850 Q3 2016 Release
            System.debug('The Split From WD Number is'+detail.SplitFromWDBORef__c+ 'Record Type is'+detail.recordtypeid);
            System.debug('The Split Flag is' +detail.IsSplitFlag__c);
            System.debug('The WO IP is'+detail.WO_Main_Installed_Product__c+ 'WD IP is'+detail.SVMXC__Serial_Number__c);
            if( detail.IsSplitFlag__c == true  && detail.recordtypeid == ServicedProduct_RT && detail.WO_Main_Installed_Product__c == detail.SVMXC__Serial_Number__c && detail.SVMXC__Serial_Number__c!= null  )
            {
                System.debug('Went Inside IF loop for Split Validation');
                MIPinSplit = detail.Tech_IPName__c;
                OriginalWDNumber = detail.Tech_OrginalWDNumber__c;
                //detail.addError('Installed product ' +detail.Tech_IPName__c+ ' is the Main IP of the original Work Order and cannot be moved to the new Work Order after the split. Please remove it from the Serviced Installed product line.');
            
            }
            
            //Added below line for checking MIP Flag condition.
            if( detail.IsSplitFlag__c == true  && detail.recordtypeid == ServicedProduct_RT )
            {
                if(WoIdWDRecMap.containskey(detail.SVMXC__Service_Order__c))
                {
                    WoIdWDRecMap.get(detail.SVMXC__Service_Order__c).add(detail);
                }
                else
                {
                    WoIdWDRecMap.put(detail.SVMXC__Service_Order__c,new list<SVMXC__Service_Order_Line__c>{detail});
                }
                
                
            }
            System.debug('The value of WoIdWDRecMap' +WoIdWDRecMap);
            
            // Condition for scenario 1
      if(detail.SVMXC__Group_Member__c == null) { // Only consider Work Details where the technician is empty
          wdlst.add(detail);
      }
      
      //Condition scenario 2        
      // Filter on Estimate Record Type
      if(detail.RecordTypeId == Label.CLAPR15SRV59 && detail.SVMXC__Service_Order__c != null){
          woidToProcess.add(detail.SVMXC__Service_Order__c);
          wdlistTobeProcess.add(detail);       
      }
      
      
      //Added for May 2015 Release bFS
      //Condition scenario 3
      if(detail.Parts_Order_Line_RMA__c==null && detail.RMANumber__c!=null && detail.ReturnedQuantity__c!=null){
        woglist.add(detail);
        rmaset.add(detail.RMANumber__c);     
      }
      
      // Condition to calculate the start/end date/time = Record type is Actual
      System.debug('#### Current WD record type: '+detail.RecordTypeId);
      System.debug('##### Actual WD record type: '+Actuals_RT);
      if (detail.RecordTypeId == Actuals_RT) {
        wdList2.add(detail);
      }
        
    }
    
    /*
    *
    * Start the processing of Work Details
    *
    */

    // Make one single query to retrieve the associated Work Orders and pass them as parameters when needed
    System.debug('#### Current workDetail\'s related Work Orders: '+allWOIDSet);
    woMap = AP_WorkDetails.getWOMapForIDSet(allWOIDSet);
    System.debug('#### Work Orders Map: '+woMap);
    
    
    // Clear seconds from WD
    AP_WorkDetails.clearSecondsFromStartAndEndTime(trigger.new);
    ServiceMaxTimesheetUtils.checkForWorkDetailOverlap(trigger.new, trigger.oldMap);
    
    /**
    * Scenario: 1
    * Update the Technician/Equipment Field  with the logged in User or the Primary FSR if the logged in User is not an FSR if Technician/Equipment Field == null
    */

    if (wdlst != null && !wdlst.isEmpty()) {
      AP_WorkDetails.setPrimaryFSR(woMap, wdlst);
    }
    
    AP_WorkDetails.setFSRTimesFromCustomerTimes(wdList2);
    AP_WorkDetails.setDateTimeInCustomerTimeZone(wdList2, woMap);
    AP_WorkDetails.setBillableFlagFromWOAndIsAdminFlag(trigger.new, woMap);
    

    if(wodlist!= null && wodlist.size()>0 && partset1!=null && rmaset!=null){
      AP_WorkDetails.partsorderlineupdate(wodlist,partset1,rmaset);
    }
    
        //Added by VISHNU C for BR-9850 Q3 2016 Release
        if (WoIdWDRecMap.size() > 0 && !(WoIdWDRecMap.isempty()) )
        {
            List<SVMXC__Service_Order_Line__c> WdSplitRec = new List<SVMXC__Service_Order_Line__c>();
            List<SVMXC__Service_Order_Line__c> WdSplitRecMIPFlagT = new List<SVMXC__Service_Order_Line__c>();
            List<SVMXC__Service_Order_Line__c> WdSplitRecMIPFlagF = new List<SVMXC__Service_Order_Line__c>();
            
            for ( Id wdId : WoIdWDRecMap.keyset() )
            {
                
                WdSplitRec = WoIdWDRecMap.get(wdId);
                for (SVMXC__Service_Order_Line__c wdrec : WdSplitRec)
                {
                    //WdSplitRec.add(wdrec);
                    if (wdrec.MIP_Flag__c ==  true)
                        WdSplitRecMIPFlagT.add(wdrec);
                    if (wdrec.MIP_Flag__c ==  false)
                        WdSplitRecMIPFlagF.add(wdrec);
                    
                }
                system.debug('MIPinSplit is ++' +MIPinSplit);
                for (SVMXC__Service_Order_Line__c wdrec : trigger.new){
                 //for (SVMXC__Service_Order_Line__c wdrec : WdSplitRec)
                    if ( WdSplitRecMIPFlagT.size() > 1 )
                        wdrec.addError('Exactly one Installed product should be selected as a Main IP for the new Work Order.');
                    if ( WdSplitRecMIPFlagF.size() == WdSplitRec.size() )
                        wdrec.addError('Exactly one Installed product should be selected as a Main IP for the new Work Order.');
                    if( MIPinSplit != null && MIPinSplit != '' )
                        wdrec.addError( 'Installed product '+MIPinSplit+ ' ('+OriginalWDNumber+') is the Main IP of the original Work Order and cannot be moved to the new Work Order after the split. Please remove it from the Serviced Installed product line.');
                    break;
                }
                
            }
        
        }
        //Changes by VISHNU C Ends here Q3
    
    
        if(woidset1 != null && woidset1.size()>0)
                     PartsOrderList =  [select id, recordtypeid,name,SVMXC__Service_Order__c,SVMXC__Order_Status__c from SVMXC__RMA_Shipment_Order__c where SVMXC__Order_Status__c='Open'  and recordtypeid =:RMA_RT and SVMXC__Service_Order__c in:woidset1];
            if(PartsOrderList!=null && PartsOrderList.size()>0)
            { 
                for(SVMXC__RMA_Shipment_Order__c po:PartsOrderList)
                {
            
                    if(woidPONameMap.containskey(po.SVMXC__Service_Order__c ))
                    {
                        woidPONameMap.get(po.SVMXC__Service_Order__c ).add(po.name);
                    }
                    else
                    {
                        //Set<String> nameset = new Set<String>();
                        nameset.add(po.name);
                        woidPONameMap.put(po.SVMXC__Service_Order__c ,nameset);
                    }
                }
        
                for (SVMXC__Service_Order_Line__c detail :toProcess)
                {
                    //Set<String> 
                    nameset = woidPONameMap.get(detail.SVMXC__Service_Order__c);
                   if(nameset!=null)
                    if(nameset.contains(detail.RMANumber__c))
                    {
                        // nothing to do
                    }
                    else
                    {
                        detail.adderror ('Work Detail '+ detail.Name+' references an invalid RMA#'+detail.RMANumber__c+' Please request a new RMA#');
                    }
                }
        
        }
        else
        {
            
            for (SVMXC__Service_Order_Line__c detail :toProcess)
                {
                    detail.adderror ('Work Detail '+ detail.Name+' references an invalid RMA#'+detail.RMANumber__c+' Please request a new RMA#');
                }
            
        }
            
    //End:Def-7908
    
    
    // May 2015 Release Stated
    
        if(fsrset != null && fsrset.size()>0){
            Map<id, SVMXC__Service_Group_Members__c> sgmap = new Map<id,SVMXC__Service_Group_Members__c>();
            List<SVMXC__Service_Group_Members__c> sglist=[select id ,Level__c from SVMXC__Service_Group_Members__c where id in :fsrset ];
            if(sglist!= null && sglist.size()>0)
            {
                sgmap.putAll(sglist);
            }
            for (SVMXC__Service_Order_Line__c detail : trigger.new)
            {
                if(detail.SVMXC__Group_Member__c !=null && sgmap.containskey(detail.SVMXC__Group_Member__c)){
                    detail.Level_Required__c = sgmap.get(detail.SVMXC__Group_Member__c).Level__c;
                }
                else{
                
                    detail.Level_Required__c = null;
                }
                
            }
            
        }
    
    //End
    
    if (detailList.size() > 0)
    {
        Map<Id, Id> contractIdMap = new Map<Id, Id>();
        if((woIdSet.size()>0)&&!(woIdSet.isempty())){ //added by suhail to fix too many SOQL queries error
        //Map<Id, SVMXC__Service_Order__c> woMap = new Map<Id, SVMXC__Service_Order__c>([SELECT Id, SVMXC__Service_Contract__c, SVMXC__PM_Plan__c, SVMXC__PM_Plan__r.SVMXC__Service_Contract__c FROM SVMXC__Service_Order__c WHERE Id IN :woIdSet]);
        for (SVMXC__Service_Order__c currentWO : woMap.values())
        {
            if (currentWO.SVMXC__Service_Contract__c != null)
            {
                contractIdMap.put(currentWO.Id, currentWO.SVMXC__Service_Contract__c);
            }
            else
            {
                contractIdMap.put(currentWO.Id, currentWO.SVMXC__PM_Plan__r.SVMXC__Service_Contract__c);
            }
        }
        if((contractIdMap.size()>0)&&!(contractIdMap.isempty())&&(ipIdSet.size()>0)&&!(ipIdSet.isempty())){ //added by suhail to fix too many SOQL queries error
            
        List<SVMXC__Service_Contract_Products__c> covProdList = [SELECT Id, SVMXC__Installed_Product__r.SVMXC__Site__c, IncludedService__c, SVMXC__Service_Contract__c FROM SVMXC__Service_Contract_Products__c WHERE SVMXC__Service_Contract__c IN :contractIdMap.values() AND SVMXC__Installed_Product__c IN :ipIdSet];
        
        for (SVMXC__Service_Order_Line__c detail : detailList)
        {
            SVMXC__Service_Order__c aWO = woMap.get(detail.SVMXC__Service_Order__c);
            for (SVMXC__Service_Contract_Products__c covProd : covProdList)
            {
                if (covProd.SVMXC__Service_Contract__c == contractIdMap.get(aWO.Id) && detail.SVMXC__Serial_Number__c == covProd.SVMXC__Installed_Product__c)
                {
                    detail.ProductLocation__c = covProd.SVMXC__Installed_Product__r.SVMXC__Site__c;
                    detail.IncludedService__c = covProd.IncludedService__c;
                }
            }
        }
      
    }
   }
 }   
  
  
/*
    if(wdlst.size()>0)
    {
        tech = [select id, Name ,SVMXC__Salesforce_User__c 
        from SVMXC__Service_Group_Members__c where SVMXC__Salesforce_User__c =:UserInfo.getUserId()]; 
        
        
        Map<Id, Id> TechIdMap = new Map<Id, Id>();
        Map<Id, SVMXC__Service_Group_Members__c> techMap = new Map<Id, SVMXC__Service_Group_Members__c>([select id, Name ,SVMXC__Salesforce_User__c,Level__c 
        from SVMXC__Service_Group_Members__c where SVMXC__Salesforce_User__c =:UserInfo.getUserId()]);
        
        wo = [select id, SVMXC__Group_Member__c,SVMXC__Group_Member__r.Level__c 
        from SVMXC__Service_Order__c where id =:woid]; 
        
        for(SVMXC__Service_Order_Line__c wd:wdlst)   
        {
            //CLAPR15SRV60 == Recordtype for Installed Serviced Product, CLAPR15SRV59== Recordtype for Estimate
            if(wd.RecordTypeId != Label.CLAPR15SRV60 ){
                for(SVMXC__Service_Group_Members__c t:techMap.values())   
                {
                    if(t.id != null && wd.SVMXC__Group_Member__c == null )
                    {
                        wd.SVMXC__Group_Member__c = t.id;
                        wd.Level_Required__c = t.Level__c;
                    }   
                }
            }
            if(techMap.IsEmpty())
            {   
                for(SVMXC__Service_Order__c w:wo)
                {
                    if(w.SVMXC__Group_Member__c != null && wd.SVMXC__Group_Member__c == null)
                    {
                        wd.SVMXC__Group_Member__c = w.SVMXC__Group_Member__c;
                        wd.Level_Required__c=w.SVMXC__Group_Member__r.Level__c;
                    }
                }   
            } 
            
        }
    }
*/         
    
      if(woidToProcess != null && wdlistTobeProcess.size()>0){
          AP_WorkDetails.UpdateWD(woMap,wdlistTobeProcess);
      }
     /**********************************************************************************************************************************/
      // Scenario:3.Inserting new part order line whenever workdetail is insering and updating partorderline into workdetail
      //Added for May 2015 Release bFS
     /**********************************************************************************************************************/
    if(woglist!=null && woglist.size()>0){
      AP_WorkDetails.Createpartorderline(rmaset,woglist);
    }
    
    }
    System.debug(Logginglevel.INFO, '#### WorkDetailBeforeInsert end');
}