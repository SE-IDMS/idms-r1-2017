/*
    Author          : Nicolas PALITZNE (Accenture)
    Date Created    : 01/12/2011
    Description     : Return Item Before Delete Trigger
    Modified on     : 23-Dec-2012 
    Modified by     : Sukumar Salla
*/

trigger ReturnItemAfterUpdate on RMA_Product__c (after update)
{
  system.debug('================== ReturnItemAfterUpdate - start ===========');
  /*
    if(Utils_SDF_Methodology.canTrigger('AP34'))
    {
        AP34_RMAMailMerge.updateReturnedProducts(trigger.new);
    }
  */  
  
  /*
     if(Utils_SDF_Methodology.canTrigger('AP_RI_RMAMailMerge'))
    {
       AP_RI_RMAMailMerge.UpdateRITechFieldsOnRMA(trigger.new);
    }

    System.Debug('****** ReturnItemBeforeUpdate Trigger ends ****'); 
    */
     if(Utils_SDF_Methodology.canTrigger('AP_RR_FieldUpdate'))
    {
    List<RMA_Product__c> RIs = new List<RMA_Product__c>();
    List<RMA_Product__c> lstRI = new List<RMA_Product__c>();
    Set<Id> RRIds = new Set<Id>();
    
   
    for(RMA_Product__c RI: trigger.new)
    {
    
        if((Trigger.oldMap.get(RI.id).SentBacktoCustomer__c != Trigger.newMap.get(RI.id).SentBacktoCustomer__c) && Trigger.newMap.get(RI.id).SentBacktoCustomer__c!=null){
                RIs.add(RI);
                System.debug('##RI##'+ RIs);
                
            }
        if((Trigger.oldMap.get(RI.id).ApprovalStatus__c!= Trigger.newMap.get(RI.id).ApprovalStatus__c) && (Trigger.newMap.get(RI.id).ApprovalStatus__c == Label.CLMAY13RR07 || Trigger.newMap.get(RI.id).ApprovalStatus__c == Label.CLMAY13RR14|| Trigger.newMap.get(RI.id).ApprovalStatus__c == Label.CLMAY13RR08 || Trigger.newMap.get(RI.id).ApprovalStatus__c == label.CLOCT13RR57 )){
                RRIds.add(RI.RMA__c);     
         }  
         //Oct 15 rel -- Divya M --> BR -7721
         if(RI.ShippedFromCustomer__c != null && RI.ShippedFromCustomer__c != Trigger.oldMap.get(RI.id).ShippedFromCustomer__c){
            lstRI.add(RI);
         }
          //Ends Oct 15 rel   
            
    }
    
    
      if(RIs.Size()>0)
      {
      //  AP_RI_FieldUpdate.updateCaseOwner(RIs);
       }
   
      system.debug('** RRIds.size() **'+RRIds.size());
      if(RRIds.size()>0)
      {
        AP_RR_FieldUpdate.updateRRStatus(RRIds);
      }
      // Oct 15 Rel -- Divya M --> BR - 7721
      if(lstRI.size()>0){
        AP_RR_FieldUpdate.updateShippedFromCustomer(lstRI);
      }
      // Ends Oct 15 Rel
    }
    
    // APRIL 2014 RELEASE: BR-4296 , Ram Prasad Chilukuri, 03Feb2013
    /*
    if(Utils_SDF_Methodology.canTrigger('AP_TechnicalExpertAssessment'))
     {
     List<RMA_Product__c> ListRI=new List<RMA_Product__c>(); 
      for(RMA_Product__c temRI: trigger.new)
         {
          if(temRI.ApprovalStatus__c!=Label.CLMAY13RR14)
              {
              ListRI.add(temRI);
              }             
         }
        if( ListRI.size()>0)
         {
         AP_TechnicalExpertAssessment.updatingMaterialStatusOnTEX(trigger.new);
         }
     }  
     */
     //  APRIL 2014 RELEASE - END
}