/*
    Author          : Stephen Norbury 
    Date Created    : 06/03/2013
    Description     : Care Team After Insert / Update.
    
    Change History:
    
    BR-2858: Add Team Leader as Team Member
*/

trigger CareTeamAfterInsertUpdate on CustomerCareTeam__c (after insert, after update) 
{
  // Fetch the Teams and Team Leaders
  
  Set <Id>     TeamIds       = new Set <Id>();
  Map <Id, Id> TeamLeaderMap = new Map <Id, Id>();
  
  for (CustomerCareTeam__c team : trigger.new)
  {
    if (team.TeamLeader__c != null)
    {
      TeamIds.add (team.Id);
      
      TeamLeaderMap.put (team.Id, team.TeamLeader__c);
    }
  }
  
  // Now, Fetch the Team Members
  
  List <TeamAgentMapping__c> members = [select Id, CCAgent__c, CCTeam__c, DefaultTeam__c from TeamAgentMapping__c where CCTeam__c in :TeamIds];
  
  Set <String> TeamMemberIds = new Set <String>();
  
  for (TeamAgentMapping__c member : members)
  {
    TeamMemberIds.add (member.CCTeam__c + '-' + member.CCAgent__c);   
  }
  
  // And, is the Team Leader already a Member?
  
  List <TeamAgentMapping__c> newMembers = new List <TeamAgentMapping__c>();
  
  for (CustomerCareTeam__c team : trigger.new)
  {
    if (team.TeamLeader__c != null)
    {
      // Are there any Members?
      
      if (TeamMemberIds != null && TeamMemberIds.size() != 0)
      {
        // Already a Member?
        
        if (! TeamMemberIds.contains(team.Id + '-' + team.TeamLeader__c))
        {
          // Add new Member
          
          TeamAgentMapping__c TeamMember = new TeamAgentMapping__c();
          
          TeamMember.CCTeam__c  = team.Id;
          TeamMember.CCAgent__c = team.TeamLeader__c;
          
          newMembers.add (TeamMember);
        }
      }
      else
      {
        // Add new Member
        
        TeamAgentMapping__c TeamMember = new TeamAgentMapping__c();
          
        TeamMember.CCTeam__c  = team.Id;
        TeamMember.CCAgent__c = team.TeamLeader__c;
          
        newMembers.add (TeamMember);
      }
    }
  }
  
  // Finally, create the new Team Members
  
  if (newMembers != null && newMembers.size() != 0)
  {
    insert newMembers;
  }
  
  // End
}