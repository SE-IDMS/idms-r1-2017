/*
OCT 15 RELEASE
BR-7370
Divya M 
*/
trigger TEXAfterInsert on TEX__c (After insert) {
    if(Utils_SDF_Methodology.canTrigger('TEX_AI')){
        list<TEX__c> texlst = new list<TEX__C>();
        system.debug('tex values===>'+trigger.new);
        for(TEX__c tex: trigger.new){
            system.debug('value of LOB-->'+tex.LineofBusiness__c+'=== Expert Center===>'+tex.ExpertCenter__c);
            if((tex.LineofBusiness__c != null ) && (tex.ExpertCenter__c != null)){
                texlst.add(tex);
            }
        }
        if(texlst.size()>0){
        AP_TechnicalExpertAssessment.insertTEXStakeholder(texlst,'After Insert');
        }
    }
}