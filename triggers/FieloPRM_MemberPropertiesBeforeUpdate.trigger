/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: Trigger
********************************************************************/

trigger FieloPRM_MemberPropertiesBeforeUpdate on MemberExternalProperties__c (before update) {
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberPropertiesBeforeUpdate')){
        
        if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberPropertiesBeforeUpdateSearchMember')){
            FieloPRM_AP_MemberPropertiesTriggers.searchMember();
        }

        System.debug('RAHL isRunning ' + FieloPRM_AP_MemberPropertiesTriggers.isRunning);
        
        if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberPropertiesBeforeUpdateCheckStatus')){
            FieloPRM_AP_MemberPropertiesTriggers.checkMemberPropertiesStatus(trigger.New);
        }
        
        if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberPropertiesBeforeUpdateProcess') && !FieloPRM_AP_MemberPropertiesTriggers.isRunning){
            FieloPRM_Batch_MemberPropProcess.runSync(trigger.new);
        }
    }
}