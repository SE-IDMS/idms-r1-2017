/*
   Author: Amitava Dutta
   Date Of Creation: 08/02/2011
   Description : Objective Settings Before Update event trigger
*/

trigger ObjectivesSettingsBeforeUpdate on ObjectivesSettings__c (before Update) 
{
  System.Debug('****** ObjectivesSettingsBeforeUpdate Trigger Start ****');
  
  if(Utils_SDF_Methodology.canTrigger('AP03'))
  {    
      List<ObjectivesSettings__c> objectiveSettings = new List<ObjectivesSettings__c>();
      List<ObjectivesSettings__c> objectiveSettingsApprover = new List<ObjectivesSettings__c>();
      List<ObjectivesSettings__c> objSettingsUpdatedAfterApproval = new List<ObjectivesSettings__c>();
      for(integer i = 0;i<Trigger.New.Size();i++) 
      {
          if(Trigger.New[i].OwnerID != Trigger.Old[i].OwnerID) {
              objectiveSettings.add(Trigger.New[i]);
          }
    
          if(Trigger.New[i].ObjectiveLeadersManagerName__c == 'N/A') 
          {
              objectiveSettingsApprover.add(Trigger.New[i]);
          }
          
          
          if(Trigger.New[i].ObjectivesModifiedAfterApproval__c == False && Trigger.New[i].Tech_ReportIsApproved__c == Label.CL00074.toUpperCase()) {
              objSettingsUpdatedAfterApproval.add(Trigger.New[i]);
          }
          
      }
      if(objectiveSettings.size() !=0) {    
          AP03_ObjectiveSettingsTrigger.UpdateObjectiveLeaderOnObjectives(objectiveSettings);
                      
      }
      if(objectiveSettingsApprover.size() !=0)
      {
          AP03_ObjectiveSettingsTrigger.UpdateObjectiveLeaderApprover(objectiveSettingsApprover);
      }
      
      if(objSettingsUpdatedAfterApproval.size() !=0) {    
          AP03_ObjectiveSettingsTrigger.recordUpdationAfterApproval(objSettingsUpdatedAfterApproval);
      }
  }
  System.Debug('****** ObjectivesSettingsBeforeUpdate Trigger End****');    
}