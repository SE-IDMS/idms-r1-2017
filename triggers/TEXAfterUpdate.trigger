Trigger TEXAfterUpdate on TEX__c (after update) {

/*
    Author          : Sukumar Salal (GD Team)
    Date Created    : 18-JUNE-2013
    Description     : When Confirm CheckBox is ticked, Reject all Return Requests on related TEX.
    Modified on     :  
    Modified by     : 
*/

    System.debug(' ****** Trigger TEXAfterUpdate - Begin ***** ');
    string strEC ;
    List<TEX__c> lstTEXs = new List<TEX__c>();
    list<TEX__c> texlst = new list<TEX__C>();
    if(Utils_SDF_Methodology.canTrigger('AP_TEXConfirmCheck'))
        {
            for(TEX__c TEX: trigger.new)
             {
                if((TEX.AssessmentType__c==Label.CLOCT13RR20 || TEX.AssessmentType__c==Label.CLOCT13RR21) && (TEX.Confirm__c==True))
             
                   lstTEXs.add(TEX);      
                //OCT 15 RELEASE -- BR-7370 --Divya M 
                strEC  = Trigger.oldMap.get(Tex.id).ExpertCenter__c;
               if((strEC == '' || strEC == null) && (Tex.LineofBusiness__c != null || Tex.LineofBusiness__c!= '') && Trigger.oldMap.get(Tex.id).ExpertCenter__c != Trigger.newMap.get(Tex.id).ExpertCenter__c  && (Trigger.newMap.get(Tex.id).ExpertCenter__c != null || Trigger.newMap.get(Tex.id).ExpertCenter__c != '')){//&& Trigger.oldMap.get(Tex.id).ExpertCenter__c != Trigger.newMap.get(Tex.id).ExpertCenter__c 
                //if((Trigger.oldMap.get(Tex.id).ExpertCenter__c == null) && (Tex.LineofBusiness__c != null || Tex.LineofBusiness__c!= '') && (Trigger.newMap.get(Tex.id).ExpertCenter__c != null || Trigger.newMap.get(Tex.id).ExpertCenter__c != '')){//&& Trigger.oldMap.get(Tex.id).ExpertCenter__c != Trigger.newMap.get(Tex.id).ExpertCenter__c 

                    texlst.add(Tex);
                }
                
                //Ends BR-7370
            } 
             System.debug('##List of TEX obj - lstTEXs##'+ lstTEXs);     
        }
    
    If(lstTEXs.size()>0)
    { 
       AP_TechnicalExpertAssessment.RejectReturnRequests(lstTEXs);
    }
    //Oct 15 Release -- BR-7820 -- Divya M    
    //AP_TechnicalExpertAssessment.caseClosing(trigger.new); // Commented Code as per BR-7820

    AP_TechnicalExpertAssessment.updateCase(trigger.new);
    //Ends BR-7820
    //OCT 15 RELEASE -- BR-7370 --Divya M 
    if(texlst.size()>0){
        AP_TechnicalExpertAssessment.insertTEXStakeholder(texlst,'After Update');
    }
    //Ends BR-7370
 // APRIL 2014 RELEASE: BR-4296 Ram Prasad Chilukuri - 03FEB2014
 
     map<string,TEX__c>setTexIds=new map<string,TEX__c>();
     for(TEX__c  Tex: Trigger.new){
       if((Trigger.oldMap.get(Tex.id).Material_Status__c!= Trigger.newMap.get(Tex.id).Material_Status__c &&Trigger.newMap.get(Tex.id).Material_Status__c==Label.CLAPR14I2P12) ||(Trigger.oldMap.get(Tex.id).ReceivedbyExpertCenter__c!=Trigger.newMap.get(Tex.id).ReceivedbyExpertCenter__c)){
                setTexIds.put(String.valueOf(Tex.id).substring(0,15),Tex);    
            }}
          System.debug(' ****      setTexIds'+ setTexIds);
    If(setTexIds.size()>0)
    { 
       AP_TechnicalExpertAssessment.updateRILogisticsStatusStatusRunOnTEX(setTexIds);
    }
 
 
 
    System.debug(' ****** Trigger TEXAfterUpdate - End ***** ');
    
    }