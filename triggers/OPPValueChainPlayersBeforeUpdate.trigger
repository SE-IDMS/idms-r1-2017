/*
    01-Apr-2013 Srinivas Nallapati  PRM Apr13   DEF-1462
*/
trigger OPPValueChainPlayersBeforeUpdate on OPP_ValueChainPlayers__c (before update) {

	if(Utils_SDF_Methodology.canTrigger('AP_OPPValueChainPlayers'))
    {
		AP_OPPValueChainPlayers.checkOpportunityPassedToPartner(trigger.new);
	}
}