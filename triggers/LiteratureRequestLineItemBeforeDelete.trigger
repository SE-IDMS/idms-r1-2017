/*
    Author          : Ramesh Rajasekaran (Schneider Electric)
    Date Created    : 08-November-2012
    Description     : Line Item records cannot be deleted for the processed literature requests.
*/

trigger LiteratureRequestLineItemBeforeDelete on LiteratureRequestLineItem__c (Before Delete) {
  System.Debug('****** LiteratureRequestLineItemBeforeDelete Trigger Start ****');    
    If(Trigger.Old.size()>0){         
            If(Trigger.Old[0].TECH_LTRStatus__c == 'Processed'){                 
                 //CLDEC12CCC08 Trigger.Old[0].addError('Sorry! You cannot delete Line items for the processes Literature requests');
                 Trigger.Old[0].addError(Label.CLDEC12CCC08 );
            }    
    }     
  System.Debug('****** LiteratureRequestLineItemBeforeDelete Trigger End ****');
}