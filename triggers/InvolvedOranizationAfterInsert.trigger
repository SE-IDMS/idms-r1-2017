trigger InvolvedOranizationAfterInsert on InvolvedOrganization__c (After Insert)
{
     system.debug('1*******Before trigger Before');
     if(Utils_SDF_Methodology.canTrigger('AP_IO_AI'))
       {
           if(Trigger.new.size()>0)
           {
            AP_InvolvedOrganization.updateInvolvedOrgonBRE(Trigger.new,'No');  
            AP_InvolvedOrganization.upDateInvolvedOrgIdsOnBRE(Trigger.new);  
            system.debug('2**********After trigger Before');
                                   
           } 
        }
}