/*
Author:Siddharth N
Purpose:
The quota lookup should be populated when a SPA request record is created.
*/
trigger SPARequestBeforeUpdate on SPARequest__c(before update) {
 if(Utils_SDF_Methodology.canTrigger('AP_SPARequestCalculations'))
 {
     List<SPARequest__c> tobeProcessedRequests=new List<SPARequest__c>();     
     List<SPARequest__c> tobeProcessedForaccno=new List<SPARequest__c>();     
     List<SPARequest__c> tobeProcessedForchannellegacynumber=new List<SPARequest__c>();
     List<SPARequest__c> spaApprlReq =new List<SPARequest__c>();
     List<SPARequest__c> tobeProcessedForThresholdDiscount=new List<SPARequest__c>();
     List<SPARequest__c> tobeProcessedForThresholdAmount=new List<SPARequest__c>();
     
     for(SPARequest__c spareq:Trigger.New)
     {     
         if(spareq.SPASalesGroup__c!=null)    
             tobeProcessedRequests.add(spareq);
         if(spareq.SalesPerson__c==null)
             spareq.SalesPerson__c=UserInfo.getUserId(); 
          //Commented if condition as part of October 2014 release since account calculation will now be applied to HK also.                    
         //if(spareq.BackOfficeCustomerNumber__c==null || spareq.BackOfficeSystemID__c=='NL_SAP')
             tobeProcessedForaccno.add(spareq);        
        if(spareq.Channel__c!=null && ((spareq.ChannelLegacyNumber__c==null || spareq.ChannelLegacyNumber__c=='') || (Trigger.NewMap.get(spareq.id).Channel__c!=Trigger.OldMap.get(spareq.id).Channel__c) || (Trigger.NewMap.get(spareq.id).BackOfficeSystemID__c!=Trigger.OldMap.get(spareq.id).BackOfficeSystemID__c)))
             tobeProcessedForchannellegacynumber.add(spareq);        
        /* Modified for BR-4953 */
        if(spareq.TECH_ApprovalStep__c!=null && spareq.TECH_ApprovalStep__c!=Trigger.oldMap.get(spareq.Id).TECH_ApprovalStep__c)
            spaApprlReq.add(spareq);
         //START added for BR-4968 October 2014 release    
        if(Trigger.oldMap.get(spareq.Id).RequestedType__c!=Trigger.newMap.get(spareq.Id).RequestedType__c){
             tobeProcessedForThresholdAmount.add(spareq);
             if(spareq.RequestedType__c!=System.Label.CLOCT14SLS11)
                 tobeProcessedForThresholdDiscount.add(spareq); 
        }
        // END of BR-4968 October 2014 release   
     }
    
    if(!(spaApprlReq.isEmpty()))
        AP_SPARequestCalculations.populateCurrentApprover(spaApprlReq);            
    
    /* End of Modification for BR-4953 */
    
    if(Trigger.New.size()>0)
      AP_SPARequestCalculations.populateSalesGroup(Trigger.New);                
    
    if(tobeProcessedRequests.size()>0)         
        AP_SPARequestCalculations.linkQuotaRecord(tobeProcessedRequests);   
    
    if(tobeProcessedForaccno.size()>0)
        AP_SPARequestCalculations.populateBackofficeAccno(tobeProcessedForaccno);   

    if(tobeProcessedForchannellegacynumber.size()>0)
        AP_SPARequestCalculations.populateChannelLegacyNumber(tobeProcessedForchannellegacynumber);  
        
     /* With the introduction of new Requested Types we are going to introduce functionlity to 
        check for the presence of atleast one record of respective type in the Threshold tables 
        inorder to update SPA having no line Items*/
    // START of BR-4968 October 2014 release 
     if(tobeProcessedForThresholdDiscount.size()>0)
      AP_SPARequestCalculations.checkForThresholdDiscount(tobeProcessedForThresholdDiscount);
      
    if(tobeProcessedForThresholdAmount.size()>0)
      AP_SPARequestCalculations.checkForThresholdAmount(tobeProcessedForThresholdAmount);
    //  END of BR-4968 October 2014 release                         
 }
}