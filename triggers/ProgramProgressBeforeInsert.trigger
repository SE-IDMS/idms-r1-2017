/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 12-Jul-2013
    Description     : IPO Strategic Program Progress Before Insert event trigger.
*/

trigger ProgramProgressBeforeInsert on Program_Progress_Snapshot__c (before insert) {

System.Debug('****** IPOStrategicProgramProgressBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('IPOStrategicProgramBeforeUpdate '))
    {
    IPOProgramProgressTrigger.IPOProgramProgressAfterInsertUpdate(Trigger.new);
    if (Trigger.new[0].ValidateInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.IPOProgramProgressValidate);
    }
System.Debug('****** IPOStrategicProgramProgressBeforeInsert Trigger End ****'); 
}