/*  Modified by : Shruti Karn
    Description : To create a Task for each SME Team Member Added to the Quote Link
    Date Modified : 12 April 2012
*/
trigger SMETeamAfterUpdate on RIT_SMETeam__c (after update)
{
    if(Utils_SDF_Methodology.canTrigger('AP17'))
    {
        System.Debug('****** SMETeamAfterUpdate Trigger Start****');
        Map<id,RIT_SMETeam__c> RITSMETeam = new Map<id,RIT_SMETeam__c>();
       
        for(RIT_SMETeam__c RITSME:Trigger.new)
        {
            if(RITSME.TeamMember__c != trigger.Oldmap.get(RITSME.id).TeamMember__c)
            {
                RITSMETeam.put(RITSME.id,RITSME); 
            }
        }
        
        if(RITSMETeam.size()>0)
        {
            AP17_SharingAccessToSMETeam.updateSMETeamMember(RITSMETeam,Trigger.oldMap);
        }
    
        System.Debug('****** SMETeamAfterUpdate Trigger End****');
    }
    if(Utils_SDF_Methodology.canTrigger('AP51'))
    {
        //Added by Shruti Karn to update tasks for each SME Member
        AP51_CreateTaskforSME.updateTask(trigger.newMap , trigger.Oldmap);
    }
}