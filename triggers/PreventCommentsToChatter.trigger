trigger PreventCommentsToChatter on FeedComment (before Insert) {
    if(Utils_SDF_Methodology.canTrigger('PreventCommentsToChatter'))
    {
        Utils_PreventUsageOfChatter.preventComments(Trigger.New);
    } 
}