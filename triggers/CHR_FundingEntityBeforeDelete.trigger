/*
    Author          : Priyanka Shetty (Schneider Electric)
    Date Created    : 12/04/2013
    Description     : Change request Funding Entity before delete event trigger.
*/
trigger CHR_FundingEntityBeforeDelete on ChangeRequestFundingEntity__c  (After Delete) 
{
    System.Debug('****** CHR_FundingEntityBeforeDelete Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('FundingEntityTriggerbeforeDelete'))
    {
        
        CHR_FundingEntityTriggers.fundingentitybeforeDelete(Trigger.old);

    }
    System.Debug('****** CHR_FundingEntityBeforeDelete Trigger End ****'); 
}