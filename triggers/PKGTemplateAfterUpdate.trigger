trigger PKGTemplateAfterUpdate on PackageTemplate__c (after update) {
    AP_TemplateTriggerUtils.UpdateCase(trigger.newMap, trigger.oldMap);
}