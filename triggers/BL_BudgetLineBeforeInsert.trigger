/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 06/11/2012
    Description     : Budget Line Before Insert event trigger.
*/

trigger BL_BudgetLineBeforeInsert on PRJ_BudgetLine__c (before insert) 
{
    System.Debug('****** BudgetLineBeforeInsert Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('BudgetLineTriggerInsert'))
    {
        System.Debug('Value New: ' + Trigger.new);
        System.Debug('Value NewMap: ' + Trigger.newMap);
        BL_BudgetLineTriggers.budgetLineBeforeInsert(Trigger.new);
    }
    System.Debug('****** BudgetLineBeforeInsert Trigger End ****'); 
}