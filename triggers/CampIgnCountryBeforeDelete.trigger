/*
29-Oct-2012    Srinivas Nallapati    Initial Creation: Dec 12 Mkt Release
*/
trigger CampIgnCountryBeforeDelete on CampaignCountry__c (before delete) {
   
    Map<id,Country__c> mapCountry = new map<id,Country__c>([select name from Country__c limit 1000]);
    
    Set<id> campaignIds = new set<id>();
    map<id,list<String>> mapCamidToCountryNames = new map<id,list<String>>();
    
    
    
    for(CampaignCountry__c  cc : trigger.old)
    {
       
        if(cc.Country__c!= null && cc.Tech_DeletedFromParentTrigger__c == false)
        {    
             campaignIds.add(cc.Campaign__c);
             
            if(mapCamidToCountryNames .containsKey(cc.Campaign__c))
                mapCamidToCountryNames .get(cc.Campaign__c).add(mapCountry.get(cc.Country__c).name);
            else
            {
                mapCamidToCountryNames .put(cc.Campaign__c, new String[]{mapCountry.get(cc.Country__c).name});
            }        
        }    
    }
    
    List<Campaign> lstCampaignToUpdate = new List<Campaign>();
    lstCampaignToUpdate = [select name, TargetedCountries__c from Campaign where id in :campaignIds];
    
    for(Campaign cam : lstCampaignToUpdate)
    {
        if(mapCamidToCountryNames.containsKey(cam.id)  && cam.TargetedCountries__c != null)
        {
            for(String st : mapCamidToCountryNames.get(cam.id))
            {
                cam.TargetedCountries__C = cam.TargetedCountries__C.replaceAll(';'+st+';', ';');
            }
        }
    }
    
    update lstCampaignToUpdate;
   
}