/********************************************************************************************************************
    Modified By : Shruti Karn
    Created Date : 2 July 2012
    Description : For September Release:
                 1. Calls AP_ContractTriggerUtils.findNewContact to check if an Active PAckage is being added to
                    Inactive/Draft Contract
    
********************************************************************************************************************/
trigger PackageBeforeInsert on Package__c (before insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_PackageTriggerUtils'))
    {
        AP_PackageTriggerUtils.checkContractStatus(trigger.new);
       //AP_PackageTriggerUtils.checkDuplicate(trigger.new);
    }
    
}