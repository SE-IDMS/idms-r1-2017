//*********************************************************************************
// Trigger Name     : ProblemTeamMemberAfterUpdate
// Purpose          : Problem  Team Member After Update event trigger
// Created by       : Global Delivery Team
// Date created     : 26th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/

trigger ProblemTeamMemberAfterUpdate on ProblemTeamMember__c (after update) {
    System.Debug('****** ProblemTeamMemberAfterUpdate Trigger Start ****');      
    
    if(Utils_SDF_Methodology.canTrigger('AP1001')){              
        Map<Id,ProblemTeamMember__c> prblmTeamOldMap = new Map<Id,ProblemTeamMember__c>(); 
        Map<Id,ProblemTeamMember__c> prblmTeamNewMap = new Map<Id,ProblemTeamMember__c>(); 
        
        for(ProblemTeamMember__c prblmTeam: Trigger.new){
            if(Trigger.oldMap.get(prblmTeam.id).User__c != Trigger.newMap.get(prblmTeam.id).User__c){
                prblmTeamOldMap.put(prblmTeam.id,Trigger.oldMap.get(prblmTeam.id));
                prblmTeamNewMap.put(prblmTeam.id,Trigger.newMap.get(prblmTeam.id));
            }
        }
        
        if(prblmTeamOldMap.size()>0 && prblmTeamNewMap.size()>0){ 
            AP1001_ProblemTeamMember.updateProblemTeamMemberToProblemShare(prblmTeamOldMap,prblmTeamNewMap); 
        }                  
    }                                               
    System.Debug('****** ProblemTeamMemberAfterUpdate Trigger End ****'); 
}