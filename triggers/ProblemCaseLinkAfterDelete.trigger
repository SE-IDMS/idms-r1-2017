// Created by Uttara - OCT 2015 Release - BR-7096

trigger ProblemCaseLinkAfterDelete on ProblemCaseLink__c (AFTER DELETE) {
    if(Utils_SDF_Methodology.canTrigger('ProblemCaseLinkAfterDelete')){   
       
       if(Trigger.old.size()>0) {
            AP_ProblemCaseLink.updateCaseAfterDelete(Trigger.Old);     
       }       
           
    }
}