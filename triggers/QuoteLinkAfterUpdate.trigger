trigger QuoteLinkAfterUpdate on OPP_QuoteLink__c (After Update) {
System.debug('****** QuoteLinkAfterUpdate start *****');
if(!VFC20_CreateSeries.doNotProcessQLKS){
System.debug('****** Not bypassing QuoteLinkAfterUpdate *****');
  if(Utils_SDF_Methodology.canTrigger('AP16')){
  Map<id,OPP_QuoteLink__c> quoteLink= new Map<id,OPP_QuoteLink__c>();
  System.Debug('****** QuoteLinkAfterUpdate Trigger Start****');
  for(OPP_QuoteLink__c quote:Trigger.new)
  {
       If(quote.Approver1__c!=trigger.Oldmap.get(Quote.id).Approver1__c||quote.Approver2__c!=trigger.Oldmap.get(Quote.id).Approver2__c||quote.Approver3__c!=trigger.Oldmap.get(Quote.id).Approver3__c
       ||quote.Approver4__c!=trigger.Oldmap.get(Quote.id).Approver4__c||quote.Approver5__c!=trigger.Oldmap.get(Quote.id).Approver5__c||quote.Approver6__c!=trigger.Oldmap.get(Quote.id).Approver6__c
       ||quote.Approver7__c!=trigger.Oldmap.get(Quote.id).Approver7__c||quote.Approver8__c!=trigger.Oldmap.get(Quote.id).Approver8__c||quote.Approver9__c!=trigger.Oldmap.get(Quote.id).Approver9__c
       ||quote.Approver10__c!=trigger.Oldmap.get(Quote.id).Approver10__c)
       {
       quotelink.put(Quote.id,Quote); 
       }
  }
  if(quotelink.size()>0)
  {
      AP16_GrantAcessToQteLink.updtAprAcc(quotelink,Trigger.oldMAP);
  }
  /*
  //Updated to grant access to Opportunity Leader
     AP49_UpdateQLKSharing.findOppLeaders(trigger.new,trigger.old);*/
   System.Debug('****** QuoteLinkAfterUpdate Trigger End****');
  }
/*New code block added in september release by abhihshek on 18/7/2011
*/
  if(Utils_SDF_Methodology.canTrigger('AP21'))
  {
      System.Debug('****** QuoteLinkAfterUpdate Trigger Start - To populate the quote issuance date****');
      list<OPP_QuoteLink__c> quoteLinkIssuanceDate= new list<OPP_QuoteLink__c>();
      for(OPP_QuoteLink__c quote:Trigger.new)
      {
          if(quote.ActiveQuote__c)
          {
              if((quote.IssuedQuoteDateToCustomer__c!=Trigger.Oldmap.get(quote.id).IssuedQuoteDateToCustomer__c)||(quote.ActiveQuote__c!=Trigger.Oldmap.get(quote.id).ActiveQuote__c))
              {
                  quoteLinkIssuanceDate.add(quote);
              }
          }
      }
      if(quoteLinkIssuanceDate.size()>0)
      {
          AP21_UpdateOpportunity.updtQteIsuDateToCust(quoteLinkIssuanceDate,false);
      }
      System.Debug('****** QuoteLinkAfterUpdate Trigger End - To populate the quote issuance date****');
      System.Debug('****** QuoteLinkAfterUpdate Trigger Start - To populate the TECH_HasActiveQLKwthApproved__c  Dec 12 Release ****');
      if(Utils_SDF_Methodology.canTrigger('AP21') && !AP11_UpdateQuoteLink.updateQlkFromOpp) // check if the trigger is called from a Opp before update trigger
      AP21_UpdateOpportunity.updateOpportunityTECHActiveWthApproved(Trigger.new);
      System.Debug('****** QuoteLinkAfterUpdate Trigger End- To populate the TECH_HasActiveQLKwthApproved__c  Dec 12 Release****');
              
  }  
  }
  System.debug('****** QuoteLinkAfterUpdate end *****');
}