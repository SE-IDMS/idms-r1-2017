trigger AnnouncementStakeholderAfterInsert on AnnouncementStakeholder__c (after Insert, after Update) {
    set<id> anncskhids = new set<id>();
    
    if(trigger.isupdate && trigger.isafter) {
        for(AnnouncementStakeholder__c annc : trigger.new){
            if(annc.Email__c != trigger.oldmap.get(annc.id).Email__c || annc.UserName__c != trigger.oldmap.get(annc.id).UserName__c){
                    anncskhids.add(annc.id);
            }
        }
    }
    if(trigger.isinsert && trigger.isafter) {
        for(AnnouncementStakeholder__c annc : trigger.new){
            anncskhids.add(annc.id);
        }
    }
    if(anncskhids.size()>0){
       AP_ASHupdateEmail.updateEmail(anncskhids);
    }
   
}