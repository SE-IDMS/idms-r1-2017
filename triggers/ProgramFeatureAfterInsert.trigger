trigger ProgramFeatureAfterInsert on ProgramFeature__c (after insert) {
    //list<ProgramFeature__c> lstNewPrgFeature = new list<ProgramFeature__c>();
    map<ID,list<ProgramFeature__c>> mapGlobalLevelFeature = new map<ID,list<ProgramFeature__c>>();
    list<ID> lstFtrProgramID = new list<ID>();
    map<ID,list<ProgramFeature__c>> mapCountryLevelAccountFeature = new map<ID,list<ProgramFeature__c>>();
    Boolean isContactFeature = false;
    if(Utils_SDF_Methodology.canTrigger('AP_PRF_CountryFeatureUpdate') || test.isRunningTest())
    {
        for(ProgramFeature__c PRGFeature : trigger.new)
        {
            if(PRGFeature.recordtypeid == Label.CLMAY13PRM06 && PRGFeature.FeatureStatus__c == Label.CLMAY13PRM09)
            {
                lstFtrProgramID.add(PRGFeature.PartnerProgram__c);//Global Partner Program
                if(!mapGlobalLevelFeature.containsKey(PRGFeature.ProgramLevel__c))
                    mapGlobalLevelFeature.put(PRGFeature.ProgramLevel__c, new list<ProgramFeature__c> {(PRGFeature)});
                else
                    mapGlobalLevelFeature.get(PRGFeature.ProgramLevel__c).add(PRGFeature);
            }
            
            if(PRGFeature.recordtypeid == Label.CLMAY13PRM04  && PRGFeature.featurestatus__c == Label.CLMAY13PRM09) 
            {
                //lstFtrProgramID.add(trigger.newMap.get(featureID).PartnerProgram__c);//Global Partner Program
                if(!mapCountryLevelAccountFeature.containsKey(PRGFeature.ProgramLevel__c))
                    mapCountryLevelAccountFeature.put(PRGFeature.ProgramLevel__c, new list<ProgramFeature__c> {(PRGFeature)});
                else
                    mapCountryLevelAccountFeature.get(PRGFeature.ProgramLevel__c).add(PRGFeature);
               if(PRGFeature.Enabled__c == Label.CLMAY13PRM11 || PRGFeature.Visibility__c == Label.CLMAY13PRM11)
               {
                   isContactFeature = true;
               }
            }
        }
        
        if(!mapGlobalLevelFeature.isEmpty())
            AP_PRF_CountryFeatureUpdate.creatCountryFeature(mapGlobalLevelFeature , lstFtrProgramID , null);
            
        if(!mapCountryLevelAccountFeature.isEmpty())
            AP_PRF_CountryFeatureUpdate.createActivateAccConFeature(mapCountryLevelAccountFeature , isContactFeature );
    }
}