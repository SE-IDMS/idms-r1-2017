//*********************************************************************************
// Trigger Name     : ProblemBeforeUpdate
// Purpose          : Problem  Before Update event trigger
// Created by       : Global Delivery Team
// Date created     : 18th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/


trigger ProblemBeforeUpdate on Problem__c (before update) {
    System.Debug('****** ProblemBeforeUpdate Trigger Start ****');
    
    List<Problem__c> prblmOwnerList = new List<Problem__c>();
    List<Problem__c> prblmLeaderList = new List<Problem__c>();  
    List<Problem__c> prblmStsList = new List<Problem__c>(); 
    List<Problem__c> prblmSeverityList = new List<Problem__c>(); 
    List<Problem__c> lstPrb = new List<Problem__c>();
    
    List<Problem__c> update8DStart = new List<Problem__c>();
    List<Problem__c> updateDateOfXA = new List<Problem__c>();
    List<Problem__c> updateDateOfRCA = new List<Problem__c>();
    List<Problem__c> updateDateOfCA = new List<Problem__c>();
    List<Problem__c> updateDateOfPA = new List<Problem__c>();
    
    //Hanamanth OCT2015
    Id ProblemSupplid = Schema.SObjectType.Problem__c.getRecordTypeInfosByName().get(label.CLOCT15I2P80).getRecordTypeId();
    
    if(Test.isRunningTest() || Utils_SDF_Methodology.canTrigger('AP1000_1'))
    //if(Utils_SDF_Methodology.canTrigger('AP1000_1'))
    {
        System.debug('------ProblemBeforeUpdate');
        //AP1000_Problem.ScenarioCheckOnUpdate(Trigger.oldMap,Trigger.newMap);
        
        //AP1000_Problem.updateBREStatusOnProblem(Trigger.new);
        
        for(Problem__c prblm: Trigger.new){
        IF(prblm.X8D_Rating__c==0){prblm.X8D_Rating__c=null;}
            if(!Test.isRunningTest()) {
             //======= Ram Chilukuri  - May2015RELEASE - START ==========
                prblm.ActualUserLastmodifiedDate__c = System.now();
                prblm.ActualUserLastmodifiedby__c = UserInfo.getUserId();
            //======= Ram Chilukuri  - May2015RELEASE - END =============
            }
            if(Trigger.oldMap.get(prblm.id).AccountableOrganization__c!= Trigger.newMap.get(prblm.id).AccountableOrganization__c){
                prblmOwnerList.add(prblm);    
            }
            if(Trigger.oldMap.get(prblm.id).ProblemLeader__c != Trigger.newMap.get(prblm.id).ProblemLeader__c){
                prblmLeaderList.add(prblm);    
            }
            //Oct 15 Release -- Divya M -- BR-6383
            if(Trigger.newMap.get(prblm.id).StatusOfContainment__c == 'Completed' || Trigger.newMap.get(prblm.id).StatusOfCorrection__c == 'Completed' || Trigger.newMap.get(prblm.id).StatusOfPrevention__c == 'Completed' || Trigger.newMap.get(prblm.id).StatusOfRootCauseAnalysis__c == 'Completed'){
                prblmStsList.add(prblm);
            }
            //Ends
            //Oct 15 Release -- Divya M -- BR-7369
            if(Trigger.newMap.get(prblm.id).Severity__c != Trigger.oldMap.get(prblm.id).Severity__c && Trigger.oldMap.get(prblm.id).Severity__c == 'Safety related'){
                prblmSeverityList.add(prblm);
            }
            //Ends
            
            //Oct 15 Release -- Divya M -- BR-7383
             system.debug('CountOfCommercialReferenceRecords__c ==>'+prblm.CountOfCommercialReferenceRecords__c +'==prblm.ProductQualityProblem__c==>'+prblm.ProductQualityProblem__c+'==prblm.Status__c==>'+prblm.Status__c+'==prblm.CountOfRelatedSymptoms__c==>'+prblm.CountOfRelatedSymptoms__c+'==prblm.Failure_Mode__c==>'+prblm.Failure_Mode__c+'==prblm.SubFailureMode__c==>'+prblm.SubFailureMode__c);
            if(((prblm.CountOfCommercialReferenceRecords__c < 1 || prblm.CountOfCommercialReferenceRecords__c == null )|| (prblm.CountOfRelatedSymptoms__c < 1 || prblm.CountOfRelatedSymptoms__c == null) || (prblm.Failure_Mode__c == null || prblm.Failure_Mode__c == '') || (prblm.SubFailureMode__c == null || prblm.SubFailureMode__c =='')) && prblm.ProductQualityProblem__c == true && prblm.Status__c == 'In Progress' && prblm.recordtypeid!=ProblemSupplid ) {
                system.debug('divya');
               prblm.addError(Label.CLOCT15I2P24);
            }
            //Ends
            
            //Added by Uttara - Oct 2015 Release - BR-7551
            if(Trigger.oldMap.get(prblm.id).X8DEvaluator__c != Trigger.newMap.get(prblm.id).X8DEvaluator__c
               && prblm.X8DEvaluator__c != null) {
                
                lstPrb.add(prblm); 
                System.debug('&&&& 8D Evaluator changed and not null');   
            }
            
            //Added by Uttara - Oct 2015 Release
            //Added Custom label to replace hardcoded values in JAN 16 release
            //CLJAN16I2P01 = 'Not Started'
            //CLJAN16I2P02 = 'Planning'
            //CLJAN16I2P04 = 'Completed'
            //CLOCT15I2P33 = 'In Progress'
            //CLJAN16I2P03 = 'Executing'
            //CLJAN16I2P05 = 'Checking'
            if(Trigger.oldMap.get(prblm.Id).Status__c != Label.CLOCT15I2P33 && prblm.Status__c == Label.CLOCT15I2P33 && prblm.DateOf8DStart__c == null) {
                System.debug('!!!! Status = In Progress');
                update8DStart.add(prblm);
            }        
            if(Trigger.oldMap.get(prblm.Id).StatusOfContainment__c != Label.CLJAN16I2P04 && prblm.StatusOfContainment__c == Label.CLJAN16I2P04 && prblm.DateOfContainment__c == null) {
                System.debug('!!!! StatusOfContainment = Executing');
                updateDateOfXA.add(prblm);
            } 
            if((Trigger.oldMap.get(prblm.Id).StatusOfContainment__c == Label.CLJAN16I2P01 || Trigger.oldMap.get(prblm.Id).StatusOfContainment__c == Label.CLJAN16I2P02) && (prblm.StatusOfContainment__c == Label.CLJAN16I2P03 || prblm.StatusOfContainment__c == Label.CLJAN16I2P05)&& prblm.DateOfContainment__c == null) {
                System.debug('!!!! StatusOfContainment = Completed');
                updateDateOfXA.add(prblm);
            }   
            if(Trigger.oldMap.get(prblm.Id).StatusOfRootCauseAnalysis__c != Label.CLJAN16I2P04 && prblm.StatusOfRootCauseAnalysis__c == Label.CLJAN16I2P04) {
                System.debug('!!!! StatusOfRootCauseAnalysis = Completed');
                updateDateOfRCA.add(prblm);
            }
            if(Trigger.oldMap.get(prblm.Id).StatusOfCorrection__c != Label.CLJAN16I2P04 && prblm.StatusOfCorrection__c == Label.CLJAN16I2P04) {
                System.debug('!!!! StatusOfCorrection = Completed');
                updateDateOfCA.add(prblm);
            }    
            if(Trigger.oldMap.get(prblm.Id).StatusOfPrevention__c != Label.CLJAN16I2P04 && prblm.StatusOfPrevention__c == Label.CLJAN16I2P04) {
                System.debug('!!!! StatusOfPrevention nalysis__c = Completed');
                updateDateOfPA.add(prblm);
            }    
        }
        
        //Added by Uttara - Oct 2015 Release
        if(!update8DStart.isEmpty() || !updateDateOfXA.isEmpty() || !updateDateOfRCA.isEmpty() || !updateDateOfCA.isEmpty() || !updateDateOfPA.isEmpty() )
            AP1000_Problem.updateDatesOnProblem(update8DStart,updateDateOfXA,updateDateOfRCA,updateDateOfCA,updateDateOfPA);
        
        
        if(prblmOwnerList.size()>0){
            AP1000_Problem.populateProblemOwner(prblmOwnerList);
        }
        if(prblmLeaderList.size()>0){
            AP1000_Problem.addUserToTeamMember(prblmLeaderList,FALSE);
        }
        //Advanced I2P : Added by Hari Krishna Start
        if(Trigger.new.size()>0){
            AP1000_Problem.assignProblemOwner(Trigger.oldMap,Trigger.newMap); // Commented as part of I2P Supplier Community Portal by Sreedhar 
        }
        //Advanced I2P : Added by Hari Krishna End
        //Oct 15 Release -- Divya M -- BR-6383
        if(prblmStsList.size()>0){
            AP1000_Problem.checkStatusofRelObj(prblmStsList);
        }
        //Ends
        //Oct 15 Release -- Divya M -- BR-7369
        if(prblmSeverityList.size()>0){
            AP1000_Problem.SeverityUpdate(prblmSeverityList);
        }
        //Ends
        
        //Added by Uttara - Oct 2015 Release - BR-7551
        if(!lstPrb.isEmpty())
            AP1000_Problem.Check8DEvaluator(Trigger.New,lstPrb);

    }
        
     
    
    System.Debug('****** ProblemBeforeUpdate Trigger End ****');
}