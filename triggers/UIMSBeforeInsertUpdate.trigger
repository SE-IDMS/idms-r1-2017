/* 
    Author  : A.D.N.V.Satyanarayana
    Created Date : 16-06-2016
    Description : In order to update the status fields when the respective fields are not blank 

*/

trigger UIMSBeforeInsertUpdate on UIMSCompany__c (before insert, before update) {
    System.debug('Trigger.New Before Update: '+Trigger.New);
    if (Utils_SDF_Methodology.canTrigger('AP_PRMAccountRegistration')) {
        for (UIMSCompany__c u : Trigger.New) {
            if(String.isNotBlank(u.bFOAccount__c)){
                u.AccountCreationStatus__c = True;
            }
            if (String.isNotBlank(u.bFOContact__c)){
                u.ContactCreationStatus__c = True;
            }
            if (String.isNotBlank(u.bFOUser__c)){
                u.bFOUserCreationStatus__c = True;
            }
            if (String.isNotBlank(u.bFOMember__c)){
                u.MemberCreationStatus__c = True;
            } 
        }
    }
    System.debug('Trigger.New After Update:' + Trigger.New);
}