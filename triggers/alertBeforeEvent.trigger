trigger alertBeforeEvent on SetupAlerts__c (before insert,before update) {

    if(Utils_SDF_Methodology.canTrigger('AP_PRMUtils')){
        AP_PRMUtils.checkAlerts(trigger.new);
    }

    
}