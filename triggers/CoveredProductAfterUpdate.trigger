trigger CoveredProductAfterUpdate on SVMXC__Service_Contract_Products__c (After update) 
{
    set<id>cppset= new set<id>();
    set<id>ipset= new set<id>();
    for(SVMXC__Service_Contract_Products__c cp:trigger.new)
    {
        if((cp.SVMXC__Installed_Product__c!=trigger.oldmap.get(cp.id).SVMXC__Installed_Product__c)||(cp.SVMXC__Start_Date__c!=trigger.oldmap.get(cp.id).SVMXC__Start_Date__c ||cp.SVMXC__End_Date__c!=trigger.oldmap.get(cp.id).SVMXC__End_Date__c))
        {
            cppset.add(cp.SVMXC__Service_Contract__c);
        }
              
        if(cp.SVMXC__Installed_Product__c!=null && cp.SVMXC__Installed_Product__c!=trigger.oldmap.get(cp.id).SVMXC__Installed_Product__c)
        {
            ipset.add(cp.SVMXC__Installed_Product__c);
        }
    }
    if(cppset!=null && cppset.size()>0)
        AP_ServiceContract.SCIBSyncUpdate(cppset);
    
    if(ipset!=null && ipset.size()>0)
    {
        Ap_Installed_Product.IBchangeMaster(ipset);
    }
}