trigger SupportRequestAfterInsert on OPP_SupportRequest__c (After Insert) 
{
    if(Utils_SDF_Methodology.canTrigger('AP22'))
    {
     System.Debug('****** SupportRequestAfterInsert Trigger Start****');
     Map<ID,OPP_SupportRequest__c> supportRequest = new Map<Id,OPP_SupportRequest__c>();
     Map<ID,OPP_SupportRequest__c> pmSupportRequest = new Map<Id,OPP_SupportRequest__c>();
     Map<ID,OPP_SupportRequest__c> adeSupportRequest = new Map<Id,OPP_SupportRequest__c>();
     Map<ID,OPP_SupportRequest__c> saSupportRequest = new Map<Id,OPP_SupportRequest__c>();
     Map<ID,OPP_SupportRequest__c> suppUser1 = new Map<Id,OPP_SupportRequest__c>();
     Map<ID,OPP_SupportRequest__c> suppUser2 = new Map<Id,OPP_SupportRequest__c>();     

        for(OPP_SupportRequest__c supportReq:trigger.new)
        {
            if(supportReq.AssignedToUser__c!=null)
            {   
                supportRequest.put(supportReq.id,supportReq);         
            }    
            if(supportReq.PMTender__c!=null)
            {
                pmSupportRequest.put(supportReq.id,supportReq);
            }
            /* Modified for BR-4172 */
            if(supportReq.AppDevEngineer__c!=null)
            {
                adeSupportRequest.put(supportReq.id,supportReq);
            }
            if(supportReq.SolutionArchitect__c!=null)
            {
                saSupportRequest.put(supportReq.id,supportReq);
            }
            if(supportReq.SupportUser1__c!=NULL)
            {
                suppUser1.put(supportReq.id,supportReq);
            }
            if(supportReq.SupportUser2__c!=NULL)
            {
                suppUser2.put(supportReq.id,supportReq);
            }
            /* End of Modification for BR-4172 */            
             
        }
        
        if(supportRequest.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(supportRequest, 'assignToTeam');
        }
        if(pmSupportRequest.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(pmSupportRequest, 'assignPMToTeam');
        }
        /* Modified for BR-4172 */
         if(adeSupportRequest.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(adeSupportRequest, 'assignADEToTeam');
        }
        if(saSupportRequest.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(saSupportRequest, 'assignSAToTeam');
        }
        if(suppUser1.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(suppUser1, 'assignSu1ToTeam');
        }  
        if(suppUser2.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(suppUser2, 'assignSu2ToTeam');
        }
        /* End of Modification for BR-4172 */        
        
        
         /*Start of sales  2012 April release update*/
            AP21_UpdateOpportunity.updateSolutionCenterInOpportunity(Trigger.new);
          /*End of sales  2012 April release update*/
    System.Debug('****** SupportRequestAfterInsert Trigger Ends****');    
    }
}