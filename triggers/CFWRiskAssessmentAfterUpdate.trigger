trigger CFWRiskAssessmentAfterUpdate on CFWRiskAssessment__c (after update) 
{
    List<CFWRiskAssessment__c> riskAssessments = new List<CFWRiskAssessment__c>();
    for(CFWRiskAssessment__c rk: trigger.new)
    {
        if(rk.ApplicableforcustomMobileApp__c!= trigger.oldMap.get(rk.Id).ApplicableforcustomMobileApp__c
        || rk.Whattypeofmobileapplication__c!= trigger.oldMap.get(rk.Id).Whattypeofmobileapplication__c
        || rk.Applicationstoringdatalocally__c!= trigger.oldMap.get(rk.Id).Applicationstoringdatalocally__c
        || rk.AccesstheenterprisesystemConsumer__c!= trigger.oldMap.get(rk.Id).AccesstheenterprisesystemConsumer__c
        || rk.accesstheenterprisesystemEnterprise__c!= trigger.oldMap.get(rk.Id).accesstheenterprisesystemEnterprise__c
        || rk.Howwillapplicationbedistributed__c!= trigger.oldMap.get(rk.Id).Howwillapplicationbedistributed__c
        || rk.WillEMMbeabletoremotelywipethedata__c!= trigger.oldMap.get(rk.Id).WillEMMbeabletoremotelywipethedata__c
        || rk.Tech_9_1MobileCoding__c != trigger.oldMap.get(rk.Id).Tech_9_1MobileCoding__c
        )
        {
            riskAssessments.add(rk);
        }
    }
    if(!(riskAssessments.isEmpty()))
        AP_CFWRiskAssessmentAfterupdate.updateRiskAssessment(riskAssessments);
}