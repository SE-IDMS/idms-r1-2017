trigger IPOStrategicMilestoneBeforeupdate on IPO_Strategic_Milestone__c (before update) {

  System.Debug('****** IPOStrategicMilestoneBeforeinsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('IPOStrategicMilestoneBeforeupdate '))
    {
     IPOStrategicMilestoneTriggers.IPOStrategicMilestoneBeforeUpdate(Trigger.new,Trigger.old);
     IPOStrategicMilestoneTriggers.IPOMilestoneUpdate_StatusUpdate(Trigger.new);
     if((Trigger.new[0].Global_Functions__c != Trigger.old[0].Global_Functions__c)|| (Trigger.new[0].Global_Business__c != Trigger.old[0].Global_Business__c) ||(Trigger.new[0].Operation_Regions__c != Trigger.old[0].Operation_Regions__c) ){
        
         IPOStrategicMilestoneTriggers.IPOStrategicMilestoneBeforeInsertUpdate(Trigger.new);
         IPOStrategicMilestoneTriggers.IPOstrategicMilestoneBeforeUpdate_CheckCascadingMilestoneScope(Trigger.new,Trigger.old);        
        if (Trigger.new[0].Validate_insert_update__c == True)
        
                Trigger.new[0].AddError(Label.ConnectGlobalProgramScopeValidateERR);  
                
                if(Trigger.new[0].ValidateScopeonUpdate__c == True)
                Trigger.new[0].AddError(Label.ConnectGlobalProgramScopeValidateERR);  
          }              
        
 }
 System.Debug('****** IPOStrategicProgramBeforeUpdate Trigger End ****'); 
}