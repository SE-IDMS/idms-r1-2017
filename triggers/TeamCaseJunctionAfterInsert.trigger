trigger TeamCaseJunctionAfterInsert on TeamCaseJunction__c (After insert) {
    
        if(Utils_SDF_Methodology.canTrigger('AP_TeamCaseJunction'))
        {
            AP_TeamCaseJunction.DefaultSupportedCaseClassifications(trigger.newMap);
        }     

}