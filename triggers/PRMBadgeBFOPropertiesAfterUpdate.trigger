trigger PRMBadgeBFOPropertiesAfterUpdate on PRMBadgeBFOProperties__c (after update) {
	if(Utils_SDF_Methodology.canTrigger('PRM_FieldChangeListener')){
		System.debug('BeginsPRMBadgeBFOPropertiesAfterUpdate');
		PRM_FieldChangeListener prmChangeListener = new PRM_FieldChangeListener();
		prmChangeListener.oldMemberExternalProperties(trigger.oldMap, false);
		
     	prmChangeListener.PRMBadgeBFOPropertiesInspecter(trigger.newMap);
		prmChangeListener.executeAction(); 
		}
}