/********************************************************************************************************************
    Created By : Vimal Karunakaran
    Description : For Oct 14  CCC Release:
                 1. Calls AP_Case_CaseHandler.updateLastActivityDate to update the Last Activity Date from CaseRelated List
********************************************************************************************************************/
trigger EscalationInternalCommentAfterInsert on CSE_L2L3InternalComments__c (after insert) {
	 System.Debug('****** EscalationInternalCommentAfterInsert  Trigger Started ****');
	 
	if(Utils_SDF_Methodology.canTrigger('AP_Case_CaseHandler') || Test.isRunningTest()){
		Set<Id> setCaseIds = new Set<Id>();
		for(CSE_L2L3InternalComments__c objEscalationInternalComment:Trigger.New){
			if(objEscalationInternalComment.Case__c!=null){
				setCaseIds.add(objEscalationInternalComment.Case__c);
			}
		}
		if(setCaseIds.size()>0){
			AP_Case_CaseHandler.updateLastActivityDate(setCaseIds);
		}
	}
	 System.Debug('****** EscalationInternalCommentAfterInsert  Trigger Finished****');
}