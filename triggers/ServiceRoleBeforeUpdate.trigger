/*     
@Hari
Created-01-06-14(Release)
Purpose Logic to check Duplicates in Service Role 
*/
trigger ServiceRoleBeforeUpdate on Role__c (before update) {

   if(Utils_SDF_Methodology.canTrigger('ServiceRoleBeforeUpdate')){
        List<Role__c> SerivceRoleList = new List<Role__c>();
        for(Role__c obj:Trigger.new){
            if(obj.InstalledProduct__c  != null 
                    && obj.Role__c  != Trigger.oldMap.get(obj.id).Role__c 
                    && obj.RecordTypeId == System.Label.CLJUL14SRV01 ){
                System.debug('*****************');
                SerivceRoleList.add(obj);
                
            
            }
        }
        if(SerivceRoleList != null && SerivceRoleList.size()>0){
            AP_IsSVMXRelatedContact.CheckDuplicateRole(SerivceRoleList ,'UPDATE');
        }
    }

}