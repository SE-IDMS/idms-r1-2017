// Added by Uttara - Q3 2016 Release - Notes to bFO (I2P)

trigger ImprovementProjectBeforeInsert on CSQ_ImprovementProject__c (Before Insert) {

    if(Utils_SDF_Methodology.canTrigger('IP')) { 
             
        Map<String,CSQ_ImprovementProject__c> mapOrgIdIPrecord = new Map<String,CSQ_ImprovementProject__c>();
        
        for(CSQ_ImprovementProject__c IP : Trigger.New) {
            
            if(IP.CSQ_AccountableOrganization__c != null) 
                mapOrgIdIPrecord.put(IP.CSQ_AccountableOrganization__c, IP);             
        }
        
        if(!mapOrgIdIPrecord.isEmpty())
            AP_ImprovementProject.SetProjectApprover(mapOrgIdIPrecord);
    }
}