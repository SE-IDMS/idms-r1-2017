trigger TEXBeforeInsert on TEX__c (Before insert) {
if(Utils_SDF_Methodology.canTrigger('AP_TechnicalExpertAssessment'))
   {
     AP_TechnicalExpertAssessment.updateCSIFlagOnTex(trigger.new)  ;  
     //AP_TechnicalExpertAssessment.updateContactEmail(trigger.new);
       //OCT 2015 RELEASE : BR-7761, DIVYA M 
     AP_TechnicalExpertAssessment.updateLineOfBusinessOnTEX(trigger.new);
     //ENDS OCT 2015 RELEASE     
     
     // OCT 2015 Release - added by Uttara PR
    
    List<TEX__c> TexList = new List<TEX__c>();   
    for(TEX__c Tex : Trigger.new) {
            Tex.LastUpdated__c = System.now();//June 2016 Release - BR-7799
            Tex.LastUpdatedby__c = UserInfo.getUserId();//June 2016 Release - BR-7799
        if(Tex.RelatedProblem__c != null && Tex.Case__c != null) {
            System.debug ('@@ Entered if 1 in Trigger');
            TexList.add(Tex);
        }    
    }    
    if(TexList.size() > 0) {
        System.debug ('@@ Entered if 2 in Trigger');
        AP_TechnicalExpertAssessment.CaseProblemLink(Texlist);    
    } 
   }
   
    
}