trigger InstalledProductTrigger on SVMXC__Installed_Product__c (after delete, after insert, after update, before delete, before insert, before update) {
   
   
   Ap_InstalledProduct ip = new Ap_InstalledProduct(Trigger.isBefore, Trigger.isAfter, 
                           Trigger.isUpdate, Trigger.isInsert, Trigger.isDelete, 
                           
                           Trigger.new, Trigger.oldMap, Trigger.newMap);
    ip.execute();
    
    
    
    
}