/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 02-Oct-2012
    Description     : Connect Cascading KPI Before Insert event trigger.
*/

trigger CascadingKPITargetBeforeInsert on Cascading_KPI_Target__c (before insert) {

System.Debug('****** CascadingKPITargetBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CascadingKPITargetBeforeInsert'))
    {
     ConnectCascadingKPITargetTriggers.CascadingKPITargetCountryChampionBeforeInsertUpdate(Trigger.new[0]);
     ConnectCascadingKPITargetTriggers.ConnectCascadingKPITargetBeforeInsertUpdate(Trigger.new[0]); 
     ConnectCascadingKPITargetTriggers.CascadingKPITargetBeforeInsertUpdate_ValidateScopeEntity(Trigger.new);
     
     if(Trigger.new[0].CountryPickList__c != null || Trigger.new[0].Scope__c != null)
    
     ConnectCascadingKPITargetTriggers.ConnectEntityKPIBeforeInsertUpdate_ValidateEntityRecord(Trigger.new);
     
     
     
     if (Trigger.new[0].ValidateScope__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_CascadingKPI_InsertUpdate_ERR1);  
                 
     if (Trigger.new[0].ValidateEntityInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.ConnectEntityKPIScopeDuplicateERR);              
        
    }
 System.Debug('****** CascadingKPITargetBeforeInsert Trigger End ****'); 
 }