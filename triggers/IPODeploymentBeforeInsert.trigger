Trigger IPODeploymentBeforeInsert on IPO_Strategic_Deployment_Network__c (before insert) {

  System.Debug('****** ConnectTeamMembersBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('IPODeploymentBeforeInsert'))
    {
        IPODeploymentNetworkTriggers.IPODeploymentNetworkBeforeInsert_ValidateScopeEntity(Trigger.New);
        IPOStrategicNetworkTriggers.IPOStrategicNetworkBeforeInsert(Trigger.New);
        
        if (Trigger.new[0].ValidateScopeonInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_Deployment_Network_Insert_ERR1);
   
 }
 System.Debug('****** ConnectTeamMembersBeforeInsert Trigger End ****'); 
}