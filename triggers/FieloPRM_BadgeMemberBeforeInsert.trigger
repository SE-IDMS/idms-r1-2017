/********************************************************************
* Company: Fielo
* Developer: 
* Created Date: 30/03/2015
* Description: 
********************************************************************/

trigger FieloPRM_BadgeMemberBeforeInsert on FieloEE__BadgeMember__c (before insert) {

    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_BadgeMemberTriggers')){
        List<FieloEE__BadgeMember__c> triggerNew = FieloPRM_AP_BadgeMemberTriggers.filterTriggerNew(trigger.new);
        if(!triggerNew.isEmpty()){
            FieloPRM_AP_BadgeMemberTriggers.badgesMembersSetActiveInactive(triggerNew, null);
            FieloPRM_AP_BadgeMemberTriggers.generateExpirationDate(triggerNew, null);
        }
    }    
}