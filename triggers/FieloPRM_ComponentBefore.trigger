/********************************************************************
* Company: Fielo
* Created Date: 30/11/2016
* Description:
********************************************************************/

trigger FieloPRM_ComponentBefore on FieloEE__Component__c(before insert, before update, before delete){    
    
    if(FieloEE__PublicSettings__c.getOrgDefaults().CacheEnabled__c != null && FieloEE__PublicSettings__c.getOrgDefaults().CacheEnabled__c){
        if(Trigger.isInsert || Trigger.isUpdate){
            FieloPRM_AP_ComponentTriggers.removeCache(Trigger.new);
        }else if(Trigger.isDelete){
            FieloPRM_AP_ComponentTriggers.removeCache(Trigger.old);
        }
    }
}