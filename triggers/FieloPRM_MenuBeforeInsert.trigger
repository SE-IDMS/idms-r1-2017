trigger FieloPRM_MenuBeforeInsert on FieloEE__Menu__c (before insert) {
	
	if(Utils_SDF_Methodology.canTrigger('FieloPRM_MenuBeforeInsert')){
	    FieloPRM_AP_MenuTriggers.emptyMenuFilter();
	    FieloPRM_AP_MenuTriggers.avoidDuplicateRule();
	}

}