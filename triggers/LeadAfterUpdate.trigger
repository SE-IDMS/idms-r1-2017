/*
    Author: Pooja Gupta
    Date: 15/02/2013
    Description: Trigger will call the method 'AP19_ReassignLeadsToContact()' of class 'AP19_Lead'  
*/


trigger LeadAfterUpdate on Lead (After Update)
{
    System.debug('***********LeadAfterUpdate Trigger Start***********');
    
    //To call the Trigger only once - We are using the string 'AP19_UpdateContact' in Utils_SDF_Methodology
    if(Utils_SDF_Methodology.canTrigger('AP19_UpdateContact'))
    {   
        System.debug('***********LeadAfterUpdate Trigger Start***********');
        
        Map<Id,Lead> leadContactToLeadMap=new Map<Id,Lead>();
        List<Lead> lstPassedtoPartnerLeads = new List<Lead>(); 
        Map<Id,Lead> leadContactRecycleMap=new Map<Id,Lead>(); 
        
        for(Id leadId:Trigger.newMap.keySet()) //NewMap of <id,Lead>
        {
            //Checking if the Updated Lead records LeadContact value is null and Contact/Account is not null
            if((Trigger.newMap.get(leadId).leadcontact__c==null) && (Trigger.oldmap.get(leadId).leadcontact__c != null) && (Trigger.newMap.get(leadId).contact__c != null || Trigger.newMap.get(leadId).account__c != null))
            {
                leadContactToLeadMap.put(Trigger.oldMap.get(leadId).leadcontact__c,Trigger.newMap.get(leadId));
            }
            //if lead is updated with Account and Not moved from Lead Contact (Lead Contact not changed   AND Account changed)
            if( (Trigger.newMap.get(leadId).account__c != null) && (Trigger.newMap.get(leadId).account__c != Trigger.oldmap.get(leadId).account__c) &&   (Trigger.newMap.get(leadId).leadcontact__c!=null) && (Trigger.newMap.get(leadId).leadcontact__c==Trigger.oldmap.get(leadId).leadcontact__c) )
                leadContactToLeadMap.put(Trigger.oldMap.get(leadId).leadcontact__c,Trigger.newMap.get(leadId));

            //Apr14 PRM        
            if(Trigger.newMap.get(leadId).ownerId != Trigger.oldMap.get(leadId).ownerId)
                lstPassedtoPartnerLeads.add(Trigger.newMap.get(leadId));
             
             //June 16 Release               
            if(Trigger.newMap.get(leadId).Status == Label.CL00510 && Trigger.newMap.get(leadId).SubStatus__c != Label.CLMAY13MKT008 && Trigger.newMap.get(leadId).TransactionalLead__c) //bMA Transactional Lead is Closed
               leadContactRecycleMap.put(Trigger.newMap.get(leadId).contact__c,Trigger.newMap.get(leadId));

        }
        system.debug('************* Recycle Contact ***** ' + leadContactRecycleMap);
        AP19_Lead.AP19_ReassignLeadsToContact(leadContactToLeadMap);
        if(!lstPassedtoPartnerLeads.isEmpty()) //Not empty
            AP19_Lead.processPassedtoPartnerLeads(lstPassedtoPartnerLeads);
            
        if(!leadContactRecycleMap.isEmpty())
           AP19_Lead.RecycleContactonTransactionLeadClosure(leadContactRecycleMap,'Update');

    }    
         System.debug('***********LeadAfterUpdate Trigger End***********');
}