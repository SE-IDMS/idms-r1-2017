trigger GenerateComponentParameters on FieloEE__Component__c (after insert) {

    if (Utils_SDF_Methodology.canTrigger('FieloPRM_AP_ComponentTriggers')) {
        Set<String> recordTypeSet = new Set<String>{'FieloPRM_C_PartnerLocatorForm'};

        Map<String,Id> recordTypeNamesById = new Map<String,Id>();
        for(RecordType record : [select id,name from RecordType where SobjectType='ComponentParameter__c']){
            recordTypeNamesById.put(record.name,record.id);
        }
        
        List<ComponentParameter__c> myComponentParametersToInsert = new List<ComponentParameter__c>();
        for(FieloEE__Component__c newComponent : [select id,Name,RecordType.name from FieloEE__Component__c where Id in:Trigger.new and RecordType.name in:recordTypeSet]){
            if(recordTypeNamesById.get(newComponent.RecordType.name)!=null){
                Id myComponentRecordTypeId = recordTypeNamesById.get(newComponent.RecordType.name);
                ComponentParameter__c myCMP = new ComponentParameter__c(RecordTypeId=myComponentRecordTypeId,Component__c=newComponent.Id);
                myComponentParametersToInsert.add(myCMP);
            }
        }
        insert myComponentParametersToInsert;
    }
}