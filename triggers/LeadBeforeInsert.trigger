/*
    Author          : ACN 
    Date Created    : 19/07/2011
    Description     : Lead Before Insert event trigger.
    
    12-June-2012    Srinivas Nalllapati     Mkt June 2012 release   BR-1772 The lead currency should be automatically set to the lead country currency during the upload. 
*/
trigger LeadBeforeInsert on Lead(before insert) 
{
    if(Utils_SDF_Methodology.canTrigger('AP19'))
    {
        System.Debug('****** LeadBeforeInsert Trigger - Updating Lead fields Starts ****');  
        set<Lead> leads = new set<Lead>();
        set<Lead> leadsTECHFields= new set<Lead>();    
        Set<Decimal> leadPriority = new set<Decimal>();   
        Map<Id, Lead> leadIds = new Map<Id, Lead>();       
        
        set<String> NonMarcomCampaignIds = new set<String>();//Dec UAT
        List<Lead> lstLeadToUpdateCampaign = new List<Lead>();
        
        Map<Id, List<Lead>> accountIds = new Map<Id, List<Lead>>();
        Map<Id, List<Lead>> contactIds = new Map<Id, List<Lead>>();
        
        for(Lead lead: Trigger.new)
        {
            if (lead.Company == null ) {
                if( lead.Account__c != null && lead.Contact__c == null)  {
                if (accountIds.ContainsKey( lead.Account__c )) 
                    accountIds.get( lead.Account__c ).add (lead);
                else
                    accountIds.put (lead.Account__c, new List<Lead>{ lead });
                }       
                
                if (lead.Account__c == null && lead.Contact__c != null) {
                    if (contactIds.ContainsKey( lead.Contact__c )) 
                        contactIds.get( lead.Contact__c ).add (lead);
                    else
                        contactIds.put (lead.Contact__c, new List<Lead>{ lead });
                }
                System.Debug('Consumer Leads with Accounts: ' + accountIds);
                System.Debug('Consumer Leads with Contacts: ' + contactIds);
            }
            if(lead.Priority__c!=null)
            {
                leadPriority.add(lead.Priority__c); 
                leads.add(lead);
            }
            if(lead.Opportunity__c!=null || lead.Country__c!=null || lead.Tech_W2LCountryCode__c != null)
                leadsTECHFields.add(lead); 
            if((lead.Status==Label.CL00447) && (lead.SubStatus__c==Label.CL00448))
                leadIds.put(lead.Id, lead);  
            //Dec UAT 
            system.debug('Hi'+lead.Tech_NonMarcomCampaignId__c+' '+lead.RecordTypeId);  
            if(lead.Tech_NonMarcomCampaignId__c != null && lead.RecordTypeId == System.Label.CLDEC12MKT12)
            {
                NonMarcomCampaignIds.add(lead.Tech_NonMarcomCampaignId__c);
                lstLeadToUpdateCampaign.add(lead);
            }//else
              //  lead.NonMarcommCampaign__c = null;                            
        }
        if(leads.size()>0)
            AP19_Lead.populateCallBack(leads,leadPriority);  
        if(leadsTECHFields.size()>0)
            AP19_Lead.populateTECHfields(leadsTECHFields);  
        if(leadIds.size()>0)
            AP19_Lead.validateLeadStatus(leadIds,'Insert');       
            
        System.Debug('****** LeadBeforeInsert Trigger - Updating Lead fields Ends ****');
        
        //Srinivas Nallapati    Sep12 Mkt Release   BR-1772
        //update lead currency to the currency of the Country
        AP19_Lead.updateLeadCurrency(trigger.new); 
        //End of Change for BR-1772 
        
        AP19_Lead.UpdateWithContactIdOfConsumerAccount(accountIds);
        AP19_Lead.UpdateWithAccountIdOfConsumerAccount(ContactIds);
        
        system.debug('The size of the list is'+NonMarcomCampaignIds.size());
        if(NonMarcomCampaignIds.size() > 0)// Dec12 Uat
        {
           AP19_Lead.validateNonMarcomCampaignId(NonMarcomCampaignIds, lstLeadToUpdateCampaign);
        }    
        
    }//End of if

}