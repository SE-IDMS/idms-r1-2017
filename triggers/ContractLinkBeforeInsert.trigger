trigger ContractLinkBeforeInsert  on OPP_ContractLink__c (before insert) 
{
     if(Utils_SDF_Methodology.canTrigger('AP52'))
     {
        AP52_ContractLinkFieldUpdate.performCurrentUserUpdates(Trigger.new);
        AP52_ContractLinkFieldUpdate.performLastUserUpdates(Trigger.new);
     }
}