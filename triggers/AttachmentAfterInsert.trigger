trigger AttachmentAfterInsert on Attachment (after insert) {
    System.Debug('****** AttachmentAfterInsert Trigger Starts****');
	if(Utils_SDF_Methodology.canTrigger('AP_Case_CaseHandler') || Test.isRunningTest()){
        /********************************************************************************************************************
            Modified By : Vimal Karunakaran
            Description : For Oct 14  CCC Release:
                         1. Calls AP_Case_CaseHandler.updateLastActivityDate to update the Last Activity Date from CaseRelated List
        ********************************************************************************************************************/
		String strCaseKeyPrefix = SObjectType.Case.getKeyPrefix(); 
		Set<Id> setCaseIds = new Set<Id>();
		for(Attachment objAttachment:Trigger.New){
			if(objAttachment.ParentId!=null){
				if(String.valueOf(objAttachment.ParentId).startsWith(strCaseKeyPrefix)){
					setCaseIds.add(objAttachment.ParentId);
				}
			}
		}
		if(setCaseIds.size()>0){
			AP_Case_CaseHandler.updateLastActivityDate(setCaseIds);
		}
	}
    System.Debug('****** AttachmentAfterInsert Trigger Ends****');
}