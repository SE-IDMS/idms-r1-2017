trigger BusinessRiskAfterUpdate on BusinessRiskEscalations__c (After update) {
      System.Debug('****** BusinessRiskAfterUpdate Trigger Start ****');
       Map<Id,BusinessRiskEscalations__c> bRiskMap = new Map<Id,BusinessRiskEscalations__c>();
         if(Utils_SDF_Methodology.canTrigger('AP09')){                  
                for(BusinessRiskEscalations__c bRisk : Trigger.new){
                       if(bRisk.Status__c != trigger.oldMap.get(bRisk.Id).Status__c){

                            bRiskMap.put(bRisk.Id,bRisk);
                           }
                     } 
                if(bRiskMap.size()>= 0){
                   AP09_BusinessRiskEscalation.updateBusinessRiskEscalationStakeholders(bRiskMap.Keyset());  
                 } 
           } 
                                                           
                           
     System.Debug('****** BusinessRiskAfterUpdate Trigger End ****');
}