trigger CoveredProductAfterDelete on SVMXC__Service_Contract_Products__c (After Delete) {
        set<id>cpset= new set<id>();
        
    for(SVMXC__Service_Contract_Products__c cp:trigger.old){
            if(cp.SVMXC__Service_Contract__c!=null){
            cpset.add(cp.SVMXC__Service_Contract__c);
            }
        }
        
        if(cpset!=null && cpset.size()>0)
        AP_ServiceContract.SCIBSyncUpdate(cpset);
        

}