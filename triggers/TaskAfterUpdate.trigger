/*
    Author          : ACN 
    Date Created    : 24/07/2011
    Description     : Task After Update event trigger.
    
    14-June-2012    Srinivas Nallapati  Sep12 Mkt Release   BR-1771:
    07-Dec-2012     Srinivas Nallapati  AC Dec 12 release   Validation to prevent creation of tasks for person accoutns DEF-
    20-March-2014   Pooja Suresh  April 14 Release          Track the date when first event or task was created for Opportunity
    
    ********************************************************************************************************************
        Modified By : Vimal Karunakaran
        Description : For Oct 14  CCC Release:
                     1. Calls AP_Case_CaseHandler.updateLastActivityDate to update the Last Activity Date from CaseRelated List
    ********************************************************************************************************************

*/

trigger TaskAfterUpdate on Task (after update){
    boolean blnCanTriggerAP25 = false;
    boolean blnCanTriggerAPCaseHandler = false;
    
    blnCanTriggerAP25 = Utils_SDF_Methodology.canTrigger('AP25');
    blnCanTriggerAPCaseHandler = Utils_SDF_Methodology.canTrigger('AP_Case_CaseHandler');
    
    if(blnCanTriggerAP25 || blnCanTriggerAPCaseHandler){
        System.Debug('****** TaskAfterUpdate Trigger - Updating Lead Starts ****');  
        Map<Id,Task> newTasks= new Map<Id,Task>();     
        Map<Id,Task> oldTasks= new Map<Id,Task>();     
        String leadKeyPrefix = SObjectType.Lead.getKeyPrefix();
        
        String strCaseKeyPrefix = SObjectType.Case.getKeyPrefix(); 
        Set<Id> setCaseIds = new Set<Id>();
        
        Map<Id,Task> newOpptyTasks= new Map<Id,Task>();
        Map<Id,Task> tasks= new Map<Id,Task>();
        String OpptyKeyPrefix = SObjectType.Opportunity.getKeyPrefix();
        Map<Id,Id> opptyTaskIds = new Map<Id,Id>();
        
        set<id> setAccIds = new set<id>();
         
        for(Task task: Trigger.new){
            Task beforeUpdate = System.Trigger.oldMap.get(task.Id);
            if(task.WhoId!=null){
                //Srinivas Nallapati  Change for BR-1771, Update lead status only when 'Call' tasks are created
                if(((String.valueOf(task.WhoId)).startsWith(leadKeyPrefix)) && ((beforeUpdate.Status!=Label.CL00327 && beforeUpdate.Status!=task.Status && task.Status==Label.CL00327)|| 
                    beforeUpdate.ActivityDate!=task.ActivityDate) && (task.TECH_TaskType__c == Label.CLSEP12MKT01 || task.TECH_TaskType__c == null) ){
                    newTasks.put(task.Id, task);
                    oldTasks.put(beforeUpdate.Id, beforeUpdate);                     
                }         
            }
             
            String strWhatId = task.WhatId; 
            if(strWhatId != null && strWhatId.startsWith('001') && task.WhatId != beforeUpdate.WhatId ){
                setAccIds.add(task.WhatId);
            }
            else if((task.WhatId!=null) && (String.valueOf(task.WhatId)).startsWith(OpptyKeyPrefix)){
                opptyTaskIds.put(task.Id,task.WhatId);
                tasks.put(task.Id,task);
            }
            //BR-7933 Exclude Last Activity Date on Case, if Call Type is defined. (Warm Transfer Issue) 
            else if((task.WhatId!=null) && (String.valueOf(task.WhatId)).startsWith(strCaseKeyPrefix) && task.CallType==null){
                setCaseIds.add(task.whatId);
            }
        }
        if(blnCanTriggerAP25){ 
            if(setAccIds.size() > 0){
                set<id> setPerAccs = new set<id>();
                setPerAccs = AP25_Task.getPersonAccountIds(setAccIds);
                  
                if(setPerAccs != null)
                    for(Task tk:Trigger.new){
                        if(setPerAccs.contains(tk.whatId))
                            tk.addError(System.Label.CLDEC12AC12);
                        }
            }
             
            if(!(newTasks.isEmpty()))
                AP25_Task.updateTasks(oldTasks,newTasks); 
                    
            System.Debug('****** TaskAfterUpdate Trigger - Updating Lead Ends****');
            System.Debug('****** TaskAfterUpdate Trigger - First Activity Date for an Opportunity Begins ****'); 
            if(opptyTaskIds.size()>0){
                Map<Id,Opportunity> opptyCreatedDate = new Map<Id,Opportunity>([SELECT CreatedDate FROM Opportunity WHERE Id IN :opptyTaskIds.values()]); 
                for(Id taskId: opptyTaskIds.keySet()){
                    if( opptyCreatedDate.get(opptyTaskIds.get(taskId)).CreatedDate.date() > Date.valueOf(Label.CLAPR14SLS62))       {
                        newOpptyTasks.put(taskId,tasks.get(taskId));
                    }
                }              
                //System.Debug('----- AP25_Task <- NewOpptyTasks '+newOpptyTasks);
                if(!(newOpptyTasks.isEmpty()))
                    AP25_Task.insertOpptyTasks(newOpptyTasks);
            }
            System.Debug('****** TaskAfterUpdate Trigger - First Activity Date for an Opportunity Ends ****');
        } 
        if(blnCanTriggerAPCaseHandler){
            if(setCaseIds.size()>0){
                AP_Case_CaseHandler.updateLastActivityDate(setCaseIds);
            }
        }
    } 
}