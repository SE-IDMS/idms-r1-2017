/*     
@Author: Ramu Veligeti
Created Date: 20-07-2011
Description: This trigger fires on Work Order on Before Update
**********
Scenarios:
**********
1.  Update Work Order status on Work Order Notification
2.  Update Work Order ownership to Primary Technician on Work Order
3.  Update Work Order ownership to Service Center Queue when NEW Work Order is created or Work Order status = ‘Rescheduled’:    
    3a) Added the requirement to assign WO owner as Technician's Manager when WO status is 'Service Completed'  
    3b) Added the requirement to assign WO owner as Current user for Entire Rescheduling process using SFM 
4.  Localized Email Notification Solution
    4a)Site Authorization
    4b)Customer Confirmation – 1 Week before scheduled date
    4c)Account Owner Notification – 1 Week before scheduled date //Added by JSINGHLA for October 2013 Release
5.  Customer Location is Updated(Location is swapped with Customer Location
6.  Update Work Order ownership to Territory queue
7.  Case Owner is Updated
8.  Contact Phone is Updated
9.  Account Type is updated
10. Owner Email Field Update
11. Updating the WO record of field ShipToAccount and BillToAccount to  soldToAccount.
12. Consolidating Certifications (Habilitation Requirements) on the Work Order.
13. Update Preferred FSR on Work Order Base on Location/Account.
14. Validation of Work order status is completed.

16. Automatically calculate WO's Primary Territory using Territory Coverage definition
17. Update to fill in the Completed By field if it's null and the Status changed to completed.

Date Modified       Modified By             Comments
----------------------------------------------------------------------------------------------------------------
21-07-2011          Ramu Veligeti           Added Logic for Second scenario
26-07-2011          Praveen Simha           Added Logic for third scenario
27-07-2011          Anita D'Souza           Modified logic for scenario, so that it fires only when WON is not null
02-08-2011          Praveen Simha           Added Logic for fourth scenario
02-08-2011          Anita D'Souza           Logic to support langauge specific email template
14-10-2011          Anita D'Souza           Logic to update Work Order ownership to Territory queue due to SDFC execution order of triggers 
07-11-2012          Jyotiranjan Singhlal    Logic to update Work Order Case Owner field if Case is changed
14-11-2012          Jyotiranjan Singhlal    Logic to update Work Order Contact Phone field if Contact is changed
19-11-2012          Jyotiranjan Singhlal    Logic to update Work Order Account Type if account is changed
20-11-2012          Jyotiranjan Singhlal    Added Time Zone Logic to scenario 5
15-03-2013          Jyotiranjan Singhlal    Updated Location detauls in scenario 5 
27-03-2013          Deepak Kumar            Update The Owner Email Field.
07-04-2013          Jyotiranjan Singhlal    Updated Logic in scenario 3 with service completed
20-06-2013          Jyotiranjan Singhlal    Updated condition in scenario 5 to avoid null values in Date Time
11-07-2013          Jyotiranjan Singhlal    Mofifed the logic for scenario 3a with Approval Manager
16-07-2013          Deepak Kumar            Added the logic to Update the WO record of field ShipToAccount and BillToAccount to  soldToAccount.
23-07-2013          Ramu Veligeti           Logic to change 'Customer Confirmed' to 'Unscheduled'
30-07-2013          Ramu Veligeti           Added the logic to update skills on WO from Habilitation requirements object
26-09-2013          Hari Krishna            Added the logic to Preferred FSR
28-02-2014          Deepak                  Added Logic to update customer and Fsr Time Zone
11-09-2015          Hari Krishna            logic changed for Work Order ownership to Primary Technician on Work Order on Customer Confirmed to Acknowledge FSE
03-02-2016          Adrian Modolea          Added logic to call ServiceMaxWOTerritoryAssignment apex class
02-03-2016          Scott Fawcett           Added logic to fill out Completed By Field when Status moves to completed if it's currently null

*/
      
trigger WorkOrderBeforeUpdate on SVMXC__Service_Order__c (before update) 
{
   
    //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX07'))
    {
    
        //Varaibles related to Scenario 1
        List<SVMXC__Service_Order__c> solst = new List<SVMXC__Service_Order__c>();
        Set<Id> wonId = new set<Id>();
        Map<Id,Id> womap = new Map<Id,Id>();
        List<WorkOrderNotification__c> wnlst = new List<WorkOrderNotification__c>();
        
        //Variables related to Scenario 2
        List<SVMXC__Service_Order__c> wolst = new List<SVMXC__Service_Order__c>();
        Set<Id> techId = new Set<Id>();
        Set<Id> woId = new Set<Id>();
        List<SVMXC__Service_Order__Share> woShareLst = new List<SVMXC__Service_Order__Share>();
        Map<Id,Set<Id>> EventMap = new Map<Id,Set<Id>>();
        List<Event> Eventlst = new List<Event>();
        Map<Id,SVMXC__Service_Group_Members__c> techMap;
        
        //Variables related to Scenario 3    
        List<SVMXC__Service_Order__c> wolst3 = new List<SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order__c> wolstnew3 = new List<SVMXC__Service_Order__c>();
        Set<Id> WoterId = new Set<Id>();
        Set<Id> woId3 = new Set<Id>();  
        List<SVMXC__Territory__c> terMap = New List <SVMXC__Territory__c>();
        Set<Id> sfuserid = new Set<Id>();
        
        //Variables related to Scenario 3a 
         List<SVMXC__Service_Order__c> wolst3a = new List<SVMXC__Service_Order__c>();  
         List<SVMXC__Service_Order__c> wolstnew3a = new List<SVMXC__Service_Order__c>();    
         Set<Id> wotech3Id = new Set<Id>();
         Set<Id> woId3a = new Set<Id>();
         List<SVMXC__Service_Group_Members__c> techmap2 = New List <SVMXC__Service_Group_Members__c>();
         
         //Variables related to Scenario 3b 
         List<SVMXC__Service_Order__c> wolst3b = new List<SVMXC__Service_Order__c>();  
         List<SVMXC__Service_Order__c> wolstnew3b = new List<SVMXC__Service_Order__c>();    
         Set<Id> woId3b = new Set<Id>();
        
        //Variables related to Scenario 3c
            set<id> Wovalidset = New set<id>();
        
        //Variablees related to Scenario 4a
        Map<id,SVMXC__Service_Order__c> woEmailMap = new Map<id,SVMXC__Service_Order__c>();
        Set<SVMXC__Service_Order__c> wolst4 = new Set<SVMXC__Service_Order__c>();       
        List<Id> woId4a = new List<Id>();  
        
        //Variablees related to Scenario 4b      
        List<Id> woId4b = new List<Id>(); 
        
        //Variablees related to Scenario 4c      
        //List<Id> woId4c = new List<Id>(); 
        
        //Variables related to Scenario 5 
        //List<SVMXC__Service_Order__c> wolst5 = new List<SVMXC__Service_Order__c>();    
        //Set<id> wocustloc = New Set<id>();
        
        List<SVMXC__Service_Order__c> wolst5 = new List<SVMXC__Service_Order__c>();    
        Set<id> woloc = New Set<id>();
       
       //Variables related to Scenario 6 
        List<SVMXC__Service_Order__c> wolst6 = new List<SVMXC__Service_Order__c>();     
        Set<id> woprytry = New Set<id>();
        
        //Variables related to Scenario 7 
        List<case> caseown = new list <case>();
        Set<id> wocase = New Set<id>();
        List<SVMXC__Service_Order__c> wolst7 = new List<SVMXC__Service_Order__c>();
        
        //Variables related to Scenario 8
        List<contact> contactphone = new list <contact>();
        Set<id> wocontact = New Set<id>();
        List<SVMXC__Service_Order__c> wolst8 = new List<SVMXC__Service_Order__c>();
        
        //Variables related to Scenario 9
        List<account> accntype = new list <account>();
        Set<id> woaccnt = New Set<id>();
        List<SVMXC__Service_Order__c> wolst9 = new List<SVMXC__Service_Order__c>();
        
        //Variables related to Scenario 10. 
        //List<User> users = new list <User>();
         Set<id> woOwnerId = New Set<id>();
         List<SVMXC__Service_Order__c> wolst1 = new List<SVMXC__Service_Order__c>();
         Set<id> woPrevOwnerId = New Set<id>();
         Map<Id,User> user1;
         // Hari Added Start
         List<SVMXC__Service_Order__c> wOrdertoProcess = new List<SVMXC__Service_Order__c>();  
        // Hari Added End  
        //variable related to scenario 11.
         list<SVMXC__Service_Order__c> wolist = New list<SVMXC__Service_Order__c>();
            //variable related to scenario 11 a
            set<Id> accid = New set<Id>();
            list<SVMXC__Service_Order__c> workOrderList = New list<SVMXC__Service_Order__c>();
          //Varible related to scenario 12.
        list<SVMXC__Service_Order__c> PreferredFSRWoList= New list<SVMXC__Service_Order__c>();
        //Varible related to scenario 13.
        list<SVMXC__Service_Order__c> WorList= New list<SVMXC__Service_Order__c>();
       List<SVMXC__Service_Order__c> woAcceptRejectList = new List<SVMXC__Service_Order__c>();
       
       //Added by VISHNU C for BR 8684 Q2 2016 Release
       Set<id> woidCancelled = New Set<id>();
       //Changes by VISHNUC C ends here
        
        for(SVMXC__Service_Order__c a:trigger.new)
        {
            system.debug('### The Order status is' + trigger.NewMap.get(a.id).SVMXC__Order_Status__c);
            
                        // Added by Suhail for DEF-10636 start
           /* if((trigger.oldMap.get(a.Id).SVMXC__Order_Status__c != a.SVMXC__Order_Status__c)&&(a.SVMXC__Order_Status__c == 'Scheduled'  || a.SVMXC__Order_Status__c == 'Customer Confirmed')){
                a.PlannerContact__c = UserInfo.getUserId();
            }*/
            // Added by Suhail for DEF-10636 end //for commit
            //24816
            //Added by VISHNU C for BR 8684 Q22016 Release
            if( ( trigger.oldMap.get(a.Id).SVMXC__Order_Status__c != a.SVMXC__Order_Status__c ) && a.SVMXC__Order_Status__c == 'Cancelled' )
            {
                //a.addError('Cancel Parts Orders before cancelling the WO');
                woidCancelled.add(a.id);
                system.debug('Work Order Cancelled Ids'+woidCancelled);
            
            }
            //Changes by VISHNU C ends here
            
            
            if(a.SVMXC__Component__c==null && a.SVMXC__Component__c!=trigger.oldmap.get(a.id).SVMXC__Component__c)
            {
                    a.SVMXC__Product__c=null;
            }
            
            
            //Condition for Scenario 1
            if((trigger.oldMap.get(a.Id).SVMXC__Order_Status__c != trigger.newMap.get(a.Id).SVMXC__Order_Status__c
            || trigger.oldMap.get(a.Id).SVMXC__Scheduled_Date_Time__c != trigger.newMap.get(a.Id).SVMXC__Scheduled_Date_Time__c
            || trigger.oldMap.get(a.Id).Comments_to_Planner__c != trigger.newMap.get(a.Id).Comments_to_Planner__c)
            && trigger.newMap.get(a.Id).Parent_Work_Order__c == null && trigger.newMap.get(a.Id).Work_Order_Notification__c!= null)
            {
                solst.add(a);
                wonId.add(a.Work_Order_Notification__c);
                system.debug('solst==='+solst+'====wonId==='+wonId);
            }        
            //Condition for Scenario 2
            //if(a.SVMXC__Order_Status__c == 'Customer Confirmed' && trigger.oldMap.get(a.id).SVMXC__Order_Status__c!=trigger.NewMap.get(a.id).SVMXC__Order_Status__c && a.SVMXC__Group_Member__c != null)
             if(trigger.oldMap.get(a.id).SVMXC__Order_Status__c!=trigger.NewMap.get(a.id).SVMXC__Order_Status__c && a.SVMXC__Order_Status__c == 'Acknowledge FSE' && trigger.oldMap.get(a.id).SVMXC__Order_Status__c == 'Customer Confirmed' &&  a.SVMXC__Group_Member__c != null)            
            {
                wolst.add(a);
                woId.add(a.Id);
                techId.add(a.SVMXC__Group_Member__c);
                system.debug('wolst==='+wolst+'==woId=='+woId+'==techId=='+techId);
            }        
            //Condition for Scenario 3           
            if(trigger.NewMap.get(a.id).SVMXC__Order_Status__c=='Rescheduled' && trigger.oldMap.get(a.id).SVMXC__Order_Status__c!=trigger.NewMap.get(a.id).SVMXC__Order_Status__c) //
            {
                wolst3.add(a);
                wolstnew3.add(a);
                woId3.add(a.Id);
                woterId.add(a.SVMXC__Primary_Territory__c);
            }   
            if(trigger.oldMap.get(a.id).SVMXC__Primary_Territory__c!=trigger.NewMap.get(a.id).SVMXC__Primary_Territory__c && a.SVMXC__Primary_Territory__c != null)
            {
                wolst3.add(a);
                wolstnew3.add(a);
                woId3.add(a.Id);
                woterId.add(a.SVMXC__Primary_Territory__c);
            }     
            //Condition for Scenario 3a        
            /*(trigger.NewMap.get(a.id).SVMXC__Order_Status__c == 'Unscheduled' && trigger.NewMap.get(a.id).SVMXC__Order_Status__c <> trigger.oldMap.get(a.Id).SVMXC__Order_Status__c 
                && trigger.oldMap.get(a.Id).SVMXC__Order_Status__c == 'Customer Confirmed') 
                   */     
            if(trigger.NewMap.get(a.id).SVMXC__Order_Status__c == 'Service Complete' && 
                trigger.oldMap.get(a.id).SVMXC__Order_Status__c!=trigger.NewMap.get(a.id).SVMXC__Order_Status__c)                  
            {
                system.debug('### The Order status is' + trigger.NewMap.get(a.id).SVMXC__Order_Status__c);
                //a.SVMXC__Closed_By__c = UserInfo.getUserId();
                //a.SVMXC__Closed_On__c = Datetime.now(); 
                wolst3a.add(a);
                wolstnew3a.add(a);
                woId3a.add(a.Id);
                wotech3Id.add(a.SVMXC__Group_Member__c);           
            }      
            //Condition for Scenario 3b
            if((trigger.NewMap.get(a.id).RescheduleReason__c != null && trigger.NewMap.get(a.id).SubStatus__c == 'To Be Rescheduled' 
            && trigger.NewMap.get(a.id).SVMXC__Order_Status__c == 'Unscheduled' 
            && trigger.NewMap.get(a.id).SVMXC__Order_Status__c <> trigger.oldMap.get(a.Id).SVMXC__Order_Status__c       
            && (trigger.oldMap.get(a.Id).SVMXC__Order_Status__c == 'Customer Confirmed'|| 
                trigger.oldMap.get(a.Id).SVMXC__Order_Status__c == 'Acknowledge FSE') )|| ((trigger.NewMap.get(a.id).SVMXC__Order_Status__c <> trigger.oldMap.get(a.Id).SVMXC__Order_Status__c) && a.SVMXC__Order_Status__c == 'Cancelled' )) 
            {
               If(trigger.oldMap.get(a.Id).SVMXC__Order_Status__c == 'Acknowledge FSE')
               {
                    if(a.TechPreviousOwner__c != null)
                    {
                        a.OwnerId= ID.valueof(a.TechPreviousOwner__c);
                    }   
               }
                //a.OwnerId = userinfo.getUserId();   
                a.SVMXC__Scheduled_Date_Time__c = null;
                a.SVMXC__Group_Member__c = null;
                a.SVMXC__Service_Group__c = null;
                a.TrackedRescheduleReason__c = '';
                a.TECH_IsAlertFSESent__c = false;
                a.SVMXC__Dispatch_Response__c = '';
                a.FirstAssignedFSR__c = 0;
                a.AcceptedFSRIds__c =''; //Def 9406 
                a.IsRejected__c = false;
            } 
            
            
            //Condition for Scenario 3c
            if(a.SVMXC__Order_Status__c=='Service Complete' && a.ownerId!=trigger.oldmap.get(a.id).OwnerId)
            {
               Wovalidset.add(a.Id);
            
            }
      
            
           //Condition for Scenario 4a
            if(a.SendEmailToThirdParty__c == FALSE && (a.Work_Order_Category__c == '3rd Party' || a.TechnicianType__c == 'Non-Schneider Employee') &&          
                a.SVMXC__Order_Status__c == 'Customer Confirmed' && 
                trigger.oldMap.get(a.id).SVMXC__Order_Status__c!=trigger.NewMap.get(a.id).SVMXC__Order_Status__c)
            {                  
                wolst4.add(a);
                woId4a.add(a.Id);
                a.SendEmailToThirdParty__c = TRUE;                      
            }    
           //Condition for Scenario 4b
            if(a.SendEmailToCustomer__c == TRUE && (a.Work_Order_Category__c == 'Onsite' || a.Work_Order_Category__c == '3rd party'))   
            {            
                wolst4.add(a);
                woId4b.add(a.Id);
                a.SendEmailToCustomer__c = FALSE;
            }
             //Condition for Scenario 4c
             /* Commented for July 15 release.
            if(a.AccountNotification__c == TRUE && a.NotifyAccountOwner__c == TRUE && (a.Work_Order_Category__c == 'Onsite' || a.Work_Order_Category__c == '3rd party'))   
            {            
                woId4c.add(a.Id);
                a.AccountNotification__c = FALSE;
            } */
           //Condition for Scenario 5
            if((trigger.oldMap.get(a.Id).CustomerRequestedDate__c != trigger.newMap.get(a.Id).CustomerRequestedDate__c)
            || (trigger.oldMap.get(a.Id).CustomerRequestedTime__c != trigger.newMap.get(a.Id).CustomerRequestedTime__c)
            ||(a.Customer_Service_Request_Time__c ==null && a.CustomerRequestedTime__c !=null && a.CustomerRequestedDate__c != null))
            {
                //Added for June 2013 release for the Invalid Date time error
                String DatetimeField;
                if(a.CustomerRequestedTime__c==null)
                    DatetimeField = '0:00'; 
                else
                    DatetimeField = a.CustomerRequestedTime__c;  
                //End for June 2013 release for the Invalid Date time error       
                Date requestedDate = a.CustomerRequestedDate__c;
                String sDate = String.valueOf(requestedDate);                     
                String newDateTime = sDate+' '+DateTimeField+':'+'00';
                a.Customer_Service_Request_Time__c = DateTime.valueOf(newDateTime);
                     
            } 
           //Condition for Scenario 5
           /* if(a.Customer_Location__c != Null && 
               (trigger.oldMap.get(a.id).Customer_Location__c!=trigger.NewMap.get(a.id).Customer_Location__c ||
                trigger.oldMap.get(a.id).Service_Business_Unit__c!=trigger.NewMap.get(a.id).Service_Business_Unit__c))                  
            {
                wolst5.add(a);
                wocustloc.add(a.Customer_Location__c);        
            } */
            if(a.SVMXC__Site__c != Null && 
               (trigger.oldMap.get(a.id).SVMXC__Site__c!=trigger.NewMap.get(a.id).SVMXC__Site__c ||
                trigger.oldMap.get(a.id).Service_Business_Unit__c!=trigger.NewMap.get(a.id).Service_Business_Unit__c))                  
            {    
                wolst5.add(a);
                woloc.add(a.SVMXC__Site__c);
                
            } 
            system.debug('Test-A'+a.SVMXC__Primary_Territory__c);
            //Condition for Scenario 6
            if(a.UpdateOwnership__c == True && a.SVMXC__Primary_Territory__c!=null &&
               trigger.oldMap.get(a.id).UpdateOwnership__c!=trigger.NewMap.get(a.id).UpdateOwnership__c )                  
            {
                wolst6.add(a);
                woprytry.add(a.SVMXC__Primary_Territory__c);        
            } 
            //Condition for Scenario 7
           if(a.SVMXC__Case__c!= null && trigger.oldMap.get(a.id).SVMXC__Case__c!=trigger.NewMap.get(a.id).SVMXC__Case__c)
            {
                wolst7.add(a);
                wocase.add(a.SVMXC__Case__c);
            }   
            //Condition for Scenario 8
           if(a.SVMXC__Contact__c!= null && trigger.oldMap.get(a.id).SVMXC__Contact__c!=trigger.NewMap.get(a.id).SVMXC__Contact__c)
            {
                wolst8.add(a);
                wocontact.add(a.SVMXC__Contact__c);
            } 
           //Condition for Scenario 9
           if(a.SVMXC__Company__c!= null && trigger.oldMap.get(a.id).SVMXC__Contact__c!=trigger.NewMap.get(a.id).SVMXC__Contact__c)
            {
                wolst9.add(a);
                woaccnt.add(a.SVMXC__Company__c);
            }  
            
           
            
            //Condition for Scenario 10
            
            if(a.OwnerId != null && trigger.oldMap.get(a.id).OwnerId!=trigger.NewMap.get(a.id).OwnerId)
            {
              wolst1.add(a);
              woOwnerid.add(a.OwnerId);
              if(trigger.oldMap.get(a.id).SVMXC__Order_Status__c =='Customer Confirmed')
              {
                    //a.Tech_PreviousOwner__c = trigger.oldMap.get(a.id).OwnerId;   // Added for BR-7889 
                    a.TechPreviousOwner__c = trigger.oldMap.get(a.id).OwnerId;// Added for BR-7889 
              }
                
              
            }


            // Hari Added Start
             /*
            if((a.SVMXC__Group_Member__c !=Trigger.OldMap.get(a.id).SVMXC__Group_Member__c))
            {           
                wOrdertoProcess.add(a);        
            }
            
            if(wOrdertoProcess != null && wOrdertoProcess.size()>0){
                System.debug('*************************************** 2'+wOrdertoProcess.size());
                AP_AssignedToolsTechnicians.updateAssignedToolsTechniciansPrimaryUser(wOrdertoProcess);
            }
            */
            if((a.SVMXC__Site__c!=Trigger.OldMap.get(a.id).SVMXC__Site__c))
            { 
                PreferredFSRWoList.add(a);
            }
            // Hari Added Ended
            //Added By Deepak for Merge of WO_OwnerEmailUpdate
            if(a.SVMXC__Company__c != null && a.SVMXC__Company__c !=Trigger.OldMap.get(a.id).SVMXC__Company__c){
                accid.add(a.SVMXC__Company__c);
                workOrderList.add(a);
            }
            //Added By Deepak: to update Service Contract from Service Line (Added in April-15)
            if( a.SVMXC__Service_Contract__c !=Trigger.OldMap.get(a.id).SVMXC__Service_Contract__c){
                WorList.add(a);
            }
            
            // related to Scenario 17 
            if (a.SVMXC__Dispatch_Response__c == 'Completed' && Trigger.oldMap.get(a.id).SVMXC__Dispatch_Response__c != 'Completed' && a.SVMXC__Closed_By__c == null)
                a.SVMXC__Closed_By__c = UserInfo.getUserId();
            
            

        } 
        //Added by VISHNU C for BR 8684 Q2 Release
        if ( woidCancelled != null && woidCancelled.size() > 0 )
        {
            List<SVMXC__RMA_Shipment_Order__c> POLineCancelledList = new List<SVMXC__RMA_Shipment_Order__c>();
            //List<SVMXC__Service_Order__c> WOCancelledList = new List<SVMXC__Service_Order__c>();
            Map<id, SVMXC__RMA_Shipment_Order__c> POCancelMap = new Map<id,SVMXC__RMA_Shipment_Order__c>();
        
            POLineCancelledList = [select id,SVMXC__Service_Order__c from SVMXC__RMA_Shipment_Order__c where SVMXC__Service_Order__c in :woidCancelled and SVMXC__Order_Status__c != :'Cancelled']; 
            //WOCancelledList = [select id,SVMXC__Order_Status__c from SVMXC__Service_Order__c where id in :WOId];
            system.debug('POLineCancelledList is' +POLineCancelledList);
            
            
            if( POLineCancelledList.size() > 0 )
            {
                for (SVMXC__RMA_Shipment_Order__c po : POLineCancelledList)
                {
                    POCancelMap.put(po.SVMXC__Service_Order__c,po);
                }
            
            }
            system.debug('POCancelMap' +POCancelMap);
            if(!POCancelMap.isempty())
            {
                for(SVMXC__Service_Order__c wo:trigger.New)
                {
                    if(POCancelMap.containskey(wo.id))
                    {
                        system.debug('Inside Cancelled Validtion');
                        wo.addError('Cancel Parts Orders before cancelling the WO');
                    }
                
                }
                
            }
            //Ap_WorkOrder.IsAllPartOrderCancelled(woidCancelled);
            //List<SVMXC__RMA_Shipment_Order__c> POLineCancelled = new List<SVMXC__RMA_Shipment_Order__c>();
        }
        
        //Changes by VISHNU C Ends here
        
        /********************************************************************************************************************/
        // Scenario: 1
        // Assign the Work Orde Status and Schedule Date Time to Work Order Notification on change of Status
        // Primary Work Order should be NULL. Means the Work Order is a Child
        /********************************************************************************************************************/
        /* Commented for July 15 Release 
        if(solst.size()>0 && wonId.size()>0)
        {
            
             Code moved to after update
            for(SVMXC__Service_Order__c a:solst)
            {
                system.debug('a.Work_Order_Notification__c==='+a.Work_Order_Notification__c);
                WorkOrderNotification__c wn = new WorkOrderNotification__c(Id = a.Work_Order_Notification__c);      //Updating Work Order Notificaiton
                wn.Work_Order_Status__c = a.SVMXC__Order_Status__c;
                wn.Scheduled_Date_Time__c = a.SVMXC__Scheduled_Date_Time__c;
                wn.CommentstoDispatcher__c = a.Comments_to_Planner__c;
                wnlst.add(wn);
            }
            
           if(wnlst.size()>0) update wnlst;
           
        }    */
        /********************************************************************************************************************/
        // Scenario: 2
        // Change the Owner to Technician on change of Status to 'Customer confirmed' BR-7889 (Customer Confirmed to Acknowledge FSE)
        // Apply Sharing Rules to the Secondary Technician in 'Read' mode.
        /********************************************************************************************************************/    
        if(wolst.size()>0)
        {
            //Fetching Primary Technician's Salesforce User from Technician Object
            try{
            techMap = new Map<Id,SVMXC__Service_Group_Members__c>([Select Id,SVMXC__Salesforce_User__c 
                                                                     from SVMXC__Service_Group_Members__c 
                                                                    where Id IN :techId AND SVMXC__Salesforce_User__c !=NULL]);//Updated by Anita on 9/1/2011    
            }catch (Exception e){system.debug('Exception in Map=='+e);}                                                             
            system.debug('woId=='+woId+'==techId=='+techId);
            
            Set<Id> sfid = new Set<Id>();
            for(SVMXC__Service_Group_Members__c a:techMap.values())
                sfid.add(a.SVMXC__Salesforce_User__c);
            
            //Fetching ServiceMax Events for the Secondary Technician
            try
            {
                Eventlst = [Select Id,WhatId,OwnerId from Event where WhatId IN :woId and OwnerId NOT IN :sfid];
                system.debug('Event='+Eventlst );
            }
            catch (Exception e){system.debug('Exceptoin==='+e);}
            if(Eventlst.size()!=0)
            {
                for(Event a:Eventlst)
                {
                    Set<Id> ownId = new Set<Id>();
                    if(EventMap.containsKey(a.WhatId))
                    {
                        ownId.addAll(EventMap.get(a.WhatId));
                        ownId.add(a.OwnerId);
                        EventMap.put(a.WhatId,ownId);
                    }
                    else
                    {
                        ownId.add(a.OwnerId);
                        EventMap.put(a.WhatId,ownId);
                    }
                }  
            }          
            system.debug('EventMap=='+EventMap);   
            
            if( techMap.size() > 0)   //Added by Anita on 9/1/2011    
            {  
                for(SVMXC__Service_Order__c a:wolst)
                {
                    system.debug('a.SVMXC__Group_Member__c=='+ a.SVMXC__Group_Member__c);
                    if(a.SVMXC__Group_Member__c!= NULL && techMap.containsKey(a.SVMXC__Group_Member__c) && a.SVMXC__Group_Member__c == techMap.get(a.SVMXC__Group_Member__c).Id && 
                        techMap.get(a.SVMXC__Group_Member__c).SVMXC__Salesforce_User__c != Null)            //Updated by Anita on 9/1/2011                
                            a.ownerId = techMap.get(a.SVMXC__Group_Member__c).SVMXC__Salesforce_User__c;    //Assignment of Work Order Technician as Owner               
                            
                }
            }            
            if(woShareLst.size()>0) 
            {
                try{
                    insert woShareLst;                                                                              //Inserting Work Order Share details
                }catch (Exception e){system.debug('Testing error'+e.getmessage());}
            }
        }    
        /**********************************************************************************************************************************/
        // Scenario: 3
        // Assign the Work Orde ownership to Service Center Queue when NEW Work Order is created or Work Order status = ‘Service Complete
        /***********************************************************************************************************************************/
        List<SVMXC__Territory__c> terList = New List <SVMXC__Territory__c>();
        if(wolst3.size()>0)
        {
            //Fetching Primary Territory 's Salesforce User 
            terMap= [Select id,Name,OwnerId
                     From SVMXC__Territory__c
                     Where Id IN :woterId ];
                      
            for(SVMXC__Territory__c wtr:terMap) 
            {
                for(SVMXC__Service_Order__c svo3:wolst3)
                {
                    if(svo3.SVMXC__Primary_Territory__c == wtr.Id)
                    {
                        //commented for April Release 2014 keeping this funtion in After update
                       svo3.OwnerId=wtr.OwnerId;            //Assignment of Primary Terrotiry's owner as Work Order owner
                        wolstnew3.add(svo3);
                    }
                }
            } 
            
            for(SVMXC__Service_Order__c svo3:wolst3)
            {       
                if(trigger.oldMap.get(svo3.Id).SVMXC__Group_Member__c != trigger.newMap.get(svo3.Id).SVMXC__Group_Member__c ||
                   trigger.oldMap.get(svo3.Id).SVMXC__Scheduled_Date_Time__c!= trigger.newMap.get(svo3.Id).SVMXC__Scheduled_Date_Time__c
                    && svo3.SVMXC__Order_Status__c == 'Rescheduled')
                {
                   svo3.SVMXC__Order_Status__c = 'Scheduled';
                   svo3.RescheduleReason__c = '';
                   svo3.RescheduleReason__c = '';
                }
            }                                           
        }    
        /***********************/
        // Scenario: 3a Starts
        /***********************/
                
        if(wolst3a.size()>0)
        {
            /*
            techmap2=[select id,Name,Manager__c   
                     From SVMXC__Service_Group_Members__c 
                     where Id IN :wotech3Id];
                     
            for(SVMXC__Service_Group_Members__c sct :techmap2)
            {
                for(SVMXC__Service_Order__c svo3a :wolst3a) 
                 {
                     //Added for Oct 2013 Release
                    if(svo3a.SVMXC__Group_Member__c==sct.id && svo3a.ApprovalManager__c != null )
                    {                   
                        svo3a.OwnerId=svo3a.ApprovalManager__c;
                        wolstnew3.add(svo3a);
                    }//End: Oct 2013 Release
                    else if(svo3a.SVMXC__Group_Member__c==sct.id && sct.Manager__c != null )
                    {                   
                        svo3a.OwnerId=sct.Manager__c;
                        wolstnew3.add(svo3a);
                    }
                    
                 }
            } */ 
            String woString = JSON.serialize(wolst3a);
            Ap_WorkOrder.ServicecompletedWOOwnership(woString,wotech3Id);     
        } 
        
        
        
        /**********************************************************************************************************************************/
        // Scenario: 3c
        // Added by: Anand
        // Date: 03/03/2015
        // Purpose:Work order Validation
        /***********************************************************************************************************************************/
       /* if(Wovalidset!= null && Wovalidset.size()>0){
          Ap_WorkOrder.WOAutoValidation(Wovalidset);
          }  */
             
        /***********************/
        // Scenario: 4a •   Site Authorization
        /***********************/           
        if(woId4a.size()>0)     
        {       
            AP28_WorkOrderLocalizedNotification.sendEmailNotification(woId4a, 1);
        }     
        /***********************/
        // Scenario: 4b •   Customer Confirmation – 1 Week before scheduled date.
        /***********************/  
                
        if(woId4b.size()>0){     
            //woEmailMap = AP28_WorkOrderLocalizedNotification.sendEmailNotification(woId4b, 2);
            //system.debug('YTI: '+woEmailMap);
            AP28_WorkOrderLocalizedNotification.sendEmailNotification(woId4b, 2);
            
        }
         /***********************/
        // Scenario: 4c •   Account Owner Notification – 1 Week before scheduled date.
        /***********************/           
            
        //if(woId4c.size()>0) {}    
            //AP28_WorkOrderLocalizedNotification.sendEmailNotification(woId4c, 3);  
        
        // BR-10089: redesign customer notif (Y. Tisserand)
        if(wolst4.size() > 0){
            for(SVMXC__Service_Order__c wo4:wolst4){
                if(woEmailMap.containsKey(wo4.id)){
                    wo4.TECH_CustomerNotificationSubject__c = woEmailMap.get(wo4.id).TECH_CustomerNotificationSubject__c;
                    wo4.TECH_CustomerNotification__c = woEmailMap.get(wo4.id).TECH_CustomerNotification__c;
                }
            }
        }
        
        /***********************/
        // Scenario: 5 •   Update customer Location.
        /***********************/
        if(wolst5.size()>0) 
        {    
        /*    list<CustomerLocation__c> custloc = [select City__c,Country__c,ZipCode__c,SVMX_Country__c, 
            StateProvince__c,AddressLine2__c,AddressLine1__c, SiteSpecificComments__c,SVMX_State__c,
            Time_Zone__c,RelatedAccount__r.Name From CustomerLocation__c Where Id IN :wocustloc];
                 
            for(CustomerLocation__c cl :custloc )   
            {
                for(SVMXC__Service_Order__c svo4:wolst5)
                {
                    if(svo4.Customer_Location__c == cl.Id)
                    {
                        svo4.SVMXC__Street__c = cl.AddressLine1__c;
                        svo4.AddressLine2__c = cl.AddressLine2__c;
                        svo4.SVMXC__City__c = cl.City__c;
                        svo4.SVMXC__State__c = cl.SVMX_State__c;
                        svo4.SVMXC__Zip__c = cl.ZipCode__c;
                        svo4.SVMXC__Country__c = cl.SVMX_Country__c;
                        svo4.SiteSpecificComments__c = cl.SiteSpecificComments__c;
                        svo4.SVMXC__Locked_By_DC__c = false;
                        svo4.CustomerLocationAccount__c = cl.RelatedAccount__r.Name;
                        if( svo4.CustomerTimeZone__c ==  null)
                            svo4.CustomerTimeZone__c = cl.Time_Zone__c;
                    }   
                }   
            }        
        */
         list<SVMXC__Site__c> loc = [select SVMXC__City__c,LocationCountry__c,LocationCountry__r.name,SVMXC__Zip__c,
         StateProvince__c,StateProvince__r.name,AddressLine2__c,SVMXC__Street__c, SiteSpecificComments__c,
         TimeZone__c,SVMXC__Account__r.Name,SVMXC__Longitude__c,SVMXC__Latitude__c From SVMXC__Site__c Where Id IN :woloc];
               
            for(SVMXC__Site__c cl :loc )   
            {
                for(SVMXC__Service_Order__c svo4:wolst5)
                {
                     
                    if(svo4.SVMXC__Site__c == cl.Id)
                    {
                        
                        svo4.SVMXC__Street__c = cl.SVMXC__Street__c;
                        svo4.AddressLine2__c = cl.AddressLine2__c;
                        svo4.SVMXC__City__c = cl.SVMXC__City__c;
                        svo4.SVMXC__State__c = cl.StateProvince__r.name;
                        svo4.SVMXC__Zip__c = cl.SVMXC__Zip__c;
                        svo4.SVMXC__Country__c = cl.LocationCountry__r.name;
                        svo4.SiteSpecificComments__c = cl.SiteSpecificComments__c;
                        svo4.SVMXC__Locked_By_DC__c = false;
                        svo4.CustomerLocationAccount__c = cl.SVMXC__Account__r.Name;
                        svo4.SVMXC__Latitude__c = cl.SVMXC__Latitude__c;
                        svo4.SVMXC__Longitude__c = cl.SVMXC__Longitude__c;
                        if( svo4.CustomerTimeZone__c ==  null)
                            svo4.CustomerTimeZone__c = cl.TimeZone__c;
                    }   
                }   
            }
        }
        /**********************************************************************************************************************************/
        // Scenario: 6
        // Update Work Order ownership to Service Center Queue[Primary Territory's Owner]
        /***********************************************************************************************************************************/
        if(wolst6.size()>0)
        {
            terList= [Select id,Name,OwnerId
                     From SVMXC__Territory__c
                     Where Id IN :woprytry ];
                     for(SVMXC__Territory__c wtr:terList)   
            
            for(SVMXC__Service_Order__c svo3:wolst6)
            {
                if(svo3.SVMXC__Primary_Territory__c == wtr.Id)
                {
                    svo3.OwnerId=wtr.OwnerId;     //Assignment of Primary Terrotiry's owner as Work Order owner
                    
                }
            }
                
        }
     /**********************************************************************************************************************************/
    // Scenario: 7
    // Case Owner is Updated
    /***********************************************************************************************************************************/
        if(wolst7.size()>0)
        {
            caseown = [select OwnerId,Owner.Name From case Where Id IN :wocase];
            for(case c :caseown)   
            {
                for(SVMXC__Service_Order__c svo5:wolst7)
                {
                    if(svo5.SVMXC__Case__c == c.Id)
                    {
                        svo5.CaseOwner__c = c.Owner.Name;
                    }   
                }   
            }  
          
        } 
     /**********************************************************************************************************************************/
    // Scenario: 8
    // Contact Phone is Updated
    /***********************************************************************************************************************************/
        if(wolst8.size()>0)
        {
            contactphone = [select MobilePhone,WorkPhone__c From contact Where Id IN :wocontact];
            for(contact cn :contactphone)   
            {
                for(SVMXC__Service_Order__c svo6:wolst8)
                {
                    if(svo6.SVMXC__Contact__c == cn.Id)
                    {
                        if(cn.MobilePhone != null )
                        {
                            svo6.Contact_Phone__c = cn.MobilePhone;
                        }
                        else if(cn.WorkPhone__c !=null)
                        {
                            svo6.Contact_Phone__c = cn.WorkPhone__c;
                        }
                    }   
                }   
            }  
          
        } 
    /**********************************************************************************************************************************/
    // Scenario: 9
    // Account Type is Updated
    /***********************************************************************************************************************************/
        if(wolst9.size()>0)
        {
            accntype = [select Type From account Where Id IN :woaccnt];
            for(account ac :accntype)   
            {
                for(SVMXC__Service_Order__c svo7:wolst9)
                {
                    if(svo7.SVMXC__Company__c == ac.Id)
                    {
                        if(ac.Type== 'GSA')
                            svo7.AccountType__c = Label.CLDEC12SRV13;
                        else
                            svo7.AccountType__c = '';
                    }   
                }   
            }  
          
        }
        
    /**********************************************************************************************************************************/
    // Scenario: 10
    // Owner Email Field Updated
    /***********************************************************************************************************************************/   
      
    if(wolst1.size()>0)
        {
            //user1 = new Map<Id,User>([SELECT Id ,Email FROM User WHERE Id in :woOwnerid]);
            //users = [SELECT Id ,Email FROM User WHERE Id in :woOwnerid ];
            //for(User u1:user1.values())   
            for(User u1:[SELECT Id ,Email FROM User WHERE Id in :woOwnerid])
            {
                for(SVMXC__Service_Order__c  wo1 :wolst1)
                {
                    wo1.TechOwnerEmail__c=u1.Email; 
                } 
            }
        }     
    
    
        for(SVMXC__Service_Order__c wo:trigger.New)
        {
             if(wo.SoldToAccount__c !=null && wo.SoldToAccount__c <> trigger.oldMap.get(wo.Id).SoldToAccount__c)
             {
                 wolist.add(wo);
                 
             }
        }
        
        if(wolist != null && wolist.size()>0)
            AP_WOBeforInsertUpdateClass.Ship_BillToAccountupdate(wolist);

        /*
            Scenario: 11
            Added by: Ramu Veligeti
            Date: 23/07/2013
            Delete Salesforce & SVMX Events when order status is changed from 'Customer Confirmed' to 'Unscheduled'
        */
        Set<Id> soId = new Set<Id>();
        List<SVMXC__Service_Order__c> slst = new List<SVMXC__Service_Order__c>();
        for(SVMXC__Service_Order__c a:trigger.new)
        {
            if((a.SVMXC__Order_Status__c == 'Unscheduled' && a.SVMXC__Order_Status__c <> trigger.oldMap.get(a.Id).SVMXC__Order_Status__c 
            && (trigger.oldMap.get(a.Id).SVMXC__Order_Status__c == 'Customer Confirmed'|| 
                trigger.oldMap.get(a.Id).SVMXC__Order_Status__c == 'Acknowledge FSE')) || 
               ((trigger.NewMap.get(a.id).SVMXC__Order_Status__c <> trigger.oldMap.get(a.Id).SVMXC__Order_Status__c) && a.SVMXC__Order_Status__c == 'Cancelled' ))
            {
                soId.add(a.Id);
                slst.add(a);
            }
        }
        
        if(soId.size()>0)
        {
            List<Event> Evntlst = [Select Id from Event where WhatId IN :soId];
            if(Evntlst.size()>0) delete Evntlst;
            
            List<SVMXC__SVMX_Event__c> svmxent = [Select Id from SVMXC__SVMX_Event__c where SVMXC__WhatId__c IN :soId];
            if(svmxent.size()>0) delete svmxent;
            
           // AP_AssignedToolsTechnicians.updateAssignedToolsTechniciansStatus(soId,'Cancelled');
            
            //for(SVMXC__Service_Order__c a: slst) a.Rescheduled__c = true;
        }

    /**********************************************************************************************************************************/
    // Scenario: 12
    // Added by: Ramu Veligeti
    // Date: 30/07/2013
    // Consolidating Certifications (Habilitation Requirements) on the Work Order.
    /***********************************************************************************************************************************/
        Set<Id> locId = new Set<Id>();
        Set<String> BU = new Set<String>();
        Set<String> CN = new Set<String>();
        List<SVMXC__Service_Order__c> solsts = new List<SVMXC__Service_Order__c>();
        for(SVMXC__Service_Order__c wo: trigger.new)
        {
            //if(wo.SVMXC__Site__c!=null && wo.Service_Business_Unit__c!=null && (trigger.oldMap.get(wo.Id).SVMXC__Site__c != wo.SVMXC__Site__c || trigger.oldMap.get(wo.Id).Service_Business_Unit__c != wo.Service_Business_Unit__c))
            if(wo.SVMXC__Site__c!=null && wo.SVMXC__Country__c!=null && (trigger.oldMap.get(wo.Id).SVMXC__Site__c != wo.SVMXC__Site__c 
            || trigger.oldMap.get(wo.Id).SVMXC__Country__c != wo.SVMXC__Country__c))
            {
                locId.add(wo.SVMXC__Site__c);
                //BU.add(wo.Service_Business_Unit__c);
                CN.add(wo.SVMXC__Country__c);
                solsts.add(wo);
            }
        }
        
        if(locId.size()>0 && BU.size()>0) ServiceMaxUpdateSkillsOnWO.HabilitationRequirement(locId,BU,solsts);
        if(locId.size()>0 && CN.size()>0) ServiceMaxUpdateSkillsOnWO.HabilitationRequirement(locId,CN,solsts);
        
        /**********************************************************************************************************************************/
        // Scenario: 13
        // Added by: Hari Krishna Singara
        // Date: 26/09/2013
        // Update Preferred FSR on Work Order Base on Location/Account..
        /***********************************************************************************************************************************/

          
        
        if(PreferredFSRWoList != null && PreferredFSRWoList.size()>0)
        {
             Ap_WorkOrder.WOPreferedTechnician(PreferredFSRWoList );
        }   
     ServiceMaxWOG.UpdateWOG(trigger.new,trigger.oldMap); 
     
     
     
        /*
        @Author : Bala Candassamy
        */
        /* Create a list of workorders*/
        List<SVMXC__Service_Order__c> woupdatelist = New List<SVMXC__Service_Order__c>();
        
        for(SVMXC__Service_Order__c a:trigger.new){
           if(trigger.oldMap.get(a.Id).Tech_Workordercountry__c != a.Tech_Workordercountry__c){
             woupdatelist.add(a); 
           } 
        }
        
        if(woupdatelist != null && woupdatelist.size()>0){
            AP_WorkOrderHandler.fillInFooter(woupdatelist);
        }


     
  
    
        /**********************************************************************************************************************************
        Modified By- Deepak
        April Release-14
        Purpose-To Track WO FSR and Customer Time Zone. 
        **********************************************************************************************************************************/
       // Set<Id> lId = new Set<Id>();
        Set<Id> fId = new Set<Id>();
        List<SVMXC__Service_Order__c> wol = new List<SVMXC__Service_Order__c>();
        
        for(SVMXC__Service_Order__c wo: trigger.new)
        {
            if(wo.CustomerTimeZone__c!=null && wo.SVMXC__Group_Member__c!=null && wo.SVMXC__Scheduled_Date_Time__c!=null && trigger.Oldmap.get(wo.Id).SVMXC__Scheduled_Date_Time__c !=
            trigger.Newmap.get(wo.Id).SVMXC__Scheduled_Date_Time__c )
            {
               // lId.add(wo.SVMXC__Site__c);
                fId.add(wo.SVMXC__Group_Member__c);
                wol.add(wo);
            }
        }
        if(wol.size()>0 && fId.size()>0)
        {
            Ap_WorkOrder.WOScheduledTimeZone(wol,fId);  
        }
        
    //Calling Class and Method WO_OwnerEmailUpdate(Added in April-15)
        if(accid!=null && workOrderList.size()>0 && accid.size()>0){
            Ap_WorkOrder.WO_OwnerEmailUpdate(workOrderList,accid);
        
        }
     //Calling Class ato update Service Contract from Service Line (Added in April-15)(Added in April-15)
         if(WorList.size()>0){
            Ap_WorkOrder.SerContFieldUpdate(WorList);
        }
        
        
        /**************************************************************
        * @author Adrian MODOLEA (adrian.modolea@servicemax.com)
        * @date 03/02/2016 (dd/mm/yyyy)
        * @description Apex class for automatically calculate work order's Primary Territory using Territory Coverage definition
        * @NOTE THIS FUNCTION REPLACES THE STANDARD SERVICE MAX FUNCTIONALITY !
            !!! Please ensure the ServiceMax setting SET049 is set to FALSE !!!
        **********************************************************/
        if(System.Label.SVMX_BypassTerritoryMatchRules.toLowerCase() == 'false')
        {   
            System.debug('### AMO  ServiceMaxWOTerritoryAssignment on WorkOrderBeforeUpdate');  
            List<SVMXC__Service_Order__c> workOrders = new List<SVMXC__Service_Order__c>(); 
            
            for(SVMXC__Service_Order__c workOrder : Trigger.new)
            {
                if(workOrder.SVMXEligibleforTerritoryRouting__c)
                {
                    Boolean toAddWo = false;
                    Set<String> types = new Set<String>();
                    Set<String> values = new Set<String>();                     
                    System.debug('### AMO WO eligible for Territory Assignment');                   
                    for(String fieldType : ServiceMaxWOTerritoryAssignment.attributesMap.keySet())
                    {
                        String fieldName = ServiceMaxWOTerritoryAssignment.attributesMap.get(fieldType);
                        String fieldValue = (String) workOrder.get(fieldName);  
                        
                        System.debug('### AMO fieldName ' + fieldName);
                        System.debug('### AMO fieldType ' + fieldType);
                        System.debug('### AMO fieldValue ' + fieldValue);
                        
                        if(fieldValue != null) {
                            types.add(fieldType);
                            values.add(fieldValue);  
                        }
                                               
                        if(workOrder.get(fieldName) != Trigger.oldMap.get(workOrder.Id).get(fieldName))
                            toAddWo = true;
                    }
                    
                    if(toAddWo)
                    {
                        System.debug('### AMO WO added for Territory Assignment');
                        ServiceMaxWOTerritoryAssignment.fieldTypes.addAll(types);
                        ServiceMaxWOTerritoryAssignment.fieldValues.addAll(values);  
                        workOrders.add(workOrder);
                    }
                }
            } 
            
            System.debug('### AMO ' + workOrders.size() + ' WOs for Territory Assignment');   
            
            if(workOrders.size() > 0)   
                ServiceMaxWOTerritoryAssignment.assignTerritory(workOrders);       
           
        }//bypass
    }
}