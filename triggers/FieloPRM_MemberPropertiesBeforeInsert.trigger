/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: 
********************************************************************/

trigger FieloPRM_MemberPropertiesBeforeInsert on MemberExternalProperties__c (Before Insert) {
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberPropertiesBeforeInsert')){
        
        if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberPropertiesBeforeInsertSearchMember')){
            FieloPRM_AP_MemberPropertiesTriggers.searchMember();
        }
        
        if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberPropertiesBeforeInsertCheckStatus')){
            FieloPRM_AP_MemberPropertiesTriggers.checkMemberPropertiesStatus(trigger.New);
        }
    }
}