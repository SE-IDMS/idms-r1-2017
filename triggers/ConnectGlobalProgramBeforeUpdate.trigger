/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 03-Feb-2012
    Description     : Connect Team Members Before Update event trigger.
*/

trigger ConnectGlobalProgramBeforeUpdate on Project_NCP__c (before update) {

  System.Debug('****** GlobalProgramBeforeUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('GlobalProgramBeforeUpdate '))
    {
    
    if((Trigger.new[0].User__c != Trigger.old[0].User__c) || (Trigger.new[0].Program_Leader_2__c != Trigger.old[0].Program_Leader_2__c) || (Trigger.new[0].Program_Leader_3__c != Trigger.old[0].Program_Leader_3__c))
      ConnectGlobalProgramTriggers.ValidateCorporateKPIConnectGlobalProgramBeforeUpdate(Trigger.new, Trigger.old);
    
     if((Trigger.new[0].Global_Functions__c != Trigger.old[0].Global_Functions__c)|| (Trigger.new[0].Global_Business__c != Trigger.old[0].Global_Business__c) || (Trigger.new[0].GSC_Regions__c != Trigger.old[0].GSC_Regions__c) || (Trigger.new[0].Partner_Region__c != Trigger.old[0].Partner_Region__c) ||(Trigger.new[0].Power_Region__c != Trigger.old[0].Power_Region__c)|| (Trigger.new[0].Smart_Cities_Division__c != Trigger.old[0].Smart_Cities_Division__c)){
     
         ConnectGlobalProgramTriggers.ConnectGlobalProgramBeforeUpdate(Trigger.new,Trigger.old);
         ConnectGlobalProgramTriggers.ConnectGlobalProgramBeforeUpdate_CheckDeploymentNetwork(Trigger.new,Trigger.old);
         ConnectGlobalProgramTriggers.ConnectGlobalProgramNetworkValidationBeforeUpdate(Trigger.new);
         
        if (Trigger.new[0].ValidateScopeonUpdate__c == True)
        
                Trigger.new[0].AddError(Label.ConnectGlobalProgramScopeValidateERR);  
          }              
        
 }
 System.Debug('****** GlobalProgramBeforeUpdate Trigger End ****'); 
}