/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 03-Feb-2012
    Description     : Connect Cascading Milestones Before Insert event trigger.
*/

trigger ConnectCascadingMilestoneBeforeInsert on Cascading_Milestone__c (before insert) {

  System.Debug('****** ConnectCascadingMilestoneBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CascadingMilestoneBeforeInsert'))
    {
        ConnectCascadingMilestoneTriggers.CascadingMilestoneBeforeInsertUpdate(Trigger.new);
        ConnectCascadingMilestoneTriggers.CascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity(Trigger.New);
           if (Trigger.new[0].ValidateScopeonInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_Cascading_Milestone_InsertUpdate_ERR1);
 }
 System.Debug('****** ConnectCascadingMilestoneBeforeInsert Trigger End ****'); 
}