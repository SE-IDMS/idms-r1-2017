/****************
To Update Coountry Values if Project is deleted 
*****************/

trigger ProjectBeforeDelete on Milestone1_Project__c (before delete ,after Delete) {

    if(trigger.IsBefore && trigger.IsDelete){
        //AP_COIN_bFO_OfferProject.UpdateOfferCountryOnDelete(trigger.old);
    }
    set<id> olcids = new set<id>();
     if(trigger.IsAfter && trigger.IsDelete){
        for(Milestone1_Project__c mil:trigger.old){
            olcids.add(mil.Offer_Launch__c);
        }
     }
     if(olcids.size()>0){
        AP_COIN_bFO_OfferProject.UpdateTechCountryOnPjctDelete(olcids);
     }

}