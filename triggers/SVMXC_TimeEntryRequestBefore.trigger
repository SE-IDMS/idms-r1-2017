trigger SVMXC_TimeEntryRequestBefore on SVMXC_Time_Entry_Request__c (before delete, before insert, before update) {

    if (trigger.isInsert || trigger.isUpdate) {
        SVMXC_TimeEntryRequestBefore.ensureTechIsOwner(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
        SVMXC_TimeEntryRequestBefore.setTechWhenNull(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
        //SVMXC_TimeEntryRequestBefore.checkForAllDayEvent(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
        SVMXC_TimeEntryRequestBefore.setAllDayDates(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
    }
}