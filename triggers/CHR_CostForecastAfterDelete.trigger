/*
    Author          : Priyanka Shetty (Schneider Electric)
    Date Created    : 12/04/2013
    Description     : Change Request Costs Forecasts After Delete event trigger.
*/
trigger CHR_CostForecastAfterDelete on ChangeRequestCostsForecast__c (After Delete) {
    if(Utils_SDF_Methodology.canTrigger('ChangeRequestCostForecastTriggerAfterDelete'))
    {
       CHR_CostForecastTriggers.CRCostForecastAfterDelete(Trigger.old);
    }
}