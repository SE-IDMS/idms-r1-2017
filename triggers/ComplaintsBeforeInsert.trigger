/*
    Author        : Srikant Joshi 
    Date Created  : 30/07/2013
    Description   : Complaints/Request Before Insert trigger.
    
*/
trigger ComplaintsBeforeInsert on ComplaintRequest__c (Before Insert) {
if(Utils_SDF_Methodology.canTrigger('AP_LCR'))
   {
        
     List<ComplaintRequest__c> listStatusChangedToClosed = new List<ComplaintRequest__c>{};
     List<ComplaintRequest__c> setDueDatelist = new List<ComplaintRequest__c>{};
     
     for(ComplaintRequest__c compReq : Trigger.New)
     {    
                compReq.LastUpdated__c = System.now();
                compReq.LastUpdatedby__c = UserInfo.getUserId();
         if(compReq.Status__c == Label.CLDEC13LCR06)
         {
             listStatusChangedToClosed.add(compReq); 
         }
         compReq.TECH_DueDateForNotifications__c=compReq.TECH_DueDate__c;//Dec 2016 Release BR-10560
                  
         // OCT 2015 - Added by Uttara - BR-7566
         if(compReq.TECH_DueDate__c == null) {
             setDueDatelist.add(compReq);
         }  
           
     }     
    
    // OCT 2015 - Added by Uttara - BR-7566
    if(!setDueDatelist.isEmpty()) {
         AP_LCR.setDueDate(setDueDatelist);
     } 
     
     AP_LCR.checkExternalOrInternalLCR(trigger.new);
     // APRIL2014 RELEASE - Comment the code - We need to allow the change of Reporting Entity during creation. - 04-MAR-2014
     //AP_LCR.validateReportingEntityOnCreation(trigger.new);
     
     // To update accountable Org during creation.
     // APRIL2014 RELEASE - Sukumar Salla - 12Feb2014
     
     System.debug('@@@ Before Insert Trigger called');
     AP_LCR.updateAccountableOrgForInternalCR(trigger.new);
     // October2014 RELEASE - Ram chilukuri
     list<ComplaintRequest__c> listold=new list<ComplaintRequest__c>(); 
      AP_LCR.populatePlant(trigger.new,listold);
      // April 2016 RELEASE - Swapnil Saurav BR-9614
      AP_LCR.updateInternalCategoryAndInternalReason(Trigger.New);
      // April 2016 RELEASE - END
      
 //     AP_LCR.updateCRReceived(trigger.new);
     // AP_LCR.updateCasefieldsBasedOnCR(trigger.new); 
    // AP_LCR.checkIfAllCRAreClosed(listStatusChangedToClosed)  ; - This is used to update the case status to closed - Not in Use
    for(ComplaintRequest__c temprecord:Trigger.new)
            {
                if(temprecord.comments__c == null || temprecord.comments__c == '<br>')
                { 
                      //temprecord.comments__c.addError('Please select comment');
                      temprecord.TECH_IsBlankComment__c=true;
                 
                }
                else {
                    temprecord.TECH_IsBlankComment__c=false;
                }
            }
   }
   
   
      
}