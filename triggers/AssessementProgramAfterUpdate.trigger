/****************************************************************************************************************************

    Author       : Sreenivas Sake
    Created Date : 23 July 2014
    Description  : 1. Add Assessment to ORF,Account Assigned Program.     
***************************************************************************************************************************/

trigger AssessementProgramAfterUpdate on AssessementProgram__c (after update) {
map<String,set<Id>> mapProgamLevel = new map<String,set<Id>>();
    for(AssessementProgram__c assessment : trigger.new)
    {
        if(assessment.AutomaticAssignment__c)
        {
            AssessementProgram__c oldAssessment = trigger.oldMap.get(assessment.Id);
            if(assessment.PartnerProgram__c != null && assessment.PartnerProgram__c != oldAssessment.PartnerProgram__c)
            {
                if(!mapProgamLevel.containsKey(assessment.PartnerProgram__c))
                        mapProgamLevel.put(assessment.PartnerProgram__c+':'+assessment.ProgramLevel__c , new set<Id> {(assessment.Assessment__c)});
                else
                    mapProgamLevel.get(assessment.PartnerProgram__c+':'+assessment.ProgramLevel__c).add(assessment.Assessment__c);
            }
            else if(assessment.ProgramLevel__c != null && assessment.ProgramLevel__c != oldAssessment.ProgramLevel__c)
            {
                if(!mapProgamLevel.containsKey(assessment.PartnerProgram__c))
                        mapProgamLevel.put(assessment.PartnerProgram__c+':'+assessment.ProgramLevel__c , new set<Id> {(assessment.Assessment__c)});
                else
                    mapProgamLevel.get(assessment.PartnerProgram__c+':'+assessment.ProgramLevel__c).add(assessment.Assessment__c);
            }
        }
    }

    if(!mapProgamLevel.isEmpty())
        AP_ASM_AddAssessment.addProgramAssessment(mapProgamLevel);
}