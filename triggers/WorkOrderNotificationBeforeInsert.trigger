trigger WorkOrderNotificationBeforeInsert on WorkOrderNotification__c (before insert) {
if(Utils_SDF_Methodology.canTrigger('SRV02')) 
{   
  for(WorkOrderNotification__c workOrderNotif: Trigger.new){
  
      // Case references auto-population on the WON
      List<CSE_ExternalReferences__c> externalRefs = [select Type__c, ClientReference__c from CSE_ExternalReferences__c where Case__c = :workOrderNotif.case__c];
          
      for(CSE_ExternalReferences__c externalRef: externalRefs)
      {
          
        if(workOrderNotif.ContractReference__c == null && externalRef.Type__c == 'Contract')
        {
              System.debug('#### WorkOrderNotificationBeforeInsert - Contract reference');
              workOrderNotif.ContractReference__c = externalRef.ClientReference__c;
        }
        
        if(workOrderNotif.WarrantyReference__c == null && externalRef.Type__c == 'Warranty')
        {
              System.debug('#### WorkOrderNotificationBeforeInsert - Warranty reference');
              workOrderNotif.WarrantyReference__c = externalRef.ClientReference__c;
        }
      }

      
      // Services Business Unit defaulting from the Product BU
      if(workOrderNotif.ServicesBusinessUnit__c == null)
      {
          List<Case> cases = [select ProductBU__c from Case where id = :workOrderNotif.case__c];
          
          for(Case myCase: cases){
              if(myCase.ProductBU__c == 'POWER')
                workOrderNotif.ServicesBusinessUnit__c = 'PW';
              else if(myCase.ProductBU__c == 'INDUSTRY')
                workOrderNotif.ServicesBusinessUnit__c = 'ID'; 
              else if(myCase.ProductBU__c == 'BUILDINGS')
                workOrderNotif.ServicesBusinessUnit__c = 'BD';
              else if(myCase.ProductBU__c == 'IT')
                workOrderNotif.ServicesBusinessUnit__c = 'IT';
              else if(myCase.ProductBU__c == 'ENERGY')
                workOrderNotif.ServicesBusinessUnit__c = 'EN';
          }
      }
/*     
@Author: Jyotiranjan Singhlal
Created Date: 07-11-2012
Description: This trigger fires on Work Order Notification on Before Insert
**********
Scenarios:
**********
1.  Case Owner is Updated 
2.  Customer Location is Updated
3.  Customer Location logic changed by Location

Date Modified       Modified By         Comments
------------------------------------------------------------------------
07-11-2012          Jyotiranjan Singhlal    Logic to update Work Order notification Case Owner field if Case is changed
20-11-2012          Jyotiranjan Singhlal    Logic to update Work Order notification Time Zone if CL is changed
11-04-2013          Jyotiranjan Singhlal    Customer Location logic (scenario 2) changed by Location
*/
    /**********************************************************************************************************************************/
    // Scenario: 1
    // Case Owner is Updated
    /***********************************************************************************************************************************/
      List<case> caseown = new list <case>();
      Set<id> woncases = New Set<id>();
      List<WorkOrderNotification__c> wonlst = new List<WorkOrderNotification__c>();
      
      if(workOrderNotif.Case__c != null)
      {
          wonlst.add(workOrderNotif);
          woncases.add(workOrderNotif.Case__c);
      }
      if(wonlst.size()>0)
      {
         caseown = [select OwnerId,Owner.Name,CustomerRequest__c,Case.ContactId from Case where id IN :woncases];
         
         for(case c: caseown)
         {
             for(WorkOrderNotification__c won: wonlst)
             {
                 if(won.Case__c == c.Id)
                 {
                     won.CaseOwner__c = c.Owner.Name;
                     //won.ServiceRequestReason__c = c.CustomerRequest__c;
                     //if(won.OnSiteContactSameasCaseContact__c == 'Yes')
                     //won.OnSiteContact__c = c.ContactId;
                 }      
             }
         }
      }
      
    /**********************************************************************************************************************************/
    // Scenario: 2
    // Customer Location is Updated(Location swapped with Customer Location)
    /***********************************************************************************************************************************/
      List<SVMXC__Site__c> custloc = new list <SVMXC__Site__c>();
      Set<id> woncl = New Set<id>();
      List<WorkOrderNotification__c> wonlst1 = new List<WorkOrderNotification__c>();
      
      if(workOrderNotif.Location__c != null)
      {
          wonlst1.add(workOrderNotif);
          woncl.add(workOrderNotif.Location__c);
      }
      if(wonlst1.size()>0)
      {
         custloc = [select TimeZone__c from SVMXC__Site__C where id IN :woncl];
         
         for(SVMXC__Site__C cl: custloc)
         {
             for(WorkOrderNotification__c wcl: wonlst1)
             {
                 if(wcl.Location__c == cl.Id)
                 {
                     if(wcl.TimeZone__c == null)
                         wcl.TimeZone__c = cl.TimeZone__c;
                 }      
             }
         }
      }
    }
  }
}