/*
Author          : Chris Hurd (ServiceMax)
Date Created    : 12-April-2013

Screnarios:
1.  Populate Work Order from part order
*/
trigger PartsOrderLineBeforeInsert on SVMXC__RMA_Shipment_Line__c (before insert,before update) {
    
    Set<Id> poIdSet = new Set<Id>();
    
    set<id>ipset= new set<id>();
    set<id> poset= new set<id>();
    list<SVMXC__RMA_Shipment_Line__c> polist=new list<SVMXC__RMA_Shipment_Line__c> ();
    list<SVMXC__RMA_Shipment_Line__c> polist1=new list<SVMXC__RMA_Shipment_Line__c> ();
    list<SVMXC__Installed_Product__c> iplist= new list<SVMXC__Installed_Product__c> ();
    list<SVMXC__Service_Order_Line__c> wolist= new list<SVMXC__Service_Order_Line__c>();
    map<id,SVMXC__Installed_Product__c> ipmap= new map<id,SVMXC__Installed_Product__c>();
    
    for (SVMXC__RMA_Shipment_Line__c detail : trigger.new)
    {
        poIdSet.add(detail.SVMXC__RMA_Shipment_Order__c);
        
        if(detail.SVMXC__Serial_Number__c!= null)
        {
            ipset.add(detail.SVMXC__Serial_Number__c);
            polist.add(detail);
        }  
        if(trigger.isupdate){
            if(detail.Shipped_Qty__c!=trigger.oldmap.get(detail.id).Shipped_Qty__c||detail.ShippingStatus__c!=trigger.oldmap.get(detail.id).ShippingStatus__c){
                poset.add(detail.id);
            }
        }   
        
    }
    if(ipset!=null)
    {
        iplist= [select id,name,SVMXC__Product__c from SVMXC__Installed_Product__c where id in:ipset];
        ipmap.putall(iplist);
        for(SVMXC__RMA_Shipment_Line__c pol:polist)
        {
            pol.SVMXC__Product__c=ipmap.get(pol.SVMXC__Serial_Number__c).SVMXC__Product__c;
            
        }
    }   
    if(trigger.isinsert){  
    AP_PartsorderLine.ProductQualityAlert(trigger.new);     
        
        if (poIdSet.size() > 0)
        {
            Map<Id, SVMXC__RMA_Shipment_Order__c> poMap = new Map<Id, SVMXC__RMA_Shipment_Order__c>([SELECT ID, SVMXC__Service_Order__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id IN :poIdSet]);
            
            for (SVMXC__RMA_Shipment_Line__c detail : trigger.new)
            {
                detail.SVMXC__Service_Order__c = poMap.get(detail.SVMXC__RMA_Shipment_Order__c).SVMXC__Service_Order__c;
            }
        }
    }
    if(poset!=null)  {
        
        for(SVMXC__Service_Order_Line__c woo:[select id,Shipped_Qty__c,LastModifiedByid from SVMXC__Service_Order_Line__c where PartsOrderLine__c in:poset])
        {
            SVMXC__Service_Order_Line__c wo= new SVMXC__Service_Order_Line__c(id=woo.id);
            
            wolist.add(wo);
        }
         for(SVMXC__Service_Order_Line__c woo:[select id,Shipped_Qty__c,LastModifiedByid from SVMXC__Service_Order_Line__c where Parts_Order_Line_RMA__c in:poset])
        {
            SVMXC__Service_Order_Line__c wo= new SVMXC__Service_Order_Line__c(id=woo.id);
            
            wolist.add(wo);
        }
        
    }if(trigger.isupdate){
        if(wolist!=null){
            update wolist;
        }
    }
}