/*
    Author          : Bhuvana Subramaniyan
    Description     : Account Before delete event trigger.
*/
trigger AccountBeforeDelete on Account (before delete) 
{    
    System.Debug('****** AccountBeforedelete Trigger Start ****');  
    if(Utils_SDF_Methodology.canTrigger('AP_AccountBeforeDeleteHandler'))
    {    
        AP_AccountBeforeDeleteHandler handler = new AP_AccountBeforeDeleteHandler();
        handler.OnBeforedelete(Trigger.old,Trigger.oldMap);
    }
    System.Debug('****** AccountBeforedelete Trigger End ****');      
}