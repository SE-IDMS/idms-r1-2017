/*
Shruti Karn             Mar2013    PRM May13 Release
Srinivas Nallapati      Mar2013    PRM May13 Release    -   Level heirarchy validation
*/
trigger ProgramLevelAfterUpdate on ProgramLevel__c (after update) {
    if(Utils_SDF_Methodology.canTrigger('AP_PRL_CountryLevelUpdate') || test.isRunningTest())
    {
        AP_PRL_CountryLevelUpdate.updateCountryLevel(trigger.newMap , trigger.oldMap);
        map<Id,list<ProgramLevel__c>> mapGlobalProgramLevel = new map<Id, list<ProgramLevel__c>>();
        Set<ID> setInactivePrgLevels = new Set<ID>(); //Release APR14
        
        for(Id levelID : trigger.newMap.keySet())
        {
            if(trigger.newMap.get(levelID).recordtypeid == Label.CLMAY13PRM18 && trigger.newMap.get(levelID).levelstatus__c != trigger.oldMap.get(levelID).levelstatus__c && trigger.newMap.get(levelID).levelstatus__c == Label.CLMAY13PRM19)
            {
                if(!mapGlobalProgramLevel.containsKey(trigger.newMap.get(levelID).partnerprogram__c))
                    mapGlobalProgramLevel.put(trigger.newMap.get(levelID).partnerprogram__c , new list<ProgramLevel__c> {(trigger.newMap.get(levelID))});
                else
                    mapGlobalProgramLevel.get(trigger.newMap.get(levelID).partnerprogram__c).add(trigger.newMap.get(levelID));
            }
            
            //START: Release Apr14
            
           if(trigger.newMap.get(levelID).LevelStatus__c == System.Label.CLJUL13PRM01 && trigger.oldMap.get(levelID).LevelStatus__c != System.Label.CLJUL13PRM01 )
                setInactivePrgLevels.add(levelID); 
                               
            //END: Release APR14
            
        }
        
        if(!mapGlobalProgramLevel.isEmpty())
            AP_PRL_CountryLevelUpdate.creatCountryLevel(mapGlobalProgramLevel , trigger.newMap.keySet());
        
        //START: Release Apr14
        system.debug('******* Inactive Levels :'+setInactivePrgLevels.size());
        if(setInactivePrgLevels.size() >0)
            AP_PartnerPRG_CountryProgramMethods.deactivateAccountAssignedPrograms(setInactivePrgLevels);        
        //END: Release Apr14

    }
    
     
    
    //AP_PRL_CountryLevelUpdate.checkLevelHierarchy(trigger.new);
}