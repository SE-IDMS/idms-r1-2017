/**********************
Trigger Name MilestoneTaskStageUpdate
Description :may Release
Hanamanth

***********************/

trigger milestoneTaskStageUpdate on Milestone1_Task__c (after insert,after update) {

if(AP_Coin_bFO_Milestone1Task.isTaskByPass==false) {
    Map<Id,Milestone1_Task__c> mapMMPhase = new Map<Id,Milestone1_Task__c>();
    
    AP_Coin_bFO_Milestone1Task.updatePhaseStageOncompleteTsk(trigger.newMap);
    
    for(Milestone1_Task__c mstObj:trigger.new) {
        
        if(mstObj.Task_Stage__c!='' && mstObj.Task_Stage__c!='Not Started') {
            mapMMPhase.put(mstObj.id,mstObj);
        } 
    
    }
    if(mapMMPhase.size() > 0 ) {
        AP_Coin_bFO_Milestone1Task.updatePhaseStageOnInProgresTsk(mapMMPhase);
    }
  AP_Coin_bFO_Milestone1Task.isTaskByPass=true;  
 }   

}