trigger UserBeforeInsert on User (before Insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_UserActions'))
    {
        AP_UserActions.populateLastActivationDateforInsert(Trigger.new);        
    }
    
    
    if(Utils_SDF_Methodology.canTrigger('AP_MatchingModule')) {
        
        List<User> userList = new List<User>();
        Set<ID> ids = new Set<ID>();
        
        for(User user:Trigger.new) {
            if(user.TECH_MatchAndRelink__c  && user.contactid != null) {
                userList.add(user);
                ids.add(user.Id); 
                user.TECH_MatchAndRelink__c = false;
            }
        }   
        
        if(ids.size() > 0){         
            AP_UserMasterDataMethods.matchAndRelink(ids); 
        }
    } 
    
}