/*
BR-7383
OCT 15 RELEASE
Divya M
to get the no of related Symptoms Associated with the respective Problems. and Update on the Update on the Problem
*/
trigger RelSymptomAfterDelete on RelatedSymptom__c (After Delete) {
    if(Utils_SDF_Methodology.canTrigger('RelSymptomAfterDelete')){   
        AP_RelatedSymptom.CountRelSymptoms(trigger.old);
    }
}