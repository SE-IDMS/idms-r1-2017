trigger InstalledProductLinkAfterinsert on JctAssetLink__c (After Insert) {

set<id> ipset= new set<id>();

for(JctAssetLink__c ipl:trigger.new){

    if(ipl.LinkToAsset__c!=null && ipl.IPManageIBinFO__c==True){
    
    ipset.add(ipl.InstalledProduct__c);
    }

    }
    
    
    if(ipset.size()>0 && ipset!=null){
    Ap_Installed_Product.IBchangeMaster(ipset);
    
    }

}