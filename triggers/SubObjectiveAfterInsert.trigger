/*
     Author:         Vamsi Krishna
     Date:           18/03/2011
     Description :   Sub Objective After Insert Event Trigger    
*/

trigger SubObjectiveAfterInsert on SubObjective__c (After Insert) 
{
    System.Debug('******* SubObjective After Insert Starts *********');
    
    if(Utils_SDF_Methodology.canTrigger('AP03'))
    {
        List<SubObjective__c> subObjectives = new List<SubObjective__c>();
   
        for(integer i=0; i<Trigger.New.size(); i++)
        {  
            if(Trigger.New[i].CustomerClassificationLevel1__c != null || Trigger.New[i].CustomerClassificationLevel2__c != null || Trigger.New[i].MarketSegment__c != null || Trigger.New[i].MarketSubSegment__c != null || Trigger.New[i].Product__c != null || Trigger.New[i].Account__c != null || Trigger.New[i].IndirectExport__c != null){     
                subObjectives.add(Trigger.New[i]);                      
            }
        }
        if(subObjectives.size() > 0)
        {
            AP03_ObjectiveSettingsTrigger.updateTechFieldsofSubObjective(subObjectives);
        }
    }   
    System.Debug('******* SubObjective After Insert Ends *********'); 
        
}