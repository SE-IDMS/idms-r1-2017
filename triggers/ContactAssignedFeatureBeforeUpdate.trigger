trigger ContactAssignedFeatureBeforeUpdate on ContactAssignedFeature__c (before update) {
    if(Utils_SDF_Methodology.canTrigger('AP_CAF'))
    {
        AP_ContactAssignedFeature.checkDuplicateCAF(trigger.new);
    }
}