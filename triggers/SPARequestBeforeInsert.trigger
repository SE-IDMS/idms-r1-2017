/*
Author:Siddharth N
Purpose:
The below list of tasks are to be completed before a spa request record is inserted
    a)if the user selects a salesperson , find the relevant salesgroup and populate that information in SPARequest__c.SalesGroup__c field.
        i)if there is no relevant salesgroup information found its fine,it happens , leave it like that.
    b)if the user has selected an account, get the legacy id related to that account and populate it in SPARequest__c.BackofficeCustomerNumber__c field.
        i)if the user has entered some value in the acc no field , error out if it doesn't match with the legacy id in bFO for that account.
        ii)if the user has not selected any account, or if he selected an account and there is no legacy id for that , he has to write something in 
            the acc no field, its fair, how can we leave it blank.
    c)The SPARequest__c.Quota__c field is populated with the relevant record based on the SalesGroup__c field.                    
*/
trigger SPARequestBeforeInsert on SPARequest__c (before insert) {
//am not really sure if this canTrigger Method helps but just following the convention.
 if(Utils_SDF_Methodology.canTrigger('AP_SPARequestCalculations'))
 {
     //in the same order described above,
     List<SPARequest__c> tobeProcessedRequests=new List<SPARequest__c>();     
     List<SPARequest__c> tobeProcessedForaccno=new List<SPARequest__c>();     
     List<SPARequest__c> tobeProcessedForchannellegacynumber=new List<SPARequest__c>();
     List<SPARequest__c> tobeProcessedForThresholdDiscount=new List<SPARequest__c>();
     List<Id> spaIds=new List<Id>();

     for(SPARequest__c spareq:Trigger.New){
         if(spareq.SalesPerson__c==null)
             spareq.SalesPerson__c=UserInfo.getUserId();
         if(spareq.SPASalesGroup__c!=null)    
             tobeProcessedRequests.add(spareq); 
          //Commented if condition as part of October 2014 release since account calculation will now be applied to HK also.         
         //if(spareq.BackOfficeCustomerNumber__c==null || spareq.BackOfficeSystemID__c=='NL_SAP')    
         tobeProcessedForaccno.add(spareq);
         if(spareq.Channel__c!=null && (spareq.ChannelLegacyNumber__c==null || spareq.ChannelLegacyNumber__c==''))
             tobeProcessedForchannellegacynumber.add(spareq);
        //START added for BR-4968 October 2014 release   
         if(spareq.RequestedType__c!=System.Label.CLOCT14SLS11)
             tobeProcessedForThresholdDiscount.add(spareq); 
        // END of BR-4968 October 2014 release      
     }        
     if(Trigger.New.size()>0)
      AP_SPARequestCalculations.populateSalesGroup(Trigger.New);       
    
    if(tobeProcessedRequests.size()>0)         
    AP_SPARequestCalculations.linkQuotaRecord(tobeProcessedRequests);         
    
    if(tobeProcessedForaccno.size()>0)
    AP_SPARequestCalculations.populateBackofficeAccno(tobeProcessedForaccno);         
    
    if(tobeProcessedForchannellegacynumber.size()>0)
    AP_SPARequestCalculations.populateChannelLegacyNumber(tobeProcessedForchannellegacynumber);  
    
    /* With the introduction of new Requested Types we are going to introduce functionlity to 
    check for the presence of atleast one record of respective type in the Threshold tables inorder to create SPA */
    // START of BR-4968 October 2014 release 
     if(tobeProcessedForThresholdDiscount.size()>0)
      AP_SPARequestCalculations.checkForThresholdDiscount(tobeProcessedForThresholdDiscount);
      
    if(Trigger.New.size()>0)
      AP_SPARequestCalculations.checkForThresholdAmount(Trigger.New);
    //  END of BR-4968 October 2014 release 
 }
}