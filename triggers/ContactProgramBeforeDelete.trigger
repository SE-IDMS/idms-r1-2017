trigger ContactProgramBeforeDelete on ContactAssignedProgram__c (Before Delete) {
      //Akram for Q2 release: migrate to PRM V2 using memberProperties
        if(Utils_SDF_Methodology.canTrigger('AP_PartnerContactAssignementMigration'))
        {
            AP_PartnerContactAssignementMigration.deleteMemberProperties(trigger.oldMap);
        }

}