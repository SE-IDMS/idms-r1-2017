trigger SpecializationRequirementAfterInsert on SpecializationRequirement__c (after insert) {
    
    if(Utils_SDF_Methodology.canTrigger('AP_SpecializationRequirement'))
    {
        List<SpecializationRequirement__c> lstGloSpeReq = new List<SpecializationRequirement__c>();
        List<SpecializationRequirement__c> lstSpeReq = new List<SpecializationRequirement__c>();
        
        for(SpecializationRequirement__c  spr : trigger.new)
        {
            if(spr.active__c && spr.SpecializationRecordType__c == 'Global Specialization')
            {
                lstGloSpeReq.add(spr);
            }

            if(spr.active__c && spr.SpecializationRecordType__c != 'Global Specialization')
                lstSpeReq.add(spr);
        }
        
        //passing to handeler class
        if(!lstGloSpeReq.isEmpty())
            AP_SpecializationRequirement.createChildSpecializationRequirements(lstGloSpeReq, null);

        if(!lstSpeReq.isEmpty())
            AP_SpecializationRequirement.createNewAccountSpecializationRequirements(lstSpeReq, 'Pending');

    }
    
}//End of trigger