/*
    05-Nov-2012     Srinivas Nallapati  Initial Creation: BR-2083   Dec12 Release
       Jun-2016     Anand               BR-8563                     Jun16 Release
       Jul-2016     Pooja Suresh        BR-10118                    Jul16 Release
       Dec-2016     Swapnil Saurav      BR-10971                    Dec16 Release
*/
trigger AccountAfterUpdate on Account (after update) {

    System.Debug('****** AccountBeforeUpdate Trigger Start ****');
    List<Account> lstAccSyncObj = new List<Account>();
    
    if(Utils_SDF_Methodology.canTrigger('AP30'))
    {
    
        set<id> AccIdAddressChanged = new set<id>();
        List<Account> accList = new List<Account>();
        List<Account> acList = new List<Account>();
        List<Id> allUserIdLst = new List<Id>();
        Map<Id,Id> newESRAccIdMap = new Map<Id,Id>();
        Map<Id,Id> oldESRAccIdMap = new Map<Id,Id>();
        set<id> accset = new set<id>();

        
        for(Account acc : trigger.new)
        {   
            //Added by Anand for Q2 Release  BR-8563
            //Q2 start
            if(acc.Account_VIPFlag__c=='VIP1' && acc.Account_VIPFlag__c!=trigger.oldmap.get(acc.id).Account_VIPFlag__c)
            {
               
                acList.add(acc);
            }

            //Q2 End

            Account oldAcc = trigger.oldmap.get(acc.id);  
            if(acc.isPersonAccount == false && (
                    acc.Street__c != oldAcc.Street__c || acc.StreetLocalLang__c  != oldAcc.StreetLocalLang__c ||
                    acc.AdditionalAddress__c  != oldAcc.AdditionalAddress__c  || 
                    acc.LocalAdditionalAddress__c  != oldAcc.LocalAdditionalAddress__c  ||
                    acc.City__c  != oldAcc.City__c  || acc.LocalCity__c   != oldAcc.LocalCity__c  ||
                    acc.ZipCode__c  != oldAcc.ZipCode__c  || acc.StateProvince__c   != oldAcc.StateProvince__c  || 
                    acc.Country__c   != oldAcc.Country__c   || acc.County__c    != oldAcc.County__c   ||
                    acc.LocalCounty__c   != oldAcc.LocalCounty__c   || acc.POBox__c    != oldAcc.POBox__c   ||
                    acc.POBoxZip__c   != oldAcc.POBoxZip__c  || 
                    acc.AutomaticGeolocation__latitude__s    != oldAcc.AutomaticGeolocation__latitude__s    ||
                    acc.AutomaticGeolocation__longitude__s   != oldAcc.AutomaticGeolocation__longitude__s 
                    )
                   ) 

            {
              AccIdAddressChanged.add(acc.id);

            }
            

            if((acc.PRMAccount__c) && (acc.PRMBusinessType__c != oldAcc.PRMBusinessType__c || acc.PRMAreaOfFocus__c != oldAcc.PRMAreaOfFocus__c)) 
            {
                accList.add(acc);
            }
            
            if(acc.PRMAccount__c){
                lstAccSyncObj.add(acc);
            }

            //Check for a Italy account, if the ExternalSalesResponsible__c field has changed
            //BR-10649 Nov 16 Release Added Spanish accounts
            
            System.debug('--- AccountAfterInsert.Account New ExternalSalesResp Id-> '+acc.ExternalSalesResponsible__c);
            System.debug('--- AccountAfterInsert.Account Old ExternalSalesResp Id-> '+oldAcc.ExternalSalesResponsible__c);
            if(acc.TECH_SDH_CountryCode__c != NULL && acc.TECH_SDH_CountryCode__c != '') {
                String countryCode = Label.CLNOV16ACC002; //CLNOV16ACC002 = IT,ES,AD
                if(acc.RecordTypeId == System.Label.CLOCT13ACC08 && countryCode.contains(acc.TECH_SDH_CountryCode__c) &&  acc.ExternalSalesResponsible__c != oldAcc.ExternalSalesResponsible__c )
                {
                    if(acc.ExternalSalesResponsible__c==null && UserInfo.getProfileId()!= System.Label.CL00828 && UserInfo.getProfileId()!= System.Label.CL00829 && UserInfo.getProfileId()!= System.Label.CL00110) // CL00828 = Delegated Admin Profile Id, CL00829 = Delegated Admin + Panda user profiel id, CL00110 = System Administrator Profile Id
                    {
                        acc.addError('Please mention the External Sales Responsible for this account.');
                    }
                    else
                    {
                        System.debug('---- entered if check for change in externalsalesresponsible in after update ----');
                        newESRAccIdMap.put(acc.ExternalSalesResponsible__c,acc.id);  //Id of new ExternalSalesResp
                        oldESRAccIdMap.put(oldAcc.ExternalSalesResponsible__c,acc.id);  //Id of previous ExternalSalesResp
                        allUserIdLst.add(acc.ExternalSalesResponsible__c);
                        allUserIdLst.add(oldAcc.ExternalSalesResponsible__c);
                    }
                }
            }
        }

        
        //Updating Contact Channel&Sub-Channel Fields based on BusinessType&AreaOfFocus.  
            if(accList.size() > 0) {
                Map<Id, Account> accMap = new Map<Id, Account>(accList);
                AP_PRMAccountRegistration apPRMAccRegistration = new AP_PRMAccountRegistration();
                apPRMAccRegistration.populateContactFields(accMap);
            }
        //BR-2083
            if(AccIdAddressChanged.size() > 0)
               AP30_UpdateAccountOwner.updateContactAddress(AccIdAddressChanged); 
        
        //Added by Anand for Q2 Release BR-8563
            if(acList!=null && acList.size()>0)
                Ap_sitelocation.VIPCertificaionfromacc(acList);
    
        if(AP_ACC_PartnerAccountUpdate.isFirstExecution && Utils_SDF_Methodology.canTrigger('AP_ACC_PartnerAccountUpdate')  &&  !(System.isBatch()|| System.isFuture()) )
        {
            set<Id> setAccountId = new set<Id>();
            for(Id acctID : trigger.newMap.keySet()){
                if(trigger.newMap.get(acctID).PRMAccount__c != trigger.oldMap.get(acctID).PRMAccount__c && trigger.newMap.get(acctID).PRMAccount__c == true && trigger.newMap.get(acctID).RecordTypeId != Label.CLAPR15PRM025)
                    setAccountId.add(acctID);
            }
            
            if(!setAccountId.isEmpty())
                AP_ACC_PartnerAccountUpdate.UpdatePartnerAccount(setAccountId);
        }

        //BR-10118 Jul16 Rel - As part of enhancement DEF-10514 (Edit Access to the field External Sales Responsible)
            if(!newESRAccIdMap.isEmpty() && !oldESRAccIdMap.isEmpty())
                AP_UpdateExternalSalesAgent.updateExternalSalesResp(newESRAccIdMap,oldESRAccIdMap);
        
    
    }//End of canTrigger


    //Akram: FieldListener for Internal Properties
    if(Utils_SDF_Methodology.canTrigger('PRM_FieldChangeListener')){
        System.debug('Begins PRM_FieldChangeListener'); 
        PRM_FieldChangeListener.checkFieldFromAccountTrigger();
    }
    
    //Akram: listen to PRM events and chek canTrigger
    if(Utils_SDF_Methodology.canTrigger('PartnerLocatorPushEventsService')){
        System.debug('Begins PartnerLocatorPushEventsService'); 
        PartnerLocatorPushEventsService.checkPartnerLocatorPushEventsFromTriggerAccount();
    }
    //End Akram
    
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_AccountTriggers')){
        if(FieloPRM_AP_AccountTriggers.isActive){
            FieloPRM_AP_AccountTriggers.addAccountsToTeam();
        }
    }
    
    //////Fielo Syncrhonize//////////////////
    // if((FieloEE__Triggers__c.getInstance(UserInfo.getUserId()).Id == null || FieloEE__Triggers__c.getInstance(UserInfo.getUserId()).F_PRM_Synchronize__c) && (FieloEE__Triggers__c.getInstance(UserInfo.getUserId()).Id != null || FieloEE__Triggers__c.getOrgDefaults().Id == null || FieloEE__Triggers__c.getOrgDefaults().F_PRM_Synchronize__c)){
    if(!Test.isRunningTest() && (Utils_SDF_Methodology.canTrigger('SyncObjects') && lstAccSyncObj != Null && lstAccSyncObj.size()>0)){
        SyncObjects instance = new SyncObjects(lstAccSyncObj);
        boolean allornone = false;
        if(!instance.currentState.error ){
            SyncObjectsResult result = instance.syncronize(allornone);
        }
    }
    ///////////////////////////////////////////
}