/* 
    Author: Nicolas PALITZYNE (Accenture)
    Created date: 26/02/2012
    Description: Trigger checking if the case classification is not covered before insert
*/   

trigger TeamCaseJunctionBeforeInsert on TeamCaseJunction__c (before insert) 
{
    // Trigger cannot be bypassed as it ensures unicity of assignments (custom version of "unique" fields that cannot be bypassed).
    AP46_CheckTeamClassification existingCoverageChecker = new AP46_CheckTeamClassification(trigger.new);
    existingCoverageChecker.checkCoverage();
}