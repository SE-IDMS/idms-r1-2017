/********************************************************************************************************************
26-03-2013    Srinivas Nallapati    May13 PRM Release : Initial Creation
6-6-2013        Shruti Karn        June 2013 Release : To add check for Cluster Partne Program
********************************************************************************************************************/
trigger PartnerProgramBeforeUpdate on PartnerProgram__c (before update) {
    
    if(Utils_SDF_Methodology.canTrigger('AP_PartnerPRG'))
    {
        //set<Id> setOwnerId = new set<Id>();
        map<Id,PartnerProgram__c> mapOwnerID = new map<Id,PartnerProgram__c>();
        for(PartnerProgram__c  prg : trigger.new)
        {
            PartnerProgram__c oldPRG = trigger.oldmap.get(prg.id);
            
            //Update the DecommissionedDate__c  date if the Program status is changed to Decommissioned.
            // All the child country programs  will be made decommissioned  by the 'After Update' trigger
            if(prg.programStatus__C != oldPRG.programStatus__c  && prg.programStatus__c == 'Decommissioned')
            {
                prg.DecommissionedDate__c = system.now();
            }
            if(prg.ownerId !=  oldPRG.ownerId)
                mapOwnerID.put(prg.ownerId , prg);
        }
    
    
        if(!mapOwnerID.isEmpty())
        {
            list<User> lstUser = new list<User>();
            lstUser = [Select id, ProgramOwner__c, ProgramAdministrator__c,ProgramManager__c from user where id in: mapOwnerID.keySet() limit 50000];
            for(User u: lstUser)
            {
                if(u.ProgramOwner__c == false && u.PRogramAdministrator__c == false && mapOwnerID.get(u.id).recordtypeid == Label.CLMAY13PRM15)
                {
                    mapOwnerID.get(u.ID).addError(Label.CLMAY13PRM45);
                    
                }
                if(u.ProgramOwner__c == false && u.PRogramAdministrator__c == false && u.PRogramManager__c == false && (mapOwnerID.get(u.id).recordtypeid == Label.CLMAY13PRM05 || mapOwnerID.get(u.id).recordtypeid == Label.CLJUN13PRM09))
                {
                    mapOwnerID.get(u.ID).addError(Label.CLMAY13PRM48);
                    
                }
            }
        }
    }
    
}//End of trigger