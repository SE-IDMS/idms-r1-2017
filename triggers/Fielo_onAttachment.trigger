/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: Trigger Class                                       *
* Trigger: Fielo_onAttachment                               *
* Created Date: 19/08/2014                                  *
************************************************************/
trigger Fielo_onAttachment on Attachment (after insert) {

    if(Utils_SDF_Methodology.canTrigger('Fielo_AttachmentTriggers')){
        if (trigger.isInsert && trigger.isAfter){
            Fielo_AttachmentTriggers.AttIdForParent(trigger.new);
        }
    }
}