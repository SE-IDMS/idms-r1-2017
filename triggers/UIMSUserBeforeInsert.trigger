/**
•   Created By: Otmane
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This Trigger will be Fired before UIMS User record is inserted 
**/ 
trigger UIMSUserBeforeInsert on UIMS_User__c (before insert) {
    
    if(Utils_SDF_Methodology.canTrigger('UimsUser')){
        Set<String> usernamesSet = new Set<String>();
        for(UIMS_User__c uimsUser: trigger.new){
            if(!String.isblank(uimsUser.email__c) && String.isblank(uimsUser.phoneId__c)){
                uimsUser.IDMSUsername__c= uimsUser.email__c + Label.CLJUN16IDMS71;
            }else if (String.isblank(uimsUser.phoneId__c)){
                uimsUser.IDMSUsername__c= uimsUser.phoneId__c + Label.CLQ316IDMS102;            
            }else{
                uimsUser.IDMSUsername__c= 'ERROR';
            }
            usernamesSet.add(uimsUser.IDMSUsername__c);
            uimsUser.Vnew__c = '1';
        }
        
        //get correspoding IDMS Users
        if(usernamesSet.size() > 0){
            List<User> users = [SELECT ID, username from User where username  in :usernamesSet];
            Map<String, Id> mapUsernameUserId = new Map<String, Id>();
            for(User usr: users){
                mapUsernameUserId.put(usr.username,usr.id);
            }
            
            for(UIMS_User__c uimsUser: trigger.new){
                String username = uimsUser.IDMSUsername__c.toLowerCase();
                Id userId = mapUsernameUserId.get(username);
                
                if(userId != null){
                    uimsUser.user__c = userId;
                    uimsUser.isConvertedToIdmsUser__c= TRUE;
                }
            }
        }
    }
}