/*
  Author: Mathieu Gaudenzi
  Created On:14/01/2011
  Description:Sub Objective After Update Event Trigger 
      

*/

trigger SubObjectiveAfterUpdate on SubObjective__c (after update){

    System.Debug('****** SubObjectiveTrackRecordUpdateAfterApproval Trigger Start ****');  
    
    if(Utils_SDF_Methodology.canTrigger('AP03'))
    {    
        List<SubObjective__c> subObjectives = new List<SubObjective__c>();
        List<SubObjective__c> subObjectivestoUpdate = new List<SubObjective__c>();
        
        for(integer i = 0; i < trigger.new.size(); i++)
        {
            if(Trigger.new[i].Account__c!= Trigger.old[i].Account__c || Trigger.new[i].Amount__c!= Trigger.old[i].Amount__c|| Trigger.new[i].CustomerClassificationLevel1__c!= Trigger.old[i].CustomerClassificationLevel1__c|| 
               Trigger.new[i].CustomerClassificationLevel2__c!= Trigger.old[i].CustomerClassificationLevel2__c|| Trigger.new[i].GenericDemand__c!= Trigger.old[i].GenericDemand__c|| Trigger.new[i].GenericUnit__c!= Trigger.old[i].GenericUnit__c
               || Trigger.new[i].MarketSegment__c!= Trigger.old[i].MarketSegment__c|| Trigger.new[i].MarketSubSegment__c!= Trigger.old[i].MarketSubSegment__c|| Trigger.new[i].ParentObjective__c!= Trigger.old[i].ParentObjective__c
               || Trigger.new[i].Product__c!= Trigger.old[i].Product__c)
            {   
                subObjectives.add(trigger.new[i]);
            }
            
            if(Trigger.new[i].CustomerClassificationLevel1__c != Trigger.Old[i].CustomerClassificationLevel1__c || Trigger.New[i].CustomerClassificationLevel2__c != Trigger.old[i].CustomerClassificationLevel2__c || Trigger.new[i].MarketSegment__c != Trigger.old[i].MarketSegment__c || Trigger.new[i].MarketSubSegment__c != Trigger.old[i].MarketSubSegment__c)
            {
                subObjectivestoUpdate.add(Trigger.New[i]);          
            } 
        }
        if(subObjectives.size() !=0)
        {         
            AP03_ObjectiveSettingsTrigger.recordUpdationAfterApprovalForSubObjective(Trigger.New);
        } 
        
        if(subObjectivestoUpdate.size() > 0)
        {
            AP03_ObjectiveSettingsTrigger.updateTechFieldsofSubObjective(subObjectivestoUpdate);
        }  
    }

    System.Debug('****** SubObjectiveTrackRecordUpdateAfterApproval Trigger End****');    
}