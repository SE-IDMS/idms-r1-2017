trigger ProgramLevelBeforeAction on ProgramLevel__c (before insert, before update) {
    if(Utils_SDF_Methodology.canTrigger('AP_ProgramLevelHandler')) {
        AP_ProgramLevelHandler.checkHierarchyLevelCount(Trigger.new,Trigger.oldMap);
    }
}