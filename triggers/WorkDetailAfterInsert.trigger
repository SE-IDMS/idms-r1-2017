/**
* @author: Nabil ZEGHACHE
* Date Created (YYYY-MM-DD): 2016-11-08
* Test class: 
* Description: This is the afterInsert trigger of the Work Detail object
* Screnarios:
*
* -----------------------------------------------------------------
*                     MODIFICATION HISTORY
* -----------------------------------------------------------------
* Modified By: Authors Name
* Modified Date: Date
* Description: Brief Description of Change + BR/Hotfix
* -----------------------------------------------------------------
*/
trigger WorkDetailAfterInsert on SVMXC__Service_Order_Line__c (after insert) {
    if(Utils_SDF_Methodology.canTrigger('SVMX23'))
    {
        //if (ServiceMaxTimesheetUtils.trg_wdl_fired == false)
        //{
        ServiceMaxTimesheetUtils.createTimeEventsFromWorkDetails(trigger.new);
        string ServicedProduct_RT = System.Label.CLQ316SRV02; //Added by VISHNU C for Q3 2016 Release BR-9850 08/08/2016
        set<id>WDToDelete= new set<id>();//Added by VISHNU C for Q3 2016 Release BR-9850 08/08/2016
        map<id,id> WOMIPMap= new  map<id,id> (); //Added by VISHNU C for Q3 2016 Release BR-9850 08/08/2016
        map<id,string> WOSNProdMap= new  map<id,string> (); //Added by VISHNU C for Q3 2016 Release BR-9850 08/08/2016
        string WOConnProfId = System.Label.CLQ2H16SRV01; //Added by VISHNU C for Hot Fix 23/08/2016 
        string LoggedinUser = UserInfo.getProfileId(); //Added by VISHNU C for Hot Fix 23/08/2016 
        //   ServiceMaxTimesheetUtils.trg_wdl_fired = true;
        //}
        set<id>partset = new set<id>();
        set<id>set2= new set<id>();
        set<id>wdId= new set<id>();//Added by VISHNU C for Nov Minor Release 2016 BR-10694
        list<SVMXC__Service_Order_Line__c >wodlist1= new list<SVMXC__Service_Order_Line__c >();        
        for(SVMXC__Service_Order_Line__c wod:trigger.new)
        {

            if(trigger.isinsert)
            {
                if(wod.recordtypeid == ServicedProduct_RT && wod.IsSplitFlag__c == true && wod.Tech_WDOriginal__c != null)
                    {
                    WDToDelete.add(wod.Tech_WDOriginal__c);
                    }
                if(wod.recordtypeid == ServicedProduct_RT && wod.IsSplitFlag__c == true && wod.Tech_WDOriginal__c != null && wod.MIP_Flag__c == true)
                {
                    //if ( wod.SVMXC__Serial_Number__c != null )
                    WOMIPMap.put(wod.SVMXC__Service_Order__c,wod.SVMXC__Serial_Number__c);
                    //if ( wod.Shipped_Serial_Number__c != null && wod.SVMXC__Product__c != null )
                    //if( wod.Shipped_Serial_Number__c != null && wod.SVMXC__Product__c != '' )
                        WOSNProdMap.put(wod.SVMXC__Service_Order__c,wod.Shipped_Serial_Number__c+'#'+wod.SVMXC__Product__c);
                }
            }
            /*
           if( trigger.isupdate && ( WOConnProfId == null || WOConnProfId != LoggedinUser ) ) //Added by VISHNU C for Hot Fix 23/08/2016)
           {
             if(wod.Parts_Order_Line_RMA__c!=null && (wod.TrackingNumber__c!= trigger.oldmap.get(wod.id).TrackingNumber__c || wod.ReturnedQuantity__c!= trigger.oldmap.get(wod.id).ReturnedQuantity__c || wod.Return_Reason__c!= trigger.oldmap.get(wod.id).Return_Reason__c || wod.ReturnedQuantity__c == 0) && wod.WOStatus__c == 'Service Complete')
            
                {    
                    partset.add(wod.Parts_Order_Line_RMA__c);
                    wodlist1.add(wod);
                    wdId.add(wod.id); //Added by VISHNU C for Nov Minor Release 2016 BR-10694
                }
                if(wod.SVMXC__Serial_Number__c!= null)
                set2.add(wod.SVMXC__Serial_Number__c);
            }
            */
            
        }
        
        /*
            if(set2!=null && set2.size()>0){
                Ap_Installed_Product.IBchangeMaster(set2);
            }
            if(trigger.isupdate){
                if(partset!=null && partset.size()>0 && wodlist1!=null && wodlist1.size()>0){
                    //AP_WorkDetails.partsordertracknumberupdate(partset,wodlist1);
                    AP_WorkDetails.partsordertracknumberupdate(partset,wdId); //Added by VISHNU C for Nov Minor Release 2016 BR-10694
                }    
            
        }
        */
        
        //Added by VISHNU C for Q3 2016 Release BR-9850
        if(trigger.isinsert)
        {

            if( WDToDelete.size() > 0 && !(WDToDelete.isempty()) )
            {
	            System.debug('#### To be Deleted Wds: '+WDToDelete);
	            AP_WorkDetails.DeleteWD(WDToDelete);
            }
            if( WOMIPMap.size() > 0 && !(WOMIPMap.isempty()) )
            {
                AP_WorkDetails.UpdateWOMIP(WOMIPMap,WOSNProdMap);
            }
            
        }   
        //Changes by VISHNU C Ends here
    }   
}