trigger ProjectAfterUpdate on Milestone1_Project__c (after update) {


    //Id projectofferlaunchid = [Select Id From RecordType  Where SobjectType = 'Milestone1_Project__c' and DeveloperName = 'Offer_Introduction_Project'][0].Id;
  
    Id projectofferlaunchid = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Introduction Project').getRecordTypeId();
    //April 2015 release Hanamanth
    Set<string>  ProjectSetid = new  Set<string> ();
    list<Milestone1_Project__c> pjctLlist = new list<Milestone1_Project__c>();
    Map<id,string> mapidoldLaunchclass = new map<id,string>();
    if(Utils_SDF_Methodology.canTrigger('AP1003')){
        
        if(Trigger.IsUpdate && Trigger.Isafter) {

            for(Milestone1_Project__c mPObj:trigger.new) {

            if(mPObj.Country_Launch_Leader__c!=null && mPObj.Country_Launch_Leader__c!=trigger.oldmap.get(mPObj.id).Country_Launch_Leader__c) {
                ProjectSetid .add(mPObj.id);

            }
            if(mPObj.Country_Withdrawal_Leader__c!=null && mPObj.Country_Withdrawal_Leader__c!=trigger.oldmap.get(mPObj.id).Country_Withdrawal_Leader__c) {
                ProjectSetid .add(mPObj.id);

            }
            //May 15 Release -- Divya M //

            if(mPObj.Recommended_Country_classification__c != trigger.oldmap.get(mPObj.id).Recommended_Country_classification__c && mPObj.RecordTypeId==projectofferlaunchid){
                mapidoldLaunchclass.put(mPObj.id,trigger.oldmap.get(mPObj.id).Recommended_Country_classification__c);
            }

            //Ends May 15 Release
            }

            // Ap_Box_FolderCreation.HttpWebserviceToFolderBox(trigger.new); 
        } 
        if(ProjectSetid .size() > 0) {
            AP_ELLAToBoxFolderCreation.projectAddCollbrCreationInBox(ProjectSetid);
        }
        if(mapidoldLaunchclass.size()>0){
            AP_bFO_COLA_OfferCountryBUTaskCreation.pjctPhasesTasksDataReset(mapidoldLaunchclass);
        }
        
        
        //March Q1 Release 2016
        if(Trigger.IsUpdate && Trigger.Isafter) {
            boolean blnIsOld=false;
            AP_bFO_COLA_OfferCountryBUTaskCreation.UserAccessforProject(trigger.new,trigger.oldmap,blnIsOld);
        }   
        
    }
    
   

   
}