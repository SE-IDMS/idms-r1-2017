trigger OrganizationBeforeInsert on BusinessRiskEscalationEntity__c (before insert) {

if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_OrgBfInsert'))
{
        AP_Organization.checkOrgEntySubentyLocation(trigger.new) ;  
        }
}