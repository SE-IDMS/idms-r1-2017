trigger ConnectMilestoneBeforeUpdate on Connect_Milestones__c (before update) {

/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 28-Feb-2012
    Description     : Connect Milestone Before Update event trigger.
*/

System.Debug('****** ConnectMilestoneBeforeUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('ConnectMilestoneBeforeUpdate'))
    {
       ConnectMilestoneTriggers.ConnectMilestoneBeforeInsertUpdate(Trigger.new);
       
        ConnectMilestoneTriggers.GlobalMilestoneBeforeUpdate_ProgressSummaryUpdate(Trigger.new);
        if (Trigger.new[0].Validate_Insert_Update__c == True)
        
             Trigger.new[0].Name.AddError(Label.Connect_Milestone_Insert_ERR1);
                 
                 //ConnectMilestoneTriggers.ConnectMilestoneBeforeInsertUpdate_AppendTextColoumns(Trigger.new);
 }
 
 // Check for Cascading Milestone before Deleting a Scope
                 
       if((Trigger.new[0].Global_Functions__c != Trigger.old[0].Global_Functions__c) ||(Trigger.new[0].Global_Business__c != Trigger.old[0].Global_Business__c)|| (Trigger.new[0].Power_Region__c != Trigger.old[0].Power_Region__c)||(Trigger.new[0].GSC_Regions__c != Trigger.old[0].GSC_Regions__c)||(Trigger.new[0].Partner_Region__c != Trigger.old[0].Partner_Region__c)|| (Trigger.new[0].Smart_Cities_Division__c != Trigger.old[0].Smart_Cities_Division__c)){
        
           ConnectMilestoneTriggers.ConnectGlobalMilestoneBeforeUpdate_CheckCascadingMilestoneScope(Trigger.new,Trigger.old);
                 
             if (Trigger.new[0].ValidateScopeonUpdate__c == True)
        
                  Trigger.new[0].Name.AddError(Label.ConnectMilestoneUpdateERR1);
              }
        
 
 System.Debug('****** ConnectMilestoneBeforeUpdate Trigger End ****'); 
}