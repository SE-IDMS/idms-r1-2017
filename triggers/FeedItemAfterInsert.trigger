/*   Author: Shivdeep   
     Date Of Creation: March 02, 2015   
     Description : Feed Item After Insert event trigger
*/

trigger FeedItemAfterInsert  on FeedItem(After Insert) {
    
    if(Utils_SDF_Methodology.canTrigger('AP09')){
        AP_ProcessFeedItem PFI = new AP_ProcessFeedItem();
        PFI.updateLastUpdatedby(Trigger.new);
        
         // Added by Uttara - OCT 2015 Release - BR-7135
       // AP_ProcessFeedItem PFI1 = new AP_ProcessFeedItem();
       // PFI1.updateXA_LastUpdatedActualUserAndDate(Trigger.new);
    }
}