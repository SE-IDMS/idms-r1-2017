/*
    Author          : Ramakrishna Singara (Schneider Electric)
    Date Created    : 11/07/2012
    Description     : Program Costs Forecasts Afterupdate event trigger.
*/
trigger PROG_CostForecastAfterUpdate on ProgramCostsForecasts__c (after update) {

    if(Utils_SDF_Methodology.canTrigger('ProgramCostForecastTriggerAfterUpdate'))
    {
        PROG_CostForecastTriggers.ProgCostForecastAfterUpdate(Trigger.new);
    }

}