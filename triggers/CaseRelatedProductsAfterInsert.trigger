//********************************************************************************************************
//  Trigger Name            : CaseRelatedProductsAfterInsert
//  Purpose                 : CSE_RelatedProduct__c  After Insert event trigger , 
//                            Update the sibling Products If Master Product is checked
//  Created by              : Vimal Karunakaran (Global Delivery Team)
//  Date created            : 25th August 2013
///*******************************************************************************************************/

/*
    Modification History    :
    Modified By             : 
    Modified Date           : 
    Description             : 
*/

trigger CaseRelatedProductsAfterInsert on CSE_RelatedProduct__c (after insert) {
    System.Debug('****** CaseRelatedProductsAfterInsert Trigger Start ****');
    
    if(Utils_SDF_Methodology.canTrigger('AP_CaseRelatedProduct_Handler')){
        AP_CaseRelatedProduct_Handler.unCheckOtherMasterProduct(null,Trigger.newMap,true);
    }
    System.Debug('****** CaseRelatedProductsAfterInsert Trigger Ends ****');
}