/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | Before Delete Trigger                                                              |
|                       |                                                                                    |
|     - Object(s)       | RMA_Product__c                                                                     |
|     - Description     |   - Used to prevent deleting Return Items on validated RMAs                        |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | April, 27th 2012                                                                   |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
trigger ReturnItemBeforeDelete on RMA_Product__c (Before Delete)
{
   System.Debug('****** ReturnItemBeforeDelete Trigger starts ****');

   if(Utils_SDF_Methodology.canTrigger('AP34'))
   {
      List<RMA_Product__c> recordsToDelete = AP35_addItemToRMA.getReturnItemsToDelete(trigger.old);

      AP34_RMAMailMerge.removeReturnedProducts(recordsToDelete);
   }

   System.Debug('****** ReturnItemBeforeDelete Trigger ends ****');
}