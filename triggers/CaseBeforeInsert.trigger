/*
    Author          : Accenture IDC Team 
    Date Created    : 18/07/2011
    Description     : Case before insert event trigger.
    
    Modified By     : Shruti Karn
    Modified Date   : 12 June 2012
    Description     : September 2012 Release:
    
    1. Copy the value of Case subject to 'Customer Request' when 'Customer Request' field is null.
    2. Call AP10_CaseTrigger.updateFirstAssignmentAge(trigger.new)
                      
    ------------------------------
    Modified By     : Stephen Norbury
    Modified Date   : 06/03/2013
    Description     : May 2013 Release:
    
    1. BR-1363: Identify the Lead Agents for each Level of Expertise
    2. BR-1363: Identify the Last Agents for each Level of Expertise
    3. BR-2476: Record the Entry and Exit Date / Times for each Level of Expertise
    4. BR-2761: First Assignment Age for each Level of Expertise
    5. BR-2762: Case Ageing
    6. BR-2152: Email is a Fax
    
    -----------------------------------------------------------------   
    Modified By     : Rakhi Kumar
    Modified Date   : 13 Feb 2013
    Description     : May 2013 Release:
    
    1. Added the method updateContact - to update case contact from Supplied Email or Supplied Phone 
                      when Case Origin is Web.
    2. For a Web to Case, copy the first 80 characters of the Customer Request into the Subject. 
    -----------------------------------------------------------------   
    Modified By     : Renjith Jose
    Modified Date   : 05 Aug 2013
    Description     : Oct 2013 Release:BR-3128
                      Pre-populate family field on case
    -----------------------------------------------------------------  
    
    Release Version   :April 15 Release
    Modified By       : Deepak Kumar
    Description       :To Update Service Contract If Service Line is updated also to Update Commercial Reference when 
                        Installed Product is Updated.

*/

trigger CaseBeforeInsert on Case (before Insert){
    List<Case> lstCasesWithExternalReference = new List<Case>();
    List<Case> webCasesWithPoCForUpdate = new List<Case>();
    List<Case> liveChannelCasesForCountryUpdate = new List<Case>();
    List<Case> webCasesWithoutPoCForUpdate = new List<Case>();
    List<Case> automaticallyCreatedCases = new List<Case>(); 
    List<Case> csList = new List<Case>();       
    Set<Id> scIdset = new Set<Id>();
    Map<String,Case> mapProductGMRCase = new Map<String,Case>();
    List<Case> lstCases = new List<Case>();
    List<Case> lstCasesWithSite = new List<Case>();
    
    if(Utils_SDF_Methodology.canTrigger('CaseBeforeInsert')){
        //Code Modified by mohammad Naved on 30 aug2016

        for(Case objCase:Trigger.new){
            If(objCase.ExternalReference__c == Label.CL00686){
                AP33_UpdateAnswerToCustomer.addStringToAnswerToCustomer(objCase,objCase.Details__c);
            }
            if(objCase.CCCountry__c==null){
                if(objCase.Origin==null){
                    objCase.Origin = Label.CL00445;
                }
                
                if (objCase.Origin.equalsIgnoreCase(Label.CL00445) && objCase.SuppliedCountry__c != null){
                    if(objCase.PortalId__c!=null){
                        webCasesWithPoCForUpdate.add(objCase);
                    }
                    else{
                        webCasesWithoutPoCForUpdate.add(objCase);                 
                    }
                }
                else if((objCase.Origin !=Label.CL00445 || (objCase.Origin == Label.CL00445 && objCase.SuppliedCountry__c == null)) && 
                    (objCase.origin !=Label.CL00446 || (objCase.origin ==Label.CL00446 && objCase.SuppliedEmail == null))) {
                    liveChannelCasesForCountryUpdate.add(objCase);
                }
            }
            if(objCase.Team__c == null  && ((objCase.Origin.equalsIgnoreCase(Label.CL00445) && objCase.SuppliedCountry__c != null && objCase.PortalId__c == null ) || (objCase.origin == Label.CL00446 && objCase.SuppliedEmail != null))){
                automaticallyCreatedCases.add(objCase);                 
            }
            //Added By Deepak For April 15 Release 
            if(objCase.SVMXC__Service_Contract__c!=null){
                csList.add(objCase);
                scIdset.add(objCase.SVMXC__Service_Contract__c);
            }
            if(objCase.SVMXC__Component__c !=null){
                csList.add(objCase);
            } 
            if(objCase.ContactID!=null || objCase.AssetOwner__c!=null || objCase.AccountID!=null){
                lstCases.add(objCase);
            }
             if(objCase.Site__c!=null){
                lstCasesWithSite.add(objCase);
            } 
            //OCT 2015 Release : Added by Uttara - BR-7096
            //if(objCase.CommercialReference_lk__c != null && objCase.CaseSymptom__c != null &&
              //(objCase.Status == Label.CLOCT15I2P31 || objCase.Status == Label.CLOCT15I2P33 || objCase.Status == Label.CLOCT15I2P34)) {
                //mapProductGMRCase.put(objCase.TECH_GMRCode__c,objCase);
            //}  
           // else if(objCase.CommercialReference_lk__c == null && objCase.Family_lk__c != null && objCase.CaseSymptom__c != null &&
                  // (objCase.Status == Label.CLOCT15I2P31 || objCase.Status == Label.CLOCT15I2P33 || objCase.Status == Label.CLOCT15I2P34)) {
                //mapProductGMRCase.put(objCase.TECH_GMRCode__c,objCase);     
            //}                
            // CLOCT15I2P31 = New
            // CLOCT15I2P33 = In Progress
            // CLOCT15I2P34 = Answer Provided to Customer          
        }
        
        //OCT 2015 Release : Added by Uttara - BR-7096
        //System.debug('#### mapProductGMRCase : ' + mapProductGMRCase );
        //if(!mapProductGMRCase.isEmpty())
           // AP_Case_CheckOpenCCCActions.CaseWithOpenProblems(mapProductGMRCase);
            
        //Added for May 2013 release
        AP10_CaseTrigger.updateContact(trigger.new);
        //Added by Anil B
        if(lstCases.size()>0){
            AP10_CaseTrigger.vipIconEnhancment(lstCases);
        }
        if(lstCasesWithSite.size()>0){
            AP10_CaseTrigger.legacySiteIdPopulation(lstCasesWithSite);
        }
        if(webCasesWithPoCForUpdate.size()>0){
            //Find PoC for Web Case, Reporting Organization, Do Team Calculation, Level of Expertise
            AP47_EmailToCaseAutoAssignments.updatePoCInfoToCase(webCasesWithPoCForUpdate);
        }
        
        if(webCasesWithoutPoCForUpdate.size()>0){
            AP20_UpdateCaseCountry.webCaseCountryUpdate(webCasesWithoutPoCForUpdate);
        }
        if(liveChannelCasesForCountryUpdate.size()>0){
            AP20_UpdateCaseCountry.caseCountryUpdate(liveChannelCasesForCountryUpdate);
        }
        
        if(automaticallyCreatedCases.size()>0){
            AP47_EmailToCaseAutoAssignments.assignTeam(automaticallyCreatedCases,false);
        }
      
        //Added for September 2012 release            
        AP10_CaseTrigger.updateContractField(trigger.new);
        
        /*------------------------------
        Modified By     : Stephen Norbury
        Modified Date   : 06/03/2013
        Description     : May 2013 Release:

        1. BR-1363: Identify the Lead Agents for each Level of Expertise
        2. BR-1363: Identify the Last Agents for each Level of Expertise
        3. BR-2476: Record the Entry and Exit Date / Times for each Level of Expertise
        4. BR-2761: First Assignment Age for each Level of Expertise
        5. BR-2762: Case Ageing
        6. BR-2152: Email is a Fax
        
        -----------------------------------------------------------------*/
       
        //Added By Deepak For April 15 Release 
        if(csList.Size()>0 && scIdset!=null && scIdset.size()>0){
            Ap_CaseServicesFunc.CaseSrvContUpdate(csList);
        }
        if(csList.size()>0){
            Ap_CaseServicesFunc.CaseIPCommRefUpdate(csList);
        }
        
        AP_Case_CaseHandler.handleCaseBeforeInsert (trigger.new);
        //After CC Country is identified (either from Team or User Record), Apply SLA Rule
        //AP20_UpdateCaseCountry.updateCaseDueDate(Trigger.new);
        //AP_Case_CaseHandler.calculateFirstAssignmentSLA(Trigger.new,null,'insert',null);
    }
}