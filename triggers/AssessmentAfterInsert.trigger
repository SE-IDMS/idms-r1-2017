/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 28 July 2013
    Description  : 1. Add Assessment to ORF,Account Assigned Program and Account Specialization
                     when automatic assignment checkbox is checked.        
***************************************************************************************************************************/
trigger AssessmentAfterInsert on Assessment__c (after insert) {
    
    map<String,set<Id>> mapProgamLevel = new map<String,set<Id>>();
    map<Id,set<Id>> mapAccountSpecialization = new map<Id,set<Id>>();
    for(Assessment__c assessment : trigger.new)
    {
        if(assessment.AutomaticAssignment__c && assessment.RecordTypeId != System.label.CLOCT13PRM01)
        {
            if(assessment.PartnerProgram__c != null && assessment.ProgramLevel__c != null)
            {
                if(!mapProgamLevel.containsKey(assessment.PartnerProgram__c))
                    mapProgamLevel.put(assessment.PartnerProgram__c+':'+assessment.ProgramLevel__c , new set<Id> {(assessment.Id)});
                else
                    mapProgamLevel.get(assessment.PartnerProgram__c+':'+assessment.ProgramLevel__c).add(assessment.id);
            }
                    
            if(assessment.Specialization__c != null)
            {
                if(!mapAccountSpecialization.containsKey(assessment.Specialization__c))
                    mapAccountSpecialization.put(assessment.Specialization__c,new set<Id>{(assessment.Id)});
                else
                    mapAccountSpecialization.get(assessment.Specialization__c).add(assessment.Id);
            }
                
        }
         
    }
    
    if(!mapAccountSpecialization.isEmpty())
        AP_ASM_AddAssessment.addSpeAssessment(mapAccountSpecialization);
    
    if(!mapProgamLevel.isEmpty())
        AP_ASM_AddAssessment.addProgramAssessment(mapProgamLevel);
    
    if(Utils_SDF_Methodology.canTrigger('AP_AssessmentHandler')) {
        AP_AssessmentHandler.SetGlobalProgramApprovalFlag (Trigger.new);
    }    
    
}