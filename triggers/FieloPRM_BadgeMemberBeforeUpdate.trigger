/********************************************************************
* Company: Fielo
* Developer: 
* Created Date: 30/03/2015
* Description: 
********************************************************************/

trigger FieloPRM_BadgeMemberBeforeUpdate on FieloEE__BadgeMember__c (before Update) {
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_BadgeMemberTriggers')){
        List<FieloEE__BadgeMember__c> triggerNew = FieloPRM_AP_BadgeMemberTriggers.filterTriggerNew(trigger.new);
        if(!triggerNew.isEmpty()){
            FieloPRM_AP_BadgeMemberTriggers.badgesMembersSetActiveInactive(triggerNew, trigger.OldMap);
            FieloPRM_AP_BadgeMemberTriggers.generateExpirationDate(triggerNew, trigger.OldMap);
        }
    }
}