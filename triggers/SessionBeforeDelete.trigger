trigger SessionBeforeDelete on TMT_Session__c (before delete) 
{
    if(Utils_SDF_Methodology.canTrigger('TMT'))
    {
        List<TMT_Session__c> DeletedSessions = new List<TMT_Session__c>();
    
        For(Integer i=0; i<trigger.old.size(); i++)
        {
            If(trigger.old[i].SubStatus__c != 'Past' && trigger.old[i].Notify__c==true )
            {
                  DeletedSessions.add(trigger.old[i]);
            }
        }
           
         if(DeletedSessions.size()>0)
            TMT_SendEmailClass.SendDeletedSessionEmail( DeletedSessions); 
    }
    
}