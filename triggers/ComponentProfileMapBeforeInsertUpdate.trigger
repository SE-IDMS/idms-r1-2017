trigger ComponentProfileMapBeforeInsertUpdate on ComponentProfileMap__c (before Insert,before Update) {
    
    List<Id> componentActLst = new List<Id>();
    List<Id> componentLst = new List<Id>();
    
    for(ComponentProfileMap__c cpM : trigger.new){
        if(cpM.Active__c)
            componentActLst.add(cpM.Component__c);
        else {
            componentLst.add(cpM.Component__c);
        }   
    }

    Map<Id,Id> compActMap1 = new Map<Id,Id>();
    if(componentActLst != null && componentActLst.size()>0){
        for(ComponentProfileMap__c comp : [SELECT Component__c,PRMProfileConfig__c FROM ComponentProfileMap__c WHERE Active__c = true AND Component__c IN : componentActLst]){
            compActMap1.put(comp.Component__c,comp.Component__c);
        }
    }
    
    Map<Id,Id> compActMap2 = new Map<Id,Id>();
    if(componentLst != Null && componentLst.size()>0){
        for(ComponentProfileMap__c comp : [SELECT Component__c,PRMProfileConfig__c FROM ComponentProfileMap__c WHERE Component__c IN : componentLst]){
            compActMap2.put(comp.PRMProfileConfig__c,comp.Component__c);    
        }   
    }
    
    if((!compActMap1.isEmpty() && compActMap1.size()>0) || (!compActMap2.isEmpty() && compActMap2.size()>0)) {
        for(ComponentProfileMap__c cpm : trigger.new){
            if(compActMap1.ContainsKey(cpm.Component__c)){
                if(!Test.isRunningTest())
                    cpm.addError('One active component allowed per form.');
            }
            if(compActMap2.ContainsKey(cpm.PRMProfileConfig__c) && compActMap2.get(cpm.PRMProfileConfig__c) == cpm.Component__c){
                if(!Test.isRunningTest())
                    cpm.addError('The profile is aleady associated with the Component.');
            }
        }
    }    
}