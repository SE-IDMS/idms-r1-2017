trigger InstalledProductSwapAfterInsert on Swap_Asset__c (after insert) {
    set<Id> dipid = new set<Id>();
    set<id>newipset= new set<id>();
    list<Swap_Asset__c>  swapassetList = new list<Swap_Asset__c>();
    Map<Id,Id> OldNewIpIdmap = new Map<Id,Id>();
    Map<Id,Swap_Asset__c> OldIpIdASMap = new Map<Id,Swap_Asset__c>();
    Map<Id,Swap_Asset__c> NewIpIdASMap = new Map<Id,Swap_Asset__c>();
    list<SVMXC__Installed_Product__c> ipplist= new list<SVMXC__Installed_Product__c>();
    list<SVMXC__Installed_Product__c> iplist = new list<SVMXC__Installed_Product__c>();

   // if(Utils_SDF_Methodology.canTrigger('SVMX15')){
        for(Swap_Asset__c swap :trigger.New){
            if(swap.DeinstalledInstalledProduct__c != swap.InstalledProduct__c && swap.InstalledProduct__c !=null && swap.DeinstalledInstalledProduct__c !=null){
                dipid.add(swap.DeinstalledInstalledProduct__c);
                newipset.add(swap.InstalledProduct__c);
                OldNewIpIdmap.put(swap.DeinstalledInstalledProduct__c,swap.InstalledProduct__c);
                system.debug('****I am Here***'+dipid);
                OldIpIdASMap.put(swap.DeinstalledInstalledProduct__c,swap);
                NewIpIdASMap.put(swap.InstalledProduct__c,swap);
                swapassetList.add(swap);
            }
        }
        
             //Added by anand for Q3 Release for BR-8232
        
       iplist= [select id,name,ProductGroupID__c,SVMXC__Date_Installed__c  from SVMXC__Installed_Product__c where id in:dipid];
        
            for(SVMXC__Installed_Product__c ipp:[select id,name,ProductGroupID__c,SVMXC__Date_Installed__c from SVMXC__Installed_Product__c where id in:newipset])
            {
                if(iplist[0].ProductGroupID__c!=null)
                ipp.ProductGroupID__c=iplist[0].ProductGroupID__c;
                ipp.SVMXC__Date_Installed__c=swapassetList[0].CustomerDateTime__c;
                ipplist.add(ipp);
            }
       
            if(ipplist.size()>0)
            update ipplist;
        
        
        
        //Covered Product Swap with Correct Service Line
        if( OldNewIpIdmap.keySet()!=null){
            system.debug('****Calling Method***'+OldNewIpIdmap);
           Ap_InstalledProductSwap.IpSwapCoveredProduct(OldNewIpIdmap);
            
            Ap_InstalledProductSwap.IpSwapOppty(OldNewIpIdmap);
                        
            Ap_InstalledProductSwap.IpSwapWorkOder(OldNewIpIdmap);
             
           Ap_InstalledProductSwap.IpSwapPmPlanCoverage(OldNewIpIdmap);
            
            if(swapassetList.size()>0){
                Ap_InstalledProductSwap.IpSwapProductWarranty(OldNewIpIdmap,swapassetList);
            }
            
            if(NewIpIdASMap.keyset()!=null){
            
                //String Serializedata = JSON.serialize(NewIpIdASMap);
                Ap_InstalledProductSwap.GainingIpmapping(NewIpIdASMap,OldNewIpIdmap);
            }
            if(OldIpIdASMap.keyset()!=null){
            
                //String Serializedata = JSON.serialize(OldIpIdASMap);
                Ap_InstalledProductSwap.LoosingIpMapping(OldIpIdASMap,OldNewIpIdmap);
            }
        }
        
   
}