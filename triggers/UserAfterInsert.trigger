/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 17 July 2012
    Description : For September Release:
                 1. To Add Users to 'Partner Program Owners' public group when the Program Owner checkbox is checked.
                     
********************************************************************************************************************/
trigger UserAfterInsert on User (After Insert) {
    System.debug('Before bypassing AP_User_ParnterPRGOwnerUpdate');
    if(Utils_SDF_Methodology.canTrigger('AP_User_ParnterPRGOwnerUpdate')){
        System.debug('No bypassing did not happen AP_User_ParnterPRGOwnerUpdate');
        list<User> lstUser = new list<User>();
        for(User u : trigger.new)
        {
            if(u.ProgramOwner__c || u.ProgramApprover__c || u.ProgramAdministrator__c)
                lstUser.add(u);
        }
        
        if(!lstUser.isEmpty())
            AP_User_ParnterPRGOwnerUpdate.addUserToGroup(lstUser);
    }
}