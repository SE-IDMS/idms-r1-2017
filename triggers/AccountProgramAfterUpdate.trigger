/********************************************************************************************************************
    Created By : Shruti Karn/Srinivas Nallapati
    Created Date : 06 March 2013
    Description : For May13 Release:
                 
                 1. Calls AP_AAP_AccountProgramUpdate.delChildRecords to:
                     a. Delete the child reords when Program Level is updated for Account Partner Program.
                 2. AP_AAP_AccountProgramUpdate.deActivateChildRecords to: 
                     a. Deactivate the child reords when  Account Partner Program becomes inactive.    
                 3. AP_AAP_AccountProgramUpdate.checkAssignedLevels
                     a. To check that duplicate Account Assigned Programs are not created.
    Updated By : Shruti Karn
    Date : 28 July 2013
    Description : To add Assessment with Automatic Assignemnet checkbox checked to Account Assigned Program.
    
********************************************************************************************************************/
trigger AccountProgramAfterUpdate on ACC_PartnerProgram__c (after update) {

    if(Utils_SDF_Methodology.canTrigger('AP_AAP_AccountProgramUpdate') || test.isRunningTest())
    {
       //AP_AAP_AccountProgramUpdate.delChildRecords(trigger.newMap , trigger.oldMap);
       // AP_AAP_AccountProgramUpdate.deActivateChildRecords(trigger.newMap , trigger.oldMap);
        
        //apr 14 prm
        List<ACC_PartnerProgram__c> inactivatedAccProgs = new List<ACC_PartnerProgram__c>();
        AP_ProcessFeatureAssignment.processAccountAssignedFeatures(trigger.new);

        //Srinivas
        AP_AAP_AccountProgramUpdate.checkAssignedLevels(trigger.new); 
        
        //October 2013 RElease
        map<String,set<Id>> mapAAPProgram = new map<String,set<Id>>();
        for(ACC_PartnerProgram__c accPRogram : trigger.new)
        {
            ACC_PartnerProgram__c oldProgram = trigger.oldMap.get(accPRogram.Id);
            if((accPRogram.PartnerProgram__c != null && accPRogram.PartnerProgram__c != oldProgram.PartnerProgram__c) || 
                        (accPRogram.ProgramLevel__c != null && accPRogram.ProgramLevel__c != oldProgram.ProgramLevel__c) )
            {
                if(!mapAAPProgram.containsKey(accPRogram.PartnerProgram__c+':'+accPRogram.ProgramLevel__c))
                    mapAAPProgram.put(accPRogram.PartnerProgram__c+':'+accPRogram.ProgramLevel__c , new set<id> {(accPRogram.Id)});
                else
                    mapAAPProgram.get(accPRogram.PartnerProgram__c+':'+accPRogram.ProgramLevel__c).add(accPRogram.Id);
            }

            if(accPRogram.Active__c != oldProgram.Active__c)
            {
                inactivatedAccProgs.add(accPRogram);
            }

        }
        if(!mapAAPProgram.isEmpty())
            AP_AAP_AccountProgramUpdate.addAssessment(mapAAPProgram);


        //apr 14 prm
       
        list<ContactAssignedProgram__c> lstContactProgram = new list<ContactAssignedProgram__c>();
        lstContactProgram = [Select id,AccountAssignedProgram__c,Active__c from ContactAssignedProgram__c where AccountAssignedProgram__c in:inactivatedAccProgs limit 10000];
        
        if(!lstContactProgram.isEmpty())
        {
            for(ContactAssignedProgram__c conProgram : lstContactProgram)
            {
                if( trigger.newMap.containsKey(conProgram.AccountAssignedProgram__c) )
                    conProgram.Active__c = trigger.newMap.get(conProgram.AccountAssignedProgram__c).Active__c;
            }
            
            Database.SaveResult[] UpdateResult = database.update(lstContactProgram,false);
            for(Integer i=0;i<UpdateResult.size();i++ )
            {
               Database.SaveResult sr =UpdateResult[i];
               if(!sr.isSuccess())
               {
                  Database.Error err = sr.getErrors()[0];
                  system.debug('err:'+err);
                  //lstAccProgram[0].addError( err.getMessage());               
               }
            }
        }
        
    }
}