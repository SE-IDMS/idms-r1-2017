/********************************************************************
* Company: Fielo
* Developer: 
* Created Date: 09/03/2015
* Description: 
********************************************************************/

trigger FieloPRM_BadgeAccountAfterUpdate on FieloPRM_BadgeAccount__c (after update) {
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_BadgeAccountTriggers')){
        if(trigger.isAfter && trigger.isUpdate && !FieloPRM_AP_BadgeAccountTriggers.isRunning){
            FieloPRM_AP_BadgeAccountTriggers.isRunning = true;
            FieloPRM_AP_BadgeAccountTriggers.setBadgeAccountName(trigger.newMap.keySet());
            FieloPRM_AP_BadgeAccountTriggers.setBadgeAccountUnique(Trigger.new);
            FieloPRM_AP_BadgeAccountTriggers.recalculatesPartenerLocatorAccount(trigger.new, trigger.oldMap);
            FieloPRM_AP_BadgeAccountTriggers.isRunning = false;
        }       
    }
}