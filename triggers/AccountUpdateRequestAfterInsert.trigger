//THIS TRIGGER UPDATES THE APPROVER FIELD WITH THE DATA STEWARD USER OF THE CORRESPONDING ACCOUNT'S COUNTRY
trigger AccountUpdateRequestAfterInsert on AccountUpdateRequest__c (before insert) 
{
    if(Utils_SDF_Methodology.canTrigger('AP_AURAfterInsertHandler'))
    {    
        AP_AURAfterInsertHandler handler = new AP_AURAfterInsertHandler();
        handler.onBeforeInsert(Trigger.new,Trigger.newMap);
    }   
}