/********************************************************************************************************************
    Modified By : Shruti Karn
    Created Date : 2 July 2012
    Description : For September Release:
                 1. Calls AP_ContractTriggerUtils.findNewContact to check if an Active PAckage is being added to
                    Inactive/Draft Contract
    
********************************************************************************************************************/
trigger PackageBeforeUpdate on Package__c (before update) {

    if(Utils_SDF_Methodology.canTrigger('AP_PackageTriggerUtils') || Test.isRunningTest())
        AP_PackageTriggerUtils.checkContractStatus(trigger.new);
}