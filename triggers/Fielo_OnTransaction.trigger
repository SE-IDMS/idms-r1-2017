/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: Trigger                                             *
* Created Date: 03/10/2014                                  *
************************************************************/
trigger Fielo_OnTransaction on FieloEE__Transaction__c (before insert) {

    if(Utils_SDF_Methodology.canTrigger('Fielo_TransactionTriggers')){    
        if (trigger.isBefore && trigger.isInsert){
            Fielo_TransactionTriggers.setsCurrencyUAD(trigger.new);
        }
    }

}