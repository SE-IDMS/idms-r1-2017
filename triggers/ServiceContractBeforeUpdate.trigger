trigger ServiceContractBeforeUpdate on SVMXC__Service_Contract__c (before update) 
{
     //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX14'))
    {
 
    list<SVMXC__Service_Contract__c> sclist =  New List<SVMXC__Service_Contract__c>();
    set<Id> scid=New set<Id>();
    String ServiceContractRT = System.Label.CLAPR15SRV01;
    String ServiceContractConnectedRT = System.Label.CLAPR15SRV04;
    String ServiceLineRT = System.Label.CLAPR15SRV02;
    String ServiceLineConnectedRT = System.Label.CLAPR15SRV03;
    String VAL= System.Label.CLAPR15SRV29;
    String EXP = System.Label.CLAPR15SRV30;
    String Amendment = System.Label.CLAPR15SRV26;
    String ETL = System.Label.CLAPR15SRV27;
    String Renewal = System.Label.CLAPR15SRV28;
    Set<Id> RenewedFromId = new Set<Id>();
    Set<Id> sscids = new Set<Id>();
    Set<Id> set9= new Set<Id>();
    Set<Id> SCSPset= new Set<Id>();
    set<id>spset1 = new set<id>();
    set<id>skuset1 = new set<id>();
    set<id>exiredset= new set<id>();
    list<SVMXC__Service_Contract__c> list9 = new list<SVMXC__Service_Contract__c>();
    list<SVMXC__Service_Contract__c> list8 = new list<SVMXC__Service_Contract__c>();
    list<SVMXC__Service_Contract__c> splist1 = new list<SVMXC__Service_Contract__c>();
    list<SVMXC__Service_Contract__c> sclist1 =  New List<SVMXC__Service_Contract__c>();
    List<SVMXC__Service_Contract__c> newProcess= new List<SVMXC__Service_Contract__c>();
    set<Id> scid1=New set<Id>();
    //Map<string,SVMXC__Service_Contract__c> mapSspidSc = new Map<string,SVMXC__Service_Contract__c>(); //added by suhail for june release 2016
    
    for(SVMXC__Service_Contract__c  sc :trigger.New)
    {
     /*
      //added by suhail for june release 2016 start
    if((sc.ParentContract__c != null)||(sc.ParentContract__c != '')){
        
        
        mapSspidSc.put(sc.SVMXC__Service_Plan__c,sc);
    }
    
    
    //added by suhail for june release 2016 end
    */
    
        if((sc.RecordTypeid == ServiceLineRT || sc.RecordTypeId ==ServiceLineConnectedRT) && ((sc.SKUComRef__c!= Trigger.oldMap.get(sc.id).SKUComRef__c && sc.SKUComRef__c!=null) || (sc.SVMXC__Service_Plan__c!= Trigger.oldMap.get(sc.id).SVMXC__Service_Plan__c && sc.SVMXC__Service_Plan__c != null)))
        {   
            spset1.add(sc.SVMXC__Service_Plan__c);
            skuset1.add(sc.SKUComRef__c);
            splist1 .add(sc);
        }
    
        if((sc.status__c== 'CAN' && sc.status__c!=trigger.oldmap.get(sc.id).status__c) || (sc.status__c== 'PCANC' && sc.status__c!=trigger.oldmap.get(sc.id).status__c))
        { 
          
            sc.SVMXC__Canceled_By__c = sc.LastModifiedByid;
            sc.SVMXC__Canceled_On__c = Datetime.now(); 
        } 
        if(sc.status__c== 'EXP' && sc.SVMXC__Active__c==FALSE)
        {
        exiredset.add(sc.id);
        
        }
         if(sc.SVMXC__Renewal_Number__c == null)
        {
            sc.SVMXC__Renewal_Number__c =0;
            
        }
        if(sc.Status__c==VAL && sc.Tech_StartDate__c== true && (trigger.oldmap.get(sc.id).Tech_StartDate__c!= sc.Tech_StartDate__c)
        && (sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId ==ServiceContractConnectedRT))
        { 
            system.debug('*****IN*****'); 
            sclist.add(sc);           
        }
        
        if((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId ==ServiceContractConnectedRT) && sc.status__c== 'CAN'){
                sc.SVMXC__Active__c =FALSE;
                }
        
        if(sc.SVMXC__Service_Plan__c!=null && sc.SVMXC__Service_Plan__c!=Trigger.oldMap.get(sc.id).SVMXC__Service_Plan__c){
            set9.add(sc.SVMXC__Service_Plan__c);
            list9.add(sc);
            }
        if(sc.SKUComRef__c!=trigger.oldmap.get(sc.id).SKUComRef__c){
            sscids.add(sc.id);
        }    
        if((sc.RecordTypeid == ServiceLineRT || sc.RecordTypeId ==ServiceLineConnectedRT) && (sc.SVMXC__Service_Plan__c==null || sc.SVMXC__Service_Plan__c!= Trigger.oldMap.get(sc.id).SVMXC__Service_Plan__c)){
            SCSPset.add(sc.id);
        }
       
        if(sc.ParentContract__c !=null && (trigger.OldMap.get(sc.Id).ParentContract__c !=sc.ParentContract__c)&&
        (sc.RecordTypeid == ServiceLineRT ||sc.RecordTypeId ==ServiceLineConnectedRT)){
            scid.add(sc.ParentContract__c);
        }
        if(sc.SVMXC__Renewed_From__c !=null && trigger.OldMap.get(sc.Id).SVMXC__Renewed_From__c !=sc.SVMXC__Renewed_From__c 
           &&(sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId ==ServiceContractConnectedRT)){
           
            RenewedFromId.add(sc.SVMXC__Renewed_From__c);
        }
         if((sc.RecordTypeId == ServiceLineRT ||sc.RecordTypeId ==ServiceLineConnectedRT) && ((sc.SVMXC__Active__c !=trigger.OldMap.get(sc.Id).SVMXC__Active__c) || (sc.SVMXC__Contract_Price2__c !=trigger.OldMap.get(sc.Id).SVMXC__Contract_Price2__c)))
        {
            scid1.add(sc.Id);
            sclist1.add(sc);
        }
        if((sc.SVMXC__Start_Date__c<=System.Today() && sc.Status__c == 'VAL' && (sc.SVMXC__Start_Date__c !=trigger.OldMap.get(sc.Id).SVMXC__Start_Date__c || sc.Status__c !=trigger.OldMap.get(sc.Id).Status__c)) 
        && (sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId ==ServiceContractConnectedRT))
        {
            sc.SVMXC__Active__c =True;
        }
        if(sc.SVMXC__End_Date__c==System.Today() && sc.SVMXC__End_Date__c!=trigger.OldMap.get(sc.Id).SVMXC__End_Date__c 
        && (sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId ==ServiceContractConnectedRT))
        {
            sc.SVMXC__Active__c =False;
            sc.Status__c = EXP;
            sc.Tech_EndDate__c = True;
        }
        if((sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId == ServiceContractConnectedRT) 
        && sc.EarliestEndDate__c == System.Today() && Trigger.oldMap.get(sc.id).EarliestEndDate__c != sc.EarliestEndDate__c
        && sc.SVMXC__Active__c == true && sc.IsEvergreen__c == False && sc.LeadingBusinessBU__c != null && sc.LeadingBusinessBU__c!='')
        {
           sc.Tech_RenewalOpportunityCreat__c = true;
           //newProcess.add(sc);
        } 
    }
    
    
     if(sscids!=null && sscids.size()>0){
        AP_ServiceContract.LoadServicePlan(trigger.new,'UPDATE');
        //AP_ServiceContract.IncludedserviceRemoval(sscids);  
    }
    
    if(sclist.size()>0)    
        AP_ServiceContract.ContractUpdate(sclist);
        
        if(set9!=null && set9.size()>0){
        AP_ServiceContract.IncludedserviceRemoval(SCSPset);
         AP_ServiceContract.IncludedServicesfromServicePlanBefore(list9,set9,'Update');
        }

        if(exiredset!=null)
        AP_ServiceContract.expmethod(exiredset);
  
     if(spset1!=null)
     {
        AP_ServiceContract.Slatermload(spset1,splist1);
     }
     else if(skuset1!=null)
     {
        AP_ServiceContract.Slatermload(skuset1,splist1);  
     }
     /*
     //added by suhail for june release 2016
     List<SVMXC__Service_Contract__c> testlist = New List<SVMXC__Service_Contract__c>();
        
     for(SVMXC__Service_Contract__c s:trigger.new){
           if(trigger.oldMap.get(s.Id).SVMXC__Service_Plan__c != s.SVMXC__Service_Plan__c){
             testlist.add(s); 
           } 
        }
        if((!mapSspidSc.isempty())&&(testlist != null)&&(testlist.size()>0)){
        AP_ServiceContract.autoPopulatePMplan(mapSspidSc);
    }
    //added by suhail for june release 2016
         */
     if(scid!=null)
        AP_ServiceContract.ServiceLineUpdate(scid,trigger.new);
     //if(RenewedFromId!=null){
       // AP_ServiceContract.RenewedNumber(RenewedFromId,trigger.new);    
    //}
    
    /*
        @Author : Bala Candassamy
        */
        /* Create a list of ServiceContracts*/
       List<SVMXC__Service_Contract__c> scupdatelist = New List<SVMXC__Service_Contract__c>();
        
        for(SVMXC__Service_Contract__c a:trigger.new){
           if(trigger.oldMap.get(a.Id).SVMXC__Renewed_From__c != a.SVMXC__Renewed_From__c){
             scupdatelist.add(a); 
           } 
        }
        
        if(scupdatelist != null && scupdatelist.size()>0){
            AP_ServiceContractInitialContract.InitialContract(scupdatelist);
        } 
        //
       // if(scid1!=null && sclist1.size()>0)    
          //  AP_ServiceContract.SericesContractPrice(sclist1,scid1);
    }   
}