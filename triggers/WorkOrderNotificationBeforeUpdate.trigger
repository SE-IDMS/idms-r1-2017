/*     
@Author: Jyotiranjan Singhlal
Created Date: 07-11-2012
Description: This trigger fires on Work Order Notification on Before Update
**********
Scenarios:
**********
1.  Case Owner is Updated
2.  Customer Location is Updated

Date Modified       Modified By         Comments
------------------------------------------------------------------------
07-11-2012          Jyotiranjan Singhlal    Logic to update Work Order notification Case Owner field if Case is changed
20-11-2012          Jyotiranjan Singhlal    Logic to update Work Order notification Time Zone if CL is changed
11-04-2013          Jyotiranjan Singhlal    Swapped the Logic of CL with Location(scenario 2)
15-04-2013          Deepak Kumar            Added logic for Sold to account Field update.
*/


trigger WorkOrderNotificationBeforeUpdate on WorkOrderNotification__c (before update) 
 {

if(Utils_SDF_Methodology.canTrigger('SRV01'))  
{  
List<WorkOrderNotification__c> tobeProcess = new List<WorkOrderNotification__c>();
List<case> caseown = new list <case>();
Map<id,Case> cidRecMap =new Map<id,Case>();
Set<id> cidset = new Set<id>();
set<id> accidRec = New set<id>();
set<Id> assetOwnerId = New set<Id>();

 
    
/**********************************************************************************************************************************/
// Scenario: 1
    // Case Owner is Updated and Sold to Account  update.
/***********************************************************************************************************************************/
for(WorkOrderNotification__c workOrderNotif: Trigger.new)
{     
    if(workOrderNotif.Case__c != null)
    {
        tobeProcess.add(workOrderNotif);
    }
    
    if(tobeProcess != null && tobeProcess.size()>0)
    {
        for(WorkOrderNotification__c workOrderNotif1: tobeProcess)
        {
            cidset.add(workOrderNotif1.Case__c);
        }
    }
    if(cidset != null && cidset.size()>0)
    {
        caseown = [select Id,OwnerId,AccountId,Owner.Name,CustomerRequest__c,AssetOwner__c from Case where id IN :cidset];
        system.debug('caseown:'+caseown);
    
        if(caseown != null && caseown.size()>0)
        {
            cidRecMap.putAll(caseown);
            system.debug('cidRecMap:'+cidRecMap);
            for(Case c:caseown)
            {
                accidRec.add(c.AccountId);
                if(c.AssetOwner__c != null)
                    accidRec.add(c.AssetOwner__c);
            }
    
        }
    }
}

List <SVMXC__Site__c> loc1 = New List <SVMXC__Site__c>();
List <SVMXC__Site__c> loc2 = New List <SVMXC__Site__c>(); 
loc1 =[select SVMXC__Account__c,PrimaryLocation__c, Id from SVMXC__Site__c where SVMXC__Account__c IN :accidRec AND PrimaryLocation__c =: True limit 1];
loc2 =[select SVMXC__Account__c,PrimaryLocation__c, Id from SVMXC__Site__c where SVMXC__Account__c IN :accidRec limit 1];

Map<id,id> aidlocid = New Map<id,id>();
if(loc1 !=null && loc1.size()>0)
{
    for(SVMXC__Site__c loc:loc1)
    {
        aidlocid.put(loc.SVMXC__Account__c,loc.Id);
    }
}
else if(loc2 !=null && loc2.size()>0)
{
    for(SVMXC__Site__c loc:loc2)
    {
        //if(loc.PrimaryLocation__c==true)
        //{
        aidlocid.put(loc.SVMXC__Account__c,loc.Id);
        //}
    }
}
for(WorkOrderNotification__c wonObj: tobeProcess)
{
    if(wonObj.Case__c != Trigger.OldMap.get(wonObj.id).Case__c)
    {
   
        if(cidRecMap.get(wonObj.Case__c).AssetOwner__c != null)
        {
            wonObj.Location__c = aidlocid.get(cidRecMap.get(wonObj.Case__c).AssetOwner__c);
        }
        else
        {
            wonObj.Location__c = aidlocid.get(cidRecMap.get(wonObj.Case__c).AccountId);
        }
    }
}



    /**********************************************************************************************************************************/
    // Scenario: 2
    // Customer Location is Updated(swapped CL with Location)
    /***********************************************************************************************************************************/
      List<SVMXC__Site__c> custloc = new List <SVMXC__Site__c>();
      Set<id> woncl = New Set<id>();
      List<WorkOrderNotification__c> wonlst1 = new List<WorkOrderNotification__c>();
    
    for(WorkOrderNotification__c workOrderNotif: Trigger.new)
    {
      if(workOrderNotif.Location__c != null)
      {
          wonlst1.add(workOrderNotif);
          woncl.add(workOrderNotif.Location__c);
      }
      if(wonlst1.size()>0)
      {
         custloc = [select TimeZone__c from SVMXC__Site__c where id IN :woncl];
         
         for(SVMXC__Site__c cl: custloc)
         {
             for(WorkOrderNotification__c wcl: wonlst1)
             {
                 if(wcl.Location__c == cl.Id)
                 {
                     if(wcl.TimeZone__c == null)
                         wcl.TimeZone__c = cl.TimeZone__c;
                 }      
             }
         }
      }
    }
 }  
}