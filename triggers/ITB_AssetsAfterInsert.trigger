//********************************************************************************************************
//  Trigger Name            : ITB_AssetsAfterInsert
//  Purpose                 : ITB_Asset__c  After Inset event trigger , 
//                            Update the associated Case with the Serial Number for Primary Asset
//  Created by              : Vimal Karunakaran (Global Delivery Team)
//  Date created            : 11th March 2013
///*******************************************************************************************************/

/*
    Modification History    :
    Modified By             : 
    Modified Date           : 
    Description             : 
*/

trigger ITB_AssetsAfterInsert on ITB_Asset__c (after insert) {
    System.Debug('****** ITB_AssetsAfterInsert Trigger Start ****');
    if(Utils_SDF_Methodology.canTrigger('AP_ITB_Assets_PrimaryAssetCheck')){
        AP_ITB_Assets_PrimaryAssetCheck.updateCaseSerialNumber(Trigger.new);
    }
    System.Debug('****** ITB_AssetsAfterInsert Trigger Ends ****');
}