/*       Author: Pooja Gupta  
         Date Of Creation: 01/01/2013   
         Description : Product Line After Update event trigger

         Modification History:
         Author: Siddharth N (GD solutions)         
         Description: Logic moved entirely to a handler
*/


trigger ProductLineAfterUpdate on OPP_ProductLine__c (After Update) {
    System.Debug('********* ProductLineAfterUpdate Trigger Start *********');
    if(Utils_SDF_methodology.canTrigger('AP_ProductLineTrigger') && VFC61_OpptyClone.isOpptyClone == false)
    {
        if(Label.CLOCT13SLS47 != 'UPDATE_OPPTY_WITH_MANY_LINES')
        {
            AP_ProductLineAfterUpdateHandler handler = new AP_ProductLineAfterUpdateHandler();
            handler.OnAfterUpdate(Trigger.new,Trigger.oldMap);
        }
        else
        {
            Map<Id, OPP_ProductLine__c> oldProductLinesMap = new Map<Id, OPP_ProductLine__c>();
            List<OPP_ProductLine__c> lstProductLinestobeProcessedInHandler = new List<OPP_ProductLine__c>();
            List<OPP_ProductLine__c> lstProductLinestobeProcessedInBatch = new List<OPP_ProductLine__c>();
            Map<Id, Integer> oppProdLineCount = new Map<Id, Integer>();
            Set<Opportunity> updOpps = new Set<Opportunity>();
            List<Opportunity> updateOpps = new List<Opportunity>();
                
            for(OPP_ProductLine__c newProductLineRecord:Trigger.New)
            {
                if(oppProdLineCount.get(newProductLineRecord.Opportunity__c)!=null)
                {
                    oppProdLineCount.put(newProductLineRecord.Opportunity__c, oppProdLineCount.get(newProductLineRecord.Opportunity__c)+1);
                }
                else
                    oppProdLineCount.put(newProductLineRecord.Opportunity__c, 1);
            }
            for(OPP_ProductLine__c newProductLineRecord:Trigger.New)
            {                
                //Modified if condition from 150 to 100 -March release
                if((newProductLineRecord.TECH_OpportunityLinesCount__c+oppProdLineCount.get(newProductLineRecord.Opportunity__c))<100)
                {
                    lstProductLinestobeProcessedInHandler.add(newProductLineRecord);     
                    oldProductLinesMap.put(newProductLineRecord.Id,Trigger.oldMap.get(newProductLineRecord.Id));                               
                }                
                else if((newProductLineRecord.TECH_OpportunityLinesCount__c+oppProdLineCount.get(newProductLineRecord.Opportunity__c))>100  && (newProductLineRecord.TECH_OpportunityLinesCount__c+oppProdLineCount.get(newProductLineRecord.Opportunity__c))<1000)
                {
                    updOpps.add(new Opportunity(TECH_IsOpportunityUpdate__c=true,Id=newProductLineRecord.Opportunity__c));
                    lstProductLinestobeProcessedInBatch.add(newProductLineRecord);                    
                    oldProductLinesMap.put(newProductLineRecord.Id,Trigger.oldMap.get(newProductLineRecord.Id));                               
               }
            }
              
            //Calls the Product Line after update 
            if(lstProductLinestobeProcessedInHandler!=null && !(lstProductLinestobeProcessedInHandler.isEmpty()))
            {
                AP_ProductLineAfterUpdateHandler handler = new AP_ProductLineAfterUpdateHandler();
                handler.OnAfterUpdate(lstProductLinestobeProcessedInHandler,oldProductLinesMap);    
            }
            //Updates the Opportunity TECH_IsOpportunityUpdate__c  field
            if(lstProductLinestobeProcessedInBatch!=null && !(lstProductLinestobeProcessedInBatch.isEmpty()))
            {
                //prepare parent productlines Map
                Map<Id,boolean> productLinesParenthasParentMap=new Map<Id,boolean>();
                Map<Id,List<Id>> ParentLineToChildLineMap=new Map<Id,List<Id>>();
                for(OPP_ProductLine__c newProductLineRecord:Trigger.new){
                    if(newProductLineRecord.ParentLine__c!=null){
                        if(ParentLineToChildLineMap.containsKey(newProductLineRecord.ParentLine__c))
                            ParentLineToChildLineMap.get(newProductLineRecord.ParentLine__c).add(newProductLineRecord.Id);
                        else
                            ParentLineToChildLineMap.put(newProductLineRecord.ParentLine__c,new List<Id>{newProductLineRecord.Id});
                    }
                        
                }
    
                for(OPP_ProductLine__c parentProductLine:[select Id from OPP_ProductLine__c where id in :ParentLineToChildLineMap.keySet() and ParentLine__c!=null])            
                    if(ParentLineToChildLineMap.containsKey(parentProductLine.Id))
                        for(Id productLineId:ParentLineToChildLineMap.get(parentProductLine.Id))
                            Trigger.newMap.get(productLineId).addError(Label.CLOCT13SLS12);
                updateOpps.addall(updOpps);
                Database.update(updateOpps);    
            }
        }
    }
    System.Debug('********* ProductLineAfterUpdate Trigger End *********');       
}