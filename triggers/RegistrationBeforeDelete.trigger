trigger RegistrationBeforeDelete on TMT_Registration__c (Before Delete)
{
     if(Utils_SDF_Methodology.canTrigger('TMT'))
     {
        Set<Id> sessionIds = new Set<Id>();
        for (TMT_Registration__c reg : trigger.old)
        {
            sessionIds.add(reg.Session__c);
        }
        Map<Id, TMT_Session__c> SessionsById = new Map<Id, TMT_Session__c>([select Id, Notify__c from TMT_Session__c where Id in : sessionIds]);

        TMT_Registration__c[] regToNotify = new TMT_Registration__c[0];
        for (TMT_Registration__c reg : trigger.old)
        {
            if(SessionsById.get(reg.Session__c).Notify__c)
            {
                regToNotify.add(reg);
            }
        }

     
        // Send notification to the trainee
        if (!regToNotify.isEmpty())
        {
            TMT_SendEmailClass.SendDeleteEmail(regToNotify);
        }
    }
}