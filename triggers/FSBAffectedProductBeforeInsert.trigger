trigger FSBAffectedProductBeforeInsert on FSBAffectedProduct__c (Before Insert) {
    
    if(Utils_SDF_Methodology.canTrigger('FSBAPBI')) {
        
        Set<String> setFSB = new Set<String>();
         
        for(FSBAffectedProduct__c fsbap : Trigger.New) {             
            setFSB.add(fsbap.FieldServiceBulletin__c);
        }
        System.debug('!!!! setFSB : ' + setFSB);
        if(!setFSB.isEmpty()) 
            AP_FSBAffectedProduct.SameProductLineCheck(setFSB,Trigger.New);
    }
}