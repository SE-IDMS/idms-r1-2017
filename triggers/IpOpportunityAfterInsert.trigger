/*Created by: Deepak Kumar
Date:16/09/2013
For the purpose of Creating Installed Product Opportunity when Opportunity Notification is getting created by SFM Buttons.
OCT13 Release
*/
trigger IpOpportunityAfterInsert on InstalledProductOnOpportunityNotif__c (after insert) 
{    
    
    List<InstalledProductOnOpportunityNotif__c> IpOppList = New List<InstalledProductOnOpportunityNotif__c>();
    
    for(InstalledProductOnOpportunityNotif__c ip :trigger.New)
    {
        if(ip.Tech_CreatedFromIpSFM__c ==true)
        //if(ip.OpportunityNotification__r.Tech_CreatedFromIB__c ==true)
        {
            IpOppList.add(ip);
            //AP_OpptyCreationAfterInsert.CreatingIpOpportunity(IpOppList);
        }    
    }
    if(IpOppList.size()>0)
    {
        AP_OpptyCreationAfterInsert.CreatingIpOpportunity(IpOppList);   
    }
   
}