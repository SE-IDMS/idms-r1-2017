/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: 
*******************************************************************/

trigger FieloPRM_AP_MemberAfterInsert on FieloEE__Member__c (After insert){
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberTrigger')){
        FieloPRM_MemberTrigger.syncrhonizeContactAndAccount(trigger.new);
        FieloPRM_MemberTrigger.searchMemberProperties(trigger.new);
    }
    
}