/*
    Author          : Priyanka Shetty(Schneider Electric)
    Date Created    : 12/04/2013
    Description     : DMT Funding Entity Before Update event trigger.
*/
trigger CHR_FundingEntityBeforeUpdate on ChangeRequestFundingEntity__c (before update) 
{
    System.Debug('****** CHR_FundingEntityBeforeUpdate Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('FundingEntityTriggerUpdate'))
    {
        
        CHR_FundingEntityTriggers.fundingentityBeforeUpdate(Trigger.newMap, Trigger.oldMap);

    }
    System.Debug('****** CHR_FundingEntityBeforeUpdate Trigger End ****'); 
}