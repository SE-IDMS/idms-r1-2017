trigger ComplaintsRequestsEscalationAfterInsert on ComplaintsRequestsEscalationHistory__c (after insert) {

    if(!AP_ComplaintsRequestsEscalation.isExcecute) {
        
        List<ComplaintsRequestsEscalationHistory__c> listCompResHis= new List<ComplaintsRequestsEscalationHistory__c>();
        
        for(ComplaintsRequestsEscalationHistory__c trgObj:trigger.new) {
            if(trgObj.Event_Type__c !=Label.CLAPR15I2P07 ) {
            
                listCompResHis.add(trgObj);
            }
        }
        if(listCompResHis.size() > 0) {
            AP_ComplaintsRequestsEscalation.updateComplaintsRequestCRS(listCompResHis);
        
        }
        AP_ComplaintsRequestsEscalation.isExcecute=true;
    }


}