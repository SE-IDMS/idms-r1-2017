trigger ContactAssignedFeatureBeforeInsert on ContactAssignedFeature__c (before insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_CAF'))
    {
        AP_ContactAssignedFeature.checkDuplicateCAF(trigger.new);
    }    
}