trigger ClusterBeforeEvent  on PRMCountry__c (before insert,before update) {
    
    if(Utils_SDF_Methodology.canTrigger('AP_PRMUtils')){
            AP_PRMUtils.checkPRMCountry(trigger.new);
    }
}