/*
Srinivas Nallapati  20-Mar-2013     May13 PRM Release
*/
trigger ProgramChangeRequestBeforeInsert on ProgramChangeRequest__c (before insert) {
    if(Utils_SDF_Methodology.canTrigger('ProgramChangeRequestBeforeInsert'))
    {
        AP_PRMChangeRequest.updateRequestApprover(trigger.new);
    }

}