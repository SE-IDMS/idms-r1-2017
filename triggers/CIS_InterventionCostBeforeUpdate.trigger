//@Author:- Ashish Sharma SESA340747
//@Date:- 22/08/2014
//@Description:- This trigger is written to assign  Cost Per Unit from Country List price Object based upon ISR EU country ,Field of activity and
//Intervention Cost's Cost type field.

trigger CIS_InterventionCostBeforeUpdate on CIS_InterventionCosts__c(before update) {
    if(Test.isRunningTest() || Utils_SDF_Methodology.canTrigger('AP_CIS_InterventionCostHandler')){  
   //Call beforeCISInterventionCostInsert from AP_CIS_InterventionCostTriggerHandler handler class.
   
   List<CIS_InterventionCosts__c> interventionCostToUpdateList = new List<CIS_InterventionCosts__c>(); 
   
   for(CIS_InterventionCosts__c interventionCostISR:trigger.new){
       if((trigger.newMap.get(interventionCostISR.Id).CIS_CostType__c!= trigger.oldMap.get(interventionCostISR.Id).CIS_CostType__c)||(interventionCostISR.CIS_CostPerUnit__c==null)){
           interventionCostToUpdateList.add(interventionCostISR);       
       }
       
       AP_CIS_InterventionCostHandler.beforeCISInterventionCostInsert(interventionCostToUpdateList);
       

   }
   
  }   
}