/*
@Author: Jyotiranjan Singhlal
Created Date: 11-04-2013
Description: This trigger fires on Installed Product on Before Update
**********
Scenarios:
**********
1.  Update Installed Product Account Details from the Location

Date Modified       Modified By         Comments
------------------------------------------------------------------------
17-07-2013          Ramu Veligeti       Logic to update Account's TECH_IsSVMXRecordPresent__c field
10-09-2013          Jyotiranjan         Commented the scenario 1
13-02-2014          Hari Krishna        Defects#032551
03-03-2014          Deepak              Asset Category and IB Duplicate Validation
*/
trigger InstalledProductBeforeupdate on SVMXC__Installed_Product__c (before update) 
{
    if(Utils_SDF_Methodology.canTrigger('SVMX10'))
    {   
        //Variablees related to Scenario 1 
        List<SVMXC__Installed_Product__c> ip1 = new List<SVMXC__Installed_Product__c>();    
        Set<id> iploc = New Set<id>();
         string INFORTECHNOBUSINESS =System.label.CLDEC15SRV05;
         string IT =System.label.CLDEC15SRV04;
     
     // in case it's a standalone user
     // Updated on 10-02-2014 for Defects#032551
     //if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLNOV13SRV03)){
      if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLNOV13SRV03)){    
    
        for(SVMXC__Installed_Product__c a:trigger.new)
        {
               
            if(a.AutoCalcManufacturingDate__c!=null && a.SVMXC__Serial_Lot_Number__c!=trigger.oldmap.get(a.id).SVMXC__Serial_Lot_Number__c && (a.ITBInstalled_Product__c== INFORTECHNOBUSINESS/*|| a.ITBInstalled_Product__c== IT*/))
            {
                 
                 
                a.ManufacturingUnitDate__c = a.AutoCalcManufacturingDate__c;

            }

        if(a.Remote_System__c != null && a.Remote_System__c.Contains('AOS')){
            //a.ToBeSentForUpdate__c = true;//Added by Anand for BR-8051(AOS interface).
            }
            //Condition for Scenario 1
            if(a.SVMXC__Site__c!= Null && 
            (trigger.oldMap.get(a.id).SVMXC__Site__c!=trigger.NewMap.get(a.id).SVMXC__Site__c))              
            {
                ip1.add(a);
                iploc.add(a.SVMXC__Site__c);        
            } 
                       
        }
        /*******************************************************************************************
        Scenario: 1
        Update Account Details from the Location
        *******************************************************************************************/
        if (!iploc.isempty() && iploc.size() > 0 ) //Added by VISHNU C for TOO Many SOQL Error B2F SC Interface
        {
            list<SVMXC__Site__c> loc = [Select Id,Name,SVMXC__Account__c,Tolabel(SVMXC__Location_Type__c) 
            From SVMXC__Site__c Where Id IN :iploc];
            
            for(SVMXC__Site__c lo1:loc)
            {          
                for(SVMXC__Installed_Product__c ip:ip1)
                {
                    if(ip.SVMXC__Site__c == lo1.Id)
                    {
                        //ip.SVMXC__Company__c = lo1.SVMXC__Account__c;
                        ip.LocationType__c = lo1.SVMXC__Location_Type__c;
                    }   
                }
            }
        }
        /*
            Added by: Ramu Veligeti
            Date: 17/07/2013
            Update TECH_IsSVMXRecordPresent__c on Account object if the Account is used in 
            ServiceMax related objects like Work Order, Installed Product, Location, Service Contract etc..
        */
        Set<Id> AccId = new Set<Id>();
        for(SVMXC__Installed_Product__c a:trigger.new){
            if(a.SVMXC__Company__c!=null && trigger.oldMap.get(a.id).SVMXC__Company__c !=a.SVMXC__Company__c){
                AccId.add(a.SVMXC__Company__c);
            }
        }
        if(AccId.size()>0 && AccId !=null)
        SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
      }// applied for standalone users   
    /********************************************************************************
            Added by: Deepak
            Date: 18/02/2014(April-14)
            Purpose-Asset Categorisation
    *********************************************************************************/
        // Commented on May Release 
        // Logic moved to Batch_IPUpdate
        /*
        set<Id> ipid = New set<Id>();
        List<SVMXC__Installed_Product__c> iplist = new List<SVMXC__Installed_Product__c>();
        for(SVMXC__Installed_Product__c a:trigger.new)
        {
            if(trigger.OldMap.get(a.Id).LifeCycleStatusOfTheInstalledProduct__c !=trigger.NewMap.get(a.Id).LifeCycleStatusOfTheInstalledProduct__c
                 ||trigger.OldMap.get(a.Id).DecomissioningDate__c !=trigger.NewMap.get(a.Id).DecomissioningDate__c
                 ||trigger.OldMap.get(a.Id).UnderContract__c !=trigger.NewMap.get(a.Id).UnderContract__c)
            {
                iplist.add(a);
                ipid.add(a.Id);
            }
            
        }
        if(iplist.size()>0 && ipid !=null)
        Ap_Installed_Product.AssetCategory(iplist);
        */
        
        
      
    /*
    BR-4237
    Putting Validation Rule to make a unique IB(No Duplicate with same specification)
    */
    //Commented below lines by VISHNU C For July 2016 Release Code Optimization fixing the APEX CPU Time out. This is commented as the Code in the class itself is commented. 
     /*    List<SVMXC__Installed_Product__c> iplst = new List<SVMXC__Installed_Product__c>();
        for(SVMXC__Installed_Product__c ip :trigger.new)
        {
            if(trigger.OldMap.get(ip.Id).Brand2__c != trigger.NewMap.get(ip.Id).Brand2__c ||
               trigger.OldMap.get(ip.Id).DeviceType2__c != trigger.NewMap.get(ip.Id).DeviceType2__c ||
               trigger.OldMap.get(ip.Id).Category__c != trigger.NewMap.get(ip.Id).Category__c||
               trigger.OldMap.get(ip.Id).SVMXC__Serial_Lot_Number__c != trigger.NewMap.get(ip.Id).SVMXC__Serial_Lot_Number__c)
            {
                iplst.add(ip);
            }
            if(trigger.OldMap.get(ip.Id).Brand2__c != trigger.NewMap.get(ip.Id).Brand2__c ||
               trigger.OldMap.get(ip.Id).DeviceType2__c != trigger.NewMap.get(ip.Id).DeviceType2__c ||
               trigger.OldMap.get(ip.Id).Category__c != trigger.NewMap.get(ip.Id).Category__c||
               trigger.OldMap.get(ip.Id).SVMXC__Serial_Lot_Number__c != trigger.NewMap.get(ip.Id).SVMXC__Serial_Lot_Number__c||
               trigger.OldMap.get(ip.Id).SVMXC__Product__c != trigger.NewMap.get(ip.Id).SVMXC__Product__c)
            {
                iplst.add(ip);
            }
        }
        if(iplst.size()>0)
        system.debug('iplst:'+iplst);
        Ap_Installed_Product.IBValidation(iplst);
     */   
      
    }   
}