/*

1-Mar-13    Shruti Karn            PRM Mar13 Release    BR-2782    Initial Creation
5-Mar-13    Srinivas Nallapati     PRM Mar13 Release    BR-2782    Updated Email alert  
  
*/
trigger ContactAfterUpdate on Contact (after update) {


     
    
    set<Id> setAccountId = new set<Id>();
    list<contact> lstContact = new list<Contact>();
    Set<String> conIds = new Set<String>();
    Map<String,String> mapConNames = new Map<String,String>();
    list<Id> lstPrimaryContactIds = new list<Id>();
    
    if(Utils_SDF_Methodology.canTrigger('AP_Contact_PartnerUserUpdate') && !(System.isBatch()|| System.isFuture()))
    {
        set<Id> setNewPartnerContactId = new set<Id>();
        set<Id> setPartnerContactId = new set<ID>();
        
        
        for(Id contID : trigger.newMap.keySet()) {
            if((trigger.newMap.get(contID).PRMUser__c!= trigger.oldMap.get(contID).PRMUser__c) && (trigger.newMap.get(contID).PRMUser__c== true)&& 
                 String.isBlank(trigger.newMap.get(contID).PRMUIMSID__c)) 
                    setNewPartnerContactId.add(contID);            
            if((trigger.newMap.get(contID).PRMContact__c!= trigger.oldMap.get(contID).PRMContact__c) && (trigger.newMap.get(contID).PRMContact__c== true))
            {
                setAccountId.add(trigger.newMap.get(contID).accountid);
                lstContact.add(trigger.newMap.get(contID));
            }
            
            if ((trigger.newMap.get(contID).FirstName != trigger.oldmap.get(contID).FirstName) || (trigger.newMap.get(contID).LastName != trigger.oldmap.get(contID).LastName)) 
            {
                mapConNames.put(trigger.oldmap.get(contID).FirstName + ' ' + trigger.oldmap.get(contID).LastName, trigger.newMap.get(contID).FirstName + ' ' + trigger.newMap.get(contID).LastName);
            }
        
            if ((trigger.newMap.get(contID).FirstName != trigger.oldmap.get(contID).FirstName) || (trigger.newMap.get(contID).LastName != trigger.oldmap.get(contID).LastName) ||
               (trigger.newMap.get(contID).WorkPhone__c != trigger.oldmap.get(contID).WorkPhone__c) || (trigger.newMap.get(contID).Email != trigger.oldmap.get(contID).Email) ||
               (trigger.newMap.get(contID).Phone != trigger.oldmap.get(contID).Phone) || (trigger.newMap.get(contID).MailingStreet != trigger.oldmap.get(contID).MailingStreet) ||
               (trigger.newMap.get(contID).MailingCity != trigger.oldmap.get(contID).MailingCity) || (trigger.newMap.get(contID).MailingPostalCode != trigger.oldmap.get(contID).MailingPostalCode) || 
               (trigger.newMap.get(contID).MailingState != trigger.oldmap.get(contID).MailingState) || (trigger.newMap.get(contID).MailingCountry != trigger.oldmap.get(contID).MailingCountry)) 
            {
                
                conIds.add(contID);
            }
            
            
            if((trigger.newMap.get(contID).PRMPrimaryContact__c != trigger.oldMap.get(contID).PRMPrimaryContact__c ) && (trigger.newMap.get(contID).PRMPrimaryContact__c == false))
            {
                lstPrimaryContactIds.add(contID);
                system.debug('--Primary Contact--'+lstPrimaryContactIds);
            }
        } 
        
        if(!lstPrimaryContactIds.isEmpty()){
            List<Id> partnerUserIds = new List<Id>();
            for(User u :[SELECT Id,IsprmSuperUser,ContactId FROM User WHERE ContactId IN :lstPrimaryContactIds]){
                system.debug('--User On--'+u);
                partnerUserIds.add(u.Id);
                }
                system.debug('--User List--'+partnerUserIds);
            if(!partnerUserIds.isEmpty())
                AP_PRMAccountRegistration.updateAsPartnerSuperUser(partnerUserIds);
         }
          
        if(!setNewPartnerContactId .isEmpty()) {
          
            if (!setNewPartnerContactId.isEmpty())
              AP_Contact_PartnerUserUpdate.insertPartnerUser (setNewPartnerContactId );
        }
        
    }
    if(!setAccountId.isEmpty() && (Utils_SDF_Methodology.canTrigger('AP_AAP_AccountProgramUpdate')))
    {
        list<Id> listProgramId = new list<Id>();
        list<ACC_PartnerProgram__c> lstAccountProgram = new list<ACC_PartnerProgram__c>();
        lstAccountProgram = [Select id, account__c, PartnerProgram__c,ProgramLevel__c from ACC_PartnerProgram__c where Account__c in :setAccountId and Active__c = true limit 50000];
        
        if(!lstAccountProgram.isEmpty())
            AP_AAP_AccountProgramUpdate.createConAssignedProgram(lstAccountProgram , lstContact);
    }
    
    
    if(!conIds.isEmpty() && (Utils_SDF_Methodology.canTrigger('Handler_ContactAfterUpdate'))) 
    {
        Handler_ContactAfterUpdate.updateBillingAccountNames(conIds, mapConNames);
        Handler_ContactAfterUpdate.BeginZuoraContactFlow(conIds);   
    }
    
    //////Fielo Syncrhonize//////////////////
        if(!Test.isRunningTest() && (Utils_SDF_Methodology.canTrigger('SyncObjects'))){
        SyncObjects instance = new SyncObjects(trigger.New);
        boolean allornone = false;
        if(!instance.currentState.error ){
            SyncObjectsResult result = instance.syncronize(allornone);
        }
    
    }
    ///////////////////////////////////////////
    
    /*
    Akram: PRM Listen to Fields (via Properties and Activities Catalog)
    Assign properties if Fields are corresponding to valid values
    */
    if(Utils_SDF_Methodology.canTrigger('PRM_FieldChangeListener')){
        System.debug('Begins PRM_FieldChangeListener');
        PRM_FieldChangeListener.checkFieldFromContactTrigger(trigger.oldMap, trigger.newMap);
                }
}