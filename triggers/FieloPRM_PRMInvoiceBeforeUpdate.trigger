/******************************************
* Developer: Fielo Team                   *
*******************************************/
trigger FieloPRM_PRMInvoiceBeforeUpdate on FieloPRM_Invoice__c (Before Update) {


    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_PRMInvoiceTriggers')){
        FieloPRM_AP_PRMInvoiceTriggers.checksDuplicates();
    }
   
}