/*
Author:Siddharth N(GD Solutions)
Purpose: To avoid the Lock row error when updating this record.
*/
trigger CAPBeforeUpdate on SFE_IndivCAP__c (before update) {
    if(Utils_SDF_Methodology.canTrigger('CAPBeforeUpdate'))
    {        
        Integer nooffuturemethodsrunning=[SELECT count() FROM AsyncApexJob WHERE JobType = 'Future' AND ApexClassId = '01pA0000002hIIu' AND MethodName = 'performInsertForPlaftormAccounts' AND CompletedDate = null and createdbyid=:UserInfo.getUserId()];
        for(SFE_IndivCAP__c cap:Trigger.New)
        {
            if(nooffuturemethodsrunning>0)
            cap.addError(Label.CLSEP12SLS10);
        }
    }
}