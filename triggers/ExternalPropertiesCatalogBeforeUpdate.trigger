trigger ExternalPropertiesCatalogBeforeUpdate on ExternalPropertiesCatalog__c (before Update) {
    if(Utils_SDF_Methodology.canTrigger('PRM_ExternalPropertiesCatalog')) {
        PRM_ExternalPropertiesCatalog.checkBFOPropertiesFieldName(trigger.new);
    }
}