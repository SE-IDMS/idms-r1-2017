/*
@Author: Nabil ZEGHACHE
Created Date: 2016-06-16
Description: This trigger fires on Interest on Opportunity Notification on Before Update
**********
Scenarios:
**********
1.  Update the amount of the interest

Date Modified       Modified By         Comments
------------------------------------------------------------------------

*/
trigger InterestOnOpportunityNotifBeforeUpdate on InterestOnOpportunityNotif__c (before update) {
    System.debug('#### Starting InterestOnOpportunityNotifBeforeUpdate trigger');
    if(Utils_SDF_Methodology.canTrigger('SRV_IntOnOppNotif_BU')) {
        List<InterestOnOpportunityNotif__c> oppNotifToBeProcessed = new List<InterestOnOpportunityNotif__c>();
        for (InterestOnOpportunityNotif__c newOppNotif : Trigger.new) {
            InterestOnOpportunityNotif__c oldOppNotif = Trigger.oldMap.get(newOppNotif.Id);
            if (
                (newOppNotif.Leading_Business_BU__c != null && !newOppNotif.Leading_Business_BU__c.equals(oldOppNotif.Leading_Business_BU__c))
                || (newOppNotif.Type__c != null && !newOppNotif.Type__c.equals(oldOppNotif.Type__c))
                || (newOppNotif.Interest__c != null && !newOppNotif.Interest__c.equals(oldOppNotif.Interest__c))
               ) {
                oppNotifToBeProcessed.add(newOppNotif);
            }
        }
        if (oppNotifToBeProcessed != null && oppNotifToBeProcessed.size() > 0) {
            AP_InterestOnOpportunityNotif.updateInterestAmount(oppNotifToBeProcessed);
        }
    }
}