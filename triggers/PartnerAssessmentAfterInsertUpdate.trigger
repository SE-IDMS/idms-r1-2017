/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 27 July 2013
    Description  : 1. Update ORF status to 'Pending' once the first Assessment is linked to the ORF
              
    Modified By  : Renjith Jose
    Modified Date: 15 Jan 2015
    Description  : EUS 049693   
        
***************************************************************************************************************************/
trigger PartnerAssessmentAfterInsertUpdate on PartnerAssessment__c (after insert, after update) {
    list<Id> lstORFId = new list<Id>();
    for(PartnerAssessment__c assessment : trigger.new)
    {
        if(assessment.OpportunityRegistrationForm__c != null && assessment.TECH_Status__c != 'draft' )
            lstORFId.add(assessment.OpportunityRegistrationForm__c);
    }
    
    if(lstORFId.size() > 0)
        AP_UpdateORFFields.updateORFStatus(lstORFId);
}