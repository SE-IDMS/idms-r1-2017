/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 12 March 2013
    Description : For May13 Release:
                 
                 1. Calls AP_PartnerPRG_CreateCountryProgram to update Country Program when Global Program is updated.
                 
    Modified By : Renjith Jose, 17-feb-2014.
    Description : BR-4703, APR14 Release. 
                  Deactivate dependent Program Levels & Features    
    
********************************************************************************************************************/
trigger PartnerProgramAfterUpdate on PartnerProgram__c (after update) {
    if(Utils_SDF_Methodology.canTrigger('AP_PartnerPRG'))
    {
        AP_PartnerPRG_CountryProgramMethods.updateCountryProgram(trigger.newMap,trigger.oldMap);
        //SART: Release APR14,BR-4702 
        //Description:Deactivate dependent Program Levels & Features
          AP_PartnerPRG_CountryProgramMethods.deactivateProgramLevelsFeatures(trigger.new);
        //END: Release APR14
    }   
}