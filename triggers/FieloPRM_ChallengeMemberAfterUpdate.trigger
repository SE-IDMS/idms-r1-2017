/********************************************************************
* Company: Fielo
* Created Date: 02/08/2016
* Description: Trigger after update for object Challenge Member
********************************************************************/
trigger FieloPRM_ChallengeMemberAfterUpdate on FieloCH__ChallengeMember__c (after update) {
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_ChallengeMemberTrigger')){
        FieloPRM_AP_ChallengeMemberTrigger.demotionChallengeReward();
        FieloPRM_AP_ChallengeMemberTrigger.processUnsubscribeMember();
    }
}