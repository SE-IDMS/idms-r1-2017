trigger FSBBeforeInsert on FieldServiceBulletin__c (Before Insert) {
    
    if(Utils_SDF_Methodology.canTrigger('FSBBI')) {
    
        Set<String> stProducts = new Set<String>();
        List<FieldServiceBulletin__c> toupdateFSB = new List<FieldServiceBulletin__c>();
        
        for(FieldServiceBulletin__c fsb : Trigger.New) {
            if(fsb.FSBApprover__c == null) {
                stProducts.add(fsb.ProductLine__c);
                toupdateFSB.add(fsb);
            }       
        }
        System.debug('!!!! stProducts : ' + stProducts);
        System.debug('!!!! toupdateFSB : ' + toupdateFSB);
        
        if(!stProducts.isEmpty())
            AP_FieldServiceBulletin.setFSBApproverOnInsert(stProducts,toupdateFSB);
    }
}