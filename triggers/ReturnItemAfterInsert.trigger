/*
    Author          : Nicolas PALITZNE (Accenture)
    Date Created    : 01/12/2011
    Description     : Return Item Before Delete Trigger
    Modified on     : 23-dec-2012
    Modified by     : Sukumar Salla
*/

trigger ReturnItemAfterInsert on RMA_Product__c (after insert)
{
if(Utils_SDF_Methodology.canTrigger('AP_RR_FieldUpdate'))
   {
        AP_RR_FieldUpdate.updateRequestTypeOnRR(trigger.new);
        //Oct 15 rel - Divya M  --> BR-7721
        AP_RR_FieldUpdate.updateShippedFromCustomer(trigger.new);
        //ends
   }
/* 
    if(Utils_SDF_Methodology.canTrigger('AP34'))
    {
        AP34_RMAMailMerge.addReturnedProducts(trigger.new); 
   }
   
   
 
   
    if(Utils_SDF_Methodology.canTrigger('AP_RI_RMAMailMerge'))
    {
        //Set<id> riSet= new Set<id>();
       // for(RMA_Product__c  rip:trigger.new)
       // riset.add(rip.id);
    
        //List<RMA_Product__c> riprdList=[Select Name, RMA__c, Quantity__c, DateCode__c, ProductCondition__c, toLabel(CustomerResolution__c) from RMA_Product__c where id in: riset];
          
        AP_RI_RMAMailMerge.UpdateRITechFieldsOnRMA(trigger.new);
    }
    */

}