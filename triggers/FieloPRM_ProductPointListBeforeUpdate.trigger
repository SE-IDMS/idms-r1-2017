/******************************************
* Developer: Fielo Team                   *
*******************************************/
trigger FieloPRM_ProductPointListBeforeUpdate on FieloPRM_ProductPointList__c (Before Update) {

    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_ProductPointListTriggers')){
        FieloPRM_AP_ProductPointListTriggers.validatesPointListOverlappingDates();
    }
   
}