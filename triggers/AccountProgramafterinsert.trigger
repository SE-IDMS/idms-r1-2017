/*****************************************************************************************
    Author : Shruti Karn
    Description : For May 2013 Release to:
                    1. Create a Features/Requirements related to Account from Country Program for the selected Level
    Updated By : Shruti Karn
    Date : 31 July 2013
    Description : For October 2013 PRM Release
                  To add Assessment with Automatic Assignemnet checkbox checked to Account Assigned Program.                
                                                         
*****************************************************************************************/
trigger AccountProgramafterinsert on ACC_PartnerProgram__c (after insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_AAP_AccountProgramUpdate') || test.isRunningTest())
    {
        AP_AAP_AccountProgramUpdate.createACCChildRecords(trigger.new);

        //Srinivas
        AP_AAP_AccountProgramUpdate.checkAssignedLevels(trigger.new); 
        map<String,set<Id>> mapAAPProgram = new map<String,set<Id>>();
        for(ACC_PartnerProgram__c accPRogram : trigger.new)
        {
            if(accPRogram.PartnerProgram__c != null && accPRogram.ProgramLevel__c != null)
            {
                if(!mapAAPProgram.containsKey(accPRogram.PartnerProgram__c+':'+accPRogram.ProgramLevel__c))
                    mapAAPProgram.put(accPRogram.PartnerProgram__c+':'+accPRogram.ProgramLevel__c , new set<id> {(accPRogram.Id)});
                else
                    mapAAPProgram.get(accPRogram.PartnerProgram__c+':'+accPRogram.ProgramLevel__c).add(accPRogram.Id);
            }
        }
        if(!mapAAPProgram.isEmpty())
                AP_AAP_AccountProgramUpdate.addAssessment(mapAAPProgram);
    }
}