/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 11/09/2011
    Description     : Budget Before Update event trigger.
*/

trigger BUD_BudgetBeforeUpdate on Budget__c (before update) 
{
    System.Debug('****** BUD_BudgetBeforeUpdate Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('BudgetTriggerUpdate'))
    {
        System.Debug('Value New Update: ' + Trigger.new);
        System.Debug('Value NewMap Update: ' + Trigger.newMap);
        
        BUD_BudgetTriggers.budgetBeforeUpdate(Trigger.newMap, Trigger.oldMap);

    }
    System.Debug('****** BUD_BudgetBeforeUpdate Trigger End ****'); 
}