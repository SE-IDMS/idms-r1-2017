/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: 
********************************************************************/

trigger FieloPRM_MemberPropertiesAfterDelete  on MemberExternalProperties__c (After Delete) {
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberPropertiesAfterDelete')){
        FieloPRM_AP_MemberPropertiesTriggers.deleteBadgeMember(trigger.old);
    }
}