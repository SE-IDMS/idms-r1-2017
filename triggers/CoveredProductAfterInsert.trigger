/*Created by: Deepak Kumar
Date:12/08/2013
For the purpose of Updating Installed Product Field Under Contract Field Value From service COntract Active Status Value.
OCT13 Release
*/
trigger CoveredProductAfterInsert on SVMXC__Service_Contract_Products__c(after insert)
{//added by suhail for jun2016 release start
    AP_CoveredProduct.PmcoverageCreate();

    if(Utils_SDF_Methodology.canTrigger('SVMX18')){
        
       
        Set<Id> scId = New Set<Id>();
        Set<Id> ipId = New Set<Id>();
         set<id> sccid = new set<id>();
        set<id>ipset= new set<id>();
         
          
        for(SVMXC__Service_Contract_Products__c cov :trigger.New)
        {  

            if(cov.SVMXC__Installed_Product__c !=null){
                sccid.add(cov.SVMXC__Service_Contract__c); 
            }
            if(cov.SVMXC__Installed_Product__c !=null){
                 ipset.add(cov.SVMXC__Installed_Product__c); 
            }
        
           // if(sccid!=null && sccid.size()>0)
            //AP_CoveredProduct.PMvisitUpdatedCoveredProduct(sccid);
        
        
            if(cov.SVMXC__Service_Contract__c !=null && cov.SVMXC__Installed_Product__c !=null)
            {
                scId.add(cov.SVMXC__Service_Contract__c);
                ipId.add(cov.SVMXC__Installed_Product__c);
            }
          
        }
        AP_IpUndrContractFldUpdate.IpUnderContrUpdate(scId,ipId); 
        if(sccid!=null && sccid.size()>0){
            AP_ServiceContract.SCIBSyncUpdate(sccid);
        }            
        if(ipset!=null && ipset.size()>0){
            Ap_Installed_Product.IBchangeMaster(ipset);
        }
    
    }
    
}