trigger ProgramRequirementAfterUpdate on ProgramRequirement__c (after update) {
    if(Utils_SDF_Methodology.canTrigger('AP_PGR_CountryRequirementUpdate'))
    {
        list<ID> lstreqProgramID = new list<ID>();
        map<ID,list<ProgramRequirement__c>> mapLevelRequirement = new map<ID,list<ProgramRequirement__c>>();
        //Srinivas July13  BR-3082
        List<ProgramRequirement__c> lstProgramReqs = new List<ProgramRequirement__c>();
        List<ProgramRequirement__c> lstActiveProgReqs = new List<ProgramRequirement__c>();

        for(Id reqID :  trigger.newMap.keySet())
        {
            if(trigger.newMap.get(reqID).active__c != trigger.oldMap.get(reqID).active__c && trigger.newMap.get(reqID).active__c)
            {
                lstreqProgramID.add(trigger.newMap.get(reqID).PartnerProgram__c);//Global Partner Program
                if(!mapLevelRequirement.containsKey(trigger.newMap.get(reqID).ProgramLevel__c))
                    mapLevelRequirement.put(trigger.newMap.get(reqID).ProgramLevel__c, new list<ProgramRequirement__c> {(trigger.newMap.get(reqID))});
                else
                    mapLevelRequirement.get(trigger.newMap.get(reqID).ProgramLevel__c).add(trigger.newMap.get(reqID));

                lstActiveProgReqs.add(trigger.newMap.get(reqID));
                
            }
            if(trigger.newMap.get(reqID).active__c != trigger.oldMap.get(reqID).active__c && trigger.newMap.get(reqID).active__c ==  false)
            {
                ////Srinivas July13  BR-3082
                lstProgramReqs.add(trigger.newMap.get(reqID));
            } 
        }
        if(!mapLevelRequirement.isEmpty())
            AP_PGR_CountryRequirementUpdate.creatCountryRequirement(mapLevelRequirement , lstreqProgramID , trigger.newMap.keySet());
        AP_PGR_CountryRequirementUpdate.updateCountryRequirement(trigger.newMap , trigger.oldMap);

        if(!lstProgramReqs.isEmpty())
            AP_PGR_CountryRequirementUpdate.updateAccountContactRequirements(lstProgramReqs);

        if(!lstActiveProgReqs.isEmpty())
            AP_PGR_CountryRequirementUpdate.newAcccountAssigendRequirements(lstActiveProgReqs);
    }
}