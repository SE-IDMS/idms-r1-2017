trigger InstalledProductSwapBeforeInsert on Swap_Asset__c (before insert) {
    //map<id,SVMXC__Installed_Product__c> ipmap = new  map<id,SVMXC__Installed_Product__c>();
    set<id>ipset= new set<id>();
    for(Swap_Asset__c sw:trigger.new)
    {
        ipset.add(sw.DeinstalledInstalledProduct__c);
    }
    
    map<id,SVMXC__Installed_Product__c> ipmap= new map<id,SVMXC__Installed_Product__c>([select id,name,SVMXC__Date_Installed__c from SVMXC__Installed_Product__c where id in:ipset]);
    for(Swap_Asset__c ssw:trigger.new)
    {
            ssw.CustomerDateTime__c=ipmap.get(ssw.DeinstalledInstalledProduct__c).SVMXC__Date_Installed__c;
    }
  
}