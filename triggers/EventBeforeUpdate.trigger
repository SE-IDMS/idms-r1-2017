/*
Author        : Christian Tran (ATS)
Date Created  : 12/04/2011
Description   : Event before Update Trigger
*/

trigger EventBeforeUpdate on Event (before update) 
{    
    if(Utils_SDF_Methodology.canTrigger('EVT_BEFOREUPDATE'))
    {
        System.debug('************** Log7 >> '+Limits.getQueries());
        List<Event> lstEvents = new List<Event>();
        set<id> setAccIds = new set<id>();
        List<Event> eventsRelTo = new List<Event>();
        
        for(Event event : trigger.new)
        {
            Event beforeUpdate = trigger.oldmap.get(event.id);
            if(event.StartDateTime!=null)
            event.TECHMonthActivity__c = event.StartDateTime.month();

            String strWhatId = event.WhatId; 
            String srtWhoId = event.whoId;
            if(strWhatId != null && strWhatId.startsWith('001') && event.WhatId != beforeUpdate.WhatId)
            {
                setAccIds.add(event.WhatId);
            }
            if(strWhatId != null && strWhatId.startsWith('001') && event.Location == null)
            {
                lstEvents.add(event);
            }
            /* Modified for BR-5608 */

            if(event.whoId!=null && srtWhoId.startsWith(Schema.SObjectType.Contact.getKeyPrefix()) && event.RecordTypeId==Label.CLOCT14SLS22)
            {
                //If Related To is blank:
                if(event.whatId==null)
                {
                    eventsRelTo.add(event);  
                }
                else if (strWhatId!=null && strWhatId.startsWith(Schema.SObjectType.Account.getKeyPrefix()) && (event.WhoId!= trigger.oldMap.get(event.Id).whoId || event.WhatId!= trigger.oldMap.get(event.Id).WhatId))
                {
                    //else if Related To is filled, check if it is an Account. Check if any of Account/Contact is changed
                    eventsRelTo.add(event);  
                }   
            }
            /* End of Modification for BR-5608 */
        }
        if(setAccIds.size() > 0)
        {
            set<id> setPerAccs = new set<id>();
            setPerAccs = AP25_Task.getPersonAccountIds(setAccIds);

            if(setPerAccs != null)
            for(Event ev:Trigger.new)
            {
                if(setPerAccs.contains(ev.whatId))
                ev.addError(System.Label.CLDEC12AC13);
            }
        }
        /* Modified for BR-5608 */
        if(!(eventsRelTo.isEmpty()))
        {
            AP_EventRecordUpdate.populateRelatedTo(eventsRelTo);
        }
        /* End of Modification for BR-5608 */       
        AP_EventRecordUpdate.updateEventLocation(lstEvents);
        System.debug('************** Log8 >> '+Limits.getQueries());
    }
}