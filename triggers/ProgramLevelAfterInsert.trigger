/*
Shruti Karn             Mar2013    PRM May13 Release
Srinivas Nallapati      Mar2013    PRM May13 Release    -   Level heirarchy validation
*/
trigger ProgramLevelAfterInsert on ProgramLevel__c (after insert) {
    //list<ProgramLevel__c> lstNewPrgLevel = new list<ProgramLevel__c>();
    if(Utils_SDF_Methodology.canTrigger('AP_PRL_CountryLevelUpdate'))
    {
        map<Id,list<ProgramLevel__c>> mapGlobalProgramLevel = new map<Id, list<ProgramLevel__c>>();
        for(ProgramLevel__c PRGLevel : trigger.new)
        {
            if(PRGLevel.recordtypeid == Label.CLMAY13PRM18 && PRGLevel.levelstatus__c == Label.CLMAY13PRM19)
            {
                if(!mapGlobalProgramLevel.containsKey(PRGLevel.partnerprogram__c))
                    mapGlobalProgramLevel.put(PRGLevel.partnerprogram__c , new list<ProgramLevel__c> {(PRGLevel)});
                else
                    mapGlobalProgramLevel.get(PRGLevel.partnerprogram__c).add(PRGLevel);
            }
        }
        
        if(!mapGlobalProgramLevel.isEmpty())
            AP_PRL_CountryLevelUpdate.creatCountryLevel(mapGlobalProgramLevel , null);

    }
    //AP_PRL_CountryLevelUpdate.checkLevelHierarchy(trigger.new);
}