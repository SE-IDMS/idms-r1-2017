/*
    Author          : Chris Hurd (ServiceMax)
    Date Created    : 28-Feb-2013
    Description     : Create Work Order Detail Lines for each Part.
    
    Screnarios:
    1.  When a parts order line is created/edited create/edit a matching line for the work details on the work order
    2.  When Commercial Reference is set or changed run through the GMR Validation to make sure it is valid
    //Modification on 12-05-2015
    1.  Work Details Creation From Parts Order Line  when RecordType is RMA.
    
*/
trigger PartsOrderLineAfterInsert on SVMXC__RMA_Shipment_Line__c (before delete, after insert, after update) {

    if(Utils_SDF_Methodology.canTrigger('SVMXC_PO_Line_01'))
    {
        List<SVMXC__Service_Order_Line__c> pollist = new List<SVMXC__Service_Order_Line__c>();
        Map<Id, RecordType> poRTMap = new Map<Id, RecordType>([SELECT Id FROM RecordType WHERE SobjectType = 'SVMXC__RMA_Shipment_Line__c' AND DeveloperName IN ('Stock_Material', 'Non_Stock_Material_with_Reference', 'Non_Stock_Material_without_Reference')]);
        String RMA_RT = System.Label.CLAPR15SRV66;
        String Parts_RT = System.Label.CLAPR15SRV61;
        String ProductServiced_RT = System.Label.CLAPR15SRV60;
        
        if (! trigger.isDelete)
        {
            
            String Reference = System.Label.CLOCT15SRV06;
            String NonReference = System.Label.CLOCT15SRV05;
            string PartsOrderRMA = System.Label.CLOCT15SRV04;
            Set<Id> poIdSet = new Set<Id>();
            Set<Id> idSet = new Set<Id>();
            Set<Id> gmrLineIdSet = new Set<Id>();
             set<id> set2 = new set<id>();

        list<SVMXC__RMA_Shipment_Order__c> polist1 = new list<SVMXC__RMA_Shipment_Order__c>();
        list<SVMXC__RMA_Shipment_Order__c> polist2 = new list<SVMXC__RMA_Shipment_Order__c>();
        list<SVMXC__RMA_Shipment_Order__c> polist3 = new list<SVMXC__RMA_Shipment_Order__c>();
        list<SVMXC__RMA_Shipment_Line__c> polinelist1 = new list<SVMXC__RMA_Shipment_Line__c>();
        list<SVMXC__RMA_Shipment_Line__c> polinelist2 = new list<SVMXC__RMA_Shipment_Line__c>();
        list<SVMXC__RMA_Shipment_Line__c> polinelist3 = new list<SVMXC__RMA_Shipment_Line__c>();
        map<id,SVMXC__RMA_Shipment_Order__c> pomap1 = new map<id,SVMXC__RMA_Shipment_Order__c>();
            
            for (SVMXC__RMA_Shipment_Line__c poLine : trigger.new)
            {
                SVMXC__RMA_Shipment_Line__c beforeUpdate = null;
                
                if (trigger.oldMap != null && trigger.oldMap.containsKey(poLine.Id))
                {
                    beforeUpdate = trigger.oldMap.get(poLine.Id);
                }
                
                //Check for scenario 1.
                if (poRTMap.containsKey(poLine.RecordTypeId))
                {
                    idSet.add(poLine.Id);
                    poIdSet.add(poLine.SVMXC__RMA_Shipment_Order__c);
                }
                
                //Check for scenario 2.
                if (poLine.CommercialReference__c != null && (trigger.isInsert || poLine.CommercialReference__c != beforeUpdate.CommercialReference__c))
                {
                    gmrLineIdSet.add(poLine.Id);
                }
                
                
                
                
                      if(trigger.isinsert){
                        if((poLine.recordtypeid == NonReference || poLine.recordtypeid == Reference)&& poLine.Create_RMA_Line__c == true)
                            {
                                set2.add(poLine.SVMXC__RMA_Shipment_Order__c);
                                polinelist1.add(poLine);
                            }
                            }
                if(trigger.isupdate){
                
                 if((poLine.recordtypeid == NonReference || poLine.recordtypeid == Reference)&& poLine.Create_RMA_Line__c == true  && poLine.Create_RMA_Line__c!= trigger.oldmap.get(poLine.id).Create_RMA_Line__c)
                {
                    set2.add(poLine.SVMXC__RMA_Shipment_Order__c);
                    polinelist1.add(poLine);
                }
                
                }

                
            }
            
            
            
             if(set2!=null)
            polist2 = [select id,name,Attention_To__c,SVMXC__Company__c,SVMXC__Service_Order__r.SVMXC__Site__r.LocationCountry__r.name,SVMXC__Contact__c,Plant_Name__c,Work_Order_Country__c,SVMXC__Service_Order__c,Freight_terms__c,SVMXC__Service_Order__r.SVMXC__Contact__c,SVMXC__Service_Order__r.SVMXC__Company__c,SVMXC__Service_Order__r.SVMXC__Site__r.LocationCountry__r.CountryCode__c,Parts_Order_RMA__c,SVMXC__Service_Order__r.SVMXC__Site__r.Name from SVMXC__RMA_Shipment_Order__c where id in:set2];
            if(polist2!=null && polist2.size()>0  && polinelist1!=null){
            
            for(SVMXC__RMA_Shipment_Order__c  po:polist2){
                    if(po.Parts_Order_RMA__c == null){
                    
                    SVMXC__RMA_Shipment_Order__c pord = new SVMXC__RMA_Shipment_Order__c();
                    
                     pord.RecordTypeid = PartsOrderRMA;
                     //pord.Attention_To__c = 
                     pord.SVMXC__Company__c = po.SVMXC__Service_Order__r.SVMXC__Company__c;
                     pord.SVMXC__Contact__c = po.SVMXC__Service_Order__r.SVMXC__Contact__c;
                    // pord.Plant_Name__c =
                     pord.Work_Order_Country__c= po.SVMXC__Service_Order__r.SVMXC__Site__r.LocationCountry__r.name;
                     pord.SVMXC__Service_Order__c = po.SVMXC__Service_Order__c;
                     pord.Acknowledege__c = true; //added by suhail for june 2016 release BR-9102
                     pord.Country_Code__c = po.SVMXC__Service_Order__r.SVMXC__Site__r.LocationCountry__r.CountryCode__c;
                     pord.Freight_terms__c = 'EXW';
                     polist1.add(pord);
                     }
                    
            
            for(SVMXC__RMA_Shipment_Line__c poll:polinelist1){
                    
                    
                    if(poll.SVMXC__RMA_Shipment_Order__c == po.id){

                 if(po.Parts_Order_RMA__c != null){  
                
                
                SVMXC__RMA_Shipment_Line__c poline = new SVMXC__RMA_Shipment_Line__c();
     
                 poline.SVMXC__RMA_Shipment_Order__c = po.Parts_Order_RMA__c;
                 poline.SVMXC__Product__c = poll.SVMXC__Product__c;
                 poline.Generic_reference__c = poll.Generic_reference__c;
                 poline.Free_text_reference__c = poll.Free_text_reference__c;
                 poline.BOMaterialReference__c = poll.BOMaterialReference__c;
                 poline.SVMXC__Service_Order__c = poll.SVMXC__RMA_Shipment_Order__r.SVMXC__Service_Order__c;
                 //poline.Plant_Name__c =
                 poline.RequestedDate__c = poll.RequestedDate__c;
                 poline.SVMXC__Expected_Quantity2__c = poll.SVMXC__Expected_Quantity2__c;
                 poline.SVMXC__Line_Type__c = 'RMA';
                 polinelist2.add(poline);
                   }
                  }
                }
             }
        }
            if(polinelist2!=null && polinelist2.size()>0)
                insert polinelist2;

            if(polist1!=null && polist1.size()>0)
          
            {
            
            Database.SaveResult[] results=  database.insert(polist1,false);
             
                for(Integer k=0;k<results.size();k++ )
                {
                    Database.SaveResult sr =results[k];
                   if(polist2!=null && polist2.size()>0 && polist1!=null && polist1.size()>0){
                       polist2[0].Parts_Order_RMA__c = polist1[0].id;
                   polist3.add(polist2[0]);
                   pomap1.putall(polist3);
                   
                    }
                }
        }
        
        update pomap1.values();
        
        if(polist1!=null && polist1.size()>0 && polinelist1!=null)
        {
        
            AP_Partsorderline.POlineCreation(polist1,polinelist1);
        
        }
            
            

            //Processing for scenario 1.
            if (idSet.size() > 0)
            {
                Map<Id, SVMXC__RMA_Shipment_Order__c> poMap = new Map<Id, SVMXC__RMA_Shipment_Order__c>([SELECT Id, SVMXC__Service_Order__c, SVMXC__Service_Order__r.IsBillable__c FROM SVMXC__RMA_Shipment_Order__c WHERE Id IN :poIdSet]);
                Map<Id, SVMXC__Service_Order_Line__c> woDetailMap = new Map<Id, SVMXC__Service_Order_Line__c>();
                //RecordType wdRT = [SELECT Id FROM RecordType WHERE SObjectType = 'SVMXC__Service_Order_Line__c' AND DeveloperName = 'Parts' LIMIT 1];
                String workDetailPartsRT = System.Label.CLJUL15SRV04;
                for (SVMXC__Service_Order_Line__c detail : [SELECT Id, SVMXC__Product__c, SVMXC__Requested_Quantity2__c, PartsOrderLine__c FROM SVMXC__Service_Order_Line__c WHERE PartsOrderLine__c IN :idSet])
                {
                    woDetailMap.put(detail.PartsOrderLine__c, detail);
                }
                List<SVMXC__Service_Order_Line__c> updatedDetailList = new List<SVMXC__Service_Order_Line__c>();
                
                for (SVMXC__RMA_Shipment_Line__c poLine : trigger.new)
                {
                    if (idSet.contains(poLine.Id))
                    {
                        SVMXC__RMA_Shipment_Order__c po = poMap.get(poLine.SVMXC__RMA_Shipment_Order__c);
                        
                        if (po.SVMXC__Service_Order__c != null)
                        {
                            SVMXC__Service_Order_Line__c detail = null;
                            
                            if (woDetailMap.containsKey(poLine.Id))
                            {
                                detail = woDetailMap.get(poLine.Id);
                            }
                            else
                            {
                                detail = new SVMXC__Service_Order_Line__c();
                                detail.PartsOrderLine__c = poLine.Id;
                                detail.SVMXC__Service_Order__c = po.SVMXC__Service_Order__c;
                                //detail.RecordTypeId = wdRT.Id;
                                detail.RecordTypeId = workDetailPartsRT;
                            }
                            List<String> detailList = new List<String>();
                            if (poLine.CommercialReference__c != null)
                            {
                                detailList.add(poLine.CommercialReference__c);
                            }
                            if (poLine.Part__c != null)
                            {
                                detailList.add(poLine.Part__c);
                            }
                           //commented by anand for Defect 10855
                           /* if (poLine.Vendor__c != null)
                            {
                                detailList.add(poLine.Vendor__c);
                            }*/
                            //Comment end
                            detail.Parts__c = String.join(detailList, ', ');
                            detail.ShippedQuantity_del__c = poLine.SVMXC__Expected_Quantity2__c;
                            detail.IsBillable__c = po.SVMXC__Service_Order__r.IsBillable__c; // Added for May Release 2014 BR-5517
                            updatedDetailList.add(detail);
                        }
                    }
                }
                
                if (updatedDetailList.size() > 0)
                {
                    database.upsert(updatedDetailList,false);
                }
            }
            
            //Processing for scenario 2
            if (gmrLineIdSet.size() > 0)
            {
                ServiceMaxUtilsPartsOrder.updateGMRValidation(gmrLineIdSet);
            }
        }
        else
        {
            Set<Id> detailIdSet = new Set<Id>();
            for (SVMXC__RMA_Shipment_Line__c poLine : trigger.old)
            {
                if (poRTMap.containsKey(poLine.RecordTypeId)) 
                {
                    detailIdSet.add(poLine.Id);
                }
            }
            
            if (detailIdSet.size() > 0)
            {
               delete [SELECT Id FROM SVMXC__Service_Order_Line__c WHERE PartsOrderLine__c IN :detailIdSet AND (InstalledQuantity__c = null OR InstalledQuantity__c = 0)];
            }
        }
        //Work Detail Creation From Parts Order Line When RecordType Is RMA.
        List<SVMXC__RMA_Shipment_Line__c> polinelist = new List<SVMXC__RMA_Shipment_Line__c>();
        set<Id> polset = new set<Id>();
       string NONFSRfile=System.Label.CLMAY15SRV02 ;
        map<Id,SVMXC__RMA_Shipment_Order__c> partsOrderRec = new map<Id,SVMXC__RMA_Shipment_Order__c>();
        if(trigger.isafter){
            if(trigger.isInsert){
            if(userinfo.getProfileId()!=NONFSRfile){
                for (SVMXC__RMA_Shipment_Line__c poLine : trigger.new)
                {
                    if(poLine.RecordTypeId ==RMA_RT && poLine.SVMXC__Service_Order__c!=null){
                        polset.add(poLine.SVMXC__RMA_Shipment_Order__c);
                        polinelist.add(poLine);
                    
                    }
                }
                if(polset!=null){
                    List<SVMXC__RMA_Shipment_Order__c> rmalist = [Select Id,Name From SVMXC__RMA_Shipment_Order__c Where id in :polset];
                    partsOrderRec.putAll(rmalist);
                }
            
                if(polinelist.size()>0)
                {
                    for(SVMXC__RMA_Shipment_Line__c poLine :polinelist){
                        
                        SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();

                        wd.TrackingNumber__c =poLine.Tracking_number__c;
                        //wd.ReturnedQuantity__c=poLine.SVMXC__Expected_Quantity2__c;
                        //added by suhail for june2016 release start
                        if((poline.PQAnumber__c != null)||(poline.PQAnumber__c != '')){
                        wd.PQA__c = poline.PQAnumber__c ; //added by suhail for june2016 release 
                        
                        }
                        //added by suhail for june2016 release end
                        wd.Return_Reason__c=poLine.Return_Reason__c;
                        wd.Parts_Order_Line_RMA__c =poLine.Id;
                        if(partsOrderRec.containsKey(poLine.SVMXC__RMA_Shipment_Order__c)){
                            wd.RMANumber__c = partsOrderRec.get(poLine.SVMXC__RMA_Shipment_Order__c).Name ;
                        }
                        wd.SVMXC__Service_Order__c = poLine.SVMXC__Service_Order__c;
                        if(poLine.SVMXC__Serial_Number__c!=null){
                            wd.SVMXC__Serial_Number__c = poLine.SVMXC__Serial_Number__c;
                            //wd.RecordTypeId =ProductServiced_RT;
                            wd.RecordTypeId =Parts_RT;
                        }
                        else{
                            wd.RecordTypeId =Parts_RT;
                            wd.SVMXC__Product__c =poLine.SVMXC__Product__c;
                            
                        }
                        pollist.add(wd);
                    }
                }
                if(pollist.size()>0){
                   database.insert(pollist,false);
                }
            }
         }
        }
    }
}