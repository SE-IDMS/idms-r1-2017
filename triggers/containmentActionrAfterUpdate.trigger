//*********************************************************************************
// Trigger Name     : containmentActionrAfterUpdate
// Purpose          : Containment Action After Update event trigger
// Created by       : Global Delivery Team
// Date created     : 27th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/

trigger containmentActionrAfterUpdate on ContainmentAction__c (after update) {
    System.Debug('****** containmentActionrAfterUpdate Trigger Start ****');      
    
    if(Utils_SDF_Methodology.canTrigger('AP1002')){              
        Map<Id,ContainmentAction__c> cntnActnOldMap = new Map<Id,ContainmentAction__c>(); 
        Map<Id,ContainmentAction__c> cntnActnNewMap = new Map<Id,ContainmentAction__c>(); 
        
        for(ContainmentAction__c cntnActn: Trigger.new){
            if(Trigger.oldMap.get(cntnActn.id).Owner__c != Trigger.newMap.get(cntnActn.id).Owner__c){
                cntnActnOldMap.put(cntnActn.id,Trigger.oldMap.get(cntnActn.id));
                cntnActnNewMap.put(cntnActn.id,Trigger.newMap.get(cntnActn.id));
            }
        }
        
        if(cntnActnOldMap.size()>0 && cntnActnNewMap.size()>0){ 
            AP1002_ContainmentAction.updateContainmentOwnerToProblemShare(cntnActnOldMap,cntnActnNewMap); 
        }                  
    }                                               
    System.Debug('****** containmentActionrAfterUpdate Trigger End ****'); 
}