/*
    Author          : Priyanka Shetty (Schneider Electric)
    Date Created    : 11/04/2013
    Description     : Change Request Funding Entity Before Insert event trigger.
*/
trigger CHR_FundingEntityBeforeInsert on ChangeRequestFundingEntity__c (before insert) 
{
    System.Debug('****** CHR_FundingEntityBeforeInsert Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('CRFundingEntityTriggerInsert'))
    {

        CHR_FundingEntityTriggers.fundingentityBeforeInsert(Trigger.new);
    }
    System.Debug('*** CHR_FundingEntityBeforeInsert Trigger End **');   
      
}