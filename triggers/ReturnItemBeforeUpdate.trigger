/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | Before Update Trigger                                                              |
|                       |                                                                                    |
|     - Object(s)       | RMA_Product__c                                                                     |
|     - Description     |   - Used to set the Shipping Address on a Return Item                              |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | April, 27th 2012                                                                   |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
trigger ReturnItemBeforeUpdate on RMA_Product__c (Before Update)
{
      //  AP_TESTConnector.updateRI(trigger.new);

    System.Debug('****** ReturnItemBeforeUpdate Trigger starts ****');  
   /*
    if(Utils_SDF_Methodology.canTrigger('AP35'))
    {
       AP35_addItemToRMA.updateShippingAddress(trigger.new);
    }
    */
    // October 2015 Release - Gayathri Shivakumar
    if(Utils_SDF_Methodology.canTrigger('AP35'))
    {
       AP35_addItemToRMA.updateReturnAddress(trigger.new);
    }
    List<RMA_Product__c> RIs = new List<RMA_Product__c>();
    if(Utils_SDF_Methodology.canTrigger('AP_RI_FieldUpdate'))
    {
    AP_RR_FieldUpdate.updateRequestTypeOnRi(trigger.new);
    for(RMA_Product__c RI: trigger.new)
    {
    Trigger.Newmap.get(RI.id).TECH_PriorLS__c=Trigger.OldMap.get(RI.id).LogisticsStatus__c;
        if((Trigger.oldMap.get(RI.id).LogisticsStatus__c != Trigger.newMap.get(RI.id).LogisticsStatus__c) && Trigger.newMap.get(RI.id).LogisticsStatus__c==Label.CLMAY13RR15)
        {
            RIs.add(RI);
            System.debug('##RI##'+ RIs);
                
        }
    }
      
    if(RIs.Size()>0)
    {
       AP_RI_FieldUpdate.updateCaseOwner(RIs);
    }
   } 
}