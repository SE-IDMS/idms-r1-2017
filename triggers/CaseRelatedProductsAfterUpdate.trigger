//********************************************************************************************************
//  Trigger Name            : CaseRelatedProductsAfterUpdate
//  Purpose                 : CSE_RelatedProduct__c  After update event trigger , 
//                            Update the sibling Products If Master Product is checked
//  Created by              : Vimal Karunakaran (Global Delivery Team)
//  Date created            : 25th August 2013
///*******************************************************************************************************/

/*
    Modification History    :
    Modified By             : 
    Modified Date           : 
    Description             : 
*/

trigger CaseRelatedProductsAfterUpdate on CSE_RelatedProduct__c (after update) {
    System.Debug('****** CaseRelatedProductsAfterUpdate Trigger Start ****');
    
    if(Utils_SDF_Methodology.canTrigger('AP_CaseRelatedProduct_Handler')){
        AP_CaseRelatedProduct_Handler.unCheckOtherMasterProduct(Trigger.oldMap,Trigger.newMap,false);
    }
    System.Debug('****** CaseRelatedProductsAfterUpdate Trigger Ends ****');
}