/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 07-Feb-2013
    Description     : Connect Country Cascading Milestones After Insert event trigger.
*/

trigger ConnectCountryCascadingMilestoneAfterInsert on Country_Cascading_Milestone__c (after insert) {
System.Debug('****** ConnectCountryCascadingMilestoneBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CountryCascadingMilestoneAfterInsert'))
    {
        ConnectCountryCascadingMilestoneTriggers.ConnectCountryCascadingMilestoneAfterInsert(Trigger.new);       
        
        
 }
 System.Debug('****** ConnectCountryCascadingMilestoneBeforeInsert Trigger End ****'); 

}