trigger OPP_ProjectTeamTriggers on OPP_ProjectTeam__c (after insert,after update,after Delete) {
    //As per best practise one trigger per object.
    //To keep it simple no action added for deletion
    //for Insertion
    if(Trigger.isInsert){
        System.debug('>>>>>>Entered - after insert/after update trigger of Project Team>>>>');
        Map<Id,OPP_ProjectTeam__c> PrjTeamMap = new Map<Id,OPP_ProjectTeam__c>();             
        for(OPP_ProjectTeam__c pt:Trigger.new)    
            PrjTeamMap .put(pt.id,pt);   
         //pass the newly created Project team members for adding in the share table to access the customer Project.   
        AP_ProjectTeam_SharingAccessToProject.insertPrjTeamMember(PrjTeamMap);         
       System.debug('>>>>>>Exit - after insert trigger of Project Team>>>>');  
    }

    //if the team member is changed add new team member to the Customer Project share.
    //to keep it simple old or prev team member is not removed from the customer Project share. 
    if(Trigger.isUpdate){
        System.debug('>>>>>>Entered - after update trigger of Project Team>>>>>>');
        Map<id,OPP_ProjectTeam__c> prjTeamMap = new Map<id,OPP_ProjectTeam__c>();
        for(OPP_ProjectTeam__c prt:Trigger.new)
        {
            if(prt.User__c!= trigger.Oldmap.get(prt.id).User__c)
                prjTeamMap .put(prt.id,prt); 
        }

        if(prjTeamMap .size()>0)
        {
            AP_ProjectTeam_SharingAccessToProject.insertPrjTeamMember(PrjTeamMap);  
        }
        System.debug('>>>>>>Exit - after update trigger of Project Team>>>>');  
    }
    //if Team Member is deleted remove the share record from the Project share object
     if(Trigger.isDelete){
         System.debug('>>>>>>Entered - after delete trigger of Project Team>>>>>>');
         AP_ProjectTeam_SharingAccessToProject.deletePrjTeamMember(trigger.oldmap); 
         System.debug('>>>>>>Exit - after delete trigger of Project Team>>>>'); 
     }
}