/*
    Author          : ACN 
    Date Created    : 19/07/2011
    Description     : Lead Before Update event trigger.
    18/Apr/2012     Srinivas Nallapati  June Mkt   , When 
    
    14-June-2012    Srinivas Nallapati  Sep12 Mkt June      BR-1897: If the lead status is changed by a lead qualifier, he/she should automatically become the lead owner.
*/   
trigger LeadBeforeUpdate on Lead(before update) 
{
    if(Utils_SDF_Methodology.canTrigger('AP19'))
    {
        System.Debug('****** LeadBeforeUpdateTrigger - Updating Lead fields Starts ****');  
        set<Lead> leads = new set<Lead>();
        set<Lead> leadsConvertedFields = new set<Lead>();        
        set<Lead> leadsTECHFields= new set<Lead>();          
        Map<Id, Lead> leadIds = new Map<Id, Lead>(); 
        Set<Decimal> leadPriority = new set<Decimal>(); 
        set<Lead> leadsConvertValidation = new set<lead>();
        
        set<String> NonMarcomCampaignIds = new set<String>();//Dec UAT
        List<Lead> lstLeadToUpdateCampaign = new List<Lead>();

        //set<Id> setRunAssignmentRuleLeadIDs = new set<id>(); // Oct13 Mkt Srinivas Nallapti - For RM Cordis Integarion
        Map<Id, List<Lead>> accountIds = new Map<Id, List<Lead>>();
        Map<Id, List<Lead>> ContactIds = new Map<Id, List<Lead>>();
        
        for(Lead lead: Trigger.new)
        {
            Lead beforeUpdate = System.Trigger.oldMap.get(lead.Id);
            //Oct13 Mkt
            if (lead.Company == null)
            {
                if(lead.Account__c != null && lead.Contact__c == null && trigger.oldMap.get(lead.id).Account__c != lead.Account__c)
                {
                    if (accountIds.ContainsKey( lead.Account__c ))
                        accountIds.get( lead.Account__c ).add (lead);
                    else
                        accountIds.put (lead.Account__c, new List<Lead>{ lead });
                }
    
                if(lead.Account__c == null && lead.Contact__c != null && trigger.oldMap.get(lead.id).Contact__c != lead.Contact__c)
                {
                    if (ContactIds.ContainsKey( lead.Contact__c ) )
                        ContactIds.get( lead.Contact__c ).add (lead);
                    else
                        ContactIds.put (lead.Contact__c, new List<Lead> { lead });
                }
            }
            //if(lead.ownerId == System.Label.CLOCT13MKT06  && lead.ProcessedBySDH__c && lead.Tech_AssignmentRuleHasRun__c == false)
            //    setRunAssignmentRuleLeadIDs.add(lead.id);

            //End of Oct 13    
            if(lead.Priority__c!=null)
            {
                leads.add(lead); 
                leadPriority.add(lead.Priority__c);
            } 
            if(lead.convertedDate!=null)
                leadsConvertedFields.add(lead);     
            if((lead.Opportunity__c!=null && (lead.Opportunity__c!= beforeUpdate.Opportunity__c))||(lead.Country__c!=null && (lead.Country__c!= beforeUpdate.Country__c)))
                leadsTECHFields.add(lead); 
            if((lead.Status==Label.CL00447) && (lead.SubStatus__c==Label.CL00448
               && lead.SubStatus__c!= beforeUpdate.SubStatus__c))
                leadIds.put(lead.Id, lead);
            if((lead.PreferredLanguage__c ==null || lead.OpportunityType__c==null  || lead.ClassificationLevel1__c ==null || lead.SuspectedValue__c ==null || lead.LeadingBusiness__c==null || lead.ZipCode__c==null || lead.OpportunityFinder__c==null || lead.City__c ==null || lead.PartnerProgram__c == null || lead.programLevel__c== null) && lead.convertedDate!=null)
                leadsConvertValidation.add(lead);
                
            //Change for batch    
            if(lead.Contact__c != beforeUpdate.Contact__c)
                lead.TECH_PreviousContactId__c = beforeUpdate .Contact__c;
            
            if(lead.LeadContact__c != beforeUpdate.LeadContact__c)
                lead.TECH_PreviousLeadContactId__c = beforeUpdate.LeadContact__c;    
            //end of change for batch
           //Dec UAT   
            if( lead.Tech_NonMarcomCampaignId__c != trigger.oldMap.get(lead.id).Tech_NonMarcomCampaignId__c && lead.RecordTypeId == System.Label.CLDEC12MKT12)
                if(lead.Tech_NonMarcomCampaignId__c != null)
                {
                    NonMarcomCampaignIds.add(lead.Tech_NonMarcomCampaignId__c);
                    lstLeadToUpdateCampaign.add(lead);
                } /* else 
                     lead.NonMarcommCampaign__c = null;    */
            //Apr 14 PRM Srinivas Nallapati
            if(Lead.ownerId != beforeUpdate.ownerId)
                //lead.Tech_LeadTransferredBy__c = beforeUpdate.ownerId;              
                lead.Tech_LeadTransferredBy__c = UserInfo.getUserId(); 
            if(lead.OwnedByPRMContact__c && lead.PartnerLeadStatus__c == System.Label.CLAPR14PRM53 && lead.PartnerLostReason__c != beforeUpdate.PartnerLostReason__c)
                lead.SubStatus__c = lead.PartnerLostReason__c;     
                 
        }
        
        
        if(leads.size()>0)
            AP19_Lead.populateCallBack(leads,leadPriority); 
            
         if(leadsConvertValidation.size()>0)
            AP19_Lead.validateLeadConversionFields(leadsConvertValidation);  
        
        if(leadsConvertedFields.size()>0)
        {
            AP19_Lead.updateConvertedFields(leadsConvertedFields);            
        }
        if(leadsTECHFields.size()>0)
            AP19_Lead.populateTECHfields(leadsTECHFields);  
        if(leadIds.size()>0)
            AP19_Lead.validateLeadStatus(leadIds,'Update');
        
        System.Debug('****** LeadBeforeUpdateTrigger - Updating Lead fields Ends ****');
        
        //Srinivas Nallapati  June 12 Release
        AP19_Lead.AssociateRelatedLeadsToContact(trigger.new);
        
        //BR-1897 Srinivas Nallapati  Sep 12 Release
        AP19_Lead.changeLeadOwner(trigger.newMap, trigger.oldMap); 
        //End of change for BR-1897
        
        //BR-1774 Srinivas Nallapati  Sep 12 Release
        AP19_Lead.UnlinkLeadLookups(trigger.newMap, trigger.oldMap);
        //End of change for BR-1774 
        
        AP19_Lead.UpdateWithContactIdOfConsumerAccount(accountIds);
        AP19_Lead.UpdateWithAccountIdOfConsumerAccount(ContactIds);
        
        if(NonMarcomCampaignIds.size() > 0)// Dec12 Uat
        {
           AP19_Lead.validateNonMarcomCampaignId(NonMarcomCampaignIds, trigger.new);
        } 

        //Oct 13
        //if(!setRunAssignmentRuleLeadIDs.isEmpty())
        //    AP19_Lead.updateWithAssignmentRules(setRunAssignmentRuleLeadIDs);
        
    }//End of if         
}//End of trigger