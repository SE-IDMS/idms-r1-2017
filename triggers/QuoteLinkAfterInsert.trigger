trigger QuoteLinkAfterInsert on OPP_QuoteLink__c (After insert) {
System.debug('****** QuoteLinkAfterInsert start *****');
if(!VFC20_CreateSeries.doNotProcessQLKS){
System.debug('****** Not bypassing QuoteLinkAfterInsert *****');
  if(Utils_SDF_Methodology.canTrigger('AP16')){
  System.Debug('****** QuoteLinkAfterInser Trigger Start****');
    Map<id,OPP_QuoteLink__c> quoteLink= new Map<id,OPP_QuoteLink__c>();
  for(OPP_QuoteLink__c quote:Trigger.new)
  {
       If(quote.Approver1__c!=null||quote.Approver2__c!=null||quote.Approver3__c!=null
       ||quote.Approver4__c!=null||quote.Approver5__c!=null||quote.Approver6__c!=null
       ||quote.Approver7__c!=null||quote.Approver8__c!=null||quote.Approver9__c!=null
       ||quote.Approver10__c!=null)
       {
       quotelink.put(Quote.id,Quote); 
       }
  }
  if(quotelink.size()>0)
  {
      AP16_GrantAcessToQteLink.grtAprEdtAcc(quotelink);
  }
  /*
  //Updated to grant access to Opportunity Leader
      AP49_UpdateQLKSharing.findOppLeaders(trigger.new,null);*/
  }
  /*New code block added in september release by abhihshek on 18/7/2011
*/
  if(Utils_SDF_Methodology.canTrigger('AP21'))
  {
  System.Debug('****** QuoteLinkAfterInsert Trigger Start - To populate the quote issuance date****');
  list<OPP_QuoteLink__c> quoteLinks= new list<OPP_QuoteLink__c>();
      for(OPP_QuoteLink__c quote:Trigger.new)
      {
          if(quote.ActiveQuote__c)
          {
              if(quote.IssuedQuoteDateToCustomer__c!=null)
              {
                  quoteLinks.add(quote);
              }
          }
      }
      if(quotelinks.size()>0)
      {
          AP21_UpdateOpportunity.updtQteIsuDateToCust(quotelinks,false);
      }
   }
   System.Debug('****** QuoteLinkAfterInser Trigger - To populate the quote issuance date End****');
    }
    System.debug('****** QuoteLinkAfterInsert end *****');
}