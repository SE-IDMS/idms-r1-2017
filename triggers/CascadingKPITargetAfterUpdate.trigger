/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 02-Oct-2012
    Description     : Connect Cascading KPI After Update event trigger.
*/

trigger CascadingKPITargetAfterUpdate on Cascading_KPI_Target__c (after update) {

System.Debug('****** CascadingKPITargetAfterUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CascadingKPITargetAfterUpdate'))
    {
   if(Trigger.old[0].Entity_Data_Reporter__c != Trigger.new[0].Entity_Data_Reporter__c || Trigger.old[0].CountryPickList__c != Trigger.new[0].CountryPickList__c)
     ConnectCascadingKPITargetTriggers.ConnectCascadingKPITargetAfterUpdate(Trigger.new,Trigger.old[0]);   
        
    }
 System.Debug('****** CascadingKPITargetAfterUpdate Trigger End ****'); 
 }