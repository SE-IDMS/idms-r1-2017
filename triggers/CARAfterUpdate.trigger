trigger CARAfterUpdate on ContactAssignedRequirement__c (after update) {
    if(Utils_SDF_Methodology.canTrigger('AP_CAR_UpdateRequirementFeature') || test.isRunningTest())
        AP_CAR_UpdateRequirementFeature.updateContactFeature(trigger.newMap , trigger.oldMap);
}