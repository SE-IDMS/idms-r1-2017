trigger ProjectOfferLifeCycleEmailTemp on Milestone1_Project__c (after insert,after update) {

    List<Milestone1_Project__c>  listMsproject= new List<Milestone1_Project__c>(); 
     List<Milestone1_Project__c>  listMpjct= new List<Milestone1_Project__c>();
    /* Forecast April Release BR-7315*/
    
    Map<id, Milestone1_Project__c> changedSellDateOldMap=new Map<id, Milestone1_Project__c>();
    Map<id, Milestone1_Project__c> changedSellDateNewMap=new Map<id, Milestone1_Project__c>();
    
    /* ---End--- */
    
    if(trigger.IsAfter && trigger.IsInsert){
        for(Milestone1_Project__c msprObj:trigger.new) {
        if(msprObj.country__c!=null)
            listMsproject.add(msprObj);
        }
       
    
    }
    
    if(trigger.IsAfter && trigger.IsUpdate){
        for(Milestone1_Project__c msprObj:trigger.new) {
            /* Forecast April Release BR-7315*/
            
            Milestone1_Project__c oldRec = Trigger.oldMap.get(msprObj.id);
            if (oldRec.Country_Announcement_Date__c!=msprObj.Country_Announcement_Date__c 
                || oldRec.Country_End_of_Commercialization_Date__c!=msprObj.Country_End_of_Commercialization_Date__c) {
                changedSellDateNewMap.put(msprObj.id, msprObj);
                changedSellDateOldMap.put(msprObj.id, oldRec);
            }
            
            /* ---End--- */   
            
            if(msprObj.country__c!=null && msprObj.country__c !=trigger.oldmap.get(msprObj.id).country__c)
                listMsproject.add(msprObj);
        }
       
    
    }
    if(listMsproject.size() > 0) {
        AP_COIN_bFO_OfferProject.UpdateEmailTempleCountry(listMsproject);
    } 
    
    /* Forecast April Release BR-7315*/
    if (changedSellDateNewMap.keySet().size()>0){
        AP_EllaProjectForecast.makeZero(changedSellDateOldMap, changedSellDateNewMap);
    }
    /* ---End--- */ 
// May 2015 Release --  Divya M 
    
    set<id> pjctids = new set<id>();
    if((trigger.IsAfter && trigger.IsUpdate) || (trigger.IsAfter && trigger.IsInsert)){
        for(Milestone1_Project__c mpObj:trigger.new) {
            pjctids.add(mpObj.id);
        }
    }
    if(pjctids.size()>0){
        for(Milestone1_Project__c mpObj:[Select id,name,Tech_BULaunchManager__c,Offer_Launch__r.Launch_Owner__c from Milestone1_Project__c where id in: pjctids]){
            if(mpObj.Tech_BULaunchManager__c != mpObj.Offer_Launch__r.Launch_Owner__c){
                mpObj.Tech_BULaunchManager__c = mpObj.Offer_Launch__r.Launch_Owner__c;
                listMpjct.add(mpObj);
            }
        }
    }
        if(listMpjct.size()>0){
            update listMpjct;
        }
    
    // Ends 
}