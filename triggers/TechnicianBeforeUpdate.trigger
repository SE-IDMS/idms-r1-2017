trigger TechnicianBeforeUpdate on SVMXC__Service_Group_Members__c (before update)
{
    //System.debug('#### Start of trigger TechnicainBeforeInsert');
    if(Utils_SDF_Methodology.canTrigger('SRV07')) 
    {
        //System.debug('#### can trigger');
        
        List<SVMXC__Service_Group_Members__c>  techlist  = new List<SVMXC__Service_Group_Members__c> ();
        //System.debug('#### techlist: '+techlist);
        
        For(SVMXC__Service_Group_Members__c tech:trigger.new)
        {
            //System.debug('#### Current tech: '+tech);
            if(tech.SVMXC__Salesforce_User__c !=null)
            {
                
                techlist.add(tech);
                //System.debug('#### Tech added. techlist: '+techlist);
            }
            
            
        }
        //System.debug('#### Techlist: '+techlist);
        if(techlist.size() > 0)  {
            //System.debug('#### Calling AP_Technician_UpdatesalesforceUser');
            AP_Technician_UpdatesalesforceUser.updatewithSalesforceUserManager(techlist);
        }
    }
}