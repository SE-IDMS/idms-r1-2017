trigger TEXBeforeUpdate on TEX__c (Before Update) {
List<TEX__c> Texlist=new list<TEX__c>();
List<TEX__c> TexlistOwner=new list<TEX__c>();
List<TEX__c> TexList1 = new List<TEX__c>(); 
map<string,TEX__c> mapTEX = new map<string,TEX__c>(); 
  if(Utils_SDF_Methodology.canTrigger('AP_TechnicalExpertAssessment') || Test.isRunningTest() )
        {
          //OCT 2015 RELEASE : BR-7761, DIVYA M 
     
        for(TEX__c TEX:Trigger.new)
            {
                Tex.LastUpdated__c = System.now();//June 2016 Release - BR-7799
                Tex.LastUpdatedby__c = UserInfo.getUserId();//June 2016 Release - BR-7799
                
                    if((Trigger.oldMap.get(TEX.id).Material_Status__c!= Trigger.newMap.get(TEX.id).Material_Status__c) && Trigger.newMap.get(TEX.id).Material_Status__c==Label.CLAPR14I2P12)// Start BR-9917 the if added as part of Q3 2016 release.
                        {if(Tex.ReceivedbyExpertCenter__c==null){
                            Tex.ReceivedbyExpertCenter__c=System.today();}
                            mapTEX.put(Tex.id,Tex);
                        }
                        else if(Trigger.newMap.get(TEX.id).Material_Status__c!=Label.CLAPR14I2P12){//CLAPR14I2P12 - Delivered to Expert Center
                          //   Tex.ReceivedbyExpertCenter__c=null;
                        }//End BR-9917 the if added as part of Q3 2016 release.
                 
                
                system.debug('~~~~~~~~TEX'+TEX);
                TEX__c oldcorr=Trigger.oldmap.get(TEX.id);
                TEX__c newcorr=Trigger.newmap.get(TEX.id);
                system.debug('~~~~~~~~TEX labels18 19'+oldcorr.Status__c+ Label.CLOCT13RR18 + '=='+newcorr.Status__c+Label.CLOCT13RR19);
                if(oldcorr.Status__c == Label.CLOCT13RR18 && newcorr.Status__c==Label.CLOCT13RR19)
               // if(oldcorr.Status__c == 'Expert Assessment to be started' && newcorr.Status__c=='Expert Assessment in progress' )
                { 
                  Texlist.add(TEX);
                }
            if((Trigger.oldMap.get(TEX.id).Status__c!= Trigger.newMap.get(TEX.id).Status__c) && Trigger.newMap.get(TEX.id).Status__c==Label.CLOCT13RR11)
            {
            TexlistOwner.add(TEX);
            System.debug('##TEX##'+ TEX);
                
            }
            
            // OCT 2015 Release - added by Uttara PR
                if(Trigger.oldMap.get(Tex.ID).RelatedProblem__c != Trigger.newMap.get(Tex.ID).RelatedProblem__c) {
                    System.debug ('@@ Entered if 1 in Trigger');
                    TexList1.add(Tex);
                }
            }
            // Start BR-9917 the if added as part of Q3 2016 release.
            if(mapTEX.size()>0){
                AP_TechnicalExpertAssessment.retrunRequestCheck(mapTEX);
            }
            // End BR-9917 the if added as part of Q3 2016 release.
            // OCT 2015 Release - added by Uttara PR
            if(TexList1.size() > 0) {
                System.debug ('@@ Entered if 2 in Trigger');
                AP_TechnicalExpertAssessment.CaseProblemLink(Texlist1);    
            }
            system.debug('~~~~~~~~Texlist'+Texlist);
            if(Texlist.size()>0)
            {
            AP_TechnicalExpertAssessment.updateOfferExpertonTex(Texlist);
           
            }
if(TexlistOwner.size()>0){
 AP_TechnicalExpertAssessment.updateCaseOwner(TexlistOwner );
 AP_TechnicalExpertAssessment.updateLineOfBusinessOnTEX(TexlistOwner);
}
AP_TechnicalExpertAssessment.updateCSIFlagOnTex(trigger.new);
//AP_TechnicalExpertAssessment.updateContactEmail(trigger.new);
}
}