/* 
*   Created By: Otmane
*   Created Date: 01/06/2016
*   Modified by:  01/12/2016
*This Trigger will be Fired before Company record is created and update Vnew field to 1
*/

trigger UIMSCompanyBeforeInsert on UIMS_Company__c (before insert) {
    
    if(Utils_SDF_Methodology.canTrigger('UimsCompany')){
        for(UIMS_Company__c uimsCompany: trigger.new) {
            uimsCompany.Vnew__c = '1';
        }
        
    }
}