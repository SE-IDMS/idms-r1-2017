/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: Trigger Class                                       *
* Trigger: Fielo_OnInvoice                                  *
* Created Date: 19/08/2014                                  *
************************************************************/
trigger Fielo_OnInvoice on Fielo_Invoice__c (after insert, after update) {

    if(Utils_SDF_Methodology.canTrigger('Fielo_InvoiceTriggers')){
        if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
            Fielo_InvoiceTriggers.transactionCreater();
        }
    }
    
}