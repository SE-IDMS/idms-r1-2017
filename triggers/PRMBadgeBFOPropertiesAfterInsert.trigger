trigger PRMBadgeBFOPropertiesAfterInsert on PRMBadgeBFOProperties__c (after insert) {
	if(Utils_SDF_Methodology.canTrigger('PRM_FieldChangeListener')){
		System.debug('BeginsPRMBadgeBFOPropertiesAfterInsert');
		
		PRM_FieldChangeListener prmChangeListener = new PRM_FieldChangeListener();
		prmChangeListener.PRMBadgeBFOPropertiesInspecter(trigger.newMap);

		prmChangeListener.executeAction();
					}
}