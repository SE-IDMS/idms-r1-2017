/*
    Author          : Ramesh Rajasekaran - Global Delivery team 
    Date Created    : 11/11/2011
    Description     : Coaching Visit Form - after insert,after update event trigger.
*/
trigger CoachingVisitFormAfterInsertUpdate on SFE_CoachingVisitForm__c (After Insert, After Update) {
	
	System.Debug('****** CoachingVisitFormAfterInsertUpdate Trigger Start ****');
	
	if(Utils_SDF_Methodology.canTrigger('AP31')){
		List<SFE_CoachingVisitForm__c> coachingVisitForm = new List<SFE_CoachingVisitForm__c>();
        for(SFE_CoachingVisitForm__c cvf:trigger.new)
        {
            coachingVisitForm.add(cvf);
        }
        if(coachingVisitForm.size()>0)    
        {
            AP31_CoachingVisitFormSharing.shareCVF(coachingVisitForm);
        }		 
	}
	   
	System.Debug('****** CoachingVisitFormAfterInsertUpdate Trigger End ****');
  
}