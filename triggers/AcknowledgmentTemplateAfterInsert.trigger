//********************************************************************************************************
//  Trigger Name            : AcknowledgmentTemplateAfterInsert
//  Purpose                 : AcknowledgmentTemplate__c  After Insert event trigger , 
//                            Set the Other Defailt Templates to False
//  Created by              : Vimal Karunakaran (Global Delivery Team)
//  Date created            : 11th March 2015
///*******************************************************************************************************/

/*
    Modification History    :
    Modified By             : 
    Modified Date           : 
    Description             : 
*/
trigger AcknowledgmentTemplateAfterInsert on AcknowledgmentTemplate__c (after insert) {
    System.Debug('****** AcknowledgmentTemplateAfterInsert Trigger Start ****');
    if(Utils_SDF_Methodology.canTrigger('AcknowledgmentTemplateAfterInsert')){
        AP_ACK_Template_Handler.unCheckOtherDefaultTemplates(null, Trigger.newMap,true);
    }
    System.Debug('****** AcknowledgmentTemplateAfterInsert Trigger Ends ****');

}