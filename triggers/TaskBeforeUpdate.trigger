trigger TaskBeforeUpdate on Task (before Update) {

    boolean blnCanTriggerAP25 = false;
    blnCanTriggerAP25 = Utils_SDF_Methodology.canTrigger('AP25');
    
    if(blnCanTriggerAP25){
        System.Debug('****** TaskBeforeUpdate Trigger - Update Task Fields for CCC Begins ****'); 
        String strCaseKeyPrefix = SObjectType.Case.getKeyPrefix(); 
        Map<Id,Task> mapcTaskIds = new Map<Id,Task>();
        Set<Id> caseIds = new Set<Id>();
        
        for(Task task: Trigger.new){
            if((task.WhatId!=null) && (String.valueOf(task.WhatId)).startsWith(strCaseKeyPrefix) && task.Status==System.label.CL00327){
                mapcTaskIds.put(task.Id,task);
                caseIds.add(task.WhatId);
            }
        }
         
        if(!(mapcTaskIds.isEmpty()))   
        AP25_Task.updateTaskCompletionFields(mapcTaskIds,caseIds);
        System.Debug('****** TaskBeforeUpdate Trigger - Update Task Fields for CCC Ends ****'); 
    
    }

}