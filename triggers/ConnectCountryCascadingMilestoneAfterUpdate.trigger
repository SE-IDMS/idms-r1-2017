/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 07-Feb-2013
    Description     : Connect Country Cascading Milestones After Update event trigger.
*/

trigger ConnectCountryCascadingMilestoneAfterUpdate on Country_Cascading_Milestone__c (after update) {
System.Debug('****** ConnectCountryCascadingMilestoneAfterUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CountryCascadingMilestoneAfterUpdate'))
    {
        ConnectCountryCascadingMilestoneTriggers.CountryCascadingMilestoneAfterUpdate(Trigger.new,Trigger.old[0]);     
    
 }
 System.Debug('****** ConnectCountryCascadingMilestoneAfterUpdate Trigger End ****'); 

}