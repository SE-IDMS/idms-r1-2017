/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 29 May 2013
    Description  : For BR # 3063. PRM June 2013 Release
                        1. notifyPartner_OppTeam
                           (a) When the status of an ORF is updated to approved or ineligible send email to partner and 
                            include the Opportunity Team on the cc: line of the email.
    
    Updated By : Shruti Karn
    Description : For October 13 PRM Release:
                 1. To add Assessment to Opportunity Registration Form.
                    
***************************************************************************************************************************/
trigger OpportunityRegistrationFormAfterUpdate on OpportunityRegistrationForm__c (After Update) {
    
    if(Utils_SDF_Methodology.canTrigger('AP_ORFNotification'))
    {
        //map<ID,list<OpportunityRegistrationForm__c>> mapOppORF = new map<ID,list<OpportunityRegistrationForm__c>>();
        list<OpportunityRegistrationForm__c> lstORF = new list<OpportunityRegistrationForm__c>();
        set<Id> setOppId = new set<Id>();
        map<Id, OpportunityRegistrationForm__c> mapORF = new map<Id, OpportunityRegistrationForm__c>();
        map<String,set<Id>> mapORFProgram = new map<String,set<Id>>();
        map<Id,ORFwithOpportunities__c> mapORFOpp = new map<Id,ORFwithOpportunities__c>();
        list<OpportunityRegistrationForm__c> lstNotifySE = new list<OpportunityRegistrationForm__c>();
        
        for(OpportunityRegistrationForm__c newORF : trigger.new)
        {
            OpportunityRegistrationForm__c oldORF = trigger.oldmap.get(newORF.id);
            System.debug('*** ORF Status ->'+ newORF.status__c + oldORF.Status__c);
            
            if(newORF.Status__C != oldORF.Status__c && (newORF.status__c == Label.CLJUN13PRM01 || newORF.status__c == Label.CLFEB15PRM01)) //Modified for BR#6967 for FEB15 Release
            {
                
                lstORF.add(newORF);
                System.debug('*** ORF Status Inside ->'+ newORF.status__c);
                setOppId.add(newORF.Tech_LinkedOpportunity__c);
            }
            
            //Updated for October 2013 Release
            //To add Assessments to ORF
            if((newORF.PartnerProgram__c != null && newORF.PartnerProgram__c != oldORF.PartnerProgram__c) || 
                    (newORF.ProgramLevel__c != null && newORF.ProgramLevel__c != oldORF.ProgramLevel__c) )
            {
                if(!mapORFProgram.containsKey(newORF.PartnerProgram__c+':'+newORF.ProgramLevel__c))
                    mapORFProgram.put(newORF.PartnerProgram__c+':'+newORF.ProgramLevel__c , new set<id> {(newORf.Id)});
                else
                    mapORFProgram.get(newORF.PartnerProgram__c+':'+newORF.ProgramLevel__c).add(newORf.Id);
            }
           
            if(newORF.Status__C != oldORF.Status__c && newORF.status__c == Label.CLJUN13PRM02)
            {
                mapORF.put(newORF.Id , newORF);
                //Insert record in ORFwithOpportunities__c table if a converted ORF status is updated to Ineligible. For EUS#032815
                if(newORF.Tech_LinkedOpportunity__c != null)
                {
                    ORFwithOpportunities__c ORFOpp = new ORFwithOpportunities__c();
                    ORFOpp.Opportunity__c = newORF.Tech_LinkedOpportunity__c;
                    ORFOpp.Opportunity_Registration_Form__c = newORF.Id;
                    if(!mapORFOpp.containsKey(newORF.Id))
                        mapORFOpp.put(newORF.Id,ORFOpp);
                }
            }
            
            //START: FEB15 Release
            //List of ORF with status = 'Additional Information Provided'
            System.debug('**** ORF Status ->'+newORF.status__c);
             if(newORF.Status__C != oldORF.Status__c && newORF.status__c == Label.CLFEB15PRM12){
                lstNotifySE.add(newORF);
            }
            //END: FEB15 Release
            
        }
        system.debug('***** Notify SE ->'+lstNotifySE);
        System.debug('**** lstORF ->'+lstORF.size());
        
            //Insert record in ORFwithOpportunities__c table if a converted ORF status is updated to Ineligible. For EUS#032815
            if(!mapORFOpp.isEmpty())
                AP_ORFNotification.addORFWithOpportunity(mapORFOpp);
            if(!lstORF.isEmpty())
                AP_ORFNotification.notifyPartner_OppTeamonApproval(lstORF,setOppId);
            if(!mapORF.isEmpty())
                AP_ORFNotification.notifyPartner_OppTeamonRejection(mapORF);
            if(!mapORFProgram.isEmpty())
                AP_ORFNotification.addAssessment(mapORFProgram);
            //START: FEB15 Release
            //Notify SE person after information Provided
            //if(!lstNotifySE.isEmpty())
                //AP_ORFNotification.notifySEPerson_AddInfoProvided(lstNotifySE);
            
            
    }
}