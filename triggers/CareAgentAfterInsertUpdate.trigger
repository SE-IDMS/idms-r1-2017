/*
    Author          : Stephen Norbury 
    Date Created    : 06/03/2013
    Description     : Care Agent After Insert / Update.
    
    Change History:
    
    BR-2858: Add Care Agent as Team Member
*/

trigger CareAgentAfterInsertUpdate on CareAgent__c (after insert, after update) 
{
  // Fetch the Agents
  
  Set <Id> AgentIds = new Set <Id>();
  
  List <TeamAgentMapping__c> newMembers = new List <TeamAgentMapping__c>();
  List <TeamAgentMapping__c> updMembers = new List <TeamAgentMapping__c>();
  
  for (CareAgent__c agent : trigger.new)
  {
    if (trigger.IsInsert || trigger.IsUpdate &&
        ((trigger.oldMap.get(agent.Id).DefaultCareTeam__c != null && trigger.newMap.get(agent.Id).DefaultCareTeam__c != null && trigger.oldMap.get(agent.Id).DefaultCareTeam__c != trigger.newMap.get(agent.Id).DefaultCareTeam__c) ||
         (trigger.oldMap.get(agent.Id).DefaultCareTeam__c != null && trigger.newMap.get(agent.Id).DefaultCareTeam__c == null) ||
         (trigger.oldMap.get(agent.Id).DefaultCareTeam__c == null && trigger.newMap.get(agent.Id).DefaultCareTeam__c != null)))
    {
      if (agent.User__c != null) { AgentIds.add (agent.User__c); }
    }
  }
  
  // Now, Fetch the Team Members
  
  List <TeamAgentMapping__c> members = [select Id, CCAgent__c, CCTeam__c, DefaultTeam__c from TeamAgentMapping__c where CCAgent__c in :AgentIds];
  
  Map <String, Id> TeamMemberMap = new Map <String, Id>();
  
  for (TeamAgentMapping__c member : members)
  {
    TeamMemberMap.put (member.CCTeam__c + '-' + member.CCAgent__c, member.Id);   
  }
  
  // And, is the Agent already a Member?
  
  for (CareAgent__c agent : trigger.new)
  {
    if (trigger.IsInsert || trigger.IsUpdate &&
        (trigger.oldMap.get(agent.Id).DefaultCareTeam__c != null && trigger.newMap.get(agent.Id).DefaultCareTeam__c != null && trigger.oldMap.get(agent.Id).DefaultCareTeam__c != trigger.newMap.get(agent.Id).DefaultCareTeam__c) ||
        (trigger.oldMap.get(agent.Id).DefaultCareTeam__c != null && trigger.newMap.get(agent.Id).DefaultCareTeam__c == null) ||
        (trigger.oldMap.get(agent.Id).DefaultCareTeam__c == null && trigger.newMap.get(agent.Id).DefaultCareTeam__c != null))
    {
      if (trigger.IsUpdate && trigger.oldMap.get(agent.Id).DefaultCareTeam__c != null)
      {
        // No longer the Default Team
        
        if (TeamMemberMap.get (trigger.oldMap.get(agent.Id).DefaultCareTeam__c + '-' + trigger.oldMap.get(agent.Id).User__c) != null)
        {
          TeamAgentMapping__c member = new TeamAgentMapping__c();
        
          member.Id             = TeamMemberMap.get (trigger.oldMap.get(agent.Id).DefaultCareTeam__c + '-' + trigger.oldMap.get(agent.Id).User__c);
          member.DefaultTeam__c = false;
        
          updMembers.add (member);
        }
      }
      
      if (trigger.newMap.get(agent.Id).DefaultCareTeam__c != null)
      {
        // The Default Team
        
        if (TeamMemberMap.get (trigger.newMap.get(agent.Id).DefaultCareTeam__c + '-' + trigger.newMap.get(agent.Id).User__c) != null)
        {
          TeamAgentMapping__c member = new TeamAgentMapping__c();
        
          member.Id             = TeamMemberMap.get (trigger.newMap.get(agent.Id).DefaultCareTeam__c + '-' + trigger.newMap.get(agent.Id).User__c);
          member.DefaultTeam__c = true;
        
          updMembers.add (member);
        }
        else
        {
          TeamAgentMapping__c member = new TeamAgentMapping__c();
        
          member.CCTeam__c      = trigger.newMap.get(agent.Id).DefaultCareTeam__c;
          member.CCAgent__c     = trigger.newMap.get(agent.Id).User__c;
          member.DefaultTeam__c = true;
        
          newMembers.add (member);  
        }
      }
    }
  }
  
  // Finally, create / update the Team Members
  
  if (newMembers != null && newMembers.size() != 0)
  {
    insert newMembers;
  }
  
  if (updMembers != null && updMembers.size() != 0)
  {
    update updMembers;
  }
  
  // End
}