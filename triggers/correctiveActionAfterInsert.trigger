//*********************************************************************************
// Trigger Name     : correctiveActionAfterInsert
// Purpose          : Corrective Action After Insert event trigger
// Created by       : Global Delivery Team
// Date created     : 29th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/

trigger correctiveActionAfterInsert on CorrectiveAction__c (after insert) {
    System.Debug('****** correctiveActionAfterInsert Trigger Start ****');      
    
    if(Utils_SDF_Methodology.canTrigger('AP1003')){              
        List<CorrectiveAction__c> correctiveActionList = new List<CorrectiveAction__c>(); 
        
        for(CorrectiveAction__c crtveActn: Trigger.new){
            if((crtveActn.RelatedProblem__c != null) && (crtveActn.Owner__c != null))
                correctiveActionList.add(crtveActn);
        }
        //Add All Containment Action Owner  to the Problem Share for Sharing the Problem Record
        AP1003_CorrectiveAction.addCorrectiveOwnerToProblemShare(correctiveActionList);
    }                                               
    System.Debug('****** correctiveActionAfterInsert Trigger End ****'); 
}