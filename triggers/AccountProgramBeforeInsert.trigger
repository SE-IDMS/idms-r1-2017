/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 06 March 2013
    Description : For May13 Release:
                 
                 1. To check if the Account is Partner Account or not.
   
********************************************************************************************************************/
trigger AccountProgramBeforeInsert on ACC_PartnerProgram__c (before insert) {
   if(Utils_SDF_Methodology.canTrigger('AccountProgramBeforeInsert'))
   {
       map<Id,ACC_PartnerProgram__c> mapAccountIdProgram = new map<Id,ACC_PartnerProgram__c>();
       list<Account> lstAccount = new list<Account>();
       for(ACC_PartnerProgram__c accPRG : trigger.new)
       {
           mapAccountIdProgram.put(accPRG.Account__c,accPRG);
        
       }
       
       lstAccount = [Select id,isPartner,PRMAccount__c from Account where id in : mapAccountIdProgram.keySet() limit 50000];
       for(Account acc : lstAccount )
       {
           if(acc.IsPartner == false)
              mapAccountIdProgram.get(acc.Id).addError('Selected Account is not a Partner Account'); 
       }  
   }
}