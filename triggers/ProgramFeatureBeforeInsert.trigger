trigger ProgramFeatureBeforeInsert on ProgramFeature__c (before insert) {

//Added for checking of duplicate feature assignment
//Release : FEB14
//Created By: Renjith Jose
//Description: BR-4496 , The Program Level cannot be assigned with both Simple ORF & ORF features

    if(Utils_SDF_Methodology.canTrigger('AP_PRF_BeforeHandler'))
     {
        AP_PRF_BeforeHandler.checkDuplicateFeature(trigger.new);
    
     }

}