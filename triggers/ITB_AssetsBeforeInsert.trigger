//********************************************************************************************************
//  Trigger Name            : ITB_AssetsBeforeInsert
//  Purpose                 : ITB_Asset__c  Before Inset event trigger , 
//                            When Assets are attached to Case, and if no primary assets are associated with Case,
//                            set the first Asset as Primary
//                            then the User should be notified that there is already an existing Primary Asset
//  Created by              : Vimal Karunakaran (Global Delivery Team)
//  Date created            : 08th March 2013
///*******************************************************************************************************/

/*
    Modification History    :
    Modified By             : 
    Modified Date           : 
    Description             : 
*/

trigger ITB_AssetsBeforeInsert on ITB_Asset__c (before insert) {
    System.Debug('****** ITB_AssetsBeforeInsert Trigger Start ****');
    if(Utils_SDF_Methodology.canTrigger('AP_ITB_Assets_PrimaryAssetCheck')){
        AP_ITB_Assets_PrimaryAssetCheck.setPrimaryAsset(Trigger.new);
    }
    System.Debug('****** ITB_AssetsBeforeInsert Trigger Ends ****');
}