//*********************************************************************************
// Trigger Name     : ProblemBeforeInsert
// Purpose          : Problem  Before Insert event trigger
// Created by       : Global Delivery Team
// Date created     : 18th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/


trigger ProblemBeforeInsert on Problem__c (before Insert) {
    System.Debug('****** PrpoblemBeforeInsert Trigger Start ****');

    if(Utils_SDF_Methodology.canTrigger('AP1000_1')){
        if(Trigger.new.size()>0){
            AP1000_Problem.populateProblemOwner(Trigger.new);
            List<Problem__c> prblmStsList = new List<Problem__c>(); 
            //======= Ram Chilukuri  - May2015RELEASE - START =======================================================
            system.debug('actual user going to get udate ');
             for(Problem__c prob : Trigger.new)
             {
                prob.ActualUserLastmodifiedDate__c = System.now();
                prob.ActualUserLastmodifiedby__c = UserInfo.getUserId();
                 if(prob.StatusOfContainment__c == 'Completed' || prob.StatusOfCorrection__c == 'Completed' || prob.StatusOfPrevention__c == 'Completed' || prob.StatusOfRootCauseAnalysis__c == 'Completed'){
                prblmStsList.add(prob);
            }
            } 
 //======= Ram Chilukuri  - May2015RELEASE - END =======================================================  
      //Mar 16 Release -- Ram C -- BR-9275
        if(prblmStsList.size()>0){
            AP1000_Problem.checkStatusofRelObj(prblmStsList);
        }
        }
    }      
    System.Debug('****** PrpoblemBeforeInsert Trigger End ****');  
}