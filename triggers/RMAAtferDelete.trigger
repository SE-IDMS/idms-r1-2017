/*
Oct 15 rel
Divya M
BR-7721
*/
trigger RMAAtferDelete on RMA__c (after Delete) {
    if(Utils_SDF_Methodology.canTrigger('RMAAtferDelete')){
        AP_RR_FieldUpdate.updateTEXShippedFromCustomer(trigger.old);
    }
}