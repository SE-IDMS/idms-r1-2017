//*********************************************************************************
// Trigger Name     : ProblemAfterInsert
// Purpose          : Problem  After Insert event trigger
// Created by       : Global Delivery Team
// Date created     : 18th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/
trigger ProblemAfterInsert on Problem__c (after Insert) {

   //Advance I2P Mohit Start
      Map<Id,Problem__c> mapPRB=new Map<Id,Problem__c>();
   //Advance I2P Mohit End
    System.Debug('****** ProblemAfterInsert Trigger Start ****');

    if(Utils_SDF_Methodology.canTrigger('AP1000_2')){
    //if(Utils_SDF_Methodology.canTrigger('AP1000')){ 
        if(Trigger.new.size()>0){
            AP1000_Problem.addUserToTeamMember(Trigger.new,TRUE);
    //Advance I2P Mohit Start
      // AP1000_Problem.scenarioCheckOnInsert(Trigger.newMap);
      for(Problem__c varPRB:Trigger.new)
      {
      
        If(varPRB.RecordTypeid !=Label.CLI2PAPR120014)
        {
           mapPRB.Put(varPRB.Id,varPRB);
        }
      }
      
      
            if(mapPRB.size()>0 && mapPRB !=null)
            {
                AP1000_Problem.scenarioCheckOnInsert(mapPRB);
           }
   //Advance I2P Mohit End     
        }
    }      
    System.Debug('****** ProblemAfterInsert Trigger End  ****');  
}