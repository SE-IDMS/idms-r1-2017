/*
  
  Author: Amitava Dutta
  Created On:05/01/2011
  Description: ObjectivesDetail Before Update Event trigger
   
*/


trigger ObjectivesDetailBeforeUpdate on ObjectivesDetail__c (before Update) 
{
    System.Debug('****** ObjectivesDetailTrackRecordUpdateAfterApproval Trigger Start ****');  
    
    if(Utils_SDF_Methodology.canTrigger('AP03'))
    {
   
    List<ObjectivesDetail__c> objectivedetails = new List<ObjectivesDetail__c>();
    for(integer i=0; i<Trigger.New.size();i++)
    {
        if(trigger.new[i].AprilAmount__c != trigger.old[i].AprilAmount__c ||
        trigger.new[i].AugustAmount__c != trigger.old[i].AugustAmount__c ||trigger.new[i].MarchAmount__c != trigger.old[i].MarchAmount__c ||
        trigger.new[i].JuneAmount__c != trigger.old[i].JuneAmount__c ||trigger.new[i].JanuaryAmount__c != trigger.old[i].JanuaryAmount__c ||
        trigger.new[i].JulyAmount__c != trigger.old[i].JulyAmount__c || trigger.new[i].MayAmount__c != trigger.old[i].MayAmount__c ||
        trigger.new[i].SeptemberAmount__c != trigger.old[i].SeptemberAmount__c || trigger.new[i].OctoberAmount__c != trigger.old[i].OctoberAmount__c 
        ||trigger.new[i].NovemberAmount__c != trigger.old[i].NovemberAmount__c ||trigger.new[i].DecemberAmount__c != trigger.old[i].DecemberAmount__c 
        ||trigger.new[i].FebruaryAmount__c != trigger.old[i].FebruaryAmount__c || trigger.new[i].Q1Amount__c != trigger.old[i].Q1Amount__c ||
        trigger.new[i].Q2Amount__c != trigger.old[i].Q2Amount__c ||trigger.new[i].Q3Amount__c != trigger.old[i].Q3Amount__c ||
        trigger.new[i].Q4Amount__c != trigger.old[i].Q4Amount__c ||trigger.new[i].YearAmount__c != trigger.old[i].YearAmount__c ||
        trigger.new[i].DirectSalePercentageYear__c != trigger.old[i].DirectSalePercentageYear__c ||
        trigger.new[i].DirectSalePercentageApr__c != trigger.old[i].DirectSalePercentageApr__c ||
        trigger.new[i].DirectSalePercentageAug__c != trigger.old[i].DirectSalePercentageAug__c ||
        trigger.new[i].DirectSalePercentageJun__c != trigger.old[i].DirectSalePercentageJun__c ||
        trigger.new[i].DirectSalePercentageMay__c != trigger.old[i].DirectSalePercentageMay__c ||
        trigger.new[i].DirectSalePercentageMar__c != trigger.old[i].DirectSalePercentageMar__c ||
        trigger.new[i].DirectSalePercentageJul__c != trigger.old[i].DirectSalePercentageJul__c ||
        trigger.new[i].DirectSalePercentageJan__c != trigger.old[i].DirectSalePercentageJan__c ||
        trigger.new[i].DirectSalePercentageSep__c != trigger.old[i].DirectSalePercentageSep__c ||
        trigger.new[i].DirectSalePercentageOct__c != trigger.old[i].DirectSalePercentageOct__c ||
        trigger.new[i].DirectSalePercentageNov__c != trigger.old[i].DirectSalePercentageNov__c ||
        trigger.new[i].DirectSalePercentageDec__c != trigger.old[i].DirectSalePercentageDec__c ||
        trigger.new[i].DirectSalePercentageFeb__c != trigger.old[i].DirectSalePercentageFeb__c ||
        trigger.new[i].DirectSaleAmountQ1__c != trigger.old[i].DirectSaleAmountQ1__c ||        
        trigger.new[i].DirectSaleAmountQ2__c != trigger.old[i].DirectSaleAmountQ2__c ||
        trigger.new[i].DirectSaleAmountQ3__c != trigger.old[i].DirectSaleAmountQ3__c ||
        trigger.new[i].DirectSaleAmountQ4__c != trigger.old[i].DirectSaleAmountQ4__c ||
        trigger.new[i].Product__c != trigger.old[i].Product__c 
        
                  )
        {
            objectivedetails.add(trigger.new[i]);
        }
    
    }  
    if(objectivedetails.size() !=0)
    {      
            AP03_ObjectiveSettingsTrigger.recordUpdationAfterApprovalForObjDetail(Trigger.New);      
    }
    }    
   
   System.Debug('****** ObjectivesDetailTrackRecordUpdateAfterApproval Trigger End****');
}