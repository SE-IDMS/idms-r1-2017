trigger UserBeforeInsertBeforeUpdate on User (before insert, before update)
{
    if(Utils_SDF_Methodology.canTrigger('UserBeforeTrigger'))
    {
        String sesa = '';
        for (User u : Trigger.new)
        {
            
            /* Only apply changes to Non Partner Users */
            if (u.UserType != null && !u.UserType.toLowerCase().contains('partner'))
            {
                
                /*  OCTOBER'12 Release
                    If the Federation Id is empty
                     - If the username contains a SESA, the Federation Id takes the SESA
                     - If the username does not contain a SESA, the Federation Id is not changed
                    If the Federation Id is not empty, it remains as it is
                 */
                
                
                // The Federation Id is empty
                if (u.FederationIdentifier == null || u.FederationIdentifier.length() == 0)
                {
                    
                    
                    // The Username contains a SESA
                    if (u.Username != null && u.Username.toUpperCase().startsWith('SESA'))
                    {
                        u.FederationIdentifier = u.Username.toUpperCase().substring (0, u.Username.indexOf('@'));
                    }
                }
                // If the Federation Id is not empty, it is put to uppercase
                else
                {
                    //A. D'Aquino 26/05/2016 removed to enable Just in time provisioning with UIMS
                    //u.FederationIdentifier = u.FederationIdentifier.toUpperCase();
                }
                
                 
                /* JUNE'12 Release
                   Replace by the OCTOBER'12 Release logic above
                
                // The Username contains a SESA, the Federation Id must be verfied and contain the user SESA
                if (u.Username != null && u.Username.toUpperCase().startsWith('SESA'))
                {
                    sesa = u.Username.toUpperCase().substring (0, u.Username.indexOf('@'));
                    if (u.FederationIdentifier == null || !u.FederationIdentifier.equals(sesa))
                    {
                        u.FederationIdentifier = sesa;
                    }
                }
                // The Username does not contain a SESA but the Federation Id contains one, it is updated to null
                else if (u.FederationIdentifier != null && u.FederationIdentifier.toUpperCase().startsWith('SESA'))
                {
                    u.FederationIdentifier = null;
                }
                */
            }
        }
    }
}