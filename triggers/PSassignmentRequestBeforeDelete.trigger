trigger PSassignmentRequestBeforeDelete on PermissionSetAssignmentRequests__c (before delete) {

    List<PermissionSetAssignmentRequests__c> psAssignmentRequest = new List<PermissionSetAssignmentRequests__c>();

    for(PermissionSetAssignmentRequests__c psRequest:Trigger.Old) {        
            psAssignmentRequest.add(psRequest);
    }    

    AP_PsAssignmentMethods.deleteAssignment(psAssignmentRequest);
}