//*********************************************************************************
// Trigger Name     : ProblemTeamMemberAfterDelete
// Purpose          : Problem  Team Member After Delete event trigger
// Created by       : Global Delivery Team
// Date created     : 26th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/

trigger ProblemTeamMemberAfterDelete on ProblemTeamMember__c (after delete) {
    System.Debug('****** ProblemTeamMemberAfterDelete Trigger Start ****');      
    
    if(Utils_SDF_Methodology.canTrigger('AP1001')){
        if(Trigger.oldMap.size()>0){ 
            AP1001_ProblemTeamMember.deleteProblemTeamMemberFromProblemShare(Trigger.oldMap); 
        }                  
    }                                               
    System.Debug('****** ProblemTeamMemberAfterDelete Trigger End ****'); 
}