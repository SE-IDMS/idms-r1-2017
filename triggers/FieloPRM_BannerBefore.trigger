/********************************************************************
* Company: Fielo
* Created Date: 30/11/2016
* Description:
********************************************************************/

trigger FieloPRM_BannerBefore on FieloEE__Banner__c(before insert, before update, before delete){
    
    if(FieloEE__PublicSettings__c.getOrgDefaults().CacheEnabled__c != null && FieloEE__PublicSettings__c.getOrgDefaults().CacheEnabled__c){
        if(Trigger.isInsert || Trigger.isUpdate){
            FieloPRM_AP_BannerTriggers.removeCache(Trigger.new);
        }else if(Trigger.isDelete){
            FieloPRM_AP_BannerTriggers.removeCache(Trigger.old);
        }
    }
}