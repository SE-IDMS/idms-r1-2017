//********************************************************************************************************
//  Trigger Name            : ITB_AssetsAfterUpdate
//  Purpose                 : ITB_Asset__c  After update event trigger , 
//                            Update the associated Case with the Serial Number for Primary Asset
//  Created by              : Vimal Karunakaran (Global Delivery Team)
//  Date created            : 11th March 2013
///*******************************************************************************************************/

/*
    Modification History    :
    Modified By             : 
    Modified Date           : 
    Description             : 
*/

trigger ITB_AssetsAfterUpdate on ITB_Asset__c (after update) {
    System.Debug('****** ITB_AssetsAfterUpdate Trigger Start ****');
    if(Utils_SDF_Methodology.canTrigger('AP_ITB_Assets_PrimaryAssetCheck')){
        AP_ITB_Assets_PrimaryAssetCheck.updateCaseSerialNumber(Trigger.new);
    }
    if(Utils_SDF_Methodology.canTrigger('AP_ITB_Assets_PrimaryAssetCheck_unCheckOtherPrimaryAssets')){
        AP_ITB_Assets_PrimaryAssetCheck.unCheckOtherPrimaryAssets(Trigger.oldMap,Trigger.newMap);
    }
    System.Debug('****** ITB_AssetsAfterUpdate Trigger Ends ****');
}