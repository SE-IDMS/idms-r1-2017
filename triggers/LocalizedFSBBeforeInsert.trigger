trigger LocalizedFSBBeforeInsert on LocalizedFSB__c (Before Insert) {
    
    if(Utils_SDF_Methodology.canTrigger('LFSB')) {
        
        Map<LocalizedFSB__c,String> mapLocOrgId = new Map<LocalizedFSB__c,String>();
        
        for(LocalizedFSB__c loc : Trigger.New) {
            mapLocOrgId.put(loc,loc.Organization__c);
        }
        
        System.debug('!!!! mapLocOrgId : ' + mapLocOrgId);
        if(!mapLocOrgId.isEmpty()) 
            AP_LocalizedFSB.populateAccUser(mapLocOrgId);
    }
}