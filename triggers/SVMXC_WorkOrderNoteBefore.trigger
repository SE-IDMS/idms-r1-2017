trigger SVMXC_WorkOrderNoteBefore on WorkOrderNote__c (before insert, before update) {

	if (trigger.isInsert || trigger.isUpdate) 
		SVMXC_WorkOrderNoteBefore.setFSRUserField(trigger.new, trigger.OldMap, trigger.isInsert, trigger.isUpdate);
}