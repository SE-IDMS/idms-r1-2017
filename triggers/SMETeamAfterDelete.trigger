/*
    Author          : Ankit Badhani (ACN) 
    Date Created    : 23/05/2011
    Description     : SME Team after insert event trigger.
*/

trigger SMETeamAfterDelete on RIT_SMETeam__c (After delete)
{
    if(Utils_SDF_Methodology.canTrigger('AP17'))
    {  
        System.Debug('**** SMETeamAfterDelete Trigger Start ****');
        
        AP17_SharingAccessToSMETeam.deleteExistingSMETeamMember(trigger.oldmap);
        
        System.Debug('**** SMETeamAfterDelete Trigger End ****');
        
    }
}