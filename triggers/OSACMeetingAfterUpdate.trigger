trigger OSACMeetingAfterUpdate on OSACMeeting__c (after Update) 
{
/*********************************************************************************
// Trigger Name     : OSACMeetingAfterUpdate  
// Purpose          : OSACMeeting  Before Update event trigger
// Created by       : Global Delivery Team-Mohit
// Date created     : 21th June 2012
// Modified by      :
// Date Modified    :
// Remarks          : For Sep - 12 Release
///********************************************************************************/


Map<String,String> mapOSACPrb = new Map<String,String>();
Set<Id> setPrbId = new Set<Id>();
List<OSACMeeting__c> lstDecision = new List<OSACMeeting__c>();
list<OSACMeeting__c> lstOsac = new list<OSACMeeting__c>();


if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_OsacMeeting'))
{
 //AP_OSACMeeting.UpdatePRBSafetyRiskDecesion(trigger.new);
 

 for(OSACMeeting__c varosac : Trigger.new) {
                
                // Added by Uttara - OCT 2015 Release - BR-6646
                if(Trigger.newmap.get(varosac.Id).Decision__c != Trigger.Oldmap.get(varosac.Id).Decision__c
                   && Trigger.newmap.get(varosac.Id).Decision__c == 'Go') {
                    
                        setPrbId.add(Trigger.newmap.get(varosac.Id).Problem__c); 
                        mapOSACPrb.put(varosac.Id, Trigger.newmap.get(varosac.Id).Problem__c);        
                }

                //oct 15 Release -- BR-7369 -- Divya M
                if((Trigger.newmap.get(varosac.Id).Problem__c != null || Trigger.newmap.get(varosac.Id).Problem__c != '') && Trigger.newmap.get(varosac.Id).Problem__c != Trigger.Oldmap.get(varosac.Id).Problem__c){
                    lstOsac.add(varosac);
                }
                
                if(varosac.Decision__c != Trigger.Oldmap.get(varosac.Id).Decision__c) 
                    lstDecision.add(varosac);
 }
    //oct 15 Release -- BR-7369 -- Divya M
    if(lstOsac.size()>0){
        AP_OSACMeeting.updateProblem(lstOsac);
    }
    //Ends
    
    if(!lstDecision.isEmpty())
        AP_OSACMeeting.UpdatePRBSafetyRiskDecesion(lstDecision,Trigger.new);    
        
 // Added by Uttara - OCT 2015 Release - BR-6646

 //if(mapOSACPrb.keyset().size() > 0 && setPrbId.size() > 0)      

     //AP_OSACMeeting.OsacGoNotifications(setPrbId, mapOSACPrb);
      
}
}