trigger BoRefUpdate on Event (before insert,before update) 
{
      
    Set<id> widset = new Set<id>();
    Map<Id,Id> eidwoidmap=New Map<Id,Id>();
    List<Event> etoProcess = new List<Event>();
   
    for(Event e:Trigger.new)
    {
        System.debug('************** Log1 >> '+Limits.getQueries());
       if(e.whatId != null)
       {
           String s=e.whatid;
           String SOPrefix = SObjectType.SVMXC__Service_Order__c.getKeyPrefix(); 
            if(s.startsWith(SOPrefix ) )
            {
                widset.add(e.whatId);
               
                eidwoidmap.put(e.id,e.WhatId);
                etoProcess.add(e);
                  
            }
          }
    }
    
    if(etoProcess != null && etoProcess.size()>0)
    {
        if(widset!= null && widset.size()>0)
        {
                Map<Id,SVMXC__Service_Order__c> idWorecMap=New Map<Id,SVMXC__Service_Order__c>();
                List<SVMXC__Service_Order__c> woList=[SELECT id, BackOfficeReference__c,WorkOrderName__c,Name,SVMXC__Site__c,SVMXC__Site__r.Name,
                SVMXC__Site__r.SVMXC__Street__c,SVMXC__Site__r.SVMXC__City__c,SVMXC__Site__r.StateProvince__r.Name,SVMXC__Site__r.SVMXC__Zip__c,
                SVMXC__Site__r.LocationCountry__r.Name,SVMXC__Contact__r.Name,Contact_Phone__c FROM SVMXC__Service_Order__c where id in :widset  ];
                idWorecMap.PutAll(woList);
                for(Event ev:etoProcess ){
                    if(eidwoidmap.containskey(ev.id)){
                        
                        if(idWorecMap.containskey(eidwoidmap.get(ev.id))){
                             SVMXC__Service_Order__c so=idWorecMap.get(eidwoidmap.get(ev.id));
                             ev.BackOfficeReference__c  = so.BackOfficeReference__c ;
                             ev.WorkOrderName__c=so.WorkOrderName__c;
                             String WOName;
                             String WONumber;
                             String BORef;
                             String LocName;
                             String LocStreet;
                             String LocCity;
                             String LocState;
                             String LocZip;
                             String Contact;
                             String LocCountry;
                             String ContactPhone;
                             
                             if(so.WorkOrderName__c != null)
                                 WOName = so.WorkOrderName__c;
                             else
                                 WOName = '';
                             if(so.Name != null)
                                 WONumber = so.Name;
                             else
                                 WONumber = '';
                             if(so.BackOfficeReference__c != null)
                                 BORef = so.BackOfficeReference__c;
                             else
                                 BORef = '';  
                             if(so.SVMXC__Site__r.Name != null)
                                 LocName = so.SVMXC__Site__r.Name;
                             else
                                 LocName = '';
                             if(so.SVMXC__Site__r.SVMXC__Street__c != null)
                                 LocStreet = so.SVMXC__Site__r.SVMXC__Street__c;
                             else
                                 LocStreet = '';
                             if(so.SVMXC__Site__r.SVMXC__City__c != null)
                                 LocCity = so.SVMXC__Site__r.SVMXC__City__c;
                             else
                                 LocCity = '';
                             if(so.SVMXC__Site__r.StateProvince__r.Name != null)
                                 LocState = so.SVMXC__Site__r.StateProvince__r.Name;
                             else
                                 LocState = '';
                             if(so.SVMXC__Site__r.SVMXC__Zip__c != null)
                                 LocZip = so.SVMXC__Site__r.SVMXC__Zip__c;
                             else
                                 LocZip = '';
                             if(so.SVMXC__Site__r.LocationCountry__r.Name != null)
                                 LocCountry = so.SVMXC__Site__r.LocationCountry__r.Name;
                             else
                                 LocCountry = '';
                             if(so.SVMXC__Contact__r.Name != null)
                                 Contact = so.SVMXC__Contact__r.Name;
                             else
                                 Contact = ''; 
                             if(so.Contact_Phone__c != null)
                                 ContactPhone = so.Contact_Phone__c;
                             else
                                 ContactPhone = ''; 
                             ev.Description ='';
                             
                             if(WOName != null && WOName !='')
                             ev.Description += 'Work Order Name:'+WOName;
                             // Dec release Started
                             //
                             /*
                             if(BORef != null && BORef !='')
                              {
                               if(ev.Description.length()>0)
                                ev.Description += +';'+'\n'+'Work Order Name:'+WOName;
                                else ev.Description +='Work Order Name:'+WOName;
                              }  
                              */
                              // Dec release Ended 
                              if(BORef != null && BORef !='')
                              {
                               if(ev.Description.length()>0)
                                ev.Description += +';'+'\n'+'Back Office Ref#:'+BORef;
                                else ev.Description +='Back Office Ref#:'+BORef;
                              }                        
                              if(LocName != null && LocName !='')
                              {
                               if(ev.Description.length()>0)
                                ev.Description += +';'+'\n'+'Location:'+LocName ;
                                else ev.Description +='Location:'+LocName ;
                              }
                              if(LocStreet != null && LocStreet !='')
                              {
                               if(ev.Description.length()>0)
                                ev.Description += +';'+'\n'+LocStreet;
                                else ev.Description += LocStreet;
                              }
                              if(LocCity != null && LocCity !='')
                              {
                               if(ev.Description.length()>0)
                                ev.Description += +';'+'\n'+LocCity;
                                else ev.Description += LocCity;
                              }
                              if(LocState != null && LocState !='')
                              {
                               if(ev.Description.length()>0)
                                ev.Description += +';'+'\n'+LocState;
                                else ev.Description += LocState;
                              }
                              if(LocZip != null && LocZip !='')
                              {
                               if(ev.Description.length()>0)
                                ev.Description += +';'+'\n'+LocZip;
                                else ev.Description += LocZip;
                              }
                              if(LocCountry != null && LocCountry !='')
                              {
                               if(ev.Description.length()>0)
                                ev.Description += +';'+'\n'+LocCountry;
                                else ev.Description += LocCountry;
                              }
                               if(Contact != null && Contact !='')
                              {
                               if(ev.Description.length()>0)
                                ev.Description += +';'+'\n'+'Customer Contact#:'+Contact;
                                else ev.Description +='Customer Contact#:'+Contact;
                              }
                              if(ContactPhone != null && ContactPhone !='')
                              {
                               if(ev.Description.length()>0)
                                ev.Description += +';'+'\n'+'Customer Contact Phone#:'+ContactPhone;
                                else ev.Description +='Customer Contact Phone#:'+ContactPhone;
                              }
                              
                             /*
                             ev.Description = 'Work Order Name:'+WOName+';'+'\n'+
                                              'Back Office Ref#:'+BORef+';'+'\n'+
                                              'Location:'+LocName+';'+'\n'+
                                               LocStreet+';'+'\n'+
                                               LocCity+';'+'\n'+
                                               LocState+';'+'\n'+
                                               LocZip+';'+'\n'+
                                               LocCountry+';'+'\n'+
                                              'Customer Contact#:'+Contact+';'+'\n'+
                                              'Customer Contact Phone#:'+ContactPhone;
                                              */
                        }
                        
                    }
                    
                }
        }
    }
 
 }