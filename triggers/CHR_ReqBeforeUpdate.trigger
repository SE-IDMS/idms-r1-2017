/*
    Author          : Srikant Joshi (Schneider Electric)
    Date Created    : 09/10/2012.                     
                      
*/
trigger CHR_ReqBeforeUpdate on Change_Request__c(Before update) {

     if(Utils_SDF_Methodology.canTrigger('CHRRequestTriggerBeforeUpdate'))
     {    
        CHR_ProjectTriggers.CHRBeforeUpdate(Trigger.newMap, Trigger.oldMap);
     }

}