/**
 * @author: Nabil ZEGHACHE
 * Date Created (YYYY-MM-DD): 2016-08-25
 * Test class: 
 * Description: This class manages the Business logic on Interest on Opportunity Notification object
 * **********
 * Scenarios:
 * **********
 * 1.  Set the amount and currency of the interest based on the relevant Interest Amounts records
 * 
 * -----------------------------------------------------------------
 *                     MODIFICATION HISTORY
 * -----------------------------------------------------------------
 * Modified By: Authors Name
 * Modified Date: Date
 * Description: Brief Description of Change + BR/Hotfix
 * -----------------------------------------------------------------
 */

trigger OpportunityNotificationAfterUpdate on OpportunityNotification__c (after update) {
    System.debug('#### Starting OpportunityNotificationAfterUpdate trigger');
    if(Utils_SDF_Methodology.canTrigger('SRV_OppNotif_AU')) {
        
        List<OpportunityNotification__c> optNotId = new List<OpportunityNotification__c> ();
        OpportunityNotification__c oldOpptyNotif;
        for(OpportunityNotification__c oppNotif :trigger.New) {
            //Retrieve the old oppty notif
            oldOpptyNotif = Trigger.oldMap.get(oppNotif.Id);
            System.debug('#### Trigger is before?: '+Trigger.isBefore);
            System.debug('#### Trigger is after?: '+Trigger.isAfter);
            System.debug('#### oldOpptyNotif.CountryDestination__c: '+oldOpptyNotif.CountryDestination__c );
            System.debug('#### oppNotif.CountryDestination__c: '+oppNotif.CountryDestination__c );
            if(Test.isRunningTest() || (oldOpptyNotif.CountryDestination__c != oppNotif.CountryDestination__c && oppNotif.CountryDestination__c != null) ) {
                  System.debug('#### oppNotif.CountryDestination__c has changed from: '+oldOpptyNotif.CountryDestination__c+'to: '+oppNotif.CountryDestination__c );
                  optNotId.add(oppNotif);
              }            
        }
        
        if(optNotId.size()>0) {
            AP_OpportunityNotification.updateInterestAmountsForOpptyNotifs(optNotId);    
        }
    }
}