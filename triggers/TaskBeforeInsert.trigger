/*

 13-June-2012   Srinivas Nallapati  Mkt Sep 2012 release    Initial Creation:BR-1771
 07-Dec-2012    Srinivas Nallapati  AC Dec 12 release   Validation to prevent creation of tasks for person accoutns DEF-
*/
trigger TaskBeforeInsert on Task (before insert) {
    if(Utils_SDF_Methodology.canTrigger('AP25')){
        Set<Id> setAccIds = new set<id>();
        Set<Id> setCaseIds = new set<id>();//2016 Q2 Release
        Map<Id, Case> mapCase = new Map<Id, Case>();//2016 Q2 Release
        List<Task> lstOrphanTasks = new List<Task>();


        for(Task tk:Trigger.new){
            String subject = tk.Subject;
            String description = tk.Description;
            if(subject != null && subject.startsWith('Email:') && description.startsWith('Additional To:')){
                tk.TECH_TaskType__c = Label.CLSEP12MKT02;
            }
            System.debug('tk.WhatId'+tk.WhatId);
            String strWhatId = tk.WhatId; 
            if(strWhatId != null && strWhatId.startsWith('001')){
               setAccIds.add(tk.WhatId);
            }
            
            //Q1 Release Item, Moved from TaskAfterInsert to BeforeInsert , Identify the Most Recently Viewed Contact, if WhoId is blank on Task
            if(tk.WhoId==null && tk.WhatId==null && tk.CallType!=null && Label.CLMAR16CCC23=='True'){
                lstOrphanTasks.add(tk);
            }

            //Added as part of Q2 2016 Release
            if(strWhatId != null && strWhatId.startsWith('500') && tk.WhoId==null) {
                setCaseIds.add(tk.WhatId);
            }
            
        }

        if(setAccIds.size() > 0){
            set<id> setPerAccs = new set<id>();
            setPerAccs = AP25_Task.getPersonAccountIds(setAccIds);
            if(setPerAccs != null)
                for(Task tk:Trigger.new){
                    if(setPerAccs.contains(tk.whatId))
                        tk.addError(System.Label.CLDEC12AC12);
                }
        }
        //Q1 Release Item, Moved from TaskAfterInsert to BeforeInsert , Identify the Most Recently Viewed Contact, if WhoId is blank on Task
        if(lstOrphanTasks.size()>0){
            AP_TaskHandler.updateLatestContact(lstOrphanTasks);
        }

        //Added as part of Q2 2016 Release
        if(setCaseIds.size()>0){
            mapCase = new Map<Id, Case>([select Id, contactID from case where id in:setCaseIds]);
            if(mapCase.size()>0){
                for(Task tk:Trigger.new){
                    String strWhatId = tk.WhatId;
                    if(strWhatId != null && strWhatId.startsWith('500') && tk.WhoId==null) {
                        if(mapCase.containsKey(tk.WhatId)){
                            tk.WhoId=mapCase.get(tk.WhatId).contactid;  
                        }
                    }   
                }
            }   
        }         
    }//End of Can trigger
}    //End of trigger