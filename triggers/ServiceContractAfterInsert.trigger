/*
Added by: Ramu Veligeti
Date: 17/07/2013
Update TECH_IsSVMX RecordPresent__c on Account object if the Account is used in ServiceMax related objects like Work Order, 
Installed Product, Location, Service Contract etc..

Modified By- Deepak
Date-05-03-2014(April Release)
Purpose-Logic to Update Contact's TECH_IsSVMXRecordPresentForContact__c 
//PM Plan automatic creation
        
*/

trigger ServiceContractAfterInsert on SVMXC__Service_Contract__c (After Insert)
{
    Set<Id> AccId = new Set<Id>();
    Set<Id> scId = new Set<Id>();
    List<SVMXC__Service_Contract__c> sclist = New List<SVMXC__Service_Contract__c>();
    Set<Id> ServId = new Set<Id>();
    Set<id>SPID=new Set<id>();
    list<SVMXC__Service_Contract__c>sllist= new list <SVMXC__Service_Contract__c>();
     set<id>slslaset = new set<id>();
    list<SVMXC__Service_Contract__c> sclist1 =  New List<SVMXC__Service_Contract__c>();
    list<SVMXC__Service_Contract__c> PMPlanList =  New List<SVMXC__Service_Contract__c>();
    set<Id> scid1=New set<Id>();
    set<Id> parentidset=New set<Id>();
     Set<Id> Servicelineset = new Set<Id>();
    String ServiceContractLineRT = System.Label.CLAPR15SRV02;//Service Line Record Id.
    String ServiceContractConnectedLineRT = System.Label.CLAPR15SRV03;
    
    for(SVMXC__Service_Contract__c a:trigger.new)
    {   
    
    if(a.RecordTypeid == ServiceContractLineRT || a.RecordTypeid == ServiceContractConnectedLineRT)
    {
        Servicelineset.add(a.ParentContract__c);
    }  
   
   
      if((a.RecordTypeid == ServiceContractLineRT || a.RecordTypeid == ServiceContractConnectedLineRT) && a.SVMXC__Service_Plan__c!=null)
       {
         SPID.add(a.SVMXC__Service_Plan__c);    
         }
         
         if((a.RecordTypeid == ServiceContractLineRT || a.RecordTypeid == ServiceContractConnectedLineRT) && a.SVMXC__Service_Level__c!= null)
       {
       
               slslaset.add(a.id);
                sllist.add(a);
       }
       
        if(a.SVMXC__Contact__c!=null)
        {       
             ServId.add(a.SVMXC__Contact__c);
            // AP_IsSVMXRelatedContact.UpdateContact(ServId );      
        }
        if(a.SVMXC__Company__c!=null)
        {
            AccId.add(a.SVMXC__Company__c);     
           // SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
        }
        //Added by Deepak For April-2015
        if(a.ParentContract__c!=null && (a.RecordTypeId == ServiceContractLineRT || a.RecordTypeId ==ServiceContractConnectedLineRT))
            {
                parentidset.add(a.ParentContract__c);
                scid1.add(a.id);
                sclist1.add(a);
            }
            
            // Added Suhail
            if((a.RecordTypeId == ServiceContractLineRT || a.RecordTypeId ==ServiceContractConnectedLineRT) && a.SKUComRef__c != null && a.SVMXC__Service_Plan__c != null){
                PMPlanList.add(a);
                              
            }
    }
    // Added Suhail
        //system.debug('############');
    if(PMPlanList != null && PMPlanList.size()>0){
            system.debug('############');
        AP_ServiceContract.autoPopulatePMplan(PMPlanList);
    }
    if(ServId != null && ServId.size()>0)
        AP_IsSVMXRelatedContact.UpdateContact(ServId );
    if(AccId != null && AccId.size()>0)
        SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
        
        
     if(slslaset!=null && slslaset.size()>0){
            AP_ServiceContract.Coveredproductslaterm(slslaset,sllist);
        }
        
         if(Servicelineset!=null && Servicelineset.size()>0)
        AP_ServiceContract.LatestDateUpdate(Servicelineset);
    //Added By Deepak for April-2015 Release
     
     //added by anand for service line price calculation
         if(parentidset.size()>0){      
         AP_ServiceContract.SericesContractPriceupdated(parentidset);     
         }
         /*if(sclist1.size()>0 && scid1.size()>0) { 
          AP_ServiceContract.SericesContractPrice(sclist1,scid1);
         }*/
         
         if(SPID!=null&&SPID.size()>0)
         AP_ServiceContract.IncludedServicesfromServicePlan(trigger.new,SPID);
}