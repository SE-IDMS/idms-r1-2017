trigger ATTAfterupdate on AssignedToolsTechnicians__c (After update) {
    
  map<Id,AssignedToolsTechnicians__c> mapATT = new map<Id,AssignedToolsTechnicians__c>();
     if(Utils_SDF_Methodology.canTrigger('AP_ATT_UpdateTechnician')|| Test.isRunningTest())
     {
         for(AssignedToolsTechnicians__c ATT : trigger.new)
         {
             //if(Att.status__c != trigger.oldMap.get(att.Id).status__c)
            // AMO : defect to consider all the changes (Q3 Sept 2016 :)
            // !!!! Method updateTechnicianStatus2 HAVE TO BE REBUILT !!!!!
            mapATT.put(att.id,att);
         }
         if(!mapATT.isEmpty())
             AP_ATT_UpdateTechnician.updateTechnicianStatus2(mapATT);
     }

}