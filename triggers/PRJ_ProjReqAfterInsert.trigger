/*
    Author          : Srikant Joshi (Schneider Electric)
    Date Created    : 09/10/2012
    Description     : To create a Funding Entity by default.                     
                      
*/
trigger PRJ_ProjReqAfterInsert on PRJ_ProjectReq__c (After insert) {

    if(Utils_SDF_Methodology.canTrigger('ProjectRequestTriggerAfterInsert'))
    {
        PRJ_ProjectTriggers.projectAfterInsert(Trigger.new);
    }


}