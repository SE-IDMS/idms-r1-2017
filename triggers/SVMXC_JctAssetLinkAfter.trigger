trigger SVMXC_JctAssetLinkAfter on JctAssetLink__c (after insert, after update) {
	
	if (trigger.isInsert)
		SVMXC_JctAssetLinkAfter.manageIPLink(trigger.New, trigger.OldMap, trigger.isInsert, trigger.isUpdate);
}