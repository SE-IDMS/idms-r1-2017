//Added by Uttara - OCT 2015 Release  - BR-7096

trigger CRBeforeDelete on CommercialReference__c (before delete) {
    
    Set<Id> PrbIds = new Set<Id>();
    
    if(Utils_SDF_Methodology.canTrigger('CRBeforeDelete')) {   
        system.debug('Entered the trigger Affected Product before delete');
        
        for(CommercialReference__c cr : Trigger.Old) {
            
            PrbIds.add(cr.Problem__c);
        }
        
        //if(!PrbIds.isEmpty()) 
            //AP_AffectedProducts.updateCase(PrbIds);
    }
}