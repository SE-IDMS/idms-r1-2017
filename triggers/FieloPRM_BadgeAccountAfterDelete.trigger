/********************************************************************
* Company: Fielo
* Developer: 
* Created Date: 09/03/2015
* Description: 
********************************************************************/

trigger FieloPRM_BadgeAccountAfterDelete on FieloPRM_BadgeAccount__c (after delete) {
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_BadgeAccountTriggers')){
        if(trigger.isAfter && trigger.isDelete){
            FieloPRM_AP_BadgeAccountTriggers.recalculatesPartenerLocatorAccount(trigger.Old, null);
        }
    }
}