/******************************************
* Developer: Fielo Team                   *
*******************************************/
trigger FieloPRM_PRMInvoiceBeforeInsert on FieloPRM_Invoice__c (Before Insert) {

    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_PRMInvoiceTriggers')){
        FieloPRM_AP_PRMInvoiceTriggers.assignsCountryAndCurrency();
        FieloPRM_AP_PRMInvoiceTriggers.checksDuplicates();
    }
   
}