// April 2015 Release created by Gayathri

trigger ComplaintsRequestsEscalationHistoryAfterInsert on ComplaintsRequestsEscalationHistory__c (After INSERT) {

   AP_LCR.UpdateSelectableOrginHistory(trigger.newmap);
}