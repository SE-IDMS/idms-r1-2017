/*
    Author          : Srikant Joshi (Schneider Electric)
    Date Created    : 11/09/2011
    Description     : Budget After Delete event trigger.
*/

trigger BUD_BudgetAfterDelete on Budget__c (after delete) 
{
    System.Debug('****** BUD_BudgetAfterDelete  Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('BudgetTriggerDelete'))
    {
        BUD_BudgetTriggers.budgetAfterDelete(Trigger.old);
    }
    System.Debug('****** BUD_BudgetAfterDelete  Trigger End ****'); 
}