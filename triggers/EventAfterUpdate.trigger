// Y. Tisserand - 03/05/16: Update Customer & FSR start/end dates when quick rescheduling
trigger EventAfterUpdate on Event (after update) {

    List<Event> eventToProcess = new List<Event>();
    //List<Event> eventToProcessWhenOwnerChage = new List<Event>();
    Map<id,Event> eventidRecMap = new Map<id,Event>();
    Map<ID,ID> eventidUseridmap = new Map<id,id>();
    Set<id> userIdset = new Set<id>();
    Map<id,Id> eventidAssToolsTechmap = new Map<id,Id>();
    Map<id,Id> AssToolsTecideventidmap = new Map<id,Id>();
    Map<id,id> useridgmidmap = new Map<id,id>();
    Set<id> workOrderIdset = new Set<id>();
    Map<id,id> AttidWoidMap = new Map<id,id>();
    map<id,id> woidAttidMap = new Map<id,id>();
    Set<id> successattidset = new Set<id>();
    Map<id,String> woidTimezoneMap = new Map<id,String>();
    Map<id,String> techidTimezoneMap = new Map<id,String>();
    
    String SOPrefix = SObjectType.SVMXC__Service_Order__c.getKeyPrefix(); 
    String strCaseKeyPrefix = SObjectType.Case.getKeyPrefix();  //BR-5827 - Added By Vimal K
    Set<Id> setCaseIds = new Set<Id>(); //BR-5827 - Added By Vimal K
    
    
    Map<Id,Event> newOpptyEvents= new Map<Id,Event>();
    Map<Id,Event> events= new Map<Id,Event>();
    String OpptyKeyPrefix = SObjectType.Opportunity.getKeyPrefix();
    Map<Id,Id> opptyEventIds = new Map<Id,Id>();
    
    String AcknowledgeFSE = System.label.CLDEC15SRV02;
         
    
    for(Event eobj : Trigger.new){
        
        if(eobj.WhatId != null) 
        {
            String st= eobj.WhatId;
        
            if( st.substring(0, 3) == SOPrefix   )
            {
                eventToProcess.add(eobj );
                userIdset.add(eobj.ownerId);
                if(eobj.ownerId != Trigger.oldMap.get(eobj.id).ownerId){
                    //eventToProcessWhenOwnerChage.add(eobj);
                    eventidUseridmap.put(eobj.id, eobj.OwnerId);
                    //userIdset.add(eobj.ownerId);

                }
            }
            else if((String.valueOf(eobj.WhatId)).startsWith(strCaseKeyPrefix)){ //BR-5827 - Added By Vimal K
                setCaseIds.add(eobj.whatId);
            }
            else if((String.valueOf(eobj.WhatId)).startsWith(OpptyKeyPrefix)) { //BR-5827 - Moved By Vimal K
                opptyEventIds.put(eobj.Id,eobj.WhatId);
                events.put(eobj.Id,eobj);
            }
        }
    
    }
    
    if(eventToProcess != null && eventToProcess.size()>0){
    
        eventidRecMap.putAll(eventToProcess );
    
         List<AssignedToolsTechnicians__c> techList = new List<AssignedToolsTechnicians__c>();
         
        
         techList=[Select Id,TechnicianEquipment__c,WorkOrder__c,WorkOrder__r.CustomerTimeZone__c,startDateTime__c,EndDateTime__c,DrivingTime__c ,DrivingTimeHome__c,EventId__c,PrimaryUser__c, TECH_CustomerStartDateTime__c, TECH_CustomerEndDateTime__c, TECH_FSRStartDateTime__c, TECH_FSREndDateTime__c  from AssignedToolsTechnicians__c where EventId__c in:eventidRecMap.keySet() ];
         for(AssignedToolsTechnicians__c  attObj:techList){
            woidTimezoneMap.put(attObj.WorkOrder__c, attObj.WorkOrder__r.CustomerTimeZone__c);
            //Event event=  eventidRecMap.get(attObj.EventId__c);
            //attObj.startDateTime__c= event.StartDateTime;
            //attObj.EndDateTime__c = event.EndDateTime;
            if(eventidUseridmap.containskey(attObj.EventId__c))
            {

                    eventidAssToolsTechmap.put(attObj.EventId__c,attobj.id);
                    AssToolsTecideventidmap.put(attobj.id,attObj.EventId__c);
                    if(attObj.PrimaryUser__c)
                    workOrderIdset.add(attObj.WorkOrder__c);
                    AttidWoidMap.put(attobj.id,attobj.WorkOrder__c);
                    woidAttidMap.put(attobj.WorkOrder__c,attobj.id);
                    
            }
         
         }

         if(userIdset != null && userIdset.size()>0){

                List<SVMXC__Service_Group_Members__c> gmList = [select id, SVMXC__Salesforce_User__c, SVMXC__Salesforce_User__r.TimeZoneSidKey from SVMXC__Service_Group_Members__c where SVMXC__Salesforce_User__c in: userIdset];
                if(gmList!= null && gmList.size()>0){

                    for(SVMXC__Service_Group_Members__c gm: gmList){
                        useridgmidmap.put(gm.SVMXC__Salesforce_User__c ,gm.id);
                        techidTimezoneMap.put(gm.SVMXC__Salesforce_User__c,gm.SVMXC__Salesforce_User__r.TimeZoneSidKey);

                     }
                }
         }
         String dateTimeFormatTxt = 'dd MMM yyyy HH:mm';
         String dateTimeFormatNonTxt = 'dd/MM/yyyy HH:mm';

         for(AssignedToolsTechnicians__c  attObj:techList){

            Event event=  eventidRecMap.get(attObj.EventId__c);
            attObj.startDateTime__c= event.StartDateTime;
            attObj.EndDateTime__c = event.EndDateTime;
            
            // Updating customer & FSR start/end dates in email notifications
            if(woidTimezoneMap.get(event.WhatId) != null){
              attObj.TECH_CustomerStartDateTime__c = event.StartDateTime.format(dateTimeFormatNonTxt, woidTimezoneMap.get(event.WhatId));
              attObj.TECH_CustomerEndDateTime__c  = event.EndDateTime.format(dateTimeFormatNonTxt, woidTimezoneMap.get(event.WhatId));
            }else{
              attObj.TECH_CustomerStartDateTime__c = event.StartDateTime.format(dateTimeFormatNonTxt);
              attObj.TECH_CustomerEndDateTime__c  = event.EndDateTime.format(dateTimeFormatNonTxt);
            }
            if(techidTimezoneMap.get(event.WhatId) != null){
              attObj.TECH_FSRStartDateTime__c = event.StartDateTime.format(dateTimeFormatTxt, techidTimezoneMap.get(event.OwnerID));
              attObj.TECH_FSREndDateTime__c  = event.EndDateTime.format(dateTimeFormatTxt, techidTimezoneMap.get(event.OwnerID));
            }else{
              attObj.TECH_FSRStartDateTime__c = event.StartDateTime.format(dateTimeFormatTxt);
              attObj.TECH_FSREndDateTime__c  = event.EndDateTime.format(dateTimeFormatTxt);          
            }
                                
            attObj.DrivingTime__c= event.SVMXC__Driving_Time__c;
            attObj.DrivingTimeHome__c = event.SVMXC__Driving_Time_Home__c;  

            if(eventidUseridmap.containskey(event.id))
            {   

                if(useridgmidmap.containskey(eventidUseridmap.get(event.id)))
                {
                    attObj.TechnicianEquipment__c = useridgmidmap.get(eventidUseridmap.get(event.id));
                }
                
            }

         }


         
         if(techList != null && techList.size()>0)
         {
         
             List<DataBase.SaveResult>  srList = DataBase.update(techList , False);

             for(Integer i=0;i<srList.size();i++){

                     if (srList.get(i).isSuccess()){

                        successattidset.add(srList.get(i).getId());

                     }

             }


         }
        
         if(workOrderIdset != null && workOrderIdset.size()>0)
         {

            list<SVMXC__Service_Order__c> wolist =[select id,SVMXC__Group_Member__c,RescheduleReason__c,OwnerId,SVMXC__Order_Status__c 
            from SVMXC__Service_Order__c where id in :workOrderIdset];
            Map<id,AssignedToolsTechnicians__c> idatSuccmap = new Map<id,AssignedToolsTechnicians__c>();
            List<AssignedToolsTechnicians__c> techList2 = new List<AssignedToolsTechnicians__c>();
            techList2=[Select Id,TechnicianEquipment__c,WorkOrder__c,startDateTime__c,EndDateTime__c, EventId__c  from AssignedToolsTechnicians__c where id in:successattidset and PrimaryUser__c =true  ];
            idatSuccmap.putAll(techList2);

               for(SVMXC__Service_Order__c woobj:wolist){

                    if(woidAttidMap.containskey(woobj.id)){

                        if(idatSuccmap.containskey(woidAttidMap.get(woobj.id)))
                        {
                           id eventid = idatSuccmap.get(woidAttidMap.get(woobj.id)).EventId__c;
                           id userid  = eventidRecMap.get(eventid).ownerid;
                           id ugmid = useridgmidmap.get(userid);
                           woobj.SVMXC__Group_Member__c = ugmid;
                           
                          // if(woobj.SVMXC__Order_Status__c=='Customer Confirmed') //commented BY Anand for DEC 15 Release Def 8850
                              if(woobj.SVMXC__Order_Status__c==AcknowledgeFSE)
                          
                           {
                             woobj.OwnerId = userid;
                             //woobj.RescheduleReason__c = 'Customer Reschedule'; //Commented by Sagar as part of Def 8952
                           }

                        }

                    }


               }
               update wolist;
         }
                    
            
    
    }
    if(setCaseIds.size()>0){ //BR-5827 - Added By Vimal K
        if(Utils_SDF_Methodology.canTrigger('AP_Case_CaseHandler') || Test.isRunningTest()){
            AP_Case_CaseHandler.updateLastActivityDate(setCaseIds);
        }
    }
    if(opptyEventIds.size()>0){
        System.Debug('****** EventAfterUpdate Trigger - First Activity Date for an Opportunity Begins ****'); 
        Map<Id,Opportunity> opptyCreatedDate = new Map<Id,Opportunity>([SELECT CreatedDate FROM Opportunity WHERE Id IN :opptyEventIds.values()]); 
        for(Id eventId: opptyEventIds.keySet()){
            if( opptyCreatedDate.get(opptyEventIds.get(eventId)).CreatedDate.date() > Date.valueOf(Label.CLAPR14SLS62)){
                newOpptyEvents.put(eventId,events.get(eventId));
            }
        }              
        if(!(newOpptyEvents.isEmpty())){
            if(Utils_SDF_Methodology.canTrigger('AP_EventAfterInsertHandler')){
                AP_EventAfterInsertHandler.insertOpptyEvents(newOpptyEvents);
            }
        }
        System.Debug('****** EventAfterUpdate Trigger - First Activity Date for an Opportunity Ends ****'); 
    }
}