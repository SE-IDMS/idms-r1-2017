trigger Coin_bFOPhaseStgae on Milestone1_Milestone__c (after insert, after update) {

Map<Id,Milestone1_Milestone__c> mapMMPhase = new Map<Id,Milestone1_Milestone__c>();

    if(trigger.IsAfter && trigger.IsInsert) {
        
        for(Milestone1_Milestone__c msmPhaseObj:trigger.new) {
            if(msmPhaseObj.stage__c=='completed') {
            
                mapMMPhase.put(msmPhaseObj.id,msmPhaseObj);
            }
          
        }
    }
    
    if(trigger.IsAfter && trigger.IsUpdate){
        for(Milestone1_Milestone__c msmPhaseObj:trigger.new) {
            if(msmPhaseObj.stage__c=='completed' && msmPhaseObj.stage__c !=trigger.oldmap.get(msmPhaseObj.id).stage__c) {

                mapMMPhase.put(msmPhaseObj.id,msmPhaseObj);
            }

        }
    }
    if(mapMMPhase.size() > 0){ 
        AP_COIN_bFO_PhaseCoin.updateTaskCoinStageToPhase(mapMMPhase);
    
    }

}