/*
1-Mar-13    Shruti Karn            PRM Mar13 Release    BR-2782    Initial Creation
5-Mar-13    Srinivas Nallapati     PRM Mar13 Release    BR-2782    Updated
  Oct-15    Pooja Suresh           ACC Oct15 Release               Updated
  Jul-16    Pooja Suresh           ACC Jul16 Release    BR-10118   Updated
  Dec-16    Swapnil Saurav         ACC Dec16 Release    BR-10971   Updated
*/
trigger AccountAfterInsert on Account (after insert) {
    
    System.Debug('****** AccountAfterInsert Trigger Start ****');                   
    List<Account> prmAccLst = new List<Account>();
    set<Id> setAccountId = new set<Id>();
    if(Utils_SDF_Methodology.canTrigger('AP30'))
    {

        List<Id> acctsDBAPilot = new List<Id>();
        List<Id> reassignExtOwnerAcctLst = new List<Id>();
        

        for(Account acc : trigger.new)
        {    
            //Checking for PRMAccount
            if(acc.PRMAccount__c)
                prmAccLst.add(acc);   
                
            //Initiate Integration event List
            if(acc.PRMAccount__c == true && !String.ValueOf(acc.RecordTypeId).StartsWithIgnoreCase(Label.CLAPR15PRM025))
                setAccountId.add(acc.Id);    
                  
            
            //Check if its a DBA Account
            if(acc.DBAPilot__c)
            {
                acctsDBAPilot.add(acc.id);
            }

            //Check if for a Italy account with a state, the Owner Profile is SE - Agent (Global - Community)
            // Nov 16 Release: BR-10649 , added Spain and andorra
            System.debug('--- AccountAfterInsert. TECH_OwnerProfileId__c -> '+acc.TECH_OwnerProfileId__c );
            System.debug('--- AccountAfterInsert. TECH_SDH_CountryCode__c -> '+acc.TECH_SDH_CountryCode__c);
            /*Below if condition has been modified in NOV16 release BR-10649 to include 2 conditions
                1. Country is Italy and State/province is blank
                2. Country is either Spain or Andorra or Italy and State/province is not blank
            */
            if(acc.TECH_SDH_CountryCode__c != null && acc.TECH_SDH_CountryCode__c != '') {
                String countryCode = System.Label.CLNOV16ACC002; //CLNOV16ACC002 = IT,ES,AD
                if(acc.RecordTypeID == System.Label.CLOCT13ACC08 && countryCode.contains(acc.TECH_SDH_CountryCode__c) && acc.TECH_OwnerProfileId__c==label.CLJUL16ACC01)
                {
                    System.debug('---- entered if check for account ownership ----');
                        reassignExtOwnerAcctLst.add(acc.id);
                }
            }
        }

        // Oct15 Release - DBA Pilot
        // Creation of contact record for all DBA Pilot accounts
        if(acctsDBAPilot.size()>0)
            AP30_UpdateAccountOwner.createContactforDBA(acctsDBAPilot);

        // Jul16 Release (BR-10118) - Automatic Ownership Re-Assignment from “External User”
        // Purpose: Initiates ownership assignment if account is created by an external agent with profile SE - Agent (Global - Community) using queue-able job for asynchrous processing.
        if (!reassignExtOwnerAcctLst.isEmpty()) {
            try {
                ID jobID = System.enqueueJob (new AP_ReassignExternalAccountOwner(reassignExtOwnerAcctLst));
                System.debug('--- AccountAfterUpdate.ReassignExternalAccountOwner QueueableJobID -> '+ jobID);
            } catch (LimitException lmtEx) {
               // If any exception, these accounts will be processed by the Batch_ReassignExternalAccountOwner 
               System.debug('--- Exception Enqueue-ing AP_ReassignExternalAccountOwner -> '+lmtEx.getstacktracestring()); 
            }
        }

    }
    
    if(AP_ACC_PartnerAccountUpdate.isFirstExecution && Utils_SDF_Methodology.canTrigger('AP_ACC_PartnerAccountUpdate')  &&  !(System.isBatch()|| System.isFuture()) )
    {
        if(!setAccountId.isEmpty())
            AP_ACC_PartnerAccountUpdate.UpdatePartnerAccount(setAccountId);
    }


    //Akram: listen to PRM events and chek canTrigger
    if(Utils_SDF_Methodology.canTrigger('PartnerLocatorPushEventsService')){
        System.debug('Begins PartnerLocatorPushEventsService'); 
        PartnerLocatorPushEventsService.checkPartnerLocatorPushEventsFromTriggerAccount();
    }
    //End Akram


    
    //////Fielo Syncrhonize//////////////////
    if(!Test.isRunningTest() && Utils_SDF_Methodology.canTrigger('SyncObjects') && SyncObjects.doRunSync == TRUE){
        // Passing only the PRMAccounts to the SyncObjects rather than all the accounts
        SyncObjects instance = new SyncObjects(prmAccLst);
        boolean allornone = false;
        if(!instance.currentState.error ){
            SyncObjectsResult result = instance.syncronize(allornone);
        }
    }
    ///////////////////////////////////////////


    System.Debug('****** AccountAfterInsert Trigger End ****');                   
}