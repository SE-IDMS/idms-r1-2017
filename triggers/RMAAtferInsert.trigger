/*
Oct 15 rel
Divya M
BR-7721
*/
trigger RMAAtferInsert on RMA__c (after Insert) {
    if(Utils_SDF_Methodology.canTrigger('RMAAtferInsert')){
        AP_RR_FieldUpdate.updateTEXShippedFromCustomer(trigger.new);
    }
}