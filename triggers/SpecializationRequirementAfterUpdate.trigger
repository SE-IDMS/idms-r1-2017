trigger SpecializationRequirementAfterUpdate on SpecializationRequirement__c (after update) {
    
    if(Utils_SDF_Methodology.canTrigger('AP_SpecializationRequirement'))
    {
        List<SpecializationRequirement__c> lstSPRUpdatedtoActive = new List<SpecializationRequirement__c>();
        List<SpecializationRequirement__c> lstSPRUpdatedtoInActive = new List<SpecializationRequirement__c>();
        
        List<SpecializationRequirement__c> lstChildSPRUpdatedtoInActive = new List<SpecializationRequirement__c>();
        List<SpecializationRequirement__c> lstClusSPRUpdatedtoActive = new List<SpecializationRequirement__c>();
        
        
        for(SpecializationRequirement__c  spr : trigger.new)
        {    
            SpecializationRequirement__c  OLDspr = trigger.oldmap.get(spr.id);
            if(spr.active__c != OLDspr.active__c &&  spr.SpecializationRecordType__c == 'Global Specialization')
            {
               if(spr.active__c)  
                   lstSPRUpdatedtoActive.add(spr);
               else
                   lstSPRUpdatedtoInActive.add(spr); 
            }
            
            if(spr.active__c != OLDspr.active__c && spr.active__c == false && spr.SpecializationRecordType__c != 'Global Specialization')
            {
                lstChildSPRUpdatedtoInActive.add(spr);
            }   

            if(spr.active__c != OLDspr.active__c && spr.active__c == true) 
                lstClusSPRUpdatedtoActive.add(spr);

        }
        
        //passing to handeler class
        if(!lstSPRUpdatedtoActive.isEmpty())
            AP_SpecializationRequirement.createChildSpecializationRequirements(lstSPRUpdatedtoActive , trigger.newmap.keyset());
        
        if(!lstSPRUpdatedtoInActive.isEmpty())
            AP_SpecializationRequirement.updateChildSpecializationRequirements(lstSPRUpdatedtoInActive);

        if(!lstChildSPRUpdatedtoInActive.isEmpty())
            AP_SpecializationRequirement.deActivateChildAccountSpecializationRequirements(lstChildSPRUpdatedtoInActive);

        if(!lstClusSPRUpdatedtoActive.isEmpty())
            AP_SpecializationRequirement.createNewAccountSpecializationRequirements(lstClusSPRUpdatedtoActive, 'Pending');
        
    }
    
}//End of trigger