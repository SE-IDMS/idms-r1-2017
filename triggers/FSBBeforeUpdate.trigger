trigger FSBBeforeUpdate on FieldServiceBulletin__c (Before Update) {
    
    if(Utils_SDF_Methodology.canTrigger('FSBBU')) {
    
        Set<String> XAFSBids = new Set<String>();
        Set<String> LFSBids = new Set<String>();
        Map<String,String> mapProductFSB = new Map<String,String>();
        List<FieldServiceBulletin__c> toupdateFSB = new List<FieldServiceBulletin__c>();
        
        for(FieldServiceBulletin__c fsb : Trigger.New) {
            if(Trigger.oldMap.get(fsb.Id).Status__c != fsb.Status__c && fsb.Status__c == '6. Completed') {
                if(fsb.Problem__c != null)
                    XAFSBids.add(fsb.Id);
                else
                    LFSBids.add(fsb.Id);
            }
            
            if(fsb.FSBApprover__c == null || (Trigger.oldMap.get(fsb.Id).ProductLine__c != fsb.ProductLine__c)) {
                mapProductFSB.put(fsb.ProductLine__c,fsb.Id);  
                toupdateFSB.add(fsb);
            }
        }
        
        if(!XAFSBids.isEmpty() || !LFSBids.isEmpty())
            AP_FieldServiceBulletin.checkStatusBeforeCompleted(XAFSBids,LFSBids,Trigger.new);
        
        System.debug('!!!! mapProductFSB : ' + mapProductFSB);
        System.debug('!!!! toupdateFSB : ' + toupdateFSB);
        
        if(!mapProductFSB.isEmpty() && !toupdateFSB.isEmpty())
            AP_FieldServiceBulletin.setFSBApprover(mapProductFSB,toupdateFSB);
    }
}