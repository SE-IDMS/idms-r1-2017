trigger OPP_ProjectTriggers on OPP_Project__c (before update) {
    //As per best practise one trigger per object.
    //If the currency is changed for the master project Batch has to recalculate the amount
    if(Trigger.isUpdate){
        List<OPP_Project__c> prjlst=new List<OPP_Project__c>();
        for(integer i = 0; i < trigger.new.size(); i++)
        {
              if(trigger.new[i].CurrencyISOCode!=trigger.old[i].CurrencyISOCode)
                    prjlst.add(trigger.new[i]);
                    
        }
        if(prjlst.size()>0)
            AP_OPP_ProjectTriggers.updateProjectForBatch(prjlst);
    }
}