/*
    Author        : Karthik Shivprakash (ACN) 
    Date Created  : 04/02/2011
    Description   : Case before Update even trigger.

    Modification History:
    ----------------------
    Author:Siddharatha Nagavarapu(Global Delivery Team )
    Purpose:For Dec -11 release to populate the lastlevel1user,lastlevel2user and lastlevel3user to be used in the TAR-2 and TAR-5 reports
    ------------------------------
    Modified By     : Shruti Karn
    Modified Date   : 12 June 2012
    Description     : For September Release to:
    
    1. Call AP10_CaseTrigger.updateFirstAssignmentAge(trigger.new)
    
    ------------------------------
    Modified By     : Vimal Karunakaran
    Modified Date   : 21 June 2012
    Description     : September 2012 Release:
    
    1. Call AP_Case_CheckOpenCCCActions.checkForOpenActionsOnCaseClosure
    
    ------------------------------
    Modified By     : Stephen Norbury
    Modified Date   : 06/03/2013
    Description     : May 2013 Release:
    
    1. BR-1363: Identify the Lead Agents for each Level of Expertise
    2. BR-1363: Identify the Last Agents for each Level of Expertise
    3. BR-2476: Record the Entry and Exit Date / Times for each Level of Expertise
    4. BR-2761: First Assignment Age for each Level of Expertise
    5. BR-2762: Case Ageing
    
    
    ------------------------------
    Modified By     : Rakhi Kumar
    Modified Date   : 13 Feb 2013
    Description     : For May 2013 Release:
    
    1. Invocation of AP37_ChatterForCase.OwnerFollowTheCase commented as part of BR-2153.
    2. Conditional invocation of updateCaseDueDate. The SLA due date will be calculated 
                         until the time the SLA Due Date has been negotiated.
                         
    --------------------------------------------------
    Release version  : OCT Release
    Modified By      : Renjith Jose
    Description      : BR-3128, Pre-populate family field on case  
    
    ------------------------------------------------------------------                    
    --------------------------------------------------
    Release version  : OCT Release
    Modified By      : Vimal K
    Description      : BR-2761, First Assignment SLA 
                       Modified AP_Case_CaseHandler.handleCaseBeforeUpdate to caluclate SLA
    ------------------------------------------------------------------ 
    Release version  : DEC13 Release
    Modified By      : Renjith Jose
    Description      : BR-4393, Case cannot be closed if there is any Complaint/Request is Opened
    
    --------------------------------------------------------------------------
    Release Version   :April15 Release
    Modified By       :Nikhil Agarwal
    Description       :BR-6641 Make Auto Follow up Days and Closure Days dynamic at Customer Care Team Level
    
    --------------------------------------------------------------------------
    Release Version   :April 15 Release
    Modified By       : Deepak Kumar
    Description       :To Update Service Contract If Service Line is updated also to Update Commercial Reference when 
                        Installed Product is Updated.
                        
    --------------------------------------------------------------------------------    
       
*/

trigger CaseBeforeUpdate on Case (before Update){
    
    if(Utils_SDF_Methodology.canTrigger('CaseBeforeUpdate')){
        //Modified on 30 Aug 2016 By Mohammad Naved 
        
        if(AP_Case_CaseHandler.blnUpdateTestRequired || AP_Case_CaseHandler.intTriggerIterationCount < AP_Case_CaseHandler.intAllowedIteration){
            Map<Id,Case> spamRelatedCases = new Map<Id,Case>();
            
            List<Case> casesToUpdate = new List<Case>();
            List<Case> webCasesToUpdate = new List<Case>();
            List<Case> lstVIPCases = new List<Case>();
            //List<Case> caseListModifiedBH = new List<Case>();
            List<Case> caseListModifiedSLA = new List<Case>();
            
            Map<Id,case>closedCaseMap=new Map<Id,case>();
            Map<Id,case>cancelledCaseMap=new Map<Id,case>();
            Map<Id,case>customerCaseMap=new Map<Id,case>();
            List<Case> lstCasesWithSite = new List<Case>(); // Added by Anil Budha (Q3 PA Release)
            List<Case> csList = new List<Case>();       
            Set<Id> scIdset = new Set<Id>();
            
            Map<String,Case> mapProductGMRCase = new Map<String,Case>();
                       
            
            AP10_CaseTrigger.resetFields(trigger.new,trigger.old);
            AP10_CaseTrigger.checkFreeProduct(trigger.new);
            AP10_CaseTrigger.updateContractField(trigger.new);
            
            AP_UpdateMatchingCase.checkCMI_CSI(trigger.newMap); // October 2015 Release BR-7781 - Major Issues 
            for(Case objCase: Trigger.new){             
                        
                if(Trigger.oldMap.get(objCase.Id).AssetOwner__c!=objCase.AssetOwner__c || Trigger.oldMap.get(objCase.Id).ContactID!=objCase.ContactId){
                    lstVIPCases.add(objCase);
                }
                if(objCase.spam__c!=Trigger.OldMap.get(objCase.id).spam__c){                     
                    spamRelatedCases.put(objCase.id,objCase);                        
                }          
                if(Trigger.oldMap.get(objCase.Id).ExternalReference__c !=objCase.ExternalReference__c && objCase.ExternalReference__c == Label.CL00686){
                    AP33_UpdateAnswerToCustomer.addStringToAnswerToCustomer(objCase,objCase.Details__c);
                }
                if(Trigger.oldMap.get(objCase.Id).Site__c!=objCase.Site__c && objCase.Site__c!=null ){
                    lstCasesWithSite.add(objCase);
                }
                
                if(objCase.CCCountry__c==null){
                    if(objCase.Origin!=null){
                        if(objCase.Origin.equalsIgnoreCase(Label.CL00445) && objCase.SuppliedCountry__c != null){
                            webCasesToUpdate.add(objCase);                 
                        }
                        else if((objCase.Origin !=Label.CL00446 || (objCase.Origin == Label.CL00446 && objCase.SuppliedCountry__c == null))
                        && (objCase.origin !=Label.CL00445 || (objCase.origin ==Label.CL00445 && objCase.SuppliedEmail == null))){
                            casesToUpdate.add(objCase);
                        }
                    }  
                }
                
                /* No need as Case BU is driven from Team           
                if(objCase.CCCountry__c != Trigger.OldMap.get(objCase.Id).CCCountry__c && Trigger.OldMap.get(objCase.Id).CCCountry__c != null){
                    caseListModifiedBH.add(objCase);
                }
                */

                if(!Test.isRunningTest() || AP_Case_CheckOpenCCCActions.blnUpdateTestRequired){
                    if(Trigger.OldMap.get(objCase.id).Status!=Label.CL00325 && objCase.Status==Label.CL00325)
                        closedCaseMap.put(objCase.id,objCase);
                    if(Trigger.OldMap.get(objCase.id).Status!=Label.CL00326 && objCase.Status==Label.CL00326) // For Cancelled Cases
                        cancelledCaseMap.put(objCase.id,objCase);
                    if((objCase.ActionNeededFrom__c== Label.CLOCT14CCC09 && (objCase.Status==Label.CL00341 ||  objCase.Status==Label.CL00230 ))) // For Customer Assigned Cases
                        customerCaseMap.put(objCase.id,objCase);
                }
                //Added By Deepak For April 15 Release 
                if( Trigger.OldMap.get(objCase.id).SVMXC__Service_Contract__c!=objCase.SVMXC__Service_Contract__c){
                    csList.add(objCase);
                    //scIdset.add(objCase.SVMXC__Service_Contract__c);
                }
                if(objCase.SVMXC__Component__c != Trigger.OldMap.get(objCase.id).SVMXC__Component__c){
                
                    csList.add(objCase);
                }
                                
                //OCT 2015 Release : Added by Uttara - BR-7096
                //if(objCase.TECH_GMRCode__c != null && objCase.TECH_GMRCode__c != '')
                    //mapProductGMRCase.put(objCase.TECH_GMRCode__c,objCase);    
            }
            
            //OCT 2015 Release : Added by Uttara - BR-7096
            //System.debug('#### mapProductGMRCase : ' + mapProductGMRCase );
            //if((!mapProductGMRCase.isEmpty())) {
                //AP_Case_CheckOpenCCCActions.CaseWithOpenProblems(mapProductGMRCase);
            //}  
            if(lstVIPCases.size()>0){
               AP10_CaseTrigger.vipIconEnhancment(lstVIPCases);
            }        
            if(webCasesToUpdate.size()>0){
                    AP20_UpdateCaseCountry.webCaseCountryUpdate(webCasesToUpdate);
                }
            if(casesToUpdate.size()>0){   
                AP20_UpdateCaseCountry.caseCountryUpdate(casesToUpdate);
            }
            
            if(spamRelatedCases.size()>0){
                System.debug('Batch Size'+spamRelatedCases.size());        
                AP10_CaseTrigger.updtCaseStatus(spamRelatedCases,Trigger.oldMap);
            }      
            if(lstCasesWithSite.size()>0){
               AP10_CaseTrigger.legacySiteIdPopulation(lstCasesWithSite);
            }
            /* Not Required Q2 2016
            if(caseListModifiedBH.size()> 0){
                AP20_UpdateCaseCountry.updateBusinessHours(caseListModifiedBH);
            }
            
            SLA checking is moved to AP_Case_CaseHandler
            if(caseListModifiedSLA.size()>0){
                System.debug('Calling SLA ');
                AP20_UpdateCaseCountry.updateCaseDueDate(caseListModifiedSLA);
            }
            */
            AP_Case_CheckOpenCCCActions.updateWFTriggeredField(Trigger.OldMap,Trigger.newMap);
            
            //Added By Deepak For April 15
            if(csList.Size()>0 ){
                Ap_CaseServicesFunc.CaseSrvContUpdate(csList);
            }
            if(csList.size()>0){
                Ap_CaseServicesFunc.CaseIPCommRefUpdate(csList);
            }
            /*  ------------------------------
            Modified By     : Stephen Norbury
            Modified Date   : 06/03/2013
            Description     : May 2013 Release:

            1. BR-1363: Identify the Lead Agents for each Level of Expertise
            2. BR-1363: Identify the Last Agents for each Level of Expertise
            3. BR-2476: Record the Entry and Exit Date / Times for each Level of Expertise
            4. BR-2761: First Assignment Age for each Level of Expertise
            5. BR-2762: Case Ageing
            */
            AP_Case_CaseHandler.handleCaseBeforeUpdate(trigger.old, trigger.new, trigger.oldMap, trigger.newMap);
            
            if(!Test.isRunningTest() || AP_Case_CheckOpenCCCActions.blnUpdateTestRequired){ //Test Case coverage is handled separately 
                /*  ------------------------------
                Created By      : Renjith Jose
                Created Date    : 11/07/2013
                Modified By     : Vimal Karunakaran
                Modified Date   : 20/02/2014
                Description     :   October 2013 Release: BR-3326 Case cannot be closed if TEX is still open.
                                    DEC-13 Release: <BR-4393> - Case cannot be closed if there is any Complaint/Request related to case with status as ‘Open’, ‘Re-open’, ‘solution found, execution pending’                
                                    Apr 14 BR-1825, Case related all Action in single Class, Handling Cancelled Scenario

                */      
                
                boolean blnIsClosed=true;
                boolean blnIsErrorRequired = true;
                
                if(closedCaseMap.size()>0){
                    blnIsClosed=true;
                    if(AP_Case_CheckOpenCCCActions.checkForOpenTasks(closedCaseMap,blnIsClosed,blnIsErrorRequired)){
                        if(AP_Case_CheckOpenCCCActions.checkForOpenCCCActions(closedCaseMap,blnIsClosed,blnIsErrorRequired)){
                            if(AP_Case_CheckOpenCCCActions.checkOpenTEXs(closedCaseMap,blnIsClosed,blnIsErrorRequired)){ 
                                //if(AP_Case_CheckOpenCCCActions.checkOpenWON(closedCaseMap,blnIsClosed,blnIsErrorRequired))
                                    AP_Case_CheckOpenCCCActions.checkOpenComplaintRequest(closedCaseMap,blnIsClosed,blnIsErrorRequired);//DEC-13 Release 
                            }
                        }
                    }
                }
                if(cancelledCaseMap.size()>0){
                    blnIsClosed=false;
                    System.debug('Create new method in AP_Case_CheckOpenCCCActions to handle these Scenarios');
                    if(AP_Case_CheckOpenCCCActions.checkForOpenTasks(closedCaseMap,blnIsClosed,blnIsErrorRequired)){
                        if(AP_Case_CheckOpenCCCActions.checkForOpenCCCActions(closedCaseMap,blnIsClosed,blnIsErrorRequired)){
                            if(AP_Case_CheckOpenCCCActions.checkOpenTEXs(closedCaseMap,blnIsClosed,blnIsErrorRequired)){ 
                                //if(AP_Case_CheckOpenCCCActions.checkOpenWON(closedCaseMap,blnIsClosed,blnIsErrorRequired)){
                                    if(AP_Case_CheckOpenCCCActions.checkOpenComplaintRequest(closedCaseMap,blnIsClosed,blnIsErrorRequired)){//DEC-13 Release 
                                         if(AP_Case_CheckOpenCCCActions.checkForOpenRROnCaseClosure(cancelledCaseMap)){ 
                                            if(AP_Case_CheckOpenCCCActions.checkForOpenOpptyNotificationOnCaseClosure(cancelledCaseMap))
                                                AP_Case_CheckOpenCCCActions.checkForExternalRefOnCaseClosure(cancelledCaseMap);
                                        }
                                    }    
                                }
                           // }
                        }
                    }
                }
                if(customerCaseMap.size()>0){
                    blnIsClosed=true;
                    blnIsErrorRequired=false;
                    if(AP_Case_CheckOpenCCCActions.checkForOpenTasks(customerCaseMap,blnIsClosed,blnIsErrorRequired)){
                        if(AP_Case_CheckOpenCCCActions.checkForOpenCCCActions(customerCaseMap,blnIsClosed,blnIsErrorRequired)){
                            if(AP_Case_CheckOpenCCCActions.checkOpenTEXs(customerCaseMap,blnIsClosed,blnIsErrorRequired)){ 
                                //if(AP_Case_CheckOpenCCCActions.checkOpenWON(customerCaseMap,blnIsClosed,blnIsErrorRequired))
                                    AP_Case_CheckOpenCCCActions.checkOpenComplaintRequest(customerCaseMap,blnIsClosed,blnIsErrorRequired);//DEC-13 Release 
                            }
                        }
                    }
                }
            }     
            //Modified By Nikhil Agarwal BR-6441 April2015 Release
            AP_Case_CaseHandler.AssignFollowUpandClosureDates(trigger.old, trigger.new, trigger.oldMap, trigger.newMap);
            // End  
        }
    }
 }