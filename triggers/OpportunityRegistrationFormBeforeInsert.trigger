trigger OpportunityRegistrationFormBeforeInsert on OpportunityRegistrationForm__c (before insert) {
    Set<Id> orfOwners = new Set<Id>();
    
    for(OpportunityRegistrationForm__C orf : trigger.new) {
        String strOwnerId = orf.ownerId;
        
        if(strOwnerId.startsWith('005')) {
            orf.Tech_OrfOwner__c = orf.OwnerId;
            orfOwners.add(orf.ownerId);
        }
        else
            orf.Tech_OrfOwner__c = null;    
    }
    
    List<User> orfOwnerAccount = [SELECT Id, Contact.Account.Name FROM User WHERE Id IN :orfOwners];
    Map<Id, User> users = new Map<Id, User>(orfOwnerAccount);    
    for(OpportunityRegistrationForm__C orf : trigger.new) {
        String strOwnerId = orf.ownerId;
        
        if(strOwnerId.startsWith('005') && users.ContainsKey(orf.ownerId)) {
            orf.TECH_PartnerAccountName__c = users.get(orf.ownerId).Contact.Account.Name;
        }
    }
}