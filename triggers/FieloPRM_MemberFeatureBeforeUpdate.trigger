/********************************************************************
* Company: Fielo
* Created Date: 31/07/2015
* Description:
********************************************************************/
trigger FieloPRM_MemberFeatureBeforeUpdate on FieloPRM_MemberFeature__c (Before Update){
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberFeatureBeforeUpdate')){
    
        if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberFeatureBeforeUpdateSetMemberFeatureSync') && !FieloPRM_AP_MemberFeatureTriggers.isRunning){
            FieloPRM_AP_MemberFeatureTriggers.setMemberFeatureSync();
        }
    
        if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberFeatureBeforeUpdateCheckMemberFeatureStatus') && !FieloPRM_AP_MemberFeatureTriggers.isRunning){
            FieloPRM_AP_MemberFeatureTriggers.checkMemberFeatureStatus(Trigger.new, Trigger.oldMap);
        }
        
        List<FieloPRM_MemberFeature__c> feats = new List<FieloPRM_MemberFeature__c>();
        for(Id thisId: Trigger.newMap.keySet()){
            if(Trigger.newMap.get(thisId).F_PRM_IsActive__c != Trigger.oldMap.get(thisId).F_PRM_IsActive__c){
                feats.add(Trigger.newMap.get(thisId));
            }
        }

        if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberFeatureBeforeUpdateRunSync') && !FieloPRM_AP_MemberFeatureTriggers.isRunning){
            FieloPRM_Batch_MemberFeatProcess.runSync(feats);
        }
    
    }
    
}