/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 17 July 2012
    Description : For September Release:
                 1. To Add Users to 'Partner Program Owners' public group when the Program Owner checkbox is checked.
                 2. To remove Users from 'Partner Program Owners' public group when the Program Owner checkbox is unchecked.
    
********************************************************************************************************************/
trigger UserAfterUpdate on User (After Update) {
    list<User> lstUserToAdd = new list<User>();
    list<User> lstToUserRemove = new list<User>();
    list<User> lstUserId = new list<User> ();
    System.debug('Before bypassing UserAfterUpdate');
    for(Id userId : trigger.newMap.keySet())
    {
        if(trigger.newMap.get(userId).ProgramOwner__c != trigger.oldMap.get(userId).ProgramOwner__c )
        {
            if(trigger.newMap.get(userId).ProgramOwner__c)
                lstUserToAdd.add(trigger.newMap.get(userId));
            else
                lstToUserRemove.add(trigger.newMap.get(userId));
        }
        
        if(trigger.newMap.get(userId).ProgramApprover__c != trigger.oldMap.get(userId).ProgramApprover__c || trigger.newMap.get(userId).ProgramAdministrator__c != trigger.oldMap.get(userId).ProgramAdministrator__c)
        {
            if(trigger.newMap.get(userId).ProgramApprover__c || trigger.newMap.get(userId).ProgramAdministrator__c)
                lstUserToAdd.add(trigger.newMap.get(userId));
            else if(trigger.newMap.get(userId).ProgramApprover__c == false && trigger.newMap.get(userId).ProgramAdministrator__c == false)
                lstToUserRemove.add(trigger.newMap.get(userId));
        }
        if(trigger.newMap.get(userId).profileId == system.label.DMT_Profile1 || trigger.newMap.get(userId).profileId == system.label.DMTProfile2 || trigger.newMap.get(userId).profileId == system.label.DMTProfile3 || trigger.newMap.get(userId).profileId == system.label.DMTProfile4)
        {
          system.debug('user profile:' +trigger.newMap.get(userId).profileId);
          lstUserId.add(trigger.newMap.get(userId));
        }
    }
    
    if(Utils_SDF_Methodology.canTrigger('AP_User_ParnterPRGOwnerUpdate')){
        System.debug('No bypassing did not happen for AP_User_ParnterPRGOwnerUpdate');
        if(!lstUserToAdd.isEmpty())
            AP_User_ParnterPRGOwnerUpdate.addUserToGroup(lstUserToAdd);
        if(!lstToUserRemove.isEmpty())
            AP_User_ParnterPRGOwnerUpdate.removeUserFromGroup(lstToUserRemove);
    }
    if(Utils_SDF_Methodology.canTrigger('PRJ_addUserToDMTGroup')){
        System.debug('No bypassing did not happen for PRJ_addUserToDMTGroup');
        if(!lstUserId.isEmpty())
          PRJ_addUserToDMTGroup.AddToGroups(lstUserId);
    }
/**************************************************************************************************     
    Modified by-Deepak for APril-14 Release 
    Date of Mod-05/02/2012  
***************************************************************************************************/ 
    if(Utils_SDF_Methodology.canTrigger('AP_User_TechnicianUpdate')){
        System.debug('No bypassing did not happen for AP_User_TechnicianUpdate');
        list<User> Ulist = new list<User>();
        set<id> uid = new set<Id>();
        for(User u : trigger.new)
        {
        if(trigger.oldMap.get(u.Id).UserRole!=trigger.newMap.get(u.Id).UserRole||
          trigger.oldMap.get(u.Id).UserBusinessUnit__c!=trigger.newMap.get(u.Id).UserBusinessUnit__c||
          trigger.oldMap.get(u.Id).FederationIdentifier!=trigger.newMap.get(u.Id).FederationIdentifier||
          trigger.oldMap.get(u.Id).MobilePhone!=trigger.newMap.get(u.Id).MobilePhone||
          trigger.oldMap.get(u.Id).Email!=trigger.newMap.get(u.Id).Email||
          trigger.oldMap.get(u.Id).Country!=trigger.newMap.get(u.Id).Country||
          trigger.oldMap.get(u.Id).Street !=trigger.newMap.get(u.Id).Street||
          trigger.oldMap.get(u.Id).City !=trigger.newMap.get(u.Id).City||
          trigger.oldMap.get(u.Id).State !=trigger.newMap.get(u.Id).State||
          trigger.oldMap.get(u.Id).PostalCode !=trigger.newMap.get(u.Id).PostalCode||
          trigger.oldMap.get(u.Id).ManagerId !=trigger.newMap.get(u.Id).ManagerId
          ) 
            {
                Ulist.add(u);
                uid.add(u.id);
            }   
        }
        if(Ulist.size()>0)
        AP_User_TechnicianUpdate.UpdateTechnician(Ulist,uid);   
    }
    
    //////Fielo Syncrhonize//////////////////
   /* if(!Test.isRunningTest()){
      Map<Id,User> oldMap = (Map<Id,User>)trigger.OldMap;
        

      List<Id> usersIds = new list<Id>();
      for(fieloee__member__c mem : [select id,fieloee__user__c,F_PRM_LastLoginDateTime__c from fieloee__member__c where fieloee__user__c in:trigger.new]){
        for(user u : trigger.new){
          //if the token or last login date changed, run syncronize future
          if(mem.fieloee__user__c == u.Id && (u.lastlogindate != mem.F_PRM_LastLoginDateTime__c || u.PRMTemporarySamlToken__c  != oldMap.get(u.Id).get('PRMTemporarySamlToken__c'))){
            usersIds.add(u.Id);
          }
        }
      }
      //this trigger is invoked just before the login (login token changed) so the lastlogindate will change after the user logs in and no trigger is invoke for this update. 
      if(!System.isFuture()){
        SyncObjects.executeSyncFuture(usersIds); 
      }
    } */
    ///////////////////////////////////////////
    
    // Assign Permission Sets related to Permission Set Group Assignment Requests
    if(Utils_SDF_Methodology.canTrigger('AP_PermissionSetGroupMethods')){
        System.debug('No bypassing did not happen for AP_PermissionSetGroupMethods');
        Set<Id> activatedUsers = new Set<Id>();
        
        for(User user:Trigger.new) {
        
            if(user.IsActive != Trigger.oldMap.get(user.Id).IsActive) {
                activatedUsers.add(user.Id);
            }
        }    
        
        AP_PermissionSetGroupMethods.assignPs(activatedUsers);
    }
}