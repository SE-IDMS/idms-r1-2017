trigger EntityKPIContributorAfterUpdate on Entity_KPI_Contributors__c (after update) {

System.Debug('****** EntityKPIContributorAfterUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('EntityKPIContributorAfterUpdate'))
    {
   //if(Trigger.old[0].User__c != Trigger.new[0].User__c)
     ConnectEntityKPIContributorTriggers.EntityKPIContributorAfterUpdate(Trigger.new,Trigger.old[0]);   
        
    }
 System.Debug('****** EntityKPIContributorAfterUpdate Trigger End ****'); 
 }