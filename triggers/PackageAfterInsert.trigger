/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 12 November 2012
    Description : For Devember 12 Release:
                 1. Calls AP_PackageTriggerUtils.updateContractTypeto update Support Contract Level on Contract based on the Package Level.
    
********************************************************************************************************************/
trigger PackageAfterInsert on Package__c (after insert) {
   // if(Utils_SDF_Methodology.canTrigger('AP_PackageTriggerUtils') || test.isRunningTest())
        AP_PackageTriggerUtils.updateContractType(trigger.new);
}