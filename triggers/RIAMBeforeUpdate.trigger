trigger RIAMBeforeUpdate on ReturnItemsApprovalMatrix__c(before update) {
  
  /*********************************************************************************
// Trigger Name     : RIAMBeforeUpdate
// Purpose          : Uniqness of the records
// Created by       : Sukumar Salla  - Global Delivery Team - IPO
// Date created     : 14-mar-2013
// Modified by      :
// Date Modified    :
// Remarks          : May 2013 Release
///********************************************************************************/
if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_NewRIAM'))
{      
    AP_RIAM_CreateNewRIAM.createNewRIAM(trigger.new);
    
}

}