trigger IPOCascadingMilestoneBeforeInsert on IPO_Strategic_Cascading_Milestone__c (before insert) {

  System.Debug('****** ConnectCascadingMilestoneBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('IPOCascadingMilestoneBeforeInsert'))
    {
        IPOCascadingMilestoneTriggers.IPOCascadingMilestoneBeforeInsertUpdate(Trigger.new);
        IPOCascadingMilestoneTriggers.IPOStrategicCascadingMilestoneBeforeInsert(Trigger.new);
        IPOCascadingMilestoneTriggers.IPOCascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity(Trigger.New);
           if (Trigger.new[0].ValidateScopeonInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_Cascading_Milestone_InsertUpdate_ERR1);
                 
          
 }
 System.Debug('****** ConnectCascadingMilestoneBeforeInsert Trigger End ****'); 
}