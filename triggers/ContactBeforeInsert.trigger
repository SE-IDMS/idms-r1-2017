/*
07-Dec-2012 Srinivas Nallapati  Dec12 AC release 
Modification History:
Modified by: Siddharth Nagavarapu(sesa206167)   
Purpose: added a logic to update contact owner if the account owner is sdh user.
*/
trigger ContactBeforeInsert on Contact (before insert) {
    
    if(Utils_SDF_Methodology.canTrigger('AP54'))
    {
        List<Contact> lstContactToCopyAddress = new List<Contact>();
        Set<Id> accIds = new Set<Id>();
        Set<String> countryCodeLst = new Set<String>();
        Set<String> stateCodeLst = new Set<String>();
        
        for(Contact con: trigger.new)
        {
            if(con.AccountId != null)   //removed 'con.UpdateAccountAddress__c && ' from logic to include BackToStandard Q2 2016 updates to address fields
            {
                lstContactToCopyAddress.add(con);
                accIds.add(con.AccountId);
                countryCodeLst.add(con.OtherCountryCode);
                stateCodeLst.add(con.OtherStateCode);
            }
              
        }
        
        
        if(lstContactToCopyAddress.size() > 0)
           AP54_Contact.copyAddressFromAccount(lstContactToCopyAddress, accIds, countryCodeLst, stateCodeLst);
    }
    
    
    if(Utils_SDF_Methodology.canTrigger('AP_CONCreateWithSEContactID'))
    {
        AP_CONCreateWithSEContactID.checkSEContactID(Trigger.New);
    }
    if(Utils_SDF_Methodology.canTrigger('AP_ContactPreferredAgent'))
    {       
        AP_ContactPreferredAgent.insertPreferredAgent(Trigger.New);
    }
    if(Utils_SDF_Methodology.canTrigger('AP_Contact_PartnerUserUpdate'))
    {       
        AP_Contact_PartnerUserUpdate.updatePRMAdminEmail(Trigger.New);
    }
    //If the switch (CLNOV13ACC20) is not set for bulk updates, we use the trigger
    if(Utils_SDF_Methodology.canTrigger('AP_updateContactOwner') && System.LABEL.CLNOV13ACC20!='FOR_SDH_BULK_UPDATES')
    {
        Map<Id,List<Contact>> tobeupdatedAccountContactMap=new Map<Id,List<Contact>>();
        for(Contact contactRecord:Trigger.New)
        {
            if(contactRecord.ownerId==ID.valueOf('005A0000001PC0pIAG')){
                if(tobeupdatedAccountContactMap.containsKey(contactRecord.AccountId))
                    tobeupdatedAccountContactMap.get(contactRecord.AccountId).add(contactRecord);
                else
                    tobeupdatedAccountContactMap.put(contactRecord.AccountId,new List<Contact>{contactRecord});
            }            
        }
        if(tobeupdatedAccountContactMap.size()>0)
           {
            AP_updateContactOwner.updateContactOwner(tobeupdatedAccountContactMap);
           }

    }
}