/*
   Author: Amitava Dutta
   Date Of Creation: 08/02/2011
   Description : Objective Settings Before Insert event trigger

*/

trigger ObjectivesSettingsBeforeInsert on ObjectivesSettings__c (before Insert) 
{
     System.Debug('****** ObjectivesSettingsAfterInsert Trigger Start ****');  
     
     List<ObjectivesSettings__C> objectivesettingslist = new List<ObjectivesSettings__c>();
     if(Utils_SDF_Methodology.canTrigger('AP03'))
     {
         for(integer i = 0; i<Trigger.New.Size() ; i++)
         {
             if(Trigger.New[i].ObjectiveLeadersManagerName__c == 'N/A')
             {
                 objectivesettingslist.add(Trigger.New[i]);
             }
         }
     
         
         AP03_ObjectiveSettingsTrigger.UpdateObjectiveLeaderOnObjectives(Trigger.New);
         if(objectivesettingslist.size() > 0)
         {
             AP03_ObjectiveSettingsTrigger.UpdateObjectiveLeaderApprover(Trigger.New);
         }    
     }
     System.Debug('****** ObjectivesSettingsAfterInsert Trigger End****');  
}