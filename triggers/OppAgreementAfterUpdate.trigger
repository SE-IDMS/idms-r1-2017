/*
  Author : IDC Team
  Created On: 21/01/2011.
  Description:Opp Agreement After Update Event Trigger 
*/


trigger OppAgreementAfterUpdate  on OppAgreement__c (after update) 
{

    System.Debug('****** OppAgreementGrantAgreementManagerAccess Trigger Start ****');  
    
    if(Utils_SDF_Methodology.canTrigger('AP04'))
    {
        List<OppAgreement__c> newAgreementlist = new List<OppAgreement__c>();
        List<OppAgreement__c> oldAgreementlist = new List<OppAgreement__c>();
        Map<Id,oppAgreement__c> agreementOppForUpdate = new Map<Id,OppAgreement__c>();
        
        for(integer i = 0; i < trigger.new.size(); i++) 
        {
            System.Debug('****New Manager: '+trigger.new[i].AgreementManager__c);
            System.Debug('****Old Manager: '+trigger.old[i].AgreementManager__c);
                if(trigger.new[i].AgreementManager__c !=null) 
                {
                    if(trigger.old[i].AgreementManager__c != null)
                    {
                        if(trigger.new[i].AgreementManager__c != trigger.old[i].AgreementManager__c) {
                            newAgreementList.add(trigger.new[i]);
                            oldAgreementList.add(trigger.old[i]);
                        }
                    } 
                    else
                    {
                         newAgreementList.add(trigger.new[i]);
                    }   
                }
                else
    
                {
                
                    if(trigger.old[i].AgreementManager__c != null)
                    
                    {
                    
                    oldAgreementList.add(trigger.old[i]);
                    
                    }
            
               }
               
            //Modified for BR-6716 - Check of Agreement Type is removed
            /*Added for December Release to update OEM agreement opportunity phase/sales stage*/   
            if(trigger.new[i].PhaseSalesStage__c!=trigger.old[i].PhaseSalesStage__c){
                if(trigger.new[i].PhaseSalesStage__c!='7 - Deliver & Validate'){
                    agreementOppForUpdate.put(trigger.new[i].id,trigger.new[i]);
                }
            }
        }
      
        if(newAgreementList.size() != 0 ) 
        {
                AP04_AgreementTrigger.grantAccessUpdate(newAgreementList, oldAgreementList);
        }
        /*Added for December Release to update OEM agreement opportunity phase/sales stage*/
        if(agreementOppForUpdate.values().size()>0){
            AP04_AgreementTrigger.updateOEMAgreementOpp(agreementOppForUpdate);
        }
   }  
   System.Debug('****** OppAgreementGrantAgreementManagerAccess Trigger End****');   
}