/*
    Author          : Srikant Joshi (Schneider Electric)
    Date Created    : 11/07/2012
    Description     : Program Costs Forecasts Beforeupdate event trigger.
*/
trigger PROG_CostForecastBeforeUpdate on ProgramCostsForecasts__c (before update) {

    if(Utils_SDF_Methodology.canTrigger('ProgramCostForecastTriggerBeforeUpdate'))
    {
        PROG_CostForecastTriggers.ProgCostForecastbeforeupdate(Trigger.newmap,Trigger.oldmap);
    }

}