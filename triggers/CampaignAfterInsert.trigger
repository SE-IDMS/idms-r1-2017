/*
26-Oct-2012     Srinivas Nallapti       Initial Creation: Dec12 Mkt release 
*/
trigger CampaignAfterInsert on Campaign (after insert) {
    if(Utils_SDF_Methodology.canTrigger('AP53'))
    { 
        AP53_Campaign AP53 = new AP53_Campaign();
        
        set<id> setGlobalCampaignIds = new set<id>();
        for(Campaign cam : trigger.new)
        {
            if(cam.Global__c)
               setGlobalCampaignIds.add(cam.id);
        }
        AP53.createUpdateCampaignCountry(setGlobalCampaignIds, new set<id>());
    }//End of canTrigger
}