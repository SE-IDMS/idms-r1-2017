//To update New Old Forecast Value 

trigger OfferLifeForecastAfterInsert on OfferLifeCycleForecast__c (after Insert,after Update) {
    if(AP_COIN_bFO_OfferLifeCycleForecast.isOfferExt==false) {
        if(Trigger.isInsert && trigger.isAfter) {    
            AP_COIN_bFO_OfferLifeCycleForecast.UpdaeForecastBCAValue(Trigger.newMap,Trigger.OldMap);
            AP_COIN_bFO_OfferLifeCycleForecast.toUpdatTotalBCA(Trigger.newMap);
        }
        if(Trigger.isupdate && trigger.isAfter) {
            AP_COIN_bFO_OfferLifeCycleForecast.afterUpdateForecastBCAValue(Trigger.newMap,Trigger.OldMap);
            AP_COIN_bFO_OfferLifeCycleForecast.toUpdatTotalBCA(Trigger.newMap);
        } 
        if(Trigger.isupdate && trigger.isBefore){
        AP_COIN_bFO_OfferLifeCycleForecast.ProjectLaunchbeforeupdate(Trigger.newMap,Trigger.OldMap);        
        }
       AP_COIN_bFO_OfferLifeCycleForecast.isOfferExt=true;    
   }    
}