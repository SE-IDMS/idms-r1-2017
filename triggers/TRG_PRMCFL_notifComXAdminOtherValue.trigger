/*

before insert
before update
before delete
after insert
after update
after delete
after undelete
This is a Pilot - will not follown best practise....
*/

trigger TRG_PRMCFL_notifComXAdminOtherValue on ContentVersion (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
   System.debug('*** Start notifComXAdminOtherValue ');
      if(Trigger.isBefore){
         System.debug('********************************************************** Trigger.isBefore');
         if(Trigger.isInsert){
            System.debug('*** Trigger isBefore + isInsert');
         }         
         if(Trigger.isUpdate){
            System.debug('*** Trigger isBefore + isUpdate');
            //trigger.new[0].addError('*** Trigger isBefore + isUpdate : newId=' + Trigger.old[0].ContentModifiedById + ' OwnerId=' + Trigger.old[0].OwnerId);
            Integer idx = 0;
            for ( ContentVersion cv : Trigger.new ) {
               if (cv.ContentModifiedById != cv.OwnerId) {
                 trigger.new[idx].addError(Label.CLOCT15CF062);  
               }  
               idx = idx +1;
            }


         }   
         if(Trigger.isDelete){
            System.debug('*** Trigger isBefore + isDelete');
         }   
      }
      
      if(Trigger.isAfter){
         System.debug('********************************************************** Trigger.isAfter');
         if(Trigger.isInsert){
            //AP_ContentLifecycleTriggerHandler.onAfterInsert(Trigger.new);
            System.debug('*** Trigger isAfter+isInsert ');
            for (ContentVersion cv : Trigger.new) {
               System.debug('--- cv.PRODUCT_MANUFACTURER__c=' + cv.PRODUCT_MANUFACTURER__c);
               System.debug('--- cv.PRODUCT_RANGE__c=' + cv.PRODUCT_RANGE__c);
               if (cv.PRODUCT_MANUFACTURER__c=='Other' || cv.PRODUCT_RANGE__c=='Other') {
                  System.debug('*** Sending email to ComX Administrator :' );
                  System.debug('--- cv.PRODUCT_BRAND_NEW_VALUE__c=' + cv.PRODUCT_BRAND_NEW_VALUE__c);
                  System.debug('--- cv.DEVICE_TYPE_NEW_VALUE__c=' + cv.DEVICE_TYPE_NEW_VALUE__c);
                  AP_PRMCFL_ContentLifecycleTriggerHandler.sendEmailtoComXAdmin(cv.ContentDocumentId, cv.DEVICE_TYPE_NEW_VALUE__c, cv.PRODUCT_BRAND_NEW_VALUE__c);
               }
            }
         }
         if(Trigger.isUpdate){
            System.debug('*** Trigger isAfter+isUpdate ');
            Integer idx = 0;
            for ( ContentVersion cv : Trigger.new ) {
               if (cv.ContentModifiedById != cv.OwnerId) {
                 trigger.new[idx].addError(Label.CLOCT15CF062);  
               }  
               idx = idx +1;
            }            

          }
         if(Trigger.isDelete){
            System.debug('*** Trigger isAfter+isDelete ');
          } 
         if(Trigger.isUndelete){
            System.debug('*** Trigger isAfter+isUndelete ');
          }   
      }   
         System.debug('*** Trigger.old=' + Trigger.old);
         System.debug('*** Trigger.oldMap=' + Trigger.oldMap);
         System.debug('*** Trigger.new=' + Trigger.new);
         System.debug('*** Trigger.newMap=' + Trigger.newMap);
         System.debug('*** Trigger.size=' + Trigger.size);


   System.debug('*** End notifComXAdminOtherValue ');   
}