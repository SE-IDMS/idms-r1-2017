/*
    Author        : Srikant Joshi 
    Date Created  : 30/07/2013
    Description   : Complaints/Request After Update trigger.
    
*/


    
     
trigger ComplaintsAfterUpdate on ComplaintRequest__c(After Update) {
system.debug('Complaint Reqeust After uPDATE - STARTS');
if(Utils_SDF_Methodology.canTrigger('AP_LCR2'))
   {
     SYSTEM.DEBUG('TEST');
     AP_LCR.updateDetectionOrg(trigger.newmap,trigger.oldmap);
     AP_LCR.updateCasefieldsBasedOnCR(trigger.new); // I2P: APRIL2014 RELEASE, 19FEB2014
     //October 2014 Release.
    // AP_sendEscalationMails.updateStakeholderonSH(Trigger.new,trigger.old,'ComplaintRequest__c','IssueOwner__c','ResolutionDueDate__c','AccountableOrganization__c'); 
   }
    
   
    
}