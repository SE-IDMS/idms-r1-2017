/*
    Author          : Stephen Norbury 
    Date Created    : 06/03/2013
    Description     : Care Agent Before Insert / Update.
    
    Change History:
    
    BR-2858: Add Care Agent as Team Member
*/

trigger CareAgentBeforeInsertUpdate on CareAgent__c (before insert, before update)
{
  // Fetch the Agents
  
  Set <Id> AgentIds = new Set <Id>();
  
  Map <Id, User> UserMap = new Map <Id, User>();
  
  for (CareAgent__c agent : trigger.new)
  {
    if (agent.User__c != null) { AgentIds.add (agent.User__c); }
  }
  
  List <User> users = [select Id, Name from User where Id in :AgentIds];
  
  if (Users != null && Users.size() != 0)
  {
  	for (User u : Users)
  	{
  	  UserMap.put (u.Id, u);
  	}
  	
  	for (CareAgent__c agent : trigger.new)
    {
      if (agent.User__c != null && UserMap.get (agent.User__c) != null) { agent.AgentName__c = UserMap.get (agent.User__c).Name; }
    }
  }
  
  // End
}