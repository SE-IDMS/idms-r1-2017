/********************************************************************************************************************
    Created By : Shruti Karn
    Description : For PRM May 13 Release:
                 1. To insert all the child records when a new Contact Assigned Program is created.
    
********************************************************************************************************************/
trigger ContactProgramAfterInsert on ContactAssignedProgram__c (after insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_CON_ContactProgramUpdate') || test.isRunningTest())
    {
        AP_CON_ContactProgramUpdate.createCONChildRecords(trigger.new);
    }

    if(Utils_SDF_Methodology.canTrigger('AP_PartnerContactAssignementMigration'))
    {
        AP_PartnerContactAssignementMigration.createAndDeleteMemberProperties(null,trigger.newMap);
    }
}