trigger AssessementProgramBeforeAction on AssessementProgram__c(before insert, before update){
    if(Utils_SDF_Methodology.canTrigger('AP_AssessmentHandler')) {
    AP_AssessmentHandler.checkProgramLevel(Trigger.new);
    }
}