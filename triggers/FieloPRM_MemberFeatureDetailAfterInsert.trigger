trigger FieloPRM_MemberFeatureDetailAfterInsert on FieloPRM_MemberFeatureDetail__c (after insert) {
    FieloPRM_AP_MemberFeatureDetailTriggers.memberFeatureActivatesInactivates(Trigger.New, null);
}