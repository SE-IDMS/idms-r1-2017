/******************************************
* Developer: Fielo Team                   *
*******************************************/
trigger FieloPRM_PRMInvoiceDetailBeforeInsert on FieloPRM_InvoiceDetail__c (Before Insert) {

    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_PRMInvoiceDetailTriggers')){
        FieloPRM_AP_PRMInvoiceDetailTriggers.assignsCurrency();
    }
   
}