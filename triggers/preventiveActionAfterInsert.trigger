//*********************************************************************************
// Trigger Name     : preventiveActionAfterInsert
// Purpose          : Preventive Action After Insert event trigger
// Created by       : Global Delivery Team
// Date created     : 29th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/

trigger preventiveActionAfterInsert on PreventiveAction__c (after insert) {
    System.Debug('****** preventiveActionAfterInsert Trigger Start ****');      
    
    if(Utils_SDF_Methodology.canTrigger('AP1004')){              
        List<PreventiveAction__c> preventiveActionList = new List<PreventiveAction__c>(); 
        
        for(PreventiveAction__c prvtveActn: Trigger.new){
            if((prvtveActn.RelatedProblem__c != null) && (prvtveActn.Owner__c != null))
                preventiveActionList.add(prvtveActn);
        }
        //Add All Preventive Action Owner  to the Problem Share for Sharing the Problem Record
        AP1004_PreventiveAction.addPreventiveOwnerToProblemShare(preventiveActionList);
    }                                               
    System.Debug('****** preventiveActionAfterInsert Trigger End ****'); 
}