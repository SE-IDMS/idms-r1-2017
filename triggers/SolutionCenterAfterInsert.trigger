//*********************************************************************************
// Trigger Name     : SolutionCenterAfterInsert
// Purpose          : SolutionCenter  After Insert event trigger
// Created by       : Global Delivery Team
// Date created     : 
// Modified by      :
// Date Modified    :
// Remarks          : For April Sales - 12 Release
///********************************************************************************/
trigger SolutionCenterAfterInsert on SolutionCenter__c (After insert) {
     if(Utils_SDF_Methodology.canTrigger('AP38')){
        AP38_createSolutionCenterCountry.createNewSolutionCenterCountry(Trigger.new);
     }
}