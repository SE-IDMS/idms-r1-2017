/******************************************
* Developer: Fielo Team                   *
*******************************************/
trigger FieloPRM_ProductPointListBeforeInsert on FieloPRM_ProductPointList__c (Before Insert) {


    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_ProductPointListTriggers')){
        FieloPRM_AP_ProductPointListTriggers.validatesPointListOverlappingDates();
    }
   
}