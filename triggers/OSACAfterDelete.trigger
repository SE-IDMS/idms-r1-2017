/*
Oct 15 Release
Divya M
BR-7369
*/

trigger OSACAfterDelete on OSACMeeting__c (after delete) {
    if(Utils_SDF_Methodology.canTrigger('OSACAfterDelete')){   
        list<OSACMeeting__c> lstOsac = new list<OSACMeeting__c>();
        for(OSACMeeting__c osac : trigger.old){
            if(osac.Problem__c != null || osac.Problem__c != ''){
                lstOsac.add(osac);
            }
        }
        
        if(lstOsac.size()>0){
            AP_OSACMeeting.updateProblem(lstOsac);
        }
    }
}