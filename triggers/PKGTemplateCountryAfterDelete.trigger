/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 17 July 2012
    Description : For September Release:
                 1. To update Available in Countries Field on related Package Template
    
********************************************************************************************************************/
trigger PKGTemplateCountryAfterDelete on PackageTemplateCountry__c (after delete) {
    if(Utils_SDF_Methodology.canTrigger('AP_PKGTemplateCountryTriggerUtils') || test.isRunningTest())
        AP_PKGTemplateCountryTriggerUtils.updateTemplateCountry(trigger.oldMap);
}