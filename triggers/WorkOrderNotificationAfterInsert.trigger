/*
    Author          : Ankit Badhani (ACN) 
    Date Created    : 01/02/2011
    Description     : Work Order Notification after insert event trigger.
*/

trigger WorkOrderNotificationAfterInsert on WorkOrderNotification__c (after insert) {
    /*
    if(Utils_SDF_Methodology.canTrigger('AP10')){
        Set<Id> CaseIds = new Set<Id>();
            for(WorkOrderNotification__c workOrderObject: Trigger.new){
                CaseIds.add(workOrderObject.Case__c);   
        }
        if(CaseIds.size()>0){
            AP08_CaseStatusUpdate.updateCaseStatus(CaseIds);
        }
    }
    */
    
    if(Utils_SDF_Methodology.canTrigger('AP10')){
        Map<id,id> wnidcidmap = new Map<id,id>();
        Set<id> cidset = new Set<id>();
    
        List<WorkOrderNotification__c> toProcess = new List<WorkOrderNotification__c>();
        for(WorkOrderNotification__c  wn:Trigger.new)
        {
            if(wn.Case__c != null)
            {
                toProcess.add(wn);
                wnidcidmap.put(wn.id,wn.Case__c); 
                cidset.add(wn.Case__c);
                
            }
        }
        if(toProcess != null && toProcess.size()>0){
            Map<id,List<SVMXC__Case_Line__c>> cidClinesMap = new Map<id,List<SVMXC__Case_Line__c>>();
            List<WONInstalledProductLine__c> liwoniplineList = new List<WONInstalledProductLine__c>();
            List<SVMXC__Case_Line__c> clinelist = [select id,SVMXC__Installed_Product__c,SVMXC__Case__c from  SVMXC__Case_Line__c where SVMXC__Case__c  in :cidset];
            for(SVMXC__Case_Line__c clobj:clinelist ){
                if(cidClinesMap.containskey(clobj.SVMXC__Case__c)){                
                    cidClinesMap.get(clobj.SVMXC__Case__c).add(clobj);                
                }
                else{
                     cidClinesMap.put(clobj.SVMXC__Case__c, new List<SVMXC__Case_Line__c>());
                     cidClinesMap.get(clobj.SVMXC__Case__c).add(clobj);
                }
            }
            for(WorkOrderNotification__c won:toProcess ){
            
                if(cidClinesMap.containskey(won.Case__c))
                {
                    for(SVMXC__Case_Line__c  cline:  cidClinesMap.get(won.Case__c))
                    {
                        WONInstalledProductLine__c wonipl = new WONInstalledProductLine__c();
                        wonipl.WorkOrderNotification__c = won.id;
                        wonipl.InstalledProduct__c= cline.SVMXC__Installed_Product__c;
                        liwoniplineList.add(wonipl);
                        
                    }
                    
                
                }
            
            }
            
            if(liwoniplineList!= null && liwoniplineList.size()>0)
            {
                insert liwoniplineList;
            }
            
        }

    
    }
}