//*********************************************************************************
// Trigger Name     : correctiveActionrAfterUpdate
// Purpose          : Containment Action After Update event trigger
// Created by       : Global Delivery Team
// Date created     : 27th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/

trigger correctiveActionrAfterUpdate on CorrectiveAction__c (after update) {
    System.Debug('****** correctiveActionrAfterUpdate Trigger Start ****');      
    
    if(Utils_SDF_Methodology.canTrigger('AP1003')){              
        Map<Id,CorrectiveAction__c> crtveActnOldMap = new Map<Id,CorrectiveAction__c>(); 
        Map<Id,CorrectiveAction__c> crtveActnNewMap = new Map<Id,CorrectiveAction__c>(); 
        
        for(CorrectiveAction__c crtveActn: Trigger.new){
            if(Trigger.oldMap.get(crtveActn.id).Owner__c != Trigger.newMap.get(crtveActn.id).Owner__c)
                crtveActnOldMap.put(crtveActn.id,Trigger.oldMap.get(crtveActn.id));
                crtveActnNewMap.put(crtveActn.id,Trigger.newMap.get(crtveActn.id));
        }
        
        if(crtveActnOldMap.size()>0 && crtveActnNewMap.size()>0){ 
            AP1003_CorrectiveAction.updateCorrectiveOwnerToProblemShare(crtveActnOldMap,crtveActnNewMap); 
        }                  
    }                                               
    System.Debug('****** correctiveActionrAfterUpdate Trigger End ****'); 
}