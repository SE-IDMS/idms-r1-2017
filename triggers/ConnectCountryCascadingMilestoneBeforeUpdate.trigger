/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 07-Feb-2013
    Description     : Connect Country Cascading Milestones Before Insert event trigger.
*/

trigger ConnectCountryCascadingMilestoneBeforeUpdate on Country_Cascading_Milestone__c (before Update) {
System.Debug('****** ConnectCountryCascadingMilestoneBeforeUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CountryCascadingMilestoneBeforeUpdate'))
    {
    ConnectCountryCascadingMilestoneTriggers.CountryMilestoneBeforeUpdate_ProgressSummaryUpdate(Trigger.new);
         if((Trigger.new[0].Country__c != Trigger.old[0].Country__c) )
        ConnectCountryCascadingMilestoneTriggers.ConnectCountryCascadingMilestoneBeforeInsertUpdate(Trigger.new); 
        ConnectCountryCascadingMilestoneTriggers.ConnectCountryCMBeforeInsertUpdate(Trigger.new);      
        
         if (Trigger.new[0].ValidateScopeonInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.ConnectCountryInsertandUpdate);
 }
 System.Debug('****** ConnectCountryCascadingMilestoneBeforeupdate Trigger End ****'); 

}