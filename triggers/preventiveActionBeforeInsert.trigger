//*********************************************************************************
// Trigger Name     : preventiveActionBeforeInsert 
// Purpose          : Preventive Action  Before Insert event trigger
// Created by       : Global Delivery Team
// Date created     : 29th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/


trigger preventiveActionBeforeInsert on PreventiveAction__c (before Insert) {
    System.Debug('****** preventiveActionBeforeInsert Trigger Start ****');
    
    List <PreventiveAction__c> preventiveActionWithoutOwnerList = new List <PreventiveAction__c>();
    
    if(Utils_SDF_Methodology.canTrigger('AP1004')){
        if(Trigger.new.size()>0){
            for(PreventiveAction__c prvtveActn:Trigger.new){
                prvtveActn.LastUpdated__c = System.now();
                prvtveActn.LastUpdatedby__c = UserInfo.getUserId();
                if(prvtveActn.Owner__c == null){
                    preventiveActionWithoutOwnerList.add(prvtveActn);
                }
            }
            AP1004_PreventiveAction.populatePreventiveActionOwner(preventiveActionWithoutOwnerList);
        }
    }      
    System.Debug('****** preventiveActionBeforeInsert Trigger End ****');  
}