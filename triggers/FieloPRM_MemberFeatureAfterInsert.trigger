/********************************************************************
* Company: Fielo
* Created Date: 01/11/2015
* Description:
********************************************************************/
trigger FieloPRM_MemberFeatureAfterInsert on FieloPRM_MemberFeature__c (After Insert) {
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberFeatureAfterInsert')){
        FieloPRM_AP_MemberFeatureTriggers.detailCreation();
    }
}