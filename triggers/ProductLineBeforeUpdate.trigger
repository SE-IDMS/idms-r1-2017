/*
    Author: Bhuvana S
    Description: Trigger for Product Line Before Insert
*/

trigger ProductLineBeforeUpdate on OPP_ProductLine__c (before update) 
{
    System.Debug('********* ProductLineBeforeUpdate Trigger Start *********');
    if(Utils_SDF_methodology.canTrigger('AP_ProductLineTrigger')){
        AP_ProductLineBeforeUpdateHandler handler = new AP_ProductLineBeforeUpdateHandler();
        handler.OnBeforeUpdate(Trigger.new,Trigger.newMap, TRigger.old, trigger.oldMap);    }
    System.Debug('********* ProductLineBeforeUpdate Trigger End *********');
}