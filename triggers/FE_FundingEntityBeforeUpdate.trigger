/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 03/08/2012
    Description     : DMT Funding Entity Before Update event trigger.
*/
trigger FE_FundingEntityBeforeUpdate on DMTFundingEntity__c (before update) 
{
    System.Debug('****** FE_FundingEntityBeforeUpdate Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('FundingEntityTriggerUpdate'))
    {
        System.Debug('Value New Update: ' + Trigger.new);
        System.Debug('Value NewMap Update: ' + Trigger.newMap);
        
        FE_FundingEntityTriggers.fundingentityBeforeUpdate(Trigger.newMap, Trigger.oldMap);

    }
    System.Debug('****** FE_FundingEntityBeforeUpdate Trigger End ****'); 
}