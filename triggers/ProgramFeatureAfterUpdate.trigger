/**********************************************************************************

Created By : Shruti Karn
Description : For PRM May 13 Release
                To activate the country feature when global feature is updated.
Updated By : Shruti Karn
Description : For July 13 REleas .BR# 3077
**********************************************************************************/
trigger ProgramFeatureAfterUpdate on ProgramFeature__c (after update) {

   
    if(Utils_SDF_Methodology.canTrigger('AP_PRF_CountryFeatureUpdate') || test.isRunningTest())
    {
        map<ID,list<ProgramFeature__c>> mapGlobalLevelFeature = new map<ID,list<ProgramFeature__c>>();
        list<ID> lstFtrProgramID = new list<ID>();
        map<ID,list<ProgramFeature__c>> mapCountryLevelAccountFeature = new map<ID,list<ProgramFeature__c>>();
        Boolean isContactFeature = false;
        
        //Srinivas July 13 BR-3082
        list<ProgramFeature__c> lstFeature = new list<ProgramFeature__c>();

        for(Id featureID : trigger.newMap.keySet())
        {
            if(trigger.newMap.get(featureID).recordtypeid == Label.CLMAY13PRM06 && trigger.newMap.get(featureID).featurestatus__c != trigger.oldMap.get(featureID).featurestatus__c ) 
            {
                if(trigger.newMap.get(featureID).featurestatus__c == Label.CLMAY13PRM09)
                {
                    lstFtrProgramID.add(trigger.newMap.get(featureID).PartnerProgram__c);//Global Partner Program
                    if(!mapGlobalLevelFeature.containsKey(trigger.newMap.get(featureID).ProgramLevel__c))
                        mapGlobalLevelFeature.put(trigger.newMap.get(featureID).ProgramLevel__c, new list<ProgramFeature__c> {(trigger.newMap.get(featureID))});
                    else
                        mapGlobalLevelFeature.get(trigger.newMap.get(featureID).ProgramLevel__c).add(trigger.newMap.get(featureID));
                }
                
                
            }
            
            if(trigger.newMap.get(featureID).recordtypeid == Label.CLMAY13PRM04 && trigger.newMap.get(featureID).featurestatus__c != trigger.oldMap.get(featureID).featurestatus__c && trigger.newMap.get(featureID).featurestatus__c == Label.CLMAY13PRM09) 
            {
                //lstFtrProgramID.add(trigger.newMap.get(featureID).PartnerProgram__c);//Global Partner Program
                if(!mapCountryLevelAccountFeature.containsKey(trigger.newMap.get(featureID).ProgramLevel__c))
                    mapCountryLevelAccountFeature.put(trigger.newMap.get(featureID).ProgramLevel__c, new list<ProgramFeature__c> {(trigger.newMap.get(featureID))});
                else
                    mapCountryLevelAccountFeature.get(trigger.newMap.get(featureID).ProgramLevel__c).add(trigger.newMap.get(featureID));
               if(trigger.newMap.get(featureID).Enabled__c == Label.CLMAY13PRM11 || trigger.newMap.get(featureID).Visibility__c == Label.CLMAY13PRM11)
               {
                   isContactFeature = true;
               }
            }
            
            //Srinivas Nallapati    July 13 
            if(trigger.newMap.get(featureID).featurestatus__c != trigger.oldMap.get(featureID).featurestatus__c && trigger.newMap.get(featureID).featurestatus__c == 'Inactive')
            {
                lstFeature.add(trigger.newMap.get(featureID));
            }
            
        }
        if(!mapGlobalLevelFeature.isEmpty())
            AP_PRF_CountryFeatureUpdate.creatCountryFeature(mapGlobalLevelFeature , lstFtrProgramID , trigger.newMap.keySet());
        AP_PRF_CountryFeatureUpdate.updateCountryFeature(trigger.newMap , trigger.oldMap );
        
       if(!mapCountryLevelAccountFeature.isEmpty())
            AP_PRF_CountryFeatureUpdate.createActivateAccConFeature(mapCountryLevelAccountFeature , isContactFeature );
        //July 13 Srinivas   BR-3082 // moved to batch for inactivation
        if(!lstFeature.isEmpty())
        {   
             //AP_PRF_CountryFeatureUpdate.updateAccountContactFeatures(lstFeature );
             //AP_ProcessFeatureAssignment.
            }   
       

    }
}