/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 23-March-2013
    Description     : Connect Champions After Update Trigger
*/

trigger ConnectChampionAfterUpdate on Champions__c (after update) {

  System.Debug('****** CountryChampionAfterUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('ConnectChampionAfterUpdate'))
    {
        ConnectChampionsTriggers.ConnectChampionsAfterUpdate(trigger.old,trigger.new);
       
 }
 System.Debug('****** ConnectChampionAfterUpdate Trigger End ****'); 
}