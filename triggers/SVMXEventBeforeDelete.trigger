/*
@Author: Anita
Created Date: 2-08-2011
Description: This trigger fires on Event on Before Delete
**********
Scenarios:
**********
1.  On Delete of Work Order Event, delete Assigned Tools & Technician record.

Date Modified       Modified By         Comments
------------------------------------------------------------------------
02-08-2011          Anita D'Souza       Added Logic for Second scenario
*/

trigger SVMXEventBeforeDelete on SVMXC__SVMX_Event__c (before delete) 
{
    //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX04')){
    
    List<AssignedToolsTechnicians__c> TechToolList = new List<AssignedToolsTechnicians__c>();
    List<String> EventIDList = new List<String>();    
    List<SVMXC__SVMX_Event__c> EventList = new List<SVMXC__SVMX_Event__c>();   
    set<id> techid = new set<id>();
    
    for (SVMXC__SVMX_Event__c sevent : Trigger.old)    
    {        
        if(sevent.Name.startswith('WO-')) 
        {        
           eventIdList.add(sevent.Id);            
        }
        if(sevent.SVMXC__Technician__c != null){
            techid.add(sevent.SVMXC__Technician__c);
        }
    }    
    if(eventIdList.size()>0)      
    {                    
         TechToolList = [Select ID from AssignedToolsTechnicians__c where EventId__c =:eventIdList];
    }
    if(techid.size()>0)
    {
        List<SVMXC__Dispatcher_Access__c> DispAccs= new List<SVMXC__Dispatcher_Access__c>();
        List<SVMXC__Service_Group_Members__c> Techlist= new List<SVMXC__Service_Group_Members__c>();
        set<id> STid = new set<id>();
         Techlist=[select id,SVMXC__Service_Group__c from SVMXC__Service_Group_Members__c where id in:techid];
         for(SVMXC__Service_Group_Members__c st:Techlist){
            STid.add(st.SVMXC__Service_Group__c);
         }
         DispAccs=[select id from SVMXC__Dispatcher_Access__c where SVMXC__Dispatcher__c=:UserInfo.getUserId() and SVMXC__Service_Team__c in:STid and Read_Only__c=true];
         if(DispAccs.size()>0 && DispAccs!= null)
         {
            trigger.old[0].addError('Sorry!! You only have read-only access.');
         }      
    }
    
    System.debug('TechToolList ='+TechToolList );
           
    //if(TechToolList.size()>0) Delete TechToolList;
        //Database.Delete(TechToolList);
        
    }
}