/*
    Author          : Ramakrishna Singara (Schneider Electric)
    Date Created    : 09/10/2012
    Description     : Project Request IPO Program default Other for October Release                      
                      
*/
trigger PRJ_ProjReqBeforeInsert on PRJ_ProjectReq__c (before insert) {

    if(Utils_SDF_Methodology.canTrigger('ProjectRequestTriggerBeforeInsert'))
    {
        PRJ_ProjectTriggers.projectBeforeInsert(Trigger.new);
    }


}