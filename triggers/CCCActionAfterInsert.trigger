//*********************************************************************************
// Trigger Name     : CCCActionAfterInsert
// Purpose          : CCCAction After Insert event trigger
// Created by       : Global Delivery Team
// Date created     : 15th June 2012
// Modified by      :
// Date Modified    :
// Remarks          : For Sep - 12 Release
///********************************************************************************/

trigger CCCActionAfterInsert on CCCAction__c (after Insert) {
    System.Debug('****** CCCActionAfterInsert  Trigger Start****');
    
    if(Utils_SDF_Methodology.canTrigger('AP_ChatterForCCCAction')){
        AP_ChatterForCCCAction.OwnerFollowTheCCCAction(Trigger.newMap);
        AP_ChatterForCCCAction.CreateFeedOnDueDate(Trigger.newMap,Trigger.oldMap);
        //AP_ChatterForCCCAction.CreateFeedOnActionClosure(Trigger.newMap,Trigger.oldMap);
    }
    /********************************************************************************************************************
        Modified By : Vimal Karunakaran
        Description : For Oct 14  CCC Release:
                     1. Calls AP_Case_CaseHandler.updateLastActivityDate to update the Last Activity Date from CaseRelated List
    ********************************************************************************************************************/
    if(Utils_SDF_Methodology.canTrigger('AP_Case_CaseHandler') || Test.isRunningTest()){
        String strCaseKeyPrefix = SObjectType.Case.getKeyPrefix(); 
        Set<Id> setCaseIds = new Set<Id>();
        for(CCCAction__c objCCCAction: Trigger.new){
            if(objCCCAction.Case__c!=null){
                setCaseIds.add(objCCCAction.Case__c);
            }
        }
        if(setCaseIds.size()>0){
            AP_Case_CaseHandler.updateLastActivityDate(setCaseIds);
        }
    }

    
    System.Debug('****** CCCActionAfterInsert  Trigger Finished****');
}