/*
    Author        : Srikant Joshi 
    Date Created  : 30/07/2013
    Description   : Complaints/Request before Update even trigger.
    
*/
trigger ComplaintsBeforeUpdate on ComplaintRequest__c(Before Update) {
if(Utils_SDF_Methodology.canTrigger('AP_LCR'))
   {
    //Added by October 2015 release.
  List<ComplaintRequest__c> listRepOrgAccOrgDepts = new List<ComplaintRequest__c>{};
   List<ComplaintRequest__c> lstCRDueDate = new List<ComplaintRequest__c>{};  //Dec 2016 Release BR-10560, added by Ram Chilukuri
     
     for(ComplaintRequest__c tempCR : Trigger.New)
     {    
                tempCR.LastUpdated__c = System.now();
                tempCR.LastUpdatedby__c = UserInfo.getUserId();
         if((tempCR.TECH_EventType__c==null || tempCR.TECH_EventType__c=='')&&(tempCR.ReportingEntity__c!= Trigger.oldmap.get(tempCR.Id).ReportingEntity__c || Trigger.oldmap.get(tempCR.Id).ReportingDepartment__c!=tempCR.ReportingDepartment__c))
         {
             listRepOrgAccOrgDepts.add(tempCR); 
         } 
        if(Trigger.oldMap.get(tempCR.id).AccountableOrganization__c!= Trigger.newMap.get(tempCR.id).AccountableOrganization__c || Trigger.oldMap.get(tempCR.id).Department__c!= Trigger.newMap.get(tempCR.id).Department__c || Trigger.oldMap.get(tempCR.id).TECH_DueDate__c!= Trigger.newMap.get(tempCR.id).TECH_DueDate__c){//Dec 2016 Release BR-10560, added by Ram Chilukuri
                lstCRDueDate.add(tempCR);    
            }        
     }
     if(listRepOrgAccOrgDepts.size()>0){
    AP_LCR.updateAccountableOrgForInternalCR(listRepOrgAccOrgDepts);
    }
    //Ended
    if(lstCRDueDate.size()>0){//Dec 2016 Release BR-10560, added by Ram Chilukuri
            AP_LCR.dueDateSetforNogications(lstCRDueDate);
        }
     List<ComplaintRequest__c> listAccountChanged = new List<ComplaintRequest__c>{};
     
     List<ComplaintRequest__c> listStatusChangedToClosed = new List<ComplaintRequest__c>{};
     
     
     for(ComplaintRequest__c compReq : Trigger.New)
     {    
         //if(compReq.AccountableOrganization__c != Trigger.oldmap.get(compReq.Id).AccountableOrganization__c)
        // {
             compReq.TECH_Accountable_org__c = true;
             listAccountChanged.add(compReq); 
         //}
         if(compReq.Status__c!= Trigger.oldmap.get(compReq.Id).Status__c)
         {
             listStatusChangedToClosed.add(compReq); 
         }  
         
         
     }
     
        
     
    
    // AP_LCR.updateCasefieldsBasedOnCR(trigger.new);
    
    // April 2015 release change
    
    AP_LCR.UpdateInvolvedOrganizations(Trigger.newmap, Trigger.oldmap); // concatenate involved organizations
     //AP_LCR.PopulateAnswerToSubEntityLocation(Trigger.newmap); // Populates Answer organization detail
//2015 April release 29-Apr-2015 START
     List<ComplaintRequest__c> listForceManualCRTrue = new List<ComplaintRequest__c>{};
     
     for(ComplaintRequest__c compReqForce : Trigger.New)
     {    
        
         if(compReqForce.ForceManualSelection__c!= Trigger.oldmap.get(compReqForce.Id).ForceManualSelection__c || Trigger.oldmap.get(compReqForce.Id).ForceManualSelection__c==True && compReqForce.AccountableOrganization__c!= Trigger.oldmap.get(compReqForce.Id).AccountableOrganization__c)
         {
             listForceManualCRTrue.add(compReqForce); 
         }   
     }
     if(listForceManualCRTrue.size()>0){
    AP_LCR.updateFieldOforganization(listAccountChanged);
    }
//2015 April release 29-Apr-2015 END
     // April 2014 Release - sukumar - 07Feb2014 ,Release Oct2014 added trigger.old
     AP_LCR.populatePlant(trigger.new,trigger.old);
     // October 2015 Release - Gayathri Shivakumar
     AP_LCR.ValidateFinalResolutionUpdate(trigger.newmap,trigger.oldmap);
     
    // AP_LCR.checkIfAllCRAreClosed(listStatusChangedToClosed)  ; 
    
    // April 2016 RELEASE BR-9614 : Swapnil Saurav
    List<ComplaintRequest__c> listCatReasonChanged = new List<ComplaintRequest__c>();
    
    for(ComplaintRequest__c tempCR : Trigger.New) {
        
        if(tempCR.NumberOfEscalations__c == 0 && (tempCR.Category__c != Trigger.oldmap.get(tempCR.Id).Category__c || tempCR.Reason__c != Trigger.oldmap.get(tempCR.Id).Reason__c)) {
            listCatReasonChanged.add(tempCR);
        }
    }
    if(listCatReasonChanged.size() > 0) {
        AP_LCR.updateInternalCategoryAndInternalReason(listCatReasonChanged);
    }
    
    // April 2016 RELEASE BR-9614 END 
    
    // Q3 16 Release - Swapnil : Fix for CR Family issue - BEGIN
    List<ComplaintRequest__c> updateCRFamilyList = new List<ComplaintRequest__c>();
    Set<String> setFamilyGMRCode = new Set<String> ();
    for(ComplaintRequest__c temprecord:Trigger.new) {
    if((temprecord.TECH_CROrderGMRCode__c != null && temprecord.FamilyOrdered__c != null && temprecord.FamilyOrdered_lk__c == null) || (temprecord.TECH_CROrderGMRCode__c != null && temprecord.FamilyOrdered__c != Trigger.oldmap.get(temprecord.id).FamilyOrdered__c && temprecord.CommercialReferenceOrdered__c == null) || (temprecord.TECH_CRReceivedGMRCode__c != null && temprecord.FamilyReceived__c != null && temprecord.FamilyReceived_lk__c == null) || (temprecord.TECH_CRReceivedGMRCode__c != null && temprecord.FamilyReceived__c != Trigger.oldmap.get(temprecord.id).FamilyReceived__c && temprecord.CommercialReferenceReceived__c == null)) {
            updateCRFamilyList.add(temprecord);
            if(temprecord.TECH_CROrderGMRCode__c != null) {
                setFamilyGMRCode.add(temprecord.TECH_CROrderGMRCode__c);
            }
            if(temprecord.TECH_CRReceivedGMRCode__c != null) {
                setFamilyGMRCode.add(temprecord.TECH_CRReceivedGMRCode__c);
            }
        }
    }
    if(updateCRFamilyList.size() > 0 && setFamilyGMRCode.size() > 0) {
        AP_LCR.updateCRFamily(updateCRFamilyList,setFamilyGMRCode);
    }
    
    // Q3 16 Release - Swapnil : Fix for CR Family issue - END
    
    for(ComplaintRequest__c temprecord:Trigger.new)
            {
                if(temprecord.comments__c == null || temprecord.comments__c == '<br>')
                { 
                      //temprecord.comments__c.addError('Please select comment');
                      temprecord.TECH_IsBlankComment__c=true;
                 
                }
                else {
                    temprecord.TECH_IsBlankComment__c=false;
                }
            }
   
}  


 
}