trigger AccountPlanAfterUpdate on SFE_AccPlan__c (after update) 
{
    //Function to share all Account Plan Confidential children records with the User in Account Plan "Account Plan Owner" field 
    AP_AccountPlanConfidential_RecordShare.shareAPCRecordWithAccountPlanOwner(Trigger.new);    
   
}