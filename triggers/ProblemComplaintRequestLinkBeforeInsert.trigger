trigger ProblemComplaintRequestLinkBeforeInsert on ProblemComplaintRequestLink__c (before insert) {

    if(Utils_SDF_Methodology.canTrigger('AP_ProblemComplaintRequestLink')) {
    
        // Added by Uttara - BR-4674 (Case-Prb link)
        List<String> CRIds = new List<String>();
        
        for(ProblemComplaintRequestLink__c pcrlink : Trigger.new) {
            
            CRIds.add(pcrlink.ComplaintRequest__c);        
        }
        AP_ProblemComplaintRequestLink.CasePrbLink(Trigger.New, CRIds);   
    }
}