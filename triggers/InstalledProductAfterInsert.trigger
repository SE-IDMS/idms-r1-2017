/*
Srinivas Nallapati - 30-Sep-2013    create child Accountrole for new InstalledProduct
*/
trigger InstalledProductAfterInsert on SVMXC__Installed_Product__c (after insert) {
     //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX22'))
    {
     set<id>set2= new set<id>();
    List<Role__c> lstSerRole = new List<Role__c>();
    Map<String,List<SVMXC__Installed_Product__c>> mapInstalledProductsWithMissingProductInfo=new Map<String,List<SVMXC__Installed_Product__c>>();
   
  
   if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLNOV13SRV03)){
   /*
    try{
    RecordType   rt =[SELECT DeveloperName,Id,SobjectType FROM RecordType  where SobjectType  ='Role__c' and DeveloperName ='InstalledProductAccountRole' limit 1];
    */ //Commented for BR-8304
    for(SVMXC__Installed_Product__c ip : trigger.new)
    {
        /*if(ip.SVMXC__Product__c != null && ip.TECH_CreatedfromSFM__c == false)
        {
            Role__c r = new Role__c(InstalledProduct__c = ip.id, Account__c = ip.SVMXC__Company__c, RecordTypeId = rt.id , Role__c = System.Label.CLOCT13SRV51 );
            lstSerRole.add(r);
        }*///Commented for BR-8304
        /*
        if(ip.SVMXC__Parent__c!=null)
        {
            set2.add(ip.SVMXC__Parent__c);
        }
        */ 
        
        //just for the sake of naming convention
        if(System.LABEL.CLNOV13SRV10=='FOR_SDH_REGULAR_UPDATES' || Test.isRunningTest())
        {           
            SVMXC__Installed_Product__c assetRecord=ip;
    
            if(assetRecord.SVMXC__Product__c==null && assetRecord.Brand2__c!=null && assetRecord.DeviceType2__c!=null && assetRecord.Category__c!=null)
            {
                String concatenatedString=assetRecord.TECH_SDHCategoryId__c+'_'+assetRecord.TECH_SDHDEVICETYPEID__c+'_'+assetRecord.TECH_SDHBRANDID__c;
                if(mapInstalledProductsWithMissingProductInfo.containsKey(concatenatedString))
                {
                    mapInstalledProductsWithMissingProductInfo.get(concatenatedString).add(assetRecord);
                }
                else
                {            
                    mapInstalledProductsWithMissingProductInfo.put(concatenatedString,new List<SVMXC__Installed_Product__c>{assetRecord});
                }
            }
        }
        
        
    
    }//End of for
    /*
    if(set2!=null && set2.size()>0)
    {
        Ap_Installed_Product.IBchangeMaster(set2);
    }
    */
    /*   
    if(!lstSerRole.isEmpty())
        insert lstSerRole;
    */
    //Commented for BR-8304
    if(mapInstalledProductsWithMissingProductInfo.size()>0){
        List<SVMXC__Installed_Product__c> lstofAssetsTobeUpdated=AP_InstalledProductHandler.calculateProductInfo(mapInstalledProductsWithMissingProductInfo);
        if(lstofAssetsTobeUpdated.size()>0)
        {
            Database.update(lstofAssetsTobeUpdated);
        }
    }    
        
   /* }

    
     catch(Exception ex){
     }*/
 }// for standalone users
 /*
   Product Warranty Creation From IP(IP-->ApplicableProduct-->WarrantyTerm-->ProductWarranty)
   for April 2015
   By-Deepak
 */
 List<SVMXC__Installed_Product__c> IPlist = new List<SVMXC__Installed_Product__c>();
 
    for(SVMXC__Installed_Product__c ip : trigger.new)
    {
        if(ip.Brand2__c !=null && ip.Category__c !=null && ip.DeviceType2__c !=null &&
            ip.SVMXC__Warranty_Start_Date__c ==null && ip.SVMXC__Warranty_End_Date__c==null && ip.WarrantyTriggerDate__c !=null && 
            ip.IsreplacementIP__c==false){
            
            IPlist.add(ip);
        }
    }
    if(IPlist.size()>0){
         if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLNOV13SRV03)){
        	Ap_Installed_Product.ProductWarrantyCreation(IPlist);
         }
    
    }
  }
 
}//End of trigger