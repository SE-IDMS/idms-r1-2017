//*********************************************************************************
// Trigger Name     : ProblemBeforeDelete
// Purpose          : Problem  Before Delete event trigger
// Created by       : Global Delivery Team
// Date created     : 11th October 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/
trigger ProblemBeforeDelete on Problem__c (before delete) {
    System.Debug('****** ProblemBeforeDelete Trigger Start ****');

    if(Utils_SDF_Methodology.canTrigger('AP1000')){
        if(Trigger.old.size()>0){
            for(Problem__c prblm: Trigger.old){
                if(UserInfo.getProfileId()!=Label.CL00110)
                prblm.addError(Label.CL10073);
            }
        }
    }      
    System.Debug('****** ProblemBeforeDelete Trigger End  ****');  
}