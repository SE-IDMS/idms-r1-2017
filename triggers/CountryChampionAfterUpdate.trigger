/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 23-March-2013
    Description     : Country Champions After Update Trigger
*/

trigger CountryChampionAfterUpdate on Country_Champions__c (after update) {

  System.Debug('****** CountryChampionAfterUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CountryChampionAfterUpdate'))
    {
        ConnectCountryChampionsTriggers.ConnectCountryChampionsAfterUpdate(trigger.old,trigger.new);
       
 }
 System.Debug('****** CountryChampionAfterUpdate Trigger End ****'); 
}