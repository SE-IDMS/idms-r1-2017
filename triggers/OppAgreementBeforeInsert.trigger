/*
    Author:Bhuvana Subramaniyan    
    Date: 08/03/2011
    Description:Agreement Before Insert Trigger.   
*/
trigger OppAgreementBeforeInsert on OppAgreement__c (before Insert) 
{    
    System.Debug('****** OppAgreementBeforeInsert Trigger Start ****'); 
    
    if(Utils_SDF_Methodology.canTrigger('AP04'))
    {
        List<OppAgreement__c> oppAgrs = new List<OppAgreement__c>();
        for(OppAgreement__c oppAgr: Trigger.new)
        {
            if(oppAgr.AccountName__c!=null)
                oppAgrs.add(oppAgr);
        }
        if(oppAgrs.size()>0)
        {
            AP04_AgreementTrigger.populateAccountOwnerEmail(oppAgrs);
        } 
    }   
    System.Debug('****** OppAgreementBeforeInsert Trigger End****');
}