/*     
@Author: Deepak
Created Date: 17-12-2013
Description: This will show error message "You have already added this installed product to the contract"
**********

*/

trigger AIPBeforeInsert on AssociatedInstalledProduct__c (before insert) 
{  
    List<AssociatedInstalledProduct__c> aipList1 = New List<AssociatedInstalledProduct__c>();
    for(AssociatedInstalledProduct__c aip: trigger.new)
    {   
        if(aip.InstalledProduct__c !=null && aip.ServiceMaintenanceContract__c !=null)
        {
            aipList1.add(aip);
        }
    }
    if(aipList1 != null && aipList1.size()>0)
    {
        AP_AssociatedInstalledProduct.AddError(aipList1);
    }
}