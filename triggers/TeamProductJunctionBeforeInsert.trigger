/*
    Author          : Rakhi Kumar 
    Date Created    : 19/06/2013
    Description     : TeamProductJunctionBeforeInsert before insert event trigger.
    ------------------------------
    Modified By     : <Author>
    Modified Date   : <Date>
    Description     : <Description>
    -----------------------------------------------------------------   
*/
trigger TeamProductJunctionBeforeInsert on TeamProductJunction__c (before Insert) {
    System.debug('Trigger.TeamProductJunctionBeforeInsert.INFO - Trigger called.');
    if(Utils_SDF_Methodology.canTrigger('AP_TeamProductMappingMethods')) {
        AP_TeamProductMappingMethods.uncheckOtherDefaultProductFamilies(trigger.new); 
    }
    System.debug('Trigger.TeamProductJunctionBeforeInsert.INFO - End of trigger.');
}