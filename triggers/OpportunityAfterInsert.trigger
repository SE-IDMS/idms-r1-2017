/*    
    Author: Pooja Bhute
    Date: 03/02/2011
    Description: Opportunity After Insert event trigger.  
*/

trigger OpportunityAfterInsert on Opportunity (after Insert) {    

    System.Debug('****** OpportunityAfterInsert Trigger Start ****');  
    
    
    if(Utils_SDF_Methodology.canTrigger('AP05'))
    {
        /*On creation of every productline for any Frame Agreement, they can be
        copied across to its Associated Opportunity */
        List<Opportunity> oppor = new List<Opportunity>();        
        List<Opportunity> opps = new List<Opportunity>();
        Map<Opportunity, String> oppScopes = new Map<Opportunity, String>();
        List<OPP_Project__c> prjlist=new List<OPP_Project__c>();
        List<Opportunity> opplist= new list<Opportunity>(); 
         List<Opportunity> masterProjectStatusCheck = new List<Opportunity>();
        for(integer i = 0; i < trigger.new.size(); i++)
        {
            if(trigger.new[i].AgreementReference__c !=null)
            { 
              opps.add(trigger.new[i]);  
            }
            if(trigger.new[i].TECH_CrossProcessConversionAmount__c!=null)
            {
                oppScopes.put(trigger.new[i], trigger.new[i].OpportunityScope__c);
            }
            if(!trigger.new[i].name.contains(label.CL00651))
            {
                oppor.add(trigger.new[i]);    
            }
           /* March 2016 release - BR-8430
           Prevent changes on Completed Master Project
           Prevent User linking the Opportunity to the hierarchy if the Master Project is completed */ 
           if(trigger.new[i].Project__c!=null)
               masterProjectStatusCheck.add(trigger.new[i]);
           if(trigger.new[i].ParentOpportunity__c !=null)
               masterProjectStatusCheck.add(trigger.new[i]);
           /* In order to considerthe entire hierarchy linked to Master Project
            we are considering Tech_MasterProjectId__c instead of project__c. project__c gets only
            Opportunities directly linked to master project but the Tech_MasterProjectId__c field will
            get all the opportunities in the entire hierarchy under Master Project*/
            if(trigger.new[i].Tech_MasterProjectId__c!=null && trigger.new[i].Tech_MasterProjectId__c!='' && trigger.new[i].Amount!=null && trigger.new[i].Amount!=0 )
            { 
              prjlist.add(new OPP_Project__c(Id=trigger.new[i].Tech_MasterProjectId__c));
              opplist.add(trigger.new[i]);
            }
        }
         if(!(masterProjectStatusCheck.isEmpty()))
            AP01_OpportunityFieldUpdate.masterProjectStatusCheck(masterProjectStatusCheck);
        //Update Amount fields of the Master Project if the Opportunity is linked to the Master Project - Monalisa Project oct 2015
        if(prjlist.size()>0)
            AP05_OpportunityTrigger.updateProjectAmountFields(prjlist,opplist);
        
        if(opps.size() !=0 && WS02_SetConstantValue.iscreateseries == false )
        {
            //AP05_OpportunityTrigger.addAgreementProdLineToOppts(opps);            
        }
        //Inserts an Opportunity line for the corresponding opportunity with default values whose TECH_CrossProcessConversionAmount__c field is filled
        if(!(oppScopes.isEmpty()))
            AP05_OpportunityTrigger.creteOppLine(oppScopes);
         // This trigger adds the contributor to the Sales Contributor object on creation of a new Opportunity.
        
        
        /*if(oppor.size()>0)
        {
            AP05_OpportunityTrigger.addContributorToSalesContributor(oppor);
        }*/

        
        // This trigger default Opp Detector to Current User if the Opp Detector has not been captured
     /*
        if(trigger.new.size()>0)
        {
            //AP05_OpportunityTrigger.DefaultOppDetector(trigger.new);
            AP44_updateOpportunityAccountChallenges.updateOpportunityAccountChallenges (trigger.new);
        }
        
     */ 
    }
    if(Utils_SDF_Methodology.canTrigger('AP_Opp_UpdateParentCloseDate'))
    {
          AP_Opp_UpdateParentCloseDate.checkForChildrenWithGreaterCloseDate(Trigger.NewMap);
          AP_Opp_UpdateParentCloseDate.updateParentOpportunity(Trigger.NewMap);
    }

    if(Utils_SDF_Methodology.canTrigger('PRM_FieldChangeListener')) {
      System.debug('OpportunityAfter Insert');
      PRM_FieldChangeListener prmChangeListener = new PRM_FieldChangeListener();
      prmChangeListener.badgeInspecter(null, trigger.newMap, PRM_FieldChangeListener.OPPORTUNITY_OBJECT_NAME);
    }

    System.Debug('****** OpportunityAfterInsert Trigger End ****');  
}