/*
    Author          : Yannick Tisserand (Accenture) 
    Date Created    : 22/02/2012
    Description     : Commercial Action Plan After Insert event trigger.
*/
trigger CAPAfterInsert on SFE_IndivCAP__c (after insert) {
  List<SFE_IndivCAP__Share> sharesToCreate = new List<SFE_IndivCAP__Share>();
  List<ID> userIds = new List<ID>();
  Map<ID, User> partnerMap = new Map<ID, User>();

  // Collect all the user ids
  for (SFE_IndivCAP__c cap: Trigger.new) { 
    userIds.add(cap.AssignedTo__c); 
  }
  
  // Retrieve all the users in one query
  List<User> users = [select id, userType from User where id in :userIds];
  
  // Identify partner users and save it in a map
  for(User user: users){
  
    if(user.userType == 'PowerPartner'){
      partnerMap.put(user.id, user); 
    } 
  }
  
  // Loop in Caps 
  for (SFE_IndivCAP__c cap: Trigger.new) { 
  
    // If the new assignee is a partner, need to create a new sharing rule
    if(partnerMap.containskey(cap.AssignedTo__c)){
      SFE_IndivCAP__Share sharingRule = new SFE_IndivCAP__Share();
      sharingRule.AccessLevel = 'Read';
      sharingRule.ParentId = cap.id;
      sharingRule.UserOrGroupId = cap.AssignedTo__c;
      sharesToCreate.add(sharingRule);
    }
  }
  
  // Create all the new sharing rules in one query
  if(!sharesToCreate.isEmpty()){
    insert sharesToCreate;
  }
}