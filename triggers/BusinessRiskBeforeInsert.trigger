/*     Author: Anuj Maheshwari  
         Date Of Creation: 13/04/2011   
         Description : Business Risk Before Insert event trigger
         Date of Update: 06/09.2011
         Description: Update Problem when Problem is attached
  */
    trigger BusinessRiskBeforeInsert on BusinessRiskEscalations__c (before Insert) {
          System.Debug('****** BusinessRiskBeforeInsert Trigger Start ****');
           AP09_BusinessRiskEscalation.updateInvOrgNamesOnBre(Trigger.new);                
           if(Utils_SDF_Methodology.canTrigger('AP09')){
                if(Trigger.new.size()>0){
                   AP09_BusinessRiskEscalation.populateSponsorAndResolutionLeader(Trigger.new);                   
                   AP09_BusinessRiskEscalation.insertBRELinkOnRelatedProblems(Trigger.new);
                   
                   
                }
                //======= shivdeep  - April2015RELEASE - START =======================================================
                for(BusinessRiskEscalations__c bRisk : Trigger.new){
                    system.debug('actual user going to get udate ');
                    bRisk.Actual_User_LastmodifiedDate__c = System.now();
                    bRisk.Actual_User_Lastmodifiedby__c = UserInfo.getUserId();
                    //OCT 15 Release -- BR-7749 -- Divya M 
                    if(bRisk.OriginatingOrganisation__c != null){
                        AP09_BusinessRiskEscalation.checkBREOrginatingOrgs(Trigger.new);    
                    }               
                    //Ends OCT 15 rel
                }
                //======= shivdeep  - April2015RELEASE - START =======================================================
           }                             
      System.Debug('****** BusinessRiskBeforeInsert Trigger End ****');  
   
     }