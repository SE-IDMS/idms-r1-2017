trigger CSIBeforeInsert on CustomerSafetyIssue__c (before insert) 
{ 
if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_CustomerSafetyIssue'))
{
  
    AP_customerSafetyIssue.blntrigger=true;
        AP_customerSafetyIssue.getStakeholder(trigger.new,'New',trigger.newmap,trigger.newmap,label.CLSEP12I2P104  )  ;  
        }
}