trigger ServiceContractAfterDelete on SVMXC__Service_Contract__c (after Delete) {
    list<SVMXC__Service_Contract__c> sclist1 =  New List<SVMXC__Service_Contract__c>();
    set<Id> scid1=New set<Id>();
    set<Id> scid2=New set<Id>();
    set<Id> parentidset=New set<Id>();
    String ServiceContractLineRT = System.Label.CLAPR15SRV02;//Service Line Record Id.
    String ServiceContractConnectedLineRT = System.Label.CLAPR15SRV03;
    
      //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX13')){
    
    for(SVMXC__Service_Contract__c a:trigger.Old){
    
      if(a.RecordTypeId == ServiceContractLineRT || a.RecordTypeId ==ServiceContractConnectedLineRT)
        {
        scid2.add(a.ParentContract__c);
        }
    
        
          if(a.ParentContract__c!=null && (a.RecordTypeId == ServiceContractLineRT || a.RecordTypeId ==ServiceContractConnectedLineRT))
            {
                parentidset.add(a.ParentContract__c);
                scid1.add(a.id);
                sclist1.add(a);
            }
    }
    
      //addedby anand
        if(parentidset.size()>0){ 
            system.debug('testparentsize'+parentidset.size());     
           AP_ServiceContract.SericesContractPriceupdated(parentidset);     
         }
       
      
      //AP_ServiceContract.SericesContractPrice(sclist1,scid1);
      
      if(scid2!=null && scid2.size()>0)
            AP_ServiceContract.LatestDateUpdate(scid2);
    }
}