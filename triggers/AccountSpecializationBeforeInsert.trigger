trigger AccountSpecializationBeforeInsert on AccountSpecialization__c (before insert) {
	
	if(Utils_SDF_Methodology.canTrigger('AP_AccountSpecialization'))
    {
    	set<id> stAccIds = new set<id>();
    	set<id> stSpeIds = new set<id>();

    	for(AccountSpecialization__c asp : trigger.new)
    	{
    		stAccIds.add(asp.Account__c);
    		stSpeIds.add(asp.Specialization__c);
    	}

    	Map<id,Account> mpAccs = new Map<id,Account>([select Country__c from Account Where id in:stAccIds]); 
    	Map<id,Specialization__c> mpSpec = new Map<id,Specialization__c>([select Country__c, TargetCountriesTxt__c,TECH_CountriesId__c, RecordTypeId, RecordType.DeveloperName from Specialization__c WHERE id in:stSpeIds]);

    	for(AccountSpecialization__c asp : trigger.new)
    	{
    		String accCountry = mpAccs.get(asp.Account__c).Country__c;

    		if(!( mpSpec.get(asp.Specialization__c).Country__c ==  accCountry || mpSpec.get(asp.Specialization__c).TECH_CountriesId__c.contains(accCountry) ) )
    			asp.addError('Account Country is not matching with the selected Specialization Country');
    	}
    }	
}