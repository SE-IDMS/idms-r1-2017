trigger IPODeploymentBeforeDelete on IPO_Strategic_Deployment_Network__c (before delete) {

  System.Debug('****** ConnectTeamMembersBeforeDelete Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('IPODeploymentBeforeDelete'))
    {

    IPODeploymentNetworkTriggers.IPODeploymentNetworkBeforeDelete(Trigger.old);
    }
}