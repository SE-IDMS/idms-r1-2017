/*
26-Oct-2012     Srinivas Nallapti       Initial Creation: Dec12 Mkt release 
*/
trigger CampaignBeforeInsert on Campaign (before insert) {
    if(Utils_SDF_Methodology.canTrigger('AP53'))
    { 
        AP53_Campaign AP53 = new AP53_Campaign();
       
        for(Campaign cam : trigger.new)
        {
            if(cam.Global__c)
            {
               //cam.TargetedCountries__c  = AP53.getAllCountryNames();
               cam.TargetedCountries__c  = System.Label.CLDEC12MKT10;
            }   
        }
       
    }//End of canTrigger
}