/*
    Author          : Rakhi Kumar 
    Date Created    : 19/06/2013
    Description     : TeamProductJunctionBeforeUpdate before update event trigger.
    ------------------------------
    Modified By     : <Author>
    Modified Date   : <Date>
    Description     : <Description>
    -----------------------------------------------------------------   
*/
trigger TeamProductJunctionBeforeUpdate on TeamProductJunction__c (before Update) {
    System.debug('Trigger.TeamProductJunctionBeforeUpdate.INFO - Trigger called.');
    if(Utils_SDF_Methodology.canTrigger('AP_TeamProductMappingMethods')) {
        AP_TeamProductMappingMethods.uncheckOtherDefaultProductFamilies(trigger.new); 
    }
    System.debug('Trigger.TeamProductJunctionBeforeUpdate.INFO - End of trigger.');
}