trigger CountryChannelsAfterupdate on CountryChannels__c (after update) {

	if (Utils_SDF_Methodology.canTrigger('AP_CountryChannels_Records')) {
	    Map<Id,CountryChannels__c> conChMap = new Map<Id,CountryChannels__c>();
	    for(CountryChannels__c conCh:trigger.newMap.values()){
	        if(trigger.Oldmap.get(conCh.id).Channel__c != conCh.Channel__c || trigger.Oldmap.get(conCh.id).SubChannel__c != conCh.SubChannel__c || trigger.Oldmap.get(conCh.id).Country__c != conCh.Country__c)
	        conChMap.put(conCh.id,conCh);
	        
	        if(!conChMap.isEmpty())
	        AP_CountryChannels_Records.updateFieldsOnChannelChange(conChMap);
	        
	        System.debug('The Size is'+conChMap.Size());
	    }
	}
}