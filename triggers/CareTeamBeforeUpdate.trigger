trigger CareTeamBeforeUpdate on CustomerCareTeam__c (Before Update) {
    System.Debug('****** CareTeamBeforeUpdate Trigger Start ****');  
    if(Utils_SDF_Methodology.cantrigger('AP_CareTeamBeforeUpdateHandler'))
    {
        List<CustomerCareTeam__c> cteamCheck = new List<CustomerCareTeam__c>();  // List of Inactive CC Teams, if any
        for(CustomerCareTeam__c cteam: trigger.new)
        {
            if(cteam.Inactive__c==True)
                cteamCheck.add(cteam);
        }
        
        if(cteamCheck.size()>0)
        {
            System.Debug('****** CareTeamBeforeUpdate check for Inactive CCTeam Starts ****');
            AP_CareTeamBeforeUpdateHandler handler = new AP_CareTeamBeforeUpdateHandler();
            handler.OnBeforeUpdate(cteamCheck);
            System.Debug('****** CareTeamBeforeUpdate check for Inactive CCTeam Ends ****');
        }
    }
     System.Debug('****** CareTeamBeforeUpdate Trigger Ends ****');
}