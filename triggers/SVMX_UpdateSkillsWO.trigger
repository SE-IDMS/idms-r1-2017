/*
Author:Ramu Veligeti
Date: 30-07-2013
Description: Concatenating the related work order skills__r.SVMXC__Skill__c.Name fields as comma separated for use in DC filtering into the Work Order Skills(text) field on the Work Order object.
*/
trigger SVMX_UpdateSkillsWO on Work_Order_Skills__c (after insert,after update,after delete) 
{
        Set<Id> woId = new Set<Id>();
        list<Work_Order_Skills__c> woskilllist= new  list<Work_Order_Skills__c>();
        list<SVMXC__Service_Order__c> workorderlist= new list<SVMXC__Service_Order__c>();
        if(!trigger.isDelete)
    {
        for(Work_Order_Skills__c ws: trigger.new)
        {
            if(trigger.isInsert && ws.Skill__c!=null) woId.add(ws.Work_Order__c);
            if(trigger.isUpdate && ws.Skill__c!=null && ws.Skill__c != trigger.oldMap.get(ws.Id).Skill__c) woId.add(ws.Work_Order__c);
        }
    }
        
    if(trigger.isDelete)
    {

        for(Work_Order_Skills__c ws: trigger.old)
            {
 
                if(ws.Skill__c!=null) 
                woId.add(ws.Work_Order__c);
            }
            
        if(woId!=null)
                woskilllist= [select id,name from Work_Order_Skills__c where Work_Order__c in:woId];
                if(woskilllist.size()==0) {
               workorderlist=[select id,name,Work_Order_Skills__c from SVMXC__Service_Order__c where id in:woId];
                 for(SVMXC__Service_Order__c wo:workorderlist)
                     {
                        wo.Work_Order_Skills__c=''; 
                     } 
                    update workorderlist;
               }    
    }
    if(woId.size()>0) {
            ServiceMaxUpdateSkillsOnWO.UpdateWOSkills(woId);
            ServiceMaxUpdateSkillsOnWO.UpdateWOSkillrec(woId);
    }
}