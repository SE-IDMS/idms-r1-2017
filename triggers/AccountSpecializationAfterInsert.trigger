/********************************************************************************************************************
    Created By : Srinivas Nallapati / Shruti Karn
    Description : For October 13 PRM Release:
                 1. To add Assessment to Account Specialization.
       
********************************************************************************************************************/
trigger AccountSpecializationAfterInsert on AccountSpecialization__c (after insert) {
    
    if(Utils_SDF_Methodology.canTrigger('AP_AccountSpecialization'))
    {
        AP_AccountSpecialization.createChildAccountSpecializationRequirements(trigger.new);
        map<Id,set<Id>> mapAccountSpecialization = new map<Id,set<Id>>();
        for(AccountSpecialization__c asp : trigger.new)
        {
            if(asp.Specialization__c != null)
            {
                if(!mapAccountSpecialization.containsKey(asp.Specialization__c))
                    mapAccountSpecialization.put(asp.Specialization__c , new set<id> {(asp.Id)});
                else
                    mapAccountSpecialization.get(asp.Specialization__c).add(asp.Id);
            }
        }
        //Shruti
        if(!mapAccountSpecialization.isEmpty())
            AP_AccountSpecialization.addAssessment(mapAccountSpecialization);
    }   
    
}//End of trigger