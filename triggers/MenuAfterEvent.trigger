trigger  MenuAfterEvent on FieloEE__Menu__c (after insert, after update) {
     
    if(Utils_SDF_Methodology.canTrigger('AP_PRMUtils')){
            AP_PRMUtils.CreateDeleteCMSShare(Trigger.new,Trigger.old,'Menu-Share');    
        }
 
}