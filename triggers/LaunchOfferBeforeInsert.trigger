/*****
Hanamanth 
*****/


trigger LaunchOfferBeforeInsert on Offer_Lifecycle__c (before insert) {

    if(Utils_SDF_Methodology.canTrigger('AP_LCR')) {
        if(trigger.isInsert && Trigger.isBefore) {
            AP_ELLAOffer.updateOfferBoxEmail(trigger.new);
        }
    
    }
}