/*
@Author: Anita
Created Date: 2-08-2011
Description: This trigger fires on Event on After Insert
**********
Scenarios:
**********
1.  On Insert of Work Order Event, create a new Assigned Tools & Technician record.

Date Modified       Modified By         Comments
------------------------------------------------------------------------
02-08-2011          Anita D'Souza       Added Logic for Second scenario
06-03-2013          Chris Hurd          Added Logic to Create multiple events based on recurring fields in WO
*/

trigger SVMXEventAfterInsert on SVMXC__SVMX_Event__c (before insert,before update,after insert) 
{
    //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX03')){
        System.debug('************** Log4 >> '+Limits.getQueries());
        if(trigger.isafter){
            List<SVMXC__SVMX_Event__c> EventList = new List<SVMXC__SVMX_Event__c>();
            set<id> evtid = new set<id>();
            for (SVMXC__SVMX_Event__c sevent : Trigger.new)    
            {
                if(sevent.Name.contains('WO-')) 
                {          
                    EventList.add(sevent);
                    evtid.add(sevent.id);
                }
            }                     
            String EventString = JSON.serialize(EventList);
            AP_EventCreate.SVMXCreateAssignedToolsTechnicians(EventString,evtid);
        }
        /*
           // Hari End
            for (SVMXC__SVMX_Event__c sevent : Trigger.new)    
            {
                System.debug('Technician OwnerID ='+sevent);
                AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c();
                
                if(sevent.Name.startswith('WO-')) 
                {          
                    AssignedTech.TechnicianEquipment__c = sevent.SVMXC__Technician__c;                   
                    AssignedTech.WorkOrder__c = sevent.SVMXC__WhatId__c;
                    AssignedTech.EventId__c= sevent.Id;
                    AssignedTech.startDateTime__c= sevent.SVMXC__StartDateTime__c;
                    AssignedTech.EndDateTime__c = sevent.SVMXC__EndDateTime__c;
                    /* 
                    if(woidRecMap.containskey(sevent.SVMXC__WhatId__c))
                    {
                        System.debug('************ asdf '+woidRecMap.get(sevent.SVMXC__WhatId__c).SVMXC__Group_Member__c);
                        System.debug('************ asdf '+sevent.SVMXC__Technician__c);
                        if(woidRecMap.get(sevent.SVMXC__WhatId__c).SVMXC__Group_Member__c == sevent.SVMXC__Technician__c )
                         AssignedTech.PrimaryUser__c = true;
                    }
                    TechToolList.add(AssignedTech);
                }
            }
            
            System.debug('TechToolList='+TechToolList);
            
            if(TechToolList != null)
                DataBase.insert(TechToolList);
         */
        //Issue 2891 Creating recurring events for work order dispatch
        if (trigger.isInsert && Utils_SDF_Methodology.canTrigger('SVMX01_REC_SVMXEvent'))
        {
            Set<Id> woIdSet = new Set<Id>();
            Set<Id> techIdSet = new Set<Id>();
            Set<Id> bhIdSet = new Set<Id>();
            
            for (SVMXC__SVMX_Event__c sEvent : trigger.new)
            {
                if (sEvent.SVMXC__WhatId__c != null)
                {
                    woIdSet.add(sEvent.SVMXC__WhatId__c);
                }
                
                if (sEvent.SVMXC__Technician__c != null)
                {
                    techIdSet.add(sEvent.SVMXC__Technician__c);
                }
            }
            if (woIdSet.size() > 0)
            {
                Map<Id, BusinessHours> bhMap = new Map<Id, BusinessHours>();
                Map<Id, SVMXC__Service_Order__c> woMap = new Map<Id, SVMXC__Service_Order__c>([SELECT Id, RecurrenceEndDate__c, Frequency__c, 
                    FrequencyUnit__c, SkipWeekends__c FROM SVMXC__Service_Order__C WHERE Id IN :woIdSet]);
                Map<Id, SVMXC__Service_Group_Members__c> techMap = new Map<Id, SVMXC__Service_Group_Members__c>([SELECT Id, SVMXC__Working_Hours__c FROM SVMXC__Service_Group_Members__c  WHERE Id IN :techIdSet]);
                //List<SVMXC__Service_Group_Members__c> techList = [SELECT Id, SVMXC__Working_Hours__c FROM SVMXC__Service_Group_Members__c  WHERE Id IN :techIdSet];
                
                for (SVMXC__Service_Group_Members__c tech : techMap.values()){
                    
                    if(tech.SVMXC__Working_Hours__c != null){
                        bhIdSet.add(tech.SVMXC__Working_Hours__c);
                    }
                }
                
                if (bhIdSet.size() > 0){
                    bhMap = new Map<Id, BusinessHours>([SELECT Id, MondayEndTime, MondayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime,
                    ThursdayEndTime, ThursdayStartTime, FridayStartTime, FridayEndTime, SaturdayEndTime, SaturdayStartTime, SundayStartTime, SundayEndTime FROM BusinessHours WHERE Id IN :bhIdSet]);
                }
                
                List<SVMXC__SVMX_Event__c> newEvents = new List<SVMXC__SVMX_Event__c>();
                BusinessHours defaultBusinessHour = [SELECT Id, MondayEndTime, MondayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime,
                    ThursdayEndTime, ThursdayStartTime, FridayStartTime, FridayEndTime, SaturdayEndTime, SaturdayStartTime, SundayStartTime, SundayEndTime FROM BusinessHours WHERE isActive = true AND isDefault = true LIMIT 1];
 
                for (SVMXC__SVMX_Event__c sEvent : trigger.new)
                {
                    if (woMap.containsKey(sEvent.SVMXC__WhatId__c) && woMap.get(sEvent.SVMXC__WhatId__c).RecurrenceEndDate__c != null)
                    {
                        SVMXC__Service_Order__c wo = woMap.get(sEvent.SVMXC__WhatId__c);
                        BusinessHours techBusinessHour = null;
                        
                        if(wo.SkipWeekends__c){
                            SVMXC__Service_Group_Members__c tech = techMap.get(sEvent.SVMXC__Technician__c);
                            
                            if(bhMap.containsKey(tech.SVMXC__Working_Hours__c)){
                                techBusinessHour = bhMap.get(tech.SVMXC__Working_Hours__c);
                            }else{
                                techBusinessHour = defaultBusinessHour;
                            }
                        }
                        
                        system.debug('YTI#####: '+techBusinessHour);
                        
                        
                        Date nextDate = ServiceMaxUtils1.getNextDate(sEvent.SVMXC__StartDateTime__c.date(), wo.Frequency__c, wo.FrequencyUnit__c, techBusinessHour);
                        while (nextDate <= wo.RecurrenceEndDate__c)
                        {
                            SVMXC__SVMX_Event__c nEvent = sEvent.clone(false, false, false, false);
                            nEvent.SVMXC__StartDateTime__c = DateTime.newInstance(nextDate, sEvent.SVMXC__StartDateTime__c.time());
                            nEvent.SVMXC__EndDateTime__c = DateTime.newInstance(nextDate.addDays(sEvent.SVMXC__StartDateTime__c.date().daysBetween(sEvent.SVMXC__EndDateTime__c.date())), sEvent.SVMXC__EndDateTime__c.time());
                            nEvent.SVMXC__ActivityDateTime__c = nEvent.SVMXC__StartDateTime__c;
                            newEvents.add(nEvent);
                            
                            nextDate = ServiceMaxUtils1.getNextDate(nextDate, wo.Frequency__c, wo.FrequencyUnit__c, techBusinessHour);
                        }
                        
                        wo.RecurrenceEndDate__c = null;
                        wo.Frequency__c = null;
                        wo.FrequencyUnit__c = null;
                        wo.SkipWeekends__c = false;
                    }
                }               
                
                if (newEvents.size() > 0)
                {
                    insert newEvents;
                    update woMap.values();
                }
            }
            
        }
        if(trigger.isupdate)
        {
                 List<AssignedToolsTechnicians__c> TechToolList = new List<AssignedToolsTechnicians__c>();
            List<SVMXC__SVMX_Event__c> EventList = new List<SVMXC__SVMX_Event__c>();
             map<id,id> techmap= new map<id,id>();
             set<id> evtid = new set<id>();
            for (SVMXC__SVMX_Event__c sevent : Trigger.new)    
            {
                System.debug('Technician OwnerID ='+sevent);
                if(sevent.Name.contains('WO-') && Trigger.isupdate && sevent.SVMXC__StartDateTime__c!= trigger.oldmap.get(sevent.id).SVMXC__StartDateTime__c) 
                {                
                   // System.debug('Technician OwnerID ='+sevent.SVMXC__Technician__c);
                    eventList.add(sevent);
                    evtid.add(sevent.id);
                }
            } 
            List<AssignedToolsTechnicians__c> techList = new List<AssignedToolsTechnicians__c>();
            if(evtid.size()>0){
                techList=[Select Id,EventId__c 
                    from AssignedToolsTechnicians__c where EventId__c in:evtid ];
                for(AssignedToolsTechnicians__c t:techList){
                    techmap.put(t.EventId__c,t.id);
                }
            }
            String dateTimeFormatTxt = 'dd MMM yyyy HH:mm';
            String dateTimeFormatNonTxt = 'dd/MM/yyyy HH:mm';
            for (SVMXC__SVMX_Event__c sevent : EventList)
            {
                if(techmap.containskey(sevent.id))
                {
                    AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c(id=techmap.get(sevent.id));  
                    AssignedTech.startDateTime__c= sevent.SVMXC__StartDateTime__c;
                    AssignedTech.EndDateTime__c = sevent.SVMXC__EndDateTime__c;
                    AssignedTech.TECH_CustomerStartDateTime__c = sevent.SVMXC__StartDateTime__c.format(dateTimeFormatNonTxt);
                    AssignedTech.TECH_CustomerEndDateTime__c  = sevent.SVMXC__EndDateTime__c.format(dateTimeFormatNonTxt);
                    AssignedTech.TECH_FSRStartDateTime__c = sevent.SVMXC__StartDateTime__c.format(dateTimeFormatTxt);
                    AssignedTech.TECH_FSREndDateTime__c  = sevent.SVMXC__EndDateTime__c.format(dateTimeFormatTxt);
                                
                    TechToolList.add(AssignedTech);
                }          
            }
            if(TechToolList != null)
                DataBase.update(TechToolList);
                
        }
        if(trigger.isbefore){
            set<id> techid = new set<id>();
            for(integer i=0;i<trigger.new.size();i++){
                if(trigger.new[i].SVMXC__Technician__c != null){
                    techid.add(trigger.new[i].SVMXC__Technician__c);
                }
            }
            if(techid.size()>0){
                List<SVMXC__Dispatcher_Access__c> DispAccs= new List<SVMXC__Dispatcher_Access__c>();
                List<SVMXC__Service_Group_Members__c> Techlist= new List<SVMXC__Service_Group_Members__c>();
                set<id> STid = new set<id>();
                 Techlist=[select id,SVMXC__Service_Group__c from SVMXC__Service_Group_Members__c where id in:techid];
                 for(SVMXC__Service_Group_Members__c st:Techlist){
                    STid.add(st.SVMXC__Service_Group__c);
                 }
                 DispAccs=[select id from SVMXC__Dispatcher_Access__c where SVMXC__Dispatcher__c=:UserInfo.getUserId() and SVMXC__Service_Team__c in:STid and Read_Only__c=true];
                 if(DispAccs.size()>0 && DispAccs!= null){
                    trigger.new[0].addError('Sorry!! You only have read-only access.');
                 }      
            }
        }
    }
}