//@Author:- Ashish Sharma SESA340747
//@Date:- 22/09/2014
//@Description:- This trigger is written to share ISR once EU CIS correspondent team is assigned.
//@Events :- Before Insert
trigger CIS_ISRAfterInsert  on CIS_ISR__c (after insert) {
    
    if(Test.isRunningTest() || Utils_SDF_Methodology.canTrigger('AP_CIS_ISRHandler')){
    List<CIS_ISR__c> isrListToShare = new List<CIS_ISR__c>();
    List<CIS_ISR__c> isrListToPopulateQueueAndItsMemberEmails = new List<CIS_ISR__c>();
    for(CIS_ISR__c  isrInstance:trigger.new){
        if(isrInstance.CIS_EUCISCorrespondentsTeam__c!=null){
            isrListToShare.add(isrInstance);
        }   
    }
    if(!isrListToShare.isEmpty()){
        AP_CIS_ISRHandler.shareISRWithPublicGroup(isrListToShare);
    }
    
    List<CIS_ISR__c> sentStatusIsrEmail = new List<CIS_ISR__c>();
    List<CIS_ISR__c> acknowledgedStatusIsrEmail = new List<CIS_ISR__c>();
    List<CIS_ISR__c> reportedStatusISREmil = new List<CIS_ISR__c>();
    List<CIS_ISR__c> reportValidatedStatusISREmil = new List<CIS_ISR__c>();
    List<CIS_ISR__c> toBeClosedStatusISREmil = new List<CIS_ISR__c>();
    for(CIS_ISR__c  isrInstance:trigger.new){
        if((isrInstance.CIS_Status__c =='Sent')&&(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c)){
            sentStatusIsrEmail.add(isrInstance);
        }
        if((isrInstance.CIS_Status__c =='Acknowledged')&&(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c)){
            acknowledgedStatusIsrEmail.add(isrInstance);
        }
        if((isrInstance.CIS_Status__c =='Report validated')&&(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c)){
            reportValidatedStatusISREmil.add(isrInstance);
        }
        if((isrInstance.CIS_Status__c =='Reported')&&(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c)){
            reportedStatusISREmil.add(isrInstance);
        }
        if((isrInstance.CIS_Status__c =='To be closed')&&(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c)){
            toBeClosedStatusISREmil.add(isrInstance);
        }
       
            
    }
    
    if(!sentStatusIsrEmail.isEmpty()){
        AP_CIS_ISRHandler.sendEmailToCISTeamsOnSentStatus(sentStatusIsrEmail);
    }
     
    if(!acknowledgedStatusIsrEmail.isEmpty()){
        AP_CIS_ISRHandler.sendEmailToCISTeamsOnAcknowledgedStatus(acknowledgedStatusIsrEmail);
    }
    if(!reportValidatedStatusISREmil.isEmpty()){
        AP_CIS_ISRHandler.sendEmailToCISTeamsOnReportValidatedStatus(reportValidatedStatusISREmil);
    }
    if(!reportedStatusISREmil.isEmpty()){
        AP_CIS_ISRHandler.sendEmailToCISTeamsOnReportedStatus(reportedStatusISREmil);
    }
    if(!toBeClosedStatusISREmil.isEmpty()){
        AP_CIS_ISRHandler.sendEmailToCISTeamsOnToBeClosedStatus(toBeClosedStatusISREmil);
    }
    
    
   } 
    
}