trigger LaunchOfferBeforeUpdate on Offer_Lifecycle__c (before update) {




 set<string> setUserId = new set<string>();
    if(Utils_SDF_Methodology.canTrigger('AP_Offer2')) {
    if(trigger.isUpdate && Trigger.isBefore) {
    
        for(Offer_Lifecycle__c trgObj:trigger.new ) {
            system.debug('HAna'+trgObj.Launch_Owner__r.email);
           // trgObj.BoxUserEmailId__c =trgObj.Launch_Owner__r.email; 
            if(trgObj.Launch_Owner__c!=null && trgObj.BoxUserEmailId__c!=null  && trgObj.Launch_Owner__c!=trigger.oldmap.get(trgObj.id).Launch_Owner__c) {
                setUserId.add(trgObj.Launch_Owner__c);
            }else if(trgObj.Withdrawal_Owner__c!=null && trgObj.BoxUserEmailId__c!=null && trgObj.Withdrawal_Owner__c!=trigger.oldmap.get(trgObj.id).Withdrawal_Owner__c) {
                setUserId.add(trgObj.Withdrawal_Owner__c); 
            }

        }
        
    
    }
    Map<id,User> mapUser =new Map<id,User>([select id ,email from user where id =:setUserId]);
    if(mapUser.size() > 0) {
        for(Offer_Lifecycle__c OffObj:trigger.new) {
            if(OffObj.Launch_Owner__c!=null) {  
                OffObj.BoxUserEmailId__c=mapUser.get(OffObj.Launch_Owner__c).email;
                OffObj.OwnerId=OffObj.Launch_Owner__c;
            }else if (OffObj.Withdrawal_Owner__c !=null) {
                OffObj.BoxUserEmailId__c=mapUser.get(OffObj.Withdrawal_Owner__c).email;
                OffObj.OwnerId=OffObj.Withdrawal_Owner__c;     
                
            }       
        
        }
    }

}

}