/********************************************************************************************************************
    Created By : Shruti Karn
    Description : For PRM May 13 Release:
                 1. Calls AP_CON_ContactProgramUpdate.deActivateChildRecords to deactivate all the child records when the Parent Program is deactivated.
                 2. Calls AP_CON_ContactProgramUpdate.delChildRecords to delete all the old child records when the related Program Level chenges and add the new child records.
    
********************************************************************************************************************/
trigger ContactProgramAfterUpdate on ContactAssignedProgram__c (After Update) {
    if(Utils_SDF_Methodology.canTrigger('AP_CON_ContactProgramUpdate') || test.isRunningTest())
    {
        list<Id> lstUpdatedContactProgramID = new list<ID>();
        list<Id> lstDeactivatedContactPrgID = new list<ID>();
        
        for(ID ContactPrgID : trigger.newMap.keySet())
        {
            //if(trigger.newMap.get(ContactPrgID).Active__c != trigger.oldMap.get(ContactPrgID).Active__c && trigger.oldMap.get(ContactPrgID).Active__c == true)
            if(trigger.newMap.get(ContactPrgID).Active__c != trigger.oldMap.get(ContactPrgID).Active__c)
            {
                lstDeactivatedContactPrgID.add(ContactPrgID);
                
            }
            
            if(trigger.newMap.get(ContactPrgID).ProgramLevel__c != trigger.oldMap.get(ContactPrgID).ProgramLevel__c)
            {
                lstUpdatedContactProgramID.add(ContactPrgID);
            }
        }
        
        //if(!lstDeactivatedContactPrgID.isEmpty())
        //    AP_CON_ContactProgramUpdate.deActivateChildRecords(trigger.newMap , trigger.oldMap);
        //if(!lstUpdatedContactProgramID.isEmpty())
        //    AP_CON_ContactProgramUpdate.delChildRecords(trigger.newMap , trigger.oldMap);

        //Apr 14 prm

        if(!lstDeactivatedContactPrgID.isEmpty() || !lstUpdatedContactProgramID.isEmpty() ){
            AP_ProcessFeatureAssignment.processContactAssignedFeatures(trigger.new);
        }

        //Akram for Q2 release: migrate to PRM V2 using memberProperties
        if(Utils_SDF_Methodology.canTrigger('AP_PartnerContactAssignementMigration'))
        {
            AP_PartnerContactAssignementMigration.createAndDeleteMemberProperties(trigger.oldMap,trigger.newMap);
        }

        
    }
}