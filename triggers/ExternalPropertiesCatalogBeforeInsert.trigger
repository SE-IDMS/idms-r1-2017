trigger ExternalPropertiesCatalogBeforeInsert on ExternalPropertiesCatalog__c (before Insert) {
    if(Utils_SDF_Methodology.canTrigger('PRM_ExternalPropertiesCatalog')) {
        PRM_ExternalPropertiesCatalog.checkBFOPropertiesFieldName(trigger.new);
    }
}