//*********************************************************************************
// Trigger Name     : CCCActionAfterUpdate
// Purpose          : CCCAction After Update event trigger
// Created by       : Global Delivery Team
// Date created     : 15th June 2012
// Modified by      :
// Date Modified    :
// Remarks          : For Sep - 12 Release
///********************************************************************************/

trigger CCCActionAfterUpdate on CCCAction__c (after update) {
    System.Debug('****** CCCActionAfterUpdate  Trigger Start****');
    
    if(Utils_SDF_Methodology.canTrigger('handleCCCActionAfterUpdate')){
        Map<Id,CCCAction__c> cccActionMap = new Map<Id,CCCAction__c>();
        for(CCCAction__c objCCCAction: Trigger.new){
            if(objCCCAction.ActionOwner__c != null){
                if(objCCCAction.ActionOwner__c != Trigger.oldMap.get(objCCCAction.id).ActionOwner__c){
                cccActionMap.put(objCCCAction.id,objCCCAction);
                }
            }
        }
        if(cccActionMap.size()>0 ){
            AP_ChatterForCCCAction.OwnerFollowTheCCCAction(cccActionMap );
        }
        AP_ChatterForCCCAction.CreateFeedOnDueDate(Trigger.newMap,Trigger.oldMap);
        //AP_ChatterForCCCAction.CreateFeedOnActionClosure(Trigger.newMap,Trigger.oldMap);
        AP_CCCAction_Handler.handleCCCActionAfterUpdate(Trigger.newMap,Trigger.oldMap);        
    }
    
    
    System.Debug('****** CCCActionAfterUpdate  Trigger Finished****');
}