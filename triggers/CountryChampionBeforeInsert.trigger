/*
    Author          : Kiran Kareddy (Schneider Electric)
    Date Created    : 23-March-2013
    Description     : Country Champions Before Insert and Update Trigger
*/

trigger CountryChampionBeforeInsert on Country_Champions__c (before insert) {

  System.Debug('****** CountryChampionAfterUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CountryChampionbeforeInsert'))
    {
        ConnectCountryChampionsTriggers.ConnectCountryChampionsbeforeInsertUpdate(trigger.new);
         if(Trigger.new[0].ValidateISDefault__c  == true)
                
         Trigger.new[0].Name.AddError(Label.ConnectDefaultCountryChamp);
       
 }
 System.Debug('****** CountryChampionAfterUpdate Trigger End ****'); 
}