trigger OpportunityRegistrationFormBeforeUpdate on OpportunityRegistrationForm__c (before update) {
    
    for(OpportunityRegistrationForm__C orf : trigger.new)
    {
        String strOwnerId = orf.ownerId;
        if(strOwnerId.startsWith('005'))
            orf.Tech_OrfOwner__c = orf.OwnerId;
        else
            orf.Tech_OrfOwner__c = null;    
    }
}