trigger OpportunityAssessmentAfterInsertUpdate on OpportunityAssessmentQuestion__c (after insert, after update) {
     if(Utils_SDF_Methodology.canTrigger('AP_AssessmentHandler')) {
       AP_AssessmentHandler.SetGlobalProgramApprovalFlag(Trigger.new);
   }
}