trigger CoveredProductAfterUnDelete on SVMXC__Service_Contract_Products__c (After UnDelete) {

        set<id>cppset= new set<id>();
        
    for(SVMXC__Service_Contract_Products__c cp:trigger.New){
            if(cp.SVMXC__Installed_Product__c!=null){
            cppset.add(cp.SVMXC__Service_Contract__c);
            }
        }
        
      if(cppset!=null && cppset.size()>0)
      AP_ServiceContract.SCIBSyncUpdate(cppset);

}