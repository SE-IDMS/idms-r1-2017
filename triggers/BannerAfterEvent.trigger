/*
    Author          : Pradeep Kumar (Schneider Electric)
    Date Created    : 06-May-2016
    Description     : Banner After Update Trigger, if country is changed
*/
trigger  BannerAfterEvent on FieloEE__Banner__c (after insert, after update) {
     if(Utils_SDF_Methodology.canTrigger('AP_PRMUtils')){
            AP_PRMUtils.CreateDeleteCMSShare(Trigger.new,Trigger.old,'Banner-Share');    
        }
 
}