trigger TeamMemberAfterAction on TeamMember__c (after insert, after update, after delete) {
    if (trigger.isUpdate || trigger.isDelete)
        AP_UpdateTeamSharing.removeSharing (trigger.oldMap);
	if (trigger.isInsert || trigger.isUpdate)
    	AP_UpdateTeamSharing.setSharing (trigger.newMap);
}