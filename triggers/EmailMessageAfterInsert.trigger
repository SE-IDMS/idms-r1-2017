/*
------------------------------
    Author          : Vimal Karunakaran
    Created Date    : 06 March 2014
    Description     : For April 2014 Release (BR–2151)
                    1. Call AP_EMailMessage postFeed() method
------------------------------
*/

trigger EmailMessageAfterInsert on EmailMessage (after Insert) {
    System.Debug('****** EmailMessageAfterInsert Trigger Start****');
    if(Utils_SDF_Methodology.canTrigger('AP_EMailMessage')){
        if(Trigger.new.size()==1){
            AP_EMailMessage.postFeedForCaseOwner(Trigger.new[0]);
        }
    }
    if(Utils_SDF_Methodology.canTrigger('AP_Case_CaseHandler') || Test.isRunningTest()){
        /********************************************************************************************************************
            Modified By : Vimal Karunakaran
            Description : For Oct 14  CCC Release:
                         1. Calls AP_Case_CaseHandler.updateLastActivityDate to update the Last Activity Date from CaseRelated List
        ********************************************************************************************************************/
        String strCaseKeyPrefix = SObjectType.Case.getKeyPrefix(); 
        Set<Id> setCaseIds = new Set<Id>();
        for(EmailMessage objEmailMessage:Trigger.New){
            if(objEmailMessage.ParentId!=null){
                if(String.valueOf(objEmailMessage.ParentId).startsWith(strCaseKeyPrefix)){
                    setCaseIds.add(objEmailMessage.ParentId);
                }
            }
        }
        if(setCaseIds.size()>0){
            AP_Case_CaseHandler.updateLastActivityDate(setCaseIds);
        }
    }
    /* 
     * @Alexandre Gonzalo For Oct 14 Rel - Add the call to method "updateCaseEmailDates" for Threshold scope.
     */
    if(Utils_SDF_Methodology.canTrigger('APAsyncEmailMessage') || Test.isRunningTest())
    {
        if(AP_AsyncEmailMessage.blnfutureCallEnabled){
            EmailMessage[] emailsWithCaseToUpdate = new EmailMessage[0];
            
            // Looping on all emails
            for (EmailMessage email : Trigger.new)
            {
                // Taking only emails attached to Cases
                if (Trigger.new[0].ParentId.getSObjectType()+''=='Case')
                {
                    // Either the email is an Inbound Email (received from the customer)
                    // Or the email is an outbound email BUT NOT an auto-response (Is outbound and created By System Interface User / 005A0000001nmj9IAA / CLOCT14CCC24)
                    if (Trigger.new[0].Incoming || Trigger.new[0].CreatedById != System.Label.CLOCT14CCC24)
                    {
                        emailsWithCaseToUpdate.add(email);
                    }
                }
            }
            if (emailsWithCaseToUpdate.size() == 1)
            {
                AP_AsyncEmailMessage.updateCaseEmailDates(emailsWithCaseToUpdate[0].parentId);
            }
        }
    }
    
    System.Debug('****** EmailMessageAfterInsert Trigger Ends****');
}