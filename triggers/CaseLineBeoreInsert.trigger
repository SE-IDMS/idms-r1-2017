/*
Created By-Deepak
For April Services Release-14
Puspose to put a validation role for Unique case and Installed product.
*/
trigger CaseLineBeoreInsert on SVMXC__Case_Line__c (before insert) 
{
    // trigger bypass
    if(Utils_SDF_Methodology.canTrigger('AP_CaseLine'))
    {
        List<SVMXC__Case_Line__c>  clList = New List<SVMXC__Case_Line__c>();
        set<Id> CaseId = New set<Id>();
        set<Id> IpId = New set<Id>();
        
        for(SVMXC__Case_Line__c cl:trigger.New)
        {
            if(cl.SVMXC__Case__c !=null && cl.SVMXC__Installed_Product__c !=null)
            {
                clList.add(cl);
                CaseId.add(cl.SVMXC__Case__c);
                IpId.add(cl.SVMXC__Installed_Product__c);
            }
        }
        if(clList.size()>0)
        AP_CaseLine.CLValidation(clList);
    }
    
}