trigger LiteratureRequestBeforeDelete on LiteratureRequest__c (before Delete) 
{
//*********************************************************************************
// Trigger Name     : LiteratureRequestBeforeDelete
// Purpose          : LiteratureRequest  Before Delete event trigger
// Created by       : Global Delivery Team
// Date created     : 18th Feb 2012
// Modified by      :
// Date Modified    :
// Remarks          : For Apr - 12 Release
///********************************************************************************/
 
   if(Utils_SDF_Methodology.canTrigger('AP40'))
   {      
        System.Debug('****** LiteratureRequestBeforeDelete Trigger Start ****');
        For(LiteratureRequest__c ltrobj : Trigger.old){          
         If(ltrobj.PONumber__c != null)
          ltrobj.PONumber__c.addError(Label.CL00735);
        }
        System.Debug('****** LiteratureRequestBeforeDelete Trigger Finished ****');        
   }
}