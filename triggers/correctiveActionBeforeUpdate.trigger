//*********************************************************************************
// Trigger Name     : correctiveActionBeforeUpdate 
// Purpose          : CorrectiveAction  Before Update event trigger
// Created by       : Global Delivery Team
// Date created     : 29th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/


trigger correctiveActionBeforeUpdate on CorrectiveAction__c (before update) {
    System.Debug('****** correctiveActionBeforeUpdate Trigger Start ****');
    
    List <CorrectiveAction__c> correctiveActionWithoutOwnerList = new List <CorrectiveAction__c>();
    
    if(Utils_SDF_Methodology.canTrigger('AP1003')){
        if(Trigger.new.size()>0){
            for(CorrectiveAction__c crtveActn:Trigger.new){
                crtveActn.LastUpdated__c = System.now();
                crtveActn.LastUpdatedby__c = UserInfo.getUserId();
                if(crtveActn.Owner__c == null){
                    correctiveActionWithoutOwnerList.add(crtveActn);
                }
            }
            AP1003_CorrectiveAction.populateCorrectiveActionOwner(correctiveActionWithoutOwnerList);
        }
    }      
    System.Debug('****** correctiveActionBeforeUpdate Trigger End ****');  
}