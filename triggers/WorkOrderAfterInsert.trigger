/*
    @Author: Chris Hurd
    Created Date: 28-03-2013
    Description: This trigger fires on Work Order on After Insert
    **********
    Scenarios:
    **********
    1.  Using Work Order field values and Work Order Assignment Object determine auto technician assignment if any.

    Date Modified       Modified By         Comments
    ------------------------------------------------------------------------
    17-07-2013          Ramu Veligeti       Logic to update Account's TECH_IsSVMXRecordPresent__c field
    05-03-2014          Deepak              Logic to Update Contact's TECH_IsSVMXRecordPresentForContact__c 
    */
    trigger WorkOrderAfterInsert on SVMXC__Service_Order__c (after insert) 
    {
        if(Utils_SDF_Methodology.canTrigger('SRV04')) 
        {
        
             // to add sharing to the creator of a follow-up WO
            SVMXC_WorkOrderAfter.createSharingForCreatorOnInsert(trigger.New, trigger.OldMap, trigger.isInsert, trigger.isUpdate);
        
        //***********Scenario 1 - Auto assignment
          Set<Id> acctIdSet = new Set<Id>();
           set<id> ippset1 = new set<id>();
          Set<String> buSet = new Set<String>();
          set<id> caseidset = new set<id>(); //added by suhail for Q3release 16  
          // Hari Added for DEC Release 2013
          List<SVMXC__Service_Order__c> woListforAssetCategory = new List<SVMXC__Service_Order__c>();
			Set<Id> ipid = new Set<Id>();
            List<SVMXC__Service_Order__c> wolist1 = new List<SVMXC__Service_Order__c>(); 
            Set<Id> ConId = new Set<Id>();
            List<SVMXC__Installed_Product__c> iplist = New List<SVMXC__Installed_Product__c>();
            String IpStatus = system.label.CLAPR14SRV02;
          
          for (SVMXC__Service_Order__c wo : trigger.new)
          {
		  
			if(wo.SVMXC__Component__c !=null && wo.SVMXC__Is_PM_Work_Order__c == false)
                {
                    ipid.add(wo.SVMXC__Component__c);   

                }
				if(wo.SVMXC__Contact__c!=null) 
                    ConId.add(wo.SVMXC__Contact__c);
				
              //added by suhail for Q3release 16 start
            if(wo.SVMXC__Case__c != null)  {
                caseidset.add(wo.SVMXC__Case__c);
                system.debug('filling caseidset');
            }
              //added by suhail for Q3release 16 end
            if (wo.SVMXC__Company__c != null)
            {
              acctIdSet.add(wo.SVMXC__Company__c);
            }
            
            if (wo.Service_Business_Unit__c != null)
            {
              buSet.add(wo.Service_Business_Unit__c);
            }
            if(wo.SVMXC__Component__c != null ){
                woListforAssetCategory.add(wo);
                ippset1.add(wo.SVMXC__Component__c);
            }
          }
          //added by suhail for Q3release 16 start
          if(caseidset != null){
              AP33_UpdateAnswerToCustomer.updateAnswerToCustomerFromWorkOrder(caseidset);
              system.debug('calling AP33_UpdateAnswerToCustomer.updateAnswerToCustomerFromWorkOrder(caseidset)');
          }
          //added by suhail for Q3release 16 end
         // added for March release 2016 BR-8786
         if(woListforAssetCategory != null && woListforAssetCategory.size()>0){
            SVMXC_WorkOrderAfter.syncProductExpertiseReqToWO(woListforAssetCategory);
         }
          
        if(ippset1!=null && ippset1.size()>0){
          Ap_Installed_Product.IBchangeMaster(ippset1);
        }
          
          
          
          if (acctIdSet.size() > 0)
          {
            
                Map<Id, Id> countryIdMap = new Map<Id, Id>();
            for (Account acct : [SELECT Id, Country__c FROM Account WHERE Id IN :acctIdSet])
            {
              countryIdMap.put(acct.Id, acct.Country__c);
            }
            List<WorkOrderAssignmentRule__c> woRuleList = [SELECT Id, BusinessUnit__c, Country__c, OnlyEnabledLocations__c, 
                  WorkOrderCategory__c, WorkOrderSource__c, WorkOrderType__c 
                  FROM WorkOrderAssignmentRule__c WHERE Country__c IN :countryIdMap.values() AND BusinessUnit__c IN :buSet 
                  ORDER BY WorkOrderSource__c ASC, WorkOrderCategory__c ASC, WorkOrderType__c ASC];
            Set<Id> siteIdSet = new Set<Id>();
                Map<Integer, WorkOrderAssignmentRule__c> woRuleMap = new Map<Integer, WorkOrderAssignmentRule__c>();
            System.debug('\n CLog1 : '+trigger.new.size());
            for (Integer i = 0; i < trigger.new.size(); i++)
            {
             System.debug('\n CLog1 : '+i);
              SVMXC__Service_Order__c wo = trigger.new.get(i);
              
              for (WorkOrderAssignmentRule__c rule : woRuleList)
              {
                if (wo.SVMXC__Site__c != null
                        && rule.Country__c == countryIdMap.get(wo.SVMXC__Company__c) 
                        && rule.BusinessUnit__c == wo.Service_Business_Unit__c 
                        && (rule.WorkOrderCategory__c == null || rule.WorkOrderCategory__c == wo.Work_Order_Category__c)
                        && (rule.WorkOrderType__c == null || rule.WorkOrderType__c == wo.SVMXC__Order_Type__c)
                        && (rule.WorkOrderSource__c == null || (rule.WorkOrderSource__c == 'Service Contract' && wo.SVMXC__Service_Contract__c != null)
                            || (rule.WorkOrderSource__c == 'PM Plan' && wo.SVMXC__PM_Plan__c != null)))
                {
                    System.debug('\n CLog : '+rule);
                    woRuleMap.put(i, rule);
                    siteIdSet.add(wo.SVMXC__Site__c);  
                }
              }
            }
            
            if (woRuleMap.size() > 0)
            {
                System.debug('\n CLog : '+woRuleMap);
                Map<Id, SVMXC__Site__c> siteMap = new Map<Id, SVMXC__Site__c>([SELECT Id, AutoAssignmentEnabled__c FROM SVMXC__Site__c WHERE Id IN :siteIdSet]);
                Map<Id, List<Role__c>> roleMap = new Map<Id, List<Role__c>>();
                Set<Id> userIdSet = new Set<Id>();
                Map<Id, Id> woAssignmentMap = new Map<Id, Id>();
                for (Role__c role : [SELECT Id, Location__c, User__c, Role__c FROM Role__c WHERE Location__c IN :siteIdSet])
                {
                  if (! roleMap.containsKey(role.Location__c))
                  {
                    roleMap.put(role.Location__c, new List<Role__c>());
                  }
                  roleMap.get(role.Location__c).add(role);
                  userIdSet.add(role.User__c);
                }
                Map<Id, SVMXC__Service_Group_Members__c> techMap = new Map<Id, SVMXC__Service_Group_Members__c>();
                for (SVMXC__Service_Group_Members__c tech : [SELECT Id, SVMXC__Service_Group__c, SVMXC__Email__c, SVMXC__Salesforce_User__c FROM SVMXC__Service_Group_Members__c WHERE SVMXC__Salesforce_User__c IN :userIdSet])
                {
                  techMap.put(tech.SVMXC__Salesforce_User__c, tech);
                }
                
                for (Integer index : woRuleMap.keySet())
                {
                  SVMXC__Service_Order__c wo = trigger.new.get(index);
                  WorkOrderAssignmentRule__c rule  = woRuleMap.get(index);
                  SVMXC__Site__c site = siteMap.get(wo.SVMXC__Site__c);
                  
                  if (roleMap.containsKey(site.Id) && (! rule.OnlyEnabledLocations__c || site.AutoAssignmentEnabled__c))
                  {
                    for (Role__c role : roleMap.get(site.Id))
                    {
                      CS_WorkOrderBURole__c CS = CS_WorkOrderBURole__c.getInstance(wo.Service_Business_Unit__c);
                     // if (role.Role__c == wo.TECH_FSEPreferredRole__c)
                     if(CS != null)
                     {
                       if(role.Role__c  == CS.RoleBu__c)
                        {
                          System.debug('\n CLog : '+role.User__c);
                          SVMXC__Service_Group_Members__c tech = techMap.get(role.User__c);
                          System.debug('\n CLog : '+tech);
                          if (tech != null)
                          {
                              woAssignmentMap.put(wo.Id, tech.Id);
                          }
                        }
                      }
                    }
                  }
                }
                
                if (woAssignmentMap.size() > 0)
                {
                    System.debug('\n CLog : '+woAssignmentMap);
                    ServiceMaxWOAutoAssignment.autoAssignTechnician(woAssignmentMap);
                }
            }
           }
           
            /*
                Added by: Ramu Veligeti
                Date: 16/07/2013
                Updates Work Order Group's Status based on the related WO's status
            */
            Map<Id,SVMXC__Service_Order__c> oldMap = new Map<Id,SVMXC__Service_Order__c>();
            ServiceMaxWOG.UpdateWOG(trigger.new,oldMap);
           
            /*
                Added by: Ramu Veligeti
                Date: 17/07/2013
                Update TECH_IsSVMXRecordPresent__c on Account object if the Account is used in 
                ServiceMax related objects like Work Order, Installed Product, Location, Service Contract etc..
            */
            Set<Id> AccId = new Set<Id>();
            for(SVMXC__Service_Order__c a:trigger.new)
            
                if(a.SVMXC__Company__c!=null) AccId.add(a.SVMXC__Company__c);
            SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
         
            /***************************************************************** 
            Added By-Deepak
            April-2014 Release(19/02/2014)
            For the purpose of Asset Categorization.
            ******************************************************************/
           
            
           /* for (SVMXC__Service_Order__c wo : trigger.new)
            {
                if(wo.SVMXC__Component__c !=null && wo.SVMXC__Is_PM_Work_Order__c == null)
                {
                    ipid.add(wo.SVMXC__Component__c);   

                }
                if(wo.SVMXC__Contact__c!=null) 
                    ConId.add(wo.SVMXC__Contact__c);
            }*/
            //if(ipid !=null)
            if( (ipid.size() > 0) && !(ipid.isempty()) )  //Added by VISHNU C for July Release 2016 Resolving Too Many SOQL Error in WO Interface
            iplist=[Select Id,Name,AssetCategory2__c,LifeCycleStatusOfTheInstalledProduct__c,SVMXC__Company__c,UnderContract__c,DecomissioningDate__c From SVMXC__Installed_Product__c
                Where Id =:ipid and AssetCategory2__c !=:IpStatus ];
            
            if(ConId !=null)
            {
                AP_IsSVMXRelatedContact.UpdateContact(ConId);
            }
            
            if(ipid.size()>0 && iplist.size()>0)
            {
                String ipString = JSON.serialize(iplist);
                Ap_WorkOrder.IPAssetCategoryUpdate(ipString,ipid);
            }
            //Ap_Installed_Product.AssetCategory(iplist);
           //update iplist;     
       }
    }