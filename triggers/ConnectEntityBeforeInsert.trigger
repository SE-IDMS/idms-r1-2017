/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 7-June-2012
    Description     : Connect Entity Progress Before Insert event trigger.
*/

trigger ConnectEntityBeforeInsert on Connect_Entity_Progress__c (before insert) {


  System.Debug('****** ConnectEntityBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('EntityBeforeInsert'))
    {
        ConnectEntityTriggers.ConnectEntityBeforeInsertUpdate_ValidateEntityRecord(Trigger.new);
                           
        ConnectEntityTriggers.ConnectEntityBeforeInsertUpdate_ValidateScopeEntity(Trigger.new);
        
       if(Trigger.new[0].ValidateEntityInsertUpdate__c == true)
                
         Trigger.new[0].Name.AddError(Label.EntityValidateERR);
        
           if (Trigger.new[0].ValidateScopeInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_EntityProgress_InsertUpdate_ERR1);
 }
 System.Debug('****** ConnectEntityBeforeInsert Trigger End ****'); 



}