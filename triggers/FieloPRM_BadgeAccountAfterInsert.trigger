/********************************************************************
* Company: Fielo
* Developer: 
* Created Date: 09/03/2015
* Description: 
********************************************************************/

trigger FieloPRM_BadgeAccountAfterInsert on FieloPRM_BadgeAccount__c (after insert) {
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_BadgeAccountTriggers')){
        if(trigger.isAfter && trigger.isInsert){
            FieloPRM_AP_BadgeAccountTriggers.setBadgeAccountName(trigger.newMap.keySet());
            FieloPRM_AP_BadgeAccountTriggers.recalculatesPartenerLocatorAccount(trigger.new, null);
            FieloPRM_AP_BadgeAccountTriggers.checkActiveBadges();
        }      
    }
}