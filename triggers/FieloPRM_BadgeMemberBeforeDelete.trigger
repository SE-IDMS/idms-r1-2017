/********************************************************************
* Company: Fielo
* Developer: 
* Created Date: 30/03/2015
* Description: 
********************************************************************/

trigger FieloPRM_BadgeMemberBeforeDelete on FieloEE__BadgeMember__c (before delete) {
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_BadgeMemberTriggers')){
        FieloPRM_AP_BadgeMemberTriggers.memberFeatureInactivation(trigger.Old);
        FieloPRM_AP_BadgeMemberTriggers.serializationOnMember(trigger.Old, trigger.OldMap);
    }
}