trigger AccountPlanConfidentialAfterInsert on AccountPlanConfidential__c (after insert) 
{    
    //Function to share Account Plan Confidential record with the User in Account Plan "Account Plan Owner" field 
    if(Utils_SDF_Methodology.canTrigger('AP_AccountPlanConfidential_RecordShare'))    
    {
        AP_AccountPlanConfidential_RecordShare.shareRecordWithAccountPlanOwner(Trigger.new);      
    } 
    
   
}