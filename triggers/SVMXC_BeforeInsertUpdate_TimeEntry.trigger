trigger SVMXC_BeforeInsertUpdate_TimeEntry on SVMXC_Time_Entry__c (before insert, before update)
{
	if(Utils_SDF_Methodology.canTrigger('SVMX23'))
	{
		Activate_Timesheet__c Activate_Timesheet   = new Activate_Timesheet__c();  // name of Custom Setting
		Activate_Timesheet = Activate_Timesheet__c.getValues('TimesheetState');  // grab the specific setting
	
		System.debug('###### AMO SVMXC_BeforeInsertUpdate_TimeEntry isInsert : ' + Trigger.isInsert);
		System.debug('###### AMO SVMXC_BeforeInsertUpdate_TimeEntry trigger.new : ' + Trigger.new);
	
		if (Activate_Timesheet.Enable_Timesheet__c==true || ServiceMaxTimesheetUtilsTest.TimesheetUtilsTest==true)
	    {
	        System.debug('###### AMO SVMXC_BeforeInsertUpdate_TimeEntry ensureTechIsOwnerAndSetDayOfWeek OK');
	        // ensures that the related tech is the owner of the time entry record
	        SVMXC_TimeEntryBefore.ensureTechIsOwnerAndSetDayOfWeek(trigger.New, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
	        
	        if (ServiceMaxTimesheetUtils.trg_insert_fired==false)
	        {
	        	System.debug('###### AMO SVMXC_BeforeInsertUpdate_TimeEntry trg_insert_fired FALSE');
	        	
	        	List<SVMXC_Time_Entry__c> teList = new List<SVMXC_Time_Entry__c>();
	        	
				if(Trigger.isInsert)
					teList = Trigger.new;
                
				//else if(Trigger.isUpdate)
		if(Trigger.isUpdate){
			for(SVMXC_Time_Entry__c te : Trigger.new){					
				if((te.Start_Date_Time__c != Trigger.oldMap.get(te.Id).Start_Date_Time__c)|| (te.End_Date_Time__c !=Trigger.oldMap.get(te.Id).End_Date_Time__c)	|| (te.Technician__c != Trigger.oldMap.get(te.Id).Technician__c) || (te.Activity__c != Trigger.oldMap.get(te.Id).Activity__c))
						{
							teList.add(te);
						}
					}
				}        	
	        	
	        	if(teList.size() > 0)
	        		ServiceMaxTimesheetUtils.checkForTimeEntryOverlap(teList, trigger.oldMap);   
	        	
	        	
	        	ServiceMaxTimesheetUtils.trg_insert_fired = true;
	        }
	        else System.debug('###### AMO SVMXC_BeforeInsertUpdate_TimeEntry trg_insert_fired TRUE');
	        
	        // AMO : 02 mai 2016, move this line outside the "if" condition, bug time overlaps, timesheet -> null
	        ServiceMaxTimesheetUtils.updateTimeEntries(trigger.new); 
	     }
		else System.debug('###### AMO SVMXC_BeforeInsertUpdate_TimeEntry ensureTechIsOwnerAndSetDayOfWeek KO');     
	}
}