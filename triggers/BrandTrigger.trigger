trigger BrandTrigger on Brand__c (after delete, after insert, after update, before delete, before insert, before update) {
    
    Ap_Brand  brand= new Ap_Brand(Trigger.isBefore, Trigger.isAfter, 
                           Trigger.isUpdate, Trigger.isInsert, Trigger.isDelete, 
                           
                           Trigger.new, Trigger.oldMap, Trigger.newMap);
    brand.execute();
    
}