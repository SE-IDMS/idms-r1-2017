trigger PSassignmentRequestAfterInsert on PermissionSetAssignmentRequests__c (after insert) {

    Set<Id> validatedPsIdSet = new Set<Id>();

    for(PermissionSetAssignmentRequests__c psRequest:Trigger.New) {

        if(psRequest.Status__c == 'Assigned') {
            validatedPsIdSet.add(psRequest.Id);
        }    
    }    

    AP_PsAssignmentMethods.assignPermissionSet(validatedPsIdSet);
}