/*
@Author: Anita
Created Date: 2-08-2011
Description: This trigger fires on Event on Before Delete
**********
Scenarios:
**********
1.  On Delete of Work Order Event, delete Assigned Tools & Technician record.

Date Modified       Modified By         Comments
------------------------------------------------------------------------
02-08-2011          Anita D'Souza       Added Logic for Second scenario
*/

trigger EventBeforeDelete on Event (Before delete) 
{
    //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX02')){
    System.debug('************** Log5 >> '+Limits.getQueries());
    List<AssignedToolsTechnicians__c> TechToolList = new List<AssignedToolsTechnicians__c>();
    List<String> EventIDList = new List<String>();    
    List<Event> EventList = new List<Event>();       
    set<id> techid = new set<id>();
    Map<id,id> eidATTidMap = new Map<id,id>();
    Set<String> eidset = new Set<String>(); 
    //List<Event> eventsForWO = new List<Event>();
    //set<id> woidSet = new Set<id>();
    
    for (Event sevent : Trigger.old)    
    {   
        String SOPrefix = SObjectType.SVMXC__Service_Order__c.getKeyPrefix();
        String whatidLookup=sevent.whatid; 
             
       // 
        //if(whatidLookup.substring(0, 3) == SOPrefix) 
        if(sevent.Subject.contains('WO-')) 
        {        
            eventIdList.add(sevent.Id);            
        }
        if(sevent.OwnerId != null){
            techid.add(sevent.OwnerId);
        }
        if(whatidLookup != null && whatidLookup.substring(0, 3) == SOPrefix)
        {
            eidset.add(sevent.Id);
            //woidSet.add(whatidLookup);
            //eventsForWO.add(sevent);
        }
        
        
    }
    // commeted for May Release 2013  
    /*  
    if(EventList != null)      
    {                    
        
         TechToolList = [Select ID from AssignedToolsTechnicians__c where EventId__c =:eventIdList];
    }
    */
    if(techid.size()>0){
        List<SVMXC__Dispatcher_Access__c> DispAccs= new List<SVMXC__Dispatcher_Access__c>();
        List<SVMXC__Service_Group_Members__c> Techlist= new List<SVMXC__Service_Group_Members__c>();
        set<id> STid = new set<id>();
         Techlist=[select id,SVMXC__Service_Group__c from SVMXC__Service_Group_Members__c where SVMXC__Salesforce_User__c in:techid];
         for(SVMXC__Service_Group_Members__c st:Techlist){
            STid.add(st.SVMXC__Service_Group__c);
         }
         DispAccs=[select id from SVMXC__Dispatcher_Access__c where SVMXC__Dispatcher__c=:UserInfo.getUserId() and SVMXC__Service_Team__c in:STid and Read_Only__c=true];
         if(DispAccs.size()>0 && DispAccs!= null){
            trigger.old[0].addError('Sorry!! You only have read-only access.');
         }      
    }
    
    System.debug('TechToolList ='+TechToolList );
    // commented By Hari Krishna GD for BR-2469  
        
    //if(TechToolList != null)
        //Database.Delete(TechToolList);
       
    /********************************************************************************************************************/
    // Scenario: 3
    // Purpose : When a Event is Deleted when work order status is Scheduled delete the respective Assigned Tools Techinician based on  Event Id in ATT. 
    // Created by : Hari Krishna - Global Delivery Team
    // Date created : 1th Oct 2013
    // Modified by :
    // Date Modified :
    // Remarks : For Oct - 15 Release
    /********************************************************************************************************************/ 
    if(eidset != null && eidset.size()>0){
        AP_AssignedToolsTechnicians.CancelledAssignedToolsTechnicians(eidset);
        //AP_AssignedToolsTechnicians.CancelledATTAtCustomerConfirmANDAckFsr(eidset);
    
    }
   
        
    }
    /********************************************************************************************************************/
    // Scenario: 2
    // Purpose : When a WO is unassigned, don't remove the Assigned Tools/Technicians lines but only "cancel" them. This will allow to track rejection ratio and reason in KPIs when needed. 
    // Created by : Hari Krishna - Global Delivery Team
    // Date created : 12th Feb 2013
    // Modified by :
    // Date Modified :
    // Remarks : For May - 13 Release
    /********************************************************************************************************************/ 
    Set<id> woidSet = new Set<id>();
    for(Event e:Trigger.old){
       if(e.whatId != null){
           String s=e.whatid;
           String SOPrefix = SObjectType.SVMXC__Service_Order__c.getKeyPrefix(); 
            if(s.startsWith(SOPrefix ) ){
                woidSet.add(e.whatId);
          }
      }
      
    }
    if(woidSet != null && woidSet.size()>0){
         //AP_AssignedToolsTechnicians.updateAssignedToolsTechniciansStatus(woidSet,'Cancelled');//Commented for BR-7889
    }
    System.debug('************** Log6 >> '+Limits.getQueries());
     
    
}