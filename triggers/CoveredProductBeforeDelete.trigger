/*Created by: Deepak Kumar
Date:12/08/2013
For the purpose of Updating Installed Product Field Under Contract Field Value From service COntract Active Status Value.
OCT13 Release
*/


trigger CoveredProductBeforeDelete on SVMXC__Service_Contract_Products__c(before delete)
{
    /*
    Set<Id> ipId = New Set<Id>();
    Set<Id> CpId = New Set<Id>();
    List<SVMXC__Service_Contract_Products__c >  CovList  = new List<SVMXC__Service_Contract_Products__c> ();
      
    for(SVMXC__Service_Contract_Products__c cov :trigger.Old)
    {
        CpId.add(cov.id);
        ipId.add(cov.SVMXC__Installed_Product__c);
        CovList.add(cov);
    }
   AP_CoveredProductBeforeDelete.DeletedRecordUpdate(ipId,CpId,CovList) ;
    */
    List<SVMXC__Service_Contract_Products__c >  CovList  = new List<SVMXC__Service_Contract_Products__c> ();
    for(SVMXC__Service_Contract_Products__c cov :trigger.Old)
    {
        if(cov.SVMXC__Installed_Product__c!= null)
        CovList.add(cov );
    }
    if(CovList != null && CovList.size()>0)
   AP_CoveredProduct.InstalledProdcutUnderContract(CovList); 
    
}