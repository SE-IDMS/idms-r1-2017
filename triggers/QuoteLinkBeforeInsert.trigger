trigger QuoteLinkBeforeInsert on OPP_QuoteLink__c (before insert) {
System.debug('****** QuoteLinkBeforeInsert start *****');
if(!VFC20_CreateSeries.doNotProcessQLKS){
System.debug('****** Not bypassing quotelink before insert *****');
    if(Utils_SDF_Methodology.canTrigger('AP02'))
    {
        AP02_QuoteLinkFieldUpdate.performCurrentUserUpdates(Trigger.new);
        AP02_QuoteLinkFieldUpdate.performLastUserUpdates(Trigger.new);
        List<OPP_QuoteLink__c> updateAgreementIds = new List<OPP_QuoteLink__c>();
        List<OPP_QuoteLink__c>AP02_actionList= new List<OPP_QuoteLink__c>();
        for(Integer i=0;i<Trigger.new.size();i++)
        {
            if(Trigger.new[i].QuoteStatus__c == 'Approved' && Trigger.new[i].ActiveQuote__c == true)
                AP02_actionList.add(Trigger.new[i]);
                
            //Populates Agreement field with TECH_AgreementId__c - Modified for BR-6915
            if(trigger.new[i].TECH_AgreementId__c!=null)
                updateAgreementIds.add(trigger.new[i]);    
                
        }
        if(AP02_actionList.size()>0){
            AP02_QuoteLinkFieldUpdate.performOppUpdates(AP02_actionList);
        }
        //Modified for BR-6915
        if(updateAgreementIds.size()>0)
        AP02_QuoteLinkFieldUpdate.updateAgreement(updateAgreementIds);
        
    }
    if(Utils_SDF_Methodology.canTrigger('AP21'))
    {
        system.debug('#####Trigger updateTECH_HasActiveQLKwthMargin starts');
        AP21_UpdateOpportunity.updateTECH_HasActiveQLKwthMargin(Trigger.new, Label.CL00749);
        system.debug('#####Trigger updateTECH_HasActiveQLKwthMargin ends');
    }   
    }
System.debug('****** QuoteLinkBeforeInsert start *****');
}