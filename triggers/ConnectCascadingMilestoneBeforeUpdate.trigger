/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 03-Feb-2012
    Description     : Connect Cascading Milestones Before Update event trigger.
*/

trigger ConnectCascadingMilestoneBeforeUpdate on Cascading_Milestone__c (before update) {

  System.Debug('****** ConnectCascadingMilestoneBeforeUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CascadingMilestoneBeforeUpdate'))
    {
        
        //ConnectCascadingMilestoneTriggers.CascadingMilestoneBeforeUpdate_AppendTextColoumns(Trigger.new);
          ConnectCascadingMilestoneTriggers.CascadingMilestoneBeforeUpdate_ProgressSummaryUpdate(Trigger.new);
     if((Trigger.new[0].Entity__c != Trigger.old[0].Entity__c) || (Trigger.new[0].Program_Scope__c != Trigger.old[0].Program_Scope__c) || (Trigger.new[0].GSC_Region__c != Trigger.old[0].GSC_Region__c)|| (Trigger.new[0].Partner_Region__c != Trigger.old[0].Partner_Region__c)|| (Trigger.new[0].Smart_Cities_Division__c != Trigger.old[0].Smart_Cities_Division__c))
        ConnectCascadingMilestoneTriggers.CascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity(Trigger.New);
       ConnectCascadingMilestoneTriggers.CascadingMilestoneBeforeUpdate(Trigger.New);
           if (Trigger.new[0].ValidateScopeonInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_Cascading_Milestone_InsertUpdate_ERR1);
                 
           if (Trigger.new[0].ValidateDeploymentLeader__c == True)
        
                 Trigger.new[0].Name.AddError(Label.connect_Deployment_LeaderERR);
 }
 System.Debug('****** ConnectCascadingMilestoneBeforeInsert Trigger End ****'); 
}