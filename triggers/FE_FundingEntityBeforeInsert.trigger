/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 03/08/2012
    Description     : DMT Funding Entity Before Insert event trigger.
*/
trigger FE_FundingEntityBeforeInsert on DMTFundingEntity__c (before insert) 
{
    System.Debug('****** FE_FundingEntityBeforeInsert Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('FundingEntityTriggerInsert'))
    {
        System.Debug('Value New: ' + Trigger.new);
        System.Debug('Value NewMap: ' + Trigger.newMap);
        FE_FundingEntityTriggers.fundingentityBeforeInsert(Trigger.new);
    }
    System.Debug('****** FE_FundingEntityBeforeInsert Trigger End ****');     
}