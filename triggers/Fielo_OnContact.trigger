/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: Trigger                                             *
* Created Date: 19/08/2014                                  *
************************************************************/
trigger Fielo_OnContact on Contact (before insert) {

    if(Utils_SDF_Methodology.canTrigger('Fielo_ContactTriggers')){    
        if (trigger.isBefore && trigger.isInsert){
            Fielo_ContactTriggers.createsAccount(trigger.new);
        }
    }

}