trigger IntroductionProjectAfterInsert on Milestone1_Project__c (after insert) {
System.Debug('********* IntroductionProjectAfterInsert Trigger Start *********');
  
   
    Id projecIntrductionRegon = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Introduction Region').getRecordTypeId();
    Id projecSolutionRegon = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Launch Solution Region').getRecordTypeId(); 
    Id projecWithdrawalRegon = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Withdrawal Region').getRecordTypeId();        
    
    List<Milestone1_Milestone__c> lstMilestone = new List<Milestone1_Milestone__c >();
    List<Milestone1_Task__c> lstTask = new List<Milestone1_Task__c >();
    map<id,Milestone1_Project__c> mapMilProject = new map<id,Milestone1_Project__c>();
    //April 2015
        Set<string>  setProjectId = new Set<string>();
    
    for (Milestone1_Project__c newProject : Trigger.new){ 
    
        //Oct2014 Release Hanamanth
        if(newProject.RecordTypeId==Label.CLOCT14COINbFO07) {
            mapMilProject.put(newProject.id,newProject);
        
        }
        else if(newProject.RecordTypeId==Label.CLOCT14COINbFO12) {
            mapMilProject.put(newProject.id,newProject);
        
        }
        else if(newProject.RecordTypeId==Label.CLOCT14COINbFO10) {
            mapMilProject.put(newProject.id,newProject);
        
        }
         else if(newProject.RecordTypeId==projecIntrductionRegon || newProject.RecordTypeId==projecSolutionRegon || newProject.RecordTypeId==projecWithdrawalRegon ) {
            mapMilProject.put(newProject.id,newProject);
        
        }
    }   
     //
       if(mapMilProject.size() > 0) {
           AP_bFO_COLA_OfferCountryBUTaskCreation.offBUPMPTaskCreation(mapMilProject,'Country');
           
       
       } 
       List<Milestone1_Project__c> lstMilestone1 = new List<Milestone1_Project__c>();
       if(trigger.isinsert && trigger.isafter) {
           for(Milestone1_Project__c mpObjt:trigger.new) {
               lstMilestone1.add(mpObjt);
               setProjectId.add(mpObjt.id); 
               
           }
           //Update end service date sprint 1
           AP_bFO_COLA_OfferCountryBUTaskCreation.updateAllEndofServicesDate(lstMilestone1);
      } 
      
  //April Release 2014 Hanamanth
    If(trigger.isInsert && Trigger.IsAfter) {
    if(!Test.isRunningTest()){
        if(setProjectId.size() > 0) {
                AP_ELLAToBoxFolderCreation.httpProjectCountryFolderInBox(setProjectId);
            }

        }
    }
    
    //March Q1 release 2016
    if(trigger.isinsert && trigger.isafter) {
        boolean blnIsNew=True;
        AP_bFO_COLA_OfferCountryBUTaskCreation.UserAccessforProject(trigger.new,trigger.oldmap,blnIsNew);
        
    }    
 }