/*   Author: Anuj Maheshwari  
         Date Of Creation: 23/04/2011   
         Description : Business Risk After Insert event trigger
    */

        trigger BusinessRiskAfterInsert  on BusinessRiskEscalations__c(After Insert) {
                System.Debug('****** BusinessRiskAfterInsert Trigger Start ****'); 
                            if(Utils_SDF_Methodology.canTrigger('AP09')){ 
                                       if(Trigger.new.size()>0){
                                             AP09_BusinessRiskEscalation.createBusinessRiskStakeholder(Trigger.new);
                                             AP09_BusinessRiskEscalation.createInvolvedOrganization(Trigger.new);
                                          }
                               }
                  System.Debug('****** BusinessRiskAfterInsert Trigger End ****'); 
                           
        }