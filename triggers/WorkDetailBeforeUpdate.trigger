/**
* @author: Nabil ZEGHACHE
* Date Created (YYYY-MM-DD): 2016-09-01
* Test class: 
* Description: This is the beforeInsert trigger of the Work Detail object
* -----------------------------------------------------------------
*                     MODIFICATION HISTORY
* -----------------------------------------------------------------
* Modified By: Authors Name
* Modified Date: Date
* Description: Brief Description of Change + BR/Hotfix
* -----------------------------------------------------------------
*/
trigger WorkDetailBeforeUpdate on SVMXC__Service_Order_Line__c (before update) {
	System.debug(Logginglevel.INFO, '#### WorkDetailBeforeUpdate start: '+trigger.new);
	
	if(Utils_SDF_Methodology.canTrigger('SRV06')) {
		System.debug(Logginglevel.INFO, '#### WorkDetailBeforeUpdate SRV06 can trigger');
		
		// This set will contain all the WO IDs related to the WDs of the trigger
		Set<Id> allWOIDSet = new Set<Id>();
		// This map will contain the set of associated WO IDs as keys and the associated WOs as values. It is meant to be used one per call to the trigger to avoid multiple SOQL queries
		Map<Id, SVMXC__Service_Order__c> woMap;
		
		Set<Id> woIdSet = new Set<Id>();
		Set<Id> ipIdSet = new Set<Id>();
	    Set<Id> partset1 = new Set<Id>();
	    Set<string> rmaset= new set<string>();
	    Set<string> rmaset1= new set<string>();
	    String RMA_RT = System.Label.CLJUL15SRV03;
	    String Parts_RT = System.Label.CLJUL15SRV04;
		String ServicedProduct_RT = System.Label.CLQ316SRV02; //Added by VISHNU C for Q3 2016 Release BR-9850 08/08/2016
	    String Actuals_RT = Label.CLSSRV_WDD_RecordType;
	    Set<String> nameset = new Set<String>();
	    List<SVMXC__Service_Order_Line__c> wodlist = new List<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> wdList = new List<SVMXC__Service_Order_Line__c>();
	    List<SVMXC__Service_Order_Line__c> detailList = new List<SVMXC__Service_Order_Line__c>();
	    //RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'SVMXC__Service_Order_Line__c' AND Name = 'Products Serviced' LIMIT 1];
	    //string rt = System.Label.CLJUL16SRV01; // added by suhail for fixing too many SOQL queries error
	    //RecordType rts = [SELECT Id FROM RecordType WHERE SobjectType = 'SVMXC__Service_Order_Line__c' AND Name = 'Labor' LIMIT 1];//added by suhail june release 2016 
	    Set<id> fsrset = new Set<id>();
	    Set<id> woidset1= new Set<id>();
	    Map<id,Set<String>> woidPONameMap = new Map<id,Set<String>>();
	    List <SVMXC__RMA_Shipment_Order__c> PartsOrderList= new List <SVMXC__RMA_Shipment_Order__c>();
	    List<SVMXC__Service_Order_Line__c> toProcess = new List<SVMXC__Service_Order_Line__c>();
	    string MIPinSplit = '';
		List<SVMXC__Service_Order_Line__c> wdlst = new List<SVMXC__Service_Order_Line__c>();
	    List<SVMXC__Service_Order_Line__c> wdList2 = new List<SVMXC__Service_Order_Line__c>(); // to store WD to calculate start/end date/time in WO time zone
	    
	    
	    for (SVMXC__Service_Order_Line__c detail : trigger.new) {
	    	System.debug('#### Current WD: '+detail);
	    	
	    	allWOIDSet.add(detail.SVMXC__Service_Order__c);
	    	
	    	// Condition for scenario 1
			if(detail.SVMXC__Group_Member__c == null && detail.SVMXC__Service_Order__c != null) { // Only consider Work Details where the technician is empty
			    wdlst.add(detail);
			}
	    	
	        SVMXC__Service_Order_Line__c beforeUpdate = null;
        
	        if (trigger.isUpdate && trigger.oldMap.containsKey(detail.Id))
	        {
	            beforeUpdate = trigger.oldMap.get(detail.Id);
	        }
        
	        if (detail.RecordTypeId == ServicedProduct_RT && detail.SVMXC__Serial_Number__c != null)
	        {
	            woIdSet.add(detail.SVMXC__Service_Order__c);
	            ipIdSet.add(detail.SVMXC__Serial_Number__c);
	            detailList.add(detail);
	        }
	        if(detail.SVMXC__Group_Member__c !=null){
	            fsrset.add(detail.SVMXC__Group_Member__c);
	        }
	        // updated  by Anand for May 2015 Release
	        if(detail.Parts_Order_Line_RMA__c==null && detail.RMANumber__c!=null && beforeUpdate!=null && beforeUpdate.RMANumber__c!=detail.RMANumber__c && detail.ReturnedQuantity__c!=beforeUpdate.ReturnedQuantity__c) {
	         	wodlist.add(detail);
	            partset1.add(detail.PartsOrderLine__c);
	            rmaset.add(detail.RMANumber__c);
	        }

			//Added by Anand for BR-7908
			System.debug('#### detail.RMANumber__c: '+detail.RMANumber__c);
			System.debug('#### trigger.oldmap.get(detail.id).RMANumber__c: '+trigger.oldmap.get(detail.id).RMANumber__c);
			System.debug('#### detail.recordtypeid: '+detail.recordtypeid);
			System.debug('#### Parts_RT: '+Parts_RT);
			if((detail.RMANumber__c!=null && detail.RMANumber__c!=trigger.oldmap.get(detail.id).RMANumber__c) && detail.recordtypeid==Parts_RT) 
			{
				rmaset1.add(detail.RMANumber__c);
				if(detail.SVMXC__Service_Order__c != null)
				{
					woidset1.add(detail.SVMXC__Service_Order__c);
					toProcess.add(detail);
				}
			}//End
			
			// Construct list to calculate the start/end date/time in WO time zone
			System.debug('#### Current WD record type: '+detail.RecordTypeId);
			System.debug('##### Actual WD record type: '+Actuals_RT);
			if (detail.RecordTypeId == Actuals_RT) { // Only consider Actuals
	        	wdList2.add(detail);
	        }
	        
            if(detail.SVMXC__Start_Date_And_Time__c != Trigger.oldMap.get(detail.Id).SVMXC__Start_Date_And_Time__c
                || detail.SVMXC__End_Date_and_Time__c != Trigger.oldMap.get(detail.Id).SVMXC__End_Date_and_Time__c
                || detail.SVMXC__Group_Member__c != Trigger.oldMap.get(detail.Id).SVMXC__Group_Member__c
                || detail.SVMXC__Line_Type__c != Trigger.oldMap.get(detail.Id).SVMXC__Line_Type__c
                || detail.SVMXC__Activity_Type__c != Trigger.oldMap.get(detail.Id).SVMXC__Activity_Type__c
            )
            {
                wdList.add(detail);
            }
	        
		}
		
		// Make one single query to retrieve the associated Work Orders and pass them as parameters when needed
		System.debug('#### Current workDetail\'s related Work Orders: '+allWOIDSet);
		woMap = AP_WorkDetails.getWOMapForIDSet(allWOIDSet);
	    System.debug('#### Work Orders Map: '+woMap);
		
		// Clear seconds from WD
		AP_WorkDetails.clearSecondsFromStartAndEndTime(trigger.new);
		ServiceMaxTimesheetUtils.checkForWorkDetailOverlap(wdList, trigger.oldMap);
		
		/**
		* Scenario: 1
		* Update the Technician/Equipment Field  with the logged in User or the Primary FSR if the logged in User is not an FSR if Technician/Equipment Field == null
		*/

		if (wdlst != null && !wdlst.isEmpty()) {
			AP_WorkDetails.setPrimaryFSR(woMap, wdlst);
		}
		
		AP_WorkDetails.setFSRTimesFromCustomerTimes(wdList2);
		AP_WorkDetails.setDateTimeInCustomerTimeZone(wdList2, woMap);
		AP_WorkDetails.setBillableFlagFromWOAndIsAdminFlag(trigger.new, woMap);
		
        
		if(wodlist!= null && wodlist.size()>0 && partset1!=null && rmaset!=null){
			AP_WorkDetails.partsorderlineupdate(wodlist,partset1,rmaset);
		}
    
        if(woidset1 != null && woidset1.size()>0)
                     PartsOrderList =  [select id, recordtypeid,name,SVMXC__Service_Order__c,SVMXC__Order_Status__c from SVMXC__RMA_Shipment_Order__c where SVMXC__Order_Status__c='Open'  and recordtypeid =:RMA_RT and SVMXC__Service_Order__c in:woidset1];
            if(PartsOrderList!=null && PartsOrderList.size()>0)
            { 
                for(SVMXC__RMA_Shipment_Order__c po:PartsOrderList)
                {
            
                    if(woidPONameMap.containskey(po.SVMXC__Service_Order__c ))
                    {
                        woidPONameMap.get(po.SVMXC__Service_Order__c ).add(po.name);
                    }
                    else
                    {
                        //Set<String> nameset = new Set<String>();
                        nameset.add(po.name);
                        woidPONameMap.put(po.SVMXC__Service_Order__c ,nameset);
                    }
                }
        
                for (SVMXC__Service_Order_Line__c detail :toProcess)
                {
                    //Set<String> 
                    nameset = woidPONameMap.get(detail.SVMXC__Service_Order__c);
                   if(nameset!=null)
                    if(nameset.contains(detail.RMANumber__c))
                    {
                        // nothing to do
                    }
                    else
                    {
                        detail.adderror ('Work Detail '+ detail.Name+' references an invalid RMA#'+detail.RMANumber__c+' Please request a new RMA#');
                    }
                }
        
        }
        else
        {
            
            for (SVMXC__Service_Order_Line__c detail :toProcess)
			{
                    detail.adderror ('Work Detail '+ detail.Name+' references an invalid RMA#'+detail.RMANumber__c+' Please request a new RMA#');
			}
            
        }
            
		//End:Def-7908
    
		// May 2015 Release Stated
    
        if(fsrset != null && fsrset.size()>0){
            Map<id, SVMXC__Service_Group_Members__c> sgmap = new Map<id,SVMXC__Service_Group_Members__c>();
            List<SVMXC__Service_Group_Members__c> sglist=[select id ,Level__c from SVMXC__Service_Group_Members__c where id in :fsrset ];
            if(sglist!= null && sglist.size()>0)
            {
                sgmap.putAll(sglist);
            }
            for (SVMXC__Service_Order_Line__c detail : trigger.new)
            {
                if(detail.SVMXC__Group_Member__c !=null && sgmap.containskey(detail.SVMXC__Group_Member__c)){
                    detail.Level_Required__c = sgmap.get(detail.SVMXC__Group_Member__c).Level__c;
                }
                else{
                
                    detail.Level_Required__c = null;
                }
                
            }
            
        }
    
		//End
    
		if (detailList.size() > 0)
		{
			Map<Id, Id> contractIdMap = new Map<Id, Id>();
			if((woIdSet.size()>0)&&!(woIdSet.isempty())){ //added by suhail to fix too many SOQL queries error
				//Map<Id, SVMXC__Service_Order__c> woMap = new Map<Id, SVMXC__Service_Order__c>([SELECT Id, SVMXC__Service_Contract__c, SVMXC__PM_Plan__c, SVMXC__PM_Plan__r.SVMXC__Service_Contract__c FROM SVMXC__Service_Order__c WHERE Id IN :woIdSet]);
				SVMXC__Service_Order__c associatedWO;
				for (SVMXC__Service_Order_Line__c currentWD : detailList)
				{
					associatedWO = woMap.get(currentWD.SVMXC__Service_Order__c);					
					if (associatedWO != null && associatedWO.SVMXC__Service_Contract__c != null)
					{
						contractIdMap.put(associatedWO.Id, associatedWO.SVMXC__Service_Contract__c);
					}
					else
					{
					    contractIdMap.put(associatedWO.Id, associatedWO.SVMXC__PM_Plan__r.SVMXC__Service_Contract__c);
					}
				}
				if((contractIdMap.size()>0)&&!(contractIdMap.isempty())&&(ipIdSet.size()>0)&&!(ipIdSet.isempty())){ //added by suhail to fix too many SOQL queries error
				    
					List<SVMXC__Service_Contract_Products__c> covProdList = [SELECT Id, SVMXC__Installed_Product__r.SVMXC__Site__c, IncludedService__c, SVMXC__Service_Contract__c FROM SVMXC__Service_Contract_Products__c WHERE SVMXC__Service_Contract__c IN :contractIdMap.values() AND SVMXC__Installed_Product__c IN :ipIdSet];
					
					for (SVMXC__Service_Order_Line__c detail : detailList)
					{
						SVMXC__Service_Order__c wo = woMap.get(detail.SVMXC__Service_Order__c);
						for (SVMXC__Service_Contract_Products__c covProd : covProdList)
						{
							if (covProd.SVMXC__Service_Contract__c == contractIdMap.get(wo.Id) && detail.SVMXC__Serial_Number__c == covProd.SVMXC__Installed_Product__c)
							{
								detail.ProductLocation__c = covProd.SVMXC__Installed_Product__r.SVMXC__Site__c;
								detail.IncludedService__c = covProd.IncludedService__c;
							}
						}
					}
				  
				}
			}
		}   	
    	
    }
    
    System.debug(Logginglevel.INFO, '#### WorkDetailBeforeUpdate end');

}