/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 14-March-2012
    Description     : Connect Team Members Before Delete event trigger.
*/

trigger ConnectTeamMembersBeforeDelete on Team_Members__c (before delete) {

  System.Debug('****** ConnectTeamMembersBeforeDelete Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('ConnectTeamMembersBeforeDelete'))
    {

    ConnectTeamMembersTriggers.TeamMembersBeforeDelete(Trigger.old);
    }
}