/*
    Author          : Priyanka Shetty (Schneider Electric)
    Date Created    : 12/04/2013
    Description     : Change Request Costs Forecasts Beforeupdate event trigger.
*/
trigger CHR_CostForecastBeforeInsert on ChangeRequestCostsForecast__c (before Insert) {

    if(Utils_SDF_Methodology.canTrigger('ChangeRequestCostForecastTriggerBeforeInsert'))
    {
        CHR_CostForecastTriggers.CRCostForecastbeforeInsert(Trigger.new);
    }

}