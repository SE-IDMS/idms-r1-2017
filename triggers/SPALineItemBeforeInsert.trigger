trigger SPALineItemBeforeInsert on SPARequestLineItem__c (before insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_SPALineItemCalculations'))
    {
        List<SPARequestLineItem__c> tobeprocessed=new List<SPARequestLineItem__c>();
        for(SPARequestLineItem__c lineitem:Trigger.New){
            // Added Free of Charge filter for BR-4968 October 2014 release 
            if(lineitem.RequestedType__c!=System.Label.CLOCT14SLS11){
                if(lineitem.LocalPricingGroup__c!=null && lineitem.LocalPricingGroup__c!='')
                    tobeprocessed.add(lineitem);
                else
                    lineitem.addError(System.Label.CLSEP12SLS27);   
                } 
        }        
            
        if(tobeprocessed.size()>0)
            AP_SPALineItemCalculations.checkForThreshold(tobeprocessed);
    }
}