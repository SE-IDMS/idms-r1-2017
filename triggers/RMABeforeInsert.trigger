/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | Before Insert Trigger                                                              |
|                       |                                                                                    |
|     - Object(s)       | RMA__c                                                                             |
|     - Description     |   - Used to set the Record Type and the Shipping Address on a new RMA              |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | April, 27th 2012                                                                   |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
trigger RMABeforeInsert on RMA__c (Before Insert)
{
    System.Debug('****** RMABeforeInsert Trigger starts ****');  

    if(Utils_SDF_Methodology.canTrigger('AP45'))
    {
       AP45_CreateNewRMA.createNewRMA(trigger.new);
    }

    System.Debug('****** RMABeforeInsert Trigger ends ****');  
}