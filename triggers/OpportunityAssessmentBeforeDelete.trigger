trigger OpportunityAssessmentBeforeDelete on OpportunityAssessment__c (before delete) {
    Set<id> oppAssidset=new Set<id>();
    List<OpportunityAssessment__c> opassList=new List<OpportunityAssessment__c>();
    opassList = Trigger.old;
    
    if(opassList != null && opassList.size()>0){
       AP_OpportunityAssessment_Delete.deleteOpportunityAssessmentResponse(opassList );
    }
}