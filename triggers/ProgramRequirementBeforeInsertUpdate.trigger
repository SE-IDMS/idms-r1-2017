trigger ProgramRequirementBeforeInsertUpdate on ProgramRequirement__c (before insert,before update) {
     if(Utils_SDF_Methodology.canTrigger('AP_PGR_CountryRequirementUpdate'))
    { 
       system.debug('**** calling trigger ***');
        AP_PGR_CountryRequirementUpdate.validateDuplicateProgramRequirement( trigger.new);
    
    }
}