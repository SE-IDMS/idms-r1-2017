trigger WorkDetailBeforeDelete on SVMXC__Service_Order_Line__c (before delete) {
    
    if(Utils_SDF_Methodology.canTrigger('SRV_WD_BD')) {
        
        Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__Service_Order_Line__c;
        Map <String, Schema.RecordTypeInfo> workDetailRecordTypeInfo = dSobjres.getRecordTypeInfosByName();
        System.debug('### Work details recordtype Ids: '+workDetailRecordTypeInfo);
        //String workDetailRecordTypeInfo.get('Usage/Consumption').getRecordTypeId();
        //String workDetailRecordType = '012A0000000nYLzIAM';
        String WorkDetailRecordType = Label.CLSSRV_WDD_RecordType;
        
        List<SVMXC__Service_Order_Line__c> wdToSaveFromDeletion = new List<SVMXC__Service_Order_Line__c>();
        
        for (SVMXC__Service_Order_Line__c wd : trigger.old) {
            if (wd.recordTypeId == workDetailRecordType && wd.SVMXC__Line_Type__c == 'Labor') {
                wdToSaveFromDeletion.add(wd);
            }
        }
        System.debug('### Work details to save from deletion: '+wdToSaveFromDeletion);
        
        List<WorkDetailDeletion__c> wddList = AP_WorkDetailDeletionManager.getWorkDetailsToSaveFromDeletion(wdToSaveFromDeletion);
        insert wddList;
        
		set<Id> idList = new set<Id>();
		for(SVMXC__Service_Order_Line__c wd: trigger.old) {
			idList.add(wd.id);
		}
		ServiceMaxTimesheetUtils.removeTimesheetEntriesFromWorkDetails(idList);
        
        
    }
    
}