/*
    Author          : Mohammad naved
    Date Created    : 30/05/2016
    Description     : Scope country after insert,after update & after delete event trigger.
*/
trigger CountryCISCorrespondentsTeamLinkTrigger on CountryCISCorrespondentsTeamLink__c (Before Insert,Before Update ,After insert,After update,after delete) {
    //Trigger to bypass when not needed.
    if(Utils_SDF_Methodology.canTrigger('CountryCISCorrespondentsTeamLinkTrigger')){
        if((trigger.isInsert||trigger.isUpdate) && trigger.isBefore){
            AP_CountryCisCorrespondentTeamLink.checkDuplicateCountry(trigger.new);     
        }
        //Call for After insert
        if(trigger.isInsert && trigger.isAfter){
            //Call Handler Method
            AP_CountryCisCorrespondentTeamLink.changeContryValue(trigger.new);
        }
        //Call for After Update
        if(trigger.isUpdate && trigger.isAfter){
            AP_CountryCisCorrespondentTeamLink.changeContryValue(trigger.new);
        }
        //Call for After Delete
        if(trigger.isDelete && trigger.isAfter){
            AP_CountryCisCorrespondentTeamLink.changeContryValue(trigger.old);
        }
    }

}