/*
Date Modified       Modified By         Comments
------------------------------------------------------------------------
16-09-2013          Deepak              Logic to Put Validation for no two location can be primary for a Account
*/

trigger LocationBeforeUpdate on SVMXC__Site__c (before update) 
{
    
    //Variablees related to Scenario 1 
    List<SVMXC__Site__c> loc1 = new List<SVMXC__Site__c>();    
    Set<id> locacc = New Set<id>();

  // trigger applies only for standalone users
  if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLNOV13SRV03)) {
         
    
    for(SVMXC__Site__c a:trigger.new)
    {
        if(a.SVMXC__Account__c != Null && 
        (trigger.oldMap.get(a.id).SVMXC__Account__c!=trigger.NewMap.get(a.id).SVMXC__Account__c))              
        {
            loc1.add(a);
            locacc.add(a.SVMXC__Account__c);        
        } 
    }
    System.debug('### LocationBeforeUpdate - Locations that are changing Accounts: '+loc1);
    System.debug('### LocationBeforeUpdate - Target Accounts: '+locacc);
    
/* commented by Nabil ZEGHACHE on April 03rd, 2014    
    list<Account> acc = [Select Id,Name,Street__c,City__c,ZipCode__c,Country__c,Country__r.Name,
            StateProvince__c,StateProvince__r.name From Account Where Id IN :locacc];
                 
    for(Account ac :acc )   
    {
        for(SVMXC__Site__c lo1:loc1)
        {
            if(lo1.SVMXC__Account__c == ac.Id)
            {
                lo1.SVMXC__Street__c = ac.Street__c;
                lo1.SVMXC__City__c = ac.City__c;
                lo1.StateProvince__c = ac.StateProvince__c;
                lo1.SVMXC__Zip__c = ac.ZipCode__c;
                lo1.LocationCountry__c = ac.Country__c;
            }   
        }   
    }*/
    
    // Added by Nabil ZEGHACHE on April 03rd, 2014
    Map<Id, Account> targetAccountsMap = new Map<Id, Account>([Select Id,Name,Street__c,City__c,ZipCode__c,Country__c,Country__r.Name,
            StateProvince__c,StateProvince__r.name From Account Where Id IN :locacc]);
    for (SVMXC__Site__c lo1:loc1) {
        Account targetAccount = (Account) targetAccountsMap.get(lo1.SVMXC__Account__c);
        if (targetAccount != null) {
            lo1.SVMXC__Street__c = targetAccount.Street__c;
            lo1.SVMXC__City__c = targetAccount.City__c;
            lo1.StateProvince__c = targetAccount.StateProvince__c;
            lo1.SVMXC__Zip__c = targetAccount.ZipCode__c;
            lo1.LocationCountry__c = targetAccount.Country__c;
        }
    }

    /*
        Added by: Ramu Veligeti
        Date: 17/07/2013
        Update TECH_IsSVMXRecordPresent__c on Account object if the Account is used in 
        ServiceMax related objects like Work Order, Installed Product, Location, Service Contract etc..
    */
    Set<Id> AccId = new Set<Id>();
    for(SVMXC__Site__c a:trigger.new)
        if(a.SVMXC__Account__c!=null) AccId.add(a.SVMXC__Account__c);
    SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
    
    /*
    Added by Deepak For Oct-13 release.
    Date: 16/09/2013
    //Validation for no two location can be primary for a Account.
    */
    
    List<SVMXC__Site__c> loclist1 = New List<SVMXC__Site__c>();
    List<SVMXC__Site__c> loclist2 = New List<SVMXC__Site__c>();
    set<Id> acid = New set<Id>();
    set<Id> locid = New set<Id>();
    
    for(SVMXC__Site__c loc :trigger.New)
    {   
        //if(loc.PrimaryLocation__c==true)
        if(loc.PrimaryLocation__c==true && (trigger.oldMap.get(loc.id).PrimaryLocation__c!=trigger.NewMap.get(loc.id).PrimaryLocation__c))
        {
            acid.add(loc.SVMXC__Account__c);
            loclist1.add(loc);
             locid.add(loc.Id);         
        }       
    }
    
    //Old code not compliant with bulk approach
    /*loclist2=[Select Id,Name,PrimaryLocation__c, SVMXC__Account__c From SVMXC__Site__c Where SVMXC__Account__c =:acid AND     PrimaryLocation__c =true AND Id not =:locid];
        for(SVMXC__Site__c loc :loclist1)
          {  
          System.debug('### LocationBeforeUpdate - Current location: '+loc.id);
              if(loclist2.size()>0 && loc.SVMXC__Account__c !=null)
                 {      
                        loc.addError('Primary Location for this Account  has been already created');
                        System.debug('### Primary Location for this Account  has been already created: '+loc.id);
                 }
          } 
    */
    
    //New code
    loclist2=[Select Id,Name,PrimaryLocation__c, SVMXC__Account__c From SVMXC__Site__c Where SVMXC__Account__c in :acid AND     PrimaryLocation__c =true AND Id not in :locid];
    System.debug('### List of primary locations that are related to target accounts excluding locations of the trigger: '+loclist2);
    if(loclist2.size()>0) {
        Set<Id> targetAccId = new Set<Id>();
        
        for(SVMXC__Site__c loc :loclist2) {
            targetAccId.add(loc.SVMXC__Account__c);
        }
        for(SVMXC__Site__c loc :loclist1) {
            if (loc.SVMXC__Account__c !=null && targetAccId.contains(loc.SVMXC__Account__c)) {
                loc.addError('Primary Location for this Account ('+loc.SVMXC__Account__c+') has been already created');
                System.debug('### Primary Location for this Account ('+loc.SVMXC__Account__c+') has been already created: '+loc.id);
            }
        }
    }
    
    }//Applies only for standalone users
}