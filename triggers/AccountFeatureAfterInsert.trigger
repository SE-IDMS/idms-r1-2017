/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 31 July 2013
    Description : For October 2013 Release:
                 
                 1. Add the stand-alone feature for Contacts.
                 2. Add Feature REquirements to Account Assigned Requirment for stand-alone Features.
    
********************************************************************************************************************/

trigger AccountFeatureAfterInsert on AccountAssignedFeature__c (after insert) {
    
    map<Id,list<AccountAssignedFeature__c>> mapAccountFeature = new map<Id,list<AccountAssignedFeature__c>>();
    map<Id,list<AccountAssignedFeature__c>> mapFeatureReq = new map<Id,list<AccountAssignedFeature__c>>();
    Id standaloneFeatureRecordType = [Select id from RecordType where developername='StandAloneFeature'and SobjectType ='AccountAssignedFeature__c' limit 1].Id;            
    for(AccountAssignedFeature__c feature : trigger.new)
    {
        if(feature.recordtypeid == standaloneFeatureRecordType )
        {
            if(feature.TECH_AddtoContact__c)
            {
                if(!mapAccountFeature.containsKey(feature.Account__c))
                    mapAccountFeature.put(feature.Account__c , new list<AccountAssignedFeature__c> {(feature)});
                else
                    mapAccountFeature.get(feature.Account__c).add(feature);
                
            }    
            if(!mapFeatureReq.containsKey(feature.featurecatalog__c))
                    mapFeatureReq.put(feature.featurecatalog__c , new list<AccountAssignedFeature__c> {(feature)});
                else
                    mapFeatureReq.get(feature.featurecatalog__c).add(feature);
        }
    }
    
    if(!mapAccountFeature.isEmpty())
        AP_AAF_FeatureUpdateMethods.createContactFeature(mapAccountFeature);
    if(!mapFeatureReq.isEmpty())
        AP_AAF_FeatureUpdateMethods.createFeatureRequirement(mapFeatureReq);
}