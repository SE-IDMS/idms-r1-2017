/********************************************************************************************************************
    Created By : Renjith Jose
    Created Date : 17 March 2014
    Description : For APR14 Release:
                  Calls AP_PartnerPRG_CreateCountryProgram to create Program Level as Registered.
********************************************************************************************************************/
trigger PartnerProgramAfterInsert on PartnerProgram__c (after insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_PartnerPRG'))
    {    
        //START: Release APR14
        AP_PartnerPRG_CountryProgramMethods.createGlobalProgramLevel(trigger.new);
       //END: Release APR14
    }   
}