/*
    Author        : Srikant Joshi 
    Date Created  : 30/07/2013
    Description   : Complaints/Request After Insert trigger.
    
*/
trigger ComplaintsAfterInsert on ComplaintRequest__c (After Insert) {
if(Utils_SDF_Methodology.canTrigger('AP_LCR'))
   {
     //AP_LCR.updateDetectionOrg(trigger.newmap,null); 
     
     // APRIL2014 RELEASE - Sukumar Salla - 12Feb2014
     // To update CommercialReferenceReceived for Technical CRs
      AP_LCR.updateCRReceived(trigger.new);
      // Oct2014 RELEASE
      AP_LCR.createSymptoms(trigger.new);
    //October 2014 Release.     
   //   AP_sendEscalationMails.createXAStakeholderonSH(Trigger.new,'ComplaintRequest__c','IssueOwner__c','ResolutionDueDate__c','AccountableOrganization__c');
   }
   
    /* for(ComplaintRequest__c temprecord:Trigger.new)
            {
                if(temprecord.comments__c == null || temprecord.comments__c == '<br>')
                { 
                      //temprecord.comments__c.addError('Please select comment');
                      temprecord.TECH_IsBlankComment__c=true;
                 
                }
                else {
                    temprecord.TECH_IsBlankComment__c=false;
                }
            }
            */
  
}