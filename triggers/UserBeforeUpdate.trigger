trigger UserBeforeUpdate on User (before Update) {
    
     
    if(Utils_SDF_Methodology.canTrigger('AP_UserActions'))
    {
        AP_UserActions.populateLastActivationDateforUpdate(Trigger.oldMap,Trigger.newMap);        
    }
    
  
    
    for (User u : Trigger.new) {

        User oldUser = Trigger.oldMap.get(u.Id);
        String name=UserInfo.getName();
        if(name =='Partners Site Guest User'){
            if((u.LanguageLocaleKey != oldUser.LanguageLocaleKey) || (u.LocaleSidKey != oldUser.LocaleSidKey))
            {
                u.LanguageLocaleKey.addError('You dont have acess to change these fields : LanguageLocaleKey,LocaleSidKey');
            }
        }
    }

    if(Utils_SDF_Methodology.canTrigger('AP_MatchingModule')){
        
        if(System.isFuture() != True){
            
            List<User> userList = new List<User>();
            List<User> changedUserList = new List<User>();
            Set<ID> rematchIds = new Set<ID>();
            Set<ID> progressiveIds = new Set<ID>();
            
            if(Test.isRunningTest()) {
                AP_MatchingModule.BYPASS_FORCE_L1 = true;
            }
            
            List<String> userFieldList = new List<String>{'CompanyName','IDMS_POBox__c','Email','MobilePhone','Phone','FirstName','LastName',
                                        'IDMS_Email_opt_in__c','Country','IDMS_PreferredLanguage__c','DefaultCurrencyIsoCode',
                                        'Street','City','PostalCode','State','IDMSClassLevel1__c','IDMSClassLevel2__c',
                                        'IDMSAnnualRevenue__c','IDMSTaxIdentificationNumber__c','Company_Website__c','IDMSCompanyMarketServed__c',
                                        'IDMSCompanyNbrEmployees__c','IDMSCompanyHeadquarters__c','IDMSMiddleName__c',
                                        'IDMSSalutation__c','Department','fax','Company_State__c','Company_Address1__c','Company_City__c',
                                                                'Company_Country__c','Company_Postal_Code__c'};
                                        
           for(integer i = 0; i < Trigger.new.size(); i++) {
            
                User oldUser = Trigger.old[i];
                User newUser = Trigger.new[i];
                Boolean isChanged = false;
                
                if(newUser.AccountId != null && newUser.contactid != null) {
                    if(AP_MatchingModule.isLevel2(oldUser)){
                        for(String field:userFieldList) {
                            if(oldUser.get(field) == null && newUser.get(Field) != null) {
                                isChanged = true;
                            }                
                        }
                        
                        if(isChanged) {
                            changedUserList.add(newUser);
                        }
                    }
                }
            }
            
            if(changedUserList.size()!= 0 && changedUserList!= null){
                for(user usr:changedUserList){
                    if(AP_MatchingModule.isLevel2(usr) 
                        && !AP_MatchingModule.FORCE_L1
                        && (usr.IDMS_User_Context__c == 'Work' || usr.IDMS_User_Context__c == '@Work')) {
                        
                        progressiveIds.add(usr.id);    
                    }            
                }
            }
            
            for(User user:Trigger.new) {
                if(user.TECH_MatchAndRelink__c && user.contactid != null
                    && (user.IDMS_User_Context__c == 'Work' || user.IDMS_User_Context__c == '@Work') ) {
                    userList.add(user);
                    rematchIds.add(user.Id); 
                    user.TECH_MatchAndRelink__c = false;
                }
            }   
            
            if(rematchIds.size() > 0){         
                AP_UserMasterDataMethods.matchAndRelink(rematchIds); 
            }
            
            if(progressiveIds.size() > 0){         
                AP_UserMasterDataMethods.UpdateAccountAndContact(progressiveIds); 
            }
        }
        else{
        
            for(User user:Trigger.new) {            
                user.TECH_MatchAndRelink__c = false;
            }
        }
    }
}