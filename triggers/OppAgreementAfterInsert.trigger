/*
  Author : IDC Team
  Created On: 21/01/2011.
  Description: OppAgreement After Insert Event Trigger
*/


trigger OppAgreementAfterInsert  on OppAgreement__c (after insert) {
    System.Debug('****** OppAgreementGrantAgreementManagerAccess Trigger Start ****');
    
    if(Utils_SDF_Methodology.canTrigger('AP04'))
    {  
        List<OppAgreement__c> agreementlist = new List<OppAgreement__c>();
        for(integer i = 0; i < trigger.new.size(); i++)
        {
            if(trigger.new[i].AgreementManager__c !=null)
            { 
              agreementList.add(trigger.new[i]);  
            }
        }
        if(agreementList.size() !=0)
        {
            AP04_AgreementTrigger.grantAccess(agreementList);
        }
    }
    System.Debug('****** OppAgreementGrantAgreementManagerAccess Trigger End****');   
}