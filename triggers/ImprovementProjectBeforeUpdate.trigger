// Added by Uttara - Q3 2016 Release - Notes to bFO (I2P)

trigger ImprovementProjectBeforeUpdate on CSQ_ImprovementProject__c (Before Update) {

    if(Utils_SDF_Methodology.canTrigger('IP1')) { 
             
        Map<String,CSQ_ImprovementProject__c> mapAccOrgIdIPrecord = new Map<String,CSQ_ImprovementProject__c>();
        Map<String,CSQ_ImprovementProject__c> mapIPid1 = new Map<String,CSQ_ImprovementProject__c>();
        Map<String,CSQ_ImprovementProject__c> mapIPid2 = new Map<String,CSQ_ImprovementProject__c>();
        
        for(CSQ_ImprovementProject__c IP : Trigger.New) {
                            
            if(IP.CSQ_AccountableOrganization__c != null && Trigger.oldMap.get(IP.Id).CSQ_AccountableOrganization__c != IP.CSQ_AccountableOrganization__c)
                mapAccOrgIdIPrecord.put(IP.CSQ_AccountableOrganization__c, IP);
            
            if(IP.CSQ_NumberOfMeasures__c > 0 && IP.CSQ_Measure_MilestoneReviewCompleted__c == true)
                mapIPid1.put(IP.Id,IP);
            
            if(IP.CSQ_NumberOfMeasures__c > 0 && IP.CSQ_ControlMilestoneReviewCompleted__c == true)
                mapIPid2.put(IP.Id,IP);
        }
        
        if(!mapAccOrgIdIPrecord.isEmpty())
            AP_ImprovementProject.SetProjectApprover(mapAccOrgIdIPrecord);
            
        if(!mapIPid1.isEmpty() || !mapIPid2.isEmpty())
            AP_ImprovementProject.checkMeasures(mapIPid1, mapIPid2);
    }
}