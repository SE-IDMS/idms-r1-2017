/*
*   Created By: Otmane
*   Created Date: 01/06/2016
*   Modified by:  01/12/2016
*   This Trigger will be Fired after UIMS Company record is updated and 
*   it calls method CompanySyncLogic from IdmsUimsUserSync Class
**/

trigger UIMSCompanyAfterUpdate on UIMS_Company__c (After Update) 
{
    
    if(Utils_SDF_Methodology.canTrigger('UimsCompany')){
        system.debug('UIMSCompanyAfterUpdate started');
        Set<Id> CompanyIds = new Set<Id>();
        for(UIMS_Company__c uimsCompany : trigger.New){ 
            CompanyIds.add(uimsCompany.Id);
        }
        IdmsUimsUserSync uimsUserSync = new IdmsUimsUserSync();
        uimsUserSync.CompanySyncLogic(CompanyIds);
    }
    
}