trigger ScorecardKPITargetBeforeUpdate on Global_KPI_Targets__c (before update) {

System.Debug('****** ScorecardKPITargetBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('ScorecardKPITargetBeforeInsert'))
    {
    ConnectGlobalKPITargetTriggers.ConnectGlobalKPITargetbeforeUpdate(Trigger.new,Trigger.old[0]);
    if((Trigger.new[0].Comments__c != Trigger.old[0].Comments__c) || Trigger.new[0].KPI_Description__c != Trigger.old[0].KPI_Description__c){
     ConnectGlobalKPITargetTriggers.ConnectCascadingKPITargetBeforeUpdate(Trigger.new);
             }        
    }
 System.Debug('****** ScorecardKPITargetBeforeInsert Trigger End ****'); 
 }