trigger PRJ_ProjReqAfterUpdate on PRJ_ProjectReq__c (after update) {

    if(Utils_SDF_Methodology.canTrigger('ProjectRequestTriggerAfterUpdate'))
    {
        PRJ_ProjectTriggers.projectAfterUpdate(Trigger.newMap, Trigger.oldMap);
    }


}