trigger ContractLinkBeforeUpdate on OPP_ContractLink__c (before update) {
    if(Utils_SDF_Methodology.canTrigger('AP52')){
        List<OPP_ContractLink__c>AP52_actionList_1= new List<OPP_ContractLink__c>();
        for(Integer i=0;i<Trigger.new.size();i++)
            if(Trigger.new[i].TECH_ApprovalStep__c != Trigger.old[i].TECH_ApprovalStep__c)
                AP52_actionList_1.add(Trigger.new[i]);
        
        List<OPP_ContractLink__c>AP52_actionList_2= new List<OPP_ContractLink__c>();
        for(Integer i=0;i<Trigger.new.size();i++)
            if(Trigger.new[i].ContractStatus__c != Trigger.old[i].ContractStatus__c && Trigger.new[i].ContractStatus__c == 'Approved' && Trigger.new[i].ActiveContract__c == true)
                AP52_actionList_2.add(Trigger.new[i]);
        
        AP52_ContractLinkFieldUpdate.performLastUserUpdates(Trigger.new);
        
        /**if(AP52_actionList_1.size()>0){
            AP52_ContractLinkFieldUpdate.performCurrentUserUpdates(AP52_actionList_1);
        }*/
         AP52_ContractLinkFieldUpdate.performCurrentUserUpdates(Trigger.new);
        
    }
    
      
}