/*
*   Created By: Otmane Haloui
*   Created Date: 01/06/2016
*   Modified by:  01/12/2016
* This Trigger will be Fired after UIMS User record is updated and 
* it calls method UserSyncLogic from IdmsUimsUserSync Class
**/
trigger UIMSUserAfterUpdate on UIMS_User__c (After Update) {
    
    if(Utils_SDF_Methodology.canTrigger('UimsUser')){
        Boolean UpdateComeFromLogin;
        Boolean isConvertedNew;
        Boolean isConvertedOld;
        
        for(UIMS_User__c u :trigger.new){
            UpdateComeFromLogin = false;         
            isConvertedNew = trigger.newMap.get(u.id).isConvertedToIdmsUser__c;
            isConvertedOld = trigger.oldMap.get(u.id).isConvertedToIdmsUser__c;
            system.debug('isConvertedOld: '+isConvertedOld + 'isConvertedNew: '+isConvertedNew); 
        }
        
        //check if the update has been fired from the user creation on the fly
        if(isConvertedNew == TRUE && isConvertedOld != isConvertedNew){
            UpdateComeFromLogin = TRUE;
        }
        //Trigger uims sync logic only when the record got updated from a flow different than Login
        if(!UpdateComeFromLogin){    
            Set<Id> uimsUserIds = new Set<Id>();
            for(UIMS_User__c uimsUser : trigger.New){ 
                uimsUserIds.add(uimsUser.Id);
            }
            IdmsUimsUserSync uimsUserSync = new IdmsUimsUserSync();
            uimsUserSync.UserSyncLogic(uimsUserIds);
        }
    }
}