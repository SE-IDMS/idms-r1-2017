/*
    Author          : Pooja Suresh
    Description     : Contact Before delete event trigger.
                      
*/
trigger ContactBeforeDelete on Contact (before delete) 
{    
    System.Debug('****** ContactBeforedelete Trigger Start ****');  
    if(Utils_SDF_Methodology.canTrigger('AP_ContactBeforeDeleteHandler'))
    {    
        AP_ContactBeforeDeleteHandler handler = new AP_ContactBeforeDeleteHandler();
        handler.OnBeforedelete(Trigger.old);
    }
    System.Debug('****** ContactBeforedelete Trigger End ****');      
}