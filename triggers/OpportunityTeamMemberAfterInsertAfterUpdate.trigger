trigger OpportunityTeamMemberAfterInsertAfterUpdate on OpportunityTeamMember (after insert,after update) {
    
    if(Utils_SDF_Methodology.canTrigger('AP_OpportunityTeamMember'))
    {
        AP_OpportunityTeamMember.updateTECH_WorkingPartner(trigger.old,trigger.new);
     
    }   
}