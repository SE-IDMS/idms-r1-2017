trigger QuoteLinkBeforeUpdate on OPP_QuoteLink__c (before update) {
System.debug('****** QuoteLinkBeforeUpdate start *****');
if(!VFC20_CreateSeries.doNotProcessQLKS){
System.debug('****** Not bypassing QuoteLinkBeforeUpdate  *****');
    if(Utils_SDF_Methodology.canTrigger('AP02')){
        List<OPP_QuoteLink__c>AP02_actionList_1= new List<OPP_QuoteLink__c>();
        List<OPP_QuoteLink__c> updateAgreementIds = new List<OPP_QuoteLink__c>();
        for(Integer i=0;i<Trigger.new.size();i++)
        {
            if(Trigger.new[i].TECH_ApprovalStep__c != Trigger.old[i].TECH_ApprovalStep__c)
                AP02_actionList_1.add(Trigger.new[i]);
            
            //Populates Agreement field with TECH_AgreementId__c - Modified for BR-6915
            if(trigger.new[i].TECH_AgreementId__c!=null)
                updateAgreementIds.add(trigger.new[i]);
        }
        List<OPP_QuoteLink__c>AP02_actionList_2= new List<OPP_QuoteLink__c>();
        for(Integer i=0;i<Trigger.new.size();i++)
            if(Trigger.new[i].QuoteStatus__c != Trigger.old[i].QuoteStatus__c && Trigger.new[i].QuoteStatus__c == 'Approved' && Trigger.new[i].ActiveQuote__c == true)
                AP02_actionList_2.add(Trigger.new[i]);
        
        AP02_QuoteLinkFieldUpdate.performLastUserUpdates(Trigger.new);

        //Modified for BR-6915
        AP02_QuoteLinkFieldUpdate.updateAgreement(updateAgreementIds);
        
        if(AP02_actionList_1.size()>0){
            AP02_QuoteLinkFieldUpdate.performCurrentUserUpdates(AP02_actionList_1);
        }
        if(AP02_actionList_2.size()>0){
            AP02_QuoteLinkFieldUpdate.performOppUpdates(AP02_actionList_2);
        }
    }
    
    if(Utils_SDF_Methodology.canTrigger('AP21') && !AP11_UpdateQuoteLink.updateQlkFromOpp ){ // check if the trigger is called from a Opp before update trigger
        system.debug('#####Trigger updateTECH_HasActiveQLKwthMargin starts');                // in order to not call it recursively
        AP21_UpdateOpportunity.updateTECH_HasActiveQLKwthMargin(Trigger.new, Label.CL00751);
        system.debug('#####Trigger updateTECH_HasActiveQLKwthMargin ends');
    }
     
  /*   if(Utils_SDF_Methodology.canTrigger('AP_QuoteLinkError'))
    {
        system.debug('#####Trigger AP_QuoteLinkError starts');
        List<Opp_QuoteLink__c> processQuoteLinks=new List<Opp_QuoteLink__c>();
        
        for(Opp_QuoteLink__c qtlnk:Trigger.New)        
            if(qtlnk.ActiveQuote__c)
                processQuoteLinks.add(qtlnk);
        if(processQuoteLinks.size()>0)
            AP_QuoteLinkError.checkQuoteLink(processQuoteLinks);
        system.debug('#####Trigger AP_QuoteLinkError ends');
    } */  
    }
   System.debug('****** QuoteLinkBeforeUpdate end *****');
}