trigger TeamCaseJunctionAfterUpdate on TeamCaseJunction__c (After Update) {

    //if(Utils_SDF_Methodology.canTrigger('AP_TeamCaseJunction'))
    {
        System.debug('TriggerStart');
        AP_TeamCaseJunction.DefaultSupportedCaseClassifications(Trigger.NewMap,Trigger.OldMap);
        System.debug('TriggerEnd');
    }

}