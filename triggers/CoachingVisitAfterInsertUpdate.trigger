/*
    Author          : Ramesh Rajasekaran - Global Delivery team 
    Date Created    : 11/11/2011
    Description     : Coaching Visit - after insert,after update event trigger.
*/
trigger CoachingVisitAfterInsertUpdate on CoachingVisit__c (after insert,after update) {
	
	System.Debug('****** CoachingVisitAfterInsertUpdate Trigger Start ****');
	
	if(Utils_SDF_Methodology.canTrigger('AP32')){		
		List<CoachingVisit__c> coachingVisit = new List<CoachingVisit__c>();
        for(CoachingVisit__c cv:trigger.new)
        {
            coachingVisit.add(cv);
        }
        if(coachingVisit.size()>0)    
        {
            AP32_UpdateCoachingCustomerVisitEvent.updateCoachingCustomerEvent(coachingVisit);
        }		 
	}
	
	System.Debug('****** CoachingVisitAfterInsertUpdate Trigger End ****');

}