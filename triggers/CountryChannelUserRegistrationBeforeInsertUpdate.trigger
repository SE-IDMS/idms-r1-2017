trigger CountryChannelUserRegistrationBeforeInsertUpdate on CountryChannelUserRegistration__c (before insert, before update) {

	if (Utils_SDF_Methodology.canTrigger('AP_CountryChannelUserRegistration')) {
		AP_CountryChannelUserRegistration.validateDuplicateUserRegField(trigger.new);
   	}
}