//*********************************************************************************
// Trigger Name     : preventiveActionAfterUpdate
// Purpose          : Preventive  Action After Update event trigger
// Created by       : Global Delivery Team
// Date created     : 27th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/

trigger preventiveActionrAfterUpdate on PreventiveAction__c (after update) {
    System.Debug('****** preventiveActionrAfterUpdate Trigger Start ****');      
    
    if(Utils_SDF_Methodology.canTrigger('AP1004')){              
        Map<Id,PreventiveAction__c> prvtveActnOldMap = new Map<Id,PreventiveAction__c>(); 
        Map<Id,PreventiveAction__c> prvtveActnNewMap = new Map<Id,PreventiveAction__c>(); 
        
        for(PreventiveAction__c prvtveActn: Trigger.new){
            if(Trigger.oldMap.get(prvtveActn.id).Owner__c != Trigger.newMap.get(prvtveActn.id).Owner__c)
                prvtveActnOldMap.put(prvtveActn.id,Trigger.oldMap.get(prvtveActn.id));
                prvtveActnNewMap.put(prvtveActn.id,Trigger.newMap.get(prvtveActn.id));
        }
        
        if(prvtveActnOldMap.size()>0 && prvtveActnNewMap.size()>0){ 
            AP1004_PreventiveAction.updatePreventiveOwnerToProblemShare(prvtveActnOldMap,prvtveActnNewMap); 
        }                  
    }                                               
    System.Debug('****** preventiveActionrAfterUpdate Trigger End ****'); 
}