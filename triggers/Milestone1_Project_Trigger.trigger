trigger Milestone1_Project_Trigger on Milestone1_Project__c (before update, before delete, before insert ) {
        set<string> setUserId = new set<string>();
        
        if( Trigger.isUpdate ){
            //TODO can we delete this?
            Milestone1_Project_Trigger_Utility.handleProjectUpdateTrigger(trigger.new);
            
             for(Milestone1_Project__c mpj:trigger.new) {
                 if(mpj.Send_activation_reminder_email__c==false) {
                     mpj.TECH_SendEmailCountrySellDate__c=null;
                 }
             //  if(mpj.Country_Announcement_Date__c!=null) {
               if(mpj.TECH_CalculateSendEmailCountrySellDate__c ==true) {
                   If(mpj.TECH_SendEmailCountrySellDate__c!=null) {
                    mpj.TECH_SendEmailCountrySellDate__c=Date.today().adddays(integer.valueof(label.CLOCT14COLAbFO22));
                    }
                    mpj.TECH_ResetSendCountrySellDate__c=true;
                }
                 if(mpj.Send_activation_reminder_email__c) {
                   
                    mpj.TECH_SendEmailCountrySellDate__c=Date.today().adddays(integer.valueof(label.CLOCT14COLAbFO22));
                    mpj.TECH_ResetSendCountrySellDate__c=true;
                }
               //}
            }
            
        } 
        else if( Trigger.isDelete ) {
            //cascades through milestones
            Milestone1_Project_Trigger_Utility.handleProjectDeleteTrigger(trigger.old);
        }
        else if( Trigger.isInsert ) {
            //checks for duplicate names
            Milestone1_Project_Trigger_Utility.handleProjectInsertTrigger( trigger.new );
           
            for(Milestone1_Project__c mpj:trigger.new) {
                if(mpj.Send_activation_reminder_email__c==false) {
                    mpj.TECH_SendEmailCountrySellDate__c=null;
                }
                if(mpj.Send_activation_reminder_email__c) {
                    mpj.TECH_SendEmailCountrySellDate__c=Date.today().adddays(integer.valueof(label.CLOCT14COLAbFO22));
                }
            }
        }
       
        
    
    //April 15 Release
        
        if(trigger.isInsert && Trigger.isBefore) {
    
            for(Milestone1_Project__c trgObj:trigger.new ) {
                
                // trgObj.BoxUserEmailId__c =trgObj.Launch_Owner__r.email; 
                if(trgObj.Country_Launch_Leader__c!=null  && trgObj.BoxUserEmailId__c==null) {
                    setUserId.add(trgObj.Country_Launch_Leader__c);
                } else if(trgObj.Country_Withdrawal_Leader__c!=null && trgObj.BoxUserEmailId__c==null) {
                    
                    setUserId.add(trgObj.Country_Withdrawal_Leader__c);
                    
                }else if(trgObj.Country_Launch_Leader__c==null && trgObj.Region_Introduction_Owner__c!=null && trgObj.Country_Withdrawal_Leader__c==null) {
                    setUserId.add(trgObj.Region_Introduction_Owner__c);
                } else if(trgObj.Country_Launch_Leader__c==null && trgObj.Region_Introduction_Owner__c==null && trgObj.Country_Withdrawal_Leader__c==null && trgObj.RegionWithdrawalManager__c!=null) {
                    setUserId.add(trgObj.RegionWithdrawalManager__c);
                         
                }
    
            }
              
           Map<id,User> mapUser =new Map<id,User>([select id ,email from user where id =:setUserId]);
            if(mapUser.size() > 0) {
                for(Milestone1_Project__c mpObj:trigger.new) {
                    
                    if(mpObj.Country_Launch_Leader__c!=null) {
                        mpObj.BoxUserEmailId__c=mapUser.get(mpObj.Country_Launch_Leader__c).email;
                         //mpObj.ownerid=mapUser.get(mpObj.Country_Launch_Leader__c).id;
                    } else if(mpObj.Country_Withdrawal_Leader__c!=null) {                
                        mpObj.BoxUserEmailId__c=mapUser.get(mpObj.Country_Withdrawal_Leader__c).email;
                         //mpObj.ownerid=mapUser.get(mpObj.Country_Withdrawal_Leader__c).id;
                    } else if(mpObj.Region_Introduction_Owner__c!=null) {
                        mpObj.BoxUserEmailId__c=mapUser.get(mpObj.Region_Introduction_Owner__c).email;
                    }else if(mpObj.RegionWithdrawalManager__c!=null) {
                        mpObj.BoxUserEmailId__c=mapUser.get(mpObj.RegionWithdrawalManager__c).email;
                        
                    }
            
                }
           
            } 
    }
   

    
    
    }