/*     
@Hari
Created-01-06-14(Release)
Purpose Logic to check Duplicates in Service Role 
*/
trigger ServiceRoleBeforeInsert on Role__c (before insert) {

    if(Utils_SDF_Methodology.canTrigger('ServiceRoleBeforeInsert')){    
        List<Role__c> SerivceRoleList = new List<Role__c>();
        for(Role__c obj:Trigger.new){
            if(obj.InstalledProduct__c  != null && obj.RecordTypeId == System.Label.CLJUL14SRV01){
            
                SerivceRoleList.add(obj);
            
            }
        }
        if(SerivceRoleList != null && SerivceRoleList.size()>0){
            AP_IsSVMXRelatedContact.CheckDuplicateRole(SerivceRoleList ,'INSERT');
        }
    }

}