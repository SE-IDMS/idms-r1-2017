trigger LocationAfterInsert on SVMXC__Site__c (after insert){
    
    
    set<id>accset= new set<id>();
    set<id>locset= new set<id>();
    //Trigger bypass
    //if(Utils_SDF_Methodology.canTrigger('SVMX21')){ 
  
  // trigger applies only for standalone users
  if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLNOV13SRV03)){
    List<ID> locationIds = new List<ID>();
    
    for(SVMXC__Site__c  site:trigger.new)
    {
    
      if(site.timeZone__c == null){
        locationIds.add(site.id); 
      }
      
      if(site.PrimaryLocation__c==true && site.SVMXC__Parent__c == null && site.SVMXC__Account__c!=null)
      {
            accset.add(site.SVMXC__Account__c);
            locset.add(site.id);
      }
    }
    
    if(accset!=null)
    {
        Ap_sitelocation.VIPCertificaion(accset,locset);
    
    }
    
    if(locationIds.size() > 0){
      // @future call 
      LocationHandler.calculateTimeZoneAsynchronously(locationIds);
    }
  }
 //}
}