/*
    Author          : Renjith Jose(GD ) 
    Date Created    : 22 August 2013
    Release         : OCT13 
    Description     : Automatically Close the Case when all the associated 
                      Work Order Notifications are Closed or Cancelled                      
    -------------------------------------------------------------------------------
*/

trigger WorkOrderNotificationAfterUpdate on WorkOrderNotification__c (after update) 
{
    //BR-2458
    if(Utils_SDF_Methodology.canTrigger('AP08')) {
        AP08_CaseStatusUpdate.CaseAutoClosure(trigger.new,trigger.OldMap);
    }

}