/*
    Author          : Nicolas Palitzyne (Accenture)
    Date Created    : 17/11/2011
    Description     : External Reference After Insert Trigger
*/

trigger ExternalReferenceAfterInsert on CSE_ExternalReferences__c (After Insert)
{
    if(Utils_SDF_Methodology.canTrigger('AP33'))
    {
        AP33_UpdateAnswerToCustomer.updateRelatedCases(trigger.new);
    }    
}