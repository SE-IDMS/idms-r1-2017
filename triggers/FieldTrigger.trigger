trigger FieldTrigger on Field__c (before insert,before update) {
    Map<Id,Field__c> newfields = Trigger.NewMap;
    Map<Id,Field__c> oldfields = Trigger.OldMap;
    Map<Id,Id> steptoappidmap = new Map<Id,Id>();    
    for(Field__c field: Trigger.New){
        if(Trigger.isUpdate && oldfields.get(field.Id).step__c!=field.step__c){
            steptoappidmap.put(field.step__c,null);
        }
        if(Trigger.isInsert){
            steptoappidmap.put(field.step__c,null);
        }
        if(field.fieldtype__c=='Reference' && field.fields__c!=null && field.fields__c.contains(';')){
            field.addError('Please select only one value in the fields for Reference type');
        }        
    }
    for(Step__c step:[select Id,app__c from Step__c where id in :steptoappidmap.keySet()])
    {
        steptoappidmap.put(step.Id,step.app__c);
    }
    for(Field__c field:Trigger.New){
        if(Trigger.isUpdate && oldfields.get(field.Id).step__c!=field.step__c){
            field.app__c=steptoappidmap.get(field.step__c);
        }
        if(Trigger.isInsert){
            field.app__c=steptoappidmap.get(field.step__c);
        }       
    }
    List<Id> appids = steptoappidmap.values();
    if(Trigger.isInsert)
{

    List<Field__c> existingfields = [select appdatafieldmap__c,id,app__c,fieldtype__c from Field__c where App__c in :appids];
    Map<Id,List<Field__c>> apptofieldsmap = new Map<Id,List<Field__c>>();
    Map<Id,Map<String,Integer>> apptofieldcounter = new Map<Id,Map<String,Integer>>();
    for(Field__c existingfield:existingfields){
        if(apptofieldsmap.containsKey(existingfield.app__c))
            apptofieldsmap.get(existingfield.app__c).add(existingfield);
        else
        {
            List<Field__c> fields = new List<Field__c>();
            fields.add(existingfield);
            apptofieldsmap.put(existingfield.app__c,fields);
        }
    }
    for(Id appid:apptofieldsmap.keySet())
    {
        apptofieldcounter.put(appid,new Map<String,Integer>());
        for(Field__c existingfield:apptofieldsmap.get(appid))
        {
            if(existingfield.fieldtype__c=='Picklist' || existingfield.fieldtype__c=='Text' || existingfield.fieldtype__c=='Lookup' || existingfield.fieldtype__c=='Reference' || existingfield.fieldtype__c=='File' || existingfield.fieldtype__c=='Multiselect Picklist' )
            {                
                if(apptofieldcounter.get(appid).containsKey('text'))
                    apptofieldcounter.get(appid).put('text',apptofieldcounter.get(appid).get('text')+1);
                else
                    apptofieldcounter.get(appid).put('text',1);                    
            }
            else if(existingfield.fieldtype__c=='Number')
            {
                if(apptofieldcounter.get(appid).containsKey('number'))
                    apptofieldcounter.get(appid).put('number',apptofieldcounter.get(appid).get('number')+1);
                else
                    apptofieldcounter.get(appid).put('number',1);  
            }
            else if(existingfield.fieldtype__c=='Date')
            {
                if(apptofieldcounter.get(appid).containsKey('date'))
                    apptofieldcounter.get(appid).put('date',apptofieldcounter.get(appid).get('date')+1);                    
                else
                    apptofieldcounter.get(appid).put('date',1);  
            }
            else if(existingfield.fieldtype__c=='Date/Time')
            {
                if(apptofieldcounter.get(appid).containsKey('datetime'))
                    apptofieldcounter.get(appid).put('datetime',apptofieldcounter.get(appid).get('datetime')+1);                    
                else
                    apptofieldcounter.get(appid).put('datetime',1);  
            }
            else if(existingfield.fieldtype__c=='Currency')
            {
                if(apptofieldcounter.get(appid).containsKey('currency'))
                    apptofieldcounter.get(appid).put('currency',apptofieldcounter.get(appid).get('currency')+1);                    
                else
                    apptofieldcounter.get(appid).put('currency',1);  
            }
            else if(existingfield.fieldtype__c=='Checkbox')
            {
                if(apptofieldcounter.get(appid).containsKey('checkbox'))
                    apptofieldcounter.get(appid).put('checkbox',apptofieldcounter.get(appid).get('checkbox')+1);                    
                else
                    apptofieldcounter.get(appid).put('checkbox',1);
            }
            else if(existingfield.fieldtype__c=='Textarea')
            {
                if(apptofieldcounter.get(appid).containsKey('richtext'))
                    apptofieldcounter.get(appid).put('richtext',apptofieldcounter.get(appid).get('richtext')+1);                    
                else
                    apptofieldcounter.get(appid).put('richtext',1);
            }
        }
    }

    for(Field__c newfield:Trigger.New)
    {            
       Id appid = newfield.app__c;
       if(newfield.fieldtype__c=='Picklist' || newfield.fieldtype__c=='Text' || newfield.fieldtype__c=='Lookup' || newfield.fieldtype__c=='Reference' || newfield.fieldtype__c=='File' || newfield.fieldtype__c=='Multiselect Picklist' )
       {                
            if(apptofieldcounter.containsKey(appid))
            {
                if(apptofieldcounter.get(appid).containsKey('text'))
                {
                    apptofieldcounter.get(appid).put('text',apptofieldcounter.get(appid).get('text')+1);
                    newfield.appdatafieldmap__c='textfield'+apptofieldcounter.get(appid).get('text')+'__c';
                }
                else
                {
                    apptofieldcounter.get(appid).put('text',1);
                    newfield.appdatafieldmap__c='textfield'+apptofieldcounter.get(appid).get('text')+'__c';
                }                        
            }
            else
            {
                apptofieldcounter.put(newfield.app__c,new Map<String,Integer>());
                apptofieldcounter.get(appid).put('text',1); 
                newfield.appdatafieldmap__c='textfield'+apptofieldcounter.get(appid).get('text')+'__c';
            }
        }
        else if(newfield.fieldtype__c=='Number')
        {
            if(apptofieldcounter.containsKey(appid))
            {
                if(apptofieldcounter.get(appid).containsKey('number'))
                {
                    apptofieldcounter.get(appid).put('number',apptofieldcounter.get(appid).get('number')+1);
                    newfield.appdatafieldmap__c='numberfield'+apptofieldcounter.get(appid).get('number')+'__c';
                }
                else
                {
                    apptofieldcounter.get(appid).put('number',1);
                    newfield.appdatafieldmap__c='numberfield'+apptofieldcounter.get(appid).get('number')+'__c';
                }                        
            }
            else
            {
                apptofieldcounter.put(newfield.app__c,new Map<String,Integer>());
                apptofieldcounter.get(appid).put('number',1); 
                newfield.appdatafieldmap__c='numberfield'+apptofieldcounter.get(appid).get('number')+'__c';
            }
        }
        else if(newfield.fieldtype__c=='Date')
        {
            if(apptofieldcounter.containsKey(appid))
            {
                if(apptofieldcounter.get(appid).containsKey('date'))
                {
                    apptofieldcounter.get(appid).put('date',apptofieldcounter.get(appid).get('date')+1);
                    newfield.appdatafieldmap__c='datefield'+apptofieldcounter.get(appid).get('date')+'__c';
                }
                else
                {
                    apptofieldcounter.get(appid).put('date',1);
                    newfield.appdatafieldmap__c='datefield'+apptofieldcounter.get(appid).get('date')+'__c';
                }                        
            }
            else
            {
                apptofieldcounter.put(newfield.app__c,new Map<String,Integer>());
                apptofieldcounter.get(appid).put('date',1); 
                newfield.appdatafieldmap__c='datefield'+apptofieldcounter.get(appid).get('date')+'__c';
            }
        }
        else if(newfield.fieldtype__c=='Date/Time')
        {
            if(apptofieldcounter.containsKey(appid))
            {
                if(apptofieldcounter.get(appid).containsKey('datetime')){
                    apptofieldcounter.get(appid).put('datetime',apptofieldcounter.get(appid).get('datetime')+1);
                    newfield.appdatafieldmap__c='datetimefield'+apptofieldcounter.get(appid).get('datetime')+'__c';
                }
                else{
                    apptofieldcounter.get(appid).put('datetime',1);
                    newfield.appdatafieldmap__c='datetimefield'+apptofieldcounter.get(appid).get('datetime')+'__c';
                }                        
            }
            else
            {
                apptofieldcounter.put(newfield.app__c,new Map<String,Integer>());
                apptofieldcounter.get(appid).put('datetime',1);
                newfield.appdatafieldmap__c='datetimefield'+apptofieldcounter.get(appid).get('datetime')+'__c';
            } 
        }
        else if(newfield.fieldtype__c=='Currency')
        {
            if(apptofieldcounter.containsKey(appid))
            {
                if(apptofieldcounter.get(appid).containsKey('currency')){
                    apptofieldcounter.get(appid).put('currency',apptofieldcounter.get(appid).get('currency')+1);
                    newfield.appdatafieldmap__c='currencyfield'+apptofieldcounter.get(appid).get('currency');
                }
                else{
                    apptofieldcounter.get(appid).put('currency',1);
                    newfield.appdatafieldmap__c='currencyfield'+apptofieldcounter.get(appid).get('currency')+'__c';
                }                        
            }
            else
            {
                apptofieldcounter.put(newfield.app__c,new Map<String,Integer>());
                apptofieldcounter.get(appid).put('currency',1);
                newfield.appdatafieldmap__c='currencyfield'+apptofieldcounter.get(appid).get('currency')+'__c';
            }
        }
        else if(newfield.fieldtype__c=='Checkbox')
        {
            if(apptofieldcounter.containsKey(appid))
            {
                if(apptofieldcounter.get(appid).containsKey('checkbox'))
                {
                    apptofieldcounter.get(appid).put('checkbox',apptofieldcounter.get(appid).get('checkbox')+1);
                    newfield.appdatafieldmap__c='checkboxfield'+apptofieldcounter.get(appid).get('checkbox')+'__c';
                }
                else
                {
                    apptofieldcounter.get(appid).put('checkbox',1);
                    newfield.appdatafieldmap__c='checkboxfield'+apptofieldcounter.get(appid).get('checkbox')+'__c';
                }                        
            }
            else
            {
                apptofieldcounter.put(newfield.app__c,new Map<String,Integer>());
                apptofieldcounter.get(appid).put('checkbox',1); 
                newfield.appdatafieldmap__c='checkboxfield'+apptofieldcounter.get(appid).get('checkbox')+'__c';
            }
        }
        
        else if(newfield.fieldtype__c=='Textarea')
        {
            if(apptofieldcounter.containsKey(appid))
            {
                if(apptofieldcounter.get(appid).containsKey('richtext'))
                {
                    apptofieldcounter.get(appid).put('richtext',apptofieldcounter.get(appid).get('richtext')+1);
                    newfield.appdatafieldmap__c='richtextfield'+apptofieldcounter.get(appid).get('richtext')+'__c';
                }
                else
                {
                    apptofieldcounter.get(appid).put('richtext',1);
                    newfield.appdatafieldmap__c='richtextfield'+apptofieldcounter.get(appid).get('richtext')+'__c';
                }                        
            }
            else
            {
                apptofieldcounter.put(newfield.app__c,new Map<String,Integer>());
                apptofieldcounter.get(appid).put('richtext',1); 
                newfield.appdatafieldmap__c='richtextfield'+apptofieldcounter.get(appid).get('richtext')+'__c';
            }
        }
    }

}      
}