/*
    Author          : Accenture Team (SFDC) 
    Date Created    : 08/08/2011
    Description     : Account Before insert event trigger.
*/
trigger AccountBeforeInsert on Account (before Insert) {
    
    System.Debug('****** AccountBeforeInsert Trigger Start ****');      
    if (Utils_SDF_Methodology.canTrigger('AP_AccountBeforeInsertHandler')) {
        AP_AccountBeforeInsertHandler handler = new AP_AccountBeforeInsertHandler();
        handler.OnBeforeInsert(Trigger.new,Trigger.newMap);
    }
    System.Debug('****** AccountBeforeInsert Trigger End ****');      
/*

    if(Utils_SDF_Methodology.canTrigger('AP30')){
    List<Account> accounts = new List<Account>();
     List<Account> populateLB = new List<Account>();
        for(Account acc:trigger.new)
        {
            accounts.add(acc);
            if(acc.LeadingBusiness__c == null){
                populateLB.add(acc);
            }
        }
        if(accounts.size()>0)    
        {
            AP30_UpdateAccountOwner.populateAccountOwner(accounts);
        }
        if(populateLB.size()>0)    
        {
            AP30_UpdateAccountOwner.populateAccountLB(populateLB);
        }
    }
    if(Utils_SDF_Methodology.canTrigger('AP_AccountRecordUpdate'))
    {
        AP_AccountRecordUpdate.updateAccountType(Trigger.New);
    }
    if(Utils_SDF_Methodology.canTrigger('AP_ACCCreateWithSEAccountId'))
    {
        AP_ACCCreateWithSEAccountId.checkSEAccountId(Trigger.New);
    }
*/
}