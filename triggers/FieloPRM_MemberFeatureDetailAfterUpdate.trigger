trigger FieloPRM_MemberFeatureDetailAfterUpdate on FieloPRM_MemberFeatureDetail__c (after update) {
    FieloPRM_AP_MemberFeatureDetailTriggers.memberFeatureActivatesInactivates(Trigger.New, Trigger.oldMap);
}