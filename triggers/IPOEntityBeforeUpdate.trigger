/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 13-Dec-2012
    Description     : IPO Entity Progress Update event trigger.
*/


trigger IPOEntityBeforeUpdate on IPO_Strategic_Entity_Progress__c (before update) {

  System.Debug('****** IPOEntityBeforeUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('EntityBeforeUpdate'))
    {
        
        //ConnectCascadingMilestoneTriggers.CascadingMilestoneBeforeUpdate_AppendTextColoumns(Trigger.new);
     if((Trigger.new[0].Entity__c != Trigger.old[0].Entity__c) || (Trigger.new[0].Scope__c != Trigger.old[0].Scope__c)){
                  IPOEntityTriggers.IPOEntityBeforeInsertUpdate_ValidateEntityRecord(Trigger.new);
                  IPOEntityTriggers.IPOEntityBeforeInsertUpdate_ValidateScopeEntity(Trigger.new);
                  
                   if(Trigger.new[0].ValidateEntityInsertUpdate__c == true)
                
                    Trigger.new[0].Name.AddError(Label.EntityValidateERR);
                    
           if (Trigger.new[0].ValidateScopeInsertUpdate__c == True)
        
                   Trigger.new[0].Name.AddError(Label.Connect_EntityProgress_InsertUpdate_ERR1);
                   }
 }
 System.Debug('****** IPOEntityBeforeUpdate Trigger End ****'); 
}