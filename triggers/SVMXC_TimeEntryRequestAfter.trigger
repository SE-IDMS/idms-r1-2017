trigger SVMXC_TimeEntryRequestAfter on SVMXC_Time_Entry_Request__c (after delete, after undelete, after update, after insert) {

    if (trigger.isInsert || trigger.isUpdate) {
        SVMXC_TimeEntryRequestAfter.submitTimeEntryRequestForApproval(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
        SVMXC_TimeEntryRequestAfter.checkForTimeEntryRequestSubmission(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
    }
    
    if (trigger.isUpdate) {     
        SVMXC_TimeEntryRequestAfter.checkForTimeEntryRequestRejection(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
        SVMXC_TimeEntryRequestAfter.checkForTimeEntryRequestApproval(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
    }
}