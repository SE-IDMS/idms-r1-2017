/********************************************************************************************************************
    Created By : Vimal Karunakaran
    Description : For Oct 14  CCC Release:
                 1. Calls AP_Case_CaseHandler.updateLastActivityDate to update the Last Activity Date from CaseRelated List
********************************************************************************************************************/
trigger ExpertInternalCommentAfterInsert on ExpertInternalComment__c (after insert) {
	System.Debug('****** ExpertInternalCommentAfterInsert  Trigger Started ****');
	
	if(Utils_SDF_Methodology.canTrigger('AP_Case_CaseHandler') || Test.isRunningTest()){
		Set<Id> setCaseIds = new Set<Id>();
		for(ExpertInternalComment__c objExpertInternalComment:Trigger.New){
			if(objExpertInternalComment.Case__c!=null){
				setCaseIds.add(objExpertInternalComment.Case__c);
			}
		}
		if(setCaseIds.size()>0){
			AP_Case_CaseHandler.updateLastActivityDate(setCaseIds);
		}
	}
	System.Debug('****** ExpertInternalCommentAfterInsert  Trigger Finished  ****');
}