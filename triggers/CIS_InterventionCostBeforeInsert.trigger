//@Author:- Ashish Sharma SESA340747
//@Date:- 21/08/2014
//@Description:- This trigger is written to assign  Cost Per Unit from Country List price Object based upon ISR EU country ,Field of activity and
//Intervention Cost's Cost type field.

trigger CIS_InterventionCostBeforeInsert on CIS_InterventionCosts__c(before insert) {
   if(Test.isRunningTest() || Utils_SDF_Methodology.canTrigger('AP_CIS_InterventionCostHandler')){   
   //Call beforeCISInterventionCostInsert from AP_CIS_InterventionCostTriggerHandler handler class.
  
   AP_CIS_InterventionCostHandler.beforeCISInterventionCostInsert(trigger.new);
   
   } 
}