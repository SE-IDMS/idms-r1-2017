trigger AssessmentBeforeAction on Assessment__c(before insert,before update){
    if(Utils_SDF_Methodology.canTrigger('AP_AssessmentHandler')) {
        AP_AssessmentHandler.checkDuplicateAssessments(Trigger.new,Trigger.oldMap);
    }
}