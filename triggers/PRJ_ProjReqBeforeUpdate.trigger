trigger PRJ_ProjReqBeforeUpdate on PRJ_ProjectReq__c (before update) {

     if(Utils_SDF_Methodology.canTrigger('ProjectRequestTriggerBeforeUpdate'))
     {    
        PRJ_ProjectTriggers.projectBeforeUpdate(Trigger.newMap, Trigger.oldMap);
     }

}