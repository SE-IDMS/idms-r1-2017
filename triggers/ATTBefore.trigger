trigger ATTBefore on AssignedToolsTechnicians__c (before insert) {
     ATTBefore.setSitesTokenOnInsert(trigger.New, trigger.isInsert);
}