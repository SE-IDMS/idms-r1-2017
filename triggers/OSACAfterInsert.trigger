/*
Oct 15 Release
Divya M
BR-7369
*/
trigger OSACAfterInsert on OSACMeeting__c (after Insert) {
    if(Utils_SDF_Methodology.canTrigger('OSACAfterInsert')){   
    AP_OSACMeeting.UpdatePRBSafetyRiskDecesion(trigger.new,trigger.new);
        list<OSACMeeting__c> lstOsac = new list<OSACMeeting__c>();
        Set<Id> setPrbId = new Set<Id>();
        Map<String,String> mapOSACPrb = new Map<String,String>();
        
        for(OSACMeeting__c osac : trigger.new){
            if(osac.Problem__c != null || osac.Problem__c != ''){
                lstOsac.add(osac);
            }
            
            // Added by Uttara - OCT 2015 Release - BR-6646
            if(osac.Decision__c == 'Go') {
                        
                setPrbId.add(osac.Problem__c); 
                mapOSACPrb.put(osac.Id, osac.Problem__c);        
            }
        }
        
        // Added by Uttara - OCT 2015 Release - BR-6646
        if(mapOSACPrb.keyset().size() > 0 && setPrbId.size() > 0) {
            
            AP_OSACMeeting.OsacGoNotifications(setPrbId, mapOSACPrb,'Go');
        }   
        
        if(lstOsac.size()>0){
            AP_OSACMeeting.updateProblem(lstOsac);
        }
    }
}