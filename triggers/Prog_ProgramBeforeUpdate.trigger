/*
    Author          : Srikant Joshi
    Date Created    : 11/07/2012
    Description     : Program Before Update event trigger.
*/

trigger Prog_ProgramBeforeUpdate on DMTProgram__c (before update) 
{
    System.Debug('****** Prog_ProgramBeforeUpdate Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('ProgramTriggerUpdate'))
    {
        Prog_ProgramTriggers.budgetBeforeUpdate(Trigger.new);

    }
    System.Debug('****** Prog_ProgramBeforeUpdate Trigger End ****'); 
}