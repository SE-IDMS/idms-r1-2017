//*********************************************************************************
// Trigger Name     : ProblemTeamMemberAfterInsert
// Purpose          : Problem  Team Member After Insert event trigger
// Created by       : Global Delivery Team
// Date created     : 25th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/

trigger ProblemTeamMemberAfterInsert on ProblemTeamMember__c (after insert) {
    System.Debug('****** ProblemTeamMemberAfterInsert Trigger Start ****');      
    
    if(Utils_SDF_Methodology.canTrigger('AP1001')){              
        List<ProblemTeamMember__c> prblmTeamList = new List<ProblemTeamMember__c>(); 
        
        for(ProblemTeamMember__c prblmTeam: Trigger.new){
            if(prblmTeam.Problem__c != null)
                prblmTeamList.add(prblmTeam);
        }
        
        if(prblmTeamList!=null && prblmTeamList.size()>0){ 
            System.debug('Inside prblmTeamList'+prblmTeamList);
            AP1000_Problem.notifyAllTeamMembers(prblmTeamList); 
        }
        //Add All Team Member to the Problem Share for Sharing the Problem Record
        AP1001_ProblemTeamMember.addProblemTeamMemberToProblemShare(Trigger.new);                  
    }                                               
    System.Debug('****** ProblemTeamMemberAfterInsert Trigger End ****'); 
}