trigger PartnerProgramBeforeInsert on PartnerProgram__c (before insert) {
    
    //Created on 01-Oct-2014, Renjith Jose
    //Release: OCT14 Release
    //Updating recordTypeid on save of partner program to view in the normal global pagelayout
    //Added as part of OCT14 Release
    //Accomodate Base Level in partner program 
    
    RecordType globRecType = [SELECT Id FROM RecordType WHERE DeveloperName = 'GlobalProgram' Limit 1];
    RecordType NewRecType =  [SELECT Id FROM RecordType WHERE DeveloperName = 'Tech_NewGlobalProgram' Limit 1];
    
    for(partnerprogram__c newPrg :trigger.new)
      {
        if(newPrg.RecordTypeId == NewRecType.id)
           newPrg.RecordTypeId = globRecType.id;
      }   

}