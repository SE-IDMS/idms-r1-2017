/*
BR-7383
OCT 15 RELEASE
Divya M
to get the no of related Symptoms Associated with the respective Problems. and Update on the Update on the Problem
*/
trigger RelSymptomAfterInsert on RelatedSymptom__c (After Insert) {
    if(Utils_SDF_Methodology.canTrigger('RelSymptomAfterInsert')){   
        AP_RelatedSymptom.CountRelSymptoms(trigger.new);
    }
}