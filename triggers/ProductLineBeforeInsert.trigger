/*
    Author: Bhuvana S
    Description: Trigger for Product Line Before Insert
*/

trigger ProductLineBeforeInsert on OPP_ProductLine__c (before Insert) 
{
    System.Debug('********* ProductLineBeforeInsert Trigger Start *********');
    if(Utils_SDF_methodology.canTrigger('AP_ProductLineTrigger')){
        AP_ProductLineBeforeInsertHandler handler = new AP_ProductLineBeforeInsertHandler();
        handler.OnBeforeInsert(Trigger.new,Trigger.newMap);    }
    System.Debug('********* ProductLineBeforeInsert Trigger End *********');
}