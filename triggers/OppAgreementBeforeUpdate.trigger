/*
    Author:Amitava Dutta
    Date: 11/02/2011.
    Description:Agreement Before Update Trigger.   
*/




trigger OppAgreementBeforeUpdate on OppAgreement__c (before Update) 
{    
    System.Debug('****** OppAgreementBeforeUpdate Trigger Start ****'); 
    
    if(Utils_SDF_Methodology.canTrigger('AP04'))
    {
        List<OppAgreement__c> agreementList = new List<OppAgreement__c>();
        List<OppAgreement__c> oppAgrs = new List<OppAgreement__c>();
        for(integer i=0;i<trigger.new.size();i++) {
            if(trigger.new[i].PhaseSalesStage__c != trigger.old[i].PhaseSalesStage__c) {
                agreementList.add(trigger.new[i]);
            }
            if(trigger.new[i].AccountName__c != trigger.old[i].AccountName__c) {
                oppAgrs.add(trigger.new[i]);
            }
        }
        
        if(agreementList.size() !=0) 
        {
           AP04_AgreementTrigger.ValidateAgrrementStatusUpdation(agreementList);
        }
        if(oppAgrs.size()>0)
        {
            AP04_AgreementTrigger.populateAccountOwnerEmail(oppAgrs);
        }    
    }
    System.Debug('****** OppAgreementBeforeUpdate Trigger End****');
}