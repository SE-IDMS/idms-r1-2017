trigger     FieloPRM_MemberSegmentAfterDelete on FieloEE__MemberSegment__c (After Delete) {
	
	if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberSegmentAfterDelete')){
    	FieloPRM_AP_MemberSegmentTrigger.disableFeatureMember();
    }
       
}