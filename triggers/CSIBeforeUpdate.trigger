trigger CSIBeforeUpdate on CustomerSafetyIssue__c (before Update) 
{
if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_CustomerSafetyIssue'))
{
 If(AP_customerSafetyIssue.blntrigger!=true)
 {
 AP_customerSafetyIssue.getStakeholder(trigger.new,label.CLSEP12I2P27,trigger.newmap,trigger.oldmap,label.CLSEP12I2P105)  ;  
 }
 }
}