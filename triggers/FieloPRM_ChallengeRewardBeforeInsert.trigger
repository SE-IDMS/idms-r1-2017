/********************************************************************
* Company: Fielo
* Created Date: 26/04/2015
* Description: Trigger before insert for object Challenge Reward
********************************************************************/
trigger FieloPRM_ChallengeRewardBeforeInsert on FieloCH__ChallengeReward__c (before insert) {
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_ChallengeRewardTrigger')){
        FieloPRM_AP_ChallengeRewardTrigger.createSegmentLevel();
        FieloPRM_AP_ChallengeRewardTrigger.setBadgeProgramLevel();      
        FieloPRM_AP_ChallengeRewardTrigger.validationChallengeReward();     
        
    }
    
}