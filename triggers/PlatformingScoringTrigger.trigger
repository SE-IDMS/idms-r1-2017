trigger PlatformingScoringTrigger on SFE_PlatformingScoring__c (before insert) {
    AP_PlatformingScoringTriggerHandler.populateWeight(trigger.new);
}