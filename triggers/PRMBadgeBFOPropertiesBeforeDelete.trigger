trigger PRMBadgeBFOPropertiesBeforeDelete on PRMBadgeBFOProperties__c (before delete) {
    if(Utils_SDF_Methodology.canTrigger('PRM_FieldChangeListener')){
        System.debug('BeginsPRMBadgeBFOPropertiesBeforeDelete');
        PRM_FieldChangeListener prmChangeListener = new PRM_FieldChangeListener();
        prmChangeListener.oldMemberExternalProperties(trigger.oldMap, false);
        
        /*PRM_FieldChangeListener prmChangeListener = new PRM_FieldChangeListener();
        prmChangeListener.PRMBadgeBFOPropertiesInspecter(trigger.oldMap);
        Map<String,Set<String>> accountRemoveList = prmChangeListener.mapAddBadgeAccount.clone();
        Map<String,Set<String>> contactRemoveList = prmChangeListener.mapAddBadgeContact.clone();
        System.debug('accountRemoveList trigger : ' + accountRemoveList);
        System.debug('contactRemoveList trigger :' + contactRemoveList);

        prmChangeListener.clearBadgeList();
        prmChangeListener.setBadgeList(accountRemoveList.clone(), contactRemoveList.clone(), new Map<String,Set<String>>(), new Map<String,Set<String>>());
        prmChangeListener.executeAction(); */
    }
}