/* 
    Author: Nicolas PALITZYNE (Accenture)
    Created date: 20/01/2012
    Description: Trigger before update for Team-Agent assignment, ensures the unicity of the default team by agent
*/

trigger TeamMemberBeforeUpdate on TeamAgentMapping__c (before update) 
{
    System.debug('Trigger.TeamMemberBeforeUpdate.INFO - Trigger called.');
    
    if(Utils_SDF_Methodology.canTrigger('AP43')) {
    
        // Retrieve all other teams related to this CC agent, if another is already marked as default team, uncheck it
        AP42_TeamAssignmentMethods.uncheckOtherDefaultTeams(trigger.new); 
    }
    
    System.debug('Trigger.TeamAgentAssignmentBeforeUpdate.INFO - End of trigger.');
}