/*
Author:Siddharth N
Purpose:
Quota to be calculated when the spa request is approved
*/
trigger SPARequestAfterUpdate on SPARequest__c (after update) {

    if(Utils_SDF_Methodology.canTrigger('AP_SPARequestCalculations'))
    {
        Set<Id> tobeProcessedQuotas=new Set<Id>();
        Map<Id,Id> tobeProcessedSalesPerson=new Map<Id,Id>();
        List<Id> spaIds=new List<id>();
        for(SPARequest__c spareq:Trigger.New){
            if(spareq.ApprovalStatus__c=='Approved' && spareq.SPAQuotaQuarter__c!=null && spareq.Opportunity__c!=null)
                tobeProcessedQuotas.add(spareq.SPAQuotaQuarter__c);
            if(spareq.SalesPerson__c!=null && spareq.salesperson__c!=spareq.ownerId)
                tobeProcessedSalesPerson.put(spareq.id,spareq.salesperson__c);    
        }
            
        if(tobeProcessedQuotas.size()>0)
        AP_SPARequestCalculations.updateRelatedQuota(tobeProcessedQuotas);                
        if(tobeProcessedSalesPerson.size()>0)
        AP_SPARequestCalculations.addSalesPersonToShare(tobeProcessedSalesPerson);  
        
        for(SPARequest__c i:trigger.new)
            spaIds.add(i.id);
        /*Account Contact should belong to the Account / Channel Contact Should belong to Channel. Inorder to prevent 
        discrepancy for the existing records which are in Approval in progress we will be checking only for the status Draft */
         for(SPARequest__c spa:[Select id,Account__c,ApprovalStatus__c,contact__r.AccountId,Channel__c,ChannelContact__c,ChannelContact__r.AccountId,RequestedType__c from SPARequest__c where id in:spaIds]){
           if(spa.contact__c!=null && spa.Account__c!=null && spa.ApprovalStatus__c=='Draft'){ 
                if(spa.Account__c!=spa.contact__r.AccountID){
                    SPARequest__c spaerr=Trigger.newMap.get(spa.id);
                    spaerr.addError(System.Label.CLOCT14SLS56);
               }
          }
          if(spa.ChannelContact__c!=null && spa.Channel__c!=null && spa.ApprovalStatus__c=='Draft'){
           if(spa.Channel__c!=spa.ChannelContact__r.AccountID){
                SPARequest__c spaerr=Trigger.newMap.get(spa.id);
                spaerr.addError(System.Label.CLOCT14SLS57);
           }  
         }      
      }
              
    }
    
}