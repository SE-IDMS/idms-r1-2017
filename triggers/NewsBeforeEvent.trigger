trigger NewsBeforeEvent on FieloEE__News__c (before insert,before update) {

    if(Utils_SDF_Methodology.canTrigger('AP_PRMUtils')){
            AP_PRMUtils.CheckRichTextFields(trigger.new,string.valueOf(trigger.new.getSObjectType()));
    }

}