/*
    Author          : Ankit Badhani (ACN) 
    Date Created    : 11/05/2011
    Description     : Case after insert event trigger.
------------------------------
    Modified By     : Vimal Karunakaran
    Modified Date   : 21 June 2012
    Description     : For September Release to:
                      1. Call AP_Case_CreateCaseActions.createPredefinedCCCActions
-----------------------------------------------------------------------------------
    Modified By     : Rakhi Kumar - Global Delivery Team
    Modified Date   : 11/06/2013
    Description     : Oct 2013 Release: BR-3111 - Disable automatic creation of CC Actions
-------------------------------------------------------------------------------------------

-
    Modified By     : Vimal Karunakaran - Global Delivery Team
    Modified Date   : 28/08/2014
    Description     : Oct 2014 Release: BR-4018  
                    1.  Calling AP_CaseRelatedProduct_Handler.UpdateCaseRelatedProduct
-------------------------------------------------------------------------------------------

Modified By     : Vimal Karunakaran - Global Delivery Team
    Modified Date   : 14/02/2015
    Description     : April 2015 Release: BR-3114  
                    1.  Calling AP_Case_UpdateCaseSolvingTeam.UpdateCaseSolvingTeam
-------------------------------------------------------------------------------------------

Modified By     : Uttara P R
    Modified Date   : 03 July 2015
    Description     : Oct 2015 Release:BR-7096
    
    -----------------------------------------------------------------  
*/

trigger CaseAfterInsert on Case (after Insert) {
    System.Debug('****** CaseAfterInsert Trigger Start****');
    
     
    if(Utils_SDF_Methodology.canTrigger('CaseAfterInsert')){
        List<Case> lstCaseWithProduct = new List<Case>();
        AP_UpdateMatchingCase.checkPMI_CMI(null,Trigger.newMap);
        Map<Id,Case> externalReferenceCases = new Map<Id,Case>();
        
        Set<Id> CaseIds = new Set<Id>();
        
        for(Case objCase : Trigger.new){
            if(objCase.CommercialReference_lk__c !=null || objCase.Family_lk__c !=null ){
                lstCaseWithProduct.add(objCase);
            }
            /*checks the External Reference field on case Object*/
            if(objCase.ExternalReference__c!=null){
                externalReferenceCases.put(objCase.id,objCase);
            }
            
            //OCT 2015 Release : Added by Uttara - BR-7096
            if(objCase.TECH_GMRCode__c != null && objCase.CaseSymptom__c != null &&
              (objCase.Status == Label.CLOCT15I2P31 || objCase.Status == Label.CLOCT15I2P33 || objCase.Status == Label.CLOCT15I2P34)) {
                CaseIds.add(objCase.Id);
            }  
                           
            // CLOCT15I2P31 = New
            // CLOCT15I2P33 = In Progress
            // CLOCT15I2P34 = Answer Provided to Customer          
        }
        
        //OCT 2015 Release : Added by Uttara - BR-7096
        System.debug('#### CaseIds: ' + CaseIds);
        if(!CaseIds.isEmpty())
            AP_UpdateMatchingCase.CaseWithOpenProblems(CaseIds);
            
        if(externalReferenceCases.size()>0){          
            System.debug('Batch Size'+externalReferenceCases.size());
            AP10_CaseTrigger.InsertExternalReference(externalReferenceCases);              
        }
        if(lstCaseWithProduct.size()>0){
            Set <String>        setGMRCodeForDelete         = new Set <String>();
            AP_CaseRelatedProduct_Handler.UpdateCaseRelatedProduct(lstCaseWithProduct , setGMRCodeForDelete);
        }
        if(!Test.IsRunningTest() || AP_Case_UpdateCaseSolvingTeam.isTest)
            AP_Case_UpdateCaseSolvingTeam.UpdateCaseSolvingTeam(null,Trigger.newMap,true);
    }
    
    System.Debug('****** CaseAfterInsert Trigger Ends****');         
 }