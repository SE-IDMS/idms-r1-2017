trigger PSassignmentRequestAfterUpdate on PermissionSetAssignmentRequests__c (after update) {

    Set<Id> validatedPsIdSet = new Set<Id>();

    for(PermissionSetAssignmentRequests__c psRequest:Trigger.New) {

        if(Trigger.oldMap.get(psRequest.id).Status__c != 'Assigned' && psRequest.Status__c == 'Assigned') {
            validatedPsIdSet.add(psRequest.Id);
        }    
    }    

    AP_PsAssignmentMethods.assignPermissionSet(validatedPsIdSet);
}