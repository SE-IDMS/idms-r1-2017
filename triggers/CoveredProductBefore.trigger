/*     
@Author: Chris Hurd
Created Date: 08-03-2013
Description: This trigger fires on Covered Product Before Insert and Update
**********
Scenarios:
**********
1.  Populate AssetLocation__c field with the Location set on Installed Product.

Date Modified       Modified By         Comments
------------------------------------------------------------------------
*/
trigger CoveredProductBefore on SVMXC__Service_Contract_Products__c (before insert, before update) {
    
    Set<Id> ipIdSet = new Set<Id>();
    String SEInterfaceSCConnector= System.Label.CLAPR15SRV37;
    String pid = userinfo.getProfileId().subString(0,15);
    if(pid != SEInterfaceSCConnector){
        for (SVMXC__Service_Contract_Products__c covProd : trigger.new)
        {
            SVMXC__Service_Contract_Products__c beforeupdate = null;
            
            if (trigger.isUpdate)
            {
                beforeUpdate = trigger.oldMap.get(covProd.Id);
            }
            
            if (covProd.SVMXC__Installed_Product__c != null && (trigger.isInsert || covProd.SVMXC__Installed_Product__c != beforeUpdate.SVMXC__Installed_Product__c))
            {
                ipIdSet.add(covProd.SVMXC__Installed_Product__c);
            }
        }
        
        if (ipIdSet.size() > 0)
        {
            Map<Id, SVMXC__Installed_Product__c> ipMap = new Map<Id, SVMXC__Installed_Product__c>([SELECT Id, SVMXC__Site__c FROM SVMXC__Installed_Product__c WHERE Id IN :ipIdSet]);
            
            for (SVMXC__Service_Contract_Products__c covProd : trigger.new)
            {
                if (ipMap.containsKey(covProd.SVMXC__Installed_Product__c))
                {
                    covProd.AssetLocation__c = ipMap.get(covProd.SVMXC__Installed_Product__c).SVMXC__Site__c;
                }
            }
        }
    }
}