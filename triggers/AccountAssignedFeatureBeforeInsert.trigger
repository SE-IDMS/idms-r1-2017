trigger AccountAssignedFeatureBeforeInsert on AccountAssignedFeature__c (before insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_AAF'))
    {
        AP_AccountAssignedFeature.checkDuplicateAAF(trigger.new);
    }   

}