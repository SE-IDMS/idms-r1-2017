//*********************************************************************************
// Trigger Name     : ContainmentActionBeforeUpdate 
// Purpose          : ContainmentAction  Before Update event trigger
// Created by       : Global Delivery Team
// Date created     : 27th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/


trigger ContainmentActionBeforeUpdate on ContainmentAction__c (before update) {
    System.Debug('****** ContainmentActionBeforeUpdate Trigger Start ****');
    
    List <ContainmentAction__c> containActionWithoutOwnerList = new List <ContainmentAction__c>();
    
    if(Utils_SDF_Methodology.canTrigger('AP1002')){
        if(Trigger.new.size()>0){
            for(ContainmentAction__c cntnActn:Trigger.new){
                if(cntnActn.Owner__c == null){
                    containActionWithoutOwnerList.add(cntnActn);
                }
                
                 // Oct 2015 Release - added by Uttara - Last Updated and Last Updated by fields population
                 if(!Test.isRunningTest()) {
                    cntnActn.ActualUserLastmodifiedDate__c = System.now();
                    cntnActn.ActualUserLastmodifiedby__c = UserInfo.getUserId();
                 }
            }
            AP1002_ContainmentAction.populateContainmentActionOwner(containActionWithoutOwnerList);
        }
        //===============Start Mohit on 22 June 2012 for september Release 2012 ==================
  AP1002_ContainmentAction.validateXA(trigger.new,'beforeupdate');
   //===============Start Mohit on 22 June 2012 for september Release 2012 ==================    
    }  
    
    System.Debug('****** ContainmentActionBeforeUpdate Trigger End ****');  
}