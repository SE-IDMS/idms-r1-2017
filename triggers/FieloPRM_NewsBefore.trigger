/********************************************************************
* Company: Fielo
* Created Date: 30/11/2016
* Description:
********************************************************************/

trigger FieloPRM_NewsBefore on FieloEE__News__c(before insert, before update, before delete){

    if(FieloEE__PublicSettings__c.getOrgDefaults().CacheEnabled__c != null && FieloEE__PublicSettings__c.getOrgDefaults().CacheEnabled__c){
        if(Trigger.isInsert || Trigger.isUpdate){
            FieloPRM_AP_NewsTriggers.removeCache(Trigger.new);
        }else if(Trigger.isDelete){
            FieloPRM_AP_NewsTriggers.removeCache(Trigger.old);
        }
    }
}