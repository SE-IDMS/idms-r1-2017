trigger AccountUpdateRequestAfterUpdate on AccountUpdateRequest__c (after update) {

if(Utils_SDF_Methodology.canTrigger('AP_AURRecordUpdate'))
    {
        AP_AURRecordUpdate.updateAccRecordFromAUR(Trigger.New);
    }
    
}