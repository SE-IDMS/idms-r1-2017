//*********************************************************************************
// Trigger Name     : containmentActionAfterInsert
// Purpose          : Containment Action After Insert event trigger
// Created by       : Global Delivery Team
// Date created     : 27th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/

trigger containmentActionAfterInsert on ContainmentAction__c (after insert) {
    System.Debug('****** containmentActionAfterInsert Trigger Start ****');      
    
    if(Utils_SDF_Methodology.canTrigger('AP1002')){              
        List<ContainmentAction__c> containmentActionList = new List<ContainmentAction__c>(); 
         Map<Id,ContainmentAction__c> CNANewMap = new Map<Id,ContainmentAction__c>();
        for(ContainmentAction__c cntnActn: Trigger.new){
            if((cntnActn.RelatedProblem__c != null) && (cntnActn.Owner__c != null))
                containmentActionList.add(cntnActn);
            if(cntnActn.AccountableOrganization__c!=null)
            {
                 CNANewMap.put(cntnActn.id,cntnActn);
            }    
        }
        //Add All Containment Action Owner  to the Problem Share for Sharing the Problem Record
        AP1002_ContainmentAction.addContainmentOwnerToProblemShare(containmentActionList);
        if(CNANewMap.size()>0)
        {
           AP1002_ContainmentAction.updateAllStakeholders(CNANewMap);   
        }
    }                                               
    System.Debug('****** containmentActionAfterInsert Trigger End ****'); 
}