/********************************************************************************************************************
    Author : Shruti Karn
    Created Date : 2 July 2012
    Description : For September Release:
                 1. Populate the Account From Contact of Contract Value Chain Player
                    
********************************************************************************************************************/
trigger CVCPBeforeUpdate on CTR_ValueChainPlayers__c (before update) {
    //call checkDuplicateAdmin
    if(Utils_SDF_Methodology.canTrigger('AP_CVCPTriggerUtils') || test.isRunningTest())
        AP_CVCPTriggerUtils.populateAccountFromContact(Trigger.new);
}