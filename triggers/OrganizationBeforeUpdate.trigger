trigger OrganizationBeforeUpdate on BusinessRiskEscalationEntity__c (Before Update) {

if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_OrgBfUpdate'))
{
        AP_Organization.checkOrgEntySubentyLocation(trigger.new) ;
        

// Oct 15 Release - Gayathri Shivakumar - Starts
        
Boolean AddrUpdated;        
for(BusinessRiskEscalationEntity__c org : trigger.new){

    BusinessRiskEscalationEntity__c OldOrg = trigger.oldMap.get(org.id);
// Check for Local address change 
    if(org.RCStreetLocalLanguage__c != OldOrg.RCStreetLocalLanguage__c || org.RCAdditionalInformationLocal_Language__c != OldOrg.RCAdditionalInformationLocal_Language__c || org.RCZipCode__c != OldOrg.RCZipCode__c || org.RCLocalCity__c != OldOrg.RCLocalCity__c || org.RCLocalCounty__c != OldOrg.RCLocalCounty__c)
    AddrUpdated = True;
// Check for Global Address change
    Else if(org.Street__c != OldOrg.Street__c || org.RCAdditionalInformation__c != OldOrg.RCAdditionalInformation__c || org.RCCity__c != OldOrg.RCCity__c || org.RCStateProvince__r.Name != OldOrg.RCStateProvince__r.Name || org.RCCountry__r.Name != OldOrg.RCCountry__r.Name || org.RCPOBox__c != OldOrg.RCPOBox__c || org.RCPOBoxZipCode__c != OldOrg.RCPOBoxZipCode__c || org.RCContactName__c != OldOrg.RCContactName__c || org.RCPhone__c != OldOrg.RCPhone__c || org.RCFax__c != OldOrg.RCFax__c)
    AddrUpdated = True;
    Else
    AddrUpdated = False;
   }

    if(AddrUpdated)
     AP_Organization.UpdateReturnItemRecords(trigger.newMap);
   }  
 }