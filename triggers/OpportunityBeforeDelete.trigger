trigger OpportunityBeforeDelete on Opportunity (before delete) {
     List<OPP_Project__c> prjlist=new List<OPP_Project__c>();
     List<Opportunity> opplist= new list<Opportunity>(); 
    for(Opportunity opp: trigger.old)
    {
        /* In order to considerthe entire hierarchy linked to Master Project
        we are considering Tech_MasterProjectId__c instead of project__c. project__c gets only
        Opportunities directly linked to master project but the Tech_MasterProjectId__c field will
        get all the opportunities in the entire hierarchy under Master Project*/
         if(opp.Tech_MasterProjectId__c!=null && opp.Tech_MasterProjectId__c!=''){
              prjlist.add(new OPP_Project__c(Id=opp.Tech_MasterProjectId__c));  
              opplist.add(opp);
          }
    }
    //Update Amount fields of the Master Project if the Opportunity is linked to the Master Project - Monalisa Project oct 2015
        if(prjlist.size()>0)
            AP05_OpportunityTrigger.updateProjectAmountFields(prjlist,opplist);

    if(Utils_SDF_Methodology.canTrigger('PRM_FieldChangeListener')) {
      System.debug('OpportunityBefore Delete');
      PRM_FieldChangeListener prmChangeListener = new PRM_FieldChangeListener();
      prmChangeListener.badgeInspecter(trigger.oldMap, null, PRM_FieldChangeListener.OPPORTUNITY_OBJECT_NAME);
    }
}