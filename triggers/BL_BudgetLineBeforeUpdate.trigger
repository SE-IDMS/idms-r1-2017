/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 06/11/2012
    Description     : Budget Line Before Update event trigger.
*/

trigger BL_BudgetLineBeforeUpdate on PRJ_BudgetLine__c (before update) 
{
    System.Debug('****** BL_BudgetLineBeforeUpdate Trigger Start ****'); 
    if(Utils_SDF_Methodology.canTrigger('BudgetLineTriggerUpdate'))
    {
        System.Debug('Value New Update: ' + Trigger.new);
        System.Debug('Value NewMap Update: ' + Trigger.newMap);
        
        BL_BudgetLineTriggers.budgetLineBeforeUpdate(Trigger.newMap, Trigger.oldMap);

    }
    System.Debug('****** BL_BudgetLineBeforeUpdate Trigger End ****'); 
}