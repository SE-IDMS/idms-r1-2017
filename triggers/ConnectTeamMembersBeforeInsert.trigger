/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 03-Feb-2012
    Description     : Connect Team Members Before Insert event trigger.
*/

trigger ConnectTeamMembersBeforeInsert on Team_Members__c (before insert) {

  System.Debug('****** ConnectTeamMembersBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('ConnectTeamMembersBeforeInsert'))
    {
        ConnectTeamMembersTriggers.TeamMembersBeforeInsert_ValidateScopeEntity(Trigger.New);
        ConnectTeamMembersTriggers.TeamMembersBeforeInsert(Trigger.new);
        ConnectNetworkTeamMembersProgramTriggers.ConnectNetworkTeamMembersBeforeInsert(Trigger.New);
        if (Trigger.new[0].ValidateScopeonInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_Deployment_Network_Insert_ERR1);
   
 }
 System.Debug('****** ConnectTeamMembersBeforeInsert Trigger End ****'); 
}