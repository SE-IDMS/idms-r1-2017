/*Created by: Deepak Kumar
Date:12/08/2013
For the purpose of Updating Installed Product Field Under Contract Field Value From service COntract Active Status Value.
OCT13 Release
*/
trigger CoveredProductBeforeUpdate on SVMXC__Service_Contract_Products__c(before update)
{
    //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SRV_APR_002'))
    {
        List<SVMXC__Service_Contract_Products__c> cpLst = New List<SVMXC__Service_Contract_Products__c>();
        // Commented by Nabil ZEGHACHE DEF-7771
        // List<SVMXC__Service_Contract_Products__c> cpList2 = New List<SVMXC__Service_Contract_Products__c>(); // Added by Nabil ZEGHACHE - 25-Feb-2015 - to store the covered IP where either the IP or the Contract has been changed, in order to recheck address consistency with the new association
        Set<Id> scId = New Set<Id>();
        Set<Id> sccId = New Set<Id>();
        Set<Id> ipId = New Set<Id>();
        Set<Id> newipIdset = New Set<Id>();
         set<id> CPscid = new set<id>(); 
        List<SVMXC__Service_Contract_Products__c> oldCoveredProduct = new List<SVMXC__Service_Contract_Products__c>();
          

        for(SVMXC__Service_Contract_Products__c cov :trigger.New)
        {   


            if(cov.SVMXC__Service_Contract__c!=null && (cov.SVMXC__Start_Date__c!=null && cov.SVMXC__Start_Date__c!=trigger.oldmap.get(cov.id).SVMXC__Start_Date__c) ||(cov.SVMXC__End_Date__c!=null && cov.SVMXC__End_Date__c!=trigger.oldmap.get(cov.id).SVMXC__End_Date__c))
                
            {
                
                sccId.add(cov.SVMXC__Service_Contract__c);
            }


            if(cov.SVMXC__Service_Contract__c !=null && cov.SVMXC__Installed_Product__c !=null && trigger.oldMap.get(cov.Id).SVMXC__Installed_Product__c != cov.SVMXC__Installed_Product__c)
            {
                scId.add(cov.SVMXC__Service_Contract__c);
                ipId.add(cov.SVMXC__Installed_Product__c);
                oldCoveredProduct.add(trigger.oldMap.get(cov.id));
                newipIdset.add(cov.SVMXC__Installed_Product__c);
            }
            if(cov.SVMXC__Service_Contract__c !=null) {
                CPscid.add(cov.SVMXC__Service_Contract__c);
            }
/* // Commented by Nabil ZEGHACHE DEF-7771 
            if (cov.SVMXC__Service_Contract__c !=null && cov.SVMXC__Installed_Product__c !=null && (trigger.oldMap.get(cov.Id).SVMXC__Installed_Product__c != cov.SVMXC__Installed_Product__c || trigger.oldMap.get(cov.Id).SVMXC__Service_Contract__c != cov.SVMXC__Service_Contract__c)) {
                cpList2.add(cov);
            }
*/
        }
        //commented by Hari Krishna 
        //AP_IpUndrContractFldUpdate.IpUnderContrUpdate(scId,ipId);    
        if(oldCoveredProduct != null && oldCoveredProduct.size()>0){
            
            AP_CoveredProduct.InstalledProdcutUnderContract(oldCoveredProduct);
            
        }


        if(newipIdset != null && newipIdset.size()>0){
            
            List<SVMXC__Installed_Product__c> ipObjList =[Select Id,UnderContract__c From SVMXC__Installed_Product__c Where Id in :newipIdset];
                for(SVMXC__Installed_Product__c ipObj :ipObjList)
                {

                    ipObj.UnderContract__c = true;
                }

                update ipObjList;
            
        }
        /*********************************************************************

        Added By Deepak
        For April-14 :-Preventing the creation of Duplicate Covered Product.
        *************************************************************************/

        List<SVMXC__Service_Contract_Products__c> cpList1 = New List<SVMXC__Service_Contract_Products__c>();
        for(SVMXC__Service_Contract_Products__c cov :trigger.New)
        {   
            if(cov.IncludedService__c !=null && cov.SVMXC__Installed_Product__c !=null && cov.SVMXC__Service_Contract__c !=null  && ((trigger.oldMap.get(cov.Id).SVMXC__Installed_Product__c != trigger.NewMap.get(cov.Id).SVMXC__Installed_Product__c)        || (trigger.oldMap.get(cov.Id).IncludedService__c !=trigger.NewMap.get(cov.Id).IncludedService__c)            || (trigger.oldMap.get(cov.Id).SVMXC__Service_Contract__c !=trigger.NewMap.get(cov.Id).SVMXC__Service_Contract__c)))
            {
                cpList1.add(cov);
            }
            if(cov.SVMXC__Installed_Product__c!=null && (trigger.oldMap.get(cov.Id).SVMXC__Installed_Product__c!=cov.SVMXC__Installed_Product__c)){
                cpLst.add(cov);              
            }
        }
        if(cpList1.size()>0)
        {
            AP_CoveredProducttrigger.AddError(cpList1);
        }
        /*********************************************************************


        Added By Deepak
        For April-15:-Populating fields from Installed Product Account.
        *************************************************************************/

         if(cplst !=null){
            AP_CoveredProduct.PopulatingAccountFields(cplst);  
        }
        /*if(CPscid!=null && CPscid.size()>0){
        AP_CoveredProduct.PMvisitUpdateOnCoveredProduct(CPscid);
        }*/
/* // Commented by Nabil ZEGHACHE DEF-7771         
        if(cpList2 != null & cpList2.size()>0) {
            AP_IPAssociationHandler.checkAddressConsistency(cpList2);
        }  
*/
        
    }
}