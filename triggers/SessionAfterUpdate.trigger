trigger SessionAfterUpdate on TMT_Session__c (after update) 
{
    if(Utils_SDF_Methodology.canTrigger('TMT'))
    {
        List<TMT_Session__c> RescheduledSessions = new List<TMT_Session__c>();
    
        For(Integer i=0; i<trigger.new.size(); i++)
        {
            If(trigger.new[i].Start__c != trigger.old[i].Start__c && trigger.new[i].Notify__c==true )
            {
                  RescheduledSessions.add(trigger.new[i]);
            }
        }
        
        if(RescheduledSessions.size()>0)
            TMT_SendEmailClass.SendRescheduledEmail( RescheduledSessions); 
    }
    
    if(Utils_SDF_Methodology.canTrigger('TMT'))
    {
        List<TMT_Session__c> ActivatedSessions = new List<TMT_Session__c>();
    
        For(Integer i=0; i<trigger.new.size(); i++)
        {
            If(trigger.new[i].Notify__c==true && trigger.old[i].Notify__c==false)
            {
                  ActivatedSessions.add(trigger.new[i]);
            }
        }
        
        if(ActivatedSessions.size()>0)
            TMT_SendEmailClass.SendSessionNotificationEmail(ActivatedSessions); 
    }
}