trigger Quote_AfterUpdate on zqu__Quote__c (after update, before update) {
    if(system.isFuture()) return;

    if (trigger.isBefore && trigger.isUpdate){
        Handler_QuoteAfterUpdate.UpdateActivationDateByCondition(trigger.oldMap, trigger.newMap);
        
        /* JPL - 2016-11-23: Quote Template determination based on zqu__Quote__c.QuoteContactName__c Lookup(Contact) field */
        Handler_QuoteAfterUpdate.updateQuoteTemplate(trigger.newMap);
        /* JPL - 2016-11-23 */
    }
    
    else if(trigger.isAfter && trigger.isUpdate){
        Set<String> zquoteBilltoIds = new Set<String>();
        Set<String> zquoteSoldtoIds = new Set<String>();
        Set<String> zquoteSubsActivationDateIds = new Set<String>();
        Set<String> zquoteSubsProvisionDateIds = new Set<String>();
        Set<String> zQuoteIds = new Set<String>();
        List<zqu__Quote__c> zSubscriptionIdSet = new List<zqu__Quote__c>();
        List<zqu__Quote__c> zSubscriptionIdToUpdate = new List<zqu__Quote__c>();
        Set<String> zouraSubscriptionId = new Set<String>();     

        for(zqu__Quote__c quo : Trigger.new){
            if(quo.zqu__ZuoraSubscriptionID__c != null && quo.zqu__ZuoraSubscriptionID__c.trim() != ''){
                zSubscriptionIdSet.add(quo);
            }
            if (quo.zqu__BillToContact__c != trigger.oldmap.get(quo.Id).zqu__BillToContact__c)
                zquoteBilltoIds.add(quo.Id);
            if (quo.zqu__SoldToContact__c != trigger.oldmap.get(quo.Id).zqu__SoldToContact__c)
                zquoteSoldtoIds.add(quo.Id);
            if (trigger.oldmap.get(quo.Id).zqu__Service_Activation_Date__c==null && quo.zqu__Service_Activation_Date__c != trigger.oldmap.get(quo.Id).zqu__Service_Activation_Date__c)
                zquoteSubsActivationDateIds.add(quo.Id);
            if ((trigger.oldmap.get(quo.Id).Z_ProvisionDate__c==null && quo.Z_ProvisionDate__c != trigger.oldmap.get(quo.Id).Z_ProvisionDate__c) ||
               (quo.Z_ProvisionDate__c !=null && trigger.oldmap.get(quo.Id).zqu__Status__c=='new' && quo.zqu__Status__c != trigger.oldmap.get(quo.Id).zqu__Status__c))
                zquoteSubsProvisionDateIds.add(quo.Id);
            if(trigger.oldmap.get(quo.Id).zqu__Status__c != quo.zqu__Status__c && quo.zqu__Status__c == 'Sent to Z-Billing'){
                zSubscriptionIdToUpdate.add(quo);
                zQuoteIds.add(quo.Id);
            }
        }
        Handler_QuoteAfterUpdate.BeginZuoraQuoteFlow(zquoteBilltoIds,zquoteSoldtoIds,zquoteSubsActivationDateIds,zquoteSubsProvisionDateIds);
        if(zSubscriptionIdToUpdate.size()>0){
            Handler_QuoteAfterUpdate.updateZSubscriptionFields(zQuoteIds);
            
        }
    }
}