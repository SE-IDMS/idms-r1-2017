/*
    Author          : Srikant Joshi (Schneider Electric)
    Date Created    : 09/10/2012
    Description     : To create a Funding Entity by default.                     
                      
*/
trigger CHR_ReqAfterInsert on Change_Request__c(After insert) {

    if(Utils_SDF_Methodology.canTrigger('CHRRequestTriggerAfterInsert'))
    {
        CHR_ProjectTriggers.CHRAfterInsert(Trigger.new);
    }


}