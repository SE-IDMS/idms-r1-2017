/*
1-Mar-13    Shruti Karn            PRM Mar13 Release    BR-2782    Initial Creation
5-Mar-13    Srinivas Nallapati     PRM Mar13 Release    BR-2782    Updated Email alert  
  
*/
trigger ContactAfterInsert on Contact (after insert) {

      

    set<Id> setAccountId = new set<Id>();
    list<Contact> lstContact = new list<Contact>();
    list<Contact> lstConSyncObj = new list<Contact>();
    if(Utils_SDF_Methodology.canTrigger('AP_Contact_PartnerUserUpdate') && !(System.isBatch()|| System.isFuture()))
    {
        set<Id> setNewPartnerContactId = new set<Id>();
        user usr = [Select Id,BypassWF__c From User where Id=: userinfo.getUserId()];
        Set<Id> accIds = new Set<Id>();
        for(Contact con : trigger.new) {
            if(con.PRMContact__c) {
                setAccountId.add(con.accountid);
                lstContact.add(con);
            }
            accIds.add(con.AccountId);
        }
        Map<Id, Account> mAccMap = new Map<Id, Account>([SELECT Id, Name, RecordTypeId,PRMAccount__c FROM Account WHERE Id IN :accIds]);
        for(Contact cont : trigger.new) {
            //setAccountId.add(cont.accountid);
            if(cont.PRMUser__c && mAccMap.ContainsKey(cont.AccountId) && !String.valueOf(mAccMap.get(cont.AccountId).RecordTypeId).startswithIgnoreCase(System.Label.CLAPR15PRM025) && 
                String.isBlank(cont.PRMUIMSID__c))               
                setNewPartnerContactId.add(cont.id);
                
            if(mAccMap.ContainsKey(cont.AccountId) && mAccMap.get(cont.AccountId).PRMAccount__c){
                lstConSyncObj.add(cont);
            }   
        }
        system.debug('--SizeOfSet'+setNewPartnerContactId.size());
        if(!setNewPartnerContactId.isEmpty())
           AP_Contact_PartnerUserUpdate.insertPartnerUser(setNewPartnerContactId );
        
    }
    if(!setAccountId.isEmpty() && (Utils_SDF_Methodology.canTrigger('AP_AAP_AccountProgramUpdate')))
    {
        list<Id> listProgramId = new list<Id>();
        list<ACC_PartnerProgram__c> lstAccountProgram = new  list<ACC_PartnerProgram__c>();
        lstAccountProgram = [Select id, account__c, PartnerProgram__c,ProgramLevel__c from ACC_PartnerProgram__c where Account__c in :setAccountId and Active__c = true limit 50000];
        //for(ACC_PartnerProgram__c acctPrg : lstAccountProgram)
            //listProgramId.add(acctPrg.Id);
        if(!lstAccountProgram.isEmpty())
            AP_AAP_AccountProgramUpdate.createConAssignedProgram(lstAccountProgram , lstContact);
    }
    
    //////Fielo Syncrhonize//////////////////

    if(!Test.isRunningTest()){
        SyncObjects instance = new SyncObjects(lstConSyncObj);
        boolean allornone = false;
        if(!instance.currentState.error ){
            SyncObjectsResult result = instance.syncronize(allornone);
        }
    }
    ///////////////////////////////////////////
    
}