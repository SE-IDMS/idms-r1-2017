trigger CBATeamAfterInsert on CBATeam__c (After insert) {

    if(Utils_SDF_Methodology.canTrigger('AP_SharingAccessToCBATeam') || Test.isRunningTest())
    {  
        System.Debug('**** SMETeamAfterInsert Trigger Start ****');
        
        
        
        Map<Id,CBATeam__c> CBATeamMap = new Map<Id,CBATeam__c>();
                
        for(CBATeam__c CBATeamObj:Trigger.new)
        {        
            CBATeamMap.put(CBATeamObj.id,CBATeamObj);
        }
    
        AP_SharingAccessToCBATeam.insertCBATeamMember(CBATeamMap);
               
        
               
    }

}