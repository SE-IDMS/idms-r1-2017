/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 02-Oct-2012
    Description     : Connect Global KPI After Insert event trigger.
*/

trigger GlobalKPITargetAfterInsert on Global_KPI_Targets__c (after insert) {

System.Debug('****** GlobalKPITargetAfterInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('GlobalKPITargetAfterInsert'))
    {
     ConnectGlobalKPITargetTriggers.ConnectGlobalKPITargetAfterInsert(Trigger.new);   
        
    }
 System.Debug('****** GlobalKPITargetAfterInsert Trigger End ****'); 
 }