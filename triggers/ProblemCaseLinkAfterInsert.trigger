trigger ProblemCaseLinkAfterInsert on ProblemCaseLink__c (after insert) {

    if(Utils_SDF_Methodology.canTrigger('ProblemCaseLinkAfterInsert')){        
        
        AP_ProblemCaseLink.updateCaseAfterInsert(Trigger.New);
    }
}