trigger SVMXC_InstalledProductAfter on SVMXC__Installed_Product__c (after delete, after insert, after update) {

	if (trigger.isInsert)
		SVMXC_InstalledProductAfter.createWDLFromInstalledProduct(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
		
	if (trigger.isInsert)
		SVMXC_InstalledProductAfter.createAssetLinkFromInstalledProduct(trigger.new, trigger.oldMap, trigger.isInsert, trigger.isUpdate);
}