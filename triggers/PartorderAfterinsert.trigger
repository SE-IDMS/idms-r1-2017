trigger PartorderAfterinsert on SVMXC__RMA_Shipment_Order__c (After insert, after update) {
    
    set<id> partset= new set<id>();
    set<id> partRMAset= new set<id>();
    
    
    for(SVMXC__RMA_Shipment_Order__c  part:trigger.new)
    {
        
        if(part.SVMXC__Order_Status__c=='Cancelled'){
            partRMAset.add(part.Parts_Order_RMA__c);
            partset.add(part.id);
            
            
        }
        
    }
    if(partRMAset!=null && partRMAset.size()>0)
        AP_Partorder.cancelPartorderRMA(partRMAset);
    
    if(partset!=null && partset.size()>0){
        AP_Partorder.cancelpartsorder(partset);
    }
    
    
}