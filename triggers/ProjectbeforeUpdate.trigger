trigger ProjectbeforeUpdate on Milestone1_Project__c (before update) {


set<string> setUserId = new set<string>();

    if(trigger.isUpdate && Trigger.isBefore) {
    
            for(Milestone1_Project__c trgObj:trigger.new ) {
                
                // trgObj.BoxUserEmailId__c =trgObj.Launch_Owner__r.email; 
                if(trgObj.Country_Launch_Leader__c!=null  && trgObj.BoxUserEmailId__c==null && trgObj.Country_Launch_Leader__c!=trigger.oldmap.get(trgObj.id).Country_Launch_Leader__c) {
                    setUserId.add(trgObj.Country_Launch_Leader__c);
                } else if(trgObj.Country_Withdrawal_Leader__c!=null && trgObj.BoxUserEmailId__c==null && trgObj.Country_Withdrawal_Leader__c!=trigger.oldmap.get(trgObj.id).Country_Withdrawal_Leader__c) {
                    
                    setUserId.add(trgObj.Country_Withdrawal_Leader__c);
                    
                } else if(trgObj.Country_Launch_Leader__c==null && trgObj.Region_Introduction_Owner__c!=null && trgObj.Country_Withdrawal_Leader__c==null && trgObj.Region_Introduction_Owner__c!=trigger.oldmap.get(trgObj.id).Region_Introduction_Owner__c) {
                         setUserId.add(trgObj.Region_Introduction_Owner__c);
                } else if(trgObj.Country_Launch_Leader__c==null && trgObj.Region_Introduction_Owner__c==null && trgObj.Country_Withdrawal_Leader__c==null && trgObj.RegionWithdrawalManager__c!=null && trgObj.RegionWithdrawalManager__c!=trigger.oldmap.get(trgObj.id).RegionWithdrawalManager__c) {
                         setUserId.add(trgObj.RegionWithdrawalManager__c);
                         
                }
    
            }
              
           Map<id,User> mapUser =new Map<id,User>([select id ,email from user where id =:setUserId]);
            if(mapUser.size() > 0) {
            for(Milestone1_Project__c mpObj:trigger.new) {
                if(mpObj.Country_Launch_Leader__c!=null) {
                    mpObj.BoxUserEmailId__c=mapUser.get(mpObj.Country_Launch_Leader__c).email;
                     //mpObj.ownerid=mapUser.get(mpObj.Country_Launch_Leader__c).id;
                } else if(mpObj.Country_Withdrawal_Leader__c!=null) {
                
                    mpObj.BoxUserEmailId__c=mapUser.get(mpObj.Country_Withdrawal_Leader__c).email;
                    //mpObj.ownerid=mapUser.get(mpObj.Country_Withdrawal_Leader__c).id;
                    
                }
                
                else if(mpObj.Region_Introduction_Owner__c!=null) {                
                    mpObj.BoxUserEmailId__c=mapUser.get(mpObj.Region_Introduction_Owner__c).email;
                    //mpObj.ownerid=mapUser.get(mpObj.Country_Withdrawal_Leader__c).id;
                    
                }
                else if(mpObj.RegionWithdrawalManager__c!=null) {                
                    mpObj.BoxUserEmailId__c=mapUser.get(mpObj.RegionWithdrawalManager__c).email;
                    //mpObj.ownerid=mapUser.get(mpObj.Country_Withdrawal_Leader__c).id;
                    
                }
        
            }
           
         } 
         
    }        


}