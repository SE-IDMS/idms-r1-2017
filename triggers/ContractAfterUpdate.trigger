/********************************************************************************************************************
    Modified By : Shruti Karn
    Created Date : 2 June 2012
    Description : For September Release:
                 1. Calls AP_ContractTriggerUtils.findNewContact to check update/insert Value chain player for the Contract's Contact
    
********************************************************************************************************************/
trigger ContractAfterUpdate on Contract (after update) {
  //Updated for September Release by Shruti Karn
  //Update by Vimal, Moved the Logic of Opportunity Creation to the Class Q2 2016 Release
  //Commented by Pooja Suresh, Moved Logic for Proactive Case Creation to Package level Q3 2016 Release
  
  if(Utils_SDF_Methodology.canTrigger('AP_ContractTriggerUtils') || test.isRunningTest()){
    AP_ContractTriggerUtils.createOpptyForExpiringContract(trigger.oldMap,trigger.newMap);
    //Commented below method call as part of BR-9333 Q3 2016 Release - Move the proactive case functionality from Contract to Package.
    //Delete below commented code after Q3 Go-live
    //AP_ContractTriggerUtils.createProactiveCaseForContracts(trigger.oldMap,trigger.newMap);
    AP_ContractTriggerUtils.findNewContact(trigger.newMap,trigger.oldMap);
    AP_ContractTriggerUtils.updateContractTypeonCase(trigger.newMap,trigger.oldMap);
  }
}