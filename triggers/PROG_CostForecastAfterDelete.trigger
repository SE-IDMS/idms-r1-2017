/*
    Author          : Ramakrishna Singara (Schneider Electric)
    Date Created    : 12/07/2012
    Description     : Program Costs Forecasts After Delete event trigger.
*/
trigger PROG_CostForecastAfterDelete on ProgramCostsForecasts__c (After Delete) {
    if(Utils_SDF_Methodology.canTrigger('ProgramCostForecastTriggerAfterDelete'))
    {
        PROG_CostForecastTriggers.ProgCostForecastAfterDelete(Trigger.old);
    }
}