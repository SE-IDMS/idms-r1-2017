trigger ProductWarrAfterUpdate on SVMXC__Warranty__c (after update) {
//Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX20')){
        List<SVMXC__Warranty__c> PWlist = New List<SVMXC__Warranty__c>();
        List<SVMXC__Installed_Product__c> IPlisttoUpdate= New List<SVMXC__Installed_Product__c>();
        set<Id> ipid = New set<Id>();
        Map<Id,SVMXC__Warranty__c> ipiPWmap = new Map<Id,SVMXC__Warranty__c>();
        for(SVMXC__Warranty__c pw :trigger.New){
            if((pw.SVMXC__Installed_Product__c !=null && pw.SVMXC__Start_Date__c !=null && pw.SVMXC__End_Date__c !=null)
                && (trigger.OldMap.get(pw.Id).SVMXC__End_Date__c !=pw.SVMXC__End_Date__c 
                || trigger.OldMap.get(pw.Id).SVMXC__Start_Date__c !=pw.SVMXC__Start_Date__c
                || trigger.OldMap.get(pw.Id).SVMXC__Installed_Product__c !=pw.SVMXC__Installed_Product__c)){
                
                    ipid.add(pw.SVMXC__Installed_Product__c);
                    PWlist.add(pw);
            }
        
        }
        for(SVMXC__Warranty__c wr :PWlist){
            if(wr.SVMXC__Installed_Product__c !=null){
                ipiPWmap.put(wr.SVMXC__Installed_Product__c,wr);
            
            }
        }
        
        if(ipid !=null){
            for(SVMXC__Installed_Product__c ip :[Select Id ,SVMXC__Warranty_Start_Date__c,SVMXC__Warranty_End_Date__c From SVMXC__Installed_Product__c Where Id in :ipid]){
                if(ipiPWmap.containsKey(ip.Id)){
                    ip.SVMXC__Warranty_Start_Date__c=ipiPWmap.get(ip.Id).SVMXC__Start_Date__c;
                    ip.SVMXC__Warranty_End_Date__c=ipiPWmap.get(ip.Id).SVMXC__End_Date__c;
                    IPlisttoUpdate.add(ip);
                }
            }
        }
        if(IPlisttoUpdate.size()>0){
            update IPlisttoUpdate;
        }
   }
}