trigger ZuoraSubscriptionBeforeInsertAfterInsertAfterUpdate on Zuora__Subscription__c (before insert, after insert, after update) {
/*   if (Trigger.isBefore && Trigger.isInsert) {
        } else */ if (Trigger.isAfter && Trigger.isInsert) {
            Handler_SubscriptionAfterUpdate.updateZquoteSubscription(Trigger.new);

            //March 2016 Release
            Set<String> SubsIds = new Set<String>();
            for(Zuora__Subscription__c sub : Trigger.new){
                if (!String.isEmpty(sub.Asset_SKU__c) & !String.isEmpty(sub.Asset_SN__c))
                        SubsIds.add(sub.Id);
            }    
            if(SubsIds.Size()>0){
                    Handler_SubscriptionAfterUpdate.updateSubscriptionAsset(SubsIds);
                }
            //End March 2016 Release
        }
        else if (Trigger.isAfter && Trigger.isUpdate) {
            Handler_SubscriptionAfterUpdate.updateZquoteExistingSubscription(Trigger.new);
            Set<String> SubsIds = new Set<String>();
            for(Zuora__Subscription__c sub : Trigger.new){
                //if (trigger.oldmap.get(sub.Id).Zuora__ServiceActivationDate__c==null && sub.Zuora__ServiceActivationDate__c != trigger.oldmap.get(sub.Id).Zuora__ServiceActivationDate__c)
                if (sub.Zuora__ServiceActivationDate__c != trigger.oldmap.get(sub.Id).Zuora__ServiceActivationDate__c)
                    SubsIds.add(sub.Id);
            }
            if(SubsIds.Size()>0){
                Handler_SubscriptionAfterUpdate.BeginZuoraSubscriptionFlow(SubsIds);
            }
        }else if(Trigger.isBefore && Trigger.isInsert){
            Handler_SubscriptionBeforeInsert.UpdateReferenceQuote(Trigger.new);
        }
}