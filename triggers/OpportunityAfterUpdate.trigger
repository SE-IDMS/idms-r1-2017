/*
Author:Siddharth N(GD Solutions)
12-Mar-2013 Shruti Karn  PRM Mar13   BR-2910
4 July 2013 Shruti Karn  PRM July13  BR-2974
*/
trigger OpportunityAfterUpdate on Opportunity (after Update) {
    System.Debug('****** OpportunityAfterUpdate Trigger Start ****');  

    Map<Opportunity, String> oppStg = new Map<Opportunity, String>();
    Map<Id, Opportunity> oppsGreaterClosedDate = new Map<Id, Opportunity>();    
    list<Opportunity> lstOppAmntUpdate = new list<Opportunity>();
    list<Opportunity> lstOppStageUpdate = new list<Opportunity>(); 
    list<OPP_Project__c> prjlist= new list<OPP_Project__c>(); 
    list<Opportunity> opplist= new list<Opportunity>(); 
    List<Opportunity> masterProjectStatusCheck = new List<Opportunity>();
    if(Utils_SDF_Methodology.canTrigger('AP44') && VFC61_OpptyClone.isOpptyClone == false)
    {
        AP44_updateOpportunityAccountChallenges.updateOpportunityAccountChallenges(Trigger.new);
    }
    if(Utils_SDF_Methodology.canTrigger('AP49')  && VFC61_OpptyClone.isOpptyClone == false)
    {
        //Updated for including sharing of Quote Link records with the Opportunity Owner    
        AP49_UpdateQLKSharing.findUpdatedOppLeaders(trigger.new,trigger.old);
    }   
    if(Utils_SDF_Methodology.canTRigger('AP01') || (Utils_SDF_Methodology.canTrigger('AP_Opp_UpdateParentCloseDate')))
    {
        for(Integer i=0;i<Trigger.new.size();i++)
        {
           /* March 2016 release - BR-8430
           Prevent changes on Completed Master Project
           Prevent User linking the Opportunity to the hierarchy if the Master Project is completed */ 
           if(trigger.new[i].Project__c!=null && trigger.new[i].Project__c!= trigger.old[i].Project__c )
               masterProjectStatusCheck.add(trigger.new[i]);
           if(trigger.new[i].ParentOpportunity__c !=null && trigger.new[i].ParentOpportunity__c != trigger.old[i].ParentOpportunity__c)
               masterProjectStatusCheck.add(trigger.new[i]);
            /*
               Monalisa Project oct 2015
                Calculate the value of Master Projects amount fields in the batch job by checking the tech field of the project object if 
                ->If the opportunity is linked to the different Project Hierarchy.
                        In order to considerthe entire hierarchy linked to Master Project
                        we are considering Tech_MasterProjectId__c instead of project__c. project__c gets only
                        Opportunities directly linked to master project but the Tech_MasterProjectId__c field will
                        get all the opportunities in the entire hierarchy under Master Project
                ->Opportunity Amount is changed.
                ->Opportunity Currency is changed
                ->Opportunity ToBeDeleted__c is changed
                ->Opportunity IncludedInForecast__c is changed
                ->Opportunity status__c is changed (it is changed from cancelled to Lost or viceversa, amount needs to be recalculated hence check on status)
                ->If Stage is changed from/to 0-Closed or 7 - Deliver & Validate              
            */
            if(((Trigger.new[i].Tech_MasterProjectId__c != null && Trigger.new[i].Tech_MasterProjectId__c != '') || (Trigger.old[i].Tech_MasterProjectId__c !=null && Trigger.old[i].Tech_MasterProjectId__c !='')) && (Trigger.new[i].Tech_MasterProjectId__c != Trigger.old[i].Tech_MasterProjectId__c || Trigger.new[i].Amount!= Trigger.old[i].Amount || Trigger.new[i].CurrencyISOCode!= Trigger.old[i].CurrencyISOCode|| Trigger.new[i].ToBeDeleted__c!= Trigger.old[i].ToBeDeleted__c || Trigger.new[i].IncludedInForecast__c!= Trigger.old[i].IncludedInForecast__c || Trigger.new[i].Status__c!= Trigger.old[i].Status__c || (Trigger.new[i].StageName != Trigger.old[i].StageName && (Trigger.new[i].StageName ==System.Label.CLOCT15SLS77 || Trigger.old[i].StageName ==System.Label.CLOCT15SLS77 || Trigger.new[i].StageName ==System.Label.CL00220 || Trigger.old[i].StageName ==System.Label.CL00220) ) ) ){
                 //if customer project is changed then update the checkbox in both project for the amount to be calculated by the batch job
                 if(Trigger.new[i].Tech_MasterProjectId__c != Trigger.old[i].Tech_MasterProjectId__c ){
                     if(Trigger.new[i].Tech_MasterProjectId__c != null && Trigger.new[i].Tech_MasterProjectId__c != ''){
                         prjlist.add(new OPP_Project__c(Id=trigger.new[i].Tech_MasterProjectId__c )); 
                         opplist.add(trigger.new[i]);
                      } 
                      if(Trigger.old[i].Tech_MasterProjectId__c !=null && Trigger.old[i].Tech_MasterProjectId__c !=''){
                         prjlist.add(new OPP_Project__c(Id=trigger.old[i].Tech_MasterProjectId__c ));
                         opplist.add(trigger.new[i]);
                     }  
                 }
                 else if(Trigger.new[i].Tech_MasterProjectId__c !=null){
                     prjlist.add(new OPP_Project__c(Id=trigger.new[i].Tech_MasterProjectId__c ));
                     opplist.add(trigger.new[i]);
                }
            }
            if(prjlist.size()>0)
                AP05_OpportunityTrigger.updateProjectAmountFields(prjlist,opplist);
            if(Utils_SDF_Methodology.canTRigger('AP01'))
            {
                //Checks if the Opportunity Stage is Stage 0 and the Status is Account Not selected or Lost
                //Removed Trigger.old[i].StageName!=Label.CL00220 for BR-8615- March 2016 release
                if(Trigger.new[i].StageName != Trigger.old[i].StageName &&  Trigger.new[i].StageName == Label.CL00272 && (Trigger.new[i].Status__c == Label.CLSEP12SLS14 || Trigger.new[i].Status__c == Label.CLSEP12SLS13))    
                {
                    oppStg.put(Trigger.new[i], 'stge0StatsLost');    
                }
                //Checks if the Opportunity is in Stage 0 and Status equals Cancelled
                //Removed Trigger.old[i].StageName!=Label.CL00220 for BR-8615- March 2016 release
                if(Trigger.new[i].StageName != Trigger.old[i].StageName && Trigger.new[i].StageName == Label.CL00272 && (Trigger.new[i].Status__c == Label.CLOCT13SLS08 || Trigger.new[i].Status__c == Label.CLOCT13SLS09))    
                {
                    oppStg.put(Trigger.new[i], 'stge0StatsCanlld');                
                }

                //Checks if the Opportunity is in Stage 7 and Status equals Won
                if(Trigger.new[i].StageName != Trigger.old[i].StageName && Trigger.old[i].StageName!=Label.CL00272 && Trigger.new[i].StageName == Label.CL00220 && (Trigger.new[i].Status__c == Label.CLOCT13SLS10 || Trigger.new[i].Status__c == Label.CLOCT13SLS11))    
                {
                    oppStg.put(Trigger.new[i], 'stge7StatsWon');                
                }
            }
            if(Utils_SDF_Methodology.canTrigger('AP_Opp_UpdateParentCloseDate'))
            {
                //Checks if the Opportunity Stage is Stage 0 and the Status is Account Not selected or Lost
                if(Trigger.new[i].CloseDate!= Trigger.old[i].CloseDate && TRigger.new[i].ParentOpportunity__c !=null)    
                {
                    oppsGreaterClosedDate.put(trigger.new[i].Id, trigger.new[i]);
                }

            } 
             //Added by Shruti KArn. For PRM October 2013 Release

            if(trigger.new[i].PartnerInvolvement__c !=null)
            {
                if(trigger.new[i].Amount != trigger.old[i].Amount && trigger.old[i].Amount != null)
                    lstOppAmntUpdate.add(trigger.new[i]);
                if(trigger.new[i].StageName != trigger.old[i].StageName)
                    lstOppStageUpdate.add(trigger.new[i]);
            }           
       }
        if(!(masterProjectStatusCheck.isEmpty()))
            AP01_OpportunityFieldUpdate.masterProjectStatusCheck(masterProjectStatusCheck);
        if(!lstOppAmntUpdate.isEmpty() && Utils_SDF_Methodology.canTrigger('AP_SendOpportunityNotification') && Boolean.valueOf(Label.CLOCT13PRM77))
            AP_SendOpportunityNotification.setEmailsonAmountChange(lstOppAmntUpdate);
        if(!lstOppStageUpdate.isEmpty() && Utils_SDF_Methodology.canTrigger('AP_SendOpportunityNotification') && Boolean.valueOf(Label.CLOCT13PRM77))
            AP_SendOpportunityNotification.setEmailsonStatusChange(lstOppStageUpdate);
        // End of PRM October 2013 Release changes
        
        //Updates the Status of the corresponding Opportunity Lines
        if(!(oppStg.isEmpty()))
        {
            AP01_OpportunityFieldUpdate.updateOppLines(oppStg);
        }
        if(!(oppsGreaterClosedDate.isEmpty()))
        {
            AP_Opp_UpdateParentCloseDate.checkForChildrenWithGreaterCloseDate(oppsGreaterClosedDate);
            AP_Opp_UpdateParentCloseDate.updateParentOpportunity(oppsGreaterClosedDate);
        } 
    }
    
    if(VFC61_OpptyClone.isOpptyClone == false)
    {
        if(Utils_SDF_Methodology.canTrigger('AP_SPARequestCalculations_opp')  && VFC61_OpptyClone.isOpptyClone == false)
        {
            Set<Id> tobeProcessedOpportunites=new Set<Id>();
            for(Id opportunityId :Trigger.NewMap.keySet())        
                if(Trigger.oldMap.get(opportunityId).StageName!=System.Label.CLSEP12SLS12 && Trigger.NewMap.get(opportunityId).StageName==System.Label.CLSEP12SLS12)            
                    tobeProcessedOpportunites.add(opportunityId);
                            
            if(tobeProcessedOpportunites.size()>0)
                AP_SPARequestCalculations.updateRelatedQuotaforLostOpportunity(tobeProcessedOpportunites);                
        }
    
        //Added by Shruti Karn for March 2013 -  add sharing of Opportunity Account for Partner Owner 
        // INITIAL VARIABLE DECLEARTIONS & SOQL QUERIES FOR INITIALIZATION      
        set<ID> setOldUserId = new set<ID>();
        list<Id> lstPendingOppID = new list<Id>();
        list<Id> lstRevokedOppId = new list<ID>();
        //END OF VARIABLE DECLERATION

        //FOR LOOP --1    
        for(Id oppID : trigger.newMap.keySet())
        {
            if((trigger.newMap.get(oppID).ownerID != trigger.oldMap.get(oppID).ownerID) || (trigger.newMap.get(oppID).accountid!= trigger.oldMap.get(oppID).accountid))
            {
                setOldUserId.add(trigger.oldMap.get(oppID).ownerid);
            }
        
            //For July 2013 PRM Release - BR# 2974
            if((trigger.newMap.get(oppID).TECH_5daysPending__c != trigger.oldMap.get(oppID).TECH_5daysPending__c) && (trigger.newMap.get(oppID).TECH_5daysPending__c))
            {
                lstPendingOppID.add(oppID);
            }
        
            if((trigger.newMap.get(oppID).TECH_10daysPending__c != trigger.oldMap.get(oppID).TECH_10daysPending__c) && (trigger.newMap.get(oppID).TECH_10daysPending__c))
            {
                lstRevokedOppId.add(oppID);
            }
            //End of change For July 2013 PRM Release - BR# 2974
        }
        // END OF FOR LOOP --1

        //MIDDLE LOGIC    
        map<Id,User> mapOppOWner = new map<Id,User>([Select id,IsPortalEnabled from User where id in :setOldUserId limit 10000]);
        list<Id> lstAccountId = new list<Id>();
        //END OF MIDDILE LOGIC

        //FOR LOOP --2    
        for(Id oppID : trigger.OldMap.keySet())
        {
        
            if(mapOppOWner != null && mapOppOWner.containsKey(trigger.OldMap.get(oppID).OwnerId) && mapOppOWner.get(trigger.OldMap.get(oppID).OwnerId).IsPortalEnabled)
                lstAccountId.add(trigger.OldMap.get(oppID).accountid);           
        }
        // END OF FOR LOOP --2

        //FINAL CALLS TO AP METHODS    
        list<opportunity> lstOpp = new list<opportunity>();
        if(!(setOldUserId.isEmpty()) && (!(lstAccountId.isEmpty())))
            lstOpp = [Select id,ownerid from opportunity where ownerid in : setOldUserId and accountid in :lstAccountId  limit 50000];
    
        if(!lstOpp.isEmpty())
        {
            for(opportunity opp : lstOpp)
            {
                if(setOldUserId.contains(opp.ownerid))
                    setOldUserId.remove(opp.ownerid);
            }
        }
        list<AccountShare> lstDelAccShare = new list<AccountShare>();
        if(!(setOldUserId.isEmpty()) && (!(lstAccountId.isEmpty())))
            lstDelAccShare = [Select id from AccountShare where accountid in :lstAccountId and UserOrGroupId in: setOldUserId and RowCause =: Label.CLMAR13PRM020 limit 10000];
        if(lstDelAccShare != null)
        {
            try
            {
                delete lstDelAccShare;
            }
            catch(Exception e)
            {
                system.debug('Error while deleting Account Sharing:'+e.getMessage());
            }
        }
    
        //For July 2013 Release BR# 2974
        if(!lstPendingOppID.isEmpty())
            AP_Opportunity_SendNotification.sendPendingOppNotification(lstPendingOppID);
        if(!lstRevokedOppId.isEmpty())
            AP_Opportunity_SendNotification.sendRevokeOppNotification(lstRevokedOppId);
        //End of change For July 2013 PRM Release - BR# 2974
    }    
    //END OF CALLS TO AP METHODS       
    if(Utils_SDF_Methodology.canTrigger('PRM_FieldChangeListener')) {
        System.debug('OpportunityAfter Update');
      PRM_FieldChangeListener prmChangeListener = new PRM_FieldChangeListener();
      prmChangeListener.badgeInspecter(trigger.oldMap, trigger.newMap, PRM_FieldChangeListener.OPPORTUNITY_OBJECT_NAME);
    }
    System.Debug('****** OpportunityAfterUpdate Trigger End ****');  
}