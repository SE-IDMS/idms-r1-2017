trigger ATTAfterInsert on AssignedToolsTechnicians__c (After insert,Before insert) {

    List<SVMXC__Service_Order__c> woorder = new List<SVMXC__Service_Order__c>();
    Map<id,SVMXC__Service_Order__c> womap = new Map<id,SVMXC__Service_Order__c>();
    Map<id,AssignedToolsTechnicians__c> technicianMap = new Map<id,AssignedToolsTechnicians__c>();
    List<SVMXC__Service_Order__Share> wordShareLst = new List<SVMXC__Service_Order__Share>();
    list<AssignedToolsTechnicians__c> Attlist1= new  list<AssignedToolsTechnicians__c>();
    Set<id> woidset = new Set<id>();
    Set<id> Technicianset = new Set<id>();
    
    for(AssignedToolsTechnicians__c obj:Trigger.new ){
        woidset.add(obj.WorkOrder__c);
        Technicianset.add(obj.TechnicianEquipment__c);
        Attlist1.add(obj);
    }
    if(trigger.isafter) {
    if(Technicianset != null && Technicianset.size()>0)
    {
        List<AssignedToolsTechnicians__c> attlist = [select id,TechnicianEquipment__r.SVMXC__Salesforce_User__c from AssignedToolsTechnicians__c where TechnicianEquipment__c in :Technicianset ];
        technicianMap.putAll(attlist);
    }
    if(woidset != null && woidset.size()>0)
    woorder  = [select id from SVMXC__Service_Order__c  where (SVMXC__Order_Status__c ='Customer Confirmed' OR SVMXC__Order_Status__c='Acknowledge FSE') and id in: woidset];
    if(woorder != null && woorder.size()>0){
        womap.putAll(woorder);
        for(AssignedToolsTechnicians__c obj:Trigger.new )
        {
            if(womap.containskey(obj.WorkOrder__c))
            {
                AssignedToolsTechnicians__c att = technicianMap.get(obj.id);
                wordShareLst.add(new SVMXC__Service_Order__Share(AccessLevel = 'Edit',ParentId =obj.WorkOrder__c,  UserOrGroupId = att.TechnicianEquipment__r.SVMXC__Salesforce_User__c));
            }
        }
    }
        if(wordShareLst.size()>0) 
        {
            try{
                Database.insert(wordShareLst, false);                                                                              //Inserting Work Order Share details
            }catch (Exception e){system.debug('Exception '+e.getmessage());}
        }
  }
    if(trigger.isbefore) {
    	if(Attlist1.size()>0)
    		AP_ATT_UpdateTechnician.updatePrimaryFSR(Attlist1);
         }
}