/*
    Author          : Accenture IDC Team 
    Date Created    : 26/07/2011
    Description     : Created as part of september release.
*/

trigger QuoteLinkBeforeDelete on OPP_QuoteLink__c (Before Delete) {
if(Utils_SDF_Methodology.canTrigger('AP21'))
  {
  System.Debug('****** QuoteLinkBeforeDelete Trigger Start****');
  list<OPP_QuoteLink__c> quoteLinks= new list<OPP_QuoteLink__c>();
      for(OPP_QuoteLink__c quote:Trigger.old)
      {
          if(quote.ActiveQuote__c)
          {
              if(quote.IssuedQuoteDateToCustomer__c!=null)
              {
                  quoteLinks.add(quote);
              }
          }
      }
      if(quotelinks.size()>0)
      {
          AP21_UpdateOpportunity.updtQteIsuDateToCust(quotelinks,true);
      }
      system.debug('#####Trigger updateTECH_HasActiveQLKwthMargin starts');
      AP21_UpdateOpportunity.updateTECH_HasActiveQLKwthMargin(Trigger.old, Label.CL00750);
      system.debug('#####Trigger updateTECH_HasActiveQLKwthMargin ends');  
  }
}