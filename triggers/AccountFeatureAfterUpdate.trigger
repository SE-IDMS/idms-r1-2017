/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 31 July 2013
    Description : For October 2013 Release:
                 
                 1. Update Contact assigned stand-alone feature when account assigned standalone feature is updated.
                 
    
********************************************************************************************************************/
trigger AccountFeatureAfterUpdate on AccountAssignedFeature__c (after update) {
    //AP_AAF_FeatureUpdateMethods.updateContactFeature(trigger.newMap,trigger.oldMap);
    Map<String , CS_AccountFeaturetoContactFeature__c> mapFieldstoUpdate = new Map<String , CS_AccountFeaturetoContactFeature__c>();
    mapFieldstoUpdate = CS_AccountFeaturetoContactFeature__c.getall();
    map<Id,AccountAssignedFeature__c> mapAccountFeature = new map<Id,AccountAssignedFeature__c>();
    Id standaloneFeatureRecordType = [Select id from RecordType where developername='StandAloneFeature'and SobjectType ='AccountAssignedFeature__c' limit 1].Id;            
    for(AccountAssignedFeature__c newFeature : trigger.new)
    {
        if(newFeature.recordtypeid == standaloneFeatureRecordType)
        {
            for(String field : mapFieldstoUpdate.keySet())
            {
                if(newFeature.get(field) != trigger.oldMap.get(newFeature.Id).get(field))
                    mapAccountFeature.put(newFeature.id,newFeature);
            }
        }
    }
    if(!mapAccountFeature.isEmpty())
        AP_AAF_FeatureUpdateMethods.updateContactFeature(mapAccountFeature);
}