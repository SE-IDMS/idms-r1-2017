/*
    Author          : Ramesh Rajasekaran (Schneider Electric)
    Date Created    : 22-November-2012
    Description     : Cases can be deleted only by the System Administrators and Delegated Administrators.
*/

trigger CaseBeforeDelete on Case (Before Delete) {
  System.Debug('****** CaseBeforeDelete Trigger Start ****');    
    If(Trigger.Old.size()>0){  
            String profileName = [Select Name from Profile where Id =: UserInfo.getProfileId()].Name;       
            If(!(profileName == Label.CL00498 || profileName == Label.CLDEC12CCC012)){                 
                 Trigger.Old[0].addError(Label.CLDEC12CCC10);
            }    
    }     
  System.Debug('****** CaseBeforeDelete Trigger End ****');
}