trigger InvolvedOranizationBeforeUpdate on InvolvedOrganization__c (Before Update)
{
     system.debug('1*******After trigger Before');
     if(Utils_SDF_Methodology.canTrigger('AP_IO_BU'))
       {
            for(InvolvedOrganization__c iOrg : Trigger.new){
         
       if(iOrg.Organization__c != trigger.oldMap.get(iOrg.Id).Organization__c){
                   iOrg.TECH_FromBRE__c=false;
               AP_InvolvedOrganization.populateRepresentative(Trigger.new); 
            }
            else{
                  if(iOrg.Representative__c==null && trigger.oldMap.get(iOrg.Id).Representative__c!=null){
                   // iOrg.Representative__c = trigger.oldMap.get(iOrg.Id).Representative__c; 
                    AP_InvolvedOrganization.populateRepresentative(Trigger.new); 
                    }
            }
            
           }}}