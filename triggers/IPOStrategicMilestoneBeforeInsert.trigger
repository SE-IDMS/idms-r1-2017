trigger IPOStrategicMilestoneBeforeInsert on IPO_Strategic_Milestone__c (before insert) {

  System.Debug('****** IPOStrategicMilestoneBeforeinsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('IPOStrategicProgramBeforeinsert '))
    {
    
     
         IPOStrategicMilestoneTriggers.IPOStrategicMilestoneBeforeInsert(Trigger.new);
         IPOStrategicMilestoneTriggers.IPOStrategicMilestoneBeforeInsertUpdate(Trigger.new);
                          
        if (Trigger.new[0].Validate_insert_update__c == True)
        
                Trigger.new[0].AddError(Label.ConnectGlobalProgramScopeValidateERR);  
                       
        
 }
 System.Debug('****** IPOStrategicProgramBeforeUpdate Trigger End ****'); 
}