/*
    Author          : Yannick Tisserand (Accenture) 
    Date Created    : 22/02/2012
    Description     : Commercial Action Plan After Update event trigger.
*/
trigger CAPAfterUpdate on SFE_IndivCAP__c (after update) {
  List<SFE_IndivCAP__Share> sharesToCreate = new List<SFE_IndivCAP__Share>();
  List<ID> shareIdsToDelete = new List<ID>();
  List<ID> userIds = new List<ID>();
  Map<ID, User> partnerMap = new Map<ID, User>();
 
  // Collect all the user ids
  for(Integer i=0; i < Trigger.new.size() ;i++){
    userIds.add(Trigger.new[i].AssignedTo__c); 
    userIds.add(Trigger.old[i].AssignedTo__c);
  }
  
  // Retrieve all the users in one query
  List<User> users = [select id, userType from User where id in :userIds];
  
  // Identify partner users and save it in a map
  for(User user: users){
  
    if(user.userType == 'PowerPartner'){
      partnerMap.put(user.id, user); 
    } 
  }
  
  // Loop in Caps
  for(Integer i=0; i < Trigger.new.size() ;i++){
  
    // if the assignee has changed
    if(Trigger.new[i].AssignedTo__c != Trigger.old[i].AssignedTo__c){
      
      // if the previous assignee was a partner, need to remove the existing sharing rule
      if(partnerMap.containsKey(Trigger.old[i].AssignedTo__c)){
        shareIdsToDelete.add(Trigger.new[i].id); 
      }
      
      // If the new assignee is a partner, need to create a new sharing rule
      if(partnerMap.containsKey(Trigger.new[i].AssignedTo__c)){
        SFE_IndivCAP__Share sharingRule = new SFE_IndivCAP__Share();
        sharingRule.AccessLevel = 'Read';
        sharingRule.ParentId = Trigger.new[i].id;
        sharingRule.UserOrGroupId = Trigger.new[i].AssignedTo__c;
        sharesToCreate.add(sharingRule);
      }
    }
  }

  // Delete all the existing sharing rules in one query
  if (!shareIdsToDelete.isEmpty()){
    delete [select id from SFE_IndivCAP__Share where ParentId IN :shareIdsToDelete and RowCause = 'Manual'];
  }
 
  // Create all the new sharing rules in one query
  if (!sharesToCreate.isEmpty()){
    insert sharesToCreate;
  }
}