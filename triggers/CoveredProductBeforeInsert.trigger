/*********************************************************************
    Created By Deepak
    For April-14 :-Preventing the creation of Duplicate Covered Product.
    Modified By- Deepak
    For April-15- Populating fields from Installed Product Account.
 *************************************************************************/

trigger CoveredProductBeforeInsert on SVMXC__Service_Contract_Products__c(before insert)
{

    //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SRV_APR_001'))
    {    
        List<SVMXC__Service_Contract_Products__c> cpList1 = New List<SVMXC__Service_Contract_Products__c>();
        // Commented by Nabil ZEGHACHE DEF-7771
        //List<SVMXC__Service_Contract_Products__c> cpList2 = New List<SVMXC__Service_Contract_Products__c>();
        List<SVMXC__Service_Contract_Products__c> cpLst = New List<SVMXC__Service_Contract_Products__c>();
        List<SVMXC__Service_Contract_Products__c> cppLst = New List<SVMXC__Service_Contract_Products__c>();
          set<id> scid = new set<id>(); 
          set<id>cpset1 = new set<id>();
        for(SVMXC__Service_Contract_Products__c cov :trigger.New)
        {  
        
        if(cov.SVMXC__Service_Contract__c !=null){
             cpset1.add(cov.SVMXC__Service_Contract__c);
             cppLst.add(cov);
             }
        
        

         if(cov.SVMXC__Service_Contract__c !=null){
             scid.add(cov.SVMXC__Service_Contract__c);
             }

            if(cov.IncludedService__c !=null && cov.SVMXC__Installed_Product__c !=null && cov.SVMXC__Service_Contract__c !=null )
            {
                cpList1.add(cov);
            }
            if(cov.SVMXC__Installed_Product__c!=null){
              cpLst.add(cov);           
            }
            /*
             * Added by Nabil ZEGHACHE on 23-Feb-2015 to check address consistency
             */
            /*
            if(cov.SVMXC__Installed_Product__c !=null && cov.SVMXC__Service_Contract__c !=null )
            {
                cpList2.add(cov);
            }
            */
            /*
             * End Added by Nabil ZEGHACHE on 23-Feb-2015 to check address consistency
             */

        }
        if(cpList1.size()>0)
        {
            AP_CoveredProducttrigger.AddError(cpList1);
        }
        if(cplst !=null){
            AP_CoveredProduct.PopulatingAccountFields(cplst);  
        }
        
       /* if(scid!=null && scid.size()>0){
        AP_CoveredProduct.PMvisitUpdateOnCoveredProduct(scid);
        }*/
        if(cpset1!=null)
        AP_CoveredProduct.Slatermload(cpset1,cppLst);
        /*
         * Added by Nabil ZEGHACHE
         * Date: 23-Feb-2015
         * Purpose: avoid adding covered products with addresses not consistent at the Service Lines level
         */
        /*
        System.debug('### Going to check address consistency');
        if(cpList2.size()>0)
        {
            System.debug('#### calling AP_InstalledProductAssociationHandler.checkAddressConsistency:'+cpList2);
            AP_IPAssociationHandler.checkAddressConsistency(cpList2);
        }
		*/
        /*
         * End added by Nabil ZEGHACHE on 23-Feb-2015
         */

    }
}