trigger CountryChannelsBeforeinsert on CountryChannels__c (before insert) {

   Map<Id,Map<Id,Set<Id>>> conChMap = AP_CountryChannels_Records.getAllChannels();    
    for(CountryChannels__c cCh:trigger.new){
        if(cCh.Active__c && conChMap.containsKey(cCh.Country__c)){
            if(conChMap.get(cCh.Country__c).containsKey(cCh.Channel__c)){
                if(cCh.SubChannel__c == Null )
                cCh.addError(System.Label.CLAPR15PRM164); //This Channel Or Sub-Channel already exist. Please choose a different Channel.
                else if(conChMap.get(cCh.Country__c).get(cCh.Channel__c).contains(cCh.SubChannel__c))
                cCh.addError(System.Label.CLAPR15PRM164); //This Channel Or Sub-Channel already exist. Please choose a different Channel.
            }  
            else
               conChMap.get(cCh.Country__c).put(cCh.Channel__c,new Set<Id>{cCh.SubChannel__c}); 
        }
        else if(cCh.Active__c ){
        
             map<Id,Set<id>> mapChannel = new map<Id,Set<id>>();
                mapChannel.put(cCh.Channel__c,new Set<Id>{cCh.SubChannel__c});
                conChMap.put(cCh.Country__c, mapChannel);
         
        }
    }
   
  }