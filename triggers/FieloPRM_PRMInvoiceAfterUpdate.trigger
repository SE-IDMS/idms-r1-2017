/******************************************
* Developer: Fielo Team                   *
*******************************************/
trigger FieloPRM_PRMInvoiceAfterUpdate on FieloPRM_Invoice__c (After Update) {

    if(Utils_SDF_Methodology.canTrigger('FieloPRM_AP_PRMInvoiceTriggers')){
        FieloPRM_AP_PRMInvoiceTriggers.updatesCurrency();
        FieloPRM_AP_PRMInvoiceTriggers.processInvoiceDetails();
    } 
   
}