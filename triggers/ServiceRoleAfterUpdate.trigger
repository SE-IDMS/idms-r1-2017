/*     
@Authore-Deepak
Created-05-03-14(Release)
===========
Scenarios:
===========
1.Purpose Logic to Update Contact's TECH_IsSVMXRecordPresentForContact__c 
2.Update Account TechSVMXRecordPresentForAccount__c for July 14 Release
*/
trigger ServiceRoleAfterUpdate on Role__c (after update) 
{
    Set<Id> ConId = new Set<Id>();
    Set<Id> AccId = new Set<Id>();
    for (Role__c ro: trigger.new)
    {
        //Scenario:1
        if(ro.Contact__c!=null) 
        ConId.add(ro.Contact__c);
        //Scenario:2
        if(ro.Account__c!=null) 
        AccId.add(ro.Account__c);
    }
    //Scenario:1
    if(ConId!= null && ConId.size()>0)
    AP_IsSVMXRelatedContact.UpdateContact(ConId);
    //Scenario:2
    if(AccId!= null && AccId.size()>0)
    SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
}