/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 02-Oct-2012
    Description     : Connect Cascading KPI Before Update event trigger.
*/

trigger CascadingKPITargetBeforeUpdate on Cascading_KPI_Target__c (before update) {

System.Debug('****** CascadingKPITargetBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CascadingKPITargetBeforeInsert'))
    {
    ConnectCascadingKPITargetTriggers.CascadingKPITargetCountryChampionBeforeInsertUpdate(Trigger.new[0]);
     ConnectCascadingKPITargetTriggers.CascadingKPITargetBeforeInsertUpdate_ValidateScopeEntity(Trigger.new);
     if((Trigger.new[0].Entity__c != Trigger.old[0].Entity__c) || (Trigger.new[0].Scope__c != Trigger.old[0].Scope__c) || (Trigger.new[0].GSC_Region__c != Trigger.old[0].GSC_Region__c)|| (Trigger.new[0].Partner_Sub_Entity__c != Trigger.old[0].Partner_Sub_Entity__c)|| (Trigger.new[0].Smart_Cities_Division__c != Trigger.old[0].Smart_Cities_Division__c || (Trigger.new[0].CountryPickList__c != Trigger.old[0].CountryPickList__c))){
       ConnectCascadingKPITargetTriggers.ConnectEntityKPIBeforeInsertUpdate_ValidateEntityRecord(Trigger.new);
       
       if (Trigger.new[0].ValidateEntityInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.ConnectEntityKPIScopeDuplicateERR);  
       
       }
     if (Trigger.new[0].ValidateScope__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_CascadingKPI_InsertUpdate_ERR1);   
                 
                    
          
    }
 System.Debug('****** CascadingKPITargetBeforeInsert Trigger End ****'); 
 }