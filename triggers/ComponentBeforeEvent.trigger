trigger ComponentBeforeEvent  on FieloEE__Component__c (before insert,before update) {

    if(Utils_SDF_Methodology.canTrigger('AP_PRMUtils')){
        AP_PRMUtils.CheckRichTextFields(trigger.new, String.valueOf(trigger.new.getSObjectType()));
    }
    

    if(trigger.isUpdate && Utils_SDF_Methodology.canTrigger('FieloPRM_AP_ComponentTriggers')){
        FieloPRM_AP_ComponentTriggers.topSectionCheckChangeLayout(trigger.new, trigger.oldmap);
    }
}