/*
Author: Siddharth Nagavarapu (GD Solutions)
Purpose: Prevent users from creating new groups
BusinessRequirement : BR-2569
Technical and Functional Specification : https://na7.salesforce.com/069A0000000t1sg
*/
trigger CollaborationGroupTrigger on CollaborationGroup (before insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_CollaborationGroup')){  
    if(Trigger.isInsert && Trigger.isBefore){
        AP_CollaborationGroupTriggerHandler.OnBeforeInsert(Trigger.new);
    }   
}
}