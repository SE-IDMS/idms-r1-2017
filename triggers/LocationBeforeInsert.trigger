/*
@Author: Jyotiranjan Singhlal
Created Date: 08-04-2013
Description: This trigger fires on Location on Before Insert
**********
Scenarios:
**********
1.  Update Location Address details from the Account
2.  On Insert of Child Location, update fields from the Parent Location.

Date Modified       Modified By         Comments
------------------------------------------------------------------------
17-07-2013          Ramu Veligeti       Logic to update Account's TECH_IsSVMXRecordPresent__c field
16-09-2013          Deepak              Logic to Put Validation for no two location can be primary for a Account
*/
trigger LocationBeforeInsert on SVMXC__Site__c (before insert) 
{
     //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX21'))
    {
    
    //Variablees related to Scenario 1 
    List<SVMXC__Site__c> loc1 = new List<SVMXC__Site__c>();    
    Set<id> locacc = New Set<id>();
    
    //Variablees related to Scenario 2
    List<SVMXC__Site__c> loc2 = new List<SVMXC__Site__c>();
    List<SVMXC__Site__c> loc3 = new List<SVMXC__Site__c>();    
    Set<id> plocId = New Set<id>();

  // trigger applies only for standalone users
  if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLNOV13SRV03)) {
         
    
    for(SVMXC__Site__c a:trigger.new)
    {
        //Condition for Scenario 1
        if(a.SVMXC__Account__c != Null)              
        {
            loc1.add(a);
            locacc.add(a.SVMXC__Account__c);        
        } 
        
        //Condition for Scenario 2
        if(a.SVMXC__Parent__c != Null)              
        {
            loc2.add(a);
            plocId.add(a.SVMXC__Parent__c);        
        }
        
    }
    /*******************************************************************************************
    Scenario: 1
    Update Location Address details from the Account
    *******************************************************************************************/
    list<Account> acc = [Select Id,Name,Street__c,City__c,ZipCode__c,Country__c,
            StateProvince__c, AutomaticGeoLocation__Latitude__s, AutomaticGeoLocation__Longitude__s, (select id from SVMXC__Sites__r  where PrimaryLocation__c=true) From Account Where Id IN :locacc];
    //Srinivas - Oct 13-  Check Primary location
    map<id,Account> mpAcc = new map<id,Account>(acc);
    system.debug(mpAcc);
    for(Account ac :acc)   
    {
        system.debug('#### ACC: '+ac);
        system.debug('####'+ac.SVMXC__Sites__r);
        system.debug('##Sites1 - '+mpAcc.get(ac.id).SVMXC__Sites__r);
        for(SVMXC__Site__c lo1:loc1)
        {
            if(lo1.SVMXC__Account__c == ac.Id)
            {
                lo1.SVMXC__Street__c = ac.Street__c;
                lo1.SVMXC__City__c = ac.City__c;
                lo1.StateProvince__c = ac.StateProvince__c;
                lo1.SVMXC__Zip__c = ac.ZipCode__c;
                lo1.LocationCountry__c = ac.Country__c;
                lo1.SVMXC__Latitude__c = ac.AutomaticGeoLocation__Latitude__s;
                lo1.SVMXC__Longitude__c = ac.AutomaticGeoLocation__Longitude__s;
            }   
        }   
    }
    /**********************************************************************************
     Scenario: 2
     On Insert of Child Location, update fields from the Parent Location.
    **********************************************************************************/
    if(loc2.size()>0 && plocId.size()>0)
      {
          //Fetching Records from the Parent Location 
          Map<Id,SVMXC__Site__c> locmap = new Map<Id,SVMXC__Site__c>([select Id,SVMXC__Account__c,SVMXC__Street__c,
          SVMXC__City__c,StateProvince__c,SVMXC__Zip__c,LocationCountry__c,SVMXC__Latitude__c, SVMXC__Longitude__c, TimeZone__c from SVMXC__Site__c where Id IN :plocId]);
          for(SVMXC__Site__c a:loc2)
          {
              if(locmap.containsKey(a.SVMXC__Parent__c))
              {
                  //Assigning Parent Location values to the Child Location
                  SVMXC__Site__c ploc = new SVMXC__Site__c();
                  ploc = locmap.get(a.SVMXC__Parent__c);
                  
                  a.SVMXC__Parent__c = ploc.Id;
                  a.SVMXC__Account__c = ploc.SVMXC__Account__c;
                  a.SVMXC__Street__c = ploc.SVMXC__Street__c;                
                  a.SVMXC__City__c = ploc.SVMXC__City__c;
                  a.StateProvince__c = ploc.StateProvince__c;
                  a.SVMXC__Zip__c = ploc.SVMXC__Zip__c;
                  a.LocationCountry__c = ploc.LocationCountry__c;
                  a.TimeZone__c = ploc.TimeZone__c;
                  a.SVMXC__Latitude__c= ploc.SVMXC__Latitude__c;
                  a.SVMXC__Longitude__c= ploc.SVMXC__Longitude__c;
              }                       
          }
      }
    /*
        Added by: Ramu Veligeti
        Date: 17/07/2013
        Update TECH_IsSVMXRecordPresent__c on Account object if the Account is used in 
        ServiceMax related objects like Work Order, Installed Product, Location, Service Contract etc..
    */
    Set<Id> AccId = new Set<Id>();
    for(SVMXC__Site__c a:trigger.new)
    {
        //system.debug('##Sites - '+mpAcc.get(a.SVMXC__Account__c).SVMXC__Sites__r);
        //Srinivas -Oct 13 -PrimaryLocation setting
        if(a.SVMXC__Account__c!=null && a.SVMXC__Parent__c == null && mpAcc.get(a.SVMXC__Account__c) != null && mpAcc.get(a.SVMXC__Account__c).SVMXC__Sites__r.size() == 0 )
            a.PrimaryLocation__c = true;

        if(a.SVMXC__Account__c!=null) AccId.add(a.SVMXC__Account__c);
    }    
    SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
    /**********************************************************************************
     Scenario: 3
     On Insert of  Location, Check the primary Location field.
    **********************************************************************************/
   
    set<Id> acid = New set<Id>();
    set<Id> locid = New set<Id>();
    
    for(SVMXC__Site__c loc :loc1)
    {   
        if(loc.PrimaryLocation__c==true)
        {
            acid.add(loc.SVMXC__Account__c);
            locid.add(loc.Id);         
        }       
    }
    loc2=[Select Id,Name,PrimaryLocation__c From SVMXC__Site__c Where SVMXC__Account__c =:acid AND     PrimaryLocation__c =true AND Id !=:locid];
    
        for(SVMXC__Site__c loc :loc1)
          {  system.debug('11111');
              if(loc2.size()>0 && loc.SVMXC__Account__c !=null)
                 {      
                        loc.addError('Primary Location for this Account  has been already created');
                 }
          }
     }//Applies for standalone users
    
    if(Utils_SDF_Methodology.canTrigger('AP_LocationManager')){
    system.debug('###RHA trigger beforeHandler enter');
    AP_LocationManager.beforeHandler(Trigger.new,Trigger.newMap);}  
    }
 }