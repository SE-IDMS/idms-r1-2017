trigger IPOCascadingMilestoneBeforeUpdate on IPO_Strategic_Cascading_Milestone__c (before update) {

  System.Debug('****** IPOCascadingMilestoneBeforeUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('CascadingMilestoneBeforeUpdate'))
    {
        IPOCascadingMilestoneTriggers.IPOStrategicCascadingMilestoneBeforeUpdate(Trigger.New,Trigger.old);
        IPOCascadingMilestoneTriggers.IPOCascadingMilestoneUpdate_StatusUpdate(Trigger.New);
        if((Trigger.new[0].Entity__c != Trigger.old[0].Entity__c) || (Trigger.new[0].Scope__c != Trigger.old[0].Scope__c))
        IPOCascadingMilestoneTriggers.IPOCascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity(Trigger.New);
        IPOCascadingMilestoneTriggers.IPOCascadingMilestoneBeforeInsertUpdate(Trigger.New);
        IPOCascadingMilestoneTriggers.IPOCascadingMilestoneBeforeUpdate(Trigger.New,Trigger.old);
           if (Trigger.new[0].ValidateScopeonInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_Cascading_Milestone_InsertUpdate_ERR1);
                 
           if (Trigger.new[0].ValidateDeployementLeader__c == True)
        
                 Trigger.new[0].Name.AddError(Label.connect_Deployment_LeaderERR);
 }
 System.Debug('****** ConnectCascadingMilestoneBeforeInsert Trigger End ****'); 
}