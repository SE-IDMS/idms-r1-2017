/*
    Author          : ACN 
    Date Created    : 24/07/2011
    Description     : Task After Insert event trigger.
    
    14-June-2012    Srinivas Nallapati  Sep12 Mkt Release       BR-1771:
*/
trigger TaskAfterInsert on Task (after insert) 
{
    boolean blnCanTriggerAP25 = false;
    boolean blnCanTriggerAP36 = false;
    boolean blnCanTriggerAPCaseHandler = false;
    boolean blnCanTriggerTaskHandler = false;

    blnCanTriggerAP25 = Utils_SDF_Methodology.canTrigger('AP25');
    blnCanTriggerAP36 = Utils_SDF_Methodology.canTrigger('AP36');
    blnCanTriggerAPCaseHandler = Utils_SDF_Methodology.canTrigger('AP_Case_CaseHandler');
    blnCanTriggerTaskHandler = Utils_SDF_Methodology.canTrigger('VFC_TaskHandler');

    if(blnCanTriggerAP25 || blnCanTriggerAP36 || blnCanTriggerAPCaseHandler || blnCanTriggerTaskHandler){
        System.Debug('****** TaskAfterInsert Trigger - Starts ****');  
        Map<Id,Task> newTasks= new Map<Id,Task>();     
        String leadKeyPrefix = SObjectType.Lead.getKeyPrefix();  

        Map<Id,Task> newOpptyTasks= new Map<Id,Task>();
        Map<Id,Task> tasks= new Map<Id,Task>();
        String OpptyKeyPrefix = SObjectType.Opportunity.getKeyPrefix();
        Map<Id,Id> opptyTaskIds = new Map<Id,Id>();

        String RMAKeyPrefix = SObjectType.RMA__c.getKeyPrefix(); 
        List<Task> RMATasksList = new List<Task>();

        String strCaseKeyPrefix = SObjectType.Case.getKeyPrefix(); 
        Set<Id> setCaseIdsForLastActivityDate = new Set<Id>();
        Set<Id> setCaseIdsForLogicalResponse = new Set<Id>();
        Set<Id> setTaskIds = new Set<Id>();
       
        
        for(Task task: Trigger.new){
            if(task.WhoId!=null){
                //Srinivas Nallapati  Change for BR-1771, Update lead status only when 'Call' tasks are created
                if((String.valueOf(task.WhoId)).startsWith(leadKeyPrefix) && (task.TECH_TaskType__c == Label.CLSEP12MKT01 || task.TECH_TaskType__c == null)) {
                    newTasks.put(task.Id, task);
                }      
            }

            System.Debug('****** TaskAfterInsert Trigger - First Activity Date for an Opportunity Begins ****'); 
            //For April 2014 Release BR-4616, Create "First Activity" tech Field on Opportunity (date when first event or task was created)
            if((task.WhatId!=null) && (String.valueOf(task.WhatId)).startsWith(OpptyKeyPrefix)){
                opptyTaskIds.put(task.Id,task.WhatId);
                tasks.put(task.Id,task);
            }
            else if((task.WhatId!=null) &&(String.valueOf(task.WhatId)).startsWith(RMAKeyPrefix )&& task.Subject.startsWith('Email')){            
                RMATasksList.add(task);      
            }
            //BR-7933 Exclude Last Activity Date on Case, if Call Type is defined. (Warm Transfer Issue) 
            else if((task.WhatId!=null) && (String.valueOf(task.WhatId)).startsWith(strCaseKeyPrefix) && task.CallType==null){
                setCaseIdsForLastActivityDate.add(task.whatId);
            }
            //BR-9545 – First logical Response
            else if((task.WhatId!=null) && (String.valueOf(task.WhatId)).startsWith(strCaseKeyPrefix) && task.CTICallType__c=='Outbound'){
                setCaseIdsForLogicalResponse.add(task.whatId);
            }
      
        }
        if(blnCanTriggerAP25){
            if(!(newTasks.isEmpty()))
                AP25_Task.insertTasks(newTasks); 
            Map<Id,Opportunity> opptyCreatedDate = new Map<Id,Opportunity>([SELECT CreatedDate FROM Opportunity WHERE Id IN :opptyTaskIds.values()]); 
            for(Id taskId: opptyTaskIds.keySet()){
                if( opptyCreatedDate.get(opptyTaskIds.get(taskId)).CreatedDate.date() > Date.valueOf(Label.CLAPR14SLS62)){
                    newOpptyTasks.put(taskId,tasks.get(taskId));
                }
            }              
            System.Debug('----- AP25_Task <- NewOpptyTasks '+newOpptyTasks);
            if(!(newOpptyTasks.isEmpty()))
                AP25_Task.insertOpptyTasks(newOpptyTasks);
            System.Debug('****** TaskAfterInsert Trigger - First Activity Date for an Opportunity Ends ****');   
        }
        if(blnCanTriggerAP36){
          if(RMATasksList.size()>0){
            AP36_UpdateCaseAfterSentRMA.insertCaseEmail(RMATasksList);  
          }
        }
        if(blnCanTriggerAPCaseHandler){
            if(setCaseIdsForLastActivityDate.size()>0){
                AP_Case_CaseHandler.updateLastActivityDate(setCaseIdsForLastActivityDate);
            }
            if(setCaseIdsForLogicalResponse.size()>0){
                AP_TaskHandler.calculateFirstResponseOnOutboundCall(setCaseIdsForLogicalResponse);
            }
        }
        
        System.Debug('****** TaskAfterInsert Trigger - Ends****'); 
    }     
}