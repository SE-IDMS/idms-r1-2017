/*
@Author: Jyotiranjan Singhlal
Created Date: 11-04-2013
Description: This trigger fires on Installed Product on Before Insert
**********
Scenarios:
**********
1.  Update Installed Product Account Details from the Location

Date Modified       Modified By         Comments
------------------------------------------------------------------------
17-07-2013          Ramu Veligeti       Logic to update Account's TECH_IsSVMXRecordPresent__c field
*/
trigger InstalledProductBeforeInsert on SVMXC__Installed_Product__c (before insert) 
{
    if(Utils_SDF_Methodology.canTrigger('SVMX_InstalledProduct2'))
    {
        if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLNOV13SRV03)) {
        
            //Variablees related to Scenario 1 
            List<SVMXC__Installed_Product__c> ip1 = new List<SVMXC__Installed_Product__c>();    
            Set<id> iploc = New Set<id>();
            Set<SVMXC__Installed_Product__c> setInstalledProductsWithMissingProductInfo=new Set<SVMXC__Installed_Product__c>();
            
            string INFORTECHNOBUSINESS =System.label.CLDEC15SRV05;
            string IT =System.label.CLDEC15SRV04;
            
            for(SVMXC__Installed_Product__c a:trigger.new)
            
            {
            
            if(a.AutoCalcManufacturingDate__c !=null && a.SVMXC__Serial_Lot_Number__c!=null && (/*a.ITBInstalled_Product__c == IT ||*/a.ITBInstalled_Product__c == INFORTECHNOBUSINESS))
                    {
              
                    a.ManufacturingUnitDate__c = a.AutoCalcManufacturingDate__c;
                    
                    }
                //Condition for Scenario 1
                if(a.SVMXC__Site__c!= Null)              
                {
                    ip1.add(a);
                    iploc.add(a.SVMXC__Site__c);        
                } 
                if(a.SVMXC__Product__c==null && a.Brand2__c!=null && a.DeviceType2__c!=null && a.Category__c!=null)
                setInstalledProductsWithMissingProductInfo.add(a);
                
                
            }
        
            //if(setInstalledProductsWithMissingProductInfo.size()>0)
               //AP_InstalledProductBeforeInsertHandler.calculateProductInfo(setInstalledProductsWithMissingProductInfo);
               
            /*******************************************************************************************
            Scenario: 1
            Update Account Details from the Location
            *******************************************************************************************/
            // Commented by Hari Krishna  for IB Connector in October Release
            
            
            list<SVMXC__Site__c> loc = [Select Id,Name,SVMXC__Account__c,Tolabel(SVMXC__Location_Type__c)
            From SVMXC__Site__c Where Id IN :iploc];
        
            for(SVMXC__Site__c lo1:loc)
            {          
                for(SVMXC__Installed_Product__c ip:ip1)
                {
                    if(ip.SVMXC__Site__c == lo1.Id)
                    {
                        ip.LocationType__c = lo1.SVMXC__Location_Type__c;
                    }   
                }
            }
            /*
            Added by: Ramu Veligeti
            Date: 17/07/2013
            Update TECH_IsSVMXRecordPresent__c on Account object if the Account is used in 
            ServiceMax related objects like Work Order, Installed Product, Location, Service Contract etc..
            */
            Set<Id> AccId = new Set<Id>();
            for(SVMXC__Installed_Product__c a:trigger.new)
            if(a.SVMXC__Company__c!=null) AccId.add(a.SVMXC__Company__c);
                SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
        }
    //}

        if(Utils_SDF_Methodology.canTrigger('SVMX_InstalledProduct2'))
        AP_InstalledProductManager.beforeHandler(Trigger.new, Trigger.newMap);
        /********************************************************************************
        Added by: Deepak
        Date: 18/02/2014(April-14)
        Purpose-Asset Categorisation
        *********************************************************************************/
        // Commented on May Release 
        // Logic moved to Batch_IPUpdate
        /*
        set<Id> ipid = New set<Id>();
        List<SVMXC__Installed_Product__c> iplist = new List<SVMXC__Installed_Product__c>();

        for(SVMXC__Installed_Product__c a:trigger.new)
        {
            if(a.LifeCycleStatusOfTheInstalledProduct__c!=null ||a.DecomissioningDate__c !=null|| a.UnderContract__c==true||
            a.SVMXC__Company__c !=null )
            {
                iplist.add(a);
                ipid.add(a.Id);
            }
        }
        if(iplist.size()>0 && ipid !=null)
            Ap_Installed_Product.AssetCategory(iplist);
        */
        /*
        BR-4237
        Putting Validation Rule to make a unique IB(No Duplicate with same specification)
        */
        List<SVMXC__Installed_Product__c> iplst = new List<SVMXC__Installed_Product__c>();
        //Added for Onfly Location Creation
        Set<id> accidset = New Set<id>();
        List<SVMXC__Installed_Product__c> iplistToCrLoc = new List<SVMXC__Installed_Product__c>();
        for(SVMXC__Installed_Product__c ip :trigger.new)
        {
            if(ip.Brand2__c!=null &&ip.DeviceType2__c !=null && 
                       ip.Category__c !=null &&ip.SVMXC__Serial_Lot_Number__c!=null)// && ip.SVMXC__Product__c !=null)
            {
                iplst.add(ip);
            }
            if(ip.SVMXC__Date_Shipped__c == null && ip.TECH_SDHPublisherMaster__c == System.Label.CLOCT15SRV01)
            {
                ip.SVMXC__Date_Shipped__c = System.Today();//Added for Defect#Def-7448
            }
            if(ip.SVMXC__Site__c == Null && ip.SVMXC__Company__c !=null){
                accidset.add(ip.SVMXC__Company__c);
                iplistToCrLoc.add(ip);
            }
        }
        if(iplst.size()>0){
            Ap_Installed_Product.IBValidation(iplst);
        }
        //Calling method for crating Loction if there is no location
        if(accidset.size()>0 && iplistToCrLoc.size()>0 && accidset !=null){
            Ap_Installed_Product.CreateLocation(iplistToCrLoc,accidset);
        }
    }
}