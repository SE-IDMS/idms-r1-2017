/*    Author        : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 02-Oct-2012
    Description     : Connect Global KPI After Update event trigger.
*/

trigger GlobalKPITargetAfterUpdate on Global_KPI_Targets__c (after update) {

System.Debug('****** GlobalKPITargetAfterUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('GlobalKPITargetAfterUpdate'))
    {
   //if(Trigger.old[0].Global_Data_Reporter__c <> Trigger.new[0].Global_Data_Reporter__c)
     ConnectGlobalKPITargetTriggers.ConnectGlobalKPITargetAfterUpdate(Trigger.new,Trigger.old[0]);   
        
    }
 System.Debug('****** GlobalKPITargetAfterUpdate Trigger End ****'); 
 }