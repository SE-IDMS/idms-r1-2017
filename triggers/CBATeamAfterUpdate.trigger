trigger CBATeamAfterUpdate on CBATeam__c (After update) {

    if(Utils_SDF_Methodology.canTrigger('AP_SharingAccessToCBATeam')|| Test.isRunningTest())
    {
    
        System.Debug('****** CBATeamAfterUpdate Trigger Start****');
        Map<id,CBATeam__c> CBATeamMap = new Map<id,CBATeam__c>();
       
        for(CBATeam__c cbaObj:Trigger.new)
        {
            if(cbaObj.CBAMember__c != trigger.Oldmap.get(cbaObj.id).CBAMember__c)
            {
                CBATeamMap.put(cbaObj.id,cbaObj); 
            }
        }
        
        if(CBATeamMap.size()>0)
        {
            AP_SharingAccessToCBATeam.updateCBATeamMember(CBATeamMap,Trigger.oldMap);
        }
    
        System.Debug('****** CBATeamAfterUpdate Trigger End****');
        
    }

}