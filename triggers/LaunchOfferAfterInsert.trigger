trigger LaunchOfferAfterInsert on Offer_Lifecycle__c (after Insert) {

    System.Debug('********* OfferAfterInsert Trigger Start *********');
    set<id> offerids = new set<id>();
    set<string> setboxOfferId = new  set<string>();
    List<Offer_Lifecycle__c> lstolc = new List<Offer_Lifecycle__c >();
    List<Milestone1_Project__c> lstProject = new List<Milestone1_Project__c >();
    List<Milestone1_Milestone__c> lstMilestone = new List<Milestone1_Milestone__c >();
    List<Milestone1_Task__c> lstTask = new List<Milestone1_Task__c >();
    Id offerlaunchid = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Launch - Product').getRecordTypeId();
    Id offersolutionlaunchid = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Launch - Solution').getRecordTypeId();
    Id offerwithdrawalid = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Withdrawal - Product').getRecordTypeId();
    
    Id projectofferlaunchid = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('BU Master Launch Project').getRecordTypeId();
    Id projectofferwithdrawalid = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('BU Master Withdrawal - Product Project').getRecordTypeId();
    Id projectoffersolutionlaunchid = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('BU Offer Solution Launch Project').getRecordTypeId();
          if(Utils_SDF_Methodology.canTrigger('AP_Offer3')) {
            
        for (Offer_Lifecycle__c newOffer : Trigger.new){  
            offerids.add(newOffer.id);
            if(newOffer.RecordTypeId==offerlaunchid) { //Offer Launch - Product Record type Id           
                 Milestone1_Project__c BUProj = new Milestone1_Project__c(
                 Name = Label.CLAPR15ELLAbFO04+' - ' + newOffer.Offer_Name__c,
                                          
                 Offer_Launch__c = newOffer.id, RecordTypeId=projectofferlaunchid);
                 lstProject.add(BUProj); 
             } 
             if(newOffer.RecordTypeId==offerwithdrawalid)  {
                  Milestone1_Project__c BUProj = new Milestone1_Project__c(
                 Name = Label.CLOCT14COLAbFO11  +' - '+ newOffer.Withdrawal_Name__c,
                                           
                 Offer_Launch__c = newOffer.id, RecordTypeId=projectofferwithdrawalid);
                 lstProject.add(BUProj); 
             }  
                if(newOffer.RecordTypeId==offersolutionlaunchid)  {
                  Milestone1_Project__c BUProj = new Milestone1_Project__c(
                 Name = Label.CLOCT14COLAbFO21  +' - '+ newOffer.Solution_Name__c,
                                           
                 Offer_Launch__c = newOffer.id, RecordTypeId=projectoffersolutionlaunchid);
                 lstProject.add(BUProj); 
             }
        
                
        }
        system.debug('HHHH'+lstProject);
        if(lstProject.size() > 0) {
             Insert lstProject;  
             
        }  
        Map<id,Milestone1_Project__c>  mapwithMilestone = new Map<id,Milestone1_Project__c>();
        
        if(lstProject.size() > 0) {
        for(Offer_Lifecycle__c newOffer : Trigger.new) {
            if(newOffer.RecordTypeId==offerwithdrawalid) {
                for(Milestone1_Project__c mpObj:lstProject) {
                    if(mpObj.Offer_Launch__c ==newOffer.id) {
                    
                        mapwithMilestone.put(mpObj.id,mpObj);
                    }
                
                
                }
            }else if(newOffer.RecordTypeId==offerlaunchid) {
                for(Milestone1_Project__c mpObj:lstProject) {
                    if(mpObj.Offer_Launch__c ==newOffer.id) {
                    
                        mapwithMilestone.put(mpObj.id,mpObj);
                    }
                
                
                }
            }
            else if(newOffer.RecordTypeId==offersolutionlaunchid) {
                for(Milestone1_Project__c mpObj:lstProject) {
                    if(mpObj.Offer_Launch__c ==newOffer.id) {
                    
                        mapwithMilestone.put(mpObj.id,mpObj);
                    }
                
                
                }
            }
            
           } 
        }
        
        if(mapwithMilestone.size()>0) {
            AP_bFO_COLA_OfferCountryBUTaskCreation.offBUPMPTaskCreation(mapwithMilestone,'BU');
        
        }
          
    Set<String> setOfferId = new Set<String>();
    Set<String> setOfferCycleId = new Set<String>();
    Map<String, List<Milestone1_Project__c>> MileStoneMap= new  Map<String, List<Milestone1_Project__c>>();
    List<Milestone1_Project__c>  UpdateMProjectList1 = new List<Milestone1_Project__c>();
    Map<string ,Milestone1_Project__c>  UpdateMProjectList = new Map<string ,Milestone1_Project__c>();
    Map<string ,Milestone1_Project__c>  UpdateMProjectMap = new Map<string ,Milestone1_Project__c>();
    
    
    if(Trigger.IsInsert && Trigger.Isafter) {
    
        for(Offer_Lifecycle__c obj:trigger.new) {
          
                setOfferId.add(obj.id);
           
        }
            
        if(setOfferId.size() > 0 ) {
            for(Milestone1_Project__c mpObj:[select id,TECH_IsOfferCycleApproved__c,Product_Picture__c,Offer_Launch__c from Milestone1_Project__c where  Offer_Launch__c=:setOfferId]) {
                if(MileStoneMap.containsKey(mpObj.Offer_Launch__c)) {
                    MileStoneMap.get(mpObj.Offer_Launch__c).add(mpObj);
                
                }else {
                    MileStoneMap.put(mpObj.Offer_Launch__c,new List<Milestone1_Project__c>()); 
                    MileStoneMap.get(mpObj.Offer_Launch__c).add(mpObj);

                }

            }
            for(Offer_Lifecycle__c obj:trigger.new) {
                if(MileStoneMap.containskey(obj.id)) {
                     for(Milestone1_Project__c mpObj1:MileStoneMap.get(obj.id)){
                        if(obj.BD_Status__c=='Published' || obj.Withdrawal_Notice_Status__c=='Published'){
                            mpObj1.TECH_IsOfferCycleApproved__c=true;
                            UpdateMProjectList.put(mpObj1.id,mpObj1);
                        }
                        
                       /* if(obj.Product_Picture__c!=null) {
                             mpObj1.Product_Picture__c=obj.Product_Picture__c;
                             UpdateMProjectList.put(mpObj1.id,mpObj1);
                        }*/
                        if((obj.Launch_Owner__c!=null ) ) {
                            mpObj1.TECH_Offer_and_Withdrawal_Owner_Email__c=obj.LaunchOwnerEmail__c;
                            UpdateMProjectList.put(mpObj1.id,mpObj1);

                        }else if( obj.Withdrawal_Owner__c!=null ) {
                            mpObj1.TECH_Offer_and_Withdrawal_Owner_Email__c=obj.LaunchOwnerEmail__c;
                            UpdateMProjectList.put(mpObj1.id,mpObj1);
                        }
                      
                     }
                
                }
            }
            if(UpdateMProjectList.size() >0 ) {
                AP_bFO_COLA_OfferCountryBUTaskCreation.IsRec=true;
                update UpdateMProjectList.values(); 
            }
            
        }
    }
//April 2015 Release Box API call 

    IF(!Test.isRunningTest()){
        if(Trigger.IsInsert && Trigger.Isafter) {
            for(Offer_Lifecycle__c off:trigger.new) {
                setboxOfferId.add(off.id);           

            }  
        }
    }
    if(setboxOfferId.size() > 0) {
        AP_ELLAToBoxFolderCreation.OfferFolderCreationInBox(setboxOfferId); 
    }
    
    //March 2016 Q1 release
    if(Trigger.IsInsert && Trigger.Isafter) {
            
        AP_ELLAOffer.OfferListUserToShare(trigger.new);
 
    }
    }
    
    
    
    
}