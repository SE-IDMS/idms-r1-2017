/*
    Author          : Vimal Karunakaran(GD ) 
    Date Created    : 21 June 2012
    Description     : For September Release to:
                      1. Call AP_Case_CreateCaseActions.createPredefinedCCCActions
                      
    ------------------------------
    Modified By     : Stephen Norbury
    Modified Date   : 06/03/2013
    Description     : May 2013 Release:
    
    1. BR-2762: Case Ageing
    2. BR-2476: Care Agents
    -----------------------------------------------------------------
    Modified By     : Rakhi Kumar - Global Delivery Team
    Modified Date   : 11/06/2013
    Description     : Oct 2013 Release: BR-3111 - Disable automatic creation of CC Actions
    -----------------------------------------------------------------
    Modified By     : Vimal Karunakaran - Global Delivery Team
    Modified Date   : 14/02/2015
    Description     : April 2015 Release: BR-3114  
                    1.  Calling AP_Case_UpdateCaseSolvingTeam.UpdateCaseSolvingTeam
    --------------------------------------------------------------------------------
    
    Modified By     : Divya M  - Global Delivery Team
    Modified Date   : 17/06/2015
    Description     : OCT 2015 Release: BR-7761  
                    1.  Calling AP_Case_CaseHandler.updateLineOfBusinessOnTEX
    --------------------------------------------------------------------------------
    
         
    
*/

trigger CaseAfterUpdate on Case (after update){
  // Stephen Norbbury (On-Shore)
      
    if(Utils_SDF_Methodology.canTrigger('CaseAfterUpdate')){
        if(AP_Case_CaseHandler.blnUpdateTestRequired || AP_Case_CaseHandler.intTriggerIterationCount < AP_Case_CaseHandler.intAllowedIteration){
            
            AP_Case_CaseHandler.handleCaseAfterUpdate(trigger.old, trigger.new, trigger.oldMap, trigger.newMap);
            AP_UpdateMatchingCase.checkPMI_CMI(Trigger.OldMap,Trigger.newMap);
            
            Map<Id,Case> externalReferenceCases = new Map<Id,Case>();
            Map<Id,Case> casesForPointRegistration = new Map<Id,Case>();
            
            Set<Id> updatedCaseIds = new Set<Id>();
            
            Map<id,Case> caseIds = new Map<id,Case>();
            if(!Test.IsRunningTest() || AP_Case_UpdateCaseSolvingTeam.isTest)
                AP_Case_UpdateCaseSolvingTeam.UpdateCaseSolvingTeam(Trigger.oldMap,Trigger.newMap,false);
            
            Map<Id,case>mapMajorIssueCases=new Map<Id,case>();
            for(Case objCase  :Trigger.newMap.values()){
                if(Trigger.OldMap.get(objCase.id).Status!='Closed' && objCase.Status=='Closed' && objCase.CustomerMajorIssue__c=='Yes')
                    mapMajorIssueCases.put(objCase.id,objCase);
                
                /*checks the External Reference field on case Object*/
                if(objCase.ExternalReference__c!=null && Trigger.OldMap.get(objCase.id).ExternalReference__c != objCase.ExternalReference__c){
                    externalReferenceCases.put(objCase.id,objCase);
                }
                
                // checks if the Product Line on the Case is Updated ProductLine__c
                if((objCase.ProductLine__c!=null || objCase.ProductLine__c!= '')&& Trigger.OldMap.get(objCase.id).ProductLine__c != objCase.ProductLine__c){
                    caseIds.put(objCase.id,objCase);
                }
                
                //if((objCase.TECH_GMRCode__c != Trigger.oldMap.get(objCase.Id).TECH_GMRCode__c || objCase.CaseSymptom__c != Trigger.oldMap.get(objCase.Id).CaseSymptom__c || objCase.CaseSubsymptom__c != Trigger.oldMap.get(objCase.Id).CaseSubsymptom__c) && (objCase.Status == Label.CLOCT15I2P31 || objCase.Status == Label.CLOCT15I2P33 || objCase.Status == Label.CLOCT15I2P34)) {                
                if((objCase.CommercialReference_lk__c != Trigger.oldMap.get(objCase.Id).CommercialReference_lk__c || objCase.Family_lk__c != Trigger.oldMap.get(objCase.Id).Family_lk__c|| objCase.CaseSymptom__c != Trigger.oldMap.get(objCase.Id).CaseSymptom__c || objCase.CaseSubsymptom__c != Trigger.oldMap.get(objCase.Id).CaseSubsymptom__c) && (objCase.Status == Label.CLOCT15I2P31 || objCase.Status == Label.CLOCT15I2P33 || objCase.Status == Label.CLOCT15I2P34)) {                     
                     System.debug('#### Commercial Reference changed and Symptom present');
                     updatedCaseIds.add(objCase.Id);
                }                           
                // CLOCT15I2P31 = New            
                // CLOCT15I2P33 = In Progress            
                // CLOCT15I2P34 = Answer Provided to Customer         

                /*Create Point Registration for Cassini on Case Closure*/
                if(objCase.Status=='Closed' && ((objCase.TECH_DebitPoints__c==false) || (objCase.TECH_DebitPoints__c==true && Trigger.oldMap.get(objCase.Id).TECH_DebitPoints__c==false)) && objCase.Points__c >= 0 && objCase.RelatedContract__c != null && objCase.FreeOfCharge__c == false){
                    casesForPointRegistration.put(objCase.Id,objCase);    
                }
            }    
            
            //OCT 2015 Release : Added by Uttara - BR-7096
            System.debug('#### updatedCaseIds : ' + updatedCaseIds );
            if((!updatedCaseIds.isEmpty())) {
                AP_UpdateMatchingCase.CaseWithOpenProblems(updatedCaseIds );
            }

            if(casesForPointRegistration.size()>0){
                AP_ContractTriggerUtils.createPointRegistration(casesForPointRegistration);
            }    
         
            if(!mapMajorIssueCases.isempty())
                AP_UpdateMatchingCase.updateCaseStakeholders(mapMajorIssueCases);
            
            if(externalReferenceCases.size()>0){          
                System.debug('Batch Size'+externalReferenceCases.size());
                AP10_CaseTrigger.InsertExternalReference(externalReferenceCases);              
            }
            if(caseIds.size()>0){
                AP_UpdateMatchingCase.updateLineOfBusinessOnTEX(caseIds);
            }
        }    
    }
}