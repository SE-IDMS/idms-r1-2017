/* 
    Author:            Pooja Gupta
    Date of Creation:  07-01-2013
    Description:       Product Line After Delete event trigger


*/


trigger ProductLineAfterDelete on OPP_ProductLine__c (After Delete)
{
    System.debug('********* ProductLineAfterDelete Trigger Start *********');
    if(Utils_SDF_methodology.canTrigger('AP_OppBusinessMixUpdate'))
    {
        list<Opportunity> ListParentOpp = New List<Opportunity>();
        
        For(OPP_ProductLine__c OppPL: Trigger.Old)
        {
            Opportunity Oprec = New Opportunity(id=OppPL.Opportunity__c);
            ListParentOpp.add(Oprec);
        }
        
        AP_OppBusinessMixUpdate.UpdateOppBusinessMix(ListParentOpp);
    }
    
    System.Debug('********* ProductLineAfterDelete Trigger End *********');
}