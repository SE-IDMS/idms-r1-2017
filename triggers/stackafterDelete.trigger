trigger stackafterDelete on Answertostack__c (after delete) {

    if(Utils_SDF_Methodology.canTrigger('AP_ComplaintsRequestsStack1')) {
    
        AP_ComplaintsRequestsStack.updateComplaintsRequestStack(trigger.old);
    } 

}