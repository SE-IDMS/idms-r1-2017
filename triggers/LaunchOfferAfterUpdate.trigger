/******
Last modified :Hanamanth On q3 Release
**************/

trigger LaunchOfferAfterUpdate on Offer_Lifecycle__c (after update) {
   
   
   Set<String> setOfferId = new Set<String>();
    Set<String> setOfferCycleId = new Set<String>();
    Map<String, List<Milestone1_Project__c>> MileStoneMap= new  Map<String, List<Milestone1_Project__c>>();
    List<Milestone1_Project__c>  UpdateMProjectList1 = new List<Milestone1_Project__c>();
    Map<string ,Milestone1_Project__c>  UpdateMProjectList = new Map<string ,Milestone1_Project__c>();
    Map<string ,Milestone1_Project__c>  UpdateMProjectMap = new Map<string ,Milestone1_Project__c>();
    List<Offer_Lifecycle__c> SEndDatetoProcess = new List<Offer_Lifecycle__c>();
    //List<Offer_Lifecycle__c> OfferList = new List<Offer_Lifecycle__c>();
    map<id,string> mapidofrlnchClas = new map<id,string>();
    Set<id> offeridset = new Set<id>();
    string OldOfferClassification;
    Id offerlaunchid = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Launch - Product').getRecordTypeId();
    
    if(Utils_SDF_Methodology.canTrigger('AP_Offer1')) {
        if(Trigger.IsUpdate && Trigger.Isafter) {

            for(Offer_Lifecycle__c obj:trigger.new) {
                //April 2015 Release -- Divya M
                if(obj.Offer_Classification__c != trigger.oldMap.get(obj.id).Offer_Classification__c && obj.RecordTypeId==offerlaunchid){
                    mapidofrlnchClas.put(obj.id,trigger.oldMap.get(obj.id).Offer_Classification__c);
                }

            }
            if(mapidofrlnchClas.size()>0){
                AP_bFO_COLA_OfferCountryBUTaskCreation.phasesTasksDataReset(mapidofrlnchClas);
                AP_bFO_COLA_OfferCountryBUTaskCreation.UpdateProjects(mapidofrlnchClas);
            }
            
        }//
        //April 2015 release Hanamanth
        Set<string>  OfferSetid = new  Set<string> ();
        if(Trigger.IsUpdate && Trigger.Isafter) {
            AP_ELLAOffer.OfferReadWriteAccess(Trigger.new,trigger.oldMap); 
            AP_ELLAOffer.BoxwebserviceOnOwnerChange(Trigger.new,trigger.oldMap);
            AP_ELLAOffer.updateOfferProjectField(Trigger.newmap,trigger.oldMap);
            AP_ELLAOffer.UpdateOfferAnnouncement(Trigger.newmap,trigger.oldMap);
            
        } 
    
    }
     
}