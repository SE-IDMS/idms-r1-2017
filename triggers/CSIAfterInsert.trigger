trigger CSIAfterInsert on CustomerSafetyIssue__c (after insert) 
{if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_CustomerSafetyIssue_After'))
{
 AP_customerSafetyIssue.getStakeholder(trigger.new,'New',trigger.newmap,trigger.newmap,label.CLSEP12I2P34)  ;  
  //Start October 2015 Release, BR-BR-7406.
  set<string> listCSINew=new set<string>(); 
            for(CustomerSafetyIssue__c tCSI : Trigger.new){
            if(tCSI.RelatedbFOCase__c != null){
                listCSINew.add(tCSI.RelatedbFOCase__c);
            }}
    if(listCSINew.size()>0){
        AP_CustomerSafetyIssue.updateSeverityHighOnCaseAftrInset(listCSINew);
    }
 //End October 2015 Release, BR-BR-7406. 
 }
 if(Utils_SDF_Methodology.canTrigger('AP_TechnicalExpertAssessment'))
    {
     AP_TechnicalExpertAssessment.updateCSIFlagOnTexRunOnCSI(trigger.new) ;
    } 
}