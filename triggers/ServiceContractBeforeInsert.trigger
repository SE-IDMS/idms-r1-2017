trigger ServiceContractBeforeInsert on SVMXC__Service_Contract__c (before insert) {
    
     //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX19'))
    {
    list<SVMXC__Service_Contract__c> sclist =  New List<SVMXC__Service_Contract__c>();
    list<SVMXC__Service_Contract__c> splist1 = new list<SVMXC__Service_Contract__c>();
    list<SVMXC__Service_Contract__c>sllist= new list <SVMXC__Service_Contract__c>();
    list<SVMXC__Service_Contract__c>list0= new list <SVMXC__Service_Contract__c>();
    set<Id> scid=New set<Id>();
    set<Id> scid1=New set<Id>();
    Set<Id> SCSPset= new Set<Id>();
    Set<Id> set9= new Set<Id>();
    set<id>slslaset = new set<id>();
    set<id>spset1 = new set<id>();
    set<id>skuset1 = new set<id>();
    String VAL= System.Label.CLAPR15SRV29;
    String ServiceContractRT = System.Label.CLAPR15SRV01;
    String ServiceLineRT = System.Label.CLAPR15SRV02;
    String ServiceLineConnectedRT = System.Label.CLAPR15SRV03;
    String ServiceContractConnectedRT = System.Label.CLAPR15SRV04;
    String SEInterfaceSCConnector= System.Label.CLAPR15SRV37;
    Set<Id> RenewedFromId = new Set<Id>();
    set<Id> spid= new set<Id>();
    set<id>scdidset= new set<id>();
    set<id>scParentid=new set<id>();
    Map<string,SVMXC__Service_Contract__c> mapSspidSc = new Map<string,SVMXC__Service_Contract__c>(); //added by suhail for june release 2016
       
     system.debug('In before trg');
     
    list<SVMXC__Service_Contract__c> lstSC2=new list<SVMXC__Service_Contract__c>();
     list<SVMXC__Service_Contract__c> list9=new list<SVMXC__Service_Contract__c>();

     
    for(SVMXC__Service_Contract__c sc: trigger.new)
    {
        
     if(sc.RecordTypeid == ServiceLineRT  || sc.RecordTypeid == ServiceLineConnectedRT) 
       {
       
               slslaset.add(sc.id);
                sllist.add(sc);
       }
    
    
    if(sc.status__c== 'CAN'){ 
      
            sc.SVMXC__Canceled_By__c = userinfo.getUserId();
            }
        if(sc.SVMXC__Renewal_Number__c == null)
        {
            sc.SVMXC__Renewal_Number__c =0;
            
        }
        
        if((sc.RecordTypeid == ServiceLineRT || sc.RecordTypeId ==ServiceLineConnectedRT) && sc.SKUComRef__c!=null)
        {
            if(sc.SVMXC__Service_Plan__c!=null)
                spset1.add(sc.SVMXC__Service_Plan__c);
            if(sc.SKUComRef__c!=null)
                skuset1.add(sc.SKUComRef__c);
            splist1 .add(sc);
        }
        
        if(sc.SKUComRef__c!=null)
        {
            lstsc2.add(sc);
        }
           /*
        if(sc.SVMXC__Service_Plan__c!= null){
        set9.add(sc.SVMXC__Service_Plan__c);
        //list9.add(sc);
        }
        
        
        if(sc.SVMXC__Service_Plan__c!= null){
            SCSPset.add(sc.id);
        }        */ // commented for q4 1026 by suhail (the variables are not used anywhere)
        
        
        if(sc.Status__c==VAL && sc.Tech_StartDate__c==true 
           &&(sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId ==ServiceContractConnectedRT)){
            sclist.add(sc);
        }
        
        if(sc.ParentContract__c !=null && sc.RecordTypeid == ServiceLineRT)
        {
        
            scid1.add(sc.ParentContract__c);
            list0.add(sc);
        
        }
   
        if(sc.ParentContract__c !=null && (sc.RecordTypeid == ServiceLineRT ||sc.RecordTypeId ==ServiceLineConnectedRT)){
            scid.add(sc.ParentContract__c);
            
        }
        if(sc.SVMXC__Renewed_From__c !=null &&(sc.RecordTypeId == ServiceContractRT || sc.RecordTypeId ==ServiceContractConnectedRT)){
            RenewedFromId.add(sc.SVMXC__Renewed_From__c);
        }
        if(sc.ParentContract__c!=null && (sc.RecordTypeId == ServiceLineRT || sc.RecordTypeId ==ServiceLineConnectedRT) && sc.OwnerId!= null)
        {
            scParentid.add(sc.ParentContract__c);
        }
    }
     
    if(lstsc2!=null&&lstsc2.size()>0){
        AP_ServiceContract.LoadServicePlan(lstsc2,'INSERT'); 
    }
    if(sclist.size()>0){
        AP_ServiceContract.ContractUpdate(sclist); 
    }
    String pid = userinfo.getProfileId().subString(0,15);
    if(scid1!=null && scid1.size()>0 && list0.size()>0 && pid != SEInterfaceSCConnector){
    
    AP_ServiceContract.LineNumberGeneration(scid1,list0);
    
    }
    
    /*
    if(spset1!=null)
    {
        AP_ServiceContract.Slatermload(spset1,splist1);
    }    
    else */ // commented for q4 1026 by suhail (might not need this)

    if(skuset1!=null)
    {
        AP_ServiceContract.Slatermload(skuset1,splist1);  
    }
   if(slslaset!=null && slslaset.size()>0){
            AP_ServiceContract.Coveredproductslaterm(slslaset,sllist);
        }
       
    if(scid!=null){
        AP_ServiceContract.ServiceLineUpdate(scid,trigger.new);    
    }
   // if(RenewedFromId!=null)
   // {
        //AP_ServiceContract.RenewedNumber(RenewedFromId,trigger.new);    
    //}

      /**
       Added by Bala Candassamy
       Call to the apex class in charge of filling InitialContract field*/
      
      AP_ServiceContractInitialContract.InitialContract(trigger.new);  
    if(scParentid!=null && scParentid.size()>0)
        AP_ServiceContract.SLOwnerUpdate(trigger.new,scParentid);
    }
}