/*
@Author: Anita
Created Date: 2-08-2011
Description: This trigger fires on Event on After Insert
**********
Scenarios:
**********
1.  On Insert of Work Order Event, create a new Assigned Tools & Technician record.

Date Modified       Modified By         Comments
------------------------------------------------------------------------
02-08-2011          Anita D'Souza       Added Logic for Second scenario
06-03-2013          Chris Hurd          Added Logic to create multiple events based on WO recurring events fields
Apr-04-2016         Scott Fawcett       Updated prefix check for all WO related Events (instead of WO- prefixed ones)
*/

trigger EventAfterInsert on Event (before insert,before update,after insert) 
{
    String SOPrefix = SObjectType.SVMXC__Service_Order__c.getKeyPrefix(); 

    //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX01')){
        System.debug('************** Log >> '+Limits.getQueries());
        if(trigger.isafter || trigger.isupdate){
            List<AssignedToolsTechnicians__c> TechToolList = new List<AssignedToolsTechnicians__c>();
            List<Event> EventList = new List<Event>();
            List<Id> UserList = new List<ID>();
             map<id,id> techmap= new map<id,id>();
             set<id> evtid = new set<id>();
            for (Event sevent : Trigger.new)    
            {
                System.debug('Technician OwnerID ='+sevent);
                // Added on May 2013 Relase for NullPointerException
                //if(sevent.Subject != null && sevent.Subject.length()>0){
                if(sevent.WhatId != null){
                    String eWhatId = sevent.WhatId;
                    //if(sevent.Subject.contains('WO-') && (trigger.isafter || (Trigger.isupdate && sevent.StartDateTime!= trigger.oldmap.get(sevent.id).StartDateTime))) 
                    if(eWhatId.substring(0,3) == SOPrefix && (trigger.isafter || (Trigger.isupdate && sevent.StartDateTime!= trigger.oldmap.get(sevent.id).StartDateTime))) 
                    {   
                                 
                        System.debug('Technician OwnerID ='+sevent.OwnerID);
                        UserList.add(sevent.OwnerID);
                        EventList.add(sevent);
                        evtid.add(sevent.id);
                    }
                }
            } 
            if(trigger.isafter){
                // commented on may Release
                /*
                List<SVMXC__Service_Group_Members__c> techList = new List<SVMXC__Service_Group_Members__c>();
                if(EventList != null)       
                    techList = [Select ID,SVMXC__Salesforce_User__c 
                        from SVMXC__Service_Group_Members__c where SVMXC__Salesforce_User__c in:UserList ];
                for(SVMXC__Service_Group_Members__c t:techList){
                    techmap.put(t.SVMXC__Salesforce_User__c,t.id);
                }
                
                System.debug('Technician List='+TechList);
                for (Event sevent : EventList)
                {
                    AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c();
                    //for (SVMXC__Service_Group_Members__c tech : techList )
                   // {   
                        //if(sevent.OwnerID == tech.SVMXC__Salesforce_User__c)
                        if(techmap.containskey(sevent.OwnerID))
                        {
                            AssignedTech.TechnicianEquipment__c = techmap.get(sevent.OwnerID);                  
                            AssignedTech.WorkOrder__c = sevent.WhatId;
                            AssignedTech.EventId__c= sevent.Id;
                            AssignedTech.startDateTime__c= sevent.StartDateTime;
                            AssignedTech.EndDateTime__c = sevent.EndDateTime;
                            TechToolList.add(AssignedTech);
                        }   
                   // }            
                }
                */
                
                // May 2013 Release Started
                      // Hari started
                if(EventList != null && EventList.size()>0)
                {
                    Set<id> widset = new Set<id>();
                    for (Event sevent : EventList)
                    {
                        widset.add(sevent.WhatId);
                    }
                    String EventString = JSON.serialize(EventList);
                    AP_EventCreate.CreateAssignedToolsTechnicians(widset,EventString,'AfterInsert');
                }
                /*
                List<SVMXC__Service_Order__c> woList= new List<SVMXC__Service_Order__c>();
                woList=[select id,SVMXC__Group_Member__c from SVMXC__Service_Order__c where id in :widset];
                Map<id,SVMXC__Service_Order__c> idwoMap = new Map<id,SVMXC__Service_Order__c>();
                idwoMap.putAll(woList);
                
                Map<id,Boolean> widIsattMap = new Map<id,Boolean>();
                Map<id,Set<id>> widattsetMap = new Map<id,Set<id>>();
                
                List<AssignedToolsTechnicians__c> asToolTechList=[select id,PrimaryUser__c,TechnicianEquipment__c,WorkOrder__c from AssignedToolsTechnicians__c where WorkOrder__c in :wIdset and Status__c ='Accepted'];
                for(AssignedToolsTechnicians__c  att: asToolTechList){
                    if(!widIsattMap.containsKey(att.WorkOrder__c )){
                        widIsattMap.put(att.WorkOrder__c,true);
                        widattsetMap.put(att.WorkOrder__c,new Set<Id>());
                        widattsetMap.get(att.WorkOrder__c).add(att.id);
                        
                    }
                    else{
                        widattsetMap.get(att.WorkOrder__c).add(att.id);
                    }
                }
                
                List<SVMXC__Service_Group_Members__c> techList = new List<SVMXC__Service_Group_Members__c>();
                if(EventList != null)       
                    techList = [Select ID,SVMXC__Salesforce_User__c 
                        from SVMXC__Service_Group_Members__c where SVMXC__Salesforce_User__c in:UserList ];
                for(SVMXC__Service_Group_Members__c t:techList){
                    techmap.put(t.SVMXC__Salesforce_User__c,t.id);
                }
                
                for (Event sevent : EventList)
                {
                    
                   
                       if(widattsetMap.containskey(sevent.WhatId)){
                            Set<id> techidset = widattsetMap.get(sevent.WhatId);
                            if(!techidset.contains(techmap.get(sevent.OwnerID))){
                                    if(techmap.containskey(sevent.OwnerID))
                                    {
                                        AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c();
                                        AssignedTech.TechnicianEquipment__c = techmap.get(sevent.OwnerID);                  
                                        AssignedTech.WorkOrder__c = sevent.WhatId;
                                        AssignedTech.EventId__c= sevent.Id;
                                        AssignedTech.startDateTime__c= sevent.StartDateTime;
                                        AssignedTech.EndDateTime__c = sevent.EndDateTime;
                                        
                                        if(idwoMap.Containskey(sevent.WhatId))
                                        {
                                           if(idwoMap.get(sevent.WhatId).SVMXC__Group_Member__c == AssignedTech.TechnicianEquipment__c )
                                           AssignedTech.PrimaryUser__c = True;
                                        }
                                        
                                        TechToolList.add(AssignedTech);
                                    } 
                            }
                             
                        }
                        else{
                            // 
                            if(techmap.containskey(sevent.OwnerID))
                                    {
                                        AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c();
                                        AssignedTech.TechnicianEquipment__c = techmap.get(sevent.OwnerID);                  
                                        AssignedTech.WorkOrder__c = sevent.WhatId;
                                        AssignedTech.EventId__c= sevent.Id;
                                        AssignedTech.startDateTime__c= sevent.StartDateTime;
                                        AssignedTech.EndDateTime__c = sevent.EndDateTime;
                                        if(idwoMap.Containskey(sevent.WhatId))
                                        {
                                           if(idwoMap.get(sevent.WhatId).SVMXC__Group_Member__c == AssignedTech.TechnicianEquipment__c )
                                           AssignedTech.PrimaryUser__c = True;
                                        }
                                        TechToolList.add(AssignedTech);
                                    }
                        }
                        
                               
                }
                   
                */
                // Hari Ended
                    
                // May 2013 Release Ended
                
                
            }
            // Commented for the BR-4585  April 2014 Release:Uncommented for defect#DEf-7361
            
            if(trigger.isupdate){
                System.debug(' \n CLog :***** jyoti');
                List<AssignedToolsTechnicians__c> techList = new List<AssignedToolsTechnicians__c>();
                if(evtid.size()>0){
                    techList=[Select Id,EventId__c 
                        from AssignedToolsTechnicians__c where EventId__c in:evtid ];
                    for(AssignedToolsTechnicians__c t:techList){
                        techmap.put(t.EventId__c,t.id);
                    }
                }
                for (Event sevent : EventList)
                {
                    if(techmap.containskey(sevent.id))
                        {
                            AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c(id=techmap.get(sevent.id));  
                            AssignedTech.startDateTime__c= sevent.StartDateTime;
                            AssignedTech.EndDateTime__c = sevent.EndDateTime;
                            AssignedTech.DrivingTime__c = sevent.SVMXC__Driving_Time__c;
                            AssignedTech.DrivingTimeHome__c = sevent.SVMXC__Driving_Time_Home__c;
                            TechToolList.add(AssignedTech);
                        }          
                }
                
            }
            System.debug('TechToolList='+TechToolList);
            
            if(TechToolList != null)
                Database.upsert(TechToolList);
            
            if(trigger.isupdate){
                String EventString = JSON.serialize(EventList); 
                //AP_EventCreate.UpdateAssignedToolsTechnicians(EventString,evtid);           
            }
                
        }
        if(trigger.isbefore)
        {
            set<id> techid = new set<id>();
            for(integer i=0;i<trigger.new.size();i++){
                if(trigger.new[i].OwnerId != null){
                    techid.add(trigger.new[i].OwnerId);
                }
            }
            if(techid.size()>0)
            {
                List<SVMXC__Dispatcher_Access__c> DispAccs= new List<SVMXC__Dispatcher_Access__c>();
                List<SVMXC__Service_Group_Members__c> Techlist= new List<SVMXC__Service_Group_Members__c>();
                set<id> STid = new set<id>();
                Techlist=[select id,SVMXC__Service_Group__c from SVMXC__Service_Group_Members__c where SVMXC__Salesforce_User__c in:techid];
                for(SVMXC__Service_Group_Members__c st:Techlist)
                {
                    STid.add(st.SVMXC__Service_Group__c);
                }
                DispAccs=[select id from SVMXC__Dispatcher_Access__c where SVMXC__Dispatcher__c=:UserInfo.getUserId() and SVMXC__Service_Team__c in:STid and Read_Only__c=true];
                if(DispAccs.size()>0 && DispAccs!= null && trigger.new[0].WhatId.getSobjectType() != Schema.SVMXC_Time_Entry__c.SObjectType)
                {
                    trigger.new[0].addError('Sorry!! You only have read-only access.');
                }      
            }
        }
        
        //Issue 2891 Creating recurring events for work order dispatch
        if (trigger.isAfter && trigger.isInsert && Utils_SDF_Methodology.canTrigger('SVMX01_REC_Event'))
        {
            system.debug('here');
            Set<Id> woIdSet = new Set<Id>();
            Set<Id> userIdSet = new Set<Id>();
            Set<Id> techIdSet = new Set<Id>();
            Set<Id> bhIdSet = new Set<Id>();
            
            for (Event sEvent : trigger.new)
            {
                if (sEvent.WhatId != null)
                {
                    woIdSet.add(sEvent.WhatId);
                }
                
                if (sEvent.OwnerId != null)
                {
                    userIdSet.add(sEvent.OwnerId);
                }
            }
            
            if (woIdSet.size() > 0)
            {
                Map<Id, BusinessHours> bhMap = new Map<Id, BusinessHours>();
                Map<Id, SVMXC__Service_Group_Members__c> userMap = new Map<Id, SVMXC__Service_Group_Members__c>();
                Map<Id, SVMXC__Service_Order__c> woMap = new Map<Id, SVMXC__Service_Order__c>([SELECT Id, RecurrenceEndDate__c, Frequency__c, 
                    FrequencyUnit__c, SkipWeekends__c FROM SVMXC__Service_Order__C WHERE Id IN :woIdSet]);
                Map<Id, SVMXC__Service_Group_Members__c> techMap = new Map<Id, SVMXC__Service_Group_Members__c>([SELECT Id, SVMXC__Salesforce_User__c, SVMXC__Working_Hours__c FROM SVMXC__Service_Group_Members__c  WHERE SVMXC__Salesforce_User__c IN :userIdSet]);
                
                
                for (SVMXC__Service_Group_Members__c tech : techMap.values()){
                    userMap.put(tech.SVMXC__Salesforce_User__c, tech);
                    
                    if(tech.SVMXC__Working_Hours__c != null){
                        bhIdSet.add(tech.SVMXC__Working_Hours__c);
                    }
                }
                system.debug('YTI#####userMap: '+userMap);
                
                if (bhIdSet.size() > 0){
                    bhMap = new Map<Id, BusinessHours>([SELECT Id, MondayEndTime, MondayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime,
                    ThursdayEndTime, ThursdayStartTime, FridayStartTime, FridayEndTime, SaturdayEndTime, SaturdayStartTime, SundayStartTime, SundayEndTime FROM BusinessHours WHERE Id IN :bhIdSet]);
                }
                
                List<Event> newEvents = new List<Event>();
                System.debug('************** before '+Limits.getQueries());
                BusinessHours defaultBusinessHour = [SELECT Id, MondayEndTime, MondayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime,
                    ThursdayEndTime, ThursdayStartTime, FridayStartTime, FridayEndTime, SaturdayEndTime, SaturdayStartTime, SundayStartTime, SundayEndTime FROM BusinessHours WHERE isActive = true AND isDefault = true LIMIT 1];
                System.debug('************** after '+Limits.getQueries());
                for (Event sEvent : trigger.new)
                {
                    system.debug('YTI#####OwnerId: '+sEvent.OwnerId);
                    
                    if (woMap.containsKey(sEvent.WhatId) && woMap.get(sEvent.WhatId).RecurrenceEndDate__c != null)
                    {
                        SVMXC__Service_Order__c wo = woMap.get(sEvent.WhatId);
                        BusinessHours techBusinessHour = null;
                        
                        if(wo.SkipWeekends__c){
                            SVMXC__Service_Group_Members__c tech = userMap.get(sEvent.OwnerId);
                            
                            if(tech != null && bhMap.containsKey(tech.SVMXC__Working_Hours__c)){
                                techBusinessHour = bhMap.get(tech.SVMXC__Working_Hours__c);
                            }else{
                                techBusinessHour = defaultBusinessHour;
                            }
                        }
                        
                        system.debug('YTI#####: '+techBusinessHour);
                     // if Clause added for April Release to avoid null exception
                    if(wo.Frequency__c != null && wo.FrequencyUnit__c != null){
                        Date nextDate = ServiceMaxUtils1.getNextDate(sEvent.StartDateTime.date(), wo.Frequency__c, wo.FrequencyUnit__c, techBusinessHour);
                        while (nextDate <= wo.RecurrenceEndDate__c)
                        {
                            Event nEvent = sEvent.clone(false, false, false, false);
                            nEvent.StartDateTime = DateTime.newInstance(nextDate, sEvent.StartDateTime.time());
                            nEvent.EndDateTime = DateTime.newInstance(nextDate.addDays(sEvent.StartDateTime.date().daysBetween(sEvent.EndDateTime.date())), sEvent.EndDateTime.time());
                            nEvent.ActivityDateTime = nEvent.startDateTime;
                            newEvents.add(nEvent);
                            
                            nextDate = ServiceMaxUtils1.getNextDate(nextDate, wo.Frequency__c, wo.FrequencyUnit__c, techBusinessHour);
                        }
                     }
                        
                        wo.RecurrenceEndDate__c = null;
                        wo.Frequency__c = null;
                        wo.FrequencyUnit__c = null;
                        wo.SkipWeekends__c = false;
                    }
                }               
                
                if (newEvents.size() > 0)
                {
                    insert newEvents;
                    update woMap.values();
                }
            }
            
        }
    }
    
    if(trigger.isAfter && trigger.isInsert)
    {
        System.Debug('****** EventAfterInsert Trigger - First Activity Date for an Opportunity Begins ****'); 
        Map<Id,Event> newOpptyEvents= new Map<Id,Event>();
        Map<Id,Event> events= new Map<Id,Event>();
        String OpptyKeyPrefix = SObjectType.Opportunity.getKeyPrefix();
        String strCaseKeyPrefix = SObjectType.Case.getKeyPrefix();  //BR-5827 - Added By Vimal K
        Set<Id> setCaseIds = new Set<Id>(); //BR-5827 - Added By Vimal K

        Map<Id,Id> opptyEventIds = new Map<Id,Id>();

        for(Event eve: Trigger.new){
            //For April 2014 Release BR-4616, Create "First Activity" tech Field on Opportunity (date when first event or eve was created)
            if((eve.WhatId!=null) && (String.valueOf(eve.WhatId)).startsWith(OpptyKeyPrefix))
            {
                opptyEventIds.put(eve.Id,eve.WhatId);
                events.put(eve.Id,eve);
            }
            else if((eve.WhatId!=null) && (String.valueOf(eve.WhatId)).startsWith(strCaseKeyPrefix)){ //BR-5827 - Added By Vimal K
                setCaseIds.add(eve.whatId);
            }
                
                
        }
        if(opptyEventIds.size()>0){ //Added by Vimal K 
            Map<Id,Opportunity> opptyCreatedDate = new Map<Id,Opportunity>([SELECT CreatedDate FROM Opportunity WHERE Id IN :opptyEventIds.values()]); 
            for(Id eventId: opptyEventIds.keySet())
            {
                if( opptyCreatedDate.get(opptyEventIds.get(eventId)).CreatedDate.date() > Date.valueOf(Label.CLAPR14SLS62))       
                {
                    newOpptyEvents.put(eventId,events.get(eventId));
                }
            }              
            //System.Debug('----- AP_EventAfterInsertHandler <- NewOpptyEvents '+newOpptyEvents);
            if(!(newOpptyEvents.isEmpty()))
                AP_EventAfterInsertHandler.insertOpptyEvents(newOpptyEvents);
        }
        if(setCaseIds.size()>0){ //BR-5827 - Added By Vimal K
            if(Utils_SDF_Methodology.canTrigger('AP_Case_CaseHandler') || Test.isRunningTest()){
                AP_Case_CaseHandler.updateLastActivityDate(setCaseIds);
            }
        }
      System.Debug('****** EventAfterInsert Trigger - First Activity Date for an Opportunity Ends ****'); 
      
    } 
}