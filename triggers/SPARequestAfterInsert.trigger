/*
Author:Siddharth N
Purpose:
Quota to be calculated when the spa request is approved
*/
trigger SPARequestAfterInsert on SPARequest__c (after insert) {
    if(Utils_SDF_Methodology.canTrigger('AP_SPARequestCalculations'))
    {        
        Map<Id,Id> tobeProcessedSalesPerson=new Map<Id,Id>();
        List<Id> spaIds = new List<Id>();
        for(SPARequest__c spareq:Trigger.New)     
            if(spareq.SalesPerson__c!=null && spareq.salesperson__c!=spareq.ownerId)
                tobeProcessedSalesPerson.put(spareq.id,spareq.salesperson__c);    
                            
        if(tobeProcessedSalesPerson.size()>0)
            AP_SPARequestCalculations.addSalesPersonToShare(tobeProcessedSalesPerson); 

         for(SPARequest__c i:trigger.new)
            spaIds.add(i.id);
            
        //Account Contact should belong to the Account / Channel Contact Should belong to Contact
        for(SPARequest__c spa:[Select id,Account__c,ApprovalStatus__c,contact__r.AccountId,Channel__c,ChannelContact__c,ChannelContact__r.AccountId from SPARequest__c where id in:spaIds]){
           if(spa.contact__c!=null && spa.Account__c!=null){ 
                if(spa.Account__c!=spa.contact__r.AccountID){
                    SPARequest__c spaerr=Trigger.newMap.get(spa.id);
                    spaerr.addError(System.Label.CLOCT14SLS56);
               }
          }
          if(spa.ChannelContact__c!=null && spa.Channel__c!=null){
           if(spa.Channel__c!=spa.ChannelContact__r.AccountID){
                SPARequest__c spaerr=Trigger.newMap.get(spa.id);
                spaerr.addError(System.Label.CLOCT14SLS57);
           }  
         }      
      }
   }
}