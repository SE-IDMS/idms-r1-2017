//@Author:- Ashish Sharma SESA340747
//@Date:- 05/08/2014
//@Description:- This trigger is written on CIS_ISR__c custom object to assign CIS correspondent team based upon field of activity and EU Country.
//@Events :- Before Insert

trigger CIS_ISRBeforeInsert on CIS_ISR__c (before insert){
    
    if(Test.isRunningTest() || Utils_SDF_Methodology.canTrigger('AP_CIS_ISRHandler')){
    List<CIS_ISR__c> toBePopulatedISRs = new  List<CIS_ISR__c>();
    List<CIS_ISR__c> toChangeOwnerShip = new   List<CIS_ISR__c>();
    List<CIS_ISR__c> toChangeOwnerShipInitially = new List<CIS_ISR__c>();
    List<CIS_ISR__c> isrListToPopulateQueueAndItsMemberEmails = new List<CIS_ISR__c>();
    List<CIS_ISR__c> lstToPopulateEndUserCustomerandCountry = new list<CIS_ISR__c>();
    // Iterate through all the ISR in trigger context
    for(CIS_ISR__c  isrInstance:trigger.new){
    if(isrInstance.CIS_EndUserContact__c!=null)  {
        lstToPopulateEndUserCustomerandCountry.add(isrInstance);
        }
    }
    
    if(!lstToPopulateEndUserCustomerandCountry.isEmpty()){
    AP_CIS_ISRHandler.populateEndUserCustomerandCountry(lstToPopulateEndUserCustomerandCountry);
    }
    
    for(CIS_ISR__c  isrInstance:trigger.new){
        // Check if EU Country and Field of activity is not null
        if(isrInstance.CIS_EndUserCountry__c!=null && isrInstance.CIS_FieldOfActivity__c!=null){
            //Prepare a list of ISR's to assign EU CIS correspondent Team
            toBePopulatedISRs.add(isrInstance);
        }
        if(isrInstance.CIS_OEMCISCorrespondentsTeam__c!=null){
            toChangeOwnerShip.add(isrInstance);    
        }
        if((isrInstance.CIS_EUCISCorrespondentsTeam__c==null)&&(isrInstance.CIS_OEMCISCorrespondentsTeam__c==null)){
            toChangeOwnerShipInitially.add(isrInstance);
        }
        if(isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c){
            isrListToPopulateQueueAndItsMemberEmails.add(isrInstance);    
        }
        
        
    }
    
    if(!toBePopulatedISRs.isEmpty()){
        AP_CIS_ISRHandler.assignEUCISCorrespondentTeamToISR(toBePopulatedISRs,trigger.newMap);
    }
    
    if(!toChangeOwnerShip.isEmpty()){
        AP_CIS_ISRHandler.changeOwnership(toChangeOwnerShip,trigger.newMap);
    }
    
    if(!toChangeOwnerShipInitially.isEmpty()){
        AP_CIS_ISRHandler.changeOwnerShipInitially(toChangeOwnerShipInitially);
    }
    
    if(!isrListToPopulateQueueAndItsMemberEmails.isEmpty()){
        AP_CIS_ISRHandler.populateOEMADEUEmailFields(isrListToPopulateQueueAndItsMemberEmails);    
    } 
  } 
}