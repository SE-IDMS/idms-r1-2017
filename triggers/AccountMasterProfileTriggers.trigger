// Account Master Profile trigger to update weight synchronously and account asynchronously
Trigger AccountMasterProfileTriggers on AccountMasterProfile__c (before insert,after insert,before update,after update) {
    
    if (Trigger.isBefore){
       System.debug('**** Before Trigger of AMP called ***');
       if (Trigger.isInsert || Trigger.isUpdate)
              AP_AccountMasterProfileTriggerHandler.updateamp(Trigger.new);        
    }
    if(Trigger.IsAfter && Utils_SDF_Methodology.canTrigger('AP_AccountUpdateFromAMP')){
         System.debug('**** After Trigger of AMP called ***');
        if(Trigger.isInsert || Trigger.isUpdate){
            ID jobID = System.enqueueJob(new AP_AccountUpdateFromAMP(Trigger.new));
            System.debug('*After Insert job ID***' + jobID);
        }    
    }     
 }