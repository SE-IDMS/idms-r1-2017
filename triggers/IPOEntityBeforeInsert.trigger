/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 13-Dec-2012
    Description     : IPO Entity Progress Before Insert event trigger.
*/

trigger IPOEntityBeforeInsert on IPO_Strategic_Entity_Progress__c (before insert) {


  System.Debug('****** IPOEntityBeforeInsert Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('EntityBeforeInsert'))
    {
        IPOEntityTriggers.IPOEntityBeforeInsertUpdate_ValidateEntityRecord(Trigger.new);
                           
       IPOEntityTriggers.IPOEntityBeforeInsertUpdate_ValidateScopeEntity(Trigger.new);
        
         if(Trigger.new[0].ValidateEntityInsertUpdate__c == true)
                
          Trigger.new[0].Name.AddError(Label.EntityValidateERR);
        
           if (Trigger.new[0].ValidateScopeInsertUpdate__c == True)
        
                 Trigger.new[0].Name.AddError(Label.Connect_EntityProgress_InsertUpdate_ERR1);
 }
 System.Debug('****** IPOEntityBeforeInsert Trigger End ****'); 





}