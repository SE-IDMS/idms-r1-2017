trigger DeviceTypeTrigger on DeviceType__c (after delete, after insert, after update, before delete, before insert, before update) {
    
    
    Ap_DeviceType  DeviceType= new Ap_DeviceType(Trigger.isBefore, Trigger.isAfter, 
                           Trigger.isUpdate, Trigger.isInsert, Trigger.isDelete, 
                           
                           Trigger.new, Trigger.oldMap, Trigger.newMap);
    DeviceType.execute();

}