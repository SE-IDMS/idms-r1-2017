trigger FeedCommentAfterInsert  on FeedComment (After Insert) {

    if(Utils_SDF_Methodology.canTrigger('AP09')){
        AP_ProcessFeedComment PFC = new AP_ProcessFeedComment();
        PFC.updateFCommentsBREActualUserUdate(Trigger.new);
        
        // Added by Uttara - OCT 2015 Release - BR-7135
        AP_ProcessFeedComment PFC1 = new AP_ProcessFeedComment();
        PFC1.updateXA_LastUpdatedActualUserAndDate(Trigger.new);

    }
}