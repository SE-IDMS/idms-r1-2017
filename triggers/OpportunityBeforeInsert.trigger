/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 26/10/2010
    Description     : Opportunity Before Insert event trigger.
*/
trigger OpportunityBeforeInsert on Opportunity (before insert) {
    System.Debug('****** OpportunityBeforeInsert Trigger Start ****');  
    List<Opportunity> oppForecasts = new List<Opportunity>();
    
    if(Utils_SDF_Methodology.canTrigger('AP01'))
    {
        AP01_OpportunityFieldUpdate.populateTechOwnerId(Trigger.new);
        
        /* Modified for BR-5894 */
        for(Opportunity opp: trigger.new)
        {
            if(opp.IncludedInForecast__c == Label.CL00584)
                oppForecasts.add(opp);
        }
        
        if(!(oppForecasts.isEmpty()))
            AP01_OpportunityFieldUpdate.populateForecast(oppForecasts);
        
        /* End of modification for BR-5894 */
    }
    // This trigger default Opp Detector to Current User if the Opp Detector has not been captured
    if(Utils_SDF_Methodology.canTrigger('AP05')){
        if(!trigger.new.IsEmpty())
        {
            AP05_OpportunityTrigger.DefaultOppDetector(trigger.new);
            //START: MAY14 Release
             //Description: BR-5565, Oppt: sales people will only be able to select the programs a partner account has been assigned to
            AP05_OpportunityTrigger.validateAsssignedPrograms(trigger.new);
        }
    }    
        
    System.Debug('****** OpportunityBeforeInsert Trigger End ****'); 
}