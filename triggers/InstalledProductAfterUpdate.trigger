/*
Product Warranty Creation From IP(IP-->ApplicableProduct-->WarrantyTerm-->ProductWarranty)
   for April 2015
   By-Deepak 
*/
trigger InstalledProductAfterUpdate on SVMXC__Installed_Product__c (after Update) {
    
     //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX22'))
    {
        set<Id> ipid = new set<Id>();
        set<Id> ipid2 = new set<Id>();
        List<SVMXC__Installed_Product__c> IPlist = new List<SVMXC__Installed_Product__c>();
        List<SVMXC__Installed_Product__c> iplist2 = new List<SVMXC__Installed_Product__c>();
        set<id> ipset= new set<id>();
        
        map<string,SVMXC__Installed_Product__c> groupidmap= new map<string,SVMXC__Installed_Product__c>();
        for(SVMXC__Installed_Product__c ip : trigger.new)
        {
            //Added by Anand for BR - 8232
                if(ip.ProductGroupID__c!=null && ip.ProductGroupID__c!='' && (ip.LifeCycleStatusOfTheInstalledProduct__c== null||ip.LifeCycleStatusOfTheInstalledProduct__c==''|| ip.LifeCycleStatusOfTheInstalledProduct__c=='IN_USE') && (ip.SVMXC__Date_Installed__c!=null&& ip.SVMXC__Date_Installed__c !=trigger.OldMap.get(Ip.Id).SVMXC__Date_Installed__c))
                 {                 
                    ipset.add(ip.id);                    
                    groupidmap.put(ip.ProductGroupID__c,ip); 
                  }

            if(ip.SVMXC__Serial_Lot_Number__c!=trigger.OldMap.get(Ip.Id).SVMXC__Serial_Lot_Number__c ||
            ip.SchneiderCommercialReference__c!=trigger.OldMap.get(Ip.Id).SchneiderCommercialReference__c ||
            ip.SVMXC__Warranty_Start_Date__c!=trigger.OldMap.get(Ip.Id).SVMXC__Warranty_Start_Date__c ||
            ip.SVMXC__Warranty_End_Date__c !=trigger.OldMap.get(Ip.Id).SVMXC__Warranty_End_Date__c || 
            ip.CommissioningDateInstallDate__c !=trigger.OldMap.get(Ip.Id).CommissioningDateInstallDate__c || 
            ip.SVMXC__Date_Installed__c !=trigger.OldMap.get(Ip.Id).SVMXC__Date_Installed__c ||            //BR-9954 By Prem for Q3 2016 Release 25/Jul/2016
            ip.ProductGroupID__c !=trigger.OldMap.get(Ip.Id).ProductGroupID__c ||                   //BR-9954 By Prem for Q3 2016 Release 25/Jul/2016
            ip.SVMXC__Date_Shipped__c!=trigger.OldMap.get(Ip.Id).SVMXC__Date_Shipped__c)
            {
                ipid.add(ip.Id);
            }
            
            if(ip.Brand2__c !=null && ip.Category__c !=null && ip.DeviceType2__c !=null && ip.SVMXC__Product__c!=null &&
               (ip.Brand2__c!=trigger.OldMap.get(Ip.Id).Brand2__c
                ||ip.Category__c!=trigger.OldMap.get(Ip.Id).Category__c
                ||ip.DeviceType2__c!=trigger.OldMap.get(Ip.Id).DeviceType2__c 
                ||ip.SVMXC__Product__c!=trigger.OldMap.get(Ip.Id).SVMXC__Product__c))
                
               {
                     iplist2.add(ip);
                     
                    
               }

               if(ip.IsreplacementIP__c==false && ip.WarrantyTriggerDate__c !=null && ip.Brand2__c !=null && ip.Category__c !=null && ip.DeviceType2__c !=null && ip.SVMXC__Product__c!=null &&
               (ip.Brand2__c!=trigger.OldMap.get(Ip.Id).Brand2__c
                ||ip.Category__c!=trigger.OldMap.get(Ip.Id).Category__c
                ||ip.DeviceType2__c!=trigger.OldMap.get(Ip.Id).DeviceType2__c 
                ||ip.SVMXC__Product__c!=trigger.OldMap.get(Ip.Id).SVMXC__Product__c
                || ip.WarrantyTriggerDate__c !=trigger.OldMap.get(Ip.Id).WarrantyTriggerDate__c))
               {
                     IPlist.add(ip);
               }
            if(ip.LifeCycleStatusOfTheInstalledProduct__c =='SWAPPED' && ip.LifeCycleStatusOfTheInstalledProduct__c !=trigger.OldMap.get(Ip.Id).LifeCycleStatusOfTheInstalledProduct__c ){
                ipid2.add(ip.Id);
            }
        }
        /*
        if(set2!=null && set2.size()>0){
             Ap_Installed_Product.IBchangeMaster(set2);
        }*/
        
        if(!ipid2.isempty() || ipid2.size() > 0 ) // Added by VISHNU C for the Hotfix Q2 fixing too many SOQL query B2F Contract creation
        {
           Ap_Installed_Product.IPWorkDetailUpdate(ipid2) ;
        
        }
        
        if(IPlist.size()>0){
            if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLNOV13SRV03)){
              Ap_Installed_Product.ProductWarrantyCreation(IPlist);
            }
        }
        if(ipid.size()>0){
            Ap_Installed_Product.ScToUpdate(ipid);
            
        }
        if(iplist2.size()>0)
        {
            Ap_Installed_Product.ProductWarrantyDeletion(iplist2);
        }
        if(ipset.size()>0 && !groupidmap.isEmpty())
        {
            Ap_Installed_Product.acceptedinstalledproduct(ipset,groupidmap);
        }
   }
 
}//End of trigger