trigger SVMXC_AfterInsertUpdateDelete_TimeEntry on SVMXC_Time_Entry__c (after delete, after insert, after update) 
{
	if(Utils_SDF_Methodology.canTrigger('SVMX23'))
	{
	    if (trigger.isDelete)
	    {
	        ServiceMaxTimesheetUtils.updateTimesheetTotals(trigger.old);
	        ServiceMaxTimesheetUtils.deleteRelatedEvents(trigger.old);
	    }
	    else if (ServiceMaxTimesheetUtils.trg_insert_fired_after == false)
	    {
	        ServiceMaxTimesheetUtils.updatetimesheetTotals(trigger.new);
	        ServiceMaxTimesheetUtils.updateRelatedEvents(trigger.new);
	        //ServiceMaxTimesheetUtils.trg_insert_fired_after = true;
	    }
	}
}