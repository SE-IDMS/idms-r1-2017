trigger CIS_ISRCommentTrigger on CIS_ISRComment__c(before insert,before update) {
    
    Set<Id> isrCommentISrIds = new Set<Id>();
    for(CIS_ISRComment__c cisISRCommentInstance:trigger.new){
        isrCommentISrIds.add(cisISRCommentInstance.CIS_ISR__c);   
    }
    Map<Id,CIS_ISR__c> mapCISIsrIdAndRecord = new Map<Id,CIS_ISR__c>();
    for(CIS_ISR__c isrRecord:[select id,CIS_EUQueueEmail__c,CIS_OEMCISQueueEmail__c,CIS_OEMQueueMembersEmails__c,CIS_EUQueueMemberEmail__c,CIS_ISR_Name__c from CIS_ISR__c where id in:isrCommentISrIds]){
        mapCISIsrIdAndRecord.put(isrRecord.id,isrRecord);   
    }
    List<CIS_ISRComment__c> cisISRList = new List<CIS_ISRComment__c>();
    
    for(CIS_ISRComment__c cisISRCommentInstance:trigger.new){
        cisISRCommentInstance.CIS_EUQueueEmail__c = mapCISIsrIdAndRecord.get(cisISRCommentInstance.CIS_ISR__c).CIS_EUQueueEmail__c;
        cisISRCommentInstance.CIS_OEMQueueEmail__c = mapCISIsrIdAndRecord.get(cisISRCommentInstance.CIS_ISR__c).CIS_OEMCISQueueEmail__c; 
        
        if(trigger.isUpdate &&(trigger.newMap.get(cisISRCommentInstance.Id).CIS_Comment__c != trigger.oldMap.get(cisISRCommentInstance.Id).CIS_Comment__c)){
            cisISRList.add(cisISRCommentInstance);
        } 
        if(trigger.isInsert){
            cisISRList.add(cisISRCommentInstance);
        } 
        
    }
    if(!cisISRList.isEmpty() && !Test.isRunningTest()){
        AP_CIS_ISRHandler.sendISRCommentEmailNotification(cisISRList,mapCISIsrIdAndRecord );
    }
    
}