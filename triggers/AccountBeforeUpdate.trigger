/*
    Author          : Accenture Team (SFDC) 
    Date Created    : 08/08/2011
    Description     : Account Before update trigger.
    
    CH01:Srikant:19/10/2011:SEPRelease:Channel Partner cannot become the Owner of any Account
*/
trigger AccountBeforeUpdate on Account (before Update) {
    
    System.Debug('****** AccountBeforeUpdate Trigger Start ****');  

    if(Utils_SDF_Methodology.canTrigger('AP30') && VFC61_OpptyClone.isOpptyClone == false)
    {
     List<Account> accounts = new List<Account>();
     List<Account> populateLB = new List<Account>();
     List<Account> accCheckDel = new List<Account>();
     List<Account> updateCountryOrState = new List<Account>();
     Set<String> countryCodeLst = new Set<String>();
     Set<String> stateCodeLst = new Set<String>();
     
        for(Account acc:trigger.new)
        {
            //SA
            system.debug('---acc---'+acc);
            
            if(acc.Country__c!=trigger.oldMap.get(acc.id).Country__c)
            {
                acc.TECH_CountryPriorValue__c  = trigger.oldMap.get(acc.id).Country__c;
            }
            if(acc.ownerID!=trigger.oldMap.get(acc.id).ownerID)
            {
                accounts.add(acc);
                
            }
            if(acc.LeadingBusiness__c == null){
                populateLB.add(acc);
            }
            if(acc.ToBeDeleted__c==true && acc.ReasonForDeletion__c!=Label.CL00521)
                accCheckDel.add(acc);

            //Back2Standard Q2 2016 - Update custom state, country fields when standard address fields are updated / changed.

            If(( acc.BillingCountryCode!=acc.BillingCountry && acc.BillingCountryCode!=acc.TECH_SDH_CountryCode__c && ((trigger.oldMap.get(acc.id).BillingCountry)==(acc.BillingCountry))  ) || ( acc.BillingStateCode!=acc.BillingState && acc.BillingStateCode!=acc.TECH_SDH_StateProvCode__c && ((trigger.oldMap.get(acc.id).BillingState)==(acc.BillingState))  ))
            {
                System.debug('---->>> inside if stmt ---');
                updateCountryOrState.add(acc);
                countryCodeLst.add(acc.BillingCountryCode);
                stateCodeLst.add(acc.BillingStateCode);
            }
           
                        
        }
        //SA
        if(updateCountryOrState != null && updateCountryOrState.size() > 0)
            system.debug('---updateCountryOrState---'+updateCountryOrState);
        
        if(updateCountryOrState.size()>0)
        {
            System.Debug('****** AccountBeforeUpdate update custom address fields Starts ****');
            AP30_UpdateAccountOwner.updateCustomAddrFields(updateCountryOrState,countryCodeLst,stateCodeLst);
            System.Debug('****** AccountBeforeUpdate update custom address fields Ends****');
            //SA
            System.debug('---'+trigger.new);
        }       
        if(accounts.size()>0)    
        {
            AP30_UpdateAccountOwner.populateAccountOwner(accounts);
        }
        
        if(populateLB.size()>0)    
        {
            System.Debug('****** AccountBeforeUpdate populateAccountLB Starts ****');
            AP30_UpdateAccountOwner.populateAccountLB(populateLB);
            System.Debug('****** AccountBeforeUpdate populateAccountLB Ends****');
        }
        
        if(accCheckDel.size()>0)
        {
            System.Debug('****** AccountBeforeUpdate check To be deleted Starts ****');
            AP30_UpdateAccountOwner.checkTobeDeleted(accCheckDel);
            System.Debug('****** AccountBeforeUpdate check To be deleted Ends****');
        }
        
        
        //CH01 START: Check if the Owner is Partner 
        //Also alerts if an account with existing cases,opportunities or cases is being deleted.
        AP30_UpdateAccountOwner.CheckOwnerIsChannelPartner(Trigger.New,Trigger.newmap);
        //CH01 End
    }
    
        System.Debug('****** AccountBeforeUpdate Trigger Ends ****');  
         
}