trigger InvolvedOranizationBeforeInsert on InvolvedOrganization__c (Before Insert) 
{
system.debug('^^^^^^^Before insert trigger');
 if(Utils_SDF_Methodology.canTrigger('AP_IO_BI'))
         {
            AP_InvolvedOrganization.populateRepresentative(Trigger.new); 
            system.debug('^^^^^^^^^After before insert trigger');  
         }   
          
}