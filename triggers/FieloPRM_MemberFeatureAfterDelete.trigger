/********************************************************************
* Company: Fielo
* Created Date: 31/07/2015
* Description:
********************************************************************/
trigger FieloPRM_MemberFeatureAfterDelete on FieloPRM_MemberFeature__c (after delete) {
    
    if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberFeatureAfterDelete')){
        FieloPRM_AP_MemberFeatureTriggers.executeDemoteCustomLogic(trigger.old);
    }
}