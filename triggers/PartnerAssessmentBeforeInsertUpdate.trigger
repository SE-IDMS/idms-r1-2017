trigger PartnerAssessmentBeforeInsertUpdate on PartnerAssessment__c (before insert, before update) {
    try
    {
    
        set<id> AccSpeid = new set<id>();
        set<id> AccAssProgId = new set<id>();
        set<id> ORFids = new set<id>();  //for Hack
        
        for(PartnerAssessment__c  pa : trigger.new)
        {
            if(pa.AccountAssignedProgram__c != null)
                AccAssProgId.add(pa.AccountAssignedProgram__c );
                
            if(pa.AccountSpecialization__c != null)
                AccSpeid.add(pa.AccountSpecialization__c);
            
            if(pa.OpportunityRegistrationForm__c != null) // for Hack
                ORFids.add(pa.OpportunityRegistrationForm__c); // for Hack
            
        }
        
        map<id,AccountSpecialization__c> mpASep = new map<id, AccountSpecialization__c>([select Account__c  from AccountSpecialization__c where id in :AccSpeid ]);
         
        Map<id, ACC_PartnerProgram__c> mpASP = new map<id, ACC_PartnerProgram__c>([select id, Account__c from ACC_PartnerProgram__c where id in :AccAssProgId ]);
        
        //Hack
        map<id, OpportunityRegistrationForm__c> mpORF = new map<id, OpportunityRegistrationForm__c>([SELECT id, Tech_ORFOwner__c, Tech_ORFOwner__r.ContactID, Tech_ORFOwner__r.Contact.AccountID, PartnerProgram__c, ProgramLevel__c FROM OpportunityRegistrationForm__c WHERE id IN:ORFids ]);
        
        set<id> Accids= new set<id>();
        set<id> PRgIds = new set<id>();
        set<id> PrrgLvlIds = new set<id>();
        
        for(OpportunityRegistrationForm__c orf : mpORF.values())
        {
            Accids.add(orf.Tech_ORFOwner__r.Contact.AccountID);
            PRgIds .add(orf.PartnerProgram__c);
            PrrgLvlIds.add(orf.ProgramLevel__c);
            
        }
        List<ACC_PartnerProgram__c> latAAP = [SELECT ID, Account__c, PartnerProgram__c, ProgramLevel__c from ACC_PartnerProgram__c WHERE Account__c in: Accids AND PartnerProgram__c in :PRgIds  AND  ProgramLevel__c  in :PrrgLvlIds ];
        map<String, ACC_PartnerProgram__c> mpAccPrg = new map<String, ACC_PartnerProgram__c>();
        for(ACC_PartnerProgram__c acp : latAAP)
        {
            mpAccPrg.put('-'+acp.Account__c + acp.PartnerProgram__c+ acp.ProgramLevel__c , acp);
        }
        //end Hack
        
        for(PartnerAssessment__c  pa : trigger.new)
        {
            if(pa.AccountSpecialization__c != null && mpASep.get(pa.AccountSpecialization__c) != null)
                pa.Account__c =  mpASep.get(pa.AccountSpecialization__c).Account__c;
                
             if(pa.AccountAssignedProgram__c != null && mpASP.get(pa.AccountAssignedProgram__c ) != null)
                pa.Account__c =  mpASP.get(pa.AccountAssignedProgram__c ).Account__c;
                
             //Hack
             if(pa.AccountAssignedProgram__c == null && pa.OpportunityRegistrationForm__c != null && mpORF.get(pa.OpportunityRegistrationForm__c) != null && mpORF.get(pa.OpportunityRegistrationForm__c).Tech_ORFOwner__c!= null
                        &&  mpORF.get(pa.OpportunityRegistrationForm__c).Tech_ORFOwner__r.ContactID != null)
             {
               OpportunityRegistrationForm__c  tmporf = mpORF.get(pa.OpportunityRegistrationForm__c);
               
               pa.Account__c =  mpORF.get(pa.OpportunityRegistrationForm__c).Tech_ORFOwner__r.Contact.AccountId;   
               
               String key = '-'+tmporf.Tech_ORFOwner__r.Contact.AccountId + tmporf.PartnerProgram__c + tmporf.ProgramLevel__c ;
               
               if( mpAccPrg.get(key ) != null)
                   pa.AccountAssignedProgram__c  = mpAccPrg.get(key).id;
             }//End Hack  
        }
    
    }
    catch(Exception e)
    {
        system.debug(e.getMessage());
    }
}