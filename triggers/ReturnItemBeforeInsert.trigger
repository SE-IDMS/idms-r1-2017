/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | Before Insert Trigger                                                              |
|                       |                                                                                    |
|     - Object(s)       | RMA_Product__c                                                                     |
|     - Description     |   - Used to set the Shipping Address on a new Return Item                          |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | April, 27th 2012                                                                   |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/

trigger ReturnItemBeforeInsert on RMA_Product__c (Before Insert)
{
if(Utils_SDF_Methodology.canTrigger('AP_RI_FieldUpdate'))
    {
AP_RR_FieldUpdate.updateRequestTypeOnRi(trigger.new);
}
    System.Debug('****** ReturnItemBeforeInsert Trigger starts ****');  
/*
    if(Utils_SDF_Methodology.canTrigger('AP35'))
    {
       AP35_addItemToRMA.updateShippingAddress(trigger.new);
    }

    System.Debug('****** ReturnItemBeforeInsert Trigger ends ****');  
    */
    // October 2015 Release - Gayathri Shivakumar
    if(Utils_SDF_Methodology.canTrigger('AP35'))
    {
       AP35_addItemToRMA.updateReturnAddress(trigger.new);
    }
}