/*
    01-Apr-2013 Srinivas Nallapati  PRM Apr13   DEF-1462
*/
trigger OPPValueChainPlayersBeforeInsert on OPP_ValueChainPlayers__c (before insert) {

	if(Utils_SDF_Methodology.canTrigger('AP_OPPValueChainPlayers'))
    {
		AP_OPPValueChainPlayers.checkOpportunityPassedToPartner(trigger.new);
	}	
	
}