/*
    Author          : Priyanka Shetty (Schneider Electric)
    Date Created    : 12/04/2013
    Description     : Change RequestCosts Forecasts Beforeupdate event trigger.
*/
trigger CHR_CostForecastBeforeUpdate on ChangeRequestCostsForecast__c (before update) {

    if(Utils_SDF_Methodology.canTrigger('ChangeRequestCostForecastTriggerBeforeUpdate'))
    {
       CHR_CostForecastTriggers.CRCostForecastbeforeupdate(Trigger.newmap,Trigger.oldmap);
    }

}