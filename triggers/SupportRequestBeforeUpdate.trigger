trigger SupportRequestBeforeUpdate on OPP_SupportRequest__c (Before Update) 
{
    if(Utils_SDF_Methodology.canTrigger('AP22'))
    {
     System.Debug('****** SupportRequestBeforeUpdate Trigger Start****');
     Map<ID,OPP_SupportRequest__c> supportRequest = new Map<Id,OPP_SupportRequest__c>();
     Map<ID,OPP_SupportRequest__c> pmSupportRequest = new Map<Id,OPP_SupportRequest__c>();
     Map<ID,OPP_SupportRequest__c> adeSupportRequest = new Map<Id,OPP_SupportRequest__c>();
     Map<ID,OPP_SupportRequest__c> saSupportRequest = new Map<Id,OPP_SupportRequest__c>();     
     Map<ID,OPP_SupportRequest__c> suppUser1 = new Map<Id,OPP_SupportRequest__c>();
     Map<ID,OPP_SupportRequest__c> suppUser2 = new Map<Id,OPP_SupportRequest__c>();   
     List<OPP_SupportRequest__c> updateAgreementIds = new List<OPP_SupportRequest__c>();  
     list<OPP_SupportRequest__c> updateSupportEmail =  new list<OPP_SupportRequest__c>();
     Map<String,Set<String>> mapQLsharedToUsers = new Map<String,Set<String>>();
     Map<Id,OPP_SupportRequest__c > mapQLIdSupportRequest= new Map<Id,OPP_SupportRequest__c>();
     Set<String> sharedToUsers;
          
        for(OPP_SupportRequest__c supportReq:trigger.new)
        {
           //AddedforBR-7964 - Additional information on the support request - March 2016 release
           if((supportReq.QuoteLink__c!=NULL && supportReq.QuoteLink__c!=trigger.oldMap.get(supportReq.id).QuoteLink__c && supportReq.TypeOfOffer__c!=NULL) || (supportReq.TypeOfOffer__c!=NULL && supportReq.QuoteLink__c!=NULL && supportReq.TypeOfOffer__c!=trigger.oldMap.get(supportReq.id).TypeOfOffer__c))
            {
                if(!(mapQLIdSupportRequest.ContainsKey(supportReq.QuoteLink__c)))
                    mapQLIdSupportRequest.put(supportReq.QuoteLink__c,supportReq);
            }
            if(supportReq.AssignedToUser__c!=trigger.oldMap.get(supportReq.id).AssignedToUser__c && supportReq.AssignedToUser__c!=NULL)
            {
                supportRequest.put(supportReq.id,supportReq);
            }
            //Populates Agreement field with TECH_AgreementId__c - Modified for BR-6915
            if(supportReq.TECH_AgreementId__c!=null)
                updateAgreementIds.add(supportReq); 
                
            if(supportReq.PMTender__c!=trigger.oldMap.get(supportReq.id).PMTender__c && supportReq.PMTender__c!=NULL)
            {
                pmSupportRequest.put(supportReq.id,supportReq);
            }
            /* Modified for BR-4172 */
            if(supportReq.AppDevEngineer__c!=trigger.oldMap.get(supportReq.id).AppDevEngineer__c&& supportReq.AppDevEngineer__c!=NULL)
            {
                adeSupportRequest.put(supportReq.id,supportReq);
            }
            if(supportReq.SolutionArchitect__c!=trigger.oldMap.get(supportReq.id).SolutionArchitect__c&& supportReq.SolutionArchitect__c!=NULL)
            {
                saSupportRequest.put(supportReq.id,supportReq);
            }            
            if(supportReq.SupportUser1__c!=trigger.oldMap.get(supportReq.id).SupportUser1__c && supportReq.SupportUser1__c!=NULL)
            {
                suppUser1.put(supportReq.id,supportReq);
            }
            if(supportReq.SupportUser2__c!=trigger.oldMap.get(supportReq.id).SupportUser2__c && supportReq.SupportUser2__c!=NULL)
            {
                suppUser2.put(supportReq.id,supportReq);
            }
            /* End of Modification for BR-4172 */
            if((supportReq.sellingCenter__c!=trigger.oldMap.get(supportReq.id).sellingCenter__c)||
            (supportReq.solutionCenter__c!=trigger.oldMap.get(supportReq.id).solutionCenter__c)) 
            {
                updateSupportEmail.add(supportReq);
            }    
            
            //BR-8607 (Mar 2016) Added by Uttara - Grant access to Quote Link
            sharedToUsers = new Set<String>();
            if(supportReq.QuoteLink__c != null && supportReq.QuoteLink__c != Trigger.oldMap.get(supportReq.Id).QuoteLink__c) {
                if(supportReq.PMTender__c != null) {
                    if(mapQLsharedToUsers.containsKey(supportReq.QuoteLink__c))
                        mapQLsharedToUsers.get(supportReq.QuoteLink__c).add(supportReq.PMTender__c);
                    else {         
                        sharedToUsers.add(supportReq.PMTender__c);  
                        mapQLsharedToUsers.put(supportReq.QuoteLink__c,sharedToUsers);
                    }    
                }    
                if(supportReq.AssignedToUser__c != null) {
                    if(mapQLsharedToUsers.containsKey(supportReq.QuoteLink__c))
                        mapQLsharedToUsers.get(supportReq.QuoteLink__c).add(supportReq.AssignedToUser__c);
                    else {         
                        sharedToUsers.add(supportReq.AssignedToUser__c);  
                        mapQLsharedToUsers.put(supportReq.QuoteLink__c,sharedToUsers);
                    } 
                }      
            } 
            if(supportReq.QuoteLink__c != null && supportReq.PMTender__c != null && supportReq.PMTender__c != Trigger.oldMap.get(supportReq.Id).PMTender__c) {
                if(mapQLsharedToUsers.containsKey(supportReq.QuoteLink__c))
                        mapQLsharedToUsers.get(supportReq.QuoteLink__c).add(supportReq.PMTender__c);
                else {         
                    sharedToUsers.add(supportReq.PMTender__c);  
                    mapQLsharedToUsers.put(supportReq.QuoteLink__c,sharedToUsers);
                }
            }    
            if(supportReq.QuoteLink__c != null && supportReq.AssignedToUser__c != null && supportReq.AssignedToUser__c != Trigger.oldMap.get(supportReq.Id).AssignedToUser__c) {
                if(mapQLsharedToUsers.containsKey(supportReq.QuoteLink__c))
                        mapQLsharedToUsers.get(supportReq.QuoteLink__c).add(supportReq.AssignedToUser__c);
                else {         
                    sharedToUsers.add(supportReq.AssignedToUser__c);  
                    mapQLsharedToUsers.put(supportReq.QuoteLink__c,sharedToUsers);
                }
            }           
        }
        //Modified for BR-6915
        if(updateAgreementIds.size()>0)
            AP22_SupportRequest.updateAgreement(updateAgreementIds);
        if(supportRequest.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(supportRequest, 'assignToTeam');
        }
        if(pmSupportRequest.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(pmSupportRequest, 'assignPMToTeam');
        }
        /* Modified for BR-4172 */
        if(adeSupportRequest.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(adeSupportRequest, 'assignADEToTeam');
        }
        if(saSupportRequest.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(saSupportRequest, 'assignSAToTeam');
        }        
        if(suppUser1.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(suppUser1, 'assignSu1ToTeam');
        }  
        if(suppUser2.size()>0)
        {
            AP22_SupportRequest.assignToOpportintyTeam(suppUser2, 'assignSu2ToTeam');
        }
        /* End of Modification for BR-4172 */
        
        if(updateSupportEmail.size()>0)
        {
            AP22_SupportRequest.updateEmail(updateSupportEmail);
        }
        
        if(!mapQLsharedToUsers.isEmpty()) 
            AP16_GrantAcessToQteLink.SupportRequestSharing(mapQLsharedToUsers);
            
        if(!mapQLIdSupportRequest.isEmpty()) 
            AP22_SupportRequest.qlTypeOfOfferPopulation(mapQLIdSupportRequest);
            
     System.Debug('****** SupportRequestBeforeUpdate Trigger ends****');   
    }
}