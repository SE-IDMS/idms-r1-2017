/*
    Author          : Jean-Philippe Levasseur
    Date Created    : 21-oct-2012
    Description     : EUS_EndUserSupport After Insert Trigger
                      Goal: create Escalation History in EndUserSupportEscalationHistory__c
    Modified on     : 
    Modified by     : 
*/

trigger EUS_EndUserSupportAfterInsert on EUS_EndUserSupport__c (After Insert)
{
   List<EUS_EndUserSupport__c> lstNewUpdateEUS = new List<EUS_EndUserSupport__c>();
   for(EUS_EndUserSupport__c anItem : trigger.new)
   {
      if(anItem.RecordTypeId == System.Label.CLNOV13EUS01)   //EUS Record Type
      {
         lstNewUpdateEUS.add(anItem);
      }
   }
   if(!lstNewUpdateEUS.isEmpty())
   {
      AP_EUS.UpdateEscalationHistory(lstNewUpdateEUS);
   }
}