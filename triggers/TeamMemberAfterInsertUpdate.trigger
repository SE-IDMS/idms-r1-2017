/*
    Author          : Stephen Norbury 
    Date Created    : 06/03/2013
    Description     : Care Team Member After Insert / Update.
    
    Change History:
    
    BR-2762: Register Team Member as Care Agent
*/

trigger TeamMemberAfterInsertUpdate on TeamAgentMapping__c (after insert, after update)
{
  // Fetch the Team Members
  
  Set <Id> UserIds = new Set <Id>();
  Set <Id> TeamIds = new Set <Id>();
  
  Map <Id, CustomerCareTeam__c> TeamMap = new Map <Id, CustomerCareTeam__c>();
  
  for (TeamAgentMapping__c member : trigger.new)
  {
    if (member.CCTeam__c != null && member.CCAgent__c != null)
    {
      TeamIds.add (member.CCTeam__c);
      UserIds.add (member.CCAgent__c);
    }
  }
  
  // Now, Fetch the Teams
  
  List <CustomerCareTeam__c> teams = [select Id, CCCountry__c, LevelOfSupport__c from CustomerCareTeam__c where Id in :TeamIds];
    
  for (CustomerCareTeam__c team : teams)
  {
    TeamMap.put (team.Id, team);   
  }
  
  // And, Fetch the Care Agents
  
  List <CareAgent__c> agents = [select Id, User__c, Country__c, DefaultCareTeam__c, LevelofExpertise__c from CareAgent__c where User__c in :UserIds];
  
  Map <Id, CareAgent__c> AgentMap = new Map <Id, CareAgent__c>();
  
  for (CareAgent__c agent : agents)
  {
    AgentMap.put (agent.User__c, agent);    
  }
  
  // Now, either create or update the Care Agents
  
  List <CareAgent__c> updAgents = new List <CareAgent__c>();
  List <CareAgent__c> newAgents = new List <CareAgent__c>();
  
  for (TeamAgentMapping__c member : trigger.new)
  {
    if (member.CCTeam__c != null && member.CCAgent__c != null)
    {
      // Care Agent exists?
      
      CustomerCareTeam__c team = TeamMap.get (member.CCTeam__c);
      
      if (AgentMap != null && AgentMap.size() != 0 && AgentMap.get (member.CCAgent__c) != null)
      {
        // Yes
        
        CareAgent__c agent = AgentMap.get (member.CCAgent__c);
        
        if (member.DefaultTeam__c != null && member.DefaultTeam__c == true)
        {
          if (agent.DefaultCareTeam__c == null || agent.DefaultCareTeam__c != member.CCTeam__c)
          {
            agent.DefaultCareTeam__c = (member.DefaultTeam__c != null && member.DefaultTeam__c == true ? team.Id : null);
            
            updAgents.add (agent);
          }
        }
      }
      else
      {
        // No, Add new Care Agent
        
        /*
        CareAgent__c agent = new CareAgent__c();
          
        agent.Country__c            = team.CCCountry__c;
        agent.LevelOfExpertise__c   = team.LevelOfSupport__c;
        agent.User__c               = member.CCAgent__c;
        agent.DefaultCareTeam__c    = (member.DefaultTeam__c != null && member.DefaultTeam__c == true ? team.Id : null);
          
        newAgents.add (agent);
        */
      }
    }
  }
  
  // Finally, create / update the Care Agents
  
  if (newAgents != null && newAgents.size() != 0)
  {
    insert newAgents;
  }
  
  if (updAgents != null && updAgents.size() != 0)
  {
    update updAgents;
  }
  
  // End
}