trigger ProgramRequirementAfterInsert on ProgramRequirement__c (after insert) {
    
    if(Utils_SDF_Methodology.canTrigger('AP_PGR_CountryRequirementUpdate'))
    {   
        List<ProgramRequirement__c> lstActiveProgReqs = new List<ProgramRequirement__c>();
        
        list<ID> lstreqProgramID = new list<ID>();
        map<ID,list<ProgramRequirement__c>> mapLevelRequirement = new map<ID,list<ProgramRequirement__c>>();
        for(ProgramRequirement__c PRGRequirement :  trigger.new)
        {
           if(PRGRequirement.Active__c)
           {
                lstreqProgramID.add(PRGRequirement.PartnerProgram__c);//Global Partner Program
                if(!mapLevelRequirement.containsKey(PRGRequirement.ProgramLevel__c))
                    mapLevelRequirement.put(PRGRequirement.ProgramLevel__c, new list<ProgramRequirement__c> {(PRGRequirement)});
                else
                    mapLevelRequirement.get(PRGRequirement.ProgramLevel__c).add(PRGRequirement);

                lstActiveProgReqs.add(PRGRequirement);
            }
        }
        if(!mapLevelRequirement.isEmpty())
            AP_PGR_CountryRequirementUpdate.creatCountryRequirement(mapLevelRequirement , lstreqProgramID , null);

        if(!lstActiveProgReqs.isEmpty())
            AP_PGR_CountryRequirementUpdate.newAcccountAssigendRequirements(lstActiveProgReqs);
    }
}