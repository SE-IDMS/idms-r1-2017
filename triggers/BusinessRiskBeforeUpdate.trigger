//*********************************************************************************
//Trigger Name :  BusinessRiskBeforeUpdate
// Purpose : To populate Executive Sponsor in BRE to notify when BRE status changes
// Created by : Ramesh Rajasekaran - Global Delivery Team
// Date created : 13th July 2011
// Modified by :
// Date Modified :
// Remarks : For July - 11 release 
///********************************************************************************/ 

trigger BusinessRiskBeforeUpdate on BusinessRiskEscalations__c (before update) {
    System.Debug('****** BusinessRiskBeforeUpdate Trigger Start ****');
    List<BusinessRiskEscalations__c> breList=new List<BusinessRiskEscalations__c>();
    List<BusinessRiskEscalations__c> breListForNewLeader=new List<BusinessRiskEscalations__c>();
    List<BusinessRiskEscalations__c> breListForNewStakeHolder = new List<BusinessRiskEscalations__c>();
    
    if(Utils_SDF_Methodology.canTrigger('AP09')){                  
        for(BusinessRiskEscalations__c bRisk : Trigger.new){
            if(bRisk.Status__c != trigger.oldMap.get(bRisk.Id).Status__c && bRisk.GSAAffected__c == 'Yes'){
                breList.add(bRisk);
            }
            
            if(bRisk.ResolutionOrganisation__c != trigger.oldMap.get(bRisk.Id).ResolutionOrganisation__c){
                breListForNewStakeHolder.add(bRisk);
            }
            
            if(bRisk.ResolutionLeader__c == null || bRisk.BusinessRiskSponsor__c ==null){
                breListForNewLeader.add(bRisk);
            }
            //======= shivdeep  - April2015RELEASE - START =======================================================
            system.debug('actual user going to get udate ');
            bRisk.Actual_User_LastmodifiedDate__c = System.now();
            bRisk.Actual_User_Lastmodifiedby__c = UserInfo.getUserId();
             //======= shivdeep  - April2015RELEASE - END ======================================================= 
            //OCT 15 Release -- BR-7749 -- Divya M 
            if(bRisk.OriginatingOrganisation__c != null && bRisk.OriginatingOrganisation__c != trigger.oldmap.get(bRisk.id).OriginatingOrganisation__c){
                AP09_BusinessRiskEscalation.checkBREOrginatingOrgs(Trigger.new);    
            }               
            //Ends OCT 15 rel 
            //======= shivdeep  - April2015RELEASE - END =======================================================            
        }                       
        if(breList!=null && breList.size()>0){  
            AP09_BusinessRiskEscalation.populateExecutiveSponsor(breList); 
        }
        if(breListForNewLeader!=null && breListForNewLeader.size()>0){  
            AP09_BusinessRiskEscalation.populateSponsorAndResolutionLeader(breListForNewLeader);
        }
        
        if(breListForNewStakeHolder!=null && breListForNewStakeHolder.size()>0){  
            AP09_BusinessRiskEscalation.createBusinessRiskStakeholder(breListForNewStakeHolder);
        }
        
        AP09_BusinessRiskEscalation.updateBRELinkOnRelatedProblems(Trigger.oldMap,trigger.newMap);   

    }                                                        
               
    System.Debug('****** BusinessRiskBeforeUpdate Trigger End ****');
}