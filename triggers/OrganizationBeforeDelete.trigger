trigger OrganizationBeforeDelete  on BusinessRiskEscalationEntity__c (Before Delete) {


if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_OrgBfDelete'))
{
        AP_Organization.checkSubentyLocationBeforeDelete(trigger.old) ;  
        }

}