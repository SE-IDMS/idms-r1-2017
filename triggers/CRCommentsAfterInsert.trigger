/*
    Author        : Swapnil Saurav 
    Date Created  : 12/04/2016
    Description   : C/R Comments After Insert trigger.
    
*/

trigger CRCommentsAfterInsert on CR_Comments__c (After Insert) {
    
    if(Utils_SDF_Methodology.canTrigger('AP_CRComments')){ 
        //April 16 Release BR-9631 Swapnil Saurav
        //To update Comments field on Complaint/Request record each time a C/R Comments record is created
        AP_CRComments.updateCommentsOnCR(trigger.new) ;
    }
}