/*
    Author          : Pooja Suresh
    Description     : Contact Before Update event trigger.
                      
*/
trigger ContactBeforeUpdate on Contact(before update)
{
    System.Debug('****** ContactBeforeUpdate Trigger Start ****');  
    if(Utils_SDF_Methodology.cantrigger('AP_ContactBeforeUpdateHandler'))
    {
        List<Contact> conCheckDel = new List<Contact>();
        List<Contact> updateContactCountryOrState = new List<Contact>();
        List<Contact> mktContact = new List<Contact>();
        Set<String> countryCodeLst = new Set<String>();
        Set<String> stateCodeLst = new Set<String>();
        List<CS_Country__c> countryList = CS_Country__c.getall().values();
        Map<String,CS_Country__c> countryMap = new Map<String,CS_Country__c> ();
        for(CS_Country__c country: countryList) {
            countryMap.put(country.CountryCode__c,country);
        }
        
        for(Contact con: trigger.new)
        {
            if(con.ToBeDeleted__c==True)
                conCheckDel.add(con);
            
            //Back2Standard Q2 2016 - Update custom state, country fields when standard address fields are updated / changed.
            if(con.OtherCountryCode!=trigger.oldMap.get(con.id).OtherCountryCode || con.OtherStateCode!=trigger.oldMap.get(con.id).OtherStateCode)
            {
                updateContactCountryOrState.add(con);
                countryCodeLst.add(con.OtherCountryCode);
                stateCodeLst.add(con.OtherStateCode);
            }
        }
        
        AP_ContactBeforeUpdateHandler handler = new AP_ContactBeforeUpdateHandler();
        if(conCheckDel.size()>0)
        {
            System.Debug('****** ContactBeforeUpdate check To be deleted Starts ****');
            handler.OnBeforeUpdate(conCheckDel);
            System.Debug('****** ContactBeforeUpdate check To be deleted Ends ****');
        }
        
        if(updateContactCountryOrState.size() > 0)
        {
            System.Debug('****** ContactBeforeUpdate update Contact state, country custom field Starts ****');
            System.debug('------>> list of Contacts -'+updateContactCountryOrState);
            handler.updateCustomAddrFields(updateContactCountryOrState, countryCodeLst, stateCodeLst);
            System.Debug('****** ContactBeforeUpdate update Contact state, country custom field Ends ****');
        }
    }
     System.Debug('****** ContactBeforeUpdate Trigger Ends ****');
}