/*
26-Oct-2012     Srinivas Nallapti       Initial Creation: Dec12 Mkt release 
*/
trigger CampaignBeforeUpdate on Campaign (before update) 
{
    if(Utils_SDF_Methodology.canTrigger('AP53'))
    { 
        AP53_Campaign AP53 = new AP53_Campaign();
        Set<id> NewGlobalCamIds = new set<id>(); //  Campaings that are made Global
        Set<id> OldGlobalCamIds = new set<id>(); //  Campaign That are removed from Global
        
        for(Campaign cam : trigger.new)
        {
            if(cam.Global__c != trigger.oldmap.get(cam.id).Global__c)
            {
                if(cam.Global__c == true)
                {
                   NewGlobalCamIds.add(cam.id);
                   //cam.TargetedCountries__c  = AP53.getAllCountryNames();
                   cam.TargetedCountries__c  = System.Label.CLDEC12MKT10;
                }   
                else
                {
                   OldGlobalCamIds.add(cam.id);
                   cam.TargetedCountries__c = null;     
                }   
            }
        }
        AP53.createUpdateCampaignCountry(NewGlobalCamIds, OldGlobalCamIds);
    }//End of canTrigger
}