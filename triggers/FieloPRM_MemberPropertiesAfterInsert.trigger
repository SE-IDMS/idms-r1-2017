/********************************************************************
* Company: Fielo
* Created Date: 09/08/2016
* Description:
********************************************************************/
trigger FieloPRM_MemberPropertiesAfterInsert on MemberExternalProperties__c (after insert) {

    if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberPropertiesAfterInsert')){
        if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberPropertiesAfterInsertProcess')){
            FieloPRM_AP_MemberPropertiesTriggers.isRunning = true;
            FieloPRM_Batch_MemberPropProcess.runSync(trigger.new);
            FieloPRM_AP_MemberPropertiesTriggers.isRunning = false;
        }
    }
    
}