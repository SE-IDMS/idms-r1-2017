//*********************************************************************************
// Trigger Name     : CCCActionBeforeUpdate
// Purpose          : CCCAction Before Update event trigger
// Created by       : Global Delivery Team
// Date created     : 15th June 2012
// Modified by      :
// Date Modified    :
// Remarks          : For Sep - 12 Release
///********************************************************************************/

trigger CCCActionBeforeUpdate on CCCAction__c (before Update) {
    System.Debug('****** CCCActionBeforeUpdate  Trigger Start****');
    
    if(Utils_SDF_Methodology.canTrigger('handleCCCActionBeforeUpdate')){
        AP_ChatterForCCCAction.updateCCCActionOwnerId(Trigger.new);
        AP_UpdateCCCActionAge.updateTaskAgeOnUpdate(Trigger.newMap,Trigger.oldMap);   
        AP_CCCAction_Handler.handleCCCActionBeforeUpdate(Trigger.newMap,Trigger.oldMap);
	}
	
    System.Debug('****** CCCActionBeforeUpdate  Trigger Finished****');
}