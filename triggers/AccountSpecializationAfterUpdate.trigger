trigger AccountSpecializationAfterUpdate on AccountSpecialization__c (after update) {

	if(Utils_SDF_Methodology.canTrigger('AP_AccountSpecialization'))
    {
    	List<AccountSpecialization__c> lstAccountSpeUpdated = new List<AccountSpecialization__c>();
    	List<AccountSpecialization__c> lstAccountSpeInactivated = new List<AccountSpecialization__c>();
    	

    	for(AccountSpecialization__c asp : trigger.new)
    	{
    		AccountSpecialization__c  OLDasp = trigger.oldmap.get(asp.id);
    		
    		if(asp.Specialization__c != OLDasp.Specialization__c)
    			lstAccountSpeUpdated.add(asp);

    		if(asp.Status__c != OLDasp.Status__c  && asp.Status__c == 'Expired')
    			lstAccountSpeInactivated.add(asp);	
    	}

    	//When Specialization on Account Apecialization is updated, Delete all old Account Specialization Requirements and Create new	
    	if(!lstAccountSpeUpdated.isEmpty())
    		AP_AccountSpecialization.createChildAccountSpecializationRequirements(lstAccountSpeUpdated);

    	//When Account Specialization is made inActive, make all the Account Specialization Requirements Inactive
    	if(!lstAccountSpeInactivated.isEmpty())
    		AP_AccountSpecialization.deActivateAccountSpecializationRequirements(lstAccountSpeInactivated);
    }	

}