/********************************************************************************************************************
    Created By : Shruti Karn
    Description : For October 13 PRM Release:
                 1. Calls AP_AAR_UpdateRequirmentFeature.updateAccountProgramOnExpiration to update the Account Program 'Eligible for' related 
                     to the requirement as ‘Demotion ' when the Requirement status is updated as 'Inactive'.
    
********************************************************************************************************************/
trigger AccountRequirementAfterInsert on AccountAssignedRequirement__c (After Insert) {

    set<Id> setAccountProgramId = new set<Id>();
    for(AccountAssignedRequirement__c req : trigger.new)
    {
        if(Req.RequirementStatus__c == Label.CLOCT13PRM24)
        {
            setAccountProgramId.add(Req.AccountAssignedProgram__c);
        }
    }
    
    if(!setAccountProgramId.isEmpty())
        AP_AAR_UpdateRequirmentFeature.updateAccountProgramOnExpiration(setAccountProgramId);
}