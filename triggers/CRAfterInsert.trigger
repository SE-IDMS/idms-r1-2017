/*
OCT 15 RELEASE
Divya M
BR-2496
*/
trigger CRAfterInsert on CommercialReference__c (after insert) {
    if(Utils_SDF_Methodology.canTrigger('CRAfterInsert')){   
        system.debug('Entered the trigger Affected Product after Insert');
        AP_AffectedProducts.updateProductLine(trigger.new);
    }
}