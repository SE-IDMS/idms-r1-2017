/********************************************************************************************************************
    Author : Shruti Karn
    Created Date : 2 June 2012
    Description : For September Release:
                 1. Checks if Contract has associated Contac
                 2. If yes, calls createValuechainPlayer() to create a corresponding Value Chain Player record for the Contact.
    
********************************************************************************************************************/
trigger ContractAfterInsert on Contract (after insert) {

    list<Contract> lstContract = new list<Contract>();
    
    for(Contract c : trigger.new)
    {
        if(c.ContactName__c != null)
        {
            lstContract.add(c);
            
        }
    }
    if(!lstContract.isEmpty() && (Utils_SDF_Methodology.canTrigger('AP_ContractTriggerUtils') || test.isrunningTest()))
        AP_ContractTriggerUtils.createValuechainPlayer(lstContract);
}