/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 12-Dec-2012
    Description     : IPO Strategic Program Before Update event trigger.
*/

trigger IPOStrategicProgramBeforeUpdate on IPO_Strategic_Program__c (before update) {

  System.Debug('****** IPOStrategicProgramBeforeUpdate Trigger Start ****'); 
if(Utils_SDF_Methodology.canTrigger('IPOStrategicProgramBeforeUpdate '))
    {
    
     if((Trigger.new[0].Global_Functions__c != Trigger.old[0].Global_Functions__c)|| (Trigger.new[0].Global_Business__c != Trigger.old[0].Global_Business__c) ||(Trigger.new[0].Operation__c != Trigger.old[0].Operation__c) ){
     
        IPOStrategicProgramTriggers.IPOStrategicProgramBeforeUpdate(Trigger.new,Trigger.old);
        IPOStrategicProgramTriggers.IPOStrategicProgramBeforeUpdate_CheckDeploymentNetwork(Trigger.new,Trigger.old);
        IPOStrategicProgramTriggers.IPOStrategicProgramNetworkValidationBeforeUpdate(Trigger.new);
         
        if (Trigger.new[0].ValidateScopeonUpdate__c == True)
        
                Trigger.new[0].AddError(Label.ConnectGlobalProgramScopeValidateERR);  
          }              
        
 }
 System.Debug('****** IPOStrategicProgramBeforeUpdate Trigger End ****'); 
}