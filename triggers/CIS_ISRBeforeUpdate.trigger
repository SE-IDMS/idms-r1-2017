/* 
@Author:- Ashish Sharma SESA340747
@Date:- 07/08/2014
@Description:- This trigger is written on CIS_ISR__c custom object to assign CIS correspondent team based upon field of activity and EU Country.
@Events :- before update
*/
trigger CIS_ISRBeforeUpdate on CIS_ISR__c (before update){    
    if(Test.isRunningTest() || Utils_SDF_Methodology.canTrigger('AP_CIS_ISRHandler')){
    List<CIS_ISR__c> toBePopulatedISRs = new  List<CIS_ISR__c>();
    List<CIS_ISR__c> toChangeOwnerShip = new   List<CIS_ISR__c>();
    List<CIS_ISR__c> toChangeOwnerShipInitially = new   List<CIS_ISR__c>();
    List<CIS_ISR__c> isrListToPopulateQueueAndItsMemberEmails = new List<CIS_ISR__c>();
    List<CIS_ISR__c> lstToPopulateEndUserCustomerandCountry = new list<CIS_ISR__c>();
    List<CIS_ISR__c> toChangeStatustosent = new list<CIS_ISR__c>();
    // Iterate through all the ISR in trigger context
    for(CIS_ISR__c  isrInstance:trigger.new){
     if(trigger.oldMap.get(isrInstance.id).CIS_EndUserContact__c!=trigger.newMap.get(isrInstance.id).CIS_EndUserContact__c)
        lstToPopulateEndUserCustomerandCountry.add(isrInstance);
    }
    
    if(!lstToPopulateEndUserCustomerandCountry.isEmpty()){
    AP_CIS_ISRHandler.populateEndUserCustomerandCountry(lstToPopulateEndUserCustomerandCountry);   
    }
    
    for(CIS_ISR__c  isrInstance:trigger.new){
         //Assign Email Id of corresponding CIS Team.
         isrInstance.CIS_OEMCISQueueEmailEmailTypeField__c= isrInstance.CIS_OEMCISQueueEmail__c;
         isrInstance.CIS_QueueEmailEmailTypeField__c= isrInstance.CIS_EUQueueEmail__c;
        // Check if EU Country and Field of activity is not null
        if(((trigger.oldMap.get(isrInstance.Id).CIS_EndUserCountry__c!= trigger.newMap.get(isrInstance.Id).CIS_EndUserCountry__c)||(trigger.oldMap.get(isrInstance.Id).CIS_FieldOfActivity__c!= trigger.newMap.get(isrInstance.Id).CIS_FieldOfActivity__c))&&(isrInstance.CIS_EndUserCountry__c!=null && isrInstance.CIS_FieldOfActivity__c!=null)){
            //Prepare a list of ISR's to assign EU CIS correspondent Team
            isrInstance.CIS_EUCISCorrespondentsTeam__c=null;
            toBePopulatedISRs.add(isrInstance);
        }
        
        if(isrInstance.CIS_OEMCISCorrespondentsTeam__c!=null){
        System.debug('isrInstance.CIS_OEMCISCorrespondentsTeam__c'+isrInstance.CIS_OEMCISCorrespondentsTeam__c);
                toChangeOwnerShip.add(isrInstance);    
        }
        else{
        System.debug('else');
            toChangeOwnerShipInitially.add(isrInstance);    
        }
         System.debug('trigger.newMap.get(isrInstance.Id).CIS_Status__c'+trigger.newMap.get(isrInstance.Id).CIS_Status__c);
         System.debug('trigger.oldMap.get(isrInstance.Id).CIS_Status__c'+trigger.oldMap.get(isrInstance.Id).CIS_Status__c);
         if(isrInstance.CIS_Status__c =='Sent' && (String.valueOf(isrinstance.CIS_SentDate__c)=='')||(String.valueOf(isrinstance.CIS_SentDate__c)==Null)){
             isrinstance.CIS_SentDate__c=system.now();
         }
         
         
         if((trigger.newMap.get(isrInstance.Id).CIS_Status__c !=trigger.oldMap.get(isrInstance.Id).CIS_Status__c )&&(isrInstance.CIS_Status__c =='Reported')){
         isrinstance.CIS_ReportedDate__c=system.now();
         }
         if((trigger.newMap.get(isrInstance.Id).CIS_Status__c !=trigger.oldMap.get(isrInstance.Id).CIS_Status__c )&&(isrInstance.CIS_Status__c =='Acknowledged')){
         isrinstance.CIS_AcknowledgementDate__c=system.today();
         }
         if((trigger.newMap.get(isrInstance.Id).CIS_Status__c !=trigger.oldMap.get(isrInstance.Id).CIS_Status__c )&&(isrInstance.CIS_Status__c =='To Be Closed')){
         isrinstance.CIS_ClosureDate__c=system.now();
         }
         if((trigger.newMap.get(isrInstance.Id).CIS_EUCISCorrespondentsTeam__c!=trigger.oldMap.get(isrInstance.Id).CIS_EUCISCorrespondentsTeam__c)&&(isrInstance.CIS_Status__c =='Acknowledged')){
             toChangeStatustosent.add(isrInstance);
         }
         if(trigger.newMap.get(isrInstance.Id).CIS_Status__c=='Sent' && trigger.oldMap.get(isrInstance.Id).CIS_Status__c=='Acknowledged'){
                toChangeStatustosent.add(isrInstance);
         } 
        
    }
    
    if(!toChangeStatustosent.isEmpty()){
        AP_CIS_ISRHandler.changeTheStatustoSentOnEUTeamChange(toChangeStatustosent);
    }
    
    
    if(!toBePopulatedISRs.isEmpty()){
        AP_CIS_ISRHandler.assignEUCISCorrespondentTeamToISR(toBePopulatedISRs,trigger.newMap);
    }
    /*
    if(!toChangeOwnerShip.isEmpty()){
        AP_CIS_ISRHandler.changeOwnership(toChangeOwnerShip,trigger.newMap);
    }
    
    if(!toChangeOwnerShipInitially.isEmpty()){
        AP_CIS_ISRHandler.changeOwnerShipInitially(toChangeOwnerShipInitially);
    }*/
    
    for(CIS_ISR__c  isrInstance:trigger.new){
        if((isrInstance.CIS_EUCISCorrespondentsTeam__c !=isrInstance.CIS_OEMCISCorrespondentsTeam__c) ){
            isrListToPopulateQueueAndItsMemberEmails.add(isrInstance);    
        } 
    }
    for(CIS_ISR__c  isrInstance:trigger.new){
        if(trigger.newMap.get(isrInstance.Id).CIS_EUCISCorrespondentsTeam__c!=trigger.oldMap.get(isrInstance.Id).CIS_EUCISCorrespondentsTeam__c){
            isrListToPopulateQueueAndItsMemberEmails.add(isrInstance);    
        } 
    }
    if(!isrListToPopulateQueueAndItsMemberEmails.isEmpty()){
          AP_CIS_ISRHandler.populateOEMADEUEmailFields(isrListToPopulateQueueAndItsMemberEmails);
    }
    
    
  }
}