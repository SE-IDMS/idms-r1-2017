trigger CSIAfterUpdate on CustomerSafetyIssue__c (After Update) 
{
if(Test.isRunningTest() ||  Utils_SDF_Methodology.canTrigger('AP_CustomerSafetyIssue_After'))
{
IF(AP_customerSafetyIssue.blntrigger!=true)
{
 
  AP_customerSafetyIssue.getStakeholder(trigger.new,label.CLSEP12I2P27,trigger.newmap,trigger.oldmap,label.CLSEP12I2P33)  ;  
 
}
//Start October 2015 Release, BR-BR-7406. added by Ram chilukuri
 set<string> listCSINew=new set<string>();
 set<string> listCSIOLD=new set<string>(); 
            for(CustomerSafetyIssue__c tCSI : Trigger.new){
            if(tCSI.RelatedbFOCase__c != trigger.oldMap.get(tCSI.Id).RelatedbFOCase__c){
                listCSINew.add(tCSI.RelatedbFOCase__c);
                listCSIOLD.add(trigger.oldMap.get(tCSI.Id).RelatedbFOCase__c);
            }}
    if(listCSINew.size()>0){    
        AP_CustomerSafetyIssue.updateSeverityHighOnCase(listCSINew,listCSIOLD);
    }
 //End October 2015 Release, BR-BR-7406.
}
//'''''Ram Chilukuri
  if(Utils_SDF_Methodology.canTrigger('AP_TechnicalExpertAssessment') || Test.isRunningTest())
     {        
        map<id,string> mapofCaseId=new map<id,string>();
        for(CustomerSafetyIssue__c cutmrSftyIssue:Trigger.new)
            {
                system.debug('~~~~~~~~cutmrSftyIssue'+cutmrSftyIssue);
                CustomerSafetyIssue__c oldcorr=Trigger.oldmap.get(cutmrSftyIssue.id);
                CustomerSafetyIssue__c newcorr=Trigger.newmap.get(cutmrSftyIssue.id);
                
                if(oldcorr.RelatedbFOCase__c != newcorr.RelatedbFOCase__c)
                { 
                  if(newcorr.RelatedbFOCase__c !=null)
                  {
                    mapofCaseId.put(cutmrSftyIssue.RelatedbFOCase__c,'true');
                  }
                    else
                    {
                        mapofCaseId.put(oldcorr.RelatedbFOCase__c,'false');
                    }
                     if(oldcorr.RelatedbFOCase__c != newcorr.RelatedbFOCase__c && oldcorr.RelatedbFOCase__c!=null )
                    {
                        mapofCaseId.put(oldcorr.RelatedbFOCase__c,'false');
                    }
                }
            }
        AP_TechnicalExpertAssessment.updateCSIFlagOnTexRunOnCSIAfterUpadate(mapofCaseId) ;
      }
}