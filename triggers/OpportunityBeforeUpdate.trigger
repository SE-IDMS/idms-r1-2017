/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 26/10/2010
    Description     : Opportunity Before Update event trigger.
    5-Mar-2013  Srinivas Nallapati  PRM Mar13   BR-2910  
    12-Mar-2013 Shruti Karn         PRM Mar13   BR-2910
    28-Mar-2013 Srinivas Nallapati  PRM May13   Allow Admin to assign Oppty  to Partner Users
*/
trigger OpportunityBeforeUpdate on Opportunity (before update) {
    //Srinivas -- Merge
    
    //End of Srinivas - merge
    if(Utils_SDF_Methodology.canTrigger('AP01'))
 {
  
// INITIAL VARIABLE DECLEARTIONS & SOQL QUERIES FOR INITIALIZATION
    //May13 PRM change -  Allow Adminsto change Oppty owner to Partner User
    List<Opportunity>AP01_actionList= new List<Opportunity>(); // Check with anil on this functionality <TODO>
    Map<ID, Contact> contactMap = null;
    Map<id,Opportunity> newOpp= new Map<id,Opportunity>();
    Map<ID, User> userMap  = new Map<ID, User>(); 
    List<Opportunity> oppForecasts = new List<Opportunity>();
    if(VFC61_OpptyClone.isOpptyClone == false && VFC20_CreateSeries.byPassSeries == false)
        userMap  = AP48_OpportunityPartnerAssignment.createUserMap(Trigger.new);
    List<Opportunity> oppScopeChanges = new List<Opportunity>();
    
    Boolean isAdminUser = false;
    String profileName=[select Name from Profile where Id=:UserInfo.getProfileId()].Name;
    List<String> profilesToSKIP = (System.Label.CLMAY13PRM33).split(',');//the valid profile prefix list is populated
    for(String pName : profilesToSKIP)//The profile name should start with the prefix else the displayerrormessage flag is set to true
    {
        if(pName.trim() == profileName.trim())
        {
            isAdminUser = true;             
        }  
    }
    //end Srinivas
    set<id> opptyIdsToCheckForVCP = new set<id>();  // rahul
    List<Opportunity> lstOppPassedToPartner = new List<Opportunity>();   //Added by Srinivas Apr13 PRM 
  
  //Shruti for March 2013 -  add sharing of Opportunity Account for Partner Owner 
    set<id> AccIds = new set<id>();    
    set<ID> setUserId = new set<ID>();
    //set<ID> setOldUserId = new set<ID>();
    list<AccountShare> lstAccShare = new list<AccountShare>();
    //End of shruti March 2013

//END OF VARIABLE DECLERATION

//FOR LOOP --1
    //For Loop -- 1
    for(Integer i=0;i<Trigger.new.size();i++)
    {
       if(trigger.new[i].OpportunityScope__c!= trigger.old[i].OpportunityScope__c && trigger.new[i].OpportunityScope__c!=null)
           oppScopeChanges.add(trigger.new[i]);
      //Srinivas  PRM Apr13
        if(trigger.new[i].PartnerInvolvement__c==System.Label.CLMAY13PRM41  && trigger.new[i].PartnerInvolvement__c != trigger.old[i].PartnerInvolvement__c)    
            lstOppPassedToPartner.add(trigger.new[i]);
        //End of PRM Apr13
        if(Trigger.new[i].Amount!=null && trigger.new[i].Amount!=trigger.old[i].Amount)
            Trigger.new[i].TECH_AmountEUR__c=Utils_Methods.convertCurrencyToEuroforDatedCurrency(Trigger.new[i].CurrencyIsoCode , Trigger.new[i].Amount);
        ///rahul
        /* Modified for BR-5894 */
        if((trigger.new[i].IncludedInForecast__c == Label.CL00584))
                oppForecasts.add(trigger.new[i]);
                
        /* End of modification for BR-5894 */       
        if(VFC61_OpptyClone.isOpptyClone == false  && VFC20_CreateSeries.byPassSeries == false)
        {
            if(Trigger.new[i].OwnerId != Trigger.old[i].OwnerId)
            {
                User newOwner1 = userMap.get(Trigger.new[i].OwnerId);
                if(newOwner1.IsPortalEnabled && String.valueOf(newOwner1.profileId).contains(Label.CLSEP12PRM18))
                {
                    opptyIdsToCheckForVCP.add(Trigger.new[i].id);
                }
                System.debug('###### Adding the opportunity so that it is processed');
                //AP01_actionList.add(Trigger.new[i]); moving outside bypass
            }
        }                
        AP01_actionList.add(Trigger.new[i]); 
         /*Modified By Abhishek on 24/05/2011 as part of ACC release*/
            if((Trigger.new[i].Name != Trigger.old[i].Name)||(Trigger.new[i].OwnerId != Trigger.old[i].OwnerId)||(Trigger.new[i].AccountId != Trigger.old[i].AccountId)
            ||(Trigger.new[i].OpptyType__c != Trigger.old[i].OpptyType__c)||(Trigger.new[i].OpportunityCategory__c != Trigger.old[i].OpportunityCategory__c)||(Trigger.new[i].CloseDate != Trigger.old[i].CloseDate)||
            (Trigger.new[i].OpptyPriorityLevel__c!= Trigger.old[i].OpptyPriorityLevel__c)||(Trigger.new[i].LeadingBusiness__c != Trigger.old[i].LeadingBusiness__c)||(Trigger.new[i].OpportunityScope__c != Trigger.old[i].OpportunityScope__c)||
            (Trigger.new[i].MarketSegment__c != Trigger.old[i].MarketSegment__c)||(Trigger.new[i].MarketSubSegment__c!= Trigger.old[i].MarketSubSegment__c))
            {
            newOPP.put(Trigger.new[i].id,Trigger.new[i]);
            }
        ///rahul

        //Srinivas  -   Dont check if the LoggedIn user is PartnerORFConverter   // May13 Srinivas Skip for Admin Users
        if(VFC61_OpptyClone.isOpptyClone == false  && VFC20_CreateSeries.byPassSeries == false)
        {
            if(isAdminUser == false && userMap.get(UserInfo.getUserId()).PartnerORFConverter__c==false && ( (Trigger.new[i].OwnerId != Trigger.old[i].OwnerId) || (Trigger.new[i].OwnerId != Trigger.new[i].TECH_OwnerID__c)) )
            {
                //System.debug('###### Adding the opportunity so that it is processed');
                //AP01_actionList.add(Trigger.new[i]); //moved it above
                
                User newOwner = userMap.get(Trigger.new[i].OwnerId);
                
                if(newOwner.IsPortalEnabled){
                  
                  // Initialize contactMap at the first need only
                  if(contactMap == null && VFC61_OpptyClone.isOpptyClone == false){
                    contactMap = AP48_OpportunityPartnerAssignment.createContactMap();
                  }
                 
                 if(!contactMap.containsKey(newOwner.contactId)){
                   Trigger.new[i].addError(Label.CL10237);
                 }
               }
            }//End of srinivas
        }

        //Shruti for March 2013 -  add sharing of Opportunity Account for Partner Owner
        Id oppID = Trigger.new[i].id; // Srinivas -  for merging code
      if((trigger.newMap.get(oppID).ownerID != trigger.oldMap.get(oppID).ownerID) || (trigger.newMap.get(oppID).accountid!= trigger.oldMap.get(oppID).accountid))
        {
            setUserId.add(trigger.newMap.get(oppID).ownerid);
            //setOldUserId.add(trigger.oldMap.get(oppID).ownerid);
        }
        AccIds.add(trigger.newMap.get(oppID).AccountId);  
        //End of shruti
        
        //START: July14 Release BR-5303
        //Updating Partner Reason for closed lost to the main reason field.
        
        if(trigger.new[i].ChannelPartnerStage__c != null && System.label.CLJUL14PRM01.contains(trigger.new[i].ChannelPartnerStage__c) && trigger.new[i].ReasonClosedLost__c!= null &&  trigger.new[i].ReasonClosedLost__c!= trigger.new[i].Reason__c && !String.isBlank(trigger.new[i].PartnerInvolvement__c))
        {
            trigger.new[i].Reason__c = trigger.new[i].ReasonClosedLost__c;
        
        }        
        //END:July14 Release BR-5303    

        //START:Oct14 Release 
        //Updating back for Channel Partner
        if(trigger.new[i].Status__c !=null && System.label.CLOCT14PRM80.contains(trigger.new[i].Status__c) && trigger.new[i].Reason__c!= null &&  trigger.new[i].ReasonClosedLost__c!= trigger.new[i].Reason__c)
        {
            trigger.new[i].ReasonClosedLost__c = trigger.new[i].Reason__c;          
        
        }   
        //END:Oct14 Release 
        
    }
// END OF FOR LOOP --1

        /* Modified for BR-5894 */
        if(!(oppForecasts.isEmpty()))
            AP01_OpportunityFieldUpdate.populateForecast(oppForecasts);
        /* End of modification for BR-5894 */

//MIDDLE LOGIC

    //Shruti - March 2013
    
    if(VFC61_OpptyClone.isOpptyClone == false  && VFC20_CreateSeries.byPassSeries == false)
    {
    map<id,Account> mpAccount = new map<id,Account>([select id, name, ownerId from Account where id IN:AccIds]); // Srinivas UAT May
    map<Id,User> mapOppOWner = new map<Id,User>([Select id,IsPortalEnabled from User where id in :setUserId limit 10000]);
    //End of shruti March 2013

//END OF MIDDILE LOGIC

//FOR LOOP --2
    for(Id oppID : trigger.newMap.keySet())  /// 2
    {
        //Shruti March 2013
        if(mpAccount.containsKey(trigger.newMap.get(oppID).AccountId) && mpAccount.get(trigger.newMap.get(oppID).AccountId).ownerId !=  trigger.newMap.get(oppID).OwnerId)
        if((trigger.newMap.get(oppID).ownerID != trigger.oldMap.get(oppID).ownerID) || (trigger.newMap.get(oppID).accountid!= trigger.oldMap.get(oppID).accountid))
        {
             if(mapOppOWner != null && mapOppOWner.containsKey(trigger.newMap.get(oppID).ownerid))
            {
                if(mapOppOWner.get(trigger.newMap.get(oppID).OwnerId).IsPortalEnabled)
                {
                    AccountShare aShare = new AccountShare();
                    aShare.AccountId= trigger.newMap.get(oppID).AccountId;
                    aShare.UserOrGroupId = trigger.newMap.get(oppID).OwnerId;
                    aShare.AccountAccessLevel = Label.CL00418;
                    aShare.OpportunityAccessLevel = Label.CLMAR13PRM019;
                    lstAccShare.add(aShare);
                }
            }
        }
        //End of shruti March 2013

    }
    }
// END OF FOR LOOP --2

//FINAL CALLS TO AP METHODS
    //Srinivas  Apr13 PRM
    if(lstOppPassedToPartner.size() > 0)
        AP01_OpportunityFieldUpdate.checkOpportunityPassedToPartner(lstOppPassedToPartner);
    //ENd of Apr13 PRM
    if(AP01_actionList.size()>0){
            AP01_OpportunityFieldUpdate.populateTechOwnerId(AP01_actionList);
    }

    //Change  Rahul-- Can move to method
    if(opptyIdsToCheckForVCP.size() > 0)
    {
        List<OPP_ValueChainPlayers__c> lstOppVCP = new List<OPP_ValueChainPlayers__c>();
        lstOppVCP = [SELECT id, OpportunityName__c from OPP_ValueChainPlayers__c where OpportunityName__r.ID in :opptyIdsToCheckForVCP and ContactRole__c=:Label.CLSEP12PRM17 limit 10000];
        
        set<id> VCPenabledOpptys = new set<id>();
                  
        for(OPP_ValueChainPlayers__c vcp : lstOppVCP)
        {
            VCPenabledOpptys.add(vcp.OpportunityName__c);
            
        }
        for(Id opptyId : opptyIdsToCheckForVCP)
        {
            if(!VCPenabledOpptys.contains(opptyId) && VFC_ConvertORFToOpportunity.byPassValidation == false)
             trigger.newmap.get(opptyId).addError(Label.CLDECPRM01);
        }
        
    }
    //End of Change  Rahul

    //Shruti -- Inserting  March 2013
    try
    {
        insert lstAccShare;
    }
    catch(Exception e)
    {
        Trigger.new[0].addError(Label.CLMAR13PRM025);
    }
    if(newOPP.size()>0)
    {            
            AP11_UpdateQuoteLink.isSynchNeededForOpp(newOPP);
    }
    if(!(oppScopeChanges.isEmpty()))
        AP01_OpportunityFieldUpdate.updateTechIsLevel2(oppScopeChanges);
    //End of Shruti March 2013

//END OF CALLS TO AP METHODS

 }  // End of CanTrigger
    
  /************************************************************************************************************/
 /************************************************************************************************************/
 
 //START: MAY14 Release
 //Description: BR-5565, Oppt: sales people will only be able to select the programs a partner account has been assigned to
 if(Utils_SDF_Methodology.canTrigger('AP05'))
 {
    AP05_OpportunityTrigger.validateAsssignedPrograms(trigger.new);
 } 
 //END: MAY14 Release
 
    
}