trigger CHR_ChangeReqBeforeUpdate on CHR_ChangeReq__c (before update) 
{		
    for(CHR_ChangeReq__c chr : trigger.new)
    {

			CHR_ChangeReq__c beforeUpdate = System.Trigger.oldMap.get(chr.Id); 

        		if((beforeUpdate.StepManual__c == CHR_ChangeReqCfg__c.getOrgDefaults().AssignBSTeamStatus__c) && beforeUpdate.StepManual__c<> chr.StepManual__c) 
        		{
	    			for (REF_TeamRef__c bsteam : [SELECT id, name,Type__c,Queue__c FROM REF_TeamRef__c WHERE id = :chr.TeamReference__c]) 
	    			{
	        			chr.OwnerId = bsteam.Queue__c;
	       
	      			}  
        
				}    
    		
    	
    }
}