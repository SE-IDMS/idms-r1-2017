/*
Oct 15 rel
Divya M
BR-7721
*/
trigger RMAAtferUpdate on RMA__c (after Update) {
   if(Utils_SDF_Methodology.canTrigger('RMAAtferUpdate')){
       for(RMA__c rma : trigger.new){
           if(Trigger.oldMap.get(rma .id).ShippedFromCustomer__c != Trigger.newMap.get(rma .id).ShippedFromCustomer__c){
                AP_RR_FieldUpdate.updateTEXShippedFromCustomer(trigger.new);
            }
        }
    }
}