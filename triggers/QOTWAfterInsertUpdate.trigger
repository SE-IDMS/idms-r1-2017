//*********************************************************
// Author: Stephen Norbury
// Creation date: 05/06/2012
// Intent:  Trigger handling;
//
// On activation of the Question, ...
//
// Change History
// --------------
// Author:
// Change date: 
// Intent: 
//*********************************************************/*

trigger QOTWAfterInsertUpdate on QOTW__c (after insert, after update) 
{
  // De-activate any other Questions
  
  Id QuestionId = null;
    
  for (QOTW__c q : Trigger.new)  
  {
    if (Trigger.isInsert || (Trigger.isUpdate && q.IsActive__c == true && Trigger.oldMap.get(q.Id).IsActive__c == false))
    {
      QuestionId = q.Id; // Select this Question as Active
    }
  }
  
  if (QuestionId != null)
  {
    List <QOTW__c> questions = [select Id, IsActive__c from QOTW__c where IsActive__c = true and Id != :QuestionId];
    
    if (questions != null && questions.size() != 0)
    {
      for (QOTW__c q : questions)
      {
        q.IsActive__c = false;
      }
      
      update questions; 
    }
  }
  
  // End
}