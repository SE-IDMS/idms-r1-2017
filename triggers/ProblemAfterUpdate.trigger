//*********************************************************************************
// Trigger Name     : ProblemAfterUpdate
// Purpose          : Problem  After Update event trigger
// Created by       : Global Delivery Team
// Date created     : 18th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/


trigger ProblemAfterUpdate on Problem__c (after update) {
    System.Debug('****** ProblemAfterUpdate Trigger Start ****');
     
    Set<Id> PrbIds = new Set<Id>();
    if(Test.isRunningTest() || Utils_SDF_Methodology.canTrigger('AP1000_3')){ 
    
    
       //Advance I2P Mohit Start
      Map<Id,Problem__c> mapNewPRB=new Map<Id,Problem__c>();
      Map<Id,Problem__c> mapOldPRB=new Map<Id,Problem__c>();
      
      
      
   //Advance I2P Mohit End
    
    //if(Utils_SDF_Methodology.canTrigger('AP1000')){ 
        //Advanced I2P : Added by Hari Krishna Start
        AP1000_Problem.addOwnerToTeamMember(Trigger.oldMap,Trigger.newMap);  
    //Advanced I2P : Added by Hari Krishna End

       
     //Advance I2P Mohit Start
     
       //AP1000_Problem.ScenarioCheckOnUpdate(Trigger.oldMap,Trigger.newMap);
      for(Problem__c varNewPRB:Trigger.new)
      {
        If(varNewPRB.RecordTypeId !=Label.CLI2PAPR120014)
        {
           mapNewPRB.Put(varNewPRB.Id,varNewPRB);
        }
        
        //OCT 2015 Release : Added by Uttara - BR-7096
        if(varNewPRB.Sensitivity__c != Trigger.oldMap.get(varNewPRB.Id).Sensitivity__c || varNewPRB.Status__c != Trigger.oldMap.get(varNewPRB.Id).Status__c || varNewPRB.TECH_Symptoms__c != Trigger.oldMap.get(varNewPRB.Id).TECH_Symptoms__c || varNewPRB.TECH_Symptoms__c !=null || varNewPRB.CountOfCommercialReferenceRecords__c>0 || varNewPRB.CountOfCommercialReferenceRecords__c != Trigger.oldMap.get(varNewPRB.Id).CountOfCommercialReferenceRecords__c  || varNewPRB.Severity__c != Trigger.oldMap.get(varNewPRB.Id).Severity__c) {
            PrbIds.add(varNewPRB.Id);    
        }

      }
      for(Problem__c varOldPRB:Trigger.Old)
      {
        If(varOldPRB.RecordTypeId !=Label.CLI2PAPR120014)
        {
           mapOldPRB.Put(varOldPRB.Id,varOldPRB);
        }
      }
      
   
      if(mapNewPRB.size()>0 && mapNewPRB!=null && mapOldPRB.size()>0 && mapOldPRB!=null)
          AP1000_Problem.ScenarioCheckOnUpdate(mapOldPRB,mapNewPRB);
          
   //Q3 2016
        AP1000_Problem.ProblemLeaderRolechange(trigger.oldmap,trigger.newmap);  
     
    }    
    //OCT 2015 Release : Added by Uttara - BR-7096    
    //AP1000_Problem.caseUpdate(Trigger.newMap);
    if(!PrbIds.isEmpty()) {
        if(!System.isFuture()) {
            AP_UpdateMatchingCase.performMatching(PrbIds);
           //AP_UpdateMatchingCase_Hana.performMatching(PrbIds); 
        }    
        //AP1000_Problem.updateCase(lstPrb);
    } 
    System.Debug('****** ProblemAfterUpdate Trigger End ****');
}