/********************************************************************************************************************
    Created By : Shruti Karn
    Description : For May 13 PRM Release:
                 1. Calls AP_AAR_UpdateRequirmentFeature.updateAccountFeature to update the Feature related 
                     to the requirement as ‘Active when all the Requirement related to feature has ‘Met’ or ‘Waived’ status.
    Updated By : Shruti Karn
    Description : For October 13 PRM Release:
                 1. Calls AP_AAR_UpdateRequirmentFeature.updateAccountProgramOnExpiration to update the Account Program 'Eligible for' related 
                     to the requirement as ‘Demotion ' when the Requirement status is updated as 'Inactive'.
    
********************************************************************************************************************/
trigger AARAfterUpdate on AccountAssignedRequirement__c (after update) {
     set<Id> setAccountProgramId = new set<Id>();
     if(Utils_SDF_Methodology.canTrigger('AP_AAR_UpdateRequirmentFeature') || test.isrunningtest())
        AP_AAR_UpdateRequirmentFeature.updateAccountFeature(trigger.newMap , trigger.oldMap);
     for(AccountAssignedRequirement__c req : trigger.new)
     {
         if(Req.RequirementStatus__c != trigger.oldMap.get(req.Id).RequirementStatus__c && Req.RequirementStatus__c == Label.CLOCT13PRM24)
         {
             setAccountProgramId.add(Req.AccountAssignedProgram__c);
         }
     }
     
     if(!setAccountProgramId.isEmpty())
          AP_AAR_UpdateRequirmentFeature.updateAccountProgramOnExpiration(setAccountProgramId);
}