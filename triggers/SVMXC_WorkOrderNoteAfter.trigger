trigger SVMXC_WorkOrderNoteAfter on WorkOrderNote__c (after insert, after update) {

	if (trigger.isInsert || trigger.isUpdate) 
		SVMXC_WorkOrderNoteAfter.syncWorkOrderNotesToWO(trigger.new, trigger.OldMap, trigger.isInsert, trigger.isUpdate);
}