//********************************************************************************************************
//  Trigger Name            : AcknowledgmentTemplateAfterUpdate
//  Purpose                 : AcknowledgmentTemplate__c  After Update event trigger , 
//                            Set the Other Defailt Templates to False
//  Created by              : Vimal Karunakaran (Global Delivery Team)
//  Date created            : 11th March 2015
///*******************************************************************************************************/

/*
    Modification History    :
    Modified By             : 
    Modified Date           : 
    Description             : 
*/
trigger AcknowledgmentTemplateAfterUpdate on AcknowledgmentTemplate__c (after update) {
	System.Debug('****** AcknowledgmentTemplateAfterUpdate Trigger Start ****');
    if(Utils_SDF_Methodology.canTrigger('AcknowledgmentTemplateAfterUpdate')){
        AP_ACK_Template_Handler.unCheckOtherDefaultTemplates(Trigger.oldMap, Trigger.newMap,false);
    }
    System.Debug('****** AcknowledgmentTemplateAfterUpdate Trigger Ends ****');
}