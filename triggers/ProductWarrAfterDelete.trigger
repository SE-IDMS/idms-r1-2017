trigger ProductWarrAfterDelete on SVMXC__Warranty__c (after delete) {
 if(Utils_SDF_Methodology.canTrigger('SVMX20')){
 List<SVMXC__Installed_Product__c> IPlisttoUpdate= New List<SVMXC__Installed_Product__c>();
     set<Id> ipset = New set<Id>();
     for(SVMXC__Warranty__c wr :trigger.old)
     {
        if(wr.SVMXC__Installed_Product__c !=null)
        ipset.add(wr.SVMXC__Installed_Product__c);
      }
          if(ipset.size()>0){
            for(SVMXC__Installed_Product__c ip :[Select Id ,SVMXC__Warranty_Start_Date__c,SVMXC__Warranty_End_Date__c From SVMXC__Installed_Product__c Where Id in :ipset])
            {
                ip.SVMXC__Warranty_Start_Date__c=null;
                ip.SVMXC__Warranty_End_Date__c= null;
                 IPlisttoUpdate.add(ip);
            }
          }
       if(IPlisttoUpdate.size()>0)
        update IPlisttoUpdate;
    }
}