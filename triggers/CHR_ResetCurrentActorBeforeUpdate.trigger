trigger CHR_ResetCurrentActorBeforeUpdate on CHR_ChangeReq__c (before update) {
	
	for(CHR_ChangeReq__c chr : trigger.new) {
		
		CHR_ChangeReq__c beforeUpdate = System.Trigger.oldMap.get(chr.Id); 
		
  		  if (beforeUpdate.OwnerId != chr.OwnerId) {
	       	  chr.CurrentActor__c = null;
  		  }
    }
}