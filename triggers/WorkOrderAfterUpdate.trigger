/*     
@Author: Anita D'Souza
Created Date: 03-08-2011
Description: This trigger fires on Work Order on After Update
**********
Scenarios:
**********
1.  Update Work Order ownership include Secondary Technician with Read only Sharing rule.

Date Modified       Modified By         Comments
------------------------------------------------------------------------
02-08-2011          Anita D'Souza       Logic to add sharing rule for secondary Technicians
16-07-2013          Ramu Veligeti       Logic to update Work Order Group's status
17-07-2013          Ramu Veligeti       Logic to update Account's TECH_IsSVMXRecordPresent__c field
17-09-2013          Deepak              Logic to update WON field i.e Work Order status etc..
04-03-2014          Deepak              Asset Categorisation.
04-03-2014          Deepak              Updating Contact in case of Service Object.
*/
trigger WorkOrderAfterUpdate on SVMXC__Service_Order__c (after update) 
{   
    System.debug('TRIGGER AFTER UPDATE WO - YASS');
    //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SVMX05'))
    {
        // to add sharing to the creator of a follow-up WO
        SVMXC_WorkOrderAfter.createSharingForCreator(trigger.New, trigger.OldMap, trigger.isInsert, trigger.isUpdate);
        
        String CANCELLED =System.label.CLDEC13SRV03;
        String UNSCHEDULED =System.label.CLDEC13SRV04;
        String CUSTOMER_CONFIRMED= System.label.CLDEC13SRV05;
        // Hari Added for DEC Release 2013
     List<SVMXC__Service_Order__c> woListforAssetCategory = new List<SVMXC__Service_Order__c>();
    List<SVMXC__Service_Order__c> wolist = New List<SVMXC__Service_Order__c>();
    List<SVMXC__Service_Order__c> wolst = new List<SVMXC__Service_Order__c>();
    Set<Id> techId = new Set<Id>();
    set<id> techid1 = new set<id>();
    set<id> technicianId = new set<id>();
    Set<Id> woId = new Set<Id>();
    Set<Id> wordId = new Set<Id>();
    set <id> woset1 = new set <id>();
    Set<Id> workorderset = new Set<Id>();
    set<Id> Wovalidset = New set<Id>();
    set<Id> IPset = New set<Id>();
    set<Id> shareset = New set<Id>();
    set<Id> woshareset = New set<Id>();
    set<id> partsset = new set<id>();
    set<id> ippset1 = new set<id>();
    set<id> caseidset = new set<id>();
    set<id> WOCompId = new set<id>(); //Added by VISHNU for Nov 16 Release BR-10694
    List<SVMXC__Service_Order__Share> woShareLst = new List<SVMXC__Service_Order__Share>();
    List<SVMXC__Service_Order__Share> wordShareLst = new List<SVMXC__Service_Order__Share>();
    Map<Id,Set<Id>> EventMap = new Map<Id,Set<Id>>();
    Map<Id,Set<Id>> EvntMap = new Map<Id,Set<Id>>();
    List<Event> Eventlst = new List<Event>();
    List<Event> Evntlst = new List<Event>();
    Map<Id,SVMXC__Service_Group_Members__c> techMap;
    Map<Id,SVMXC__Service_Group_Members__c> technicianMap;
    List<SVMXC__Service_Order__c> woAcceptRejectList = new List<SVMXC__Service_Order__c>();
    
    for(SVMXC__Service_Order__c a:trigger.new)
    {  
//  Added by Anand For Workorder validation
        System.debug('just before adding to Wovalidset');
        //Below Line commented by VISHNU C for Q3 2016 BR-9732
        //if(a.SVMXC__Order_Status__c =='Service Complete' && a.SVMXC__Order_Status__c!=trigger.oldmap.get(a.id).SVMXC__Order_Status__c)        // && a.ownerId!=trigger.oldmap.get(a.id).OwnerId) // Yassine.B 07/05/2015 : comment put temporary for the E2E business presentation
        //Below code added by VISHNU for Nov 16 Release BR-10694
        if(a.recordtypeid != '012120000019LAp' && a.SVMXC__Order_Status__c=='Service Complete')
        {
                WOCompId.add(a.id);
        }
     
        if(a.Tech_AutoValidation__c == true && a.Tech_AutoValidation__c!=trigger.oldmap.get(a.id).Tech_AutoValidation__c) //Added Line by VISHNU C for Q3 2016 BR-9732
            {
               Wovalidset.add(a.Id);
              // techId1.add(a.SVMXC__Group_Member__c);
               System.debug('just after adding to Wovalidset');
            }
            
        if(a.SVMXC__Order_Type__c =='Commissioning & Installation' && (a.SVMXC__Order_Status__c=='Service Complete' || a.SVMXC__Order_Status__c=='Service Validated') && a.SVMXC__Order_Status__c!=trigger.oldmap.get(a.id).SVMXC__Order_Status__c)
            {
                IPset.add(a.SVMXC__Component__c);
                workorderset.add(a.id);
            
            }
            
        if(a.SVMXC__Order_Status__c == 'Customer Confirmed' && a.SVMXC__Order_Status__c!=trigger.oldmap.get(a.id).SVMXC__Order_Status__c)
        {
            partsset.add(a.id);
        }
        
        if(a.SVMXC__Order_Status__c == 'Customer Confirmed' && trigger.oldmap.get(a.id).SVMXC__Order_Status__c=='Acknowledge FSE'&& a.SVMXC__Order_Status__c!=trigger.oldmap.get(a.id).SVMXC__Order_Status__c)
        {
            woshareset.add(a.id);
        }

        //wolist.add(a);
        if( a.SVMXC__Group_Member__c != null && a.SVMXC__Order_Status__c == 'Customer Confirmed' && trigger.oldMap.get(a.id).SVMXC__Order_Status__c!=a.SVMXC__Order_Status__c)
        {
            wolst.add(a);
            woId.add(a.Id);
            techId.add(a.SVMXC__Group_Member__c);
            system.debug('wolst==='+wolst+'==woId=='+woId+'==techId=='+techId);
        } 
        if(a.SVMXC__Order_Status__c == 'Acknowledge FSE' && trigger.oldMap.get(a.id).SVMXC__Order_Status__c!=trigger.NewMap.get(a.id).SVMXC__Order_Status__c && a.SVMXC__Group_Member__c != null)
        {
            wolist.add(a);
            wordId.add(a.Id);
            technicianId.add(a.SVMXC__Group_Member__c);
            system.debug('wolist==='+wolist+'==wordId=='+wordId+'==technicianId=='+technicianId);
        } 
        
        if(a.SVMXC__Component__c !=Trigger.OldMap.get(a.id).SVMXC__Component__c)
        {
        
            woListforAssetCategory.add(a);
        }
           //modified By Anand for Q3 Release for BR-8826
        if((a.SVMXC__Order_Status__c=='Service Validated' || a.SVMXC__Order_Status__c=='Closed'|| a.SVMXC__Order_Status__c=='Cancelled')&&(a.SVMXC__Order_Status__c!=trigger.OldMap.get(a.id).SVMXC__Order_Status__c) &&  a.SVMXC__Case__c != null )
        {
            
            caseidset.add(a.SVMXC__Case__c);
        
        }
        
        if(a.SVMXC__Component__c != null && a.SVMXC__Component__c !=Trigger.OldMap.get(a.id).SVMXC__Component__c){
            
            ippset1.add(a.SVMXC__Component__c);
        }  
        
        if(a.SVMXC__Order_Status__c =='Service Complete' &&  a.ownerId!=trigger.oldmap.get(a.id).OwnerId)
            {
               shareset.add(a.Id);
            }
        if(a.SVMXC__Order_Status__c == 'Service Validated' && a.SVMXC__Order_Status__c!=trigger.oldMap.get(a.id).SVMXC__Order_Status__c)
        {
        
            woset1.add(a.id);
        }
        if((a.WOLevel__c == 'Accepted' || a.WOLevel__c == 'Rejected'  )  /*&& a.WOLevel__c !=Trigger.OldMap.get(a.id).WOLevel__c*/){
            woAcceptRejectList.add(a);            
        }
        
    } 
    //Below code added by VISHNU for Nov 16 Release BR-10694    
    if( WOCompId != null && WOCompId.size() > 0 )
    {
        List<SVMXC__Service_Order_Line__c> WDRMAList = new List<SVMXC__Service_Order_Line__c>();
        set<id>partset = new set<id>();
        set<id>wdId= new set<id>();
        WDRMAList = [select Id,Parts_Order_Line_RMA__c from SVMXC__Service_Order_Line__c where Parts_Order_Line_RMA__c != null and SVMXC__Service_Order__c in :WOCompId];
        for(SVMXC__Service_Order_Line__c wdRMA:WDRMAList )
        {
            partset.add(wdRMA.Parts_Order_Line_RMA__c);
            wdId.add(wdRMA.id);
        
        }
        if(partset!=null && partset.size()>0 && wdId!=null && wdId.size()>0)
        {
            AP_WorkDetails.partsordertracknumberupdate(partset,wdId);
        }
        
    }
    //Vishnu Code Ends here for BR-10694
    /********************************************************************************************************************/
    // Scenario: 2
    // Change the Owner to Technician on change of Status to 'Customer confirmed'
    // Apply Sharing Rules to the Secondary Technician in 'Read' mode.
    /********************************************************************************************************************/    
    if(wolst.size()>0)
    {
        system.debug('\n **********  Apply Sharing Rules to the Secondary Technician in Read mode. '+wolst);   
        //Fetching Primary Technician's Salesforce User from Technician Object
        try
        {
            techMap = new Map<Id,SVMXC__Service_Group_Members__c>([Select Id,SVMXC__Salesforce_User__c 
                                                                 from SVMXC__Service_Group_Members__c 
                                                                where Id IN :techId]);
        }
        catch (Exception e){system.debug('Exception in Map=='+e);}                                                             
                
        Set<Id> sfid = new Set<Id>();
        for(SVMXC__Service_Group_Members__c a:techMap.values())
            sfid.add(a.SVMXC__Salesforce_User__c);
        
        //Fetching ServiceMax Events for the Secondary Technician
        try
        {
            Eventlst = [Select Id,WhatId,OwnerId from Event where WhatId IN :woId /*and OwnerId NOT IN :sfid*/];
            system.debug('Event='+Eventlst );
        }
        catch (Exception e){system.debug('Exceptoin==='+e);}       
        system.debug('EventMap=='+EventMap);        
        for(SVMXC__Service_Order__c a:wolst)
        {           
            for(Event b:Eventlst)
            {
                if(a.Id == b.WhatID)
                {
                    woShareLst.add(new SVMXC__Service_Order__Share(AccessLevel = 'Edit',                        //Sharing 'Read' access to Primary Tech
                                                                           ParentId = a.Id, 
                                                                           UserOrGroupId = b.OwnerId));    
                }
            } 
        }    
        if(woShareLst.size()>0) 
        {
             system.debug('************* '+woShareLst );
            try{
                Database.insert(woShareLst, false);                                                                              //Inserting Work Order Share details
            }catch (Exception e){system.debug('Testing error'+e.getmessage());}
        }
    }
    /********************************************************************************************************************/
    // Scenario: 2a 
    // Apply Sharing Rules to the Secondary Technician in 'Read/Edit mode Status to 'Acknowledge FSR'
    /********************************************************************************************************************/    
    if(wolist.size()>0)
    {
        //Fetching Primary Technician's Salesforce User from Technician Object
        try
        {
            technicianMap = new Map<Id,SVMXC__Service_Group_Members__c>([Select Id,SVMXC__Salesforce_User__c from SVMXC__Service_Group_Members__c where Id IN :technicianId]);
        }
        catch (Exception e){system.debug('Exception in Map=='+e);}                                                             
                
        Set<Id> sfUserid = new Set<Id>();
        for(SVMXC__Service_Group_Members__c a:technicianMap.values())
            sfUserid.add(a.SVMXC__Salesforce_User__c);
        
        //Fetching ServiceMax Events for the Secondary Technician
        try
        {
            Evntlst = [Select Id,WhatId,OwnerId from Event where WhatId IN :wordId and OwnerId NOT IN :sfUserid];
            System.debug('\n ********'+Evntlst);
            system.debug('Event='+Evntlst );
        }
        catch (Exception e){system.debug('Exceptoin==='+e);}       
        system.debug('EvntMap=='+EvntMap);        
        for(SVMXC__Service_Order__c a:wolist)
        {           
            for(Event b:Evntlst)
            {
                if(a.Id == b.WhatID)
                {
                    wordShareLst.add(new SVMXC__Service_Order__Share(AccessLevel = 'Edit',                        //Sharing 'Read' access to Primary Tech
                                                                           ParentId = a.Id, 
                                                                           UserOrGroupId = b.OwnerId));    
                }
            } 
        }    
        if(wordShareLst.size()>0) 
        {
            try{
                Database.insert(wordShareLst);                                                                              //Inserting Work Order Share details
            }catch (Exception e){system.debug('Testing error'+e.getmessage());}
        }
    }
    /********************************************************************************************************************/
    // Scenario: 3
    // Purpose :
    //          (a), When a WO Status is updated to "Cancelled" or "Rescheduled" related Assigned Tools Technicians Status field will be updated to "Cancelled"  which are other than Rejected. 
    //          (b), When work order is update with new Team member , respective Assigned tools/Technician record was update with primary
    //          (c), When work order Back Office Reference is update , respective Event records was update with Back Office Reference
    //          (d), When work order Name is updated , respective Event records was update with Back work order Name
    
    // Created by : Hari Krishna - Global Delivery Team
    // Date created : 12th Feb 2013
    // Modified by :
    // Date Modified :
    // Remarks : For May - 13 Release
    /********************************************************************************************************************/ 
      Set<id> woStatusChangeidSet = new Set<id>();
      List<SVMXC__Service_Order__c> wOrdertoProcess = new List<SVMXC__Service_Order__c>();
       List<SVMXC__Service_Order__c> wOrdertobeupdate = new List<SVMXC__Service_Order__c>();
      List<SVMXC__Service_Order__c> wOrdertoProcessThree = new List<SVMXC__Service_Order__c>();
      List<SVMXC__Service_Order__c> wOrdertoProcessFour = new List<SVMXC__Service_Order__c>();
      for(SVMXC__Service_Order__c wo:trigger.new){
          
        if((wo.SVMXC__Order_Status__c == CANCELLED && Trigger.OldMap.get(wo.id).SVMXC__Order_Status__c  != CANCELLED)
            || (wo.SVMXC__Order_Status__c == UNSCHEDULED  && wo.SVMXC__Order_Status__c != trigger.oldMap.get(wo.Id).SVMXC__Order_Status__c && 
            (trigger.oldMap.get(wo.Id).SVMXC__Order_Status__c == CUSTOMER_CONFIRMED ||trigger.oldMap.get(wo.Id).SVMXC__Order_Status__c == 'Acknowledge FSE')))
        {
             woStatusChangeidSet.add(wo.id);
             //wo.SVMXC__Group_Member__c = null;
             //wo.SVMXC__Scheduled_Date_Time__c = null;
             wOrdertobeupdate.add(wo);
             
        }
        
        if(UserInfo.getProfileId()!=Id.valueOf(System.LABEL.CLDEC14SRV01))//Added for EUS#046939 for Dec 14 Release
        {            
            if((wo.SVMXC__Group_Member__c !=Trigger.OldMap.get(wo.id).SVMXC__Group_Member__c ))
            {           
                wOrdertoProcess.add(wo);        
            }
            
            if(wo.BackOfficeReference__c !=  Trigger.OldMap.get(wo.id).BackOfficeReference__c)
            {           
                wOrdertoProcessThree.add(wo);        
            }
            if(wo.WorkOrderName__c !=  Trigger.OldMap.get(wo.id).WorkOrderName__c )
            {           
                wOrdertoProcessFour.add(wo);        
            }
        }//Added for EUS#046939 for Dec 14 Release
          
      } 
      if(woStatusChangeidSet!= null && woStatusChangeidSet.size()>0){
                System.debug('*************************************** 1'+woStatusChangeidSet.size());
                 AP_AssignedToolsTechnicians.updateAssignedToolsTechniciansStatus(woStatusChangeidSet ,CANCELLED);
                 
      }
      if(wOrdertoProcess != null && wOrdertoProcess.size()>0){
           System.debug('*************************************** 2'+wOrdertoProcess.size());
            AP_AssignedToolsTechnicians.updateAssignedToolsTechniciansPrimaryUser(wOrdertoProcess);
      }
      if(wOrdertoProcessThree != null && wOrdertoProcessThree.size()>0){
            AP_AssignedToolsTechnicians.updateAssignedToolsTechniciansFieldUpdate(wOrdertoProcessThree,'BackOfficeReference__c','BackOfficeReference__c');
      }
      if(wOrdertoProcessFour != null && wOrdertoProcessFour.size()>0){
            AP_AssignedToolsTechnicians.updateAssignedToolsTechniciansFieldUpdate(wOrdertoProcessFour,'WorkOrderName__c','WorkOrderName__c');
      }
      if(wOrdertobeupdate!= null && wOrdertobeupdate.size()>0)
      {
          //update wOrdertobeupdate ;
      }
      // Added by Anand for Workorder validation
     if(Wovalidset!= null && Wovalidset.size()>0){
          Ap_WorkOrder.WOAutoValidation(Wovalidset); 
          //AP_WorkOrderAfterUpdate.WOAutoValidation(Wovalidset);
         System.debug('Yass: Ap_WorkOrder.WOAutoValidation(Wovalidset)' + Wovalidset);
      }
     
      //Added for Defect#Def-7260
      if(shareset!=null )//&& techid1!=null)
      {
          Ap_WorkOrder.PrimaryFSRAdding(shareset);
         
      }
      if(woset1!=null && woset1.size()>0)
      {
        Ap_WorkOrder.PrimaryFSRRemoving(woset1);
      }
      //End for Defect#Def-7260
     
      if(IPset!=null && IPset.size()>0 ){
            Ap_WorkOrder.IPCommissioning(IPset,workorderset);
      }
     if(partsset!=null && partsset.size()>0){
        Ap_WorkOrder.Partsordersharing(partsset);
      }
        
      
        
        /*
            Added by: Ramu Veligeti
            Date: 16/07/2013
            Updates Work Order Group's Status based on the related WO's status
        */
        //ServiceMaxWOG.UpdateWOG(trigger.new,trigger.oldMap);
        
        /*
            Added by: Ramu Veligeti
            Date: 17/07/2013
            Update TECH_IsSVMXRecordPresent__c on Account object if the Account is used in 
            ServiceMax related objects like Work Order, Installed Product, Location, Service Contract etc..
        */
        Set<Id> AccId = new Set<Id>();
        for(SVMXC__Service_Order__c a:trigger.new)
        {
            if(a.SVMXC__Company__c!=null && trigger.OldMap.get(a.Id).SVMXC__Company__c !=a.SVMXC__Company__c){
                AccId.add(a.SVMXC__Company__c);
            }
        }
        if(AccId != null)       
            SVMX_IsSVMXRelatedAccount.UpdateAccount(AccId);
        /*
            Added by: Deepak
            Date: 17/09/2013
            Update  Work Order Notification fields....
        */
        Set<Id> WorkId = new Set<Id>();
        Set<Id> WonId = new Set<Id>();
                //Id casId;// = new Set<Id>();
        Set<Id> casId =  new Set<Id>();// DEF-8733
        List<SVMXC__Service_Order__c> Wocaselist = new List<SVMXC__Service_Order__c>();
       for(SVMXC__Service_Order__c a : trigger.new)
       {
            WorkId.add(a.Id);
            if((trigger.oldMap.get(a.Id).SVMXC__Order_Status__c != trigger.newMap.get(a.Id).SVMXC__Order_Status__c
            || trigger.oldMap.get(a.Id).SVMXC__Scheduled_Date_Time__c != trigger.newMap.get(a.Id).SVMXC__Scheduled_Date_Time__c
            || trigger.oldMap.get(a.Id).Comments_to_Planner__c != trigger.newMap.get(a.Id).Comments_to_Planner__c)
            && trigger.newMap.get(a.Id).Parent_Work_Order__c == null && trigger.newMap.get(a.Id).Work_Order_Notification__c!= null)
            {
                wonId.add(a.Work_Order_Notification__c);
            }
             //Added for DEF-6715 by Deepak
            if(trigger.oldMap.get(a.Id).SVMXC__Order_Status__c != a.SVMXC__Order_Status__c &&
               a.SVMXC__Order_Status__c =='Closed' && a.SVMXC__Case__c !=null && a.Work_Order_Notification__c ==null)
            {
                Wocaselist.add(a);
               //casId=a.SVMXC__Case__c;
                casId.add(a.SVMXC__Case__c);
            }    
        }
        if(wonId!= null && wonId.size()>0 && WorkId != null && WorkId.size()>0)
        {
            AP_WorkOrderAfterUpdate.UpdateWONStatus(WonId,WorkId);
        }
        /*if(Wocaselist!= null && Wocaselist.size()>0 && casId!=null )
        {
            AP_WorkOrderAfterUpdate.UpdateCaseStatus(Wocaselist,casId);
        }*/
        
         // Dec Release 2013
         /*
         // Descoped from april 2014
       if(woListforAssetCategory!= null && woListforAssetCategory.size()>0)
       {
            Ap_WorkOrder.AssetCategory(woListforAssetCategory);
            
       }
        */
        // added for March release 2016 BR-8786
        system.debug('skill creation calss is called'+woListforAssetCategory.size());
         if(woListforAssetCategory != null && woListforAssetCategory.size()>0){
            system.debug('skill creation calss is called');
            SVMXC_WorkOrderAfter.syncProductExpertiseReqToWO(woListforAssetCategory);
         }
        
         
/***************************************************************** 
Modified By-Deepak
April-2014 Release(19/02/2014)
Purpose- Asset Categorisation.
******************************************************************/
      Set<Id> ipid = new Set<Id>();
      List<SVMXC__Installed_Product__c> iplist = New List<SVMXC__Installed_Product__c>();
      String IpStatus = system.label.CLAPR14SRV02;
      Set<Id> ConId = new Set<Id>();
      
       for (SVMXC__Service_Order__c wo : trigger.new)
        {  
            if(wo.SVMXC__Contact__c!=null && trigger.OldMap.get(wo.Id).SVMXC__Contact__c !=wo.SVMXC__Contact__c) 
            {
                ConId.add(wo.SVMXC__Contact__c);
            }
            if(trigger.OldMap.get(wo.Id).SVMXC__Component__c !=trigger.NewMap.get(wo.Id).SVMXC__Component__c)
            {
                {
                    if(trigger.NewMap.get(wo.Id).SVMXC__Component__c !=null)
                    {  
                        ipid.add(trigger.NewMap.get(wo.Id).SVMXC__Component__c);
                        system.debug('ipid2:'+ipid);
                    }
                    else if(trigger.NewMap.get(wo.Id).SVMXC__Component__c ==null)
                    {
                        
                        ipid.add(trigger.OldMap.get(wo.Id).SVMXC__Component__c);
                        system.debug('ipid1:'+ipid);
                    }
                    system.debug('ipid:'+ipid);
                }
            }
            
        }
        if(ipid !=null)
        iplist=[Select Id,Name,AssetCategory2__c,LifeCycleStatusOfTheInstalledProduct__c,SVMXC__Company__c,UnderContract__c,SVMXC__Company__r.ClassLevel1__c,DecomissioningDate__c From SVMXC__Installed_Product__c
                Where Id =:ipid and AssetCategory2__c !=:IpStatus];
        system.debug('iplist:'+iplist);        
        if(iplist.size()>0 && ipid.size()>0)
        Ap_Installed_Product.AssetCategory(iplist);
        update iplist;
        //BR-4404
        if(ConId !=null)
        AP_IsSVMXRelatedContact.UpdateContact(ConId);
        /***************************************************************** 
        Modified By-Hari
        April-2014 Release(25/03/2014)
        Purpose- Assign the Work Orde ownership to Service Center Queue when NEW Work Order is created or Work Order status = ‘Service Complete
        ******************************************************************/
        Set<Id> woidset = new Set<Id>();
        List<SVMXC__Service_Order__c> sorList = new List<SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order__c> toupdate = new List<SVMXC__Service_Order__c>();
        for(SVMXC__Service_Order__c a:trigger.new)
        {
            if(trigger.NewMap.get(a.id).SVMXC__Order_Status__c == 'Service Complete' && 
                    trigger.oldMap.get(a.id).SVMXC__Order_Status__c!=trigger.NewMap.get(a.id).SVMXC__Order_Status__c) //
            {
                //woidset.add(a.id);
                sorList.add(a);
            }  
        }
        
                
        List<SVMXC__Service_Group_Members__c> groupMembersList = New List <SVMXC__Service_Group_Members__c>();
        Map<id,SVMXC__Service_Group_Members__c> sgmap = new Map<id,SVMXC__Service_Group_Members__c>();
        
        if(woidset.size()>0)
        {
             groupMembersList= [select id,Name,Manager__c  From SVMXC__Service_Group_Members__c  where Id IN :woidset ];
             sgmap.putAll(groupMembersList);
        }
        /* commented by suhail (This part does nothing !) -- R4 2016 -START-
        for(SVMXC__Service_Order__c svo3:sorList)
        {
            
            if(svo3.ApprovalManager__c != null){
                //svo3.OwnerId=svo3.ApprovalManager__c;
                //toupdate.add(svo3);
            }
            else if(sgmap.containskey(svo3.SVMXC__Group_Member__c)){
                if(sgmap.get(svo3.SVMXC__Group_Member__c).Manager__c != null)
                {
                    //svo3.OwnerId=sgmap.get(svo3.SVMXC__Group_Member__c).Manager__c;
                    //toupdate.add(svo3);
                }
            }
            
        } commented by suhail (This part does nothing !) -- R4 2016 -end-*/
        if(toupdate != null && toupdate.size()>0){
            //update toupdate;
        }
        if(ippset1!=null && ippset1.size()>0){
        Ap_Installed_Product.IBchangeMaster(ippset1);
        }
        
         /***************************************************************** 
        Modified By-Hari
        Dec-2015 Release(17/1/2015)
        Purpose- WO Level On Accept/Reject Field value Changes update the Respective 
                 Assigned Tools Tech Record Status Value 
        ******************************************************************/
        if(woAcceptRejectList != null && woAcceptRejectList.size()>0){
            //String EventString = JSON.serialize(woAcceptRejectList);
            AP_WorkOrderAfterUpdate.UpdateAssignedToolsTechnicians(woAcceptRejectList);
            
        }
        if(caseidset.size()>0)
        {
        Ap_WorkOrder.CloseWocase(caseidset);
        }
        if(woshareset.size()>0)
        {
            Ap_WorkOrder.FSRSharingaccess(woshareset);
        }
        
       
        
    }   
   
     
}