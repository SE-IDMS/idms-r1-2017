trigger ReturnItemAfterDelete on RMA_Product__c (After Delete) 
{
if(Utils_SDF_Methodology.canTrigger('AP_RI_FieldUpdate'))
    {
 AP_RR_FieldUpdate.updateRequestTypeOnRRDelete(trigger.old);
 //Oct 15 Release -- BR-7721  -- Divya M
 AP_RR_FieldUpdate.updateShippedFromCustomer(trigger.old);
 }
}