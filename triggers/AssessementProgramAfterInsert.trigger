/****************************************************************************************************************************

    Author       : Sreenivas Sake
    Created Date : 23 July 2014
    Description  : 1. Add Assessment to ORF,Account Assigned Program.
***************************************************************************************************************************/
trigger AssessementProgramAfterInsert on AssessementProgram__c (after insert) {

    map<String,set<Id>> mapProgamLevel = new map<String,set<Id>>();
    
    for(AssessementProgram__c assessment : trigger.new)
    {
        if(assessment.AutomaticAssignment__c)
        {
            if(assessment.PartnerProgram__c != null && assessment.ProgramLevel__c != null)
            {
                if(!mapProgamLevel.containsKey(assessment.PartnerProgram__c))
                    mapProgamLevel.put(assessment.PartnerProgram__c+':'+assessment.ProgramLevel__c , new set<Id> {(assessment.Assessment__c)});
                else
                    mapProgamLevel.get(assessment.PartnerProgram__c+':'+assessment.ProgramLevel__c).add(assessment.Assessment__c);
            }
        }
    }
    if(!mapProgamLevel.isEmpty())
        AP_ASM_AddAssessment.addProgramAssessment(mapProgamLevel);
}