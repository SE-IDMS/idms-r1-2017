trigger OSACMeetingBeforeUpdate  on OSACMeeting__c (before update) 
{
//*********************************************************************************
// Trigger Name     : OSACMeetingBeforeUpdate  
// Purpose          : OSACMeeting  Before Update event trigger
// Created by       : Global Delivery Team-Mohit
// Date created     : 21th June 2012
// Modified by      :Hanamanth
// Date Modified    :
// Remarks          : For Sep - 12 Release
///********************************************************************************/

Map<String,String> mapOSACPrb = new Map<String,String>();
Set<Id> setPrbId = new Set<Id>();
if( Test.isRunningTest() || Utils_SDF_Methodology.canTrigger('AP_Osacmeeting'))
{
   //AP_OSACMeeting.PopulateProblemLeaderCSQ(trigger.new);
AP_OSACMeeting.getDateconversion(trigger.new);
   List<OSACMeeting__c> lstGSAC=new List<OSACMeeting__c>();
   for(OSACMeeting__c vargsac:trigger.new) {
   
    if(vargsac.Decision__c != Trigger.Oldmap.get(vargsac.Id).Decision__c && vargsac.Decision__c == 'Go') {                    
        setPrbId.add(Trigger.newmap.get(vargsac.Id).Problem__c); 
        mapOSACPrb.put(vargsac.Id, Trigger.newmap.get(vargsac.Id).Problem__c);        
    }
   
   vargsac.Tech_Booked__c='';
    vargsac.Tech_Decision__c='';
   If( (trigger.newmap.get(vargsac.id).problem__c !=trigger.Oldmap.get(vargsac.id).problem__c) ||(trigger.newmap.get(vargsac.id).Decision__c !=trigger.Oldmap.get(vargsac.id).Decision__c))
   { 
   if((trigger.newmap.get(vargsac.id).problem__c !=trigger.Oldmap.get(vargsac.id).problem__c) && trigger.newmap.get(vargsac.id).problem__c!=null)
   {
    vargsac.Tech_Booked__c='Booked';
   }
   if((trigger.newmap.get(vargsac.id).Decision__c !=trigger.Oldmap.get(vargsac.id).Decision__c))
   {
   vargsac.Tech_Decision__c=trigger.newmap.get(vargsac.id).Decision__c;
   
   }
   lstGSAC.add(vargsac);
   }
   }
  
   if(lstGSAC.size()>0){
   
    AP_OSACMeeting.getStakeholder(lstGSAC,setPrbId, mapOSACPrb);   
   }

}
}