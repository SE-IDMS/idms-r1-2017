trigger FieloPRM_MemberFeatureDetailAfterDelete on FieloPRM_MemberFeatureDetail__c (after delete) {
    FieloPRM_AP_MemberFeatureDetailTriggers.memberFeatureActivatesInactivates(Trigger.Old, null);
}