/********************************************************************************************************************
    Created By : Shruti Karn
    Description : For October 13 PRM Release:
                 1. To add Assessment to Opportunity Registration Form.
       
********************************************************************************************************************/
trigger OpportunityRegistrationFormAfterInsert on OpportunityRegistrationForm__c (After Insert) 
{
    map<String,set<Id>> mapORFProgram = new map<String,set<Id>>();
    for(OpportunityRegistrationForm__c newORF : trigger.new)
    {
        
        if(newORF.PartnerProgram__c != null && newORF.ProgramLevel__c != null && newORF.Status__c != Label.CLOCT13PRM16)
        {
            if(!mapORFProgram.containsKey(newORF.PartnerProgram__c+':'+newORF.ProgramLevel__c))
                mapORFProgram.put(newORF.PartnerProgram__c+':'+newORF.ProgramLevel__c , new set<id> {(newORf.Id)});
            else
                mapORFProgram.get(newORF.PartnerProgram__c+':'+newORF.ProgramLevel__c).add(newORf.Id);
        }
        
    }
    
    if(!mapORFProgram.isEmpty())
                AP_ORFNotification.addAssessment(mapORFProgram);
}