/**
* @author: Nabil ZEGHACHE (not sure who actually created the class in the first place)
* Date Created (YYYY-MM-DD): 2016-08-25
* Test class: 
* Description: This is the beforeInsert trigger of the Opportunity Notification object
* -----------------------------------------------------------------
*                     MODIFICATION HISTORY
* -----------------------------------------------------------------
* Modified By: Authors Name
* Modified Date: Date
* Description: Brief Description of Change + BR/Hotfix
* -----------------------------------------------------------------
*/
trigger OpportunityNotificationBeforeInsert  on OpportunityNotification__c (Before Insert) 
{
    System.debug('#### Starting OpportunityNotificationBeforeInsert trigger');
    //Trigger bypass
    if(Utils_SDF_Methodology.canTrigger('SRV_OppNotif_BI')){
        
        List<OpportunityNotification__c> oppList = new List<OpportunityNotification__c>();
        Set<Id> optNotId = New set<Id> (); // variable used to store the IDs of the Opportunity notifications 
        for (OpportunityNotification__c oppN : Trigger.new)    
        {
            if(oppN.OpportunityDetector__c == null)
            {  
                oppN.OpportunityDetector__c = UserInfo.getUserId();
                oppList.add(oppN);
            }
            
            if(oppN.Tech_CreatedFromIB__c == true || oppN.TechCreatedFromSC__c == true)
            {
                optNotId.add(oppN.Id);
            }            
            
            
            if(oppn.WorkOrder__c != null){
                SVMXC__Service_Order__c wo = [select SVMXC__Company__r.id, SVMXC__Company__r.Country__r.id, Service_Business_Unit__c from SVMXC__Service_Order__c where id =: oppN.WorkOrder__c];
                
                system.debug('YTI '+wo);
                
                if(wo != null){
                    if(oppn.AccountForConversion__c == null){
                        oppn.AccountForConversion__c = wo.SVMXC__Company__r.id;
                    }
                    
                    if(oppn.CountryDestination__c == null){
                        oppn.CountryDestination__c = wo.SVMXC__Company__r.Country__r.id;
                    }
                    
                    if(oppn.LeadingBusinessBU__c == null){
                        oppn.LeadingBusinessBU__c = wo.Service_Business_Unit__c;
                    }     
                }
            }
        }
        // added Sept 30, 2015 to set the Project Name on Insert
        OpportunityNotificationBeforeInsertClass.setProjectNameAndCloseDate(trigger.New, trigger.OldMap, trigger.isInsert, trigger.isUpdate);
        if(optNotId.size()>0)
        {
            AP_OpptyCreationAfterInsert.CreatingOpportunity(optNotId);    
        }
        
    }        
}