/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 12 November 2012
    Description : 
    For Devember 12 Release:
                 1. Calls AP_PackageTriggerUtils.updateContractTypeto update Support Contract Level on Contract based on the Package Level.
    Modified By: Pooja Suresh                 
    For Q3 2016 Release: BR-9333                 
                 2. Calls AP_PackageTriggerUtils.createProactiveCases to create proactive cases for active packages if conditions are met.
********************************************************************************************************************/
trigger PackageAfterUpdate on Package__c (after update) {

    list<Package__c> lstPKG = new list<PAckage__c>();
    
    for(Id pkgID : trigger.newMap.keySet())
    {
        if((trigger.newMap.get(pkgID).Support_Level__c != trigger.oldMap.get(pkgID).Support_Level__c) 
            ||(trigger.newMap.get(pkgID).enddate__c != trigger.oldMap.get(pkgID).enddate__c)
            ||(trigger.newMap.get(pkgID).startdate__c != trigger.oldMap.get(pkgID).startdate__c)
            ||(trigger.newMap.get(pkgID).status__c != trigger.oldMap.get(pkgID).status__c))
            lstPKG.add(trigger.newMap.get(pkgID));
    }
       
   // if(Utils_SDF_Methodology.canTrigger('AP_PackageTriggerUtils') || test.isRunningTest())
        if(!lstPKG.isEmpty())
            AP_PackageTriggerUtils.updateContractType(lstPKG);

        AP_PackageTriggerUtils.createProactiveCases(trigger.oldmap,trigger.newmap);       //Q3 2016
}