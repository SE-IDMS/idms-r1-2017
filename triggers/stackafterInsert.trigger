trigger stackafterInsert on Answertostack__c (after insert) {

    if(Utils_SDF_Methodology.canTrigger('AP_ComplaintsRequestsStack')) {
    
        AP_ComplaintsRequestsStack.updateComplaintsRequestStack(trigger.new);
    
    } 

}