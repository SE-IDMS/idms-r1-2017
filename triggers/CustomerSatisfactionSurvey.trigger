trigger CustomerSatisfactionSurvey on CustomerSatisfactionSurvey__c (after delete, after insert, after update, before delete, before insert, before update) {
    
    Ap_CustomerSatisfactionSurvey pp = new Ap_CustomerSatisfactionSurvey(Trigger.isBefore, Trigger.isAfter, 
                           Trigger.isUpdate, Trigger.isInsert, Trigger.isDelete,                         
                           Trigger.new, Trigger.oldMap, Trigger.newMap);
    pp.execute();


}