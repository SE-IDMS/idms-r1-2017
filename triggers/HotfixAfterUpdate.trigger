trigger HotfixAfterUpdate on Hotfix__c (after update) {
    if(Utils_SDF_Methodology.canTrigger('HotFix')){
        Integer approvedHotFixes=[SELECT count() FROM Hotfix__c WHERE Status__c = 'approved'];
        for (Hotfix__c temp:trigger.new)
        {
            if(temp.status__c=='approved')
            {
                AP_HotFixHandler.processHotFixForApproved();
            }

            if(temp.status__c=='migrated'&& approvedHotFixes==0)
            {
               AP_HotFixHandler.processHotFixForMigrated();
            }
        }

    }
}