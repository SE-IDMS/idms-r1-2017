/********************************************************************************************************************
    Created By : Vimal Karunakaran
    Description : For Oct 14  CCC Release:
                 1. Calls AP_Case_CaseHandler.updateLastActivityDate to update the Last Activity Date from CaseRelated List
********************************************************************************************************************/
trigger CaseCommentAfterInsert on CaseComment (after insert) {
    System.Debug('****** CaseCommentAfterInsert  Trigger Started ****');
    
    if(Utils_SDF_Methodology.canTrigger('AP_Case_CaseHandler') || Test.isRunningTest()){
        Set<Id> setCaseIds = new Set<Id>();
        for(CaseComment objCaseComment:Trigger.New){
            if(objCaseComment.ParentId!=null){
                setCaseIds.add(objCaseComment.ParentId);
            }
        }
        if(setCaseIds.size()>0){
            AP_Case_CaseHandler.updateLastActivityDate(setCaseIds);
        }
    }
    System.Debug('****** CaseCommentAfterInsert  Trigger Finished ****');
}