/*
    01-Apr-2013 Srinivas Nallapati  PRM Apr13   DEF-1462
*/
trigger OpportunityTeamMemberBeforeInsert on OpportunityTeamMember (before insert) {
    
    if(Utils_SDF_Methodology.canTrigger('AP_OpportunityTeamMember') && VFC20_CreateSeries.byPassSeries == false)
    {
        AP_OpportunityTeamMember.checkOpportunityPartnerInvolvement(trigger.new);
    }   
}