trigger CBATeamAfterDelete on CBATeam__c (After delete) {

    if(Utils_SDF_Methodology.canTrigger('AP_SharingAccessToCBATeam')|| Test.isRunningTest())
    {  
        System.Debug('**** CBATeamAfterDelete Trigger Start ****');
        
        AP_SharingAccessToCBATeam.deleteExistingCBATeamMember(trigger.oldmap);
        
        System.Debug('**** CBATeamAfterDelete Trigger End ****');
        
    }

}