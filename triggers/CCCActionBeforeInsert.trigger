//*********************************************************************************
// Trigger Name     : CCCActionBeforeInsert
// Purpose          : CCCAction Before Insert event trigger
// Created by       : Global Delivery Team
// Date created     : 15th June 2012
// Modified by      :
// Date Modified    :
// Remarks          : For Sep - 12 Release
///********************************************************************************/

trigger CCCActionBeforeInsert on CCCAction__c (before Insert) {
    System.Debug('****** CCCActionBeforeInsert  Trigger Start****');
    
    if(Utils_SDF_Methodology.canTrigger('AP_ChatterForCCCAction')){    
        AP_ChatterForCCCAction.updateCCCActionOwnerId(Trigger.new);
    }
    
    if(Utils_SDF_Methodology.canTrigger('AP_UpdateCCCActionAge')){
        AP_UpdateCCCActionAge.updateCCCActionAgeonInsert(Trigger.new);
    }
    
    System.Debug('****** CCCActionBeforeInsert  Trigger Finished****');
}