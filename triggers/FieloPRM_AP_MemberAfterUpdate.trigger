/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: 
*******************************************************************/

trigger FieloPRM_AP_MemberAfterUpdate on FieloEE__Member__c (After Update) {

    if(Utils_SDF_Methodology.canTrigger('FieloPRM_MemberTrigger')){
        if(!FieloPRM_MemberTrigger.isAlreadyRun){
            FieloPRM_MemberTrigger.isAlreadyRun = true;
            FieloPRM_MemberTrigger.createMemberHistory(trigger.newMap, trigger.oldMap);
            FieloPRM_MemberTrigger.isActiveAccount(trigger.new, trigger.oldMap);
            FieloPRM_MemberTrigger.hybridSegment(trigger.newMap,trigger.oldMap);
            FieloPRM_MemberTrigger.addMemberToTeam(trigger.new, trigger.oldMap);
            FieloPRM_MemberTrigger.isAlreadyRun = false;
        }
    }
    
}