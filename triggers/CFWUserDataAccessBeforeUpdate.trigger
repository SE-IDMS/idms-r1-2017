/* DESCRIPTION: UPDATES RISK ASSESSEMENT RECORD TYPE TO CLOCT15CFW03 WHEN USER DATA ACCESS RECORD IS SAVED*/
trigger CFWUserDataAccessBeforeUpdate on CFWUserDataAccess__c (before update) 
{
    List<CFWUserDataAccess__c> udas= new List<CFWUserDataAccess__c>();
    
    for(CFWUserDataAccess__c uda: trigger.new)
    {
        if(uda.CertificationRiskAssessment__c!=null)
        {
            udas.add(uda);
        }
    }
    if(udas.size()>0)
        AP_CFWUserDataAccessBeforeUpdateHandler.updateRiskAssessment(udas);    
}