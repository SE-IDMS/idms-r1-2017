@isTest(SeeAllData=false) 
global class VFC_LiveAgent_PrechatForm_New_TEST {
    
	static testMethod void testFindConfig() {
        /* Create form custom settings */
        CS_LiveAgent_PrechatFormSettings__c config = new CS_LiveAgent_PrechatFormSettings__c(
            Name = 'FR-test', //-test',
            isRoutingMenuVisible__c = false,
            IsCategoryVisible__c = true,
            CategoryValues__c = '0;1;2;3;4;5;6;7;8;9'
        );
        insert config;
        
        /* Create form assignment custom settings */
        String buttonId = '573A000000005De';
        CS_LiveAgent_PrechatFormAssignment__c assignment = new CS_LiveAgent_PrechatFormAssignment__c(
        	Name = buttonId,
            PrechatFormSettingsName__c = config.Name
        );
        insert assignment;
        
        String endpoint = 'https://d4m.la4-c1cs-was.salesforceliveagent.com/content/s/chat?';
        endpoint += 'language=en_US#';
        endpoint += 'deployment_id=5721200000004cF&';
        endpoint += 'org_id=00D7A0000000R1B&';
        endpoint += 'button_id=' + buttonId + '&';
        endpoint += 'session_id=a08b5006-577a-4adc-8fe8-a78c6f1cbdb6';
        
        PageReference vfPage = Page.VFP_LiveAgent_PrechatForm_New;
        vfPage.getHeaders().put(VFC_LiveAgent_PrechatForm_New.USER_AGENT_KEY, 'test');
        vfPage.getParameters().put('endpoint', endpoint);
        Test.setCurrentPage(vfPage);
        
        VFC_LiveAgent_PrechatForm_New con = new VFC_LiveAgent_PrechatForm_New();
        System.assertEquals(buttonId, con.settings.buttonId);
        System.assertEquals('la4-c1cs-was', con.settings.endpointId);
        System.assertEquals('5721200000004cF', con.settings.deploymentId);
        System.assertEquals(config.Name, con.settings.Form.Name);
        System.assertEquals(null, con.settings.RoutingRulesMap);
        System.assertEquals(10, con.settings.Categories.size());
    }
    
    static testMethod void testFindDefaultConfig1() {
        
        /* Create default form custom settings */
        CS_LiveAgent_PrechatFormSettings__c defaultConfig = new CS_LiveAgent_PrechatFormSettings__c(
            Name = 'Default'
        );
        insert defaultConfig;
        
        String buttonId = '573A000000005De';
        String endpoint = 'https://d4m.la4-c1cs-was.salesforceliveagent.com/content/s/chat?';
        endpoint += 'language=en_US#';
        endpoint += 'deployment_id=5721200000004cF&';
        endpoint += 'org_id=00D7A0000000R1B&';
        endpoint += 'button_id=' + buttonId + '&';
        endpoint += 'session_id=a08b5006-577a-4adc-8fe8-a78c6f1cbdb6';
        
        // Test 1: button_id set in the url but not assigned to any form
        PageReference vfPage = Page.VFP_LiveAgent_PrechatForm_New;
        vfPage.getHeaders().put(VFC_LiveAgent_PrechatForm_New.USER_AGENT_KEY, 'test');
        vfPage.getParameters().put('endpoint', endpoint);
        Test.setCurrentPage(vfPage);
        
        VFC_LiveAgent_PrechatForm_New con = new VFC_LiveAgent_PrechatForm_New();
        System.assertEquals(buttonId, con.settings.buttonId);
        System.assertEquals('Default', con.settings.Form.Name);
        
        // Test 2: no button_id set in the url
        vfPage = Page.VFP_LiveAgent_PrechatForm_New;
        vfPage.getHeaders().put(VFC_LiveAgent_PrechatForm_New.USER_AGENT_KEY, 'test');
        vfPage.getParameters().put('endpoint', 'https://www.google.com?language=en_US#');
        Test.setCurrentPage(vfPage);
        
        con = new VFC_LiveAgent_PrechatForm_New();
        //System.assertEquals('EN', con.languageCode);
        System.assertEquals('Default', con.settings.Form.Name);
        System.assertEquals(0, con.settings.Categories.size());
    }
    
    static testMethod void testConfigWithRoutingRules() {
        /* Create form custom settings */
        CS_LiveAgent_PrechatFormSettings__c config = new CS_LiveAgent_PrechatFormSettings__c(
            Name = 'FR-test',
            isRoutingMenuVisible__c = true
        );
        insert config;
        
        /* Create form assignment custom settings */
        String buttonId = 'ABC';
        CS_LiveAgent_PrechatFormAssignment__c assignment = new CS_LiveAgent_PrechatFormAssignment__c(
        	Name = buttonId,
            PrechatFormSettingsName__c = config.Name
        );
        insert assignment; 
        
        /* Create some routing rules */
        List<CS_LiveAgent_FormRoutingRule__c> listRoutings = new List<CS_LiveAgent_FormRoutingRule__c>();
        for (Integer i=0 ; i<100 ; i++) {
            if (i<10 || i>=90) {
                CS_LiveAgent_FormRoutingRule__c routing = new CS_LiveAgent_FormRoutingRule__c(
                    Name = 'FR-test-'+i,
                    RoutingLabel__c = 'Product'+i, 
                    ChatButtonID__c = 'Testbutton'
                );
                listRoutings.add(routing);
            } else {                
                CS_LiveAgent_FormRoutingRule__c routing = new CS_LiveAgent_FormRoutingRule__c(
                    Name = 'FR-test-'+i,
                    RoutingLabel__c = 'AProduct'+i, 
                    ChatButtonID__c = 'Testbutton',
                    Group__c = 'Group 1',
                    IsOrderNumberVisible__c = true,
                    AreProductFieldsVisible__c = true
                );
                listRoutings.add(routing);
            }
        }
        insert listRoutings;
        
        PageReference vfPage = Page.VFP_LiveAgent_PrechatForm_New;
        vfPage.getHeaders().put(VFC_LiveAgent_PrechatForm_New.USER_AGENT_KEY, 'test');
        vfPage.getParameters().put('endpoint', 'https://www.google.com?button_id=' + buttonId + '&');
        Test.setCurrentPage(vfPage);
        
        VFC_LiveAgent_PrechatForm_New con = new VFC_LiveAgent_PrechatForm_New();
        System.assertEquals(buttonId, con.settings.buttonId);
        System.assertEquals(config.Name, con.settings.Form.Name);
        
        Map<String, CS_LiveAgent_FormRoutingRule__c> routingRules = con.settings.RoutingRulesMap;
        System.assertEquals(100, routingRules.size());
    }
    
    static testMethod void testUnsupportedBrowser() {
        
        PageReference vfPage = Page.VFP_LiveAgent_PrechatForm_New;
        vfPage.getHeaders().put(VFC_LiveAgent_PrechatForm_New.USER_AGENT_KEY, System.Label.CLSEP16CCCLA19.split(',')[0].trim());
        vfPage.getParameters().put('endpoint', 'https://www.google.com');
        Test.setCurrentPage(vfPage);
        
        VFC_LiveAgent_PrechatForm_New con = new VFC_LiveAgent_PrechatForm_New();
        System.assert(!con.IsValidBrowser);
    }
    
    static testMethod void testIsContactOnlyExisting() {
        System.assert(!VFC_LiveAgent_PrechatForm_New.isContactOnlyExisting('helloworld@helloworld.hw'));
    }
}