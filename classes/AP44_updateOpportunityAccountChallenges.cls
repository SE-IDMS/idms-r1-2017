public class AP44_updateOpportunityAccountChallenges{
    
    public static Map<String,Integer> stageMap=null;
    public static void populateMap()
    {
        if(stageMap==null)
        {
            stageMap=new Map<String,Integer>();           
            for(Schema.PicklistEntry ple:Opportunity.StageName.getDescribe().getPicklistValues())
           stageMap.put(ple.getValue(),Integer.valueOf(ple.getValue().subString(0,1)));                              
        }                
    }
    
    public Static void updateOpportunityAccountChallenges (List<Opportunity> opplist)
    {
        Map<id,Opportunity> oppidOppRecMap=new Map<id,Opportunity>();
        List<OpportunityAccountChallenge__c> oppAccChallgList=new List<OpportunityAccountChallenge__c>();
        populateMap();
        
        if(opplist != null && opplist.size()>0){
            oppidOppRecMap.putAll(opplist);
            
            oppAccChallgList=[Select id, ForecastedRevenue__c,ActualRevenue__c,OpptyContributiontoAcctChallenge__c,AccountChallenge__c ,Opportunity__c  from OpportunityAccountChallenge__c where Opportunity__c in:oppidOppRecMap.keySet() ];
            if(oppAccChallgList!=null && oppAccChallgList.size()>0){
                for(OpportunityAccountChallenge__c  oppAccChall:oppAccChallgList){
                    if(oppidOppRecMap.containsKey(oppAccChall.Opportunity__c )){
                        Opportunity opp=oppidOppRecMap.get(oppAccChall.Opportunity__c );
                        if(stageMap.containsKey(opp.StageName) && stageMap.get(opp.StageName)>=1 && stageMap.get(opp.StageName)<=6){
                            System.debug('*****'+opp.StageName);
                            if(opp.Amount != null && oppAccChall.OpptyContributiontoAcctChallenge__c != null && opp.Probability != null ){
                                oppAccChall.ForecastedRevenue__c = opp.Amount * oppAccChall.OpptyContributiontoAcctChallenge__c * opp.Probability ;
                                oppAccChall.ActualRevenue__c = 0;
                            }
                        }
                        else if(stageMap.containsKey(opp.StageName) && (stageMap.get(opp.StageName)==0 || stageMap.get(opp.StageName)==7))
                        {
                            System.debug('*****'+opp.StageName);
                            if(opp.Amount != null && oppAccChall.OpptyContributiontoAcctChallenge__c != null && opp.Probability != null ){
                               oppAccChall.ActualRevenue__c = opp.Amount * oppAccChall.OpptyContributiontoAcctChallenge__c * opp.Probability ;
                                oppAccChall.ForecastedRevenue__c = 0; 
                            }
                        }
                    }
                }
                update oppAccChallgList;
            }
        }
    } 
}