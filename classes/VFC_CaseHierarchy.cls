global with sharing class VFC_CaseHierarchy {

   global boolean toHighlight{get;set;}
     
    global class cNode{
        public Case cNodeCase{get;set;}
        public Id parentCNodeCaseId{get;set;}
        public Id cNodeCaseId{get;set;}
        public String accName{get;set;}
        public Id accId{get;set;}
        public String conName{get;set;}
        public Id conId{get;set;}
        public string ownerName{get;set;}
        public id ownerId{get;set;}
        public id mainCaseNodeId{get;set;}
        public boolean highLightNode{get;set;}
        
        global cNode(Case cNodeCase,Id parentCNodeCaseId,Id cNodeCaseId,String accName, Id accId,String conName,Id conId,string ownerName,Id ownerId,Id mainCaseNodeId){
            this.cNodeCase= cNodeCase;
            this.parentCNodeCaseId= parentCNodeCaseId;
            this.cNodeCaseId= cNodeCaseId;
        
            this.accName = accName;
            this.accId = accId;
            this.conName = conName;
            this.conId = conId;
            this.ownerName = ownerName;
            this.ownerId = ownerId;
            this.mainCaseNodeId = mainCaseNodeId;
            if(cNodeCaseId ==mainCaseNodeId){
                this.highLightNode = true;
            }
            else{
                this.highLightNode = false;
            }
           
        }
    }
    global static List<cNode> cNodeList{get;set;}
   
    global VFC_CaseHierarchy(){}
       
          
     
   
    @RemoteAction
    global static List<cNode> getCaseHierarchyNodes(string caseId){
         String mainCaseId =caseId;
         String mainCaseFifteenDigitId ; 
         String caseObjId = caseId;
         cNodeList = new List<cNode>();
         
         Boolean top= false;
         Integer topLevel =0;
         Case caseInstance;
         //Getting topmost parent in hierarchy
         while(!top){
             caseInstance =[select id,subject,RelatedSupportCase__c,CaseNumber,Status,Owner.Name,Owner.Id,Account.name,Account.Id,Contact.Name,Contact.Id,(select id,subject from Cases) from case where id=:caseObjId limit 1];  
             if(caseInstance.id == mainCaseId  ){
                 mainCaseFifteenDigitId = caseInstance.id; 
             }
             if(caseInstance.RelatedSupportCase__c!=null){
                 caseObjId = caseInstance.RelatedSupportCase__c;        
             } 
             else{
                 top = true;
             }
             topLevel++;  
         }
         //Getting whole hierarchy starting from top parent
         
         Set<Id> toQueryParentIds = new Set<Id>();
         Set<Id> alreadyQueriedParentIds = new Set<Id>();
         Set<Id> addedOnceIds = new Set<Id>();
         Set<Id> tempCaseIdsDump = new Set<Id>();
         
         toQueryParentIds.add(caseObjId);
         cNodeList.add(new cNode(caseInstance,caseInstance.RelatedSupportCase__c,caseInstance.Id,caseInstance.Account.name,caseInstance.Account.Id,caseInstance.Contact.Name,caseInstance.Contact.Id,caseInstance.Owner.Name,caseInstance.Owner.Id,mainCaseFifteenDigitId ));
         
         while(!toQueryParentIds.isEmpty()){
             
             for(Case caseRec:[select id,RelatedSupportCase__c,CaseNumber,Subject,Status,Owner.Name,Owner.Id,Account.name,Account.Id,Contact.Name,Contact.Id from case where RelatedSupportCase__c in:toQueryParentIds]){
                  if(mainCaseId == caseRec.id){
                      mainCaseFifteenDigitId=caseRec.id; 
                  }
                  cNodeList.add(new cNode(caseRec,caseRec.RelatedSupportCase__c,caseRec.Id,caseRec.Account.name,caseRec.Account.Id,caseRec.Contact.Name,caseRec.Contact.Id,caseRec.Owner.Name,caseRec.Owner.Id,mainCaseFifteenDigitId)); 
                  tempCaseIdsDump.add(caseRec.Id);   
             }
             toQueryParentIds.clear();
             toQueryParentIds.addAll(tempCaseIdsDump); 
             tempCaseIdsDump.clear();  
                
         }
         
         
        system.debug('--------------cNodeList size---------------'+cNodeList.size());
        system.debug('--------------cNodeList ---------------'+cNodeList);
         
        return cNodeList;
    }
}