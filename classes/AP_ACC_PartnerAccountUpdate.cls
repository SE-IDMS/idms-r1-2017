/*********************************************************************************************
1-Mar-13    Shruti Karn            PRM Mar13 Release    BR-2782    Initial Creation
5-Mar-13    Srinivas Nallapati     PRM Mar13 Release    BR-2782    Updated
*********************************************************************************************/
public class AP_ACC_PartnerAccountUpdate
{
    public static Boolean isFirstExecution = true;
    
    @future
    public static void UpdatePartnerAccount(set<ID> setAccountId)
    {
        system.debug('AP_ACC_PartnerAccountUpdate:UpdatePartnerAccount() start');
        List<AccountAssessment__c> acasmts = new List<AccountAssessment__c>([SELECT Account__c, PartnerProgram__c, ProgramLevel__c FROM AccountAssessment__c WHERE Account__c = :setAccountId]);
        isFirstExecution = false;
        Map<Id,List<AccountAssessment__c>> mapasmt = new Map<Id,List<AccountAssessment__c>>();
        List<ACC_PartnerProgram__c> assessment = new List<ACC_PartnerProgram__c>();
        List<Id> delAssessment = new List<Id>();
        Map<String,AccountAssessment__c> mapid = new Map<String,AccountAssessment__c>();
        List<AccountAssessment__c> delLst = new List<AccountAssessment__c>();
        List<AccountAssessment__c> delLst1 = new List<AccountAssessment__c>();
        
        for(AccountAssessment__c am : acasmts) {
            mapasmt.put(am.Account__c,new List<AccountAssessment__c>{(am)});
            string key = am.Account__c + '-' + am.ProgramLevel__c ;
            mapid.put(key,am);             
        }
        
        if(setAccountId != null)
        {
            list<Account> lstAccount = new list<Account>();
            lstAccount = [Select PRMAccount__c , isPartner from Account where id in: setAccountId limit 10000];
            if(!lstAccount.isEmpty())
            {
               System.Debug('lstAccount:'+lstAccount);
                for(Account acc: lstAccount) {
                    System.Debug('Inside for:');
                    acc.isPartner = acc.PRMAccount__c;
                    System.Debug('Account:'+lstAccount);
                    if(!mapasmt.isEmpty())
                    for(AccountAssessment__c asmt : mapasmt.get(acc.Id)) {
                        ACC_PartnerProgram__c accprgm = new ACC_PartnerProgram__c(Account__c = acc.Id, PartnerProgram__c = asmt.PartnerProgram__c, ProgramLevel__c = asmt.ProgramLevel__c);
                        delLst.add(asmt);
                        assessment.add(accprgm);
                        
                    }
                
                }
                System.Debug('assessment:'+assessment);
                try
                {
                    update lstAccount;
                    System.Debug('Account:'+lstAccount);
                }
                
                catch(Exception e)
                {
                    system.debug('Error while updating Account'+e.getMessage());
                }
                if(!assessment.isEmpty()) {
                    Database.SaveResult[] accPrgmInsert = Database.insert(assessment, false);
                    System.Debug('accPrgmInsert:'+accPrgmInsert);
                    integer i = 0;
                    for(Database.SaveResult sr: accPrgmInsert)
                    {
                        
                        if(sr.isSuccess()) {
                           // delAssessment.add(sr.getId());
                            System.Debug('sr.getid()'+sr.getId());
                            delLst1.add(delLst[i]);
                            System.Debug('mapid.get(sr.getId())'+mapid.get(sr.getId()));
                        }
                        i = i + 1;  
                    }
                }
            }
        }
       /* for(Id id : delAssessment) {
            if(mapid.containskey(id))
                delLst.add(mapid.get(id));
        }*/
        if(!delLst1.isEmpty())
            delete delLst1;
        system.debug('AP_ACC_PartnerAccountUpdate:UpdatePartnerAccount() Ends');
    }
}