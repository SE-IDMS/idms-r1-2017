/*****
Last Modified By:Hanamanth asper Q1 release 
Comments 
******/

@isTest
private class VFC_WithdrawalReferenceMassInsert_TEST{
    static testMethod void unitTest1(){
        ApexPages.StandardSetController controller1;
        //list<User> userList = AP_EllaProjectForecast_TEST.createUsers();
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA12');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        //system.runas(userList[0]){
            
          CS_WithdrawalSubstitutionCSVMap__c customSetRecs = new CS_WithdrawalSubstitutionCSVMap__c(Name = 'WR01', ColumnNumber__c = '0', HeaderName__c = 'Old Commercial Reference', Object__c = 'WithdrawalReference__c');
          insert customSetRecs;
            
          Id rt3 = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Withdrawal - Product').getRecordTypeId();
            Offer_Lifecycle__c inOffLif3= new Offer_Lifecycle__c(); 
                inOffLif3.RecordTypeId=rt3;
                inOffLif3.Offer_Name__c='Test offer Schneider3';
                inOffLif3.End_of_Services_Date__c = date.today();
                inOffLif3.Withdrawal_Notice_Status__c='Draft';
                inOffLif3.Withdrawal_Owner__c = newUser.id;
                inOffLif3.End_of_Commercialization_Date__c = date.today();
            Insert inOffLif3;             
            PageReference pg = Page.VFP_WithdrawalReferenceMassInsert;
            Test.setCurrentPage(pg);
            pg.getParameters().put('Id', inOffLif3.Id);
            // define controller --panggil yg public class, page reference
            VFC_WithdrawalReferenceMassInsert controller = new VFC_WithdrawalReferenceMassInsert(controller1);
            Product2 prod = new Product2();
            String commercialReference = 'Test'; 
            String strMaterialDescription = 'abc123';
            String businessUnit = 'INDUSTRIAL AUTOMATION';
            String productLine = 'IDMOT - Motion Control';
            String productFamily = 'OLD';
            String family = 'GDP UTILISES DANS GM';
            boolean isMarketingDocumentation = true;
            boolean isOld = true;
            boolean isSparePart = false;
            prod = Utils_TestMethods.createCommercialReference(commercialReference,strMaterialDescription, businessUnit, productLine, productFamily, family,isMarketingDocumentation, isOld, isSparePart);
            insert prod;
            Blob bodyBlob =Blob.valueOf('Old Commercial Reference\r\n'+prod.Id+'\r\n "test"\r\n "test\r\n Test"');
            //Blob bodyBlob=Blob.valueOf(blobValue);
            controller.ContentFile= bodyBlob;
            Test.startTest();
            controller.displayCSVEntries();
            Test.stopTest();
            controller.displayWithdrReferenceAfterValidation();
            controller.insertWithdrReference();
            controller.docancel();
            
        //} 
        
    }
    static testmethod void otherMethods(){
        
        ApexPages.StandardSetController controller1;
        //list<User> userList = AP_EllaProjectForecast_TEST.createUsers();
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA12');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        //system.runas(userList[0]){
            Test.startTest();
            CS_WithdrawalSubstitutionCSVMap__c customSetRecs = new CS_WithdrawalSubstitutionCSVMap__c(Name = 'WR01', ColumnNumber__c = '0', HeaderName__c = 'Old Commercial Reference', Object__c = 'WithdrawalReference__c');
          insert customSetRecs;
            Id rt3 = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Withdrawal - Product').getRecordTypeId();
            Offer_Lifecycle__c inOffLif3= new Offer_Lifecycle__c(); 
                inOffLif3.RecordTypeId=rt3;
                inOffLif3.Offer_Name__c='Test offer Schneider3';
                inOffLif3.End_of_Services_Date__c = date.today();
                inOffLif3.Withdrawal_Notice_Status__c='Draft';
                inOffLif3.Withdrawal_Owner__c = newUser.id;
                inOffLif3.End_of_Commercialization_Date__c = date.today();
            Insert inOffLif3;      
            
            PageReference pg = Page.VFP_SubstitutionInsert;
            Test.setCurrentPage(pg);
            pg.getParameters().put('Id', inOffLif3.Id);
            // define controller --panggil yg public class, page reference
            VFC_WithdrawalReferenceMassInsert controller = new VFC_WithdrawalReferenceMassInsert(controller1);
            String contents = 'Test, Test/r/n, "Test"'; 
            Boolean skipHeaders = true;
            //controller.parseCSV(contents, skipHeaders);
                        
            Integer RowNumber = 1;
            boolean Dup = false;
            boolean isSelected = true;
            string OldCommRef = 'abc';
            string Image = 'bcd';
            boolean vCommRef = false;
            boolean Error = false;
            boolean noCommRef = false; 
            
            VFC_WithdrawalReferenceMassInsert.WrapperWithdrawalReference wrap = new VFC_WithdrawalReferenceMassInsert.WrapperWithdrawalReference(RowNumber,Dup,isSelected,OldCommRef,Image,vCommRef,Error,noCommRef);
            VFC_WithdrawalReferenceMassInsert.WrapperWithdrawalReference wrap1 = new VFC_WithdrawalReferenceMassInsert.WrapperWithdrawalReference(RowNumber,Dup,isSelected,OldCommRef,Image,vCommRef,Error,noCommRef);
            controller.lstWrapperRIs_AfterValidate = new list<VFC_WithdrawalReferenceMassInsert.WrapperWithdrawalReference>();
            controller.lstWrapperRIs_AfterValidate.add(wrap);
            controller.lstWrapperRIs_AfterValidate.add(wrap1);
            controller.insertWithdrReference();
            //controller.lstWrapperRIs_AfterValidate[0].OldCommercialReference = '';
            controller.lstWrapperRIs = controller.lstWrapperRIs_AfterValidate;
            controller.displayWithdrReferenceAfterValidation();
            controller.sortWrapperItems(controller.lstWrapperRIs_AfterValidate);
            
            WithdrawalReference__c withDrawalRef = new WithdrawalReference__c(
                Offer__c = inOffLif3.Id,
                OldCommercialReference__c = 'productId'
            );
            list<WithdrawalReference__c> withDrawalRefList = new list<WithdrawalReference__c>();
            withDrawalRefList.add(withDrawalRef);
            controller.existingReference(withDrawalRefList);
            
            
            String commRef = 'Test';
            controller.keySet = new set<String>();
            controller.keySet.add(commRef);
            controller.checkDuplicate(commRef);
            
            String qty = '12';
            //controller.removeSpclChar(qty);
            
            String inputVal = 'ac\n""';
            //controller.removeSpclChar(inputVal);
            
            List<String> csvHeaderRow = new list<String>();
            csvHeaderRow.add('hello');
            csvHeaderRow.add('201');
            csvHeaderRow.add('Success');
            controller.validateHeaders(csvHeaderRow);
            /*
            controller.displayCSVEntries();
            controller.displayWithdrReferenceAfterValidation();
            controller.insertWithdrReference();
            controller.docancel();
            */
            //VFC_SubstitutionInsert.WrapperSubstitution wrap = new VFC_SubstitutionInsert.WrapperSubstitution();
            Test.stopTest();
        //}
        
    }

}