public with sharing class PRM_ExternalPropertiesCatalog {
    public static Boolean checkBFOPropertiesFieldName(List<ExternalPropertiesCatalog__c> newList) {
        Boolean checkOK=true;
        for(ExternalPropertiesCatalog__c epc: newList) {
          //string recordtypename = Schema.SObjectType.ExternalPropertiesCatalog__c.getRecordTypeInfosById().get(epc.recordtypeid).getname();
            
            //if (System.Label.CLJUN16PRM057 == '012120000019LAMAA2' && epc.TableName__c != null) { 
            if (System.Label.CLJUN16PRM057 == epc.RecordTypeId && epc.TableName__c != null) {
                SObjectType epcDesc = Schema.getGlobalDescribe().get(epc.TableName__c);
                System.debug('epcDesc : ' + epcDesc);
                Map<String,Schema.SObjectField> epcFields = epcDesc.getDescribe().fields.getMap();
                System.debug('epcFields.keySet() : ' + epcFields.keySet());
                Schema.sObjectField field = epcFields.get(epc.FieldName__c);
                System.debug('field : ' + field);
                if (field == null || (field != null && !(field.getDescribe().getType() == Schema.DisplayType.Email ||
                                                          field.getDescribe().getType() == Schema.DisplayType.String ||
                                                          field.getDescribe().getType() == Schema.DisplayType.URL ||
                                                          field.getDescribe().getType() == Schema.DisplayType.Phone ||
                                                          field.getDescribe().getType() == Schema.DisplayType.ID ||
                                                          field.getDescribe().getType() == Schema.DisplayType.Reference ||
                                                          (field.getDescribe().getType() == Schema.DisplayType.TextArea && field.getDescribe().getLength()<256) ||
                                                          field.getDescribe().getType() == Schema.DisplayType.Picklist))) {
                    String error = field==null?'':(field.getDescribe().getType() == Schema.DisplayType.TextArea?': Text Area field with length>255 is not supported in PAC':' : The field type "'+field.getDescribe().getType()+'" is not supported by PAC');
                    epc.addError(System.Label.CLJUN16PRM01+error);
                    checkOK=false;
                }
            }
        }
        return checkOK;
    }
}