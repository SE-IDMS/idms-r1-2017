@isTest 
private class IPOStrategicMilestoneTriggers_Test{
    static testMethod void TestMethod1() {
    
    //+Ve case
   
         User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPO9');
        System.runAs(runAsUser){
             IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
        
       
         IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp;
            IPO_Strategic_Milestone__c IPOmile1=new IPO_Strategic_Milestone__c(IPO_Strategic_Program_Name__c=IPOp.id,IPO_Strategic_Milestone_Name__c='mile1',Global_Functions__c='GM',Global_Business__c='ITB',Operation_Regions__c='NA',Status_in_Quarter_Q1__c='test',Roadblocks_Q1__c='test',Progress_Summary_Q4__c=null,Progress_Summary_Q3__c=null,Progress_Summary_Q2__c=null,Progress_Summary_Q1__c='In Progress',Decision_Review_Outcomes_Q1__c='test');
            insert IPOmile1;  
             IPO_Strategic_Milestone__c IPOmile2=new IPO_Strategic_Milestone__c(IPO_Strategic_Program_Name__c=IPOp.id,IPO_Strategic_Milestone_Name__c='mile1',Global_Functions__c='GM',Global_Business__c='ITB',Operation_Regions__c='NA',Status_in_Quarter_Q2__c='test',Roadblocks_Q2__c='test',Progress_Summary_Q4__c=null,Progress_Summary_Q2__c='In Progress',Progress_Summary_Q3__c=null, Progress_Summary_Q1__c=null,Decision_Review_Outcomes_Q2__c='test');
            insert IPOmile2; 
             IPO_Strategic_Milestone__c IPOmile3=new IPO_Strategic_Milestone__c(IPO_Strategic_Program_Name__c=IPOp.id,IPO_Strategic_Milestone_Name__c='mile1',Global_Functions__c='GM',Global_Business__c='ITB',Operation_Regions__c='NA',Status_in_Quarter_Q3__c='test',Roadblocks_Q3__c='test',Progress_Summary_Q4__c=null,Progress_Summary_Q3__c='In Progress',Decision_Review_Outcomes_Q3__c='test');
            insert IPOmile3; 
            IPO_Strategic_Cascading_Milestone__c IPOcas1= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOmile1.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Global Funtions',Entity__c='GM');
            insert IPOcas1;
            IPO_Strategic_Cascading_Milestone__c IPOcas2= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOmile2.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Global Business',Entity__c='ITB');
            insert IPOcas2;
            IPO_Strategic_Cascading_Milestone__c IPOcas3= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOmile3.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Operation Regions',Entity__c='NA');
             insert IPOcas3;
            
          IPO_Strategic_Program__c IPOp1=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='S&I;HR',Global_Business__c='ITB');
            insert IPOp1;
        IPO_Strategic_Milestone__c IPOmile4=new IPO_Strategic_Milestone__c(IPO_Strategic_Program_Name__c=IPOp.id,IPO_Strategic_Milestone_Name__c='mile1',Global_Functions__c='GM',Global_Business__c='ITB',Operation_Regions__c='NA',Status_in_Quarter_Q4__c='test',Roadblocks_Q4__c='test',Progress_Summary_Q4__c='In Progress',Decision_Review_Outcomes_Q4__c='test');
           
           Test.startTest(); 
           IPOmile1.Global_Functions__c=null;
           IPOmile2.Global_Business__c='power';
           IPOmile3.Operation_Regions__c='APAC';
           IPOmile1.Status_in_Quarter_Q1__c = 'test1';
           IPOmile1.Roadblocks_Q1__c='test1';
           IPOmile1.Decision_Review_Outcomes_Q1__c='test1';
           IPOmile1.Progress_Summary_Q1__c='Completed';
           IPOmile1.Progress_Summary_Q2__c='Completed';
           IPOmile1.Progress_Summary_Q3__c='None';
           IPOmile1.Progress_Summary_Q4__c='None';
           
                  
           IPOmile2.Status_in_Quarter_Q2__c = 'test1';
           IPOmile2.Roadblocks_Q2__c='test1';
           IPOmile2.Decision_Review_Outcomes_Q2__c='test1';
           IPOmile2.Progress_Summary_Q2__c='Completed';
           IPOmile2.Progress_Summary_Q1__c='None';
           IPOmile2.Progress_Summary_Q3__c='None';
           IPOmile2.Progress_Summary_Q4__c='None';
           
           IPOmile3.Status_in_Quarter_Q3__c = 'test1';
           IPOmile3.Roadblocks_Q3__c='test1';
           IPOmile3.Decision_Review_Outcomes_Q3__c='test1';
           IPOmile3.Progress_Summary_Q3__c='Completed';
           IPOmile3.Progress_Summary_Q2__c='None';
           IPOmile3.Progress_Summary_Q1__c='None';
           IPOmile3.Progress_Summary_Q4__c='None';
           
           
           
           try{
           update IPOmile1;
           update IPOmile2;
           update IPOmile3;
           
           }catch(DmlException d)
            {
              d.getMessage();
            }
           
           try{
           insert IPOmile4;
           }catch(DmlException h)
            {
              h.getMessage();
            }
            
       
       Test.stopTest(); 
    }
  }
}