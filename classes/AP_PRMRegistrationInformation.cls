global class AP_PRMRegistrationInformation{
    global String email { get; set; }
    global String firstName { get; set; }
    global String lastName {get; set; }
    global String phoneNumber { get; set; }
    global String phoneType { get; set; }
    global String jobFunction { get; set; }
    global String jobTitle { get; set; }
    global String businessType { get; set; }
    global String areaOfFocus { get; set; }
    global String userLanguage { get; set; }    
    global string federationId { get; set; } 
    global string bFOLanguageLocaleKey;
    
    global String countryCode { get; set;}
    global String stateProvinceCode { get; set;}
    
    global String companyName { get; set; }
    global String companyFederatedId { get; set; }
    global String companyPhone { get; set; }
    global String companyAddress1 { get; set; }
    global String companyAddress2 { get; set; }
    global String companyCity { get; set; } 
    global String companyZipcode { get; set; }
    global boolean companyHeaderQuarters { get; set; }
    global String companyWebsite { get; set; }
    global boolean includeCompanyInfoInPublicSearch { get; set; }
    global boolean IAmSelfEmployed { get; set; }
    global String companySpeciality { get; set; }
    global string employeeSize { get; set; }
    global string annualSales { get; set; }
    
    global AP_PRMRegistrationInformation(){
        includeCompanyInfoInPublicSearch = true;
        IAmSelfEmployed= false;
        companyHeaderQuarters = false;
        phoneType = 'PhoneType';
        businessType = '';
        areaOfFocus = '';
        jobTitle = '';
        jobFunction = '';
        bFOLanguageLocaleKey = '';
    }
}