public class AP_Cassini_SSO_UserHandler implements Auth.SamlJitHandler {

    /*private Set<String> getApplications(String assertion){
        Set<String> ls1 = new Set<String>();
        String regExp = '<saml:AttributeValue xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"xs:string\">cn=([\\sa-zA-Z0-9 _-]+),ou=Application,ou=AccessControl,ou=Group,dc=schneider,dc=com</saml:AttributeValue>';
        Pattern pattern1 = Pattern.compile(regExp);
        Matcher matcher = pattern1.matcher(assertion);
        while(matcher.find()){
           ls1.add(matcher.group(1));
        } 
        system.debug('**return Value**'+ls1);       
        return ls1;
    }*/
    public User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
            
           return null;
    }
    public void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        
            System.Debug('*** start updateUser : federationIdentifier=' + federationIdentifier); 
            User currentUser = [SELECT id, PRMTemporarySamlToken__c,PRMRegistrationCompleted__c, ContactId FROM User WHERE id = :userId LIMIT 1 ];
            System.debug('**Current User**'+currentUser);
            String oldAssertion = currentUser.PRMTemporarySamlToken__c;       
            String newAssertion = assertion;
            String authenticationToken = assertion;
            System.debug('*OldAssertion : newAssertion '+oldAssertion +':'+newAssertion);
            currentUser.PRMTemporarySamlToken__c = assertion;
            system.debug('assertion'+assertion);
            system.debug('currentUser.PRMTemporarySamlToken__c'+currentUser.PRMTemporarySamlToken__c);
            
            UPDATE currentUser;
    }   
    

}