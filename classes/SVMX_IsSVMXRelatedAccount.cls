/*
@Author: Ramu Veligeti
Date: 17/07/2013
Description: To update TECH_IsSVMXRecordPresent__c on Account object if the Account is used in 
             ServiceMax related objects like Work Order, Installed Product, Location, Service Contract etc..
*/
public class SVMX_IsSVMXRelatedAccount
{
    public static void UpdateAccount(Set<Id> AccId)
    {
        
        List<Account> acclst1= new List<Account>();
         if (!AccId.isempty() && AccId.size() > 0 ) //Added by VISHNU C for TOO Many SOQL Error B2F SC Interface
        acclst1 = [select id,TECH_IsSVMXRecordPresent__c from Account where id in : AccId and TECH_IsSVMXRecordPresent__c =false]; 
        List<Account> acclst = new List<Account>(); 
        if(acclst1.size()>0 && acclst1 != null)
        { 
            /*
            for(Id ac: AccId)
            {
                Account acc = new Account(Id=ac);
                acc.TECH_IsSVMXRecordPresent__c = true;
                acclst.add(acc);
            }
            */
           for(Account  aobj: acclst1 ){
               aobj.TECH_IsSVMXRecordPresent__c = true;
           } 
            update acclst1 ;
            
        }
        
        //if(acclst.size()>0) update acclst;
    }
}