@isTest
private class VFC_SearchProductCatalog_TEST
{
    static testMethod void TestIBSearch() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;

        SVMXC__Site__c location1 = new SVMXC__Site__c();
        location1.Name = 'Test Location';
        location1.SVMXC__Location_Type__c = 'SITE';
        location1.SVMXC__Account__c = account1.id;
        location1.TimeZone__c = 'Europe/Paris';
        insert location1;
        
        SVMXC__Installed_Product__c asset1 = new SVMXC__Installed_Product__c();
        asset1.Name = 'Test Asset';
        asset1.SVMXC__Company__c = account1.id;
        asset1.SVMXC__Site__c = location1.id;
        asset1.SVMXC__Status__c = 'Installed';
        asset1.BrandToCreate__c = 'Schneider Electric';
        asset1.DeviceTypeToCreate__c = 'Circuit Breaker';
        asset1.RangeToCreate__c = 'Range';
        insert asset1;
        
        string [] searchfilterList= new string[]{};
            
        for(Integer token=0;token<17;token++)
        {
            searchfilterList.add('a');          
        }
            
        List<String> keywords = new List<String>();
        Utils_DummyDataForTest dummydata = new Utils_DummyDataForTest();
        Utils_DataSource.Result  tempresult = new Utils_DataSource.Result();
        tempresult = dummydata.Search(keywords,keywords);
            
        sObject tempsObject = tempresult.recordList[0];
       
        VCC_IBProductSearch ProductforAsset1 = new VCC_IBProductSearch();
        ProductforAsset1.ActionType= 'simple';
        VCC06_DisplaySearchResults DisplaySearchResultsController =ProductforAsset1.resultsController;  
        VCC05_DisplaySearchFields DisplaySearchFieldsController = new VCC05_DisplaySearchFields(); 
            
        DisplaySearchFieldsController.pickListType = 'IB_PRODUCT';
        DisplaySearchFieldsController.SearchFilter = new String[4];
        DisplaySearchFieldsController.SearchFilter[0] = '00';
        DisplaySearchFieldsController.SearchFilter[1] = Label.CL00355;    
        DisplaySearchFieldsController.searchText = 'search\\stest';
        DisplaySearchFieldsController.key = 'searchComponent';
        DisplaySearchFieldsController.PageController = ProductforAsset1;
        DisplaySearchFieldsController.init();
        ProductforAsset1.searchController = DisplaySearchFieldsController;   
        DisplaySearchFieldsController.searchText = 'search\\stest';
        ProductforAsset1.search();
        ProductforAsset1.clear();
            
        ProductforAsset1.ObjID = asset1.ID;
        ProductforAsset1.Cleared = false;
        
        
        ApexPages.StandardController AssetStandardController = new ApexPages.StandardController(asset1);
        VFC_SearchProductCatalog controller = new VFC_SearchProductCatalog(AssetStandardController );
        
        Utils_IBProductSearchMethods WOImplementation = controller.WOImplementation;
        WOImplementation.init(asset1.id, ProductforAsset1);
        WOImplementation.PerformAction(tempresult.recordList[0], ProductforAsset1);
        WOImplementation.cancel(ProductforAsset1); 
    }
}