/*
    Author              : Hari
    Date Created        : 22-Oct-2014
    Description         : Test Class for AP_InstalledProductHandler
*/
@isTest
private class AP_InstalledProductHandler_Test{


    static testMethod void classTest(){
    
           
            
             Account objAccount = Utils_TestMethods.createAccount();
            objAccount.RecordTypeid = Label.CLOCT13ACC08;
            insert objAccount;
        
        Product2 p1 = new Product2(SKU__c= 'SKU',Name = 'Test Product ',ExtProductId__c='testing');
        insert p1;
        
        
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c(SVMXC__Company__c = objAccount.id,SVMXC__Product__c = p1.id);
       Test.starttest();
       try{
       insert ip1;
       }
       catch (exception e){}
       Test.stoptest();
        Map<String,List<SVMXC__Installed_Product__c>> mapInstalledProductsWithMissingProductInfo = new Map<String,List<SVMXC__Installed_Product__c>>();
        mapInstalledProductsWithMissingProductInfo.put('testing',new List<SVMXC__Installed_Product__c>{ip1});
        AP_InstalledProductHandler.calculateProductInfo(mapInstalledProductsWithMissingProductInfo);
            }

}