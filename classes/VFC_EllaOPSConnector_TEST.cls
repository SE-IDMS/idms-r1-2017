/*****
Last Modified By:Hanamanth asper Q1 release 
as Hotfix
******/


@isTest
public class VFC_EllaOPSConnector_TEST{
    public static testMethod void checkWithdrawalPushTest() {
        
        //User Creation
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA123');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        // Offer Creation
        Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
        offerRec.Withdrawal_Notice_Status__c = 'Published';
        insert offerRec;
        
        //Withdrawal Creation
        list<WithdrawalReference__c> withdrawal = new list<WithdrawalReference__c>();
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'XBTG5230'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'VX5IM2195M1271'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'VX5IM2220V21'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = '19203403V509001R'));
        insert withdrawal;
        
        PageReference pageRef = Page.VFP_EllaOPSConnector;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', offerRec.Id);
        ApexPages.currentPage().getParameters().put('callFor', 'Withdrawal');
    Test.setMock(WebServiceMock.class, new AP_BasicHttpBinding_IOPSImportMock_TEST('Status'));
        VFC_EllaOPSConnector controller = new VFC_EllaOPSConnector(new ApexPages.StandardController(offerRec));
        controller.callOPSService();
        controller.result = 'Test';
        PageReference pageRef1 = Page.VFP_EllaOPSConnector;
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('Id', offerRec.Id);
        ApexPages.currentPage().getParameters().put('callFor', 'Withdrawal');
        ApexPages.currentPage().getParameters().put('statusCall', 'true');
        
        try{ 
          controller.callOPSService();
        }catch(Exception e){
            
        }
    }
    
    public static testMethod void checkSubstitutionPushTest() {
        
        //User Creation
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA123');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        // Offer Creation
        Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
        offerRec.Withdrawal_Notice_Status__c = 'Published';
        insert offerRec;
        
        //Withdrawal Creation
        list<WithdrawalReference__c> withdrawal = new list<WithdrawalReference__c>();
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'XBTG5230'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'VX5IM2195M1271'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'VX5IM2220V21'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = '19203403V509001R'));
        insert withdrawal;
        
        PageReference pageRef = Page.VFP_EllaOPSConnector;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', offerRec.Id);
        ApexPages.currentPage().getParameters().put('callFor', 'Substitution');

        VFC_EllaOPSConnector controller = new VFC_EllaOPSConnector(new ApexPages.StandardController(offerRec));
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new AP_BasicHttpBinding_IOPSImportMock_TEST('Status'));
        try{
        controller.callOPSService();
        }catch(Exception e){     
        }
        try{
        controller.callSubstitutionPushService();
        }catch(Exception e){     
        }
        try{     
        controller.callSubstitutionStatusService();
        }catch(Exception e){     
        }
        try{    
        controller.callWithdrawalPushService();
        }catch(Exception e){     
        }
        try{    
        controller.callWithdrawalStatusService();
        }catch(Exception e){     
        }
        Test.stopTest();
        
        controller.returnMethod();
        
        PageReference pageRef1 = Page.VFP_EllaOPSConnector;
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('Id', offerRec.Id);
        ApexPages.currentPage().getParameters().put('callFor', 'Substitution');
        ApexPages.currentPage().getParameters().put('statusCall', 'true');
        try{ 
          controller.callOPSService();
        }catch(Exception e){
            
        }
    }
}