@IsTest
public class FieloPRM_VFC_MigrationToolControllerTest {
    static testMethod void test1(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
            System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c prog = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'Polo';
            member.FieloEE__FirstName__c = 'Marco';
            member.FieloEE__Street__c = 'test';
            member.FieloEE__Program__c = prog.Id;
            insert member;
            
            Contact conta = new Contact();
            conta.LastName = 'TEST';
            conta.FieloEE__Member__c = member.Id;
            insert conta;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.Name = 'Lucas Test';
            thisMenu.FieloEE__Title__c = 'asd';
            insert thisMenu;
            
            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            
            
            FieloEE__SegmentDomain__c segDomain = new FieloEE__SegmentDomain__c();
            segDomain.FieloEE__Menu__c  = thisMenu.id ;
            segDomain.FieloEE__Segment__c = segment.id;
            
            insert segDomain;
            
            FieloEE__Section__c section = new FieloEE__Section__c();
            section.FieloEE__Menu__c = thisMenu.Id;
            insert section;
            
            FieloEE__Tag__c tag = new FieloEE__Tag__c();
            tag.name = 'test';
            insert tag;
            
            FieloEE__Category__c category = FieloPRM_UTILS_MockUpFactory.createCategory(thisMenu.id);
        
            
            FieloEE__Component__c comp = new FieloEE__Component__c();
            comp.FieloEE__Section__c = section.Id;
            comp.FieloEE__Menu__c = thisMenu.Id;
            comp.FieloEE__Tag__c = tag.id;
            insert comp;
            
            FieloEE__News__c contentFeed = new FieloEE__News__c();
            contentFeed.FieloEE__IsActive__c = true;
            contentFeed.FieloEE__Component__c = comp.id;
            contentFeed.FieloEE__CategoryItem__c = category.id;
            insert contentFeed;            
            
            FieloEE__News__c contentFeed2 = new FieloEE__News__c();
            contentFeed2.FieloEE__IsActive__c = true;
            contentFeed2.FieloEE__Component__c = comp.id;
            contentFeed2.FieloEE__CategoryItem__c = category.id;
            insert contentFeed2;
            
            Attachment inputFile = new Attachment(Name = 'asd', Body = Blob.valueOf('asd'), parentId = contentFeed2.Id);
            insert inputFile;
            
            contentFeed2.FieloEE__AttachmentId__c = inputFile.Id; 
            update contentFeed2;
            
            FieloEE__SegmentDomain__c segDomain2 = new FieloEE__SegmentDomain__c();
            segDomain2.FieloEE__News__c  = contentFeed.id ;
            segDomain2.FieloEE__Segment__c = segment.id;
            
            insert segDomain2;
            
            FieloEE__SegmentDomain__c segDomain3 = new FieloEE__SegmentDomain__c();
            segDomain3.FieloEE__News__c  = contentFeed2.id ;
            segDomain3.FieloEE__Segment__c = segment.id;
            
            insert segDomain3;
            
            FieloEE__Banner__c banner = new FieloEE__Banner__c();
            banner.FieloEE__isActive__c = false;
            banner.FieloEE__Component__c  = comp.id;
            banner.FieloEE__Category__c = category.id;
            banner.Name = 'test';
            insert banner;
            
            FieloEE__Banner__c banner2 = new FieloEE__Banner__c();
            banner2.FieloEE__isActive__c = false;
            banner2.FieloEE__Component__c  = comp.id;
            banner2.FieloEE__Category__c = category.id;
            banner2.Name = 'test';
            insert banner2;
            
            FieloEE__SegmentDomain__c segDomain4 = new FieloEE__SegmentDomain__c();
            segDomain4.FieloEE__Banner__c = banner.id ;
            segDomain4.FieloEE__Segment__c = segment.id;
            
            insert segDomain4;
            
            FieloEE__SegmentDomain__c segDomain99 = new FieloEE__SegmentDomain__c();
            segDomain99.FieloEE__Banner__c = banner2.id ;
            segDomain99.FieloEE__Segment__c = segment.id;
            
            insert segDomain99;
            
            FieloEE__TagItem__c tagItemB = new FieloEE__TagItem__c();
            //tagItem.F_PRM_Category__c = category.id;
            tagItemB.FieloEE__Banner__c = banner.id;
            tagItemB.FieloEE__Tag__c = tag.id;
            insert tagItemB ;
            
            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c();
            //tagItem.F_PRM_Category__c = category.id;
            tagItem.FieloEE__News__c = contentFeed.id;
            tagItem.FieloEE__Tag__c = tag.id;
            insert tagItem;
            
            Test.startTest();
            FieloPRM_VFC_MigrationToolController cont = new FieloPRM_VFC_MigrationToolController();
            List<SelectOption> options = cont.getPrograms();
            cont.inputFile = new Attachment();
            cont.importFile();
            cont.inputFile.body = Blob.valueOf('blob');
            cont.inputFile.Name = 'abb';
            cont.importFile();
            cont.inputFile.body = Blob.valueOf('blob');
            cont.inputFile.Name = 'ab.b';
            cont.importFile();
            cont.inputFile.body = Blob.valueOf('<?xml version="1.0" encoding="UTF-8"?><menus type="list" object="FieloEE__Menu__c" bulkMode="true"><news type="relatedList" object="FieloEE__News__c"><new type="record"><recordtype type="field" field="recordtype">FieloPRM_NewsWidget</recordtype><FieloEE__Body type="field" field="FieloEE__Body__c">Learn everything you need to know about transformers affected by DOE mandates. In addition to new product specs and efficiency data, you&amp;#39;ll get the inside scoop on tips for getting ready for the new transformers launching January 1, 2016. &lt;br>&lt;br>&lt;b>&lt;u>Product Essentials:&lt;/u>&lt;/b> &lt;br>&lt;a href="https://schneider-electric.box.com/shared/static/nu6qr5s9intm49gh8i6u779z6r1vdsb3.pdf" target="_blank">&amp;gt; Technical Publication: How DOE Mandates Affect Low Voltage&lt;br>   Distribution Transformers &lt;/a>&lt;br>&lt;a href="https://schneider-electric.box.com/shared/static/hw40ck13m4j5g7n11b29kfc3e5kn1sci.pdf" target="_blank">&amp;gt; Technical Publication: New Transformer Specification Data &lt;/a>&lt;br>&lt;a href="http://blog.schneider-electric.com/energy-management-energy-efficiency/2015/08/14/why-you-should-care-and-prepare-for-new-distribution-transformer-energy-efficiency-standards/" target="_blank">&amp;gt; Blog: Why You Should Care for New Transformer Standards&lt;/a>&lt;br>&lt;a href="http://blog.schneider-electric.com/energy-management-energy-efficiency/2015/08/14/why-you-should-care-and-prepare-for-new-distribution-transformer-energy-efficiency-standards/" target="_blank">&amp;gt; Blog: Get Ready for New Distribution Transformer Efficiency Rules&lt;/a>&lt;br>&lt;a href="https://www.youtube.com/watch?v=6E-xQmGtIvA&amp;amp;feature=youtu.be" target="_blank">&amp;gt; Webinar: Medium &amp;amp; Low Voltage Transformers and Efficient Solutions&lt;/a>&lt;br>&lt;a href="https://schneider-electric.box.com/shared/static/i7xa172111xfp7rml5xu6ar8szd1ptk1.pdf" target="_blank">&amp;gt; FAQ: SQD EE to EX Transformer&lt;/a>. &lt;br> </FieloEE__Body><FieloEE__ExtractText type="field" field="FieloEE__ExtractText__c">Learn everything you need to know about transformers affected by DOE mandates. In addition to new product specs and efficiency data, youll get the inside scoop on tips for getting ready for the new transformers launching January 1, 2016.</FieloEE__ExtractText><FieloEE__IsActive type="field" field="FieloEE__IsActive__c">true</FieloEE__IsActive><FieloEE__Order type="field" field="FieloEE__Order__c">1</FieloEE__Order><FieloEE__PublishDate type="field" field="FieloEE__PublishDate__c">2015-12-16 00:00:00</FieloEE__PublishDate><FieloEE__QuantityComments type="field" field="FieloEE__QuantityComments__c">0</FieloEE__QuantityComments><FieloEE__QuantityLikes type="field" field="FieloEE__QuantityLikes__c">0</FieloEE__QuantityLikes><FieloEE__Title type="field" field="FieloEE__Title__c">Avoid installation surprises - DOE Transformer changes</FieloEE__Title><FieloEE__HasSegments type="field" field="FieloEE__HasSegments__c">false</FieloEE__HasSegments><F_PRM_ContentFilter type="field" field="F_PRM_ContentFilter__c">P-US</F_PRM_ContentFilter><Body_EN type="field" field="Body_EN__c">Learn everything you need to know about transformers affected by DOE mandates. In addition to new product specs and efficiency data, you&amp;#39;ll get the inside scoop on tips for getting ready for the new transformers launching January 1, 2016. &lt;br>&lt;br>&lt;b>&lt;u>Product Essentials:&lt;/u>&lt;/b> &lt;br>&lt;a href="https://schneider-electric.box.com/shared/static/nu6qr5s9intm49gh8i6u779z6r1vdsb3.pdf" target="_blank">&amp;gt; Technical Publication: How DOE Mandates Affect Low Voltage&lt;br>   Distribution Transformers &lt;/a>&lt;br>&lt;a href="https://schneider-electric.box.com/shared/static/hw40ck13m4j5g7n11b29kfc3e5kn1sci.pdf" target="_blank">&amp;gt; Technical Publication: New Transformer Specification Data &lt;/a>&lt;br>&lt;a href="http://blog.schneider-electric.com/energy-management-energy-efficiency/2015/08/14/why-you-should-care-and-prepare-for-new-distribution-transformer-energy-efficiency-standards/" target="_blank">&amp;gt; Blog: Why You Should Care for New Transformer Standards&lt;/a>&lt;br>&lt;a href="http://blog.schneider-electric.com/energy-management-energy-efficiency/2015/08/14/why-you-should-care-and-prepare-for-new-distribution-transformer-energy-efficiency-standards/" target="_blank">&amp;gt; Blog: Get Ready for New Distribution Transformer Efficiency Rules&lt;/a>&lt;br>&lt;a href="https://www.youtube.com/watch?v=6E-xQmGtIvA&amp;amp;feature=youtu.be" target="_blank">&amp;gt; Webinar: Medium &amp;amp; Low Voltage Transformers and Efficient Solutions&lt;/a>&lt;br>&lt;a href="https://schneider-electric.box.com/shared/static/i7xa172111xfp7rml5xu6ar8szd1ptk1.pdf" target="_blank">&amp;gt; FAQ: SQD EE to EX Transformer&lt;/a>. </Body_EN><ExtractText_EN type="field" field="ExtractText_EN__c">Learn everything you need to know about transformers affected by DOE mandates. In addition to new product specs and efficiency data, you get the inside scoop on tips for getting ready for the new transformers launching January 1, 2016.</ExtractText_EN><Title_EN type="field" field="Title_EN__c">Avoid installation surprises - DOE Transformer changes</Title_EN><F_PRM_PartnerNameWelcomeTitle type="field" field="F_PRM_PartnerNameWelcomeTitle__c">false</F_PRM_PartnerNameWelcomeTitle><F_PRM_PublishingInclude type="field" field="F_PRM_PublishingInclude__c">false</F_PRM_PublishingInclude><category type="recordName">News</category><tags type="relatedList" object="FieloEE__TagItem__c"><tag type="recordName">News Electrical Contractor</tag></tags></new></news></menus>');
            cont.inputFile.Name = 'ab.xml';
            cont.importFile();
            cont.inputFile.body = Blob.valueOf('<?xml version="1.0" encoding="UTF-8"?><menus type="list" object="FieloEE__Menu__c" bulkMode="true"><news type="relatedList" object="FieloEE__News__c"><new type="record"><recordtype type="field" field="recordtype">FieloPRM_NewsWidget</recordtype><FieloEE__Body type="field" field="FieloEE__Body__c">The A/E team for the 253,000-sq-ft James B. Hunt Jr. Library was tasked with designing an iconic facility that would serve as a signature structure for North Carolina State University&amp;#39;s Centennial Campus. The building had to be flexible and modern to meet the 21st century&amp;#39;s new learning paradigms. At the same time, the building was required to meet the state&amp;#39;s stringent new energy efficiency law requiring it to exceed the ASHRAE 90.1-2004 requirements by at least 30%. As part of the facility&amp;#39;s design, the owner and architects wanted to connect the building to the environment by providing extensive glass on all façades. AEI was challenged to allow these striking views while minimizing the load to the HVAC system. AEI also faced the challenge of designing MEP systems that support one of the most technologically sophisticated learning spaces in the world, including a bookBot robotic book delivery system and five giant high-definition display walls. The library&amp;#39;s Teaching &amp;amp; Visualization Lab and Creativity Lab include large projectors and servers with high power requirements, producing a tremendous amount of heat load. The final challenge was a total project budget reduction of $10.7 million during the design development phase. &lt;br>&lt;br>&lt;br>&lt;a href="http://www.csemag.com/index.php?id=2816&amp;amp;no_cache=1&amp;amp;tx_ttnews%5btt_news%5d=96561" target="_blank">&amp;gt;Learn More&lt;/a></FieloEE__Body><FieloEE__ExtractText type="field" field="FieloEE__ExtractText__c">The A/E team for the 253,000-sq-ft James B. Hunt Jr. Library was tasked with designing an iconic facility that would serve as a signature structure for North Carolina State Universitys Centennial Campus.</FieloEE__ExtractText><FieloEE__IsActive type="field" field="FieloEE__IsActive__c">true</FieloEE__IsActive><FieloEE__PublishDate type="field" field="FieloEE__PublishDate__c">2013-08-01 00:00:00</FieloEE__PublishDate><FieloEE__QuantityComments type="field" field="FieloEE__QuantityComments__c">0</FieloEE__QuantityComments><FieloEE__QuantityLikes type="field" field="FieloEE__QuantityLikes__c">0</FieloEE__QuantityLikes><FieloEE__Title type="field" field="FieloEE__Title__c">Designing electrical systems for higher education</FieloEE__Title><FieloEE__HasSegments type="field" field="FieloEE__HasSegments__c">false</FieloEE__HasSegments><F_PRM_ContentFilter type="field" field="F_PRM_ContentFilter__c">P-US-News-Evaluating software tools for electrical system design</F_PRM_ContentFilter><Body_EN type="field" field="Body_EN__c">The A/E team for the 253,000-sq-ft James B. Hunt Jr. Library was tasked with designing an iconic facility that would serve as a signature structure for North Carolina State University&amp;#39;s Centennial Campus. The building had to be flexible and modern to meet the 21st century&amp;#39;s new learning paradigms. At the same time, the building was required to meet the state&amp;#39;s stringent new energy efficiency law requiring it to exceed the ASHRAE 90.1-2004 requirements by at least 30%. As part of the facility&amp;#39;s design, the owner and architects wanted to connect the building to the environment by providing extensive glass on all façades. AEI was challenged to allow these striking views while minimizing the load to the HVAC system. AEI also faced the challenge of designing MEP systems that support one of the most technologically sophisticated learning spaces in the world, including a bookBot robotic book delivery system and five giant high-definition display walls. The library&amp;#39;s Teaching &amp;amp; Visualization Lab and Creativity Lab include large projectors and servers with high power requirements, producing a tremendous amount of heat load. The final challenge was a total project budget reduction of $10.7 million during the design development phase. &lt;br>&lt;br>&lt;br>&lt;a href="http://www.csemag.com/index.php?id=2816&amp;amp;no_cache=1&amp;amp;tx_ttnews%5btt_news%5d=96561" target="_blank">&amp;gt;Learn More&lt;/a></Body_EN><ExtractText_EN type="field" field="ExtractText_EN__c">The A/E team for the 253,000-sq-ft James B. Hunt Jr. Library was tasked with designing an iconic facility that would serve as a signature structure for North Carolina State Universitys Centennial Campus.</ExtractText_EN><Title_EN type="field" field="Title_EN__c">Designing electrical systems for higher education</Title_EN><F_PRM_PartnerNameWelcomeTitle type="field" field="F_PRM_PartnerNameWelcomeTitle__c">false</F_PRM_PartnerNameWelcomeTitle><F_PRM_PublishingInclude type="field" field="F_PRM_PublishingInclude__c">false</F_PRM_PublishingInclude><image type="image">https://devmajbfo-secommunities.cs2.force.com/partners/servlet/servlet.FileDownload?file=015R0000000J8DMIA0</image><category type="recordName">News</category><tags type="relatedList" object="FieloEE__TagItem__c"><tag type="recordName">News Consulting Engineers</tag></tags></new></news></menus>');
            cont.inputFile.Name = 'ab.xml';
            cont.importFile();
            cont.option = 'Banner';
            cont.query = '';
            cont.inputFile.body = Blob.valueOf('<?xml version="1.0" encoding="UTF-8"?><menus type="list" object="FieloEE__Menu__c" bulkMode="true"><banners type="relatedList" object="FieloEE__Banner__c"><banner type="record"><recordtype type="field" field="recordtype">FieloPRM_BannerWidget</recordtype><Name type="field" field="Name">P-US-PROEC-Proficient for EC-MDF Smaller Footprint</Name><FieloEE__Link type="field" field="FieloEE__Link__c">http://www.schneider-electric.com/products/us/e...</FieloEE__Link><FieloEE__Order type="field" field="FieloEE__Order__c">3</FieloEE__Order><FieloEE__OverlayHtml type="field" field="FieloEE__OverlayHtml__c">&lt;div class="title">Smaller Footprint&lt;/div>&lt;div class="box"> &lt;div class="bold">I-Line Combo Panelboard &lt;/div> We have combined I-Line and Lighting sections in one panelboard. Easy Installation, Flexible Design, Space-saving Enclosure. &lt;/div>&lt;div class="link">> Click here&lt;/div></FieloEE__OverlayHtml><FieloEE__isActive type="field" field="FieloEE__isActive__c">true</FieloEE__isActive><F_PRM_ContentFilter type="field" field="F_PRM_ContentFilter__c">P-US-PROEC-Proficient for EC-MDF Innovative Product2</F_PRM_ContentFilter><F_PRM_GoToLink type="field" field="F_PRM_GoToLink__c">http://www.schneider-electric.com/products/us/e...</F_PRM_GoToLink><F_PRM_LinkBehavior type="field" field="F_PRM_LinkBehavior__c">Open external link in new tab without header and footer</F_PRM_LinkBehavior><OverlayHtml_EN type="field" field="OverlayHtml_EN__c">&lt;div class="title">Smaller Footprint&lt;/div>&lt;div class="box"> &lt;div class="bold">I-Line Combo Panelboard &lt;/div> We have combined I-Line and Lighting sections in one panelboard. Easy Installation, Flexible Design, Space-saving Enclosure. &lt;/div>&lt;div class="link">> Click here&lt;/div></OverlayHtml_EN><F_PRM_PublishingInclude type="field" field="F_PRM_PublishingInclude__c">false</F_PRM_PublishingInclude><FieloEE__HasSegments type="field" field="FieloEE__HasSegments__c">false</FieloEE__HasSegments><image type="image">https://devmajbfo-secommunities.cs2.force.com/partners/servlet/servlet.FileDownload?file=01512000004f26gAAA</image><category type="recordName">PROficient for Electrical Contractor</category><tags type="relatedList" object="FieloEE__TagItem__c"><tag type="recordName">US - PROficient for Electrical Contractor - My daily features</tag></tags></banner></banners></menus>');
            cont.inputFile.Name = 'ab.xml';
            cont.importFile();
            cont.inputFile.body = Blob.valueOf('<?xml version="1.0" encoding="UTF-8"?><menus type="list" object="FieloEE__Menu__c" bulkMode="true"><banners type="relatedList" object="FieloEE__Banner__c"><banner type="record"><recordtype type="field" field="recordtype">FieloPRM_Banner</recordtype><Name type="field" field="Name">Acces à Facility Hero</Name><FieloEE__Link type="field" field="FieloEE__Link__c">http://www.facilityhero.com</FieloEE__Link><FieloEE__Order type="field" field="FieloEE__Order__c">1</FieloEE__Order><FieloEE__OverlayHtml type="field" field="FieloEE__OverlayHtml__c">&lt;div class="title">Title&lt;/div> &lt;div class="box"> &lt;div class="bold">Subtitle&lt;/div> Paragraph &lt;/div> &lt;div class="link">> Link Label&lt;/div></FieloEE__OverlayHtml><FieloEE__isActive type="field" field="FieloEE__isActive__c">true</FieloEE__isActive><F_PRM_ContentFilter type="field" field="F_PRM_ContentFilter__c">P-FR-ECOXP-PRO-MDF4-1</F_PRM_ContentFilter><F_PRM_GoToLink type="field" field="F_PRM_GoToLink__c">http://www.facilityhero.com</F_PRM_GoToLink><F_PRM_LinkBehavior type="field" field="F_PRM_LinkBehavior__c">Open external link in new tab without header and footer</F_PRM_LinkBehavior><OverlayHtml_FR type="field" field="OverlayHtml_FR__c">&lt;div class="title">Accès à Facility Hero&lt;/div> &lt;div class="box"> &lt;div class="bold">&lt;/div> Améliorez la maintenance de votre bâtiment avec le carnet de maintenance numérique : Facility Hero &lt;/div> &lt;div class="link">> Accédez à Facility Hero&lt;/div></OverlayHtml_FR><F_PRM_PublishingInclude type="field" field="F_PRM_PublishingInclude__c">false</F_PRM_PublishingInclude><FieloEE__HasSegments type="field" field="FieloEE__HasSegments__c">true</FieloEE__HasSegments><component type="recordName">Exploitation</component><segments type="relatedList" object="FieloEE__SegmentDomain__c"><segment type="recordName">APAC - IT Resellers</segment></segments></banner></banners></menus>');
            cont.inputFile.Name = 'ab.xml';
            cont.importFile();
            cont.inputFile.body = Blob.valueOf('<?xml version="1.0" encoding="UTF-8"?><menus type="list" object="FieloEE__Menu__c" bulkMode="true"><banners type="relatedList" object="FieloEE__Banner__c"><banner type="record"><recordtype type="field" field="recordtype">FieloPRM_Banner</recordtype><Name type="field" field="Name">Acces à Facility Hero</Name><FieloEE__Link type="field" field="FieloEE__Link__c">http://www.facilityhero.com</FieloEE__Link><FieloEE__Order type="field" field="FieloEE__Order__c">1</FieloEE__Order><FieloEE__OverlayHtml type="field" field="FieloEE__OverlayHtml__c">&lt;div class="title">Title&lt;/div> &lt;div class="box"> &lt;div class="bold">Subtitle&lt;/div> Paragraph &lt;/div> &lt;div class="link">> Link Label&lt;/div></FieloEE__OverlayHtml><FieloEE__isActive type="field" field="FieloEE__isActive__c">true</FieloEE__isActive><F_PRM_ContentFilter type="field" field="F_PRM_ContentFilter__c">P-FR-ECOXP-PRO-MDF4-1</F_PRM_ContentFilter><F_PRM_GoToLink type="field" field="F_PRM_GoToLink__c">http://www.facilityhero.com</F_PRM_GoToLink><F_PRM_LinkBehavior type="field" field="F_PRM_LinkBehavior__c">Open external link in new tab without header and footer</F_PRM_LinkBehavior><OverlayHtml_FR type="field" field="OverlayHtml_FR__c">&lt;div class="title">Accès à Facility Hero&lt;/div> &lt;div class="box"> &lt;div class="bold">&lt;/div> Améliorez la maintenance de votre bâtiment avec le carnet de maintenance numérique : Facility Hero &lt;/div> &lt;div class="link">> Accédez à Facility Hero&lt;/div></OverlayHtml_FR><F_PRM_PublishingInclude type="field" field="F_PRM_PublishingInclude__c">false</F_PRM_PublishingInclude><FieloEE__HasSegments type="field" field="FieloEE__HasSegments__c">false</FieloEE__HasSegments><component type="recordName">Exploitation</component><tags type="relatedList" object="FieloEE__TagItem__c"><tag type="recordName">Technical Support</tag></tags></banner></banners></menus>');
            cont.inputFile.Name = 'ab.xml';
            cont.importFile();
            cont.export();
            cont.option = '';
            cont.export();
            cont.downloadExport();
            cont.downloadImport();
            Test.stopTest();
        }
    }
}