public without sharing class VFC02_TeamReferenceControllerExtension {
    private final REF_TeamRef__c team;
    public Id groupId {get; set;}
    private String dmlException;
    public String selectedQueueName; 
    //public Boolean bTestFlag = false;
    
    public String getSelectedQueueName() {
        return selectedQueueName;
    }
     
    public VFC02_TeamReferenceControllerExtension(ApexPages.StandardController stdController){
            this.team = (REF_TeamRef__c)stdController.getRecord();
             String queueId = getGroup();
            if (queueId  != null) {
            List<Group> sfdcGroup = [select Id, name from Group where Id = :queueId limit 1];
              if (!sfdcGroup.isEmpty()) {
                  selectedQueueName = sfdcGroup [0].name;
               } else {
                   selectedQueueName  =null;
               }
                //Group group= [select Id,name from Group where Id = :team.Queue__c limit 1];
                //List<Group> group= [select id, Name from Group where id = :queueId];
               }
            System.debug('##### VFC02_TeamReferenceController');
   }
   
   public REF_TeamRef__c getTeam() {
       return team;
   }
   
   private String getGroup() {
   system.debug('#### getGroupId');
   system.debug('#### team.Id : ' + team.Id);
       List<REF_TeamRef__c> team = [select id, Queue__c from REF_TeamRef__c where id = :team.Id];
       if ( team!= null && !team.isEmpty()) {
            if (team[0].Queue__c != null) {
                system.debug('#### getGroupId : ' + team[0].Queue__c);
                return team[0].Queue__c;
            } else {
                return null;
            }
       } else {
                return null;
            }
   }
   
   
   public PageReference updateRecord(){
       // try {
            team.Queue__c=groupId;
            System.debug('######### avant');
            //update sfeIndivCAP;
            Database.SaveResult sr= Database.update(team, false);
            if(!sr.isSuccess()) {
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, sr.getErrors()[0].getMessage()));
                   dmlException = sr.getErrors()[0].getMessage();
                   System.debug('######### dedans : ' +dmlException );
                   
            }
            System.debug('######### apres');
        return null;
    }
    
    
    
   /*=========================
   TEST METHOD TEST METHOD
   ===========================*/
   public static testmethod void testteamRefController(){
   	REF_TeamRef__c testTeam = new REF_TeamRef__c();
   	testTeam.Name = 'testteam';
   	insert testTeam;
   	
   	Group testQueue = new Group();
   	testQueue.Name = 'test';
   	testQueue.type='Queue';
   	insert testQueue;
   	
   	QueueSobject oQueue = new QueueSobject();
   	oQueue.QueueId = testQueue.id;
   	oQUeue.SobjectType = 'CHR_ChangeReq__c';
   	//insert oQUeue;
   	
   	ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(testTeam);
   	test.startTest();
   	VFC02_TeamReferenceControllerExtension testCtl = new 	VFC02_TeamReferenceControllerExtension(sc);
   	testCtl.groupId = testQueue.Id;
   	testCtl.updateRecord();
   	String testName = testCtl.getSelectedQueueName();
   	
   	testCtl = new 	VFC02_TeamReferenceControllerExtension(sc);
   	REF_TeamRef__c testResultTeam =  testCtl.getTeam();
   	test.stopTest();
   }
   
    
   
}