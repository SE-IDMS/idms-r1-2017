public with sharing class VFC_NewCountrySpecialization 
{
    public ID GlobalSpecializationId {get;set;}
    public VFC_NewCountrySpecialization(ApexPages.StandardController controller) 
    {
        GlobalSpecializationId = controller.getID();
    }
    
    Public PageReference createCountrySpecialization()
    {
        Specialization__c CountrySpec  = new Specialization__c();
        list<Country__c> lstCountry = new list<country__c>();
        list<Specialization__c> lstExistingCountrySpecialization = new list<Specialization__c>();
        //list<SpecializationCountry__c> lstProgramCountries = new list<SpecializationCountry__c>();
        
        String strFields = '';
        Savepoint sp;
        try
        {
            GlobalSpecializationID = ApexPages.currentPage().getParameters().get('GlobalSpeId');
            Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Specialization__c.fields.getMap();
            if (mapFields != null)
            {
                for (Schema.SObjectField ft : mapFields.values())// loop through all field tokens (ft)
                {
                    strFields += ft.getDescribe().getName()+',';
                }
            }

            if (strFields.endsWith(','))
            {
                strFields = strFields.substring(0,strFields.lastIndexOf(','));
            }
            String globalSPEQuery = 'SELECT '+ strFields + ' FROM Specialization__c WHERE id = :GlobalSpecializationID limit 1';
            Specialization__c globalSPE = database.query(globalSPEQuery );
            
            if(globalSPE != null)
            {
                if(globalSPE.Status__C != 'Active')
                {
                    ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR,'You are allowed to create Country / Cluster specializations for active Specializations only');
                    ApexPages.addmessage(msg3); 
                    return null;
                }
            }
            User currentUser = [Select Country__c from User where id =:UserInfo.getUserId() limit 1];
            if(currentUser.Country__c != null)
            {
                lstCountry = [Select id,Name,countrycode__c from Country__c where Countrycode__c =: currentUser.Country__c limit 1];
            }
            if(!lstCountry.isEmpty() && lstCountry.size() == 1 )
            {
                lstExistingCountrySpecialization = [Select id from Specialization__c where GlobalSpecialization__c = :GlobalSpecializationID and country__c =:lstCountry[0].id  limit 1];
            }    
            if(!lstExistingCountrySpecialization.isEmpty() )
            {   
                ApexPages.Message msg2 = new ApexPages.Message(ApexPages.Severity.ERROR,'Country Specialization already created for this user country');
                ApexPages.addmessage(msg2); 
                return null;
            }
            
            
            CountrySpec = globalSPE.clone(false, true);
            if(!lstCountry.isEmpty() && lstCountry.size() == 1 )
            {
                if(globalSPE.TECH_CountriesId__c != null && globalSPE.TECH_CountriesId__c.contains(lstCountry[0].Id))
                {
                    CountrySpec.Country__c = lstCountry[0].Id;
                    CountrySpec.Name = CountrySpec.Name + ' - ' +lstCountry[0].Name;
                }
                else
                {
                    ApexPages.Message msg1 = new ApexPages.Message(ApexPages.Severity.ERROR,'Specialization is not applicable to your Country. Please raise a Change Request to add your Country to Specailization');
                    ApexPages.addmessage(msg1); 
                    return null;
                }
            }
            else
            {
                ApexPages.Message msg1 = new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have a Country assigned.Please assign a country and then try again.');
                ApexPages.addmessage(msg1); 
                return null;
            }
            
            //For Cluster Specialization
            String PRGtype = ApexPages.currentPage().getParameters().get('type');
            if(PRGtype == 'country')
                CountrySpec.recordtypeid = System.Label.CLOCT13PRM41;
            else if(PRGtype == 'cluster')
            {
                CountrySpec.recordtypeid = System.Label.CLOCT13PRM42;
                CountrySpec.TECH_CountriesId__c = lstCountry[0].Id;
            }
            CountrySpec.GlobalSpecialization__c = GlobalSpecializationID;
            CountrySpec.Status__c = 'Draft';
            CountrySpec.GlobalProgramOwner__c  = globalSPE.ownerId;
            CountrySpec.ownerid = userinfo.getUserID();
            sp = Database.setSavepoint();

            insert CountrySpec;

            //Creating Specialization Market Segments
            list<SpecializationMarket__c> lstSpecializationMarket = new list<SpecializationMarket__c>();
            list<SpecializationMarket__c> lstNewSpecializationMarket = new list<SpecializationMarket__c>();
            lstSpecializationMarket = [Select id, MarketSegmentCatalog__c from SpecializationMarket__c where Specialization__c = :GlobalSpecializationID limit 1000];
            if(!lstSpecializationMarket .isEmpty())
                for(SpecializationMarket__c speMarket : lstSpecializationMarket )
                {
                    SpecializationMarket__c newSpeMarket = new SpecializationMarket__c();
                    newSpeMarket.MarketSegmentCatalog__c = speMarket.MarketSegmentCatalog__c;
                    newSpeMarket.Specialization__c = CountrySpec.Id;
                    lstNewSpecializationMarket.add(newSpeMarket);
                }
            if(!lstNewSpecializationMarket.isEmpty())
                insert lstNewSpecializationMarket;
            
            //Creating Specialization Market Segments
            list<SpecializationClassification__c> lstSpecializationClassification = new list<SpecializationClassification__c>();
            list<SpecializationClassification__c> lstNewSpecializationClassification = new list<SpecializationClassification__c>();
            lstSpecializationClassification = [Select id, ClassificationLevelCatalog__c from SpecializationClassification__c where Specialization__c = :GlobalSpecializationID limit 1000];
            if(!lstSpecializationClassification.isEmpty())
                for(SpecializationClassification__c speClass : lstSpecializationClassification )
                {
                    SpecializationClassification__c newSpeClass = new SpecializationClassification__c();
                    newSpeClass.ClassificationLevelCatalog__c = speClass.ClassificationLevelCatalog__c;
                    newSpeClass.Specialization__c = CountrySpec.Id;
                    lstNewSpecializationClassification.add(newSpeClass);
                }
            if(!lstNewSpecializationClassification.isEmpty())
                insert lstNewSpecializationClassification;
            //Creating Specialization Domains
            list<SpecializationDomainsOfExpertise__c> lstSpecializationDomains = new list<SpecializationDomainsOfExpertise__c>();
            list<SpecializationDomainsOfExpertise__c> lstNewSpecializationDomains = new list<SpecializationDomainsOfExpertise__c>();
            lstSpecializationDomains = [Select id, DomainsOfExpertiseCatalog__c from SpecializationDomainsOfExpertise__c where Specialization__c = :GlobalSpecializationID limit 1000];
            if(!lstSpecializationDomains.isEmpty())
                for(SpecializationDomainsOfExpertise__c speDomains : lstSpecializationDomains )
                {
                    SpecializationDomainsOfExpertise__c newSpeDomains = new SpecializationDomainsOfExpertise__c();
                    newSpeDomains.DomainsOfExpertiseCatalog__c = speDomains.DomainsOfExpertiseCatalog__c;
                    newSpeDomains.Specialization__c = CountrySpec.Id;
                    lstNewSpecializationDomains.add(newSpeDomains);
                }
            if(!lstNewSpecializationDomains.isEmpty())
                insert lstNewSpecializationDomains; 
            /////    
            String error = createChildRecords(CountrySpec , GlobalSpecializationID);
            if(error != null && error.trim() != '')
            {
                Database.rollback(sp); //TODO  add a error on page
                return null;
            }    
            PageReference countrySPEPage = new PageReference('/'+CountrySpec.Id );
            return countrySPEPage;
                

        }catch(Exception ex){
            system.debug('Excpetion:'+ex);
            Database.rollback(sp);
            system.debug(ex.getMessage());
            String er = ex.getMessage(); 
            er =  er.substring(er.indexof('EXCEPTION') > 1 ? er.indexof('EXCEPTION')+10 : 0,  er.indexof('[]]') > 1 ? er.indexof('[]'): er.length()); 
            //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, er,'')); 
            System.debug(ex.getMessage()); 
           
            return null;
        }
        return null;
        
    }
    
        //Method to create child records for new Country Specilization
    public String createChildRecords(Specialization__c countrySpec, ID GlobalSpecializationId)
    {
        //Specialization Requirements
        List<SpecializationRequirement__c> lstGlobalSpecializationReqs = new List<SpecializationRequirement__c>();
        List<SpecializationRequirement__c> lstCSpecializationReqs = new List<SpecializationRequirement__c>();
        lstGlobalSpecializationReqs = [SELECT RecordTypeId, Active__c, GlobalSpecializationRequirement__c, RequirementCatalog__c, Specialization__c, Target__c FROM SpecializationRequirement__c WHERE Specialization__c=:GlobalSpecializationId limit 1000];
        for(SpecializationRequirement__c gSpeReq : lstGlobalSpecializationReqs)
        {
            SpecializationRequirement__c newCSpeReq = new SpecializationRequirement__c();
            newCSpeReq.GlobalSpecializationRequirement__c = gSpeReq.id ;
            newCSpeReq.Specialization__c = countrySpec.id ;

            newCSpeReq.RecordTypeId = gSpeReq.RecordTypeId ;
            newCSpeReq.Active__c = gSpeReq.Active__c ;
            newCSpeReq.RequirementCatalog__c = gSpeReq.RequirementCatalog__c ;
            newCSpeReq.Target__c = gSpeReq.Target__c ;

            lstCSpecializationReqs.add(newCSpeReq);
        }    

        try{
                insert lstCSpecializationReqs;

        }Catch(Exception e)
        {
            System.debug('Error while creating child Specialization Requirements '+e.getMessage());
            String er = e.getMessage(); 
            return er.substring(er.indexof('EXCEPTION') > 1 ? er.indexof('EXCEPTION')+10 : 0,  er.indexof('[]]') > 1 ? er.indexof('[]'): er.length()); 
        }

        return null;
    }//End of method
}//End of class