global class VFC_PRMConfirmEmailChangeController {

    global transient String FederatedId { get; set; }
    global transient String EmailToken { get; set; } 
    global transient String FName { get; set; }
    global transient String LName { get; set; }
    global transient String StaticValues { get; set; }
    global String CntrySupportEmail { get; set; }

    global VFC_PRMConfirmEmailChangeController() {
        FederatedId = ApexPages.currentPage().getParameters().get('federatedId');
        EmailToken = ApexPages.currentPage().getParameters().get('token');     
        FName = ApexPages.currentPage().getParameters().get('fn');     
        LName = ApexPages.currentPage().getParameters().get('ln');    
        system.debug('token:' + EmailToken );
        
      
        Map<String, Object> mapDependentData = new Map<String, Object>(getDependentPickList());
        System.debug('***** dependent ->'+mapDependentData);
        if(mapDependentData != null && mapDependentData.size() > 0)
            StaticValues = JSON.serialize(mapDependentData);
            
        List<Contact> lstContact = new List<Contact>([SELECT Id,Name,PRMEmail__c,PRMCountry__c FROM Contact WHERE PRMUIMSID__c = :FederatedId]);
        if(lstContact != null && !lstContact.isEmpty()) {
            String pcntry = '%'+lstContact[0].PRMCountry__c+'%';
            List<PRMCountry__c> lstPRMCountry = new List<PRMCountry__c>([SELECT CountrySupportEmail__c, Country__c, TECH_Countries__c, CountryPortalEnabled__c FROM PRMCountry__c
                                                                         WHERE CountryPortalEnabled__c = true AND (Country__c = :lstContact[0].PRMCountry__c OR TECH_Countries__c LIKE :pcntry)]);
            if(lstPRMCountry != null && !lstPRMCountry.isEmpty()) {
                CntrySupportEmail = lstPRMCountry[0].CountrySupportEmail__c;
            }  
        }  
    }

    //----------------------------------------------------------------------------------------------------
    // Dependent picklist
    //-----------------------------------------------------
    global static Map<String, Object> getDependentPickList() {
        Map<String, Object> mRefData = new Map<String, Object>();
        List<Country__c> lCountries = [SELECT Id, Name, (SELECT Id,Name FROM StatesProvinces__r) FROM Country__c];
        mRefData.put('CountryList', lCountries);
        List<ClassificationLevelCatalog__c> lClassification = [SELECT Id, Name, ClassificationLevelName__c, ParentClassificationLevel__c, (SELECT Id, Name, ClassificationLevelName__c FROM Classification_Levels__r) FROM ClassificationLevelCatalog__c WHERE Active__c = true AND ParentClassificationLevel__c = ''];
        mRefData.put('ClassificationList', lClassification);
        mRefData.put('Translations',getStaticLabelTranslations('complete profile'));
        return mRefData;
    }

    //--------------------------------------------------------------------------------
    global static Map<String, String> getStaticLabelTranslations(string category) {
        String defaultLanguage = 'en_US';
        Map<String, String> mTranslations = new Map<String, String>();
        User[] lUser = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
        if (lUser != null && lUser.size() > 0)
            defaultLanguage = lUser[0].LanguageLocaleKey;
            
        String categoryValue = '%' + category+'%';  

        List<ExternalString> lStaticLabels = [SELECT Id, Name, Category, Language, Value,
                                                (SELECT Language, Value FROM Localization WHERE Language = :defaultLanguage)
                                                FROM ExternalString WHERE Category LIKE :categoryValue];
        for (ExternalString es : lStaticLabels) {
            String tVal = es.Value;
            if (es.Localization != null && es.Localization.size() > 0){
                tVal = es.Localization[0].Value;
            }
            mTranslations.put(es.Name, tVal);
        }

        return mTranslations;
    }
    @RemoteAction
    global static VFC_PRMUserRegistrationController.ActivateUserResult confirmEmailChange(String activationToken, String userFederatedId){
       // Boolean uimsresult = false;
       VFC_PRMUserRegistrationController.ActivateUserResult uimsresult = new VFC_PRMUserRegistrationController.ActivateUserResult();
       uimsresult.Success = false;
        String cId;
        AP_UserRegModel uimsUser = new AP_UserRegModel();
        try {
            System.debug('** Activation Token received**:' + activationToken);
            /*activationToken = EncodingUtil.urlDecode(activationToken, 'UTF-8');
            System.debug('** Decoded token:' + activationToken);
            */
            if (string.isNotBlank(activationToken)) {
              // HACK: WHEN THE PAGE IS ACCESSED WITH FIELO's URL, getParameters value returned value is double decoded.
              if (activationToken.contains(' '))
                  activationToken = activationToken.replace(' ' ,'+');
            }
            System.debug('** Activation Token received**:' + activationToken);
            String callerFid  = System.label.CLAPR15PRM019;
            uimsv2ImplServiceImsSchneiderComNew2.UserManager_UIMSV2_ImplPort uimsport = new uimsv2ImplServiceImsSchneiderComNew2.UserManager_UIMSV2_ImplPort();
            uimsport.timeout_x        = Integer.valueOf(System.label.CLAPR15PRM021);
            uimsport.clientCertName_x = System.Label.CLAPR15PRM018;
            uimsport.endpoint_x       = System.Label.CLAPR15PRM056;
            
            //uimsresult = uimsport.updateEmail(callerFid, activationToken);
            uimsresult.Success = uimsport.updateEmail(callerFid, activationToken);
            if (uimsresult.Success == true) {
                List<User> lUsr = new List<User>([SELECT Id,Name,ContactId,AccountId, PRMTemporarySamlToken__c FROM User WHERE FederationIdentifier = :userFederatedId ]);
                /*if(lUsr.size() > 0) {
                String authenticationToken = lUsr[0].PRMTemporarySamlToken__c;
                cId = lUsr[0].ContactId;
                uimsv2ImplServiceImsSchneiderComNew2.UserManager_UIMSV2_ImplPort uimsPort1 = new uimsv2ImplServiceImsSchneiderComNew2.UserManager_UIMSV2_ImplPort();
                uimsPort1.timeout_x = Integer.valueOf(System.label.CLAPR15PRM021);
                uimsPort1.clientCertName_x = System.Label.CLAPR15PRM018;
                uimsPort1.endpoint_x = System.Label.CLAPR15PRM151;//'https://ims2w-int.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/UserManager';*/
                
                uimsUser = AP_PRMUtils.GetContactInfo_Admin(userFederatedId);
                system.debug('**UIMS USer**' + uimsUser);
                
             
                    List<Contact> lUsrContact = new List<Contact>([SELECT Id,Name,PRMEmail__c FROM Contact WHERE Id = :lUsr[0].ContactId]);
                    if (lUsrContact != null && !lUsrContact.isEmpty()) {
                        lUsrContact[0].PRMEmail__c = uimsUser.email;
                        uimsresult.RedirectEmail = uimsUser.email;
                        uimsresult.RedirectUrl = '/partners/apex/VFP_PartnersLogout?newEmail='+uimsresult.RedirectEmail;
                        UPDATE lUsrContact;
                    }
                    AP_PRMUtils.updateUserEmail(lUsr[0].Id,uimsUser.email);
                }
                
            System.debug('**Response**'+uimsresult);
            return uimsresult;
        }
        catch (DmlException e) {
            System.debug('**** error'+e.getMessage());
        }
        catch (Exception e) {
            uimsresult.Success = false;
            uimsresult.ErrorMsg = e.getMessage();
            System.debug('**** error'+e.getMessage());
            /*if(e.getMessage().containsIgnoreCase('Unable to retrieve federated id')){
                uimsresult.Success = true; 
            }*/
            IntegrationCalloutEvent__c calloutRec = new IntegrationCalloutEvent__c();
            calloutRec.Contact__c = cId;
            calloutRec.EventType__c = 'emailChange';
            calloutRec.Status__c = False;
            
            insert calloutRec; 
        }
        return uimsresult;
    }
}