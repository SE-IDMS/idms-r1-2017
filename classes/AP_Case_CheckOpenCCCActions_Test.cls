@isTest
private class AP_Case_CheckOpenCCCActions_Test{
    public static testMethod void testCaseWithOpenCCCActions(){
        System.debug('#### AP_Case_CheckOpenCCCActions_Test.testCaseWithOpenCCCActions Started ####');
       
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='IN';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'In Progress');
        insert objCaseForAction;
        
        CCCAction__c objAction = Utils_TestMethods.createCCCAction(objCaseForAction.Id);
        insert objAction;
        
        Map<Id,Case> caseMap= new Map<Id,Case>();
        caseMap.put(objCaseForAction.id, objCaseForAction);
        AP_Case_CheckOpenCCCActions.checkForOpenCCCActions(caseMap,true,false);
        AP_Case_CheckOpenCCCActions.checkForOpenCCCActions(caseMap,false,false);
        
        System.debug('#### AP_Case_CheckOpenCCCActions_Test.testCaseWithOpenCCCActions Ends ####');
    }
    
    public static testMethod void testCaseWithOpenTEX(){ 
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='IT';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        Case objCase = Utils_TestMethods.createCase(objAccount.Id,objContact.Id,'In Progress');
        objCase.ProductBU__c      = 'INFRASTRUCTURE';
        objCase.ProductLine__c    = 'INTAG - TELVENT AGRICULTURE';
        objCase.ProductFamily__c  = 'ENTERPRISE MGMNT SOL';
        objCase.Family__c         = 'PROCEDURE SUIT';
        objCase.TECH_LaunchGMR__c = false;    
        objCase.CheckedbyGMR__c   = true;    
        insert objCase;
        
        BusinessRiskEscalationEntity__c brEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
        insert brEntity;
        BusinessRiskEscalationEntity__c brSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
        insert brSubEntity;
        BusinessRiskEscalationEntity__c br = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation ();
        insert br;
         
        TEX__c objTex = new TEX__c();
        objTex.Case__c = objCase.id;
        objTex.AssessmentType__c = 'Product Return to Expert Center';
        objTex.Status__c = 'Expert Assessment to be started';
        objTex.Prioritylevel__c = 'Normal';
        objTex.FrontOfficeTEXManager__c = UserInfo.getUserId();
        objTex.FrontOfficeOrganization__c = br.Id;
        objTex.ExpertCenter__c = br.Id;
        objTex.AffectedOffer__c = 'testAffectOffer';
        Insert objTex;
            
        Map<Id,Case> caseMap= new Map<Id,Case>();
        caseMap.put(objCase.id, objCase);
        AP_Case_CheckOpenCCCActions.checkOpenTEXs(caseMap,true,false);
        AP_Case_CheckOpenCCCActions.checkOpenTEXs(caseMap,false,false);
    }
    
    public static testMethod void testCaseWithOpenLCR(){
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='GB';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        Case objCase = Utils_TestMethods.createCase(objAccount.Id,objContact.Id,'In Progress');
        objCase.ProductBU__c      = 'INFRASTRUCTURE';
        objCase.ProductLine__c    = 'INTAG - TELVENT AGRICULTURE';
        objCase.ProductFamily__c  = 'ENTERPRISE MGMNT SOL';
        objCase.Family__c         = 'PROCEDURE SUIT';
        objCase.TECH_LaunchGMR__c = false;    
        objCase.CheckedbyGMR__c   = true;    
        Database.insert(objCase);
        
        BusinessRiskEscalationEntity__c objOrganization = new BusinessRiskEscalationEntity__c();
        objOrganization.Name = 'TEST-LCR';
        objOrganization.Entity__c = 'TEST-LCR';
        Database.insert(objOrganization);
        
        ComplaintRequest__c objComplaintRequest = new ComplaintRequest__c();
        objComplaintRequest.Case__c = objCase.id;
        objComplaintRequest.LatestExpectedDate__c = Date.Today();
        objComplaintRequest.Category__c = '4 - Post-Sales Tech Support';
        objComplaintRequest.ReportingEntity__c = objOrganization.id;
        objComplaintRequest.Reason__c = 'Troubleshooting';
        objComplaintRequest.Status__c = 'Open';
        Database.insert(objComplaintRequest);
        
        Map<Id,Case> caseMap= new Map<Id,Case>();
        caseMap.put(objCase.id, objCase);
        AP_Case_CheckOpenCCCActions.checkOpenComplaintRequest(caseMap,true,false);
        AP_Case_CheckOpenCCCActions.checkOpenComplaintRequest(caseMap,false,false);
    }
    
    public static testMethod void testCaseWithOpenWON(){
    
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='ES';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'In Progress');
        insert objCase;
        
        WorkOrderNotification__c objWON = Utils_TestMethods.createWorkOrderNotification(objCase.Id,null);
        Database.insert(objWON);
        
        Map<Id,Case> caseMap= new Map<Id,Case>();
        caseMap.put(objCase.id, objCase);
        AP_Case_CheckOpenCCCActions.checkOpenWON(caseMap,true,false);
        AP_Case_CheckOpenCCCActions.checkOpenWON(caseMap,false,false);
    }
    
    public static testMethod void testCaseWithRMA(){
    
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='FR';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'In Progress');
        insert objCase;
        
        RMA__c objRMA = new RMA__c();
        objRMA.Case__c = objCase.Id;
        Database.insert(objRMA);
        
        Map<Id,Case> caseMap= new Map<Id,Case>();
        caseMap.put(objCase.id, objCase);
        AP_Case_CheckOpenCCCActions.checkForOpenRROnCaseClosure(caseMap);
    }
    
    public static testMethod void testCaseWithTasks(){
    
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='IN';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'In Progress');
        insert objCase;
        
        Task objTask = Utils_TestMethods.createTask(objCase.Id, objContact.id, 'Open');
        Database.insert(objTask);
        
        Map<Id,Case> caseMap= new Map<Id,Case>();
        caseMap.put(objCase.id, objCase);
        AP_Case_CheckOpenCCCActions.checkForOpenTasks(caseMap,true,false);
        AP_Case_CheckOpenCCCActions.checkForOpenTasks(caseMap,false,false);
    }
    
    public static testMethod void testCaseWithOpenOpptyNotification(){
    
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='IN';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'In Progress');
        insert objCase;
        
        OpportunityNotification__c objOpptyNotification = Utils_TestMethods.createOpportunityNotification(null);
        objOpptyNotification.Case__c = objCase.Id;
        objOpptyNotification.CountryDestination__c = objCountry.Id;
        Database.insert(objOpptyNotification);
        
        Map<Id,Case> caseMap= new Map<Id,Case>();
        caseMap.put(objCase.id, objCase);
        AP_Case_CheckOpenCCCActions.checkForOpenOpptyNotificationOnCaseClosure(caseMap);
    }
    
    public static testMethod void testCaseWithExternalReference(){
    
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='FR';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'In Progress');
        insert objCase;
        
        CSE_ExternalReferences__c objExtrnlReference = Utils_TestMethods.createCaseExternalRef(objCase.Id);
        Database.insert(objExtrnlReference);
        
        Map<Id,Case> caseMap= new Map<Id,Case>();
        caseMap.put(objCase.id, objCase);
        AP_Case_CheckOpenCCCActions.checkForExternalRefOnCaseClosure(caseMap);
    }
    public static testMethod void testCaseClosure(){ 
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='FR';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'In Progress');
        insert objCase;
		
		AP_Case_CheckOpenCCCActions.blnUpdateTestRequired=true;
		AP_Case_CaseHandler.blnUpdateTestRequired=true;
        
		objCase.AnswerToCustomer__c='We are Closing this Case as we provided the correct answer';
		objCase.Status=System.Label.CL00325;
		update objCase; 
		
        
    }
	public static testMethod void testCaseCancellation(){ 
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='IT';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'In Progress');
        insert objCase;
        
		AP_Case_CheckOpenCCCActions.blnUpdateTestRequired=true;
		AP_Case_CaseHandler.blnUpdateTestRequired=true;
        
		objCase.Status=System.Label.CL00326;
		update objCase;
        
    }
	public static testMethod void testCustomerCaseFollowup(){ 
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='GB';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'In Progress');
        insert objCase;
        
		AP_Case_CheckOpenCCCActions.blnUpdateTestRequired=true;
		AP_Case_CaseHandler.blnUpdateTestRequired=true;
        
		objCase.ActionNeededFrom__c= Label.CLOCT14CCC09;
        objCase.Status=System.Label.CL00230;
		update objCase;
    }
}