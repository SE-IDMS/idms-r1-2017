/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CHR_CurrentActorController_TEST {

    static testMethod void myUnitTest() {
    	CHR_ChangeReq__c chr1 = Utils_TestMethods.createRTCHR('sample1','SCE01_EMEAS_01');
        //CHR_ChangeReq__c chr1 = Utils_TestMethods.createCHR('sample1');
        CHR_ChangeReq__c chr2 = Utils_TestMethods.createRTCHR('sample2','SCE01_EMEAS_01');
        //CHR_ChangeReq__c chr2 = Utils_TestMethods.createCHR('sample2');
        insert chr1;
        insert chr2;
    
        List<CHR_ChangeReq__c> chrs = new  List<CHR_ChangeReq__c>();
        chrs.add(chr1);
        chrs.add(chr2);
        PageReference vfPage = Page.CHR_CurrentActor;
        Test.setCurrentPage(vfPage);
        ApexPages.currentPage().getParameters().put('retURL', '/006');
        
		ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(chrs);
		
		CHR_CurrentActorController controller = new CHR_CurrentActorController(ssc);
		controller.updateCurrentActor();
		
		ApexPages.StandardController sc = new ApexPages.StandardController(chr1);
		CHR_CurrentActorController controller2 = new CHR_CurrentActorController(sc);
		controller.updateCurrentActor();
    }
}