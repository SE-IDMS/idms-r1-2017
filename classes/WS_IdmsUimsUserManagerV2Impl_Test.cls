/*
* Test for WS_IdmsUimsUserManagerV2Impl and WS_IdmsUimsUserManagerV2Service
* */

@isTest
Public class WS_IdmsUimsUserManagerV2Impl_Test {
    //Test1 : Calling setpassword method from WS_IdmsUimsUserManagerV2Impl
    static testmethod void mockres(){
        try{       
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            tes.setPassword('callerFid','authentificationToken','password');
        }
        catch (exception e){}        
    }
    //Test2: Creating instance for all subclass WS_IdmsUimsUserManagerV2Service 
    static testmethod void mockres2(){
        try{
            WS_IdmsUimsUserManagerV2Service.acceptUserMergeResponse acept= new WS_IdmsUimsUserManagerV2Service.acceptUserMergeResponse();
            WS_IdmsUimsUserManagerV2Service.rejectUserMergeResponse rej= new WS_IdmsUimsUserManagerV2Service.rejectUserMergeResponse();
            WS_IdmsUimsUserManagerV2Service.updatePasswordResponse updatedres= new WS_IdmsUimsUserManagerV2Service.updatePasswordResponse();
            WS_IdmsUimsUserManagerV2Service.accessTree act= new WS_IdmsUimsUserManagerV2Service.accessTree();
            WS_IdmsUimsUserManagerV2Service.updateUserResponse updres= new WS_IdmsUimsUserManagerV2Service.updateUserResponse();
            WS_IdmsUimsUserManagerV2Service.program prg= new WS_IdmsUimsUserManagerV2Service.program();
            WS_IdmsUimsUserManagerV2Service.setPasswordResponse setped= new WS_IdmsUimsUserManagerV2Service.setPasswordResponse();
            WS_IdmsUimsUserManagerV2Service.activateIdentityResponse resend= new WS_IdmsUimsUserManagerV2Service.activateIdentityResponse();
            WS_IdmsUimsUserManagerV2Service.getProgramsResponse prs= new WS_IdmsUimsUserManagerV2Service.getProgramsResponse();
            WS_IdmsUimsUserManagerV2Service.ids_element ids= new WS_IdmsUimsUserManagerV2Service.ids_element();
            WS_IdmsUimsUserManagerV2Service.entry_element  enty= new WS_IdmsUimsUserManagerV2Service.entry_element();
            WS_IdmsUimsUserManagerV2Service.getUser getuser= new WS_IdmsUimsUserManagerV2Service.getUser();
            WS_IdmsUimsUserManagerV2Service.updatePassword uppwd= new WS_IdmsUimsUserManagerV2Service.updatePassword();
            WS_IdmsUimsUserManagerV2Service.setPassword  pwd= new WS_IdmsUimsUserManagerV2Service.setPassword();
            WS_IdmsUimsUserManagerV2Service.identity  iden= new WS_IdmsUimsUserManagerV2Service.identity();
            WS_IdmsUimsUserManagerV2Service.requestEmailChange res= new WS_IdmsUimsUserManagerV2Service.requestEmailChange();
            WS_IdmsUimsUserManagerV2Service.updateEmail res1= new WS_IdmsUimsUserManagerV2Service.updateEmail();
            WS_IdmsUimsUserManagerV2Service.userV5 res3= new WS_IdmsUimsUserManagerV2Service.userV5();
            WS_IdmsUimsUserManagerV2Service.getAccessControl res4= new  WS_IdmsUimsUserManagerV2Service.getAccessControl();
            WS_IdmsUimsUserManagerV2Service.accessList_element res5= new WS_IdmsUimsUserManagerV2Service.accessList_element();
            WS_IdmsUimsUserManagerV2Service.updateUser  res6=new WS_IdmsUimsUserManagerV2Service.updateUser();
            WS_IdmsUimsUserManagerV2Service.officialsIds res7= new WS_IdmsUimsUserManagerV2Service.officialsIds();
            WS_IdmsUimsUserManagerV2Service.getAccessControlResponse res8= new WS_IdmsUimsUserManagerV2Service.getAccessControlResponse();
            WS_IdmsUimsUserManagerV2Service.activateIdentity res9= new WS_IdmsUimsUserManagerV2Service.activateIdentity();
            WS_IdmsUimsUserManagerV2Service.accessElement res10= new WS_IdmsUimsUserManagerV2Service.accessElement();      
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes= new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();       
            tes.activateIdentity('zzz','ttt','yyy');       
            
        }
        catch (exception e){}
    }
    // Test3: calling requestemailchange method from WS_IdmsUimsUserManagerV2Impl
    static testmethod void mockres3(){
        try{
            WS_IdmsUimsUserManagerV2Service.accessElement res10= new WS_IdmsUimsUserManagerV2Service.accessElement();
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            tes.requestEmailChange('callerFid','samlAssertion',res10,'email');
        }
        catch (exception e){}
    }
    // Test4 : calling acceptUserMerge method from WS_IdmsUimsUserManagerV2Impl
    static testmethod void mockres4(){
        try{
            // WS_IdmsUimsUserManagerV2Service.accessElement res10= new WS_IdmsUimsUserManagerV2Service.accessElement();
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            tes.acceptUserMerge('callerFid','samlAssertion');
        }
        catch (exception e){}
    }
    //Test5 : calling rejectUserMerge method from WS_IdmsUimsUserManagerV2Impl
    static testmethod void mockres5(){
        try{
            // WS_IdmsUimsUserManagerV2Service.accessElement res10= new WS_IdmsUimsUserManagerV2Service.accessElement();
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            tes.rejectUserMerge('callerFid','samlAssertion');
        }
        catch (exception e){}
    }
    //Test6 : calling updateEmail method from WS_IdmsUimsUserManagerV2Impl
    static testmethod void mockres6(){
        try{
            // WS_IdmsUimsUserManagerV2Service.accessElement res10= new WS_IdmsUimsUserManagerV2Service.accessElement();
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            tes.updateEmail('callerFid','samlAssertion');
        }
        catch (exception e){}
    }
    //Test7 : calling updatePassword method from WS_IdmsUimsUserManagerV2Impl
    static testmethod void mockres7(){
        try{
            // WS_IdmsUimsUserManagerV2Service.accessElement res10= new WS_IdmsUimsUserManagerV2Service.accessElement();
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            tes.updatePassword('callerFid','samlAssertion','oldPassword','newPassword');
        }
        catch (exception e){}
    }
    //Test8 : calling updateUser method from WS_IdmsUimsUserManagerV2Impl
    static testmethod void mockres8(){
        try{
            WS_IdmsUimsUserManagerV2Service.userV5 res10= new WS_IdmsUimsUserManagerV2Service.userV5();
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            tes.updateUser('callerFid','samlAssertion',res10);
        }
        catch (exception e){}
    }
    //Test9 : calling getUser method from WS_IdmsUimsUserManagerV2Impl
    static testmethod void mockres9(){
        try{
            WS_IdmsUimsUserManagerV2Service.userV5 res10= new WS_IdmsUimsUserManagerV2Service.userV5();
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            tes.getUser('callerFid','samlAssertion');
        }
        catch (exception e){}
    }
    //Test10 : calling getPrograms method from WS_IdmsUimsUserManagerV2Impl
    static testmethod void mockres10(){
        try{
            WS_IdmsUimsUserManagerV2Service.userV5 res10= new WS_IdmsUimsUserManagerV2Service.userV5();
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            tes.getPrograms('callerFid','samlAssertion');
        }
        catch (exception e){}
    }
    //Test11 : calling getAccessControl method from WS_IdmsUimsUserManagerV2Impl
    static testmethod void mockres11(){
        try{
            WS_IdmsUimsUserManagerV2Service.userV5 res10= new WS_IdmsUimsUserManagerV2Service.userV5();
            WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort tes = new WS_IdmsUimsUserManagerV2Impl.UserManager_UIMSV2_ImplPort();
            tes.getAccessControl('callerFid','samlAssertion');
        }
        catch (exception e){}
    }
}