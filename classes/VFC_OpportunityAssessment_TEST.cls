@isTest
private class VFC_OpportunityAssessment_TEST{
    static testMethod void OpportunityAssessmentTest() {
    //Start of Test Data preparation
       
          
        Account acc= Utils_TestMethods.createAccount();
        insert acc;
        Opportunity opp=Utils_TestMethods.createOpenOpportunity(acc.id);
        insert opp;
        Assessment__c ass = new Assessment__c();
        ass.Name = 'Test';
        ass.LeadingBusiness__c = 'BD';
        ass.Description__c = 'Test Data';
        ass.MaximumScore__c = 50;
        ass.Active__c = true;
        insert ass;
                
        OpportunityAssessmentQuestion__c oppAssQue = new OpportunityAssessmentQuestion__c();
        OppAssQue.QuestionDescription__c = 'Test';
        OppAssQue.QuestionDescription__c = 'Test description';
        OppAssQue.Sequence__c = 1;
        OppAssQue.Weightage__c = 20;
        OppAssQue.Assessment__c = ass.id;
        OppAssQue.AnswerType__c = 'Picklist';
        insert OppAssQue;
        
        OpportunityAssessmentQuestion__c oppAssQue1 = new OpportunityAssessmentQuestion__c();
        OppAssQue1.QuestionDescription__c = 'Test1';
        OppAssQue1.QuestionDescription__c = 'Test description';
        OppAssQue1.Sequence__c = 2;
        OppAssQue1.Assessment__c = ass.id;
        OppAssQue1.AnswerType__c = 'Text';
        insert OppAssQue1;
        
        OpportunityAssessmentAnswer__c  oppAssAns = new OpportunityAssessmentAnswer__c();
        oppAssAns.AnswerOption__c = 'Test Ans';
        oppAssAns.CommentDescription__c = 'Test Data';
        oppAssAns.Score__c = 40;
        oppAssAns.Sequence__c = 1;
        oppAssAns.ShowComments__c = true;
        oppAssAns.Question__c = OppAssQue.id;
        insert oppAssAns;      
            
        
        OpportunityAssessmentAnswer__c  oppAssAns1 = new OpportunityAssessmentAnswer__c();
        oppAssAns1.AnswerOption__c = 'Test Ans1';
        oppAssAns1.CommentDescription__c = 'Test Data';
        oppAssAns1.Score__c = 40;
        oppAssAns1.Sequence__c = 1;
        oppAssAns1.ShowComments__c = true;
        oppAssAns1.Question__c = OppAssQue.id;
        insert oppAssAns1;
        
        OpportunityAssessmentAnswer__c  oppAssAns2 = new OpportunityAssessmentAnswer__c();
        oppAssAns2.AnswerOption__c = 'Test Ans2';
        oppAssAns2.CommentDescription__c = 'Test Data';
        //oppAssAns2.Score__c = 40;
        oppAssAns2.Sequence__c = 1;
        oppAssAns2.ShowComments__c = true;
        oppAssAns2.Question__c = OppAssQue1.id;
        insert oppAssAns2;
         
        OpportunityAssessment__c oppAssesment = new OpportunityAssessment__c();
        oppAssesment.Assessment__c = ass.id;
        oppAssesment.Opportunity__c = opp.id;
        insert oppAssesment ;
        
         PageReference pageRef = Page.VFP_OpportunityAssessment;
         Test.setCurrentPage(pageRef);
         ApexPages.currentPage().getParameters().put(System.Label.CLSEP12SLS08, opp.id);
         ApexPages.StandardController ssc = new ApexPages.StandardController(new OpportunityAssessment__c() );
         VFC_OpportunityAssessment OppAss = new VFC_OpportunityAssessment(ssc); 
         OppAss.getOpportunityRec();
         
         OppAss.SelectedLB = 'BD';
         OppAss.populateAssessment();
         OppAss.SelectedAssment= ass.id;
         OppAss.populateQuestions();
         for(VFC_OpportunityAssessment.QueAnsWrapper qa:  OppAss.oppAssmentData){
             if(qa.isPickList)
                 qa.selectedAnswer = '--None--';
             if(qa.isText) 
                 qa.tAnswer = 'asdf';   
                 
         }
         OppAss.dosave();
         OppAss.doEdit();
         for(VFC_OpportunityAssessment.QueAnsWrapper qa:  OppAss.oppAssmentData){
             if(qa.questionId ==OppAssQue.id )
                 qa.selectedAnswer = oppAssAns2.id;
              
                 
         }
         OppAss.onPickChange();
         OppAss.dosave();
         OppAss.doEdit();
         for(VFC_OpportunityAssessment.QueAnsWrapper qa:  OppAss.oppAssmentData){
             if(qa.questionId ==OppAssQue.id )
                 qa.selectedAnswer = '--None--';
              
                 
         }
         OppAss.dosave();
         
         
        OppAss.doCancel();
       
        
       Assessment__c  ass2= Utils_TestMethods.createAssessment();
       ass2.Name = 'Test';
       insert ass2;
       Account acc2 = Utils_TestMethods.createAccount();
       insert acc2;
       Opportunity  opp2 = Utils_TestMethods.createOpenOpportunity(acc2.id);
       insert opp2;
       OpportunityAssessmentQuestion__c  oppAssQue2=Utils_TestMethods.createOpportunityAssessmentQuestion(ass2.id,'Picklist',1);
       insert oppAssQue2;
       OpportunityAssessmentAnswer__c oppAssAns3 = Utils_TestMethods.createOpportunityAssessmentAnswer(oppAssQue2.id);
       insert oppAssAns3 ;
       OpportunityAssessmentAnswer__c oppAssAns4 = Utils_TestMethods.createOpportunityAssessmentAnswer(oppAssQue.id);
       oppAssAns4.ShowComments__c = true;
       insert oppAssAns4 ;
       OpportunityAssessment__c oppAss2=Utils_TestMethods.createOpportunityAssessment(opp2.id,ass2.id);
       insert oppAss2;
       OpportunityAssessmentResponse__c  oppAssRes2 = Utils_TestMethods.createOpportunityAssessmentResponse(oppAssQue2.id,oppAssAns3.id,oppAss2.id);
       insert oppAssRes2 ;
       ApexPages.StandardController ssc2 = new ApexPages.StandardController(oppAss2);
      // VFC_OpportunityAssessment vfOppAss2 = new VFC_OpportunityAssessment(ssc2); 
        
         
        
    }
    static testMethod void OpportunityAssessmentTest2() {
       Assessment__c  ass= Utils_TestMethods.createAssessment();
       ass.LeadingBusiness__c ='IT';
       ass.name = 'Test AssessMent';
       insert ass;
       Account acc = Utils_TestMethods.createAccount();
       insert acc;
       Opportunity  opp = Utils_TestMethods.createOpenOpportunity(acc.id);
       insert opp;
       OpportunityAssessmentQuestion__c  oppAssQue=Utils_TestMethods.createOpportunityAssessmentQuestion(ass.id,'Picklist',1);
       insert oppAssQue;
       OpportunityAssessmentAnswer__c oppAssAns = Utils_TestMethods.createOpportunityAssessmentAnswer(oppAssQue.id);
       insert oppAssAns ;
       OpportunityAssessmentAnswer__c oppAssAns2 = Utils_TestMethods.createOpportunityAssessmentAnswer(oppAssQue.id);
       oppAssAns2.ShowComments__c = true;
       insert oppAssAns2 ;
       OpportunityAssessmentQuestion__c  oppAssQue2=Utils_TestMethods.createOpportunityAssessmentQuestion(ass.id,'Text',1);
       oppAssQue2.Weightage__c=null;
       insert oppAssQue2;
       OpportunityAssessmentAnswer__c oppAssAns4 = Utils_TestMethods.createOpportunityAssessmentAnswer(oppAssQue2.id);
       oppAssAns4.Score__c = null;
       insert oppAssAns4 ;
       OpportunityAssessment__c oppAss=Utils_TestMethods.createOpportunityAssessment(opp.id,ass.id);
       insert oppAss;
       
       OpportunityAssessmentResponse__c  oppAssRes = Utils_TestMethods.createOpportunityAssessmentResponse(oppAssQue.id,oppAssAns.id,oppAss.id);
       insert oppAssRes ;
        PageReference pageRef = Page.VFP_OpportunityAssessment;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(System.Label.CLSEP12SLS08, opp.id);
        ApexPages.StandardController ssc = new ApexPages.StandardController(oppAss );
        VFC_OpportunityAssessment OppAss2 = new VFC_OpportunityAssessment(ssc); 
        OppAss2.getOppAssessment();
        OppAss2.doEdit();
        
            for(VFC_OpportunityAssessment.QueAnsWrapper w:  OppAss2.oppAssmentData){
                if(w.questionId == oppAssQue.id){
                    w.selectedAnswer = oppAssAns2.id;
                    w.isShowComDesc = true;
                    w.commentAnswer = 'test';
                
                }
            }
            OppAss2.queId = oppAssQue.id;
            OppAss2.onPickChange();
            
            
            OppAss2.doSave();
            
            PageReference pageRef2 = Page.VFP_OpportunityAssessment;
        Test.setCurrentPage(pageRef2);
        ApexPages.currentPage().getParameters().put(System.Label.CLSEP12SLS08, opp.id);
        ApexPages.StandardController ssc2 = new ApexPages.StandardController(oppAss );
        VFC_OpportunityAssessment OppAss3 = new VFC_OpportunityAssessment(ssc); 
            
            OppAss2.doEdit();
            for(VFC_OpportunityAssessment.QueAnsWrapper w:  OppAss2.oppAssmentData){
                if(w.questionId == oppAssQue.id){
                    w.selectedAnswer = oppAssAns.id;
                    w.isShowComDesc = false;
                    //w.commentAnswer = 'test';
                
                }
            }
             OppAss2.queId = oppAssQue.id;
            OppAss2.onPickChange();
             OppAss2.doSave();
             for(VFC_OpportunityAssessment.QueAnsWrapper w:  OppAss2.oppAssmentData){
                if(w.questionId == oppAssQue.id){
                    w.selectedAnswer = System.Label.CL00355;
                   // w.isShowComDesc = false;
                    //w.commentAnswer = 'test';
                
                }
            }
             OppAss2.doSave();
       
    }
     static testMethod void OpportunityAssessmentTestNegativie() {
       Account acc = Utils_TestMethods.createAccount();
       insert acc;
       Opportunity  opp = Utils_TestMethods.createOpenOpportunity(acc.id);
       insert opp;
        PageReference pageRef = Page.VFP_OpportunityAssessment;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put(System.Label.CLSEP12SLS08, opp.id);
        ApexPages.StandardController ssc = new ApexPages.StandardController(new  OpportunityAssessment__c());
        VFC_OpportunityAssessment OppAss2 = new VFC_OpportunityAssessment(ssc); 
        OppAss2.SelectedLB = System.Label.CL00355;
        OppAss2.populateAssessment();
     }
}