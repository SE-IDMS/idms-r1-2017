global class IDMSidpChainingJitHandler implements Auth.SamlJitHandler {
    
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,String federationIdentifier, Map<String, String> attributes, String assertion) {
        system.debug('jit handler create user started with fed id : '+federationIdentifier );
        User idpUser = IdmsUimsUserSync.createUserSaml(federationIdentifier, 'Work'); 
          system.debug('idpUser : '+idpUser );
        return idpUser ;
    }
    
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,String federationIdentifier, Map<String, String> attributes, String assertion) {
        system.debug('jit handler update user with fed id: '+federationIdentifier);
    }
    
}