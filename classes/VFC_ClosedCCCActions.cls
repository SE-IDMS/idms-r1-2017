public class VFC_ClosedCCCActions {

//*********************************************************************************
// Controller Name  : VFP_ClosedCCCActions 
// Purpose          : Controller class for VFP_ClosedCCCActions
// Created by       : Global Delivery Team
// Date created     : 18th June 2012
// Modified by      :
// Date Modified    :
// Remarks          : For  Release
///********************************************************************************/
    
    List<CCCAction__c> Actionlist=new List<CCCAction__c>();
    
    final Integer  RECORD_COUNT= Integer.valueOf(Label.CLSEP12CCC10);
    
    /*============================================================================
        C.O.N.S.T.R.U.C.T.O.R
    =============================================================================*/    
    public VFC_ClosedCCCActions (){
        Utils_SDF_Methodology.log('START constructor Execution  ');  
        Utils_SDF_Methodology.startTimer();
        Integer Count=0;         
              
        for(CCCAction__c objCCCAction: [Select Id, ActionType__c, Status__c, Case__c, Case__r.CaseNumber, Priority__c, ActionOwner__c,ActionOwner__r.Name, DueDate__c, CCCTeam__c, CCCTeam__r.Name , CreatedById, CreatedBy.Name from CCCAction__c  where status__c =: 'Closed' AND CreatedById =: UserInfo.getUserId()  order by DueDate__c DESC limit :RECORD_COUNT] ){            
            Actionlist.add(objCCCAction);                            
        }
              
        userName=UserInfo.getFirstName()+' '+UserInfo.getLastName();
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END constructor Execution ');
        Utils_SDF_Methodology.Limits();
    }
   
    public List<CCCAction__c> getCCCActions(){
        return Actionlist;
    }
    
    public String userName{get;set;}    
}