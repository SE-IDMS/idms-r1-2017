//*******************************************************************************************
//Created by      : Ranjith
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This class is handling the confirmatin PIN Response from external system
//
//*******************************************************************************************

global class IDMSConfirmPinResponse{
    public String Message;  
    public String Status; 
    public string Federation_Id;
    public string Id;       
    
    //Constructor per default
    public IDMSConfirmPinResponse(){}
    
    //Constructor of a confirm Pin response
    public IDMSConfirmPinResponse(String Status,String Message,String FederationId,String SalesforceId){
        this.Status         = Status;
        this.Message        = Message;
        this.Federation_Id  = FederationId;
        this.Id             = SalesforceId;
    }
}