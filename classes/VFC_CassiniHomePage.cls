global class VFC_CassiniHomePage {
    public String strFaq {get;set;}
    public String strError {get;set;}
    public static List<Package__c> lstPackage ;
    public static List<Contract> lstContracts;
    @RemoteAction  
    public static List<Case> getOpenCases(){ 
      List<Case> lstCases;
      List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
         if(lstContactId.size()>0){ 
             lstCases=[SELECT CaseNumber,CreatedDate,Description,Family_lk__r.Name,Id,LastModifiedDate,Status,CustomerRequest__c,ActionNeededFrom__c FROM Case where ContactID=:lstContactId[0].ContactId and Status!='Closed' and RelatedContract__r.WebAccess__c =: 'Cassini' ORDER BY LastModifiedDate DESC Limit 200]; 
         }
         return lstCases;
    }
    
    @RemoteAction  
    public static List<Case> getClosedCases(){ 
      List<Case> lstCases;
      List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
         if(lstContactId.size()>0){ 
             lstCases=[SELECT CaseNumber,CreatedDate,Description,Family_lk__r.Name,Id,LastModifiedDate,Status,CustomerRequest__c,ActionNeededFrom__c FROM Case where ContactID=:lstContactId[0].ContactId and Status='Closed' and RelatedContract__r.WebAccess__c =: 'Cassini' ORDER BY LastModifiedDate DESC Limit 200]; 
         }
         return lstCases;
    }
    
    @RemoteAction  
    public static List<Case> getSearchedCases(String searchCase){ 
      String strSearchKeyword='%'+searchCase+'%';
      List<Case> lstCases;
      List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
         if(lstContactId.size()>0){ 
             lstCases=[SELECT CaseNumber,CreatedDate,Description,Family_lk__r.Name,Id,LastModifiedDate,Status,CustomerRequest__c,ActionNeededFrom__c FROM Case where ContactID=:lstContactId[0].ContactId and (CaseNumber LIKE :strSearchKeyword) and RelatedContract__r.WebAccess__c =: 'Cassini' ORDER BY LastModifiedDate DESC Limit 200]; 
         }
         return lstCases;
    }  
    
    public PageReference  sendToFaqPage(){
      Integer contractDays =0 ;
      List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){ List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){ List<Contract> lstContractCheck=[SELECT Id,Points__c,EndDate FROM Contract Where Id=:lstCVCPs[0].Contract__c and WebAccess__c =: 'Cassini' LIMIT 1];
               if(!lstContractCheck.isEmpty()){ if(!Test.isRunningTest()) contractDays=System.Today().daysbetween(lstContractCheck[0].EndDate);
                }
            }
        }
        PageReference  pf = new PageReference('/cassini/apex/VFP_CassiniFAQPage');
        pf.getParameters().put('Faq',strFaq);
        if(contractDays==0){ pf.getParameters().put('contractDays','No');
        }
        else{ pf.getParameters().put('contractDays','Yes');
        }
        pf.setRedirect(true);
        return pf; 

    }
    public void checkContract(){
      lstContracts = new  List<Contract >();
      Integer contractDays =0;
      lstPackage  = new List<Package__c>();
        List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){ 
            List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){ 
               lstContracts=[SELECT Id,Points__c,EndDate,Status FROM Contract Where Id=:lstCVCPs[0].Contract__c and WebAccess__c =: 'Cassini' LIMIT 1];
               if(!lstContracts.isEmpty()){ if(!Test.isRunningTest()) contractDays=System.Today().daysbetween(lstContracts[0].EndDate); lstPackage=[SELECT PackageTemplate__c,PackageTemplate__r.PortalAccess__c,PackageTemplate__r.PointOfContact__c,PackageTemplate__r.PointOfContact__r.Priority1Routing__c,PackageTemplate__r.PointOfContact__r.DefaultRoutingTeam__c,PackageTemplate__r.PointOfContact__r.Organization__c FROM Package__c WHERE Contract__c = :lstContracts[0].Id AND PackageTemplate__r.PortalAccess__c = TRUE AND PackageTemplate__r.PointOfContact__r.ChannelType__c ='Portal' ORDER BY LastModifiedDate DESC LIMIT 1]; 
               }
            }
        }
        if(lstContracts.isEmpty()){ strError ='no_access'; return ; }
        else if(!lstPackage.isEmpty()  &&( lstContracts[0].Status =='Grace' ||(lstContracts[0].Status =='Activated' &&  system.now() < lstContracts[0].EndDate.addDays(30)))){ strError ='unableFaq_casecreate'; }
        else if(lstPackage.isEmpty()  &&( lstContracts[0].Status =='Grace' ||(lstContracts[0].Status =='Activated' &&  system.now()< lstContracts[0].EndDate.addDays(30)))){ strError = 'unablefaq'; }
        else { strError ='no_access'; }
               
     }/*
     global class ResetPasswordResult {
        global Boolean Success { get; set; }
        global String ErrorMsg { get; set; }
    }
     @RemoteAction
    global static ResetPasswordResult ResetPassword(string emailAddress) {
        User usr;
        String bfoUName = emailAddress + Label.CLMAR13PRM05;
        ResetPasswordResult result = new ResetPasswordResult();
        string federatedId='';
        try {
            result.Success = false;
            List<User> lUsr = new List<User>([SELECT Id, federationIdentifier, email FROM USER WHERE Username = :bfoUName AND IsActive = true LIMIT 1]);
            if (!lUsr.isEmpty()) {
                usr = lUsr[0];
                federatedId = (usr != null ? usr.FederationIdentifier : '');
            }
            else {
                uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort uimsPort = new
                                        uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
                uimsPort.timeout_x = 120000;
                uimsPort.clientCertName_x = System.Label.CLAPR15PRM018;
                uimsPort.endpoint_x = System.Label.CLAPR15PRM017;
                uimsv2ServiceImsSchneiderComAUM.userFederatedIdAndType srchUsrResult = uimsPort.searchUser(System.Label.CLAPR15PRM016, emailAddress);
                System.debug('Search Result: ' + srchUsrResult);
                federatedId = srchUsrResult.federatedId;
            }
            
            if(String.isNotBlank(federatedId)) {
                
                uimsv2ServiceImsSchneiderComAUM.accessElement application = new uimsv2ServiceImsSchneiderComAUM.accessElement();
                application.type_x = System.label.CLAPR15PRM020;//'APPLICATION';
                application.id = System.label.CLAPR15PRM022;//'prm';
                
                uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort abc = new uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
                abc.endpoint_x = System.label.CLAPR15PRM017; //'https://ims-sqe.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/2WAuthenticated-UserManager';
                abc.timeout_x = Integer.valueOf(System.label.CLAPR15PRM021);//120000; 
                abc.clientCertName_x = System.label.CLAPR15PRM018;//'Certificate_IMS';
                String resetStatus = abc.resetPassword(System.label.CLAPR15PRM019, federatedId, application);
                System.debug('*** Password reset status:' + resetStatus);
                result.Success = true;
            } else {
                result.ErrorMsg = 'Invalid email address';
            }

        } catch (Exception e) {
            System.debug('Error:' + e.getMessage() + e.getStackTraceString());
        }
        return result;
    }*/
    
}