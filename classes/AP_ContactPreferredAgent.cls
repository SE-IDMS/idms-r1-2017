public with sharing class AP_ContactPreferredAgent
{
   /*
        >> BR-BR-2236. MAY 2013 Account / Contact Release <<
        - This function will auto-copy the Account 'Preferred Agent' and 'Preferred Agent Back Up' to Contact.
        - Copy the value on creation of contact only.
        
        >> MODIFIED FOR BR-3065. OCT 2013 Account / Contact Release <<
        - This function will auto-copy the Account 'Service Contract Type' to Contact, if its value is VIC .
        - Copy the value on creation of contact only. 
        
        >> MODIFIED FOR BR-4081. APRIL 2014 Account / Contact Release <<
        - This BR cancels the development done for BR-3065
        - Avoid duplicating Service Contract Type "VIC" on Contact, when creating Contact From Account
        
    */
   public static void insertPreferredAgent(List<Contact> lstCon)
    {  
        Map<ID,List<Contact>> mapContacts=new Map<ID,List<Contact>>();
        List<ID> lstAccIds=new List<ID>();

        for(Contact con : lstCon)
        {            
           if(con.AccountId != null && ( con.Pagent__c == null || con.PBAAgent__c == null || con.ServiceContractType__c == null ))
           {
               if(mapContacts.containsKey(con.AccountId))
               {                    
                    mapContacts.get(con.AccountId).add(con);                 
               }
               else
               {
                    mapContacts.put(con.AccountId, new list<Contact>{con});
                    lstAccIds.add(con.AccountId);
               }
           }           
        }
        if(!mapContacts.isEmpty())
        {           
            String qry= 'select id,Pagent__c,PBAAgent__c,ServiceContractType__c from Account where ID IN :lstAccIds limit '+ Limits.getLimitQueryRows();
            for(Account acc : Database.query(qry))
            {                 
                List<Contact> lstConTemp = mapContacts.get(acc.Id);
                for(Contact con2 : lstConTemp)
                {   
                    if(acc.Pagent__c != null && con2.Pagent__c == null)
                    {
                        con2.Pagent__c= acc.Pagent__c;
                    }
                    if(acc.PBAAgent__c != null && con2.PBAAgent__c == null)
                    {
                        con2.PBAAgent__c= acc.PBAAgent__c;
                    } 
                    
        /* 
            Commented for BR-4081 of April 2014 Release
                    if(acc.ServiceContractType__c == System.Label.CLMAY13CCC08 && con2.ServiceContractType__c == null)    // CLMAY13CCC08: value is VIC 
                    {
                        con2.ServiceContractType__c= acc.ServiceContractType__c;
                    }
        */
                }                 
            }
        }        
    }
}