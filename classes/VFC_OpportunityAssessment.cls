public with sharing class VFC_OpportunityAssessment {
     //*********************************************************************************
        // Class Name : VFC_OpportunityAssessment
        // Purpose : Controller class To Create Opportunity Assessments and Update 
        // Created by : Hari Krishna - Global Delivery Team
        // Date created : 10th July 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Sep - 12 Release
        ///*********************************************************************************/
    
    Map<String,String> PLValueLabelMap = new Map<String,String>();
    OpportunityAssessment__c oppAssment;
    
    public OpportunityAssessment__c getOppAssessment(){
        return this.oppAssment;
    }
    public List<SelectOption> lBOptions{get;set;}
    public List<SelectOption> AssmentOptions{get;set;}
    public String SelectedLB{get;set;}
    public String SelectedAssment{get;set;}
    public Boolean ShowQuestions{get;set;}
    public List<QueAnsWrapper> oppAssmentData{get;set;}
    public Map<id,OpportunityAssessmentAnswer__c> idopaMap{get;set;} 
    public Map<String,String> idOppAssAnsMap{get;set;}
    public String idOppAssAnsMapKey{get;set;}{}{idOppAssAnsMapKey='';}
    public Boolean editMode{get;set;}
    public Boolean detailMode{get;set;}
    public Boolean isShowEditButton{get;set;}
    public String queId{get;set;}
    Opportunity opp;
    public Boolean disablePicklists{get;set;}
    public Opportunity  getOpportunityRec(){return opp;}
    private boolean hasAccess;{hasAccess=false;}
    
    public VFC_OpportunityAssessment(ApexPages.StandardController controller) {
        
        //Describe the picklist LeadingBusiness__c and Create a Map with value Label
        Schema.DescribeFieldResult F = Assessment__c.LeadingBusiness__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for(Schema.PicklistEntry pe:P){
           
            PLValueLabelMap.put(pe.getValue(),pe.getLabel());
        }
        
        List<String> fieldList = new List<String>();
        fieldList.add('Assessment__c');
        fieldList.add('Assessment__r.LeadingBusiness__c');
        fieldList.add('AssessmentScore__c');
        fieldList.add('Opportunity__c');
        fieldList.add('Opportunity__r.Name');
        fieldList.add('Assessment__r.Name');
        fieldList.add('Assessment__r.MaximumScore__c');
        fieldList.add('AssessmentScore__c');
        fieldList.add('Percentage__c');
        fieldList.add('ORFAssessment__c');
        fieldList.add('PartnerAssessment__c');
        if(!Test.isRunningTest())
        controller.addFields(fieldList);
        this.oppAssment =(OpportunityAssessment__c)controller.getRecord();
        if(oppAssment.ORFAssessment__c == false)
        {
            ShowQuestions =  false;
            idopaMap =  new Map<id,OpportunityAssessmentAnswer__c>();
            idOppAssAnsMap = new Map<String,String>();
            isShowEditButton = true;
            
            oppAssmentData =  new List<QueAnsWrapper>();
            editMode = false;
            if(ApexPages.currentPage().getParameters().get(System.Label.CLSEP12SLS08)  != null  && ApexPages.currentPage().getParameters().get(System.Label.CLSEP12SLS08).length()>0){
                this.opp  = [select id, Name from Opportunity where id =: ApexPages.currentPage().getParameters().get(System.Label.CLSEP12SLS08)  ];
                    
            }
            
            
            if(this.oppAssment.id !=  null){
                 checkAssementAccess();
                  if(hasAccess){
                       isShowEditButton = true;
                  }
                  else{isShowEditButton = false;}
                if(ApexPages.currentPage().getParameters().get('retURL')!= null){
                     
                     editMode  = true;
                     detailMode = false;
                     if(!hasAccess){
                       // ShowQuestions = false;
                           editMode  = false;
                          detailMode = true;
                          isShowEditButton = false;
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,system.Label.CLSEP12SLS39));
                    }
                     
                }
                else{
                    editMode  = false;
                    detailMode = true;
                }
                system.debug('asdf1234'+isShowEditButton);
                lBOptions =new List<SelectOption>();
                AssmentOptions= new List<SelectOption>();
                SelectedLB = PLValueLabelMap.get(this.oppAssment.Assessment__r.LeadingBusiness__c);
                SelectedAssment = this.oppAssment.Assessment__c;
                System.debug('Debug-->>>>'+this.oppAssment.Assessment__r.LeadingBusiness__c);
                System.debug('Debug-->>>>'+PLValueLabelMap.get(this.oppAssment.Assessment__r.LeadingBusiness__c));
                System.debug('Debug-->>>>'+this.oppAssment.Assessment__c);
                System.debug('Debug-->>>>'+this.oppAssment.Assessment__r.Name);
               if(!Test.isRunningTest())
               lBOptions.add(new SelectOption(this.oppAssment.Assessment__r.LeadingBusiness__c ,PLValueLabelMap.get(this.oppAssment.Assessment__r.LeadingBusiness__c)));
               if(!Test.isRunningTest())
               AssmentOptions.add(new SelectOption(this.oppAssment.Assessment__c+'',this.oppAssment.Assessment__r.Name));
                disablePicklists = true;
                
                //editMode  = false;
                ShowQuestions =  true;
                if(this.oppAssment.Opportunity__c != null ){
                    this.opp  = [select id, Name from Opportunity where id =: this.oppAssment.Opportunity__c ];
                    
                }
                prepareOppAssmentData();
                
            }
            else{
                lBOptions =new List<SelectOption>();
                AssmentOptions= new List<SelectOption>();
                lBOptions.add(new SelectOption(System.Label.CL00355,System.Label.CL00355));
                AssmentOptions.add(new SelectOption(System.Label.CL00355,System.Label.CL00355)); 
                Set<String> lbset=new Set<String>();
                for(Assessment__c assMent:[select id,LeadingBusiness__c from Assessment__c]){
                     if(assMent.LeadingBusiness__c != null)
                    lbset.add(assMent.LeadingBusiness__c);               
                }
                for(String s:lbset){
                    System.debug('*****'+s);
                    System.debug('*****'+PLValueLabelMap.get(s));
                    if(PLValueLabelMap.containskey(s))
                    lBOptions.add(new SelectOption(s ,PLValueLabelMap.get(s)));
                }
                
            }
        
        }
    }
    
     public PageReference redirectToPartnerAssessment()
    {
        if(oppAssment.ORFAssessment__c == true)
        {
            PageReference pageRef = new PAgeReference('/apex/VFP_PartnerAssessment?readOnly='+true+'&id='+oppAssment.PartnerAssessment__c+'&retURL='+oppAssment.Opportunity__c);
            pageRef.setRedirect(true);
            return pageRef;
        }
        else 
            return null;
    }
    
    public void populateAssessment(){
        AssmentOptions.clear();
        oppAssmentData.clear();
        ShowQuestions =  false;
        if(SelectedLB!=System.Label.CL00355 ){
            AssmentOptions.add(new SelectOption(System.Label.CL00355,System.Label.CL00355)); 
                for(Assessment__c  a:[select id,Name from Assessment__c where LeadingBusiness__c =:SelectedLB and Active__c = true]){
                    AssmentOptions.add(new SelectOption(a.id+'',a.Name));
                }
        }
        else{AssmentOptions.add(new SelectOption(System.Label.CL00355,System.Label.CL00355)); editMode =  false; }
        
    }
    public void populateQuestions(){
        if(SelectedAssment != System.Label.CL00355 ){
            ShowQuestions =  true;
            editMode = true;
            detailMode =false;                
            prepareOppAssmentData();
        }
        else{
             editMode = false;
        }
    }
    public class QueAnsWrapper{
        public Boolean isPickList{get;set;}        
        public Boolean isText{get;set;}
        public Boolean isShowComment{get;set;}
        public String questionDec{get;set;}
        public List<SelectOption> pickListOptions{get;set;}
        public String selectedAnswer{get;set;}
        public String AnswerResponce{get;set;}
        public String selectedAnswerText{get;set;}
        public String commentAnswer{get;set;}
        public String CommentDescription{get;set;}
        public Boolean isShowComDesc{get;set;}
        public Decimal sequence{get;set;}        
        public String questionId{get;set;}
        public Boolean isShowAnsComment{get;set;}
        public String tAnswerId{get;set;}
        public String tAnswer{get;set;}
   }
   public void prepareOppAssmentData(){
        System.debug('***'+SelectedAssment);
        if(SelectedAssment != System.Label.CL00355){
            idopaMap.clear();
            idOppAssAnsMap.clear();
            idOppAssAnsMapkey ='';
            idOppAssAnsMap.put(System.Label.CL00355,'');
        Map<id,OpportunityAssessmentResponse__c> qIdAnsRes  =  new Map<id,OpportunityAssessmentResponse__c>(); 
        if(this.oppAssment.id != null){
            System.debug('asdf'+this.oppAssment.id);
            Set<id> questionIdSet=new Set<id>();
            List<OpportunityAssessmentResponse__c>  oppAssRes=[select Answer__c,AnswerOption__c,OpportunityAssessment__c,Question__c,QuestionText__c,ResponseText__c from OpportunityAssessmentResponse__c where OpportunityAssessment__c  =:this.oppAssment.id];
            for(OpportunityAssessmentResponse__c oar:oppAssRes){
                qIdAnsRes.put(oar.Question__c, oar);
            }
            System.debug('asdf'+qIdAnsRes);
        }
            
                    oppAssmentData =  new List<QueAnsWrapper>();    
                    
                    Map<id,OpportunityAssessmentQuestion__c> oppAssQueMap =  new Map<id,OpportunityAssessmentQuestion__c>();
                    Map<id,List<OpportunityAssessmentAnswer__c>> qidAnsRecListMap = new Map<id,List<OpportunityAssessmentAnswer__c>>();
                    List<OpportunityAssessmentQuestion__c> oppAssQuestionsList=[select id , AnswerType__c ,Assessment__c,QuestionDescription__c, Sequence__c, Weightage__c from OpportunityAssessmentQuestion__c where Assessment__c =:SelectedAssment order by Sequence__c];
                    System.debug('asdf'+oppAssQuestionsList.size());
                    if(oppAssQuestionsList!= null && oppAssQuestionsList.size()>0){
                        System.debug('asdf');
                        oppAssQueMap.putAll(oppAssQuestionsList);
                        for(OpportunityAssessmentAnswer__c oPPAssAns:[select id, AnswerOption__c ,CommentDescription__c , Question__c ,Score__c, Sequence__c,ShowComments__c from OpportunityAssessmentAnswer__c where Question__c in :oppAssQueMap.keySet() order by Sequence__c]){
                            if(!qidAnsRecListMap.containsKey(oPPAssAns.Question__c)){
                                qidAnsRecListMap.put(oPPAssAns.Question__c,new List<OpportunityAssessmentAnswer__c>());
                                qidAnsRecListMap.get(oPPAssAns.Question__c).add(oPPAssAns);
                            }
                            else{
                                qidAnsRecListMap.get(oPPAssAns.Question__c).add(oPPAssAns);
                            }
                            idopaMap.put(oPPAssAns.id,oPPAssAns);
                            idOppAssAnsMap.put(oPPAssAns.id ,oPPAssAns.AnswerOption__c);
                            idOppAssAnsMapkey +='$'+oPPAssAns.id+'$';
                        }
                       
                        for(OpportunityAssessmentQuestion__c que:oppAssQuestionsList){
                            QueAnsWrapper w = new QueAnsWrapper();
                            w.questionId = que.id;
                            w.questionDec =  que.QuestionDescription__c;
                            w.sequence = que.Sequence__c;
                            w.isShowComment = false;
                            w.isShowAnsComment = false;
                            
                             if(que.AnswerType__c == 'Picklist' ){
                                w.selectedAnswerText = '';
                                w.isPickList = true;
                                w.pickListOptions = new List<SelectOption>();
                                w.pickListOptions.add(new SelectOption(System.Label.CL00355,System.Label.CL00355)); 
                                //preparing Dropdown for the questions
                                if(qidAnsRecListMap.containsKey(que.id) ){
                                    List<OpportunityAssessmentAnswer__c> opAAList = new List<OpportunityAssessmentAnswer__c>();
                                    opAAList=qidAnsRecListMap.get(que.id);      
                                    
                                    for(OpportunityAssessmentAnswer__c opaa: opAAList){
                                        if( opaa.ShowComments__c)
                                         w.isShowAnsComment = true;
                                         w.pickListOptions.add(new SelectOption(opaa.id,opaa.AnswerOption__c)); 
                                    }
                                }
                                if(this.oppAssment.id != null){
                                    
                                        if(qIdAnsRes.Containskey(que.id))
                                        {
                                            w.selectedAnswer = qIdAnsRes.get(que.id).Answer__c;
                                            if(w.selectedAnswer != null){
                                               
                                               // w.AnswerResponce = qIdAnsRes.get(que.id).ResponseText__c;
                                                w.AnswerResponce =  idOppAssAnsMap.get(w.selectedAnswer);
                                                w.selectedAnswerText = qIdAnsRes.get(que.id).ResponseText__c;
                                                if(idopaMap.get(w.selectedAnswer).ShowComments__c == true)
                                                {
                                                    w.commentAnswer = qIdAnsRes.get(que.id).ResponseText__c;
                                                    
                                                    if(idopaMap.get(w.selectedAnswer).ShowComments__c){
                                                        w.isShowComment = true;
                                                        w.isShowComDesc = true;
                                                        w.CommentDescription = idopaMap.get(w.selectedAnswer).CommentDescription__c ;
                                                    }
                                                }
                                            }
                                            else{
                                                w.selectedAnswer = System.Label.CL00355;
                                            }
                                         }
                                }
                                
                             }
                             else{w.isPickList = false;}
                             if(que.AnswerType__c == 'Text'){
                                w.isText =  true;
                                   if(qidAnsRecListMap.containskey(que.id))
                                    w.tAnswerId =qidAnsRecListMap.get(que.id)[0].id;
                                if(this.oppAssment.id != null){
                                     if(qIdAnsRes.ContainsKey(que.id))
                                      w.tAnswer = qIdAnsRes.get(que.id).ResponseText__c;
                                }
                             }
                             else{w.isText = false;}
                             oppAssmentData.add(w);
                        }
                    }
            
        }
   }
   public PageReference onPickChange(){
       
        
        for(QueAnsWrapper  w:oppAssmentData){
            if(w.selectedAnswer != System.Label.CL00355 ){
                if(queId == w.questionId && w.isShowAnsComment== true && idopaMap.get(w.selectedAnswer).ShowComments__c == true ){
                    w.isShowComment = true;
                    w.AnswerResponce = idopaMap.get(w.selectedAnswer).AnswerOption__c;
                    //w.selectedAnswerText = w.commentAnswer;
                    w.isShowComDesc = true;
                    w.CommentDescription = idopaMap.get(w.selectedAnswer).CommentDescription__c ;
                    
                }
                if(queId == w.questionId &&  idopaMap.get(w.selectedAnswer).ShowComments__c == false ){
                    w.isShowComment = false;
                    w.commentAnswer ='';
                    w.AnswerResponce = idopaMap.get(w.selectedAnswer).AnswerOption__c;
                    w.selectedAnswerText = idopaMap.get(w.selectedAnswer).AnswerOption__c;
                    w.isShowComDesc = false;
                    w.CommentDescription = '' ;
                }
                
            }
            else{
                w.isShowComment = false;
                w.commentAnswer ='';
                w.isShowComDesc = false;
                w.CommentDescription = '' ;
            }
            
        }
       
        return null;
    }
    public PageReference doSave(){
              Boolean doInsert =  true;
              Boolean doUpdate = true;
            //On New 
            if(this.oppAssment.id == null){
                if(SelectedAssment != System.Label.CL00355 ){
                         this.oppAssment.Opportunity__c = ApexPages.currentPage().getParameters().get(System.Label.CLSEP12SLS08) ;
                         this.oppAssment.Assessment__c = SelectedAssment; 
                 }
                 Savepoint sp = Database.setSavepoint();
                 insert this.oppAssment;
                 list<OpportunityAssessmentResponse__c> oassResponseList = new List<OpportunityAssessmentResponse__c>();
                if(oppAssmentData != null && oppAssmentData.size()>0){
                    for(QueAnsWrapper w:oppAssmentData ){
                        OpportunityAssessmentResponse__c oPPAssResponse=new OpportunityAssessmentResponse__c();
                        oPPAssResponse.Question__c = w.questionId;
                        oPPAssResponse.OpportunityAssessment__c = this.oppAssment.id;
                        if(w.isPickList  ){
                            if(w.selectedAnswer != System.Label.CL00355){
                             oPPAssResponse.Answer__c = w.selectedAnswer;
                                 if(w.isShowComment ){
                                        //w.selectedAnswerText = idopaMap.get(w.selectedAnswer).AnswerOption__c;
                                         w.selectedAnswerText = w.commentAnswer;
                                     if(w.commentAnswer.trim().length()>0){
                                         oPPAssResponse.ResponseText__c = w.commentAnswer;
                                     }
                                     
                                 }
                                 else{
                                     //oPPAssResponse.ResponseText__c = idopaMap.get(w.selectedAnswer).AnswerOption__c;
                                     w.selectedAnswerText = idopaMap.get(w.selectedAnswer).AnswerOption__c;
                                     oPPAssResponse.ResponseText__c = '';
                                  }
                           }
                           /*
                           else{
                              
                               oPPAssResponse.ResponseText__c = '';
                               
                           }
                           */
                           
                        }
                        if(  w.isText ){
                          if(w.tAnswer.trim().length()>0){  
                              oPPAssResponse.Answer__c = w.tAnswerId;
                              oPPAssResponse.ResponseText__c = w.tAnswer;
                          }
                          else{
                            
                             oPPAssResponse.Answer__c = w.tAnswerId;
                              oPPAssResponse.ResponseText__c ='';
                             
                          }
                        }
                        oassResponseList.add(oPPAssResponse);
                    }
                }
                if(oassResponseList !=  null && oassResponseList.size()>0){
                      //if(doInsert )
                      /*
                      Database.SaveResult[] results=Database.insert(oassResponseList ,false);
                      for (Integer i = 0; i < results.size(); i++) {
                            if (results[i].isSuccess()) {
                            System.debug('Successfully created ID: '
                                  + results[i].getId());
                            } else {
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,results[i].getErrors()[0].getMessage()));
                            System.debug('Error: could not create sobject '
                                  + 'for array element ' + i + '.');
                            System.debug('   The error reported was: '
                                  + results[i].getErrors()[0].getMessage() + '\n');
                            }
                        }
                        */
                      upsert    oassResponseList ;
                }
                
                
            }
            else{
                     List<OpportunityAssessmentResponse__c>  oppAssRes=[select Answer__c,AnswerOption__c,OpportunityAssessment__c,Question__c,QuestionText__c,ResponseText__c from OpportunityAssessmentResponse__c where OpportunityAssessment__c  = :this.oppAssment.id];
                    for(OpportunityAssessmentResponse__c oar:oppAssRes){
                            for(QueAnsWrapper w:oppAssmentData ){
                                if(oar.Question__c == w.questionId){
                                    if(w.isPickList  ){
                                        if(w.selectedAnswer != System.Label.CL00355){
                                            oar.Answer__c = w.selectedAnswer;
                                            if(w.isShowComment ){
                                               w.selectedAnswerText = w.commentAnswer;
                                                if(w.commentAnswer.trim().length()>0){
                                                     oar.ResponseText__c = w.commentAnswer;
                                                }
                                                
                                             }
                                             else{
                                               // oar.ResponseText__c = idopaMap.get(w.selectedAnswer).AnswerOption__c;
                                                oar.ResponseText__c = '';
                                                w.selectedAnswerText = idopaMap.get(w.selectedAnswer).AnswerOption__c;
                                             }
                                          }
                                        else{
                                               
                                               oar.Answer__c = null;
                                               oar.ResponseText__c = '';
                                              
                                           }
                                    }
                                    if(  w.isText ){
                                       
                                             oar.ResponseText__c = w.tAnswer;
                                        
                                         
                                    }
                                }
                            }
                        }
                        //if(doUpdate)
                        update oppAssRes;
                        /*
                        Database.SaveResult[] results=Database.update(oppAssRes,false);
                        for (Integer i = 0; i < results.size(); i++) {
                            if (results[i].isSuccess()) {
                            System.debug('Successfully created ID: '
                                  + results[i].getId());
                            } else {
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,results[i].getErrors()[0].getMessage()));
                            System.debug('Error: could not create sobject '
                                  + 'for array element ' + i + '.');
                            System.debug('   The error reported was: '
                                  + results[i].getErrors()[0].getMessage() + '\n');
                            }
                        }
                */
            }
            
            
          
            if(doUpdate == true && doInsert ==  true ){
                Decimal TotalScore = 0;
                //Map<id,OpportunityAssessmentResponse__c> idoarMap=new Map<id,OpportunityAssessmentResponse__c>();
                //idoarMap.putAll(oassResponseList);
                for(OpportunityAssessmentResponse__c oasres:[select id,Score__c from  OpportunityAssessmentResponse__c where OpportunityAssessment__c  = :this.oppAssment.id ]){
                   
                    if(oasres.Score__c != null){
                        TotalScore +=oasres.Score__c;
                    }
                }
                if(TotalScore  != null){
                   this.oppAssment.AssessmentScore__c = TotalScore  ;
                   /*
                   Database.SaveResult result=Database.update(this.oppAssment,false);
                   if (!result.isSuccess()) 
                   ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,result.getErrors()[0].getMessage()));
                   */
                     update this.oppAssment;
                }
                detailMode =  true;
                editMode = false;
                disablePicklists = true;
            }
        //return null;
        PageReference  pref= new PageReference('/apex/VFP_OpportunityAssessment?id='+this.oppAssment.id);
        pref.setRedirect(true);
        return pref;
    }
    public PageReference doEdit(){
         detailMode =false;
         editMode  = true;
        return null;
    }
    Public PageReference doCancel(){
         PageReference p=new PageReference('/'+this.oppAssment.Opportunity__c);
          p.setRedirect(true);
          return p;
         
    }
    private void checkAssementAccess()
    {
        /*
        Savepoint sp = Database.setSavepoint();
        Database.SaveResult result=Database.update(this.oppAssment,false);
        if(result.isSuccess())
        hasAccess=true;           
        
        Database.rollback(sp);  
        */   
        try{ 
            UserRecordAccess  hasAccessToEdit=[SELECT RecordId FROM UserRecordAccess WHERE RecordId =:this.oppAssment.id  AND UserId = :userinfo.getuserId() AND HasEditAccess = true] ;
            if(hasAccessToEdit!=null){
                hasAccess = true;
            }
            else{hasAccess = false;}
        }  
        catch(Exception ex){
            System.debug('Error : '+ex);
        }  
    }
    
    public PageReference pageLoad(){
       /*
        checkAssementAccess();
        if(this.oppAssment.id!= null){
            if(!hasAccess){
                ShowQuestions = false;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You Have no Access to  Edit this Record'));
            }
        }
        */
        return null;
    }
   
}