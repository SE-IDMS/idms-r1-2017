@istest
private class PRM_Batch_PortalUserListViewUpdate_TEST {
   static testMethod void myUnitTest_manual() {
    
   
    Test.startTest();
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.country__c = country.id;
        acc.ClassLevel1__c = 'Panel Builder';
        acc.ClassLevel2__c = 'Control Panel Makers';
        insert acc;
        acc.PRMAccount__c = true;
        update acc;
        
        CS_PRM_ApexJobSettings__c  cJobSetting = new CS_PRM_ApexJobSettings__c();
        cJobSetting.LastRun__c = Date.today();
        cJobSetting.InitialSync__c = true;
        cJobSetting.Name = 'ParseListViews';
        cJobSetting.ObjectToParse__c = 'Opportunity,Account,OpportunityRegistrationForm__c';
        cJobSetting.RelatedObjects__c = 'OPP_ProductLine__c,OpportunityRegistrationProducts__c,ActivityHistory,OpenActivity,Note';
        insert cJobSetting;
        
        AP_ParseListViewMetadata classInstance = new AP_ParseListViewMetadata();
        
        PRM_Batch_PortalUserListViewUpdate batch_ppsa1 = new PRM_Batch_PortalUserListViewUpdate();
        if([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5)
            Database.executebatch(batch_ppsa1,2);
    }
     static testMethod void myUnitTest_schedule() {
        Test.startTest();
        String CRON_EXP = '0 0 0 1 2 ? 2025';  
        String jobId = System.schedule('PortalUserListViewUpdate', CRON_EXP, new PRM_Batch_PortalUserListViewUpdate() );
        Test.stopTest();
    }
    
    
}