public class VCC14_I2PRR_RIEmailTemplate{

                        
   public string strRRid{get;set;}
   List<Account> accounts;
   List<RMA_Product__c> RMAProduct;

             
        public List<RMA_Product__c> getRMAProduct() {
        
               system.debug('## strrrid ##'+strRRid); 

                if(RMAProduct == null) 
                    RMAProduct = [SELECT name,toLabel(CustomerResolution__c) ,DateCode__c,Quantity__c,ReturnCenter__c,SerialNumber__c,ShippingAddress__c,Return_Center_Address__c, RealCustomerReference__c FROM RMA_Product__c where RMA__c =:strRRid  order by Return_Center_Address__c];
                return RMAProduct;

        }

        

}