@isTest
public class VFC_DownloadData_TEST {
    private static testMethod void downloadData_TEST() {
        App__c objApp = new App__c();
        objApp.Name 				= 'TestAppName';
        objApp.Description__c 		= 'TestAppDescription';
        objApp.logo__c 				= 'https://salesforc.com';
        objApp.ObjectLabel__c 		= 'TestAppObjectLabelA';
        objApp.ObjectPluralLabel__c = 'TestAppObjectLabelAs';
        objApp.ObjectShortcut__c 	= 'XYZ';
        objApp.parentapp__c 		=  null;
        objApp.Status__c 			=  'Approved';
        objApp.wikilink__c			= 'https://mywiki.com';
        insert objApp; 
        
        View__c objView = new View__c();
        objView.Name='MyTestView';
        objView.app__c = objApp.id;
        objView.listviewdefinition__c ='{"conditions":[],"columns":["textfield3__c"]}';        
        
        View__c objView1 = new View__c();
        objView1.Name='MyTestView1';
        objView1.app__c = objApp.id;        
        objView1.listviewdefinition__c ='{"conditions":[{"appdatafieldmap":"textfield3__c",  "displayname": "Text",  "fieldtype": "Text",  "type": "equals",  "value": "sid",  "index": 1}],"overallcondition": "1","columns":["textfield3__c"]}';

        
        View__c objView2 = new View__c();
        objView2.Name='MyTestView2';
        objView2.app__c = objApp.id;        

        insert new List<View__c>{objView,objView1,objView2};
        
        ListViewDefinition mydefinition = new ListViewDefinition();
        mydefinition.initialize();
        
        Step__c objStep = new Step__c();
        objStep.app__c = objApp.Id;
        objStep.stepnumber__c = 1;
        objStep.Name ='testStep one';
        objStep.statusvalueonentrytothisstep__c ='Draft';
        objStep.actionvalueforapprove__c = 'started';
        
        insert objStep;
        
        Field__c objField=new Field__c();
        objField.app__c=objApp.Id;
        objField.step__c=objStep.Id;
        objField.layoutposition__c=1;
        objField.appdatafieldmap__c = 'textfield3__c';
        objField.fieldtype__c ='Picklist';
        objField.required__c=false;
        Insert objField;
        
        ofwgroup__c group1 = new ofwgroup__c();
        group1.Name = 'Test group';
        group1.App__c = objApp.Id;
        Insert group1;
        
        groupappuserjunction__c groupjunction = new groupappuserjunction__c();
        groupjunction.group__c = group1.Id;
        groupjunction.User__c = UserInfo.getUserId();
        insert groupjunction;
        
        objApp.DownloadableGroup__c = group1.Id;
        update objApp;
        
        Record__c record = new Record__c(Name='ofw-123');
        record.step__c = objStep.Id;
        record.app__c = objApp.Id;
        record.textfield3__c='sid';
        record.status__c = 'Draft';
        insert record;
        
        ApexPages.CurrentPage().getparameters().put('id', objView.id);
        ApexPages.StandardController sc = new ApexPages.standardController(objView);
        VFC_DownloadData sic = new VFC_DownloadData(sc);		
        
        ApexPages.CurrentPage().getparameters().put('id', objView1.id);
        ApexPages.StandardController sc2 = new ApexPages.standardController(objView1);
        VFC_DownloadData sic2 = new VFC_DownloadData(sc2);	
        
        ApexPages.CurrentPage().getparameters().put('id', objView2.id);
        ApexPages.StandardController sc3 = new ApexPages.standardController(objView2);
        VFC_DownloadData sic3 = new VFC_DownloadData(sc3);		
                
        AppServices.getDownloadAccess(objApp.Id,group1.Id);
    }
    
}