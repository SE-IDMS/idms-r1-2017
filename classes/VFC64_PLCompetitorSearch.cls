/*    Author          : Accenture Team    
      Date Created    : 24/10/2011     
      Description     : Controller extensions for VFP64_PLCompetitorSearch. */
                        
public class VFC64_PLCompetitorSearch extends VFC_ControllerBase{
    //Iniatialize variables
    private ProductLineCompetitor__c currentComp = new ProductLineCompetitor__c();
    public Utils_DataSource dataSource;
    public List<SelectOption> columns{get;set;}
    public List<DataTemplate__c> fetchedRecords{get;set;}
    public String interfaceType{get;set;}
    public List<String> keywords;
    public List<String> filters;
    public Boolean DisplayResults{get;set;}
    public Utils_DataSource.Result searchResult ;
    Public Utils_PicklistManager plManager;
    public string searchText{get;set;}       
    public String level1{set; get;}
    public String level2{get; set;}
    Public List<String> Labels{get;set;}
    public String level1Label{set; get{return Labels[0];}}
    public String level2Label{set; get{return Labels[1];}}
    public List<SelectOption> items1{set; get;}
    public List<SelectOption> items2{set; get;}
    List<DataTemplate__c> plCompetitors = New List<DataTemplate__c>();
    public Boolean displayError{get;set;}
             
    public VCC06_DisplaySearchResults resultsController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                system.debug('--------displaySearchResults -------'+displaySearchResults );                
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }
     //Constructor
    public VFC64_PLCompetitorSearch(ApexPages.StandardController controller) 
    {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC64_PLCompetitorSearch******');            
        currentComp= (ProductLineCompetitor__c)controller.getRecord();
        keywords = new List<String>();
        filters = new List<String>();
        columns = new List<SelectOption>();
        Labels = new List<String>();
        interfaceType = 'COMP';
        displayError = false;
                
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
        
        dataSource = Utils_Factory.getDataSource(interfaceType);
        columns = new List<SelectOption>();
        columns = dataSource.getColumns();
        plManager = Utils_Factory.getPickListManager(interfaceType);
        
        if( plManager != null)
        {
            Labels = plManager.getPickListLabels();
            items1 = plManager.getPicklistValues(1, null);
            items2 = plManager.getPicklistValues(2, null);
        }
        
        fetchedRecords = new List<DataTemplate__c>();
        System.Debug('****** Initializing the Properties and Variables Ends for VFC64_PLCompetitorSearch******');
    
    
    }             
    
    public PageReference  search() 
    {     
    /* This method will be called on click of "Search" Button.This method calls the factory methods
     * and fetches the search results
     */
       System.Debug('****** Searching Begins  for VFC64_PLCompetitorSearch******');
        if(resultsController.searchResults!=null)
            resultsController.searchResults.clear();

        fetchedRecords.clear();
        keywords = new List<String>();
        filters = new List<String>();
        if(searchText!=null)
            keywords.add(searchText.trim());
        if(level1!=null)
            filters.add(level1);
        if(level2!=null)
            filters.add(level2); 
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
        if(keywords.size()!=0 || filters.size()!=0)
        {
            if(!filters[0].equalsIgnoreCase(Label.CL00355) || !filters[1].equalsIgnoreCase(Label.CL00355) || keywords[0].length()>0)
            {
                try
                {
                    System.debug('#### Filters : ' + filters);
                    //Makes the Datasource call for search                    
                    searchResult = dataSource.Search(keywords,filters);
                    fetchedRecords = searchResult.recordList;
                    System.debug('Data source Result'+fetchedRecords);
                    System.debug(' Result'+searchResult);
                    DisplayResults = true;
                    
                    if(searchResult.NumberOfRecord > 100)
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00349));
                }
                catch(Exception exc)
                {
                    system.debug('Exception while calling Competitor Search '+ exc.getMessage() );
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00398));
                }                
            }
            else if(filters[0].equalsIgnoreCase(Label.CL00355) && filters[1].equalsIgnoreCase(Label.CL00355) && keywords[0].length()==0)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00454));

            }             
        }         
        System.Debug('****** Searching Ends  for VFC64_PLCompetitorSearch******');        
        return null;          
    }
    /* This method will be called on click of "Clear" Button.This method clears the value in the search text box
     * and the values in picklists
     */
    public PageReference clear() 
    {
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Begins  for VFC64_PLCompetitorSearch******');     
        fetchedRecords = new List<DataTemplate__c>();
        searchText = null;
        keywords = new List<String>();
        filters = new List<String>();        
        DisplayResults = false;
        level1 = null;
        level2 = null;
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Ends for VFC64_PLCompetitorSearch******');     
        return null;
    }
    /* This method will be called from the component controller on click of "Select" link.This method correspondingly inserts/updates
     * the competitor line.
     */
    public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
    {
        System.Debug('****** Insertion of product line competitor begins******');
        
        VFC64_PLCompetitorSearch thisController = (VFC64_PLCompetitorSearch)controllerBase;
        
        
        PageReference newPlCompPage = new PageReference('/'+SObjectType.ProductLineCompetitor__c.getKeyPrefix()+'/e');
                
        if(system.currentpagereference().getParameters().get(CS007_StaticValues__c.getvalues('CS007_11').Values__c)!=null)
        {
            newPlCompPage.getParameters().put(CS007_StaticValues__c.getvalues('CS007_11').Values__c, system.currentpagereference().getParameters().get(CS007_StaticValues__c.getvalues('CS007_11').Values__c));            
        }        
        if(system.currentpagereference().getParameters().get(CS007_StaticValues__c.getvalues('CS007_12').Values__c) != null)
        {
            newPlCompPage.getParameters().put(CS007_StaticValues__c.getvalues('CS007_12').Values__c, system.currentpagereference().getParameters().get(CS007_StaticValues__c.getvalues('CS007_12').Values__c));                       
        }
        
        newPlCompPage.getParameters().put('nooverride', '1');      
        
        //setting the return url
         if (system.currentpagereference().getParameters().get(System.Label.CL00330) != NULL)
            newPlCompPage.getParameters().put(System.Label.CL00330, system.currentpagereference().getParameters().get(System.Label.CL00330));
        else if (system.currentpagereference().getParameters().get(CS007_StaticValues__c.getValues('CS007_12').Values__c)!= NULL)
            newPlCompPage.getParameters().put(System.Label.CL00330, system.currentpagereference().getParameters().get(CS007_StaticValues__c.getValues('CS007_12').Values__c));
        
        if(plCompetitors!=null)
        plCompetitors.add((DataTemplate__c)obj);
        
        for(DataTemplate__c dt : pLCompetitors)
        {
            if(dt.field1__c != null)
                newPlCompPage.getParameters().put(CS007_StaticValues__c.getvalues('CS007_13').Values__c, dt.field1__c);
        }
        String compName = newPlCompPage.getParameters().get(CS007_StaticValues__c.getvalues('CS007_13').Values__c);
        if(compName!=null)
        {
            string query1 = CS005_Queries__c.getValues('CS005_9').StaticQuery__c+' '+'where Name =\''+compName+'\' Limit 1';
            List<Competitor__c> comp = database.query(query1);
            if(comp!=null && !comp.isEmpty())
            newPlCompPage.getParameters().put(CS007_StaticValues__c.getvalues('CS007_14').Values__c,comp[0].Id);
        }        
            
        String pam= system.currentpagereference().getParameters().get(CS007_StaticValues__c.getvalues('CS007_12').Values__c);
        String compid=newPlCompPage.getParameters().get(CS007_StaticValues__c.getvalues('CS007_14').Values__c);        
        if(pam!=null && compid!=null)
        {
            string query2 = CS005_Queries__c.getValues('CS005_10').StaticQuery__c+' '+'where PLCID__c=\''+pam+'\' and Competitor__c=\''+compid+'\'';
            List<ProductLineCompetitor__c> oppComps = database.query(query2);          
                   
            if(oppComps!=null && oppComps.size()>0)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00677));
                newPlCompPage = null;
            }        
        }
        return newPlCompPage;
    }                          
}