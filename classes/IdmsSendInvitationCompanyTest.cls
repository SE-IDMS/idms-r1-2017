//Test class of IdmsSendInvitationCompany
@isTest
class IdmsSendInvitationCompanyTest{
    
    //test method of send invitation company
    static testmethod void testIdmsSendInvitationControllerHome(){
        IdmsTermsAndConditionMapping__c custSets=new IdmsTermsAndConditionMapping__c(Name='IDMS',
                                                                                     url__c='http://www2.schneider-electric.com/sites/corporate/en/general/legal-information/terms-of-use.page');
        insert custSets;
        IDMSApplicationMapping__c appMap = new IDMSApplicationMapping__c(Name='IDMS',context__c = 'home', AppName__c='IDMS');
        insert appMap; 
        ApexPages.currentPage().getParameters().put('app','IDMS');
        IdmsSendInvitationCompanyController controller=new IdmsSendInvitationCompanyController();
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.getCompanyPicklist();
        controller.email='idmstesttoto@accenture.com';
        controller.nextToCompany();
        controller.reset();
        controller.submit();
        controller.submitHome();
        controller.verify();
        ApexPages.currentPage().getParameters().put('recaptcha_challenge_field','True');
        ApexPages.currentPage().getParameters().put('recaptcha_response_field','True');
        
        controller.submitHome();
        controller.verify();
        String strPublicKey=controller.publicKey;
        controller.showCaptcha=false;
        controller.submitHome();
        controller.email='12345test';
        controller.submitHome(); 
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.showCaptcha=true;
        controller.submit();
        controller.Company_State = '001';
        controller.Company_Country = 'FR';
        controller.backToWork();
        controller.email='p@gmail.com';
        controller.MarketSegment = 'ID5';
        controller.AnnualRevenue = '90';
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.showCaptcha=false;
        controller.submitHome();
        controller.email='p@gmail.com';
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.showCaptcha=true;
        controller.submitHome();
        controller.showCaptcha=false;
        controller.submitHome();
    }
    static testmethod void testIdmsSendInvitationControllerWork(){
        IdmsTermsAndConditionMapping__c custSets=new IdmsTermsAndConditionMapping__c(Name='IDMS',
                                                                                     url__c='http://www2.schneider-electric.com/sites/corporate/en/general/legal-information/terms-of-use.page');
        insert custSets;
        IDMSApplicationMapping__c appMap = new IDMSApplicationMapping__c(Name='IDMS',context__c = 'work', AppName__c='IDMS');
        insert appMap; 
        ApexPages.currentPage().getParameters().put('app','IDMS');
        IdmsSendInvitationCompanyController controller=new IdmsSendInvitationCompanyController();
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.getCompanyPicklist();
        controller.email='idmstesttoto@accenture.com';
        controller.nextToCompany();
        controller.reset();
        controller.submit();
        controller.submitHome();
        controller.verify();
        ApexPages.currentPage().getParameters().put('recaptcha_challenge_field','True');
        ApexPages.currentPage().getParameters().put('recaptcha_response_field','True');
        controller.submitHome();
        controller.verify();
        String strPublicKey=controller.publicKey;
        controller.showCaptcha=false;
        controller.submitHome();
        controller.email='12345test';
        controller.submitHome(); 
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.showCaptcha=true;
        controller.submit();
        controller.Company_State = '001';
        controller.Company_Country = 'FR';
        controller.backToWork();
        controller.email='p@gmail.com';
        controller.MarketSegment = 'ID5';
        controller.AnnualRevenue = '90';
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.showCaptcha=false;
        controller.submitHome();
        controller.email='p@gmail.com';
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.showCaptcha=true;
        controller.submitHome();
        controller.showCaptcha=false;
        controller.submitHome();
        controller.mobCntryCode='+91';
        controller.showCaptcha=false;
        controller.submit();
    }
    
}