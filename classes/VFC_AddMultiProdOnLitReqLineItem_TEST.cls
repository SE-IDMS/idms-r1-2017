//*********************************************************************************
// Class Name       : VFC_AddMultipleProductsForLitReqLineItem_TEST
// Purpose          : Test Class for VFC_AddMultipleProductsForLitReqLineItem
// Created by       : Vimal K (Global Delivery Team)
// Date created     : 20th December 2013
// Modified by      :
// Date Modified    :
// Remarks          : For Dec - 13 Release
///********************************************************************************/

@isTest
private class VFC_AddMultiProdOnLitReqLineItem_TEST{
    static testmethod void AddMultiProdOnLitReqLineItemTest(){
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        insert objContact;
        
        Case objCase = Utils_TestMethods.createCase(objAccount.Id,objContact.Id,'Open');
        objCase.CommercialReference__c= 'CommercialReferenceTest';    
        objCase.ProductBU__c = 'Business';    
        objCase.ProductLine__c = 'ProductLine__c';    
        objCase.ProductFamily__c ='ProductFamily';     
        objCase.Family__c ='Family'; 
        objCase.TECH_GMRCode__c ='00112233'; 
        insert objCase;
        
        Literaturerequest__c objLTRRequest = Utils_TestMethods.createLiteratureRequest(objAccount.Id, objContact.Id, objCase.id);    
        insert objLTRRequest;
        
        LiteratureRequestLineItem__c objLTRRequestLineItem = new LiteratureRequestLineItem__c(    
                                                    CommercialReference__c= 'CommercialReferenceTest',    
                                                    Literaturerequest__c = objLTRRequest.Id,    
                                                    Quantity__c = 1);    
        Insert objLTRRequestLineItem;
        
        List<String> keywords = new List<String>();
        Utils_DummyDataForTest dummydata = new Utils_DummyDataForTest();
        Utils_DataSource.Result  tempresult = new Utils_DataSource.Result();
        tempresult = dummydata.Search(keywords,keywords);
        List<DataTemplate__c> dataTemplates = (List<DataTemplate__c>)tempresult.recordList;
        
        
        ApexPages.StandardController objLTRLineItemStandardController = new ApexPages.StandardController(objLTRRequestLineItem); 
        VFC_AddMultipleProductsForLitReqLineItem objLTRLineItemPageController = new VFC_AddMultipleProductsForLitReqLineItem(objLTRLineItemStandardController);
        
        VCC08_GMRSearch productForLTRLineItem = new VCC08_GMRSearch();
        productForLTRLineItem.pageController = objLTRLineItemPageController;
        
        Test.startTest();
        objLTRLineItemPageController.GMRSearchForlitLineItem.Init('TEST',productForLTRLineItem);
        productForLTRLineItem.ActionType = 'multiple';
        objLTRLineItemPageController.GMRSearchForlitLineItem.PerformAction(dataTemplates[0],productForLTRLineItem);
        VFC_AddMultipleProductsForLitReqLineItem.InsertLiteratureRequestLineItem(dataTemplates,objLTRRequest);
        objLTRLineItemPageController.GMRSearchForlitLineItem.Cancel(productForLTRLineItem);
        Test.stopTest();
    }
}