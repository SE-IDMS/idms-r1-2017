global with sharing class VFC73_AddRMAProduct {    
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public RMA_Product__c riRecord{get;set;}
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    public String jsonLevel{get;set;}{jsonLevel='';} 
    public RMA__c relatedRMA{get;set;} 
    Public boolean impossibleToAddProduct{get; set;}
    Public boolean ChangeProduct{get; set;}
   
    public VFC73_AddRMAProduct(ApexPages.StandardController controller){
          
        //riRecord = (RMA_Product__c)controller.getRecord();
        impossibleToAddProduct = False;
        List<RMA_Product__c> RMAProductList = [SELECT ID, RMA__c FROM RMA_Product__c WHERE ID=:controller.getRecord().ID];
        
        if(RMAProductList .size() == 1)
        {
            riRecord = RMAProductList[0];
        }
        else
        {
           // riRecord = (RMA_Product__c)controller.getRecord();
             riRecord=(RMA_Product__c)controller.getRecord(); 
        }
        
        system.debug('**** riRecord  **** '+ riRecord );
        
        // Get the ChP variable from the URL in order to determine the process
        String Mode = system.currentpagereference().getParameters().get('Mode');
                
        if(Mode=='ChP')
        {
            ChangeProduct = true;
        }
        else
        {
            ChangeProduct = false;
        }
         
             
        // Retrieve the ID of the related RMA through the URL
        String RMAID = system.currentpagereference().getParameters().get('retURL');  
        
        if(RMAID != null)
        {
            RMAID = RMAID.substring(1); 
            System.debug('#### RMAID = ' + RMAID);
        }
         
        List<RMA__c> RMAList = [SELECT ID, Name, ProductCondition__c,Status__c, TEX__c,TEX__r.ForceManualSelection__c FROM RMA__c WHERE ID=:RMAID];
        
        System.debug('#### RMALIST (RMAID from retURL) = ' + RMAList);
        
        if(RMAList.size() == 0)
        {
             RMAList = [SELECT ID, Name, ProductCondition__c,Status__c, TEX__c,TEX__r.ForceManualSelection__c FROM RMA__c WHERE ID=:riRecord.RMA__c];    
        }
        
        System.debug('#### RMALIST (riRecord.RMA__c)= ' + RMAList); 
        
        if(RMAList.size() == 1)
        {       
            relatedRMA = RMAList[0];
            //riRecord.ProductCondition__c=RMAList[0].ProductCondition__c;
        }
              
    }
    
    public String scriteria{get;set;}{    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
    }
    
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
    


        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
                
            }
           
        }
        if(!Test.isRunningTest()){    
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
    

    
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        return Database.query(query);       
    }
    public PageReference performSelect(){ 
    
        system.debug('## riRecord ##'+ riRecord);
        PageReference pageResult;     
        if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');
        system.debug('1. ##### jsonLevel' + jsonLevel);    
        if(jsonLevel=='')    
            jsonLevel=pageParameters.get('jsonLevel');
         system.debug('2. ##### jsonLevel' + jsonLevel);
        WS_GMRSearch.gmrDataBean DBL= (WS_GMRSearch.gmrDataBean) JSON.deserialize(jsonSelectString, WS_GMRSearch.gmrDataBean.class);   
        
        
        if(riRecord.RMA__c != null){  
            
                     if(DBL.commercialReference != null)
                    riRecord.Name= DBL.commercialReference;
                     riRecord.CommercialReference__c= DBL.commercialReference;
                    //riRecord.Family__c = DBL.family.label;
                     if(DBL.businessLine.label != null)
                    riRecord.BusinessUnit__c =DBL.businessLine.label.UnEscapeXML();
                     if(DBL.productLine.label != null)
                    riRecord.ProductLine__c =DBL.productLine.label.UnEscapeXML();
                    system.debug('*****ProductLine__c**'+DBL.productLine.label.UnEscapeXML());
                    system.debug('*****ProductLine__c**'+riRecord.ProductLine__c);
                    //riRecord.TECH_ProductFamily__c =DBL.strategicProductFamily.label;
                     riRecord.ProductFamily__c = DBL.strategicProductFamily.label.UnEscapeXML();
                     if(DBL.family.label != null)
                    riRecord.Family__c =DBL.family.label.UnEscapeXML();
                    if(DBL.subFamily.label!=null)
                    riRecord.SubFamily__c =DBL.subFamily.label.UnEscapeXML();                   
                    if(DBL.description != null)                 
                    riRecord.ProductDescription__c =DBL.description;                   
                    if(DBL.productGroup.label!=null)
                    riRecord.ProductGroup__c =DBL.productGroup.label.UnEscapeXML();
                     if(DBL.productSuccession.label!=null)
                    riRecord.ProductSuccession__c =DBL.productSuccession.label.UnEscapeXML();
                    
                    
                    if(DBL.businessLine.value != null)
                        riRecord.TECH_GMRCode__c = DBL.businessLine.value;
                    if(DBL.productLine.value != null)
                        riRecord.TECH_GMRCode__c += DBL.productLine.value;
                    if(DBL.strategicProductFamily.label != null)
                        riRecord.TECH_GMRCode__c += DBL.strategicProductFamily.value;
                    if(DBL.family.label != null)
                        riRecord.TECH_GMRCode__c += DBL.family.value;
                    if(DBL.subFamily.label != null)
                        riRecord.TECH_GMRCode__c += DBL.subFamily.value;
                    if(DBL.productSuccession.label != null)
                        riRecord.TECH_GMRCode__c += DBL.productSuccession.value;
                    if(DBL.productGroup.label != null)
                        riRecord.TECH_GMRCode__c += DBL.productGroup.value;
         
                
        }                  
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'CaseID cannot be blank'));
            return null;
        }       

        try{
            pageResult = goToProductEdit();                                   
            return pageResult;
        }
        catch(DmlException dmlexception){
            for(integer i = 0;i<dmlexception.getNumDml();i++)
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,dmlexception.getDmlMessage(i)));    
            return null;                  
        }  
    
    }
    public PageReference performSelectFamily(){
    
    
        if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');
        WS04_GMR.gmrDataBean DBL= (WS04_GMR.gmrDataBean) JSON.deserialize(jsonSelectString, WS04_GMR.gmrDataBean.class);   
    
         if(riRecord.RMA__c != null){         
            if(DBL.businessLine.label != null)
             riRecord.BusinessUnit__c =DBL.businessLine.label.UnEscapeXML();               
            if(DBL.productLine.label != null)
                riRecord.ProductLine__c =DBL.productLine.label.UnEscapeXML();                        
            if(DBL.strategicProductFamily.label != null)
               // riRecord.TECH_ProductFamily__c =DBL.strategicProductFamily.label;
                riRecord.ProductFamily__c = DBL.strategicProductFamily.label.UnEscapeXML(); 
                riRecord.Name = DBL.Family.label.UnEscapeXML();
            if(DBL.family.label != null)
                riRecord.Family__c =DBL.family.label.UnEscapeXML();  
                riRecord.Name = DBL.Family.label.UnEscapeXML();

                               
            riRecord.ProductDescription__c = null;
            riRecord.SubFamily__c = null;
            riRecord.ProductSuccession__c = null;
            riRecord.ProductGroup__c = null;       
    
            if(DBL.businessLine.value != null)
                riRecord.TECH_GMRCode__c = DBL.businessLine.value;
            if(DBL.productLine.value != null)
                riRecord.TECH_GMRCode__c += DBL.productLine.value;
            if(DBL.strategicProductFamily.label != null)
                riRecord.TECH_GMRCode__c += DBL.strategicProductFamily.value;
            if(DBL.family.label != null)
                riRecord.TECH_GMRCode__c += DBL.family.value;
                            
        }
        else{
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Return Request ID cannot be blank'));
             return null;
        }       

        try{
             PageReference pageResult;
            pageResult = goToProductEdit();                                   
            return pageResult;
            
            //upsert riRecord;
            //PageReference casePage;            
            //casePage=new PageReference('/'+riRecord.id);                        
            //return casePage;
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
            return null;
        }        
        
        
        
        return null;
    }
    
    public PageReference pageCancelFunction(){
        PageReference pageResult;
    /*
        if(riRecord.id!=null){
            pageResult = new PageReference('/'+ riRecord.id);
            try{
                if(riRecord.status != 'Closed' && riRecord.status != 'Cancelled' ){
                    riRecord.TECH_LaunchGMR__c = false;
                    DataBase.SaveResult UpdateCase = Database.Update(riRecord);
                }
                pageResult = new ApexPages.StandardController(riRecord).view();
            }
            catch(exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+e.getMessage()));
            }   
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
    */
    return null;
}

    public PageReference goToProductEdit()
    {        
        
    
    
        // Create a new edit page for the RMA 
        PageReference aNewRMAProducteditPage = new PageReference('/'+SObjectType.RMA_Product__c.getKeyPrefix()+'/e' );
        
        // Set the return to the parent RMA (retURL)
        aNewRMAProducteditPage.getParameters().put(Label.CL00330, riRecord.RMA__c);   
        
        // Disable override
        aNewRMAProducteditPage.getParameters().put(Label.CL00690, Label.CL00691); 
        
        // Pre-default the parent case with the ID (_lkid) and the RMA Number  
        if(riRecord.RMA__c != null && relatedRMA != null && relatedRMA.Name != null)
        {
            aNewRMAProducteditPage.getParameters().put(Label.CL00695, riRecord.RMA__c);
            aNewRMAProducteditPage.getParameters().put(Label.CL00696, relatedRMA.Name); 
            aNewRMAProducteditPage.getParameters().put(Label.CLDEC12RR03, relatedRMA.ProductCondition__c); 
             // Set the recordType
          if(relatedRMA .TEX__c != null)
            {
                system.debug('## relatedRMA ##'+ relatedRMA .TEX__c);
                aNewRMAProducteditPage.getParameters().put('RecordType',Label.CLOCT13RR28); 
                //String SvURL = '/apex/VFP_CreateReturnItem?TEXOverrideFlag='+relatedTex.ForceManualSelection__c;
                 aNewRMAProducteditPage.getParameters().put(Label.CLDEC12RR02, '/apex/VFP_CreateReturnItem?rigmrcode='+ riRecord.Tech_GMRCode__c + '&TEXOverrideFlag='+relatedRMA.TEX__r.ForceManualSelection__c + '&Productline='+riRecord.ProductLine__c);   
            }
        }
        
        // Populate the commercial reference (name) with the search text of the PM0 search   
        if(riRecord!= null && riRecord.Name != null)
        {
            aNewRMAProducteditPage.getParameters().put(Label.CL00697, riRecord.Name);
            aNewRMAProducteditPage.getParameters().put(Label.CL10208, riRecord.ProductFamily__c);
            aNewRMAProducteditPage.getParameters().put(Label.CLDEC12RR08, riRecord.Tech_GMRCode__c);
            //aNewRMAProducteditPage.getParameters().put('00NA0000009wkKa', riRecord.ProductLine__c );
            
            //k:START
            aNewRMAProducteditPage.getParameters().put(Label.CLOCT14I2P32, riRecord.CommercialReference__c); //'00N17000000Joys' in devOct
            aNewRMAProducteditPage.getParameters().put(Label.CLOCT14I2P33, riRecord.Family__c); //'00N17000000Joyd' in devOct
            //aNewRMAProducteditPage.getParameters().put('00NA0000009hS8a', riRecord.ProductFamily__c);
            //k:STOP
           // '00NZ0000000akdJ'
        }
        else
        {      
           // aNewRMAProducteditPage.getParameters().put(Label.CL00697, GMRSearchClass.searchController.searchText); 
        }
        
       

         //*** RELEASE DEC12 - START ****************************************************//
        // pass the saveURL
        // CLDEC12RMA02 - saveURL
        // CLDEC12RMA01 - '/apex/VFP_CreateReturnItem?rigmrcode='+ riRecord.Tech_GMRCode__c
           String productLineUrl = riRecord.ProductLine__c;
           System.debug(productLineUrl);
           productLineUrl = EncodingUtil.urlEncode(productLineUrl,'UTF-8');
           System.debug(productLineUrl);
           
           aNewRMAProducteditPage.getParameters().put('RMA', riRecord.RMA__c);
            if(relatedRMA .TEX__c != null)
            {
               aNewRMAProducteditPage.getParameters().put(Label.CLDEC12RR02, '/apex/VFP_CreateReturnItem?rigmrcode='+ riRecord.Tech_GMRCode__c + '&TEXOverrideFlag='+relatedRMA.TEX__r.ForceManualSelection__c+ '&Productline='+productLineUrl );   
            }    
            else
            {
                aNewRMAProducteditPage.getParameters().put(Label.CLDEC12RR02, '/apex/VFP_CreateReturnItem?rigmrcode='+ riRecord.Tech_GMRCode__c+ '&Productline='+productLineUrl );
            }
            //system.debug('@@@@@ aNewRMAProducteditPage '+ aNewRMAProducteditPage);
         //aNewRMAProducteditPage = EncodingUtil.urlEncode(aNewRMAProducteditPage,'UTF-8');  
        //*** RELEASE DEC12 - END********************************************************//
        system.debug('@@@@@ aNewRMAProducteditPage '+ aNewRMAProducteditPage);
        
        //aNewRMAProducteditPage.setRedirect(true);
        return aNewRMAProducteditPage;       
    }
    
    Public PageReference Validate()
    {
       if (relatedRMA.Status__c==Label.CLMAY13RR12 || relatedRMA.Status__c==Label.CLMAY13RR13)  // CLMAY13RR13 - Submitted ; CLMAY13RR12 - Validated
        {
            System.debug('## Status ##'+ relatedRMA.Status__c );
            impossibleToAddProduct = True;
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, LABEL.CLMAY13RR03));
            return null;
        }
        return null;
    } 
    
    Public PageReference doGoBack()
    {
     PageReference pref=new PageReference('/'+relatedRMA.id);
        pref.setRedirect(true);
        return pref;
    
    }
}