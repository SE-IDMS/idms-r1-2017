/************
Phase - Coin(Milestone1_Milestone__c)
May release 2014
*************/
public class AP_COIN_bFO_PhaseCoin{


    public static void updateTaskCoinStageToPhase(Map<Id,Milestone1_Milestone__c> mapPhaseCoin) {
    
    
        Set<Id>  setPhaseId = new Set<Id>();
        List<Milestone1_Project__c>  listUpdateStage = new  List<Milestone1_Project__c>();
       
        Map<string,Milestone1_Project__c>  mapUpdateStage = new  Map<string,Milestone1_Project__c>();
        Map<id,List<Milestone1_Milestone__c>> mapLisMMPhase = new Map<id,List<Milestone1_Milestone__c>>();
         
        for(Milestone1_Milestone__c mmObj:mapPhaseCoin.values()) {
        
            if(mapLisMMPhase.containskey(mmObj.Project__c)) {
                mapLisMMPhase.get(mmObj.Project__c).add(mmObj);
            }else {
                mapLisMMPhase.put(mmObj.Project__c,new list<Milestone1_Milestone__c>());
                mapLisMMPhase.get(mmObj.Project__c).add(mmObj);
                
            }
            setPhaseId.add( mmObj.Project__c);
        
        }
        for(Milestone1_Project__c MpObj:[select id,stage__c from Milestone1_Project__c where id IN:setPhaseId]) {
           
            if(mapLisMMPhase.containskey(MpObj.id)) {
                
                for(Milestone1_Milestone__c msmObj:mapLisMMPhase.get(MpObj.id)) {
                    if(MpObj.id==msmObj.Project__c) {
                      if(MpObj.stage__c!=msmObj.name) {
                          MpObj.stage__c=msmObj.name;
                          listUpdateStage.add(MpObj);
                          mapUpdateStage.put(MpObj.id,MpObj);   
                      }
                    
                    }
                 }                       
            }
        
        }
        
        if(mapUpdateStage.size() >0) {
            update mapUpdateStage.values();
        }
    
    
    }


   

}