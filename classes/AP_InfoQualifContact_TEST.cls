@isTest
private class AP_InfoQualifContact_TEST
{
    private static Datetime current = Datetime.newInstance(2016, 1, 1, 1, 1, 1);

    @testSetup
    static void setup()
    {
        Country__c country = new Country__c(Name = 'fakeCountry', CountryCode__c = 'FR');
        insert country;

        Account fakeAccount1 = new Account(
            Name = 'fakeAccount1',
            Street__c = 'fakeStreet',
            ZipCode__c = '00000',
            Country__c = country.Id
        );
        insert fakeAccount1;

        Contact fakeContact1 = new Contact(
            LastName = 'fakeContact1',
            FirstName= 'fakeContact1',
            AccountId = fakeAccount1.Id,
            Salutation = 'Mr',
            Country__c = country.Id,
            Email = 'p@p.fr'
        );
        insert fakeContact1;

        InfoQualif__c iq = new InfoQualif__c(
            Account__c = fakeAccount1.Id
        );
        insert iq;

        InfoQualifContact__c iqc = new InfoQualifContact__c(
            Contact__c = fakeContact1.Id,
            InfoQualif__c = iq.Id,
            FirstnameValidationDate__c = current,
            SalutationValidationDate__c = current,
            ReportsToIdValidationDate__c = current,
            JobTitleValidationDate__c = current,
            JobFunctionValidationDate__c = current,
            WorkPhoneValidationDate__c = current,
            EmailValidationDate__c = current,
            ProfilingValidationDate__c = current,
            CollectifsValidationDate__c = current,
            CanauxCommValidationDate__c = current,
            HeurePrefereeTelephoneValidationDate__c = current,
            JobDescrValidationDate__c = current,
            ReseauxSociauxValidationDate__c = current,
            SchneiderPromoteurValidationDate__c = current,
            AnimValidationDate__c = current,
            GeekValidationDate__c = current,
            ACOValidationDate__c = current,
            CatDEValidationDate__c = current,
            CatBTValidationDate__c = current,
            CatINDValidationDate__c = current,
            CatPrismaValidationDate__c = current,
            CatNSXValidationDate__c = current,
            CatUEValidationDate__c = current,
            DisChiffreurValidationDate__c = current,
            DisTypesClientsSuivisDateTime__c = current,
            DisReseauxExpertsValidationDate__c = current
        );
        insert iqc;
    }

    @isTest
    static void checkLastValidationDate()
    {
        InfoQualifContact__c iqc = [SELECT LastValidationDate__c FROM InfoQualifContact__c LIMIT 1];
        System.assertEquals(current, iqc.LastValidationDate__c);
    }

    @isTest
    static void checkDis()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.ClassLevel1__c = 'WD';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'PR';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        InfoQualifContact__c iqc = [SELECT Id FROM InfoQualifContact__c LIMIT 1];
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(11, iqc.CompletionCount3__c);

        me.UserBusinessUnit__c = 'PW';
        update me;

        update iq;
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(11, iqc.CompletionCount3__c);

        iq.classLevel1Approb__c = 'Validé';

        update iq;
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(10, iqc.CompletionCount3__c);
    }

    @isTest
    static void checkIdnSystemIntegrators()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.ClassLevel1__c = 'SI';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'IA';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        InfoQualifContact__c iqc = [SELECT Id FROM InfoQualifContact__c LIMIT 1];
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(11, iqc.CompletionCount3__c);

        iq.classLevel1Approb__c = 'Validé';

        update iq;
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(9, iqc.CompletionCount3__c);
    }

    @isTest
    static void checkPpebTableautiers()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.ClassLevel1__c = 'PB';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'BD';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        InfoQualifContact__c iqc = [SELECT Id FROM InfoQualifContact__c LIMIT 1];
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(11, iqc.CompletionCount3__c);

        iq.classLevel1Approb__c = 'Validé';

        update iq;
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(23, iqc.CompletionCount3__c);
    }

    @isTest
    static void checkIdnTableautiers()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.ClassLevel1__c = 'PB';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'IA';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        InfoQualifContact__c iqc = [SELECT Id FROM InfoQualifContact__c LIMIT 1];
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(11, iqc.CompletionCount3__c);

        iq.classLevel1Approb__c = 'Validé';

        update iq;
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(9, iqc.CompletionCount3__c);
    }

    @isTest
    static void checkIdnOems()
    {
        Account acc = [SELECT Id FROM Account LIMIT 1];
        acc.ClassLevel1__c = 'OM';
        update acc;

        User me = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        me.UserBusinessUnit__c = 'IA';
        update me;

        InfoQualif__c iq = [SELECT Id FROM InfoQualif__c LIMIT 1];
        update iq;

        InfoQualifContact__c iqc = [SELECT Id FROM InfoQualifContact__c LIMIT 1];
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(11, iqc.CompletionCount3__c);

        iq.classLevel1Approb__c = 'Validé';

        update iq;
        update iqc;

        iqc = [SELECT CompletionCount3__c FROM InfoQualifContact__c WHERE Id = :iqc.Id];
        System.assertEquals(9, iqc.CompletionCount3__c);
    }
}