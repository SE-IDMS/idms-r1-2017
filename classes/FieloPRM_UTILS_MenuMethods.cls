/********************************************************************
* Company: Fielo
* Created Date: 26/04/2015
* Description: Util class for Menu object
********************************************************************/

public class FieloPRM_UTILS_MenuMethods{
    
    public class MenusContainer{
        public List<FieloEE__Menu__c> parentMenus {get; set;}
        public Map<Id,List<FieloEE__Menu__c>> childMenusMap {get; set;}
        public MenusContainer(){
            parentMenus = new List<FieloEE__Menu__c>();
            childMenusMap = new Map<Id,List<FieloEE__Menu__c>>();        
        }
        public void addMenu(FieloEE__Menu__c menu){
            parentMenus.add(menu);
            childMenusMap.put(menu.Id, new List<FieloEE__Menu__c>());
        }
    }
    
    /********************************************************************
    METHOD: getMenus
    DML: NO
    COMMENTS: returns all menus and submenus for a contactid and section
    ********************************************************************/
    public static MenusContainer getMenus(String section, String contactId){
    
        List<Contact> listContact = [SELECT Id, FieloEE__Member__c, FieloEE__Member__r.FieloEE__Program__c FROM Contact WHERE Id =: contactId AND FieloEE__Member__c != null LIMIT 1];
        
        //if no member found returns null
        if(listContact.isEmpty()){return null;}
        
        Id programId = listContact[0].FieloEE__Member__r.FieloEE__Program__c;
        Id memberId =  listContact[0].FieloEE__Member__c;
        
        set<Id> setMemberSegments = FieloEE.SegmentService.getMemberSegments(memberId, null);
        
        Set<Id> segMenuIds = new Set<Id>();
        for(FieloEE__SegmentDomain__c sd: [SELECT Id, FieloEE__Segment__c, FieloEE__Menu__c FROM FieloEE__SegmentDomain__c WHERE FieloEE__Segment__c IN: setMemberSegments]){
            segMenuIds.add(sd.FieloEE__Menu__c);
        }
        
        //query parent menus
        String allFields = '';
        for(Schema.SObjectField nameAPIfield: Schema.SObjectType.FieloEE__Menu__c.fields.getMap().values()){
            allFields += allFields==''?String.ValueOf(nameAPIfield):', '+String.ValueOf(nameAPIfield);
        }
        String query = 'SELECT ' + allFields + ' FROM FieloEE__Menu__c ' +
                     + 'WHERE FieloEE__Menu__c = NULL AND '
                     + 'FieloEE__Program__c =: programId AND '
                     + 'FieloEE__Placement__c =: section AND '
                     + 'F_PRM_Status__c = \'Active\' AND '
                     + '(FieloEE__Visibility__c = \'Both\' OR FieloEE__Visibility__c = \'Private\') AND '
                     + '(Id IN: segMenuIds OR FieloEE__RedemptionRule__c IN: setMemberSegments OR (FieloEE__RedemptionRule__c = NULL AND FieloEE__HasMultiSegments__c = FALSE)) '
                     + 'ORDER BY FieloEE__Order__c';
        
        MenusContainer retMenu = new MenusContainer();
        set<Id> setMenuParents = new set<Id>();
        for(FieloEE__Menu__c menu: Database.query(query)){
            retMenu.addMenu(menu);
            setMenuParents.add(menu.Id);
        }
    
        //we are returning child menus only for Top section because it's only visible in the header in the site      
        if(section == 'Top'){
            //look for the child menus and filter by segment
            String querySubMenus = 'SELECT ' + allFields + ' FROM FieloEE__Menu__c '
                                 + 'WHERE FieloEE__Menu__c IN: setMenuParents AND '
                                 + 'F_PRM_Status__c = \'Active\' AND '
                                 + '(FieloEE__Visibility__c = \'Both\' OR FieloEE__Visibility__c = \'Private\') AND '
                                 + '(Id IN: segMenuIds OR FieloEE__RedemptionRule__c IN: setMemberSegments OR (FieloEE__RedemptionRule__c = NULL AND FieloEE__HasMultiSegments__c = FALSE)) '
                                 + 'ORDER BY FieloEE__Order__c';
            
            //load the submenus in the structure
            for(FieloEE__Menu__c subMenu: Database.query(querySubMenus)){
                retMenu.childMenusMap.get(subMenu.FieloEE__Menu__c).add(subMenu);
            }
        }
        
        return retMenu;
    }
    
    public static List<String> validateAndReplaceDuplicateExternalNames(List<String> listExternalName){
        
        Set<String> setExternalNames = new Set<String>();
        
        List<String> listReturnExternalNames1 = new List<String>();
        
        //this variable is used to make external names that are duplicate in the list as unique
        Integer auxNumber = 0;
        
        for(String externalName: listExternalName){

            //if external name has more than 80 characters, only the first 80 characters are used
            if(externalName.length() > 80){
                externalName = externalName.left(80);   
            }
            
            //special characters are removed
            externalName = externalName.replaceAll('[^a-z^A-z^0-9]', '_');
            
            //check if in the new list there are duplicate external names, if duplicates are found a dateTime string + number is added to make it unique
            if(!setExternalNames.contains(externalName)){
                setExternalNames.add(externalName);
            }else{
                if((externalName.length() + getDateTimeString().length() + string.valueOf(auxNumber).length()) > 80){
                    externalName = externalName.left(80 - (getDateTimeString().length() + string.valueOf(auxNumber).length()));
                }
                externalName += getDateTimeString() + string.valueOf(auxNumber);
                setExternalNames.add(externalName);
                auxNumber++;
            }
            listReturnExternalNames1.add(externalName);
        }
        
        //check with database if any of the new external names are already being used
        List<FieloEE__Menu__c> listMenusWithExistingExternalName = [SELECT Id, FieloEE__ExternalName__c FROM FieloEE__Menu__c WHERE FieloEE__ExternalName__c IN: listExternalName];
                
        List<String> listReturnExternalNames2 = new List<String>();
        
        //if the list is not empty, the duplicate external names has to be modified
        if(!listMenusWithExistingExternalName.isEmpty()){
        
            Set<String> setExistingExternalNames = new Set<String>();
            
            //saves existing external names in database to a set
            for(FieloEE__Menu__c menu: listMenusWithExistingExternalName){
                setExistingExternalNames.add(menu.FieloEE__ExternalName__c);
            }            
            
            //loops the new external names list and if they exist in database the external name is modified to be unique
            for(String externalName: listReturnExternalNames1){
                if(setExistingExternalNames.contains(externalName)){
                    if((externalName.length() + getDateTimeString().length() + string.valueOf(auxNumber).length()) > 80){
                        externalName = externalName.left(80 - (getDateTimeString().length() + string.valueOf(auxNumber).length()));
                    }                   
                    externalName += getDateTimeString() + string.valueOf(auxNumber);
                    listReturnExternalNames2.add(externalName);
                    auxNumber++;
                }
            }
            return listReturnExternalNames2;
        }
      
        return listReturnExternalNames1;
    }

    //returns the date time string for now() in a string of 14 characters (year(4) + month(2) + day(2) + hour(2) + minute(2) + second(2))
    public static string getDateTimeString(){
        String year = string.valueOf(dateTime.now().year());
        String month = string.valueOf(dateTime.now().month()).length() == 2 ? string.valueOf(dateTime.now().month()) : '0' + string.valueOf(dateTime.now().month());
        String day = string.valueOf(dateTime.now().day()).length() == 2 ? string.valueOf(dateTime.now().day()) : '0' + string.valueOf(dateTime.now().day());
        String hour = string.valueOf(dateTime.now().hour()).length() == 2 ? string.valueOf(dateTime.now().hour()) : '0' + string.valueOf(dateTime.now().hour());
        String minute = string.valueOf(dateTime.now().minute()).length() == 2 ? string.valueOf(dateTime.now().minute()) : '0' + string.valueOf(dateTime.now().minute());
        String second = string.valueOf(dateTime.now().second()).length() == 2 ? string.valueOf(dateTime.now().second()) : '0' + string.valueOf(dateTime.now().second());
        return (year + month + day + hour + minute + second); //14 characters
    }

    public static void createEntitySubscriptions(Set<String> setMenuIds){
    
        String userId = UserInfo.getUserId();
        Set<String> setMenuWithEntitySub =  new Set<String>();
     
        //checks if the menus already have an entity subscription
        for(EntitySubscription entitySub: [SELECT Id, ParentId FROM EntitySubscription WHERE SubscriberId =: userId AND ParentId IN: setMenuIds LIMIT 1000]){
            setMenuWithEntitySub.add(entitySub.ParentId);
        }
        
        List<EntitySubscription> listEntitySubscriptionToInsert = new List<EntitySubscription>();
        
        //creates the new entity subscription for menus that doesn't have already
        for(String menuId: setMenuIds){
            if(!setMenuWithEntitySub.contains(menuId)){
                listEntitySubscriptionToInsert.add(new EntitySubscription(ParentId = menuId, SubscriberId = userId));
            }
        }
        
        //inserts the new entity subscriptions
        if(!listEntitySubscriptionToInsert.isEmpty()){
            insert listEntitySubscriptionToInsert;
        }
    }
    
    //Method to obtain a member "Home" menu
    public static String setHomeMenuCookie(FieloEE__Member__c member, CountryChannels__c memberPrimaryChannel){
        String homeMenuCookie = ApexPages.currentPage().getCookies().get('myHomeMenu') != null ? ApexPages.currentPage().getCookies().get('myHomeMenu').getValue() : null;
        
        if(homeMenuCookie == null || homeMenuCookie == ''){
            set<Id> setSegmentId = FieloEE.RedemptionUtil.lookForMatchingRedemptionRulesCookie();
            
            set<Id> setTeams = new set<Id>();
            for(FieloCH__TeamMember__c teamMember: [SELECT FieloCH__Team2__c FROM FieloCH__TeamMember__c WHERE FieloCH__Member2__c =: member.Id AND FieloCH__Team2__c != null]){
                setTeams.add(teamMember.FieloCH__Team2__c);
            }
            
            Integer quantityEnrolledPrograms = 0;
            
            set<Id> setMenuIds = new set<Id>();
            for(FieloCH__ChallengeMember__c cm: [SELECT Id, FieloCH__Challenge2__r.F_PRM_MenuProgram__c, FieloCH__Challenge2__r.F_PRM_MenuProgram__r.FieloEE__RedemptionRule__c FROM FieloCH__ChallengeMember__c WHERE (FieloCH__Member__c =: member.Id OR (FieloCH__Team__c != null AND FieloCH__Team__c IN: setTeams)) AND FieloCH__Challenge2__r.FieloCH__IsActive__c = true AND FieloCH__Challenge2__r.F_PRM_MenuProgram__r.F_PRM_Type__c != 'Template' AND FieloCH__Challenge2__r.F_PRM_MenuProgram__c != null AND FieloCH__Challenge2__r.F_PRM_MenuProgram__r.F_PRM_Status__c != 'Draft']){
                setMenuIds.add(cm.FieloCH__Challenge2__r.F_PRM_MenuProgram__c);
                if(cm.FieloCH__Challenge2__r.F_PRM_MenuProgram__r.FieloEE__RedemptionRule__c != null && setSegmentId.contains(cm.FieloCH__Challenge2__r.F_PRM_MenuProgram__r.FieloEE__RedemptionRule__c)){
                    quantityEnrolledPrograms++;
                }
            }
            
            if(quantityEnrolledPrograms <= 1){
                List<FieloEE__SegmentDomain__c> segmentDomainsMenu = [SELECT Id FROM FieloEE__SegmentDomain__c WHERE FieloEE__Segment__c IN: setSegmentId AND FieloEE__Menu__c IN: setMenuIds LIMIT 2];
                quantityEnrolledPrograms += segmentDomainsMenu.size();
            }
            
            if(quantityEnrolledPrograms == 1){
                if(memberPrimaryChannel != null){
                    if(memberPrimaryChannel.DefaultLandingPage__c == 'Program'){
                        //Sets "My Programs" menu as "Home" menu
                        setACookie('myHomeMenu', setMyProgramsMenuCookie(member));
                    }else if(memberPrimaryChannel.DefaultLandingPage__c == 'Partnership'){
                        //Sets "My Partnership" menu as "Home" menu
                        setACookie('myHomeMenu', setMyPartnershipMenuCookie(memberPrimaryChannel));
                    }
                }
            }else{
                //Sets "My Partnership" menu as "Home" menu
                setACookie('myHomeMenu', setMyPartnershipMenuCookie(memberPrimaryChannel));
            }
        }
        
        homeMenuCookie = ApexPages.currentPage().getCookies().get('myHomeMenu').getValue();
        if(homeMenuCookie == null || homeMenuCookie == ''){
            setACookie('myHomeMenu', Label.Fielo_PRM_CL_005_HomeRedirect.replace('/Menu/', ''));
        }
        
        return ApexPages.currentPage().getCookies().get('myHomeMenu').getValue();
    }

    //Method to obtain a member "My Partnership" menu
    public static String setMyPartnershipMenuCookie(CountryChannels__c memberPrimaryChannel){
        if(!ApexPages.currentPage().getCookies().containsKey('myPartnerShipMenu') || ( ApexPages.currentPage().getCookies().containsKey('myPartnerShipMenu') && ApexPages.currentPage().getCookies().get('myPartnerShipMenu').getValue() == null)){
            if(memberPrimaryChannel != null){
                setACookie('myPartnerShipMenu', memberPrimaryChannel.F_PRM_Menu__r.FieloEE__ExternalName__c);
            }
            
            //Default menu
            if(!ApexPages.currentPage().getCookies().containsKey('myPartnerShipMenu')){
                setACookie('myPartnerShipMenu', Label.Fielo_PRM_CL_005_HomeRedirect.replace('/Menu/', ''));
            }
        }
        return ApexPages.currentPage().getCookies().get('myPartnerShipMenu').getValue();
    }
    
    //Method to obtain a member "My Programs" menu
    public static String setMyProgramsMenuCookie(FieloEE__Member__c member){
        if(!ApexPages.currentPage().getCookies().containsKey('myProgramsMenu') || ( ApexPages.currentPage().getCookies().containsKey('myProgramsMenu') && ApexPages.currentPage().getCookies().get('myProgramsMenu').getValue() == null)){
            set<id> setSegmentId = FieloEE.RedemptionUtil.lookForMatchingRedemptionRulesCookie();
            
            set<Id> setTeams = new set<Id>();
            for(FieloCH__TeamMember__c teamMember: [SELECT FieloCH__Team2__c FROM FieloCH__TeamMember__c WHERE FieloCH__Member2__c =: member.Id AND FieloCH__Team2__c != null]){
                setTeams.add(teamMember.FieloCH__Team2__c);
            }
            
            Boolean menuSetted = false;
            set<id> setMenuIds = new set<id>();
            for(FieloCH__ChallengeMember__c cm: [SELECT id, FieloCH__Challenge2__r.F_PRM_MenuProgram__c, FieloCH__Challenge2__r.F_PRM_MenuProgram__r.FieloEE__ExternalName__c, FieloCH__Challenge2__r.F_PRM_MenuProgram__r.FieloEE__RedemptionRule__c FROM FieloCH__ChallengeMember__c WHERE FieloCH__Challenge2__r.FieloCH__IsActive__c = true AND (FieloCH__Member__c =: member.Id OR FieloCH__Team__c IN: setTeams) AND FieloCH__Challenge2__r.F_PRM_MenuProgram__c != null AND FieloCH__Challenge2__r.F_PRM_MenuProgram__r.F_PRM_Type__c != 'Template' AND FieloCH__Challenge2__r.F_PRM_MenuProgram__r.F_PRM_Status__c != 'Draft' ORDER BY FieloCH__Challenge2__r.Name]){
                if(cm.FieloCH__Challenge2__r.F_PRM_MenuProgram__r.FieloEE__RedemptionRule__c != null && setSegmentId.contains(cm.FieloCH__Challenge2__r.F_PRM_MenuProgram__r.FieloEE__RedemptionRule__c)){
                    setACookie('myProgramsMenu', cm.FieloCH__Challenge2__r.F_PRM_MenuProgram__r.FieloEE__ExternalName__c);
                    menuSetted = true;
                    break;
                }else{
                    setMenuIds.add(cm.FieloCH__Challenge2__r.F_PRM_MenuProgram__c);
                }
            }
            
            if(!menuSetted){
                list<FieloEE__SegmentDomain__c> segmentDomainsMenu = [SELECT Id, FieloEE__Menu__c, FieloEE__Menu__r.FieloEE__ExternalName__c FROM FieloEE__SegmentDomain__c WHERE FieloEE__Segment__c IN: setSegmentId AND FieloEE__Menu__c IN: setMenuIds ORDER BY FieloEE__Menu__r.Name LIMIT 1];
                if((!segmentDomainsMenu.isEmpty()) && segmentDomainsMenu[0].FieloEE__Menu__r.FieloEE__ExternalName__c != null){
                    setACookie('myProgramsMenu', segmentDomainsMenu[0].FieloEE__Menu__r.FieloEE__ExternalName__c);
                }
                
                //Default menu
                if(!ApexPages.currentPage().getCookies().containsKey('myProgramsMenu')){
                    setACookie('myProgramsMenu', Label.Fielo_PRM_CL_006_ProgramRedirect.replace('/Menu/', ''));
                }
            }
        }
        
        return ApexPages.currentPage().getCookies().get('myProgramsMenu').getValue();
    }
    
    //Method to obtain a member "Products" or "Support" menu
    public static String setCountryMenuByParentCookie(FieloEE__Member__c member, String mainMenuExternalName, String cookieName){
        if(!ApexPages.currentPage().getCookies().containsKey(cookieName) || ( ApexPages.currentPage().getCookies().containsKey(cookieName) && ApexPages.currentPage().getCookies().get(cookieName).getValue() == null)){
            
            List<FieloEE__Menu__c> menusMemberCountry = [SELECT Id, FieloEE__ExternalName__c FROM FieloEE__Menu__c WHERE FieloEE__Menu__r.FieloEE__ExternalName__c =: mainMenuExternalName AND F_PRM_Country__r.CountryCode__c =: member.F_Country__r.CountryCode__c LIMIT 1];
            if(!menusMemberCountry.isEmpty()){
                setACookie(cookieName, menusMemberCountry[0].FieloEE__ExternalName__c);
            }else{
                List<FieloEE__Menu__c> menusMemberPRMCountry = [SELECT Id, FieloEE__ExternalName__c FROM FieloEE__Menu__c WHERE FieloEE__Menu__r.FieloEE__ExternalName__c =: mainMenuExternalName AND F_PRM_Country__r.CountryCode__c =: member.F_PRM_Country__r.CountryCode__c LIMIT 1];
                if(!menusMemberPRMCountry.isEmpty()){
                    setACookie(cookieName, menusMemberPRMCountry[0].FieloEE__ExternalName__c);
                }else{
                    setACookie(cookieName, Label.FieloPRM_CL_53_PageRedirect.replace('/Menu/', ''));
                }
            }
        }
        return ApexPages.currentPage().getCookies().get(cookieName).getValue();
    }
    
    //Method to obtain the corresponding page if the user didn't complete the registration or this is his first login
    public static String getRegistrationStringMenu(){
        String regViewMode = '';
        cookie cRegViewMode = ApexPages.currentPage().getCookies().get('regViewMode');
        if(cRegViewMode != null)
            regViewMode = cRegViewMode.getValue();
        //User query
        User u = (!Test.isRunningTest()) ? AP_PRMUtils.isUserRegistered() : [SELECT Id, PRMRegistrationCompleted__c, ContactId, Contact.PRMTnC__c FROM User WHERE FirstName =: 'Test User First' LIMIT 1];
        
        if (u.ContactId != null && u.PRMRegistrationCompleted__c && 
            (String.isBlank(u.Contact.PRMTnC__c) || 
             (String.isNotBlank(u.Contact.PRMTnC__c) && (u.Contact.PRMTnC__c == 'D' || u.Contact.PRMTnC__c == 'N')))) {
            return 'MainTermsAndConditions';
        } 
        else if (u.PRMRegistrationCompleted__c && AP_PRMUtils.IsFirstLogin()) {
            //If the registration is complete
            //if(){
                //If this is the first login, redirects to "RegistrationCompletion" menu
                return 'RegistrationCompletion';
            // }
        }else{
            //If the registration is incomplete
            
            if(u.ContactId != null && !u.PRMRegistrationCompleted__c){
                //Contact Query
                Contact cont = Database.query('SELECT Id, PRMCountry__r.CountryCode__c, PRMCompanyInfoSkippedAtReg__c, PRMLanguage__c, AccountId, Account.PRMCountry__r.CountryCode__c FROM Contact ' + (Test.isRunningTest() ? ('LIMIT 1') : ('WHERE Id = \'' + u.ContactId + '\'')));
                
                //Country Code search
                String countryCode = '';
                if( (!cont.PRMCompanyInfoSkippedAtReg__c) && cont.Account.PRMCountry__r.CountryCode__c != null){
                    countryCode = cont.Account.PRMCountry__r.CountryCode__c;
                } else {
                    countryCode = cont.PRMCountry__r.CountryCode__c;
                }
                
                //If the registration is not complete, redirects to the "ContinueRegistration" menu with the corresponding parameters
                Integer activeClusterCount = [SELECT count() FROM PRMCountry__c WHERE CountryPortalEnabled__c = TRUE AND (Country__r.CountryCode__c =:countryCode OR MemberCountry1__r.CountryCode__c =: countryCode OR MemberCountry2__r.CountryCode__c =: countryCode OR MemberCountry3__r.CountryCode__c =: countryCode OR MemberCountry4__r.CountryCode__c =: countryCode OR MemberCountry5__r.CountryCode__c =: countryCode)];
                if(activeClusterCount > 0) {
                    if(countryCode == null){
                        return 'ContinueRegistration?vw='+regViewMode;
                    }else{
                        if(cont.PRMLanguage__c != null){
                            cont.PRMLanguage__c = cont.PRMLanguage__c.equalsIgnoreCase('id')?'in':cont.PRMLanguage__c+ '&vw=' + regViewMode;
                            return 'ContinueRegistration?country=' + countryCode + '&language=' + cont.PRMLanguage__c;
                        }else{
                            return 'ContinueRegistration?country=' + countryCode+ '&vw=' + regViewMode;
                        }
                    }
                }else{
                    return 'ContinueRegistration?country=' + countryCode + '&err=nochannel&vw=' + regViewMode;
                }
            }
            Cookie privateReg= new Cookie('privateReg', 'privateReg',null, -1, false);
            ApexPages.currentPage().setCookies(new Cookie[]{privateReg});
        }
        return null;
    }
    
    //Cookie set method
    public static void setACookie(String key, String value){
        Cookie aux = new Cookie(key, value, null, (key == 'language' ? 31536000 : -1), false);
        ApexPages.currentPage().setCookies(new Cookie[]{aux});
    }
    
    //Method to reset all redirect cookies in case that the member has changed
    public static void resetRedirectCookie(){
        List<String> allMemberCookies = new List<String>{'IdPrimaryChannelMenu', 'myHomeMenu', 'myPartnerShipMenu', 'myProgramsMenu', 'productsMenu', 'supportMenu', 'language'};
        for(String cookieName: allMemberCookies){
            Cookie aux = new Cookie(cookieName, null, null, 0, false);
            ApexPages.currentPage().setCookies(new Cookie[]{aux});
        }
    }
}