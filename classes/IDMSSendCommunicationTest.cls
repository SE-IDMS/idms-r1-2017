/*
* Test for IDMSSendCommunication
* */
@isTest
public class IDMSSendCommunicationTest{
    
    //Test 1: Send SMS to user
    static testMethod void IDMSSendCommunicationTestMethod() {
        
        User userObj = new User(alias = 'user', email='test363741' + '@cognizant.com', 
                                emailencodingkey='UTF-8', lastname='Testlast1',Firstname='Testfirst', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                timezonesidkey='Europe/London', username='testuser342' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='teja6415',IDMS_Registration_Source__c = 'Test',
                                IDMS_User_Context__c = '@Work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432',IDMS_VC_NEW__c='testnew',IDMS_VC_OLD__c='testnew');
        
        insert userObj;
        
        smagicbasic__smsMagic__c  smsMagicObj= new smagicbasic__smsMagic__c(smagicbasic__SenderId__c=userObj.Id,
                                                                            smagicbasic__PhoneNumber__c=userObj.mobilephone,smagicbasic__Name__c=userObj.Name);
        
        // insert smsMagicObj for test check;                                
        
        String TemplateName='IDMS_SendEmail_PasswordRequestReSet';
        String appId = '12345';
        String signature='abcd';
        
        IDMSSendCommunication.sendemail(userObj.Id,TemplateName,appId);
        IDMSSendCommunication.sendEmailCreateUserPasswordSet(userObj.Id,TemplateName,appId,signature);
        IDMSSendCommunication.sendEmailCreateUserWithPwd(userObj.Id,TemplateName,appId,signature);
        IDMSSendCommunication.sendSMS(userObj);
    }
    
    //Test 2: Send rest email to user
    static  testMethod void IDMSSendCommunicationTestMethod1() {
        String user_id = UserInfo.getUserId();
        String appId = '12345';
        IDMSSendCommunication SendCommunication=new IDMSSendCommunication();
        SendCommunication.sendEmailResetPassword(user_id, appId);
    }
}