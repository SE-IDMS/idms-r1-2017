@isTest
public class VFC_AdjustVisit_TEST
{
    public static testmethod void adjustVisitTest()
    {
        Id userid=userInfo.getUserId();
        Account acc=Utils_TestMethods.createAccount();
        acc.ToBeDeleted__c=true;
        insert acc;
        Account acc2=Utils_TestMethods.createAccount();
        acc.ToBeDeleted__c=false;
        insert acc2;
        List<id> accIdList=new List<id>();
        accIdList.add(acc.id);
        accIdList.add(acc2.id);
        VFC_AdjustVisit.eventtWrapper ewr=new VFC_AdjustVisit.eventtWrapper();
        SFE_IndivCAP__c  cap=Utils_TestMethods.createIndividualActionPlan(userid);
        insert cap;
        CS_CurrencyDecimalSeperator__c CustomSetting=new CS_CurrencyDecimalSeperator__c(Name=UserInfo.getLocale(),DecimalSeperator__c=',');
        insert CustomSetting;
        Pagereference pg = new pagereference('/apex/apex/VFP_AdjustVisit'); 
        Test.setCurrentPageReference(pg);
        ApexPages.currentPage().getParameters().put('AssignedToId',cap.AssignedTo__c);
        ApexPages.currentPage().getParameters().put('capid', cap.Id);
        VFC_AdjustVisit adjvisitCon = new VFC_AdjustVisit();
        VFC_AdjustVisit.getsalesEventClosedLastFY(cap.AssignedTo__c);
        
        VFC_AdjustVisit.getsalesEventFromCAP(cap.AssignedTo__c);
        VFC_AdjustVisit.getALLevent(cap.AssignedTo__c);
        VFC_AdjustVisit.getsalesEventClosed(cap.AssignedTo__c);
        VFC_AdjustVisit.getsalesEventOpen(cap.AssignedTo__c);
        
        VFC_AdjustVisit.submitAccounts(accIdList,new list<VFC_AdjustVisit.eventtWrapper>{ewr},cap.AssignedTo__c );
        VFC_AdjustVisit.simulateImpacts(new list<VFC_AdjustVisit.eventtWrapper>{ewr});
        VFC_AdjustVisit.addAccounts();
        
        VFC_AdjustVisit.eventtWrapper   eventWrap=new VFC_AdjustVisit.eventtWrapper();
        VFC_AdjustVisit.wrapAddAccounts wrapAddAcc=new VFC_AdjustVisit.wrapAddAccounts();
        VFC_AdjustVisit.eventtWrap evtWrap=new VFC_AdjustVisit.eventtWrap();
        VFC_AdjustVisit.opptytWrap Opptywrap=new VFC_AdjustVisit.opptytWrap();
        VFC_AdjustVisit.isManager(cap.id);
        VFC_AdjustVisit.getOpptyData(acc.id);
        Map<Id,VFC_AdjustVisit.eventtWrap> evtwrapMap=new Map<Id,VFC_AdjustVisit.eventtWrap>();
        evtwrapMap.put(acc.id,evtWrap);
        VFC_AdjustVisit.geteventData(evtwrapMap,evtwrapMap,evtwrapMap,evtwrapMap,evtwrapMap,cap.id);
        
        ewr.Adjustment=1;
        ewr.AdjustmentHidden=1;
        ewr.OpenEventsOriginal=2;
        VFC_AdjustVisit.adjustvisits(new list<VFC_AdjustVisit.eventtWrapper>{ewr},cap.id,cap.AssignedTo__c);
        
    }
    public static testmethod void adjustVisitTest2()
    {
        Id userid=userInfo.getUserId();
        Account acc=Utils_TestMethods.createAccount();
        insert acc;
        Account acc2=Utils_TestMethods.createAccount();
        insert acc2;
        List<id> accIdList=new List<id>();
        accIdList.add(acc.id);
        accIdList.add(acc2.id);
        VFC_AdjustVisit.eventtWrapper ewr=new VFC_AdjustVisit.eventtWrapper();
        //ewr.Adjustment=1;
        SFE_IndivCAP__c  cap=Utils_TestMethods.createIndividualActionPlan(userid);
        insert cap;
        CS_CurrencyDecimalSeperator__c CustomSetting=new CS_CurrencyDecimalSeperator__c(Name=UserInfo.getLocale(),DecimalSeperator__c=',');
        insert CustomSetting;
        Pagereference pg = new pagereference('/apex/apex/VFP_AdjustVisit'); 
        Test.setCurrentPageReference(pg);
        ApexPages.currentPage().getParameters().put('AssignedToId',cap.AssignedTo__c);
        ApexPages.currentPage().getParameters().put('capid', cap.Id);
        VFC_AdjustVisit adjvisitCon = new VFC_AdjustVisit();
        VFC_AdjustVisit.getsalesEventClosedLastFY(cap.AssignedTo__c);
        
        VFC_AdjustVisit.getsalesEventFromCAP(cap.AssignedTo__c);
        VFC_AdjustVisit.getALLevent(cap.AssignedTo__c);
        VFC_AdjustVisit.getsalesEventClosed(cap.AssignedTo__c);
        VFC_AdjustVisit.getsalesEventOpen(cap.AssignedTo__c);
        
        VFC_AdjustVisit.submitAccounts(accIdList,new list<VFC_AdjustVisit.eventtWrapper>{ewr},cap.AssignedTo__c );
        VFC_AdjustVisit.simulateImpacts(new list<VFC_AdjustVisit.eventtWrapper>{ewr});
        VFC_AdjustVisit.addAccounts();
        VFC_AdjustVisit.adjustvisits(new list<VFC_AdjustVisit.eventtWrapper>{ewr},cap.id,cap.AssignedTo__c);
        VFC_AdjustVisit.eventtWrapper   eventWrap=new VFC_AdjustVisit.eventtWrapper();
        VFC_AdjustVisit.wrapAddAccounts wrapAddAcc=new VFC_AdjustVisit.wrapAddAccounts();
        VFC_AdjustVisit.eventtWrap evtWrap=new VFC_AdjustVisit.eventtWrap();
        VFC_AdjustVisit.opptytWrap Opptywrap=new VFC_AdjustVisit.opptytWrap();
        VFC_AdjustVisit.isManager(cap.id);
        VFC_AdjustVisit.getOpptyData(acc.id);
        
        ewr.Adjustment=0;
        ewr.AdjustmentHidden=-1;
        ewr.OpenEventsOriginal=2;
        List<Event> evtList=new List<Event>();
         Contact con = new Contact();
            con.AccountId = acc.Id;
            con.FirstName = 'test contact Fn';
            con.LastName  = 'test contact Ln';
            insert con;
        Event ev=Utils_TestMethods.createEvent(acc.id,con.id,'Planned');
        ev.RecordTypeId=Label.CLMAY13SLS01;
        ev.Activitydate=system.today();
        insert ev;
        evtList.add(ev);
        VFC_AdjustVisit.adjustvisits(new list<VFC_AdjustVisit.eventtWrapper>{ewr},cap.id,cap.AssignedTo__c);
        
        Map<Id,VFC_AdjustVisit.eventtWrap> evtwrapMap=new Map<Id,VFC_AdjustVisit.eventtWrap>();
        evtwrapMap.put(acc.id,evtWrap);
        VFC_AdjustVisit.geteventData(evtwrapMap,evtwrapMap,evtwrapMap,evtwrapMap,evtwrapMap,cap.id);
        
    }
    public static testmethod void adjustVisitTest3()
    {
        Id userid=userInfo.getUserId();
        Account acc=Utils_TestMethods.createAccount();
        insert acc;
        Account acc2=Utils_TestMethods.createAccount();
        insert acc2;
        List<id> accIdList=new List<id>();
        accIdList.add(acc.id);
        accIdList.add(acc2.id);
        VFC_AdjustVisit.eventtWrapper ewr=new VFC_AdjustVisit.eventtWrapper();
        ewr.Adjustment=1;
        ewr.accID=acc.id;
        SFE_IndivCAP__c  cap=Utils_TestMethods.createIndividualActionPlan(userid);
        insert cap;
        CS_CurrencyDecimalSeperator__c CustomSetting=new CS_CurrencyDecimalSeperator__c(Name=UserInfo.getLocale(),DecimalSeperator__c=',');
        insert CustomSetting;
        Pagereference pg = new pagereference('/apex/apex/VFP_AdjustVisit'); 
        Test.setCurrentPageReference(pg);
        ApexPages.currentPage().getParameters().put('AssignedToId',cap.AssignedTo__c);
        ApexPages.currentPage().getParameters().put('capid', cap.Id);
        VFC_AdjustVisit adjvisitCon = new VFC_AdjustVisit();
        VFC_AdjustVisit.getsalesEventClosedLastFY(cap.AssignedTo__c);
        
        VFC_AdjustVisit.getsalesEventClosed(cap.AssignedTo__c);
        VFC_AdjustVisit.getsalesEventOpen(cap.AssignedTo__c);
        
        list<VFC_AdjustVisit.eventtWrapper> ewrList=new list<VFC_AdjustVisit.eventtWrapper>();
        ewrList.add(ewr);
        VFC_AdjustVisit.submitAccounts(accIdList,new list<VFC_AdjustVisit.eventtWrapper>{ewr},cap.AssignedTo__c );
        //to cover else block
        list<VFC_AdjustVisit.eventtWrapper> ewrList2=new list<VFC_AdjustVisit.eventtWrapper>();
        List<id> accIdList2=new List<id>();
        VFC_AdjustVisit.submitAccounts(accIdList2,ewrList2,cap.AssignedTo__c );
        VFC_AdjustVisit.addAccounts();
        
        ewr.Adjustment=0;
        ewr.AdjustmentHidden=-1;
        ewr.OpenEventsOriginal=1;
        VFC_AdjustVisit.adjustvisits(ewrList,cap.id,cap.AssignedTo__c);
        ewr.OpenEventsOriginal=2;
        List<Event> evtList=new List<Event>();
         Contact con = new Contact();
            con.AccountId = acc.Id;
            con.FirstName = 'test contact Fn';
            con.LastName  = 'test contact Ln';
            insert con;
        Event ev=Utils_TestMethods.createEvent(acc.id,con.id,'Planned');
        ev.RecordTypeId=Label.CLMAY13SLS01;
        ev.Activitydate=system.today();
        ev.TECH_PlatformingScoringID__c=null;
        insert ev;
        evtList.add(ev);
        
        VFC_AdjustVisit.adjustvisits(ewrList,cap.id,cap.AssignedTo__c);
        VFC_AdjustVisit.getALLevent(cap.AssignedTo__c);
        
        VFC_AdjustVisit.getsalesEventFromCAP(cap.AssignedTo__c);
        VFC_AdjustVisit.eventtWrap evtWrap=new VFC_AdjustVisit.eventtWrap();
        Map<Id,VFC_AdjustVisit.eventtWrap> evtwrapMap=new Map<Id,VFC_AdjustVisit.eventtWrap>();
        evtwrapMap.put(acc.id,evtWrap);
        Map<Id,VFC_AdjustVisit.eventtWrap> evtwrapMap2=new Map<Id,VFC_AdjustVisit.eventtWrap>();
        evtwrapMap2.put(acc2.id,evtWrap);
        VFC_AdjustVisit.geteventData(evtwrapMap,evtwrapMap2,evtwrapMap,evtwrapMap,evtwrapMap,cap.id);
        
    }
}