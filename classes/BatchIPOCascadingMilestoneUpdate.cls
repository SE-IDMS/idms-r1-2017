/*
    Author              : Priyanka Shetty (Schneider Electric)
    Date Created        :  26-Jul-2013
    Description         : Batch class to update IPO Cascading Milestones to make Current Quarter fields populated through workflows
*/
global class BatchIPOCascadingMilestoneUpdate implements Database.Batchable<sObject>{

global String query='select Id from IPO_Strategic_Cascading_Milestone__c';

global database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query);}

global void execute(Database.BatchableContext BC, List<sObject> scope){
    List<IPO_Strategic_Cascading_Milestone__c> cms = new List<IPO_Strategic_Cascading_Milestone__c>();
    for(sObject s : scope){
            IPO_Strategic_Cascading_Milestone__c a = (IPO_Strategic_Cascading_Milestone__c)s;
            cms .add(a);
    }

    update cms;   
}
global void finish(Database.BatchableContext BC){
}

}