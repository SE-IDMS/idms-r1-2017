public with sharing class VFC_ServiceCertificateCreationCheck {
      public boolean showError {get; set;}
      public boolean showWarning {get; set;}
     
      public list<SVMXC__Service_Contract__c> smclinelist {get;set;}
      //commit
      public list<SVMXC__Service_Contract_Products__c> listCproductsOfSl {get;set;}
      public list<SVMXC__Service_Contract_Products__c> listUCCproductsOfSl {get;set;}
     
       public list<SVMXC__Service_Contract_Products__c> listCproductsNoCoverage {get;set;}
      SVMXC__Service_Contract__c contract =new SVMXC__Service_Contract__c();
      Map<id,SVMXC__Service_Contract__c> slineMap = new Map<id,SVMXC__Service_Contract__c>();
      Map<id,List<SVMXC__Service_Contract_Products__c>> slListCPMap = new Map<id,List<SVMXC__Service_Contract_Products__c>>();

        public VFC_ServiceCertificateCreationCheck(ApexPages.StandardController controller) {
              showError = false;
              showWarning = false;
              contract = (SVMXC__Service_Contract__c )controller.getRecord();
              system.debug('contract id:'+contract.id);
              smclinelist = [SELECT id,name from SVMXC__Service_Contract__c where ParentContract__c =: contract.id];
              system.debug('smclinelist:'+smclinelist);
              slineMap.putAll(smclinelist );
              system.debug('slineMap:'+slineMap);
              system.debug('keyset of slinemap:'+slineMap.keyset());
              List<SVMXC__Service_Contract_Products__c> cplist = [select id,TECH_InstalledProductWarrantyStartDate__c,SVMXC__Service_Contract__c,SVMXC__End_Date__c,SVMXC__Start_Date__c,SVMXC__Service_Contract_Products__c.SVMXC__Service_Contract__r.name,TECH_InstalledProductWarrantyEndDate__c,SVMXC__Service_Contract_Products__c.SVMXC__Installed_Product__r.name,SVMXC__Service_Contract_Products__c.SVMXC__Installed_Product__r.id, name,SVMXC__Product__c,SVMXC__Product__r.Name from SVMXC__Service_Contract_Products__c where SVMXC__Service_Contract__c in :slineMap.keyset()];
              system.debug('cplist:'+cplist);
              
              for(SVMXC__Service_Contract_Products__c cpObj: cplist ){
                  if(!slListCPMap.containskey(cpObj.SVMXC__Service_Contract__c))
                  {
                      slListCPMap.put(cpObj.SVMXC__Service_Contract__c,new List<SVMXC__Service_Contract_Products__c>{cpObj});
                  }
                  else{
                      slListCPMap.get(cpObj.SVMXC__Service_Contract__c).add(cpObj);
                  }
              
              }
              system.debug('slListCPMap:'+slListCPMap);
             
             
                
                
              list<list<SVMXC__Service_Contract_Products__c>> templist = slListCPMap.values();
              system.debug('templist:'+templist);
              listCproductsOfSl = new list<SVMXC__Service_Contract_Products__c>();
             for(list<SVMXC__Service_Contract_Products__c> templist2: templist){
                 listCproductsOfSl.addall(templist2);
             }
             system.debug('listCproductsOfSl:'+listCproductsOfSl);
             listUCCproductsOfSl = new list<SVMXC__Service_Contract_Products__c>();
             listCproductsNoCoverage = new list<SVMXC__Service_Contract_Products__c>();
              for(SVMXC__Service_Contract_Products__c tempcp: listCproductsOfSl){
                  if((tempcp.TECH_InstalledProductWarrantyEndDate__c < system.today())||(tempcp.TECH_InstalledProductWarrantyEndDate__c == null)||(tempcp.TECH_InstalledProductWarrantyStartDate__c == null)){
                      listUCCproductsOfSl.add(tempcp);
                  }
                  if(tempcp.SVMXC__Installed_Product__c == null){
                       listCproductsNoCoverage.add(tempcp);
                  }
              }
             
              if(!listCproductsNoCoverage.isempty()){
                  showError = true;
                  showWarning = false; //to be changed to false
              }else if(!listUCCproductsOfSl.isempty()){
                  showError = false; //to be changed to false
                  showWarning = true;
              }else{
                 showError = false; //to be changed to false
                  showWarning = false; 
              }
            
        }
        
        public PageReference init(){
        
            PageReference pref= null; 
        
            if(showError  == false &&  showWarning  == false){
                String url='';
                url='/apex/VFP_ServiceContractCertificate?ObjName=SVMXC.Service_Contract&id='+contract.id;
                pref  =new  PageReference(url);
                pref.setRedirect(true);
                
            }
            return pref;
        
        }
        public PageReference continue1(){
        
            PageReference pref= null; 
        
           
                String url='';
                url='/apex/VFP_ServiceContractCertificate?ObjName=SVMXC.Service_Contract&id='+contract.id;
                pref  =new  PageReference(url);
               return pref;
        
        }
        public PageReference cancel(){
        PageReference pref= null; 
        String url='';
                url='/'+contract.id;
                pref  =new  PageReference(url);
               return pref;
        
        }
        
        
        

    }