/*
    Author          : Deepak
    Date Created    : 27/01/2015 April Release
    Description     : Batch Apex class Opportunity Creation.
              
*/
    global class Batch_RenewalOpportunityCreation implements Database.Batchable<sObject>,Schedulable, Database.AllowsCallouts{

    public Integer rescheduleInterval = Integer.ValueOf(Label.CLAPR14SRV13);
    
    public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};
    
    global boolean debug = false;
    
    public String scheduledJobName = 'Batch_RenewalOpportunityCreation';
    
     global Database.QueryLocator start(Database.BatchableContext BC){ 
         
         String ServiceContractRT = System.Label.CLAPR15SRV01;
         String ServiceContractConnectedRT = System.Label.CLAPR15SRV04;
         return Database.getQueryLocator([select id,RecordTypeId,Name,OwnerId,CurrencyIsoCode,Tech_RenewalOpportunityCreat__c,LatestRenewalOpportunity__c,SoldtoAccount__c,DefaultInstalledAtAccount__c,
         OpportunitySource__c,SVMXC__End_Date__c,EarliestEndDate__c,Account_Type__c,SVMXC__Sales_Rep__c,OpportunityType__c,LeadingBusinessBU__c,SendCustomerRenewalNotification__c,
         IsEvergreen__c from SVMXC__Service_Contract__c Where LatestRenewalOpportunity__c=null AND Tech_RenewalOpportunityCreat__c=true 
         AND (RecordTypeId =: ServiceContractRT OR RecordTypeId =: ServiceContractConnectedRT)]);  
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){ 
        system.debug('scope11'+scope.size());
        AP_ServiceContract.RenewalOpportunity(scope);
        //Database.update(scope,false);
    }


    global void finish(Database.BatchableContext BC)
    {
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        System.debug('### scheduledJobDetailList: '+scheduledJobDetailList);
                     
        if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) {
            // There is at least one scheduled job already for this name
            // Looking if one of scheduled job with the correct name is in an 'Active' state
            List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
            System.debug('### scheduledJobList: '+scheduledJobList);
            if (scheduledJobList != null && scheduledJobList.size()>0) {
                canReschedule = False;
            }
        }
        if (canReschedule && !Test.isRunningTest() ) {
            System.scheduleBatch(new Batch_RenewalOpportunityCreation(), scheduledJobName, rescheduleInterval);
        }
        
    }
    global void execute(SchedulableContext sc) 
    {
        Batch_RenewalOpportunityCreation generateOppty = new Batch_RenewalOpportunityCreation();
        Database.executeBatch(generateOppty);
    }   

}