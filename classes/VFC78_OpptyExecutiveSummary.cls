/*
    Author          : Siddharth Nagavarapu (GD) 
    Date Created    : 13/02/2012
    Description     : Extension for the page VFP78_OpptyExecutiveSummary.Populates the data required in the visualforce page.
    Methods         : getactivities - queries the related activities and make them available to the page
                      getopptAmount - Since the amount cannot be displayed directly using an apex:outputField , the amount is calculated based on user default currency and opportunity default currency and displayed in the same way as it is displayed on opportunity.
                      getSalesTeam - queries the related opportunity team members and make them available to the page.
*/
public class VFC78_OpptyExecutiveSummary {

/*Variables*/
private Opportunity oppt;
public String opptAmount='';

public Decimal actualCurrency{get;set;}
public String actualCurrencyCode{get;set;}
public String actualDecimalPlacesString{get;set;}

public Decimal convertedCurrency{get;set;}
public String convertedCurrencyCode{get;set;}
public String convertedDecimalPlacesString{get;set;}

/*
Author          : Siddharth Nagavarapu (GD)
Method          : Controller
Description     : initialization of the oppt variable and make "Amount" and "created date" fields available for getopptAmount method.
*/
    public VFC78_OpptyExecutiveSummary(ApexPages.StandardController controller) {
        List<String> fieldList=new List<String>();
        fieldList.add('Amount');
        fieldList.add('CreatedDate');
        if(!test.isRunningTest())
        controller.addFields(fieldList);
        oppt=(Opportunity)controller.getRecord();               
        populateOpptAmount();    
    }   
/*
Author          : Siddharth Nagavarapu (GD)
Method          : getactivities
Description     : retrieve all the related open activities which contains 'soc' in the subject.
*/    
    public List<Task> getactivities()
    {
        List<Task> allTasks=[select owner.name,Subject,OwnerId,ActivityDate,Status from Task where WhatId=:oppt.id];
        if(allTasks.size()==0)
        {
            Task t=new Task();
            t.subject=null;
            t.status=null;
            t.whatid=null;
            t.activitydate=null;
            allTasks.add(t);
        }
        
        return allTasks;
    }
/*
Author          : Siddharth Nagavarapu (GD)
Method          : populateOpptAmount
Description     : The currency info and the conversion information is queried from DatedConversionRate,opportunity,UserInfo and processed to display the amount in this format
                  OpptCurrency Amount(UserCurrency ConvertedAmount)
*/    
public void populateOpptAmount()
{
    if(oppt.Amount!=null)
        {
            Map<String,Integer> isoCodeDecimalMap=new Map<String,Integer>();
            List<CurrencyType> allCurrencyTypes=[SELECT IsoCode,DecimalPlaces FROM CurrencyType WHERE IsoCode=:oppt.CurrencyIsoCode or IsoCode=:UserInfo.getDefaultCurrency()];
            for(CurrencyType ctype:allCurrencyTypes)
            isoCodeDecimalMap.put(ctype.IsoCode,ctype.DecimalPlaces);
            
            Decimal convertedOpptAmount=[select convertCurrency(Amount) from Opportunity where id =:oppt.id].Amount;
            
            if(oppt.CurrencyIsoCode!=UserInfo.getDefaultCurrency()){                               
               convertedOpptAmount=convertedOpptAmount.setScale(isoCodeDecimalMap.get(UserInfo.getDefaultCurrency()));
               
               convertedCurrency=convertedOpptAmount;
               convertedCurrencyCode=UserInfo.getDefaultCurrency();
               convertedDecimalPlacesString='';
               for(Integer i=0;i<isoCodeDecimalMap.get(UserInfo.getDefaultCurrency());i++)
               convertedDecimalPlacesString+='0';
               if(convertedDecimalPlacesString!='')
               convertedDecimalPlacesString='.'+convertedDecimalPlacesString;
               
               actualCurrency=oppt.Amount;
               actualCurrencyCode=oppt.CurrencyIsoCode;
               actualDecimalPlacesString='';
               for(Integer i=0;i<isoCodeDecimalMap.get(UserInfo.getDefaultCurrency());i++)
               actualDecimalPlacesString+='0';               
               if(actualDecimalPlacesString!='')
               actualDecimalPlacesString='.'+actualDecimalPlacesString;
               
               
               opptAmount=UserInfo.getDefaultCurrency()+' '+convertedOpptAmount;
               opptAmount=oppt.currencyIsoCode+' '+oppt.Amount+' ('+opptAmount+')';
               
               System.debug('#actualCurrency#'+actualCurrency);
            }                
            else{
            opptAmount=oppt.CurrencyIsoCode+' '+oppt.Amount;            
            actualCurrency=oppt.Amount;
               actualCurrencyCode=oppt.CurrencyIsoCode;
               actualDecimalPlacesString='';
               for(Integer i=0;i<isoCodeDecimalMap.get(oppt.CurrencyIsoCode);i++)
               actualDecimalPlacesString+='0';               
               if(actualDecimalPlacesString!='')
               actualDecimalPlacesString='.'+actualDecimalPlacesString;
            }
        }
}
   
/*
Author          : Siddharth Nagavarapu (GD)
Method          : getSalesTeam
Description     : Retrieve the related Opportunity Team Members
*/     
    public List<OpportunityTeamMember> getSalesTeam()
    {
        List<OpportunityTeamMember> salesteam=[select User.Name,UserId,TeamMemberRole from OpportunityTeamMember where OpportunityId=:oppt.id];
        if(salesTeam.size()==0)
        {
            OpportunityTeamMember tempOpportunityTeamMember=new OpportunityTeamMember();
            tempOpportunityTeamMember.UserId=null;
            tempOpportunityTeamMember.TeamMemberRole=null;
            salesteam.add(tempOpportunityTeamMember);
        }        
        return salesteam;        
    }
}