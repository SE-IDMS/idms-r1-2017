/********************************************************************
* Company: Fielo
* Developer: Tomás García E
* Created Date: 25/03/2015
* Description: 
********************************************************************/

public class FieloPRM_UTILS_Member{
    
    
    public static String postMoveMemberByContact( String contactIdFrom, String contactIdTo){
    
        list<Contact> contacts = new list <Contact>();
        
        contacts = [SELECT id, FieloEE__Member__c FROM Contact WHERE id =: contactIdFrom OR id =: contactIdTo LIMIT 2];
        
        Contact contactFrom = new Contact();
        Contact contactTo = new Contact();
        
        if(contacts .Size() == 2){
            for(Contact con : contacts){
                if( con.id == contactIdFrom  ){
                    contactFrom = con;
                }
                if( con.id == contactIdTo ){
                    contactTo = con;
                }
            }
            
            if(contactFrom.FieloEE__Member__c != null ){
                contactTo.FieloEE__Member__c = contactFrom.FieloEE__Member__c ;
                contactFrom.FieloEE__Member__c = null;
            }
            
            Database.SaveResult[] srListBdge = Database.update(contacts, false);
            integer countersrListBdge  = 0;
        
            for (Database.SaveResult sr : srListBdge ){
                if (!sr.isSuccess()){               
                    for(Database.Error err : sr.getErrors()){                

                        return 'error = ' + err.getMessage();

                    }
                }
            }

            //Assign user to Member
            User[] newUser = [select Id from  User where ContactId=:contactTo.Id];
            FieloEE__Member__c[] member = [select FieloEE__User__c,OwnerId from  FieloEE__Member__c where id=:contactTo.FieloEE__Member__c];
            if(newUser.Size()==1 && member.Size()==1){
                member[0].FieloEE__User__c = newUser[0].Id;
                member[0].OwnerId = newUser[0].Id;
                Database.SaveResult srListMember = Database.update(member[0], false);
                if (!srListMember.isSuccess()){               
                    for(Database.Error err : srListMember.getErrors()){                
                        return 'error = ' + err.getMessage();
                     }
                }
            }else{
                //Nothing
            }
        }

        return null; 
    }
    
    public static String postMoveMembersByAccount( String accountIdFrom, String accountIdTo){

        list<FieloEE__Member__c> Members = new list <FieloEE__Member__c>();
        
        Members = [SELECT id, F_Account__c  FROM FieloEE__Member__c WHERE F_Account__c =: accountIdFrom];        
        
        Account acc = [SELECT id FROM Account WHERE id =: accountIdFrom LIMIT 1];       
        
        if(Members.Size() > 0){
            for(FieloEE__Member__c mem : Members ){  
                if(acc != null){
                    mem.F_Account__c = accountIdTo;
                }     
            }
 
            Database.SaveResult[] srListBdge = Database.update(Members, false);
        
            for (Database.SaveResult sr : srListBdge ){
                if (!sr.isSuccess()){               
                    for(Database.Error err : sr.getErrors()){                
                        return 'error = ' + err.getMessage();
                    }
                }
            }
            
        }
        
        return null; 
    }
    
    public static String getBaseLanguage(){
       String langId = ApexPages.currentPage().getParameters().get('language');
       Cookie langCookie = ApexPages.currentPage().getCookies().get('language');
       if(langId != null){
           langCookie = new Cookie('language',langId,null,-1,false);
           ApexPages.currentPage().setCookies(new Cookie[]{langCookie});
           return langId;
       }else if(langCookie != null && langCookie.getValue() != null){
           return langCookie.getValue();
       }else{
           return UserInfo.getLanguage();
       }

       return '';
    }
    
    private static CountryChannels__c memberPrimaryChannel;
    public static CountryChannels__c getMemberPrimaryChannel(FieloEE__Member__c member){
        if(memberPrimaryChannel == null){
            if(member.F_PRM_PrimaryChannel__c != null && member.F_PRM_Channels__c != null){
                //Country Cluster search
                List<PRMCountry__c> countrycluster = [SELECT Id, Country__c, Country__r.CountryCode__c FROM PRMCountry__c WHERE CountryCode__c =: member.F_Country__r.CountryCode__c OR TECH_Countries__c LIKE: ('%' + member.F_Country__r.CountryCode__c + '%') LIMIT 1];

                if(!countrycluster.isEmpty()){
                    //Country Channel search
                    List<CountryChannels__c> countryChannelMember = [SELECT Id, F_PRM_Menu__c, F_PRM_Menu__r.FieloEE__ExternalName__c, DefaultLandingPage__c FROM CountryChannels__c WHERE SubChannelCode__c =: member.F_PRM_PrimaryChannel__c AND F_PRM_CountryCode__c =: countrycluster[0].Country__r.CountryCode__c AND F_PRM_Menu__c != null AND F_PRM_Menu__r.F_PRM_Type__c != 'Template' AND F_PRM_Menu__r.F_PRM_Status__c != 'Draft' LIMIT 1];
                    if(countryChannelMember.IsEmpty()){
                        list<string> channelsFilter = new list<String>();
                        for(string chaAux: member.F_PRM_Channels__c.split(',',0)){
                            if(chaAux != null && chaAux != ''){
                                channelsFilter.add(chaAux);
                            }
                        }
                        if(!channelsFilter.isEmpty()){
                            List<CountryChannels__c> countryChannelFilters = [SELECT Id, F_PRM_Menu__c, F_PRM_Menu__r.FieloEE__ExternalName__c, DefaultLandingPage__c FROM CountryChannels__c WHERE SubChannelCode__c LIKE: channelsFilter AND F_PRM_CountryCode__c =: member.F_Country__r.CountryCode__c AND F_PRM_Menu__c != null AND F_PRM_Menu__r.F_PRM_Type__c != 'Template' AND F_PRM_Menu__r.F_PRM_Status__c != 'Draft' ORDER BY Channel_Name__c];
                            if(!countryChannelFilters.IsEmpty()){

                                memberPrimaryChannel = countryChannelFilters[0];
                            }
                        }
                    }else{
                        memberPrimaryChannel = countryChannelMember[0];
                    }
                 }
            }
        }
        return memberPrimaryChannel;
    }
    
}