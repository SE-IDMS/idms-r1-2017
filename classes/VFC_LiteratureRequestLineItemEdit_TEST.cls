/*
Author:Ramesh Rajasekaran
Purpose:Test Methods for the class VFC_LiteratureRequestLineItemEdit
*/

@isTest
private class VFC_LiteratureRequestLineItemEdit_TEST{
    static testMethod void litTestmethod() {
    
    Account account1 = Utils_TestMethods.createAccount();
    insert account1;
    
    Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1'); 
    insert contact1;
    
    Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
    case1.CommercialReference__c= 'Commercial\\sReference\\stest';    
    case1.ProductBU__c = 'Business';    
    case1.ProductLine__c = 'ProductLine__c';    
    case1.ProductFamily__c ='ProductFamily';     
    case1.Family__c ='Family';    
    insert case1;
    
    Case case2 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
    case2.CommercialReference__c= 'Commercial\\sReference\\stest';    
    case2.ProductBU__c = 'Business';    
    case2.ProductLine__c = 'ProductLine__c';    
    case2.ProductFamily__c ='ProductFamily';     
    case2.Family__c ='Family';    
    insert case2;
    delete case2;
            
    Literaturerequest__c ltr = Utils_TestMethods.createLiteratureRequest(account1.Id, contact1.Id, case1.id);    
    insert ltr;
        
    LiteratureRequestLineItem__c ltritem = new LiteratureRequestLineItem__c(    
    CommercialReference__c= 'Commercial\\sReference\\stest',    
    Literaturerequest__c = ltr.Id,    
    Quantity__c = 1);    
    Insert ltritem;  
    
    Literaturerequest__c ltr1 = Utils_TestMethods.createLiteratureRequest(account1.Id, contact1.Id, case1.id);    
    insert ltr1;
    //ltr1.Status__c = 'Processed';
    //Update ltr1;
    
    LiteratureRequestLineItem__c ltritem1 = new LiteratureRequestLineItem__c(    
    CommercialReference__c= 'Commercial\\sReference\\stest',    
    Literaturerequest__c = ltr1.Id,    
    Quantity__c = 1);    
    Insert ltritem1; 
    
    Delete ltritem1;
    
    ApexPages.currentPage().getParameters().put('Id',ltritem.Id);  
    
    ApexPages.StandardController cc1 = new ApexPages.StandardController(ltritem);
    VFC_LiteratureRequestLineItemEdit  vfc = new VFC_LiteratureRequestLineItemEdit(cc1);
   
    }
}