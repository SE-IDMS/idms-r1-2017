public class BslUtils {

    //public static String url_DocumentServiceV002 = 'http://oreo.schneider-electric.com/bsl-fo-service/DocumentServiceV002';
    public static String url_DocumentServiceV002 = System.Label.BslApi_DocumentServiceUrl;//'https://oreo-sqe.btsec.dev.schneider-electric.com/bsl-fo-service/DocumentSecuredServiceV002';
    public static String url_downloadUrlPrefix = System.Label.BslApi_DownloadUrl;//'http://oreo-sqe.dev.schneider-electric.com/files';
    public static String url_downloadUrlPrivatePrefix = System.Label.BslApi_DownloadUrlPrivate;//'http://oreo-sqe.dev.schneider-electric.com/files';
    public static String SST_LIST_SEPARATOR = '@SST@';
    
    public static String getDownloadUrl(String audience){
        return (audience==System.Label.BslApi_PublicDownloadAudience?url_downloadUrlPrefix:url_downloadUrlPrivatePrefix)+'?p_File_Id=';
    }
    public static String getDownloadUrlByDocOid(String audience){
        return (audience==System.Label.BslApi_PublicDownloadAudience?url_downloadUrlPrefix:url_downloadUrlPrivatePrefix)+'?p_Doc_Oid=';
    }
    public static String getDownloadUrlByDocRef(String audience){
        return (audience==System.Label.BslApi_PublicDownloadAudience?url_downloadUrlPrefix:url_downloadUrlPrivatePrefix)+'?p_Doc_Ref=';
    }
    public static String getDownloadUrlByDocRefForMultipleFiles(String audience,String archiveName){
        return (audience==System.Label.BslApi_PublicDownloadAudience?url_downloadUrlPrefix:url_downloadUrlPrivatePrefix)+'?p_Archive_Name='+archiveName+'&p_Doc_Ref=';
    }

    public static String getDownloadUrlForMultipleFiles(String audience,String archiveName){
        return (audience==System.Label.BslApi_PublicDownloadAudience?url_downloadUrlPrefix:url_downloadUrlPrivatePrefix)+'?p_Archive_Name='+archiveName+'&p_File_Id=';
    }

   
    public static String getDownloadThumbnailUrlByDocRef(String audience){
        return (audience==System.Label.BslApi_PublicDownloadAudience?url_downloadUrlPrefix:url_downloadUrlPrivatePrefix)+'?p_File_Type=big%20Thumbnail&p_Doc_Ref=';
    }
    
    
    
    public static String getStaticResource(String key){
        StaticResource sr = [
                select Body
                from StaticResource
                where Name = :key
                ];
        return sr.Body.toString();
    }
    
    
    public static String retrieveSoap(HttpResponse res){
        String data= res.getBody();
        if(data==null||data.equals('')){
            return '';
        }else{
            Integer start = data.indexOf('<soap:Envelope');
            if(start==-1){
                return null;
            }else{
                Integer stop = data.indexOf('</soap:Envelope>',start);    
                if(stop==-1){
                    return null;
                }else{
                    return data.substring(start,stop+16);        
                }
            }
        }
        //multipart/related; type="application/xop+xml"; boundary="uuid:ab292cea-bd0a-497a-adc6-d811f043a5bc"; start="<root.message@cxf.apache.org>"; start-info="text/xml"  
    } 
    
    
    public static String callBsl(Boolean useMockup,String url,String methodName,Map<String,String> keyValues){
        if(useMockup!=null&&useMockup){
             return mockCall(url,methodName,keyValues);
        }else{
            return postCall(url,methodName,keyValues);
        }
    }
    public class HttpException extends Exception {}    
    public static String postCall(String url,String methodName,Map<String,String> keyValues){
        
        Boolean mockEnable =false;
        try{ mockEnable = Boolean.valueOf(System.Label.BslApi_UseMockup); }catch(Exception e){}
        
        if(mockEnable){
            return mockCall(url, methodName, keyValues);
        }else{
            //try{
            HttpRequest req = new HttpRequest();
            req.setEndpoint(url);
            req.setMethod('POST');
            req.setTimeout(120000);
            req.setHeader('Content-Type','text/xml;charset=UTF-8');
            req.setClientCertificateName(System.Label.BslApi_CertificateName);
            
            String template = BslUtils.getStaticResource(methodName);
            System.debug(Logginglevel.INFO,'template:' + template);
            //System.debug('keyValues:' + keyValues);
            for(String key : keyValues.keySet()) {
                //System.debug('LOOP:' + key); 
                String value = keyValues.get(key);
                if(value!=null){
                    template=template.replace('${'+key+'}',keyValues.get(key));    
                }
                
            }
            System.debug(Logginglevel.INFO,'BSL Message Sent:' + template);   
            req.setBody(template);
            Http http = new Http();
            HttpResponse res = http.send(req); 
            System.debug(Logginglevel.INFO,'BSL Message Received:' + res.getBody()); 
            if(res.getStatusCode()!=200){
                throw new HttpException(res.getStatus());
            }
            String soap = BslUtils.retrieveSoap(res);
            return soap;
        }
    }
    
    public static String mockCall(String url,String methodName,Map<String,String> keyValues){
        if(methodName.equals('BslApi_DocumentServiceV002_getDocumentList')){
            return BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocumentList_Response');
        }else if(methodName.equals('BslApi_DocumentServiceV002_getDocDetailsByRef')){
            return BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocDetails_Response');
        }else if(methodName.equals('BslApi_DocumentServiceV002_getDocDetails')){
            return BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocDetails_Response');
        }else if(methodName.equals('BslApi_DocumentServiceV002_getDocumentCount')){
            
            if('docType'.equals(keyValues.get('countByType'))){
                return BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocumentCount_docType_Response');    
            }else if('docTypeGroup'.equals(keyValues.get('countByType'))){
                return BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocumentCount_docTypeGroup_Response');    
            }else if('range'.equals(keyValues.get('countByType'))){
                return BslUtils.getStaticResource('BslApi_DocumentServiceV002_getDocumentCount_range_Response');    
            }
        }
        return '';
    }
    
    public class SoapException extends Exception {}    
    public static List<Map<String,String>> parseSoapResponse(String soap,List<String> expectedKeys){
        List<Map<String,String>> r = new List<Map<String,String>>();  
       
        if(soap==null){
            return new List<Map<String,String>>();  //TODO change to null;
        }else{
            System.debug('Soap Received:' + soap);   
            XmlStreamReader reader = new XmlStreamReader(soap);
            
            Map<String,String> currentMap = null;
            String currentElementName = null;
            List<String> pathList= null;
            String faultstring = null;
            Boolean fillFaultstring = false;
            while(reader.hasNext()) { 
                //System.debug('Event Type:' + reader.getEventType()); 
                if (reader.getEventType() == XmlTag.START_ELEMENT) {
                    currentElementName = reader.getLocalName();
                    if(pathList!=null){
                        pathList.add(currentElementName);
                    }
                    for(Integer i = 0,n= reader.getAttributeCount();i<n;i++){
                        String b = reader.getAttributeNamespace(i);
                        String a = reader.getAttributeLocalName(i);
                        String c = reader.getAttributeValue(b, a);
                        //System.debug('a'+a+'b'+b+'c'+c);
                        if(currentMap!=null){
                            currentMap.put(String.join( pathList, '/' )+'@'+a, c);
                        }
                    }
                    if('return'.equals(reader.getLocalName())){
                        pathList= new List<String>();
                        currentMap = new Map<String,String>();
                    }else if('faultstring'.equals(reader.getLocalName())){
                        fillFaultstring = true;
                       
                    }
                }else if (fillFaultstring&&reader.getEventType() == XmlTag.CHARACTERS) {
                    faultstring = reader.getText();
                    System.debug('Receive soap ERROR :'+faultstring);
                    throw new SoapException('Receive soap error : '+faultstring);
                    fillFaultstring = false;
                    
                } else if (reader.getEventType() == XmlTag.CHARACTERS) {
                    //System.debug('has text 5GCZE '+currentElementName+' '+reader.getText());
                    if(pathList!=null)  {
                        if(currentMap.containsKey(String.join( pathList, '/' ))){
                            currentMap.put(String.join( pathList, '/' ), currentMap.get(String.join( pathList, '/' )) + SST_LIST_SEPARATOR + reader.getText());  
                        }else{
                            currentMap.put(String.join( pathList, '/' ), reader.getText());  
                        }
                        
                    }
                    //r+=reader.getLocalName();
                    //System.debug(reader.getLocalName()); 
                } else if (reader.getEventType() == XmlTag.END_ELEMENT) {
                    if('return'.equals(reader.getLocalName())){
                        r.add(currentMap);
                    }
                    if(pathList!=null&&pathList.size()>0){
                        pathList.remove(pathList.size()-1);
                    }
                }
                reader.next(); 
            } 
            fillEmpty(r,expectedKeys);

            System.debug('#Soap Parsed:' + r); 
            for(Integer i=0;i<r.size();i++){
                System.debug('#Soap Parsed Map:' + r.get(i)); 
                Map<String,String> maps = r.get(i);
                for(String key:maps.keySet()){
                    System.debug('#Soap Parsed Element:' + key + ' : '+maps.get(key)); 
                }
            }   
            return r;
        }
        
    }
    
    public static void fillEmpty(List<Map<String,String>> maps,List<String> keys){
        for(Integer i=0,n=maps.size();i<n;i++){
            for(Integer j=0,m=keys.size();j<m;j++){
                if(!maps[i].containsKey(keys[j])){
                    maps[i].put(keys[j],'');
                }
            }
        }
    }
    
    
    public class DocumentDetailsBean {
        public String creationDate {get;set;}
        public String description {get;set;}
        public String detailsUrl {get;set;}
        public String docId {get;set;}
        public String documentDate {get;set;}
        public String docTypeName {get;set;}
        public String downloadUrl {get;set;}
        public String fileId {get;set;}
        public String fileName {get;set;}
        public String fileSize {get;set;}
        public String fileExtension {get;set;}
        public String keywords {get;set;}
        public String lastModificationDate {get;set;}
        public String reference {get;set;}
        public String title {get;set;}
        public String iconFileName {get;set;}
        public String error {get;set;}
        public String audience {get;set;}
    }
    
    public class ScopeBean {
        public String brand {set;get;}
        public String country {set;get;}
        public String channel {set;get;}
        public String program {set;get;}
        public Boolean useMockup {set;get;}
        public String error {set;get;}
        public Boolean showRecentlyAdded {set;get;}
    }
    
    public class LocaleBean {
        public String language {set;get;}
        public String country {set;get;}
    }
    
    public class DocListResultBean {
        public List<BslUtils.DocumentDetailsBean> docs {set;get;}
        public String error {set;get;}
        public String creationDate {get;set;}
        public String description {get;set;}
        public String detailsUrl {get;set;}
        public String docId {get;set;}
        public String documentDate {get;set;}
        public String docTypeName {get;set;}
        public String downloadUrl {get;set;}
        public String fileId {get;set;}
        public String fileName {get;set;}
        public String fileSize {get;set;}
        public String fileExtension {get;set;}
        public String keywords {get;set;}
        public String lastModificationDate {get;set;}
        public String reference {get;set;}
        public String title {get;set;}
        public String iconFileName {get;set;}
        

    }
    
    public class DocCountResultBean {
        public List<BslUtils.DocumentCountBean> counts {set;get;}
        public String error {set;get;}
    }
    
    public class DocumentCountBean implements Comparable {
        public String id {set;get;}
        public String label {set;get;}
        public Integer numberOfDocs {set;get;}
         public Integer hashCode() {
            return label==null?0:label.hashCode();
        }
        
        public Integer compareTo(Object obj) {
            
            if (obj instanceof DocumentCountBean) {
                DocumentCountBean p = (DocumentCountBean)obj;
                if(label==null){
                    if(p.label==null){
                         return 1;
                    }else{
                        return -1;
                    }
                }else{
                    if(p.label==null){
                        return -1;
                    }else{
                        return label.compareTo(p.label);
                    }
                }
            }else{
                return -1;
            }
            
        }
      
    }
    public static String getImageFileName(String extension){
        if(extension==null){
            return 'icon_default.png';
        }else if(extension.toLowerCase().contains('bin')){
            return 'icon_bin.png';
        }else if(extension.toLowerCase().contains('doc')){
            return 'icon_doc.png';
        }else if(extension.toLowerCase().contains('dwf')){
            return 'icon_dwf.png';
        }else if(extension.toLowerCase().contains('dwg')){
            return 'icon_dwg.png';
        }else if(extension.toLowerCase().contains('dxf')){
            return 'icon_dxf.png';
        }else if(extension.toLowerCase().contains('exe')){
            return 'icon_exe.png';
        }else if(extension.toLowerCase().contains('gif')){
            return 'icon_gif.png';
        }else if(extension.toLowerCase().contains('htm')){
            return 'icon_htm.png';
        }else if(extension.toLowerCase().contains('html')){
            return 'icon_html.png';
        }else if(extension.toLowerCase().contains('img')){
            return 'icon_img.png';
        }else if(extension.toLowerCase().contains('ipg')){
            return 'icon_ipg.ipg';
        }else if(extension.toLowerCase().contains('pdf')){
            return 'icon_pdf.png';
        }else if(extension.toLowerCase().contains('png')){
            return 'icon_png.png';
        }else if(extension.toLowerCase().contains('ppt')){
            return 'icon_ppt.png';
        }else if(extension.toLowerCase().contains('rar')){
            return 'icon_rar.png';
        }else if(extension.toLowerCase().contains('stp')){
            return 'icon_stp.png';
        }else if(extension.toLowerCase().contains('pdf')){
            return 'icon_pdf.png';
        }else if(extension.toLowerCase().contains('stp')){
            return 'icon_stp.png';
        }else if(extension.toLowerCase().contains('xls')){
            return 'icon_xls.png';    
        }else if(extension.toLowerCase().contains('xml')){
            return 'icon_xml.png';            
        }else if(extension.toLowerCase().contains('zip')){
            return 'icon_zip.png';
        }else{
            return 'icon_default.png';
        }
    
    }
        
    public static String commentBegin(String s){
        return (s==null||s.equals('')?'<!--':'');
    }
    public static String commentEnd(String s){
        return (s==null||s.equals('')?'-->':'');
    }
    
    public static DocumentDetailsBean getDocDetails(ScopeBean scope,LocaleBean locale,String federatedId,String docRef){
        DocumentDetailsBean result = new DocumentDetailsBean();
        if(scope.error!=null){
            result.error = scope.error;
        }else{
            try{
                String s = BslUtils.callBsl(
                    scope.useMockup,url_DocumentServiceV002
                    ,'BslApi_DocumentServiceV002_getDocDetailsByRef'
                    ,new Map<String,String>{
                        'brand'=>scope.brand
                            ,'country'=>scope.country
                            ,'channel'=>scope.channel
                            ,'channel_begin'=>commentBegin(scope.channel)
                            ,'channel_end'=>commentEnd(scope.channel)
                            ,'program'=>scope.program
                            ,'program_begin'=>commentBegin(scope.program)
                            ,'program_end'=>commentEnd(scope.program)
                            ,'localeLanguage'=>locale.language
                            ,'localeCountry'=>locale.country
                            ,'docRef'=>docRef
                            ,'federatedId'=>federatedId
                            
                            });
                
                List<Map<String,String>> r = BslUtils.parseSoapResponse(
                    s
                    ,new List<String>{'title','reference','description','downloadUrl'
                        ,'files/file/filename','files/file/size','keywords','documentType@translation'
                        ,'lastModificationDate','creationDate','documentDate','audience','files/file/id'}
                );
                if(r.size()>0){
                    Map<String,String> m = r.get(0);
                    if(m!=null){
                        String audience = r[0].get('audience@id');
                        if(!r[0].get('files/file/extension').contains(SST_LIST_SEPARATOR)){
                            r.get(0).put('downloadUrl',getDownloadUrl(audience));
                        }else{
                            r.get(0).put('downloadUrl',getDownloadUrlForMultipleFiles(audience,r[0].get('title')+'.zip'));
                        }
                        

                    }
                    
                    DocumentDetailsBean b = result;
                    String fileId = r[0].get('files/file/id');
                    b.fileId = (fileId!=null && fileId.contains(SST_LIST_SEPARATOR))?String.join(fileId.split(SST_LIST_SEPARATOR),','):fileId;
                    b.title = r[0].get('title');
                    b.reference = r[0].get('reference');
                    b.description = r[0].get('description');
                    b.downloadUrl = r[0].get('downloadUrl');
                    b.fileName = r[0].get('files/file/filename').contains(SST_LIST_SEPARATOR)?r[0].get('title')+'.zip':r[0].get('files/file/filename');
                    b.audience = r[0].get('audience@id');
                    b.fileSize = r[0].get('files/file/size');
                    try{
                        Decimal init=0;
                        for(String singleFileSize: b.fileSize.split(SST_LIST_SEPARATOR)){
                            init += Decimal.valueOf(singleFileSize);
                        }
                        init = init/1000000;
                        b.fileSize = String.valueOf(init.setScale(2));
                    }catch(Exception e) {}
                    
                    
                    b.fileExtension = r[0].get('files/file/extension').contains(SST_LIST_SEPARATOR)?'.ZIP':r[0].get('files/file/extension');
                    b.keywords = r[0].get('keywords');
                    b.docTypeName = r[0].get('documentType@translation');
                    b.lastModificationDate = Date.valueOf(r[0].get('lastModificationDate')).format();
                    b.creationDate = Date.valueOf(r[0].get('creationDate')).format();
                    b.documentDate = Date.valueOf(r[0].get('documentDate')).format();
                    b.iconFileName=getImageFileName(b.fileExtension);
                    b.downloadUrl+=b.fileId+'&p_enDocType='+b.docTypeName+'&p_File_Name='+b.fileName+'&p_Reference='+docRef;
                }
                
            }catch(Exception e){result.error = e.getMessage();}
        }
        return result;
    }
    
    public static DocListResultBean getDocList(ScopeBean scope,LocaleBean locale,String federatedId,String searchString,
        String docTypeGroupOids,String docTypeGroupOidList,Integer docTypeOid,Integer rangeOid,String rangeStatus,String queryBean,String firstResult,String maxResult
    ){
        DocListResultBean resu = new DocListResultBean();                                                           
        resu.docs = new List<DocumentDetailsBean>();
        if(scope.error!=null){
            resu.error = scope.error;
        }else{
            List<Map<String,String>> r = new List<Map<String,String>>();
            try{                                                               
                String s = BslUtils.callBsl(
                    scope.useMockup,
                    url_DocumentServiceV002
                    ,'BslApi_DocumentServiceV002_getDocumentList'
                    ,new Map<String,String>{
                        'brand'=>scope.brand
                            ,'country'=>scope.country
                            ,'channel'=>scope.channel
                            ,'channel_begin'=>commentBegin(scope.channel)
                            ,'channel_end'=>commentEnd(scope.channel)
                            ,'program'=>scope.program
                            ,'program_begin'=>commentBegin(scope.program)
                            ,'program_end'=>commentEnd(scope.program)
                            ,'localeLanguage'=>locale.language
                            ,'localeCountry'=>locale.country
                            ,'searchString'=>(searchString==null?'':searchString)
                            ,'firstResult'=>(firstResult==null||firstResult.equals('')?'0':firstResult)
                            ,'maxResult'=>(maxResult==null||maxResult.equals('')?'6':maxResult)
                            ,'docTypeGroupOid'=>docTypeGroupOids
                            ,'docTypeGroupOid_begin'=>(docTypeGroupOids==null||docTypeGroupOids.equals('')?'<!--':'')
                            ,'docTypeGroupOid_end'=>(docTypeGroupOids==null||docTypeGroupOids.equals('')?'-->':'')
                            ,'docTypeGroupOidList'=>docTypeGroupOidList
                            ,'docTypeGroupOidList_begin'=>(docTypeGroupOidList==null||docTypeGroupOidList.equals('')?'<!--':'')
                            ,'docTypeGroupOidList_end'=>(docTypeGroupOidList==null||docTypeGroupOidList.equals('')?'-->':'')
                            ,'docTypeOid'=>String.valueOf(docTypeOid)
                            ,'docTypeOid_begin'=>(docTypeOid==0?'<!--':'')
                            ,'docTypeOid_end'=>(docTypeOid==0?'-->':'')
                            ,'rangeStatus'=>(rangeStatus==null?'':rangeStatus)
                            ,'rangeStatus_begin'=>(rangeStatus==null||rangeStatus.equals('')?'<!--':'')
                            ,'rangeStatus_end'=>(rangeStatus==null||rangeStatus.equals('')?'-->':'')
                            ,'ranges'=>String.valueOf(rangeOid)
                            ,'ranges_begin'=>(rangeOid==0?'<!--':'')
                            ,'ranges_end'=>(rangeOid==0?'-->':'')
                            ,'queryBean'=>(queryBean==null?'':queryBean)
                            ,'federatedId'=>federatedId
                            });
                r = BslUtils.parseSoapResponse(s,new List<String>{'title','reference','docId','detailsUrl','documentDate','documentType@translation','audience'});
                if(r.size()>0){
                    Map<String,String> m = r.get(0);
                    if(m!=null){
                        String docId = m.get('docId');
                        r.get(0).put('detailsUrl','?page=docDetails&docOid='+docId);
                    }
                    for(Integer i =0,n=r.size();i<n;i++){
                        DocumentDetailsBean b = new DocumentDetailsBean();
                        b.title = r[i].get('title');
                        b.reference = r[i].get('reference');
                        b.docId = r[i].get('docId');
                        b.detailsUrl = r[i].get('detailsUrl');
                        b.documentDate = Date.valueOf(r[i].get('documentDate')).format();
                        b.description = r[i].get('description');
                        b.fileExtension = r[i].get('files/file/extension').contains(SST_LIST_SEPARATOR)?'.ZIP':r[i].get('files/file/extension');
                        String fileId = r[0].get('files/file/id');
                        b.fileId = (fileId!=null && fileId.contains(SST_LIST_SEPARATOR))?String.join(fileId.split(SST_LIST_SEPARATOR),','):fileId;
                        b.fileName = r[i].get('files/file/filename').contains(SST_LIST_SEPARATOR)?b.title+'.zip':r[i].get('files/file/filename');
                        b.fileSize = r[i].get('files/file/size');
                        b.audience = r[i].get('audience@id');
                        try{
                            Decimal init=0;
                            for(String singleFileSize: b.fileSize.split(SST_LIST_SEPARATOR)){
                                init += Decimal.valueOf(singleFileSize);
                            }
                            init = init/1000000;
                            b.fileSize = String.valueOf(init.setScale(2));
                        }catch(Exception e) {}
                        
                        b.docTypeName = r[i].get('documentType@translation');
                        b.iconFileName=getImageFileName(b.fileExtension);
                        resu.docs.add(b);
                    }
                }                                          
            }catch(Exception e){
                resu.error = e.getMessage();
            }
        }
        return resu;
    }
    
    public static DocCountResultBean getDocCount(ScopeBean scope,LocaleBean locale,String docTypeGroupOids,Integer docTypeOid,String countByType,String rangeStatus,String federatedId){
        DocCountResultBean resu = new DocCountResultBean();
        resu.counts = new List<DocumentCountBean>();
        if(scope.error!=null){
            resu.error = scope.error;
        }else{
            try{
                String s = BslUtils.callBsl(
                    scope.useMockup,url_DocumentServiceV002
                    ,'BslApi_DocumentServiceV002_getDocumentCount'
                    ,new Map<String,String>{
                        'brand'=>scope.brand
                            ,'country'=>scope.country
                            ,'channel'=>scope.channel
                            ,'channel_begin'=>commentBegin(scope.channel)
                            ,'channel_end'=>commentEnd(scope.channel)
                            ,'program'=>scope.program
                            ,'program_begin'=>commentBegin(scope.program)
                            ,'program_end'=>commentEnd(scope.program)
                            ,'localeLanguage'=>locale.language
                            ,'localeCountry'=>locale.country
                            ,'docTypeGroupOid'=>docTypeGroupOids
                            ,'docTypeGroupOid_begin'=>commentBegin(docTypeGroupOids)
                            ,'docTypeGroupOid_end'=>commentEnd(docTypeGroupOids)
                            ,'docTypeOid'=>String.valueOf(docTypeOid)
                            ,'docTypeOid_begin'=>(docTypeOid==0?'<!--':'')
                            ,'docTypeOid_end'=>(docTypeOid==0?'-->':'')
                            ,'countByType'=>countByType
                            ,'rangeStatus'=>rangeStatus
                            ,'rangeStatus_begin'=>(rangeStatus.equals('')?'<!--':'')
                            ,'rangeStatus_end'=>(rangeStatus.equals('')?'-->':'')
                            ,'federatedId'=>federatedId
                            });
                List<Map<String,String>> r = BslUtils.parseSoapResponse(s,new List<String>{'id','label','numberOfDocs'});
                
                for(Integer i =0,n=r.size();i<n;i++){
                    DocumentCountBean b = new DocumentCountBean();
                    b.id = r[i].get('id');
                    b.label = r[i].get('label');
                    b.numberOfDocs = Integer.valueOf(r[i].get('numberOfDocs'));
                    resu.counts.add(b);
                }
                resu.counts.sort();
            }catch(Exception e){
                resu.error = e.getMessage();
            }
        }
        return resu;
    }
    
    public static BslUtils.ScopeBean findBslScope(String categoryId){
        BslUtils.ScopeBean scope = new BslUtils.ScopeBean();
        
        PRM_SST_Conf__c bs = null;
        try{
             bs = [SELECT brand__c,country__c,program__c,channel__c,Use_Mockup__c,showRecentlyAdded__c FROM PRM_SST_Conf__c where FieloCategory__c = :categoryId];
        }catch(Exception e){
            scope.error = 'cant find configuration record in PRM_SST_Conf__c for category :'+categoryId;
        }
        
        if(bs!=null){
            if(bs.channel__c!=null){scope.channel = bs.channel__c.escapeHtml4();}
            if(bs.program__c!=null){scope.program = bs.program__c.escapeHtml4();}
            if(bs.brand__c!=null){scope.brand = bs.brand__c.escapeHtml4();}
            if(bs.country__c!=null){scope.country = bs.country__c.escapeHtml4();}
            if(bs.Use_Mockup__c!=null){scope.useMockup = bs.Use_Mockup__c;}
            scope.showRecentlyAdded =  bs.showRecentlyAdded__c;
        }
        return scope;
    }
    
    public static String getDocTypeGroupBslOids(String categoryId){
        PRM_SST_Conf__c bs = null;
        try{
            bs = [SELECT DocTypeGroupBslOids__c FROM PRM_SST_Conf__c where FieloCategory__c = :categoryId];
        }catch(Exception e){}
        String result=null;
        if(bs!=null){
            result = bs.DocTypeGroupBslOids__c;
        }
        return result;
    }
    
    public static String getRecentlyAddedDocTypeGroupBslOids(String categoryId){
        PRM_SST_Conf__c bs = null;
        try{
            bs = [SELECT RecentlyAddedDocTypeGroupBslOids__c FROM PRM_SST_Conf__c where FieloCategory__c = :categoryId];
        }catch(Exception e){}
        String result=null;
        if(bs!=null){
            result = bs.RecentlyAddedDocTypeGroupBslOids__c;
        }
        
        
        
        return result;
    }
    
    public static String toDocTypeGroupBslOidXmlListString(String docTypeGroupBslOidListString){
        System.debug('toDocTypeGroupBslOidXmlListString 1: ' + docTypeGroupBslOidListString);
        
        String r = '';
        if(docTypeGroupBslOidListString!=null&&!''.equals(docTypeGroupBslOidListString)){
            String qb = '<docTypeGroups>${docTypeGroupOid}</docTypeGroups>';
            String[] docTypeGroupStrings =  docTypeGroupBslOidListString.split(',');
            if(docTypeGroupStrings!=null &&docTypeGroupStrings.size()>0){
                for(Integer i =0,n=docTypeGroupStrings.size();i<n;i++){
                    if(docTypeGroupStrings[i]!=null&&!''.equals(docTypeGroupStrings[i])){
                        r+=qb.replace('${docTypeGroupOid}',docTypeGroupStrings[i]);
                    }
                }
            }
        }
        System.debug('toDocTypeGroupBslOidXmlListString 2: ' + r);
        return r;
    }
        
    }