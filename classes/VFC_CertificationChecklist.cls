public with sharing class VFC_CertificationChecklist 
{
    public Map<String, String> questionsMap{get;set;}
    public CertificationChecklist__c certChecklist{get;set;}
    public Map<String, CS_RiskValues__c> rskValues{get;set;}
    public string AccessRights_Comments{get;set;}
    public string AlignmentwithIPO_Comments{get;set;}
    public string Anticipationfut_Comments{get;set;}
    public string ApplicationOwner_Comments{get;set;}
    public string BatchPlanGlobal_Comments{get;set;}
    public string BatchPlanSE_Comments{get;set;}
    public string BusinessOwner_Comments{get;set;}
    public string ConnTobFO_Comments{get;set;}
    public string ConnTobFO1_Comments{get;set;}
    public string ConnTobFO2_Comments{get;set;}
    public string ConnectionToSE_Comments{get;set;}
    public string CustomWrittenCe_Comments{get;set;}
    public string DataBreach_Comments{get;set;}
    public string DataCollectionFm_Comments{get;set;}
    public string DataPrivQuestion_Comments{get;set;}
    public string DataRetention_Comments{get;set;}
    public string DisasterRecovery_Comments{get;set;}
    public string DisasterRecovery1_Comments{get;set;}
    public string Encryption_Comments{get;set;}
    public string EntMobMgnt_Comments{get;set;}
    public string ExtractionPerData_Comments{get;set;}
    public string ImpactStudyGlobal_Comments{get;set;}
    public string ImpactStudySE_Comments{get;set;}
    public string KnowledgeTransfer_Comments{get;set;}
    public string LackOfAlignment_Comments{get;set;}
    public string LocalDeployment_Comments{get;set;}
    public string LocalDeployment1_Comments{get;set;}
    public string MobileCoding_Comments{get;set;}
    public string MobileStorage_Comments{get;set;}
    public string MonitoringGlobal_Comments{get;set;}
    public string MonitoringSE_Comments{get;set;}
    public string AdminNonDisc_Comments{get;set;}
    public string PersDataQual_Comments{get;set;}
    public string PrivacyNotice_Comments{get;set;}
    public string ProjScope_Comments{get;set;}
    public string RelavancePerDt_Comments{get;set;}
    public string SaaSCloudSec_Comments{get;set;}
    public string SafeHosting_Comments{get;set;}
    public string SENetwork_Comments{get;set;}
    public string ServiceLevel_Comments{get;set;}
    public string SignOfContract_Comments{get;set;}
    public string SizingGlobal_Comments{get;set;}
    public string SizingSE_Comments{get;set;}
    public string AppCriticality_Comments{get;set;}
    public string SupportModel_Comments{get;set;}
    public string SupportModelGlobal_Comments{get;set;}
    public string SupportModelSE_Comments{get;set;}
    public string SupportTeamIden_Comments{get;set;}
    public string UnapprCldInfra_Comments{get;set;}
    public string UserAuthentication_Comments{get;set;}
    public string UsersChoice_Comments{get;set;}
    public boolean dispStatus{get;set;}
    public string urlValue{get;set;}
    public Map<String, CS_RskAssessmentMappingVals__c> mappingFields{get;set;}
    public boolean dispInputFields{get;set;}
    public boolean filterKO{get;set;}
    public boolean unfilterAll{get;set;}    
    //CONSTRUCTOR
    public VFC_CertificationChecklist(ApexPages.StandardController controller) 
    {
        certChecklist = (CertificationChecklist__c)controller.getRecord();
        certChecklist = database.query(queryAllFields());
        rskValues = CS_RiskValues__c.getAll(); 
        mappingFields = CS_RskAssessmentMappingVals__c.getAll();
        //CHECKS IF THE LOGGED IN USER IS MEMBER OF CORE TEAM
        List<Group> groups = [select Id,Name from Group where Name=:Label.CLOCT15CFW57];
        if(groups!=null && groups.size()==1)
        {
            List<GroupMember> groupMembs = [Select Id, UserOrGroupId From GroupMember Where GroupId = :groups[0].Id and UserOrGroupId =: UserInfo.getUserId()];
            if(groupMembs!=null && groupMembs.size()==1)
                dispStatus = true;
        }               
        unfilterAll = true;
    }
    
    //SAVES THE CERTIFICATION CHECKLIST AND REDIRECTS TO CERTIFICATON CHECKLIST PAGE
    public pagereference save()
    {
       Pagereference pg;
       
       //APPENDS EXISTING COMMENTS TO THE COMMENTS FIELD OF EACH SECTION       
       if(ProjScope_Comments!=null && ProjScope_Comments.length()>0 && ProjScope_Comments!='&nbsp;')
       {
           if(certChecklist.ProjScope_Comments__c == null)
               certChecklist.ProjScope_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ProjScope_Comments+' \r\n';
           else
               certChecklist.ProjScope_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss')     +': '+ ProjScope_Comments + ' \r\n'+certChecklist.ProjScope_Comments__c;     
       }
       if(AccessRights_Comments!=null && AccessRights_Comments.length()>0 && AccessRights_Comments!='&nbsp;')
       {
           if(certChecklist.AccessRights_Comments__c == null)
               certChecklist.AccessRights_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ AccessRights_Comments+' \r\n';
           else
               certChecklist.AccessRights_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ AccessRights_Comments + ' \r\n'+certChecklist.AccessRights_Comments__c;     
       }
       if(AlignmentwithIPO_Comments!=null && AlignmentwithIPO_Comments.length()>0 && AlignmentwithIPO_Comments!='&nbsp;')
       {
           if(certChecklist.AlignmentwithIPO_Comments__c == null)
               certChecklist.AlignmentwithIPO_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ AlignmentwithIPO_Comments+' \r\n';
           else
               certChecklist.AlignmentwithIPO_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ AlignmentwithIPO_Comments + ' \r\n'+certChecklist.AlignmentwithIPO_Comments__c;     
       }
       if(Anticipationfut_Comments!=null && Anticipationfut_Comments.length()>0 && Anticipationfut_Comments!='&nbsp;')
       {
           if(certChecklist.Anticipationfut_Comments__c == null)
               certChecklist.Anticipationfut_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ Anticipationfut_Comments+' \r\n';
           else
               certChecklist.Anticipationfut_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ Anticipationfut_Comments + ' \r\n'+certChecklist.Anticipationfut_Comments__c;     
       }
       if(ApplicationOwner_Comments!=null && ApplicationOwner_Comments.length()>0 && ApplicationOwner_Comments!='&nbsp;')
       {
           if(certChecklist.ApplicationOwner_Comments__c == null)
               certChecklist.ApplicationOwner_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ApplicationOwner_Comments+' \r\n';
           else
               certChecklist.ApplicationOwner_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ApplicationOwner_Comments + ' \r\n'+certChecklist.ApplicationOwner_Comments__c;     
       }
       if(BatchPlanGlobal_Comments!=null && BatchPlanGlobal_Comments.length()>0 && BatchPlanGlobal_Comments!='&nbsp;')
       {
           if(certChecklist.BatchPlanGlobal_Comments__c == null)
               certChecklist.BatchPlanGlobal_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ BatchPlanGlobal_Comments+' \r\n';
           else
               certChecklist.BatchPlanGlobal_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ BatchPlanGlobal_Comments + ' \r\n'+certChecklist.BatchPlanGlobal_Comments__c;     
       }
       if(BatchPlanSE_Comments!=null && BatchPlanSE_Comments.length()>0 && BatchPlanSE_Comments!='&nbsp;')
       {
           if(certChecklist.BatchPlanSE_Comments__c == null)
               certChecklist.BatchPlanSE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ BatchPlanSE_Comments+' \r\n';
           else
               certChecklist.BatchPlanSE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ BatchPlanSE_Comments + ' \r\n'+certChecklist.BatchPlanSE_Comments__c;     
       }
       if(BusinessOwner_Comments!=null && BusinessOwner_Comments.length()>0 && BusinessOwner_Comments!='&nbsp;')
       {
           if(certChecklist.BusinessOwner_Comments__c == null)
               certChecklist.BusinessOwner_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ BusinessOwner_Comments+' \r\n';
           else
               certChecklist.BusinessOwner_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ BusinessOwner_Comments + ' \r\n'+certChecklist.BusinessOwner_Comments__c;     
       }
       if(ConnTobFO_Comments!=null && ConnTobFO_Comments.length()>0 && ConnTobFO_Comments!='&nbsp;')
       {
           if(certChecklist.ConnTobFO_Comments__c == null)
               certChecklist.ConnTobFO_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ConnTobFO_Comments+' \r\n';
           else
               certChecklist.ConnTobFO_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ConnTobFO_Comments + ' \r\n'+certChecklist.ConnTobFO_Comments__c;     
       }
       if(ConnTobFO1_Comments!=null && ConnTobFO1_Comments.length()>0 && ConnTobFO1_Comments!='&nbsp;')
       {
           if(certChecklist.ConnTobFO1_Comments__c == null)
               certChecklist.ConnTobFO1_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ConnTobFO1_Comments+' \r\n';
           else
               certChecklist.ConnTobFO1_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ConnTobFO1_Comments + ' \r\n'+certChecklist.ConnTobFO1_Comments__c;     
       }
       if(ConnTobFO2_Comments!=null && ConnTobFO2_Comments.length()>0 && ConnTobFO2_Comments!='&nbsp;')
       {
           if(certChecklist.ConnTobFO2_Comments__c == null)
               certChecklist.ConnTobFO2_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ConnTobFO2_Comments+' \r\n';
           else
               certChecklist.ConnTobFO2_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ConnTobFO2_Comments + ' \r\n'+certChecklist.ConnTobFO2_Comments__c;     
       }
       if(ConnectionToSE_Comments!=null && ConnectionToSE_Comments.length()>0 && ConnectionToSE_Comments!='&nbsp;')
       {
           if(certChecklist.ConnectionToSE_Comments__c == null)
               certChecklist.ConnectionToSE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ConnectionToSE_Comments+' \r\n';
           else
               certChecklist.ConnectionToSE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ConnectionToSE_Comments + ' \r\n'+certChecklist.ConnectionToSE_Comments__c;     
       }
       if(CustomWrittenCe_Comments!=null && CustomWrittenCe_Comments.length()>0 && CustomWrittenCe_Comments!='&nbsp;')
       {
           if(certChecklist.CustomWrittenCe_Comments__c == null)
               certChecklist.CustomWrittenCe_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ CustomWrittenCe_Comments+' \r\n';
           else
               certChecklist.CustomWrittenCe_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ CustomWrittenCe_Comments + ' \r\n'+certChecklist.CustomWrittenCe_Comments__c;     
       }
       if(DataBreach_Comments!=null && DataBreach_Comments.length()>0 && DataBreach_Comments!='&nbsp;')
       {
           if(certChecklist.DataBreach_Comments__c == null)
               certChecklist.DataBreach_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ DataBreach_Comments+' \r\n';
           else
               certChecklist.DataBreach_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ DataBreach_Comments + ' \r\n'+certChecklist.DataBreach_Comments__c;     
       }
       if(DataCollectionFm_Comments!=null && DataCollectionFm_Comments.length()>0 && DataCollectionFm_Comments!='&nbsp;')
       {
           if(certChecklist.DataCollectionFm_Comments__c == null)
               certChecklist.DataCollectionFm_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ DataCollectionFm_Comments+' \r\n';
           else
               certChecklist.DataCollectionFm_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ DataCollectionFm_Comments + ' \r\n'+certChecklist.DataCollectionFm_Comments__c;     
       }
       if(DataPrivQuestion_Comments!=null && DataPrivQuestion_Comments.length()>0 && DataPrivQuestion_Comments!='&nbsp;')
       {
           if(certChecklist.DataPrivQuestion_Comments__c == null)
               certChecklist.DataPrivQuestion_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ DataPrivQuestion_Comments+' \r\n';
           else
               certChecklist.DataPrivQuestion_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ DataPrivQuestion_Comments + ' \r\n'+certChecklist.DataPrivQuestion_Comments__c;     
       }
       if(DisasterRecovery_Comments!=null && DisasterRecovery_Comments.length()>0 && DisasterRecovery_Comments!='&nbsp;')
       {
           if(certChecklist.DisasterRecovery_Comments__c == null)
               certChecklist.DisasterRecovery_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ DisasterRecovery_Comments+' \r\n';
           else
               certChecklist.DisasterRecovery_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ DisasterRecovery_Comments + ' \r\n'+certChecklist.DisasterRecovery_Comments__c;     
       }
       if(DisasterRecovery1_Comments!=null && DisasterRecovery1_Comments.length()>0 && DisasterRecovery1_Comments!='&nbsp;')
       {
           if(certChecklist.DisasterRecovery1_Comments__c == null)
               certChecklist.DisasterRecovery1_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ DisasterRecovery1_Comments+' \r\n';
           else
               certChecklist.DisasterRecovery1_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ DisasterRecovery1_Comments + ' \r\n'+certChecklist.DisasterRecovery1_Comments__c;     
       }
       if(Encryption_Comments!=null && Encryption_Comments.length()>0 && Encryption_Comments!='&nbsp;')
       {
           if(certChecklist.Encryption_Comments__c == null)
               certChecklist.Encryption_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ Encryption_Comments+' \r\n';
           else
               certChecklist.Encryption_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ Encryption_Comments + ' \r\n'+certChecklist.Encryption_Comments__c;     
       }
       if(EntMobMgnt_Comments!=null && EntMobMgnt_Comments.length()>0 && EntMobMgnt_Comments!='&nbsp;')
       {
           if(certChecklist.EntMobMgnt_Comments__c == null)
               certChecklist.EntMobMgnt_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ EntMobMgnt_Comments+' \r\n';
           else
               certChecklist.EntMobMgnt_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ EntMobMgnt_Comments + ' \r\n'+certChecklist.EntMobMgnt_Comments__c;     
       }
       if(ImpactStudyGlobal_Comments!=null && ImpactStudyGlobal_Comments.length()>0 && ImpactStudyGlobal_Comments!='&nbsp;')
       {
           if(certChecklist.ImpactStudyGlobal_Comments__c == null)
               certChecklist.ImpactStudyGlobal_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ImpactStudyGlobal_Comments+' \r\n';
           else
               certChecklist.ImpactStudyGlobal_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ImpactStudyGlobal_Comments + ' \r\n'+certChecklist.ImpactStudyGlobal_Comments__c;     
       }
       if(ImpactStudySE_Comments!=null && ImpactStudySE_Comments.length()>0 && ImpactStudySE_Comments!='&nbsp;')
       {
           if(certChecklist.ImpactStudySE_Comments__c == null)
               certChecklist.ImpactStudySE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ImpactStudySE_Comments+' \r\n';
           else
               certChecklist.ImpactStudySE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ImpactStudySE_Comments + ' \r\n'+certChecklist.ImpactStudySE_Comments__c;     
       }
       if(KnowledgeTransfer_Comments!=null && KnowledgeTransfer_Comments.length()>0 && KnowledgeTransfer_Comments!='&nbsp;')
       {
           if(certChecklist.KnowledgeTransfer_Comments__c == null)
               certChecklist.KnowledgeTransfer_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ KnowledgeTransfer_Comments+' \r\n';
           else
               certChecklist.KnowledgeTransfer_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ KnowledgeTransfer_Comments + ' \r\n'+certChecklist.KnowledgeTransfer_Comments__c;     
       }
       if(LackOfAlignment_Comments!=null && LackOfAlignment_Comments.length()>0 && LackOfAlignment_Comments!='&nbsp;')
       {
           if(certChecklist.LackOfAlignment_Comments__c == null)
               certChecklist.LackOfAlignment_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ LackOfAlignment_Comments+' \r\n';
           else
               certChecklist.LackOfAlignment_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ LackOfAlignment_Comments + ' \r\n'+certChecklist.LackOfAlignment_Comments__c;     
       }
       if(MobileCoding_Comments!=null && MobileCoding_Comments.length()>0 && MobileCoding_Comments!='&nbsp;')
       {
           if(certChecklist.MobileCoding_Comments__c == null)
               certChecklist.MobileCoding_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ MobileCoding_Comments+' \r\n';
           else
               certChecklist.MobileCoding_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ MobileCoding_Comments + ' \r\n'+certChecklist.MobileCoding_Comments__c;     
       }
       if(MobileStorage_Comments!=null && MobileStorage_Comments.length()>0 && MobileStorage_Comments!='&nbsp;')
       {
           if(certChecklist.MobileStorage_Comments__c == null)
               certChecklist.MobileStorage_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ MobileStorage_Comments+' \r\n';
           else
               certChecklist.MobileStorage_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ MobileStorage_Comments + ' \r\n'+certChecklist.MobileStorage_Comments__c;     
       }
       if(MonitoringGlobal_Comments!=null && MonitoringGlobal_Comments.length()>0 && MonitoringGlobal_Comments!='&nbsp;')
       {
           if(certChecklist.MonitoringGlobal_Comments__c == null)
               certChecklist.MonitoringGlobal_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ MonitoringGlobal_Comments+' \r\n';
           else
               certChecklist.MonitoringGlobal_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ MonitoringGlobal_Comments + ' \r\n'+certChecklist.MonitoringGlobal_Comments__c;     
       }
       if(MonitoringSE_Comments!=null && MonitoringSE_Comments.length()>0 && MonitoringSE_Comments!='&nbsp;')
       {
           if(certChecklist.MonitoringSE_Comments__c == null)
               certChecklist.MonitoringSE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ MonitoringSE_Comments+' \r\n';
           else
               certChecklist.MonitoringSE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ MonitoringSE_Comments + ' \r\n'+certChecklist.MonitoringSE_Comments__c;     
       }
       if(SaaSCloudSec_Comments!=null && SaaSCloudSec_Comments.length()>0 && SaaSCloudSec_Comments!='&nbsp;')
       {
           if(certChecklist.SaaSCloudSec_Comments__c == null)
               certChecklist.SaaSCloudSec_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SaaSCloudSec_Comments+' \r\n';
           else
               certChecklist.SaaSCloudSec_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SaaSCloudSec_Comments + ' \r\n'+certChecklist.SaaSCloudSec_Comments__c;     
       }
       if(SafeHosting_Comments!=null && SafeHosting_Comments.length()>0 && SafeHosting_Comments!='&nbsp;')
       {
           if(certChecklist.SafeHosting_Comments__c == null)
               certChecklist.SafeHosting_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SafeHosting_Comments+' \r\n';
           else
               certChecklist.SafeHosting_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SafeHosting_Comments + ' \r\n'+certChecklist.SafeHosting_Comments__c;     
       }
       if(SENetwork_Comments!=null && SENetwork_Comments.length()>0 && SENetwork_Comments!='&nbsp;')
       {
           if(certChecklist.SENetwork_Comments__c == null)
               certChecklist.SENetwork_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SENetwork_Comments+' \r\n';
           else
               certChecklist.SENetwork_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SENetwork_Comments + ' \r\n'+certChecklist.SENetwork_Comments__c;     
       }
       if(ServiceLevel_Comments!=null && ServiceLevel_Comments.length()>0 && ServiceLevel_Comments!='&nbsp;')
       {
           if(certChecklist.ServiceLevel_Comments__c == null)
               certChecklist.ServiceLevel_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ServiceLevel_Comments+' \r\n';
           else
               certChecklist.ServiceLevel_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ ServiceLevel_Comments + ' \r\n'+certChecklist.ServiceLevel_Comments__c;     
       }
       if(SizingGlobal_Comments!=null && SizingGlobal_Comments.length()>0 && SizingGlobal_Comments!='&nbsp;')
       {
           if(certChecklist.SizingGlobal_Comments__c == null)
               certChecklist.SizingGlobal_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SizingGlobal_Comments+' \r\n';
           else
               certChecklist.SizingGlobal_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SizingGlobal_Comments + ' \r\n'+certChecklist.SizingGlobal_Comments__c;     
       }
       if(SizingSE_Comments!=null && SizingSE_Comments.length()>0 && SizingSE_Comments!='&nbsp;')
       {
           if(certChecklist.SizingSE_Comments__c == null)
               certChecklist.SizingSE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SizingSE_Comments+' \r\n';
           else
               certChecklist.SizingSE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SizingSE_Comments + ' \r\n'+certChecklist.SizingSE_Comments__c;     
       }
       if(AppCriticality_Comments!=null && AppCriticality_Comments.length()>0 && AppCriticality_Comments!='&nbsp;')
       {
           if(certChecklist.AppCriticality_Comments__c == null)
               certChecklist.AppCriticality_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ AppCriticality_Comments+' \r\n';
           else
               certChecklist.AppCriticality_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ AppCriticality_Comments + ' \r\n'+certChecklist.AppCriticality_Comments__c;     
       }
       if(SupportModel_Comments!=null && SupportModel_Comments.length()>0 && SupportModel_Comments!='&nbsp;')
       {
           if(certChecklist.SupportModel_Comments__c == null)
               certChecklist.SupportModel_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SupportModel_Comments+' \r\n';
           else
               certChecklist.SupportModel_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SupportModel_Comments + ' \r\n'+certChecklist.SupportModel_Comments__c;     
       }
       if(SupportModelGlobal_Comments!=null && SupportModelGlobal_Comments.length()>0 && SupportModelGlobal_Comments!='&nbsp;')
       {
           if(certChecklist.SupportModelGlobal_Comments__c == null)
               certChecklist.SupportModelGlobal_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SupportModelGlobal_Comments+' \r\n';
           else
               certChecklist.SupportModelGlobal_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SupportModelGlobal_Comments + ' \r\n'+certChecklist.SupportModelGlobal_Comments__c;     
       }
       if(SupportModelSE_Comments!=null && SupportModelSE_Comments.length()>0 && SupportModelSE_Comments!='&nbsp;')
       {
           if(certChecklist.SupportModelSE_Comments__c == null)
               certChecklist.SupportModelSE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SupportModelSE_Comments+' \r\n';
           else
               certChecklist.SupportModelSE_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SupportModelSE_Comments + ' \r\n'+certChecklist.SupportModelSE_Comments__c;     
       }
       if(SupportTeamIden_Comments!=null && SupportTeamIden_Comments.length()>0 && SupportTeamIden_Comments!='&nbsp;')
       {
           if(certChecklist.SupportTeamIden_Comments__c == null)
               certChecklist.SupportTeamIden_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SupportTeamIden_Comments+' \r\n';
           else
               certChecklist.SupportTeamIden_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ SupportTeamIden_Comments + ' \r\n'+certChecklist.SupportTeamIden_Comments__c;     
       }
       if(UnapprCldInfra_Comments!=null && UnapprCldInfra_Comments.length()>0 && UnapprCldInfra_Comments!='&nbsp;')
       {
           if(certChecklist.UnapprCldInfra_Comments__c == null)
               certChecklist.UnapprCldInfra_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ UnapprCldInfra_Comments+' \r\n';
           else
               certChecklist.UnapprCldInfra_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ UnapprCldInfra_Comments + ' \r\n'+certChecklist.UnapprCldInfra_Comments__c;     
       }
       if(UserAuthentication_Comments!=null && UserAuthentication_Comments.length()>0 && UserAuthentication_Comments!='&nbsp;')
       {
           if(certChecklist.UserAuthentication_Comments__c == null)
               certChecklist.UserAuthentication_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ UserAuthentication_Comments+' \r\n';
           else
               certChecklist.UserAuthentication_Comments__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ UserAuthentication_Comments + ' \r\n'+certChecklist.UserAuthentication_Comments__c;     
       }
        Database.SaveResult results = database.update(certChecklist,false);
        if(!(results.issuccess()))
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,results.getErrors()[0].getMessage());
            ApexPages.addMessage(myMsg);
            pg = null;
        }    
        else 
        {   
            pg = new pagereference('/apex/VFP_CertificationChecklist?id='+certChecklist.Id);        
            pg.setRedirect(true);
        }
        return pg;
    }
    
    //QUERY ALL CERTFICATION CHECKLIST FIELDS
    public String queryAllFields()
    {
        Schema.DescribeSObjectResult descRes =  CertificationChecklist__c.sObjectType.getDescribe();
        List<Schema.SObjectField> tempFields = descRes.fields.getMap().values();
        List<Schema.DescribeFieldResult> fields  = new List<Schema.DescribeFieldResult>();
        for(Schema.SObjectField sof : tempFields)
        {
            fields.add(sof.getDescribe());
        } 
        String query = 'select ';
        for(Schema.DescribeFieldResult dfr : fields)
        {
            query = query + dfr.getName() + ',';
        }
        query = query.subString(0,query.length() - 1);
        query = query + ' from ';
        query = query + descRes.getName();
        query = query +  ' where Id = \'';
        query = query + certChecklist.Id + '\'';
        system.debug('Build Query == ' + query);
        return query;
    }

    //UPDATES CERTIFICATION CHECKLIST STATUS TO CERTIFICATION REQUESTED
    public pagereference submitCertReqst()
    {
        Pagereference pg;
        certChecklist.CertificationChecklistStatus__c = Label.CLOCT15CFW56;
        Database.SaveResult results = database.update(certChecklist,false);
        if(!(results.issuccess()))
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,results.getErrors()[0].getMessage());
            ApexPages.addMessage(myMsg);
            pg = null;
        }    
        else
        {    
            pg = new pagereference('/apex/VFP_CertificationChecklist?id='+certChecklist.Id);        
            pg.setRedirect(true);            
        }
        return pg;
    }
    
    //REDIRECTS TO THE CORRESPONDING PAGE
    public pagereference urlPageRedirect()
    {
        if(urlValue.startsWith('012'))
            return new Pagereference('/apex/VFP_CFWDetailPage?RecordType='+urlValue+'&id='+certChecklist.CertificationRiskAssessment__c);        
        else if(urlValue == 'VFP_CertificationChecklist')
        {
            return new pagereference('/apex/'+urlValue+'?id='+certChecklist.Id);            
        }
        else if(urlValue == 'VFP_CFWGuidelinesPolicies')
        {
            //CHECKS IF GUIDELINES RECORD ALREADY EXISTS, IF NOT, CREATES THE RECORD
            List<CertificationGuidelines__c> certGuidelines= [select Id,CertificationRiskAssessment__c from CertificationGuidelines__c where CertificationRiskAssessment__c=:certChecklist.CertificationRiskAssessment__c];
            if(certGuidelines!=null && certGuidelines.size()>0)
                return new pagereference('/apex/'+urlValue+'?id='+certGuidelines[0].Id);
            else
            {
                CertificationGuidelines__c certGuideline= new CertificationGuidelines__c();
                certGuideline.CertificationRiskAssessment__c = certChecklist.CertificationRiskAssessment__c;
                insert certGuideline;
                return new pagereference('/apex/'+urlValue+'?id='+certGuideline.Id);
            }
        }
        else
            return new pagereference('/apex/'+urlValue+'?id='+certChecklist.CertificationRiskAssessment__c);
    }
    //RENDERS INPUT FEILDS
    public pagereference edit()
    {
        dispInputFields = true;
        unfilterAll = false;
        filterKO = false;
        return null;    
    }
    //CANCEL BUTTON ACTION
    public pagereference Cancel()
    {
        pagereference pg = new pagereference('/apex/VFP_CertificationChecklist?id='+certChecklist.Id);        
        pg.setRedirect(true);            
        return pg;
    }

    //DISPLAYS FIELDS WITH KO AS VALUE
    public pagereference filterKO()
    {
        filterKO = true;
        unfilterAll = false;
        return null;
    }
    //DISPLAYS ALL FIELDS
    public pagereference unfilterAll()
    {
        unfilterAll = true;
        filterKO = false;
        return null;
    }
}