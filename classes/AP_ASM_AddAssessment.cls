/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 28 July 2013
    Description  : 1. Add Assessment to ORF,Account Assigned Program and Account Specialization
                     when automatic assignment checkbox is checked.        
***************************************************************************************************************************/
public class AP_ASM_AddAssessment
{
    /*********************************************************************************************************
        To add Assessment to ORF and Account Assigned Program
    *********************************************************************************************************/
    public static void addProgramAssessment(map<String,set<Id>> mapProgramAssessment)
    {
        Savepoint sp;
        try
        {
            list<Id> lstProgramId = new list<Id>();
            list<Id> lstProgramLevelId = new list<Id>();
            set<Id> setAssessmentID = new set<Id>();
            list<OpportunityRegistrationForm__c> lstORF = new list<OpportunityRegistrationForm__c>();
            list<PartnerAssessment__c> lstPartnerAssessment = new list<PartnerAssessment__c>();
            list<OpportunityAssessmentQuestion__c> lstQuestion = new list<OpportunityAssessmentQuestion__c>();
            list<OpportunityAssessmentResponse__c> lstResponse = new list<OpportunityAssessmentResponse__c>();
            list<ACC_PartnerProgram__c> lstAccountProgram = new list<ACC_PartnerProgram__c>();
            map<Id,list<OpportunityAssessmentQuestion__c>> mapAssessmentQuestion = new map<Id,list<OpportunityAssessmentQuestion__c>>();
            
            
            for(String str : mapProgramAssessment.keySet())
            {
                if(str.contains(':'))
                {
                    lstProgramId.add(str.split(':').get(0));
                    lstProgramLevelId.add(str.split(':').get(1));
                }
                setAssessmentID.addAll(mapProgramAssessment.get(str));
            }
                  
            lstORF = [Select id,PartnerProgram__c,Programlevel__c from OpportunityRegistrationForm__c where PartnerProgram__c in :lstProgramId and ProgramLevel__c in:lstProgramLevelId limit 10000];
            lstAccountProgram = [Select id,PartnerProgram__c,Programlevel__c from ACC_PartnerProgram__c where PartnerProgram__c in :lstProgramId and ProgramLevel__c in:lstProgramLevelId limit 10000];
            lstQuestion = [Select id,assessment__c from OpportunityAssessmentQuestion__c where assessment__c in :setAssessmentID limit 10000];
                   
            for(OpportunityAssessmentQuestion__c ques : lstQuestion)
            {
                if(!mapAssessmentQuestion.containsKey(ques.assessment__c))
                    mapAssessmentQuestion.put(ques.assessment__c , new list<OpportunityAssessmentQuestion__c> {(ques)});
                else
                    mapAssessmentQuestion.get(ques.assessment__c).add(ques);
            }
            for(OpportunityRegistrationForm__c orf : lstORF)
            {
                if(mapProgramAssessment.containsKey(orf.PartnerProgram__c+':'+orf.ProgramLevel__c))
                {
                    for(Id assessmentID : mapProgramAssessment.get(orf.PartnerProgram__c+':'+orf.ProgramLevel__c))
                    {
                        PartnerAssessment__c newAssessment = new PartnerAssessment__c();
                        newAssessment.assessment__c = assessmentID;
                        newAssessment.OpportunityRegistrationForm__c = orf.id;
                        newAssessment.recordtypeid = Label.CLOCT13PRM01;
                        lstPartnerAssessment.add(newAssessment);
                                           
                    }
                }
            }
            
            for(ACC_PartnerProgram__c accProgram : lstAccountProgram)
            {
                if(mapProgramAssessment.containsKey(accProgram.PartnerProgram__c+':'+accProgram.ProgramLevel__c))
                {
                    for(Id assessmentID : mapProgramAssessment.get(accProgram.PartnerProgram__c+':'+accProgram.ProgramLevel__c))
                    {
                        PartnerAssessment__c newAssessment = new PartnerAssessment__c();
                        newAssessment.assessment__c = assessmentID;
                        newAssessment.AccountAssignedProgram__c = accProgram.id;
                        newAssessment.recordtypeid = Label.CLOCT13PRM02;
                        lstPartnerAssessment.add(newAssessment);
                                           
                    }
                }
            }
            sp = Database.setSavepoint();
            if(!lstPartnerAssessment.isempty())
                insert lstPartnerAssessment;
            for(PartnerAssessment__c partnerAssessment : lstPartnerAssessment)
            {
                if(mapAssessmentQuestion.containsKey(partnerAssessment.assessment__c))
                {
                    for(OpportunityAssessmentQuestion__c ques : mapAssessmentQuestion.get(partnerAssessment.assessment__c))
                    {
                        OpportunityAssessmentResponse__c response = new OpportunityAssessmentResponse__c();
                        response.partnerassessment__C = partnerAssessment.Id;
                        response.question__c = ques.Id;
                        lstResponse.add(response);
                    }
                }
            }
            if(!lstResponse.isEmpty())
                insert lstResponse;
        }
        catch(Exception e)
        {
            system.debug('Exception in addProgramAssessment:'+e.getmessage());
            database.rollback(sp);
        }
    }
    
    /*********************************************************************************************************
        To add Assessment to Account Specialization
    *********************************************************************************************************/
    public static void addSpeAssessment(map<Id,set<Id>> mapAccountSpecialization)
    {
        list<AccountSpecialization__c> lstAccSpe = new list<AccountSpecialization__c>();
        set<Id> setAssessment = new set<Id>();
        list<OpportunityAssessmentQuestion__c> lstQues = new list<OpportunityAssessmentQuestion__c>();
        list<OpportunityAssessmentResponse__c> lstResponse = new list<OpportunityAssessmentResponse__c>();
        map<Id,list<OpportunityAssessmentQuestion__c>> mapAssessmentQuestion = new map<Id,list<OpportunityAssessmentQuestion__c>>();
        list<PartnerAssessment__c> lstPartnerAssessment = new list<PartnerAssessment__c>();
        Savepoint sp;
        
        try
        {
            sp = Database.setSavepoint();
            for(Id speId : mapAccountSpecialization.keySet())
                setAssessment.addAll(mapAccountSpecialization.get(speId));
            
            lstAccSpe = [Select id,Specialization__c from AccountSpecialization__c where specialization__c in:mapAccountSpecialization.keySet() limit 10000];
            lstQues = [Select id,assessment__c from OpportunityAssessmentQuestion__c where assessment__c in:setAssessment limit 10000];
            for(OpportunityAssessmentQuestion__c ques : lstQues)
            {
                if(!mapAssessmentQuestion.containsKey(ques.assessment__c))
                    mapAssessmentQuestion.put(ques.assessment__c , new list<OpportunityAssessmentQuestion__c> {(ques)});
                else
                    mapAssessmentQuestion.get(ques.assessment__c).add(ques);
            }
            
             for(AccountSpecialization__c aspe: lstAccSpe)
                {
                    if(mapAccountSpecialization.containsKey(aspe.Specialization__c))
                    {
                        for(Id assmntID : mapAccountSpecialization.get(aspe.Specialization__c))
                        {
                            PartnerAssessment__c newAssessment = new PartnerAssessment__c();
                            newAssessment.assessment__c = assmntID;
                            newAssessment.AccountSpecialization__c = aspe.Id;
                            newAssessment.recordtypeid = Label.CLOCT13PRM03;
                            lstPartnerAssessment.add(newAssessment);
                                               
                        }
                    }
                }
                
                if(!lstPartnerAssessment.isempty())
                    insert lstPartnerAssessment;
                for(PartnerAssessment__c partnerAssessment : lstPartnerAssessment)
                {
                    if(mapAssessmentQuestion.containsKey(partnerAssessment.assessment__c))
                    {
                        for(OpportunityAssessmentQuestion__c ques : mapAssessmentQuestion.get(partnerAssessment.assessment__c))
                        {
                            OpportunityAssessmentResponse__c response = new OpportunityAssessmentResponse__c();
                            response.partnerassessment__C = partnerAssessment.Id;
                            response.question__c = ques.Id;
                            lstResponse.add(response);
                        }
                    }
                }
                if(!lstResponse.isEmpty())
                    insert lstResponse;
           }
           catch(Exception e)
           {
               system.debug('Exception in addProgramAssessment:'+e.getmessage());
               database.rollback(sp);
           }
    }
}