@istest
    
private class VFC34_PriceAndAvailabilitySearch_TEST
{
    static testMethod void TestPA() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        contact1.Country__c= [Select Id, Name from Country__c Limit 1].Id;
        insert contact1;
        
        Contact contact2 = Utils_TestMethods.createContact(account1.Id,'TestContact2');
        insert contact2;
        
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        case1.CommercialReference__c= 'Commercial\\sReference\\stest';
        case1.ProductBU__c = 'Business';
        case1.ProductLine__c = 'ProductLine__c';
        case1.ProductFamily__c ='family';
        insert case1;
        
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(case1);
        VFC34_PriceAndAvailabilitySearch PAController = new VFC34_PriceAndAvailabilitySearch(CaseStandardController);
        VCC06_DisplaySearchResults componentcontroller = new VCC06_DisplaySearchResults(); 
        componentcontroller.key = 'PAResultComponent';
        componentcontroller.PageController = PAController ;  
      
        PAController.ShowAllLegacy();             
        PAController.DataSource.test = true;
        PAController.PartNo = '';
        PAController.CheckPA();
        PAController.clearPA();
        PAController.PartNo = 'TESTPARTNO';
        PAController.Quantity = null;
        PAController.CheckPA();
        PAController.Quantity = 10;
        PAController.CheckPA();
        PAController.BackOffice = Label.CL00355;
        PAController.CheckPA();
        PAController.PerformAction(PAController.fetchedPA,PAController);
        PAController.DisplayCheckPA(PAController.fetchedPA);
        PAController.SavePA();
        PAController.Exit();
        PAController.SwitchToSimple();
    }
}