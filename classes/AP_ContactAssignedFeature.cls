public with sharing class AP_ContactAssignedFeature {

    //Using custom settings mapping
    public static void createPermissionSetAssignments2(list<ContactAssignedFeature__c> lstNewCAF)
    {
        set<id> conIds = new set<id>();
        map<id,set<id>> mpConToFCs = new map<id,set<id>>();
        for(ContactAssignedFeature__c caf : lstNewCAF)
        {
            conIds.add(caf.Contact__c);

            if(mpConToFCs.containsKey(caf.Contact__c))
            {
                mpConToFCs.get(caf.Contact__c).add(caf.FeatureCatalog__c);
            }else
            {
                set<id> tst = new set<id>(); tst.add(caf.FeatureCatalog__c);
                mpConToFCs.put(caf.Contact__c, tst);
            }

        }

       // List<User> lstPartnerUsers = [SELECT Profile.UserLicense.Name,id, IsActive, ContactId, (Select Id, PermissionSetId, AssigneeId from PermissionSetAssignments) FROM User WHERE isActive=true AND contactId IN : conIds];
      List<User> lstPartnerUsers = [SELECT ProfileId,id, IsActive, ContactId, (Select Id, PermissionSetId, AssigneeId from PermissionSetAssignments) FROM User WHERE isActive=true AND contactId IN : conIds];      
      map<id,set<id>> mpUsrToExtPermissionSet = new map<id,set<id>>();
      Set<Id> setUserProfileID = new Set<Id>(); //October 2014 release
        for(User usr : lstPartnerUsers)
        {
            
            if(usr.PermissionSetAssignments != null && usr.PermissionSetAssignments.size()>0 )
            {
                Set<id> tset = new Set<id>();
                for(PermissionSetAssignment psa : usr.PermissionSetAssignments)
                    tset.add(psa.PermissionSetId);

                mpUsrToExtPermissionSet.put(usr.id, tset);
            } 
            //October 2014 Release
            
            //October 2014 Release
           // String userProfileID = String.valueOf(usr.ProfileId).substring(0,15);
           // System.debug('***** profile ID ->'+userProfileID );
            setUserProfileID.add(usr.ProfileId);  
                                   
        }
        
        
        System.debug('**** User Profile Ids ***'+setUserProfileID);
       //OCT14 Release
        List<CS_PRM_FeaturePermissionSetMap__c> lstFeaturePermissionCustomSet = [Select Name,ProfileId__c,PermissionSetIds__c,PermissionSetNames__c from CS_PRM_FeaturePermissionSetMap__c
                                                                                 Where ProfileId__c in :setUserProfileID Limit 20];
        
            
       // map<string,CS_PRM_FeaturePermissionSetMap__c > mpCS = CS_PRM_FeaturePermissionSetMap__c.getAll();
       map<string,CS_PRM_FeaturePermissionSetMap__c > mpCS =  new map<string,CS_PRM_FeaturePermissionSetMap__c >(); //October 2014 Release
        
        if(lstFeaturePermissionCustomSet.size() > 0)
        {
            for(CS_PRM_FeaturePermissionSetMap__c aFeatureMap :lstFeaturePermissionCustomSet)
            {
                mpCS.put(aFeatureMap.Name.substringBeforeLast('-')+aFeatureMap.ProfileId__c, aFeatureMap);
            }   
        }
        
        //End: October 14 Release
        System.debug('**** mpCS '+ mpCS);
        
        List<FeatureCatalog__c> lstFC = [SELECT Category__c,Description__c,Name FROM FeatureCatalog__c];
        Map<id,FeatureCatalog__c> mpFcs = new Map<id,FeatureCatalog__c>(lstFC);
        
        map<id,set<id>> mpUserToNewPs = new map<id,set<id>>();

        list<PermissionSetAssignment> insertPSA = new list<PermissionSetAssignment>();
        String userLicenseType; //July 2014
        for(User us : lstPartnerUsers)
        {
            
            //July 14
            userLicenseType = us.ProfileId;
            //if(us.Profile.UserLicense.Name.contains(Label.CLJUL14PRM02))//Label.CLJUL14PRM02
            //  { 
            //     userLicenseType = '-'+Label.CLJUL14PRM02;
                // System.debug('**** User LicenseType ->'+userLicenseType);
            //  }
            // else
              // System.debug('**** User LicenseType Community->');
            
            set<id> exstPermissions = mpUsrToExtPermissionSet.get(us.id); 
            for(id fcId : mpConToFCs.get(us.ContactId) )
            {   
                system.debug('fcId::'+fcId);
                if(fcId != null)
                {
                    String multicatagory = mpFcs.get(fcId).Category__c;
                    system.debug('multicatagory'+multicatagory);
                     
                    if(multicatagory != null)
                    {
                        List<String> lstCate = multicatagory.split(';');
                        system.debug('*** lstCate->'+lstCate);
                        for(String catagory : lstCate)
                            if(catagory != null)   
                            {
                               system.debug('*** mpcs->'+mpCS.get(catagory+userLicenseType));
                                system.debug('*** mpcs'+mpCS);
                               
                               // if(mpCS.get(catagory) != null && mpCS.get(catagory).PermissionSetIds__c != null)
                                if(mpCS.get(catagory+userLicenseType) != null && mpCS.get(catagory+userLicenseType).PermissionSetIds__c != null)
                                {
                                    List<String> lstpst = mpCS.get(catagory+userLicenseType).PermissionSetIds__c.split(',');
                                    if(lstpst.size() > 0)
                                    {
                                        for(String psId : lstpst)
                                        {   
                                            system.debug('psId'+psId);
                                            if(!(exstPermissions != null && exstPermissions.contains(psId)))
                                            {
                                                
                                                if(mpUserToNewPs.containsKey(us.id) && mpUserToNewPs.get(us.id).contains(psId))
                                                {
                                                    //permissionset already added to the list
                                                }
                                                else{

                                                    system.debug('psId added'+psId);
                                                    PermissionSetAssignment psa = new PermissionSetAssignment();
                                                    psa.AssigneeId = us.id;
                                                    psa.PermissionSetId = psId;
                                                    insertPSA.add(psa);

                                                    if(mpUserToNewPs.containsKey(us.id))
                                                        mpUserToNewPs.get(us.id).add(psId);
                                                    else
                                                    {   
                                                        set<id> tset2 = new set<id>(); tset2.add(psid);
                                                        mpUserToNewPs.put(us.id, tset2) ;
                                                    }
                                                }

                                            }

                                        }
                                    }
                                }
                            }   
                        
                    }
                }
            }
        }

        //Database.insert(insertPSA, false);
        insert insertPSA;
    } 


    //using cs
    public static void deletePermissionSetAssignments2(list<ContactAssignedFeature__c> lstUpdatedCAF)
    {

        lstUpdatedCAF = [Select Contact__c, Active__c, FeatureCatalog__c , FeatureCatalog__r.Category__c FROM ContactAssignedFeature__c WHERE ID IN:lstUpdatedCAF];
        set<id> conIds = new set<id>();
        map<id,set<String>> mpConToFCs = new map<id,set<String>>();
        for(ContactAssignedFeature__c caf : lstUpdatedCAF)
        {
            conIds.add(caf.Contact__c);
            String cates = caf.FeatureCatalog__r.Category__c;
            list<String> stCates = new list<String>();
            if(cates != null)
                stCates = cates.split(';');

            if(mpConToFCs.containsKey(caf.Contact__c))
            {
                mpConToFCs.get(caf.Contact__c).addAll(stCates);
            }else
            {
                
                Set<String> stStr = new set<String>();
                stStr.addAll(stCates);
                mpConToFCs.put(caf.Contact__c, stStr);
            }
        }
        map<string,CS_PRM_FeaturePermissionSetMap__c > mpCS = CS_PRM_FeaturePermissionSetMap__c.getAll();
        map<id,set<string>> mpPStoCats = new map<id,set<string>>();
        for(CS_PRM_FeaturePermissionSetMap__c cs : mpCS.values())
        {
            String cate = cs.name;
            String psids = cs.PermissionSetIds__c;
            List<id> psidset = new list<id>();
            if(psids != null)
                psidset = psids.split(',');
            for(String psid : psidset)
            {
                if(mpPStoCats.containsKey(psid))
                    mpPStoCats.get(psid).add(cate);
                else
                {
                    set<string> test = new set<string>();
                    test.add(cate);
                    mpPStoCats.put(psid, test);
                }   
            }
        }
        //PermissionSetIds__c

        List<User> lstPartnerUsers = [SELECT id, IsActive, ContactId FROM User WHERE isActive=true AND contactId IN : conIds];
        map<id, id> mpUsrtoCon = new map<id, id>();
        for(User us : lstPartnerUsers)
        {
            mpUsrtoCon.put(us.Id, us.ContactId);
        }
    
        /////////////////////////////////////////

        List<PermissionSetAssignment> lstPSA = [Select ID, AssigneeId, PermissionSetId from PermissionSetAssignment WHERE AssigneeId IN:lstPartnerUsers];
        List<PermissionSetAssignment> delPSA = new List<PermissionSetAssignment>();

        for(PermissionSetAssignment psa : lstPSA)
        {
            id conId = mpUsrtoCon.get(psa.AssigneeId);
            set<String> fccats = mpConToFCs.get(conId);
            set<string> pscats = mpPStoCats.get(psa.PermissionSetId);
            if(fccats != null && pscats != null)
                for(String psCa: pscats)
                {
                    if(fccats.contains(psCa))
                    {
                        delPSA.add(psa);
                        break;
                    }
                }
        
        }

        delete delPSA;
    }


    //Check duplicate Contact Assigned features
    public static void checkDuplicateCAF(List<ContactAssignedFeature__c> lstNewCAF)
    {
        map<String,ContactAssignedFeature__c> mpCaf = new map<String,ContactAssignedFeature__c>();
        set<Id> cons = new set<id>();
        set<id> fCs = new set<id>();
        for(ContactAssignedFeature__c tCAF : lstNewCAF)
        {
            
            cons.add(tCAF.Contact__c);
            fCs.add(tCAF.FeatureCatalog__c);
            String tstr = tCAF.Contact__c+'-'+tCAF.FeatureCatalog__c;
            if(mpCaf.containsKey(tstr))
            {   
                mpCaf.get(tstr).addError(System.Label.CLAPR14PRM57);
                tCAF.addError(System.Label.CLAPR14PRM57);
            }
            else
            {
                mpCaf.put(tstr , tCAF);
            }
        }

        List<ContactAssignedFeature__c> lstEistCAF = [Select Contact__c, FeatureCatalog__c From ContactAssignedFeature__c Where Contact__c in:cons AND FeatureCatalog__c In:fCs and ID NOT IN:lstNewCAF];

        for(ContactAssignedFeature__c eCAF : lstEistCAF)
        {
            String tstr = eCAF.Contact__c+'-'+eCAF.FeatureCatalog__c;
            if(mpCaf.containsKey(tstr) && mpCaf.get(tstr).id != eCAF.id )
            {   
                mpCaf.get(tstr).addError(System.Label.CLAPR14PRM58);
            }
        }

    }
}