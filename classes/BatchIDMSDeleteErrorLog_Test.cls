@isTest
public class BatchIDMSDeleteErrorLog_Test {
    
    static testMethod void TestOne() {
    
             test.starttest();
        IDMS_ErrorLog__c  el = new IDMS_ErrorLog__c (ExceptionDate__c=System.now().addDays(-7)); 
        insert el;
              
              
        BatchIDMSDeleteErrorLog SreBatch = New BatchIDMSDeleteErrorLog();
           Database.executeBatch(SreBatch);
        
        string CORN_EXP = '0 0 0 1 4 ?';
        string CORN_EXPS = '0 0 0 1 4 ?';
        
        BatchIDMSDeleteErrorLog ipbatch2 = new BatchIDMSDeleteErrorLog ();
        string jobid = system.schedule('my batch job', CORN_EXP, new BatchIDMSDeleteErrorLog());
        CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
        test.stoptest();
        
    }

}