/*
    Author          : Kiran Kareddy (Schneider Electric)
    Date Created    :  16-Feb-2012
    Description     : Test Class for ConnectNeworkTeamMembersProgramTriggers
*/
@isTest
private class ConnectNetworkTeamMembersProgram_Test {
static testMethod void testConnectNetworkTeamMembersProgram() {
 User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connect6');
        System.runAs(runAsUser){
            Initiatives__c inti=Utils_TestMethods_Connect.createInitiative();
            insert inti;
Project_NCP__c cp3=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',Global_Functions__c='GM',  
Global_Business__c='ITB',Power_Region__c='NA');
insert cp3;
Project_NCP__c cp4=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp1',Global_Functions__c='GSC-Regional',  
Global_Business__c='ITB;Partner-Division;'+Label.Connect_Smart_Cities,Power_Region__c='NA',GSC_Regions__c='NA;EMEAS',Smart_Cities_Division__c='test',Partner_Region__c='test;test1');
insert cp4;
Champions__c C=new Champions__c(Scope__c='Global Functions',Entity__c='GM');
insert c;
Team_Members__c Team3=new Team_Members__c(Program__c=cp3.id,Team_Name__c='Global Functions', 
Entity__c='GM');
insert Team3;
Team_Members__c Team4=new Team_Members__c(Program__c=cp3.id,Team_Name__c='Global Business', 
Entity__c='ITB');
insert Team4;
Team_Members__c Team10=new Team_Members__c(Program__c=cp4.id,Team_Name__c='Global Business', 
Entity__c='Partner-Division',Partner_Region__c='test');
insert Team10;
Team_Members__c Team18=new Team_Members__c(Program__c=cp4.id,Team_Name__c='Global Business', 
Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test');
insert Team18;

Team_Members__c Team5=new Team_Members__c(Program__c=cp3.id,Team_Name__c='Operation Regions', 
Entity__c='NA');
insert Team5;
Team_Members__c Team8=new Team_Members__c(Program__c=cp4.id,Team_Name__c='Global Functions', 
Entity__c='GSC-Regional',GSC_Region__c='NA');
insert Team8;
Team_Members__c Team6=new Team_Members__c(Program__c=cp3.id,Team_Name__c='Global Functions', 
Entity__c='GM');
insert Team6;
Team_Members__c T6= [SELECT Program_Network__c FROM Team_Members__c WHERE Id =:Team6.Id];
System.assert(T6.Program_Network__c==true);
Team6.Team_Name__c='Global Business';
Team6.Entity__c='ITB';
Team5.Entity__c='EMEAS';
Team8.GSC_Region__c='EMEAS';
Team10.Partner_Region__c='test;test1';
Test.startTest();
update Team6;
update Team8;
update Team10;
Test.stopTest();Team_Members__c T7= [SELECT Program_Network__c FROM Team_Members__c WHERE Id =:Team6.Id];
System.assert(T7.Program_Network__c==false);
}
}
}