/*
    Author          : Srinivas Nallapati
    Date Created    : 16/02/2012
    Description     : Controller lists all related open lead .
    06/Aug/2012    Srinivas Nallapati    Updated for modifying the columns DEF-0661
    29/Oct/2012    Srinivas Nallapati    BR-2272: Updated to prevent user opening the page from Open Leads
*/
public with sharing class VFC92_CloseDuplicateLeads{
    public Lead led {get;set;}
    public List<Lead> relatedOpenLeads {get;set;}
    //public Boolean hasOpenLeads {get;set;} {hasOpenLeads=false;} //Tobe Deleted for Dec12
    //
    public Boolean hasError {get;set;} {hasError = false;}
    //
    public Boolean hasUpdateError {get;set;} {hasUpdateError=false;}
    
    public List<LeadWrapper> lstLeadWraps {get;set;} {lstLeadWraps = new List<LeadWrapper>();}
    public List<LeadWrapper> lstResultLeadWraps {get;set;} {lstResultLeadWraps = new List<LeadWrapper>();}
   
    public VFC92_CloseDuplicateLeads(ApexPages.StandardController controller) {
        led = (Lead)controller.getRecord();
        led = [select id, Tech_CampaignName__c, name, Status, isConverted, LeadContact__c, Contact__c from Lead where id = :led.id];
        //Srinivas For Dec12 mkt
        if(led.Status == Label.CL00554 || led.Status == Label.CL00447) //if New or Working
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.CLDEC12MKT05));
            hasError = true;
        }//End of change

        //Srinivas - May13 MKT
        if(!( VFC_ConvertLead.hasEditOnLead(Userinfo.getUserId(), led.Id)) && led.isConverted== false)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLMAY13MKT005));
            hasError = true;    
        }
        //End of May13 Chnage

    }
    
    public void checkOpenLeads(){

        relatedOpenLeads = new List<Lead>();
        if(led.LeadContact__c != null)
            relatedOpenLeads = [select Tech_CampaignName__c, Timeframe__c,SubStatus__c, Status, SolutionInterest__c, PromoTitle__c, Priority__c, Name, CreatedDate, ClosedDate__c,
                        Category__c,Owner.FirstName,Owner.LastName, OwnerID, ResponseDate__c, CampaignName__c,keycode__c, ConvertedDate, MarcomType__c, isConverted, Phone, Owner.name from lead where LeadContact__c =:led.LeadContact__c and (Status = :Label.CL00554 OR Status = :Label.CL00447) and id !=:led.id order by CreatedDate desc limit 500];
        if(led.Contact__c != null)
            relatedOpenLeads = [select Tech_CampaignName__c, Timeframe__c,SubStatus__c, Status, SolutionInterest__c, PromoTitle__c, Priority__c, Name, CreatedDate, ClosedDate__c,
                        Category__c,Owner.FirstName,Owner.LastName, OwnerID, ResponseDate__c, CampaignName__c,keycode__c, ConvertedDate, MarcomType__c, isConverted, Phone, Owner.name from lead where Contact__c =:led.Contact__c and (Status = :Label.CL00554 OR Status = :Label.CL00447) and id !=:led.id order by CreatedDate desc limit 500];
        
        
        
        if(relatedOpenLeads.size() == 0)//Chnage for Dec -- Srinivas
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.CLMKT120001));
            hasError = true;
        }        
        if(relatedOpenLeads.size() > 0){
            
            set<id> QueueGroupIds = new set<id>();
            for(Lead ld : relatedOpenLeads){
                string strOwnerID = ld.OwnerId;
                if(!strOwnerID.startsWith('005'))
                    QueueGroupIds.add(ld.OwnerId);
            }
            
            map<id,Group> mapQueueGroups = new map<id,Group>([Select Type, Name, Email From Group where id in :QueueGroupIds]);
            
            lstLeadWraps.clear();
            for(Lead ld : relatedOpenLeads){
                string strOwnerID = ld.OwnerId;
                if(!strOwnerID.startsWith('005'))
                    lstLeadWraps.add(new LeadWrapper(ld));
                else
                    lstLeadWraps.add(new LeadWrapper(ld));      
            }
            
        }    
        relatedOpenLeads.clear();       
    }
   
    
    public PageReference closeSelected(){
        

        
        List<Lead> lstSelectdLeads = new List<Lead>();
        map<id,Lead> mapUnchangedLeads = new map<id,Lead>();
        
        for(LeadWrapper lw : lstLeadWraps )
        {
            if(lw.isSelected){
                mapUnchangedLeads.put(lw.ld.id, lw.ld.clone(true));
                lw.ld.Status = Label.CL00560;
                lw.ld.SubStatus__c = Label.CLJUN13MKT01; 
                lstSelectdLeads.add(lw.ld);
            }    
        }
        
        //if no Lead Selected
        if(lstSelectdLeads.size() == 0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.CLDEC12MKT06));
            return null;
        }        
        
        
        
        Database.Saveresult[] lstResult =  Database.update(lstSelectdLeads, false);
        
        for(Integer i = 0; i<lstSelectdLeads.size() ; i++){
          
          Database.Saveresult res = lstResult[i];
          if(!res.isSuccess()){
            LeadWrapper tmpLW =  new LeadWrapper(mapUnchangedLeads.get(lstSelectdLeads[i].id));
            for(Database.Error er : res.getErrors()){
              tmpLW.errorMessage = er.getMessage();
            }
            lstResultLeadWraps.add(tmpLW);
            hasUpdateError = true;
          }
        }  
        lstLeadWraps.clear();
        relatedOpenLeads.clear();
        
        if(hasUpdateError)
            return page.VFP93_CloseDuplicateLeadsResult;
        else
          return goToLeadDetailPage();

    }
    
        
    public PageReference goToLeadDetailPage(){
        String lcretUrl = ApexPages.currentPage().getParameters().get('lcretURL');
        PageReference pg;
        if(lcretUrl != null)
        {
          pg = new PageReference(lcretUrl);
          return pg;
        }    
        
        if(led.isConverted){
            pg = page.VFP44_LeadDetail;
            pg.getParameters().put('id', led.Id);
        }else{
            pg = new PageReference('/'+led.id);
        }
        return pg;
    }
    
    public class LeadWrapper{
        public boolean isSelected {get;set;} {isSelected = false;}
        public Lead Ld {get;set;}
        public String errorMessage {get;set;}
        
        public LeadWrapper(Lead ld){
            this.Ld = ld;
        }
    }
 
    
}