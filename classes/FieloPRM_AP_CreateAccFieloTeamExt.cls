public with sharing class FieloPRM_AP_CreateAccFieloTeamExt {
	public Id accountId{get;set;}

	public FieloPRM_AP_CreateAccFieloTeamExt(ApexPages.StandardController stdCon) {
		accountId = stdCon.getId();
	}

	public PageReference execute(){
		FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{accountId});
		return new PageReference('/' + accountId);
	}
}