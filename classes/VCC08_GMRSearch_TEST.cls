@isTest
private class VCC08_GMRSearch_TEST{
    static testMethod void VCC08_GMRSearchTest(){
        //Start of Test Data preparation


        Account account1 = Utils_TestMethods.createAccount();
        insert account1;

        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        contact1.Country__c= [Select Id, Name from Country__c Limit 1].Id;
        insert contact1;

        Contact contact2 = Utils_TestMethods.createContact(account1.Id,'TestContact2');
        insert contact2;

        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        case1.CommercialReference__c= 'Commercial\\sReference\\stest';
        case1.ProductBU__c = 'Business';
        case1.ProductLine__c = 'ProductLine__c';
        case1.ProductFamily__c ='family';
        insert case1;

        string [] searchfilterList= new string[]{};

        for(Integer token=0;token<17;token++){
            searchfilterList.add('a');          
        }

        List<String> keywords = new List<String>();
        Utils_DummyDataForTest dummydata = new Utils_DummyDataForTest();
        Utils_DataSource.Result  tempresult = new Utils_DataSource.Result();
        tempresult = dummydata.Search(keywords,keywords);

        sObject tempsObject = tempresult.recordList[0];
        //End of Test Data preparation.

        Test.startTest();
        ApexPages.StandardController cc1 = new ApexPages.StandardController(case1);
        PageReference pageRef= Page.VFP25_AddUpdateProductforCase; 
        PageReference pageRef1= Page.VFP25_AddUpdateProductforCase; 


        VFC_ControllerBase basecontroller = new VFC_ControllerBase();
        VFC25_AddUpdateProductforCase ProductCaseController = new VFC25_AddUpdateProductforCase(cc1);

        System.debug('~~~~~~~~~~Start of Test~~~~~~~~~~');
        VCC08_GMRSearch ProductforCase = new VCC08_GMRSearch();
        VCC08_GMRSearch ProductforCase1 = new VCC08_GMRSearch(cc1);
        VCC06_DisplaySearchResults componentcontroller1 =ProductforCase1.resultsController; 

        VCC05_DisplaySearchFields componentcontroller = new VCC05_DisplaySearchFields(); 

        componentcontroller.pickListType = 'PM0_GMR';
        componentcontroller.SearchFilter = new String[4];
        componentcontroller.SearchFilter[0] = '00';
        //componentcontroller.SearchFilter[1] = Label.CL00355;    
        componentcontroller.SearchFilter[1] = '11';    
        componentcontroller.SearchFilter[2] = '22';    
        componentcontroller.SearchFilter[3] = Label.CL00355;    
        
        componentcontroller.searchText = 'search\\stest';
        componentcontroller.key = 'searchComponent';
        componentcontroller.PageController = ProductforCase1;
        componentcontroller.init();
        System.debug('#### COMPONENT CONTROLLER : ' + componentcontroller);
        ProductforCase1.searchController = componentcontroller; 
        System.debug('#### PAGE COMPONENT CONTROLLER : ' + ProductforCase1.searchController);    
        pageRef.getParameters().put('key','searchComponent');
        test.setCurrentPageReference(pageRef);  
        componentcontroller.searchText = 'search\\stest';
        componentcontroller.SearchFilter[0] = '00';
        componentcontroller.SearchFilter[1] = '11';    
        componentcontroller.SearchFilter[2] = '22';    
        componentcontroller.SearchFilter[3] = Label.CL00355;    
        ProductforCase1.search();
        if(ProductforCase1.fetchedRecords != null && !ProductforCase1.fetchedRecords.isEmpty()){
            DataTemplate__c firstResult = ProductforCase1.fetchedRecords[0];
            System.debug('firstResult: ' + firstResult);
            System.debug('ProductforCase1: ' + ProductforCase1);
            ProductforCase1.pageController=ProductCaseController;
            ProductforCase1.actionNumber=1;
            ProductforCase1.Actions = ProductCaseController.GMRsearchForCase;
            Pagereference pageResult = ProductforCase1.performAction(firstResult,ProductforCase1);
        }
        
        ProductforCase1.RefreshResults();
        ProductforCase1.clear();
        Pagereference pageResult1 = ProductforCase1.cancel();
        ProductforCase1.getInit();
      
        Test.stopTest();
    }
}