/**
 * This is test class for IDMSCreateUIMSUser class. 
 **/
//This method tests create user and company with identity type Mobile.
@IsTest public class IDMSCreateUIMSUserTest{
    
    public static testMethod void idmsUimsTest(){
        user u=new user();
        u.firstname='test';
        u.lastname='test';
        u.MobilePhone='1234567890';
        u.username='testuser.test@test.com';
        u.IDMS_Registration_Source__c='idms';
        u.IDMSIdentityType__c='Mobile';
        u.IDMS_User_Context__c='Work';
        IDMSCreateUIMSUser testuser=new IDMSCreateUIMSUser(u);
        QueueableContext context; 
        try{testuser.execute(context);
            u.CompanyName='Accenture';
            u.IDMSCompanyGoldenId__c='12345';
            u.Company_Country__c='US';
            u.IDMSMarketSubSegment__c='EU';
            u.IDMSCompanyHeadquarters__c=true;
            testuser.createCompany(u,'1234567890');
            
           }
        catch(exception e){
            System.debug('in exception');
        }
    }
    
    //This method tests create user and company with identity type Email.
    public static testMethod void idmsUimsTest2(){
        
        user u=new user();
        u.firstname='test';
        u.lastname='test';
        u.Email='testuser.test@test.com';
        u.username='testuser.test@test.com';
        u.IDMS_Registration_Source__c='uims';
        u.IDMSIdentityType__c='Email';
        u.IDMS_User_Context__c='Work';
        IDMSCreateUIMSUser testuser=new IDMSCreateUIMSUser(u);
        QueueableContext context; 
        try{
            testuser.execute(context);
            u.CompanyName='Accenture1';
            u.IDMSCompanyGoldenId__c='12345';
            u.Company_Country__c='US';
            u.IDMSMarketSubSegment__c='';
            u.IDMSCompanyHeadquarters__c=false;
            u.IDMSMarketSegment__c='EU';
            testuser.createCompany(u,'1234567890');
        }
        catch(Exception e){}
    }
}