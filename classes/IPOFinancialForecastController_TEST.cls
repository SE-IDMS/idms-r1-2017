@isTest
private class IPOFinancialForecastController_TEST {
    static testMethod void testIPOFinancialForecastController() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPOAt');
                System.runAs(runAsUser){
                
                 //Use the PageReference Apex class to instantiate 
        PageReference pageRef = Page.IPOFinancialForecast;
       
       //In this case, the Visualforce page named 'IPO_Cost_Forecast' is the starting point of this Test method. 
        Test.setCurrentPage(pageRef);
        
       //Insert Data
      
      IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
       
      IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp; 
            
      IPO_Financial_Forecast__c cf1 = new IPO_Financial_Forecast__c ();  
      cf1.IPO_Program__c = IPOp.id;
      cf1.Type__c = 'Capex';
      cf1.orderNumber__c = 1;
      cf1.Amount__c = 200.00;
      cf1.Year__c = 2013;   
      insert cf1;
      
      IPO_Financial_Forecast__c cf2 = new IPO_Financial_Forecast__c();
      cf2.IPO_Program__c = IPOp.id;
      cf2.Type__c = 'Build';
      cf2.orderNumber__c = 2;
      cf2.Amount__c = 200.00;
      cf2.Year__c = 2013;
      insert cf2;
      
      IPO_Financial_Forecast__c cf3 = new IPO_Financial_Forecast__c();
      cf3.IPO_Program__c = IPOp.id;
      cf3.Type__c = 'Develop';
      cf3.orderNumber__c = 3;
      cf3.Amount__c = 200.00;
      cf3.Year__c = 2013;
      insert cf3;
      
      IPO_Financial_Forecast__c cf4 = new IPO_Financial_Forecast__c();
      cf4.IPO_Program__c = IPOp.id;
      cf4.Type__c = 'Run';
      cf4.orderNumber__c = 4;
      cf4.Amount__c = 200.00;
      cf4.Year__c = 2013;
      insert cf4;
      
      IPO_Financial_Forecast__c cf5 = new IPO_Financial_Forecast__c();
      cf5.IPO_Program__c = IPOp.id;
      cf5.Type__c = 'Cash Out';
      cf5.orderNumber__c = 5;
      cf5.Amount__c = 200.00;
      cf5.Year__c = 2013;
      insert cf5;
      
      IPO_Financial_Forecast__c cf6 = new IPO_Financial_Forecast__c();
      cf6.IPO_Program__c = IPOp.id;
      cf6.Type__c = 'Total P&L';
      cf6.orderNumber__c = 6;
      cf6.Amount__c = 200.00;
      cf6.Year__c = 2013;
      insert cf6;
      
      IPO_Financial_Forecast__c cf7 = new IPO_Financial_Forecast__c();
      cf7.IPO_Program__c = IPOp.id;
      cf7.Type__c = 'P&L Business';
      cf7.orderNumber__c = 7;
      cf7.Amount__c = 200.00;
      cf7.Year__c = 2013;
      insert cf7;
      
      IPO_Financial_Forecast__c cf8 = new IPO_Financial_Forecast__c();
      cf8.IPO_Program__c = IPOp.id;
      cf8.Type__c = 'P&L IPO';
      cf8.orderNumber__c = 8;
      cf8.Amount__c = 200.00;
      cf8.Year__c = 2013;
      insert cf8;
      
      IPOFFColumns__c ffc2 = new IPOFFColumns__c ();
      ffc2.Amount__c = 0.00;
      ffc2.Name = 'Capex';
      ffc2.orderNumber__c = 1;
      ffc2.Editable__c = true;
      ffc2.InFormula__c = true;
      insert ffc2;
      
      IPOFFColumns__c ffc3 = new IPOFFColumns__c ();
      ffc3.Amount__c = 0.00;
      ffc3.Name = 'Build';
      ffc3.orderNumber__c = 2;
      ffc3.Editable__c = true;
      ffc3.InFormula__c = true;
      insert ffc3;
      
      IPOFFColumns__c ffc4 = new IPOFFColumns__c ();
      ffc4.Amount__c = 0.00;
      ffc4.Name = 'Develop';
      ffc4.orderNumber__c = 3;
      ffc4.Editable__c = true;
      ffc4.InFormula__c = true;
      insert ffc4;
      
      IPOFFColumns__c ffc5 = new IPOFFColumns__c ();
      ffc5.Amount__c = 0.00;
      ffc5.Name = 'Run';
      ffc5.orderNumber__c = 4;
      ffc5.Editable__c = true;
      ffc5.InFormula__c = true;
      insert ffc5;
      
      IPOFFColumns__c ffc6 = new IPOFFColumns__c ();
      ffc6.Amount__c = 0.00;
      ffc6.Name = 'Cash Out';
      ffc6.orderNumber__c = 5;
      ffc6.Editable__c = false;
      //ffc6.InFormula__c = true;
      insert ffc6;
      
      IPOFFColumns__c ffc7 = new IPOFFColumns__c ();
      ffc7.Amount__c = 0.00;
      ffc7.Name = 'Total P&L';
      ffc7.orderNumber__c = 6;
      ffc7.Editable__c = false;
     // ffc7.InFormula__c = true;
      insert ffc7;
      
      IPOFFColumns__c ffc8 = new IPOFFColumns__c ();
      ffc8.Amount__c = 0.00;
      ffc8.Name = 'P&L Business';
      ffc8.orderNumber__c = 1;
      ffc8.Editable__c = true;
      ffc8.InFormula__c = false;
      insert ffc8;
      
      IPOFFColumns__c ffc9 = new IPOFFColumns__c ();
      ffc9.Amount__c = 0.00;
      ffc9.Name = 'P&L IPO';
      ffc9.orderNumber__c = 1;
      ffc9.Editable__c = true;
      ffc9.InFormula__c = false;
      insert ffc9;
      
      
     // fyear1 = 2013;
     // insert fyear1;
      
        pageref.getparameters().put('Id',IPOp.id);
       // pageref.getparameters().put('Year','2013');
        
      
        ApexPages.StandardController stanPage= new ApexPages.StandardController(IPOp);
        IPOFinancialForecastController controller = new IPOFinancialForecastController(stanPage);
        controller.getForecast();
        controller.EditCostForecast();
        controller.SaveData();
        controller.AssignYear();
        controller.CancelCostForecast();
        controller.DeleteRecords();
        controller.getConfLevel();
        controller.getFinancial_Scope();
        controller.getYear();
        controller.getCLYear();
}
}

static testMethod void testIPOCostForecastController1() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPOAt');
                System.runAs(runAsUser){
                
                 //Use the PageReference Apex class to instantiate 
        PageReference pageRef = Page.IPOFinancialForecast;
       
       //In this case, the Visualforce page named 'IPO_Cost_Forecast' is the starting point of this Test method. 
        Test.setCurrentPage(pageRef);
        
       //Insert Data
     
      IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
       
      IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp;      
      
     // IPO_Financial_Forecast__c  cf1 = new IPO_Financial_Forecast__c ();
     // cf1.IPO_Program__c = IPOp.id;
     // insert cf1;
        
        IPOFFColumns__c ffc2 = new IPOFFColumns__c ();
      ffc2.Amount__c = 0.00;
      ffc2.Name = 'Capex';
      ffc2.orderNumber__c = 1;
      ffc2.Editable__c = true;
      ffc2.InFormula__c = true;
      insert ffc2;
      
      IPOFFColumns__c ffc3 = new IPOFFColumns__c ();
      ffc3.Amount__c = 0.00;
      ffc3.Name = 'Build';
      ffc3.orderNumber__c = 2;
      ffc3.Editable__c = true;
      ffc3.InFormula__c = true;
      insert ffc3;
      
      IPOFFColumns__c ffc4 = new IPOFFColumns__c ();
      ffc4.Amount__c = 0.00;
      ffc4.Name = 'Develop';
      ffc4.orderNumber__c = 3;
      ffc4.Editable__c = true;
      ffc4.InFormula__c = true;
      insert ffc4;
      
      IPOFFColumns__c ffc5 = new IPOFFColumns__c ();
      ffc5.Amount__c = 0.00;
      ffc5.Name = 'Run';
      ffc5.orderNumber__c = 4;
      ffc5.Editable__c = true;
      ffc5.InFormula__c = true;
      insert ffc5;
      
      IPOFFColumns__c ffc6 = new IPOFFColumns__c ();
      ffc6.Amount__c = 0.00;
      ffc6.Name = 'Cash Out';
      ffc6.orderNumber__c = 5;
      ffc6.Editable__c = false;
      //ffc6.InFormula__c = true;
      insert ffc6;
      
      IPOFFColumns__c ffc7 = new IPOFFColumns__c ();
      ffc7.Amount__c = 0.00;
      ffc7.Name = 'Total P&L';
      ffc7.orderNumber__c = 6;
      ffc7.Editable__c = false;
     // ffc7.InFormula__c = true;
      insert ffc7;
      
      IPOFFColumns__c ffc8 = new IPOFFColumns__c ();
      ffc8.Amount__c = 0.00;
      ffc8.Name = 'P&L Business';
      ffc8.orderNumber__c = 1;
      ffc8.Editable__c = true;
      ffc8.InFormula__c = false;
      insert ffc8;
      
      IPOFFColumns__c ffc9 = new IPOFFColumns__c ();
      ffc9.Amount__c = 0.00;
      ffc9.Name = 'P&L IPO';
      ffc9.orderNumber__c = 1;
      ffc9.Editable__c = true;
      ffc9.InFormula__c = false;
      insert ffc9;
      
      
        pageref.getparameters().put('Id',IPOp.id);
      
        ApexPages.StandardController stanPage= new ApexPages.StandardController(IPOp);
        IPOFinancialForecastController controller = new IPOFinancialForecastController(stanPage);
        controller.getForecast();
        controller.EditCostForecast();
        controller.SaveData();
        controller.AssignYear();
        controller.CancelCostForecast();
        controller.DeleteRecords();
        controller.getConfLevel();
        controller.getYear();
        controller.getCLYear();
}
}
}