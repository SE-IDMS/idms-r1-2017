/**************************************************************************************
    Author: Fielo Team (Elena J. Schwarzböck)
    Date: 18/06/2015
    Description: 
    Related Components: <Component1> / <Componente2>...
***************************************************************************************/

global with sharing class FieloPRM_VFP_BannerWidgetRLExtension{

    public List<FieloEE__Banner__c> listBanners {get;set;}

    public FieloEE__Component__c comp {get;set;}
    
    public String errorMessage {get;set;}

    public FieloPRM_VFP_BannerWidgetRLExtension(ApexPages.StandardController controller){
    
        if(!Test.isRunningTest()){
            controller.addFields(new List<String>{'FieloEE__Tag__c','FieloEE__Category__c'}); 
        }
    
        comp = (FieloEE__Component__c) controller.getRecord();
        
        if(comp.FieloEE__Tag__c != null && comp.FieloEE__Category__c != null){
        
            List<FieloEE__TagItem__c> listTagItems = [SELECT Id, FieloEE__Banner__c, FieloEE__Tag__c FROM FieloEE__TagItem__c WHERE FieloEE__Tag__c =: comp.FieloEE__Tag__c];
        
            Set<Id> setBanners = new Set<Id>();
        
            for(FieloEE__TagItem__c tagItem: listTagItems){
                setBanners.add(tagItem.FieloEE__Banner__c);
            }
        
            listBanners = [SELECT Id, Name, FieloEE__Order__c, FieloEE__IsActive__c FROM FieloEE__Banner__c WHERE Id IN: setBanners AND FieloEE__Category__c =: comp.FieloEE__Category__c ORDER BY FieloEE__Order__c];
        }
    }
    
    public PageReference doNewBanner(){
    
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'FieloPRM_BannerWidget' AND SobjectType = 'FieloEE__Banner__c'];

        FieloEE__Banner__c newBanner = new FieloEE__Banner__c(
            FieloEE__Category__c = comp.FieloEE__Category__c,
            Name = 'Banner',
            RecordTypeId = rt.Id
        );
        try{insert newBanner;}catch(DMLException e){errorMessage = e.getMessage();return null;}

        FieloEE__TagItem__c newTagItem = new FieloEE__TagItem__c(
            FieloEE__Tag__c = comp.FieloEE__Tag__c,
            FieloEE__Banner__c = newBanner.Id
        );
        
        try{insert newTagItem;}catch(DMLException e){errorMessage = e.getMessage();return null;}
        
        PageReference pageRef = new PageReference ('/' + newBanner.Id + '/e');
        pageRef.getParameters().put('retURL','/' + comp.Id);
        return pageRef;                
        
    }   

    public PageReference doDelete(){
    
        string bannerId = ApexPages.currentpage().getParameters().get('bannerId');
    
        List<FieloEE__TagItem__c> tagItem = [SELECT Id FROM FieloEE__TagItem__c WHERE FieloEE__Banner__c =: bannerId];

        if(!tagItem.isEmpty()){
            try{delete tagItem;}catch(DMLException e){errorMessage = e.getMessage();return null;}
        }
        
        FieloEE__Banner__c banner = new FieloEE__Banner__c(
            Id = bannerId
        );
        
        try{delete banner;}catch(DMLException e){errorMessage = e.getMessage();return null;}
                
        PageReference pageRef = new PageReference ('/' + comp.Id);
        return pageRef;                
        
    }
    
}