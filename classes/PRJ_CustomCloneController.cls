public class PRJ_CustomCloneController
{

 public ApexPages.StandardController controller;
    public PRJ_ProjectReq__c oPR{get;set;}
    public String result{get;set;}
    public PRJ_CustomCloneController(ApexPages.StandardController controller)
    {        
        this.controller = controller;    
        oPR = (PRJ_ProjectReq__c) controller.getRecord();
        
        
    }
    public void callCustomClone()
    {
       system.debug('Inside callCustomClone' );
        
        result = PRJ_CreateDeepClone.PRJ_createCustomClone(oPR.Id);
        if(result=='true')
           result = ' Request processes succesfully. Clone Completed';
        system.debug('the result is : ' + result);
          
      } 
 
            
     
}