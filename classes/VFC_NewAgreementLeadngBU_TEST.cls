@isTest
private class VFC_NewAgreementLeadngBU_TEST
{     
    static List<OppAgreement__c> aggList = new List<OppAgreement__c>();
    static ProductLineForAgreement__c[] objProdLine = new List<ProductLineForAgreement__c>();
    static User user,newUser,newUser2 ;
    static Country__c newCountry;
    static OppAgreement__c objAgree;
    static Account acc;
    static Opportunity objOpp;
    static OppAgreement__c objAgree2;
    static List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();        
    static testmethod void testMassUpdate()
    {
         
         acc = Utils_TestMethods.createAccount();
         Database.SaveResult AccountInsertResult = Database.insert(acc, false);
        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2; 
         objAgree2 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
         objAgree2.PhaseSalesStage__c = '3 - Identify & Qualify';   
         objAgree2.AgreementValidFrom__c = Date.today();
         
         objAgree2.FrameOpportunityInterval__c = 'Monthly';
         objAgree2.SignatureDate__c = Date.today();
         objAgree2.Agreement_Duration_in_month__c = '2';
         insert objAgree2;
         
         ApexPages.StandardController  controller = new ApexPages.StandardController(objAgree2);
         ApexPages.currentPage().getParameters().put('id',objAgree2.Id);  
         ApexPages.currentPage().getParameters().put(Label.CLAPR15SLS64,acc.Id);  
         VFC_NewAgreementLeadngBU leadBU = new VFC_NewAgreementLeadngBU(controller);
         leadBU.redirectToNewPage();
     }
}