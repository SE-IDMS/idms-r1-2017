/**
* @author Adrian MODOLEA (adrian.modolea@servicemax.com)
* @date 03/02/2016 (dd/mm/yyyy)
* @description Apex class for automatically calculate work order's Primary Territory using Territory Coverage definition
* @NOTE THIS FUNCTION REPLACES THE STANDARD SERVICE MAX FUNCTIONALITY !
	!!! Please ensure the ServiceMax setting SET049 is set to FALSE !!!
*
* VERSION HISTORY 
* @author Adrian MODOLEA (adrian.modolea@servicemax.com) 
* @date 02/05/2016 (dd/mm/yyyy)
* @description Put selective the query that retrieves the territory coverage
*/
public without sharing class ServiceMaxWOTerritoryAssignment 
{ 
	// Collection of field names (attributes names) from the WOs in the trigger collection
	public static Set<String> fieldTypes = new Set<String>(); 

	// Collection of field values (attributes values) from the WOs in the trigger collection
	public static Set<String> fieldValues = new Set<String>();
	
	// Collection of WO Status values to consider for the owner update (BR-10862, Nov 2016)
	private static Set<String> woStatusForOwnerChange = new Set<String>{
		'New', 'Unscheduled', 'Scheduled', 'Cancelled'
	};

	// Collection of pairs (ATTRIBUTE + WO FIELD NAME) that will be used in 
	//	this custom territory match rules functionality 
	//	(values are saved into a custom setting)
	public static Map<String, String> attributesMap 
	{
		get {
			/*'BU+Zip' 		=> '',
			'City' 			=> 'SVMXC__City__c',
			'Country' 		=> 'SVMXC__Country__c',
			'State' 		=> 'SVMXC__State__c',
			'Zip' 			=> 'SVMXC__Zip__c'
			'Zip Prefix' 	=> 'Zip_Prefix__c' */
			if(attributesMap == null)
			{
				attributesMap = new Map<String, String>();
				for(ServiceMax_TerritoryMapAttributes__c attribute : ServiceMax_TerritoryMapAttributes__c.getall().values())
					attributesMap.put(attribute.Name, attribute.WO_Field_Name__c);
			}
			return attributesMap;
		}
		private set; 
	}
	
	// Wrapper class that defines a SM Territory and its associated territory coverages
	//	Coverages are grouped by attribute type (Ex. Country, City, etc.)
	public class SMTerritory
	{
		public SVMXC__Territory__c territory			{get; set;}	// territory object information
		public Map<String, Set<String>> coverages		{get; set;} // coverage values grouped by attribute type
		public Map<String, Integer> countPerType		{get; set;} // how many coverages per attribute type
		
		// object contructor
		public SMTerritory(SVMXC__Territory__c territory)
		{
			this.territory = territory;
			this.coverages = new Map<String, Set<String>>();
			
			// init each attribute entry collection and associate an empty collection of values
			for(String key : ServiceMaxWOTerritoryAssignment.attributesMap.keySet())
				this.coverages.put(key, new Set<String>());
		}
	}

	// Collection of collections for storing all territories and related territory coverages information
	//	the first collection level is grouped by Territory Depth
	// return : Map< KEY : Depth , VALUE : Map< KEY : TeritoryId, VALUE : SMTerritory> > >
	public static Map<Integer, Map<Id, SMTerritory>> territoriesMap  
	{	
		get
		{
			if(territoriesMap == null)
			{
				territoriesMap = new Map<Integer, Map<Id, SMTerritory>>();
				
				Map<Id, Map<String, Integer>> territoryInfo = new Map<Id, Map<String, Integer>>();	
				List<SVMXC__Territory_Coverage__c> territoryCoverages = new List<SVMXC__Territory_Coverage__c>();
				
				// search all territories 
				for(SVMXC__Territory_Coverage__c tc : [SELECT SVMXC__Type__c, SVMXC__Value__c, 
															SVMXC__Territory__c, SVMXC__Territory__r.Name, SVMXC__Territory__r.SVMXC__Parent_Territory__c, SVMXC__Territory__r.Depth__c, SVMXC__Territory__r.OwnerId
														FROM SVMXC__Territory_Coverage__c
														WHERE SVMXC__Type__c IN :fieldTypes AND SVMXC__Value__c IN :fieldValues
														AND SVMXC__Active__c = true 
														ORDER BY SVMXC__Territory__r.Depth__c DESC, SVMXC__Territory__c LIMIT 10000])
				{
					territoryInfo.put(tc.SVMXC__Territory__c, new Map<String, Integer>());
					territoryCoverages.add(tc);	
				}
				
				// get info of the coverage type for each territory
				for(AggregateResult ar : [SELECT SVMXC__Territory__c, SVMXC__Type__c, count(Id) nb
											FROM SVMXC__Territory_Coverage__c
											WHERE SVMXC__Territory__c IN :territoryInfo.keySet()
											AND SVMXC__Active__c = true
											GROUP BY SVMXC__Territory__c, SVMXC__Type__c])
				{
					territoryInfo.get((Id)ar.get('SVMXC__Territory__c')).put( (String) ar.get('SVMXC__Type__c'), (Integer) ar.get('nb'));
				}
				
				for(SVMXC__Territory_Coverage__c tc : territoryCoverages)
				{
					Integer depth = (Integer) tc.SVMXC__Territory__r.Depth__c;
					
					if(!territoriesMap.containsKey(depth)) // first element for this depth
					{
						// create a SMTerritory for this territory coverage 
						SMTerritory smTerritory = new SMTerritory(tc.SVMXC__Territory__r);
						
						// associate the countPerType
						smTerritory.countPerType = territoryInfo.get(tc.SVMXC__Territory__c);
						
						// add this coverage value in the SMTerritory's coverages collection
						smTerritory.coverages.get(tc.SVMXC__Type__c).add(tc.SVMXC__Value__c);
						
						// initiate the collection of SMTerritory for this depth
						territoriesMap.put(depth, new Map<Id, SMTerritory>{tc.SVMXC__Territory__c => smTerritory});
					}
					else // territories have been already added for this depth 
					{
						// get the collection of SMTerritory associated to this depth
						Map<Id, SMTerritory> depthMap = territoriesMap.get(depth);
						
						if(!depthMap.containsKey(tc.SVMXC__Territory__c)) // first coverage for this territory
						{
							SMTerritory smTerritory = new SMTerritory(tc.SVMXC__Territory__r);
							smTerritory.coverages.get(tc.SVMXC__Type__c).add(tc.SVMXC__Value__c);
							smTerritory.countPerType = territoryInfo.get(tc.SVMXC__Territory__c);
							depthMap.put(tc.SVMXC__Territory__c, smTerritory);
						}
						else // coverages have already been added for this territory 
						{
							SMTerritory smTerritory = depthMap.get(tc.SVMXC__Territory__c);
							smTerritory.coverages.get(tc.SVMXC__Type__c).add(tc.SVMXC__Value__c);
						}
					}
				}
			}
			
			return territoriesMap;
		}
		private set;
	}

	// method to assign the primary territory for each work order
	public static void assignTerritory(List<SVMXC__Service_Order__c> workOrders)
	{
		for(SVMXC__Service_Order__c workOrder : workOrders)
		{
			// search if a territory is matching the WO criteria
			SMTerritory smTerritory = getTerritoryMatch(workOrder);
			
			if(smTerritory != null) {
				workOrder.SVMXC__Primary_Territory__c = smTerritory.territory.Id;
				// update the wo owner only for some of the WO Status values (BR-10862, Nov 2016)
				if(woStatusForOwnerChange.contains(workOrder.SVMXC__Order_Status__c))
					workOrder.OwnerId = smTerritory.territory.OwnerId;
			}
			else {
				/*DEFECT : XXXXX : remove reset the territory and the owner if no match*/ 
				//workOrder.SVMXC__Primary_Territory__c = null;
				//workOrder.OwnerId = UserInfo.getUserId();
			}
		}
	}
	
	// Method that looks into the Territory Hierarchy from the bottom level to the top
	// and tries to find the first territory that matches the work order attributes
	private static SMTerritory getTerritoryMatch(SVMXC__Service_Order__c workOrder)
	{
		System.debug('## AMO getTerritoryMatch for wo : ' + workOrder);
		SMTerritory currentSmTerritory = null;
		Integer currentTerritoryMatchCount = 0;
		
		System.debug('## AMO territoriesMap : ' 	+ territoriesMap);
		System.debug('## AMO fieldTypes : ' 		+ fieldTypes);
		System.debug('## AMO fieldValues : ' 		+ fieldValues);
		
		for(Map<Id, SMTerritory> depthMap : territoriesMap.values())
		{
			for(SMTerritory smTerritory : depthMap.values())
			{
				Integer territoryMatchCount = 0;
				
				// AMO (OCT 2016) : KSR000001411783 : the routing should check whether all specified coverages types
				//	are covered by the WO record
				// clone all the territory types; each time the a wo attribute is checked against the territory coverages
				//	the attribute will be removed from this collection; 
				// the territory can match only if all the territory attributes types are checked
				Set<String> territoryTypes = new Set<String>(smTerritory.countPerType.keySet());
				
				System.debug('## AMO : smTerritory in loop : (DEPTH ' + smTerritory.territory.Depth__c + ') ' + smTerritory.territory.Name + ' : ' + smTerritory); 
				
				// Determine if this Territory is eligible by searching the wo values into the terrotory coverages
				System.debug('## AMO : Start checking the WO fieldTypes : ' + fieldTypes);
				for(String key : fieldTypes)
				{
					String fieldName = attributesMap.get(key); // WO api field name
					String woFieldValue = (String) workOrder.get(fieldName); // current WO field value for this field name
					System.debug('## AMO : Fould value for ' + fieldName + ' : ' + woFieldValue);
					Set<String> exactCoverageValues = smTerritory.coverages.get(key); // exact coverage values for this territory and this field attribute (key)
					Integer countForType = smTerritory.countPerType.get(key);
					
					if(countForType != null && countForType > 0) 
					{
						// remove this key from the territoryTypes
						territoryTypes.remove(key);
						
						System.debug('## AMO : there are one or more values for this key attribute : ' + key);
						// there are one or more values for this key attribute
						// if the wo's value for this field name is in the coverage list then this territory may be eligible
						// otherwise this territory is not the good one
						if(exactCoverageValues.contains(woFieldValue)) {
							territoryMatchCount++;
							System.debug('## AMO : territoryMatchCount++ ' + territoryMatchCount);
						} else {
							territoryMatchCount = 0; 
							System.debug('## AMO : break : don\'t evaluate the other attributes');
							break; // don't evaluate the other attributes
						}
					} else {
						System.debug('## AMO : no value for this key attribute : ' + key); 
						//=> ignore, this territory has no definition for this attribute
					}
				}
				System.debug('## AMO : End check of the WO fieldTypes');
				
				if(territoryMatchCount > 0 && territoryTypes.size() == 0 && territoryMatchCount > currentTerritoryMatchCount)
				{
					currentTerritoryMatchCount = territoryMatchCount;
					currentSmTerritory = smTerritory;
				}
			}
		}
		
		System.debug('## AMO getTerritoryMatch territory found : ' + currentSmTerritory);
		System.debug('## AMO getTerritoryMatch territory match count : ' + currentTerritoryMatchCount);
			
		return currentSmTerritory;
	}
	
	
	
/*

Depth formula field to be created in the Work Order custom object (SVMXC__Service_Order__c) :

IF(
	ISBLANK(SVMXC__Parent_Territory__c),
	0,
	IF(
		ISBLANK(SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__c),
		1,
		IF(
			ISBLANK(SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__c),
			2,
			IF(
				ISBLANK(SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__c),
				3,
				IF(
					ISBLANK(SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__c),
					4,
					IF(
						ISBLANK(SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__c),
						5,
						IF(
							ISBLANK(SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__r.SVMXC__Parent_Territory__c),
							6,
							-1
						)
					)
				)
			)
		)	
	)
)

*/	

	
		   
}