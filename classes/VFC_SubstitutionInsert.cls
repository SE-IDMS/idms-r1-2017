/*****
Comments :Inserting withdrawal Substitution 
******/


public class  VFC_SubstitutionInsert {

   
    public Boolean IsErr{get;set;}
    public string nameFile{get;set;}
    public Blob ContentFile{get;set;}
    public boolean noFile{get; set;}
    public boolean vFlag{get; set;}
    public Integer totalNoOfHeaders;
    public List<Substitution__c> validWRs {get; set;}
    
    //
    public boolean eFlag{get;set;}
    public boolean IsCheck{get; set;}
    public Offer_Lifecycle__c OfferLifecycleObj {get; set;}{OfferLifecycleObj = new Offer_Lifecycle__c();}
    public string RCName{get; set;}
    public boolean afterValidate{get; set;}    
    public boolean IsError{get; set;}
    public boolean canCreate{get; set;}
    public string ImageValue{get; set;}
    public string ValidCheck{get; set;}
    public string poco{get; set;}
    public boolean NoRCs {get; set;}
    public boolean InvalidCommRef{get; set;}
    public boolean InvalidCustRes{get; set;}
    public boolean isDuplicate{get; set;}
    public boolean isNoCommRef{get;set;}
    public boolean validCustResRet{get;set;}
    public boolean validCustResRep{get;set;}
    public boolean isNoCusRes{get;set;}
    public boolean isNoQty{get;set;}
    public integer intRowNumber{get; set;}
        
        //Is Boolean April 2015
        public boolean blnValidAction{get;set;}
        public boolean blnVablnlidOldCommercialRef{get;set;}
        public boolean blnValidQuatyNew{get;set;}
        public boolean blnValidQuantyOld{get;set;}
        public boolean blnValidSubstionDate{get;set;}
        public boolean blnValidGroupSubst{get;set;}
        public boolean blnValidSubReason{get;set;}
        public boolean blnValidNewCommercialRef{get;set;}
        public boolean blnError{get; set;}
        public boolean blnValidContributor{get;set;}
        public boolean blnValidCountry{get;set;}
        public boolean blnValidNewRang{get;set;}
        public boolean blnIsDuplicate{get;set;}
         public boolean blnErrorToAvoidInst{get;set;}
        
    public List<WrapperSubstitution> lstWrapperRIs{get;set;}
    public List<WrapperSubstitution> lstWrapperRIs_AfterValidate{get;set;}
   
    
    map<string, list<string>>mapCaps=new map<string,list<string>>();
    Public list<selectOption> lstSelectOption{get;set;}
    Public string strProdF{get; set;}
    Public string strProductFamily {get; set;}
    set <String> stCustomerResolutions = new set <String> ();
    set <String> stProducFamily = new set <String> ();
    Set <string>setCRpicklistvalues = new Set<String>(); 
    Public set<string> keySet {get; set;}
    set<String> MasterKeySet = new Set<String> ();
    List<CS_WithdrawalSubstitutionCSVMap__c> HeaderValues {get;set;}
    map<String, string> mapCRType = new map<string, string>();
    
    //March release.
    public boolean blnIsAccess{get;set;}

    public VFC_SubstitutionInsert(ApexPages.StandardSetController controller) {
         IsErr=False; 
         blnIsAccess=false;
         blnErrorToAvoidInst=false;
         HeaderValues = new List<CS_WithdrawalSubstitutionCSVMap__c> ();
          id profileId = [Select Id,Name from Profile where name = 'System Administrator'].id;
         set<Id> userAccSet = new set<Id>();    
         if(apexpages.currentpage().getparameters().containskey('Id')) {
            string offerRecordId=ApexPages.CurrentPage().getParameters().get('Id');
            LIST<Offer_Lifecycle__c> offerList =[select Id,ownerid,Segment_Marketing_VP__c,Launch_Owner__c,Launch_Area_Manager__c,Marketing_Director__c,Withdrawal_Owner__c,Name,BoxSharedLink__c,BoxFolderId__c from Offer_Lifecycle__c where Id=: offerRecordId];
            if(offerList.size() >0) {
                OfferLifecycleObj=offerList[0];
                userAccSet = new set<Id>{OfferLifecycleObj.ownerid,OfferLifecycleObj.Segment_Marketing_VP__c,OfferLifecycleObj.Launch_Area_Manager__c,OfferLifecycleObj.Marketing_Director__c,OfferLifecycleObj.Withdrawal_Owner__c};
            }
         }
         if(userAccSet.contains(Userinfo.getuserid()) || userinfo.getProfileid()==profileId) {
         //check the Duplicates
         list<Substitution__c> listWithdrawal=[select id,Offer__c,OldCommercialReference__c from Substitution__c where Offer__c=:OfferLifecycleObj.id];
         
         if(listWithdrawal.size() > 0) {
            existingReference(listWithdrawal);
         }
         //CS_WithdrawalSubstitutionCSVMap__c
         List<CS_WithdrawalSubstitutionCSVMap__c> csList =CS_WithdrawalSubstitutionCSVMap__c.getall().values();
         system.debug('TEST------->'+CS_WithdrawalSubstitutionCSVMap__c.getall().values());
         for(CS_WithdrawalSubstitutionCSVMap__c CsWS:csList ) {
            if(CsWS.Object__C=='Substitution__c') {
                 CS_WithdrawalSubstitutionCSVMap__c test=new CS_WithdrawalSubstitutionCSVMap__c ();
                 test=CsWS;
                 HeaderValues.add(test);
            }
         }
        totalNoOfHeaders=HeaderValues.size();
        system.debug(HeaderValues);
         }else {
             blnIsAccess=True;
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLMAR16ELLAbFO11));//  'You dont have access to create Substitution'
          
         }

    }
    
    //Checking Duplicates  // prepare MasterKey Set
    
    public void existingReference(list<Substitution__c> listExistWithdrawal) {
           string strKey;
         for(Substitution__c WRObj : listExistWithdrawal ){
         
            if(WRObj.OldCommercialReference__c!=null){
                MasterKeySet.add(WRObj.OldCommercialReference__c);
            }   
         }
     
    } 
    
    //
    //
    public Pagereference displayCSVEntries(){
     
        List<List<String>> CsvList = new List<List<String>>();
        List<String> headerRow=new List<String>();
        afterValidate = False;
        lstWrapperRIs=new list<WrapperSubstitution>();
        lstWrapperRIs_AfterValidate=new list<WrapperSubstitution>();
       

        // Check whether CSV content and Parse
        if(ContentFile!=null )
        {
            try{
                CsvList = parseCSV(ContentFile.tostring(),false);
                system.debug('## CsvList after Parsing ##'+ CsvList );
                }
            catch(Exception ex)     
            {
                noFile=True;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLOCT13RR43)); // CLOCT13RR43 -> Invalid file content.
            }
        }
        else
        {
            noFile=True;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLOCT13RR42)); // CLOCT13RR42->Please Choose File before you Click UploadFile button
            return null;
        }

        // Check Parsed CsvList 
        if(CsvList.isempty() || CsvList.size()==1)
        {
            noFile=True;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLOCT13RR44)); //CLOCT13RR44 -> No Data to import.
            return null;
        }
        else
        {
            // get the Header row from the CSV and validate whether columns are as per template
            headerRow=csvList[0];   
            system.debug('@@csvlist size'+ csvList.size());
            if(!validateHeaders(headerRow))
            {
            noFile=True;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLOCT13RR45));  //CLOCT13RR45 -> Invalid File
            return null;
            }  
            else
            {
               if(csvList.size()>integer.valueof(Label.CLOCT13RR69))  //CLOCT13RR69 - 400
               {
                   noFile=True;
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLOCT13RR68));  //CLOCT13RR68 -> Number of rows in the CSV File exceeds the Limit.
                   return null;
               }
              
              try{

                 //String CN = ReturnRequestRecord.Case__r.Account.Country__c;    
                 //String ProdCond = ReturnRequestRecord.ProductCondition__c;        
                 String gmr = '';
                    for (Integer i=1;i<CsvList.size();i++)
                    {
                        String[] inputvalues = new String[]{};
                        inputvalues = CsvList[i];                 
                        Integer rNum;
                        
                        rNum=i+1;
                       
                       //CommRef- inputvalues[0],ProdF - inputvalues[1], Qty - inputvalues[2], CRS - inputvalues[3], SN - inputvalues[4], DC - inputvalues[5]
                        for(integer k=1; k>=15; k--)
                        { 
                       
                            if(inputvalues.size()<=headerRow.size()-k)
                            {
                                inputvalues[totalNoOfHeaders-k]='';
                            }
                            else
                            {
                                inputvalues[totalNoOfHeaders-k]= removeSpclChar(inputvalues[totalNoOfHeaders-k]).toUpperCase();
                            }
                            system.debug('@@inputvalues['+string.valueof(totalNoOfHeaders-k)+']'+inputvalues[totalNoOfHeaders-k] );
                        }
                        
                    
                                    
                         lstWrapperRIs.add(new WrapperSubstitution( rNum,isDuplicate,IsCheck,inputvalues[0],inputvalues[1],inputvalues[2],inputvalues[3],inputvalues[4],inputvalues[5],inputvalues[6],inputvalues[7],inputvalues[8],inputvalues[9],inputvalues[10],inputvalues[11],inputvalues[12],inputvalues[13],inputvalues[14],false,false,false,false,false,false,false,false,false,false)); 
                        
                    }
                }
              catch(Exception ex)     
              {
                 noFile=True;
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getmessage() + ex.getStackTraceString()));
              }
           }        
        }
        system.debug(' @@ lstWrapperRIs - Before Validation'+ lstWrapperRIs);
        return null;
       }
       
       
       //ParseCSV method helps to Parse the CSV content (blob) into list of Strings.
      @TestVisible private List<List<String>> parseCSV(String contents,Boolean skipHeaders)  {
     
        List<String> fieldList = new List<string>();
        List<List<String>> allFields = new List<List<String>>();
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field        
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try 
        {
            lines = contents.split('\r'); // using carriage return accomodates windows, unix, and mac files
            //http://www.maxi-pedia.com/Line+termination+line+feed+versus+carriage+return+0d0a
        } 
        catch (System.ListException e) 
        {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        Integer num = 0;
        for(String line: lines) 
        {
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) break;
            List<String> fields = line.split(',');
            while(fields.size()!=totalNoOfHeaders)  // if the last column in the csv file of any row doesnt have any value then replace that with empty string.
            {
               fields.add('');
            } 
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field: fields) 
            {                
                if (field.startsWith('"') && field.endsWith('"')) 
                {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } 
                else if (field.startsWith('"')) 
                {
                    makeCompositeField = true;
                    compositeField = field;
                } 
                else if (field.endsWith('"')) 
                {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } 
                else if (makeCompositeField) 
                {
                    compositeField +=  ',' + field;
                } 
                else 
                {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        if(allFields.size() > 0)
            fieldList = allFields[0];
        return allFields;       
    }
    
    public boolean validateHeaders(List<String> csvHeaderRow){
    
      boolean hFlag = true;
      if(totalNoOfHeaders==csvHeaderRow.size()){
      
          for(integer i=0;i<=csvHeaderRow.size();i++)
          { 
             for(CS_WithdrawalSubstitutionCSVMap__c cs: HeaderValues)
             {
                system.debug('## cs.ColumnNumber__c'+ i+'=' +integer.valueof(cs.ColumnNumber__c) );
                if(i==integer.valueof(cs.ColumnNumber__c)) { 
                    if(csvHeaderRow[i]!=cs.HeaderName__c){
                        hFlag=false;
                    }
                }
             }
          }
      }
      else { 
        hFlag=false;
      }
      system.debug('##hflag'+ hFlag);
      return hFlag; 
    }
    
    // removeSpclChar method helps to remove the special characters \n and " from the CSV Columns
    @TestVisible private string removeSpclChar(String inputValue) {
   
       if(inputValue.contains('\n')) {inputValue = inputValue.remove('\n');}
       if(inputValue.contains('"')) {inputValue=inputValue.remove('"');}
       return inputValue.trim(); 
    }
    
    // Validated  method is used to validate and display the CSV data.
    Public pagereference displayWithdrReferenceAfterValidation(){
                 afterValidate=true;
                 blnErrorToAvoidInst=false;
                 keySet = new set<string>();
                lstWrapperRIs_AfterValidate = new List<WrapperSubstitution> ();
                map<string, string> mapNewCommRef = new map<string, string>();
                map<string, string> mapOldCommRef = new map<string, string>();
                
                set<string> pickListActionSet = new set<string>();
                
                List<string> lstNewCommRef = new List<String>();
                List<string>  lstOldCommRef = new List<String>();
                
                vFlag=true;
                boolean blnNoOdComRef =false;
                boolean blnNoNewComRef =false;
                blnValidAction=false;
                blnVablnlidOldCommercialRef=false;
                blnValidQuatyNew=false;
                blnValidQuantyOld=false;
                blnValidSubstionDate=false;
                blnValidGroupSubst=false;
                blnValidSubReason=false;
                blnValidNewCommercialRef=false;
                blnError=false;
                blnValidContributor=false;
                blnValidCountry=false;
                blnValidNewRang=false;
                blnIsDuplicate=false;
                canCreate=true;
                
                
               
                Schema.DescribeFieldResult fieldResult = Substitution__c.Action__c.getDescribe();
                List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
                for (Schema.PicklistEntry a : values){
                 
                        pickListActionSet.add(a.getValue());
                }
                set<string> picklstReason = new  set<string>();
                //Reason
                Schema.DescribeFieldResult fieldReason = Substitution__c.SubstitutionReason__c.getDescribe();
                List<Schema.PicklistEntry> valuesRs = fieldReason.getPicklistValues();
                for (Schema.PicklistEntry a : valuesRs){
                    picklstReason.add(a.getValue());
                }
               
                
                for(WrapperSubstitution wri : lstWrapperRIs){
                        
                    if(wri.NewCommercialReference!='' && wri.NewCommercialReference!=null){
                            lstNewCommRef.add(wri.NewCommercialReference);                           
                    }
                    if(wri.OldCommercialReference!='') {
                            lstOldCommRef.add(wri.OldCommercialReference);
                    }
           
                }
               
                mapNewCommRef =getOldCommRef(lstNewCommRef);
                mapOldCommRef =getOldCommRef(lstOldCommRef);
                
                         
                for(WrapperSubstitution wrpObj:lstWrapperRIs) {
                
                      blnVablnlidOldCommercialRef=false;
                      blnIsDuplicate=false;
                        
                        if(wrpObj.OldCommercialReference!='') {
                                if(!mapOldCommRef.containskey(wrpObj.OldCommercialReference))     {
                                  
                                    blnVablnlidOldCommercialRef=true;
                                     blnIsDuplicate=true;
                                    lstWrapperRIs_AfterValidate.add(new WrapperSubstitution( wrpObj.intRowNumber,isDuplicate,false,wrpObj.SubstitutionId,wrpObj.SubstituteId,wrpObj.Action,wrpObj.OldCommercialReference,wrpObj.QuantityOld,wrpObj.SubstitutionBeginning,wrpObj.CommentGB,wrpObj.GroupingOfSubstitutions,wrpObj.SubstitutionReason,wrpObj.NewCommercialReference,wrpObj.Newrange,wrpObj.QuantityNew,wrpObj.SubGroup,wrpObj.Contributor,wrpObj.Country,false,true,false,false,false,false,false,false,false,false)); 
                        
                                }
                                system.debug('t2');
                        }
                        else if(wrpObj.OldCommercialReference==''){
                            blnVablnlidOldCommercialRef=true;
                             blnIsDuplicate=true;
                            lstWrapperRIs_AfterValidate.add(new WrapperSubstitution( wrpObj.intRowNumber,isDuplicate,false,wrpObj.SubstitutionId,wrpObj.SubstituteId,wrpObj.Action,wrpObj.OldCommercialReference,wrpObj.QuantityOld,wrpObj.SubstitutionBeginning,wrpObj.CommentGB,wrpObj.GroupingOfSubstitutions,wrpObj.SubstitutionReason,wrpObj.NewCommercialReference,wrpObj.Newrange,wrpObj.QuantityNew,wrpObj.SubGroup,wrpObj.Contributor,wrpObj.Country,false,true,false,false,false,false,false,false,false,false)); 
                            system.debug('t3');
                        }
                        
                       
                       boolean check_Duplicate=false;
                       if(wrpObj.NewCommercialReference!=null && wrpObj.NewCommercialReference!='') {
                            check_Duplicate=checkDuplicate(wrpObj.NewCommercialReference) ;
                       }
                    if(!blnVablnlidOldCommercialRef && check_Duplicate) {
                        blnIsDuplicate=true;
                        system.debug('thhahahah--TEST');
                        canCreate=True;
                        lstWrapperRIs_AfterValidate.add(new WrapperSubstitution( wrpObj.intRowNumber,blnIsDuplicate,false,wrpObj.SubstitutionId,wrpObj.SubstituteId,wrpObj.Action,wrpObj.OldCommercialReference,wrpObj.QuantityOld,wrpObj.SubstitutionBeginning,wrpObj.CommentGB,wrpObj.GroupingOfSubstitutions,wrpObj.SubstitutionReason,wrpObj.NewCommercialReference,wrpObj.Newrange,wrpObj.QuantityNew,wrpObj.SubGroup,wrpObj.Contributor,wrpObj.Country,false,true,false,false,false,false,false,false,false,false)); 
                    } 

                    if(!blnIsDuplicate) {
                        system.debug('thahahhaha(((((--TEST');
                        canCreate=true;
                        lstWrapperRIs_AfterValidate.add(new WrapperSubstitution( wrpObj.intRowNumber,false,true,wrpObj.SubstitutionId,wrpObj.SubstituteId,wrpObj.Action,wrpObj.OldCommercialReference,wrpObj.QuantityOld,wrpObj.SubstitutionBeginning,wrpObj.CommentGB,wrpObj.GroupingOfSubstitutions,wrpObj.SubstitutionReason,wrpObj.NewCommercialReference,wrpObj.Newrange,wrpObj.QuantityNew,wrpObj.SubGroup,wrpObj.Contributor,wrpObj.Country,false,false,false,false,false,false,false,false,false,false)); 

                    }   
                    
                    
                        
                
                }
                
                for(WrapperSubstitution wrapperObj:lstWrapperRIs_AfterValidate) {
                    if(wrapperObj.Duplicate==true || wrapperObj.isValidOldCommercialRef==true) {
                        canCreate=false;
                         
                        break; 
                    }
                    
                }
                if(!canCreate) {
                    blnErrorToAvoidInst=true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLJULY15ELLAbFO01));
                }
                
                
                system.debug('TEST HANU------->'+lstWrapperRIs_AfterValidate);
        return null;
    }
    
    public pagereference insertWithdrReference() {
    //
    system.debug(' $$ lstWrapperRIs_AfterValidate $$'+ lstWrapperRIs_AfterValidate );
          validWRs = new List<Substitution__c>();
          for(WrapperSubstitution wrpSubstion: lstWrapperRIs_AfterValidate)
          {
            If(wrpSubstion.isRCOverride == True) {

                Substitution__c withRefObj = new Substitution__c(Offer__c=OfferLifecycleObj.id);
                withRefObj.Action__c =wrpSubstion.Action;
                withRefObj.Comment__c =wrpSubstion.CommentGB;
                withRefObj.Contributor__c =wrpSubstion.Contributor;
                withRefObj.Country__c =wrpSubstion.Country;
                withRefObj.Group__c =wrpSubstion.SubGroup;
                withRefObj.GroupingOfSubstitutions__c =wrpSubstion.GroupingOfSubstitutions;
                withRefObj.NewcommercialReference__c =wrpSubstion.NewCommercialReference;
                withRefObj.NewRange__c =wrpSubstion.Newrange;
                if(wrpSubstion.QuantityNew!='') {
                    withRefObj.QuantityNew__c =decimal.valueof(wrpSubstion.QuantityNew.trim());
                }
                if(wrpSubstion.QuantityOld!='') {
                    withRefObj.QuantityOld__c =decimal.valueof(wrpSubstion.QuantityOld.trim());
                }
                if(wrpSubstion.SubstituteId!='') {
                    if(string.isNotBlank(wrpSubstion.SubstituteId)) {
                        withRefObj.SubstituteId__c =decimal.valueof(wrpSubstion.SubstituteId.trim());
                    }
                }
                if(wrpSubstion.SubstitutionBeginning!='') {
                    if(wrpSubstion.SubstitutionBeginning.contains('-')) {
                        string str=wrpSubstion.SubstitutionBeginning.trim().replace('-','/');
                        date mydate = date.parse(str);
                        //Date myDate = date.valueOf(str);
                        
                        withRefObj.SubstitutionBeginning__c =myDate;
                    }else {
                        string str=wrpSubstion.SubstitutionBeginning.trim();
                        date mydate = date.parse(str);
                        //Date myDate = date.valueOf(str);
                        
                        withRefObj.SubstitutionBeginning__c =myDate;
                    }
                    
                    
                }
                if(wrpSubstion.SubstitutionId!='') {
                    string strTrim=wrpSubstion.SubstitutionId.trim();
                    
                        if(string.isNotBlank(strTrim)) {
                            system.debug('strTrim'+strTrim);
                            withRefObj.SubstitutionId__c =decimal.valueof(strTrim);
                        }   
                    
                }
                
                withRefObj.SubstitutionReason__c =wrpSubstion.SubstitutionReason;
                

                withRefObj.OldCommercialReference__c=wrpSubstion.OldCommercialReference;
                validWRs.add(withRefObj);
            }
          }
          
          pagereference pr;
          if(validWRs.size()==0)
          {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.CLOCT13RR53));   //CLOCT13RR53 - No records to import
            eFlag=True;
            lstWrapperRIs_AfterValidate.clear();
           displayWithdrReferenceAfterValidation();
            pr = null;
          }
          else
          { try{    
             Insert validWRs;
              pr = new PageReference('/'+OfferLifecycleObj.id );
             }catch(exception exp) {
                eFlag=true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,exp.getmessage() + exp.getStackTraceString()));
                 pr = null;
             }
            
          }
          return pr;
    
    }
    
   
    
     Public  boolean checkDuplicate(String CommRef){
        String Key;
        key = CommRef;
        if(keySet.size()==0){
            keySet.add(key);
            
                isDuplicate=False;
            
        }
        else{

            if(keySet.contains(key)){

                isDuplicate=True;
                IsCheck = False;
            }
            else{

                isDuplicate=False;
                keySet.add(key);
            }
        }
        SYSTEM.DEBUG('@@ CHECK DUPLICATE - ISDUPLICATE'+ isDuplicate);
        return IsDuplicate;
    }
    
    
    //Checking Valid commercial  ref
    public  Map<string, string> getOldCommRef(List<String> lstCommRef)
     {
        map<String, string> mapOldCommRef = new map<string, string>();
                for(Product2 productObj: [select Name,ProductCode,ProductGDP__r.BusinessUnit__c,ProductGDP__r.ProductLine__c,ProductGDP__r.ProductFamily__c,ProductGDP__r.Family__c,ProductGDP__r.SubFamily__c,ProductGDP__r.Series__c,ProductGDP__r.GDP__c,GDPGMR__c from Product2 where Name In:lstCommRef])
                {
                         mapOldCommRef.put(productObj.name,productObj.name);
                }
        
        
         
          
        return mapOldCommRef;
     }
         
        public static boolean isValidDate(String inDate) {
                
                
                try {
                Datetime dt = DateTime.parse(inDate);
                } catch (exception pe) {
                        return false;
                }
                return true;
        }

     public void sortWrapperItems(List<WrapperSubstitution> lstWrapperRIs_AfterValidate){
  
      boolean swapped=true;
      integer j = 0;
      WrapperSubstitution tmp;
      while (swapped){
        swapped = false;
        j++;
        for (integer i = 0; i < lstWrapperRIs_AfterValidate.size() - j; i++) {
          if (lstWrapperRIs_AfterValidate[i].intRowNumber > lstWrapperRIs_AfterValidate[i + 1].intRowNumber){
               tmp = lstWrapperRIs_AfterValidate[i];
               lstWrapperRIs_AfterValidate[i] = lstWrapperRIs_AfterValidate[i + 1];
               lstWrapperRIs_AfterValidate[i + 1] = tmp;
               swapped = true;
           }
        }
      }
  }
    
    public pagereference docancel()  {
   
       PageReference pr = new PageReference('/'+OfferLifecycleObj.id );
       return pr;
    }
        
        @TestVisible private boolean QtyNumberCheck(string Qty)
        {
                Pattern isnumbers = Pattern.Compile(Label.CLOCT13RR61);  //CLOCT13RR61 -> ^[0-9]+$
                Matcher patternMatch = isnumbers.matcher(Qty);
                return patternMatch.Matches();
        }

    
    //Wrapper class
    
    public class WrapperSubstitution{
        public boolean isRCOverride{get;set;}         
      
        public string  Action{get;set;}
       
        public string CommentGB{get;set;}
        public string Contributor {get;set;}
        public string Country{get;set;}
        public string SubGroup {get;set;}
        public string GroupingOfSubstitutions{get;set;}
        public string Newrange{get;set;}
        public string OldCommercialReference{get;set;}
        public string NewCommercialReference{get;set;}
        public string QuantityNew{get;set;}
        public string QuantityOld{get;set;}
        public string SubstituteId{get;set;}
        public string SubstitutionBeginning{get;set;}
        public string SubstitutionId{get;set;}
        public string SubstitutionReason{get;set;}
        public integer intRowNumber{get; set;}
        
        public boolean isValidAction{get;set;}
        public boolean isValidOldCommercialRef{get;set;}
        public boolean isValidQuatyNew{get;set;}
        public boolean isValidQuantyOld{get;set;}
        public boolean isValidSubstionDate{get;set;}
        public boolean isValidGroupSubst{get;set;}
        public boolean isValidSubReason{get;set;}
        public boolean IsValidNewCommercialRef{get;set;}
         public boolean isError{get; set;}
        public boolean isValidContributor{get;set;}
        public boolean isValidCountry{get;set;}
        public boolean Duplicate{get;set;}
        
        public WrapperSubstitution(Integer RowNumber,boolean isDep,boolean isSelected,string strSubstId,string strsubId,string strActn,string strOldComref,string strQtyO,string daSubDate,string strComtGB,string strgrpSub,string srrRes,string strNewComref,string strnewRg,string strQtyN,string strGrp,string strConbutor,string strCoutry,boolean blAt,boolean bloldCR,boolean blQtN,boolean blQtO ,boolean blDt,boolean blGrSb,boolean blRs ,boolean blnCR,boolean blContrb,boolean blCtry) {
            intRowNumber =RowNumber;
            isRCOverride = isSelected;
            Action =strActn;
            CommentGB=strComtGB;
            Contributor =strConbutor;
            Country=strCoutry;
            SubGroup =strGrp; 
            GroupingOfSubstitutions=strgrpSub;
            Newrange=strnewRg;
            OldCommercialReference=strOldComref;
            NewCommercialReference =strNewComref;
            QuantityNew=strQtyN;
            QuantityOld=strQtyO;
            SubstituteId=strsubId;
            SubstitutionBeginning=daSubDate;
            SubstitutionId=strSubstId;
            SubstitutionReason=srrRes;
            
            isValidAction =blAt;
            isValidOldCommercialRef =bloldCR;
            isValidQuatyNew=blQtN;
            isValidQuantyOld=blQtO;
            isValidSubstionDate=blDt;
            isValidGroupSubst =blGrSb;
            isValidSubReason =blRs;
            IsValidNewCommercialRef=blnCR;
            isValidContributor =blContrb;
            isValidCountry =blCtry;
            Duplicate=isDep;
           
        }
     
    }
    
   
    
}//