global class Batch03_CustomerVisitKPI implements Database.Batchable<sObject>,Schedulable {
    private String queryString='';
    String startDate='';
    String endDate='';
    Map<String,String> startEndDateMap=new Map<String,String>();
  
       global Database.QueryLocator start(Database.BatchableContext BC){       
        return Database.getQueryLocator(queryString);
    }
    
    global Batch03_CustomerVisitKPI()
    {
    //only for the test method
    }
    
    global Batch03_CustomerVisitKPI (String sdate,String edate)
    {
        startDate=sdate;
        endDate=edate;
        if(!Test.isRunningTest())
        this.queryString='select id,ownerId,ActivityDate from Event where RecordType.Name=\'Customer Visit\' and Status__c=\'closed\' and startDateTime>='+startDate+' and startDateTime<='+endDate;
        else
        this.queryString='select id,ownerId,ActivityDate from Event where RecordType.Name=\'Customer Visit\' and Status__c=\'closed\' and startDateTime>='+startDate+' and startDateTime<='+endDate+' LIMIT 2';
    } 
    
    global void execute(SchedulableContext sc){
        //the initial schedule is only controlled        
        //get the start date and end date of the last 3 months
        //lets say that this batch is scheduled to run on 1st of every month
        
        createBatches();
        //For previous month        
    }
    
    global void createBatches()
    {
        DateTime test=System.now().addDays(System.now().day()*-1);
        DateTime endTimePrevMonth=datetime.newInstanceGmt(test.year(), test.month(), test.day(), 23, 59, 59);

        DateTime test1=System.now().addDays((System.now().day() -1)*-1).addMonths(-1);
        DateTime startTimePrevMonth=datetime.newInstanceGmt(test1.year(), test1.month(), test1.day(), 0, 0, 0);
        
        //clear the records and run the batch        
        database.executeBatch(new Batch03_CustomerVisitKPI(formatDateString(startTimePrevMonth),formatDateString(endTimePrevMonth)));
        
        
        //For the previous-previous month
        DateTime startTimePrevPrevMonth=startTimePrevMonth.addMonths(-1);
        DateTime tempEndTime=startTimePrevMonth.addDays(-1);
        DateTime endTimePrevPrevMonth=datetime.newInstanceGmt(tempEndTime.year(), tempEndTime.month(), tempEndTime.day(), 23, 59, 59);
                      
        database.executeBatch(new Batch03_CustomerVisitKPI(formatDateString(startTimePrevPrevMonth),formatDateString(endTimePrevPrevMonth)));                        
    }
    
    global static String formatDateString(DateTime dt)
    {
    //    2011-07-01T00:00:00.000Z
    String finalDate='';
    finalDate=dt.year()+'-';
    if(dt.month()<10)
    finalDate+='0'+dt.month()+'-';
    else
    finalDate+=dt.month()+'-';
    
    if(dt.day()<10)
    finalDate+='0'+dt.day()+'T';
    else
    finalDate+=dt.day()+'T';
    
    
    if(dt.hour()<10)
    finalDate+='0'+dt.hour()+':';
    else
    finalDate+=dt.hour()+':';
    
    if(dt.minute()<10)
    finalDate+='0'+dt.minute()+':';
    else
    finalDate+=dt.minute()+':';
    
    if(dt.second()<10)
    finalDate+='0'+dt.second()+'.000Z';
    else
    finalDate+=dt.second()+'.000Z';
    
    return finalDate;
        //return (dt.year()+'-'+dt.month()+'-'+dt.day()+'T'+dt.hour()+':'+dt.minute()+':'+dt.second()+'.000Z');
    }
    
       global void execute(Database.BatchableContext BC, List<sObject> scope){
           
        Map<String, CustomerVisitKPI__c> dailyMap = new Map<String, CustomerVisitKPI__c>();
        Map<String, CustomerVisitKPI__c> monthlyMap = new Map<String, CustomerVisitKPI__c>();
        Set<Id> ownerIds = new Set<Id>();
        
        // retreive the list of ownerIds and maintain in a list
        for (Event e : (Event[])scope){
            ownerIds.add(e.ownerId);
        }
        Map<id,User> activeStatus =new Map<id,User>([select IsActive from User where id in :ownerIds]);        
        
        // The execution happens in multiple batches so from batch 2 the existing records are taken into account.
        //  get daily records already created and put into Map , this is to manag
        Date specialStartDate=date.valueOf(startDate);
        Date specialEndDate=date.valueOf(endDate);
        
        for(CustomerVisitKPI__c  dailyrecordKPI: [select id, date__c, DailyCount__c, ownerid from CustomerVisitKPI__c where date__c <= :specialEndDate and date__c>=:specialStartDate  and isMonthlyRecord__c = false and ownerId IN :ownerIds]){
            dailyMap.put(dailyrecordKPI.ownerid + '_' + dailyrecordKPI.date__c,dailyrecordKPI);         
        }

        //  get monthly records already created and put into Map
        for(CustomerVisitKPI__c monthlyrecordKPI: [select id, date__c, NoofDayswithVisitsgreaterthan2__c, NoofDayswithVisitsbetween0and2__c, ownerid,EventIdsList__c from CustomerVisitKPI__c where date__c <= :specialEndDate and date__c>=:specialStartDate and isMonthlyRecord__c = true and ownerId IN :ownerIds]){
            monthlyMap.put(monthlyrecordKPI.ownerid, monthlyrecordKPI);
        }

        for(Event e : (Event[])scope){
        if(activeStatus.get(e.ownerId).IsActive){
        
            String key = e.ownerId + '_' + e.ActivityDate;
            //if the month;y record does not exist for this user or if the monthly record exists and the event is not in the eventIdsList field of a monthly record
            if(!monthlyMap.containsKey(e.ownerId) || (monthlyMap.containsKey(e.ownerId) && monthlyMap.get(e.ownerId).EventIdsList__c!=null && !monthlyMap.get(e.ownerId).EventIdsList__c.contains(e.id))){
                if(dailyMap.containsKey(key)){              
                    CustomerVisitKPI__c dailyrecord = dailyMap.get(key);                
                    dailyrecord.DailyCount__c += 1;             
                    // we only increment the > 2 day field on Month record when passing the threshold
                    if(monthlyMap.get(e.ownerid).EventIdsList__c!=null)
                    monthlyMap.get(e.ownerid).EventIdsList__c+=','+e.id;
                    else
                    monthlyMap.get(e.ownerid).EventIdsList__c=e.id;
                    
                    if(dailyrecord.DailyCount__c == 3){
                        CustomerVisitKPI__c monthlyrecord = monthlyMap.get(e.ownerid);
                        monthlyrecord.NoofDayswithVisitsgreaterthan2__c += 1;            //  ensure we increment > 2 visits monthly figure batchTable record
                        monthlyrecord.NoofDayswithVisitsbetween0and2__c -= 1;            //  and decrement days with visit < 2 > 0;
                    }
                }
                else{
                    //  create a daily record as it doesn't yet exist for this user and day combo
                    CustomerVisitKPI__c newDailyRecord = new CustomerVisitKPI__c();
                    newDailyRecord.date__c = e.ActivityDate;
                    newDailyRecord.DailyCount__c = 1;
                    newDailyRecord.ownerid = e.ownerid;
                    dailyMap.put(key, newDailyRecord);
                    System.debug('#dailyMap.values()'+dailyMap.values());
                    //  check if monthly record exists for user for this month and create if not
                    if(!monthlyMap.containsKey(e.ownerid)){
                        CustomerVisitKPI__c newMonthlyRecord = new CustomerVisitKPI__c();
                        newMonthlyRecord.date__c = e.ActivityDate.toStartOfMonth();
                        newMonthlyRecord.NoofDayswithVisitsbetween0and2__c = 1;
                        newMonthlyRecord.NoofDayswithVisitsgreaterthan2__c = 0;
                        newMonthlyRecord.isMonthlyRecord__c = true;
                        newMonthlyRecord.ownerid = e.ownerid;                       
                        monthlyMap.put(e.ownerid, newMonthlyRecord);
                    }else{
                        CustomerVisitKPI__c monthlyrecord = monthlyMap.get(e.ownerid);
                        monthlyrecord.NoofDayswithVisitsbetween0and2__c += 1;                   
                    }               
                    if(monthlyMap.get(e.ownerid).EventIdsList__c!=null)
                    monthlyMap.get(e.ownerid).EventIdsList__c+=','+e.id;
                    else
                    monthlyMap.get(e.ownerid).EventIdsList__c=e.id;
                    
                }
            }
          }
        }

        // commit records
        System.debug('#dailyMap.values()2'+dailyMap.values());
        upsert dailyMap.values();
        upsert monthlyMap.values();
    }    
/*********************************************************************************
    Method Name      : start
    Parameters       : Database.BatchableContext
    Purpose          : The finish method is called after all batches are processed. 
                       A detailed mail is sent to the mail id specified in the custom label CL00081 regarding the batch execution.
    Created by       : Siddharatha Nagavarapu Global Delivery Team
    Date created     : 25th October 2011
    Modified by      :
    Date Modified    :
    Remarks          : For Dec - 11 Release
 ********************************************************************************/       
    global void finish(Database.BatchableContext BC){
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems from AsyncApexJob where Id = :BC.getJobId()];
        //send email with any errors 
        if(a.NumberOfErrors > 0 || Test.isRunningTest()){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(System.Label.CL00081.split(','));
            mail.setSubject('report Regarding the monthly update of customer visit KPI\'s');
            mail.setPlainTextBody(System.Label.CL00084 + a.TotalJobItems + '/' + a.NumberOfErrors + '. ' + System.Label.CL00085 );
            
            if(!Test.isRunningTest())                          // ensure we never actually send an email during //a Test Run
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
     }
     
}