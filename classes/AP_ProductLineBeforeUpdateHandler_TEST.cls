/*
    Author          : Bhuvana S
    Description     : Test class for AP_ProductLineBeforeUpdateHandler
*/
@isTest
private class AP_ProductLineBeforeUpdateHandler_TEST 
{
    static testMethod void testProdLine()
    {
        CS_ProductLineBusinessUnit__c prodBU1= new CS_ProductLineBusinessUnit__c(Name='BDADS',BUCode__c='test',ProductBU__c='BUILDING MANAGEMENT',ProductLine__c='BDADS - BLDGS FIELD SERVICES',SystemIdReference__c='test');
        insert prodBU1;
        
        CS_ProductLineBusinessUnit__c prodBU2= new CS_ProductLineBusinessUnit__c(Name='BDIBS',BUCode__c='test',ProductBU__c='BUILDING MANAGEMENT',ProductLine__c='BDIBS - BDIBS FIELD SERVICES',SystemIdReference__c='test');
        insert prodBU2;
       
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2;
        
        OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
        pl1.Quantity__c = 2;
        pl1.Amount__c = 2;  
        pl1.PartnerProductLine__c ='BDADS';
        insert pl1;
        
        pl1.Amount__c = 1;
        pl1.Amount__c = 1; 
        pl1.PartnerProductLine__c ='BDIBS';
        update pl1;

    }
}