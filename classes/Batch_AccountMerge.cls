global class Batch_AccountMerge implements Database.Batchable<AggregateResult>, Database.stateful, Schedulable{
    
    global DateTime startTime;
    global DateTime endTime;
    global List<PRM_Technical_Merge_Record__c> listRecords = new List<PRM_Technical_Merge_Record__c>();
    
    global Iterable<AggregateResult> start(Database.BatchableContext BC) {
        return new AggregateResultMergeAccIterable();
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        startTime = datetime.now();
        Map<string,integer> elligibleAccMap = new Map<string,integer>();
        //Process all AggregateResult and store them in a Map with the count of each group
        for (Sobject so : scope)  {
            AggregateResult ar = (AggregateResult)so;
            elligibleAccMap.put(string.valueof(ar.get('SEAccountID__c')),Integer.valueOf(ar.get('expr0')));
        }
        System.debug('##########################elligibleAccMap = ' + elligibleAccMap);
        
        //Get Business Account & PRM Account RecordType IDs
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Id partnerAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='PRMAccount' limit 1][0].Id;
        
        //Get all accounts from the elligibleAccMap and store SurvivorAccount and ToBeDeletedAccount in their respective Maps
        Map<String,Account> SurvivorAccountMap = new Map<String,Account>();
        Map<String,Account> ToBeDeletedAccountMap = new Map<String,Account>();
        
        for(Account a : [SELECT Id, SEAccountID__c, PRMUIMSSEAccountId__c, PRMUIMSID__c, RecordtypeId,
        PRMAccountRegistrationStatus__c, PRMStreet__c, PRMAdditionalAddress__c, PRMAreaOfFocus__c, PRMBusinessType__c,
        PRMCity__c, PRMCompanyName__c, PRMCompanyPhone__c, PRMWebsite__c, PRMCountry__c, PRMCurrencyIsoCode__c,
        PRMPLShowOnMap__c, PRMDomainsOfExpertise__c, PRMEmployeeSize__c, PRMExcludeFromReports__c, PRMCorporateHeadquarters__c,
        PRMIncludeCompanyInfoInSearchResults__c, PRMIsActiveAccount__c, PRMMarketServed__c,
        PRMOrigin__c, PRMPreferredDistributor1__c, PRMPreferredDistributor2__c, PRMPreferredDistributor3__c,
        //PRMPreferredDistributor4__c, PRMAccount__c, PRMReasonForDecline__c,PRMSEAccountNumber__c, PRMStateProvince__c,
        PRMPreferredDistributor4__c, PRMAccount__c, PRMReasonForDecline__c,PRMSEAccountNumber2__c, PRMStateProvince__c,
        PRMTnC__c, PRMTaxId__c, PRMAnnualSales__c, PRMZipCode__c, PLShowInPartnerLocator__c,
        PLCompanyName__c, PLCompanyDescription__c, PLStreet__c, PLAdditionalAddress__c, PLStateProvince__c,
        PLCountry__c, PLCity__c, PLZipCode__c, PLMainContactFirstName__c, PLMainContactLastName__c,
        PLMainContactPhone__c, PLPartnerLocatorFeatureGranted__c, PLAreaOfFocus__c, PLBusinessType__c,
        PLDomainsOfExpertise__c, PLMainContactEmail__c, PLFax__c, PLMarketServed__c, PLWebsite__c, PLDatapool__c
        FROM Account WHERE SEAccountID__c IN: elligibleAccMap.keySet() AND RecordtypeId =: businessAccRTId AND PRMUIMSID__c =: null]){
            //Recordtype = Business Account AND PRMUIMSId = null => Survivor Account
            SurvivorAccountMap.put(a.SEAccountID__c, a);
        }
        
        for(Account a : [SELECT Id, SEAccountId__c, PRMUIMSSEAccountId__c, PRMUIMSID__c, RecordtypeId,
        PRMAccountRegistrationStatus__c, PRMStreet__c, PRMAdditionalAddress__c, PRMAreaOfFocus__c, PRMBusinessType__c,
        PRMCity__c, PRMCompanyName__c, PRMCompanyPhone__c, PRMWebsite__c, PRMCountry__c, PRMCurrencyIsoCode__c,
        PRMPLShowOnMap__c, PRMDomainsOfExpertise__c, PRMEmployeeSize__c, PRMExcludeFromReports__c, PRMCorporateHeadquarters__c,
        PRMIncludeCompanyInfoInSearchResults__c, PRMIsActiveAccount__c, PRMMarketServed__c,
        PRMOrigin__c, PRMPreferredDistributor1__c, PRMPreferredDistributor2__c, PRMPreferredDistributor3__c,        
        //PRMPreferredDistributor4__c, PRMAccount__c, PRMReasonForDecline__c,PRMSEAccountNumber__c, PRMStateProvince__c,
        PRMPreferredDistributor4__c, PRMAccount__c, PRMReasonForDecline__c,PRMSEAccountNumber2__c, PRMStateProvince__c,
        PRMTnC__c, PRMTaxId__c, PRMAnnualSales__c, PRMZipCode__c
        FROM Account WHERE PRMUIMSSEAccountId__c IN: elligibleAccMap.keySet() AND RecordtypeId =: partnerAccRTId AND PRMUIMSID__c <> null]){
            //Recordtype = Partner Account AND PRMUIMSId != null => Tobedeleted Account
            ToBeDeletedAccountMap.put(a.PRMUIMSSEAccountId__c, a);
        }
        System.debug('##########################SurvivorAccountMap = ' + SurvivorAccountMap);
        System.debug('##########################ToBeDeletedAccountMap = ' + ToBeDeletedAccountMap);
        
        //Copy of the PRM fields values in the Survivor Account
        List<Account> listAccToBeUpdated = new List<Account>();
        
        //Merge the account & child records
        for(String s : SurvivorAccountMap.keySet()){
            PRM_Technical_Merge_Record__c p = new PRM_Technical_Merge_Record__c();
            if(elligibleAccMap.get(s) == 1){
                if(ToBeDeletedAccountMap.containsKey(s)){
                    try{
                        //API FIELO Merge Members
                        //FieloPRM_REST_MoveMembersByAccount.postMoveMembersByAccount(ToBeDeletedAccountMap.get(s).Id, SurvivorAccountMap.get(s).Id);
                        Account a = new Account();
                        a = copyPRMField(SurvivorAccountMap.get(s), ToBeDeletedAccountMap.get(s));
                        listAccToBeUpdated.add(a);
                        Merge SurvivorAccountMap.get(s) ToBeDeletedAccountMap.get(s);
                        p.Name = SurvivorAccountMap.get(s).SEAccountID__c;
                        p.PRMUIMS__c = SurvivorAccountMap.get(s).PRMUIMSID__c;
                        p.Deleted_Record_Id__c = ToBeDeletedAccountMap.get(s).Id;
                        p.Survivor_Id__c = SurvivorAccountMap.get(s).Id;
                        p.Object__c = 'Account';
                        p.Success__c = true;
                        listRecords.add(p);
                    }
                    catch (Exception e){
                        p.Name = SurvivorAccountMap.get(s).SEAccountID__c;
                        p.PRMUIMS__c = SurvivorAccountMap.get(s).PRMUIMSID__c;
                        p.Deleted_Record_Id__c = ToBeDeletedAccountMap.get(s).Id;
                        p.Survivor_Id__c = SurvivorAccountMap.get(s).Id;
                        p.Object__c = 'Account';
                        p.Error__c = true;
                        p.Error_Message__c = e.getMessage();
                        listRecords.add(p);
                    }
                }
            }
            else{
                p.Name = SurvivorAccountMap.get(s).SEAccountID__c;
                p.PRMUIMS__c = SurvivorAccountMap.get(s).PRMUIMSID__c;
                p.Deleted_Record_Id__c = ToBeDeletedAccountMap.get(s).Id;
                p.Survivor_Id__c = SurvivorAccountMap.get(s).Id;
                p.Object__c = 'Account';
                p.Error__c = true;
                p.Error_Message__c = 'n survivor accounts with the same SEAccountID__c. The merge has to be done manually';
                listRecords.add(p);
            }
        }
        update listAccToBeUpdated;
        endTime = datetime.now();
    }
    
    global void finish(Database.BatchableContext BC) {
        PRMTechnicalMergeHistory__c accountMergeHistory = new PRMTechnicalMergeHistory__c(Name = BC.getJobId(),
        Batch_Start_Time__c = startTime, Batch_End_Time__c = endTime);
        insert accountMergeHistory;
        
        for(PRM_Technical_Merge_Record__c r: listRecords){
            r.Batch_Id__c = accountMergeHistory.Id;
        }
        insert listRecords;
    }
    
    //Schedule method
    global void execute(SchedulableContext sc) {
        Batch_AccountMerge batchAcc = new Batch_AccountMerge();
        database.executeBatch(batchAcc);
    }
    
    global Account copyPRMField(Account Survivor, Account toBeDeleted){
        Survivor.PRMUIMSSEAccountId__c = toBeDeleted.PRMUIMSSEAccountId__c;        
        Survivor.PRMAccountRegistrationStatus__c = toBeDeleted.PRMAccountRegistrationStatus__c;
        Survivor.PRMStreet__c = toBeDeleted.PRMStreet__c;
        Survivor.PRMAdditionalAddress__c = toBeDeleted.PRMAdditionalAddress__c;
        Survivor.PRMAreaOfFocus__c = toBeDeleted.PRMAreaOfFocus__c;
        Survivor.PRMBusinessType__c = toBeDeleted.PRMBusinessType__c;
        Survivor.PRMCity__c = toBeDeleted.PRMCity__c;
        Survivor.PRMCompanyName__c = toBeDeleted.PRMCompanyName__c;
        Survivor.PRMCompanyPhone__c = toBeDeleted.PRMCompanyPhone__c;
        Survivor.PRMWebsite__c = toBeDeleted.PRMWebsite__c;
        Survivor.PRMCountry__c = toBeDeleted.PRMCountry__c;
        Survivor.PRMCurrencyIsoCode__c = toBeDeleted.PRMCurrencyIsoCode__c;
        Survivor.PRMPLShowOnMap__c = toBeDeleted.PRMPLShowOnMap__c;
        Survivor.PRMDomainsOfExpertise__c = toBeDeleted.PRMDomainsOfExpertise__c;
        Survivor.PRMEmployeeSize__c = toBeDeleted.PRMEmployeeSize__c;
        Survivor.PRMExcludeFromReports__c = toBeDeleted.PRMExcludeFromReports__c;
        Survivor.PRMCorporateHeadquarters__c = toBeDeleted.PRMCorporateHeadquarters__c;
        Survivor.PRMIncludeCompanyInfoInSearchResults__c = toBeDeleted.PRMIncludeCompanyInfoInSearchResults__c;
        Survivor.PRMIsActiveAccount__c = toBeDeleted.PRMIsActiveAccount__c;
        Survivor.PRMMarketServed__c = toBeDeleted.PRMMarketServed__c;
        Survivor.PRMOrigin__c = toBeDeleted.PRMOrigin__c;
        Survivor.PRMPreferredDistributor1__c = toBeDeleted.PRMPreferredDistributor1__c;
        Survivor.PRMPreferredDistributor2__c = toBeDeleted.PRMPreferredDistributor2__c;
        Survivor.PRMPreferredDistributor3__c = toBeDeleted.PRMPreferredDistributor3__c;
        Survivor.PRMPreferredDistributor4__c = toBeDeleted.PRMPreferredDistributor4__c;
        Survivor.PRMAccount__c = toBeDeleted.PRMAccount__c;
        Survivor.PRMReasonForDecline__c = toBeDeleted.PRMReasonForDecline__c;  
        //Survivor.PRMSEAccountNumber__c = toBeDeleted.PRMSEAccountNumber__c;
        Survivor.PRMSEAccountNumber2__c = toBeDeleted.PRMSEAccountNumber2__c;
        Survivor.PRMStateProvince__c = toBeDeleted.PRMStateProvince__c;
        Survivor.PRMTnC__c = toBeDeleted.PRMTnC__c;
        Survivor.PRMTaxId__c = toBeDeleted.PRMTaxId__c;
        Survivor.PRMAnnualSales__c = toBeDeleted.PRMAnnualSales__c;
        Survivor.PRMZipCode__c = toBeDeleted.PRMZipCode__c;
        Survivor.PRMUIMSID__c = toBeDeleted.PRMUIMSID__c;

        return Survivor;
    }
}