public class IdmsUimsReconcUpdateCompanyBulk {
    static IDMSCaptureError errorLog=new IDMSCaptureError();
    public static void updateUimsCompanies(List<IDMSCompanyReconciliationBatchHandler__c> recCompaniesToUpdate, String batchId){
        Set<string> idmsUids = new Set<String>();
        Integer attemptsMaxLmt = integer.valueof(label.CLNOV16IDMS022);
        for(IDMSCompanyReconciliationBatchHandler__c idmsUser : recCompaniesToUpdate){
            idmsUids.add(idmsUser.IdmsUser__c);
        }
        //retrieve list of idms users
        //ToDo: remove unncessary fields from SOQL once mapping is verified.
        List<User> idmsUsers = [Select Id, Email,MobilePhone,Phone,FirstName,LastName,IDMS_Email_opt_in__c,IDMS_User_Context__c,IDMSMiddleName__c,CompanyName, 
                                Country,IDMS_PreferredLanguage__c,IDMSCompanyGoldenId__c, Company_Country__c, DefaultCurrencyIsoCode, IDMSClassLevel2__c,
                                Street,City,PostalCode,State,IDMS_County__c,Job_Title__c,Company_City__c, Company_Postal_Code__c, IDMSCompanyPoBox__c, IDMSCompanyHeadquarters__c,
                                Company_State__c, Company_Address1__c, Company_Website__c, IDMSMarketSegment__c, IDMSMarketSubSegment__c, IDMSCompanyMarketServed__c, IDMSCompanyNbrEmployees__c, 
                                IDMSClassLevel1__c, IDMSTaxIdentificationNumber__c,IDMSCompanyFederationIdentifier__c,IDMSCompanyCounty__c,
                                IDMS_POBox__c,FederationIdentifier,IDMS_Registration_Source__c,IDMS_AdditionalAddress__c,IDMSMiddleNameECS__c,Job_Function__c,Company_Address2__c,
                                IDMSSalutation__c,IDMSSuffix__c,Fax,IDMSDelegatedIdp__c,IDMSIdentityType__c,IDMSJobDescription__c, IDMSPrimaryContact__c,
                                IDMS_VC_NEW__c, IDMS_VC_OLD__c from User where Id IN:idmsUids];
        System.debug('*****ReSend IDMS Users company info to Update*****'+idmsUsers);
        try{
            //stub class logic starts
            IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort bulkService = new IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort();
            string callerFid = LAbel.CLJUN16IDMS104;
            bulkService.endpoint_x = Label.CLNOV16IDMS020;
            bulkService.clientCertName_x = System.Label.CLJUN16IDMS103;
            
            //Request to update Accounts
            IdmsUsersResyncflowServiceIms.accountsUpdateRequest accountUpdateRequest = new  IdmsUsersResyncflowServiceIms.accountsUpdateRequest();
            accountUpdateRequest.accounts  = new IdmsUsersResyncflowServiceIms.createAccountRequestBean();
            accountUpdateRequest.accounts.accountForUpdate = new List<IdmsUsersResyncflowServiceIms.accountForUpdate>();
            
            //response from uims
            IdmsUsersResyncflowServiceIms.updateAccountsResponse response = new IdmsUsersResyncflowServiceIms.updateAccountsResponse();
            
            for(User usr: idmsUsers){
                IdmsUsersResyncflowServiceIms.accountForUpdate accountForUpdate= new IdmsUsersResyncflowServiceIms.accountForUpdate();
                accountForUpdate.federatedId = usr.IDMSCompanyFederationIdentifier__c;
                accountForUpdate.account = new IdmsUsersResyncflowServiceIms.accountBean();
                IdmsUsersResyncflowServiceIms.accountBean uimsAccountBean = IdmsUimsReconcCompanyMapping.mapIdmsCompanyUims(usr);
                accountForUpdate.account = uimsAccountBean;
                accountUpdateRequest.accounts.accountForUpdate.add(accountForUpdate);
            }
            System.debug('*****accountUpdateRequest*****'+accountUpdateRequest);
            //UIMS response. 
            response.return_x = bulkService.updateAccounts(callerFid,accountUpdateRequest);
            IdmsUsersResyncflowServiceIms.accountUpdateReturnBean[] responseValues = response.return_x.accounts.accountUpdatedRes;
            System.debug('*******UIMS response for update Company info****'+response.return_x);
            
            Set<String> uimsCompFedIds = new Set<String>();
            if(responseValues.size()> 0){
                for(IdmsUsersResyncflowServiceIms.accountUpdateReturnBean responseValue: responseValues){
                    uimsCompFedIds.add(responseValue.federatedId);
                }
                system.debug('response company fed ids from uims****'+uimsCompFedIds);
            }
            List<User> updateIdmsUsers = new List<User>();
            List<IDMSReconciliationBatchLog__c> createBatchLogs= new List<IDMSReconciliationBatchLog__c>();
            List<IDMSCompanyReconciliationBatchHandler__c> handlerRecordsToDel = new List<IDMSCompanyReconciliationBatchHandler__c>();
            List<IDMSCompanyReconciliationBatchHandler__c> handlerRecordsToUpdate = new List<IDMSCompanyReconciliationBatchHandler__c>();
            
            //corresponding user ids set
            set<String> idmsUserIds = new set<String>();
            Map<String, User> mapUimsCompFedIdIdmsUser = new Map<String, User>();
            List<User> idmsUserList = [Select Id, Email,username, FederationIdentifier, IDMSCompanyFederationIdentifier__c, IDMS_VC_NEW__c, IDMS_VC_OLD__c from User where ID IN: idmsUids AND IDMSCompanyFederationIdentifier__c IN:uimsCompFedIds];
            System.debug('*****List of users****'+idmsUserList);
            for(User usr: idmsUserList){
                mapUimsCompFedIdIdmsUser.put(usr.IDMSCompanyFederationIdentifier__c, usr);
                idmsUserIds.add(usr.Id);
                //ToDo: check if company fed id is null then create batch log and update handler record 
            }
            System.debug('*****Map of comapany fed id and user object****'+mapUimsCompFedIdIdmsUser);
            
            Map<String, IDMSCompanyReconciliationBatchHandler__c> mapUserIdHandler = new Map<String, IDMSCompanyReconciliationBatchHandler__c>();
            //corresponding User handler list
            List<IDMSCompanyReconciliationBatchHandler__c> userHandlerList = [select Id, IdmsUser__c, NbrAttempts__c, HttpMessage__c  from IDMSCompanyReconciliationBatchHandler__c where IdmsUser__c IN: idmsUserIds];
            for(IDMSCompanyReconciliationBatchHandler__c userHandler: userHandlerList){
                mapUserIdHandler.put(userHandler.IdmsUser__c, userHandler);
            }   
            System.debug('*****Map of idms User Id and Handler object****'+mapUserIdHandler);
            
            for(IdmsUsersResyncflowServiceIms.accountUpdateReturnBean responseValue: responseValues){
                User idmsUserUpdate = mapUimsCompFedIdIdmsUser.get(responseValue.federatedId);
                if(responseValue.returnCode == 0){
                    //successfully updated
                    if(idmsUserUpdate!=null){
                        idmsUserUpdate.IDMS_VC_OLD__c  = idmsUserUpdate.IDMS_VC_NEW__c;
                        idmsUserUpdate.IDMSCompanyFederationIdentifier__c = responseValue.federatedId;
                        updateIdmsUsers.add(idmsUserUpdate);
                    }
                    system.debug('***IDMS users before Update in the list***'+updateIdmsUsers);
                    
                    //add to batch log. 
                    IDMSReconciliationBatchLog__c idmsBatchLog = IdmsReconcBatchLogsHandler.batchLogsForUpdateCompany(idmsUserUpdate, responseValue, batchId);
                    createBatchLogs.add(idmsBatchLog);     
                    System.debug('Batch log created for success users:'+createBatchLogs);
                    
                    //Add handler record in the list to delete.
                    IDMSCompanyReconciliationBatchHandler__c handlerRecToDelete = mapUserIdHandler.get(idmsUserUpdate.Id);
                    handlerRecordsToDel.add(handlerRecToDelete);
                    system.debug('****handlerRecordsToDel*****:'+handlerRecordsToDel);
                }else {
                    system.debug('*** This users company info failed to update in UIMS****');
                    //Update User handler record.
                    IDMSCompanyReconciliationBatchHandler__c handlerRecToUpdate = mapUserIdHandler.get(idmsUserUpdate.Id);
                    if(handlerRecToUpdate.NbrAttempts__c < attemptsMaxLmt){
                        handlerRecToUpdate.NbrAttempts__c = handlerRecToUpdate.NbrAttempts__c + 1;
                    }
                    handlerRecToUpdate.HttpMessage__c = 'FederatedId:'+responseValue.federatedId+'\n'+'Has been updated:'+responseValue.hasBeenUpdated+'\n'+'Message:'+responseValue.message+'\n'+'Return Code:'+responseValue.returnCode;
                    handlerRecordsToUpdate.add(handlerRecToUpdate);
                    system.debug('*** User handler records to update ****'+handlerRecordsToUpdate);
                    
                    //add to batch log. 
                    IDMSReconciliationBatchLog__c idmsBatchFailedLog = IdmsReconcBatchLogsHandler.batchLogsForUpdateCompany(idmsUserUpdate, responseValue, batchId);
                    createBatchLogs.add(idmsBatchFailedLog);     
                    System.debug('Batch log created for failed users company info:'+createBatchLogs);
                }
            }
            Update updateIdmsUsers;
            Insert createBatchLogs;
            Delete handlerRecordsToDel;
            Update handlerRecordsToUpdate;
        }catch(Exception e){
            System.debug('Exception occurs:'+e.getStackTraceString());  
            //insert exception in Idms error logs. 
            errorLog.IDMScreateLog('IDMSReconciliationBatch','IdmsUimsReconcUpdateCompanyBulk','Bulk Company Update service in UIMS','',Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),batchId,Datetime.now(),'','',false,'',false,null);
        }
    }
}