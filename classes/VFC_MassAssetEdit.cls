public with sharing class VFC_MassAssetEdit {
    
    public List<SVMXC__Case_Line__c> clinesList {get;set;}
    public List<SVMXC__Installed_Product__c> ipList ;
    public List<InstalledProductWrapper> ipwList {get;set;}
    Set<id> ipidset = new Set<id>();
    Set<id> cldset = new Set<id>();
    Boolean isSuccess ;
    
    /*============================================================================
        C.O.N.S.T.R.U.C.T.O.R
    =============================================================================*/
    public VFC_MassAssetEdit(ApexPages.StandardSetController controller) {
        clinesList =controller.getSelected();
        isSuccess  = true;
        ipwList  = new List<InstalledProductWrapper>();
        
        for(SVMXC__Case_Line__c cl:clinesList){
            //ipidset.add(cl.SVMXC__Installed_Product__c);
            
            cldset.add(cl.id);
        }
        for(SVMXC__Case_Line__c cl:[select id,SVMXC__Installed_Product__c from  SVMXC__Case_Line__c where id in :cldset]){
            ipidset.add(cl.SVMXC__Installed_Product__c);
        }
        if(ipidset != null && ipidset.size()>0){
        
            
            
            //ipList = [Select id,Quantity__c,SVMXC__Product__c ,SVMXC__Product__r.Name,SVMXC__Site__c,Brand2__c,DeviceType2__c,LifeCycleStatusOfTheInstalledProduct__c,SchneiderCommercialReference__c,SVMXC__Serial_Lot_Number__c, Name, brand2__r.Name, deviceType2__r.name, range__c, Category__r.id, Category__r.name, SVMXC__Company__r.name,SVMXC__Site__r.name from SVMXC__Installed_Product__c where id in :ipidset];
            for(SVMXC__Installed_Product__c  ipobj:[Select id,Quantity__c,SVMXC__Product__c ,SVMXC__Product__r.Name,SVMXC__Site__c,Brand2__c,DeviceType2__c,LifeCycleStatusOfTheInstalledProduct__c,SchneiderCommercialReference__c,SVMXC__Serial_Lot_Number__c, Name, brand2__r.Name, deviceType2__r.name, range__c, Category__r.id, Category__r.name, SVMXC__Company__r.name,SVMXC__Site__r.name from SVMXC__Installed_Product__c where id in :ipidset]){
                InstalledProductWrapper ipw = new InstalledProductWrapper();
                ipw.ipObject = ipobj;
                ipw.Message = '';
                ipwList.add(ipw );
                
            }
        }
        
    }
     
    public void doSave(){
    
        isSuccess = true;
        ipList = new List<SVMXC__Installed_Product__c>();
        
        for(InstalledProductWrapper  ipw :ipwList ){
            ipList.add(ipw.ipObject);
        }
        if(ipList != null && ipList.size()>0){
        
           Database.SaveResult[] results  = Database.update(ipList, false);
           system.debug('results'+results);
            for (Integer i=0; i<results.size(); i++) {
                 Database.SaveResult saveResult = results[i];
                 system.debug('saveResult'+saveResult);
                  if (!saveResult.isSuccess()) {

                      Database.Error err = saveResult.getErrors()[0];
                      ipwList[i].Message =err.getMessage();
                      isSuccess = false;
                      system.debug('isSuccess'+isSuccess+'---'+err);
                  }
            }
        
        }
        
        if(isSuccess)
         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Saved Successfully'));

       
    }
    public PageReference doRedirect(){
    
         if(isSuccess )
        {
            
           PageReference  pref= new PageReference(ApexPages.currentPage().getParameters().get('retURL')); 
           pref.setRedirect(true);
           return pref;
        }
        else{
            return null;
        }
    
    
    }
    public PageReference doCancel(){
             PageReference  pref= new PageReference(ApexPages.currentPage().getParameters().get('retURL')); 
           pref.setRedirect(true);
           return pref;
    }
    public PageReference doSaveAndDone(){
        doSave();
        PageReference  pref = doRedirect();
        return pref;
    }
    
    public class InstalledProductWrapper{
    
        public SVMXC__Installed_Product__c  ipObject{get;set;} 
        public String Message{get;set;}
    }
    
    

}