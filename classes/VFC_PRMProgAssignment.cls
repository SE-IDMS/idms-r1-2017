public with sharing class VFC_PRMProgAssignment extends VFC_ExternalInfoDashboardController
{
   public list <ProgramLevel__c> prog {get;set;}  
   public string searchString {get;set;}  
   public string accCountry;
   public VFC_PRMProgAssignment () {  
        
       accCountry=ApexPages.CurrentPage().getParameters().get('accCountry');
     //  system.debug('****searchQuery****'+accCountry);
      // string searchQuery='select  ID,name,PartnerProgram__r.TECH_CountriesId__c,PartnerProgram__r.ID,PartnerProgram__r.name,PartnerProgram__r.country__c from ProgramLevel__c where   partnerProgram__r.ProgramStatus__c= \'Active\' and LevelStatus__c = \'Active\' and ( partnerProgram__r.recordtypeid=\'012A0000000nrKB\') and PartnerProgram__r.country__c= :accCountry';  
        // and //partnerProgram__r.recordtypeid= \'012A0000000nrKBIAY\' OR
        //partnerProgram__r.recordtypeid=\'012A0000000npYm\'
      //   system.debug('****searchQuery'+searchQuery);
         
     // prog = Database.query(searchQuery); 
       /* SET countries
        for (sObject progObj: prog1){
            IF( prog1.PartnerProgram__r.Country__c = accCountry AND PartnerProgram__r.TECH_CountriesId__c.indexOf(accCountry) ) {
             prog.add(progObj);
            }
        } */
   }  
   public void Search(){  
     string searchQuery='select  ID,name,PartnerProgram__r.ID,PartnerProgram__r.name,PartnerProgram__r.country__c from ProgramLevel__c where LevelStatus__c = \'Active\' AND PartnerProgram__r.name like \'%'+searchString+'%\' and partnerProgram__r.ProgramStatus__c= \'Active\' and ( partnerProgram__r.recordtypeid=\'012A0000000npYmIAI\' or partnerProgram__r.recordtypeid=\'012A0000000nrKBIAY\' ) and PartnerProgram__r.country__c= :accCountry' ;  
     //and PartnerProgram__r.country__c= :accCountry'
     system.debug('****searchQuery'+searchQuery);
     prog = Database.query(searchQuery); 
     if (prog.size()==0){
      SaveStatus = 'valErrors1';
      ValidationMessages.add( 'No Programs for corresponding country.');
      }
    //partnerProgram__r.recordtypeid= \'012A0000000npYm\' OR      
   }  
   
   public void SaveProgram(){
       ValidationMessages.clear();
      try{
           ID accountID = (ID)ApexPages.CurrentPage().getParameters().get('accountID');
           ID programID = (ID)ApexPages.CurrentPage().getParameters().get('programID');
           ID programLevelID = (ID)ApexPages.CurrentPage().getParameters().get('programLevelID');
          // String programName =(String)ApexPages.CurrentPage().getParameters().get('programName');
           ACC_PartnerProgram__c accountProgram  =new ACC_PartnerProgram__c();
           accountProgram.account__c=accountID ;
           accountProgram.PartnerProgram__c=programID ;
           accountProgram.ProgramLevel__c=programLevelID ;
          
          // accountProgram.PartnerProgramName__c=
           insert accountProgram;   
            SaveStatus = 'success';
            ValidationMessages.add( 'Program assigned successfully.');
      }
        
      catch(Exception ex){
            SaveStatus = 'valErrors';
            ValidationMessages.add( 'Error while assigning Program :'+ ex.getMessage());
      }      
    } 
   
     }