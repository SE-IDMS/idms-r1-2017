/********************************************************************
* Company: Fielo
* Created Date: 09/07/2016
* Description: 
********************************************************************/
public with sharing class FieloPRM_UTILS_SegmentHybrid{
    
    public static map<id, set<id>> segmentByMember(list<FieloEE__Member__c> listMembers, List<FieloEE__RedemptionRule__c> segments){
        
        if(listMembers == null || segments == null){return null;}
        
        map<id, set<id>> mapMemberSegments = new map<id, set<id>>();
        
        for(FieloEE__Member__c member: listMembers){

            mapMemberSegments.put(member.Id, new set<Id>());
            
            for(FieloEE__RedemptionRule__c segment: segments){

                if(segment.FieloEE__Redemption_Criterias__r.size() > 0 && FieloEE.OrganizationUtil.applyCriterias(segment.FieloEE__Redemption_Criterias__r, member, null)){
                    mapMemberSegments.get(member.Id).add(segment.Id);
                }
            }
        }
        return mapMemberSegments;
    }
    
}