/*
    Author          : Ramakrishna Singara (Schneider Electric) 
    Date Created    : 07/08/2012
    Description     : Utils_TestMethods_DMT Test Class For September Release
                        
*/
@isTest
private class Utils_TestMethods_DMT_TEST
{
    static testMethod void Utils_TestMethods_DMT() 
    {
        test.startTest();
    
        User usr = Utils_TestMethods.createStandardDMTUser('Test');
        insert usr;
        
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(usr.id,'Created','Buildings','Global');
        insert testDMTA1; 
        
        PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
        testProj.WorkType__c = 'Master Project';
        testProj.BusinessTechnicalDomain__c = 'Supply Chain';
        testProj.Parent__c = 'Buildings';
        testProj.ParentFamily__c = 'Business';
        testProj.BusGeographicZoneIPO__c = 'Global';
        testProj.ExpectedStartDate__c = System.today();
        testProj.GoLiveTargetDate__c =System.today().addDays(2);
        insert testProj;
        
        //DMTFundingEntity__c dmtFund = Utils_TestMethods_DMT.createDMTFundingEntity(testProj, '2013');
        //insert dmtFund;
        DMTFundingEntity__c dmtFund1 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj, '2013',true,'Busines','Buildings','Global','','Q1','2011');
        insert dmtFund1;
        DMTFundingEntity__c dmtFund2 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj, '2013',true,'Business','Building','Global','');
        insert dmtFund2;
        DMTFundingEntity__c dmtFund3 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj, '2012',true,false,'Test');
        insert dmtFund3;
       
      
        test.stopTest();

        
    }
}