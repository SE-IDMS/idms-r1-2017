/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: Utility Class                                       *
* Created Date: 13/08/2014                                  *
************************************************************/
public with sharing class Fielo_Utilities {
    
    /*Este metodo recibe el DeveloperName de un menu y retorna un PageReference al Menu*/
    public static PageReference getMenuReference(String menuName){
        
        PageReference pageRet = new PageReference('/apex/'+FieloEE.ProgramUtil.getProgramByDomain().FieloEE__CustomPage__c);
        
        List<FieloEE__Menu__c> menuTransaction = [SELECT Id, FieloEE__CustomURL__c FROM FieloEE__Menu__c WHERE F_DeveloperName__c =: menuName LIMIT 1];
        if(!menuTransaction.isEmpty()){
            if(menuTransaction[0].FieloEE__CustomURL__c != null){
                pageRet = new PageReference('/apex/'+menuTransaction[0].FieloEE__CustomURL__c);
            }
            pageRet.getParameters().put('idMenu', menuTransaction[0].Id);
        }
        pageRet.setRedirect(true);
        return pageRet;
    }
    
    /*Este metodo recibe el DeveloperName de un menu y retorna un PageReference al Menu*/
    public static PageReference getMenuReference2(String menuName){
        
        PageReference pageRet = new PageReference('/apex/'+FieloEE.ProgramUtil.getProgramByDomain().FieloEE__CustomPage__c);
        
        List<FieloEE__Menu__c> menuTransaction = [SELECT Id, FieloEE__CustomURL__c FROM FieloEE__Menu__c WHERE Name =: menuName LIMIT 1];
        if(!menuTransaction.isEmpty()){
            if(menuTransaction[0].FieloEE__CustomURL__c != null){
                pageRet = new PageReference('/apex/'+menuTransaction[0].FieloEE__CustomURL__c);
            }
            pageRet.getParameters().put('idMenu', menuTransaction[0].Id);
        }
        pageRet.setRedirect(true);
        return pageRet;
    }
    
    /*Este metodo recibe el Id de un menu y retorna un PageReference al Menu*/
    public static PageReference getMenuReferenceById(Id menuId){
        
        PageReference pageRet = new PageReference('/apex/'+FieloEE.ProgramUtil.getProgramByDomain().FieloEE__CustomPage__c);
        
        List<FieloEE__Menu__c> menuTransaction = [SELECT Id, FieloEE__CustomURL__c FROM FieloEE__Menu__c WHERE Id =: menuId LIMIT 1];
        if(!menuTransaction.isEmpty()){
            if(menuTransaction[0].FieloEE__CustomURL__c != null){
                pageRet = new PageReference('/apex/'+menuTransaction[0].FieloEE__CustomURL__c);
            }
            pageRet.getParameters().put('idMenu', menuTransaction[0].Id);
        }
        pageRet.setRedirect(true);
        return pageRet;
    }
    
}