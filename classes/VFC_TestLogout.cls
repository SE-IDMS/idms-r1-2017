Public class VFC_TestLogout{

    public Cookie apex_LoginURLCookie; 
    public String AppId;

    public VFC_TestLogout(){
        system.debug('in Logout controller');
        
        apex_LoginURLCookie = ApexPages.currentPage().getCookies().get('apex__LoginURLCookie');
        system.debug('all cookies: ' + ApexPages.currentPage().getCookies());
        system.debug('apex_LoginURLCookie : '+ apex_LoginURLCookie);
        
        if (apex_LoginURLCookie != null) {
            system.debug('get AppId from cookie');
        }
    }

}