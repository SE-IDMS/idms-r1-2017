/**

 */
@isTest
private class VFC_AccountRecordTypeSelect_Test {

    static testMethod void myUnitTest() {
        
        PageReference vfPage = Page.VFP_AccountSearchByPartner;
        Test.setCurrentPage(vfPage);
        
        VFC_AccountRecordTypeSelect vfc = new VFC_AccountRecordTypeSelect();
        vfc.redirect();
        
        List<Profile> profiles = [select id from profile where name='SE - Customer Care Support'];
        if(profiles.size()>0)
        {
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');        
            system.runas(user)
            {
                VFC_AccountRecordTypeSelect vfc2 = new VFC_AccountRecordTypeSelect();
                vfc2.redirect();
            }
        }    
        
    }//End of test
}//Endof class