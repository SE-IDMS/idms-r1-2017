@isTest
private class VFC77_TerritorySearch_TEST{


    static testMethod void VFC77_TerritorySearchTest() {
    //Start of Test Data preparation
    
    // Profile profile = [select id from profile where name='SE - Sales Manager' LIMIT 1];
    // User user = Utils_TestMethods.createStandardUser(profile.id, 'TrS1');
    // database.insert(user);

        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        User user;
        System.runAs(sysadmin2){
            UserandPermissionSets useruserandpermissionsetrecord=new UserandPermissionSets('Users','SE - Sales Manager');
           user = useruserandpermissionsetrecord.userrecord;
            Database.insert(user);

            List<PermissionSetAssignment> permissionsetassignmentlstforuser=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:useruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforuser.add(new PermissionSetAssignment(AssigneeId=user.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforuser.size()>0)
            Database.insert(permissionsetassignmentlstforuser);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }

    
    Profile profile1 = [select id from profile where name='System Administrator' LIMIT 1];    
    User user2 = Utils_TestMethods.createStandardUser(profile1.id,'TrS2');
    database.insert(user2);
        
    
    
    
    Country__c country1 = Utils_TestMethods.createCountry();
    insert country1;
    system.debug('####################################### country1 : '+country1);
    
    StateProvince__c StateProvince1 = Utils_TestMethods.createStateProvince(country1.id);
    insert StateProvince1;
    system.debug('####################################### StateProvince1 : '+StateProvince1);
    
    SE_Territory__c territory1 =  Utils_TestMethods.createTerritory(country1.id, StateProvince1.id);
    insert territory1 ;
    system.debug('####################################### territory1 : '+territory1 );
        
    Account account1 = Utils_TestMethods.createAccount(user2.Id, country1.id);
    insert account1;
    system.debug('####################################### account1 : '+account1);
    
    Account account2 = Utils_TestMethods.createAccount(user.Id, country1.id);
    insert account2;
    
    
    accountTeamMember accountTeam1 = new accountTeamMember(AccountID=account2.id,userId=user.id);
    database.insert(accountTeam1);
    
    
    List<String> keywords = New List<String>();
    keywords.add('Test');
    List<String> filters = New List<String>();
    filters.add('BD');
    filters.add(StateProvince1.id);
    filters.add(country1.id);
    
    Utils_DataSource dataSource = Utils_Factory.getDataSource('TERR');
    Utils_DataSource.Result searchResult = dataSource.Search(keywords,filters);
    sObject tempsObject = searchResult.recordList[0];
    
    system.debug('####################################### '+tempsObject );
    //End of Test Data preparation.
    
    Test.startTest();
    system.runAs(user2)
    {
    ApexPages.StandardController acc1 = new ApexPages.StandardController(account1);
    PageReference pageRef= Page.VFP77_TerritorySearch;
    pageRef.getParameters().put('id', account1.id);
    Test.setCurrentPage(pageRef);
    
    VFC_ControllerBase basecontroller = new VFC_ControllerBase();        
    system.debug('####################################### '+pageRef);
    System.debug('~~~~~~~~~~Start of Test 1~~~~~~~~~~');
    VFC77_TerritorySearch terrSearch = new VFC77_TerritorySearch(acc1);
    VCC06_DisplaySearchResults componentcontroller = terrSearch.resultsController; 
    
    terrSearch .searchText='test';
    terrSearch.level1 = 'BD';
    terrSearch.level2 = StateProvince1.id;
    terrSearch.checkAccess();
    terrSearch.search();
    terrSearch.clear();
    terrSearch.cancel();
             
    terrSearch.PerformAction(tempsObject, terrSearch);
    terrSearch.searchText='';
    terrSearch.level1 = Label.CL00355;
    terrSearch.level2 = Label.CL00355;
    terrSearch.search();
         
    }
    AccountShare aShare = new AccountShare();
    aShare.accountId= account1.Id;
    aShare.UserOrGroupId = user.Id;
    aShare.accountAccessLevel = 'edit';
    aShare.OpportunityAccessLevel = 'read';
    aShare.CaseAccessLevel = 'edit';
    insert aShare;  
    system.runAs(user)
    {
    ApexPages.StandardController acc2 = new ApexPages.StandardController(account1);
    PageReference pageRef= Page.VFP77_TerritorySearch;
    pageRef.getParameters().put('id', account1.id);
    Test.setCurrentPage(pageRef);
    VFC_ControllerBase basecontroller = new VFC_ControllerBase();        
    
    System.debug('~~~~~~~~~~Start of Test 2~~~~~~~~~~');
    VFC77_TerritorySearch terrSearch = new VFC77_TerritorySearch(acc2);
    VCC06_DisplaySearchResults componentcontroller = terrSearch.resultsController;
    terrSearch.checkAccess();
    }
    
    system.runAs(user)
    {
    ApexPages.StandardController acc3 = new ApexPages.StandardController(account2);
    PageReference pageRef= Page.VFP77_TerritorySearch;
    pageRef.getParameters().put('id', account2.id);
    Test.setCurrentPage(pageRef);
    VFC_ControllerBase basecontroller = new VFC_ControllerBase();        
    
    System.debug('~~~~~~~~~~Start of Test 3~~~~~~~~~~');
    VFC77_TerritorySearch terrSearch = new VFC77_TerritorySearch(acc3);
    VCC06_DisplaySearchResults componentcontroller = terrSearch.resultsController;
    terrSearch.checkAccess();
    }
    
    }
    
    
}