global class AssessmentQuestionAnswer
{
	 // bFO ID of Assessment
    WebService string ID { get; set; }
    
    // Name of the Assessment
    WebService string QUESTIONID { get; set; }
    
    // Description of the Assessment
    WebService string ASSESSMENTID { get; set; }

    // Maximum Score of the Assessment
    WebService string ANSWER { get; set; }

    // Program of the Assessment
    WebService string SEQUENCE { get; set; }

        
    // LastUpdatetdDate timestamp in bFO
    WebService Datetime SDH_VERSION { get; set; }
    
    public AssessmentQuestionAnswer() {
       
    }
}