@isTest
public class VFC_PartnerLeadAcceptReject_Test
{

	static testMethod void test1() 
	{


		 Account Acc = Utils_TestMethods.createAccount();
         insert Acc;
         
         Opportunity opp= Utils_TestMethods.createOpenOpportunity(Acc.Id);
         insert opp;
         
         Country__c country= Utils_TestMethods.createCountry();
         insert country; 

        Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();       
        Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName();
        MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
        insert markCallBack;
            
        Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
        insert con1;
        List<Lead> leads = new list<lead>();
         for(Integer i=0;i<5;i++)
         {
            Lead lead1 = Utils_TestMethods.createLead(opp.Id, country.Id,100);
            //lead1.RecordTypeId = mapRecordType.get('Non Marcom Lead').getRecordTypeId();
            //lead1.Tech_NonMarcomCampaignId__c = '0ftjunkdata78ms';
            lead1.Contact__C = con1.id;
            lead1.Account__C = Acc.id;
            lead1.Tech_LeadTransferredBy__c = UserInfo.getUserId();
            leads.add(lead1);   
         }
        Database.SaveResult[] leadsInsert= Database.insert(leads, true);
        List<Id> leadsId = new List<Id>();
        for(Database.SaveResult sr: leadsInsert)
        {
            if(!sr.isSuccess())
            {
                Database.Error err = sr.getErrors()[0];
                System.Debug('######## AP19_Lead_TEST Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
            } 
            else
               leadsId.add(sr.Id);
        }
            
            
        leads.clear();
        leads = [select Status, subStatus__c from lead where Id in: leadsId];
        System.Debug('The status and substatus of the lead is:'+leads);
        
        Lead lead1 = [select Status, subStatus__c,FirstName,CallBackLimit__c,Country__c, Priority__c,TECH_Country__c,TECH_OpportunityOwner__c from lead where Id in: leadsId limit 1];
      	

      	Recordtype partneropprecordType = [Select id from RecordType where developername =  'PartnerLeadTask' limit 1];
         Task task1 = new Task(
                OwnerId = UserInfo.getUserId(),
                Subject = 'Lead ' + lead1.FirstName + ' ' + System.Label.CLOCT13PRM08,
                Description = System.Label.CLOCT13PRM09,
                RecordTypeId = System.Label.CLAPR14PRM21,
                TECH_LeadId__c = lead1.Id);
        //Task task1 = Utils_TestMethods.createPartnerOpportunityTask(opp, usr.id);
        task1.recordtypeid = partneropprecordType.id;
        //task1.OwnerId = usr.id;
        insert task1;

        PartnerOpportunityStatusHistory__c popp = new PartnerOpportunityStatusHistory__c();
        popp.Lead__c = task1.TECH_LeadId__c;
        popp.user__c = task1.OwnerId;
        popp.Status__c = 'Assigned';
        insert popp;

       // Lead1.ownerid = ;

         
       PageReference pg = Page.VFP_PartnerLeadAcceptReject;
       Test.setCurrentPage(pg);

		ApexPages.currentPage().getParameters().put('taskid',task1.Id);
		ApexPages.currentPage().getParameters().put('type','Reject');
		
    	ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(task1);
    	VFC_PartnerLeadAcceptReject vfcon = new VFC_PartnerLeadAcceptReject(sdtcon1);
    	vfcon.updateTask() ;
    	vfcon.partnerTask.RejectReason__c = 'Bad Contact Data';
    	vfcon.updateTask() ;
    	vfcon.isApproved();
    	ApexPages.currentPage().getParameters().put('type','Accept');
    	vfcon.updateTask() ;

    	vfcon.getreqType();
    	vfcon.doCancel();
    	vfcon.isApproved(); 
    	
    }    
}