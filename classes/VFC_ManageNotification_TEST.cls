@isTest
private class VFC_ManageNotification_TEST {

    static testMethod void myUnitTest1() {
        Test.startTest();
        Country__c Country = new Country__c (Name='TCY', CountryCode__c='TCY'); 
        insert country;
            
        PRMCountry__c prmCountry = new PRMCountry__c(CountryPortalEnabled__c = true,
                                          CountrySupportEmail__c = 'abc@xyz.com',
                                          PLDatapool__c = 'rn_US',
                                          Country__c = country.id
                                          );
        insert prmCountry;
        
        CS_PRM_ApexJobSettings__c CS_ApexJob = new CS_PRM_ApexJobSettings__c(Name = 'BatchProcessNotifications', InitialSync__c = true, LastRun__c = System.Now());
        insert CS_ApexJob;
        
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
        insert cLChild;
        
        
        CountryChannels__c thisNewCountryChannel = new CountryChannels__c();
        thisNewCountryChannel.Channel__c = cLC.Id;
        thisNewCountryChannel.SubChannel__c = cLChild.id;
        thisNewCountryChannel.Country__c = country.Id;
        thisNewCountryChannel.Active__c = True;
        thisNewCountryChannel.PRMCountry__c = prmCountry.Id;
        insert thisNewCountryChannel;
        
        SetupAlerts__c alert1 = new SetupAlerts__c(Name = 'Test', Content__c = 'This is Test', Display__c = 'SOO',NotificationType__c = 'ACC_REG_APPRVD', PRMCountryClusterSetup__c = prmCountry.Id, Status__c = 'Draft' );
        insert alert1;
        
        ChannelSystemAlert__c chaSys = new ChannelSystemAlert__c(CountryChannels__c = thisNewCountryChannel.Id, SystemAlert__c = alert1.Id);
        insert chaSys;
        
        FieloEE.MockUpFactory.setCustomProperties(false);
        Id recordtypeManual = [SELECT Id FROM RecordType WHERE DeveloperName = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'].Id;
        
        FieloEE__RedemptionRule__c fieRedRule = new FieloEE__RedemptionRule__c(Recordtypeid = recordtypeManual, F_PRM_SegmentFilter__c = 'P-TCY897', F_PRM_Country__c = prmCountry.Id, FieloEE__isActive__c = true);
        Insert fieRedRule;
        
        SegmentSystemAlert__c SegSysAlert = new SegmentSystemAlert__c(FieloPRM_Segment__c = fieRedRule.Id, SystemAlert__c = alert1.Id);
        insert SegSysAlert;
        
        PageReference pageRef = Page.VFP_ManageNotification;
        Test.setCurrentPage(pageRef);
        Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(alert1);
        ApexPages.currentPage().getParameters().put('prmcountryid',prmCountry.id);
         
        VFC_ManageNotification newAlert = new VFC_ManageNotification(thisController);
        newAlert.saveParam = 'Published';
        List<RecordType> lstRecType = [Select id,DeveloperName from RecordType where DeveloperName = 'Notification' OR DeveloperName = 'UserNotification'];
        if(lstRecType.size() > 0){
            for(RecordType recType:lstRecType){
                if(recType.DeveloperName == 'Notification')
                    newAlert.sysAlert.Recordtypeid = recType.Id;
            }
        }
        
        newAlert.doReset();
        newAlert.doCancel();
        newAlert.redirectEditPage();
        newAlert.doSave();
        newAlert.doRevokeAlert();
        Test.stopTest();
    }
    static testMethod void myUnitTest2() {
        Test.startTest();
        Country__c Country = new Country__c (Name='TCY', CountryCode__c='TCY'); 
        insert country;
            
        PRMCountry__c prmCountry = new PRMCountry__c(CountryPortalEnabled__c = true,
                                          CountrySupportEmail__c = 'abc@xyz.com',
                                          PLDatapool__c = 'rn_US',
                                          Country__c = country.id
                                          );
        insert prmCountry;
        
        CS_PRM_ApexJobSettings__c CS_ApexJob = new CS_PRM_ApexJobSettings__c(Name = 'BatchProcessNotifications', InitialSync__c = true, LastRun__c = System.Now());
        insert CS_ApexJob;
        
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
        insert cLChild;
        
        
        CountryChannels__c thisNewCountryChannel = new CountryChannels__c();
        thisNewCountryChannel.Channel__c = cLC.Id;
        thisNewCountryChannel.SubChannel__c = cLChild.id;
        thisNewCountryChannel.Country__c = country.Id;
        thisNewCountryChannel.Active__c = True;
        thisNewCountryChannel.PRMCountry__c = prmCountry.Id;
        insert thisNewCountryChannel;
        
        FieloEE.MockUpFactory.setCustomProperties(false);
        Id recordtypeManual = [SELECT Id FROM RecordType WHERE DeveloperName = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'].Id;
        
        FieloEE__RedemptionRule__c fieRedRule = new FieloEE__RedemptionRule__c(Recordtypeid = recordtypeManual, F_PRM_SegmentFilter__c = 'P-TCY897', F_PRM_Country__c = prmCountry.Id, FieloEE__isActive__c = true );
        Insert fieRedRule;
        
        /*SetupAlerts__c alert1 = new SetupAlerts__c(Name = 'Test', Content__c = 'This is Test', Display__c = 'SOO',NotificationType__c = 'ACC_REG_APPRVD', PRMCountryClusterSetup__c = prmCountry.Id, Status__c = 'Draft' );
        insert alert1;
        
        ChannelSystemAlert__c chaSys = new ChannelSystemAlert__c(CountryChannels__c = thisNewCountryChannel.Id, SystemAlert__c = alert1.Id);
        insert chaSys;
        
        SegmentSystemAlert__c SegSysAlert = new SegmentSystemAlert__c(FieloPRM_Segment__c = fieRedRule.Id, SystemAlert__c = alert1.Id);
        insert SegSysAlert;
        */
        SetupAlerts__c alert1 = new SetupAlerts__c();
        PageReference pageRef = Page.VFP_ManageNotification;
        Test.setCurrentPage(pageRef);
        Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(alert1);
        ApexPages.currentPage().getParameters().put('prmcountryid',prmCountry.id);
         
        VFC_ManageNotification newAlert = new VFC_ManageNotification(thisController);
        newAlert.saveParam = 'Draft';
        List<RecordType> lstRecType = [Select id,DeveloperName from RecordType where DeveloperName = 'Notification' OR DeveloperName = 'UserNotification'];
        if(lstRecType.size() > 0){
            for(RecordType recType:lstRecType){
                if(recType.DeveloperName == 'Notification')
                    newAlert.sysAlert.Recordtypeid = recType.Id;
            }
        }
        //newAlert.sysAlert.
        newAlert.doReset();
        newAlert.doRevokeAlert();
        newAlert.doCancel();
        newAlert.redirectEditPage();
        newAlert.doSave();

        Test.stopTest();
    }
}