/*
    Author        : Accenture IDC Team 
    Date Created  : 18/07/2011
    Description   : Class utilised by triggers CaseBeforeInsert and CaseBeforeUpdate.
*/

public class AP20_UpdateCaseCountry
{
    // Update Cases created by Web 2 Case by linking to suppliedCountry
    public static Map<Id,Account> caseAccountMap = new Map<Id, Account>();
    public static void webCaseCountryUpdate(List<Case> webCasesToUpdate)
    {          
        Map<String,Id> countryMap = new Map<String,Id>();
        List<String> countries = New List<String>();
        
        for(Case caseswithWebCountry : webCasesToUpdate)
            countries.add(caseswithWebCountry.SuppliedCountry__c);
                
        for(Country__c countryObj: [SELECT Id,Name,CountryCode__c FROM Country__c where (Name IN: countries OR CountryCode__c IN: countries  )])
        {
            countryMap.put(countryObj.name,countryObj.Id);
            countryMap.put(countryObj.CountryCode__c ,countryObj.Id);
        }
        
        for(Case caseObjWeb : webCasesToUpdate)
        {  
            // SuppliedCountry__c is meant to be the name of the country. 
            // Case is related to the matching Country object.
            caseObjWeb.CCCountry__c = countryMap.get(caseObjWeb.SuppliedCountry__c);
        }
    }

    // Update Cases created by CC agent using Country of the user when known or Account / Contact country.
    public static void caseCountryUpdate(List<Case> casesToUpdate){
        // Set of IDs for all contacts related to cases
        Set <String> ContactsID = new Set <String>(); // Related Contacts
        Set <Id>    setAccountId = new Set<Id>(); //Related Account
        Set<Case>    casesForUserCountryUpdate = new Set<Case>();
        Map<Id, Contact> mapContact = new Map<Id, Contact>();
        Map<Id, Account> mapAccount = new Map<Id, Account>();
        
        for(Case cases: casesToUpdate){
            if(cases.ContactId!=null){
                ContactsID.add(cases.ContactId);
            }
            else if(cases.AccountId!=null){
                setAccountId.add(cases.AccountId);
            }
        }
        
        if(ContactsID.size()>0){
            // Query the list of contacts for the set of IDs of Contact
            mapContact = new Map<Id, Contact>([SELECT ID, Country__c, Account.Country__c FROM Contact WHERE id IN:ContactsID]);
        }
        if(setAccountId.size()>0){
            // Query the list of Account for the set of IDs of Account
            mapAccount = new Map<Id, Account>([SELECT ID, Country__c FROM Account WHERE id IN:setAccountId]);
        }
        for(Case objCase :casesToUpdate){ 
            // If the contact is related to the case
            if(objCase.ContactId!=null){
                if(mapContact.containsKey(objCase.ContactId)){
                    if(mapContact.get(objCase.ContactId).Country__c!=null){
                        objCase.CCCountry__c = mapContact.get(objCase.ContactId).Country__c;
                    }
                    else if(mapContact.get(objCase.ContactId).Account.Country__c!=null){
                        objCase.CCCountry__c = mapContact.get(objCase.ContactId).Account.Country__c;
                    }
                    else{
                        casesForUserCountryUpdate.add(objCase);
                    }
                }
                else{
                    casesForUserCountryUpdate.add(objCase);
                }
            }
            else if(objCase.AccountId!=null){
                if(mapAccount.containsKey(objCase.AccountId)){
                    if(mapAccount.get(objCase.AccountId).Country__c!=null){
                        objCase.CCCountry__c = mapAccount.get(objCase.AccountId).Country__c;
                    }
                    else{
                        casesForUserCountryUpdate.add(objCase);
                    }
                }
                else{
                    casesForUserCountryUpdate.add(objCase);
                }
            }
            else{
                casesForUserCountryUpdate.add(objCase);
            }
        }
        
        if(casesForUserCountryUpdate.size()>0){
            // Returns the Country Code for Country of the current user
            String strUserCountry = [SELECT Country__c from User WHERE id =:UserInfo.getUserId()].Country__c ;
        
            // If the Logged In user has a related country
            if(strUserCountry != null ){
                Id countryId;
                List<Country__c> lstUserCountry = [SELECT id FROM Country__c WHERE CountryCode__c =:strUserCountry OR CountryCode__c =:strUserCountry LIMIT 1];
                
                if(lstUserCountry.size()==1){
                    countryId = lstUserCountry[0].ID;
                    for(Case objCasesForUserCountry : casesForUserCountryUpdate){
                            objCasesForUserCountry.CCCountry__c = countryId;
                    }               
                }
            }
        }   
    }
 
    
    
     
         
        // Method to switch the Origin to "internal" if the related Account is an internal one
        public static void updateInternalCases(List<Case> aCaseList){
            if(aCaseList != null && aCaseList.size() > 0){
                // Map Contacts to their related IDs in the case
                Map<ID,Contact> parentContactsMap = new Map<ID,Contact>();
                Set<ID> parentContactsIDSet = new Set<ID>();
                
                for(Case aCase:aCaseList){
                    parentContactsIDSet .add(aCase.ContactID);
                }
                           
                parentContactsMap = new Map<Id, Contact>([SELECT ID, AccountID, Account.ClassLevel1__c FROM Contact WHERE ID IN :parentContactsIDSet]);
                
                
                // For every case, check the related account's classification level 1
                // If Classification Level 1 is equal to "Internal" then set the Origin of the case to "Internal"
                for(Case aCase:aCaseList){
                    if(parentContactsMap.containsKey(aCase.ContactID)){             
                        if(parentContactsMap.get(aCase.ContactID).AccountId != null && parentContactsMap.get(aCase.ContactID).Account.ClassLevel1__c == 'IG'){
                            aCase.Origin = Label.CL00687;                              
                        }
                    }
                }           
            }
        }
        
        public static void updateBusinessHours (List<Case> caseLst){
        //*********************************************************************************
        // Method Name : updateBusinessHours
        // Purpose : To insert/update the BusinessHour which is is based on the CCC Country of Case
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 24th February 2012
        // Remarks : For Apr - 12 Release
        // Modified by : Vimal Karunakaran (GD)
        // Purpose : To get Business Hour from Country Object itself,rather querying from BusinessHour object
        // Date Modified : 16th Feb 2015
        // Remarks : For Apr - 15 Release
        ///*********************************************************************************/
        
        System.Debug('****AP20_UpdateCaseCountry updateBusinessHours  Started****');
    
        List<BusinessHours> bhList = new List<BusinessHours>();
        Set<Id> setCCCCountryId = new Set<Id>();
        Map<Id,Id> MapCountry = new Map<Id,Id>();
        //Set<String> setCCCCountry = new Set<String>();
        Map<String, Id> mapCoutryBusinessHours = new Map<String, Id>();
        
        for (Case objCase: caseLst){
            if(objCase.CCCountry__c != null)
                setCCCCountryId.add(objCase.CCCountry__c);
        }
        if(setCCCCountryId != null && setCCCCountryId.size()>0)
        for(Country__c con: [Select Id, BusinessHours__c from Country__c where Id in:setCCCCountryId ]){
            //setCCCCountry.add(con.Name); 
            MapCountry.put(con.id,con.BusinessHours__c);
        }
        /* For Apr - 15 Release
        bhList = [Select Id, Name,IsActive from BusinessHours where Name in: setCCCCountry AND IsActive =TRUE];
        for(BusinessHours objBusinessHours : bhList){
            mapCoutryBusinessHours.put(objBusinessHours.Name,objBusinessHours.Id);
        }
        */
            
        for (Case objCase: caseLst){
            if(MapCountry.containsKey(objCase.CCCountry__c)){
                //if(mapCoutryBusinessHours.containsKey(MapCountry.get(objCase.CCCountry__c))){
                objCase.BusinessHoursId = MapCountry.get(objCase.CCCountry__c);
                //}
            }
            else{
                objCase.BusinessHoursId = Label.CL00754;
            }
        }
        System.Debug('****AP20_UpdateCaseCountry updateBusinessHours  Finished****');
    }
    public static ServiceLevelAgreement__c getSLAPriority(Case objCase, List<ServiceLevelAgreement__c> slaList){
        //*********************************************************************************
        // Method Name : getSLAPriority
        // Purpose : Business Logic for identifying SLA based on Case Parameters. The 
        //           SLA is defined based on priority set by Business
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 24th February 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Apr - 12 Release
        ///*********************************************************************************/
        
        //Method accepts 2 parameter, one is Case Object, and other is List of SLA associated with CCC country of the Case.
        //Service Level Agreement has 8 fields, excluding SLA field. 
        //Method will iteratite through all the SLA, which has maximum match with the Case fields. The priority is defined on TD
        //Returns matching SLA.        
        
                
        ServiceLevelAgreement__c slaMatch;
        Integer intMaxMatchCount = 0;
        Integer intMatchCountReturn = 0;
        Integer intMaxNullMatchCount = 0;
        List<ServiceLevelAgreement__c> slaNullList =new List<ServiceLevelAgreement__c>();
        Boolean blnSLAMatchAdded = false;
        
        Account objAccount;
        //for(Integer intPriority=1; intPriority<=8; intPriority++){
            for(ServiceLevelAgreement__c objSLA :slaList) {
                objAccount = new Account();
                if(caseAccountMap.containsKey(objCase.Id)){                
                    objAccount = caseAccountMap.get(objCase.Id);
                }
                else{
                    objAccount.AccountSupportLevel__c = objCase.TECH_AccountSupportLevel__c;
                    objAccount.ClassLevel1__c = objCase.ACC_ClassLevel1__c;
                    objAccount.ServiceContractType__c = objCase.AccountServiceContractType__c;
                }
                intMatchCountReturn = getSLAMatchCount(objCase,objSLA,objAccount);
                System.Debug('intMaxMatchCount:' + intMaxMatchCount);
                System.Debug('intMatchCountReturn:' + intMatchCountReturn);
                if(intMaxMatchCount <= intMatchCountReturn){
                   if(intMaxMatchCount < intMatchCountReturn){
                       intMaxMatchCount = intMatchCountReturn;
                       slaMatch = objSLA;
                       slaNullList.clear();
                       blnSLAMatchAdded = false;
                       System.debug('New Matching SLA Found');
                       System.debug('Old SLA:' + slaMatch);
                       System.debug('New SLA:' + objSLA);
                    }
                    if(intMaxMatchCount == intMatchCountReturn){
                        if(!blnSLAMatchAdded){
                            slaNullList.add(slaMatch);
                            blnSLAMatchAdded =true;
                        }
                        slaNullList.add(objSLA);
                    }
                }  
            }
            System.debug('slaNullList.size():' + slaNullList.size());
            if(slaNullList!=null && slaNullList.size()>0){
                System.debug('slaNullList: ' + slaNullList);
                slaMatch = getMaxNullSLA(slaNullList);
                System.debug('SLA With Max Null:' + slaMatch);
            }
        //}
        System.debug('Final Matching SLA:' + slaMatch);
        System.Debug('****AP20_UpdateCaseCountry getSLAPriority Finished****');
        
        return slaMatch;
    }
    public static ServiceLevelAgreement__c getMaxNullSLA(List<ServiceLevelAgreement__c> slaNullList){
        //*********************************************************************************
        // Method Name : getMaxNullSLA
        // Purpose : To get the  SLA with maximum Null
        // 
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 24th February 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Apr - 12 Release
        ///*********************************************************************************/
        
        
        System.Debug('****AP20_UpdateCaseCountry getMaxNullSLA Started****');
        Integer intNullCount = 0;
        Integer intMaxNullCount = 0;
        ServiceLevelAgreement__c slaWithMaxNull = new ServiceLevelAgreement__c();
        
        for(ServiceLevelAgreement__c slaEqualMatch : slaNullList){
            intNullCount = 0;
            if(slaEqualMatch.CustomerClasslevel1__c == null){
                intNullCount= intNullCount + 1;
                System.debug('ACC_ClassLevel1__c is Null');
            }
            
            if(slaEqualMatch.AccountSupportLevel__c == null){
                intNullCount= intNullCount + 1;
                System.debug('TECH_AccountSupportLevel__c is Null');
            }
            
            if(slaEqualMatch.ContactSupportLevel__c == null){
                intNullCount= intNullCount + 1;
                System.debug(' TECH_ContactSupportLevel__c is Null');
            }
            
            if(slaEqualMatch.AccountServiceContractType__c == null){
                intNullCount= intNullCount + 1;
                System.debug('AccountServiceContractType__c is Null');
            }
            
            if(slaEqualMatch.CaseCategory__c == null){
                intNullCount= intNullCount + 1;
                System.debug(' TECH_ContactSupportLevel__c is Null');
            }
            
            if(slaEqualMatch.CaseReason__c == null){
                intNullCount= intNullCount + 1;
                System.debug('Symptom__c is Null');
            }
            
            if(slaEqualMatch.CaseOrigin__c == null){
                intNullCount= intNullCount + 1;
                System.debug('Origin is Null');
            }
            
            if(slaEqualMatch.CasePriority__c == null){
                intNullCount= intNullCount + 1;
                System.debug('Priority is Null');
            }
            if(slaEqualMatch.DebitPointReason__c == null){
                intNullCount= intNullCount + 1;
                System.debug('Debit Point Reason is Null');
            }
            
            if(slaEqualMatch.CCCCountry__c == null){
                intNullCount= intNullCount + 1;
                System.debug('CCCountry__c is Null');
            }
            System.Debug('SLA Null Count:' + slaEqualMatch.Name + ': ' +  intNullCount );
            if(intMaxNullCount <=intNullCount){
                if(intMaxNullCount <intNullCount){
                    intMaxNullCount = intNullCount;
                    slaWithMaxNull = slaEqualMatch;
                }
                //if(intMaxNullCount == intNullCount){
                //}
            }
        }
        System.Debug('****AP20_UpdateCaseCountry getMaxNullSLA Finished ****');
        return slaWithMaxNull;
    }
    public static void updateCaseDueDate(List<Case> caseLst, Map <Id,Country__c> CountryMap){
        //*********************************************************************************
        // Method Name : updateCaseDueDate
        // Purpose : To insert/update the Due Date based on the BusinessHour assigned to the Case
        //           BusinessHour is based on the CCC Country of Case
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 24th February 2012
        // Modified by : Rakhi Kumar
        // Date Modified : 25th February 2013
        // Description : May 2013 Release: TECH_SLADueDate__c to be set. This field will store
        //               the calculated DueDate. The existing DueDate field will now be editable to
        //               record negotiated Due Date.        
        // Remarks : For Apr - 12 Release
        ///*********************************************************************************/
        
        
        System.Debug('****AP20_UpdateCaseCountry updateCaseDueDate Started****');

        Set<Id> setCCCCountryID = new Set<Id>();
        
        List<ServiceLevelAgreement__c> lstSLA = new List<ServiceLevelAgreement__c>();
        List<Case> lstNewCase = new List<Case>();
        ServiceLevelAgreement__c slaMatch;
        Integer intCaseSLA;
        Id BusinessHoursId = null;
        Boolean blnSLAFound =false;
        Map<Id,List<ServiceLevelAgreement__c>> mapCountrySLA = new Map<Id,List<ServiceLevelAgreement__c>>();
        for (Case objCase: caseLst){
            //BR-8094 - Modify SLA to look for CCC Location rather than CCC Country
            //if(objCase.CCCountry__c !=null)
            //    setCCCCountryID.add(objCase.CCCountry__c);
            if(objCase.CCCLocation__c !=null)
                setCCCCountryID.add(objCase.CCCLocation__c);
            
            if(objCase.CreatedDate==null)
                lstNewCase.add(objCase);
        }
        if(lstNewCase.size()>0)
            populateCaseAccountMap(lstNewCase);
            
        //lstSLA = [SELECT Name, AccountSupportLevel__c, CaseCategory__c, CaseOrigin__c, CasePriority__c, CaseReason__c, CCCCountry__c, ContactSupportLevel__c, CustomerClasslevel1__c, AccountServiceContractType__c, SLA__c FROM ServiceLevelAgreement__c where   CCCCountry__c =: setCCCCountryID];
        lstSLA = [SELECT Name, AccountSupportLevel__c, CaseCategory__c, CaseOrigin__c, CasePriority__c, CaseReason__c, CCCCountry__c, ContactSupportLevel__c, CustomerClasslevel1__c, AccountServiceContractType__c, SLA__c,PrimaryFirstAssignmentSLA__c, AdvancedFirstAssignmentSLA__c, ExpertFirstAssignmentSLA__c,DebitPointReason__c FROM ServiceLevelAgreement__c where   CCCCountry__c =: setCCCCountryID];
                
        for(ServiceLevelAgreement__c objSLA: lstSLA){
            if(mapCountrySLA.containsKey(objSLA.CCCCountry__c)){
                mapCountrySLA.get(objSLA.CCCCountry__c).add(objSLA);
            }
            else{
                List<ServiceLevelAgreement__c> slaList = new List<ServiceLevelAgreement__c>();
                slaList.add(objSLA);
                mapCountrySLA.put(objSLA.CCCCountry__c,slaList);
            }
            
        }
        for (Case objCase: caseLst){
            if(objCase.CCCLocation__c !=null && CountryMap!=null && CountryMap.containsKey(objCase.CCCLocation__c) &&(CountryMap.get(objCase.CCCLocation__c).BusinessHours__c != null || CountryMap.get(objCase.CCCLocation__c).BusinessHours__c !=Label.CL00754)){
                if(!(Test.isRunningTest() && caseLst.size() > Integer.ValueOf(Label.CL00399))){
                    BusinessHoursId = CountryMap.get(objCase.CCCLocation__c).BusinessHours__c;
                    List<ServiceLevelAgreement__c> slaList = null;
                    blnSLAFound =false;
                    //BR-8094 - Modify SLA to look for CCC Location rather than CCC Country
                    /*
                    if(mapCountrySLA.containsKey(objCase.CCCountry__c)){
                        slaList = mapCountrySLA.get(objCase.CCCountry__c);
                    }
                    */
                    if(mapCountrySLA.containsKey(objCase.CCCLocation__c)){
                        slaList = mapCountrySLA.get(objCase.CCCLocation__c);
                    }
                    If(slaList !=null && slaList.size()>0){
                        slaMatch = getSLAPriority(objCase,slaList);
                    }
                    if(slaMatch != null){
                        intCaseSLA = slaMatch.SLA__c.intValue();
                        Datetime createDate;
                        if(objCase.CreatedDate==null)
                            createDate = System.now();
                        else
                            createDate = objCase.CreatedDate;
                        objCase.TECH_SLA__c =  slaMatch.Name;
                        objCase.TECH_PrimaryFirstAssignmentSLA__c =  slaMatch.PrimaryFirstAssignmentSLA__c;
                        objCase.TECH_AdvancedFirstAssignmentSLA__c =  slaMatch.AdvancedFirstAssignmentSLA__c;
                        objCase.TECH_ExpertFirstAssignmentSLA__c =  slaMatch.ExpertFirstAssignmentSLA__c;
                        
                        if((!(BusinessHoursId==null || BusinessHoursId ==Label.CL00754)) && (!(objCase.TECH_IsDueDateChanged__c))){
                            objCase.DueDate__c = BusinessHours.add(BusinessHoursId,createDate , intCaseSLA*60*60*1000); 
                            objCase.TECH_SLADueDate__c = objCase.DueDate__c;
                        }
                    }
                    else{
                        objCase.DueDate__c =null;
                        objCase.TECH_SLADueDate__c = null;
                        objCase.TECH_SLA__c =  null;
                    }
                    System.debug('System.now()' + System.now() );
                    System.debug('intCaseSLA' + intCaseSLA);
                    System.debug('objCase.DueDate__c:' + objCase.DueDate__c);
                }
            }
        }
        System.Debug('****AP20_UpdateCaseCountry updateCaseDueDate Finished****');
    }
    public static Integer getSLAMatchCount(Case objCase,ServiceLevelAgreement__c objSLA,Account objAccount){
        //*********************************************************************************
        // Method Name : getSLAMatchCount
        // Purpose : To get the Matching SLA Count for each SLA
        // System Context, when Symptom page is cancelled
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 24th February 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Apr - 12 Release
        ///*********************************************************************************/
        
        
        System.Debug('****AP20_UpdateCaseCountry getSLAMatchCount Started****');
        Integer intMatchCount =0;
        String strAccountCustClassLevel1 = '';
        String strAccountSupportLevel = '';
        String strAccountServiceContractType = ''; 
        
        System.debug('objCase.AccountID' + objCase.AccountID);
        if(objCase.CreatedDate==null){
            strAccountSupportLevel        = objAccount.AccountSupportLevel__c;
            strAccountCustClassLevel1     = objAccount.ClassLevel1__c;
            strAccountServiceContractType = objAccount.ServiceContractType__c;
        }
        else{
            strAccountSupportLevel        = objCase.TECH_AccountSupportLevel__c;
            strAccountCustClassLevel1     = objCase.ACC_ClassLevel1__c;
            strAccountServiceContractType = objCase.AccountServiceContractType__c;    
        }
        
        

        System.debug('strAccountSupportLevel' + strAccountSupportLevel);
        System.debug('strAccountCustClassLevel1' + strAccountCustClassLevel1);
        System.debug('strAccountServiceContractType' + strAccountServiceContractType);
        
        //objCase.ACC_ClassLevel1__c
        System.debug('objCase.ACC_ClassLevel1__c:' + objCase.ACC_ClassLevel1__c);
        System.debug('objSLA.CustomerClasslevel1__c: ' + objSLA.CustomerClasslevel1__c);
        //if((objCase.ACC_ClassLevel1__c != null && objCase.ACC_ClassLevel1__c != '' && objCase.ACC_ClassLevel1__c == objSLA.CustomerClasslevel1__c)){
        if((strAccountCustClassLevel1 == objSLA.CustomerClasslevel1__c)){
            intMatchCount= intMatchCount + 1;
            System.debug('ACC_ClassLevel1__c Matched');
        }
        System.debug('objCase.Account.AccountSupportLevel__c:' + objCase.Account.AccountSupportLevel__c);
        System.debug('objCase.TECH_AccountSupportLevel__c:' + objCase.TECH_AccountSupportLevel__c);
        System.debug('objSLA.AccountSupportLevel__c:' + objSLA.AccountSupportLevel__c);
        //if((objCase.TECH_AccountSupportLevel__c != null && objCase.TECH_AccountSupportLevel__c != '' &&objCase.TECH_AccountSupportLevel__c == objSLA.AccountSupportLevel__c)){
        if((strAccountSupportLevel == objSLA.AccountSupportLevel__c)){
            intMatchCount= intMatchCount + 1;
            System.debug('TECH_AccountSupportLevel__c Matched');
        }
        System.debug('objCase.TECH_ContactSupportLevel__c:' + objCase.TECH_ContactSupportLevel__c);
        System.debug('objSLA.ContactSupportLevel__c:' + objSLA.ContactSupportLevel__c);
        //if((objCase.TECH_ContactSupportLevel__c != null && objCase.TECH_ContactSupportLevel__c != '' && objCase.TECH_ContactSupportLevel__c == objSLA.ContactSupportLevel__c)){
        if((objCase.TECH_ContactSupportLevel__c == objSLA.ContactSupportLevel__c)){
            intMatchCount= intMatchCount + 1;
            System.debug(' TECH_ContactSupportLevel__c Matched');
        }
        
        System.debug('objCase.AccountServiceContractType__c:' + objCase.AccountServiceContractType__c);
        System.debug('objSLA.AccountServiceContractType__c:' +objSLA.AccountServiceContractType__c);
        //if((objCase.AccountServiceContractType__c != null && objCase.AccountServiceContractType__c != '' && objCase.AccountServiceContractType__c== objSLA.AccountServiceContractType__c)){
        if((strAccountServiceContractType == objSLA.AccountServiceContractType__c)){
            intMatchCount= intMatchCount + 1;
            System.debug('AccountServiceContractType__c Matched');
        }
        
        System.debug('objCase.SupportCategory__c:' + objCase.SupportCategory__c);
        System.debug('objSLA.CaseCategory__c:' + objSLA.CaseCategory__c);
        //if((objCase.SupportCategory__c != null && objCase.SupportCategory__c != '' && objCase.SupportCategory__c == objSLA.CaseCategory__c)){
        if((objCase.SupportCategory__c == objSLA.CaseCategory__c)){
            intMatchCount= intMatchCount + 1;
            System.debug(' TECH_ContactSupportLevel__c Matched');
        }
        
        System.debug('objCase.Symptom__c:' + objCase.Symptom__c);
        System.debug('objSLA.CaseReason__c:' + objSLA.CaseReason__c);
        //if((objCase.Symptom__c != null && objCase.Symptom__c != '' && objCase.Symptom__c == objSLA.CaseReason__c)){
        if((objCase.Symptom__c == objSLA.CaseReason__c)){
            intMatchCount= intMatchCount + 1;
            System.debug('Symptom__c Matched');
        }
        System.debug('objCase.Origin:' + objCase.Origin);
        System.debug('objSLA.CaseOrigin__c:' + objSLA.CaseOrigin__c );
        //if((objCase.Origin != null && objCase.Origin != '' && objCase.Origin == objSLA.CaseOrigin__c)){
        if((objCase.Origin == objSLA.CaseOrigin__c)){
            intMatchCount= intMatchCount + 1;
            System.debug('Origin Matched');
        }
        System.debug('objCase.Priority:' + objCase.Priority);
        System.debug('objSLA.CasePriority__c:' + objSLA.CasePriority__c);
        //if((objCase.Priority != null && objCase.Priority != '' && objCase.Priority == objSLA.CasePriority__c)){
        if((objCase.Priority == objSLA.CasePriority__c)){
            intMatchCount= intMatchCount + 1;
            System.debug('Priority Matched');
        }
        
        System.debug('objCase.PointDebitReason__c:' + objCase.PointDebitReason__c);
        System.debug('objSLA.DebitPointReason__c:' + objSLA.DebitPointReason__c);
        //if((objCase.PointDebitReason__c != null && objCase.PointDebitReason__c != '' && objCase.PointDebitReason__c == objSLA.DebitPointReason__c)){
        if((objCase.PointDebitReason__c == objSLA.DebitPointReason__c)){
            intMatchCount= intMatchCount + 1;
            System.debug('Debit Point Reason Matched');
        }
        System.debug('objCase.CCCountry__c:' + objCase.CCCountry__c);
        System.debug('objSLA.CCCCountry__c:' + objSLA.CCCCountry__c);
        //if((objCase.CCCountry__c != null && objCase.CCCountry__c == objSLA.CCCCountry__c)){
        if((objCase.CCCountry__c == objSLA.CCCCountry__c)){
            intMatchCount= intMatchCount + 1;
            System.debug('CCCountry__c Matched');
        }
        
        System.Debug('****AP20_UpdateCaseCountry getSLAMatchCount Finished****');
        return intMatchCount;
    }
    public static void populateCaseAccountMap(List<Case> lstCase){
        //*********************************************************************************
        // Method Name : populateCaseAccountMap
        // Purpose : To populate the CaseAccount Map on Case Insert 
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 24th February 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Apr - 12 Release
        ///*********************************************************************************/
        
        
        System.Debug('****AP20_UpdateCaseCountry populateCaseAccountMap Started****');
        
        Set<Id> setCaseID = new Set<Id>();
        caseAccountMap = new Map<Id,Account>();
        for (Case objCase: lstCase){
            setCaseID.add(objCase.ContactId);
        }
        Map<Id,Contact> mapContact = new Map<Id,Contact>([Select Id, Account.AccountSupportLevel__c, Account.ClassLevel1__c, Account.ServiceContractType__c from Contact where Id in: setCaseID]);
        Account objAccount=null;
        for (Case objCase: lstCase){
            if(mapContact.containsKey(objCase.ContactId)){
                objAccount =null;
                objAccount=new Account();
                objAccount.AccountSupportLevel__c = mapContact.get(objCase.ContactId).Account.AccountSupportLevel__c;
                objAccount.ClassLevel1__c = mapContact.get(objCase.ContactId).Account.ClassLevel1__c;
                objAccount.ServiceContractType__c = mapContact.get(objCase.ContactId).Account.ServiceContractType__c;
                caseAccountMap.put(objCase.Id,objAccount);
            }
        }
        System.Debug('****AP20_UpdateCaseCountry populateCaseAccountMap Finished****');
    }
    
    public static void updateCaseObject(Case objCase){
        //*********************************************************************************
        // Method Name : updateCaseObject
        // Purpose : To insert/update the TECH Fields on case object on 
        // System Context, when Symptom page is cancelled
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 24th February 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Apr - 12 Release
        ///*********************************************************************************/
        
        
        System.Debug('****AP20_UpdateCaseCountry updateCaseObject Started****');
        try{
            Database.SaveResult caseUpdateResult = Database.update(objCase);
            
            if(!caseUpdateResult.isSuccess()){
                Database.Error err = caseUpdateResult.getErrors()[0];
                System.Debug('######## AP20 Symptom Update Error : '+err.getStatusCode()+' '+err.getMessage());
            }
        }
        catch(Exception ex){
            System.Debug('######## AP20 Symptom Update Error :' + ex );
        }
        
        System.Debug('****AP20_UpdateCaseCountry updateCaseObject Finished****');
    }
}