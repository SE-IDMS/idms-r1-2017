/*****************************************************************************************
    Author : Shruti Karn
    Description : For September 2012 Release to override 'New Package' button so that:
                  1. End is populated based on the selected Package Template duration.
                                       
*****************************************************************************************/
public class VFC_NewPackage
{
    public Package__c pkg{get;set;}
    public String startDate {get;set;}
    PackageTemplate__c pkgTemplate;
    ApexPages.StandardController controller;
    String queryString;
    
/******************************************************************************************************************
    Constructor
******************************************************************************************************************/
    public VFC_NewPackage(ApexPages.StandardController controller)
    {
        List<String> Fields=new List<String>{'PackageTemplate__c','Contract__c','Support_Level__c','Name','Status__c','LegacyPackageId__c','StartDate__c','EndDate__c','DONOTDELIVER__c','DONOTPRINT__c','Active__c','OfferedPrice__c','Description__c','AutomaticProactiveCase__c','ProactiveCaseSupportTeam__c'};
        if(!Test.isRunningTest())
        controller.addFields(Fields);
        this.controller = controller;
        pkg = new Package__c();
        pkg = (Package__c) controller.getRecord();
        Package__c tempPKG = new PAckage__c();
        tempPKG = pkg;

        if(pkg.Id != null)
        {
            tempPKG = [Select id, PackageTemplate__c , Contract__c, Name, Status__c, LegacyPackageId__c, StartDate__c, EndDate__c, Description__c, AutomaticProactiveCase__c, ProactiveCaseSupportTeam__c from Package__c where id =: pkg.Id limit 1];
        }
        if(tempPKG.PackageTemplate__c != null && tempPKG.Id == null )
        {
           pkgTemplate  = new PackageTemplate__c();
           pkgTemplate = [Select id, PackageDuration__c,SupportLevel__c,OfferedPrice__c from PackageTemplate__c where id =: tempPKG.PackageTemplate__c limit 1];
           if(pkgTemplate.PackageDuration__c != null)
               pkg.Enddate__c = system.today().addMonths((Integer)pkgTemplate.PackageDuration__c) ;
           pkg.Support_Level__c = pkgTemplate.SupportLevel__c;
           pkg.OfferedPrice__c = pkgTemplate.OfferedPrice__c;

        }
      
        PageReference thisPage = ApexPages.currentPage();
        List<String> url = new list<String>();
        url = thisPage.getUrl().split('\\?');
        if(url.size() > 1)
            queryString = url[1];
    }
    
/******************************************************************************************************************
    Calcuate the End date based on the Package Template's duration and start date
******************************************************************************************************************/

    public void calculateEndDate()
    {
        if(startDate!= null && startDate.indexOf(':') != -1)
        {
            String[] result = startDate.split(':');
            pkg.PackageTemplate__c = result[0];
            startDate = result[1];
        }
        if(startDate != null && startDate.trim() != '')
        {
            try
            {
                date strtDate = date.parse(startDate );
                              
                String query = 'Select id, PackageDuration__c,SupportLevel__c,OfferedPrice__c from PackageTemplate__c where id ='+'\''+pkg.packagetemplate__c+'\''+ ' limit 1';
                if(pkg.PackageTemplate__c != null)
                {
                    pkgTemplate = database.query(query);
                    if(pkgTemplate.PackageDuration__c != null)
                        pkg.Enddate__c = strtDate.addMonths((Integer)pkgTemplate.PackageDuration__c) ;
                    pkg.Support_Level__c = pkgTemplate.SupportLevel__c; 
                    pkg.OfferedPrice__c = pkgTemplate.OfferedPrice__c;
                }
            }
            catch(Exception e)
            {
system.debug('Error while calculating End Date:'+e.getMessage());
            }
        }
       
    }
/******************************************************************************************************************
    Functionality for Save and New Package
******************************************************************************************************************/
    public PageReference SaveandNew()
    {
        try
        {
            upsert pkg;
            Schema.DescribeSObjectResult describeResult = Controller.getRecord().getSObjectType().getDescribe();
            PageReference pr = new PageReference('/' + describeResult.getKeyPrefix() + '/e?' + queryString);
            pr.setRedirect(true);
            return pr;  
        }
        catch(Exception e)
        {
           
            for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),'')); System.debug(e.getDmlMessage(i)); 
            } 
            return null;
        }
    }
    
    
}