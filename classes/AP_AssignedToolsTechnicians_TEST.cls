/*
    Author          : Hari Krishna
    Date Created    : 21/03/2013
    Description     : Test class for AP_AssignedToolsTechnicians
*/
@isTest(SeeAllData=true)
private class AP_AssignedToolsTechnicians_TEST {


static testMethod void testATT2(){

SVMXC__Service_Order__c wo =  new SVMXC__Service_Order__c(SVMXC__Order_Status__c = 'Customer Confirmed');
        Test.startTest();
        try{
         insert wo;
         AssignedToolsTechnicians__c tec= new AssignedToolsTechnicians__c(WorkOrder__c=wo.id,PrimaryUser__c = true);
         insert tec;
         
         
         
         
         }
         catch (exception e){}
         Test.stoptest();

}

     static testMethod void testATT(){
            
            List<SVMXC__Service_Group_Members__c> Techlist = New List<SVMXC__Service_Group_Members__c>();
            SVMXC__Service_Group__c st;
            SVMXC__Service_Group_Members__c sgm;
            RecordType[] rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c'];
            RecordType[] rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c'];  
              
             for(RecordType rt : rts) //Loop to take a record type at a time
            {
                // Create Module Record
                if(rt.DeveloperName == 'Technician')
               {
                     st = new SVMXC__Service_Group__c(
                                               // RecordTypeId =rt.Id,
                                                SVMXC__Active__c = true,
                                                Name = 'Test Service Team'                                                                                        
                                                );
                Test.startTest();
                    insert st;
                    
                } 
          }
            for(RecordType rt : rtssg) //Loop to take a record type at a time
            {
                // Create Module Record
               if(rt.DeveloperName == 'Technican')
                {
                     sgm = new SVMXC__Service_Group_Members__c(
                                                RecordTypeId =rt.Id,
                                                SVMXC__Active__c = true,
                                                PrimaryAddress__c='Home',
                                                Name = 'Test user',
                                                SVMXC__Service_Group__c =st.id,
                                                SESAID__c ='201771',
                                                SVMXC__Role__c = 'Schneider Employee',
                                                //SVMXC__Salesforce_User__c = UserInfo.getUserId(),
                                                Manager__c = UserInfo.getUserId()
                                                );
                   insert sgm;
                   // Techlist.add(sgm);
                } 
                //insert Techlist[0];
         }       
            
            SVMXC__Dispatcher_Access__c dispatchAccess = new SVMXC__Dispatcher_Access__c();
            dispatchAccess.SVMXC__Dispatcher__c = UserInfo.getUserId();
            dispatchAccess.SVMXC__Service_Team__c = st.id;
            insert dispatchAccess;
            
            //SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
             Account acc = Utils_TestMethods.createAccount();
             insert acc;
        Country__c Ctry = Utils_TestMethods.createCountry(); 
        insert Ctry;

        CustomerLocation__c Cust_Location = Utils_TestMethods.createCustomerLocation(Acc.Id, Ctry.Id);
        insert Cust_Location;
        
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
         workOrder.Work_Order_Category__c='On-site';                 
         workOrder.SVMXC__Order_Type__c='Maintenance';
         workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
         workOrder.SVMXC__Order_Status__c = 'UnScheduled';
         workOrder.CustomerRequestedDate__c = Date.today();
         workOrder.Service_Business_Unit__c = 'Energy';
         workOrder.SVMXC__Priority__c = 'Normal-Medium';
         workOrder.Customer_Location__c = Cust_Location.Id;
         workOrder.SendAlertNotification__c = true;
         workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder.BackOfficeReference__c = '111111';
         
         insert workOrder;
         
         Event e = Utils_TestMethods.createEvent(workOrder.id,null,'Planned');
         insert e;
         Event e2 = Utils_TestMethods.createEvent(workOrder.id,null,'Planned');
         insert e2;
         
        workOrder.SVMXC__Group_Member__c = sgm.id;
        update workOrder;
        List<SVMXC__Service_Order__c> wolist = new List<SVMXC__Service_Order__c>();
        
        SVMXC__Service_Order__c wo11 =  new SVMXC__Service_Order__c(SVMXC__Order_Status__c = 'Rescheduled');
         insert wo11;
         
        
         set<string> evset= new set<string>();
         
         Event Ev2= new Event ( Subject='WO-12343',  StartDateTime=system.now().addDays(1),  EndDateTime=system.now().addDays(1),
                                                    Whatid=wo11.id,
                                                    Ownerid=UserInfo.getUserId()                                                    
                                                    );
            insert  Ev2;
            update Ev2;
            //Delete Ev2;
            evset.add(Ev2.id);
        
         
         AssignedToolsTechnicians__c technician = new AssignedToolsTechnicians__c();
         technician.WorkOrder__c = workOrder.id;
         technician.Status__c ='Accepted';
         technician.PrimaryUser__c = true;
         technician.TechnicianEquipment__c = sgm.id; 
         try   {       
         insert technician;
         
         }
         catch (exception ee){}
         Test.StopTest();
          
          
         set<id> widset = new Set<id>();
         widset.add(workOrder.id);
         wolist.add(workOrder);
        
         
         AP_AssignedToolsTechnicians.updateAssignedToolsTechniciansStatus(widset ,'Cancelled');
         AP_AssignedToolsTechnicians.updateAssignedToolsTechniciansPrimaryUser(wolist );
         AP_AssignedToolsTechnicians.updateAssignedToolsTechniciansFieldUpdate(wolist,'BackOfficeReference__c','BackOfficeReference__c');
          if(evset!=null)
         AP_AssignedToolsTechnicians.CancelledAssignedToolsTechnicians(evset);
            
        Delete Ev2;
        
     }
    
     static testMethod void testATT3(){
         
          List<SVMXC__Service_Group_Members__c> Techlist = New List<SVMXC__Service_Group_Members__c>();
            SVMXC__Service_Group__c st;
            SVMXC__Service_Group_Members__c sgm;
            RecordType[] rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c'];
            RecordType[] rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c'];  
              
             for(RecordType rt : rts) //Loop to take a record type at a time
            {
                // Create Module Record
                if(rt.DeveloperName == 'Technician')
               {
                     st = new SVMXC__Service_Group__c(
                                               // RecordTypeId =rt.Id,
                                                SVMXC__Active__c = true,
                                                Name = 'Test Service Team'                                                                                        
                                                );
                Test.startTest();
                    insert st;
                    
                } 
            }
            for(RecordType rt : rtssg) //Loop to take a record type at a time
            {
                // Create Module Record
               if(rt.DeveloperName == 'Technican')
                {
                     sgm = new SVMXC__Service_Group_Members__c(
                                                RecordTypeId =rt.Id,
                                                SVMXC__Active__c = true,
                                                PrimaryAddress__c='Home',
                                                Name = 'Test user',
                                                SVMXC__Service_Group__c =st.id,
                                                SESAID__c ='201771',
                                                SVMXC__Role__c = 'Schneider Employee',
                                                //SVMXC__Salesforce_User__c = UserInfo.getUserId(),
                                                Manager__c = UserInfo.getUserId()
                                                );
                   insert sgm;
                   // Techlist.add(sgm);
                } 
                //insert Techlist[0];
         } 
         
          Account acc = Utils_TestMethods.createAccount();
          insert acc;
          Country__c Ctry = Utils_TestMethods.createCountry(); 
          insert Ctry;

        CustomerLocation__c Cust_Location = Utils_TestMethods.createCustomerLocation(Acc.Id, Ctry.Id);
        insert Cust_Location;
        
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
         workOrder.Work_Order_Category__c='On-site';                 
         workOrder.SVMXC__Order_Type__c='Maintenance';
         workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
         workOrder.SVMXC__Order_Status__c = 'UnScheduled';
         workOrder.CustomerRequestedDate__c = Date.today();
         workOrder.Service_Business_Unit__c = 'Energy';
         workOrder.SVMXC__Priority__c = 'Normal-Medium';
         workOrder.Customer_Location__c = Cust_Location.Id;
         workOrder.SendAlertNotification__c = true;
         workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder.BackOfficeReference__c = '111111';
         
         insert workOrder;
         Event e = Utils_TestMethods.createEvent(workOrder.id,null,'Planned');
         insert e;
          AssignedToolsTechnicians__c technician = new AssignedToolsTechnicians__c();
         technician.WorkOrder__c = workOrder.id;
         technician.Status__c ='Accepted';
         technician.PrimaryUser__c = true;
         technician.EventID__c = e.id;
         technician.TechnicianEquipment__c = sgm.id; 
         try{       
          	insert technician;
         	AP_AssignedToolsTechnicians.CancelledAssignedToolsTechnicians(new set<String>{e.id});
         }
         catch(Exception ex){
             System.debug('Exception ex'+ex);
         }
         
         
         
     }
     
             
}