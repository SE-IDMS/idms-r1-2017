global class AP_WOFSRNotification {
    /*
    @future
    public static void UpdateTechStatus(ID currentworkOrder){   
        List<AssignedToolsTechnicians__c> attList = [Select id, WorkOrder__c, Tech_WOStatus__c,Status__c from AssignedToolsTechnicians__c where WorkOrder__c =:currentworkOrder  and Status__c ='Accepted'];
        if(attList != null && attList.size()>0){        
            for(AssignedToolsTechnicians__c att:attList){                    
                att.Tech_WOStatus__c= 'Customer Confirmed';                    
            }                       
        }   
    }*/
    
    public static Map<String,String> customeTimeZoneMap = new Map<String,String>();

    public static Map<String,String> getPickList(){
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get('SVMXC__Service_Order__c');
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); 
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); 
        List<Schema.PicklistEntry> pick_list_values = field_map.get('CustomerTimeZone__c').getDescribe().getPickListValues(); 
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
          
         customeTimeZoneMap.put(a.getValue(),a.getLabel());
          
       }

       return customeTimeZoneMap;
    }
        
    public static boolean IsTesting(boolean executeFromtest){
        if (executeFromtest == true){
            return true;
        }
        else return false;
    }
        
        //Technician notification, if executeFromtest = ture then it's executed through a test class
    public static void NotifyTechnicians(Id currentworkOrder){
        try
        {
            
            String RecordTypeSt = System.Label.CLAPR15SRV78;
            String WorkOrderId = currentworkOrder;
            String ACCEPTED = Label.CLDEC13SRV06;
            Set<String> fieldsNotbeInQuery = new Set<String>();
            fieldsNotbeInQuery.add('CustomerTimeZone__c'); 
            Map<String, Schema.SObjectType> allObjs =new Map<String, Schema.SObjectType>();
            allObjs = Schema.getGlobalDescribe();
            Map<String,Schema.SObjectField> objfieldMap = new Map<String,Schema.SObjectField>();                
            Schema.SObjectType objT =allObjs.get('SVMXC__Service_Order__c') ;
            objfieldMap = objT.getDescribe().fields.getMap();
            String QueryFields ='';
            QueryFields +=' SVMXC__Contact__r.WorkPhone__c ';
            QueryFields +=', toLabel(CustomerTimeZone__c) ';
            QueryFields +=', SVMXC__Contact__r.Name ';
            QueryFields +=', SVMXC__Company__r.Name ';
            QueryFields +=', toLabel(SVMXC__Company__r.Type) ';
            QueryFields +=', SVMXC__Site__r.Name ';
            QueryFields +=', SVMXC__Service_Contract__r.Name ';
            QueryFields +=', SVMXC__Service_Contract__r.BOContractType__c ';
            for (String fieldKey : objfieldMap.keySet())
            {
                Map<String,String> fMap = new Map<String,String>();
                Schema.SObjectField fsObj = objfieldMap.get(fieldKey);
                Schema.DescribeFieldResult f = fsObj.getDescribe();
                if(f.isAccessible () == true && ! fieldsNotbeInQuery.contains(f.getLocalName()))
                {                       
                    if(QueryFields.length()>0){
                        QueryFields +=','+f.getLocalName();
                    }
                    else{
                        QueryFields +=f.getLocalName();
                    }
                }
            }
                
            String QueryString = 'Select '+QueryFields+' From SVMXC__Service_Order__c ';
            String WhereClause =' where Id = \''+currentworkOrder +'\'';
            QueryString = QueryString+WhereClause;            
            SVMXC__Service_Order__c SWO = DataBase.query(QueryString);            
            List<AssignedToolsTechnicians__c> TechnicianIds=[Select id, Name ,TechnicianEquipment__c, LatestNotificationDate__c From AssignedToolsTechnicians__c where WorkOrder__c = :WorkOrderId and Status__c =:ACCEPTED  limit 10];
            List<String> lstids= new List<String>();
            string TNames = ' ';
            System.debug('###RHA technichian list size = '+ TechnicianIds.size());          
            Set<id> gmidset = new Set<id>();
            Map<id,SVMXC__Service_Group_Members__c>  gidrecmap = new map<id,SVMXC__Service_Group_Members__c>();
            Map<id,String>  atTechname = new map<id,String>();
            for (AssignedToolsTechnicians__c TechName :TechnicianIds)
            {
                gmidset.add(TechName.TechnicianEquipment__c);
            }           
            if(gmidset!= null && gmidset.size()>0){
                List<SVMXC__Service_Group_Members__c> gmList = [select id, Name,SVMXC__Salesforce_User__r.TimeZoneSidKey, SVMXC__Email__c, SVMXC__Phone__c, Send_Notification__c, LatestNotificationDate__c, SVMXC__Service_Group__r.RecordTypeId from SVMXC__Service_Group_Members__c where id in :gmidset ];
                if(gmList!= null && gmList.size()>0)
                gidrecmap.putAll(gmList);
            }
            for (AssignedToolsTechnicians__c attoolsobj :TechnicianIds)
            {
                if(gidrecmap.containskey(attoolsobj.TechnicianEquipment__c))
                {
                    SVMXC__Service_Group_Members__c obj = gidrecmap.get(attoolsobj.TechnicianEquipment__c);
                    String tname= obj.name;
                    if(atTechname.containskey(attoolsobj.id))
                    {
                        String exName = atTechname.get(attoolsobj.id);
                        atTechname.put(attoolsobj.id,exName+'  ' + tname);                      
                    }
                    else{
                        atTechname.put(attoolsobj.id, tname);
                    }
                }

            }
            List<AssignedToolsTechnicians__c> attListToUpdate =new List<AssignedToolsTechnicians__c>();
            // Send notification to each Technician
                for (AssignedToolsTechnicians__c Tech1 :TechnicianIds)
                {
                    
                    TNames = TNames  +' '+ atTechname.get(Tech1.id) + '.';
                    //String FSENames = FSENames +'      FSR scheduled for the Work Order: ' + atTechname.get(Tech1.id) + '.';
                }
            
                String FSENames = '      FSR scheduled for the Work Order: ' + TNames + '.';
                for (AssignedToolsTechnicians__c Tech :TechnicianIds)
                {
                    SVMXC__Service_Group_Members__c technician =gidrecmap.get(Tech.TechnicianEquipment__c);
                    //String FSENames = '      FSR scheduled for the Work Order: ' + atTechname.get(Tech.id) + '.';
                    System.debug('\n hari log : '+technician.SVMXC__Service_Group__r.RecordTypeId);
                    System.debug('\n hari log : '+RecordTypeSt);
                    if (technician.SVMXC__Service_Group__r.RecordTypeId == RecordTypeSt)
                    {
                        //Go to Email notification
                        if (technician.Send_Notification__c == 'Email')
                        {
                            if (technician.SVMXC__Email__c != null){
                                System.debug('###RHA email method enter Fired'+ technician.SVMXC__Email__c);
                                Email_Notification(SWO, technician,  FSENames);
                                Tech.LatestNotificationDate__c = datetime.now();
                                SWO.TECH_IsAlertFSESent__c = True;
                                //update Tech;
                                attListToUpdate.add(Tech);
                                
                            }
                        }
                        //Go to SMS notification
                        if(technician.Send_Notification__c == 'SMS')
                        {
                            if (technician.SVMXC__Phone__c != null){
                                System.debug('###RHA SMS method enter Fired'+ technician.SVMXC__Phone__c);
                                SMS_Notification(SWO, technician);
                                Tech.LatestNotificationDate__c = datetime.now();
                                SWO.TECH_IsAlertFSESent__c = True;

                                //update Tech;
                                attListToUpdate.add(Tech);

                            }
                        }
                        System.debug('\n hari log : '+technician.Send_Notification__c);
                        // Goto Both Email and SMS notifications
                        if (technician.Send_Notification__c == 'SMS and Email')
                        {
                            if (technician.SVMXC__Phone__c != null && technician.SVMXC__Email__c != null){
                                System.debug('###RHA SMS AND Email method enter Fired'+ technician.SVMXC__Phone__c);
                                SMS_Notification(SWO, technician);
                                Email_Notification(SWO, technician,  FSENames);
                                Tech.LatestNotificationDate__c = datetime.now();
                                SWO.TECH_IsAlertFSESent__c = True;

                                //update Tech;
                                attListToUpdate.add(Tech);

                            }

                        }

                        
                    }
                }
                if(attListToUpdate!= null && attListToUpdate.size()>0)
                update attListToUpdate;
                
                update SWO;
            
        }
        catch(Exception ex)
        {
            System.debug(' Exception occured   '+ ex.getMessage());
        }   


    }
    // send SMS notification to the technicians
    private static void SMS_Notification(SVMXC__Service_Order__c currentworkOrder, SVMXC__Service_Group_Members__c technician){

        System.debug('\n hari log : ');
        boolean executeFromtest = false;
        Datetime schedultedDate = currentworkOrder.SVMXC__Scheduled_Date_Time__c;
        string formattedSchedultedDate = schedultedDate .format('dd/MM/yyyy HH:mm');
        String WorkOrderId = currentworkOrder.Id;
        SVMXC__Service_Order__c SWO = currentworkOrder;    
        
        //String  templateText = 'Dear FSR a new work order has been assigned to you,'+ '\r\n';
          String  templateText = ' ';
          if (SWO.SVMXC__Company__r.Name != null){
            templateText = templateText + '      Customer Name: '+ SWO.SVMXC__Company__r.Name + '.'+ '\r\n'; 
        }
        templateText = templateText +'      Work Order Type :'+ SWO.SVMXC__Order_Type__c+ '.'+ '\r\n';
        if (SWO.WorkOrderSubType__c != null){
            templateText = templateText + '      Work Order Sub Type : '+ SWO.WorkOrderSubType__c + '.'+ '\r\n'; 
        }
        if  (SWO.SVMXC__Contact__r.Name != null ){
            templateText = templateText + '      Contact Name: '+ SWO.SVMXC__Contact__r.Name + ','+ '\r\n'; 
        }
        if  (SWO.SVMXC__Contact__r.WorkPhone__c != null ){
            templateText = templateText + '      Contact phone number: ' + SWO.SVMXC__Contact__r.WorkPhone__c  + '.'+ '\r\n'; 
        }
        templateText = templateText + '      Work Order Number: ' + currentworkOrder.Name + '.'+ '\r\n';
        if(SWO.CustomerTimeZone__c  != technician.SVMXC__Salesforce_User__r.TimeZoneSidKey)
        {
            templateText = templateText + '      Location scheduled date time: '+ SWO.TECH_CustomerScheduledDateTimeECH__c + '.'+ '\r\n';
            templateText = templateText + '      FSR local scheduled date time: '+ SWO.TECH_FSRScheduledDateTime__c + '.'+ '\r\n';
        }
        else{
            templateText = templateText + '      Scheduled Date Time: '+ SWO.TECH_CustomerScheduledDateTimeECH__c + '.'+ '\r\n';
        }
        //templateText = templateText + '      Work Order Number: ' + technician.Name + '.'+ '\r\n';
        if (SWO.SVMXC__Site__c != null){
            templateText = templateText + '      Customer Location: '+ SWO.SVMXC__Site__r.Name + '.'+ '\r\n'; 
        } 
        if (SWO.SVMXC__Service_Contract__c != null){
            templateText = templateText + '      Service Contract: '+ SWO.SVMXC__Service_Contract__r.Name + '.'+ '\r\n'; 
            templateText = templateText + '      Service Contract Type: '+ SWO.SVMXC__Service_Contract__r.BOContractType__c + '.'+ '\r\n'; 
        } 
        if (SWO.SVMXC__Company__r.Type != null && SWO.SVMXC__Company__r.Type != ''){
            templateText = templateText + '      Key Account Type: '+ SWO.SVMXC__Company__r.Type + '.'+ '\r\n'; 
        }                
                               
        // SMS sender name
        String senderName = currentworkOrder.Name;//SenderId assing to user by default it is'WorkOrder Name'
        // SMS sender ID
        String senderId = currentworkOrder.Id;
        
        System.debug('###RHA Message: SMS text =  '+ templateText); 
            
        System.debug('###RHA Message: ENTER THE SEND SMS');
            smagicbasic__smsMagic__c smsObject = new smagicbasic__smsMagic__c();
            
            smsObject.smagicbasic__SenderId__c = senderId;
            smsObject.smagicbasic__PhoneNumber__c= technician.SVMXC__Phone__c;
            smsObject.smagicbasic__Name__c = currentworkOrder.Name; 
            smsObject.smagicbasic__ObjectType__c = 'Technician';
            smsObject.smagicbasic__disableSMSOnTrigger__c = 0;
            
            smsObject.smagicbasic__SMSText__c = templateText;
            smsObject.TechnicianEquipment__c = technician.Id;//Added for May Release BR-3074
            
        System.debug('###RHA Message: SMS object =  sender:'+ smsObject.smagicbasic__SenderId__c+ '; Phone'+ smsObject.smagicbasic__PhoneNumber__c + ';Name: ' + smsObject.smagicbasic__Name__c + ';' + smsObject.smagicbasic__ObjectType__c + ';Message :' + smsObject.smagicbasic__SMSText__c);
        
        if (IsTesting(executeFromtest) == false){ //do not send the message if the executeFromtest parameter = true
            if (smsObject != null){
                try{
                //technician.LatestNotificationDate__c = Date.today();
                System.debug('###RHA LatestNotificationDate'+ technician.LatestNotificationDate__c);
                insert smsObject;
                // upsert technician;
                }catch(Exception e){
                          System.debug('----- ###RHA Send SMS has failed');
                        }
             }
        }
    }

    // send Email notification to the technicians
    private static void Email_Notification(SVMXC__Service_Order__c currentworkOrder, SVMXC__Service_Group_Members__c technician,  String FSENames){
    System.debug('\n hari log : ');
        boolean executeFromtest = false; 
        Datetime schedultedDate = currentworkOrder.SVMXC__Scheduled_Date_Time__c;
        string formattedSchedultedDate = schedultedDate .format('dd/MM/yyyy HH:mm');
        //string OrgUrl = System.Label.FSEAlertWorkOrderURL;
        String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + currentworkOrder.Id;
        String WorkOrderId = currentworkOrder.Id;
       
        SVMXC__Service_Order__c SWO = currentworkOrder;
        
       
       
        //String  templateText = 'A new work order has been assigned to you:'+ '\r\n';
        
        String  templateText = 'Dear ' + technician.Name +','+'\r\n' + '      A new work order has been assigned to you.'+ '\r\n';                       
        if (SWO.SVMXC__Company__r.Name != null){
            templateText = templateText + '      Customer Name: '+ SWO.SVMXC__Company__r.Name + '.'+ '\r\n'; 
        }
        templateText = templateText +'      Work Order Type :'+ SWO.SVMXC__Order_Type__c+ '.'+ '\r\n';
        if (SWO.WorkOrderSubType__c != null){
            templateText = templateText + '      Work Order Sub Type : '+ SWO.WorkOrderSubType__c + '.'+ '\r\n'; 
        }
        if  (SWO.SVMXC__Contact__r.Name != null ){
            templateText = templateText + '      Contact Name: '+ SWO.SVMXC__Contact__r.Name + ','+ '\r\n'; 
        }
        if  (SWO.SVMXC__Contact__r.WorkPhone__c != null ){
            templateText = templateText + '      Contact phone number: ' + SWO.SVMXC__Contact__r.WorkPhone__c  + '.'+ '\r\n'; 
        }
        templateText = templateText + '      Work Order Number: ' + currentworkOrder.Name + '.'+ '\r\n';
        if(SWO.CustomerTimeZone__c  != technician.SVMXC__Salesforce_User__r.TimeZoneSidKey)
        {
            templateText = templateText + '      Location scheduled date time: '+ SWO.TECH_CustomerScheduledDateTimeECH__c + '.'+ '\r\n';
            templateText = templateText + '      FSR local scheduled date time: '+ SWO.TECH_FSRScheduledDateTime__c + '.'+ '\r\n';
        }
        else{
            templateText = templateText + '      Scheduled Date Time: '+ SWO.TECH_CustomerScheduledDateTimeECH__c + '.'+ '\r\n';
        }
        //templateText = templateText + '      Work Order Number: ' + technician.Name + '.'+ '\r\n';
        if (SWO.SVMXC__Site__c != null){
            templateText = templateText + '      Customer Location: '+ SWO.SVMXC__Site__r.Name + '.'+ '\r\n'; 
        } 
        if (SWO.SVMXC__Service_Contract__c != null){
            templateText = templateText + '      Service Contract: '+ SWO.SVMXC__Service_Contract__r.Name + '.'+ '\r\n'; 
            templateText = templateText + '      Service Contract Type: '+ SWO.SVMXC__Service_Contract__r.BOContractType__c + '.'+ '\r\n'; 
        } 
        if ( SWO.SVMXC__Company__r.Type != null && SWO.SVMXC__Company__r.Type != ''){
            templateText = templateText + '      Key Account Type: '+ SWO.SVMXC__Company__r.Type + '.'+ '\r\n'; 
        }
        templateText = templateText + FSENames + '\r\n'+ 
                                        '      Link: ' + fullRecordURL + '\r\n'+
                                        'Regards.'+ '\r\n'+
                                        '*[This is an automated message - please do not reply]'+ '\r\n';
                                        
       // End Email Message template
        
        Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
        
        ID orgWideEmailAddressId = System.Label.CLAPR15SRV79;
        
        mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
        mail.setToAddresses(new String [] {technician.SVMXC__Email__c});
        mail.setSubject('Work Order Notification');
        mail.setPlainTextBody(templateText);
        if (IsTesting(executeFromtest) == false){ //do not send the message if the executeFromtest parameter = true
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            System.debug('###RHA LatestNotificationDate'+ technician.LatestNotificationDate__c);
         }
         System.debug('\n hari log : '+mail);
    }

    public static void QuickRescheduleNotification(SVMXC__Service_Order__c newWOrder,SVMXC__Service_Order__c oldWOrder,integer notif) {
            String WorkOrderId = newWOrder.Id;
            String ACCEPTED = Label.CLDEC13SRV06;
            //RecordType RecordTypeTechId = [Select Id From RecordType where  DeveloperName = 'Technician' and SobjectType = 'SVMXC__Service_Group__c'];
            String RecordTypeIdst = system.label.CLAPR15SRV78;
            Account acc =  new Account();
            if(newWOrder.SVMXC__Company__c != null)
             acc = [Select id, name from Account where id = :newWOrder.SVMXC__Company__c];
            
            List<AssignedToolsTechnicians__c> TechnicianIds=[Select id, Name ,TechnicianEquipment__c, LatestNotificationDate__c From AssignedToolsTechnicians__c where WorkOrder__c = :WorkOrderId and Status__c =:ACCEPTED  ];
            List<String> lstids= new List<String>();
            Set<id> ServiceGroupMemberIds = new Set<id>();
            string TNames = ' ';
            System.debug('###RHA technichian list size = '+ TechnicianIds.size());
            List<SVMXC__Service_Group_Members__c> serviceGroupMemList = new List<SVMXC__Service_Group_Members__c>();
            Map<id,SVMXC__Service_Group_Members__c> serviceGroupMemMap = new Map<id,SVMXC__Service_Group_Members__c>();
            for (AssignedToolsTechnicians__c attObj :TechnicianIds)
            {
                ServiceGroupMemberIds.add(attObj.TechnicianEquipment__c);
            }
            
            if(ServiceGroupMemberIds != null && ServiceGroupMemberIds.size()>0)
            {
                    serviceGroupMemList = [select id,SVMXC__Salesforce_User__r.TimeZoneSidKey, Name, SVMXC__Email__c, SVMXC__Phone__c, Send_Notification__c, LatestNotificationDate__c, SVMXC__Service_Group__r.RecordTypeId from SVMXC__Service_Group_Members__c where id in :ServiceGroupMemberIds];
                    serviceGroupMemMap.putAll(serviceGroupMemList);
            }
            
            // Get the list Name of all technicians that are involved in the Work Order
            for (SVMXC__Service_Group_Members__c sgmObj :serviceGroupMemMap.values())
            {
               
                TNames = TNames  +'  ' + sgmObj.Name;
            }
            
            // String that concatenate all the technicians list.
            String FSENames = '      FSR scheduled for the Work Order: ' + TNames + '.';
            
             // Send notification to each Technician
            for (AssignedToolsTechnicians__c Tech :TechnicianIds)
            {
            
                SVMXC__Service_Group_Members__c technician ;
                if(serviceGroupMemMap.containsKey(Tech.TechnicianEquipment__c)){
                
                    technician = serviceGroupMemMap.get(Tech.TechnicianEquipment__c);
                
                }
                        //build the email notification list
                        
                        //global if record type = 'Technician'
                if (technician.SVMXC__Service_Group__r.RecordTypeId == RecordTypeIdst)
                {
                    //Go to Email notification
                    if (technician.Send_Notification__c == 'Email')
                    {
                        if (technician.SVMXC__Email__c != null){
                            System.debug('###RHA email method enter Fired'+ technician.SVMXC__Email__c);
                            if(notif==1)
                            {
                                Email_RSNotification(newWOrder, oldWOrder, technician,acc,1);
                            }
                            else if(notif==2)
                            {
                                Email_RSNotification(newWOrder, oldWOrder, technician,acc,2);
                            }
                            
                            Tech.LatestNotificationDate__c = datetime.now();                    
                            //update Tech;
                            
                            }
                    }
                    
                    //Go to SMS notification
                    if(technician.Send_Notification__c == 'SMS')
                    {
                        if (technician.SVMXC__Phone__c != null){
                            System.debug('###RHA SMS method enter Fired'+ technician.SVMXC__Phone__c);
                            if(notif==1)
                            {
                                SMS_RSNotification(newWOrder, oldWOrder, technician,acc,1);
                            }
                            else if(notif==2)
                            {
                                SMS_RSNotification(newWOrder, oldWOrder, technician,acc,2);
                            }
                            //SMS_Notification(oldWOrder, technician);
                            Tech.LatestNotificationDate__c = datetime.now();                    
                            //update Tech;
                            
                            }
                    }
                    // Goto Both Email and SMS notifications
                    if (technician.Send_Notification__c == 'SMS and Email')
                    {
                        if (technician.SVMXC__Phone__c != null && technician.SVMXC__Email__c != null){
                            System.debug('###RHA SMS AND Email method enter Fired'+ technician.SVMXC__Phone__c);
                            if(notif==1)
                            {
                                SMS_RSNotification(newWOrder, oldWOrder, technician,acc,1);
                                Email_RSNotification(newWOrder, oldWOrder, technician,acc,1);
                            }
                            else if(notif==2)
                            {
                                SMS_RSNotification(newWOrder, oldWOrder, technician,acc,2);
                                Email_RSNotification(newWOrder, oldWOrder, technician,acc,2);
                            }
                            Tech.LatestNotificationDate__c = datetime.now();                    
                            //update Tech;
                            
                           }
                    }
                    
                }
            }
            
            update TechnicianIds;
        
    }
        // send SMS notification to the technicians when Work order reschedule 
    private static void SMS_RSNotification(SVMXC__Service_Order__c currentworkOrder,SVMXC__Service_Order__c previousworkOrder, SVMXC__Service_Group_Members__c technician,Account acc,integer notiftype){
       
        String  templateText;
        String fullRecordURL2 = URL.getSalesforceBaseUrl().toExternalForm() + '/' + currentworkOrder.Id;
         Map<String, String> picklistMap = getPickList();

        if(notiftype ==1)
        {
            templateText = 'Dear ' + technician.Name +',' + '\r\n'; 
                        
            templateText = templateText + 'Due to ' +currentworkOrder.RescheduleReason__c +', the work order '+currentworkOrder.name;
            if(acc!=null && acc.id!= null)
            templateText = templateText+' related to Customer '+acc.name;
            
            if(previousworkOrder.CustomerTimeZone__c  != technician.SVMXC__Salesforce_User__r.TimeZoneSidKey)
            {
                 String customerTimeZone ;
                String TechnicianTimeZone;
                if(picklistMap.containsKey(previousworkOrder.CustomerTimeZone__c))
                    customerTimeZone =  picklistMap.get(previousworkOrder.CustomerTimeZone__c);
                if(picklistMap.containsKey(technician.SVMXC__Salesforce_User__r.TimeZoneSidKey))
                    TechnicianTimeZone =  picklistMap.get(technician.SVMXC__Salesforce_User__r.TimeZoneSidKey);
                templateText = templateText + 'with intervention initially planned on '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c+', '+customerTimeZone +  '\r\n';
                
                templateText = templateText + ' (& '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c+''+TechnicianTimeZone+')';

            }
            else{
                
                templateText = templateText + 'with intervention initially planned on '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c + '.'+ '\r\n';
               
            }
            
            templateText = templateText + 'has been rescheduled.';
            templateText = templateText+' \r\n';            
            templateText = templateText + 'Please refer to the work order in reference for details.';
            templateText = templateText+' \r\n';
            templateText = templateText+' \r\n'+
                            '      Link: ' + fullRecordURL2 + '\r\n';
            templateText = templateText+'Thank you for taking note of this scheduling changes.';
            
           
        }
        else if(notiftype ==2)
        {
            templateText = 'Dear ' + technician.Name +',' + '\r\n'; 
                        
            templateText = templateText + 'Due to ' +currentworkOrder.RescheduleReason__c +', the work order '+currentworkOrder.name;
            if(acc!=null && acc.id!= null)
            templateText = templateText+' related to Customer '+acc.name;           
            if(previousworkOrder.CustomerTimeZone__c  != technician.SVMXC__Salesforce_User__r.TimeZoneSidKey)
            {
                 String customerTimeZone ;
                String TechnicianTimeZone;
                if(picklistMap.containsKey(previousworkOrder.CustomerTimeZone__c))
                    customerTimeZone =  picklistMap.get(previousworkOrder.CustomerTimeZone__c);
                if(picklistMap.containsKey(technician.SVMXC__Salesforce_User__r.TimeZoneSidKey))
                    TechnicianTimeZone =  picklistMap.get(technician.SVMXC__Salesforce_User__r.TimeZoneSidKey);
                templateText = templateText + 'with intervention initially planned on '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c+', '+customerTimeZone +  '\r\n';             
                templateText = templateText + ' (& '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c+''+TechnicianTimeZone+')';


            }
            else{
               
                templateText = templateText + 'with intervention initially planned on '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c + '.'+ '\r\n';
                
            }

            
            templateText = templateText + 'has been rescheduled.';
            templateText = templateText+' \r\n';            
            templateText = templateText + 'Please refer to the work order in reference for details.';
            templateText = templateText+' \r\n';
            templateText = templateText+' \r\n'+
                            '      Link: ' + fullRecordURL2 + '\r\n';
            templateText = templateText+'Thank you for taking note of this scheduling changes.';
        }
         // End Email Message template
                                         
                               
        // SMS sender name
        String senderName = currentworkOrder.Name;//SenderId assing to user by default it is'WorkOrder Name'
        // SMS sender ID
        String senderId = currentworkOrder.Id;
        
        System.debug('###RHA Message: SMS text =  '+ templateText); 
            
        System.debug('###RHA Message: ENTER THE SEND SMS');
            smagicbasic__smsMagic__c smsObject = new smagicbasic__smsMagic__c();
            
            smsObject.smagicbasic__SenderId__c = senderId;
            smsObject.smagicbasic__PhoneNumber__c= technician.SVMXC__Phone__c;
            smsObject.smagicbasic__Name__c = currentworkOrder.Name; 
            smsObject.smagicbasic__ObjectType__c = 'Technician';
            smsObject.smagicbasic__disableSMSOnTrigger__c = 0;
            
            smsObject.smagicbasic__SMSText__c = templateText;
            smsObject.TechnicianEquipment__c = technician.Id;//Added for May Release BR-3074
            
        System.debug('###RHA Message: SMS object =  sender:'+ smsObject.smagicbasic__SenderId__c+ '; Phone'+ smsObject.smagicbasic__PhoneNumber__c + ';Name: ' + smsObject.smagicbasic__Name__c + ';' + smsObject.smagicbasic__ObjectType__c + ';Message :' + smsObject.smagicbasic__SMSText__c);
        
        if (!Test.isRunningTest()){ //do not send the message if the executeFromtest parameter = true
            if (smsObject != null){
                try{
                //technician.LatestNotificationDate__c = Date.today();
                System.debug('###RHA LatestNotificationDate'+ technician.LatestNotificationDate__c);
                insert smsObject;
                // upsert technician;
                }catch(Exception e){
                          System.debug('----- ###RHA Send SMS has failed');
                        }
             }
        }
    }
     // send Email notification to the technicians when Work order reschedule 
    private static void Email_RSNotification(SVMXC__Service_Order__c currentworkOrder,SVMXC__Service_Order__c previousworkOrder ,SVMXC__Service_Group_Members__c technician,Account acc,integer notiftype)
    {
        String  templateText;
        String fullRecordURL1 = URL.getSalesforceBaseUrl().toExternalForm() + '/' + currentworkOrder.Id;

        Map<String, String> picklistMap = getPickList();
        if(notiftype ==1)
        {
            templateText = 'Dear ' + technician.Name +',' + '\r\n'; 
                        
            templateText = templateText + 'Due to ' +currentworkOrder.RescheduleReason__c +', the work order '+currentworkOrder.name;
            if(acc!=null && acc.id!= null)
            templateText = templateText+' related to Customer '+acc.name;
           
            if(previousworkOrder.CustomerTimeZone__c  != technician.SVMXC__Salesforce_User__r.TimeZoneSidKey)
            {
                String customerTimeZone ;
                String TechnicianTimeZone;
                if(picklistMap.containsKey(previousworkOrder.CustomerTimeZone__c))
                    customerTimeZone =  picklistMap.get(previousworkOrder.CustomerTimeZone__c);
                if(picklistMap.containsKey(technician.SVMXC__Salesforce_User__r.TimeZoneSidKey))
                    TechnicianTimeZone =  picklistMap.get(technician.SVMXC__Salesforce_User__r.TimeZoneSidKey);


                templateText = templateText + 'with intervention initially planned on '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c+', '+customerTimeZone+ ''+ '\r\n';
                templateText = templateText + ' (& '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c+''+TechnicianTimeZone+')';
               
            }
            else{
                
                templateText = templateText + ' with intervention initially planned on '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c + '.'+ '\r\n';
                
            }
            
            templateText = templateText + 'has been rescheduled.';
            templateText = templateText+' \r\n';            
            templateText = templateText + 'Please refer to the work order in reference for details.';
            templateText = templateText+' \r\n';
            templateText = templateText+' \r\n'+
                            '      Link: ' + fullRecordURL1 + '\r\n';
            templateText = templateText+'Thank you for taking note of this scheduling changes.';
        }
        else if(notiftype ==2)
        {
            templateText = 'Dear ' + technician.Name +',' + '\r\n'; 
                        
            templateText = templateText + 'Due to ' +currentworkOrder.RescheduleReason__c +', the work order'+currentworkOrder.name;
            if(acc!=null && acc.id!= null)
            templateText = templateText+' related to Customer '+acc.name;           
            if(previousworkOrder.CustomerTimeZone__c  != technician.SVMXC__Salesforce_User__r.TimeZoneSidKey)
            {
                String customerTimeZone ;
                String TechnicianTimeZone;
                if(picklistMap.containsKey(previousworkOrder.CustomerTimeZone__c))
                    customerTimeZone =  picklistMap.get(previousworkOrder.CustomerTimeZone__c);
                if(picklistMap.containsKey(technician.SVMXC__Salesforce_User__r.TimeZoneSidKey))
                    TechnicianTimeZone =  picklistMap.get(technician.SVMXC__Salesforce_User__r.TimeZoneSidKey);
                templateText = templateText + 'with intervention initially planned on '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c+', '+customerTimeZone + '\r\n';
                templateText = templateText + ' (& '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c+''+TechnicianTimeZone+')';
            }
            else{
                templateText = templateText + ' with intervention initially planned on '+ previousworkOrder.TECH_CustomerScheduledDateTimeECH__c + '.'+ '\r\n';
                
            }           
            
            templateText = templateText + 'has been rescheduled.';
            templateText = templateText+' \r\n';            
            templateText = templateText + 'Please refer to the work order in reference for details.';
            templateText = templateText+' \r\n';
            templateText = templateText+' \r\n'+
                            '      Link: ' + fullRecordURL1 + '\r\n';
            templateText = templateText+'Thank you for taking note of this scheduling changes.';
        }
        
         // End Email Message template
        
        Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
        ID orgWideEmailAddressId = System.Label.CLAPR15SRV79;
        
        mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
        mail.setToAddresses(new String [] {technician.SVMXC__Email__c});
        mail.setSubject('Work Order Notification');
        mail.setPlainTextBody(templateText);
        if (!Test.isRunningTest()){ //do not send the message if the executeFromtest parameter = true
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            System.debug('###RHA LatestNotificationDate'+ technician.LatestNotificationDate__c);
         }
        
    
    }
    public static void UncheckAlertFSE(SVMXC__Service_Order__c currentworkOrder) {
        if (currentworkOrder.TECH_IsAlertFSESent__c == True){
            try{
                String WorkOrderId = currentworkOrder.Id;
                SVMXC__Service_Order__c SWO = [Select Id, TECH_IsAlertFSESent__c from SVMXC__Service_Order__c where Id =:WorkOrderId limit 1];
                SWO.TECH_IsAlertFSESent__c = False;
                upsert SWO;
                }catch(Exception e){
                  System.debug('----- ###RHA IsAlertFSESent');
                }
       }
   }
    
        

}