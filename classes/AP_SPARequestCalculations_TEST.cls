@isTest
private class AP_SPARequestCalculations_TEST
{

    static testMethod void runPositiveTestCases() {
    
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
        SPAThresholdAmount__c thresholdamount1=Utils_SPA.createThresholdAmount('HK_SAP','Free of charge');
        insert thresholdamount1;
  
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        SPAThresholdDiscount__c thresholddiscount1=Utils_SPA.createThresholdDiscount('HK_SAP','Final Multiplier');
        insert thresholddiscount1;
        SPAThresholdDiscount__c thresholddiscount2=Utils_SPA.createThresholdDiscount('HK_SAP','Compensation%');
        insert thresholddiscount2;

        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        
        User spatest= Utils_TestMethods.createStandardUser('spatest');
        insert spatest;
        UserParameter__c param1=Utils_SPA.createUserParameter(salesgroup.id,spatest.id);
        insert param1;
        
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        
        List<SPAQuotaQuarter__c> quotaquarters=Utils_SPA.createQuotaQuarters(quota.Id);
        insert quotaquarters;
        
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount');
        insert lineitem;

        //Free of Charge
        SPARequest__c sparequest1=Utils_SPA.createSPARequest('Free of charge',opp.id);
        insert sparequest1;
        SPARequestLineItem__c lineitem1=Utils_SPA.createSPARequestLineItem(sparequest1.id,'Free of charge');
        insert lineitem1;
        Test.startTest();
        //Advised Discount
        SPARequest__c sparequest2=Utils_SPA.createSPARequest('Advised Discount %',opp.id);
        insert sparequest2;
        SPARequestLineItem__c lineitem2=Utils_SPA.createSPARequestLineItem(sparequest2.id,'Advised Discount %');
        insert lineitem2;
        
         insert Utils_SPA.createPricingDeskMembers(sparequest.BackOfficeSystemID__c,lineitem.LocalPricingGroup__c);
         sparequest=[select SPAQuotaQuarter__r.CurrencyIsoCode,SPAQuotaQuarter__r.QuotaRemaining__c,Account__c,ApprovalStatus__c,Approver1__c,Approver2__c,Approver3__c,BackOfficeCustomerNumber__c,BackOfficeSystemID__c,CreatedById,CreatedDate,CurrencyIsoCode,CustomerPricingGroup__c,DealSize__c,EndDate__c,ExceptionInformations__c,Id,Name,NetDiscountAmount__c,Opportunity__c,OwnerId,QuotaExceeded__c,RecordTypeId,RequiredApprovalLevel__c,Salesperson__c,SPAQuotaQuarter__c,SPASalesGroup__c,StartDate__c,(select ApprovalComments__c,Business__c,CommercialReference__c,CurrencyIsoCode,ListPriceLineAmount__c,ListPrice__c,LocalPricingGroup__c,NetLineAmount__c,ProductLineManager__c,RecommendationStatus__c,AdditionalDiscount__c,FinalMultiplier__c,NetPrice__c,RequestedType__c,RequestedValue__c,RequiredApprovalLevel__c,StandardDiscount__c,StandardMultiplier__c,StandardPriceLineAmount__c,StandardPrice__c,TargetQuantity__c from SPARequestLineItems__r where id=:lineitem.id ) from SPARequest__c where id=:sparequest.id];

        sparequest.ApprovalStatus__c='Approval in Progress';
        sparequest.ownerid=spatest.id;
        sparequest.TECH_ApprovalStep__c=2;
        update sparequest;
        
        LegacyAccount__c legacyAccount=new LegacyAccount__c(Account__c=sparequest.account__c,LegacyAccountType__c='PA',LegacyName__c='HK_SAP');
        insert legacyAccount;
        sparequest.TECH_ApprovalStep__c=3;
        sparequest.ApprovalStatus__c='Approved';
        update sparequest; 
        sparequest.TECH_ApprovalStep__c=1;
        update sparequest; 
        sparequest.TECH_ApprovalStep__c=4;
        update sparequest; 
        opp.StageName=System.Label.CLSEP12SLS12; 
        update opp;  
        Set<Id> oppset=new Set<Id>();
        oppset.add(opp.id);
        AP_SPARequestCalculations.updateRelatedQuotaforLostOpportunity(oppset);
        Test.stopTest();          
    }
   
}