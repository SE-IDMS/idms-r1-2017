/*
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  06/12/2016 
*   This class provides methods to send notifications (email, sms) to users 
**/
public class IDMSSendCommunication{
    
    //Send email to user based on templateName
    public static void sendemail(Id UserId,string TemplateName,String appId)
    { 
        //code added to get url on the basis of application name START
        String url = IDMSEmailLinksApp.getEmailLinkApp(appId,2); 
        if(url == null){
            url = Label.CLQ316IDMS131;  
        }
        system.debug('url---'+url);  
        //code added to get url on the basis of application name END
        system.debug('--sendemail---'+TemplateName);
        user u                             = [select id,firstname,contactid from user where id = :userId];     
        String signature                   = IDMSEmailEncryption.EmailEncryption(u);
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        //TemplateName                       = system.Label.CLDEC16IDMS009;
        List<EmailTemplate> lstTemplate;
        lstTemplate                        = [select id,HtmlValue,Subject from emailtemplate where DeveloperName = :TemplateName];
        String subject                     = lstTemplate[0].Subject;
        String firstname                   = u.firstname;  
        Id TemplateId;
        if(lstTemplate  != null && lstTemplate .size() >0){
            TemplateId = lstTemplate[0].id;                
        }
        email.setTemplateId(TemplateId);                               
        String htmlBody                 = lstTemplate[0].HtmlValue;
        htmlBody                        = htmlBody.replace('{!firstname}',firstname); 
        htmlBody                        = htmlBody.replace('{!url}',url); 
        if(String.isNotBlank('{!AppId}') && string.isNotBlank(appId))                  
            htmlBody = htmlBody.replace('{!AppId}', appId);
        else
            htmlBody = htmlBody.replace('{!AppId}', '');     
        
        if(String.isNotBlank('{!sig}') && string.isNotBlank(signature))                        
            htmlBody = htmlBody.replace('{!sig}', signature);
        else
            htmlBody = htmlBody.replace('{!sig}', '');         
        
        if(String.isNotBlank('{!uid}') && string.isNotBlank(UserId))    
            htmlBody = htmlBody.replace('{!uid}', UserId);
        else
            htmlBody = htmlBody.replace('{!uid}', '');       
        
             
        htmlBody = htmlBody.replace('{!action}', 'InitPwd');
        email.setHtmlBody(htmlBody);
        email.setTargetObjectId(u.id);            
        email.saveAsActivity = false;
        email.setSubject(subject);  
        Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
        system.debug('---email---'+email);
    }
    
    //send email to user created with password
    public static void sendEmailCreateUserPasswordSet(Id UserId,string TemplateName,String appId,String signature)
    {             
        //code added to get url on the basis of application name START
        String url = IDMSEmailLinksApp.getEmailLinkApp(appId,0); 
        if(url == null){
            url = Label.CLQ316IDMS131;  
        }
        system.debug('url---'+url);  
        //code added to get url on the basis of application name END
        system.debug('--sendemail---'+TemplateName);
        user u             = [select id,firstname,contactid from user where id = :userId];   
        system.debug('user---'+u);    
        String firstname   = u.firstname;  
        //code for getting email templatename for app 
        //TemplateName       = system.Label.CLDEC16IDMS010; 
        /** email template translation commentted by onkar.
        IDMSApplicationMapping__c appMap;
        appMap             = IDMSApplicationMapping__c.getInstance(appid);
        if(appMap != null){
            TemplateName = (String)appMap.get('TemplateForUserCreate__c');
            system.debug('TemplateName---'+TemplateName);  
        }
        **/
        // String signature;
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<EmailTemplate> lstTemplate;
        lstTemplate                        = [select id,HtmlValue,Subject from emailtemplate where DeveloperName = :TemplateName];
        String subject                     = lstTemplate[0].Subject;
        Id TemplateId;
        if(lstTemplate  != null && lstTemplate .size() >0){
            TemplateId = lstTemplate[0].id;                
        }
        email.setTemplateId(TemplateId);                               
        String htmlBody                    = lstTemplate[0].HtmlValue;
        htmlBody                           = htmlBody.replace('{!firstname}',firstname); 
        htmlBody                           = htmlBody.replace('{!url}',url); 
        if(String.isNotBlank('{!AppId}') && string.isNotBlank(appId))                  
            htmlBody = htmlBody.replace('{!AppId}', appId);
        else
            htmlBody = htmlBody.replace('{!AppId}', '');     
        
        if(String.isNotBlank('{!sig}') && string.isNotBlank(signature))                        
            htmlBody = htmlBody.replace('{!sig}', signature);
        else
            htmlBody = htmlBody.replace('{!sig}', '');         
        
        if(String.isNotBlank('{!uid}') && string.isNotBlank(UserId))    
            htmlBody = htmlBody.replace('{!uid}', UserId);
        else
            htmlBody = htmlBody.replace('{!uid}', '');       
        
             
        htmlBody = htmlBody.replace('{!action}', 'InitPwd');
        email.setHtmlBody(htmlBody);
        email.setSubject(subject);   
        email.setTargetObjectId(u.id);            
        email.saveAsActivity                = false;
        Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
        system.debug('---email---'+email);
    }
    
    //send email to user created without password
    public static void sendEmailCreateUserWithPwd(Id UserId,string TemplateName,String appId,String signature)
    {
        system.debug('--sendemail---'+TemplateName);
        user u                             = [select id,firstname,contactid from user where id = :userId];   
        system.debug('user---'+u);    
        String firstname                   = u.firstname;  
        String url                         = Label.CLJUN16IDMS143;   
        //code for getting email templatename for app 
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<EmailTemplate> lstTemplate;
        lstTemplate                        = [select id,HtmlValue,Subject from emailtemplate where DeveloperName = :TemplateName];
        String subject                     = lstTemplate[0].Subject;
        Id TemplateId;
        if(lstTemplate  != null && lstTemplate .size() >0){
            TemplateId = lstTemplate[0].id;                
        }
        email.setTemplateId(TemplateId);                               
        String htmlBody         = lstTemplate[0].HtmlValue;
        htmlBody                = htmlBody.replace('{!firstname}',firstname); 
        htmlBody                = htmlBody.replace('{!url}',url); 
        if(String.isNotBlank('{!AppId}') && string.isNotBlank(appId))                  
            htmlBody            = htmlBody.replace('{!AppId}', appId);
        else
            htmlBody            = htmlBody.replace('{!AppId}', '');     
        
        if(String.isNotBlank('{!sig}') && string.isNotBlank(signature))                        
            htmlBody            = htmlBody.replace('{!sig}', signature);
        else
            htmlBody            = htmlBody.replace('{!sig}', '');         
        
        if(String.isNotBlank('{!uid}') && string.isNotBlank(UserId))    
            htmlBody            = htmlBody.replace('{!uid}', UserId);
        else
            htmlBody            = htmlBody.replace('{!uid}', '');       
        
             
        htmlBody        = htmlBody.replace('{!action}', 'InitPwd');
        email.setHtmlBody(htmlBody);
        email.setSubject(subject);   
        email.setTargetObjectId(u.id);            
        email.saveAsActivity                = false;
        Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
        system.debug('---email---'+email);
    }
    
    //Send SMS to user
    public static  void sendSMS(User u){
        smagicbasic__smsMagic__c smsObject              = new smagicbasic__smsMagic__c();   
        smsObject.smagicbasic__SenderId__c              = u.id;
        smsObject.smagicbasic__PhoneNumber__c           = u.mobilephone;
        smsObject.smagicbasic__Name__c                  = u.name;
        
        // here is to mention that we send an SMS to a IDMS User. We should use this field to different SMS between IDMS and other streams
        smsObject.smagicbasic__ObjectType__c            = 'User';
        smsObject.smagicbasic__disableSMSOnTrigger__c   = 0;
        
        // Here you assign the SMS content that you have generated previouly
        smsObject.smagicbasic__SMSText__c               = Label.CLDEC16IDMS006 +u.IDMSPinVerification__c + Label.CLDEC16IDMS007;
        // Here we assign the id of the User
        smsObject.smagicbasic__User__c                  = u.id;
        
        if(!test.isrunningtest()){
            if (smsObject != null){
                try{
                    insert smsObject;
                    system.debug('sms has been created:'+smsObject.id+'for phone no:'+smsObject.smagicbasic__PhoneNumber__c);
                }catch(Exception e){
                    System.debug('----- ###IDMS Send SMS has failed');
                }
            }
        }
    }
    
    //Send email to reset user's password
    Public boolean sendEmailResetPassword(Id id,String appId){
        
        user usr                            = [select id from user where id =:id];
        String signature                    = IDMSEmailEncryption.EmailEncryption(usr);
        Messaging.SingleEmailMessage email  = new Messaging.SingleEmailMessage();
        try{ 
            String emailtempid              = [select id from emailtemplate where name=:Label.CLQ316IDMS039 limit 1].id;
            EmailTemplate emailTemplate     = [select Id, Subject, HtmlValue, Body from EmailTemplate where Id =: emailtempid];
            String subject                  = emailTemplate.Subject;
            subject                         = subject.replace('{!subject}', 'Password reset email');
            String htmlBody                 = emailTemplate.HtmlValue;
            if(appId != null || appId != '') // Ranjith
                htmlBody                    = htmlBody.replace('{!AppId}', appId);
            else
                htmlBody                    = htmlBody.replace('{!AppId}', '');
            
            htmlBody                        = htmlBody.replace('{!sig}', signature);
            htmlBody                        = htmlBody.replace('{!uid}', id);
            htmlBody                        = htmlBody.replace('{!action}', 'SetUserPwd'); 
            email.setTemplateId(emailtempid);
            email.setTargetObjectId(id);   
            email.setSubject(subject);         
            email.saveAsActivity = false;
            email.setHtmlBody(htmlBody);
            // Sends the email
            Messaging.SendEmailResult [] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
            system.debug('---email---'+email);
            return true;
        }
        catch(Exception e){
            return false;
        }  
    }
}