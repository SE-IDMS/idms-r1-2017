public class AP1004_PreventiveAction{
    public static void populatePreventiveActionOwner(List <PreventiveAction__c> preventiveActionList){
    //*********************************************************************************
        // Method Name      : populatePreventiveActionOwner
        // Purpose          : To populate Preventive Action Owner from Accountable Organization 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 29th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1004_PreventiveAction  populatePreventiveActionOwner  Started****');
        
        //Variable Declaration Section
        Map<ID,EntityStakeholder__c[]>  stakeholderMap = new Map<Id,EntityStakeholder__c[]>();
        Map<ID,String>  orgRoleMap = new Map<Id,String>();
        
        
        for(PreventiveAction__c prvtveActn : preventiveActionList){
            if(prvtveActn.AccountableOrganization__c != null)
                orgRoleMap.put(prvtveActn.AccountableOrganization__c,Label.CL10056); 
        }
        stakeholderMap = Utils_P2PMethods.retrieveOrganizationUserDetails(orgRoleMap);
        
        for(PreventiveAction__c prvtveActn : preventiveActionList){
            if(prvtveActn.AccountableOrganization__c != null)
                if(stakeholderMap.containsKey(prvtveActn.AccountableOrganization__c))
                prvtveActn.Owner__c = stakeholderMap.get(prvtveActn.AccountableOrganization__c)[0].User__c;   
        }
        
        System.Debug('****AP1004_PreventiveAction  populatePreventiveActionOwner  Finished****');
    }
    
    public static void addPreventiveOwnerToProblemShare(List <PreventiveAction__c> preventiveActionList){
        //*********************************************************************************
        // Method Name      : addPreventiveOwnerToProblemShare
        // Purpose          : To add Preventive Action Owner to Problem Share Object 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 29th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        System.Debug('****AP1004_PreventiveAction addPreventiveOwnerToProblemShare  Started****');
        List<Problem__Share> prblmShareList = new List<Problem__Share>();
        
        for(PreventiveAction__c prvtveActn: preventiveActionList){
            prblmShareList.add(Utils_P2PMethods.createProblemShare(prvtveActn.RelatedProblem__c,prvtveActn.Owner__c,Label.CL10068));
        }
        Utils_P2PMethods.insertPrblmShare(prblmShareList);
        System.Debug('****AP1004_PreventiveAction addPreventiveOwnerToProblemShare  Finished****');
    }
    
    public static void deletePreventiveOwnerFromProblemShare(Map<Id,PreventiveAction__c> preventiveActionOldMap){
        //*********************************************************************************
        // Method Name      : deletePreventiveOwnerFromProblemShare
        // Purpose          : Delete Sharing for Preventive Action owner when Problem Preventive Action Owner is updated 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 29th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1004_PreventiveAction deletePreventiveOwnerFromProblemShare Started****');
        Set<Id> prblmId = new Set<id>();
        Set<Id> prvtveActionOwnerId = new Set<id>();
        List<PreventiveAction__c> oldpreventiveActionList = preventiveActionOldMap.values();
        List<Problem__Share> prblmShareList = new List<Problem__Share>();
        Map <String, Problem__Share> prblmShareMap = new Map<String, Problem__Share>();
        List<Problem__Share> prblmShareListForDelete = new List<Problem__Share>();
        for(PreventiveAction__c prvtveActn: oldpreventiveActionList){
            prblmId.add(prvtveActn.RelatedProblem__c);
            prvtveActionOwnerId.add(prvtveActn.Owner__c);
        }

        prblmShareList = [Select Id, ParentId, UserOrGroupId, AccessLevel,RowCause from  Problem__Share where ParentId IN:prblmId 
                                                                                                            AND UserOrGroupId IN: prvtveActionOwnerId  AND RowCause=:Label.CL10068 ];
        for (Problem__Share prblmShare:prblmShareList ){
        prblmShareMap.put(String.valueOf(prblmShare.ParentId) + String.valueOf(prblmShare.UserOrGroupId) ,prblmShare);
        }
        //Delete the record from Problem Share only, if the Owner is not present in any other Preventive Action
        //The Map "otherPreventiveActionMap" will get all the other Preventive Action associated with the Problem for this validation. 
		Map<String,PreventiveAction__c> otherPreventiveActionMap= new Map<String,PreventiveAction__c>();
		for (PreventiveAction__c  otherPreventiveAction : [Select Id, RelatedProblem__c, Owner__c from  PreventiveAction__c where RelatedProblem__c IN:prblmId 
                                                                                                        AND Id NOT IN: preventiveActionOldMap.KeySet()]) {
			otherPreventiveActionMap.put(String.valueOf(otherPreventiveAction.RelatedProblem__c) + String.valueOf(otherPreventiveAction.Owner__c),otherPreventiveAction);
		}
		
        for(PreventiveAction__c prvtveActn: oldpreventiveActionList){
            if(prblmShareMap.containsKey(String.valueOf(prvtveActn.RelatedProblem__c) + String.valueOf(prvtveActn.Owner__c))){
                if(!(otherPreventiveActionMap.containsKey(String.valueOf(prvtveActn.RelatedProblem__c) + String.valueOf(prvtveActn.Owner__c)))){
                	prblmShareListForDelete.add(prblmShareMap.get(String.valueOf(prvtveActn.RelatedProblem__c) + String.valueOf(prvtveActn.Owner__c)));
                }
            }
        }
        if(prblmShareListForDelete.size()>0){
            Utils_P2PMethods.deletePrblmShare(prblmShareListForDelete);
        }

        System.Debug('****AP1004_PreventiveAction deletePreventiveOwnerFromProblemShare Finished****');
    }
    public static void updatePreventiveOwnerToProblemShare(Map<Id,PreventiveAction__c> preventiveActionOldMap,Map<Id,PreventiveAction__c> preventiveActionNewMap){
        //*********************************************************************************
        // Method Name      : updatePreventiveOwnerToProblemShare
        // Purpose          : To Update Sharing for Preventive Action Owner when Preventive Action Record is updated 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 29th Augest 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Oct - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP1004_PreventiveAction updatePreventiveOwnerToProblemShare Started****');
        
        deletePreventiveOwnerFromProblemShare(preventiveActionOldMap);
         
        List<Problem__Share> prblmShareListForInsert = new List<Problem__Share>();
        List<PreventiveAction__c> preventiveActionList =preventiveActionNewMap.values();
        for(PreventiveAction__c prvtveActn: preventiveActionList){
            prblmShareListForInsert.add(Utils_P2PMethods.createProblemShare(prvtveActn.RelatedProblem__c,prvtveActn.Owner__c,Label.CL10068));
        }
        Utils_P2PMethods.insertPrblmShare(prblmShareListForInsert);
        
        System.Debug('****AP1004_PreventiveAction updatePreventiveOwnerToProblemShare Finished****');
    }
}