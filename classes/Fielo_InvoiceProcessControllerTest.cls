/************************************************************
* Developer: Denis Aranda                                   *
* Type: Test Class                                          *
* Created Date: 9.23.2014                                   *
* Tested Class: Fielo_InvoiceProcessController              *
************************************************************/

@isTest
public with sharing class Fielo_InvoiceProcessControllerTest{
  
    public static testMethod void testUnit1(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(FieloEE__Member__c = false);
        insert deactivate;
        
        FieloDRE__ObjectsToProcess__c objectsToProcess = new FieloDRE__ObjectsToProcess__c(
            Name = 'Fielo Invoice Installer',
            FieloDRE__FieldMember__c = 'F_Invoice__r.F_Member__c',
            FieloDRE__FieldToRuleAppliedMasterLookup__c = 'Id',
            FieloDRE__FieldTotalPoints__c = 'F_PointsInstaller__c',
            FieloDRE__APINameObject__c = 'Fielo_InvoiceDetails__c',
            FieloDRE__RuleAppliedMasterLookup__c = 'F_InvoiceDetail__c',
            FieloDRE__RuleRecordTypeDevName__c = 'Fielo_InvoiceDetailInstaller',
            FieloDRE__FieldProcessed__c = 'F_Processed__c'
        );
        insert objectsToProcess;
        
        FieloEE__Member__c refMem = new FieloEE__Member__c(FieloEE__LastName__c= 'MarcoRT', FieloEE__FirstName__c = 'PoloRT', F_BusinessType__c = 'RT', FieloEE__Approved__c= true );
        insert refMem;
        
        FieloEE__Member__c mem = new FieloEE__Member__c(FieloEE__LastName__c= 'Marco', FieloEE__FirstName__c = 'Polo', F_BusinessType__c = 'SC', FieloEE__ReferalMember__c = refMem.Id);
        insert mem;

        Fielo_Invoice__c invoice1 = new Fielo_Invoice__c(F_InvoiceDate__c = date.today(), F_Member__c = mem.Id, F_Status__c = 'Pending', F_Retailer__c= refMem.Id);
        insert invoice1;

        Fielo_InvoiceProcessController controllerTest = new Fielo_InvoiceProcessController(new ApexPages.StandardController(invoice1));
        delete invoice1;
        controllerTest.doProcess();
        
        Fielo_Invoice__c invoice = new Fielo_Invoice__c(F_InvoiceDate__c = date.today(), F_Member__c = mem.Id, F_Status__c = 'Pending', F_Retailer__c= refMem.Id);
        insert invoice;
        
        Fielo_InvoiceProcessController controller = new Fielo_InvoiceProcessController(new ApexPages.StandardController(invoice));
        controller.doProcess();
        
        Fielo_InvoiceDetails__c detail = new Fielo_InvoiceDetails__c(F_Invoice__c = invoice.Id, Name= '456', F_Price__c=12, F_Volume__c=12, F_SkipEngine__c = false);
        insert detail;
          
        controller.doProcess();
        controller.doConfirm();
        controller.doGoToInv();
        
    }
    
    public static testMethod void testUnit2(){
    
        FieloEE.MockUpFactory.setCustomProperties(false);
        
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(FieloEE__Member__c = false);
        insert deactivate;

        FieloEE__Member__c mem = new FieloEE__Member__c(FieloEE__LastName__c= 'Marco', FieloEE__FirstName__c = 'Polo', F_BusinessType__c = 'SC');
        insert mem;
        
        Fielo_Invoice__c invoice = new Fielo_Invoice__c(F_InvoiceDate__c = date.today(), F_Member__c = mem.Id, F_Status__c = 'Rejected');
        insert invoice;
        
        Fielo_InvoiceProcessController controller = new Fielo_InvoiceProcessController(new ApexPages.StandardController(invoice));
        controller.doProcess();
        
        Fielo_InvoiceDetails__c detail = new Fielo_InvoiceDetails__c(F_Invoice__c = invoice.Id, Name= '456', F_Price__c=12, F_Volume__c=12, F_SkipEngine__c = false);
        insert detail;
       
        controller.doConfirm();

        
    }
    
}