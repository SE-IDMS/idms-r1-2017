//Test class of IdmsSendInvitationCompany Controller
@isTest
class IdmsSendInvitationCompanyControllerTest{
    
    //test method of send invitation company vfp controller
    static testmethod void testIdmsSendInvitationCompanyController(){
        IdmsTermsAndConditionMapping__c custSets=new IdmsTermsAndConditionMapping__c(Name='IDMS',
                                                                                     url__c='http://www2.schneider-electric.com/sites/corporate/en/general/legal-information/terms-of-use.page');
        insert custSets;
        ApexPages.currentPage().getParameters().put('app','IDMS');
        IdmsSendInvitationCompanyController controller=new IdmsSendInvitationCompanyController();
        controller.getCompanyPicklist();
        controller.email='idmstesttoto@accenture.com';
        controller.nextToCompany();
        controller.reset();
        controller.submit();
        controller.submitHome();
        controller.verify();
        ApexPages.currentPage().getParameters().put(Label.CLJUN16IDMS27,'True');
        ApexPages.currentPage().getParameters().put(Label.CLJUN16IDMS28,'True');
        controller.submitHome();
        controller.verify();
        String strPublicKey=controller.publicKey;
        controller.showCaptcha=false;
        controller.submitHome();
        controller.email='12345test';
        controller.submitHome();
    }
}