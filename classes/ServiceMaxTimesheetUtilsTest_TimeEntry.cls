@isTest(SeeAllData=true)
Public class ServiceMaxTimesheetUtilsTest_TimeEntry
{
    static testmethod  void testInsertentry(){
User mgrUserObj = Utils_TestMethods.createStandardUser('TestMgr');
        mgrUserObj.ManagerId = UserInfo.getUserId();
        insert mgrUserObj;
        User userObj = Utils_TestMethods.createStandardUser('Test');
        userObj.ManagerId = mgrUserObj.Id;
        insert userObj;
        
     SVMXC__Service_Group__c team = new SVMXC__Service_Group__c(Name = 'Test team');                
        insert team;
        SVMXC__Service_Group_Members__c tech2 = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = team.Id);
            insert tech2;
        SVMXC_Timesheet__c timesheet= new SVMXC_Timesheet__c(Start_Date__c= system.today());
        insert timesheet;
    SVMXC_Time_Entry__c Timeentry= new SVMXC_Time_Entry__c(Start_Date_Time__c=system.now(),End_Date_Time__c=system.now()+5,Activity__c='QA Feedback',Technician__c=tech2.id,Timesheet__c=timesheet.id);
            insert Timeentry;
            Timeentry.Activity__c='Meetings';
            Timeentry.Start_Date_Time__c=system.now()+1;
            Timeentry.End_Date_Time__c=system.now()+7;
            update Timeentry;
            }
        }