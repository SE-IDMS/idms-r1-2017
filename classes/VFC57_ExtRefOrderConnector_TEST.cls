@isTest
private class VFC57_ExtRefOrderConnector_TEST
{
    static testMethod void VFC57_ExtRefOrderConnectorTestMethod()
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        contact1.Country__c= [Select Id, Name from Country__c Limit 1].Id;
        insert contact1;
               
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        insert Case1;
        
        CSE_ExternalReferences__c ExtRef = new CSE_ExternalReferences__c();
        ExtRef.Case__c = Case1.ID;
        ExtRef.TECH_Account__c = account1.ID;
        insert ExtRef; 
        
        ApexPages.StandardController ExtRefStandardController = new ApexPages.StandardController(ExtRef);
        
        VFC57_ExtRefOrderConnector CaseOrderController = new VFC57_ExtRefOrderConnector(ExtRefStandardController);
        
        VCC07_OrderConnectorController OrderComponentController = new VCC07_OrderConnectorController();
        OrderComponentController.OrderMethods = CaseOrderController.OrderImplementation;
        OrderComponentController.StandardObjectID = ExtRef.Id;
    
        OrderComponentController.getInit();   
        OrderComponentController.SelectedOrder = new OPP_OrderLink__c();     
        OrderComponentController.Action();
        OrderComponentController.cancel();
        
    }
}