/*    Created by - Jyotiranjan Singhlal
      Created For - VFC_RunSpecifiedTestsNew
*/
@IsTest public with sharing class VFC_RunSpecifiedTestsNew_Test {
   public static testMethod void testAPApexTestClass() {
       /*
       Release__c objRelease = new Release__c();
       objRelease.Name='testRelease';
       insert objRelease;  

       List<Component__c> lstComponents = new List<Component__c>();
       Component__c objComponent =new Component__c();
       objComponent.ComponentType__c = 'Apex Class';
       objComponent.FieldName__c = 'AP_TestClass';
       objComponent.IsMigrate__c =true;
       objComponent.Owner__c =UserInfo.getUserId();
       objComponent.Track__c ='CCC';
       objComponent.Type__c ='New';
       objComponent.Release__c = objRelease.Id;
       lstComponents.add(objComponent);
       
       Component__c objComponent1 =new Component__c();
       objComponent1.ComponentType__c = 'Apex Class';
       objComponent1.FieldName__c = 'AP01_OpportunityFieldUpdate';
       objComponent1.IsMigrate__c =true;
       objComponent1.Owner__c =UserInfo.getUserId();
       objComponent1.Track__c ='CCC';
       objComponent1.Type__c ='New';
       objComponent1.Release__c = objRelease.Id;
       lstComponents.add(objComponent1);
       
       insert lstComponents;
       */
       List<APApexTestClass__c> lstApexTestClass = new List<APApexTestClass__c>();
       APApexTestClass__c objApexTestClass = new APApexTestClass__c();
       objApexTestClass.Name = 'AP01_OpportunityFieldUpdate';
       objApexTestClass.TestClass__c = 'AP05_OpportunityTrigger_Test';
       objApexTestClass.Type__c = 'Apex Class';
       objApexTestClass.RunTest__c= true;
       lstApexTestClass.add(objApexTestClass);
       
       APApexTestClass__c objApexTestClass1 = new APApexTestClass__c();
       objApexTestClass1.Name = 'AP_ProductLineAfterInsertHandler';
       objApexTestClass1.TestClass__c = 'AP_ProductLineAfterInsertHandler_TEST';
       objApexTestClass1.Type__c = 'Apex Class';
       objApexTestClass1.RunTest__c= true;
       lstApexTestClass.add(objApexTestClass1);
       
       APApexTestClass__c objApexTestClass2 = new APApexTestClass__c();
       objApexTestClass2.Name = 'AP_ProductLineAfterUpdateHandler';
       objApexTestClass2.TestClass__c = 'AP_ProductLineAfterUpdateHandler_TEST';
       objApexTestClass2.Type__c = 'Apex Class';
       objApexTestClass2.RunTest__c= true;
       lstApexTestClass.add(objApexTestClass2);
       
       insert lstApexTestClass;
       
       Pagereference objPageReference = Page.VFP_GeneratePackagexml;
       Test.setCurrentPageReference(objPageReference);
       
       ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstApexTestClass);
       sc.setSelected(lstApexTestClass);
       VFC_RunSpecifiedTestsNew objGenerateXML = new VFC_RunSpecifiedTestsNew(sc);
       //VFC_RunSpecifiedTests objGenerateXML = new VFC_RunSpecifiedTests(new ApexPages.StandardSetController(lstComponents));
    }

}