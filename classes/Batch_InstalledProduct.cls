/*
    Author          : Deepak
    Date Created    : 
    Description     : Batch Apex class 
              
*/
    global class Batch_InstalledProduct implements Database.Batchable<sObject>{

    
     global Database.QueryLocator start(Database.BatchableContext BC){ 
      String StLimit =Label.CLFEB15SRV00;
       String Query = 'select id,UniqueIBfieldID__c,SchneiderBrand__c,Batch_RecordUpdate__c  From SVMXC__Installed_Product__c Where Category__c !=null AND SVMXC__Serial_Lot_Number__c!=null AND ForceCreation__c = False AND SchneiderBrand__c = true AND Batch_RecordUpdate__c=false Limit '+StLimit;
       return Database.getQueryLocator(Query);
       
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){           
        AP_InstalledProductBatch.ProcessProduct(scope);
        Database.update(scope,false);
    }


    global void finish(Database.BatchableContext BC){
        
    }

}