@isTest
private class VCC_CSVParser_TEST {
    static testMethod void testVCC_CSVParser() {
        VCC_CSVParser vfCtrl = new VCC_CSVParser();
        //vfCtrl.uploadedFile=Blob.valueof('testing');
        String contents='Field1,Field2,Field3\n1,,Smith\n2,Fred,O\'Connor\n3,Destiny,"Awaits, DDS"\n\n';
        vfCtrl.uploadedFile= Blob.valueOf(contents);
        vfCtrl.upload();     
        vfCtrl.cancel();
        test.starttest();
            contents='Field1;Field2;Field3\n1,,Smith\n2,Fred,O\'Connor\n3,Destiny","Awaits, DDS"\n\n';
            vfCtrl.uploadedFile= Blob.valueOf(contents);
            vfCtrl.upload();     
            vfCtrl.cancel();
        test.stoptest();
        try{vfCtrl.uploadedFile= null;vfCtrl.upload();vfCtrl.cancel();}
        Catch(Exception ex){}
    }
}