/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: Test Class                                          *
* Created Date:                                             *
* Tested Class: Fielo_NewsViewExtensions                    *
************************************************************/

@isTest
public with sharing class Fielo_NewsViewExtensionsTest{
  
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        
        Account acc= new Account(
            Name = 'test',
            Street__c = 'Some Street',
            ZipCode__c = '012345'
        );
        insert acc;
                
        FieloEE__Member__c mem = new FieloEE__Member__c(
            FieloEE__FirstName__c = 'test',
            FieloEE__LastName__c= 'test'
        );
        insert mem;
    
        FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
        insert memb;    
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c(
            FieloEE__Title__c = 'test'
        );
        insert menu;
        
        FieloEE__Component__c component = new FieloEE__Component__c(
            FieloEE__Menu__c = Menu.Id
        );
        insert component;
        
        
        ApexPages.currentPage().getParameters().put('idMenu', menu.Id);
        ApexPages.currentPage().getParameters().put('compId', component.Id);
        Fielo_NewsViewExtensions test = new Fielo_NewsViewExtensions(new ApexPages.StandardController(acc));
        test.refresh();
    
    }
}