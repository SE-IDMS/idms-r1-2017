global with sharing class FieloPRM_C_ChatComponentController {

    public static ChatConfiguration chatConfig{get;set;}
    public Id memberId {get;set;}
    @RemoteAction
    global static ChatConfiguration getChatConfiguration(Id memberId,Id tagId){
        try {
            FieloEE__Member__c member;
            //F_Country__r.CountryCode__c,
            for (FieloEE__Member__c m : [SELECT F_PRM_Country__c, F_Account__c,F_PRM_PrimaryChannel__c,F_PRM_Country__r.SupportedLanguage1__c,F_PRM_Country__r.SupportedLanguage2__c,F_PRM_Country__r.SupportedLanguage3__c,FieloEE__User__r.LanguageLocaleKey FROM FieloEE__Member__c WHERE Id =: memberId]){
                member = m;
                break;
            }
            //Suuported LanguageNumber Logic
            Integer languageNumber=1;
            if((member.F_PRM_Country__r.SupportedLanguage2__c!=null || member.F_PRM_Country__r.SupportedLanguage3__c!=null) && member.FieloEE__User__r.LanguageLocaleKey!=null){
                if(member.FieloEE__User__r.LanguageLocaleKey.equals(member.F_PRM_Country__r.SupportedLanguage2__c)){
                    languageNumber=2;
                }else if(member.FieloEE__User__r.LanguageLocaleKey.equals(member.F_PRM_Country__r.SupportedLanguage3__c)){
                    languageNumber=3;
                }
            }
            List<PRMChatComponentConfiguration__c> chat =  [select Id,MemberPrimaryChannel__c,CCRegionCode__c,ChatAudience__c,CountryCode__c,CountryFullName__c,CustomForm__c,DeploymentId__c,CountryCluster__c,ProactiveChatButtonId__c,ReactiveChatButtonId__c,Tag__c,TimerConfigId__c,WindowHeight__c,WindowWidth__c
                                            from PRMChatComponentConfiguration__c where IsActive__c=true AND LanguageNumber__c=:languageNumber AND ((CountryCluster__c=:member.F_PRM_Country__c AND MemberPrimaryChannel__c=null) OR (CountryCluster__c=:member.F_PRM_Country__c AND MemberPrimaryChannel__r.Name=:member.F_PRM_PrimaryChannel__c))
                                            order by Tag__c,MemberPrimaryChannel__c NULLS LAST limit 1]; 

            System.debug('chat ' + chat);
            if(chat.size()>0){
                return initChat(chat[0]);
            }else{
                ChatConfiguration myChat = new ChatConfiguration();
                myChat.errorMsg = 'no chat for member: '+ member;
                return myChat;
            }
            
        } catch(Exception e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            ChatConfiguration myChat = new ChatConfiguration();
            myChat.errorMsg = 'The following exception has occurred: ' + e.getMessage() + e.getStackTraceString();
            return myChat;

        }
        return null;
    }

    private static ChatConfiguration initChat(PRMChatComponentConfiguration__c chat) {

        ChatConfiguration myChat = new ChatConfiguration();
        myChat.ccRegionCode = chat.CCRegionCode__c!=null?chat.CCRegionCode__c:'';
        myChat.chatAudience = chat.ChatAudience__c!=null?chat.ChatAudience__c:'';
        myChat.countryCode = chat.CountryCode__c!=null?chat.CountryCode__c:'';
        myChat.countryFullName = chat.CountryFullName__c!=null?chat.CountryFullName__c:'';
        myChat.customForm = chat.CustomForm__c!=null?chat.CustomForm__c:'';
        myChat.deploymentId = chat.DeploymentId__c!=null?chat.DeploymentId__c:'';
        myChat.proactiveChatButtonId = chat.ProactiveChatButtonId__c; 
        myChat.reactiveChatButtonId = chat.ReactiveChatButtonId__c; 
        myChat.timerConfigId = String.valueOf(chat.TimerConfigId__c);
        myChat.windowHeight = Integer.valueOf(chat.WindowHeight__c);
        myChat.windowWidth = Integer.valueOf(chat.WindowWidth__c);
        myChat.proActive = myChat.proactiveChatButtonId != null? true : false;
        myChat.deploymentJsUrl = Label.CLJUL16PRM001;
        myChat.chatInitUrl = Label.CLJUL16PRM002;
        myChat.organizationId = UserInfo.getOrganizationId();

        System.debug('myChat ' + myChat);
        return myChat;
    }


    global class ChatConfiguration {
        public String ccRegionCode{get;set;}
        public String chatAudience{get;set;}
        public String countryCode{get;set;}
        public String countryFullName{get;set;}
        public String customForm{get;set;}
        public String deploymentId{get;set;}
        public String proactiveChatButtonId{get;set;}
        public String reactiveChatButtonId{get;set;}
        public String timerConfigId{get;set;}
        public Integer windowHeight{get;set;}
        public Integer windowWidth{get;set;}
        public String deploymentJsUrl{get; set;}
        public String chatInitUrl{get;set;}
        public Boolean proActive{get;set;}
        public String organizationId{get;set;}
        /* case of error */
        public String errorMsg{get;set;}
    }
}