public without sharing Class VFC_ConfigureProgressiveProfile {

    public PRMProfileConfig__c PRMProfileConfig {get;set;}

    public List<ccFPWrapperClass> ccpWrapLst {get;set;}
    public PRMProfileConfig__c ccFProfileConfig {get;set;}
    public List<SelectOption> picklistValuesBT{get;set;}
    public Boolean showCntry{get;set;}
    public Boolean showChannel{get;set;}
    public Boolean showFeature{get;set;}
    public String cntryId;
    public Integer counter {get;set;}
    public List<ccFPWrapperClass> ccpWrapDeleteLst;
    public String source;
    public String prmConfigId;

    public VFC_ConfigureProgressiveProfile(ApexPages.StandardController stdCont){
        ccpWrapDeleteLst = new List<ccFPWrapperClass>();
        ccpWrapLst = new List<ccFPWrapperClass>();
        //String channleId = ApexPages.currentPage().getParameters().get('cid');
        ccFProfileConfig = new PRMProfileConfig__c();
        PRMProfileConfig = new PRMProfileConfig__c();
        picklistValuesBT = new List<SelectOption>();
        cntryId = ApexPages.currentPage().getParameters().get('cntryId');
        source = ApexPages.currentPage().getParameters().get('from');
        String channelId = ApexPages.currentPage().getParameters().get('channelId');


        if(source == 'cntry' && String.isNotBlank(cntryId)){
            PRMProfileConfig.PRMCountryClusterSetup__c = cntryId;
            showCntry = true;
            showChannel = false;
            showFeature = false;
        }

        if(source == 'channel' && String.isNotBlank(channelId) && String.isNotBlank(cntryId)) {
            PRMProfileConfig.CountryChannel__c = channelId;
            //PRMProfileConfig.PRMCountryClusterSetup__c = cntryId;
            showChannel = true;
            showCntry = false;
            showFeature = false;
        }


        List<PRMProfileConfigInfo__c> prmProConfigInfo;
        List<PRMProfileConfig__c> prmProCon;

        if(source == 'Manage' ){
            prmConfigId = ApexPages.currentPage().getParameters().get('prmConfigId');
            String prmConfigName = ApexPages.currentPage().getParameters().get('Name');
            PRMProfileConfig.Name = prmConfigName;
            if(String.isNotBlank(channelId)){
                showChannel = true;
                PRMProfileConfig.CountryChannel__c = channelId;
                system.debug('** Config Channel and COuntry***'+prmProCon);
            } else if(String.isNotBlank(cntryId)){
                showCntry = true;
                PRMProfileConfig.PRMCountryClusterSetup__c = cntryId;
                system.debug('** Country***'+prmProCon);
            }

            if(String.isNotBlank(prmConfigId))
                prmProConfigInfo = [SELECT Id,FieldAPIName__c,ObjectAPIName__c,Mandatory__c FROM PRMProfileConfigInfo__c WHERE PRMProfileConfig__c =: prmConfigId];

            system.debug('** Config Info ** '+prmProConfigInfo+ '** size **'+prmProConfigInfo.size());
            if(prmProConfigInfo != Null && prmProConfigInfo.size()>0){

                for(PRMProfileConfigInfo__c ppci : prmProConfigInfo){

                    ccFPWrapperClass wrap = new ccFPWrapperClass();
                    wrap.objField = ppci.FieldAPIName__c;
                    //wrap.objFieldValue = cc.ObjectAPIName__c;
                    wrap.Mandatory = ppci.Mandatory__c;
                    wrap.prmConfigInfoId = ppci.Id;


                    ccpWrapLst.add(wrap);
                }
            }
            if(ccpWrapLst.size()<=0)
                ccpWrapLst.add(new ccFPWrapperClass());
        }
        if(ccpWrapLst.size()<=0 && (source=='channel' || source=='cntry'))
            ccpWrapLst.add(new ccFPWrapperClass());

    }


    public PageReference insertRow(){
        system.debug('** before **'+ccpWrapLst);

        system.debug('*****'+ccpWrapLst[ccpWrapLst.size()-1].objField);
        
        if(ccpWrapLst[ccpWrapLst.size()-1].objField == Null){
            system.debug('Here**');
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.CLQ416PRM009));
            Return Null;
        }
        
        ccpWrapLst.add(new ccFPWrapperClass());
        system.debug('** after **'+ccpWrapLst);

        Return Null;
    }

    public List<SelectOption> getpicklistValues(){
        System.debug('Entered Account-->');
        List<SelectOption> Options = new List<SelectOption>();
        Options.add(new SelectOption('','--Select--'));

        for (CS_PRM_ProgressiveProfileConfig__c aFld : [SELECT Id, Name, FieldLabel__c, MappedObject__c, MappedField__c FROM CS_PRM_ProgressiveProfileConfig__c ORDER BY FieldSortOrder__c ASC]) {
            Options.add(new SelectOption(aFld.Name,aFld.FieldLabel__c));
        }
        return Options;
    }

    public Class ccFPWrapperClass{

        public String objField{get;set;}
        public String objFieldValue{get;set;}
        public String objTextFieldValue{get;set;}
        public List<SelectOption> WrapOptions {get;set;}
        public Integer Element{get;set;}
        public Boolean Mandatory{get;set;}
        public Id prmConfigInfoId{get;set;}

        public ccFPWrapperClass(){
            objField = '';
            objFieldValue = '';
            WrapOptions= new List<SelectOption>();
            Mandatory  = true;
        }
    }

    public PageReference SaveProfileConfig(){

        system.debug(' Size Save****'+ccpWrapLst.size());
        Integer count = checkForDuplicates();
        if(count >0){
            System.debug('count:' + count);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,System.Label.CLQ416PRM008));
            return null;
        }
        else{
            PRMCountry__c prmCluster;
            CountryChannels__c countryChannel;
            PRMProfileConfig__c ppc = new PRMProfileConfig__c();
            List<PRMProfileConfigInfo__c> ccfpLst = new List<PRMProfileConfigInfo__c>();
            Map<String,Id> recordTypeMap = new Map<String,Id> ();
            List<RecordType> recordtypes = new List<RecordType> ([SELECT DeveloperName,Id,Name FROM RecordType WHERE SObjectType = 'PRMProfileConfig__c']);
            if(recordtypes != null && recordtypes.size() >0){
                for(RecordType rtype :recordtypes)
                    recordTypeMap.put(rtype.DeveloperName,rtype.Id);
            }

            if(String.isNotBlank(prmConfigId))
                ppc.Id = prmConfigId;

            if(source != 'Manage'){
                if(String.isNotBlank(PRMProfileConfig.PRMCountryClusterSetup__c)) {
                        
                        ppc.RecordTypeId = recordTypeMap.get('CountryLevelProfileForm');
                        ppc.Active__c = True;
                        ppc.FeatureCatalog__c = PRMProfileConfig.FeatureCatalog__c;
                        ppc.PRMCountryClusterSetup__c = PRMProfileConfig.PRMCountryClusterSetup__c;
                        ppc.Name = PRMProfileConfig.Name;

                } else if(String.isNotBlank(PRMProfileConfig.CountryChannel__c)) {

                        ppc.RecordTypeId = recordTypeMap.get('ChannelLevelProfileForm');
                        ppc.Active__c = True;
                        ppc.FeatureCatalog__c = PRMProfileConfig.FeatureCatalog__c;
                        ppc.CountryChannel__c = PRMProfileConfig.CountryChannel__c;
                        ppc.Name = PRMProfileConfig.Name;
                        //if(String.isNotBlank(cntryId))
                          //  ppc.PRMCountryClusterSetup__c = cntryId;

                }
                INSERT ppc;
            }

            if (String.isNotBlank(ppc.Id)) {
                for (ccFPWrapperClass cc : ccpWrapLst){
                    System.debug('**Each Element **'+cc);
                    if (String.isBlank(cc.objField)) continue;
                    PRMProfileConfigInfo__c ccfp = new PRMProfileConfigInfo__c ();
                    ccfp.FieldAPIName__c = cc.objField;
                    ccfp.Mandatory__c    = cc.Mandatory;
                    ccfp.PRMProfileConfig__c  = ppc.Id;

                    ccfp.ObjectAPIName__c  = cc.objFieldValue;
                    ccfp.Id  = cc.prmConfigInfoId;

                    ccfpLst.add(ccfp);
                }
            }

            System.debug('** Upsert List**'+ccfpLst+'**Size **'+ccfpLst.size());
            if(ccfpLst != Null && ccfpLst.size()>0)
                UPSERT ccfpLst;

             List<PRMProfileConfigInfo__c> ccfpDelLst = new List<PRMProfileConfigInfo__c>();
             if(ccpWrapDeleteLst != Null && ccpWrapDeleteLst.size()>0){
                 for(ccFPWrapperClass cc :ccpWrapDeleteLst){
                     if(cc.prmConfigInfoId != null){

                     PRMProfileConfigInfo__c ccfp = new PRMProfileConfigInfo__c();
                        ccfp.FieldAPIName__c = cc.objField;
                        ccfp.Mandatory__c    = cc.Mandatory;
                        ccfp.PRMProfileConfig__c  = ppc.Id;

                        ccfp.ObjectAPIName__c  = cc.objFieldValue;
                        ccfp.Id  = cc.prmConfigInfoId;

                    ccfpDelLst.add(ccfp);
                 }
                 }

             }

             if(ccfpDelLst != Null && ccfpDelLst.size()>0){
                Delete ccfpDelLst;
             }

            if(String.isNotBlank(PRMProfileConfig.CountryChannel__c))
                return new PageReference('/'+PRMProfileConfig.CountryChannel__c);
            else
                return new PageReference('/'+PRMProfileConfig.PRMCountryClusterSetup__c);
        }

    }

    public PageReference Cancel(){
        system.debug('** **'+PRMProfileConfig.CountryChannel__c);
        if(String.isNotBlank(PRMProfileConfig.CountryChannel__c))
            return new PageReference('/'+PRMProfileConfig.CountryChannel__c);
        else if (String.isNotBlank(PRMProfileConfig.PRMCountryClusterSetup__c))
            return new PageReference('/'+PRMProfileConfig.PRMCountryClusterSetup__c);

    return null;
    }

    public PageReference RemoveRow(){
        System.debug('******'+counter);
        system.debug(' Size ****'+ccpWrapLst.size());
        system.debug(' Size Delete****'+ccpWrapDeleteLst.size());
        ccpWrapDeleteLst.add(ccpWrapLst[counter]);
        ccpWrapLst.remove(counter);
        if(ccpWrapLst.size() <= 0){
            ccpWrapLst.add(new ccFPWrapperClass());
        }

    system.debug(' Size Remove****'+ccpWrapLst.size());
    return null;
    }
    public Integer checkForDuplicates(){
        set<String> fieldNames = new set<String> ();
        Integer count = 0;
        for (ccFPWrapperClass cc : ccpWrapLst){
            if(fieldNames.contains(cc.objField)){
                System.debug('Duplicate field found:' + cc.objField);
                count++;
            }
            else{
                System.debug('Adding into set:' + cc.objField);
                fieldNames.add(cc.objField);
            }
        }
        System.debug('count:' + count);
        return count;
    }
}