@isTest
private class VFC_NotificationConfiguration_TEST{
    static testMethod void testNewAlert() {
    
        list<RecordType> recordTypes = new list<RecordType>([Select Id, developerName from RecordType WHERE developerName IN ('PRMGlobal','PRMCountryCluster')]);
        
        Country__c c = Utils_TestMethods.createCountry();
        insert c;
        
        PRMCountry__c cluster = new PRMCountry__c();
        cluster.Name = 'Test Cluster';
        cluster.Country__c = c.Id;
        cluster.PLDatapool__c = 'test';
        cluster.CountrySupportEmail__c = 'test@abc.com';
        cluster.DefaultLandingPage__c = '/test';
        insert cluster;
        
        setupAlerts__c alert2 = new setupAlerts__c ();
        alert2.Name = 'Test Alert 2';
        alert2.Active__c = true;
        alert2.Content__c = 'This is a test alert';
        alert2.Display__c = 'Show Only Once';
        alert2.FromDate__c = System.now();
        alert2.ToDate__c = system.today() + 2;
        alert2.recordTypeId = recordTypes[1].Id;
        alert2.PRMCountryClusterSetup__c = cluster.Id;
        try{
            insert alert2;
        }
        catch (exception e){    
            system.debug('exception:' + e);
        }
        
        VFC_NotificationConfiguration vctrl = new VFC_NotificationConfiguration();
        
        setupAlerts__c alert1 = new setupAlerts__c ();
        alert1.Name = 'Test Alert 1';
        alert1.Active__c = true;
        alert1.Content__c = 'This is a test alert';
        alert1.Display__c = 'Repetitively On Every Login Until Acknowledged';
        alert1.FromDate__c = System.now();
        alert1.ToDate__c = system.today() + 2;
        alert1.RecordTypeId = recordTypes[0].Id;
        insert alert1;
        
        
        
        VFC_NotificationConfiguration vctrl1 = new VFC_NotificationConfiguration();
        VFC_NotificationConfiguration.closePopup(alert1.Id,alert1.Display__c,false);
        VFC_NotificationConfiguration.closePopup(alert1.Id,alert1.Display__c,true);
        VFC_NotificationConfiguration.closePopup(alert2.Id,alert2.Display__c,false);
        
       
    }
}