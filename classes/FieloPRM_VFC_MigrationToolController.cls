public with sharing class FieloPRM_VFC_MigrationToolController {
    public String option{get;set;}
    public String query{get;set;}
    public String result{get;set;}
    public String programID{get;set;}
    public list<FieloEE__Program__c> listProgram {get;set;}

    public Attachment inputFile {get; set;}
    
    public List<Id> insertedIDs;

    public FieloPRM_VFC_MigrationToolController() {
        option = 'Content Feed';
        //query = ' WHERE  F_PRM_ContentFilter__c like \'P-RU-SP-SPE-News\'  ';
        listProgram = [SELECT id,Name FROM FieloEE__Program__c Order BY FieloEE__Default__c];

    }
    public pageReference export(){
        string xmlReturn = '';
        if(option == 'Banner'){
            xmlReturn = FieloPRM_UTILS_MigrationMethods.export(programID,query , true);
        }else{
            xmlReturn =  FieloPRM_UTILS_MigrationMethods.export(programID,query , false);
        }
        result =xmlReturn ;
        return null;
    }
    public pageReference importFile() {

        if(String.isBlank(result)){
            //checks whether the file exist
            if(inputFile.body == null){ 
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.CONFIRM,'File null'));
                inputFile = new Attachment();
                return null;
            }

            if(!inputFile.Name.contains('.')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.CONFIRM,' Error Format File'));
                inputFile = new Attachment();
                return null;
            }

            String fileExt = inputFile.Name.subString(inputFile.Name.lastIndexOf('.'));
            if(!fileExt.equalsIgnoreCase('.xml')){ //checks the file format
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.CONFIRM,'Error Format File'));
                inputFile = new Attachment();
                return null;
            }

        }

        try{
            if(!String.isBlank(result)){

                if(option == 'Banner'){
                    insertedIDs = FieloPRM_UTILS_MigrationMethods.importMenus(result, programID, true, true);
                }else{
                    insertedIDs = FieloPRM_UTILS_MigrationMethods.importMenus(result, programID, true, false);
                }
            }else {
                if(option == 'Banner'){
                    insertedIDs = FieloPRM_UTILS_MigrationMethods.importMenus(inputFile.body.toString(), programID, true,true);   
                }else{
                    insertedIDs = FieloPRM_UTILS_MigrationMethods.importMenus(inputFile.body.toString(), programID, true,false);   
                }

            }


            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.CONFIRM, 'Success'));           
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, e.getMessage()));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error, e.getStackTraceString()));

        }           

        inputFile = new Attachment();
        return null;
    }

    public List<SelectOption> getPrograms(){
       List<SelectOption> options = new list<SelectOption>();
       String mins = '30';
       for ( FieloEE__Program__c p: listProgram){
           options.add(new SelectOption(p.id, p.Name));
       }
       return options;     
    }
    
    public PageReference downloadExport(){
        Document doc = new Document(Name='FieloExportMigrationDoc', Body=Blob.valueOf(result), ContentType='text/xml', Type='xml', FolderId = UserInfo.getUserId());
        insert doc;
        return new PageReference('/apex/FieloPRM_VFP_DownloadXML?fileId=' + doc.Id);
    }
    
    public PageReference downloadImport(){
        String textInsertedIds = '';
        for(Id thisId: insertedIds){
            textInsertedIds += thisId + '\n';
        }
        Document doc = new Document(Name='FieloImportMigrationDoc', Body=Blob.valueOf(textInsertedIds), ContentType='text/plain', Type='txt', FolderId = UserInfo.getUserId());
        insert doc;
        return new PageReference('/apex/FieloPRM_VFP_DownloadXML?fileId=' + doc.Id);
    }

}