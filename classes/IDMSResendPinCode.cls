//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This class is getting called by Resend PIN API. 
//                    This class resend pin to users on thier registered mobile number.
//
//*******************************************************************************************


public without sharing class IDMSResendPinCode {
    //This method accepts User id and Federation id to send randomly generated pin code. 
    public IDMSResponseWrapperCodePIN  ResendCodePin(String UserId, String FederationId){
        if(!mandatoryFieldsMissing(UserId,FederationId))
        {
            if (!SearchExistingUserId(UserId))
            {
                List<User> user = [select ID, IDMSPinVerification__c, Phone, mobilephone, IDMSNewPhone__c from User where Id =: UserId];
                system.debug('User coming from IFW:'+user);
                User usertoSend; 
                if(user.size()> 0){
                    usertoSend = user[0];
                }
                usertoSend.IDMSPinVerification__c = String.valueof(100000+ Math.round(Math.random()*100000));
                update usertoSend;
                IDMSSendCommunication.sendSMS(usertoSend);
                system.debug('SMS has been sent on:'+usertoSend.phone);
                return new IDMSResponseWrapperCodePIN(Label.CLQ316IDMS066, Label.CLQ316IDMS065);
                
            }else if(!SearchExistingUserFedId(FederationId)){
                List<User> u = [select ID, FederationIdentifier, IDMSPinVerification__c, Phone,mobilephone, IDMSNewPhone__c from User where FederationIdentifier =: FederationId];
                system.debug('User coming from IFW:'+u);
                User usrtoSend;
                if(u.size()> 0){
                    usrtoSend = u[0];
                }
                usrtoSend.IDMSPinVerification__c = String.valueof(100000+ Math.round(Math.random()*100000));
                update usrtoSend;
                IDMSSendCommunication.sendSMS(usrtoSend);
                system.debug('SMS has been sent on:'+usrtoSend.phone);
                return new IDMSResponseWrapperCodePIN(Label.CLQ316IDMS066, Label.CLQ316IDMS065);
            }else {
                return new IDMSResponseWrapperCodePIN(Label.CLQ316IDMS067, Label.CLQ316IDMS096);
            }
        }else {
            return new IDMSResponseWrapperCodePIN(Label.CLQ316IDMS067, Label.CLQ316IDMS058);
        }
    }
    //Mandatory fields check
    public boolean mandatoryFieldsMissing(String UserId, String FederationId)
    {
        if(String.isEmpty(UserId) && String.isEmpty(FederationId)){
            return true;
        }
        return false;
    }
    
    //Check User id.
    public boolean UserIdFieldMissing(String UserId)
    {
        if(String.isEmpty(UserId)){
            return true;
        }
        return false;
    }
    
    //Check user exists
    public boolean SearchExistingUserId(String UserId){
        system.debug('User id:'+UserId);
        List<User> usrIds = [select Id, mobilephone, FederationIdentifier from User where  Id =: UserId  limit 1];
        if(usrIds.isEmpty()){
            system.debug('entered here');
            return true;
        }
        else{
            return false;
        }
    }
    
    //Get existing user using fed Id. 
    public boolean SearchExistingUserFedId(String FederationId){
        system.debug('Fed Id:'+FederationId);
        List<User> usrFedIds = [select ID, FederationIdentifier, mobilephone from User where FederationIdentifier != '' AND FederationIdentifier =: FederationId limit 1];
        if(usrFedIds.isEmpty()){
            system.debug('entered here');
            return true;
        }
        else{
            return false;
        }
    }
    
}