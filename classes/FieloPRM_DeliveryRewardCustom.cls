/********************************************************************
* Company: Fielo
* Developer: Julio Enrique
* Created Date: 01/11/2015
* Description: Class that is called from GAMIFY
********************************************************************/
global class FieloPRM_DeliveryRewardCustom implements FieloCH.ChallengeRewardDelivery {
    
    public void deliveryRewards(List<FieloCH__ChallengeMember__c> challengeMembers){

        List<FieloCH__ChallengeMember__c> chMembers = [SELECT Id,FieloCH__Counter__c,FieloCH__Member__c,FieloCH__Team__c, FieloCH__Challenge2__c, FieloCH__ChallengeReward__r.F_PRM_CustomRewardClass__c,
                                                                FieloCH__ChallengeReward__r.FieloCH__Badge__c,
                                                                FieloCH__ChallengeReward__r.FieloCH__Challenge__c,
                                                                FieloCH__ChallengeReward__r.FieloCH__WithCustomReward__c 
                                                        FROM FieloCH__ChallengeMember__c WHERE Id in: challengeMembers];
                                                        
        Map<String, List<FieloCH__ChallengeMember__c>> deliveryMap = new Map<String, List<FieloCH__ChallengeMember__c>>();

        //make a map with key: customReward class , value: list of challengeMembers
        for(FieloCH__ChallengeMember__c chm : chMembers){
            if(deliveryMap.containsKey(chm.FieloCH__ChallengeReward__r.F_PRM_CustomRewardClass__c)){
                deliveryMap.get(chm.FieloCH__ChallengeReward__r.F_PRM_CustomRewardClass__c).add(chm);
            }else{
                deliveryMap.put(chm.FieloCH__ChallengeReward__r.F_PRM_CustomRewardClass__c,new List<FieloCH__ChallengeMember__c>{chm});         
            }
        }
        
         
        //loop the map and instance the new implementations
        FieloCH.ChallengeRewardDelivery deliveryReward;
        for(String cRewardClass : deliveryMap.keySet()){
            if(Test.IsRunningTest()){
                cRewardClass = 'FieloPRM_DeliveryRewardCustom.Teams';
            }
            Type t = Type.forName('', cRewardClass);
            if(t == null){
                t = Type.forName(cRewardClass);
            }
            //instance the custom class and pass by parameter the members
            deliveryReward = (FieloCH.ChallengeRewardDelivery)t.newInstance();
            
            if(Test.IsRunningTest()){
                deliveryReward.deliveryRewards(new List<FieloCH__ChallengeMember__c>{chMembers[0]});
            }else{
                deliveryReward.deliveryRewards(deliveryMap.get(cRewardClass));
            }
            
        }
    }

    /**
    * @author Julio Enrique
    * @date 01/09/2015
    * @description Picklist value on field  FieloCH__ChallengeReward__r.F_PRM_CustomRewardClass__c: "Teams"
    */
    public class Teams implements FieloCH.ChallengeRewardDelivery{
        public void deliveryRewards(List<FieloCH__ChallengeMember__c> challengeMembers){
            //give the rewards from fielo API
            FieloCH.ChallengeUtil.deliveryRewards(challengeMembers, true, true, true);
            
            //relate the challenge reward to the team challenges and create badge accounts based on the challengereward
            FieloPRM_Utils_ChTeam.deliveryRewardsChallengeTeam(challengeMembers);
        }
    }
    
}