@isTest
Public class VFC_SRV_MYFSCaseDetailController_Test{
    static testMethod void caseStatusTestMethod(){
        
        /*  Country__c count = Utils_TestMethods.createCountry();
insert count;

Account account1 = Utils_TestMethods.createAccount();
account1.country__c=count.id;
insert account1;


Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact');
insert contact1;

Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
insert case1;
*/   
        Country__c count = new Country__c(Name='TestCountry', CountryCode__c='TCY');
        Account account1 =  new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345');
        account1.country__c=count.id;
        insert account1;
        Contact contact1 = new Contact(
            FirstName='Test',
            LastName='TestContact',
            AccountId=account1.Id,
            JobTitle__c='Z3',
            CorrespLang__c='EN',
            WorkPhone__c='1234567890'
        );
        insert contact1;
        Case case1 = new Case(
            Status='Open',
            AccountId=account1.Id,
            ContactId=contact1.Id,
            SupportCategory__c='6-General',
            Priority='Medium',
            Subject='Complaint',
            origin='Phone'
        );
        insert case1;
        Test.startTest();        
        VFC_SRV_MYFSCaseDetailController caseDetailController = new VFC_SRV_MYFSCaseDetailController();
        caseDetailController.caseId = case1.Id;
        caseDetailController.getCaseDetails();
        Test.stopTest();
    }
}