/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  01-Oct-2012
    Description         : Class for Connect Cascading KPI Target Triggers Assigns owner and Sharing permission for users
*/

public class ConnectCascadingKPITargetTriggers
{

  Public Static void CascadingKPITargetBeforeInsertUpdate_ValidateScopeEntity(list<Cascading_KPI_Target__c> CKPI)
       {
 
               
                  System.Debug('****** CascadingKPITargetBeforeInsertUpdate_ValidateScopeEntity Start ****');   
                        
                  List<Global_KPI_Targets__c> GKPIT = new List<Global_KPI_Targets__c>();
                  GKPIT = [Select id,Name,CountryPickList__c,Global_Functions__c, Operations_Region__c, Global_Business__c, GSC_Regions__c,Partner_Sub_Entity__c,Global_Ops_Regions__c,Smart_Cities_Division__c from Global_KPI_Targets__c where Year__c = :CKPI[0].Year__c];
                  integer GF; integer GB; integer PR; integer GSC; integer GOR;integer SC;integer Ctry;
                  for(Cascading_KPI_Target__c C:CKPI){
                  GF = 0;
                  GB = 0;
                  PR = 0;
                  GSC = 0;
                  GOR = 0;
                  SC = 0;
                  Ctry=0;
                  
                  C.validatescope__c = false;
                  for(Global_KPI_Targets__c M: GKPIT){
                         if(C.Global_KPI_Target__c == M.id){
                             if(M.CountryPickList__c !=null){
                              if((C.CountryPickList__c != null)&& !(M.CountryPickList__c.Contains(C.CountryPickList__c )))
                                Ctry=Ctry+1;
                             }
                             else if((M.CountryPickList__c == null) && (C.CountryPickList__c != null))
                             Ctry=Ctry+1;
                             
                           if(M.Global_Functions__c !=null){
                             if((C.Scope__c == Label.Connect_Global_Functions) && !(M.Global_Functions__c.Contains(C.Entity__c)))
                               GF = GF + 1;
                            }
                            else if((M.Global_Functions__c == null)&& (C.Scope__c == Label.Connect_Global_Functions))
                            GF = GF + 1;
                           
                           if(M.Global_Business__c !=null){
                             if((C.Scope__c == Label.Connect_Global_Business) && !(M.Global_Business__c.Contains(C.Entity__c)))
                               GB = GB + 1;
                             }
                             else if((M.Global_Business__c == null) && (C.Scope__c == Label.Connect_Global_Business))
                             GB = GB + 1;
                             
                           if(M.Operations_Region__c !=null) { 
                               if((C.Scope__c == Label.Connect_Power_Region) && !(M.Operations_Region__c.Contains(C.Entity__c)))
                               PR = PR + 1;
                              }
                               else if((M.Operations_Region__c == null) && (C.Scope__c == Label.Connect_Power_Region))
                               PR = PR + 1;
                              
                              
                              // GSC Region Validation
                              
                             if((M.GSC_Regions__c !=null)&& (C.GSC_Region__c !=null)){
                               if((C.Scope__c == Label.Connect_Global_Functions) && (C.Entity__c == Label.Connect_GSC) && !(M.GSC_Regions__c.Contains(C.GSC_Region__c)))
                               GSC = GSC + 1;
                              } 
                               else if((M.GSC_Regions__c == null) && (C.Entity__c == Label.Connect_GSC) )
                               GSC = GSC + 1;
                               
                                // SC Region Validation
                              
                             if((M.Smart_Cities_Division__c !=null)&& (C.Smart_Cities_Division__c !=null)){
                               if((C.Scope__c == Label.Connect_Global_Business) && (C.Entity__c == Label.Connect_Smart_Cities) && !(M.Smart_Cities_Division__c.Contains(C.Smart_Cities_Division__c)))
                               SC = SC + 1;
                              } 
                               else if((M.Smart_Cities_Division__c == null) && (C.Entity__c == Label.Connect_Smart_Cities) )
                               SC = SC + 1;
                                                            
                               // Global Operations Region Validation
                             if((M.Global_Ops_Regions__c !=null)&& (C.Global_Ops_Regions__c !=null)){
                               if((C.Scope__c == Label.Connect_Power_Region) && (C.Entity__c == Label.Global_Operations) && !(M.Global_Ops_Regions__c.Contains(C.Global_Ops_Regions__c)))
                               GOR = GOR + 1;
                              } 
                               else if((M.Global_Ops_Regions__c == null) && (C.Entity__c == Label.Global_Operations) )
                               GOR = GOR + 1;
                                                            
                                                     
                               
                               } // if Ends
                               
                                 
                               System.Debug('GSC Value ' + GSC + M.GSC_Regions__c + 'Entity ' + C.Entity__c);
                               if((GF > 0) ||  (GB > 0) ||  (PR > 0) || (GSC > 0) || (GOR > 0)|| (SC > 0) || (Ctry > 0) )
                                   C.validatescope__c = True;
                               
                              } // For Ends
                             } //For Ends
                             
                             
                System.Debug('****** CascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity Stop ****');                
                  
              }
              
              //Duplicate entry validation
              
              
Public Static void ConnectEntityKPIBeforeInsertUpdate_ValidateEntityRecord(List<Cascading_KPI_Target__c> CKPIEntity){
  
list<Cascading_KPI_Target__c> Eprogress=new list<Cascading_KPI_Target__c>();
    
         Eprogress=[select id,Scope__c,Entity__c,CountryPickList__c,GSC_Region__c,Partner_Sub_Entity__c,Global_KPI_Target__c,Year__c,Smart_Cities_Division__c from Cascading_KPI_Target__c where Year__c = :CKPIEntity[0].Year__c];
            for(Cascading_KPI_Target__c CEn:CKPIEntity){
                CEn.ValidateEntityInsertUpdate__c=false;
                for(Cascading_KPI_Target__c EP : Eprogress){
                    if(EP.Global_KPI_Target__c == CEn.Global_KPI_Target__c && EP.Year__c == CEn.Year__c && CEn.CountryPickList__c == null){
                        if(CEn.GSC_Region__c == null && CEn.Partner_Sub_Entity__c == null && CEn.Smart_Cities_Division__c == null && CEn.Scope__c == EP.Scope__c && CEn.Entity__c == EP.Entity__c){
                    
                           CEn.ValidateEntityInsertUpdate__c=true;
                        }
                                         
                        if(!(CEn.GSC_Region__c == null)){
                            if(CEn.Scope__c == EP.Scope__c && CEn.Entity__c == EP.Entity__c && CEn.GSC_Region__c == EP.GSC_Region__c)
                    
                            CEn.ValidateEntityInsertUpdate__c=true;
                        }
                        
                         if(!(CEn.Smart_Cities_Division__c == null)){
                         system.debug('----'+EP.Smart_Cities_Division__c+'------'+CEn.Smart_Cities_Division__c);
                            if(CEn.Scope__c == EP.Scope__c && CEn.Entity__c == EP.Entity__c && CEn.Smart_Cities_Division__c == EP.Smart_Cities_Division__c){
                    
                            CEn.ValidateEntityInsertUpdate__c=true;
                            }
                        }
                                
                        if(!(CEn.Partner_Sub_Entity__c == null)){
                            if(CEn.Scope__c == EP.Scope__c && CEn.Entity__c == EP.Entity__c && CEn.Partner_Sub_Entity__c == EP.Partner_Sub_Entity__c)
                    
                            CEn.ValidateEntityInsertUpdate__c=true;
                        }                   
                      }
                      
                    else If((CEn.GSC_Region__c == null) && (CEn.Smart_Cities_Division__c == null) && (CEn.Partner_Sub_Entity__c == null)&& (CEn.CountryPickList__c == null)){
                      
                        if(EP.Global_KPI_Target__c == CEn.Global_KPI_Target__c && EP.Year__c == CEn.Year__c ){
                          if(CEn.CountryPickList__c == EP.CountryPickList__c){                    
                            CEn.ValidateEntityInsertUpdate__c=true;
                        }
                       }
                       }                       
                
                    else if(EP.Global_KPI_Target__c == CEn.Global_KPI_Target__c && EP.Year__c == CEn.Year__c && CEn.CountryPickList__c != null && EP.id != CEn.id){
                    system.debug('Global Values  ' + EP.Global_KPI_Target__c + ' ' + CEn.Global_KPI_Target__c +  ' ' + EP.id);
                    if(CEn.CountryPickList__c == EP.CountryPickList__c ){   
                    system.debug('Country Values  ' + CEn.CountryPickList__c + ' ' + EP.CountryPickList__c);                 
                    CEn.ValidateEntityInsertUpdate__c=true;
                        }
                    }
                }
            }
    }
              
              
                  

// Add the Owner of the Cascading KPI record to be same as the Global KPI record

    Public Static void ConnectCascadingKPITargetBeforeInsertUpdate(Cascading_KPI_Target__c CKPI)
      {
      
      System.Debug('****** ConnectCascadingKPITargetBeforeInsertUpdate Start ****'); 
          
       Global_KPI_Targets__c GKPI = [Select id, KPI_Name__c, Ownerid, Global_KPI_Owner__c, Comments__c, KPI_Description__c from Global_KPI_Targets__c where id = :CKPI.Global_KPI_Target__c];
                   
        // copy Description and Comment
        
          CKPI.Comments__c = GKPI.Comments__c;
          CKPI.KPI_Description__c = GKPI.KPI_Description__c;  
                 
             
       System.Debug('****** ConnectCascadingKPITargetBeforeInsertUpdate End ****');
      }
      
      
      //add Country Champion autopopulation
      
      Public Static void CascadingKPITargetCountryChampionBeforeInsertUpdate(Cascading_KPI_Target__c CKPI){
      
          List<Country_Champions__c> CChamp = new List<Country_Champions__c>();
                   CChamp=[select Name,Country__c,User__r.id from Country_Champions__c where Country__c=:CKPI.CountryPickList__c and Year__c =:CKPI.Year__c and IsDefault__c = :'Yes'];
              if(CChamp.size()==0)
                CKPI.Entity_Data_Reporter__c = null;
            for(Country_Champions__c CC:CChamp)
            {
                if(CKPI.CountryPickList__c !=null){
                    CKPI.Entity_Data_Reporter__c = CC.User__r.id;
                }
                
            }
        }
      
   // Add share permission for Entity Data Reporter
   
      Public Static void ConnectCascadingKPITargetAfterInsert(List<Cascading_KPI_Target__c> CKPI)
      {
         List<Cascading_KPI_Target__Share>  CascadingKPIShare = new List<Cascading_KPI_Target__Share>();
         Global_KPI_Targets__c GKPI = [Select id, KPI_Name__c, Ownerid, Global_KPI_Owner__c, Global_Data_Reporter__c, Comments__c, KPI_Description__c from Global_KPI_Targets__c where id = :CKPI[0].Global_KPI_Target__c];
                    
         for(Cascading_KPI_Target__c cascadingkpi : CKPI){
                    
           Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();           
           cshare.ParentId = cascadingkpi.id;
           cshare.UserOrGroupId = cascadingkpi.Entity_Data_Reporter__c;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Entity_Data_Reporter__c;
           CascadingKPIShare.add(cshare);
          }
         Database.insert(CascadingKPIShare,false);
  
          // Add permission to Corporate data reporter
          
         for(Cascading_KPI_Target__c cascadingkpi : CKPI){
           
           Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();           
           cshare.ParentId = cascadingkpi.id;
           cshare.UserOrGroupId = GKPI.Global_Data_Reporter__c;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Corporate_Data_Reporter__c;
           CascadingKPIShare.add(cshare);
          }
         Database.insert(CascadingKPIShare,false);
  
         
      // Add share permission for KPI Owner     
         
         for(Cascading_KPI_Target__c cascadingkpi : CKPI){
           
           Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
           cshare.ParentId = cascadingkpi.id;
           cshare.UserOrGroupId = cascadingkpi.Corporate_KPI_Owner__c;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Corporate_KPI_Owner__c;
           CascadingKPIShare.add(cshare);
          }
         Database.insert(CascadingKPIShare,false);
         
    // Add Program Leader Share Permissions
    
    for(Global_KPI_Targets__Share GKPIShare :[Select Parentid, RowCause, UserOrGroupId, AccessLevel from  Global_KPI_Targets__Share where Parentid = :GKPI.id and RowCause = :'Program_Leaders__c'])
    {
       Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
       cshare.ParentId = CKPI[0].id;
       cshare.UserOrGroupId = GKPIShare.UserOrGroupId;
       cshare.AccessLevel = 'read';
       cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Program_Leaders__c;
       CascadingKPIShare.add(cshare);
     }
         Database.insert(CascadingKPIShare,false);
         
         // Add share permission for KPI Administrator
         
         for(Cascading_KPI_Target__c cascadingkpi : CKPI){
           
           Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
           cshare.ParentId =cascadingkpi.id;
           cshare.UserOrGroupId = Label.CorporateKPIAdminGroup;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Corporate_KPI_Administrator__c;
           CascadingKPIShare.add(cshare);
          }
         Database.insert(CascadingKPIShare,false); 
         
         //add share permission for connect champions
         
         
             list<Champions__c> Connectchampions = new list<Champions__c>();
             Connectchampions =[select id,Scope__c,Entity__c,GSC_Regions__c,Partner_Division__c,Smart_Cities_Division__c,Champion_Name__r.id  from Champions__c where Year__c=:CKPI[0].Year__c];
             for(Cascading_KPI_Target__c T : CKPI){
            
             for(Champions__c C: ConnectChampions){
             if(T.CountryPickList__c ==null){
              if(!(T.Entity__c == Label.Connect_Smart_Cities) && !(T.Entity__c == Label.Connect_Partner) && !(T.Entity__c == Label.Connect_GSC)){
               If((T.Scope__C == C.Scope__c) && (T.Entity__c == C.Entity__c))  {
                   Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();           
                     cshare.ParentId = T.id;
                    cshare.UserOrGroupId = C.Champion_Name__r.id;
                    cshare.AccessLevel = 'read';
                   cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Connect_Champion__c;
                   CascadingKPIShare.add(cshare);
                   
                    }// If Ends
                    } //If Ends
                if((T.Entity__c == Label.Connect_Smart_Cities)){
                   If((T.Scope__C == C.Scope__c) && (T.Entity__c == C.Entity__c) && (T.Smart_Cities_Division__c == C.Smart_Cities_Division__c)) {
                   Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();           
                     cshare.ParentId = T.id;
                    cshare.UserOrGroupId = C.Champion_Name__r.id;
                    cshare.AccessLevel = 'read';
                   cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Connect_Champion__c;
                   CascadingKPIShare.add(cshare);
                  
                    }// If Ends
                    }//If Ends
                    
                if((T.Entity__c == Label.Connect_GSC)){
                   If((T.Scope__C == C.Scope__c) && (T.Entity__c == C.Entity__c) && (T.GSC_Region__c == C.GSC_Regions__c)) {
                   Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
                     cshare.ParentId = T.id;
                    cshare.UserOrGroupId = C.Champion_Name__r.id;
                    cshare.AccessLevel = 'read';
                   cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Connect_Champion__c;
                   CascadingKPIShare.add(cshare);
                  
                    }// If Ends
                    }//If Ends
                if((T.Entity__c == Label.Connect_Partner)){
                   If((T.Scope__C == C.Scope__c) && (T.Entity__c == C.Entity__c) && (T.Partner_Sub_Entity__c == C.Partner_Division__c)) {
                    Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
                     cshare.ParentId = T.id;
                    cshare.UserOrGroupId = C.Champion_Name__r.id;
                    cshare.AccessLevel = 'read';
                   cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Connect_Champion__c;
                   CascadingKPIShare.add(cshare);
                   
                    }// If Ends
                    }//If Ends
                    }
                  } // For loop 1 Ends
                 } // For loop 2 Ends
                 Database.insert(CascadingKPIShare,false);  
                 
                 
            //NA Operations,China champion assignment
            
             for(Cascading_KPI_Target__c T : CKPI){
             if(T.CountryPickList__c !=null &&  T.CountryPickList__c !='CN-China' && T.CountryPickList__c !='NA-Canada' && T.CountryPickList__c !='NA-Mexico' && T.CountryPickList__c !='NA-United States' ){
                               
           Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
                     cshare.ParentId = T.id;
                    cshare.UserOrGroupId = Label.ConnectNonChinaandNA;
                    cshare.AccessLevel = 'read';
                   cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Country_Champion__c;
                   CascadingKPIShare.add(cshare);
                   }  
                   else if( T.CountryPickList__c !=null && T.CountryPickList__c =='CN-China'){
                   Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
                     cshare.ParentId = T.id;
                    cshare.UserOrGroupId = Label.ConnectChinaGroup;
                    cshare.AccessLevel = 'read';
                   cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Country_Champion__c;
                   CascadingKPIShare.add(cshare);                
                  } 
                  else if(T.CountryPickList__c !=null && (T.CountryPickList__c =='NA-Canada' || T.CountryPickList__c =='NA-Mexico' || T.CountryPickList__c =='NA-United States')){
                    Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
                    cshare.ParentId = T.id;
                    cshare.UserOrGroupId = Label.ConnectNAgroup;
                    cshare.AccessLevel = 'read';
                    cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Country_Champion__c;
                    CascadingKPIShare.add(cshare); 
                    } 
                    } 
                  Database.insert(CascadingKPIShare,false); 
     }
       
    Public Static void ConnectCascadingKPITargetAfterUpdate(List<Cascading_KPI_Target__c> CKPI, Cascading_KPI_Target__c CKPIold)
      {
      
      List<Cascading_KPI_Target__Share>  CascadingKPIShare = new List<Cascading_KPI_Target__Share>();
      
       //NA Operations,China champion assignment
       
       if( CKPIold.CountryPickList__c != CKPI[0].CountryPickList__c){
      List<Cascading_KPI_Target__Share>  CascadingKPISharedelete = [Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId = :CKPIold.Id and RowCause = :Schema.Cascading_KPI_Target__Share.RowCause.Country_Champion__c]; 
     if(CascadingKPISharedelete.size() > 0)              
      Database.delete(CascadingKPISharedelete[0],false);
      }
            
             for(Cascading_KPI_Target__c T : CKPI){
             if(T.CountryPickList__c !=null &&  T.CountryPickList__c !='CN-China' && T.CountryPickList__c !='NA-Canada' && T.CountryPickList__c !='NA-Mexico' && T.CountryPickList__c !='NA-United States' ){
                               
           Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
                     cshare.ParentId = T.id;
                    cshare.UserOrGroupId = Label.ConnectNonChinaandNA;
                    cshare.AccessLevel = 'read';
                   cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Country_Champion__c;
                   CascadingKPIShare.add(cshare);
                   }  
                   else if( T.CountryPickList__c !=null && T.CountryPickList__c =='CN-China'){
                   Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
                     cshare.ParentId = T.id;
                    cshare.UserOrGroupId = Label.ConnectChinaGroup;
                    cshare.AccessLevel = 'read';
                   cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Country_Champion__c;
                   CascadingKPIShare.add(cshare);                
                  } 
                  else if(T.CountryPickList__c !=null && (T.CountryPickList__c =='NA-Canada' || T.CountryPickList__c =='NA-Mexico' || T.CountryPickList__c =='NA-United States')){
                    Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
                    cshare.ParentId = T.id;
                    cshare.UserOrGroupId = Label.ConnectNAgroup;
                    cshare.AccessLevel = 'read';
                    cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Country_Champion__c;
                    CascadingKPIShare.add(cshare); 
                    } 
                    } 
                  Database.insert(CascadingKPIShare,false); 
         
         for(Cascading_KPI_Target__c cascadingkpi : CKPI){
           
           Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
           cshare.ParentId = cascadingkpi.id;
           cshare.UserOrGroupId = cascadingkpi.Entity_Data_Reporter__c;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Entity_Data_Reporter__c;
           CascadingKPIShare.add(cshare);
          }
         Database.insert(CascadingKPIShare,false);
         
     if( CKPIold.Entity_Data_Reporter__c != CKPI[0].Entity_Data_Reporter__c){
      List<Cascading_KPI_Target__Share>  CascadingKPISharedelete = [Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId = :CKPIold.Id and UserOrGroupId = :CKPIold.Entity_Data_Reporter__c ]; 
     if(CascadingKPISharedelete.size() > 0)              
      Database.delete(CascadingKPISharedelete[0],false);
      }
       
      
     }
     
  
           
    }