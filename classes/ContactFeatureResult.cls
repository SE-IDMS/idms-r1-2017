global class ContactFeatureResult{
    // WS error codes & error message
    WebService Integer returnCode;
    WebService String message;
    WebService Integer totalNoOfContactFeature; 
    
    WebService ContactFeature[] ContactFeatures { get; set; }
    WebService Datetime BatchMaxLastModifiedDate {get;set;}
}