@isTest
public class VFC_TrackUserRegStatus_Test{
    @testSetup static void testSetupMethodPRM() {
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
        for(Profile p : ps)
        {
             profiles.put(p.name, p.id);
        }        
                  
        user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null limit 1];
        admin.BypassVR__c =True;
        Test.startTest();
        ID  profilesId;
        system.runas(admin){
            
            Country__c TestCountry = new Country__c();
            TestCountry.Name   = 'India';
            TestCountry.CountryCode__c = 'IN';
            TestCountry.InternationalPhoneCode__c = '91';
            TestCountry.Region__c = 'APAC';
            INSERT TestCountry;
            
            StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
            INSERT testStateProvince;
            
            Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', 
                                    Country__c=TestCountry.id, PRMCompanyName__c='TestAccount',PRMCountry__c=TestCountry.id,PRMBusinessType__c='FI',
                                    PRMAreaOfFocus__c='FI1',PLShowInPartnerLocatorForced__c = true, PRMUIMSID__c ='789dheuu37hsysh3sh3', StateProvince__c = testStateProvince.id,
                                    city__c = 'Bangalore');
            insert PartnerAcc;
          
            Contact PartnerCon = new Contact(
                  FirstName='Test',
                  LastName='lastname',
                  AccountId=PartnerAcc.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  WorkPhone__c='1234567890',
                  Country__c=TestCountry.Id,
                  PRMFirstName__c = 'Test',
                  PRMLastName__c = 'lastname',
                  PRMEmail__c = 'test.lastName@yopmail.com',
                  PRMWorkPhone__c = '999121212',
                  PRMPrimaryContact__c = true,
                  PRMUIMSID__c = '897dheuu37hsysh3sh3',
                  city__c = 'Bangalore'
                  );
                  
                  Insert PartnerCon;
            profilesId = [select id from profile where name='SE - Channel Partner (Community)'].Id; 
                
                  
            User   u1= new User(Username = 'testUserOne@schneider-electric.com'+Label.CLMAR13PRM05, LastName = 'User11', alias = 'tuser1',
                              CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                              Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                PRMTemporarySamlToken__c = 'ASDHFTWEEXD121212', 
                              LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId,ContactID = PartnerCon.Id, UserPermissionsSFContentUser=true, FederationIdentifier = '897dheuu37hsysh3sh3');        
            insert u1;  

            PermissionSet fieloPermissionSet = [Select ID,UserLicenseId from PermissionSet where Name='FieloPRM_SE' Limit 1];
            PermissionSetAssignment newPermissionSetAssignment1 = new PermissionSetAssignment();
            newPermissionSetAssignment1.AssigneeId = u1.id;
            newPermissionSetAssignment1.PermissionSetId = fieloPermissionSet.id;
            Insert newPermissionSetAssignment1; 
            PartnerAcc.Owner = u1;
            UIMSCompany__c uimCom = New UIMSCompany__c( UserFederatedId__c = '897dheuu37hsysh3sh3',
                                                    UimsCompanyId__c = PartnerAcc.PRMUIMSID__c,
                                                    BusinessType__c = 'FI',
                                                    AreaofFocus__c = 'FI3',
                                                    DoNotHaveCompany__c = False,
                                                    SkippedCompanyInfoStep__c = False,
                                                    UIMSActivationStatus__c = True,
                                                    Email__c='test@testmail.com',
                                                    AccountCreationStatus__c = True,
                                                    ContactCreationStatus__c = True,
                                                    MemberCreationStatus__c = True,
                                                    AuthenticationToken__c = 'iijduei-hjejdnndje-njdjjeue',
                                                    UserRegistrationInformation__c = '{"userLanguage":"fr","uimsCompanyId":null,"termsAndConditions":true,"taxIdRequired":false,"taxIdHiddenOnProfile":null,"taxIdHidden":false}'
                                                    );
            insert uimCom;
        }
    }
    /*
    static testMethod void  trackUserRegStatus01_Test(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);

        System.runAs(u){
            PageReference pageRef = Page.VFP_TrackUserRegStatus;
            Test.setCurrentPage(pageRef);
            UIMSCompany__c abccom = New UIMSCompany__c();
            Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(abccom);
            VFC_TrackUserRegStatus thisTrackUserRegStatus = new VFC_TrackUserRegStatus(thisController);
            thisTrackUserRegStatus.refIndex = 0;
            thisTrackUserRegStatus.userFederatedId = '897dheuu37hsysh3sh3';
            //thisTrackUserRegStatus.userEmailId = 'testUser@schneider-electric.com';
            thisTrackUserRegStatus.doSearch();
            thisTrackUserRegStatus.doProvisionbFOAccess();
            
        }
    }
    static testMethod void  trackUserRegStatus02_Test(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);

        System.runAs(u){
            PageReference pageRef = Page.VFP_TrackUserRegStatus;
            Test.setCurrentPage(pageRef);
            UIMSCompany__c abccom = New UIMSCompany__c();
            Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(abccom);
            VFC_TrackUserRegStatus thisTrackUserRegStatus = new VFC_TrackUserRegStatus(thisController);
            thisTrackUserRegStatus.refIndex = 0;
            //thisTrackUserRegStatus.userFederatedId = '897dheuu37hsysh3sh3';
            thisTrackUserRegStatus.userEmailId = 'testUserOne@schneider-electric.com';
            thisTrackUserRegStatus.doSearch();
            thisTrackUserRegStatus.doProvisionbFOAccess();
        }
    }*/
    static testMethod void  trackUserRegStatus03_Test(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        Account acc = [SELECT Id,PRMUIMSID__c FROM Account WHERE Name = 'TestAccount'];
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);

        System.runAs(u){
            Test.startTest();          

             AP_UserRegModel usr = new AP_UserRegModel();
        
        usr.skippedCompanyStep=true;
        usr.email='test@testmail.com';
        
        
        
        UIMSCompany__c UIMSLead123=[Select Id,UserRegistrationInformation__c From UIMSCompany__c Where UserFederatedId__c = '897dheuu37hsysh3sh3'];
        UIMSLead123.UserRegistrationInformation__c=(String)JSON.serialize(usr);
        System.debug('Update4567 :'+UIMSLead123.UserRegistrationInformation__c);
        update UIMSLead123;        
        System.debug('Update456 :'+UIMSLead123.UserRegistrationInformation__c);
        
        
            PageReference pageRef = Page.VFP_TrackUserRegStatus;
            Test.setCurrentPage(pageRef);
            UIMSCompany__c abccom = New UIMSCompany__c();
            abccom.UIMSCompanyId__c = acc.PRMUIMSID__c;
            Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(abccom);
            VFC_TrackUserRegStatus thisTrackUserRegStatus = new VFC_TrackUserRegStatus(thisController);
            thisTrackUserRegStatus.refIndex = 0;
            thisTrackUserRegStatus.userFederatedId = '897dheuu37hsysh3sh3';
            thisTrackUserRegStatus.userEmailId = 'testUserOne@schneider-electric.com';
            thisTrackUserRegStatus.doSearch();
            String mail='test@testmail.com';
            
            String Str = [Select Id,UserRegistrationInformation__c From UIMSCompany__c Where UserFederatedId__c = '897dheuu37hsysh3sh3'].UserRegistrationInformation__c;
            thisTrackUserRegStatus.getUserRegModel(Str);   
            
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
            Id jobId=VFC_TrackUserRegStatus.doProvisionbFOAccess(mail);
            //VFC_TrackUserRegStatus.jobtimeoutStatus(jobId);
            Test.stopTest();
           } 
        }
    }