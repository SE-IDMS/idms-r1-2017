public with sharing class Utils_SellingCenterPicklistManager implements Utils_PicklistManager{
// Return name of the levels
    Public list<String> getPickListLabels()
    {
        List<String> Labels = new List<String>();
        Labels.add(Label.CL10090);
        Labels.add(Label.CL10091);
        return Labels;
    } 

    // Return All Picklist Values 
    public List<SelectOption> getPicklistValues(Integer i, String S)
    {
       List<SelectOption> options = new  List<SelectOption>();
       
       options.add(new SelectOption(Label.CL00355,Label.CL00355)); 
 
       // Populate Business fields and Country when Page Loads
       if(i==1)
       {
            // Populate Business Picklist Values
            Schema.DescribeFieldResult business = Schema.sObjectType.OPP_SellingCenter__c.fields.Business__c;
            for (Schema.PickListEntry PickVal : business.getPicklistValues())
            {
                options.add(new SelectOption(PickVal.getValue(),PickVal.getLabel()));
            }
       }
       if(i==2)
       {
            // Populate Country Picklist Values
            for(Country__c countries: [Select Id, Name from Country__c order by Name asc])
            {
                options.add(new SelectOption(countries.Id,countries.Name));
            }
       }  

       return  options;  
    }
}