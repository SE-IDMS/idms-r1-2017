/*
    Author          : Kiran Kareddy (Schneider Electric)
    Date Created    :  16-Feb-2012
    Description     : Test Class for ConnectTeamMembersTriggers 
*/
@isTest
private class ConnectTeamMembersTriggers_Test {
static testMethod void testConnectTeamMembers() {
 User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connect8');
        System.runAs(runAsUser){
            Initiatives__c inti=Utils_TestMethods_Connect.createInitiative();
            insert inti;
Project_NCP__c cprogram=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;'+Label.Connect_Smart_Cities,GSC_Regions__c='test;test1',Smart_Cities_Division__c='test;test1');
insert cprogram;
Champions__c C=new Champions__c(Scope__c='Global Functions',Entity__c='GM',IsDefault__c ='Yes');
insert C;
system.debug('Champion name: '+C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName);
Champions__c C1=new Champions__c(Scope__c='Global Business',Entity__c='ITB',IsDefault__c ='Yes');
insert c1;
Champions__c C2=new Champions__c(Scope__c='Global Business',Entity__c='Power',IsDefault__c ='Yes');
insert c2;
Champions__c C3=new Champions__c(Scope__c='Global Business',Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test',IsDefault__c ='Yes');
insert c3;
Champions__c C5=new Champions__c(Scope__c='Global Business',Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test1',IsDefault__c ='Yes');
insert c5;
Champions__c C4=new Champions__c(Scope__c='Global Functions',Entity__c='GSC-Regional',GSC_Regions__c='test',IsDefault__c ='Yes');
insert c4;
Champions__c C6=new Champions__c(Scope__c='Global Functions',Entity__c='GSC-Regional',GSC_Regions__c='test1',IsDefault__c ='Yes');
insert c6;
Team_Members__c teammember1=new Team_Members__c(Program__c=cprogram.id,Team_Name__c='Global Functions', Entity__c='GM');
insert teammember1;
Team_Members__c teammember9=new Team_Members__c(Program__c=cprogram.id,Team_Name__c='Global Functions', Entity__c='GSC-Regional',GSC_Region__c='test');
insert teammember9;
Team_Members__c teammember3= [SELECT ValidateScopeonInsertUpdate__c,Champion_Name__c FROM Team_Members__c WHERE Id =:teammember1.Id];
System.assert(teammember3.ValidateScopeonInsertUpdate__c==false);
System.assertEquals(C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName,teammember3.Champion_Name__c);
Team_Members__c teammember2=new Team_Members__c(Program__c=cprogram.id,Team_Name__c='Global Business', Entity__c='ITB');
insert teammember2;
Team_Members__c teammember8=new Team_Members__c(Program__c=cprogram.id,Team_Name__c='Global Business', Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test');
insert teammember8;
Team_Members__c teammember7=new Team_Members__c(Program__c=cprogram.id,Team_Name__c='Global Functions', Entity__c='Testing');
try{
insert teammember7;
}catch(DmlException d)
{
d.getMessage();
}
teammember2.Entity__c='Power';
Test.startTest();
try{
update teammember2;
}
catch(DmlException d)
{
d.getMessage();
}
Team_Members__c teammember4= [SELECT ValidateScopeonInsertUpdate__c,Champion_Name__c FROM Team_Members__c WHERE Id =:teammember2.Id];
System.assertEquals(C2.Champion_Name__r.FirstName + ' ' + C2.Champion_Name__r.LastName,teammember4.Champion_Name__c);

delete teammember2;
teammember8.Smart_Cities_Division__c='test1';
update teammember8;
teammember9.GSC_Region__c='test1';
update teammember9;

Test.stopTest();
}
}
}