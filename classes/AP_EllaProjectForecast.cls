public with sharing class AP_EllaProjectForecast{

    public static boolean firstTime=true;
    public static boolean firstTime2=true;
    public static boolean firstTime3=true;


    public static void makeZero(Map<id, Milestone1_Project__c> changedSellDateOldMap, Map<id, Milestone1_Project__c> changedSellDateNewMap){
        MapsWrapper mw = makeZeroWithoutCalc(changedSellDateOldMap, changedSellDateNewMap);
        if (mw!=null && mw.toUpdate.size()>0) {
            calcAll(mw);
        }
    }

    public static MapsWrapper makeZeroWithoutCalc(Map<id, Milestone1_Project__c> changedSellDateOldMap, Map<id, Milestone1_Project__c> changedSellDateNewMap){
    
        if (firstTime2){
            firstTime2=false;
            System.debug('This time NOT called');
        } else {
            System.debug('This time called');
            List<OfferLifeCycleForecast__c> listofferLife = new List<OfferLifeCycleForecast__c> ();
    
            listofferLife=[Select id,Month__c,Project__c,BU__c,Actual__c,Country__c,ForecastQuarter__c,ForcastYear__c, Project__r.Country_Announcement_Date__c,Project__r.Country_End_of_Commercialization_Date__c, ForecastMonthbyNumber__c from OfferLifeCycleForecast__c
                 where Project__c IN :changedSellDateNewMap.keySet()]; // AND ForecastMonthbyNumber__c!=null
     
            Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > > projToListofferLifeMap=new Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > >(); //id, Year, Month, Forecast
    
            Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > > projToNewListofferLifeMap=new Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > >(); //id, Year, Month, Forecast
            
            Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > > projQuarterOfferLifeMap=new Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > >(); //id, Year, Quarter, Forecast
            Map< Id, Map< Integer, List<OfferLifeCycleForecast__c> > > projYearOfferLifeMap=new Map< Id, Map< Integer, List<OfferLifeCycleForecast__c> > >(); //id, Year, Forecast
    
            for (OfferLifeCycleForecast__c currOfferLife :listofferLife){
                if (currOfferLife.Month__c!=label.CLAPRIL14COINbFO28 && currOfferLife.Month__c!=null && currOfferLife.Month__c!=''){ //label.CLAPRIL14COINbFO28: Year
                    if (!projToListofferLifeMap.keySet().contains(currOfferLife.Project__c)){
                        projToListofferLifeMap.put(currOfferLife.Project__c, new Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> >());
                        projToNewListofferLifeMap.put(currOfferLife.Project__c, new Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> >());
                    }
                    if (!projToListofferLifeMap.get(currOfferLife.Project__c).keySet().contains(Integer.valueOf(currOfferLife.ForcastYear__c))){
                        projToListofferLifeMap.get(currOfferLife.Project__c).put(Integer.valueOf(currOfferLife.ForcastYear__c), new Map<Integer, List<OfferLifeCycleForecast__c>>());
                        projToNewListofferLifeMap.get(currOfferLife.Project__c).put(Integer.valueOf(currOfferLife.ForcastYear__c), new Map<Integer, List<OfferLifeCycleForecast__c>>());
                    }
                    if (!projToListofferLifeMap.get(currOfferLife.Project__c).get(Integer.valueOf(currOfferLife.ForcastYear__c)).keySet().contains(Integer.valueOf(currOfferLife.ForecastMonthbyNumber__c))){
                        projToListofferLifeMap.get(currOfferLife.Project__c).get(Integer.valueOf(currOfferLife.ForcastYear__c)).put(Integer.valueOf(currOfferLife.ForecastMonthbyNumber__c), new List<OfferLifeCycleForecast__c>());
                        projToNewListofferLifeMap.get(currOfferLife.Project__c).get(Integer.valueOf(currOfferLife.ForcastYear__c)).put(Integer.valueOf(currOfferLife.ForecastMonthbyNumber__c), new List<OfferLifeCycleForecast__c>());
                    }
                    
                    projToListofferLifeMap.get(currOfferLife.Project__c).get(Integer.valueOf(currOfferLife.ForcastYear__c)).get(Integer.valueOf(currOfferLife.ForecastMonthbyNumber__c)).add(currOfferLife); //.clone()
                    projToNewListofferLifeMap.get(currOfferLife.Project__c).get(Integer.valueOf(currOfferLife.ForcastYear__c)).get(Integer.valueOf(currOfferLife.ForecastMonthbyNumber__c)).add(currOfferLife);
                } else if (currOfferLife.Month__c==null || currOfferLife.Month__c==''){
                    if (!projQuarterOfferLifeMap.keySet().contains(currOfferLife.Project__c)){
                        projQuarterOfferLifeMap.put(currOfferLife.Project__c, new Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> >());
                    }
                    if (!projQuarterOfferLifeMap.get(currOfferLife.Project__c).keySet().contains(Integer.valueOf(currOfferLife.ForcastYear__c))){
                        projQuarterOfferLifeMap.get(currOfferLife.Project__c).put(Integer.valueOf(currOfferLife.ForcastYear__c), new Map<Integer, List<OfferLifeCycleForecast__c>>());
                    }
                    if (!projQuarterOfferLifeMap.get(currOfferLife.Project__c).get(Integer.valueOf(currOfferLife.ForcastYear__c)).keySet().contains(Integer.valueOf(currOfferLife.ForecastQuarter__c.subString(currOfferLife.ForecastQuarter__c.length()-1, currOfferLife.ForecastQuarter__c.length())))){
                        projQuarterOfferLifeMap.get(currOfferLife.Project__c).get(Integer.valueOf(currOfferLife.ForcastYear__c)).put(Integer.valueOf(currOfferLife.ForecastQuarter__c.subString(currOfferLife.ForecastQuarter__c.length()-1, currOfferLife.ForecastQuarter__c.length())), new List<OfferLifeCycleForecast__c>());
                    }
                    
                    projQuarterOfferLifeMap.get(currOfferLife.Project__c).get(Integer.valueOf(currOfferLife.ForcastYear__c)).get(Integer.valueOf(currOfferLife.ForecastQuarter__c.subString(currOfferLife.ForecastQuarter__c.length()-1, currOfferLife.ForecastQuarter__c.length()))).add(currOfferLife);
                }  else if (currOfferLife.Month__c==label.CLAPRIL14COINbFO28){
                    if (!projYearOfferLifeMap.keySet().contains(currOfferLife.Project__c)){
                        projYearOfferLifeMap.put(currOfferLife.Project__c, new Map< Integer, List<OfferLifeCycleForecast__c> >());
                    }
                    if (!projYearOfferLifeMap.get(currOfferLife.Project__c).keySet().contains(Integer.valueOf(currOfferLife.ForcastYear__c))){
                        projYearOfferLifeMap.get(currOfferLife.Project__c).put(Integer.valueOf(currOfferLife.ForcastYear__c), new List<OfferLifeCycleForecast__c>());
                    }
                    
                    projYearOfferLifeMap.get(currOfferLife.Project__c).get(Integer.valueOf(currOfferLife.ForcastYear__c)).add(currOfferLife);
                }
                //projToListofferLifeMap.get(currOfferLife.Project__c).add(currOfferLife);
            }
    
        List<OfferLifeCycleForecast__c> toUpdate=new List<OfferLifeCycleForecast__c>();
        for (Id projId: projToListofferLifeMap.keySet()){
            for (Integer Year: projToListofferLifeMap.get(projId).keySet()) {
                //listofferLife = projToListofferLifeMap.get(projId);
                //listofferLife = projToListofferLifeMap.get(projId).;
                //for (Integer i=0;i<listofferLife.size();i++){
                for (Integer Month: projToListofferLifeMap.get(projId).get(Year).keySet()) {
                    //OfferLifeCycleForecast__c curr=listofferLife.get(i);
                    OfferLifeCycleForecast__c curr=projToListofferLifeMap.get(projId).get(Year).get(Month).get(0);
                    //System.debug('1 curr.id:'+curr.id);
                    //System.debug('1 curr:'+curr);
                    System.debug('11111111'+curr+''+curr.Project__r.Country_Announcement_Date__c);
                    if(curr.Project__r.Country_Announcement_Date__c != null){
                        if ( ( Integer.valueOf(curr.ForcastYear__c)<curr.Project__r.Country_Announcement_Date__c.year() )|| ( Integer.valueOf(curr.ForcastYear__c)==curr.Project__r.Country_Announcement_Date__c.year() && ( ( curr.Month__c!=null && curr.Month__c!='' && curr.ForecastMonthbyNumber__c != null) && Integer.valueOf(curr.ForecastMonthbyNumber__c)<curr.Project__r.Country_Announcement_Date__c.month() ) )) { // && curr.ForecastQuarter__c!=''
                            resetValues(curr, toUpdate);
                        }
                    }else if(curr.Project__r.Country_End_of_Commercialization_Date__c != null){
                        if ( (Integer.valueOf(curr.ForcastYear__c)>curr.Project__r.Country_End_of_Commercialization_Date__c.year() )|| ( Integer.valueOf(curr.ForcastYear__c)==curr.Project__r.Country_End_of_Commercialization_Date__c.year() && ( ( curr.Month__c!=null && curr.Month__c!='' && curr.ForecastMonthbyNumber__c != null) && Integer.valueOf(curr.ForecastMonthbyNumber__c)>curr.Project__r.Country_End_of_Commercialization_Date__c.month()))) { // && curr.ForecastQuarter__c!=''
                            resetValues(curr, toUpdate);
                        }  
                    }
                }
            }
    
        }
        //update toUpdate;    
            MapsWrapper mw=new MapsWrapper();
    //mw setting start
            mw.changedSellDateOldMap=changedSellDateOldMap;
            mw.changedSellDateNewMap=changedSellDateNewMap;
            mw.toUpdate=toUpdate;
            mw.projQuarterOfferLifeMap=projQuarterOfferLifeMap; 
            mw.projToNewListofferLifeMap=projToNewListofferLifeMap; 
    
            mw.projToListofferLifeMap=projToListofferLifeMap; //id, Year, Month, Forecast
            mw.projToNewListofferLifeMap=projToNewListofferLifeMap; //id, Year, Month, Forecast
            mw.projQuarterOfferLifeMap=projQuarterOfferLifeMap; //id, Year, Quarter, Forecast
            mw.projYearOfferLifeMap=projYearOfferLifeMap; //id, Year, Forecast
    
            return mw;
    /*
            if (mw.toUpdate.size()>0) {
                calcAll(mw);
            }
    */
    
        }
        return null;
    }
	/* This method is to reset the value of forecast and update in the list*/
    public static void resetValues(OfferLifeCycleForecast__c offerLifeCycFore, List<OfferLifeCycleForecast__c> toUpdate){
		offerLifeCycFore.Country__c=0;
		offerLifeCycFore.Actual__c=0;
		offerLifeCycFore.BU__c=0;
		toUpdate.add(offerLifeCycFore);
	}

    public static void calcAll(MapsWrapper mw){
        calcQuarter(mw);
        calcYear(mw);
        
        if (mw.toUpdate!=null && mw.toUpdate.size()>0){
            update mw.toUpdate;
        }
    }

    public class MapsWrapper{
        Map<id, Milestone1_Project__c> changedSellDateOldMap;
        Map<id, Milestone1_Project__c> changedSellDateNewMap;
        List<OfferLifeCycleForecast__c> toUpdate;
        
        Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > > projToListofferLifeMap; //id, Year, Month, Forecast
        Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > > projToNewListofferLifeMap; //id, Year, Month, Forecast
        Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > > projQuarterOfferLifeMap; //id, Year, Quarter, Forecast
        Map< Id, Map< Integer, List<OfferLifeCycleForecast__c> > > projYearOfferLifeMap; //id, Year, Forecast
        
    }

    public static void calcQuarter(MapsWrapper mw){
        Map<id, Milestone1_Project__c> changedSellDateOldMap=mw.changedSellDateOldMap;
        Map<id, Milestone1_Project__c> changedSellDateNewMap=mw.changedSellDateNewMap;
        List<OfferLifeCycleForecast__c> toUpdate=mw.toUpdate;
        Map<Id, Map<Integer, Map<Integer, List<OfferLifeCycleForecast__c>>>> projQuarterOfferLifeMap=mw.projQuarterOfferLifeMap;    
        Map<Id, Map<Integer, Map<Integer, List<OfferLifeCycleForecast__c>>>> projToNewListofferLifeMap=mw.projToNewListofferLifeMap;    
        Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > > projToListofferLifeMap=mw.projToListofferLifeMap; //id, Year, Month, Forecast
        Map< Id, Map< Integer, List<OfferLifeCycleForecast__c> > > projYearOfferLifeMap=mw.projYearOfferLifeMap; //id, Year, Forecast
        for (Id ProjId: projToListofferLifeMap.keySet()){
            Map<Integer, Map<Integer, List<OfferLifeCycleForecast__c>>> quarterofferLifeMap = projQuarterOfferLifeMap.get(projId);  
            Map<Integer, Map<Integer, List<OfferLifeCycleForecast__c>>> newlistofferLifeMap = projToNewListofferLifeMap.get(projId);    
            //Map<Integer, Map<Integer, List<OfferLifeCycleForecast__c>>> listofferLifeMap = projQuarterOfferLifeMap.get(projId);   
            
            for (Integer Year: quarterofferLifeMap.keySet()){
                for (Integer currQuarter: quarterofferLifeMap.get(Year).keySet()) {
                    System.debug('Year:'+Year);
                    System.debug('currQuarter:'+currQuarter);
                    //Integer currQuarterNumber=Integer.valueOf(currQuarter.subString(currQuarter.length()-1, currQuarter.length()));
                    Integer currQuarterNumber=currQuarter;
                    Decimal currQuarterActualSum=0;
                    Decimal currQuarterCountrySum=0;
                    Decimal currQuarterBUSum=0;
                    for (Integer monthi=((currQuarterNumber-1)*3) + 1;monthi<=currQuarterNumber*3; monthi++) {
                        if (newlistofferLifeMap.keySet().contains(Year) && newlistofferLifeMap.get(Year).keySet().contains(monthi)) { 
                            currQuarterActualSum=currQuarterActualSum+newlistofferLifeMap.get(Year).get(monthi).get(0).Actual__c;
                            currQuarterCountrySum=currQuarterCountrySum+newlistofferLifeMap.get(Year).get(monthi).get(0).Country__c;
                            currQuarterBUSum=currQuarterBUSum+newlistofferLifeMap.get(Year).get(monthi).get(0).BU__c;
                        }
                    }
                    quarterofferLifeMap.get(Year).get(currQuarter).get(0).Actual__c=currQuarterActualSum;
                    quarterofferLifeMap.get(Year).get(currQuarter).get(0).Country__c=currQuarterCountrySum;
                    quarterofferLifeMap.get(Year).get(currQuarter).get(0).BU__c=currQuarterBUSum;
                    toUpdate.add(quarterofferLifeMap.get(Year).get(currQuarter).get(0));
                    
                    
                }
            }
        }    
    }




    public static void calcYear(MapsWrapper mw){
        Map<id, Milestone1_Project__c> changedSellDateOldMap=mw.changedSellDateOldMap;
        Map<id, Milestone1_Project__c> changedSellDateNewMap=mw.changedSellDateNewMap;
        List<OfferLifeCycleForecast__c> toUpdate=mw.toUpdate;
    
        Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > > projToListofferLifeMap=mw.projToListofferLifeMap; //id, Year, Month, Forecast
        Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > > projToNewListofferLifeMap=mw.projToNewListofferLifeMap; //id, Year, Month, Forecast
        Map< Id, Map< Integer, Map<Integer, List<OfferLifeCycleForecast__c>> > > projQuarterOfferLifeMap=mw.projQuarterOfferLifeMap; //id, Year, Quarter, Forecast
        Map< Id, Map< Integer, List<OfferLifeCycleForecast__c> > > projYearOfferLifeMap=mw.projYearOfferLifeMap; //id, Year, Forecast
    
    
            //List<OfferLifeCycleForecast__c> toUpdate=new List<OfferLifeCycleForecast__c>();
        if(!projToListofferLifeMap.isEmpty()){
            
            for (Id ProjId: projToListofferLifeMap.keySet()){            
                Map<Integer, List<OfferLifeCycleForecast__c>> yearofferLifeMap = new Map<Integer, List<OfferLifeCycleForecast__c>>();
                if(!projYearOfferLifeMap.isEmpty() && projYearOfferLifeMap.containsKey(projId))
                    yearofferLifeMap = projYearOfferLifeMap.get(projId); 
                Map<Integer, Map<Integer, List<OfferLifeCycleForecast__c>>> newlistofferLifeMap = new Map<Integer, Map<Integer, List<OfferLifeCycleForecast__c>>>();
                if(!projToNewListofferLifeMap.isEmpty() && projToNewListofferLifeMap.containsKey(projId))
                    newlistofferLifeMap = projToNewListofferLifeMap.get(projId);    
                //Map<Integer, Map<Integer, List<OfferLifeCycleForecast__c>>> listofferLifeMap = projQuarterOfferLifeMap.get(projId);               
                if(!yearofferLifeMap.isEmpty()){
                    for (Integer Year: yearofferLifeMap.keySet()){
                        System.debug('Year:'+Year);
                        
                        //Integer currQuarterNumber=Integer.valueOf(currQuarter.subString(currQuarter.length()-1, currQuarter.length()));
                        Decimal currYearActualSum=0;
                        Decimal currYearCountrySum=0;
                        Decimal currYearBUSum=0;
                        for (Integer monthi=1;monthi<=12; monthi++) {
                            if (newlistofferLifeMap.keySet().contains(Year) && newlistofferLifeMap.get(Year).keySet().contains(monthi)) { 
                                currYearActualSum=currYearActualSum+newlistofferLifeMap.get(Year).get(monthi).get(0).Actual__c;
                                currYearCountrySum=currYearCountrySum+newlistofferLifeMap.get(Year).get(monthi).get(0).Country__c;
                                currYearBUSum=currYearBUSum+newlistofferLifeMap.get(Year).get(monthi).get(0).BU__c;
                            }
                        }
                        yearofferLifeMap.get(Year).get(0).Actual__c=currYearActualSum;
                        yearofferLifeMap.get(Year).get(0).Country__c=currYearCountrySum;
                        yearofferLifeMap.get(Year).get(0).BU__c=currYearBUSum;
                        toUpdate.add(yearofferLifeMap.get(Year).get(0));
                    }
                }
            }
        }
    }

}