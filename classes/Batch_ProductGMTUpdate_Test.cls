/*  Test For Batch_Updated */

@isTest
private class Batch_ProductGMTUpdate_Test {

    static testMethod void TestProductGMTUpdate(){

        //Inserting ReferentialDataMapping__c Records
        List<ReferentialDataMapping__c> refDataMap = new List<ReferentialDataMapping__c>();
        ReferentialDataMapping__c ref2 = new ReferentialDataMapping__c();
            ref2.fromfield__c='parenthierarchy__r.BusinessUnit__c';
            ref2.fromobject__c='OPP_Product__c';
            ref2.tofield__c='TECH_CROrderBusiness__c';
            ref2.active__c=true;
            ref2.whereclausefromfield__c='name';
            //ref2.name='parenthierarchy__r.BusinessUnit__cToTECH_CROrderBusiness__c';
            ref2.whereclausetofield__c='CommercialReferenceOrdered__c';
            ref2.toobject__c='ComplaintRequest__c';
            
            refDataMap.add(ref2);
        ReferentialDataMapping__c ref4 = new ReferentialDataMapping__c();
            ref4.fromfield__c='BusinessUnit__c';
            ref4.fromobject__c='OPP_Product__c';
            ref4.tofield__c='Status__c';
            ref4.active__c=true;
            ref4.whereclausefromfield__c='name';
            //ref4.name='parenthierarchy__r.BusinessUnit__cToTECH_CROrderBusiness__c Error';
            ref4.whereclausetofield__c='CommercialReferenceOrdered__c';
            ref4.toobject__c='ComplaintRequest__c';
        
            refDataMap.add(ref4);
            
        insert refDataMap;
        //Inserting From Object Records
        OPP_Product__c OppPdct = new OPP_Product__c(
        name='REPARATIONS Error',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='02BQ8SZ2------0DZH', 
        businessunit__c='Open',isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false);
        insert OppPdct;
        
        List<OPP_Product__c> ListPdct=new List<OPP_Product__c>();
        OPP_Product__c OppPdct1 = new OPP_Product__c(
        name='REPARATIONS I H M',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY', 
        businessunit__c='Open', parenthierarchy__c=OppPdct.id,isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false);
        ListPdct.add(OppPdct1);
        
        OPP_Product__c OppPdct2 = new OPP_Product__c(
        name='REPARATIONS I H M',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY', 
        businessunit__c='INDUSTRIAL AUTOMATION', parenthierarchy__c=OppPdct.id,isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false
        );
        ListPdct.add(OppPdct2);
        
        OPP_Product__c OppPdct3 = new OPP_Product__c(
        name='REPARATIONS I H M',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY', 
        businessunit__c='INDUSTRIAL AUTOMATION', parenthierarchy__c=OppPdct.id,isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false
        );
        ListPdct.add(OppPdct3);
        
        insert ListPdct;
        
        //Inserting To Object Records
        Country__c Cntry = Utils_TestMethods.createCountry();
        Insert Cntry;

        BusinessRiskEscalationEntity__c BsnsRskEsc = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1',SubEntity__c='LCR Sub-Entity',Location__c='LCR Location',
                                            Location_Type__c= 'Return Center',Street__c = 'LCR Street',RCPOBox__c = '123456',RCCountry__c = Cntry.Id,RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',ContactEmail__c ='Test@schneider.com');
        Insert BsnsRskEsc; 

        List<ComplaintRequest__c> CompReq=new List<ComplaintRequest__c>();
        ComplaintRequest__c CompReq1 = new ComplaintRequest__c(
        CommercialReferenceOrdered__c='REPARATIONS I H M',
        letterofreserve__c=false, forcemanualselection__c=false, infotransport__c='No', infolac__c='No', tech_answer__c=false, category__c='Product Selection & Services', 
        leadstoproductqualityissue__c='No', status__c='Open', tech_symptomsavailable__c=false, tech_forward__c=false, needtolaunchtechnicalcomplaint__c='No', reportingentity__c=BsnsRskEsc.id, 
        tech_isblankcomment__c=true, doescustomerreturnproduct__c='No',  accountableorganization__c=BsnsRskEsc.id, accountableorganizationworking__c=false, infoquality__c='No', tech_external1__c=false, 
        reason__c='Catalog Selection', tech_accountable_org__c=false, qualitycheck__c=false, stockcheckrequired__c=false, priority__c='Normal', countforccl__c=false, 
        tech_ischeck__c=false, tech_isexternal__c='No', infoccc__c='No'
        );
        CompReq.add(CompReq1);
        
        ComplaintRequest__c CompReq2 = new ComplaintRequest__c(
        CommercialReferenceOrdered__c='REPARATIONS Error',
        letterofreserve__c=false, forcemanualselection__c=false, infotransport__c='No', infolac__c='No', tech_answer__c=false, category__c='Product Selection & Services', 
        leadstoproductqualityissue__c='No', status__c='Closed', tech_symptomsavailable__c=false, tech_forward__c=false, needtolaunchtechnicalcomplaint__c='No', reportingentity__c=BsnsRskEsc.id,
        tech_isblankcomment__c=true, doescustomerreturnproduct__c='No',  accountableorganization__c=BsnsRskEsc.id,accountableorganizationworking__c=false, infoquality__c='No', tech_external1__c=false, 
        reason__c='Catalog Selection', tech_accountable_org__c=false, qualitycheck__c=false, stockcheckrequired__c=false, priority__c='Normal', countforccl__c=false, 
        tech_ischeck__c=false, tech_isexternal__c='No', infoccc__c='No', SatisfactionOnQualityofAnswer__c='ABCDEFGHIJKLMNOPQRSTUVWXYZ', FinalResolution__c='ABCD', Final_Resolution__c='EFGH', SatisfactionOnSpeedofAnswer__c='ABCD'
        );
        //Final Resolution, 
        CompReq.add(CompReq2);
        insert CompReq;
         
        Batch_ProductGMTUpdate BtchPdct = new Batch_ProductGMTUpdate(false,'ComplaintRequest__c');
        Test.startTest();
        Database.executeBatch(BtchPdct);
        Test.stopTest();
    }
    static testMethod void TestProductGMTUpdate1(){

        //Inserting ReferentialDataMapping__c Records
        List<ReferentialDataMapping__c> refDataMap = new List<ReferentialDataMapping__c>();
        
        ReferentialDataMapping__c ref2 = new ReferentialDataMapping__c();
            ref2.fromfield__c='parenthierarchy__r.BusinessUnit__c';
            ref2.fromobject__c='OPP_Product__c';
            ref2.tofield__c='TECH_CROrderBusiness__c';
            ref2.active__c=true;
            ref2.whereclausefromfield__c='name';
            //ref2.name='parenthierarchy__r.BusinessUnit__cToTECH_CROrderBusiness__c';
            ref2.whereclausetofield__c='CommercialReferenceOrdered__c';
            ref2.toobject__c='ComplaintRequest__c';
            
            refDataMap.add(ref2);
        ReferentialDataMapping__c ref4 = new ReferentialDataMapping__c();
            ref4.fromfield__c='BusinessUnit__c';
            ref4.fromobject__c='OPP_Product__c';
            ref4.tofield__c='Status__c';
            ref4.active__c=true;
            ref4.whereclausefromfield__c='name';
            //ref4.name='parenthierarchy__r.BusinessUnit__cToTECH_CROrderBusiness__c Error';
            ref4.whereclausetofield__c='CommercialReferenceOrdered__c';
            ref4.toobject__c='ComplaintRequest__c';
        
            refDataMap.add(ref4);
            
        insert refDataMap;

        
        //Inserting From Object Records
        OPP_Product__c OppPdct = new OPP_Product__c(
        name='REPARATIONS Error',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='02BQ8SZ2------0DZH', 
        businessunit__c='Open',isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false);
        insert OppPdct;
        
        List<OPP_Product__c> ListPdct=new List<OPP_Product__c>();
        OPP_Product__c OppPdct1 = new OPP_Product__c(
        name='REPARATIONS I H M',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY', 
        businessunit__c='Open', parenthierarchy__c=OppPdct.id,isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false);
        ListPdct.add(OppPdct1);
        
        OPP_Product__c OppPdct2 = new OPP_Product__c(
        name='REPARATIONS I H M',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY', 
        businessunit__c='INDUSTRIAL AUTOMATION', parenthierarchy__c=OppPdct.id,isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false
        );
        ListPdct.add(OppPdct2);
        
        OPP_Product__c OppPdct3 = new OPP_Product__c(
        name='REPARATIONS I H M',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY', 
        businessunit__c='INDUSTRIAL AUTOMATION', parenthierarchy__c=OppPdct.id,isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false
        );
        ListPdct.add(OppPdct3);
        
        insert ListPdct;
        
        //Inserting To Object Records
        Country__c Cntry = Utils_TestMethods.createCountry();
        Insert Cntry;

         BusinessRiskEscalationEntity__c BsnsRskEsc = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1',SubEntity__c='LCR Sub-Entity',Location__c='LCR Location',
                                            Location_Type__c= 'Return Center',Street__c = 'LCR Street',RCPOBox__c = '123456',RCCountry__c = Cntry.Id,RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',ContactEmail__c ='Test@schneider.com');
        Insert BsnsRskEsc; 
        
        List<ComplaintRequest__c> CompReq=new List<ComplaintRequest__c>();
        ComplaintRequest__c CompReq1 = new ComplaintRequest__c(
        CommercialReferenceOrdered__c='REPARATIONS I H M',
        letterofreserve__c=false, forcemanualselection__c=false, infotransport__c='No', infolac__c='No', tech_answer__c=false, category__c='Product Selection & Services', 
        leadstoproductqualityissue__c='No', status__c='Open', tech_symptomsavailable__c=false, tech_forward__c=false, needtolaunchtechnicalcomplaint__c='No', reportingentity__c=BsnsRskEsc.id, 
        tech_isblankcomment__c=true, doescustomerreturnproduct__c='No',  accountableorganization__c=BsnsRskEsc.id, accountableorganizationworking__c=false, infoquality__c='No', tech_external1__c=false, 
        reason__c='Catalog Selection', tech_accountable_org__c=false, qualitycheck__c=false, stockcheckrequired__c=false, priority__c='Normal', countforccl__c=false, 
        tech_ischeck__c=false, tech_isexternal__c='No', infoccc__c='No'
        );
        CompReq.add(CompReq1);
        
        ComplaintRequest__c CompReq2 = new ComplaintRequest__c(
        CommercialReferenceOrdered__c='REPARATIONS Error',
        letterofreserve__c=false, forcemanualselection__c=false, infotransport__c='No', infolac__c='No', tech_answer__c=false, category__c='Product Selection & Services', 
        leadstoproductqualityissue__c='No', status__c='Closed', tech_symptomsavailable__c=false, tech_forward__c=false, needtolaunchtechnicalcomplaint__c='No', reportingentity__c=BsnsRskEsc.id,
        tech_isblankcomment__c=true, doescustomerreturnproduct__c='No',  accountableorganization__c=BsnsRskEsc.id,accountableorganizationworking__c=false, infoquality__c='No', tech_external1__c=false, 
        reason__c='Catalog Selection', tech_accountable_org__c=false, qualitycheck__c=false, stockcheckrequired__c=false, priority__c='Normal', countforccl__c=false, 
        tech_ischeck__c=false, tech_isexternal__c='No', infoccc__c='No', SatisfactionOnQualityofAnswer__c='ABCDEFGHIJKLMNOPQRSTUVWXYZ', FinalResolution__c='ABCD', Final_Resolution__c='EFGH', SatisfactionOnSpeedofAnswer__c='ABCD'
        );
        //Final Resolution, 
        CompReq.add(CompReq2);
        insert CompReq;
        
        list<ReferentialDataError__c> refEorrorList = new list<ReferentialDataError__c>();
        ReferentialDataError__c refDataError = new ReferentialDataError__c();
            refDataError.ErrorInFields__c = 'ErrorInFields__c';
            refDataError.ErrorMessage__c = 'ErrorMessage__c';
            refDataError.IDofToObject__c = CompReq1.id;
            refDataError.ObjectName__c = 'ObjectName__c';
            refDataError.ReRun__c = true;
            refDataError.StatusCode__c = 'StatusCode__c';
            refEorrorList.add(refDataError);
        
        ReferentialDataError__c refDataError1 = new ReferentialDataError__c();
            refDataError1.ErrorInFields__c = 'ErrorInFields__c1';
            refDataError1.ErrorMessage__c = 'ErrorMessage__c1';
            refDataError1.IDofToObject__c = CompReq2.id;
            refDataError1.ObjectName__c = 'ObjectName__c1';
            refDataError1.ReRun__c = true;
            refDataError1.StatusCode__c = 'StatusCode__c1';
            refEorrorList.add(refDataError1);
        insert refEorrorList;
            
       Batch_ProductGMTUpdate BtchPdct1 = new Batch_ProductGMTUpdate(true,'ComplaintRequest__c');
         Test.startTest();
        Database.executeBatch(BtchPdct1);
        Test.stopTest();
    }

}