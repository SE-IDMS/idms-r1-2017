/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_X3rd_Party_CostTest
{
    private static testmethod void testTrigger()
    {
        // Code to cover the one line trigger, the method called has been tested separately by the packaged tests.
        try { insert new X3rd_Party_Cost__c(); } catch(Exception e) { }
    }
}