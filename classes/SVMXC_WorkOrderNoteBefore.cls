public class SVMXC_WorkOrderNoteBefore {

	public static void setFSRUserField (List<WorkOrderNote__c> triggerNew, Map<Id, WorkOrderNote__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to set FSR User field for newly inserted records (Written May 20, 2015)
            
        */
        
	    for (WorkOrderNote__c won : triggerNew) {
            if (won.FSRUser__c == null)
                won.FSRUser__c = UserInfo.getUserId(); 
        }       
	}
}