/*
    Author          : Srinivas Nallapati
    Date Created    : 05/Mar/2012
    Description     : Test class for SCA01_ProblemWeeklyDigest
*/
@isTest
private class SCA01_ProblemWeeklyDigest_TEST {
    static testMethod void myUnitTest() {
    	Country__c country= Utils_TestMethods.createCountry();
        insert country;
        
        BusinessRiskEscalationEntity__c AccountableOrganization= Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
        insert AccountableOrganization;
        
        Problem__c prob = Utils_TestMethods.createProblem(AccountableOrganization.id, 1);
        insert prob;
        
        prob.Sensitivity__c = 'Public';
        update prob;
        
        SCA01_ProblemWeeklyDigest st = new SCA01_ProblemWeeklyDigest();
        st.SendProblemDigest();
        st.bTestFlag = true;
        st.SendProblemDigest();
        
    }
}