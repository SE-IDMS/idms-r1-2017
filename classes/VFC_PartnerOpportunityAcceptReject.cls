public class VFC_PartnerOpportunityAcceptReject {

    private Task partnerTask;
    private OpportunityTeamMember otm;
    private PartnerOpportunityStatusHistory__c poStatus;
    private OPP_ValueChainPlayers__c pvcp;
    private String type ;
    String urlStr ;
     
    public VFC_PartnerOpportunityAcceptReject(ApexPages.StandardController controller) {
        this.partnerTask = [SELECT Id, TECH_OpportunityId__c, OwnerId, RecordTypeId FROM Task WHERE 
                            id =:ApexPages.currentPage().getParameters().get('taskid') LIMIT 1];

        this.otm = [SELECT Id, UserId, OpportunityId, User.ContactId FROM OpportunityTeamMember WHERE 
                    (UserId = :this.partnerTask.OwnerId AND OpportunityId = :this.partnerTask.TECH_OpportunityId__c AND 
                    TeamMemberRole = :System.Label.CLOCT13PRM13) LIMIT 1];
        
        this.poStatus = [SELECT Id, Opportunity__c, User__c, Status__c FROM PartnerOpportunityStatusHistory__c WHERE
                            (Opportunity__c = :this.partnerTask.TECH_OpportunityId__c AND User__c = :this.partnerTask.OwnerId AND
                            Status__c = 'Assigned') LIMIT 1];

        this.pvcp = [SELECT Id, OpportunityName__c, Contact__c FROM OPP_ValueChainPlayers__c WHERE
                        (OpportunityName__c = :this.partnerTask.TECH_OpportunityId__c AND Contact__c = :this.otm.User.ContactId AND
                        ContactRole__c = :System.Label.CLSEP12PRM16) LIMIT 1];
                        
        system.debug('PartnerTask :' + this.partnerTask );
        system.debug('PartnerTask Id:' + this.partnerTask.Id);
        this.type = ApexPages.currentPage().getParameters().get('type') ;
        system.debug('typec:' + type);
        
        urlStr = '/';
        System.debug(Site.getPrefix());
        System.debug(Site.getCurrentSiteUrl());
        
        if(Site.getPrefix() != null && (Site.getPrefix() == '/partners' || Site.getPrefix() == '/pomp'))
                urlStr = Site.getCurrentSiteUrl();
            
         
           
    }

    public PageReference updateTask() {
        Pagereference pageref;
        system.debug('type:' + this.type);
        this.poStatus.Status__c = type + 'ed';
        this.partnerTask.recordtypeid = System.Label.CLOCT13PRM14;
        this.partnerTask.Status = System.Label.CL00327;
       // pageref = new pagereference(urlStr + (this.type == 'Accept' ? this.partnerTask.Tech_OpportunityID__c : this.partnerTask.Id));
        
        
        pageref = new pagereference(urlStr + (this.type == 'Accept' ? this.partnerTask.Tech_OpportunityID__c : this.partnerTask.Id));

        // TODO: If Accepted
        // 1. Update the junction object with Status as 'Accepted'
        // TODO: If Rejected
        // 1. Delete the record from OpportunityTeamMember for the PartnerUserId
        // 2. Update the Opportunity PartnerInvolvement to None
        // 3. Update the junction object with Status as 'Rejected'

        if (this.type == 'Reject') {
            Opportunity o = new Opportunity(Id = this.otm.OpportunityId, PartnerInvolvement__c = null);

            System.Debug ('Oppty Team Member: ' + this.otm);
            delete this.otm;
            
            System.Debug ('Partner Oppty VCP: ' + this.pvcp);
            delete pvcp;
            
            System.Debug ('Opportunity: ' + o);
            update o;
        }

        System.Debug ('Partner Oppty Task: ' + this.partnerTask);
        update this.partnerTask;
        
        System.Debug ('Partner Oppty History Status: ' + this.poStatus);
        update this.poStatus;
        return pageref;
    }
}