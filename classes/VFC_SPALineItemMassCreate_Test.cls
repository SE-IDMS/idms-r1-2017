/*
Author:Siddharth Nagavarapu(GD Solutions)
Purpose: Test class for the class VFC_SPALineItemMassCreate (mass create spa lineitems)
Date:01/05/2013
*/
@isTest
private class VFC_SPALineItemMassCreate_Test {
    
    @isTest static void test_method_one() 
    {
        // Implement test code
        //create SPA Request
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
                //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP',System.Label.CLOCT14SLS11);
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest(System.Label.CLOCT14SLS11,opp.id);
        insert sparequest;      
        List<CS_SPALineItemCSVMap__c> csspalineitemdata=new List<CS_SPALineItemCSVMap__c>();
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='Business__c',FieldType__c='',isRequired__c=False,Name='Business',MassCreationType__c='Commercial Reference'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='Description__c',FieldType__c='',isRequired__c=False,Name='Comments',MassCreationType__c='Commercial Reference'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='CommercialReference__c',FieldType__c='',isRequired__c=TRUE,Name='Commercial Reference',MassCreationType__c='Commercial Reference'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='RequestedType__c',FieldType__c='picklist',isRequired__c=TRUE,Name='Requested Type',MassCreationType__c='Commercial Reference'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='RequestedValue__c',FieldType__c='decimal',isRequired__c=TRUE,Name='Requested Value',MassCreationType__c='Commercial Reference'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='TargetQuantity__c',FieldType__c='decimal',isRequired__c=TRUE,Name='Target Quantity',MassCreationType__c='Commercial Reference'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='CurrencyIsoCode',FieldType__c='picklist',isRequired__c=False,Name='Currency Code',MassCreationType__c='Commercial Reference'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='TestBoolean',FieldType__c='boolean',isRequired__c=False,Name='TestBoolean',MassCreationType__c='Commercial Reference'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='TestDate',FieldType__c='date',isRequired__c=False,Name='TestDate',MassCreationType__c='Commercial Reference'));
        insert csspalineitemdata;
        Test.startTest(); 

        ApexPages.StandardSetController controller= new ApexPages.StandardSetController(new List<SPARequest__c>{sparequest});            
        VFC_SPALineItemMassCreate controllerinstance=new VFC_SPALineItemMassCreate(controller);            
        ApexPages.currentPage().getParameters().put('id',sparequest.id);     
        controllerinstance=new VFC_SPALineItemMassCreate(controller);    
        StringListWrapper sampleStringListWrapper=new StringListWrapper();               
        sampleStringListWrapper.stringList.add(new List<String>{'Business','Commercial Reference','Currency Code','Requested Type','Requested Value','Target Quantity'});
        sampleStringListWrapper.stringList.add(new List<String>{'PW','E2000 WE','INR',System.Label.CLOCT14SLS11,'12','123'});
        sampleStringListWrapper.stringList.add(new List<String>{'PW','E3034H1 FWWW','INR',System.Label.CLOCT14SLS11,'12','123'});
        controllerinstance.csvList=sampleStringListWrapper;
        controllerinstance.type=System.Label.CLOCT14SLS31;
        controllerinstance.getItems();
        controllerinstance.preapareLineItemList();        
        Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('PosMultipleProducts'));
        controllerinstance.create();            
        controllerinstance.proceedWithInsert(); 
        Test.stopTest();
    }
    @isTest static void test_method_two() 
    {
        // Implement test code
        //create SPA Request
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
                //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP',System.Label.CLOCT14SLS11);
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest(System.Label.CLOCT14SLS11,opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;        
        List<CS_SPALineItemCSVMap__c> csspalineitemdata=new List<CS_SPALineItemCSVMap__c>();
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='Business__c',FieldType__c='',isRequired__c=False,Name='Business',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='Description__c',FieldType__c='',isRequired__c=False,Name='Comments',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='CommercialReference__c',FieldType__c='',isRequired__c=TRUE,Name='Commercial Reference',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='RequestedType__c',FieldType__c='picklist',isRequired__c=TRUE,Name='Requested Type',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='RequestedValue__c',FieldType__c='decimal',isRequired__c=TRUE,Name='Requested Value',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='TargetQuantity__c',FieldType__c='decimal',isRequired__c=TRUE,Name='Target Quantity',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='CurrencyIsoCode',FieldType__c='picklist',isRequired__c=False,Name='Currency Code',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='TestBoolean',FieldType__c='boolean',isRequired__c=False,Name='TestBoolean',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='TestDate',FieldType__c='date',isRequired__c=False,Name='TestDate',MassCreationType__c='Local Pricing Group'));
        insert csspalineitemdata;
        Test.startTest(); 
        ApexPages.StandardSetController controller= new ApexPages.StandardSetController(new List<SPARequest__c>{sparequest});            
        VFC_SPALineItemMassCreate controllerinstance=new VFC_SPALineItemMassCreate(controller);            
        ApexPages.currentPage().getParameters().put('id',sparequest.id);     
        controllerinstance=new VFC_SPALineItemMassCreate(controller);    
        StringListWrapper sampleStringListWrapper=new StringListWrapper();               
        sampleStringListWrapper.stringList.add(new List<String>{'Business','Commercial Reference','Currency Code','Requested Type','Requested Value','Target Quantity'});
        sampleStringListWrapper.stringList.add(new List<String>{'PW','E2000 WE','INR','Discount','12','123'});
        sampleStringListWrapper.stringList.add(new List<String>{'PW','E3034H1 FWWW','INR','Discount','12','123'});
        controllerinstance.csvList=sampleStringListWrapper;
        try{
            controllerinstance.typeSelect();
        }
        catch(Exception e){
             Boolean expectedExceptionThrown = true;
             system.assertEquals(expectedExceptionThrown,true);      
        }
        controllerinstance.type=System.Label.CLOCT14SLS32;
        controllerinstance.preapareLineItemList();        
        Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('NegMultipleProducts'));
        controllerinstance.create();            
        controllerinstance.proceedWithInsert(); 
        
        Test.stopTest();
    }
     @isTest static void test_method_three() 
    {
        // Implement test code
        //create SPA Request
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
                //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP',System.Label.CLOCT14SLS11);
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest(System.Label.CLOCT14SLS11,opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;        
        List<CS_SPALineItemCSVMap__c> csspalineitemdata=new List<CS_SPALineItemCSVMap__c>();
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='Business__c',FieldType__c='',isRequired__c=False,Name='Business',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='Description__c',FieldType__c='',isRequired__c=False,Name='Comments',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='CommercialReference__c',FieldType__c='',isRequired__c=TRUE,Name='Commercial Reference',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='RequestedType__c',FieldType__c='picklist',isRequired__c=TRUE,Name='Requested Type',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='RequestedValue__c',FieldType__c='decimal',isRequired__c=TRUE,Name='Requested Value',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='TargetQuantity__c',FieldType__c='decimal',isRequired__c=TRUE,Name='Target Quantity',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='CurrencyIsoCode',FieldType__c='picklist',isRequired__c=False,Name='Currency Code',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='TestBoolean',FieldType__c='boolean',isRequired__c=False,Name='TestBoolean',MassCreationType__c='Local Pricing Group'));
        csspalineitemdata.add(new CS_SPALineItemCSVMap__c(ApiName__c='TestDate',FieldType__c='date',isRequired__c=False,Name='TestDate',MassCreationType__c='Local Pricing Group'));
        insert csspalineitemdata;
       try{
            sparequest.ApprovalStatus__c='Approved';
            update sparequest;
            ApexPages.StandardSetController controller= new ApexPages.StandardSetController(new List<SPARequest__c>{sparequest});            
            VFC_SPALineItemMassCreate controllerinstance=new VFC_SPALineItemMassCreate(controller);            
            ApexPages.currentPage().getParameters().put('id',sparequest.id);     
            controllerinstance=new VFC_SPALineItemMassCreate(controller);
        }
        catch(Exception e){
             Boolean expectedExceptionThrown = true;
             system.assertEquals(expectedExceptionThrown,true);      
        }

    }
    
}