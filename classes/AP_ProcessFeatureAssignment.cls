/*
17-Feb-2014     Srinivas Nallapati  PRM Apr 14  Initial Creation
*/
public class AP_ProcessFeatureAssignment {

    public static void processAccountAssignedFeatures(List<ACC_PartnerProgram__c> lstAccProgs)
    {
        set<id> accIds = new set<id>();
        for(ACC_PartnerProgram__c aap : lstAccProgs)
        {
            accIds.add(aap.Account__c);
        }

        List<ACC_PartnerProgram__c> allRelatedAAP = [SELECT id, ProgramLevel__c, Account__C FROM ACC_PartnerProgram__c WHERE Account__c in :accIds and Active__c=true];
        set<id> plIds = new set<id>();
        map<id,set<id>> mpAcctoPLids = new map<id,set<id>>();
        for(ACC_PartnerProgram__c aap : allRelatedAAP)
        {
            plIds.add(aap.ProgramLevel__c);

            if(mpAcctoPLids.containsKey(aap.Account__C))
            {
                mpAcctoPLids.get(aap.Account__C).add(aap.ProgramLevel__c);
            }
            else
            {
                set<id> tmpset = new set<id>(); tmpset.add(aap.ProgramLevel__c);
                mpAcctoPLids.put(aap.Account__C, tmpset);
            }
        }

        //List<ProgramLevel__c> allRelatedPLs = [Select id from ProgramLevel__c where id in :plIds];
        List<ProgramFeature__c> lstPFs = [SELECT FeatureCatalog__c, FeatureStatus__c, ProgramLevel__c, Visibility__c FROM ProgramFeature__c WHERE FeatureStatus__c='Active' AND ProgramLevel__c IN:plIds];

        map<id,set<id>> mpLvlToFC = new map<id,set<id>>();
        for(ProgramFeature__c pf : lstPFs)
        {
            if(mpLvlToFC.containsKey(pf.ProgramLevel__c))
            {
                mpLvlToFC.get(pf.ProgramLevel__c).add(pf.FeatureCatalog__c);
            }
            else
            {
                set<id> tmpst = new set<id>(); tmpst.add(pf.FeatureCatalog__c);
                mpLvlToFC.put(pf.ProgramLevel__c, tmpst);
            }
        }
        

        List<AccountAssignedFeature__c> lstAllRelatedAAF = [Select Account__c, Active__c, FeatureCatalog__c FROM  AccountAssignedFeature__c WHERE Account__c in:accIds];
        for(AccountAssignedFeature__c aaf : lstAllRelatedAAF)
        {
            set<id> assignedPlIds = mpAcctoPLids.get(aaf.Account__c);
            Boolean flg = false;
            if(assignedPlIds != null)
            for(id plId : assignedPlIds)
            {
                set<id> activeFCs = mpLvlToFC.get(plId);
                if(activeFCs!= null && activeFCs.contains(aaf.FeatureCatalog__c))
                    flg = true; //assigned level has active feature assigend to it
            }

            if(flg == false)//if no Level has acitve PF for this feature
                aaf.Active__c = false;
            else
                aaf.Active__c = true;       

        }

        Database.update(lstAllRelatedAAF, false);

    }//end of method


    public static void processContactAssignedFeatures(List<ContactAssignedProgram__c> lstConProgs)
    {
        set<id> conIds = new set<id>();
        for(ContactAssignedProgram__c cap : lstConProgs)
        {
            conIds.add(cap.Contact__c);
        }

        List<ContactAssignedProgram__c> allRelatedCAP = [SELECT id, ProgramLevel__c , Contact__c FROM ContactAssignedProgram__c WHERE Contact__c in :conIds and Active__c=true];
        set<id> plIds = new set<id>();
        map<id,set<id>> mpContoPLids = new map<id,set<id>>();
        for(ContactAssignedProgram__c cap : allRelatedCAP)
        {
            plIds.add(cap.ProgramLevel__c);

            if(mpContoPLids.containsKey(cap.Contact__c))
            {
                mpContoPLids.get(cap.Contact__c).add(cap.ProgramLevel__c);
            }
            else
            {
                set<id> tmpset = new set<id>(); tmpset.add(cap.ProgramLevel__c);
                mpContoPLids.put(cap.Contact__c, tmpset);
            }
        }

        //List<ProgramLevel__c> allRelatedPLs = [Select id from ProgramLevel__c where id in :plIds];
        List<ProgramFeature__c> lstPFs = [SELECT FeatureCatalog__c, FeatureStatus__c, ProgramLevel__c, Visibility__c FROM ProgramFeature__c WHERE FeatureStatus__c='Active' AND ProgramLevel__c IN:plIds];

        map<id,set<id>> mpLvlToFC = new map<id,set<id>>();
        for(ProgramFeature__c pf : lstPFs)
        {
            if(mpLvlToFC.containsKey(pf.ProgramLevel__c))
            {
                mpLvlToFC.get(pf.ProgramLevel__c).add(pf.FeatureCatalog__c);
            }
            else
            {
                set<id> tmpst = new set<id>(); tmpst.add(pf.FeatureCatalog__c);
                mpLvlToFC.put(pf.ProgramLevel__c, tmpst);
            }
        }


        List<ContactAssignedFeature__c> lstAllRelatedCAF = [Select Contact__c, Active__c, FeatureCatalog__c FROM  ContactAssignedFeature__c WHERE Contact__c in:conIds];
        for(ContactAssignedFeature__c caf : lstAllRelatedCAF)
        {
            set<id> assignedPlIds = mpContoPLids.get(caf.Contact__c);
            Boolean flg = false;
            if(assignedPlIds != null)
            for(id plId : assignedPlIds)
            {
                set<id> activeFCs = mpLvlToFC.get(plId);
                if(activeFCs != null && activeFCs.contains(caf.FeatureCatalog__c))
                    flg = true; //assigned level has active feature assigend to it
            }

            if(flg == false)//if no Level has acitve PF for this feature
                caf.Active__c = false;
            else
                caf.Active__c = true;

        }

        Database.update(lstAllRelatedCAF, false);
    }//end of method
}//End of class