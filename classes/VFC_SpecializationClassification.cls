public with sharing class VFC_SpecializationClassification {
    
    public list<SpecializationClassification__c> lstSpecClass {get;set;}
    
    public VFC_SpecializationClassification(ApexPages.StandardController controller) {
        lstSpecClass  = [Select ClassificationLevelCatalog__r.ClassificationLevelName__c from SpecializationClassification__c where Specialization__c=:controller.getId() and ClassificationLevelCatalog__r.ParentClassificationLevel__c= null order by ClassificationLevelCatalog__r.ClassificationLevelName__c limit 1000]; 
        
    }

}