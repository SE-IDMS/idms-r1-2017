/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 18/10/2010
    Description     : Test class for Controller CreateScoringRecords.  Verifies that Platform Scoring records are created 
                        for the user specified in the assignedTo__c field on an Individual Commercial Action Plan record 
                        - a Platform Scoring record is created for each Account the user owns or belongs to the Account Team.
*/
@isTest
private class VFC04_SFETargetting_TEST {

    static testMethod void testCreateScoringRecordsController() { 
        // setup test data
        List<User> users = new List<User>();
        User testUser1 = Utils_TestMethods.createStandardUser('tst1');
        users.add(testUser1);
        User testUser2 = Utils_TestMethods.createStandardUser('tst2');
        users.add(testUser2);
        insert users;
         
        List<Account> accounts = new List<Account>();        
        Account acc2 = Utils_TestMethods.createAccount(testUser1.Id);
        acc2.PrimaryRelationshipLeader__c=Label.CLDEC14SLS02; //Sales Rep
        accounts.add(acc2);
        Account acc3 = Utils_TestMethods.createAccount(testUser2.Id);
        acc3.PrimaryRelationshipLeader__c=Label.CLDEC14SLS03; //Inside Sales
        accounts.add(acc3);
        insert accounts;
        
        AccountTeamMember accMember = new AccountTeamMember(UserId = testUser1.Id, AccountId = acc3.Id);
        insert accMember;
        SFE_IndivCAP__c indivCAP = Utils_TestMethods.createIndividualActionPlan(testUser1.Id);
        //indivCAP.AssignedTo__c=UserInfo.getUserId();
        indivCAP.AssignedTo__c=testUser1.Id;
        insert indivCAP;
        
        List<Account> accList = new List<Account>();
        for(Integer i = 0; i < 5; i ++){  
            accList.add(Utils_TestMethods.createAccount(testUser1.Id));
        }
        insert accList;
        
        // initialise controller etc.
        PageReference pageRef = Page.VFP04_SFETargetting;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', indivCAP.Id);
        VFC04_SFETargetting c = new VFC04_SFETargetting();
        // pre-checks
        List<SFE_PlatformingScoring__c> platformRecords = [select id from SFE_PlatformingScoring__c where IndivCAP__c = :indivCAP.Id];
        System.assertEquals(0, platformRecords.size()); 
        List<Id> accountList=new List<Id>();  
        accountList.add(acc3.Id);
        // simulate page entry view
        Test.startTest();
        c.processhere=1;
        c.createPlatformScorings();
        c.createPlatformScoringsFromClone(accountList,indivCAP);
        Test.stopTest();
        
        // check results
        platformRecords = [select id, PlatformedAccount__c from SFE_PlatformingScoring__c where IndivCAP__c = :indivCAP.Id];
        //System.assertEquals(6, platformRecords.size());    // i.e. 5 accounts the user owners + 1 he is in the account team
        Boolean foundAcc3Score = false;
        for(SFE_PlatformingScoring__c pr : platformRecords){
            if (pr.PlatformedAccount__c == acc3.Id){
                foundAcc3Score = true;                      // user belongs to the account team of the associated account
            }
        }
        //System.assertEquals(true, foundAcc3Score);
        
        // re-run code - this is purely for coverage purposes as in the code a uniqiue field violation (UniqueKey__c) will be thrown with this second call.
        c.createPlatformScorings();
        
        // re-run code - for coverage reasons ensure that 'break' statement (Account related) are called due to max record insert limit being exceeded
        
        // first delete records
        delete platformRecords;
        // add additional Accounts so resultant Platform limit will exceed max record creation (in the class isRunningTest is used to set the limit to 10)
        accList.clear();
        for(Integer i = 0; i < 10; i ++){  
            accList.add(Utils_TestMethods.createAccount(testUser1.Id));
        }
        insert accList;
        c.createPlatformScorings();
        
        // now cover condition where the Owner of the Indiv CAP record is different to the user trying to create Platform Scoring records
        User runAsUser = Utils_TestMethods.createStandardUser('tst5');
        insert runAsUser;   
        System.runAs(runAsUser){
            pageRef = Page.VFP04_SFETargetting;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', indivCAP.Id);
            VFC04_SFETargetting c1 = new VFC04_SFETargetting();
            c1.createPlatformScorings();
             List<Account> tempacclist= new List<Account>();
            for(Integer i = 5; i < 10; i ++){  
                tempacclist.add(Utils_TestMethods.createAccount(testUser1.Id));
            }
            for(Account a:tempacclist)
                a.PrimaryRelationshipLeader__c=Label.CLDEC14SLS02;
            List<Database.SaveResult> srs=Database.insert(tempacclist,false);
            List<Id> accountIds=new List<Id>();
            for(Database.SaveResult sr:srs)
            {
                if(sr.isSuccess())
                accountIds.add(sr.getId());
            }
            c1.BatchSize=3;
            c1.callFutureMethods(accountIds,indivCAP.Id);
            c1.cancel();            
        }
    }
    static testmethod void test2()
    {
        Id userId=userInfo.getUserId();
        List<account> accList=new List<account>();
        List<Id> accIdList=new List<Id>();
        Account a = Utils_TestMethods.createAccount();
        a.ToBeDeleted__c=false;
        a.Inactive__c=false;
        a.PrimaryRelationshipLeader__c='Sales Rep';
        insert a;
        Account a2 = Utils_TestMethods.createAccount();
        a2.PrimaryRelationshipLeader__c='Inside Sales';
        insert a2;
        accIdList.add(a.id);
        List<User> users = new List<User>();
        User testUser1 = Utils_TestMethods.createStandardUser('tst1');
        users.add(testUser1);
        insert users;
        SFE_IndivCAP__c indivCAP = Utils_TestMethods.createIndividualActionPlan(userId);
        insert indivCAP;
        SFE_IndivCAP__c indivCAP2 = Utils_TestMethods.createIndividualActionPlan(testUser1.id);
        indivCAP2.Status__c='Released';
        indivCAP2.EndDate__c=System.today()-1;
        
        insert indivCAP2;
        
        AccountTeamMember accMember = new AccountTeamMember(UserId = testUser1.Id, AccountId = a.Id);
        insert accMember;
        SFE_PlatformingScoring__c pltfromscoring=new SFE_PlatformingScoring__c();
        AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
        insert amp;
        AccountMasterProfilePAM__c aPAM=new AccountMasterProfilePAM__c(GMRBusinessUnit__c='ECOBUILDINGS',AccountMasterProfile__c=amp.id,PAM__c=10,DirectSales__c=2,IndirectSales__c=4);
        insert aPAM;
        PageReference pageRef =new pagereference('/apex/VFP04_SFETargetting?id='+indivCAP2.id);//changed id from indivcap.id to indivCAP2
        Test.setCurrentPage(pageRef);
        VFC04_SFETargetting con = new VFC04_SFETargetting();
        con.processhere=200;
        
        con.createPlatformScorings();
        con.createPlatformScoringsFromClone(accIdList,indivCAP);
        con.cancel();
        List<SFE_PlatformingScoring__c> pltfromscoringList=new List<SFE_PlatformingScoring__c>();
        Datetime LastYearsDate= Datetime.now().addDays(-365);
        pltfromscoring=Utils_TestMethods.createPlatformScoring(indivCAP2.id,a2.id,10,'2');
        insert pltfromscoring;
        
        //pltfromscoring=Utils_TestMethods.createPlatformScoring(indivCAP2.id,a.id,10,'2');//new
        //insert pltfromscoring;
        
        pltfromscoringList.add(pltfromscoring);
        System.debug('pltfromscoring with last Year created date:'+pltfromscoring);
        con.BatchSize=300;
        con.createPlatformScorings();
        con.callFutureMethods(pltfromscoringList);
        con.callFutureMethods(accIdList,indivCAP2.Id);
        con.cancel();
        
    }
    static testmethod void test3()
    {
        Id userId=userInfo.getUserId();
        List<account> accList=new List<account>();
        List<Id> accIdList=new List<Id>();
        Account a = Utils_TestMethods.createAccount();
        a.ToBeDeleted__c=false;
        a.Inactive__c=false;
        a.PrimaryRelationshipLeader__c='Sales Rep';
        
        insert a;
        Account a3 = Utils_TestMethods.createAccount();
        a3.PrimaryRelationshipLeader__c='Sales Rep';
        insert a3;
        system.debug('Account a1 owner admin:'+a.Owner.name);
        User testUser1 = Utils_TestMethods.createStandardUser('tst1');
        
        insert testUser1;
        Account a2;
        System.RunAs(testUser1) {
        a2 = Utils_TestMethods.createAccount();
        a2.PrimaryRelationshipLeader__c='Inside Sales';
        insert a2;    // owner of account a2 is test user 1
        system.debug('current user is:'+userInfo.getUsername());
        system.debug('Account a2 owner testuser1:'+a.Owner.name);
        }
        system.debug('current user is:'+userInfo.getUsername());
        accIdList.add(a.id);
        accIdList.add(a2.id);
        accIdList.add(a3.id);
        List<User> users = new List<User>();
        
        SFE_IndivCAP__c indivCAP = Utils_TestMethods.createIndividualActionPlan(userId);
        indivCAP.StartDate__c = date.newInstance(2016, 08, 31);
        indivCAP.EndDate__c = date.newInstance(2016, 09, 15);
        insert indivCAP;
        SFE_IndivCAP__c lastyearcap = Utils_TestMethods.createIndividualActionPlan(userId);
        lastyearcap.StartDate__c = date.newInstance(2016, 07, 28);
        lastyearcap.EndDate__c = date.newInstance(2016, 08, 29);
        lastyearcap.Status__c='Released';
        insert lastyearcap;
        
        AccountTeamMember accMember = new AccountTeamMember(UserId = userId, AccountId = a2.Id); 
        insert accMember;
        SFE_PlatformingScoring__c pltfromscoring=new SFE_PlatformingScoring__c();
        SFE_PlatformingScoring__c pltfromscoring2=new SFE_PlatformingScoring__c();
        AccountMasterProfile__c amp2=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='2',Q5Rating__c='2',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
        insert amp2;
    AccountMasterProfilePAM__c aPAM=new AccountMasterProfilePAM__c(GMRBusinessUnit__c='ECOBUILDINGS',AccountMasterProfile__c=amp2.id,PAM__c=10,DirectSales__c=2,IndirectSales__c=4);
        insert aPAM;
      
        PageReference pageRef =new pagereference('/apex/VFP04_SFETargetting?id='+indivCAP.id);
        Test.setCurrentPage(pageRef);
        VFC04_SFETargetting con = new VFC04_SFETargetting();
        //con.processhere=200;
        con.BatchSize=1;
        pltfromscoring=Utils_TestMethods.createPlatformScoring(indivCAP.id,a.id,10,'2');
        insert pltfromscoring;
        pltfromscoring2=Utils_TestMethods.createPlatformScoring(lastyearcap.id,a.id,10,'2');
        insert pltfromscoring2;
        con.createPlatformScorings();
        con.processhere=-1;
        con.createPlatformScoringsFromClone(accIdList,indivCAP);
        con.cancel();
        List<SFE_PlatformingScoring__c> pltfromscoringList=new List<SFE_PlatformingScoring__c>();
        pltfromscoringList.add(pltfromscoring);
        //con.BatchSize=0;
        con.createPlatformScorings();
        
        con.createPlatformScorings();
        
        con.callFutureMethods(pltfromscoringList);
        
        con.callFutureMethods(accIdList,lastyearcap.Id);
        con.cancel();
        
    }
    
    static testmethod void test4()
    {
        Id userId=userInfo.getUserId();
        User salesAccUser = Utils_TestMethods.createStandardUser('tst1');
        insert salesAccUser;
        Account a1;
        System.RunAs(salesAccUser) {
        a1 = Utils_TestMethods.createAccount(); 
        a1.PrimaryRelationshipLeader__c='Inside Sales';
        insert a1; 
        AccountTeamMember atm=Utils_TestMethods.createAccTeamMember(a1.id,userId);
        insert atm;
        }
        
        system.debug('current user is:'+userInfo.getUsername());    
        Account a2 = Utils_TestMethods.createAccount();  //owner is sysAdmin 
        a2.ToBeDeleted__c=false;
        a2.ExternalSalesResponsible__c=null;
        a2.Inactive__c=false;
        a2.PrimaryRelationshipLeader__c='Sales Rep';
        insert a2;
        system.debug('a2:'+a2);
        
        SFE_IndivCAP__c currentCAP = Utils_TestMethods.createIndividualActionPlan(userId); 
        currentCAP.StartDate__c = date.newInstance(2016, 08, 31);
        currentCAP.EndDate__c = date.newInstance(2016, 09, 15);
        insert currentCAP;
        SFE_IndivCAP__c lastyearcap = Utils_TestMethods.createIndividualActionPlan(userId);
        lastyearcap.StartDate__c = date.newInstance(2016, 07, 28);
        lastyearcap.EndDate__c = date.newInstance(2016, 08, 29);
        lastyearcap.Status__c='Released';
        insert lastyearcap;
       
        SFE_PlatformingScoring__c pltfromscoringforAcc1Lastyear=new SFE_PlatformingScoring__c();
        SFE_PlatformingScoring__c pltfromscoring2=new SFE_PlatformingScoring__c();
        pltfromscoringforAcc1Lastyear=Utils_TestMethods.createPlatformScoring(lastyearcap.id,a1.id,10,'2');
        insert pltfromscoringforAcc1Lastyear;
        
        PageReference pageRef =new pagereference('/apex/VFP04_SFETargetting?id='+currentCAP.id);
        Test.setCurrentPage(pageRef);
        VFC04_SFETargetting con = new VFC04_SFETargetting();
        con.createPlatformScorings();
     }
   static testmethod void test5()
    {
        Id userId=userInfo.getUserId();
        User usr=[select id,UserBusinessUnit__c from user where id=:userId];
        User salesAccUser = Utils_TestMethods.createStandardUser('tst1');
        salesAccUser.UserBusinessUnit__c=usr.UserBusinessUnit__c;
        insert salesAccUser;
        Account a1;
        SFE_IndivCAP__c lastyearcap;
        System.RunAs(salesAccUser) {
        a1 = Utils_TestMethods.createAccount(); 
        a1.PrimaryRelationshipLeader__c='Inside Sales';
        insert a1; 
        AccountTeamMember atm=Utils_TestMethods.createAccTeamMember(a1.id,userId);
        insert atm;
        
        lastyearcap = Utils_TestMethods.createIndividualActionPlan(salesAccUser.id);
        lastyearcap.StartDate__c = date.newInstance(2016, 07, 28);
        lastyearcap.EndDate__c = date.newInstance(2016, 08, 29);
        lastyearcap.Status__c='Released';
        insert lastyearcap;
        }
        
        system.debug('current user is:'+userInfo.getUsername());    
        Account a2 = Utils_TestMethods.createAccount();  
        a2.ToBeDeleted__c=false;
        a2.ExternalSalesResponsible__c=null;
        a2.Inactive__c=false;
        a2.PrimaryRelationshipLeader__c='Sales Rep';
        insert a2;
        system.debug('a2:'+a2);
        
        SFE_IndivCAP__c currentCAP = Utils_TestMethods.createIndividualActionPlan(userId); 
        currentCAP.StartDate__c = date.newInstance(2016, 08, 31);
        currentCAP.EndDate__c = date.newInstance(2016, 09, 15);
        insert currentCAP;
        
        SFE_PlatformingScoring__c pltfromscoringforAcc1Lastyear=new SFE_PlatformingScoring__c();
        //SFE_PlatformingScoring__c pltfromscoring2=new SFE_PlatformingScoring__c();
        pltfromscoringforAcc1Lastyear=Utils_TestMethods.createPlatformScoring(lastyearcap.id,a1.id,10,'2');
        insert pltfromscoringforAcc1Lastyear;
        
        PageReference pageRef =new pagereference('/apex/VFP04_SFETargetting?id='+currentCAP.id);
        Test.setCurrentPage(pageRef);
        VFC04_SFETargetting con = new VFC04_SFETargetting();
        con.createPlatformScorings();
     }
}