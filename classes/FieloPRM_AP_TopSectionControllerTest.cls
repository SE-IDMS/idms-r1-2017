/**************************************************************************************
    Author: Fielo Team
    Date: 13/04/2016
    Description: 
    Related Components: FieloPRM_AP_TopSectionController
***************************************************************************************/
@isTest
public with sharing class FieloPRM_AP_TopSectionControllerTest{
  
    public static testMethod void testUnit1(){
        
        Id IdRTComponentTopSection = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'FieloPRM_C_TopSection'].Id;
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c();
        FieloEE__Component__c comp;
        User u = new User();
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test', 
                FieloEE__LastName__c= 'test'
            );
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc= new Account(
                Name = 'test',
                Street__c = 'Some Street',
                ZipCode__c = '012345'
            );
            Insert acc;
            
            menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Test Menu'
            );
            insert menu;
            
            comp = new FieloEE__Component__c(
                Name = 'Top Section',
                FieloEE__Menu__c = menu.Id, 
                RecordTypeId = IdRTComponentTopSection,
                FieloEE__Layout__c = 'Slider'
            );
            Insert comp;
            
            FieloEE__RedemptionRule__c seg = FieloPRM_UTILS_MockUpFactory.createSegmentManual();

            FieloEE__RedemptionRuleCriteria__c rrc = new FieloEE__RedemptionRuleCriteria__c(
                FieloEE__FieldName__c = 'Name',
                FieloEE__RedemptionRule__c = seg.Id
            );
            insert rrc;
            
            FieloEE__News__c news = new FieloEE__News__c(
                FieloEE__IsActive__c = true,
                FieloEE__HasSegments__c = true,
                F_PRM_Style__c = 'Background Image',
                F_PRM_PartnerNameWelcomeTitle__c = true,
                FieloEE__AttachmentId__c = 'XXXXXXXXXXXXXXX',
                F_PRM_BackgroundColor__c = 'White',
                F_PRM_ContentFilter__c = 'P-WW',
                FieloEE__Title__c = 'Test',
                FieloEE__ExtractText__c = 'Test',
                F_PRM_LinkLabel__c = 'Test',
                F_PRM_GoToLink__c = 'www.fielo.com'
            );
            Insert news;

            FieloEE__SegmentDomain__c segDom = new FieloEE__SegmentDomain__c(
                FieloEE__Segment__c = seg.id, 
                FieloEE__News__c =  news.Id
            );
            Insert segDom;
            
            FieloEE__MemberSegment__c memSeg = new FieloEE__MemberSegment__c(
                FieloEE__Member2__c = mem.Id, 
                FieloEE__Segment2__c = seg.Id
            );
            Insert memSeg;
            
            FieloEE__Member__c membertest = [SELECT Id, FieloEE__User__c FROM FieloEE__Member__c WHERE id =: mem.id ];
            u = [SELECT Id, UserName FROM User WHERE Id =: membertest.FieloEE__User__c ];
        }
        
        system.runAs(u){
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            FieloPRM_AP_TopSectionController controller = new FieloPRM_AP_TopSectionController();
            controller.componentObj = comp;
        }

    }
}