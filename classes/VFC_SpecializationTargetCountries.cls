public with sharing class VFC_SpecializationTargetCountries {
    
    public List<SpecializationCountry__c> lstTargetedCountries {get;set;} {lstTargetedCountries = new List<SpecializationCountry__c>();}
    
    public VFC_SpecializationTargetCountries(ApexPages.StandardController cont) {
        lstTargetedCountries = [SELECT Country__c, Specialization__c, Country__r.name, Specialization__r.name FROM SpecializationCountry__c WHERE Specialization__c=:cont.getId() ORDER BY country__r.name ASC];
    }

}