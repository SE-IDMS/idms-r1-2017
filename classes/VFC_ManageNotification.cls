public class VFC_ManageNotification {
    public SetupAlerts__c sysAlert {get;set;}
    public SelectOption[] selClassificationLevel { get; set; }
    public SelectOption[] allClassificationLevel{ get; set; }
    public SelectOption[] selSegments { get; set; }
    public SelectOption[] allSegments{ get; set; }
    public String prmCountry ;
    public Map<id,id> mapSegAlert = new Map<id,id>();
    public Map<id,id> mapChanelAlert = new Map<id,id>();
    public String saveParam {get;set;}
    public SetupAlerts__c originalAlert;
    
    //public SelectOption[] publishedChannels { get; set; }
    //public SelectOption[] publishedSegments { get; set; }
   // public List<ChannelSystemAlert__c> lstChanelAlerts {get;set;}
    
    public SelectOption[] ListOfNotifications { get; set;}

    public VFC_ManageNotification(ApexPages.StandardController controller) {
        selClassificationLevel = new List<SelectOption>();  
        allClassificationLevel = new List<SelectOption>();
        sysAlert = new SetupAlerts__c();
        selSegments = new List<SelectOption>();  
        allSegments = new List<SelectOption>();
        ListOfNotifications = new List<SelectOption>();
        List<RecordType> lstRecType = [Select id,DeveloperName from RecordType where DeveloperName = 'Notification' OR DeveloperName = 'UserNotification'];
        if(lstRecType.size() > 0){
            for(RecordType recType:lstRecType){
                if(recType.DeveloperName == 'Notification'){
                    sysAlert.RecordTypeId = recType.Id;
                    ListOfNotifications.add(new SelectOption(recType.Id,'Broadcast Notification'));
                }
                else if(recType.DeveloperName == 'UserNotification')
                    ListOfNotifications.add(new SelectOption(recType.Id,'Custom Notification'));
            }
        }
        
        Map<id,id> mapExistClassificationLevel2 = new Map<id,id>();
        this.sysAlert =(SetupAlerts__c )controller.getRecord();
        if(sysAlert.Id!=null){
            System.debug('***** System Alert->'+sysAlert) ;
            sysAlert = [Select Contact__c, Contact__r.FirstName, Contact__r.Lastname, RecordType.DeveloperName, Name,FromDate__c,ToDate__c,PRMCountryClusterSetup__c,Content__c,Status__c,RecordType.Name from SetupAlerts__c where id=:sysAlert.Id];
            if(sysAlert.PRMCountryClusterSetup__c != null)
                prmCountry = sysAlert.PRMCountryClusterSetup__c;
        }
        
        originalAlert = sysAlert;
        
        if(ApexPages.currentPage().getParameters().containsKey('prmcountryid')){
            prmCountry = ApexPages.currentPage().getParameters().get('prmcountryid');
            sysAlert.PRMCountryClusterSetup__c = prmCountry;
        }   
        //-------------------------
          List<CountryChannels__c> lstAllChannels = [SELECT id,ChannelCode__c, Channel_Name__c, Sub_Channel__c,SubChannelCode__c, 
                                                        (SELECT Id,CountryChannels__c,CountryChannels__r.Sub_Channel__c FROM Channel_System_Alerts__r 
                                                          WHERE SystemAlert__c = :sysAlert.id)
                                                    FROM CountryChannels__c 
                                                    WHERE PRMCountry__r.CountryPortalEnabled__c = true AND PRMCountry__c = :prmCountry AND  Active__c = TRUE
                                                    ORDER BY Channel_Name__c LIMIT 500];
        for(CountryChannels__c aChnl :lstAllChannels){          
            for(ChannelSystemAlert__c aChlAlert :aChnl.Channel_System_Alerts__r){
                mapChanelAlert.put(aChlAlert.CountryChannels__c,aChlAlert.id);
                selClassificationLevel.add(new SelectOption(aChlAlert.CountryChannels__c,aChlAlert.CountryChannels__r.Sub_Channel__c));         
            }
            if(!mapChanelAlert.containsKey(aChnl.id))
              allClassificationLevel.add(new SelectOption(aChnl.id, aChnl.Sub_Channel__c ));            
        }
        
        
        //-------------------------             
        //Segments
        //-------------------------
        list<FieloEE__RedemptionRule__c> lstSegments = [SELECT id, Name,
                                                            (SELECT id,FieloPRM_Segment__c,FieloPRM_Segment__r.Name 
                                                             FROM Segment_System_Alerts__r where SystemAlert__c = :sysAlert.id)
                                                        FROM FieloEE__RedemptionRule__c 
                                                        WHERE F_PRM_Country__c = :prmCountry AND Recordtypeid != '012A0000000oDfD'
                                                        AND FieloEE__isActive__c = true
                                                        ORDER By Name];
                                                            
        for(FieloEE__RedemptionRule__c aSegment :lstSegments){
            
            for(SegmentSystemAlert__c aSegAlert :aSegment.Segment_System_Alerts__r){
                mapSegAlert.put(aSegAlert.FieloPRM_Segment__c,aSegAlert.id); 
                selSegments.add(new SelectOption(aSegAlert.FieloPRM_Segment__c,aSegAlert.FieloPRM_Segment__r.Name)); 
            }
            if(!mapSegAlert.containsKey(aSegment.id)) 
                allSegments.add(new SelectOption(aSegment.id,aSegment.Name));
            
        }
        
    }
    
    public pagereference doSave(){
        //validation
        try{
            
            String returnId = sysAlert.PRMCountryClusterSetup__c;
            System.debug('******* Save Param->'+saveParam);
            //System.debug('****publishedChannels '+publishedChannels );
            boolean errFlg = false;
            
           System.debug('**** sel Segment->'+selSegments.size());
            System.debug('**** sel Segment->'+selClassificationLevel.size());
            if(String.isBlank(sysAlert.Name)){
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,system.label.CLMAR16PRM14));
                errFlg = true;
            }
            if(saveParam == 'Published'){
                sysAlert.PublishedDate__c = System.now();   
                /*if(sysAlert.FromDate__c == null){
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,system.label.CLMAR16PRM15));
                    errFlg = true;
                }   
                if(sysAlert.ToDate__c == null){
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,system.label.CLMAR16PRM16));
                    errFlg = true;
                }  */
                 
               if(selSegments.size() == 0 && selClassificationLevel.size() == 0 && sysAlert.RecordtypeId != System.Label.CLMAR16PRM21){
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,system.label.CLMAR16PRM17));
                    errFlg = true;
                }
                if(String.isBlank(sysAlert.Content__c)){
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,system.label.CLMAR16PRM022));
                errFlg = true;
                }
                if(String.isBlank(sysAlert.Contact__c) && sysAlert.RecordtypeId == System.Label.CLMAR16PRM21){
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,system.label.CLMAR16PRM23));
                errFlg = true;
                }
            }
            
                        
            
            if(errFlg)
                return null;
            
            sysAlert.Status__c = saveParam;
            sysAlert.NotificationType__c = sysAlert.RecordtypeId == System.Label.CLMAR16PRM21 ? 'CUSTOM' : null;
           // sysAlert.PRMCountryClusterSetup__c = sysAlert.RecordtypeId != System.Label.CLMAR16PRM21 ? sysAlert.PRMCountryClusterSetup__c : null;
            System.debug('**** before update->'+sysAlert);
            upsert sysAlert;
             
            System.debug('**** after update->');    
            list<SegmentSystemAlert__c> lstSelectedSegmentAlerts = new list<SegmentSystemAlert__c>();
            list<ChannelSystemAlert__c> lstSelectedChanelAlerts = new list<ChannelSystemAlert__c>();
            
            Map<id,id> mapSelSegments = new Map<id,id>();
            for(SelectOption selSeg :selSegments){
                mapSelSegments.put(selSeg.getValue(),selSeg.getValue());
                if(!mapSegAlert.containsKey(selSeg.getValue())) {
                    SegmentSystemAlert__c aSegAlert = new SegmentSystemAlert__c();
                    aSegAlert.FieloPRM_Segment__c = selSeg.getValue();
                    aSegAlert.SystemAlert__c = sysAlert.id;
                    lstSelectedSegmentAlerts.add(aSegAlert);
                }   
            }
            
            List<SegmentSystemAlert__c> lstDelSegmentAlert = new List<SegmentSystemAlert__c>();
            //Find segments that are deSelected
            for(id segAlert :mapSegAlert.keyset()){
                if(!mapSelSegments.containsKey(segAlert)){
                    SegmentSystemAlert__c delSegAlert = new SegmentSystemAlert__c();
                    delSegAlert.id = mapSegAlert.get(segAlert);
                    lstDelSegmentAlert.add(delSegAlert);    
                    System.debug('*** deletion ->'+delSegAlert.id);
                }
            }
            //Delete those segments that are deSelected.
            if(lstDelSegmentAlert.size() > 0)
                delete lstDelSegmentAlert;          
            
            if(lstSelectedSegmentAlerts.size() > 0)
                insert lstSelectedSegmentAlerts;
            
            System.debug('** System Alert->'+sysAlert.id);
            System.debug('****selClassificationLevel->'+selClassificationLevel);
            //Channels            
            Map<id,id> mapSelChanels = new Map<id,id>();
            for(SelectOption selChanel :selClassificationLevel){
                mapSelChanels.put(selChanel.getValue(),selChanel.getValue());
                if(!mapChanelAlert.containsKey(selChanel.getValue())) {
                    ChannelSystemAlert__c aChanelAlert = new ChannelSystemAlert__c();
                    aChanelAlert.CountryChannels__c = selChanel.getValue();
                    aChanelAlert.SystemAlert__c = sysAlert.id;
                    lstSelectedChanelAlerts.add(aChanelAlert);
                    System.debug('**** Inside');
                }   
            }
            
            System.debug('*** Selected Channels->'+lstSelectedChanelAlerts);
            //Find Channels that are deSelected
            List<ChannelSystemAlert__c> lstDelChnlAlert = new List<ChannelSystemAlert__c>();
            for(id chnlAlert :mapChanelAlert.keyset()){
                if(!mapSelChanels.containsKey(chnlAlert)){
                    ChannelSystemAlert__c delChlAlert = new ChannelSystemAlert__c();
                    delChlAlert.id = mapChanelAlert.get(chnlAlert);
                    lstDelChnlAlert.add(delChlAlert);    
                    System.debug('*** deletion ->'+delChlAlert.id);
                }
            }
            
            //Delete Channels that are deSelected
            if(lstDelChnlAlert.size() > 0)
                Delete lstDelChnlAlert;
            
            
            //insert Channels
            if(lstSelectedChanelAlerts.size() >0)
                insert lstSelectedChanelAlerts;
                
                
            if(saveParam == 'Published' && sysAlert.RecordtypeId != System.Label.CLMAR16PRM21){
                Id batchProcessId;  
                Batch_ManageNotifications pn = new  Batch_ManageNotifications(); 
                pn.query = 'SELECT Id, Name, Content__c, PRMCountryClusterSetup__r.Country__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry1__r.CountryCode__c,'+
                            'PRMCountryClusterSetup__r.MemberCountry2__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry3__c,PRMCountryClusterSetup__r.MemberCountry4__c,'+
                            'PRMCountryClusterSetup__r.MemberCountry5__c, Status__c, ToDate__c,SegmentAlertsCount__c,ChannelAlertCount__c,'+
                            'PRMCountryClusterSetup__r.MemberCountry3__r.CountryCode__c,'+
                            'PRMCountryClusterSetup__r.MemberCountry4__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry5__r.CountryCode__c,'+
                            '(SELECT Id, Name, CountryChannels__c, CountryChannels__r.Active__c, CountryChannels__r.ChannelCode__c,'+ 
                                    'CountryChannels__r.PRMCountry__c,CountryChannels__r.PRMCountry__r.CountryCode__c, CountryChannels__r.SubChannelCode__c,'+
                                    'CountryChannels__r.Country__r.CountryCode__c, SystemAlert__c FROM Channel_System_Alerts__r),'+
                            '(SELECT Id, Name, FieloPRM_Segment__c, FieloPRM_Segment__r.F_PRM_Country__r.CountryCode__c, SystemAlert__c FROM Segment_System_Alerts__r)'+
                ' FROM SetupAlerts__c WHERE id=\''+sysAlert.id+'\'';
                
                batchProcessId = Database.executeBatch(pn); 
                System.debug('Returned batch process ID:@@@@@@@@@@@@@@@@@@ ' + batchProcessId); 
            }                          
                
                
            
            //return new pagereference('/'+sysAlert.PRMCountryClusterSetup__c);
            return new pagereference('/'+returnId);
        }
        catch(System.DmlException  e)
        {
            System.debug('****'+sysAlert);
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(0),''));
            System.debug(e.getDmlMessage(0));
            return null;
        }       
        
    }
    
    public pagereference doReset(){
        PageReference pageRef;
        if(sysAlert.id != null) {
            pageRef = new PageReference('/apex/vfp_ManageNotification?id='+sysAlert.id);
            pageRef.setRedirect(true);
        }
        else {
            pageRef = new PageReference('/apex/vfp_ManageNotification');
            pageRef.getParameters().put('prmcountryid',prmCountry);
            pageRef.setRedirect(true);   
        }
        return pageRef;
    }
    
    public pagereference doRevokeAlert(){
        system.debug('*** Revoke Alert');
        
        if(sysAlert.Status__c != 'Published'){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,system.label.CLMAR16PRM19));
            return null;
        }
        try{
        sysAlert.Status__c = 'Revoked';
        sysAlert.PublishedDate__c = null;
        Update sysAlert;
        
        //Get all alerts belongs to the System Alert
        /*List<TrackSetupAlets__c> lstTrackSetupAlerts = [Select id from TrackSetupAlets__c where SetupAlerts__c = :sysAlert.id ];
        if(lstTrackSetupAlerts.size() > 0)
            Delete lstTrackSetupAlerts; */
        
            Id batchProcessId;  
            Batch_RevokeNotifications rn = new Batch_RevokeNotifications(); 
            rn.query = 'SELECT Id, Name FROM SetupAlerts__c WHERE id=\''+sysAlert.id+'\'';
            batchProcessId = Database.executeBatch(rn); 
            System.debug('Returned batch process ID:@@@@@@@@@@@@@@@@@@ ' + batchProcessId);  
        }
        catch (Exception e) {
            System.debug('**Exception **'  + e.getMessage() + e.getStackTraceString()); 
        }
                   
        return new pagereference('/'+sysAlert.PRMCountryClusterSetup__c);
    }
    
     public pagereference doCancel(){
         
        String redirectPage ;
        if(sysAlert.id == null) 
            redirectPage = '/'+prmCountry;
        else
            redirectPage = '/'+sysAlert.PRMCountryClusterSetup__c;
        
        PageReference pageRef = new PageReference(redirectPage);
        pageRef.setRedirect(true);
        return pageRef;

    }
    
    public pagereference redirectEditPage(){
         
        String redirectPage ;
        System.debug('**** Redirect page->'+sysAlert);
        if(sysAlert.RecordType.DeveloperName != 'Notification' && sysAlert.id != null && sysAlert.RecordType.DeveloperName != 'UserNotification'){ 
            redirectPage = '/'+sysAlert.id+'/e?nooverride=1&retURL=%2F'+sysAlert.id;
            PageReference pageRef = new PageReference(redirectPage);
            return pageRef;
        }
        else return null;
    }

}