@isTest
    private class AP_bFO_COLA_Offer_TEST{
    static testMethod void unitTest1(){
    //for Offer launch product record types
 
      
       user newUser1 = [select id,name, email,LastName,UserName from user where id=:UserInfo.getUserID()];
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User newUser2 = new User(Alias = 'tasdf', Email='staag837dfvn3jdc@testorg.com',EmailEncodingKey='UTF-8', LastName='Testingefasjhdfg', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='stasdfasdf@testorg.com');
        insert newUser2;
        List<CS_COLAbFO_ProjectPhaseTask__c>   csListPhaseTask =new  List<CS_COLAbFO_ProjectPhaseTask__c>();
        CS_COLAbFO_ProjectPhaseTask__c CS3 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='Country',Name ='test1',Phase_Name__c ='Major Phase1',LaunchClassification__c='Major',RecordType__c ='Offer Introduction Project',Task_Name__c ='Major Phase 1 Task1 abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz');                          
                 csListPhaseTask.add(CS3);      
        CS_COLAbFO_ProjectPhaseTask__c CS4 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='Country',Name ='Test2',Phase_Name__c ='Major Phase 2',LaunchClassification__c='Major',RecordType__c ='Offer Introduction Project',Task_Name__c ='Major Phase 2 Task1');
             csListPhaseTask.add(CS4);
        CS_COLAbFO_ProjectPhaseTask__c CS5 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='Country',Name ='Test3',Phase_Name__c ='Strategic Phase 1',LaunchClassification__c='Strategic',RecordType__c ='Offer Introduction Project',Task_Name__c ='Strategic Phase 1 Task1');
             csListPhaseTask.add(CS5);
        CS_COLAbFO_ProjectPhaseTask__c CS6 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='Country',Name ='test4',Phase_Name__c ='Strategic Phase 2',LaunchClassification__c='Strategic',RecordType__c ='Offer Introduction Project',Task_Name__c ='Strategic Phase 2 Task1');                          
                 csListPhaseTask.add(CS6);   
                   CS_COLAbFO_ProjectPhaseTask__c CS7 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='BU',Name ='test5',Phase_Name__c ='Major Phase1',LaunchClassification__c='Major',RecordType__c ='BU Master Launch Project',Task_Name__c ='Major Phase 1 Task1 abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz');                          
                 csListPhaseTask.add(CS7);      
        CS_COLAbFO_ProjectPhaseTask__c CS8 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='BU',Name ='Test6',Phase_Name__c ='Major Phase 2',LaunchClassification__c='Major',RecordType__c ='BU Master Launch Project',Task_Name__c ='Major Phase 2 Task1');
             csListPhaseTask.add(CS8);
        CS_COLAbFO_ProjectPhaseTask__c CS2 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='BU',Name ='Test7',Phase_Name__c ='Strategic Phase 1',LaunchClassification__c='Strategic',RecordType__c ='BU Master Launch Project',Task_Name__c ='Strategic Phase 1 Task1');
             csListPhaseTask.add(CS2);
        CS_COLAbFO_ProjectPhaseTask__c CS1 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='BU',Name ='test8',Phase_Name__c ='Strategic Phase 2',LaunchClassification__c='Strategic',RecordType__c ='BU Master Launch Project',Task_Name__c ='Strategic Phase 2 Task1');                          
                 csListPhaseTask.add(CS1);  
        Insert csListPhaseTask;
             
         Id rt = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Launch - Product').getRecordTypeId();
         Offer_Lifecycle__c inOffLif= new Offer_Lifecycle__c(); 
                inOffLif.RecordTypeId=rt;
                inOffLif.Offer_Name__c='Test offer Schneider';
                inOffLif.Leading_Business_BU__c ='IT';
                inOffLif.Offer_Classification__c ='Major';
                inOffLif.Forecast_Unit__c='Kilo Euro';
                inOffLif.Launch_Master_Assets_Status__c='Red';
                inOffLif.Stage__c ='0 - Define';
                inOffLif.Launch_Owner__c = newUser1.id;
                inOffLif.BD_Status__c='Draft';
                inOffLif.Forecast_Stage__c='Do Contract';
                inOffLif.Sales_Date__c=Date.today();
                inOffLif.Description__c = 'Test DEsc';
        Test.startTest();
            Insert inOffLif; 
        Test.stopTest();
            list<Country__c> ctrylist = new list<Country__c>();
            Country__c cotryObj2= new Country__c(Name ='China', CountryCode__c='1CH');
            ctrylist.add(cotryObj2);
            Country__c cotryObj3= new Country__c(Name ='India', CountryCode__c='1IN');
            ctrylist.add(cotryObj3);
            Insert ctrylist;
            
            Id Pctrt1 = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Introduction Project').getRecordTypeId();
            list<Milestone1_Project__c> milist = new list<Milestone1_Project__c>();
            Milestone1_Project__c mileProject1= new Milestone1_Project__c(RecordTypeId=Pctrt1,Name ='Country Project2 ',Offer_Launch__c=inOffLif.id,ForecastValidator__c=newUser1.id,
                ForecastStatus__c='Submitted for Validated',Country_Launch_Leader__c=newUser1.id,Send_activation_reminder_email__c= true,Country__c=cotryObj2.id,Country_Announcement_Date__c=Date.today());
            milist.add(mileProject1);
            Milestone1_Project__c mileProject2= new Milestone1_Project__c(RecordTypeId=Pctrt1,Name ='Country Project3 ',Offer_Launch__c=inOffLif.id,ForecastValidator__c=newUser1.id,
                ForecastStatus__c='Submitted for Validated',Country_Launch_Leader__c=newUser1.id,Country__c=cotryObj3.id,Country_Announcement_Date__c=Date.today());
            milist.add(mileProject2);
            
            insert milist;
            //insert Announcement
            Announcement__c annc = new Announcement__c(Name = 'Test Announcement',Offer_Lifecycle__c=inOffLif.id);
            insert annc;
              
          
            inOffLif.Sales_Date__c=Date.today().addDays(4);
            inOffLif.Offer_Classification__c ='Strategic';
            inOffLif.IsChangeLaunchClassification__c = true;
            inOffLif.BD_Status__c='Published'; 
            inOffLif.Launch_Owner__c = newUser2.id;   
            inOffLif.Forecast_Stage__c='Launch Commitment';
            inOffLif.Description__c = 'Test DEscription updated';
            
            //update inOffLif; 
        
            //elete mileProject2;
        
             mileProject1.Country_Launch_Leader__c =newUser2.id;
             mileProject1.BoxUserEmailId__c = null;
            mileProject1.Recommended_Country_classification__c ='Major';
                //update mileProject1;
            // To cover other methods Manjunath
            map<Id,String> mapId = new map<Id,String>();
          mapId.put(inOffLif.Id, inOffLif.Offer_Classification__c);
          AP_bFO_COLA_OfferCountryBUTaskCreation.phasesTasksDataReset(mapId);
          
          mapId = new map<Id,String>();
          mapId.put(mileProject2.Id, mileProject2.Recommended_Country_classification__c);
          AP_bFO_COLA_OfferCountryBUTaskCreation.pjctPhasesTasksDataReset(mapId);
        
          AP_bFO_COLA_OfferCountryBUTaskCreation.UpdateProjects(mapId);
    }
     static testMethod void unitTest2(){
     //for Withdraw record types
 
      user newUser1 = [select id,name, email,LastName,UserName from user where id=:UserInfo.getUserID()];
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User newUser2 = new User(Alias = 'tasdf', Email='staag837dfvn3jdc@testorg.com',EmailEncodingKey='UTF-8', LastName='Testingefasjhdfg', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='stasdfasdf@testorg.com');
        insert newUser2;
        List<CS_COLAbFO_ProjectPhaseTask__c>   csListPhaseTask=new  List<CS_COLAbFO_ProjectPhaseTask__c>(); 
        CS_COLAbFO_ProjectPhaseTask__c CS7 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='Country',Name ='CountryTask9',Phase_Name__c ='3. SELL',RecordType__c ='Offer Withdrawal Project',Task_Name__c ='Adapt the solution and sub-items to local Market');                          
              csListPhaseTask.add(CS7);        
        CS_COLAbFO_ProjectPhaseTask__c CS8 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='BU',Name ='BUWith',Phase_Name__c ='3. SELL',RecordType__c ='BU Master Withdrawal - Product Project',Task_Name__c ='301. Announcement (EU/Channels/Partners)');                          
            csListPhaseTask.add(CS8);
        Insert csListPhaseTask;
          Id rt3 = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Withdrawal - Product').getRecordTypeId();
            Offer_Lifecycle__c inOffLif3= new Offer_Lifecycle__c(); 
                inOffLif3.RecordTypeId=rt3;
                inOffLif3.Offer_Name__c='Test offer Schneider3';
                inOffLif3.Leading_Business_BU__c ='IT';
                inOffLif3.Offer_Classification__c ='Strategic';
                inOffLif3.Forecast_Unit__c='Kilo Euro';
                inOffLif3.Launch_Master_Assets_Status__c='Red';
                inOffLif3.Stage__c ='0 - Define';
                inOffLif3.End_of_Services_Date__c = date.today();
                inOffLif3.Withdrawal_Notice_Status__c='Draft';
                inOffLif3.Withdrawal_Owner__c =newUser1.id;
                inOffLif3.End_of_Commercialization_Date__c = date.today();
            Insert inOffLif3; 
            Country__c cotryObj= new Country__c();
                cotryObj.Name ='France';
                cotryObj.CountryCode__c='1FR';
            Insert cotryObj;
           Id Pctrt2 = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Withdrawal Project').getRecordTypeId();
             Milestone1_Project__c mileProject3= new Milestone1_Project__c(); 
                mileProject3.RecordTypeId=Pctrt2;
                mileProject3.Name ='By Country Project1 ';
                mileProject3.Offer_Launch__c=inOffLif3.id;
                mileProject3.ForecastValidator__c=newUser1.id;
                mileProject3.ForecastStatus__c='Submitted for Validated';
                mileProject3.Country_Withdrawal_Leader__c=newUser1.id;
                mileProject3.Country__c=cotryObj.id;
                mileProject3.Country_Announcement_Date__c=Date.today();
                //mileProject3.Country_Launch_Leader__c=newUser1.id;
                Test.startTest();
                Insert mileProject3 ;
                Test.stopTest(); 
            
            inOffLif3.Withdrawal_Owner__c = newUser2.id;
            inOffLif3.Withdrawal_Notice_Status__c='Published';
            inOffLif3.End_of_Services_Date__c = date.today().addDays(9);
            inOffLif3.End_of_Commercialization_Date__c = date.today().addDays(8);
            update inOffLif3;
            
            //System.runAs(user12) {
                mileProject3.Country_Withdrawal_Leader__c = newUser2.id;
                mileProject3.BoxUserEmailId__c = null;
                update mileProject3;
            //}
            
    }
    
     static testMethod void unitTest3(){
     
     //for solution record types
        Test.startTest();
      /* User newUser2 = Utils_TestMethods.createStandardUser('TestUser'); 
        Database.SaveResult UserInsertResult = Database.insert(newUser2, true); */
         user newUser1 = [select id,name, email,LastName,UserName from user where id=:UserInfo.getUserID()];
       // list<user> newUser2 = [select id,name, email,LastName,UserName from user limit 3];
        List<CS_COLAbFO_ProjectPhaseTask__c>   csListPhaseTask=new  List<CS_COLAbFO_ProjectPhaseTask__c>();  
        CS_COLAbFO_ProjectPhaseTask__c CS7 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='Country',Name ='CountryTask9',Phase_Name__c ='3. SELL',RecordType__c ='Offer Launch Solution Project',Task_Name__c ='Adapt the solution and sub-items to local Market');                          
              csListPhaseTask.add(CS7);        
        CS_COLAbFO_ProjectPhaseTask__c CS8 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='BU',Name ='BUWith',Phase_Name__c ='3. SELL',RecordType__c ='BU Offer Solution Launch Project',Task_Name__c ='301. Announcement (EU/Channels/Partners)');                          
            csListPhaseTask.add(CS8);
        Insert csListPhaseTask;
          Id rt3 = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Launch - Solution').getRecordTypeId();
            Offer_Lifecycle__c inOffLif3= new Offer_Lifecycle__c(); 
                inOffLif3.RecordTypeId=rt3;
                inOffLif3.Offer_Name__c='Test offer Schneider3';
                inOffLif3.Leading_Business_BU__c ='IT';
                inOffLif3.Offer_Classification__c ='Strategic';
                inOffLif3.Forecast_Unit__c='Kilo Euro';
                inOffLif3.Launch_Master_Assets_Status__c='Red';
                inOffLif3.Stage__c ='0 - Define';
                inOffLif3.BD_Status__c='Published'; 
            Insert inOffLif3; 
            Country__c cotryObj= new Country__c();
                cotryObj.Name ='France';
                cotryObj.CountryCode__c='1FR';
            Insert cotryObj;
           Id Pctrt2 = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Withdrawal Project').getRecordTypeId();
             Milestone1_Project__c mileProject3= new Milestone1_Project__c(); 
                mileProject3.RecordTypeId=Pctrt2;
                mileProject3.Name ='By Country Project1 ';
                mileProject3.Offer_Launch__c=inOffLif3.id;
                mileProject3.ForecastValidator__c=userInfo.getUserId();
                mileProject3.ForecastStatus__c='Submitted for Validated';
                mileProject3.Country_Withdrawal_Leader__c=userInfo.getUserId();
                mileProject3.Country__c=cotryObj.id;
                mileProject3.Country_Announcement_Date__c=Date.today();
                mileProject3.Country_Launch_Leader__c=newUser1.id;
            Insert mileProject3 ;
            Test.stopTest();    
    }
    
    static testMethod void unitTest4(){
        
        user newUser1 = [select id,name, email,LastName,UserName from user where id=:UserInfo.getUserID()];
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User newUser2 = new User(Alias = 'tasdf', Email='staag837dfvn3jdc@testorg.com',EmailEncodingKey='UTF-8', LastName='Testingefasjhdfg', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='stasdfasdf@testorg.com');
        insert newUser2;
        List<CS_COLAbFO_ProjectPhaseTask__c>   csListPhaseTask =new  List<CS_COLAbFO_ProjectPhaseTask__c>();
        CS_COLAbFO_ProjectPhaseTask__c CS3 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='Country',Name ='test1',Phase_Name__c ='Major Phase1',LaunchClassification__c='Major',RecordType__c ='Offer Introduction Project',Task_Name__c ='Major Phase 1 Task1 abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz');                          
                 csListPhaseTask.add(CS3);      
        CS_COLAbFO_ProjectPhaseTask__c CS4 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='Country',Name ='Test2',Phase_Name__c ='Major Phase 2',LaunchClassification__c='Major',RecordType__c ='Offer Introduction Project',Task_Name__c ='Major Phase 2 Task1');
             csListPhaseTask.add(CS4);
        CS_COLAbFO_ProjectPhaseTask__c CS5 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='Country',Name ='Test3',Phase_Name__c ='Strategic Phase 1',LaunchClassification__c='Strategic',RecordType__c ='Offer Introduction Project',Task_Name__c ='Strategic Phase 1 Task1');
             csListPhaseTask.add(CS5);
        CS_COLAbFO_ProjectPhaseTask__c CS6 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='Country',Name ='test4',Phase_Name__c ='Strategic Phase 2',LaunchClassification__c='Strategic',RecordType__c ='Offer Introduction Project',Task_Name__c ='Strategic Phase 2 Task1');                          
                 csListPhaseTask.add(CS6);   
                   CS_COLAbFO_ProjectPhaseTask__c CS7 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='BU',Name ='test5',Phase_Name__c ='Major Phase1',LaunchClassification__c='Major',RecordType__c ='BU Master Launch Project',Task_Name__c ='Major Phase 1 Task1 abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz abcdefghijklmnopqrstuvwxyz');                          
                 csListPhaseTask.add(CS7);      
        CS_COLAbFO_ProjectPhaseTask__c CS8 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='BU',Name ='Test6',Phase_Name__c ='Major Phase 2',LaunchClassification__c='Major',RecordType__c ='BU Master Launch Project',Task_Name__c ='Major Phase 2 Task1');
             csListPhaseTask.add(CS8);
        CS_COLAbFO_ProjectPhaseTask__c CS2 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='BU',Name ='Test7',Phase_Name__c ='Strategic Phase 1',LaunchClassification__c='Strategic',RecordType__c ='BU Master Launch Project',Task_Name__c ='Strategic Phase 1 Task1');
             csListPhaseTask.add(CS2);
        CS_COLAbFO_ProjectPhaseTask__c CS1 = new CS_COLAbFO_ProjectPhaseTask__c(BU_Country__c ='BU',Name ='test8',Phase_Name__c ='Strategic Phase 2',LaunchClassification__c='Strategic',RecordType__c ='BU Master Launch Project',Task_Name__c ='Strategic Phase 2 Task1');                          
                 csListPhaseTask.add(CS1);  
        Insert csListPhaseTask;
             
         Id rt = Schema.SObjectType.Offer_Lifecycle__c.getRecordTypeInfosByName().get('Offer Launch - Product').getRecordTypeId();
         Offer_Lifecycle__c inOffLif= new Offer_Lifecycle__c(); 
                inOffLif.RecordTypeId=rt;
                inOffLif.Offer_Name__c='Test offer Schneider';
                inOffLif.Leading_Business_BU__c ='IT';
                inOffLif.Offer_Classification__c ='Strategic';
                inOffLif.Forecast_Unit__c='Kilo Euro';
                inOffLif.Launch_Master_Assets_Status__c='Red';
                inOffLif.Stage__c ='0 - Define';
                inOffLif.Launch_Owner__c = newUser1.id;
                inOffLif.BD_Status__c='Published'; 
                inOffLif.Forecast_Stage__c='Launch Commitment';
                inOffLif.Sales_Date__c=Date.today().addDays(4);
                inOffLif.Description__c = 'Test DEscription updated';
                //inOffLif.IsChangeLaunchClassification__c = true;
                
            Test.startTest();
            Insert inOffLif; 
            Test.StopTest();
            
            list<Country__c> ctrylist = new list<Country__c>();
            Country__c cotryObj2= new Country__c(Name ='China', CountryCode__c='1CH');
            ctrylist.add(cotryObj2);
            Country__c cotryObj3= new Country__c(Name ='India', CountryCode__c='1IN');
            ctrylist.add(cotryObj3);
            Insert ctrylist;
            
            Id Pctrt1 = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Introduction Project').getRecordTypeId();
            list<Milestone1_Project__c> milist = new list<Milestone1_Project__c>();
            Milestone1_Project__c mileProject1= new Milestone1_Project__c(RecordTypeId=Pctrt1,Name ='Country Project2 ',Offer_Launch__c=inOffLif.id,ForecastValidator__c=newUser1.id,
                ForecastStatus__c='Submitted for Validated',Country_Launch_Leader__c=newUser1.id,Send_activation_reminder_email__c= true,Country__c=cotryObj2.id,Country_Announcement_Date__c=Date.today());
            milist.add(mileProject1);
            Milestone1_Project__c mileProject2= new Milestone1_Project__c(RecordTypeId=Pctrt1,Name ='Country Project3 ',Offer_Launch__c=inOffLif.id,ForecastValidator__c=newUser1.id,
                ForecastStatus__c='Submitted for Validated',Country_Launch_Leader__c=newUser1.id,Country__c=cotryObj3.id,Country_Announcement_Date__c=Date.today());
            milist.add(mileProject2);
            
            insert milist;
            //insert Announcement
            Announcement__c annc = new Announcement__c(Name = 'Test Announcement',Offer_Lifecycle__c=inOffLif.id);
            insert annc;
            
            Delete mileProject2;
        
             mileProject1.Country_Launch_Leader__c =newUser2.id;
             mileProject1.BoxUserEmailId__c = null;
          //      mileProject1.TECH_CalculateSendEmailCountrySellDate__c =true;
                update mileProject1;
    }
}