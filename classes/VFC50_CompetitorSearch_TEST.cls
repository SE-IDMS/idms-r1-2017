@isTest
private class VFC50_CompetitorSearch_TEST{

    static testMethod void VFC50_CompetitorSearchTest() {
         //Start of Test Data preparation
         
    Profile profile = [select id from profile where name='System Administrator' LIMIT 1];
    User user = Utils_TestMethods.createStandardUser(profile.id, '2test');
    
    Profile profile1 = [select id from profile where name='System Administrator' LIMIT 1];    
    User user2 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse2');
    database.insert(user2);
        
    User user3 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse3');
    database.insert(user3);
    
    User user4 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse4');
    database.insert(user4);
    
    Country__c country1 = Utils_TestMethods.createCountry();
    insert country1;
    
    Competitor__c comp = Utils_TestMethods.createCompetitor();
    insert comp;
    
    CompetitorCountry__c compCountry= Utils_TestMethods.createCompetitorCountry(comp.id, country1.id);
    insert compCountry;
    
    Account account1 = Utils_TestMethods.createAccount();
    insert account1;
        
    Opportunity oppty = Utils_TestMethods.createOpenOpportunity(account1.id);
    oppty.OwnerId = user3.id;      
    insert oppty;
    
    opportunityTeamMember opportunityTeam1 = new opportunityTeamMember(OpportunityID=oppty.id,userId=user2.id);
    database.insert(opportunityTeam1);
    
    opportunityShare Sharing = new opportunityShare();
    sharing.OpportunityId=oppty.id;
    sharing.UserOrGroupId= user2.id;
    sharing.OpportunityAccessLevel = Label.CL00203;   
    insert(sharing);
        
    List<String> keywords = New List<String>();
    keywords.add('test');
    List<String> filters = New List<String>();
    filters.add('BD');
    filters.add(country1.id);
        
    Utils_DataSource dataSource = Utils_Factory.getDataSource('COMP');
    Utils_DataSource.Result searchResult = dataSource.Search(keywords,filters);
    sObject tempsObject = searchResult.recordList[0];
    PageReference pageRef;
    VFC_ControllerBase basecontroller = new VFC_ControllerBase();        
    //End of Test Data preparation.
    
    system.runAs(user3)
    {        
        ApexPages.StandardController oc1 = new ApexPages.StandardController(new OPP_OpportunityCompetitor__c());        
        VFC50_CompetitorSearch compSearch = new VFC50_CompetitorSearch(oc1);
        pageRef= Page.VFP50_CompetitorSearch; 
        pageRef.getParameters().put(Label.CL00452, oppty.id);
        pageRef.getParameters().put(Label.CL00515, oppty.Name);

        System.debug('~~~~~~~~~~Start of Test~~~~~~~~~~');
        VCC06_DisplaySearchResults componentcontroller = compSearch.resultsController; 
        compSearch.searchText='test';
        compSearch.level1 = 'BD';
        compSearch.level2 = country1.id;
        Test.setCurrentPageReference(pageRef);        
        compSearch.PerformAction(tempsObject, compSearch);
       // system.debug('********search****'+)
        
        compSearch.checkAccess();
        compSearch.search();
        compSearch.clear();
        compSearch.cancel();
        
        compSearch.searchText='';
        compSearch.level1 = Label.CL00355;
        compSearch.level2 = Label.CL00355;
        compSearch.search();

        for(Integer index=0; index<1000; index++)
        {
            compSearch.searchText=compSearch.searchText+'1234567890';
        }
        compSearch.search();             
        
        OPP_OpportunityCompetitor__c opComp = new OPP_OpportunityCompetitor__c(TECH_CompetitorName__c=comp.id, OpportunityName__C = oppty.id);
        insert opComp;
        compSearch.PerformAction(tempsObject, compSearch);
        
        pageRef.getParameters().put(Label.CL00452, null);
        Test.setCurrentPageReference(pageRef);         
        compSearch.cancel();           
    }
    
    pageRef = NULL;
     
    system.runAs(user2)
    {
        ApexPages.StandardController oc2 = new ApexPages.StandardController(new OPP_OpportunityCompetitor__c());        
        
        pageRef= page.VFP50_CompetitorSearch; 
        pageRef.getParameters().put(Label.CL00452, oppty.id);
        pageRef.getParameters().put(Label.CL00515, oppty.Name);
        Test.setCurrentPageReference(pageRef); 
        VFC50_CompetitorSearch compSearch = new VFC50_CompetitorSearch(oc2);
        compSearch.checkAccess(); 
    }
    
    
    pageRef = NULL;
    system.runAs(user)
    {     
        ApexPages.StandardController oc3= new ApexPages.StandardController(new OPP_OpportunityCompetitor__c());
        PageReference pageRef2= page.VFP50_CompetitorSearch; 
        pageRef2.getParameters().put(Label.CL00452, oppty.id);
        pageRef2.getParameters().put(Label.CL00515, oppty.Name);
        Test.setCurrentPageReference(pageRef2); 
        VFC50_CompetitorSearch cls2 = New VFC50_CompetitorSearch (oc3);
        cls2.checkAccess(); 
    }
    system.runAs(user4)
    {     
        ApexPages.StandardController oc4= new ApexPages.StandardController(new OPP_OpportunityCompetitor__c());
        pageRef= page.VFP50_CompetitorSearch; 
        pageRef.getParameters().put(Label.CL00452, oppty.id);
        pageRef.getParameters().put(Label.CL00515, oppty.Name);
        Test.setCurrentPageReference(pageRef); 
        VFC50_CompetitorSearch cls3 = New VFC50_CompetitorSearch(oc4);
        cls3.checkAccess(); 
    }
}
}