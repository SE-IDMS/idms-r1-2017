@isTest
 public class VCC_CassiniMultiAttachment_TEST{
     public static testMethod void TestAttachmentcontroller() { 
        
        Country__c country=new Country__c(countrycode__c='EY',Name='YEMEN');
        insert country;
        System.assert(country!=null);
        
        Account acc1=new Account();
        acc1.Name='Test Acc1234567891';
        acc1.Street__c ='869689';
        acc1.City__c ='Bangalore';
        acc1.Country__c=country.Id;
        acc1.BillingCountry='US';
        acc1.BillingState='NY';
        acc1.BillingCity='New York';
        acc1.POBox__c='NY City';
        insert acc1;
        System.assert(acc1!=null);
        
        Contact con1=new Contact();
        con1.AccountId=acc1.Id;
        con1.FirstName='Test12341';
        con1.Email='Prashanth.GS@non.schneider-electric.com';
        con1.MobilePhone='09999999999';
        con1.LastName='test98761';
        insert con1;
        System.assert(con1!=null);
        
        Contract contract=new Contract();
        contract.StartDate=System.now().Date();
        contract.Points__c=109;
        contract.WebAccess__c='Cassini';
        contract.AccountId=acc1.Id;
        contract.ContactName__c=con1.Id;
        insert contract;
        System.assert(contract!=null);
        
        Case objCase=new Case(Description='Test Desc12341',CustomerRequest__c='Test CustReq12341',
                              RelatedContract__c=contract.Id,ContactID=con1.Id);
        insert objCase;
        System.assert(objCase!=null);
        
        Attachment attach=new Attachment(); 
        attach.Name='Unit Test Attachment12345671'; 
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
        String strBodyBlob=String.ValueOf(EncodingUtil.base64Encode(bodyBlob));
        attach.body=bodyBlob; 
        attach.parentId=objCase.Id; 
        attach.ContentType = 'application/msword'; 
        attach.IsPrivate = false; 
        attach.Description = 'Test1234561'; 
        insert attach; 
        System.assert(attach!=null);
        
        test.startTest();
            //VCC_CassiniMultiAttachment attachmentPage=new VCC_CassiniMultiAttachment();
            //VCC_CassiniMultiAttachment.attachBlob(String.valueOf(objCase.Id),String.valueOf(attach.Id),attach.Name,attach.ContentType,strBodyBlob);
            VCC_CassiniMultiAttachment.attachBlob(String.valueOf(objCase.Id),String.valueOf(attach.Id),attach.Name,attach.ContentType,strBodyBlob);
            VCC_CassiniMultiAttachment.attachBlob(String.valueOf(objCase.Id),'',attach.Name,attach.ContentType,strBodyBlob);
            VCC_CassiniMultiAttachment.deleteAttachment(String.valueOf(attach.Id));
        test.stopTest();
    }
}