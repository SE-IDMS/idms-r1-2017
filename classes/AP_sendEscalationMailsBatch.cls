public class AP_sendEscalationMailsBatch
{
public static String debugLog;

public static List<user> getUsers(list<string>setUserids)
    {
        List<user> luser=[SELECT id,Email, ManagerEmail__c,Manager1Email__c from user where id IN :setUserids];
        return luser;
    }
Public static void createXAStakeholderonSH(List<sObject> containActionList,string objtlookup,string owner,string duedate,string accOrg){
        List<Stakeholders__c> listStholder=new List<Stakeholders__c>();
        map<id,String> mapXAOwnerids=new map<id,String>();      
        map<id,date> mapXAidDuedate=new map<id,date>();
        map<id,String> mapXAOrgids=new map<id,String>();
        map<id,String> mapXAStatusids=new map<id,String>();
        map<id,String> mapcsConEmailids=new map<id,String>();
        map<id,sObject> sObjectMap=new map<id,sObject>();
        system.debug('##containActionList ##'+ containActionList);
        for(sObject cAction:containActionList)
        { 
         system.debug('##cAction##'+ cAction);
               mapXAOwnerids.put(cAction.id,String.valueof(cAction.get(owner)));              
               mapXAidDuedate.put(cAction.id,Date.valueof(cAction.get(duedate)));
               mapXAOrgids.put(cAction.id,String.valueof(cAction.get(accOrg)));
               mapXAStatusids.put(cAction.id,String.valueof(cAction.get('Status__c')));
               sObjectMap.put(cAction.id, cAction);
            // Start As part of April 2015 release the bellow if condition added for Complaint Request  to get the contact email id,  added by Ram Chilukuri BR is BR-7133.
               if(cAction.getSobjectType().getDescribe().getLocalName() == 'ComplaintRequest__c'){
                    mapcsConEmailids.put(cAction.id,String.valueof(cAction.get('ContactEmailAccountableOrganization__c')));
                    datetime ddate=Datetime.valueof(cAction.get(duedate));
                    string sdate=ddate.format('yyyy-MM-dd');
                    mapXAidDuedate.put(cAction.id,Date.valueof(sdate));
                } // End As part of April 2015 release
        }
         system.debug('##mapXAOwnerids##'+ mapXAOwnerids);
         system.debug('##mapXAidDuedate##'+ mapXAidDuedate);
        If(mapXAOwnerids.size()>0 || mapXAidDuedate.size()>0 || mapXAOrgids.size()>0) {
      createStakeholder(mapXAOwnerids,mapXAidDuedate,mapXAOrgids,mapXAStatusids,sObjectMap,objtlookup,mapcsConEmailids);}
      //return 200;
    }
    Public static void createStakeholder(map<id,string> mapOwnerids,map<id,date> mapidDuedate,map<id,string> mapXAOrgids,map<id,String> mapXAStatusids, map<id,sObject> sObjectMap, string lfield,map<id,String> mapcsConEmailids){
        debugLog=debugLog+'createSH';
        //Integer toRet=0;      
       //System.debug('createStakeholder called for '+lfield);
        Map<Id, user> userMap=new Map<Id, user>();
        map<string,list<CS_Stakeholders__c>> mapCS=getCustomsetingsMap();
        system.debug('##mapCS ##'+ mapCS);
        List<Stakeholders__c> listStholder=new List<Stakeholders__c>();
        map<id,string> mapusrAccOrgManager=getUsersofAccOrg(mapXAOrgids.values(),label.CLAPR15I2P09);
        map<id,string> mapusrAccOrgusr=getUsersofAccOrg(mapXAOrgids.values(),label.CLOCT14I2P07);
        
        system.debug('@@@@@  mapusrAccOrgManager@@@@@'+mapusrAccOrgManager);
        list<string>FinalSet=new list<string>();
  //   Set<string>FinalSet=new Set<string>();
          FinalSet.addall(mapOwnerids.values());
          FinalSet.addall(mapusrAccOrgusr.values());
          FinalSet.addall(mapusrAccOrgManager.values());
          list<user> listUsers=getUsers(FinalSet);
//List<user> listUsers=getUsers(FinalSet);       
        for(user tempuser:listUsers){
            userMap.put(tempuser.id,tempuser);
        }
        system.debug('##userMap ##'+ userMap);
        
        Map<id, sObject> sObjectsToSeeMap=new Map<id, sObject>();
        Map<id, Integer> sObjectsLastNotificationDayMap=new Map<id, Integer>();
        for(id cAction:mapOwnerids.keyset())
        { 
         user tuser=userMap.get(mapOwnerids.get(cAction));
         date ddate=mapidDuedate.get(cAction);
          system.debug('##tuser##'+tuser);
            //k:Fix for 8th Day
            //Integer maxcs=getMaxcs(mapCS.get(lfield));
            //k:Fix for 8th Day
            for(CS_Stakeholders__c tmpCS :mapCS.get(lfield))
            {system.debug('##tmpCS ##'+tmpCS );
                If(tuser!=null){
                    Stakeholders__c sh = new Stakeholders__c();
                    sh.put(lfield,cAction);

                   if(mapidDuedate.get(cAction)==null){sh.DueDate__c=null;}
                   else{ 
                   sh.DueDate__c = ddate+Integer.valueof(tmpCS.EscalationDays__c);
                   //sh.DueDate__c=getBusinessDueDate(ddate,Integer.valueof(tmpCS.EscalationDays__c));(commented as part of BR-9441 to include weekends in overdue days calculations)
                   }

                    Integer daysFromddate;
                    if (ddate!=null){
                        //daysFromddate=(ddate.daysBetween(System.today())-daysBetweenExcludingWeekendsOld(getDateTime(ddate)+1, getDateTime(System.today())));(commented as part of BR-9441 to include weekends in overdue days calculations)
                        daysFromddate=ddate.daysBetween(System.today());
                    }
                    Sobject currObject = sObjectMap.get(cAction);

                    if (currObject.get('LastNotificationDay__c')==null){
                        currObject.put('LastNotificationDay__c', 0);
                    }
                    
                    if (System.today()==sh.DueDate__c|| (daysFromddate!=null && Integer.valueof(tmpCS.EscalationDays__c)!=7&&Integer.valueof(tmpCS.EscalationDays__c)==getLastNotificationDay(mapCS.get(lfield), daysFromddate) && daysFromddate>Integer.valueof(tmpCS.EscalationDays__c) && Integer.valueof(currObject.get('LastNotificationDay__c'))<Integer.valueof(tmpCS.EscalationDays__c)) || (Integer.valueof(tmpCS.EscalationDays__c)==7&&currObject.getSobjectType().getDescribe().getLocalName() == 'ComplaintRequest__c' && Integer.valueof(currObject.get('LastNotificationDay__c'))==7 && DateTime.valueOf(currObject.get('TECH_ReminderDate__c')).date()<=System.today()) ){
                        //Integer.valueof(currObject.get('LastNotificationDay__c'))>=getLastOfLastNotificationDay(mapCS.get(lfield), daysFromddate);
                        sh.Status__c=mapXAStatusids.get(cAction);
                        sh.Level__c=tmpCS.Role__c;
                        sh.EmailTemplate__c=tmpCS.EmailTemplate__c;
                        if(tmpCS.Role__c==Label.CLOCT14I2P04){ // CLOCT14I2P04 owner
                        sh.EmailID__c=tuser.Email;
                        }
                        else if(tmpCS.Role__c==Label.CLOCT14I2P05){ // CLOCT14I2P05 Manager
                        sh.EmailID__c=tuser.ManagerEmail__c;
                        }
                        else if(tmpCS.Role__c==Label.CLOCT14I2P06){ // CLOCT14I2P06 Manager+1
                        system.debug('*****object***'+currObject.getSobjectType().getDescribe().getLocalName());
                         if(currObject.getSobjectType().getDescribe().getLocalName() != 'ComplaintRequest__c'){//Dec 2016 Release BR-10560, added by Ram Chilukuri
                        sh.EmailID__c=tuser.Manager1Email__c;
                         }
                        }
                        else if(tmpCS.Role__c==Label.CLOCT14I2P07){ // CLOCT14I2P07 CS&Q Manager
                        user tuser1=userMap.get(mapusrAccOrgusr.get(mapXAOrgids.get(cAction)));
                            if(tuser1!=null){
                                sh.EmailID__c=tuser1.Email;}
                            else{sh.EmailID__c=null;}
                             
                            
                        }
                            
                     //Added by Ram remove records with no emails.
                      if(sh.EmailID__c!=null){
                      listStholder.add(sh);}
                     //  listStholder.add(sh);// end ram
                       //currObject.put('LastNotificationDay__c',tmpCS.EscalationDays__c);
                       if (sObjectsLastNotificationDayMap.get(cAction)!=null){
                        if (Integer.valueOf(tmpCS.EscalationDays__c)>sObjectsLastNotificationDayMap.get(cAction)) {
                            sObjectsLastNotificationDayMap.put(cAction, Integer.valueOf(tmpCS.EscalationDays__c));
                        }
                       } else {
                        sObjectsLastNotificationDayMap.put(cAction, Integer.valueOf(tmpCS.EscalationDays__c));
                        sObjectsToSeeMap.put(cAction, currObject);
                       }
                   }
                }
                 else{
/* Start As part of April 2015 release the bellow elase condition added for Complaint Request  to create the stakeholders record with contact email id if owner field is null, added by Ram Chilukuri BR is BR-7133.*/
                 string contactEmail;
                 if(cAction.getSobjectType().getDescribe().getLocalName() == 'ComplaintRequest__c'){
                    contactEmail = mapcsConEmailids.get(cAction);
                    if(contactEmail != null){
                     system.debug('object name ===>'+lfield+'==Role from CS is==>'+tmpCS.Role__c+'==Contact email==>'+contactEmail);
                        Stakeholders__c sh1 = new Stakeholders__c();
                        sh1.put(lfield,cAction);
                       if(mapidDuedate.get(cAction)==null){sh1.DueDate__c=null;}
                       else{ 
                       sh1.DueDate__c=mapidDuedate.get(cAction)+Integer.valueof(tmpCS.EscalationDays__c);
                       //sh1.DueDate__c=getBusinessDueDate(mapidDuedate.get(cAction),Integer.valueof(tmpCS.EscalationDays__c));(commented as part of BR-9441 to include weekends in overdue days calculations)
                       }
                       
                     Integer daysFromddate;
                    if (ddate!=null ){
                        //daysFromddate=(ddate.daysBetween(System.today())-daysBetweenExcludingWeekendsOld(getDateTime(ddate)+1, getDateTime(System.today())));(commented as part of BR-9441 to include weekends in overdue days calculations)
                        daysFromddate=ddate.daysBetween(System.today());
                    }
                    Sobject currObject = sObjectMap.get(cAction);

                    if (currObject.get('LastNotificationDay__c')==null){
                        currObject.put('LastNotificationDay__c', 0);
                    }
                    
                    if (System.today()==sh1.DueDate__c || (daysFromddate!=null && Integer.valueof(tmpCS.EscalationDays__c)!=7 && Integer.valueof(tmpCS.EscalationDays__c)==getLastNotificationDay(mapCS.get(lfield), daysFromddate) && daysFromddate>Integer.valueof(tmpCS.EscalationDays__c) && Integer.valueof(currObject.get('LastNotificationDay__c'))<Integer.valueof(tmpCS.EscalationDays__c)) || (Integer.valueof(tmpCS.EscalationDays__c)==7&&currObject.getSobjectType().getDescribe().getLocalName() == 'ComplaintRequest__c' && Integer.valueof(currObject.get('LastNotificationDay__c'))==7 && DateTime.valueOf(currObject.get('TECH_ReminderDate__c')).date()<=System.today())){
                            sh1.Status__c=mapXAStatusids.get(cAction);
                            sh1.Level__c=tmpCS.Role__c;
                            sh1.EmailTemplate__c=tmpCS.EmailTemplate__c; 
                        if(tmpCS.Role__c==Label.CLOCT14I2P04){ // CLOCT14I2P04 owner
                        sh1.EmailID__c=contactEmail;
                        }
                        else if(tmpCS.Role__c==Label.CLOCT14I2P07){ // CLOCT14I2P07 CS&Q Manager
                        user tuser2=userMap.get(mapusrAccOrgusr.get(mapXAOrgids.get(cAction)));
                            if(tuser2!=null){
                                sh1.EmailID__c=tuser2.Email;}           
                            }
                        else if(tmpCS.Role__c==Label.CLAPR15I2P09){
                            user tuser3=userMap.get(mapusrAccOrgManager.get(mapXAOrgids.get(cAction)));
                            if(tuser3!=null){
                               sh1.EmailID__c=tuser3.Email;}    
                        }                       
                            if(sh1.EmailID__c!=null){
                            listStholder.add(sh1);}
                               //currObject.put('LastNotificationDay__c',tmpCS.EscalationDays__c);
                               if (sObjectsLastNotificationDayMap.get(cAction)!=null){
                                    if (Integer.valueOf(tmpCS.EscalationDays__c)>sObjectsLastNotificationDayMap.get(cAction)) {
                                        sObjectsLastNotificationDayMap.put(cAction, Integer.valueOf(tmpCS.EscalationDays__c));
                                    }
                               } else {
                                    sObjectsLastNotificationDayMap.put(cAction, Integer.valueOf(tmpCS.EscalationDays__c));
                                    sObjectsToSeeMap.put(cAction, currObject);
                               }

                            System.debug('sh Added:'+sh1.EmailID__c);
                       }
                       
                        
                    }
                  } //ends
                }
            }   
        }
        
        debugLog=debugLog+',listStholder:'+listStholder;
        system.debug('##listStholder ##'+ listStholder+ 'debugLogs:'+debugLog);
        
      if(listStholder.size()>0){
      /*toRet++;
      if (toRet==202) {
          System.debug('KImp:');
          Database.SaveResult[] lsr = database.insert(listStholder);
          for (Database.SaveResult dsr:lsr){
              if (dsr.getErrors().size()>0) {
                  System.debug('kImp Error Occured:'+dsr.getErrors());
              }
          }
      } else {
          insert listStholder;
      }*/
//          insert listStholder;
          //boolean pass=true;
          //Map<id, sObject> sObjectsToSeeMap=new Map<id, sObject>();
          //Map<id, sObject> toUpdateLastNotification=new Map<id, sObject>();
          Set<sObject> toUpdateLastNotification=new Set<sObject>();
          String[] possibleIdFields=new String[]{'PA__c','CA__c','ComplaintRequest__c','XA__c'};
          Database.SaveResult[] lsr = database.insert(listStholder);
          
          for (Integer lsri=0; lsri<lsr.size();lsri++){
            if (lsr[lsri].getErrors().size()==0){
                Id currId=null;
                for (String currField:possibleIdFields) {
                    if (listStholder[lsri].get(currField)!=null && listStholder[lsri].get(currField)!=''){
                        currId=String.valueOf(listStholder[lsri].get(currField));
                    }
                }
                if (currId!=null) {
                    //toUpdateLastNotification.put(currID, sObjectsToSeeMap.get(currID));
                    sObjectsToSeeMap.get(currID).put('LastNotificationDay__c', sObjectsLastNotificationDayMap.get(currID));
                    toUpdateLastNotification.add(sObjectsToSeeMap.get(currID));
                }
            }
          }
          if(toUpdateLastNotification.size()>0) {
            List<sObject> toUpdateLastNotificationList=new List<sObject>();
            toUpdateLastNotificationList.addAll(toUpdateLastNotification);
            System.debug('Before Change List=' + toUpdateLastNotificationList);
            List<sObject> FinalListToUpdate = new List<sObject>();
            List<sObject> crListToUpdate = new List<sObject>(); // created to exclude issue owner,accountable organization, due date and status from getting updated 
            //List<ComplaintRequest__c> crIdList = new List<ComplaintRequest__c>(); 
            ComplaintRequest__c copyOfsingleCR = new ComplaintRequest__c();//stores single CR from the toUpdateLastNotificationList
              for(sObject cr:toUpdateLastNotificationList) { 
                  if(cr.getSobjectType().getDescribe().getLocalName() == 'ComplaintRequest__c')
                  {     
                      
                      System.debug('cr='+cr);
                      copyOfsingleCR = (ComplaintRequest__c) cr;
                      System.debug('test2='+copyOfsingleCR);
                      System.debug('test2 id ='+ copyOfsingleCR.Id);
                      ComplaintRequest__c crToAdd = new ComplaintRequest__c(id=copyOfsingleCR.id); // created to put values in new crListToUpdate
                      //crToAdd.id = copyOfsingleCR.id;
                      crToAdd.LastNotificationDay__c = copyOfsingleCR.LastNotificationDay__c;
                      if(copyOfsingleCR.LastNotificationDay__c==3 || copyOfsingleCR.LastNotificationDay__c==7){//Dec 2016 Release BR-10560, added by Ram Chilukuri
                        crToAdd.LastNotificationDay__c=7;
                        crToAdd.TECH_ReminderDate__c=System.Now()+7;   
                      }
                      crListToUpdate.add(crToAdd);
                      //toUpdateLastNotificationList.remove(cr);
                  }
                  else {
                      FinalListToUpdate.add(cr);// this list exclude records of Complaint/Request object.
                  }
              }
              //System.debug('Complaint/Request = '+ crIdList);
            //crListToUpdate = [select id, IssueOwner__c, TECH_DueDate__c, AccountableOrganization__c,Status__c,ContactEmailAccountableOrganization__c from ComplaintRequest__c where ID IN: crIdList];
            System.debug('CR Final List=' + crListToUpdate);
            FinalListToUpdate.addAll(crListToUpdate);  // adding the fresh created list for Complaint/Request
            System.debug('Final List=' + FinalListToUpdate);
            Database.SaveResult[] usr = database.update(FinalListToUpdate);
            //Database.SaveResult[] usr = database.update(toUpdateLastNotificationList);
            //update toUpdateLastNotificationList;
          }

          /*for (Database.SaveResult dsr:lsr){
              if (dsr.getErrors().size()>0) {
                  //pass=false;
              } else {
                
                toUpdateLastNotification.put()
              }
          }*/

      }
      //return toRet;
    }
    //----------------------
    
    //Method NOT Used in this class (Can be removed if not used in it's TEST class also.)
    public static Integer getLastOfLastNotificationDay(List<CS_Stakeholders__c> csList, Integer daysFromddate){
        //Integer.valueof(currObject.get('LastNotificationDay__c'))>=getLastOfLastNotificationDay(mapCS.get(lfield), daysFromddate);
        //It should be comparing daysFromddate< (something) also.
        //daysFromDate=3
        //LND>2 (No) LND>=2 (NO)
        //LND>1 (No) LND>=1 (YES)
        Integer max1=0;
        for(CS_Stakeholders__c tmpCS :csList) {
            if (Integer.valueOf(tmpCS.EscalationDays__c)>max1 && Integer.valueOf(tmpCS.EscalationDays__c)<daysFromddate){
                max1=Integer.valueOf(tmpCS.EscalationDays__c);
            }
        }
        
        Integer max2=0;
        for(CS_Stakeholders__c tmpCS :csList) {
            if (Integer.valueOf(tmpCS.EscalationDays__c)>max2 && Integer.valueOf(tmpCS.EscalationDays__c)<max1){
                max2=Integer.valueOf(tmpCS.EscalationDays__c);
            }
        }
        
        return max2;
    }


    public static Integer getLastNotificationDay(List<CS_Stakeholders__c> csList, Integer daysFromddate){
        //Integer.valueof(currObject.get('LastNotificationDay__c'))>=getLastOfLastNotificationDay(mapCS.get(lfield), daysFromddate);
        //It should be comparing daysFromddate< (something) also.
        //daysFromDate=3
        //LND>2 (No) LND>=2 (NO)
        //LND>1 (No) LND>=1 (YES)
        Integer max1=0;
        for(CS_Stakeholders__c tmpCS :csList) {
            if (Integer.valueOf(tmpCS.EscalationDays__c)>max1 && Integer.valueOf(tmpCS.EscalationDays__c)<=daysFromddate){//added by Ram '='
                max1=Integer.valueOf(tmpCS.EscalationDays__c);
            }
        }
       
        return max1;
    }
    
    //Method NOT Used
/*    public static Integer getMaxcs(List<CS_Stakeholders__c> csList){
        Integer max1=0;
        for(CS_Stakeholders__c tmpCS :csList) {
            if (Integer.valueOf(tmpCS.EscalationDays__c)>max1){
                max1=Integer.valueOf(tmpCS.EscalationDays__c);
            }
        }
        return max1;
    }
*/    
    public static DateTime getDateTime(Date dToday){
        Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
        return dt;
    }
    
public static map<string, list<CS_Stakeholders__c>> getCustomsetingsMap(){

        map<string,list<CS_Stakeholders__c>> mapCS=new map<string,list<CS_Stakeholders__c>>();
            Map<string,CS_Stakeholders__c> cvalues = CS_Stakeholders__c.getAll();
        
         for(CS_Stakeholders__c tempCS:cvalues.Values())
            {
            if(mapCS.containskey(tempCS.ObjectName__c)){
                    mapCS.get(tempCS.ObjectName__c).add(tempCS);
                }
            else{
                mapCS.put(tempCS.ObjectName__c, new List<CS_Stakeholders__c>());            
                mapCS.get(tempCS.ObjectName__c).add(tempCS);
                }
            }
            return mapCS;
        }
        
        static Integer countt=0;
public static date getBusinessDueDate(Date duedate,integer dys){
        date ModifiedDate;
        integer days;
        
        
        date edate=duedate+dys;
        System.debug('edate:'+countt+':'+edate);
        System.debug('edate.day():'+countt+':'+edate.day());
        datetime startdate=datetime.newInstance(duedate.year(), duedate.month(),duedate.day());
        datetime endDate=datetime.newInstance(edate.year(), edate.month(),edate.day());
        System.debug('edate.day():'+countt+':'+edate.day());
        //datetime endDate=datetime.newInstance(edate, 0);
        System.debug('endDate:'+countt+':'+endDate);
        countt++;
        days=daysBetweenExcludingWeekends(startdate + 1,endDate);     
        system.debug('@@@@@@@days%%'+days+'  @@@@@@@%dys%%'+dys);       
            ModifiedDate=duedate+dys+days;
            DateTime myDateTime = (DateTime)ModifiedDate;
        String dayOfWeek = myDateTime.format('E');
        if (dayOfWeek=='Sun')
                {
                ModifiedDate=ModifiedDate+1;
                }
        else if(dayOfWeek=='Sat'){ModifiedDate=ModifiedDate+2;}
        
            system.debug('%%%%%@@@@@@@1@'+ModifiedDate);
     
         return ModifiedDate; 
        }
public static Integer daysBetweenExcludingWeekends(Datetime startDate, Datetime endDate) {
   Integer i = 0;

    while (startDate <= endDate) {
        if (startDate.format('EEE')== 'Sat'){i = i + 1; endDate = endDate.addDays(1);}else if( startDate.format('EEE') == 'Sun') {
            i = i + 1;
            System.debug('ed:'+endDate);
            endDate = endDate.addDays(1);
            System.debug('ed:'+endDate);
        }
        startDate = startDate.addDays(1);
    }

    return i;
}       

/* 
Commented as part of BR-9441 March 16 Release
Swapnil Saurav
public static Integer daysBetweenExcludingWeekendsOld(Datetime startDate, Datetime endDate) {
   Integer i = 0;

    while (startDate <= endDate) {
        if (startDate.format('EEE')== 'Sat'){i = i + 1;}else if( startDate.format('EEE') == 'Sun') {
            i = i + 1;
        }
        startDate = startDate.addDays(1);
    }

    return i;
}       
*/
/*//Used only for Testing
public static Integer daysBetweenExcludingWeekendsBefore(Datetime startDate, Datetime endDate) {
   Integer i = 0;

    while (startDate <= endDate) {
        if (endDate.format('EEE')== 'Sat'){i = i + 1; startDate = startDate.addDays(-1);}else if( endDate.format('EEE') == 'Sun') {
            i = i + 1;
            //System.debug('ed:'+endDate);
            startDate = startDate.addDays(-1);
            //System.debug('ed:'+endDate);
        }
        endDate = endDate.addDays(-1);
    }

    return i;
}*/

public static map<id,string> getUsersofAccOrg(list<string>setUserids,string srole)
    {
        map<id,string> MapOrgStkHolders=new map<id,string>();        
        system.debug('*****Role***'+srole);
        system.debug('*****setUserids***'+setUserids);
    list<EntityStakeholder__c>StkHldrValues=[SELECT Id,User__c,Role__c,BusinessRiskEscalationEntity__c from EntityStakeholder__c where BusinessRiskEscalationEntity__c IN:setUserids and Role__c=:srole];
//  if(StkHldrValues.size()>0){
For(EntityStakeholder__c tempStkHldrValue: StkHldrValues)
{
MapOrgStkHolders.put(tempStkHldrValue.BusinessRiskEscalationEntity__c,tempStkHldrValue.User__c);
}
system.debug('@@@@@  MapOrgStkHolders@@@@@'+MapOrgStkHolders);
return MapOrgStkHolders;
    }//}       
}