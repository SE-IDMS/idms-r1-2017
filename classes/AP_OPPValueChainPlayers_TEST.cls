/*
    Author          : Bhuvana S
    Description     : Test class for AP_OPPValueChainPlayers
*/
@isTest
private class AP_OPPValueChainPlayers_TEST 
{
    static testMethod void testValueChainPlayers()
    {
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
        for(Profile p : ps)
        {
            profiles.put(p.name, p.id);
        }
        user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null and BypassVR__c=TRUE limit 1];
        User stdUser;
        Account acc;
        Contact con;
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        system.runas(admin)
        {
            acc = Utils_TestMethods.createAccount();
            acc.Country__c=country.id;
            insert acc;  
    
            con = new Contact(FirstName='Test',LastName='Contact1',AccountId=acc.Id,JobTitle__c='Z3',CorrespLang__c='EN',WorkPhone__c='1234567890',ToBeDeleted__c=true,ReasonForDeletion__c='asdfasdf',DuplicateWith__c=null);
            insert con;
        }
        stdUser=Utils_testMethods.createStandardUser('test5');
        stdUser.currencyIsoCode='INR';
        stdUser.ProfileId = System.Label.CLMAR13PRM03;        
        stdUser.contactId = con.Id;
        stdUser.PortalRole = 'Manager';
        insert stdUser;    
        system.runas(stdUser)
        {                   
            Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
            opp2.Reason__c = 'testReason';
            //opp2.partnerInvolvement__c = System.Label.CLMAY13PRM41;
            insert opp2;
            
            OpportunityTeamMember op = Utils_TestMethods.createOppTeamMember(opp2.Id, stdUser.Id);
            insert op;

            Contact vcpContact = new Contact(FirstName='Test',LastName='lastname',AccountId=acc.Id,JobTitle__c='Z3',CorrespLang__c='EN',WorkPhone__c='1234567890',Country__c=country.Id);     
            insert vcpContact;

            List<OPP_ValueChainPlayers__c> lstVCP = new List<OPP_ValueChainPlayers__c>();
            OPP_ValueChainPlayers__c vcp1 = Utils_TestMethods.createValueChainPlayer(acc.Id,con.Id, opp2.Id);
            vcp1.ContactRole__c = System.Label.CLMAY13PRM40;
            lstVCP.add(vcp1);

            OPP_ValueChainPlayers__c vcp2 = Utils_TestMethods.createValueChainPlayer(null,con.Id, opp2.Id);
            vcp2.ContactRole__c= System.Label.CLMAY13PRM40;
            lstVCP.add(vcp2);

            insert lstVCP;
        }
    }
    //To provide coverage of OpportunityBeforeUpdate Trigger 
       static testMethod void testValueChainPlayers2()
    {
        test.starttest();
        Map<String,ID> profiles = new Map<String,ID>();
        List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
        for(Profile p : ps)
        {
            profiles.put(p.name, p.id);
        }
        user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null and BypassVR__c=TRUE limit 1];
        User stdUser,stdUser2;
        Account acc;
        Contact con,con2;
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        system.runas(admin)
        {
            acc = Utils_TestMethods.createAccount();
            acc.Country__c=country.id;
            insert acc;  
            List<Contact> conlst = new List<Contact>();
            con = new Contact(FirstName='Test',LastName='Contact1',AccountId=acc.Id,JobTitle__c='Z3',CorrespLang__c='EN',WorkPhone__c='1234567890',ToBeDeleted__c=true,ReasonForDeletion__c='asdfasdf',DuplicateWith__c=null);
            conlst.add(con);
            con2 = new Contact(FirstName='Test',LastName='Contact2',AccountId=acc.Id,JobTitle__c='Z3',CorrespLang__c='EN',WorkPhone__c='1234567890',ToBeDeleted__c=true,ReasonForDeletion__c='asdfasdf',DuplicateWith__c=null);
            conlst.add(con2);
            insert conlst;
        }
            stdUser=Utils_testMethods.createStandardUser('test5');
            stdUser.currencyIsoCode='INR';
            stdUser.ProfileId = System.Label.CLSEP12PRM18;      
            stdUser.contactId = con.Id;
            stdUser.BypassVR__c=TRUE;
            insert stdUser; 
            stdUser2=Utils_testMethods.createStandardUser('test6');
            stdUser2.currencyIsoCode='INR';
            stdUser2.ProfileId = System.Label.CLSEP12PRM18;     
            stdUser2.contactId = con2.Id;
            stdUser2.BypassVR__c=TRUE;
            insert stdUser2;  
        test.stoptest(); 
        system.runas(stdUser)
        {                   
            Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
            insert opp2;
            
            OpportunityTeamMember op = Utils_TestMethods.createOppTeamMember(opp2.Id, stdUser.Id);
            insert op;

            Contact vcpContact = new Contact(
                  FirstName='Test',
                  LastName='lastname',
                  AccountId=acc.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  WorkPhone__c='1234567890',
                  Country__c=country.Id
                  );   
            insert vcpContact;

            List<OPP_ValueChainPlayers__c> lstVCP = new List<OPP_ValueChainPlayers__c>();
            OPP_ValueChainPlayers__c vcp1 = Utils_TestMethods.createValueChainPlayer(acc.Id,con.Id, opp2.Id);
            vcp1.ContactRole__c = System.Label.CLMAY13PRM40;
            lstVCP.add(vcp1);

            OPP_ValueChainPlayers__c vcp2 = Utils_TestMethods.createValueChainPlayer(null,con.Id, opp2.Id);
            vcp2.ContactRole__c= System.Label.CLMAY13PRM40;
            lstVCP.add(vcp2);

            insert lstVCP;
            try{
                opp2.OwnerID=stdUser2.Id;
                opp2.Reason__c = 'testReason';
                opp2.partnerInvolvement__c = System.Label.CLMAY13PRM41;
                opp2.IncludedInForecast__c=Label.CL00584;
                update opp2;
            }
            Catch(Exception ex){}
        }
    }
}