/*
+-----------------------+---------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                               |
+-----------------------+---------------------------------------------------------------------------------------+
| Description           |                                                                                       |
|                       |                                                                                       |
|     - Component       | VFC10200_Accept                                                                       |
|                       |                                                                                       |
|     - Object(s)       | Case                                                                                  |
|     - Description     |   - Change the Case Owner to the Active User from Detail View                         |
|                       |     > Default Controller: VFC10200_Accept(ApexPages.StandardController controller)    |
|                       |     > Method:             acceptCurrentCase()                                         |
|                       |                                                                                       |
|                       |   - Change the Case Owner to the Active User from List Views                          |
|                       |     > Default Controller: VFC10200_Accept(ApexPages.StandardSetController controller) |
|                       |     > Method:             acceptCases()                                               |
+-----------------------+---------------------------------------------------------------------------------------+
| Delivery Date         | February, 24th 2012                                                                   |
+-----------------------+---------------------------------------------------------------------------------------+
| Modifications         | BR-1825 (April 2014 Release) Modified Case Status to In Progress while Saving the     |
                        |   Case.  Modified By Vimal K(GD India)                                                                             |
+-----------------------+---------------------------------------------------------------------------------------+
| Governor informations |                                                                                       |
+-----------------------+---------------------------------------------------------------------------------------+
*/
public class VFC10200_Accept
{
   //Variables
   public String     retUrl;
   public Id         currentId {get;set;}           //Used if Detail View
   public List<Case> casesList = new List<Case>();  //Used if List   View
   public Boolean    errorFlag {get;set;}

   /////////////////
   // DETAIL VIEW //
   /////////////////

   //Default Controller for Detail Views
   public VFC10200_Accept(ApexPages.StandardController controller)
   {
      System.Debug('## >>> BEGIN: VFC10200_Accept(ApexPages.StandardController controller) <<<');

      this.currentId = ApexPages.currentPage().getParameters().get('Id');
      this.errorFlag = false;

      System.Debug('## >>>>>> this.currentId = ' + this.currentId + ' <<<');

      System.Debug('## >>> END:   VFC10200_Accept(ApexPages.StandardController controller) <<<');
   }

   //Change Owner to Active User for Detail Views
   public PageReference acceptCurrentCase()
   {
      System.Debug('## >>> BEGIN: VFC10200_Accept.acceptCurrentCase() <<<');

      PageReference lRetUrl       = null;
      Id            currentUserId = UserInfo.getUserId();

      //Querying Case record based on current Case Id
      List<Case> lcaseList = [SELECT
                                 Id,
                                 CaseNumber,
                                 OwnerId,
                                 status //BR-1825
                              FROM Case
                              WHERE Id = :this.currentId];
      System.Debug('## >>>>>> lcaseList before updating = ' + lcaseList + ' <<<');

      if(lcaseList.size() > 0)
      {
         //Affecting current user to current Case
         lcaseList.get(0).OwnerId = currentUserId;
         lcaseList.get(0).status = System.Label.CL00341; //BR-1825
         System.Debug('## >>>>>> lcaseList after updating = ' + lcaseList + ' <<<');

         try
         {
            Database.update(lcaseList.get(0));
            lRetUrl = redirectToCurrentCase();
         }
         catch(System.DmlException Ex)
         {
            this.errorFlag = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, Ex.getDmlMessage(0)));
         }
      }
      else
      {
         this.errorFlag = true;
         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, system.label.CL10200));
      }

      System.Debug('## >>> END  : VFC10200_Accept.acceptCurrentCase() <<<');
      return lRetUrl;
   }

   //Redirect to current Case
   public PageReference redirectToCurrentCase()
   {
      System.Debug('## >>> BEGIN: VFC10200_Accept.redirectToCurrentCase() <<<');
      
      PageReference l_RetUrl = new PageReference('/' + this.currentId);

      System.Debug('## >>> END  : VFC10200_Accept.redirectToCurrentCase() <<<');
      return l_RetUrl;
   }


   /////////////////
   // LIST VIEWS  //
   /////////////////

   //Default Controller for List Views
   public VFC10200_Accept(ApexPages.StandardSetController controller)
   {
      System.Debug('## >>> BEGIN: VFC10200_Accept(ApexPages.StandardSetController controller) <<<');

      this.retUrl    = ApexPages.currentPage().getParameters().get(system.label.CL00330);
      this.casesList = controller.getSelected();
      this.errorFlag = false;

      System.Debug('## >>>>>> this.retUrl    = ' + this.retUrl + ' <<<');
      System.Debug('## >>>>>> this.casesList = ' + this.casesList + ' <<<');

      System.Debug('## >>> END:   VFC10200_Accept(ApexPages.StandardSetController controller) <<<');
   }

   //Change Owner to Active User for List Views
   public PageReference acceptCases()
   {
      System.Debug('## >>> BEGIN: VFC10200_Accept.acceptCases() <<<');

      PageReference lRetUrl       = null;
      Set<ID>       caseIds       = new Set<Id>();
      Id            currentUserId = UserInfo.getUserId();
      
      if(this.casesList.size() > 0)
      {
         //Adding Ids of all the selected cases to a set
         for(Case aCase : this.casesList)
         {
            caseIds.add(aCase.Id);
         }
         System.Debug('## >>>>>> CaseIds = ' + caseIds + ' <<<');

         //Querying Case records based on Case Ids
         List<Case> lcaseList = [SELECT
                                    Id,
                                    CaseNumber,
                                    OwnerId,
                                    status //BR-1825
                                 FROM Case
                                 WHERE Id IN :caseIds];
         System.Debug('## >>>>>> lcaseList before updating = ' + lcaseList + ' <<<');

         //Affecting current user to Cases
         for(Case aCase : lcaseList)
         {
            aCase.OwnerId = currentUserId;
            aCase.status  = System.Label.CL00341;
         }
         System.Debug('## >>>>>> lcaseList after updating = ' + lcaseList + ' <<<');

         //Updating records into the database if DML limits are not reached
         if(lcaseList.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
         {
            System.Debug('######## VFC10200_Accept Error Update: '+ system.label.CL00264);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00264));
         }
         else
         {
            Database.SaveResult[] VFC10200_SaveResults = Database.update(lcaseList, false);

            //Checking for unsuccessful updates
            for(Database.SaveResult sr : VFC10200_SaveResults)
            {
               if(!sr.isSuccess())
               {
                  Database.Error err = sr.getErrors()[0];
               }
            }
         }

         lRetUrl = redirectToListView();
      }
      else
      {
         this.errorFlag = true;
         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, system.label.CL10200));
      }

      System.Debug('## >>> END  : VFC10200_Accept.acceptCases() <<<');
      return lRetUrl;
   }

   //Redirect to List View
   public PageReference redirectToListView()
   {
      System.Debug('## >>> BEGIN: VFC10200_Accept.redirectToListView() <<<');
      
      PageReference l_RetUrl = null;

      if(retUrl != null)
      {
         l_RetUrl = new PageReference(retUrl);
      }

      System.Debug('## >>> END  : VFC10200_Accept.redirectToListView() <<<');
      return l_RetUrl;
   }
}