@isTest
private class AP_ChatterForCCCAction_Test{
    public static testMethod void testCreateChatterForCCCActions(){
        System.debug('#### AP_ChatterForCCCAction_Test.testCreateChatterForCCCActions Started ####');
        
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCase;
        
        Country__c country=Utils_TestMethods.createCountry();
        insert country;
        
        CustomerCareTeam__c ccteam=Utils_TestMethods.CustomerCareTeam(country.id);
        ccteam.teammailbox__c='testmailbox@test.com';
        insert ccteam;        
        
        List<CCCAction__c> lstAction = new List<CCCAction__c>();
        CCCAction__c objAction1 = Utils_TestMethods.createCCCAction(objCase.Id);
        CCCAction__c objAction2 = Utils_TestMethods.createCCCAction(objCase.Id);
       
        objAction1.TECH_DueDateReached__c =TRUE;
        objAction1.CCCTeam__c=ccteam.id;
        lstAction.add(objAction1);
        lstAction.add(objAction2);
        system.debug('-->>'+lstAction);
        
        Database.SaveResult[] dbInsertResult = Database.Insert(lstAction, false);
         
        objAction2.TECH_DueDateReached__c =TRUE;
        update objAction2;
        system.debug('-->>'+objAction2);
        
        System.debug('#### AP_ChatterForCCCAction_Test.testCreateChatterForCCCActions Ends ####');
    }
}