@isTest
    private class VFC_SendToServices_TEST {
        private static Pagereference pageReference; 
        private static ApexPages.StandardController standardController;  
        private static VFC_SendToServices sendToServices;
        
        static testMethod void sendToServicesTestMethod(){ 
            DateTime dT = System.now();
            Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
            Account account = Utils_TestMethods.createAccount();
            Database.insert(account);
            
            Account account2 = Utils_TestMethods.createAccount();
            Database.insert(account2);
            
            SVMXC__Site__c site = new SVMXC__Site__c();
            site.Name = 'Test Location';
            site.SVMXC__Street__c  = 'Test Street';
            site.SVMXC__Account__c = account.id;
            site.TimeZone__c = 'Test';
            site.PrimaryLocation__c = true;
           // insert site;
           Database.insert(site);
            
            
            Contact contact = Utils_TestMethods.createContact(account.id, 'Doe');
            Database.insert(contact);
            Case myCase = Utils_TestMethods.createCase(account.id, contact.id, 'Open');
            myCase.CheckedbyGMR__c = true;
            Database.insert(myCase);
            Country__c country = Utils_TestMethods.createCountry();
            Database.insert(country);
            CustomerLocation__c customerLocation = Utils_TestMethods.createCustomerLocation(account.id, country.id);
            Database.insert(customerLocation);
            list<WorkOrderNotification__c> wonlist= new list<WorkOrderNotification__c>();
            WorkOrderNotification__c won = Utils_TestMethods.createWorkOrderNotification(myCase.id, customerLocation.id);    
            won.OnSiteContactSameasCaseContact__c = 'Yes';
            won.Priority__c=null;
            won.Location__c= null;
            won.InstalledAtAccount__c= account.Id;
            won.NonBillableReason__c ='Warranty';
            won.WarrantyReference__c = 'text';
            
            
            Database.insert(won);
            
            
            WONInstalledProductLine__c wonip = new WONInstalledProductLine__c();
            //wonip.Name='text';
            wonip.WorkOrderNotification__c = won.Id;
            insert wonip;
            
            
            pageReference = new pagereference('/apex/VFP_SendToServices?retURL=%2F500%3Ffcf%3D00BA0000006JE16'); 
            Test.setCurrentPageReference(pageReference);         
            standardController = new ApexPages.StandardController(won);  
            sendToServices = new VFC_SendToServices(standardController);
            // Test Warning message
            sendToServices.SendToServices(); 
            
            
            won.ServicesBusinessUnit__c = 'EN';
            won.WorkOrderCategory__c = 'On-site';
            won.WorkOrderType__c = 'Maintenance';
            won.CustomerRequestedDate__c = myDate;
            won.CustomerRequestedTime__c = '12:00 AM';
            won.TimeZone__c = 'Europe/Paris';
            won.ServiceRequestReason__c = 'Reason';
            won.Priority__c = 'Normal';
            won.NonBillableReason__c ='Contract';
            won.InstalledAtAccount__c= account2.Id;
            won.ServiceNotificationNumber__c='text11';
            update won;
            //Database.update(won,false);
         // Test Conversion
            sendToServices.SendToServices();
        }
        static testMethod void sendToITBServicesTestMethod(){
            User testUser = Utils_TestMethods.createStandardUserWithNoCountry('ITBTEST');
            testUser.BypassVR__c=true;
            testUser.BypassTriggers__c='AP_Case_CaseHandlerCaseBeforeInsert;AP_Case_CaseHandlerCaseBeforeUpdate;AP20;';
            System.runAs(testUser) {
                DateTime dT = System.now();
                Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
                Account account = Utils_TestMethods.createAccount();
                Database.insert(account);
                Contact contact = Utils_TestMethods.createContact(account.id, 'Doe');
                Database.insert(contact);
                Case myCase = Utils_TestMethods.createCase(account.id, contact.id, 'Open');
                myCase.CheckedbyGMR__c = true;
                Test.startTest();
                Database.insert(myCase);
                
                //ITB Assets for ITB WON
                ITB_Asset__c objAsset  = Utils_TestMethods.createITBAsset(myCase.id);
                objAsset.Address__c  = 'TestAddress';
                objAsset.AssetComments__c  = 'TestComments';
                objAsset.City__c  = 'TestCity';
                objAsset.CommercialReference__c  = 'BK500';
                objAsset.LegacyAccountID__c  = '1-242343';
                objAsset.LegacyAccountName__c  = 'TestAccount';
                objAsset.LegacyAssetId__c  = '1-22222';
                objAsset.LegacyAssetNumber__c  = '1-22222';
                objAsset.LegacyName__c  = 'IT_SB';
                objAsset.PrimaryFlag__c  = TRUE;
                objAsset.SerialNumber__c  = 'SFSEDFSDFDFDS';
                objAsset.StateProvince__c  = 'TESTSTATE';
                objAsset.TECH_AssetRefrenceLinkID__c  = '34545345345';
                objAsset.ZipCode__c ='342342';
                Database.insert(objAsset);
                

                WorkOrderNotification__c ITBwon = Utils_TestMethods.createWorkOrderNotification(myCase.id, null);
                ITBwon.OnSiteContactSameasCaseContact__c = 'Yes';
                ITBwon.ServicesBusinessUnit__c = 'IT';
                ITBwon.WorkOrderCategory__c = 'On-site';
                ITBwon.WorkOrderType__c = 'Maintenance';
                ITBwon.CustomerRequestedDate__c = myDate;
                ITBwon.CustomerRequestedTime__c = '12:00 AM';
                ITBwon.TimeZone__c = 'Europe/Paris';
                ITBwon.ServiceRequestReason__c = 'Reason'; 
                Database.insert(ITBwon);
                
                standardController = new ApexPages.StandardController(ITBwon);  
                sendToServices = new VFC_SendToServices(standardController);
                sendToServices.CallServices();
                Test.stopTest(); 
            }
        }
    }