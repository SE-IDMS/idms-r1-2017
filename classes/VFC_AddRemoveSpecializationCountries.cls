/*
22-Jul-2013    Srinivas/Shruti    PRM Oct13 Release    Fun: Specialization 
*/
public with sharing class VFC_AddRemoveSpecializationCountries {
    
    Public Boolean hasError {get;set;} {hasError= false;}
    
    public SelectOption[] selCountries { get; set; }
    public SelectOption[] allCountries { get; set; }
    
    public Specialization__c spec {get;set;}
    public List<SpecializationCountry__c> specCountries = new List<SpecializationCountry__c>();//Used to create/update Specialization countris. used in DML
    
    public VFC_AddRemoveSpecializationCountries(ApexPages.StandardController controller) {
        
        selCountries = new List<SelectOption>();  
        allCountries = new List<SelectOption>();
        spec = (Specialization__c)controller.getRecord();
        
        List<Country__c> countries = new list<Country__c>();
        spec  = [SELECT OwnerID, RecordType.DeveloperName, GlobalSpecialization__r.TECH_CountriesId__c, TargetCountriesTxt__c, TECH_CountriesId__c, ProgramApprover__c ,ProgramAdministrator__c  FROM Specialization__c WHERE id=:spec.id];
        
        if(spec != null && spec.RecordType.DeveloperName == Label.CLOCT13PRM51 )// if Cluster Specilization
        {
            list<String> lstCountryIds = new list<String>();
            if(spec.GlobalSpecialization__r.TECH_CountriesId__c !=null)
            {
                for (String s : spec.GlobalSpecialization__r.TECH_CountriesId__c.split(',')) 
                {
                    lstCountryIds.add(s);
                }
            }
            if(!lstCountryIds.isEmpty())
                countries = [select Name, Id from Country__c where id in :lstCountryIds order by name ASC];
        }
        else{
            countries = [select Name, Id from Country__c order by name ASC];
        }
      
        //create selected/available select options
        Map<Id, String> countriesIdName = new Map<Id, String>();
        for(Country__c c: countries)
        {
            countriesIdName.put(c.Id, c.Name);
        }
        Set<String> selOpts1 = new Set<String>();
        if(spec.TECH_CountriesId__c !=null)
        {
            for (String s : spec.TECH_CountriesId__c.split(',')) 
            {
                selCountries.add(new SelectOption(s, countriesIdName.get(s)));
                selOpts1.add(s);
            }  
        }
        ////
       for(Country__c c : countries) 
        {
            if(!(selOpts1.contains(c.Id)))
                allCountries.add(new SelectOption(c.Id, c.Name));
        }
        ///Authorisation to add countries to Specialization
        Boolean isAdminUser = false;
        String profileName=[select Name from Profile where Id=:UserInfo.getProfileId()].Name;
        List<String> profilesToSKIP = (System.Label.CLMAY13PRM33).split(',');//the valid profile prefix list is populated
        for(String pName : profilesToSKIP)//The profile name should start with the prefix else the displayerrormessage flag is set to true
        {
            if(pName.trim() == profileName.trim())
            {
                isAdminUser = true;             
            }  
        }

        if(isAdminUser == false && UserInfo.getUserId() != spec.OwnerId && UserInfo.getUserId() != spec.ProgramApprover__c  && UserInfo.getUserId() != spec.ProgramAdministrator__c)
        {
            hasError = true;    
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You are not authorised to add countries to Specialization'));
            //return null; 
        }   
        
    }//End of Constructor
    
    public PageReference updateCountries()
    {
        spec.TargetCountriesTxt__c = null;
        spec.TECH_CountriesId__c= null;
        for(SelectOption so : selCountries) 
        {
            if(spec.TargetCountriesTxt__c ==null)
                spec.TargetCountriesTxt__c = so.getLabel();
            else
                spec.TargetCountriesTxt__c = spec.TargetCountriesTxt__c + ',' +so.getLabel();            
            if(spec.TECH_CountriesId__c==null)
                spec.TECH_CountriesId__c= so.getValue();
            else
                spec.TECH_CountriesId__c= spec.TECH_CountriesId__c+ ',' +so.getValue();            
        }
        
        Database.update(spec);
        List<SpecializationCountry__c> specializationCountries = new List<SpecializationCountry__c>();
        specializationCountries = [SELECT Country__c, Specialization__C FROM SpecializationCountry__c WHERE Specialization__C = :spec.id ];
        
        delete specializationCountries ;
        if(spec.TECH_COuntriesId__c!=null)
        {
            for(String s: spec.TECH_COuntriesId__c.split(','))
            {
                SpecializationCountry__c sc = new SpecializationCountry__c();
                sc.Country__c = s;
                sc.Specialization__c = spec.id;
                specCountries.add(sc);
            }
            insert specCountries;
        }
        
        return new PageReference('/'+spec.id);
    }//End of method updateCountries()

}