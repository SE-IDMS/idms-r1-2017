/*
    Author          : Priyanka Shetty (Schneider Electric)
    Date Created    : 14/14/2013
    Description     : Controller class for the page CHR_FundingEntity                      
                      
*/

public with sharing class CHR_ManageFundingEntity
{
    public ApexPages.StandardController controller;
    
    public ChangeRequestFundingEntity__c oFE {get;set;}
    
    public List<Integer> yearsInt {get;set;}
    
    public Map<Integer,string> yearsMap {get;set;}
    
    public Map<string,string> alphaTypes {get;set;}
    
    public Map<Integer,List<string>> keysforyear {get;set;}
    
    public String mapkey{get;set;}{mapkey='';}
    
    public List<string> lstTypeOrder {get;set;}
    
    public Set<string> setTypeOrder {get;set;}
    
    Map<string,DMTFundingEntityCostItemsFields__c> mapCS {get;set;}
     
    public List<string> lstType {get;set;}
    
    Boolean boolActive = false;
    public Map<string,string> keyMap {get;set;}
    {
        keyMap = new Map<string,string>();
    }
    
    public Map<string,string> fieldnameMap {get;set;}
    {
        fieldnameMap = new Map<string,string>();
        system.debug(DMTFundingEntityCostItemsFields__c.getall().values());
        Map<String,DMTFundingEntityCostItemsFields__c> csMap = DMTFundingEntityCostItemsFields__c.getAll();
        System.Debug('csMap size: ' + csMap.keySet().size());
        for(String s:csMap.keySet())
        {
            string strValue = csMap.get(s).CostItemFieldAPIName__c;
            System.Debug('strValue: ' + strValue);
            fieldnameMap.put(s,strValue);
            mapkey += s + ',';
            keyMap.put(strValue,s);
            
        }
        System.Debug('Mapkey: ' + mapkey);
    }
    
    public string startYear {get;set;}
    
    public CHR_ManageFundingEntity(ApexPages.StandardController controller)
    {
        this.controller = controller;
        if(!Test.isRunningTest())
        {
            controller.addFields(fieldnameMap.values());
        }                
        oFE = (ChangeRequestFundingEntity__c) controller.getRecord();
        mainInit();
 
    }
    
    private void mainInit()
    {
        yearsInt = new List<Integer>();
        yearsMap = new Map<Integer,string>();
        alphaTypes = new Map<string,string>();
        keysforyear = new Map<Integer,List<string>>();
        lstTypeOrder = new List<string>();
        setTypeOrder = new Set<string>();
        mapCS = DMTFundingEntityCostItemsFields__c.getAll();
        lstType = new List<string>();
        
        for(DMTFundingEntityCostItemsFields__c cs1:mapCS.values())
        {
            if(!alphaTypes.containsKey(cs1.Type__c))
                alphaTypes.put(cs1.Type__c,cs1.Type__c);        
            if(!setTypeOrder.contains(cs1.TypeOrder__c))
                setTypeOrder.add(cs1.TypeOrder__c);
        }
        
        for(string to:setTypeOrder)
        {
            lstTypeOrder.add(to);
        }
        
        lstTypeOrder.sort();
        
        
        for(integer i=0;i<lstTypeOrder.size();i++)
        {
            for(DMTFundingEntityCostItemsFields__c cs1:mapCS.values())
            {
                if(cs1.TypeOrder__c == lstTypeOrder.get(i))
                {
                    /*
                    string strType = cs1.Type__c;
                    boolean isDuplicate = false;
                    for(integer j=0;j<lstType.size();j++)
                    {
                        if(lstType.get(j) == strType)
                        {
                            isDuplicate = true;
                            break;
                        }
                    }
                    if(!isDuplicate)
                        lstType.add(cs1.Type__c);
                    break;
                    */
                    lstType.add(cs1.Type__c);
                    break;
                }
            }
        }               

       oFE = [select Name,ChangeRequest__c,BudgetYear__c,CAPEXY0__c,CAPEXY1__c,CAPEXY2__c,CAPEXY3__c,OPEXOTCY0__c,OPEXOTCY1__c,OPEXOTCY2__c,OPEXOTCY3__c,SmallEnhancementsY0__c,SmallEnhancementsY1__c,SmallEnhancementsY2__c,SmallEnhancementsY3__c,RunningCostsImpactY0__c,RunningCostsImpactY1__c,RunningCostsImpactY2__c,RunningCostsImpactY3__c,PLFundEstimatedY0__c,PLFundEstimatedY1__c,PLFundEstimatedY2__c,PLFundEstimatedY3__c,TotalCAPEX__c,TotalOPEXOTC__c,TotalSmallEnhancements__c,TotalRunningCostsImpact__c,TotalFY0__c,TotalFY1__c,TotalFY2__c,TotalFY3__c,TotalCostFY0FY1FY2__c from ChangeRequestFundingEntity__c where Id = :oFE.Id];
        
        startYear = oFE.BudgetYear__c;
        
        generateKeysBasedOnStartYear();
             
    }
    
    
    private void generateKeysBasedOnStartYear()
    {
        List<String> concat = new List<String>();
        Integer iYear = Integer.valueOf(startYear.trim());
        Integer calcYear = 0;
        
        //for (integer i = 0;i<PRJ_Config__c.getInstance().NumberOfYearsForChangeRequestCostsForecast__c;i++) 
        for (integer i = 0;i<4;i++) 
        {
            concat = new List<String>();
            calcYear = iYear + i;         
            yearsInt.add(calcYear);
            yearsMap.put(calcYear,'Y' + i);
            for (String subkey : alphaTypes.values())
            {
                string strVal = yearsMap.get(calcYear);
                concat.add(subkey+strVal);                              
            }
            keysforyear.put(calcYear, concat);
        }        
    }
    
    public pagereference updateFundingEntity()
    {        
        try
        {
            update oFE;
            
            Map<string, list<ChangeRequestFundingEntity__c>> EntityFelistMap = new Map<string, list<ChangeRequestFundingEntity__c>>();
            ChangeRequestFundingEntity__c  fe=[select id ,CurrencyISOCode,ChangeRequest__c,BudgetYear__c  from ChangeRequestFundingEntity__c   where id =:oFE.id];
           //list<ChangeRequestCostsForecast__c>  oBudget=[select id,CurrencyISOCode,TECHTotalPlannedITCosts__c,TotalCashoutCAPEX__c,TotalCashoutOPEX__c,TotalInternalCAPEX__c,TotalInternalOPEX__c,TotalSmallEnhancements__c, NewTotalCashoutCAPEX__c, NewTotalCashoutOPEX__c, NewTotalInternalCAPEX__c, NewTotalInternalOPEX__c, NewTotalSmallEnhancements__c,PastBudgetCashoutCAPEXCumulative__c,PastBudgetCashoutOPEXCumulative__c,PastBudgetInternalCAPEXCumulative__c,PastBudgetInternalOPEXCumulative__c,PastBudgetSmallEnhancementsCumulativ__c,PastBudgetGDCostsCumulative__c, Active__c from ChangeRequestCostsForecast__c where ChangeRequest__c = :fe.ChangeRequest__c and Active__c = true];
            list<ChangeRequestCostsForecast__c>  oBudget=[select id,CurrencyISOCode,TECHTotalPlannedITCosts__c,TotalCashoutCAPEX__c,TotalCashoutOPEX__c,TotalSmallEnhancements__c, NewTotalCashoutCAPEX__c, NewTotalCashoutOPEX__c, NewTotalSmallEnhancements__c, Active__c from ChangeRequestCostsForecast__c where ChangeRequest__c = :fe.ChangeRequest__c and Active__c = true];
            if(oBudget!=null && oBudget.size()>0){
                list< ChangeRequestFundingEntity__c> ferec =[select ParentFamily__c, Parent__c, GeographicZoneSE__c, GeographicalSEOrganization__c,CurrencyISOCode, TotalCostFY0FY1FY2__c , BudgetYear__c, id, ActiveForecast__c, SelectedinEnvelop__c, ChangeRequest__c,  ForecastYear__c, ForecastQuarter__c from  ChangeRequestFundingEntity__c where ChangeRequest__c =:oFE.ChangeRequest__c and BudgetYear__c=:oFE.BudgetYear__c and ActiveForecast__c = true];
                List<ChangeRequestFundingEntity__c> fetomakeTotal= new List<ChangeRequestFundingEntity__c>();
                Map<String , integer> intQuaterMap = new Map<String,integer>();
                intQuaterMap.put('Q1',1);
                intQuaterMap.put('Q2',2);
                intQuaterMap.put('Q3',3);
                intQuaterMap.put('Q4',4);
                Map<String , ChangeRequestFundingEntity__c> quaterFEMap = new Map<String,ChangeRequestFundingEntity__c>();
                for(ChangeRequestFundingEntity__c dmtfe : ferec){
                    
    
                    String mapKey='';
                    mapkey=dmtfe.ParentFamily__c+dmtfe.Parent__c+dmtfe.GeographicZoneSE__c+dmtfe.GeographicalSEOrganization__c;
                    if(!EntityFelistMap.containsKey(mapkey)){
                        
    
                        EntityFelistMap.put(mapkey,new List<ChangeRequestFundingEntity__c>());
                        EntityFelistMap.get(mapKey).add(dmtfe);
                        
                    }
                    else{
                        EntityFelistMap.get(mapKey).add(dmtfe);
                    }
                    
                }
                if(EntityFelistMap!= null && EntityFelistMap.size()>0){
                    
                    for(String ks: EntityFelistMap.keySet()){
                        
                        List<ChangeRequestFundingEntity__c> felist=new List<ChangeRequestFundingEntity__c>();
                        felist = EntityFelistMap.get(ks);
                        if(felist.size()>1){
                            ChangeRequestFundingEntity__c temp=new ChangeRequestFundingEntity__c();
                            
                          
                            for(Integer i=0;i<felist.size();i++){
                                
                                  if(i== 0){
                                    temp =felist[0];
                                  }
                                  else{
                                    Integer tFryr=Integer.valueOf(temp.ForecastYear__c);
                                    Integer nFryr=Integer.valueOf(felist[i].ForecastYear__c);
                                    if(tFryr == nFryr){
                                        if(intQuaterMap.get(temp.ForecastQuarter__c )> intQuaterMap.get(felist[i].ForecastQuarter__c)){
                                        
                                        }
                                        else{
                                            temp =felist[i];
                                        }
                                    }
                                    else if(nFryr > tFryr){
                                        temp= felist[i];
                                    }
                                    
                                  }
                                
                                
                                
                            }
                            fetomakeTotal.add(temp);
                            
                        }
                        else if(felist.size() == 1){
                            fetomakeTotal.add(felist[0]);
                        }
                        
                    }
                    
                }
                system.debug('***********Funding Entity size***********:  '+fetomakeTotal.size());
                Decimal totalCost=0;
                List<DatedConversionRate> dtCRList = new List<DatedConversionRate>();
                Map<String, Decimal> isoCodeConvMap = new Map<String, Decimal>();
                Set<String> isoCodeSet =new Set<String>();
                if(fetomakeTotal!= null && fetomakeTotal.size()>0){
                    for(ChangeRequestFundingEntity__c fund: fetomakeTotal){
                        isoCodeSet.add(fund.CurrencyISOCode);
                        system.debug('***********Funding Entity IsoCode List***********:  '+isoCodeSet);
    
                    }
                }
                
                if(oBudget[0]!= null){
                    isoCodeSet.add(oBudget[0].CurrencyISOCode);
                    system.debug('***********Budget IsoCode***********:  '+isoCodeSet);
                }
                
                if(isoCodeSet!= null && isoCodeSet.size()>0){
                    dtCRList= [SELECT ConversionRate,IsoCode FROM DatedConversionRate where  IsoCode in :isoCodeSet and startDate<=:System.Today().toStartOfMonth() and NextStartDate>=:System.Today()];
                    system.debug('***********Date Convertion List***********:  '+dtCRList.size());
                }
                
                if(dtCRList!= null && dtCRList.size()>0){
                    for(DatedConversionRate dcr : dtCRList){
                        isoCodeConvMap.put(dcr.IsoCode,dcr.ConversionRate);
    
    
                    }
    
                }
                
                if(fetomakeTotal!= null && fetomakeTotal.size()>0){
                    for(ChangeRequestFundingEntity__c fundEnt: fetomakeTotal){
                        System.debug('******* '+isoCodeConvMap.get(oBudget[0].CurrencyISOCode));
                         System.debug('******* '+isoCodeConvMap.get(fundEnt.CurrencyISOCode));
                        totalCost += ((fundEnt.TotalCostFY0FY1FY2__c)*isoCodeConvMap.get(oBudget[0].CurrencyISOCode))/isoCodeConvMap.get(fundEnt.CurrencyISOCode);
                        system.debug('***********totalCost***********:  '+totalCost);
                    }
                }
                
                Decimal tpc;
                totalCost = totalCost.setScale(0);
                System.debug('asdf'+oBudget[0].TECHTotalPlannedITCosts__c);
                if(oBudget[0].TECHTotalPlannedITCosts__c!= null){
                    tpc=oBudget[0].TECHTotalPlannedITCosts__c.setScale(0);
                }
                System.debug('!!!!!!!!!'+totalCost );
                string st;
                Decimal TotalDC;
                Decimal tdc;
                integer td;
                if(totalCost !=tpc ){
                    system.debug('***********totalCost***********:  '+totalCost      +oBudget[0].TECHTotalPlannedITCosts__c);
                    TotalDC =totalCost - tpc;
                    tdc = (TotalDC)*isoCodeConvMap.get(fe.CurrencyISOCode)/isoCodeConvMap.get(oBudget[0].CurrencyISOCode);
                    td = math.abs(integer.valueof(tdc.setScale(0)));
                    st = string.valueOf(td);
                    //tdc = math.abs(integer.valueof(totalCost - tpc));
                    //st = string.valueOf(tdc);
                    ApexPages.addMessage(new ApexPages.Message ( ApexPages.Severity.WARNING,Label.DMT_MessageFundingNotEqualToCostsForecast + '  '+fe.CurrencyISOCode + ' '+st));
                    //ApexPages.addMessage(new ApexPages.Message ( ApexPages.Severity.WARNING,Label.DMT_MessageFundingNotEqualToCostsForecast ));
                    
                }   
                /*if(totalCost !=tpc ){
                    system.debug('***********totalCost***********:  '+totalCost      +oBudget[0].TECHTotalPlannedITCosts__c);
                    tdc = math.abs(integer.valueof(totalCost - tpc));
                    st = string.valueOf(tdc);
                    //ApexPages.addMessage(new ApexPages.Message ( ApexPages.Severity.WARNING,Label.DMT_MessageFundingNotEqualToCostsForecast + '  '+fe.CurrencyISOCode + ' '+st));
                    ApexPages.addMessage(new ApexPages.Message ( ApexPages.Severity.WARNING,Label.DMT_MessageFundingNotEqualToCostsForecast));
                }*/   
                
            }
            



          mainInit();
        }        
        catch(DmlException dmlexp)
        {
            for(integer i = 0;i<dmlexp.getNumDml();i++)
                ApexPages.addMessages(new noMessageException(dmlexp.getDmlMessage(i)));
                      
        }                
        return null;
    }
       
    public Pagereference backToFundingEntity()
    {
        PageReference pageRef = new PageReference('/'+ oFE.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
     public class noMessageException extends Exception{}
    
}