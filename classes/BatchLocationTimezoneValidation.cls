global class BatchLocationTimezoneValidation implements Database.Batchable<sObject>, Schedulable, Database.AllowsCallouts {
  
    public Integer rescheduleInterval = Integer.ValueOf(Label.CLAPR14SRV13);
    
    public Integer batchSize = 100;
    // and TECH_isAddressSynchedWithAccount__c = true
    // public String query = 'SELECT Id, Name, SVMXC__Longitude__c, SVMXC__Latitude__c, TimeZone__c, SVMXC__Account__r.id, LocationCountry__r.CountryCode__c, ValidTimeZone__c FROM SVMXC__Site__c WHERE PrimaryLocation__c = true and ValidTimeZone__c = false and SVMXC__Latitude__c != null and SVMXC__Longitude__c != null limit '+Label.CLAPR14SRV16;
    // DEF-8936 
    public String query = 'SELECT Id, Name, SVMXC__Longitude__c, SVMXC__Latitude__c, TimeZone__c, SVMXC__Account__r.id, LocationCountry__r.CountryCode__c, ValidTimeZone__c FROM SVMXC__Site__c WHERE ValidTimeZone__c = false and SVMXC__Latitude__c != null and SVMXC__Longitude__c != null limit '+Label.CLAPR14SRV16;
    
    public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};
    
    global boolean debug = false;
    
    public String scheduledJobName = 'BatchLocationTimezoneValidation';
    
    global Database.Querylocator start(Database.BatchableContext BC){
        System.debug('### Starting BatchLocationTimezoneValidation Batch');
        System.debug('### rescheduleInterval: '+rescheduleInterval);
        return Database.getQueryLocator(query);    
    }

        global void execute(Database.BatchableContext BC,List <sObject> scope)
    {
        System.debug('### Executing BatchLocationTimezoneValidation Batch');
        System.debug('### Scope size: '+scope.size());
        System.debug('### Scope: '+scope);
        LocationHandler.calculateTimeZone((List<SVMXC__Site__c>) scope);
    }
    
    global void finish(Database.BatchableContext BC) {
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        System.debug('### scheduledJobDetailList: '+scheduledJobDetailList);
                     
        if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) {
            // There is at least one scheduled job already for this name
            // Looking if one of scheduled job with the correct name is in an 'Active' state
            List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
            System.debug('### scheduledJobList: '+scheduledJobList);
            if (scheduledJobList != null && scheduledJobList.size()>0) {
                canReschedule = False;
            }
        }
        if (canReschedule && !Test.isRunningTest() ) {
            System.scheduleBatch(new BatchLocationTimezoneValidation(), scheduledJobName, rescheduleInterval, batchSize);
        }
    }
    
    global void execute(SchedulableContext sc) {
        BatchLocationTimezoneValidation calculateLocationTimezone = new BatchLocationTimezoneValidation ();
        Database.executeBatch(calculateLocationTimezone, batchSize );
    }    
}