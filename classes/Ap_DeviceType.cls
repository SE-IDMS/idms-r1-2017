public class Ap_DeviceType extends TriggerBase{

    
    private map<Id, DeviceType__c> newMap;
    private map<Id, DeviceType__c> oldMap;
    private List<DeviceType__c> newRecList;
    Set<String> ExistGoldenIDSet = new Set<String>();
    
    public Ap_DeviceType(boolean isBefore, boolean isAfter, boolean isUpdate, boolean isInsert, boolean isDelete,
        List<DeviceType__c> newList,
        map<Id, DeviceType__c > oldMap, map<Id, DeviceType__c> newMap) {
        super(isBefore, isAfter, isUpdate, isInsert, isDelete);

        this.newMap = newMap;
        this.oldMap = oldMap;
        this.newRecList = newList;
        
    }
    
    public Ap_DeviceType() {
    }
    public override void onBeforeInsert() {
        
        Set<String> GoldenAssetSet = new Set<String>();        
        
        for(DeviceType__c Obj :newRecList){
            
            if(Obj.SDHDEVICETYPEID__c != null)
            GoldenAssetSet.add(Obj.SDHDEVICETYPEID__c);         
        }
        if(GoldenAssetSet != null && GoldenAssetSet.size()>0)
        {
            for(DeviceType__c obj:[Select id,SDHDEVICETYPEID__c from DeviceType__c where SDHDEVICETYPEID__c in :GoldenAssetSet]){
                ExistGoldenIDSet.add(obj.SDHDEVICETYPEID__c);
            }
        }
        if(Utils_SDF_Methodology.canTrigger('SDHDEVICETYPEIDCHECK')|| Test.isRunningTest() )
        sdhGoldenUniqeCheck();
        
        
        
    }
    public  void sdhGoldenUniqeCheck() {
        for(DeviceType__c Obj :newRecList){
            
            if(Obj.SDHDEVICETYPEID__c != null)
            {
                if(ExistGoldenIDSet.contains(Obj.SDHDEVICETYPEID__c))
                Obj.addError('SDH DeviceType Id  already Exist');
                
            }
        }
        
    }



}