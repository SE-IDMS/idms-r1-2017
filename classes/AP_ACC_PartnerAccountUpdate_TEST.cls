/*
    Srinivas Nallapati    18-Mar-2013    PRM Mar13  Test class for 'AP_ACC_PartnerAccountUpdate'  
*/
@isTest
private class AP_ACC_PartnerAccountUpdate_TEST
{
    static testMethod void testPartnerAcc() 
    {
        test.startTest();
            list<Account> accounts= new list<Account>();

            //list<AccountAssessment__c> accAssemts = new list<AccountAssessment__c>();
            for(integer i= 0;i<200;i++)
            {
                Account ac= Utils_TestMethods.createAccount();
                //AccountAssessment__c accAssmt = new AccountAssessment__c();
                if(math.mod(i,2) == 0)
                    ac.PRMAccount__c = true;
                accounts.add(ac);
                //accAssmt.Account__c = ac.Id;
                //accAssemts.add(accAssmt);
            }
            Database.insert(accounts); 
            //Database.insert(accAssemts); 
            for(Account ac : accounts)
                ac.PRMAccount__c = true;

            Database.update(accounts); 
            
            
        test.stopTest();
    }
}