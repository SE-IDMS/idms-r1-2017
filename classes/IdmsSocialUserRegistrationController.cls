//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This is Social registration controller which allows social users to fill missing fields after authentication. 
//                    This is the controller of IdmsSocialUserRegistration VFP (@home context) 
//
//*******************************************************************************************

public without sharing class IdmsSocialUserRegistrationController{
    // User inputs are stored in these variables.
    public  string language{get;set;}
    public String email {get; set;} 
    public String firstName{get; set;} 
    public String lastname{get; set;}   
    public String phomenum{get; set;}
    public String ucountry{get; set;}
    public Boolean notvalid{get;set;}
    public Boolean checked{get;set;}
    public String gridOut{get; set;}
    public Boolean popUPrender{get;set;}
    public Boolean displayPopup{get;set;}
    public Boolean checkedEmailOpt{get;set;}
    public String reg1Check{get; set;}
    public String regCheckEmailOpt{get; set;}
    public List<SelectOption> statusOptionsCountry{get;set;}
    public List<SelectOption> statusOptionsEmailOptIn{get;set;}
    public Boolean bolTwitterFlag{get; set;}
    public String appName;
    
    Public String appid= System.currentPagereference().getParameters().get('app');
    public String oAuthUrl1;
    public String oAuthUrl2;
    public String oAuthUrl3;
    //variable added for recaptch
    public Boolean enterRecaptcha       = false;
    public Boolean enterRecaptchcorrect = false;
    private static String baseUrl       = Label.CLJUN16IDMS14;
    // The keys you get by signing up for reCAPTCHA for your domain
    private static String privateKey    = Label.CLJUN16IDMS15;
    public String publicKey { 
        get { return Label.CLJUN16IDMS16; }
    }
    
    // Create properties for the non-VF component input fields generated
    // by the reCAPTCHA JavaScript.
    public String challenge { 
        get {
            return ApexPages.currentPage().getParameters().get(Label.CLJUN16IDMS27);
        }
    }
    public String response  { 
        get {
            return ApexPages.currentPage().getParameters().get(Label.CLJUN16IDMS28);
        }
    }
    
    // Whether the submission has passed reCAPTCHA validation or not
    public Boolean verified { get; private set; }
    
     
    //Constructor of the VFP
    public IdmsSocialUserRegistrationController() {
        this.verified   = false;
        bolTwitterFlag  = false;
        email           = '';
        language= ApexPages.currentPage().getParameters().get('language');
        system.debug('~~~~Constructor method 1 ');
        //Pre populate User details only for Social login users 
        String RegSourceFb = [select Id, IDMS_Registration_Source__c from User where Id=:UserInfo.getUserId() limit 1].IDMS_Registration_Source__c;
        if(RegSourceFb == Label.CLJUN16IDMS154 || RegSourceFb == Label.CLQ316IDMS018 || RegSourceFb == Label.CLQ316IDMS020 || RegSourceFb == Label.CLQ316IDMS022)
        {
            if(RegSourceFb != Label.CLQ316IDMS022) email = UserInfo.getUserEmail();
            firstName       = UserInfo.getFirstName();
            lastname        = UserInfo.getLastName();
            system.debug('Constructor method email,FN,LN '+email+'!'+firstName+'!'+lastname);
        }
        if(RegSourceFb == Label.CLQ316IDMS022) {
            bolTwitterFlag = true;
        }
        
        //allow users to update their email id, where in created on the fly in reg handler.
        String curnt_UsrEmail = UserInfo.getUserEmail();
        if(curnt_UsrEmail.contains('@facebook')){
            bolTwitterFlag=true;
        }
        
    }
    /*
    **Method Name -ChangeProfile
    **
    **Purpose :-  Call the rest class method to change the profile.
    ***/
    /**
    public void changeProfile(){
        String remoteURL = 'https://se--devidms.cs61.my.salesforce.com/services/apexrest/ProfileUpdate?_HttpMethod=PATCH';
        String profilename ='SE - IDMS Home';
        Profile objProfile = [SELECT Id FROM Profile WHERE Name=:profilename limit 1];
        HTTPRequest httpRequest = new HTTPRequest();
        String AuthUrl ='Bearer ' +userinfo.getSessionId();
        system.debug('******Get user session Id:'+userinfo.getSessionId());
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setHeader('Authorization', AuthUrl);
        httpRequest.setMethod('POST');
        httpRequest.setBody('{"userId":'+UserInfo.getUserId()+'"profileId":'+objProfile.id+'}');
        httpRequest.setEndpoint(remoteURL);
        HTTPResponse httpResponse = new Http().send(httpRequest);
        system.debug('httpResponse'+httpResponse);
    }    
    **/
    /**
    //callsite method to change current user profile 
    public void callPublicSite(){
     system.debug('*****Inside call public site:');
     String userId = UserInfo.getUserId();
     String remoteURL = 'https://devidms-secommunities.cs61.force.com/identity/SocialUserRegistration?app=IDMSHome&userId='+userId+'&profileId=00e120000019IIb';
     HttpRequest httpRequest = new HttpRequest();
     httpRequest.setMethod('GET');
     httpRequest.setEndpoint(remoteURL);
     HttpResponse httpResponse = new Http().send(httpRequest);
    } 
	**/
    
    //Verify if the captcha provided is correct by invoking Google API
    public PageReference verify() {
        enterRecaptcha = true;
        System.debug('reCAPTCHA verification attempt');
        // On first page load, form is empty, so no request to make yet
        if ( challenge == null || response == null ) { 
            System.debug('reCAPTCHA verification attempt with empty form');
            return null; 
        }
        
        HttpResponse r = makeRequest(baseUrl,
                                     'privatekey=' + privateKey + 
                                     '&remoteip='  + remoteHost + 
                                     '&challenge=' + challenge +
                                     '&response='  + response );
        
        if ( r != null ) {
            this.verified = (r.getBody().startsWithIgnoreCase('true'));
        }
        
        if(this.verified) {
            // If they pass verification Or simply return a PageReference to the "next" page
            enterRecaptchcorrect = true;
            return null;
        }
        else {
            // stay on page to re-try reCAPTCHA
            return null; 
        }
    }
    
    //return to a NULL page
    public PageReference reset() {
        return null; 
    }   
    
    /* Private helper methods */
    @TestVisible
    private static HttpResponse makeRequest(string url, string body)  {
        HttpResponse response   = null;
        HttpRequest req         = new HttpRequest();   
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody (body);
        try {
            Http http           = new Http();
            response            = http.send(req);
            System.debug('reCAPTCHA response: ' + response);
            System.debug('reCAPTCHA body: ' + response.getBody());
        } catch(System.Exception e) {
            System.debug('ERROR: ' + e);
        }
        return response;
    }   
    
    private String remoteHost { 
        get { 
            String ret                  = '127.0.0.1';
            // also could use x-original-remote-host 
            Map<String, String> hdrs    = ApexPages.currentPage().getHeaders();
            if (hdrs.get('x-original-remote-addr') != null)
                ret                     =  hdrs.get('x-original-remote-addr');
            else if (hdrs.get('X-Salesforce-SIP') != null)
                ret                     =  hdrs.get('X-Salesforce-SIP');
            return ret;
        }
    }
    
    //This method is used to load the country picklist and get the terms and condition from custom label
    public void autoRun()
    {
        gridOut                                       = 'color:white;background-color:green;background-image:none;;float:left;';
        regCheckEmailOpt                              = Label.CLJUN16IDMS07;
        Schema.DescribeFieldResult statusFieldCountry = Schema.User.Countrycode.getDescribe();
        statusOptionsCountry                          = new list<SelectOption>();
        //country pick list
        statusOptionsCountry.add(new SelectOption(Label.CLJUN16IDMS06,Label.CLJUN16IDMS05));
        for (Schema.Picklistentry picklistCountry : statusFieldCountry.getPicklistValues())
        {
            if(picklistCountry.getValue() == Label.CLJUN16IDMS06){
                continue;
            }
            else{
                statusOptionsCountry.add(new SelectOption(picklistCountry.getValue(),picklistCountry.getLabel()));
            }
        }
        
        //code for terms condition mapping with app
        String termUrl  = Label.CLJUN16IDMS118;
        String url      = '<a href='+ termUrl + ' target=_blank>'+Label.CLJUN16IDMS29+'</a>';
        gridOut         = 'color:white;background-color:green;background-image:none;;float:left;';
        reg1Check       = Label.CLJUN16IDMS19+' '+ +url+' '+ Label.CLJUN16IDMS20;
    }
    
    //This method is used to validate the fields of registration page which user entered and call the Update user method 
    public PageReference save() {
        //Validation for email to check blank value
        if (email == null || email == '') {
            ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS02);
            ApexPages.addMessage(msgareaOfF);
            return null;
        }
        
        //validation for Email to check correct format
        if(!Pattern.matches(Label.CLJUN16IDMS30, email))
        {
            notvalid    = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS08)); 
            popUPrender = false;
            return null;
        }else{
            notvalid = false;
        }
        
        //validation for firstname and last name 
        if (firstName == null||firstName == ''|| lastName == null || lastName == '') {
            ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS12);
            ApexPages.addMessage(msgareaOfF);
            return null;
        }
        
        //validation for phone no. format check
        Boolean result = false;
        if (phomenum != '')
        {
            String PhValid          = Label.CLJUN16IDMS31;
            Pattern mypattern       = Pattern.compile(PhValid);
            Matcher myMatcher       = mypattern.matcher(phomenum);
            result                  = myMatcher.matches();
            If(result == false){
                ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS13);
                ApexPages.addMessage(msgareaOfF);
                return null;
            }
            else {
                result = true;
            }
        }
        
        //validation for terms and condition check
        if (checked == false) {
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS21);
            ApexPages.addMessage(msgntChkd );
            return null;
        }
        
        //get currency,timezone,language,preflanguage,local on the basis of country
        String currencyUser;
        IdmsCountryMapping__c cntrmap;
        cntrmap = IdmsCountryMapping__c.getInstance(ucountry);
        if(cntrmap!=null)
        {
            currencyUser = (String)cntrmap.get('DefaultCurrencyIsoCode__c');
            if(currencyUser == null){
                currencyUser = Label.CLJUN16IDMS04;
            }
        }
        else{
            currencyUser = Label.CLJUN16IDMS04;
        }
        String languageUser     = Label.CLJUN16IDMS09;
        String planguageUser    = Label.CLJUN16IDMS10;
        String localeUser       = Label.CLJUN16IDMS11;
        String timezoneUser     = Label.CLJUN16IDMS22;
        
        //set email opt in
        String emailOpt='false';
        if(checkedEmailOpt == true){
            emailOpt='Y';
        }
        else{
            emailOpt='N';
        }
        
        //captch call to verify
        if (challenge != null || response != null) { 
            verify();
            if(enterRecaptchcorrect!=true){
                ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS32);
                ApexPages.addMessage(msgntChkd );
                return null; 
            }
            else{
                enterRecaptchcorrect = true;
            }
        }else{
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS32);
            ApexPages.addMessage(msgntChkd );
            return null;
        }
        /**
        String remoteURL = 'https://se--devidms.cs61.my.salesforce.com/services/apexrest/ProfileUpdate?_HttpMethod=PATCH';
        String profilename ='SE - IDMS Home';
        Profile objProfile = [SELECT Id FROM Profile WHERE Name=:profilename limit 1];
        HTTPRequest httpRequest = new HTTPRequest();
        String AuthUrl ='Bearer ' +userinfo.getSessionId();
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setHeader('Authorization', AuthUrl);
        httpRequest.setMethod('POST');
        String strJson ='{"userId":"'+string.valueof(UserInfo.getUserId())+'","profileId":"'+String.valueof(objProfile.id)+'"}';
        system.debug('strJson::::'+strJson);
        httpRequest.setBody(strJson);
        httpRequest.setEndpoint(remoteURL);
        HTTPResponse httpResponse = new Http().send(httpRequest);
        system.debug('httpResponse'+httpResponse);
        //String profilename ='SE - IDMS Home';
        //Profile objProfile = [SELECT Id FROM Profile WHERE Name=:profilename limit 1];
        //String remoteURL = 'http://se--DevIDMS.cs61.my.salesforce.com/services/apexrest/ProfileUpdate?userId=' +UserInfo.getUserId()+'&profileId='+objProfile.id;
        //HttpRequest httpRequest = new HttpRequest();
        //httpRequest.setMethod('GET');
        //httpRequest.setEndpoint(remoteURL);
        //HttpResponse httpResponse = new Http().send(httpRequest);
        **/
        User Sc_User = [select Id,AccountId, Email,Username,Country,firstName, lastName, MobilePhone, LanguageLocaleKey, LocaleSidKey,
                        TimeZoneSidKey, IDMS_PreferredLanguage__c, profileid, IDMS_Registration_Source__c, IDMS_User_Context__c , IDMS_Email_opt_in__c
                        from User where Id=:UserInfo.getUserId() limit 1];
        Sc_User.Email                       = email;
        Sc_User.Country                     = ucountry;
        Sc_User.Country__c                  = ucountry;
        Sc_User.firstName                   = firstName;
        Sc_User.lastName                    = lastname;
        Sc_User.MobilePhone                 = phomenum;
        Sc_User.LanguageLocaleKey           = languageUser;
        Sc_User.LocaleSidKey                = localeUser;
        Sc_User.TimeZoneSidKey              = timezoneUser;
        Sc_User.IDMS_PreferredLanguage__c   = planguageUser;
        Sc_User.DefaultCurrencyIsoCode      = currencyUser;
        Sc_User.IDMS_Email_opt_in__c        = emailOpt;
        string strContext = Sc_User.IDMS_User_Context__c.substring(1,Sc_User.IDMS_User_Context__c.length());
        Sc_User.profileid        = getProfileId(strContext);
        update Sc_User;
        List<User> userIds = [select Id from User where Id =:Sc_User.Id limit 1];
        if(userIds.size()>0){
            Id userId = userIds[0].Id;
            updatePersonAccountContact(userId);
        }
        //oAuth change
        IdmsSocialUser__c socialUseroAuth;
        socialUseroAuth = IdmsSocialUser__c.getInstance('IdmsKey');
        if(socialUseroAuth!=null){
            oAuthUrl1 = (String)socialUseroAuth.get('oAuthUrl__c');
            oAuthUrl2 = (String)socialUseroAuth.get('oAuthUrl2__c');
            oAuthUrl3 = (String)socialUseroAuth.get('oAuthUrl3__c');
        }
        String oAuthUrl = oAuthUrl1 + oAuthUrl2 + oAuthUrl3;
        if(oAuthUrl1 == null && oAuthUrl2 == null && oAuthUrl3 == null){
            //Get app details from Custom setting
            IDMSApplicationMapping__c appMap;
            appMap = IDMSApplicationMapping__c.getInstance(appid);
            if(appMap != null){
                appName = (String)appmap.get('AppId__c');
            }
            pagereference pr_saml = new Pagereference(Label.CLQ316IDMS226+appName);
            pr_saml.setRedirect(true);
            return pr_saml;
        }else {
            pagereference pr_oauth = new Pagereference(Label.CLNOV16IDMS065+oAuthUrl); 
            pr_oauth.setRedirect(true);
            return pr_oauth;
        }
    }
    //method that provides the profile Id related to the context in parameter
    public Id getProfileId(String context){
       String profilename = IDMS_Profile_Mapping__c.getInstance(context).Profile_Name__c;
       
        List<Profile> ProfileIds = [SELECT Id FROM Profile WHERE Name=:profilename limit 1];
        if(ProfileIds.size() > 0){
            return ProfileIds[0].id;
        }
        return null;
    }
    
    //This method update the related personAccount asynchrounously with userId updated values
    @future      
    private static void updatePersonAccountContact(Id userId) {
        if(!Test.isrunningTest()){
            User Sc_User = [select Id,AccountId, Email,Username,Country,firstName, lastName, MobilePhone, LanguageLocaleKey, LocaleSidKey,
                            TimeZoneSidKey, IDMS_PreferredLanguage__c, IDMS_Registration_Source__c, IDMS_User_Context__c , IDMS_Email_opt_in__c
                            from User where Id =: userId];
            // Update Consumer Account associated to the Social User
            Account prsnAccountContact = [Select ID, LastName ,firstName ,personEmail , country__pc , country__c, IDMS_Contact_UserExternalId__pc,
                                          PersonContactId From Account Where Id = :Sc_User.AccountId];
            //Retrieve country code id of the user
            country__c cid = [select id from country__c where countrycode__c = :Sc_User.Country limit 1];
            
            prsnAccountContact.LastName         = Sc_User.lastName;
            prsnAccountContact.firstName        = Sc_User.firstName;
            prsnAccountContact.personEmail      = Sc_User.email;  
            prsnAccountContact.country__pc      = cid.Id;
            prsnAccountContact.Country__c       = cid.Id;
            update prsnAccountContact; 
        }
    }
    
}