public class SVMXC_TimeEntryRequestAfter {

    public static void checkForTimeEntryRequestSubmission (List<SVMXC_Time_Entry_Request__c> triggerNew, Map<Id, SVMXC_Time_Entry_Request__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to check for Time Entry Request Submission.  If submitted an Event needs to be inserted for the related 
            Technician with the Subject of Pending Time Entry Request (Written Mar 18 2015)
        */  
        
        Set<Id> timeEntryReqIds = new Set<Id>();
        List<Event> newEvents = new List<Event>();
        
        for (SVMXC_Time_Entry_Request__c ter : triggerNew) {
            // find records that have moved from Open to Submitted
            if ((isInsert && ter.Status__c == 'Submitted') ||
                (isUpdate && ter.Status__c == 'Submitted' && triggerOld.get(ter.Id).Status__c != 'Submitted')) {
                
                // need to create a new Event to match the incoming Time Entry Request
                Event newEvent = createEvent(ter, 'Submitted');
                
                newEvents.add(newEvent);
                
            }
        }
        
        if (!newEvents.isEmpty()) {
            insert newEvents;
        }
        
    }

    public static void checkForTimeEntryRequestRejection (List<SVMXC_Time_Entry_Request__c> triggerNew, Map<Id, SVMXC_Time_Entry_Request__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to check for Time Entry Request Rejection.  If rejected, the related Event needs to be deleted (Written Mar 18 2015)
        */
        
        Set<Id> timeEntryRequestIds = new Set<Id>();
        
        for (SVMXC_Time_Entry_Request__c ter : triggerNew) {
            // find records that have moved from Submitted to Rejected
            if (ter.Status__c == 'Rejected' && triggerOld.get(ter.Id).Status__c != 'Rejected') 
                timeEntryRequestIds.add(ter.Id);
        }
        
        if (!timeEntryRequestIds.isEmpty()) {
            // these Time Entry Requests were rejected, so we want to delete the related Event that was created when it was Submitted
            List<Event> eventsToDelete = [SELECT Id FROM Event WHERE WhatId IN : timeEntryRequestIds];
            
            if (!eventsToDelete.isEmpty())
                delete eventsToDelete;
        }
    }
    
    public static void checkForTimeEntryRequestApproval (List<SVMXC_Time_Entry_Request__c> triggerNew, Map<Id, SVMXC_Time_Entry_Request__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to check for Time Entry Request Approval.  If Approved, two things need to occur
            1.  The related Event needs to be update to reflect that the Time Entry Request is approved.
            2.  Corresponding Time Entries need to be created along with any additonal Time Entries related to Travel Time.
            (Written Mar 18 2015)
            
            Update November 10, 2015
            1.  Related Time Entries for All Day Events are broken up into individual Time Entries based on Working Hours object 
                and converted to match the timezone of the working hours object.
                
            Update February 19, 2016
            1.  All Day Events now consider both Working Hours and Break Hours to potentially create 2 Time Entries per day.
        */
       
        Set<Id> timeEntryRequestIds = new Set<Id>();
        Map<Id, SVMXC_Time_Entry_Request__c> timeEntryRequestMap = new Map<Id, SVMXC_Time_Entry_Request__c>();
        List<SVMXC_Time_Entry__c> newTimeEntries = new List<SVMXC_Time_Entry__c>();
        Set<Id> technicianIdsSet = new Set<Id>();
        Map<Id, SVMXC__Service_Group_Members__c> techMap = new Map<Id, SVMXC__Service_Group_Members__c>();
        
        TimeZone userTZ = UserInfo.getTimeZone();
        
        for (SVMXC_Time_Entry_Request__c ter1 : triggerNew) {
            if (ter1.All_Day_Event__c && ter1.Technician__c != null && !technicianIdsSet.contains(ter1.Technician__c))
                technicianIdsSet.add(ter1.Technician__c);
        }
        
        if  (!technicianIdsSet.isEmpty()) {
            techMap = queryTechnician(technicianIdsSet);    
        }
        
        for (SVMXC_Time_Entry_Request__c ter : triggerNew) {
            // find records that have moved from Submitted to Approved
            if (ter.Status__c == 'Approved' && triggerOld.get(ter.Id).Status__c != 'Approved') {
                timeEntryRequestIds.add(ter.Id);   
                timeEntryRequestMap.put(ter.Id, ter);     
                
                // need to create a related Time Entry from each Time Entry Request (and possibly two more to cover Travel Time)
                if (ter.All_Day_Event__c) {
                    
                    // determine if technician has valid Business Hours Record
                    if (techMap.get(ter.Technician__c).SVMXC__Working_Hours__c == null) {
                        ter.addError('The Technician does not have a Working Hours record setup to determine how to build the All Day Event.  Contact the Administrator to have it setup.');
                        continue;
                    } 
                    
                    // determine how many days are in the range
                    
                    // set comparison dates, new functionality was introduced with March release to set the date based on creator timezone to alleviate
                    // timezone difference issues.  However, there are still in-flight TERs that need to accommodate missing data for the field
                    DateTime workingStartDateTime = ter.Start_Date__c;
                    DateTime workingEndDateTime = ter.End_Date__c;
                    
                    if (ter.Start_Date_for_All_Day_Event__c != null)
                        workingStartDateTime = DateTime.newInstance(ter.Start_Date_for_All_Day_Event__c.year(), ter.Start_Date_for_All_Day_Event__c.month(), ter.Start_Date_for_All_Day_Event__c.day());
                    
                     if (ter.End_Date_for_All_Day_Event__c != null)
                        workingEndDateTime = DateTime.newInstance(ter.End_Date_for_All_Day_Event__c.year(), ter.End_Date_for_All_Day_Event__c.month(), ter.End_Date_for_All_Day_Event__c.day());
                    
                    
                    Integer daysBetween =  daysBetweenDateTime(workingStartDateTime, workingEndDateTime);
                    
                    Timezone workingHoursTZ = Timezone.getTimeZone(techMap.get(ter.Technician__c).SVMXC__Working_Hours__r.TimeZoneSidKey);  
                    
                    for (Integer i=0; i < daysBetween + 1; i++) {
                        
                        //DateTime workingDateTime = getGmtDateTime(ter.Start_Date__c.addDays(i));
                        //DateTime workingDateTime = ter.Start_Date__c.addDays(i);
                        DateTime workingDateTime = workingStartDateTime.addDays(i);
                        
                        // determine if day has a break hour to consider
                        Boolean techHasBreakHours = doesTechHaveBreakHourForGivenDay(workingDateTime, techMap.get(ter.Technician__c));
                        
                        if (techHasBreakHours) {
                        
                            // create first part of Time Entry (up to break)
                            Time startTime =  getTimeFromBusinessHours (workingDateTime, techMap.get(ter.Technician__c), 'startBreak1st');
                            Time endTime =  getTimeFromBusinessHours (workingDateTime, techMap.get(ter.Technician__c), 'endBreak1st');
                            
                            if (startTime != null && endTime != null)
                                newTimeEntries.add(buildTimeEntry(workingDateTime, startTime, endTime, ter, userTZ, workingHoursTZ));
                            
                            // create second part of Time Entry (after break)    
                            Time startTime2 =  getTimeFromBusinessHours (workingDateTime, techMap.get(ter.Technician__c), 'startBreak2nd');
                            Time endTime2 =  getTimeFromBusinessHours (workingDateTime, techMap.get(ter.Technician__c), 'endBreak2nd');
                            
                            if (startTime2 != null && endTime2 != null)
                                newTimeEntries.add(buildTimeEntry(workingDateTime, startTime2, endTime2, ter, userTZ, workingHoursTZ));   
                            
                        } else {
                        
                            // tech does not have a valid break time for given day, so only one Time Entry will be created
                            Time startTime3 =  getTimeFromBusinessHours (workingDateTime, techMap.get(ter.Technician__c), 'start');
                            Time endTime3 =  getTimeFromBusinessHours (workingDateTime, techMap.get(ter.Technician__c), 'end');
                            
                            if (startTime3 != null && endTime3 != null)
                                newTimeEntries.add(buildTimeEntry(workingDateTime, startTime3, endTime3, ter, userTZ, workingHoursTZ)); 
                        
                        }                                    
                    } 
                    
                } else {
                        
                    // non All Day Events 
                    newTimeEntries.add(createTimeEntry(ter.Start_Date__c, ter.End_Date__c, ter.Technician__c, ter.Activity__c, 
                                                    ter.Notes__c, ter.Id, ter.All_Day_Event__c));
                
                    if (ter.Travel_To_Hours__c != null && ter.Travel_To_Hours__c > 0) {
                        
                        Integer minuteAdj = computeMinutesFromFractionalHours(ter.Travel_To_Hours__c) * -1;                    
                        DateTime startDate = ter.Start_Date__c.addMinutes(minuteAdj);
                    
                        newTimeEntries.add(createTimeEntry(startDate, ter.Start_Date__c, ter.Technician__c, 'Travel Time', 
                                                            ter.Notes__c, ter.Id, ter.All_Day_Event__c));  
                    }
                    
                    if (ter.Travel_From_Hours__c != null && ter.Travel_From_Hours__c > 0) {
                        
                        Integer minuteAdj2 = computeMinutesFromFractionalHours(ter.Travel_From_Hours__c);
                        DateTime endDate = ter.End_Date__c.addMinutes(minuteAdj2);
                    
                        newTimeEntries.add(createTimeEntry(ter.End_Date__c, endDate, ter.Technician__c, 'Travel Time', 
                                                            ter.Notes__c, ter.Id, ter.All_Day_Event__c));   
                    } 
                }   
            }
        }
        
        System.debug('#### newTimeEntries ' + newTimeEntries);
        if (!newTimeEntries.isEmpty())
            insert newTimeEntries;
        
        if (!timeEntryRequestIds.isEmpty()) {
            
            Set<Id> timeEntriesWithEventsId = new Set<Id>();
            
            // look for already created Events and update the Subject to reflect that the Time Entry Request has been approved
            List<Event> eventsToUpdate = [SELECT Id, Subject FROM Event WHERE WhatId IN : timeEntryRequestIds];
            
            if (!eventsToUpdate.isEmpty()) {
                for (Event e : eventsToUpdate) {
                    String currentSubject = e.Subject;
                    currentSubject = currentSubject.replace('Submitted', 'Approved');
                    e.Subject = currentSubject;
                    
                    // remove the Time Entry Id from the timeEntryRequestIds Set to keep track of new Events that need to be inserted
                    // since these didn't pass through the approval process
                    timeEntriesWithEventsId.add(e.WhatId);
                }
                
                // look for new Events that need to be inserted
                if (!timeEntriesWithEventsId.isEmpty()) {
                    
                    for (Id i : timeEntryRequestIds) {
                        if (timeEntriesWithEventsId.contains(i)) {
                            Event newEvent = createEvent(timeEntryRequestMap.get(i), 'Approved');
                            eventsToUpdate.add(newEvent);      
                        }                   
                    }
                }
                
                upsert eventsToUpdate;
            }
        }
    }

    public static void submitTimeEntryRequestForApproval (List<SVMXC_Time_Entry_Request__c> triggerNew, Map<Id, SVMXC_Time_Entry_Request__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to submit a Time Entry Request to the approval process (Written Mar 18 2015)
        */
        
         if (isUpdate || isInsert) {
       
            // check if the TER is being submitted against an existing approved/submitted timesheet
            Set<Id> fsrIds = new Set<Id>();
            Set<Id> terIds = new Set<Id>();
            Set<Id> terMatchedIds = new Set<Id>();
            Date startDate = Date.today();
            Date endDate = Date.today();
            
            for (SVMXC_Time_Entry_Request__c ter : triggerNew) {
                if (ter.Submitted_for_Approval__c && ter.Start_Date__c != null) {
                    
                    Date currentDate = Date.newinstance(ter.Start_Date__c.year(), ter.Start_Date__c.month(), ter.Start_Date__c.day());          
                    terIds.add(ter.Id);
                    
                    if (ter.Technician__c != null && !fsrIds.contains(ter.Technician__c))
                        fsrIds.add(ter.Technician__c);
                    if (currentDate < startDate)
                        startDate = currentDate;
                    if (currentDate > endDate)
                        endDate = currentDate;
                }           
            }
            
            if (!terIds.isEmpty()) {
                
                startDate = startDate.addDays(-7);
                endDate = endDate.addDays(7);
                
                System.debug('startDate for Timesheet search ' + startDate);
                System.debug('endDate for Timesheet search ' + endDate);
                
                // query timesheets that might be within the range of the time entry requests
                List<SVMXC_Timesheet__c> timeSheets = [SELECT Technician__c, Start_Date__c, End_Date__c, Status__c, Name 
                                                        FROM SVMXC_Timesheet__c WHERE Technician__c IN : fsrIds
                                                        AND Start_Date__c >= :startDate AND End_Date__c <= :endDate];
                
                for (SVMXC_Time_Entry_Request__c ter1 : triggerNew) {
                
                    // check to see if any of the submitted requests fall into a Timesheet period that is either approved or submitted
                    if (terIds.contains(ter1.Id)) {
                        
                        Date currentDate = Date.newinstance(ter1.Start_Date__c.year(), ter1.Start_Date__c.month(), ter1.Start_Date__c.day());   
                        Boolean match = false;
                        
                        for (SVMXC_Timesheet__c ts : timeSheets) {
                            
                            if (ts.Technician__c == ter1.Technician__c && ts.Start_Date__c <= currentDate && ts.End_Date__c >= currentDate
                                && (ts.Status__c == 'Submitted' || ts.Status__c == 'Approved')) {
                                    match = true;
                                    System.debug('### Match to Timesheet found : ' + ts.Name);
                                    System.debug('### Time Entry Request : ' + ter1.Name);
                                    break;
                            }       
                        }
                        
                        if (match) {
                            terMatchedIds.add(ter1.Id); 
                        }
                    }           
                }
            }
            
            // obtain running user mgr field
            User runningUser = [SELECT ManagerId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
    
            for (SVMXC_Time_Entry_Request__c ter2: triggerNew) {
             
                if (ter2.Submitted_for_Approval__c) {       
                
                    if (!terMatchedIds.contains(ter2.Id)) {
                        if (runningUser.ManagerId != null) {
                            
                            // create the new approval request to submit
                            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                            req.setComments('Submitted for approval via Submit for Approval button.');
                            req.setObjectId(ter2.Id);
                            // submit the approval request for processing
                            Approval.ProcessResult result = Approval.process(req);
                            // display if the request was successful
                            System.debug('Submitted for approval successfully: '+result.isSuccess());
                            
                            // if it was successfully submitted for approval, do anything?
                            if (result.isSuccess()) {
                                // do anything else?
                            } else {
                                ter2.addError('There was an issue submitting this record to the approval process');
                            }
                            
                        } else {
                            // submitter is missing the manager field on their user record
                            ter2.addError('This Time Entry Request can not be submitted for approval because your user record does not contain a manager.  Please contact your administrator to get one established.');
                        }
                    } else {
                        // the time entry request is related to an already approved or submitted timesheet and should not be allowed to be saved
                        ter2.addError('This Time Entry Request is associated to an existing Timesheet that is already approved or submitted.'); 
                    }
                }    
            }
        }   
    }
    
    private static Integer daysBetweenDateTime (DateTime incomingStartDate, DateTime incomingEndDate) {
        
        Date startDate = Date.newInstance(incomingStartDate.year(), incomingStartDate.month(), incomingStartDate.day());
        Date endDate = Date.newInstance(incomingEndDate.year(), incomingEndDate.month(), incomingEndDate.day());
        
        System.debug('#### incomingStartDate ' + incomingStartDate);
        System.debug('#### incomingEndDate' + incomingEndDate);      
        System.debug('#### startDate ' + startDate);
        System.debug('#### endDate' + endDate);
        
        System.debug('#### daysBetweenDateTime : ' + startDate.daysBetween(endDate));
        return startDate.daysBetween(endDate); 
        
    }
    
    public static Integer getTimeZoneOffsetBetweenUserAndBusinessHours (TimeZone userTZ, TimeZone workingHoursTZ, DateTime incomingDate) {
        
        // returns TimeZone offset in seconds for a given date between running User and Working Hours in Seconds
        Integer timeZoneOffsetMillisecondsForWH = workingHoursTZ.getOffset(incomingDate);               
        Integer timeZoneOffsetMillisecondsForUser = userTZ.getOffset(incomingDate);
                            
        System.debug('#### timeZoneOffset between GMT and working hours timezone in milliseconds ' + timeZoneOffsetMillisecondsForWH);
        System.debug('#### timeZoneOffset between GMT and user timezone in milliseconds ' + timeZoneOffsetMillisecondsForUser);
        
        return ((timeZoneOffsetMillisecondsForUser - timeZoneOffsetMillisecondsForWH) / 1000);    
        
    }
      
    private static SVMXC_Time_Entry__c buildTimeEntry (DateTime workingDateTime, Time startTime, Time endTime, SVMXC_Time_Entry_Request__c ter, TimeZone userTZ, TimeZone workingHoursTZ) {
    
        SVMXC_Time_Entry__c newTimeEntry = null;
               
        Date startDate = Date.newInstance(workingDateTime.year(), workingDateTime.month(), workingDateTime.day());              
                            
        // these times are in the users local timezone, so we need to determine any offset to the timezone of the working hours                         
        DateTime startDateTime = DateTime.newInstance(startDate, startTime);
        DateTime endDateTime = DateTime.newInstance(startDate, endTime);
        System.debug('##### startDateTime ' + startDateTime);
                            
        Integer timeZoneOffsetSeconds = getTimeZoneOffsetBetweenUserAndBusinessHours (userTZ, workingHoursTZ, startDateTime);
        System.debug('##### timeZoneOffsetSeconds ' + timeZoneOffsetSeconds);
        
        newTimeEntry = createTimeEntry(startDateTime.addSeconds(timeZoneOffsetSeconds), endDateTime.addSeconds(timeZoneOffsetSeconds), 
                                        ter.Technician__c, ter.Activity__c, ter.Notes__c, ter.Id, ter.All_Day_Event__c);   
        //newTimeEntry = createTimeEntry(startDateTime, endDateTime, ter.Technician__c, ter.Activity__c, ter.Notes__c, ter.Id, ter.All_Day_Event__c);   
        
        
        return newTimeEntry;
    
    }
    
    
    private static Boolean doesTechHaveBreakHourForGivenDay(DateTime incomingDateTime, SVMXC__Service_Group_Members__c tech) {
        
        Boolean techHasBreakHours = false;
        
        // determine day of week
        String dayOfWeek = incomingDateTime.format('EEEE');
        Time workingTime;
        
        System.debug('### dayOfWeek ' + dayOfWeek);
        
        if (dayofWeek == 'Monday' && tech.SVMXC__Break_Hours__r.MondayStartTime != null && tech.SVMXC__Break_Hours__r.MondayEndTime != null)
            techHasBreakHours = true;
        else if (dayofWeek == 'Tuesday' && tech.SVMXC__Break_Hours__r.TuesdayStartTime != null && tech.SVMXC__Break_Hours__r.TuesdayEndTime != null)
            techHasBreakHours = true;
        else if (dayofWeek == 'Wednesday' && tech.SVMXC__Break_Hours__r.WednesdayStartTime != null && tech.SVMXC__Break_Hours__r.WednesdayEndTime != null)
            techHasBreakHours = true;
        else if (dayofWeek == 'Thursday' && tech.SVMXC__Break_Hours__r.ThursdayStartTime != null && tech.SVMXC__Break_Hours__r.ThursdayEndTime != null)
            techHasBreakHours = true;  
        else if (dayofWeek == 'Friday' && tech.SVMXC__Break_Hours__r.FridayStartTime != null && tech.SVMXC__Break_Hours__r.FridayEndTime != null)
            techHasBreakHours = true;
        else if (dayofWeek == 'Saturday' && tech.SVMXC__Break_Hours__r.SaturdayStartTime != null && tech.SVMXC__Break_Hours__r.SaturdayEndTime != null)
            techHasBreakHours = true;
        else if (dayofWeek == 'Sunday' && tech.SVMXC__Break_Hours__r.SundayStartTime != null && tech.SVMXC__Break_Hours__r.SundayEndTime != null)
            techHasBreakHours = true;
        
        return techHasBreakHours;
    
    }
    
    private static Time getTimeFromBusinessHours (DateTime incomingDate, SVMXC__Service_Group_Members__c techRecord, String startOrEnd) {
        
        System.debug('getTimeFromBusinessHours method()');
        System.debug('### incomingDate ' + incomingDate);
        System.debug('### startOrEnd ' + startOrEnd);
        
        // determine day of week
        String dayOfWeek = incomingDate.format('EEEE');
        Time workingTime;
        
        System.debug('### dayOfWeek ' + dayOfWeek);
        
        if (dayOfWeek == 'Monday') {
            if (startOrEnd == 'start' || startOrEnd == 'startBreak1st')
                workingTime = techRecord.SVMXC__Working_Hours__r.MondayStartTime;
            else if (startOrEnd == 'end' || startOrEnd == 'endBreak2nd')
                workingTime = techRecord.SVMXC__Working_Hours__r.MondayEndTime;
            else if (startOrEnd == 'endBreak1st')
                workingTime = techRecord.SVMXC__Break_Hours__r.MondayStartTime;
            else if (startOrEnd == 'startBreak2nd')
                workingTime = techRecord.SVMXC__Break_Hours__r.MondayEndTime;
            
        } else if (dayOfWeek == 'Tuesday') {
            if (startOrEnd == 'start' || startOrEnd == 'startBreak1st')
                workingTime = techRecord.SVMXC__Working_Hours__r.TuesdayStartTime;
            else if (startOrEnd == 'end' || startOrEnd == 'endBreak2nd')
                workingTime = techRecord.SVMXC__Working_Hours__r.TuesdayEndTime;
            else if (startOrEnd == 'endBreak1st')
                workingTime = techRecord.SVMXC__Break_Hours__r.TuesdayStartTime;
            else if (startOrEnd == 'startBreak2nd')
                workingTime = techRecord.SVMXC__Break_Hours__r.TuesdayEndTime;
            
        } if (dayOfWeek == 'Wednesday') {
            if (startOrEnd == 'start' || startOrEnd == 'startBreak1st')
                workingTime = techRecord.SVMXC__Working_Hours__r.WednesdayStartTime;
            else if (startOrEnd == 'end' || startOrEnd == 'endBreak2nd')
                workingTime = techRecord.SVMXC__Working_Hours__r.WednesdayEndTime;
            else if (startOrEnd == 'endBreak1st')
                workingTime = techRecord.SVMXC__Break_Hours__r.WednesdayStartTime;
            else if (startOrEnd == 'startBreak2nd')
                workingTime = techRecord.SVMXC__Break_Hours__r.WednesdayEndTime;
            
        } if (dayOfWeek == 'Thursday') {
            if (startOrEnd == 'start' || startOrEnd == 'startBreak1st')
                workingTime = techRecord.SVMXC__Working_Hours__r.ThursdayStartTime;
            else if (startOrEnd == 'end' || startOrEnd == 'endBreak2nd')
                workingTime = techRecord.SVMXC__Working_Hours__r.ThursdayEndTime;
            else if (startOrEnd == 'endBreak1st')
                workingTime = techRecord.SVMXC__Break_Hours__r.ThursdayStartTime;
            else if (startOrEnd == 'startBreak2nd')
                workingTime = techRecord.SVMXC__Break_Hours__r.ThursdayEndTime;
            
        } if (dayOfWeek == 'Friday') {
            if (startOrEnd == 'start' || startOrEnd == 'startBreak1st')
                workingTime = techRecord.SVMXC__Working_Hours__r.FridayStartTime;
            else if (startOrEnd == 'end' || startOrEnd == 'endBreak2nd')
                workingTime = techRecord.SVMXC__Working_Hours__r.FridayEndTime;
            else if (startOrEnd == 'endBreak1st')
                workingTime = techRecord.SVMXC__Break_Hours__r.FridayStartTime;
            else if (startOrEnd == 'startBreak2nd')
                workingTime = techRecord.SVMXC__Break_Hours__r.FridayEndTime;
            
        } if (dayOfWeek == 'Saturday') {
            if (startOrEnd == 'start' || startOrEnd == 'startBreak1st')
                workingTime = techRecord.SVMXC__Working_Hours__r.SaturdayStartTime;
            else if (startOrEnd == 'end' || startOrEnd == 'endBreak2nd')
                workingTime = techRecord.SVMXC__Working_Hours__r.SaturdayEndTime;
            else if (startOrEnd == 'endBreak1st')
                workingTime = techRecord.SVMXC__Break_Hours__r.SaturdayStartTime;
            else if (startOrEnd == 'startBreak2nd')
                workingTime = techRecord.SVMXC__Break_Hours__r.SaturdayEndTime;
            
        } if (dayOfWeek == 'Sunday') {
            if (startOrEnd == 'start' || startOrEnd == 'startBreak1st')
                workingTime = techRecord.SVMXC__Working_Hours__r.SundayStartTime;
            else if (startOrEnd == 'end' || startOrEnd == 'endBreak2nd')
                workingTime = techRecord.SVMXC__Working_Hours__r.SundayEndTime;
            else if (startOrEnd == 'endBreak1st')
                workingTime = techRecord.SVMXC__Break_Hours__r.SundayStartTime;
            else if (startOrEnd == 'startBreak2nd')
                workingTime = techRecord.SVMXC__Break_Hours__r.SundayEndTime;
            
        }
        
        System.debug('#### returning workingTime of ' + workingTime + ' for ' + startOrEnd);
    
        return workingTime;
    }
    
    private static Map<Id, SVMXC__Service_Group_Members__c> queryTechnician(Set<Id> technicianIdsSet) {
        
        Map<Id, SVMXC__Service_Group_Members__c> returnMap = new Map<Id, SVMXC__Service_Group_Members__c> 
                ([SELECT SVMXC__Working_Hours__r.TimeZoneSidKey, 
                SVMXC__Working_Hours__r.SaturdayEndTime, SVMXC__Working_Hours__r.SaturdayStartTime, 
                SVMXC__Working_Hours__r.FridayEndTime, SVMXC__Working_Hours__r.FridayStartTime, 
                SVMXC__Working_Hours__r.ThursdayEndTime, SVMXC__Working_Hours__r.ThursdayStartTime, 
                SVMXC__Working_Hours__r.WednesdayEndTime, SVMXC__Working_Hours__r.WednesdayStartTime, 
                SVMXC__Working_Hours__r.TuesdayEndTime, SVMXC__Working_Hours__r.TuesdayStartTime, 
                SVMXC__Working_Hours__r.MondayEndTime, SVMXC__Working_Hours__r.MondayStartTime, 
                SVMXC__Working_Hours__r.SundayEndTime, SVMXC__Working_Hours__r.SundayStartTime, 
                SVMXC__Working_Hours__r.IsDefault, SVMXC__Working_Hours__r.IsActive, 
                SVMXC__Working_Hours__r.Name, SVMXC__Working_Hours__r.Id, SVMXC__Break_Hours__c,
                SVMXC__Break_Hours__r.SaturdayEndTime, SVMXC__Break_Hours__r.SaturdayStartTime, 
                SVMXC__Break_Hours__r.FridayEndTime, SVMXC__Break_Hours__r.FridayStartTime, 
                SVMXC__Break_Hours__r.ThursdayEndTime, SVMXC__Break_Hours__r.ThursdayStartTime, 
                SVMXC__Break_Hours__r.WednesdayEndTime, SVMXC__Break_Hours__r.WednesdayStartTime, 
                SVMXC__Break_Hours__r.TuesdayEndTime, SVMXC__Break_Hours__r.TuesdayStartTime, 
                SVMXC__Break_Hours__r.MondayEndTime, SVMXC__Break_Hours__r.MondayStartTime, 
                SVMXC__Break_Hours__r.SundayEndTime, SVMXC__Break_Hours__r.SundayStartTime, 
                SVMXC__Break_Hours__r.IsDefault, SVMXC__Break_Hours__r.IsActive, 
                SVMXC__Break_Hours__r.Name, SVMXC__Break_Hours__r.Id
                FROM SVMXC__Service_Group_Members__c WHERE Id IN : technicianIdsSet]);
                
        return returnMap;
    }
    
    private static Event createEvent(SVMXC_Time_Entry_Request__c incomingTER, String status) {
        
        Event returnObj = new Event();
        
        // need to create a new Event to match the incoming Time Entry Request
        Event newEvent = new Event();
        returnObj.Subject = incomingTER.Activity__c + ' - ' + status;
        returnObj.WhatId = incomingTER.Id;
        
        // AMO : August 9th, 2016 : CASE 00042347 : TER'S SERVICE TECK Not displaying in the Dispatch console        
        // add in travel time to Event creation if specified and the All Day Event isn't specified.
        if (!incomingTER.All_Day_Event__c) 
        {
        	DateTime startDate = incomingTER.Start_Date__c;
        	if (incomingTER.Travel_To_Hours__c != null && incomingTER.Travel_To_Hours__c > 0)
        	{    
            	Integer hourAdj = Integer.valueOf(incomingTER.Travel_To_Hours__c) * -1;
            	startDate = incomingTER.Start_Date__c.addHours(hourAdj);
        	}
            returnObj.StartDateTime = startDate;          
        } else
            returnObj.StartDateTime = incomingTER.Start_Date__c.date();
        
        // add in travel time to Event creation if specified and the All Day Event isn't specified. 
        if (!incomingTER.All_Day_Event__c) 
        {
        	DateTime endDate = incomingTER.End_Date__c;
        	if (incomingTER.Travel_From_Hours__c != null && incomingTER.Travel_From_Hours__c > 0)
        	{            
            	Integer hourAdj2 = Integer.valueOf(incomingTER.Travel_From_Hours__c);
            	endDate = incomingTER.End_Date__c.addHours(hourAdj2);
        	}
        	returnObj.EndDateTime = endDate;            
        } else
            returnObj.EndDateTime = incomingTER.End_Date__c.date();
            
        returnObj.IsAllDayEvent = incomingTER.All_Day_Event__c;
        returnObj.OwnerId = incomingTER.OwnerId;
        returnObj.Description = incomingTER.Notes__c;
        
        return returnObj;
    }
    
    private static SVMXC_Time_Entry__c createTimeEntry (DateTime startDate, DateTime endDate, Id techId, String Activity, 
                                                        String Comments, Id TimeEntryId, Boolean allDayEvent) {
        
        SVMXC_Time_Entry__c returnObj = new SVMXC_Time_Entry__c();
        
        System.debug('#### setting start datetime for Time Entry as : ' + startDate);
        System.debug('#### setting enddatetime for Time Entry as : ' + endDate);
        
        returnObj.Start_Date_Time__c = startDate;
        returnObj.End_Date_Time__c = endDate;
        returnObj.Technician__c = techId;
        returnObj.Activity__c = Activity;
        returnObj.Comments__c = Comments;
        returnObj.Time_Entry_Request__c = TimeEntryId;
        returnObj.All_Day_Event_from_Time_Entry_Request__c = allDayEvent;
        
        return returnObj;
    }
    
    private static Integer computeMinutesFromFractionalHours(Decimal incomingHours) {
        
        Integer returnMinutes = 0;  
        
        if (incomingHours != null && incomingHours > 0) {
            Decimal minutes = incomingHours * 60;
            returnMinutes = Integer.valueOf(minutes);
        }
        
        return returnMinutes;
    }
    
}