@isTest
private class VFC_MarkForAutoFollowup_TEST{
    static testMethod void performAction_Test() {
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        objOpenCase.AnswerToCustomer__c ='Issue is resolved, Need Follow up';
        Database.insert(objOpenCase);

        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objOpenCase);   
        VFC_MarkForAutoFollowup MarkForFollowupPage = new VFC_MarkForAutoFollowup(CaseStandardController );
        Test.startTest();
        PageReference pageRef = Page.VFP_MarkForAutoFollowup;
        Test.setCurrentPage(pageRef);

        MarkForFollowupPage.performAction();
        MarkForFollowupPage.returnToCase();
        Test.stopTest();
    }
}