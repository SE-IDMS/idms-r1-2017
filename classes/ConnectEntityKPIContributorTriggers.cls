/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  01-Oct-2012
    Description         : Class for Connect Entity KPI Contributor Triggers Sharing permission on Cascading KPIs for users
*/

public class ConnectEntityKPIContributorTriggers
{

Public Static void EntityKPIContributorAfterInsert(list<Entity_KPI_Contributors__c> ECKPI)
 {
 List<Cascading_KPI_Target__Share>  CascKPIShare = new List<Cascading_KPI_Target__Share>();
 for(Entity_KPI_Contributors__c EKC:ECKPI)
 {
 Cascading_KPI_Target__Share  CascadingKPIShare = new Cascading_KPI_Target__Share();
         
           string permission;
           if(EKC.Contributor_Permission__c == 'Read Only') 
              permission = 'read';
            else
              permission = 'edit';
               
           CascadingKPIShare.ParentId = EKC.Cascading_KPI_Target__c;
           CascadingKPIShare.UserOrGroupId = EKC.User__c;
           CascadingKPIShare.AccessLevel = permission;
           CascadingKPIShare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Entity_Data_Contributor__c;
          CascKPIShare.add(CascadingKPIShare);
}
       Database.insert(CascKPIShare,false);
         
         //System.debug(' Record ID ' + ECKPI.Cascading_KPI_Target__r);
  }
  
  Public Static void EntityKPIContributorAfterUpdate(List<Entity_KPI_Contributors__c> EKPIC, Entity_KPI_Contributors__c CKPICold)
      {
      List<Cascading_KPI_Target__Share>  CascKPIShare = new List<Cascading_KPI_Target__Share>();
      
     
      List<Cascading_KPI_Target__Share>  CascadingKPISharedelete = [Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId = :CKPICold.Cascading_KPI_Target__c and UserOrGroupId = :CKPICold.User__c]; 
     system.debug('------'+CascadingKPISharedelete.size());
      if(CascadingKPISharedelete.size() > 0)
                    
      Database.delete(CascadingKPISharedelete[0],false);
     
      
       for(Entity_KPI_Contributors__c EKC:EKPIC)
 {
 Cascading_KPI_Target__Share  CascadingKPIShare = new Cascading_KPI_Target__Share();
         
           string permission;
           if(EKC.Contributor_Permission__c == 'Read Only') 
              permission = 'read';
            else
              permission = 'edit';
               
           CascadingKPIShare.ParentId = EKC.Cascading_KPI_Target__c;
           CascadingKPIShare.UserOrGroupId = EKC.User__c;
           CascadingKPIShare.AccessLevel = permission;
           CascadingKPIShare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Entity_Data_Contributor__c;
          CascKPIShare.add(CascadingKPIShare);
}
try{
       Database.insert(CascKPIShare,false);
       }catch(DMLException e){
        System.Debug('Error Creating sharing permission');          
       }
   }
   
   Public Static void EntityKPIContributorAfterDelete(List<Entity_KPI_Contributors__c> EKPIC)
      {
      
      
      List<Cascading_KPI_Target__Share>  CascadingKPISharedelete = [Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId = :EKPIC[0].Cascading_KPI_Target__c and UserOrGroupId = :EKPIC[0].User__c]; 
      if(CascadingKPISharedelete.size() > 0)              
      Database.delete(CascadingKPISharedelete[0],false);
      }
  
  
 }