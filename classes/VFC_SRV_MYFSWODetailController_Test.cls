@isTest
Public class VFC_SRV_MYFSWODetailController_Test{
    
    static testMethod void queryAccountsAndCountries() {
       
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
                              and developerName = 'Customer'
                              limit 1
                             ][0].Id;
        
        Account acc1 = new Account(Name = 'TestAccount', Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
        insert acc1;
        Country__c c = new Country__c(Name='India');
        Contact Cnct = new Contact(AccountId = acc1.id, Country__c=c.Id,email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
        insert cnct;
        Profile p = [SELECT Id FROM Profile WHERE Name = :System.label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];
         test.starttest();
       /* user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
                          EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                          ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
                          // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
                          isActive = True, BypassVR__c = True, contactId = cnct.id);
        insert u;
       */ 
      //  system.runAs(u) {
            
            SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id);
            insert location1;
            
            RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
                                                      and developerName = 'LocationContactRole'
                                                      limit 1
                                                     ]; //[0].Id;
            Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
            insert ro;
            // creating IP
            Product2 prd = New Product2(Name='Circuit Breaker',IsActive=true);
            insert prd;
             Date myDate = Date.newInstance(2016, 2, 20);
             SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'Test', SVMXC__Site__c = location1.Id, SVMXC__Company__c = acc1.Id, 
                                                                             SVMXC__Product__c=prd.Id, SKUToCreate__c='Test2',SVMXC__Warranty_Start_Date__c=myDate);            
            insert ip;      
            //Creating Work Order
            DateTime myDateTime2 = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
            SVMXC__Service_Order__c newrecpast= new SVMXC__Service_Order__c();
            newrecpast.SVMXC__Company__c=acc1.Id;
            newrecpast.SVMXC__Site__c=location1.Id;
            newrecpast.SVMXC__Contact__c=cnct.id;
            newrecpast.SVMXC__Scheduled_Date_Time__c=myDateTime2;
            newrecpast.SVMXC__Order_Status__c = 'Service Complete';
            newrecpast.SVMXC__Component__c = ip.id;
            insert  newrecpast;
            Attachment attach= new Attachment();
            attach.Name = 'testbFS___Service_Intervention_Report.pdf' ;
            attach.parentId = newrecpast.Id;
            attach.Description ='Work Order Output Doc';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach.body=bodyBlob;
            insert attach; 
            SVMXC__Service_Order_Line__c woDetail = new SVMXC__Service_Order_Line__c(SVMXC__Serial_Number__c=ip.id,SVMXC__Service_Order__c=newrecpast.id); 
            insert woDetail;
            VFC_SRV_MYFSWODetailController woController = new VFC_SRV_MYFSWODetailController();
            woController.woId = newrecpast.Id;
            woController.woStatus =  newrecpast.SVMXC__Order_Status__c;
            woController.getWODetails();
            
      //  }
        
    }
}