@isTest
public class VFC_SetupDefaultOppCompetitor_TEST
{
    static testMethod void runPositiveTestCases() {
	Id userId=userInfo.getUserId();
    List<UserParameter__c> userparamList=new List<UserParameter__c>();
    List<Competitor__C> compList=new List<Competitor__C>();
	
    PageReference pg=page.VFP_SetupDefaultOpportunityCompetitor;
	pg.getParameters().put('userId',userId);
	test.setCurrentPage(pg);
	UserParameter__c u=new UserParameter__c();
    system.debug('***userId***'+userId);	
	SPASalesGroup__c s=new SPASalesGroup__c();
        s.Name ='Test Sales group6';
        insert s;
        system.debug('inserted SPASalesGroup'+s);
               
        Competitor__c c9=new Competitor__c();
        c9.name='qr';
        c9.Active__c=true;
        insert c9;
        
        Competitor__c c10=new Competitor__c();
        c10.name='OTHER';
        c10.Active__c=true;
        insert c10;
        compList=[select Id,name from Competitor__C where Name='OTHER'];
        
        u.User__c=userId;
        u.SPASalesGroup__c=s.id;
        u.Competitor__c=c9.id;
        u.Competitor2__c=c9.id;
        u.Competitor3__c=c9.id;
        u.Competitor4__c=c9.id;
        u.Competitor5__c=c9.id;
        u.Competitor6__c=c9.id;
        u.Competitor7__c=c9.id;
        u.Competitor8__c=c9.id;
        u.Competitor9__c=c9.id;
        u.Competitor10__c=c9.id;
        insert u;
        userparamList.add(u);
	Apexpages.StandardController sc = new Apexpages.standardController(u);
	VFC_SetupDefaultOpportunityCompetitor sdoc=new VFC_SetupDefaultOpportunityCompetitor(sc);
	
        
    sdoc.hasCompetitor=True;
    Test.startTest();
    sdoc.upserttUserParameters();
    
    sdoc.hasCompetitor=False;   
    
    sdoc.upserttUserParameters();
    sdoc.Cancel();    
    Test.stopTest();    
    }
    
}