/*   Author: Sukumar Salla (GD Team)  
     Date Of Creation:  
     Description : Controller for the Complaint/Request creation page 
*/

public class VFC_NewComplaintRequest
{
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/

    public ComplaintRequest__c NewComplaintRequest{get;set;} // Current new TEX
    public Case relatedCase{get;set;} // Master Case
    public ComplaintRequest__c relatedComplaintRequest{get;set;}
    Public Boolean IsCancel{get; set;} // Flag to display Cancel Button
    Public boolean isAuthorizedUser {get; set;}
       

    /*=======================================
      CONSTRUCTOR
    =======================================*/ 
    
    public VFC_NewComplaintRequest(ApexPages.StandardController controller)
    {
        NewComplaintRequest = (ComplaintRequest__c)controller.getRecord();
      
    } 
    
    /*=======================================
      METHODS
    =======================================*/ 
 
    // Method to initialize the RMA with the defaulting values
    public void initializeComplaintRequest(ComplaintRequest__c aNewComplaintRequest)
    {
        relatedCase = New Case();           
        String caseID = system.currentpagereference().getParameters().get(Label.CL00330);
                  
            if (caseID != null)
            {
                caseID = caseID.substring(1); 
            } 
 
     
        // Retrieve related case
        List<Case> caseList = [SELECT ID,CaseNumber, Account.Country__r.FrontOfficeOrganization__c,Account.Country__r.FrontOfficeOrganization__r.name,Contact.Country__r.FrontOfficeOrganization__r.Name,Contact.Country__r.FrontOfficeOrganization__c,SupportCategory__c,Symptom__c,SubSymptom__c,Status,CommercialReference__c,ProductFamily__c,Family__c,SerialNumber__c,DateCode__c,Quantity__c,ProjectName__c  from Case WHERE ID=:caseID];       
        if(caseList.size() == 0)
        {
            caseList = [SELECT ID,CaseNumber,Account.Country__r.FrontOfficeOrganization__c,Account.Country__r.FrontOfficeOrganization__r.name,Contact.Country__r.FrontOfficeOrganization__r.Name,Contact.Country__r.FrontOfficeOrganization__c,SupportCategory__c,Symptom__c,SubSymptom__c,Status,CommercialReference__c,ProductFamily__c,Family__c,SerialNumber__c,DateCode__c,Quantity__c, ProjectName__c from Case WHERE ID=:NewComplaintRequest.Case__c];
        }
        
        if(caseList.size() == 1)
        {
            relatedCase = caseList[0];
        }
       
    }

    
    
    // Action to switch automatically to the edit page with pre-defaulted values
    public pagereference goToEditPage()
    {
        // Pre-populate fields of the edit page
        initializeComplaintRequest(NewComplaintRequest);
        
        
       // To prevent creation of complaint request on closed status on case. 
       if(relatedCase.id!=null && relatedCase.Status==Label.CLDEC13LCR30)
        {
           IsCancel=True;
           ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLDEC13LCR31));
           return null;
        }
        else if(relatedCase.id!=null && relatedCase.SupportCategory__c==null)
        {
           IsCancel=True;
           ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLJUN16I2P02));
           return null;
        }
        else if(relatedCase.id!=null && !Label.CLAPR14I2P03.contains(string.Valueof(relatedCase.SupportCategory__c)) && relatedCase.Symptom__c==null)
        {
           IsCancel=True;
           ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLAPR14I2P23));
           return null;
        }

         // Create a new edit page for the RMA 
        PageReference aNewComplaintRequestEditPage = new PageReference('/'+SObjectType.ComplaintRequest__c.getKeyPrefix()+'/e' );
        // Disable override
            aNewComplaintRequestEditPage.getParameters().put(Label.CL00690, Label.CL00691);
        
        system.debug('** related case'+relatedCase);
        if(relatedCase.id!=null)
        {
            isAuthorizedUser  = true;
            // Set the return to the parent Case (retURL)
            aNewComplaintRequestEditPage.getParameters().put(Label.CL00330, NewComplaintRequest.Case__c);   
             
            // Pre-default the parent case with the ID (_lkid) and the CaseNumber          
            aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR15, NewComplaintRequest.Case__c);
            aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR16, relatedCase.CaseNumber);
            //aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR24, '1' );  
            aNewComplaintRequestEditPage.getParameters().put('RecordType',Label.CLDEC13LCR27); // - setting the external recordtype
            aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR17, relatedCase.SupportCategory__c);  // category
            aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR18, relatedCase.Symptom__c);          // reason
            
            // we have to comment this line as subreason is no more on Case.
            //aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR19, relatedCase.SubSymptom__c);       // subreason
list<OrganizationStakeholdersForCR__c> OrgStakHolder= new list<OrganizationStakeholdersForCR__c>();
            OrgStakHolder =[select id, Organization__r.Name,Organization__c,Department__c,Owner__c from OrganizationStakeholdersForCR__c where Role__c = 'Complaint/Request creator' and Owner__c = : UserInfo.getUserId()]; //BR-7936 October 2015 release.
            if(OrgStakHolder.size()>0){
                aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR20, OrgStakHolder[0].Organization__c);  // TargetEntity
               aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR21, OrgStakHolder[0].Organization__r.Name);
               aNewComplaintRequestEditPage.getParameters().put(Label.CLOCT15I2P06,OrgStakHolder[0].Department__c);
            }else{
            
            aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR20, relatedCase.Account.Country__r.FrontOfficeOrganization__c);  // TargetEntity
            aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR21, relatedCase.Account.Country__r.FrontOfficeOrganization__r.Name);
             
                }
            aNewComplaintRequestEditPage.getParameters().put(Label.CLAPR14I2P04, relatedCase.ProjectName__c);
            
            // If its a Technical Complaint/Request we are supposed to prepopulate below fields - APRIL2014 Release - Sukumar - 06Feb2014
            if(relatedCase.SupportCategory__c==Label.CLAPR14I2P02)
            {
                 //aNewComplaintRequestEditPage.getParameters().put(Label.CLAPR14I2P04, relatedCase.CommercialReference__c);  // Commercial Reference Received
                 //prepopulation of CommericalReference Received, Family received is done from Insert Trigger (ApexClass: AP_LCR)
                 // If CommericalReference is null on Case don't prepopulate ProductFamily on ComplaintRequest - Not required
                 system.debug('****relatedCase.CommercialReference__c '+relatedCase.CommercialReference__c);
                 /*
                 if(relatedCase.CommercialReference__c!=null)
                 {
                     aNewComplaintRequestEditPage.getParameters().put(Label.CLAPR14I2P05, relatedCase.ProductFamily__c); // Product Family Received
                 }
                 */
                 //aNewComplaintRequestEditPage.getParameters().put(Label.CLAPR14I2P20, relatedCase.Family__c); // Family Received
                 
                 
                 aNewComplaintRequestEditPage.getParameters().put(Label.CLAPR14I2P06, relatedCase.SerialNumber__c);  // Serial Number
                 aNewComplaintRequestEditPage.getParameters().put(Label.CLAPR14I2P07, relatedCase.DateCode__c);  // Date Code
                 aNewComplaintRequestEditPage.getParameters().put(Label.CLAPR14I2P14, string.valueof(relatedCase.Quantity__c)); //Quantity Part/Product Received
            }
            
        } 
         
        else
        {
           system.debug('** CR from tab');
           Set<Id> listOrgStakHolderId = new Set<Id>{};
            map<Id,OrganizationStakeholdersForCR__c > mapOrgStakHolderId = new map<Id,OrganizationStakeholdersForCR__c >{};

            //aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR24, '0' ); 
            aNewComplaintRequestEditPage.getParameters().put('RecordType',Label.CLDEC13LCR28);  // setting the internal record type



            for(OrganizationStakeholdersForCR__c OrgStakHolder : [select id, Organization__r.Name,Department__c,Organization__c,Owner__c from OrganizationStakeholdersForCR__c where Role__c = 'Complaint/Request creator' and Owner__c = : UserInfo.getUserId()])
            {
                listOrgStakHolderId.add(OrgStakHolder.Owner__c);
                system.debug('**OrgStakHolder.Organization__r.Name'+OrgStakHolder.Organization__r.Name);
               //NewComplaintRequest.ReportingEntity__c =OrgStakHolder.BusinessRiskEscalationEntity__c;
              
               aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR20, OrgStakHolder.Organization__c);  // TargetEntity
               aNewComplaintRequestEditPage.getParameters().put(Label.CLDEC13LCR21, OrgStakHolder.Organization__r.Name);
               aNewComplaintRequestEditPage.getParameters().put(Label.CLOCT15I2P06,OrgStakHolder.Department__c);
              /* 
               aNewComplaintRequestEditPage.getParameters().put(Label.CLAPR14I2P19,UserInfo.getUserId());  // TargetEntity
               aNewComplaintRequestEditPage.getParameters().put(Label.CLAPR14I2P20, UserInfo.getUserName());
              */ 
              
           
            }
           if(!listOrgStakHolderId.isEmpty() && listOrgStakHolderId.contains(UserInfo.getUserId()) )
            {  
            isAuthorizedUser = True;
            }
            else {isAuthorizedUser = False;}
            
            system.debug('**** isAuthorizedUser '+ isAuthorizedUser);       
            if(isAuthorizedUser == false)
            {
                IsCancel=true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLDEC13LCR07));
                aNewComplaintRequestEditPage=null;
            }
        }
        

        return aNewComplaintRequestEditPage;                                                    
    }
    
    Public Pagereference doCancel()
    {
         Pagereference pr ;
         string CaseId = relatedCase.Id;
         if(CaseId!=null)
         {
         pr = new PageReference('/'+ CaseId);
         pr.setredirect(true);
         return pr;
         }
         else
         {
         pr = new PageReference(label.CLDEC13LCR23);
         pr.setredirect(true);
         return pr;
         }
    }               
}