public with sharing class FieloPRM_AP_ChallengeRewardTrigger {

    public static void createSegmentLevel(){
        /*
        list<FieloCH__ChallengeReward__c> triggernew = trigger.new;
        set<id> setidsBadge = new set<id>(); 
        for( FieloCH__ChallengeReward__c  challengereward : triggernew){
            if(challengereward.FieloCH__Badge__c != null){

                setidsBadge.add(challengereward.FieloCH__Badge__c);
            }
        }

        list<FieloEE__Badge__c> listBadge = [Select id,F_PRM_Segment__c, name FROM FieloEE__Badge__c WHERE id in: setidsBadge AND F_PRM_Segment__c = null ]; 
        
        String idRecordType = [Select id,developerName, sobjecttype  from RecordType WHERE sobjecttype   = 'FieloEE__RedemptionRule__c' and developerName = 'Manual'].id;

        map<id,FieloEE__RedemptionRule__c> mapIdBadgeSegment = new map<id,FieloEE__RedemptionRule__c>();
        for(FieloEE__Badge__c badge : listBadge){


            FieloEE__RedemptionRule__c segment = new FieloEE__RedemptionRule__c();
            segment.RecordTypeid = idRecordType;
            segment.name = 'LVL-' + badge.name.left(70);
            segment.FieloEE__Description__c = 'Segment for the level ' + badge.name ;
            segment.F_ExternalId__c = badge.id;
            mapIdBadgeSegment.put(badge.id, segment);

        }

        insert mapIdBadgeSegment.values();

        for(FieloEE__Badge__c badge : listBadge){     
            badge.F_PRM_Segment__c = mapIdBadgeSegment.get(badge.id).id;
        }

        update listBadge;*/
    }

    public static void setBadgeProgramLevel(){


       list<FieloCH__ChallengeReward__c> triggernew = trigger.new;
        set<id> setidsBadge = new set<id>(); 
        for( FieloCH__ChallengeReward__c  challengereward : triggernew){
            if(challengereward.FieloCH__Badge__c != null){
                setidsBadge.add(challengereward.FieloCH__Badge__c);
            }
        }

        list<FieloEE__Badge__c> listBadge = [Select id,F_PRM_Segment__c,F_PRM_Type__c, name, F_PRM_Challenge__c FROM FieloEE__Badge__c WHERE id in: setidsBadge AND F_PRM_Segment__c = null ]; 
        
        

        
        list<FieloEE__Badge__c> listBadgeUpdate = new list<FieloEE__Badge__c>();
        for(FieloEE__Badge__c badge : listBadge){


            badge.F_PRM_Type__c = 'Program Level';
            listBadgeUpdate.add(badge);

        }

        

        update listBadgeUpdate;

    }

    public static void validationChallengeReward(){


       list<FieloCH__ChallengeReward__c> triggernew = trigger.new;
        set<id> setidsBadge = new set<id>(); 
        for( FieloCH__ChallengeReward__c  challengereward : triggernew){
            if(challengereward.FieloCH__Badge__c != null){
                setidsBadge.add(challengereward.FieloCH__Badge__c);
            }
        }

        list<FieloCH__ChallengeReward__c> listChallengeReward = [Select id,FieloCH__Badge__c,FieloCH__Badge__r.Name,FieloCH__Challenge__c,FieloCH__Challenge__r.Name FROM FieloCH__ChallengeReward__c WHERE FieloCH__Badge__c in: setidsBadge]; 
        
        map<id,FieloCH__ChallengeReward__c> mapBadgeUsed = new map<id,FieloCH__ChallengeReward__c>();
        for(FieloCH__ChallengeReward__c chREward: listChallengeReward){
            mapBadgeUsed.put(chREward.FieloCH__Badge__c,chREward);
        }       

        for( FieloCH__ChallengeReward__c  challengereward : triggernew){
            if(challengereward.FieloCH__Badge__c != null){
                if(mapBadgeUsed.containsKey(challengereward.FieloCH__Badge__c)){
                    
                    String labelAux = Label.FieloPRM_CL_60_ErrorChallengeRewards;
                    List<String> parameters = new List<String>();
                    parameters.add(mapBadgeUsed.get(challengereward.FieloCH__Badge__c).FieloCH__Badge__r.Name);
                    parameters.add(mapBadgeUsed.get(challengereward.FieloCH__Badge__c).FieloCH__Challenge__r.Name);
                    
                    String msError = String.format(labelAux, parameters );
                    challengereward.addError(msError);
                }
            }
        }

    }
}