/*
    Author          : Santhanam Subramaniam
    Date Created    : 11/17/2011
    Description     : Test class for DMTStakeholderSearchController
*/

@isTest
private class DMTStakeholderSearchController_TEST {

    static testMethod void myUnitTest() {
        
        DMTStakeholder__c stackhldr = new DMTStakeholder__c(FirstName__c='Test',LastName__c='DMTStakeholder',Email__c='test@schneider-electric.com',ProjectRoles__c='Requester');
        insert stackhldr;
        
        PageReference vfPage = Page.DMTStakeholderSearch;
        Test.setCurrentPage(vfPage);
        DMTStakeholderSearchController DMTCtrl = new DMTStakeholderSearchController(new ApexPages.StandardController(stackhldr));
        Test.startTest();
        DMTCtrl.getSearchString();
        DMTCtrl.getQueryString();
        DMTCtrl.doSearchDMTStakeholder();
        DMTCtrl.continueCreation();
        Test.stopTest();
        
    }
}