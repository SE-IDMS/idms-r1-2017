global class Z3ResultClass{

/******************************************************************************************************************************
    Inner Class to hold the result  which will be returned as the Contact result of the webservice call.
******************************************************************************************************************************/
    
    global class ContactResult {
        //Variables 
        webservice String ContactID;
        webservice String SEContactID;
        webservice String ContactFirstName;
        webservice String ContactLastName;
        webservice String AccountID;
        webservice String ParentAccountID;
        webservice String JobTitle;
        webservice Boolean Inactive;
        webservice String JobFunction;
        webservice String Salutaion;
        webservice String WorkPhone;
        webservice String WorkPhoneExtension;
        webservice String WorkFax;
        webservice String ContactEmail;
        webservice String Country;
        webservice String City;
        webservice String Street;
        webservice String StateProvince;
        webservice String PostalZipCode;
        webservice DateTime LastModifiedDate;
        
        webservice Integer success;
        webservice String errorMessage;
    }
    global class ContactWithAgreementResult{
        //Variables 
        webservice String ContactID;
        webservice String SEContactID;
        webservice String ContactFirstName;
        webservice String ContactLastName;
        webservice String AccountID;
        webservice String ParentAccountID;
        webservice String ContactEmail;
        webservice String PackageTemplateIDs;
        
        webservice Integer success;
        webservice String errorMessage;
    }
    
    global class LegacyAccountInfoResult{
        //Variables 
        webservice String AccountID;
        webservice String LegacyNumber;
        
        webservice Integer success;
        webservice String errorMessage;
    }
    public static LegacyAccountInfoResult createLegacyAccountInfoResult(Integer Success, String ErrorMessage, LegacyAccount__c LegacyAccountResult){
            LegacyAccountInfoResult result = new LegacyAccountInfoResult();        
            if(LegacyAccountResult!= null){
                result.AccountID = LegacyAccountResult.Account__c;
                result.LegacyNumber = LegacyAccountResult.LegacyNumber__c;
            }                
            result.success = success;
            result.errorMessage = errorMessage; 
            return result;
    }
    
        global class ChangedDataResult{
        webservice String cType;
        webservice String mobjId;
        webservice String cData;
    }
    
    global class ChangesSinceResult{
        webservice ChangedDataResult[] lstChangedDataResult;
        webservice DateTime ServerTimestamp;
        webservice Integer success;
        webservice String errorMessage;
    }
    
    
    public static ContactResult createContactResult(Integer Success, String ErrorMessage, Contact resultContact){

        ContactResult result = new ContactResult();
        if(resultContact!= null){
            result.ContactID = resultContact.Id;
            result.SEContactID = resultContact.SEContactID__c;
            result.ContactFirstName = resultContact.FirstName;
            result.ContactLastName= resultContact.LastName;
            result.AccountID= resultContact.AccountId;
           if(resultContact.Account.ParentId != null)
                result.ParentAccountID= resultContact.Account.ParentId;//contactParentAcct;
            else
                result.ParentAccountID= resultContact.AccountId;//contactAcct;
            result.JobTitle = resultContact.JobTitle__c;
            result.Inactive = resultContact.InActive__c;
            result.JobFunction = resultContact.JobFunc__c;
            result.Salutaion = resultContact.Salutation;
            result.WorkPhone = resultContact.WorkPhone__c;
            result.WorkPhoneExtension = resultContact.WorkPhoneExt__c;
            result.WorkFax = resultContact.Fax;
            result.ContactEmail = resultContact.email;
            result.Country= resultContact.Country__r.Name;
            result.Street =  resultContact.Street__c;
            result.City = resultContact.City__c;
            result.StateProvince = resultContact.StateProv__r.Name;
            result.PostalZipCode = resultContact.ZipCode__c;
            result.LastModifiedDate = resultContact.lastmodifieddate;
        }                
        result.success = success;
        result.errorMessage = errorMessage; 
        return result;
    } 
    
/******************************************************************************************************************************
    Inner Class to hold the result  which will be returned as the Contract result of the webservice call.
******************************************************************************************************************************/
    
    global class ContractList
    {
        //Variables to hold lists and values
       
        webservice String ContractID;
        webservice String ContractNumber;
        webservice String ContractName;
        webservice String ContactName;
        webservice String AccountName;
        webservice DateTime EndDate;
        webservice String AccountCountry;
      
        //Variable to hold the status and result of Integration
        webservice Integer success;
        webservice String errorMessage;
         
    }       
    
    /******************************************************************************************************************************
    Inner Class to hold the result  which will be returned as the Account result of the webservice call.
******************************************************************************************************************************/
    
    global class AccountResult {
        //Variables 
        
        webservice String AccountID;        
        webservice String AccountName;
        webservice DateTime LastModifiedDate;            
        webservice String SEAccountId;
        webservice Integer success;
        webservice String errorMessage;
    }
   
    
    public static AccountResult createAccountResult(Integer Success, String ErrorMessage, Account resultAccount){

        AccountResult result = new AccountResult();
        if(resultAccount != null){
            If(resultAccount.ParentId != null){
                result.AccountID = resultAccount.ParentId;           
                result.AccountName = resultAccount.Parent.Name;                       
                result.LastModifiedDate = resultAccount.Parent.lastmodifieddate; 
                result.SEAccountId = resultAccount.Parent.SEAccountID__c;  
            }
            else{
                result.AccountID = resultAccount.Id;           
                result.AccountName = resultAccount.Name;                       
                result.LastModifiedDate = resultAccount.lastmodifieddate; 
                result.SEAccountId = resultAccount.SEAccountID__c;
            }           
        }                
            result.success = success;
            result.errorMessage = errorMessage; 
            return result;
    } 
    
      /******************************************************************************************************************************
    Inner Class to hold the result  which will be returned as the Package Template result of the webservice call.
******************************************************************************************************************************/
    
    global class ContactAgreementListResult {
        //Variables 
        
        webservice String PackageTemplateID;
        webservice String PackageTemplateName;
        webservice String ServiceNumber;
        webservice String SupportLevel;
        webservice boolean PortalAccess;        
        
        webservice Integer success;
        webservice String errorMessage;
    }
   
    
    public static ContactAgreementListResult createContactAgreementListResult (Integer Success, String ErrorMessage, PackageTemplate__c resultPackageTemplate, String productList){

        ContactAgreementListResult result = new ContactAgreementListResult();
        if(resultPackageTemplate != null){
            result.PackageTemplateID= resultPackageTemplate.Id;
            result.PackageTemplateName = resultPackageTemplate.Name;
            result.ServiceNumber= resultPackageTemplate.ServiceNumber__c;                       
            result.SupportLevel= resultPackageTemplate.SupportLevel__c;
            result.PortalAccess= resultPackageTemplate.PortalAccess__c;                     
        }                
            result.success = success;
            result.errorMessage = errorMessage; 
            return result;
    }
    
    
    
       /******************************************************************************************************************************
    Inner Class to hold the result  which will be returned as the Account result of the webservice call.
******************************************************************************************************************************/
    
    global class ChildAccountResult {
        //Variables 
        
        webservice String AccountID;
        webservice String Country;
        webservice String CountryCode;
        webservice String AccountName;
        webservice DateTime LastModifiedDate;
        webservice DateTime ParentAccLastModifiedDate;      
        webservice String SEAccountId;
        webservice Integer success;
        webservice String errorMessage;
    }
   
    /*
    public static ChildAccountResult createChildAccountResult(Integer Success, String ErrorMessage, Account resultAccount,Datetime Parentlastmodifieddate){

        ChildAccountResult result = new ChildAccountResult();
        if(resultAccount != null){
            result.AccountID = resultAccount.Id;
            result.Country = resultAccount.Country__c;
            result.CountryCode = resultAccount.Country__r.CountryCode__c;
            result.AccountName = resultAccount.Name;                       
            result.LastModifiedDate = resultAccount.lastmodifieddate;
            result.ParentAccLastModifiedDate = Parentlastmodifieddate; 
            result.SEAccountId = resultAccount.SEAccountID__c;
        }                
            result.success = success;
            result.errorMessage = errorMessage; 
            return result;
    } */
    
     public static ChildAccountResult createChildAccountResult(Integer Success, String ErrorMessage, Account resultAccount,Datetime Parentlastmodifieddate){

        ChildAccountResult result = new ChildAccountResult();
        if(resultAccount != null){
            result.AccountID = resultAccount.Id;
            result.Country = resultAccount.Country__c;
            result.CountryCode = resultAccount.Country__r.CountryCode__c;
            result.AccountName = resultAccount.Name;                       
            result.LastModifiedDate = resultAccount.lastmodifieddate;
            If(resultAccount.ParentId == null){
            result.ParentAccLastModifiedDate = resultAccount.lastmodifieddate; 
            }else{
            result.ParentAccLastModifiedDate = Parentlastmodifieddate; 
            }
            result.SEAccountId = resultAccount.SEAccountID__c;
        }                
            result.success = success;
            result.errorMessage = errorMessage; 
            return result;
    }
    
      /******************************************************************************************************************************
    Inner Class to hold the result  which will be returned as the Package Template result of the webservice call.
******************************************************************************************************************************/
    
    global class PackageTemplateResult {
        //Variables 
        
        webservice String PackageTemplateID;
        webservice String PackageTemplateName;
        webservice String ServiceNumber;
        webservice String SupportLevel;
        webservice boolean PortalAccess;
        webservice Decimal PackageDuration;
        webservice boolean DONOTPRINT;
        webservice boolean DONOTDELIVER;
        webservice boolean Renewable;
        webservice DateTime LastModifiedDate;
        webservice DateTime CreatedDate;    
        webservice String ProductList;
        webservice String LegacyTemplateId;
        webservice DateTime maxProdLastmodifieddate;
        
        webservice Integer success;
        webservice String errorMessage;
    }
   
    
    public static PackageTemplateResult createPackageTemplateResult(Integer Success, String ErrorMessage, PackageTemplate__c resultPackageTemplate, String productList, Datetime dtLastmodifieddate){

        PackageTemplateResult result = new PackageTemplateResult();
        if(resultPackageTemplate != null){
            result.PackageTemplateID= resultPackageTemplate.Id;
            result.PackageTemplateName = resultPackageTemplate.Name;
            result.ServiceNumber= resultPackageTemplate.ServiceNumber__c;                       
            result.SupportLevel= resultPackageTemplate.SupportLevel__c;
            result.PortalAccess= resultPackageTemplate.PortalAccess__c;
            result.PackageDuration= resultPackageTemplate.PackageDuration__c;
            result.DONOTPRINT= resultPackageTemplate.DoNotPrint__c;
            result.DONOTDELIVER= resultPackageTemplate.DoNotDeliver__c;                       
            result.Renewable = resultPackageTemplate.Renewable__c;
            result.CreatedDate = resultPackageTemplate.CreatedDate;                       
            result.LastModifiedDate = resultPackageTemplate.LastModifiedDate;
            result.LegacyTemplateId = resultPackageTemplate.LegacyTemplateId__c;
            result.ProductList= productList;
            result.maxProdLastmodifieddate = dtLastmodifieddate;          
        }                
            result.success = success;
            result.errorMessage = errorMessage; 
            return result;
    }
    
    global class ContractResult
    {
        webservice list<contractlist> lstContract;
        webservice Integer ErrorCode;
        webservice String ErrorMessage;
    }
    
    public static ContractResult createContractResult(Integer Success, String ErrorMessage,map<Id,Package__c> mapContractPackage)
    {
        ContractResult result = new ContractResult();
        result.lstContract = new list<contractlist>();
        
        if(mapContractPackage!= null && !mapContractPackage.isEmpty()) 
        {
            for(Id contractId :  mapContractPackage.keySet())
            {
                               
                contractlist contract = new contractlist();
                contract.ContractID = contractId ;
                contract.ContractNumber = mapContractPackage.get(contractId ).Contract__r.contractnumber;
                contract.ContractName = mapContractPackage.get(contractId ).Contract__r.name;
                contract.ContactName = mapContractPackage.get(contractId ).Contract__R.contactname__r.Name;
                contract.AccountName = mapContractPackage.get(contractId ).Contract__r.Account.Name;
                contract.EndDate = mapContractPackage.get(contractId ).enddate__c;
                contract.AccountCountry = mapContractPackage.get(contractId ).Contract__r.Account.postalcountry__r.Name;
                result.lstContract.add(contract);
            }
        }
        result.ErrorCode = success;
        result.errorMessage = errorMessage;
        return result;
    }
    
    global class ContractNoteResult
    {
        webservice Integer ErrorCode;
        webservice String ErrorMessage;
    }
    
    public static ContractNoteResult createNoteResult(Integer Success, String ErrorMessage)
    {
        ContractNoteResult result = new ContractNoteResult();
        result.ErrorCode = Success;
        result.ErrorMessage = ErrorMessage;
        return result;
    }
    
    global class CustomerContract
    {
        webservice String ContractID;
        webservice DateTime EndDate;
        webservice String SupportLevel;
        webservice String ContactFirstName;
        webservice String ContactLastName;
        webservice String ParentAccountName;
        webservice String AcocuntName;
        
    }
    
    global class CustomerContractResult
    {
        webservice list<CustomerContract> lstCustomerContract;
        webservice Integer ErrorCode;
        webservice String ErrorMessage;
    }
    public static CustomerContractResult createCustomerContract(Integer Success, String ErrorMessage,map<Id,Package__c> mapIdContract, map<Id, Date> mapContractSupportLevel,String contactFirstName, String contactLastName)
    {
        CustomerContractResult result = new CustomerContractResult();
        result.lstCustomerContract = new list<CustomerContract>();
        if(mapIdContract!= null && !mapIdContract.isEmpty())
        {
            for(Id ContractId : mapIdContract.keySet())
            {
                CustomerContract resultContract = new CustomerContract();
                resultContract.ContractID = ContractId ;
                resultContract.EndDate = mapContractSupportLevel.get(ContractId);
                resultContract.SupportLevel =  mapIdContract.get(ContractId ).Contract__r.SupportContractLevel__c;
                resultContract.ContactFirstName = contactFirstName;//mapIdContract.get(ContractId ).Contract__r.ContactName__r.FirstName;
                resultContract.ContactLastName = contactLastName;//mapIdContract.get(ContractId ).Contract__r.ContactName__r.LastName;
                resultContract.ParentAccountName = mapIdContract.get(ContractId ).Contract__r.Account.Parent.Name;
                resultContract.AcocuntName = mapIdContract.get(ContractId ).Contract__r.Account.Name;
                result.lstCustomerContract.add(resultContract);
            }
        }
        result.ErrorCode = Success;
        result.ErrorMessage = ErrorMessage;
        return result;
    }
     

}