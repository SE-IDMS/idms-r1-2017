@isTest
public class AP_PRMRegistration_TEST {

	static PRMCountry__c prmCountry;
	static CountryChannels__c prmChannel;

	@testSetup
	public static void InitializeTestData () {
        Country__c testCountry = new Country__c(Name   = 'India', CountryCode__c = 'IN', InternationalPhoneCode__c = '91', Region__c = 'APAC');
        INSERT testCountry;
        
        StateProvince__c testStateProvince = new StateProvince__c(Name = 'Karnataka', Country__c = testCountry.Id, CountryCode__c = testCountry.CountryCode__c, StateProvinceCode__c = '10');
        INSERT testStateProvince;

		ClassificationLevelCatalog__c bt = new ClassificationLevelCatalog__c(Name = 'PB', ClassificationLevelName__c = 'Panel Builder', Active__c = true);		
		INSERT bt;

		ClassificationLevelCatalog__c af = new ClassificationLevelCatalog__c(Name = 'PB4', ClassificationLevelName__c = 'Local/Small Panel Builder', ParentClassificationLevel__c = bt.Id, Active__c = true);		
		INSERT af;

		prmCountry = new PRMCountry__c (Name = 'India Test', Country__c = testCountry.Id, CountrySupportEmail__c = 'test@test.com', CountryPortalEnabled__c = true, PLDatapool__c = 'in_IN', SupportedLanguage1__c = 'en_IN');
		INSERT prmCountry;

		prmChannel = new CountryChannels__c(PRMCountry__c = prmCountry.Id, Channel__c = bt.Id, SubChannel__c = af.Id, AllowPrimaryToManage__c = true, Active__c = true);
		INSERT prmChannel;
	}

	public static testMethod void NewContactNewCompanyTest () {

        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String usrFedId = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        
        Blob b2 = Crypto.GenerateAESKey(128);
        String h2 = EncodingUtil.ConvertTohex(b2);
        String cmpFedId = h2.SubString(0,8)+ '-' + h2.SubString(8,12) + '-' + h2.SubString(12,16) + '-' + h2.SubString(16,20) + '-' + h2.substring(20);

		AP_PRMRegistrationInformation newregInfo = new AP_PRMRegistrationInformation();
	    newregInfo.email = 'test.release.180@yopmail.com';
	    newregInfo.firstName = 'Test';
	    newregInfo.lastName = 'Class-180';
	    newregInfo.businessType = 'PB';
	    newregInfo.areaOfFocus = 'PB4';
	    newregInfo.userLanguage = 'en';
	    newregInfo.phoneNumber = '324234324';

	    newregInfo.federationId = usrFedId;
	    newregInfo.countryCode = 'IN';
	    newregInfo.companyName = 'Registration Validation.1';
	    newregInfo.companyPhone = '324234324';
	    newregInfo.companyFederatedId = cmpFedId;
	    newregInfo.companyAddress1 = 'Elnath Building';
		newregInfo.companyAddress2 = 'B Wing';
	    newregInfo.companyCity = 'Bengaluru';
	    newregInfo.companyZipcode = '560103';
	    newregInfo.companySpeciality = '';
	    newregInfo.employeeSize = '';
	    newregInfo.annualSales = '';
	    newregInfo.companyWebsite = 'www.test.com';
	    newregInfo.includeCompanyInfoInPublicSearch = true;
	    newregInfo.IAmSelfEmployed = false;
	    newregInfo.companyHeaderQuarters = false;

		System.debug('*** Start: NewContactNewCompanyTest' + newregInfo);
		AP_PRMRegistrationService.CreateRegistration (newregInfo);
	}

	public static testMethod void NewContactNewCompanyTest2 () {

        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String usrFedId = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        
        Blob b2 = Crypto.GenerateAESKey(128);
        String h2 = EncodingUtil.ConvertTohex(b2);
        String cmpFedId = h2.SubString(0,8)+ '-' + h2.SubString(8,12) + '-' + h2.SubString(12,16) + '-' + h2.SubString(16,20) + '-' + h2.substring(20);

		AP_PRMRegistrationInformation newregInfo = new AP_PRMRegistrationInformation();
	    newregInfo.email = 'test.release.180@yopmail.com';
	    newregInfo.firstName = 'Test';
	    newregInfo.lastName = 'Class-180';
	    newregInfo.businessType = 'PB';
	    newregInfo.areaOfFocus = 'PB4';
	    newregInfo.userLanguage = 'en';
	    newregInfo.phoneNumber = '324234324';

	    newregInfo.federationId = usrFedId;
	    newregInfo.countryCode = 'ID';
	    newregInfo.companyName = 'Registration Validation.1';
	    newregInfo.companyPhone = '324234324';
	    newregInfo.companyFederatedId = cmpFedId;
	    newregInfo.companyAddress1 = 'Elnath Building';
		newregInfo.companyAddress2 = 'B Wing';
	    newregInfo.companyCity = 'Bengaluru';
	    newregInfo.companyZipcode = '560103';
	    newregInfo.companySpeciality = '';
	    newregInfo.employeeSize = '';
	    newregInfo.annualSales = '';
	    newregInfo.companyWebsite = 'www.test.com';
	    newregInfo.includeCompanyInfoInPublicSearch = true;
	    newregInfo.IAmSelfEmployed = false;
	    newregInfo.companyHeaderQuarters = false;

		System.debug('*** Start: NewContactNewCompanyTest' + newregInfo);
		AP_PRMRegistrationService.CreateRegistration (newregInfo);
	}

	public static testMethod void NewContactNewCompanyTest3 () {

        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String usrFedId = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        
        Blob b2 = Crypto.GenerateAESKey(128);
        String h2 = EncodingUtil.ConvertTohex(b2);
        String cmpFedId = h2.SubString(0,8)+ '-' + h2.SubString(8,12) + '-' + h2.SubString(12,16) + '-' + h2.SubString(16,20) + '-' + h2.substring(20);

		AP_PRMRegistrationInformation newregInfo = new AP_PRMRegistrationInformation();
	    newregInfo.email = 'test.release.180@yopmail.com';
	    newregInfo.firstName = 'Test';
	    newregInfo.lastName = 'Class-180';
	    newregInfo.businessType = 'PB';
	    newregInfo.areaOfFocus = 'PB4';
	    newregInfo.userLanguage = 'en';
	    newregInfo.phoneNumber = '324234324';
	    newregInfo.jobTitle = 'AA';
	    newregInfo.jobFunction = 'AB';

	    newregInfo.federationId = usrFedId;
	    newregInfo.countryCode = 'IN';
	    newregInfo.companyName = 'Registration Validation.1';
	    newregInfo.companyPhone = '324234324';
	    newregInfo.companyFederatedId = cmpFedId;
	    newregInfo.companyAddress1 = 'Elnath Building';
		newregInfo.companyAddress2 = 'B Wing';
	    newregInfo.companyCity = 'Bengaluru';
	    newregInfo.companyZipcode = '560103';
	    newregInfo.companySpeciality = '';
	    newregInfo.employeeSize = '';
	    newregInfo.annualSales = '';
	    newregInfo.companyWebsite = 'www.test.com';
	    newregInfo.includeCompanyInfoInPublicSearch = true;
	    newregInfo.IAmSelfEmployed = false;
	    newregInfo.companyHeaderQuarters = false;

		System.debug('*** Start: NewContactNewCompanyTest' + newregInfo);
		AP_PRMRegistrationService.CreateRegistration (newregInfo);
	}

	public static testMethod void NewContactNewCompanyTest4 () {

        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String usrFedId = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        
        Blob b2 = Crypto.GenerateAESKey(128);
        String h2 = EncodingUtil.ConvertTohex(b2);
        String cmpFedId = h2.SubString(0,8)+ '-' + h2.SubString(8,12) + '-' + h2.SubString(12,16) + '-' + h2.SubString(16,20) + '-' + h2.substring(20);

		AP_PRMRegistrationInformation newregInfo = new AP_PRMRegistrationInformation();
	    newregInfo.firstName = 'Test';
	    newregInfo.lastName = 'Class-180';
	    newregInfo.businessType = 'PB';
	    newregInfo.areaOfFocus = 'PB4';
	    newregInfo.userLanguage = 'en';
	    newregInfo.phoneNumber = '324234324';
	    newregInfo.jobTitle = 'AA';
	    newregInfo.jobFunction = 'AB';

	    newregInfo.federationId = usrFedId;
	    newregInfo.countryCode = 'IN';
	    newregInfo.companyName = 'Registration Validation.1';
	    newregInfo.companyPhone = '324234324';
	    newregInfo.companyAddress1 = 'Elnath Building';
		newregInfo.companyAddress2 = 'B Wing';
	    newregInfo.companyCity = 'Bengaluru';
	    newregInfo.companyZipcode = '560103';
	    newregInfo.companySpeciality = '';
	    newregInfo.employeeSize = '';
	    newregInfo.annualSales = '';
	    newregInfo.companyWebsite = 'www.test.com';

		System.debug('*** Start: NewContactNewCompanyTest' + newregInfo);
		AP_PRMRegistrationService.CreateRegistration (newregInfo);
	}

	public static testMethod void AppConstantsTest () {
		AP_PRMAppConstants constants = new AP_PRMAppConstants();
		System.debug('*** 1' + AP_PRMAppConstants.CNTMODE_NEW);
		System.debug('*** 1' + AP_PRMAppConstants.CNTMODE_EXT);
		System.debug('*** 1' + AP_PRMAppConstants.CNTMODE_UIMSEXT);
		System.debug('*** 1' + AP_PRMAppConstants.ACCMODE_NEW);
		System.debug('*** 1' + AP_PRMAppConstants.ACCMODE_EXT);
		System.debug('*** 1' + AP_PRMAppConstants.APPL_PRTL);
		System.debug('*** 1' + AP_PRMAppConstants.APPL_POMP);
		System.debug('*** 1' + AP_PRMAppConstants.OPERATION_OK);
		System.debug('*** 1' + AP_PRMAppConstants.OPERATION_FAIL);
		
		AP_PRMAppConstants.RegistrationValidationErrors err1 = AP_PRMAppConstants.RegistrationValidationErrors.ERR_EMAIL_BLANK; 
	} 

	public static testMethod void GetRegistrationInfoTest () {
		AP_PRMRegistrationService.GetRegistrationInfo();	
	}
}