public with sharing class PRJ_ProjectTriggers {
      /*==========================================
      Author: S?bastien CAUCHETEUX
      Date: 01/02/11
      Description: Class holding all the trigger
      event methods
      ==========================================*/
      
      
      public static final Map<Id,RecordType> recordTypeMap=new Map<Id,RecordType>([select id, Name, DeveloperName from RecordType where sObjectType='PRJ_ProjectReq__c' and IsActive = true]);
      //public static final ID adminProfileId = [select Id from Profile where Name='System Administrator'].Id;

      /*============================
      Project BEFORE UPDATE
      ==============================*/
      
      public static void projectBeforeUpdate(Map<Id,PRJ_ProjectReq__c> newMap,Map<Id,PRJ_ProjectReq__c> oldMap)
      {
        list<PRJ_ProjectReq__c> projectListToProcessCONDITION1 = new list<PRJ_ProjectReq__c>();
        list<PRJ_ProjectReq__c> projectListToProcessCONDITION2 = new list<PRJ_ProjectReq__c>();
        list<PRJ_ProjectReq__c> projectListToProcessCONDITION3 = new list<PRJ_ProjectReq__c>();
        list<PRJ_ProjectReq__c> projectListToProcessCONDITION4 = new list<PRJ_ProjectReq__c>();
     
        for (ID projId:newMap.keyset())
        {
            PRJ_ProjectReq__c newProj = newMap.get(projId);
            PRJ_ProjectReq__c oldProj = oldMap.get(projId);
            
            /* If the record type changes, assign the old record type label to the field - Previous Step.
                This field is used as a merge field in email notifications sent to project actors on status change. */
            
              if (recordTypeMap.containskey(oldProj.RecordTypeId))
              {
                  if (newProj.RecordTypeId <> oldProj.RecordTypeId)
                  {
                      newProj.PreviousStep__c = recordTypeMap.get(oldProj.RecordTypeId).Name;
                  }
                  
                  if(newProj.NextStep__c == System.Label.DMT_StatusProjectOpen) 
                  {
                      newProj.TECHActualStep__c = System.Label.DMT_StatusInProgress;
                  }
                  else if(newProj.NextStep__c == System.Label.DMT_StatusCancelled || newProj.NextStep__c == System.Label.DMT_StatusPostponed)
                  {
                      if(recordTypeMap.get(newProj.RecordTypeId).DeveloperName == System.Label.DMT_RecordTypeProjectFinished)
                      {
                          newProj.TECHActualStep__c = System.Label.DMT_StatusInProgress;
                      }    
                      else
                      {
                          newProj.TECHActualStep__c = recordTypeMap.get(newProj.RecordTypeId).Name;
                      }
                  }
                  else
                  {
                      newProj.TECHActualStep__c = recordTypeMap.get(newProj.RecordTypeId).Name;
                  }
                
              }

              /* Added for Feb 2014 release by Divya
                Populate the IPO Managing Org based on Bus/Tech Domain*/

             system.debug('Bus Tech Domain:' + newProj.BusinessTechnicalDomain__c);
              list<ManagingOrgSetting__c> ManagingOrgSetting = ManagingOrgSetting__c.getall().Values();
                 for(integer i =0; i<ManagingOrgSetting.size();i++)
                 {
                     if( ManagingOrgSetting[i].Business_Technical_Domain__c == newProj.BusinessTechnicalDomain__c)
                         newProj.ManagingBusinessOrganization__c = ManagingOrgSetting[i].Managing_Business_Organization__c;
                 }
               system.debug('IPO Managing Org:' + newProj.ManagingBusinessOrganization__c);

             /* Added by Divya
                Only the Domain owner can change the Bus/Tech Domain after Valid Gate */

            if(newProj.NextStep__c != System.Label.DMT_StatusDraft && newProj.NextStep__c == System.Label.DMT_StatusCreated && (oldProj.BusinessTechnicalDomain__c != newProj.BusinessTechnicalDomain__c))
            {
                System.debug('<<<<<<<<<<<<<<>>>>>>>>>>>>>> Inside the loop');
                System.debug('<<<<<<<<<<<<<<>>>>>>>>>>>>>>' + newProj.NextStep__c );
                Id user = UserInfo.getUserId();
                Id profileId = UserInfo.getProfileId();
                String ProfileName;
                
              if(!Test.isRunningTest())
                 ProfileName = [Select Id, Name from Profile where Id =:profileId].Name;
              else
                 ProfileName = 'TestProfile';
             
                if(ProfileName != 'System Administrator')
                {
                    list<DMTAuthorizationMasterData__c> dList = ([SELECT AuthorizedUser1__c,AuthorizedUser2__c,AuthorizedUser3__c,AuthorizedUser4__c,AuthorizedUser5__c,AuthorizedUser6__c,AuthorizedUser7__c,AuthorizedUser8__c,AuthorizedUser9__c,AuthorizedUser10__c FROM DMTAuthorizationMasterData__c WHERE BusinessTechnicalDomain__c = :oldProj.BusinessTechnicalDomain__c]);
                    
                    Set<Id> Ids = new Set<Id> ();
                    for(DMTAuthorizationMasterData__c d :dList)
                    {
                        Ids.add(d.AuthorizedUser1__c);
                        Ids.add(d.AuthorizedUser2__c);
                        Ids.add(d.AuthorizedUser3__c);
                        Ids.add(d.AuthorizedUser4__c);
                        Ids.add(d.AuthorizedUser5__c);
                        Ids.add(d.AuthorizedUser6__c);
                        Ids.add(d.AuthorizedUser7__c);
                        Ids.add(d.AuthorizedUser8__c);
                        Ids.add(d.AuthorizedUser9__c);
                        Ids.add(d.AuthorizedUser10__c);
                        
                    }
                    system.debug('<<<<<<<<<<<<<<>>>>>>>>>>>>> ' + Ids.Size() + Ids);
                   
                    if(!Ids.Contains(user))
                    {
                        System.debug('<<<<<<<<<<<<>>>>>>>>>>>>> not the domain owner');
                        PRJ_ProjectReq__c pr = newMap.get(newProj.Id);
                     if(!Test.isRunningTest()){ 
                        pr.BusinessTechnicalDomain__c.addError('Cannot change the Business/Technical Domain after VALID GATE. Please contact the Domain Owner');
                       
                    }   
                   }                    
              }
            }

            /* Populate the next approver */

            if(newProj.NextStep__c != 'ARCHIVE')
            {
                projectListToProcessCONDITION1.add(newProj);
            }
            if(newProj.NextStep__c == System.Label.DMT_StatusProjectOpen && newProj.NextStep__c <> oldProj.NextStep__c)
            {
                projectListToProcessCONDITION3.add(newProj);
            }
            if(newProj.PRJ_ValidGateNotification__c == true && newProj.PRJSentValidGateNotification__c == false)
            {
                projectListToProcessCONDITION2.add(newProj);
            }
            if(newProj.PRJFAGateNotification__c == true && newProj.PRJSentFAGateNotification__c == false)
            {
                projectListToProcessCONDITION4.add(newProj);
            }
            
        }
        if(projectListToProcessCONDITION1.size()>0)
        {
            PRJ_ProjectUtils.GetNextAuthorizedUser(projectListToProcessCONDITION1);
        }
          
        if (projectListToProcessCONDITION3.size()>0) 
        {
            PRJ_ProjectUtils.verifyHFMCodeEnteredandSelectedInEnvelop(projectListToProcessCONDITION3);   
        }
        if(projectListToProcessCONDITION2.size() > 0){
            PRJ_ProjectUtils.sendValidGateNotification(projectListToProcessCONDITION2);
          }
          
        if(projectListToProcessCONDITION4.size() > 0){
            PRJ_ProjectUtils.sendFAGateNotification(projectListToProcessCONDITION4);
          }
            
        if(newMap.values().size()>0)
        {
            PRJ_ProjectUtils.updateProjectRequestFields(newMap.values());
            PRJ_ProjectUtils.assignProposedIPOProgramToActualIPOProgram(newMap,oldMap); 
        }
     }
      
      /*============================
      Project AFTER UPDATE
      ==============================*/
      public static void projectAfterUpdate(Map<Id,PRJ_ProjectReq__c> newMap,Map<Id,PRJ_ProjectReq__c> oldMap)
      {
        list<PRJ_ProjectReq__c> projectListToProcessCONDITION1 = new list<PRJ_ProjectReq__c>();   
        list<PRJ_ProjectReq__c> projectListToProcessCONDITION2 = new list<PRJ_ProjectReq__c>();  
        list<PRJ_ProjectReq__c> projectListToProcessCONDITION3 = new list<PRJ_ProjectReq__c>();  
        list<PRJ_ProjectReq__c> projectListToProcessCONDITION4 = new list<PRJ_ProjectReq__c>();  
        for (ID projId:newMap.keyset())
        {          
          PRJ_ProjectReq__c newProj = newMap.get(projId);
          PRJ_ProjectReq__c oldProj = oldMap.get(projId);
                    
          if(newProj.NextStep__c == System.Label.DMT_StatusCreated && (newProj.BusinessTechnicalDomain__c <> oldProj.BusinessTechnicalDomain__c) && (newProj.NextStep__c == oldProj.NextStep__c) && (newProj.WorkType__c == 'Master Project' || newProj.WorkType__c == 'Sub Project'))             
          {
            System.Debug('newProj.Status inner: ' + newProj.NextStep__c);
            System.Debug('oldProj.Status inner: ' + oldProj.NextStep__c);
            System.Debug('newProj.BusinessTechnicalDomain__c inner: ' + newProj.BusinessTechnicalDomain__c);
            System.Debug('oldProj.BusinessTechnicalDomain__c inner: ' + oldProj.BusinessTechnicalDomain__c);
                    
            projectListToProcessCONDITION2.add(newProj);
          }  
          
         if ((newProj.NextStep__c != oldProj.NextStep__c) && (newProj.WorkType__c == 'Master Project' || newProj.WorkType__c == 'Sub Project'))
         {
          system.debug('Project Added ' + newProj);
           projectListToProcessCONDITION1.add(newProj);
          }
          
          
        }
        if(projectListToProcessCONDITION2.size() > 0)
          PRJ_ProjectUtils.sendCreatedNotification(projectListToProcessCONDITION2,'Change of Domain Owner');
          
        if(projectListToProcessCONDITION1.size() > 0){
          system.debug('&&&&&&&&&& Notification Block ' );
          PRJ_ProjectUtils.sendStepChangeNotification(projectListToProcessCONDITION1);
          }
          
      }
      
      
      
      
      
      // Ramakrishna Singara Added Code for October Release
      //Start
      /*============================
      Project BEFORE INSERT
      ==============================*/
      
      public static void projectBeforeInsert(List<PRJ_ProjectReq__c> PrjReqListToProcessCONDITION1){
         PRJ_ProjectUtils.ipoProgBeforeInsert(PrjReqListToProcessCONDITION1);
         PRJ_ProjectUtils.GetNextAuthorizedUser(PrjReqListToProcessCONDITION1);
      }
      // End
      
      //START Srikant Joshi Added Code for Febraury Release 2013
      
      public static void projectAfterInsert(List<PRJ_ProjectReq__c> PrjReqListToProcessCONDITION1){
     //    PRJ_ProjectUtils.projectRequestDmtFEAutoCreation(PrjReqListToProcessCONDITION1);
      }
      // End

    }