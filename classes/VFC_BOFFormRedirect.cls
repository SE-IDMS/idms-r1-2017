/*
    Author: Bhuvana Subramaniyan
    Description: The class redirects user to BOF form if the criteria is satisfied
*/
public with sharing class VFC_BOFFormRedirect 
{
    //Variable Declaration
    public OPP_QuoteLink__c quoteLnk{get;set;}
    public String pgMsg{get;set;}
    public boolean isBOF{get;set;}
    public string bofId{get;set;}
    
    public List<BudgetaryOfferForm__c> bofForm = new List<BudgetaryOfferForm__c>();    
    public Pagereference pg;
    
    //Constructor
    public VFC_BOFFormRedirect(ApexPages.StandardController controller) 
    {
        quoteLnk = (OPP_QuoteLink__c)controller.getRecord();
        //Queries the Quote Link
        quoteLnk = [select TypeOfOffer__c,Name,OpportunityName__r.LeadingBusiness__c, OpportunityName__r.MarketSegment__c,createdDate from OPP_QuoteLink__c where Id=: quoteLnk.Id];
        
        //Queries the Budgetary Offer form
        bofForm = [select Id,QuoteLink__c from BudgetaryOfferForm__c where QuoteLink__c =: quoteLnk.Id];
                
    }
    //Action method invoked when the VFP_BOFFormRedirect page loads
    public pagereference bofForm()
    {
        //Checks if the Quote Link is created after Apr 2014
        Datetime quteDateTime = DateTime.valueOf(Label.CLAPR14SLS62);
        if(quoteLnk.createdDate > quteDateTime)
        {
            //Checks if the Quote Link is Budgetary or Non Budgetary and renders the message accordingly
            if((quoteLnk.TypeOfOffer__c == Label.CLAPR14SLS14 || quoteLnk.TypeOfOffer__c == Label.CLAPR14SLS15 || quoteLnk.TypeOfOffer__c == Label.CLAPR14SLS16)  || (quoteLnk.TypeOfOffer__c==null))
            {        
                pgMsg =Label.CLAPR14SLS11;            
            }
            else if((quoteLnk.TypeOfOffer__c == Label.CLAPR14SLS10))
            {            
                //Redirects to BOF Detail page
                if((!(bofForm.isEmpty())) && bofForm[0].Id!=null)
                {
                    bofId = bofForm[0].Id;   
                    pg = new pagereference('/'+bofId);        
                }
                else
                {
                    //Redirects to New BOF Form Page prepopulating Quote Link
                    pgMsg = Label.CLAPR14SLS12;
                    isBOF = true;            
                    bofId = BudgetaryOfferForm__c.SObjectType.getDescribe().getKeyPrefix()+'/e?retURL=%2F'+quoteLnk.Id+'&'+Label.CLAPR14SLS13+'='+quoteLnk.Name +'&'+Label.CLAPR14SLS13+'_lkid='+quoteLnk.Id+'&' +Label.CLAPR14SLS55+'='+quoteLnk.OpportunityName__r.LeadingBusiness__c+'&' +Label.CLAPR14SLS56+'='+quoteLnk.OpportunityName__r.MarketSegment__c;                                           
                }          
            }
        }
        else
        {
            pgMsg = Label.CLAPR14SLS31;
        }
        return pg;             
    }    
}