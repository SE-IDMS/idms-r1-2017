@isTest(SeeAllData=true)
Public class LocationBeforeUpdate_test {

    static testMethod void testloc(){
 test.startTest();
Country__c testCountry = Utils_TestMethods.createCountry();   
     insert testCountry; 

StateProvince__c sp = new StateProvince__c(Name='test',
                                           StateProvinceCode__c='tes',
                                          CountryCode__c='st',
                                          Country__c=testCountry.Id);  
insert sp;                                         
Account acc = Utils_TestMethods.createAccount();
              acc.Street__c='New Street';
              acc.POBox__c='XXX';
              acc.ZipCode__c='12345';
              acc.City__c='test';
              acc.Country__c=testCountry.Id;
              acc.StateProvince__c=sp.Id;
              insert acc;
        Account acc1 = new Account();
        acc1.Name='testname';
        acc1.City__c='test123';
        insert acc1;
          
 
 SVMXC__Site__c loc = new SVMXC__Site__c(Name=acc.Name,
                                       SVMXC__Account__c=acc.Id,
                                       SVMXC__Street__c=acc.Street__c,
                                       SVMXC__Country__c=testCountry.Id,
                                       SVMXC__Zip__c=acc.ZipCode__c,
                                       SVMXC__City__c=acc.Country__c,
                                       //SVMXC__City__c=acc.Country__c,
                                       StateProvince__c=acc.StateProvince__c,
                                         PrimaryLocation__c=false
                                      //SVMXC__City__c=acc.Country__c
                                      // SVMXC__Parent__c=loc.Id
                                        );

Insert loc;
     loc.SVMXC__Account__c=acc1.id; 
     loc.PrimaryLocation__c=true;   
update loc;

//if(SVMXC__Parent__c != null)
//{
SVMXC__Site__c l = new SVMXC__Site__c(
                                      SVMXC__Parent__c=loc.Id,
                                       SVMXC__Account__c=loc.SVMXC__Account__c,
                                       SVMXC__Street__c =loc.SVMXC__Street__c,
                                       SVMXC__Country__c = loc.SVMXC__Country__c,
                                       SVMXC__Zip__c=loc.SVMXC__Zip__c,
                                       StateProvince__c = loc.StateProvince__c,
                                       SVMXC__City__c= loc.SVMXC__City__c
                                      );

//}
insert l;
update l ;
}}