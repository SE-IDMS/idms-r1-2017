/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :27-July-2015
    Modification Log    : 
    Description         : Test Method for DMT Notes & Attachment Class and Remedy Redirect Class
*/

@isTest(SeeAllData=true)  
private class DMTVFPNotesAttachment_Test{
    static testMethod void NotesAttachmentTest() {
    
       User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('DMTX');
      
        
        System.runAs(runAsUser){
        
        
          //Use the PageReference Apex class to instantiate 
        PageReference pageRef = Page.DMT_NotesAttachments;
       
       //In this case, the Visualforce page named 'DMT_NotesAttachments' is the starting point of this Test method. 
        Test.setCurrentPage(pageRef);
        
        DMT_NotesAttachement controller = new DMT_NotesAttachement(); 
        
         controller.FilterChange();
         controller.getReportFilter();
         controller.AllNotesAndAttachments();
        }
       }
       
  static testMethod void RemedyRedirectTest() {
    
       User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('DMTY');      
    
 
        System.runAs(runAsUser){
        
        
        
          //Use the PageReference Apex class to instantiate 
        PageReference pageRef = Page.DMT_EndUserSupport;
       
       //In this case, the Visualforce page named 'DMT_EndUserSupportRedirect' is the starting point of this Test method. 
        Test.setCurrentPage(pageRef);
        
         DMT_EndUserSupportRedirect  controller = new DMT_EndUserSupportRedirect (); 
        
         controller.PgRedirectOnLoad();
         
        
        }
       }
      }