/*
Author:Sid
Purpose: Default handler class for the visualforce page VFP_ContactSearch
*/
without sharing
global class VFC_ContactSearch2 {
    
    global VFC_ContactSearch2 () {
           // If the user is using an advanced version of Internet explorer, the line below enforces that it continues to use those features.
           Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge');
      }
  /*
    Method Name: linkToCase
    Parameters: String - caserecord - serialized/strigified version of the case record.    
    purpose: to update the account information on the case.
  */
@RemoteAction
    global static Database.SaveResult linktocase(String caserecord){
        Case caserec=(Case)JSON.deserialize(caserecord, Case.class);
        System.debug(caserec);
        return Database.update(caserec,true);
    }
}