public class VFC_PRMHome{

    public String RedirectURL{set;}
    
    public VFC_PRMHome() {
    }
    
    public pageReference UpdatePartnerRedirectURL(){
   
        //User Id -> u.id
        //
        String retURL = '';
        ID PartnerID = UserInfo.getUserId(); 
        List<PartnerRequest__c> lstLoginPartner = new List<PartnerRequest__c>();  
        
            if(ApexPages.currentPage().getParameters().containsKey(Label.CLOCT14PRM11)) //backURL
                retURL = ApexPages.currentPage().getParameters().get(Label.CLOCT14PRM11);             
            
            if(retURL == null || retURL.trim() == '')             
                retURL = 'http://www.schneider-electric.com';           
            
                lstLoginPartner = [Select id,Partner__c,RedirectURL__c from PartnerRequest__c Where Partner__c = :PartnerID Limit 1];
                if(!lstLoginPartner.isempty()) 
                    lstLoginPartner[0].RedirectURL__c = retURL;
                else {
                    
                    PartnerRequest__c aPartnerRequest = new PartnerRequest__c();
                    aPartnerRequest.Partner__c = PartnerID;
                    aPartnerRequest.RedirectURL__c = retURL;
                    lstLoginPartner.add(aPartnerRequest);           
                }
            
                try{
                    Upsert lstLoginPartner;
                }               
                    catch(System.DmlException  e){          
                    System.debug('Error while updating partner url redirection ->'+e.getDmlMessage(0)); 
                }
           // }         
        //}
        //Redirecting to bFO home page 
        String landingPage;
        if(ApexPages.currentPage().getParameters().containsKey(Label.CLOCT14PRM10)) //landingURL
            landingPage = ApexPages.currentPage().getParameters().get(Label.CLOCT14PRM10); 
        
        if(landingPage == null || landingPage.trim() == '')
            landingPage = system.label.CLOCT14PRM13;
            
        PageReference redirect = new PageReference(landingPage);
        return redirect;

   
   }
   //-------------------------------------------------------------------------------------------------------------------------------------------------
   public String getRedirectURL(){
        PRMCountry__c countrycluster;
       ID loggedInUserId = UserInfo.getUserId(); 
      
        User u = [SELECT id,Contact.PRMCountry__r.CountryCode__c FROM User WHERE Id = :loggedInUserId LIMIT 1];
         System.debug('*** Logged in User->'+ u );
        if(u != null){    
            String countryCode = u.Contact.PRMCountry__r.CountryCode__c;
            String countryCodeAux = '%' + countryCode  + '%';
            PRMCountry__c[] countryclusterList = [SELECT id,DefaultLandingPage__c FROM PRMCountry__c WHERE (CountryCode__c = :countryCode OR  TECH_Countries__c like : countryCodeAux) and CountryPortalEnabled__c=true LIMIT  1];
            if(countryclusterList.size()>0){
                countrycluster = countryclusterList[0];
            }else{
                countrycluster = [SELECT id,DefaultLandingPage__c FROM PRMCountry__c WHERE CountryCode__c = 'WW'];
            }
            return countrycluster.DefaultLandingPage__c;
        }else{
            countrycluster = [SELECT id,DefaultLandingPage__c FROM PRMCountry__c WHERE CountryCode__c = 'WW'];
            return countrycluster.DefaultLandingPage__c;
        }
   
   }
    
    
   
   //END: Release OCT14
   //--------------------------
   
 
}