@isTest
public class VFC_MassPsAssignment_TEST {

     static testMethod void testMassPsAssignment() {
        
        User adminUser = [SELECT Id, Name, FirstName, SESAExternalId__c  FROM User WHERE ProfileId = '00eA0000000uVHP' AND isActive = true LIMIT 1];

        System.runAs (adminUser) {
            Test.startTest();
            
            List<PermissionSetGroup__c> permissionSetGroupList =  permissionTestMethods.createPermissionSetGroups();    
            VFC_MassPsAssignment massPsAssignmentController = new VFC_MassPsAssignment();
            
            massPsAssignmentController.saveAssignments();
            massPsAssignmentController.returnAddSeveralUserPage();
            
            // Search by Name
            massPsAssignmentController.searchName = permissionSetGroupList.get(0).Name;                        
            massPsAssignmentController.search();
            
            ApexPages.currentPage().getParameters().put('selectId', permissionSetGroupList.get(0).Id);            
            massPsAssignmentController.selectGroup();
            
            massPsAssignmentController.userName = adminUser.FirstName;
            massPsAssignmentController.searchUsers();
            
            massPsAssignmentController.userSesaList = 'Test';
            massPsAssignmentController.importUsers();
            
            massPsAssignmentController.selectedUserList.add(new SelectOption(adminUser.Id, adminUser.Name));
            
            massPsAssignmentController.saveAssignments();
            massPsAssignmentController.backToMassAssign();
            
            Test.stopTest();
         }
     }

}