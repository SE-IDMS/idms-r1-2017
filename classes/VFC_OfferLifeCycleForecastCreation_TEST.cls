/***
Test clss forVFC_OfferLifeCycleForecastCreation;

Last Modified By:Hanamanth asper Nov release 
******/



    @isTest
    public class VFC_OfferLifeCycleForecastCreation_TEST {
        public static integer intQuarterMonth;
        public static testMethod void forecastUnitTest() {
            Offer_Lifecycle__c inOffLif= new Offer_Lifecycle__c(); 
            inOffLif.Name='Test Schneider';
            inOffLif.Leading_Business_BU__c ='IT';
            inOffLif.Offer_Classification__c ='Major';
            inOffLif.Forecast_Unit__c='Kilo Euro';
            inOffLif.Launch_Master_Assets_Status__c='Red';
            inOffLif.Stage__c ='0 - Define';
            
            Insert inOffLif; 
            
            Country__c cotryObj= new Country__c();
            cotryObj.Name ='France';
            cotryObj.CountryCode__c='1FR';
            Insert cotryObj;
            
            Country__c cotryObj1= new Country__c();
            cotryObj1.Name ='Italy';
            cotryObj1.CountryCode__c='IT1';
            Insert cotryObj1;
            
            Milestone1_Project__c mileProject= new Milestone1_Project__c(); 
            Schema.DescribeSObjectResult d = Schema.SObjectType.Milestone1_Project__c;    
            Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();  
            String MPRecordTypeId = rtMapByName.get('Offer Introduction Project').getRecordTypeId();
            mileProject.RecordTypeId=MPRecordTypeId;
            mileProject.Name ='By Country Project ';
            mileProject.Offer_Launch__c=inOffLif.id;
            mileProject.ForecastValidator__c=userInfo.getUserId();
            mileProject.ForecastStatus__c=label.CLAPRIL14COINbFO29;
            //mileProject.ForecastStatus__c = 'Validated';  
            mileProject.Country_End_of_Commercialization_Date__c = Date.today();
            mileProject.TECH_IsForecastByMonth__c=true;
            mileProject.TECH_IsForecastByQuarter__c=true;
            mileProject.TECH_IsForecastByYear__c = true;
            mileProject.Country__c=cotryObj.id;
            //mileProject.Forecast_Unit__c = '';
            mileProject.Country_Announcement_Date__c=Date.today();
            
            insert mileProject ;
            
            Map<String,OfferLifeCycleForecast__c> mapforecast = new Map<String,OfferLifeCycleForecast__c> ();            
            List<OfferLifeCycleForecast__c> ListoffForecast= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast = new OfferLifeCycleForecast__c();
            for(integer i=1; i<MONTHS_OF_YEAR.size(); i++ ) {
                offForecast = new OfferLifeCycleForecast__c();
                offForecast.project__c=mileProject.id;
                offForecast.month__c=monthName(i);
                offForecast.ForcastYear__c='2014';
                offForecast.Actual__c =1000;
                offForecast.BU__c =2000;
                offForecast.Country2__c='France';
                offForecast.Country__c=4000;
                if(i <=3) {
                offForecast.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast.add(offForecast);
            
            
            }
            insert  ListoffForecast;
            for (OfferLifeCycleForecast__c offlife:ListoffForecast){
                mapforecast.put(offlife.ForecastQuarter__c+offlife.ForcastYear__c,offlife);         
            }
            
            Map<String,OfferLifeCycleForecast__c> mapforecast1 = new Map<String,OfferLifeCycleForecast__c> ();            
            List<OfferLifeCycleForecast__c> ListoffForecast1= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast1 = new OfferLifeCycleForecast__c();
            for(integer i=1; i<MONTHS_OF_QUARTAR.size(); i++ ) {
                offForecast1 = new OfferLifeCycleForecast__c();
                offForecast1.project__c=mileProject.id;
               // offForecast1.month__c=monthName(i);
                offForecast1.ForcastYear__c='2014';
                offForecast1.Actual__c =0;
                offForecast1.BU__c =0;
               // offForecast1.Country2__c='Italy';
                offForecast1.Country__c=0;
                if(i <=3) {
                offForecast1.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast1.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast1.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast1.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast1.add(offForecast1);
            
            
            }
            insert  ListoffForecast1;
            for (OfferLifeCycleForecast__c offlife:ListoffForecast1){
                mapforecast1.put(offlife.ForecastQuarter__c+offlife.ForcastYear__c,offlife);         
            }
            
            String set_Option='Month';
            String set_Option1='Year';
            String set_Option2='Quarter';
            ApexPages.currentPage().getParameters().put('Id', mileProject.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(mileProject);       
            VFC_OfferLifeCycleForecastCreation coForecast = new VFC_OfferLifeCycleForecastCreation(controller); 
            coForecast.getRadioValue(); 
            coForecast.getforeCastByYear();             
            coForecast.forecastSetionYear();
            coForecast.setSelectOption(set_Option);
            coForecast.setSelectOption(set_Option1);
            coForecast.setSelectOption(set_Option2);
            coForecast.selectedMQY='Quarter';
            coForecast.selectedMQY='Year';
            coForecast.selectedMQY='Month';
            coForecast.selectedforCstYear = '3';
            coForecast.selectedforCstYear ='4';
            coForecast.saveOfferCycfCast();
            coForecast.saveForecastbyQuarter();
            coForecast.saveForecastbyYear();
            coForecast.makeReadyOny();
            coForecast.cancelforecast();
            coForecast.forecastTotalQrtCal(mapforecast);
            coForecast.fetchColQuarteNames(mapforecast);
            coForecast.DispayForecastRecord(3);
            coForecast.DispayForecastRecord(4);
            coForecast.OfferForeCastYearandMonth(3);
            coForecast.Onselection();
            coForecast.calculateToatlForecsUnit();
            coForecast.forecastTotalQrtCal(mapforecast);
            coForecast.Onselection1();
           // coForecast.fetchColQuarteNames();
            coForecast.foreCastYearandMonth(4);
            coForecast.forecastQuarterMonth();
            coForecast.forecastQuarterToMonth();
            coForecast.forecastSetionYear();
            coForecast.forecastYearbyMonth();
            coForecast.getSelectOption();
            coForecast.toCheckForecastIsInteger(mapforecast1,mapforecast1,'Year');
            coForecast.forecasEdit = 'edit';
            coForecast.forecasEdit = '';
            
           
        }
    private static final List<String> MONTHS_OF_YEAR = new String[]{'January','February','March','April','May','June','July','August','September','October','November','December','Total by Year'};


    private static String monthName(Integer monthNum) {

        return(MONTHS_OF_YEAR[monthNum - 1]);

    }
    private static final List<String> MONTHS_OF_QUARTAR = new String[]{'Q1','Q1','Q1','Q2','Q2','Q2','Q3','Q3','Q3','Q4','Q4','Q4'};


    private String quarterName(Integer monthNum) {

        return(MONTHS_OF_QUARTAR[monthNum - 1]);

    }
    
     public static testMethod void forecastUnitTest1() {
            Offer_Lifecycle__c inOffLif= new Offer_Lifecycle__c(); 
            inOffLif.Name='Test Schneider';
            inOffLif.Leading_Business_BU__c ='IT';
            inOffLif.Offer_Classification__c ='Major';
            inOffLif.Forecast_Unit__c='Unit';
            inOffLif.Launch_Master_Assets_Status__c='Red';
            inOffLif.Stage__c ='0 - Define';
            
            Insert inOffLif; 
            
            Country__c cotryObj= new Country__c();
            cotryObj.Name ='France';
            cotryObj.CountryCode__c='1FR';
            Insert cotryObj;
            
            Country__c cotryObj1= new Country__c();
            cotryObj1.Name ='Italy';
            cotryObj1.CountryCode__c='IT1';
            Insert cotryObj1;
            
            Milestone1_Project__c mileProject= new Milestone1_Project__c(); 
            Schema.DescribeSObjectResult d = Schema.SObjectType.Milestone1_Project__c;    
            Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();  
            String MPRecordTypeId = rtMapByName.get('Offer Introduction Project').getRecordTypeId();
            mileProject.RecordTypeId=MPRecordTypeId;
            mileProject.Name ='By Country Project ';
            mileProject.Offer_Launch__c=inOffLif.id;
            mileProject.ForecastValidator__c=userInfo.getUserId();
            //mileProject.ForecastStatus__c='Submitted for Validated';
            mileProject.ForecastStatus__c = 'Validated';  
            mileProject.Country_End_of_Commercialization_Date__c = Date.today();
            mileProject.TECH_IsForecastByMonth__c=true;
            mileProject.TECH_IsForecastByQuarter__c=true;
            mileProject.TECH_IsForecastByYear__c = true;
            mileProject.Country__c=cotryObj.id;
            mileProject.Country_Announcement_Date__c=Date.today();
            mileProject.Country_End_of_Commercialization_Date__c = Date.today();
            
            insert mileProject ;
            
            if(mileProject.Country_End_of_Commercialization_Date__c!=null) {
                //intQuarterYear =countryProj.Country_End_of_Commercialization_Date__c.year();
                intQuarterMonth =mileProject.Country_End_of_Commercialization_Date__c.month();
            }
            Map<String,OfferLifeCycleForecast__c> mapforecast = new Map<String,OfferLifeCycleForecast__c> ();            
            List<OfferLifeCycleForecast__c> ListoffForecast= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast = new OfferLifeCycleForecast__c();
            for(integer i=intQuarterMonth; i<MONTHS_OF_YEAR.size(); i-- ) {
                if(i!=0) {
                offForecast = new OfferLifeCycleForecast__c();
                offForecast.project__c=mileProject.id;
                offForecast.month__c=monthName(i);
                offForecast.ForcastYear__c='2017';
                offForecast.Actual__c =1000;
                offForecast.BU__c =2000;
                offForecast.Country2__c='France';
                offForecast.Country__c=4000;
                if(i <=3) {
                offForecast.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast.add(offForecast);
            
                }
                else {
                break;
                }
            }
            insert  ListoffForecast;
            for (OfferLifeCycleForecast__c offlife:ListoffForecast){
                mapforecast.put(offlife.ForecastQuarter__c+offlife.ForcastYear__c,offlife);         
            }
            
            Map<String,OfferLifeCycleForecast__c> mapforecast1 = new Map<String,OfferLifeCycleForecast__c> ();            
            List<OfferLifeCycleForecast__c> ListoffForecast1= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast1 = new OfferLifeCycleForecast__c();
            for(integer i=1; i<MONTHS_OF_QUARTAR.size(); i++ ) {
                offForecast1 = new OfferLifeCycleForecast__c();
                offForecast1.project__c=mileProject.id;
               //offForecast1.month__c=monthName(i);
                offForecast1.ForcastYear__c='2017';
                offForecast1.Actual__c =0;
                offForecast1.BU__c =0;
               // offForecast1.Country2__c='Italy';
                offForecast1.Country__c=0;
                if(i <=3) {
                offForecast1.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast1.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast1.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast1.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast1.add(offForecast1);
            
            
            }
            insert  ListoffForecast1;
            for (OfferLifeCycleForecast__c offlife:ListoffForecast1){
                mapforecast1.put(offlife.ForecastQuarter__c+offlife.ForcastYear__c,offlife);         
            }
            
                        
            String set_Option='Month';
            String set_Option1='Year';
            String set_Option2='Quarter';
            ApexPages.currentPage().getParameters().put('Id', mileProject.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(mileProject);       
            VFC_OfferLifeCycleForecastCreation coForecast = new VFC_OfferLifeCycleForecastCreation(controller); 
             VFC_OfferLifeCycleForecastCreation.ForcastFieldsWrapper fw = new VFC_OfferLifeCycleForecastCreation.ForcastFieldsWrapper('1','2',0,0,0,True);
            coForecast.getRadioValue(); 
            coForecast.getforeCastByYear();             
            coForecast.forecastSetionYear();
            coForecast.setSelectOption(set_Option);
            coForecast.setSelectOption(set_Option1);
            coForecast.setSelectOption(set_Option2);
            coForecast.selectedMQY='Quarter';
            coForecast.selectedMQY='Year';
            coForecast.selectedMQY='Month';
            coForecast.selectedforCstYear = '3';
            coForecast.selectedforCstYear ='4';
            coForecast.saveOfferCycfCast();
            coForecast.saveForecastbyQuarter();
            coForecast.saveForecastbyYear();
            coForecast.makeReadyOny();
            coForecast.cancelforecast();
            coForecast.forecastTotalQrtCal(mapforecast);
            coForecast.fetchColQuarteNames(mapforecast);
            coForecast.DispayForecastRecord(3);
            coForecast.DispayForecastRecord(4);
            coForecast.OfferForeCastYearandMonth(3);
            coForecast.Onselection();
            coForecast.calculateToatlForecsUnit();
            coForecast.forecastTotalQrtCal(mapforecast);
            coForecast.Onselection1();
           // coForecast.fetchColQuarteNames();
            coForecast.foreCastYearandMonth(4);
            coForecast.forecastQuarterMonth();
            coForecast.forecastQuarterToMonth();
            coForecast.forecastSetionYear();
            coForecast.forecastYearbyMonth();
            coForecast.getSelectOption();
            coForecast.toCheckForecastIsInteger(mapforecast1,mapforecast1,'Year');
            coForecast.forecasEdit = 'edit';
            coForecast.forecasEdit = '';
        
        }
        

public static testMethod void forecastUnitTest2() {
            Offer_Lifecycle__c inOffLif= new Offer_Lifecycle__c(); 
            inOffLif.Name='Test Schneider';
            inOffLif.Leading_Business_BU__c ='IT';
            inOffLif.Offer_Classification__c ='Major';
            inOffLif.Forecast_Unit__c='Unit';
            inOffLif.Launch_Master_Assets_Status__c='Red';
            inOffLif.Stage__c ='0 - Define';
            
            Insert inOffLif; 
            
            Country__c cotryObj= new Country__c();
            cotryObj.Name ='France';
            cotryObj.CountryCode__c='1FR';
            Insert cotryObj;
            
            Country__c cotryObj1= new Country__c();
            cotryObj1.Name ='Italy';
            cotryObj1.CountryCode__c='IT1';
            Insert cotryObj1;
            
            Milestone1_Project__c mileProject= new Milestone1_Project__c(); 
            Schema.DescribeSObjectResult d = Schema.SObjectType.Milestone1_Project__c;    
            Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();  
            String MPRecordTypeId = rtMapByName.get('Offer Introduction Project').getRecordTypeId();
            mileProject.RecordTypeId=MPRecordTypeId;
            mileProject.Name ='By Country Project ';
            mileProject.Offer_Launch__c=inOffLif.id;
            mileProject.ForecastValidator__c=userInfo.getUserId();
            //mileProject.ForecastStatus__c='Submitted for Validated';
            mileProject.ForecastStatus__c = 'Validated';  
            mileProject.Country_End_of_Commercialization_Date__c = Date.today();
            mileProject.TECH_IsForecastByMonth__c=true;
            mileProject.TECH_IsForecastByQuarter__c=true;
            mileProject.TECH_IsForecastByYear__c = true;
            mileProject.Country__c=cotryObj.id;
            mileProject.Country_Announcement_Date__c=null;
            mileProject.Country_End_of_Commercialization_Date__c = Date.today();
            
            insert mileProject ;
            
            if(mileProject.Country_End_of_Commercialization_Date__c!=null) {
                //intQuarterYear =countryProj.Country_End_of_Commercialization_Date__c.year();
                intQuarterMonth =mileProject.Country_End_of_Commercialization_Date__c.month();
            }
            Map<String,OfferLifeCycleForecast__c> mapforecast = new Map<String,OfferLifeCycleForecast__c> ();            
            List<OfferLifeCycleForecast__c> ListoffForecast= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast = new OfferLifeCycleForecast__c();
            for(integer i=intQuarterMonth; i<MONTHS_OF_YEAR.size(); i-- ) {
                if(i!=0) {
                offForecast = new OfferLifeCycleForecast__c();
                offForecast.project__c=mileProject.id;
                offForecast.month__c=monthname(i);
                offForecast.ForcastYear__c='2014';
                offForecast.Actual__c =1000;
                offForecast.BU__c =0;
                offForecast.Country2__c='France';
                offForecast.Country__c=4000;
                if(i <=3) {
                offForecast.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast.add(offForecast);
            
                }
                else {
                break;
                }
            }
            insert  ListoffForecast;
            for (OfferLifeCycleForecast__c offlife:ListoffForecast){
                mapforecast.put(offlife.ForecastQuarter__c+offlife.ForcastYear__c,offlife);         
            }
            
            Map<String,OfferLifeCycleForecast__c> mapforecast1 = new Map<String,OfferLifeCycleForecast__c> ();            
            List<OfferLifeCycleForecast__c> ListoffForecast1= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast1 = new OfferLifeCycleForecast__c();
            for(integer i=1; i<MONTHS_OF_QUARTAR.size(); i++ ) {
                offForecast1 = new OfferLifeCycleForecast__c();
                offForecast1.project__c=mileProject.id;
               //offForecast1.month__c=monthName(i);
                offForecast1.ForcastYear__c='2014';
                offForecast1.Actual__c =0;
                offForecast1.BU__c =0;
               // offForecast1.Country2__c='Italy';
                offForecast1.Country__c=0;
                if(i <=3) {
                offForecast1.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast1.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast1.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast1.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast1.add(offForecast1);
            
            
            }
            insert  ListoffForecast1;
            for (OfferLifeCycleForecast__c offlife:ListoffForecast1){
                mapforecast1.put(offlife.ForecastQuarter__c+offlife.ForcastYear__c,offlife);         
            }
            
            
        Map<String,OfferLifeCycleForecast__c> mapforecast2 = new Map<String,OfferLifeCycleForecast__c> ();            
            List<OfferLifeCycleForecast__c> ListoffForecast2= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast2 = new OfferLifeCycleForecast__c();
            for(integer i=1; i<MONTHS_OF_QUARTAR.size(); i++ ) {
                offForecast2 = new OfferLifeCycleForecast__c();
                offForecast2.project__c=mileProject.id;
                offForecast2.month__c= monthname(i);
                offForecast2.ForcastYear__c='2014';
                offForecast2.Actual__c =2000;
                offForecast2.BU__c =1000;
                offForecast2.Country2__c='Italy';
                offForecast2.Country__c =4000;
                if(i <=3) {
                offForecast2.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast2.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast2.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast2.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast2.add(offForecast2);
            
            
            }
            insert  ListoffForecast2;
            for (OfferLifeCycleForecast__c offlife:ListoffForecast2){
                mapforecast2.put(offlife.ForecastQuarter__c+offlife.ForcastYear__c,offlife);         
            }
            
                        
            String set_Option='Month';
            String set_Option1='Year';
            String set_Option2='Quarter';
            ApexPages.currentPage().getParameters().put('Id', mileProject.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(mileProject);       
            VFC_OfferLifeCycleForecastCreation coForecast = new VFC_OfferLifeCycleForecastCreation(controller); 
            VFC_OfferLifeCycleForecastCreation.ForcastFieldsWrapper fw = new VFC_OfferLifeCycleForecastCreation.ForcastFieldsWrapper('1','2',0,0,0,True);
            coForecast.getRadioValue(); 
            coForecast.ForecastName = 'TestForecast';
            coForecast.ForecastValue = '10000';
            coForecast.ForecastQuart = 'Q1';
            coForecast.getforeCastByYear();             
            coForecast.forecastSetionYear();
            coForecast.setSelectOption(set_Option);
            coForecast.setSelectOption(set_Option1);
            coForecast.setSelectOption(set_Option2);
            coForecast.selectedMQY='Quarter';
            coForecast.selectedMQY='Year';
            coForecast.selectedMQY='Month';
            coForecast.selectedforCstYear = '3';
            coForecast.selectedforCstYear ='4';
            coForecast.saveOfferCycfCast();
            coForecast.saveForecastbyQuarter();
            coForecast.saveForecastbyYear();
            coForecast.makeReadyOny();
            coForecast.cancelforecast();
            coForecast.forecastTotalQrtCal(mapforecast2);
            coForecast.fetchColQuarteNames(mapforecast2);
            coForecast.DispayForecastRecord(3);
            coForecast.DispayForecastRecord(4);
            coForecast.OfferForeCastYearandMonth(3);
            coForecast.Onselection();
            coForecast.calculateToatlForecsUnit();
            coForecast.forecastTotalQrtCal(mapforecast);
            coForecast.Onselection1();
           // coForecast.fetchColQuarteNames();
            coForecast.foreCastYearandMonth(4);
            coForecast.forecastQuarterMonth();
            coForecast.forecastQuarterToMonth();
            coForecast.forecastSetionYear();
            coForecast.forecastYearbyMonth();
            coForecast.getSelectOption();
            coForecast.toCheckForecastIsInteger(mapforecast2,mapforecast2,'Month');
           // coForecast.forecasEdit = 'edit';
            coForecast.forecasEdit = '';
            
            
        
        }
        
    
}