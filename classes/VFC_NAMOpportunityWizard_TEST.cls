/* Test Class for VFC_NAMOpportunityWizard */

@isTest
public class VFC_NAMOpportunityWizard_TEST 
{
    static testmethod void opportunitySearch(){
        ApexPages.StandardController controller;
        VFC_NAMOpportunityWizard oppSrch;
        Country__c newCountry = Utils_TestMethods.createCountry();
        newCountry.Name = 'USA';
        newCountry.CountryCode__c=System.Label.CLQ416SLS012;
        insert newCountry;       
        Account acc = Utils_TestMethods.createAccount();
        acc.NAme='test';
        acc.Country__c=newCountry.Id;
        acc.recordtypeId=system.label.CLOCT13ACC08;
        insert acc;
        ApexPages.currentPage().getParameters().put('accid',acc.Id);
        Opportunity opp= Utils_TestMethods.createOpportunity(acc.Id);
        opp.Name = 'ttttttt';
        opp.TECH_IsOpportunityUpdate__c  = true;
        opp.Reason__c = 'test';
        opp.OpptyType__c = 'Solutions';        
        opp.COuntryOfDestination__c = null;
        opp.Location__c = 'testLocation';
        opp.AccountId = acc.Id;
        insert opp;
        test.starttest();
        SE_Territory__c ot=new SE_Territory__c();
        ot.Name='test territory';
        ot.Country__c=newCountry.Id;
        ot.Business__c='GC';
        insert ot;
        controller = new ApexPages.StandardController(opp);        
        oppSrch = new VFC_NAMOpportunityWizard(controller);
        oppSrch.OpptyType= 'Standard';
        oppSrch.opp.AccountId=acc.Id;
        oppSrch.opportunityRecord.OpportunityTerritory__c=ot.Id;
        oppSrch.member.UserID=[SELECT Id FROM user WHERE profile.Name = 'System Administrator' and isActive=true and UserRoleID!=null limit 1].Id;
        oppSrch.opptyName= 'ttttttt';
        oppSrch.vcp1.Account__c=acc.Id;
        oppSrch.vcp1.AccountRole__c='Agent';
        oppSrch.opportunityRecord.Location__c='loc';
        oppSrch.opportunityRecord.AccountId=acc.Id;
        oppSrch.sortExpression = 'Name';
        oppSrch.sortDirection = 'DESC';
        oppSrch.initialCheck();
        oppSrch.searchOpportunities();                                      
        oppSrch.createOpportunity(); 
        oppSrch.cancel();  
        oppSrch.createRelatedRecords(); 
        oppSrch.saveVCP();
        oppSrch.skip();
        oppSrch.opptyCreation(); 
        oppSrch.getstageName();
        oppSrch.typeStageMapping();
        VFC_NAMOpportunityWizard.getRecords('test',newCountry.Id); 
        test.stoptest();
    }
}