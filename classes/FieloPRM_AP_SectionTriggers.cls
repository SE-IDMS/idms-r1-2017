public class FieloPRM_AP_SectionTriggers{
    
    public static void removeCache(List<FieloEE__Section__c> sections){        
        Set<Id> parentSectionIds = new Set<Id>();
        for(FieloEE__Section__c section : sections){
            if(section.FieloEE__Parent__c != null){
                parentSectionIds.add(section.FieloEE__Parent__c);
            }
        }

        if(!parentSectionIds.isEmpty()){
            update [SELECT Id FROM FieloEE__Section__c WHERE Id in : parentSectionIds];
        }
    }
}