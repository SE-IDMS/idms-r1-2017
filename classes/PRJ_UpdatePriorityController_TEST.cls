@isTest
private class PRJ_UpdatePriorityController_TEST
{
    
    static testMethod void test_PRJ_UpdatePriorityController() 
    {
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Europe'); 
        insert testDMTA1;
        
        PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();      
        testProj.Name = 'Test';
        testProj.Parent__c = 'Buildings';
        testProj.ParentFamily__c = 'Business';
        testProj.BusGeographicZoneIPO__c = 'Europe';
        testProj.BusGeographicalIPOOrganization__c = 'CIS';
        testProj.GeographicZoneSE__c = 'APAC';
        testProj.Tiering__c = 'Tier 1';
        testProj.ProjectRequestDate__c = system.today();
        testProj.Description__c ='Test';
        testProj.GlobalProcess__c = 'Customer Care';
        testProj.ProjectClassification__c = 'Infrastructure';
        testProj.ScoreResultCalculated__c='2 (Important 2)';
        testProj.BusinessTechnicalDomain__c = 'Supply Chain';
        insert testProj;
        
        Budget__c testBud = Utils_TestMethods.createBudget(testProj,true);
        insert testBud;
        DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid',testProj.BusinessTechnicalDomain__c); 
        insert testDMTA2;
        
        testProj.NextStep__c='Created';
        update testProj;
        
        DMTAuthorizationMasterData__c testDMTA3 = new DMTAuthorizationMasterData__c();
        testDMTA3.AuthorizedUser5__c = UserInfo.getUserId();
        testDMTA3.AuthorizedUser6__c = UserInfo.getUserId();
        testDMTA3.NextStep__c = 'quoted';
        testDMTA3.Parent__c = testProj.Parent__c;
        testDMTA3.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
        testDMTA3.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
        insert testDMTA3;   
         
        testProj.NextStep__c = 'Valid';
        testProj.ExpectedStartDate__c = System.today();
        testProj.GoLiveTargetDate__c =System.today().addDays(2);
        testProj.AppGlobalOpservdecisiondate__c = System.today();
        testProj.AppServicesDecision__c = 'Yes';
        testProj.ScoreResultCalculated__c='4 (Mandatory)';
        update testProj;
        
        
        PRJ_UpdatePriorityController vfCtrl = new PRJ_UpdatePriorityController(new ApexPages.StandardController(testProj));
        //testProj = new PRJ_ProjectReq__c();      
        vfCtrl.updateProjectRequest();
        vfCtrl.backToProjectRequest();
        
        DMTAuthorizationMasterData__c testDMTA4 = new DMTAuthorizationMasterData__c();
        testDMTA4.AuthorizedUser1__c = UserInfo.getUserId();
        testDMTA4.AuthorizedUser5__c = UserInfo.getUserId();
        testDMTA4.AuthorizedUser6__c = UserInfo.getUserId();
        testDMTA4.NextStep__c = 'Fundings Authorized';
        testDMTA4.Parent__c = testProj.Parent__c;
        testDMTA4.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
        testDMTA4.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
        insert testDMTA4; 
            
        testProj.NextStep__c = 'Quoted';
        testProj.DecisionSecurityResponsible__c = 'Yes';
        testProj.DecisionSecurityResponsibleDate__c = System.Today();
        testProj.DecisionControllerProposalValidation__c =  'Yes';
        testProj.DecisionProposalValidationDate__c = System.Today();
        testProj.ScoreResultCalculated__c = '2 (Important 2)';
        update testProj;
        
    }
    static testMethod void test_PRJ_UpdatePriorityController1() 
    {
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Europe'); 
        insert testDMTA1;
        
        PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();      
        testProj.Name = 'Test';
        testProj.Parent__c = 'Buildings';
        testProj.ParentFamily__c = 'Business';
        testProj.BusGeographicZoneIPO__c = 'Europe';
        testProj.BusGeographicalIPOOrganization__c = 'CIS';
        testProj.GeographicZoneSE__c = 'APAC';
        testProj.Tiering__c = 'Tier 1';
        testProj.ProjectRequestDate__c = system.today();
        testProj.Description__c ='Test';
        testProj.GlobalProcess__c = 'Customer Care';
        testProj.BusinessTechnicalDomain__c = 'Supply Chain';
        testProj.ProjectClassification__c = 'Infrastructure';
        insert testProj;
        
        Budget__c testBud = Utils_TestMethods.createBudget(testProj,true);
        insert testBud;
        
        DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid',testProj.BusinessTechnicalDomain__c); 
        insert testDMTA2;
        
        testProj.NextStep__c='Created';
        update testProj;
         
        DMTAuthorizationMasterData__c testDMTA3 = new DMTAuthorizationMasterData__c();
        testDMTA3.AuthorizedUser5__c = UserInfo.getUserId();
        testDMTA3.AuthorizedUser6__c = UserInfo.getUserId();
        testDMTA3.NextStep__c = 'quoted';
        testDMTA3.Parent__c = testProj.Parent__c;
        testDMTA3.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
        testDMTA3.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
         insert testDMTA3;  
         
        testProj.NextStep__c = 'Valid';
        testProj.ExpectedStartDate__c = System.today();
        testProj.GoLiveTargetDate__c =System.today().addDays(2);
        testProj.AppGlobalOpservdecisiondate__c = System.today();
        testProj.AppServicesDecision__c = 'Yes';
        update testProj;
        
        PRJ_UpdatePriorityController vfCtrl = new PRJ_UpdatePriorityController(new ApexPages.StandardController(testProj));
        vfCtrl.updateProjectRequest();
        vfCtrl.backToProjectRequest();
        
        DMTAuthorizationMasterData__c testDMTA4 = new DMTAuthorizationMasterData__c();
        testDMTA4.AuthorizedUser1__c = UserInfo.getUserId();
        testDMTA4.AuthorizedUser5__c = UserInfo.getUserId();
        testDMTA4.AuthorizedUser6__c = UserInfo.getUserId();
        testDMTA4.NextStep__c = 'Fundings Authorized';
        testDMTA4.Parent__c = testProj.Parent__c;
        testDMTA4.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
        testDMTA4.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
        insert testDMTA4; 
        
        testProj.NextStep__c = 'Quoted';
        testProj.DecisionSecurityResponsible__c = 'Yes';
        testProj.DecisionSecurityResponsibleDate__c = System.Today();
        testProj.DecisionControllerProposalValidation__c =  'Yes';
        testProj.DecisionProposalValidationDate__c = System.Today();
        testProj.ScoreResultCalculated__c = '2 (Important 2)';
        update testProj;
        
    }
    static testMethod void test_PRJ_UpdatePriorityController2() 
    {
    
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Europe'); 
        insert testDMTA1;
        
        PRJ_ProjectReq__c testProj1 = new PRJ_ProjectReq__c();      
        testProj1.Name = 'Test';
        testProj1.Parent__c = 'Buildings';
        testProj1.ParentFamily__c = 'Business';
        testProj1.BusGeographicZoneIPO__c = 'Europe';
        testProj1.BusGeographicalIPOOrganization__c = 'CIS';
        testProj1.GeographicZoneSE__c = 'APAC';
        testProj1.Tiering__c = 'Tier 1';
        testProj1.ProjectRequestDate__c = system.today();
        testProj1.Description__c ='Test';
        testProj1.GlobalProcess__c = 'Customer Care';
        testProj1.ProjectClassification__c = 'Infrastructure';
        testProj1.BusinessTechnicalDomain__c = 'Supply Chain';
        insert testProj1;
        
        Budget__c testBud = Utils_TestMethods.createBudget(testProj1,true);
        insert testBud;
        
        DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid',testProj1.BusinessTechnicalDomain__c); 
        insert testDMTA2;
        testProj1.NextStep__c='Created';
        update testProj1;
        
        DMTAuthorizationMasterData__c testDMTA3 = new DMTAuthorizationMasterData__c();
        testDMTA3.AuthorizedUser5__c = UserInfo.getUserId();
        testDMTA3.AuthorizedUser6__c = UserInfo.getUserId();
        testDMTA3.NextStep__c = 'quoted';
        testDMTA3.Parent__c = testProj1.Parent__c;
        testDMTA3.BusGeographicZoneIPO__c = testProj1.BusGeographicZoneIPO__c;
        testDMTA3.BusinessTechnicalDomain__c = testProj1.BusinessTechnicalDomain__c;
         insert testDMTA3;  
         
        testProj1.NextStep__c = 'Valid';
        testProj1.ExpectedStartDate__c = System.today();
        testProj1.GoLiveTargetDate__c =System.today().addDays(2);
        testProj1.AppGlobalOpservdecisiondate__c = System.today();
        testProj1.AppServicesDecision__c = 'Yes';
        update testProj1;
        
        DMTAuthorizationMasterData__c testDMTA4 = new DMTAuthorizationMasterData__c();
        testDMTA4.AuthorizedUser1__c = UserInfo.getUserId();
        testDMTA4.AuthorizedUser5__c = UserInfo.getUserId();
        testDMTA4.AuthorizedUser6__c = UserInfo.getUserId();
        testDMTA4.NextStep__c = 'Fundings Authorized';
        testDMTA4.Parent__c = testProj1.Parent__c;
        testDMTA4.BusGeographicZoneIPO__c = testProj1.BusGeographicZoneIPO__c;
        testDMTA4.BusinessTechnicalDomain__c = testProj1.BusinessTechnicalDomain__c;
        insert testDMTA4; 
        
        testProj1.NextStep__c = 'Quoted';
        testProj1.DecisionSecurityResponsible__c = 'Yes';
        testProj1.DecisionSecurityResponsibleDate__c = System.Today();
        testProj1.DecisionControllerProposalValidation__c =  'Yes';
        testProj1.DecisionProposalValidationDate__c = System.Today();
       // testProj1.ScoreResultCalculated__c = '4 (Mandatory)';
        update testProj1;

        
        User runAsUser = Utils_TestMethods.createStandardDMTUser('tst2');
        test.startTest();
        insert runAsUser;
        System.runAs(runAsUser)
        {

        PRJ_UpdatePriorityController vfCtrl = new PRJ_UpdatePriorityController(new ApexPages.StandardController(testProj1));
        vfCtrl.updateProjectRequest();
        vfCtrl.backToProjectRequest();
        testProj1.ScoreResultCalculated__c = '4 (Mandatory)';
        vfCtrl.updateProjectRequest();
       }         
    }
     
}