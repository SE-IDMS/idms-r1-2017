/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This rest class is responsible for calling the services for Create and update user 
**/

@RestResource(urlMapping='/IDMSUser/*')
global with sharing class IdmsUserRest{
    //This is to call update user service    
    @HttpPut
    global static IDMSResponseWrapper doPut(User UserRecord){
        system.debug('idms update user ---- '+UserInfo.getUserId());
        
        IDMSUpdateUser updateUser = new IDMSUpdateUser();             
        IDMSResponseWrapper response;
        RestResponse res = RestContext.response;
        
        response= updateUser.updateUser(UserRecord);
        system.debug('api response:  '+Response.Message);
        
        if(Response.Message.startsWith('Exception at Line')){
            res.statuscode = Integer.valueOf(Label.CLDEC16IDMS001);
        }
        else{       
            res.statuscode = getHTTPResponseCode(Response.Message); 
        }
        system.debug('status code:  '+res.statuscode);
        if(response.status.equalsignorecase('error')){         
            response.IDMSUserRecord = null;
        }
        else{        
            response.IDMSUserRecord = getUserObject(Response.IDMSUserRecord);
        }
        return response;
    }
    
    //This is used to call create user service
    
    @HttpPost 
    global static IDMSResponseWrapper doPost(User UserRecord,string Password){ 
        
        system.debug('userrecord --> ' + UserRecord );
        system.debug('Password --> ' + Password ) ;
        
        IDMSCreateUser createUser = new IDMSCreateUser();
        User User = new User();
        User =  UserRecord;
         IdmsPasswordPolicy psswdPolicy=new IdmsPasswordPolicy();                
        
        IDMSResponseWrapper Response;
        try{
            if(String.isnotblank(User.IDMS_Registration_Source__c) && (!UserRecord.IDMS_Registration_Source__c.equalsignorecase('uims')) ){
                if(String.isnotblank(User.FederationIdentifier)){
                    User.FederationIdentifier = '';
                }            
            }             
            User.IsIDMSUser__c = true;
            //Ranjith 
            Boolean passwordActive = false ; 
            
            if(Password != null) {
                if(String.isBlank(Password)){
                    Response = new IDMSResponseWrapper(null,Label.CLJUN16IDMS76,'Password should not be blank');
                    passwordActive = false ; 
                }
                //Prashanth
                //Modified as a part of New password policy
                //else if(!Pattern.matches(Label.CLNOV16IDMS129, Password)){
                if(!psswdPolicy.checkPolicyMetOrNotWithFNLN(Password,User.FirstName,User.LastName)){
                    Response = new IDMSResponseWrapper(null,Label.CLJUN16IDMS76,Label.CLJUN16IDMS52);
                    passwordActive = false ; 
                }else
                    PasswordActive = true ; 
            }else 
                passwordActive = true ;
        
            
            //condition added because of when identity type is mobile and the password is provided on the registration flow, UIMS are not able to handle such as use case
            system.debug('inside password -- condition--');
            if(UserRecord.Email == null && UserRecord.MobilePhone !=null && Password != null){
                system.debug('inside new condition--');
                Response = new IDMSResponseWrapper(null,Label.CLJUN16IDMS76,'This use case is not permitted.');
                passwordActive = false ; 
            }
            
            system.debug('password True ' + passwordActive ) ;
            
            if(passwordActive){
                createUser.pwd = password;
                if(User.IDMS_User_Context__c == null || User.IDMS_User_Context__c == '' || !(UserRecord.IDMS_User_Context__c.equals('@Work')) && !(UserRecord.IDMS_User_Context__c.equals('Work')) && !(UserRecord.IDMS_User_Context__c.equals('Home')) && !(UserRecord.IDMS_User_Context__c.equals('@Home'))){
                    Response = new IDMSResponseWrapper(null,Label.CLJUN16IDMS76,'Invalid User Context');
                }else if(String.isnotblank(User.IDMS_User_Context__c) && (UserRecord.IDMS_User_Context__c.contains('Home') || UserRecord.IDMS_User_Context__c.contains('home'))){                              
                    Response = createUser.createUser(User);                        
                }
                else if(String.isnotblank(User.IDMS_User_Context__c) && (UserRecord.IDMS_User_Context__c.contains('Work') || UserRecord.IDMS_User_Context__c.contains('work') )){            
                    Response = createUser.createUserWork(User);  
                    system.debug('response Work--> ' +  Response ) ;    
                }
                else{
                    Response = new IDMSResponseWrapper(null,Label.CLJUN16IDMS76,'Required Fields Missing - User Context');
                }
            }
            system.debug('---response--'+Response );
            RestResponse res = RestContext.response;  
            system.debug('---res--'+res);
            
            if(res != null){                
                if(Response != null && getHTTPResponseCode(Response.Message) != null)
                {          
                    system.debug('---inside if---'+Response.Message);
                    res.statuscode = getHTTPResponseCode(Response.Message);          
                }
                else{
                    if((Response != null && Response.status.equalsignorecase('error')) || Response == null)
                        res.statuscode = Integer.valueOf(Label.CLDEC16IDMS005);
                    else if (Response != null &&  Response.status.equalsignorecase('success'))        
                        res.statuscode = Integer.valueOf(Label.CLDEC16IDMS011);
                    if(Response != null && Response.Message.contains('Invalid User')) 
                        res.statuscode = Integer.valueOf(Label.CLDEC16IDMS001) ;
                    if(Response != null && Response.Message.contains('Invalid Value')) 
                        res.statuscode = Integer.valueOf(Label.CLDEC16IDMS001) ;
                    if(Response != null && Response.Message.contains('Invalid State')) 
                        res.statuscode = Integer.valueOf(Label.CLDEC16IDMS001) ;                                        
                }
            }  
            
            system.debug('---response.status---'+response.status);
            if(response.status.equalsignorecase('success') && User != null){
                if(String.isNotBlank(password)){
                    //Set password in IDMS
                    System.setpassword(User.id,password); 
                    system.debug('---after password set---'+  password);
                }
                system.debug('---- User.IDMSIdentityType__c----'+User.IDMSIdentityType__c);
                if(User.IDMSIdentityType__c != null && User.IDMSIdentityType__c.equalsignorecase('Email')){
                    system.debug('---- in iff----');
                    sendemailPOST(User.id,String.isnotBlank(Password),User.IDMS_Registration_Source__c,User.IDMSToken__c);
                    //added to add in UIMS table for Email
                    IDMS_CheckUserExistHandler.createDummyUIMSUser(User.email,'', 'Email');
                }else{
                    sendSMS(User);
                    //added to add in UIMS table for Mobile
                    IDMS_CheckUserExistHandler.createDummyUIMSUser ('', User.MobilePhone, 'Mobile');
                }
                IdmsUserRest.updateUser(User.id);             
            } 
            
            if(response.status.equalsignorecase('error')){         
                response.IDMSUserRecord=null;
            }
            else{        
                response.IDMSUserRecord = getUserObject(user);
            }
            if(test.isrunningtest()){
                integer testInteger =1/0;
            }
        }
        catch(Exception e){
            
            string username = (String.isnotBlank(UserRecord.email))?UserRecord.email+Label.CLJUN16IDMS71:UserRecord.MobilePhone+Label.CLQ316IDMS102;
            String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
            System.debug(e.getMessage());
            if(String.isnotblank(UserRecord.IDMS_Registration_Source__c) && UserRecord.IDMS_Registration_Source__c == Label.CLJUN16IDMS72){
                new IDMSCaptureError().IDMScreateLog(null,Label.CLJUN16IDMS73,Label.CLJUN16IDMS74,username,ExceptionMessage ,UserRecord.IDMS_Registration_Source__c,
                                                     Datetime.now(),UserRecord.IDMS_User_Context__c,UserRecord.FederationIdentifier,null,false,Label.CLJUN16IDMS98);                
            }
            else{
                new IDMSCaptureError().IDMScreateLog(Label.CLJUN16IDMS74,Label.CLJUN16IDMS73,Label.CLJUN16IDMS74,username,ExceptionMessage ,UserRecord.IDMS_Registration_Source__c,
                                                     Datetime.now(),UserRecord.IDMS_User_Context__c,UserRecord.FederationIdentifier,null,false,Label.CLJUN16IDMS98);                
            }
            if(e.getmessage().contains('INVALID_NEW_PASSWORD')){
                response = new IDMSResponseWrapper(null,Label.CLJUN16IDMS76,'Password is not correct');                
            }else{
                response = new IDMSResponseWrapper(null,Label.CLJUN16IDMS76,Label.CLJUN16IDMS91);
            }
        }                     
        return Response ;
    }
    
    
    //This method is used to get http responce of the request
    static Integer getHTTPResponseCode(string message){
        Map<String,IDMSStatusCodeMapping__c> MapErrorCode = IDMSStatusCodeMapping__c.getAll();    
        Integer HTTPcode ;
        for(String str : MapErrorCode.keyset()){                
            if(message.startswith(str)){                
                HTTPcode = (Integer)MapErrorCode.get(str).httpCode__c;
                break;
            }
        } 
        system.debug('--- getHTTPResponseCode---'+ HTTPcode);  
        return  HTTPcode;
    }
    //This method us used to get object of user
    Public static user getUserObject(User Obj){    
        user userToSend=new user(); 
        try{
            
            Schema.FieldSet UserField;
            
            if(obj.IDMS_User_Context__c.contains('home') || obj.IDMS_User_Context__c.contains('Home')){
                UserField = Schema.SObjectType.User.fieldSets.getMap().get('UserUpdate');              
            }
            else if(obj.IDMS_User_Context__c.contains('work') || obj.IDMS_User_Context__c.contains('Work')){
                UserField = Schema.SObjectType.User.fieldSets.getMap().get('UserFieldsWork');              
            }
            else{
                UserField = Schema.SObjectType.User.fieldSets.getMap().get('UserUpdate');              
            }
            id sfdcid = Obj.Id;
            
            List<Schema.FieldSetMember> UserFieldFieldObj=UserField.getFields();    
            String SOQLquery = 'select profileId,accountid, contactid,contact.name ';    
            for(Schema.FieldSetMember UserFieldFieldObjItr : UserFieldFieldObj){    
                SOQLquery = SOQLquery + ',' + UserFieldFieldObjItr.getFieldPath();    
            }    
            SOQLquery = SOQLquery + ' from user WHERE id= :sfdcid  limit 1';  
            system.debug('---SOQLquery---'+SOQLquery);
            
            
            if(Obj.Id != null)  {
                user u = database.query(SOQLquery);                   
                for(Schema.FieldSetMember UserFieldFieldObjItr1 : UserFieldFieldObj){            
                    if(String.isNotBlank((String)(u.get(UserFieldFieldObjItr1.getFieldPath())))){
                        system.debug('----'+UserFieldFieldObjItr1.getFieldPath());
                        userToSend.put(UserFieldFieldObjItr1.getFieldPath(),u.get(UserFieldFieldObjItr1.getFieldPath()));
                    }
                }
            }
            else{
                for(Schema.FieldSetMember UserFieldFieldObjItr1:UserFieldFieldObj){            
                    if(String.isNotBlank((String)(Obj.get(UserFieldFieldObjItr1.getFieldPath())))){
                        userToSend.put(UserFieldFieldObjItr1.getFieldPath(),Obj.get(UserFieldFieldObjItr1.getFieldPath()));
                    }
                }            
            }
        }catch(Exception ex){
            userToSend = Obj;
        }      
        return userToSend;  
    }
    
    
    //code to deactivate the user once password is set
    @future
    public static void updateUser(id userid){
        IDMSDeactivateUser.DeactivateUser(userid);
        
    }
    //This future method is used to set the newly create user
    @future
    Public static void setPasswordUIMS(string password, string authToken){
        IDMSUIMSWebServiceCalls uimsClsobj = new IDMSUIMSWebServiceCalls();
        uimsClsobj.uimsSetPassword(password,authToken);
        
    }
    
    @future
    //Send email
    Public static void sendemailPOST(Id UserId,Boolean Passwordsent,string appid,String signature){           
        if(Passwordsent){ 
            //Get required email template
            User idmsUser = [select Id, IdmsAppLanguage__c from User where Id =:UserId limit 1];
            system.debug('application language:'+idmsUser.IdmsAppLanguage__c);
            IDMSEmailTable__c emailTemplate = AP_IDMSEmailTemplateHandler.GetEmailTemplateCS(appid,idmsUser.IdmsAppLanguage__c);
            IDMSSendCommunication.sendEmailCreateUserWithPwd(UserId,(String)emailTemplate.EmailTemplateReg__c,appid,signature);               
        }
        else{
            //Get required email template
            User idmsUser = [select Id, IdmsAppLanguage__c from User where Id =:UserId limit 1];
            system.debug('application language:'+idmsUser.IdmsAppLanguage__c);
            IDMSEmailTable__c emailTemplate = AP_IDMSEmailTemplateHandler.GetEmailTemplateCS(appid,idmsUser.IdmsAppLanguage__c);
            IDMSSendCommunication.sendEmailCreateUserPasswordSet(UserId,(String)emailTemplate.EmailTemplateRegPwSet__c,appid,signature);               
        }                
    }
    
    
    //Send Message
    Public static void sendSMS(User UserObj){           
        IDMSSendCommunication.sendSMS(UserObj);
    }  
    
    //get user exist in IDMS or UIMS
    @HttpGet
    global static Boolean doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        IDMSResponseWrapper Response;
        String usernameEnv;
        List<User> existingContacts;
        List<User> existingusername;
        List<UIMS_User__c> existingUIMSEmail;
        String emailOrPhone = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        if(String.isNotBlank(emailOrPhone)){
            emailOrPhone=emailOrPhone.trim();
        }
        //username in case user's identity is mobile
        if(String.isNotBlank(emailOrPhone) && Pattern.matches(Label.CLJUN16IDMS31, emailOrPhone) ){
            existingContacts = [SELECT id, email FROM User WHERE MobilePhone=:emailOrPhone];
            //check for UIMS table
            existingUIMSEmail = [SELECT id FROM UIMS_User__c WHERE phoneId__c = :emailOrPhone];
            
            if(emailOrPhone.startsWith('+')){
                emailOrPhone = emailOrPhone.replace('+', '00');
            }else if(!emailOrPhone.startsWith('00')){
                emailOrPhone = '00' + emailOrPhone;
            }
            usernameEnv = emailOrPhone+Label.CLQ316IDMS102;
            if(existingUIMSEmail.size()==0){
                existingUIMSEmail = [SELECT id FROM UIMS_User__c WHERE phoneId__c = :emailOrPhone];
            }
            existingusername = [SELECT id, email FROM User WHERE username= :usernameEnv];
            
        }
        else{
            existingContacts = [SELECT id, email FROM User WHERE email = :emailOrPhone];
            usernameEnv = emailOrPhone + Label.CLJUN16IDMS71;
            existingusername = [SELECT id, email FROM User WHERE username= :usernameEnv];
            //check for UIMS table
            existingUIMSEmail = [SELECT id FROM UIMS_User__c WHERE EMail__c = :emailOrPhone];
        }
        if(existingContacts.size() > 0 || existingusername.size() > 0 || existingUIMSEmail.size()>0 )
        {
            return true;   
            
        }
        res.statuscode = integer.valueOf(Label.CLDEC16IDMS002);
        return false;   
    }
}