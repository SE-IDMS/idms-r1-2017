/*****

Release :April 2015
Comments : BOX API Connection Calls 
******/ // committed on autorabit

Public class AP_ELLAToBoxConnect {   
   
   
    public  static CS_ELLABoxAccessToken__c  boxAuthToken {get;set;}
    public static boolean isAccesChanged{get;set;}
    public AP_ELLAToBoxConnect(){
    
        boxAuthToken = new  CS_ELLABoxAccessToken__c();
      
    }
    public class bFOBoxException extends Exception {}
    
    Public static string folderStructureCreation (string folderName,string parentFolderId,string CollobratorUserEmail ) {
        string boxResponse;
        if(folderName!=null && parentFolderId!=null) {
            string HttpBodyRequestParent ='{"name":"'+folderName+'", "parent": {"id": "'+parentFolderId+'"}}';
        
            boxResponse=create_folder(HttpBodyRequestParent,CollobratorUserEmail,parentFolderId);
            
        }else {
            string HttpBodyRequest ='{"name":"'+folderName+'", "parent": {"id": "'+label.CLAPR15ELLAbFO07+'"}}';
            boxResponse=create_folder(HttpBodyRequest,CollobratorUserEmail,null);
        }
        return boxResponse;
    
    }
  
    public static  string create_folder( string BoxfolderName,string CollobratorUserEmail,string PrntFoderId) {
        isAccesChanged=false;
        //Folder Creation Box ENDPOINT  URL 
        string strEndPoint =label.CLAPRIL15ELLAbFO05;//'https://api.box.com/2.0/folders';
        string foldercollaboraId;
        boxCurrentAccessToken();
        Http h = new Http();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(strEndPoint);
        req.setHeader('Authorization', 'Bearer ' + boxAuthToken.AccessToken__c);
        
        req.setBody(BoxfolderName);
        req.setMethod(label.CLAPR15ELLAbFO30);//Post

        system.debug('HAnana12---->'+boxAuthToken .AccessToken__c);
        
        req.setTimeout(integer.valueof(label.CLAPR15ELLAbFO18));//120000
        req.setHeader(label.CLAPR15ELLAbFO31,label.CLAPR15ELLAbFO37);//Connection //label.CLAPR15ELLAbFO31,Keep-Alive


        HttpResponse res1 = h.send(req); 
        system.debug('TestGET_Post'+res1.getBody());
        if(res1.getStatus()==label.CLAPR15ELLAbFO26) {//'unauthorized'
           
            throw new bFOBoxException('Error :'+label.CLAPR15ELLAbFO15);
        }
        if(res1.getStatus()==label.CLAPR15ELLAbFO27) {//'conflict'
            String strBoxErrorJson = res1.getBody();
            boxErrorObject boxErrorResult = (boxErrorObject)JSON.deserialize(strBoxErrorJson,boxErrorObject.class);
            system.debug('HNA'+boxErrorResult);
           throw new bFOBoxException(label.CLAPR15ELLAbFO29 +boxErrorResult.message);//Error :
            //throw new bFOBoxException('Item with the same name already exists In Box.');
        }
        
        if(res1.getStatus()==label.CLAPR15ELLAbFO28) {//'internal_server_error'
            String strBoxErrorJson = res1.getBody();
            boxErrorObject boxErrorResult = (boxErrorObject)JSON.deserialize(strBoxErrorJson,boxErrorObject.class);
            throw new bFOBoxException(label.CLAPR15ELLAbFO29+boxErrorResult.message);
           // throw new bFOBoxException('Box internal server error.');
        }
        
        JSONParser parser = JSON.createParser(res1.getBody());
        
        string folderId = '';
               
        while (parser.nextToken() != null) {
        
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME) {
            
                if(parser.getText() == label.CLAPR15ELLAbFO24){//'id'

                    parser.nextToken();
                    folderId  = parser.getText();
                }
            
            }
            
            if(folderId != ''){
                break;
            }
        }
        system.debug('Hanu'+folderId);
        if(res1.getStatus()==label.CLAPR15ELLAbFO32) {//'Created'
        
            string boxfoldershareLink= box_SharedLinkFolder(folderId);
            system.debug('Test_Link'+'Folder_Id='+folderId+'ShareLink='+boxfoldershareLink);
            //subFolderCreation(folderId);
            if(PrntFoderId!=null) {
                foldercollaboraId=box_collaboraFolder(PrntFoderId,CollobratorUserEmail);
            }else {
                foldercollaboraId=box_collaboraFolder(folderId,CollobratorUserEmail);
                CreateMasterFolder(folderId);
            }
           
            
            boxResponseObject boxResp = new boxResponseObject(folderId,boxfoldershareLink,foldercollaboraId);
            String JSONString = JSON.serialize(boxResp);
            //'Folder_Id='+folderId+'ShareLink='+boxfoldershareLink;
            if(isAccesChanged) {
             update boxAuthToken;
            }
            return JSONString;
        }else {
            return null;
        }
        
    }
   
    
    //Create a Shared Link for a Folder
     
     public static string box_SharedLinkFolder(string box_FolderID) {
    
        string url=label.CLAPR15ELLAbFO33+box_FolderID;//'https://api.box.com/2.0/folders/'
        
        Http sharedHttp = new Http();
            
        HttpRequest ShareReq = new HttpRequest();
        ShareReq.setEndpoint(url);
        system.debug('%%%%%%%%%---->'+url);
        ShareReq.setHeader('Authorization', 'Bearer ' + boxAuthToken.AccessToken__c);
        ShareReq.setBody('{"shared_link": {"access": "'+label.CLAPRIL15ELLAbFO01+'"}}');
        ShareReq.setMethod(label.CLAPR15ELLAbFO34);//PUT
       
        HttpResponse shareResp= sharedHttp.send(ShareReq); 
        
        JSONParser parser = JSON.createParser(shareResp.getBody());
        
        string ShareFolderLink = '';
               
        while (parser.nextToken() != null) {
        
            if(parser.getCurrentToken() == JSONToken.FIELD_NAME) {
            
                if(parser.getText() == 'url'){//url
                    parser.nextToken();
                    ShareFolderLink  = parser.getText();
                }
            
            }
            
            if(ShareFolderLink != ''){
                break;
            }
        }
        system.debug('Tcommentbox--->'+shareResp.getBody()+'LINKBP '+ShareFolderLink);
        return ShareFolderLink; 
    
    }
    Public static void CreateMasterFolder(string parentFolderMaster) {
        //Folder Creation Box ENDPOINT  URL 
        string strEndPoint =label.CLAPRIL15ELLAbFO05;//'https://api.box.com/2.0/folders';
        string FolderName=label.CLAPR15ELLAbFO38;//'Master documents';
         string HttpBodyRequestParent ='{"name":"'+FolderName+'", "parent": {"id": "'+parentFolderMaster+'"}}';
        
        boxCurrentAccessToken();
        Http h = new Http();

        HttpRequest req = new HttpRequest();
        req.setEndpoint(strEndPoint);
        req.setHeader('Authorization', 'Bearer ' + boxAuthToken.AccessToken__c);
        
        req.setBody(HttpBodyRequestParent);
        req.setMethod(label.CLAPR15ELLAbFO30);//post

        system.debug('HAnana12---->'+boxAuthToken .AccessToken__c);
        req.setTimeout(integer.valueof(label.CLAPR15ELLAbFO18));
        req.setHeader(label.CLAPR15ELLAbFO31,label.CLAPR15ELLAbFO37);
        


        HttpResponse res1 = h.send(req); 
        system.debug('TestGET_Post'+res1.getBody());
        
        JSONParser parser = JSON.createParser(res1.getBody());
    
    }
    public static string box_collaboraFolder(string box_FolderID,string collortorEmailId) {
        //End Point Collaboration end Point URL
        system.debug('HAnana12---->'+box_FolderID);
        string collbrFolder;  
        string strEndPoint =label.CLAPRIL15ELLAbFO06;//'https://api.box.com/2.0/collaborations';
       
        CS_ELLABoxAccessToken__c tokenA = CS_ELLABoxAccessToken__c.getValues(label.CLAPR15ELLAbFO39);
        Http h = new Http();
    
        HttpRequest req = new HttpRequest();
        req.setEndpoint(strEndPoint);
        req.setHeader('Authorization', 'Bearer ' + tokenA.AccessToken__c);
    
        req.setBody('{"item": { "id": "'+box_FolderID+'", "type": "folder"}, "accessible_by": { "login": "'+collortorEmailId+'", "type": "user" }, "role": "'+label.CLMAR16ELLAbFO14+'"}');
    
        req.setMethod(label.CLAPR15ELLAbFO30);//post

        system.debug('HAnana12---->'+tokenA.AccessToken__c);
        req.setTimeout(integer.valueof(label.CLAPR15ELLAbFO18));//120000

        req.setHeader(label.CLAPR15ELLAbFO31,label.CLAPR15ELLAbFO37); //Connection //Keep-Alive'
        HttpResponse res1 = h.send(req); 
         system.debug(' res1 '+ res1 );
        if(res1.getStatusCode()>=integer.valueof(label.CLAPR15ELLAbFO35)) {// status code 300
            String strBoxErrorJson = res1.getBody();
            boxErrorObject boxErrorResult = (boxErrorObject)JSON.deserialize(strBoxErrorJson,boxErrorObject.class);
            //throw new bFOBoxException('Error :'+boxErrorResult.message);
        }
        boxCollaborationObject boxCalbrt=(boxCollaborationObject)JSON.deserialize(res1.getBody(),boxCollaborationObject.class);
        
        if(boxCalbrt.id!=null || boxCalbrt.id!='') {
            collbrFolder=boxCalbrt.id;
        }
        return collbrFolder;
    
    

    }
     
    
    //checking 
    
    public static string  boxCurrentAccessToken() {
        string strBoxCurrentAccessToken;
        //boxAuthToken = new  CS_ELLABoxAccessToken__c();
        boxAuthToken = CS_ELLABoxAccessToken__c.getValues(label.CLAPR15ELLAbFO39);
            if(boxAuthToken!=null) {
            
                if(boxAuthToken.BoxAccessTokenExpires__c < Datetime.now()) {
                    if(boxAuthToken.BoxRefreshTokenExpire__c > Datetime.now()) {
                        getAccesFromRefreshToken();
                        isAccesChanged=true;
                    }
        
                }
        
            }  
        return null;
    }
    public static void getAccesFromRefreshToken() {
        boxAuthToken = CS_ELLABoxAccessToken__c.getValues(label.CLAPR15ELLAbFO39);
       
        boxAuthAccessRequest(label.CLAPR15ELLAbFO36+ boxAuthToken.RefreshToken__c);//grant_type=refresh_token&refresh_token=
    }
   
    public  static void boxAuthAccessRequest( String boxValue) {
        
        string boxClientID =label.CLAPRIL15ELLAbFO02;//'v363zs5973gb9stitownjzq4jshn5w2m';
        string boxSecretCode =label.CLAPRIL15ELLAbFO03;//'AsW2HfisiH5cE8Gj6mdQQ89N11bIZJa1';
        string endPointValue =label.CLAPRIL15ELLAbFO04;//'https://api.box.com/oauth2/token';
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        req.setMethod(label.CLAPR15ELLAbFO30);

        req.setEndpoint(endPointValue);
        req.setBody(boxValue + '&client_id='+boxClientID +'&client_secret=' +boxSecretCode );

        HttpResponse res = http.send(req);
        String boxJsonString = res.getBody();
        
        //Update Custom setting 
        if(res.getStatusCode() ==integer.valueof(label.CLAPR15ELLAbFO40)) {//created status code 200
            UpdatedBoxAcceRefToken(boxJsonString);
        }
        system.debug('TEST---->^^^^^^'+boxJsonString);
        
        
    } 
    
    public static void UpdatedBoxAcceRefToken(string boxJsonObject)     {
    
        BoxAuthResult boxTokenResult = (BoxAuthResult)JSON.deserialize(boxJsonObject,BoxAuthResult.class);
        if(boxTokenResult!=null) {
        
            boxAuthToken.AccessToken__c = boxTokenResult.access_token;
            boxAuthToken.RefreshToken__c = boxTokenResult.refresh_token;
            boxAuthToken.BoxAccessTokenExpires__C = Datetime.now().addSeconds(boxTokenResult.expires_in);
            boxAuthToken.BoxRefreshTokenExpire__c = Datetime.now().addDays(integer.valueof(label.CLAPR15ELLAbFO17));
            
            
        
        }
        
        //update boxAuthToken;  
    
    
    }
    
    public static string addBoxCollaboraFolder(string box_FoldrID,string collbrEmailId) {
      
        string collbrFolder;
        system.debug('TEST-->'+box_FoldrID);
        collbrFolder=box_collaboraFolder(box_FoldrID,collbrEmailId);
        
         if(collbrFolder!=null || collbrFolder!='' ) {
             boxResponseObject boxResp = new boxResponseObject(null,null,collbrFolder);
             String JSONString = JSON.serialize(boxResp);
             return JSONString;
         }else {
             return null;
         
         }
    
    
    }
    public class BoxAuthResult {
        public String access_token {get;set;}
        public Integer expires_in {get;set;}
        public List<String> restricted_to {get;set;}
        public String refresh_token {get;set;}
        public String token_type {get;set;}
    } 
    
    public class boxResponseObject {
        string folderId{get;set;}
        string folderShareLink{get;set;}
        string CollaborationId{get;set;}
        
        
        public boxResponseObject(string boxfolderId, string sLink,string colbrId )
        {
            folderId =boxfolderId;
            folderShareLink=sLink; 
            CollaborationId=colbrId;
            
        }
    }
    
    public class boxErrorObject {
    
        public string type {get;set;}
        public integer status{get;set;}
        public string code{get;set;}
        public string help_url{get;set;}
        public string message {get;set;}
        public string request_id{get;set;}
        
    
    }
    public class boxCollaborationObject {
    
        public string type {get;set;}
        public string id{get;set;}
        public miniUserObject created_by{get;set;}
        public DateTime created_at{get;set;}
        public DateTime modified_at {get;set;}
        public DateTime expires_at{get;set;}
        public string status{get;set;}
        public miniUserObject accessible_by{get;set;}
        public string role{get;set;}
        public DateTime acknowledged_at {get;set;}
        public minifolderobject item{get;set;}
        
    
    }
     public class miniUserObject{
        public String type {get;set;}
        public String id {get;set;}
        public String name {get;set;}
        public String login {get;set;}
    }
    
    public class miniFolderObject {
        public String type {get;set;}
        public String id {get;set;}
        public String sequence_id {get;set;}
        public String etag {get;set;}
        public String sha1 {get;set;}
        public String name {get;set;}
    }

}