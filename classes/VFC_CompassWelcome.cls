/*******************************************/
//
// Customer Self-Service Portal (Compass)
//
// Stephen Norbury and Brice Nofiele
//
//
/*******************************************/

public class VFC_CompassWelcome {
 
    public String password { get; set; }
    public String username { get; set; }
    
    public boolean LoginPanel             {get; set;}
    public boolean ForgottenPasswordPanel {get; set;}
    public boolean HomePanel              {get; set;}
    
    // Authentication
        
    public PageReference login()
    {
        PageReference HomePage = null;
        
        if ((username != null && username.length() != 0) &&
            (password != null && password.length() != 0))
        {
          HomePage = Site.login(username, password, '/apex/VFP_PortletCasesView');
          
          // Authenticated?
      
          HomePanel              = true;
          LoginPanel             = false; 
          ForgottenPasswordPanel = true;
          
          if (UserInfo.getUserId() != null)
          {
            User CustomerUser = [select ContactId from User where Id = :UserInfo.getUserId()][0];
        
            if (CustomerUser != null)
            {
              //HomePage = new PageReference ('apex/VFP_CompassSupport'); //Page.VFP_CompassSupport; // Switch to the Compass Support
            }
          }
        }
                
        return HomePage; 
    }
    
    public PageReference forgottenpassword() 
    {
        HomePanel              = true;
        LoginPanel             = false; 
        ForgottenPasswordPanel = true; 
                  
        return null;
    }

    public PageReference ok() 
    {
        if (username != null && username.length() != 0 && Site.forgotPassword(username))
        {
          HomePanel              = true;
          LoginPanel             = true; 
          ForgottenPasswordPanel = false; 
        }
        else
        {
          HomePanel              = true;
          LoginPanel             = false; 
          ForgottenPasswordPanel = true; 
        }
        
        return null;
    }

    public PageReference cancel() 
    {
        HomePanel              = true;
        LoginPanel             = true; 
        ForgottenPasswordPanel = false; 
                
        return null;
    }

    
    public VFC_CompassWelcome () 
    {
      HomePanel = true; LoginPanel = true; ForgottenPasswordPanel = false;
    }
    
    // End
}