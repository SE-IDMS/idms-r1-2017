/*
    Author          : Nicolas Palitzyne ~ nicolas.palitzyne@accenture.com 
    Date Created    : 29/04/2011
    Description     : Utility class for PM0 Picklist Management
*/

Public Class Utils_PM0PicklistManager implements Utils_PicklistManager
{
    Map<String,String> ProductLineMap; // Map between Values and Labels for the product Line Picklist
    Map<String,String> ProductFamilyMap; // Map between Values and Labels for the Product Family Picklist
    
    // Return name of the levels
    Public list<String> getPickListLabels()
    {
        List<String> Labels = new List<String>();
        Labels.add(Label.CL00351);
        Labels.add(Label.CL00352);
        Labels.add(Label.CL00353);
        Labels.add(Label.CL00354);
        return Labels;
    } 

    // Return dependent picklist value 
    public List<SelectOption> getPicklistValues(Integer i, String S)
    {
        if(ProductLineMap == null)
        {
            ProductLineMap = new Map<String,String>(); 
            Schema.DescribeFieldResult pickList = Schema.sObjectType.OPP_Product__c.fields.ProductLine__c;
            
            for (Schema.PickListEntry PickVal : pickList.getPicklistValues())
            {
                ProductLineMap.put(PickVal.getValue(),PickVal.getLabel());
            }
        }
              
        if(ProductFamilyMap == null)
        {
            ProductFamilyMap = new Map<String,String>(); 
            Schema.DescribeFieldResult pickList = Schema.sObjectType.OPP_Product__c.fields.ProductFamily__c;
            
            for (Schema.PickListEntry PickVal : pickList.getPicklistValues())
            {
                ProductFamilyMap.put(PickVal.getValue(),PickVal.getLabel());
            }
        }
             
       List<SelectOption> options = new  List<SelectOption>();
       
       If(S == Label.CL00355)
           return null;
          
       options.add(new SelectOption(Label.CL00355,Label.CL00355)); 
 
       // Populate Business fields
       if(i==1)
       {
            Schema.DescribeFieldResult Business = Schema.sObjectType.OPP_Product__c.fields.BusinessUnit__c;
            for (Schema.PickListEntry PickVal : Business.getPicklistValues())
            {
                options.add(new SelectOption(PickVal.getValue(),PickVal.getLabel()));
            }
       } 
       
       // Select a Product Line
       if(i==2)
       {          
           For(OPP_Product__c P:[SELECT id, Name, BusinessUnit__c, Family__c, ProductFamily__c, ProductLine__c FROM OPP_Product__c WHERE BusinessUnit__c=:S AND ProductFamily__c=null AND ProductLine__c!=null])
           {
               if(ProductLineMap.get(P.ProductLine__c) != null)
                    options.add(new SelectOption(P.ProductLine__c,ProductLineMap.get(P.ProductLine__c))); 
               else            
                    options.add(new SelectOption(P.ProductLine__c,P.ProductLine__c));    
           } 
       } 
    
       // Select a Product Family
       if(i==3)
       {
           For(OPP_Product__c P:[SELECT id, Name, BusinessUnit__c, Family__c, ProductFamily__c, ProductLine__c FROM OPP_Product__c WHERE ProductLine__c=:S AND Family__c=null AND ProductFamily__c!=null])
           {
               if(ProductFamilyMap.get(P.ProductFamily__c) != null)
                    options.add(new SelectOption(P.ProductFamily__c,ProductFamilyMap.get(P.ProductFamily__c))); 
               else 
                   options.add(new SelectOption(P.ProductFamily__c,P.ProductFamily__c));         
           } 
       } 
       
       // Select a Family
       if(i==4)
       {
           For(OPP_Product__c P:[SELECT id, Name, BusinessUnit__c, Family__c, ProductFamily__c, ProductLine__c FROM OPP_Product__c WHERE ProductFamily__c=:S AND Family__c!=null])
           {
                options.add(new SelectOption(P.Family__c,P.Family__c));    
           } 
       } 

       return  options;  
    }
}