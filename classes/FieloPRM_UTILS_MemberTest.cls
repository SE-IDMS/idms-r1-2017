/********************************************************************
* Company: Fielo
* Created Date: 11/08/2016
* Description: Test class for class FieloPRM_UTILS_Member
********************************************************************/

@isTest
public without sharing class FieloPRM_UTILS_MemberTest {
    
    @testSetup
    static void setupTest() {
    }
    
    @isTest
    static void unitTest1(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
            update program;
        
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.POBox__c = 'test';
            insert acc;
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'testNamePolo'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'testNameMarco';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
            member.F_PRM_PrimaryChannel__c = 'UK';
            
            insert member;
            
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = '123';
            con1.FieloEE__Member__c = member.id;
            insert con1;
            
            Account acc2 = new Account();
            acc2.Name = 'test acc2';
            acc2.POBox__c = 'test2';
            insert acc2;
            
            FieloEE__Member__c member2 = new FieloEE__Member__c();
            member2.FieloEE__LastName__c= 'testNamePolo'+ String.ValueOf(DateTime.now().getTime());
            member2.FieloEE__FirstName__c = 'testNameMarco';
            member2.FieloEE__Street__c = 'test';
            member2.F_PRM_PrimaryChannel__c = 'UK';
            
            insert member2;
            
            //Create portal account owner
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
            User portalAccountOwner1 = new User(
                UserRoleId = portalRole.Id,
                ProfileId = profile1.Id,
                Username = System.now().millisecond() + 'test2@test.com',
                Alias = 'batman',
                Email='bruce.wayne@abengoa.com',
                EmailEncodingKey='UTF-8',
                Firstname='Bruce',
                Lastname='Wayne',
                LanguageLocaleKey='en_US',
                LocaleSidKey='en_US',
                TimeZoneSidKey='America/Chicago'
            );
            Database.insert(portalAccountOwner1);
             
            //Create account
            Account portalAccount1 = new Account(
                Name = 'TestAccount',
                OwnerId = portalAccountOwner1.Id
            );
            Database.insert(portalAccount1);

            
            Contact con2 = new Contact();
            con2.FirstName = 'test2';
            con2.LastName = 'tset2';
            con2.SEContactID__c = 'test22';
            con2 .PRMUIMSId__c = '1234';
            con2.AccountId = portalAccount1.Id;
            con2.FieloEE__Member__c = member2.id;
            insert con2;
            
            Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE '%Portal User%' Limit 1];

            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = portalProfile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',BypassVR__c = true,ContactId = con2.id);
            insert u;
            
        
            FieloPRM_UTILS_Member.getBaseLanguage();
            FieloPRM_UTILS_Member.postMoveMemberByContact(con1.id, con2.id);
            FieloPRM_UTILS_Member.postMoveMembersByAccount(acc.id,acc2.id);
            FieloPRM_UTILS_Member.getMemberPrimaryChannel(member);
        }
    
    }
}