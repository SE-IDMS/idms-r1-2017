/********************************************************************
* Company: Fielo
* Created Date: 08/08/2016
* Description: 
********************************************************************/
@isTest
public with sharing class FieloPRM_Batch_BadgeMemberExpirationTest {
    
    @testSetup static void methodName() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        FieloEE.MockUpFactory.setCustomProperties(false);

        FieloEE__Member__c member = FieloEE.MockUpFactory.createMember('Test member', '1234', 'DNI');
        FieloEE__Member__c member1 = FieloEE.MockUpFactory.createMember('Test member1', '12341', 'DNI1');
        member = [select FieloEE__User__c from fieloee__member__c where id=:member.Id]; 
        
        FieloEE__Badge__c badge = new FieloEE__Badge__c (
            Name='test', 
            F_PRM_Type__c = 'Program Level', 
            F_PRM_BadgeAPIName__c = '1'
        );
        
        FieloEE__Badge__c badge2 = new FieloEE__Badge__c (
            Name='test2', 
            F_PRM_Type__c = 'Program Level', 
            F_PRM_BadgeAPIName__c = '2'
        );
        
        insert new List<FieloEE__Badge__c>{badge, badge2}; 
        
        FieloEE__BadgeMember__C badgemember = new FieloEE__BadgeMember__C(
            FieloEE__Badge2__c = badge.id,
            FieloEE__Member2__c = member.id,
            F_PRM_Status__c = 'Active'
        );
        insert badgemember;

        FieloCH__Challenge__c challenge =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');
        FieloCH__ChallengeReward__c rew = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge);
        rew.FieloCH__Badge__c = badge.id;
        update rew;
        
        badgemember.F_PRM_ExpirationDateText__c = FieloPRM_UTILS_BadgeMember.getExpirationDateText(Date.today());
        update badgemember;
        
        CS_PRM_ApexJobSettings__c conf = new CS_PRM_ApexJobSettings__c(
            Name ='FieloPRM_Batch_BadgeMemberExpiration', 
            F_PRM_TryCount__c = 5, 
            F_PRM_BatchSize__c = 50, 
            F_PRM_SyncAmount__c = 10
        );
        insert conf;
        
    }
    
    @isTest static void test_method_one() {
        FieloPRM_Batch_BadgeMemberExpiration bat = new FieloPRM_Batch_BadgeMemberExpiration();
        Test.startTest();
        ID batchprocessid = Database.executeBatch(bat);        
        FieloPRM_Batch_BadgeMemberExpiration.scheduleNow();
        Test.stopTest();
    }
  
}