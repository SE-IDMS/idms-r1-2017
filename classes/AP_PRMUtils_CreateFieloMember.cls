public class AP_PRMUtils_CreateFieloMember {

    public class ContactIds {
        @InvocableVariable(Label='lstContactId' description='List of PRM Contacts for which the member needs to be associated' Required=true) 
        public List<Id> lContactId;
    }

    @InvocableMethod(label='Get List of PRM Contact to create Fielo Member' description='This method is called from flow on User')
    public static void CreateMember(List<ContactIds> mUserList) {
        List<UIMSCompany__c> lUCmp = new List<UIMSCompany__c>();    
        try{
            Id programDefaultId = System.label.CLAPR15PRM149;
            for(ContactIds contactId : mUserList) {
                lUCmp = [SELECT Id,bFOContact__c,InformationMessage__c FROM UIMSCompany__c WHERE bFOContact__c =:contactId.lContactId]; 
                Map<Id,String> errorsResult = FieloEE.ContactConvertion.convertContacts(contactId.lContactId,programDefaultId);
                System.debug('**** Fielo Result->'+errorsResult);
                if(lUCmp != null && lUCmp.size() > 0)
                    lUCmp[0].MemberCreationStatus__c = True;
            }
        } Catch(Exception e){
            System.debug('--Exception Here PRM Contact Insert--'+e.getMessage() + ' ' + e.getStackTraceString());
            if(lUCmp != null && lUCmp.size() > 0)
                lUCmp[0].InformationMessage__c = e.getMessage() + ' ' + e.getStackTraceString();
        }
        finally{
            if(lUCmp != null && lUCmp.size() > 0)
                UPDATE lUcmp;
        
        }
}}