/*
    Author          : Srinivas Nallapati
    Date Created    : 22/June/2012
    Description     : For scheduling NoOfOpenLeadsUpdate batch job.
                        
*/
global class SCA02_NoOfOpenLeadsUpdateBatch implements Schedulable{
    
    global void execute(SchedulableContext sc) {
        Batch_NoOfOpenLeadsUpdate updateContacts = new Batch_NoOfOpenLeadsUpdate();
        Id batchprocessid = Database.executeBatch(updateContacts);
        system.debug(batchprocessid);
    }
    
}