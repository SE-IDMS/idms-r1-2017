/******************************************************/
/*
/* Question of the Week; 
/*
/* Question Controller
/*
/* Created on: 26/12/2012 
/*
/* Purpose:
/*  handle the QOTWs on Person Accounts
/*
/* Author: Pooja Gupta
/******************************************************/

public with sharing class VFC_AccountQuestionController 
{    
  
  public List <String>       Choices        = new List <String>();
  public List <String>       EnglishChoices = new List <String>();
  public List <AnswerChoice> AnswerChoices  = new List <AnswerChoice>();
  public List <SelectOption> Options        = new List <SelectOption>();
  
  public List <SelectOption> getChoices() 
  {
    return options; 
  }

  public List <AnswerChoice> getAnswerChoices ()    
  {
    return AnswerChoices;    
  }

  public String   Choice                  { get; set;}
  public String   Query                   { get; set;}
  public String   Answer                  { get; set;}
  public String   OtherAnswer             { get; set;}
  public boolean  OtherAnswerSelected     { get; set;}
  public String   OtherRemarks            { get; set;}
  public boolean  Declined                { get; set;}
  public boolean  QuestionNotAvailable    { get; set;}
  public boolean  QuestionAlreadyAnswered { get; set;}

  public class AnswerChoice 
  {
    public String  Answer          { get; set;}
    public String  EnglishAnswer   { get; set;}
    public boolean Selected        { get; set;}    
  } 
  
  public Account              customer { get; set;}
  public QuestionTemplate__c Template { get; set;}

  public String OpeningComments { get; set;}
  public String ClosingComments { get; set;}
  public String DisplayStyle    { get; set;}

  CustomerQuickQuestion__c PreviousResponse = null;
  
  /* Constructor */
             
  public VFC_AccountQuestionController (ApexPages.StandardController controller)   
  {      
    customer = (Account) controller.getRecord();
 
    // Fetch the Active QOTW
    
    List <QOTW__c> questions = [select Id, StartDate__c, EndDate__c from QOTW__c where IsActive__c = true];
    
    if (questions != null && questions.size() == 1)
    {
      // Fetch the Question Templates
      
      QOTW__c Question = questions[0];
       
      List <QuestionTemplate__c> templates = [select Id, OpeningComments__c, ClosingComments__c, Question__c, Answers__c, DisplayStyle__c, IncludeOther__c, IncludeCommentsBox__c, Language__c from QuestionTemplate__c where QuestionoftheWeek__c = :Question.Id];
    
      if (templates != null && templates.size() != 0)
      {
        // Select the template by Language and Build the Page
    
        //AF Afrikaans
        //AR Arabic
        //AZ Azerbaijani
        //BG Bulgarian
        //CA Catalan
        //CS Czech
        //DA Danish
        //DE German
        //EL Greek
        //EN English
        //ES Spanish
        //ET Estonian
        //FI Finnish
        //FR French
        //HE Hebrew
        //HR Croatian
        //HU Hungarian
        //ID Indonesian
        //IS Icelandic
        //IT Italian
        //JA Japanese
        //JP Japanese
        //KA Georgian
        //KO Korean
        //LT Lithuanian
        //LV Latvian
        //MS Malaysian
        //NL Dutch
        //NO Norwegian
        //PL Polish
        //PT Portuguese
        //RO Romanian
        //RU Russian
        //SH Serbo-Croatian
        //SK Slovakian
        //SL Slovenian
        //SQ Albanian
        //SR Serbian
        //SV Swedish
        //TG Tajik
        //TH Thai
        //TK Turkmen
        //TR Turkish
        //UK Ukrainian
        //UZ Uzbek

        // Ensure that the Contact has a Prefered Language or default to English
                
        if (customer.CorrespLang__pc == null || customer.CorrespLang__pc.length() == 0) customer.CorrespLang__pc = 'EN';
        
        // Fetch the matching template by Language
        
        Template = null; QuestionTemplate__c EnglishTemplate = null;
         
        for (QuestionTemplate__c t : templates)
        {
          // Match the Language
          
          if (t.Language__c == 'English') 
          {                    
            EnglishTemplate = t;
          }
        }
        
        for (QuestionTemplate__c t : templates)
        {
          // Match the Language
          
          if (t.Language__c == 'English') 
          {                    
            EnglishTemplate = t;
          }

          if (customer.CorrespLang__pc == 'AF' && t.Language__c == 'Afrikaans'      ||
              customer.CorrespLang__pc == 'AR' && t.Language__c == 'Arabic'         ||
              customer.CorrespLang__pc == 'AZ' && t.Language__c == 'Azerbaijani'    ||
              customer.CorrespLang__pc == 'BG' && t.Language__c == 'Bulgarian'      ||  
              customer.CorrespLang__pc == 'CA' && t.Language__c == 'Catalan'        ||
              customer.CorrespLang__pc == 'CS' && t.Language__c == 'Czech'          ||
              customer.CorrespLang__pc == 'DA' && t.Language__c == 'Danish'         ||
              customer.CorrespLang__pc == 'DE' && t.Language__c == 'German'         ||
              customer.CorrespLang__pc == 'EL' && t.Language__c == 'Greek'          ||
              customer.CorrespLang__pc == 'EN' && t.Language__c == 'English'        ||
              customer.CorrespLang__pc == 'ES' && t.Language__c == 'Spanish'        ||
              customer.CorrespLang__pc == 'ET' && t.Language__c == 'Estonian'       ||
              customer.CorrespLang__pc == 'FI' && t.Language__c == 'Finnish'        ||
              customer.CorrespLang__pc == 'FR' && t.Language__c == 'French'         ||
              customer.CorrespLang__pc == 'HE' && t.Language__c == 'Hebrew'         ||
              customer.CorrespLang__pc == 'HR' && t.Language__c == 'Croatian'       ||
              customer.CorrespLang__pc == 'HU' && t.Language__c == 'Hungarian'      ||   
              customer.CorrespLang__pc == 'ID' && t.Language__c == 'Indonesian'     ||
              customer.CorrespLang__pc == 'IS' && t.Language__c == 'Icelandic'      ||
              customer.CorrespLang__pc == 'IT' && t.Language__c == 'Italian'        ||
              customer.CorrespLang__pc == 'JA' && t.Language__c == 'Japanese'       ||
              customer.CorrespLang__pc == 'JP' && t.Language__c == 'Japanese'       ||
              customer.CorrespLang__pc == 'KA' && t.Language__c == 'Georgian'       ||
              customer.CorrespLang__pc == 'KO' && t.Language__c == 'Korean'         ||
              customer.CorrespLang__pc == 'LT' && t.Language__c == 'Lithuanian'     ||
              customer.CorrespLang__pc == 'LV' && t.Language__c == 'Latvian'        ||
              customer.CorrespLang__pc == 'MS' && t.Language__c == 'Malaysian'      ||
              customer.CorrespLang__pc == 'NL' && t.Language__c == 'Dutch'          ||
              customer.CorrespLang__pc == 'NO' && t.Language__c == 'Norwegian'      ||   
              customer.CorrespLang__pc == 'PL' && t.Language__c == 'Polish'         ||
              customer.CorrespLang__pc == 'PT' && t.Language__c == 'Portuguese'     ||
              customer.CorrespLang__pc == 'RO' && t.Language__c == 'Romanian'       ||
              customer.CorrespLang__pc == 'RU' && t.Language__c == 'Russian'        ||
              customer.CorrespLang__pc == 'SH' && t.Language__c == 'Serbo-Croatian' ||
              customer.CorrespLang__pc == 'SK' && t.Language__c == 'Slovakian'      ||
              customer.CorrespLang__pc == 'SL' && t.Language__c == 'Slovenian'      ||
              customer.CorrespLang__pc == 'SQ' && t.Language__c == 'Albanian'       ||
              customer.CorrespLang__pc == 'SR' && t.Language__c == 'Serbian'        ||
              customer.CorrespLang__pc == 'SV' && t.Language__c == 'Swedish'        ||
              customer.CorrespLang__pc == 'TG' && t.Language__c == 'Tajik'          ||
              customer.CorrespLang__pc == 'TH' && t.Language__c == 'Thai'           ||          
              customer.CorrespLang__pc == 'TK' && t.Language__c == 'Turkmen'        ||
              customer.CorrespLang__pc == 'TR' && t.Language__c == 'Turkish'        ||
              customer.CorrespLang__pc == 'UK' && t.Language__c == 'Ukrainian'      ||
              customer.CorrespLang__pc == 'UZ' && t.Language__c == 'Uzbek')
 
          {                    
            Template = t; break;
          }
        }
        
        // Use English Template if preferred Language Template does not exist
        
        QuestionNotAvailable = false;

        if (Template == null) 
        {
          // Use the English Template
          
          if (EnglishTemplate != null)
          {
            Template = EnglishTemplate;
          }
          else
          {
            QuestionNotAvailable = true;
          }
        }
        
        if (Template != null && EnglishTemplate != null)
        {
            Choices        = template       .Answers__c != null ? template       .Answers__c.split('\n') : null;
            EnglishChoices = EnglishTemplate.Answers__c != null ? EnglishTemplate.Answers__c.split('\n') : null;
  
            if (Choices != null)
            {
              AnswerChoice answer = null;
      
              String Choice = null;
              
              for (Integer i=0; i<Choices.size(); i++)
              {
                answer = new AnswerChoice();
        
                Choice = Choices[i];
                Choice = Choice.replaceAll(';','');
                Choice = Choice.replaceAll('\n','');
                Choice = Choice.replaceAll('\r','');
                answer.Answer        = Choices       [i]; // Native  Language Choice;
                answer.EnglishAnswer = EnglishChoices[i]; // English Language Choice;

                answer.Selected = false;
        
                AnswerChoices.add (answer);
        
                Options.add(new SelectOption(Choice,Choice));
              }
            }
    
            if (template.IncludeOther__c == true)
            {
              Options.add(new SelectOption('Other','Other'));
            }
        }
      }
      
      // See if the Contact has already answered the Question
      
      List <CustomerQuickQuestion__c> responses = [select Id, Question__c, Response__c, Remarks__c, Declined__c from CustomerQuickQuestion__c where Account__c = :Customer.Id and QuestionTemplate__c = :Template.Id];
    
      if (responses != null && responses.size() == 1)
      {
        PreviousResponse = responses[0];

        Query  = PreviousResponse.Question__c != null ? PreviousResponse.Question__c : '';
        Answer = PreviousResponse.Response__c != null ? PreviousResponse.Response__c : '';
    
        OtherRemarks = PreviousResponse.Remarks__c;
        Declined     = PreviousResponse.Declined__c;

        QuestionAlreadyAnswered = true;
      }
      else
      {
        QuestionAlreadyAnswered = false;
      }
    }
    else
    {
      // Active QotW is not available
      
      QuestionNotAvailable = true;
    }
  }
  
  public PageReference SaveResponse ()
  {
    // Construct the Customer Response
    
    if (PreviousResponse != null)
    {
      // Existing response

      PreviousResponse.Response__c = Choice != null ? Choice : '';
    
      for (AnswerChoice choice : AnswerChoices)
      {
        if (choice.Selected)
        { 
          PreviousResponse.Response__c += ';' + choice.EnglishAnswer;
        }
      }
    
      if (OtherAnswerSelected != null && OtherAnswerSelected && OtherAnswer != null) PreviousResponse.Response__c += ';' + OtherAnswer;

      PreviousResponse.Remarks__c  = OtherRemarks;
      PreviousResponse.Declined__c = Declined;

      update PreviousResponse;     
    }
    else
    {
      // New Response
      
      CustomerQuickQuestion__c response = new CustomerQuickQuestion__c();
    
//      response.Contact__c           = customer.Id;
//      response.Account__c           = customer.AccountId;
      response.Account__c             = customer.Id;
      List <Contact> ContactDetail = [select Id from Contact where AccountID = :Customer.ID];
      response.Contact__c           = ContactDetail[0].Id;              
      response.QuestionTemplate__c  = template.Id;
      response.Question__c          = template.Question__c;

      response.Response__c = Choice != null ? Choice : '';
    
      for (AnswerChoice choice : AnswerChoices)
      {
        if (choice.Selected)
        { 
          response.Response__c += ';' + choice.EnglishAnswer;
        }
      }
    
      if (OtherAnswerSelected != null && OtherAnswerSelected && OtherAnswer != null) response.Response__c += ';' + OtherAnswer;

      response.Remarks__c  = OtherRemarks;
      response.Declined__c = Declined;    
     
      insert response; 
     
    }
    
    return null;
  } 
  // End        
}