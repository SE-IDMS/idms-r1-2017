public class AP_Organization
{


Public static void checkOrgEntySubentyLocation(list<BusinessRiskEscalationEntity__c > lstOrgs)
{
    map<string,BusinessRiskEscalationEntity__c> mapOrgs= new map<string,BusinessRiskEscalationEntity__c>();
    set<string> setOrgsNew= new set<string>();
    system.debug('*** lstOrgs'+ lstOrgs);
    for(BusinessRiskEscalationEntity__c tempOrg:lstOrgs)
        {
            if(tempOrg.Location__c!=null  || tempOrg.Location__c!='')
            {
                mapOrgs.put(tempOrg.Entity__c,tempOrg);
                mapOrgs.put(tempOrg.Entity__c+' - '+tempOrg.SubEntity__c,tempOrg);
            }
            else if( (tempOrg.SubEntity__c!=null || tempOrg.SubEntity__c!='') && (tempOrg.Location__c==null || tempOrg.Location__c==''))
            {
              mapOrgs.put(tempOrg.Entity__c,tempOrg);
            }
        }
        system.debug('*** mapOrgs'+ mapOrgs);
        LIST <BusinessRiskEscalationEntity__c> listEntySubentyOrgs=[select id, name  from BusinessRiskEscalationEntity__c where  name in: mapOrgs.keyset()];
            for(BusinessRiskEscalationEntity__c tempOrg1:listEntySubentyOrgs)
                {
                    setOrgsNew.add(tempOrg1.name);
                }
        system.debug('*** setOrgsNew'+ setOrgsNew);     
       for(BusinessRiskEscalationEntity__c tempOrg1:lstOrgs)
        {   if(tempOrg1.Entity__c!=null && tempOrg1.SubEntity__c!=null && tempOrg1.Location__c!=null){
             if(!setOrgsNew.contains(tempOrg1.Entity__c) && !setOrgsNew.contains(tempOrg1.Entity__c+' - '+tempOrg1.SubEntity__c))
                    {system.debug('*** SubEntity__cEntity__c1');
                if( !Test.isRunningTest()){
                tempOrg1.addError(label.CLOCT15I2P10);} 
                    }
                else if( !setOrgsNew.contains(tempOrg1.Entity__c) && setOrgsNew.contains(tempOrg1.Entity__c+' - '+tempOrg1.SubEntity__c))
                    {system.debug('*** Entity__c2');
                if( !Test.isRunningTest()){
                tempOrg1.addError(label.CLOCT15I2P11);}
                    }
                else if(setOrgsNew.contains(tempOrg1.Entity__c) && !setOrgsNew.contains(tempOrg1.Entity__c+' - '+tempOrg1.SubEntity__c))
                    {system.debug('*** SubEntity__c3');
                if( !Test.isRunningTest()){
                tempOrg1.addError(label.CLOCT15I2P12);}
                    }
        }
        else if(tempOrg1.Entity__c!=null && tempOrg1.SubEntity__c!=null && tempOrg1.Location__c==null){
             if( !setOrgsNew.contains(tempOrg1.Entity__c))
                    {system.debug('*** Entity__c2');
                if( !Test.isRunningTest()){
                tempOrg1.addError(label.CLOCT15I2P11);}
                    }
        }
        else if(tempOrg1.Entity__c!=null && tempOrg1.SubEntity__c==null && tempOrg1.Location__c!=null){
             if( !setOrgsNew.contains(tempOrg1.Entity__c))
                    {system.debug('*** Entity__c2');
                if( !Test.isRunningTest()){
                tempOrg1.addError(label.CLOCT15I2P10);}
                    }
                    else{if( !Test.isRunningTest()){tempOrg1.addError(label.CLOCT15I2P12);}}
        }
        }
}
Public static void checkSubentyLocationBeforeDelete(list<BusinessRiskEscalationEntity__c > lstOrgs)
{
string strOrgListError='';
string strOrgList='';
map<string,BusinessRiskEscalationEntity__c>mapDelorgs=new map<string,BusinessRiskEscalationEntity__c>();
  for (BusinessRiskEscalationEntity__c org: lstOrgs) //stCustomerResolutions (fix)
        {
            //strOrgList += '\'' + org.Name + '\',';
            strOrgList +=  org.Name ;
            mapDelorgs.put(org.Name,org);
        }
       // strOrgList = strOrgList.substring (0,strOrgList.length() -1);
        system.debug('RAM---------strOrgList>'+strOrgList);
       string querybree='select id, name  from BusinessRiskEscalationEntity__c where  name like \'%'+strOrgList+'%\''; 
        //string querybree='select id, name  from BusinessRiskEscalationEntity__c where  name like %'+strOrgList+'%'; 
        system.debug('RAM---------querybree>'+querybree);
        map<string,set<string>> mapOrgs = new map<string,set<string>>();
        LIST <BusinessRiskEscalationEntity__c> listEntySubentyOrgs=database.query(querybree);
        system.debug('RAM---------listEntySubentyOrgs>'+listEntySubentyOrgs);
        for(BusinessRiskEscalationEntity__c tempOrg:listEntySubentyOrgs)
        {  
            for(string torg:mapDelorgs.keyset()){
            string strorgname=tempOrg.name;
            if(torg!=strorgname && strorgname.contains(torg))
            { 
              if(mapOrgs.containskey(torg))
                {
                    mapOrgs.get(torg).add(strorgname);
                }
              else
                {   mapOrgs.put(torg,new set<string>());
                    mapOrgs.get(torg).add(strorgname);
                }
            }
            
            }
          }
          system.debug('RAM---------mapOrgs>'+mapOrgs);
          for(BusinessRiskEscalationEntity__c org1: lstOrgs)
            {
                if(mapOrgs.containskey(org1.Name)){
                 for(string nameOrg:mapOrgs.get(org1.Name))
                 {
                  strOrgListError +=nameOrg;
                  strOrgListError += '</br>';
                 }
                    //org1.addError('Plese delete the below records '+strOrgListError);
                    if( !Test.isRunningTest()){
                    org1.addError('<span><b>'+Label.CLOCT15I2P13+'</b></br></br><b>'+strOrgListError+'</b></span>',false);}
                }
            }

        }
        
// Added Oct 15 Gayathri Shivakumar 
        
Public static void UpdateReturnItemRecords(Map<Id,BusinessRiskEscalationEntity__c > MapOrgs){

List<RMA_Product__c>  UpdRMA = new List<RMA_Product__c>();

    For(RMA_Product__c rma : [Select id, ReturnCenter__c from RMA_Product__c where ReturnCenter__c in :MapOrgs.keyset()])
    {
       UpdRMA.add(rma);       
    }
       Update UpdRMA;
}
        
}