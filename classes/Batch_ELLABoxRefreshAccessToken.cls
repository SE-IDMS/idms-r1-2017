/*****
Release :April 2015 
Batch class to refresh the token
Divya M
******/

global class Batch_ELLABoxRefreshAccessToken implements Database.Batchable<sObject>,Database.AllowsCallouts{   
   
   
    global  static CS_ELLABoxAccessToken__c  boxAuthToken;
    
    global final String Query = 'select AccessToken__c,BoxAccessTokenExpires__c,BoxRefreshTokenExpire__c,RefreshToken__c from CS_ELLABoxAccessToken__c';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        boxCurrentAccessToken();
    }
    //checking 
    
    public static string  boxCurrentAccessToken() {
        string strBoxCurrentAccessToken;
        boxAuthToken = new  CS_ELLABoxAccessToken__c();
        boxAuthToken = CS_ELLABoxAccessToken__c.getValues('Admin');
            if(boxAuthToken!=null) {
               
                    if(boxAuthToken.BoxRefreshTokenExpire__c > Datetime.now()) {
                        getAccesFromRefreshToken();
                       
                    }                  
        
            }  
        return null;
    }
    public static void getAccesFromRefreshToken() {
        boxAuthToken = CS_ELLABoxAccessToken__c.getValues('Admin');
       
        boxAuthAccessRequest('grant_type=refresh_token&refresh_token=' + boxAuthToken.RefreshToken__c);
    }
   
    public  static void boxAuthAccessRequest( String boxValue) {
        
        string boxClientID =label.CLAPRIL15ELLAbFO02;//'v363zs5973gb9stitownjzq4jshn5w2m';
        string boxSecretCode =label.CLAPRIL15ELLAbFO03;//'AsW2HfisiH5cE8Gj6mdQQ89N11bIZJa1';
        string endPointValue =label.CLAPRIL15ELLAbFO04;//'https://api.box.com/oauth2/token';
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        req.setMethod('POST');
        req.setEndpoint(endPointValue);
        req.setBody(boxValue + '&client_id='+boxClientID +'&client_secret=' +boxSecretCode );

        HttpResponse res = http.send(req);
        String boxJsonString = res.getBody();
        
        //Update Custom setting 
        if(res.getStatusCode() ==200) {
            UpdatedBoxAcceRefToken(boxJsonString);
        }
        system.debug('TEST---->^^^^^^'+boxJsonString);
        
        
    } 
    
    global void finish(Database.BatchableContext BC){

    }
    public static void UpdatedBoxAcceRefToken(string boxJsonObject)     {

        BoxAuthResult boxTokenResult = (BoxAuthResult)JSON.deserialize(boxJsonObject,BoxAuthResult.class);
        if(boxTokenResult!=null) {
            boxAuthToken.AccessToken__c = boxTokenResult.access_token;
            boxAuthToken.RefreshToken__c = boxTokenResult.refresh_token;
            boxAuthToken.BoxAccessTokenExpires__C = Datetime.now().addSeconds(boxTokenResult.expires_in);
            boxAuthToken.BoxRefreshTokenExpire__c = Datetime.now().addDays(60);
        }
        update boxAuthToken;  

    }
    // Inner Class Object
    public class BoxAuthResult {
        public String access_token {get;set;}
        public Integer expires_in {get;set;}
        public List<String> restricted_to {get;set;}
        public String refresh_token {get;set;}
        public String token_type {get;set;}
    } 
    
    public class boxResponseObject {
        string folderId{get;set;}
        string folderShareLink{get;set;}
        
        
        public boxResponseObject(string boxfolderId, string sLink )
        {
            folderId =boxfolderId;
            folderShareLink=sLink; 
            
        }
    }
}