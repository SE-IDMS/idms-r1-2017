@isTest
private class VFC_AssessmentClone_TEST{
    static testMethod void myUnitTest(){
        List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0){
            User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            usr.BypassWF__c = false;
            usr.ByPassVR__c = true;
            usr.ProgramAdministrator__c = true;
            System.runas(usr){

                Country__c country = Utils_TestMethods.createCountry();
                country.countrycode__c ='ASM';
                insert country;

                //creates a global partner program
                PartnerPRogram__c gp1 = new PartnerPRogram__c();
                gp1 = Utils_TestMethods.createPartnerProgram();
                gp1.TECH_CountriesId__c = country.id;
                gp1.recordtypeid = Label.CLMAY13PRM15;
                gp1.ProgramStatus__c = Label.CLMAY13PRM47;
                insert gp1;

                //creates a program level for the global program
                ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
                insert prg1;
                prg1.levelstatus__c = 'active';
                update prg1;

                //creates a country partner program
                PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
                insert cp1;
                cp1.ProgramStatus__c = 'active';
                update cp1;

                //creates a program level for the country partner program
                ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
                insert prg2;
                prg2.levelstatus__c = 'active';
                update prg2;

                Recordtype prgORFAssessmentRecordType = [Select id from RecordType where developername =  'PRMProgramApplication' limit 1];
                
                Assessment__c asm = new Assessment__c();
                asm = Utils_TestMethods.createAssessment();
                asm.AutomaticAssignment__c = true;
                asm.recordTypeId = prgORFAssessmentRecordType.Id ;
                asm.Active__c = true;
                asm.Name = 'Sample Assessment';
                asm.PartnerProgram__c = gp1.id;
                asm.ProgramLevel__c = prg1.id;
                insert asm;

                System.Debug('The assessment is:' + asm);

                OpportunityAssessmentQuestion__c oppQues = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.id, 'Picklist',4);
                insert oppQues;
                
                System.Debug('The assessment question is:' + oppQues);
                
                OpportunityAssessmentAnswer__c oppAns = Utils_TestMethods.createOpportunityAssessmentAnswer(oppQues.id);
                insert oppAns;

                System.Debug('The assessment answer is:' + oppAns);

                ApexPages.currentPage().getParameters().put('Id',asm.Id);

                Apexpages.Standardcontroller controller = new Apexpages.Standardcontroller(asm);
             //   Pagereference pg = Page.VFP_AssessmentClone ; 
             //   Test.setCurrentPage(pg);
                
                VFC_AssessmentClone vfcon = new VFC_AssessmentClone(controller);
                vfcon.clonedName = 'Sample';
                vfcon.main();
                vfcon.cloneAssessment();
                vfcon.doCancel();
                
           }
        }
    }
}