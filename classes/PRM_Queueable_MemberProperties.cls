public with sharing class PRM_Queueable_MemberProperties implements Queueable {
	public Map<Id,MemberExternalProperties__c> oldMap {get;set;}
	public Map<Id,MemberExternalProperties__c> newMap {get;set;}

	public PRM_Queueable_MemberProperties(Map<Id,MemberExternalProperties__c> oldRecords, Map<Id,MemberExternalProperties__c> newRecords) {
        oldMap = oldRecords;
        newMap = newRecords;
	}

    public void execute(QueueableContext context) {	
    	System.debug('begin execute oldMap ' + oldMap);
        System.debug('begin execute newMap ' + newMap);
    	list<MemberExternalProperties__c> addList = new list<MemberExternalProperties__c>();
    	list<MemberExternalProperties__c> removeList = new list<MemberExternalProperties__c>();
    	Set<Id> idSet = oldMap!=null?oldMap.keySet():newMap.keySet();
    	for (Id idRecord:idSet) {
    		Boolean oldFlag=oldMap!=null?oldMap.get(idRecord).DeletionFlag__c:true;
    		Boolean newFlag= newMap!=null?newMap.get(idRecord).DeletionFlag__c:true;
            System.debug('oldFlag ' + oldFlag);
            System.debug('newFlag ' + newFlag);
    		if (oldFlag != newFlag && oldFlag) {
    			addList.add(newMap.get(idRecord));
    		} else if (oldFlag != newFlag && newFlag) {
    			removeList.add(oldMap.get(idRecord));
    		}
    	}
        System.debug('addList ' + addList);
        System.debug('removeList ' + removeList);
        FieloPRM_AP_MemberPropertiesTriggers.createBadgeMember(addList);
        FieloPRM_AP_MemberPropertiesTriggers.deleteBadgeMember(removeList);
    }  
}