/*
    Author          : Bhuvana Subramaniyan (ACN) 
    Date Created    : 12/01/2010 
    Description     : Test Class for VFC16_NotesAndAttachments.
*/
@isTest
private class VFC16_NotesAndAttachments_TEST
{
    //Test method
    static testMethod void testNotesAndAttachments() 
    {
        //inserts Account
        Account acc = RIT_Utils_TestMethods.createAccount();
        insert acc;
        acc = [select id from Account where Id=:acc.Id];
        
        //inserts Opportunity
        Opportunity opp = RIT_Utils_TestMethods.createOpportunity(acc.Id);
        insert opp;
        opp = [select id from Opportunity where Id =: opp.Id];
        
        //inserts QuoteLink
        OPP_QuoteLink__c quoteLink = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
        insert quoteLink;        
        quoteLink = [select id from OPP_QuoteLink__c where Id=:quoteLink.Id];
        
         //inserts Financial Environment record for quoteLink 
        RIT_FinancialEnv__c finEnv = RIT_Utils_TestMethods.createFinEnv(quoteLink.Id);
        insert finEnv;
        finEnv = [select id from RIT_FinancialEnv__c where id=:finEnv.Id];
        
        //inserts Contractual Environment
        RIT_ContractualEnv__c contEnv = RIT_Utils_TestMethods.createConEnv(quoteLink.Id);
        insert contEnv;
        contEnv =  [select id from RIT_ContractualEnv__c where id=:contEnv.Id];
        
        //inserts Project Information
        RIT_ProjectInformation__c projInfo= RIT_Utils_TestMethods.createProjInfo(quoteLink.Id);
        insert projInfo;
        projInfo=  [select id from RIT_ProjectInformation__c where id=:projInfo.Id];
        
        //inserts Project Information
        RIT_ProjectManagement__c projMgnt = RIT_Utils_TestMethods.createProjMgnt(quoteLink.Id);
        insert projMgnt;
        projMgnt =  [select id from RIT_ProjectManagement__c where id=:projMgnt.Id];
        
        //inserts Budget
        RIT_Budget__c budget= RIT_Utils_TestMethods.createbudget(quoteLink.Id);
        insert budget;
        budget=  [select id from RIT_Budget__c where id=:budget.Id];
        
        //inserts Techical & Manufacturing
        RIT_TechManScope__c techManf= RIT_Utils_TestMethods.createTechManf(quoteLink.Id);
        insert techManf;
        techManf=  [select id from RIT_TechManScope__c where id=:techManf.Id];
        
        //inserts Commercial & Competitive Environment
        RIT_CommCompEnv__c commComp= RIT_Utils_TestMethods.createCommComp(quoteLink.Id);
        insert commComp;
        commComp=  [select id from RIT_CommCompEnv__c where id=:commComp.Id];
        
        RIT_CommCompEnv__c commComp1= RIT_Utils_TestMethods.createCommComp(quoteLink.Id);
        insert commComp1;
        commComp1=  [select id from RIT_CommCompEnv__c where id=:commComp1.Id];

        //insert Note
        Note note= RIT_Utils_TestMethods.createNotes(finEnv.Id);
        insert note;
        note=  [select id from Note  where id=:note.Id];
        
        //Test method Execution Starts
        Test.startTest();
        
        VFC16_NotesAndAttachments notesAndAtttachments = new VFC16_NotesAndAttachments();
        
        notesAndAtttachments.objectName = 'RIT_FinancialEnv__c';
        notesAndAtttachments.objectId = finEnv.Id;
        notesAndAtttachments.getNotesAndAttachments();
        
        notesAndAtttachments.objectName = 'RIT_ContractualEnv__c';
        notesAndAtttachments.objectId = contEnv.Id;
        notesAndAtttachments.getNotesAndAttachments();
        
        notesAndAtttachments.objectName = 'RIT_Budget__c';
        notesAndAtttachments.objectId = budget.Id;
        notesAndAtttachments.getNotesAndAttachments();
        
        notesAndAtttachments.objectName = 'RIT_CommCompEnv__c';
        notesAndAtttachments.objectId = commComp.Id;
        notesAndAtttachments.getNotesAndAttachments();
        
        notesAndAtttachments.objectName = 'RIT_TechManScope__c';
        notesAndAtttachments.objectId = techManf.Id;
        notesAndAtttachments.getNotesAndAttachments();
        
        notesAndAtttachments.objectName = 'RIT_ProjectManagement__c';
        notesAndAtttachments.objectId = projMgnt.Id;
        notesAndAtttachments.getNotesAndAttachments();
        
        notesAndAtttachments.objectName = 'RIT_ProjectInformation__c';
        notesAndAtttachments.objectId = projInfo.Id;
        notesAndAtttachments.getNotesAndAttachments();
        
        notesAndAtttachments = new VFC16_NotesAndAttachments();
        notesAndAtttachments.objectName = 'RIT_CommCompEnv__c';
        notesAndAtttachments.objectId = commComp1.Id;
        notesAndAtttachments.getNotesAndAttachments();
                
        Test.StopTest();
        //Test method Execution Ends
    }
}