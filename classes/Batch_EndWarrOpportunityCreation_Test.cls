@isTest
public class Batch_EndWarrOpportunityCreation_Test{
    
    public static testMethod void TestOne() {
    
        Country__c country= Utils_TestMethods.createCountry();
       country.CountryCode__c = 'SG';   
        insert country; 
    
        //Creating Data
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.RecordTypeid = Label.CLOCT13ACC08;
        objAccount.Country__c =country.id;
        insert objAccount;
        
        System.debug('objAccount:'+objAccount.Country__c);
        
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = objAccount .id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContactVIS');
        objContact.Country__c= country.id;
        insert objContact;
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        objCase.SVMXC__Site__c = site1.id;
        insert objCase;
        
        Brand__c brand1_1 = new Brand__c();

        brand1_1.Name ='Brand 1-1';
        brand1_1.SDHBRANDID__c = 'Test_BrandSDHID';
        brand1_1.IsSchneiderBrand__c = true;
        insert brand1_1 ;
        
        DeviceType__c dt1_1 = new DeviceType__c();
        dt1_1.name = 'Device Type 1-1';
        dt1_1.SDHDEVICETYPEID__c = 'Test_DeviceTypeSDHID1-1';
        insert dt1_1 ;
        
        OPP_Product__c opProduct= New OPP_Product__c();
        opProduct.Name='opp_product';
        opProduct.BusinessUnit__c='INFOR TECHNO. BUSINESS';
        opProduct.ProductFamily__c='ITB21';
        opProduct.ProductLine__c='PWACB';
        insert opProduct;
        
        system.debug('opProduct:'+opProduct.BusinessUnit__c);
        
        Product2 prod = New Product2();
        prod.Name = 'Product_test';
        prod.RenewableService__c = true;
        prod.Brand2__c=brand1_1.Id;
        prod.DeviceType2__c=dt1_1.Id;
        prod.ProductGDP__c=opProduct.Id;
        insert prod;
        
        System.debug('prod:'+prod.ProductGDP__c);
        
        
        Set<String> buset = new Set<String>();
        CS_BUCountry__c BuCountry = new CS_BUCountry__c();
        BuCountry.BatchType__c ='EOW';
        BuCountry.CountryCode__c ='EG';
        BuCountry.Name ='tesx';
        BuCountry.CountryCode__c ='INFOR TECHNO. BUSINESS';
        insert BuCountry;
        
        //[SELECT BusinessUnit__c,CountryCode__c,Id FROM CS_BUCountry__c WHERE BatchType__c = 'EOW'];
        //System.debug('listBuCountry:'+listBuCountry );
       /* for(CS_BUCountry__c obj:listBuCountry ){
            System.debug('buset' );
            if(obj.BatchType__c =='EOW'){
                buset.add(obj.BusinessUnit__c+obj.CountryCode__c);
                
            }
        }*/
        
        List<SVMXC__Installed_Product__c> ipobjlist = new List<SVMXC__Installed_Product__c>();
        
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = objAccount.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        ip1.RecordTypeId = Label.CLAPR14SRV09;
        ip1.SVMXC__Product__c = prod.Id;
        ip1.SVMXC__Serial_Lot_Number__c = 'TestSerialLotNumber';
        ip1.EndOfWarrantyOpportunityGenerated__c= false;
        ip1.SVMXC__Warranty_End_Date__c = date.today() + 125;
        ip1.ObsolescenceOpportunityGenerated__c= false;
        //ip1.ProductGDP__c ='';
        //date myDate = date.newInstance(2015, 12, 17);
        ip1.CommissioningDateInstallDate__c=date.today();
        ip1.Tech_IpAssociatedToSC__c= false;
        ip1.AssetCategory2__c='Category 1';
        ip1.UnderContract__c=false;
        ip1.SKUToCreate__c='jhgfus';
        //ip1.EndOfWarrantyOpportunityTriggered__c = 100;
       // ip1.ProductBuCountry__c ='INFOR TECHNO. BUSINESSEG';
        ipobjlist.add(ip1);
        insert ipobjlist;
        //test.startTest();
        System.debug('value :'+ip1.ProductBuCountry__c );
        System.debug('value2 :'+ip1.SVMXC__Warranty_End_Date__c );
        System.debug('valu3 :'+ip1.SVMXC__Product__r.ProductGDP__c );
        
        
        Batch_EndWarrOpportunityCreation ipbatch  = new Batch_EndWarrOpportunityCreation();
        ID batchprocessid = Database.executeBatch(ipbatch);
        
        Batch_ObsolenceOpportunityCreation ipb = New Batch_ObsolenceOpportunityCreation();
        ID batchid = Database.executeBatch(ipb);
       // test.stopTest();
       
        string CORN_EXP = '0 0 0 1 4 ?';
        string CORN_EXPS = '0 0 0 1 4 ?';
        
        Batch_EndWarrOpportunityCreation ipbatch2 = new Batch_EndWarrOpportunityCreation ();
        string jobid = system.schedule('my batch job', CORN_EXP, new Batch_EndWarrOpportunityCreation());
        CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
        
        Batch_ObsolenceOpportunityCreation ipbatch3 = new Batch_ObsolenceOpportunityCreation();
        string jobid2 = system.schedule('my batch job2', CORN_EXPS , new Batch_ObsolenceOpportunityCreation());
        CronTrigger ct2 = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId2];
        
    }
     
}