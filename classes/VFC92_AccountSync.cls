public with sharing class VFC92_AccountSync {
private final Account acct;

public boolean hasAccess{get;set;}{hasAccess=true;}
public boolean SEaccountidisnotnull{get;set;}{SEaccountidisnotnull=true;}
public boolean accountstatusCM{get;set;}{accountstatusCM=true;}


    public VFC92_AccountSync(ApexPages.StandardController controller) {
    if(!test.isRunningTest())
    controller.addFields(new List<String>{'SEAccountID__c','AccountStatus__c','OwnerId','AccountTeamMembers.UserId'});
    acct=(Account)controller.getRecord();        
    }       
   
    public PageReference redirectToMW()
    {
        List<UserRecordAccess> useraccess=[select RecordId,HasEditAccess from UserRecordAccess where RecordId=:acct.id and UserId=:UserInfo.getUserId()];
        if(useraccess.size()==0 || (useraccess.size()>0 && !useraccess[0].HasEditAccess))
        hasAccess=false;
        
        if(!useraccess[0].HasEditAccess){
            hasAccess=false;
        }
        else if(acct.AccountStatus__c!=System.Label.CL00819){
            accountstatusCM=false;
        }    
        else if(acct.SEAccountID__c==null ){
            SEaccountidisnotnull=false;
        }
        if(hasAccess && SEaccountidisnotnull && accountstatusCM)
        {        
            //String parameters=System.Label.CL00820+'='+UserInfo.getSessionId()+'&'+System.Label.CL00821+'&'+System.Label.CL00822+'='+acct.SEAccountID__c+'&'+System.Label.CL00823+'='+[select email from user where id=:UserInfo.getUserId()].email;
            //PageReference pg=new PageReference(System.Label.CL00818+'?'+parameters);
            //Added for Cordys Oath Setup in April 14
            String parameters=System.Label.CL00821+'&'+System.Label.CL00822+'='+acct.SEAccountID__c+'&'+System.Label.CL00823+'='+[select email from user where id=:UserInfo.getUserId()].email;
            PageReference pg=new PageReference(System.Label.CL00818+parameters);
            pg.setRedirect(true);
            return pg;
        }
        else
        return null;   
    }
}