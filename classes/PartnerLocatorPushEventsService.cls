public class PartnerLocatorPushEventsService{
    
    //ShowPL Picklist Values
    private static String PICKLIST_AGREE = 'Agree';
    private static String PICKLIST_REFUSE = 'Refuse';
    private static String PICKLIST_ANR = 'ANR';
    //Event picklist values
    public static String PICKLIST_ADD_OR_UPDATE = 'AU';
    public static String PICKLIST_DELETE_IF_EXIST = 'DE';
    public static String PICKLIST_UNKNOWN = 'N';
    //Type picklist values
    private static String PICKLIST_PL = 'PartnerLocator';
    public static String PICKLIST_BFO_PROP = 'BFOProperties';
    //Record Type name
    private static String RECORD_TYPE_BUSINESS_ACCOUNT='Business Account';
    
    private static List<CS_PRMPartnerLocatorSimpleEventFields__c> plFieldCSList;
    private static List<String> plFieldList;
    private static List<String> PLFields {get 
        {
            if(plFieldCSList ==null){
                plFieldCSList = CS_PRMPartnerLocatorSimpleEventFields__c.getall().values();
                plFieldList = new List<String>();
                for(CS_PRMPartnerLocatorSimpleEventFields__c plFieldCS:plFieldCSList){
                    plFieldList.add(plFieldCS.FieldAPIName__c);
                }
            }
            return plFieldList;
        }
    }
    private static Map<Id,String> recordTypeNamesByIdSingleton;
    private static Boolean recordTypeSoqlExecuted=false;

    public static String INTEGRATION_EVENT_FEATURE='IntegrationEvent_FEATURE';
    public static String BFOPROPERTIES_FEATURE='BFOPropertiesOProperties_FEATURE';
    public static boolean isDescribeDone = false;
    public static Set<String> validShowInPL;


    public static Map<String,Boolean> checkContextAccountEligibilityToIntegrationEvents(List<Account> newAccountList,List<Account> oldAccountList){
        Map<String,Boolean> elegibilityByFeatureName = new Map<String,Boolean>();
        elegibilityByFeatureName.put(INTEGRATION_EVENT_FEATURE,false);
        elegibilityByFeatureName.put(BFOPROPERTIES_FEATURE,false);

        if(oldAccountList!=null && newAccountList!=null){
            for(Integer i=0;i<oldAccountList.size();i++){
                if(oldAccountList[i].PLDatapool__c!=null || newAccountList[i].PLDatapool__c!=null){
                    elegibilityByFeatureName.put(INTEGRATION_EVENT_FEATURE,true);
                    Set<String> currentValues = new Set<String>{PICKLIST_AGREE,PICKLIST_REFUSE,PICKLIST_ANR};
                    if(!isDescribeDone){
                        fillValidPicklistValues();
                    }
                    for(String value:validShowInPL){
                        if(!currentValues.contains(value)){
                            throw new PLEventsException('the value of the picklist PLShowInPartnerLocator__c : '+value + ' is not supported in IntegrationSimpleEvent Creation for PL ('+ currentValues +'). try to update code and logic for the new value');
                        }
                    }
                }
                if(oldAccountList[i].PRMUIMSID__c!=null || newAccountList[i].PRMUIMSID__c!=null){
                    elegibilityByFeatureName.put(BFOPROPERTIES_FEATURE,true);
                }

            }   
        }else if(newAccountList!=null){
                //Trigger new / old / delete context
                for(Integer i=0;i<newAccountList.size();i++){
                    if(newAccountList[i].PLDatapool__c!=null){
                        elegibilityByFeatureName.put(INTEGRATION_EVENT_FEATURE,true);
                        Set<String> currentValues = new Set<String>{PICKLIST_AGREE,PICKLIST_REFUSE,PICKLIST_ANR};
                        if(!isDescribeDone){
                            fillValidPicklistValues();
                        }
                        for(String value:validShowInPL){
                            if(!currentValues.contains(value)){
                                throw new PLEventsException('the value of the picklist PLShowInPartnerLocator__c : '+value + ' is not supported in IntegrationSimpleEvent Creation for PL ('+ currentValues +'). try to update code and logic for the new value');
                            }
                        }
                    }
                    if(newAccountList[i].PRMUIMSID__c!=null){
                        elegibilityByFeatureName.put(BFOPROPERTIES_FEATURE,true);
                    }
                }   
        }
        return elegibilityByFeatureName;
    }
    
    public static void checkInsertedPLValuesAndPushEvents(Account newAccount,Map<Id,String> recordTypeNamesById,List<IntegrationSimpleEvent__c> integListToInsert){
        Datetime processStart = Datetime.now();
        
        if(!checkNewAndGenerateEvent(newAccount.PLShowInPartnerLocator__c,PICKLIST_AGREE,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'INSERT PLShowInPartnerLocator__c Agree',PICKLIST_PL,integListToInsert,processStart)){
                checkNewAndGenerateEvent(newAccount.PLShowInPartnerLocator__c,PICKLIST_REFUSE,PICKLIST_DELETE_IF_EXIST,newAccount.Id,'INSERT PLShowInPartnerLocator__c Refuse',PICKLIST_PL,integListToInsert,processStart);
        }
            
        checkNewAndGenerateEvent(newAccount.PLShowInPartnerLocatorForced__c,true,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'INSERT PLShowInPartnerLocatorForced__c true',PICKLIST_PL,integListToInsert,processStart); 
        checkNewAndGenerateEvent(newAccount.PLPartnerLocatorFeatureGranted__c,true,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'INSERT PLPartnerLocatorFeatureGranted__c true',PICKLIST_PL,integListToInsert,processStart);
        checkNewAndGenerateEvent(recordTypeNamesById.get(newAccount.RecordTypeId),RECORD_TYPE_BUSINESS_ACCOUNT,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'INSERT RecordType BA true',PICKLIST_PL,integListToInsert,processStart); 
            
        Integer j=0;
        //PLFields events
        while(j<PLFields.size() && !checkChangeAndGenerateEvent(newAccount.get(PLFields.get(j)),null,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'INSERT '+PLFields.get(j),PICKLIST_PL,integListToInsert,processStart)){
            j++;
        }
    }
    
    public static void checkUpdatedPLValuesChangesAndPushEvents(Account oldAccount,Account newAccount,Map<Id,String> recordTypeNamesById,List<IntegrationSimpleEvent__c> integListToInsert){
        Datetime processStart = Datetime.now();
        
        if(!checkUpdateAndGenerateEvent(oldAccount.PLShowInPartnerLocator__c,null,newAccount.PLShowInPartnerLocator__c,PICKLIST_AGREE,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'Update PLShowInPartnerLocator__c null TO Agree',PICKLIST_PL,integListToInsert,processStart)){
            if(!checkUpdateAndGenerateEvent(oldAccount.PLShowInPartnerLocator__c,null,newAccount.PLShowInPartnerLocator__c,PICKLIST_REFUSE,PICKLIST_DELETE_IF_EXIST,newAccount.Id,'Update PLShowInPartnerLocator__c null TO Refuse',PICKLIST_PL,integListToInsert,processStart)){
                if(!checkUpdateAndGenerateEvent(oldAccount.PLShowInPartnerLocator__c,PICKLIST_AGREE,newAccount.PLShowInPartnerLocator__c,null,PICKLIST_DELETE_IF_EXIST,newAccount.Id,'Update PLShowInPartnerLocator__c Agree TO null',PICKLIST_PL,integListToInsert,processStart)){
                    if(!checkUpdateAndGenerateEvent(oldAccount.PLShowInPartnerLocator__c,PICKLIST_REFUSE,newAccount.PLShowInPartnerLocator__c,null,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'Update PLShowInPartnerLocator__c Refuse TO null',PICKLIST_PL,integListToInsert,processStart)){
                        if(!checkUpdateAndGenerateEvent(oldAccount.PLShowInPartnerLocator__c,PICKLIST_REFUSE,newAccount.PLShowInPartnerLocator__c,PICKLIST_AGREE,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'Update PLShowInPartnerLocator__c Refuse TO Agree',PICKLIST_PL,integListToInsert,processStart)){
                            if(!checkUpdateAndGenerateEvent(oldAccount.PLShowInPartnerLocator__c,PICKLIST_ANR,newAccount.PLShowInPartnerLocator__c,PICKLIST_REFUSE,PICKLIST_DELETE_IF_EXIST,newAccount.Id,'Update PLShowInPartnerLocator__c ANR TO Refuse',PICKLIST_PL,integListToInsert,processStart)){
                                checkUpdateAndGenerateEvent(oldAccount.PLShowInPartnerLocator__c,PICKLIST_ANR,newAccount.PLShowInPartnerLocator__c,PICKLIST_AGREE,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'Update PLShowInPartnerLocator__c ANR TO Agree',PICKLIST_PL,integListToInsert,processStart);
                            }
                            else{
                                //Nothing
                            }
                        }else{
                            //Nothing
                        }
                    }else{
                        //Nothing
                    }
                }else{
                    //Nothing
                }
            }else{
                //Nothing
            }
        }else{
            //Nothing
        }
    
    
        
        if(!checkUpdateAndGenerateEvent(oldAccount.PLShowInPartnerLocatorForced__c,false,newAccount.PLShowInPartnerLocatorForced__c,true,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'Update PLShowInPartnerLocatorForced__c false TO true',PICKLIST_PL,integListToInsert,processStart)){
            checkUpdateAndGenerateEvent(oldAccount.PLShowInPartnerLocatorForced__c,true,newAccount.PLShowInPartnerLocatorForced__c,false,PICKLIST_DELETE_IF_EXIST,newAccount.Id,'Update PLShowInPartnerLocatorForced__c true TO false',PICKLIST_PL,integListToInsert,processStart);
        }else{
            //Nothing
        }
         
        if(!checkUpdateAndGenerateEvent(oldAccount.PLPartnerLocatorFeatureGranted__c,false,newAccount.PLPartnerLocatorFeatureGranted__c,true,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'Update PLPartnerLocatorFeatureGranted__c false TO true',PICKLIST_PL,integListToInsert,processStart)){
            checkUpdateAndGenerateEvent(oldAccount.PLPartnerLocatorFeatureGranted__c,true,newAccount.PLPartnerLocatorFeatureGranted__c,false,PICKLIST_DELETE_IF_EXIST,newAccount.Id,'Update PLPartnerLocatorFeatureGranted__c true TO false',PICKLIST_PL,integListToInsert,processStart);
        }else{
            //Nothing
        }
        
        if(recordTypeNamesById.get(newAccount.RecordTypeId)!=RECORD_TYPE_BUSINESS_ACCOUNT){
            checkChangeAndGenerateEvent(oldAccount.RecordTypeId,newAccount.RecordTypeId,PICKLIST_DELETE_IF_EXIST,newAccount.Id,'Changed RecordType FROM '+RECORD_TYPE_BUSINESS_ACCOUNT,PICKLIST_PL,integListToInsert,processStart);
        }else{
             checkChangeAndGenerateEvent(oldAccount.RecordTypeId,newAccount.RecordTypeId,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'Changed RecordType TO '+RECORD_TYPE_BUSINESS_ACCOUNT,PICKLIST_PL,integListToInsert,processStart);
        }
        
        //PLFields
        Boolean oldAllNULL = checkPLFieldsAreNull(oldAccount);
        Boolean newAllNULL = checkPLFieldsAreNull(newAccount);
        
        if(!checkChangeFromSpecificValueAndGenerateEvent(true,oldAllNULL,newAllNULL,PICKLIST_ADD_OR_UPDATE,newAccount.Id,'PLFields from NULL to Not NULL',PICKLIST_PL,integListToInsert,processStart)){
            if(!checkChangeFromSpecificValueAndGenerateEvent(true,newAllNULL,oldAllNULL,PICKLIST_DELETE_IF_EXIST,newAccount.Id,'PLFields from Not NULL to NULL',PICKLIST_PL,integListToInsert,processStart)){
                //loop
                Integer j=0;
                while(j<PLFields.size() && !checkChangeAndGenerateEvent(oldAccount.get(PLFields.get(j)),newAccount.get(PLFields.get(j)),PICKLIST_ADD_OR_UPDATE,newAccount.Id,'Changed '+PLFields.get(j)+' Field',PICKLIST_PL,integListToInsert,processStart)){
                        j++;
                }
            }else{
                //Nothing
            }
        }else{
            //Nothing
        }
            
    }
    
    public static void checkDeletedPLValuesChangesAndPushEvents(Account oldAccount,List<IntegrationSimpleEvent__c> integListToInsert){
        Datetime processStart = Datetime.now();
        generateEvent(PICKLIST_DELETE_IF_EXIST,null,oldAccount.Id,'Delete record',PICKLIST_PL,integListToInsert,processStart);
    }
    
    public static void checkPLValuesUnDeleteChangesAndPushEvents(Account newAccount,List<IntegrationSimpleEvent__c> integListToInsert){
        Datetime processStart = Datetime.now();
        generateEvent(PICKLIST_ADD_OR_UPDATE,newAccount.Id,null,'Undelete record',PICKLIST_PL,integListToInsert,processStart);
    }
    
    
    public static Boolean checkUpdateAndGenerateEvent(Object oldValue,Object compareOldValue,Object newValue,Object compareNewValue,String ActionCode,String bfoId,String eventName,String type,List<IntegrationSimpleEvent__c> integListToInsert,Datetime processStart){
        if(oldValue==compareOldValue &&  newValue==compareNewValue){
                IntegrationSimpleEvent__c integToInsert = new IntegrationSimpleEvent__c(bfoId__c=bfoId,RelatedBFOId__c=bfoId,ActionCode__c=ActionCode,name=eventName,Type__c=type,CalculationTime__c=Datetime.now().getTime()-processStart.getTime());
                integListToInsert.add(integToInsert);
                return true;
        }
        return false;
    }
    
    public static Boolean checkChangeFromSpecificValueAndGenerateEvent(Object oldValueFrom,Object oldValue,Object newValue,String ActionCode,String bfoId,String eventName,String type,List<IntegrationSimpleEvent__c> integListToInsert,Datetime processStart){
        if(oldValueFrom==oldValue && oldValue != newValue){
                IntegrationSimpleEvent__c integToInsert = new IntegrationSimpleEvent__c(bfoId__c=bfoId,RelatedBFOId__c=bfoId,ActionCode__c=ActionCode,name=eventName,Type__c=type,CalculationTime__c=Datetime.now().getTime()-processStart.getTime());
                integListToInsert.add(integToInsert);
                return true;
        }
        return false;
    }
    
    public static Boolean checkChangeAndGenerateEvent(Object oldValue,Object newValue,String ActionCode,String bfoId,String eventName,String type,List<IntegrationSimpleEvent__c> integListToInsert,Datetime processStart){
        if(oldValue != newValue){
                IntegrationSimpleEvent__c integToInsert = new IntegrationSimpleEvent__c(bfoId__c=bfoId,RelatedBFOId__c=bfoId,ActionCode__c=ActionCode,name=eventName,Type__c=type,CalculationTime__c=Datetime.now().getTime()-processStart.getTime());
                integListToInsert.add(integToInsert);
                return true;
        }
        return false;
    }
    
    
    public static Boolean checkNewAndGenerateEvent(Object value,Object compareValue,String ActionCode,String bfoId,String eventName,String type,List<IntegrationSimpleEvent__c> integListToInsert,Datetime processStart){
        if(value==compareValue){
                IntegrationSimpleEvent__c integToInsert = new IntegrationSimpleEvent__c(bfoId__c=bfoId,RelatedBFOId__c=bfoId,ActionCode__c=ActionCode,name=eventName,Type__c=type,CalculationTime__c=Datetime.now().getTime()-processStart.getTime());
                integListToInsert.add(integToInsert);
                return true;
        }
        return false;
    }
    
    public static void generateEvent(String ActionCode,String bfoId,String relatedBFO,String eventName,String type,List<IntegrationSimpleEvent__c> integListToInsert,Datetime processStart){
        IntegrationSimpleEvent__c integToInsert = new IntegrationSimpleEvent__c(bfoId__c=bfoId,RelatedBFOId__c=relatedBFO,ActionCode__c=ActionCode,name=eventName,Type__c=type,CalculationTime__c=Datetime.now().getTime()-processStart.getTime());
        integListToInsert.add(integToInsert);
    }

    public static void generateEventWithProperty(String ActionCode,String bfoId,String relatedBFO,String eventName,String type,String property,List<IntegrationSimpleEvent__c> integListToInsert,Datetime processStart){
        IntegrationSimpleEvent__c integToInsert = new IntegrationSimpleEvent__c(bfoId__c=bfoId,RelatedBFOId__c=relatedBFO,ActionCode__c=ActionCode,name=eventName,Type__c=type,CalculationTime__c=Datetime.now().getTime()-processStart.getTime(),PropertyName__c=property);
        integListToInsert.add(integToInsert);
    }
    
    public static Map<Id,String> getAccountRecordTypeNameByIdMap(){
        if(recordTypeNamesByIdSingleton==null){
            recordTypeNamesByIdSingleton = new Map<Id,String>();
        }
        if(recordTypeSoqlExecuted==true){
            return recordTypeNamesByIdSingleton;
        }else{
            recordTypeSoqlExecuted =false;
            for(RecordType record : [select id,name from RecordType where SobjectType='Account']){
                recordTypeNamesByIdSingleton.put(record.id,record.name);
            }
            return recordTypeNamesByIdSingleton;
        }
    }
    
    public static boolean checkPLFieldsAreNull(Account acc){
        Boolean valueIsNull = true;
        Integer i=0;
        while(valueIsNull && i<PLFields.size()){
            if(acc.get(PLFields[i])!=null){
                valueIsNull =false;
            }
            i++;
        }
        return valueIsNull;
    }

    /**detect new values*/
    private static void fillValidPicklistValues(){
        
        Schema.DescribeFieldResult typeDescribe = Account.PLShowInPartnerLocator__c.getDescribe();
        validShowInPL = new Set<String>();
        for(Schema.PicklistEntry typeVal : typeDescribe.getPicklistValues())
        {
            validShowInPL.add(typeVal.getValue());
        }
        isDescribeDone=true;
    }

    public static void checkPartnerLocatorPushEventsFromTriggerAccount(){
        Map<String,Boolean> eligibileByfeature = null;
        Map<Id,String> recordTypeNamesById = null;
        List<IntegrationSimpleEvent__c> integListToInsert = null;
        eligibileByfeature = PartnerLocatorPushEventsService.checkContextAccountEligibilityToIntegrationEvents((List<Account>)Trigger.new,(List<Account>)Trigger.old);
        if(eligibileByfeature.get(PartnerLocatorPushEventsService.INTEGRATION_EVENT_FEATURE)){
            integListToInsert = new List<IntegrationSimpleEvent__c>();
            recordTypeNamesById = PartnerLocatorPushEventsService.getAccountRecordTypeNameByIdMap();
            if(Trigger.isUpdate){
                for(Integer i=0;i<trigger.size;i++){
                    //Generate Integration Simple Event if necessary
                    if(((Account)trigger.old[i]).PLDatapool__c!=null || ((Account)trigger.new[i]).PLDatapool__c!=null){
                        PartnerLocatorPushEventsService.checkUpdatedPLValuesChangesAndPushEvents((Account)trigger.old[i],(Account)trigger.new[i],recordTypeNamesById,integListToInsert);
                    }
                }
            }else{
                for(Account acc:(List<Account>)Trigger.New){
                    if(acc.PLDatapool__c!=null)
                        PartnerLocatorPushEventsService.checkInsertedPLValuesAndPushEvents(acc,recordTypeNamesById,integListToInsert);
                }
            }
            //Insert potentially Events in group (1 DML)
            if(integListToInsert.size()>0)
                insert integListToInsert;
        }   
    }
    
    public class PLEventsException extends Exception{

    }

}