/**
     * This class contains unit tests for validating the behavior of Apex classes
     * and triggers.
     *
     * Unit tests are class methods that verify whether a particular piece
     * of code is working properly. Unit test methods take no arguments,
     * commit no data to the database, and are flagged with the testMethod
     * keyword in the method definition.
     *
     * All test methods in an organization are executed whenever Apex code is deployed
     * to a production organization to confirm correctness, ensure code
     * coverage, and prevent regressions. All Apex classes are
     * required to have at least 75% code coverage in order to be deployed
     * to a production organization. In addition, all triggers must have some code coverage.
     * 
     * The @isTest class annotation indicates this class only contains test
     * methods. Classes defined with the @isTest annotation do not count against
     * the organization size limit for all Apex scripts.
     *
     * See the Apex Language Reference for more information about Testing and Code Coverage.
     */
    @isTest(SeeAllData=true)
    private class ServiceMaxWorkOrder_TEST {

        static testMethod void testAutoAssignment() {
            Country__c ctry = new Country__c(Name = 'Test', CountryCode__c = 'TS');
            insert ctry;
            
            StateProvince__c state = new StateProvince__c(NAme = 'TestState', Country__c = ctry.Id, CountryCode__c = 'US', StateProvinceCode__c = 'TS');
            insert state;
            
            Account acc = Utils_TestMethods.createAccount();
            insert acc;
                    
            SVMXC__Site__c site = new SVMXC__Site__c(Name = 'TestBatchName',
                                                 SVMXC__City__c = 'Junction City',
                                                 SVMXC__State__c = 'Kansas',
                                                 SVMXC__Street__c = '704 S. Adams',
                                                 SVMXC__Zip__c = '66441',
                                                 LocationCountry__c = ctry.Id,
                                                 SVMXC__Location_Type__c='Building',
                                                 SVMXC__Account__c=acc.Id,
                                                 TimeZone__c='India');
            insert site;
            
            SVMXC__Service_Group__c team = new SVMXC__Service_Group__c(Name = 'test');
            insert team;
            
            User testuser = new User();
            testUser.Email              = 'test@servicemax.com';
            testUser.Username           =  'svmxcuser@servicemax.com';
            testUser.LastName           = 'test';
            testUser.Alias              = 'test';
            testUser.ProfileId          = UserInfo.getProfileId();
            testUser.LanguageLocaleKey  = 'en_US';
            testUser.LocaleSidKey       = 'en_US';
            testUser.TimeZoneSidKey     = 'America/Chicago';
            testUser.EmailEncodingKey   = 'UTF-8';
            testUser.ManagerId = UserInfo.getUserId();
            
            insert testUser;
            
            Account acct = new Account(Name = 'TestAcct', Country__c = ctry.Id, Street__c = 'TestCity', StateProvince__c = state.Id, ZipCode__c = '666441',OwnerId=testUser.Id);
            insert acct;
            
            
            RecordType[] rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c'];  
                SVMXC__Service_Group_Members__c tech    ;   
              for(RecordType rt : rtssg) //Loop to take a record type at a time
                {
                    // Create Module Record
                   if(rt.DeveloperName == 'Technican')
                    {
                      tech = new SVMXC__Service_Group_Members__c(
                                                    RecordTypeId =rt.Id,
                                                    SVMXC__Active__c = true,
                                                    PrimaryAddress__c='Home',
                                                    Name = 'Test user',
                                                    SVMXC__Service_Group__c =team.id,
                                                    SESAID__c ='201771',
                                                    SVMXC__Role__c = 'Schneider Employee',
                                                    SVMXC__Salesforce_User__c = UserInfo.getUserId(),
                                                    Manager__c = testUser.Id
                                                    );
                        insert tech;
                    }
                }
             
                      
                    
         /*  RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c' AND DeveloperName = 'Technician' LIMIT 1];
            
            SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(//RecordTypeId = rt.Id,
            Name = 'test', SVMXC__Service_Group__c = team.Id, SVMXC__Salesforce_User__c = testUser.Id);
            insert tech;*/
            
            //Added By Deepak.
            Product2 prd = New Product2(Name='Circuit Breaker',IsActive=true);
            insert prd;
            
                    
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'Test', SVMXC__Site__c = site.Id, SVMXC__Company__c = acc.Id,RangeToCreate__c='test',SVMXC__Product__c=prd.Id,SKUToCreate__c='Test2',BrandToCreate__c='schneider-electric',DeviceTypeToCreate__c='Circuit Breaker',OwnerId=testUser.Id,TECH_CreatedfromSFM__c =true);
            insert ip;
            
            RecordType rte = [SELECT Id FROM RecordType WHERE SobjectType = 'Role__c' AND DeveloperName = 'InstalledProductAccountRole' LIMIT 1];
            
            Role__c role = new Role__c(                //added by deepak
                Location__c = site.Id,
                Role__c = 'Preferred FSE Buildings',
                RecordTypeId=rte.Id,
                InstalledProduct__c =ip.Id
                //User__c = testUser.Id
            );
            
            insert role;
            //update ip;
            
            WorkOrderAssignmentRule__c rule = new WorkOrderAssignmentRule__c();
            rule.BusinessUnit__c = 'BD';
            rule.Country__c = ctry.Id;
            insert rule;
            
            Test.startTest();
            
            SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(
                SVMXC__Company__c = acct.Id,
                SVMXC__Site__c = site.Id,
                SVMXC__Component__c = ip.Id,
                Service_Business_Unit__c = 'BD',
                OwnerId=testUser.Id             
            );
            
            /*insert wo;
            
            Test.stopTest();
            
            wo = [SELECT Id, SVMXC__Group_Member__c FROM SVMXC__Service_Order__C WHERE Id = :wo.Id LIMIT 1];
            
            system.assertEquals(tech.Id, wo.SVMXC__Group_Member__c);*/
            
        }
        
        static testMethod void testPMLocationServices()
        {
            Account acct = Utils_TestMethods.createAccount();
            insert acct;
            
            //Added By Deepak.
            Product2 prd = New Product2(Name='Circuit Breaker',IsActive=true);
            insert prd;
            
            SVMXC__Site__c site = new SVMXC__Site__c();
            site.SVMXC__Account__c = acct.Id;
            site.Name = 'Test Site';
            insert site;
            
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'Test', SVMXC__Site__c = site.Id, SVMXC__Company__c = acct.Id,RangeToCreate__c='test',SVMXC__Product__c=prd.Id,SKUToCreate__c='Test2',BrandToCreate__c='schneider-electric',DeviceTypeToCreate__c='Circuit Breaker',TECH_CreatedfromSFM__c =true);
            insert ip;
            
           RecordType rte = [SELECT Id FROM RecordType WHERE SobjectType = 'Role__c' AND DeveloperName = 'InstalledProductAccountRole' LIMIT 1];
            
            Role__c role = new Role__c(                //added by deepak
                Location__c = site.Id,
                Role__c = 'Preferred FSE Buildings',
                RecordTypeId=rte.Id,
                InstalledProduct__c =ip.Id
                //User__c = testUser.Id
            );
            insert role;
            //update ip;
            
            SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(Name = 'Test', SVMXC__Company__c = acct.Id);
            insert contract;
            
            SVMXC__Service_Contract_Services__c is = new SVMXC__Service_Contract_Services__c(SVMXC__Service_Contract__c = contract.Id);
            insert is;

            SVMXC__Service_Contract_Products__c covProd = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c = contract.Id, SVMXC__Installed_Product__c = ip.Id, IncludedService__c = is.Id);
            insert covProd;
                    
            SVMXC__PM_Plan__c pm = new SVMXC__PM_Plan__c(Name = 'test', SVMXC__Service_Contract__c = contract.Id, SVMXC__Work_Order_Assign_To_User__c = UserInfo.getUserId());
            insert pm;
            
            Test.startTest();
            
            SVMXC__Service_Order__c wo = Utils_TestMethods.createWorkOrder(acct.Id);
            wo.SVMXC__PM_Plan__c = pm.Id;
            wo.SVMXC__Service_Contract__c = contract.Id;
            wo.SVMXC__Component__c = ip.Id;
            insert wo;
            
            wo = [SELECT Id, SVMXC__Site__c, IncludedService__c FROM SVMXC__Service_Order__c WHERE Id = :wo.Id LIMIT 1];
            
            system.assertEquals(site.Id, wo.SVMXC__Site__c);
            system.assertEquals(is.Id, wo.IncludedService__c);
            
            Test.stopTest();
            
        } 
        
        static testMethod void testPMLocationServicesOnDetail()
        {
            Account acct = Utils_TestMethods.createAccount();
            insert acct;
            
            //Added By Deepak.
            Product2 prd = New Product2(Name='Circuit Breaker',IsActive=true);
            insert prd;
            
            SVMXC__Site__c site = new SVMXC__Site__c();
            site.SVMXC__Account__c = acct.Id;
            site.Name = 'Test Site';
            insert site;
            
            
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'Test', SVMXC__Site__c = site.Id, SVMXC__Company__c = acct.Id,RangeToCreate__c='test',SVMXC__Product__c=prd.Id,SKUToCreate__c='Test2',BrandToCreate__c='schneider-electric',DeviceTypeToCreate__c='Circuit Breaker',TECH_CreatedfromSFM__c =true);
            insert ip;
            
            RecordType rte = [SELECT Id FROM RecordType WHERE SobjectType = 'Role__c' AND DeveloperName = 'InstalledProductAccountRole' LIMIT 1];
            
            Role__c role = new Role__c(                //added by deepak
                Location__c = site.Id,
                Role__c = 'Preferred FSE Buildings',
                RecordTypeId=rte.Id,
                InstalledProduct__c =ip.Id
                //User__c = testUser.Id
            );
            insert role;
            
            //update ip;
            
            SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(Name = 'Test', SVMXC__Company__c = acct.Id);
            insert contract;
            
            SVMXC__Service_Contract_Services__c is = new SVMXC__Service_Contract_Services__c(SVMXC__Service_Contract__c = contract.Id);
            insert is;

            SVMXC__Service_Contract_Products__c covProd = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c = contract.Id, SVMXC__Installed_Product__c = ip.Id, IncludedService__c = is.Id);
            insert covProd;
                    
            SVMXC__PM_Plan__c pm = new SVMXC__PM_Plan__c(Name = 'test', SVMXC__Service_Contract__c = contract.Id, SVMXC__Work_Order_Assign_To_User__c = UserInfo.getUserId());
            insert pm;
            
            Test.startTest();
            
            SVMXC__Service_Order__c wo = Utils_TestMethods.createWorkOrder(acct.Id);
            wo.SVMXC__PM_Plan__c = pm.Id;
            wo.SVMXC__Service_Contract__c = contract.Id;
            insert wo;
            
            RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'SVMXC__Service_Order_Line__c' AND Name = 'Products Serviced' LIMIT 1];
            
            SVMXC__Service_Order_Line__c detail = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c = wo.Id, RecordTypeId = rt.Id, SVMXC__Serial_Number__c = ip.Id);
            insert detail;
            
            detail = [SELECT Id, ProductLocation__c, IncludedService__c FROM SVMXC__Service_Order_Line__c WHERE Id = :detail.Id LIMIT 1];
            
            system.assertEquals(site.Id, detail.ProductLocation__c);
            system.assertEquals(is.Id, detail.IncludedService__c);
            
            Test.stopTest();
            
        } 
    }