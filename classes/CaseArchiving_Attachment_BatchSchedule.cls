global class CaseArchiving_Attachment_BatchSchedule implements Schedulable {
    global void execute(SchedulableContext SC) {
        Integer size = Integer.Valueof(System.Label.CLJUN16CCC06);
        CaseArchiving_Attachment_Batch  testh= new CaseArchiving_Attachment_Batch();
        Database.executeBatch(testh,size);
    }
}