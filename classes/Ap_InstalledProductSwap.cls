global  Class Ap_InstalledProductSwap{
    @future
    public Static  void IpSwapCoveredProduct(Map<Id,Id> OldNewIPMap){
        List<SVMXC__Service_Contract_Products__c> covplist = new List<SVMXC__Service_Contract_Products__c>();
        List<SVMXC__Service_Contract_Products__c> covplistToUpdate = new List<SVMXC__Service_Contract_Products__c>();
        
        system.debug('Clog\n OldNewIPMap_keyset:'+OldNewIPMap);
        
        covplist=[Select Id,Name,SVMXC__Installed_Product__c,SVMXC__Service_Contract__c,SVMXC__Service_Contract__r.SVMXC__End_Date__c     From    SVMXC__Service_Contract_Products__c Where (SVMXC__Service_Contract__r.SVMXC__End_Date__c >= today) and SVMXC__Installed_Product__c in  :OldNewIPMap.Keyset()];
        system.debug('Cog\n covplist:'+covplist);
        
        if(covplist.size()>0){
            for(SVMXC__Service_Contract_Products__c  cov :covplist){
                if(OldNewIPMap.containskey(cov.SVMXC__Installed_Product__c) && cov.SVMXC__Service_Contract__r.SVMXC__End_Date__c >= Date.today()){
                System.debug('\n Deepak'+cov.SVMXC__Service_Contract__r.SVMXC__End_Date__c);
                System.debug('\n Deepak'+Date.today());
                System.debug('\n Deepak'+cov);
                    cov.SVMXC__Installed_Product__c = OldNewIPMap.get(cov.SVMXC__Installed_Product__c);
                    covplistToUpdate.add(cov);
                    system.debug('Clog\n covplist:'+covplistToUpdate);
                    
                }           
            }
        }
        if(covplistToUpdate.size()>0){
            //String Serializedata = JSON.serialize(covplistToUpdate);
            
           // Ap_InstalledProductSwap.IpSwapCoveredProduct2(Serializedata);
            Database.saveresult[] sr = Database.Update(covplistToUpdate,false);
            //update covplistToUpdate;
        }
    }/*
    @future
    public Static  void IpSwapCoveredProduct2(String Serializedata){
        List<SVMXC__Service_Contract_Products__c> covList =(List<SVMXC__Service_Contract_Products__c>)JSON.deserialize(Serializedata,List<SVMXC__Service_Contract_Products__c>.class);
        
        if(covList !=null && covList.size()>0){
            Database.saveresult[] sr = Database.Update(covList,false);
        }
    }*/
    
     @future
    public Static  void IpSwapOppty(Map<Id,Id> OldNewIPMap){
        List<Installed_product_on_Opportunity_Line__c> AIplist = new List<Installed_product_on_Opportunity_Line__c>();
        List<Installed_product_on_Opportunity_Line__c> aiplistToUpdate = new List<Installed_product_on_Opportunity_Line__c>();
        
        system.debug('Clog\n OldNewIPMap_keyset:'+OldNewIPMap.Keyset());
        
        string Delivered =Label.CL00220;
        string closed =label.CL00272;
        
        AIplist =[Select Id,Name,Installed_Product__c,Opportunity_Line__c,Opportunity_Line__r.Opportunity__r.StageName   From    Installed_product_on_Opportunity_Line__c Where (Opportunity_Line__r.Opportunity__r.StageName !=:Delivered AND Opportunity_Line__r.Opportunity__r.StageName !=:closed) and Installed_Product__c in  :OldNewIPMap.Keyset()];
        
        system.debug('Clog\n AIplist'+AIplist);
        
        if(AIplist.size()>0){
            for(Installed_product_on_Opportunity_Line__c  aip :AIplist){
                if(OldNewIPMap.containskey(aip.Installed_Product__c)){
                    aip.Installed_Product__c = OldNewIPMap.get(aip.Installed_Product__c);
                    aiplistToUpdate.add(aip);
                    system.debug('Clog\n covplist:'+aiplistToUpdate);
                }           
            }
        }
        if(aiplistToUpdate.size()>0){
            Database.saveresult[] sr = Database.Update(aiplistToUpdate,false);
        }
    }
     @future
    public Static  void IpSwapWorkOder(Map<Id,Id> OldNewIPMap){
        List<SVMXC__Service_Order__c> wolist = new List<SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order__c> wolistToUpdate = new List<SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order_Line__c> wdlist = new List<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> wdlistToUpdate = new List<SVMXC__Service_Order_Line__c>();
        
        string ProductServiced = Label.CLAPR15SRV60;
        
        system.debug('Clog\n OldNewIPMap.Keyset():'+OldNewIPMap.Keyset());
        if(OldNewIPMap.Keyset() !=null){
             wolist =[Select Id,Name,SVMXC__Component__c ,(Select Id,Name,SVMXC__Serial_Number__c From SVMXC__Service_Order_Line__r Where RecordTypeId=:ProductServiced) From    SVMXC__Service_Order__c Where (SVMXC__Order_Status__c ='New' OR SVMXC__Order_Status__c ='Unscheduled') and SVMXC__Component__c in  :OldNewIPMap.Keyset()];
        }
        system.debug('Clog\n wolist'+wolist);
        
        if(wolist.size()>0){
            for(SVMXC__Service_Order__c  wo :wolist){
                if(OldNewIPMap.containskey(wo.SVMXC__Component__c)){
                    wo.SVMXC__Component__c = OldNewIPMap.get(wo.SVMXC__Component__c);
                    wolistToUpdate.add(wo);
                    system.debug('Clog\n covplist:'+wolistToUpdate);
                }
               /* if(wo.SVMXC__Service_Order_Line__r !=null){
                    wdlist.addAll(wo.SVMXC__Service_Order_Line__r);
                }*/
            }
            /*
            for(SVMXC__Service_Order_Line__c  wd :wdlist){
                if(OldNewIPMap.containskey(wd.SVMXC__Serial_Number__c)){
                    wd.SVMXC__Serial_Number__c = OldNewIPMap.get(wd.SVMXC__Serial_Number__c);
                    wdlistToUpdate.add(wd);
                    system.debug('Clog\n covplist:'+wolistToUpdate);
                }
            }*/
        }
        /*
        else{
            for(SVMXC__Service_Order__c  wo :[Select Id,Name,SVMXC__Component__c From    SVMXC__Service_Order__c Where (SVMXC__Order_Status__c !='New' OR SVMXC__Order_Status__c !='Unscheduled') and SVMXC__Component__c in  :ipid]){
                wo.Tech_DecommissionedIpToUpdate__c = true;
                wolistToUpdate.add(wo);
            }
        }*/
         wdlist =[Select Id,Name,SVMXC__Serial_Number__c From SVMXC__Service_Order_Line__c Where RecordTypeId=:ProductServiced and (SVMXC__Service_Order__r.SVMXC__Order_Status__c='New' or SVMXC__Service_Order__r.SVMXC__Order_Status__c='Unscheduled') and SVMXC__Serial_Number__c in :OldNewIPMap.Keyset()];
        
        for(SVMXC__Service_Order_Line__c  wd :wdlist){
            if(OldNewIPMap.containskey(wd.SVMXC__Serial_Number__c)){
                wd.SVMXC__Serial_Number__c = OldNewIPMap.get(wd.SVMXC__Serial_Number__c);
                wdlistToUpdate.add(wd);
                system.debug('Clog\n covplist:'+wolistToUpdate);
            }
        }
        
        if(wolistToUpdate.size()>0){
            Database.saveresult[] sr = Database.Update(wolistToUpdate,false);
        }
        if(wdlistToUpdate.size()>0){
            Database.saveresult[] sr = Database.Update(wdlistToUpdate,false);
        }
    }
     @future
    //Swap Pm Plan Coverage
    public Static  void IpSwapPmPlanCoverage(Map<Id,Id> OldNewIPMap){
        List<SVMXC__PM_Coverage__c> pmcovlist = new List<SVMXC__PM_Coverage__c>();
        List<SVMXC__PM_Coverage__c> pmcovlistToUpdate = new List<SVMXC__PM_Coverage__c>();
        
        system.debug('Clog\n OldNewIPMap.Keyset():'+OldNewIPMap.Keyset());
        
        pmcovlist=[Select Id,Name,SVMXC__Product_Name__c  From  SVMXC__PM_Coverage__c Where SVMXC__Product_Name__c in  :OldNewIPMap.Keyset()];
        system.debug('Cog\n pmcovlist:'+pmcovlist);
        
        if(pmcovlist.size()>0){
            for(SVMXC__PM_Coverage__c  pm :pmcovlist){
                if(OldNewIPMap.containskey(pm.SVMXC__Product_Name__c)){
                    pm.SVMXC__Product_Name__c = OldNewIPMap.get(pm.SVMXC__Product_Name__c);
                    pmcovlistToUpdate.add(pm);
                    system.debug('Clog\n pmcovlistToUpdate:'+pmcovlistToUpdate);
                }           
            }
        }
        if(pmcovlistToUpdate.size()>0){
            //Database.saveresult[] sr = Database.Update(covplistToUpdate,false);
            update pmcovlistToUpdate;
        }
    }
    
    //Product Warranty
     
    public Static  void IpSwapProductWarranty(Map<Id,Id> OldNewIPMap,list<Swap_Asset__c>  swapassetList){
        List<SVMXC__Warranty__c> WarList = new List<SVMXC__Warranty__c>();
        List<SVMXC__Warranty__c> WarListGainingIP = new List<SVMXC__Warranty__c>(); //Added by VISHNU C for BR 8815
        List<SVMXC__Warranty__c> warlistToUpdate = new List<SVMXC__Warranty__c>();
        List<SVMXC__Installed_Product__c> GainingIP = new List<SVMXC__Installed_Product__c>(); //Added by VISHNU C for BR 8815
        List<SVMXC__Installed_Product__c> GainingIPUpdate = new List<SVMXC__Installed_Product__c>(); //Added by VISHNU C for BR 8815
        
        
        system.debug('Clog\n OldNewIPMap.Keyset():'+OldNewIPMap.Keyset());
        
        
        WarList =[Select Id,Name,SVMXC__Installed_Product__c,SVMXC__Start_Date__c,SVMXC__End_Date__c   From    SVMXC__Warranty__c Where  SVMXC__Installed_Product__c in  :OldNewIPMap.Keyset()];
        //Added below line by VISHNU C BR 8815
        GainingIP = [Select Id,SVMXC__Warranty_Start_Date__c,SVMXC__Warranty_End_Date__c   From SVMXC__Installed_Product__c Where  Id in  :OldNewIPMap.Values()];
        if(GainingIP.size()>0)
        {
            for (SVMXC__Installed_Product__c ip1: GainingIP)
            {
                ip1.SVMXC__Warranty_Start_Date__c = null;
                ip1.SVMXC__Warranty_End_Date__c = null;
                GainingIPUpdate.add(ip1);
            }
            Database.saveresult[] sr = Database.Update(GainingIPUpdate,false);
        }
        
        WarListGainingIP =[Select Id,Name,SVMXC__Installed_Product__c,SVMXC__Start_Date__c,SVMXC__End_Date__c   From    SVMXC__Warranty__c Where  SVMXC__Installed_Product__c in  :OldNewIPMap.Values()]; 
        if(WarListGainingIP.size()>0){
            system.debug('Warranty List to be Deleted'+WarListGainingIP);
            Delete WarListGainingIP;
        }
        //Changes by VISHNU C Ends
        system.debug('Clog\n WarList'+WarList);
        
        //Old installed product fields
        List<SVMXC__Installed_Product__c> oldipList =[Select Id,CommissioningDateInstallDate__c,SVMXC__Warranty_End_Date__c From SVMXC__Installed_Product__c where Id in :OldNewIPMap.keyset()];
        Map<Id,SVMXC__Installed_Product__c> oldipMap = new map<Id,SVMXC__Installed_Product__c>();
        if(oldipList.size()>0){
            oldipMap.putAll(oldipList);
            system.debug('Clog \n oldipMap'+oldipMap);
        }
        //New installed product fields
        List<SVMXC__Installed_Product__c> newipList =[Select Id,CommissioningDateInstallDate__c,SVMXC__Warranty_End_Date__c From SVMXC__Installed_Product__c where Id in :OldNewIPMap.Values()];
        Map<Id,SVMXC__Installed_Product__c> newipMap = new map<Id,SVMXC__Installed_Product__c>();
        if(newipList.size()>0){
            newipMap.putAll(newipList);
            system.debug('Clog \n newipMap'+newipMap);
        }
        
        
        
        if(WarList.size()>0){
            for(SVMXC__Warranty__c  pw :WarList){
                if(OldNewIPMap.containskey(pw.SVMXC__Installed_Product__c)){
                    //pw.SVMXC__Installed_Product__c = OldNewIPMap.get(pw.SVMXC__Installed_Product__c);
                    
                    system.debug('Clog \n ProdcuWar:'+pw.SVMXC__Installed_Product__c);
                    
                    system.debug('Clog \n ProdcuWar_OldNewIPMap:'+OldNewIPMap.get(pw.SVMXC__Installed_Product__c));
                    
                    system.debug('Clog \n ProdcuWar_OldNewIPMap2:'+newipMap.get(OldNewIPMap.get(pw.SVMXC__Installed_Product__c)));
                    
                    system.debug('Clog \n OldIPMap:'+ oldipMap.get(pw.SVMXC__Installed_Product__c));
                    
                    
                    if(newipMap.containskey(OldNewIPMap.get(pw.SVMXC__Installed_Product__c))){
                       // pw.SVMXC__Start_Date__c = newipMap.get(OldNewIPMap.get(pw.SVMXC__Installed_Product__c)).CommissioningDateInstallDate__c;
                        
                        pw.SVMXC__Start_Date__c = swapassetList[0].CommissioningDateInstallDate__c; //Commented by VISHNU C for BR 8815
                        
                        system.debug('Clog \n ProdcuWar_StartDate:'+newipMap.get(OldNewIPMap.get(pw.SVMXC__Installed_Product__c)).CommissioningDateInstallDate__c);
                    }
                    if(oldipMap.containskey(pw.SVMXC__Installed_Product__c) && newipMap.containskey(OldNewIPMap.get(pw.SVMXC__Installed_Product__c))){
                        
                        //Date myDate = newipMap.get(OldNewIPMap.get(pw.SVMXC__Installed_Product__c)).CommissioningDateInstallDate__c;
                        
                        Date myDate=swapassetList[0].CommissioningDateInstallDate__c;
                        System.debug('myDate:'+myDate);
                        
                        date newDate = myDate.addMonths(3);

                        //myDate.addMonths(3);
                        System.debug('newDate:'+newDate);
                        
                        system.debug('Clog\n &*&*&*&*:'+oldipMap.get(pw.SVMXC__Installed_Product__c).SVMXC__Warranty_End_Date__c);

                        /*%%%%%%%%%%%%%%%% Need to check for the correct date%%%%%%%% */
                        if(oldipMap.get(pw.SVMXC__Installed_Product__c).SVMXC__Warranty_End_Date__c>=newDate ){
                            pw.SVMXC__End_Date__c = oldipMap.get(pw.SVMXC__Installed_Product__c).SVMXC__Warranty_End_Date__c; //Commented by VISHNU C for BR 8815
                            
                            system.debug('Clog \n ProdcuWar_EndDate:'+oldipMap.get(pw.SVMXC__Installed_Product__c).SVMXC__Warranty_End_Date__c);
                        }
                        else{
                            pw.SVMXC__End_Date__c =newDate; //Commented by VISHNU C for BR 8815
                            system.debug('Clog \n ProdcuWar_EndDate2:'+oldipMap.get(pw.SVMXC__Installed_Product__c).SVMXC__Warranty_End_Date__c);
                            
                        }
                    }
                    pw.SVMXC__Installed_Product__c = OldNewIPMap.get(pw.SVMXC__Installed_Product__c);
                    
                    system.debug('pw.SVMXC__End_Date__c:'+pw.SVMXC__End_Date__c);
                    
                    warlistToUpdate.add(pw); //Commented by VISHNU C for BR 8815
                    system.debug('Clog\n warlistToUpdate:'+warlistToUpdate);
                }           
            }
        }
        
        if(warlistToUpdate.size()>0){
            Database.saveresult[] sr = Database.Update(warlistToUpdate,false);
        }
    }
    //Loosing Installed Product
        
    public Static  void LoosingIpMapping(Map<Id,Swap_Asset__c> OldIpIdASMap,Map<Id,Id> OldNewIPMap){
        //OldIpIdASMap
        
                
        List<SVMXC__Installed_Product__c> oldipListtoupdate = new List<SVMXC__Installed_Product__c>();
        List<SVMXC__Installed_Product__c> oldipList = [Select Id,CommissioningDateInstallDate__c,SVMXC__Company__c,SVMXC__Site__c,LifeCycleStatusOfTheInstalledProduct__c,AssetCategory2__c,DecomissioningDate__c,UnderContract__c,SVMXC__Warranty_Start_Date__c,PurchasedDate__c,
        SVMXC__Date_Shipped__c,EndOfInstalledProductLifeDate__c,SVMXC__Warranty_End_Date__c,PartSwapHistory__c From SVMXC__Installed_Product__c where Id in :OldNewIPMap.keyset()];
        
        String swapped = Label.CLDEC15SRV01;
        String  inactive =label.CLAPR14SRV00;
        Map<Id,SVMXC__Installed_Product__c> oldipMap = new map<Id,SVMXC__Installed_Product__c>();
        if(oldipList.size()>0){
            oldipMap.putAll(oldipList);
        }
        for(SVMXC__Installed_Product__c ip :oldipList){
            ip.SVMXC__Company__c = null;
            ip.SVMXC__Site__c=null;
            ip.LifeCycleStatusOfTheInstalledProduct__c=swapped;
            ip.AssetCategory2__c=inactive;
            if(OldIpIdASMap.containskey(ip.id)){
                ip.DecomissioningDate__c=OldIpIdASMap.get(ip.id).DecommissionedDate__c;
            }
            ip.UnderContract__c=false;
            ip.SVMXC__Warranty_End_Date__c=null;
            ip.SVMXC__Warranty_Start_Date__c=null;
            ip.PurchasedDate__c=null;
            ip.SVMXC__Date_Shipped__c=null;
            ip.SVMXC__Date_Shipped__c=null;
            //Need to change its a formula field
            //ip.EndOfInstalledProductLifeDate__c='';
            ip.SVMXC__Warranty_End_Date__c=null;
            ip.SVMXC__Date_Installed__c=null;
            ip.ReplacedbyIP__c = OldNewIPMap.get(ip.Id);
            
               
            ip.SwapDateTime__c = datetime.valueOf(DateTime.now().format('yyyy-MM-dd hh:mm:ss'));
            ip.Tech_SwapAssetHistoryRich__c = oldipMap.get(ip.Id).PartSwapHistory__c;
            ip.IsreplacementIP__c = false;
            ip.CommissioningDateInstallDate__c=null;
            
            oldipListtoupdate.add(ip);
        }
        if(oldipListtoupdate.size()>0){
            Database.saveresult[] sr = Database.Update(oldipListtoupdate,false);
        }
        
    }
    //Gaining Installed Product NewIpIdASMap
   
    public Static  void GainingIpmapping(Map<Id,Swap_Asset__c> newIpIdASMap,Map<Id,Id> OldNewIPMap){
    
        //M//ap<Id,Swap_Asset__c> newIpIdASMap =(Map<Id,Swap_Asset__c>)JSON.deserialize(serialized,Map<Id,Swap_Asset__c>.class);
        
        map<Id,SVMXC__Installed_Product__c> NewIpIdOldIpMap= new map<Id,SVMXC__Installed_Product__c>();
        
        List<SVMXC__Installed_Product__c> oldipList = [Select Id,CommissioningDateInstallDate__c,SVMXC__Company__c,SVMXC__Site__c,LifeCycleStatusOfTheInstalledProduct__c,AssetCategory2__c,DecomissioningDate__c,UnderContract__c,                 SVMXC__Warranty_Start_Date__c,PurchasedDate__c,
        SVMXC__Date_Shipped__c,EndOfInstalledProductLifeDate__c,SVMXC__Warranty_End_Date__c From SVMXC__Installed_Product__c where Id in :OldNewIPMap.keyset()];
        system.debug('Clog \n oldipList :'+oldipList);
        
        List<SVMXC__Installed_Product__c> iptoUpdate = new List<SVMXC__Installed_Product__c>();
        if(oldipList.size()>0){
            for(SVMXC__Installed_Product__c ip :oldipList){
            
                NewIpIdOldIpMap.put(OldNewIPMap.get(ip.id), ip );
            }
        }
        
        String  inuse =label.CLAPR14SRV06;
        String  active = label.CLAPR14SRV01;
        system.debug('Clog \n NewIpIdOldIpMap :'+NewIpIdOldIpMap);
        
        List<SVMXC__Installed_Product__c> newipList = [Select Id,SVMXC__Company__c,SVMXC__Site__c,LifeCycleStatusOfTheInstalledProduct__c,AssetCategory2__c,DecomissioningDate__c,UnderContract__c,                 SVMXC__Warranty_Start_Date__c,PurchasedDate__c,
        SVMXC__Date_Shipped__c,EndOfInstalledProductLifeDate__c,SVMXC__Warranty_End_Date__c From SVMXC__Installed_Product__c where Id in :OldNewIPMap.values()];
        
        system.debug('Clog \n newipList :'+newipList);
       
        if(newipList.size()>0){
            for(SVMXC__Installed_Product__c ip :newipList){
                if(NewIpIdOldIpMap.containskey(ip.Id)){
                
                    system.debug('Clog \n ***1111 :'+NewIpIdOldIpMap.get(ip.Id).SVMXC__Company__c);
                    
                    ip.SVMXC__Company__c = NewIpIdOldIpMap.get(ip.Id).SVMXC__Company__c;
                    ip.SVMXC__Site__c = NewIpIdOldIpMap.get(ip.Id).SVMXC__Site__c;
                    ip.LifeCycleStatusOfTheInstalledProduct__c=inuse;
                    
                    //This needs to be checked becuase if Product warranty is updated it be updated also as trigger is there in PW after update/insert
                    //ip.SVMXC__Warranty_End_Date__c
                    
                    ip.AssetCategory2__c = active;
                    
                    ip.PurchasedDate__c = null;
                    if(newIpIdASMap.containskey(ip.id)){
                        ip.CommissioningDateInstallDate__c=newIpIdASMap.get(ip.id).CommissioningDateInstallDate__c;
                    }
                  	//ip.SVMXC__Date_Installed__c=null;
                    ip.ReplacedIP__c = NewIpIdOldIpMap.get(ip.Id).Id;
                    
                     //Datetime dt = (DateTime)json.deserialize('"' + Datetime.now() + '"', datetime.class);
                    ip.SwapDateTime__c = datetime.valueOf(DateTime.now().format('yyyy-MM-dd hh:mm:ss'));
                    ip.IsreplacementIP__c = true; 
                    
                    iptoUpdate.add(ip);
                }
            }
            
            if(iptoUpdate.size()>0){
                Database.saveresult[] sr = Database.Update(iptoUpdate,false);
            }
            
            //swapping DownStream Link
            List<JctAssetLink__c> jastlistDown = new List<JctAssetLink__c>();
            
            for(JctAssetLink__c asl :[Select Id,Name,InstalledProduct__c,LinkToAsset__c from JctAssetLink__c Where LinkToAsset__c in :OldNewIPMap.keyset()]){
                asl.LinkToAsset__c =OldNewIPMap.get(asl.LinkToAsset__c);
                //asl.LinkToAsset__c = OldNewIPMap.get(asl.LinkToAsset__c);
                jastlistDown.add(asl);
            }
            if(jastlistDown.size()>0){
                Database.saveresult[] sr = Database.Update(jastlistDown,false);
            }
            //swapping UpStream Link
            List<JctAssetLink__c> jastlistup = new List<JctAssetLink__c>();
            
            for(JctAssetLink__c asl :[Select Id,Name,InstalledProduct__c,LinkToAsset__c from JctAssetLink__c Where InstalledProduct__c in :OldNewIPMap.keyset()]){
                //asl.InstalledProduct__c =OldNewIPMap.get(asl.InstalledProduct__c);
                asl.InstalledProduct__c = OldNewIPMap.get(asl.InstalledProduct__c);
                jastlistup.add(asl);
            }
            if(jastlistup.size()>0){
                Database.saveresult[] sr = Database.Update(jastlistup,false);
            }
            //swapping Service Role
            List<Role__c> rollist = new List<Role__c>();
            for(Role__c rol :[Select Id,Name,InstalledProduct__c from Role__c Where InstalledProduct__c in :OldNewIPMap.keyset()]){
                rol.InstalledProduct__c =OldNewIPMap.get(rol.InstalledProduct__c);
                rollist.add(rol);
            }
            if(rollist.size()>0){
                Database.saveresult[] sr = Database.Update(rollist,false);
            }
             
        }
    }
}