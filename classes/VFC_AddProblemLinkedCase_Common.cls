Public Class VFC_AddProblemLinkedCase_Common
{
public List<WrapperProblem> lstWrapper{get;set;}   
public List<WrapperCase> lstWrapperCases{get;set;}                   //This WrapperList is to hold associated problems. 
public list<Case> lstCase;                                              //List Holding Problem
public list<CommercialReference__c> lstCR;                                  //List Holding Commercial Refrences
public list<ProblemCaseLink__c> lstProblemLinkedCaseTemp;                       // List Problem Linked case
public Problem__c ObjPRb{get;set;}
public Case cs{get;set;}
public boolean isNoGMRDATA{get;set;}
public boolean isshowPB1{get;set;}
public boolean isshowPB2{get;set;}
public boolean isadd{get;set;}
public boolean isremove{get;set;}
public list<ProblemCaseLink__c> lstProblemLinkedCase{get;set;}
public list<ProblemCaseLink__c> lstToRemoveProblemLinkedCase{get;set;}
Public string strErrorMessage{get;set;}
public boolean isPagetabledisplay{get;set;}
Public boolean isCancel{get;set;}
public string strHdn{get;set;}
string pid;
public VFC_AddProblemLinkedCase_Common()
{ 
lstToRemoveProblemLinkedCase=new list<ProblemCaseLink__c>();
iscancel=false;
isadd=false;
isremove=false;
isshowPB1=false;
isshowPB2=false;
isNoGMRDATA=true;
isPagetabledisplay=true;
set<string> stSymptoms=new set<string>();                                  //Set to hold Symptoms
set<string> stCommercialRefrences=new set<string>();                       //Set to hold Commercial Refrences
set<string> stFamily=new set<string>();
lstWrapper=new list<WrapperProblem>();
lstWrapperCases=new list<WrapperCase>();
lstCR= new list<CommercialReference__c>();                               //Initialization of List
lstCase=new list<case>();                                         // Initialization of Problem List
lstProblemLinkedCase=new list<ProblemCaseLink__c>();
Set<string> stProblem=new Set<string>();
map<string, Problem__c> mpProblem=new map<string,Problem__c>();      //Set Holding for Problem Id 

    

    if(ApexPages.currentPage().getParameters().get('pid')!=null && ApexPages.currentPage().getParameters().get('pid')!='') // this is to check whether it is Comming From Problem Detail page
    {   isshowPB1=true;
        
         pid=ApexPages.currentPage().getParameters().get('pid');
        lstCR=[select id ,CommercialReference__c,Problem__c,Family__c,GMRCode__c,Problem__r.Symptoms__c,Problem__r.TECH_Symptoms__c from CommercialReference__c where  Problem__c =: ApexPages.currentPage().getParameters().get('pid')];
        
       // if ((lstCR.size()==0) ||  (lstCR[0].Problem__r.Symptoms__c==null) || (lstCR[0].Problem__r.TECH_Symptoms__c==null))
       if ((lstCR.size()==0) || (lstCR[0].Problem__r.TECH_Symptoms__c==null))
          { 
             // throw error message if Commercial Reference / Symptom are not filled on Problem.
             iscancel=true;
             isadd=false;
             isremove=false;
             isPagetabledisplay=false;
             isNoGMRDATA=false;
             ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, label.CLI2PDEC120008));
          }
          
      else
        {
                     
               
        lstProblemLinkedCaseTemp =[select id,Case__c,Problem__c from  ProblemCaseLink__c where Problem__c=:ApexPages.currentPage().getParameters().get('pid')];
        set<id> stc=new set<id>();
        if(lstProblemLinkedCaseTemp.size()>0)
        {
            for(ProblemCaseLink__c plc: lstProblemLinkedCaseTemp)
            {
               stc.add(plc.case__c);
            }
        }
        string strPrbCRfamily='';
        
        for(CommercialReference__c varCR: lstCR)
        {
            if(varCR.Problem__r.TECH_Symptoms__c!=null)
            {   
                if(varCR.Problem__r.TECH_Symptoms__c.contains(label.CLI2PAPR120004))  //Checking Delimetre ;
                {
                  stSymptoms.addall(varCR.Problem__r.TECH_Symptoms__c.split(label.CLI2PAPR120004));
                }
                else
                {
                  stSymptoms.add(varCR.Problem__r.TECH_Symptoms__c);
                
                }
            
            }
            if(varCR.CommercialReference__c!=null)
            {
             stCommercialRefrences.add(varCR.CommercialReference__c);
             
            }
             if(varCR.GMRCode__c!=null)
            {
            //strPrbCRfamily=varCR.GMRCode__c.substring(0,7)+'%';
             strPrbCRfamily=varCR.GMRCode__c.substring(0,8);
            stFamily.add(strPrbCRfamily);
            }
        }
        system.debug('***stSymptoms'+stSymptoms);
        system.debug('***stFamily'+stFamily);
        system.debug('***stCommercialRefrences'+stCommercialRefrences);
        list<case> lstCaseWithCR=new list<case>();
        
       
  /*  
    =========== OLD VERSION =========================  
     lstCaseWithCR=[select id,CommercialReference__c,CreatedDate,Status,Account.Country__r.name,TECH_GMRCode__c,SupportCategory__c,CCCountry__r.name,Symptom__c,SubSymptom__c,casenumber,Subject,CaseSymptom__c,CaseSubSymptom__c,TECH_CaseSymptoms__c from Case where (( (TECH_CaseSymptoms__c IN:stSymptoms) AND (CommercialReference__c IN :stCommercialRefrences )) OR(TECH_CaseSymptoms__c IN:stSymptoms AND (CommercialReference__c =null) AND (TECH_GMRCode__c Like : stFamily))) AND CreatedDate = LAST_N_DAYS:365 limit :integer.valueof(Label.CLI2PDEC120010)];

      system.debug('****lstCaseWithCR'+lstCaseWithCR);
       
       if(lstCaseWithCR.size()>0)
       {
       
        lstCase.addall(lstCaseWithCR);
       
       }
    =========== END - OLD VERSION ===================
    */
    
    
    
    
    lstCaseWithCR=[select id, CommercialReference__c, CreatedDate,Status,Account.Country__r.name,TECH_GMRCode__c,SupportCategory__c,CCCountry__r.name,Symptom__c,SubSymptom__c,casenumber,Subject,CaseSymptom__c,CaseSubSymptom__c,TECH_CaseSymptoms__c,TECH_GMRFamily__c from Case where (TECH_CaseSymptoms__c IN:stSymptoms) AND ((CommercialReference__c IN :stCommercialRefrences ) OR (TECH_GMRFamily__c IN : stFamily)) AND CreatedDate = LAST_N_DAYS:90 order by createddate DESC LIMIT :integer.valueof(Label.CLI2PDEC120010) ];
    
      if(lstCaseWithCR.size()>0)
       {       
         lstCase.addall(lstCaseWithCR);
       }
    
    
   /* 
     // ================ QUERY WITH ONLY CREATEDATE IN WHERE CLAUSE ======================
     lstCaseWithCR=[select id, CommercialReference__c, CreatedDate,Status,Account.Country__r.name,TECH_GMRCode__c,SupportCategory__c,CCCountry__r.name,Symptom__c,SubSymptom__c,casenumber,Subject,CaseSymptom__c,CaseSubSymptom__c,TECH_CaseSymptoms__c from Case where CreatedDate = LAST_N_DAYS:365 order by createddate DESC Limit 1000];   
     
     system.debug('****lstCaseWithCR'+lstCaseWithCR.size());
     
     if(lstCaseWithCR.size()>0)
     {
     for( case c: lstCaseWithCR)
     {
       string symptomValue = c.CaseSymptom__c + ' - ' + c.CaseSubSymptom__c;
      // system.debug('*** symptomValue'+ symptomValue );
       // string symptomValue = c.CaseSymptom__c;
        system.debug('** symptomValue'+ symptomValue );
        system.debug('** TECH_CaseSymptoms__c'+ c.TECH_CaseSymptoms__c);
        
        
        if (stSymptoms.contains(symptomValue))
        {
            system.debug('** IN - symptomValue'+symptomValue);
            if(stCommercialRefrences.contains(c.CommercialReference__c) )
            {
            system.debug('** c.CommercialReference__c'+ c.CommercialReference__c);
            lstCase.add(c);
            }
            else if(c.CommercialReference__c==null)
            {
                system.debug('*** Else - c.CommercialReference__c'+ c.CommercialReference__c);
                for(string f: stFamily)
                {
                if(c.TECH_GMRCode__c.contains(f))
                {
                    system.debug('***c.TECH_GMRCode__c'+c.TECH_GMRCode__c);
                    system.debug('***f'+f);
                    lstCase.add(c);
                }
            }
            }
        }
     }
     } 
     // ==================== END ========================================
     
      */
     system.debug('****lstCase.size'+lstCase.size());
      
       
       map<string, string> mapproblemCRNumber=new map<string,string>();
     
       set<id> stId=new set<id>();
       for(Case varCase: lstCase)
       {
        stId.add(varCase.id);
       }
     If(lstCase.size()>0 && ApexPages.currentPage().getParameters().get('act')=='ad')   ////Checking Action Parameter
     { isadd=true;
       isremove=false;
       for(case varCase: lstCase)
       { 
      
       if( stc.contains(varCase.id))
       {
       }
       else
       {
          lstWrapperCases.add(new WrapperCase(false,string.valueof(varCase.id),varCase.Subject,varCase.Status,varCase.CaseSymptom__c,varCase.Account.Country__r.name,varCase.Symptom__c,varCase.SubSymptom__c,varCase.casenumber,string.valueof(date.valueof(varCase.CreatedDate)), VarCase.CaseSymptom__c, varCase.CaseSubSymptom__c));
       }
       }
     }
     
     system.debug('## wrapperSize ##'+ lstWrapperCases.size());
    If(lstWrapperCases.size() == 0)
    {
    iscancel=true;
    isadd=false;
    isremove=false;
    isPagetabledisplay=false;
     isNoGMRDATA=false;
    if(ApexPages.currentPage().getParameters().get('act')=='ad')   ////Checking Action Parameter
    {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, label.CLI2PDEC120002));
    }else if(ApexPages.currentPage().getParameters().get('act')=='rd')   ////Checking Action Parameter
    {
       ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, label.CLI2PDEC120004));
    }
    
    }       
        }
 
}



if(ApexPages.currentPage().getParameters().get('cid')!=null && ApexPages.currentPage().getParameters().get('cid')!='') // Identifying whether request is generated from Case
{
        isshowPB2=true;
        
        cs=[select id,CommercialReference__c,TECH_GMRCode__c ,Family__c,CaseSymptom__c,CaseSubSymptom__c from Case where id=:ApexPages.currentPage().getParameters().get('cid')];
        system.debug('**cs'+cs);
       
        if ( (cs.CommercialReference__c==null || cs.Family__c==null) && cs.CaseSymptom__c == null)
        {
              iscancel=true;
              isadd=false;
              isremove=false;
              isPagetabledisplay=false;
              isNoGMRDATA=false;
        
              ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, label.CLI2PDEC120009));
                           
        }
        else
        {
                    string strCaseFamily=cs.TECH_GMRCode__c.substring(0,7)+'%';
        set<string> stPLCID=new set<string>();
        for(ProblemCaseLink__c varplctemp:[select id,Case__c,Problem__c from ProblemCaseLink__c where Case__c=:ApexPages.currentPage().getParameters().get('cid')])
        {
        
        stPLCID.add(varplctemp.Problem__c);
        }
        
        
        
        
        
        //lstCR=[select id ,CommercialReference__c,Problem__c,Problem__r.Symptoms__c,TECH_Symptoms__c,Family__c,GMRCode__c  from CommercialReference__c where  (Problem__r.Symptoms__c INCLUDES(:(cs.CaseSymptom__c+' - '+cs.CaseSubSymptom__c))) AND (CommercialReference__c=:cs.CommercialReference__c OR GMRCode__c Like :strCaseFamily)];
         lstCR=[select id ,CommercialReference__c,Problem__c,Problem__r.Symptoms__c,Problem__r.TECH_Symptoms__c,Family__c,GMRCode__c  from CommercialReference__c where  (CommercialReference__c=:cs.CommercialReference__c OR GMRCode__c Like :strCaseFamily)];

        map<string, string> mapproblemCRNumber=new map<string,string>();
        system.debug('** lstCR'+lstCR);
        system.debug('*** cs.CaseSymptom__c'+ cs.CaseSymptom__c); // SUKUMAR
        
        for(CommercialReference__c varcs :lstCR)
        {
         system.debug('***varcs.Problem__r.Symptoms__c'+varcs.Problem__r.Symptoms__c); // SUKUMAR
         system.debug('***varcs.Problem__r.TECH_Symptoms__c'+varcs.Problem__r.TECH_Symptoms__c); // SUKUMAR
         //if(varcs.Problem__r.TECH_Symptoms__c.contains(cs.CaseSymptom__c+' - '+cs.CaseSubSymptom__c))
         if(varcs.Problem__r.TECH_Symptoms__c!=null && varcs.Problem__r.TECH_Symptoms__c.contains(cs.CaseSymptom__c+' - '+cs.CaseSubSymptom__c))
         {
          stProblem.add(varcs.Problem__c);
         } 
        }
        system.debug('** stProblem'+stProblem);
        for(string str: stProblem)                   // iterating Over Problem Id
        {
         String strCommercialRefrenceNumber='';
         for(CommercialReference__c varCR: lstCR)    // Iterating Over Commercial Refrences
         {
             If(varCR.Problem__c == str)  //Checking ProblemId with Commercial RefrenceProblem Id
             {
                strCommercialRefrenceNumber= strCommercialRefrenceNumber+varCR.CommercialReference__c;
             }
         }
         mapproblemCRNumber.put(str,strCommercialRefrenceNumber);
        
        }
       
        if(stProblem.size() >0 && ApexPages.currentPage().getParameters().get('act')=='ad')  //Checking Action Parameter
        {   isadd=true;
            isremove=false;
            
            for(Problem__c varPRB: [select id ,recordtypeid ,Tech_SafetyRelated__c,Status__c,CreatedDate,Severity__c,Name,Title__c,WhatIsTheProblem__c,WhyIsItaProblem__c,SummaryOfContainment__c,Symptoms__c,SensitivityIcon__c from Problem__c where (ID IN: stProblem ) AND (SensitivityIcon__c='' ) Order By CreatedDate DESC limit :integer.valueof(Label.CLI2PDEC120010)])
            { 
                system.debug('** varPRB'+varPRB);
                if(varPRB.recordtypeid !=id.valueof(label.CLI2PAPR120014)) //Checking Record Type Supplier
                {
                    system.debug('** Supplier');
                
                    if(!stPLCID.contains(varPRB.id))
                    {
                    
                         lstWrapper.add(new WrapperProblem(false,string.valueof(varPRB.id),varPRB.name,varPRB.Title__c,varPRB.WhatIsTheProblem__c,varPRB.WhyIsItaProblem__c,varPRB.Status__c,mapproblemCRNumber.get(string.valueof(varPRB.id)),varPRB.Symptoms__c,varPRB.Severity__c,string.valueof(date.valueof(varPRB.CreatedDate))));
                    }  
                 
               }
            }
        }
       


    If(lstWrapper.size() == 0)
    {
    iscancel=true;
    isadd=false;
    isremove=false;
    isPagetabledisplay=false;
    isNoGMRDATA=false;
    if(ApexPages.currentPage().getParameters().get('act')=='ad')   // Label.CLI2PAPR120005 Representing Action 
    {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, label.CLI2PDEC120001));
    }else if(ApexPages.currentPage().getParameters().get('act')=='rd')
    {
       ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, label.CLI2PDEC120005));
    }
    }

        }      
}

}
/*
public pagereference rmvproblemlinkedcase()      // This method is Used To Remove the Existing Problem linked case
{    PageReference pageRef;
     list<ProblemCaseLink__c> lstProblemtodel=new list<ProblemCaseLink__c>();
     list<id> lst=new list<id>();
     set<id> stidtoremove=new set<id>();
    if(lstWrapper.size()>0)
    {
        for(WrapperProblem wrp:lstWrapper)
        {
            //for(ProblemCaseLink__c varPLC:lstToRemoveProblemLinkedCase)
           // {
             //  if(wrp.strPrbID==varPLC.Problem__c && varPLC.case__c==cs.Id && wrp.isPRBSelected==true)
               // {
                //    lstProblemtodel.add(varPLC);
                //}
            //}
          for(ProblemCaseLink__c varPLC:lstToRemoveProblemLinkedCase)
            {
               if(wrp.strPrbID==strHdn && varPLC.case__c==cs.Id )
                {
                    lstProblemtodel.add(varPLC);
                }
            }
        }
    }
    if(lstWrapperCases.size()>0)
    {
        for(WrapperCase wrp: lstWrapperCases)
        {
            for(ProblemCaseLink__c varPLC:lstToRemoveProblemLinkedCase)
            { 
                if(wrp.strCaseID==string.valueof(varPLC.case__c) && varPLC.Problem__c==pid &&  wrp.isPRBSelected==true)
                {
                  lstProblemtodel.add(varPLC);
                }
            }
       }
    }
    
    if(lstProblemtodel.size()>0 )
    {  
       delete lstProblemtodel;
       if(ApexPages.currentPage().getParameters().get('pid')!=null && ApexPages.currentPage().getParameters().get('pid')!='')
       {
       pageRef = new PageReference('/'+ObjPRb.Id);
       }
       else if(ApexPages.currentPage().getParameters().get('cid')!=null && ApexPages.currentPage().getParameters().get('cid')!='')
       { pageRef = new PageReference('/'+cs.Id);
       }
    }
    pageRef.setredirect(true);
    return pageRef;
}*/

Public  pagereference addProblemLinkedCase()     // This method is Used To add the Problem linked case
{  
    PageReference pageRef;
    if(ApexPages.currentPage().getParameters().get('pid')!=null && ApexPages.currentPage().getParameters().get('pid')!='')
    {   
       for(WrapperCase wrp: lstWrapperCases)
       {  
            if(wrp.isPRBSelected==true)
            {
            ProblemCaseLink__c PrbCase=new ProblemCaseLink__c(Case__c=wrp.strCaseID,Problem__c=pid);
            lstProblemLinkedCase.add(PrbCase);
            }
       }
       
        if(lstProblemLinkedCase.size()>0)
        {
           insert lstProblemLinkedCase;
           pageRef = new PageReference('/'+pid);
           pageRef.setRedirect(false);
          return pageRef;
        }
        else
        {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, Label.CLI2PDEC120003));
        return null;
        
        }
        
    }
    if(ApexPages.currentPage().getParameters().get('cid')!=null && ApexPages.currentPage().getParameters().get('cid')!='') // Identifying whether request is generated from Case
    {
        for(WrapperProblem wrp: lstWrapper)
        {
         
            if(wrp.strPrbID==strHdn)
            {
            ProblemCaseLink__c PrbCase=new ProblemCaseLink__c(Case__c=cs.Id,Problem__c=wrp.strPrbID);
            lstProblemLinkedCase.add(PrbCase);
            }
            
        }
       
        if(lstProblemLinkedCase.size()>0)
        {
           insert lstProblemLinkedCase;
            pageRef = new PageReference('/'+cs.Id);
            pageRef.setRedirect(false);
           return pageRef;
        }else
        {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, Label.CLI2PDEC120003));
        return null;
        
        }
    }

    return null;
    
}

public class WrapperCase
{
    public string  strCaseID{get;set;}                  // This Is to Hold Problem Id
    public boolean isPRBSelected{get;set;}              // this is to select  list of problem 
    public string  strProblemName{get;set;}             //this  is to hold  name of problem
    public string  sterCommercialRefrence{get;set;}     // this is to hold Commercial Refrence on Problem
    public string  strSymptoms{get;set;}                // this to hold symptoms on problem.
    public string  strCaseNumber{get;set;}              //Property to hold Case Number
    public string  strSubject{get;set;}                 //Property to hold Subject
    public string  strCaseStatus{get;set;}              //Property to hold Case Status
    public string  strCategorey{get;set;}               //Property to hold Categorey
    public string  strCCCountry{get;set;}               //Property to hold Country Name
    public string strCreationdate{get;set;}             //Property to hold Case Creation Date
    public string strSymp{get; set;}
    public string strSubSymp{get; set;}
    public WrapperCase( boolean isSelected,string caseid,string PrbName, string CaseStatus,string casecategorey,string caseCountryname,string strCommercialReftrence,string strSymptom,string casenumber,string caseCreationdate,string caseSymptom, string caseSubSymptom)
    {
        strCaseID=caseid;
        isPRBSelected=isSelected;
        strProblemName=PrbName;
        sterCommercialRefrence=strCommercialReftrence;
        strSymptoms=strSymptom;
        strCaseNumber=casenumber;
        strCaseStatus=CaseStatus;
        strCategorey=casecategorey;
        strCCCountry=caseCountryname;
        strCreationdate=caseCreationdate;
        strSymp=caseSymptom;
        strSubSymp=caseSubSymptom;
        
    }

}
public class WrapperProblem
{   
    public string  strCreateDate{get;set;}               //this is to hold strCreateDate
    public string  strSeverity{get;set;}                  //this is to hold severity
    public string  strPrbID{get;set;}                    // This Is to Hold Problem Id
    public boolean isPRBSelected{get;set;}              // this is to select  list of problem 
    public string  strProblemName{get;set;}             //this  is to hold  name of problem
    public string  sterCommercialRefrence{get;set;}     // this is to hold Commercial Refrence on Problem
    public string  strSymptoms{get;set;}                // this to hold symptoms on problem.
    public string  strTitle{get;set;}                   //this is to hold title of problem
    public string  strwhatisproblem{get;set;}           //this is to hold what is problem
    public string  strwhyisitproblem{get;set;}          //this is to hold why is problem
    public string  strcontainmentsummary{get;set;}      //this is to hold xasummary
    public WrapperProblem( boolean isSelected,string prbid,string PrbName,string problemtitle,string strwhatisproblem1,string strwhyitisproblem,string strsummarycontainment,string strCommercialReftrence,string strSymptom,string severty,string createdt)
    {
        strPrbID=prbid;
        isPRBSelected=isSelected;
        strProblemName=PrbName;
        sterCommercialRefrence=strCommercialReftrence;
        strSymptoms=strSymptom;
        strTitle=problemtitle;
        strwhatisproblem=strwhatisproblem1;
        system.debug('--------------->'+strwhatisproblem);
        strwhyisitproblem=strwhyitisproblem;
        strcontainmentsummary=strsummarycontainment;
        strSeverity=severty;
        strCreateDate=createdt;
    }
}
Public pagereference cancelAddRemovePLC()                         //This Method is Used to Redirect to Problem or Case Detail Page
{ 
PageReference pageRef;                                             //Initialization Of Page Refrence
if(ApexPages.currentPage().getParameters().get('cid') !='')         //Checking whether Parameter for Case to Redirect Case Detail Page
{
  pageRef = new PageReference('/'+cs.Id);
  pageRef.setRedirect(true);
}
else if(ApexPages.currentPage().getParameters().get('pid') !='')      //Checking whether Parameter for Problem to Redirect Case Detail Page
{
pageRef = new PageReference('/'+pid);
  pageRef.setRedirect(true);
}


return pageRef;

}



}