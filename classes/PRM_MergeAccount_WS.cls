global class PRM_MergeAccount_WS {

    global static Map<string,integer> elligibleAccMap = new Map<string,integer>();
    
    global class resultWrapper {
        webservice String returnCode;
        webservice String returnMessage;
        webservice List<String> listSuccessMergeId;
        webservice Decimal calcTime;
        webservice String TechMergeId;
        webservice List<resultStepWrapper> steps;
        webservice Integer errorCode;
        
        public resultWrapper(String code, String mess, Decimal calc){
            returnCode = code;
            returnMessage = mess;
            calcTime = calc;
            TechMergeId = '';
            steps = new List<resultStepWrapper>();
        }

        public void addStep(String stepName, Decimal calc){
            String stepNumber = 'Step '+steps.size()+': ';
            steps.add(new resultStepWrapper(stepNumber+stepName,calc));
        }
    }

    global class resultStepWrapper {
        webservice String stepName;
        webservice Decimal calcTime;

        public resultStepWrapper(String stepName, Decimal calc){
            this.stepName = stepName;
            this.calcTime = calc;
        }
    }

    webservice static resultWrapper mergeSingleAccount(String goldenId){
        return mergeAccount(goldenId,false,false);
    }

    webservice static resultWrapper mergeSingleAccountAndTransfer(String goldenId){
        return mergeAccount(goldenId,true,true);
    }


    webservice static resultWrapper mergeAccountSkipVerification(List<String> listSEAccId){
        if(listSEAccId.size()!=1){
            DateTime startTime = datetime.now();
            DateTime endTime = datetime.now();
            resultWrapper res = new resultWrapper('', '', 0);
            endTime = dateTime.now();
            res.calcTime = endTime.getTime() - startTime.getTime();
            res.returnCode = 'E';
            res.returnMessage = 'Merge Account WS accept only one Account';
            return res;
        }else{
            return mergeAccount(listSEAccId[0],true,false);
        }
    }

    webservice static resultWrapper mergeAccountAndTransfer(List<String> listSEAccId){
        if(listSEAccId.size()!=1){
            DateTime startTime = datetime.now();
            DateTime endTime = datetime.now();
            resultWrapper res = new resultWrapper('', '', 0);
            endTime = dateTime.now();
            res.calcTime = endTime.getTime() - startTime.getTime();
            res.returnCode = 'E';
            res.returnMessage = 'Merge Account WS accept only one Account';
            return res;
        }else{
            return mergeAccount(listSEAccId[0],true,true);
        }
    }

    webservice static resultWrapper mergeAccount(List<String> listSEAccId){
        if(listSEAccId.size()!=1){
             DateTime startTime = datetime.now();
            DateTime endTime = datetime.now();
            resultWrapper res = new resultWrapper('', '', 0);
            endTime = dateTime.now();
            res.calcTime = endTime.getTime() - startTime.getTime();
            res.returnCode = 'E';
            res.returnMessage = 'Merge Account WS accept only one Account';
            return res;
        }else{
            return mergeAccount(listSEAccId[0],false,false);
        }
    }
    
    private static resultWrapper mergeAccount(String goldenId,Boolean skipVerification,Boolean transfer){
        Savepoint sp;
        DateTime startTime = datetime.now();
        DateTime endTime = datetime.now();
        Map<string,Account> businessAccMap = new Map<string,Account>();

        

        resultWrapper res = new resultWrapper('', '', 0);
        res.listSuccessMergeId = new List<String>();
        
        
        List<PRM_Technical_Partner_Record__c> listRecords = new List<PRM_Technical_Partner_Record__c>();
        List<PRM_Technical_Merge_Record__c> listRecords2 = new List<PRM_Technical_Merge_Record__c>();
        
        //Get Business Account & PRM Account RecordType IDs
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Id partnerAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='PRMAccount' limit 1][0].Id;
        
        List<String> listPartnerAccountId = new List<String>();
        
        try{
            
            for(AggregateResult ar : [SELECT PRMUIMSSEAccountId__c, COUNT(Name) FROM Account
                WHERE RecordtypeId =: partnerAccRTId AND PRMUIMSID__c <> null AND isPartner = true AND PRMUIMSSEAccountId__c =: goldenId
                GROUP BY PRMUIMSSEAccountId__c HAVING COUNT(Name) = 1]){
                    listPartnerAccountId.add(String.valueOf(ar.get('PRMUIMSSEAccountId__c')));
            }
            
            //Process all AggregateResult and store them in a Map with the count of each group
            for(AggregateResult ar : [SELECT SEAccountID__c, COUNT(Name) FROM Account WHERE RecordtypeId =: businessAccRTId AND PRMUIMSID__c =: null
                AND SEAccountID__c IN: listPartnerAccountId 
                GROUP BY SEAccountID__c HAVING COUNT(Name) >= 1]){
                    elligibleAccMap.put(string.valueof(ar.get('SEAccountID__c')),Integer.valueOf(ar.get('expr0')));
            }
            System.debug('##########################elligibleAccMap = ' + elligibleAccMap);   
            res.addStep('same SEAccountID__c check',datetime.now().getTime()-startTime.getTime());
            List<String> checkList = new List<String>();
            for(String s : elligibleAccMap.keySet()){
                if(elligibleAccMap.get(s) != 1){
                    endTime = dateTime.now();
                    res.calcTime = endTime.getTime() - startTime.getTime();
                    res.returnCode = 'E';
                    res.returnMessage = 'n survivor accounts with the same SEAccountID__c: \''+s+'\'. The merge has to be done manually';
                    return res;
                  } 
                checkList.add(s);
             }
           List<Account> accs = new List<Account>([SELECT Id FROM Account WHERE SEAccountID__c IN : checkList AND Owner.ContactId != null]);
           
            for(Account a : accs){               
                    res.addStep('same SEAccountID__c check',datetime.now().getTime()-startTime.getTime());                       
                    endTime = dateTime.now();
                    res.calcTime = endTime.getTime() - startTime.getTime();
                    res.returnCode = 'E';
                    res.returnMessage = 'This account has been owned by an External User: \''+a+'\'. The merge has to be done manually';
                    return res;                           
              }   
        

            
            if(elligibleAccMap.size() > 0){
                /*if(transfer){
                    res.addStep('initial eligibility check',datetime.now().getTime()-startTime.getTime());
            
                    Lead[] myLead = [select Id,Account__r.PRMUIMSSEAccountId__c from Lead where Account__r.PRMUIMSSEAccountId__c =:goldenId and Contact__c=null];
                    List<Lead> myLeadToUpdate = new  List<Lead>();
                    if(myLead.size()>0){
                        for(Integer i=0;i<myLead.size();i++){
                            myLeadToUpdate.add(new Lead(iD=myLead[i].id,Account__r = new Account(SEAccountId__c=myLead[i].Account__r.PRMUIMSSEAccountId__c)));
                        }
                        update myLeadToUpdate;
                    }
                    myLead = [select Id,Account__r.PRMUIMSSEAccountId__c,Contact__r.PRMUIMSSEContactId__c from Lead where Account__r.PRMUIMSSEAccountId__c =:goldenId and Contact__c!=null and Contact__r.PRMUIMSSEContactId__c!=null];
                    myLeadToUpdate = new  List<Lead>();
                    if(myLead.size()>0){
                        for(Integer i=0;i<myLead.size();i++){
                            myLeadToUpdate.add(new Lead(iD=myLead[i].id,Account__r = new Account(SEAccountId__c=myLead[i].Account__r.PRMUIMSSEAccountId__c),Contact__r=new Contact(SEContactId__c=myLead[i].Contact__r.PRMUIMSSEContactId__c)));
                        }
                        update myLeadToUpdate;
                    }
                    Case[] myCase = [select Id,Account.PRMUIMSSEAccountId__c from Case where Account.PRMUIMSSEAccountId__c =:goldenId]; 
                    List<Case> myCaseToUpdate = new  List<Case>();
                    if(myCase.size()>0){
                        for(Integer i=0;i<myCase.size();i++){
                            myCaseToUpdate.add(new Case(iD=myCase[i].id,Account = new Account(SEAccountId__c=myCase[i].Account.PRMUIMSSEAccountId__c)));
                        }
                        update myCaseToUpdate;
                    }

                    Case[] myCase = [select Id,Account.PRMUIMSSEAccountId__c,Contact.PRMUIMSSEContactId__c,Status from Case where Account.PRMUIMSSEAccountId__c =:goldenId]; 
                    List<Case> myCaseToUpdate = new  List<Case>();
                    Map<String,String> caseByStatus = new Map<String,String>();
                    if(myCase.size()>0){
                        for(Integer i=0;i<myCase.size();i++){
                             Case caseUpdate;
                            if(myCase[i].Contact!=null && myCase[i].Contact.PRMUIMSSEContactId__c!=null){
                                caseUpdate = new Case(iD=myCase[i].id,Account = new Account(SEAccountId__c=myCase[i].Account.PRMUIMSSEAccountId__c),Contact = new Contact(SEContactId__c=myCase[i].Contact.PRMUIMSSEContactId__c));
                                if(myCase[i].Status!=null || myCase[i].Status.equals('Closed') || myCase[i].Status.equals('Cancelled')){
                                    caseByStatus.put(myCase[i].Contact.PRMUIMSSEContactId__c,myCase[i].Status);
                                    caseUpdate.Status='In Progress';
                                }
                            }else{
                                caseUpdate = new Case(iD=myCase[i].id,Account = new Account(SEAccountId__c=myCase[i].Account.PRMUIMSSEAccountId__c));
                            }
                            myCaseToUpdate.add(caseUpdate);
                        }
                        update myCaseToUpdate;
                    }
                    if(myCaseToUpdate.size()>0){
                    myCase = [select Id,Contact.SEContactId__c from Case where id in:myCaseToUpdate and ContactId!=null]; 
                    List<Case> myCaseToUpdate2 = new List<Case>();
                        if(myCase.size()>0){
                            for(Integer i=0;i<myCase.size();i++){
                                if(caseByStatus.containsKey(myCase[i].Contact.SEContactId__c)){
                                    myCaseToUpdate2.add(new Case(iD=myCase[i].id,Status=caseByStatus.get(myCase[i].Contact.SEContactId__c)));
                                }
                            }
                            update myCaseToUpdate2;
                        }
                    }*/
                    /*
                   Opportunity[] myOpp = [select Id,Account.PRMUIMSSEAccountId__c,EndUserCustomer__r.PRMUIMSSEAccountId__c,FinalCustSiteAcc__r.PRMUIMSSEAccountId__c from Opportunity where Account.PRMUIMSSEAccountId__c =:goldenId or EndUserCustomer__r.PRMUIMSSEAccountId__c =:goldenId or FinalCustSiteAcc__r.PRMUIMSSEAccountId__c =:goldenId];
                    List<Opportunity> myOppToUpdate = new  List<Opportunity>();
                    if(myOpp.size()>0){
                        for(Integer i=0;i<myOpp.size();i++){
                            Opportunity myOpportunity = new Opportunity();
                            Account acc = myOpp[i].Account.PRMUIMSSEAccountId__c!=null?new Account(SEAccountId__c=myOpp[i].Account.PRMUIMSSEAccountId__c):null;
                            Account endUserAccount = myOpp[i].EndUserCustomer__r.PRMUIMSSEAccountId__c!=null?new Account(SEAccountId__c=myOpp[i].EndUserCustomer__r.PRMUIMSSEAccountId__c):null;
                            Account finalCustSiteAcc = myOpp[i].FinalCustSiteAcc__r!=null?new Account(SEAccountId__c=myOpp[i].FinalCustSiteAcc__r.PRMUIMSSEAccountId__c):null;
                            myOppToUpdate.add(new Opportunity(iD=myOpp[i].id,Account = acc,EndUserCustomer__r = endUserAccount,FinalCustSiteAcc__r = finalCustSiteAcc));
                        }
                        update myOppToUpdate;
                    }

                    Account[] accountList = [select (SELECT Id FROM ActivityHistories),(SELECT Id FROM OpenActivities)  from Account where PRMUIMSSEAccountId__c =:goldenId];
                    List<Id> activityList = new List<Id>();
                    for(Account acc:accountList){
                        for(ActivityHistory actHist:acc.ActivityHistories){
                            activityList.add(actHist.Id);
                        }
                        for(OpenActivity opAct:acc.OpenActivities){
                            activityList.add(opAct.Id);
                        }
                    }
                    if(activityList.size()>0){
                        List<SObject> myTaskToUpdate = new  List<SObject>();
                        SObject[] myTask = Database.query('select Id,Account.PRMUIMSSEAccountId__c from Task where id in:activityList'); 
                        for(Integer i=0;i<myTask.size();i++){
                            SObject obj = myTask[i].id.getSObjectType().newSObject(myTask[i].id);
                            Account a = (Account)myTask[i].getSObject('Account');
                            obj.putSObject('What',new Account(SEAccountId__c=a.PRMUIMSSEAccountId__c));
                            myTaskToUpdate.add(obj);
                        }
                        update myTaskToUpdate;

                        List<SObject> myEventToUpdate = new  List<SObject>();
                        SObject[] myEvent = Database.query('select Id,Account.PRMUIMSSEAccountId__c from Event where id in:activityList and isChild=false and isRecurrence=false'); 
                        for(Integer i=0;i<myEvent.size();i++){
                            SObject obj = myEvent[i].id.getSObjectType().newSObject(myEvent[i].id);
                            Account a = (Account)myEvent[i].getSObject('Account');
                            obj.putSObject('What',new Account(SEAccountId__c=a.PRMUIMSSEAccountId__c));
                            myEventToUpdate.add(obj);
                        }
                        update myEventToUpdate;
                    }

                }else if(!skipVerification){

                    Lead[] myLead = [select Id from Lead where Account__r.PRMUIMSSEAccountId__c =:goldenId];
                    if(myLead.size()>0){
                        endTime = dateTime.now();
                        res.calcTime = endTime.getTime() - startTime.getTime();
                        res.returnCode = 'E';
                        res.returnMessage = 'Partner Account have Lead(s):'+myLead;
                        return res;
                   }
                     //Check For Case
                   Case[] myCase = [select Id from Case where Account.PRMUIMSSEAccountId__c =:goldenId];
                    if(myCase.size()>0){
                        endTime = dateTime.now();
                        res.calcTime = endTime.getTime() - startTime.getTime();
                        res.returnCode = 'E';
                        res.returnMessage = 'Partner Account have Case(s):'+myCase;
                        return res;
                   }

                   Opportunity[] myOpp = [select Id from opportunity where Account.PRMUIMSSEAccountId__c =:goldenId or EndUserCustomer__r.PRMUIMSSEAccountId__c =:goldenId or FinalCustSiteAcc__r.PRMUIMSSEAccountId__c =:goldenId];
                   if(myOpp.size()>0){
                        endTime = dateTime.now();
                        res.calcTime = endTime.getTime() - startTime.getTime();
                        res.returnCode = 'E';
                        res.returnMessage = 'Partner  Account have an Opportunity:'+myOpp[0].Id;
                        return res;
                   }

                   Account[] acc = [select (SELECT Id FROM ActivityHistories),(SELECT Id FROM OpenActivities)  from Account where PRMUIMSSEAccountId__c =:goldenId];
                   if(acc.size()>0){
                        if(acc[0].ActivityHistories.size()>0 || acc[0].OpenActivities.size()>0){
                            endTime = dateTime.now();
                            res.calcTime = endTime.getTime() - startTime.getTime();
                            res.returnCode = 'E';
                            res.returnMessage = 'Partner Account have an Open Activity or an Activity History: '+acc[0].ActivityHistories +' / '+acc[0].OpenActivities;
                            return res;
                       }
                    }
                }*/
                
                //Elligible business account must be enable as partner account (isPartner = true)
                List<Account> listAccToUpdate = new List<Account>();
                for(Account a : [SELECT Id, isPartner, RecordtypeId, PRMUIMSID__c, SEAccountID__c
                FROM Account WHERE SEAccountID__c IN: elligibleAccMap.keySet()]){
                    if(a.RecordtypeId == businessAccRTId && a.PRMUIMSID__c == null && elligibleAccMap.get(a.SEAccountId__c) == 1 && a.isPartner == false){
                        PRM_Technical_Partner_Record__c p = new PRM_Technical_Partner_Record__c();
                        try{
                            a.isPartner = true;
                            listAccToUpdate.add(a);
                            p.Name = a.SEAccountID__c;
                            p.PRMUIMS__c = a.PRMUIMSID__c;
                            p.Record_Id__c = a.Id;
                            p.Object__c = 'Account';
                            p.Success__c = true;
                            listRecords.add(p);
                        }
                         catch (Exception e){
                            p.Name = a.SEAccountID__c;
                            p.PRMUIMS__c = a.PRMUIMSID__c;
                            p.Record_Id__c = a.Id;
                            p.Object__c = 'Account';
                            p.Error__c = true;
                            p.Error_Message__c = fromObjectToStringResizedForDisplay(e.getMessage());
                            listRecords.add(p);
                            throw e;
                        }
                    }
                }
                sp = Database.setSavepoint();
                
                //if associated contacts to a B.A. have external Users as Owner => update temporary the Contact Owner
                Map<Id,Id> mapExternalOwnerIdByContactId = new Map<Id,Id>();
                List<Contact> conExtOwnerToUpdate = new List<Contact>();
                for(Contact con: [SELECT Id,OwnerId,Account.OwnerId from contact where AccountId IN: listAccToUpdate and OwnerId in (SELECT Id from User where (UserType ='PowerPartner' or UserType ='CSPLitePortal' or UserType ='PowerCustomerSuccess' or UserType='CustomerSuccess'))]){
                    mapExternalOwnerIdByContactId.put(con.Id,con.OwnerId);
                    con.ownerId = con.Account.OwnerId;
                    conExtOwnerToUpdate.add(con);
                    System.debug('Con OwnerId'+con.OwnerId);
                }
                System.debug('conExtOwnerToUpdate'+conExtOwnerToUpdate);
                //if childs contacts have external Users as Owner => update temporary the Contact
                if(conExtOwnerToUpdate.size()>0){
                    update conExtOwnerToUpdate;
                    System.debug('Updated conExtOwnerToUpdate'+conExtOwnerToUpdate);
                }
                //update isPartner
                update listAccToUpdate;
                
                System.debug('Updated conExtOwnerToUpdate'+conExtOwnerToUpdate);
                res.addStep('Update Account (isPartner = true)',datetime.now().getTime()-startTime.getTime());
                endTime = dateTime.now();
                
                PRMTechnicalMergeHistory__c accountMergeHistory = new PRMTechnicalMergeHistory__c(Batch_Start_Time__c = startTime,
                Batch_End_Time__c = endTime);
                insert accountMergeHistory;
                
                for(PRM_Technical_Partner_Record__c r: listRecords){
                    r.Batch_Id__c = accountMergeHistory.Id;
                }
                insert listRecords;
                
                //Get all accounts from the elligibleAccMap and store SurvivorAccount and ToBeDeletedAccount in their respective Maps
                Map<String,Account> SurvivorAccountMap = new Map<String,Account>();
                Map<String,Account> ToBeDeletedAccountMap = new Map<String,Account>();
                
                List<CS_PRMTechnicalMergeFieldMapping__c> mFields = CS_PRMTechnicalMergeFieldMapping__c.getAll().values();

                List<String> flds = new List<String>();
                for(CS_PRMTechnicalMergeFieldMapping__c tm : mfields){
                    if('Account'.equalsIgnoreCase(tm.ObjectName__c)) flds.add(tm.FieldAPIName__c);
                }
            
                String str = '';
                str = String.join(flds, ', ');
                
                List<String> keys = new List<String>();
                for(String k : elligibleAccMap.keySet()){
                    String k1='\''+k+'\'';
                    keys.add(k1);
                }
                String str1 = '';
                str1 = String.join(keys, ', ');
                
                //Survivor Account
                String query = 'SELECT Id, SEAccountID__c, RecordtypeId,' + str + ' FROM Account WHERE SEAccountID__c IN (' + str1 + ') AND RecordtypeId = \'' +businessAccRTId + '\' AND PRMUIMSID__c = null';
                List<Account> survList = Database.query(query);
                
                for(Account a : survList){
                    //Recordtype = Business Account AND PRMUIMSId = null => Survivor Account
                    SurvivorAccountMap.put(a.SEAccountID__c, a);
                    
                }
                
                //ToBeDeleted Account
                String query1 = 'SELECT Id, SEAccountId__c, RecordtypeId,' + str + ' FROM Account WHERE PRMUIMSSEAccountId__c IN (' + str1 + ') AND RecordtypeId = \'' + partnerAccRTId + '\' AND PRMUIMSID__c <> null';
                List<Account> delList = Database.query(query1);
                
                for(Account a : delList){
                    //Recordtype = Partner Account AND PRMUIMSId != null => Tobedeleted Account
                    ToBeDeletedAccountMap.put(a.PRMUIMSSEAccountId__c, a);
                }
                System.debug('##########################SurvivorAccountMap = ' + SurvivorAccountMap);
                System.debug('##########################ToBeDeletedAccountMap = ' + ToBeDeletedAccountMap);
                

                res.addStep('Construct Maps SurvivorAccountMap/ToBeDeletedAccountMap',datetime.now().getTime()-startTime.getTime());
                //Copy of the PRM fields values in the Survivor Account
                List<Account> listAccToBeUpdated = new List<Account>();
                
               
                //Merge the account & child records
                for(String s : SurvivorAccountMap.keySet()){
                    PRM_Technical_Merge_Record__c p = new PRM_Technical_Merge_Record__c();
                    if(elligibleAccMap.get(s) == 1){
                        if(ToBeDeletedAccountMap.containsKey(s)){
                            try{
                                //API FIELO Merge Members
                                FieloPRM_REST_MoveMembersByAccount.postMoveMembersByAccount(ToBeDeletedAccountMap.get(s).Id, SurvivorAccountMap.get(s).Id);                          
                                Account a = new Account();
                                a = copyPRMField(SurvivorAccountMap.get(s), ToBeDeletedAccountMap.get(s));
                                listAccToBeUpdated.add(a);
                                
                                Merge SurvivorAccountMap.get(s) ToBeDeletedAccountMap.get(s);
                                p.Name = SurvivorAccountMap.get(s).SEAccountID__c;
                                p.PRMUIMS__c = SurvivorAccountMap.get(s).PRMUIMSID__c;
                                p.Deleted_Record_Id__c = ToBeDeletedAccountMap.get(s).Id;
                                p.Survivor_Id__c = SurvivorAccountMap.get(s).Id;
                                p.Object__c = 'Account';
                                p.Success__c = true;
                                listRecords2.add(p);
                                res.listSuccessMergeId.add(SurvivorAccountMap.get(s).SEAccountID__c);
                                //if childs contacts have external Users as Owner =>  => put back the external Owner as Owner
                                if(conExtOwnerToUpdate.size()>0){
                                    for(Contact con:conExtOwnerToUpdate){
                                        con.ownerId = mapExternalOwnerIdByContactId.get(con.Id);
                                    }
                                    update conExtOwnerToUpdate;
                                }
                                
                            }
                            catch (Exception e){
                                p.Name = SurvivorAccountMap.get(s).SEAccountID__c;
                                p.PRMUIMS__c = SurvivorAccountMap.get(s).PRMUIMSID__c;
                                p.Deleted_Record_Id__c = ToBeDeletedAccountMap.get(s).Id;
                                p.Survivor_Id__c = SurvivorAccountMap.get(s).Id;
                                p.Object__c = 'Account';
                                p.Error__c = true;
                                p.Error_Message__c = fromObjectToStringResizedForDisplay(e.getMessage());
                                listRecords2.add(p);
                                throw e;
                            }
                        }
                    }
                    else{
                        p.Name = SurvivorAccountMap.get(s).SEAccountID__c;
                        p.PRMUIMS__c = SurvivorAccountMap.get(s).PRMUIMSID__c;
                        p.Deleted_Record_Id__c = ToBeDeletedAccountMap.get(s).Id;
                        p.Survivor_Id__c = SurvivorAccountMap.get(s).Id;
                        p.Object__c = 'Account';
                        p.Error__c = true;
                        p.Error_Message__c = 'n survivor accounts with the same SEAccountID__c. The merge has to be done manually';
                        listRecords2.add(p);
                    }
                }
                res.addStep('Merge Account',datetime.now().getTime()-startTime.getTime());
                if(listAccToBeUpdated.size()>0)
                 update listAccToBeUpdated;

                res.addStep('Update Account',datetime.now().getTime()-startTime.getTime());
                
                res.returnCode = 'S';
                res.returnMessage = res.listSuccessMergeId.size() + ' success / ' + listRecords2.size();
                return res;
            }
            else{
                res.calcTime = endTime.getTime() - startTime.getTime();
                res.returnCode = 'S';
                res.returnMessage = '0 merge done';
                return res;
            }
        }catch (Exception e){
            Database.rollback(sp);
            res.returnCode = 'E';
            res.returnMessage = res.listSuccessMergeId.size() + ' Error / ' + listRecords2.size() + '. Exception: '+e.getMessage();
            res.errorCode = (e!=null && e.getMessage().contains(System.Label.PRM_Merge_GroupMembershipError))?1:0;
            throw e;   
        }finally{
            DateTime startTime2 = dateTime.now();
            endTime = datetime.now();
            
            PRMTechnicalMergeHistory__c accountMergeHistory2 = new PRMTechnicalMergeHistory__c(Batch_Start_Time__c = startTime2,
            Batch_End_Time__c = endTime);
            insert accountMergeHistory2;
            
            for(PRM_Technical_Merge_Record__c r: listRecords2){
                r.Batch_Id__c = accountMergeHistory2.Id;
            }
            system.debug('#listRecords2 size'+listRecords2.size());
            system.debug('#listRecords2'+listRecords2);
            insert listRecords2;

            res.addStep('Insert PRM_Technical_Merge_Record__c /aGI',datetime.now().getTime()-startTime.getTime());
            res.calcTime = endTime.getTime() - startTime.getTime();
            res.TechMergeId = accountMergeHistory2.Id;
            return res;
        }
    }
    
     global static Account copyPRMField(Account Survivor, Account toBeDeleted){
     /*
        Survivor.PRMUIMSSEAccountId__c = toBeDeleted.PRMUIMSSEAccountId__c;
        Survivor.PRMAccountProfileComplete__c = toBeDeleted.PRMAccountProfileComplete__c;
        Survivor.PRMAccountRegistrationStatus__c = toBeDeleted.PRMAccountRegistrationStatus__c;
        Survivor.PRMStreet__c = toBeDeleted.PRMStreet__c;
        Survivor.PRMAdditionalAddress__c = toBeDeleted.PRMAdditionalAddress__c;
        Survivor.PRMAreaOfFocus__c = toBeDeleted.PRMAreaOfFocus__c;
        Survivor.PRMBusinessType__c = toBeDeleted.PRMBusinessType__c;
        Survivor.PRMCity__c = toBeDeleted.PRMCity__c;
        Survivor.PRMCompanyName__c = toBeDeleted.PRMCompanyName__c;
        Survivor.PRMCompanyPhone__c = toBeDeleted.PRMCompanyPhone__c;
        Survivor.PRMWebsite__c = toBeDeleted.PRMWebsite__c;
        Survivor.PRMCountry__c = toBeDeleted.PRMCountry__c;
        Survivor.PRMCurrencyIsoCode__c = toBeDeleted.PRMCurrencyIsoCode__c;
        Survivor.PRMPLShowOnMap__c = toBeDeleted.PRMPLShowOnMap__c;
        Survivor.PRMDomainsOfExpertise__c = toBeDeleted.PRMDomainsOfExpertise__c;
        Survivor.PRMEmployeeSize__c = toBeDeleted.PRMEmployeeSize__c;
        Survivor.PRMExcludeFromReports__c = toBeDeleted.PRMExcludeFromReports__c;
        Survivor.PRMCorporateHeadquarters__c = toBeDeleted.PRMCorporateHeadquarters__c;
        Survivor.PRMIncludeCompanyInfoInSearchResults__c = toBeDeleted.PRMIncludeCompanyInfoInSearchResults__c;
        Survivor.PRMIsActiveAccount__c = toBeDeleted.PRMIsActiveAccount__c;
        Survivor.PRMMarketServed__c = toBeDeleted.PRMMarketServed__c;
        Survivor.PRMOrigin__c = toBeDeleted.PRMOrigin__c;
        Survivor.PRMPreferredDistributor1__c = toBeDeleted.PRMPreferredDistributor1__c;
        Survivor.PRMPreferredDistributor2__c = toBeDeleted.PRMPreferredDistributor2__c;
        Survivor.PRMPreferredDistributor3__c = toBeDeleted.PRMPreferredDistributor3__c;
        Survivor.PRMPreferredDistributor4__c = toBeDeleted.PRMPreferredDistributor4__c;
        Survivor.PRMAccount__c = toBeDeleted.PRMAccount__c;
        Survivor.PRMReasonForDecline__c = toBeDeleted.PRMReasonForDecline__c;  
        //Survivor.PRMSEAccountNumber__c = toBeDeleted.PRMSEAccountNumber__c;
        Survivor.PRMSEAccountNumber2__c = toBeDeletedPRMSEAccountNumber2__c;
        Survivor.PRMStateProvince__c = toBeDeleted.PRMStateProvince__c;
        Survivor.PRMTermsAndConditionsAccepted__c = toBeDeleted.PRMTermsAndConditionsAccepted__c;
        Survivor.PRMTaxId__c = toBeDeleted.PRMTaxId__c;
        Survivor.PRMAnnualSales__c = toBeDeleted.PRMAnnualSales__c;
        Survivor.PRMZipCode__c = toBeDeleted.PRMZipCode__c;
        Survivor.PRMUIMSID__c = toBeDeleted.PRMUIMSID__c;
        Survivor.PRMAdditionalComments__c  = toBeDeleted.PRMAdditionalComments__c;
        Survivor.PRMOriginSource__c = toBeDeleted.PRMOriginSource__c;
        Survivor.PRMOriginSourceInfo__c = toBeDeleted.PRMOriginSourceInfo__c;
        Survivor.PRMAccType__c = toBeDeleted.PRMAccType__c; 
        Survivor.PRMRegistrationActivationDate__c = toBeDeleted.PRMRegistrationActivationDate__c;
        Survivor.PRMRegistrationReviewedRejectedDate__c = toBeDeleted.PRMRegistrationReviewedRejectedDate__c;
        Survivor.F_PRM_ProgramTeam__c = toBeDeleted.F_PRM_ProgramTeam__c;
        //Survivor.IsPartner = true;
        
        return Survivor; */   
    
    
        Sobject s1= AP_PRMUtils.CopyMergeField(AP_PRMAppConstants.PRMTechnicalMerge.Account,toBeDeleted,Survivor);
        System.debug('Sobject123 :'+s1);
        Account a1 = new Account(); 
        if(s1!=null)          
        a1= (Account)s1;       
        System.debug('Account123 :'+a1);        
        
        return a1;   
    }

    public static String fromObjectToStringResizedForDisplay(String value){
        return fromObjectToStringResizedForDisplay(value,100,150,' ... ');
    }


    private static String fromObjectToStringResizedForDisplay(String value,Integer beginSize,Integer endSize,String cutMessage){
        if (value!=null){
            Integer maxSizeAllowrd = beginSize + endSize + cutMessage.length();
            Integer length = value.length();
            if (length <= maxSizeAllowrd){
                return value;
            }
            else {
                String myResult = value.substring(0,beginSize);
                myResult+= cutMessage;
                myResult+= value.substring(length-endSize);
                return myResult;
            }
        }
        else {
            return value;
        }
    }
}