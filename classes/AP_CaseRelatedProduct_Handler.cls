public class AP_CaseRelatedProduct_Handler{
    public static void UpdateCaseRelatedProduct(List<Case> lstCaseForInsert , Set <String> setGMRCodeForDelete){
    //public static void UpdateCaseRelatedProduct(Map<String, Case> mapGMRCodeCaseForUpdate , List<Case> lstCaseForInsert , Set <String> setGMRCodeForDelete){
        String strErrorId ;
        String strErrorMessage;
        List<CSE_RelatedProduct__c> lstCaseRelatedProductsForUpdate = new List<CSE_RelatedProduct__c>(); 
        List<CSE_RelatedProduct__c> lstCaseRelatedProductsForInsert = new List<CSE_RelatedProduct__c>(); 
        List<CSE_RelatedProduct__c> lstCaseRelatedProductsForDelete = new List<CSE_RelatedProduct__c>(); 
        Set<Id> caseSetId = new Set<Id>();
        
        if(setGMRCodeForDelete.size()>0){
            lstCaseRelatedProductsForDelete = new List<CSE_RelatedProduct__c>([Select Id,MasterProduct__c from CSE_RelatedProduct__c where TECH_UniqueId__c IN:setGMRCodeForDelete]); 
            if(lstCaseRelatedProductsForDelete.size()>0){
                Database.delete(lstCaseRelatedProductsForDelete,false);
            }
        }
        
        if(lstCaseForInsert.size()>0){
            for(Case objCase: lstCaseForInsert){
                CSE_RelatedProduct__c objCaseRelatedProductForInsert = new CSE_RelatedProduct__c();
                objCaseRelatedProductForInsert.Case__c                   =  objCase.Id;
                objCaseRelatedProductForInsert.BusinessUnit__c           =  objCase.ProductBU__c;
                objCaseRelatedProductForInsert.ProductLine__c            =  objCase.ProductLine__c;
                objCaseRelatedProductForInsert.ProductFamily__c          =  objCase.ProductFamily__c;
                objCaseRelatedProductForInsert.Family__c                 =  objCase.Family__c;
                objCaseRelatedProductForInsert.Family_lk__c              =  objCase.Family_lk__c;
                objCaseRelatedProductForInsert.SubFamily__c              =  objCase.SubFamily__c;
                objCaseRelatedProductForInsert.ProductSuccession__c      =  objCase.ProductSuccession__c;
                objCaseRelatedProductForInsert.ProductGroup__c           =  objCase.ProductGroup__c;
                objCaseRelatedProductForInsert.ProductDescription__c     =  objCase.ProductDescription__c;
                objCaseRelatedProductForInsert.CommercialReference__c    =  objCase.CommercialReference__c;
                objCaseRelatedProductForInsert.CommercialReference_lk__c =  objCase.CommercialReference_lk__c;
                objCaseRelatedProductForInsert.TECH_GMRCode__c           =  objCase.TECH_GMRCode__c;
                if(objCase.CommercialReference_lk__c!=null)
                    objCaseRelatedProductForInsert.TECH_UniqueId__c          =  objCase.Id + '-' + objCase.CommercialReference_lk__c;
                else if(objCase.Family_lk__c!=null)
                    objCaseRelatedProductForInsert.TECH_UniqueId__c          =  objCase.Id + '-' + objCase.Family_lk__c;
                objCaseRelatedProductForInsert.MasterProduct__c          =  true;
                if(objCase.CommercialReference__c != null)
                    objCaseRelatedProductForInsert.Name                  =  objCase.CommercialReference__c;
                else if(objCase.TECH_GMRCode__c != null && objCase.TECH_GMRCode__c.length() ==8 )
                    objCaseRelatedProductForInsert.Name                  =  objCase.Family__c;
                else if(objCase.TECH_GMRCode__c != null && objCase.TECH_GMRCode__c.length() ==6 )
                    objCaseRelatedProductForInsert.Name                  =  objCase.ProductFamily__c;
                else if(objCase.TECH_GMRCode__c != null && objCase.TECH_GMRCode__c.length() ==4 )
                    objCaseRelatedProductForInsert.Name                  =  objCase.ProductLine__c;
                else if(objCase.TECH_GMRCode__c != null && objCase.TECH_GMRCode__c.length() ==2 )
                    objCaseRelatedProductForInsert.Name                  =  objCase.ProductBU__c;
                
                lstCaseRelatedProductsForInsert.add(objCaseRelatedProductForInsert);
            }
        }
        
        if(lstCaseRelatedProductsForInsert.size()>0){
            try{
                Database.UpsertResult[] CaseRelatedProductSaveResult = Database.upsert(lstCaseRelatedProductsForInsert,Schema.CSE_RelatedProduct__c.TECH_UniqueId__c,false);
                //Database.SaveResult[] CaseRelatedProductSaveResult = Database.insert(lstCaseRelatedProductsForInsert,false);
                
                for(Database.UpsertResult objSaveResult: CaseRelatedProductSaveResult){
                //for(Database.SaveResult objSaveResult: CaseRelatedProductSaveResult){
                    if(!objSaveResult.isSuccess()){
                        Database.Error err = objSaveResult.getErrors()[0];
                        strErrorId = objSaveResult.getId() + ':' + '\n';
                        strErrorMessage = err.getStatusCode()+' '+err.getMessage()+ '\n';
                        System.debug(strErrorId + ' ' + strErrorMessage);
                    }
                }
                
            }    
            catch(Exception ex){
                strErrorMessage += ex.getTypeName()+'- '+ ex.getMessage() + '\n';
                System.debug(strErrorMessage);
            }
        }
        
    }
    public static void unCheckOtherMasterProduct(Map<Id, CSE_RelatedProduct__c> MapOldProducts, Map<Id, CSE_RelatedProduct__c> MapNewProducts, boolean IsInsert){
        //*********************************************************************************
        // Method Name   :unCheckOtherMasterProduct
        // Purpose       : When User sets one of Product as Master Product for a Case, and 
        //                If Master  is already set on a different Productfor the Same Case, 
        //                then uncheck all other Master Product on the Case
        // Created by    : Vimal Karunakaran - Global Delivery Team
        // Date created  : 25th August 2014
        // Modified by   :
        // Date Modified :
        // Remarks       : For Oct - 14 (Major) Release
        ///*********************************************************************************/
        
        Set<Id> caseIDSet = new Set<Id>();
        Set<Id> ProductIDSet = new Set<Id>();
        Map<Id,Id> mapRelatedProductCase = new Map<Id,Id>();
        
        for(CSE_RelatedProduct__c objCaseRelatedProduct: MapNewProducts.values()){
            if((IsInsert && objCaseRelatedProduct.MasterProduct__c) || (MapOldProducts !=null && ((MapOldProducts.get(objCaseRelatedProduct.id).MasterProduct__c!= MapNewProducts.get(objCaseRelatedProduct.id).MasterProduct__c) && MapNewProducts.get(objCaseRelatedProduct.id).MasterProduct__c))){
                caseIDSet.add(objCaseRelatedProduct.Case__c);
                ProductIDSet.add(objCaseRelatedProduct.ID);
                mapRelatedProductCase.put(objCaseRelatedProduct.Id, objCaseRelatedProduct.Case__c);
            }
        }
        
        if(caseIDSet.size()>0){
            List<CSE_RelatedProduct__c> lstExistingProducts = new List<CSE_RelatedProduct__c>([Select Id from CSE_RelatedProduct__c where MasterProduct__c =:true AND  Case__c =: caseIDSet AND ID !=:ProductIDSet]);
            if(lstExistingProducts.size()>0){
                for(CSE_RelatedProduct__c objExistingProduct: lstExistingProducts){
                    objExistingProduct.MasterProduct__c=false;   
                }
                Database.SaveResult[] ExistingMasterProductSaveResult = Database.update(lstExistingProducts,false);
            }
        }
    }   
}