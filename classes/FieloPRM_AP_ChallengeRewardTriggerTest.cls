@isTest
public with sharing class FieloPRM_AP_ChallengeRewardTriggerTest {
    
         static testMethod void unitTest(){


            User us = new user(id = userinfo.getuserId());
              us.BypassVR__c = true;
              update us;
              
              System.RunAs(us){

                        FieloEE.MockUpFactory.setCustomProperties(false);

                        
                        Account acc = new Account();
                        acc.Name = 'test acc';
                        insert acc;
                        
                        Account accTo= new Account();
                        accTo.Name = 'test acc to';
                        insert accTo;

                        FieloEE__Member__c member = new FieloEE__Member__c();
                        member.FieloEE__LastName__c= 'Polo';
                        member.FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime());
                        member.FieloEE__Street__c = 'test';
                        member.F_Account__c = acc.id;
                             
                        insert member;   

                        FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
                        

                        
                        list<FieloEE__Badge__c> listbadge = new list<FieloEE__Badge__c>();
                        FieloEE__Badge__c badge = new FieloEE__Badge__c();
                                
                        badge.name = 'Badge';
                        badge.F_PRM_Type__c = 'Program Level';
                        badge.F_PRM_Segment__c =  segment.id;
                        badge.F_PRM_BadgeAPIName__c = 'apinametest';
                        listbadge.add(badge) ;

                        FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
                                
                        badge2.name = 'Badge';
                        badge2.F_PRM_Type__c = 'BFOProperties';
                        badge2.F_PRM_Segment__c =  segment.id;
                        badge2.F_PRM_BadgeAPIName__c = 'apinametest2';
                        listbadge.add(badge2) ;

                        insert listbadge;

                        FieloCH__Challenge__c challenge =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');

                        

                        FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
                        FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
                        FieloCH__MissionCriteria__c missionCriteria1 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
                        FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );

                        FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission1.id );     
                        FieloCH__MissionChallenge__c missionChallenge2  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission2.id );     
                        
                        FieloCH__ChallengeReward__c challengeReward = new FieloCH__ChallengeReward__c(FieloCH__Challenge__c = challenge.Id, FieloCH__Badge__c = badge.id);  
                        insert challengeReward;  
                        challengeReward.FieloCH__LogicalExpression__c = '(1)';
                        
                        update challengeReward;

                        challenge =      FieloPRM_UTILS_MockUpFactory.activateChallenge(challenge);

                        test.startTEst();

                              FieloCH__Challenge__c challenge2 =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST2', 'Competition', 'Objective');
                              FieloCH__ChallengeReward__c challengeReward2 = new FieloCH__ChallengeReward__c(FieloCH__Challenge__c = challenge.Id, FieloCH__Badge__c = badge.id);  
                              try{
                                    insert challengeReward2;  
                              }catch(Exception e){

                              }
                              

                        test.stopTEst();
            }
            
        }
}