@isTest
Public class VFC_SRV_MYFSWOReportsController_Test{
    
    static testMethod void queryAccountsAndCountries() {
        
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
                              and developerName = 'Customer'
                              limit 1
                             ][0].Id;
        Country__c country1 = new Country__c(Name='INDIA',CountryCode__c='IN');
        insert country1;
        Account acc1 = new Account(Name = 'TestAccount',Country__c = Country1.Id, Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
        insert acc1;
        Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
        insert cnct;
        
        Profile p = [SELECT Id FROM Profile WHERE Name =: System.label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];
        
       /* user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
                          EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                          ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
                          // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
                          isActive = True, BypassVR__c = True, contactId = cnct.id);
        insert u; */
        //system.runAs(u) {
        
        SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c = true);
        insert location1;
        SVMXC__Site__c location2 = new SVMXC__Site__c(Name = 'TestLocation2', SVMXC__Location_Type__c = 'Room', SVMXC__Account__c = acc1.Id);
        insert location2;
        
        RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
                                                  and developerName = 'LocationContactRole'
                                                  limit 1
                                                 ]; //[0].Id;
        /*Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
        insert ro;*/
        //Creating Work Order
        list<SVMXC__Service_Order__c> Wolist = new list<SVMXC__Service_Order__c>();
        DateTime myDateTime = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
        

VFC_SRV_MYFSWOReportsController controler1= new VFC_SRV_MYFSWOReportsController();
        DateTime myDateTime2 = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
        SVMXC__Service_Order__c newrecpast= new SVMXC__Service_Order__c();
        newrecpast.SVMXC__Company__c=acc1.Id;
        newrecpast.SVMXC__Site__c=location1.Id;
        newrecpast.SVMXC__Contact__c=cnct.id;
        newrecpast.SVMXC__Scheduled_Date_Time__c=myDateTime2;
        newrecpast.SVMXC__Order_Status__c = 'Service Complete';
        insert  newrecpast;
       
         Attachment attach= new Attachment();
attach.Name = 'testbFS___Service_Intervention_Report.pdf' ;
attach.parentId = newrecpast.Id;
attach.Description ='Work Order Output Doc';
Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
attach.body=bodyBlob;
insert attach;
 Attachment attach2= new Attachment();
attach2.Name = 'testbFS___Service_Intervention_Report2.pdf' ;
attach2.parentId = newrecpast.Id;
attach2.Description ='Work Order Output Doc';
Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body2');
attach2.body=bodyBlob2;
insert attach2;
System.assertEquals(attach.Description,'Work Order Output Doc'); 
Id woId = newrecpast.id;
String attachQuery = 'SELECT Id, Name,CreatedDate,ParentId, LastModifiedDate FROM Attachment where Name like \'%' + system.label.CLSRVMYFS_PDFEXTENSION + '%\' and (Name like  \'%' + system.label.CLSRVMYFS_SRVINTERVENTION + '%\' or Description =\'' + system.label.CLSRVMYFS_WODescription + '\') and parentId =:woId';
list<Attachment> attachmentList = database.query(attachQuery); 
String woQuery ='select id,name,createddate,SVMXC__Scheduled_Date_Time__c,TECH_CustomerScheduledDateTimeECH__c,SVMXC__Order_Status__c,SVMXC__Site__r.name,SVMXC__Scheduled_Date__c,SVMXC__Problem_Description__c,SubStatus__c,SVMXC__Order_Type__c,Technical_Approval__c,RecommendationsToClient__c,CustomerTimeZone__c,(SELECT Id, Name,CreatedDate,ParentId, LastModifiedDate FROM Attachments) from SVMXC__Service_Order__c where Id =:woId';
System.assertEquals(attachmentList.size(),2);
list<SVMXC__Service_Order__c> wotestList = database.query(woQuery); 
 //controler1.processWOInterventionReports(new list<SVMXC__Service_Order__c>{newrecpast});
  controler1.processWOInterventionReports(wotestList);
        //VFC_SRV_MYFsPersonalInformatinController Test Coverage
        System.debug('raghav'+ newrecpast);
        // System.debug('raghav2'+ attach);
        Test.setCurrentPageReference(new PageReference('Page.VFP_SRV_MYFSDocumentsPage'));
        
        System.currentPageReference().getParameters().put('Params', acc1.Id);
        System.currentPageReference().getParameters().put('country', Country1.CountryCode__c);
        test.starttest();
        VFC_SRV_MYFSWOReportsController SiteControler= new VFC_SRV_MYFSWOReportsController();
        SiteControler.size = 1;
        SiteControler.site='View All';
        SiteControler.WOStatus = 'Completed';
        SiteControler.fromdate = '2016-01-01';
        SiteControler.todate = '';
        
        SiteControler.UltimateParentAccount = acc1.Id;
        
        SiteControler.getInterventionReports();
        
        VFC_SRV_MYFSWOReportsController SiteControler2= new VFC_SRV_MYFSWOReportsController();
        SiteControler2.size = 1;
        SiteControler2.site=location1.id ;
        SiteControler2.WOStatus = 'Completed';
        SiteControler2.fromdate = '2016-01-01';
        SiteControler2.todate = '';
        
        Map<String, String> selectoptionMap = new Map<String,String>();
        selectoptionMap.put(location1.id,location1.Name);
        selectoptionMap.put(location2.Id,location2.Name);
        // SiteControler2.selectoptionMap = selectoptionMap;
        SiteControler2.UltimateParentAccount = acc1.Id;
        //  SiteControler2.SelectedCountry = acc1.country__c;
        SiteControler2.getInterventionReports();
     
        // SiteControler2.pastInterventions();
        Test.stoptest();
        // }
    }
    static testMethod void FromDateTodateLocation() {
        test.starttest();
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
                              and developerName = 'Customer'
                              limit 1
                             ][0].Id;
          Country__c country1 = new Country__c(Name='INDIA',CountryCode__c='IN');
        insert country1;                     
        
        Account acc1 = new Account(Name = 'TestAccount', Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
        insert acc1;
        Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
        insert cnct;
        Profile p = [SELECT Id FROM Profile WHERE Name = : System.label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];
        
       /* user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
                          EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                          ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
                          // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
                          isActive = True, BypassVR__c = True, contactId = cnct.id);
        insert u;*/
        //system.runAs(u) {
        
        SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id);
        insert location1;
        
        RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
                                                  and developerName = 'LocationContactRole'
                                                  limit 1
                                                 ]; //[0].Id;
       /* Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
        insert ro;*/
        list<SVMXC__Service_Order__c> Wolist = new list<SVMXC__Service_Order__c>();
        DateTime myDateTime = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
        for(integer i=1;i<10;i++){
            SVMXC__Service_Order__c newrec= new SVMXC__Service_Order__c();
            newrec.SVMXC__Company__c=acc1.Id;
            newrec.SVMXC__Site__c=location1.Id;
            newrec.SVMXC__Contact__c=cnct.id;
            newrec.SVMXC__Scheduled_Date_Time__c=myDateTime;
            newrec.SVMXC__Order_Status__c = 'Service Complete';
            Wolist.add(newrec);
        }
        insert wolist;
        
        
        Attachment attach= new Attachment();
        attach.Name = 'testbFS___Service_Intervention_Report.pdf' ;
        attach.parentId = wolist[0].Id;
        attach.Description ='Work Order Output Doc';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        insert attach; 
           System.currentPageReference().getParameters().put('Params', acc1.Id);
        System.currentPageReference().getParameters().put('country', Country1.CountryCode__c);
        
        VFC_SRV_MYFSWOReportsController reportControler= new VFC_SRV_MYFSWOReportsController();
        reportControler.size = 1;  
        reportControler.site='View All';
        reportControler.fromdate = '2016-01-03';
        reportControler.todate = '2016-07-03';
        reportControler.fromDateSelector();
        
        VFC_SRV_MYFSWOReportsController reportControler2= new VFC_SRV_MYFSWOReportsController();
        reportControler2.size = 1;  
        reportControler2.site=location1.id;
        reportControler2.fromdate = '2016-01-03';
        reportControler2.todate = '2016-07-03';
        reportControler2.fromDateSelector();
        
        VFC_SRV_MYFSWOReportsController reportControler3= new VFC_SRV_MYFSWOReportsController();
        reportControler3.size = 1;  
        reportControler3.site='View All';
        reportControler3.fromdate = '2016-01-03';
        reportControler3.todate = '';
        reportControler3.fromDateSelector();
        
        VFC_SRV_MYFSWOReportsController reportControler4= new VFC_SRV_MYFSWOReportsController();
        reportControler4.size = 1;  
        reportControler4.site=location1.id;
        reportControler4.fromdate = '2016-01-03';
        reportControler4.todate = '';
        reportControler4.fromDateSelector();
        
        VFC_SRV_MYFSWOReportsController reportControler5= new VFC_SRV_MYFSWOReportsController();
        reportControler5.size = 1;  
        reportControler5.site='View All';
        reportControler5.fromdate = '2016-01-03';
        reportControler5.todate = '2016-07-03';
        reportControler5.toDateSelector();
        Test.stoptest();
        VFC_SRV_MYFSWOReportsController reportControler6= new VFC_SRV_MYFSWOReportsController();
        reportControler6.size = 1;  
        reportControler6.site=location1.id;
        reportControler6.fromdate = '2016-01-03';
        reportControler6.todate = '2016-07-03';
        reportControler6.toDateSelector();
        
        VFC_SRV_MYFSWOReportsController reportControler7= new VFC_SRV_MYFSWOReportsController();
        reportControler7.size = 1;  
        reportControler7.site='View All';
        reportControler7.fromdate = '';
        reportControler7.todate = '2016-07-03';
        reportControler7.toDateSelector();
        
        VFC_SRV_MYFSWOReportsController reportControler8= new VFC_SRV_MYFSWOReportsController();
        reportControler8.size = 1;  
        reportControler8.site=location1.id;
        reportControler8.fromdate = '';
        reportControler8.todate = '2016-07-03';
        reportControler8.toDateSelector();
        //SiteControler.ToDateSelector();
        
      //   }
    } 
}