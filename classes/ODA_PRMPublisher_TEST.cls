@isTest
private class ODA_PRMPublisher_TEST{
    static testmethod void TEST_ODA_TEST(){
        ODA_PRMPublisher PrmPublish= new ODA_PRMPublisher();
        PrmPublish.refreshGetters();
        PrmPublish.publishAll();
        PrmPublish.unpublishAll();
        ODA_PRMPublisher.wrapSObject wrapSobj = new ODA_PRMPublisher.wrapSObject(null);
    
    }
    static testmethod void TEST_ODA_TEST2(){
        ODA_PRMPublisher PrmPublish= new ODA_PRMPublisher();
        PrmPublish.publishFieloEE_TagItem();
        PrmPublish.unpublishFieloEE_Tag();
        PrmPublish.publishFieloEE_Tag();
        PrmPublish.unpublishFieloEE_SegmentDomain();
        PrmPublish.publishFieloEE_SegmentDomain();
        PrmPublish.unpublishFieloEE_Section();
        PrmPublish.publishFieloEE_Section();
        PrmPublish.unpublishFieloEE_RedemptionRuleCriteria();
        
    }
    static testmethod void TEST_ODA_TEST3(){
        ODA_PRMPublisher PrmPublish= new ODA_PRMPublisher();
        PrmPublish.publishFieloEE_News();
        PrmPublish.unpublishFieloEE_Banner();
        PrmPublish.publishFieloEE_Banner();
        PrmPublish.unpublishFieloEE_RedemptionRule();
        PrmPublish.publishFieloEE_RedemptionRule();
        PrmPublish.unpublishFieloEE_Component();
        PrmPublish.publishFieloEE_Component();
        PrmPublish.publishFieloEE_RedemptionRuleCriteria();
        PrmPublish.unpublishFieloEE_News();
    }
}