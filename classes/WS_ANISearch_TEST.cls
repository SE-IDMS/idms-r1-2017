/*
Author          : Rakhi Kumar - Global Delivery Team
Date Created    : 26/02/2013
Description     : Test class for Web Service for ANI# search on contacts, account and ANI objects.
                  CTI Integration module for  May 2013 Release
-----------------------------------------------------------------
  
*/
@isTest
private class WS_ANISearch_TEST{
     static testMethod void testWS_ANISearchMatchContacts()
    {
        String salesforceURL = URL.getSalesforceBaseUrl().toExternalForm();
        list<Account> lstAccounts = new list<Account>();
        list<Contact> lstContacts = new list<Contact>();
        list<ANI__c> lstANIs = new list<ANI__c>();
        //Create Country
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        //Create StateProvince
        StateProvince__c stateProvince = Utils_TestMethods.createStateProvince(country.id);
        insert stateProvince;
        
        //Create Preferred Agent
        User agent1 = Utils_TestMethods.createStandardUser('Agent1');
        insert agent1;
        
        //Create Preferred Backup Agent
        User agent2 = Utils_TestMethods.createStandardUser('Agent2');
        insert agent2;
        
        Account accObj1 = Utils_TestMethods.createAccount();
        accObj1.Name = 'DAC Account';
        accObj1.Phone = '99999991';
        accObj1.Country__c = country.id;
        accObj1.StateProvince__c = stateProvince.id;
        accObj1.LeadingBusiness__c = 'Buildings';
        accObj1.ClassLevel1__c = 'Retailer';
        accObj1.Type = 'Prospect';
        accObj1.ServiceContractType__c = 'DAC';
        accObj1.City__c = 'bangalore';
        accObj1.ZipCode__c = '560066';
        accObj1.Street__c = 'EPIP';
        accObj1.POBox__c = '12345';
        accObj1.Pagent__c = agent1.Id;
        accObj1.PBAAgent__c = agent2.Id;
        lstAccounts.add(accObj1); 
        
        Account accObj2 = Utils_TestMethods.createAccount();
        accObj2.Name = ' Standard Account';
        accObj2.Phone = '3300000002';
        accObj2.Country__c = country.id;
        accObj2.StateProvince__c = stateProvince.id;
        accObj2.LeadingBusiness__c = 'Buildings';
        accObj2.ClassLevel1__c = 'Retailer';
        accObj2.Type = 'Prospect';
        accObj2.ServiceContractType__c = '';
        accObj2.City__c = 'bangalore';
        accObj2.ZipCode__c = '560066';
        accObj2.Street__c = 'EPIP';
        accObj2.POBox__c = '12345';
        accObj2.Pagent__c = agent1.Id;
        accObj2.PBAAgent__c = agent2.Id;
        lstAccounts.add(accObj2); 
        
        Account accObj3 = Utils_TestMethods.createAccount();
        accObj3.Name = 'VIC Account';
        accObj3.Phone = '3300000002';
        accObj3.Country__c = country.id;
        accObj3.StateProvince__c = stateProvince.id;
        accObj3.LeadingBusiness__c = 'Buildings';
        accObj3.ClassLevel1__c = 'Retailer';
        accObj3.Type = 'Prospect';
        accObj3.ServiceContractType__c = 'VIC';
        accObj3.City__c = 'bangalore';
        accObj3.ZipCode__c = '560066';
        accObj3.Street__c = 'EPIP Zone';
        accObj3.POBox__c = '123456';
        accObj3.Pagent__c = agent1.Id;
        accObj3.PBAAgent__c = agent2.Id;
        lstAccounts.add(accObj3); 
        
        Account accObj4 = Utils_TestMethods.createAccount();
        accObj4.Name = 'Std Account';
        accObj4.Phone = '99999994';
        accObj4.Country__c = country.id;
        accObj4.StateProvince__c = stateProvince.id;
        accObj4.LeadingBusiness__c = 'Buildings';
        accObj4.ClassLevel1__c = 'Retailer';
        accObj4.Type = 'Prospect';
        accObj4.ServiceContractType__c ='VIC';
        accObj4.City__c = 'bangalore';
        accObj4.ZipCode__c = '560066';
        accObj4.Street__c = 'EPIP Zone';
        accObj4.POBox__c = '123456';
        accObj4.Pagent__c = agent1.Id;
        accObj4.PBAAgent__c = agent2.Id;
        lstAccounts.add(accObj4); 
       
        insert lstAccounts;
        
        Contact conObj1 = Utils_TestMethods.createContact(accObj2.id, ' Contact 1' );
        conObj1.firstname = '';
        conObj1.WorkPhone__c = '3300000000';
        conObj1.accountid = accObj1.id;
        conObj1.ServiceContractType__c = '';
        conObj1.Pagent__c = agent1.Id;
        conObj1.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj1);
        
        Contact conObj2 = Utils_TestMethods.createContact(accObj2.id, ' Contact 2' );
        conObj2.WorkPhone__c = '3300000001';
        conObj2.accountid = accObj2.id;
        conObj2.ServiceContractType__c = 'VIC';
        conObj2.Pagent__c = agent1.Id;
        conObj2.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj2);
        
        Contact conObj3 = Utils_TestMethods.createContact(accObj2.id, 'Contact3' );
        conObj3.WorkPhone__c = '3300000002';
        conObj3.accountid = accObj2.id;
        conObj3.ServiceContractType__c = 'Contract Support';
        conObj3.Pagent__c = agent1.Id;
        conObj3.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj3);
        
        Contact conObj4 = Utils_TestMethods.createContact(accObj2.id, ' Contact4' );
        conObj4.WorkPhone__c = '3300000002';
        conObj4.accountid = accObj2.id;
        conObj4.ServiceContractType__c = 'VIC';
        conObj4.Pagent__c = agent1.Id;
        conObj4.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj4);
        
        Contact conObj5 = Utils_TestMethods.createContact(accObj4.id, ' Contact5' );
        conObj5.WorkPhone__c = '3300000003';
        conObj5.accountid = accObj4.id;
        conObj5.ServiceContractType__c = 'VIC';
        conObj5.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj5);
        
        Contact conObj8 = Utils_TestMethods.createContact(accObj2.id, ' Contact8' );
        conObj8.WorkPhone__c = '3300000005';
        conObj8.accountid = accObj2.id;
        conObj8.ServiceContractType__c = 'Contract Support';
        conObj8.Pagent__c = agent1.Id;
        conObj8.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj8);
        
        Contact conObj80 = Utils_TestMethods.createContact(accObj2.id, ' Contact80' );
        conObj80.WorkPhone__c = '33000000056';
        conObj80.accountid = accObj2.id;
        conObj80.ServiceContractType__c = 'Contract Support';
        conObj80.Pagent__c = agent1.Id;
        conObj80.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj80);
        
        Contact conObj9 = Utils_TestMethods.createContact(accObj2.id, ' Contact9' );
        conObj9.WorkPhone__c = '33000000055';
        conObj9.accountid = accObj2.id;
        conObj9.ServiceContractType__c = 'Contract Support';
        conObj9.Pagent__c = agent1.Id;
        conObj9.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj9);
        
        Contact conObj13 = Utils_TestMethods.createContact(accObj2.id, ' Contact13' );
        conObj13.WorkPhone__c = '3300000009';
        conObj13.accountid = accObj2.id;
        conObj13.ServiceContractType__c = 'Standard';
        conObj13.Pagent__c = agent1.Id;
        conObj13.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj13);
        
        Contact conObj14 = Utils_TestMethods.createContact(accObj2.id, ' Contact14' );
        conObj14.WorkPhone__c = '33000000099';
        conObj14.accountid = accObj2.id;
        conObj14.ServiceContractType__c = 'Standard';
        conObj14.Pagent__c = agent1.Id;
        conObj14.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj14);
        
        Contact conObj15 = Utils_TestMethods.createContact(accObj4.id, ' Contact15' );
        conObj15.WorkPhone__c = '3300000010';
        conObj15.accountid = accObj4.id;
        conObj15.ServiceContractType__c = 'DAC';
        conObj15.Pagent__c = agent1.Id;
        conObj15.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj15);
        
        insert lstContacts;
        
        //Create open case for  Contact 1
        Case cseObj = Utils_TestMethods.createCase(accObj2.Id, conObj9.Id, 'Open');
        insert cseObj;
        
        Case cseObj1 = Utils_TestMethods.createCase(accObj2.Id, conObj14.Id, 'Open');
        insert cseObj1;
      
        Contract testContract = Utils_TestMethods.createContract(accObj1.Id, conObj8.Id);
        insert testContract ;
         
        testContract.webaccess__c = 'Eclipse';
        testContract.Status = 'Activated';    
        testContract.startdate = system.today();
        testContract.ContractTerm = 3;
        update testContract;    
        System.debug('testContract'+testContract);
        
        Contract testContractinactive = Utils_TestMethods.createContract(accObj2.Id, conObj80.Id);
        insert testContractinactive ;
         
        testContractinactive.webaccess__c = 'Eclipse';
        testContractinactive.Status = 'Expired';    
        testContractinactive.startdate = system.today();
        testContractinactive.ContractTerm = 2;
        update testContractinactive;    
        System.debug('testContractinactive'+testContractinactive);
        
        CTR_ValueChainPlayers__c  testCVCP =Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        insert testCVCP;
        CTR_ValueChainPlayers__c  objCVCP =Utils_TestMethods.createCntrctValuChnPlyr(testContractinactive.Id);
        insert objCVCP;
        
        //Create ANI object related to DAC Contact(Contact15) - No. 3300000010
        Ani__c aniObj1 = new Ani__c();
        aniObj1.ANINumber__c = '3300000010';
        aniObj1.Contact__c = conObj15.id;
        lstANIs.add(aniObj1);
        
        //Create ANI object related to  Contact(Contact2) - No. 99999993
        Ani__c aniObj2 = new Ani__c();
        aniObj2.ANINumber__c = '99999993';
        aniObj2.Contact__c = conObj5.id;
        lstANIs.add(aniObj2);
        insert lstANIs;
      
        
        WS_ANISearch.Customer customer = new WS_ANISearch.Customer();
        Id[] fixedSearchResults = null; 
        
        test.startTest();    
        
        //SINGLE  CONTACT MATCH
        fixedSearchResults = new Id[1];
        fixedSearchResults[0] = conObj1.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        customer = WS_ANISearch.getANICustomerInfos('3300000000');
        
        //SINGLE  CONTACT MATCH
        fixedSearchResults = new Id[1];
        fixedSearchResults[0] = conObj2.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        customer = WS_ANISearch.getANICustomerInfos('3300000001');
        
        
        //SINGLE DAC CONTACT MATCH
        fixedSearchResults = new Id[1];
        fixedSearchResults[0] = conObj8.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        customer = WS_ANISearch.getANICustomerInfos('3300000005');       
        
        //SINGLE DAC CONTACT MATCH
        fixedSearchResults = new Id[1];
        fixedSearchResults[0] = conObj9.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        customer = WS_ANISearch.getANICustomerInfos('33000000055');       
        
        //SINGLE DAC CONTACT MATCH
        fixedSearchResults = new Id[1];
        fixedSearchResults[0] = conObj80.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        customer = WS_ANISearch.getANICustomerInfos('33000000056');       
        
        //SINGLE DAC CONTACT MATCH
        fixedSearchResults = new Id[1];
        fixedSearchResults[0] = conObj13.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        customer = WS_ANISearch.getANICustomerInfos('3300000009');       
        
        fixedSearchResults = new Id[2];
        fixedSearchResults[1] = aniObj1.Id;
        fixedSearchResults[0] = conObj15.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        customer = WS_ANISearch.getANICustomerInfos('3300000010');       
        
        //MULTIPLE  CONTACT MATCH
        fixedSearchResults = new Id[2];
        fixedSearchResults[0] = conObj4.Id;
        fixedSearchResults[1] = conObj3.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        customer = WS_ANISearch.getANICustomerInfos('3300000002');
          
        test.stoptest(); 

    }
    static testMethod void testWS_ANISearchMultiMatch(){
    String salesforceURL = URL.getSalesforceBaseUrl().toExternalForm();
        list<Account> lstAccounts = new list<Account>();
        list<Contact> lstContacts = new list<Contact>();
        list<ANI__c> lstANIs = new list<ANI__c>();
            //Create Country
            Country__c country = Utils_TestMethods.createCountry();
            insert country;
            
            //Create StateProvince
            StateProvince__c stateProvince = Utils_TestMethods.createStateProvince(country.id);
            insert stateProvince;

            //Create Preferred Agent
            User agent1 = Utils_TestMethods.createStandardUser('Agent1');
            insert agent1;

            //Create Preferred Backup Agent
            User agent2 = Utils_TestMethods.createStandardUser('Agent2');
            insert agent2;
            
            CustomerCareTeam__c PrimaryCareTeam = new CustomerCareTeam__c (Name='PrimaryCareTeam', CCCountry__c = Country.Id, LevelOfSupport__c = 'Primary', Email__c = 'PrimaryEmailAddress');
            insert PrimaryCareTeam;
            
            //Create DAC Account
            Account accObj1 = Utils_TestMethods.createAccount();
            accObj1.Name = 'DAC Account';
            accObj1.Phone = '+3300000014';
            accObj1.Country__c = country.id;
            accObj1.StateProvince__c = stateProvince.id;
            accObj1.LeadingBusiness__c = 'Buildings';
            accObj1.ClassLevel1__c = 'Retailer';
            accObj1.Type = 'Prospect';
            accObj1.ServiceContractType__c = 'Standard';
            accObj1.CallFlowOption__c='Custom Profile';
            accObj1.IVRMenu__c='IVR_Panelbuilders';
            accObj1.City__c = 'bangalore';
            accObj1.ZipCode__c = '560066';
            accObj1.Street__c = 'EPIP';
            accObj1.POBox__c = '12345';
            accObj1.Pagent__c = agent1.Id;
            accObj1.PBAAgent__c = agent2.Id;
            lstAccounts.add(accObj1); 
            
            Account accObj2 = Utils_TestMethods.createAccount();
            accObj2.Name = 'VIC2 Account';
            accObj2.Phone = '3300000014';
            accObj2.Country__c = country.id;
            accObj2.StateProvince__c = stateProvince.id;
            accObj2.LeadingBusiness__c = 'Buildings';
            accObj2.ClassLevel1__c = 'Retailer';
            accObj2.Type = 'Prospect';
            accObj2.ServiceContractType__c = 'Standard';
            accObj2.CallFlowOption__c='Custom Profile';
            accObj1.IVRMenu__c='IVR_Panelbuilders';
            accObj2.City__c = 'bangalore';
            accObj2.ZipCode__c = '560066';
            accObj2.Street__c = 'EPIP Zone';
            accObj2.POBox__c = '123456';
            accObj2.Pagent__c = agent1.Id;
            accObj2.PBAAgent__c = agent2.Id;
            System.debug('####### - VIC Multimatch Account 1 - ' + accObj2 + '#######');
            lstAccounts.add(accObj2);
            
            Account accObj3 = Utils_TestMethods.createAccount();
            accObj3.Name = 'VIC Account';
            accObj3.Phone = '3300000089';
            accObj3.Country__c = country.id;
            accObj3.StateProvince__c = stateProvince.id;
            accObj3.LeadingBusiness__c = 'Buildings';
            accObj3.ClassLevel1__c = 'Retailer';
            accObj3.Type = 'Prospect';
            accObj3.ServiceContractType__c = 'Standard';
            accObj3.CallFlowOption__c='Fast Track';
            accObj3.Preferred_CC_Team__c=PrimaryCareTeam.id;
            accObj3.City__c = 'bangalore';
            accObj3.ZipCode__c = '560066';
            accObj3.Street__c = 'EPIP Zone';
            accObj3.POBox__c = '123456';
            accObj3.Pagent__c = agent1.Id;
            accObj3.PBAAgent__c = agent2.Id;
            System.debug('####### - DAC Account 3 - ' + accObj3 + '#######');
            lstAccounts.add(accObj3);
            //Create Standard Account
            Account accObj4 = Utils_TestMethods.createAccount();
            accObj4.Name = 'Std Account';
            accObj4.Phone = '3300000089';
            accObj4.Country__c = country.id;
            accObj4.StateProvince__c = stateProvince.id;
            accObj4.LeadingBusiness__c = 'Buildings';
            accObj4.ClassLevel1__c = 'Retailer';
            accObj4.Type = 'Prospect';
            accObj4.ServiceContractType__c = 'Standard';
            accObj4.CallFlowOption__c='Fast Track';
            accObj4.Preferred_CC_Team__c=PrimaryCareTeam.id;            
            accObj4.City__c = 'bangalore';
            accObj4.ZipCode__c = '560066';
            accObj4.Street__c = 'EPIP Zone';
            accObj4.POBox__c = '123456';
            accObj4.Pagent__c = agent1.Id;
            accObj4.PBAAgent__c = agent2.Id;
            System.debug('####### - DAC Account 3 - ' + accObj4 + '#######');
            lstAccounts.add(accObj4); 
            
            
            insert lstAccounts;
                //Create VIC Contact 13 - No. +3300000022
            Contact conObj13 = Utils_TestMethods.createContact(accObj1.id, ' Contact13' );
            conObj13.WorkPhone__c = '+3300000022';
            conObj13.accountid = accObj1.id;
            conObj13.ServiceContractType__c = 'Standard';
            conObj13.CallFlowOption__c='Custom Profile';
            conObj13.IVRMenu__c='IVR_Panelbuilders';
            conObj13.Pagent__c = agent1.Id;
            conObj13.PBAAgent__c = agent2.Id;
            lstContacts.add(conObj13);
            System.debug('####### - VIC Contact 13-' + conObj13 + '#######');
            
            //Create VIC Contact 14 - No. +3300000022
            Contact conObj14 = Utils_TestMethods.createContact(accObj1.id, ' Contact14' );
            conObj14.WorkPhone__c = '+3300000022';
            conObj14.accountid = accObj1.id;
            conObj14.ServiceContractType__c = 'Standard';
            conObj14.CallFlowOption__c='Custom Profile';
            conObj14.IVRMenu__c='IVR_Panelbuilders';
            conObj14.Pagent__c = agent1.Id;
            conObj14.PBAAgent__c = agent2.Id;
            lstContacts.add(conObj14);
            System.debug('####### - VIC Contact 14-' + conObj14 + '#######');
            
            Contact conObj1 = Utils_TestMethods.createContact(accObj1.id, ' Contact13' );
            conObj1.WorkPhone__c = '+3300000021';
            conObj1.accountid = accObj1.id;
            conObj1.ServiceContractType__c = 'Standard';
            conObj1.CallFlowOption__c='Fast Track';
            conObj1.PreferredCCTeam__c=PrimaryCareTeam.id;
            conObj1.Pagent__c = agent1.Id;
            conObj1.PBAAgent__c = agent2.Id;
            lstContacts.add(conObj1);
            
            //Create VIC Contact 14 - No. +3300000022
            Contact conObj2 = Utils_TestMethods.createContact(accObj1.id, ' Contact2' );
            conObj2.WorkPhone__c = '+3300000021';
            conObj2.accountid = accObj1.id;
            conObj2.ServiceContractType__c = 'Standard';
            conObj2.CallFlowOption__c='Fast Track';
            conObj2.PreferredCCTeam__c=PrimaryCareTeam.id;
            conObj2.Pagent__c = agent1.Id;
            conObj2.PBAAgent__c = agent2.Id;
            lstContacts.add(conObj2);
            
            insert lstContacts;
                WS_ANISearch.Customer customer = new WS_ANISearch.Customer();
                Id[] fixedSearchResults = null;  
            Test.StartTest();
                fixedSearchResults = new Id[2];
                fixedSearchResults[0] = conObj14.Id;
                fixedSearchResults[1] = conObj13.Id;
                Test.setFixedSearchResults(fixedSearchResults);
                customer = WS_ANISearch.getANICustomerInfos('3300000022');
                
                fixedSearchResults = new Id[2];
                fixedSearchResults[0] = accObj1.Id;
                fixedSearchResults[1] = accObj2.Id;
                Test.setFixedSearchResults(fixedSearchResults);
                customer = WS_ANISearch.getANICustomerInfos('3300000014');
                
                fixedSearchResults = new Id[2];
                fixedSearchResults[0] = accObj3.Id;
                fixedSearchResults[1] = accObj4.Id;
                Test.setFixedSearchResults(fixedSearchResults);
                customer = WS_ANISearch.getANICustomerInfos('3300000089');
                
                fixedSearchResults = new Id[2];
                fixedSearchResults[0] = conObj2.Id;
                fixedSearchResults[1] = conObj1.Id;
                Test.setFixedSearchResults(fixedSearchResults);
                customer = WS_ANISearch.getANICustomerInfos('3300000021');
            Test.StopTest(); 
        
        }
        static testMethod void testWS_ANISearchMatchAccounts(){
        String salesforceURL = URL.getSalesforceBaseUrl().toExternalForm();
        list<Account> lstAccounts = new list<Account>();
        list<Contact> lstContacts = new list<Contact>();
        list<ANI__c> lstANIs = new list<ANI__c>();
            //Create Country
            Country__c country = Utils_TestMethods.createCountry();
            insert country;
            
            //Create StateProvince
            StateProvince__c stateProvince = Utils_TestMethods.createStateProvince(country.id);
            insert stateProvince;

            //Create Preferred Agent
            User agent1 = Utils_TestMethods.createStandardUser('Agent1');
            insert agent1;

            //Create Preferred Backup Agent
            User agent2 = Utils_TestMethods.createStandardUser('Agent2');
            insert agent2;
            
            //Create DAC Account
            Account accObj1 = Utils_TestMethods.createAccount();
            accObj1.Name = 'DAC Account';
            accObj1.Phone = '+3300000014';
            accObj1.Country__c = country.id;
            accObj1.StateProvince__c = stateProvince.id;
            accObj1.LeadingBusiness__c = 'Buildings';
            accObj1.ClassLevel1__c = 'Retailer';
            accObj1.Type = 'Prospect';
            accObj1.ServiceContractType__c = 'DAC';
            accObj1.City__c = 'bangalore';
            accObj1.ZipCode__c = '560066';
            accObj1.Street__c = 'EPIP';
            accObj1.POBox__c = '12345';
            accObj1.Pagent__c = agent1.Id;
            accObj1.PBAAgent__c = agent2.Id;
            lstAccounts.add(accObj1); 
            
            Account accObj2 = Utils_TestMethods.createAccount();
            accObj2.Name = 'VIC2 Account';
            accObj2.Phone = '3300000017';
            accObj2.Country__c = country.id;
            accObj2.StateProvince__c = stateProvince.id;
            accObj2.LeadingBusiness__c = 'Buildings';
            accObj2.ClassLevel1__c = 'Retailer';
            accObj2.Type = 'Prospect';
            accObj2.ServiceContractType__c = 'VIC';
            accObj2.City__c = 'bangalore';
            accObj2.ZipCode__c = '560066';
            accObj2.Street__c = 'EPIP Zone';
            accObj2.POBox__c = '123456';
            accObj2.Pagent__c = agent1.Id;
            accObj2.PBAAgent__c = agent2.Id;
            
            lstAccounts.add(accObj2);
            
             //Create VIC Account
            Account accObj3 = Utils_TestMethods.createAccount();
            accObj3.Name = 'VIC Account';
            accObj3.Phone = '3300000012';
            accObj3.Country__c = country.id;
            accObj3.StateProvince__c = stateProvince.id;
            accObj3.LeadingBusiness__c = 'Buildings';
            accObj3.ClassLevel1__c = 'Retailer';
            accObj3.Type = 'Prospect';
            accObj3.ServiceContractType__c = 'VIC';
            accObj3.City__c = 'bangalore';
            accObj3.ZipCode__c = '560066';
            accObj3.Street__c = 'EPIP Zone';
            accObj3.POBox__c = '123456';
            accObj3.Pagent__c = agent1.Id;
            accObj3.PBAAgent__c = agent2.Id;
            
            lstAccounts.add(accObj3);
            //Create Standard Account
            Account accObj4 = Utils_TestMethods.createAccount();
            accObj4.Name = 'Std Account';
            accObj4.Phone = '3300000013';
            accObj4.Country__c = country.id;
            accObj4.StateProvince__c = stateProvince.id;
            accObj4.LeadingBusiness__c = 'Buildings';
            accObj4.ClassLevel1__c = 'Retailer';
            accObj4.Type = 'Prospect';
            accObj4.ServiceContractType__c = 'Standard';
            accObj4.City__c = 'bangalore';
            accObj4.ZipCode__c = '560066';
            accObj4.Street__c = 'EPIP Zone';
            accObj4.POBox__c = '123456';
            accObj4.Pagent__c = agent1.Id;
            accObj4.PBAAgent__c = agent2.Id;
            lstAccounts.add(accObj4); 
            
            Account accObj5 = Utils_TestMethods.createAccount();
            accObj5.Name = 'VIC3 Account';
            accObj5.Phone = '3300000017';
            accObj5.Country__c = country.id;
            accObj5.StateProvince__c = stateProvince.id;
            accObj5.LeadingBusiness__c = 'Buildings';
            accObj5.ClassLevel1__c = 'Retailer';
            accObj5.Type = 'Prospect';
            accObj5.ServiceContractType__c = 'VIC';
            accObj5.City__c = 'bangalore';
            accObj5.ZipCode__c = '560066';
            accObj5.Street__c = 'EPIP Zone';
            accObj5.POBox__c = '123456';
            accObj5.Pagent__c = agent1.Id;
            accObj5.PBAAgent__c = agent2.Id;
            
            lstAccounts.add(accObj5);
        insert lstAccounts;
         //Create VIC Contact 13 - No. +3300000014
        Contact conObj13 = Utils_TestMethods.createContact(accObj1.id, ' Contact13' );
        conObj13.WorkPhone__c = '+3300000014';
        conObj13.accountid = accObj1.id;
        conObj13.ServiceContractType__c = 'Standard';
        conObj13.CallFlowOption__c='External Phone';
        conObj13.Pagent__c = agent1.Id;
        conObj13.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj13);
        
        //Create VIC Contact 14 - No. +3300000014
        Contact conObj14 = Utils_TestMethods.createContact(accObj1.id, ' Contact14' );
        conObj14.WorkPhone__c = '+3300000014';
        conObj14.accountid = accObj1.id;
        conObj14.ServiceContractType__c = 'Standard';
        conObj14.CallFlowOption__c='External Phone';
        conObj14.Pagent__c = agent1.Id;
        conObj14.PBAAgent__c = agent2.Id;
        lstContacts.add(conObj14);
        
        insert lstContacts;
        
         Ani__c aniObj1 = new Ani__c();
        aniObj1.ANINumber__c = '3300000017';
        aniObj1.account__c = accObj5.id;
        lstANIs.add(aniObj1);
         insert lstANIs;
            
            WS_ANISearch.Customer customer = new WS_ANISearch.Customer();
            Id[] fixedSearchResults = null; 
            test.startTest();
        
            //SINGLE DAC ACCOUNT MATCH
            fixedSearchResults = new Id[1];
            fixedSearchResults[0] = accObj1.Id;
            Test.setFixedSearchResults(fixedSearchResults);
            customer = WS_ANISearch.getANICustomerInfos('3300000014');
            
            //SINGLE STANDARD ACCOUNT MATCH
            fixedSearchResults = new Id[1];
            fixedSearchResults[0] = accObj4.Id;
            Test.setFixedSearchResults(fixedSearchResults);
            customer = WS_ANISearch.getANICustomerInfos('3300000013');
            
            fixedSearchResults = new Id[1];
            fixedSearchResults[0] = accObj3.Id;
            Test.setFixedSearchResults(fixedSearchResults);
            customer = WS_ANISearch.getANICustomerInfos('3300000012');
            
            fixedSearchResults = new Id[2];
            fixedSearchResults[0] = accObj5.Id;
            fixedSearchResults[1] = accObj2.Id;
            Test.setFixedSearchResults(fixedSearchResults);
            customer = WS_ANISearch.getANICustomerInfos('3300000017');
            
            fixedSearchResults = new Id[2];
            fixedSearchResults[0] = accObj5.Id;
            fixedSearchResults[1] = aniObj1.Id;
            Test.setFixedSearchResults(fixedSearchResults);
            customer = WS_ANISearch.getANICustomerInfos('3300000017');
        test.stopTest();
    }
    static testmethod void hiddenANItestmethod(){
      WS_ANISearch.Customer customer = new WS_ANISearch.Customer();
      customer = WS_ANISearch.getANICustomerInfos('-Hidden Number-');
    }
}