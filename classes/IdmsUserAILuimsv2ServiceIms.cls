/**
•   Created By: 
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: These classes are used for IDMS AIL to UIMS calls
**/
public class IdmsUserAILuimsv2ServiceIms {
    public class getAccessControlByUser {
        public String callerFid;
        public String federatedId;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
            private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
                private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                    private String[] field_order_type_info = new String[]{'callerFid','federatedId'};
                        }
    public class RequestedEntryNotExistsException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'label'};
                    }
    public class UnexpectedLdapResponseException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'label'};
                    }
    public class grantAccessControlToUser {
        public String callerFid;
        public String federatedId;
        public IdmsUserAILuimsv2ServiceIms.accessElement access;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
            private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
                private String[] access_type_info = new String[]{'access','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
                    private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                        private String[] field_order_type_info = new String[]{'callerFid','federatedId','access'};
                            }
    public class getAccessControlByUserResponse {
        public IdmsUserAILuimsv2ServiceIms.accessTree return_x;
        private String[] return_x_type_info = new String[]{'return','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'return_x'};
                    }
    public class revokeAccessControlToUser {
        public String callerFid;
        public String federatedId;
        public IdmsUserAILuimsv2ServiceIms.accessElement access;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
            private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
                private String[] access_type_info = new String[]{'access','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
                    private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                        private String[] field_order_type_info = new String[]{'callerFid','federatedId','access'};
                            }
    public class InvalidImsServiceMethodArgumentException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'label'};
                    }
    public class childs_element {
        public IdmsUserAILuimsv2ServiceIms.accessElement[] access;
        private String[] access_type_info = new String[]{'access','http://uimsv2.service.ims.schneider.com/',null,'0','-1','false'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'access'};
                    }
    public class accessTree {
        public IdmsUserAILuimsv2ServiceIms.accessList_element accessList;
        private String[] accessList_type_info = new String[]{'accessList','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'accessList'};
                    }
    public class revokeAccessControlToUserResponse {
        public Boolean return_x;
        private String[] return_x_type_info = new String[]{'return','http://uimsv2.service.ims.schneider.com/',null,'1','1','false'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'return_x'};
                    }
    public class accessList_element {
        public IdmsUserAILuimsv2ServiceIms.accessElement[] access;
        private String[] access_type_info = new String[]{'access','http://uimsv2.service.ims.schneider.com/',null,'0','-1','false'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'access'};
                    }
    public class IMSServiceSecurityCallNotAllowedException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'label'};
                    }
    public class grantAccessControlToUserResponse {
        public Boolean return_x;
        private String[] return_x_type_info = new String[]{'return','http://uimsv2.service.ims.schneider.com/',null,'1','1','false'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'return_x'};
                    }
    public class InactiveUserImsException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'label'};
                    }
    public class LdapTemplateNotReadyException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'label'};
                    }
    public class accessElement {
        public String type_x;
        public String id;
        public IdmsUserAILuimsv2ServiceIms.childs_element childs;
        private String[] type_x_type_info = new String[]{'type','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
            private String[] id_type_info = new String[]{'id','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
                private String[] childs_type_info = new String[]{'childs','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
                    private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                        private String[] field_order_type_info = new String[]{'type_x','id','childs'};
                            }
    public class SecuredImsException extends Exception{
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
            private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
                private String[] field_order_type_info = new String[]{'label'};
                    }
}