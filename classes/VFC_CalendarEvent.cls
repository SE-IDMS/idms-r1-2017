/**
 *  About
 *  -----
 *  Author: Paul Roth <proth@salesforce.com>
 *  Created for: Project SE
 *  Create date: Full Calendar event object
 *  
 *  Details
 *  -----
 *  Full Calendar event object
 *  
 *  Update History
 *  -----
 *  Thu Aug 08 18:35:16 CDT 2013 @24 /Internet Time/
 *  - initial version
 *  
 *  Issues / TODOs
 *  -----
 *  There is a concern that the coloring for the event should not be in apex,
 *  but should instead be in JavaScript. So as events are expired, they do not
 *  need further communication back and forth to SalesForce.com.
 *  
 *  As the calendar is refreshed anyway when the user navigates between months
 *  or navigates away from the page and back, this was considered less of a risk
 *  and was mitigated further through the 'press-and-hold' on the date selector
 *  (month/day) to refresh the latest from SalesForce.com.
 *  
 *  (There are a number of tasks, please see rally project)
**/
global with sharing class VFC_CalendarEvent {
    public ID id;
    public String title;
    public String description;
    public Boolean allDay;
    public DateTime eventStart;
    public DateTime eventEnd;
    public String color;
    public String type;
    public Boolean isComplete;
    public Boolean isConfirmed;
    public String whatId;
    public String whatName;     
    
    //-- @TODO: figure out how to get the user's start of day as it doesn't appear to be on the user record.
    //-- @TODO: investigate whether this even matters as the event is marked as all day
    public static final Time START_OF_DAY = Time.newInstance( 9, 0, 0, 0 );
    
    //-- specifies to the javascript side that we are using a task or an event
    public static final String TYPE_EVENT = 'Event';
    public static final String TYPE_TASK = 'Task';
    
    /** Set of statuses that are considered complete. **/
    public static final Set<String> COMPLETED_STATUSES = new Set<String>();
    
    //-- static initializer
    static {
        String completedStatusStr = null;
        
        if( Test.isRunningTest() ){
            completedStatusStr = ';Completed;Done;';
        } else {
            completedStatusStr = 'Completed;Done';
        }
        
        System.debug( 'VFC_CalendarEvent_static:' + completedStatusStr );
        for( String completedStatus : completedStatusStr.split( ';' ) ){
            if( completedStatus != null && completedStatus != '' ){
                COMPLETED_STATUSES.add( completedStatus );
            }
        }
    }
    
    //-- @TODO: move to a more unified location
    public static String GREEN_LIGHT ='green';
    public static String RED_LIGHT   ='red';
    public static String GREY_LIGHT  ='grey';
    public static String BLUE_LIGHT  ='blue';
    
    /** pattern used to differentiate dates and times from a locale string
     * <p>there is no time.format(), but there is date.format()
     **/
    //public static Pattern DATE_TIME_PATTERN = Pattern.compile( '^([^\\s]+)\\s(.+)$' );
    
    public VFC_CalendarEvent( Event e ){
        this.id = e.Id;
        this.title = e.Subject;
        this.description = e.Description;
        this.allDay = e.IsAllDayEvent;
        this.eventStart = e.StartDateTime;
        this.eventEnd = e.EndDateTime;
        this.type = TYPE_EVENT;
        this.isComplete = false;
        this.isConfirmed = e.ClientConfirmed__c;
        this.whatId = e.whatId;
        this.whatName = (!VFC_CalendarUTILS.empty(e.whatId)?e.what.name:'');
        
        if( e.StartDateTime != null && (DateTime.now().getTime() < e.StartDateTime.getTime()) ){
            this.color = BLUE_LIGHT;
        } else {
            this.color = GREY_LIGHT;
        }
    }
    
    public VFC_CalendarEvent( Task t ){
        this.id = t.Id;
        this.title = t.subject;
        this.description = t.Description;
        this.allDay = true;
        this.isConfirmed = true;
        this.whatId = t.whatId;
        this.whatName = (!VFC_CalendarUTILS.empty(t.whatId)?t.what.name:'');     
        //if( t.ReminderDateTime == null ){
            this.eventStart = DateTime.newInstance( t.ActivityDate, START_OF_DAY );
        //} else {
        //    this.eventStart = t.ReminderDateTime;
        //}
        this.type = TYPE_TASK;
        
        //-- @TODO: investigate whether this matters as it is an all day event.
        this.eventEnd = this.eventStart;
        
        this.isComplete = false;
        if( COMPLETED_STATUSES.contains( t.Status )){
            this.isComplete = true;
            this.color = GREY_LIGHT;
        } else if( (Date.today()).daysBetween( t.activityDate ) < 0 ){
            //-- today is after activity date
            this.color = RED_LIGHT;
        } else {
            this.color = GREEN_LIGHT;
        }
    }
}