/*
    01-Apr-2013 Srinivas Nallapati  PRM Apr13   DEF-1462
    11-Feb-2013   Srinivas Nallapati   Apr14 PRM  validate Partner Involvement 
*/
public class AP_OPPValueChainPlayers{

    public static void checkOpportunityPassedToPartner(List<OPP_ValueChainPlayers__c>  lsVCP)
    {
        set<Id> setOpportunityIds = new set<id>();
        List<OPP_ValueChainPlayers__c> vcpAcc=new List<OPP_ValueChainPlayers__c>();
        Map<Id,Id> conMapAcc=new Map<Id,Id>();
        List<Id> con=new List<Id>();
        List<Contact> lstcon=new List<Contact>();
        for(OPP_ValueChainPlayers__c vcp : lsVCP)
        {
            //Start added for April 2014 release for BR-4662
            if(vcp.Contact__c!=null &&vcp.Account__c==null){
                vcpAcc.add(vcp);
                con.add(vcp.Contact__c);
            }
             //END added for April 2014 release for BR-4662 
            if(vcp.Contact__c != null && vcp.ContactRole__c == System.Label.CLMAY13PRM40)
                setOpportunityIds.add(vcp.OpportunityName__c);
        }
        //Start added for April 2014 release for BR-4662
         if(!(con.IsEmpty())){
             lstcon=[Select Id,Contact.Account.Id from Contact where id in :con];
             for(Integer i=0;i<lstcon.size();i++){
                 if(!conMapAcc.containsKey(lstcon[i].Id))
                     conMapAcc.put(lstcon[i].Id,lstcon[i].Account.Id);
             }
             for(Integer i=0;i<vcpAcc.size();i++){
                 vcpAcc[i].Account__c=conMapAcc.get(vcpAcc[i].Contact__c);
             }
         }
        //END added for April 2014 release for BR-4662 

        if(setOpportunityIds.size() > 0)
        {
            List<Opportunity>  lsOpp = new List<Opportunity>();
            lsOpp = [select PartnerInvolvement__c, (select UserId, User.ContactId, OpportunityId  from OpportunityTeamMembers where User.ProfileId = :System.Label.CLMAR13PRM03 AND User.ContactId != null and User.isActive = true) from Opportunity where PartnerInvolvement__c = :System.Label.CLMAY13PRM41 and id in:setOpportunityIds LIMIT 50000];
            //Apr14
            set<id> conIds = new set<id>();
            for(Opportunity top : lsOpp)
            {
                for(OpportunityTeamMember totm : top.OpportunityTeamMembers)
                {
                    conIds.add(totm.User.ContactId);
                }
            }

            set<id> featureCons = new set<id>();
            //Add Feature ID in the SOQL where condition
            List<ContactAssignedFeature__c> lstConFea = [SELECT Active__c,Contact__c,FeatureCatalog__c FROM ContactAssignedFeature__c WHERE Active__c = true AND FeatureCatalog__c = 'a71J000000001v1'];
            for(ContactAssignedFeature__c conf : lstConFea)
            {
                featureCons.add(conf.Contact__c);
            }
            //end apr14    
            List<OpportunityTeamMember>  lstOTMtoSendMail  = new List<OpportunityTeamMember>();
            for(Opportunity op : lsOpp)
            {
                for(OpportunityTeamMember otm : op.OpportunityTeamMembers)
                {
                    //apr14
                    if(featureCons.contains(otm.User.ContactId))
                    {
                        otm.TECH_PartnerInvolvedDate__c = System.now();
                        lstOTMtoSendMail.add(otm);
                    }    
                }
            }

            if(lstOTMtoSendMail.size() > 0)
                update lstOTMtoSendMail; 
        }   
    }
}