global class Batch_ConsAcctAggregateResultIterator implements Iterator<AggregateResult>{
  AggregateResult[] results {get;set;}
  Integer index {get; set;}

  global Batch_ConsAcctAggregateResultIterator(){
      index = 0;

      results = [SELECT SEAccountID__c, Count(Id) totalIDs from Account Where IsPersonAccount = true AND SEAccountID__c!=null Group BY SEAccountID__c having Count(Id)>1];
  }
   
  global boolean hasNext()
  {
    return results != null && !results.isEmpty() && index < results.size();
  }

  global AggregateResult next()
  {
    return results[index++];
  }
}