@isTest
Public Class Batch_CTIUnrecognizedCalls_TEST{
   
    static testMethod void unitTest(){
        Account accountObj = Utils_TestMethods.createAccount();
        Database.insert(accountObj);
             
        contact contactObj=Utils_TestMethods.createContact(accountObj.Id,'testContact');
        contactObj.WorkPhone__c='123456';
        Database.insert(contactObj);     
        
        Date dueDate=system.Today();
        Task taskObj = Utils_TestMethods.createOpenActivity(accountObj.Id,contactObj.id,'Completed',dueDate);
        taskObj.ANI_Is_Entered__c=False;
        taskObj.ANI__c='123456';
        Database.insert(taskObj);  
        
        Batch_CTIUnrecognizedCalls ctiBatch  = new Batch_CTIUnrecognizedCalls();
        ID batchprocessid =Database.executeBatch(ctiBatch);      
    }
    static testMethod void unitTest1(){
        Account accountObj = Utils_TestMethods.createAccount();
        accountObj.phone='1234567';
        Database.insert(accountObj);
             
        contact contactObj=Utils_TestMethods.createContact(accountObj.Id,'testContact');
        contactObj.WorkPhone__c='123456';
        Database.insert(contactObj);     
        
        Date dueDate=system.Today();
        Task taskObj = Utils_TestMethods.createOpenActivity(accountObj.Id,contactObj.id,'Completed',dueDate);
        taskObj.ANI_Is_Entered__c=False;
        taskObj.ANI__c='1234567';
        Database.insert(taskObj);  
        
        Batch_CTIUnrecognizedCalls ctiBatch  = new Batch_CTIUnrecognizedCalls();
        ID batchprocessid =Database.executeBatch(ctiBatch);      
           
    }
    static testMethod void unitTest2(){
        Account accountObj = Utils_TestMethods.createAccount();
        Database.insert(accountObj);
             
        contact contactObj=Utils_TestMethods.createContact(accountObj.Id,'testContact');
        contactObj.WorkPhone__c='123456';
        Database.insert(contactObj);     
        
        Ani__c aniObj1 = new Ani__c();
        aniObj1.ANINumber__c = '12345678';
        aniObj1.Contact__c = contactObj.id;
        database.insert(aniObj1);
        
        Date dueDate=system.Today();
        Task taskObj = Utils_TestMethods.createOpenActivity(accountObj.Id,contactObj.id,'Completed',dueDate);
        taskObj.ANI_Is_Entered__c=False;
        taskObj.ANI__c='12345678';
        Database.insert(taskObj);  
        
        Batch_CTIUnrecognizedCalls ctiBatch  = new Batch_CTIUnrecognizedCalls();
        ID batchprocessid =Database.executeBatch(ctiBatch);      
    }

}