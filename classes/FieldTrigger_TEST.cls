@IsTest(SeeAllData=true)
public class FieldTrigger_TEST {
    static TestMethod void testMethod1(){
        App__c app=new App__c();
        app.Name='TestApp';        
        app.Description__c = 'manual steps';
        app.ObjectShortcut__c = 'SDT';
        app.Status__c = 'Draft';
        insert app;
        Step__c step= new Step__c();
        step.app__c=app.ID;        
        step.Name = 'SDT01_STEP';
        // step.RecordType.Name = 'First Step';
        step.actionvalueforapprove__c = 'action1';
        step.actionvalueforreject__c = 'reject1';
        step.approvebuttonlabel__c = 'approve';
        step.rejectbuttonlabel__c= 'reject';
        step.ownerselectionforthisstep__c = 'Manual';
        step.statusvalueonentrytothisstep__c= 'draft';
        step.statusvalueonrejectonthisstep__c = 'reject';
        step.stepnumber__c = 1;
        Insert Step; 
        
        //game starts now
        Field__c txtfld=new Field__c();
        txtfld.step__c=Step.Id;
        txtfld.layoutposition__c=1;
        txtfld.fieldtype__c = 'Text';
        Insert txtfld;
        
        txtfld.app__c = null;
        update txtfld;
        
        
        Field__c Picklistfield = new Field__c();
        Picklistfield.step__c = Step.Id;
        Picklistfield.fieldtype__c='Picklist';
        Picklistfield.layoutposition__c = 1;
        Picklistfield.app__c = app.Id;
        insert Picklistfield;
        
        Field__c Textfield = new Field__c();
        Textfield.step__c = Step.Id;
        Textfield.fieldtype__c='Text';
        Textfield.layoutposition__c = 1;
        Textfield.app__c = app.Id;
        insert Textfield;
        
        Field__c Lookupfield = new Field__c();
        Lookupfield.step__c = Step.Id;
        Lookupfield.fieldtype__c='Lookup';
        Lookupfield.layoutposition__c = 1;
        Lookupfield.app__c = app.Id;
        insert Lookupfield;
        
        Field__c Referencefield = new Field__c();
        Referencefield.step__c = Step.Id;
        Referencefield.fieldtype__c='Reference';
        Referencefield.layoutposition__c = 1;
        Referencefield.app__c = app.Id;
        insert Referencefield;
        
        Field__c Filefield = new Field__c();
        Filefield.step__c = Step.Id;
        Filefield.fieldtype__c='File';
        Filefield.layoutposition__c = 1;
        Filefield.app__c = app.Id;
        insert Filefield;
        
        Field__c MultiselectPicklistfield = new Field__c();
        MultiselectPicklistfield.step__c = Step.Id;
        MultiselectPicklistfield.fieldtype__c='Multiselect Picklist';
        MultiselectPicklistfield.layoutposition__c = 1;
        MultiselectPicklistfield.app__c = app.Id;
        insert MultiselectPicklistfield;        
        
        Field__c NumberField1 = new Field__c();
        NumberField1.step__c = step.Id;
        NumberField1.app__c = app.Id;
        NumberField1.fieldtype__c='Number';
        NumberField1.layoutposition__c = 1;
        insert NumberField1;
        
        Field__c NumberField2 = new Field__c();
        NumberField2.step__c = step.Id;
        NumberField2.app__c = app.Id;
        NumberField2.fieldtype__c='Number';
        NumberField2.layoutposition__c = 1;
        insert NumberField2;
        
         Field__c NumberField3 = new Field__c();
        NumberField3.step__c = step.Id;
        NumberField3.app__c = app.Id;
        NumberField3.fieldtype__c='Number';
        NumberField3.layoutposition__c = 1;
        insert NumberField3;
        
        Field__c DateField1 = new Field__c();
        DateField1.step__c = step.Id;
        DateField1.app__c = app.Id;
        DateField1.fieldtype__c='Date';
        DateField1.layoutposition__c = 1;
        insert DateField1;
        
        Field__c DateField2 = new Field__c();
        DateField2.step__c = step.Id;
        DateField2.app__c = app.Id;
        DateField2.fieldtype__c='Date';
        DateField2.layoutposition__c = 1;
        insert DateField2;
        
        Field__c TimeField1 = new Field__c();
        TimeField1.step__c = step.Id;
        TimeField1.app__c = app.Id;
        TimeField1.fieldtype__c='Date/Time';
        TimeField1.layoutposition__c = 1;
        insert TimeField1;
        
        Field__c TimeField2 = new Field__c();
        TimeField2.step__c = step.Id;
        TimeField2.app__c = app.Id;
        TimeField2.fieldtype__c='Date/Time';
        TimeField2.layoutposition__c = 1;
        insert TimeField2;
        
        Field__c CurrencyField1 = new Field__c();
        CurrencyField1.step__c = step.Id;
        CurrencyField1.app__c = app.Id;
        CurrencyField1.fieldtype__c='Currency';
        CurrencyField1.layoutposition__c = 1;
        insert CurrencyField1;
        
        Field__c CurrencyField2 = new Field__c();
        CurrencyField2.step__c = step.Id;
        CurrencyField2.app__c = app.Id;
        CurrencyField2.fieldtype__c='Currency';
        CurrencyField2.layoutposition__c = 1;
        insert CurrencyField2;
        
        Field__c CheckboxField1 = new Field__c();
        CheckboxField1.step__c = step.Id;
        CheckboxField1.app__c = app.Id;
        CheckboxField1.fieldtype__c='Checkbox';
        CheckboxField1.layoutposition__c = 1;
        insert CheckboxField1;
        
        Field__c CheckboxField2 = new Field__c();
        CheckboxField2.step__c = step.Id;
        CheckboxField2.app__c = app.Id;
        CheckboxField2.fieldtype__c='Checkbox';
        CheckboxField2.layoutposition__c = 1;
        insert CheckboxField2;
        
        Field__c TextareaField1 = new Field__c();
        TextareaField1.step__c = step.Id;
        TextareaField1.app__c = app.Id;
        TextareaField1.fieldtype__c='Textarea';
        TextareaField1.layoutposition__c = 1;
        insert TextareaField1;
        
        Field__c TextareaField2 = new Field__c();
        TextareaField2.step__c = step.Id;
        TextareaField2.app__c = app.Id;
        TextareaField2.fieldtype__c='Textarea';
        TextareaField2.layoutposition__c = 1;
        insert TextareaField2;
        
        
    }
}