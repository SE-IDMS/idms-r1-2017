global class CaseArchiving_DeleteArchiveCases_Batch implements Database.batchable<Sobject>{

    global Database.QueryLocator start (Database.BatchableContext bc){
        String Query ;
        Integer size = Integer.Valueof(System.Label.CLJUL14CCC04);
        if(!Test.isRunningTest()){
            if(size==1)
                Query =   'select id ,CaseNumber from case where Casenumber='+System.label.CLJUL14CCC07+' ';
            else    
                Query = 'select id ,CaseNumber from case where archivedstatus__c ='+ System.Label.CLJUL14CCC09+ ' ';
         }
         else{
             Query = 'select id,casenumber from case';
         
         }  
        return  Database.getQueryLocator(Query);
    }
    global void execute(Database.BatchableContext Bc,List<Sobject> Scope){
        List<ArchiveCase__c> lstACsesToBeDel = new List<ArchiveCase__c>(); 
        Map<String,case> cseNumberRecMap = new Map<String,case>();
        set<String> aCseNameSet = new set<String>();
        for(sObject genralisedObj: Scope){
            sObject sObj = genralisedObj;
            Case cseObj = (Case)genralisedObj;
            cseNumberRecMap.put(cseObj.CaseNumber,cseObj);
        }
        
        for(ArchiveCase__c ArchiveObj : [select id,Name from ArchiveCase__c where Name in : cseNumberRecMap.keySet()]){
            lstACsesToBeDel.add(ArchiveObj);
        }
       
        if(lstACsesToBeDel!=null && lstACsesToBeDel.size()>0)
            delete lstACsesToBeDel;
    }
    global void finish(Database.BatchableContext bc){
    
    }
}