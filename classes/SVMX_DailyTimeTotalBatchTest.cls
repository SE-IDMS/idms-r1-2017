/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class SVMX_DailyTimeTotalBatchTest {

    private testmethod static void test()
    {
        SVMXC__Service_Group__c team = new SVMXC__Service_Group__c(
            Name = 'test'
        );
        
        insert team;
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(
            SVMXC__Service_Group__c = team.Id,
            SVMXC__Salesforce_User__c = UserInfo.getUserId()
        );
        
        insert tech;
        
        Map<String, String> settingsMap = new Map<String,String>();
        settingsMap.put('TIMESHEET002', '1');
        settingsMap.put('TIMESHEET003', 'true');
        
        ServiceMaxTimesheetUtils.appSettings = settingsMap;
        
        SVMXC_Timesheet__c ts = new SVMXC_Timesheet__c(
            Technician__c = tech.Id,
            Start_Date__c = system.today().toStartOfMonth(),
            End_Date__c = system.today().addMonths(1).toStartOfMonth().addDays(-1)
        );
        
        insert ts;
        
        SVMXC_Time_Entry__c te = new SVMXC_Time_Entry__c();
        te.Activity__c = 'Administration';
        te.Start_Date_Time__c = system.now();
        te.End_Date_Time__c = system.now().addHours(1);
        te.Timesheet__c = ts.Id;
        te.Technician__c = tech.Id;
        insert te;
        
        Test.startTest();
        SVMX_DailyTimeTotalBatch batch = new SVMX_DailyTimeTotalBatch('SELECT ID, Technician__r.SVMXC__Salesforce_User__c, OwnerId, Technician__r.SVMXC__Salesforce_User__r.TimeZoneSidKey FROM SVMXC_Timesheet__c WHERE Id = \'' + ts.Id + '\'');
        Database.executeBatch(batch);
        Test.stopTest();
        
    }
}