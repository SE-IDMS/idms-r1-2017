global class WS_EmailToCase{
    global class caseCreateRequest{
        webservice String       CaseId; 
        webservice String       AnswerToCustomer;
        webservice String       Subject;
        webservice String       Category; 
        webservice String       Reason;
        webservice String       SubReason;
        webservice String       ContactId; 
        //webservice String       AccountId;
        //webservice String       CreatedBy;
        webservice String       Owner; 
        webservice String       Customer_Request;
        webservice String       CountryCode;
        webservice String       Case_Origin;
        webservice String       Status;
        //webservice String       Case_Team;
        webservice String       CTI_Skill;
        webservice String       EmailCreationDateTime;
        webservice String       EmailOwnershipDateTime;
        webservice String       Priority;
        webservice String       Related_Case;
        webservice String       GenesysInteractionId;
        //webservice Blob[]       Attchments;
        webservice List<ExternalAttachment>       Attchments;
    }
    global class caseResult{
        webservice String       CaseId;
        webservice String       CaseNumber;
        webservice String       Account_Name;
        webservice String       Contact_FirstName;
        webservice String       Contact_LastName;
        webservice String       Contact_ServiceContractType;
        webservice String       Contact_SupportLevel;
        webservice String       Contact_CorrespLanguage;
        webservice String       Account_EmailAutoResponse;
        webservice String       TechStatus;
        webservice String       ErrorCode;
        webservice String       ErrorLabel;
    }
    global class ExternalAttachment{
        webservice String       strFileName;
        webservice Blob         Attchment;
    }
    
    global class closeCaseRequest{
        webservice String       CaseId;
        webservice String       AnswerToCustomer;
        webservice String       Owner;
        webservice List<ExternalAttachment>       Attchments;
    }
    global class closeCaseResult{
        webservice String       TechStatus;
        webservice String       ErrorCode;
        webservice String       ErrorLabel;
    }
    global class attachCaseResult{
        webservice String   CaseId;
        webservice String   CaseNumber;
        webservice String   Status;
        webservice String   Account_Name;
        webservice String   Contact_FirstName;
        webservice String   Contact_LastName;
        webservice String   Contact_ServiceContractType;
        webservice String   Contact_SupportLevel;
        webservice String   Contact_CorrespLanguage;
        webservice String   TechStatus;
        webservice String   ErrorCode;
        webservice String   ErrorLabel;
    }
    global class attachContactResult{
        webservice String   Account_Name;
        webservice String   Contact_FirstName;
        webservice String   Contact_LastName;
        webservice String   Contact_ServiceContractType;
        webservice String   Contact_SupportLevel;
        webservice String   Contact_CorrespLanguage;
        webservice String   TechStatus;
        webservice String   ErrorCode;
        webservice String   ErrorLabel;
    }
    
    webService static caseResult createCase(caseCreateRequest objCaseRequest) {
        caseResult objCaseRequestResult = new caseResult();
        if(objCaseRequest == null){
            objCaseRequestResult.TechStatus = 'KO';
            objCaseRequestResult.ErrorLabel = 'Please send a valid Input parameter';
            return objCaseRequestResult;
        }
        if(objCaseRequest.CaseId != null && objCaseRequest.CaseId.Trim() != ''){
            objCaseRequestResult = closeExistingOpenCase(objCaseRequest);
        }
        else{
            if(objCaseRequest.ContactId == null || objCaseRequest.ContactId.Trim() == ''){
                objCaseRequestResult.TechStatus = 'KO';
                objCaseRequestResult.ErrorLabel = 'Please press Create Case button from a Contact context';
            }
            else{
                objCaseRequestResult = createNewCase(objCaseRequest);
            }
        }       
        return objCaseRequestResult;
    }
    webService static closeCaseResult closeCase(closeCaseRequest objcloseCaseRequest) {
        closeCaseResult objcloseCaseResult = new closeCaseResult();
        if(objcloseCaseRequest == null){
            objcloseCaseResult.TechStatus = 'KO';
            objcloseCaseResult.ErrorLabel = 'Please send a valid Input parameter';
            return objcloseCaseResult;
        }
        
        if(objcloseCaseRequest.CaseId == null || objcloseCaseRequest.CaseId.Trim() == ''){
            objcloseCaseResult.TechStatus = 'KO';
            objcloseCaseResult.ErrorLabel = 'Invalid Case Id';
            return objcloseCaseResult;
        }
        if(objcloseCaseRequest.AnswerToCustomer == null || objcloseCaseRequest.AnswerToCustomer.Trim() == ''){
            objcloseCaseResult.TechStatus = 'KO';
            objcloseCaseResult.ErrorLabel = 'You must provide a detailed answer to the customer.';
            return objcloseCaseResult;
        }
        
        String strCaseId = objcloseCaseRequest.CaseId;
        if(strCaseId.left(3)!='500'){
            objcloseCaseResult.TechStatus = 'KO';
            objcloseCaseResult.ErrorLabel = 'Invalid Case Id';
            return objcloseCaseResult;
        }
        try{
            List<Case> lstCloseCase = new List<Case>([Select Id, OwnerId, AnswerToCustomer__c, Status From Case where Id =:strCaseId LIMIT 1]);
            if(lstCloseCase.size()>0){
                if(lstCloseCase[0].Status != 'Closed'){
                    lstCloseCase[0].Status ='Closed';
                    if(objcloseCaseRequest.Owner!=''){
                        String strOwnerSESA = objcloseCaseRequest.Owner + '@%';
                        List<User> lstUser = new List<User>([SELECT Id FROM User WHERE Username LIKE :strOwnerSESA LIMIT 1]);
                        if(lstUser.size()>0){
                        lstCloseCase[0].OwnerId                 =   lstUser[0].Id;
                        }
                    }
                    lstCloseCase[0].AnswerToCustomer__c=objcloseCaseRequest.AnswerToCustomer;
                    
                    if(objcloseCaseRequest.Attchments != null && objcloseCaseRequest.Attchments.size()>0 ){
                        attachExternalAttachmentToCase(lstCloseCase[0].Id, objcloseCaseRequest.Attchments);
                    }
                    
                    Database.DMLOptions DMLoption = new Database.DMLOptions(); 
                    DMLoption.allowFieldTruncation = true;
                    DMLoption.optAllOrNone = true;
                    DataBase.SaveResult caseUpdateSaveResult = Database.update(lstCloseCase[0],DMLoption);   
                    objcloseCaseResult.TechStatus ='OK';
                }
                else{
                    objcloseCaseResult.TechStatus = 'KO';
                    objcloseCaseResult.ErrorLabel = 'ALREADY_CLOSED';
                }
            }
        }
        catch(Exception e){
            objcloseCaseResult.TechStatus = 'KO';
            objcloseCaseResult.ErrorLabel = e.getMessage();
            
        }
        return objcloseCaseResult;
    }
    webService static attachCaseResult attachCase(String strCaseId) {
        attachCaseResult objattachCaseResult = new attachCaseResult();
        
        
        if(strCaseId.Trim() == ''){
            objattachCaseResult.TechStatus = 'KO';
            objattachCaseResult.ErrorLabel = 'Please press Attach Case from a Case Page';
            return objattachCaseResult;
        }
        
        if(strCaseId.left(3)!='500'){
            objattachCaseResult.TechStatus = 'KO';
            objattachCaseResult.ErrorLabel = 'Invalid Case Id';
            return objattachCaseResult;
        }
        try{
            List<Case> lstattchCase = new List<Case>([Select Id, CaseNumber, Status, Account.Name,Contact.FirstName, Contact.LastName, Contact.ServiceContractType__c,Contact.ContactSupportLevel__c,Contact.CorrespLang__c From Case where Id =:strCaseId LIMIT 1]);
            if(lstattchCase.size()>0){
                objattachCaseResult.CaseId = lstattchCase[0].Id;
                objattachCaseResult.CaseNumber = lstattchCase[0].CaseNumber;
                objattachCaseResult.Status  =   lstattchCase[0].Status; 
                objattachCaseResult.Account_Name = lstattchCase[0].Account.Name;
                objattachCaseResult.Contact_FirstName = lstattchCase[0].Contact.FirstName; 
                objattachCaseResult.Contact_LastName = lstattchCase[0].Contact.LastName;
                objattachCaseResult.Contact_ServiceContractType = lstattchCase[0].Contact.ServiceContractType__c;
                objattachCaseResult.Contact_SupportLevel = lstattchCase[0].Contact.ContactSupportLevel__c;
                objattachCaseResult.Contact_CorrespLanguage = lstattchCase[0].Contact.CorrespLang__c;
                objattachCaseResult.TechStatus = 'OK';
            }
        }
        catch(Exception e){
            objattachCaseResult.TechStatus = 'KO';
            objattachCaseResult.ErrorLabel = e.getMessage();
            
        }
        return objattachCaseResult;
    }
    
    webService static attachContactResult attachContact(String strContactId) {
        attachContactResult objAttachContactResult = new attachContactResult();
        
        
        if(strContactId.Trim() == ''){
            objAttachContactResult.TechStatus = 'KO';
            objAttachContactResult.ErrorLabel = 'Please Send a Valid Contact ID';
            return objAttachContactResult;
        }
        
        if(strContactId.left(3)!='003' && strContactId.left(3)!='001'){
            objAttachContactResult.TechStatus = 'KO';
            objAttachContactResult.ErrorLabel = 'Invalid Contact Id';
            return objAttachContactResult;
        }
        try{
            List<Contact> lstattchContact = new List<Contact>([Select Id, FirstName, LastName, Account.Name,ServiceContractType__c, ContactSupportLevel__c, CorrespLang__c From Contact where Id=:strContactId OR (AccountId =:strContactId AND IsPersonAccount=TRUE) LIMIT 1]);
            if(lstattchContact.size()>0){
                objAttachContactResult.Account_Name = lstattchContact[0].Account.Name;
                objAttachContactResult.Contact_FirstName = lstattchContact[0].FirstName; 
                objAttachContactResult.Contact_LastName = lstattchContact[0].LastName;
                objAttachContactResult.Contact_ServiceContractType = lstattchContact[0].ServiceContractType__c;
                objAttachContactResult.Contact_SupportLevel = lstattchContact[0].ContactSupportLevel__c;
                objAttachContactResult.Contact_CorrespLanguage = lstattchContact[0].CorrespLang__c;
                objAttachContactResult.TechStatus = 'OK';
            }
            else{
                objAttachContactResult.TechStatus = 'KO';
                objAttachContactResult.ErrorLabel = 'Please Send a Valid Contact ID';
            }
        }
        catch(Exception e){
            objAttachContactResult.TechStatus = 'KO';
            objAttachContactResult.ErrorLabel = e.getMessage();
            
        }
        return objAttachContactResult;
    }
    private static void attachExternalAttachmentToCase(Id CaseId, List<ExternalAttachment> lstExternalAttachments){
        List<Attachment> lstAttachment = new List<Attachment>();
        for(ExternalAttachment objExternalAttachment: lstExternalAttachments){
            Attachment objAttachment = new Attachment();
            objAttachment.Body = objExternalAttachment.Attchment;
            objAttachment.Name = objExternalAttachment.strFileName;
            objAttachment.ParentId = CaseId;
            lstAttachment.add(objAttachment);
        }
        DataBase.SaveResult[] attachmentInsertSaveResult = Database.Insert(lstAttachment,false);
    }
    private static caseResult createNewCase(caseCreateRequest objCaseRequest){  
        caseResult objCaseRequestResult = new caseResult();
        try{
            String strContactId = objCaseRequest.ContactId;
            List<Contact> lstContact = new List<Contact>([Select Id , AccountId from Contact where Id=:strContactId OR (AccountId =:strContactId AND IsPersonAccount=TRUE) LIMIT 1]);
            if(lstContact.size()> 0){
                System.debug(objCaseRequest);
                Contact objContact = lstContact[0];
                Case objNewCase= new Case();
                objNewCase.Subject                      =   (objCaseRequest.Subject!='') ? objCaseRequest.Subject:'';
                objNewCase.SupportCategory__c           =   (objCaseRequest.Category!='') ? objCaseRequest.Category:'';
                objNewCase.Symptom__c                   =   (objCaseRequest.Reason!='') ? objCaseRequest.Reason:'';
              //objNewCase.SubSymptom__c                   =   (objCaseRequest.Reason!='') ? objCaseRequest.SubReason:'';  BR-4310 Commented By Vimal K (GD India)
                objNewCase.ContactId                    =   objContact.Id;
                objNewCase.AccountId                    =   objContact.AccountId;
              //objNewCase.CreatedById                  =   (objCaseRequest.CreatedBy!='') ? objCaseRequest.CreatedBy:null;
              //objNewCase.OwnerId                      =   (objCaseRequest.Owner!='') ? objCaseRequest.Owner:null;
                if(objCaseRequest.Owner!=''){
                    String strOwnerSESA = objCaseRequest.Owner + '@%';
                    List<User> lstUser = new List<User>([SELECT Id FROM User WHERE Username LIKE :strOwnerSESA LIMIT 1]);
                    if(lstUser.size()>0){
                    objNewCase.OwnerId                  =   lstUser[0].Id;
                    }
                }
                objNewCase.CustomerRequest__c           =   (objCaseRequest.Customer_Request!='') ? objCaseRequest.Customer_Request:'';
                
                //objNewCase.CCCountry__c                 =   (objCaseRequest.CountryCode!='') ? objCaseRequest.CountryCode:null;
                if(objCaseRequest.CountryCode!=''){
                    List<Country__c> lstCountry = new List<Country__c>([Select Id from Country__c where CountryCode__c =:objCaseRequest.CountryCode LIMIT 1]);
                    if(lstCountry.size()>0)
                        objNewCase.CCCountry__c = lstCountry[0].Id;
                }
                objNewCase.Origin                       =   (objCaseRequest.Case_Origin!='') ? objCaseRequest.Case_Origin:'';
                objNewCase.Status                       =   'Open';
              //objNewCase.Team__c                      =   (objCaseRequest.Case_Team!='') ? objCaseRequest.Case_Team:null;
                if(objNewCase.Origin=='Phone' && objCaseRequest.CTI_Skill!='' && objNewCase.CCCountry__c !=null){
                    /* BR-4310 Commented By Vimal K (GD India)
                    List<CustomerCareTeam__c> lstCCTeam = new List<CustomerCareTeam__c>([Select Id, (SELECT CaseReason__c,CaseSubReason__c,CaseCategory__c FROM TeamCaseJunction__r where  Default__c = True LIMIT 1) from CustomerCareTeam__c where CTI_Skill__c=:objCaseRequest.CTI_Skill  AND CCCountry__c =:objNewCase.CCCountry__c LIMIT 1]);
                    */
                    List<CustomerCareTeam__c> lstCCTeam = new List<CustomerCareTeam__c>([Select Id, (SELECT CaseReason__c,CaseCategory__c FROM TeamCaseJunction__r where  Default__c = True LIMIT 1) from CustomerCareTeam__c where CTI_Skill__c=:objCaseRequest.CTI_Skill  AND CCCountry__c =:objNewCase.CCCountry__c LIMIT 1]);
                    if(lstCCTeam.size()>0){
                        System.debug(lstCCTeam);
                        objNewCase.Team__c=lstCCTeam[0].Id;
                        if(lstCCTeam[0].TeamCaseJunction__r != null){
                            TeamCaseJunction__c objTeamJunction = lstCCTeam[0].TeamCaseJunction__r;   
                            System.debug(objTeamJunction);    
                            if(objTeamJunction.CaseCategory__c != null)         
                                objNewCase.SupportCategory__c   =   objTeamJunction.CaseCategory__c;
                            if(objTeamJunction.CaseReason__c != null)
                                objNewCase.Symptom__c   =   objTeamJunction.CaseReason__c;
                            /* BR-4310 Commented By Vimal K (GD India)
                            if(objTeamJunction.CaseSubReason__c != null)
                                objNewCase.SubSymptom__c    =   objTeamJunction.CaseSubReason__c;
                            */
                        }
                    }
                }
                objNewCase.EmailCreationDateTime__c     =   (objCaseRequest.EmailCreationDateTime!='')  ? Datetime.valueOf(objCaseRequest.EmailCreationDateTime):null;
                objNewCase.EmailOwnershipDateTime__c    =   (objCaseRequest.EmailOwnershipDateTime!='') ? Datetime.valueOf(objCaseRequest.EmailOwnershipDateTime):null;
                objNewCase.Priority                     =   (objCaseRequest.Priority!='') ? objCaseRequest.Priority:'';
                System.debug(objCaseRequest.Related_Case);
                objNewCase.RelatedSupportCase__c        =   (objCaseRequest.Related_Case!='') ? objCaseRequest.Related_Case:null;
                objNewCase.GenesysInteractionId__c      =   (objCaseRequest.GenesysInteractionId!='') ? objCaseRequest.GenesysInteractionId:'';
                if(objNewCase.Origin=='Email'){
                }
                else if(objNewCase.Origin=='Phone'){
                    
                }

                Database.DMLOptions DMLoption = new Database.DMLOptions(); 
                DMLoption.allowFieldTruncation = true;
                DMLoption.optAllOrNone = true;
                DMLoption.EmailHeader.triggerAutoResponseEmail = false;  
                DataBase.SaveResult caseInsertSaveResult = Database.Insert(objNewCase,DMLoption);
                
                Id caseId = objNewCase.Id;
                
                if(objCaseRequest.Attchments != null && objCaseRequest.Attchments.size()>0 ){
                    attachExternalAttachmentToCase(caseId, objCaseRequest.Attchments);
                }
                
                
                List<Case> lstResultCase = new List<Case>([Select Id, CaseNumber, Account.Name, Account.EmailAutoResponse__c, Contact.FirstName, Contact.LastName, Contact.ServiceContractType__c, Contact.ContactSupportLevel__c, Contact.CorrespLang__c from Case where Id=:caseId LIMIT 1 ]);
                if(lstResultCase.size()>0){
                    objCaseRequestResult.CaseId = lstResultCase[0].Id;
                    objCaseRequestResult.CaseNumber = lstResultCase[0].CaseNumber;
                    objCaseRequestResult.Account_Name = lstResultCase[0].Account.Name;
                    objCaseRequestResult.Contact_FirstName = lstResultCase[0].Contact.FirstName;
                    objCaseRequestResult.Contact_LastName = lstResultCase[0].Contact.LastName;
                    objCaseRequestResult.Contact_ServiceContractType = lstResultCase[0].Contact.ServiceContractType__c;
                    objCaseRequestResult.Contact_SupportLevel = lstResultCase[0].Contact.ContactSupportLevel__c;
                    objCaseRequestResult.Contact_CorrespLanguage = lstResultCase[0].Contact.CorrespLang__c;
                    objCaseRequestResult.Account_EmailAutoResponse = lstResultCase[0].Account.EmailAutoResponse__c;
                    objCaseRequestResult.TechStatus='OK';
                }               
            }
            else{
                objCaseRequestResult.TechStatus = 'KO';
                objCaseRequestResult.ErrorLabel = 'Please press Create Case button from a Contact context';
            }
        }
        catch(Exception e){
            System.debug(e.getMessage());
            objCaseRequestResult.TechStatus = 'KO';
            objCaseRequestResult.ErrorLabel = e.getMessage();
        }
        return objCaseRequestResult;
    }
    private static caseResult closeExistingOpenCase(caseCreateRequest objCaseRequest){  
        caseResult objCaseRequestResult = new caseResult();
        try{
            List<Case> lstResultCase = new List<Case>([Select Id, OwnerId, AnswerToCustomer__c, Status,ContactId From Case where Id =:objCaseRequest.CaseId LIMIT 1]);
            if(lstResultCase.size()>0){
                if(lstResultCase[0].Status!='Closed'){
                    if(objCaseRequest.Status=='Closed'){
                        if(objCaseRequest.AnswerToCustomer == null || objCaseRequest.AnswerToCustomer.Trim() == ''){
                            objCaseRequestResult.TechStatus = 'KO';
                            objCaseRequestResult.ErrorLabel = 'You must provide a detailed answer to the customer.';
                            return objCaseRequestResult;
                        }
                        try{
                            if(lstResultCase[0].Status != 'Closed'){
                                lstResultCase[0].Status ='Closed';
                                if(objCaseRequest.Owner!=''){
                                    String strOwnerSESA = objCaseRequest.Owner + '@%';
                                    List<User> lstUser = new List<User>([SELECT Id FROM User WHERE Username LIKE :strOwnerSESA LIMIT 1]);
                                    if(lstUser.size()>0){
                                    lstResultCase[0].OwnerId                 =   lstUser[0].Id;
                                    }
                                }
                                lstResultCase[0].AnswerToCustomer__c=objCaseRequest.AnswerToCustomer;
                                
                                if(objCaseRequest.Attchments != null && objCaseRequest.Attchments.size()>0 ){
                                    attachExternalAttachmentToCase(lstResultCase[0].Id, objCaseRequest.Attchments);
                                }
                                
                                Database.DMLOptions DMLoption = new Database.DMLOptions(); 
                                DMLoption.allowFieldTruncation = true;
                                DMLoption.optAllOrNone = true;
                                DataBase.SaveResult caseUpdateSaveResult = Database.update(lstResultCase[0],DMLoption);   
                                objCaseRequestResult.TechStatus ='OK';
                            }
                            else{
                                objCaseRequestResult.TechStatus = 'KO';
                                objCaseRequestResult.ErrorLabel = 'ALREADY_CLOSED';
                            }
                        }
                        catch(Exception e){
                            objCaseRequestResult.TechStatus = 'KO';
                            objCaseRequestResult.ErrorLabel = e.getMessage();
                            
                        }   
                    }
                    else{  //If Input request Status is not equal to 'Closed'
                        lstResultCase=null;
                        lstResultCase = new List<Case>([Select Id, Status, CaseNumber, Account.Name, Account.EmailAutoResponse__c, ContactId, Contact.FirstName, Contact.LastName, Contact.ServiceContractType__c, Contact.ContactSupportLevel__c, Contact.CorrespLang__c from Case where Id=:objCaseRequest.caseId LIMIT 1 ]);
                        if(lstResultCase.size()>0){
                            objCaseRequestResult.CaseId = lstResultCase[0].Id;
                            objCaseRequestResult.CaseNumber = lstResultCase[0].CaseNumber;
                            objCaseRequestResult.Account_Name = lstResultCase[0].Account.Name;
                            objCaseRequestResult.Contact_FirstName = lstResultCase[0].Contact.FirstName;
                            objCaseRequestResult.Contact_LastName = lstResultCase[0].Contact.LastName;
                            objCaseRequestResult.Contact_ServiceContractType = lstResultCase[0].Contact.ServiceContractType__c;
                            objCaseRequestResult.Contact_SupportLevel = lstResultCase[0].Contact.ContactSupportLevel__c;
                            objCaseRequestResult.Contact_CorrespLanguage = lstResultCase[0].Contact.CorrespLang__c;
                            objCaseRequestResult.Account_EmailAutoResponse = lstResultCase[0].Account.EmailAutoResponse__c;
                            objCaseRequestResult.TechStatus='OK';
                        }
                    }
                }
                else{ //if Case Status is Closed
                    objCaseRequest.ContactId= lstResultCase[0].ContactId;
                    objCaseRequest.Related_Case = lstResultCase[0].Id;
                    objCaseRequestResult = createNewCase(objCaseRequest);
                }
            }
            else{
                objCaseRequestResult.TechStatus = 'KO';
                objCaseRequestResult.ErrorLabel = 'CASE ID NOT FOUND';
            }
        }
        catch(Exception e){
            System.debug(e.getMessage());
            objCaseRequestResult.TechStatus = 'KO';
            objCaseRequestResult.ErrorLabel = e.getMessage();
        }
        return objCaseRequestResult;
    }
}