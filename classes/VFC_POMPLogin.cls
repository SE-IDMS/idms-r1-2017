public without sharing class VFC_POMPLogin {
    
    public transient string PartnerV2Login {get;set;}
    public Map<String,String> URLParams = ApexPages.currentPage().getParameters();
    public Boolean newPOMP{get;set;}
    
    public VFC_POMPLogin() {
        User usr = [SELECT Id, Contact.NewPompCommunity__c FROM User WHERE Id = :UserInfo.getUserId()];
        newPOMP = usr.Contact.NewPompCommunity__c;
        string loginParameters = '&app='+System.label.CLMAR16PRM010;
        //List<FieloEE__Program__c> lstProgram = [Select id,FieloEE__SiteURL__c from FieloEE__Program__c where id= :System.label.CLAPR15PRM149];
        //if(lstProgram.size() > 0)
        partnerV2Login = System.label.CLAPR15PRM308+'/Menu/pomplogin';
            
      
        Cookie cLanguage ; 
        Cookie Country ;    
        String languageCode='';
        String cntryCode='';
        
        for(String s:URLParams.keyset()){
            loginParameters += '&'+s+'='+URLParams.get(s);
        }
        
        system.debug('loginParameters:' + loginParameters);
            
        if(!string.isBlank(loginParameters)){
            PartnerV2Login += '?'+loginParameters;    
            
       }
       system.debug('partnerV2Login :' + partnerV2Login );
    }
    public PageReference redirectpage(){
        System.debug('>>>>'+PartnerV2Login);
        PageReference pageRef = new PageReference(PartnerV2Login);
        pageRef.setRedirect(true);
        return pageRef;
    }
    public PageReference redirectNewPompPage(){
        System.debug('>>>>'+newPOMP);
        System.debug('>>>>'+URLParams.containsKey('PompStartUrl'));
        System.debug('>>>>'+URLParams);
        PageReference pageRef;
        if(newPOMP){
            if(!URLParams.containsKey('PompStartUrl')){
                pageRef = new PageReference('/pomp/Home');
                pageRef.setRedirect(true);
            }
            else{
                pageRef = new PageReference('/pomp/Home');
                pageRef.setRedirect(true);
            }
        }
        else{
            if(!URLParams.containsKey('PompStartUrl')){
                pageRef = new PageReference('/pomp/home/home.jsp');
                pageRef.setRedirect(true);
            }
            else{
                pageRef = new PageReference(URLParams.get('PompStartUrl'));
                pageRef.setRedirect(true);
            }
        }
        return pageRef;        
    }
}