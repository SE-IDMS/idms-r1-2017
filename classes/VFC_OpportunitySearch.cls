/*
Author:Siddharth N(GD Solutions)
Purpose:Search for duplicate opportunities and prepopulate the opportunity fields with values
Custom Label Reference
Name | value | purpose
*/
public without sharing class VFC_OpportunitySearch{

    public Opportunity opportunityRecord{get;set;}{opportunityRecord=new Opportunity();}    
    public final Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
    public String selectedOpportunityId{get;set;}{selectedOpportunityId=null;}
    public boolean isORFRelated{get;set;}{isORFRelated=false;}
    public boolean searchStep{get;set;}{searchStep=true;}
    public List<Opportunity> results{get;set;}{results=new List<Opportunity>();}
    public boolean disableCreateOpportunitybutton{get;set;}{disableCreateOpportunitybutton=true;}
    public String OpportunityTypePassed='';
    public boolean openOpptys{get;set;}
    private Account accountRecord;
    private Account endUserAccount;
    public PageReference newOppPage = new PageReference('/006/e');
    public String customerProjectId {get;set;}{customerProjectId =null;} 
    public String customerProjectName {get;set;}{customerProjectName =null;} 
    public User userRecord;
    
    public VFC_OpportunitySearch(ApexPages.StandardController controller) {
        opportunityRecord=(Opportunity)controller.getRecord();
        opportunityRecord.RecordTypeId=System.Label.CLOCT13SLS16;
        //opportunityRecord.OpportunitySource__c=System.Label.CLOCT13SLS04; 
        OpportunityTypePassed='';  
        
        if(pageParameters.containsKey('OpptyType'))
                OpportunityTypePassed=System.currentPageReference().getParameters().get('OpptyType');            
        if(pageParameters.containsKey('oppRegFormId'))
            isORFRelated=true;
        populateCountryOfDestination(); //Current user country is used for prepopulation and also for the NAM Wizard direction         
        if(opportunityRecord.AccountId!=null)
            populateAccountRelatedDetails(); 
       //Added Customer Project prepopulation for the Monalisa Project - oct 2015
        if(System.currentPageReference().getParameters().get('prjId')!=null)
            customerProjectId =System.currentPageReference().getParameters().get('prjId');
        if(System.currentPageReference().getParameters().get('prjName')!=null)
            customerProjectName = System.currentPageReference().getParameters().get('prjName');
          
    }
    /* Modified for BR - 3308 */
    private String sortExp = 'name';
    public String sortDirection{get;set;} {sortDirection = 'ASC';}
    public String sortExpression
    {
     get
     {
        return sortExp;
     }
     set
     {
       //if the column is clicked on then switch between Ascending and Descending modes
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
       sortExp = value;
     }
    }
    /* End of Modification for BR - 3308 */ 
    //Current user country is used for prepopulation and also for the NAM Wizard direction   
    private void populateCountryOfDestination()
    {
        userRecord=[Select id, Country__c,country from user where id=:UserInfo.getUserId() limit 1];
        //populate country of destination
        if(opportunityRecord.CountryOfDestination__c==null)
        {
                if(userRecord.Country__c!=null)
                {
                    for(Country__c countryRecord:[select id from Country__c where CountryCode__c=: userRecord.Country__c limit 1])
                    {
                        opportunityRecord.CountryOfDestination__c=countryRecord.id;
                    }
                }            
        }        
    }

    private String getSearchString()
    {
        /* Modified for BR-3308 
           Modifications include - Inclusion of Sort logic, including End User, Opportunity Stages in Search criteria
        */
        if(opportunityRecord.Name!=null && opportunityRecord.Name.length()>40)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.warning,Label.CLOCT14SLS09);
            ApexPages.addMessage(myMsg);
        }    
        String sortFullExp = sortExpression + ' ' + sortDirection;
        string nullvaluesort;
        //String searchString =  'FIND {'+Utils_Methods.escapeForSOSL(opportunityRecord.Name)+'*} IN NAME FIELDS ';

        String searchString =  'FIND \'' + opportunityRecord.Name + '*\' IN NAME FIELDS ';  //Changed from October14 Release
        searchString += ' RETURNING Opportunity(Id,Name,AccountId,EndUserCustomer__c,EndUserCustomer__r.Name, Account.Name, OwnerId, Owner.Name, CountryOfDestination__c,CountryOfDestination__r.Name,Location__c,StageName,CloseDate,Amount ';
            List<String> whereConditions = new List<String>();
            if(opportunityRecord.Location__c!=null && opportunityRecord.Location__c!='')
                whereConditions.add('Location__c=\''+Utils_Methods.escapeForWhere(opportunityRecord.Location__c)+'\'');
            if(opportunityRecord.CountryOfDestination__c!=null)      
                whereConditions.add('CountryOfDestination__c=\''+opportunityRecord.CountryOfDestination__c+'\'');
            if(opportunityRecord.EndUserCustomer__c!=null)      
                whereConditions.add('EndUserCustomer__c=\''+opportunityRecord.EndUserCustomer__c+'\'');
            if(opportunityRecord.AccountId!=null)                //Added account filter in oct 2015 release DEF-8329
                whereConditions.add('AccountId=\''+opportunityRecord.AccountId+'\'');
            if(openOpptys == true)
            {
                whereConditions.add('StageName!=\''+Label.CL00272+'\'');
                whereConditions.add('StageName!=\''+Label.CL00220+'\'');
            }
            if(sortDirection == 'ASC')            
                nullvaluesort= ' NULLS LAST';
            if(sortDirection == 'DESC')            
                nullvaluesort = ' NULLS FIRST';    
            if(whereConditions.size() > 0) 
            {
                searchString += ' WHERE ';
                for(String condition : whereConditions) {
                    searchString += condition + ' AND ';   
                }
            // delete last 'AND'
                searchString = searchString.substring(0, searchString.lastIndexOf(' AND '));                
            }
            searchString+=' ORDER BY ' + sortFullExp + nullvaluesort + ')';
            /* End of Modification for BR-3308 */
            
        return searchString;    
    }


    public void search()
    {       
        String searchQuery=getSearchString();
        
        /* Modification for BR-3308 */
        //if(opportunityRecord.AccountId!=null && accountRecord==null)
        //Removed accountRecord==null for DEF-8219 oct 2015 release
        if(opportunityRecord.AccountId!=null)
                populateAccountRelatedDetails();   
        
        /* End of Modification for BR-3308 */        
        try{
            List<List<Sobject>> searchResults=search.query(searchQuery);
            Utils_SDF_Methodology.log('START search: ', searchQuery);        
            if(searchResults.size()>0){
                Utils_SDF_Methodology.startTimer();
                results=(List<Opportunity>)searchResults[0];
                
                if(results.size()>30)
                disableCreateOpportunitybutton=true;
                else
                disableCreateOpportunitybutton=false;
            }
        }
        catch(Exception ex){    
            Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,ex.getmessage()));
        }
        finally{
            Utils_SDF_Methodology.stopTimer();
            Utils_SDF_Methodology.log('END search');
            Utils_SDF_Methodology.Limits();
        }
    }
    
    public PageReference continueCreation()
    {        
         /* Modified for BR-3308 */
         
         //Populates the Opportunity Name as per the new naming convention
         String oppName;
         if(opportunityRecord.Name!=null)
             oppName = opportunityRecord.Name;
         if(opportunityRecord.Location__c!=null  && opportunityRecord.Location__c!='')
             oppName = oppName + ' - '+ opportunityRecord.Location__c;
    
         if(opportunityRecord.AccountId!=null && accountRecord!=null)
         {
             if(accountRecord.AccShortName__c!=null)
                 oppName = oppName + ' - '+ accountRecord.AccShortName__c;
             else if(accountRecord.Name!=null)
                 oppName = oppName + ' - '+ accountRecord.Name;
         }
         if(opportunityRecord.EndUserCustomer__c!=null)
         {
             if(opportunityRecord.EndUserCustomer__c!=opportunityRecord.AccountId)
             {
                 endUserAccount = [select Name,Id from Account where Id=:opportunityRecord.EndUserCustomer__c];
                 oppName = oppName + ' - '+ endUserAccount.Name;
                 newOppPage.getParameters().put(System.Label.CLAPR14SLS27,endUserAccount.Name);
                 newOppPage.getParameters().put(System.Label.CLAPR14SLS28,endUserAccount.Id);
             } 
             else if(accountRecord.Name!=null)
                 oppName = oppName + ' - '+ accountRecord.Name;
         }
        /* End of Modification for BR-3308 */
        
         // add parameters from the initial context (except <save_new>)
        for(String param : pageParameters.keySet())
            newOppPage.getParameters().put(param, pageParameters.get(param));
        if(newOppPage.getParameters().containsKey('save_new'))
            newOppPage.getParameters().remove('save_new'); 
        newOppPage.getParameters().put('nooverride', '1');
        newOppPage.getParameters().put('opp3', oppName);
        try{
          String userLeadingBU=[Select id,UserBusinessUnit__c from User where id=:UserInfo.getUserId() limit 1].UserBusinessUnit__c ;
          String opptyBU='';
          if(userLeadingBU!='' && userLeadingBU!=null){
              opptyBU=CS_OpportunityLeadingBUMapping__c.getValues(userLeadingBU).OpportunityLeadingBU__c;
              newOppPage.getParameters().put(System.Label.CL00308,opptyBU);
          }
        }
        catch(Exception ex){
            System.debug('Exception'+ex.getMessage());
        }
        
        if(opportunityRecord.CountryOfDestination__c!=null)
        {
                newOppPage.getParameters().put(System.Label.CL00046, [SELECT Name FROM Country__c WHERE Id=:opportunityRecord.CountryOfDestination__c].Name);
                newOppPage.getParameters().put(System.Label.CL00047, opportunityRecord.CountryOfDestination__c);
        }        
        if(opportunityRecord.Location__c!=null)    
            newOppPage.getParameters().put(System.Label.CL00048, opportunityRecord.Location__c);  
        System.debug('*#### opportunityRecord.AccountId: '+opportunityRecord.AccountId);                          
        populateCaseRelatedDefaults();
    
    
        newOppPage.getParameters().put(System.label.CL00721,opportunityRecord.OpportunitySource__c);    
        calculateRecordType();    
        //CLDEC12SLS01 - html id for parentOpportunity field,
        //CLDEC12SLS02 - html id for includedInForecast field on opportunity page layout
        if(pageParameters.get(Label.CLDEC12SLS01)==null){
            if(!Test.isRunningTest())//CL00004 - value "Yes"
            newOppPage.getParameters().put(Label.CLDEC12SLS02,Label.CL00004);   
        }
        //Added Parent keycode population - DEF-10050 - June 2016 Q2 release
        else{
            Opportunity opptn=[select Keycode__c from Opportunity where Id=:pageParameters.get(Label.CLDEC12SLS01)];
            if(opptn.Keycode__c!=null)
               newOppPage.getParameters().put(Label.CLJUN16SLS14,opptn.Keycode__c);   
        }
        
        //Prepopulate the Customer Project for Monalisa Project - Oct 2015
        if(customerProjectId !=null)
            newOppPage.getParameters().put(System.Label.CLOCT15SLS05, customerProjectId );
         if(customerProjectName !=null)
            newOppPage.getParameters().put(System.Label.CLOCT15SLS06, customerProjectName );
    
        return newOppPage;
    }
    
    public PageReference selectOpportunityForORF()
    {
    // CLSEP12PRM08 - 'Rejected'
        OpportunityRegistrationForm__c opregForm = new OpportunityRegistrationForm__c(Id = pageParameters.get('oppRegFormId'),Status__c= System.Label.CLSEP12PRM08,RejectReason__c=Label.CLOCT14SLS47);         
        update opregForm;
    
        ORFwithOpportunities__c junctionObject=new ORFwithOpportunities__c(Opportunity__c=selectedOpportunityId,Opportunity_Registration_Form__c=opregForm.id);
        insert junctionObject;
    
        PageReference redirectToOppRegForm = new PageReference('/apex/VFP_ConvertORFToOpportunity?id='+opregForm.id);    
        redirectToOppRegForm.setRedirect(true);
        return redirectToOppRegForm;
    }
    
    public PageReference createOpportunity()
    {
        searchStep=false;
        return null;
    }
    
    private void populateAccountRelatedDetails()
    {       
        /* Modified for BR-3308 
           Modifications include -- Included Account Short Name in the query, populated Account Short Name to accShortName variable, 
           prepopulating End User if any for EUS, FUS Accounts in Opportunity Search Page.         
        */
        if(opportunityRecord.AccountId!=null)
        {
            accountRecord = [SELECT Name,LeadingBusiness__c,AccShortName__c,MarketSegment__c,MarketSubSegment__c FROM Account WHERE Id=:opportunityRecord.AccountId limit 1];
            if(accountRecord!=null)
            {
                newOppPage.getParameters().put('opp4', accountRecord.Name);
                
                newOppPage.getParameters().put('opp4_lkid',accountRecord.Id );
                //newOppPage.getParameters().put(System.Label.CL00308, accountRecord.LeadingBusiness__c);
                newOppPage.getParameters().put(System.Label.CL00306, accountRecord.MarketSegment__c);
                newOppPage.getParameters().put(System.Label.CL00307, accountRecord.MarketSubSegment__c);
                if(OpportunityTypePassed==System.Label.CLAPR14SLS25)
                {
                    if(opportunityRecord.EndUserCustomer__c==null)
                    opportunityRecord.EndUserCustomer__c = accountRecord.Id;
                    if(opportunityRecord.EndUserCustomer__c==opportunityRecord.AccountId)
                    {
                        newOppPage.getParameters().put(System.Label.CLAPR14SLS27,accountRecord.Name);
                        newOppPage.getParameters().put(System.Label.CLAPR14SLS28,accountRecord.Id);
                    }                
                } 
                if(OpportunityTypePassed==System.Label.CLAPR14SLS26)
                {
                    newOppPage.getParameters().put(System.Label.CLAPR14SLS29,accountRecord.Name);
                    newOppPage.getParameters().put(System.Label.CLAPR14SLS30,accountRecord.Id);
                }           
            }
        }
        /* End of Modification for BR-3308 */
    }
    
    private void populateCaseRelatedDefaults()
    {
        String CaseID= pageParameters.get(system.label.CL00329);
        if(caseID!=null)
        {  
            // Added this below code to prepopulate the Opportunity Source from the Case screen.                    
            for(Case caseRecord:[Select AccountID,CampaignKeycode__c,PromotionSource__c from case where ID =:CaseId limit 1]){      
                newOppPage.getParameters().put(CS007_StaticValues__c.getValues('CS007_16').Values__c,caseRecord.CampaignKeycode__c);      //Prepopulates Opportunity Keycode                                                                    
                opportunityRecord.AccountId=caseRecord.AccountId;
                if(caseRecord.PromotionSource__c!=null && CS_CaseOppSourceMapping__c.getAll().containskey(caseRecord.PromotionSource__c))
                {
                    newOppPage.getParameters().put(System.label.CL00721,CS_CaseOppSourceMapping__c.getValues(caseRecord.PromotionSource__c).OpportunitySource__c);
                }
            }
        }
    }
    
    private void calculateRecordType()
    {
        if(opportunityRecord.OpptyType__c != null){
            if(opportunityRecord.OpptyType__c.equalsIgnoreCase(System.Label.CL00241))
            {
                newOppPage.getParameters().put(System.Label.CL00216, System.Label.CL00548);
                newOppPage.getParameters().put(System.Label.CL00549, System.Label.CL00241);
                if(opportunityRecord.StageName != null){
                    newOppPage.getParameters().put('opp11',opportunityRecord.StageName);                                
                    if(opportunityRecord.StageName.equalsIgnoreCase(System.Label.CLOCT13SLS13) || opportunityRecord.StageName.equalsIgnoreCase(System.Label.CL00146))
                    {
                        newOppPage.getParameters().put('RecordType',System.Label.CL00551);
                    }
                    if(opportunityRecord.StageName.equalsIgnoreCase(System.Label.CL00147))
                    {
                        newOppPage.getParameters().put('RecordType',System.Label.CL00552);
                    }
                    if(opportunityRecord.StageName.equalsIgnoreCase(System.Label.CL00547))
                    {
                        newOppPage.getParameters().put('RecordType',System.Label.CL00553);
                    }
                }
    
            }
            if(opportunityRecord.OpptyType__c.equalsIgnoreCase(System.Label.CL00240))
            {
                newOppPage.getParameters().put(System.Label.CL00216, System.Label.CL00548); //Added for oct 2015 release - DEF-8303
                newOppPage.getParameters().put(System.Label.CL00549, System.Label.CL00240);
                if(opportunityRecord.StageName != null)
                {
                    newOppPage.getParameters().put('opp11',opportunityRecord.StageName);                                
                    if(opportunityRecord.StageName.equalsIgnoreCase(System.Label.CLOCT13SLS13) || opportunityRecord.StageName.equalsIgnoreCase(System.Label.CL00146))
                    {
                        newOppPage.getParameters().put('RecordType',System.Label.CL00144);
                    }
                    if(opportunityRecord.StageName.equalsIgnoreCase(System.Label.CL00147))
                    {
                        newOppPage.getParameters().put('RecordType',System.Label.CL00145);
                    }
                }
    
            } 
                //September Release Changes for Type and Stage Update
            if(opportunityRecord.OpptyType__c.equalsIgnoreCase(System.Label.CL00724))
            {
                newOppPage.getParameters().put(System.Label.CL00216, System.Label.CL00548);
                newOppPage.getParameters().put(System.Label.CL00549, System.Label.CL00724);
                if(opportunityRecord.StageName != null)
                {
                    newOppPage.getParameters().put('opp11',opportunityRecord.StageName);                                
                    if(opportunityRecord.StageName.equalsIgnoreCase(System.Label.CLOCT13SLS13) || opportunityRecord.StageName.equalsIgnoreCase(System.Label.CL00146))
                    {
                        newOppPage.getParameters().put('RecordType',System.Label.CL00551);
                    }
                    if(opportunityRecord.StageName.equalsIgnoreCase(System.Label.CL00147))
                    {
                        newOppPage.getParameters().put('RecordType',System.Label.CL00552);
                    }
                    if(opportunityRecord.StageName.equalsIgnoreCase(System.Label.CL00547))
                    {
                        newOppPage.getParameters().put('RecordType',System.Label.CL00553);
                    }
                }
            }
        }
        //Added for Monalisa Project October 2015 - DEF-8283
        if(customerProjectId!=null)
            newOppPage.getParameters().put(System.Label.CL00216, System.Label.CLOCT15SLS75);
    }
    
    //Added in Q4 - 2016 release - NAM Wizard BR 
    public PageReference checkNAMWizardRedirection(){
        //Redirect user to NAM wizard if he/she belongs to US and has NAM Wizard Permission set
        if(userRecord.country ==System.Label.CLQ416SLS012 || userRecord.country ==System.Label.CLQ416SLS014){
            List<AggregateResult> cntList=[SELECT count(Id) cnt FROM PermissionSetAssignment WHERE AssigneeId= :UserInfo.getUserId() AND PermissionSet.Name = 'NAM_Opportunity_Wizard'];
            if((integer)cntList[0].get('cnt') > 0){
                PageReference pg;
                if(opportunityRecord.AccountId!=null)
                    pg=new PageReference(Site.getPathPrefix()+System.Label.CLQ416SLS029+'?accID='+opportunityRecord.AccountId);
                else
                    pg=new PageReference(Site.getPathPrefix()+System.Label.CLQ416SLS029);
                pg.setredirect(true); 
                return pg;
            }
            else
              return null;    
        }
        else 
            return null;
    }   
}