/********************************************************************
* Company: Fielo
* Created Date: 05/08/2016
* Description: Page extension class for page FieloPRM_VFP_IncludeIFrame
********************************************************************/

global without sharing class FieloPRM_VFE_IncludeIFrameExtension{
    
    public String primaryChannelCategoryName {get; set;}
    public FieloEE__ProgramLayout__c layout {get; set;}
    public String primaryChannelCategoryId {get; set;}
    public ContactWrapper contactWrapper {get; set;}
    public MemberWrapper memberWrapper {get; set;}
    public FieloEE__Menu__c menu2 {get; set;}
    public String idPrimaryMenu {get;set;}
    public Boolean isMobile {get;set;}
    public Boolean isTablet {get;set;}

    public String fieldOptionalName {
        get{
            if(fieldOptionalName == null){
                String fieldName = 'OptionalName_' + FieloEE.OrganizationUtil.getLanguage() + '__c';
                Boolean bodyExist = Schema.SObjectType.FieloEE__Component__c.fields.getMap().containskey(fieldName);
                if(bodyExist)
                    fieldOptionalName = fieldName;
                else
                    fieldOptionalName = 'FieloEE__OptionalName__c';
            }
            return fieldOptionalName;
        }
        set;
    }


    public String fieldMenuTitle {
        get{
            if(fieldMenuTitle == null){
                String fieldName = 'Title_' + FieloEE.OrganizationUtil.getLanguage() + '__c';
                Boolean bodyExist = Schema.SObjectType.FieloEE__Menu__c.fields.getMap().containskey(fieldName);
                if(bodyExist)
                    fieldMenuTitle = fieldName;
                else
                    fieldMenuTitle = 'FieloEE__Title__c';
            }
            return fieldMenuTitle;
        }
        set;
    }

    public FieloPRM_VFE_IncludeIFrameExtension(FieloEE.PageController controller){
        if(ApexPages.currentPage().getCookies().get('IdPrimaryChannelMenu') != null){
            idPrimaryMenu = ApexPages.currentPage().getCookies().get('IdPrimaryChannelMenu').getValue();
        }
        
        String allFields = '';
        for(Schema.SObjectField nameAPIfield : Schema.SObjectType.FieloEE__Menu__c.fields.getMap().values()){
            allFields += allFields==''?String.ValueOf(nameAPIfield):', '+String.ValueOf(nameAPIfield);
        }
    
        try{menu2 = Database.query('SELECT '+allFields+' FROM FieloEE__Menu__c WHERE Id = \''+Apexpages.currentPage().getParameters().get('idMenu')+'\'');}catch(Exception e){system.debug('***Exception'+e.getStackTraceString());}
        
        //Akram
        Id memId = FieloEE.MemberUtil.getMemberId();
        
        if(memId != null){        
            memberWrapper = new MemberWrapper(FieloEE.MemberUtil.getAllFieldsLoggedMember());
            FieloEE__Member__c member = [SELECT Id, F_PRM_PrimaryChannel__c,F_Country__r.CountryCode__c, F_PRM_Channels__c,F_Country__c FROM FieloEE__Member__c WHERE Id =: memId];
            String codMenu = '';
            
            List<CountryChannels__c> countryChannels = new List<CountryChannels__c>();

            if(member.F_PRM_PrimaryChannel__c != '' && member.F_PRM_PrimaryChannel__c != null){                
                primaryChannelCategoryId = '';
                primaryChannelCategoryName = '';
                try{
                    countryChannels = [SELECT Id, F_PRM_Menu__c, F_PRM_Menu__r.FieloEE__ExternalName__c FROM CountryChannels__c WHERE SubChannelCode__c = : member.F_PRM_PrimaryChannel__c AND  F_PRM_CountryCode__c =: member.F_Country__r.CountryCode__c LIMIT 1];                  
                    FieloEE__Category__c category = [SELECT Id, Name FROM FieloEE__Category__c WHERE F_PRM_Menu__c =: countryChannels[0].F_PRM_Menu__c];
                    primaryChannelCategoryId = category.Id;
                    primaryChannelCategoryName = category.Name;
                }catch(Exception e){
                    System.debug('e: ' + e);
                }
            }
        }else{
            memberWrapper = new MemberWrapper();
        }
                
        List<Contact> listContact = [SELECT Id, PRMFirstName__c, PRMLastName__c, PRMEmail__c, PRMWorkPhone__c, PRMMobilePhone__c, PRMJobTitle__c, Account.PRMStreet__c, Account.Name FROM Contact WHERE Id IN (SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId()) LIMIT 1];
        String userLanguage = UserInfo.getLanguage(); 
        
        if(!listContact.isEmpty()){
            contactWrapper = new ContactWrapper(listContact[0].PRMFirstName__c + ' ' + listContact[0].PRMLastName__c, listContact[0].PRMEmail__c, listContact[0].PRMMobilePhone__c, listContact[0].PRMWorkPhone__c, listContact[0].PRMJobTitle__c, listContact[0].Account.PRMStreet__c,userLanguage,listContact[0].Account.Name);
        }
        
        layout = FieloEE.ProgramUtil.getFrontEndLayout();
        
        String userAgent = System.currentPageReference().getHeaders().get('User-Agent');
        String device = System.currentPageReference().getParameters().get('device');
        
        if (!Test.isRunningTest()) {        
            isMobile = (userAgent.contains('iPhone') || device == 'mobile');
            isTablet = (userAgent.contains('iPad') || (userAgent.contains('AppleWebKit') && device != null && device != 'mobile'));
        }
    }

    public string obtenerlenguaje(){
        return FieloEE.OrganizationUtil.getLanguage();
    }

     public PageReference redirectPageLogin(){
        PageReference pageRet = null;
        Id memId = FieloEE.MemberUtil.getMemberId();
        String cntryCookie = '';
        string isPreview = ApexPages.currentPage().getParameters().get('isPreview');
        
        try{
            if(memId != null && menu2 != null){
                
                if(menu2.FieloEE__ExternalName__c != 'ContinueRegistration'){
                     
                    if((menu2.F_PRM_Type__c == 'Template' || menu2.F_PRM_Status__c == 'Draft') &&  isPreview != '1'){
                        return new PageReference('/VFP_PRMMaintenancePage');
                    }
                     
                    String countryCode = '';
                    User u;
                    if(Test.isRunningTest())
                        u = [SELECT Id, FirstName, PRMRegistrationCompleted__c, ContactId FROM User WHERE FirstName=:'Test User First' LIMIT 1];
                    else
                        u =  AP_PRMUtils.isUserRegistered(); 
                    
                    Contact cont = [SELECT Id, PRMCountry__r.CountryCode__c, AccountId, PRMCompanyInfoSkippedAtReg__c From Contact WHERE Id=:u.ContactId]; 
                    Account acc = [SELECT Id, PRMCountry__r.CountryCode__c FROM Account WHERE Id=:cont.AccountId]; 
                    
                    if(!cont.PRMCompanyInfoSkippedAtReg__c){ 
                        countryCode = acc.PRMCountry__r.CountryCode__c; 
                        if(acc.PRMCountry__r.CountryCode__c == null)
                            countryCode = cont.PRMCountry__r.CountryCode__c;
                    } else { 
                        countryCode = cont.PRMCountry__r.CountryCode__c; 
                    }
                    if(String.isBlank(countryCode)){
                        Cookie cookieCountry = ApexPages.currentPage().getCookies().get('country');
                        if(cookieCountry != null)
                            cntryCookie = cookieCountry.getValue();
                        countryCode = cntryCookie;
                   }
                   
                   if(u.PRMRegistrationCompleted__c == false) {
                        integer activeChannelsCount = [SELECT count() FROM CountryChannels__c WHERE Country__r.CountryCode__c =:countryCode AND Active__c = True LIMIT 10];
                        if(activeChannelsCount > 0)
                            return new PageReference('/Menu/ContinueRegistration?country='+countryCode);
                        else
                            return new PageReference('/Menu/ContinueRegistration?country='+countryCode+'&err=nochannel');  
                    } 
                
                }
                if(menu2.FieloEE__visibility__c == 'Public' || (menu2.FieloEE__visibility__c == 'Hidden' && menu2.F_PRM_AvailableGuestUsers__c == true)){
                    return new pageREference('/partners');
                }
                
                return pageRet;
                
            }else{
                if(menu2 != null && (menu2.FieloEE__visibility__c != 'Public' && menu2.FieloEE__visibility__c != 'Both' && menu2.F_PRM_AvailableGuestUsers__c != true)){   
                    pageRet = new pageREference('/Menu/Login');    
                }
                
            }
        }catch(Exception e){}
        
        return pageRet;
    }

    @RemoteAction
    global static void sendEmailToCase(String description,String currentPage) {
        Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
        mail.setToAddresses(new String[] {'contactus_webinquiries@schneider-electric.com'});
        mail.setSubject('Product Technical Inquiry');
        mail.setBccSender(false);
        mail.setUseSignature(false);
        List<Contact> listContact = new List<Contact>();
        if(Test.isRunningTest())
            listContact = [SELECT Id, PRMFirstName__c, PRMLastName__c, PRMEmail__c, PRMWorkPhone__c, PRMMobilePhone__c, PRMJobTitle__c, Account.PRMStreet__c, Account.Name FROM Contact LIMIT 1];
        else
            listContact = [SELECT Id, PRMFirstName__c, PRMLastName__c, PRMEmail__c, PRMWorkPhone__c, PRMMobilePhone__c, PRMJobTitle__c, Account.PRMStreet__c, Account.Name FROM Contact WHERE Id IN (SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId()) LIMIT 1];
        
        if(!listContact.isEmpty()){
            String phone = listContact[0].PRMWorkPhone__c!=null?listContact[0].PRMWorkPhone__c:(listContact[0].PRMMobilePhone__c!=null?listContact[0].PRMMobilePhone__c:'');
            String firstName = listContact[0].PRMFirstName__c!=null?listContact[0].PRMFirstName__c:'';
            String email = listContact[0].PRMEmail__c!=null?listContact[0].PRMEmail__c:'';
            String lastName = listContact[0].PRMLastName__c!=null?listContact[0].PRMLastName__c:'';
            String accountName = listContact[0].Account.Name!=null?listContact[0].Account.Name:'';
            String accountAddress = listContact[0].Account.PRMStreet__c!=null?listContact[0].Account.PRMStreet__c:'';
            mail.setReplyTo(email);
            mail.setHtmlBody('<html><body>First Name = '+firstName+'<br/>Last Name = '+lastName+'<br/>Company Name = '+accountName+'<br/>Company Address = '+accountAddress+'<br/>Email = '+email+'<br/>Phone ='+phone+'<br/>Provide the webpage link ='+currentPage+'<br/>MESSAGE = '+description+'</body></html>');
            if(!Test.isRunningTest())
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    public class MemberWrapper{
        public String CountryName {get;set;}
        public String CountryCode {get;set;}
        public String primaryChannel {get;set;}
        
        public MemberWrapper(){
            this.countryCode = '';
            this.countryName = '';
            this.primaryChannel = '';
        }
        
        public MemberWrapper(FieloEE__Member__c member){
            this.countryCode = member.F_PRM_CountryCode__c;
            this.countryName = member.F_CountryName__c;
            this.primaryChannel = member.F_PRM_PrimaryChannel__c;
        }
    }

    public class ContactWrapper{
        public String name {get;set;}
        public String email {get;set;}
        public String mobilePhone {get;set;}
        public String workPhone {get;set;}
        public string jobTitle {get;set;}
        public string address {get;set;}
        public string conLanguage {get;set;}
        public string accountName {get;set;}
    
        public ContactWrapper(){
            this.name = '';
            this.email = '';
            this.mobilePhone = '';
            this.workPhone = '';
            this.jobTitle = '';
            this.address = '';
            this.conLanguage = '';
            this.accountName = '';
        }
        public ContactWrapper(String p_name, String p_email, String p_mobilePhone, String p_workPhone, String p_jobTitle, String p_address, String p_conLanguage, String p_accountName){
            this.name = p_name;
            this.email = p_email;
            this.mobilePhone = p_mobilePhone;
            this.workPhone = p_workPhone;
            this.jobTitle = p_jobTitle;
            this.address = p_address;  
            this.conLanguage = p_conLanguage;
            this.accountName = p_accountName;    
        }
    }
    
}