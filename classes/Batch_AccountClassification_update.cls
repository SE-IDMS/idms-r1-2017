global class Batch_AccountClassification_update implements Database.Batchable<sObject>
{
String query;

global Batch_AccountClassification_update (String Q)
{
    query=Q;
}
    
global Database.querylocator start(Database.BatchableContext BC)
{
return Database.getQueryLocator(query);
}
    
global void execute(Database.BatchableContext BC, List<sObject> scope)
{
List<Account> accns = new List<Account>();
	for(sObject s : scope)
    {
        Account a = (Account)s;
	 if(a.ClassLevel1__c=='XX' && a.Type=='SA' && a.PRMAccount__c==false)
            {
               a.ClassLevel1__c='EU'; 
                accns.add(a);
              
            }
            else if((a.Type=='TA' && a.PRMAccount__c==false && a.ClassLevel1__c=='XX' )||(a.Type=='NA' && a.PRMAccount__c==false && 
a.ClassLevel1__c=='XX'))
            {
              a.ClassLevel1__c='EU';  
                accns.add(a);
                
            }
            else if(a.Type=='GSA'  && a.ClassLevel1__c=='XX')
            {
               a.ClassLevel1__c='OM';  
               accns.add(a);
                
            }
        
      else if(a.ClassLevel1__c=='XX' && a.ParentId!=NULL && a.parent.classLevel1__c!=NULL && a.parent.classLevel1__c!='XX')
            {
               a.ClassLevel1__c=a.parent.classLevel1__c; 
                accns.add(a);
              
            }
     else if((a.ClassLevel1__c=='XX' && a.ParentId==NULL && a.UltiParentAcc__c!=NULL && a.UltiParentAcc__r.ClassLevel1__c!=NULL && a.UltiParentAcc__r.ClassLevel1__c!='XX') ||
                    (a.ClassLevel1__c=='XX' && a.ParentId!=NULL && a.parent.classLevel1__c=='XX' && a.UltiParentAcc__c!=NULL && a.UltiParentAcc__r.ClassLevel1__c!=NULL &&a.UltiParentAcc__r.ClassLevel1__c!='XX'))
            {
              a.ClassLevel1__c=a.UltiParentAcc__r.ClassLevel1__c;  
                accns.add(a);
                
            }
        
     else  if( a.ClassLevel1__c=='XX' && (a.MarketSegment__c=='ID5' || a.MarketSegment__c=='IT3' || a.MarketSegment__c=='BD9' || 
a.MarketSegment__c=='EN1' || a.MarketSegment__c=='ID4' || a.MarketSegment__c=='BD4'
           || a.MarketSegment__c=='BD3' || a.MarketSegment__c=='BD6' || a.MarketSegment__c=='PW3' || a.MarketSegment__c=='ID3' || 
a.MarketSegment__c=='EN2' || a.MarketSegment__c=='PW1' || a.MarketSegment__c=='EN3' || a.MarketSegment__c=='ID2' ))
            {
               a.ClassLevel1__c='EU'; 
                accns.add(a);
              
            }
            else if(a.MarketSegment__c=='BDZ' && a.ClassLevel1__c=='XX')
            {
              a.ClassLevel1__c='LC';  
                accns.add(a);
                
            }
        else if(a.MarketSegment__c=='IT2' && a.ClassLevel1__c=='XX')
            {
              a.ClassLevel1__c='FI';  
                accns.add(a);                
            }
        else if(a.MarketSegment__c=='IDZ' && a.ClassLevel1__c=='XX')
            {
              a.ClassLevel1__c='SI';  
                accns.add(a);                
            }
         else if(a.MarketSegment__c=='ID1' && a.ClassLevel1__c=='XX')
            {
              a.ClassLevel1__c='OM';  
                accns.add(a);                
            }        
         else if(a.MarketSegment__c=='BD1' && a.ClassLevel1__c=='XX')
            {
              a.ClassLevel1__c='RT';  
                accns.add(a);                
            }
        else if(a.MarketSegment__c=='IT1' && a.ClassLevel1__c=='XX')
            {
              a.ClassLevel1__c='VR';  
                accns.add(a);                
            }
        
      else  if(a.ClassLevel1__c=='XX' && a.ParentId!=NULL)
 {
if(a.parent.MarketSegment__c=='ID5' || a.parent.MarketSegment__c=='IT3' || a.parent.MarketSegment__c=='BD9' || a.parent.MarketSegment__c=='EN1' || a.parent.MarketSegment__c=='ID4' || a.parent.MarketSegment__c=='BD4'
           || a.parent.MarketSegment__c=='BD3' || a.parent.MarketSegment__c=='BD6' || a.parent.MarketSegment__c=='PW3' || a.parent.MarketSegment__c=='ID3' || a.parent.MarketSegment__c=='EN2' || a.parent.MarketSegment__c=='PW1' || a.parent.MarketSegment__c=='EN3' || a.parent.MarketSegment__c=='ID2' )
            {
               a.ClassLevel1__c='EU'; 
                accns.add(a);
              
            }
            else if(a.parent.MarketSegment__c=='BDZ')
            {
              a.ClassLevel1__c='LC';  
                accns.add(a);                
            }
        else if(a.parent.MarketSegment__c=='IT2')
            {
              a.ClassLevel1__c='FI';  
                accns.add(a);                
            }
        else if(a.parent.MarketSegment__c=='IDZ')
            {
              a.ClassLevel1__c='SI';  
                accns.add(a);                
            }
         else if(a.parent.MarketSegment__c=='ID1')
            {
              a.ClassLevel1__c='OM';  
                accns.add(a);                
            }        
         else if(a.parent.MarketSegment__c=='BD1')
            {
              a.ClassLevel1__c='RT';  
                accns.add(a);                
            }
        else if(a.parent.MarketSegment__c=='IT1')
            {
              a.ClassLevel1__c='VR';  
                accns.add(a);                
            }
}
else if (a.ClassLevel1__c=='XX' && a.ParentId==NULL && a.UltiParentAcc__c!=NULL)
{
    if(a.UltiParentAcc__r.MarketSegment__c=='ID5' || a.UltiParentAcc__r.MarketSegment__c=='IT3' || a.UltiParentAcc__r.MarketSegment__c=='BD9' || a.UltiParentAcc__r.MarketSegment__c=='EN1' || a.UltiParentAcc__r.MarketSegment__c=='ID4' || a.UltiParentAcc__r.MarketSegment__c=='BD4'
           || a.UltiParentAcc__r.MarketSegment__c=='BD3' || a.UltiParentAcc__r.MarketSegment__c=='BD6' || a.UltiParentAcc__r.MarketSegment__c=='PW3' || a.UltiParentAcc__r.MarketSegment__c=='ID3' || a.UltiParentAcc__r.MarketSegment__c=='EN2' || a.UltiParentAcc__r.MarketSegment__c=='PW1' || a.UltiParentAcc__r.MarketSegment__c=='EN3' || a.UltiParentAcc__r.MarketSegment__c=='ID2' )
            {
               a.ClassLevel1__c='EU'; 
                accns.add(a);
              
            }
            else if(a.UltiParentAcc__r.MarketSegment__c=='BDZ')
            {
              a.ClassLevel1__c='LC';  
                accns.add(a);                
            }
        else if(a.UltiParentAcc__r.MarketSegment__c=='IT2')
            {
              a.ClassLevel1__c='FI';  
                accns.add(a);                
            }
        else if(a.UltiParentAcc__r.MarketSegment__c=='IDZ')
            {
              a.ClassLevel1__c='SI';  
                accns.add(a);                
            }
         else if(a.UltiParentAcc__r.MarketSegment__c=='ID1')
            {
              a.ClassLevel1__c='OM';  
                accns.add(a);                
            }        
         else if(a.UltiParentAcc__r.MarketSegment__c=='BD1')
            {
              a.ClassLevel1__c='RT';  
                accns.add(a);                
            }
        else if(a.UltiParentAcc__r.MarketSegment__c=='IT1')
            {
              a.ClassLevel1__c='VR';  
                accns.add(a);                
            } 
			
}
else if (a.ACCTYPE__C=='ID' && a.ClassLevel1__c=='XX')
{
a.ClassLevel1__c='EU';  
accns.add(a);
}
        
        
        
}
Database.SaveResult[] srList = Database.update(accns, false);
for(Database.SaveResult sr : srList) {
if (sr.isSuccess()) {
System.debug('Updated Acc Classification successfully for ' +
 sr.getId());
}
else {
System.debug('Updating returned the following errors.');
for(Database.Error e : sr.getErrors()) {
System.debug(e.getMessage());
}
} 
} 
}
    
global void finish(Database.BatchableContext BC)
{
}  
    
}