/***************************************************************************
For July 2013 Release
When the Opportunity with status  Stage -2 and the Partner Involvment 'Passed to PArtner' is
not updated for 5 or 10 days (that is after 5 or 10 days if the opportunity status is stage -2')
 send email to partner and 
include the Opportunity Team on the cc: line of the email.

**************************************************************************/
public class AP_Opportunity_SendNotification
{
    @future
    public static void sendPendingOppNotification(list<Id> lstOppID)
    {
        try
        {
            list<OpportunityTeamMember> lstOppTeam = new list<OpportunityTeamMember>();
            map<Id, list<opportunityteamMember>> mapOppTeam = new map<Id, list<opportunityteamMember>>();
            list<Messaging.SingleEmailMessage> lstMail = new list<Messaging.SingleEmailMessage>();
            map<id,opportunity> mapOpp = new map<Id, Opportunity>([Select id,ownerid,owner.email,owner.ContactId, TECH_5daysPending__c from opportunity where id in:lstOppID limit 10000]);
            lstOppTeam = [SELECT Id,OpportunityId,UserId,Opportunity.OwnerId,User.Email,Opportunity.Owner.Email,TeamMemberRole,User.ContactId FROM OpportunityTeamMember WHERE OpportunityId in : lstOppID limit 10000];
            for(opportunityteamMember oppTeam : lstOppTeam)
            {
                if(!mapOppTeam.containsKey(oppTeam.opportunityId))
                    mapOppTeam.put(oppTeam.opportunityId , new list<opportunityteamMember> {(oppTeam)});
                else
                    mapOppTeam.get(oppTeam.opportunityId).add(oppTeam);
            }
            
            for(Id oppId : lstOppID)
            {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                //toAddresses.add(mapOpp.get(oppId).owner.email);
                mapOpp.get(oppId).TECH_5daysPending__c = false;
                if(mapOppTeam.containsKey(oppId))
                {
                    
                    list<String> ccAddresses = new list<String>(); 
                    //ccAddresses.add(mapOpp.get(oppId).owner.email);
                    for(opportunityteamMember teamMem : mapOppTeam.get(oppID))
                    {
                        
                        //list<String> toAddresses = new list<String>(); 
                        if(teamMem.TeamMemberRole == Label.CLJUL13PRM05)
                            //toAddresses.add(teamMem.User.Email);
                            mail.settargetObjectId(teamMem.UserId);
                        else if(teamMem.User.ContactId == null)
                        {
                            ccAddresses.add(teamMem.User.Email);
                        }
                        //mail.setToAddresses(toAddresses);
                        //mail.setCCAddresses(ccAddresses);
                    }
                    system.debug('mapOpp.get(oppId).owner.ContactId:'+mapOpp.get(oppId).owner.ContactId);
                    if(mapOpp.get(oppId).owner.ContactId != null)
                       mail.settargetObjectId(mapOpp.get(oppId).ownerId);
                    else
                       ccAddresses.add(mapOpp.get(oppId).owner.email);

                    mail.setCCAddresses(ccAddresses);        
                }
                mail.setwhatId(oppId);
                mail.setorgWideEmailAddressId(Label.CLJUN13PRM04);
                mail.settemplateId(Label.CLJUL13PRM06);
                mail.setSaveAsActivity(false);
                lstMail.add(mail);
                system.debug('lstMail:'+lstMail);
            }
            
            if(!lstMail.isEmpty())
            {
                List<Messaging.SendEmailResult> lsRes = Messaging.sendEmail(lstMail, false);
                for(Messaging.SendEmailResult ser : lsRes)
                {
                    if(!ser.isSuccess())
                        system.debug('Error sending email '+ser.getErrors());
                }    
            }    
            update mapOpp.values();
        }
        catch(Exception e)
        {
            system.debug('Exception AP_Opportunity_SendNotification:sendPendingOppNotification:Error in sending email to Opportunity Team:'+e.getMessage());
        }
    }
    
    @future
    public static void sendRevokeOppNotification(list<Id> lstOppID)
    {
         try
        {
            //Auto Partner removal Oct13 - 
            List<Task> lstPartnerTasks = [select OwnerId, Status, TECH_OpportunityId__c, RecordTypeId FROM Task where RecordTypeId =:System.Label.CLOCT13PRM07 AND  TECH_OpportunityId__c in :lstOppID AND Status !='Completed'];
            set<id> tskOwnerIds = new set<id>();
            for(Task tk : lstPartnerTasks)
            {
                tskOwnerIds.add(tk.ownerId);
                tk.Status = System.Label.CL00327;
                tk.recordtypeid = System.Label.CLOCT13PRM14; //012A0000000gJbK
            }
            List<OpportunityTeamMember> lstOtmTmps= [SELECT Id, UserId, OpportunityId, User.ContactId FROM OpportunityTeamMember WHERE 
                    (UserId IN :tskOwnerIds AND OpportunityId IN:lstOppID AND 
                    TeamMemberRole = :System.Label.CLOCT13PRM13)];
            set<id> OtmConIds = new set<id>();
            for(OpportunityTeamMember otmtemp : lstOtmTmps)
            {
                OtmConIds.add(otmtemp.User.ContactId);
            }
            List<PartnerOpportunityStatusHistory__c> lstPOSHs = [SELECT Id, Opportunity__c, User__c, Status__c FROM PartnerOpportunityStatusHistory__c WHERE
                            (Opportunity__c IN :lstOppID AND User__c IN :tskOwnerIds AND
                            Status__c = 'Assigned')];

            for(PartnerOpportunityStatusHistory__c po : lstPOSHs)
            {
                po.Status__c = 'Retracted';
            }
            
            List<OPP_ValueChainPlayers__c> lstOVCPs= [SELECT Id, OpportunityName__c, Contact__c FROM OPP_ValueChainPlayers__c WHERE
                        (OpportunityName__c IN:lstOppID AND Contact__c IN :OtmConIds AND
                        ContactRole__c = :System.Label.CLSEP12PRM16)];



            //End of Partner Removal

            list<OpportunityTeamMember> lstOppTeam = new list<OpportunityTeamMember>();
            map<Id, list<opportunityteamMember>> mapOppTeam = new map<Id, list<opportunityteamMember>>();
            list<Messaging.SingleEmailMessage> lstMail = new list<Messaging.SingleEmailMessage>();
            map<id,opportunity> mapOpp = new map<Id, Opportunity>([Select id,ownerid,owner.email from opportunity where id in:lstOppID limit 10000]);
            lstOppTeam = [SELECT Id,OpportunityId,UserId,Opportunity.OwnerId,User.Email,Opportunity.Owner.Email,TeamMemberRole,User.ContactId FROM OpportunityTeamMember WHERE OpportunityId in : lstOppID limit 10000];
            for(opportunityteamMember oppTeam : lstOppTeam)
            {
                if(!mapOppTeam.containsKey(oppTeam.opportunityId))
                    mapOppTeam.put(oppTeam.opportunityId , new list<opportunityteamMember> {(oppTeam)});
                else
                    mapOppTeam.get(oppTeam.opportunityId).add(oppTeam);
            }
            system.debug('mapOppTeam:'+mapOppTeam);
            for(Id oppId : lstOppID)
            {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                //list<String> toAddresses = new list<String>(); 
                //toAddresses.add(mapOpp.get(oppId).owner.email);
                mapOpp.get(oppId).TECH_10daysPending__c = false;
                mapOpp.get(oppId).PartnerInvolvement__c = null;//Added for partner removal - Srinivas
                mail.settargetObjectId(mapOpp.get(oppId).ownerId);
                system.debug('in for:'+oppId);
                if(mapOppTeam.containsKey(oppId))
                {
                    system.debug('in if');
                    list<String> ccAddresses = new list<String>(); 
                    for(opportunityteamMember teamMem : mapOppTeam.get(oppID))
                    {
                        
                        if(teamMem.User.ContactId == null)
                        {
                            system.debug('in if2');
                            ccAddresses.add(teamMem.User.Email);
                        }
                        system.debug('ccAddresses:'+ccAddresses);
                        //mail.setToAddresses(toAddresses);
                        mail.setCCAddresses(ccAddresses);
                    }
                }
                mail.setwhatId(OppId);
                mail.setorgWideEmailAddressId(Label.CLJUN13PRM04);
                mail.settemplateId(Label.CLJUL13PRM07);
                mail.setSaveAsActivity(false);
                lstMail.add(mail);
                system.debug('lstMail:'+lstMail);
            }
            
            if(!lstMail.isEmpty())
            {
                List<Messaging.SendEmailResult> lsRes = Messaging.sendEmail(lstMail, false);
                for(Messaging.SendEmailResult ser : lsRes)
                {
                    if(!ser.isSuccess())
                        system.debug('Error sending email '+ser.getErrors());
                }    
            } 
            update mapOpp.values();
            //Added for partner removal - Srinivas
            delete lstOtmTmps;
            delete lstOVCPs;
            update lstPartnerTasks;
            update lstPOSHs;

        }
        catch(Exception e)
        {
            system.debug('Exception AP_Opportunity_SendNotification:sendRevokeOppNotification:Error in sending email to Opportunity Team:'+e.getMessage());
        }
    }
}