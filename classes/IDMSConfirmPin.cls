//*******************************************************************************************
//Created by      : Ranjith
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This class provides methods and algorithm to confirm the code PIN received by User.
//
//*******************************************************************************************

public without sharing class IDMSConfirmPin{
    
    IDMSCaptureError Errorcls;  
    string callerFid = Label.CLJUN16IDMS104;
    
    //This method implement the confirm pin algorithm
    public IDMSConfirmPinResponse ConfirmPin(string id,String FederationIdentifier,String Profile_update_source,String PinCode,String Operation,String Password){
        IDMSConfirmPinResponse response;
        Errorcls                                    = new IDMSCaptureError(); 
        set<string> operationstring                 = new set<string>();
        Schema.DescribeFieldResult IDMSOperation    = Schema.User.IDMSOperation__c.getDescribe();
        
        for (Schema.Picklistentry idmsOpr : IDMSOperation.getPicklistValues())
        {
            operationstring.add(idmsOpr.getValue());
        }
        system.debug('----'+operationstring );
        
        list <user> users= new list <user>();
        try{
            system.debug('id-->' + id );
            system.debug('fedId--> ' + FederationIdentifier) ; 
            if (String.isBlank(Profile_update_source)) {
                system.debug('entering profile--> ' + Profile_update_source) ;
                //Create Error Log record
                //Errorcls
                IDMSConfirmPin.IDMScreateLog('ConfirmPin','IDMSConfirmPin','ConfirmPin',null,'Profile Update Source is null',
                                       Profile_update_source,Datetime.now(),null,FederationIdentifier,null,false,null);                
                return new IDMSConfirmPinResponse(Label.CLJUN16IDMS76,Label.CLQ316IDMS001,null, null); 
            }
            else if (String.isBlank(PinCode)) {
                system.debug('pin code--> ' + PinCode) ;
                //Errorcls
                IDMSConfirmPin.IDMScreateLog('ConfirmPin','IDMSConfirmPin','ConfirmPin',null,'Pin Code is Mandatory',PinCode,
                                       Datetime.now(),null,FederationIdentifier,null,false,null); 
                
                return new IDMSConfirmPinResponse(Label.CLJUN16IDMS76,'Pin code is Mandatory',null, null); 
            }
            else if(string.isnotblank(id) && id != null){
                system.debug('entering id-->' + id );
                users = [SELECT id, name, IsActive, IDMSPinVerification__c, mobilephone, IDMSOperation__c, IDMSNewPhone__c, username, IDMS_User_Context__c, Federationidentifier 
                         FROM user WHERE Id =:Id limit 1];
                System.debug('users'+users);
                system.debug('id not null--> ' + users) ;
                if (users.size() == 0 ){
                    //Errorcls
                    IDMSConfirmPin.IDMScreateLog('ConfirmPin','IDMSConfirmPin','ConfirmPin',null,'User not found based on Id',Profile_update_source,
                                           Datetime.now(),null,FederationIdentifier,null,false,null);                
                    
                    return new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLJUN16IDMS40,null,null);
                }
            }
            else if((string.isnotblank(FederationIdentifier) || FederationIdentifier != null) && (id == null || id == '') )        
            {
                system.debug('entering fedid-->' + FederationIdentifier);
                users = [SELECT id, name, IDMSPinVerification__c, IsActive, IDMSOperation__c, IDMSNewPhone__c, username, IDMS_User_Context__c, Federationidentifier 
                         FROM user WHERE FederationIdentifier =:FederationIdentifier limit 1];
                System.debug('users2'+users); 
                if (users.size() == 0 ){
                    //Errorcls
                    IDMSConfirmPin.IDMScreateLog('ConfirmPin','IDMSConfirmPin','ConfirmPin',null,'User not found based on FedId',Profile_update_source,
                                           Datetime.now(),null,FederationIdentifier,null,false,null);                
                    return new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLJUN16IDMS39,null,null);
                }
            }
            
            else {
                system.debug('entering else --> ') ; 
                //Errorcls
                IDMSConfirmPin.IDMScreateLog('ConfirmPin','IDMSConfirmPin','ConfirmPin',null,Label.CLJUN16IDMS42,Profile_update_source,
                                       Datetime.now(),null,FederationIdentifier,null,false,null);                
                return new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLJUN16IDMS42,null,null);
            }
            system.debug('users after--> ' + users  + ' pin code ' + PinCode) ;
            if(users != null && users.size()>0 && users[0].IDMSPinVerification__c == PinCode){
                System.debug('users[0].IDMSPinVerification__c'+users[0].IDMSPinVerification__c);
                if(operationstring.contains(Operation) && Operation.equalsignorecase(Label.CLQ316IDMS004)){
                    if(String.isBlank(Password)){
                        //Errorcls
                        IDMSConfirmPin.IDMScreateLog('ConfirmPin','IDMSConfirmPin','ConfirmPin',null,'Password Cannot be Null when IDMSOperation is Setpassword',
                                               Profile_update_source,Datetime.now(),null,FederationIdentifier,null,false,null);      
                        return new IDMSConfirmPinResponse(Label.CLJUN16IDMS76,Label.CLQ316IDMS062,null, null); 
                    }
                    if(!Pattern.matches(Label.CLNOV16IDMS129, Password) && String.isNotBlank(Password)){
                        return new IDMSConfirmPinResponse(Label.CLJUN16IDMS76,Label.CLJUN16IDMS52,null, users[0].id); 
                    }
                    if (users[0].IsActive == true){
                        System.setPassword(users[0].id, Password);
                        users[0].IDMSPinVerification__c = null;
                        setPassWithSms(Profile_update_source, users[0].id, Password);
                        Database.SaveResult SaveResult = Database.update(users[0],false);
                        if (SaveResult.isSuccess()) {  
                            return new IDMSConfirmPinResponse (Label.CLJUN16IDMS94,Label.CLQ316IDMS012,users[0].FederationIdentifier,users[0].id);
                        }    
                        else{                         
                            response = new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLQ316IDMS008,users[0].FederationIdentifier,users[0].id);
                        }
                    }
                    else {
                        String idmsPin                  = users[0].IDMSPinVerification__c;
                        users[0].IsActive               = true;
                        users[0].IDMSPinVerification__c = null;
                        // update users;
                        system.debug('users--> ' + users  + ' ' + users[0].id + 'Password --> ' + Password ) ;  
                        
                        Database.SaveResult SaveResult = Database.update(users[0],false);
                        if (SaveResult.isSuccess()) {
                            system.debug('users future --> ' + users  + ' ' + users[0].id + 'Password --> ' + Password ) ;  
                            updatePassword(users[0].id,Password); 
                            setPassWithSms(Profile_update_source, users[0].id, Password);
                            response = new IDMSConfirmPinResponse (Label.CLJUN16IDMS94,Label.CLQ316IDMS063,users[0].FederationIdentifier,users[0].id);
                        }
                        else{
                            return new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLQ316IDMS011,users[0].FederationIdentifier,users[0].id);
                        }
                        
                    }
                }
                else if (operationstring.contains(Operation) && Operation.equalsignorecase(Label.CLQ316IDMS005)){
                    system.debug('entering userupdaterecord' ) ;
                    users[0].MobilePhone                = users[0].IDMSNewPhone__c;
                    users[0].IDMSPinVerification__c     = null;
                    users[0].username                   = users[0].IDMSNewPhone__c+Label.CLSEP16IDMS2;
                    users[0].email                      = users[0].IDMSNewPhone__c + Label.CLQ316IDMS044;
                    if(!Profile_update_source.equalsignorecase('UIMS')){
                        system.debug('UIMS CALL' );
                        IDMSUIMSWebServiceCalls.updateUIMSUser(JSON.serialize(users[0]),callerFid);
                        system.debug('Post UIMS CALL' );
                    }
                    system.debug('before save ' );
                    Database.SaveResult SaveResult = Database.update(users[0],false);
                    system.debug('after save ' );
                    if (SaveResult.isSuccess()) {
                        system.debug('sucess' );
                        return new IDMSConfirmPinResponse (Label.CLJUN16IDMS94,Label.CLQ316IDMS009,users[0].FederationIdentifier,users[0].id);
                        system.debug('after response sucess' );
                    }
                    else{
                        system.debug('skiped sucess and in else and error' );
                        response = new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLQ316IDMS013,users[0].FederationIdentifier,users[0].id);
                    }
                }
                else
                {
                    return new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLQ316IDMS014,users[0].FederationIdentifier,users[0].id);
                }
                
            }      
            else{
                return new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLQ316IDMS015,users[0].FederationIdentifier,users[0].id);
                
            }
        }
        catch(Exception e){
            String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
            //Errorcls
            IDMSConfirmPin.IDMScreateLog('ConfirmPin','IDMSConfirmPin','ConfirmPin',users[0].username,ExceptionMessage ,Profile_update_source,Datetime.now(),users[0].IDMS_User_Context__c,FederationIdentifier,null,false,null);                
            response = new IDMSConfirmPinResponse (Label.CLJUN16IDMS76,Label.CLQ316IDMS030,users[0].FederationIdentifier,users[0].id); 
        }
        return response;    
    }
    
    public void setPassWithSms(String updateSource, ID UserId, String Password){
        if(!updateSource.equalsignorecase('UIMS')){
            user fedId = [select federationidentifier,mobilePhone, IDMS_VU_NEW__c from user where id =:UserId];
            system.debug('UIMS Calls'+fedId);
            IDMSEncryption encrypt = new IDMSEncryption();
            String samlAssrestion  = encrypt.ecryptedToken(fedId.federationidentifier,fedId.IDMS_VU_NEW__c);
            IDMSUIMSWebServiceCalls.setPasswordWithSMS(fedId.mobilePhone, Password, samlAssrestion);
        } 
    }
    
    @future
    public static void updatePassword(String Id1,string Password1){
        System.setPassword(Id1, Password1);        
    }
    
     @future
    public static void IDMScreateLog(String API_Name,String Classname,String MethodName,String Username,String Exception_StackTrace,
                              String Registration_Source,DateTime Exception_Date,String User_Context,String FederationId,
                              String Contacted_email,boolean Email_to_be_sent,String Functional_Step){
       
       IDMSCaptureError Errorcls = new IDMSCaptureError ();  
       Errorcls.IDMScreateLog(API_Name,Classname,MethodName,Username,Exception_StackTrace,
                              Registration_Source,Exception_Date,User_Context,FederationId,
                              Contacted_email,Email_to_be_sent,Functional_Step);      
    }
}