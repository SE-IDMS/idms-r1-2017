global with sharing class VFC_DisplayResultUnifiedSearch {

    public String NoteForReturnCenter { get; set; }
    
    public List<ProductLineQualityContactMapping__c> listobjPL{get;set;}
    public ProductLineQualityContactMapping__c objPL{get;set;}
    public List<RMAShippingToAdmin__c> lstRCs {get; set;}
    
    public boolean isError{get;set;}
    
    public boolean isCancel{get;set;}
    
    public String gmrCode{get;set;}
    public String mode{get;set;}
    public String commRef{get;set;}
    
    public List<Problem__c> lstProblems {get; set;}
    Public boolean noProblems {get; set;}
    public String noProblemsMsg {get;set;} {noProblemsMsg=Label.CLOCT14I2P27;}
    Public boolean noRCs {get; set;}
    Public String noRCsMsg {get; set;} {noRCsMsg=Label.CLOCT14I2P28;}
    public Boolean noQualityContact {get;set;} {noQualityContact=false;}
    public String noQualityContactMsg {get;set;} {noQualityContactMsg=Label.CLOCT14I2P29;}
     
    public List<CommercialReference__c> crs {get;set;}
     
    public Integer limitToDisplay{get;set;}
    public List<CRWrap> crsWrapRes1{get;set;}
    public boolean displayLinkToReport{get;set;}
    public String linkToReport{get;set;}
    
    public String debugVal{get;set;}
    
    //Added by Uttara - BR-7096 : OCT 2015 Release
    public String source {get; set;}
    public Boolean isCase {get; set;} {isCase = false;}
    public Boolean isSFRP {get; set;} {isSFRP = false;}
    public Boolean isUPS {get; set;} {isUPS = false;}
    public String caseId {get; set;}
    public Case currentCase {get; set;}
    public Set<Id> problemIds {get; set;}
    public Set<Id> problemIds1 {get; set;}
    public string strHdn {get;set;}
    public list<ProblemCaseLink__c> lstProblemLinkedCase = new list<ProblemCaseLink__c>();
    public set<string> stPLCID = new set<string>();
    
    
    class CRWrap implements Comparable{
        public CommercialReference__c cr{get;set;}
    
        public Integer compareTo(Object obj){
        //Have to sort in Ascending Now.
        //In favouring condition for this return 1 when this should come first  (Replace -1 with 1 and 1 with -1, NO THAT's WRONG.)*/
        /*Avoid: //Have to sort in Descending by default.
            //In favouring condition for this return -1 when this should come first */
            CRWrap crObj=(CRWrap)obj;
            //Check if Severity=10
            //Have to sort even if Severity is 10
    /*        if ( crObj.cr.problem__r.Severity__c.equals(Label.CLOCT14I2P22)
            && cr.problem__r.Severity__c.equals(Label.CLOCT14I2P22) ) {
                if (cr.problem__r.Priority__c > crObj.cr.problem__r.Priority__c) {
                    return 1;
                } else {
                    return -1;
                }
            }
            
            if ( crObj.cr.problem__r.Severity__c.equals(Label.CLOCT14I2P22) ) return 1;
            if ( cr.problem__r.Severity__c.equals(Label.CLOCT14I2P22) ) return -1;
    */        
            //If both have Severity<10 then compare Priority
            if (cr.problem__r.Priority__c > crObj.cr.problem__r.Priority__c) {
                return 1;        //Replaced
            } else if (cr.problem__r.Priority__c < crObj.cr.problem__r.Priority__c) {
                return -1; //Replaced
            }
            return 0;
        }
        public CRWrap(CommercialReference__c cr){
            this.cr=cr;
        }
    }
    
    Public VFC_DisplayResultUnifiedSearch() {
    
       gmrCode= System.CurrentPageReference().getparameters().get('gmrCode');
       mode= System.CurrentPageReference().getparameters().get('mode');
       commRef= System.CurrentPageReference().getparameters().get('commRef');
       source= System.CurrentPageReference().getparameters().get('source');
       if(source == '' || source == null) {
           isUPS = true;
           System.debug('!!!! Entered UPS');
       }
       if(source == 'case') {  //If page invoked from Case detail page
           System.debug('!!!! Entered case');
           isCase = true;
           caseId = Apexpages.currentPage().getParameters().get('id');
           currentCase = [SELECT Id,Status,CommercialReference__c,Family__c,TECH_GMRCode__c,CaseSymptom__c,CaseSubSymptom__c  FROM Case WHERE Id =: caseId];
               commRef = currentCase.CommercialReference__c;  
           System.debug('!!!! case : ' + currentcase);
       }
       if(source == 'SFRP') { // If page invoked from Search for Relevant Problem button on Case
           System.debug('!!!! Entered SFRP');
           isSFRP = true;
           caseId = Apexpages.currentPage().getParameters().get('id');
           currentCase = [SELECT Id,Status,CommercialReference__c,TECH_GMRCode__c,CaseSymptom__c,CaseSubSymptom__c FROM Case WHERE Id =: caseId];
               commRef = currentCase.CommercialReference__c;  
           System.debug('!!!! SFRP : ' + currentcase);
       }       
       System.debug('mode='+mode);
       System.debug('gmrCode: '+gmrCode);
       noRCs=false;
       
       if((!isUPS && currentcase.CaseSymptom__c != null && currentcase.CaseSymptom__c != '') || isUPS) {
           if (gmrCode!=null && gmrCode!='') {
             
                 listobjPL= [SELECT ProductLine__c,QualityContact__r.name,QualityContact__r.IsActive, Id, BusinessUnit__c, AccountableOrganization__r.Name, AccountableOrganization__c from ProductLineQualityContactMapping__c where TECH_PLGMRCode__c=:gmrCode.substring(0,Integer.valueOf(Label.CLOCT14I2P08))]; // TECH_PM0CodeInGMR__c Changed to TECH_PLGMRCode__c //Label.CLOCT14I2P08=4
                 system.debug('listobjPL:'+listobjPL);
                 system.debug('     gmrCode.substring(0, Integer.valueOf(Label.CLOCT14I2P08)):'+     gmrCode.substring(0,Integer.valueOf(Label.CLOCT14I2P08))); //Label.CLOCT14I2P08=4
            
                 If ( listobjPL.size()>0 && listobjPL!=null ) {
                    objPL=listobjPL[0];
                    system.debug('**objPL** + objPL');
                 }
                 else {
                    isError=True; 
                    noQualityContact=true;
                 }            
               
                 lstRCs = getLocalRCs();
                 if (lstRCs.isempty()) 
                    noRCs = True; 
                 else {
                    sortRCs(lstRCs);
                    if (lstRCs.size()>Integer.valueOf(Label.CLOCT14I2P23) ) 
                        NoteForReturnCenter='Note: '+ Label.CLOCT14I2P23 +' of '+lstRCs.size()+' centers are displayed above. Run a report on Receiving Point Determination Rules to see the complete list.'; 
                 } 
                 
                 //TO Display Problems:
                 if (mode!=null) 
                     setProblems();
                 else 
                     noProblems=true;                 
             }
             else {
                   isCancel=True; 
                   ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CLOCT15I2P35)); // 'Select appropriate Commercial Reference or Family on Case.'
             }
        }
        else {
            isCancel=True; 
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error, Label.CLOCT15I2P36)); // 'Select appropriate Symptom on Case.'
        } 
    }      
      
    public void setProblems(){
        noProblems=false;
        // Page called from case detail page link
        if(isCase && source == 'case' && (currentCase.Status == Label.CLOCT15I2P31 || currentCase.Status == Label.CLOCT15I2P33 || currentCase.Status == Label.CLOCT15I2P34)) { // page called from case detail page
            for(ProblemCaseLink__c varplctemp:[SELECT id,Case__c,Problem__c FROM ProblemCaseLink__c WHERE Case__c=:caseId]) {
                stPLCID.add(varplctemp.Problem__c);
            }
            
            String queryStr='select problem__c, problem__r.name, problem__r.Tech_SafetyRelated__c, Problem__r.TECH_Symptoms__c, problem__r.SensitivityIcon__c,problem__r.Tech_SafetyRelated1__c, problem__r.title__c, problem__r.Severity__c, problem__r.status__c, problem__r.WhatIsTheProblem__c, problem__r.WhyIsItaProblem__c, problem__r.SummaryOfContainment__c, problem__r.Priority__c, problem__r.sensitivity__c, id, commercialreference__c, gmrcode__c, problem__r.AccountableEntity__c, problem__r.How_to_behave_with_customers__c from CommercialReference__c where ';
            if (mode.equals(Label.CLOCT14I2P24)){ //Commercial Reference
                 queryStr=queryStr+'commercialreference__c=\''+commRef+'\'';
            } 
            else if (mode.equals(Label.CLOCT14I2P25)){ //Family
                 queryStr=queryStr+'gmrCode__c LIKE \''+gmrCode.substring(0,Integer.valueOf(Label.CLOCT14I2P03))+'%\''; //Label.CLOCT14I2P03=8
            }
            crs = database.query(queryStr);
            System.debug('!!!! Case : Commercial Refs : '+ crs);
            
           if (crs==null || crs.size()==0) {
               noProblems=true;
           } 
           else {
               crsWrapRes1=new List<CRWrap>();
               problemIds=new Set<Id>();
               for (CommercialReference__c cr:crs){
                 if (!problemIds.contains(cr.Problem__c) && !stPLCID.contains(cr.Problem__c) && (cr.Problem__r.Status__c == Label.CLOCT15I2P31 || cr.Problem__r.Status__c == Label.CLOCT15I2P32 || cr.Problem__r.Status__c == Label.CLOCT15I2P33) ) { // 'New' , 'Stand By' , 'In Progress'
                     problemIds.add(cr.Problem__c);
                     
                     if (problemIds.contains(cr.Problem__c) && cr.Problem__r.TECH_Symptoms__c != null) {
                    
                       if(cr.Problem__r.TECH_Symptoms__c!=null && cr.Problem__r.TECH_Symptoms__c.contains(currentcase.CaseSymptom__c+' - '+currentcase.CaseSubSymptom__c)) 
                               crsWrapRes1.add(new CRWrap(cr));
                   }
                 }
               }
               
               crsWrapRes1.sort();
               if(crsWrapRes1.size() == 0) 
                   noProblems=true;               
            }
        }
        
        // page called from Search for Relevant Problem button
        else if(isSFRP && source == 'SFRP' && (currentCase.Status == Label.CLOCT15I2P31 || currentCase.Status == Label.CLOCT15I2P33 || currentCase.Status == Label.CLOCT15I2P34)) { // page called from case detail page
            for(ProblemCaseLink__c varplctemp:[SELECT id,Case__c,Problem__c FROM ProblemCaseLink__c WHERE Case__c=:caseId]) {
                stPLCID.add(varplctemp.Problem__c);
            }
            System.debug('!!!! stPLCID' + stPLCID);
            
            String queryStr='select problem__c, problem__r.name, Problem__r.TECH_Symptoms__c, problem__r.Tech_SafetyRelated__c, problem__r.SensitivityIcon__c,problem__r.Tech_SafetyRelated1__c, problem__r.title__c, problem__r.Severity__c, problem__r.status__c, problem__r.WhatIsTheProblem__c, problem__r.WhyIsItaProblem__c, problem__r.SummaryOfContainment__c, problem__r.Priority__c, problem__r.sensitivity__c, id, commercialreference__c, gmrcode__c, problem__r.AccountableEntity__c, problem__r.How_to_behave_with_customers__c from CommercialReference__c where ';
            if (mode.equals(Label.CLOCT14I2P24)){ //Commercial Reference
                 queryStr=queryStr+'commercialreference__c=\''+commRef+'\'';
            } 
            else if (mode.equals(Label.CLOCT14I2P25)){ //Family
                 queryStr=queryStr+'gmrCode__c LIKE \''+gmrCode.substring(0,Integer.valueOf(Label.CLOCT14I2P03))+'%\''; //Label.CLOCT14I2P03=8
            }
            crs = database.query(queryStr);
            
            System.debug('!!!! SFRP : Commercial Refs : '+ crs + 'Size = ' + crs.size());
            
           if (crs==null || crs.size()==0) {
               noProblems=true;
           } 
           else {
               crsWrapRes1=new List<CRWrap>();
               problemIds=new Set<Id>();
               for (CommercialReference__c cr:crs) {
                   if (!stPLCID.contains(cr.Problem__c)) {
                       
                       problemIds.add(cr.Problem__c);
                       if(cr.Problem__r.TECH_Symptoms__c!=null && cr.Problem__r.TECH_Symptoms__c.contains(currentcase.CaseSymptom__c+' - '+currentcase.CaseSubSymptom__c)) 
                               crsWrapRes1.add(new CRWrap(cr));                  
                   }
               }
           
               System.debug('!!!! problemIds' + problemIds);
               if(!Test.isRunningTest()) { 
                   
                   crsWrapRes1.sort();
                   if(crsWrapRes1.size() == 0) 
                       noProblems=true;
               }
           }
               
        }
        
        else if(isCase && (currentCase.Status == Label.CL00325 || currentCase.Status == Label.CL00326)) // 'Closed' , 'Cancelled'
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLOCT15I2P37)); // 'Selected Case is either Closed or Cancelled.'
        
        // page called from Home page - Unified Product Search
        else { 
            //if (Schema.getGlobalDescribe().get('Problem__c').getDescribe().isAccessible()) {
                String queryStr='select problem__c, problem__r.name, problem__r.Tech_SafetyRelated__c, problem__r.SensitivityIcon__c,problem__r.Tech_SafetyRelated1__c, problem__r.title__c, problem__r.Severity__c, problem__r.status__c, problem__r.WhatIsTheProblem__c, problem__r.WhyIsItaProblem__c, problem__r.SummaryOfContainment__c, problem__r.Priority__c, problem__r.sensitivity__c, id, commercialreference__c, gmrcode__c, problem__r.AccountableEntity__c, problem__r.How_to_behave_with_customers__c from CommercialReference__c where ';
                if (mode.equals(Label.CLOCT14I2P24)){ //Commercial Reference
                     //queryStr=queryStr+'gmrCode__c=\''+gmrCode+'\'';
                     queryStr=queryStr+'commercialreference__c=\''+commRef+'\'';
                } else if (mode.equals(Label.CLOCT14I2P25)){ //Family
                    queryStr=queryStr+'gmrCode__c LIKE \''+gmrCode.substring(0,Integer.valueOf(Label.CLOCT14I2P03))+'%\''; //Label.CLOCT14I2P03=8
                }
                crs = database.query(queryStr);
            //}
            /*      System.debug('crs:'+crs);
                  if (crs!=null) {
                    System.debug('crs.size():'+crs.size());
                  }
            List<Problem__c> prbl1=[select id,name from Problem__c limit 5];
    
                  System.debug('prbl1:'+prbl1);
                  if (prbl1!=null) {
                    System.debug('prbl1.size():'+prbl1.size());
                  }
            */
            
           if (crs==null || crs.size()==0) {
               noProblems=true;
           } else {
               crsWrapRes1=new List<CRWrap>();
               problemIds=new Set<Id>();
               for (CommercialReference__c cr:crs){
                 if (!problemIds.contains(cr.Problem__c)) {
                     problemIds.add(cr.Problem__c);
                     crsWrapRes1.add(new CRWrap(cr));
                 }
               }
               crsWrapRes1.sort();
               Integer limitSet=Integer.valueOf(Label.CLOCT14I2P21);
               limitToDisplay=limitSet;
               displayLinkToReport=false;
        
            if (crsWrapRes1.size()>limitSet){
                if (crsWrapRes1[limitSet].cr.Problem__r.Severity__c.equals(Label.CLOCT14I2P22)){ //Checking next one after limitSet
                    for (Integer i=limitSet;i<crsWrapRes1.size();i++){
                        if (!crsWrapRes1[i].cr.Problem__r.Severity__c.equals(Label.CLOCT14I2P22)) {
                            limitToDisplay=i;
                            break;
                        }
                    }
                }
                if (crsWrapRes1.size()>limitToDisplay) {
                    displayLinkToReport=true;
                }
            }        
       }
    }
    }
    
    public PageReference gotoReport(){
        if (mode.equals(Label.CLOCT14I2P24)){ //Commercial Reference
            //return new PageReference('/'+Label.CLOCT14I2P26+'?pv0='+commRef+'&bool_filter=1 OR (2 AND NOT(2))');
            return new PageReference('/'+Label.CLOCT14I2P26+'?pv0='+commRef);
        }  else if (mode.equals(Label.CLOCT14I2P25)){ //Family
            //return new PageReference('/'+Label.CLOCT14I2P26+'?pv1='+gmrCode.substring(0,Integer.valueOf(Label.CLOCT14I2P03))+'&bool_filter=2 OR (1 AND NOT(1))');
            return new PageReference('/'+Label.CLOCT14I2P26+'?pv1='+gmrCode.substring(0,Integer.valueOf(Label.CLOCT14I2P03)));
            //    queryStr=queryStr+'gmrCode__c LIKE \''+gmrCode.substring(0,Integer.valueOf(Label.CLOCT14I2P03))+'%\''; //Label.CLOCT14I2P03=8
        }
        return null;
    }
    
    public PageReference goBack(){
        Map<String,String> passBackParams = System.CurrentPageReference().getparameters().clone();
        passBackParams.remove('GMRCode');
        passBackParams.remove('Mode');
        passBackParams.put('backPage','true');
        PageReference pageRef= Page.VFP_UnifiedSearch;
        pageRef.getParameters().putAll(passBackParams);
        return pageRef;
    }
    
    
    public void sortRCs(List<RMAShippingToAdmin__c> RMAList) //Family First
      {
          boolean swapped=true;
          integer j = 0;
          RMAShippingToAdmin__c tmp;
          while (swapped){
            swapped = false;
            j++;
            for (integer i = 0; i < RMAList.size() - j; i++) {
              if (getNumberForSortRC(RMAList[i]) > getNumberForSortRC(RMAList[i + 1])){
                    
                   //To Swap
                   tmp = RMAList[i];
                   RMAList[i] = RMAList[i + 1];
                   RMAList[i + 1] = tmp;
                   
                   swapped = true;
               }
            }
          }
      }
    
    public Integer getNumberForSortRC(RMAShippingToAdmin__c rc){
    if ( (rc.commercialReference__c==null || rc.commercialReference__c=='' ) && ( rc.Family__c!=null && rc.Family__c!='') ) return 1;  // TECH_Family__c CHANGED TO Family__c
    if (rc.commercialReference__c!=null && rc.commercialReference__c!='') return 2;
    return 3;
    }
      
      
    public list<RMAShippingToAdmin__c> getLocalRCs()
     {
        String strFamily;
        String strCommercialReference;
        string newPF = gmrCode;
        /*
        if (newPF!=null && mode.equals(Label.CLOCT14I2P25)) 
        {
         strFamily = newPF.substring(0,Integer.valueOf(Label.CLOCT14I2P03)); //Label.CLOCT14I2P03=8
        }
        else
        {
         strFamily = null;
         strCommercialReference=commRef;
        }
        */
         //
         strFamily = newPF.substring(0,Integer.valueOf(Label.CLOCT14I2P03)); //Label.CLOCT14I2P03=8
         if (mode.equals(Label.CLOCT14I2P24)){ //Commercial Reference
            strCommercialReference=commRef;
         }
         //
         /* FIX: 21st Nov 2014: START
         System.debug('UserInfo: '+UserInfo.getName());
         System.debug('UserInfo.getLocale(): '+UserInfo.getLocale());
         System.debug('UserInfo.getLocale().split(\'_\')[1]: '+UserInfo.getLocale().split('_')[1]);
    
         //FIX: 21st Nov 2014: END
         */
         
         //String strCountryCode = UserInfo.getLocale().split('_')[1];
         User user1 = [SELECT id, country__c from USER where id=:UserInfo.getUserId()];
         String strCountryCode = user1.country__c;
         
    //     RMAShippingToAdmin__c rma1 = [select id, name, country__c, country__r.name from RMAShippingToAdmin__c where id='a451700000000Dg'];
         if (strCountryCode==null) {
         /*debugVal='strCountryCode:'+strCountryCode+'\ncountry__r.name'+rma1.country__r.name+'\ncountry__c:'+rma1.country__c+'\nstrFamily:'+strFamily+
         '\nstrCommercialReference:'+strCommercialReference+'\nstrFamily:'+strFamily;*/
            strCountryCode='';
         }
        
         
         System.debug('strFamily'+strFamily);
    
         String strQuery = 'select id, RCFax__c, RCContactName__c,Country__r.name,Country__r.CountryCode__c,ContactEmail__c, RCStreetLocalLang__c,RCLocalAdditionalAddress__c,RCLocalCity__c,RCLocalCounty__c,ReturnCenter__c,RCPhone__c,RCAdditionalAddress__c, TECH_ProductFamilyGMRCode__c, RCPOBox__c, RCPOBoxZip__c,ProductFamily__r.TECH_PM0CodeInGMR__c, ProductFamily__r.name, RCCounty__c, RCStateProvince__c,ReturnCenter__r.RCStateProvince__c,Country__c,RCStreet__c,RCCity__c,RCZipCode__c, RCCountry__c,ReturnCenter__r.RCCountry__c,ReturnCenter__r.RCCountry__r.Name,ReturnCenter__r.Name,Capability__c,CommercialReference__c, Family__c,RCShippingAddress__c,Ruleactive__c,Comments__c from RMAShippingToAdmin__c where Ruleactive__c=True';  //TECH_Family__c is Removed. Thought to CHANGED TO Family__c 
    //Commented as we don't have commercialReference and Family in Text form. So, commented this code and added next line to query according to GMRCode.     
    //Uncommenting the code as now we are passing Commercial Reference in commRef.
    
    //if (mode.equals(Label.CLOCT14I2P24)){ //Commercial Reference // This will not execute for Comemrcial Reference
                if ( (strCommercialReference !=null) && (strFamily !=null ))
                   {
                   System.debug('strCommercialReference '+strCommercialReference );
                   strQuery = strQuery + ' AND ((((CommercialReference__c= \''+strCommercialReference+'\') OR ( ((CommercialReference__c=\'\') AND TECH_ProductFamilyGMRCode__c= \''+ strFamily +'\')))';             
                   }
                   else if ((strCommercialReference ==null ) && (strFamily ==null ))
                   {
                      strQuery = strQuery + ' AND ((((CommercialReference__c=\'\') AND (TECH_ProductFamilyGMRCode__c=\'\'))';
                   }
                 
                   else if ((strCommercialReference ==null ) && (strFamily !=null ))
                   {
                        System.debug('strFamily '+ strFamily );
                         strQuery = strQuery + ' AND (((CommercialReference__c=\'\') AND ( TECH_ProductFamilyGMRCode__c= \''+ strFamily +'\')';
                        
                   }
    //}               
                   //System.debug('strFamily '+ strFamily );
    //               System.debug('strFamily '+ strFamily );
                   //strQuery = strQuery + ' AND (('
                   strQuery = strQuery + ' AND ';
                   /*if (mode.equals(Label.CLOCT14I2P25)){ //Family  //See if this is not requried. This is added by Khushman.
                        strQuery = strQuery + '( TECH_GMRCode__c like \''+ strFamily +'%\') AND  '; // AND ( ( (TECH_GMRCode__c like '8BC6ZIEC%') //Added line to query according to GMRCode.     
                   }*/
                   //strQuery = strQuery +'(Country__c=\'\' OR Country__r.name=\''+strCountryCode+'\'))'; //AND  (Country__c='' OR Country__r.CountryCode__c='IN') )[Over of last, 1 left of last]  (Tech_GMRCode and Country/NoCountry)
                   strQuery = strQuery +'(Country__c=\'\' OR Country__r.CountryCode__c=\''+strCountryCode+'\'))'; //AND  (Country__c='' OR Country__r.CountryCode__c='IN') )[Over of last, 1 left of last]  (Tech_GMRCode and Country/NoCountry)
                   //strQuery =  strQuery + 'OR ( (Country__r.CountryCode__c=\''+strCountryCode+'\') AND (CommercialReference__c=\'\') AND (TECH_GMRCode__c=\'\')))'; //OR (   (Country__r.CountryCode__c='IN') AND (CommercialReference__c='') AND (TECH_GMRCode__c='')   ) )[1st over] //This is for Global
                   //strQuery =  strQuery + 'OR ( (Country__r.name=\''+strCountryCode+'\') AND (CommercialReference__c=\'\') AND (TECH_GMRCode__c=\'\')))'; //OR (   (Country__r.CountryCode__c='IN') AND (CommercialReference__c='') AND (TECH_GMRCode__c='')   ) )[1st over] //This is for Global
                   strQuery =  strQuery + 'OR ( (Country__r.CountryCode__c=\''+strCountryCode+'\') AND (CommercialReference__c=\'\') AND (TECH_GMRCode__c=\'\')))'; //OR (   (Country__r.CountryCode__c='IN') AND (CommercialReference__c='') AND (TECH_GMRCode__c='')   ) )[1st over] //This is for Global
                   strQuery =  strQuery + 'order by ReturnCenter__r.name';
     //            strQuery =  strQuery + 'limit ' + Label.CLOCT14I2P23; // Commented because have to know size of Result
    // strQuery =  strQuery + 'limit 0'; //Temp Remove this
                   system.debug('Local Rule'+ strQuery );
                   //debugVal=debugVal+'\nQuery:'+strQuery;
                   return database.query(strQuery);
     }
    
    Public  pagereference addProblemLinkedCase() { // This method is Used To add the Problem linked case
        
        PageReference pageRef;
        
        if(caseId!=null && caseId !='') // Identifying whether request is generated from Case
        {
            for(Id wrp : problemIds)
            {
                if(wrp==strHdn) {
                    System.debug('strHdn : ' + strHdn);
                    ProblemCaseLink__c PrbCase = new ProblemCaseLink__c(Case__c=caseId, Problem__c=wrp);
                    lstProblemLinkedCase.add(PrbCase);
                }    
            }
            
            if(lstProblemLinkedCase.size()>0)
            {
                try {
                    insert lstProblemLinkedCase;
                }
                catch(Exception e) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,e.getMessage()));   
                }
                
                pageRef = new PageReference('/'+caseId);
                pageRef.setRedirect(false);
                return pageRef;
            }
            
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, 'No Problems to be added.'));
                return null;
            }
        }
    
        return null;
    
    }
    
    public PageReference goBackToCase() {
        PageReference pageRef = new PageReference('/'+caseId);
        return pageRef;
    }
}