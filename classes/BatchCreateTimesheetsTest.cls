@isTest
private class BatchCreateTimesheetsTest {

    @isTest (seeAllData=false)
    static void test_CreateTimesheetsBatchJob() {
        
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        
        Test.startTest();

        // Schedule the test job
        String jobId = System.schedule('BatchCreateTimesheetsSchedule',
                        CRON_EXP, 
                        new BatchCreateTimesheetsSchedule());
         
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                            FROM CronTrigger WHERE id = :jobId];

        // Verify the expressions are the same
        System.assertEquals(CRON_EXP, ct.CronExpression);

        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);

        // Verify the next time the job will run
        System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
 
        Test.stopTest();
    }
    
    @isTest(seeAllData=true)
    static void test_CreateTimesheetsBatchJob2() {
    
        Integer daysFromToday = 500;
        Date dateToCheck = Date.today().addDays(daysFromToday);
        
        SVMXC__Service_Group__c team = new SVMXC__Service_Group__c(Name = 'test');       
        insert team;
        
        user userObj = Utils_TestMethods.createStandardUser('test');
        insert userObj;
        
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = team.Id, 
                                                                                   SVMXC__Salesforce_User__c = userObj.Id);       
        insert tech;
        
        SVMXC__Service_Group_Members__c tech2 = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = team.Id, 
                                                                                   Manager__c = userObj.Id); 
                                                                                   
        insert tech2; 
           
        List<SVMXC_Timesheet__c> timesheetsList = [SELECT Technician__c FROM SVMXC_Timesheet__c 
                                                  WHERE Start_Date__c <= :dateToCheck AND End_Date__c >=:dateToCheck
                                                  AND Technician__c =: tech.Id];
        
        System.assertEquals(0, timesheetsList.size());        
        
        Test.startTest();
        
        BatchCreateTimesheets batch = new BatchCreateTimesheets();
    
        String query = 'SELECT Id, Manager__r.ProfileId, ';    
        query += 'SVMXC__Salesforce_User__r.ProfileId ';
        query += 'FROM SVMXC__Service_Group_Members__c ';
        query += 'WHERE SVMXC__Active__c = true ';
        query += 'AND SVMXC__Salesforce_User__r.IsActive = true ';
        
        if (System.label.SVMX_TimeSheet_Record_Type_Ids != 'None Specified')
            query += 'AND RecordTypeId =\'' + System.label.SVMX_TimeSheet_Record_Type_Ids + '\' ';
    
        query += ' LIMIT 100';
    
        batch.query = query;
        batch.daysFromToday = daysFromToday;

        System.debug('#### query : ' + query);
        Database.executeBatch(batch);

        Test.stopTest();
        
        //List<SVMXC_Timesheet__c> timesheetsList2 = [SELECT Technician__c FROM SVMXC_Timesheet__c 
        //                                          WHERE Start_Date__c <= :dateToCheck AND End_Date__c >=:dateToCheck
        //                                          AND Technician__c =: tech.Id];
       
        //System.assertEquals(1, timesheetsList2.size());
    }
}