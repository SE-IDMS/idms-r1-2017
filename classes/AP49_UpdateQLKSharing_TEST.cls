/*
    Author:Shruti Karn
    Description:This test class is for the the apex class AP49_UpdateQLKSharing
    Created Date: 02-02-2012
    */
    
    @isTest
    private class AP49_UpdateQLKSharing_TEST{
        static Account accounts;
        static Opportunity opp, opp1;
        static list<opportunity> lstOpp = new list<opportunity>();
        static list<OPP_QuoteLink__c> quoteLink = new list<OPP_QuoteLink__c >();
        static list<OPP_QuoteLink__c> quoteLink1 = new list<OPP_QuoteLink__c >();
        static User newuser;
        static
        {
            accounts = Utils_TestMethods.createAccount();
            Database.insert(accounts); 
            
                          
            opp= Utils_TestMethods.createOpportunity(accounts.id);
            Database.insert(opp);   
                      
            opp1= Utils_TestMethods.createOpportunity(accounts.id);
            Database.insert(opp1);  
            
            
            lstOpp.add(opp);
            
            
            for(integer i=1;i<=200;i++)
            {
            OPP_QuoteLink__c newQuoteLink=Utils_TestMethods.createQuoteLink(opp.id);
            quoteLink.add(newQuoteLink);
            }
            
    
            for(integer i=1;i<=200;i++)
            {
            OPP_QuoteLink__c newQuoteLink=Utils_TestMethods.createQuoteLink(opp1.id);
            quoteLink1.add(newQuoteLink);
            }
            
            
            Database.insert(quoteLink);
            // Profile profile = [select id from profile where name='SE - Non-sales RITE Approver'];    
            // newuser = Utils_TestMethods.createStandardUser(profile.id,'Users');
            // Database.insert(newuser );
            User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
            sysadmin2.isActive=true;
            System.runAs(sysadmin2){
                UserandPermissionSets newuseruserandpermissionsetrecord=new UserandPermissionSets('Users','SE - Non-sales RITE Approver');
                newuser = newuseruserandpermissionsetrecord.userrecord;
                Database.insert(newuser);
                }
            
        }
        static testMethod void UpdateQLKSharing_TestMethod()
        {
            test.StartTest();      
            
            opp1.ownerId =  newuser.Id;
            database.update(opp1);  
            
            list<opportunity> lstOpp1 = new list<opportunity>();
            lstOpp1.add(opp1);
            
            AP49_UpdateQLKSharing.findUpdatedOppLeaders(lstOpp,lstOpp1);
            AP49_UpdateQLKSharing.findOppLeaders(quoteLink,quoteLink1);
            test.StopTest();   
            //
            //AP49_UpdateQLKSharing test1 = new AP49_UpdateQLKSharing();
            //test1.findUpdatedOppLeaders(lstOpp,lstOpp1);
        }
    }