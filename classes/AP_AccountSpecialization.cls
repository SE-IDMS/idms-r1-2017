/********************************************************************************************************************
    Created By : Srinivas Nallapati / Shruti Karn
    Description : For October 13 PRM Release:
                 1. To add Assessment to Account Specialization.
       
********************************************************************************************************************/
Public Class AP_AccountSpecialization
{
    public static void createChildAccountSpecializationRequirements(List<AccountSpecialization__c> lstAccSpes)
    {   
        try
        {
            set<id> speIds = new set<id>();
            for(AccountSpecialization__c asp : lstAccSpes)
            {
                speIds.add(asp.Specialization__c);
            }
            
            List<SpecializationRequirement__c> lstSpeReq = [SELECT name,RequirementCatalog__r.RecordTypeId,Specialization__c from SpecializationRequirement__c where Specialization__c in:speIds and Active__c=true];
            map<id,List<SpecializationRequirement__c>> mpSpeIDToReqs = new map<id,List<SpecializationRequirement__c>>();
            for(SpecializationRequirement__c spr : lstSpeReq)
            {
                if(!mpSpeIDToReqs.containsKey(spr.Specialization__c))
                    mpSpeIDToReqs.put(spr.Specialization__c, new list<SpecializationRequirement__c>{(spr)});
                else
                    mpSpeIDToReqs.get(spr.Specialization__c).add(spr);
            }

            List<AccountSpecializationRequirement__c> lstAccSpeReqsToInsert = new List<AccountSpecializationRequirement__c>();
            for(AccountSpecialization__c asp : lstAccSpes)
            {
                if(mpSpeIDToReqs.containsKey(asp.Specialization__c))
                {
                    for(SpecializationRequirement__c spreq : mpSpeIDToReqs.get(asp.Specialization__c))
                    {
                        AccountSpecializationRequirement__c aspreq = new AccountSpecializationRequirement__c();
                        aspreq.SpecializationRequirement__c = spreq.id;
                        aspreq.AccountSpecialization__c = asp.id;
                        aspreq.status__c = 'Completed'; 
                        //TODO
                        aspreq.recordtypeid = CS_RequirementRecordType__c.getValues(spreq.RequirementCatalog__r.RecordTypeId).ASPRRecordTypeID__c;
                            

                        lstAccSpeReqsToInsert.add(aspreq);
                    }
                }   
            }
            if(!lstAccSpeReqsToInsert.isEmpty())
            {
               Database.SaveResult[] SaveResult1 = database.insert(lstAccSpeReqsToInsert,false);
               for(Integer i=0;i<SaveResult1.size();i++ )
               {
                   Database.SaveResult sr =SaveResult1[i];
                   if(!sr.isSuccess())
                   {
                      Database.Error err = sr.getErrors()[0];
                      lstAccSpes[0].addError( err.getMessage());               
                   }
               }
            }
        }catch(Exception e)
        {
            system.debug('Exception while creating Account Specialization and related records:'+e.getMessage());
            lstAccSpes [0].addError('An error occurred while creating Account Specialization .Please contact your system administrator');
        }
            

    }//End of method


    public static void reCreateNewChildAccountSpecializationRequirements(List<AccountSpecialization__c> lstAccountSpeUpdated)
    {
        List<AccountSpecializationRequirement__c> lstASReqsToDelete = [SELECT Id from AccountSpecializationRequirement__c WHERE AccountSpecialization__c in :lstAccountSpeUpdated];
        if(!lstASReqsToDelete.isEmpty())
        {
           Database.DeleteResult[] DelResult = database.delete(lstASReqsToDelete,false);
           for(Integer i=0;i<DelResult.size();i++ )
           {
               Database.DeleteResult sr =DelResult[i];
               if(!sr.isSuccess())
               {
                  Database.Error err = sr.getErrors()[0];
                  system.debug('err:'+err);
               }
           }
        } 
    }//End of method


    public static void deActivateAccountSpecializationRequirements(List<AccountSpecialization__c> lstAccountSpe)
    {
        List<AccountSpecializationRequirement__c> lstASReqsToInactivate = [SELECT Id from AccountSpecializationRequirement__c WHERE AccountSpecialization__c in :lstAccountSpe];
        for(AccountSpecializationRequirement__c aspreq : lstASReqsToInactivate)
        {
            aspreq.status__c = 'Inactive';

        }

        Database.SaveResult[] UpdateResult = database.update(lstASReqsToInactivate,false);
        for(Integer i=0;i<UpdateResult.size();i++ )
        {
            Database.SaveResult sr =UpdateResult[i];
            if(!sr.isSuccess())
            {
               Database.Error err = sr.getErrors()[0];
               system.debug('err:'+err);
            }
        }   
        
    }//End of method
    
    /****************************************************************************************************
         To create  Assessment for Account Specialization when:
            1. Assessment.PartnerProgram = Account Specialization.Partner PRogram
            2. Assessment.ProgramLevel  = Account Specialization.PRogramLevel
            3. Assessment.Automatic Assignment = true
    *****************************************************************************************************/
    public static void addAssessment(map<Id,set<Id>> mapAccountSpecialization)
    {
        list<Assessment__c> lstAssessment = new list<Assessment__c>();
        list<OpportunityAssessmentQuestion__c> lstQues = new list<OpportunityAssessmentQuestion__c>();
        list<OpportunityAssessmentResponse__c> lstResponse = new list<OpportunityAssessmentResponse__c>();
        map<Id,list<OpportunityAssessmentQuestion__c>> mapAssessmentQuestion = new map<Id,list<OpportunityAssessmentQuestion__c>>();
        list<PartnerAssessment__c> lstPartnerAssessment = new list<PartnerAssessment__c>();
        Savepoint sp;
        
        try
        {
            sp = Database.setSavepoint();
            lstAssessment = [Select id,Specialization__c from Assessment__c where specialization__C in :mapAccountSpecialization.keySet() and AutomaticAssignment__c =true limit 10000];
            lstQues = [Select id,assessment__c from OpportunityAssessmentQuestion__c where assessment__c in:lstAssessment limit 10000];
            for(OpportunityAssessmentQuestion__c ques : lstQues)
            {
                if(!mapAssessmentQuestion.containsKey(ques.assessment__c))
                    mapAssessmentQuestion.put(ques.assessment__c , new list<OpportunityAssessmentQuestion__c> {(ques)});
                else
                    mapAssessmentQuestion.get(ques.assessment__c).add(ques);
            }
            
             for(Assessment__c assessment : lstAssessment)
                {
                    if(mapAccountSpecialization.containsKey(assessment.Specialization__c))
                    {
                        for(Id acctSpeID : mapAccountSpecialization.get(assessment.Specialization__c))
                        {
                            PartnerAssessment__c newAssessment = new PartnerAssessment__c();
                            newAssessment.assessment__c = assessment.Id;
                            newAssessment.AccountSpecialization__c = acctSpeID;
                            newAssessment.recordtypeid = Label.CLOCT13PRM03;
                            lstPartnerAssessment.add(newAssessment);
                                               
                        }
                    }
                }
               
                if(!lstPartnerAssessment.isempty())
                    insert lstPartnerAssessment;
                for(PartnerAssessment__c partnerAssessment : lstPartnerAssessment)
                {
                    if(mapAssessmentQuestion.containsKey(partnerAssessment.assessment__c))
                    {
                        for(OpportunityAssessmentQuestion__c ques : mapAssessmentQuestion.get(partnerAssessment.assessment__c))
                        {
                            OpportunityAssessmentResponse__c response = new OpportunityAssessmentResponse__c();
                            response.partnerassessment__C = partnerAssessment.Id;
                            response.question__c = ques.Id;
                            lstResponse.add(response);
                        }
                    }
                }
                if(!lstResponse.isEmpty())
                    insert lstResponse;
           }
           catch(Exception e)
           {
               system.debug('Exception in addProgramAssessment:'+e.getmessage());
               database.rollback(sp);
           }
    }

}//End of class