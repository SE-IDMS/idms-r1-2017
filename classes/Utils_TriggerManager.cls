/**
 * Interface containing methods Trigger Handlers must implement 
 * 
 */
public interface Utils_TriggerManager
{
    /**
     * execute - responsible for launching execution of business logic.
     *
     */
    void execute();
    
    /**
     * onBeforeInsert
     *
     */
    void onBeforeInsert();
    
    /**
     * onBeforeUpdate
     *
     */
    void onBeforeUpdate();

    /**
     * onBeforeDelete
     *
     */
    void onBeforeDelete();

    /**
     * onAfterInsert
     *
     * Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     *
     */
    void onAfterInsert();

    /**
     * onAfterUpdate
     *
     */
    void onAfterUpdate();

    /**
     * onAfterDelete
     *
     */
    void onAfterDelete();
}