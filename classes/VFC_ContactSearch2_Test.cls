/*
 * Author:Sid
 * purpose:Test class for the Controller VFC_ContactSearch2  
*/
@isTest
private class VFC_ContactSearch2_Test {
    static testMethod void validateLinkToCase() {       
       Account accountrecord=Utils_TestMethods.createAccount();
       insert accountrecord; 
       Contact contactrecord=Utils_TestMethods.createContact(accountrecord.Id, 'ContactSearchContactName');
       insert contactrecord;                 
       Case Caserecord=Utils_TestMethods.createCase(accountrecord.Id,contactrecord.Id,'New');
       insert Caserecord;
       System.assertEquals(Caserecord.ContactId,contactrecord.Id);
        //relink it with the new contact record.
       Contact newcontactrecord=Utils_TestMethods.createContact(accountrecord.Id, 'NewContactSearchContactName');
       insert newcontactrecord; 
       VFC_ContactSearch2 searchcontroller=new VFC_ContactSearch2();
       Caserecord.ContactId=newcontactrecord.Id;        
       System.debug(JSON.serialize(Caserecord));
       VFC_ContactSearch2.linktocase(JSON.serialize(Caserecord));
    }
}