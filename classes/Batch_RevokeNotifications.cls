global class Batch_RevokeNotifications implements Database.Batchable<sObject>,Schedulable{
    public String query;
    public static String CRON_EXP = '0 0 19 * * ? *'; // CRON JOB expression to run the batch job at 10PM everyday
    
    global Batch_RevokeNotifications(){
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
       if(query == null){
            //List<RecordType> recordTypeLst = new List<RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Notification' LIMIT 1]);
            query = 'SELECT Id, Name FROM SetupAlerts__c WHERE Status__c = \'Revoked\'';
        }
        System.debug('***Query ->'+query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SetupAlerts__c> scope){
       
       System.debug('**** Scope->'+scope);
        if(scope.size() > 0 && !scope.isEmpty())        
            AP_ManageNotificationHandler.revokeNotification(scope);
            
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
    global static String ScheduleIt () {
        Batch_RevokeNotifications batchRevokeNotifications = new Batch_RevokeNotifications();
        return System.schedule('Create Notification', CRON_EXP, batchRevokeNotifications);
    }
    
    global void execute(SchedulableContext sc) 
    {
        Batch_RevokeNotifications batchRevokeNotifications = new Batch_RevokeNotifications(); 
        Database.executebatch(batchRevokeNotifications);
    }
}