/**
 * @author Benjamin LAMOTTE
 * @date Creation 21/09/2015
 * @description Controller for the page ZuoraUpdateBillingAccount
 */
public with sharing class ZuoraUpdateBillingAccountController {

    //Billing Account Object
    public Zuora__CustomerAccount__c zuoraBillingAccount        {get;set;}
    public String batch                                         {get;set;}
    public String paymentTerm                                   {get;set;}
    public Boolean isError                                      {get;set;}
    public Boolean accountSuccess                               {get;set;}
    public Boolean showButtons                                  {get;set;}

    //public String companyCode {get;set;}
    //public String companyLanguage {get;set;}

    public String zuoraTemplateId                               {get;set;}
    public String zuoraCommunicationProfileId                   {get;set;}
    public String localERPCustomerID                            {get;set;}
    
    public String billToCountry                                 {get;set;}
    public String billToState                                   {get;set;}
    public String soldToCountry                                 {get;set;}
    public String soldToState                                   {get;set;}

    //Bill To and Sold To Contact
    public ZuoraContact zuoraBillTo {get;set;}
    public ZuoraContact zuoraSoldTo {get;set;}
    
    private ZuoraContact zuoraOldBillTo;
    private ZuoraContact zuoraOldSoldTo;

    //Zuora API
    private Zuora.zApi zAPI;

    //Other
    public String paymentMethodOther = ((PaymentMethodCS__c.getInstance(Label.lbl_PaymentMethod) != null)?(PaymentMethodCS__c.getInstance(Label.lbl_PaymentMethod).Zuora_Payment_id__c):(''));
    //SEPA
    public String paymentMethodSEPA = ((PaymentMethodCS__c.getInstance(Label.lbl_PaymentMethod_SEPA) != null)?(PaymentMethodCS__c.getInstance(Label.lbl_PaymentMethod_SEPA).Zuora_Payment_id__c):(''));
    //Wire Transfer
    public String paymentMethodWireTransfer = ((PaymentMethodCS__c.getInstance(Label.lbl_PaymentMethod_WireTransfer) != null)?(PaymentMethodCS__c.getInstance(Label.lbl_PaymentMethod_WireTransfer).Zuora_Payment_id__c):(''));    
    
    /**
     * @author Benjamin LAMOTTE
     * @date Creation 21/09/2015
     * @description Constructor, initialize data of Billing Account and contacts
     * @param standardController : Standard controller of Billing Account object
     */
    public ZuoraUpdateBillingAccountController(Apexpages.Standardcontroller stdController) {
        //try {
            showButtons = true;
            //Query the concerned Billing Account
            zuoraBillingAccount = (Zuora__CustomerAccount__c) stdController.getRecord();

            zuoraBillingAccount = [SELECT Id,   Zuora__Zuora_Id__c, Country__c, CountrySoldTo__c, Name, Zuora__Account__c, Zuora__Account__r.Name, Zuora__AccountNumber__c, Zuora__Status__c, Zuora__BillToName__c,
                                    Zuora__BillToId__c, Zuora__SoldToId__c, Zuora__SoldToName__c, Zuora__PaymentTerm__c, 
                                    Zuora__AutoPay__c,  Zuora__DefaultPaymentMethod__c, Zuora__BillCycleDay__c, Zuora__Currency__c, 
                                    Zuora__BillToWorkPhone__c, Zuora__BillToWorkEmail__c, Zuora__LastInvoiceDate__c, Zuora__Batch__c, 
                                    Zuora__BillToAddress1__c, Zuora__BillToAddress2__c, Zuora__BillToCity__c, Zuora__BillToState__c, StateProv__c, StateProvSoldTo__c,
                                    Zuora__BillToPostalCode__c, Zuora__BillToCountry__c, ImmediateInvoiceAccount__c, InvoiceOwner__c,
                                    VATNumber__c, CompanyCode__c, ReferenceName__c, CodiceFiscale__c, CustomerCompanyName__c, CompanyLanguage__c, LocalERPCustomerID__c
                                    FROM Zuora__CustomerAccount__c WHERE Id =: zuoraBillingAccount.Id];
            
            System.debug('zuoraBillingAccount     ' + zuoraBillingAccount.Zuora__BillToCountry__c + '   ' + zuoraBillingAccount.Zuora__BillToState__c + ' z id   ' +  zuoraBillingAccount.Zuora__Zuora_Id__c);
            
            paymentTerm = zuoraBillingAccount.Zuora__PaymentTerm__c;
            batch = zuoraBillingAccount.Zuora__Batch__c;

            localERPCustomerID = zuoraBillingAccount.LocalERPCustomerID__c;

            //Initialize the Bill To Contact
            zuoraBillTo = new ZuoraContact();
            zuoraBillTo.Id = zuoraBillingAccount.Zuora__BillToId__c;
          

            billToState = zuoraBillingAccount.Zuora__BillToState__c;
            billToCountry = zuoraBillingAccount.Zuora__BillToCountry__c;

            if (!String.isBlank( getStateIdByName(zuoraBillingAccount.Zuora__BillToState__c)))
                zuoraBillingAccount.StateProv__c = getStateIdByName(zuoraBillingAccount.Zuora__BillToState__c);
            if (!String.isBlank( getCountryIdByName(zuoraBillingAccount.Zuora__BillToCountry__c)))
                zuoraBillingAccount.Country__c = getCountryIdByName(zuoraBillingAccount.Zuora__BillToCountry__c);

        
            /*if(zuoraBillingAccount.Zuora__BillToState__c != null || !zuoraBillingAccount.Zuora__BillToState__c.equals('')){
                zuoraBillingAccount.StateProv__c = getStateIdByName(zuoraBillingAccount.Zuora__BillToState__c); 
            }
            
            System.debug('IDDDDD ' + getStateIdByName(zuoraBillingAccount.Zuora__BillToState__c) + ' ' + zuoraBillingAccount.StateProv__c);
            
            if(zuoraBillingAccount.Zuora__BillToCountry__c != null || zuoraBillingAccount.Zuora__BillToCountry__c != ''){
                zuoraBillingAccount.Country__c = getCountryIdByName(zuoraBillingAccount.Zuora__BillToCountry__c);
            }*/
            
            //Split the Bill To Name into First and Last Name
            List<String> listBillToName = zuoraBillingAccount.Zuora__BillToName__c.split(' ');
            zuoraBillTo.LastName = listBillToName.get(listBillToName.size()-1);
            listBillToName.remove(listBillToName.size()-1);
            zuoraBillTo.FirstName = String.join(listBillToName, ' ');

            zuoraBillTo.WorkEmail = zuoraBillingAccount.Zuora__BillToWorkEmail__c;
            zuoraBillTo.WorkPhone = zuoraBillingAccount.Zuora__BillToWorkPhone__c;
            zuoraBillTo.Address1 = zuoraBillingAccount.Zuora__BillToAddress1__c;
            zuoraBillTo.Address2 = zuoraBillingAccount.Zuora__BillToAddress2__c;
            zuoraBillTo.City = zuoraBillingAccount.Zuora__BillToCity__c;
            zuoraBillTo.PostalCode = zuoraBillingAccount.Zuora__BillToPostalCode__c;
            zuoraBillTo.State = zuoraBillingAccount.Zuora__BillToState__c;
            zuoraBillTo.Country = zuoraBillingAccount.Zuora__BillToCountry__c;
            
            
            
            
            zuoraBillingAccount.CompanyLanguage__c = this.zuoraBillingAccount.CompanyLanguage__c;
            
            zuoraOldBillTo = new ZuoraContact(zuoraBillTo);

            //Initialize the Sold To Contact
            // Zuora connection
            zAPI = ZuoraUtilities.zuoraApiAccess();

            //Get Sold To data from Zuora
            String query = 'SELECT Id,  FirstName, LastName, WorkEmail, WorkPhone,';
            query += ' Address1, Address2, City, PostalCode, State, Country ';
            query += ' FROM Contact WHERE ';
            query += ' Id = \'' + zuoraBillingAccount.Zuora__SoldToId__c + '\' ';

            List<Zuora.zObject> listZuoraContact = ZuoraUtilities.queryToZuora(zAPI, query);
            System.debug('####### zResult :  '+ listZuoraContact);
            Zuora.zObject zuoraContact =  listZuoraContact.get(0);

            zuoraSoldTo = new ZuoraContact();
            zuoraSoldTo.Id = (String) zuoraContact.getValue('Id');
            zuoraSoldTo.FirstName = (String) zuoraContact.getValue('FirstName');
            zuoraSoldTo.LastName = (String) zuoraContact.getValue('LastName');
            zuoraSoldTo.WorkEmail = (String) zuoraContact.getValue('WorkEmail');
            zuoraSoldTo.WorkPhone = (String) zuoraContact.getValue('WorkPhone');
            zuoraSoldTo.Address1 = (String) zuoraContact.getValue('Address1');
            zuoraSoldTo.Address2 = (String) zuoraContact.getValue('Address2');
            zuoraSoldTo.City = (String) zuoraContact.getValue('City');
            zuoraSoldTo.PostalCode = (String) zuoraContact.getValue('PostalCode');

            zuoraSoldTo.State = (String) zuoraContact.getValue('State'); 
            zuoraSoldTo.Country = (String) zuoraContact.getValue('Country');

            if (!String.isBlank( getStateIdByName( (String) zuoraContact.getValue('State'))))
                zuoraBillingAccount.StateProvSoldTo__c = getStateIdByName( (String) zuoraContact.getValue('State'));    
            
            if (!String.isBlank( getCountryIdByName( (String) zuoraContact.getValue('Country'))))
                zuoraBillingAccount.CountrySoldTo__c = getCountryIdByName( (String) zuoraContact.getValue('Country'));
            
             /*if(zuoraBillingAccount.CountrySoldTo__c != null){
                soldToCountry = getCountryNameById(zuoraBillingAccount.CountrySoldTo__c);
            } else{
                soldToCountry = zuoraSoldTo.Country;
            }
            if(zuoraBillingAccount.StateProvSoldTo__c != null){
                soldToState = getStateNameById(zuoraBillingAccount.StateProvSoldTo__c);
            } else{
                soldToState = zuoraSoldTo.State;
            }*/
            
            System.debug('SOLD TO STATE ----------- ' + zuoraSoldTo.State);

            zuoraOldSoldTo = new ZuoraContact(zuoraSoldTo);

        /*} catch(Exception e){
            appendMessage(ApexPages.Severity.FATAL, 'Technical error :'+e);
        }*/
    }
    
    //Nikita 22/11/2016
    private String getStateNameById(Id stateProvId){
        List<StateProvince__c> stateProvList = [SELECT ID, Name, StateProvinceCode__c FROM StateProvince__c WHERE Id = :stateProvId LIMIT 1];
        if(stateProvList .Size() > 0){
            return stateProvList [0].Name;
        }
        return '';
    }
    
    //Nikita 22/11/2016
    private String getCountryNameById(Id countryId){
        List<Country__c> CountryList = [SELECT ID, Name, CountryCode__c FROM Country__c WHERE ID = :countryId LIMIT 1];
        if(CountryList.Size() > 0){
            return CountryList[0].Name;
        }
        return '';
    }
    
    /**
     * @author Benjamin LAMOTTE
     * @date Creation 21/09/2015
     * @description Update the billing account and contacts in Zuora
     */
    public PageReference updateBilligAccount(){
        try{
            //Check if user can edit the billing account
            List<UserRecordAccess> userAccess = [SELECT RecordId,HasEditAccess FROM UserRecordAccess WHERE RecordId=:zuoraBillingAccount.Id and UserId=:UserInfo.getUserId()];
            if(userAccess.size()>0 && userAccess[0].HasEditAccess){
                List<Zuora.zObject> listContactToUpdate = new List<Zuora.zObject>();
                Boolean isBillToUpdate = false;
                Boolean isSoldToUpdate = false;
                accountSuccess = false;
                
                //If updates in Bill To, synchronize it with Zuora
                
                if(zuoraBillingAccount.CompanyLanguage__c==null && zuoraBillingAccount.CompanyCode__c==null){
                    ApexPages.Message fieldErrorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please ensure Company Language and Company Code are selected');
                    ApexPages.addMessage(fieldErrorMsg);
                } else if(zuoraBillingAccount.CompanyLanguage__c!=null && zuoraBillingAccount.CompanyCode__c==null){
                    ApexPages.Message fieldErrorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please ensure Company Code is selected');
                    ApexPages.addMessage(fieldErrorMsg);
                } else if(zuoraBillingAccount.CompanyLanguage__c==null && zuoraBillingAccount.CompanyCode__c!=null){
                    ApexPages.Message fieldErrorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please ensure Company Language is selected');
                    ApexPages.addMessage(fieldErrorMsg);
                } else{
                    if(!zuoraBillTo.equals(zuoraOldBillTo)){
                        //Bill To Contact
                        Zuora.zObject contactBillToUpdate = new Zuora.zObject('Contact');
                        contactBillToUpdate.setValue('Id', zuoraBillTo.Id);
                        contactBillToUpdate.setValue('FirstName', zuoraBillTo.FirstName);
                        contactBillToUpdate.setValue('LastName', zuoraBillTo.LastName);
                        contactBillToUpdate.setValue('WorkEmail', zuoraBillTo.WorkEmail);
                        contactBillToUpdate.setValue('WorkPhone', zuoraBillTo.WorkPhone);
                        
                        contactBillToUpdate.setValue('Address1', zuoraBillTo.Address1);
                        contactBillToUpdate.setValue('Address2', zuoraBillTo.Address2);
                        contactBillToUpdate.setValue('City', zuoraBillTo.City);
                        contactBillToUpdate.setValue('PostalCode', zuoraBillTo.PostalCode);
                        contactBillToUpdate.setValue('State', getStateNameById(this.zuoraBillingAccount.StateProv__c));
                        contactBillToUpdate.setValue('Country', getCountryNameById(this.zuoraBillingAccount.Country__c));
                        
                        listContactToUpdate.add(contactBillToUpdate);
                        isBillToUpdate = true;
                        
                        if(isBillToUpdate==true){
                            //append
                        }
                    }
                    
                    
                    //If updates in Sold To, synchronize it with Zuora
                    if(!zuoraSoldTo.equals(zuoraOldSoldTo)){
                        //Sold To Contact
                        Zuora.zObject contactSoldToUpdate = new Zuora.zObject('Contact');
                        contactSoldToUpdate.setValue('Id', zuoraSoldTo.Id);
                        contactSoldToUpdate.setValue('FirstName', zuoraSoldTo.FirstName);
                        contactSoldToUpdate.setValue('LastName', zuoraSoldTo.LastName);
                        contactSoldToUpdate.setValue('WorkEmail', zuoraSoldTo.WorkEmail);
                        contactSoldToUpdate.setValue('WorkPhone', zuoraSoldTo.WorkPhone);
                        
                        contactSoldToUpdate.setValue('Address1', zuoraSoldTo.Address1);
                        contactSoldToUpdate.setValue('Address2', zuoraSoldTo.Address2);
                        contactSoldToUpdate.setValue('City', zuoraSoldTo.City);
                        contactSoldToUpdate.setValue('PostalCode', zuoraSoldTo.PostalCode);
                        //contactSoldToUpdate.setValue('State', soldToState);
                        contactSoldToUpdate.setValue('State', getStateNameById(this.zuoraBillingAccount.StateProvSoldTo__c));
                        //contactSoldToUpdate.setValue('Country', soldToCountry);
                        contactSoldToUpdate.setValue('Country', getCountryNameById(this.zuoraBillingAccount.CountrySoldTo__c));
                            
                        listContactToUpdate.add(contactSoldToUpdate);
                        isSoldToUpdate = true;
                    }
                    
                    //if there is an update, retrieve all Billing Accounts (except the one that is edited) for the SFDC Account
                    //and update matched contacts (same email and phone)
                    if(isBillToUpdate || isSoldToUpdate){
                        List<Zuora__CustomerAccount__c> listBillingAccount = [SELECT Zuora__Zuora_Id__c, Zuora__BillToId__c, Zuora__SoldToId__c 
                                                                              FROM  Zuora__CustomerAccount__c 
                                                                              WHERE Zuora__Account__c =: zuoraBillingAccount.Zuora__Account__c
                                                                              AND Id !=: zuoraBillingAccount.Id];
                        
                        String strConIdCriteriaBillTo = '';
                        String strConIdCriteriaSoldTo = '';
                        //If there is other Billing Account for the SFDC Account
                        //Retrieve all contacts (Bill and Sold To) from Zuora for these BA
                        //Update them if they matched the modified contact
                        if(listBillingAccount.size() > 0){
                            for(Zuora__CustomerAccount__c zAccount : listBillingAccount){
                                if(strConIdCriteriaBillTo != ''){
                                    strConIdCriteriaBillTo += ' OR ';
                                }
                                if(strConIdCriteriaSoldTo != ''){
                                    strConIdCriteriaSoldTo += ' OR ';
                                }
                                
                                strConIdCriteriaBillTo += ' Id = \'' + zAccount.Zuora__BillToId__c + '\'';
                                strConIdCriteriaSoldTo += ' Id = \'' + zAccount.Zuora__SoldToId__c + '\'';
                            }
                            
                            String strBillToIdCriteria = '';
                            //Criteria to exclude the Bill To Contact that is already updated
                            if(isBillToUpdate){
                                strBillToIdCriteria = ' AND Id != \'' + zuoraBillTo.Id + '\'';
                            }
                            
                            String stSoldToIdCriteria = '';
                            //Criteria to exclude the Sold To Contact that is already updated
                            if(isSoldToUpdate){
                                stSoldToIdCriteria = ' AND Id != \'' + zuoraSoldTo.Id + '\'';
                            }                   
                            
                            String queryBillTo = 'SELECT Id, WorkEmail, WorkPhone FROM Contact WHERE '+strConIdCriteriaBillTo+strBillToIdCriteria;
                            String querySoldTo = 'SELECT Id, WorkEmail, WorkPhone FROM Contact WHERE '+strConIdCriteriaSoldTo+stSoldToIdCriteria;
                            
                            List<Zuora.zObject> listAccountContactsBillTo = ZuoraUtilities.queryToZuora(zAPI, queryBillTo);
                            List<Zuora.zObject> listAccountContactsSoldTo = ZuoraUtilities.queryToZuora(zAPI, querySoldTo);
                            for(Zuora.zObject zContact : listAccountContactsBillTo){
                                zContact.setValue('WorkEmail', zContact.getValue('WorkEmail')==null?'':zContact.getValue('WorkEmail'));
                                zContact.setValue('WorkPhone', zContact.getValue('WorkPhone')==null?'':zContact.getValue('WorkPhone'));
                                
                                //If the Bill To Contact has been changed, update the contact if he matched
                                if(isBillToUpdate){
                                    Zuora.zObject contactToUpdate = new Zuora.zObject('Contact');
                                    contactToUpdate.setValue('Id', zContact.getValue('Id'));
                                    contactToUpdate.setValue('FirstName', zuoraBillTo.FirstName);
                                    contactToUpdate.setValue('LastName', zuoraBillTo.LastName);
                                    contactToUpdate.setValue('WorkEmail', zuoraBillTo.WorkEmail);
                                    contactToUpdate.setValue('WorkPhone', zuoraBillTo.WorkPhone);
                                    contactToUpdate.setValue('Address1', zuoraBillTo.Address1);
                                    contactToUpdate.setValue('Address2', zuoraBillTo.Address2);
                                    contactToUpdate.setValue('City', zuoraBillTo.City);
                                    contactToUpdate.setValue('PostalCode', zuoraBillTo.PostalCode);
                                    contactToUpdate.setValue('State', getStateNameById(this.zuoraBillingAccount.StateProv__c));
                                    contactToUpdate.setValue('Country', getCountryNameById(this.zuoraBillingAccount.Country__c));
                                    
                                    //System.debug('State if bill to update ' + zuoraBillTo.State);
                                    
                                    listContactToUpdate.add(contactToUpdate);
                                }
                            }
                            
                            for(Zuora.zObject zContact : listAccountContactsSoldTo){
                                //If the Sold To Contact has been changed, update the contact if he matched
                                if(isSoldToUpdate){
                                    Zuora.zObject contactToUpdate = new Zuora.zObject('Contact');
                                    contactToUpdate.setValue('Id', zContact.getValue('Id'));
                                    contactToUpdate.setValue('WorkEmail', zuoraSoldTo.WorkEmail);
                                    
                                    listContactToUpdate.add(contactToUpdate);
                                }
                            }
                            
                        }
                    }
                    
                    //Synchronize the Billing Account
                    Zuora.zObject accountToUpdate = new Zuora.zObject('Account');
                    initBillingAccountName();
                    accountToUpdate.setValue('Id', zuoraBillingAccount.Zuora__Zuora_Id__c);
                    accountToUpdate.setValue('Name', zuoraBillingAccount.Name);
                    accountToUpdate.setValue('PaymentTerm', paymentTerm);
                    accountToUpdate.setValue('InvoiceOwner__c', zuoraBillingAccount.InvoiceOwner__c);
                    accountToUpdate.setValue('ImmediateInvoiceAccount__c', zuoraBillingAccount.ImmediateInvoiceAccount__c);
                    accountToUpdate.setValue('Batch', this.batch);
                    
                    accountToUpdate.setValue('CompanyCode__c', zuoraBillingAccount.CompanyCode__c);
                    
                    accountToUpdate.setValue('InvoiceTemplateId', this.zuoraTemplateId);
                    accountToUpdate.setValue('CommunicationProfileId', this.zuoraCommunicationProfileId);
                    accountToUpdate.setValue('CodiceFiscale__c', zuoraBillingAccount.CodiceFiscale__c);
                    accountToUpdate.setValue('LocalERPCustomerID__c', zuoraBillingAccount.LocalERPCustomerID__c);
                    accountToUpdate.setValue('CompanyLanguage__c', zuoraBillingAccount.CompanyLanguage__c);
                    
                    System.debug('1 DEFAULT PAYMENT METHOD: ' + PaymentMethodCS__c.getInstance(zuoraBillingAccount.Zuora__DefaultPaymentMethod__c));
                    System.debug('1 ZBAcc DEFAULT PM '+zuoraBillingAccount.Zuora__DefaultPaymentMethod__c);
                    
                    
                    if (zuoraBillingAccount.Zuora__DefaultPaymentMethod__c== 'WireTransfer' || zuoraBillingAccount.Zuora__DefaultPaymentMethod__c == 'Wire Transfer') {
                        accountToUpdate.setValue('DefaultPaymentMethodId',paymentMethodWireTransfer);
                    } else if(zuoraBillingAccount.Zuora__DefaultPaymentMethod__c== 'Other') {
                        accountToUpdate.setValue('DefaultPaymentMethodId',paymentMethodOther);
                    } else {
                        ApexPages.Message paymentErrorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Payment method not managed (Currently just Other and Wire Transfer)');
                        ApexPages.addMessage(paymentErrorMsg);
                    }
                    
                    /*OLD code before nikita's fix
                    * 
                    * 
                    * if(PaymentMethodCS__c.getInstance(zuoraBillingAccount.Zuora__DefaultPaymentMethod__c)!=null){
                    accountToUpdate.setValue('DefaultPaymentMethodId',PaymentMethodCS__c.getInstance(zuoraBillingAccount.Zuora__DefaultPaymentMethod__c).Zuora_Payment_id__c);
                    System.debug('2 DEFAULT PAYMENT METHOD: ' + PaymentMethodCS__c.getInstance(zuoraBillingAccount.Zuora__DefaultPaymentMethod__c).Zuora_Payment_id__c);
                    sYStem.debug('2 ZBAcc DEFAULT PM '+zuoraBillingAccount.Zuora__DefaultPaymentMethod__c);
                    }*/
                    
                    isError = false;
                    
                    //Synchronize with Zuora
                    If(listContactToUpdate.size() > 0){
                        
                        for(Zuora.zObject contact : listContactToUpdate){
                            System.debug('Contacts to update in zuora ----------- ' + contact);
                        }
                        
                        List<Zuora.zApi.SaveResult> saveResultsList = ZuoraUtilities.updateZuoraObjects(zAPI, listContactToUpdate);     
                        
                        //Analyze result
                        List<String> listError;
                        for (Zuora.zApi.SaveResult saveResult : saveResultsList) {
                            listError = ZuoraUtilities.analyzeSaveResult(saveResult);
                            if (!listError.isEmpty()) {
                                for (Zuora.zApi.SaveResult result : saveResultsList){
                                    if (!result.Success){
                                        Zuora.zObject[] errors = result.errors;
                                        String errorText;
                                        for (Zuora.zObject error : errors) {
                                            errorText = errorText + (String)error.getValue('Code') + ': ' + (String)error.getValue('Message') + '\n';
                                        }
                                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Failure to update account: \n ' + errorText);
                                        ApexPages.addMessage(myMsg);
                                    }         
                                }
                                //appendMessage(ApexPages.Severity.ERROR, String.valueOf(listError));
                                isError = true;
                            }
                        }
                    }
                    
                    List<Zuora.zApi.SaveResult> saveResultsList = ZuoraUtilities.updateZuoraObjects(zAPI, new List<Zuora.zObject>{accountToUpdate}); 
                    //Analyze result
                    List<String> listError;
                    List<Zuora.zApi.SaveResult> errResults = new List<Zuora.zApi.SaveResult>();
                    for (Zuora.zApi.SaveResult saveResult : saveResultsList) {
                        listError = ZuoraUtilities.analyzeSaveResult(saveResult);
                        if (!listError.isEmpty()) {
                            appendMessage(ApexPages.Severity.ERROR, String.valueOf(listError));
                            isError = true;
                            Zuora.zApi.SaveResult res = new Zuora.zApi.SaveResult();
                            res.Success = false;
                            Zuora.zObject error = new Zuora.zObject('Error');
                            res.errors = new Zuora.zObject[]{error};
                                errResults = new List<Zuora.zApi.SaveResult>{};
                                    errResults.add(res);
                        }
                    }
                    
                    //If no error, redirect to the accout detail page
                    if(!isError){
                        zuoraBillingAccount.Zuora__Batch__c = this.batch;
                        zuoraBillingAccount.Zuora__PaymentTerm__c = paymentTerm;
                        //ZuoraBillingAccount.CountrySoldTo__c = getCountryIdByName(soldToCountry);
                        //ZuoraBillingAccount.StateProvSoldTo__c = getStateIdByName(soldToState);
                        appendMessage(ApexPages.Severity.CONFIRM, 'Successfully Edited Account.');
                        accountSuccess = true;
                        showButtons = false;
                        update zuoraBillingAccount;
                        //return redirectToAccount();
                    }
                }
                
                return null;
                
            }
            
            //User cannot edit the billing account
            else{
                isError = true;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'You don’t have sufficient privilege to edit this Billing Account');
                ApexPages.addMessage(myMsg);
                return null;
            }
        }
        
        // Error handling
        catch(Exception e){
            appendMessage(ApexPages.Severity.FATAL, 'Technical error : '+e);
            return null;
        }
    }

    /**
     * @author Benjamin LAMOTTE
     * @date Creation 21/09/2015
     * @description Add message to VF page
     */
    public static void appendMessage(ApexPages.Severity messageType, String message) {
        ApexPages.addMessage(new ApexPages.Message(messageType, message));
    }

    /**
     * @author Benjamin LAMOTTE
     * @date Creation 21/09/2015
     * @description Redirect to the Account detail page
     */
    public PageReference redirectToAccount() {
        PageReference pageReference = new PageReference('/'+zuoraBillingAccount.Id);
        pageReference.setRedirect(true);

        return null;
        //return pageReference;
        //redirection to previous page is handled in the page with js    
    }

    /**
     * @author Benjamin LAMOTTE
     * @date Creation 23/12/2015
     * @description Build payment terms picklist
     * @param List<SelectOption> : List of all options
     */
    public List<SelectOption> getPaymentTermOptions() {
        List<SelectOption> paymentTermOptions = new List<SelectOption>();
        paymentTermOptions.add(new SelectOption('Due Upon Receipt','Due Upon Receipt'));
        paymentTermOptions.add(new SelectOption('Net 30','Net 30'));
        paymentTermOptions.add(new SelectOption('Net 45','Net 45'));
        paymentTermOptions.add(new SelectOption('Net 60','Net 60'));

        return paymentTermOptions;
    }

    /**
     * @author Benjamin LAMOTTE
     * @date Creation 23/12/2015
     * @description Build batch picklist based on Immediate Invoice field
     * @param List<SelectOption> : List of all options
     */
    public List<SelectOption> getBatchOptions() {
        List<SelectOption> batchOptions = new List<SelectOption>();
        if(zuoraBillingAccount.ImmediateInvoiceAccount__c == 'Yes'){
           batchOptions.add(new SelectOption('Batch1','Batch1')); 
           batchOptions.add(new SelectOption('Batch4','Batch4')); 
           batchOptions.add(new SelectOption('Batch5','Batch5'));
        }
        else{
           batchOptions.add(new SelectOption('Batch2','MonthlyInternal'));
           batchOptions.add(new SelectOption('Batch3','MonthlyExternal'));   
        }
        return batchOptions;
    }

    /**
     * @author Benjamin LAMOTTE
     * @date Creation 23/12/2015
     * @description Build Country picklist values
     * @param List<SelectOption> : List of all options
     */
    public List<SelectOption> getCountryOptions() {
        List<SelectOption> countryOptions = new List<SelectOption>();
        List<Country__c> countryList = [SELECT ID, Name FROM Country__c ORDER BY Name];
        if(CountryList.Size() > 0){
            for(Country__c c : CountryList){
                countryOptions.add(new SelectOption(c.Name, c.Name));
            }
        }
        return countryOptions;
    }

    /**
     * @author Benjamin LAMOTTE
     * @date Creation 23/12/2015
     * @description Calculate Default Payment Method based on Invoice Owner field
     */
    public void initPaymentMethod(){
        if(zuoraBillingAccount.InvoiceOwner__c == 'Y'){
            zuoraBillingAccount.Zuora__DefaultPaymentMethod__c = 'WireTransfer'; //Label.lbl_PaymentMethod_WireTransfer;
        }
        else{
            zuoraBillingAccount.Zuora__DefaultPaymentMethod__c = 'Other'; //Label.lbl_PaymentMethod;
        }
    }

     /**
     * @author Benjamin LAMOTTE
     * @date Creation 28/04/2016
     * @description Calculate Billing Account Name based on Invoice Owner flag
     */
    public void initBillingAccountName(){
        if(zuoraBillingAccount.InvoiceOwner__c == 'Y'){
            System.debug('Defining name of account ');
           
            zuoraBillingAccount.Name = zuoraBillingAccount.Zuora__Account__r.Name.left(28)+'-'+zuoraBillingAccount.Zuora__DefaultPaymentMethod__c.left(2)+'-'+zuoraSoldTo.City.left(15)+'-'+getCountryCodeById(this.zuoraBillingAccount.CountrySoldTo__c);
            System.debug('name of account ' + zuoraBillingAccount.Name);
            System.debug('COUNTRY OF CONTACT' + zuoraSoldTo.Country);
        }
        
    }


   private String getCountryCodeById(Id countryId){
        List<Country__c> CountryList = [SELECT ID, Name, CountryCode__c FROM Country__c WHERE Id = :countryId LIMIT 1];
        if(CountryList.Size() > 0){
            return CountryList[0].CountryCode__c;
        }
        return '';
    }
    
    private String getCountryIdByName(String countryName){
        List<Country__c> CountryList = [SELECT ID, Name, CountryCode__c FROM Country__c WHERE Name = :countryName LIMIT 1];
        if(CountryList.Size() > 0){
            return CountryList[0].ID;
        }
        return '';
    }
    
    //Nikita 23/11/16
    private String getStateIdByName(String stateProvName){
        List<StateProvince__c> stateProvList = [SELECT ID, Name, StateProvinceCode__c FROM StateProvince__c WHERE Name = :stateProvName LIMIT 1];
        if(stateProvList .Size() > 0){
            return stateProvList [0].ID;
        }
        return '';
    } 
    
    public void refreshCommProfileAndTemplate(){
        String invTemplateQuery = 'SELECT ID,Country__c,zqu__ZuoraId__c FROM Zqu__InvoiceTemplate__c WHERE CompanyCode__c = \''+this.zuoraBillingAccount.CompanyCode__c+'\' AND CompanyLanguage__c = \''+this.zuoraBillingAccount.CompanyLanguage__c+'\' LIMIT 1';
        System.debug('###### INFO: invTemplateQuery = '+ invTemplateQuery);
        List<Zqu__InvoiceTemplate__c> zuoraTemplateIdList = Database.query(invTemplateQuery);
        //List<Zqu__InvoiceTemplate__c> zuoraTemplateIdList = [SELECT ID,Country__c,zqu__ZuoraId__c FROM Zqu__InvoiceTemplate__c WHERE Country__c = :this.currentSFAccount.Country__c  AND CompanyCode__c=:this.zuoraAccount.CompanyCode__c AND CompanyLanguage__c =:this.zuoraAccount.CompanyLanguage__c LIMIT 1];
        if(zuoraTemplateIdList.size()>0){
            this.zuoraTemplateId = zuoraTemplateIdList[0].zqu__ZuoraId__c;
        }

        String comProfileQuery = 'SELECT ID,Country__c,zqu__ZuoraId__c FROM Zqu__CommunicationProfile__c WHERE CompanyCode__c= \''+this.zuoraBillingAccount.CompanyCode__c+'\' AND CompanyLanguage__c = \''+this.zuoraBillingAccount.CompanyLanguage__c +'\' LIMIT 1';
        System.debug('###### INFO: comProfileQuery = '+ comProfileQuery);
        List<Zqu__CommunicationProfile__c> zuoraCommProfileList = Database.query(comProfileQuery);
        //List<Zqu__CommunicationProfile__c> zuoraCommProfileList = [SELECT ID,Country__c,zqu__ZuoraId__c FROM Zqu__CommunicationProfile__c WHERE Country__c = :this.currentSFAccount.Country__c AND CompanyCode__c=:this.zuoraAccount.CompanyCode__c AND CompanyLanguage__c =:this.zuoraAccount.CompanyLanguage__c LIMIT 1];
        if(zuoraCommProfileList.size()>0){
            this.zuoraCommunicationProfileId = zuoraCommProfileList[0].zqu__ZuoraId__c;
        }
    }
    

    /**
     * @author Benjamin LAMOTTE
     * @date Creation 21/09/2015
     * @description Wrapper class to represent a Zuora Contact
     */
    public class ZuoraContact {
        public String Id {get;set;}
        public String FirstName {get;set;}
        public String LastName {get;set;}
        public String WorkEmail {get;set;}
        public String WorkPhone {get;set;}
        public String Address1 {get;set;}
        public String Address2 {get;set;}
        public String City {get;set;}
        public String PostalCode {get;set;}
        public String State {get;set;}
        public String Country {get;set;}

        /**
         * @author Benjamin LAMOTTE
         * @date Creation 22/09/2015
         * @description Standard constructor
         */
        public ZuoraContact(){

        }

        /**
         * @author Benjamin LAMOTTE
         * @date Creation 22/09/2015
         * @description Constructor that copy a ZuoraContact
         * @param zContact : The Zuora Contact to copy
         */
        public ZuoraContact(ZuoraContact zContact){
            this.Id = zContact.Id;
            this.FirstName = zContact.FirstName==null?'':zContact.FirstName;
            this.LastName = zContact.LastName==null?'':zContact.LastName;
            this.WorkEmail = zContact.WorkEmail==null?'':zContact.WorkEmail;
            this.WorkPhone = zContact.WorkPhone==null?'':zContact.WorkPhone;
            this.Address1 = zContact.Address1==null?'':zContact.Address1;
            this.Address2 = zContact.Address2==null?'':zContact.Address2;
            this.City = zContact.City==null?'':zContact.City;
            this.PostalCode = zContact.PostalCode==null?'':zContact.PostalCode;
            this.State = zContact.State==null?'':zContact.State;
            this.Country = zContact.Country==null?'':zContact.Country;
        }

        /**
         * @author Benjamin LAMOTTE
         * @date Creation 22/09/2015
         * @description Test if the contact is equal to another contact
         * @param zContact : The Zuora Contact to compare
         * @param Boolean : True or false
         */ 
        public Boolean equals(ZuoraContact zContact) {
            Boolean isEqual = false;
            if(this.Id == zContact.Id && this.FirstName == zContact.FirstName && this.LastName == zContact.LastName
                && this.WorkEmail == zContact.WorkEmail && this.WorkPhone == zContact.WorkPhone
                && this.Address1 == zContact.Address1 && this.Address2 == zContact.Address2
                && this.City == zContact.City && this.PostalCode == zContact.PostalCode
                && this.State == zContact.State && this.Country == zContact.Country){
                isEqual = true;
            }

            return isEqual;
        }
    }
}