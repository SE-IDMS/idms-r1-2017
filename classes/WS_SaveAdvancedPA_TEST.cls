@istest

private class WS_SaveAdvancedPA_TEST{

    static testMethod void SaveAdvancedPA_test(){
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TestCountryPA'; 
        objCountry.CountryCode__c='PAA';
        insert objCountry;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        objContact.Country__c= objCountry.Id;
        insert objContact;
        
        Case objCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        insert objCase;
    
        WS_SaveAdvancedPA.PARequestDataBean  objPARequestDataBean = new WS_SaveAdvancedPA.PARequestDataBean();
        List<WS_SaveAdvancedPA.AdvancedPADataBean> lstAdvancedPADataBean = new List<WS_SaveAdvancedPA.AdvancedPADataBean>();
        List<WS_SaveAdvancedPA.AvailabilityDataBean> lstAvailabilityDataBean = new List<WS_SaveAdvancedPA.AvailabilityDataBean>();
        
        WS_SaveAdvancedPA.AdvancedPADataBean  objAdvancedPADataBean = new WS_SaveAdvancedPA.AdvancedPADataBean();
        WS_SaveAdvancedPA.AvailabilityDataBean  objAvailabilityDataBean = new WS_SaveAdvancedPA.AvailabilityDataBean();
        objAvailabilityDataBean.AvailableDate = System.Now();
        objAvailabilityDataBean.PromiseQuantity = 10;
        lstAvailabilityDataBean.add(objAvailabilityDataBean);
        objAdvancedPADataBean.availabilityDataBeanList = lstAvailabilityDataBean;
        objAdvancedPADataBean.PartNumber ='PART#2233';
        objAdvancedPADataBean.QuantityAsked  = 20;
        objAdvancedPADataBean.UnitNetPrice = '50';
        objAdvancedPADataBean.MinimumOrderQuantiy = 20;
        objAdvancedPADataBean.TotalNetPrice ='1000';
        objAdvancedPADataBean.NetPriceUnit =50;
        objAdvancedPADataBean.NetPriceCurrency ='INR';
        lstAdvancedPADataBean.add(objAdvancedPADataBean);
        objPARequestDataBean.advancedPADataBeanList=lstAdvancedPADataBean;
        WS_SaveAdvancedPA.ReturnDataBean objReturnDataBean = WS_SaveAdvancedPA.SaveAdvancedPA(objPARequestDataBean);
    }
}