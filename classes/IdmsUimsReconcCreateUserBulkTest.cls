@isTest
public class IdmsUimsReconcCreateUserBulkTest {
static testMethod void IdmsUimsReconcCreateUser(){
         
         IdmsUimsReconcCreateUserBulk idmsUimsReconcCreateUserBulkcls=new IdmsUimsReconcCreateUserBulk();   
         user userObj1 = new User(alias = 'user', email='test3127415' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                             timezonesidkey='Europe/London', username='test3127415' + '@accenture.com'+System.Label.CLJUN16IDMS71,Company_Postal_Code__c='12345',
                             Company_Phone_Number__c='9986995000',FederationIdentifier ='188b80b1-c622-4f20-8b1d-3ae86af11c4d',IDMS_Registration_Source__c = 'Test',
                             IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                             IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432', IDMS_VU_NEW__c = '1', IDMS_VU_OLD__c='1', IDMS_VC_NEW__c='1',IDMS_VC_OLD__c='1');
         insert userObj1;
         System.assertEquals(userObj1.email, 'test3127415' + '@accenture.com');
         System.assertEquals(userObj1.IDMS_User_Context__c , 'work');
         
         if(userObj1 != null){
         IDMSUserReconciliationBatchHandler__c handler=new IDMSUserReconciliationBatchHandler__c(AtCreate__c=true,AtUpdate__c=true,
                                                             HttpMessage__c='Test134',NbrAttempts__c=1,IdmsUser__c=userObj1.Id);
         insert handler; 
         
          System.assertEquals(handler.AtCreate__c, true);
        
         List<IDMSUserReconciliationBatchHandler__c> lstHandler=new List<IDMSUserReconciliationBatchHandler__c>();
         lstHandler.add(handler);
         String callFid = 'IDMSAdmin';
         
         try{
         Test.startTest();
             Test.setMock(WebServiceMock.class, new IdmsUsersResyncimplFlowServiceImsMock());
              Map<String, User> mapUimsEmailIdmsUser = new Map<String, User>();
              mapUimsEmailIdmsUser.put(userObj1.email, userObj1);
             IdmsUimsReconcCreateUserBulk.createUimsUsers(lstHandler, 'batchId');
           
         }
           catch(exception e) {} 
         Test.stopTest();
         }
         }
         
         
         static testMethod void IdmsUimsReconcCreateUser2(){
         
         IdmsUimsReconcCreateUserBulk idmsUimsReconcCreateUserBulkcls=new IdmsUimsReconcCreateUserBulk();   
         user userObj1 = new User(alias = 'user', email='test31274199' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                             timezonesidkey='Europe/London', username='test31274199' + '@accenture.com'+System.Label.CLJUN16IDMS71,Company_Postal_Code__c='12345',
                             Company_Phone_Number__c='9986995000',FederationIdentifier ='',IDMS_Registration_Source__c = 'Test',
                             IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                             IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432', IDMS_VU_NEW__c = '1', IDMS_VU_OLD__c='1', IDMS_VC_NEW__c='1',IDMS_VC_OLD__c='1');
         insert userObj1;
         System.assertEquals(userObj1.email, 'test31274199' + '@accenture.com');
         System.assertEquals(userObj1.IDMS_User_Context__c , 'work');
         
         if(userObj1 != null){
         IDMSUserReconciliationBatchHandler__c handler=new IDMSUserReconciliationBatchHandler__c(AtCreate__c=true,AtUpdate__c=true,
                                                             HttpMessage__c='Test134',NbrAttempts__c=1,IdmsUser__c=userObj1.Id);
         insert handler; 
         
          System.assertEquals(handler.AtCreate__c, true);
        
         List<IDMSUserReconciliationBatchHandler__c> lstHandler=new List<IDMSUserReconciliationBatchHandler__c>();
         lstHandler.add(handler);
         String callFid = 'IDMSAdmin';
         
         try{
         Test.startTest();
             Test.setMock(WebServiceMock.class, new IdmsUsersResyncimplFlowServiceImsMock());
              Map<String, User> mapUimsEmailIdmsUser = new Map<String, User>();
              mapUimsEmailIdmsUser.put(userObj1.email, userObj1);
             IdmsUimsReconcCreateUserBulk.createUimsUsers(null, 'batchId');
           
         }
           catch(exception e) {} 
         Test.stopTest();
         }
         }
         
         
}