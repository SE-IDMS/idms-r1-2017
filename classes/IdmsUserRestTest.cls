/**
Description: This covers IdmsUserRest class
**/
@isTest

public class IdmsUserRestTest{
    //Test 1: this is to test put operation of main rest class
    static testMethod void IdmsUserRestTestMethod() {
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/IDMSUser';  
        req.httpMethod = 'Put';
        RestContext.request= req;
        RestContext.response= res;
        
        User userObj = new User(alias = 'user', email='test363741' + '@cognizant.com', 
                                emailencodingkey='UTF-8', lastname='Testlast1',Firstname='Testfirst', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                timezonesidkey='Europe/London', username='testuser7542' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='tej4415',IDMS_Registration_Source__c = 'Test',
                                IDMS_User_Context__c = '@Work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432',IDMS_VC_NEW__c='testnew',IDMS_VC_OLD__c='testnew'); 
        insert userObj;
        
        String Boolean_Passwordsent='IDMS_SendEmail_PasswordRequestReSet';
        String Password ='4838284';
        String appid='3500832';
        String signature='testsig';
        test.Starttest();
        IdmsUserRest UserRest=new IdmsUserRest();
        IdmsUserRest.updateUser(userObj.Id);
        IDMSResponseWrapper responseWrapper=IdmsUserRest.doPut(userObj);
        IDMSResponseWrapper responseWrapper1=IdmsUserRest.doPost(userObj, Password);
        test.Stoptest();
        
    }
}