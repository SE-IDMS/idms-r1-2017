/*
Purpose: To populate the Family,GDP,documentation,sparepart,old product information on the cache fields of product
Author:Sid
*/
global class Batch_ProductCalculations implements Database.Batchable<sObject>,Schedulable {
    
   String query='select isDocumentationFormula__c,isMarketingDocumentation__c,isOldFormula__c,isOld__c,isSparePart__c,isSparePartFormula__c,ProductFamilyId__c,ProductFamily__c,productgdpgmr__c,GDPGMR__c, Cache_Family__c,FamilyName__c,Cache_ProductFamily__c,ProductFamilyName__c,Cache_ProductLine__c,ProductLine__c,Cache_BusinessUnit__c,BusinessUnit__c from Product2 where ProductGDP__c!=null';

    
    global Batch_ProductCalculations() {     
        if(!Boolean.valueOf(System.Label.CalculateCacheFieldsonProductsComplete))
         query += ' and GDPGMR__c=null';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
     
        List<Product2> tobeupdatedproducts=new List<Product2>();     
        for(Sobject tempsobject:scope)
        {
            Product2 productRecord=(Product2)tempsobject;
            productRecord.isMarketingDocumentation__c=productRecord.isDocumentationFormula__c;
            productRecord.isOld__c=productRecord.isOldFormula__c;           
           // productRecord.isSparePart__c=productRecord.isSparePartFormula__c;
            productRecord.ProductFamily__c=productRecord.ProductFamilyId__c;
            productRecord.GDPGMR__c=productRecord.productgdpgmr__c;
            productRecord.Cache_Family__c=productRecord.FamilyName__c;
            productRecord.Cache_ProductFamily__c=productRecord.ProductFamilyName__c;
            productRecord.Cache_ProductLine__c=productRecord.ProductLine__c;
            productRecord.Cache_BusinessUnit__c=productRecord.BusinessUnit__c;
            
            tobeupdatedproducts.add(productRecord);
        }   

    update tobeupdatedproducts;    
    
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    //Added for December 2014 release to be able to schedule this batch class
     global void execute(SchedulableContext sc)
    {
        Batch_ProductCalculations product2Updates = new Batch_ProductCalculations(); 
        database.executebatch(product2Updates);
    }
    
}