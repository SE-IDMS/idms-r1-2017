/**
Description: This covers the IDMS_UIMS_Mapping class
**/
@isTest
public class IDMS_UIMS_MappingTest{
    //Test 1: this method is used to create user in IDMS from UIMS table
    static testmethod void testCreateIDMSUserFromUIMS(){
        IDMS_UIMS_Mapping mapping = new IDMS_UIMS_Mapping() ; 
        UIMS_User__c uimsUser = new UIMS_User__c(EMail__c='test@test.com',FirstName__c='test',AddInfoAddress__c='test',FederatedID__c='1234-abc1234',Countrycode__c='IN',
                                                 County__c ='IN',JobDescription__c='SE',JobFunction__c='SE',JobTitle__c='Test',LocalityName__c='Test',MiddleName__c='M',
                                                 Cell__c='123456',PostOfficeBox__c='12345',LanguageCode__c='EN',Salutation__c='test',LastName__c='test',State__c='US',
                                                 Street__c='US',GoldenId__c='123'); 
        insert uimsUser;
        UIMS_Company__c uimsCompany = new UIMS_Company__c(OrganizationName__c= 'test',GoldenId__c='1234',CountryCode__c='IN',CurrencyCode__c='US',MarketSegment__c='ID5',
                                                          PostalCode__c='test',PostOfficeBox__c='1234',St__c='US',Street__c='US',TelephoneNumber__c='123456',HeadQuarter__c ='TRUE',
                                                          AddInfoAddress__c='Hello',PostalCity__c='Hello',WebSite__c='www.test.com',AnnualSales__c='1234',MarketServed__c='1234',
                                                          EmployeeSize__c='1234') ; 
        insert uimsCompany ;
        String contextHome = 'home'; 
        mapping.CreateIDMSUserFromUIMS(uimsUser,uimsCompany,contextHome);
        String contextWork = 'work';
        mapping.CreateIDMSUserFromUIMS(uimsUser,uimsCompany,contextWork);
        
        UIMS_User__c uimsUser1 = new UIMS_User__c(EMail__c='test@test.com',FirstName__c='test',AddInfoAddress__c='test',FederatedID__c='1234-abc1234',Countrycode__c='IN',
                                                  County__c ='IN',JobDescription__c='SE',JobFunction__c='SE',JobTitle__c='Test',LocalityName__c='Test',MiddleName__c='M',
                                                  Cell__c='123456',PostOfficeBox__c='12345',LanguageCode__c='EN',Salutation__c='test',LastName__c='test',State__c='US',
                                                  Street__c='US',GoldenId__c='123',Vnew__c='1'); 
        insert uimsUser1;
        UIMS_Company__c uimsCompany1 = new UIMS_Company__c(OrganizationName__c= 'test',GoldenId__c='1234',CountryCode__c='IN',CurrencyCode__c='US',MarketSegment__c='ID5',
                                                           PostalCode__c='test',PostOfficeBox__c='1234',St__c='US',Street__c='US',TelephoneNumber__c='123456',HeadQuarter__c ='TRUE',
                                                           AddInfoAddress__c='Hello',PostalCity__c='Hello',WebSite__c='www.test.com',AnnualSales__c='1234',MarketServed__c='1234',
                                                           EmployeeSize__c='1234',Vnew__c='1') ; 
        insert uimsCompany1 ;
        String contextHome1 = 'home'; 
        mapping.CreateIDMSUserFromUIMS(uimsUser1,uimsCompany1,contextHome1);
        String contextWork1 = 'work';
        mapping.CreateIDMSUserFromUIMS(uimsUser1,uimsCompany1,contextWork1);
        
    }
    //Test 2:test create the person account
    static testmethod void testCreatePersonAccount(){
        IDMS_UIMS_Mapping mapping = new IDMS_UIMS_Mapping() ; 
        User userCompany = new User(alias = 'userpass', email='userpass' + '@accenture.com', 
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                    BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                    timezonesidkey='Europe/London', username='userpass' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                                    Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                    IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                    IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',DefaultCurrencyIsoCode = '');
        
        insert userCompany ;
        mapping.CreatePersonAccount(userCompany) ; 
    }
    
    //Test 3:test enrich by company
    static testmethod void testenrichUserByCompany(){
        IDMS_UIMS_Mapping mapping = new IDMS_UIMS_Mapping() ; 
        User userWork = new User(alias = 'userwork', email='userwork' + '@accenture.com', 
                                 emailencodingkey='UTF-8', lastname='work', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                 BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='userwork' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                 IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                 IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        
        mapping.enrichUserByCompany('work',userWork ) ; 
        
    }
    //Test 4: test market segment
    static testmethod void testMargerSegment(){
        IDMS_UIMS_Mapping.getMarketSubSegmentMap();
    }
    
}