/**
 * @author: Nabil ZEGHACHE (not sure who actually created the class in the first place)
 * Date Created (YYYY-MM-DD): 2016-08-24
 * Test class: 
 * Description: This class manages the Business logic on Interest on Opportunity Notification object
 * **********
 * Scenarios:
 * **********
 * 1.  Set the amount and currency of the interest based on the relevant Interest Amounts records
 * 
 * -----------------------------------------------------------------
 *                     MODIFICATION HISTORY
 * -----------------------------------------------------------------
 * Modified By: Authors Name
 * Modified Date: Date
 * Description: Brief Description of Change + BR/Hotfix
 * -----------------------------------------------------------------
 */

@isTest
public class AP_OpportunityNotification_Test {
    
    static Country__c country1;
    static Country__c country2;
    
    static StateProvince__c stateCountry1_01;
    static StateProvince__c stateCountry1_02;
    static StateProvince__c stateCountry2_01;
    static StateProvince__c stateCountry2_02;
    
    static Account acc1;
    static Account acc2;
    
    static Contact contact1;
    static Contact contact2;
    
    static SVMXC__Site__c loc1;
    static SVMXC__Site__c loc2;
    
    static SVMXC__Territory__c territory1;
    
    static Case objCase;
    
    static SVMXC__Service_Order__c workOrder1;
    static SVMXC__Service_Order__c workOrder2;
    
    static InterestOnOpportunityNotif__c interest1_1;
    static InterestOnOpportunityNotif__c interest1_2;
    static InterestOnOpportunityNotif__c interest2_1;
    static InterestOnOpportunityNotif__c interest2_2;
    
    static InterestAmountOnOpportunityNotif__c interestAmount1;
    static InterestAmountOnOpportunityNotif__c interestAmount2;
    static InterestAmountOnOpportunityNotif__c interestAmount3;
    static InterestAmountOnOpportunityNotif__c interestAmount4;
    static InterestAmountOnOpportunityNotif__c interestAmount5;
    
    static OpportunityNotification__c opportunityNotif101;
    static OpportunityNotification__c opportunityNotif102;
    
    /*
     * Static call to initiate data needed within the test methods.
     */
    static {
 
        // Create countries
        List<Country__c> countryList = new List<Country__c>();

        country1 = Utils_TestMethods.createCountry();
        country1.name='France';
        country1.CountryCode__c = 'FR';
        countryList.add(country1);
        
        country2 = Utils_TestMethods.createCountry();
        country2.name='USA';
        country2.CountryCode__c = 'US';
        countryList.add(country2);
        System.debug('### Inserting countryList: '+countryList);
        insert countryList;
        System.debug('### Inserted countryList: '+countryList);
        
        // Create States/Provinces
        List<StateProvince__c> stateProvinceList = new List<StateProvince__c>();
        stateCountry1_01 = new StateProvince__c(Name='AP_OpportunityNotification_Test - FR - Paris', Country__c=country1.Id, CountryCode__c=country1.CountryCode__c, StateProvinceCode__c='75');
        stateProvinceList.add(stateCountry1_01);
        stateCountry1_02 = new StateProvince__c(Name='AP_OpportunityNotification_Test - FR - Hauts-de-Seine', Country__c=country1.Id, CountryCode__c=country1.CountryCode__c, StateProvinceCode__c='92');
        stateProvinceList.add(stateCountry1_02);
        stateCountry2_01 = new StateProvince__c(Name='AP_OpportunityNotification_Test - US - New York', Country__c=country2.Id, CountryCode__c=country2.CountryCode__c, StateProvinceCode__c='NY');
        stateProvinceList.add(stateCountry2_01);
        stateCountry2_02 = new StateProvince__c(Name='AP_OpportunityNotification_Test - US - California', Country__c=country2.Id, CountryCode__c=country2.CountryCode__c, StateProvinceCode__c='CA');
        stateProvinceList.add(stateCountry2_02);
        
        System.debug('### Inserting stateProvinceList: '+stateProvinceList);
        insert stateProvinceList;
        System.debug('### Inserted stateProvinceList: '+stateProvinceList);
        
        // Create Accounts
        List<Account> accountList = new List<Account>();

        acc1 = Utils_TestMethods.createAccount();
        acc1.Country__c = country1.id;
        acc1.PostalStateProvince__c = stateCountry1_01.Id;
        acc1.Name = 'Account FR 01';
        acc1.City__c = 'Paris';
        acc1.PostalStateProvince__c = stateCountry1_01.Id;
        acc1.StateProvince__c = stateCountry1_01.Id;
        acc1.BillingCity = acc1.City__c;
        
        accountList.add(acc1);
        
        acc2 = Utils_TestMethods.createAccount();
        acc2.Country__c = country2.id;
        acc2.Name = 'Account US 01';
        acc2.City__c = 'New York';
        acc2.PostalStateProvince__c = stateCountry2_01.Id;
        acc2.StateProvince__c = stateCountry2_01.Id;
        acc2.BillingCity = acc2.City__c;

        accountList.add(acc2);

        System.debug('### Inserting accountList: '+accountList);
        insert accountList;
        System.debug('### Inserted accountList: '+accountList);
        
        // Create Locations
        List<SVMXC__Site__c> locationList = new List<SVMXC__Site__c>();
        
        loc1 = Utils_TestMethods.createLocation(acc1.Id);
        loc1.Name = 'Account 1 - SITE';
        locationList.add(loc1);
        
        loc2 = Utils_TestMethods.createLocation(acc2.Id);
        loc2.Name = 'Account 2 - SITE';
        locationList.add(loc2);

        System.debug('### Inserting locationList: '+accountList);
        insert locationList;
        System.debug('### Inserted locationList: '+accountList);
        
        // Create Service Territory
        List<SVMXC__Territory__c> territoryList = new List<SVMXC__Territory__c>();

        territory1 = new SVMXC__Territory__c();
        territory1.name = 'AP_OpportunityNotification_Test - Territory 1';
        territory1.SVMXC__Active__c = true;
        territory1.SVMXC__Description__c = 'AP_OpportunityNotification_Test - Territory 1';
        territory1.SVMXC__Territory_Code__c = 'AP_OpportunityNotification_Test - Territory 1';
        territory1.ExternalId__c = 'AP_OpportunityNotification_Test - Territory 1';
        territoryList.add(territory1);
        
        System.debug('### Inserting territoryList: '+territoryList);
        insert territoryList;
        System.debug('### Inserted territoryList: '+territoryList);
        
        // Create contacts
        List<Contact> contactList = new List<Contact>();

        contact1 = Utils_TestMethods.createContact(acc1.Id, 'Contact 1');
//        System.debug('### contact1: '+contact1);
        contact1.Email = 'contact1@yopmail.com';
        contact1.WorkPhone__c = '+331234567890';
        contact1.AccountId = acc1.Id;
//        System.debug('### contact1: '+contact1);
        contactList.add(contact1);
        
        contact2 = Utils_TestMethods.createContact(acc2.Id, 'Contact 2');
//        System.debug('### contact2: '+contact2);
        contact2.Email = 'contact2@yopmail.com';
        contact2.WorkPhone__c = '+12345678901';
        contact2.AccountId = acc2.Id;
//        System.debug('### contact2: '+contact2);
        contactList.add(contact2);
        
//        insert contact1;
//        insert contact2;
        System.debug('### Inserting contactList: '+contactList);
        insert contactList;
        System.debug('### Inserted contactList: '+contactList);
        
        // Create Cases
        List<Case> caseList = new List<Case>();

        objCase = Utils_TestMethods.createCase(acc1.Id,contact1.Id,'Open');
        /* BR-4310 Commented By Vimal K (GD India)
        objCase.SupportCategory__c = Label.CL00408;
        objCase.Symptom__c = Label.CL00737;
        objCase.SubSymptom__c = Label.CL00742;
        */
        objCase.SupportCategory__c = Label.CL00737;
        caseList.add(objCase);
        
        System.debug('### Inserting caseList: '+caseList);
        insert caseList;
        System.debug('### Inserted caseList: '+caseList);
        
        // Create Work Orders
        List<SVMXC__Service_Order__c> workOrderList = new List<SVMXC__Service_Order__c>();
        
        workOrder1 =  Utils_TestMethods.createWorkOrder(acc1.id);
        workOrder1.Work_Order_Category__c='On-site';                 
        workOrder1.SVMXC__Order_Type__c='Maintenance';
        workOrder1.SVMXC__Problem_Description__c = 'BLALBLALA';
        workOrder1.SVMXC__Order_Status__c = 'UnScheduled';
        workOrder1.CustomerRequestedDate__c = Date.today();
        workOrder1.Service_Business_Unit__c = 'Energy';
        workOrder1.SVMXC__Priority__c = 'Normal-Medium';
        workOrder1.SendAlertNotification__c = true;
        workOrder1.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder1.CustomerTimeZone__c = 'Europe/Paris';
        
        workOrder1.BillToAccount__c = acc1.Id;
        workOrder1.SoldToAccount__c = acc1.Id;
        
        workOrder1.SVMXC__Company__c = acc1.Id;
        workOrder1.SVMXC__Site__c = loc1.Id;
        workOrder1.SVMXC__Contact__c = contact1.Id;
        
        workOrderList.add(workOrder1);
        
        workOrder2 =  Utils_TestMethods.createWorkOrder(acc2.id);
        workOrder2.Work_Order_Category__c='On-site';                 
        workOrder2.SVMXC__Order_Type__c='Maintenance';
        workOrder2.SVMXC__Problem_Description__c = 'BLALBLALA';
        workOrder2.SVMXC__Order_Status__c = 'UnScheduled';
        workOrder2.CustomerRequestedDate__c = Date.today();
        workOrder2.Service_Business_Unit__c = 'Energy';
        workOrder2.SVMXC__Priority__c = 'Normal-Medium';
        workOrder2.SendAlertNotification__c = true;
        workOrder2.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder2.CustomerTimeZone__c = 'America/New_York';
        
        workOrder2.BillToAccount__c = acc2.Id;
        workOrder2.SoldToAccount__c = acc2.Id;
        
        workOrder2.SVMXC__Company__c = acc2.Id;
        workOrder2.SVMXC__Site__c = loc2.Id;
        workOrder2.SVMXC__Contact__c = contact2.Id;
        
        workOrderList.add(workOrder2);
        
        System.debug('### Inserting workOrderList: '+workOrderList);
        insert workOrderList;
        System.debug('### Inserted workOrderList: '+workOrderList);
        
        // create interest
        List<InterestAmountOnOpportunityNotif__c> interestAmountList = new List<InterestAmountOnOpportunityNotif__c>();
        interestAmount1 = createInterestAmount(country1, 'IT', 'Service (IT)', 'Spare Sales (spares part only)', 100.0, 'EUR');
        interestAmountList.add(interestAmount1);
        interestAmount2 = createInterestAmount(country1, 'EN', 'Service (Energy)', 'Other Service', 1000.0, 'EUR');
        interestAmountList.add(interestAmount2);
        interestAmount3 = createInterestAmount(country2, 'IT', 'Product (IT)', 'Other HW', 10000.0, 'USD');
        interestAmountList.add(interestAmount3);
        interestAmount4 = createInterestAmount(country2, 'EN', 'Product (Energy)', 'Other HW', 100000.0, 'USD');
        interestAmountList.add(interestAmount4);
        interestAmount5 = createInterestAmount(country2, 'SO', 'Service (Solar)', 'Solar Services', 1000000.0, 'USD');
        interestAmountList.add(interestAmount5);
        insert interestAmountList;
        
        
        List<OpportunityNotification__c> opptyNotifList = new List<OpportunityNotification__c>();
        
        opportunityNotif101 = Utils_TestMethods.createOpportunityNotification(workOrder1.id);
        opportunityNotif101.Name = 'Test Opp 101';
        opportunityNotif101.case__c=objCase.id;
        opportunityNotif101.OpportunityScope__c ='BDSL3';
        opportunityNotif101.AccountForConversion__c =acc1.id;
        opportunityNotif101.CountryDestination__c=country1.id;
        opportunityNotif101.RejectionReason__c='null';
        opportunityNotif101.OpportunityOwner__c=null;
        opportunityNotif101.Amount__c=null;
        
        opptyNotifList.add(opportunityNotif101);
        
        opportunityNotif102 = Utils_TestMethods.createOpportunityNotification(workOrder2.id);
        opportunityNotif102.Name = 'Test Opp 102';
        opportunityNotif102.OpportunityScope__c ='BDADS';
        opportunityNotif102.OpportunityOwner__c=userinfo.getuserid();
        opportunityNotif102.AccountForConversion__c =acc2.id;
        opportunityNotif102.CountryDestination__c=country2.id;
        opportunityNotif102.RejectionReason__c='Other';
        opportunityNotif102.OpportunityOwner__c=null;
        opportunityNotif102.Amount__c=null;
        
        opptyNotifList.add(opportunityNotif102);

        System.debug('#### Inserting opptyNotifList: '+opptyNotifList);
        insert opptyNotifList;
        System.debug('#### Inserted opptyNotifList: '+opptyNotifList);
    }
    
    
    static testMethod void OpportunityNotificationTestMethod()
    { 
        
        try{

            Test.startTest();
            
            // Create Opprtunity Notification
            List<OpportunityNotification__c> oppNotifList = new List<OpportunityNotification__c>();

            OpportunityNotification__c opportunityNotif1 = Utils_TestMethods.createOpportunityNotification(workOrder1.id);
            opportunityNotif1.Name = 'Test Opp';
            opportunityNotif1.case__c=objCase.id;
            opportunityNotif1.OpportunityScope__c ='BDSL3';
            opportunityNotif1.AccountForConversion__c =acc1.id;
            opportunityNotif1.CountryDestination__c=country1.id;
            opportunityNotif1.RejectionReason__c='null';
            opportunityNotif1.OpportunityOwner__c=null;
            opportunityNotif1.Amount__c=null;
            
            oppNotifList.add(opportunityNotif1);
                        
            insert oppNotifList;
    
            Team_Members_Opportunity_Notification__c tmon= new Team_Members_Opportunity_Notification__c();
            tmon.Opportunity_Notification__c=opportunityNotif1.id;
            insert tmon;
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
             ip.name='test ip';
            insert ip;
    
            InstalledProductOnOpportunityNotif__c Ipon= new InstalledProductOnOpportunityNotif__c();
            Ipon.InstalledProduct__c=ip.id;
            Ipon.OpportunityNotification__c=opportunityNotif1.id;
            
            insert Ipon;
            
            Test.stopTest();
        }
        catch(Exception ex){
            
        }
    }
    
    static testMethod void changeOppNotifCountryOfDestination() {
        
        
        // Creating Interests
        List<InterestOnOpportunityNotif__c> interestList = new List<InterestOnOpportunityNotif__c>();
        InterestOnOpportunityNotif__c interest1 = createInterest(opportunityNotif101, interestAmount1.Leading_Business_BU__c, interestAmount1.Type__c, interestAmount1.Interest__c);
        interestList.add(interest1);
        InterestOnOpportunityNotif__c interest2 = createInterest(opportunityNotif101, interestAmount2.Leading_Business_BU__c, interestAmount2.Type__c, interestAmount2.Interest__c);
        interestList.add(interest2);
        InterestOnOpportunityNotif__c interest3 = createInterest(opportunityNotif102, interestAmount3.Leading_Business_BU__c, interestAmount3.Type__c, interestAmount3.Interest__c);
        interestList.add(interest3);
        InterestOnOpportunityNotif__c interest4 = createInterest(opportunityNotif102, interestAmount4.Leading_Business_BU__c, interestAmount4.Type__c, interestAmount4.Interest__c);
        interestList.add(interest4);
        System.debug('#### Inserting interestList: '+interestList);
        insert interestList;        
        System.debug('#### Inserted interestList: '+interestList);
        
        for (InterestOnOpportunityNotif__c currentInterest : interestList) {
            System.debug('#### currentInterest: '+currentInterest);
            System.debug('#### currentInterest amount: '+currentInterest.amount__c);
            System.debug('#### currentInterest currency: '+currentInterest.CurrencyIsoCode);
        }

        Test.startTest();
        
        // Now changing the opportunity notification country of destination
        List<OpportunityNotification__c> opptyNotifList = new List<OpportunityNotification__c>();
        opportunityNotif101.CountryDestination__c=country2.id;
        opptyNotifList.add(opportunityNotif101);
        
        opportunityNotif102.CountryDestination__c=country1.id;
        opptyNotifList.add(opportunityNotif102);
        
        System.debug('#### Updating country for opptyNotifList: '+opptyNotifList);
        update opptyNotifList;
        System.debug('#### Updated country for opptyNotifList: '+opptyNotifList);
        
        interest1.Leading_Business_BU__c = interestAmount5.Leading_Business_BU__c;
        interest1.Type__c = interestAmount5.Type__c;
        interest1.Interest__c = interestAmount5.Interest__c;
        
        update interest1;
        
        Test.stopTest();
    }
    
    static testMethod void searchOpportunityTestMethodMobilityVersion() { 
    
    
        try {
            
            RecordType MobileRTId = [SELECT Id FROM RecordType WHERE SObjectType = 'OpportunityNotification__c' AND DeveloperName = 'Mobile' LIMIT 1];
           
            OpportunityNotification__c opportunityNotif1 = Utils_TestMethods.createOpportunityNotification(workOrder1.id);
            opportunityNotif1.RecordTypeId = MobileRTId.Id;
            opportunityNotif1.From_Mobile__c = true;
            opportunityNotif1.Project_Name__c = 'Test';
            opportunityNotif1.Name = 'Test Opp';
            opportunityNotif1.case__c=objCase.id;
            opportunityNotif1.OpportunityScope__c ='BDSL3';
            opportunityNotif1.AccountForConversion__c =acc1.id;
            opportunityNotif1.CountryDestination__c=country1.id;
            opportunityNotif1.RejectionReason__c='Duplicate';
            opportunityNotif1.OpportunityOwner__c=null;
            opportunityNotif1.Location__c=loc1.id;
            opportunityNotif1.LeadingBusinessBU__c='BD';
            Database.insert(opportunityNotif1);
            
            // create interest
            InterestOnOpportunityNotif__c oppNotifInterest = new InterestOnOpportunityNotif__c();
            oppNotifInterest.OpportunityNotification__c = opportunityNotif1.Id;
            oppNotifInterest.Leading_Business_BU__c = 'EN';
            oppNotifInterest.Type__c = 'Product (Energy)';
            oppNotifInterest.Interest__c = 'Other HW';
            insert oppNotifInterest;
        
            // create ip reference to opp notification
            //InstalledProductOnOpportunityNotif__c  ipopn = new InstalledProductOnOpportunityNotif__c();
            //ipopn.Tech_CreatedFromIpSFM__c = true;
            //ipopn.OpportunityNotification__c = opportunityNotif1.id;
            //insert ipopn;
        
            //User newUser = Utils_TestMethods.createStandardUser('random test alias');
            //insert newUser;
            
            // create team member
            //Team_Members_Opportunity_Notification__c oppNotifTeamMember = new Team_Members_Opportunity_Notification__c();
            //oppNotifTeamMember.Opportunity_Notification__c = opportunityNotif1.Id;
            //oppNotifTeamMember.Detector__c = newUser.Id;
            //insert oppNotifTeamMember;

            // create attachment
            Attachment newAttachment1 = new Attachment(Name='OppNotificationTestAttachment.pdf');
            newAttachment1.parentId = opportunityNotif1.Id;
            Blob bodyblob = Blob.valueOf('Test Class Attachment Body');
            newAttachment1.body = bodyblob;
            insert newAttachment1;
          
          //  Controller.markAsDuplicate();
            
        } catch (exception e){
            System.debug('Exception '+e.getMessage());
            System.debug('Exception '+e.getStackTraceString());
        }  
    }
    
    private static InterestOnOpportunityNotif__c createInterest(OpportunityNotification__c anOppNotif, String aLeading_Business_BU, String aType, String aInterest) {
        InterestOnOpportunityNotif__c result;
        if (anOppNotif != null 
            && aLeading_Business_BU != null 
            && aType != null 
            && aInterest != null 
           ) {
               result = new InterestOnOpportunityNotif__c(
                   OpportunityNotification__c = anOppNotif.Id, 
                   Leading_Business_BU__c = aLeading_Business_BU, 
                   Type__c = aType, 
                   Interest__c = aInterest
               );
           }
        return result;
    }

    private static InterestAmountOnOpportunityNotif__c createInterestAmount(Country__c aCountry, String aLeading_Business_BU, String aType, String aInterest, Decimal anAmount, String aCurrencyIsoCode) {
        InterestAmountOnOpportunityNotif__c result;
        if (aCountry != null 
            && aLeading_Business_BU != null 
            && aType != null 
            && aInterest != null 
            && anAmount != null 
            && aCurrencyIsoCode != null
           ) {
               result = new InterestAmountOnOpportunityNotif__c(
                   Country__c = aCountry.Id, 
                   Leading_Business_BU__c = aLeading_Business_BU, 
                   Type__c = aType, 
                   Interest__c = aInterest, 
                   Amount__c = anAmount, 
                   CurrencyIsoCode = aCurrencyIsoCode
               );
           }
        return result;
    }


}