/**
13-June-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for VFC_NewPackage
 */
@isTest
private class VFC_NewPackage_TEST {

    static testMethod void myUnitTest() {
    
        //Test converage for the myPage visualforce page

        PageReference pageRef = Page.VFP_NewPackage;
        
        Test.setCurrentPageReference(pageRef);
        
        // create an instance of the controller
        PackageTemplate__c PKGTemplate = Utils_TestMethods.createPkgTemplate();
        PKGTemplate.PackageDuration__c = 12;
        insert PKGTemplate;
        
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id , null);
        insert testContract;
        
        Package__c Pkg = Utils_TestMethods.createPackage(testContract.Id , PKGTemplate.Id);
        Pkg.status__c = 'Active';
       // PAckage__c pkg = new package__c();
        ApexPages.StandardController sdtCon = new ApexPages.StandardController(Pkg);
        VFC_NewPackage myPageCon = new VFC_NewPackage(sdtCon);
        
        myPageCon.startDate = PKGTemplate.ID + ':' +system.today().format();
        myPageCon.calculateEndDate();
        myPageCon.SaveandNew();
       
        pkg.status__c = 'Draft';
        pkg.packagetemplate__c =PKGTemplate.Id;
        insert pkg;   
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(Pkg);
        VFC_NewPackage myPageCon1 = new VFC_NewPackage(sdtCon1);
        
        myPageCon1.startDate = PKGTemplate.ID + ':' +system.today().format();
        //myPageCon1.calculateEndDate();
        //myPageCon.pkg.status__c = 'Expired';
       // try
       // {
            myPageCon1.SaveandNew();
       // }
//catch(Exception e)
       // {
      //      System.assert( e.getMessage().contains(System.Label.CLSEP12CCC27), e.getMessage() );
     //   }
        
    }
    
        
}