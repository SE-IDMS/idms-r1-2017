//Test class for  VFC_UpdateDiscussionTopic
@isTest
public class VFC_UpdateDiscussionTopic_TEST {
    static testMethod void testMethodDiscussionTopic() {
            Account acc = Utils_TestMethods.createAccount();
            Database.Insert(acc); 
            Contact contacts = Utils_TestMethods.createContact(acc.Id,'TestContact');
            Database.insert(contacts); 
            Event Evt = Utils_TestMethods.createEvent(acc.id,contacts.Id,'New');
            Evt.Subject='Meeting';
            System.debug('>>>>>acc>>>>'+acc);
            System.debug('>>>>>con>>>>'+contacts);
            System.debug('>>>>>eve>>>>'+Evt);
            
            Database.Insert(Evt);
   
            

            User userCountry = [select Country__c from User where id=:userinfo.getUserId()];
            DiscussionTopic__c dsc = new DiscussionTopic__c (Name=userCountry.Country__c+'Test Discussion Topic',DiscussionTopicDescription__c='Test Discussion Topic');
            Database.Insert(dsc);
            ApexPages.StandardController sc= new ApexPages.StandardController(Evt);
            Pagereference pp = page.VFP_UpdateDiscussionTopic;
            Test.setCurrentPageReference(pp); 
            ApexPages.currentPage().getParameters().put('id',Evt.id);            
            VFC_UpdateDiscussionTopic vfc= new VFC_UpdateDiscussionTopic(sc);   
            VFC_UpdateDiscussionTopic.getDiscussionTopics(); 
            String jsonSelectString='{"Id":"'+Evt.Id+'","Off3Comments__c":"demo","Off3Comments2__c":"","Off3Comments3__c":"","Off3Comments4__c":"","Off3Comments5__c":"","X3DiscussedTopic1__c":"","X3DiscussedTopic2__c":"","X3DiscussedTopic3__c":"","X3DiscussedTopic4__c":"","X3DiscussedTopic5__c":"","Success__c":""}';
            VFC_UpdateDiscussionTopic.updateEvent(jsonSelectString); 
    }
}