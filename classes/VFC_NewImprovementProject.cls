// Added by Uttara for Q3 2016 Release - Notes to I2P

Public class VFC_NewImprovementProject {
    
    public Id CurrentUserId {get; set;}
    public list<CSQ_Profile__c> CSQP = new List<CSQ_Profile__c>();
    
    public VFC_NewImprovementProject(ApexPages.StandardController controller) {
        System.debug('!!!! Entered controller');
    }
    
    public PageReference NewEditPage() {
        System.debug('!!!! Entered NewEditPage method');
        
        PageReference NewIP = new PageReference('/'+SObjectType.CSQ_ImprovementProject__c.getKeyPrefix()+'/e?');   // /e for Edit Page
        NewIP.getParameters().put('ent', Label.CLQ316I2P04); // setting the 'ent' with CSQ Profile object Id
        NewIP.getParameters().put(Label.CL00690, Label.CL00691); // setting no-override = 1
        
        System.debug('!!!! NewIP 1 : ' + NewIP);
        
        NewIP.getParameters().put('RecordType',Label.CLQ316I2P01);  // setting the Opportunity for Improvement record type  
        
        System.debug('!!!! NewIP 2 : ' + NewIP);
        
        CurrentUserId = UserInfo.getUserId();
        CSQP = [SELECT CSQ_bFOUser__c, CSQ_DefaultOrganization__c, CSQ_DefaultOrganization__r.Name, CSQ_DefaultOrganization__r.Id FROM CSQ_Profile__c WHERE CSQ_bFOUser__c =: CurrentUserId];
        
        System.debug('!!!! CSQP : ' + CSQP);
        
        if(!CSQP.isEmpty()) {
            NewIP.getParameters().put(Label.CLQ316I2P02,CSQP[0].CSQ_DefaultOrganization__r.Name); // setting Detection Organization field
            NewIP.getParameters().put(Label.CLQ316I2P03,CSQP[0].CSQ_DefaultOrganization__r.Id); // setting Detection Organization lookup field
        }
        System.debug('!!!! NewIP 3 : ' + NewIP);
        
        NewIP.getParameters().put(Label.CL00330,SObjectType.CSQ_ImprovementProject__c.getKeyPrefix()+'/o'); // setting the retURL 
        
        System.debug('!!!! NewIP 4 : ' + NewIP);
        return NewIP;
    }
}