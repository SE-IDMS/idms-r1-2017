/**
•   Created By: 
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class is for uims call for AIL
**/
public class IdmsUserAILuimsv2ImplServiceIms {
    public class UserAccessManager_UIMSV2_ImplPort {
        public String endpoint_x = Label.CLQ316IDMS300;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/', 'IdmsUserAILuimsv2ServiceIms', 'http://uimsv2.impl.service.ims.schneider.com/', 'IdmsUserAILuimsv2ImplServiceIms'};
            public Boolean revokeAccessControlToUser(String callerFid,String federatedId,IdmsUserAILuimsv2ServiceIms.accessElement access) {
                IdmsUserAILuimsv2ServiceIms.revokeAccessControlToUser request_x = new IdmsUserAILuimsv2ServiceIms.revokeAccessControlToUser();
                request_x.callerFid = callerFid;
                request_x.federatedId = federatedId;
                request_x.access = access;
                IdmsUserAILuimsv2ServiceIms.revokeAccessControlToUserResponse response_x;
                Map<String, IdmsUserAILuimsv2ServiceIms.revokeAccessControlToUserResponse> response_map_x = new Map<String, IdmsUserAILuimsv2ServiceIms.revokeAccessControlToUserResponse>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        '',
                        'http://uimsv2.service.ims.schneider.com/',
                        'revokeAccessControlToUser',
                        'http://uimsv2.service.ims.schneider.com/',
                        'revokeAccessControlToUserResponse',
                        'IdmsUserAILuimsv2ServiceIms.revokeAccessControlToUserResponse'}
                );
                response_x = response_map_x.get('response_x');
                return response_x.return_x;
            }
        //This method is to get access of user
        public IdmsUserAILuimsv2ServiceIms.accessTree getAccessControlByUser(String callerFid,String federatedId) {
            IdmsUserAILuimsv2ServiceIms.getAccessControlByUser request_x = new IdmsUserAILuimsv2ServiceIms.getAccessControlByUser();
            request_x.callerFid = callerFid;
            request_x.federatedId = federatedId;
            IdmsUserAILuimsv2ServiceIms.getAccessControlByUserResponse response_x;
            Map<String, IdmsUserAILuimsv2ServiceIms.getAccessControlByUserResponse> response_map_x = new Map<String, IdmsUserAILuimsv2ServiceIms.getAccessControlByUserResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'getAccessControlByUser',
                    'http://uimsv2.service.ims.schneider.com/',
                    'getAccessControlByUserResponse',
                    'IdmsUserAILuimsv2ServiceIms.getAccessControlByUserResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
        //This method is usedto grant access to user in UIMS
        public Boolean grantAccessControlToUser(String callerFid,String federatedId,IdmsUserAILuimsv2ServiceIms.accessElement access) {
            IdmsUserAILuimsv2ServiceIms.grantAccessControlToUser request_x = new IdmsUserAILuimsv2ServiceIms.grantAccessControlToUser();
            request_x.callerFid = callerFid;
            request_x.federatedId = federatedId;
            request_x.access = access;
            IdmsUserAILuimsv2ServiceIms.grantAccessControlToUserResponse response_x;
            Map<String, IdmsUserAILuimsv2ServiceIms.grantAccessControlToUserResponse> response_map_x = new Map<String, IdmsUserAILuimsv2ServiceIms.grantAccessControlToUserResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'grantAccessControlToUser',
                    'http://uimsv2.service.ims.schneider.com/',
                    'grantAccessControlToUserResponse',
                    'IdmsUserAILuimsv2ServiceIms.grantAccessControlToUserResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
    }
}