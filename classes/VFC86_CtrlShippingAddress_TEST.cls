/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | VFC86_CtrlShippingAddress_TEST                                                     |
|                       |                                                                                    |
|     - Object(s)       | RMA__c                                                                             |
|     - Description     |   - Test method for checking if the Shipping Address is defined                    |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | April, 27th 2012                                                                   |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
@isTest
private class VFC86_CtrlShippingAddress_TEST
{
    static testMethod void testVFC86_CtrlShippingAddress_TEST1()
    {
       System.debug('#### START test method for VFC86_CtrlShippingAddress_TEST ####');

       Account acc1 = Utils_TestMethods.createAccount();
       insert acc1;

       Contact con1 = Utils_TestMethods.createContact(acc1.Id,'TestContact');
       insert con1;

       Case case1 = Utils_TestMethods.createCase(acc1.Id, con1.Id, 'Open');
       insert case1;

       RMA__c newRMA = new RMA__c(Case__c=case1.Id, AccountStreet__c='aa');       
       insert newRMA;

       ApexPages.StandardController RMAStandardController = new ApexPages.StandardController(newRMA);
       VFC86_CtrlShippingAddress RMAController = new VFC86_CtrlShippingAddress(RMAStandardController);

       System.assertEquals(RMAController.getShippingAddressIsValid(), false);

       RMAController.AddWarningMsg();
       RMAController.redirectToCurrentRMA();

       System.debug('#### END   test method for VFC86_CtrlShippingAddress_TEST ####');
    }
}