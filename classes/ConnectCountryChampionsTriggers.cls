/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  23-April-2013
    Description         : Class for Connect Country Cascading Milestone Sharing Permission
*/

public class ConnectCountryChampionsTriggers
{

Public Static void ConnectCountryChampionsAfterUpdate(List<Country_Champions__c> CHold, List<Country_Champions__c> CHnew)
      {
// Add permission to Country Deployment Leader
      
         List<Country_Cascading_Milestone__Share>  CCMShare = new List<Country_Cascading_Milestone__Share>();
                  
         List<Country_Cascading_Milestone__c> CCM = new  List<Country_Cascading_Milestone__c>();  
        
         CCM = [Select id from Country_Cascading_Milestone__c where Country__c = :CHnew[0].Country__c];
         
         Set<id> CCMId = new Set<id>();
         
         for(Country_Cascading_Milestone__c CCMile : CCM){
           CCMId.add(CCMile.id);
           Country_Cascading_Milestone__Share cshare = new Country_Cascading_Milestone__Share();
           cshare.ParentId = CCMile.id;
           cshare.UserOrGroupId = CHnew[0].User__c;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Country_Cascading_Milestone__Share.RowCause.Country_Champs__c;
           CCMShare.add(cshare);
          }
         Database.insert( CCMShare,false);

 if(CHold[0].User__c != CHnew[0].User__c){  
     
     For(Country_Cascading_Milestone__Share  CCMSharedeleteCC :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Country_Cascading_Milestone__Share where ParentId in :CCMId and UserOrGroupId = :CHold[0].User__c and RowCause = :Schema.Country_Cascading_Milestone__Share.RowCause.Country_Champs__c]){
     system.debug('*----------*' + CCMSharedeleteCC.Parentid);
     if(CCMSharedeleteCC.Parentid != null)              
      Database.delete(CCMSharedeleteCC,false);
       }
      } 
    
     }
     
     Public Static void ConnectCountryChampionsbeforeInsertUpdate(List<Country_Champions__c> CH)
     {
      
     List<Country_Champions__c> countrychamp = new List<Country_Champions__c>();
     countrychamp = [select IsDefault__c,Country__c,User__c,Year__c from Country_Champions__c where IsDefault__c ='Yes'];
     for(Country_Champions__c C:CH){
     C.ValidateISDefault__c = false;
     for(Country_Champions__c CC:countrychamp)
     {
     if(C.IsDefault__c =='Yes' && C.Country__c !=null && C.User__c !=null && C.Country__c == CC.Country__c && C.Year__c == CC.Year__c)
     C.ValidateISDefault__c = true;
     }
     } 
     }    
     
}