/*
Author: Sid (bFO Solution) & Polyspot team
description: this class is a controller class for the page VFP_DK_ContactSeach Page , this controller provides a way to make
a webservice callout to polyspot engine and it will return back with the json response.

*/
global class VFC_DK_ContactSearch {

    public String name{get;set;}

    /*
    Author: Sid & Polyspot team.
    Function : getContent
    Parameters : params - string , comma seperated list of parameters to be sent to the polyspot server.
    Description : The below method provides a way to send the request to the required polyspot engine and get the json response back.
    The technical part is to create a http request and authenticate the service using Basic Authentication and get the response.
    */
    
     public VFC_DK_ContactSearch () {
           Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge');
      }

    @RemoteAction
    global static String getContent(String params) {
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(System.Label.PolyspotEndPoint);
        req.setMethod('POST');
        req.setBody(params);

        String username = System.Label.PolyspotUserName;
        String password = System.Label.PolyspotPassword;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        HttpResponse res = h.send(req);

        return res.getBody();
    }

}