global class ReadPicklistTranslationMockImpl  implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {

        // test for the type of the response, OP was just doing readMetaData()
        if (request instanceof MetadataService.readMetadata_element) {
          MetadataService.readMetadata_element requestReadMetadata_element  = (MetadataService.readMetadata_element) request;
          // This allows you to generalize the mock response by type of metadata read
          if (requestReadMetadata_element.type_x == 'CustomObjectTranslation') { 
             MetadataService.readCustomObjectTranslationResponse_element mockRes   = new MetadataService.readCustomObjectTranslationResponse_element();
             mockRes.result = new MetaDataService.ReadCustomObjectTranslationResult();
             mockRes.result.records = createCustomObjects();
             response.put('response_x', mockRes);
          }
          else if (requestReadMetadata_element.type_x == 'CustomField') { 
            //response.put('response_x', RCOT);
          }        
        }
    }
   
    public static MetadataService.CustomObjectTranslation[] createCustomObjects () {
        
          List<MetadataService.PicklistValueTranslation> newPickVal= new List<MetadataService.PicklistValueTranslation>();
          MetadataService.PicklistValueTranslation picval = new MetadataService.PicklistValueTranslation();
          picval.masterLabel = 'EU';
          picval.translation = 'End User or Consumer';
          newPickVal.add(picval);
          //newPickVal[0].masterLabel ='EU';
          //newPickVal[0].translation ='End User or Consumer';
          picval.masterLabel = 'FI';
          picval.translation = 'Financier';
          newPickVal.add(picval);
          
          picval.masterLabel = 'IG';
          picval.translation = 'Internal Schneider Entity';
          newPickVal.add(picval);
          
          picval.masterLabel = 'LC';
          picval.translation = 'Contractors';
          newPickVal.add(picval);
          
           picval.masterLabel = 'OM';
          picval.translation = 'Electrical Equipment Manufacturers';
          newPickVal.add(picval);
       
          List<MetadataService.CustomFieldTranslation> cf= new  List<MetadataService.CustomFieldTranslation>();
          MetadataService.CustomFieldTranslation c = new MetadataService.CustomFieldTranslation();
          c.name = 'newList';
          c.picklistValues = newPickVal;
          cf.add(c);
          
           MetadataService.CustomFieldTranslation cc = new MetadataService.CustomFieldTranslation();
          cc.name = 'PRMBusinessType__c';
          cc.picklistValues = newPickVal;
          cf.add(cc);
          
         MetadataService.CustomObjectTranslation newRes = new MetadataService.CustomObjectTranslation();
         newRes.fullName = 'Account-en_US';
         newRes.fields = cf;
         
            return (new List<MetadataService.CustomObjectTranslation> {newRes});
    } 
    
}