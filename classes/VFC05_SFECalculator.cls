/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 20/10/2010
    Description     : Controller extension that calculates the Total number of visits resulting from all 
                        associated Platforming Scoring records (that are Account specific).
*/
public with sharing class VFC05_SFECalculator {
    private final SFE_IndivCAP__c iDivCAP;
    private final Integer PLATFORM_SCORING_LIMIT = 10000;
    
    public VFC05_SFECalculator(ApexPages.StandardController stdController) {
        this.iDivCAP = (SFE_IndivCAP__c)stdController.getRecord();
    }
    
    public Integer totalVisits {
        get{
            Integer countQ1 = 0, countQ2 = 0, countQ3 = 0, countS1 = 0, countS2 = 0, countS3 = 0, countG1 = 0, countG2 = 0, countG3 = 0;
            //Calculate the total number of accounts in each quadrant
            for(SFE_PlatformingScoring__c ps : [select Id, CustomerProfile__c from SFE_PlatformingScoring__c where IndivCAP__c = :iDivCAP.Id limit :PLATFORM_SCORING_LIMIT]){
                if(ps.CustomerProfile__c == 'Q1'){
                    countQ1++;
                }else if(ps.CustomerProfile__c == 'Q2'){
                    countQ2++;
                }else if(ps.CustomerProfile__c == 'Q3'){
                    countQ3++;
                }else if(ps.CustomerProfile__c == 'S1'){
                    countS1++;
                }else if(ps.CustomerProfile__c == 'S2'){
                    countS2++;
                }else if(ps.CustomerProfile__c == 'S3'){
                    countS3++;
                }else if(ps.CustomerProfile__c == 'G1'){
                    countG1++;
                }else if(ps.CustomerProfile__c == 'G2'){
                    countG2++;
                }else if(ps.CustomerProfile__c == 'G3'){
                    countG3++;
                }
            }
            //Ensure we never multiply by a null value
            if(iDivCAP.Q1VisitFreq__c == null)
                iDivCAP.Q1VisitFreq__c = 0;
            if(iDivCAP.Q2VisitFreq__c == null)
                iDivCAP.Q2VisitFreq__c = 0;
            if(iDivCAP.Q3VisitFreq__c == null)
                iDivCAP.Q3VisitFreq__c = 0;
            if(iDivCAP.S1VisitFreq__c == null)
                iDivCAP.S1VisitFreq__c = 0;
            if(iDivCAP.S2VisitFreq__c == null)
                iDivCAP.S2VisitFreq__c = 0;
            if(iDivCAP.S3VisitFreq__c == null)
                iDivCAP.S3VisitFreq__c = 0;
            if(iDivCAP.G1VisitFreq__c == null)
                iDivCAP.G1VisitFreq__c = 0;
            if(iDivCAP.G2VisitFreq__c == null)
                iDivCAP.G2VisitFreq__c = 0;
            if(iDivCAP.G3VisitFreq__c == null)
                iDivCAP.G3VisitFreq__c = 0;

            //Calculate total visits
            return totalVisits = (iDivCAP.Q1VisitFreq__c.intValue() * countQ1) +
                                    (iDivCAP.Q2VisitFreq__c.intValue() * countQ2) + 
                                    (iDivCAP.Q3VisitFreq__c.intValue() * countQ3) +
                                    (iDivCAP.S1VisitFreq__c.intValue() * countS1) +
                                    (iDivCAP.S2VisitFreq__c.intValue() * countS2) +
                                    (iDivCAP.S3VisitFreq__c.intValue() * countS3) +
                                    (iDivCAP.G1VisitFreq__c.intValue() * countG1) +
                                    (iDivCAP.G2VisitFreq__c.intValue() * countG2) +
                                    (iDivCAP.G3VisitFreq__c.intValue() * countG3);  
        }
        set;
    }
}