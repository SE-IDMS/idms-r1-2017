/*     
@Author: Deepak
Created Date: 17-09-2013
Description: This will update WON work order status field.
**********

*/

Public Class AP_WorkOrderAfterUpdate
{   
    
    Static boolean blnUpdatedCase=false;
    public static void UpdateWONStatus(Set<Id> WonId,Set<Id> WoId)
    {   
        List<SVMXC__Service_Order__c> wolst = new List<SVMXC__Service_Order__c>();
        List<WorkOrderNotification__c> wnlst = new List<WorkOrderNotification__c>();
        
        /*  if(WonId.size()>0)  
        {   
            wnlst = [Select Work_Order_Status__c,Scheduled_Date_Time__c,CommentstoDispatcher__c From WorkOrderNotification__c Where Id=:wonId];
        }
        */
        if(WoId.size()>0)
        {
            wolst = [Select Tolabel(SVMXC__Order_Status__c),SVMXC__Scheduled_Date_Time__c,Comments_to_Planner__c,Work_Order_Notification__c From SVMXC__Service_Order__c Where Id=:WoId];
        }
        system.debug('wolst:'+wolst);
        if(wolst.size()>0 && WoId.size()>0 && WonId.size()>0)
        {
            for(SVMXC__Service_Order__c wo :wolst)
            {
                WorkOrderNotification__c won = new WorkOrderNotification__c(Id = wo.Work_Order_Notification__c);
                won.Work_Order_Status__c = wo.SVMXC__Order_Status__c;
                won.Scheduled_Date_Time__c = wo.SVMXC__Scheduled_Date_Time__c;
                won.CommentstoDispatcher__c = wo.Comments_to_Planner__c;
                wnlst.add(won);
            }
           if(wnlst.size()>0) update wnlst;
            
        }
    }
    //public static void UpdateCaseStatus(List<SVMXC__Service_Order__c> Wolist,Id caId )
    public static void UpdateCaseStatus(List<SVMXC__Service_Order__c> Wolist,Set<id>  caIds )// DEF-8733
    {   
       // List<SVMXC__Service_Order__c> wolst = new List<SVMXC__Service_Order__c>();
       if(!blnUpdatedCase){
            List<Case> caselst = new List<Case>();
            
            if(Wolist.size()>0 &&  caIds !=null)
            {
                //for(SVMXC__Service_Order__c wo :Wolist)
                for(Id cid :caIds)
                {
                    Case cs = new Case(Id = cid);
                    cs.Status = 'Closed';
                    
                    caselst.add(cs);
                }
               if(caselst.size()>0) 
               {
                   //update caselst;
                   Database.update(caselst,false);
               }
                blnUpdatedCase=true;
            }
        }
    }
    
    public static void UpdateAssignedToolsTechnicians(List<SVMXC__Service_Order__c> Wolist )
    {   
        System.debug('******************UpdateAssignedToolsTechnicians Start *************************');
        
		List<SVMXC__Service_Order__c> withEmail = new List<SVMXC__Service_Order__c>();
		List<SVMXC__Service_Order__c> withSFM = new List<SVMXC__Service_Order__c>();
		List<AssignedToolsTechnicians__c> attList = new List<AssignedToolsTechnicians__c>();
		Set<id> uidset = new Set<id>();
		Map<id,SVMXC__Service_Order__c> woMap = new Map<id,SVMXC__Service_Order__c>();
		woMap.putAll(Wolist);
        Set<id> woidSet = new Set<id>();
        /*
		for(SVMXC__Service_Order__c wo: Wolist){
		
			if(wo.LoginFSR__c != null )
			{
				withEmail.add(wo);
				uidset.add(wo.LoginFSR__c);
			}
			else{
				withSFM.add(wo);
			}
		
		}
		
		if(withEmail != null && withEmail.size()>0){
		
			 List<AssignedToolsTechnicians__c> attList1 = [SELECT Id,ReasonforRejection__c,Status__c,TechnicianEquipment__c,WorkOrder__c,TechnicianEquipment__r.SVMXC__Salesforce_User__c  FROM AssignedToolsTechnicians__c where WorkOrder__c in :woMap.keySet()  AND TechnicianEquipment__r.SVMXC__Salesforce_User__c  =: uidset AND ( Status__c = 'Assigned' OR Status__c = 'Accepted' )];
			 if(attList1 != null && attList1.size()>0)
			 attList.addAll(attList1);
		
		
		}
		if(withSFM != null && withSFM.size()>0){
		
			 List<AssignedToolsTechnicians__c> attList2 = [SELECT Id,ReasonforRejection__c,Status__c,TechnicianEquipment__c,WorkOrder__c,TechnicianEquipment__r.SVMXC__Salesforce_User__c  FROM AssignedToolsTechnicians__c where WorkOrder__c in :woMap.keySet()  AND TechnicianEquipment__r.SVMXC__Salesforce_User__c  =: UserInfo.getUserid() AND  ( Status__c = 'Assigned' OR Status__c = 'Accepted' )];
			 if(attList2 != null && attList2.size()>0)
			 attList.addAll(attList2);
		
		
		}*/
		attList =[SELECT Id,ReasonforRejection__c,Status__c,TechnicianEquipment__c,WorkOrder__c,TechnicianEquipment__r.SVMXC__Salesforce_User__c  FROM AssignedToolsTechnicians__c where WorkOrder__c in :woMap.keySet()  AND TechnicianEquipment__r.SVMXC__Salesforce_User__c  =: UserInfo.getUserid() AND  ( Status__c = 'Assigned' OR Status__c = 'Accepted' )];
		if(attList != null && attList.size()>0)
		{
			for(AssignedToolsTechnicians__c obj:attList){
				if(woMap.containskey(obj.WorkOrder__c)){
					obj.Status__c = woMap.get(obj.WorkOrder__c).WOLevel__c;
					if(woMap.get(obj.WorkOrder__c).WOLevel__c == 'Rejected')
					obj.ReasonforRejection__c = woMap.get(obj.WorkOrder__c).Reason_for_Rejection__c;
				}
			
			}
			
			Database.SaveResult[] results = Database.Update(attList,false);
			for(Integer k=0;k<results.size();k++ )
			{
				Database.SaveResult sr =results[k];
				if(sr.isSuccess())
				{                           
					  woidSet.add(attList[k].WorkOrder__c);    
				}
				
			}
			// AMO (August 30th 2016 : Q3 BFO 2016 : Technical BR : remove @future call)
			/*
			if(woidSet != null && woidSet.size()>0){
				UpdateWorkOrders(woidSet);
				System.debug('*******************************************');
                
			}
			*/
		}
		System.debug('******************UpdateAssignedToolsTechnicians end *************************');       
    }
    // AMO (August 30th 2016 : Q3 BFO 2016 : Technical BR : remove @future method)
    /*
    @future
    public static void UpdateWorkOrders(Set<id> widset )
    {
            if(widset != null && widset.size()>0){
                List<SVMXC__Service_Order__c> wlist = [Select id, WOLevel__c ,Reason_for_Rejection__c from SVMXC__Service_Order__c where id in : widset ];
                for(SVMXC__Service_Order__c wo: wlist){
                    wo.Reason_for_Rejection__c= null;
                    wo.WOLevel__c = null;
                    //wo.LoginFSR__c = null;
                }           
                update wlist;
            }
    }
    */
}