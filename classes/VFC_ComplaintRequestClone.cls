public class VFC_ComplaintRequestClone{
// Custom clone ceated by Ram chilukuri as pert of October 2015 release BR-4653
// The custom clone works based on custome setting, the custom setting will store fields to clone (The custome setting contains field api name, field label and field id)
    public Id CRId;
    public ComplaintRequest__c complaintRequestRecord;
    public string strRelatedCaseNumber='';
    public list<ComplaintRequest__c> lstCRClonefields=new list<ComplaintRequest__c>();
    
// If the ComplaintRequest__c is displayed in a service cloud console, the custom clone button sends the parameter isdtp=nv in the VFP_ComplaintRequestClone page URL
// the URL parameter isdtp=nv allows to hide the salesforce header and sidebar
// The isdtp URL parameter will be stored in the isdtp class attribute
    public String isdtp='';
    
    public VFC_ComplaintRequestClone(ApexPages.StandardController controller) {
            complaintRequestRecord=(ComplaintRequest__c)controller.getRecord();
            CRId=complaintRequestRecord.Id;
        if(CRId==null && System.currentPageReference().getParameters().containskey('CRId'))
            CRId=Apexpages.currentPage().getParameters().get('CRId');
            string crFields ='';
            for(CS_ComplaintRequestCloneFields__c tempCS:CS_ComplaintRequestCloneFields__c.getAll().Values())
            {
                crFields += tempCS.CRFieldApiName__c + ',';
            }
            crFields= crFields.substring (0,crFields.length() -1);
            string queryCrs='select id,'+crFields+', RecordTypeId from ComplaintRequest__c where id = '+ '\''+CRId+'\'';
            system.debug('****queryCrs--------->'+queryCrs);
            lstCRClonefields=database.query(queryCrs);
            system.debug('****lstCRClonefields--------->'+lstCRClonefields);        
        if (Apexpages.currentPage().getParameters().containskey('isdtp'))
            isdtp=Apexpages.currentPage().getParameters().get('isdtp');
    }
      
    public PageReference CRCloneLink(){
            PageReference pageRef = new PageReference('/'+SObjectType.ComplaintRequest__c.getKeyPrefix()+'/e');
            // Disable override
           pageRef.getParameters().put(Label.CL00690, Label.CL00691);
        if(lstCRClonefields.size()>0){
            for(CS_ComplaintRequestCloneFields__c tempCS:CS_ComplaintRequestCloneFields__c.getAll().Values())
                { system.debug('********lstCRClonefields[0].RecordTypeId'+lstCRClonefields[0].RecordTypeId);
                    if(string.valueof(lstCRClonefields[0].RecordTypeId).substring(0,15) !=Label.CLAPR16I2P02 && (tempCS.CRFieldApiName__c!='Case__c' || tempCS.CRFieldApiName__c!='Case__r.CaseNumber')){
                        string field =tempCS.CRFieldApiName__c;
                            if(field.contains('.')){
                                String nextField = field.substringAfter('.');
                                String relation = field.substringBefore('.');
                                field=relation.replace('__r','__c');
                                if(string.valueof(lstCRClonefields[0].get(field))!=null){ 
                                string obj = getFieldValue(lstCRClonefields[0],tempCS.CRFieldApiName__c); 
                                    if(obj!=null){
                                        pageRef.getParameters().put(string.valueof(tempCS.CRFieldId__c),obj);
                                    }
                                } 
                            }
                            else if(lstCRClonefields[0].get(tempCS.CRFieldApiName__c)!=Null && tempCS.DataType__c==null){
                                pageRef.getParameters().put(string.valueof(tempCS.CRFieldId__c),string.valueof(lstCRClonefields[0].get(tempCS.CRFieldApiName__c)));
                            }
                            else if(lstCRClonefields[0].get(tempCS.CRFieldApiName__c)!=Null && tempCS.DataType__c=='DateTime'){
                             Datetime dt=datetime.valueof(lstCRClonefields[0].get(tempCS.CRFieldApiName__c));
                             //string dts=dt.format('d/M/y hh:mm a');
                             string myDate = Utils_Datetimeformatted.getTimeZoneValue(dt); 
                               // pageRef.getParameters().put(string.valueof(tempCS.CRFieldId__c),dts);
                                pageRef.getParameters().put(string.valueof(tempCS.CRFieldId__c),myDate);
                        }
                            pageRef.getParameters().put('RecordType',Label.CLDEC13LCR28);
                }
            else
                {   
                    string field =tempCS.CRFieldApiName__c;
                    if(field.contains('.')){
                        String nextField = field.substringAfter('.');
                        String relation = field.substringBefore('.');
                        field=relation.replace('__r','__c');
                        if(string.valueof(lstCRClonefields[0].get(field))!=null){
                        string obj = getFieldValue(lstCRClonefields[0],tempCS.CRFieldApiName__c); 
                        if(obj!=null  && obj!=''){
                            pageRef.getParameters().put(string.valueof(tempCS.CRFieldId__c),obj);

                        }
                        system.debug('RAM---->'+obj);
                        }
                        }
                        else if(lstCRClonefields[0].get(tempCS.CRFieldApiName__c)!=Null && tempCS.DataType__c==null){
                                pageRef.getParameters().put(string.valueof(tempCS.CRFieldId__c),string.valueof(lstCRClonefields[0].get(tempCS.CRFieldApiName__c)));
                        }
                        else if(lstCRClonefields[0].get(tempCS.CRFieldApiName__c)!=Null && tempCS.DataType__c=='DateTime'){
                                Datetime dt=datetime.valueof(lstCRClonefields[0].get(tempCS.CRFieldApiName__c));
                             //string frmt= DateTime.now().format();
                             
                           
                            string myDate = Utils_Datetimeformatted.getTimeZoneValue(dt);  
                            system.debug('HANAMANTH09090->'+myDate);
                             //string dts=dt.format(frmt);
                                pageRef.getParameters().put(string.valueof(tempCS.CRFieldId__c),string.valueof(myDate));
                                //pageRef.getParameters().put(string.valueof(tempCS.CRFieldId__c),string.valueof(lstCRClonefields[0].get(tempCS.CRFieldApiName__c)));
                        }
                    
                            pageRef.getParameters().put('RecordType',Label.CLDEC13LCR27);
                    }
                }
            pageRef.getParameters().put('retURL',CRId);
            
            // Add the isdtp URL parameter if exists and to the clone page URL
            if (isdtp != null && isdtp.length()>0)
                pageRef.getParameters().put('isdtp',isdtp);
        }
        pageRef.setRedirect(true);
        return pageRef;
    }
    public static string getFieldValue(SObject o,String field){
     if(o == null || field==null){
        return null;
     }
     string fldval;
     if(field.contains('.')){
        String nextField = field.substringAfter('.');
        String relation = field.substringBefore('.');
        return fldval=(string)o.getSObject(relation).get(nextField);
        }
        else
        {
        return fldval= (string)o.get(field);
        // return fldval=string.valueOfGmt(o.get(field))
        }
    }
}