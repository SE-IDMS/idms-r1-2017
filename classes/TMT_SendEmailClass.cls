/*
    Author          : Nicolas Palitzyne (Accenture) 
    Date Created    : 21/03/2011
    Description     : Apex Outbound Mails Class
*/

Public class TMT_SendEmailClass
{
    public static String HEADER_IMG_URL = Label.CL00298;
    
     // Send a Mail to the related Trainee if a Registration is canceled
     public static void SendDeleteEmail(List<TMT_Registration__c> RList)
     {
              
         List<User> Trainees = new List<User>();
         set<String> TraineesIDs = new set<String>(); 
         Map<USer,TMT_Registration__c> TraineeMap = new Map<User,TMT_Registration__c>();
         
         // Retrieve all Registrations IDs
         For(TMT_Registration__c R:RList)
         {
             TraineesIDs.add(R.Trainee__c);
         }
        
         // Get all related Trainees
         Trainees = [SELECT Id, Name, firstName, LastName, email FROM User WHERE Id IN :TraineesIDs];
         
         System.debug('>>>> Trainees :' + Trainees);
 
         // Map Trainees and Registrations
          For(TMT_Registration__c R:RList)
         {
             For(User T:Trainees)      
             {
                if(R.Trainee__c == T.Id)
                    TraineeMap.put(T,R);
            }
        }
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();


        For(User U:Trainees)
        {
            // Create new email 
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            // Get the Registration
            TMT_Registration__c Regi = TraineeMap.get(U);
                    
            String Body8 = '<b>More Details : </b> '+Regi.Addinfo__c+'</p>';
            if(Regi.Addinfo__c == null)
                Body8 = ' ';
                
            DateTime StartTime = Regi.Start__c;
            DateTime EndTime = Regi.End__c;
            String SesStart = StartTime.format('dd.MM.yyyy HH:mm');
            String SesEnd;  
            if(EndTime != null)
                SesEnd = EndTime.format('dd.MM.yyyy HH:mm');
            else 
                SeSend = ' ';    
                        
            String[] toAddresses = new String[] {U.email};
            mail.setSenderDisplayName(Label.CL00299); // Specify who is the sender
            mail.setToAddresses(toAddresses);
            mail.setSubject(Label.CL00300);
            mail.setSaveAsActivity(true);
            String Body0 = '<img src='+HEADER_IMG_URL+' height="76" width="806" >';
            String Body1 = '<p>Dear '+U.Name+',</p><p>Please note that your registration for the <b>bridge front office training session has been Canceled</b>. Another session should be scheduled promptly.</p>';
            String Body11 = '<p> In case of any problem, please liaise with '+UserInfo.getFirstName()+' '+UserInfo.getLastName()+'.</p>';
            String Body2 = '<p><FONT color=#BB0066><b>Canceled session details</b></FONT><br>';
            String Body3 = '<b>Curriculum : </b> '+Regi.Curriculum__c+'<br>'; 
            String Body4 = '<b>Start scheduled : </b> '+SesStart +'<br>';
            String Body5 = '<b>End scheduled : </b> '+SesEnd +'<br>';
            String Body7 = '<b>Location : </b> '+Regi.Location__c+', '+Regi.Address__c+', '+Regi.Zipcode__c+' '+Regi.City__c+'<br>';
            String Body9 = '<p>Kind Regards, </p>';
            String Body10 = '<p>'+UserInfo.getFirstName()+' '+UserInfo.getLastName()+'</p>';
            // 
            mail.setHTMLBody('<FONT face=Arial color=#222222">'+Body0+Body1+Body11+Body2+Body3+Body4+Body5+Body7+Body8+Body9+Body10+'</FONT>');
            // Add the mail to the list to be sent 
            mails.add(mail); 
            
        }
        
         // Send mails
         try
         {
            Messaging.sendEmail(mails);
         }
         catch(Exception e)
         {
             System.debug('---- Send email fail: ' + e.getmessage());
         }
        
    }
    
    // Send a Mail to the related Trainee if a Session is deleted
    public static void SendDeletedSessionEmail(List<TMT_Session__c> SList)
    {
        set<String> SessionsID = new set<String>();
        set<String> OwnersID = new set<String>();
        map<ID,User> OwnerMap = new map<ID,User>();
        
        For(TMT_Session__c S:SList)
        {
            SessionsID.add(S.Id);
            if(!OwnersID.contains(S.OwnerID))
                OwnersID.add(S.OwnerID);        
        }
        
        // List of all Owners
        List<User> SessionOwners = [SELECT Id, Name, Email FROM User WHERE Id IN :OwnersID];
        System.debug('>>> Session Owners : '+ SessionOwners);
        
        // Map Owners to their Sessions
        For(TMT_Session__c S:SList)
        {
            For(User SO:SessionOwners)    
            {
                if(S.OwnerID == SO.Id)
                    OwnerMap.put(S.Id,SO);
            }
        }
        
    
        List<TMT_Registration__c> RList = [SELECT Id, Name, Trainee__c, Curriculum__c, Start__c, End__c, Location__c, Addinfo__c,
        Address__c, Zipcode__c, City__c, Session__c 
        FROM TMT_Registration__c WHERE Session__c IN :SessionsID];
        
        System.debug('##>>>>> Registrations : '+RList);
        
        List<User> Trainees = new List<User>();
        set<String> TraineesIDs = new set<String>(); 
        Map<TMT_Registration__c,User> RegistrationMap = new Map<TMT_Registration__c,User>();
        
        // Retrieve all Registrations IDs
        For(TMT_Registration__c R:RList)
        {
            TraineesIDs.add(R.Trainee__c);
        }
        
        // Get all related Trainees
        Trainees = [SELECT Id, Name, firstName, LastName, email FROM User WHERE Id IN :TraineesIDs];
        
        System.debug('##>>>> Trainees :' + Trainees);
 
        // Map Trainees and Registrations
         For(TMT_Registration__c R:RList)
        {
            For(User T:Trainees)      
            {
                if(R.Trainee__c == T.Id)
                    RegistrationMap.put(R,T);

            }
        }
        
        System.debug('##>>>> Registration map:' + RegistrationMap );
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

        For(TMT_Registration__c Regi:Rlist)
        {
            // Create new email 
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            // Get the related Trainee
            User U =  RegistrationMap.get(Regi);
            System.debug('## >> Trainee :' + U );
            
            User SessionOwner = OwnerMap.get(Regi.Session__c);
            
            String Body8 = '<b>More Details : </b> '+Regi.Addinfo__c+'</p>';
            if(Regi.Addinfo__c == null)
                Body8 = ' ';
                          
            DateTime StartTime = Regi.Start__c;
            DateTime EndTime = Regi.End__c;
            String SesStart = StartTime.format('dd.MM.yyyy HH:mm');  
            String SesEnd;  
            if(EndTime != null)
                SesEnd = EndTime.format('dd.MM.yyyy HH:mm');
            else 
                SeSend = ' ';   
            
            String[] toAddresses = new String[] {U.email};
            mail.setSenderDisplayName(Label.CL00299); // Specify who is the sender
            mail.setToAddresses(toAddresses);
            mail.setSubject(Label.CL00301);
            mail.setSaveAsActivity(true);
            String Body0 = '<img src='+HEADER_IMG_URL+' height="76" width="806" >';
            String Body1 = '<p>Dear '+U.Name+',</p><p>Please note that your scheduled <b>bridge front office training session has been Canceled</b>. Another session should be scheduled promptly.</p>';
            String Body11 = '<p> In case of any problem, please liaise with '+SessionOwner.Name+'.</p>';
            String Body2 = '<p><FONT color=#BB0066><b>Canceled session details</b></FONT><br>';
            String Body3 = '<b>Curriculum : </b> '+Regi.Curriculum__c+'<br>'; 
            String Body4 = '<b>Start scheduled : </b> '+SesStart+'<br>';
            String Body5 = '<b>End scheduled : </b> '+SesEnd+'<br>';
            String Body7 = '<b>Location : </b> '+Regi.Location__c+', '+Regi.Address__c+', '+Regi.Zipcode__c+' '+Regi.City__c+'<br>';
            String Body9 = '<p>Kind Regards, </p>';
            String Body10 = '<p>'+SessionOwner.Name+'</p>';
            String Body12 = 'Email : '+SessionOwner.email;
        
            mail.setHTMLBody('<FONT face=Arial color=#222222">'+Body0+Body1+Body11+Body2+Body3+Body4+Body5+Body7+Body8+Body9+Body10+Body12+'</FONT>');
            
            
            // Add the mail to the list to be sent 
            mails.add(mail); 
            
        }
        
         // Send mails
         try
         {
            Messaging.sendEmail(mails);
         }
         catch(Exception e)
         {
             System.debug('---- Send email fail: ' + e.getmessage());
         }
        
    }
    
    // Send a Mail to the related Trainee if a Session is update with Notify registered users = true
    public static void SendSessionNotificationEmail(List<TMT_Session__c> SList)
    {
        set<String> SessionsID = new set<String>();
        set<String> OwnersID = new set<String>();
        map<ID,User> OwnerMap = new map<ID,User>();
        
        For(TMT_Session__c S:SList)
        {
            SessionsID.add(S.Id);
            if(!OwnersID.contains(S.OwnerID))
                OwnersID.add(S.OwnerID);        
        }
        
        // List of all Owners
        List<User> SessionOwners = [SELECT Id, Name, Email FROM User WHERE Id IN :OwnersID];
        System.debug('>>> Session Owners : '+ SessionOwners);
        
        // Map Owners to their Sessions
        For(TMT_Session__c S:SList)
        {
            For(User SO:SessionOwners)    
            {
                if(S.OwnerID == SO.Id)
                    OwnerMap.put(S.Id,SO);
            }
        }
        
    
        List<TMT_Registration__c> RList = [SELECT Id, Name, Trainee__c, Curriculum__c, Start__c, End__c, Location__c, Addinfo__c,
        Address__c, Zipcode__c, City__c, Session__c 
        FROM TMT_Registration__c WHERE Session__c IN :SessionsID];
        
        System.debug('##>>>>> Registrations : '+RList);
        
        List<User> Trainees = new List<User>();
        set<String> TraineesIDs = new set<String>(); 
        Map<TMT_Registration__c,User> RegistrationMap = new Map<TMT_Registration__c,User>();
        
        // Retrieve all Registrations IDs
        For(TMT_Registration__c R:RList)
        {
            TraineesIDs.add(R.Trainee__c);
        }
        
        // Get all related Trainees
        Trainees = [SELECT Id, Name, firstName, LastName, email FROM User WHERE Id IN :TraineesIDs];
        
        System.debug('##>>>> Trainees :' + Trainees);
 
        // Map Trainees and Registrations
        For(TMT_Registration__c R:RList)
        {
            For(User T:Trainees)      
            {
                if(R.Trainee__c == T.Id)
                    RegistrationMap.put(R,T);

            }
        }
        
        //Get Email Template
        EmailTemplate et = [SELECT id FROM EmailTemplate WHERE developerName = 'REGConfirmation'];
                
        System.debug('##>>>> Registration map:' + RegistrationMap );
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

        For(TMT_Registration__c Regi:Rlist)
        {
            // Create new email 
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            // Get the related Trainee
            User U =  RegistrationMap.get(Regi);
            System.debug('## >> Trainee :' + U );
            
            User SessionOwner = OwnerMap.get(Regi.Session__c);
            
            String Body9 = '<b>More Details : </b> '+Regi.Addinfo__c+'</p>';
            if(Regi.Addinfo__c == null)
                Body9 = ' ';
                          
            DateTime StartTime = Regi.Start__c;
            DateTime EndTime = Regi.End__c;
            String SesStart = StartTime.format('dd.MM.yyyy HH:mm');  
            String SesEnd;  
            if(EndTime != null)
                SesEnd = EndTime.format('dd.MM.yyyy HH:mm');
            else 
                SeSend = ' ';   
            
            String[] toAddresses = new String[] {U.email};
            mail.setSenderDisplayName(Label.CL00299); // Specify who is the sender
            mail.setToAddresses(toAddresses);
            //mail.setSubject(Label.CL00301);
            mail.setSubject('Training Session Scheduled');
            mail.setSaveAsActivity(true);
            String Body0 = '<img src='+HEADER_IMG_URL+' height="76" width="806" >';
            String Body1 = '<p>Dear '+U.Name+',</p><p>You are expected to attend to a <b>bridge Front Office training</b> in order to prepare you for the upcoming rollout of this new Customer Relationship Management (CRM) solution. Please find the details of your session below.</p>';
            String Body2 = '<p><FONT color=#BB0066><b>Important</b></FONT><br>';
            String Body3 = '<b>This training session is mandatory</b> and part of the bridge Front Office Deployment. Access to the bFO tool will be granted only after you attended this training. Please liaise with '+SessionOwner.Name+' in case of any problem.<br/>';
            String Body4 = '<p><FONT color=#BB0066><b>Session details</b></FONT><br>';
            String Body5 = '<b>Curriculum : </b> '+Regi.Curriculum__c+'<br>'; 
            String Body6 = '<b>Start scheduled : </b> '+SesStart+'<br>';
            String Body7 = '<b>End scheduled : </b> '+SesEnd+'<br>';
            String Body8 = '<b>Location : </b> '+Regi.Location__c+', '+Regi.Address__c+', '+Regi.Zipcode__c+' '+Regi.City__c+'<br>';
            String Body10 = '<p><FONT color=#BB0066><b>What you have to do now</b></FONT><br>';
            String Body11 = 'Please make your travel arrangements and update your Lotus Notes / Outlook calendar accordingly.<br/>';
            String Body12 = '<p>Thanks and regards,</p>';
            String Body13 = '<p>'+SessionOwner.Name+'</p>';
            String Body14 = 'Email : '+SessionOwner.email;
        
            mail.setHTMLBody('<FONT face=Arial color=#222222">'+Body0+Body1+Body2+Body3+Body4+Body5+Body6+Body7+Body8+Body9+Body10+Body11+Body12+Body13+Body14+'</FONT>');
            
            
            // Add the mail to the list to be sent 
            mails.add(mail); 
            
        }
        
         // Send mails
         try
         {
            Messaging.sendEmail(mails);
         }
         catch(Exception e)
         {
             System.debug('---- Send email fail: ' + e.getmessage());
         }
        
    }

    // Send a Mail to the related Trainee if a Session is Rescheduled
    public static void SendRescheduledEmail(List<TMT_Session__c> SList)
    {
        set<String> SessionsID = new set<String>();
        set<String> OwnersID = new set<String>();
        map<ID,User> OwnerMap = new map<ID,User>();
        
        For(TMT_Session__c S:SList)
        {
            SessionsID.add(S.Id);
            if(!OwnersID.contains(S.OwnerID))
                OwnersID.add(S.OwnerID);        
        }
        
        // List of all Owners
        List<User> SessionOwners = [SELECT Id, Name, Email FROM User WHERE Id IN :OwnersID];
        System.debug('>>> Session Owners : '+ SessionOwners);
        
        // Map Owners to their Sessions
        For(TMT_Session__c S:SList)
        {
            For(User SO:SessionOwners)    
            {
                if(S.OwnerID == SO.Id)
                    OwnerMap.put(S.Id,SO);
            }
        }
        
    
        List<TMT_Registration__c> RList = [SELECT Id, Name, Trainee__c, Curriculum__c, Start__c, End__c, Location__c, Addinfo__c,
        Address__c, Zipcode__c, City__c, Session__c 
        FROM TMT_Registration__c WHERE Session__c IN :SessionsID];
        
        System.debug('##>>>>> Registrations : '+RList);
        
        List<User> Trainees = new List<User>();
        set<String> TraineesIDs = new set<String>(); 
        Map<TMT_Registration__c,User> RegistrationMap = new Map<TMT_Registration__c,User>();
        
        // Retrieve all Registrations IDs
        For(TMT_Registration__c R:RList)
        {
            TraineesIDs.add(R.Trainee__c);
        }
        
        // Get all related Trainees
        Trainees = [SELECT Id, Name, firstName, LastName, email FROM User WHERE Id IN :TraineesIDs];
        
        System.debug('##>>>> Trainees :' + Trainees);
 
        // Map Trainees and Registrations
         For(TMT_Registration__c R:RList)
        {
            For(User T:Trainees)      
            {
                if(R.Trainee__c == T.Id)
                    RegistrationMap.put(R,T);

            }
        }
        
        System.debug('##>>>> Registration map:' + RegistrationMap );
        
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

        For(TMT_Registration__c Regi:Rlist)
        {
            // Create new email 
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            // Get the related Trainee
            User U =  RegistrationMap.get(Regi);
            System.debug('## >> Trainee :' + U );
            
            User SessionOwner = OwnerMap.get(Regi.Session__c);
                  
            String Body8 = '<b>More Details : </b> '+Regi.Addinfo__c+'</p>';
            if(Regi.Addinfo__c == null)
                Body8 = ' ';
                             
            DateTime StartTime = Regi.Start__c;
            DateTime EndTime = Regi.End__c;
            String SesStart = StartTime.format('dd.MM.yyyy HH:mm');  
            String SesEnd;  
            if(EndTime != null)
                SesEnd = EndTime.format('dd.MM.yyyy HH:mm');
            else 
                SeSend = ' ';   
                
            String[] toAddresses = new String[] {U.email};
            mail.setSenderDisplayName(Label.CL00299); // Specify who is the sender
            mail.setToAddresses(toAddresses);
            mail.setSubject(Label.CL00301);
            mail.setSaveAsActivity(true);
            String Body0 = '<img src='+HEADER_IMG_URL+' height="76" width="806" >';
            String Body1 = '<p>Dear '+U.Name+',</p><p>Please note that your <b>bridge front office training session has been rescheduled.</b> Updated details of your session are displayed below.</p>';
            String Body20 = '<p><FONT color=#BB0066><b>Important</b></FONT><br>';
            String Body21 = '<b>This training session is mandatory</b> and part of the bridge Front Office Deployment. Access to the bFO tool will be granted only after you attended this training. Please liaise with '+SessionOwner.Name+' in case of any problem. <br>';
            String Body2 = '<p><FONT color=#BB0066><b>Session details</b></FONT><br>';
            String Body3 = '<b>Curriculum : </b> '+Regi.Curriculum__c+'<br>'; 
            String Body4 = '<b>Start scheduled : </b> '+SesStart+'<br>';
            String Body5 = '<b>End scheduled : </b> '+SesEnd+'<br>';
            String Body7 = '<b>Location : </b> '+Regi.Location__c+', '+Regi.Address__c+', '+Regi.Zipcode__c+' '+Regi.City__c+'<br>';
            
            String Body9 = '<p>Kind Regards, </p>';
            String Body10 = '<p>'+SessionOwner.Name+'</p>';
            String Body12 = 'Email : '+SessionOwner.email;
        
            mail.setHTMLBody('<FONT face=Arial color=#222222">'+Body0+Body1+Body20+Body21+Body2+Body3+Body4+Body5+Body7+Body8+Body9+Body10+Body12+'</FONT>');
            // Add the mail to the list to be sent 
            mails.add(mail); 
            
        }
        
         try
         {
            Messaging.sendEmail(mails);
         }
         catch(Exception e)
         {
             System.debug('---- Send email fail: ' + e.getmessage());
         }
        
    }
}