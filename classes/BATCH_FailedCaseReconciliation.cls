global class BATCH_FailedCaseReconciliation implements  Schedulable,Database.Batchable<sObject> {
    global Integer intLimitNumber;
    global String  strQuery='';

    
    global BATCH_FailedCaseReconciliation(Integer intLmtNumber){ 
        if(intLmtNumber==0 || intLmtNumber==null) 
            intLmtNumber=20000;
        intLimitNumber=intLmtNumber;
    }

    global BATCH_FailedCaseReconciliation(){ 
        intLimitNumber=20000;
    }
    
    global BATCH_FailedCaseReconciliation(String strSOQL){ 
        if(strSOQL.length()!=0)
            strQuery= strSOQL;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        if(strQuery.length()==0)
            return Database.getQueryLocator([Select Id, TECH_TemplateName__c,Origin__c,Team__c,SupportCategory__c,CCCountry__c,CommercialReference_lk__c,Contact__c,CustomerRequest__c,Description__c,TECH_EmailToCaseDebug__c,Family_lk__c,Priority__c,Symptom__c,RelatedSupportCase__c,Status__c,Subject__c,SubSymptom__c,TECH_SPOC__c,TECH_SupportEmail__c,SuppliedEmail__c,ReportingOrganizationId__c, AccountableOrganizationId__c from FailedCase__c where IsReconciled__c=false Order by CreatedDate DESC limit :intLimitNumber]);
        else
            return  Database.getQueryLocator(strQuery);  
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        reconcileFailedCases(scope);
    }

    global void reconcileFailedCases(List<sObject> scope){
        String strErrorId           ='';
        String strErrorMessage      ='';
        String strErrorType         ='';
        String strErrorStackTrace   ='';
        boolean blnRunBatch         =false;
        boolean blnException        = false;
        boolean blnDontAutoRespond  = false;

        Set<String> setFailedCaseId                 = new Set<string>();
        Set<Id> setNewCaseId                        = new Set<Id>();
        Map<String,Id> mapFailedCaseWithNewCaseId   = new Map<String,Id>();
        List<Case> lstCaseTobeInserted              = new List<Case>();
        System.debug('Inside Method');
        blnRunBatch=true;
        if(blnRunBatch || Test.isRunningTest() ){
            Map<String,String> mapFailedCases = new Map<String,String>();
            mapFailedCases.put('TECH_TemplateName__c'        , 'TECH_TemplateName__c'     );
            mapFailedCases.put('Origin__c'                   , 'Origin'                   );
            mapFailedCases.put('Team__c'                     , 'Team__c'                  );
            mapFailedCases.put('SupportCategory__c'          , 'SupportCategory__c'       );
            mapFailedCases.put('CCCountry__c'                , 'CCCountry__c'             );
            mapFailedCases.put('CommercialReference_lk__c'   , 'CommercialReference_lk__c');
            mapFailedCases.put('Contact__c'                  , 'ContactId'                );
            mapFailedCases.put('Description__c'              , 'Description'              );
            mapFailedCases.put('TECH_EmailToCaseDebug__c'    , 'TECH_EmailToCaseDebug__c' );
            mapFailedCases.put('Family_lk__c'                , 'Family_lk__c'             );
            mapFailedCases.put('RelatedSupportCase__c'       , 'RelatedSupportCase__c'    );
            mapFailedCases.put('Priority__c'                 , 'Priority'                 );
            mapFailedCases.put('Symptom__c'                  , 'Symptom__c'               );
            mapFailedCases.put('Status__c'                   , 'Status'                   );
            mapFailedCases.put('Subject__c'                  , 'Subject'                  );
            mapFailedCases.put('TECH_SupportEmail__c'        , 'TECH_SupportEmail__c'     );
            mapFailedCases.put('TECH_SPOC__c'                , 'TECH_SPOC__c'             );
            mapFailedCases.put('SuppliedEmail__c'            , 'SuppliedEmail'            );
            mapFailedCases.put('CustomerRequest__c'          , 'CustomerRequest__c'       );
            mapFailedCases.put('Id'                          , 'TECH_FailedCaseId__c'     );  
            mapFailedCases.put('ReportingOrganizationId__c'  ,'ReportingOrganization__c'  );
            mapFailedCases.put('AccountableOrganizationId__c','AccountableOrganization__c');
        

            for(sObject objFailedCase: scope){
                
                Schema.DescribeSObjectResult FailedCaseDescribeResult = objFailedCase.getsObjectType().getDescribe();
                
                String strCaseObject ='Case';
                Schema.SObjectType CaseSObjectType = Schema.getGlobalDescribe().get(strCaseObject);
                SObject objCase = CaseSObjectType.newSObject();
                        
                for(Schema.SObjectField objField : FailedCaseDescribeResult.fields.getMap().values()){
                    if(objField.getDescribe().isAccessible() && objField.getDescribe().isCreateable() && !objField.getDescribe().isUnique() &&
                        !objField.getDescribe().isAutoNumber() && !objField.getDescribe().isCalculated()){
                            if(mapFailedCases.containsKey(objField.getDescribe().getName())){
                                objCase.put(mapFailedCases.get(objField.getDescribe().getName()),objFailedCase.get(objField.getDescribe().getName()));
                            }
                    }
                    if(objField.getDescribe().getName()=='Id'){
                        objCase.put(mapFailedCases.get(objField.getDescribe().getName()),objFailedCase.get(objField.getDescribe().getName()));
                        setFailedCaseId.add((String) objFailedCase.get(objField.getDescribe().getName()));
                    }
                }
                Case objcaseToInsert = (Case) objCase;
                objcaseToInsert.LastCustomerEmailDate__c = System.now();
                objcaseToInsert.OldestCustomerEmailWithoutReplyDate__c =System.now();
                lstCaseTobeInserted.add(objcaseToInsert);
            }

            if(lstCaseTobeInserted.size()==1){
                SavePoint objReconsileSavePoint = Database.setSavepoint();
                try{
                    Case objNewCase = lstCaseTobeInserted[0];
                    Database.DMLOptions DMLoption = new Database.DMLOptions(); 
                    DMLoption.allowFieldTruncation = true;
                    DMLoption.optAllOrNone = false;
                    if(objNewCase.TECH_SPOC__c==null){
                        blnDontAutoRespond=true;
                    }
                    
                    if(!blnDontAutoRespond){
                        blnDontAutoRespond = AP24_EmailToApex.NoAutoResponseForSender(objNewCase.SuppliedEmail);
                    }

                    if(objNewCase.Origin == System.Label.CLMAY13CCC02 || blnDontAutoRespond || (!Boolean.valueOf(System.Label.CLMAY15CCC01))) { // Test or Origin Fax  or If sender email address is in Dont Auto Respond Settings or if Email send to Agent, Or Global Auto Response = FALSE (Hotfix May 2015)
                        DMLoption.EmailHeader.triggerAutoResponseEmail = false;  
                    }
                    else {
                        DMLoption.EmailHeader.triggerAutoResponseEmail = true;  
                    }

                    DataBase.SaveResult newCase_SR = database.insert(objNewCase,DMLoption);
                    if(objNewCase.id!=null){
                        List<Attachment> attachmentsToNewCase = new List<Attachment>(); 
                        Attachment objTempAttachment;

                        for(Attachment objFailedAttachment: [Select SystemModstamp, ParentId, OwnerId, Name,LastModifiedDate,LastModifiedById, IsPrivate, IsDeleted, Id, Description, CreatedDate, CreatedById, ContentType,
                            BodyLength, Body From Attachment where parentId =:objNewCase.TECH_FailedCaseId__c]){
                                objTempAttachment           = objFailedAttachment.clone(false,false);
                                objTempAttachment.parentId  = objNewCase.Id;
                                attachmentsToNewCase.add(objTempAttachment);
                        }
                        if(attachmentsToNewCase.size()>0){
                            Database.insert(attachmentsToNewCase);
                        }

                        List<FailedEmailMessage__c> lstFailedEmailMessage = new List<FailedEmailMessage__c>([Select Id, FromAddress__c , Incoming__c , ToAddress__c, CcAddress__c, Subject__c , TextBody__c, Headers__c, HTMLBody__c, ParentID__c, MessageDate__c, Status__c from FailedEmailMessage__c where ParentID__c=: objNewCase.TECH_FailedCaseId__c]);
                        if(lstFailedEmailMessage.size()>0){
                            EmailMessage objEmailMessage = new EmailMessage();
                            objEmailMessage.FromAddress     =   lstFailedEmailMessage[0].FromAddress__c     ;
                            objEmailMessage.CcAddress       =   lstFailedEmailMessage[0].CcAddress__c       ;
                            objEmailMessage.Headers         =   lstFailedEmailMessage[0].Headers__c         ;
                            objEmailMessage.HtmlBody        =   lstFailedEmailMessage[0].HTMLBody__c        ;
                            objEmailMessage.Incoming        =   lstFailedEmailMessage[0].Incoming__c        ;
                            objEmailMessage.MessageDate     =   lstFailedEmailMessage[0].MessageDate__c     ;
                            objEmailMessage.ParentId        =   objNewCase.Id        ;
                            objEmailMessage.Status          =   '1'                  ;
                            objEmailMessage.Subject         =   lstFailedEmailMessage[0].Subject__c         ;
                            objEmailMessage.TextBody        =   lstFailedEmailMessage[0].TextBody__c        ;
                            objEmailMessage.ToAddress       =   lstFailedEmailMessage[0].ToAddress__c       ;
                            AP_AsyncEmailMessage.blnfutureCallEnabled=false;
                            Database.insert(objEmailMessage);
                        }
                    }
                }
                catch(Exception ex){
                    Database.rollback(objReconsileSavePoint);
                    blnException=true;
                    strErrorMessage += ex.getTypeName()+' - '+ ex.getMessage() + '\n';
                    strErrorType += ex.getTypeName()+' - '+ ex.getLineNumber() + '\n';
                    strErrorStackTrace += ex.getTypeName()+' - '+ ex.getStackTraceString() + '\n';
                }
                finally{
                    FailedCase__c objFailedCaseFinal = new FailedCase__c(Id = (Id)lstCaseTobeInserted[0].TECH_FailedCaseId__c);
                    if(blnException){
                        objFailedCaseFinal.ReconciliationStatus__c='Failed';
                        objFailedCaseFinal.ReconciliationMessage__c = strErrorMessage + '\n' + strErrorType + '\n' + strErrorStackTrace;
                    }
                    else{
                        objFailedCaseFinal.ReconciliationStatus__c='Success';
                        objFailedCaseFinal.ReconciliationMessage__c = 'Processed Successfully';
                    }
                    objFailedCaseFinal.IsReconciled__c=true;
                    Database.DMLOptions objDMLoption = new Database.DMLOptions();
                    objDMLoption.optAllOrNone = false;
                    objDMLoption.AllowFieldTruncation = true;
                    Database.SaveResult objSaveResult = Database.update(objFailedCaseFinal,objDMLoption);
                    
                }
            }
        }
    }   

    global void finish(Database.BatchableContext BC) {
    }

    //Schedule method
    global void execute(SchedulableContext sc) {
        BATCH_FailedCaseReconciliation objFailedCaseReconciliation = new BATCH_FailedCaseReconciliation(1000);
        database.executeBatch(objFailedCaseReconciliation,1);
    }
}