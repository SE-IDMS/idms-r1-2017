/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: Test Class                                          *
* Created Date: 14.04.2014                                  *
* Tested Class: FieloPRM_AP_BadgeAccountTriggers            *
************************************************************/

@isTest
public with sharing class FieloPRM_AP_BadgeAccountTriggersTest {
  
    public static testMethod void testUnit(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            //account
             Account acc= new Account(
                Name = 'test',
                Street__c = 'Some Street',
                ZipCode__c = '012345',
                PLPartnerLocatorFeatureGranted__c = true,
                PRMAccount__c = true,
                PRMUIMSID__c = 'test123'
            );
            insert acc;
            
             Account acc2= new Account(
                Name = 'test2',
                Street__c = 'Some Street',
                ZipCode__c = '012345',
                PLPartnerLocatorFeatureGranted__c = false,
                PRMAccount__c = true,
                PRMUIMSID__c = 'test1234'
            );
            insert acc2;       
            
            FieloCH__Challenge__c challenge =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective'); 
            FieloCH__ChallengeReward__c rew = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge);

            //badge
            FieloEE__Badge__c badge = new FieloEE__Badge__c(
                Name = 'test ProgramLevel',
                F_PRM_Type__c = 'Program Level',
                F_PRM_BadgeAPIName__c = 'asd',
                F_PRM_Challenge__c = challenge.id
            );
            insert badge;
            
            List<FieloPRM_BadgeAccount__c> badgeAccList = new List<FieloPRM_BadgeAccount__c>();
            //Badge-Account 
            badgeAccList.add(new FieloPRM_BadgeAccount__c(
                Name = 'TestBadgeAccount',
                F_PRM_Account__c = acc.Id,
                F_PRM_Badge__c = badge.Id
            ));
            
            badgeAccList.add(new FieloPRM_BadgeAccount__c(
                Name = 'TestBadgeAccount2',
                F_PRM_Account__c = acc2.Id,
                F_PRM_Badge__c = badge.Id
            ));
            
            badgeAccList.add(new FieloPRM_BadgeAccount__c(
                Name = 'TestBadgeAccount3',
                F_PRM_Account__c = acc2.Id,
                F_PRM_Badge__c = badge.Id
            ));
            insert badgeAccList;
            
            badgeAccList.get(0).Name = 'Modifyed Name';
            update badgeAccList;
            
            delete badgeAccList;
        }
           
    }
}