public with sharing class SyncObjectsResult {
	
	public boolean error{get;set;}
	public String message{get;set;}
	public list<Log> logs{get;set;}
	
	public SyncObjectsResult(){
		this.error = false;
		this.message = '';
		this.logs = new list<Log>();
	}
	
	public SyncObjectsResult(String resultMessage,boolean error){
		this.error = error;
		this.message = resultMessage;
		this.logs = new list<Log>();
	}
	
	public void addError(Log logRec){
		this.error = true;
		this.logs.add(logRec);
	}
	
	public void addInfo(Log logRec){
		this.logs.add(logRec);
	}
	
	public void processDmlResults(list<Database.SaveResult> saveResults,map<String,String> mapToFrom){
		String fromRecordId ;
		String toRecordId ;
		for (Database.SaveResult sr : saveResults) {
			//get Ids
	    	fromRecordId = mapToFrom.get(sr.getId());
	    	toRecordId = sr.getId();
			
		    if (sr.isSuccess()) {
				addInfo(new Log('Success : From record with Id: ' + fromRecordId + ' to record with Id: ' + toRecordId));
		    }else {
		        // Operation failed, so get all errors
		        for(Database.Error err : sr.getErrors()) {
					addInfo(new Log('Error : From record with Id: ' + fromRecordId + ' to record with Id: ' + toRecordId + ' :::: ' + err.getMessage()));
		        }
		    }
		}
	}
	
	public class Log{
		public String message{get;set;}
		
		public Log(){
		}
		public Log(Exception e){
			this.message = e.getMessage();
		}

		public Log(String message){
			this.message = message;
		}

	}
}