/****
Class name:AP_ELLAOffsharingclass

GD Team:
*****/

public class AP_ELLAOffsharing { 
    
    
   
    public static List<sObject> shrsListsObj  = new List<sObject>();    
    public static sObject sharObj;    
    
    Map<string, sObject> MapUpdateTargetObj = new Map<string, sObject>();
    //to sharing method for sObject
    public static Void ShareObjetReadW(Map<id,set<Id>> mapUserRecords,string strShareObj){
      
        for(id PartId:mapUserRecords.keyset()){
            for(id usrId:mapUserRecords.get(PartId)) {
                system.debug('Schoffer-->'+usrId);
                sharObj = (sObject )createNewObject(strShareObj);

                sharObj.put('UserOrGroupId',usrId);
                sharObj.put('AccessLevel', 'edit');
                sharObj.put('ParentId',PartId);                
                shrsListsObj.add(sharObj);
                system.debug('shrsListsObj-->'+shrsListsObj);
                
            }
             
        }           
        if(shrsListsObj.size() > 0) {  
            try {      
            Database.SaveResult[] lsr = Database.insert(shrsListsObj,false);
            }catch(Exception exp) {
            
            }       
        } 
       
    }
   
   public static sObject createNewObject(String strObjtName) {
       
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(strObjtName);        
        
        return targetType.newSObject(); 
    }
    
    public static void RemoveAccesOnObject(Map<string,Set<string>> MapParentAccId,string sObjectName) {
        
        if(MapParentAccId.size() > 0 && MapParentAccId.Values().size() > 0 ) {
            
            Set<string> SetParent=MapParentAccId.keyset();
            set<string> SetUsrAcc = new set<string>();
            for(string strKy:MapParentAccId.keyset()) {
                for(string strset:MapParentAccId.get(strKy)) {
                    SetUsrAcc.add(strset);
                    
                }
            }
            
            
            
            string frSQuery='Select Id from '+sObjectName+' where UserOrGroupId IN:SetUsrAcc and ParentId IN:SetParent and RowCause!=\''+'Owner'+'\'';
            system.debug('TESTInside---->'+frSQuery );
            List<sObject> sobjList = Database.query(frSQuery);
            system.debug('TEST---->'+sobjList );

            if(sobjList.size() > 0) {

                delete sobjList;  
            }
        } 
        
        
    }
   
}