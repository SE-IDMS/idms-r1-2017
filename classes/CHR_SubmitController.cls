public with sharing class CHR_SubmitController { 
  
  public ApexPages.StandardController controller;
  
  List<REF_RoutingBOBS__c> rout {get;set;}
  
  public CHR_ChangeReq__c oChangeReq {get;set;}
  
  List<String> parts = null;
  
  public String geoSelection {get;set;}
  
  public String teamHierachy {get;set;}
  
  private List<SelectOption> items;
  
  public String bo {get;set;}
  
  private String orgLevel1 = '';
  private String orgLevel2 = '';
  private String orgLevel3 = '';
  private String orgLevel4 = '';
  
  public Boolean hasNoExtactRouting {get;set;}
  
  public Boolean selectedRequesterWithNoSesa {get;set;}
  
  private Boolean alreadyRefreshed {get;set;}
  
  public List<SelectOption> getItems() {
    List<SelectOption> items = new List<SelectOption>();

    //default value
    items.add(new SelectOption('','--Select another Business Organization --'));

    //query force.com database to get the organization records.
    for (REF_OrganizationRecord__c orgrec: [SELECT id, name,Type__c FROM REF_OrganizationRecord__c WHERE Type__c = 'Business Organization']) {
        items.add(new SelectOption(orgrec.id,orgrec.name));
    }

    return items;
    }
  
  public String sesa {
    get {
            //Check if there is a requester in the stakeholder list (add a condition in the where clause    )
            for (ChangeRequestStakeholder__c stakeholder:[select ActorNew__r.SEContactID__c from ChangeRequestStakeholder__c where ChangeRequest__c=:oChangeReq.Id and Role__c='Requester']){
                if (stakeholder.ActorNew__r.SEContactID__c != '' && stakeholder.ActorNew__r.SEContactID__c != null) {
                    return stakeholder.ActorNew__r.SEContactID__c;
                } else {
                    if (!alreadyRefreshed) { 
                        selectedRequesterWithNoSesa = true;
                    }
                }
            }
            
            //List<User> userlist = [select id,SEContactID__c from User where id = :UserInfo.getUserId()];
            List<User> userlist = [select id,SEContactIDUser__c from User where id = :UserInfo.getUserId()];
            if ( userlist!= null && !userlist.isEmpty()) {
                //return userlist[0].SEContactID__c;
                return userlist[0].SEContactIDUser__c;
            } else {
                return null;
            }
        }
    set;
  }
  
  public Boolean hasWrongProfile {
    get {
        if (sesa == null) {
            return true;
        } else {
            return false;
        }
    }
    set;
  }
  
  public REF_OrganizationRecord__c refBO {
    get { 
        if (oChangeReq.TECH_RoutingTree__c!= null) return parseRoutingTree(1);
        return null;
        }
    set;
  }
  
  public REF_OrganizationRecord__c refLevel2 {
    get { 
        if (oChangeReq.TECH_RoutingTree__c!= null) return parseRoutingTree(2);
        return null;
        }
    set;
  }
  
  public REF_OrganizationRecord__c refLevel3 {
    get { 
        if (oChangeReq.TECH_RoutingTree__c!= null) return parseRoutingTree(3);
        return null;
        }
    set;
  }
  
  public REF_OrganizationRecord__c refLevel4 {
    get { 
        if (oChangeReq.TECH_RoutingTree__c!= null) return parseRoutingTree(4);
        return null;
        }
    set;
  }

  public CHR_SubmitController (ApexPages.StandardController controller){ 
    this.controller = controller; 
    alreadyRefreshed = false;
    mainInit(); 
  }



  private void mainInit(){ 
    //Object Initialization 
    oChangeReq = (CHR_ChangeReq__c)controller.getRecord();
    hasNoExtactRouting = true; 
    selectedRequesterWithNoSesa = false;
    
    //Build default geographic scope based on the requester info
    if (oChangeReq.TECH_RoutingTree__c==null){
        //Contact requester;
        DMTStakeholder__c requester;
        try{
            //requester = [select id,L1__c,L2__c,L3__c,L4__c from Contact where SEContactID__c =:sesa];
            requester = [select id,L1__c,L2__c,L3__c,L4__c from DMTStakeholder__c where SEContactID__c =:sesa];
        } catch (Exception e ){
            //exception happens when there is no contact matching the current user's SESA
            return;
        }
        oChangeReq.TECH_RoutingTree__c='';
        if (requester.L1__c <> null) oChangeReq.TECH_RoutingTree__c+=requester.L1__c ;
        if (requester.L2__c <> null) oChangeReq.TECH_RoutingTree__c+=(oChangeReq.TECH_RoutingTree__c.length()>0?'-':'')+requester.L2__c ;
        if (requester.L3__c <> null) oChangeReq.TECH_RoutingTree__c+=(oChangeReq.TECH_RoutingTree__c.length()>0?'-':'')+requester.L3__c ;
        if (requester.L4__c <> null) oChangeReq.TECH_RoutingTree__c+=(oChangeReq.TECH_RoutingTree__c.length()>0?'-':'')+requester.L4__c ;
        System.debug('oChangeReq.TECH_RoutingTree__c=>'+oChangeReq.TECH_RoutingTree__c);
    }    
     geoSelection = oChangeReq.TECH_RoutingTree__c;
    
    searchMathingTeam();
    
  }
  
  private Map<String,String> getOrgRecInfo(List<String> parts) {
    Map<String,String> ret = null;
    if ((parts != null) && !parts.isEmpty()) {
        ret = new Map<String,String>();
        for (REF_OrganizationRecord__c orgRecs : [SELECT id, name,Type__c FROM REF_OrganizationRecord__c WHERE id in :parts]) {
            ret.put(orgRecs.Type__c,orgRecs.Id);
        }
        return ret;
    } else {
        return ret;
    }
  }
  
  public REF_OrganizationRecord__c parseRoutingTree(Integer level){
    if (geoSelection<>null) oChangeReq.TECH_RoutingTree__c = geoSelection;
    if (oChangeReq.TECH_RoutingTree__c!= null){
        //we parse the tree
        System.debug('@@@@@@' + oChangeReq.TECH_RoutingTree__c);
        list<String> parts = oChangeReq.TECH_RoutingTree__c.split('-');
        System.debug('@@@@@@' + parts);
        System.debug('@@@@@@' + parts.size());
        //Next, we query from the org record table to get the labels associated to the levels
        Map<Id,REF_OrganizationRecord__c> orgRecMap = new Map<Id,REF_OrganizationRecord__c>();
        for (REF_OrganizationRecord__c orgRec : [SELECT id, name,Type__c FROM REF_OrganizationRecord__c WHERE id in :parts]) {
            orgRecMap.put(orgRec.Id,orgRec);
        }
        if (level <= parts.size() && parts.size()>1){
            if (orgRecMap.containskey(parts[level-1])) {
                return orgRecMap.get(parts[level-1]);
            }
        } else if (level <= parts.size() && parts.size()==1){
            if (!parts[0].equals('')) {
                return orgRecMap.get(parts[level-1]);
            }
        }
    }
            return null;
  }
  
  
  public pagereference passUserSelection() {
    oChangeReq.TECH_RoutingTree__c = geoSelection;
    parts = (geoSelection).split('-');
    searchMathingTeam();
    //oChangeReq.TeamReference__c = parts[parts.size()-1];
    return null; 
  }
  
  public pagereference reinitializeBO() {
    oChangeReq.TECH_RoutingTree__c=bo;
    alreadyRefreshed = true;
    mainInit();
    refBO=parseRoutingTree(1);      
    selectedRequesterWithNoSesa = false;
    return null;
  }
  
  public void searchMathingTeam(){
    String filterQuery='';
    if (refBO<>null)    filterQuery += ' and L1__c = \''+refBO.Id+'\'';
    else filterQuery += ' and L1__c = null';
    if (refLevel2<>null)    filterQuery += ' and L2__c = \''+refLevel2.Id+'\'';
    else filterQuery += ' and L2__c = null';
    if (refLevel3<>null)    filterQuery += ' and L3__c = \''+refLevel3.Id+'\'';
    else filterQuery += ' and L3__c = null';
    if (refLevel4<>null)    filterQuery += ' and L4__c = \''+refLevel4.Id+'\'';
    else filterQuery += ' and L4__c = null';    
    if (oChangeReq.Process__c<>null) filterQuery += ' and Process__c = \''+oChangeReq.Process__c+'\'';
    else filterQuery += ' and Process__c = null';
    
    String queryString ='select TeamReference__c from  REF_RoutingBOBS__c where TeamReference__c<> null ' +  filterQuery ;
    queryString += ' limit 1';
    
    List<Sobject> results= new List<SObject>();
    try{
        results = Database.query(queryString);
        if (results.size()>0){
             oChangeReq.TeamReference__c = ((REF_RoutingBOBS__c)results[0]).TeamReference__c;
            hasNoExtactRouting = false;
            
        }else {
            oChangeReq.TeamReference__c = null;
            hasNoExtactRouting= true;
        }
    } catch (Exception e){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }
    
  }
  
  public pagereference updateTeamF(){ 
    if (geoSelection != null && !geoSelection.equals('')) {
    parts = (geoSelection).split('-');
    
    //oChangeReq.TeamReference__c = teamc;
    System.debug('@@@@@@@@@@@@ geoSelection : ' + geoSelection);
    System.debug('@@@@@@@@@@@@ parts[parts.size()-1] : ' + parts[parts.size()-1]);
    Map<String,String> orgRecs = getOrgRecInfo(parts);
    if (orgRecs.containsKey('Country')) {
        oChangeReq.OrgCountry__c = orgRecs.get('Country');
    } else {
        oChangeReq.OrgCountry__c = null;
    }
    if (orgRecs.containsKey('Geography')) {
        oChangeReq.OrgSchneiderGeo__c = orgRecs.get('Geography');
    } else {
        oChangeReq.OrgSchneiderGeo__c = null;
    }
    if (orgRecs.containsKey('Process')) {
        oChangeReq.OrgProcess__c = orgRecs.get('Process');
    } else  {
        oChangeReq.OrgProcess__c = null;
    }
    if (orgRecs.containsKey('Business Organization')) {
        oChangeReq.OrgBusinessOrg__c = orgRecs.get('Business Organization');
    } else {
        oChangeReq.OrgBusinessOrg__c = null;
    }
    
    //oChangeReq.TeamReference__c = parts[parts.size()-1];
    
    //Saving the routing path
    oChangeReq.TECH_RoutingTree__c = geoSelection;
    
    update oChangeReq; //mainInit(); 
    }
    return new PageReference('/'+oChangeReq.Id); 
  }
  
  

}