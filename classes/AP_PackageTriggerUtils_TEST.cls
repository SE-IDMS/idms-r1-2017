/**
13-June-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for PackageBeforeInsert,   PackageBeforeUpdate and AP_PAckageTriggerUtils
Modified by Pooja Suresh: Added code to test trigger of proactive case creation
 */
@isTest
private class   AP_PackageTriggerUtils_TEST {

    static testMethod void myUnitTest() {
        
        PackageTemplate__c PKGTemplate = Utils_TestMethods.createPkgTemplate();
        insert PKGTemplate;
        
        //Country__c objPOCCountry = Utils_TestMethods.createCountry();
        //insert(objPOCCountry);

        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id , null);
        insert testContract;
        
        Package__c Pkg = Utils_TestMethods.createPackage(testContract.Id , PKGTemplate.Id);
        Pkg.status__c = 'Draft';
        insert Pkg;

        CustomerCareTeam__c proactiveSupportTeam = new CustomerCareTeam__c(Name='Agent A Primary Team',LevelOfSupport__c='Primary'); //CCCountry__c=objPOCCountry.ID,
        insert proactiveSupportTeam;
        
        list<CS_SupportContractLevel__c> customSetting = new list<CS_SupportContractLevel__c>();
        
        CS_SupportContractLevel__c cs1 =new CS_SupportContractLevel__c();
        cs1.name = 'Level 1';
        cs1.SupportContractLevel__c = 'PLATINUM (Single point of contact, technical resource)';
        customSetting.add(cs1);
            
        CS_SupportContractLevel__c cs2 =new CS_SupportContractLevel__c();
        cs2.name = 'Level 2';
        cs2.SupportContractLevel__c = 'GOLD (24X7 Technical Support)';
        customSetting.add(cs2);
        
        CS_SupportContractLevel__c cs3 =new CS_SupportContractLevel__c();
        cs3.name = 'Level 3';
        cs3.SupportContractLevel__c = 'CHANNEL (ACE Distributor Technical )';
        customSetting.add(cs3);
        
        CS_SupportContractLevel__c cs4 =new CS_SupportContractLevel__c();
        cs4.name = 'Level 4';
        cs4.SupportContractLevel__c = 'SILVER (24x5 technical support)';
        customSetting.add(cs4);
        
        CS_SupportContractLevel__c cs5 =new CS_SupportContractLevel__c();
        cs5.name = 'Level 5';
        cs5.SupportContractLevel__c = 'BRONZE (12x5 Technical Support)';
        customSetting.add(cs5);
        
        insert customSetting;
       // Utils_SDF_Methodology.canTrigger('AP_PackageTriggerUtils') = true;
        try
        {
            Pkg.status__c = 'Active';
            Pkg.AutomaticProactiveCase__c = true;
            Pkg.ProactiveCaseSupportTeam__c = proactiveSupportTeam.id;
            Pkg.TECH_TriggerFirstProactiveCase__c = true;
            Pkg.TECH_TriggerSecondProactiveCase__c = true;
            update Pkg;
        }
        catch(Exception e)
        {
            System.assert( e.getMessage().contains(System.Label.CLSEP12CCC27), e.getMessage() );
        }
        testContract.status = 'Activated';
        testContract.LeadingBusinessBU__c = System.Label.CLDEC12CCC01;
        update testContract;
        Pkg.startdate__c = system.today();
        Pkg.enddate__c = system.today()+2;
        Pkg.Support_Level__c = 'Level 3';
        update Pkg;
        
        delete Pkg;
        
        
    }
}