//Generated by wsdl2apex
/**
•   Created By: 
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class contains subclass for UserManager in UIMS 
**/  
public class WS_IdmsUimsAuthUserManagerV2Service{
    public class resetPassword {
        public String callerFid;
        public String federatedId;
        public WS_IdmsUimsAuthUserManagerV2Service.accessElement application;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] application_type_info = new String[]{'application','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','federatedId','application'};
    }
    public class RequestedEntryNotExistsException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class LdapTemplateNotReadyException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class identity {
        public String federatedID;
        public String email;
        public String firstName;
        public String lastName;
        public String languageCode;
        private String[] federatedID_type_info = new String[]{'federatedID','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] email_type_info = new String[]{'email','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] firstName_type_info = new String[]{'firstName','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] lastName_type_info = new String[]{'lastName','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] languageCode_type_info = new String[]{'languageCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'federatedID','email','firstName','lastName','languageCode'};
    }
    public class RequestedInternalUserException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class createIdentity {
        public String callerFid;
        public WS_IdmsUimsAuthUserManagerV2Service.userV5 identity;
        public WS_IdmsUimsAuthUserManagerV2Service.accessElement application;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] identity_type_info = new String[]{'identity','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] application_type_info = new String[]{'application','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','identity','application'};
    }
    public class InactiveUserImsException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class searchUser {
        public String callerFid;
        public String email;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] email_type_info = new String[]{'email','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','email'};
    }
    public class resetPasswordResponse {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class childs_element {
        public WS_IdmsUimsAuthUserManagerV2Service.accessElement[] access;
        private String[] access_type_info = new String[]{'access','http://uimsv2.service.ims.schneider.com/',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'access'};
    }
    public class createdIdentityReport {
        public String federatedID;
        public Boolean hasBeenCreated;
        public String errorMessage;
        private String[] federatedID_type_info = new String[]{'federatedID','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] hasBeenCreated_type_info = new String[]{'hasBeenCreated','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] errorMessage_type_info = new String[]{'errorMessage','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'federatedID','hasBeenCreated','errorMessage'};
    }
    public class reactivateResponse {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class IMSServiceSecurityCallNotAllowedException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class InvalidImsPropertiesFileException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class accessElement {
        public String type_x;
        public String id;
        public WS_IdmsUimsAuthUserManagerV2Service.childs_element childs;
        private String[] type_x_type_info = new String[]{'type','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] id_type_info = new String[]{'id','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] childs_type_info = new String[]{'childs','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'type_x','id','childs'};
    }
    public class InvalidImsServiceMethodArgumentException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class userV5 {
        public String contactID;
        public String federatedID;
        public String email;
        public String firstName;
        public String lastName;
        public String languageCode;
        private String[] federatedID_type_info = new String[]{'federatedID','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] email_type_info = new String[]{'email','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] firstName_type_info = new String[]{'firstName','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] lastName_type_info = new String[]{'lastName','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] languageCode_type_info = new String[]{'languageCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        // private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        // private String[] field_order_type_info = new String[]{'federatedID','email','firstName','lastName','languageCode'};

        public String countryCode;
        public String companyId;
        public String employeeNumber;
        public String fax;
        public String goldenId;
        public String jobDescription;
        public String jobFunction;
        public String jobTitle;
        public String cell;
        public String partnerAccountId;
        public Boolean primaryContact;
        public String sourceSystemId;
        public String phone;
        public String salutation;
        public String channel;
        public String subChannel;
        public String state;
        // added by sudhanshu
        public String addInfoAddress;
        // end
        public String addInfoAddressECS;
        public String associatedBrand;
        public String bdComContents;
        public String contactPreference;
        public String county;
        public String countyECS;
        public String deletionFlag;
        public String domainOfExpertise;
        public String facsimileTelephoneExtension;
        public String facsimileTelephoneNumberPersonal;
        public String gender;
        public String givenNameECS;
        public String howHeard;
        public String howHeardOtherReason;
        public String inheritAccountAddress;
        public String itComContents;
        public String jobFunctionLabel;
        public String jobTitleLabel;
        public String localityName;
        public String localityNameECS;
        public String mailPersonal;
        public String middleName;
        public String middleNameECS;
        public String mobilePersonal;
        public String note;
        public String pmComContents;
        public String postalCode;
        public String postOfficeBox;
        public String postOfficeBoxCode;
        public Boolean prefComCall;
        public Boolean prefComEmail;
        public Boolean prefComFax;
        public Boolean prefComPostMail;
        public Boolean prefComSurvey;
        public Boolean prefComText;
        public String publisherGoldenId;
        public String pwComContents;
        public String scontIdReportsTo;
        public String sdhVersion;
        public String securityComContents;
        public String snECS;
        public String strategicThemes;
        public String street;
        public String streetECS;
        public String telephoneExtension;
        public String telephoneNumberPersonal;
        public String timeZone;
        public Boolean isApproved;
        public Boolean completionFlag;
        public String bfoId;
        public Boolean isInternalUser;
        public Boolean isActive;
        private String[] countryCode_type_info = new String[]{'countryCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] companyId_type_info = new String[]{'companyId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] employeeNumber_type_info = new String[]{'employeeNumber','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] fax_type_info = new String[]{'fax','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] goldenId_type_info = new String[]{'goldenId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] jobDescription_type_info = new String[]{'jobDescription','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] jobFunction_type_info = new String[]{'jobFunction','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] jobTitle_type_info = new String[]{'jobTitle','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] cell_type_info = new String[]{'cell','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] partnerAccountId_type_info = new String[]{'partnerAccountId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] primaryContact_type_info = new String[]{'primaryContact','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] sourceSystemId_type_info = new String[]{'sourceSystemId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] phone_type_info = new String[]{'phone','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] salutation_type_info = new String[]{'salutation','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] channel_type_info = new String[]{'channel','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] subChannel_type_info = new String[]{'subChannel','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] state_type_info = new String[]{'state','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        // added
        private String[] addInfoAddress_type_info = new String[]{'addInfoAddress','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        //
        private String[] addInfoAddressECS_type_info = new String[]{'addInfoAddressECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] associatedBrand_type_info = new String[]{'associatedBrand','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] bdComContents_type_info = new String[]{'bdComContents','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] contactPreference_type_info = new String[]{'contactPreference','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] county_type_info = new String[]{'county','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] countyECS_type_info = new String[]{'countyECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] deletionFlag_type_info = new String[]{'deletionFlag','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] domainOfExpertise_type_info = new String[]{'domainOfExpertise','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] facsimileTelephoneExtension_type_info = new String[]{'facsimileTelephoneExtension','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] facsimileTelephoneNumberPersonal_type_info = new String[]{'facsimileTelephoneNumberPersonal','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] gender_type_info = new String[]{'gender','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] givenNameECS_type_info = new String[]{'givenNameECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] howHeard_type_info = new String[]{'howHeard','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] howHeardOtherReason_type_info = new String[]{'howHeardOtherReason','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] inheritAccountAddress_type_info = new String[]{'inheritAccountAddress','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] itComContents_type_info = new String[]{'itComContents','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] jobFunctionLabel_type_info = new String[]{'jobFunctionLabel','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] jobTitleLabel_type_info = new String[]{'jobTitleLabel','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] localityName_type_info = new String[]{'localityName','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] localityNameECS_type_info = new String[]{'localityNameECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] mailPersonal_type_info = new String[]{'mailPersonal','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] middleName_type_info = new String[]{'middleName','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] middleNameECS_type_info = new String[]{'middleNameECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] mobilePersonal_type_info = new String[]{'mobilePersonal','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] note_type_info = new String[]{'note','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] pmComContents_type_info = new String[]{'pmComContents','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postalCode_type_info = new String[]{'postalCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postOfficeBox_type_info = new String[]{'postOfficeBox','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] postOfficeBoxCode_type_info = new String[]{'postOfficeBoxCode','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] prefComCall_type_info = new String[]{'prefComCall','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] prefComEmail_type_info = new String[]{'prefComEmail','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] prefComFax_type_info = new String[]{'prefComFax','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] prefComPostMail_type_info = new String[]{'prefComPostMail','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] prefComSurvey_type_info = new String[]{'prefComSurvey','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] prefComText_type_info = new String[]{'prefComText','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] publisherGoldenId_type_info = new String[]{'publisherGoldenId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] pwComContents_type_info = new String[]{'pwComContents','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] scontIdReportsTo_type_info = new String[]{'scontIdReportsTo','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] sdhVersion_type_info = new String[]{'sdhVersion','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] securityComContents_type_info = new String[]{'securityComContents','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] snECS_type_info = new String[]{'snECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] strategicThemes_type_info = new String[]{'strategicThemes','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] street_type_info = new String[]{'street','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] streetECS_type_info = new String[]{'streetECS','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] telephoneExtension_type_info = new String[]{'telephoneExtension','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] telephoneNumberPersonal_type_info = new String[]{'telephoneNumberPersonal','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] timeZone_type_info = new String[]{'timeZone','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] isApproved_type_info = new String[]{'isApproved','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] completionFlag_type_info = new String[]{'completionFlag','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] bfoId_type_info = new String[]{'bfoId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] isInternalUser_type_info = new String[]{'isInternalUser','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] isActive_type_info = new String[]{'isActive','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        //private String[] field_order_type_info = new String[]{'federatedID','email','firstName','lastName','languageCode','countryCode','companyId','employeeNumber','fax','goldenId','jobDescription','jobFunction','jobTitle','cell','partnerAccountId','primaryContact','sourceSystemId','phone','salutation','channel','subChannel','state','addInfoAddressECS','associatedBrand','bdComContents','contactPreference','county','countyECS','deletionFlag','domainOfExpertise','facsimileTelephoneExtension','facsimileTelephoneNumberPersonal','gender','givenNameECS','howHeard','howHeardOtherReason','inheritAccountAddress','itComContents','jobFunctionLabel','jobTitleLabel','localityName','localityNameECS','mailPersonal','middleName','middleNameECS','mobilePersonal','note','pmComContents','postalCode','postOfficeBox','postOfficeBoxCode','prefComCall','prefComEmail','prefComFax','prefComPostMail','prefComSurvey','prefComText','publisherGoldenId','pwComContents','scontIdReportsTo','sdhVersion','securityComContents','snECS','strategicThemes','street','streetECS','telephoneExtension','telephoneNumberPersonal','timeZone','isApproved','completionFlag','bfoId','isInternalUser','isActive'};
        private String[] field_order_type_info = new String[]{'federatedID','email','firstName','lastName','languageCode','countryCode','companyId','employeeNumber','fax','goldenId','jobDescription','jobFunction','jobTitle','cell','partnerAccountId','primaryContact','sourceSystemId','phone','salutation','channel','subChannel','state','addInfoAddress','addInfoAddressECS','associatedBrand','bdComContents','contactPreference','county','countyECS','deletionFlag','domainOfExpertise','facsimileTelephoneExtension','facsimileTelephoneNumberPersonal','gender','givenNameECS','howHeard','howHeardOtherReason','inheritAccountAddress','itComContents','jobFunctionLabel','jobTitleLabel','localityName','localityNameECS','mailPersonal','middleName','middleNameECS','mobilePersonal','note','pmComContents','postalCode','postOfficeBox','postOfficeBoxCode','prefComCall','prefComEmail','prefComFax','prefComPostMail','prefComSurvey','prefComText','publisherGoldenId','pwComContents','scontIdReportsTo','sdhVersion','securityComContents','snECS','strategicThemes','street','streetECS','telephoneExtension','telephoneNumberPersonal','timeZone','isApproved','completionFlag','bfoId','isInternalUser','isActive'};
    }
    public class createIdentityResponse {
        public WS_IdmsUimsAuthUserManagerV2Service.createdIdentityReport return_x;
        private String[] return_x_type_info = new String[]{'return','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class UnexpectedLdapResponseException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class reactivate {
        public String callerFid;
        public String federatedId;
        public WS_IdmsUimsAuthUserManagerV2Service.accessElement application;
        private String[] callerFid_type_info = new String[]{'callerFid','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] application_type_info = new String[]{'application','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'callerFid','federatedId','application'};
    }
    public class searchUserResponse {
        public WS_IdmsUimsAuthUserManagerV2Service.userFederatedIdAndType return_x;
        private String[] return_x_type_info = new String[]{'return','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class SecuredImsException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class userFederatedIdAndType {
        public String federatedId;
        public Boolean isInternal;
        private String[] federatedId_type_info = new String[]{'federatedId','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] isInternal_type_info = new String[]{'isInternal','http://uimsv2.service.ims.schneider.com/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'federatedId','isInternal'};
    }
    public class ImsMailerException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
    public class ImsLdapException extends Exception {
        public String label;
        private String[] label_type_info = new String[]{'label','http://uimsv2.service.ims.schneider.com/',null,'1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://uimsv2.service.ims.schneider.com/','false','false'};
        private String[] field_order_type_info = new String[]{'label'};
    }
}