/*
25-Apr-2013    Srinivas Nallapati    Test Class
*/
@isTest
public class VFC_NewProgramRequirement_Test{
    
    static testMethod void newProgramRequirement1()
    {
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        User u = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        u.BypassVR__c = true;
        u.country__c = country.countrycode__c;
        insert u;
        
        //PartnerPRogram__c globalProgram  = new PartnerPRogram__c();
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalProgram.TECH_CountriesId__c = country.id;
        insert globalProgram;
        
        ProgramLevel__c pl1 = new ProgramLevel__C();
        pl1.PartnerProgram__C = globalProgram.id;
        pl1.name = 'Level test 1';
        pl1.ModifiablebyCountry__c = 'Yes';
        pl1.Hierarchy__c = '2';
        pl1.description__c = 'test description1';
        insert pl1; 
        
        ProgramRequirement__c pr = new ProgramRequirement__c();
        
        ApexPages.currentPage().getParameters().put(Label.CLMAY13PRM01,  pl1.name);
        ApexPages.currentPage().getParameters().put(Label.CLMAY13PRM29,  pl1.id);
        ApexPages.currentPage().getParameters().put(Label.CLMAY13PRM32,  '012Z00000008hG8');
        ApexPages.currentPage().getParameters().put(Label.CLMAY13PRM31,  globalProgram.id);
        ApexPages.currentPage().getParameters().put(Label.CLMAY13PRM30,  globalProgram.name);
        ApexPages.currentPage().getParameters().put('retUrl',  '/'+pl1.id);
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(pr);
        VFC_NewProgramRequirement myPageCon1 = new VFC_NewProgramRequirement(sdtCon1);
        myPageCon1.goToProgramReqEditPage();
        system.debug('###### '+myPageCon1.goToProgramReqEditPage());
    }
    
}//End of test class