/*
    Description: Test Class for VFC_OpportunityLinesDataMigration
*/
@isTest
private class VFC_OpportunityLinesDataMigration_TEST
{
    static testMethod void testOppLineUpdates1()
    {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();
        List<OPP_Product__c> prods = new List<OPP_Product__c>();
        Integer numBatchJobs;
        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        Opportunity opp = Utils_TestMethods.createOpportunity(acc.Id);
        opp.Amount = 1000;
        opp.StageName = '3 - Identify & Qualify';
        opp.TECH_IsOpportunityUpdate__c  = true;
        opp.Reason__c = 'test';
        opp.OpportunityScope__c= 'ITSL2';
        opp.DataMigration__c = false;
        insert opp;
        
        Opportunity opp5 = Utils_TestMethods.createOpportunity(acc.Id);
        opp5.Amount = 1000;
        opp5.StageName = '3 - Identify & Qualify';
        opp5.TECH_IsOpportunityUpdate__c  = true;
        opp5.Reason__c = 'test';
        opp5.OpportunityScope__c= 'ITSL2';
        opp5.DataMigration__c = false;
        insert opp5;
        
        Opportunity opp6 = Utils_TestMethods.createOpportunity(acc.Id);
        opp6.Amount = 1000;
        opp6.StageName = '6 - Negotiate & Win';
        opp6.TECH_IsOpportunityUpdate__c  = true;
        opp6.Reason__c = 'test';
        opp6.OpportunityScope__c= 'ITSL2';
        opp6.DataMigration__c = false;
        insert opp6;
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp6.Id);
            pl1.Quantity__c = 0;
            pl1.ProductBU__c = 'ENERGY';
            pl1.ProductLine__c = 'EN';
            prodLines.add(pl1);
        }
        Database.SaveResult[] lsr = Database.Insert(prodLines);
        OPP_Product__c product = new OPP_Product__c();
        product.BusinessUnit__c = 'BD';
        product.Name = 'ITSL2';
        product.IsActive__c = true;
        product.ProductFamily__c='BBTMP';
        product.TECH_PM0CodeInGMR__c = '____';
        prods.add(product);
        
        OPP_Product__c product1 = new OPP_Product__c();
        product1.BusinessUnit__c = 'BD';
        product1.Name = 'TESt';
        product1.IsActive__c = true;
        product1.ProductFamily__c='BBTMP';
        product1.TECH_PM0CodeInGMR__c = '____';
        prods.add(product1);
        
        Database.insert(prods);

        Revenue_Line__c rl5 = new Revenue_Line__c();
        rl5.opportunity__c = opp5.id;
        rl5.Unit_Price__c = 0;
        insert rl5;

        List<Opportunity> opps = new List<Opportunity>();
        
        ApexPages.StandardSetController  controller = new ApexPages.StandardSetController(opps);
        VFC_OpportunityLinesDataMigration oppSrch = new VFC_OpportunityLinesDataMigration(controller);
        oppSrch.stagesToOperate.add('3 - Identify & Qualify');
        oppSrch.stagesToOperate.add('6 - Negotiate & Win');
        oppSrch.stagesToOperate.add('0 - Closed');
        oppSrch.canRunBatch = true;
        oppSrch.OpportunityIds = opp.Id+','+opp5.Id+','+opp6.Id+',';
        oppSrch.prepareMaps();
        oppSrch.ProcessOpportunites();
        numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
        if(numBatchJobs<5 && test.isRunningTest())
            oppSrch.processBatch();
        oppSrch.statusUpdate();
        oppSrch.getItems();
    }
    
    static testMethod void testOppLineUpdates2()
    {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();
        List<OPP_Product__c> prods = new List<OPP_Product__c>();
        Integer numBatchJobs;
        
     
        Country__c country1 = Utils_TestMethods.createCountry();
        insert country1;
     
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
    
        OPP_Product__c product = new OPP_Product__c();
        product.BusinessUnit__c = 'BD';
        product.Name = 'ITSL2';
        product.IsActive__c = true;
        product.ProductFamily__c='BBTMP';
        product.TECH_PM0CodeInGMR__c = '____';
        prods.add(product);
        
        OPP_Product__c product1 = new OPP_Product__c();
        product1.BusinessUnit__c = 'BD';
        product1.Name = 'TESt';
        product1.IsActive__c = true;
        product1.ProductFamily__c='BBTMP';
        product1.TECH_PM0CodeInGMR__c = '____';
        prods.add(product1);
        
        Database.insert(prods);
        Opportunity opp3 = Utils_TestMethods.createOpportunity(acc.Id);
        opp3.TECH_IsOpportunityUpdate__c  = true;
        opp3.Reason__c = 'test';
        opp3.OpportunityScope__c= 'ITSL2';
        opp3.DataMigration__c = false;
        opp3.Status__c = 'Cancelled by Schneider';
        insert opp3;
        
        Competitor__c comp = Utils_TestMethods.createCompetitor();
        insert comp;
        
        CompetitorCountry__c compCountry= Utils_TestMethods.createCompetitorCountry(comp.id, country1.id);
        insert compCountry;
        
        
        Opportunity opp8 = Utils_TestMethods.createOpportunity(acc.Id);
        opp8.TECH_IsOpportunityUpdate__c  = true;
        opp8.Reason__c = 'test';
        opp8.OpportunityScope__c= 'ITSL2';
        opp8.DataMigration__c = false;
        opp8.StageName = '6 - Negotiate & Win';
        insert opp8;
        
        OPP_OpportunityCompetitor__c opComp = new OPP_OpportunityCompetitor__c(TECH_CompetitorName__c=comp.id, OpportunityName__C = opp8.id);
        insert opComp;
        
        Opportunity opp = [select StageName, Status__c from Opportunity where Id=:opp8.Id];
        
        opp.StageName = '7 - Deliver & Validate';
        opp.Status__c = 'Won - Deliver & Validate by Partner';
        update opp;
        
        List<Opportunity> opps = new List<Opportunity>();
        opps.add(opp3);
        ApexPages.StandardSetController  controller = new ApexPages.StandardSetController(opps);
        VFC_OpportunityLinesDataMigration oppSrch = new VFC_OpportunityLinesDataMigration(controller);
        oppSrch.stagesToOperate.add('6 - Negotiate & Win');
        oppSrch.stagesToOperate.add('7 - Deliver & Validate');
        oppSrch.stagesToOperate.add('0 - Closed');
        oppSrch.canRunBatch = true;
        oppSrch.OpportunityIds = opp3.Id+','+opp8.Id+',';
        oppSrch.prepareMaps();
        oppSrch.ProcessOpportunites();
        numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
        if(numBatchJobs<5 && test.isRunningTest())
            oppSrch.processBatch();
        oppSrch.statusUpdate();
        oppSrch.getItems();
        oppSrch.justDML();
    }
    static testMethod void testOppLineUpdates()
    {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();
        List<OPP_Product__c> prods = new List<OPP_Product__c>();
        List<Revenue_Line__c> revenueLines = new List<Revenue_Line__c>();
        Integer numBatchJobs ;

        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.TECH_IsOpportunityUpdate__c  = true;
        opp2.Amount = 1;
        opp2.Reason__c = 'test';
        opp2.OpportunityScope__c= 'ITSL2';
        opp2.DataMigration__c = false;
        insert opp2;
        
        Opportunity opp4 = Utils_TestMethods.createOpportunity(acc.Id);
        opp4.Amount = 10000;
        opp4.TECH_IsOpportunityUpdate__c  = true;
        opp4.Reason__c = 'test';
        opp4.OpportunityScope__c= 'ITSL2';
        opp4.DataMigration__c = false;
        opp4.StageName ='5 - Prepare & Bid';
        opp4.LeadingBUsiness__c= 'IT';
        insert opp4;

        for(Integer i=0;i<1;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            pl1.ProductBU__c = 'Test2';
            pl1.ProductLine__c = 'IDOL3';
            prodLines.add(pl1);
        }
        for(Integer i=0;i<1;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp4.Id);
            pl1.Quantity__c = 10;
            pl1.ProductBU__c = 'Test2';
            pl1.ProductLine__c = 'IDOL3';
            prodLines.add(pl1);
        }

        Database.SaveResult[] lsr = Database.Insert(prodLines);
        OPP_Product__c product = new OPP_Product__c();
        product.BusinessUnit__c = 'BD';
        product.Name = 'ITSL2';
        product.IsActive__c = true;
        product.ProductFamily__c='BBTMP';
        product.TECH_PM0CodeInGMR__c = '____';
        prods.add(product);
        
        OPP_Product__c product1 = new OPP_Product__c();
        product1.BusinessUnit__c = 'BD';
        product1.Name = 'TESt';
        product1.IsActive__c = true;
        product1.ProductFamily__c='BBTMP';
        product1.TECH_PM0CodeInGMR__c = '____';
        prods.add(product1);
        
        Database.insert(prods);
        
        
        Revenue_Line__c rl2 = new Revenue_Line__c();
        rl2.opportunity__c = opp2.id;
        revenueLines.add(rl2);
        
        Revenue_Line__c rl3 = new Revenue_Line__c();
        rl3.opportunity__c = opp2.id;
        rl3.Unit_Price__c = 0;
        revenueLines.add(rl3);
        
        Revenue_Line__c rl4 = new Revenue_Line__c();
        rl4.opportunity__c = opp4.id;
        rl4.Unit_Price__c = 0;
        revenueLines.add(rl4);
        Database.insert(revenueLines);        
        
        
        List<Opportunity> opps = new List<Opportunity>();
        opps.add(opp2);

        ApexPages.StandardSetController  controller = new ApexPages.StandardSetController(opps);
        VFC_OpportunityLinesDataMigration oppSrch = new VFC_OpportunityLinesDataMigration(controller);
        oppSrch.stagesToOperate.add('0 - Closed');
        oppSrch.stagesToOperate.add('5 - Prepare & Bid');
        oppSrch.canRunBatch = true;
        oppSrch.OpportunityIds = opp2.Id+','+opp4.Id+',';
        oppSrch.prepareMaps();
        oppSrch.ProcessOpportunites();
        numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
        if(numBatchJobs<5 && test.isRunningTest())
            oppSrch.processBatch();
        oppSrch.statusUpdate();
        oppSrch.getItems();
        oppSrch.justDML();
    }
}