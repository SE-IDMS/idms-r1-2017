@isTest(SeeAllData=true)   
private class PRJ_ProjectTriggers_TEST{   
    public static testmethod void testSecurity(){
        List<DMTStakeholder__c> ContactList = new List<DMTStakeholder__c>();
        List<PRJ_Stakeholder__c> actorList = new List<PRJ_Stakeholder__c>();
        
        //User otherUser = [select Id,SEContactID__c from User where Id <> :UserInfo.getUserId() and IsActive = TRUE limit 1];
        // Ram Added Starts
        User otherUser = new User();
        otherUser.FirstName = 'Test';
        otherUser.LastName = 'User';
        otherUser.Email = 'test.email@schneider-electric.com';
        otherUser.Alias = 'tstuser';
        otherUser.Username = 'tstuser@bridge-fo.com';
        otherUser.CommunityNickname = 'tstuserforDMT';
        otherUser.TimeZoneSidKey = 'Asia/Kolkata';
        otherUser.LocaleSidKey = 'en_US';
        otherUser.EmailEncodingKey = 'ISO-8859-1';
        otherUser.ProfileId = '00eA0000000uVHP';
        otherUser.LanguageLocaleKey = 'en_US';
        
        insert otherUser;
        // Ram Added Ends
         //[select Id,SEContactIDUser__c from User where Id <> :UserInfo.getUserId() and IsActive = TRUE limit 1];
        //otherUser.SEContactId__c = 'TEST123456789';
        update otherUser;
        
        //User currentUser = [select id,SEContactID__c from User where id = :UserInfo.getUserId() limit 1];

        // User currentUser = [select id,SEContactIDUser__c from User where id = :UserInfo.getUserId() limit 1];
        
        User currentUser = new User();
        currentUser.FirstName = 'Test';
        currentUser.LastName = 'User';
        currentUser.Email = 'test.email2@schneider-electric.com';
        currentUser.Alias = 'tstuser2';
        currentUser.Username = 'tstuser2@bridge-fo.com';
        currentUser.CommunityNickname = 'tstuserforDMT2';
        currentUser.TimeZoneSidKey = 'Asia/Kolkata';
        currentUser.LocaleSidKey = 'en_US';
        currentUser.EmailEncodingKey = 'ISO-8859-1';
        currentUser.ProfileId = '00eA0000000uVHP';
        currentUser.LanguageLocaleKey = 'en_US';
        
        insert currentUser;
        
        
        Account testAccount = new Account(name='Schneider',Street__c='Test');
        insert testAccount;
        
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
        insert testDMTA1; 
        
        DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
        insert testDMTA2; 
            
        
        PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
        testProj.Name='Test Project';
        testProj.OwnerId = otherUser.Id;
        testProj.Parent__c = 'Buildings';
        testProj.BusinessTechnicalDomain__c = 'Supply Chain';
        testProj.BusGeographicZoneIPO__c = 'Global';
        insert testProj;
        
        Budget__c testBudget = new Budget__c();
        testBudget.Active__c=true;
        testBudget.ProjectReq__c = testProj.id;
        insert testBudget;
        
        Schema.DescribeFieldResult F = PRJ_Stakeholder__c.Role__c.getDescribe();
          

         // Contact testContact=    Utils_testMethods.createContact('Test', 'TEST123456789', testAccount.Id);
          REF_OrganizationRecord__c orgRec = Utils_testMethods.createOrganizationRecord('Test','Business Organization');
            insert orgRec;
          DMTStakeholder__c testContact= new DMTStakeholder__c(LastName__c='Test',Email__c='test@hotmail.com',SEContactID__c='TEST123456789',L1__c=orgRec.id,ProjectRoles__c = 'Requester');
        
        
        insert testContact;
        
        Integer countContact = 0;
        for(Schema.PicklistEntry pickValue:f.getPicklistValues()){  
          PRJ_STakeholder__c actor = new PRJ_Stakeholder__c();
          actor.ProjectReq__c = testProj.id;
          actor.ActorNew__c = testContact.Id;
          actor.Role__c = pickValue.getValue();
          actorList.add(actor);
        }
        insert actorList;
        
        test.startTest();
        
        testProj.NextStep__c = 'Created';
        try{
            update testProj;
        }catch (Exception e){
            
        }
        System.runAs(otherUser){
            update testProj;
        }
        
        //currentUser.SEContactID__c = 'TEST123456789';
        update currentUser;
        
        //otherUser.SEContactID__c = '';
        update otherUser;
        
        testProj.NextStep__c = 'Open';
        update testProj;
        
        testProj.ProgrammedGoLiveDate__c = Date.today();
        testProj.ExpectedStartDate__c = Date.today();
        testProj.GoLiveTargetDate__c = Date.today() + 1;
        testProj.AuthorizeFunding__c = 'Yes';
        //DMTFundingEntity__c testFE = Utils_TestMethods_DMT.createDMTFundingEntity(testProj,'2012',true,true,'Test');
        //insert testFE;
        
        // Ramakrishna Start 
        DMTFundingEntity__c testFE = new DMTFundingEntity__c();
        testFE.ParentFamily__c = 'Business';
        testFE.ProjectReq__c =testProj.id;
        insert testFE;
        
        //PRJ_ProjectUtils proReq  = new PRJ_ProjectUtils();
        
        list<PRJ_ProjectReq__c> testProj2List = new list<PRJ_ProjectReq__c>();
        System.debug('********************* Rama Krishna Testing Start**************');
        for(Integer i=0;i<=10;i++){
            PRJ_ProjectReq__c testProj2 = new PRJ_ProjectReq__c(OwnerId = otherUser.Id);
             testProj2.Name='Test Project Ramu '+i;
             testProj2.NextStep__c ='Draft';
             testProj2.WorkType__c = 'Master Project';
             testProj.Parent__c = 'Buildings';
            testProj2.BusinessTechnicalDomain__c = 'Supply Chain';
            testProj2.BusGeographicZoneIPO__c = 'Global';
             testProj2List.add(testProj2);
             
        }
        insert testProj2List;
        
        list<PRJ_ProjectReq__c> prList = new list<PRJ_ProjectReq__c>();
        
        List<Budget__c> listtestBudget = new List<Budget__c>();
        
        
        for(PRJ_ProjectReq__c Pr : testProj2List){
            Budget__c testBudget2 = new Budget__c();
            Pr.Name='Test Project Ramu Next step : created ';
            testBudget2.Active__c=true;
            testBudget2.ProjectReq__c = Pr.id;
            listtestBudget.add(testBudget2);
            pr.NextStep__c ='Created';
            pr.BusinessTechnicalDomain__c = 'Test';
            prList.add(Pr);
             
        }
        insert listtestBudget;
        update prList;
        
        PRJ_ProjectUtils.sendCreatedNotification(testProj2List,'test');
         //PRJ_ProjectUtils.verifyHFMCodeEnteredandSelectedInEnvelop(testProj2List);
         //PRJ_ProjectUtils.sendStepChangeNotification(testProj2List);
         //Modified By Ramakrishna End
        testProj.NextStep__c = 'Project Open';
        update testProj;
        
        test.stopTest();
      }
        
        
      public static testmethod void testEmailNotification(){
        
        List<DMTStakeholder__c> ContactList = new List<DMTStakeholder__c>();
        List<PRJ_Stakeholder__c> actorList = new List<PRJ_Stakeholder__c>();
        
        
        Account testAccount = new Account(name='Schneider',Street__c = 'Test');
        insert testAccount;
        
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
        insert testDMTA1; 
        
        PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.Description__c = 'Test';
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            testProj.BusinessTechnicalDomain__c = 'Supply Chain';
            insert testProj;
        

        
        Schema.DescribeFieldResult F = PRJ_Stakeholder__c.Role__c.getDescribe();
          
        for(Schema.PicklistEntry pickValue:f.getPicklistValues()){
          DMTStakeholder__c testContact= new DMTStakeholder__c(LastName__c='Test',Email__c='test@hotmail.com',ProjectRoles__c = 'Requester');
          //Contact testContact=    Utils_testMethods.createContact('Test', 'TEST123456789', testAccount.Id);
          testContact.Email__c = 'test@hotmail.com';
          contactList.add(testContact);
        }
        
        insert contactList;
        
        Integer countContact = 0;
        for(Schema.PicklistEntry pickValue:f.getPicklistValues()){  
          PRJ_STakeholder__c actor = new PRJ_Stakeholder__c();
          actor.ProjectReq__c = testProj.id;
          actor.ActorNew__c = contactList[countContact].Id;
          actor.Role__c = pickValue.getValue();
          actorList.add(actor);
        }
        insert actorList;
        
        
        If (PRJ_Config__c.getInstance()==null || PRJ_Config__c.getInstance().NotificationTemplate__c==null){
          PRJ_Config__c testConfig = new PRJ_Config__c();
          testConfig.NotificationTemplate__c = [select developerName  from EmailTemplate limit 1].developerName;
          upsert testConfig;
        }
        
        
        Integer counter=0;
        
        test.startTest();
        
        /*
        for (RecordType recType:recordTypeMap.values()){
          counter++;
          testProj.RecordTypeId = recType.Id;
          update testProj;
          
          if (counter==10) break;
        }
        */
        testProj.PreviousStep__c = 'Business IT Project - Validate Proposals';
        update testProj;
        test.stopTest();
        
        
        
      }
      public static testmethod void testMethods1(){
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
        insert testDMTA1; 
        
        DMTAuthorizationMasterData__c testDMTA7 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Employee Experience','Global');
        insert testDMTA7; 
        
        DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
        insert testDMTA2; 
        
        DMTAuthorizationMasterData__c testDMTA6 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Customer Experience');
        insert testDMTA6; 
            
            PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.Description__c = 'Test';
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            testProj.BusinessTechnicalDomain__c = 'Supply Chain';
            insert testProj;
            
        DMTFundingEntity__c fund = new DMTFundingEntity__c();
        fund.ProjectReq__c = testProj.Id;
        fund.ActiveForecast__c = true;
        fund.Parent__c = 'Buildings';
        fund.SelectedinEnvelop__c = false;
        insert fund;
        
        DMTFundingEntity__c fund1 = new DMTFundingEntity__c();
        fund1.ProjectReq__c = testProj.Id;
        fund1.ActiveForecast__c = true;
        fund1.Parent__c = 'Employee Experience';
        fund1.SelectedinEnvelop__c = false;
        insert fund1;
        
            
        Budget__c testBud = Utils_TestMethods.createBudget(testProj,true);
        insert testBud; 
            
        test.startTest();
        
        testProj.NextStep__c='Created';
        update testProj;
        
        testProj.BusinessTechnicalDomain__c = 'Customer Experience';
        update testProj;
        
        DMTAuthorizationMasterData__c testD1 = new DMTAuthorizationMasterData__c();
        testD1.AuthorizedUser5__c = UserInfo.getUserId();
        testD1.AuthorizedUser6__c = UserInfo.getUserId();
        testD1.NextStep__c = 'Quoted';
        testD1.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
        insert testD1;
        
        testProj.NextStep__c = 'Valid';
        testProj.AuthorizeFunding__c = 'Escalate';
        testProj.subStatus__c = 'Working';
        testProj.PRJ_ValidGateNotification__c  = true;
        testProj.PRJSentValidGateNotification__c = false;
        testProj.PLImpact2017__c = 65000;
        update testProj;
        
        
        DMTAuthorizationMasterData__c testDMTA4 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),System.Label.DMT_StatusQuoted,testProj.BusinessTechnicalDomain__c,testProj.BusGeographicZoneIPO__c);
        insert testDMTA4;
        
        
        testProj.subStatus__c = 'Complete';
        testProj.PLImpact2017__c = 40000;
        update testProj;
        
        testProj.PLImpact2017__c = 200000;
        update testProj;
        
        testProj.PLImpact2017__c = 400000;
        update testProj;
        
        testProj.NextStep__c = 'Quoted';
        update testProj;
        
        fund.ActiveForecast__c = true;
        fund.SelectedinEnvelop__c = true;
        fund.HFMCode__c = '2345';
        Update fund;
        
        fund1.ActiveForecast__c = true;
        fund1.SelectedinEnvelop__c = true;
        fund1.HFMCode__c = '2345';
        Update fund1;
        
        
        DMTAuthorizationMasterData__c testDMTA5 = new DMTAuthorizationMasterData__c();
        testDMTA5.AuthorizedUser1__c = UserInfo.getUserId();
        testDMTA5.AuthorizedUser5__c = UserInfo.getUserId();
        testDMTA5.AuthorizedUser6__c = UserInfo.getUserId();
        testDMTA5.NextStep__c = 'Fundings Authorized';
        testDMTA5.Parent__c = testProj.Parent__c;
        testDMTA5.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
        testDMTA5.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
        insert testDMTA5; 
        
        
        testProj.PLImpact2017__c = 200000;
        update testProj;
        
        
        testProj.BusGeographicZoneIPO__c = 'APAC';
        update testProj;
        
        test.StopTest();
        
        testProj.PLImpact2017__c = 500000;
        update testProj;
        
        testProj.PLImpact2017__c = 1500000;
        testProj.BusGeographicZoneIPO__c = 'APAC';
        update testProj;
        
        testProj.BusGeographicZoneIPO__c = 'Global';
        testProj.PLImpact2017__c = 200000;
        testProj.PRJFAGateNotification__c  = true;
        testProj.PRJSentFAGateNotification__c = false;
        update testProj;
        
        testProj.PLImpact2017__c = 500000;
        testProj.PRJFAGateNotification__c  = true;
        testProj.PRJSentFAGateNotification__c = false;
        update testProj;
        
        testProj.PLImpact2017__c = 15000000;
        testProj.NextStep__c='Fundings Authorized';
        testProj.Substatus__c = 'Partial';
        testProj.TECH_GatePartialUser__c = testDMTA5.AuthorizedUser5__c;
        testProj.AuthorizeFunding__c = 'Yes';
        testProj.PRJFAGateNotification__c  = true;
        testProj.PRJSentFAGateNotification__c = false;
        system.debug('>>>>>' +testProj.NextStep__c); 
        update testProj;
        
        
      
      }
}