public class VFC_LiveAgent_OpenContactSearchInConsole {
    public Boolean liveAgentUser {get; private set;}
    
    public VFC_LiveAgent_OpenContactSearchInConsole(){
        liveAgentUser = [select id, name, userpermissionsliveagentuser from user where id = : UserInfo.getUserId()].userpermissionsliveagentuser;
    }
}