public with sharing class VFC07_SFEBubble extends VFC_ControllerBase {
        
        private final Integer PLATFORMACCOUNT_LIMIT = 9999;    //governor limit of 9999 records retrieved in SOQL
        public SFE_IndivCAP__c sfeIndivCAP = null;
        private String flashVars;
        public Boolean bTestFlag = false;                                               //used in Test method on second invokation of the createEvents() method - ensures complete code coverage
        private String dmlException;
        private Boolean isUpdatable = false;
        /**Modified as part of september release
        **/
        public String platformingId ;
        public List<DataTemplate__c> fetchedRecords{get;set;}
        public List<SelectOption> columns{get;set;}

         public VFC07_SFEBubble(ApexPages.StandardController stdController) {    

            System.debug('<<<<<<********** Constructor*************>>>>>>');                   
             platformingId = stdController.getID();
                if (
                Schema.sObjectType.SFE_IndivCAP__c.fields.AttractiAxisThreshold1__c.isUpdateable() &&
                Schema.sObjectType.SFE_IndivCAP__c.fields.AttractiAxisThreshold2__c.isUpdateable() &&
                Schema.sObjectType.SFE_IndivCAP__c.fields.MktShareThreshold1__c.isUpdateable() &&
                Schema.sObjectType.SFE_IndivCAP__c.fields.MktShareThreshold2__c.isUpdateable()) {
                        isUpdatable = true;
                }
                System.debug('##### : Schema.sObjectType.SFE_IndivCAP__c.fields.AttractiAxisThreshold1__c.isUpdateable() : ' + Schema.sObjectType.SFE_IndivCAP__c.fields.AttractiAxisThreshold1__c.isUpdateable());
                System.debug('##### Schema.sObjectType.SFE_IndivCAP__c.fields.AttractiAxisThreshold2__c.isUpdateable() : ' + Schema.sObjectType.SFE_IndivCAP__c.fields.AttractiAxisThreshold2__c.isUpdateable());
                System.debug('##### Schema.sObjectType.SFE_IndivCAP__c.fields.MktShareThreshold1__c.isUpdateable() : ' + Schema.sObjectType.SFE_IndivCAP__c.fields.MktShareThreshold1__c.isUpdateable());
                System.debug('##### Schema.sObjectType.SFE_IndivCAP__c.fields.MktShareThreshold2__c.isUpdateable() : ' + Schema.sObjectType.SFE_IndivCAP__c.fields.MktShareThreshold2__c.isUpdateable());
                
                sfeIndivCAP = [select id, AttractiAxisThreshold1__c,AttractiAxisThreshold2__c,MktShareThreshold1__c,MktShareThreshold2__c from SFE_IndivCAP__c 
                where id =:platformingId];                
         }



         public String getBubbles() {
                String stringX = '';
                String stringY = '';
                Integer x = 0;
                Integer y = 0;
                Set<String> points=new Set<String>();
                String output='';
                boolean error=false;
                try{ 
                        if(this.sfeIndivCAP.Id != null){
                                for(SFE_PlatformingScoring__c ps : [select Id,Score__c,MktShare__c from SFE_PlatformingScoring__c where IndivCAP__c = :this.sfeIndivCAP.Id limit :PLATFORMACCOUNT_LIMIT]){
                                      
                                /*        if(ps.Score__c != null){
                                                x = ps.Score__c.intValue();
                                                System.debug('### ps.Score__c ==> ' + x);
                                        }
                                        if(ps.MktShare__c != null){
                                                y = ps.MktShare__c.intValue();
                                                System.debug('### (ps.MktShare__c ==> ' + y);
                                        }
                                        */
                                        if((ps.Score__c != null)&&(ps.MktShare__c != null)){
                                            if(!points.contains(ps.Score__c.intValue().format()+','+ps.MktShare__c.intValue().format()))
                                            {
                                                points.add(ps.Score__c.intValue().format()+','+ps.MktShare__c.intValue().format());
                                                stringX+= ps.Score__c.intValue().format()+',';
                                                stringY+= ps.MktShare__c.intValue().format()+',';
                                            }                                                                                            
                                        }
                                        else
                                        error=true;
                                }
                                System.debug('points'+points);
                                
                                        
                                                if (points.size()!=0) {
                                                stringX = stringX.substring(0, stringX.length() - 1);
                                                stringY = stringY.substring(0, stringY.length() - 1);   
                                                }
                                                output = 'xBubbles=' + stringX + '&yBubbles=' + stringY + '&rBubble=1';                     
                                        
                                                if(error) {
                                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CL00088));
                                                }
                                        //        return null;
                                        
                        }
                        if(Test.isRunningTest() && bTestFlag)
                        throw new TestException ();             //used during tests only to achieve coverage
        }catch (Exception e){
            ApexPages.addMessages(e);
        }
        return output;
         }
         
         public String getFlashVars(){
                String varsOutput = getBubbles();
                flashVars = '';
                flashVars+='xThreshT1=' + this.sfeIndivCAP.AttractiAxisThreshold1__c;
                flashVars+='&xThreshT2=' + this.sfeIndivCAP.AttractiAxisThreshold2__c;
                flashVars+='&yThreshT1=' + this.sfeIndivCAP.MktShareThreshold1__c;
                flashVars+='&yThreshT2=' + this.sfeIndivCAP.MktShareThreshold2__c;
                if (varsOutput != '') {
                        flashVars+='&'+ varsOutput;
                }
                if (dmlException != null) {
                        flashVars+='&msg=' + dmlException;
                }
                if (isUpdatable == false) {
                        flashVars+='&isUpdatable=false';
                }
                System.debug('### Sending FlashVars to Bubble Chart ==> ' + this.flashVars);
                return flashVars;
         }
         
         
        public String errMsg {
                get{
                        return dmlException;
                }
                set;
        }
        
        public PageReference refreshSFEBubbleCtrl(){
                try {
                        System.debug('######### avant');
                        //update sfeIndivCAP;
                        Database.SaveResult sr= Database.update(sfeIndivCAP, false);
                        if(!sr.isSuccess() || (Test.isRunningTest() && bTestFlag)){
                           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, sr.getErrors()[0].getMessage()));
                           dmlException = sr.getErrors()[0].getMessage();
                           System.debug('######### dedans : ' +dmlException );
                           
                    }
                        System.debug('######### apres');
                }catch (Exception ex){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(errMsg);
            return null;
        }
                return null;
        }
        
        //Properties Section
        public Boolean bNoErrors {get{return !ApexPages.hasMessages();}}
        
        public class TestException extends Exception {}
        
        public SFE_IndivCAP__c getSfeIndivCAP (){
                return this.sfeIndivCAP;
        }
        
        public void setSfeIndivCAP (SFE_IndivCAP__c sfeIndivCAP){
                try{
                        this.sfeIndivCAP = sfeIndivCAP;
                } catch (Exception ex){
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(errMsg);
        }        
        }
        
        /* Abhishek Modified for september release*/
     public void display()
     {      
        System.debug('<<<<<<********** Display *************>>>>>>');        
        
           
         fetchedRecords = new List<DataTemplate__c>();
            {                
                DatatemplateWrap dWrap= new DatatemplateWrap();                 
                for(integer i=0;i<11;i++)
                {
                    DataTemplate__c dt = new DataTemplate__c();                            
                    dt.field1__c=dWrap.customerProfile()[i];   
                    dt.field2__c=String.valueOf(dWrap.customerPlatformed[i]); 
                    dt.field3__c=String.valueOf(Math.roundToLong(dWrap.sumOfPam[i])+' '+dWrap.isoCode());
                    dt.field4__c=dWrap.visitFreq[i];                                                                    
                    fetchedRecords.add(dt);
                }
                 system.debug('*******'+fetchedRecords);  
            }
            columns = new List<SelectOption>();
            Columns.add(new SelectOption('Field1__c',sObjectType.SFE_PlatformingScoring__c.Fields.CustomerProfile__c.label)); 
            Columns.add(new SelectOption('Field2__c',Label.CL00612)); 
            Columns.add(new SelectOption('Field3__c',label.CL00613));
            Columns.add(new SelectOption('Field4__c',Label.CL00614));
     }
          
     Static list<String> customerProfiles = new list<string>{label.CL00066,label.CL00067,label.CL00068,label.CL00063,label.CL00064,label.CL00065,label.CL00060,label.CL00061,label.CL00062,Label.CL00435,Label.CL00436};      
     public class DatatemplateWrap
     {         
         list<SFE_PlatformingScoring__c> scoringcpnotnull=[select CustomerProfile__c,PAMPlaforming__c,convertCurrency(DirectSalesPlatforming__c),convertCurrency(IndirectSalesPlatforming__c),MktShare__c from SFE_PlatformingScoring__c where IndivCAP__c =:ApexPages.currentPage().getParameters().get('id') and CustomerProfile__c!=null and CustomerProfile__c!=' '];             
         Integer nullrecords=[select count() from SFE_PlatformingScoring__c where IndivCAP__c =:ApexPages.currentPage().getParameters().get('id') and (CustomerProfile__c=null or CustomerProfile__c=' ') ];             
         list<SFE_PlatformingScoring__c> scoringcpnullandplatnotnull=[select CustomerProfile__c,PAMPlaforming__c,convertCurrency(DirectSalesPlatforming__c),convertCurrency(IndirectSalesPlatforming__c),MktShare__c from SFE_PlatformingScoring__c where IndivCAP__c =:ApexPages.currentPage().getParameters().get('id') and (CustomerProfile__c=null or CustomerProfile__c=' ') and DirectSalesPlatforming__c!=null and IndirectSalesPlatforming__c!=null];
         
         public Integer[] customerPlatformed = new Integer[customerProfiles.size()];         
         public Double[] sumOfPam= new Double[customerProfiles.size()];      
        SFE_IndivCAP__c cap=[select s1VisitFreq__c,s2VisitFreq__c,s3VisitFreq__c,Q1VisitFreq__c,Q2VisitFreq__c,Q3VisitFreq__c,g1VisitFreq__c,g2VisitFreq__c,
                                     g3VisitFreq__c from SFE_IndivCAP__c where Id =:ApexPages.currentPage().getParameters().get('id')];                                  
        public List<string> visitFreq;          
       
        
        public DatatemplateWrap()
        {
            //System.debug('populatecustomerPlatformed'+Limits.getScriptStatements());
            System.debug('populatecustomerPlatformed'+Limits.getCpuTime());
            populatecustomerPlatformed();
            System.debug('populatesumOfPam'+Limits.getCpuTime());
            populatesumOfPam();
            System.debug('populatevisitFreq'+Limits.getCpuTime());
            populatevisitFreq();
        }
         public list<String> customerProfile()         
         {      
             return customerProfiles;
         }
        
         public void populatevisitFreq()
         {                  
          ApexPages.StandardController sc1 = new ApexPages.StandardController(cap);
          VFC05_SFECalculator sfe = new VFC05_SFECalculator(sc1);
                 String Total = string.valueof(cap.s1VisitFreq__c*customerPlatformed[0]+cap.s2VisitFreq__c*customerPlatformed[1]+cap.s3VisitFreq__c*customerPlatformed[2]+cap.Q1VisitFreq__c*customerPlatformed[3]+cap.q2VisitFreq__c*customerPlatformed[4]+cap.q3VisitFreq__c*customerPlatformed[5]+cap.g1VisitFreq__c*customerPlatformed[6]+cap.g2VisitFreq__c*customerPlatformed[7]+cap.g3VisitFreq__c*customerPlatformed[8]);             
                 visitFreq= new list<string>{string.valueof(cap.s1VisitFreq__c*customerPlatformed[0]),String.valueOf(cap.s2VisitFreq__c*customerPlatformed[1]),String.valueOf(cap.s3VisitFreq__c*customerPlatformed[2]),String.valueOf(cap.Q1VisitFreq__c*customerPlatformed[3]),String.valueOf(cap.q2VisitFreq__c*customerPlatformed[4]),String.valueOf(cap.q3VisitFreq__c*customerPlatformed[5]),String.valueOf(cap.g1VisitFreq__c*customerPlatformed[6]),String.valueOf(cap.g2VisitFreq__c*customerPlatformed[7]),String.valueOf(cap.g3VisitFreq__c*customerPlatformed[8]),'0',Total};                   
         }         
             
         public void populatecustomerPlatformed()
         {          
         //for(integer i=0;i<11;i++)
         //{
            //customerPlatformed[i]=0;
             customerPlatformed=new List<Integer>{0,0,0,0,0,0,0,0,0,0,0};
         //}      
            try{
                
                  for(SFE_PlatformingScoring__c score:scoringcpnotnull)
                     {
                        for(integer count= 0; count<customerProfiles.size(); count++)
                        {
                            if(score.CustomerProfile__c!=null && score.CustomerProfile__c.equalsIgnoreCase(customerProfiles[count]))
                            {
                                customerPlatformed[count] = customerPlatformed[count] + 1;
                            }            
                        }
                    }               
                    //customerPlatformed[9] = nullrecords;
                                      
                    customerPlatformed[10]=customerPlatformed[0]+customerPlatformed[1]+customerPlatformed[2]+customerPlatformed[3]
                    +customerPlatformed[4]+customerPlatformed[5]+customerPlatformed[6]+customerPlatformed[7]+customerPlatformed[8]+customerPlatformed[9];  
                
            }   
            catch(Exception e){
                System.debug('********* exception '+e);
            }      
            
                  
         }                       
           
         public void populatesumOfPam()
         {
          //   for(integer i=0;i<11;i++)
            // {
              //   sumOfPam[i]=0;
                 sumOfPam=new List<Double>{0,0,0,0,0,0,0,0,0,0,0};
           //  } 
           Integer counta=0;                                  
                 for(SFE_PlatformingScoring__c score:scoringcpnotnull)
                 {                    
                 System.debug(counta++); 
                    for(integer count= 0; count<customerProfiles.size(); count++)
                    {
                        System.debug('----------------------------------------------------------------------');
                        if(score.CustomerProfile__c!=null && score.CustomerProfile__c.equalsIgnoreCase(customerProfiles[count]) && score.DirectSalesPlatforming__c!=null && score.IndirectSalesPlatforming__c!=null)
                        {

                            sumOfPam[count] = sumOfPam[count]+score.PAMPlaforming__c/1000;
                            //(score.DirectSalesPlatforming__c/1000+score.IndirectSalesPlatforming__c/1000)*score.MktShare__c/100;
                            System.debug('######## '+count);
                             System.debug('****  '+sumOfPam[count]);
                             System.debug('**** DirectSalesPlatforming :  '+score.DirectSalesPlatforming__c +'  IndirectSalesPlatforming: '+score.IndirectSalesPlatforming__c +' IndirectSalesPlatforming : '+score.IndirectSalesPlatforming__c +' MktShare :'+ score.MktShare__c);

                        }                        
                    }
                }
                for(SFE_PlatformingScoring__c score:scoringcpnullandplatnotnull)
                {
                System.debug(counta++);                                    
                            sumOfPam[9]=sumOfPam[9]+(score.DirectSalesPlatforming__c+score.IndirectSalesPlatforming__c)*score.MktShare__c/100;
                }                                                                             
                if(scoringcpnullandplatnotnull.size()>0 || scoringcpnotnull.size()>0)
                {
                sumOfPam[10]=sumOfPam[0]+sumOfPam[1]+sumOfPam[2]+sumOfPam[3]
                            +sumOfPam[4]+sumOfPam[5]+sumOfPam[6]+sumOfPam[7]+sumOfPam[8]+sumOfPam[9];
                }             
         }  
         String iso=[select DefaultCurrencyIsoCode from user where id=:userInfo.getUserId()].DefaultCurrencyIsoCode;
         public string isoCode()
         {             
             return iso;
         }
     }
}