/*
    Author          : Bhuvana Subramaniyan (ACN) 
    Date Created    : 12/01/2010 
    Description     : Test Class for VFC13_RITEForm.
*/
@isTest
private class VFC13_RITEForm_TEST
{
    static Account acc;
    static Opportunity opp;
    static List<OPP_QuoteLink__c>  quoteLinks = new List<OPP_QuoteLink__c>();
    static OPP_QuoteLink__c quoteLink,quoteLink1,quoteLink2;
    static RIT_ProjectInformation__c projInfo1;
    static RIT_ProjectInformation__c projInfo;
    static RIT_FinancialEnv__c finEnv,finEnv1;
    static Note note,note1,note2,note3,note4,note5,note6,note7;
    static List<Note> notes = new List<Note>();
    static RIT_ContractualEnv__c contEnv,contEnv1;
    static RIT_Budget__c budget;
    static RIT_ProjectManagement__c projMgnt;
    static RIT_TechManScope__c techManf,techManf1;
    static RIT_LiquidatedDamages__c liqDamages;
    static RIT_CommCompEnv__c commComp;
    static RIT_SubContractOGSupp__c subContract,subContract1;
    static RIT_TermsAndMeansOfPayment__c termsAndMeans;
    static RIT_Risks__c risk,risk1,risk2,risk3,risk4,risk5,risk6;
    static List<RIT_Risks__c> risks = new List<RIT_Risks__c>();
    static ApexPages.StandardController con;
    static VFC13_RITEForm riteForm;
    static 
    {
        //inserts Account
        acc = Utils_TestMethods.createAccount();
        insert acc;
        //inserts Opportunity
        opp = RIT_Utils_TestMethods.createOpportunity(acc.Id);
        insert opp;
        //inserts QuoteLink
        quoteLink = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
        quoteLink.TypeOfOffer__c = Label.CLAPR14SLS14;
        quoteLinks.add(quoteLink);
 
        //inserts QuoteLink
        quoteLink1 = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
        quoteLinks.add(quoteLink1);
        //inserts QuoteLink
        quoteLink2 = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
        quoteLinks.add(quoteLink2); 
        database.insert(quoteLinks);
        
        //inserts Project Information
        projInfo1= RIT_Utils_TestMethods.createProjInfowithConsort(quoteLinks[2].Id);
        insert projInfo1;
        //inserts Project Information
        projInfo= RIT_Utils_TestMethods.createProjInfo(quoteLinks[0].Id);
        projInfo.DurationOfTheProjectInMonths__c = 13;
        insert projInfo;
        
        //Inserts Tech Manf 
        techManf= RIT_Utils_TestMethods.createTechManf(quoteLinks[0].Id);
        techManf.AnyEquipmentSpecificallyDesigned__c= 'Yes';
        techManf.DoWeTakeAnyPerformanceCommitment__c = 'Yes';
        insert techManf;
        
        //Inserts Tech Manf 
        techManf1= RIT_Utils_TestMethods.createTechManf(quoteLinks[1].Id);
        insert techManf1;
        
        //Inserts Sub contracters
        subContract  = RIT_Utils_TestMethods.createSubContr(quoteLinks[0].Id);
        subContract.CriticalOGSuppliers__c = 'No';
        subContract.IsBackToBackConditionsNegotiatedNew__c = 'No';
        insert subContract;
        
        //Inserts Sub contracters
        subContract1  = RIT_Utils_TestMethods.createSubContr(quoteLinks[1].Id);        
        insert subContract1;
        
        //Inserts Sub contracters
        subContract  = RIT_Utils_TestMethods.createSubContr(quoteLinks[0].Id);
        subContract.CriticalOGSuppliers__c = 'No';
        insert subContract;
        
        //Inserts Project Managent
        projMgnt= RIT_Utils_TestMethods.createProjMgnt(quoteLinks[0].Id);
        insert projMgnt;
        
        //Inserts Comm & Comp
        commComp= RIT_Utils_TestMethods.createCommComp(quoteLinks[0].Id);
        insert commComp;
        
        //Inserts Contractual Env
        contEnv = new RIT_ContractualEnv__c(QuoteLink__c = quoteLinks[0].Id);
        contEnv.CDListedWithTheOrderOfPrecedence__c= 'No';
        contEnv.NewLegalEntity__c= 'No';
        contEnv.InCaseSEIsActingAsSubContractor__c = 'Yes';
        contEnv.ComingIntoForceBeginDeliveryTime__c = 'No';
        contEnv.ClientLateForPayment__c = 'No';
        contEnv.WhatIsTheApplicableLawInContractNew__c = 'No';
        contEnv.ChangesInLegislation__c = 'Yes';
        contEnv.NewLegalEntity__c = 'Yes';
        contEnv.IsSELiabilityLimitedInTheContract__c = 'No';
        contEnv.ConsequentIndirectDamagesExcluded__c = 'No';
        contEnv.LDForDelay__c = 'No';
        contEnv.DoesProjectInvolveAnyIPRights__c = 'No';
        contEnv.CommunicationChartChange__c = 'No';        
        insert contEnv;
        
        contEnv1 = new RIT_ContractualEnv__c(QuoteLink__c = quoteLinks[1].Id);
        insert contEnv1;
        //Inserts Liquidated Damages
        liqDamages = RIT_Utils_TestMethods.createLiqDam(contEnv.Id);
        insert liqDamages;
        
        //inserts Financial Environment record for quoteLink 
        finEnv = RIT_Utils_TestMethods.createFinEnv(quoteLinks[0].Id);
        insert finEnv;
        
        //inserts Financial Environment record for quoteLink 
        finEnv1 = RIT_Utils_TestMethods.createFinEnv(quoteLinks[1].Id);
        finEnv1.PaymentTermsSecured__c = Label.CLAPR14SLS72;
        finEnv1.ProjectInnovativeFinance__c = 'Yes';
        finEnv1.ClientOverseaEntity__c = 'Yes';
        finEnv1.ExportCountryRestriction__c = 'Yes';
        finEnv1.DownpaymentCashFlow__c = 'No';
        finEnv1.ProjGrossAboveMinMargin__c = 'No';
        finEnv1.BankGuarnteeReqClinet__c = 'Yes';
        finEnv1.ProtectedRawMaterial__c = 'No';
        finEnv1.IsThereAnExchangeRisk__c = 'Yes';
        finEnv1.ParentCompanyGuaranteeRequired__c = 'Yes';
        insert finEnv1;

        //inserts Terms and Means of Payment for Financial Environment
        termsAndMeans = RIT_Utils_TestMethods.createTermsAndMeans(finEnv.Id);
        insert termsAndMeans;
        //insert Note
        note= RIT_Utils_TestMethods.createNotes(finEnv.Id);
        notes.add(note);
        //insert Note
        note1= RIT_Utils_TestMethods.createNotes(projInfo.Id);
        notes.add(note1);
        note2= RIT_Utils_TestMethods.createNotes(contEnv.Id);
        notes.add(note2);
        note3= RIT_Utils_TestMethods.createNotes(subContract.Id);
        notes.add(note3);
        note4= RIT_Utils_TestMethods.createNotes(techManf.Id);
        notes.add(note4);
        note5= RIT_Utils_TestMethods.createNotes(projMgnt.Id);
        notes.add(note5);
        note6= RIT_Utils_TestMethods.createNotes(commComp.Id);
        notes.add(note6);
        Database.insert(notes);
        //inserts Risk for Financial Env
        risk3 =  RIT_Utils_TestMethods.createFinEnvRisks(quoteLinks[0].id,finEnv.Id);
        risks.add(risk3);
        risk4 =  RIT_Utils_TestMethods.createProjMgntRisks(quoteLinks[0].id,projMgnt.Id);
        risks.add(risk4);
        risk5 =  RIT_Utils_TestMethods.createCommCompRisks(quoteLinks[0].id,commComp.Id);
        risks.add(risk5);
        database.insert(risks);

    }
    //Test method
    static testMethod void testRITEForm() 
    {       
        Test.StartTest();
        
        con = new ApexPages.StandardController(quoteLink);
        PageReference pageRef = Page.VFP13_RITEForm;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', quoteLinks[0].id);
        //Test method Execution Begins
                
        riteForm = new VFC13_RITEForm(con);
        riteForm  = new VFC13_RITEForm();
        
        ApexPages.currentPage().getParameters().put('parametername', 'View'+finEnv.Id);
        ApexPages.currentPage().getParameters().put('paramterId',finEnv.Id );
        ApexPages.currentPage().getParameters().put('parameter', 'RITEFormRules');
        riteForm.pageRedirect();
        
        delete termsAndMeans;
        
        ApexPages.currentPage().getParameters().put('parametername', 'View'+finEnv.Id);
        ApexPages.currentPage().getParameters().put('paramterId',finEnv.Id );
        ApexPages.currentPage().getParameters().put('id', quoteLinks[0].id);
        riteForm.pageRedirect();
        ApexPages.currentPage().getParameters().put('parametername', 'View'+finEnv1.Id);
        ApexPages.currentPage().getParameters().put('paramterId',finEnv1.Id );
        ApexPages.currentPage().getParameters().put('id', quoteLinks[0].id);
        riteForm.pageRedirect();
        ApexPages.currentPage().getParameters().put('parametername', 'View'+projInfo.Id);
        ApexPages.currentPage().getParameters().put('paramterId',projInfo.Id );
        ApexPages.currentPage().getParameters().put('parameter', 'RITEFormRules');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'View'+techManf.Id);
        ApexPages.currentPage().getParameters().put('paramterId',techManf.Id );
        ApexPages.currentPage().getParameters().put('parameter', 'RITEFormRules');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'View'+techManf1.Id);
        ApexPages.currentPage().getParameters().put('paramterId',techManf1.Id );
        ApexPages.currentPage().getParameters().put('parameter', 'RITEFormRules');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'View'+subContract1.Id);
        ApexPages.currentPage().getParameters().put('paramterId',subContract1.Id );
        ApexPages.currentPage().getParameters().put('parameter', 'RITEFormRules');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'View'+subContract.Id);
        ApexPages.currentPage().getParameters().put('paramterId',subContract.Id );
        ApexPages.currentPage().getParameters().put('parameter', 'RITEFormRules');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'View'+contEnv.Id);
        ApexPages.currentPage().getParameters().put('paramterId',contEnv.Id );
        ApexPages.currentPage().getParameters().put('parameter', 'RITEFormRules');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'View'+contEnv1.Id);
        ApexPages.currentPage().getParameters().put('paramterId',contEnv1.Id );
        ApexPages.currentPage().getParameters().put('parameter', 'RITEFormRules');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'AddProjInfo');
        ApexPages.currentPage().getParameters().put('parameter', 'PrintPreview');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'AddSubContracts');
        ApexPages.currentPage().getParameters().put('parameter', 'PrintPreview');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'AddTechManufacturingScope');
        ApexPages.currentPage().getParameters().put('parameter', 'PrintPreview');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'AddContractualEnvironment');
        ApexPages.currentPage().getParameters().put('parameter', 'PrintPreview');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('parametername', 'AddCommercialCompetitive');
        ApexPages.currentPage().getParameters().put('parameter', 'PrintPreview');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('id', quoteLinks[0].id);
        ApexPages.currentPage().getParameters().put('parametername', 'AddProjMgt');
        ApexPages.currentPage().getParameters().put('parameter', 'RITE');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('id', quoteLinks[0].id);
        ApexPages.currentPage().getParameters().put('parametername', 'AddBudget');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('id', quoteLinks[0].id);
        ApexPages.currentPage().getParameters().put('parametername', 'AddConsortium');
        riteForm.pageRedirect();
        
        ApexPages.currentPage().getParameters().put('id', quoteLinks[1].id);
        ApexPages.currentPage().getParameters().put('parametername', 'AddFinEnv');
        riteForm.pageRedirect();

        ApexPages.currentPage().getParameters().put('parametername', 'AddConsortium'); 
        ApexPages.currentPage().getParameters().put('id',quoteLinks[0].id );              
        riteForm.pageRedirect();
        
        con = new ApexPages.StandardController(quoteLink1);
        pageRef = Page.VFP13_RITEForm;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', quoteLinks[1].id);
        //Test method Execution Begins
                
        riteForm = new VFC13_RITEForm(con);
        riteForm  = new VFC13_RITEForm();
        Test.StopTest();
    }
    static testMethod void testRITEForm1() 
    {       
        Test.startTEst();
        con = new ApexPages.StandardController(quoteLink);
        riteForm = new VFC13_RITEForm(con);
        ApexPages.currentPage().getParameters().put('id', quoteLinks[1].id);
        ApexPages.currentPage().getParameters().put('parametername', null);
        ApexPages.currentPage().getParameters().put('paramterId',null );
        ApexPages.currentPage().getParameters().put('parameter', null);
        riteForm.pageRedirect();
        
        riteForm.getProjInfo();
        riteForm.getProjMgnt();
        riteForm.getCommComp();
        riteForm.getTechManf();
        riteForm.getSubCont();
        riteForm.getBudget();
        riteForm.getConsortium();
        riteForm.getFinEnv();
        riteForm.getContEnv();
        
        con = new ApexPages.StandardController(quoteLink1);
        riteForm = new VFC13_RITEForm(con);
        riteForm.pageRedirect();

        riteForm.getProjInfo();
        riteForm.getProjMgnt();
        riteForm.getCommComp();
        riteForm.getTechManf();
        riteForm.getSubCont();
        riteForm.getBudget();
        riteForm.getConsortium();
        riteForm.getFinEnv();
        riteForm.getContEnv();
        
        con = new ApexPages.StandardController(quoteLink2);
        riteForm = new VFC13_RITEForm();
        riteForm = new VFC13_RITEForm(con);
        ApexPages.currentPage().getParameters().put('parametername', 'View'+projInfo1.id);
        ApexPages.currentPage().getParameters().put('paramterId', projInfo1.id);
        ApexPages.currentPage().getParameters().put('id',quoteLinks[2].id );  
        riteForm.pageRedirect();
        
        riteForm = new VFC13_RITEForm();
        riteForm = new VFC13_RITEForm(con);
        ApexPages.currentPage().getParameters().put('parametername', 'AddConsortium');              
        ApexPages.currentPage().getParameters().put('id',quoteLinks[2].id );  
        riteForm.pageRedirect();
        riteForm.fullRITERedirect();
        Test.stoptest();
        //Test method Execution Ends
    }
}