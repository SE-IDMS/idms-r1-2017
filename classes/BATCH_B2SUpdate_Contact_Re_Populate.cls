global class BATCH_B2SUpdate_Contact_Re_Populate implements Database.Batchable<sObject>
{
    String query;

    global BATCH_B2SUpdate_Contact_Re_Populate(String Q)
    {
        query=Q;
    }
      
    global Database.querylocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
      
    global void execute(Database.BatchableContext BC, List<Contact> scope)  // TODO : Update the object name in List<_______>
    {
        //List<Contact> accns = new List<Contact>();
        for(Contact s : scope)
        {
            Contact a = (Contact)s;
            
            // TODO : Update the fields to be re populated 
            //a.OtherStateCode=a.TECH_SDH_StateProvCode__c; 
            //a.OtherCountryCode=a.TECH_SDH_CountryCode__c;
            a.OtherCity=a.City__c;
            a.OtherStreet=a.Street__c;
            a.OtherPostalCode=a.ZipCode__c;
            //accns.add(a);            
                  
        }
        
       
        
        Database.SaveResult[] srList = Database.update(scope, false);
        for(Database.SaveResult sr : srList) 
        {
            if (sr.isSuccess()) 
            {
                System.debug('Updated successfully for ' +
                sr.getId());
            }
            else 
            {
                System.debug('Updating returned the following errors.');
                for(Database.Error e : sr.getErrors()) {
                System.debug(e.getMessage());
            }
        }

    }
      
   }
    global void finish(Database.BatchableContext BC)
    {
    }  
    
}