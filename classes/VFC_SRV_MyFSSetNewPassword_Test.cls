@isTest
public class VFC_SRV_MyFSSetNewPassword_Test {
    
    public static testMethod void testResetPassword() {

        Test.startTest();
        Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());        
        
        VFC_SRV_MyFSSetNewPassword.setNewPasswordRemoting('Password', 'Token');
        VFC_SRV_MyFSSetNewPassword.setNewPasswordRemoting('Password', null);
        VFC_SRV_MyFSSetNewPassword.setNewPasswordRemoting(null, null);     
        
        VFC_SRV_MyFsLoginPage.ResetPasswordResult resetPasswordResult = new VFC_SRV_MyFsLoginPage.ResetPasswordResult();
        
        VFC_SRV_MyFSSetNewPassword.debugMode = true;
        VFC_SRV_MyFSSetNewPassword.handleException('Exception Message', resetPasswordResult);
        VFC_SRV_MyFSSetNewPassword.debugMode = false;
        VFC_SRV_MyFSSetNewPassword.handleException('Exception Message', resetPasswordResult);
        VFC_SRV_MyFSSetNewPassword.handleException('The link in the email has expired', resetPasswordResult);
    }

}