@isTest

private class FieloPRM_REST_PostRemoveBadgeContactTest{

    static testMethod void unitTest(){
        
         
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
    
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'testNamePolo';
            member.FieloEE__FirstName__c = 'testNameMarco';
            member.FieloEE__Street__c = 'test';
          
            insert member;
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.Name = 'test ProgramLevel';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_BadgeAPIName__c = 'asd'; 
            insert badge;
            
            FieloEE__BadgeMember__c badMem = new FieloEE__BadgeMember__c();
            badMem.FieloEE__Member2__c = member.id;
            badMem.FieloEE__Badge2__c = badge.id;
            insert badMem;

            FieloPRM_BadgeAccount__c badgeAcc = new FieloPRM_BadgeAccount__c ();  
            badgeAcc.F_PRM_Badge__c = badge.id;
            badgeAcc.F_PRM_Account__c = acc.id;
            
            insert badgeAcc;
    
            
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            con1.FieloEE__Member__c = member.id;
            insert con1;
            
           /* Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
            con1.SEContactID__c = 'test';
            update con1;*/
                     
            list<String> listIds = new list<String>();
            listIds.add(con1.id);
        
            FieloPRM_REST_PostRemoveBadgeFromContact rest = new FieloPRM_REST_PostRemoveBadgeFromContact();
            Map<String ,List<String>> accToBadges = new Map<String ,List<String>>();
            List<String> aux = new List<String>();
            aux.add(badge.F_PRM_BadgeAPIName__c);
            accToBadges.put(con1.SEContactID__c , aux);
            FieloPRM_REST_PostRemoveBadgeFromContact.postRemoveBadgeFromContact('Program Level', accToBadges);
        }
    
    }
    
}