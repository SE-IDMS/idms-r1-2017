public class VFC_updateTargetAndSegmentation {
    
    PartnerProgram__c  partProg;
    PartnerProgram__c  pgmType;
    list<PartnerProgramClassification__c> lstExistingClasification = new list<PartnerProgramClassification__c>();
    list<PartnerProgramClassification__c> lstExistingLvl2 = new list<PartnerProgramClassification__c>();
    
    Set<Id> selOptsCL1 = new Set<Id>();
    public Boolean hasError{get;set;}
    public SelectOption[] selClassificationLevel { get; set; }
    public SelectOption[] allClassificationLevel{ get; set; }
    public String CLtype {get;set;}
    
    list<PartnerProgramMarket__c> lstExistingMarketSeg = new list<PartnerProgramMarket__c>();
    list<PartnerProgramMarket__c> lstExistingSubSeg = new list<PartnerProgramMarket__c>();
    Set<Id> selOptsMS1 = new Set<Id>();
    public SelectOption[] selMarketSeg { get; set; }
    public SelectOption[] allMarketSeg{ get; set; }
    public String MStype {get;set;}
    
    list<ProgramDomainsOfExpertise__c> lstExistingDE = new list<ProgramDomainsOfExpertise__c>();
    list<ProgramDomainsOfExpertise__c> lstExistingDE2 = new list<ProgramDomainsOfExpertise__c>();
    Set<Id> selOptsDE1 = new Set<Id>();
    public SelectOption[] selDE { get; set; }
    public SelectOption[] allDE{ get; set; }
    public String DEtype {get;set;}

    list<ProgramProduct__c> lstExistingPP = new list<ProgramProduct__c>();
    list<ProgramProduct__c> lstExistingPP2 = new list<ProgramProduct__c>();
    Set<Id> selOptsPP1 = new Set<Id>();
    public SelectOption[] selPP { get; set; }
    public SelectOption[] allPP{ get; set; }
    public String PPtype {get;set;}
    
    
    Map<id,List<id>> mapClassLevel = new Map<id,List<id>>();
    integer count;
    integer l2count;
    
    public VFC_updateTargetAndSegmentation(ApexPages.StandardController controller) 
    {
        count = 0;
        l2count = 0;
        
        List<String> Fields=new List<String>{'ClassificationLevel1__c','ClassificationLevel2__c'};
        if(!test.isRunningTest())
            controller.addFields(Fields);
        selClassificationLevel = new List<SelectOption>();  
        allClassificationLevel = new List<SelectOption>();
        set<String> setCL1 = new set<String>();
        partProg = (PartnerProgram__c)controller.getRecord();
        CLtype = ApexPages.currentPage().getParameters().get('CLtype');
        
        pgmType = [Select ProgramType__c From PartnerProgram__c Where Id= : partProg.Id];
        
        //lstExistingClasification = [Select classificationlevelCatalog__r.Name, ClassificationLevelCatalog__c,classificationlevelCatalog__r.classificationlevelname__c from PartnerProgramClassification__c where partnerprogram__c = : partProg.Id and classificationlevelCatalog__r.parentclassificationlevel__c = null order by classificationlevelCatalog__r.classificationlevelname__c limit 1000];
        //OCt14
        lstExistingClasification = [Select classificationlevelCatalog__r.Name, ClassificationLevelCatalog__c,classificationlevelCatalog__r.classificationlevelname__c,
                                        classificationlevelCatalog__r.parentclassificationlevel__c 
                                    from PartnerProgramClassification__c where partnerprogram__c = : partProg.Id 
                                    order by classificationlevelCatalog__r.classificationlevelname__c limit 1000];
            
        Map<id,id> mapExistClassificationLevel2 = new Map<id,id>();
        if(CLtype == Label.CLJUN13PRM03)
        {
            if(lstExistingClasification != null && !lstExistingClasification.isEmpty())
            {
                for (PartnerProgramClassification__c s : lstExistingClasification ) 
                {
                    if(s.classificationlevelCatalog__r.parentclassificationlevel__c == null)
                        selClassificationLevel.add(new SelectOption(s.ClassificationLevelCatalog__c,s.classificationlevelCatalog__r.classificationlevelname__c));
                    selOptsCL1.add(s.ClassificationLevelCatalog__c);
                    //setCL1.add(s.classificationlevelCatalog__r.Name);
                    if(s.classificationlevelCatalog__r.parentclassificationlevel__c != null){
                        selClassificationLevel.add(new SelectOption(s.ClassificationLevelCatalog__c+'_'+s.classificationlevelCatalog__r.parentclassificationlevel__c,'  - ' + s.classificationlevelCatalog__r.classificationlevelname__c));
                        mapExistClassificationLevel2.put(s.ClassificationLevelCatalog__c,s.ClassificationLevelCatalog__c);
                        
                    }   
                }
                System.debug('selOptsCL1.size()'+selOptsCL1.size());  
            }
            System.debug('selClassificationLevel.size() outside IF'+selClassificationLevel);
            list<ClassificationLevelCatalog__c> lstAllClassificationLevel1 = new list<ClassificationLevelCatalog__c>();
            // lstAllClassificationLevel1 = [SELECT id,name,classificationlevelname__c FROM ClassificationLevelCatalog__c WHERE ParentClassificationLevel__c = null and active__c = true order by classificationlevelname__c limit 1000];        
            lstAllClassificationLevel1 = [SELECT Id, Name, ClassificationLevelName__c, (SELECT Id, Name, ClassificationLevelName__c FROM Classification_Levels__r ORDER BY ClassificationLevelName__c) FROM ClassificationLevelCatalog__c WHERE ParentClassificationLevel__c = null ORDER BY ClassificationLevelName__c LIMIT 1000];
            for(ClassificationLevelCatalog__c classificationLevel : lstAllClassificationLevel1) {
                //if(!(selOptsCL1.contains(classificationLevel.Id)))
                // Modify the logic to not add the parent level if all childlevels are already selected
                allClassificationLevel.add(new SelectOption(classificationLevel.Id, classificationLevel.classificationlevelname__c ));
                for (ClassificationLevelCatalog__c childLevel : classificationLevel.Classification_Levels__r) {
                    if(!mapExistClassificationLevel2.containsKey(childLevel.Id))
                        allClassificationLevel.add(new SelectOption(childLevel.Id + '_' + classificationLevel.Id, '  - ' + childLevel.classificationlevelname__c ));                                        
                    
                }
            }
        }

        else if(CLtype == Label.CLOCT13PRM80)
        {
            list<ClassificationLevelCatalog__c> lstClassificationLevel2 = new list<ClassificationLevelCatalog__c>();
            if(lstExistingClasification != null && !lstExistingClasification.isEmpty())
            {
                for (PartnerProgramClassification__c s : lstExistingClasification ) 
                {
                    setCL1.add(s.classificationlevelCatalog__r.Name);
                }
                lstClassificationLevel2 = [SELECT id,name,classificationlevelname__c FROM ClassificationLevelCatalog__c WHERE ParentClassificationLevel__r.name in:setCL1 and active__c = true order by classificationlevelname__c limit 1000];
            }
            else
            {
                hasError = true; 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select Classfication Level 1'));
            }
            lstExistingLvl2 = [Select id,ClassificationLevelCatalog__c,classificationlevelCatalog__r.classificationlevelname__c from PartnerProgramClassification__c WHERE partnerprogram__c = : partProg.Id and classificationlevelCatalog__r.parentclassificationlevel__c != null order by classificationlevelCatalog__r.classificationlevelname__c limit 5000];
            for(PartnerProgramClassification__c existingLvl2 : lstExistingLvl2)
            {
                selClassificationLevel.add(new SelectOption(existingLvl2.ClassificationLevelCatalog__c,existingLvl2.classificationlevelCatalog__r.classificationlevelname__c));
                selOptsCL1.add(existingLvl2.ClassificationLevelCatalog__c);
            }
            for(ClassificationLevelCatalog__c allLvl2 : lstClassificationLevel2 )
            {
                if(!(selOptsCL1.contains(allLvl2.Id)))
                    allClassificationLevel.add(new SelectOption(allLvl2.Id, allLvl2.classificationlevelname__c ));
            }
            
        }//End of Classification Level
        
        //Market Segment
        selMarketSeg = new List<SelectOption>();  
        allMarketSeg = new List<SelectOption>();
        set<String> setMS1 = new set<String>();
        partProg = (PartnerProgram__c)controller.getRecord();
        MStype = ApexPages.currentPage().getParameters().get('MStype');
        lstExistingMarketSeg = [Select MarketSegmentCatalog__r.Name, MarketSegmentCatalog__c,MarketSegmentCatalog__r.MarketSegmentName__c from PartnerProgramMarket__c where partnerprogram__c = : partProg.Id and MarketSegmentCatalog__r.ParentMarketSegment__c = null order by MarketSegmentCatalog__r.MarketSegmentName__c limit 1000];
        if(MStype == Label.CLJUN13PRM05)
        {
            if(lstExistingMarketSeg != null && !lstExistingMarketSeg.isEmpty())
            {
                for (PartnerProgramMarket__c s : lstExistingMarketSeg ) 
                {
                    selMarketSeg.add(new SelectOption(s.MarketSegmentCatalog__c,s.MarketSegmentCatalog__r.MarketSegmentName__c));
                    selOptsMS1.add(s.MarketSegmentCatalog__c);
                    
                }  
            }
            
            list<MarketSegmentCatalog__c> lstallMarketSeg1 = new list<MarketSegmentCatalog__c>();
            lstallMarketSeg1 = [SELECT id,name,MarketSegmentName__c FROM MarketSegmentCatalog__c WHERE ParentMarketSegment__c = null order by MarketSegmentName__c limit 1000];        
            for(MarketSegmentCatalog__c MarketSegment : lstallMarketSeg1) 
            {
                if(!(selOptsMS1.contains(MarketSegment.Id)))
                    allMarketSeg.add(new SelectOption(MarketSegment.Id, MarketSegment.MarketSegmentName__c ));
            }
        }

        else if(MStype == Label.CLOCT13PRM81)
        {
            list<MarketSegmentCatalog__c> lstMarketSegment = new list<MarketSegmentCatalog__c>();
            if(lstExistingMarketSeg != null && !lstExistingMarketSeg.isEmpty())
            {
                for (PartnerProgramMarket__c s : lstExistingMarketSeg ) 
                {
                    setMS1.add(s.MarketSegmentCatalog__r.Name);
                }
                lstMarketSegment = [SELECT id,name,MarketSegmentName__c FROM MarketSegmentCatalog__c WHERE ParentMarketSegment__r.name in:setMS1 order by MarketSegmentName__c limit 1000];
            }
            else
            {
                hasError = true; 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select Market Segment'));
            }
            lstExistingSubSeg = [Select id,MarketSegmentCatalog__c,MarketSegmentCatalog__r.MarketSegmentName__c from PartnerProgramMarket__c WHERE partnerprogram__c = : partProg.Id and MarketSegmentCatalog__r.ParentMarketSegment__c != null order by MarketSegmentCatalog__r.MarketSegmentName__c limit 5000];
            for(PartnerProgramMarket__c existingLvl2 : lstExistingSubSeg)
            {
                selMarketSeg.add(new SelectOption(existingLvl2.MarketSegmentCatalog__c,existingLvl2.MarketSegmentCatalog__r.MarketSegmentName__c));
                selOptsMS1.add(existingLvl2.MarketSegmentCatalog__c);
            }
            for(MarketSegmentCatalog__c allLvl2 : lstMarketSegment )
            {
                if(!(selOptsMS1.contains(allLvl2.Id)))
                    allMarketSeg.add(new SelectOption(allLvl2.Id, allLvl2.MarketSegmentName__c ));
            }
            
        } 
        //End of Market Segement
        
        //Strat Domains of expertise
        selDE = new List<SelectOption>();  
        allDE = new List<SelectOption>();
        set<String> setDE1 = new set<String>();
        partProg = (PartnerProgram__c)controller.getRecord();
        DEtype = ApexPages.currentPage().getParameters().get('DEtype');
        //lstExistingDE = [Select DomainsOfExpertiseCatalog__r.Name, DomainsOfExpertiseCatalog__c ,DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c from ProgramDomainsOfExpertise__c where partnerprogram__c = : partProg.Id and DomainsOfExpertiseCatalog__r.ParentDomainsOfExpertise__c = null order by DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c limit 1000];
        lstExistingDE = [Select DomainsOfExpertiseCatalog__r.Name, DomainsOfExpertiseCatalog__c ,DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c from ProgramDomainsOfExpertise__c where partnerprogram__c = : partProg.Id  order by DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c limit 1000];
        
        if(lstExistingDE != null && !lstExistingDE.isEmpty())
        {
            for (ProgramDomainsOfExpertise__c s : lstExistingDE ) 
            {

                selDE.add(new SelectOption(s.DomainsOfExpertiseCatalog__c ,s.DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c));
                selOptsDE1.add(s.DomainsOfExpertiseCatalog__c );
                //setDE1.add(s.DomainsOfExpertiseCatalog__r.Name);
            }  
        }
        
        list<DomainsOfExpertiseCatalog__c > lstAllDE = new list<DomainsOfExpertiseCatalog__c >();
        lstAllDE = [SELECT id,name,DomainsOfExpertiseName__c FROM DomainsOfExpertiseCatalog__c  WHERE ParentDomainsOfExpertise__c != null and active__c = true order by DomainsOfExpertiseName__c limit 1000];        
        for(DomainsOfExpertiseCatalog__c  DE : lstAllDE) 
        {
            if(!(selOptsDE1.contains(DE.Id)))
                allDE.add(new SelectOption(DE.Id, DE.DomainsOfExpertiseName__c ));
        }
        //End of domains of expertise  

        //Start PP
        selPP = new List<SelectOption>();  
        allPP = new List<SelectOption>();
        set<String> setPP1 = new set<String>();
        partProg = (PartnerProgram__c)controller.getRecord();
        PPtype = ApexPages.currentPage().getParameters().get('DEtype');
        //System.debug('ProgramType----------'+pgmType.ProgramType__c);
        //lstExistingDE = [Select DomainsOfExpertiseCatalog__r.Name, DomainsOfExpertiseCatalog__c ,DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c from ProgramDomainsOfExpertise__c where partnerprogram__c = : partProg.Id and DomainsOfExpertiseCatalog__r.ParentDomainsOfExpertise__c = null order by DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c limit 1000];
        lstExistingPP = [Select ProductLineCatalog__r.Name, ProductLineCatalog__c ,ProductLineCatalog__r.ProductName__c from ProgramProduct__c where partnerprogram__c = : partProg.Id  order by ProductLineCatalog__r.ProductName__c limit 1000];
        
        if(lstExistingPP != null && !lstExistingPP.isEmpty())
        {
            for (ProgramProduct__c pp : lstExistingPP ) 
            {

                selPP.add(new SelectOption(pp.ProductLineCatalog__c ,pp.ProductLineCatalog__r.ProductName__c));
                selOptsPP1.add(pp.ProductLineCatalog__c );
                //setPP1.add(pp.ProductLineCatalog__r.Name);
            }  
        }
        
        list<ProductLineCatalog__c > lstAllPP = new list<ProductLineCatalog__c >();
        lstAllPP = [SELECT id,name,ProductName__c FROM ProductLineCatalog__c  WHERE active__c = true order by ProductName__c limit 1000];        
        for(ProductLineCatalog__c  plc : lstAllPP) 
        {
            if(!(selOptsPP1.contains(plc.Id)))
                allPP.add(new SelectOption(plc.Id, plc.ProductName__c ));
        }

        //End of PP     
        
    }//End of Constrictor
    
    public PageReference Save() 
    {
        Savepoint sp =  Database.setSavepoint();
        count = 0;
        l2count = 0;
        try
        {            
            set<Id> setSelIds = new set<ID>();
            list<PartnerProgramClassification__c> lstNewPrgClassification = new list<PartnerProgramClassification__c>();
            for(SelectOption so : selClassificationLevel) 
            {
               PartnerProgramClassification__c partClasification = new PartnerProgramClassification__c();
                partClasification.partnerprogram__c = partProg.Id;
                String classNew ; //OCT14
                if(so.getValue().contains('_')) {
                    classNew = so.getValue().substringBefore('_'); //OCT14
                    l2count = l2count +1;
                }
                else {
                    classNew = so.getValue();       //OCT14
                    count = count +1;            
                } 
                partClasification.ClassificationLevelCatalog__c = classNew;
                lstNewPrgClassification.add(partClasification);
                setSelIds.add(classNew);
                System.debug('setSelIds----'+setSelIds);
                System.debug('classNew----'+classNew);
            }
            System.debug('count----'+count);
            System.debug('l2count----'+l2count);
            if(CLtype == Label.CLJUN13PRM03 && lstExistingClasification != null && !lstExistingClasification.isEmpty())
            {
                System.debug('selOptsCL1----'+selOptsCL1);
                selOptsCL1.removeAll(setSelIds);
                /*List<Id> listDel = new List<Id>();
                for(PartnerProgramClassification__c delList : lstExistingClasification)
                {
                    listDel.add(delList.Id);
                }*/
                lstExistingLvl2 = [Select id,ClassificationLevelCatalog__c,classificationlevelCatalog__r.classificationlevelname__c from PartnerProgramClassification__c WHERE partnerprogram__c = : partProg.Id and classificationlevelCatalog__r.parentclassificationlevel__c in : selOptsCL1 limit 5000];
                //lstExistingLvl2 = [Select id from PartnerProgramClassification__c WHERE partnerprogram__c = : partProg.Id and classificationlevelCatalog__r.parentclassificationlevel__c in : selOptsCL1 and classificationlevelCatalog__r.parentclassificationlevel__c not in : listDel limit 5000];
                //System.debug('lstExistingLvl2----'+lstExistingLvl2);
                delete lstExistingClasification;
                //System.debug('lstExistingLvl2----'+lstExistingLvl2);
                //delete lstExistingLvl2;
                //delete lstExistingClasification;
            }
            else if(CLtype == Label.CLOCT13PRM80 && lstExistingLvl2 != null && !lstExistingLvl2.isEmpty())
            {
                delete lstExistingLvl2;
            }
           // lstExistingClasification.clear();
            
            //End of Classification Level    
            
            //Market Segment
             set<Id> setSelMSIds = new set<ID>();
            list<PartnerProgramMarket__c > lstNewPrgMarket = new list<PartnerProgramMarket__c>();
            for(SelectOption so : selMarketSeg) 
            {
                PartnerProgramMarket__c partSegment = new PartnerProgramMarket__c();
                partSegment.partnerprogram__c = partProg.Id;
                partSegment.MarketSegmentCatalog__c = so.getValue();
                lstNewPrgMarket.add(partSegment);
                setSelMSIds.add(so.getValue());
                System.debug('setSelMSIds----'+setSelMSIds);
            }
            if(MStype == Label.CLJUN13PRM05 && lstExistingMarketSeg != null && !lstExistingMarketSeg.isEmpty())
            {
                System.debug('selOptsMS1----'+selOptsMS1);
                selOptsMS1.removeAll(setSelMSIds);
                lstExistingSubSeg = [Select id from PartnerProgramMarket__c WHERE partnerprogram__c = : partProg.Id and MarketSegmentCatalog__r.ParentMarketSegment__c in:selOptsMS1 limit 5000];
                delete lstExistingMarketSeg;
                delete lstExistingSubSeg;
                //partProg.MarketSegment2__c = '';
                
            }
            else if(MStype == Label.CLOCT13PRM81 && lstExistingSubSeg != null && !lstExistingSubSeg.isEmpty())
            {
                delete lstExistingSubSeg;
            }
           // lstExistingMarketSeg.clear();

            //End of Market Segment   
            
            //Start Domains of Expertise
            set<Id> setSelDEIds = new set<ID>();
            list<ProgramDomainsOfExpertise__c> lstNewPrgDE = new list<ProgramDomainsOfExpertise__c>();
            for(SelectOption so : selDE) 
            {
                ProgramDomainsOfExpertise__c partDE = new ProgramDomainsOfExpertise__c();
                partDE.partnerprogram__c = partProg.Id;
                partDE.DomainsOfExpertiseCatalog__c  = so.getValue();
                lstNewPrgDE.add(partDE);
                setSelDEIds.add(so.getValue());
            }
            Delete lstExistingDE;
          //  lstExistingDE.clear();
            
            //End of Domains of Expertise 

            //Start Partner Products
            set<Id> setSelPPIds = new set<ID>();
            list<ProgramProduct__c> lstNewPrgProd = new list<ProgramProduct__c>();
            for(SelectOption so : selPP) 
            {
                ProgramProduct__c partPP = new ProgramProduct__c();
                partPP.partnerprogram__c = partProg.Id;
                partPP.ProductLineCatalog__c  = so.getValue();
                lstNewPrgProd.add(partPP);
                setSelPPIds.add(so.getValue());
            }
            Delete lstExistingPP;
           // lstExistingPP.clear();
            System.debug('ProgramType----------'+pgmType.ProgramType__c);
            System.debug('lstNewPrgClassification----------'+lstNewPrgClassification);
            System.debug('lstNewPrgClassification empty----------'+lstNewPrgClassification.isEmpty());
            System.debug('selOptsCL1----------'+selOptsCL1.size());
            System.debug('selClassificationLevel.size() outside IF'+selClassificationLevel);
            System.debug('count outside if----------'+count);
            System.debug('l2count outside IF'+l2count);
            if((pgmType.ProgramType__c==System.Label.CLOCT14PRM60) && ((lstNewPrgClassification != null && lstNewPrgClassification.isEmpty())))
            {
                
                hasError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLOCT14PRM61));
                Database.rollback(sp);
                
                lstNewPrgClassification.clear();
                lstNewPrgMarket.clear();
                lstNewPrgDE.clear();
                lstNewPrgProd.clear();

                return null;
               
            }
            else if((pgmType.ProgramType__c==System.Label.CLOCT14PRM60) && (count > 1 || l2count > 1)) {
                hasError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLOCT14PRM79));
                Database.rollback(sp);
                
                lstNewPrgClassification.clear();
                lstNewPrgMarket.clear();
                lstNewPrgDE.clear();
                lstNewPrgProd.clear();
                
                return null;        
            }
            else if((pgmType.ProgramType__c!=System.Label.CLOCT14PRM60) && ((lstNewPrgClassification != null && lstNewPrgClassification.isEmpty()) ||
                (lstNewPrgMarket != null && lstNewPrgMarket.isEmpty()) ||
                (lstNewPrgDE != null && lstNewPrgDE.isEmpty()) ||
                (lstNewPrgProd != null && lstNewPrgProd.isEmpty())))
                {
                    hasError = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLMAY14PRM06));
                    Database.rollback(sp);

                    lstNewPrgClassification.clear();
                    lstNewPrgMarket.clear();
                    lstNewPrgDE.clear();
                    lstNewPrgProd.clear();

                    return null;
                }
            
            lstExistingClasification.clear();
            lstExistingMarketSeg.clear();
            lstExistingPP.clear();
            lstExistingDE.clear();
            

            if(lstNewPrgClassification != null && !lstNewPrgClassification.isEmpty())
               insert lstNewPrgClassification;

            if(lstNewPrgMarket != null && !lstNewPrgMarket.isEmpty())
                insert lstNewPrgMarket;

            if(lstNewPrgDE != null && !lstNewPrgDE.isEmpty())
                insert lstNewPrgDE;

            if(lstNewPrgProd != null && !lstNewPrgProd.isEmpty())
                insert lstNewPrgProd;
            //End of Partner Products
                
        }
        catch(DMLException e)
        {
            Database.rollback(sp);
            hasError = true;    
            for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),'')); System.debug(e.getDmlMessage(i)); 
            } 
                       
            return null;
        }
        
        return new pagereference('/'+partProg.Id);    
       return null;
    }

}//End of class