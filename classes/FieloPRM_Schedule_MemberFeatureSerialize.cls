/********************************************************************
* Company: Fielo
* Developer: Pablo Cassinerio
* Created Date: 23/08/2016
* Description: 
********************************************************************/
global class FieloPRM_Schedule_MemberFeatureSerialize implements Schedulable{

    global Boolean isPromote;
    global String action = ''; //OK, ERROR, SERIALIZE
    global List<FieloPRM_MemberFeature__c> newFeats;
    global String errorMessage = '';
    
    global void execute(SchedulableContext sc){
        List<FieloPRM_MemberFeature__c> feats = new List<FieloPRM_MemberFeature__c>();
        for(FieloPRM_MemberFeature__c mf: newFeats){
            feats.add(new FieloPRM_MemberFeature__c(Id = mf.Id, F_PRM_Status__c = mf.F_PRM_Status__c, F_PRM_Member__c = mf.F_PRM_Member__c, F_PRM_TryCount__c = mf.F_PRM_TryCount__c, F_PRM_Feature__c = mf.F_PRM_Feature__c));
        }
        set<id> setIdsMember = new set<id>();
        set<id> setIdsFeatures = new set<id>();
        List<FieloEE__ErrorLog__c> logs = new List<FieloEE__ErrorLog__c>();
        
        if(action.equals('ERROR')){
            //ERROR LOGS AND EXIT
            for(FieloPRM_MemberFeature__c feat: feats){
                FieloEE__ErrorLog__c eLog = new FieloEE__ErrorLog__c(FieloEE__Message__c = errorMessage,
                                    FieloEE__UserId__c = UserInfo.getUserId(),
                                    F_PRM_MemberFeature__c = feat.Id);
                eLog.FieloEE__Type__c = isPromote ? 'Custom logic error' : 'Custom logic demote error';
                logs.add(eLog);
                feat.F_PRM_TryCount__c++;
                feat.F_PRM_IsSynchronic__c = false;
            }
            try{
                update feats;
            }catch(Exception e){} //AFTER DELETE
            
            insert logs;
            return;
        }else if(action.equals('OK')){
            for(FieloPRM_MemberFeature__c feat: feats){
                try{
                    feat.F_PRM_CustomLogicOK__c = true;    
                }catch(Exception e){}//AFTER DELETE
            }
        }
        
        for(FieloPRM_MemberFeature__c memberFeature : feats){
            setIdsMember.add(memberFeature.F_PRM_Member__c);
            setIdsFeatures.add(memberFeature.F_PRM_Feature__c);
        }
        
        map<id,FieloEE__Member__c> mapIdMember = new map<id,FieloEE__Member__c>([SELECT id, F_PRM_MemberFeatures__c FROM  FieloEE__Member__c WHERE id in : setIdsMember]);
        map<id,FieloPRM_Feature__c> mapIdFeature = new map<id,FieloPRM_Feature__c>([SELECT id,Name FROM  FieloPRM_Feature__c WHERE id in : setIdsFeatures]);
        map<String,String> mapMemberIdToFeatures= new map<String,String>();
        list<FieloEE__Member__c> listMemberToUpdate = new list<FieloEE__Member__c>();
        
        for(FieloPRM_MemberFeature__c memberFeature : feats){
            FieloEE__Member__c member = mapIdMember.get(memberFeature.F_PRM_Member__c);
            FieloPRM_Feature__c feature = mapIdFeature.get(memberFeature.F_PRM_Feature__c);
            if(isPromote){
                if(feature != null && member != null){
                    if(!mapMemberIdToFeatures.containsKey(member.id)){
                        String AuxSegmentation;
                        if(member.F_PRM_MemberFeatures__c == null){
                            AuxSegmentation = feature.name.trim();
                        }else if( member.F_PRM_MemberFeatures__c.indexOf(feature.name.trim()) == -1){
                            AuxSegmentation = member.F_PRM_MemberFeatures__c + ',' + feature.name.trim();
                        }
                        mapMemberIdToFeatures.put(member.id,AuxSegmentation );
                    }else{
                        String AuxSegmentation = mapMemberIdToFeatures.get(member.id); 
                        if(member.F_PRM_MemberFeatures__c == null){
                            AuxSegmentation  += ',' + feature.name.trim();
                        }else{
                            if(!member.F_PRM_MemberFeatures__c.contains(feature.name.trim()) && member.F_PRM_MemberFeatures__c.indexOf(feature.name.trim()) == -1){
                                AuxSegmentation += ',' + member.F_PRM_MemberFeatures__c + ',' + feature.name.trim();
                            }
                        }
                        mapMemberIdToFeatures.put(member.id, AuxSegmentation );
                    }   
                }
            }else{
                if(!mapMemberIdToFeatures.containsKey(member.id) && member.F_PRM_MemberFeatures__c != null){
                    String programlevelString =  feature.Name.trim();
                    String aux = '';
                    if(member.F_PRM_MemberFeatures__c != null && member.F_PRM_MemberFeatures__c != ''){
                        aux = member.F_PRM_MemberFeatures__c.replaceAll(programlevelString,'');
                    }
                    aux  = aux.replaceAll(',,',',');
                    if(aux.right(1) == ','){
                        aux  = aux.left(aux.length()-1);
                    }
                    if(aux.left(1) == ','){
                        aux  = aux.right(aux.length()-1);
                    }
                    mapMemberIdToFeatures.put(member.id, aux);
                }else{
                    String aux = '';
                    if(mapMemberIdToFeatures.get(member.id) != null && mapMemberIdToFeatures.get(member.id) != ''){
                        aux = mapMemberIdToFeatures.get(member.id);
                    }
                    String programlevelString =  feature.Name.trim();
                    aux  = aux.replaceAll(programlevelString,'');   
                    aux  = aux.replaceAll(',,',',');
                    if(aux.right(1) == ','){
                        aux  = aux.left(aux.length()-1);
                    }
                    if(aux.left(1) == ','){
                        aux  = aux.right(aux.length()-1);
                    }
                    mapMemberIdToFeatures.put(member.id, aux);
                }
            }
        }
        
        if(!mapMemberIdToFeatures.isEmpty()){
            for(String idMem : mapMemberIdToFeatures.keySet()){
                FieloEE__Member__c toUpdateSegment = mapIdMember.get(idMem);  
                toUpdateSegment.F_PRM_MemberFeatures__c = mapMemberIdToFeatures.get(idMem) ;  
                listMemberToUpdate.add(toUpdateSegment );
            }
        }
        
        if(listMemberToUpdate.size() > 0){
            List<Database.SaveResult> memberResList = Database.update(listMemberToUpdate, false);
            for(Integer i = 0; i < memberResList.size(); i++){
                Database.SaveResult sr = memberResList[i];
                if(!sr.isSuccess()){
                    for(FieloPRM_MemberFeature__c feat: feats){
                        if(feat.F_PRM_Member__c == listMemberToUpdate.get(i).Id){
                            for(Database.Error err : sr.getErrors()) {
                                logs.add(new FieloEE__ErrorLog__c(FieloEE__Message__c = err.getMessage(),
                                        FieloEE__StackTrace__c = 'Status code:' + err.getStatusCode(), 
                                        FieloEE__Type__c = isPromote ? 'Serialization error' : 'Deserialization error', 
                                        FieloEE__UserId__c = UserInfo.getUserId(),
                                        F_PRM_MemberFeature__c = feat.Id));
                                try{
                                    feat.F_PRM_TryCount__c++;
                                    feat.F_PRM_IsSynchronic__c = false;
                                }catch(Exception e){}//AFTER DELETE
                            }
                        }
                    }
                }else{
                    for(FieloPRM_MemberFeature__c feat: feats){
                        if(feat.F_PRM_Member__c == listMemberToUpdate.get(i).Id){
                            try{
                                feat.F_PRM_SerializeOK__c = true;
                            }catch(Exception e){}//AFTER DELETE
                        }
                    }
                }
            }
        }
        if(!logs.isEmpty()){
            insert logs;
        }
        try{
            update feats;
        }catch(Exception e){}//AFTER DELETE
    }

    public static void scheduleNow(Boolean isPro, String action, List<FieloPRM_MemberFeature__c> newFeats, String err){  
        DateTime nowTime = datetime.now().addSeconds(65);
        String Seconds      = '0';
        String Minutes      = nowTime.minute() < 10 ? '0' + nowTime.minute() : String.valueOf(nowTime.minute());
        String Hours        = nowTime.hour()   < 10 ? '0' + nowTime.hour()   : String.valueOf(nowTime.hour());
        String DayOfMonth   = String.valueOf(nowTime.day());
        String Month        = String.ValueOf(nowTime.month());
        String DayOfweek    = '?';
        String optionalYear = String.valueOf(nowTime.year());
        String CronExpression = Seconds+' '+Minutes+' '+Hours+' '+DayOfMonth+' '+Month+' '+DayOfweek+' '+optionalYear;
        FieloPRM_Schedule_MemberFeatureSerialize cl = new FieloPRM_Schedule_MemberFeatureSerialize();
        cl.isPromote = isPro;
        cl.action = action;
        cl.newFeats= newFeats;
        cl.errorMessage = err;
        try{
            System.schedule('FieloPRM_Schedule_MemberFeatureSerialize '+system.now() + ':' + system.now().millisecond(), CronExpression, cl);    
        }catch(Exception e){

        }catch(System.UnexpectedException e){

        }
    }
}