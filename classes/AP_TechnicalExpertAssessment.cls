public class AP_TechnicalExpertAssessment
{
  
   
public static void updateCSIFlagOnTex(List<TEX__c> ListTEX)
   {
        Map<id,id> mapofTexCaseIds=new map<id,id>();  
        Map<id,id> mapofCaseCSIids=new Map<id,id>();
        for(TEX__c tempTex:ListTEX)
            {
                if(tempTex.Case__c!=null)
                    { 
                        mapofTexCaseIds.put(tempTex.id,tempTex.Case__c);
                         
                    }
            }
        List<CustomerSafetyIssue__c> ListCSI=[SELECT Id,RelatedbFOCase__c FROM CustomerSafetyIssue__c where RelatedbFOCase__c in: mapofTexCaseIds.values()];
        for(CustomerSafetyIssue__c tempCSI:ListCSI)
            {
             mapofCaseCSIids.put(tempCSI.RelatedbFOCase__c,tempCSI.id);
            }
        for(TEX__c Texfinal:ListTEX)
            { 
               if(mapofCaseCSIids.containskey(Texfinal.Case__c))
                   {
                    Texfinal.TECH_CSIFlag__c=true;
                   }
                   else
                   {
                   Texfinal.TECH_CSIFlag__c=false;
                   }
            }
   }
public static void updateCSIFlagOnTexRunOnCSI(List<CustomerSafetyIssue__c> ListCSI)
   {
        set<string> setofCaseIds=new set<string>();  
  
        for(CustomerSafetyIssue__c tempCSI:ListCSI)
            {
                if(tempCSI.RelatedbFOCase__c!=null)
                    { 
                        setofCaseIds.add(tempCSI.RelatedbFOCase__c);                          
                    }
            }
        List<TEX__c> ListTEX=[SELECT Id,TECH_CSIFlag__c FROM TEX__c where Case__c in: setofCaseIds];
        for(TEX__c Texfinal:ListTEX)
              { 
                if(Texfinal.TECH_CSIFlag__c == false)
                   {
                        Texfinal.TECH_CSIFlag__c=true;
                   }
              }
        update ListTEX;
   }    
public static void updateCSIFlagOnTexRunOnCSIAfterUpadate(map<id,string> ListCSI)
   {
        set<string> setofCaseIds=new set<string>(); 
        set<string> setofCaseIdsCSI=new set<string>(); 
        for(string cid:ListCSI.keyset())
            {
                if(ListCSI.get(cid)=='false')
                    {
                        setofCaseIds.add(cid);
                    }
            }
        if (setofCaseIds.size()>0)
        {
            List<CustomerSafetyIssue__c> ListCSIs=[SELECT Id,RelatedbFOCase__c FROM CustomerSafetyIssue__c where RelatedbFOCase__c in: setofCaseIds];
            for(CustomerSafetyIssue__c tempCSI:ListCSIs)
                {
                    setofCaseIdsCSI.add(tempCSI.RelatedbFOCase__c);
                }
        }
        List<TEX__c> ListTEX=[SELECT Id,TECH_CSIFlag__c,Case__c FROM TEX__c where Case__c in: ListCSI.keyset()];
        
            for(TEX__c Texfinal:ListTEX)
                { 
                    if(ListCSI.containskey(Texfinal.Case__c))
                        {
                            if(setofCaseIdsCSI.contains(Texfinal.Case__c))
                                {
                                    Texfinal.TECH_CSIFlag__c=true;
                                }
                            else
                                {
                                    Boolean b = Boolean.valueOf(ListCSI.get(Texfinal.Case__c));
                                    Texfinal.TECH_CSIFlag__c=b;
                                }
                        }
                }
        update ListTEX;
   }
public static void updateOfferExpertonTex(List<TEX__c> ListTEX)
   {   
        List<TEX__c> ListTEXFinal= new list<TEX__c>();  
        for(TEX__c tempTex:ListTEX)
            {
                if(tempTex.OfferExpert__c==null)
                    {
                        tempTex.OfferExpert__c=UserInfo.getUserId();
                    }
            }
   }
   /*
public static void validateTexStatus(List<Case> ListCaseNew)
   {   
        Map<id,case> MapNewCase=new map<id,case>();    
        for(case tempcasen:ListCaseNew)
            { 
                MapNewCase.put(tempcasen.id,tempcasen);
            }
        List<TEX__c> ListTEX=[SELECT Id,Status__c,Case__c FROM TEX__c where Case__c in:MapNewCase.keyset()];
        system.debug('~~~~~ListTEX'+ListTEX);
        for(TEX__c tempTex:ListTEX)
            {
                if(tempTex.Status__c!='Closed')
                    {
                        if(MapNewCase.containskey(tempTex.Case__c))
                            {
                                Case temcase=MapNewCase.get(tempTex.Case__c);
                                temcase.addERROR(Label.CLOCT13RR10);
                            }
                    }
            }
   }
  
   public static void updatePriorityLevelonTex(List<Case> ListCaseNew)
   {   
        Map<id,case> MapNewCase=new map<id,case>();    
        for(case tempcasen:ListCaseNew)
            { 
                MapNewCase.put(tempcasen.id,tempcasen);
            }
        List<TEX__c> ListTEX=[SELECT Id,Case__c FROM TEX__c where Case__c in:MapNewCase.keyset()];
        update listTEX;
   }
    */
public static void updateCaseOwner(List<TEX__c> ListTEX)
    {
        Map<id, List<TEX__c>> mapCaseId= new map<id, List<TEX__c>>();
        for(TEX__c tempTex: ListTEX)
            {
                ID value = tempTex.Case__c;
                if(mapCaseId.Containskey(tempTex.Case__c))
                    {
                        mapCaseId.get(tempTex.Case__c).add(tempTex);
                    }
                else
                    {
                        mapCaseId.put(tempTex.Case__c, new List<TEX__c>());
                        mapCaseId.get(tempTex.Case__c).add(tempTex);
                    }
            }
        List<Case> lstCases = [Select id, Owner.id from Case where id in: mapCaseId.keyset()];
        map<id, id> mapCaseIdOwner = new Map<id,id>();
        for(Case tmpCase: lstCases )
            {
                if(!Test.isRunningTest())
                    { 
                        if(tmpCase.Owner.id.getsobjecttype()==user.sobjecttype)
                            {
                                mapCaseIdOwner.put(tmpCase.id, tmpCase.Owner.id);
                            }
                    }
                else
                    {
                        mapCaseIdOwner.put(tmpCase.id, tmpCase.Owner.id);
                    }       
            }
     
        List<TEX__c> lstNewTex = new List<TEX__c>();
        Id CaseOwnerId;
        for(id caseId: mapCaseId.keyset())
            {
                if(mapCaseIdOwner.containskey(caseId))
                    {
                        CaseOwnerId = mapCaseIdOwner.get(caseId);
                    }
                for(TEX__c tmpTex: mapCaseId.get(caseId))
                    {
                        tmpTex.TECH_CaseOwner__c= CaseOwnerId;            
                    }        
            }
  } 
public static void updateMaterialStatusTEX(List<RMA_Product__c> ListOfRMAItems)
    {
        set<string> setTex=new set<string>();
        List<TEX__c> listofTex=new List<TEX__c>();
        for( RMA_Product__c tempRItem:ListOfRMAItems)
            {
               if(tempRItem.TECH_TEX__c !=null)
                    {
                     setTex.add(tempRItem.TECH_TEX__c);
                    }
            }
        List<TEX__c> listTexIds=getTEXList(setTex);
             system.debug('~~~~listTexIds'+listTexIds);
             System.debug('~~~~listTexIds'+listTexIds);
        for(TEX__c tempTex:listTexIds)
            {
              if(tempTex.Material_Status__c!=Label.CLOCT13RR05)//'In Transit to Expert Center'
                {
                    tempTex.Material_Status__c=Label.CLOCT13RR05;
                    listofTex.add(tempTex);
                }
            } 
        update listofTex;
    }
public static List<TEX__c> getTEXList(set<string>setIds)
    {
        List<TEX__c> listTexIds = new List<TEX__c>();
        listTexIds=[SELECT ID,Material_Status__c from TEX__c where id in:setIds];
        return listTexIds;
    }
public static Map<string,string>getRTItemList(set<string>setIds) 
    {
        Map<string,string> mapofTexidMStus=new map<string,string>();
        List<RMA_Product__c> listRiIds = new List<RMA_Product__c>();
        if(!Test.isRunningTest())
            listRiIds=[SELECT ID,TECH_TEX__c ,LogisticsStatus__c, ApprovalStatus__c from RMA_Product__c where TECH_TEX__c in:setIds];
        if(Test.isRunningTest())
            listRiIds=[SELECT ID,TECH_TEX__c ,LogisticsStatus__c, ApprovalStatus__c from RMA_Product__c where TECH_TEX__c in:setIds LIMIT 100];
        
        System.debug('~~~ listRiIds:' + listRiIds);
            for(RMA_Product__c tempItem:listRiIds)
                {
                    if (tempItem.ApprovalStatus__c !='Rejected')
                        {
                      //      mapofTexidMStus.put(tempItem.TECH_TEX__c,'');
                      //  }
                    //else
                   // {
                    if(tempItem.LogisticsStatus__c !=Label.CLOCT13RR08 && tempItem.LogisticsStatus__c !=Label.CLOCT13RR09)
                        {
                           system.debug('Label.CLOCT13RR08+Label.CLOCT13RR09'+Label.CLOCT13RR08+' '+Label.CLOCT13RR09+' '+ tempItem.LogisticsStatus__c);
                            string tempstatus='';
                            if(tempItem.LogisticsStatus__c !=Label.CLOCT13RR15 )
                                {
                                system.debug('Logistic satus'+tempItem.LogisticsStatus__c);
                               if(tempItem.LogisticsStatus__c !=null)
                                    {
                                    mapofTexidMStus.put(tempItem.TECH_TEX__c,Label.CLOCT13RR06);
                                    }
                                    
                                    
                                }
                                else
                                {
                                if(mapofTexidMStus.containskey(tempItem.TECH_TEX__c))
                                {
                                    //Do nothing
                                } 
                                else
                                {
                                    mapofTexidMStus.put(tempItem.TECH_TEX__c,Label.CLOCT13RR07);  
                                }
                                 
                                
                                }
                                /*
                            else if(tempItem.LogisticsStatus__c ==null ||tempItem.LogisticsStatus__c =='')
                                {
                                    mapofTexidMStus.put(tempItem.TECH_TEX__c,Label.CLOCT13RR05);
                                }
                            else
                                {
                                    tempstatus=Label.CLOCT13RR06;
                                    mapofTexidMStus.put(tempItem.TECH_TEX__c,tempstatus);   
                                }
                                */
                                
                        }
                        else
                        {
                        
                           if(mapofTexidMStus.containskey(tempItem.TECH_TEX__c))
                                {
                                    //Do nothing
                                } 
                                else
                                {
                                    mapofTexidMStus.put(tempItem.TECH_TEX__c,Label.CLOCT13RR07);  
                                }
                        }
                    }
                }
                for(RMA_Product__c tempRI:listRiIds)
                {
                if (tempRI.LogisticsStatus__c ==null)
                                        {
                                          if(mapofTexidMStus.containskey(tempRI.TECH_TEX__c))
                                                {
                                                    mapofTexidMStus.put(tempRI.TECH_TEX__c,Label.CLOCT13RR06);
                                                } 
                                          
                                        }
                                        }
                
            for(string tempid:setIds)
                {
                    if(mapofTexidMStus.containskey(tempid))
                        {
                        //Do nothing
                        } 
                    else
                        {
                            mapofTexidMStus.put(tempid,'');
                        }
                }
        return mapofTexidMStus;
    }
public static List<TEX__c> getUpdatedTEXList(Map<string,string> mapofTexidMStus,List<TEX__c> listTexId)
    {
        List<TEX__c> listTexIds=new List<TEX__c>();
        for(TEX__c tempTex:listTexId)
            {
              string tid=tempTex.id;
              if(tempTex.Material_Status__c !=mapofTexidMStus.get(tid.substring(0, 15)))
                {
                    tempTex.Material_Status__c=mapofTexidMStus.get(tid.substring(0, 15));
                    listTexIds.add(tempTex);
                }                
            }       
        return listTexIds;
    }
    /*  // - APRIL 2014 RELEASE: BR-4296, Ram Prasad Chilukuri, 03Feb2014
public static void updatingMaterialStatusOnTEX(List<RMA_Product__c> ListOfRMAItems){
        System.debug('~~~ ListOfRMAItems:' + ListOfRMAItems);
        Map<string,string> mapofTexidMStus=new Map<string,string>();
        set<string> setTex=new set<string>();
        List<RMA_Product__c> listofRItems=new List<RMA_Product__c>();
        List<TEX__c> listofTex=new List<TEX__c>();
      
        for( RMA_Product__c tempRItem:ListOfRMAItems)
            {
                if(tempRItem.TECH_TEX__c !=null)
                    {
                     setTex.add(tempRItem.TECH_TEX__c);
                    }
            }
             System.debug('~~~ setTex:' + setTex);
        mapofTexidMStus=getRTItemList(setTex);
             System.debug('~~~ mapofTexidMStus :' + mapofTexidMStus );
        List<TEX__c> listTexId= getTEXList(mapofTexidMStus.keyset());
             System.debug('~~~ listTexId :' + listTexId );
        List<TEX__c>listofTexs=getUpdatedTEXList(mapofTexidMStus,listTexId);
             System.debug('~~~ listofTexs:' + listofTexs);
        update listofTexs;
    } 
public static void updateMaterialStatusOnTEXRunOnRI(set<string>setRiIds)
    {   
        Map<string,string> mapofTexidMStus=getRTItemList(setRiIds);
        system.debug('~~~~~~~~RRmapofTexidMStus'+mapofTexidMStus);
        List<TEX__c>listTexId= getTEXList(mapofTexidMStus.keyset());
        system.debug('~~~~~~~~RRlistTexId'+listTexId);
        List<TEX__c>listofTex=getUpdatedTEXList(mapofTexidMStus,listTexId);
        system.debug('~~~~~~~~RRlistofTex'+listofTex);
        update listofTex;
    }
    
    */
    //  APRIL 2014 RELEASE - END
    
    /*
    public static void updateContactEmail(List<TEX__c> ListTEX)
   {
        set<string> SetCaseIds=new set<string>();
        List<TEX__c> ListTEXFinal= new list<TEX__c>();        
        Map<id,string> mapCaseContEmail=new map<id,string>();
        for(TEX__c tempTex:ListTEX)
            {
                if(tempTex.ContactEmail__c !=null)
                    { 
                        if(tempTex.TECH_ContactEmail__c!=tempTex.ContactEmail__c)
                        {
                            tempTex.TECH_ContactEmail__c=tempTex.ContactEmail__c;
                            ListTEXFinal.add(tempTex);   
                        }
                    }
                else
                    { 
                        SetCaseIds.add(tempTex.Case__c);
                    }   
            }
        List<case> Listcase=[SELECT Id, Contact.Email FROM case where id in: SetCaseIds ];
        for(case tempCase:Listcase)
            {
                mapCaseContEmail.put(tempCase.id,tempCase.Contact.Email);               
            }
        for(TEX__c tmpTex:ListTEX)
            {
             if(mapCaseContEmail.containskey(tmpTex.Case__c))
                {
                 tmpTex.TECH_ContactEmail__c=mapCaseContEmail.get(tmpTex.Case__c);
                 ListTEXFinal.add(tmpTex);  
                }
            }        
   }
   */
/*   public static void caseClosing(List<TEX__c> ListofTEXs)
   {   
        set<id> SetCaseIds=new set<id>();
        for (TEX__c temTex:ListofTEXs)
                {
                    SetCaseIds.add(temTex.case__c);             
                }           
                
        set<id> SetNewCase=new set<id>();    
          
        List<TEX__c> ListTEX=[SELECT Id,Status__c,Case__c FROM TEX__c where Case__c in:SetCaseIds];
        
        system.debug('~~~~~ListTEX'+ListTEX);
        for(TEX__c tempTex:ListTEX)
            {
                if(tempTex.Status__c!='Closed')
                    {
                        if(SetCaseIds.contains(tempTex.Case__c))
                            {
                            
                                SetCaseIds.remove(tempTex.Case__c);                                
                            }                           
                    }
            }
            system.debug('~~~~~SetCaseIds'+SetCaseIds);
    List<case> Listcase=[SELECT Id,Status FROM case where id in:SetCaseIds];
    List<case> Listcases=new List<case>();
    for(case tempcase:Listcase) 
        {
         if(tempcase.Status!='Closed')
         {
       //  tempcase.AnswerToCustomer__c='Closing the Case when all the realted TEXs are closed on Case' ; 
         tempcase.Status='Closed';
          Listcases.add(tempcase);
          }
        }
        system.debug('~~~~~List of cases on TEX closing'+Listcases);
        try
        {
        update Listcases;
        }
        catch(Exception e)
        {
        System.debug('Try Catch for Case Closing'+e);
        }
   }
*/   
  //=== sukumar code - start
public static void RejectReturnRequests(List<TEX__c> lstTEXs)
    {
        Set<Id> SetTEXIds = new Set<Id>(); 
        Set<Id> SetRRIds = new Set<Id>(); 
        Set<Id> SetRiIds = new Set<Id>(); 
        map<id, RMA__c> mapRRId = new map<id, RMA__c>();
        Map<id,id> mapRItoPI = new map<id,id>();
        List<RMA__c> lstRMAs = new List<RMA__c>();
        List<RMA__c> lstFinalRMAs = new List<RMA__c>();
        List<RMA_Product__c> lstRIs = new List<RMA_Product__c>();
    
        for(TEX__c tempTEX : lstTEXs)
            {
                SetTEXIds.add(tempTEX.Id);
            }
        lstRMAs = [Select id, TEX__c, Status__c from RMA__c where TEX__c in: SetTEXIds ];
        system.debug('## lstRMAs ##'+ lstRMAs );
        for(RMA__c RR: lstRMAs)
            {
                SetRRIds.add(RR.Id);
                mapRRId.put(RR.Id, RR);
            }
        lstRIs = [Select id, RMA__c, RMA__r.Approver__c,ApprovalStatus__c,ApproverLevel1__c from RMA_Product__c where RMA__c in: mapRRId.KeySet() ];
        system.debug('## lstRIs ##'+ lstRIs );

        for(RMA_Product__c RI: lstRIs)
            {
                SetRiIds.add(RI.Id);
            }

        List<ProcessInstanceWorkitem> lstPIWs = [Select p.ProcessInstance.Status, p.ProcessInstance.TargetObjectId,p.ProcessInstanceId,p.OriginalActorId,p.Id,p.ActorId From ProcessInstanceWorkitem p where p.ProcessInstance.TargetObjectId in: SetRiIds and p.ProcessInstance.Status='Pending']; 
        system.debug('## List of PIWs ##' + lstPIWs);

        for(ProcessInstanceWorkitem PIW: lstPIWs)
            {
                mapRItoPI.put( PIW.ProcessInstance.TargetObjectId, PIW.id);
            }

        for(RMA_Product__c RI: lstRIs)
            {
                if( mapRItoPI.containskey(RI.id))
                    {
                        string ProcessId = mapRItoPI.get(RI.id);
                        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                        //req.setComments('RR MAY2013 RELEASE - Data Migration');
                        req.setAction('Reject');
                        req.setNextApproverIds(new Id[] {RI.ApproverLevel1__c});
                        req.setWorkitemId(ProcessId);
                        // Submit the request for approval
                        Approval.ProcessResult result =  Approval.process(req);
                    }
                RI.ApprovalStatus__c = 'Rejected';
                RI.RMA__r.Status__c='Reject';
                if(!mapRRId.containskey(RI.RMA__c))
                    {
                        lstFinalRMAs.add(mapRRId.get(RI.RMA__c));
                    }     
                //RR.RecordType.DeveloperName='submittedRMA';
            }
        system.debug('## lstRIs2 ##'+ lstRIs );
        system.debug('## lstFinalRMAs ##'+ lstFinalRMAs);
        Update lstRIs;
        Update lstFinalRMAs;
    }
    //=== sukumar code - end
    
    // APRIL 2014 RELEASE: BR-4296, Ram Prasad Chilukuri , 03Feb2014
      public static void updateRILogisticsStatusStatusRunOnTEX(map<string,TEX__c>setTexIds)
    {        List<RMA_Product__c> listUpdRi=new List<RMA_Product__c> ();
            List<RMA_Product__c> listRiIds=[SELECT ID,RMA__c,RMA__r.Status__c,TECH_TEX__c,ReceivedbyReturnCenter__c,LogisticsStatus__c from RMA_Product__c where TECH_TEX__c in:setTexIds.keyset()];
            system.debug('~~~~~~~~listRiIds'+listRiIds);
            for(RMA_Product__c temRI:listRiIds)
                { string flag='No'; 
                    if(temRI.LogisticsStatus__c!=Label.CLAPR14I2P11 && temRI.RMA__r.Status__c=='Validated')
                      {
                      temRI.LogisticsStatus__c=Label.CLAPR14I2P11;
                     flag='Yes'; 
                      
                      }
                            if(temRI.ReceivedbyReturnCenter__c!=setTexIds.get(temRI.TECH_TEX__c).ReceivedbyExpertCenter__c)//BR-9917 the if added as part of Q3 2016 release.
                                {temRI.ReceivedbyReturnCenter__c=setTexIds.get(temRI.TECH_TEX__c).ReceivedbyExpertCenter__c;
                             flag='Yes'; 
                            } 
                            if(flag=='Yes'){
                            listUpdRi.add(temRI);}                    
                }
                system.debug('~~~~~~~~listUpdRi'+listUpdRi);
          update listUpdRi;
    }
    // - APRIL 2014 RELEASE - END
    
    
    // OCT 2015 Release - added by Uttara PR
    
    public static void CaseProblemLink(List<TEX__c> TexList) {
        
        System.debug ('@@ Entered Class method');
        
        List<ProblemCaseLink__c> CPLinks = new List<ProblemCaseLink__c>();
        
        for(Integer i=0; i<TexList.size(); i++) {
            
            ProblemCaseLink__c cp = new ProblemCaseLink__c();
            
            cp.Case__c = TexList[i].Case__c;
            cp.Problem__c = TexList[i].RelatedProblem__c;    
            CPLinks.add(cp);
        }
        
        System.debug(' @@ CP '+CPLinks);
        Database.SaveResult[] CPresults = Database.INSERT(CPLinks,false);
    }
    //OCT 2015 RELEASE : BR-7761, DIVYA M 
     public static void updateLineOfBusinessOnTEX(list<TEX__c> lstTEX){
        set<String> setPL = new set<string>();
        for(TEX__c tx : lstTEX){
            setPL.add(tx.Product_Line__c);
        }
        if(setPL.size()>0){
        list<ProductLineQualityContactMapping__c> listPLIScope = [Select id,AccountableOrganization__c,QualityContact__c,QualityContact__r.id,AccountableOrganization__r.id, AccountableOrganization__r.SubEntity__c, Product__c,ProductLine__c from ProductLineQualityContactMapping__c where ProductLine__c in: setPL];
        map<string,List<ProductLineQualityContactMapping__c>> mapPLIS = new map<string,List<ProductLineQualityContactMapping__c>>();
        for(ProductLineQualityContactMapping__c pl : listPLIScope){
            if(pl.ProductLine__c!=null) {
                if(mapPLIS.containsKey(pl.ProductLine__c)) {
                        mapPLIS.get(pl.ProductLine__c).add(pl);
                }else {
                    mapPLIS.put(pl.ProductLine__c,new  list<ProductLineQualityContactMapping__c>{pl} );
                }
            
            }
            
        }
        for(TEX__c tex : lstTEX){
            system.debug('expert center is --->'+tex.ExpertCenter__c);
           // if(tex.ExpertCenter__c != null){
               if(mapPLIS.containsKey(tex.Product_Line__c)) {
                    for(ProductLineQualityContactMapping__c plis : mapPLIS.get(tex.Product_Line__c)){
                        system.debug('PLIS product line is ==>'+plis.ProductLine__c+'=== TEX product line ====>'+tex.Product_Line__c+'== subentity of org ====>'+plis.AccountableOrganization__r.SubEntity__c+'====');
                        boolean isTexUpdate=false;
                        system.debug('Line of business before entering updated is==>'+tex.LineofBusiness__c);
                        if(plis.ProductLine__c == tex.Product_Line__c){
                            if(plis.AccountableOrganization__r.SubEntity__c != null && plis.AccountableOrganization__r.SubEntity__c != ''){
                                system.debug('Line of business subentity is not null ==>'+plis.AccountableOrganization__r.SubEntity__c+'====');
                                tex.LineofBusiness__c= plis.AccountableOrganization__r.id ;
                                //break;
                                isTexUpdate =true;
                            }
                            else if(plis.AccountableOrganization__r.SubEntity__c == null && plis.AccountableOrganization__r.SubEntity__c == ''){
                                 system.debug('Line of business subentity is null ==>'+plis.AccountableOrganization__r.SubEntity__c+'====');
                                 tex.LineofBusiness__c= null;
                            }
                            if(plis.QualityContact__c != null || plis.QualityContact__c != ''){
                                tex.QualityContact__c = plis.QualityContact__r.id;                  
                            }
                            if(isTexUpdate) {
                                break;
                            }
                        }
                    }
                }
            //}
            if(tex.LastNotificationDay__c==null) {
               tex.LastNotificationDay__c = 0;
           }
        }
        }
     }
     //method calls whenever the Product line on the case is Updated
      public static void updateLineOfBusinessOnTEXfromCase(Map<id,Case> caseIds){
        //*********************************************************************************
         // Created by : Divya M  - Global Delivery Team
         // Date created : 17th June 2015
         // Desc :BR-7761 --> Oct 2015 Release
         ///*********************************************************************************/    
        set<string> setPL = new set<string>();
        for(case cs : caseIds.values()){
            setPL.add(cs.ProductLine__c);

        } 
        if(setPL.size()>0){
        list<TEX__c> lstTEX = [Select id,Case__c,Product_Line__c,LineofBusiness__c,QualityContact__c from TEX__c where Case__c in: caseIds.KeySet() ];
        list<TEX__c> lstTEXtoUpdate = new list<TEX__c>();
        list<ProductLineQualityContactMapping__c> listPLIScope = [Select id,AccountableOrganization__c,QualityContact__c,QualityContact__r.id,AccountableOrganization__r.id, AccountableOrganization__r.SubEntity__c, Product__c,ProductLine__c from ProductLineQualityContactMapping__c where ProductLine__c in:setPL];
        map<string,ProductLineQualityContactMapping__c> mapPLIS = new map<string,ProductLineQualityContactMapping__c>();
        for(ProductLineQualityContactMapping__c pl : listPLIScope){
            mapPLIS.put(pl.ProductLine__c,pl);
        }
        for(Case cs:caseIds.values()){
            for(TEX__c tex:lstTEX){
            string caseid = cs.id;
                if(tex.Case__c == caseid.substring(0,15)){
                    for(ProductLineQualityContactMapping__c plis : mapPLIS.values()){
                        system.debug('PLIS product line is ==>'+plis.ProductLine__c+'=== TEX product line ====>'+tex.Product_Line__c+'== subentity of org ====>'+plis.AccountableOrganization__r.SubEntity__c);
                        system.debug('Line of business before entering updated is==>'+tex.LineofBusiness__c);
                        if(plis.ProductLine__c == tex.Product_Line__c){
                            if(plis.AccountableOrganization__r.SubEntity__c != null && plis.AccountableOrganization__r.SubEntity__c != ''){
                                tex.LineofBusiness__c= plis.AccountableOrganization__r.id ;
                            }else {//if(plis.AccountableOrganization__r.SubEntity__c == null && plis.AccountableOrganization__r.SubEntity__c == ''){
                                 system.debug('Line of business subentity is null ==>'+plis.AccountableOrganization__r.SubEntity__c+'====');
                                 tex.LineofBusiness__c= null ;
                            }if(plis.QualityContact__c != null || plis.QualityContact__c != ''){
                                tex.QualityContact__c = plis.QualityContact__r.id;                  
                            }
                            lstTEXtoUpdate.add(tex);
                        }
                        /*else{
                            tex.LineofBusiness__c= null ;
                            lstTEXtoUpdate.add(tex);
                        }*/
                    }
                }
            }
        }
        if(lstTEXtoUpdate.size()>0){
            update lstTEXtoUpdate;
        }

        }
    }  
     // ENDS BR-7761
    //OCT 2015 RELEASE : BR-7370, DIVYA M 
     public static void insertTEXStakeholder(list<TEX__c> lstTEX,string event){
        Set<id> OrgIds = new set<Id>();
        for(TEX__c tex: lstTEX){
            OrgIds.add(tex.LineofBusiness__c);
        }
        if(OrgIds.size()>0){
            list<EntityStakeholder__c> orgSHLst = new list<EntityStakeholder__c>();
            list<TEXStakeholder__c> TEXSHLst = new List<TEXStakeholder__c>();
            set<string> setemail = new set<string>();
            orgSHLst = [Select id,Role__c,User__c,User__r.email,BusinessRiskEscalationEntity__c from EntityStakeholder__c where (BusinessRiskEscalationEntity__c in: OrgIds AND Role__c =: Label.CLOCT15I2P14)];
            if(orgSHLst.size()>0){
                for(EntityStakeholder__c orgSH : orgSHLst){
                    if(orgSH.Role__c == Label.CLOCT15I2P14){
                        setemail.add(orgSH.User__r.email);
                    }
                }
            }
            if(setemail.size()>0){
                for(TEX__c tex : lstTEX){
                    for(string stremail : setemail){
                        TEXStakeholder__c texSH = new TEXStakeholder__c();
                        texSH.Role__c = Label.CLOCT15I2P14;
                        texSH.Email__c = stremail;
                        texSH.TEX__c = tex.id;
                        texSH.Origin__c = event;
                        if(texSH != null){
                            TEXSHLst.add(texSH);
                            system.debug('TEX__r.Name==>'+tex.Name+'==TEX__r.AffectedOffer__c=='+tex.AffectedOffer__c+'==TEX__r.FrontOfficeOrganization__r.Location__c=='+tex.FrontOfficeOrganization__r.Location__c+'==TEX__r.ExpertCenter__r.Name=='+tex.ExpertCenter__r.Name);
                        }
                    }
                }
            }
            if(TEXSHLst.size()>0){
                insert TEXSHLst;
            }
        }
     }
     //ENds BR-7370 
     //Oct 15 Release -- BR-7820 -- Divya M 
      public static void updateCase(List<TEX__c> ListofTEXs){   
        list<case> lstCases = new list<case>();
         list<case> listcaseRecs = new list<case>();
        set<id> SetCaseIds=new set<id>();
        for (TEX__c temTex:ListofTEXs){
            if(temTex.Case__c!=null && (temTex.Status__c=='Closed'|| temTex.Status__c=='Cancelled' )){
            SetCaseIds.add(temTex.case__c); }
        }
        if(SetCaseIds.size()>0){
            lstCases = [Select id, ActionNeededFrom__c, LevelOfExpertise__c from Case where id in: setCaseIds];
        }
       for(case tempcase : lstCases){
            if(tempcase.LevelOfExpertise__c == 'Primary')
            {
               tempcase.ActionNeededFrom__c = 'Primary';              
               listcaseRecs.add(tempcase);
            }
            else if(tempcase.LevelOfExpertise__c == 'Advanced')
            {
               tempcase.ActionNeededFrom__c   = 'Advanced';              
               listcaseRecs.add(tempcase);
            }
            else if(tempcase.LevelOfExpertise__c == 'Expert')
            {
               tempcase.ActionNeededFrom__c   = 'Expert';              
               listcaseRecs.add(tempcase);
            }
       }
        try{
            if(listcaseRecs.size()>0){
                update listcaseRecs;
            }
        }
        catch(Exception e)
        {
        System.debug('Try Catch for Case updating'+e);
        }
   }
   //ends BR-7820
   public static void retrunRequestCheck(map<string,TEX__c> mapTEX )
    {// Start BR-9917 the method added as part of Q3 2016 release.
        
        Set<Id> SetTEXIds = new Set<Id>(); 
        Set<Id> SetRiIds = new Set<Id>(); 
        List<RMA__c> lstRMAs = new List<RMA__c>();
        
        lstRMAs = [Select id, TEX__c, Status__c from RMA__c where TEX__c in: mapTEX.keyset() AND Status__c=:system.label.CLOCT16I2P01 ];
        system.debug('## lstRMAs ##'+ lstRMAs );
        for(RMA__c RR: lstRMAs)
            {
                SetTEXIds.add(RR.TEX__c );               
            }
            system.debug('## SetTEXIds ##'+ SetTEXIds );
            system.debug('## mapTEX ##'+ mapTEX );
        for(TEX__c tmpTEX: mapTEX.values())
            {
                if(!SetTEXIds.contains(tmpTEX.id)){
                    tmpTEX.addERROR(Label.CLOCT16I2P02);
                }
            }
    }// Start BR-9917  of the method

}