/*******
Comments :TEST CLASS Q3 2016
********/

    @isTest
    Public Class AP_ELLAOffsharing_TEST {



    static Testmethod  void Test_AP_ELLAOffsharing () { 
    
        User newUser = Utils_TestMethods.createStandardUser('ELLAMQ1');  
        User newUser1 = Utils_TestMethods.createStandardUser('ELLAMQ1Share');  
        
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        
         Database.SaveResult UserInsertResult1 = Database.insert(newUser1, false);
         
          User newUser11 = Utils_TestMethods.createStandardUser('yhrgiusd');
            Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
            newUser11.BypassVR__c = TRUE;

            newUser11.UserRoleID = rol_ID ;
            newUser11.BypassTriggers__c='AP_Offer1;AP_Offer3';    
            Database.SaveResult UserInsertResult12 = Database.insert(newUser11, false);  
         system.runas(newUser11){ 
        Offer_Lifecycle__c offerRec = createOffer(newUser.Id);
                
            offerRec.Description__c='tetstsh8';
        insert offerRec;
        
        Announcement__c  intestACC= new Announcement__c();
        intestACC.Offer_Lifecycle__c=offerRec.id;
        intestACC.name='testt888';
        insert intestACC;
        
        offerRec.Launch_Owner__c=newUser11.id;
        offerRec.Withdrawal_Owner__c=newUser11.id;
        offerRec.End_of_Services_Date__c=system.today();
        offerRec.BoxUserEmailId__c='test898090@schneider-electric.com';
        offerRec.Offer_Classification__c='Strategic'; 
        offerRec.Description__c='tetstsh8uuhj';
        update offerRec;
          country__c c = new country__c(name = 'TestCountryHP',countrycode__c = 'ZZp',Region__c='APAC',SubRegion__c ='China - zone');
        insert c;
        Milestone1_Project__c projRec =createProject(offerRec.Id, c.Id, newUser.Id);
        
          
        insert projRec;
        
        
        
        
        Map<id,set<Id>> mapUserRecords = new Map<id,set<Id>>();
        
        Map<string,Set<string>> MapParentAccId = new Map<string,Set<string>>(); 
        
        mapUserRecords.put(projRec.id,new Set<id>{newUser1.id});
        //MapParentAccId.put(projRec.id,new Set<string>{newUser1.id});
      
        AP_ELLAOffsharing.ShareObjetReadW(mapUserRecords,label.CLMAR16ELLAbFO04);
        AP_ELLAOffsharing.RemoveAccesOnObject(MapParentAccId,label.CLMAR16ELLAbFO04);
        
         }    
    
    
    }
    
    public static Offer_Lifecycle__c createOffer(Id userId){
        Offer_Lifecycle__c offer = new Offer_Lifecycle__c(
            Name = 'TestOffer', 
            Offer_Name__c = 'Offet Test',
            Leading_Business_BU__c = 'Cloud Services',
            Description__c = 'Offer Description',
            Sales_Date__c = system.today(),
            Process_Type__c = 'PMP',
            Launch_Owner__c = userId,
            Forecast_Unit__c = 'Kilo Euro',
            Marketing_Director__c = userId,
            Withdrawal_Owner__c=userId,
            Offer_Status__c = 'Red',
            Launch_Master_Assets_Status__c = 'Green',
            Offer_Classification__c = 'Major'
            
        );
        //insert offer;
        return offer;
    }
    
    public static Milestone1_Project__c createProject(Id OfferId, Id cId, Id userId){
        Milestone1_Project__c pjct = new Milestone1_Project__c(
            name='Test Project1',
            offer_launch__c = OfferId,
            country__c=cId,
            Country_Launch_Leader__c=userId,
            Country_Announcement_Date__c=system.today(),
            region__c='APAC'
        );
        //insert pjct;
        return pjct;
    }
    
    static Testmethod  void Test_AP_ELLAOffsharing_1 () { 
        User newUser = Utils_TestMethods.createStandardUser('ELLAo12');  
        User newUser1 = Utils_TestMethods.createStandardUser('ELLAMQ1Share');  
        
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        
         Database.SaveResult UserInsertResult1 = Database.insert(newUser1, false);
         
          User newUser11 = Utils_TestMethods.createStandardUser('yhh7uyu');
            Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
            newUser11.BypassVR__c = TRUE;

            newUser11.UserRoleID = rol_ID ;
            newUser11.BypassTriggers__c='AP_Offer1;AP_Offer3';    
            Database.SaveResult UserInsertResult12 = Database.insert(newUser11, false);  
            
            
            
          User newUser22 = Utils_TestMethods.createStandardUser('yhh2uyu');
            
            newUser22.BypassVR__c = TRUE;

            newUser22.UserRoleID = rol_ID ;
              
            Database.SaveResult UserInsertResult122 = Database.insert(newUser22, false);  
         system.runas(newUser11){ 
         try {
        Offer_Lifecycle__c offerRec = createOffer(newUser.Id);
        offerRec.Withdrawal_Owner__c=newUser22.id;
        insert offerRec;
        
        offerRec.Launch_Owner__c=newUser11.id;
        offerRec.Withdrawal_Owner__c=newUser11.id;
        offerRec.End_of_Services_Date__c=system.today();
        offerRec.BoxUserEmailId__c='test898090@schneider-electric.com';
        offerRec.Offer_Classification__c='Major'; 
        update offerRec;
          country__c c = new country__c(name = 'TestCountryHP',countrycode__c = 'ZZp',Region__c='APAC',SubRegion__c ='China - zone');
        insert c;
        Milestone1_Project__c projRec =createProject(offerRec.Id, c.Id, newUser.Id);
        
       projRec.Country_Launch_Leader__c=newUser11.id; 
        insert projRec;
        
        
        
        
        
        
         } catch(Exception expt ) {
             
         }
      
        
         }    
    
    
    }
     static Testmethod  void Test_AP_ELLAOffsharing_2 () { 
        User newUser = Utils_TestMethods.createStandardUser('ELLAo12');  
        User newUser1 = Utils_TestMethods.createStandardUser('ELLAMQ1Share');  
        
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        
         Database.SaveResult UserInsertResult1 = Database.insert(newUser1, false);
         
          User newUser11 = Utils_TestMethods.createStandardUser('yhh7uyu');
            Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
            newUser11.BypassVR__c = TRUE;

            newUser11.UserRoleID = rol_ID ;
            newUser11.BypassTriggers__c='AP_Offer1;AP_Offer3';    
            Database.SaveResult UserInsertResult12 = Database.insert(newUser11, false);            
            
            
          User newUser22 = Utils_TestMethods.createStandardUser('yhh2uyu');
            
            newUser22.BypassVR__c = TRUE;

            newUser22.UserRoleID = rol_ID ;
              
            Database.SaveResult UserInsertResult122 = Database.insert(newUser22, false);  
         system.runas(newUser11){ 
         try {
             
        
        Offer_Lifecycle__c offerRec = createOffer(newUser.Id);
        offerRec.Withdrawal_Owner__c=newUser22.id;
        insert offerRec;
        
       
          country__c c = new country__c(name = 'TestCountryHP',countrycode__c = 'ZZp',Region__c='APAC',SubRegion__c ='China - zone');
        insert c;
        Test.startTest(); 
         Milestone1_Project__c pjct4 = new Milestone1_Project__c(
            name='Test Project178',
            offer_launch__c = offerRec.Id,
            country__c= c.Id,
            RegionWithdrawalManager__c=newUser.Id,
            Country_Announcement_Date__c=system.today(),
            region__c='APAC'
        );
        
        insert pjct4;
        pjct4.RegionWithdrawalManager__c =newUser22.id;
        update  pjct4;
         Test.stopTest(); 
        
         
        
        
        
         } catch(Exception expt ) {
             
         }
      
        
         }    
    
    
    }
    
    
     static Testmethod  void Test_AP_ELLAOffsharing_3 () { 
        User newUser = Utils_TestMethods.createStandardUser('ELLAo12');  
        User newUser1 = Utils_TestMethods.createStandardUser('ELLAMQ1Share');  
        
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        
         Database.SaveResult UserInsertResult1 = Database.insert(newUser1, false);
         
          User newUser11 = Utils_TestMethods.createStandardUser('yhh7uyu');
            Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
            newUser11.BypassVR__c = TRUE;

            newUser11.UserRoleID = rol_ID ;
            newUser11.BypassTriggers__c='AP_Offer1;AP_Offer3';    
            Database.SaveResult UserInsertResult12 = Database.insert(newUser11, false);            
            
            
          User newUser22 = Utils_TestMethods.createStandardUser('yhh2uyu');
            
            newUser22.BypassVR__c = TRUE;

            newUser22.UserRoleID = rol_ID ;
              
            Database.SaveResult UserInsertResult122 = Database.insert(newUser22, false);  
         system.runas(newUser11){ 
         try {
             
        
        Offer_Lifecycle__c offerRec = createOffer(newUser.Id);
        offerRec.Withdrawal_Owner__c=newUser22.id;
        insert offerRec;
        
       
          country__c c = new country__c(name = 'TestCountryHP',countrycode__c = 'ZZp',Region__c='APAC',SubRegion__c ='China - zone');
        insert c;
        Test.startTest(); 
         
         Milestone1_Project__c pjct3 = new Milestone1_Project__c(
            name='Test Project19OII8',
            offer_launch__c = offerRec.Id,
            country__c= c.Id,
            Region_Introduction_Owner__c=newUser.Id,
            Country_Announcement_Date__c=system.today(),
            region__c='APAC'
        );
        
        insert pjct3;
        pjct3.Region_Introduction_Owner__c =newUser22.id;
        update pjct3;
         Test.stopTest(); 
        
         
        
        
        
         } catch(Exception expt ) {
             
         }
      
        
         }    
    
    
    }
    
     static Testmethod  void Test_AP_ELLAOffsharing_4 () { 
        User newUser = Utils_TestMethods.createStandardUser('ELLAo12');  
        User newUser1 = Utils_TestMethods.createStandardUser('ELLAMQ1Share');  
        
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        
         Database.SaveResult UserInsertResult1 = Database.insert(newUser1, false);
         
          User newUser11 = Utils_TestMethods.createStandardUser('yhh7uyu');
            Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
            newUser11.BypassVR__c = TRUE;

            newUser11.UserRoleID = rol_ID ;
            newUser11.BypassTriggers__c='AP_Offer1;AP_Offer3';    
            Database.SaveResult UserInsertResult12 = Database.insert(newUser11, false);            
            
            
          User newUser22 = Utils_TestMethods.createStandardUser('yhh2uyu');
            
            newUser22.BypassVR__c = TRUE;

            newUser22.UserRoleID = rol_ID ;
              
            Database.SaveResult UserInsertResult122 = Database.insert(newUser22, false);  
         system.runas(newUser11){ 
         try {
             
        
        Offer_Lifecycle__c offerRec = createOffer(newUser.Id);
        offerRec.Withdrawal_Owner__c=newUser22.id;
        insert offerRec;
        
       
          country__c c = new country__c(name = 'TestCountryHP',countrycode__c = 'ZZp',Region__c='APAC',SubRegion__c ='China - zone');
        insert c;
        Test.startTest(); 
         
         Milestone1_Project__c pjct3 = new Milestone1_Project__c(
            name='Test Project19OII8',
            offer_launch__c = offerRec.Id,
            country__c= c.Id,
            RegionWithdrawalManager__c=newUser.Id,
            Country_Announcement_Date__c=system.today(),
            region__c='APAC'
        );
         
        insert pjct3;
        pjct3.RegionWithdrawalManager__c =newUser22.id;
        update pjct3;
         Test.stopTest(); 
        
         
        
        
        
         } catch(Exception expt ) {
             
         }
      
        
         }    
    
    
    }
    
    



}