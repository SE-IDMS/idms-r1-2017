/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class used for encryption and decryption process for SAML assertion in UIMS
**/ 
public class IDMSEncryption{
    //This method encrypts token with type AES128.
    public string encryptedToken1(string fedId){
        Blob key                = Crypto.generateAesKey(128);
        String csvBody          = EncodingUtil.base64Encode(key);
        blob key2               = EncodingUtil.base64Decode(csvBody);
        Blob data               = Blob.valueOf(fedId+';'+datetime.now()+';'+'1');
        Blob encrypted          = Crypto.encryptWithManagedIV('AES128', key, data);
        Blob decrypted          = Crypto.decryptWithManagedIV('AES128', key2, encrypted);
        String decryptedString  = decrypted.toString();
        
        system.debug('decrypted:'+decryptedstring);
        return EncodingUtil.base64Encode(encrypted);
    }
    
    //This method encrypts and signs with certificate.
    public void encryptedTokenNew(){
        Blob data           = Blob.valueOf('123-123;'+datetime.now()+';'+'1');
        blob encrypted      = System.Crypto.signWithCertificate('RSA-SHA1', data, 'bFO_IDMS_to_UIMS');
        system.debug(EncodingUtil.base64Encode(encrypted));
    }
    
    //This method encrypts the signed certificate.
    public string ecryptedToken(String fedId,string vnew){
        Blob data       = Blob.valueOf(fedId+';'+datetime.now()+';'+vnew);
        string certName = label.CLQ316IDMS132;
        blob encrypted  = System.Crypto.signWithCertificate(label.CLQ316IDMS133, data,certName);
        return fedId+';'+datetime.now()+';'+vnew+';'+EncodingUtil.base64Encode(encrypted);
    }
    
}