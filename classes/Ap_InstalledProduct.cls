public class Ap_InstalledProduct extends TriggerBase{

    
    private map<Id, SVMXC__Installed_Product__c> newMap;
    private map<Id, SVMXC__Installed_Product__c> oldMap;
    private List<SVMXC__Installed_Product__c> newRecList;
    Map<id,Id> ipidlocidMap = new Map<id,Id>();
    Set<String> ExistGoldenAssetSet = new Set<String>();
    
    public Ap_InstalledProduct(boolean isBefore, boolean isAfter, boolean isUpdate, boolean isInsert, boolean isDelete,
        List<SVMXC__Installed_Product__c> newList,
        map<Id, SVMXC__Installed_Product__c > oldMap, map<Id, SVMXC__Installed_Product__c> newMap) {
        super(isBefore, isAfter, isUpdate, isInsert, isDelete);

        this.newMap = newMap;
        this.oldMap = oldMap;
        this.newRecList = newList;
        
    }
    
    public Ap_InstalledProduct() {
    }
    public override void onBeforeInsert() {
        
        Set<String> GoldenAssetSet = new Set<String>();        
        
        for(SVMXC__Installed_Product__c ipObj :newRecList){
            
            if(ipObj.GoldenAssetId__c != null)
            GoldenAssetSet.add(ipObj.GoldenAssetId__c);         
        }
        if(GoldenAssetSet != null && GoldenAssetSet.size()>0)
        {
            for(SVMXC__Installed_Product__c obj:[Select id,GoldenAssetId__c from SVMXC__Installed_Product__c where GoldenAssetId__c in :GoldenAssetSet]){
                ExistGoldenAssetSet.add(obj.GoldenAssetId__c);
            }
        }
          //if(Utils_SDF_Methodology.canTrigger('SDHGoldenAssetId'))
        sdhGoldenUniqeCheck();
        
        
        
    }
    public  void sdhGoldenUniqeCheck() {
        for(SVMXC__Installed_Product__c ipObj :newRecList){
            
            if(ipObj.GoldenAssetId__c != null)
            {
                if(ExistGoldenAssetSet.contains(ipObj.GoldenAssetId__c))
                ipObj.addError('Golden Asset already Exist');
                
            }
        }
        
    }
     public override void onBeforeUpdate() {
         sdhGoldenUniqeCheck();
     }
    public override void onAfterUpdate() {
        
         for(SVMXC__Installed_Product__c ipObj :newRecList){
            
            if(ipObj.SVMXC__Site__c != null)
            {
                if(oldMap.containsKey(ipObj.id))
                {                  
                    if(oldMap.get(ipObj.id).SVMXC__Site__c != null){
                        
                        if(ipObj.SVMXC__Site__c != oldMap.get(ipObj.id).SVMXC__Site__c)
                        ipidlocidMap.put(ipObj.id,ipObj.SVMXC__Site__c );
                        
                    }
                    
                    
                }
            }
                    
         }
        updateAllCoveredProductLocation();
        
        
        
    }
    public  void updateAllCoveredProductLocation() {
         if(ipidlocidMap != null && ipidlocidMap.size()>0){
         
             List<SVMXC__Service_Contract_Products__c> cpordList =[select id, AssetLocation__c ,SVMXC__Installed_Product__c  from SVMXC__Service_Contract_Products__c where SVMXC__Installed_Product__c  in :ipidlocidMap.keySet()];    
             if(cpordList  != null && cpordList.size()>0)
             {
                 for(SVMXC__Service_Contract_Products__c cpord : cpordList )
                 {
                     cpord.AssetLocation__c =  ipidlocidMap.get(cpord.SVMXC__Installed_Product__c);
                 }
                 update cpordList ;
             }             
             
         }
     }



}