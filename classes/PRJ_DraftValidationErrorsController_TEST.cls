/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 04/24/2012
    Description     : Test class for the class PRJ_DraftValidationErrorsController
*/


@isTest
private class PRJ_DraftValidationErrorsController_TEST
{
    static testMethod void DraftValidationErrorControllerUnitTest()
    {
        //User runAsUser = Utils_TestMethods.createStandardDMTUser('tst2');
        
        //insert runAsUser;
        //System.runAs(runAsUser)
        //{
        
        // Modified By Ramakrishna For Sep Release
        
             DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
            insert testDMTA1; 
            
            PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.Description__c = 'Test';
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            testProj.BusinessTechnicalDomain__c = 'Supply Chain';
            insert testProj;
            
            
            PRJ_DraftValidationErrorsController pagePR = new PRJ_DraftValidationErrorsController(new ApexPages.StandardController(testProj));
            pagePR.updateProject();
                    
            DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
            insert testDMTA2;  
            
            testProj.NextStep__c = 'Created';
            Budget__c Budget = new Budget__c(ProjectReq__c =testProj.id,Active__c=true);
            Insert Budget ;
            update testProj;
            
            System.Debug('Status : ' + testProj.NextStep__c);
            System.debug('Record Type ID : ' + testProj.RecordTypeId);
            System.debug('Record Type : ' + testProj.RecordType.Name);
            pagePR.updateProject();
            
            DMTAuthorizationMasterData__c testDMTA3 = new DMTAuthorizationMasterData__c();
            testDMTA3.AuthorizedUser5__c = UserInfo.getUserId();
            testDMTA3.AuthorizedUser6__c = UserInfo.getUserId();
            testDMTA3.NextStep__c = 'quoted';
            testDMTA3.Parent__c = testProj.Parent__c;
            testDMTA3.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
            testDMTA3.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
            insert testDMTA3;  
            
            
            testProj.NextStep__c = 'Valid';
            testProj.ExpectedStartDate__c = system.today();
            testProj.GoLiveTargetDate__c = system.today();
            testProj.AppServicesDecision__c = 'Yes';
            testProj.AppGlobalOpservdecisiondate__c = system.today();
            update testProj;  
            
            test.startTest();          
            pagePR.updateProject();
            
            DMTAuthorizationMasterData__c testDMTA5 = new DMTAuthorizationMasterData__c();
            testDMTA5.AuthorizedUser1__c = UserInfo.getUserId();
            testDMTA5.AuthorizedUser5__c = UserInfo.getUserId();
            testDMTA5.AuthorizedUser6__c = UserInfo.getUserId();
            testDMTA5.NextStep__c = 'Fundings Authorized';
            testDMTA5.Parent__c = testProj.Parent__c;
            testDMTA5.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
            testDMTA5.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
            insert testDMTA5; 
            
            testProj.NextStep__c = 'Quoted';
            testProj.DecisionSecurityResponsible__c = 'Yes';
            testProj.DecisionSecurityResponsibleDate__c = system.today();
            testProj.DecisionControllerProposalValidation__c = 'Yes';
            testProj.DecisionProposalValidationDate__c = system.today();
            testProj.ScoreResultCalculated__c = '1 (Nice to have)';
            update testProj;    
            
            DMTFundingEntity__c dmtfund = new DMTFundingEntity__c();
            dmtfund.ProjectReq__c = testProj.id;
            dmtfund.ParentFamily__c = 'Business';
            dmtfund.Parent__c = 'Buildings';
            dmtfund.GeographicZoneSE__c = 'Global';
            dmtfund.BudgetYear__c = '2012';
            dmtfund.ForecastQuarter__c = 'Q1';
            dmtfund.ForecastYear__c = '2011';
            dmtfund.ActiveForecast__c = true;
            dmtfund.HFMCode__c = '100';
            dmtfund.CAPEXY0__c = 100;
            dmtfund.SelectedinEnvelop__c = true;
            insert dmtfund;
            
            pagePR.updateProject();
            
            testProj.NextStep__c = 'Project Open';
            testProj.AuthorizeFunding__c = 'Yes';
            testProj.ProjectClosureDate__c = system.today();
            testProj.EffectiveGoLiveDate__c = system.today();
            update testProj;            
            pagePR.updateProject();
            testProj.NextStep__c = 'Finished';
            testProj.AuthorizeFunding__c = 'Yes';
            testProj.BCINumber__c = '100';
            update testProj;            
            pagePR.updateProject();
             
            
                                    
        //}
        test.stopTest();
                            
    }
    static testMethod void DraftValidationErrorControllerUnitTest2()
    {
        User runAsUser = Utils_TestMethods.createStandardDMTUser('tst2');
        
        insert runAsUser;
        System.runAs(runAsUser)
        {
            DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
            insert testDMTA1; 
        
        // Modified By Ramakrishna For Sep Release
            PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.Description__c = 'Test';
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            testProj.BusinessTechnicalDomain__c = 'Supply Chain';
            insert testProj;
            
            PRJ_DraftValidationErrorsController pagePR = new PRJ_DraftValidationErrorsController(new ApexPages.StandardController(testProj));
            pagePR.updateProject();
            
            DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
            insert testDMTA2;  
            
            testProj.NextStep__c = 'Created';
            Budget__c Budget = new Budget__c(ProjectReq__c =testProj.id,Active__c=true);
            Insert Budget ;
            update testProj;
            
            System.Debug('Status : ' + testProj.NextStep__c);
            System.debug('Record Type ID : ' + testProj.RecordTypeId);
            System.debug('Record Type : ' + testProj.RecordType.Name);
            
            pagePR.updateProject();
            
            DMTAuthorizationMasterData__c testDMTA4 = new DMTAuthorizationMasterData__c();
            testDMTA4.AuthorizedUser5__c = UserInfo.getUserId();
            testDMTA4.AuthorizedUser6__c = UserInfo.getUserId();
            testDMTA4.NextStep__c = 'quoted';
            testDMTA4.Parent__c = testProj.Parent__c;
            testDMTA4.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
            testDMTA4.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
            insert testDMTA4;  
            
            testProj.NextStep__c = 'Valid';
            testProj.ExpectedStartDate__c = system.today();
            testProj.GoLiveTargetDate__c = system.today();
            testProj.AppServicesDecision__c = 'Yes';
            testProj.AppGlobalOpservdecisiondate__c = system.today();
            update testProj;  
            
            test.startTest();         
            pagePR.updateProject();
            
            DMTAuthorizationMasterData__c testDMTA5 = new DMTAuthorizationMasterData__c();
            testDMTA5.AuthorizedUser1__c = UserInfo.getUserId();
            testDMTA5.AuthorizedUser5__c = UserInfo.getUserId();
            testDMTA5.AuthorizedUser6__c = UserInfo.getUserId();
            testDMTA5.NextStep__c = 'Fundings Authorized';
            testDMTA5.Parent__c = testProj.Parent__c;
            testDMTA5.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
            testDMTA5.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
            insert testDMTA5; 
            
            testProj.NextStep__c = 'Quoted';
            testProj.DecisionSecurityResponsible__c = 'Yes';
            testProj.DecisionSecurityResponsibleDate__c = system.today();
            testProj.DecisionControllerProposalValidation__c = 'Yes';
            testProj.DecisionProposalValidationDate__c = system.today();
            testProj.ScoreResultCalculated__c = '1 (Nice to have)';
            update testProj;            
            DMTFundingEntity__c dmtfund = new DMTFundingEntity__c();
            dmtfund.ProjectReq__c = testProj.id;
            dmtfund.ParentFamily__c = 'Business';
            dmtfund.Parent__c = 'Buildings';
            dmtfund.GeographicZoneSE__c = 'Global';
            dmtfund.BudgetYear__c = '2012';
            dmtfund.ForecastQuarter__c = 'Q1';
            dmtfund.ForecastYear__c = '2011';
            dmtfund.ActiveForecast__c = true;
            dmtfund.HFMCode__c = '100';
            dmtfund.CAPEXY0__c = 100;
            dmtfund.SelectedinEnvelop__c = true;
            insert dmtfund;
            pagePR.updateProject();
            
            testProj.NextStep__c = 'Project Open';
            testProj.AuthorizeFunding__c = 'Yes';
            testProj.ProjectClosureDate__c = system.today();
            testProj.EffectiveGoLiveDate__c = system.today();
            update testProj;            
            pagePR.updateProject();
            testProj.NextStep__c = 'Finished';
            testProj.AuthorizeFunding__c = 'Yes';
            testProj.BCINumber__c = '100';
            update testProj;            
            pagePR.updateProject();
             
            
                                    
        }
        test.stopTest();
                            
    }
}