/*
Author: Sid (bFO Solution) & Polyspot team
description: this class is a controller class for the page VFP_DK_ContactSeach Page , this controller provides a way to make
a webservice callout to polyspot engine and it will return back with the json response.*/

/*
https://cs22.salesforce.com/01p17000000DA6k
*/
global class VFC_L2G_AccountContact360 {
    public static final String ACCOUNT_RETRIEVED_FIELDS = 'LinkedCommodity__c,TECH_Downloadable__pc,GeoLocation__pc,TickerSymbol,CorrespLang__pc,EmailAutoResponse__c,DoNotCont__pc,AccountSupportLevel__c,DoNotMail__pc,AccountLocalName__c,Comments__c,LocalAdditionalAddress__c,Authority__pc,StateProv__pc,MarcomPrefPostalMail__pc,BillingStreet,SynchronizeWithMarketo__pc,CallFlowOption__pc,BuildingsManagementDomain__pc,PersonAssistantName,SpecialConditions__c,AccountStatus__c,Tech_CountryCode__pc,Pagent__pc,LeadDoNotFollowUp__pc,ShippingLongitude,Rating,NaicsDesc,SVMXC__Access_Hours__c,CreatedById,WorkFaxExt__pc,PersonAssistantPhone,CallFlowOption__c,mkto_si__Last_Interesting_Moment_Source__pc,AssociatedBrand__pc,TECH_ActivePRMPrograms__c,AccountSource,Account_VIPFlag__c,Tradestyle,AutomaticGeoLocation__c,IsCustomerPortal,OwnerId,ReasonForDeletion__c,TargetedBy__c,HowHeard__pc,BillingState,ExportCtrlCompliance__c,SystemModstamp,JobDescr__pc,DoNotSur__pc,PersonMailingLatitude,TECH_IsSVMXRecordPresentForContact__pc,PersonLastCURequestDate,LocalMidInit__pc,AccType__c,LastReferencedDate,PostalCounty__c,POBoxZip__c,Preferred_CC_Team__c,MarcomPrefSurvey__pc,SVMXC__Longitude__c,Home__pc,mkto2__Inferred_State_Region__pc,CommPref__pc,StateProvince__c,TECH_AccountOwner__c,MarcomPrefFax__pc,ContactSupportLevel__pc,ProblemtoPreventionDeployed__c,PRMContact__pc,mkto2__Inferred_Postal_Code__pc,CreatedDate,AdditionalAddress__c,LocalFirstName__pc,JigsawCompanyId,UpdateAccountAddress__pc,Sic,SupplierMarket__c,PersonDoNotCall,AccountRelationshipStatus__c,mkto2__Lead_Score__pc,RecordTypeId,LocalCity__pc,TimeZone__pc,DUNS__c,NbrofAccountPlans__c,ClassLevel2__c,PersonHasOptedOutOfEmail,mkto_si__Last_Interesting_Moment_Desc__pc,AccLifecycle__c,GlobUltiAcc__c,mkto_si__HideDate__pc,BusinessName__c,UltiParentAcc__c,ScopeOfOwnership__c,smagicbasic__SMSOptOut__pc,TECH_RequiresAUR__c,NaicsCode,NbrofActiveAccountPlans__c,MarcomPrefText__pc,Site,PS_OLH__Mail_ID__pc,PS_OLH__Chat_ID__c,WorkPhone__pc,AccountNumber,GeoLocation__Latitude__ps,mkto_si__Last_Interesting_Moment_Type__pc,Country__c,SVMXC__Business_Hours__c,FieloEE__Number_of_Contacts__c,mkto2__Original_Source_Type__pc,LastViewedDate,LeadStatus__pc,ApplicationOfSETechnology__c,PersonMailingStreet,mkto_si__Last_Interesting_Moment_Date__pc,FaxNational__pc,IsPersonAccount,MarketSegment__c,Industry,Phone,mkto2__Acquisition_Program_Id__pc,PersonContactId,SEAccountID__c,PersonMobilePhone,RelationshipSuiteLastUpdated__c,LocalCounty__pc,FaxInternational__pc,BillingLatitude,Timeframe__pc,PersonOtherLongitude,Sic4code__c,LastActivityDate,ServiceContractType__pc,IVRMenu__c,ExistingLeadOwner__pc,County__pc,PreferredCCTeam__pc,LastValidatedAddressInformation__pc,Street__c,PRMPrimaryContact__pc,mkto2__Inferred_Phone_Area_Code__pc,Tech_OwnedbyPartnerUser__c,PersonMailingCountry,Description,LastModifiedById,HasBudget__pc,PersonMailingState,MarketServed__c,SDHGoldenVersion__pc,PS_OLH__Mail_ID__c,SE_Territory__c,mkto_si__Relative_Score_Value__pc,SEContactID__pc,DuplicateWith__c,DomUltimAcc__c,GeoLocation__Latitude__s,Ownership,PersonEmail,ParentId,StrategicThemes__pc,mkto2__Inferred_Country__pc,LastModifiedDate,PS_OLH__Chat_ID__pc,PhoneNational__c,LastName,PersonDepartment,NumberOfEmployees,FaxInternational__c,PersonHasOptedOutOfFax,DandbCompanyId,PostalStreet__c,TimeZone__c,OperationsEnterpriseManagement__pc,ShippingCity,UltimateBusinessName__c,TECH_IsSVMXRecordPresent__c,PersonTitle,GeoLocation__c,Jigsaw,IsLocked,ShippingCountry,PostalAdditionalInformation__c,DBID__c,PRMAccount__c,SVMXC__Preferred_Technician__c,Street__pc,Salutation,Synchronize_With_Marketo__c,LeadingBusiness__c,MarcomPrefEmail__pc,LocalLastName__pc,PersonOtherStreet,BillingCity,PersonOtherPostalCode,Aphone__pc,IsDeleted,VenusID__c,JobFunc__pc,AutomaticGeoLocation__Latitude__s,PostalCity__c,PBAAgent__c,DunsNumber,Need__pc,PreferredDistributors__c,BigMachines__Partner_Organization__c,LastValidatedContactCommunication__pc,ToBeDeleted__pc,POBox__pc,Pagent__c,AnnualRevenue,PersonOtherPhone,LastEngagementDate__pc,DemographicScore__pc,BillingPostalCode,PersonEmailBouncedDate,FirstName,NbrofPlatformingScoring__c,PersonLeadSource,LocalCounty__c,PersonMailingPostalCode,DuplicateWith__pc,ShippingStreet,mkto2__Original_Source_Info__pc,WorkPhoneExt__pc,YearStarted,FieloEE__Member__pc,SourceDetail__pc,mkto2__Inferred_Company__pc,AdditionalAddress__pc,MasterRecordId,ZipCode__pc,PersonHomePhone,POBoxZip__pc,ClassLevel1__c,mkto2__Inferred_City__pc,MayEdit,FaxNational__c,StreetLocalLang__pc,County__c,PhoneInternational__c,CorporateHeadquarters__c,PersonLastCUUpdateDate,SicDesc,LocalAdditionalAddress__pc,EmployeeSize__c,SecurityManagementDomain__pc,VATNumber__c,JobTitle__pc,PersonMailingCity,PersonOtherState,MarketSubSegment__c,IVRMenu__pc,MidInit__pc,PrimaryRelationshipLeader__c,AnnualSales__c,PostalCountry__c,ShippingLatitude,PRMUser__pc,UnsubscribeAll__pc,BillingLongitude,BillingCountry,DBLegalName__c,CurrencyIsoCode,SDHGoldenVersion__c,PreferredName__pc,SVMXC__Latitude__c,InActive__pc,AccountQualifiedForPlatformingCAP__c,City__c,AEmail__pc,ProductLines__pc,REP_PilotSource__pc,PBAAgent__pc,TECH_CountryPriorValue__c,PersonMailingLongitude,LocalCity__c,LastValidatedContactDetail__pc,ProcessMachinesManagementDomain__pc,RelationshipLeaders__c,Id,POBox__c,ShippingPostalCode,StreetLocalLang__c,PersonOtherCity,IsPartner,MarcomPrefPhone__pc,AutomaticGeoLocation__Longitude__s,PersonEmailBouncedReason,AssociatedRelationshipSuite__c,GeoLocation__Longitude__s,Type,HQParentAcc__c,ShippingState,ToBeDeleted__c,ReasonForDeletion__pc,PersonOtherCountry,AddressAddInfor__pc,AccShortName__c,mkto2__Original_Search_Engine__pc,TECH_NumberOfOpenLeads__pc,GeoLocation__Longitude__ps,mkto_si__Urgency_Value__pc,Tech_CountryCode__c,BehavioralScore__pc,PowermanagementDomain__pc,Country__pc,Website,SubStatus__pc,Comments__pc,City__pc,Fax,mkto2__Inferred_Metropolitan_Area__pc,PersonBirthdate,mkto_si__Priority__pc,mkto2__Acquisition_Program__pc,mkto2__Original_Search_Phrase__pc,OtherreasonHowHeard__pc,ZipCode__c,UltimateDuns__c,LastValidatedMarketingCommunication__pc,ServiceContractType__c,Name,PersonOtherLatitude,mkto2__Original_Referrer__pc,mkto2__Acquisition_Date__pc,PostalStateProvince__c,ITRoomManagementDomain__pc,TECH_OpenOpportunityCount__c, Owner.Name,Country__r.Name,CreatedBy.Name,LastModifiedBy.Name,Pagent__r.Name,PBAAgent__r.Name,TECH_AccountOwner__r.Name';
    public static final String CONTACT_RETRIEVED_FIELDS = 'AEmail__c,mkto2__Inferred_Postal_Code__c,InActive__c,BehavioralScore__c,PRMPrimaryContact__c,LastModifiedDate,WorkPhone__c,TECH_IsSVMXRecordPresentForContact__c,LeadSource,SubStatus__c,LastName,OtherPostalCode,smagicbasic__SMSOptOut__c,HomePhone,MarcomPrefPhone__c,FaxInternational__c,DoNotCall,OtherLongitude,AssociatedBrand__c,TimeZone__c,mkto2__Original_Source_Info__c,ContactSupportLevel__c,mkto_si__Urgency_Value__c,LocalAdditionalAddress__c,Comments__c,JobTitle__c,MobilePhone,mkto2__Original_Source_Type__c,GeoLocation__c,MailingLongitude,IsLocked,Jigsaw,MarcomPrefText__c,MidInit__c,PRMUser__c,Salutation,HasOptedOutOfEmail,LastCUUpdateDate,mkto2__Acquisition_Program_Id__c,MarcomPrefFax__c,MailingLatitude,Email,Timeframe__c,LastEngagementDate__c,CreatedById,OtherState,IsDeleted,CallFlowOption__c,PBAAgent__c,EmailBouncedDate,Need__c,mkto2__Inferred_Phone_Area_Code__c,mkto_si__Last_Interesting_Moment_Date__c,OwnerId,ReasonForDeletion__c,mkto_si__Relative_Score_Value__c,Pagent__c,LeadDoNotFollowUp__c,mkto_si__Last_Interesting_Moment_Source__c,OtherPhone,MailingState,DoNotCont__c,mkto2__Original_Search_Engine__c,SystemModstamp,MailingCountry,MailingPostalCode,AddressAddInfor__c,Birthdate,FirstName,mkto2__Original_Search_Phrase__c,DoNotMail__c,OtherLatitude,LocalCounty__c,LeadStatus__c,mkto2__Original_Referrer__c,LastReferencedDate,ReportsToId,POBoxZip__c,MarcomPrefEmail__c,Title,MasterRecordId,CommPref__c,UnsubscribeAll__c,JigsawContactId,PreferredCCTeam__c,LastValidatedContactDetail__c,JobFunc__c,PreferredName__c,mkto_si__Last_Interesting_Moment_Desc__c,MayEdit,FaxNational__c,AdditionalAddress__c,CreatedDate,FieloEE__Member__c,County__c,mkto_si__Priority__c,MarcomPrefSurvey__c,mkto2__Inferred_State_Region__c,RecordTypeId,LastValidatedMarketingCommunication__c,PRMContact__c,mkto2__Inferred_Company__c,mkto2__Inferred_Country__c,LastValidatedAddressInformation__c,LastCURequestDate,StrategicThemes__c,WorkFaxExt__c,MailingStreet,mkto2__Acquisition_Program__c,MarcomPrefPostalMail__c,AssistantName,mkto_si__Last_Interesting_Moment_Type__c,UpdateAccountAddress__c,Authority__c,CurrencyIsoCode,PS_OLH__Chat_ID__c,mkto2__Acquisition_Date__c,LocalLastName__c,SDHGoldenVersion__c,OtherCity,JobDescr__c,Country__c,SourceDetail__c,TECH_NumberOfOpenLeads__c,City__c,mkto_si__HideDate__c,LastViewedDate,AccountId,EmailBouncedReason,LocalCity__c,Id,POBox__c,ProductLines__c,WorkPhoneExt__c,StreetLocalLang__c,SEContactID__c,IsPersonAccount,LocalFirstName__c,Phone,StateProv__c,GeoLocation__Longitude__s,mkto2__Inferred_City__c,HasOptedOutOfFax,REP_PilotSource__c,HasBudget__c,ToBeDeleted__c,TECH_Downloadable__c,LastActivityDate,DoNotSur__c,mkto2__Lead_Score__c,IVRMenu__c,MailingCity,HowHeard__c,LocalMidInit__c,ExistingLeadOwner__c,Street__c,Tech_CountryCode__c,OtherStreet,Department,IsEmailBounced,Description,OtherreasonHowHeard__c,LastModifiedById,Fax,AssistantPhone,LastValidatedContactCommunication__c,ZipCode__c,Aphone__c,ServiceContractType__c,Name,CorrespLang__c,mkto2__Inferred_Metropolitan_Area__c,PS_OLH__Mail_ID__c,OtherCountry,DemographicScore__c,DuplicateWith__c,GeoLocation__Latitude__s,SynchronizeWithMarketo__c, Account.Name,Owner.Name,PBAAgent__r.Name,Pagent__r.Name,Country__r.Name,Contact.Account.Street__c,Contact.Account.City__c,Contact.Account.ZipCode__c';
    public static final String CASE_RETRIEVED_FIELDS = 'TECH_CustomerReplyReceived__c,TECH_SendCaseFollwupEmail__c,SVMXC__BW_Selected_On__c,REP_ResolvedIn5Days__c,AssetOwner__c,HasSelfServiceComments,SVMXC__Billing_Type__c,PreferredOutboundMedia__c,SVMXC__Perform_Auto_Entitlement__c,AnswerToCustomer__c,SVMXC__BW_Territory__c,SVMXC__SLA_Clock_Pause_Restart_Time__c,CaseNumber,SVMXC__Is_PM_Case__c,CustomerMajorIssue__c,TECH_OpenLevel3EscalationAgeinHrs__c,PortalCustomerName__c,TECH_Hours_OffSetLevel2__c,TECH_DateMarkedAsSpam__c,SVMXC__Top_Level__c,SVMXC__BW_Time_Zone__c,REP_ResolvedIn1Day__c,SVMXC__Component__c,CreatedById,OtherExternalReferenceType2__c,CallbackPhone__c,SVMXC__PM_Plan__c,TECH_IsPMIFlag__c,OwnerId,TECH_OpenAnswerAvailabletoL2AgeinHrs__c,TECH_DebitPoints__c,TECH_OpenAnsAvlblPrSptCtrAgeinHrsSatThrs__c,TECH_AdvancedFirstAssignmentSLA__c,Quantity__c,LastLevel3User__c,ActionNeededFrom__c,TECH_SupportEmail__c,TECH_OpenPrSprtCntrEsclnAgeinHrsSatThurs__c,CaseSubSymptom__c,SubFamily__c,SystemModstamp,ProductFamily__c,SVMXC__SLA_Clock_Pause_Days__c,TECH_NoOfOpenPendingActivities__c,TECH_OpenAgeinHrsSunThurs__c,SerialNumber__c,SVMXC__Booking_Window__c,LastReferencedDate,FirstAssignmentAge__c,ClosedDate,Subject,Level1ExitDateTime__c,SVMXC__Initial_Response_Internal_By__c,FreeOfCharge__c,RelatedSupportCase__c,LastLevel2User__c,SVMXC__Onsite_Response_Customer_By__c,TECH_EmailToCaseDebug__c,TECH_OpenExpSptCtrEsclnAgeinHrsSatThurs__c,SVMXC__SLA_Terms__c,TECH_FollowupWFTriggered__c,SuppliedJobTitle__c,TECH_IsSymptomMajor__c,SystemIdReference__c,CreatedDate,PrimaryFirstAssignmentDueDate__c,Level2ExitDateTime__c,CommercialReference__c,REP_ResolvedIn2Days__c,SuppliedCompany,Operating_System__c,SVMXC__SLA_Clock_Paused__c,Details__c,PointDebitReason__c,SVMXC__Preferred_Start_Time__c,Level2ResolutionHrs__c,ProductBU__c,TECH_TemplateName__c,TECH_TeamMailBox__c,TECH_OpenPrSprtCntrEsclnAgeinHrsSunThurs__c,IsClosed,ProductDescription__c,RelatedContract__c,TECH_2015ReferentialUpdated__c,SuppliedCountry__c,SupportCategory__c,ThreadID__c,SVMXC__BW_Slots_Before__c,PS_OLH__Chat_ID__c,SVMXC__Clock_Paused_Forever__c,CampaignKeycode__c,PromotionSource__c,ClientReference2__c,ProjectName__c,TECH_ClosedPndngAcnAgeinHrsSunThrs__c,TECH_GMRCode__c,TECH_CustomerResponseReceived__c,Level3ExitDateTime__c,SoftwareVersion__c,TECH_OpenAwaitingDetailsCustomerAge__c,SVMXC__Restoration_Customer_By__c,LastViewedDate,AdvancedLeadCaseTeam__c,SystemReference__c,SVMXC__Entitlement_Type__c,Details2__c,TECH_NumberOfUnreadEmails__c,Level2EntryDateTime__c,TECH_LaunchSymptom__c,TECH_Hours_OffSetLevel3__c,ExpertLastCaseTeam__c,AdvancedLastCaseTeam__c,TECH_OpenAwaitingDetailsInternalAge__c,SVMXC__Service_Contract__c,SuppliedName,SVMXC__Entitlement_Notes__c,Description,LastModifiedById,TECH_GMRLauchForPortal__c,TECH_ZeroPendingActions__c,SVMXC__Actual_Onsite_Response__c,Status,Level1ResolutionHrs__c,CheckedBySymptom__c,TECH_CaseDataUpdated__c,SVMXC__Resolution_Customer_By__c,PS_OLH__Mail_ID__c,AdvancedFirstAssignmentDueDate__c,LastLevel1User__c,Points__c,LastCustomerEmailDate__c,TECH_IsDueDateChanged__c,TECH_BypassQueueAssignment__c,TECH_ByPassVRForGMRPopulate__c,SVMXC__SLA_Clock_Pause_Minutes__c,TECH_CaseStatus__c,CaseSymptom__c,PCRExternalReferenceCount__c,ParentId,Spam__c,PortalTeamLevel1__c,LastModifiedDate,Origin,PortalTeamLevel2__c,AdvancedFirstAssignmentAge__c,TECH_HoursOffSet__c,SVMXC__Product__c,SVMXC__Actual_Initial_Response__c,TECH_OpenAnsAvlblAdvSptCtrAgeinHrsSunThr__c,TECH_OpenAdvSptCtrEsclnAgeinHrsSatThurs__c,CaseOtherSymptom__c,IsLocked,SVMXC__SLA_Clock_Pause_Hours__c,ExpertInternalComments__c,ProductGroup__c,Level3ResolutionHrs__c,FirstReplyToCustomerDate__c,TECH_OpenLevel2EscalationAgeinHrs__c,L2L3InternalComments__c,SVMXC__Initial_Response_Customer_By__c,TECH_OpenAnswerAvailabletoL1AgeinHrs__c,TECH_ExpertFirstAssignmentSLA__c,IsDeleted,Family_lk__c,CaseReOpenCount__c,OtherSymptom__c,OldestCustomerEmailWithoutReplyDate__c,TECH_PrimaryFirstAssignmentSLA__c,SVMXC__SLA_Clock_Pause_Time__c,SVMXC__Resolution_Internal_By__c,Team__c,Symptom__c,SuppliedLanguage__c,SVMXC__Onsite_Response_Internal_By__c,ExpertFirstAssignmentDueDate__c,TECH_OpenAnsAvlblPrSptCtrAgeinHrsSunThrs__c,SVMXC__Warranty__c,TECH_ClosedPendingActionAgeinHrs__c,TECH_SLADueDate__c,TECH_OpenAgeinHrs__c,LeadAdvancedAgent__c,TECH_AutoClosureWFTriggered__c,TECH_SLA__c,LeadExpertAgent__c,SVMXC__Actual_Restoration__c,DueDate__c,Severity__c,CCCountry__c,ProductLine__c,CreatedByTeam__c,TECH_OpenAwaitingDtlsIntrnalAgeSatThurs__c,SuppliedLastName__c,DateCode__c,SVMXC__Actual_Resolution__c,Priority,CommercialReference_lk__c,SVMXC__Restoration_Internal_By__c,TECH_ClosedPndngAcnAgeinHrsSatThrs__c,SVMXC__Proforma_Invoice_Amount__c,MayEdit,ContactId,DebitPoints__c,SVMXC__SLA_Clock_Extension_Minutes__c,SVMXC__Preferred_End_Time__c,TECH_OpenAwaitingDtlsCstmrAgeSunThrs__c,ExpertLeadCaseTeam__c,ExternalReference2__c,SVMXC__Scheduled_Date__c,SVMXC__Is_Invoice_Created__c,GenesysInteractionId__c,SVMXC__BW_Selected_By__c,TECH_OpenAdvSptCtrEsclnAgeinHrsSunThurs__c,ProductSuccession__c,ClientReference__c,AutoFollowupwithCustomer__c,IsEscalated,LastActivityDate__c,CheckedbyGMR__c,SVMXC__Site__c,SVMXC__Is_SLA_Calculated__c,TECH_AutoClosureForCase__c,TECH_LaunchGMR__c,ActualClosedDate__c,Level3EntryDateTime__c,LevelOfExpertise__c,TECH_FAQ__c,SVMXC__BW_Date__c,SupportContractLevel__c,DebitPointsUpdateError__c,CurrencyIsoCode,SVMXC__Time_to_Initial_Response__c,TECH_OpenAgeinHrsSatThurs__c,LastReplyToCustomerDate__c,TECH_SPOC__c,CTRValueChainPlayer__c,LastStatusChangeDate__c,TECH_OpenAwaitingDtlsIntrnlAgeSunThurs__c,TECH_CaseStatusClosedRequested__c,CustomerMood__c,TECH_OpenAnsAvlblAdvSptCtrAgeHrsSatThrs__c,SVMXC__Is_Service_Covered__c,SuppliedEmail,AccountId,SVMXC__SLA_Clock_Pause_Reason__c,SubSymptom__c,Id,ActualTechnicalLevelofCase__c,OtherExternalReferenceType__c,LeadPrimaryAgent__c,Type,SuppliedPhone,PortalID__c,EmailOwnershipDateTime__c,Family__c,SVMXC__Time_to_Onsite_Response__c,ExpertFirstAssignmentAge__c,TECH_OpenExpSptCtrEsclnAgeinHrsSunThurs__c,ClosingCriteria__c,SVMXC__Time_to_Restore__c,SVMXC__Is_Entitlement_Performed__c,EmailCreationDateTime__c,TECH_ContactName__c,ContractType__c,Tech_CountryCode__c,ExternalReference__c,SuppliedFirstName__c,TECH_OpenLevel1EscalationAgeinHrs__c,Reason,SVMXC__Auto_Entitlement_Status__c,Level1EntryDateTime__c,Product_Type__c,TECH_CaseLevel__c,PrimaryLastCaseTeam__c,SystemReference2__c,PortalCaseTeam__c,CustomerRequest__c,SVMXC__Time_to_Resolve__c,TECH_OpenAwaitingDtlsCstmrAgeSatThurs__c,SuppliedAddress__c,SVMXC__Proforma_Invoice__c,PrimaryFirstAssignmentAge__c,HasCommentsUnreadByOwner,TeamLeader__c,PrimaryLeadCaseTeam__c, Account.Name, Contact.Name, Owner.Name, CreatedBy.Name, LastModifiedBy.Name, Team__r.Name';
    public static final String latest_LIMIT = ' LIMIT 3 ';
    public static final String latest_ORDER_BY = ' ORDER BY CreatedDate DESC ';
    
    public Map<String, Object> sfdcObjectMap;

    public String getJsonSfdcObjectMap() {
        return JSON.serialize(sfdcObjectMap);
    }

    public Id sfdcId18;

    public Id getSfdcId18()
    {
        return sfdcId18;
    }

    public String name{get;set;}

    public Long getCurrentTimestamp()
    {
        return Datetime.now().getTime();
    }

    public String getSfdcBaseHostname()
    {
        return ApexPages.currentPage().getHeaders().get('X-Salesforce-Forwarded-To');
    }

    /*
    Author: Sid & Polyspot team.
    Function : getContent
    Parameters : params - string , comma seperated list of parameters to be sent to the polyspot server.
    Description : The below method provides a way to send the request to the required polyspot engine and get the json response back.
    The technical part is to create a http request and authenticate the service using Basic Authentication and get the response.
    */
    /*
     public VFC_L2G_AccountContact360 (ApexPages.StandardController controller) {
           Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge');
      }
    */
     public VFC_L2G_AccountContact360 () {
           Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=Edge');
           sfdcObjectMap = new Map<String, Object>();
            try
            {
                String id = Apexpages.currentPage().getParameters().get('bFOId');        
                String objectType = Apexpages.currentPage().getParameters().get('bFOType'); 
                
                // to handle sfdc ID with 15 or 18 chars
                sfdcId18 = Apexpages.currentPage().getParameters().get('bFOId');
                
                if(id != null)
                {
                    Account account;
                    Contact contact;                    
                    if(objectType  != null && objectType == 'Contact')
                    {
                        contact = retrieveContact(id);
                        if(contact.AccountId != null)
                            account = retrieveAccount(contact.AccountId);                
                    }
                    else
                    {
                        account = retrieveAccount(id);
                    }
                    
                    sfdcObjectMap.put('account', account);
                    sfdcObjectMap.put('contact', contact);
                                        
                    if(account != null && account.Id != null)
                    {
                        List<Contact> latestContacts = retrieveLatestContacts(account.Id);
                        sfdcObjectMap.put('latestContacts', latestContacts);
                        
                        List<Case> latestCases= retrieveLatestCases(account.Id);
                        sfdcObjectMap.put('latestCases', latestCases);
                    }
                    
                    String newid = Apexpages.currentPage().getParameters().get('newid');
                    String newObjectType = Apexpages.currentPage().getParameters().get('newObjectType');
               
                    if(newid != null && newObjectType != null) {
                        if(newObjectType == 'Case')
                        {
                            Case newCase = retrieveCase(newid);
                            sfdcObjectMap.put('currentCase', newCase);
                        }
                        else if(newObjectType == 'OpportunityNotification')
                        {
                            OpportunityNotification__c opportunityNotification = retrieveOpportunityNotification(newid);
                            sfdcObjectMap.put('currentOpportunityNotification', opportunityNotification);
                        }
                    }

                }
            }
            catch(Exception e)
            {
                System.debug('An exception occurred: ' + e.getMessage());
                sfdcObjectMap.put('Exception', 'An exception occurred: ' + e.getMessage());
            }
      }
      
        private Account retrieveAccount(String id)
        {
            String query = 'SELECT ' + ACCOUNT_RETRIEVED_FIELDS + ' FROM Account WHERE Id = :id LIMIT 1';
            return Database.query(query);
        }
        
        private Contact retrieveContact(String id)
        {
            String query = 'SELECT ' + CONTACT_RETRIEVED_FIELDS + ' FROM Contact WHERE Id = :id LIMIT 1';
            return Database.query(query);
        }
        
        private List<Contact> retrieveLatestContacts(String id)
        {
            String query = 'SELECT ' + CONTACT_RETRIEVED_FIELDS + ' FROM Contact WHERE Account.Id = :id ' + latest_ORDER_BY + latest_LIMIT ;
            return Database.query(query);
        }
        
        private List<Case> retrieveLatestCases(String id)
        {
            String query = 'SELECT ' + CASE_RETRIEVED_FIELDS + ' FROM Case WHERE Account.Id = :id ' + latest_ORDER_BY + latest_LIMIT ;
            return Database.query(query);
        }

        private Case retrieveCase(String id)
        {
            String query = 'SELECT ' + CASE_RETRIEVED_FIELDS + ' FROM Case WHERE Id = :id LIMIT 1';
            return Database.query(query);
        }   

        private OpportunityNotification__c retrieveOpportunityNotification(String id)
        {
            String query = 'SELECT ' + retrieveTypeFieldNames(Schema.SObjectType.OpportunityNotification__c) + ' FROM OpportunityNotification__c WHERE Id = :id LIMIT 1';
            return Database.query(query);
        }   

        // This method could work for Account and contact, we didn't check if it retrieve all the required data so not used at the moment
        // For cases it throws an exception => probably because too many fields 
        private String retrieveTypeFieldNames(Schema.DescribeSObjectResult objectType)
        {
            String query = '';
            Map<String, Schema.SObjectField> fields = objectType.fields.getMap();
            List<String> fieldNames = new List<String>();
            fieldNames.addAll(fields.keySet());
            for (integer i = 0 ; i < fieldNames.size() ; i++) {
                String fieldName = fieldNames.get(i);
                query += fieldName;
                if(i + 1 < fieldNames.size())
                    query += ', ';
            }
            return query;
        }
        
    @RemoteAction
    global static String getContent(String params) {
        return executeProxy(params,'/engine/api/select');
    }
    @RemoteAction
    global static String getDocument(String params) {
        return executeProxy(params,'/engine/documents');
    }
    @RemoteAction
    global static String getWelcome(String params) {
        return executeProxy(params,'/engine/api/welcome');
    }
    @RemoteAction
    global static String getPrediction(String params) {
        return executeProxy(params,'/engine/api/ce');
    }
    @RemoteAction
    global static String executeProxy(String params, String webserviceURL) {
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(System.Label.CLFEB15CCCL2G_CMEndPoint + webserviceURL);
        req.setMethod('POST');
        req.setBody(params);

        String username = System.Label.CLFEB15CCCL2G_CMUserName;
        String password = System.Label.CLFEB15CCCL2G_CMPassword;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        HttpResponse res = h.send(req);

        return res.getBody();
    }
    @RemoteAction
    global static String postUpdateQuery(String data) {
        Http h = new Http();    
        HttpRequest req = new HttpRequest();
        req.setEndpoint(System.Label.CLFEB15CCCL2G_CMEndPoint + '/engine/documents');
        req.setMethod('POST');
        
        String boundary = '---------------------------128151090031095';
        String contentType = 'multipart/form-data; boundary=' + boundary;
    
        // multipart body
        String body = '\r\n';
        body += '--' + boundary + '\r\n';
        body += 'Content-Disposition: form-data; name=\'file\' \r\n\r\n';
        body += data + '\r\n';
        body += '--' + boundary + '--';
        req.setBody( body );
                
        // Authentication
        String username = System.Label.CLFEB15CCCL2G_CMUserName;
        String password = System.Label.CLFEB15CCCL2G_CMPassword;
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-type', contentType); 
        
        // Send the request, and return a response
        HttpResponse res = h.send(req);        
        return res.getBody();
    }
}