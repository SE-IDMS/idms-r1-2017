/*
    Author          : ACN 
    Date Created    : 17/11/2011
    Description     : Test class for VFC72_ProdLineCloning class 
*/
@isTest
private class VFC72_ProdLineCloning_TEST
{
    static testMethod void testProductLineClone() 
    {
        // List<Profile> profiles = [select id,Name from profile where name='SE - Sales Manager' OR name like '%System Administrator%'];
        List<Profile> profiles = [select id,Name from profile where name like '%System Administrator%'];
        User salesManager1,systemAdmin;

        List<User> users = new List<User>();
        list<User> lstUser = new list<User>();
        for(Profile p: profiles)
        {
            // if(p.name == 'SE - Sales Manager'){
            //     salesManager1 = Utils_TestMethods.createStandardUser(p.Id,'tU1VFC65');                
            //     lstUser.add(salesManager1 );
            // }
            // else if(p.name=='System Administrator')
                systemAdmin= Utils_TestMethods.createStandardUserByPassVR(p.Id,'tU3VFC65');
         }
         insert lstUser;


         User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;        
        System.runAs(sysadmin2){
            UserandPermissionSets salesManager1userandpermissionsetrecord=new UserandPermissionSets('Users','SE - Sales Manager');
            salesManager1 = salesManager1userandpermissionsetrecord.userrecord;
            Database.insert(salesManager1);
            lstUser.add(salesManager1);
            List<PermissionSetAssignment> permissionsetassignmentlstforsalesManager1=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:salesManager1userandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforsalesManager1.add(new PermissionSetAssignment(AssigneeId=salesManager1.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforsalesManager1.size()>0)
            Database.insert(permissionsetassignmentlstforsalesManager1);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }
         
         //inserts Account
         Account acc = RIT_Utils_TestMethods.createAccount();
         acc.ZipCode__c = '100001';
         insert acc;
         acc = [select id from Account where Id=:acc.Id];
         Opportunity opp;
         list<AccountShare> lstAccountShare = new list<AccountShare>();
         for(integer i=0;i<lstUSer.size();i++)
         {
            AccountShare aShare = new AccountShare();
            aShare.accountId= acc.Id;
            aShare.UserOrGroupId = lstUSer[i].Id;
            aShare.accountAccessLevel = 'edit';
            aShare.OpportunityAccessLevel = 'read';
            aShare.CaseAccessLevel = 'edit';
            //insert aShare;  
            lstAccountShare.add(aShare);
         }                  
         insert lstAccountShare;
         system.runas(salesManager1)
         {
             //inserts Opportunity
             opp = RIT_Utils_TestMethods.createOpportunity(acc.Id);
             insert opp;
             OPP_ProductLine__c productLine = Utils_TestMethods.createProductLine(opp.Id); 
             insert productLine;
             ApexPages.StandardController sc = new ApexPages.StandardController(productLine);
             VFC72_ProdLineCloning cloneProdLine = new VFC72_ProdLineCloning(sc);
             cloneProdLine.cloneProdLine();
             cloneProdLine.continueClone();
         }
     }
     static testMethod void testProdLineClone()
     {     
             //inserts Account
             Account acc = RIT_Utils_TestMethods.createAccount();
             acc.ZipCode__c = '100001';
             insert acc;
             Opportunity opp = RIT_Utils_TestMethods.createOpportunity(acc.Id);
             insert opp;
             OPP_ProductLine__c productLine = Utils_TestMethods.createProductLine(opp.Id); 
             insert productLine;
             ApexPages.StandardController sc = new ApexPages.StandardController(productLine);
             VFC72_ProdLineCloning cloneProdLine = new VFC72_ProdLineCloning(sc);
             cloneProdLine.cloneProdLine();
             cloneProdLine.continueClone();
     
     }

}