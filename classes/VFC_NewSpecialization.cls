public class VFC_NewSpecialization {

    public VFC_NewSpecialization(ApexPages.StandardController controller) {

    }
    
    public PageReference redirectToNewSpecialization()
    {
        System.Debug('VFC_NewSpecialization redirectToSpecialization - Method is called.');
        // Create a new edit page for the CCC Action 
        PageReference newPartnerProgramURL = new PageReference('/'+SObjectType.Specialization__c.getKeyPrefix()+'/e' );
        
        try {
            
            String retURL = system.currentpagereference().getParameters().get('retURL'); // Get return URL paramater
           
            // Set the return URL to the edit page with the value from the current visual force page
            if(retURL  != null) {
                newPartnerProgramURL.getParameters().put(Label.CL00330, retURL);  
           
            }
            newPartnerProgramURL.getParameters().put(Label.CLMAY13PRM32, Label.CLOCT13PRM53);  
            newPartnerProgramURL.getParameters().put(Label.CL00690, Label.CL00691);             
        }
        catch(Exception e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
                System.debug('VFC_NewSpecialization.redirectToNewSpecialization'+e.getTypeName()+' - '+ e.getMessage());
        }
        System.Debug('VFC_NewSpecialization End of Method.');
        return newPartnerProgramURL;
    }//End of method

}