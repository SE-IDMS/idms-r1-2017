//@Author :- Mohammad Naved
//@Description :- Test class to test the Scope of country  trigger function.
//@ Date 10/09/2014
@isTest
public class AP_Scope_Of_Countries_Test{
    //Method to initilize CIS date for this test class.
    public static void initInsert(){
        //Initilizing Country object data
        List<Country__c> countryList = new  List<Country__c>();
        
        Country__c countryInstance1 = new Country__c();
        countryInstance1.Name = 'India';
        countryInstance1.CountryCode__c ='IN';
        countryList.add(countryInstance1);
        
        Country__c countryInstance2 = new Country__c();
        countryInstance2.Name = 'France';
        countryInstance2.CountryCode__c ='FR';
        countryList.add(countryInstance2);
       
        Database.insert(countryList);
        
        //Initilizing Correspondent Team Data
        List<CIS_CorrespondentsTeam__c> correspondentTeamList = new List<CIS_CorrespondentsTeam__c>();
        
        CIS_CorrespondentsTeam__c cisCorrespondentTeamInstance1 = new CIS_CorrespondentsTeam__c();
        cisCorrespondentTeamInstance1.CIS_FieldsOfActivity__c='ED: Low Voltage';
        cisCorrespondentTeamInstance1.CIS_ScopeOfCountries__c ='France';
        correspondentTeamList.add(cisCorrespondentTeamInstance1);
        
        CIS_CorrespondentsTeam__c cisCorrespondentTeamInstance2 = new CIS_CorrespondentsTeam__c();
        cisCorrespondentTeamInstance2.CIS_FieldsOfActivity__c='ED: Medium Voltage';
        cisCorrespondentTeamInstance2.CIS_ScopeOfCountries__c='India';
        correspondentTeamList.add(cisCorrespondentTeamInstance2);
        
        Database.insert(correspondentTeamList);
        
        
        CountryCISCorrespondentsTeamLink__c ccct = new CountryCISCorrespondentsTeamLink__c();
        ccct.CISCorrespondentsTeam__c = cisCorrespondentTeamInstance1.id;
        ccct.Country__c=countryInstance2.id;
        insert ccct;
        ccct.Country__c=countryInstance1.id;
        update ccct;
        
        delete ccct;
        
        CountryCISCorrespondentsTeamLink__c ccct2 = new CountryCISCorrespondentsTeamLink__c();
        ccct2.CISCorrespondentsTeam__c = cisCorrespondentTeamInstance2.id;
        ccct2.Country__c=countryInstance1.id;
        insert ccct2;
        
        
    }
    @isTest
    public static void myUnitTest1() {
        initInsert();
    }


}