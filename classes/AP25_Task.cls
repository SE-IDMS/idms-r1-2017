/*
    Author          : ACN 
    Date Created    : 24/07/2011
    Description     : Class utilized by TaskAfterInsert, TaskAfterUpdate Triggers.
*/
public class AP25_Task
{
    /* This method is called from TaskAfterInsert Trigger. This method segregates completed tasks & non completed tasks,
     * queries the Leads and assigns the value to the lead fields accordingly. Also, calls the updateLeads method 
     * to update the leads.
     */
    public static void insertTasks(Map<Id,task> newTasks)
    {
        set<Id> completedTasks = new set<Id>();
        set<Id> inProgTasks = new set<Id>(); 
        Map<Id, Date> whoIdSchDate = new Map<Id, Date>();
    
        for(Task task:newTasks.values())
        {
            if(task.ActivityDate==null)
                task.ActivityDate.addError(Label.CL00580); 
            else
            {   
                if(task.Status == Label.CL00327)
                    completedTasks.add(task.whoId);                      //Stores the whoId of the completed tasks
                else if(task.Status!= Label.CL00327)
                {
                    inProgTasks.add(task.whoId);                         //Stores the whoId of the non completed tasks
                    whoIdSchDate.put(task.whoId, task.ActivityDate);
                }
            }
        }
        
        //Queries the List of leads which needs to be updated
        List<Lead> leads = [select Status, SubStatus__c, LastCallAttemptDate__c, ScheduledCallbackDate__c,CallAttempts__c,RecordTypeId from lead where Id in: completedTasks OR Id in:inProgTasks];
      
        for(Lead lead: leads)
        {
             //This field is used to identify whether a lead has an activity
             lead.TECH_IsActivityAsc__c = true;
             
             //Updates the Scheduled CallBack Date of Lead with the Recent non completed activity's due date
             if(whoIdSchDate.containsKey(lead.Id))
                  lead.ScheduledCallbackDate__c = whoIdSchDate.get(lead.Id);
             
             //Updates the Last Call Attempted Date of Lead with the Task Created Date
             lead.LastCallAttemptDate__c = Date.Today();
        }        
        
        //Calls the updateLeads method 
        updateLeads(completedTasks, inProgTasks, leads);                
    }

    /* This method is called from TaskAfterUpdate Trigger. This method segregates completed tasks & non completed tasks,
     * queries the Leads and assigns the value to the lead fields accordingly. Also, calls the updateLeads method 
     * to update the leads.
     */    
    public static void updateTasks(Map<Id,Task> oldtasks, Map<Id,task> newTasks)
    {
        set<Id> completedTasks = new set<Id>();
        set<Id> inProgTasks = new set<Id>(); 
        set<id> dueDateChanged = new Set<Id>();     
        Map<Id, Date> whoIdSchDate = new Map<Id, Date>();
 
        for(Task task:newTasks.values())
        {
            if(task.ActivityDate==null)
                task.ActivityDate.addError(Label.CL00580); 
            else
            { 
                //Checks if the Status value has been changed during Task Update
                if(task.status == Label.CL00327 && task.status!=oldTasks.get(task.Id).Status)
                {
                    completedTasks.add(task.whoId);                         //Stores the whoId of the completed tasks
                }
                 
                //Checks if the Due Date has been changed during Task Update
                if(task.ActivityDate!=oldTasks.get(task.Id).ActivityDate)
                {
                    dueDateChanged.add(task.whoId);
                    whoIdSchDate.put(task.whoId, task.ActivityDate);
                    inProgTasks.add(task.whoId);                            //Stores the whoId of the completed tasks         
                }
            }
        }
        
        //Queries the List of leads which needs to be updated
        List<Lead> leads = [select Status, SubStatus__c, LastCallAttemptDate__c, ScheduledCallbackDate__c,CallAttempts__c,RecordTypeId from lead where Id in: completedTasks OR Id in:inProgTasks OR Id in:dueDateChanged];
      
        for(Lead lead: leads)
        {
             //Updates the Scheduled CallBack Date of Lead with the Recent non completed activity's due date
             if(dueDateChanged.contains(lead.Id) && whoIdSchDate.containsKey(lead.Id))
             {
                 lead.ScheduledCallbackDate__c = whoIdSchDate.get(lead.Id);
             }             
        }        
        //Calls the update leads method
        updateLeads(completedTasks, inProgTasks, leads);        
    }
    
    /* Based on the Task status, this method segregates the lead having completed and non-completed tasks.
     * Calls the logACall method for the completedtasks & calls the schACall method for the non-completed tasks,
     * Updates the leads accordingly.
     */
    public static void updateLeads(Set<Id> completedTasks, set<Id> inProgTasks, List<lead> leads)
    {    
        List<Lead> leadsToBeUpdated = new List<Lead>(); 
        List<Lead> completedTaskLeads = new List<Lead>();
        List<Lead> nonCompTaskLeads = new List<Lead>();  

        for(Lead lead: leads)
        {
             //Assigns the value for Completed Tasks
             if(completedTasks.contains(lead.Id))   
             {
                 completedTaskLeads.add(lead);
             }
             //Assigns the value for non completed tasks
             else if(inProgTasks.contains(lead.Id))   
             {
                 nonCompTaskLeads.add(lead);            
             }
        }

        //Calls the logACall method for completed Tasks
        for(Lead lead: logACall(completedTaskLeads))
        {
            leadsToBeUpdated.add(lead);
        }
        //Calls the schACall method for non-completed Tasks        
        for(Lead lead: schACall(nonCompTaskLeads))
        {
            leadsToBeUpdated.add(lead);
        }
        if(leadsToBeUpdated.Size() > 0)
        {
            if(leadsToBeUpdated.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## AP25 Error Inserting : '+ Label.CL00264);
            }
            else
            {
                Database.SaveResult[] leadsUpdate= Database.update(leadsToBeUpdated, false);
                for(Database.SaveResult sr: leadsUpdate)
                {
                    if(!sr.isSuccess())
                        System.Debug('######## AP25 Error Inserting: '+sr.getErrors()[0].getStatusCode()+' '+sr.getErrors()[0].getMessage());                    
                }
            }                         
        }        
    }

    /* This method updates the Lead Status to Working, Sub-Status to Call Attempted and increments
       the call attempts */    
    public static List<Lead> logACall(List<Lead> leads)
    {
        for(Lead lead:leads)
        {
            //Br-5681 Changes by Revanth R 
            //Restrict the Status,Sub-status changes for Marketo Leads
            if(lead.RecordTypeId != System.Label.CLJUL14MKT01){
            lead.Status = Label.CL00447;    
            lead.SubStatus__c = Label.CL00449;
            }
            
            else
              // March 2016 Release
             //Change the Sub-status changes for Marketo Leads
            if(lead.RecordTypeId == System.Label.CLJUL14MKT01 && lead.status == System.Label.CL00555){
              lead.SubStatus__c = System.Label.CLMAR16MKT01;
            }
            
            lead.CallAttempts__c+=1.0;
        }
        return leads;
    }
    
    /* This method updates the Lead Status to Working, Sub-Status to Callback Requested and decrements
       the call attempts */       
    public static List<Lead> schACall(List<Lead> leads)
    {
        for(Lead lead:leads)
        {
            //Br-5681 Changes by Revanth R 
            //Restrict the Status,Sub-status changes for Marketo Leads
            if(lead.RecordTypeId != System.Label.CLJUL14MKT01){
            lead.Status = Label.CL00447; 
            lead.SubStatus__c = Label.CL00448;
            }
            if(lead.CallAttempts__c>0)
                lead.CallAttempts__c-=1.0;  
        }
        return leads;
    }
    
    //DEC 12 AC uat def-
    public static set<id> getPersonAccountIds(set<id> setAccIds)
    {
        map<id, Account> mapPersonAccs = new map<id,Account>([select id from Account where id in:setAccIds and IsPersonAccount=true]);
        if(mapPersonAccs != null && mapPersonAccs.size() > 0)
           return mapPersonAccs.keyset();
        
        return null;   
    }
    
    //APRIL 2014 RELEASE BR-4616
    //The date when first event or task was created is to be stored in "TECH_First Activity" field of Opportunity
    public static void insertOpptyTasks(Map<Id,Task> newOpptyTasks)
    {
        System.Debug('--------- newOpptyTasks '+newOpptyTasks);
        Map<Id, Date> whatIdCreatedDate = new Map<Id, Date>();
        List<Opportunity> updateOppties = new List<Opportunity>();
        set<Id> completedWhatIds = new set<Id>();
        
        System.Debug('--------- completedWhatIds '+completedWhatIds);
        
        for(Task otask: newOpptyTasks.values())
        {
            System.Debug('--------- otask '+otask);
            if(!(completedWhatIds.contains(otask.whatId)))  //checks if the completedWhatIds does not contain the same Opportunity 
            {
                    completedWhatIds.add(otask.whatId);                         //Stores the unique whatId of tasks
                    System.Debug('--------- completedWhatIds '+completedWhatIds);
                    whatIdCreatedDate.put(otask.whatId, otask.CreatedDate.date());     
                    System.Debug('--------- otask.whatId'+otask.whatId+'-------------otask.CreatedDate '+otask.CreatedDate.date());
            }
        }
        
        //Queries the List of leads which needs to be updated
        List<Opportunity> opptyLst = [select Id, TECH_FirstActivity__c from Opportunity where Id in: completedWhatIds Limit 50000];
        
        for(Opportunity opp: opptyLst)
        {
             //Updates the First Activity field of Opportunity with the created date of the task
             if((opp.TECH_FirstActivity__c==NULL) && whatIdCreatedDate.containsKey(opp.Id))
             {
                  System.Debug('--------- TECH_FirstActivity__c for opp.Id '+opp.Id+'-- is --'+opp.TECH_FirstActivity__c);
                  opp.TECH_FirstActivity__c = date.today();
                  System.Debug('--------- TECH_FirstActivity__c for opp.Id '+opp.Id+'-- is --'+opp.TECH_FirstActivity__c);
                  updateOppties.add(opp);
             }
             System.Debug('--------- updateOppties '+updateOppties);
        }
        
        if(updateOppties.Size() > 0)
        {
            if(updateOppties.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())  //determine if there are too many Opportunities to update and avoid governor limits
            {
                System.Debug('######## AP25 Opportunity Update Error : '+ Label.CL00264);
            }
            else
            {
                Database.SaveResult[] opptyUpdate= Database.update(updateOppties, false);
                for(Database.SaveResult sr: opptyUpdate)
                {
                    System.Debug('--------- opptyUpdate '+opptyUpdate);
                    if(!sr.isSuccess())
                    {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) 
                        {
                            System.debug('######## AP25 Opportunity Update Error :');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            //System.debug('Fields that affected this error: ' + err.getFields());
                        }
                    }                   
                }
            }                         
        }
    }     
    
    
    /* October 2015 Release : CCC BR-5961
     * This method is called from TaskBeforeUpdate Trigger. This method updates Completed Date/Time and CCC Case Task Resolution Age fields of completed Tasks.
     */
    public static void updateTaskCompletionFields(Map<Id,task> completedTasks, Set<Id> caseIds)
    {    
    
        Map<Id,Case> caseBusinessHours = new Map<Id,Case>([Select Id, BusinessHoursId from Case where id in:caseIds]);
        System.debug('--->> caseBusinessHours ::'+CaseBusinessHours);
        
        for(Task task:completedTasks.values()){
            task.CCCTaskCompletionTime__c = System.Now();
            task.CCCTaskResolutionAge__c = (BusinessHours.diff(caseBusinessHours.get(task.WhatId).BusinessHoursId, task.CreatedDate, System.Now()) / 1000.0 / 3600.0);
            System.debug('--->> Created Datetime :: '+task.CreatedDate);
            System.debug('--->> Completed Datetime :: '+task.CCCTaskCompletionTime__c);
            System.debug('--->> CCC Task Resolution Age :: '+task.CCCTaskResolutionAge__c);
        }
        
        
    }
}