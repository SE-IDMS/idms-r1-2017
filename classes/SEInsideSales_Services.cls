global class SEInsideSales_Services{
    @AuraEnabled
    global static List<Sobject> getData(String fields,String sobjectname, String condition,Integer limitcount){
        try{
        String thequery = 'select '+fields+' from '+sobjectname+' where '+condition + ' limit ' + limitcount;
        System.debug(thequery);
        return Database.query(thequery);
        }
        catch(Exception ex){
            return null;
        }
    }
    @AuraEnabled
    global static List<Event> getSalesEvents(Boolean isDummy){
        if(isDummy)
            return [SELECT Id, RecordType.Name,RecordTypeId, WhoId,Who.Name, WhatId,What.Name,Subject, Location, IsAllDayEvent, DurationInMinutes, StartDateTime, EndDateTime, Description, Status__c,CreatedById,CreatedBy.Name FROM Event limit 10];
            else
            return [SELECT Id, RecordType.Name,RecordTypeId, WhoId,Who.Name, WhatId,What.Name,Subject, Location, IsAllDayEvent, DurationInMinutes, StartDateTime, EndDateTime, Description, Status__c,CreatedById,CreatedBy.Name,SalesEventType__c FROM Event  where OwnerId=:UserInfo.getUserId() and StartDateTime = next_n_days:14];
        }
    
    @AuraEnabled
    global static List<Event> getEventsForDate(String starttime, String endtime){          
       return Database.query('SELECT Id, RecordType.Name,RecordTypeId, WhoId,Who.Name, WhatId,What.Name,Subject, Location, IsAllDayEvent, DurationInMinutes, StartDateTime, EndDateTime, Description, Status__c,CreatedById,CreatedBy.Name FROM Event  where CreatedById=\''+UserInfo.getUserId()+'\' and StartDateTime>='+starttime+' and EndDateTime <='+endtime);
    }
    
    @AuraEnabled
    global static EventInfo getEventInformation(String eventId){
        Event eventrecord = [Select Id,RecordType.Name,WhoId,Who.Name,WhatId,What.Name,Subject,Location,DurationInMinutes,StartDateTime,EndDateTime,Description,Status__c,SalesEventType__c from Event where id=:eventId limit 1];
        String sobjecttype = eventrecord.WhatId.getSObjectType()+'';
        System.debug(sobjecttype);
        EventInfo eventinfo = new EventInfo();
        eventinfo.eventrec=eventrecord;
        if(sobjecttype == 'Account')
        {
            eventinfo.accountrec = [Select Id,Name, BillingStreet, BillingStateCode,BillingCountry,BillingPostalCode,Phone,Owner.Name,(Select Id,Name,Amount,CloseDate from Opportunities), (Select Id,CaseNumber,Status,Subject,Priority,Owner.Name from Cases) from Account where Id=:eventrecord.WhatId limit 100];
            eventinfo.opportunities = eventinfo.accountrec.opportunities;
            eventinfo.cases = eventinfo.accountrec.cases;
        }
        
        if(sobjecttype == 'Opportunity')
        {
           Opportunity opprecord = [Select Id,Name,Amount,CloseDate,AccountId,Account.Name,Account.BillingStreet,Account.BillingStateCode,Account.BillingPostalCode,Account.Phone,Account.Owner.Name from Opportunity where Id=:eventrecord.WhatId];
            eventinfo.opportunities = new List<Opportunity>{opprecord};
           /* eventinfo.accountrec = new Account(Id=opprecord.AccountId);
            eventinfo.accountrec.Name=opprecord.Account.Name;
            eventinfo.accountrec.BillingStreet=opprecord.Account.BillingStreet;
            eventinfo.accountrec.BillingStateCode=opprecord.Account.BillingStateCode;
            eventinfo.accountrec.BillingPostalCode=opprecord.Account.BillingPostalCode;            
            eventinfo.accountrec.Phone=opprecord.Account.Phone;*/
            eventinfo.accountrec = opprecord.Account;
        }
        
        System.debug(eventinfo);
        return eventinfo;        
    }
    
    @AuraEnabled
    global static Event getEventDates(String eventId){
        return [Select Id,DurationInMinutes,StartDateTime,EndDateTime from Event where id=:eventId limit 1];
    }
    
    @AuraEnabled
    global static Event getEventRecordforEdit(String eventId){
        return [SELECT Id, OwnerId, Subject, RecordTypeId, SalesEventType__c, Status__c, Description, WhoId, WhatId, StartDateTime, EndDateTime, ShowAs,Off3Comments__c,Off3Comments2__c,Off3Comments3__c,Off3Comments4__c,Off3Comments5__c,X3DiscussedTopic1__c,X3DiscussedTopic2__c,X3DiscussedTopic3__c,X3DiscussedTopic4__c,X3DiscussedTopic5__c FROM Event where id=:eventId limit 1];
    }
    
    @AuraEnabled
    global static Boolean saveEvent(Event eventrecord){
        System.debug(eventrecord);
        Database.SaveResult sr = Database.update(eventrecord);
        return sr.isSuccess();
    }
    
    @AuraEnabled
    global static Map<String,List<Opportunity>> getMyOpportunites(){
        List<Opportunity> opps_i_own = [SELECT id,Name,Amount,StageName,CloseDate,Account.Name, SourceReference__c, OpptyType__c,Probability from Opportunity where OwnerId=:UserInfo.getUserId()];
        List<Opportunity> opps_w_i_own_account = [SELECT id,Name,Amount,StageName,CloseDate,Account.Name, SourceReference__c, OpptyType__c,Probability from Opportunity where AccountId in (select id from Account where OwnerId=:UserInfo.getUserId())];
        List<Opportunity> opps_am_part_of_accountteam = [SELECT id,Name,Amount,StageName,CloseDate,Account.Name, SourceReference__c, OpptyType__c,Probability from Opportunity where AccountId in (select AccountId from AccountTeamMember where UserId=:UserInfo.getUserId())];
        Map<String,List<Opportunity>> alllist = new Map<String,List<Opportunity>>();
     	alllist.put('oppsiown',opps_i_own);
        alllist.put('opps_w_i_own_account',opps_w_i_own_account);
        alllist.put('opps_am_part_of_accountteam',opps_am_part_of_accountteam);
        Set<Opportunity> all_of_the_above= new Set<Opportunity>();
        all_of_the_above.addAll(opps_i_own);
        all_of_the_above.addAll(opps_w_i_own_account);
        all_of_the_above.addAll(opps_am_part_of_accountteam);
        List<Opportunity> allistofopps = new List<Opportunity>();
        allistofopps.addAll(all_of_the_above);
		alllist.put('oppall',allistofopps);
        return alllist;        
    }    
    
    @AuraEnabled
    global static List<Lead> getMyLeads(){
        return [SELECT Id, Name,FirstName, LastName, Company, Status, Priority__c, LastCallAttemptDate__c, toLabel(JobTitle__c), Phone,NonMarcommCampaign__r.Name, Urgency__c, RelativeScore__c FROM Lead where OwnerId=:UserInfo.getUserId()];
    }
    
    @AuraEnabled
    global static List<Campaign> getMyCampaigns(){        
        return [SELECT Id, Name, BUFocus__c, ProductFocus__c, StartDate, EndDate, Status, NumberOfContacts, LastModifiedDate FROM Campaign where Name like '%Inside_Sales%'];        
    }
    
    @AuraEnabled
    global static String attachContactsToCampaign(String campaignid,List<String> contactids){
        List<Lead> toInsertLeads = new List<Lead>();        
        for(Contact contactRecord: [SELECT LastName,FirstName,MobilePhone,Phone,Email,CurrencyISOCode,Country__c from contact where id in :contactids]){
           Lead leadRecord  = new Lead();          	
           leadRecord.NonMarcommCampaign__c = campaignid;
           leadRecord.LastName = contactRecord.LastName;
            leadRecord.FirstName = contactRecord.FirstName;           
           leadRecord.MobilePhone = contactRecord.MobilePhone;
           leadRecord.Email = contactRecord.Email;
           leadRecord.Phone = contactRecord.Phone;
           leadRecord.CurrencyISOCode = contactRecord.CurrencyISOCode;
           leadRecord.Country__c = contactRecord.Country__c;
           toInsertLeads.add(leadRecord);
        }
        List<Database.SaveResult> srs = Database.insert(toInsertLeads);
        return JSON.serialize(srs);
    }
    
    @AuraEnabled
    global static List<Account> getMyAccounts(){
        List<Account> myaccounts = [Select Id,Name,AccountMasterProfile__c,BillingCity,BillingState,PrimaryRelationshipLeader__c,Owner.Name,(SELECT convertCurrency(DirectSales__c),convertCurrency(IndirectSales__c),convertCurrency(TotalPAM__c),CurrencyISOCode,Name FROM Account_Master_Profiles__r)  from Account where OwnerId=:UserInfo.getUserId()];
        List<Account> ampartofteamaccounts = [Select Id,Name,AccountMasterProfile__c,BillingCity,BillingState,PrimaryRelationshipLeader__c,Owner.Name,(SELECT convertCurrency(DirectSales__c),convertCurrency(IndirectSales__c),convertCurrency(TotalPAM__c),CurrencyISOCode,Name FROM Account_Master_Profiles__r) from Account where Id in (select AccountId from AccountTeamMember where UserId=:UserInfo.getUserId())];
        Set<Account> setAccounts = new Set<Account>();
        setAccounts.addAll(myaccounts);
        setAccounts.addAll(ampartofteamaccounts);
        List<Account> toreturnAccounts = new List<Account>();
        toreturnAccounts.addAll(setAccounts);
        return toreturnAccounts;
    }
    
    @AuraEnabled
    global static Boolean updateOpportunityAmount(List<Opportunity> opps){
        List<Database.SaveResult> srs = Database.update(opps,false);
        return true;
    }
    
    @AuraEnabled
    global static Account getAccountById(String accountId) {
        List<Account> acountList = [SELECT Id, Name, Phone, BillingCountry, BillingState, BillingCity, BillingStateCode, BillingStreet, BillingPostalCode FROM Account WHERE Id=:accountId];
        if (acountList.size() > 0) {
            return acountList[0];
        } 
        return null;
    }    
    
    global class EventInfo{
        @AuraEnabled
        global Event eventrec{get;set;}
        @AuraEnabled
        global Account accountrec{get;set;}
        @AuraEnabled
        global List<Opportunity> opportunities{get;set;}
        @AuraEnabled
        global List<Case> cases{get;set;}                
    }
    
    @AuraEnabled
    global static List<DiscussionTopic__c> getDiscussionTopics(){
        User userCountry = [select Country__c from User where id=:userinfo.getUserId()];        
        return [select Name,DiscussionTopicDescription__c from DiscussionTopic__c Where Name like :userCountry.Country__c+'%' Order by Name];
    }
    
    
}