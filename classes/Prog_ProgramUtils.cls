/*
    Author          : Srikant Joshi
    Date Created    : 11/07/2012
    Description     : Utils class for the class Pro_ProgramTriggers.
                      Populate the totals for Fy0,Fy1,Fy2,Fy3.
                      
*/

public with sharing class Prog_ProgramUtils
{

    public static void updateTotalsFields(List<DMTProgram__c> listProgramNew)
    {      
        for(DMTProgram__c prog : listProgramNew){
            //HOTFIX - 09/09/2012 - Removed P & L Costs from Totals - Srikant Joshi
            /*
            prog.NewFY0TotalCosts__c = prog.NewFY0AppServicesCustExp__c+prog.NewFY0BusinessCashoutCAPEX__c+prog.NewFY0BusinessCashoutOPEXOTC__c+prog.NewFY0BusinessInternal__c+prog.NewFY0BusinessRunningCostsImpact__c+prog.NewFY0BusInternalChargedBacktoIPO__c+prog.NewFY0CashoutCAPEX__c+prog.NewFY0CashoutOPEX__c+prog.NewFY0GDCosts__c+prog.NewFY0NonIPOITCostsInternal__c+prog.NewFY0Regions__c+prog.NewFY0RunningITCostsImpact__c+prog.NewFY0SmallEnhancements__c+prog.NewFY0TechnologyServices__c;
            prog.NewFY1TotalCosts__c = prog.NewFY1AppServicesCustExp__c+prog.NewFY1BusinessCashoutCAPEX__c+prog.NewFY1BusinessCashoutOPEXOTC__c+prog.NewFY1BusinessInternal__c+prog.NewFY1BusinessRunningCostsImpact__c+prog.NewFY1BusInternalChargedBacktoIPO__c+prog.NewFY1CashoutCAPEX__c+prog.NewFY1CashoutOPEX__c+prog.NewFY1GDCosts__c+prog.NewFY1NonIPOITCostsInternal__c+prog.NewFY1Regions__c+prog.NewFY1RunningITCostsImpact__c+prog.NewFY1SmallEnhancements__c+prog.NewFY1TechnologyServices__c;
            prog.NewFY2TotalCosts__c = prog.NewFY2AppServicesCustExp__c+prog.NewFY2BusinessCashoutCAPEX__c+prog.NewFY2BusinessCashoutOPEXOTC__c+prog.NewFY2BusinessInternal__c+prog.NewFY2BusinessRunningCostsImpact__c+prog.NewFY2BusInternalChargedBacktoIPO__c+prog.NewFY2CashoutCAPEX__c+prog.NewFY2CashoutOPEX__c+prog.NewFY2GDCosts__c+prog.NewFY2NonIPOITCostsInternal__c+prog.NewFY2Regions__c+prog.NewFY2RunningITCostsImpact__c+prog.NewFY2SmallEnhancements__c+prog.NewFY2TechnologyServices__c;
            prog.NewFY3TotalCosts__c = prog.NewFY3AppServicesCustExp__c+prog.NewFY3BusinessCashoutCAPEX__c+prog.NewFY3BusinessCashoutOPEXOTC__c+prog.NewFY3BusinessInternal__c+prog.NewFY3BusinessRunningCostsImpact__c+prog.NewFY3BusInternalChargedBacktoIPO__c+prog.NewFY3CashoutCAPEX__c+prog.NewFY3CashoutOPEX__c+prog.NewFY3GDCosts__c+prog.NewFY3NonIPOITCostsInternal__c+prog.NewFY3Regions__c+prog.NewFY3RunningITCostsImpact__c+prog.NewFY3SmallEnhancements__c+prog.NewFY3TechnologyServices__c;
            //END HOTFIX - 09/09/2012
            */
            prog.TECHTotalPlannedITCosts__c = prog.TotalITCostsCashout__c + prog.TotalITCostsInternal__c;
        }
        
    }
}