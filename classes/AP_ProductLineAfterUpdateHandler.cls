/*
Author: Siddharth N(GD Solutions)
Purpose: Merged different functionalities of account before insert to a single class
*/
public without sharing class AP_ProductLineAfterUpdateHandler {    
    private Map<Id,List<OPP_ProductLine__c>> opportunityProductLinesMap=new Map<Id,List<OPP_ProductLine__c>>();
    private Map<Id,List<OPP_ProductLine__c>> opportunityAllProdLinesMap=new Map<Id,List<OPP_ProductLine__c>>();
    private List<iD> oppIds = new List<Id>();
    private Map<Id,Opportunity> opportunityMap=new Map<Id,Opportunity>();
    private Map<String,CS_ProductLineBU__c> businessUnitMap = CS_ProductLineBU__c.getall();
    private Set<Id> prodLnesId = new Set<Id>();
    private List<OPP_ProductLine__c> prodLnsUpd = new List<OPP_ProductLine__c>();
    private List<OPP_ProductLine__c> prodLns = new List<OPP_ProductLine__c>();
    private Map<Id, String> errorMsg = new Map<Id, String>();
    private Map<Id,String> errorProdLines = new Map<Id,String>();
    public void OnAfterUpdate(List<OPP_ProductLine__c> newProductLines,Map<Id,OPP_ProductLine__c> oldProductLinesMap){

        // EXECUTE AFTER INSERT LOGIC
        for(OPP_ProductLine__c productLine:newProductLines)
        {
            oppIds.add(productLine.Opportunity__c);
            if(opportunityProductLinesMap.containsKey(productLine.Opportunity__c))
                opportunityProductLinesMap.get(productLine.Opportunity__c).add(productLine);
            else
                opportunityProductLinesMap.put(productLine.Opportunity__c,new List<OPP_ProductLine__c>{productLine});
            //Checks if the record has Parent Line and Line Type equals Child Line

            if(productLine.ParentLine__c !=null && oldProductLinesMap.get(productLine.Id)!=null && productLine.ParentLine__c != oldProductLinesMap.get(productLine.Id).ParentLine__c)
            {
                prodLnesId.add(productLine.Id);
            }
        }

        Integer availableRows=Limits.getLimitQueryRows()-Limits.getQueryRows();
        
        //Queries all the Product Lines related to the Opportunity
        List<OPP_ProductLine__c> allProductLines = new List<OPP_ProductLine__c>();
        if(!(oppIds.isEmpty()))
            allProductLines = [select Id,IncludedinForecast__c,Amount__c,LineAmount__c,BusinessMixValue__c,CurrencyISOCode,IsaLevel3memberopptyLine__c, IsALevel2__c, ProductBU__c, LineStatus__c,Opportunity__c,ParentLine__c, LineType__c from OPP_ProductLine__c where Opportunity__c in: oppIds limit :availableRows];
        
        for(OPP_ProductLine__c productLine:allProductLines)
        {
            if(opportunityAllProdLinesMap.containsKey(productLine.Opportunity__c))
                opportunityAllProdLinesMap.get(productLine.Opportunity__c).add(productLine);
            else
                opportunityAllProdLinesMap.put(productLine.Opportunity__c,new List<OPP_ProductLine__c>{productLine});
        }
        //Queries required fields of Opportunity
        if(!(opportunityProductLinesMap.keySet().isEmpty()))
        opportunityMap = new Map<id,Opportunity>([Select StageName,CurrencyISOCode,IsAMultiBusinessLevel2__c,Amount,CloseDate,BusinessMix__c, TECH_IsLevel3__c from Opportunity where id in :opportunityProductLinesMap.keySet() limit :availableRows]);        
        
        handleParentLineUpdates(newProductLines);        
        handleOpportunityUpdates();
        for(OPP_ProductLine__c productLine:newProductLines)
        {
            if(errorProdLines.containsKey(productLine.Id))
                productLine.addError(errorProdLines.get(productLine.Id));
        }
    }
    
    //The method checks if the Product Line is only 2 levels deep
    private void handleParentLineUpdates(List<OPP_ProductLine__c> newProductLines)
    {
        for(OPP_ProductLine__c pl: newProductLines)
        {
            for(OPP_ProductLine__c p2: prodLns)
            {
                if(p2.ParentLine__c == pl.Id)
                {
                    pl.addError(Label.CLOCT13SLS12);
                }
            }
        }        
    }
    
    //The method updates Amount, Revenue Amount Converted, Business Mix , Is A multi-level Business, Is A Level 3 fields of Opportunity    
    private void handleOpportunityUpdates()
    {
        List<Opportunity> oppToBeUpd = new List<Opportunity>();
        Set<Id> prodLinesId = new Set<Id>();     
        Map<Integer, Id> updOpps= new Map<Integer, Id>();   

        //Iterates through all the Product Lines
        for(Id opportunityId : opportunityAllProdLinesMap.keySet())
        {
            Opportunity currentOpportunity = opportunityMap.get(opportunityId);

            Decimal amount = 0, revAmount=0, revAmountEUR = 0;
            Decimal bussMixVal=0,level3 = 0,level3Else = 0, level2= 0,prevBussMix, prevLevel3;

            String BUMix;
            
            for(OPP_ProductLine__c productLine:opportunityAllProdLinesMap.get(opportunityId))
            {
                //calculate Amount
                String stgeName = opportunityMap.get(opportunityId).StageName;
                if(stgeName == Label.CLOCT13SLS13 || stgeName == Label.CL00146 || stgeName == Label.CL00147 || stgeName == Label.CL00547 || stgeName == Label.CLOCT13SLS17 || stgeName == Label.CLSEP12SLS11)
                {
                    if(productLine.LineAmount__c!=null && productLine.LineType__c == Label.CLOCT13SLS07 && (productLine.LineStatus__c == Label.CLOCT13PRM20 || productLine.LineStatus__c == Label.CLOCT13SLS05 || productLine.LineStatus__c == Label.CLSEP12I2P103 || productLine.LineStatus__c == Label.CLOCT13SLS28|| productLine.LineStatus__c == Label.CLOCT13SLS29))
                    {
                        amount+=productLine.LineAmount__c; 
                    }    
                }
                else if(stgeName == Label.CL00272)
                {
                    if(productLine.LineAmount__c!=null && productLine.LineType__c == Label.CLOCT13SLS07 && productLine.LineStatus__c !=Label.CL00139  && productLine.LineStatus__c !=Label.CLJUN16SLS01)       // added LineStatus__c != Service Contract Amendment for June 2016 Release BR - 9826
                    {
                        amount+=productLine.LineAmount__c; 
                    }    
                }
                /*
                //Calculate Revenue Amount
                if(productLine.LineAmount__c!=null && productLine.LineType__c == Label.CLOCT13SLS07 && productLine.LineStatus__c != Label.CL00139 && productLine.LineStatus__c != Label.CLSEP12SLS14 && productLine.LineStatus__c != Label.CL00326 && productLine.LineStatus__c!= Label.CLSEP12PRM20)
                {
                    revAmount+=productLine.LineAmount__c; 
                }                

                //Calculates Revenue Amount EUR
                if(currentOpportunity.RevenueAmountEUR__c==null)
                    currentOpportunity.RevenueAmountEUR__c  = 0;
                if(Date.today() <= currentOpportunity.CloseDate && revAmount!=0)
                {
                    revAmountEUR = Utils_Methods.convertCurrencyToEuroforDatedCurrency(currentOpportunity.CurrencyIsoCode , revAmount);
                }*/
                //Calculates Business Mix
                if(productLine.LineType__c == Label.CLOCT13SLS07 && productLine.LineStatus__c!= Label.CL00139 && productLine.LineStatus__c!= Label.CLSEP12SLS14 && productLine.LineStatus__c!= Label.CL00326 && businessUnitMap.get(productLine.ProductBU__c)!=null)
                {                
                    if(BUMix==null)
                        BUMix=businessUnitMap.get(productLine.ProductBU__c).ProductBU__c+'';
                    else if(!(BUMix.contains(businessUnitMap.get(productLine.ProductBU__c).ProductBU__c)))
                        BUMix+=';'+businessUnitMap.get(productLine.ProductBU__c).ProductBU__c;
                }                
                //Calculates IsAmultiLevelBusiness & TECH_IsALevel3
                if(productline.LineStatus__c!= Label.CL00326 && 
                    productLine.LineType__c == Label.CLOCT13SLS07 &&
                    productline.LineStatus__c!= Label.CLSEP12SLS14 && 
                    productline.LineStatus__c!= Label.CL00139)
                {
                    if(productline.IsaLevel3memberopptyLine__c ==1)
                    {
                        ++level3Else;
                    }                    
                    for(OPP_ProductLine__c productLine1:opportunityAllProdLinesMap.get(opportunityId))
                    {               
                        if(productLine1.Id!= productLine.Id && 
                        productLine.LineType__c == Label.CLOCT13SLS07 &&
                        productline1.LineStatus__c!= Label.CL00326 && 
                        productline1.LineStatus__c!= Label.CLSEP12SLS14 && 
                        productline1.LineStatus__c!= Label.CL00139                     
                        )
                        {
                            /*if(productline.IsALevel2__c ==1 && productline1.IsALevel2__c ==1 && productLine.ProductBU__c!= productLine1.ProductBU__c)
                            {
                                ++bussMixVal;
                            }*/
                            if(productline.IsALevel2__c ==1 && productline1.IsALevel2__c ==1)
                            {
                                ++bussMixVal;
                            }
                            /*if(productline.IsaLevel3memberopptyLine__c ==1 && productline1.IsaLevel3memberopptyLine__c ==1 && productLine.ProductBU__c!= productLine1.ProductBU__c)
                            {
                                ++level3;
                            } */
                            
                            /*if(productline.IsALevel2__c ==1 && productline1.IsaLevel3memberopptyLine__c ==1 && productLine.ProductBU__c!= productLine1.ProductBU__c)
                            {
                                ++level2;
                            }*/                           
                            if(productline.IsALevel2__c ==1 && productline1.IsALevel2__c ==1 && productLine.BusinessMixValue__c!= productLine1.BusinessMixValue__c)
                            {
                                ++level2;
                            }   
                        }
                    } 
                }              
            }
            Integer j=0,noOfOpps=1;
            prevBussMix   = currentOpportunity.IsAMultiBusinessLevel2__c;         
            prevLevel3=currentOpportunity.TECH_IsLevel3__c;

            //Updates the Opporutnity Fields
            if(BUMix!=currentOpportunity.BusinessMix__c)
            {
                currentOpportunity.BusinessMix__c  = BUMix;  
                ++j;
            }
            if(bussMixVal>=2)
                    currentOpportunity.IsAMultiBusinessLevel2__c = 1;
                else
                    currentOpportunity.IsAMultiBusinessLevel2__c = 0;                

                /*if(level3<2)
                {
                    if(level2>=1 && level3Else>=1)
                        currentOpportunity.TECH_IsLevel3__c = 1;
                    else
                        currentOpportunity.TECH_IsLevel3__c = 0;
                }
                else
                    currentOpportunity.TECH_IsLevel3__c = 1;
                */
                
                if(level2>=2)
                        currentOpportunity.TECH_IsLevel3__c = 1;
                    else
                        currentOpportunity.TECH_IsLevel3__c = 0;
                if(amount!=currentOpportunity.amount)
                {
                    currentOpportunity.amount = amount;
                    ++j;
                }
                /*
                if(revAmount!= currentOpportunity.revenueAmount__c)
                {
                    currentOpportunity.revenueAmount__c = revAmount;
                    currentOpportunity.revenueAmountEUR__c= revAmountEUR; 
                    ++j;
                }*/

                if(j>0 || prevBussMix!=currentOpportunity.IsAMultiBusinessLevel2__c || prevLevel3!=currentOpportunity.TECH_IsLevel3__c)
                {
                     oppToBeUpd.add(currentOpportunity);                   
                     updOpps.put(noOfOpps, currentOpportunity.Id);  
                     noOfOpps = noOfOpps+1;                 
        }       }
        //Updates the Opportunity
        system.debug(oppToBeUpd+'List of Opportunities to be updated');
        Database.saveResult[] sr= Database.update(oppToBeUpd,false);
        
        //Handling the Errors 
        Integer recs = 1;
        for(Database.saveresult s: sr)
        {
            if((!(s.isSuccess())))
                errorMsg.put(updOpps.get(recs), s.getErrors()[0].getMessage());
            recs = recs+1;
        }
        
        for(Id oppsId: errorMsg.keyset())
        {
            if(opportunityAllProdLinesMap.get(oppsId)!=null)
            {
                for(OPP_ProductLine__c prLne: opportunityAllProdLinesMap.get(oppsId))
                {            
                    errorProdLines.put(prLne.Id, errorMsg.get(oppsId));
                }
            }    
        } 

        


    }    
}