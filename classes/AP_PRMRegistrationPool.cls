/**
* @author Anil Sistla
* @date 05/11/2016
* @description Model class used for JSON Serialize / Deserialize while populating the registration pool.
*/

public class AP_PRMRegistrationPool {
    public Account AccountInfo { get; set; }
    public Contact ContactInfo { get; set; }
    public User UserInfo { get; set; }
    public FieloEE__Member__c MemberInfo { get; set; }
    
    public void AP_PRMRegistrationPool() {
        
    }
}