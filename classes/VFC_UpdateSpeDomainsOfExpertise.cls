public with sharing class VFC_UpdateSpeDomainsOfExpertise {

    Specialization__c  spec;
    list<SpecializationDomainsOfExpertise__c> lstExistingDE = new list<SpecializationDomainsOfExpertise__c>();
    list<SpecializationDomainsOfExpertise__c> lstExistingDE2 = new list<SpecializationDomainsOfExpertise__c>();
    
    Set<Id> selOpts1 = new Set<Id>();
    public Boolean hasError{get;set;}
    public SelectOption[] selDE { get; set; }
    public SelectOption[] allDE{ get; set; }
    public String type {get;set;}
    
    
    public VFC_UpdateSpeDomainsOfExpertise(ApexPages.StandardController controller) {
       /* List<String> Fields=new List<String>{'DomainsOfExpertise1__c','DomainsOfExpertise2__c'};
        if(!test.isRunningTest())
            controller.addFields(Fields);
        */
        
        selDE = new List<SelectOption>();  
        allDE = new List<SelectOption>();
        set<String> setDE1 = new set<String>();
        spec = (Specialization__c)controller.getRecord();
        type = ApexPages.currentPage().getParameters().get('type');
        //lstExistingDE = [Select DomainsOfExpertiseCatalog__r.Name, DomainsOfExpertiseCatalog__c ,DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c from SpecializationDomainsOfExpertise__c where Specialization__c = : spec.Id and DomainsOfExpertiseCatalog__r.ParentDomainsOfExpertise__c = null order by DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c limit 1000];
        lstExistingDE = [Select DomainsOfExpertiseCatalog__r.Name, DomainsOfExpertiseCatalog__c ,DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c from SpecializationDomainsOfExpertise__c where Specialization__c = : spec.Id  order by DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c limit 1000];
        if(lstExistingDE != null && !lstExistingDE.isEmpty())
        {
            for (SpecializationDomainsOfExpertise__c s : lstExistingDE ) 
            {

                selDE.add(new SelectOption(s.DomainsOfExpertiseCatalog__c ,s.DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c));
                selOpts1.add(s.DomainsOfExpertiseCatalog__c );
                //setDE1.add(s.DomainsOfExpertiseCatalog__r.Name);
            }  
        }
        
        list<DomainsOfExpertiseCatalog__c > lstAllDE = new list<DomainsOfExpertiseCatalog__c >();
        lstAllDE = [SELECT id,name,DomainsOfExpertiseName__c FROM DomainsOfExpertiseCatalog__c  WHERE ParentDomainsOfExpertise__c != null and active__c = true order by name limit 1000];        
        for(DomainsOfExpertiseCatalog__c  DE : lstAllDE) 
        {
            if(!(selOpts1.contains(DE.Id)))
                allDE.add(new SelectOption(DE.Id, DE.DomainsOfExpertiseName__c ));
        }
        /*
        if(type == 'Level1')
        {
            if(lstExistingDE != null && !lstExistingDE.isEmpty())
            {
                for (SpecializationDomainsOfExpertise__c s : lstExistingDE ) 
                {

                    selDE.add(new SelectOption(s.DomainsOfExpertiseCatalog__c ,s.DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c));
                    selOpts1.add(s.DomainsOfExpertiseCatalog__c );
                    //setDE1.add(s.DomainsOfExpertiseCatalog__r.Name);
                }  
            }
            
            list<DomainsOfExpertiseCatalog__c > lstAllDE1 = new list<DomainsOfExpertiseCatalog__c >();
            lstAllDE1 = [SELECT id,name,DomainsOfExpertiseName__c FROM DomainsOfExpertiseCatalog__c  WHERE ParentDomainsOfExpertise__c = null and active__c = true order by DomainsOfExpertiseName__c limit 1000];        
            for(DomainsOfExpertiseCatalog__c  DE : lstAllDE1) 
            {
                if(!(selOpts1.contains(DE.Id)))
                    allDE.add(new SelectOption(DE.Id, DE.DomainsOfExpertiseName__c ));
            }
        }

        else if(type == 'Level2')
        {
            list<DomainsOfExpertiseCatalog__c > lstDE2 = new list<DomainsOfExpertiseCatalog__c >();
            if(lstExistingDE != null && !lstExistingDE.isEmpty())
            {
                for (SpecializationDomainsOfExpertise__c s : lstExistingDE ) 
                {
                    setDE1.add(s.DomainsOfExpertiseCatalog__r.Name);
                }
                lstDE2 = [SELECT id,name,DomainsOfExpertiseName__c FROM DomainsOfExpertiseCatalog__c  WHERE ParentDomainsOfExpertise__r.name in:setDE1 and active__c = true order by DomainsOfExpertiseName__c limit 1000];
            }
            else
            {
                hasError = true; 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select Domains of Expertise 1'));
            }
            lstExistingDE2 = [Select id,DomainsOfExpertiseCatalog__c ,DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c from SpecializationDomainsOfExpertise__c WHERE Specialization__c = : spec.Id and DomainsOfExpertiseCatalog__r.ParentDomainsOfExpertise__c != null order by DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c limit 5000];
            for(SpecializationDomainsOfExpertise__c existingDE2 : lstExistingDE2)
            {
                selDE.add(new SelectOption(existingDE2.DomainsOfExpertiseCatalog__c ,existingDE2.DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c));
                selOpts1.add(existingDE2.DomainsOfExpertiseCatalog__c );
            }
            for(DomainsOfExpertiseCatalog__c  allLvl2 : lstDE2 )
            {
                if(!(selOpts1.contains(allLvl2.Id)))
                    allDE.add(new SelectOption(allLvl2.Id, allLvl2.DomainsOfExpertiseName__c ));
            }
            
        }
        */
    }
    
    public PageReference Save() 
    {
        Savepoint sp =  Database.setSavepoint();
        try
        {            
            set<Id> setSelIds = new set<ID>();
            list<SpecializationDomainsOfExpertise__c> lstNewPrgDE = new list<SpecializationDomainsOfExpertise__c>();
            for(SelectOption so : selDE) 
            {
                SpecializationDomainsOfExpertise__c partDE = new SpecializationDomainsOfExpertise__c();
                partDE.Specialization__c = spec.Id;
                partDE.DomainsOfExpertiseCatalog__c  = so.getValue();
                lstNewPrgDE.add(partDE);
                setSelIds.add(so.getValue());
            }
            /*
            if(type == Label.CLJUN13PRM03 && lstExistingDE != null && !lstExistingDE.isEmpty())
            {
                selOpts1.removeAll(setSelIds);
                lstExistingDE2 = [Select id,DomainsOfExpertiseCatalog__c ,DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c from SpecializationDomainsOfExpertise__c WHERE Specialization__c = : spec.Id and DomainsOfExpertiseCatalog__r.ParentDomainsOfExpertise__c in : selOpts1 limit 5000];
                delete lstExistingDE;
                delete lstExistingDE2;
                
            }
            else if(type == Label.CLJUN13PRM04 && lstExistingDE2 != null && !lstExistingDE2.isEmpty())
            {
                delete lstExistingDE2;
            }
            */
            delete lstExistingDE;
            lstExistingDE.clear();
            
            if(lstNewPrgDE != null && !lstNewPrgDE.isEmpty())
                insert lstNewPrgDE;
        }
        catch(DMLException e)
        {
            Database.rollback(sp);
            hasError = true;    
            for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),'')); System.debug(e.getDmlMessage(i)); 
            } 
                       
            return null;
        }
        
        return new pagereference('/'+spec.Id);    
       return null;
    }

}