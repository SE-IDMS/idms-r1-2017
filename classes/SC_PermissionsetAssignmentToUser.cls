global class SC_PermissionsetAssignmentToUser implements Schedulable{
   global void execute(SchedulableContext sc) 
   
   {
    if(!TEST.isRunningTest())
    {
      string qry = 'SELECT Id,Permission_sets_assigned__c,SystemModstamp FROM user where isActive = True' ;
      database.executebatch (new Batch_PermsnAssignAllUser(qry),200);
   }
    
   }
}