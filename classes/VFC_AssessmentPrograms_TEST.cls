@isTest
private class VFC_AssessmentPrograms_TEST
{
    public static testmethod void TestMethod1()
    {
        /*Assessment__c asm = new Assessment__c();
        asm = Utils_TestMethods.createAssessment();
        asm.AutomaticAssignment__c = true;
        asm.Active__c = true;
        insert asm;*/

        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='ASM';
        insert country;

        //creates a global partner program
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;

        //creates a program level for the global program
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;

        //creates a country partner program
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;

        //creates a program level for the country partner program
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;
        prg2.levelstatus__c = 'active';
        update prg2;
        
        Recordtype prgORFAssessmentRecordType = [Select id from RecordType where developername =  'PRMProgramApplication' limit 1];
            
        Assessment__c asm = new Assessment__c();
        asm = Utils_TestMethods.createAssessment();
        asm.recordTypeId = prgORFAssessmentRecordType.Id ;
        asm.AutomaticAssignment__c = true;
        asm.Active__c = true;
        asm.Name = 'Sample Assessment';
        asm.PartnerProgram__c = gp1.id;
        asm.ProgramLevel__c = prg1.id;
        insert asm;
        /*try {
        insert asm;
        }
        catch(Exception e) {
            System.debug('An error has Occured');
        }*/

        AssessementProgram__c assmentRec = new AssessementProgram__c();
        assmentRec.Assessment__c = asm.Id;
        assmentRec.PartnerProgram__c = cp1.Id;
        assmentRec.ProgramLevel__c = prg2.Id;
        assmentRec.Active__c = True;
        insert assmentRec;
        
        ApexPages.currentPage().getparameters().put('Id',asm.id);  
        ApexPages.StandardController controller = new ApexPages.StandardController(asm);
        VFC_AssessmentPrograms ClassObj = new VFC_AssessmentPrograms(controller);
        ClassObj.addNewRow();
        //ClassObj.deleteRow();
        //VFC_AssessmentPrograms programRec = new VFC_AssessmentPrograms();
        ClassObj.getProgramFeatures();
        ClassObj.ProgramAssessment[0].ToBeDeleted = true;
        //programRec.ToBeDeleted = true;
        ClassObj.deleteRow();
        ClassObj.doSave();
        ClassObj.doCancel();
    }
}