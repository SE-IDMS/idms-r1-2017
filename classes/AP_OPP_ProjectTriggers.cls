/*Class utilised by Master Project Trigger.
Monalisa project oct 2015 release*/
public class AP_OPP_ProjectTriggers
{
     //If the currency is changed for the master project Batch has to recalculate the amount
     public static void updateProjectForBatch(List<OPP_Project__c> prjlst)
    {
            for(OPP_Project__c prj:prjlst)
                prj.Tech_AmoutUpdateByBatch__c=TRUE;
    }
}