/*
    Pooja Suresh
    Release     : Jul16 Release (BR-10118) - Automatic Ownership Re-Assignment from “External User”
    Purpose     : Reassigns ownership for an account created by an external agent with profile SE - Agent (Global - Community) using queue-able job.
                  An internal user is assigned as account owner based on rules defined in Account Ownership Rule for EAO functionality
                  Simultaneouly external agent is added to account team with read/write access to account, oppty and case
 */

public without sharing class AP_UpdateExternalSalesAgent{

    //BR-10118 Jul16 Rel - As part of enhancement DEF-10514 (Edit Access to the field External Sales Responsible)
    //Making field editable, need to ensure to remove previous ExternalSalesResp from AccountTeam and add the new ExternalSalesResp into AccountTeam
    public static void updateExternalSalesResp(Map<Id,Id> newESRAccIdMap, Map<Id,Id> oldESRAccIdMap)
    {
        System.debug('------ Update External Sales Responsible Starts ------');
        system.debug('------ AP_UpdateExternalSalesAgent.updateExternalSalesResp.newESRAccIdMap --'+ newESRAccIdMap );
        system.debug('------ AP_UpdateExternalSalesAgent.updateExternalSalesResp.oldESRAccIdMap --'+ oldESRAccIdMap );

        List<Id> deleteOldAtm = new List<Id>();
        List<Id> deleteAccShare = new List<Id>();
        List<AccountTeamMember> createNewAtm = new List<AccountTeamMember>();
        Set<String> oldAtmKey = new Set<String>();    //Unique key list of oldATM UserId+AccountID
        List<AccountTeamMember> oldAtmList = [Select Id,UserId,AccountID from AccountTeammember where AccountID IN : oldESRAccIdMap.values() and UserId IN :oldESRAccIdMap.keySet() and TeamMemberRole =:'ESR'];
        List<AccountShare> accShareList = [SELECT UserOrGroupId,AccountId,RowCause FROM AccountShare WHERE RowCause = 'Manual' and AccountId IN: oldESRAccIdMap.values() and UserOrGroupId IN :oldESRAccIdMap.keySet()];
        
        for(Id oldId: oldESRAccIdMap.keySet())
        {
            oldAtmKey.add(string.valueOf(oldId)+string.valueOf(oldESRAccIdMap.get(oldId)));
        }
        system.debug('------ AP_UpdateExternalSalesAgent.updateExternalSalesResp.oldAtmKey --'+ oldAtmKey );

        for(AccountTeamMember oldAtm: oldAtmList)
        {
            String key = String.ValueOf(oldAtm.UserId) + oldAtm.AccountID;    //key=UserId+AccountId
            system.debug('------ AP_UpdateExternalSalesAgent.updateExternalSalesResp.key --'+ key );
            if(oldAtmKey.contains(key))
                deleteOldAtm.add(oldAtm.Id);
        }
        system.debug('------ AP_UpdateExternalSalesAgent.updateExternalSalesResp.deleteOldAtm --'+ deleteOldAtm );

        for(AccountShare oldAccShr: accShareList)
        {
            String key = String.ValueOf(oldAccShr.UserOrGroupId) + oldAccShr.AccountID;    //key=UserId+AccountId
            system.debug('------ AP_UpdateExternalSalesAgent.updateExternalSalesResp.key --'+ key );
            if(oldAtmKey.contains(key))
                deleteAccShare.add(oldAccShr.Id);
        }
        system.debug('------ AP_UpdateExternalSalesAgent.updateExternalSalesResp.deleteAccShare --'+ deleteAccShare );

        for(Id newId: newESRAccIdMap.keySet())
        {
            AccountTeammember atm = new AccountTeammember(UserId = newId, AccountID = newESRAccIdMap.get(newId), TeamMemberRole = 'ESR', AccountAccessLevel='Edit', OpportunityAccessLevel='Edit', CaseAccessLevel='Edit');
            createNewAtm.add(atm);
        }
        system.debug('------ AP_UpdateExternalSalesAgent.updateExternalSalesResp.createNewAtm --'+ createNewAtm );


        if(!deleteOldAtm.isEmpty())
                Database.delete(deleteOldAtm,false);
                
        if(!deleteOldAtm.isEmpty())
                Database.delete(deleteAccShare,false);

        if(!createNewAtm.isEmpty())
                Database.insert(createNewAtm,false);
        

        System.debug('------ Update External Sales Responsible Ends ------');
    }
}