/*
    Author          : Priyanka Shetty (Schneider Electric)
    Date Created    : 03/22/2012
    Description     : Controller class for the visualforce page CHR_UpdateSelectionInBudgetEnvelop
                      Updates the value for the field 'Selected in Envelop?' after checking whether the logged-in user has the authority to do so.
                      
*/
public with sharing class CHR_UpdateSelectionInBudgetEnvelop
{
    public ApexPages.StandardController controller;
    
    public ChangeRequestFundingEntity__c oFE{get;set;}
       
    public CHR_UpdateSelectionInBudgetEnvelop(ApexPages.StandardController controller)
    {
        this.controller = controller;
        oFE = (ChangeRequestFundingEntity__c) controller.getRecord();
        mainInit(); 
    }    
    
    private void mainInit()
    {
        oFE = [Select Id,Parent__c,SelectedinEnvelop__c,ChangeRequest__r.Parent__c,ChangeRequest__r.NextStep__c,ChangeRequest__r.Platform__c from ChangeRequestFundingEntity__c where Id = :oFE.Id];
    }
    
    public PageReference updateFundingEntity()
    {
        verifyUserCanChangeStatus();        
        return null;
    }
    
    public PageReference backToFundingEntity()
    {
        PageReference pageRef = new PageReference('/'+ oFE.Id);
        pageRef.setRedirect(true);
        return pageRef;        
    }
        
    private void verifyUserCanChangeStatus()
    {   
        
        //begin
        //check if logged-in user can edit the status values
        System.Debug('Logged In User ID: ' + UserInfo.getUserId());
        boolean isAuthorized = false;
        boolean isDefault = false;
  

            List<DMTAuthorizationMasterData__c> objAuth = [SELECT AuthorizedUser1__c,AuthorizedUser2__c,AuthorizedUser3__c,AuthorizedUser4__c,AuthorizedUser5__c,AuthorizedUser6__c,AuthorizedUser7__c,AuthorizedUser8__c,AuthorizedUser9__c,AuthorizedUser10__c,Platform__c,BusinessTechnicalDomain__c,Id,NextStep__c,ParentFamily__c,Parent__c FROM DMTAuthorizationMasterData__c where NextStep__c =:System.Label.DMT_StatusCreated AND Platform__c =:oFE.ChangeRequest__r.Platform__c limit 1];

            Set<string> setUser = new Set<string>();
            
            if(objAuth.size() > 0)
            { 
                System.Debug('objAuth not null');
                SObject sobjectAuth =(SObject)objAuth[0];
                for(Integer i=1;i<11;i++)
                {
                    if(sobjectAuth.get('AuthorizedUser'+i+'__c') !=null)
                        setUser.add(sobjectAuth.get('AuthorizedUser'+i+'__c')+'');
                }
                if(setUser.contains(UserInfo.getUserId()))
                {
                    System.Debug('Found');
                    isAuthorized = true;
                    System.Debug('isAuthorized1: ' + isAuthorized);
                }                         
            }     
            else
            {
                System.Debug('objAuth = null');
                isDefault = true;
                System.Debug('isDefault1: ' + isDefault);
            }
        
   
 
          
        if(UserInfo.getProfileId()== System.Label.CL00110)
            isAuthorized = true;

        System.Debug('isAuthorized3: ' + isAuthorized);

        if(isAuthorized)
        {
            //call the method of the without sharing class   

            CHR_UpdateSelectionInBudgetEnvelopVerify.updateFundingEntity(oFE);
        }                 
        else
        {  
                           
            if(isDefault)
            {

                try
                {              
                    update oFE;
                    System.Debug('Here4');
                    if(Test.isRunningTest())
                        throw new noMessageException('Test Exception');
                }
                catch(DmlException dmlexp)
                {
                    for(integer i = 0;i<dmlexp.getNumDml();i++)
                        ApexPages.addMessages(new noMessageException(dmlexp.getDmlMessage(i)));                      
                }
                catch(Exception exp)
                {
                    ApexPages.addMessages(new noMessageException(exp.getMessage()));
                }
            }
            else
            {
                 ApexPages.addMessages(new noMessageException(System.Label.DMT_MessageNotAuthorizedToChangeStatus));          
            }            
        } 
        //end
                          
    }  
                      
    
    public class noMessageException extends Exception{}
    
}