/********************************************************************
* Company: Fielo
* Created Date: 02/08/2016
* Description: Test class for class FieloPRM_AP_ChallengeMemberTrigger
********************************************************************/
@isTest
public with sharing class FieloPRM_AP_ChallengeMemberTriggerTest{
  
    public static testMethod void testUnit1(){

        User us = new user(id = userinfo.getuserId());

        us.BypassVR__c = true;
        update us;
        
        FieloEE.MockUpFactory.setCustomProperties(false);
        
        FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
        
        program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
        update program;

        Account acc = new Account(Name = 'acc');
        insert acc;
        
        FieloEE__Member__c member = new FieloEE__Member__c(
            FieloEE__LastName__c= 'Polo', 
            FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()), 
            FieloEE__Street__c = 'test', 
            F_Account__c = acc.Id
        );
        insert member;
        
        Contact c = new contact(
            Firstname = 'test',
            Lastname = 'test2',
            AccountId = acc.Id,
            FieloEE__member__c = member.id     
        );
        insert c;

        member = [select FieloEE__User__c, F_Account__c from fieloee__member__c where id=:member.Id]; 

        FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc.Id});

        FieloCH__Challenge__c ch = FieloPRM_UTILS_MockUpFactory.createChallenge('testCH', 'Competition','Objective');
        
        ch.FieloCH__Mode__c = 'Teams';
        update ch;
        
        FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__BadgeMember__c', 'count', null, 'Equals or greater than', 1);

        FieloCH__MissionCriteria__c missionCriteria = FieloPRM_UTILS_MockUpFactory.createCriteria('text', 'F_PRM_BadgeName__c', 'equals', 'Test', null, null, false, mission1.Id);

        FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(ch.id , mission1.id);
        
        FieloEE__Badge__c badge = new FieloEE__Badge__c (
            Name = 'test', 
            F_PRM_Type__c = 'Program Level', 
            F_PRM_BadgeAPIName__c = '1'
        );
        
        FieloEE__Badge__c badge2 = new FieloEE__Badge__c (
            Name = 'test2', 
            F_PRM_Type__c = 'Program Level', 
            F_PRM_BadgeAPIName__c = '2'
        );

        List<FieloEE__Badge__c> badges = new List<FieloEE__Badge__c>{badge, badge2};
        insert badges; 
        
        FieloCH__ChallengeReward__c chr1 = new FieloCH__ChallengeReward__c(
            FieloCH__LogicalExpression__c = '1', 
            FieloCH__Challenge__c = ch.Id, 
            FieloCH__Badge__c = badges[0].id
        );
        
        FieloCH__ChallengeReward__c chr2 = new FieloCH__ChallengeReward__c(
            FieloCH__LogicalExpression__c = '1',
            FieloCH__Challenge__c = ch.Id, 
            FieloCH__Badge__c = badges[1].id
        );

        List<FieloCH__ChallengeReward__c> chRewards = new List<FieloCH__ChallengeReward__c>{chr1,chr2};
        insert chRewards;
        
        ch.FieloCH__isActive__c = true; 
        update ch; 
        
        Id team = [SELECT Id, Name FROM FieloCH__Team__c LIMIT 1].Id;
        
        FieloCH__TeamMember__c teamMember = new FieloCH__TeamMember__c(
            FieloCH__Team2__c = team,
            FieloCH__Member2__c = member.Id
        );
        insert teamMember;
        
        try{
            FieloCH__TeamMember__c teamMember2 = new FieloCH__TeamMember__c(
                FieloCH__Team2__c = team,
                FieloCH__Member2__c = member.Id
            );
            insert teamMember2;
        }catch(Exception e){}

        System.runas(us){
            FieloCH__ChallengeMember__c challengeMember = new FieloCH__ChallengeMember__c(
                FieloCH__Challenge2__c = ch.Id,
                FieloCH__Team__c = team
            );
            insert challengeMember;
            
            challengeMember.FieloCH__Status__c = 'Unsubscribe';
            challengeMember.FieloCH__ChallengeReward__c = chr1.id;
            update challengeMember;
                        
       }
       
    }
}