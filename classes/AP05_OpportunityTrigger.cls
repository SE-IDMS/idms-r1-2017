/*    
    Author: Pooja Bhute
    Date: 02/02/2011
    Description:This class is basically used to handle all the business logic involved in Triggers on the Opportunity object.    
*/

public without sharing class AP05_OpportunityTrigger {
    
    public static Boolean testClassFlag = True;
    public static Boolean seriestobecreated = false;
    public static Boolean isSeriesCreated = WS02_SetConstantValue.iscreateseries ;
    //Copying the Agreement Product Line to Opportunity Product Line on creation of an Opportunity for Agreement.    
    /*public static void addAgreementProdLineToOppts(List<Opportunity> opportunities)
    {
        //AP01_OpportunityFieldUpdate.blOpportunityTestClassFlag  = false;
        System.Debug('****AP05_OpportunityTrigger addAgreementProdLineToOppts Started ****');
        if(testClassFlag == True )
        {  
           
                List<OPP_ProductLine__c> productLinesToInsert = new  List<OPP_ProductLine__c>();
                List<ProductLineForAgreement__c> productLineAgreements = new List<ProductLineForAgreement__c>();
                decimal mdecIntervalLevel = 0;   
                List<String> agreementIds = new List<String>();
                for(Opportunity opportunity:opportunities)
                {
                    agreementIds.add(opportunity.AgreementReference__c);
                }
                
                System.Debug('AP05 opportunity Id: '+opportunities.Size());
                if(!(agreementIds.isEmpty()))
                productLineAgreements = [SELECT ID, Amount__c, DeliveryDate__c, Designation__c, SupplyingPlant__c, SystemIDReference__c, 
                    Quantity__c, Agreement__c, UnitForQuantity__c FROM ProductLineForAgreement__c 
                    WHERE Agreement__c in : agreementIds]; 
                                        
                if(productLineAgreements.size() >0)
                {
                    for(ProductLineForAgreement__c productLineAgreement : productLineAgreements )
                    {
                            for(Opportunity opportunity : opportunities)
                            {
                                if(opportunity.AgreementReference__c == productLineAgreement.Agreement__c)
                                {
                                    mdecIntervalLevel = decimal.valueOf(WS03_NewFrameOpportunity.GetAgreementInterval(opportunity.AgreementReference__c));
                                    OPP_ProductLine__c newProductLine = new OPP_ProductLine__c(); 
                                    if(productLineAgreement.Amount__c !=null && productLineAgreement.Amount__c !=0)
                                    {
                                        newProductLine.Amount__c = productLineAgreement.Amount__c ;// mdecIntervalLevel ;
                                    }
                                    else
                                    {
                                        newProductLine.Amount__c = 0;
                                    }  
                                    newProductLine.DeliveryDate__c = productLineAgreement.DeliveryDate__c;
                                    newProductLine.Designation__c = productLineAgreement.Designation__c;
                                    newProductLine.SupplyingPlant__c = productLineAgreement.SupplyingPlant__c;
                                    newProductLine.Product__c = productLineAgreement.SystemIDReference__c;
                                    newProductLine.Opportunity__c = opportunity.ID;                 
                                    newProductLine.Quantity__c = productLineAgreement.Quantity__c;
                                    newProductLine.UnitForQuantity__c =productLineAgreement.UnitForQuantity__c;
                                    productLinesToInsert.add(newProductLine);
                                }
                            }                        
                    }                
                   
                    if(productLinesToInsert.Size() > 0){
                        if(productLinesToInsert.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                            System.Debug('######## AP05 Error Inserting : '+ Label.CL00264);
                        }
                        else{
                            Database.SaveResult[] productLineInsertResult = Database.insert(productLinesToInsert, false);
                            for(Database.SaveResult sr: productLineInsertResult){
                                if(!sr.isSuccess()){
                                    Database.Error err = sr.getErrors()[0];
                                    System.Debug('######## AP05 Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                                }
                            }
                        }
                    }
             }
             
        }
        
        System.Debug('****AP05_OpportunityTrigger addAgreementProdLineToOppts Ended ****');
                 
    }*/
    
    //Add the Contributor in the Opportunity Split which is related to the new Opportunity created.
    public static void addContributorToSalesContributor(List<Opportunity> opportunities)
    {
        System.Debug('****AP05_OpportunityTrigger addContributorToSalesContributor Started ****');
        if(testClassFlag == True) 
        {
            //Modified for BR-5772            
            List<OpportunitySplit> opptySplitsToInsert = new List <OpportunitySplit>();
            Id splitTypeId = [select Id from OpportunitySplitType where DeveloperName=: Label.CLAPR15SLS70].Id;
            for(Opportunity oppr : opportunities)
            {
                    OpportunitySplit opptySplit = new OpportunitySplit();
                    opptySplit = new OpportunitySplit();               
                    opptySplit.OpportunityId = oppr.ID;
                    opptySplit.SplitPercentage = 100;
                    opptySplit.SplitTypeId = splitTypeId; 
                    //updated for december release
                    if(VFC_ConvertORFToOpportunity.ORFOwnerid != null)
                        opptySplit.SplitOwnerId =VFC_ConvertORFToOpportunity.ORFOwnerid;
                    else
                        opptySplit.SplitOwnerId =oppr.ownerId;
                    opptySplit.Role__c=Label.CL00265;                    
                    opptySplitsToInsert.add(opptySplit);
            }
             if(opptySplitsToInsert.Size() > 0)
             {
                if(opptySplitsToInsert.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
                {
                    System.Debug('######## AP05 Error Inserting : '+ Label.CL00264);
                }
                else
                {
                    Database.SaveResult[] opptySplitInsertResult = Database.insert(opptySplitsToInsert, false);
                    for(Database.SaveResult sr: opptySplitInsertResult)
                    {
                        if(!sr.isSuccess())
                        {
                            Database.Error err = sr.getErrors()[0];
                            System.Debug('######## AP05 Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                        }
                    }
                }           
            }
       System.Debug('****AP05_OpportunityTrigger addContributorToSalesContributor Ended ****');         
    } 
    } 
    
    //Default the Opportunity Detector to the Current User
    public static void DefaultOppDetector(List<Opportunity> opportunities){ 
        System.Debug('****AP05_OpportunityTrigger DefaultOppDetector Started****');
        try{
            if(opportunities!=null && !opportunities.isEmpty()){    
                for(Opportunity oppr : opportunities){
                    if (oppr.OpportunityDetector__c == null){
                        oppr.OpportunityDetector__c = UserInfo.getUserID();
                    }
                }
            }
        }
        catch(Exception e){
            system.debug('Exception: ' + e);
        }
    System.Debug('****AP05_OpportunityTrigger DefaultOppDetector Ended ****'); 
    }   
    
    
    //The method inserts an Opportunity line for the corresponding opportunity with default values whose TECH_CrossProcessConversionAmount__c field is filled
    public static void creteOppLine(Map<Opportunity,STring> opps)
    {    
       
        
        Map<String, CS_ProductLineBusinessUnit__c> prodLneBUs = CS_ProductLineBusinessUnit__c.getAll(); 
        Map<String, STring> prodLneBU = new Map<String, String>();
        Map<String, STring> prodLnePL = new Map<String, String>();
        List<OPP_ProductLine__c> prodLnes = new List<OPP_ProductLine__c>();
        Map<String ,Id> sysIdRef = new Map<String, Id>();
               
        //Iterates through the Custom Settings and gets the Product BU and Product Line for the corresponding Opp Scope
        for(CS_ProductLineBusinessUnit__c pl: prodLneBUs.values())
        {
            for(String s: opps.Values())
            {
                if(pl.ProductLine__c.startsWith(s))        
                {
                    prodLneBU.put(s, pl.ProductBU__c);
                    prodLnePL.put(s, pl.ProductLine__c);
                    sysIdRef.put(s, pl.SystemIdReference__c);                    
                }
            }
        }
        //Creates Opportunity Lines populating Product BU and Product Line        
        for(Opportunity o: opps.keyset())
        {
            OPP_ProductLine__c prodLne = new OPP_ProductLine__c();
            prodLne.Amount__c = o.TECH_CrossProcessConversionAmount__c;            
            prodLne.ProductBU__c = prodLneBU.get(o.OpportunityScope__c);
            prodLne.LineStatus__c = Label.CLSEP12PRM20;
            prodLne.ProductLine__c = prodLnePL.get(o.OpportunityScope__c);
            prodLne.Product__c = sysIdRef.get(o.OpportunityScope__c);
            prodLne.Opportunity__c = o.Id;
            prodLne.CurrencyISOCode = o.CurrencyISOCode;
            prodLnes.add(prodLne);
        }
        //Inserts the Opportunity Line
        List<Database.SaveResult> prodLinesInsert = Database.insert(prodLnes, false);
    }
  
  //-----------------------------------------------------------------------------------------------------------------------------
 //START :Release MAY14
    //Description: validate partner program for Partner Called-In
    public static boolean validateAsssignedPrograms(List<Opportunity> newOppList)
    {
        map<id,Opportunity> mapOpp = new map<id,Opportunity>();
        Boolean validProgram;
        SET<ID> setOppAccIds = new SET<ID>();
                
        System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - Started *******');
        for(Opportunity aOpp :newOppList)
        {
            if(aOpp.TECH_Partner_Program__c != null && aOpp.PartnerInvolvement__c == System.label.CLMAY14PRM01) //'Partner Called-In'
               {
                mapOpp.put(aOpp.id,aOpp);
                setOppAccIds.add(aOpp.AccountID);
               } 
        
        }
        
        if(mapOpp.size() == 0)
            return false;
        
                
        List<OpportunityTeamMember> lstOppTeamMember = new List<OpportunityTeamMember>();
        //finding Opp Teams 
        lstOppTeamMember = [Select UserId,OpportunityId from OpportunityTeamMember where OpportunityId = :mapOpp.keySet() AND 
                                                         TeamMemberRole = :System.label.CLOCT13PRM13 Limit 20];                      
                                                         
        System.debug('****** AP05_OpportunityTrigger - Team Member - '+lstOppTeamMember.size());                                       
        Map<ID,List<ID>> MapUserIds = new Map<ID,List<ID>>();
        Map<ID,List<ID>> mapOppTeamContacts = new Map<ID,List<ID>>(); 
        SET<ID> setUserIds = new Set<ID>();
        
        //mapping Oppid & user Id
        for(OpportunityTeamMember aTeam :lstOppTeamMember)
        {
            if(MapUserIds.containsKey(aTeam.OpportunityId))
                MapUserIds.get(aTeam.OpportunityId).add(aTeam.UserId);
            else
                MapUserIds.put(aTeam.OpportunityId,new List<ID>{(aTeam.UserId)}); 
                
            setUserIds.add(aTeam.UserId);
        }
        Map<id,Opportunity> mapAccOpp = new Map<id,Opportunity>();  
        Map<id,Opportunity> mapContOpp = new Map<id,Opportunity>();
        Set<ID> SetPartAcc = new Set<ID>();
        
        //Find opp with team member and without team member
        for(Opportunity aOpp :mapOpp.values())
        {
            if(MapUserIds.containsKey(aOpp.id))
                mapContOpp.put(aOpp.id,aOpp);
            else
            {
                mapAccOpp.put(aOpp.id,aOpp);
                SetPartAcc.add(aOpp.AccountId);
                
            }   
        }  
        
         System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - Contact '+mapContOpp.size());
         System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - Account '+mapAccOpp.size());
        
        
        //Validate against Contact Assigned Program
        if(mapContOpp.size() !=0) //Primary partner Contact
        {
            List<User> lstUser = [Select user.id,user.ContactID from User where id in :setUserIds and ContactID != null];
            List<ContactAssignedProgram__c> lstContAssignedPrg = new List<ContactAssignedProgram__c>();
                    
            System.debug('****** AP05_OpportunityTrigger - Team Member - Contact -  '+lstUser.size()); 
            
            Map<Id,id> mapContacts = new Map<Id,id>();
            
            For(user aUser :lstUser)
                mapContacts.put(aUser.id,aUser.ContactID);  
                
             System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - user Team '+mapContacts.size());
        
            //Mapping Opp ID with contacts
            SET<ID> setTeamCont = new SET<ID>();
            //Mapping between Oppid & contact
            for(OpportunityTeamMember aTeam :lstOppTeamMember)
            {
                if(mapContacts.containsKey(aTeam.UserId))
                {
                    if(mapOppTeamContacts.containsKey(aTeam.OpportunityId))
                        mapOppTeamContacts.get(aTeam.OpportunityId).add(mapContacts.get(aTeam.UserId));
                    else    
                        mapOppTeamContacts.put(aTeam.OpportunityId,new List<ID>{mapContacts.get(aTeam.UserId)});
                        
                    setTeamCont.add(mapContacts.get(aTeam.UserId)); 
                    
                }   
            }
            
            System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - mapContOpp.values -'+mapOppTeamContacts.values());
                
            lstContAssignedPrg = [Select Contact__c,PartnerProgram__c,ProgramLevel__c from ContactAssignedProgram__c 
                                    where Contact__c in :setTeamCont and Active__c = true];      
    
            Map<ID,List<ContactAssignedProgram__c>> mapContAssPrg = new Map<id,List<ContactAssignedProgram__c>>();
            
            for(ContactAssignedProgram__c aContPrg :lstContAssignedPrg)
            {
                if(mapContAssPrg.containsKey(aContPrg.Contact__c))
                    mapContAssPrg.get(aContPrg.Contact__c).add(aContPrg);
                else
                    mapContAssPrg.put(aContPrg.Contact__c,new List<ContactAssignedProgram__c>{(aContPrg)});
            }
              
             System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - ContAssigned Prg values '+mapContAssPrg.values());  
                    
            
            //System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - ContAssigned Prg '+mapContAssPrg.size());
            
           // System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - mapContOpp.values -'+mapContOpp.values());
            
            for(Opportunity aOpp :mapContOpp.values())
            {
                validProgram = false;
                
                //System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - Oppid -'+aOpp.id);
               // System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - mapOppTeamContacts -'+mapOppTeamContacts.get(aOpp.id));
                
                for(ID aTeamCont :mapOppTeamContacts.get(aOpp.id)) //list of Team for an Opp
                {
                    if(mapContAssPrg.size() > 0)
                    {
                        for(ContactAssignedProgram__c aContAssPrg :mapContAssPrg.get(aTeamCont)) //for each assigned program
                        {
                            if(aOpp.ProgramLevel__c != null)
                                {   
                                    if(aContAssPrg.PartnerProgram__c == aOpp.TECH_Partner_Program__c &&  aContAssPrg.ProgramLevel__c == aOpp.ProgramLevel__c)
                                    {
                                        validProgram = true;
                                        break;
                                    }
                                }
                            else
                                {
                                    if(aContAssPrg.PartnerProgram__c == aOpp.TECH_Partner_Program__c )
                                    {
                                        validProgram = true;
                                        break;
                                  
                                    }
                            }                            
                            
                        }
                    }       
                        
                    if(validProgram == true)
                        break;
                        
                }
    
                if(validProgram == false)
                {
                    aOpp.addERROR(System.Label.CLMAY14PRM02);   
                    return false;
                }   
                
            }
                    
                            
        }
                
        //Account Assigned Program
        //IF there is no team member found then validate against Account Assigned Program
        
        if(mapAccOpp.size() !=0) //Primary partner Contact
        {

            List<ACC_PartnerProgram__c> lstAccAssignedPrg = new List<ACC_PartnerProgram__c>();
                     
            lstAccAssignedPrg = [Select Account__c,PartnerProgram__c,ProgramLevel__c 
                                    from ACC_PartnerProgram__c where Account__c = :SetPartAcc and Active__c = true];  
            
            map<Id,List<ACC_PartnerProgram__c>> mapAccProgram = new map<Id,List<ACC_PartnerProgram__c>>();
            
            for(ACC_PartnerProgram__c aAccPrg :lstAccAssignedPrg)
            {
                if(mapAccProgram.containsKey(aAccPrg.Account__c))
                    mapAccProgram.get(aAccPrg.Account__c).add(aAccPrg);
                else
                    mapAccProgram.put(aAccPrg.Account__c,new List<ACC_PartnerProgram__c>{(aAccPrg)});
            
            }
            
            System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms - Account Assigned Prg '+mapAccProgram.size());
            
            //Querying of Partner Account
            List<Account> lstPartnerAcc = [Select id from Account where id in :setOppAccIds and IsPartner = true];
            Map<Id,Id> mapAccount = new Map<Id,id>();
            for(Account aAcc :lstPartnerAcc)
                mapAccount.put(aAcc.id,aAcc.id);
            
            
            for(Opportunity aOpp :mapAccOpp.values())
            {
               if(mapAccount.containsKey(aOpp.Accountid))
                {
                    validProgram = false;
                    if(mapAccProgram.size() > 0)
                    {
                        for(ACC_PartnerProgram__c aAccPrg :mapAccProgram.get(aOpp.Accountid))
                        {
                        
                            if(aOpp.ProgramLevel__c != null)
                            {   
                                if(aAccPrg.PartnerProgram__c == aOpp.TECH_Partner_Program__c &&  aAccPrg.ProgramLevel__c == aOpp.ProgramLevel__c)
                                {
                                    validProgram = true;
                                    break;
                                }
                            }
                            else
                            {
                                if(aAccPrg.PartnerProgram__c == aOpp.TECH_Partner_Program__c)
                                {
                                    validProgram = true;
                                    break;
                                }
                            }
                        } 
                    }   

                    if(validProgram == false)
                    {
                        aOpp.addERROR(System.Label.CLMAY14PRM02);   
                        return false;
                    }   
                }
            }
        } // if(mapAccOpp.size() !=0)   
       
        System.debug('****** AP05_OpportunityTrigger - validateAsssignedPrograms -Completed *******');
        return true;
    
    } 
    //END: Release MAY14
    
   //Update Tech_AmoutUpdateByBatch__c field of the Master Project if the Opportunity is linked to the Master Project - Monalisa Project oct 2015   
    public static void updateProjectAmountFields(List<OPP_Project__C> projects,List<Opportunity> opplst){ 
        System.Debug('****AP05_OpportunityTrigger updateProjectAmountFields Started****');
        List<OPP_Project__c> prjupdatlist=new List<OPP_Project__c>();
        Map<Integer, OPP_Project__C> updOpps= new Map<Integer, OPP_Project__C>(); 
        Map<Id,OPP_Project__c> dupprjMAp=new Map<Id,OPP_Project__c>();
        Integer j=0; 

        for(OPP_Project__c prj:projects){
            System.debug('>>>>>Master Project Id to update for Batch>>>> '+prj.Id);
            if(prj.Tech_AmoutUpdateByBatch__c!=TRUE){
             if(dupprjMAp.containsKey(prj.Id)){}
                
            else{
                dupprjMAp.put(prj.Id,prj);        
                prj.Tech_AmoutUpdateByBatch__c=TRUE;
                prjupdatlist.add(prj);
                updOpps.put(j,prj);
                j++;
              }
            }
        }
        Database.SaveResult[] srList;
        if(prjupdatlist.size()>0)
            srList= Database.update(prjupdatlist, false);
         //To handle the error and display it better way 
        Integer i = 0;
        if(srList!=null){
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
    
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        //System.debug('The following error has occurred.');
                        //System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        Id failedprj = updOpps.get(i).Id;
    
                        for (Opportunity opp: opplst) {
                            if (opp.project__c== failedprj) {
                                opp.addError(err.getMessage());
                            }
                        }
                    }
                }
                i++;
            }
        }
         System.Debug('****AP05_OpportunityTrigger updateProjectAmountFields Completed ****');
    }   
}