@isTest
public class IDMSUserReconHandlerClassTest {
    
    static testmethod void userIdenEmailToReconTest(){
        ID profileId = [SELECT Id FROM Profile WHERE Name= 'SE - SRV Advanced User' limit 1].Id; 
        ID UserRoleID = [SELECT Id, Name FROM UserRole WHERE Name='CEO'].Id;
        User u = new User(firstName='testFirst',ProfileID=profileId , UserRoleId = UserRoleID , lastName='testLast', email ='testuser99@accenture.com', userName = 'testuser99@accenture.com'+Label.CLJUN16IDMS71, 
                          alias = 'testnewu', languagelocalekey = 'en_US',  localesidkey = 'en_US', emailEncodingKey = 'UTF-8', timeZoneSidKey = 'America/Los_Angeles');
        insert u;
        string resErrorMsg = 'Error in creation';
        IDMSUserReconciliationBatchHandler__c userToReconciliate = new IDMSUserReconciliationBatchHandler__c(AtCreate__c=true,AtUpdate__c=true, HttpMessage__c='http error from callout', IdmsUser__c=u.Id,NbrAttempts__c=1.0);
        IDMSUserReconHandlerClass.userIdenEmailToRecon(resErrorMsg, u.Id);
        IDMSUserReconHandlerClass.userIdentityPhoneToRecon(resErrorMsg, u.Id);
    }
}