/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | VFC60_GoogleLinks                                                                  |
|                       |                                                                                    |
|     - Object(s)       | Account, Contact                                                                   |
|     - Description     |   - Calculate the Google Map link, managing Carriage returns                       |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | October, 28th 2011                                                                 |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
public with sharing class VFC60_GoogleLinks
{
   //Constants
   private Static Final String GoogleMapRoot = System.Label.CL10069;

   //Variables
   public Id     currentId     {get;set;}
   public String currentObject {get;set;}

   //Default Controller
   public VFC60_GoogleLinks()
   {
      System.Debug('## >>> BEGIN: VFC60_GoogleLinks() <<<');

      this.currentId     = ApexPages.currentPage().getParameters().get('Id');
      this.currentObject = ApexPages.currentPage().getParameters().get('sobject');

      System.Debug('## >>>>>> this.currentId     = ' + this.currentId + ' <<<');
      System.Debug('## >>>>>> this.currentObject = ' + this.currentObject + ' <<<');

      System.Debug('## >>> END:   VFC60_GoogleLinks() <<<');
   }

   //Redirect to Google Map Link
   public PageReference redirectToGoogleMap()
   {
      System.Debug('## >>> BEGIN: VFC60_GoogleLinks.redirectToGoogleMap() <<<');

      //Variables
      String        GoogleMapURL = null;
      String        GoogleMapSea = '';
      PageReference retPage      = null;

      if (this.currentObject == 'Account')
      {
         List<Account> Accounts = [SELECT
                                      Id,
                                      Name,
                                      Street__c,
                                      City__c,
                                      ZipCode__c,
                                      StateProvince__r.Name,
                                      Country__r.Name
                                    FROM Account
                                    WHERE Id = :this.currentId];

         if (Accounts != null && !Accounts.isEmpty())
         {
            Account currentAccount = Accounts.get(0);
/*
            GoogleMapSea =          ((currentAccount.Street__c             != null) ? currentAccount.Street__c             : '')
                           + ', ' + ((currentAccount.City__c               != null) ? currentAccount.City__c               : '')
                           + ', ' + ((currentAccount.ZipCode__c            != null) ? currentAccount.ZipCode__c            : '')
                           + ', ' + ((currentAccount.StateProvince__r.Name != null) ? currentAccount.StateProvince__r.Name : '')
                           + ', ' + ((currentAccount.Country__r.Name       != null) ? currentAccount.Country__r.Name       : '');
*/
            //City, ZipCode and Country are mandatory on page layout
            GoogleMapSea =          ((currentAccount.Street__c             != null) ? currentAccount.Street__c             : '')
                           + ', ' + currentAccount.City__c
                           + ', ' + currentAccount.ZipCode__c
                           + ', ' + ((currentAccount.StateProvince__r.Name != null) ? currentAccount.StateProvince__r.Name : '')
                           + ', ' + currentAccount.Country__r.Name;
         }
      }
      else if (this.currentObject == 'Contact')
      {
         List<Contact> Contacts = [SELECT
                                      Id,
                                      Name,
                                      Street__c,
                                      City__c,
                                      ZipCode__c,
                                      StateProv__r.Name,
                                      Country__r.Name
                                    FROM Contact
                                    WHERE Id = :this.currentId];

         if (Contacts != null && !Contacts.isEmpty())
         {
            Contact currentContact = Contacts.get(0);
            GoogleMapSea =          ((currentContact.Street__c         != null) ? currentContact.Street__c         : '')
                           + ', ' + ((currentContact.City__c           != null) ? currentContact.City__c           : '')
                           + ', ' + ((currentContact.ZipCode__c        != null) ? currentContact.ZipCode__c        : '')
                           + ', ' + ((currentContact.StateProv__r.Name != null) ? currentContact.StateProv__r.Name : '')
                           + ', ' + ((currentContact.Country__r.Name   != null) ? currentContact.Country__r.Name   : '');
         }
      }

      if (GoogleMapSea != ', , , , ')
         GoogleMapSea = EncodingUtil.urlEncode(GoogleMapSea, 'UTF-8');
      else
         GoogleMapSea = '';
      
      GoogleMapURL = GoogleMapRoot + GoogleMapSea;
      
      System.Debug('## >>>>>> GoogleMapURL = ' + GoogleMapURL + ' <<<');

      retPage = new PageReference(GoogleMapURL);
      
      System.Debug('## >>> END  : VFC60_GoogleLinks.redirectToGoogleMap() <<<');
      return retPage;
   }
}