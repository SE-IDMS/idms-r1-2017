/*
Author:Siddharth Nagavarapu (sesa206167)
purpose: Batch to create account share based on account team member.
*/

global class Batch_enableAccessToAccountTeamMember implements Database.Batchable<sObject>{

   global Batch_enableAccessToAccountTeamMember(){

   }

   global Database.QueryLocator start(Database.BatchableContext BC){
       String query='SELECT AccountAccessLevel,AccountId,TeamMemberRole,UserId FROM AccountTeamMember where CreatedById=\'005A0000001PC0pIAG\' and AccountAccessLevel!=\'Edit\'';
          //and CreatedDate > LAST_N_DAYS:2
       System.debug('The query used is \n'+query);   
       return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
    List<AccountShare> tobeInsertedAccountShareRecords=new List<AccountShare>();
     for(sObject s : scope){
      AccountTeamMember accountTeamMemberRecord=(AccountTeamMember)s;
        tobeInsertedAccountShareRecords.add(new AccountShare(AccountAccessLevel='Edit',AccountId=accountTeamMemberRecord.AccountId,UserorGroupId=accountTeamMemberRecord.UserId));
     }
     insert tobeInsertedAccountShareRecords;     
    }
    
  
   global void execute(SchedulableContext sc) {
     
          Batch_enableAccessToAccountTeamMember batchInstance = new Batch_enableAccessToAccountTeamMember(); 
          database.executebatch(batchInstance,400);         

   }

   global void finish(Database.BatchableContext BC){
   }
}