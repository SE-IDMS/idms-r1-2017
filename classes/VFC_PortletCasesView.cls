Global class VFC_PortletCasesView implements AP_ObjectPaginatorListener {
        Id usid;// {get;set;}
    Id contid {get;set;}
    Id accid {get;set;} 
    List<Case> results;
    Public String ContactName{get;set;}
    Public String FirstName {get;set;}
    Public String lastname{get;set;}
    Public String Company{get;set;}
   
    //List <ticket> tickets {get;set;}
    List<Case> casess;
    Public Integer Count{get;set;}
    public String pcasesview;// {get;set;}
    public void setpcasesview (String s){this.pcasesview  = s;}
    public List<SelectOption> getpcasesview (){
        List<SelectOption> options = new List<SelectOption>();
        Options.add(new SelectOption('AC','All cases'));
        Options.add(new SelectOption('AOC','All open cases'));
        Options.add(new SelectOption('ACC','All closed cases'));
        options.add(new SelectOption('AMC','All My Cases'));        
        options.add(new SelectOption('MOC','All My Open Cases'));
        options.add(new SelectOption('MCC','All My Closed Cases'));
        
        return options;
    }
    public pageReference casesList(){
        paginator = new AP_ObjectPaginator(recordsPerPage ,this);
        all.clear();
        all=new List <ticket> ();
        all=  getTickets();
        System.debug('\n*******************************'+all.size());
        Count = all.size();
        paginator.setRecords(all);
        return null;
    } 
    // pagenation
    
   global List<ticket> tickets {get;private set;}   
    global AP_ObjectPaginator paginator {get;private set;}
    global List<ticket> all{get;private set;}
    Public List<ticket> allrecords {get;set;}
    public Integer recordsPerPage{get;set;}  
    public void setCasess(List<case> lc){this.casess = lc;}
    
    public List<Case> getCasess() {return null;}
    public boolean getIsAuthenticated (){
        return (IsAuthenticated());
    }
    public boolean IsAuthenticated (){
        return (! UserInfo.getUserType().equalsIgnoreCase('Guest'));
    }
    String selectedPcasesview {get; set;}
    public String getselectedPcasesview () {return selectedPcasesview ;}
    public void setselectedPcasesview (String pt) {this.selectedPcasesview = pt;}
    
    public Id getusid(){return this.usid;}
    public void setusid(Id id){this.usid = id;}
    
    public ID getcontid(){return this.contid;}
    public void setcontid(Id id){this.contid = id;}

    public ID getaccid (){return this.accid ;}
    public void setaccid (Id id){this.accid  = id;}

   // public String getpcasesview (){return this.pcasesview ;}
    //Constructor
    public VFC_PortletCasesView (){  
       User UserRec= [select id,firstname,lastname,CompanyName from user where id=:UserInfo.getUserId()];
       FirstName = UserRec.FirstName;
       LastName = UserRec.LastName;   
       Company = UserRec.CompanyName;                                             
        if (UserInfo.getUserType().equalsIgnoreCase('Guest')){
            
        }
        else{
            usid = UserInfo.getUserId();
            if(!test.isrunningTest()){
            if (usid != null){
                contid = [Select ContactId from User where Id =: usid LIMIT 100][0].ContactId;
                system.debug('ContactId'+contid);
                if ( contid != null){
                    accid = [Select AccountId from Contact where Id =: contid LIMIT 100][0].AccountId;
                }
            }
          }
          else{
           contid = [select Id,AccountId from Contact limit 1].Id;
           accid =  [Select AccountId from Contact Where Id=:contid limit 1].AccountId;
          }
        
        }
        recordsPerPage = integer.valueof(system.label.CLOCT15CCC31);//30
        tickets = new List <ticket> ();
        all = new List <ticket> ();
        allrecords= new List <ticket> ();
        casess = new List<Case>();
        casess = [SELECT Status, Id,Account.Name,Contact.Name,AccountId,ContactId, CaseNumber, Subject, Origin, CustomerRequest__c,Site__r.name,CustomerRequestedDate__c,Priority, CreatedDate, IsClosed, closedDate, HasCommentsUnreadByOwner, ActionNeededFrom__c FROM Case Where AccountId=:accid and ProductLine__c='IDFLD - FIELD DEVICES' ORDER BY CreatedDate DESC LIMIT 100 ]; // AND CreatedDate >= LAST_N_DAYS:60      
        //casess = [SELECT Status, Id,Account.Name,Contact.Name,AccountId,ContactId, CaseNumber, Subject, Origin, CustomerRequest__c,Site__r.name,CustomerRequestedDate__c,Priority, CreatedDate, IsClosed, closedDate, HasCommentsUnreadByOwner, ActionNeededFrom__c FROM Case  WHERE ContactId =: contid ORDER BY CreatedDate DESC LIMIT 100 ]; // AND CreatedDate >= LAST_N_DAYS:60 
        System.debug('\n*******************************'+casess.size() );
        all=  setTicket(casess);
        paginator = new AP_ObjectPaginator(recordsPerPage ,this);
        paginator.setRecords(all);
        Count = allrecords.size();
        
    }
    public List<ticket> setTicket (List<case> cases){
        List<ticket> tick = new List<ticket>();
        if (cases != null){
            for (Case c : cases){
                ticket tt = new ticket();
                tt.id = c.id;
                tt.CaseNumber  = c.CaseNumber;
                tt.Subject     = c.Subject;
                tt.Status = c.status;
                tt.CreatedDate = c.CreatedDate.format();
                tt.Account= c.Account.name;
                tt.Contact= c.Contact.name; 
                tt.Priority= c.Priority;
                tt.targetdate =c.CustomerRequestedDate__C;
                tt.HasUnreadComment = c.HasCommentsUnreadByOwner;  
                if (c.ClosedDate != null){
                    if (c.status != null && c.status.equalsIgnoreCase('In Progress')){
                        tt.ClosedDate = null;
                    }
                    else{
                        tt.ClosedDate = c.ClosedDate.format();
                    }
                }
                else{
                    if (c.status != null && c.status.equalsIgnoreCase('Closed')){
                        tt.ClosedDate = c.CreatedDate.format();
                    }   
                }
                tick.add(tt);          
            }        
        }return tick;
    }  
    public List<ticket> getTickets() {
    
        List<Case> AllclosedCases = new List<Case>();
        List<Case> AllnewCases = new List<Case>();
        List<Case> AllopenCases = new List<Case>();
        List<Case> AllwaitingCases = new List<Case>();
        List<Case> AllinProgressCases = new List<Case>();
        List<Case> AllorderedCases = new List<Case>();
        List<Case> MyclosedCases = new List<Case>();
        List<Case> MynewCases = new List<Case>();
        List<Case> MyopenCases = new List<Case>();
        List<Case> MywaitingCases = new List<Case>();
        List<Case> MyinProgressCases = new List<Case>();
        List<Case> MyorderedCases = new List<Case>();
        ticket tik = new ticket ();
        List <ticket> OrderedTickets = new List <ticket>();
        List <ticket> openTickets        = new List <ticket>();
        List <ticket> closedTickets = new List <ticket>();
       
        System.debug('-->>Cases'+casess);
        System.debug('-->>Size'+casess.size());

        if (casess != null){
            for (Case c : casess){                     
              if      (c.status.equalsIgnoreCase('New'))       {c.status = 'New';}
              else if (c.status.equalsIgnoreCase('Cancelled')) {c.status = 'Closed';}
              else if (c.status.equalsIgnoreCase('Closed'))    {c.status = 'Closed';}
              else if (c.ActionNeededFrom__c != null && c.ActionNeededFrom__c.equalsIgnoreCase('Customer')) {c.status = 'Waiting for Customer';} 
              else c.status = 'In Progress'; 
            }
         System.debug('-->>Cases'+casess);
            for (Case c : casess){
                String caseConId = String.valueOf(c.ContactId);
                 
                String UserConId = String.valueOf(contid);
                system.debug('caseConId'+caseConId);
                system.debug('UserConId'+UserConId);
                
            if(caseConId.equals(UserConId )){
                   if      (c.status == 'Closed')                {MyclosedCases.add(c);}
                 else if (c.status == 'New')                   {MynewCases.add(c);}
                 else if (c.status == 'Waiting for Customer' ) {MywaitingCases.add(c);}
                 else                                          {MyinProgressCases.add(c);}
                
                 }
               
                    if      (c.status == 'Closed')                {AllclosedCases.add(c);}
                     else if (c.status == 'New')                   {AllnewCases.add(c);}
                     else if (c.status == 'Waiting for Customer' ) {AllwaitingCases.add(c);}
                     else                                          {AllinProgressCases.add(c);}
                 
            } 
            System.debug('-->>closedCases'+AllclosedCases);
            System.debug('-->>newCases'+AllnewCases);
            System.debug('-->>waitingCases'+AllwaitingCases); 
            System.debug('-->>inProgressCases'+AllinProgressCases);         
           if(MywaitingCases != null) {
               for(case t : MywaitingCases){
                   myorderedCases.add (t);
                   myopenCases.add(t); 
               }
               System.debug('openCases'+AllopenCases);
           }
           if (MynewCases != null) { 
               for(case t :mynewCases){
                   myorderedCases.add (t);  
                   myopenCases.add(t);
               } 
               System.debug('openCases'+AllopenCases);
           }
           if(MyinProgressCases != null) {
               for(case t : myinProgressCases){
                   myorderedCases.add (t);
                   myopenCases.add(t);
               }
           }
           if(myclosedCases != null){ 
               for (case t : myclosedCases){
                   myorderedCases.add (t);
                   System.debug('orderedCases'+myorderedCases);
               }
           }
           if(AllwaitingCases != null) {
               for(case t : AllwaitingCases){
                   AllorderedCases.add (t);
                   AllopenCases.add(t); 
               }
               System.debug('openCases'+AllopenCases);
           }
           if (AllnewCases != null) { 
               for(case t :AllnewCases){
                   AllorderedCases.add (t);  
                   AllopenCases.add(t);
               } 
               System.debug('openCases'+AllopenCases);
           }
           if(AllinProgressCases != null) {
               for(case t : AllinProgressCases){
                   AllorderedCases.add (t);
                   AllopenCases.add(t);
               }
           }
           if(AllclosedCases != null){ 
               for (case t : AllclosedCases){
                   AllorderedCases.add (t);
                   System.debug('orderedCases'+myorderedCases);
               }
           }             

            if (selectedpcasesview == 'MOC') {
                openTickets = setTicket(MyopenCases);
                System.debug('openTickets'+openTickets);
                return openTickets ;
            }
            else if (selectedpcasesview == 'MCC') {
                closedTickets = setTicket(MyclosedCases);
                System.debug('closedTickets'+closedTickets);
                return closedTickets ;
            }
            else if(selectedpcasesview == 'AMC') {
                OrderedTickets = setTicket(myorderedCases);
                System.debug('OrderedTickets'+OrderedTickets);
                return OrderedTickets;
            }
            else if(selectedpcasesview == 'AOC'){
              openTickets = setTicket(AllopenCases);
                System.debug('openTickets'+openTickets);
                return openTickets ;
            }
            else if(selectedpcasesview == 'ACC'){
            closedTickets = setTicket(AllclosedCases);
                System.debug('closedTickets'+closedTickets);
                return closedTickets ;
            }
            else{
              OrderedTickets = setTicket(AllorderedCases);
                System.debug('OrderedTickets'+OrderedTickets);
                return OrderedTickets;
            }

        }
        
       
        return null;
    }
    Global String getrecordsDisplay(){
        System.Debug('****** Displaying the number of start & end of the record Begins ******');
        string results;
        if(all.size() == 0){
                results = 0+'';
        } 
        else if((paginator.pageNumberDisplayFriendly==paginator.pageCount) && all.size()>0){
            System.debug('Entered in 2rd Block');
            results= ' ('+((paginator.pageNumberDisplayFriendly-1)*recordsPerPage+1) + ' - '+ all.size()+') ';
        }
         
        else if((all.size()>recordsPerPage)&& all.size()>0){
            System.debug('Entered in 3rd Block');
            results= ' ('+((paginator.pageNumberDisplayFriendly-1)*recordsPerPage+1) + ' - '+((paginator.pageNumberDisplayFriendly)*recordsPerPage)+') ';
        }  
        
        else if(all.size()>0){
            System.debug('Entered in 4rd Block');
            results= ' (1 - '+ all.size() + ') ';
        }
        system.debug('------results-----'+results);
        return results;
    }
    Public void setrecordsDisplay(){
    }
    global void handlePageChange(List<Object> newPage){
        tickets.clear();
        if(newPage != null){
            for(Object obj : newPage){
                tickets.add((ticket)obj );
            }
        }
    }
    public class ticket{
        public Id id  {get; set;}
        public String CaseNumber  {get; set;}
        public String Subject     {get; set;}
        public String Status      {get; set;}
        public String CreatedDate {get; set;}
        public String ClosedDate  {get; set;}
        public Boolean HasUnreadComment{get; set;}
        Public string Account{get;set;}
        Public string Contact{get;set;}
        Public String Site{get;set;}
        Public String Priority{get;set;}
        Public date targetdate{get;set;}

    }
    

 }