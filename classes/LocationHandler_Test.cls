@isTest
private class LocationHandler_Test{
    static Integer counter = 1;
	static testMethod void myUnitTest(){

        List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0){
            User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrLoc1');
            usr.BypassWF__c = false;
            usr.ProgramAdministrator__c = true;
            usr.UserRoleId = '00EA0000000MiDK';
            
            
            System.runas(usr) {
            
                Country__c aCountry = Utils_TestMethods.createCountry();
                
                aCountry.countrycode__c ='TLH';
                insert aCountry;
                System.debug('## Country inserted: '+aCountry);
                
                Account acc = Utils_TestMethods.createAccount();
                acc.country__c = aCountry.id;
                acc.Street__c = '123 Park Street';
                acc.City__c = 'Paris';
                insert acc;
                System.debug('## Account inserted: '+acc);
                
                List<SVMXC__Site__c> locations = new List<SVMXC__Site__c>();
                
                SVMXC__Site__c location = createSVMXCLocation(acc, true);
                locations.add(location);
                System.debug('## Primary location: '+location);
                
                for (Integer i = 1; i <= 10 ; i++) {
                    location = createSVMXCLocation(acc, false);
                    locations.add(location);
                    System.debug('## Location '+i+': '+location);
                }
                System.debug('## Locations list size :' + locations.size());
                
                insert locations;
                System.debug('## Locations to be inserted:' + locations);
//                String query = 'SELECT Id, Name, SVMXC__Street__c, SVMXC__City__c, StateProvince__c, SVMXC__Zip__c, LocationCountry__c, SVMXC__Account__c, SVMXC__Account__r.Name, SVMXC__Account__r.Street__c, SVMXC__Account__r.City__c, SVMXC__Account__r.StateProvince__c, SVMXC__Account__r.ZipCode__c, SVMXC__Account__r.Country__c FROM SVMXC__Site__c WHERE TECH_isAddressSynchedWithAccount__c = False limit 200';
//                String query = 'SELECT Id, Name, SVMXC__Street__c, SVMXC__City__c, StateProvince__c, SVMXC__Zip__c, LocationCountry__c, SVMXC__Account__c, SVMXC__Account__r.Name, SVMXC__Account__r.Street__c, SVMXC__Account__r.City__c, SVMXC__Account__r.StateProvince__c, SVMXC__Account__r.ZipCode__c, SVMXC__Account__r.Country__c, TECH_isAddressSynchedWithAccount__c FROM SVMXC__Site__c WHERE id IN ('+locations+') limit 200';
                
                List<SVMXC__Site__c> insertedLocations = [SELECT Id, Name, SVMXC__Street__c, SVMXC__City__c, StateProvince__c, SVMXC__Zip__c, LocationCountry__c, SVMXC__Account__c, SVMXC__Account__r.Name, SVMXC__Account__r.Street__c, SVMXC__Account__r.City__c, SVMXC__Account__r.StateProvince__c, SVMXC__Account__r.ZipCode__c, SVMXC__Account__r.Country__c, TECH_isAddressSynchedWithAccount__c FROM SVMXC__Site__c WHERE id IN :locations limit 200];
                System.debug('## Inserted Locations:' + insertedLocations);

                Integer i = 1;
                for (SVMXC__Site__c currentLoc : insertedLocations) {
                    currentLoc.SVMXC__Street__c = currentLoc.SVMXC__Street__c + ' ' + i;
                    currentLoc.SVMXC__City__c = currentLoc.SVMXC__City__c + ' ' + i;
                    System.debug('## Current Location '+i+': '+currentLoc);
                    i++;
                }
                update insertedLocations;

                
                System.debug('## Updated Locations:' + insertedLocations);
            }
            
            Test.startTest();
            BatchLocationAddressAlignmentWithAccount updateLocationAddresses = new BatchLocationAddressAlignmentWithAccount();
            Database.executeBatch(updateLocationAddresses);
            Test.stopTest();
        }
        
    }
    
    static SVMXC__Site__c createSVMXCLocation(Account anAccount, Boolean isPrimaryLocation) {
        SVMXC__Site__c result = null;
        if (anAccount != null) {
            result = new SVMXC__Site__c();
            result.Name = 'TEST LOCATION ' + counter;
            result.SVMXC__Street__c = anAccount.Street__c;
            result.SVMXC__City__c = anAccount.City__c;
            result.StateProvince__c = anAccount.StateProvince__c;
            result.SVMXC__Zip__c = anAccount.ZipCode__c;
            result.LocationCountry__c = anAccount.Country__c;
            result.SVMXC__Account__c = anAccount.Id;
            result.PrimaryLocation__c = isPrimaryLocation;
            result.SVMXC__Latitude__c = 100;
            result.SVMXC__Longitude__c = 200;

            counter ++;
        }
        return result;
    }
     
}