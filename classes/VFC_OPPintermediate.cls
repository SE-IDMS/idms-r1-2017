/*
    June Q2 2016 Release - Intermediate Page for services - BR-9581
*/
public with sharing class VFC_OPPintermediate 
{
    public Opportunity opp;
    public string selectedvalue{get;set;}
    public VFC_OPPintermediate(ApexPages.StandardController controller) 
    {
        opp=(Opportunity)controller.getrecord();
    }
    Public pagereference RedirecPMOpage()
    {
          Id uid= userinfo.getuserid();
          user u = [select id,name,ServiceMaxUser__c from user where id =:uid];
          
        //Boolean smaxuser = Userinfo.isCurrentUserLicensed('SVMXC');
       // if(smaxuser==false)
           if(u.ServiceMaxUser__c == false)
            {
                 if(opp.Id==null && ApexPages.currentPage().getParameters().get('isdtp')!=null && ApexPages.currentPage().getParameters().get('isdtp').right(18).startsWith('006')) //for Service Cloud Console
                {
                    System.debug('>>>> Id passed to console>>'+ApexPages.currentPage().getParameters().get('isdtp').right(18));
                    opp=new Opportunity(Id=ApexPages.currentPage().getParameters().get('isdtp').right(18));
                }
                 string url='';
                 url= '/apex/VFP_AddUpdateProductsForOpportunity?id='+opp.id;
                 pagereference pgf= new pagereference(url);
                    return pgf;
            }
        else
            {
                return null;
            }
    }
    Public pagereference RedirecSearchpage()
    {    
        if(opp.Id==null && ApexPages.currentPage().getParameters().get('isdtp')!=null && ApexPages.currentPage().getParameters().get('isdtp').right(18).startsWith('006')) //for Service Cloud Console
        {
            System.debug('>>>> Id passed to console>>'+ApexPages.currentPage().getParameters().get('isdtp').right(18));
            opp=new Opportunity(Id=ApexPages.currentPage().getParameters().get('isdtp').right(18));
        }
        if(selectedvalue=='By PMO Selection')
        {
            string url='';
            url= '/apex/VFP_AddUpdateProductsForOpportunity?id='+opp.id;
            pagereference pgf= new pagereference(url);
            return pgf;
        }
        else if(selectedvalue=='By Installed Product')
        {
            string url='';
            url='/apex/VFP_SearchInstalledProduct?objid='+opp.id +'&sobj=OPP';
            pagereference pgf= new pagereference(url);
            return pgf;
        }
        else
        {
            return null;
        }
    }
}