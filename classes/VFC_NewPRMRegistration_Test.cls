@isTest
public Class VFC_NewPRMRegistration_Test {

    @testSetup static void testSetupMethod() {
        
        Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
  
            INSERT testCountry;
            
             StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
        INSERT testStateProvince;
        
        Account newAcc = new Account();
            newAcc.RecordTypeId = System.Label.CLAPR15PRM025;  
            newAcc.Name  =  'NewAccount';
            newAcc.PRMCompanyName__c = 'NewCompanyName';
            newAcc.PRMStreet__c       = 'Elnath Street ';
            newAcc.PRMAdditionalAddress__c = 'Marathalli';
            newAcc.PRMCity__c              = 'Bangalore';
          
            newAcc.PRMCompanyPhone__c    = '987654321';
            newAcc.PRMWebsite__c         = 'Https://schneider-electric.com';
            newAcc.PRMZipCode__c         = '560103';
            newAcc.PRMCorporateHeadquarters__c = true;
            newAcc.PRMUIMSID__c = '123456';
            newAcc.PRMBusinessType__c = 'FI';
            newAcc.PRMAreaOfFocus__c  = 'FI1';
            newAcc.PRMCountry__c = testCountry.Id;
        INSERT newAcc;      
            
        PRMCountry__c prmCountry = new PRMCountry__c();
            prmCountry.CountryPortalEnabled__c = True;
            prmCountry.MemberCountry1__c = testCountry.Id;
            prmCountry.PLDatapool__c = 'datapool';
        INSERT prmCountry;
            
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        INSERT cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
        INSERT cLChild; 
            
        CountryChannels__c cChls = new CountryChannels__c();
            cChls.Channel__c = cLC.Id;
            cChls.SubChannel__c = cLChild.Id;
            cChls.Country__c = testCountry.Id;
            cChls.Active__c = True; 
            cChls.PRMCountry__c = prmCountry.Id;
        INSERT cChls; 
        List<CS_ContactLanguageToUserLanguage__C> ContactLangToUse = new List<CS_ContactLanguageToUserLanguage__C>();
        ContactLangToUse.add(new CS_ContactLanguageToUserLanguage__C(UserLanguageKey__c = 'en_US', Name = 'EN'));
        insert ContactLangToUse;
    }


    public static testMethod void testMethod1() {
            
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
       UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
       User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                        
                         BypassVR__c = True);
        
        
       System.runAs(u) { 
       
           Account newAcc = [SELECT Id FROM Account WHERE PRMCompanyName__c=:'NewCompanyName'];
           //Contact newCon = [SELECT Id,PRMPrimaryContact__c FROM Contact WHERE lastName =: 'lastNameContact'];
           Country__c testCountry = [SELECT Id FROM Country__c WHERE Name=:'India'];
           Stateprovince__c testStateProvince = [SELECT Id FROM StateProvince__c WHERE Name=:'Karnataka'];
           
           Contact newCon = new Contact();
            newCon.FirstName = 'firstNameContact'; 
            newCon.LastName = 'lastNameContact';
            newCon.PRMUIMSID__c = '123456';
            newCon.AccountId = newAcc.Id;
            newCon.PRMContact__c = True;
            newCon.PRMPrimaryContact__c = True;
            newCon.Email = '';
            newCon.PRMEmail__c = 'test123@abc.com';
            newCon.PRMWorkPhone__c = '9876543210';
        INSERT newCon;
            
           Test.StartTest();
               PageReference pageRef = Page.VFP_NewPRMRegistration;
               Test.setCurrentPage(pageRef);
               ApexPages.currentPage().getParameters().put('accid',newAcc.Id);
               
               AP_UserRegModel regModel = new AP_UserRegModel();
               regModel.UserLanguage = 'en';
               regModel.AccountId = newAcc.Id;
               regModel.UserLanguage = 'en';
               regModel.Email = 'test123@abc.com';
               regModel.CompanyCountry = testCountry.Id;
               regModel.companyStateProvince = testCountry.Id;
               regModel.ContactId = newCon.Id;
               regModel.businessType = 'FI';
               regModel.areaOfFocus  = 'FI1';
               regModel.isPrimaryContact = false;
               regModel.SkippedCompanyStep = true;
               
               VFC_NewPRMRegistration.NewRegistrationInfo regInfo = new VFC_NewPRMRegistration.NewRegistrationInfo();
               regInfo.UserRegInfo = regModel;
               
               VFC_NewPRMRegistration prmReg = new VFC_NewPRMRegistration();
               prmReg.RegistrationInfo = regInfo;
               
               PageReference pg = prmReg.SaveRegistration();
               Boolean emailExist = prmReg.EmailExistsUIMS;
               Boolean notPRMAcc = prmReg.NotPRMAccount;
              
           Test.StopTest();
       }
    }
       
    public static testMethod void testMethod2() {
        
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
       UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
       User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) { 
            
           Account newAcc = [SELECT Id FROM Account WHERE PRMCompanyName__c=:'NewCompanyName'];
           //Contact newCon = [SELECT Id FROM Contact WHERE lastName =: 'lastNameContact'];
           Country__c testCountry = [SELECT Id FROM Country__c WHERE Name=:'India'];
           Stateprovince__c testStateProvince = [SELECT Id FROM StateProvince__c WHERE Name=:'Karnataka'];
           
           Contact newCon123 = new Contact();
            newCon123.FirstName = 'firstNameContact12'; 
            newCon123.LastName = 'lastNameContact';
            newCon123.PRMUIMSID__c = '123456';
            newCon123.AccountId = newAcc.Id;
            newCon123.PRMContact__c = True;
            newCon123.PRMPrimaryContact__c = True;
            newCon123.Email = '';
            newCon123.PRMEmail__c = 'test123@abc.com';
            newCon123.PRMWorkPhone__c = '9876543210';
        INSERT newCon123;
        
           Test.StartTest();
               PageReference pageRef = Page.VFP_NewPRMRegistration;
               Test.setCurrentPage(pageRef);
               ApexPages.currentPage().getParameters().put('accid',newAcc.Id);
               
               AP_UserRegModel regModel = new AP_UserRegModel();
               regModel.UserLanguage = 'en';
               regModel.AccountId = newAcc.Id;
               regModel.UserLanguage = 'en';
               regModel.Email = 'test123@abc.com';
               regModel.CompanyCountry = testCountry.Id;
               regModel.companyStateProvince = testStateProvince.Id;
               regModel.ContactId = newCon123.Id;
               regModel.businessType = 'FI';
               regModel.areaOfFocus  = 'FI1';
               
               VFC_NewPRMRegistration.NewRegistrationInfo regInfo = new VFC_NewPRMRegistration.NewRegistrationInfo();
               regInfo.UserRegInfo = regModel;
               regInfo.ContactModeSelected = 'EXISTINGCONTACT';
               
               VFC_NewPRMRegistration prmReg = new VFC_NewPRMRegistration();
               prmReg.RegistrationInfo = regInfo;
               
               PageReference pg = prmReg.SaveRegistration();
                     
           Test.StopTest();
       }
       
       
    }
       
    public static testMethod void testMethod3() {
        
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
       UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
       User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) { 
           
           Account newAcc = [SELECT Id FROM Account WHERE PRMCompanyName__c=:'NewCompanyName'];
           //Contact newCon1 = [SELECT Id FROM Contact WHERE lastName =: 'lastNameContact1'];
           Country__c testCountry = [SELECT Id FROM Country__c WHERE Name=:'India'];
           Stateprovince__c testStateProvince = [SELECT Id FROM StateProvince__c WHERE Name=:'Karnataka'];
           
           Contact newCon1 = new Contact();
            newCon1.FirstName = 'firstNameContact1'; 
            newCon1.LastName = 'lastNameContact1';
            newCon1.PRMUIMSID__c = '123456890';
            newCon1.AccountId = newAcc.Id;
            newCon1.PRMContact__c = True;
            newCon1.Email = '';
            newCon1.PRMEmail__c = 'test123@abc123.com';
            newCon1.PRMMobilePhone__c = '987654321012';
            INSERT newCon1;
            
           Test.StartTest();
               PageReference pageRef = Page.VFP_NewPRMRegistration;
               Test.setCurrentPage(pageRef);
               ApexPages.currentPage().getParameters().put('accid',newAcc.Id);
               
               AP_UserRegModel regModel = new AP_UserRegModel();
               regModel.UserLanguage = 'en';
               regModel.AccountId = newAcc.Id;
               regModel.Email = 'test123@abc123.com';
               regModel.CompanyCountry = testCountry.Id;
               regModel.companyStateProvince = testStateProvince.Id;
               regModel.ContactId = newCon1.Id;
               regModel.businessType = 'FI';
               regModel.areaOfFocus  = 'FI1';
               regModel.isPrimaryContact = true;
               
               VFC_NewPRMRegistration.NewRegistrationInfo regInfo = new VFC_NewPRMRegistration.NewRegistrationInfo();
               regInfo.UserRegInfo = regModel;
               regInfo.ContactModeSelected = 'EXISTINGCONTACT';
               
               VFC_NewPRMRegistration prmReg = new VFC_NewPRMRegistration();
               prmReg.RegistrationInfo = regInfo;
               
               PageReference pg = prmReg.SaveRegistration();
               
               regInfo.ContactModeSelected = 'CNTMODE_UIMSEXT';
               prmReg.RegistrationInfo = regInfo;
               pg = prmReg.SaveRegistration();
               
           Test.StopTest();
       }
       
       
    }
       
    public static testMethod void testMethod4() {
        
       Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
       UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
       User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) { 
           
           Account newAcc = [SELECT Id,PRMCountry__c,PRMBusinessType__c,PRMAreaOfFocus__c FROM Account WHERE PRMCompanyName__c=:'NewCompanyName'];
       
           Test.StartTest();       
               PageReference pageRef = Page.VFP_NewPRMRegistration;
               Test.setCurrentPage(pageRef);
               ApexPages.currentPage().getParameters().put('accid',newAcc.Id);
               
               AP_UserRegModel regModel = new AP_UserRegModel();
               regModel.UserLanguage = 'en';
               
               VFC_NewPRMRegistration.NewRegistrationInfo regInfo = new VFC_NewPRMRegistration.NewRegistrationInfo();
               regInfo.UserRegInfo = regModel;
               regInfo.NewAccount = newAcc;
               
               VFC_NewPRMRegistration prmReg = new VFC_NewPRMRegistration();
               prmReg.RegistrationInfo = regInfo;
               
               PageReference pg2 = prmReg.GetAvailableChannels();
               PageReference pg1 = prmReg.GetAvailableSubChannels();
           Test.StopTest();
       }  
    }
}