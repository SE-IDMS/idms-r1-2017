public class VFC_MassUploadComponents {

    public StringListWrapper csvList{get;set;}{csvList=new StringListWrapper();} //the wrapper which is exchanged between componentcontroller and current extension
    public Map<String,CS_ComponentCSVMap__c> csvNameAPINameMap = new Map<String,CS_ComponentCSVMap__c>();
    public List<ComponentWrapper> ComponentWrapperRecords{get;set;}{ComponentWrapperRecords=new List<ComponentWrapper>();}
    public List<Component__c> comp {get; set;}
    public Boolean flag {get; set;} // used for rendering
    public Boolean flagHR {get; set;} // used for rendering the Header Row details
    public ID ReleaseID {get; set;} //ID of the Release record from where button is clicked
    public List<CS_ComponentCSVMap__c> HR1 {get; set;} //Header Row values
    
    public VFC_MassUploadComponents(ApexPages.StandardSetController controller) {
        this.ReleaseID = Apexpages.currentPage().getParameters().get('id'); //gets the record id from the url of the page
        flag = false;
        flagHR = true;
        System.debug('!!! csvList 1 : ' + csvList);
    }
    
    public List<CS_ComponentCSVMap__c> getHeaderRow() {
    
        HR1 = new List<CS_ComponentCSVMap__c>();
        
        CS_ComponentCSVMap__c h1 = [SELECT Name FROM CS_ComponentCSVMap__c WHERE Name = 'Component Type'];
        System.debug('**** h1' + h1);
        HR1.add(h1);
        CS_ComponentCSVMap__c h2 = [SELECT Name FROM CS_ComponentCSVMap__c WHERE Name = 'Track'];
        HR1.add(h2);
        CS_ComponentCSVMap__c h3 = [SELECT Name FROM CS_ComponentCSVMap__c WHERE Name = 'Object Name'];
        HR1.add(h3);
        CS_ComponentCSVMap__c h4 = [SELECT Name FROM CS_ComponentCSVMap__c WHERE Name = 'Field Name'];
        HR1.add(h4);
        CS_ComponentCSVMap__c h5 = [SELECT Name FROM CS_ComponentCSVMap__c WHERE Name = 'Type'];
        HR1.add(h5);
        CS_ComponentCSVMap__c h6 = [SELECT Name FROM CS_ComponentCSVMap__c WHERE Name = 'Owner'];
        HR1.add(h6);
        CS_ComponentCSVMap__c h7 = [SELECT Name FROM CS_ComponentCSVMap__c WHERE Name = 'Release'];
        HR1.add(h7);
        
        return HR1;
    }
    
    public List<ComponentWrapper> getcomponentlist() {
        
        flagHR = false;
        System.debug('<<<<<<<<<< componentlist method is called') ;
        
        for(CS_ComponentCSVMap__c cs: [SELECT Name,APIName__c,isRequired__c FROM CS_ComponentCSVMap__c])
              csvNameAPINameMap.put(cs.Name,cs); 
              
        System.debug('!!! csvList 2 : ' + csvList);
                        
        if(csvList.stringList.size()>0) { //No. of rows/records that have to be uploaded
              List<String> headerRow=new List<String>();                      
              headerRow=csvList.stringList[0]; //headers of the various fields
                            
              ComponentWrapperRecords.clear();
              for(Integer rowcount=1;rowcount<csvList.stringList.size();rowcount++) { //loop for going through each row of the csv file(excel)
                    List<String> cells=csvList.stringList[rowcount]; //stores one particular row of the csv file(excel)
                    Component__c tempcomp = new Component__c();
                    
                    for(Integer cellcount=0;cellcount<cells.size();cellcount++) { //loop for going through each column of one particular row
                        //the name is retreived from headerRow[cellcount] and the corresponding value is retreived from cells[cellcount]    
                          
                        tempcomp.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,cells[cellcount]);
                        
                        if(tempcomp.Release__c == null)
                            tempcomp.Release__c = ReleaseID;
                        
                        if(tempcomp.Owner__c == null)
                            tempcomp.Owner__c = UserInfo.getUserId(); //current user
                    }
                                        
                    ComponentWrapperRecords.add(new ComponentWrapper(tempcomp,'Yet to Insert'));
              }                                     
        }
                
        return ComponentWrapperRecords ;
    }
    
    public PageReference Save() {
        List<Component__c> complist = new List<Component__c>();
        
        for( ComponentWrapper cw : ComponentWrapperRecords ) 
            complist.add(cw.compitem); 
        
        try {
            
            INSERT complist; 
             
            PageReference savepage = new PageReference('/'+ReleaseID);
            return savepage;  
        }
        catch(DmlException D) {
            ApexPages.Message DML = new ApexPages.Message(ApexPages.Severity.ERROR,D.getMessage());
            ApexPages.addMessage(DML); 
            return null;   
        }
        catch(Exception E) {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.Severity.ERROR,E.getMessage());
            ApexPages.addMessage(errormsg);
            return null; 
        }
        
    }
    
    public PageReference Cancel() {
        flag = false;
        PageReference cancelpage = new PageReference('/'+ReleaseID);
        return cancelpage;
    }
    
    class ComponentWrapper {
        public Component__c compitem{get;set;}
        public String status{get;set;}          
        public String message{get;set;}
        ComponentWrapper(Component__c compitem,String status) {
            this.compitem=compitem;
            this.status=status;
        }           
    }
}