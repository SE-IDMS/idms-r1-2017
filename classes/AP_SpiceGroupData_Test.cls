/*
Author:Siddharatha N (bFO solution team)
Class:AP_SpiceGroupData_Test
Purpose:contains the unit test scripts for preventing the posts and comments to Collaboration Group(chatter groups)
*/
@isTest
private class AP_SpiceGroupData_Test{

    static testMethod void testValidateSpiceGrpupFunctionality(){        
        Test.startTest();
        //create a collaboration group
        CollaborationGroup tstCollaborationGroup =new CollaborationGroup(CollaborationType='Public',Name='SpiceGroupData Test Group',Description='SpiceGroupData Group Description');
        insert tstCollaborationGroup;
        //insert a record in spiceGroupData
        //positive test case
        SpiceGroupData__c tstSpiceGroupData=new SpiceGroupData__c(CommentsAllowedInChatter__c=false,GroupId__c=tstCollaborationGroup.id,Name='SpiceGroupData Test Group',PostsAllowedinChatter__c=false);
        insert tstSpiceGroupData;                
        
        //unit test method to validate id without having to query it(just verifying a possiblity)
        List<SpiceGroupData__c> tmpLstSpiceGroupData=new List<SpiceGroupData__c>();
        tmpLstSpiceGroupData.add(tstSpiceGroupData);
        //AP_SpiceGroupData.ValidateGroupId(tmpLstSpiceGroupData);                
                
        Test.stopTest();
    }    
}