/****************************************************************************/
//Author         :    Pooja Suresh
//Description    :    Testing Controller that marks duplicate contacts 
//Release        :    May 2013
/****************************************************************************/
@isTest
private class VFC_ContactMarkAsDuplicate_TEST
{
     
     static Account acc;
     static PageReference pref;
     static List<Contact> conList = new List<Contact>();
     static VFC_ContactMarkAsDuplicate comp;
     static ApexPages.StandardSetController ssc;
    
    private static testmethod void myUnitTestData()
  {
      
    
    Country__c cntry = new Country__c(Name='TestCountry', CountryCode__c='TCY');
    insert cntry;
 
    acc = new Account(Name='TestAccount', 
                    ClassLevel1__c='Internal', 
                    LeadingBusiness__c='BD', 
                    POBox__c='XXX',
                    Street__c='New Street', 
                    City__c='New City',  
                    ZipCode__c='012345',
                    Country__c=cntry.id);          
    insert acc;
        
    conList.add( new Contact(FirstName='Test',LastName='Contact1',AccountId=acc.Id,JobTitle__c='Z3',CorrespLang__c='EN',WorkPhone__c='1234567890',ToBeDeleted__c=true,ReasonForDeletion__c='',DuplicateWith__c=null));
    conList.add( new Contact(FirstName='Test',LastName='Contact2',AccountId=acc.Id,JobTitle__c='Z3',CorrespLang__c='EN',WorkPhone__c='1234567890',ToBeDeleted__c=false,ReasonForDeletion__c='',DuplicateWith__c=null));
    conList.add( new Contact(FirstName='Test',LastName='Contact3',AccountId=acc.Id,JobTitle__c='Z3',CorrespLang__c='EN',WorkPhone__c='1234567890',ToBeDeleted__c=false,ReasonForDeletion__c='',DuplicateWith__c=null));
    insert conList;
    
    
    
    
    
    pref = Page.VFP_ContactMarkAsDuplicate;
    pref.getParameters().put('id',acc.id);
    Test.setCurrentPage(pref);
    
    ssc = new ApexPages.StandardSetController(conList);
            ssc.setSelected(conList);
    comp = new VFC_ContactMarkAsDuplicate(ssc);
    
 
    Test.startTest();
    
        List<Contact> loopList = ssc.getSelected();
            
            comp.selMasterID = conList[0].id;
            pref = comp.selectAsMaster();
        
        System.assert(ApexPages.getMessages().size() == 1); 
        System.assert(ApexPages.getMessages().get(0).getDetail() == label.CLMAY13ACC05); 
        System.assert(ApexPages.getMessages().get(0).getSeverity() == ApexPages.Severity.ERROR);
        
                   
            comp.selMasterID = conList[1].id;
            pref = comp.selectAsMaster();
        
        System.assert(conList[2].get('ToBeDeleted__c') == True,'value not true');
        
    Test.StopTest();
  
  }
 
}