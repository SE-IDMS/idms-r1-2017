//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This class to leverage Send Invitation Rest API functionality called by IFW.
//                  IFW call this API with email, invitation id and redirect url parameter.
//                  All three parameters are mandatory. 
//
//*******************************************************************************************

public without sharing class IDMSInvitationValidation {
    //This method take required input from IFW and send invitation to requested email. 
    public IDMSResponseWrapperInvitation validateInvitation(String email, String InvitationId, String RedirectUrl)   {
        if(!mandatoryFieldsMissing(email, InvitationId, RedirectUrl)){
            if(validateemail(email))
            {
                if(!userExists(email)){
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    // Assign the email addresses receivevd from application.
                    String[] toAddresses = new String[] {email};
                        mail.setToAddresses(toAddresses);
                    // Specify the name used as the display name.
                    mail.setSenderDisplayName('IDMS Registartion Invitation');
                    // Specify the subject line for your email address.
                    mail.setSubject('IDMS Invitation : ' + InvitationId);
                    // Specify the text content of the email.
                    mail.setPlainTextBody('Your invitation: ' + InvitationId +' has been created.');
                    mail.setHtmlBody('Your invitation:<b> ' + InvitationId +' </b>has been created.<p>'+
                                     'To view your invitation <a href='+RedirectUrl+'&InvitationId='+InvitationId+'>click here.</a>');
                    // Send the email you have created.
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    return new IDMSResponseWrapperInvitation('Success',Label.CLQ316IDMS059);   
                }else
                    //Email alerady registered check
                    return new IDMSResponseWrapperInvitation(Label.CLJUN16IDMS76,Label.CLQ316IDMS114);
            }else
                //Email format 
                return new IDMSResponseWrapperInvitation(Label.CLJUN16IDMS76,Label.CLQ316IDMS060);  
        }else
            //Missing mandatory 
            return new IDMSResponseWrapperInvitation(Label.CLJUN16IDMS76,Label.CLQ316IDMS061);  
        return null;
    }
    
    public Boolean mandatoryFieldsMissing(String email, String InvitationId, String RedirectUrl){
        if(String.isEmpty(email) || String.isEmpty(InvitationId) || String.isEmpty(RedirectUrl)){
            return TRUE;
        }
        return FALSE;
    }
    
    //This method check if requested email id is valid or not. 
    public Boolean validateemail(String email)
    {
        Boolean res = true;
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        
        if (!MyMatcher.matches()) 
            res = false;
        return res; 
        
    }
    
    //This method check if user exists or not in IDMS.
    public boolean userExists(String email){
        
        //validation check if email already exist
        List<User> existingContacts = [SELECT id, email FROM User WHERE email = :email];
        String username=email+Label.CLJUN16IDMS71;
        List<User> existingusername = [SELECT id, email FROM User WHERE username= :username];
        //check for UIMS table
        List<UIMS_User__c> existingUIMSEmail = [SELECT id FROM UIMS_User__c WHERE EMail__c = :email];
        if(existingContacts.size() > 0 || existingusername.size() > 0 || existingUIMSEmail.size()>0 )
        {
            return true;
        }else
            return false;   
    }
    
}