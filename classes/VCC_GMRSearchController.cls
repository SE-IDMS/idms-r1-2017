global with sharing class VCC_GMRSearchController
{
public String scriteria{get;set;}{
    MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
    if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
        scriteria=pageParameters.get('commercialReference');
 }
public String gmrcode{get;set;}        
  @RemoteAction
  global static List<OPP_Product__c> getOthers(String business) {
    String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
    return Database.query(query);       
}

@RemoteAction
global static List<WS04_GMR.gmrDataBean> remoteSearch(String searchString,String gmrcode)
{
    WS04_GMR.GMRSearchPort GMRConnection = new WS04_GMR.GMRSearchPort();
    WS04_GMR.criteriaInDataBean Criteria=new WS04_GMR.criteriaInDataBean();
    WS04_GMR.paginationInDataBean Pagination=new WS04_GMR.paginationInDataBean();

    GMRConnection.endpoint_x=Label.CL00376;
    GMRConnection.timeout_x=40000;
    GMRConnection.ClientCertName_x=Label.CL00616;

    Pagination.maxLimitePerPage=100;
    Pagination.pageNumber=1;

Criteria.keyWordList=searchString.split(' '); //prepare the search string
if(gmrcode != null)
{
    if(gmrcode.length()==2)
    {
        Criteria.businessLine = gmrcode;            
    }
    else if(gmrcode.length()==4)
    {
     Criteria.businessLine = gmrcode.substring(0,2);            
     Criteria.productLine =  gmrcode.substring(2,4);            
 }
 else if(gmrcode.length()==6)
 {
   Criteria.businessLine = gmrcode.substring(0,2);            
   Criteria.productLine = gmrcode.substring(2,4);
   Criteria.strategicProductFamily = gmrcode.substring(4,6);
}
else if(gmrcode.length()==8)
{
   Criteria.businessLine = gmrcode.substring(0,2);            
   Criteria.productLine = gmrcode.substring(2,4);
   Criteria.strategicProductFamily = gmrcode.substring(4,6); 
   Criteria.Family = gmrcode.substring(6,8);
}
}
if(!Test.isRunningTest()){
    WS04_GMR.resultDataBean results=GMRConnection.globalSearch(Criteria,Pagination);
    return results.gmrDataBeanList;
}
else{
return null;
}
}

}