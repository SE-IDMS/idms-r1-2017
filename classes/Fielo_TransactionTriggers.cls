/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: Trigger Class                                       *
* Trigger: Fielo_OnTransaction                              *
* Created Date: 03/10/2014                                  *
************************************************************/
public with sharing class Fielo_TransactionTriggers{

    public static void setsCurrencyUAD(List<FieloEE__Transaction__c> triggerNew){

        Set<String> setMembers = new Set<String>();
    
        for(FieloEE__Transaction__c trans: triggerNew){
            setMembers.add(trans.FieloEE__Member__c);          
        }
        
        Map<Id,FieloEE__Member__c> mapMembers = new Map<Id,FieloEE__Member__c>([SELECT Id FROM FieloEE__Member__c WHERE Id IN: setMembers AND FieloEE__Program__r.Name = 'Schneider Electric Rewards Program']);
        
        for(FieloEE__Transaction__c trans: triggerNew){
            if(mapMembers.containsKey(trans.FieloEE__Member__c)){
                trans.CurrencyIsoCode = 'AED';
            }
        }
         
    }
}