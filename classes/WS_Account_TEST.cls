@isTest
private class WS_Account_TEST
{
    @testsetup
    static void createTestData()
    {
        Country__c cntry1 = new Country__c(Name='TestCountry1', CountryCode__c='IT');
        insert cntry1;

        StateProvince__c state1 = new StateProvince__c(Name='TestState1', Country__c=cntry1.id, CountryCode__c='IT', StateProvinceCode__c='AT');
        insert state1;

        Account acc1 = new Account(Name='TestAccbFOID', City__c='TestCity', Street__c='TestStreet', ZipCode__c='12345', Country__c=cntry1.id, RecordTypeId  = System.Label.CLOCT13ACC08);
        Account acc2 = new Account(Name='TestAccSEAccID', SEAccountID__c='0123456', City__c='TestCity', Street__c='TestStreet', ZipCode__c='12345', Country__c=cntry1.id, RecordTypeId  = System.Label.CLOCT13ACC08);
        Account acc3 = new Account(Name='TestAccLegacy', City__c='TestCity', Street__c='TestStreet', ZipCode__c='12345', Country__c=cntry1.id, RecordTypeId  = System.Label.CLOCT13ACC08);
        Account acc4 = new Account(Name='TestAccDuplicate', City__c='TestCity', Street__c='TestStreet', ZipCode__c='12345', Country__c=cntry1.id, RecordTypeId  = System.Label.CLOCT13ACC08);
        Account acc5 = new Account(Name='TestAccDuplicate', City__c='TestCity', Street__c='TestStreet', ZipCode__c='12345', Country__c=cntry1.id, RecordTypeId  = System.Label.CLOCT13ACC08);
        Account acc6 = new Account(Name='TestAcc1DupAcct', City__c='TestCity', Street__c='TestStreet', ZipCode__c='12345', Country__c=cntry1.id, RecordTypeId  = System.Label.CLOCT13ACC08);

        List<Account> lstAccts = new List<Account>{acc1, acc2, acc3, acc4, acc5, acc6};
        insert lstAccts;
        //System.debug(' Lst of Accounts inserted -->> '+ lstAccts);
        
        LegacyAccount__c objLegAcct = new LegacyAccount__c(Account__c=acc3.id, LegacyName__c='ITB_ORA', LegacyNumber__c='Leg00');
        insert objLegAcct;

        User user = new User(alias = 'user', email='user' + '@schneider-electric.com', 
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CL00110, 
                             timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = 'SESA2345');
        insert user;

    }

    static testmethod void createAccountTest()
    {
        //bFOAccount with bFOID
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'TestAccbFOID' LIMIT 1];
        WS_Account.bFOAccount wsAcc1 = new WS_Account.bFOAccount();
        wsAcc1.bFOAccountID=acc.Id;
        wsAcc1.publisherID='ITB_ORA'; 
        wsAcc1.legacyAccountID='Leg01'; 
        wsAcc1.AccountName=acc.Name; 
        wsAcc1.PhysicalStreet='TestStreet'; 
        wsAcc1.PhysicalCity='TestCity'; 
        wsAcc1.PhysicalCountryCode='IT'; 
        wsAcc1.CurrencyIsoCode='EUR'; 
        wsAcc1.AccType='DC'; 
        wsAcc1.ClassLevel1='FI'; 
        wsAcc1.OwnerSESAID='SESA2345'; 
        wsAcc1.RecordType='Customer';

        //bFOAccount with SEAccountID
        WS_Account.bFOAccount wsAcc2 = wsAcc1.clone();
        wsAcc2.SEAccountID='0123456'; 
        wsAcc2.AccountName='TestAccSEAccID';
        wsAcc2.bFOAccountID='';

        //bFOAccount with legacy account with publisherID+legacyAccountID exist in bFO
        WS_Account.bFOAccount wsAcc3 = wsAcc1.clone();
        wsAcc3.legacyAccountID='Leg00'; 
        wsAcc3.AccountName='TestAccLegacy';
        wsAcc3.bFOAccountID='';

        //New Account to be created in bFO
        WS_Account.bFOAccount wsAcc4 = wsAcc3.clone(); 
        wsAcc4.legacyAccountID='Leg02';
        wsAcc4.AccountName='TestAcc1';

        //1 Duplicate account exists in bFO
        WS_Account.bFOAccount wsAcc7 = wsAcc3.clone(); 
        wsAcc7.legacyAccountID='Leg02Dup';
        wsAcc7.AccountName='TestAcc1DupAcct';
        
        //More than 1 duplicate account exists in bFO
        WS_Account.bFOAccount wsAcc5 = wsAcc3.clone(); 
        wsAcc5.legacyAccountID='Leg03';
        wsAcc5.AccountName='TestAccDuplicate';
        wsAcc5.AccountLocalName='LocalAccountName';
        wsAcc5.PhysicalZipCode='1234';        
        wsAcc5.LocalCity='localTestCity';
        wsAcc5.MarketSegment='BD1';
        wsAcc5.MarketSubSegment='B38';
        wsAcc5.AdditionalAddress='AddAddress';
        wsAcc5.POBox='XXX';
        wsAcc5.StreetLocalLang='StreetLocalLang';
        wsAcc5.LocalAdditionalAddress='LocalAddAddress';
        wsAcc5.LeadingBusiness='BD';
        wsAcc5.PhysicalStateCode='AT';
        
        //All mandatory fields dont have value
        WS_Account.bFOAccount wsAcc6 = new WS_Account.bFOAccount(); 
        wsAcc6.SEAccountID='0123456';

        //Account with wrong bFOID
        WS_Account.bFOAccount wsAcc8 = wsAcc1.clone();
        wsAcc8.AccountName='TestAccWrongbFOID';
        wsAcc8.bFOAccountID='001004568348298';

        //Account with wrong SEAcctID
        WS_Account.bFOAccount wsAcc9 = wsAcc2.clone();
        wsAcc9.AccountName='TestAccWrongSEID';
        wsAcc9.SEAccountID='01234';
        
        List<WS_Account.bFOAccount>  acclist = new List<WS_Account.bFOAccount>();
        
        User user = [SELECT Id, FederationIdentifier FROM User WHERE FederationIdentifier='SESA2345' LIMIT 1];
        
        Test.startTest();
        
        System.runAs(user)
        {
        
            System.debug('wsAcc1 -->> '+wsAcc1);
            acclist.add(wsAcc1);
            WS_Account.bulkCreateAccounts(acclist);

            acclist.clear();
            System.debug('wsAcc2 -->> '+wsAcc2);
            acclist.add(wsAcc2);
            WS_Account.bulkCreateAccounts(acclist);

            acclist.clear();
            System.debug('wsAcc3 -->> '+wsAcc3);
            acclist.add(wsAcc3);
            WS_Account.bulkCreateAccounts(acclist);
        
            acclist.clear();
            System.debug('wsAcc4 -->> '+wsAcc4);
            acclist.add(wsAcc4);
            WS_Account.bulkCreateAccounts(acclist);

            acclist.clear();
            System.debug('wsAcc5 -->> '+wsAcc5);
            acclist.add(wsAcc5);
            WS_Account.bulkCreateAccounts(acclist);
        
            acclist.clear();
            System.debug('wsAcc6 -->> '+wsAcc6);
            acclist.add(wsAcc6);
            WS_Account.bulkCreateAccounts(acclist);
        
            acclist.clear();
            System.debug('wsAcc7 -->> '+wsAcc7);
            acclist.add(wsAcc7);
            WS_Account.bulkCreateAccounts(acclist);
        
            acclist.clear();
            System.debug('wsAcc8 -->> '+wsAcc8);
            acclist.add(wsAcc8);
            WS_Account.bulkCreateAccounts(acclist);

            acclist.clear();
            System.debug('wsAcc9 -->> '+wsAcc9);
            acclist.add(wsAcc9);
            WS_Account.bulkCreateAccounts(acclist);
          
            acclist.clear();
            WS_Account.bulkCreateAccounts(acclist);
            
        }

        Test.stopTest();
    }

}