public class AP_WorkDetailDeletionManager {
    
    public static List<WorkDetailDeletion__c> getWorkDetailsToSaveFromDeletion(List<SVMXC__Service_Order_Line__c> aListOfWorkDetailsToBeDeleted) {
        System.debug('### Starting AP_WorkDetailDeletionManager.getWorkDetailsToSaveFromDeletion');
        System.debug('### aListOfWorkDetailsToBeDeleted: '+aListOfWorkDetailsToBeDeleted);
        List<WorkDetailDeletion__c> result = new List<WorkDetailDeletion__c>();
        System.debug('### result instantiation: '+result);
        if (aListOfWorkDetailsToBeDeleted != null && aListOfWorkDetailsToBeDeleted.size() > 0 ) {
            System.debug('### aListOfWorkDetailsToBeDeleted is not null and has a size > 0');
            for (SVMXC__Service_Order_Line__c wd : aListOfWorkDetailsToBeDeleted) {
                System.debug('### Current wd: '+wd);
                WorkDetailDeletion__c wdd = new WorkDetailDeletion__c(
                    
                    BO_Material_Reference__c = wd.BO_Material_Reference__c, 
                    BackOfficeSystem__c = wd.BackOfficeSystem__c, 
                    CountryOfBackOffice__c = wd.CountryOfBackOffice__c, 
                    FSR_SESA__c = wd.FSR_SESA__c, 
                    HourType__c = wd.HourType__c, 
                    WorkDetailID__c = wd.Id, 
                    InstalledQuantity__c = wd.InstalledQuantity__c, 
                    IsBillable__c = wd.IsBillable__c, 
                    Level_Required__c = wd.Level_Required__c, 
                    WorkDetailName__c = wd.Name, 
                    PartsOrderLine__c = wd.PartsOrderLine__c, 
                    Record_Type_Name__c = wd.Record_Type_Name__c, 
                    Activity_Type__c = wd.SVMXC__Activity_Type__c, 
                    End_Date_and_Time__c = wd.SVMXC__End_Date_and_Time__c, 
                    Line_Type__c = wd.SVMXC__Line_Type__c, 
                    Service_Order__c = wd.SVMXC__Service_Order__c, 
                    Start_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c, 
                    Work_Description__c = wd.SVMXC__Work_Description__c, 
                    //TECH_OutboundEvent__c = wd.TECH_OutboundEvent__c, 
                    Tech_PartsOrderLine__c = wd.Tech_PartsOrderLine__c, 
                    TotalTime__c = wd.TotalTime__c, 
                    WO_BackOfficeReference__c = wd.WO_BackOfficeReference__c, 
                    Service_Order_Name__c = wd.WO_Name__c,
                    WO_Project__c = wd.WO_Project__c,  // BR-10420 Added by Prem for Q3 Release
                    Work_Order_Record_Type__c = wd.Work_Order_Record_Type__c//added by suhail for dec16
                );

                
                result.add(wdd);
                System.debug('### result: '+result);
            }
        }
        System.debug('###');
        return result;
    }

}