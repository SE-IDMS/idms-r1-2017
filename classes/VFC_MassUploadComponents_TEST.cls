@isTest
public class VFC_MassUploadComponents_TEST {
    static testMethod void massupload() {
        Profile P = [select id from profile where name='Standard User'];
        User U = new User(alias = 'standt', email='standarduser@testorg.com',
                          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                          localesidkey='en_US', profileid = P.Id,
                          timezonesidkey='America/Los_Angeles', username='standardusers@testorg.com');
        INSERT U;   
            
        Release__c release = new Release__c();
        release.Name = 'test release';
        release.ReleaseStartDate__c = system.today();
        release.PlanEndDate__c = system.today()+1;
            
        INSERT release;
            
        List<Component__c> compList = new List<Component__c>();
            
        Component__c comp1 = new Component__c();
        comp1.ComponentType__c = 'Page Layout';
        comp1.Track__c = 'Sales';
        comp1.ObjectName__c = 'Opportunity';
        comp1.FieldName__c = 'test field 1';
        comp1.Type__c = 'New';
        comp1.Owner__c = U.ID;
        comp1.Release__c = release.ID;
        compList.add(comp1);
            
        Component__c comp2 = new Component__c();
        comp2.ComponentType__c = 'Page Layout';
        comp2.Track__c = 'Sales';
        comp2.ObjectName__c = 'Opportunity';
        comp2.FieldName__c = 'test field 2';
        comp2.Type__c = 'Existing';
        comp2.Owner__c = U.ID;
        comp2.Release__c = release.ID;
        compList.add(comp2);
            
        INSERT compList;
            
        List<CS_ComponentCSVMap__c> cs_List = new List<CS_ComponentCSVMap__c>();
        CS_ComponentCSVMap__c cs1 = new CS_ComponentCSVMap__c (Name = 'Component Type', APIName__c = 'ComponentType__c', isRequired__c=true);
        CS_ComponentCSVMap__c cs2 = new CS_ComponentCSVMap__c (Name = 'Track', APIName__c = 'Track__c', isRequired__c=true);
        CS_ComponentCSVMap__c cs3 = new CS_ComponentCSVMap__c (Name = 'Field Name', APIName__c = 'FieldName__c', isRequired__c=true);
        CS_ComponentCSVMap__c cs4 = new CS_ComponentCSVMap__c (Name = 'Object Name', APIName__c = 'ObjectName__c', isRequired__c=false);
        CS_ComponentCSVMap__c cs5 = new CS_ComponentCSVMap__c (Name = 'Type', APIName__c = 'Type__c', isRequired__c=true);
        CS_ComponentCSVMap__c cs6 = new CS_ComponentCSVMap__c (Name = 'Release', APIName__c = 'Release__c', isRequired__c=true);
        CS_ComponentCSVMap__c cs7 = new CS_ComponentCSVMap__c (Name = 'Owner', APIName__c = 'Owner__c', isRequired__c=true);
        cs_List.add(cs1);
        cs_List.add(cs2);
        cs_List.add(cs3);
        cs_List.add(cs4);
        cs_List.add(cs5);
        cs_List.add(cs6);
        cs_List.add(cs7);
            
        Insert cs_List;
            
        Map<String,CS_ComponentCSVMap__c> CSVMap = new Map<String,CS_ComponentCSVMap__c>();
        for(Integer i=0; i<cs_List.size();i++) {
            CSVMap.put(cs_List[i].Name,cs_List[i]);    
        }
        
        StringListWrapper csvList = new StringListWrapper();            
        List<String> list1 = new List<String>();
        list1.add('Component Type');
        list1.add('Track');  
        list1.add('Object Name');
        list1.add('Field Name');
        list1.add('Type');
        list1.add('Owner');
        list1.add('Release');
        List<String> list2 = new List<String>();
        list2.add('Page Layout');
        list2.add('Shared');
        list2.add('ABC');
        list2.add('ABC1');
        list2.add('New');
        list2.add('');
        list2.add('');        
        csvList.stringList.add(list1);
        csvList.stringList.add(list2);          
        
        PageReference pageRef = Page.VFP_MassUploadComponents;
        pageRef.getParameters().put('id', release.Id); //parameter to be passed before calling the constructor
        Test.setCurrentPage(pageRef);
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(compList);
        VFC_MassUploadComponents muc = new VFC_MassUploadComponents(sc);
           
        muc.getHeaderRow();
        muc.csvList = csvList;
        muc.csvNameAPINameMap = CSVMap;
        muc.getcomponentlist();
        muc.Save();
        muc.Cancel();       
    }
}