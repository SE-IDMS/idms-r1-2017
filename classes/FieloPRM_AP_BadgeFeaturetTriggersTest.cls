/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: Test Class                                          *
* Created Date: 14.04.2014                                  *
* Tested Class: FieloPRM_AP_BadgeFeaturetTriggers           *
************************************************************/

@isTest
public with sharing class FieloPRM_AP_BadgeFeaturetTriggersTest {
  
    public static testMethod void testUnit(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            //Feature
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c(
                Name = 'TestRamiro',
                F_PRM_FeatureAPIName__c = 'TestRamiroApiName',
                F_PRM_FeatureCode__c = 'Fecodetest'
            );
            insert feature;
            
            FieloPRM_Feature__c feature2 = new FieloPRM_Feature__c(
                Name = 'TestRamiro2',
                F_PRM_FeatureAPIName__c = 'TestRamiroApiName2',
                F_PRM_FeatureCode__c = 'Fecodetest2'
            );
            insert feature2;
            
            //Badge
            FieloEE__Badge__c badge = new FieloEE__Badge__c(
                Name = 'test ProgramLevel',
                F_PRM_Type__c = 'Program Level',
                F_PRM_BadgeAPIName__c = 'asd'
            );
            insert badge;
            
            FieloEE__Badge__c badge2 = new FieloEE__Badge__c(
                Name = 'test ProgramLevel2',
                F_PRM_Type__c = 'Program Level',
                F_PRM_BadgeAPIName__c = 'asd2'
            );
            insert badge2;
            
            //Badge - Feature
            list<FieloPRM_BadgeFeature__c> badgeFeatureList = new List<FieloPRM_BadgeFeature__c>();
            badgeFeatureList.add(new FieloPRM_BadgeFeature__c(
                F_PRM_Badge__c = badge.Id,
                F_PRM_Feature__c = feature.Id
            ));
            badgeFeatureList.add(new FieloPRM_BadgeFeature__c(
                F_PRM_Badge__c = badge2.Id,
                F_PRM_Feature__c = feature2.Id
            ));
            badgeFeatureList.add(new FieloPRM_BadgeFeature__c(
                F_PRM_Badge__c = badge.Id,
                F_PRM_Feature__c = feature2.Id
            ));
            badgeFeatureList.add(new FieloPRM_BadgeFeature__c(
                F_PRM_Badge__c = badge.Id,
                F_PRM_Feature__c = feature2.Id
            ));        
            insert badgeFeatureList;
            
            
            
            //delete
            delete badgeFeatureList;
        }
    }
}