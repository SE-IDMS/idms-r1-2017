@isTest
public class AP_PRMGroupMemberEmailHandler_Test {

    public static testMethod void emailHander1(){
    
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        System.runAs(u) {
        
            Country__c testCountry = new Country__c();
                testCountry.Name   = 'India';
                testCountry.CountryCode__c = 'IN';
                testCountry.InternationalPhoneCode__c = '91';
                testCountry.Region__c = 'APAC'; 
            INSERT testCountry;
            
            Account acc = new Account();
            acc.Name = 'Subhashish';
            acc.RecordTypeId = System.Label.CLAPR15PRM025; 
            acc.Country__c = testCountry.Id;
            INSERT acc;
            
            Contact cc = new Contact();
            cc.FirstName = 'Subhashish';
            cc.LastName = 'Sahu';
            cc.AccountId = acc.Id;
            cc.Email = 'con@yopmail.com';
            cc.PRMEmail__c = 'con@yopmail.com';
            INSERT cc;
            
            
            AP_PRMGroupMemberEmailHandler emailHandler = new AP_PRMGroupMemberEmailHandler();
            
            Messaging.InboundEmail email  = new Messaging.InboundEmail();  
            email.subject = 'ContactId: '+cc.Id;
            Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
            
            Test.startTest();
                Messaging.InboundEmailResult emailResult = emailHandler.handleInboundEmail(email, envelope); 
            Test.stopTest();
        }
    }


}