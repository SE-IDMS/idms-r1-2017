@isTest
private class VFC_GenerateXml_TEST {
    static testMethod void test_GenerateXml(){
      
       Pagereference objPageReference = Page.VFP_GeneratePackagexml;
       Test.setCurrentPageReference(objPageReference);

       Release__c objRelease = new Release__c();
       objRelease.Name='testRelease';
       insert objRelease;

       Component__c objComponent =new Component__c();
       objComponent.ComponentType__c = 'Apex Class';
       objComponent.FieldName__c = 'AP_TestClass';
       objComponent.IsMigrate__c =true;
       objComponent.Owner__c =UserInfo.getUserId();
       objComponent.Release__c = objRelease.Id;
       objComponent.Track__c ='CCC';
       objComponent.Type__c ='New';
       insert objComponent;


       VFC_GenerateXml objGenerateXML = new VFC_GenerateXml(new ApexPages.StandardController(objRelease));
       objGenerateXML.selectedvalue ='CCC';
       objGenerateXML.queryXmlData();
    }

}