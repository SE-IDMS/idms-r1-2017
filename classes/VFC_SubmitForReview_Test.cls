/************************************************************
* Developer: Kaushal Kishore                                *
* Type: Test Class                                          *
* Created Date: 21.07.2016                                  *
* Tested Class: VFC_SubmitForReview                         *
*************************************************************/

@IsTest
private class VFC_SubmitForReview_Test
{
    public static testMethod void testSubmitForReview1(){ 
       createRecord();
       
       List<Hotfix__c> lstHotFix = new List<Hotfix__c>();
       Hotfix__c objHotFix = new Hotfix__c();
       objHotFix.ComponentDetails__c = 'Test Apex Class';
       objHotFix.Type__c = 'Metadata Change';
       objHotFix.Process_Stream__c = 'Sales';
       objHotFix.Severity__c = '3-Medium';
       objHotFix.Remedy_Ticket_ID__c = '1234';
       objHotFix.IncidentOwner__c = UserInfo.getUserId();
       objHotFix.Usersaffected__c = '<100';
       objHotFix.WorkaroundExists__c = 'No';
       objHotFix.AffectData__c = 'No';
       objHotFix.AffectCritical__c = 'No';
       objHotFix.RevisionCommit__c = '1212';
       objHotFix.Status__c = label.CLJUL16PAN01; //New
       //objHotFix.RecordTypeId = label.CLJUL16PAN08; //Requestor

       lstHotFix.add(objHotFix);      

       insert lstHotFix;
       
       Hotfix__c ObjHotfix1 = new Hotfix__c();
       ObjHotfix1 = [select id,RevisionCommit__c,Status__c,Hotfix_Assigned_To__c,CreatedById from Hotfix__c where id =:lstHotFix[0].id]; 
       
       Test.startTest();
       PageReference pref = Page.VFPSubmitForReview;      
       pref.getParameters().put('id',ObjHotfix1.id);
       Test.setCurrentPage(pref);

       ApexPages.StandardController controller1 = new ApexPages.StandardController(ObjHotfix1);
       VFC_SubmitForReview objSubmitForReviewClass = new VFC_SubmitForReview(controller1);
       objSubmitForReviewClass.UpdateRecordStatus();
       Test.stopTest();
    }
    
    public static testMethod void testSubmitForReview2(){
       createRecord();  
       
       List<Hotfix__c> lstHotFix = new List<Hotfix__c>();
       Hotfix__c objHotFix2 =new Hotfix__c();
       objHotFix2.ComponentDetails__c = 'Test Apex Class2';
       objHotFix2.Type__c = 'Metadata Change';
       objHotFix2.Process_Stream__c = 'Sales';
       objHotFix2.Severity__c = '3-Medium';
       objHotFix2.Remedy_Ticket_ID__c = '1234';
       objHotFix2.IncidentOwner__c = UserInfo.getUserId();
       objHotFix2.Usersaffected__c = '<100';
       objHotFix2.WorkaroundExists__c = 'No';
       objHotFix2.AffectData__c = 'No';
       objHotFix2.AffectCritical__c = 'No';
       objHotFix2.RevisionCommit__c = '1212';
       objHotFix2.Status__c = label.CLJUL16PAN02; //Requested
       //objHotFix2.RecordTypeId = label.CLJUL16PAN09; //Reviewer

       lstHotFix.add(objHotFix2);
       
       insert lstHotFix;
       
       Hotfix__c ObjHotfix1 = new Hotfix__c();
       ObjHotfix1 = [select id,RevisionCommit__c,Status__c,Hotfix_Assigned_To__c,CreatedById from Hotfix__c where id =:lstHotFix[0].id]; 
       
       Test.startTest();
       // Setup test data
       // This code runs as the system user
          Profile p = [SELECT Id FROM Profile WHERE Name='system administrator'];
          User u = new User(Alias = 'abckk', Email='abckk@schneider-electric.com',
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
          LocaleSidKey='en_US', ProfileId = p.Id,
          TimeZoneSidKey='America/Los_Angeles', UserName='abckk@schneider-electric.com');
      System.runAs(u) {
       PageReference pref = Page.VFPSubmitForReview;      
       pref.getParameters().put('id',ObjHotfix1.id);
       Test.setCurrentPage(pref);

       ApexPages.StandardController controller1 = new ApexPages.StandardController(ObjHotfix1);
       VFC_SubmitForReview objSubmitForReviewClass = new VFC_SubmitForReview(controller1);
       objSubmitForReviewClass.UpdateRecordStatus();
       Test.stopTest();
       }
    }
    
    public static testMethod void testSubmitForReview3(){
           createRecord(); 
       
           List<Hotfix__c> lstHotFix = new List<Hotfix__c>();
           Hotfix__c objHotFix3 =new Hotfix__c();
           objHotFix3.ComponentDetails__c = 'Test Apex Class3';
           objHotFix3.Type__c = 'Metadata Change';
           objHotFix3.Process_Stream__c = 'Sales';
           objHotFix3.Severity__c = '3-Medium';
           objHotFix3.Remedy_Ticket_ID__c = '1234';
           objHotFix3.IncidentOwner__c = UserInfo.getUserId();
           objHotFix3.Usersaffected__c = '<100';
           objHotFix3.WorkaroundExists__c = 'No';
           objHotFix3.AffectData__c = 'No';
           objHotFix3.AffectCritical__c = 'No';
           objHotFix3.RevisionCommit__c = '1212';
           objHotFix3.Hotfix_Assigned_To__c = UserInfo.getUserId(); //'005A0000001pvTs';
           objHotFix3.Status__c = label.CLJUL16PAN03; //Requested
           //objHotFix3.RecordTypeId = label.CLJUL16PAN10; //Validator
    
           lstHotFix.add(objHotFix3);
           
           insert lstHotFix;
           
           Hotfix__c ObjHotfix1 = new Hotfix__c();
           ObjHotfix1 = [select id,RevisionCommit__c,Status__c,Hotfix_Assigned_To__c,CreatedById from Hotfix__c where id =:lstHotFix[0].id]; 
           
           Test.startTest();
           PageReference pref = Page.VFPSubmitForReview;      
           pref.getParameters().put('id',ObjHotfix1.id);
           Test.setCurrentPage(pref);
    
           ApexPages.StandardController controller1 = new ApexPages.StandardController(ObjHotfix1);
           VFC_SubmitForReview objSubmitForReviewClass = new VFC_SubmitForReview(controller1);
           objSubmitForReviewClass.UpdateRecordStatus();
           Test.stopTest();
       }
       
       public static testMethod void testSubmitForReview4(){
           createRecord();  
       
           List<Hotfix__c> lstHotFix = new List<Hotfix__c>();
           Hotfix__c objHotFix3 =new Hotfix__c();
           objHotFix3.ComponentDetails__c = 'Test Apex Class3';
           objHotFix3.Type__c = 'Metadata Change';
           objHotFix3.Process_Stream__c = 'Sales';
           objHotFix3.Severity__c = '3-Medium';
           objHotFix3.Remedy_Ticket_ID__c = '1234';
           objHotFix3.IncidentOwner__c = UserInfo.getUserId();
           objHotFix3.Usersaffected__c = '<100';
           objHotFix3.WorkaroundExists__c = 'No';
           objHotFix3.AffectData__c = 'No';
           objHotFix3.AffectCritical__c = 'No';
           objHotFix3.RevisionCommit__c = '1212';
           objHotFix3.Hotfix_Assigned_To__c = UserInfo.getUserId(); //'005A0000001pvTs';
           objHotFix3.Status__c = label.CLJUL16PAN05; //Requested
           //objHotFix3.RecordTypeId = label.CLJUL16PAN10; //Validator
    
           lstHotFix.add(objHotFix3);
           
           insert lstHotFix;
           
           Hotfix__c ObjHotfix1 = new Hotfix__c();
           ObjHotfix1 = [select id,RevisionCommit__c,Status__c,Hotfix_Assigned_To__c,CreatedById from Hotfix__c where id =:lstHotFix[0].id]; 
           
           Test.startTest();
           PageReference pref = Page.VFPSubmitForReview;      
           pref.getParameters().put('id',ObjHotfix1.id);
           Test.setCurrentPage(pref);
    
           ApexPages.StandardController controller1 = new ApexPages.StandardController(ObjHotfix1);
           VFC_SubmitForReview objSubmitForReviewClass = new VFC_SubmitForReview(controller1);
           objSubmitForReviewClass.UpdateRecordStatus();
           Test.stopTest();
       }
       
       public static testMethod void testSubmitForReview5(){
           createRecord();
       
           List<Hotfix__c> lstHotFix = new List<Hotfix__c>();
           Hotfix__c objHotFix3 =new Hotfix__c();
           objHotFix3.ComponentDetails__c = 'Test Apex Class3';
           objHotFix3.Type__c = 'Metadata Change';
           objHotFix3.Process_Stream__c = 'Sales';
           objHotFix3.Severity__c = '3-Medium';
           objHotFix3.Remedy_Ticket_ID__c = '1234';
           objHotFix3.IncidentOwner__c = UserInfo.getUserId();
           objHotFix3.Usersaffected__c = '<100';
           objHotFix3.WorkaroundExists__c = 'No';
           objHotFix3.AffectData__c = 'No';
           objHotFix3.AffectCritical__c = 'No';
           objHotFix3.RevisionCommit__c = '1212';
           objHotFix3.Hotfix_Assigned_To__c = UserInfo.getUserId(); //'005A0000001pvTs';
           objHotFix3.Status__c = label.CLJUL16PAN06; //Requested
           //objHotFix3.RecordTypeId = label.CLJUL16PAN10; //Validator
    
           lstHotFix.add(objHotFix3);
           
           insert lstHotFix;
           
           Hotfix__c ObjHotfix1 = new Hotfix__c();
           ObjHotfix1 = [select id,RevisionCommit__c,Status__c,Hotfix_Assigned_To__c,CreatedById from Hotfix__c where id =:lstHotFix[0].id]; 
           
           Test.startTest();
           PageReference pref = Page.VFPSubmitForReview;      
           pref.getParameters().put('id',ObjHotfix1.id);
           Test.setCurrentPage(pref);
    
           ApexPages.StandardController controller1 = new ApexPages.StandardController(ObjHotfix1);
           VFC_SubmitForReview objSubmitForReviewClass = new VFC_SubmitForReview(controller1);
           objSubmitForReviewClass.UpdateRecordStatus();
           Test.stopTest();
       }
       
       public static testMethod void testSubmitForReview6(){
           createRecord();
       
           List<Hotfix__c> lstHotFix = new List<Hotfix__c>();
           Hotfix__c objHotFix3 =new Hotfix__c();
           objHotFix3.ComponentDetails__c = 'Test Apex Class3';
           objHotFix3.Type__c = 'Metadata Change';
           objHotFix3.Process_Stream__c = 'Sales';
           objHotFix3.Severity__c = '3-Medium';
           objHotFix3.Remedy_Ticket_ID__c = '1234';
           objHotFix3.IncidentOwner__c = UserInfo.getUserId();
           objHotFix3.Usersaffected__c = '<100';
           objHotFix3.WorkaroundExists__c = 'No';
           objHotFix3.AffectData__c = 'No';
           objHotFix3.AffectCritical__c = 'No';
           objHotFix3.RevisionCommit__c = '1212';
           objHotFix3.Hotfix_Assigned_To__c = UserInfo.getUserId(); //'005A0000001pvTs';
           objHotFix3.Status__c = 'Closed';
    
           lstHotFix.add(objHotFix3);
           
           insert lstHotFix;
           
           Hotfix__c ObjHotfix1 = new Hotfix__c();
           ObjHotfix1 = [select id,RevisionCommit__c,Status__c,Hotfix_Assigned_To__c,CreatedById from Hotfix__c where id =:lstHotFix[0].id]; 
           
           Test.startTest();
           PageReference pref = Page.VFPSubmitForReview;      
           pref.getParameters().put('id',ObjHotfix1.id);
           Test.setCurrentPage(pref);
    
           ApexPages.StandardController controller1 = new ApexPages.StandardController(ObjHotfix1);
           VFC_SubmitForReview objSubmitForReviewClass = new VFC_SubmitForReview(controller1);
           objSubmitForReviewClass.UpdateRecordStatus();
           Test.stopTest();
       }
       
       public static testMethod void testSubmitForReview7(){
           createRecord();  
           
           List<Hotfix__c> lstHotFix = new List<Hotfix__c>();
           Hotfix__c objHotFix2 =new Hotfix__c();
           objHotFix2.ComponentDetails__c = 'Test Apex Class2';
           objHotFix2.RevisionCommit__c = null;
           objHotFix2.Status__c = label.CLJUL16PAN01; //Requested
           //objHotFix2.RecordTypeId = label.CLJUL16PAN09; //Reviewer
    
           lstHotFix.add(objHotFix2);
           
           insert lstHotFix;
           
           Hotfix__c ObjHotfix1 = new Hotfix__c();
           ObjHotfix1 = [select id,RevisionCommit__c,Status__c,Hotfix_Assigned_To__c,CreatedById from Hotfix__c where id =:lstHotFix[0].id]; 
           
           Test.startTest();
           PageReference pref = Page.VFPSubmitForReview;      
           pref.getParameters().put('id',ObjHotfix1.id);
           Test.setCurrentPage(pref);
    
           ApexPages.StandardController controller1 = new ApexPages.StandardController(ObjHotfix1);
           VFC_SubmitForReview objSubmitForReviewClass = new VFC_SubmitForReview(controller1);
           objSubmitForReviewClass.UpdateRecordStatus();
           Test.stopTest();
       }
       
       static testmethod void createRecord()
       {
           Release__c objRelease = new Release__c();
           objRelease.Name='testRelease';
           insert objRelease; 
       }
}