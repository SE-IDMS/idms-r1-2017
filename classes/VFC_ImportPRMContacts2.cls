public class VFC_ImportPRMContacts2 {
    public transient String prmOriginSource { get; set; }
    public transient String prmOriginSourceInfo { get; set; }
    public transient String TemplateId { get; set; }
    public String DummyModel { 
        get { return JSON.serialize(this.tModel); } 
        set; 
    }
    
    private transient AP_UserRegModel tModel = new AP_UserRegModel();
    
    public List<SelectOption> getOriginSourceOptions() {
        List<SelectOption> originOptions= new List<SelectOption>();
        originOptions.add(new SelectOption('','--NONE--'));
        Schema.DescribeFieldResult fieldResult = UIMSCompany__c.OriginSource__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
            if(f.getValue() != 'Other')
                originOptions.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        originOptions.sort();
        originOptions.add(new SelectOption('Other','Other'));
        return originOptions;
    }
    
    public VFC_ImportPRMContacts2 () {
        CS_PRM_ApexJobSettings__c tmplSetting = CS_PRM_ApexJobSettings__c.getInstance ('IMPORT_PRM_CNT_TMPL_ID');
        if (tmplSetting != null) TemplateId = tmplSetting.QueryFields__c;
    }

    @RemoteAction
    public static Map<String,String> ImportPRMContacts (List<AP_UserRegModel> lNewCnts) {
        Map<String, String> regLinkMap = new Map<String, String>();
        List<String> idStringList = new List<String>();
        String domain='';
        List<String> lFlds = AP_PRMUtils.RegistrationTrackingFields();
        Map<String,AP_UserRegModel> mNewCnts = validateAccountsAndContacts(lNewCnts);
        List<AP_UserRegModel> lValidCnts = new List<AP_UserRegModel>();
        for(AP_UserRegModel regMdl:lNewCnts){
            String errorMessage = (mNewCnts.get(regMdl.email).accountId == 'error' && mNewCnts.get(regMdl.email).ContactId == 'error') ? 'Error: Given CompanyBfoId and ContactBfoId are incorrect' : (mNewCnts.get(regMdl.email).accountId == 'error') ? 'Error: Given CompanyBfoId is incorrect' : (mNewCnts.get(regMdl.email).ContactId == 'error') ? 'Error: Given ContactBfoId is incorrect' : null;
            if(String.isNotBlank(errorMessage))
                regLinkMap.put(regMdl.email.toLowerCase(), errorMessage);
            else
                lValidCnts.add(regMdl);
        }
        if(!lValidCnts.isEmpty()){
            List<UIMSCompany__c> uimsUpdatedLst = AP_PRMUtils.CreateUserRegistrationTracking (AP_PRMAppConstants.RegistrationType.MassRegistration, lValidCnts);
            List<FieloEE__Program__c> prgm = new List<FieloEE__Program__c>([SELECT FieloEE__SiteDomain__c FROM FieloEE__Program__c WHERE Name = 'PRM Is On']);
            if(prgm.size() > 0 && !prgm.isEmpty())
                domain = prgm[0].FieloEE__SiteDomain__c;
            for(UIMSCompany__c ums:uimsUpdatedLst){
                idStringList.add(ums.Id);
            }
            
            String qryFilter = 'Id IN:idStringList';
            String queryString = 'SELECT Id,'+ String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE '+qryFilter;
            System.debug('*** str'+queryString);
            for(UIMSCompany__c ums:Database.Query(queryString)){
                String regLink = string.isNotBlank(ums.language__c) ? ('https://'+domain+'/Menu/Registration?country='+ums.countryCode__c+'&language='+ums.language__c+'&leadId='+ums.Name+'&leadSource=prm&bt='+ums.BusinessType__c+'&af='+ums.AreaOfFocus__c):('https://'+domain+'/Menu/Registration?country='+ums.countryCode__c+'&leadId='+ums.Name+'&leadSource=prm&bt='+ums.BusinessType__c+'&af='+ums.AreaOfFocus__c);
                regLinkMap.put(ums.Email__c, regLink);
            }
        }
        system.debug('*** regLinkMap'+regLinkMap);
        return regLinkMap;
    }
    
    public static Map<String,AP_UserRegModel> validateAccountsAndContacts(List<AP_UserRegModel> lNewCnts){
        Set<String> accountIds = new Set<String>();
        Set<String> contactIds = new Set<String>();
        for(AP_UserRegModel regModl:lNewCnts){
            if(String.isNotBlank(regModl.contactID)){
                regModl.contactID = regModl.contactID.left(15);
                contactIds.add(regModl.contactID);
            }
            if(String.isNotBlank(regModl.accountId)){
                regModl.accountId = regModl.accountId.left(15);
                accountIds.add(regModl.accountId);
            }
        }
        Map<String, Contact> contactsMap = new Map<String,Contact>();
        List<Contact> contactsList = [SELECT ID,PRMUIMSID__c,Account.PRMUIMSID__c,AccountId FROM Contact WHERE Id IN:contactIds];
        for(contact con:contactsList){
            String sId = con.Id;
            contactsMap.put(sId.left(15),con);
            accountIds.add(con.AccountId);
        }
        Map<String, Account> AccountsMap = new Map<String,Account>();
        List<Account> accountList = [SELECT ID,PRMUIMSID__c,OwnerId,(SELECT ID,PRMUIMSID__c,AccountId, PRMPrimaryContact__c FROM Contacts) FROM Account WHERE ID IN :accountIds];
        for(Account acc:accountList){
            String sId = acc.Id;
            AccountsMap.put(sId.left(15),acc);
        }
        Map<String, AP_UserRegModel> mValidCnts = new Map<String,AP_UserRegModel>();
        integer i=0;
        for(AP_UserRegModel regModl:lNewCnts){
            System.Debug('primary Contact '+regModl.primayContact+' '+ regModl.isPrimaryContact);
            if(String.isBlank(regModl.contactID) && String.isBlank(regModl.accountId))
                mValidCnts.put(regModl.email,regModl);
            else if(String.isNotBlank(regModl.contactID) && !contactsMap.containsKey(regModl.contactID)){
                if(String.isNotBlank(regModl.accountId) && !AccountsMap.containsKey(regModl.accountId)){
                    regModl.contactID = 'error';
                    regModl.accountId = 'error';
                }
                else
                    regModl.contactID = 'error';
                mValidCnts.put(regModl.email,regModl);
            }
            else if (String.isBlank(regModl.contactID) && String.isNotBlank(regModl.accountId) && !AccountsMap.containsKey(regModl.accountId)){
                regModl.accountId = 'error';
                mValidCnts.put(regModl.email,regModl);
            }
            else if(String.isNotBlank(regModl.ContactId) && contactsMap.containsKey(regModl.contactID)){
                System.debug('*** '+AccountsMap+' '+contactsMap+' '+regModl.ContactId);
                String sAId = contactsMap.get(regModl.ContactId).AccountId;
                sAId = sAId.left(15);
                regModl.companyFederatedId = String.isNotBlank(AccountsMap.get(sAId).PRMUIMSID__c) ? AccountsMap.get(sAId).PRMUIMSID__c : regModl.companyFederatedId;
                regModl.existingContact = true;
                for(Contact con:AccountsMap.get(sAId).Contacts){
                    String sId = con.Id;
                    sId = sId.left(15);
                    if(sId == regModl.contactID){
                        regModl.federationId = String.isNotBlank(con.PRMUIMSID__c) ? con.PRMUIMSID__c : regModl.federationId;
                        regModl.accountId = con.AccountId;
                        regModl.existingAccount = true;
                    }
                    if(con.PRMPrimaryContact__c && sId  != regModl.contactID)
                        regModl.primayContact = regModl.isPrimaryContact = false;
                }
                System.Debug('primary Contact '+regModl.primayContact+' '+ regModl.isPrimaryContact);
                mValidCnts.put(regModl.email,regModl);
                System.Debug('existingContact existingAccount '+regModl.existingContact+' '+regModl.existingAccount);
            }
            else if(String.isNotBlank(regModl.accountId) && AccountsMap.containsKey(regModl.accountId) && String.isBlank(regModl.contactID)){
                regModl.companyFederatedId = String.isNotBlank(AccountsMap.get(regModl.accountId).PRMUIMSID__c ) ? AccountsMap.get(regModl.accountId).PRMUIMSID__c : regModl.companyFederatedId;
                regModl.accOwnerID = AccountsMap.get(regModl.accountId).OwnerId;
                regModl.existingAccount = true;
                for(Contact con:AccountsMap.get(regModl.AccountId).Contacts){
                    if(con.PRMPrimaryContact__c)
                        regModl.primayContact = regModl.isPrimaryContact = false;
                }
                System.Debug('primary Contact '+regModl.primayContact+' '+ regModl.isPrimaryContact);
                mValidCnts.put(regModl.email,regModl);
                System.Debug('existingAccount '+regModl.existingAccount);
            }
        System.Debug('primary Contact '+regModl.primayContact+' '+ regModl.isPrimaryContact);
        }
        return mValidCnts;
    }
    @RemoteAction
    public static Map<String, CountryChannelInfo> GetActiveChannels (List<String> lCountry) {
        return AP_PRMUtils.GetActiveCountryChannelsByCountry (lCountry);
    }

    @RemoteAction
    public static String GetNewBatchImportId () {
        String newBatchImportId = '';
        try {
            BatchImport__c newImport = new BatchImport__c(ImportedBy__c = UserInfo.getUserId(), ImportedDate__c = System.NOW());
            INSERT newImport;
            newBatchImportId = newImport.Id;
        }
        catch (Exception e) {
            System.debug ('*** Exception generating new batch import id. ' + e.getMessage() + e.getStackTraceString());
        }
        return newBatchImportId;
    }
}