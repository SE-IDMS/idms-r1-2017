public class AP_CountryChannelUserRegistration{

    public static void validateDuplicateUserRegField(list<CountryChannelUserRegistration__c> lstNewUserRegFlds){
        
        map<String,CountryChannelUserRegistration__c> mapNewFlds = new map<String,CountryChannelUserRegistration__c>();
        set<id> channelID = new Set<id>();
        set<id> regFldId = new Set<id>();
        for(CountryChannelUserRegistration__c aRegFld :lstNewUserRegFlds){
            String aKey = String.valueOf(aRegFld.CountryChannels__c)+aRegFld.UserRegFieldName__c;
            System.debug('***** Country Channel Reg ->'+aKey);
            channelID.add(aRegFld.CountryChannels__c);
            if(aRegFld.id != null)
                regFldId.add(aRegFld.id);           
                
            if(!mapNewFlds.containsKey(aKey)){
                mapNewFlds.put(aKey,aRegFld);
                System.debug('**** new entry ***'); 
                
            }   
            else
            {
                aRegFld.addError(System.label.CLAPR15PRM015);           
                System.debug('**** duplicate error ');
            }   
        
        }
        
        //find against saved data
        /*
        System.debug('**** New Fields ->'+mapNewFlds);
        
        List<CountryChannelUserRegistration__c> lstSavedData = [Select id,CountryChannels__c,UserRegFieldName__c from 
                                                                    CountryChannelUserRegistration__c where CountryChannels__c = :channelID AND
                                                                    id not in :regFldId];
                                                                
        for(CountryChannelUserRegistration__c aFld :lstSavedData){
            String savedKey = String.valueOf(aFld.CountryChannels__c)+aFld.UserRegFieldName__c;
            System.debug('**** Saved Data ->'+savedKey);    
            if(mapNewFlds.containsKey(savedKey)){
                System.debug('**** Duplicate Error ****');
                mapNewFlds.get(savedKey).addError(System.label.CLAPR15PRM015);  
            }
            else
                System.debug('**** Not Duplicate Error **** '+savedKey);
        } */      
        
    }

}