@isTest
private class Batch_PRMBadgeBFOProperties_TEST
{
    public static testMethod void testContact (){
        User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c=True;
        insert us;

        System.runAs(us){  
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
            insert france;

            List<Account> accList = PRM_FieldChangeListenerTest.getAccountsInMass(1,france.Id,92000);
            Contact con = PRM_FieldChangeListenerTest.getContact(accList[0]);
            FieloEE__Member__c member = PRM_FieldChangeListenerTest.getFieloEEMember(accList[0].Id);
            con.FieloEE__Member__c=member.Id;
            con.PRMFirstName__c='AKRAMTEST';
            update con;

        
            //Id recordTypeId = Label.CLJUN16PRM057;
            Id recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'ExternalPropertiesCatalog__c' AND DeveloperName ='BFOProperties'].Id;
            ExternalPropertiesCatalog__c  catalog = new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,TableName__c='Contact',Type__c='BFOProperties',PRMCountryFieldName__c='PRMCountry__c',FieldName__c='PRMFirstName__c',SubType__c='ValidEmail',PropertyName__c='ValidEmail',MinimumLengthCheck__c =0);
            insert catalog;
            List<PRMBadgeBFOProperties__c> propList = new List<PRMBadgeBFOProperties__c>();
            propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Equals',Value__c='AKRAMTEST',PRMCountry__c=france.Id,externalPropertiesCatalog__c=catalog.Id));
            insert propList;

            test.startTest();
            Batch_PRMBadgeBFOProperties batchPRMBadgeBFOProperties = new Batch_PRMBadgeBFOProperties('Contact',new List<Id>{propList[0].Id});
            Database.executebatch(batchPRMBadgeBFOProperties);
            test.stopTest();

            List<MemberExternalProperties__c> memProp = [select Id from MemberExternalProperties__c where member__c=:member.Id and ExternalPropertiesCatalog__c=:catalog.Id];
            //System.assertEquals(1,memProp.size());
        }
    }

    public static testMethod void testAccount (){
        User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c=True;
        insert us;

        System.runAs(us){  
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
            insert france;

            List<Account> accList = PRM_FieldChangeListenerTest.getAccountsInMass(1,france.Id,92000);
            Contact con = PRM_FieldChangeListenerTest.getContact(accList[0]);
            FieloEE__Member__c member = PRM_FieldChangeListenerTest.getFieloEEMember(accList[0].Id);
            con.FieloEE__Member__c=member.Id;
            con.PRMFirstName__c='AKRAMTEST';
            update con;

        
            //Id recordTypeId = Label.CLJUN16PRM057;
            Id recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'ExternalPropertiesCatalog__c' AND DeveloperName ='BFOProperties'].Id;
            ExternalPropertiesCatalog__c  catalog = new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,TableName__c=PRM_FieldChangeListener.ACCOUNT_OBJECT_NAME,Type__c='BFOProperties',PRMCountryFieldName__c='PRMCountry__c',FieldName__c='PRMZipCode__c',SubType__c='ValidEmail',PropertyName__c='ValidEmail',MinimumLengthCheck__c =0);
            insert catalog;
            List<PRMBadgeBFOProperties__c> propList = new List<PRMBadgeBFOProperties__c>();
            propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Equals',Value__c='92000',PRMCountry__c=france.Id,externalPropertiesCatalog__c=catalog.Id));
            insert propList;

            test.startTest();
            Batch_PRMBadgeBFOProperties batchPRMBadgeBFOProperties = new Batch_PRMBadgeBFOProperties(PRM_FieldChangeListener.ACCOUNT_OBJECT_NAME,new List<Id>{propList[0].Id});
            Database.executebatch(batchPRMBadgeBFOProperties);
            test.stopTest();

            List<MemberExternalProperties__c> memProp = [select Id from MemberExternalProperties__c where member__c=:member.Id and ExternalPropertiesCatalog__c=:catalog.Id];
            //System.assertEquals(1,memProp.size());
        }
    }


    public static testMethod void testOpportunityProperty (){

        User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c=True;
        insert us;
        System.runAs(us){    
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            
            Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
            insert france;

            List<Account> accList = PRM_FieldChangeListenerTest.getAccountsInMass(1,france.Id,92000);
            Contact con = PRM_FieldChangeListenerTest.getContact(accList[0]);
            FieloEE__Member__c member = PRM_FieldChangeListenerTest.getFieloEEMember(accList[0].Id);
            con.FieloEE__Member__c=member.Id;
            con.PRMFirstName__c='AKRAMTEST';
            update con;

        
            //Id recordTypeId = Label.CLJUN16PRM057;
            Id recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'ExternalPropertiesCatalog__c' AND DeveloperName ='BFOProperties'].Id;
            ExternalPropertiesCatalog__c  catalog = new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,TableName__c=PRM_FieldChangeListener.OPPORTUNITY_OBJECT_NAME,Type__c='BFOProperties',PRMCountryFieldName__c='PRMCountry__c',FieldName__c='LocalOpptyName__c',SubType__c='ValidEmail',PropertyName__c='ValidEmail',MinimumLengthCheck__c =0);
            insert catalog;
            List<PRMBadgeBFOProperties__c> propList = new List<PRMBadgeBFOProperties__c>();
            propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Equals',Value__c='75020',PRMCountry__c=france.Id,externalPropertiesCatalog__c=catalog.Id));
            insert propList;

            Opportunity opp = Utils_TestMethods.createOpportunity(accList[0].Id);
            opp.LocalOpptyName__c ='75020';
            insert opp;

            test.startTest();
            Batch_PRMBadgeBFOProperties batchPRMBadgeBFOProperties = new Batch_PRMBadgeBFOProperties(PRM_FieldChangeListener.OPPORTUNITY_OBJECT_NAME,new List<Id>{propList[0].Id});
            Database.executebatch(batchPRMBadgeBFOProperties);
            test.stopTest();
        }
    }
}