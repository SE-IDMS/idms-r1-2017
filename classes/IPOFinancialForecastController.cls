public with sharing class IPOFinancialForecastController {

    public IPOFinancialForecastController(ApexPages.StandardController controller) {
     isSave = True;
     isEdit = False;

    
    }
    
      
  // Declaration
    
 public map<string,string> Financial_scope  = new map<string,string>();
 public  List<FForecast> Forecast = new List<FForecast>();
 public boolean isSave{get;set;}
 public boolean isEdit{get;set;}
 public string pageid{get;set;}
 public integer i;
 public map<string,decimal> FV = new map<string,decimal>();
 public map<string,decimal> TF = new map<string,decimal>();
 public List<IPO_Financial_Forecast__c> IPOFF = new List<IPO_Financial_Forecast__c>();
 public List<IPOFFColumns__c> FFCS = new List<IPOFFColumns__c>(); 
 public List<FFYear> Year = new List<FFYear>();
 public List<FFYear> CLYear = new List<FFYear>();
 public Map<string,string> ConfLevel = new map<string,string>();
 
 public Map<string,string> getConfLevel(){
 AssignYear();
 For(FFYear CY: Year)
 ConfLevel.put(CY.FYear,''); 
 return ConfLevel;
 }
 
 public Map<string,string> getFinancial_scope(){
 AssignYear();
 For(FFYear CY: Year)
 Financial_scope.put(CY.FYear,''); 
 return Financial_scope;
 }
  
public List<FForecast> getForecast(){
   
    pageid = ApexPages.currentPage().getParameters().get('Id');
    List<FForecast> Fcast = new List<FForecast>();

  
    decimal TempTF;
    string mapid;  
 
        
    IPOFF = [Select Id, Type__c, Year__c, Amount__c, isEditable__c, InFormula__c, Financial_Scope__c,OrderNumber__c,Confidence_Level__c from IPO_Financial_Forecast__c where IPO_Program__c = :pageid order by OrderNumber__c];
    FFCS = [Select Name, Amount__c, Editable__c, inFormula__c, OrderNumber__c from IPOFFColumns__c order by OrderNumber__c];
    
   
    // Assign the Year by calling the function
      AssignYear();
         
           
    // if No records in the Financial Forecast 
    
    if(IPOFF.size() == 0){
        
        For(IPOFFColumns__c CS:FFCS){
         FForecast F = new FForecast();
         F.Type = CS.Name; 
         F.Editable = CS.Editable__c;
         F.InFormula = CS.InFormula__c;
         F.COrder = CS.OrderNumber__c;
         TempTF = 0;
        For(FFYear CYear: Year){
         ConfLevel.put(CYear.Fyear,'');
         Financial_scope.put(CYear.Fyear,'');
         mapid = F.Type + CYear.FYear;
         system.debug('Mapping Id .... ' + mapid);
         FV.put(mapid, CS.Amount__c);
         TempTF = TempTF + CS.Amount__c;
        
         }
            TF.put(F.Type, TempTF); 
            F.Amount = FV;  
            F.TotForecast = TF;
            F.CLevel = ConfLevel;
            F.Fscope = Financial_scope;
            Fcast.add(F);
           
            }
            
            Forecast = Fcast; 
            
     }
     
     else {
      
      For(IPO_Financial_Forecast__c FCst:IPOFF){   
                     
        For(FFYear CYear: Year){ 
        
        if(Decimal.ValueOf(CYear.FYear) == FCst.Year__c){             
             mapid = FCst.Type__c + CYear.FYear;                     
             FV.put(mapid, FCst.Amount__c);
              if(FCst.Confidence_Level__c != null)
                  ConfLevel.put(CYear.FYear,FCst.Confidence_Level__c);                   
              else
                  ConfLevel.put(CYear.FYear,'');                         
                   
                  
              if(FCst.Financial_scope__c != null)
                Financial_scope.put(CYear.FYear, FCst.Financial_Scope__c); 
              else
                 Financial_scope.put(CYear.FYear, ''); 
                                 
              }                     
            }
           }
      // Calculate Total Forecast
      
       
       For(IPOFFColumns__c CS:FFCS){    
       TempTF = 0;          
        For(IPO_Financial_Forecast__c FCst:IPOFF){              
        if(CS.Name == FCst.Type__c){   
         TempTF = TempTF + FV.get(CS.Name + FCst.Year__c);
         System.debug('Temp Value ' + TempTF + ' ' + CS.Name);        
          }
         }
         TF.put(CS.Name,TempTF);
         }
        
               
       For(IPOFFColumns__c CS:FFCS){
         FForecast F = new FForecast();
         F.Type = CS.Name; 
         F.Editable = CS.Editable__c;
         F.InFormula = CS.InFormula__c;
         F.COrder = CS.OrderNumber__c; 
            F.Amount = FV;   
            F.CLevel = ConfLevel;   
            F.Fscope = Financial_scope;       
            F.TotForecast = TF;
            Fcast.add(F);
           
            }
            
            Forecast = Fcast; 
            
     }
            
   
     return Fcast;
   }
  
  

public pagereference EditCostForecast(){

isSave = False;
isEdit = True;
return null;
}

public pagereference CancelCostForecast(){
 isSave = True;
isEdit = False;
return null;
}

public pagereference SaveData(){
system.debug(' Save Data ');
List<IPO_Financial_Forecast__c> FinF = new  List<IPO_Financial_Forecast__c>();
List<IPO_Financial_Forecast__c> FUpdate = new  List<IPO_Financial_Forecast__c>();
map<string,decimal> Value = new map<string,decimal>();
map<string,decimal> CashOut = new map<string,decimal>();
decimal TotAmount;

double COut;
boolean PLCode = True;
 
 
 // Save New Records
 
  AssignYear();

 if(IPOFF.size() == 0){
 
 
 try{
  For(FFYear Y:Year){
    COut = 0;
   For(FForecast NV:Forecast){
         
     IPO_Financial_Forecast__c FF = new IPO_Financial_Forecast__c();
     FF.Type__c = NV.Type;
     FF.IsEditable__c = NV.Editable;
     FF.InFormula__c = NV.InFormula;
     FF.OrderNumber__c = NV.Corder;
     FF.Amount__c = NV.Amount.get(NV.Type + Y.FYear);
     FF.Year__c = Decimal.ValueOf(Y.FYear);
     FF.Confidence_Level__c = NV.CLevel.get(Y.FYear);
     FF.IPO_Program__c = pageid;
     FF.Financial_Scope__c = NV.Fscope.get(Y.FYear);
     FF.CurrencyIsoCode = 'EUR'; 
     system.debug(' Inserting ');
     if (NV.InFormula == True)
       COut = COut + NV.Amount.get(NV.Type + Y.FYear);
     if(NV.Type == 'Cash Out')
       FF.Amount__c = COut;
     else if(NV.Type == 'Total P&L')
       FF.Amount__c = COut - NV.Amount.get('Capex' + Y.FYear);
     FinF.add(FF);
     
 
   TotAmount = NV.Amount.get('P&L Business'+ Y.FYear) + NV.Amount.get('P&L IPO'+Y.Fyear) + NV.Amount.get('P&L Others' + Y.Fyear);
      
      system.debug('Total Amount ' + TotAmount + 'Cmp' + NV.Amount.get('Total P&L' + Y.FYear));
      If((TotAmount  != NV.Amount.get('Total P&L' + Y.FYear)) && TotAmount != 0){
      PLCode = False;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.IPOMessage));
     }
    } 
   }
    if(PLCode==True)
     Insert FinF;
   }Catch(Exception e){
     System.Debug('+++++++++++ Insert Error +++++++++++');
     }
   }
// Update Existing Data

Else {
 system.debug(' Updating ' + IPOFF.size());
try{
For(FFYear Y:Year){
        COut = 0; 
  For(IPO_Financial_Forecast__c FF:IPOFF){ 
               
    For(FForecast NV:Forecast){ 
     if(FF.Type__c == NV.Type && FF.Year__c == Decimal.ValueOf(Y.FYear)){
     FF.IsEditable__c = NV.Editable;
     FF.InFormula__c = NV.InFormula;
     FF.Amount__c = NV.Amount.get(NV.Type + Y.FYear);
     FF.Confidence_Level__c = NV.CLevel.get(Y.FYear);
     FF.CurrencyIsoCode = 'EUR'; 
     FF.Financial_Scope__c = NV.Fscope.get(Y.FYear);
     system.debug(' Updating ' + ConfLevel.get(Y.FYear));
     if (NV.InFormula == True)
       COut = COut + NV.Amount.get(NV.Type + Y.FYear);
     if(NV.Type == 'Cash Out')
       FF.Amount__c = COut;
     else if(NV.Type == 'Total P&L')
       FF.Amount__c = COut - NV.Amount.get('Capex' + Y.FYear);
      FUpdate.add(FF);
      
      TotAmount = NV.Amount.get('P&L Business'+ Y.FYear) + NV.Amount.get('P&L IPO'+Y.Fyear) + NV.Amount.get('P&L Others' + Y.Fyear);
      
      system.debug('Total Amount ' + TotAmount + 'Cmp' + NV.Amount.get('Total P&L' + Y.FYear));
      If((TotAmount  != NV.Amount.get('Total P&L' + Y.FYear)) && TotAmount != 0){
      PLCode = False;
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Total of P&L Business and P&L IPO should be equal to Total P&L'));
     }
      }
     }
     }
        
     }
     if(PLCode == True)
      update FUpdate;
     
 }Catch(Exception e){
   System.Debug('+++++++++++ Update Error +++++++++++' + e);
 }
}
if(PLCode == True){
isSave = True;
isEdit = False;
}
else{
isSave = False;
isEdit = True;
}
return null;
}

public void AssignYear()
{
// Assign the Years
     Year.clear();
     For(i=0; i< Integer.Valueof(Label.IPOFFYear); i++){   
     
     FFYear Y = new FFYear(); 
           Y.FYear = String.Valueof(Integer.Valueof(Label.IPOYear) + i);           
           Year.add(Y);
           }
    }  
public List<FFYear> getYear(){

return Year;
}

public List<FFYear> getCLYear(){
AssignYear();
return Year;
}


public pagereference DeleteRecords()
{

List<IPO_Financial_Forecast__c> FF = new List<IPO_Financial_Forecast__c>();
 For(IPO_Financial_Forecast__c F : IPOFF){
    FF.add(F);
  }
  
  delete FF;
  
  return null;
 }


public class FForecast{

public string  Type{get;set;}
public string Year{get;set;}
public Map<string,decimal> Amount{get;set;}
public Map<string,string> CLevel{get;set;}
public Map<string,string> Fscope{get;set;}
public Map<string,decimal> TotForecast {get;set;}
public boolean Editable{get;set;}
public boolean InFormula{get;set;}
public decimal Corder{get;set;}
}

public class FFYear{
 public string FYear{get;set;}



 }
 


}