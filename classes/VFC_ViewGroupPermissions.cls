public class VFC_ViewGroupPermissions {

    public PermissionSetGroup__c psGroup {get;set;}
    public List<permissionWrapper> objPermWrapperList {get;set;}
    public static Map<String, Schema.DescribeSObjectResult> objectDescribeResultsMap = new Map<String, Schema.DescribeSObjectResult>();
    
    public String objectName {get;set;}
    
    public class permissionWrapper implements Comparable{

       private final String SEPARATOR = ', ';
       private final String CREATE_RIGHT = 'Create';
       private final String READ_RIGHT = 'Read';
       private final String EDIT_RIGHT = 'Edit';
       private final String DELETE_RIGHT = 'Delete';
       private final String VIEWALL_RIGHT = 'View All';
       private final String MODIFYALL_RIGHT = 'Modify All';

       public String objectType {get;set;}
       public String levelofAccess {get;set;}

       public permissionWrapper(ObjectPermissions objPerm) {
            
            if(VFC_ViewGroupPermissions.objectDescribeResultsMap.get(objPerm.sObjectType) != null) {
                objectType = VFC_ViewGroupPermissions.objectDescribeResultsMap.get(objPerm.sObjectType).getLabel();
            }
        
            levelofAccess = '';
        
            if(objPerm.PermissionsModifyAllRecords) {
                levelofAccess = MODIFYALL_RIGHT;
            }
            else {
                levelofAccess   = objPerm.PermissionsCreate           ? addSeparator(levelofAccess) + CREATE_RIGHT : '';
                levelofAccess   += objPerm.PermissionsViewAllRecords  ? addSeparator(levelofAccess) + VIEWALL_RIGHT 
                                                                      : (objPerm.PermissionsRead ? addSeparator(levelofAccess) + READ_RIGHT : '');
                levelofAccess   += objPerm.PermissionsEdit            ? addSeparator(levelofAccess) + EDIT_RIGHT : '';
                levelofAccess   += objPerm.PermissionsDelete          ? addSeparator(levelofAccess) + DELETE_RIGHT : '';                       
            }        
        }
        
        public String addSeparator(String levelofAccess) {
            return levelofAccess.equals('') ? '' : SEPARATOR;
        }
        
        public Integer compareTo(Object compareTo) {
            permissionWrapper wrapper = (permissionWrapper)compareTo;
            
            return objectType.compareTo(wrapper.objectType);
        }
    }

    public VFC_ViewGroupPermissions(ApexPages.StandardController permissionSetGroupController) {
    
        psGroup = [SELECT Id, Name, RequiresValidation__c,  RichDescription__c, TECH_IsAssigned__c, TECH_Status__c,
                   (SELECT Id, PermissionSetId__c, Name FROM PermissionSetGroupMembers__r),                                                                                     
                   MemberList__c, Type__c, Stream__c FROM PermissionSetGroup__c 
                   WHERE Id = :permissionSetGroupController.getId() LIMIT 1];
    
        filter();
    }

    public void filter() {
    
        Map<Id, List<ObjectPermissions>> objPermissionMap = getObjectPermissions(new List<PermissionSetGroup__c>{psGroup});
        
        objPermWrapperList = new List<permissionWrapper>();
        
        for(ObjectPermissions objPerm:objPermissionMap.get(psGroup.Id)) {
            
            permissionWrapper wrapper = new permissionWrapper(objPerm);
            
            if(objectName != null && objectName != '') {
                
                if(wrapper.objectType.contains(objectName.trim())) {
                    objPermWrapperList.add(wrapper);
                }                              
            }
            else {
                objPermWrapperList.add(wrapper);
            }     
        }  
        
        objPermWrapperList.sort();      
    }

    public Map<Id, List<ObjectPermissions>> getObjectPermissions(List<PermissionSetGroup__c> permissionSetGroupList) {
        
        Map<Id, List<ObjectPermissions>> groupPermissionMap = new Map<Id, List<ObjectPermissions>>();
        
        for(PermissionSetGroup__c psGroup:permissionSetGroupList) {
             groupPermissionMap.put(psGroup.Id, new List<ObjectPermissions>());   
        }
        
        Map<Id, PermissionSet> psGroupPermissionSetMap = new Map<Id, PermissionSet>();
        
        for(PermissionSetGroup__c psGroup:permissionSetGroupList) {
            for(PsGroupMember__c psMember:psGroup.PermissionSetGroupMembers__r) {
                psGroupPermissionSetMap.put(psMember.PermissionSetId__c, null);
            }           
        }
        
        Set<Id> keySet = psGroupPermissionSetMap.keySet();
        Set<Id> permissionSetIdSet = new Set<Id>();
        
        String query = 'SELECT Id, (SELECT Id, PermissionsCreate, PermissionsDelete,'
                       + ' PermissionsEdit, PermissionsModifyAllRecords,' 
                       + ' PermissionsRead, PermissionsViewAllRecords,'
                       + ' SobjectType FROM ObjectPerms) FROM PermissionSet' 
                       + ' WHERE Id IN :keySet';
        
        system.debug('Query = ' + query);
        
        for(PermissionSet permissionSet:database.query(query)) {
            psGroupPermissionSetMap.put(permissionSet.Id, permissionSet);
        }

        Set<String> objectNameSet = new Set<String>();

        for(PermissionSetGroup__c psGroup:permissionSetGroupList) {
        
            Map<String, ObjectPermissions> sObjectPermMap = new Map<String, ObjectPermissions>();
        
            for(PsGroupMember__c psMember:psGroup.PermissionSetGroupMembers__r) {
                PermissionSet permissionSet = psGroupPermissionSetMap.get(psMember.PermissionSetId__c);
                
                if(permissionSet != null) {
                    for(ObjectPermissions objPermissions:permissionSet.ObjectPerms) {
                        ObjectPermissions resultMerge = mergeTwoPermissions(objPermissions, sObjectPermMap.get(objPermissions.SobjectType));                    
                        sObjectPermMap.put(resultMerge.sObjectType, resultMerge);
                        objectNameSet.add(resultMerge.sObjectType);
                    }                
                }                
            }   
            
            groupPermissionMap.put(psGroup.Id, sObjectPermMap.values());             
        }
        
        List<String> objectNameList = new List<String>();
        objectNameList.addAll(objectNameSet);

        for(Schema.DescribeSObjectResult objectDescribe:Schema.describeSObjects(objectNameList)) {
            objectDescribeResultsMap.put(objectDescribe.getName(), objectDescribe);
        }
        
        return groupPermissionMap;        
    }
    
    public ObjectPermissions mergeTwoPermissions(ObjectPermissions objPermissionsA, ObjectPermissions objPermissionsB) {

        ObjectPermissions result = objPermissionsA;

        if(objPermissionsA != null && objPermissionsB != null) {
          
            if(objPermissionsA.PermissionsCreate || objPermissionsB.PermissionsCreate) {
                result.PermissionsCreate = true;
            }
            
            if(objPermissionsA.PermissionsRead || objPermissionsB.PermissionsRead) {
                result.PermissionsRead = true;
            }
 
            if(objPermissionsA.PermissionsEdit || objPermissionsB.PermissionsEdit) {
                result.PermissionsEdit = true;
            }
            
            if(objPermissionsA.PermissionsDelete || objPermissionsB.PermissionsDelete) {
                result.PermissionsDelete = true;
            }            

            if(objPermissionsA.PermissionsViewAllRecords || objPermissionsB.PermissionsViewAllRecords) {
                result.PermissionsViewAllRecords = true;
            }          

            if(objPermissionsA.PermissionsModifyAllRecords || objPermissionsB.PermissionsModifyAllRecords) {
                result.PermissionsModifyAllRecords = true;
            }      
          
        }
        else if(objPermissionsB == null && objPermissionsA != null) {
            result = objPermissionsA;
        }
        else if(objPermissionsA == null && objPermissionsB != null) {
            result = objPermissionsB;
        }
        
        return result;
    }
}