/********************************************************************
 * ZuoraProductSelectorRelaunchControlTest
 *
 * Author: Kevin Fabregue - Zuora Europe
 ********************************************************************/


@isTest (SeeAllData=true)
public with sharing class ZuoraProductSelectorRelaunchControlTest {

    private static testMethod void ZuoraProductSelectorRelaunchControlTestMethod() {


        Country__c country = null;
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           country = countries[0];
        }
        else {
           country = new Country__c(Name='France', CountryCode__c='FR');
           insert country;
        }

        Account acc = new Account(Name='Test Acc', SEAccountID__c='123', Tech_CountryCode__c='FR', City__c='Paris', Country__c=country.Id, Street__c = 'street', ZipCode__c='123', Z_ProfileDiscount__c='1234');

        List<Account> accList = new List<Account>();
        accList.add(acc);
        
        insert accList;

        zqu__Quote__c quote = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = '123');
        insert quote;
        //zqu__Quote__c quoteMul = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = '1234', ReferenceQuote__c=quote.Id);
        //insert quoteMul;
/* JPL - 2016-11-23: Put in comment because triggers an Error in Production
         List<zqu__Quote_Template__c> quoteTemplates = new List<zqu__Quote_Template__c>();
         quoteTemplates = [SELECT Id, Correspondence_Language__c, Country__c, Entity__c, Name, zqu__IsDefault__c, zqu__Quote_Type__c, zqu__Template_Id__c from zqu__Quote_Template__c where zqu__IsDefault__c=true and zqu__Quote_Type__c= 'New Subscription'];
         if(!(quoteTemplates.size()>0))
         {
             zqu__Quote_Template__c quoteTemp = new  zqu__Quote_Template__c(Correspondence_Language__c='EN',ZQU__ISDEFAULT__C=true,NAME='TestQuoteTemplate',ZQU__TEMPLATE_ID__C='testTemplateId',COUNTRY__C='FR',ENTITY__C='123');
             insert quoteTemp;
         }
JPL - 2016-11-23 */

        zqu__ZProduct__c pr1 = new zqu__ZProduct__c(Name='Test Product', zqu__ZuoraId__c='1234', zqu__SKU__c='123', FilteringNeeded__c='N', Type__c='Primary', AddonSKUs__c='A.B.C.D.E.F.G.H.I.J.K', zqu__Deleted__c=false, Entity__c='1234');
        zqu__ZProduct__c pr2 = new zqu__ZProduct__c(Name='Test Product', zqu__ZuoraId__c='5678', zqu__SKU__c='123', FilteringNeeded__c='N', Type__c='Standalone', zqu__Deleted__c=false, Entity__c='1234');

        List<zqu__ZProduct__c> prList = new List<zqu__ZProduct__c>{pr1, pr2};

        insert prList;

        zqu__ProductRatePlan__c prp1 = new zqu__ProductRatePlan__c(Country_Rate_Plan__c='FR', DiscountProfile__c='123', zqu__ZuoraId__c='1234', zqu__ZProduct__c=pr1.Id, zqu__Description__c='description', zqu__EffectiveEndDate__c=Date.today().addDays(365));
        zqu__ProductRatePlan__c prp2 = new zqu__ProductRatePlan__c(Country_Rate_Plan__c='FR', DiscountProfile__c='123', zqu__ZuoraId__c='5678', zqu__ZProduct__c=pr2.Id, zqu__EffectiveEndDate__c=Date.today().addDays(365));

        List<zqu__ProductRatePlan__c> prpList = new List<zqu__ProductRatePlan__c>{prp1, prp2};

        insert prpList;

        zqu__QuoteAmendment__c qa = new zqu__QuoteAmendment__c(zqu__Quote__c=quote.Id);

        insert qa;

        zqu__QuoteRatePlan__c qrp1 = new zqu__QuoteRatePlan__c(zqu__Quote__c=quote.Id, zqu__ProductRatePlan__c=prp1.Id, zqu__QuoteAmendment__c=qa.Id);
        zqu__QuoteRatePlan__c qrp2 = new zqu__QuoteRatePlan__c(zqu__Quote__c=quote.Id, zqu__ProductRatePlan__c=prp2.Id, zqu__QuoteAmendment__c=qa.Id);

        insert new List<zqu__QuoteRatePlan__c>{qrp1, qrp2};

        String strSKUs = 'A.B.C.D.E.F.G.H.I.J';

        Test.startTest();
        ZuoraProductSelectorRelaunchController zpsrc = new ZuoraProductSelectorRelaunchController(new ApexPages.StandardController(quote));
        zpsrc.onload();
        ApexPages.currentPage().getParameters().put('quoteType', 'Amendment');
        //ApexPages.currentPage().getParameters().put('AmendOption', 'DowngradeProduct');
        ApexPages.currentPage().getParameters().put('AmendOption', 'UpgradeProduct');
        zpsrc.onload();
        zpsrc.navigateBack();
        zpsrc.next();
        //ZuoraProductSelectorRelaunchController.retrieveRatePlansFromReferenceQuote(quoteMul);
        ZuoraProductSelectorRelaunchController.updateQuoteAddOnSkus(quote, strSKUs);
        ZuoraProductSelectorRelaunchController.updateQuoteDowngradeSkus(quote, strSKUs);
        ZuoraProductSelectorRelaunchController.updateQuoteUpgradeSkus(quote, strSKUs);
        ZuoraProductSelectorRelaunchController.faketest();
        Test.stopTest();
    }
}