/* 
@Author: Ramu Veligeti 
Date: 16/07/2013 
Description: This class performs following actions: 
Method CreateWOG: Creates a new Work Order Group record if a new Parent WO is created, 
                  Updates the WorkOrderGroup Lookup with the Parent WO's WorkOrderGroup Id if it is a child WO. 
Method UpdateWOG: Updates the Work Order Group's status based on the WO's status. 
*/ 
public class ServiceMaxWOG 
{ 
    public static void CreateWOG(List<SVMXC__Service_Order__c> wolst) 
    { 
        List<WorkOrderGroup__c> woglst = new List<WorkOrderGroup__c>(); 
        List<SVMXC__Service_Order__c> svopm = new List<SVMXC__Service_Order__c>(); 
        List<SVMXC__Service_Order__c> svo = new List<SVMXC__Service_Order__c>(); 
        List<SVMXC__Service_Order__c> svo1 = new List<SVMXC__Service_Order__c>(); 
        Set<Id> wogId = new Set<Id>(); 
        Set<Id> pmId = new Set<Id>(); 
        Map<String,Id> wogMap = new Map<String,Id>(); 
    Map<String,Id> wogMap2 = new Map<String,Id>(); 
        Map<Id,SVMXC__Service_Order__c> wogd = new Map<Id,SVMXC__Service_Order__c>(); 
        Set<String> distwog = new Set<String>(); 
        Set<String> distwog2 = new Set<String>(); // Hari Added
         string WOconnectorRT = System.Label.CLAPR15SRV77; 
    /*RecordType rt =[SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType where SobjectType='SVMXC__Service_Order__c' and DeveloperName ='WO_ConnectedWorkOrder'];*/ 
        
        for(SVMXC__Service_Order__c wo:wolst) 
        { 
            //if(!distwog.contains(wo.BackOfficeReference__c+wo.BackOfficeSystem__c) && wo.WorkOrderGroup__c==null && wo.Parent_Work_Order__c==null && (wo.Work_Order_Notification__c==null || wo.Work_Order_Notification__r.WorkOrderGroup__c==null)) 
            
            if(!distwog.contains(wo.BackOfficeReference__c+wo.BackOfficeSystem__c) && wo.WorkOrderGroup__c==null && wo.Parent_Work_Order__c==null && wo.Work_Order_Notification__c==null  || (wo.Work_Order_Notification__c !=null && wo.WorkOrderGroup__c==null)) 
            { 
              System.debug('\n CLog: ************************************************************* ServiceMaxWOG : CreateWOG *************************'); 
                WorkOrderGroup__c wog = new WorkOrderGroup__c(); 
                wog.Account__c = wo.SVMXC__Company__c; 
                wog.SoldTo__c = wo.SoldToAccount__c; 
                wog.ServiceMaintenanceContract__c = wo.SVMXC__Service_Contract__c; 
                System.debug('\n CLog: ******  '+wo.BackOfficeReference__c+' ******** '+wo.BackOfficeSystem__c); 
                // Modefied by Hari for DEF-5773 
                if(wo.RecordTypeId == WOconnectorRT){ 
                  wog.BackOfficeReference__c = wo.TECH_WOGBackOfficeReference__c; 
                } 
                else{ 
                  wog.BackOfficeReference__c = wo.BackOfficeReference__c; 
                } 
        
                wog.CountryofBackOffice__c = wo.CountryofBackOffice__c; 
                wog.BackOfficeSystem__c = wo.BackOfficeSystem__c; 
                //wog.WOGName__c = 'Test'; 
                if(wo.SVMXC__Order_Status__c =='New' || wo.SVMXC__Order_Status__c =='Unscheduled' || wo.SVMXC__Order_Status__c =='Scheduled' || wo.SVMXC__Order_Status__c =='WIP' || wo.SVMXC__Order_Status__c =='Acknowledge FSE' || wo.SVMXC__Order_Status__c =='Customer Confirmed') 
                    wog.Status__c = 'Open'; 
                else if(wo.SVMXC__Order_Status__c =='Service Delivered' || wo.SVMXC__Order_Status__c =='Service Complete') 
                    wog.Status__c = 'Completed'; 
                else if(wo.SVMXC__Order_Status__c == 'Cancelled') 
                    wog.Status__c = 'Cancelled'; 
                else if(wo.SVMXC__Order_Status__c == 'Closed') 
                    wog.Status__c = 'Closed'; 
              
                System.debug('\n hari log : '+wog);
                
                if(wo.RecordTypeId != WOconnectorRT)
                {
                      woglst.add(wog); 
                }               
                else if(!distwog2.contains(wog.BackOfficeReference__c)  && wo.RecordTypeId == WOconnectorRT)// Hari Added
                {
                      woglst.add(wog); 
                }
                svo.add(wo); 
                System.debug('\n hari log : '+wo.BackOfficeReference__c+wo.BackOfficeSystem__c);
                distwog.add(wo.BackOfficeReference__c+wo.BackOfficeSystem__c);
                distwog2.add(wog.BackOfficeReference__c);               
                System.debug('\n hari log : '+distwog);
            } 
            else if(wo.WorkOrderGroup__c!=null || (wo.Parent_Work_Order__c!=null && wo.ParentWorkOrderGroup__c!=null)) 
            { 
                if(wo.WorkOrderGroup__c==null && wo.ParentWorkOrderGroup__c!=null) 
                { 
                    wo.WorkOrderGroup__c = (ID)wo.ParentWorkOrderGroup__c; 
                    wogId.add((ID)wo.ParentWorkOrderGroup__c); 
                    if(wo.BackOfficeReference__c!=null) wogd.put((ID)wo.ParentWorkOrderGroup__c,wo); 
                } 
                  
                if(wo.WorkOrderGroup__c!=null) 
                { 
                    wogId.add(wo.WorkOrderGroup__c); 
                    if(wo.BackOfficeReference__c!=null) wogd.put(wo.WorkOrderGroup__c,wo); 
                } 
                
            } 
            else if(wo.SVMXC__PM_Plan__c!=null && wo.WorkOrderGroup__c==null) 
            { 
                pmId.add(wo.SVMXC__PM_Plan__c); 
                svopm.add(wo); 
            } 
        } 
        
        if(pmId.size()>0) UpdateWOGPMPlan(pmId,svopm); 
        
        if(woglst.size()>0) 
        { 
            insert woglst; 
            for(WorkOrderGroup__c wg: woglst){ 
        
                wogMap.put(wg.BackOfficeReference__c+wg.BackOfficeSystem__c,wg.Id); 
        } 
                System.debug('\n CLog: ****** '+wogMap); 
                //wogMap.put(String.valueOf(wg.Account__c)+String.valueOf(wg.SoldTo__c)+String.valueOf(wg.ServiceMaintenanceContract__c)+String.valueOf(wg.BackOfficeReference__c),wg.Id); 
              System.debug('\n CLog: ****** '+wolst.size()); 
              System.debug('\n CLog: ****** '+wolst); 
            //for(SVMXC__Service_Order__c wo: svo) 
            for(SVMXC__Service_Order__c wo: wolst) 
            { 
                if(wo.RecordTypeId == WOconnectorRT) 
                { 
                  if(wogMap.containsKey(wo.TECH_WOGBackOfficeReference__c+wo.BackOfficeSystem__c)) 
                  { 
                    wo.WorkOrderGroup__c = wogMap.get(wo.TECH_WOGBackOfficeReference__c+wo.BackOfficeSystem__c); 
                  } 
                } 
                else{ 
                  if(wogMap.containsKey(wo.BackOfficeReference__c+wo.BackOfficeSystem__c)) 
                  { 
                    wo.WorkOrderGroup__c = wogMap.get(wo.BackOfficeReference__c+wo.BackOfficeSystem__c); 
                  } 
               } 
                
        
                System.debug('\n CLog: ****** '+wo); 
            } 
        } 
        
        if(wogId.size()>0) UpdateWOGStatus(wogId); 
        if(wogd.size()>0) UpdateWOGFields(wogd); 
    } 
    
    public static void UpdateWOG(List<SVMXC__Service_Order__c> wolst,Map<Id,SVMXC__Service_Order__c> oldMap) 
    { 
        Set<Id> wogId = new Set<Id>(); 
        Map<Id,SVMXC__Service_Order__c> wogd = new Map<Id,SVMXC__Service_Order__c>(); 
        
        for(SVMXC__Service_Order__c wo: wolst) 
        { 
            if(oldMap.size()>0 && oldMap.containsKey(wo.Id) && wo.SVMXC__Order_Status__c <> oldMap.get(wo.Id).SVMXC__Order_Status__c && wo.WorkOrderGroup__c!=null) 
                wogId.add(wo.WorkOrderGroup__c); 
            else if(oldMap.size()==0 && wo.SVMXC__Order_Status__c!=null && wo.WorkOrderGroup__c!=null) 
                wogId.add(wo.WorkOrderGroup__c); 
            else if(oldMap.size()>0 && oldMap.containsKey(wo.Id) && wo.WorkOrderGroup__c <> oldMap.get(wo.Id).WorkOrderGroup__c) 
                wogId.add(oldMap.get(wo.Id).WorkOrderGroup__c); 
                
            if(wo.WorkOrderGroup__c==null && wo.ParentWorkOrderGroup__c!=null && wo.BackOfficeReference__c!=null) wogd.put((ID)wo.ParentWorkOrderGroup__c,wo); 
            if(wo.WorkOrderGroup__c!=null && wo.BackOfficeReference__c!=null) wogd.put(wo.WorkOrderGroup__c,wo); 
        } 
        
        if(wogId.size()>0) UpdateWOGStatus(wogId); 
        if(wogd.size()>0) UpdateWOGFields(wogd); 
    } 
    
    public static void UpdateWOGStatus(Set<Id> wogId) 
    { 
        List<WorkOrderGroup__c> wglst = [Select Id, Status__c, (select Id,SVMXC__Order_Status__c from WorkOrders__r) from WorkOrderGroup__c where Id in :wogId]; 
        
        for(WorkOrderGroup__c wg: wglst) 
        { 
            List<String> openstat = new List<String>(); 
            List<String> compstat = new List<String>(); 
            List<String> cancstat = new List<String>(); 
            List<String> closstat = new List<String>(); 
            List<String> valdstat = new List<String>(); 
  
            for(SVMXC__Service_Order__c so: wg.WorkOrders__r) 
            { 
                if(so.SVMXC__Order_Status__c =='New' || so.SVMXC__Order_Status__c =='Unscheduled' || so.SVMXC__Order_Status__c =='Scheduled' || so.SVMXC__Order_Status__c =='WIP' || so.SVMXC__Order_Status__c =='Acknowledge FSE' || so.SVMXC__Order_Status__c =='Customer Confirmed') 
                    openstat.add(so.SVMXC__Order_Status__c); 
                else if(so.SVMXC__Order_Status__c =='Service Delivered' || so.SVMXC__Order_Status__c =='Service Complete') 
                    compstat.add(so.SVMXC__Order_Status__c); 
                else if(so.SVMXC__Order_Status__c == 'Service Validated') 
                    valdstat.add(so.SVMXC__Order_Status__c); 
                else if(so.SVMXC__Order_Status__c == 'Closed') 
                    closstat.add(so.SVMXC__Order_Status__c); 
                else if(so.SVMXC__Order_Status__c == 'Cancelled') 
                    cancstat.add(so.SVMXC__Order_Status__c); 
            } 
            
            if(openstat.size()>0) wg.Status__c = 'Open'; 
            else if(compstat.size()>0) wg.Status__c = 'Completed'; 
            else if(valdstat.size()>0 && openstat.size()==0 && compstat.size()==0) wg.Status__c = 'Validated'; 
            else if((closstat.size()>0 || compstat.size()>0) && openstat.size()==0 && compstat.size()==0 && valdstat.size()==0) wg.Status__c = 'Closed'; 
            else if(cancstat.size()>0 && openstat.size()==0 && compstat.size()==0 && closstat.size()==0 && valdstat.size()==0) wg.Status__c = 'Cancelled'; 
            system.debug('wg.WorkOrders__r.size()=='+wg.WorkOrders__r.size()); 
            if(wg.WorkOrders__r.size()==0) wg.Status__c = 'Cancelled'; 
        } 
        
        if(wglst.size()>0) update wglst; 
    } 
    
    public static void UpdateWOGPMPlan(Set<Id> pmId,List<SVMXC__Service_Order__c> svopm) 
   { 
        //Map<Id,Id> wopm = new Map<Id,Id>(); 
        Map<String,Id> wopm = new Map<String,Id>(); 
        List<WorkOrderGroup__c> woglst = new List<WorkOrderGroup__c>(); 
        Map<String,Id> wogMap = new Map<String,Id>(); 
        
        for(SVMXC__Service_Order__c sv : [Select Id,SVMXC__PM_Plan__c,SVMXC__PM_SC__c,WorkOrderGroup__c,CreatedDate from SVMXC__Service_Order__c where SVMXC__PM_Plan__c IN :pmId and WorkOrderGroup__c!=null]) 
        { 
            wopm.put(sv.SVMXC__PM_Plan__c+sv.SVMXC__PM_SC__c+sv.CreatedDate.date(), sv.WorkOrderGroup__c); 
        } 
        
        for(SVMXC__Service_Order__c wo: svopm) 
        { 
            if(wopm.containsKey(wo.SVMXC__PM_Plan__c)) 
                wo.WorkOrderGroup__c = wopm.get(wo.SVMXC__PM_Plan__c+wo.SVMXC__PM_SC__c+system.today()); 
            else 
            { 
                WorkOrderGroup__c wog = new WorkOrderGroup__c(); 
                wog.Account__c = wo.SVMXC__Company__c; 
                wog.SoldTo__c = wo.SoldToAccount__c; 
                wog.ServiceMaintenanceContract__c = wo.SVMXC__Service_Contract__c; 
                wog.BackOfficeReference__c = wo.BackOfficeReference__c; 
                wog.WOGName__c = 'Test'; 
                wog.WO_PM_Plan__c = wo.SVMXC__PM_Plan__c+wo.SVMXC__PM_SC__c+system.today(); 
                if(wo.SVMXC__Order_Status__c =='New' || wo.SVMXC__Order_Status__c =='Unscheduled' || wo.SVMXC__Order_Status__c =='Scheduled' || wo.SVMXC__Order_Status__c =='WIP' || wo.SVMXC__Order_Status__c =='Acknowledge FSE' || wo.SVMXC__Order_Status__c =='Customer Confirmed') 
                    wog.Status__c = 'Open'; 
                else if(wo.SVMXC__Order_Status__c =='Service Delivered' || wo.SVMXC__Order_Status__c =='Service Complete') 
                    wog.Status__c = 'Completed'; 
                else if(wo.SVMXC__Order_Status__c == 'Cancelled') 
                    wog.Status__c = 'Cancelled'; 
                else if(wo.SVMXC__Order_Status__c == 'Closed') 
                    wog.Status__c = 'Closed'; 
                woglst.add(wog); 
            } 
        } 
        
        if(woglst.size()>0) 
        { 
            insert woglst; 
            
            for(WorkOrderGroup__c wg: woglst) 
                wogMap.put(wg.WO_PM_Plan__c,wg.Id); 
  
            for(SVMXC__Service_Order__c wo: svopm) 
                if(wogMap.containsKey(wo.SVMXC__PM_Plan__c+wo.SVMXC__PM_SC__c+system.today())) 
                    wo.WorkOrderGroup__c = wogMap.get(wo.SVMXC__PM_Plan__c+wo.SVMXC__PM_SC__c+system.today()); 
        } 
    } 
    
    public static void UpdateWOGFields(Map<Id,SVMXC__Service_Order__c> wogId) 
    { 
        if( (wogId.size() > 0) && !(wogId.isempty()) ) //Added by VISHNU C for July 2016 Release Resolving Too Many SOQL Error in WO Interface
        {
            List<WorkOrderGroup__c> wrg = [Select Id,BackOfficeReference__c, BackOfficeSystem__c,CountryofBackOffice__c from WorkOrderGroup__c where Id in :wogId.keySet() and BackOfficeReference__c=null]; 
            system.debug('wogId=='+wogId+'=='+wrg); 
            for(WorkOrderGroup__c wg: wrg) 
            { 
                wg.BackOfficeSystem__c = wogId.get(wg.Id).BackOfficeSystem__c; 
                wg.BackOfficeReference__c = wogId.get(wg.Id).BackOfficeReference__c; 
                wg.CountryofBackOffice__c = wogId.get(wg.Id).CountryofBackOffice__c; 
            } 
            
            if(wrg.size()>0) update wrg; 
        }
    } 
}