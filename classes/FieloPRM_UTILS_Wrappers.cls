/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 10/08/2016
* Description: 
********************************************************************/

public class FieloPRM_UTILS_Wrappers{
    
    public class MemberWrapper{
        public String id {get;set;}
        public String CountryName {get;set;}
        public String CountryCode {get;set;}
        public String primaryChannel {get;set;}
        public String allChannelsCode {get;set;}
        public String numericId {get;set;}
        public String globalContactPPC {get;set;}
        public Integer points {get;set;}
        
        public MemberWrapper(){
            this.id = '';
            this.countryCode = '';
            this.countryName = '';
            this.primaryChannel = '';
        }
        
        public MemberWrapper(FieloEE__Member__c member){
            this.id = member.id;
            this.countryCode = member.F_PRM_CountryCode__c;
            this.countryName = member.F_CountryName__c;
            this.primaryChannel = member.F_PRM_PrimaryChannel__c;
            this.allChannelsCode =  member.F_PRM_Channels__c;
            this.numericId =  member.Name!=null?member.Name.subString(2):'';
            this.globalContactPPC =  String.valueOf(member.F_PRM_PrimaryContact__c);
            this.points = member.FieloEE__Points__c!= null ? member.FieloEE__Points__c.intValue() : null;
        }
    }
    
    public class ContactWrapper{
        public String name {get;set;}
        public String email {get;set;}
        public String mobilePhone {get;set;}
        public String workPhone {get;set;}
        public string jobTitle {get;set;}
        public string address {get;set;}
        public string conLanguage {get;set;}
        public string accountName {get;set;}
    
        public ContactWrapper(){
            this.name = '';
            this.email = '';
            this.mobilePhone = '';
            this.workPhone = '';
            this.jobTitle = '';
            this.address = '';
            this.conLanguage = '';
            this.accountName = '';
        }
        public ContactWrapper(String p_name, String p_email, String p_mobilePhone, String p_workPhone, String p_jobTitle, String p_address, String p_conLanguage, String p_accountName){
            this.name = p_name;
            this.email = p_email;
            this.mobilePhone = p_mobilePhone;
            this.workPhone = p_workPhone;
            this.jobTitle = p_jobTitle;
            this.address = p_address;  
            this.conLanguage = p_conLanguage;
            this.accountName = p_accountName;
        }
    }
    
}