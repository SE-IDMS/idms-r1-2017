/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: Test Class                                          *
* Created Date: 14.04.2014                                  *
* Tested Class: FieloPRM_UTILS_ProgramMethods               *
************************************************************/

@isTest
public with sharing class FieloPRM_UTILS_ProgramMethodsTest{
  
    public static testMethod void testUnit(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            /*FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;*/
            //Insert Member
            FieloEE__Program__c program = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
            
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            FieloPRM_UTILS_ProgramMethods.getProgramByContact(acc.Id);
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c= 'Ichazo',
                FieloEE__FirstName__c = 'Ramiro',
                FieloEE__Street__c = 'test',
                FieloEE__Program__c = program.Id,
                F_Account__c = acc.id
            );
            insert member;
            
            Contact con = new Contact(
                FieloEE__Member__c = member.Id,
                LastName = 'Test',
                FirstName = 'TestName'
            );
            insert con;
            
            FieloPRM_UTILS_ProgramMethods.getProgramByContact(con.Id);
        }
        
    }
}