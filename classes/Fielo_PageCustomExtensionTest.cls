/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: Test Class                                          *
* Created Date: 28.07.2014                                  *
* Tested Class:  Fielo_PageCustomExtension                  *
************************************************************/

@isTest
public with sharing class Fielo_PageCustomExtensionTest {
  
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        
        Account acc= new Account(
            Name = 'test',
            Street__c = 'Some Street',
            ZipCode__c = '012345'
        );
        insert acc;
                
        FieloEE__Member__c mem = new FieloEE__Member__c(
            FieloEE__FirstName__c = 'test',
            FieloEE__LastName__c= 'test'
        );
        insert mem;
    
        FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
        insert memb;
        
        Fielo_PageCustomExtension test1 = new Fielo_PageCustomExtension(new FieloEE.PageController());
        test1 = new Fielo_PageCustomExtension(new FieloEE.ChangePasswordFieloController());
        test1 = new Fielo_PageCustomExtension(new FieloEE.RegisterStep1Controller());
        test1 = new Fielo_PageCustomExtension(new Fielo_InvoiceUploadController ());
        test1.goToActivities();
        
        test1 = new Fielo_PageCustomExtension(new ApexPages.StandardController(new Case()));
        
        ApexPages.currentPage().getParameters().put('Nombre', 'Ramiro');
        ApexPages.currentPage().getParameters().put('Apellido', 'Ichazo');
        String aux = test1.getParameters();
        
        Fielo_PageCustomExtension test2 = new Fielo_PageCustomExtension(new FieloEE.ShoppingCartItemsController());
        try{test2.doVoucherCustom();}catch(Exception e){}
    }
}