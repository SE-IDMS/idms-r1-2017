public class SVMXC_InstalledProductAfter {

    public static void createWDLFromInstalledProduct (List<SVMXC__Installed_Product__c> triggerNew, Map<Id, SVMXC__Installed_Product__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to create Work Detail Line on the creation of a new Installed Product (that has reference to WO) - Written : May 5, 2015
        */
    
        Set<Id> ipSet = new Set<Id>();
        
        for (SVMXC__Installed_Product__c ip : triggerNew) {
            if (ip.WorkOrder__c != null && isValidId(ip.WorkOrder__c))
                ipSet.add(ip.Id);
        }
        
        if (!ipSet.isEmpty()) {
            
            Id ipServicedRT = Schema.SOBjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Installed Products Serviced').getRecordTypeId();
            List<SVMXC__Service_Order_Line__c> newServiceLines = new List<SVMXC__Service_Order_Line__c>();
            
            for (SVMXC__Installed_Product__c ip2 : triggerNew) {
                
                if (ipSet.contains(ip2.Id)) {
                    
                    // create WDL for each Installed Product that had a WO reference
                    SVMXC__Service_Order_Line__c newLine = new SVMXC__Service_Order_Line__c();
    
                    newLine.RecordTypeId = ipServicedRT;
                    newLine.SVMXC__Service_Order__c = ip2.WorkOrder__c;
                    newLine.SVMXC__Serial_Number__c = ip2.Id;
                    
                    newServiceLines.add(newLine);
                }
            }
            if (!newServiceLines.isEmpty())
                insert newServiceLines;     
        }
    }
    
    public static void createAssetLinkFromInstalledProduct (List<SVMXC__Installed_Product__c> triggerNew, Map<Id, SVMXC__Installed_Product__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to create Asset Link record for newly inserted Installed Product that has a Parent relationship - Written : June 10, 2015
            NOTE : These is currently no functionality to update related Asset Link records when the related IP is updated.
        */
    
    	List<JctAssetLink__c> newAssetLinks = new List<JctAssetLink__c>();
    	
    	for (SVMXC__Installed_Product__c ip : triggerNew) {
    		if (ip.SVMXC__Parent__c != null) {
    			JctAssetLink__c newAssetLink = new JctAssetLink__c();
    			newAssetLink.LinkToAsset__c = ip.SVMXC__Parent__c; // upstream
    			newAssetLink.InstalledProduct__c = ip.Id; // downstream
    			// AS_LINK_1 equals the English translated value of "Child"
    			newAssetLink.LinkType__c = 'AS_LINK_1';
    			newAssetLink.UpdateRelatedIPonInsert__c = false;
    			newAssetLinks.add(newAssetLink);		
    		}	
    	}
    	
    	if (!newAssetLinks.isEmpty())
    		insert newAssetLinks;
    }
    
    private static Boolean isValidId(String strId) {
    	Boolean isValid = (strId InstanceOf ID) ? true : false ;
    return isValid ;
	}
}