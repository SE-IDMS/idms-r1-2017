@IsTest
private class AP_PartnerContactAssignMigrationTest {
    @testSetup static void PartnerCommunityUItestsetup(){
        List<BypassTriggersOnLeadConversion__c> BypassTriggersOnLeadConversion = new List<BypassTriggersOnLeadConversion__c>{
            new BypassTriggersOnLeadConversion__c(Name='AP_PRL_CountryLevelUpdate'),
            new BypassTriggersOnLeadConversion__c(Name='AP_ProgramLevelHandler'),
            new BypassTriggersOnLeadConversion__c(Name='AP_PartnerPRG'),
            new BypassTriggersOnLeadConversion__c(Name='AP_updateContactOwner'),
            new BypassTriggersOnLeadConversion__c(Name='AP_ContactPreferredAgent'),
            new BypassTriggersOnLeadConversion__c(Name='AP_CONCreateWithSEContactID'),
            new BypassTriggersOnLeadConversion__c(Name='AP54'),
            new BypassTriggersOnLeadConversion__c(Name='Handler_ContactAfterUpdate'),
            new BypassTriggersOnLeadConversion__c(Name='AP_AAP_AccountProgramUpdate'),
            new BypassTriggersOnLeadConversion__c(Name='AP_ContactBeforeUpdateHandler'),
            new BypassTriggersOnLeadConversion__c(Name='AP_AccountRecordUpdate'),
            new BypassTriggersOnLeadConversion__c(Name='AP_ACCCreateWithSEAccountId'),
            new BypassTriggersOnLeadConversion__c(Name='AP_AccountBeforeInsertHandler'),
            new BypassTriggersOnLeadConversion__c(Name='AP30'),
            new BypassTriggersOnLeadConversion__c(Name='AP44'),
            new BypassTriggersOnLeadConversion__c(Name='AP_ACC_PartnerAccountUpdate'),
            new BypassTriggersOnLeadConversion__c(Name='AP_Contact_PartnerUserUpdate'),
            new BypassTriggersOnLeadConversion__c(Name='FieloPRM_AP_AccountTriggers'),
            new BypassTriggersOnLeadConversion__c(Name='Fielo_ContactTriggers'),
            new BypassTriggersOnLeadConversion__c(Name='PartnerLocatorPushEventsService'),
            new BypassTriggersOnLeadConversion__c(Name='PRM_FieldChangeListener'),
            new BypassTriggersOnLeadConversion__c(Name='SyncObjects'),
            new BypassTriggersOnLeadConversion__c(Name='AP25'),
            new BypassTriggersOnLeadConversion__c(Name='AccountProgramBeforeInsert'),
            new BypassTriggersOnLeadConversion__c(Name='AP_UserActions')
        };
        insert BypassTriggersOnLeadConversion;
    }
    static testMethod void testAssignement(){
        Utils_SDF_Methodology.BypassOnLeadConvertion = true;
        User newUser = Utils_TestMethods.createStandardUser('TestUser');  
        newUser.BypassVR__c = TRUE;
        //newUser.BypassTriggers__c = 'AP_PRL_CountryLevelUpdate;AP_ProgramLevelHandler;AP_PartnerPRG;AP_updateContactOwner;AP_ContactPreferredAgent;AP_CONCreateWithSEContactID;AP54;Handler_ContactAfterUpdate;AP_AAP_AccountProgramUpdate;AP_ContactBeforeUpdateHandler;AP_AccountRecordUpdate;AP_ACCCreateWithSEAccountId;AP_AccountBeforeInsertHandler;AP30;AP44;AP_ACC_PartnerAccountUpdate;AP_Contact_PartnerUserUpdate;FieloPRM_AP_AccountTriggers;Fielo_ContactTriggers;PartnerLocatorPushEventsService;PRM_FieldChangeListener;SyncObjects;AP25;AccountProgramBeforeInsert';
        insert newUser;
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='US';
        insert country;
        
        Country__c country1 = Utils_TestMethods.createCountry();
        country1.countrycode__c ='FR';
        insert country1;

        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
        newRecTypeMap.name = recCatalogActivityReq.Id;
        newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
        newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;

        insert newRecTypeMap;
        
        //creates a global partner program
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;

        //creates a program level for the global program
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;

        //creates a country partner program
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;

        //creates a program level for the country partner program
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;
        
        ProgramLevel__c prgL2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        prgL2.Name = 'PrgLevel2';
        prgL2.levelstatus__c = 'active';
        insert prgL2;


        //Creates a migration extenal properties
        ExternalPropertiesCatalogType__c epcExternalSystemType = new ExternalPropertiesCatalogType__c(Source__c='BFO',Type__c='MigrationToProgramV2',TypeOfSource__c='BFO');
        insert epcExternalSystemType;
        Id ExternalPropCatalogRTId = [SELECT Id FROM RecordType WHERE sObjectType = 'ExternalPropertiesCatalog__c' AND DeveloperName = 'ExternalProperties'].Id;
        ExternalPropertiesCatalog__c epcMigration =  new ExternalPropertiesCatalog__c(RecordTypeId=ExternalPropCatalogRTId,ExternalPropertiesCatalogType__c=epcExternalSystemType.Id,PropertyName__c='test prop',Subtype__c=prg2.Id);
        insert epcMigration;

        prg2.PropertiesAndActivitiesCatalog__c = epcMigration.Id;
        prg2.levelstatus__c = 'active';
        update prg2;
            
        system.runAs(newUser){
            Account acc = Utils_TestMethods.createAccount();
            acc.country__c = country.id;
            Insert acc;

            acc.isPartner = true;
            acc.PRMAccount__c = True;
            acc.PRMUIMSId__c = acc.Id;
            update acc;

            Contact contact = Utils_TestMethods.createContact(acc.Id, 'Test Contact');
            contact.PRMContact__c = true;
            contact.PRMUIMSId__c = acc.Id;
            Insert contact;

            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloEE__Program__c prog = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'TestAkram';
            member.FieloEE__FirstName__c = 'Akram';
            member.FieloEE__Street__c = 'test';
            member.FieloEE__Program__c = prog.Id;
            member.F_Account__c = acc.Id;
            insert member;

            contact.FieloEE__Member__c = member.Id;
            update contact;
            Test.startTest();
            ACC_PartnerProgram__c accPgm = Utils_TestMethods.createAccountProgram(prg2.id, cp1.id, acc.id);
            //acc.TECH_UniqueAssignedId__c = 
            insert accPgm;
            accPgm.active__c = true;
            update accPgm;

            map<Id,ContactAssignedProgram__c> mapContPrg = new map<Id,ContactAssignedProgram__c>();
            ContactAssignedProgram__c contactAssignedProgram = new ContactAssignedProgram__c();
            contactAssignedProgram.AccountAssignedProgram__c = accPgm.Id;
            contactAssignedProgram.Active__c = True;
            contactAssignedProgram.Contact__c = contact.Id;
            contactAssignedProgram.PartnerProgram__c = cp1.id;
            contactAssignedProgram.ProgramLevel__c = prg2.id;
            insert contactAssignedProgram;
            
            contactAssignedProgram.ProgramLevel__c = prgL2.id;
            contactAssignedProgram.Active__c = false;
            Update contactAssignedProgram;
            
            mapContPrg.put(contactAssignedProgram.id,contactAssignedProgram);
            AP_PartnerContactAssignementMigration.deleteMemberProperties(mapContPrg);

            //List<MemberExternalProperties__c> mepList = [SELECT Id FROM MemberExternalProperties__c WHERE Member__c=:member.Id];
            //System.assertEquals(1, mepList.size());

            delete contactAssignedProgram;
            System.debug('Total Number of SOQL Queries allowed in this apex code context: ' + Limits.getLimitQueries());
            //mepList = [SELECT Id FROM MemberExternalProperties__c WHERE Member__c=:member.Id];
            //System.assertEquals(0, mepList.size());
        }
        Test.stopTest();
        
    }
    
}