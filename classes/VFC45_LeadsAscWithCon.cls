/*
    Author          : Accenture Team
    Date Created    : 18/07/2011 
    Description     : Controller extensions for VFP45_LeadsAscWithCon.This class Access the 
                      corresponding Contact record via the standard Contact controller method.
    18/Apr/2012    Srinivas Nallapati    updated the SOQL added "order by Status asc"
    06/Aug/2012    Srinivas Nallapati    Updated for modifying the columns DEF-0661
    30/Oct/2012    Srinivas Nallapati    New Lead from the Leads related list on ContactBR-1858
    30/Oct/2012    Srinivas Nallapati    Removed the Catrgory column, replace it with Campaign Name  BR-1911
*/ 
/*
   26/Jun/2014    Cecile LARTAUD    Update: Handle compatibility of links behavior in the console.
*/
public with sharing class VFC45_LeadsAscWithCon extends VFC_ControllerBase
{
    //Iniatialize variables
    public Contact con{get;private set;}
    public List<Lead> leads{get;set;}
    public List<DataTemplate__c> dataTemplates {get;set;}
    public List<SelectOption> columns{get;set;}
    public String GotoNewLeadUrl;
    //Constructor
    public VFC45_LeadsAscWithCon(ApexPages.StandardController controller) 
    {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC45_LeadsAscWithCon******');  
        con= (Contact)controller.getRecord();
        leads = new List<Lead>();
        dataTemplates = new List<DataTemplate__c>();
        columns = new List<SelectOption>();
        System.Debug('****** Initializing the Properties and Variables Ends for VFC45_LeadsAscWithCon******'); 
    }
    
      /* This method is called on the load of VFP45_LeadsAscWithCon page. This method 
         queries the Converted & unconverted leads related to the corresponding contact 
         and displays the leads lists or a message if no Leads associated.
      */  
    
    public pagereference displayLeads()
    {
        System.Debug('****** Querying of Leads Begins ******');  
        if(con!=null)
        {
            leads = [select Tech_CampaignName__c, Timeframe__c,SubStatus__c, Status, SolutionInterest__c, PromoTitle__c, Priority__c, Name, CreatedDate, ClosedDate__c, Category__c,Owner.FirstName, Owner.LastName, OwnerID, ResponseDate__c, CampaignName__c,keycode__c, ConvertedDate, MarcomType__c from Lead where Contact__c =:con.Id AND RecordTypeId !=: System.Label.CLJUL14MKT01 order by CreatedDate desc];      
        }
       
        /*set<id> QueueGroupIds = new set<id>();
        for(Lead led : leads)
        {
            string strOwnerID = led.OwnerId;
            if(!strOwnerID.startsWith('005'))
               QueueGroupIds.add(led.OwnerId);
        }
        
        map<id,Group> mapQueueGroups = new map<id,Group>([Select Type, Name, Email From Group where id in :QueueGroupIds]);
        */
        /* 
        columns.add(new SelectOption('Field1__c','Lead Name')); // First Column
        columns.add(new SelectOption('Field2__c',Schema.sObjectType.Lead.Fields.status.label));
        columns.add(new SelectOption('Field3__c',Schema.sObjectType.Lead.Fields.Category__c.label));
        columns.add(new SelectOption('Field4__c',Schema.sObjectType.Lead.Fields.PromoTitle__c.label));
        columns.add(new SelectOption('Field5__c',Schema.sObjectType.Lead.Fields.Timeframe__c.label));
        columns.add(new SelectOption('Field6__c',Schema.sObjectType.Lead.Fields.SolutionInterest__c.label));
        columns.add(new SelectOption('Field7__c',Schema.sObjectType.Lead.Fields.Priority__c.label));
        columns.add(new SelectOption('Field8__c','Lead Owner')); // Last Column
        */
      
       // Select l.Timeframe__c, l.SubStatus__c, l.Status, l.SolutionInterest__c, l.PromoTitle__c, l.Priority__c, l.Name, l.CreatedDate, l.ClosedDate__c, l.Category__c From Lead l
        columns.add(new SelectOption('Field1__c','Lead Name')); // First Column
        columns.add(new SelectOption('Field2__c',Schema.sObjectType.Lead.Fields.CreatedDate.label));
        columns.add(new SelectOption('Field3__c',Schema.sObjectType.Lead.Fields.Status.label));
        columns.add(new SelectOption('Field4__c',Schema.sObjectType.Lead.Fields.SubStatus__c.label));
        columns.add(new SelectOption('Field5__c',Schema.sObjectType.Lead.Fields.ClosedDate__c.label));
        columns.add(new SelectOption('Field6__c',Schema.sObjectType.Lead.Fields.Priority__c.label));
        columns.add(new SelectOption('Field7__c',Schema.sObjectType.Lead.Fields.Timeframe__c.label));
        columns.add(new SelectOption('Field8__c',Schema.sObjectType.Lead.Fields.Tech_CampaignName__c.label)); // DEF-1217
        columns.add(new SelectOption('Field9__c',Schema.sObjectType.Lead.Fields.SolutionInterest__c.label));
        columns.add(new SelectOption('Field10__c',System.Label.CLDEC12AC14)); // Last Column
       //BR-1911    Srinivas Nallapati
       
       for(Lead lead: leads)
        {
            DataTemplate__c dt = new DataTemplate__c();
            if(lead.ConvertedDate!=null){           
                dt.field1__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + '/apex/VFP44_LeadDetail?id='+lead.Id+'" onClick="openTab(\''+lead.Id+'\',\''+lead.Name+'\');return false">'+lead.Name+'</a>';                
            }   
            else{
                dt.field1__c = '<a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + '/'+lead.Id+'" onClick="openTab(\''+lead.Id+'\',\''+lead.Name+'\');return false">'+lead.Name+'</a>';
            }
            
            dt.field2__c = String.valueOf(lead.CreatedDate);
            dt.field3__c = lead.Status;
            dt.field4__c = lead.SubStatus__c;
            dt.field5__c = String.valueOf(lead.ClosedDate__c);
            dt.field6__c = String.valueOf(lead.Priority__c);
            dt.field7__c = lead.Timeframe__c;
            dt.field8__c = lead.Tech_CampaignName__c;  //DEF-1217
            dt.field9__c = lead.SolutionInterest__c;
            dt.field10__c = lead.Owner.FirstName +' '+lead.Owner.LastName;  // BR-1911    Srinivas Nallapati
            
            /*string strOwnerID = lead.OwnerId; 
            if(strOwnerID.startsWith('005')) 
                dt.field8__c = lead.Owner.FirstName +' '+lead.Owner.LastName;
            else
                dt.field8__c =  mapQueueGroups.get(lead.OwnerId).name;
            */
                    
            dataTemplates.add(dt);
        }
        System.Debug('****** Querying of Leads Ends ******'); 
        return null;
        
    }
    
    //Added for BR-1858, New Lead page from the accociated leads section of Contact detail page. This method redirects the user to new lead page
    public PageReference gotoNewLead()
    {
        con= [select name,FirstName, LastName,Accountid, Account.name,Account.Country__r.Name,Account.Country__c,WorkPhone__c,MobilePhone,Email from Contact where id = :con.id];
        PageReference pg = new PageReference('/00Q/e');
        pg.getParameters().put('CF00NA0000009eEic',con.Account.name);
        pg.getParameters().put('CF00NA0000009eEic_lkid',Con.Accountid);
        pg.getParameters().put('CF00NA0000009eEit',con.name);
        pg.getParameters().put('CF00NA0000009eEit_lkid',Con.id);
        pg.getParameters().put('CF00NA0000009eEiu',con.Account.Country__r.Name);
        pg.getParameters().put('CF00NA0000009eEiu_lkid',con.Account.Country__c);

        pg.getParameters().put('name_firstlea2',con.FirstName);
        pg.getParameters().put('name_lastlea2',con.LastName);
                
        pg.getParameters().put('lea3',con.Account.name);

        pg.getParameters().put('lea8',con.WorkPhone__c);
        pg.getParameters().put('lea9',con.MobilePhone);
        pg.getParameters().put('lea11',con.Email);
        
        return pg;
    }
    
    // If we are in the console, return this page url
    public String getGotoNewLeadUrl() {
        con= [select name,FirstName, LastName,Accountid, Account.name,Account.Country__r.Name,Account.Country__c,WorkPhone__c,MobilePhone,Email from Contact where id = :con.id];
        PageReference pg = new PageReference('/00Q/e');
        pg.getParameters().put('CF00NA0000009eEic',con.Account.name);
        pg.getParameters().put('CF00NA0000009eEic_lkid',Con.Accountid);
        pg.getParameters().put('CF00NA0000009eEit',con.name);
        pg.getParameters().put('CF00NA0000009eEit_lkid',Con.id);
        pg.getParameters().put('CF00NA0000009eEiu',con.Account.Country__r.Name);
        pg.getParameters().put('CF00NA0000009eEiu_lkid',con.Account.Country__c);
        pg.getParameters().put('00NA0000009yS3W','1');
        
        pg.getParameters().put('name_firstlea2',con.FirstName);
        pg.getParameters().put('name_lastlea2',con.LastName);
                
        pg.getParameters().put('lea3',con.Account.name);

        pg.getParameters().put('lea8',con.WorkPhone__c);
        pg.getParameters().put('lea9',con.MobilePhone);
        pg.getParameters().put('lea11',con.Email);
        
        return pg.getUrl();
    }
    
}//End of Class