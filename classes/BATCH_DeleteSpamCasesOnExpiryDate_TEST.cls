/*
Author          : Rakhi Kumar - Global Delivery Team
Date Created    : 19/03/2013
Description     : Test class for BATCH_CASE_DeleteSpamCasesOnExpiryDate - May 2013 Release
-----------------------------------------------------------------
                          MODIFICATION HISTORY
-----------------------------------------------------------------
Modified By     : <>
Modified Date   : <>
Description     : <>
-----------------------------------------------------------------
*/
@isTest
private class BATCH_DeleteSpamCasesOnExpiryDate_TEST{
    static testMethod void unitTest() {
        Test.startTest();
        USERROLE ur = [select id,name from userrole where name ='CEO' limit 1];
        User contextUser = Utils_TestMethods.createStandardUserWithNoCountry('TestUser');
        contextUser.BypassVR__c = true;
        contextUser.userRoleId=  ur.id;
        insert contextUser;
        
        System.runAs(contextUser)
        {
            Integer spamExpiryPeriod = Integer.ValueOf(System.Label.CLMAY13CCC01) + 5;//Retrieve expiry period for spam cases
            Date dateMarkedAsSpam = System.Today().addDays(-spamExpiryPeriod );//Reverse calculate the date marked as spam(with reference to current date) based on the expiry period.
            
            Integer orphanExpiryPeriod = Integer.ValueOf(System.Label.CLOCT15CCC13) + 5;//Retrieve expiry period for Orphan cases
            Date dateMarkedAsorphan = System.Today().addDays(-orphanExpiryPeriod );//Reverse calculate the date marked as Orphan(with reference to current date) based on the expiry period.
            

            Account account1 = Utils_TestMethods.createAccount();
            insert account1;
            
            Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact');
            insert contact1;
            
            List<Case> lstCase = new List<Case>();      
            Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'In Progress');
            case1.Spam__c = true;
            case1.TECH_DateMarkedAsSpam__c = dateMarkedAsSpam;
            lstCase.add(case1);
            
            
            Case case2 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'In Progress');
            case2.Spam__c = true;
            case2.TECH_DateMarkedAsSpam__c = dateMarkedAsSpam;
            lstCase.add(case2);
            
            
            String strOrphanQueueId   = System.Label.CLOCT15CCC12;
            Case case3 = Utils_TestMethods.createCase(null, null, 'In Progress');
            case3.TECH_DateMarkedAsOrphan__c = dateMarkedAsorphan;
            lstCase.add(case3);
            
            insert lstCase;
            

            
            System.Debug('##################           Unit Test 1 - Begin          ##################');
            
            //BATCH DELETE SPAM CASES
            /*
            SCHEDULE_CASE_DeleteSpamCases spamCasesScheduledApex = new SCHEDULE_CASE_DeleteSpamCases();
            DateTime currentDate = System.Now();
            
            String CRON_EXPRESSION = '' + 59 + ' ' + currentDate.minute() + ' ' + currentDate.hour() + ' ' + currentDate.day() + ' ' + currentDate.month() + ' ? ' + currentDate.year()+1;
            
            String jobId = System.schedule('CCC Delete Spam Cases', CRON_EXPRESSION, spamCasesScheduledApex);
            */
            
            Id batchInstanceId = Database.executeBatch(new BATCH_CASE_DeleteSpamCasesOnExpiryDate(), 1000); 
            System.Debug('##################           Unit Test 1 - End            ##################');
            
                    
            Test.stopTest();
        }
    }
}