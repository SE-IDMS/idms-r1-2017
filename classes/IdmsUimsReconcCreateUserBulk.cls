public without sharing class IdmsUimsReconcCreateUserBulk{
    
    static IDMSCaptureError errorLog=new IDMSCaptureError();
    
    public static void createUimsUsers(List<IDMSUserReconciliationBatchHandler__c> recUsersToCreate, String batchId){
        
        Set<string> idmsUids = new Set<String>();
        for(IDMSUserReconciliationBatchHandler__c idmsUser : recUsersToCreate){
            idmsUids.add(idmsUser.IdmsUser__c);
        }
        //retrieve list of idms users
        List<User> idmsUsers = [Select Id, Email,MobilePhone,Phone,FirstName,LastName,IDMS_Email_opt_in__c,IDMS_User_Context__c,IDMSMiddleName__c,
                                Country,IDMS_PreferredLanguage__c,DefaultCurrencyIsoCode, Street,City,PostalCode,State,IDMS_County__c,Job_Title__c,
                                IDMS_POBox__c,FederationIdentifier,IDMS_Registration_Source__c,IDMS_AdditionalAddress__c,IDMSMiddleNameECS__c,Job_Function__c,
                                IDMSSalutation__c,IDMSSuffix__c,Fax,IDMSDelegatedIdp__c,IDMSIdentityType__c,IDMSJobDescription__c, IDMSPrimaryContact__c,  IDMS_VU_NEW__c,
                                IDMS_VU_OLD__c from User where Id IN:idmsUids];
        
        //stub class logic starts
        IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort bulkService = new IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort();
        string callerFid = LAbel.CLJUN16IDMS104;
        bulkService.endpoint_x = Label.CLNOV16IDMS020;
        bulkService.clientCertName_x = System.Label.CLJUN16IDMS103;
        IdmsUsersResyncflowServiceIms.userCreateRequest objCreateUser = new IdmsUsersResyncflowServiceIms.userCreateRequest();
        objCreateUser.contacts = new List<IdmsUsersResyncflowServiceIms.contactBean>();
        
        //Response from UIMS
        IdmsUsersResyncflowServiceIms.createContactsResponse objCreateUserRes = new IdmsUsersResyncflowServiceIms.createContactsResponse();
        objCreateUserRes.return_x = new List<IdmsUsersResyncflowServiceIms.createContactReturnBean>();
        
        for(User usr :idmsUsers){
            IdmsUsersResyncflowServiceIms.contactBean uimsCntBean = IdmsUimsReconcUserMapping.mapIdmsUserUims(usr);
            objCreateUser.contacts.add(uimsCntBean);
        }
        //UIMS create Contact method, by default users are in active mode.
        objCreateUserRes.return_x = bulkService.createContacts(callerFid,objCreateUser, null, null, false);
        system.debug('*****after create method response from UIMS****'+objCreateUserRes.return_x);
        List<User> updateIdmsUsers = new List<User>();
        List<IDMSUserReconciliationBatchHandler__c> handlerRecordsToDel = new List<IDMSUserReconciliationBatchHandler__c>();
        List<IDMSReconciliationBatchLog__c> createBatchLogs= new List<IDMSReconciliationBatchLog__c>();
        
        List<IDMSUserReconciliationBatchHandler__c> handlerRecordsToUpdate = new List<IDMSUserReconciliationBatchHandler__c>();
        Integer attemptsMaxLmt = integer.valueof(label.CLNOV16IDMS022);
        
        //Uims response email ids set
        Set<String> uimsEmailIds = new Set<String>();
        //corresponding user ids set
        set<String> idmsUserIds = new set<String>();
        Map<String, User> mapUimsEmailIdmsUser = new Map<String, User>();
        Map<String, IDMSUserReconciliationBatchHandler__c> mapIdmsUserHandler = new Map<String, IDMSUserReconciliationBatchHandler__c>();
        String idmsUserName;
        Set<String> idmsUserNames = new Set<String>();
        try{
            if(objCreateUserRes.return_x.size()>0){
                for(IdmsUsersResyncflowServiceIms.createContactReturnBean responseUims: objCreateUserRes.return_x){
                    uimsEmailIds.add(responseUims.email);
                    idmsUserName = responseUims.email+Label.CLJUN16IDMS71;
                    idmsUserNames.add(idmsUserName);
                }
                //corresponding Idms Users list 
                List<User> idmsUserList = [Select Id, Email,username, FederationIdentifier, IDMS_VU_NEW__c, IDMS_VU_OLD__c from User where ID IN: idmsUids AND username IN:idmsUserNames];
                for(User idmsUser: idmsUserList){
                    //We don't have username in response in order to traverse the list. 
                    mapUimsEmailIdmsUser.put(idmsUser.Email, idmsUser);
                    idmsUserIds.add(idmsUser.id);
                    system.debug('***Map of Email id and User record***'+mapUimsEmailIdmsUser);
                }
                //corresponding User handler list
                List<IDMSUserReconciliationBatchHandler__c> userHandlerList = [select Id, IdmsUser__c, NbrAttempts__c, HttpMessage__c  from IDMSUserReconciliationBatchHandler__c where IdmsUser__c IN: idmsUserIds];
                for(IDMSUserReconciliationBatchHandler__c userHandler : userHandlerList){
                    mapIdmsUserHandler.put(userHandler.IdmsUser__c, userHandler);
                    system.debug('***Map bFO User Id****Handler record****'+mapIdmsUserHandler);
                }            
                
                for(IdmsUsersResyncflowServiceIms.createContactReturnBean responseUims: objCreateUserRes.return_x){
                    User idmsUsrUpdate = mapUimsEmailIdmsUser.get(responseUims.email);
                    system.debug('inside for loop and before If condition****'+idmsUsrUpdate);
                    if(responseUims.ID!=Label.CLNOV16IDMS055 && responseUims.message.equalsIgnoreCase(Label.CLNOV16IDMS054)){
                        system.debug('*** This user successfully created in UIMS****'+responseUims.ID);
                        
                        //Add Idms user in a list to update
                        if(idmsUsrUpdate!=null){
                            idmsUsrUpdate.IDMS_VU_OLD__c  = idmsUsrUpdate.IDMS_VU_NEW__c;
                            idmsUsrUpdate.FederationIdentifier = responseUims.ID;
                            updateIdmsUsers.add(idmsUsrUpdate);
                        }
                        system.debug('***IDMS users before Update in the list***'+updateIdmsUsers);
                        
                        IDMSReconciliationBatchLog__c idmsBatchLog = IdmsReconcBatchLogsHandler.batchLogForCreateUser(idmsUsrUpdate, responseUims, batchId);
                        createBatchLogs.add(idmsBatchLog);     
                        System.debug('Batch log created for success users:'+createBatchLogs);
                        
                        //Add handler record in the list to delete.
                        IDMSUserReconciliationBatchHandler__c handlerRecToDelete = mapIdmsUserHandler.get(idmsUsrUpdate.Id);
                        system.debug('****handlerRecToDelete*****:'+handlerRecToDelete);
                        handlerRecordsToDel.add(handlerRecToDelete);
                        
                    }else {
                        system.debug('*** This user failed to create in UIMS****');
                        //Update User handler record.
                        IDMSUserReconciliationBatchHandler__c handlerRecToUpdate = mapIdmsUserHandler.get(idmsUsrUpdate.Id);
                        if(handlerRecToUpdate.NbrAttempts__c < attemptsMaxLmt){
                            handlerRecToUpdate.NbrAttempts__c = handlerRecToUpdate.NbrAttempts__c + 1;
                        }
                        handlerRecToUpdate.HttpMessage__c = 'FederatedId:'+responseUims.ID+'\n'+'Email Id:'+responseUims.email+'\n'+'Message:'+responseUims.message+'\n'+'Return Code:'+responseUims.returnCode;
                        handlerRecordsToUpdate.add(handlerRecToUpdate);
                        system.debug('*** User handler records to update ****'+handlerRecordsToUpdate);
                        
                        IDMSReconciliationBatchLog__c idmsBatchFailedLog = IdmsReconcBatchLogsHandler.batchLogForCreateUser(idmsUsrUpdate, responseUims, batchId);
                        createBatchLogs.add(idmsBatchFailedLog);     
                        System.debug('Batch log created for failed users:'+createBatchLogs);
                    }           
                }
            }
            Update updateIdmsUsers;
            Insert createBatchLogs;
            Delete handlerRecordsToDel;
            Update handlerRecordsToUpdate;
        } catch(Exception e){
            System.debug('Exception occurs:'+e.getStackTraceString());  
            //insert exception in Idms error logs. 
            errorLog.IDMScreateLog('IDMSReconciliationBatch','IdmsUimsReconcBulkCalls','Bulk user creation in UIMS','',Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),batchId,Datetime.now(),'','',false,'',false,null);
        }    
    }
}