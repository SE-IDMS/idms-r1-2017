@isTest
private class VFC_NewContact_Test
{
    static TestMethod Void testUrlRedirect()
    { 
        Country__c country1 = Utils_TestMethods.createCountry();
        insert country1;
        
        StateProvince__c state = Utils_TestMethods.createStateProvince(country1.id);
        insert state;
        
        User newUser = Utils_TestMethods.createStandardUser('conUser');
        insert newUser;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.Street__c = 'TestStreet';
        acc.City__c = 'TestCity';
        acc.StateProvince__c = state.id;
        acc.StreetLocalLang__c = 'TestStreetLocalLang';
        acc.POBox__c = 'TestPOBox';
        acc.POBoxZip__c = 'Test1234';
        acc.ZipCode__c =  '1234';
        acc.LocalCity__c = 'TestLocalCity';
        acc.Country__c = country1.id;
        acc.County__c = 'TestCounty';
        acc.LocalCounty__c = 'TestLocalCounty';
        acc.Name = 'TestAccountName';
        acc.Pagent__c = newUser.id;
        acc.PBAAgent__c = newUser.Id;
        insert acc;
        
        Contact con = new Contact();
        
        // Positive Test Case
        PageReference pageRefpositive = Page.VFP_CompetitorSearch;
        Test.setCurrentPage(pageRefpositive);
        pageRefpositive.getParameters().put('accid',acc.id);
        
        ApexPages.StandardController scp = new ApexPages.StandardController(con);
        VFC_NewContact  ctrlp= new VFC_NewContact(scp);
        ctrlp.redirectToContact();
        
        // Negative Test Case
        PageReference pageRefnegative = Page.VFP_CompetitorSearch;
        Test.setCurrentPage(pageRefnegative);
        //pageRefnegative.getParameters().put('accid',acc.id);
        
        ApexPages.StandardController scn = new ApexPages.StandardController(con);
        VFC_NewContact ctrln = new VFC_NewContact(scn);
        ctrln.redirectToContact();
    }    
}