/************************************************************
* Developer: Fielo Team                                     *
* Type: Test Class                                          *
* Created Date: 27.06.2016                                  *
* Tested Class:  Fielo_RecomendExtension                    *
************************************************************/
@isTest
public with sharing class Fielo_RecomendExtensionTest {
  
    public static testMethod void testUnit(){
    
        FieloEE.MockUpFactory.setCustomProperties(false);
        
        FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
        program.Name = 'Schneider Electric Rewards Program';
        update program;        
               
        Fielo_RecomendExtension test1 = new Fielo_RecomendExtension(new FieloEE.ProgramController());

    }
}