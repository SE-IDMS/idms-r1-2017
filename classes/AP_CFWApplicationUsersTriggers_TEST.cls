/*
    Author          : Bhuvana S
    Description     : Test class for AP_CFWApplicationUsersTriggers
*/
@isTest
private class AP_CFWApplicationUsersTriggers_TEST 
{
    static testMethod void testudaBeforeUpdate()
    {
        test.startTest();
            
        CFWRiskAssessment__c cfwr = new CFWRiskAssessment__c();
        cfwr.ProjectName__c = 'testProjectName';
        insert cfwr;
        
        CFWKeyBusinessObjects__c cbo = new CFWKeyBusinessObjects__c();
        cbo.CertificationRiskAssessment__c = cfwr.Id;
        insert cbo;
        
        CFWApplicationUsers__c cau = new CFWApplicationUsers__c();
        cau.CertificationRiskAssessment__c = cfwr.Id;
        insert cau;
                
        test.stoptest();
    }
}