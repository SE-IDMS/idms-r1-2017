global class Fielo_UserCreation implements  FieloEE.UserCreation {
    global User getUser(FieloEE__Member__c m, Id profileId, Id contactId){                
        String email = m.FieloEE__Email__c;      
        String communityNickname = m.FieloEE__LastName__c + String.valueOf(contactId).substring(12,15);   
        String subAlias = (m.FieloEE__LastName__c.length()>4) ? m.FieloEE__LastName__c.substring(0,4) : m.FieloEE__LastName__c;
        String alias = (m.FieloEE__FirstName__c != null) ? m.FieloEE__FirstName__c.substring(0,1) + subalias : subalias;                    
        orgSettings = [SELECT DefaultLocaleSidKey, LanguageLocaleKey FROM Organization]; 
        FieloEE__Program__c program = FieloEE.ProgramUtil.getProgram(m.FieloEE__Program__c);
        
        if(program.FieloEE__Language__c == null)
            program.FieloEE__Language__c = 'en_US';
        if(program.FieloEE__Locale__c == null)
            program.FieloEE__Locale__c = orgSettings.LanguageLocaleKey;
        if(program.FieloEE__TimeZone__c == null)
            program.FieloEE__TimeZone__c = UserInfo.getTimeZone().getId();
        
        User u = new User(
            Username = email, 
            Email = email, 
            FirstName = m.FieloEE__FirstName__c, 
            LastName = m.FieloEE__LastName__c, 
            Alias = alias, 
            emailencodingkey='UTF-8',
            profileid = profileId, 
            contactId = contactId, 
            LocaleSidKey = program.FieloEE__Locale__c, 
            TimeZoneSidKey = program.FieloEE__TimeZone__c, 
            LanguageLocaleKey = 'en_US',
            CommunityNickname = communityNickname,
            DefaultCurrencyIsoCode = 'AED'
        );                 
        return u;
    }

    private static Organization orgSettings {get{
            if(orgSettings == null){
                orgSettings = [SELECT DefaultLocaleSidKey, LanguageLocaleKey FROM Organization];
            }return orgSettings;
        }
    }
}