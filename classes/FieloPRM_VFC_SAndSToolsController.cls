public class FieloPRM_VFC_SAndSToolsController {
    
    //public User currentuser {get;set;}
    
    
    
    
    
    public static FieloEE__Member__c getMemberInfo(){
        FieloEE__Member__c m = new FieloEE__Member__c();
        User u = new User();
        u.Email='NoUser@gmail.com';
        m.FieloEE__User__r = u;
        try{
            m = [Select Name
                 ,FieloEE__User__r.Email
                 ,FieloEE__User__r.Name
                 //,FieloEE__User__r.Country seems to not be used
                 ,FieloEE__User__r.FederationIdentifier
                 ,FieloEE__User__r.LanguagePreferred__c // e.g  English
                 ,FieloEE__User__r.LanguageLocaleKey // e.g  en_US
                 
                 ,F_Country__r.CountryCode__c // e.g  US
                 //,F_Country__r.Name // e.g USA
                 
                 ,F_PRM_PrimaryChannel__c // e.g  SP 
                 ,F_PRM_SecondaryChannel1__c // e.g LC2
                 
                 From FieloEE__Member__c Where FieloEE__User__r.Id = :userinfo.getuserId() ];
        }catch(Exception e){}
        return m;
    }
    
    
    public Id myTagId {get;set;}
    public Id myCategoryId {get;set;}
    public Id myComponentId {get;set;}
    public Boolean testMode {get;set;}
    
    public FieloEE__Component__c getComponentDetails(){
        FieloEE__Component__c c = null;
        if(myComponentId!=null){
            c = [
                Select Name,F_PRM_DocType__c
                From FieloEE__Component__c
                Where F_PRM_DocType__c  != null
            ];
        }
        return c ;
    }
    
    public static String getFederationIdentifier(){
        FieloEE__Member__c member = new FieloEE__Member__c();
        try{
            member = [select Name,FieloEE__User__r.FederationIdentifier from FieloEE__Member__c where FieloEE__User__r.Id=:userinfo.getuserId()];
        }catch(Exception e){}
        if(member!=null&&member.FieloEE__User__r.FederationIdentifier!=null){
            return member.FieloEE__User__r.FederationIdentifier;
        }else{
            return null;  
        }
        
    }
    
    public static String getListUrl(){
        if('tool'.equals(getParam('type'))){
            return System.Label.SSTAPI_ToolListUrl;    
        }else{
            return System.Label.SSTAPI_DocumentListUrl;    
        }
    }
    
    public String getDocDetailUrl(){return System.Label.SSTAPI_DocumentDetailsUrl;}
    /*public static String getDownloadUrl(){return BslUtils.getDownloadUrl(''); }
    public static String getDownloadUrlByDocOid(){return BslUtils.getDownloadUrlByDocOid();}*/
    public static String getDownloadUrlByDocRef(){return null;}
    public static String getDownloadThumbnailUrlByDocRef(){return null;}
    
    public static String getPage(){
        String page = ApexPages.currentPage().getParameters().get('page');
        if(page==null){
            return 'home';
        }else{
            return page;
        }
    }
    
    public static String getInnerPage(){
        String page = ApexPages.currentPage().getParameters().get('page');
        //System.debug('#5G8H23 '+page);
        if(page==null||page.equals('')){
            return 'home';
        }else{
            return page;
        }
    }
    
    public static String getPageNumber(){
        String firstResult = ApexPages.currentPage().getParameters().get('firstResult');
        String maxResult = ApexPages.currentPage().getParameters().get('maxResult');
        if(firstResult!=null&&!firstResult.equalsIgnoreCase('')&&maxResult!=null&&!maxResult.equalsIgnoreCase('')){
            
            return String.valueOf((Integer.valueOf(firstResult)/Integer.valueOf(maxResult))+1);
        }else{
            return '1';
        }
    }
    public static String getFirstResultPrev(){
        return getFirstResultGeneric(true);
    }
    public static String getFirstResultNext(){
        return getFirstResultGeneric(false);
    }
    
    public static String getMaxResult(){
       String maxResult = ApexPages.currentPage().getParameters().get('maxResult');
        if(maxResult!=null&&!maxResult.equalsIgnoreCase('')){
            return maxResult;
        }else{
            return '6';
        }
    }
    
    public static String getFirstResultGeneric(Boolean prevOrNext){
        String firstResult = ApexPages.currentPage().getParameters().get('firstResult');
        String maxResult = ApexPages.currentPage().getParameters().get('maxResult');
        if(firstResult!=null&&!firstResult.equalsIgnoreCase('')&&maxResult!=null&&!maxResult.equalsIgnoreCase('')){
            if(prevOrNext){
                return String.valueOf(Integer.valueOf(firstResult)-Integer.valueOf(maxResult));   
            }else{
                return String.valueOf(Integer.valueOf(firstResult)+Integer.valueOf(maxResult));    
            }
        }else{
            if(prevOrNext){
                return '0';   
            }else{
                return '6';    
            }
        }
    }
    
    public String getDocTypeGroupOids(){
     
       String docTypeGroupBslOids = null;
        if(getParam('type')=='tool'){
            docTypeGroupBslOids = BslUtils.getDocTypeGroupBslOids(getParam('idCategory'));
        }else{

        }
        return docTypeGroupBslOids;
    }
    
    public Boolean getShowRecentlyAdded(){
          return getScope().showRecentlyAdded;
    }

    BslUtils.DocListResultBean recentlyAddedDocList=null;
    public BslUtils.DocListResultBean getRecentlyAddedDocList(){
        Integer docTypeOid=0;
        if(getParam('docTypeOid')!=null&&!getParam('docTypeOid').equals('')){
            docTypeOid = Integer.valueOf(getParam('docTypeOid'));
        }
        Integer rangeOid=0;
        if(getParam('rangeOid')!=null&&!getParam('rangeOid').equals('')){
            rangeOid = Integer.valueOf(getParam('rangeOid'));
        }
        String docTypeGroupBslOidXmlListString=null;
        if(getParam('type')!='tool'){
            docTypeGroupBslOidXmlListString = BslUtils.toDocTypeGroupBslOidXmlListString(BslUtils.getRecentlyAddedDocTypeGroupBslOids(getParam('idCategory')));
        }
        if(recentlyAddedDocList==null){
            recentlyAddedDocList = BslUtils.getDocList(
            getScope(),getLocale(),getFederationIdentifier()
            ,getParam('searchString'),getParam('docTypeGroupOid')
            ,docTypeGroupBslOidXmlListString
            ,docTypeOid,rangeOid,getParam('rangeStatus'),'',getParam('firstResult'),getParam('maxResult'));
        }
        
        return recentlyAddedDocList;
    }
    BslUtils.DocListResultBean docList=null;
    public BslUtils.DocListResultBean getDocList(){
        Integer docTypeOid=0;
        if(getParam('docTypeOid')!=null&&!getParam('docTypeOid').equals('')){
            docTypeOid = Integer.valueOf(getParam('docTypeOid'));
        }
        Integer rangeOid=0;
        if(getParam('rangeOid')!=null&&!getParam('rangeOid').equals('')){
            rangeOid = Integer.valueOf(getParam('rangeOid'));
        }
        String docTypeGroupBslOids = null;
        if(getParam('type')=='tool'){
            docTypeGroupBslOids = BslUtils.getDocTypeGroupBslOids(getParam('idCategory'));
        }
        if(docList==null){
            docList = BslUtils.getDocList(
            getScope(),getLocale(),getFederationIdentifier()
            ,getParam('searchString'),getParam('docTypeGroupOid'),null,docTypeOid,rangeOid,getParam('rangeStatus'),'',getParam('firstResult'),getParam('maxResult'));
        }
        return docList;
    }
    
    BslUtils.DocListResultBean featuredToolList=null;
    public BslUtils.DocListResultBean getFeaturedToolList(){
        if(featuredToolList==null){
         featuredToolList = getFeaturedList('FieloPRM_Featured_Tool' );
        }
        return featuredToolList;
    }

    
    BslUtils.DocListResultBean featuredDocList=null;
    public BslUtils.DocListResultBean getFeaturedDocList(){
        if(featuredDocList==null){
         featuredDocList = getFeaturedList('FieloPRM_Featured_Document' );
        }
        return featuredDocList;
    }
    
    public BslUtils.DocListResultBean getFeaturedList(String recordTypeName){
        
            String idCategory = getParam('idCategory');
            List<FieloEE__News__c> tagItems = null;
            String fieldTitleLang = 'Title_'+getLocale().language+'__c';
            Boolean exist = false;
            Map<String, Schema.SObjectField> fieldsDescribe = FieloEE__News__c.SObjectType.getDescribe().fields.getMap();
            for(Schema.SObjectField field : fieldsDescribe.values()){
                String apiName = field.getDescribe().getName();
                if (apiName==  fieldTitleLang) {
                    exist = true;
                    break;
                }
            }
            fieldTitleLang = exist?fieldTitleLang:'FieloEE__ExtractText__c';
            tagItems = Database.query('SELECT Name,F_PRM_ExtDocumentReference__c,'+fieldTitleLang+' FROM FieloEE__News__c WHERE RecordType.DeveloperName = :recordTypeName AND FieloEE__CategoryItem__c = :idCategory AND FieloEE__IsActive__c = true ORDER BY FieloEE__Order__c LIMIT 4 ');
                
                                
            String qb = '<queryBean name="DOC_REFERENCE" value="${docReference}"/>';
            
            String queryBeans = '';
            if(tagItems!=null &&tagItems.size()>0){
                
                for(Integer i =0,n=tagItems.size();i<n;i++){
                    if(tagItems[i].F_PRM_ExtDocumentReference__c!=null){
                        queryBeans+=qb.replace('${docReference}',tagItems[i].F_PRM_ExtDocumentReference__c);
                    }else{
                        System.debug('#4F9H2:' + tagItems[i].Name);
                    }
                }
                featuredDocList =  BslUtils.getDocList(getScope(),getLocale(),getFederationIdentifier(),/*searchString*/null,/*docTypeGroupBslOids*/null,/*docTypeGroupBslOidList*/null,0,0,/*rangeStatus*/null,queryBeans,getParam('firstResult'),getParam('maxResult'));
                Map<String,BslUtils.DocumentDetailsBean> m = new Map<String,BslUtils.DocumentDetailsBean>();
                for(Integer i =0,n=featuredDocList.docs.size();i<n;i++){
                    BslUtils.DocumentDetailsBean c = featuredDocList.docs.get(i); 
                    System.debug(LoggingLevel.INFO,'#4F9H3:' + c.reference);
                    m.put(c.reference,c);
                }
                List<BslUtils.DocumentDetailsBean> sortedList = new List<BslUtils.DocumentDetailsBean>();
                for(Integer i =0,n=tagItems.size();i<n;i++){
                    FieloEE__News__c ti = tagItems.get(i);
                    String dReference = ti.F_PRM_ExtDocumentReference__c;
                    System.debug('#4F9H4:' + dReference);
                    String title = ti.get(fieldTitleLang)!=null?(String) ti.get(fieldTitleLang):null;
                    BslUtils.DocumentDetailsBean ddb  = m.get(dReference);
                    if(ddb!=null){
                        if(title!=null){
                           ddb.title = title; 
                        }
                        //ddb.title = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor';
                        //ddb.description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor.';
                        //This Map is prooudly initiated with the 6th sens, believe in your feelings when changing the values...
                        Map<Integer,Integer> cropTitleByNumberOfItems = new Map<Integer,Integer>{1 => 200, 2 => 100, 3 =>55,4 => 33};
                        ddb.title = fromObjectToStringResizedForDisplay(ddb.title,cropTitleByNumberOfItems.get(tagItems.size()),'...');
                        
                        Map<Integer,Integer> cropTitleByDescOfItems = new Map<Integer,Integer>{1 => 750, 2 => 300, 3 =>180,4 => 120};
                        ddb.description = fromObjectToStringResizedForDisplay(ddb.description,cropTitleByDescOfItems.get(tagItems.size()),'...');
  
                        sortedList.add(ddb);
                    }
                    
                    
                    
                }
                featuredDocList.docs = sortedList;
                
            }else{
                BslUtils.DocListResultBean r = new BslUtils.DocListResultBean();
                r.docs= new List<BslUtils.DocumentDetailsBean>();
                featuredDocList = r;
            }
        return featuredDocList;
    }
    
    public List<FieloEE__News__c> getAdditionalInfo(){
        String idCategory = getParam('idCategory');
        String fieldBodyLang = 'Body_'+getLocale().language+'__c';
        Boolean exist = false;
        Map<String, Schema.SObjectField> fieldsDescribe = FieloEE__News__c.SObjectType.getDescribe().fields.getMap();
        for(Schema.SObjectField field : fieldsDescribe.values()){
            String apiName = field.getDescribe().getName();
            if (apiName==  fieldBodyLang) {
                exist = true;
                break;
            }
        }
        fieldBodyLang = exist?fieldBodyLang:'FieloEE__Body__c';
        String soql = 'select Name,'+fieldBodyLang+' FROM FieloEE__News__c WHERE RecordType.DeveloperName = \'FieloPRM_Additional_Information\' AND FieloEE__IsActive__c = true AND FieloEE__CategoryItem__c =: idCategory ORDER BY FieloEE__PublishDate__c desc LIMIT 1';
        List<FieloEE__News__c> newsList = Database.query(soql);
        for(FieloEE__News__c news:newsList){
            news.FieloEE__Body__c = news.get(fieldBodyLang)!=null?(String)news.get(fieldBodyLang):news.FieloEE__Body__c;
        }
        return newsList;
    }


    BslUtils.DocCountResultBean docTypeGroupCount = null;
    public BslUtils.DocCountResultBean getDocTypeGroupCount(){
        if(docTypeGroupCount==null){
            docTypeGroupCount =  BslUtils.getDocCount(getScope(),getLocale(),getDocTypeGroupOids(),0,'docTypeGroup','',getFederationIdentifier());    
        }
        return docTypeGroupCount;
    }
    
    BslUtils.DocCountResultBean docTypeCount = null;
    public BslUtils.DocCountResultBean getDocTypeCount(){
        if(docTypeCount==null){
            String docTypeGroupOid = getDocTypeGroupOids();
            if(docTypeGroupOid!=null){
                 return BslUtils.getDocCount(getScope(),getLocale(),docTypeGroupOid,0,'docType','',getFederationIdentifier());
            }else if(getParam('docTypeGroupOid')!=null&&!getParam('docTypeGroupOid').equals('0')){
                 docTypeGroupOid= getParam('docTypeGroupOid');
                 docTypeCount =  BslUtils.getDocCount(getScope(),getLocale(),docTypeGroupOid,0,'docType','',getFederationIdentifier());
            }else{
                //List<BslUtils.DocumentCountBean>();
                docTypeCount =  new BslUtils.DocCountResultBean();
            }
        }
        return docTypeCount;
    }
    
    BslUtils.DocCountResultBean rangeCount=null;
    public BslUtils.DocCountResultBean getRangeCount(){
        if(rangeCount==null){
            Integer docTypeOid = 0;
            if(getParam('docTypeOid')!=null&&!getParam('docTypeOid').equals('')){
                docTypeOid = Integer.valueOf(getParam('docTypeOid'));
            }
            rangeCount =  BslUtils.getDocCount(getScope(),getLocale(),null,docTypeOid,'range',getParam('rangeStatus'),getFederationIdentifier());
        }
        return rangeCount;
            
    }
    BslUtils.DocumentDetailsBean docDetails = null;
    public BslUtils.DocumentDetailsBean getDocDetails(){
        if(docDetails==null){
            if(getParam('docRef')==null){
                docDetails = new BslUtils.DocumentDetailsBean();
            }else{
                docDetails = BslUtils.getDocDetails(getScope(),getLocale(),getFederationIdentifier(),EncodingUtil.urlDecode(getParam('docRef'),'UTF-8'));
            }
        }
        return docDetails;
    }
    
    public static String getParam(String name){
        return ApexPages.currentPage().getParameters().get(name);
    }
    public BslUtils.ScopeBean getScope(){
        FieloEE__Member__c m = getMemberInfo();
        
        BslUtils.ScopeBean scope = BslUtils.findBslScope(getParam('idCategory'));
        //scope.country=m.F_Country__r.CountryCode__c;
        //scope.country='US';
        if(testMode!=null&&testMode){
            scope.useMockup=true;
        }
        
        return scope;
    }
    
    public static BslUtils.LocaleBean getLocale(){
        FieloEE__Member__c m = getMemberInfo();
        BslUtils.LocaleBean locale = new BslUtils.LocaleBean();
        if(m.FieloEE__User__r.LanguageLocaleKey!=null){
            List<String> arr = m.FieloEE__User__r.LanguageLocaleKey.split('_',0);
            locale.language=arr[0];
            //locale.country=arr[1];
            /*changes by fielo*/
            if(arr.size() > 1){
                locale.country=arr[1];
            }else{
                locale.country= m.F_Country__r.CountryCode__c;
            }
            /*end of changes by fielo*/
        }else{
            locale.language='en';
            locale.country='GB';

        }
        return locale;
    }

    public static String fromObjectToStringResizedForDisplay(String value,Integer endSize,String cutMessage){
        if (value!=null){
            Integer maxSizeAllowrd = endSize;
            Integer length = value.length();
            if (length <= maxSizeAllowrd){
                return value;
            }
            else {
                String myResult = value.substring(0,endSize-3);
                myResult+= cutMessage;
                return myResult;
            }
        }
        else {
            return value;
        }
    }

}