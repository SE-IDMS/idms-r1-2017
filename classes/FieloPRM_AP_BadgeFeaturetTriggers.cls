/**************************************************************************************
    Author: Fielo Team (Elena J. Schwarzböck)
    Date: 10/03/2015
    Description: 
    Related Components: 
***************************************************************************************/

public with sharing class FieloPRM_AP_BadgeFeaturetTriggers{

    public static final string PARTNER_LOCATOR  = 'Partner Locator';

    public static void recalculatesPartenerLocatorAccount(List<FieloPRM_BadgeFeature__c> triggerOld){
    
        Set<Id> setFeatures = new Set<Id>();
    
        for(FieloPRM_BadgeFeature__c badgeFeature: triggerOld){
            setFeatures.add(badgeFeature.F_PRM_Feature__c);
        }
        
        List<FieloPRM_Feature__c> listFeature = [SELECT Id, Name FROM FieloPRM_Feature__c WHERE Id IN: setFeatures AND Name =: PARTNER_LOCATOR];
        
        if(!listFeature.isEmpty()){
            
        }


    }
    
}