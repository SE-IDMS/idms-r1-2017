/******************************************
* Developer: Fielo Team                   *
*******************************************/
@isTest
public class Fielo_TransactionTriggersTest{
    
    public static testMethod void testUnit1(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        User u = new User();
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            Country__c country = new Country__c(
                CountryCode__c = 'US',
                CurrencyIsoCode = 'USD'
            );
            insert country;
            
            PRMCountry__c countrycluster = new PRMCountry__c(
                Country__c = country.id,
                TECH_Countries__c = 'US',
                PLDatapool__c = 'asd'
            );
            insert countrycluster;
            
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test', 
                FieloEE__LastName__c = 'test',
                F_Country__c = country.Id
            );
            Insert mem;
               
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc = new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;

            Fielo_Invoice__c invoice = new Fielo_Invoice__c(
                F_InvoiceDate__c = date.today(), 
                F_Member__c = mem.Id, 
                F_Status__c = 'Pending'
            );
            insert invoice;

            FieloEE__Transaction__c trans = new FieloEE__Transaction__c(
                FieloEE__Member__c = invoice.F_Member__c,
                FieloEE__Type__c = 'Invoice',
                F_Invoice__c = invoice.Id
            );
            
            insert trans;
        
        }

    }    
}