/**
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  07/12/2016 
*   This class used to set, update and reset user password.
**/

public without sharing class IDMSPasswordService{
    
    IDMSCaptureError Errorcls;   
    //This method set user password.
    public IDMSResponseWrapper setPassworduser(String Id,String FederationIdentifier,String NewPwd,String IDMS_Profile_update_source,String token){ 
        IDMSResponseWrapper response;
        Boolean isFedId = false;
        Errorcls = new IDMSCaptureError();  
        User u;
        IdmsPasswordPolicy policy=new IdmsPasswordPolicy();

        System.debug('ID--'+Id);

        if(String.isBlank(Id) || String.isBlank(NewPwd)){
            return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS023); 
        }
        else{
            //Prashanth
            //Modified as a part of New password policy
            if(String.isNotBlank(NewPwd)){
                //if(!Pattern.matches(Label.CLNOV16IDMS129, NewPwd)){
                if(!policy.checkPolicyMetOrNot(NewPwd,Id,FederationIdentifier)){
                    return new IDMSResponseWrapper(null,'Error',Label.CLJUN16IDMS52);
                }
            } 
            
            if(Id == null && FederationIdentifier == null ){
                return new IDMSResponseWrapper(null,'Error','User not found'); 
            }
            if(Id == null && FederationIdentifier != null){
                if(!FederationIdentifier.contains('-') ){
                    return new IDMSResponseWrapper(null,'Error','Fed Id should contain -'); 
                }
                else{
                    isFedId = true;
                }
            }
            if(IDMS_Profile_update_source == null ||IDMS_Profile_update_source == ''  ||( IDMS_Profile_update_source.startswith(' ') &&  IDMS_Profile_update_source.endswith(' '))){
                return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS024); 
            }
            if(String.IsBlank(token) || token == 'null'){
                return new IDMSResponseWrapper(null,'Error','Token cannot be blank or null'); 
            }
            if(id != null && !Id.startsWithIgnoreCase('005')){
                return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS026); 
            } 
            try{
                List<User> users;
                if(!isFedId == true){
                    users = [Select id,username,IsActive,FederationIdentifier  from user where id=:Id limit 1];
                    if(users.size() > 0){
                        u = users[0];
                    }
                }
                else{
                    users = [Select id,username,IsActive,FederationIdentifier  from user where FederationIdentifier =:FederationIdentifier limit 1];
                    if(users.size() > 0){
                        u = users[0];
                    }
                }
            }
            
            catch(Exception e){
                String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
                Errorcls.IDMScreateLog(null,'IDMSPasswordService','setPassworduser','Id',ExceptionMessage ,IDMS_Profile_update_source ,Datetime.now(),null,null,null,false,null);                
                return new IDMSResponseWrapper(null,'Error','User not found'); 
            }
            if(u != null){
                try{
                    if(!Test.isrunningTest()){ 
                        if(LoginCheck(u.username,NewPwd)){
                            return new IDMSResponseWrapper(null,'Error',Label.CLNOV16IDMS236);
                        }
                    }
                    
                    System.debug('inside set password--'+u.id + 'isactive --> ' +u.isActive);
                    Boolean isSigCorrect = IDMSEmailEncryption.EmailDecryption(token,u.id);
                    //Ranjith 
                    if(u.isActive) {
                        if(isSigCorrect){
                            System.setPassword(u.id,NewPwd);  
                        }
                        else{
                            return new IDMSResponseWrapper(null,'Error','Token is incorrect.'); 
                        }
                        System.debug('idms profile update Source --- ' + IDMS_Profile_update_source);                     
                        
                        if(!IDMS_Profile_update_source.equalsIgnoreCase(Label.CLQ316IDMS042)){ 
                            User user = [select federationidentifier, IDMS_VU_NEW__c from user where id=:u.id limit 1][0];
                            string userFedId = user.federationidentifier;
                            string vnew = user.IDMS_VU_NEW__c;
                            
                            IDMSEncryption encryption = new IDMSEncryption();
                            String samlAssrestion = encryption.ecryptedToken(userFedId,vnew);
                            IDMSUIMSWebServiceCalls.uimsSetPasswordUser(NewPwd,samlAssrestion);
                            return new IDMSResponseWrapper(u,'Success',Label.CLQ316IDMS029);    
                        }
                        return new IDMSResponseWrapper(u,'Success',Label.CLQ316IDMS029);
                    } else{
                        u.IsActive=true;
                        update u;
                        if(isSigCorrect){
                            System.setPassword(u.id,NewPwd);}
                        else{
                            return new IDMSResponseWrapper(null,'Error','Token is incorrect.'); 
                        }
                        
                        //need to call activate identity method here
                        if(!IDMS_Profile_update_source.equalsIgnoreCase(Label.CLQ316IDMS042)){
                            IDMSEncryption encryption = new IDMSEncryption();
                            string vnew = [select IDMS_VU_NEW__c from user where id =:u.id].IDMS_VU_NEW__c;
                            String authToken = encryption.ecryptedToken(u.federationIdentifier,vnew);
                            if(!Test.isrunningTest()){ 
                                IDMSUIMSWebServiceCalls.ActivateIdentityUIMS(NewPwd, authToken);
                            }
                            return new IDMSResponseWrapper(u,'Success',Label.CLQ316IDMS029);  
                        }
                        return new IDMSResponseWrapper(u,'Success',Label.CLQ316IDMS029); 
                    }
                    return new IDMSResponseWrapper(u,'Success',Label.CLQ316IDMS029);
                    
                }catch(Exception e){
                    System.debug('inside set catch--'+e);
                    String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
                    Errorcls.IDMScreateLog(null,'IDMSPasswordService','setPassworduser','Id',ExceptionMessage ,IDMS_Profile_update_source ,
                                           Datetime.now(),null,null,null,false,null);                
                    
                    if(e.getMessage().contains('invalid repeated password')){
                        //use the same token if using the old password
                        u.IDMSToken__c                      = token;
                        u.IDMSSignatureGenerationDate__c    = system.today();
                        update u;
                        return new IDMSResponseWrapper(null,'Error',Label.CLNOV16IDMS236);
                    }
                    return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS030); 
                }
            }
            else{
                return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS027); 
            }
        }
    }
    
    //This method check if user is logged in or not and return boolean.
    public boolean LoginCheck(string username,string password){
        string orgId    = label.CLQ316IDMS109;
        string endpoint = label.CLQ316IDMS135;
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');   
        req.setTimeout(60000);
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type', 'text/xml;charset=UTF-8');        
        req.setHeader('SOAPAction', '""');
        String soapMessage= '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:partner.soap.sforce.com">'+'<SOAP-ENV:Header>'+'<ns1:LoginScopeHeader>'+'<ns1:organizationId>'+orgId+'</ns1:organizationId>'+'</ns1:LoginScopeHeader>'+'</SOAP-ENV:Header>'+'<SOAP-ENV:Body>'+'<ns1:login>'+'<ns1:username>'+username+'</ns1:username>'+'<ns1:password>'+password+'</ns1:password>'+'</ns1:login>'+'</SOAP-ENV:Body>'+'</SOAP-ENV:Envelope>';

        req.setBody(soapMessage);       
        if(Test.isrunningTest()){ 
            return true;
        }
        HttpResponse res =  new Http().send(req);
        System.debug(res.getBody());
        if(res.getBody().contains(username)){
            return true;
        }
        else if(res.getBody().contains('INVALID_LOGIN'))
        {
            return false;
        }
        else{
            return false;
        }
    }
    
    //This method updates user password. It accepts new password, old password and update resource as parameters
    public IDMSResponseWrapper updatePassworduser(String ExistingPwd, String NewPwd,String Profile_update_source){
        Errorcls=new IDMSCaptureError();  
        IdmsPasswordPolicy policy=new IdmsPasswordPolicy();
        if(String.isBlank(ExistingPwd) || String.isBlank(NewPwd) ){
            return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS031);   
        }
        else{
            if(String.isBlank(Profile_update_source) || ( Profile_update_source.startswith(' ') &&  Profile_update_source.endswith(' '))){
                return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS024); 
            }
            if(ExistingPwd.equals(NewPwd)){
                return new IDMSResponseWrapper(null,'Error','New Password and Existing Password are the same.');
            }
            //Prashanth
            //Modified as a part of New password policy
            //if(!Pattern.matches(Label.CLNOV16IDMS129, NewPwd)){
            if(!policy.checkPolicyMetOrNot(NewPwd,UserInfo.getUserId(),null)){
                return new IDMSResponseWrapper(null,'Error',Label.CLJUN16IDMS52);
            }           
            User user;
            try{
                user = [select id,FederationIdentifier,username, IDMS_VU_NEW__c from User WHERE id =:UserInfo.getUserId() limit 1];
                boolean res = LoginCheck(user.username,ExistingPwd); 
                if(res == false){
                    return new IDMSResponseWrapper(null,'Error','Existing Password is not correct'); 
                }
                System.debug('UserInfo22--'+UserInfo.getSessionId());
            }
            catch(Exception e){
                String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
                Errorcls.IDMScreateLog(null,'IDMSPasswordService','updatePassworduser',user.username,ExceptionMessage ,Profile_update_source,
                                       Datetime.now(),null,null,null,false,null);                
                return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS030); 
            }
            if(user != null){
                System.debug('ExistingPwd--'+ExistingPwd);
                try{
                    System.setPassword(user.id,NewPwd);
                    if(!Profile_update_source.equalsIgnoreCase(Label.CLQ316IDMS042)){
                        IDMSEncryption encryption =new IDMSEncryption();
                        String samlAssrestion = encryption.ecryptedToken(user.federationidentifier,user.IDMS_VU_NEW__c);
                        IDMSUIMSWebServiceCalls.updatePasswordUIMS(ExistingPwd,NewPwd,samlAssrestion);
                        return new IDMSResponseWrapper(user,'success',Label.CLQ316IDMS029);
                    }
                    else{
                        return new IDMSResponseWrapper(user,'success',Label.CLQ316IDMS029);
                    }
                }
                catch(Exception e){
                    String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
                    Errorcls.IDMScreateLog(null,'IDMSPasswordService','updatePassworduser',user.username,ExceptionMessage ,Profile_update_source,
                                           Datetime.now(),null,null,null,false,null);                
                    return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS030);                     
                }
            }
            else{
                return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS027); 
            }
        }
    }
    
    //This method update user password.  
    public IDMSResponseWrapper updatePasswordFederation(String NewPwd , String FederationIdentifier,String Profile_update_source){
        User ufed ; 
        IdmsPasswordPolicy policy=new IdmsPasswordPolicy();
        if(String.IsBlank(NewPwd)) 
            return new IDMSResponseWrapper(null,'Error','Password Should not be null'); 
        else if(String.IsBlank(FederationIdentifier)) {
            return new IDMSResponseWrapper(null,'Error','Federation Id is null'); 
        }else if(String.IsBlank(Profile_update_source)) {
            return new IDMSResponseWrapper(null,'Error','Profile Update Source is null'); 
        //Prashanth
        //Modified as a part of New password policy
        //}else if(!Pattern.matches(Label.CLNOV16IDMS129, NewPwd)){
        }else if(!policy.checkPolicyMetOrNot(NewPwd,null,FederationIdentifier)){
            return new IDMSResponseWrapper(null,'Error',Label.CLJUN16IDMS52);
        }
        else if(String.isNotBlank(NewPwd) && String.isNotBlank(FederationIdentifier) && String.isNotBlank(Profile_update_source)) {
                    if(Profile_update_source == 'uims' || Profile_update_source == 'UIMS'){
                        try{
                            ufed = [Select id,username,IsActive from user where FederationIdentifier =:FederationIdentifier limit 1];
                            System.setPassword(ufed.id,NewPwd);
                            return new IDMSResponseWrapper(ufed,'Success',Label.CLQ316IDMS029) ;
                        }catch (Exception e) {
                            return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS030); 
                        }
                    }else {
                        return new IDMSResponseWrapper(null,'Error','Profile is not UIMS') ;
                    }
                }else 
                    return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS027); 
    }
    
    //This method resets user password. 
    public IDMSResponseWrapper resetPassworduser(User UserRecord, String appId, String appLanguage){
        System.debug('Application id on reset password : '+appId);
        User uid;
        User uemail; 
        //User uphone ; 
        Errorcls = new IDMSCaptureError();
        
        if(String.isBlank(UserRecord.IDMS_Profile_update_source__c) || ( UserRecord.IDMS_Profile_update_source__c.startswith(' ') &&  UserRecord.IDMS_Profile_update_source__c.endswith(' '))){
            return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS024);     
        }else{ 
            if(UserRecord.email != null ) {
                try{  
                    String emailSuffix = UserRecord.email +Label.CLJUN16IDMS71;
                    uemail = [Select name,IDMSPinVerification__c,IDMSNewPhone__c,IdmsAppLanguage__c, username, IDMSIdentityType__c, email, MobilePhone,id from user where username =: emailSuffix   limit 1];    
                    if(uemail == null) {
                        return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS045);  
                    }else {
                        if(uemail.IDMSIdentityType__c !=null && uemail.IDMSIdentityType__c.equalsIgnoreCase('email')){
                            // Retreive email template 
                            system.debug('application language:'+uemail.IdmsAppLanguage__c);
            				IDMSEmailTable__c emailTemplate = AP_IDMSEmailTemplateHandler.GetEmailTemplateCS(appId,appLanguage);
                            IDMSSendCommunication.sendemail(uemail.id,(String)emailTemplate.EmailTemplatePwReset__c,UserRecord.IDMS_Profile_update_source__c);
                            return new IDMSResponseWrapper(uemail,'Success',Label.CLQ316IDMS036); 
                        }else if(uemail.IDMSIdentityType__c !=null && uemail.IDMSIdentityType__c.equalsIgnoreCase('Mobile')){
                            IDMSSendCommunication.sendSMS(uemail);
                            return new IDMSResponseWrapper(uemail,'Success',Label.CLQ316IDMS038); 
                        }else 
                            return new IDMSResponseWrapper(null,'Error','Identity Type Missing');  
                    }
                }catch(exception e){
                    String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
                    Errorcls.IDMScreateLog(null,'IDMSPasswordService','resetPassworduser',UserRecord.email,ExceptionMessage ,UserRecord.IDMS_Profile_update_source__c,Datetime.now(),null,null,null,false,null);                
                    
                    return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS045); 
                }  
            }else if(UserRecord.MobilePhone != null ) {
                try{                     
                    //JIRA - 127 
                    String phoneSuffix = String.valueOf(UserRecord.MobilePhone);
                   
                    if(phoneSuffix != null ) {
                        phoneSuffix = phoneSuffix.trim(); 
                    }
                    system.debug('phone suffix --> ' + phoneSuffix) ;
                    //Jira 528
                    if((phoneSuffix !=null || phoneSuffix !='') && Pattern.matches(Label.CLJUN16IDMS31, phoneSuffix ) ){
                        system.debug('entering phone--> ') ; 
                        if(phoneSuffix.startsWith('+')){
                            phoneSuffix = phoneSuffix.replace('+', '00');
                            system.debug('entering phone + --> ') ; 
                        }
                        else if(!phoneSuffix.startsWith('00')){
                            phoneSuffix = '00' + phoneSuffix ;
                            system.debug('entering phone 00 --> ') ; 
                        }
                        phoneSuffix = phoneSuffix +Label.CLQ316IDMS102;
                    }
                    system.debug('phone suffix --> ' + phoneSuffix) ;
                    
                    List<User> uphone = new List<User>([Select name, IDMSPinVerification__c,IDMSNewPhone__c,username, IDMSIdentityType__c, email, MobilePhone,id from user where username =: phoneSuffix limit 1]);  
                    system.debug('username--> ' + uphone.size()  + ' -- ' + uphone[0].IDMSIdentityType__c ) ;  
                    if(uphone.size() == 0 ) {
                        system.debug('uphone size-- > ' ) ;
                        return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS045);  
                    }
                    else { 
                        if(uphone[0].IDMSIdentityType__c.equalsIgnoreCase('email')){
                            system.debug('mobile email--> ');
                            //IDMSSendCommunication.sendemail(uphone[0].id,Label.CLQ316IDMS039,UserRecord.IDMS_Profile_update_source__c);
                            // Retreive email template 
                            system.debug('application language:'+uemail.IdmsAppLanguage__c);
            				IDMSEmailTable__c emailTemplate = AP_IDMSEmailTemplateHandler.GetEmailTemplateCS(appId,appLanguage);
                            IDMSSendCommunication.sendemail(uphone[0].id,(String)emailTemplate.EmailTemplatePwReset__c,UserRecord.IDMS_Profile_update_source__c);
                            
                            return new IDMSResponseWrapper(uphone[0],'Success',Label.CLQ316IDMS036); 
                        } else if (uphone[0].IDMSIdentityType__c == 'Mobile'){
                            uphone[0].IDMSPinVerification__c = String.valueof(100000+ Math.round(Math.random()*100000));
                            update uphone;
                            system.debug('mobile mobile--> ');
                            IDMSSendCommunication.sendSMS(uphone[0]);
                            return new IDMSResponseWrapper(uphone[0],'Success',Label.CLQ316IDMS038); 
                        }else 
                            return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS045);     
                    }
                }
                catch(exception e ) {
                    String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
                    Errorcls.IDMScreateLog(null,'IDMSPasswordService','resetPassworduser',UserRecord.MobilePhone,ExceptionMessage,UserRecord.IDMS_Profile_update_source__c,Datetime.now(),null,null,null,false,null);                
                    
                    return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS045);     
                }
            }else 
                return new IDMSResponseWrapper(null,'Error',Label.CLQ316IDMS045); 
        }
    }
    
}