global Class CaseArchiving_ProcessRelatedCase implements Database.batchable<Sobject>{
    global Database.QueryLocator start (Database.BatchableContext bc){
        String sqlQuery;
        String strYear=System.Label.CLJUL14CCC02;
        integer intClsDate = Integer.Valueof(strYear);
        Date clsDate =System.today().addYears(intClsDate);  
        System.debug(clsDate);
        String strYearNew=System.Label.CLJUL14CCC03;
        integer intClsDateNew = Integer.Valueof(strYearNew);
        Date clsDateNew =System.today().addYears(intClsDateNew);  
        System.debug(clsDateNew);
        Integer size = Integer.Valueof(System.Label.CLJUL14CCC04);            
        if(!Test.isRunningTest()){
            if(size==1)
                sqlQuery='Select id,RelatedSupportCase__c,caseNumber,RelatedSupportCase__r.CaseNumber from Case Where caseNumber ='+System.label.CLJUL14CCC07+ 'and Status='+System.label.CLJUL14CCC01+' and ArchivedStatus__c='+System.label.CLJUL14CCC10;
            else
                //sqlQuery='Select id,RelatedSupportCase__c,casenumber,RelatedSupportCase__r.CaseNumber from Case Where Status='+System.label.CLJUL14CCC01+' and ArchivedStatus__c='+System.label.CLJUL14CCC10+ ' and((ClosedDate<=:clsDate and SupportCategory__c!='+ System.label.CLJUL14CCC06+') OR (ClosedDate<=:clsDateNew and SupportCategory__c=' +System.label.CLJUL14CCC06+'))and RelatedSupportcase__c!=null limit ' + size;
               sqlQuery='Select id,RelatedSupportCase__c,casenumber,RelatedSupportCase__r.CaseNumber from Case Where Status='+System.label.CLJUL14CCC01+' and ArchivedStatus__c='+System.label.CLJUL14CCC10 ;
        }
        else
            sqlQuery='Select id,casenumber,relatedSupportcase__c,RelatedSupportCase__r.CaseNumber from Case where RelatedSupportcase__c!=null'; 
        
        System.debug('\n query: '+SqlQuery);
        return  Database.getQueryLocator(sqlQuery);
    }       
    global void execute(Database.BatchableContext Bc,List<Sobject> Scope){
        
        Set<id> aCseIdsToBeDel= new Set<id>();
        Set<id> cseIdsToBeDel= new Set<id>();
        set<String> relatedaCseNameSet= new set<String>();
        Set<id> cseIdSet = new Set<id>();
        set<ID> parentIdSet = new set<Id>();
        List<ArchiveLog__c> lstArchiveLogs = new List<ArchiveLog__c>();
        List<ArchiveCase__c> lstAcsesTobeDel = new List<ArchiveCase__c>();
        Map<String, Case> cseNumRecMap = new Map<String, Case>();           
        Map<String, ArchiveCase__c> aCseNameRecMap = new Map<String, ArchiveCase__c>();
        Map<id,ArchiveCase__C> aCseIdRecMap = new Map<id,ArchiveCase__C>();  
        Map<Id,case> cseIdRecMap = new Map<Id,case>();
        Map<String, Case> relatedCseNumRecMap = new Map<String, Case>();
        
        for(sObject genralisedObj: Scope){
            Case cseObj = (Case)genralisedObj;
            cseNumRecMap.put(cseObj.CaseNumber,cseObj);
            cseIdRecMap.put(cseObj.id,cseObj);
            cseIdSet.add(cseObj.id);
            relatedaCseNameSet.add(cseObj.RelatedSupportCase__r.CaseNumber);
        }
        System.debug('relatedaCseNameSet-->'+relatedaCseNameSet);   
        System.debug('caseNumRecMap=>'+cseNumRecMap);
        /*
        if(Test.isRunningTest()){
            List<ArchiveCase__c>  Acasesobj=[Select id,name from ArchiveCase__c where Name='12345'];
            Set<Id> acaseId = new Set<Id>();
             for(Archivecase__c acseobj: Acasesobj){
                aCseIdRecMap.put(acseobj.id,acseobj);
                aCseNameRecMap.put(acseobj.name,acseobj);
                acaseId.add(acseobj.id);
             }
        }
        */
        
        List<ArchiveCase__c> lstArchivecses = [select id,name from Archivecase__c where Name in:cseNumRecMap.keyset()];
        System.debug('lstArchivecses->'+lstArchivecses);
        for(ArchiveCase__c ac:lstArchivecses){
            aCseNameRecMap.put(ac.Name,ac);
            acseIdRecMap.put(ac.id,ac);
        }
        System.debug('cseIdSet'+cseIdSet );
        System.debug('aCseIdRecMap-> '+aCseIdRecMap);
        System.debug('aCseNameRecMap-> '+aCseNameRecMap);
            //Sepearate 
        /*    
        if(Test.isRunningTest()){
            List<ArchiveCase__c>  lstacase=[Select id,name from ArchiveCase__c where Name='123456'];
            for(archivecase__c relAcase: lstacase){
                aCseNameRecMap.put(relAcase.name,relAcase);
            }
        } 
        else{
*/
            List<archivecase__c> lstRelatedACases= [select id,name from archivecase__c where Name IN:relatedaCseNameSet];
            System.debug('lstRelatedACases:'+lstRelatedACases);   
            for(archivecase__c relAcaseObj: lstRelatedACases){
                aCseNameRecMap.put(relAcaseObj.name,relAcaseObj);
            } 
       // }    
                
        List<case> lstRelatedCases = [select id,casenumber,RelatedSupportCase__c,RelatedSupportCase__r.casenumber from case where casenumber IN:relatedaCseNameSet];
        for(case relatedCseObj : lstRelatedCases){
           relatedCseNumRecMap.put(relatedCseObj.casenumber,relatedCseObj);
        }
        
        relatedaCseNameSet.addAll(cseNumRecMap.Keyset());
        System.debug('relatedaCseNameSet :'+relatedaCseNameSet );
        System.debug('relatedCseNumRecMap:'+relatedCseNumRecMap);
       /*
        if(Test.isRunningTest()){
            List<Archivecase__c> lstArchiveCases= [select id,name,RelatedSupportArchiveCase__c from archivecase__c where Name='123456'];
            for(archiveCase__c acaseObj: lstArchiveCases){
               if (aCseIdRecMap.containsKey(acaseObj.id)==true ){
                    if(CseNumRecMap.containskey(Acaseobj.name)){
                        if(aCseNameRecMap.containskey(CseNumRecMap.get(Acaseobj.name).RelatedSupportCase__r.CaseNumber)){           
                            acaseObj.RelatedSupportArchiveCase__c=aCseNameRecMap.get(CseNumRecMap.get(Acaseobj.name).RelatedSupportCase__r.CaseNumber).id;
                            System.debug('acaseObj:'+acaseObj) ;
                        }
                    }   
                }  
            }
        }
      */
        List<Archivecase__c> lstArchiveCases= [select id,name,RelatedSupportArchiveCase__c from archivecase__c where Name In :cseNumRecMap.Keyset()];
        System.debug('lstArchiveCases:'+lstArchiveCases);    
        for(archiveCase__c acaseObj: lstArchiveCases){
           if (aCseIdRecMap.containsKey(acaseObj.id)==true ){
                if(CseNumRecMap.containskey(Acaseobj.name)){
                    if(aCseNameRecMap.containskey(CseNumRecMap.get(Acaseobj.name).RelatedSupportCase__r.CaseNumber)){           
                        acaseObj.RelatedSupportArchiveCase__c=aCseNameRecMap.get(CseNumRecMap.get(Acaseobj.name).RelatedSupportCase__r.CaseNumber).id;
                    System.debug('acaseObj:'+acaseObj) ;
                    }
                }   
            } 
        }   
        System.debug('lstArchiveCases'+lstArchiveCases);
        

        if((lstArchiveCases != null && lstArchiveCases.size()>0)){
            Database.SaveResult[] sresults  =  Database.update (lstArchiveCases, false);
                for(Integer k=0;k<sresults.size();k++ ){
                    Database.SaveResult sr =sresults[k];
                    if(sr.isSuccess()&&!Test.isRunningTest())
                        System.debug('Successfully inserted ID: ' + sr.getId());
                    
                    else{
                        
                        String str = '';
                        ArchiveLog__c aLogObj = new ArchiveLog__c();
                        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
                        String idstr ='';
                        if(Test.isRunningTest()) {
                            idstr=lstArchiveCases[k].id;
                        }
                        else {
                            idstr=relatedCseNumRecMap.get(lstArchiveCases[k].Name).id;
                        }
                        //String idstr=relatedCseNumRecMap.get(lstArchiveCases[k].Name).id;
                        String RecordURL =sfdcBaseURL + '/' +idstr;
                        for(Database.Error err : sr.getErrors()){
                            str =str+err.getMessage();
                        }
                        aLogObj.ErrorMessage__c=str;
                        aLogObj.Date__c=System.now();
                        aLogObj.RecordURL__c=RecordURL;
                        aLogObj.FailedRecordName__c=lstArchiveCases[k].RelatedSupportArchiveCase__c;
                        lstArchiveLogs.add(aLogObj);
                        aCseIdsToBeDel.add(lstArchiveCases[k].RelatedSupportArchiveCase__c);
                        System.debug('idstr:'+idstr);
                        cseIdsToBeDel.add(idstr);
                    }    
                }    
            if(lstArchiveLogs != null && lstArchiveLogs.size()>0)
                insert lstArchiveLogs;
        } 
        /*
        for(Id aCseId : aCseIdsToBeDel){
            ArchiveCase__c acse = new ArchiveCase__c(Id=aCseId);
            lstAcsesTobeDel.add(acse);  
        }
        if(lstAcsesTobeDel!=null && lstAcsesTobeDel.size()>0  && !Test.isRunningTest())
            delete lstAcsesTobeDel; */
        if(cseIdsToBeDel != null && cseIdsToBeDel.size()>0 ){
        
            List<Case> lstFailedRelatedCses = [select id, ArchivedStatus__c,caseNumber from case where id in :cseIdsToBeDel];
            for(Case cobj:lstFailedRelatedCses )
                cobj.ArchivedStatus__c = 'Failed';
        
            Database.SaveResult[] sresults1  =  Database.update(lstFailedRelatedCses, false);
        }
        if(cseIdSet!=null && cseIdSet.size()>0){
            cseIdSet.removeAll(cseIdsToBeDel);
        }
        if(cseIdSet!=null && cseIdSet.size()>0){
            List<Attachment> lstAttachments = [select id,ParentID from attachment where ParentID in :cseIdSet];
            List<case> caseList=[select id,CaseNumber,RelatedSupportCase__r.casenumber,Archivedstatus__c,ArchivedDate__c from case where id  in : cseIdSet];
            for(Attachment Attachobj : lstAttachments){
                parentIdSet.add(Attachobj.ParentID);
            }
            
            for(case cObj:caseList){
                if(!aCseNameRecMap.containsKey(cObj.RelatedSupportCase__r.casenumber)){
                    cobj.ArchivedStatus__c='Has Parent Case';   
                }
                else if(parentIdSet.contains(cObj.id)){
                    cobj.ArchivedStatus__c='Has Attachment';   
                }
                else if(aCseNameRecMap.containsKey(cObj.RelatedSupportCase__r.casenumber)){
                    cObj.ArchivedStatus__c='success';
                    cobj.ArchivedDate__c=system.now();
                }
            }   
            if(caseList!=null && caseList.size()>0)
            Database.SaveResult[] sresults  =  Database.update(caseList, false);
        }
    }
    global void finish(Database.BatchableContext Bc){
        
    }
}