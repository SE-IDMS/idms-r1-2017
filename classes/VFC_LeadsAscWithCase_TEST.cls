@isTest
private class VFC_LeadsAscWithCase_TEST 
{
    
    
    static testMethod void testdisplayLeads() 
    {
            test.startTest();
            //Account acc = Utils_TestMethods.createAccount();
            //Database.SaveResult accountSaveResult = Database.insert(Acc,objDMLOptions); 
            
            Account acc = Utils_TestMethods.createAccount();
            insert acc;
            
            Contact con = Utils_TestMethods.createContact(acc.id,'Test contact');   
            insert con;
            
            Case cse = Utils_TestMethods.createCase(acc.id,con.id,'New');
            insert cse;
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            
            Lead lead2 = Utils_TestMethods.createLead(null, country.Id,100);
            lead2.Case__c=cse.id;
            insert lead2;

            lead2.ClassificationLevel1__c='LC';
            lead2.LeadingBusiness__c='BD';
            lead2.SuspectedValue__c=100;
            lead2.ZipCode__c='123789';
            lead2.OpportunityFinder__c ='Lead Agent';
            lead2.City__c='US';
            lead2.Street__c = 'testStreet';
            update lead2;
            
            /*Database.LeadConvert leadConv = new database.LeadConvert();
            leadConv.setLeadId(lead2.id);
            leadConv.setConvertedStatus('3 - Closed - Opportunity Created');
            
            Database.LeadConvertResult lcr = Database.convertLead(leadConv);
            */
            lead2 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,Contact__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,ConvertedContactId,opportunity__c from lead where Id =: lead2.Id limit 1];            
            
            Lead lead1 = Utils_TestMethods.createLeadWithCon(con.Id, country.Id,100);
            lead1.Account__c= con.Account.id;
            insert lead1;            
            lead1 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Contact__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            System.assertEquals(con.Id,lead1.Contact__c);
            
            
            ApexPages.StandardController sc = new ApexPages.StandardController(cse);
            VFC_LeadsAscWithCase leadsAscWithCase= new VFC_LeadsAscWithCase(sc);
            leadsAscWithCase.displayLeads();
            leadsAscWithCase.gotoNewLead();
            leadsAscWithCase.getGotoNewLeadUrl();
            test.stopTest();
          
       
    }
}