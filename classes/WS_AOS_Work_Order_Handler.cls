public class WS_AOS_Work_Order_Handler{

    public Static Set<String> ipGoldenIdset = new Set<String>(); // installed product GoldenId Set
    public Static Set<String> accountGoldenIdset= new Set<String>(); // Account GoldenId Set
    public Static List<WS_AOS_Work_Order.WorkOrderResult> results = new List<WS_AOS_Work_Order.WorkOrderResult>(); // response
    public Static List<WS_AOS_Work_Order.WorkOrder> toProcess= new List<WS_AOS_Work_Order.WorkOrder>(); // 
    public static Set<String> FirstNameSet = new Set<String>();
    public static Set<String> LastNameSet = new Set<String>();
    public static Set<String> EmailSet = new Set<String>();
    public static Set<String> PhoneSet = new Set<String>();
    public static Map<String,SVMXC__Installed_Product__c> ipMap = new Map<String,SVMXC__Installed_Product__c>();

    public Static List<WS_AOS_Work_Order.WorkOrderResult> CreateServiceOrder(List<WS_AOS_Work_Order.WorkOrder> uwoList)    
    {
        List<SVMXC__Service_Order__c> serviceOrderList = new List<SVMXC__Service_Order__c>();
        Set<String>  remoteSysWOIdSet = new Set<String>();
        if(uwoList != null && uwoList.size()>0){
            for(WS_AOS_Work_Order.WorkOrder uwo: uwoList){
                MandatoryFields  mf =   MandataryCheckOnCreate(uwo);
                System.debug('**** mandatory '+mf);
                // 
                if(mf.isMFPass)
                {   
                    ipGoldenIdset.add(uwo.InstalledProduct.GoldenID);
                    accountGoldenIdset.add(uwo.InstalledAtAccount);
                    remoteSysWOIdSet.add(uwo.RemoteSystemWorkOrderID);
                    toProcess.add(uwo);
                    if(uwo.Contact != null){                    
                        WS_AOS_Work_Order.Contact conUnit = uwo.Contact;
                        if(conUnit.FirstName != null && conUnit.FirstName !='')
                        {
                            FirstNameSet.add(conUnit.FirstName);
                        } 
                        if(conUnit.LastName != null && conUnit.LastName !='')
                        {
                            LastNameSet.add(conUnit.LastName);
                        }
                        if(conUnit.Email != null && conUnit.Email !='')
                        {
                            EmailSet.add(conUnit.Email);
                        }
                        if(conUnit.Phone != null && conUnit.Phone !='')
                        {
                            PhoneSet.add(conUnit.Email);
                        }
                    }
                   
                }
                else{
                   // error Response due to Mandatory fields
                       WS_AOS_Work_Order.WorkOrderResult woResult = new WS_AOS_Work_Order.WorkOrderResult();
                       woResult.success = false;
                       woResult.type= 'CREATE';
                       woResult.errorMessage = 'Mandatory Fiels Missing : '+mf.fields;
                       woResult.workOrderNumber = uwo.workOrderNumber ;                    
                       results.add(woResult);
                
                }
            }
        }
        else{
                   WS_AOS_Work_Order.WorkOrderResult woResult = new WS_AOS_Work_Order.WorkOrderResult();
                   woResult.success = false;
                   woResult.type= 'CREATE';
                   woResult.errorMessage = ' Please send Workorder to process';                                    
                   results.add(woResult);
            
        }
        
        if(toProcess != null && toProcess.size()>0)
        {
            
            List<SVMXC__Service_Order__c> existingWO = [Select id, Remote_System_Work_Order_ID__c from SVMXC__Service_Order__c where Remote_System_Work_Order_ID__c in:remoteSysWOIdSet ];
            Map<String,SVMXC__Service_Order__c> existingRSWOMap = new Map<String,SVMXC__Service_Order__c>();
            if(existingWO != null && existingWO.size()>0){
                for(SVMXC__Service_Order__c wo: existingWO){
                    existingRSWOMap.put(wo.Remote_System_Work_Order_ID__c, wo);
                }
            }
            List<SVMXC__Service_Order__c> workOrderList = new List<SVMXC__Service_Order__c>();
            for(WS_AOS_Work_Order.WorkOrder uwo: toProcess){
            
                if(!existingRSWOMap.containskey(uwo.RemoteSystemWorkOrderID))
                {
                    workOrderList.addALL(prepareServiceOrder(new List<WS_AOS_Work_Order.WorkOrder>{uwo}));
                }
                else{
                    WS_AOS_Work_Order.WorkOrderResult woEResult = new WS_AOS_Work_Order.WorkOrderResult();
                    woEResult.success = false;
                    woEResult.type = 'CREATE';
                    woEResult.errorMessage =  ' work order with Remote System WorkOrder ID '+uwo.RemoteSystemWorkOrderID +' allready exist ';
                    woEResult.workOrderNumber =  uwo.workOrderNumber;                   
                    results.add(woEResult);
                }
            
            }
            
            if(workOrderList != null && workOrderList.size()>0)
            {
                 Database.SaveResult[] srresults = CreateOrUpdateServices((List<Sobject>)workOrderList,'CREATE');
                 Map<id,SVMXC__Service_Order__c> woMap = new Map<id,SVMXC__Service_Order__c>();
                 Set<id> woidset = new Set<id>();
                 for(Integer k=0;k<srresults.size();k++ )
                {
                  Database.SaveResult sr = srresults[k];  
                  if(sr.isSuccess())
                    {
                         woidset.add(sr.getId());
                    }
                }
                if(woidset!= null && woidset.size()>0)
                {
                    woMap.putAll([Select id,WorkOrderGroup__c from SVMXC__Service_Order__c where id in :woidset ]);
                    
                }
                for(Integer k=0;k<srresults.size();k++ )
                {   
                     Database.SaveResult sr = srresults[k];
                     WS_AOS_Work_Order.WorkOrderResult woResult = new WS_AOS_Work_Order.WorkOrderResult();
                     
                    if(sr.isSuccess())
                    {
                        woResult.success = true;
                        woResult.type = 'CREATE';
                        //webservice String errorMessage{get;set;}
                        woResult.workOrderbFOID = sr.getId();
                        woResult.workOrderNumber = workOrderList[k].WorkOrderName__c;
                        woResult.workOrderGroupbFOID = woMap.get(sr.getId()).WorkOrderGroup__c;
                        //workOrderGroupbFOID{get;set;}
                        results.add(woResult);
                       
                    }
                    else{
                        woResult.success = false;
                        woResult.type = 'CREATE';
                        //webservice String errorMessage{get;set;}
                         String ErroMessage ='';
                        for(Database.Error err : sr.getErrors()) {
                            ErroMessage = ' '+err.getFields()+' : '+err.getMessage();                              
                        }
                        woResult.errorMessage =  ErroMessage;
                        woResult.workOrderNumber = workOrderList[k].WorkOrderName__c; 
                        results.add(woResult);
                        
                    }
                    
                }                
                 
                 
            }
            
        }
        
        return results;
    
    }
    
    public Static MandatoryFields MandataryCheckOnCreate(WS_AOS_Work_Order.WorkOrder unitWO)    
    {
        
            MandatoryFields mf = new MandatoryFields();
            String mandatoryFields ='';                
            Boolean mandatory = true;
                
                if(unitWO.InstalledProduct == null ){
                     mandatory = false;
                     mandatoryFields += mandatoryFields + ' InstalledProduct  ';
                } 
                else{
                    if( unitWO.InstalledProduct.GoldenID == null || unitWO.InstalledProduct.GoldenID == '')
                    {   
                        mandatory = false;
                        mandatoryFields += mandatoryFields + ' InstalledProduct GoldenId ';
                    }
                }
                if(unitWO.CustomerRequestDate == null){          
                     mandatory = false;
                     mandatoryFields += mandatoryFields + ' CustomerRequestDate '; 
                }               
                if(unitWO.Priority == null || unitWO.Priority ==''){   
                    mandatory = false;
                    mandatoryFields += mandatoryFields + ' Priority '; 
                }                
                if(unitWO.WorkOrderReason == null || unitWO.WorkOrderReason ==''){ 
                    mandatory = false;
                    mandatoryFields += mandatoryFields + ' WorkOrderReason ';
                }                
                if(unitWO.CommentsToPlanner == null || unitWO.CommentsToPlanner ==''){   
                    mandatory = false;
                    mandatoryFields += mandatoryFields + ' CommentsToPlanner '; 
                }
                /*              
                if(unitWO.Status != null && unitWO.Status !=''){ 
                     mandatory = false;
                    mandatoryFields += mandatoryFields + ' Status '; 
                }                
                if(unitWO.Category != null && unitWO.Category !=''){
                    mandatory = false;
                    mandatoryFields += mandatoryFields + ' Category ';
                }                
                if(unitWO.Type != null && unitWO.Type !=''){ 
                    mandatory = false;
                    mandatoryFields += mandatoryFields + ' Type ';
                }                
                if(unitWO.SubType != null && unitWO.SubType !=''){ 
                    mandatory = false;
                    mandatoryFields += mandatoryFields + ' SubType ';
                }                
                if(unitWO.IsBillable != null && unitWO.IsBillable !=''){ 
                    mandatory = false;
                    mandatoryFields += mandatoryFields + ' IsBillable ';
                }                
                if(unitWO.Location != null && unitWO.Location !=''){
                    mandatory = false;
                    mandatoryFields += mandatoryFields + ' Location ';
                }*/
                
                 if(unitWO.RemoteSystemWorkOrderID == null || unitWO.RemoteSystemWorkOrderID ==''){
                    mandatory = false;
                    mandatoryFields += mandatoryFields + ' RemoteSystemWorkOrderID ';
                }
            mf.isMFPass =  mandatory;
            mf.fields = mandatoryFields;
            
            
            return mf;
    
    }
    public Static SVMXC__Service_Order__c prepareServiceOrder(WS_AOS_Work_Order.WorkOrder uwo)    
    {
        SVMXC__Service_Order__c serviceOrder = new SVMXC__Service_Order__c();
        
        serviceOrder.CustomerRequestedDate__c = uwo.CustomerRequestDate;
        serviceOrder.SVMXC__Priority__c = uwo.Priority;
        serviceOrder.SVMXC__Problem_Description__c = uwo.WorkOrderReason;
        serviceOrder.Comments_to_Planner__c = uwo.CommentsToPlanner;
        serviceOrder.WorkOrderName__c= uwo.workOrderNumber;
        
        // Default Fields
        serviceOrder.SVMXC__Order_Status__c = 'New';
        serviceOrder.Work_Order_Category__c = 'Onsite';
        serviceOrder.SVMXC__Order_Type__c = 'Preventive Maintenance';
        serviceOrder.IsBillable__c = 'No';
        serviceOrder.Remote_System_Work_Order_ID__c = 'AOS';
        
        return serviceOrder ;
    
    }
    public Static List<SVMXC__Service_Order__c> prepareServiceOrder(List<WS_AOS_Work_Order.WorkOrder> uwoList)    
    {
        
        List<SVMXC__Service_Order__c> serviceOrderList = new List<SVMXC__Service_Order__c>();
        String SoqlQuery = getContactQuery();       
        System.debug('/n SoqlQuery : '+SoqlQuery);      
        String IpSoqlQuery  = getInstalledProductQuery();
        System.debug('/n IpSoqlQuery : '+IpSoqlQuery);  
        List<Sobject> conList = new List<Sobject>();
        if(SoqlQuery.length()>0)
        conList = DataBase.query(SoqlQuery);
        List<SVMXC__Installed_Product__c> ipList = DataBase.query(IpSoqlQuery);
        for(SVMXC__Installed_Product__c ipobj: ipList){
            ipMap.put(ipobj.GoldenAssetId__c,ipobj);
        }
        for(WS_AOS_Work_Order.WorkOrder uwo: uwoList){
            SVMXC__Service_Order__c woobj = prepareServiceOrder(uwo);
            if(uwo.Contact != null )
            {
                MatchContact(conList,woobj,uwo.Contact); 
            }
            if(uwo.InstalledProduct != null  )
            {
                if(ipMap.containskey(uwo.InstalledProduct.GoldenID))
                {
                    woobj.SVMXC__Component__c = ipMap.get(uwo.InstalledProduct.GoldenID).id;
                    woobj.SVMXC__Company__c = ipMap.get(uwo.InstalledProduct.GoldenID).SVMXC__Company__c;
                    woobj.SVMXC__Site__c = ipMap.get(uwo.InstalledProduct.GoldenID).SVMXC__Site__c;
                }
                
            }
            //woobj.ServiceLine__c = 
            //woobj.SVMXC__Service_Contract__c =
            //woobj.Service_Business_Unit__c =

            serviceOrderList.add(woobj);
        }
        
        return serviceOrderList ;
    
    }
    
    public Static void  MatchContact(List<Sobject> conList ,SVMXC__Service_Order__c wo , WS_AOS_Work_Order.Contact ucon ){
         String key ='';
         String ckey ='';
         ID ContactId;
         if(ucon.FirstName != null && ucon.FirstName !='')
            key +=ucon.FirstName;
        if(ucon.LastName != null && ucon.LastName !='')
            key +=ucon.LastName;
        if(ucon.Email != null && ucon.Email !='')
            key +=ucon.Email;
        if(ucon.Phone != null && ucon.Phone !='')
            key +=ucon.Phone;
        for(Sobject con: conList){
            ckey ='';
            if(ucon.FirstName != null && ucon.FirstName !='')
                if(con.get('FirstName') != null && con.get('FirstName') !='')
                    ckey +=con.get('FirstName');                   
            if(ucon.LastName != null && ucon.LastName !='')
                if(con.get('LastName') != null && con.get('LastName') !='')
                    ckey +=con.get('LastName');
            if(ucon.Email != null && ucon.Email !='')
                if(con.get('Email') != null && con.get('Email') !='')
                    ckey +=con.get('Email');
            if(ucon.Phone != null && ucon.Phone !='')
                if(con.get('MobilePhone') != null && con.get('MobilePhone') !='')
                    ckey +=con.get('MobilePhone'); 
                    
            if(key == ckey){                    
                ContactId = con.id;    
                break ;             
             }
            
        }
        wo.SVMXC__Contact__c = ContactId;   
        if(wo.SVMXC__Contact__c == null)
        {           
            wo.ContactInformation__c  = 'CONTACT AUTO-MATCHING FAILED \n Contact :';
            if(ucon.FirstName != null )
                wo.ContactInformation__c  += ucon.FirstName +' ';
            if(ucon.LastName != null)
                wo.ContactInformation__c  += ucon.LastName +' ';
            if(ucon.Email != null)
                wo.ContactInformation__c  += ucon.Email+' ';
            if(ucon.Phone != null)
                wo.ContactInformation__c  += ucon.Phone+' ';
        
        }
    
    }
    
    // Helper class for mandatory check
    public class MandatoryFields{
        public Boolean isMFPass{get;set;}
        public String fields{get;set;}
        
    }
    public Static Database.SaveResult[] CreateOrUpdateServices(List<SObject> sObjectList,String type)
    {
            Database.SaveResult[] sresults ;
            if(sObjectList != null && sObjectList.size()>0){                
                if(type == 'CREATE' ){
                    sresults = Database.insert(sObjectList, false);
                }
                else if(type == 'UPDATE')
                {
                    sresults = Database.update(sObjectList, false);
                }
            }
        return sresults;
    }
    
    public static String getInstalledProductQuery(){
        
                String Query=' select id, name ,GoldenAssetId__c,SVMXC__Site__c,SVMXC__Company__c,SVMXC__Company__r.SEAccountID__c,CustomerSerialNumber__c, SchneiderCommercialReference__c,SVMXC__Company__r.Country__r.CountryCode__c from SVMXC__Installed_Product__c ';
        
                String QueryString ='';
                String WhereClause ='';
                
                 if(IPGoldenIDset != null && IPGoldenIDset.size()>0)
                {
                        WhereClause =' GoldenAssetId__c in  ('+ SOQLListFormat(IPGoldenIDset) +') ';
                
                }
                if(WhereClause.length()>0)
                QueryString = Query +' Where '+ WhereClause;
                else{
                    QueryString ='';
                }
                System.debug('\n Log WWS : ip query '+QueryString);
                return QueryString;
    }
    public static String getContactQuery(){
        
          String Query='';
                String QueryString ='';
                String WhereClause ='';
               
                String AccountFilter ='';                
                QueryString = ' Select id,SEContactID__c,FirstName,LocalFirstName__c,MidInit__c,LocalMidInit__c,LastName,LocalLastName__c,Email,MobilePhone,WorkPhone__c from Contact ';
                
                if(FirstNameSet != null && FirstNameSet.size()>0)
                {                                       
                    WhereClause += (WhereClause.length()>0) ?  ' OR  FirstName in  ('+ SOQLListFormat(FirstNameSet) +') ' : '  FirstName in  ('+ SOQLListFormat(FirstNameSet) +') ';                    
                }
                if(LastNameSet != null && LastNameSet.size()>0)
                {
                    WhereClause += (WhereClause.length()>0) ?  ' OR  LastName in  ('+ SOQLListFormat(LastNameSet) +') ' : '  LastName in  ('+ SOQLListFormat(LastNameSet) +') ';
                }
                if(EmailSet != null && EmailSet.size()>0)
                {
                    WhereClause += (WhereClause.length()>0) ?  ' OR  Email in  ('+ SOQLListFormat(EmailSet) +') ' : '  Email in  ('+ SOQLListFormat(EmailSet) +') ';
                }
                if(PhoneSet != null && PhoneSet.size()>0)
                {
                    WhereClause += (WhereClause.length()>0) ?  ' OR  MobilePhone in  ('+ SOQLListFormat(PhoneSet) +') ' : '  MobilePhone in  ('+ SOQLListFormat(PhoneSet) +') ';
                }
                if(WhereClause.length()>0)
                Query =  QueryString +' where '+WhereClause+' Limit 10000 ';
                else{Query = '';}
                System.debug('\n scLog :'+Query);
                return Query;
        
        
        return null;
    }
    //description : format the list to inorporate to a dynamic request
    //By adding quotes between each values
    //input list of string
    //outpu string formated list
    public static string SOQLListFormat(set<string> input){   
        String SOQL_ListFormat = '';
        for (string Value : input) {
                String value_in_quotes = '\''+String.escapeSingleQuotes(Value)+'\'';
        if (SOQL_ListFormat!='') { SOQL_ListFormat+=','; }  //  add a comma if this isn't the first one
                SOQL_ListFormat += value_in_quotes;
        }   
        return SOQL_ListFormat;
    }


}