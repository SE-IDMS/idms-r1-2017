/**
    Author          : Nitin Khunal
    Date Created    : 20/07/2016
    Description     : Test Class for VFC_PACaseCreate apex class
*/
@isTest
private class VFC_PACaseCreate_TEST {
    
    @testSetup static void testData() {
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.SEAccountID__c = '12345678';
        testAccount.City__c = 'Bangalore';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        insert testcontact;
        
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        CTR_ValueChainPlayers__c testCVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        testCVCP.legacypin__c ='1234';
        testCVCP.Contact__c = testcontact.Id;
        insert testCVCP;
        
        List<OPP_Product__c> lstOppProduct = new List<OPP_Product__c>();
        OPP_Product__c oppProductTest1 = Utils_TestMethods.createProductHierarchyAtFamily('INDUSTRIAL AUTOMATION', 'IDMOT - Motion Control', 
                                                                                        'OLD', 'GDP UTILISES DANS GM');
        oppProductTest1.Name = '+/- 10V ANALOG DRIVES';
        oppProductTest1.TECH_PM0CodeInGMR__c = '7546__';
        lstOppProduct.add(oppProductTest1);

        OPP_Product__c oppProductTest2 = Utils_TestMethods.createProductHierarchyAtFamily('INDUSTRIAL AUTOMATION', 'PTEQP - LV EQPT (IEC&NEMA)', 
                                                                                        'INTEGRTD EQPT OPT NEMA', 'IPC2/IPC/MPS');
        oppProductTest2.Name = '04DMPARTKIT';
        oppProductTest2.TECH_PM0CodeInGMR__c = '7546____';
        lstOppProduct.add(oppProductTest2);

        OPP_Product__c oppProductTest3 = Utils_TestMethods.createProductHierarchyAtFamily('INDUSTRIAL AUTOMATION', 'PTEQP - LV EQPT (IEC&NEMA)', 
                                                                                        'INTEGRTD EQPT OPT NEMA', 'IPC2/IPC/MPS');
        oppProductTest3.Name = '04DMPARTKITT';
        oppProductTest3.TECH_PM0CodeInGMR__c = '75466__';
        lstOppProduct.add(oppProductTest3);
        insert lstOppProduct;

        GCSSite__c testSite = new GCSSite__c(Name = 'Test Site', Account__c = testAccount.Id, Active__c = true, LegacySiteId__c = '12345');
        Insert testSite;

    }
    
    @isTest static void testMethod1() {
        VFC_PACaseCreate caseCreateController = new VFC_PACaseCreate();
        List<OPP_Product__c> lstOppProduct = VFC_PACaseCreate.getOthers('7546');
        system.assertEquals(lstOppProduct[0].TECH_PM0CodeInGMR__c, '7546__');

        List<OPP_Product__c> lstOppProduct1 = VFC_PACaseCreate.getOthers('75466');
        VFC_PACaseCreate.getLoggedInUserDateTime();
    }

    @isTest static void testMethod2() {
        List<VFC_PACaseCreate.SiteRadioWrapper> lstSites = VFC_PACaseCreate.SearchSites('Test', '');
        system.assertEquals(lstSites[0].siteobj.name, 'Test Site');
        system.assertEquals(lstSites[0].siteobj.Active__c, True);
        system.assertEquals(lstSites[0].siteobj.LegacySiteId__c, '12345');
    }

    @isTest static void testMethod3() {
        //BusinessRiskEscalationEntity__c testBusinessRisk = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        //testBusinessRisk.Name = system.label.CLQ316PA024;
        //insert testBusinessRisk;
        Country__c testCountry = [select id, CountryCode__c from Country__c limit 1];
        CustomerCareTeam__c ccCare = Utils_TestMethods.CustomerCareTeam(testCountry.Id);
        ccCare.Name = 'test';
        insert ccCare;
        PA_CaseTeamAssignment__c caseSetting = new PA_CaseTeamAssignment__c(name=testCountry.CountryCode__c, Assigned_CC_Team__c = ccCare.Id);
        insert caseSetting;
        Contact con = [select accountId from Contact limit 1];
        GCSSite__c gcsSite = [select id from GCSSite__c limit 1];
        OPP_Product__c oppProduct = [select id from OPP_Product__c limit 1];
        String testCaseId = VFC_PACaseCreate.caseCreate(con.AccountId, con.id, gcsSite.Id, userinfo.getUserId(), 'Laptop X200', '', '', 
                                                        'Test', 'Testing', true, 'Technical Support (TEC)', 'H90', '12345', '2016-08-22 16:03:00');
        
    }

    @isTest static void testMethod4() {
        Account acc = [select id from Account limit 1];
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId='12345'; 
        uInfo.strAccountGoldenId='12345678';
        uInfo.strCustomerFirstId='1234';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInv'; 
        WS_PAPortletUserManagement.SiteResult userresult = WS_PAPortletUserManagement.createUser(uInfo);
        WS_PAPortletUserManagement.ManualSharingInformation manualShareInfo = new WS_PAPortletUserManagement.ManualSharingInformation();
        manualShareInfo.strAccountGoldenId = '12345678';
        manualShareInfo.strSharingAccessLevel = 'Edit';
        manualShareInfo.strWebUserName = 'testInv';
        WS_PAPortletUserManagement.SiteResult userresult1 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo);
        system.runAs(new user(Id = userresult.userId)) {
            VFC_PACaseCreate.pickaccountValue();
            VFC_PACaseCreate.userContact();
        }
        VFC_PACaseCreate.pickContactValue(acc.Id);
        list<contact> lstContact = VFC_PACaseCreate.searchContact('Contact', acc.Id);
        system.assertEquals(lstContact[0].Name, 'Test Contact');
    }
    
    @isTest static void testMethod5() {
               
        Account objAccount = [select id from Account limit 1];
        
        Contact objContact = [select id from Contact limit 1];
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        // Create several Products
        List<Product2> productToInsert = new List<Product2>();
        Integer usecase = 0;
        for(Integer i=0 ; i <= 15 ; i++){ //(String commercialReference,String strMaterialDescription, String businessUnit, String productLine, String productFamily, String family,
            if(usecase == 1){
                Product2 objCommercialReference = Utils_TestMethods.createCommercialReference('VFCPROD'+i,'MaterialDescription For VFCPROD'+i,'BU-VFCPROD'+i, 'PL-VFCPROD'+i, 'PF-VFCPROD'+i, 'F-VFCPROD1',false, false, false);
                productToInsert.add(objCommercialReference);
                usecase = 0;
            }else{
                Product2 objCommercialReference = Utils_TestMethods.createCommercialReference('VFCPROD'+i,'MaterialDescription For VFCPROD'+i,'BU-VFCPROD'+i, 'PL-VFCPROD'+i, 'PF-VFCPROD'+i, 'F-VFCPROD0',false, false, false);
                productToInsert.add(objCommercialReference);
                usecase = 1;
            }
        }
        Product2 objCommercialReference1 = Utils_TestMethods.createCommercialReference('VFCPROD22','MaterialDescription For VFCPROD','BU-VFCPROD', 'PL-VFCPROD', 'PF-VFCPROD', 'F-VFCPROD3',false, false, false);
        productToInsert.add(objCommercialReference1);
        Product2 objCommercialReference2 = Utils_TestMethods.createCommercialReference('VFCPROD33','MaterialDescription For VFCPROD','BU-VFCPROD', 'PL-VFCPROD', 'PF-VFCPROD', 'F-VFCPROD4',false, false, false);
        productToInsert.add(objCommercialReference2);
        Database.insert(productToInsert);
        
        List<Product2> searchresults = [Select id,Name,ProductFamilyId__c,ProductGDP__r.HierarchyType__c,ProductGDP__r.HierarchyTypeoftheParent__c from Product2];
        String debug = '';
        for(Product2 prod : searchResults){
            debug += prod.ProductFamilyId__c +','+prod.ProductGDP__r.HierarchyType__c+','+prod.ProductGDP__r.HierarchyTypeoftheParent__c+' || ';
        }
        system.debug('######## searchresults debug '+debug);
        // Create 2 Case Related Product
        objCommercialReference1 = [Select id,ProductGDP__r.HierarchyType__c,ProductGDP__r.HierarchyTypeoftheParent__c,ProductCode,Name,ProductGDP__c,CommercialReference__c,ProductGDP__r.BusinessUnit__c,ProductGDP__r.ParentHierarchy__c,ProductGDP__r.FamilyID__c,ProductGDP__r.ProductLine__c,ProductGDP__r.ProductFamily__c,ProductGDP__r.Family__c,ProductGDP__r.SubFamily__c,ProductGDP__r.Series__c,ProductGDP__r.GDP__c,ProductGDP__r.TECH_PM0CodeInGMR__c,GDPGMR__c,toLabel(CommercialStatus__c),ProductFamilyId__c from Product2 where Id =:objCommercialReference1.Id];  
        objCommercialReference2 = [Select id,ProductGDP__r.HierarchyType__c,ProductGDP__r.HierarchyTypeoftheParent__c,ProductCode,Name,ProductGDP__c,CommercialReference__c,ProductGDP__r.BusinessUnit__c,ProductGDP__r.ParentHierarchy__c,ProductGDP__r.FamilyID__c,ProductGDP__r.ProductLine__c,ProductGDP__r.ProductFamily__c,ProductGDP__r.Family__c,ProductGDP__r.SubFamily__c,ProductGDP__r.Series__c,ProductGDP__r.GDP__c,ProductGDP__r.TECH_PM0CodeInGMR__c,GDPGMR__c,toLabel(CommercialStatus__c),ProductFamilyId__c from Product2 where Id =:objCommercialReference2.Id];  
        
        system.debug('######## objCommercialReference1 '+objCommercialReference1.ProductFamilyId__c+'/'+objCommercialReference1.ProductGDP__r.HierarchyType__c+'/'+objCommercialReference1.ProductGDP__r.HierarchyTypeoftheParent__c);
        
        system.debug('######## objCommercialReference2 '+objCommercialReference2.ProductFamilyId__c+'/'+objCommercialReference2.ProductGDP__r.HierarchyType__c+'/'+objCommercialReference2.ProductGDP__r.HierarchyTypeoftheParent__c);
        
        CSE_RelatedProduct__c objCaseRelatedProduct1 = new CSE_RelatedProduct__c();
        objCaseRelatedProduct1.Case__c = objOpenCase.Id;
        objCaseRelatedProduct1.CommercialReference__c = objCommercialReference1.Name;
        objCaseRelatedProduct1.CommercialReference_lk__c = objCommercialReference1.Id;
        objCaseRelatedProduct1.Family_lk__c = objCommercialReference1.ProductFamilyId__c;
        objCaseRelatedProduct1.BusinessUnit__c = objCommercialReference1.ProductGDP__r.BusinessUnit__c;
        objCaseRelatedProduct1.ProductLine__c = objCommercialReference1.ProductGDP__r.ProductLine__c;
        objCaseRelatedProduct1.ProductFamily__c = objCommercialReference1.ProductGDP__r.ProductFamily__c;
        objCaseRelatedProduct1.Family__c = objCommercialReference1.ProductGDP__r.Family__c;
        objCaseRelatedProduct1.TECH_GMRCode__c = objCommercialReference1.ProductGDP__r.TECH_PM0CodeInGMR__c;
        objCaseRelatedProduct1.TECH_UniqueId__c = objCaseRelatedProduct1.Case__c + '-' + objCaseRelatedProduct1.CommercialReference_lk__c;
        objCaseRelatedProduct1.Name = objCommercialReference1.Name;
        objCaseRelatedProduct1.MasterProduct__c = true;
        Database.insert(objCaseRelatedProduct1);
        
        CSE_RelatedProduct__c objCaseRelatedProduct2 = new CSE_RelatedProduct__c();
        objCaseRelatedProduct2.Case__c = objOpenCase.Id;
        objCaseRelatedProduct2.Family_lk__c = objCommercialReference2.ProductFamilyId__c;
        objCaseRelatedProduct2.BusinessUnit__c = objCommercialReference2.ProductGDP__r.BusinessUnit__c;
        objCaseRelatedProduct2.ProductLine__c = objCommercialReference2.ProductGDP__r.ProductLine__c;
        objCaseRelatedProduct2.ProductFamily__c = objCommercialReference2.ProductGDP__r.ProductFamily__c;
        objCaseRelatedProduct2.Family__c = objCommercialReference2.ProductGDP__r.Family__c;
        objCaseRelatedProduct2.TECH_GMRCode__c = objCommercialReference2.ProductGDP__r.TECH_PM0CodeInGMR__c;
        objCaseRelatedProduct2.TECH_UniqueId__c = objCaseRelatedProduct2.Case__c + '-' + objCaseRelatedProduct2.Family_lk__c;
        objCaseRelatedProduct2.Name = objCommercialReference2.Name;
        objCaseRelatedProduct2.MasterProduct__c = false;
        objCaseRelatedProduct2.SubFamily__c = null;
        objCaseRelatedProduct2.ProductSuccession__c = null;
        objCaseRelatedProduct2.ProductGroup__c = null;
        objCaseRelatedProduct2.ProductDescription__c = null;
        objCaseRelatedProduct2.CommercialReference__c = null;
        objCaseRelatedProduct2.CommercialReference_lk__c = null;
        Database.insert(objCaseRelatedProduct2);
            
        Test.startTest();
        //WS_GMRSearch.resultFilteredDataBean results;
        VFC_PACaseCreate.getOthers(objCommercialReference1.GDPGMR__c.subString(0,2));
        VFC_PACaseCreate.getOthers(objCommercialReference1.GDPGMR__c.subString(0,4));
        VFC_PACaseCreate.getOthers(objCommercialReference1.GDPGMR__c.subString(0,6));
        VFC_PACaseCreate.getOthers(objCommercialReference1.GDPGMR__c.subString(0,8));

        /** 
            Test : Function remoteSearch 
        **/
        List<Object>  lstFilteredDataBean = VFC_PACaseCreate.remoteSearch('',objCommercialReference1.GDPGMR__c.subString(0,4),false,false,false,false,false);
        lstFilteredDataBean = VFC_PACaseCreate.remoteSearch('VFCPROD',objCommercialReference1.GDPGMR__c.subString(0,4),false,false,false,false,false);
        lstFilteredDataBean = VFC_PACaseCreate.remoteSearch('VFCPROD',objCommercialReference1.GDPGMR__c.subString(0,6),false,false,false,false,false);
        lstFilteredDataBean = VFC_PACaseCreate.remoteSearch('VFCPROD',objCommercialReference1.GDPGMR__c.subString(0,8),false,false,false,false,false);
        lstFilteredDataBean = VFC_PACaseCreate.remoteSearch('VFCPROD',objCommercialReference1.GDPGMR__c,false,false,false,false,false);
        //lstFilteredDataBean = VFC_PACaseCreate.remoteSearch('','',true,true,true,true,true);
        
    }
}