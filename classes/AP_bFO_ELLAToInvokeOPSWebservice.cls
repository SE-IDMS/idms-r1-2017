/*

*/

global class  AP_bFO_ELLAToInvokeOPSWebservice {

    public static string serviceResult;
    webService static AP_ELLA_TO_OPS_WebServcieWithdrawal.responseMes invokeOPSWebservice(string offerId) {
        if(offerId!=null) {
            //serviceResult=AP_ELLA_TO_OPS_WebServcieWithdrawal.withdrawalObjectDataToOPS(offerId); 
            return AP_ELLA_TO_OPS_WebServcieWithdrawal.withdrawalObjectDataToOPS(offerId); 
        }
        return AP_ELLA_TO_OPS_WebServcieWithdrawal.generateResp(Label.CLJUNE15ELLABFO13, Label.CLJUNE15ELLABFO12);// Problem in communication. Please contact the administrator // Error
    }
    
    webService static AP_ELLA_TO_OPS_WebServcieWithdrawal.responseMes invokeSubstitutionOPSWebservice(string WithdrSubstitutionId) {
        if(WithdrSubstitutionId!=null) {
            //serviceResult = AP_ELLA_TO_OPS_WebServcieWithdrawal.SubstitutionObjectDataToOPS(WithdrSubstitutionId); 
            return AP_ELLA_TO_OPS_WebServcieWithdrawal.SubstitutionObjectDataToOPS(WithdrSubstitutionId);  
        }
        return AP_ELLA_TO_OPS_WebServcieWithdrawal.generateResp(Label.CLJUNE15ELLABFO13, Label.CLJUNE15ELLABFO12); // Problem in communication. Please contact the administrator // Error
    }   
    

    //Withdrawal Button to Check Job status  
    //webService static string checkWithdrawalJobStatusInOPS(string withdrawalId, string callFor) {
    webService static AP_ELLA_TO_OPS_WebServcieWithdrawal.responseMes checkWithdrawalJobStatusInOPS(string withdrawalId, string callFor) {
        List<Offer_Lifecycle__c>  listWithdrawal = new List<Offer_Lifecycle__c >();
        if(withdrawalId!=null) {
            listWithdrawal =[select id,WithdrawalOPSJobId__c,Withdrawal_Name__c,End_of_CommercializationAnnouncementDate__c,End_of_Services_Date__c,End_of_Commercialization_Date__c,End_of_Services_Announcement_Date__c,Customer_Warnings_Precautions__c, SubstitutionOPSJobId__c,SubstitutionOPSJobStatusDetails__c from Offer_Lifecycle__c  where id=:withdrawalId];
            if(listWithdrawal.size() > 0) {
                if(callFor != null && callFor != '' && callFor == Label.CLMAY15ELLABFO04){ // Withdrawal
                    if(listWithdrawal[0].WithdrawalOPSJobId__c!=null) {
                        return AP_ELLA_TO_OPS_WebServcieWithdrawal.getOPSWithdrawalResult(string.valueof(listWithdrawal[0].WithdrawalOPSJobId__c),withdrawalId);
                    }else {
                        return AP_ELLA_TO_OPS_WebServcieWithdrawal.generateResp(Label.CLMAY15ELLABFO02, Label.CLJUNE15ELLABFO20); // Withdrwal not synced,You can not able to check the Job Status in OPS // Warning
                    }
                }else if(callFor != null && callFor != '' && callFor == Label.CLMAY15ELLABFO05){  // Substitution
                    if(listWithdrawal[0].SubstitutionOPSJobId__c!=null) {
                        return AP_ELLA_TO_OPS_WebServcieWithdrawal.getOPSSubstitutionResult(string.valueof(listWithdrawal[0].SubstitutionOPSJobId__c),withdrawalId);
                    }else {
                        return AP_ELLA_TO_OPS_WebServcieWithdrawal.generateResp(Label.CLMAY15ELLABFO03, Label.CLJUNE15ELLABFO20); // Substitution not synced,You can not able to check the Job Status in OPS // Warning   
                    }
                } 
            }
        }
        return AP_ELLA_TO_OPS_WebServcieWithdrawal.generateResp(Label.CLJUNE15ELLABFO13, Label.CLJUNE15ELLABFO12);// Problem in communication. Please contact the administrator // Error
    }
}