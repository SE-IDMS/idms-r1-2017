/********************************************************************
* Company: Fielo
* Created Date: 09/07/2016
* Description:
********************************************************************/
@isTest
public class FieloPRM_MemberTriggerTest {
    
     public static testMethod void testUnit1(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            
            FieloEE.MockUpFactory.setCustomProperties(false);

            Country__c testCountryLocal = new Country__c(
                Name   = 'Local Country Test',
                CountryCode__c = 'US',
                InternationalPhoneCode__c = '01',
                Region__c = 'TESTL'
            );
            insert testCountryLocal;
                        
            PRMCountry__c prmCountryLocal = new PRMCountry__c(
                Name = 'Local Country Test',
                CountryPortalEnabled__c = true,
                Country__c = testCountryLocal.Id,
                PLDatapool__c = 'pagecontrol1',
                TECH_MigrationUniqueID__c = 'TestLocal'
            );
            insert prmCountryLocal;
                        
            Account acc = new Account(
                Name = 'acc',
                PRMIsActiveAccount__c = true
            );
            insert acc;
            
            Contact con = new Contact(
                AccountId = acc.Id,
                FirstName = 'test contact Fn',
                LastName  = 'test contact Ln',
                PRMCompanyInfoSkippedAtReg__c = false,
                PRMCountry__c = testCountryLocal.Id,
                PRMFirstName__c = 'PRM test contact Fn', 
                PRMLastName__c  = 'PRM test contact Fn',
                PRMUIMSId__c = '123'
            );
            insert con;
            
            List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)'];
            
            UserRole portalRole = [SELECT Id FROM UserRole WHERE PortalType = 'None' LIMIT 1];
            User u = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            u.ContactId = con.Id;
            u.PRMRegistrationCompleted__c = false;
            u.BypassWF__c = false;
            u.BypassVR__c = true;
            u.FirstName ='Test User First';
            u.ContactId = con.Id;
            insert u;

            RecordType manualSegment = [SELECT Id FROM RecordType WHERE sObjecttype = 'FieloEE__RedemptionRule__c' AND Name = 'Manual'];

            FieloEE__RedemptionRule__c segment = new FieloEE__RedemptionRule__c(
                RecordTypeId = manualSegment.Id,
                F_PRM_Type__c = 'Hybrid',
                FieloEE__isActive__c = true,
                F_PRM_Country__c = prmCountryLocal.Id
            );
            insert segment;

            FieloEE__RedemptionRuleCriteria__c segmentCriteria = new FieloEE__RedemptionRuleCriteria__c(
                FieloEE__RedemptionRule__c = segment.Id,
                FieloEE__FieldName__c = 'F_PRM_PrimaryChannel__c',          
                FieloEE__Operator__c = 'contains',
                FieloEE__Values__c = 'LC',
                FieloEE__FieldType__c = 'Text'
            );
            insert segmentCriteria;
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo',
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'test',
                F_Account__c = acc.Id,
                F_PRM_IsActivePRMContact__c = false,
                F_PRM_CountryCodeInjectQuery__c = 'US',
                F_PRM_Country__c = prmCountryLocal.Id,
                F_Country__c = testCountryLocal.Id,
                F_PRM_PrimaryChannel__c = 'LC1',
                FieloEE__User__c = u.Id              
            );         
            insert member;

            FieloEE__RedemptionRule__c segment1 = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            
            FieloEE__MemberSegment__c memberSegment = new FieloEE__MemberSegment__c(
                FieloEE__Member2__c = member.Id,
                FieloEE__Segment2__c = segment1.Id           
            );
            insert memberSegment;

            test.StartTest();

            member.F_PRM_PrimaryChannel__c = 'LC2';
            member.F_PRM_IsActivePRMContact__c = true;
            member.F_PRM_ContactRegistrationStatus__c = 'Pending';
            member.F_PRM_ProgramLevels__c = 'PROG1';
            update member;

            member.F_PRM_IsActivePRMContact__c = false;
            member.F_PRM_ContactRegistrationStatus__c = 'Validated';           
            update member;
            
            test.StopTest();

        }
    }

}