/*
    Author              : Gayathri Shivakumar(Schneider Electric)
    Date Created        :  26-Jul-2013
    Description         : Test Class for ScheduledIPOStrategicProgramUpdate 
*/

@isTest 
private class ScheduledIPOStrategicProgramUpdate_Test{
  static testMethod void classTest(){
    test.startTest();
    ScheduledIPOStrategicProgramUpdate  s = new ScheduledIPOStrategicProgramUpdate ();
    system.schedule('IPOStrategicProgramUpdates','0 0 0 7 7 ? 2022',s);
    test.stopTest();
  }
}