/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: VF Controller                                       *
* VisualForce: Fielo_RelatedListView                        *
* Created Date: 08.09.2014                                  *
* Description:                                              *
************************************************************/

public class Fielo_RelatedListController{
    
    //Variables PUBLICAS
    public Fielo_GeneralPager pager {get;set;}
    public List<string> listFields {get;set;}
    public Fielo_ListView__c currentLV;
    public Id fRecordId;
    public String nameRelatedLst{get;set;}
    //Variables PRIVADAS
    private boolean initialized = false;
    //Variables para usar desde el Where Condition
    private Id userId = UserInfo.getUserId();
    private Id memberId = FieloEE.memberUtil.getmemberId();
    
    public void setcurrentLV(Fielo_ListView__c LV) {
            currentLV = LV;
            constructor();
    }
    public Fielo_ListView__c getcurrentLV() {return currentLV;}
    
    public void setfRecordId(Id record) {
            fRecordId = record;
            constructor();
    }
    public Id getfRecordId() {return fRecordId;}
        
    //CONSTRUCTOR
    private void constructor(){
        if (initialized){
            return;
        }
        if (currentLV == null || fRecordId == null){
            return;
        }
        initialized = true;
        nameRelatedLst = Schema.getGlobalDescribe().get(currentLV.F_RelatedListObjectAPIName__c).getDescribe().getLabelPlural();
        generatePager();
    }
           
    private void generatePager(){
        //Cargo la lista de campos a mostrar
        listFields = currentLV.F_FieldSet__c.split(',');
        
        //Armo el query
        string query = 'SELECT Id, ';
        query += currentLV.F_FieldSet__c;
        query += ' FROM ' + currentLV.F_RelatedListObjectAPIName__c;
        query += currentLV.F_WhereCondition__c != null ? ' ' + currentLV.F_WhereCondition__c : '';
        
        //Genero el paginador
        try{
            pager = new Fielo_GeneralPager(Database.getQueryLocator(query), currentLV.F_PageSize__c != null ? Integer.ValueOf(currentLV.F_PageSize__c) : 10);
        }catch (Exception e){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }               
    }
    
    public PageReference doCreate(){
        PageReference pageRet = Fielo_Utilities.getMenuReferenceById(currentLV.F_RedirectNew__c);
        pageRet.getParameters().put('mode', 'c');
        pageRet.setRedirect(true);
        return pageRet;
    }
    
    public PageReference doRead(){
        string recordIdR = system.currentPageReference().getParameters().get('recordIdR');
        PageReference pageRet = Fielo_Utilities.getMenuReferenceById(currentLV.F_RedirectDetails__c);
        pageRet.getParameters().put('recordId', recordIdR);
        pageRet.getParameters().put('mode', 'r');
        System.debug('recordId: ' + recordIdR);
        pageRet.setRedirect(true);
        return pageRet;
    }
    
    public PageReference doUpdate(){
        string recordIdR = system.currentPageReference().getParameters().get('recordIdR');
        PageReference pageRet = Fielo_Utilities.getMenuReferenceById(currentLV.F_RedirectEdit__c);
        pageRet.getParameters().put('recordId', recordIdR);
        pageRet.getParameters().put('mode', 'u');
        pageRet.setRedirect(true);
        return pageRet;
    }

    public PageReference doDelete(){
        string recordIdR = system.currentPageReference().getParameters().get('recordIdR');
        PageReference pageRet = Fielo_Utilities.getMenuReferenceById(currentLV.F_RedirectDelete__c);
        pageRet.getParameters().put('recordId', recordIdR);
        pageRet.getParameters().put('mode', 'd');
        pageRet.setRedirect(true);
        return pageRet;
    }
       
}