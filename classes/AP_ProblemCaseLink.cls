// Created by Uttara - OCT 2015 Release - BR-7096


public class AP_ProblemCaseLink {

    // Added by Uttara - OCT 2015 Release - BR-7096
    public static void updateCaseAfterDelete(List<ProblemCaseLink__c> oldPCL) {
        
        Set<Id> caseIds = new Set<Id>();
        
        for(ProblemCaseLink__c pcl : oldPCL) {
            caseIds.add(pcl.Case__c);
        }    
        
        AP_UpdateMatchingCase.CaseWithOpenProblems(caseIds);
    }
    
    public static void updateCaseAfterInsert(List<ProblemCaseLink__c> newPCL) {
        
        Set<Id> caseIds = new Set<Id>();
        Set<Id> problemIds = new Set<Id>();
        List<ProblemCaseUnifiedLink__c> PCULExisting = new List<ProblemCaseUnifiedLink__c>();
        
        for(ProblemCaseLink__c pcl : newPCL) {
            caseIds.add(pcl.Case__c);
            problemIds.add(pcl.Problem__c);    
        }
        
        PCULExisting = [SELECT Case__c, Case__r.Status, Problem__c FROM ProblemCaseUnifiedLink__c WHERE Case__c IN: caseIds AND Problem__c IN: problemIds AND (Case__r.Status =: Label.CLOCT15I2P31 OR Case__r.Status =: Label.CLOCT15I2P33 OR Case__r.Status =: Label.CLOCT15I2P34)];
        Database.Delete(PCULExisting);
        
    }
}