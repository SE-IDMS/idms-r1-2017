/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: Test Class                                          *
* Created Date: 14.04.2014                                  *
* Tested Class: FieloPRM_UTILS_Methods                      *
************************************************************/

@isTest
public with sharing class FieloPRM_UTILS_MethodsTest{
  
    public static testMethod void testUnit(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            List<SObject> acclist = new List<SObject>();
            acclist.add(new Account(
                Name = 'Test2'
            ));
            acclist.add(new Account(
                Name = 'Ramiro'
            ));
            insert acclist;
            
            map<String, Sobject> mapa = FieloPRM_UTILS_Methods.generateMapByField('Id', accList);
            FieloPRM_UTILS_Methods.addError('Test Error');
            FieloPRM_UTILS_Methods.addWarn('Test Warning');
            FieloPRM_UTILS_Methods.addInfo('Test Info');
            FieloPRM_UTILS_Methods.debug('Test Debug');
            FieloPRM_UTILS_Methods.debugError('Test Debug Error');
        }
    }
}