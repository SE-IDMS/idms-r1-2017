/*     
@Author: Deepak
Created Date: 03-04-14
Description: Test class for Ap_Installed_Product
**********
*/
@isTest(seealldata=true)
Public Class Ap_Installed_Product_test
{

    static testMethod void proddeleteTest() 
    {
        Test.starttest();
     Brand__c brand1_1 = new Brand__c();
            brand1_1.Name ='Brand 1-1';
            brand1_1.SDHBRANDID__c = 'Test_B33randSDHID';
            brand1_1.IsSchneiderBrand__c = true;
            brand1_1.TECH_SDHQUERYAction__c ='Upsert';
            insert brand1_1 ;
             Brand__c brand1_2 = new Brand__c();
            brand1_2.Name ='Brand 1-1';
            brand1_2.SDHBRANDID__c = 'Test_B43ran';
            brand1_2.IsSchneiderBrand__c = true;
            brand1_2.TECH_SDHQUERYAction__c ='Upsert';
            insert brand1_2 ;
            
            DeviceType__c dt1_1 = new DeviceType__c();
            dt1_1.name = 'Device Type 1-1';
            dt1_1.SDHDEVICETYPEID__c = 'Test_Dev33iceTypeSDHID1-1';
            dt1_1.TECH_SDHQUERYAction__c ='Upsert';
            insert dt1_1 ;
            
            Category__c cat = new Category__c();
            cat.Name = 'text_cat';
            cat.CategoryNote__c = 'text';
            cat.CategoryType__c = 'RANGE';
            cat.TECH_SDHQueryAction__c ='Up33sert';
            insert cat;
           
          SVMXC__Installed_Product__c ipp = new SVMXC__Installed_Product__c(SVMXC__Date_Installed__c=system.today(),name='testip',ProductGroupID__c='test123',LifeCycleStatusOfTheInstalledProduct__c='IN_USE');
            ipp.Brand2__c=brand1_1.id;
            ipp.Category__c=cat.id;
            ipp.DeviceType2__c=dt1_1.id;       
            insert ipp; 
            ipp.Brand2__c = brand1_2.id;
            update ipp;
            Test.stoptest();
    
    
    }
    static testMethod void prodwarrTest() 
    {
        Test.starttest();
            SVMXC__Installed_Product__c ipp= new SVMXC__Installed_Product__c(SVMXC__Date_Installed__c=system.today(),name='testip',ProductGroupID__c='test123',LifeCycleStatusOfTheInstalledProduct__c='IN_USE');
        insert ipp;
        ipp.SVMXC__Date_Installed__c=system.today()+2;
        update ipp;

        SVMXC__Installed_Product__c ip= new SVMXC__Installed_Product__c(name='testip');
        insert ip;

        SVMXC__Warranty__c prodwarr= new SVMXC__Warranty__c(SVMXC__Installed_Product__c =ip.id);
        insert prodwarr;
        delete prodwarr;
        Test.stoptest();


    }

    static testMethod void Testip() 
    {
        Test.StartTest();
        
        account acc= new account(name='testacc');
        insert acc;
       
        SVMXC__Installed_Product__c ip= new SVMXC__Installed_Product__c(name='testip',ManageIBinFO__c=true);
        insert ip;
        JctAssetLink__c ipl= new JctAssetLink__c(LinkToAsset__c=ip.id);
        insert ipl;
        
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(name='testip',SVMXC__Site__c = null,SVMXC__Company__c=acc.id);
        insert ip2;
        Test.StopTest();
    }
    
   static testMethod void Test11() 
   {
   Test.starttest();
       account acc= new account(name='testacc');
       insert acc;
       account ac2= new account(name='testacc11');
       insert ac2;
       SVMXC__Site__c site= new SVMXC__Site__c(name= 'testsite',timeZone__c ='GMT');
       insert site;
       SVMXC__Site__c site2= new SVMXC__Site__c(name= 'testsite11',timeZone__c ='GMT');
       insert site2;
        Brand__c brand1 = new Brand__c(Name ='Brand 1-1',SDHBRANDID__c = 'Test_B33randSDHID',IsSchneiderBrand__c = true);
                insert brand1 ;
        Brand__c brand2 = new Brand__c(Name ='Brand 1');
                insert brand2 ;
       
       
       SVMXC__Installed_Product__c ip= new SVMXC__Installed_Product__c(name='testip',Remote_System__c='AOS', SVMXC__Company__c=acc.id,SVMXC__Site__c=site.id,Brand2__c=brand1.id);
       insert ip;
       ip.SVMXC__Company__c=ac2.id;
       ip.SVMXC__Site__c=site2.id;
       ip.Brand2__c=brand2.id;
       update ip;
       Test.stoptest();
       
   }
    
    
    static testMethod void Test() 
    {
         
        //User creation
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassVR__c=true,BypassTriggers__c = 'SVMX20;SVMX10;SVMX22;SVMX_InstalledProduct2;SVMX21;AP_LocationManager',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
        
        system.runAs(user){   
            Account testAccount = Utils_TestMethods.createAccount();
            insert testAccount;
            
            SVMXC__Site__c site1 = new SVMXC__Site__c();
            site1.Name = 'Test Location';
            site1.SVMXC__Street__c  = 'Test Street';
            site1.SVMXC__Account__c = testAccount.id;
            site1.PrimaryLocation__c = true;
            site1.timeZone__c ='GMT';
            insert site1;
            
            Country__c country= Utils_TestMethods.createCountry(); 
            country.CountryCode__c= 'hkh';   
            insert country; 
            
            Brand__c brand1_1 = new Brand__c();
            brand1_1.Name ='Brand 1-1';
            brand1_1.SDHBRANDID__c = 'Test_B33randSDHID';
            brand1_1.IsSchneiderBrand__c = true;
            brand1_1.TECH_SDHQUERYAction__c ='Upsert';
            insert brand1_1 ;
            
            DeviceType__c dt1_1 = new DeviceType__c();
            dt1_1.name = 'Device Type 1-1';
            dt1_1.SDHDEVICETYPEID__c = 'Test_Dev33iceTypeSDHID1-1';
            dt1_1.TECH_SDHQUERYAction__c ='Upsert';
            insert dt1_1 ;
            
            Category__c cat = new Category__c();
            cat.Name = 'text_cat';
            cat.CategoryNote__c = 'text';
            cat.CategoryType__c = 'RANGE';
            cat.TECH_SDHQueryAction__c ='Up33sert';
            insert cat;
            Category__c cat2 = new Category__c();
            cat2.Name = 'text_cat2';
            cat2.CategoryNote__c = 'text2';
            cat2.CategoryType__c = 'RANGE';
            cat2.TECH_SDHQueryAction__c ='Up33sert2';
            insert cat2;
            
            
            OPP_Product__c opProduct= New OPP_Product__c();
            opProduct.Name='opp_product';
            opProduct.BusinessUnit__c='BD';
            opProduct.ProductFamily__c='ITB21';
            opProduct.ProductLine__c='PWACB';
            insert opProduct;
            
            OPP_Product__c opProduct2= New OPP_Product__c();
            opProduct2.Name='opp_product';
            opProduct2.BusinessUnit__c='EN';
            opProduct2.ProductFamily__c='ITB212';
            opProduct2.ProductLine__c='PWACB1';
            insert opProduct2;
            
            Product2 prod = New Product2();
            prod.Name = 'Product_test';
            prod.Brand2__c=brand1_1.Id;
            prod.DeviceType2__c=dt1_1.Id;
            prod.ProductGDP__c=opProduct.Id;
            prod.CategoryId__c=cat.Id;
            insert prod;
            
            Product2 prod2 = New Product2();
            prod2.Name = 'Product_test';
            prod2.Brand2__c=brand1_1.Id;
            prod2.DeviceType2__c=dt1_1.Id;
            prod2.ProductGDP__c=opProduct.Id;
            prod2.CategoryId__c=cat2.Id;
            insert prod2;
            test.starttest();
            
            SVMXC__Service_Template__c wt = new SVMXC__Service_Template__c();
            wt.Name = 'test_WT';
            wt.SVMXC__Duration_of_Material_Coverage__c = 5;
            wt.SVMXC__Unit_of_Time_Material__c = 'Days';
            insert wt ;
            
            
            
            system.debug('value2:'+wt.SVMXC__Duration_of_Material_Coverage__c);
            system.debug('value3:'+wt.SVMXC__Unit_of_Time_Material__c);
            
            SVMXC__Service_Template_Products__c applicProd = new SVMXC__Service_Template_Products__c();
            applicProd.Product__c = prod.Id;
            applicProd.SVMXC__Service_Template__c = wt.Id; 
            //applicProd.Name = 'prod';
            insert applicProd;
            
            SVMXC__Service_Template__c wt2 = new SVMXC__Service_Template__c();
            wt2.Name = 'test_WT2';
            wt2.SVMXC__Duration_of_Material_Coverage__c = 6;
            wt2.SVMXC__Unit_of_Time_Material__c = 'Weeks';
            insert wt2 ;
            
            SVMXC__Service_Template_Products__c applicProd2 = new SVMXC__Service_Template_Products__c();
            applicProd2.Product__c = prod2.Id;
            applicProd2.SVMXC__Service_Template__c = wt2.Id; 
            //applicProd.Name = 'prod';
            insert applicProd2;
            set<Id> ipid = new set<Id>();
            
            SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(Name = 'Test Intalled Product2'
                                                                                ,SVMXC__Product__c= prod2.Id,
                                                                                Brand2__c=brand1_1.id,
                                                                                DeviceType2__c=dt1_1.id,
                                                                                Category__c=cat2.id,
                                                                                SVMXC__Status__c= 'new',                                                                              
                                                                                SVMXC__Serial_Lot_Number__c = '12343455',
                                                                            CommissioningDateInstallDate__c = date.today()+10
                                                                                );
            insert ip2;
            
            ipid.add(ip2.Id);
            //ipid.add(ip3.Id);
            
            
             SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
            ip1.SVMXC__Company__c = testAccount.id;
            ip1.Name = 'Test Intalled Product ';
            ip1.SVMXC__Status__c= 'new';
            ip1.GoldenAssetId__c = 'GoledenAssert33Id1';
            ip1.BrandToCreate__c ='Test Brand';
            ip1.SVMXC__Site__c = site1.id;
            ip1.DeviceTypeToCreate__c = 'device1';
            ip1.LifeCycleStatusOfTheInstalledProduct__c = system.label.CLAPR14SRV05;
            ip1.DecomissioningDate__c = date.today();
            ip1.AssetCategory2__c =system.label.CLAPR14SRV01;
            ip1.SVMXC__Serial_Lot_Number__c = '1234';
            ip1.Brand2__c = brand1_1.Id;
            ip1.Category__c =cat.Id;
            ip1.DeviceType2__c = dt1_1.Id;
            ip1.SVMXC__Product__c =prod.Id;
            ip1.CommissioningDateInstallDate__c = date.today()+5;
           // ip1.PurchasedDate__c= date.today()+2;
            //ip1.SVMXC__Date_Shipped__c= date.today();
            insert ip1;
            
            ipid.add(ip1.Id);
            
            
           
            
            //iplist.add(ip1);
            set<Id> ipid2 = new set<Id>();
            ipid2.add(ip1.Id);
            system.debug('ipl11:'+ip1); 
            system.debug('ipl11:'+ip1.WarrantyTriggerDate__c);
            
            SVMXC__Warranty__c prodWarr = new SVMXC__Warranty__c();
            prodWarr.SVMXC__Installed_Product__c = ip1.id;
            prodWarr.SVMXC__Service_Template__c = wt.Id;
            prodWarr.SVMXC__Start_Date__c =ip1.WarrantyTriggerDate__c;
            insert  prodWarr;
            system.debug('prodWarr:'+prodWarr);
            
            SVMXC__Warranty__c prodWarr2 = new SVMXC__Warranty__c();
            prodWarr2.SVMXC__Installed_Product__c = ip2.id;
            prodWarr2.SVMXC__Service_Template__c = wt2.Id;
            prodWarr2.SVMXC__Start_Date__c =ip2.WarrantyTriggerDate__c;
            insert  prodWarr2;
        
             List<SVMXC__Installed_Product__c> iplist = New List<SVMXC__Installed_Product__c>();
            iplist = [Select Id,SVMXC__Company__c,SVMXC__Status__c,WarrantyTriggerDate__c,SVMXC__Product__c
                        ,CommissioningDateInstallDate__c,DeviceType2__c,UnderContract__c,Category__c,Brand2__c,SVMXC__Serial_Lot_Number__c,
                        DecomissioningDate__c,LifeCycleStatusOfTheInstalledProduct__c,SVMXC__Site__c,BrandToCreate__c
                         ,AssetCategory2__c,DeviceTypeToCreate__c,GoldenAssetId__c,Name From SVMXC__Installed_Product__c Where Id in :ipid];
            
            
            
            SVMXC__Service_Contract__c testContract0 = new SVMXC__Service_Contract__c();
             testContract0.SVMXC__Company__c = testAccount.Id;
             insert testContract0;

            SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(testAccount.id);
            workOrder.SVMXC__Order_Status__c = 'UnScheduled';
            workOrder.CustomerRequestedDate__c = Date.today();
            workOrder.Service_Business_Unit__c = 'Energy';
            workOrder.SVMXC__Component__c = ip1.Id;
            insert workOrder;
            
            SVMXC__Service_Contract_Products__c covp = new SVMXC__Service_Contract_Products__c();
            covp.SVMXC__Installed_Product__c = ip1.Id;
            covp.SVMXC__Service_Contract__c=testContract0.Id;
            insert covp;

            Ap_Installed_Product.AssetCategory(iplist);
            update iplist;
            
            Ap_Installed_Product.ProductWarrantyCreation(iplist);
            
            Ap_Installed_Product.ScToUpdate(ipid);
            test.stoptest();
        }
    }
}