public class VFC_PublicGroupSearch extends VFC_ControllerBase {

    public String searchString {get;set;}
    public List<DataTemplate__c> searchResult {get;set;}
    public List<SelectOption> columns {get;set;}

    // controller of the paginated result component, retrieved through the VFC_ControllerBase methods
    public VCC06_DisplaySearchResults resultsController {
        set;
        get {
            if(getcomponentControllerMap()!=null) {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');                                  
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }

    public VFC_PublicGroupSearch() {

        generateColumnHeaders();
        
        searchString = ApexPages.currentPage().getParameters().get('searchString');
        
        if(searchString != null) {
            find();
        }       
    }
    
    public void generateColumnHeaders() {

        columns = new List<SelectOption>();
        columns.add(new SelectOption('Field1__c', 'Label'));
        columns.add(new SelectOption('Field2__c', 'Group Name'));
        
        searchResult = new List<DataTemplate__c>();    
    }    
    
    public void find() {
        
        String approximateSearchText = '%'+searchString+'%';
        searchResult = new List<DataTemplate__c>();
        
        for(Group publicGroup:[SELECT Id, DeveloperName, Name, Type, DoesIncludeBosses 
                               FROM GRoup WHERE Name LIKE :approximateSearchText 
                               AND Type = 'Regular']) {
                               
            searchResult.add(new DataTemplate__c(Field1__c = '<a href="/apex/VFP_GroupOverview?Id='
                                                              +publicGroup.Id +'&exportCSV=false&setupid=PublicGroups&searchstring='+searchString
                                                              +'">'+publicGroup.Name+'</a>',
                                                 Field2__c = publicGroup.DeveloperName,
                                                 Field3__c = publicGroup.Id));
        }
    }
    
    public override pageReference PerformAction(sObject obj, VFC_ControllerBase controllerBase) {
        
        PageReference groupOverviewPage;
        
        DataTemplate__c selectedGroup = (DataTemplate__c)obj;
        
        return new PageReference('/apex/VFP_GroupOverview?Id='+selectedGroup.Field3__c+'&exportCSV=false&setupid=PublicGroups');        
    }
}