// Description : Controller for the Field Service Bulletin creation page

public class VFC_NewFieldServiceBulletin {

    public String prbId {get; set;}
    public Problem__c Prb {get; set;} 
    public FieldServiceBulletin__c FSBeditPage {get; set;}

    public VFC_NewFieldServiceBulletin(ApexPages.StandardController controller) {
        
        prbId = ApexPages.currentPage().getParameters().get(Label.CLQ216I2P04);
        System.debug('!!!! prbId : ' + prbId);
    } 
    
    public PageReference goToEditPage() {
        
        System.debug('!!!! Enter goToEditPage');
        PageReference FSBeditPage = new PageReference('/'+SObjectType.FieldServiceBulletin__c.getKeyPrefix()+'/e?');
        FSBeditPage.getParameters().put('ent', Label.CLQ216I2P07); // setting the ent with FSB object Id
        FSBeditPage.getParameters().put(Label.CL00690, Label.CL00691); // setting nooverride = 1 
        
        if(prbId != null) {        
            System.debug('!!!! From Problem');
            
            Prb = [SELECT Name,Severity__c FROM Problem__c WHERE Id =: prbId];
            FSBeditPage.getParameters().put(Label.CLQ216I2P05,Prb.Name);  // setting the FSB Problem field
            FSBeditPage.getParameters().put(Label.CLQ216I2P04,prbId);  // setting the FSB Problem field                     
            FSBeditPage.getParameters().put(Label.CL00330, prbId); // setting the retURL
            FSBeditPage.getParameters().put('RecordType',Label.CLQ216I2P02);  // setting the FSB Problem record type            
            FSBeditPage.getParameters().put(Label.CLQ216I2P03,Prb.Severity__c);  // setting the FSB Severity field                
        }
        else {        
            System.debug('!!!! From Tab');
            
            FSBeditPage.getParameters().put('RecordType',Label.CLQ216I2P01);  // setting the FSB Product record type    
            FSBeditPage.getParameters().put(Label.CL00330,Label.CLQ216I2P06); // setting the retURL         
        }
        
        System.debug('!!!! FSBeditPage : '+ FSBeditPage);
        return FSBeditPage;
    }
}