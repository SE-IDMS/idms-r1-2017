@isTest
public class VFC_SRV_MYFsPersonalInformatinCtrl_Test{
 static testMethod void queryAccountsAndCountries() {
  test.starttest();
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
  ][0].Id;

  Account acc1 = new Account(Name = 'TestAccount', Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
  insert cnct;
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

  user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
   // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;
  system.runAs(u) {

   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;

     
   //VFC_SRV_MYFsPersonalInformatinController Test Coverage
   
   VFC_SRV_MYFsPersonalInformatinController PersonalInformation = new VFC_SRV_MYFsPersonalInformatinController();
   PersonalInformation.save();
   PersonalInformation.cancel();
   PersonalInformation.getUser();
   VFC_SRV_MYFsPersonalInformatinController.setContactFields(Cnct,u);
   Test.stoptest();
  }
 }



}