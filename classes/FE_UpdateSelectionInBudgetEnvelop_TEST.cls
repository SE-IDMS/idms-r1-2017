@isTest
private class FE_UpdateSelectionInBudgetEnvelop_TEST
{
    static testmethod void test_fe_updateselection()
    {
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Europe');
        insert testDMTA1; 
        
        PRJ_ProjectReq__c testProj = Utils_TestMethods.createPRJRequest();
        testProj.WorkType__c = 'Master Project';
        testProj.BusinessTechnicalDomain__c = 'Supply Chain';
        testProj.Parent__c = 'Buildings';
        //Modified By Ramakrishna For June Release
        testProj.ParentFamily__c = 'Business';   
        testproj.BusGeographicZoneIPO__c = 'Europe';     
        insert testProj;
        //Modified By Ramakrishna For June Release
        Budget__c Budget = new Budget__c(ProjectReq__c =testProj.id,Active__c=true);
        Insert Budget ;
        
        DMTFundingEntity__c testFE1 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj,'2012',false,'Business','Buildings','EMEAS','EMEAs','Q4','2011');
        insert testFE1;
        testFE1.SelectedinEnvelop__c = true;
                
        FE_UpdateSelectionInBudgetEnvelop vfCtrl = new FE_UpdateSelectionInBudgetEnvelop(new ApexPages.StandardController(testFE1));
      
        vfCtrl.updateFundingEntity();
        vfCtrl.backToFundingEntity(); 
        
        PRJ_ProjectReq__c testProj2 = Utils_TestMethods.createPRJRequest();
        testProj2.WorkType__c = 'Master Project';
        testProj2.BusinessTechnicalDomain__c = 'Supply Chain';
        testProj2.Parent__c = 'Buildings';
        //Modified By Ramakrishna For June Release
        testProj2.ParentFamily__c = 'Business';   
        insert testProj2;
        //Modified By Ramakrishna For June Release
        DMTFundingEntity__c testFE2 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj2,'2012',false,'Business','Power','EMEAS','EMEAs','Q4','2011');
        insert testFE2;
        testFE2.SelectedinEnvelop__c = true;
               
        vfCtrl = new FE_UpdateSelectionInBudgetEnvelop(new ApexPages.StandardController(testFE2));
      
        vfCtrl.updateFundingEntity();
        vfCtrl.backToFundingEntity();  
        
        PRJ_ProjectReq__c testProj3 = Utils_TestMethods.createPRJRequest();
        testProj3.WorkType__c = 'Master Project';
        testProj3.BusinessTechnicalDomain__c = 'Supply Chain';
        testProj3.Parent__c = 'Buildings';
        testProj3.ParentFamily__c = 'Business'; 
        testProj3.GeographicZoneSE__c = 'India'; 
        testProj3.BusGeographicZoneIPO__c = 'Europe';   
        insert testProj3;
        
        test.startTest();
        
        Budget__c Budget1 = new Budget__c();
        Budget1.ProjectReq__c = testProj3.id;
        Budget1.Active__c = true;
        insert Budget1 ;
        
        DMTFundingEntity__c testFE3 = new DMTFundingEntity__c(); 
        testFE3.ProjectReq__c = testProj3.id;
        testFE3.Parent__c = testProj3.Parent__c;
        testFE3.ParentFamily__c = testProj3.ParentFamily__c;
        testFE3.GeographicZoneSE__c = testProj3.GeographicZoneSE__c;
        testFE3.BudgetYear__c = '2012';
        testFE3.ForecastQuarter__c = 'Q1';
        testFE3.ForecastYear__c = '2011';
        testFE3.CAPEXY0__c = 100;
        
        insert testFE3;
        
        Profile pr = [select id from profile where name=:System.Label.DMTProfile1];
        user usr = new user(ProfileId = pr.Id);
        usr.Alias = 'Tst';
        usr.Email = 'Test3@schneider-electric.com';
        usr.Username = 'Test3@bridge-fo.com.devsep';
        //usr.ProfileId = pr.Id;   
        
        User runAsUser = Utils_TestMethods.createStandardDMTUser('tst3');
        //test.startTest();
        insert runAsUser;
         
        System.runAs(runAsUser){
            //testProj3.NextStep__c = 'Created';
            //testProj3.BusinessTechnicalDomain__c = 'Supply Chain';
            //update testProj3;
            
            DMTAuthorizationMasterData__c testDMTA3 = new DMTAuthorizationMasterData__c();
            testDMTA3.AuthorizedUser1__c = usr.id;
            testDMTA3.NextStep__c = 'Created';
            testDMTA3.Parent__c = testFE3.Parent__c;
            testDMTA3.BusGeographicZoneIPO__c = 'Europe';
            insert testDMTA3;
            
            FE_UpdateSelectionInBudgetEnvelop vfCtrl2 = new FE_UpdateSelectionInBudgetEnvelop(new ApexPages.StandardController(testFE3));
            testFE3.SelectedinEnvelop__c = true;
            vfCtrl2.updateFundingEntity();
            vfCtrl2.backToFundingEntity();

            System.debug('-------%%---'+testDMTA3.NextStep__c+testDMTA3.Parent__c+testDMTA3.BusGeographicZoneIPO__c);
            System.debug('-------%%---'+testProj3.NextStep__c+testProj3.Parent__c+testProj3.BusGeographicZoneIPO__c);
            
            Delete testDMTA3;
            vfCtrl2.updateFundingEntity();
            vfCtrl2.backToFundingEntity();
            
        }   //Modified By Ramakrishna For June Release End
        test.stopTest();    
                                    
    }
}