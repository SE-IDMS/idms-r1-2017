/**
 * An apex page controller that exposes the site login functionality 
 */
global virtual with sharing class VFC_SRV_MyFsLoginPage {
    
    public String emailAddress                                    {get;set;}
    public String startURL                                        {get;set;}
    
    global transient Boolean AuthenticationFailed                 {get;set;}
    global transient string ReferredFrom                          {get;set;}
    global transient AP_IMSSSOInitParams.SSOInitParams InitParams {get;set;}
    global transient String ErrorMsgCode                          {get;set;}
    global transient string tokenValue                            {get;set;}
    global transient string UwserLanguage                         {get;set;}
    global transient String Country                               {get;set;}

    global transient String referredFromUrl = ApexPages.currentPage().getHeaders().get('Referer');
    global transient String currentURL = Apexpages.currentPage().getUrl();
    global transient String gotoParam = ApexPages.currentPage().getParameters().get('goto');
    global transient String sunQueryStringParam = ApexPages.currentPage().getParameters().get('SunQueryParamsString');
    global transient String IDToken = ApexPages.currentPage().getParameters().get('IDToken1');
    global transient String languageURL = ApexPages.currentPage().getParameters().get('language');
    global transient String countryURL = ApexPages.currentPage().getParameters().get('country');
    global transient string loginParameters = '';
    global transient String countryValue = '';
    global transient String tUsderLanguage =''; 
    global static boolean debugMode = false;  
    
    global transient String newPassword      {get;set;}
    global transient String confirmPassword      {get;set;}
    
    global transient String encoded          {get;set;}
    global transient String errorMessage     {get;set;} 
    global transient String gotoValue        {get;set;}
    global transient String gotoOnFail       {get;set;}
    global transient String gx_charset       {get;set;}    
    global transient String IDButton         {get;set;} 
    global transient String SunQueryParamsString {get;set;} 

   global class ResetPasswordResult {
        global Boolean Success {get;set;}
        global String ErrorMsg {get;set;}
    }       
    
    global PageReference redirectToSSOPage() {
    
        PageReference redirectPage;
        
        if(UserInfo.getUserId().equals(Label.SRV_MyFs_GuestUserId)) {
        
            system.debug('User Id is My FS Guest User.');
        
            if(String.isEmpty(gotoValue) || String.isEmpty(sunQueryStringParam)){
                redirectPage = new PageReference(Label.SRV_MyFs_LoginURL);
                redirectPage.setRedirect(true);
            }        
        }
        else {
            system.debug('User Id is not My FS Guest User.');
            
            redirectPage = new PageReference(Label.SRV_MyFs_DefaultLandingPage);
            redirectPage.setRedirect(true);
        }
        
        return redirectPage;    
    }
    
    global VFC_SRV_MyFsLoginPage() {        
        startURL = ApexPages.currentPage().getParameters().get('startURL');

        for(String param:ApexPages.currentPage().getParameters().keySet()) {
            system.debug(param+'= '+ApexPages.currentPage().getParameters().get(param));
        }
        
        encoded          = ApexPages.currentPage().getParameters().get('encoded');
        errorMessage     = ApexPages.currentPage().getParameters().get('errorMessage');
        gotoValue        = ApexPages.currentPage().getParameters().get('goto');
    
        /* Deep Linking Mechanism based on Relay State
        if(gotoValue != null) {
        
            String gotoValueDecoded = EncodingUtil.base64Decode(gotoValue).toString();
            gotoValueDecoded += '&RelayState='+startURL;            
            Blob gotoValueDecodedBlob = Blob.valueof(gotoValueDecoded);
            
            gotoValue = EncodingUtil.base64encode(gotoValueDecodedBlob);
                    
        }
        */
                
        gotoOnFail       = ApexPages.currentPage().getParameters().get('gotoOnFail');
        gx_charset       = ApexPages.currentPage().getParameters().get('gx_charset');
        IDButton         = ApexPages.currentPage().getParameters().get('IDButton');        
        SunQueryParamsString = ApexPages.currentPage().getParameters().get('SunQueryParamsString');        
          
        // String referredFrom = 'https://ims-sqe.btsec.dev.schneider-electric.com/opensso/UI/Login?errorMessage=auth.failed&Login.Submit=Login&goto=http%3A%2F%2Fims-sqe.btsec.dev.schneider-electric.com%2Fopensso%2Fidpssoinit%3FmetaAlias%3D%252Fidp%26spEntityID%3Dhttps%3A%2F%2Fse--devAPR.cs22.my.salesforce.com&gx_charset=UTF-8&spEntityID=https%3A%2F%2Fse--devAPR.cs22.my.salesforce.com&errorMessage=auth.failed';
        Boolean isAuthFailed = false;
        String tmpIDToken;
        
        if(String.isNotBlank(referredFromUrl)) {
            
            String decodedReferredFrom = EncodingUtil.urlDecode(referredFromUrl, 'UTF-8');
            this.ReferredFrom = decodedReferredFrom;
            
            if (decodedReferredFrom.indexOf('?') > 0){
            
                String params = decodedReferredFrom.substringAfter('?');
                String[] aParams = params.split('&', 0);
                for (String s : aParams) {
                    if (s.startsWith('errorMessage=') == true) {
                        String[] aErrMsg = s.split('=');
                        if (aErrMsg != null && aErrMsg.size() > 1 && 
                                (aErrMsg[1] == 'auth.failed' || aErrMsg[1] == 'usernot.active' || aErrMsg[1] == 'userhasnoprofile.org')) {
                            isAuthFailed = true;
                            ErrorMsgCode = aErrMsg[1];
                            ApexPages.currentPage().getParameters().put('authfail', String.valueOf(isAuthFailed));
                        }
                    }               
                }
                
            }
        }
        
        this.AuthenticationFailed = isAuthFailed;
        System.debug('Authentication Failed: ' + isAuthFailed);
        System.debug('Token: ' + tokenValue);
        ApexPages.currentPage().getParameters().put('IDToken1', String.valueOf(tokenValue));        
        
    }
    
     global String getIsSSOParamSet() {
     
        if(String.isEmpty(gotoValue) || String.isEmpty(sunQueryStringParam)){
        
            System.debug('*** referredFromUrl->'+referredFromUrl);
            System.debug('***currentURL :' + currentURL );
            System.debug('*** Custom Base URL->'+site.getBaseCustomUrl());
                        
            String baseURL = String.isBlank(Site.getBaseCustomUrl()) ? Site.getBaseUrl() : Site.getBaseCustomUrl();
            
            System.debug('**** baseURL ->'+ baseURL);
            
            if(String.isNotBlank(IDToken)) {
                loginParameters += '&IDToken1='+IDToken;
            }    
                    
            system.debug('loginParameters:' + loginParameters);
            
            if(String.isNotBlank(referredFromUrl)){
                System.debug('***** referredFromUrl = ' + referredFromUrl);    
                if(String.isNotBlank(loginParameters)) {
                    return System.Label.SRV_MyFs_SSOInitURL +'&RelayState='+referredFromUrl+loginParameters;
                }
                else {
                    return System.Label.SRV_MyFs_SSOInitURL +'&RelayState='+referredFromUrl;
                }
            }          
            else {
            
                System.debug('***** else part of redirection');
                if(String.isNotBlank(loginParameters)) {
                    return System.Label.SRV_MyFs_SSOInitURL+loginParameters;
                }
                else {
                    return System.Label.SRV_MyFs_SSOInitURL;
                }
            }   
        }       
        else return '';
    }
    
    @RemoteAction
    global static ResetPasswordResult resetPasswordRemoting(String email) {
        
        ResetPasswordResult resetPasswordResult = new ResetPasswordResult();
        resetPasswordResult.Success = false;
        resetPasswordResult.ErrorMsg = email;
        
        if(email != null) {
        
            User usr;
            String federatedId='';
            
            try {
                List<User> lUsr = new List<User>([SELECT Id, FederationIdentifier, Email 
                                                  FROM USER WHERE Email = :email 
                                                  AND IsActive = true LIMIT 1]);                                                  
                if (!lUsr.isEmpty()) {
                    usr = lUsr[0];
                    federatedId = (usr != null ? usr.FederationIdentifier : '');
                }
                else {
                   resetPasswordResult.ErrorMsg = Label.SRV_MyFs_EmailDoesNotExist;
                   resetPasswordResult.Success = false;
                }
                
                if(String.isNotBlank(federatedId)) {
                    
                    uimsv2ServiceImsSchneiderComAUM.accessElement application = new uimsv2ServiceImsSchneiderComAUM.accessElement();
                    application.type_x = 'APPLICATION';
                    application.id = 'myfs';
                    
                    uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort uimsUserManagerService = new uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
                    uimsUserManagerService.endpoint_x = Label.SRV_MyFs_ResetPasswordEndpoint;
                    uimsUserManagerService.timeout_x  = 120000;
                    uimsUserManagerService.clientCertName_x = Label.SRV_MyFs_ClientCertName;
                    String resetStatus = uimsUserManagerService.resetPassword(Label.SRV_MyFs_CallerId, federatedId, application);
                    
                    System.debug('Password reset status:' + resetStatus);
                    
                    resetPasswordResult.Success = true;
                    resetPasswordResult.ErrorMsg = Label.SRV_MyFs_Success;
                    
                } 
                else {
                   resetPasswordResult.ErrorMsg = Label.SRV_MyFs_UnknownEmail;
                   resetPasswordResult.Success = false;
                }            
           } 
           catch (Exception e) {
               resetPasswordResult.ErrorMsg = 'Error:' + e.getMessage();
               resetPasswordResult.Success = false;
               
               System.debug('Error:' + e.getMessage() + e.getStackTraceString());
           }
       }        
          
       return resetPasswordResult;
    }
}