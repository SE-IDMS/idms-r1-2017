/*
* Test class of IdmsSendInvitationFuture class 
**/
@isTest
class IdmsSendInvitationFutureTest{
    
    //Test method: send email to invited user
    static testmethod void SendEmail(){
        
        IDMS_Send_Invitation__c IDMSSendInvitation= new IDMS_Send_Invitation__c(Email__c='idmstestoto1234@accenture.com', IDMS_Registration_Source__c='uims',IDMS_User_Context__c='home');
        insert IDMSSendInvitation;
        Test.startTest();
        IdmsSendInvitationFuture.SendEmail(IDMSSendInvitation.Id, IDMSSendInvitation.Email__c);
        Test.stopTest();
    }
}