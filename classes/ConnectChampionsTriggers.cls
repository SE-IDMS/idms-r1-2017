/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  23-April-2013
    Description         : Class for Connect Champions Sharing Permission
*/

public class ConnectChampionsTriggers
{

Public Static void ConnectChampionsAfterUpdate(List<Champions__c> CHold, List<Champions__c> CHnew)
      {
// Add permission to Connect Champions Leader
      
         List<Cascading_KPI_Target__Share>  CCMShare = new List<Cascading_KPI_Target__Share>();
                  
         List<Cascading_KPI_Target__c> CCM = new  List<Cascading_KPI_Target__c>();  
         
         if((CHnew[0].Smart_Cities_Division__c == null) && (CHnew[0].GSC_Regions__c == null) && (CHnew[0].Partner_Division__c == null))
        
            CCM = [Select id from Cascading_KPI_Target__c where Entity__c = :CHnew[0].Entity__c];
            
         else
         
            if(CHnew[0].Entity__c == Label.Connect_Smart_Cities && CHnew[0].Smart_Cities_Division__c != null)
        
              CCM = [Select id from Cascading_KPI_Target__c where Entity__c = :CHnew[0].Entity__c and Smart_Cities_Division__c = :CHnew[0].Smart_Cities_Division__c];
            
         else if(CHnew[0].Entity__c == Label.Connect_GSC && CHnew[0].GSC_Regions__c != null)
        
              CCM = [Select id from Cascading_KPI_Target__c where Entity__c = :CHnew[0].Entity__c and GSC_Region__c = :CHnew[0].GSC_Regions__c];
            
         else if(CHnew[0].Entity__c == Label.Connect_Partner && CHnew[0].Partner_Division__c != null)
        
              CCM = [Select id from Cascading_KPI_Target__c where Entity__c = :CHnew[0].Entity__c and Partner_Sub_Entity__c = :CHnew[0].Partner_Division__c];
            
         
         Set<id> CCMId = new Set<id>();
         
         for(Cascading_KPI_Target__c CCMile : CCM){
           CCMId.add(CCMile.id);
           Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           cshare.ParentId = CCMile.id;
           cshare.UserOrGroupId = CHnew[0].Champion_Name__c;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Entity_Champion__c;
           CCMShare.add(cshare);
          }
         Database.insert( CCMShare,false);

 if(CHold[0].Champion_Name__c != CHnew[0].Champion_Name__c){  
     
     For(Cascading_KPI_Target__Share  CCMSharedeleteCC :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId in :CCMId and UserOrGroupId = :CHold[0].Champion_Name__c and RowCause = :Schema.Cascading_KPI_Target__Share.RowCause.Entity_Champion__c]){
     system.debug('*----------*' + CCMSharedeleteCC.Parentid);
     if(CCMSharedeleteCC.Parentid != null)              
      Database.delete(CCMSharedeleteCC,false);
       }
      } 
    
     }     
          
}