/*
    Author          : Global Delivery Team - Bangalore
    Date Created    : 26/05/2011
    Description     : Test class for AP1004_PreventiveAction class.
*/
@isTest
private class AP1004_PreventiveAction_Test {

    static testMethod void testPreventiveAction() {
        // TO DO: implement unit test        
    User newUser1 = Utils_TestMethods.createStandardUser('preUser1');         
    Database.insert(newUser1);   
    
    User newUser2 = Utils_TestMethods.createStandardUser('preUser2');         
    Database.insert(newUser2);     
   
    BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
    breEntity.Name='Test Name125';
            breEntity.Entity__c='Test Entity125';
    Database.insert(breEntity);
          
    Problem__c Prblm = Utils_TestMethods.createProblem(breEntity.id); 
    Database.insert(Prblm);
     
    RootCause__c rootCause = Utils_TestMethods.createRootCause(prblm.Id);
    Database.insert(rootCause);
    rootCause.RootCauseType__c = 'Systemic Root Cause - Problem creation';
    rootCause.Status__c = 'Confirmed';
    Database.update(rootCause);
    
    PreventiveAction__c preAct = Utils_TestMethods.createPreventiveAction(rootCause.Id,breEntity.id,prblm.Id, newuser1.id);
    Database.insert(preAct);
    
    system.assertEquals(preAct.Owner__c,newuser1.id);       
    
    preAct.Owner__c = newUser2.id;
    Database.update(preAct);
    }    
}