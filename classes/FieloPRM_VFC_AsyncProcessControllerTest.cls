/********************************************************************
* Company: Fielo
* Developer: Elena J. Schwarzböck
* Created Date: 05/09/2016
* Description: 
********************************************************************/
@isTest
public without sharing class FieloPRM_VFC_AsyncProcessControllerTest { 
    public static testMethod void testUnit(){
    
        FieloCH__AsynchProcess__c record = new FieloCH__AsynchProcess__c();
        record.FieloCH__BadgeMemberTimeUnit__c = 1; 
        record.SetupOwnerId = userInfo.getUserId();  
        insert record;
    
        FieloPRM_VFC_AsyncProcessController controller = new FieloPRM_VFC_AsyncProcessController();
        controller.doEdit();
        controller.doSave();           
        controller.doNew();
        System.currentPageReference().getParameters().put('recordId',record.SetupOwnerId); 
        controller.doUpdate();
        controller.doDelete();
        controller.doCancel();
        controller.doCancelNew();
        controller.recordNew.SetupOwnerId = record.SetupOwnerId;
        controller.memberProxy.FieloEE__User__c = record.SetupOwnerId;
        controller.doSaveNew();
    }
}