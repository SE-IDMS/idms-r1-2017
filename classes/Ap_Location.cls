/*
@Author: Hari Krishna
Created Date: 10-09-2013
Description: This trigger fires on Location on 
**********
Scenarios:
**********
1.   check GoldenLocation unique October 2013 Release
2.   check primary Location Creation creation November 2013 Release

Date Modified       Modified By         Comments
------------------------------------------------------------------------
10-09-2013          Hari Krishna       Logic to check GoldenLocation unique
*/
public class Ap_Location extends TriggerBase{
    
    public static final string STANDARD_LOCATION = Label.CLNOV13SRV01;
    public static final string SE_READONLYSITE = Label.CLNOV13SRV02;
    private map<Id, SVMXC__Site__c> newMap;
    private map<Id, SVMXC__Site__c> oldMap;
    private List<SVMXC__Site__c> newRecList;
    Set<String> ExistGoldenIDSet = new Set<String>();
    private List<SVMXC__Site__c> primeryLocationforWSList  = new List<SVMXC__Site__c>(); 
    public Static string SITESUFFIX = Label.CLFEB14SRV01;  
    public Static string SPACESUFFIX = ' '; 
    private static final Integer SITE_NAME_LENGTH = 80;
    
    public Ap_Location(boolean isBefore, boolean isAfter, boolean isUpdate, boolean isInsert, boolean isDelete,
                       List<SVMXC__Site__c> newList,
                       map<Id, SVMXC__Site__c > oldMap, map<Id, SVMXC__Site__c> newMap) {
                           super(isBefore, isAfter, isUpdate, isInsert, isDelete);
                           
                           this.newMap = newMap;
                           this.oldMap = oldMap;
                           this.newRecList = newList;
                           
                       }
    
    public Ap_Location() {
    }
    /*
      // commented for June 2015 Hot Fix code coverage issue.
    public override void onBeforeInsert() {
        
        Set<String> GoldenAssetSet = new Set<String>();        
        
        for(SVMXC__Site__c Obj :newRecList){
            
            if(Obj.GoldenLocationId__c != null)
                GoldenAssetSet.add(Obj.GoldenLocationId__c);  
            //we have modified it as we faced a issue with connectors testing, please contact ramzi before reverting it back
            //if(obj.PrimaryLocation__c == False && obj.TECH_CreateFromWS_IP__c == false && obj.SVMXC__Parent__c == null && (obj.RecordTypeId == SE_READONLYSITE || obj.RecordTypeId == STANDARD_LOCATION) ){                
            if(obj.TECH_CreateFromWS_IP__c == false && Userinfo.getProfileId() == Label.CLNOV13SRV03  && obj.SVMXC__Parent__c == null && (obj.RecordTypeId == SE_READONLYSITE || obj.RecordTypeId == STANDARD_LOCATION) ){                
                primeryLocationforWSList.add(obj);              
            }        
        }
        if(GoldenAssetSet != null && GoldenAssetSet.size()>0)
        {
            for(SVMXC__Site__c obj:[Select id,GoldenLocationId__c from SVMXC__Site__c where GoldenLocationId__c in :GoldenAssetSet]){
                ExistGoldenIDSet.add(obj.GoldenLocationId__c);
            }
        }
        if(Utils_SDF_Methodology.canTrigger('SDHLOCATIONIDCHECK')|| Test.isRunningTest() )
            sdhGoldenUniqeCheck();
        
        if(Utils_SDF_Methodology.canTrigger('Ap_LocationPLforWS') || Test.isRunningTest()){
            if(primeryLocationforWSList != null && primeryLocationforWSList.size()>0 )
            {
                primaryLocationCreationForWS(primeryLocationforWSList);
            }
        }
        
        
        
    }*/
    public  void sdhGoldenUniqeCheck() {
        for(SVMXC__Site__c Obj :newRecList){
            
            if(Obj.GoldenLocationId__c != null)
            {
                if(ExistGoldenIDSet.contains(Obj.GoldenLocationId__c))
                    Obj.addError('GoldenLocation already Exist');
                
            }
        }
        
    }
    /*
     // commented for June 2015 Hot Fix code coverage issue.
    public override void onBeforeUpdate() {
        
        for(SVMXC__Site__c Obj :newRecList){                        
            
            //we have modified it as we faced a issue with connectors testing, please contact ramzi before reverting it back
            //if(obj.PrimaryLocation__c == False && obj.TECH_CreateFromWS_IP__c == false && obj.SVMXC__Parent__c == null && (obj.RecordTypeId == SE_READONLYSITE || obj.RecordTypeId == STANDARD_LOCATION)){                
            if(obj.TECH_CreateFromWS_IP__c == false && Userinfo.getProfileId() == Label.CLNOV13SRV03  && obj.SVMXC__Parent__c == null && (obj.RecordTypeId == SE_READONLYSITE || obj.RecordTypeId == STANDARD_LOCATION)){                
                primeryLocationforWSList.add(obj);              
            }         
        }
        if(Utils_SDF_Methodology.canTrigger('SVR') || Test.isRunningTest()){
            if(primeryLocationforWSList != null && primeryLocationforWSList.size()>0 )
            {
                primaryLocationCreationForWS(primeryLocationforWSList);
            }
        }
        
        
    }
    
    public void primaryLocationCreationForWS(List<SVMXC__Site__c> SitesList){
        
        System.debug('**********************' +' Primary Location need to Create ');
        Set<id> accountIdSet = new Set<id>();
        Map<id,Id> accidsiteidMap = new Map<id,Id>();
        List<SVMXC__Site__c> PrimaryLocationNeedtoCreate = new List<SVMXC__Site__c>();
        Set<id> accneedprimarylocIdSet = new Set<id>();
        
        
        
        if(primeryLocationforWSList!= null && primeryLocationforWSList.size()>0)  
        {
            for(SVMXC__Site__c obj:primeryLocationforWSList){
                
                if(obj.SVMXC__Account__c != null)
                {
                    accountIdSet.add(obj.SVMXC__Account__c);
                }
            }
            System.debug('**********************' +accountIdSet);
            if(accountIdSet!= null && accountIdSet.size()>0){
                for(SVMXC__Site__c obj:[select id,SVMXC__Account__c,PrimaryLocation__c from SVMXC__Site__c where  SVMXC__Account__c in :accountIdSet and PrimaryLocation__c = true])
                {
                    if(obj.PrimaryLocation__c)
                        accidsiteidMap.put(obj.SVMXC__Account__c,obj.id);
                }
                System.debug('**********************' +accidsiteidMap);
                
            }
            for(SVMXC__Site__c obj:primeryLocationforWSList){
                
                if(obj.SVMXC__Account__c != null)
                {
                    if(accidsiteidMap.containsKey(obj.SVMXC__Account__c))
                        obj.SVMXC__Parent__c = accidsiteidMap.get(obj.SVMXC__Account__c);
                    else{
                        
                        accneedprimarylocIdSet.add(obj.SVMXC__Account__c);
                        PrimaryLocationNeedtoCreate.add(obj);//obj.PrimaryLocation__c = true;  
                        System.debug('**********************' +accneedprimarylocIdSet); 
                        System.debug('**********************' +PrimaryLocationNeedtoCreate);                    
                        
                    }
                }
            }
            if(PrimaryLocationNeedtoCreate != null && PrimaryLocationNeedtoCreate.size()>0)
            {
                
                List<CS_AccountToLocation__c> acctoloc = CS_AccountToLocation__c.getall().values();
                
                String SQLHeader ='';
                String SQLLine ='';
                
                for(CS_AccountToLocation__c obj: acctoloc)
                {
                    if(obj.SourceObjField__c != null )
                    {
                        if(SQLHeader !='')
                            SQLHeader +=',';
                        SQLHeader +=obj.SourceObjField__c;                  
                    }
                    System.debug('\n SQLHeader: '+SQLHeader);
                    
                    
                }
                if(accneedprimarylocIdSet != null && accneedprimarylocIdSet.size()>0){
                    
                    List<SVMXC__Site__c> siteList = new List<SVMXC__Site__c>();
                    
                    String SqlQuery='Select id, Name , '+SQLHeader+' from Account where id in :accneedprimarylocIdSet';
                    
                    System.debug('\n query: '+SqlQuery);
                    
                    List<SObject> sobjects =Database.Query(SqlQuery);
                    if(sobjects!= null && sobjects.size()>0){
                        List<Account> accList = (List<Account>)sobjects;
                        for(Account acc : accList){
                            
                            // create location
                            SObject sObj = (SObject)acc;
                            SVMXC__Site__c site = new SVMXC__Site__c();
                            site.PrimaryLocation__c =  true;
                            site.TECH_CreateFromWS_IP__c = true;
                            site.SVMXC__Account__c = acc.id;
                            SObject tObj = (SObject)site;
                            
                            // DEF-7527
                            // NZE: changed the logic to avoid errors when the Account name is too long
                            //tObj.put('Name',sobj.get('Name')+SPACESUFFIX+SITESUFFIX); // old code not taking into account the site name limit
                            System.debug('#### Computing site name');
                            tObj.put('Name',computeSiteName((String)sobj.get('Name')));
                            //END DEF-7527
                            
                            tObj.put('RecordTypeId',Label.CLOCT13SRV36);
                            tObj.put('SVMXC__Location_Type__c', 'Site');
                            
                            for(CS_AccountToLocation__c obj: acctoloc)
                            {
                                if(obj.SourceObjField__c != null  && obj.TargetObjField__c != null )
                                {                               
                                    tObj.put(obj.TargetObjField__c,sobj.get(obj.SourceObjField__c));
                                }
                                
                            }                       
                            siteList.add((SVMXC__Site__c)tObj);
                            //System.debug('\n CLog : '+aidsiteIdMap);
                            
                        }
                    }
                    
                    if(siteList != null && siteList.size()>0){
                        Database.SaveResult[] sresults  =  Database.insert(siteList, false);
                        for(Integer k=0;k<sresults.size();k++ )
                        {
                            Database.SaveResult sr =sresults[k];
                            if(sr.isSuccess())
                            {
                                System.debug('#### sr: '+sr);
                                accidsiteidMap.put(siteList[k].SVMXC__Account__c, sr.getId());
                            }
                            else{
                                System.debug('********************** error in creation of site'+sr.getErrors() );  
                                //This need to handeled 
                            }
                        }
                    }
                    System.debug('**********************' +accidsiteidMap);  
                    for(SVMXC__Site__c obj:PrimaryLocationNeedtoCreate){
                        
                        if(obj.SVMXC__Account__c != null)
                        {
                            obj.PrimaryLocation__c = false;
                            if(accidsiteidMap.containsKey(obj.SVMXC__Account__c))
                                if(!obj.TECH_CreateFromWS_IP__c )
                                obj.SVMXC__Parent__c = accidsiteidMap.get(obj.SVMXC__Account__c);
                        }
                        
                    }
                    
                    
                    
                }   
                
            }
            
        }
    } */
    public static List<SVMXC__Site__c> createLocation(Set<id> accidSet){
        System.debug('####' +accidSet);
        List<SVMXC__Site__c> siteList = new List<SVMXC__Site__c>();
        Set<id> aidSet = new Set<id>();
        aidSet = accidSet;
        
        if(accidSet != null && accidSet.size()>0){
            
            List<CS_AccountToLocation__c> acctoloc = CS_AccountToLocation__c.getall().values();                
            String SQLHeader ='';
            String SQLLine ='';
            
            for(CS_AccountToLocation__c obj: acctoloc)
            {
                if(obj.SourceObjField__c != null )
                {
                    if(SQLHeader !='')
                        SQLHeader +=',';
                    SQLHeader +=obj.SourceObjField__c;                  
                }
                System.debug('\n SQLHeader: '+SQLHeader);                       
                
            }           
            
            String SqlQuery='Select id, Name , '+SQLHeader+' from Account where id in :aidSet';
            System.debug('\n finalQuery: '+SqlQuery);
            List<SObject> sobjects =Database.Query(SqlQuery);
            
            if(sobjects!= null && sobjects.size()>0){
                List<Account> accList = (List<Account>)sobjects;
                for(Account acc : accList){                
                    // create location
                    SObject sObj = (SObject)acc;
                    SVMXC__Site__c site = new SVMXC__Site__c();
                    site.PrimaryLocation__c =  true;
                    site.TECH_CreateFromWS_IP__c = true;
                    site.SVMXC__Account__c = acc.id;
                    SObject tObj = (SObject)site;
                    
                    // DEF-7527
                    // NZE: changed the logic to avoid errors when the Account name is too long // old code not taking into account the site name limit
                    //tObj.put('Name',sobj.get('Name')+SPACESUFFIX+SITESUFFIX);
                    System.debug('#### Computing site name');
                    tObj.put('Name',computeSiteName((String)sobj.get('Name')));
                    // END DEF-7527
                    
                    tObj.put('RecordTypeId',Label.CLOCT13SRV36);
                    tObj.put('SVMXC__Location_Type__c', 'Site');
                    
                    for(CS_AccountToLocation__c obj: acctoloc)
                    {
                        if(obj.SourceObjField__c != null  && obj.TargetObjField__c != null )
                        {                               
                            tObj.put(obj.TargetObjField__c,sobj.get(obj.SourceObjField__c));
                        }
                        
                    }                       
                    siteList.add((SVMXC__Site__c)tObj);
                    //System.debug('\n CLog : '+aidsiteIdMap);
                    
                }
            }
            
        }
        
        return siteList;
        
    }
    
    public static String computeSiteName(String anAccountName) {
        System.debug('#### START computeSiteName:'+anAccountName);
        String result = null;
        Integer MaxLen = SITE_NAME_LENGTH - SPACESUFFIX.length()- SITESUFFIX.length();
        if (anAccountName != null) {
            if(anAccountName.length() >= MaxLen)
                   result = anAccountName.substring(0, SITE_NAME_LENGTH - SPACESUFFIX.length()- SITESUFFIX.length()) + SPACESUFFIX + SITESUFFIX;
            else{
                result  = anAccountName+ SPACESUFFIX + SITESUFFIX;
             }
        }
        System.debug('#### END computeSiteName:'+result);
        return result;
    }
   
}