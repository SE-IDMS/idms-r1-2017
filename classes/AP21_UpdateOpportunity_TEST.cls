/*
    Author          : Accenture Team
    Date Created    : 01/08/2011
    Description     : Test class for AP21_UpdateOpportunity class.
*/
@isTest
private class AP21_UpdateOpportunity_TEST 
{
    private static Account accounts;
    private static Opportunity opportunity;
    private static Opportunity opportunity1;
    private static OPP_QuoteLink__c newQuoteLink;
    private static Date myDate3 = date.newInstance(2011, 8, 19); 
    
    static testMethod void updtQteIsuDateToCustTestMethod() 
    {
        
        accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts); 
        
        opportunity= Utils_TestMethods.createOpportunity(accounts.id);
        opportunity.OpptyPriorityLevel__c='standard';
        opportunity.OpptyType__c = 'Standard'; 
        insert opportunity;
        Date myDate1;
        List<OPP_QuoteLink__c> quoteLinks = New List<OPP_QuoteLink__c>();
        for(integer i=1;i<=29;i++)
        {
            myDate1 = date.newInstance(2011, 9, i);
            OPP_QuoteLink__c newQuoteLinks=Utils_TestMethods.createQuoteLink(opportunity.id);
            newQuoteLinks.IssuedQuoteDateToCustomer__c =myDate1 ;
            newQuoteLinks.Margin__c = 10;
            newQuoteLinks.ActiveQuote__c=true; 
            quoteLinks.add(newQuoteLinks);
        }
        test.startTest();
        Database.insert(quoteLinks);
        
        List<OPP_QuoteLink__c>  lsq = new List<OPP_QuoteLink__c>();
        newQuoteLink=Utils_TestMethods.createQuoteLink(opportunity.id);
        newQuoteLink.IssuedQuoteDateToCustomer__c = date.newInstance(2011, 8, 19) ;
        newQuoteLink.ActiveQuote__c=true;
        newQuoteLink.Margin__c = 10;  
        //insert newQuoteLink;
        lsq.add(newQuoteLink);
        /*
        OPP_QuoteLink__c newQuoteLink1=Utils_TestMethods.createQuoteLink(opportunity.id);
        newQuoteLink1.IssuedQuoteDateToCustomer__c = myDate3 ;
        newQuoteLink1.ActiveQuote__c=true; 
        newQuoteLink1.Margin__c = 10;  
        //insert newQuoteLink1;
        lsq.add(newQuoteLink);
        */
        insert lsq;
        
        delete newQuoteLink;
        
        
           
        test.stopTest();
   }
   
   static testMethod void updtQteIsuDateToCustTestMethod1() 
    {

        accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts); 
        opportunity1= Utils_TestMethods.createOpportunity(accounts.id);
        opportunity1.OpptyPriorityLevel__c='standard';
        opportunity1.OpptyType__c = 'Solutions'; 
        insert opportunity1;                      
        test.startTest();
        OPP_QuoteLink__c newQuoteLink2=Utils_TestMethods.createQuoteLink(opportunity1.id);
        newQuoteLink2.IssuedQuoteDateToCustomer__c = myDate3 ;
        newQuoteLink2.ActiveQuote__c=true; 
        newQuoteLink2.Margin__c = 10;
        insert newQuoteLink2;
        
        newQuoteLink2.IssuedQuoteDateToCustomer__c=NULL;
        update newQuoteLink2;
        test.stopTest();
    }
    
    static testMethod void updateSolutionCenterInOpportunityTestMethod() 
    {
        //*********************************************************************************
        // Method Name      : updateSolutionCenterInOpportunity
        // Purpose          : On Creation on Supports Request it will update Opportunity Field SolutionCenter when it was empty.
        // Created by       : Hari Krishna - Global Delivery Team
        // Date created     : 
        // Modified by      :
        // Date Modified    :
        // Remarks          : For April Sales - 12 Release
        ///********************************************************************************/
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
              
        Opportunity oppty = Utils_TestMethods.createOpenOpportunity(account1.id);
        oppty.OwnerId = UserInfo.getUserId();      
        insert oppty;  
            
        SolutionCenter__c solcenter= Utils_TestMethods.createSolutionCenter();
        solcenter.Name = 'Test for VFP79_SearchSolutionCenter';
        solcenter.BusinessesServed__c ='BD';
        insert  solcenter;
        test.startTest();
        OPP_SupportRequest__c suppReq=Utils_TestMethods.createSupportRequest(oppty.id);
        suppReq.SolutionCenter__c = solcenter.id;
        insert suppReq;
        oppty=[select SolutionCtr__c from Opportunity where id =:oppty.id];        
         System.assertEquals(suppReq.SolutionCenter__c, oppty.SolutionCtr__c);
         test.stopTest();
        
    }
    static testMethod void updateOpportunityTECHActiveWthApprovedTestMethod() 
    {
        //*********************************************************************************
        // Method Name      : updateSolutionCenterInOpportunity
        // Purpose          : On Update on QuoteLink with Quote Status = approved it will update Opportunity Field TECH_HasActiveQLKwthApproved__c True.
        // Created by       : Hari Krishna - Global Delivery Team
        // Date created     : 
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Dec Sales - 12 Release
        ///********************************************************************************/

        Account account1 = Utils_TestMethods.createAccount();        
        insert account1;
              
        Opportunity oppty = Utils_TestMethods.createOpenOpportunity(account1.id);
        oppty.OwnerId = UserInfo.getUserId();      
        insert oppty;  
        test.startTest();
        OPP_QuoteLink__c oppQuoteLink = Utils_TestMethods.createQuoteLink(oppty.id);
        insert oppQuoteLink ;
        oppQuoteLink.QuoteStatus__c ='Approved';
        update oppQuoteLink ;
            test.stopTest();
     
        
    }
}