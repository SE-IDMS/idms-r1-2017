/*
Class:AP_SPALineItemCalculations
Author:Siddharth N(GD Solutions)
*/
public with sharing class AP_SPALineItemCalculations
{

public static Map<Id,List<SPARequestLineItem__c>> RequestLineitemMap=null;
    public static void checkForThreshold(List<SPARequestLineItem__c> lineitems)
    {
        populateRequestLineitemMap(lineitems);    
        Map<String,List<SPARequestLineItem__c>> concatStringMap=new Map<String,List<SPARequestLineItem__c>>();
        List<String> backofficelst=new List<String>();
        List<String> lpglist=new List<String>();       
        
        for(SPARequest__c request:[select BackOfficeSystemID__c,id from SPARequest__c where id in :RequestLineitemMap.keySet()])        
        {
            if(RequestLineitemMap.containsKey(request.id))
            {
                for(SPARequestLineItem__c lineitem:RequestLineitemMap.get(request.id))
                {
                  String keyString='';
                   /*For BR-4968 added new Type Compensation% in Threshold Discount Table which will be used only for Advised Discount 
                     and hence below code is modified accordingly*/
                    if(lineitem.RequestedType__C!=System.Label.CLOCT14SLS13)
                       keyString=request.BackOfficeSystemID__c+':'+lineitem.LocalPricingGroup__c+':'+System.Label.CLOCT14SLS12;
                    else
                       keyString=request.BackOfficeSystemID__c+':'+lineitem.LocalPricingGroup__c+':'+System.Label.CLOCT14SLS14; 
                       
                    if(concatStringMap.containsKey(keyString))                    
                        concatStringMap.get(keyString).add(lineitem);                    
                    else
                    {
                        List<SPARequestLineItem__c> templist=new List<SPARequestLineItem__c>();
                        templist.add(lineitem);
                        concatStringMap.put(keyString,templist);                    
                    }
                    
                    
                    backofficelst.add(request.BackOfficeSystemID__c);                    
                    lpglist.add(lineitem.LocalPricingGroup__c);                    
                }
            }               
        }
        
        //get all the discount records relevant to this
        Set<String> validvalues=new Set<String>();
        for(SPAThresholdDiscount__c discount:[select id,BackOfficeSystemID__c,LocalPricingGroup__c,Type__c from SPAThresholdDiscount__c where BackOfficeSystemID__c in :backofficelst and LocalPricingGroup__c in :lpglist]) {     
            if(discount.Type__c!=System.Label.CLOCT14SLS14)
                validvalues.add(discount.BackOfficeSystemID__c+':'+discount.LocalPricingGroup__c+':'+System.Label.CLOCT14SLS12);
             else
                validvalues.add(discount.BackOfficeSystemID__c+':'+discount.LocalPricingGroup__c+':'+System.Label.CLOCT14SLS14); 
         }   

        for(String keystring:concatStringMap.keySet())
        {
            if(!validvalues.contains(keyString))
            {
                for(SPARequestLineItem__c lineitem:concatStringMap.get(keyString))
                lineitem.addError(System.Label.CLSEP12SLS20);
            }
        }

    }
        
    
    private static void populateRequestLineitemMap(List<SPARequestLineItem__c> lineitems)
    {
        if(RequestLineitemMap==null)
        {
            RequestLineitemMap=new Map<Id,List<SPARequestLineItem__c>>();                       
            for(SPARequestLineItem__c lineitem:lineitems)            
            {
                if(RequestLineitemMap.containsKey(lineitem.SPARequest__c))          
                    RequestLineitemMap.get(lineitem.SPARequest__c).add(lineitem);           
                else
                    {
                        List<SPARequestLineItem__c> templist=new List<SPARequestLineItem__c>();
                        templist.add(lineitem);
                        RequestLineitemMap.put(lineitem.SPARequest__c,templist);            
                    }                
            }
        }
    }   
    
}