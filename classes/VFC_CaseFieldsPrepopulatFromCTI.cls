public  class VFC_CaseFieldsPrepopulatFromCTI  {
//*********************************************************************************
// Class Name       : VFC_CaseFieldsPrepopulatFromCTI
// Purpose          : Case creation page with pre-populated information: Account, Contact, Category, Reason, CC Team, ANI
// Created by       : Hari Krishna - Global Delivery Team
// Date created     : 
// Modified by      :
// Date Modified    :
// Remarks          : For May Global CCC - 13 Release , BR-2197 
///********************************************************************************/

    
    public  Map<String,String> pageParameters = new Map<String,String>();
    public Boolean isRedirect=false;
    
    public String AccountId = Label.CLMAY13CCC11;
    public String AccountName = Label.CLMAY13CCC12;
    public String ContactId = Label.CLMAY13CCC13;
    public String ContactName = Label.CLMAY13CCC14;
    public String CaseTeamName = Label.CLMAY13CCC15;
    public String CaseTeamID = Label.CLMAY13CCC16;
    //public String LevelOfSupp = Label.CLMAY13CCC17;
    public String CallbackPhone = Label.CLMAY13CCC18;
    Public String CountryCodeId   = Label.CLMAY13CCC19;
    Public String CountryCodeName   = Label.CLMAY13CCC20;
    
    public VFC_CaseFieldsPrepopulatFromCTI(ApexPages.StandardController controller) {
        
        // current page parameters
        pageParameters = ApexPages.currentPage().getParameters();

    }
    Public PageReference init(){
        String SOPrefix = SObjectType.Case.getKeyPrefix(); 
        String url= '/'+SOPrefix+'/e';
        PageReference newCasePage = new PageReference(url);
        String QueryString = ' SELECT CTI_Skill__c,Name, id,LevelofSupport__c, (SELECT CaseReason__c,CaseSubReason__c,CaseCategory__c FROM TeamCaseJunction__r where  Default__c = True limit 1) FROM CustomerCareTeam__c ';
        
        String WhereClause = '';
        Boolean IsCountryCodeCorrect = false;
        
        if(pageParameters.containskey('ContactId')&& pageParameters.get('ContactId') !=''){
            /*
             if(pageParameters.containskey('ANI')){
             
                 //newCasePage.getParameters().put(CallbackPhone,pageParameters.get('ANI'));
                 String temp=ApexPages.currentPage().getUrl();
                 String s1 =  temp.substringAfterLast('ANI=');
                 String strAni = s1.substringBefore('&');                 
                 newCasePage.getParameters().put(CallbackPhone,strAni);
             }
            */ 
             
           
           try{
           
           
                if(pageParameters.containskey('CountryCode'))
                 {
                    try{
                        Country__c  contry = [SELECT CountryCode__c,Id,Name FROM Country__c where CountryCode__c =:pageParameters.get('CountryCode')];
                        
                        if(contry!= null)
                        {
                            IsCountryCodeCorrect = true;
                            String cId =contry.id;
                            newCasePage.getParameters().put(CountryCodeName,contry.Name);   
                            newCasePage.getParameters().put(CountryCodeId,contry.id);
                            whereClause =' CCCountry__c = :cId ';
                        }
                    }
                    catch(Exception ex){
                        
                        System.debug('Exception '+ex.getMessage());
                        
                    }
                 }
             
             
                 String conId = pageParameters.get('ContactId');
           
                 try{ 
                      Contact con=[select id,AccountId,Account.Name,Name from Contact where id =:conId];
                       if(con != null){
                            isRedirect = true;               
                             newCasePage.getParameters().put(AccountId,con.AccountId);
                             newCasePage.getParameters().put(AccountName, con.Account.Name );
                             newCasePage.getParameters().put(ContactId, con.id );
                             newCasePage.getParameters().put(ContactName, con.Name );                             
                       }
                   }
                   catch(Exception ex){
                        System.debug('Exception '+ex.getMessage());
                   }
                    
                   if(pageParameters.containskey('CtiSkill') && pageParameters.get('CtiSkill') !='' ){
                         
                        String CtiSkill = pageParameters.get('CtiSkill');
                        String QSt ;
                        if(WhereClause.length()>0 && IsCountryCodeCorrect)
                         QSt = QueryString +' where '+ WhereClause + ' AND CTI_Skill__c = :CtiSkill '+' limit 1 ';
                        else{QSt = QueryString + ' where '+' CTI_Skill__c = :CtiSkill '+' limit 1 ';}
                        
                        CustomerCareTeam__c cct  = new CustomerCareTeam__c();
                        try{
                              cct = Database.query(QSt);
                           }
                         Catch(Exception e){
                            System.debug('***************Exception'+    e);
                         }
                        
                    
                     if(cct != null){
                        
                        newCasePage.getParameters().put(CaseTeamName, cct.Name );
                        newCasePage.getParameters().put(CaseTeamID, cct.id );
                        //if(cct.LevelofSupport__c!= null)
                        //newCasePage.getParameters().put(LevelOfSupp, cct.LevelofSupport__c );
                        if(cct.TeamCaseJunction__r != null)
                        {
                            for(TeamCaseJunction__c tcj: cct.TeamCaseJunction__r){
                                   
                                    if(tcj.CaseCategory__c != null)
                                    {     
                                        
                                        newCasePage.getParameters().put(Label.CL00738, tcj.CaseCategory__c );
                                    }
                                    if(tcj.CaseReason__c != null)
                                    {                               
                                        newCasePage.getParameters().put(Label.CL00739, tcj.CaseReason__c );
                                    }
                                    if(tcj.CaseSubReason__c != null)
                                    {                                
                                        newCasePage.getParameters().put(Label.CL00740, tcj.CaseSubReason__c );
                                    }           
                                    
                                    
                                    
                            }
                        }
                     }
                     
                    
                   }
           }
           catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
           }                     
         
            
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CLMAY13CCC21));
        }
        if(isRedirect)
        {
            newCasePage.getParameters().put('nooverride', '1');
            return newCasePage ;
        }
        else{return null;}
    }
    
}