public class SVMXC_TimeEntryRequestBefore {

    public static void setAllDayDates (List<SVMXC_Time_Entry_Request__c> triggerNew, Map<Id, SVMXC_Time_Entry_Request__c> triggerOld, Boolean isInsert, Boolean isUpdate) {
        /*
            Method to check for All Day Event Time Entry Requests and set the All Day Event Days needed to normalize the date to mitigate potential
            timezone differences (Written Feb 19 2016)
            
        */
        
        for (SVMXC_Time_Entry_Request__c ter : triggerNew) {
        
            if (ter.All_Day_Event__c) {
            
                if (isInsert) {
                    if (ter.Start_Date__c != null)
                        ter.Start_Date_for_All_Day_Event__c = ter.Start_Date__c.date();
                    if (ter.End_Date__c != null)
                        ter.End_Date_for_All_Day_Event__c = ter.End_Date__c .date();
                } else if (isUpdate) {
                    if (ter.Start_Date__c != null && ter.Start_Date__c != triggerOld.get(ter.Id).Start_Date__c && ter.CreatedById == UserInfo.getUserId())
                        ter.Start_Date_for_All_Day_Event__c = ter.Start_Date__c.date();
                     if (ter.End_Date__c != null && ter.End_Date__c != triggerOld.get(ter.Id).End_Date__c && ter.CreatedById == UserInfo.getUserId())
                        ter.End_Date_for_All_Day_Event__c = ter.End_Date__c.date();       
                }
            }
        }    
    }

/*    
    public static void checkForAllDayEvent (List<SVMXC_Time_Entry_Request__c> triggerNew, Map<Id, SVMXC_Time_Entry_Request__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        
            //Method to check for All Day Event Time Entry Requests.  If found the start and end date times
            //are adjusted to midnight to match functionality with Event record (Written Apr 28 2015)
            
		for (SVMXC_Time_Entry_Request__c ter : triggerNew) {
            if (ter.All_Day_Event__c) {
                
                Time midnightTime = Time.newInstance(0, 0, 0, 0);
                DateTime currentStartDateTime = ter.Start_Date__c;
                Date currentStartDate = Date.newInstance(currentStartDateTime.year(), currentStartDateTime.month(), currentStartDateTime.day());        
                DateTime newStartDateTime = DateTime.newInstance(currentStartDate, midnightTime);
                ter.Start_Date__c = newStartDateTime;
                
                DateTime currentEndDateTime;
                if (ter.End_Date__c != null)
                    currentEndDateTime = ter.End_Date__c;
                else 
                    currentEndDateTime = ter.Start_Date__c;
                    
                Date currentEndDate = Date.newInstance(currentEndDateTime.year(), currentEndDateTime.month(), currentEndDateTime.day()); 
                DateTime newEndDateTime = DateTime.newInstance(currentEndDate, midnightTime);
                ter.End_Date__c = newEndDateTime;
            }
        }
        
    }
*/

    public static void setTechWhenNull (List<SVMXC_Time_Entry_Request__c> triggerNew, Map<Id, SVMXC_Time_Entry_Request__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
        /*
            Method to set Technician/Equipment on insert/update if the value is null.  
            It will be set to the running users related tech record if it exists (Written Mar 19 2015)
            
        */
        
        Set<Id> TERIds = new Set<Id>();
        
        for (SVMXC_Time_Entry_Request__c ter : triggerNew) {
            if (ter.Technician__c == null)
                TERIds.add(ter.Id); 
        }
        
        if (!TERIds.isEmpty()) {
            
            List<SVMXC__Service_Group_Members__c> techList = [SELECT Id FROM SVMXC__Service_Group_Members__c 
                                WHERE SVMXC__Salesforce_User__c =: UserInfo.getUserId()];
            
            if (!techList.isEmpty()) {
                
                // assumes that only one tech record is found for running user
                Id techId = techList[0].Id;
                
                for (SVMXC_Time_Entry_Request__c ter1 : triggerNew) {
                    if (TERIds.contains(ter1.Id) && ter1.Technician__c == null)
                        ter1.Technician__c = techId;
                }   
            }
        }
    }

    public static void ensureTechIsOwner (List<SVMXC_Time_Entry_Request__c> triggerNew, Map<Id, SVMXC_Time_Entry_Request__c> triggerOld, Boolean isInsert, Boolean isUpdate) {
        /*
            Method to ensure that the tech is the owner of the record  (Written Mar 18 2015)
          
        */
        
        Set<Id> techIds = new Set<Id>();
        
        if (isInsert) {
            for (SVMXC_Time_Entry_Request__c ter0: triggerNew) {
                if (ter0.Technician__c != null && !techIds.contains(ter0.Technician__c))
                    techIds.add(ter0.Technician__c);
            }
        } else if (isUpdate) {
            for (SVMXC_Time_Entry_Request__c ter1: triggerNew) {
                if (ter1.Technician__c != null && ter1.Technician__c != triggerOld.get(ter1.Id).Technician__c && !techIds.contains(ter1.Technician__c))
                    techIds.add(ter1.Technician__c);
            }           
        }
      
        if (!techIds.isEmpty()) {
            Map <Id, SVMXC__Service_Group_Members__c> techMap = new Map<Id, SVMXC__Service_Group_Members__c> 
                        ([SELECT Id, SVMXC__Salesforce_User__c FROM SVMXC__Service_Group_Members__c 
                        WHERE Id IN: techIds AND SVMXC__Salesforce_User__c != null]);
            
            if (techMap.size() > 0) {
                
                for (SVMXC_Time_Entry_Request__c te2: triggerNew) { 
                
                    if (te2.Technician__c != null && techMap.containsKey(te2.Technician__c) 
                            && te2.OwnerId != techMap.get(te2.Technician__c).SVMXC__Salesforce_User__c)
                        te2.OwnerId = techMap.get(te2.Technician__c).SVMXC__Salesforce_User__c;
                }
            }
        }  
    }

}