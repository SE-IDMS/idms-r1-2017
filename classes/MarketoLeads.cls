Public class MarketoLeads {

    public class LeadRecord {
        public String id;
        public String email;
        public String lastName;
        public String firstName;
        public String ClassificationLevel1;        
        public String ClassificationLevel2;
        public String phone;
        public String mobilePhone;
        public String jobFunction;
        public String title;
        public String company;
        public String mainPhone;
        public String country;        
        public String address;
        public String AdditionalAddress;
        public String state;
        public String city;   
        public String postalCode;  
        public String StateProvince;
        public String leadPartitionId;
    }
    
    public String requestId;
    public Boolean success;
    public List<LeadRecord> result;
}