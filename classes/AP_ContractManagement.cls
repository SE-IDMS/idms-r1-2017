public class AP_ContractManagement
{
    public class changeContractAccountResult
        {
        public Id           ClonedContractId = null;
        public Map <Id, Id> NotesMap         = new Map <Id, Id>();
        public Map <Id, Id> AttachmentsMap   = new Map <Id, Id>();
        }
      
        public static changeContractAccountResult changeContractAccount (Id ContractId, Id AccountId)
        {
        Contract CustomerContract = null;
        Contract CustomerContract1 = null;
        Contract ClonedContract   = null;
        Account  CustomerAccount  = null;
        
        final String  StatusActivated  = 'Activated';
        final String  StatusTerminated = 'Terminated';
        final String  StatusDraft      = 'Draft';
        List <String> sObjectFields = new List <String> ();
        List<Id> userIds = new List<Id>();
        
        changeContractAccountResult result = new changeContractAccountResult();
        
        // Check Status of Contract
        try
        {
            if (ContractId != null && AccountId != null)
            {
            CustomerContract = Database.query ('select Id, AccountId, Status from Contract where Id= \'' + ContractId + '\'');  
            CustomerAccount  = Database.query ('select Id                    from Account  where Id= \'' + AccountId + '\'');  
                
                if (CustomerContract != null && CustomerContract.Status.equalsIgnoreCase(StatusActivated))  
                {
                    if (CustomerAccount != null && CustomerContract.AccountId != AccountId)
                    {
                      // Deep-Clone the Contract
                      
                        sObject sobjContract= new Contract();
                        String allSObjectFieldsQuery = Utils_Methods.generateSelectAllQuery(sobjContract);
                        allSObjectFieldsQuery+=' where Id = \'' + ContractId + '\'';
                  
                        String allSObjectFieldsQuery1='select LegacyContractId__c from Contract where Id=\''+ContractId+'\'';
                        
                        CustomerContract = Database.query (allSObjectFieldsQuery);  
                        CustomerContract1 = Database.query (allSObjectFieldsQuery1);  
                          
                        ClonedContract = CustomerContract.clone (false, true); //', true, true);
                  
                        ClonedContract.Id        = null;
                        ClonedContract.Status    = StatusDraft;
                        ClonedContract.AccountId = AccountId;
                            if(CustomerContract1.LegacyContractId__c!=NULL && CustomerContract1.LegacyContractId__c!='')
                            {
                            ClonedContract.LegacyContractId__c=CustomerContract1.LegacyContractId__c;
                            CustomerContract1.LegacyContractId__c=null;
                            update CustomerContract1;
                            }
                        
                        insert ClonedContract;
                    
                        result.ClonedContractId = ClonedContract.Id;
                        
                        ClonedContract.Status = StatusActivated;
                        update ClonedContract;
                        
                        //to delete the CVCP created by trigger on creating a new contract
                        List <CTR_ValueChainPlayers__c> objtobedeleted = null;
                        string CVCPTobeDeleted ='select id,Contract__c from CTR_ValueChainPlayers__c where Contract__c'+'=\''+ ClonedContract.Id + '\'';
                        objtobedeleted=Database.query(CVCPTobeDeleted);
                        if(objtobedeleted.size()>0)
                        delete objtobedeleted;
                        
                        List <Case> objects = null;
                        List <CTR_ValueChainPlayers__c> objects2 = null;
                        List <ContractCustomerLocation__c> objects3 = null;
                        List<Sobject> objects1 = null;
                        List <SObject> childObjects = new List <Sobject>();
                        List <ContractExternalReference__c> objects4 = null;
                        List <Event> objects5 = null;
                        List <Package__c> objects6 = null;
                        List <Task> objects7 = null;
                        List <WorkOrderNotification__c> objects8 = null;
                        
                        Schema.DescribeSObjectResult TR1 = CTR_ValueChainPlayers__c.sObjectType.getDescribe();
                        
                        if (TR1.isQueryable() && TR1.isUpdateable())
                          { 
                            Schema.DescribeFieldResult F1 = CTR_ValueChainPlayers__c.Contract__c.getDescribe();
                            Schema.sObjectField FR1 = F1.getSObjectField();
                                String query1='select id,CreatedById,Contract__c from CTR_ValueChainPlayers__c where Contract__c'+'=\''+ CustomerContract.Id + '\'';
                                objects2=Database.query(query1);
                            
                                for(CTR_ValueChainPlayers__c so : objects2)
                                  {
                                    userIds.add(so.CreatedbyId);
                                  }
                            
                            Map<Id, User> activeUsers = new Map<Id,User>([select Id, IsActive from User where Id in: userIds and IsActive=:true]);
                            
                                for (CTR_ValueChainPlayers__c so : objects2)
                                {
                                    if(activeUsers.get(so.CreatedById)!=null){
                                    Sobject tobj=(sObject)so;
                                    tobj.put ('Contract__c', ClonedContract.Id);
                                    childObjects.add (tobj);
                                    }
                                }
                            }
                        if (childObjects != null && childObjects.size() != 0)
                        {
                          Database.saveResult[] lst =database.update(childObjects);
                        }
                        Schema.DescribeSObjectResult TR = Case.sObjectType.getDescribe();
                        
                        if (TR.isQueryable() && TR.isUpdateable())
                          { 
                            Schema.DescribeFieldResult F = Case.RelatedContract__c.getDescribe();
                            Schema.sObjectField FR = F.getSObjectField();
                                String query1='select id,status,CreatedById,RelatedContract__c from Case where RelatedContract__c'+'=\''+ CustomerContract.Id + '\'' + ' AND (Status=\'In Progress\' OR Status=\'New\')';
                                objects=Database.query(query1);
                            
                                for(Case so : objects)
                                  {
                                    userIds.add(so.CreatedbyId);
                                  }
                            
                            Map<Id, User> activeUsers = new Map<Id,User>([select Id, IsActive from User where Id in: userIds and IsActive=:true]);
                            
                                for (Case so : objects)
                                  {
                                    if(activeUsers.get(so.CreatedById)!=null){
                                    Sobject tobj=(sObject)so;
                                    tobj.put ('RelatedContract__c', ClonedContract.Id);
                                    childObjects.add (tobj);
                                    }
                                }
                            }
                            if (childObjects != null && childObjects.size() != 0)
                            {
                              Database.saveResult[] lst =database.update(childObjects);
                            }
                        
                          ////ContractCustomerLocation__c
                        
                            Schema.DescribeSObjectResult TR3 = ContractCustomerLocation__c.sObjectType.getDescribe();
                            if (TR3.isQueryable() && TR3.isUpdateable())
                              { 
                                String query3='select id,CreatedById,Contract__c from ContractCustomerLocation__c where Contract__c'+'=\''+ CustomerContract.Id + '\'';
                                objects3=Database.query(query3);
                        
                                for(ContractCustomerLocation__c so : objects3)
                                  {
                                    userIds.add(so.CreatedbyId);
                                  }
                                Map<Id, User> activeUsers = new Map<Id,User>([select Id, IsActive from User where Id in: userIds and IsActive=:true]);
                        
                                for (ContractCustomerLocation__c so : objects3)
                                  {
                                    if(activeUsers.get(so.CreatedById)!=null){
                                    Sobject tobj=(sObject)so;
                                    tobj.put ('Contract__c', ClonedContract.Id);
                                    childObjects.add (tobj);
                                        }
                                    } 
                                }
                             if (childObjects != null && childObjects.size() != 0)
                                {
                                  Database.saveResult[] lst =database.update(childObjects);
                                }
                    
                            ////ContractExternalReference__c
                    
                            Schema.DescribeSObjectResult TR4 = ContractExternalReference__c.sObjectType.getDescribe();
                            if (TR4.isQueryable() && TR4.isUpdateable())
                              { 
                                String query4='select id,CreatedById,Contract__c from ContractExternalReference__c where Contract__c'+'=\''+ CustomerContract.Id + '\'';
                                objects4=Database.query(query4);
                        
                            for(ContractExternalReference__c so : objects4)
                              {
                                userIds.add(so.CreatedbyId);
                              }
                        
                            Map<Id, User> activeUsers = new Map<Id,User>([select Id, IsActive from User where Id in: userIds and IsActive=:true]);
                        
                            for (ContractExternalReference__c so : objects4)
                              {
                                if(activeUsers.get(so.CreatedById)!=null){
                                Sobject tobj=(sObject)so;
                                tobj.put ('Contract__c', ClonedContract.Id);
                                childObjects.add (tobj);
                                    }
                                } 
                            }
                         if (childObjects != null && childObjects.size() != 0)
                        {
                          Database.saveResult[] lst =database.update(childObjects);
                        }
                    
                        ////Event
                    
                            Schema.DescribeSObjectResult TR5 = Event.sObjectType.getDescribe();
                            if (TR5.isQueryable() && TR5.isUpdateable())
                              { 
                                String query5='select id,CreatedById,WhatId from Event where WhatId'+'=\''+ CustomerContract.Id + '\'';
                                objects5=Database.query(query5);
                        
                            for(Event so : objects5)
                              {
                                userIds.add(so.CreatedbyId);
                              }
                        
                            Map<Id, User> activeUsers = new Map<Id,User>([select Id, IsActive from User where Id in: userIds and IsActive=:true]);
                        
                            for (Event so : objects5)
                              {
                                if(activeUsers.get(so.CreatedById)!=null){
                                Sobject tobj=(sObject)so;
                                tobj.put ('WhatId', ClonedContract.Id);
                                childObjects.add (tobj);
                                    }
                                } 
                            }
                         if (childObjects != null && childObjects.size() != 0)
                        {
                          Database.saveResult[] lst =database.update(childObjects);
                        }
                    
                        ////Package__c
                        
                            Schema.DescribeSObjectResult TR6 = Package__c.sObjectType.getDescribe();
                            if (TR6.isQueryable() && TR6.isUpdateable())
                              { 
                            String query6='select id,CreatedById,Contract__c from Package__c where Contract__c'+'=\''+ CustomerContract.Id + '\'';
                            objects6=Database.query(query6);
                        
                            for(Package__c so : objects6)
                              {
                                userIds.add(so.CreatedbyId);
                              }
                            
                                Map<Id, User> activeUsers = new Map<Id,User>([select Id, IsActive from User where Id in: userIds and IsActive=:true]);
                            
                                for (Package__c so : objects6)
                                  {
                                    if(activeUsers.get(so.CreatedById)!=null){
                                    Sobject tobj=(sObject)so;
                                    tobj.put ('Contract__c', ClonedContract.Id);
                                    childObjects.add (tobj);
                                        }
                                    } 
                                }
                             if (childObjects != null && childObjects.size() != 0)
                            {
                              Database.saveResult[] lst =database.update(childObjects);
                            }
                        
                         ////Task
                        
                            Schema.DescribeSObjectResult TR7 = Task.sObjectType.getDescribe();
                                if (TR7.isQueryable() && TR7.isUpdateable())
                                  { 
                                    String query7='select id,CreatedById,WhatId from Task where WhatId'+'=\''+ CustomerContract.Id + '\'';
                                    objects7=Database.query(query7);
                            
                                for(Task so : objects7)
                                  {
                                    userIds.add(so.CreatedbyId);
                                  }
                            
                                    Map<Id, User> activeUsers = new Map<Id,User>([select Id, IsActive from User where Id in: userIds and IsActive=:true]);
                            
                                for (Task so : objects7)
                                  {
                                    if(activeUsers.get(so.CreatedById)!=null){
                                    Sobject tobj=(sObject)so;
                                    tobj.put ('WhatId', ClonedContract.Id);
                                    childObjects.add (tobj);
                                        }
                                    } 
                                }
                             if (childObjects != null && childObjects.size() != 0)
                            {
                              Database.saveResult[] lst =database.update(childObjects);
                            }
                        
                         ////WorkOrderNotification__c
                        
                            Schema.DescribeSObjectResult TR8 = WorkOrderNotification__c.sObjectType.getDescribe();
                            if (TR8.isQueryable() && TR8.isUpdateable())
                              { 
                            String query8='select id,CreatedById,Contract__c from WorkOrderNotification__c where Contract__c'+'=\''+ CustomerContract.Id + '\'';
                            objects8=Database.query(query8);
                        
                            for(WorkOrderNotification__c so : objects8)
                              {
                                userIds.add(so.CreatedbyId);
                              }
                            
                            Map<Id, User> activeUsers = new Map<Id,User>([select Id, IsActive from User where Id in: userIds and IsActive=:true]);
                            
                            for (WorkOrderNotification__c so : objects8)
                              {
                                if(activeUsers.get(so.CreatedById)!=null){
                                Sobject tobj=(sObject)so;
                                tobj.put ('Contract__c', ClonedContract.Id);
                                childObjects.add (tobj);
                                    }
                                } 
                            }
                             if (childObjects != null && childObjects.size() != 0)
                            {
                              Database.saveResult[] lst =database.update(childObjects);
                            }
                        
                
                    // Re-parent the Attachments

                    List <Attachment> attachmentsToInsert = new List <Attachment>();
                    List <Attachment> attachmentsToDelete = new List <Attachment>();

                    sObjectFields = new List <String> ();

                    // Get the fields from the Contract object

                    sObjectFields.addAll (Schema.SObjectType.Attachment.fields.getMap().keySet());
                    /*
                    allSObjectFieldsQuery = 'select ' + sObjectFields.get(0); 

                    for (Integer i=1 ; i < sObjectFields.size() ; i++)
                    {
                      allSObjectFieldsQuery += ', ' + sObjectFields.get(i);
                    }

                    allSObjectFieldsQuery += ' from Attachment where ParentId = \'' + CustomerContract.Id + '\'';
                    */
                    
                    allSObjectFieldsQuery='select name,body,ParentId from Attachment where ParentId = \'' + CustomerContract.Id + '\'';
                    
                    List <Attachment> attachments = null;
                    attachments = Database.query (allSObjectFieldsQuery);

                    Attachment ClonedAttachment;

                    for (Attachment A : attachments)
                    {
                      ClonedAttachment = A.clone(false,false);
                  
                      ClonedAttachment.parentId = ClonedContract.Id;

                      attachmentstoInsert.add(ClonedAttachment);

                      attachmentsToDelete.add (A);

                    }
                    insert attachmentsToInsert;

                    // Map the Ids
                    
                    for (Integer i=0; i<attachments.size(); i++)
                    {
                      result.AttachmentsMap.put (attachmentsToDelete[i].Id, attachmentsToInsert[i].Id);
                    }
                    
                    delete attachmentsToDelete;
                    
                    // Re-parent the Notes
                    
                    List <Note> notesToInsert = new List <Note>();
                    List <Note> notesToDelete = new List <Note>();

                    sObjectFields = new List <String>{};

                    // Get the fields from the Contract object

                    sObjectFields.addAll (Schema.SObjectType.Note.fields.getMap().keySet());
                    /*
                    
                    allSObjectFieldsQuery = 'select ' + sObjectFields.get(0); 

                    for (Integer i=1 ; i < sObjectFields.size() ; i++)
                    {
                      allSObjectFieldsQuery += ', ' + sObjectFields.get(i);
                    }

                    allSObjectFieldsQuery += ' from Note where ParentId = \'' + CustomerContract.Id + '\'';
                    */
                    
                    allSObjectFieldsQuery='select body,title,ParentId from Note where ParentId = \'' + CustomerContract.Id + '\'';
                    List <Note> notes = new list<Note>();

                    notes = Database.query (allSObjectFieldsQuery);

                    Note ClonedNote = new note();
                    
                    for (Note N : notes)
                    {
                      ClonedNote = N.clone(false,false);
                  
                      ClonedNote.parentId = ClonedContract.Id;

                      notestoInsert.add(ClonedNote);

                      notesToDelete.add (N);
                    }
                    insert notesToInsert;
                    

                    // Map the Ids
                    
                    for (Integer i=0; i<notes.size(); i++)
                    {
                      result.NotesMap.put (notesToDelete[i].Id, notesToInsert[i].Id);
                    }
                    
                    delete notesToDelete;
                    
                    
                     // Terminate the Contract

                    CustomerContract.Status = StatusTerminated;

                    update CustomerContract;
                    
                    }
                }
            }
        }
        catch(System.DMLException e) 
              {
                throw(e);
              }
            return result;
        }
    }