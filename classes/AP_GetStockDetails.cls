// Created by Uttara PR - Nov 2016 Release - Escape Notes project

public class AP_GetStockDetails {
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == JSONToken.END_OBJECT ||
                curr == JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }

    public class Organization {
        public String id {get;set;} 
        public String code {get;set;} 
        public String type_Z {get;set;} // in json: type
        public String FOOrg {get;set;} // Front Office Organization

        public Organization(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'id') {
                            id = parser.getText();
                        } else if (text == 'code') {
                            code = parser.getText();
                        } else if (text == 'type') {
                            type_Z = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Organization consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Stock_Z {
        public String name {get;set;} 
        public String value {get;set;} 

        public Stock_Z(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'name') {
                            name = parser.getText();
                        } else if (text == 'value') {
                            value = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Stock_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public String targetERP {get;set;} 
    public List<Product> product {get;set;} 

    public AP_GetStockDetails(JSONParser parser) {
        while (parser.nextToken() != JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != JSONToken.VALUE_NULL) {
                    if (text == 'targetERP') {
                        targetERP = parser.getText();
                    } else if (text == 'product') {
                        product = new List<Product>();
                        while (parser.nextToken() != JSONToken.END_ARRAY) {
                            product.add(new Product(parser));
                        }
                    } else {
                        System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class Product {
    
        public Boolean selected {get;set;} // Select checkbox on page
        public String sku {get;set;} 
        public String targetSku {get;set;} 
        public Organization organization {get;set;} 
        public List<Stock> stock {get;set;} 
        public Error error {get;set;} 

        public Product(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'sku') {
                            sku = parser.getText();
                        } else if (text == 'targetSku') {
                            targetSku = parser.getText();
                        } else if (text == 'organization') {
                            organization = new Organization(parser);
                        } else if (text == 'stock') {
                            stock = new List<Stock>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                stock.add(new Stock(parser));
                            }
                        } else if (text == 'error') {
                            error = new Error(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Product consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Error {
        public String message {get;set;} 

        public Error(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'message') {
                            message = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Error consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Stock {
        public String name {get;set;} 
        public String value {get;set;} 
        public String currency1 {get;set;} 

        public Stock(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'name') {
                            name = parser.getText();
                        } else if (text == 'value') {
                            value = parser.getText();
                        } else if (text == 'currency') {
                            currency1 = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Stock consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static AP_GetStockDetails parse(String json) {
        return new AP_GetStockDetails(System.JSON.createParser(json));
    }
}