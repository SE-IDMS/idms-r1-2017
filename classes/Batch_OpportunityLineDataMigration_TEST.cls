@isTest
private class Batch_OpportunityLineDataMigration_TEST {
    static testMethod void testOpportunityLineDataMigration() {
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        
        Opportunity objOpp = Utils_TestMethods.createOpenOpportunity(objAccount.Id);
        insert objOpp;
        
        OPP_ProductLine__c objOppLine = Utils_TestMethods.createProductLine(objOpp.Id);
        objOppLine.ProductBU__c = 'ProdBUA1';
        objOppLine.ProductLine__c = 'ProdLineA1';
        objOppLine.ProductFamily__c = 'ProdFamilyA1';
        objOppLine.Family__c = 'FamilyA1';
        objOppLine.TECH_CommercialReference__c = 'CMREF123';
        objOppLine.TECH_2014ReferentialUpdated__c =FALSE;
        insert objOppLine;
        
        TECH_CommRefAllLevel__c objCommRefAllLevel = new TECH_CommRefAllLevel__c();
        objCommRefAllLevel.Name = 'CMREF123'; 
        objCommRefAllLevel.BusinessUnit__c = 'ProdBUA1'; 
        objCommRefAllLevel.ProductLine__c = 'ProdLineA1';  
        objCommRefAllLevel.ProductFamily__c = 'ProdFamilyA1';  
        objCommRefAllLevel.Family__c = 'FamilyA1';  
        objCommRefAllLevel.SubFamily__c = 'SubFamilyA1';  
        objCommRefAllLevel.ProductSuccession__c = 'ProdSuccessA1';  
        objCommRefAllLevel.ProductGroup__c = 'ProdGroupA1';  
        objCommRefAllLevel.NewGMRCode__c = '01122435452516738B'; 
        
        insert objCommRefAllLevel;
    
        Test.startTest();
        List<String> lstStageNames = new List<String>();
        lstStageNames.add('1 - Understand Business Context');
        Batch_OpportunityLineDataMigration objBatchJob = new  Batch_OpportunityLineDataMigration(lstStageNames,1);
        Database.executeBatch(objBatchJob);
        Test.stopTest();
    }
}