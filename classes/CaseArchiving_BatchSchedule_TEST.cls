@isTest
Public Class CaseArchiving_BatchSchedule_TEST{
    static testMethod void myUnitTest_schedule() {
            List<CS_ForFieldMapping__c>  cslist = new List<CS_ForFieldMapping__c>();
    
        CS_ForFieldMapping__c obj1= new CS_ForFieldMapping__c();
        obj1.name='Test_1';
        obj1.SourceObject__c ='Case';
        obj1.TargetObject__c = 'ArchiveCase__c';
        obj1.SourceField__c = 'AnswerToCustomer__c';
        obj1.TargetField__c = 'AnswerToCustomer__c';
        cslist.add(obj1);
        
        CS_ForFieldMapping__c obj2= new CS_ForFieldMapping__c();
        obj2.name='Test_2';
        obj2.SourceObject__c ='Case';
        obj2.TargetObject__c = 'ArchiveCase__c';
        obj2.SourceField__c = 'AssetId';
        obj2.TargetField__c = 'Asset__c';
        cslist.add(obj2);
        
        CS_ForFieldMapping__c obj3= new CS_ForFieldMapping__c();
        obj3.name='Test_3';
        obj3.SourceObject__c ='Case';
        obj3.TargetObject__c = 'ArchiveCase__c';
        obj3.SourceField__c = 'AssetOwner__c';
        obj3.TargetField__c = 'AssetOwner__c';
        cslist.add(obj3);
        
        CS_ForFieldMapping__c obj4= new CS_ForFieldMapping__c();
        obj4.name='Test_4';
        obj4.SourceObject__c ='Case';
        obj4.TargetObject__c = 'ArchiveCase__c';
        obj4.SourceField__c = 'CallbackPhone__c';
        obj4.TargetField__c = 'CallbackPhone__c';
        cslist.add(obj4);
        
        CS_ForFieldMapping__c obj5= new CS_ForFieldMapping__c();
        obj5.name='Test_5';
        obj5.SourceObject__c ='Case';
        obj5.TargetObject__c = 'ArchiveCase__c';
        obj5.SourceField__c = 'CampaignKeycode__c';
        obj5.TargetField__c = 'CampaignKeycode__c';
        cslist.add(obj5);
        
        CS_ForFieldMapping__c obj6= new CS_ForFieldMapping__c();
        obj6.name='Test_6';
        obj6.SourceObject__c ='Case';
        obj6.TargetObject__c = 'ArchiveCase__c';
        obj6.SourceField__c = 'CaseNumber';
        obj6.TargetField__c = 'Name';
        cslist.add(obj6);
       
        CS_ForFieldMapping__c obj7= new CS_ForFieldMapping__c();
        obj7.name='Test_7';
        obj7.SourceObject__c ='Case';
        obj7.TargetObject__c = 'ArchiveCase__c';
        obj7.SourceField__c = 'OwnerId';
        obj7.TargetField__c = 'OwnerId';
        cslist.add(obj7);
            
        CS_ForFieldMapping__c obj11= new CS_ForFieldMapping__c();
        obj11.name='Test_11';
        obj11.SourceObject__c ='CaseStage__c';
        obj11.TargetObject__c = 'ArchiveCaseStage__c';
        obj11.SourceField__c = 'BusinessHours__c';
        obj11.TargetField__c = 'BusinessHours__c';
        cslist.add(obj11);
        
        CS_ForFieldMapping__c obj12= new CS_ForFieldMapping__c();
        obj12.name='Test_12';
        obj12.SourceObject__c ='CaseStage__c';
        obj12.TargetObject__c = 'ArchiveCaseStage__c';
        obj12.SourceField__c = 'Name';
        obj12.TargetField__c = 'Name';
        cslist.add(obj12);
        
        CS_ForFieldMapping__c obj13= new CS_ForFieldMapping__c();
        obj13.name='Test_13';
        obj13.SourceObject__c ='InquiraFAQ__c';
        obj13.TargetObject__c = 'ArchiveInquiraFAQ__c';
        obj13.SourceField__c = 'InfoCenterURL__c';
        obj13.TargetField__c = 'InfoCenterURL__c';
        cslist.add(obj13);
        
        CS_ForFieldMapping__c obj14= new CS_ForFieldMapping__c();
        obj14.name='Test_14';
        obj14.SourceObject__c ='InquiraFAQ__c';
        obj14.TargetObject__c = 'ArchiveInquiraFAQ__c';
        obj14.SourceField__c = 'Name';
        obj14.TargetField__c = 'Name';
        cslist.add(obj14);
        
        CS_ForFieldMapping__c obj15= new CS_ForFieldMapping__c();
        obj15.name='Test_15';
        obj15.SourceObject__c ='InquiraFAQ__c';
        obj15.TargetObject__c = 'ArchiveInquiraFAQ__c';
        obj15.SourceField__c = 'URL__c';
        obj15.TargetField__c = 'URL__c';
        cslist.add(obj15);
        
        CS_ForFieldMapping__c obj16= new CS_ForFieldMapping__c();
        obj16.name='Test_16';
        obj16.SourceObject__c ='CaseComment';
        obj16.TargetObject__c = 'ArchiveCaseStage__c';
        obj16.SourceField__c = 'CommentBody';
        obj16.TargetField__c = 'CommentBody__c';
        cslist.add(obj16);
        
        CS_ForFieldMapping__c obj17= new CS_ForFieldMapping__c();
        obj17.name='Test_17';
        obj17.SourceObject__c ='CaseComment';
        obj17.TargetObject__c = 'ArchiveCaseStage__c';
        obj17.SourceField__c = 'IsPublished';
        obj17.TargetField__c = 'IsPublished__c';
        cslist.add(obj17);
        
        CS_ForFieldMapping__c obj18= new CS_ForFieldMapping__c();
        obj18.name='Test_18';
        obj18.SourceObject__c ='CaseComment';
        obj18.TargetObject__c = 'ArchiveCaseStage__c';
        obj18.SourceField__c = 'CreatedById';
        obj18.TargetField__c = 'CreatedBy__c';
        cslist.add(obj18);
        
        
        CS_ForFieldMapping__c obj19= new CS_ForFieldMapping__c();
        obj19.name='Test_19';
        obj19.SourceObject__c ='EmailMessage';
        obj19.TargetObject__c = 'ArchiveEmailMessage__c';
        obj19.SourceField__c = 'FromName';
        obj19.TargetField__c = 'FromName__c';
        cslist.add(obj19);
        
        CS_ForFieldMapping__c obj20= new CS_ForFieldMapping__c();
        obj20.name='Test_20';
        obj20.SourceObject__c ='EmailMessage';
        obj20.TargetObject__c = 'ArchiveEmailMessage__c';
        obj20.SourceField__c = 'Status';
        obj20.TargetField__c = 'Status__c';
        cslist.add(obj20);
        
        
        CS_ForFieldMapping__c obj21= new CS_ForFieldMapping__c();
        obj21.name='Test_21';
        obj21.SourceObject__c ='EmailMessage';
        obj21.TargetObject__c = 'ArchiveEmailMessage__c';
        obj21.SourceField__c = 'Subject';
        obj21.TargetField__c = 'Subject__c';
        cslist.add(obj21);
        
        CS_ForFieldMapping__c obj22= new CS_ForFieldMapping__c();
        obj22.name='Test_22';
        obj22.SourceObject__c ='SVMXC__Case_Line__c';
        obj22.TargetObject__c = 'ArchiveInstalledProduct__c';
        obj22.SourceField__c = 'SVMXC__Entitled_Exchange_Type__c';
        obj22.TargetField__c = 'SVMXC_Entitled_Exchange_Type__c';
        cslist.add(obj22);
        
        CS_ForFieldMapping__c obj23= new CS_ForFieldMapping__c();
        obj23.name='Test_23';
        obj23.SourceObject__c ='SVMXC__Case_Line__c';
        obj23.TargetObject__c = 'ArchiveInstalledProduct__c';
        obj23.SourceField__c = 'SVMXC__Entitlement_History__c';
        obj23.TargetField__c = 'SVMXC_Entitlement_History__c';
        cslist.add(obj23);
        
        CS_ForFieldMapping__c obj24= new CS_ForFieldMapping__c();
        obj24.name='Test_24';
        obj24.SourceObject__c ='SVMXC__Case_Line__c';
        obj24.TargetObject__c = 'ArchiveInstalledProduct__c';
        obj24.SourceField__c = 'SVMXC__Line_Status__c';
        obj24.TargetField__c = 'SVMXC_Line_Status__c';
        cslist.add(obj24);
        
        insert cslist;
    
        CS_Severity__c sevobj = new CS_Severity__c();
        sevobj.value__c=7;
        sevobj.name='test CS';
        insert sevobj;


        Account accountObj = Utils_TestMethods.createAccount();
        Database.insert(accountObj);
             
        contact contactObj=Utils_TestMethods.createContact(accountObj.Id,'testContact');
        Database.insert(contactObj);     
        
        Country__c countryObj = Utils_TestMethods.createCountry();
        Database.insert(countryObj);
    
    Case caseObj=Utils_TestMethods.createCase(accountObj.id,contactObj.id,'closed');
        //caseObj.Family_lk__c = objProduct1.Id;
        Database.insert(caseObj);
        
       
     
        /*
        OPP_Product__c objProduct1 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU','TEST_PL','TEST_PF','TEST_FLMY');
        Database.insert(objProduct1);
        
        Lead lead1 = Utils_TestMethods.createLead(null, countryObj.Id,100);
        insert lead1;
        
        CustomerLocation__c custlocobj=  Utils_TestMethods.createCustomerLocation(accountObj.id,countryObj.Id);
        Database.insert(custlocobj);
        
        
        CCCAction__c cccactionobj= Utils_TestMethods.createCCCAction(caseObj.Id);
        Database.insert(cccactionobj);
       
       
        ITB_Asset__c itbassetobj= Utils_TestMethods.createITBAsset(caseObj.Id);
        Database.insert(itbassetobj);
       
        LiteratureRequest__c literaturereqObj=Utils_TestMethods.createLiteratureRequest(accountObj.Id, contactObj.id,caseObj.Id);
        Database.insert(literaturereqObj);
        
        Opportunity oppobj=Utils_TestMethods.createOpportunity(accountObj.id);
        oppobj.ReferenceCase__c=caseObj.id;
        Database.insert(oppobj);
        
        EmailMessage[] emailObj = new EmailMessage[0];
         emailObj.add(new EmailMessage(FromAddress = 'test@abc.org', Incoming = True, ToAddress= 'anil.budha@non.schneider-electric.com', Subject = 'Test email', TextBody = '23456 ', ParentId =caseObj.id )); 
        Database.insert(emailObj);
        
        CSE_RelatedProduct__c cseRelProdObj = new CSE_RelatedProduct__c();
        cseRelProdObj.Case__c=caseObj.id;
        Database.insert(cseRelProdObj); 
        
        
        */
        
        
        
        Test.startTest();
        
        
        
        CaseComment cseComentObj = new CaseComment(ParentId = caseObj.id,IsPublished = false);
        Database.insert(cseComentObj);
             
        CaseStage__c casestageobj = new casestage__c();
        casestageobj.case__c=caseObj.id;
        Database.insert(casestageobj);
         
        CSE_L2L3InternalComments__c internalobj = new CSE_L2L3InternalComments__c();
        internalobj.Case__c=caseObj.id;
        Database.insert(internalobj);
        
        PS_OLH__Chat__c chatObj= new PS_OLH__Chat__c();
        chatObj.PS_OLH__Case__c=caseObj.id;
        chatObj.PS_OLH__Account__c=accountObj.Id;
        chatObj.PS_OLH__Contact__c=contactObj.id;
        Database.insert(chatObj);
        /*
        PS_OLH__Mail__c mailObj=new PS_OLH__Mail__c();
        mailObj.PS_OLH__Account__c=accountObj.Id;
        mailObj.PS_OLH__Contact__c=contactObj.id;
        mailObj.PS_OLH__Lead__c=lead1.id;
        mailObj.PS_OLH__Case__c=caseObj.id;
        Database.insert(mailObj);
        */
        InquiraFAQ__c inquiraobj=Utils_TestMethods.createFAQ(caseObj.Id);
        Database.insert(inquiraobj);
            
        RMA__c rmaObj = new RMA__c();
        rmaObj.AccountName__c=accountObj.Id;
        rmaObj.ContactName__c=contactObj.Id;
        rmaObj.Case__c=caseObj.Id;
        rmaObj.ProductCondition__c='TestPC';
        rmaObj.CustomerResolution__c='TestRes';
        rmaObj.AccountCity__c='TestCity';
        rmaObj.AccountZipCode__c='1111';
        rmaObj.AccountCountry__c=countryObj.Id;
        Database.insert(rmaObj);
        
        
        CustomerSafetyIssue__c saftyobj =  new CustomerSafetyIssue__c();
        saftyobj.AffectedCustomer__c=accountObj.Id;
        saftyobj.CountryOfOccurrence__c=countryObj.id;
        saftyobj.RelatedbFOCase__c=caseObj.id;
        Database.insert(saftyobj);
        Test.stopTest();
    /*
        WorkOrderNotification__c workordernotiobj=Utils_TestMethods.createWorkOrderNotification(caseObj.id,custlocobj.id);
        Database.insert(workordernotiobj);

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', ProfileId = p.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
                    
            
        BusinessRiskEscalations__c bresObj = Utils_TestMethods.createBusinessRiskEscalation(accountObj.Id,countryObj.id,u.Id);
        Database.Insert(bresObj); 
        
         BusinessRiskEscalationCaseLink__c breobj= new BusinessRiskEscalationCaseLink__c();
         breobj.BusinessRisk__c=bresObj.id;
         breobj.Case__c=caseObj.id;
         insert breobj;
         
        Product2 prod = new Product2( ProductCode='UTProd001',
                Name='UTProd001',
                Family='Oncology',
                SVMXC__Product_Line__c='Desktop',
                IsActive=true);
        insert prod;
        Profile p1 = [select id from profile where name = 'System Administrator' limit 1];
        User newUser1 = new User(alias = 'alias1', 
                                email = 'maxexpress@servicemax.com', 
                                emailencodingkey = 'UTF-8', 
                                lastname = 'newuserTest1',
                                languagelocalekey = 'en_US', 
                                localesidkey = 'en_US', 
                                profileid = p1.id, 
                                timezonesidkey = 'America/Los_Angeles', 
                                username = 'svmx1@testorg.com'
                                //UserRoleId= ur.Id, 
                                );

        insert newUser1;
        SVMXC__Territory__c ter1=new  SVMXC__Territory__c(Name ='pter');  
        insert  ter1;  
        SVMXC__Service_Group__c st1= new SVMXC__Service_Group__c(
                     Name='St1',
                     SVMXC__Active__c=true,
                     SVMXC__Group_Type__c='Internal',
                     SVMXC__Group_Code__c='test213');
            insert st1;
        
        SVMXC__Service_Group_Members__c tech1 = Utils_TestMethods.createTech_Equip(newUser1.Id,st1.Id);
            tech1.recordtypeid = Label.CLDEC12SRV39;
        insert tech1;
        SVMXC__Site__c  loc = New SVMXC__Site__c(SVMXC__Street__c='Add Lin 1',
                    AddressLine2__c='Add Line 2',
                    SVMXC__City__c='Bangalore',
                    HealthAndSafety__c='Drug Test',
                    SVMXC__Account__c=accountObj.id,
                    SVMXC__Zip__c='1234'); 
        Database.insert(loc);           
        WorkOrderNotification__c won1=   new WorkOrderNotification__c(
            Location__c=loc.id,
            Work_Order_Status__c='open',
            ContactLastName__c='WNContact',
            ContactPhoneNumber__c='984456565',
            Scheduled_Date_Time__c=system.now(), 
            ServicesBusinessUnit__c='ITB',
            Case__c=caseObj.id );
        insert won1;
        
        
        SVMXC__Service_Order__c WO = new SVMXC__Service_Order__c(                                        
            SVMXC__Company__c=accountObj.Id,
            SVMXC__Order_Status__c = 'Open', 
            BackOfficeReference__c='123',  
            SVMXC__Product__c = prod.id,
            SVMXC__Case__c=caseObj.id,
            SVMXC__Contact__c = contactObj.Id, 
            SVMXC__Primary_Territory__c=ter1.id,
            RescheduleReason__c='Weather Related',
            Service_Business_Unit__c='Energy',
            Parent_Work_Order__c = null,
            SVMXC__Group_Member__c=tech1.id,
            Work_Order_Notification__c=won1.id,
            Is_Billable__c = true,
            SVMXC__Locked_By_DC__c=true,
            Customer_Service_Request_Time__c= system.now());
        insert WO;
        
        
        
        LiveChatTranscript transcriptObj = new LiveChatTranscript();
        transcriptObj.caseID=caseObj.id;
        transcriptObj.LeadID=lead1.id;
        Database.insert(transcriptObj);
       
        Problem__c problemobj = Utils_TestMethods.createProblem(lCROrganzObj.id);
        Database.insert(problemobj);
        
        ProblemCaseLink__c problemcaseobj = new ProblemCaseLink__c();
        problemcaseobj.case__c = caseObj.id;
        problemcaseobj.problem__c = problemobj.id;
        database.insert(problemcaseobj);

    */
            
            
            
            
            
                    String CRON_EXP = '0 0 0 1 1 ? 2025';  
                    String jobId = System.schedule('CaseArchiving_BatchSchedule_TEST', CRON_EXP, new CaseArchiving_BatchSchedule() );

            
    }

}