/**
 * Created By: Zohar Sinay(KTC)
 * Date: 25/01/2015
 * Description: This class creates the SOAP request to create/update Quotes and Subscriptions at Zuora
 **/
public class Zuora_Request_Soap_Handler {
     
    public class zuoraHandler{
        public String id;
        public String code;
        public String message;

        public zuoraHandler(String id, String code, String message){
            this.id = id;
            this.code = code;
            this.message = message;
        }
    }

    public static zuoraHandler loginToZuora(){
        String ZUORALOGIN;
        String ZUORAPASSWORD;
        String ZUORAENDPOINT;
 
        Zuora.zApi zApiInstance = new Zuora.zApi();
        try {
        
              if(Test.isRunningTest())
              {
                  ZUORALOGIN    = 'lamia_SE_DEV@zuora.com';
                  ZUORAPASSWORD = 'ZuoraKer2014!';
                  ZUORAENDPOINT = 'https://apisandbox-zforsf.zuora.com/apps/services/a/35.0';
              }
              else
              {
                  ZUORALOGIN    = Credentials_CS__c.getall().get('Zuora').Login__c;
                  ZUORAPASSWORD = Credentials_CS__c.getall().get('Zuora').Password__c;
                  ZUORAENDPOINT = Credentials_CS__c.getall().get('Zuora').EndPoint__c;
              }

            zApiInstance.setEndpoint(ZUORAENDPOINT);
            Zuora.zApi.LoginResult login= zApiInstance.zlogin(ZUORALOGIN, ZUORAPASSWORD);
            
            system.debug('kerensen login: ' + login);
            system.debug('kerense login.session: ' + login.session);
            return(new zuoraHandler(login.session, null, null));
        } catch (Zuora.zRemoteException ex) {
            system.debug('kerensen ex.code: ' + ex.code);
            system.debug('kerensen ex.getMessage(): ' + ex.getMessage());
            return(new zuoraHandler(null, ex.code, ex.getMessage()));
        //more exception handling code here...
        } catch (Zuora.zAPIException ex) {
            system.debug('kerensen ex.getMessage(): ' + ex.getMessage());
            return(new zuoraHandler(null, ex.code, ex.getMessage()));
        //more exception handling code here...
        } catch (Zuora.zForceException ex) {
            system.debug('kerensen ex.getMessage(): ' + ex.getMessage());
            return(new zuoraHandler(null, ex.code, ex.getMessage()));
        //more exception handling code here...
        } catch (Exception ex){
            system.debug('kerensen ex.getMessage(): ' + ex.getMessage());
            return(new zuoraHandler(null, null, ex.getMessage()));
        }

    }
    /**
     * 
     */
    public static zuoraHandler updateZuoraAccount(String sessionId, String ZuoraAccountId, String ZuoraBillToOrSoldToId, String ZuoraBillToOrSoldTo){
        system.debug('kerensen is here');
        HttpRequest req = createZuoraRequestHeaders();

        string soapXml =
        '<?xml version="1.0" encoding="UTF-8"?>'+
           '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
              '<soapenv:Header>'+
                 '<ns1:SessionHeader xmlns:ns1="http://api.zuora.com/" soapenv:mustUnderstand="0">'+
                    '<ns1:session>'+sessionId+'</ns1:session>'+
                 '</ns1:SessionHeader>'+
              '</soapenv:Header>'+
              '<soapenv:Body>'+
                 '<ns1:update xmlns:ns1="http://api.zuora.com/">'+
                    '<ns1:zObjects xmlns:ns2="http://object.api.zuora.com/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:Account">'+
                       '<ns2:Id>'+validateNotNull(ZuoraAccountId)+'</ns2:Id>'+           
                        '<ns2:'+ZuoraBillToOrSoldTo+'>'+validateNotNull(ZuoraBillToOrSoldToId)+'</ns2:'+ZuoraBillToOrSoldTo+'>'+
                    '</ns1:zObjects>'+
                 '</ns1:update>'+
              '</soapenv:Body>'+
           '</soapenv:Envelope>';

        system.debug('Kerensen soapXml = ' + soapXml);

        try{
            req.setBody(soapXml);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            system.debug('Kerensen res.getBody(): ' + res.getBody());
            system.debug('Kerensen res.getStatusCode(): ' + res.getStatusCode());
            return parse(res.getBody());
        }
        catch(Exception e){
            return new zuoraHandler(null, null, e.getMessage());
        }
    }
    
    

    public static zuoraHandler updateZuoraContact(String sessionId, Contact con, String zuoraContactId){
        system.debug('kerensen is here');
        HttpRequest req = createZuoraRequestHeaders();
        
        string soapXml =

        '<?xml version="1.0" encoding="UTF-8"?>'+
           '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
              '<soapenv:Header>'+
                 '<ns1:SessionHeader xmlns:ns1="http://api.zuora.com/" soapenv:mustUnderstand="0">'+
                    '<ns1:session>'+validateNotNull(sessionId)+'</ns1:session>'+
                 '</ns1:SessionHeader>'+
              '</soapenv:Header>'+
              '<soapenv:Body>'+
                 '<ns1:update xmlns:ns1="http://api.zuora.com/">'+
                    '<ns1:zObjects xmlns:ns2="http://object.api.zuora.com/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:Contact">'+
                       '<ns2:Id>'+validateNotNull(zuoraContactId)+'</ns2:Id>'+
                        '<ns2:Address1>'+validateNotNull(con.MailingStreet)+'</ns2:Address1>'+
                        '<ns2:City>'+validateNotNull(con.MailingCity)+'</ns2:City>'+
                        '<ns2:Country>'+validateNotNull(con.MailingCountry)+'</ns2:Country>'+
                        '<ns2:FirstName>'+validateNotNull(con.FirstName)+'</ns2:FirstName>'+
                        '<ns2:LastName>'+validateNotNull(con.LastName)+'</ns2:LastName>'+   
                        '<ns2:PostalCode>'+validateNotNull(con.MailingPostalCode)+'</ns2:PostalCode>'+
                        '<ns2:State>'+validateNotNull(con.MailingState)+'</ns2:State>'+
                        '<ns2:WorkEmail>'+validateNotNull(con.Email)+'</ns2:WorkEmail>'+
                        '<ns2:WorkPhone>'+validateNotNull(con.WorkPhone__c)+'</ns2:WorkPhone>'+
                    '</ns1:zObjects>'+
                 '</ns1:update>'+
              '</soapenv:Body>'+
           '</soapenv:Envelope>';
        
        system.debug('Kerensen soapXml = ' + soapXml);

        try{
            req.setBody(soapXml);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            return parse(res.getBody());
        }
        catch(Exception e){
            return new zuoraHandler(null, null, e.getMessage());
        }
    
    }

        public static zuoraHandler createZuoraContact(String sessionId, Contact con, String zuoraAccountId){
        system.debug('kerensen is here');
        HttpRequest req = createZuoraRequestHeaders();
        
        string soapXml =

        '<?xml version="1.0" encoding="UTF-8"?>'+
           '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
              '<soapenv:Header>'+
                 '<ns1:SessionHeader xmlns:ns1="http://api.zuora.com/" soapenv:mustUnderstand="0">'+
                    '<ns1:session>'+validateNotNull(sessionId)+'</ns1:session>'+
                 '</ns1:SessionHeader>'+
              '</soapenv:Header>'+
              '<soapenv:Body>'+
                 '<ns1:create xmlns:ns1="http://api.zuora.com/">'+
                      '<ns1:zObjects xmlns:ns2="http://object.api.zuora.com/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:Contact">'+
                            '<ns2:AccountId>'+validateNotNull(zuoraAccountId)+'</ns2:AccountId>'+
                            '<ns2:Address1>'+validateNotNull(con.MailingStreet)+'</ns2:Address1>'+
                            '<ns2:City>'+validateNotNull(con.MailingCity)+'</ns2:City>'+
                            '<ns2:Country>'+validateNotNull(con.Country__r.Name)+'</ns2:Country>'+
                            '<ns2:FirstName>'+validateNotNull(con.FirstName)+'</ns2:FirstName>'+
                            '<ns2:LastName>'+validateNotNull(con.LastName)+'</ns2:LastName>'+   
                            '<ns2:PostalCode>'+validateNotNull(con.MailingPostalCode)+'</ns2:PostalCode>'+
                            '<ns2:State>'+validateNotNull(con.MailingState)+'</ns2:State>'+
                            '<ns2:WorkEmail>'+validateNotNull(con.Email)+'</ns2:WorkEmail>'+
                            '<ns2:WorkPhone>'+validateNotNull(con.WorkPhone__c)+'</ns2:WorkPhone>'+
                      '</ns1:zObjects>'+
                '</ns1:create>'+
              '</soapenv:Body>'+
           '</soapenv:Envelope>';
        
        system.debug('Kerensen soapXml: ' + soapXml);

        try{
            req.setBody(soapXml);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            return parse(res.getBody());
        }
        catch(Exception e){
            return new zuoraHandler(null, null, e.getMessage());
        }
    }

    /// create update subscription()
    /// create update quote()

    public static zuoraHandler updateZuoraSubscription(String sessionId, String ZuoraSubscriptiontId, String zuoraDate, String activationOrProvisionDate){
        system.debug('kerensen is here');
        HttpRequest req = createZuoraRequestHeaders();

        string soapXml =
        '<?xml version="1.0" encoding="UTF-8"?>'+
           '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
              '<soapenv:Header>'+
                 '<ns1:SessionHeader xmlns:ns1="http://api.zuora.com/" soapenv:mustUnderstand="0">'+
                    '<ns1:session>'+sessionId+'</ns1:session>'+
                 '</ns1:SessionHeader>'+
              '</soapenv:Header>'+
              '<soapenv:Body>'+
                 '<ns1:update xmlns:ns1="http://api.zuora.com/">'+
                    '<ns1:zObjects xmlns:ns2="http://object.api.zuora.com/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="ns2:Subscription">'+
                       '<ns2:Id>'+validateNotNull(ZuoraSubscriptiontId)+'</ns2:Id>'+           
                        '<ns2:'+activationOrProvisionDate+'>'+validateNotNull(zuoraDate)+'</ns2:'+activationOrProvisionDate+'>'+
                    '</ns1:zObjects>'+
                 '</ns1:update>'+
              '</soapenv:Body>'+
           '</soapenv:Envelope>';

        system.debug('Kerensen soapXml = ' + soapXml);

        try{
            req.setBody(soapXml);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            system.debug('Kerensen res.getBody(): ' + res.getBody());
            system.debug('Kerensen res.getStatusCode(): ' + res.getStatusCode());
            return parse(res.getBody());
        }
        catch(Exception e){
            return new zuoraHandler(null, null, e.getMessage());
        }
    }
    
    

    public static zuoraHandler deleteZuoraContact(String sessionId, String zuoraContactId){
        system.debug('kerensen is here');
        HttpRequest req = createZuoraRequestHeaders();

        string soapXml =
        '<?xml version="1.0" encoding="UTF-8"?>'+
           '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
              '<soapenv:Header>'+
                 '<ns1:SessionHeader xmlns:ns1="http://api.zuora.com/" soapenv:mustUnderstand="0">'+
                    '<ns1:session>'+sessionId+'</ns1:session>'+
                 '</ns1:SessionHeader>'+
              '</soapenv:Header>'+
              '<soapenv:Body>'+
                 '<ns1:delete xmlns:ns1="http://api.zuora.com/">'+
                    '<ns1:type>'+'Contact'+'</ns1:type>'+  
                    '<ns1:ids>'+validateNotNull(zuoraContactId)+'</ns1:ids>'+           
                 '</ns1:delete>'+
              '</soapenv:Body>'+
           '</soapenv:Envelope>';

        system.debug('Kerensen soapXml = ' + soapXml);

        try{
            req.setBody(soapXml);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            system.debug('Kerensen res.getBody(): ' + res.getBody());
            system.debug('Kerensen res.getStatusCode(): ' + res.getStatusCode());
            return parse(res.getBody());
        }
        catch(Exception e){
            return new zuoraHandler(null, null, e.getMessage());
        }
    }


    private static HttpRequest createZuoraRequestHeaders(){
        HttpRequest req = new HttpRequest();
        String ZUORAENDPOINT;
        if(Test.isRunningTest())
            ZUORAENDPOINT  = 'https://apisandbox-zforsf.zuora.com/apps/services/a/35.0';
        else
            ZUORAENDPOINT = Credentials_CS__c.getall().get('Zuora').EndPoint__c;
        req.setEndpoint(ZUORAENDPOINT);
        req.setMethod('POST');
        req.setTimeout(120000);
        req.setHeader('content-type', 'application/soap+xml; charset=utf-8');
        req.setHeader('content-type', 'text/xml; charset=UTF-8');
        req.setHeader('Transfer-Encoding', 'chunked');
        req.setHeader('SOAPAction', '');
        req.setHeader('User-Agent', 'Axis2');

        return req;
    }


    // function checks if the value of the field is there, if so it keeps it, it empties it
    private static String validateNotNull(string value){
        if(value!=null && value.trim()!=''){
            return value;
        }
        return '';
    }

    public static zuoraHandler parse(String toParse) {

        DOM.Document doc = new DOM.Document();     
        try {
            doc.load(toParse);   
            system.debug('kerensen toParse: ' + toParse);
            DOM.XMLNode root = doc.getRootElement();
            return walkThrough(root, new zuoraHandler(null,null,null)); 
        } catch (System.XMLException e) {  // invalid XML
            return new zuoraHandler(null, null, e.getMessage());

        }
    }

    private static zuoraHandler walkThrough(DOM.XMLNode node, zuoraHandler zHandler) {
        zuoraHandler result = zHandler;

        if (node.getNodeType() == DOM.XMLNodeType.COMMENT) {
            return result;
        }
        if (node.getNodeType() == DOM.XMLNodeType.TEXT) {
            return result;
        }
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT) {
            if(node.getName() == 'Code'){
                result.code = node.getTexT();
                return result;
            }
            if(node.getName() == 'Message'){
                result.message = node.getTexT();
                return result;
            }
            if(node.getName() == 'Id'){
                result.id = node.getTexT();
                return result;
            }
            for (Dom.XMLNode child: node.getChildElements()) {
              walkThrough(child, result);
            }
            return result;
        }
        return new zuoraHandler(null, null, null);  //should never reach here
    }

}