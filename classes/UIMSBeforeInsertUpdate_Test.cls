/* 
    Author  : A.D.N.V.Satyanarayana
    Created Date : 28-06-2016
    Description : In order find whether the fields are auto updated with respect to fields which are not blank as per logic wrote at UIMSBeforeInsertUpdate Trigger

*/

@isTest
public class UIMSBeforeInsertUpdate_Test{

static testMethod void testMe(){
    Country__c testCountry = new Country__c();
                testCountry.Name   = 'India';
                testCountry.CountryCode__c = 'IN';
                testCountry.InternationalPhoneCode__c = '91';
                testCountry.Region__c = 'APAC';
            INSERT testCountry;
            

      PRMCountry__c prmCountry = new PRMCountry__c();
                prmCountry.Name = 'Test pagecontrol';
                prmCountry.CountryPortalEnabled__c = true;
                prmCountry.Country__c = testCountry.Id;
                prmCountry.PLDatapool__c = 'pagecontrol';
            INSERT prmcountry;
            
      User us = new user(id = userinfo.getuserId());
                us.BypassVR__c = true;
            update us;
            
      System.RunAs(us){

           FieloEE.MockUpFactory.setCustomProperties(false);          
                
           Account acc = new Account();
               acc.Name = 'test acc';
               insert acc;
                
           Contact con = new Contact();
                con.AccountId = acc.Id;
                con.FirstName = 'test contact Fn';
                con.LastName  = 'test contact Ln';
                con.PRMCompanyInfoSkippedAtReg__c = false;
                con.PRMCountry__c = testCountry.Id;
                con.PRMFirstName__c = 'PRM test contact Fn'; 
                con.PRMLastName__c  = 'PRM test contact Fn';
                insert con;
                
            List<Profile> profiles = [select id from profile where name='SE - Channel Partner (Community)'];
                if(profiles.size()>0){
                    UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
                    User u = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
                    u.ContactId = con.Id;
                    u.PRMRegistrationCompleted__c = false;
                    u.BypassWF__c = false;
                    u.BypassVR__c = true;
                    u.FirstName ='Test User First';
                    insert u;
                }
            
                
             FieloEE__Member__c member = new FieloEE__Member__c(
                    FieloEE__LastName__c = 'Polo',
                    FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                    FieloEE__Street__c = 'Calle Falsa',
                    F_PRM_PrimaryChannel__c = 'TST',F_Country__c= testCountry.Id,
                    F_Account__c=acc.Id
                );
                    insert member;

              UIMSCompany__c u = new UIMSCompany__c();
                  u.bFOAccount__c=acc.Id;
                  u.bFOContact__c= con.Id;
                  u.bFOMember__c= member.Id;
                  u.bFOUser__c=us.Id;
                    insert u;
                    System.debug('UIMSCompany123 :'+u.Id);
                

        }

    }

}