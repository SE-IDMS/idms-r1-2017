/*****
Last Modified By:Hanamanth asper Q1 release 
******/
@istest
public class VFC_SubstitutionInsert_TEST{
    
    public static Substitution__c createSubstitution(Id commercialRefId, Id OfferId){
        Substitution__c sub = new Substitution__c(
            //Name = 'TestSubstitution',
            Action__c = 'create-update',
            Comment__c = 'Test',
            Contributor__c = 'Test',
            Country__c = 'India',
            Group__c = 'Test',
            GroupingOfSubstitutions__c = 'AND',
            NewcommercialReference__c = commercialRefId,
            NewRange__c = 'Test',
            Offer__c = OfferId,
            OldCommercialReference__c = 'Test',
            QuantityNew__c = 20,
            QuantityOld__c = 22,
            SubstituteId__c = 22,
            SubstitutionBeginning__c = System.today(),
            SubstitutionId__c = 22,
            SubstitutionReason__c = 'End of life'
        );
        return sub;
    }
    
    static testmethod void otherMethods(){
        
        ApexPages.StandardSetController controller1;
        //list<User> userList = AP_EllaProjectForecast_TEST.createUsers();
        User newUser = Utils_TestMethods.createStandardUser('yuwejUseLLA12');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);   
        
        //system.runas(userList[0]){
            Test.startTest();
            list<CS_WithdrawalSubstitutionCSVMap__c> customSetRecs = new list<CS_WithdrawalSubstitutionCSVMap__c>();
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub00', ColumnNumber__c = '0', HeaderName__c = 'Substitution Id', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub01', ColumnNumber__c = '1', HeaderName__c = 'Substitute Id', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub02', ColumnNumber__c = '2', HeaderName__c = 'Action', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub03', ColumnNumber__c = '3', HeaderName__c = 'Old commercial reference', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub04', ColumnNumber__c = '4', HeaderName__c = 'Quantity (old)', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub05', ColumnNumber__c = '5', HeaderName__c = 'Substitution beginning (MM/DD/YY)', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub06', ColumnNumber__c = '6', HeaderName__c = 'Comment (en_GB)', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub07', ColumnNumber__c = '7', HeaderName__c = 'Grouping of substitutions', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub08', ColumnNumber__c = '8', HeaderName__c = 'Substitution reason', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub09', ColumnNumber__c = '9', HeaderName__c = 'New commercial reference', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub10', ColumnNumber__c = '10', HeaderName__c = 'New range', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub11', ColumnNumber__c = '11', HeaderName__c = 'Quantity (new)', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub12', ColumnNumber__c = '12', HeaderName__c = 'Group', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub13', ColumnNumber__c = '13', HeaderName__c = 'Contributor', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub14', ColumnNumber__c = '14', HeaderName__c = 'Country', Object__c = 'Substitution__c'));
        
        insert customSetRecs;
            Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
            insert offerRec;
            
            PageReference pg = Page.VFP_SubstitutionInsert;
            Test.setCurrentPage(pg);
            pg.getParameters().put('Id', offerRec.Id);
            // define controller --panggil yg public class, page reference
            VFC_SubstitutionInsert controller = new VFC_SubstitutionInsert(controller1);
            String contents = 'Test, Test/r/n, "Test"'; 
            Boolean skipHeaders = true;
            //controller.parseCSV(contents, skipHeaders);
                        
            Datetime dt = system.today();
            String inDate = String.valueOf(dt);
            VFC_SubstitutionInsert.isValidDate(inDate);
            
            Integer RowNumber = 2;
            boolean isDep = true;
            boolean isSelected = true;
            string strSubstId = '123';
            string strsubId = '123';
            string strActn = '123';
            string strOldComref = '234';
            string strQtyO = '234';
            string daSubDate = '03-04-15';
            string strComtGB = '345';
            string strgrpSub = '345';
            string srrRes = '345';
            string strNewComref = '456';
            string strnewRg = '456';
            string strQtyN = '456';
            string strGrp = '567';
            string strConbutor = '567';
            string strCoutry = '567';
            boolean blAt = true;
            boolean bloldCR = true;
            boolean blQtN = true;
            boolean blQtO = true;
            boolean blDt = true;
            boolean blGrSb = true;
            boolean blRs = true;
            boolean blnCR = true;
            boolean blContrb = true;
            boolean blCtry = true;
            VFC_SubstitutionInsert.WrapperSubstitution wrap = new VFC_SubstitutionInsert.WrapperSubstitution( RowNumber,isDep, isSelected, strSubstId, strsubId,strActn, strOldComref, strQtyO, daSubDate, strComtGB, strgrpSub, srrRes, strNewComref, strnewRg, strQtyN, strGrp, strConbutor, strCoutry, blAt, bloldCR, blQtN, blQtO , blDt, blGrSb, blRs , blnCR, blContrb, blCtry);
            VFC_SubstitutionInsert.WrapperSubstitution wrap1 = new VFC_SubstitutionInsert.WrapperSubstitution( RowNumber,isDep, isSelected, strSubstId, strsubId,strActn, strOldComref, strQtyO, daSubDate, strComtGB, strgrpSub, srrRes, strNewComref, strnewRg, strQtyN, strGrp, strConbutor, strCoutry, blAt, bloldCR, blQtN, blQtO , blDt, blGrSb, blRs , blnCR, blContrb, blCtry);
            controller.lstWrapperRIs_AfterValidate = new list<VFC_SubstitutionInsert.WrapperSubstitution>();
            controller.lstWrapperRIs_AfterValidate.add(wrap);
            controller.lstWrapperRIs_AfterValidate.add(wrap1);
            controller.insertWithdrReference();
            controller.lstWrapperRIs_AfterValidate[0].OldCommercialReference = '';
            controller.lstWrapperRIs = controller.lstWrapperRIs_AfterValidate;
            controller.displayWithdrReferenceAfterValidation();
            controller.sortWrapperItems(controller.lstWrapperRIs_AfterValidate);
            
            
            String commRef = 'Test';
            controller.keySet = new set<String>();
            controller.keySet.add(commRef);
            controller.checkDuplicate(commRef);
            
            String qty = '12';
            controller.removeSpclChar(qty);
            
            String inputVal = 'ac\n""';
            controller.removeSpclChar(inputVal);
            
            List<String> csvHeaderRow = new list<String>();
            csvHeaderRow.add('hello');
            csvHeaderRow.add('201');
            csvHeaderRow.add('Success');
            controller.validateHeaders(csvHeaderRow);
            /*
            controller.displayCSVEntries();
            controller.displayWithdrReferenceAfterValidation();
            controller.insertWithdrReference();
            controller.docancel();
            */
            //VFC_SubstitutionInsert.WrapperSubstitution wrap = new VFC_SubstitutionInsert.WrapperSubstitution();
            
            controller.nameFile = 'abc';
            controller.RCName = 'aa';
            controller.ImageValue = 'aa';
            controller.poco = 'aa';
            controller.strProdF = 'aa';
            controller.strProductFamily = 'aa';
            
            controller.IsError = true;
            controller.canCreate = true;
            controller.ValidCheck = 'abc';
            controller.NoRCs = true;
            controller.InvalidCommRef = true;
            controller.InvalidCustRes = true;
            controller.isNoCommRef = true;
            
            controller.validCustResRet = true;
            controller.validCustResRep = true;
            controller.isNoCusRes = true;
            controller.isNoQty = true;
            
            controller.intRowNumber = 10;

            Test.stopTest();
        //}
        
    }
    
    static testmethod void parseCSVMethod(){
        
        
        
        ApexPages.StandardSetController controller1;
        //list<User> userList = AP_EllaProjectForecast_TEST.createUsers();
        User newUser = Utils_TestMethods.createStandardUser('yrffrELL12');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        //system.runas(userList[0]){
            Test.startTest();
            list<CS_WithdrawalSubstitutionCSVMap__c> customSetRecs = new list<CS_WithdrawalSubstitutionCSVMap__c>();
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub00', ColumnNumber__c = '0', HeaderName__c = 'Substitution Id', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub01', ColumnNumber__c = '1', HeaderName__c = 'Substitute Id', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub02', ColumnNumber__c = '2', HeaderName__c = 'Action', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub03', ColumnNumber__c = '3', HeaderName__c = 'Old commercial reference', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub04', ColumnNumber__c = '4', HeaderName__c = 'Quantity (old)', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub05', ColumnNumber__c = '5', HeaderName__c = 'Substitution beginning (MM/DD/YY)', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub06', ColumnNumber__c = '6', HeaderName__c = 'Comment (en_GB)', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub07', ColumnNumber__c = '7', HeaderName__c = 'Grouping of substitutions', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub08', ColumnNumber__c = '8', HeaderName__c = 'Substitution reason', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub09', ColumnNumber__c = '9', HeaderName__c = 'New commercial reference', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub10', ColumnNumber__c = '10', HeaderName__c = 'New range', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub11', ColumnNumber__c = '11', HeaderName__c = 'Quantity (new)', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub12', ColumnNumber__c = '12', HeaderName__c = 'Group', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub13', ColumnNumber__c = '13', HeaderName__c = 'Contributor', Object__c = 'Substitution__c'));
        customSetRecs.add(new CS_WithdrawalSubstitutionCSVMap__c(Name = 'Sub14', ColumnNumber__c = '14', HeaderName__c = 'Country', Object__c = 'Substitution__c'));
        
        insert customSetRecs;
            Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
            insert offerRec;
            
            PageReference pg = Page.VFP_SubstitutionInsert;
            Test.setCurrentPage(pg);
            pg.getParameters().put('Id', offerRec.Id);
            // define controller --panggil yg public class, page reference
            VFC_SubstitutionInsert controller = new VFC_SubstitutionInsert(controller1);
            
            //trying to acheiving in one method
            controller.displayCSVEntries();
            controller.displayWithdrReferenceAfterValidation();
            controller.insertWithdrReference();
            controller.docancel();
            
            pg.getParameters().put('Id', '');
            controller.displayCSVEntries();
            controller.displayWithdrReferenceAfterValidation();
            controller.insertWithdrReference();
            controller.docancel();
            
            Product2 prod = new Product2();
            String commercialReference = 'Test'; 
            String strMaterialDescription = 'abc123';
            String businessUnit = 'INDUSTRIAL AUTOMATION';
            String productLine = 'IDMOT - Motion Control';
            String productFamily = 'OLD';
            String family = 'GDP UTILISES DANS GM';
            boolean isMarketingDocumentation = true;
            boolean isOld = true;
            boolean isSparePart = false;
            prod = Utils_TestMethods.createCommercialReference(commercialReference,strMaterialDescription, businessUnit, productLine, productFamily, family,isMarketingDocumentation, isOld, isSparePart);
            insert prod;
            
            Substitution__c substitution = createSubstitution(prod.Id,offerRec.Id);
            insert substitution;
            PageReference pg1 = Page.VFP_SubstitutionInsert;
            Test.setCurrentPage(pg1);
            pg1.getParameters().put('Id', offerRec.Id);
            // define controller --panggil yg public class, page reference
            VFC_SubstitutionInsert controllerNew = new VFC_SubstitutionInsert(controller1);
            //WithoutValues
            String blobValue = 'Substitution Id,Substitute Id,Action,Old commercial reference,Quantity (old),Substitution beginning (MM/DD/YY),Comment (en_GB),Grouping of substitutions,Substitution reason,New commercial reference,New range,Quantity (new),Group,Contributor,Country\r\n';
            blobValue += '1234,1234,create-update,"ProdcutId",2,04/12/15,Test6\rxbtg5230 ,AND,End of life,Test10","Test11, 2,Test13,Test14,India' ;
            Blob bodyBlob=Blob.valueOf(blobValue);
            //bodyBlob =Blob.valueOf('1234,1234,create-update,ProdcutId,2,04/12/15,Test6\rxbtg5230 ,AND,End of life,Test10,Test11, 2,Test13,Test14,India');
            controller.ContentFile= bodyBlob;
            
            //controller.displayCSVEntries();
            
            //WithValues
            Blob bodyBlob1=Blob.valueOf('1234,1234,create-update,ProdcutId,2,04/12/15,Test6\rxbtg5230 ,AND,End of life,Test10,Test11, 2,Test13,Test14,India');
            controller.ContentFile= bodyBlob1;
            controller.displayCSVEntries();
            
            Test.stopTest();
        //}
        
    }
}