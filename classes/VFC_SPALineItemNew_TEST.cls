/*
Author:Siddharth
Purpose:Test Methods for the class VFC_SPALineItemNew
Methods:    Constructor,
            Private void setApprovers (List<SPALineItem> lstSPALineItems)
            Private boolean checkMinimumDealSizeMet ()
            private integer getThresholdLevelForProductLine (List<SPALineItem>lstSPALineItems)
            private void  SetPricingDeskMembers ()
            private void shareSPARecord()
            public List<User> lstApproversForSharing
            

*/
@isTest
private class VFC_SPALineItemNew_TEST
{

    static testMethod void runPositiveTestCases() 
    {

        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
                //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount');        
        lineitem.ProductLineManager__c=UserInfo.getUserId();                
        List<SPAPricingDeskMember__c> members=Utils_SPA.createPricingDeskMembers(sparequest.BackOfficeSystemID__c,lineitem.LocalPricingGroup__c);
        insert members;
        List<SPARequestPricingDeskMember__c> requestpricingdeskmembers=new List<SPARequestPricingDeskMember__c>();
        for(SPAPricingDeskMember__c member:members)        
            requestpricingdeskmembers.add(new SPARequestPricingDeskMember__c(SPAPricingDesk__c=member.id,SPARequest__c=sparequest.id));
            insert requestpricingdeskmembers;
        test.starttest();        
        Map<String,String> RecordtypeMap=new Map<String,Id>();
         for(RecordType rt:[select id,DeveloperName from RecordType where SobjectType='SPARequestLineItem__c'])
        RecordtypeMap.put(rt.DeveloperName,(rt.id+'').substring(0,15));                
        lineitem.RecordTypeId=RecordtypeMap.get('SPA_RequestLineItemWithLocalPricingGroup');                
        ApexPages.StandardController controller=new ApexPages.StandardController(lineitem);        
        VFC_SPALineItemNew controllerinstance=new VFC_SPALineItemNew(controller);
        Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('PosProductPricing'));
        controllerinstance.save();
        controllerinstance.doSaveAndNew();       
        test.stopTest();
    }
        
    static testMethod void runNegativeTestCases() 
    {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount');
        lineitem.ProductLineManager__c=UserInfo.getUserId();                
        List<SPAPricingDeskMember__c> members=Utils_SPA.createPricingDeskMembers(sparequest.BackOfficeSystemID__c,lineitem.LocalPricingGroup__c);
        insert members;
        List<SPARequestPricingDeskMember__c> requestpricingdeskmembers=new List<SPARequestPricingDeskMember__c>();
        for(SPAPricingDeskMember__c member:members)        
            requestpricingdeskmembers.add(new SPARequestPricingDeskMember__c(SPAPricingDesk__c=member.id,SPARequest__c=sparequest.id));
            insert requestpricingdeskmembers;
            
        test.starttest();
        
        Map<String,String> RecordtypeMap=new Map<String,Id>();
         for(RecordType rt:[select id,DeveloperName from RecordType where SobjectType='SPARequestLineItem__c'])
        RecordtypeMap.put(rt.DeveloperName,(rt.id+'').substring(0,15));                
        lineitem.RecordTypeId=RecordtypeMap.get('SPA_RequestLineItemWithLocalPricingGroup'); 
       /* insert lineitem ;
        sparequest.ApprovalStatus__c='Approval in Progress';
        update sparequest;
        PageReference vfPage = Page.VFP_SPALineItemNew;
        Test.setCurrentPage(vfPage);
        ApexPages.currentPage().getParameters().put('id',lineitem.id); */              
        ApexPages.StandardController controller=new ApexPages.StandardController(lineitem);        
        VFC_SPALineItemNew controllerinstance=new VFC_SPALineItemNew(controller);
        Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('NegProductPricing'));
        controllerinstance.save();
        controllerinstance.doSaveAndNew();
        
        test.stopTest();
     }
        
     static testMethod void runPositiveTestCases1() 
     {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        thresholddiscount.LocalPricingGroup__c='test';
        insert thresholddiscount;
        thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount');
        lineitem.ProductLineManager__c=UserInfo.getUserId();                
        List<SPAPricingDeskMember__c> members=Utils_SPA.createPricingDeskMembers(sparequest.BackOfficeSystemID__c,lineitem.LocalPricingGroup__c);
        insert members;
        List<SPARequestPricingDeskMember__c> requestpricingdeskmembers=new List<SPARequestPricingDeskMember__c>();
        for(SPAPricingDeskMember__c member:members)        
            requestpricingdeskmembers.add(new SPARequestPricingDeskMember__c(SPAPricingDesk__c=member.id,SPARequest__c=sparequest.id));
            insert requestpricingdeskmembers;
        
        test.starttest();
    
        Map<String,String> RecordtypeMap=new Map<String,Id>();
        for(RecordType rt:[select id,DeveloperName from RecordType where SobjectType='SPARequestLineItem__c'])
        RecordtypeMap.put(rt.DeveloperName,(rt.id+'').substring(0,15));     
        lineitem.CommercialReference__c='CR1';
        lineitem.RecordTypeId=RecordtypeMap.get('SPA_RequestLineItemwithCommercialReference');    
        ApexPages.StandardController controller1=new ApexPages.StandardController(lineitem);        
        VFC_SPALineItemNew controllerinstance1=new VFC_SPALineItemNew(controller1);
        Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('PosCommReference'));  
        controller1=new ApexPages.StandardController(lineitem);
        controllerinstance1=new VFC_SPALineItemNew(controller1);      
        controllerinstance1.save();
        controllerinstance1.doSaveAndNew();
        
        test.stopTest();
    }
    static testMethod void runNegativeTestCases1() 
    {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        thresholddiscount.LocalPricingGroup__c='test';
        insert thresholddiscount;
        thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount');
        lineitem.ProductLineManager__c=UserInfo.getUserId();                
        List<SPAPricingDeskMember__c> members=Utils_SPA.createPricingDeskMembers(sparequest.BackOfficeSystemID__c,lineitem.LocalPricingGroup__c);
        insert members;
        List<SPARequestPricingDeskMember__c> requestpricingdeskmembers=new List<SPARequestPricingDeskMember__c>();
        for(SPAPricingDeskMember__c member:members)        
            requestpricingdeskmembers.add(new SPARequestPricingDeskMember__c(SPAPricingDesk__c=member.id,SPARequest__c=sparequest.id));
            insert requestpricingdeskmembers;
        
        test.starttest();
        
        Map<String,String> RecordtypeMap=new Map<String,Id>();
        for(RecordType rt:[select id,DeveloperName from RecordType where SobjectType='SPARequestLineItem__c'])
        RecordtypeMap.put(rt.DeveloperName,(rt.id+'').substring(0,15));        
        lineitem.CommercialReference__c='CR1';
        lineitem.RecordTypeId=RecordtypeMap.get('SPA_RequestLineItemwithCommercialReference'); 
        /*insert lineitem ;
        PageReference vfPage1 = Page.VFP_SPALineItemNew;
        Test.setCurrentPage(vfPage1);
        ApexPages.currentPage().getParameters().put('id',lineitem.id);*/
        ApexPages.StandardController controller1=new ApexPages.StandardController(lineitem);          
        VFC_SPALineItemNew controllerinstance1=new VFC_SPALineItemNew(controller1);
        Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('NegCommReference'));  
        controller1=new ApexPages.StandardController(lineitem);
        controllerinstance1=new VFC_SPALineItemNew(controller1);      
        controllerinstance1.save();
        controllerinstance1.doSaveAndNew();
        
        test.stopTest();
    }
        static testMethod void EditCommercialReference() 
    {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount');   
        lineitem.ProductLineManager__c=UserInfo.getUserId();                
        List<SPAPricingDeskMember__c> members=Utils_SPA.createPricingDeskMembers(sparequest.BackOfficeSystemID__c,lineitem.LocalPricingGroup__c);
        insert members;
        List<SPARequestPricingDeskMember__c> requestpricingdeskmembers=new List<SPARequestPricingDeskMember__c>();
        for(SPAPricingDeskMember__c member:members)        
            requestpricingdeskmembers.add(new SPARequestPricingDeskMember__c(SPAPricingDesk__c=member.id,SPARequest__c=sparequest.id));
            insert requestpricingdeskmembers;
        
        test.starttest();
        
        Map<String,String> RecordtypeMap=new Map<String,Id>();
        for(RecordType rt:[select id,DeveloperName from RecordType where SobjectType='SPARequestLineItem__c'])
        RecordtypeMap.put(rt.DeveloperName,(rt.id+'').substring(0,15));        
        lineitem.CommercialReference__c='CR1';
        lineitem.RecordTypeId=RecordtypeMap.get('SPA_RequestLineItemwithCommercialReference'); 
        insert lineitem ;
        PageReference vfPage1 = Page.VFP_SPALineItemNew;
        Test.setCurrentPage(vfPage1);
        ApexPages.currentPage().getParameters().put('id',lineitem.id);
        ApexPages.StandardController controller1=new ApexPages.StandardController(lineitem);          
        VFC_SPALineItemNew controllerinstance1=new VFC_SPALineItemNew(controller1);
        Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('NegCommReference'));  
        controller1=new ApexPages.StandardController(lineitem);
        controllerinstance1=new VFC_SPALineItemNew(controller1);      
        controllerinstance1.save();
        controllerinstance1.doSaveAndNew();
        
        test.stopTest();
    }
        static testMethod void EditLocalPricingGroup() 
    {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;       
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount');     
        lineitem.ProductLineManager__c=UserInfo.getUserId();                
        List<SPAPricingDeskMember__c> members=Utils_SPA.createPricingDeskMembers(sparequest.BackOfficeSystemID__c,lineitem.LocalPricingGroup__c);
        insert members;
        List<SPARequestPricingDeskMember__c> requestpricingdeskmembers=new List<SPARequestPricingDeskMember__c>();
        for(SPAPricingDeskMember__c member:members)        
            requestpricingdeskmembers.add(new SPARequestPricingDeskMember__c(SPAPricingDesk__c=member.id,SPARequest__c=sparequest.id));
            insert requestpricingdeskmembers;
            
        test.starttest();
        
        Map<String,String> RecordtypeMap=new Map<String,Id>();
         for(RecordType rt:[select id,DeveloperName from RecordType where SobjectType='SPARequestLineItem__c'])
        RecordtypeMap.put(rt.DeveloperName,(rt.id+'').substring(0,15));                
        lineitem.RecordTypeId=RecordtypeMap.get('SPA_RequestLineItemWithLocalPricingGroup'); 
        insert lineitem ;
        sparequest.ApprovalStatus__c='Approval in Progress';
        update sparequest;
        PageReference vfPage = Page.VFP_SPALineItemNew;
        Test.setCurrentPage(vfPage);
        ApexPages.currentPage().getParameters().put('id',lineitem.id);              
        ApexPages.StandardController controller=new ApexPages.StandardController(lineitem);        
        VFC_SPALineItemNew controllerinstance=new VFC_SPALineItemNew(controller);
        Test.setMock(WebServiceMock.class, new WS_MOCK_SPA_TEST('NegProductPricing'));
        controllerinstance.save();
        controllerinstance.doSaveAndNew();
        
        test.stopTest();
     }
}