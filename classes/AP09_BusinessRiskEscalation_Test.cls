/*    Author:Anuj Maheshwari   
      Date:25/04/2011.   
      Description:This class tests the AP09_BusinessRiskEscalation class.  
*/
  @isTest  
 private class AP09_BusinessRiskEscalation_Test{
  
    static TestMethod void testingPopulateSponsorAndResolutionLeader(){
   
       System.Debug('****** Testing the Method of Populating Sponsor and resolution Leader ****'); 
         List<User> ListOfUser = new List<User>();
         Id UserRoleID2 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
         Test.startTest(); 
         User newUser = Utils_TestMethods.createStandardUser('TestUser'); 
         newUser.UserRoleID = UserRoleID2;
         ListOfUser.add(newUser);
         
         /*
         Database.SaveResult UserInsertResult = Database.insert(newUser, false);         
         system.debug(' Test 31 :newUser1 : '+ UserInsertResult.isSuccess() );  
         if(!UserInsertResult.isSuccess()){
             system.debug(' Test 31_1 :newUser1 : ');
             Database.Error err = UserInsertResult.getErrors()[0];
         }
         */
         User newUser2 = Utils_TestMethods.createStandardUser('TestUse2');
         newUser2.UserRoleID = UserRoleID2;
         ListOfUser.add(newUser2);
         /*
         Database.SaveResult SecondUserInsertResult = Database.insert(newUser2, false);         
         system.debug(' Test 32 : newUser2 :'+ SecondUserInsertResult.isSuccess() );    
         if(!SecondUserInsertResult.isSuccess()){
             system.debug(' Test 31_2 :newUser2 : ');
             Database.Error err = SecondUserInsertResult.getErrors()[0];        
         
         } 
         */
      system.runAS(newUser){
         Country__c newCountry = Utils_TestMethods.createCountry();
         // CL00320 = EMEAS
          newCountry.Region__c=Label.CL00320;
          insert newCountry;
         /* 
         Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
         system.debug(' Test 32 : newUser2 :'+ SecondUserInsertResult.isSuccess() );   
         if(!CountryInsertResult.isSuccess()){
             system.debug(' Test 32_1 : newUser2 :' );
             Database.Error err = CountryInsertResult.getErrors()[0];
         }
         */
         Account acct = Utils_TestMethods.createAccount();
         // CL00318 = Industry
                 acct.LeadingBusiness__c = Label.CL00318;
                 insert acct;
        /*       
         Database.SaveResult AccountInsertResult = Database.insert(acct, false);
           
         if(!AccountInsertResult.isSuccess()){
             system.debug( ' Test S1a AccountInsertResult.getErrors() ' + AccountInsertResult.getErrors()[0]);
             Database.Error err = AccountInsertResult.getErrors()[0];
             
         }
         */
        /*     
         Account acc = Utils_TestMethods.createAccountWithParentId(acct.Id);
                 //acc.Type = 'GSA';
                 acc.Account_VIPFlag__c = 'Gold';
                 acc.UltiParentAcc__c=acc.id ;
         Database.SaveResult AccountInsertResult1 = Database.insert(acc, false);
         */

        // == sukumar code - start  
        Account acc = Utils_TestMethods.createAccount();
            acc.Type = 'GSA'; 
            acc.Account_VIPFlag__c = 'VIP1';
            acc.UltiParentAcc__c=acct.id ; 
            insert acc;
            AccountTeamMember ca = new AccountTeamMember();
                    ca.AccountId = acct.Id;
                    ca.TeamMemberRole = 'Executive Sponsor';
                    ca.UserId = UserInfo.getUserId() ;
                    insert ca;
        // Database.SaveResult AccountInsertResult1 = Database.insert(acc, false);
        // == sukumar code - end
         /*
         if(!AccountInsertResult1.isSuccess()){
             system.debug( ' Test S1a AccountInsertResult1.isSuccess()' + AccountInsertResult1.isSuccess() );
             Database.Error err = AccountInsertResult1.getErrors()[0];
         }
         */
          system.debug('*********Test s1 *Ultimate Account*****'+acc.UltiParentAcc__c+acc.Id);
       
       // Create Business Risk Escalation Entity
       List< BusinessRiskEscalationEntity__c> bREEList1 = new List< BusinessRiskEscalationEntity__c>();
       List< BusinessRiskEscalationEntity__c> bREEList2 = new List< BusinessRiskEscalationEntity__c>();
       List< BusinessRiskEscalationEntity__c> bREEList = new List< BusinessRiskEscalationEntity__c>();
       BusinessRiskEscalationEntity__c bREE111 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg1', Entity__c = 'TestEntity1', SubEntity__c = '', Location__c = ''); 
       bREEList1.add(bREE111);   
         BusinessRiskEscalationEntity__c bREE12 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg1', Entity__c = 'TestEntity1', SubEntity__c = 'TestSubEntity1', Location__c = ''); 
               bREEList2.add(bREE12); 
         BusinessRiskEscalationEntity__c bREE1 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg1', Entity__c = 'TestEntity1', SubEntity__c = 'TestSubEntity1', Location__c = 'TestLocation1'); 
       bREEList.add(bREE1);        
       BusinessRiskEscalationEntity__c bREE21 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg2', Entity__c = 'TestEntity2', SubEntity__c = '', Location__c = '');
       bREEList1.add(bREE21);
       BusinessRiskEscalationEntity__c bREE22 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg2', Entity__c = 'TestEntity2', SubEntity__c = 'TestSubEntity2', Location__c = '');
       bREEList2.add(bREE22);
       BusinessRiskEscalationEntity__c bREE2 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg2', Entity__c = 'TestEntity2', SubEntity__c = 'TestSubEntity2', Location__c = 'TestLocation2');
       bREEList.add(bREE2);
       BusinessRiskEscalationEntity__c bREE31 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg3', Entity__c = 'TestEntity3', SubEntity__c = '', Location__c = '');
       bREEList1.add(bREE31);
       BusinessRiskEscalationEntity__c bREE32= new BusinessRiskEscalationEntity__c(Name = 'TestOrg3', Entity__c = 'TestEntity3', SubEntity__c = 'TestSubEntity3', Location__c = '');
       bREEList2.add(bREE32);
       BusinessRiskEscalationEntity__c bREE3 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg3', Entity__c = 'TestEntity3', SubEntity__c = 'TestSubEntity3', Location__c = 'TestLocation3');
       bREEList.add(bREE3);
       BusinessRiskEscalationEntity__c bREE41 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg4', Entity__c = 'TestEntity4', SubEntity__c = '', Location__c = '');
       bREEList1.add(bREE41);
       BusinessRiskEscalationEntity__c bREE42 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg4', Entity__c = 'TestEntity4', SubEntity__c = 'TestSubEntity4', Location__c = '');
       bREEList2.add(bREE42);
       BusinessRiskEscalationEntity__c bREE4 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg4', Entity__c = 'TestEntity4', SubEntity__c = 'TestSubEntity4', Location__c = 'TestLocation4');
       bREEList.add(bREE4);
       BusinessRiskEscalationEntity__c bREE51 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg5', Entity__c = 'TestEntity5', SubEntity__c = '', Location__c = '');
       bREEList1.add(bREE51); 
BusinessRiskEscalationEntity__c bREE52 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg5', Entity__c = 'TestEntity5', SubEntity__c = 'TestSubEntity5', Location__c = '');
       bREEList2.add(bREE52);    
BusinessRiskEscalationEntity__c bREE5 = new BusinessRiskEscalationEntity__c(Name = 'TestOrg5', Entity__c = 'TestEntity5', SubEntity__c = 'TestSubEntity5', Location__c = 'TestLocation5');
       bREEList.add(bREE5);   
insert bREEList1;
insert bREEList2;      
       insert bREEList ;
                
          
          
        /*  
          Database.SaveResult[] businessRiskEscalationEntityInsertResult = Database.insert(bREEList, false);
                for(Database.SaveResult sr: businessRiskEscalationEntityInsertResult){
                      if(!sr.isSuccess()){
                          // System.Debug('######## Test s2 AP_09 Test Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                          System.Debug('######## Test s2 AP_09 Test Error Inserting: ');
                          Database.Error err = sr.getErrors()[0]; 
                        }
                }
        */
      // create Entity Stakeholders
        List<EntityStakeholder__c> entityStakeholderList = new List<EntityStakeholder__c>();
            
          EntityStakeholder__c stakeholder1 = Utils_TestMethods.createEntityStakeholder(bREE4.Id,newUser.Id,Label.CL00309);
             entityStakeholderList.add(stakeholder1); 
  
          //EntityStakeholder__c stakeholder2 = Utils_TestMethods.createEntityStakeholder(bREE4.Id,newUser2.Id,Label.CL00310);
          EntityStakeholder__c stakeholder2 = Utils_TestMethods.createEntityStakeholder(bREE4.Id,newUser.Id,Label.CL00310);

          entityStakeholderList.add(stakeholder2);
      
          EntityStakeholder__c stakeholder3 = Utils_TestMethods.createEntityStakeholder(bREE1.Id,newUser2.Id,Label.CL00312);

          entityStakeholderList.add(stakeholder3);
    
          EntityStakeholder__c stakeholder4 = Utils_TestMethods.createEntityStakeholder(bREE1.Id,newUser2.Id,Label.CL00313);             
          entityStakeholderList.add(stakeholder4);

          EntityStakeholder__c stakeholder5 = Utils_TestMethods.createEntityStakeholder(bREE1.Id,newUser2.Id,Label.CL00314);              
          entityStakeholderList.add(stakeholder5);
    
          EntityStakeholder__c stakeholder6 = Utils_TestMethods.createEntityStakeholder(bREE1.Id,newUser2.Id,Label.CL00315);              
          entityStakeholderList.add(stakeholder6);
  
         EntityStakeholder__c stakeholder7 = Utils_TestMethods.createEntityStakeholder(bREE2.Id,newUser.Id,Label.CL00316);
          entityStakeholderList.add(stakeholder7);

         EntityStakeholder__c stakeholder8 = Utils_TestMethods.createEntityStakeholder(bREE3.Id,newUser.Id,Label.CL00317);
          entityStakeholderList.add(stakeholder8);
         /* 
         EntityStakeholder__c stakeholder9 = Utils_TestMethods.createEntityStakeholder(bREE4.Id,newUser.Id,Label.CL00313);
          entityStakeholderList.add(stakeholder9);
          */
         EntityStakeholder__c stakeholder10 = Utils_TestMethods.createEntityStakeholder(bREE5.Id,newUser2.Id,Label.CL00321);
          entityStakeholderList.add(stakeholder10);  
        insert entityStakeholderList;
        /*
 Database.SaveResult[] entityStakeholderInsertResult = Database.insert(entityStakeholderList, false);
                for(Database.SaveResult sr: entityStakeholderInsertResult){
                      if(!sr.isSuccess()){
                         // System.Debug('#######Test s3 # AP_09 Test Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                          System.Debug('#######Test s3 # AP_09 Test Error Inserting: ');
                          Database.Error err = sr.getErrors()[0];
                        }
                } 
           */
      system.debug('****Test s4* stakeholder8*******'+stakeholder8);
      system.debug('*********Test s5 *Ultimate Account*****'+acc.UltiParentAcc__c+acc.Id);
      //ram
     // User newUser1 = Utils_TestMethods.createStandardUser('TestUser');    
       // Database.SaveResult UserInsertResult = Database.insert(newUser1, false);  
       BusinessRiskEscalationEntity__c bREE551= new BusinessRiskEscalationEntity__c(Entity__c ='Test', Location_Type__c='Country Front Office (verticalized)');
          insert bREE551;
       BusinessRiskEscalationEntity__c bREE55= new BusinessRiskEscalationEntity__c(); 
          bREE55.Entity__c ='Test'; 
          bREE55.SubEntity__c= 'Argentina1';
          bREE55.Location_Type__c='Country Front Office (verticalized)';
          insert bREE55;
         BusinessRiskEscalationEntity__c bREE9= new BusinessRiskEscalationEntity__c(); 
          bREE9.Entity__c ='Test'; 
          bREE9.SubEntity__c= 'Argentina1';
          bREE9.Location__c='Loc2';
          bREE9.Location_Type__c='Country Front Office (verticalized)';
          insert bREE9;
         BusinessRiskEscalationEntity__c bREE101= new BusinessRiskEscalationEntity__c(Entity__c ='Test1');
          insert bREE101;
          BusinessRiskEscalationEntity__c bREE10= new BusinessRiskEscalationEntity__c(); 
          bREE10.Entity__c ='Test1'; 
          bREE10.SubEntity__c= 'Argentina11';
          //bREE10.Location__c='Loc22';
          //bREE10.Location_Type__c='Country Front Office (verticalized)';
          insert bREE10;
          BusinessRiskEscalationEntity__c bREE119= new BusinessRiskEscalationEntity__c(Entity__c ='Test11');
          insert bREE119;
          BusinessRiskEscalationEntity__c bREE11= new BusinessRiskEscalationEntity__c(); 
          bREE11.Entity__c ='Test11'; 
          bREE11.SubEntity__c= 'Argentina111';
          insert bREE11;
    
          EntityStakeholder__c stakeholderx = Utils_TestMethods.createEntityStakeholder(bREE9.Id,newUser.Id,Label.CL10036);
          insert stakeholderx;
      //ram
       BusinessRiskEscalations__c brEscalation1 = Utils_TestMethods.createBusinessRiskEscalation(acc.Id,newCountry.Id,newUser.Id);
      brEscalation1.OriginatingOrganisation__c  =bREE9.id;
      brEscalation1.ResolutionOrganisation__c = bREE10.id;
      brEscalation1.Status__c = 'ABC';
      //brEscalation1.AffectedCustomer__r.Type='GSA';//AffectedCustomer__c=acc.id;//'GSA'; ==commented sukumar
        //brEscalation1.add(breE);
        
        /*
        User TstU1 = Utils_TestMethods.createStandardUser('TstU1');         
        Database.SaveResult TstUtResult = Database.insert(newUser, false);  
        */
        BusinessRiskEscalationEntity__c breEntity = new BusinessRiskEscalationEntity__c(Entity__c ='Test11qeyguer');
        Database.insert(breEntity);
      /*
        EntityStakeholder__c stakehlder1 = Utils_TestMethods.createEntityStakeholder(breEntity.Id,newUser.Id,Label.CL00309);
        Database.insert(stakehlder1);
    
        stakehlder1.Role__c = 'CS&Q Manager';
        Database.update(stakehlder1);
       */
        List<Problem__c> prblmList= new List<Problem__c>();
        Problem__c Prblm1 = Utils_TestMethods.createProblem(breEntity.id); 
        prblmList.add(Prblm1);
        Problem__c Prblm2 = Utils_TestMethods.createProblem(breEntity.id); 
        prblmList.add(Prblm2);
        Database.insert(prblmList);
        
        brEscalation1.RelatedProblem__c =Prblm1.id;
                      
        // Database.SaveResult BusinessRiskEscalationInsertResult = Database.insert(brEscalation1, false);
        insert brEscalation1;
        /*
        system.debug('Test 1 BusinessRiskEscalationInsertResult' + BusinessRiskEscalationInsertResult);
        
         
          if(!BusinessRiskEscalationInsertResult.isSuccess())
             Database.Error err = BusinessRiskEscalationInsertResult.getErrors()[0];
        */    
            List<BusinessRiskEscalationStakeholder__c> stakeholderList = new List<BusinessRiskEscalationStakeholder__c>();  
              BusinessRiskEscalations__c testBRE =[Select Id,ResolutionLeader__c,BusinessRiskSponsor__c from   BusinessRiskEscalations__c where Id =: brEscalation1.Id];
             stakeholderList =[Select Id,name,User__c,BusinessRisk__c  from BusinessRiskEscalationStakeholder__c where BusinessRisk__c  =: brEscalation1.Id];
             
             system.assertEquals(testBRE.ResolutionLeader__c,stakeholder2.User__c);
             system.assertEquals(testBRE.BusinessRiskSponsor__c,stakeholder1.User__c);

              brEscalation1.Status__c = 'Closed'; 
              brEscalation1.ActionPlanAgreedWithCustomer__c = 'Done';
              brEscalation1.ClosureCriteria__c = 'Checked';
              brEscalation1.RelatedProblem__c =Prblm2.id;
              brEscalation1.ResolutionOrganisation__c = bREE11.id;
              system.debug('  brEscalation1.GSAAffected__c   ' + brEscalation1.GSAAffected__c);
          Database.SaveResult BusinessRiskEscalationUpdateResult = Database.update(brEscalation1, false);
         
        /*  if(!BusinessRiskEscalationUpdateResult.isSuccess()){
               Database.Error err = BusinessRiskEscalationInsertResult.getErrors()[0];
             }*/
       Test.stopTest();
       
       }
                
    }
        static TestMethod void testPopulateBREStakeHolder(){
        System.Debug('****** Testing the Method of Populating StakeHolder Information ****');  
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator' limit 1][0].UserRoleID;
        Test.startTest(); 
        
        List<User> testUserList = new List<User>();
        User newUser = Utils_TestMethods.createStandardUser('TestUser');
        newUser.UserRoleID = UserRoleID1;
        testUserList.add(newUser);
        User newUser2 = Utils_TestMethods.createStandardUser('TestUse2');
        newUser2.UserRoleID = UserRoleID1;
        testUserList.add(newUser2);
        User newUser3 = Utils_TestMethods.createStandardUser('TestUse3');
        newUser3.UserRoleID = UserRoleID1;      
        testUserList.add(newUser3);
        User newUser4 = Utils_TestMethods.createStandardUser('TestUse4');
        newUser4.UserRoleID = UserRoleID1;      
        testUserList.add(newUser4);
        User newUser5 = Utils_TestMethods.createStandardUser('TestUse5');
        newUser5.UserRoleID = UserRoleID1;      
        testUserList.add(newUser5);
        User newUser6 = Utils_TestMethods.createStandardUser('TestUse6');
        newUser6.UserRoleID = UserRoleID1;      
        testUserList.add(newUser6);
        User newUser7 = Utils_TestMethods.createStandardUser('TestUse7');
        newUser7.UserRoleID = UserRoleID1;      
        testUserList.add(newUser7);
        User newUser8 = Utils_TestMethods.createStandardUser('TestUse8');
        newUser8.UserRoleID = UserRoleID1;      
        testUserList.add(newUser8);
        User newUser9 = Utils_TestMethods.createStandardUser('TestUse9');
        newUser9.UserRoleID = UserRoleID1;      
        testUserList.add(newUser9);
        User newUser10 = Utils_TestMethods.createStandardUser('TestUs10');
        newUser10.UserRoleID = UserRoleID1;     
        testUserList.add(newUser10);
        User newUser11 = Utils_TestMethods.createStandardUser('TestUs11');
        newUser11.UserRoleID = UserRoleID1;     
        testUserList.add(newUser11);
        User newUser12 = Utils_TestMethods.createStandardUser('TestUs12');
        newUser12.UserRoleID = UserRoleID1;     
        testUserList.add(newUser12);
        
       // Database.SaveResult[] UserInsertResult = Database.insert(testUserList, false);  
        insert testUserList;       
        system.debug(' Test s11 UserInsertResult' + testUserList );
        
/*        if(!UserInsertResult[0].isSuccess())
            Database.Error err = UserInsertResult[0].getErrors()[0];
*/      system.runAs(newUser12){
        Account acct = Utils_TestMethods.createAccount();
        acct.LeadingBusiness__c = Label.CL00319;


        // Database.SaveResult AccountInsertResult = Database.insert(acct, false);
        insert acct;
        system.debug(' Test s12 Inserting account : ' + acct);
        /*
        if(!AccountInsertResult.isSuccess())
            Database.Error err = AccountInsertResult.getErrors()[0];
        */
        Account acc = Utils_TestMethods.createAccountWithParentId(acct.Id);
        //acc.Type = 'GSA';
        acc.Account_VIPFlag__c = 'Gold';
        // Database.SaveResult AccountInsertResult1 = Database.insert(acc, false);
        insert acc;
        system.debug(' Test s13 Inserting account with parent ID: ' + acc );
        System.debug('**********Ultimate Account*****'+acc.UltiParentAcc__c+acc.Id);
        
        AccountTeammember oATM = new AccountTeammember(UserId = newUser11.ID,AccountID = acct.ID, TEAMMEMBERROLE = 'ESP' );
        insert oATM;
        // Create Business Risk Escalation Entity
        List< BusinessRiskEscalationEntity__c> bREEList1 = new List< BusinessRiskEscalationEntity__c>();
          List< BusinessRiskEscalationEntity__c> bREEList = new List< BusinessRiskEscalationEntity__c>();

        // BusinessRiskEscalationEntity__c bREE1 =Utils_TestMethods.createBRE(Label.CL00319,Null,Null,Null);
        BusinessRiskEscalationEntity__c bREE1 =Utils_TestMethods.createBRE('AprilTestOrg1',Null,Null,Null);
        bREEList.add(bREE1);

        // BusinessRiskEscalationEntity__c bREE2 = Utils_TestMethods.createBRE(Label.CL00319,'Low Voltage',Null,Null);
        BusinessRiskEscalationEntity__c bREE21 = Utils_TestMethods.createBRE('AprilTestOrg2',null,Null,Null);
        bREEList1.add(bREE21);
        BusinessRiskEscalationEntity__c bREE2 = Utils_TestMethods.createBRE('AprilTestOrg2','Low Voltage',Null,Null);
        bREEList.add(bREE2);

        // BusinessRiskEscalationEntity__c bREE3 = Utils_TestMethods.createBRE(Label.CL00318,'Low Voltage','France',Label.CL10057);       
        BusinessRiskEscalationEntity__c bREE31 = Utils_TestMethods.createBRE('AprilTestOrg3',null,null,Label.CL10057);   
        bREEList1.add(bREE31);
        BusinessRiskEscalationEntity__c bREE32 = Utils_TestMethods.createBRE('AprilTestOrg3','Low Voltage',null,Label.CL10057);   
        //bREEList1.add(bREE32);
        BusinessRiskEscalationEntity__c bREE3 = Utils_TestMethods.createBRE('AprilTestOrg3','Low Voltage','France',Label.CL10057);   
        bREEList.add(bREE3);
    
        // Database.SaveResult[] businessRiskEscalationEntityInsertResult = Database.insert(bREEList, false);
        insert bREEList1;
        insert bREE32;
        insert bREEList;
        /*
        for(Database.SaveResult sr: businessRiskEscalationEntityInsertResult){
            if(!sr.isSuccess()){
                Database.Error err = sr.getErrors()[0];
                System.Debug('######## Test s13 AP_09 Test Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
            }
        }
        */
        
        // create Entity Stakeholders
        List<EntityStakeholder__c> entityStakeholderList = new List<EntityStakeholder__c>();

        EntityStakeholder__c stakeholder1 = Utils_TestMethods.createEntityStakeholder(bREE1.Id,newUser.Id,Label.CL10036);
        entityStakeholderList.add(stakeholder1); 

        EntityStakeholder__c stakeholder2 = Utils_TestMethods.createEntityStakeholder(bREE1.Id,newUser2.Id,Label.CL10038);
        entityStakeholderList.add(stakeholder2);

        EntityStakeholder__c stakeholder3 = Utils_TestMethods.createEntityStakeholder(bREE2.Id,newUser3.Id,Label.CL10036);
        entityStakeholderList.add(stakeholder3);

        EntityStakeholder__c stakeholder4 = Utils_TestMethods.createEntityStakeholder(bREE2.Id,newUser4.Id,Label.CL10046);             
        entityStakeholderList.add(stakeholder4);

        EntityStakeholder__c stakeholder5 = Utils_TestMethods.createEntityStakeholder(bREE2.Id,newUser5.Id,Label.CL10041);              
        entityStakeholderList.add(stakeholder5);

        EntityStakeholder__c stakeholder6 = Utils_TestMethods.createEntityStakeholder(bREE2.Id,newUser6.Id,Label.CL00314);              
        entityStakeholderList.add(stakeholder6);

        EntityStakeholder__c stakeholder7 = Utils_TestMethods.createEntityStakeholder(bREE2.Id,newUser7.Id,Label.CL10040);
        entityStakeholderList.add(stakeholder7);

        EntityStakeholder__c stakeholder8 = Utils_TestMethods.createEntityStakeholder(bREE3.Id,newUser8.Id,Label.CL10035);
        entityStakeholderList.add(stakeholder8);

        EntityStakeholder__c stakeholder9 = Utils_TestMethods.createEntityStakeholder(bREE3.Id,newUser9.Id,Label.CL10037);
        entityStakeholderList.add(stakeholder9);

        EntityStakeholder__c stakeholder10 = Utils_TestMethods.createEntityStakeholder(bREE3.Id,newUser10.Id,Label.CL10036);
        entityStakeholderList.add(stakeholder10);

        EntityStakeholder__c stakeholder11 = Utils_TestMethods.createEntityStakeholder(bREE2.Id,newUser11.Id,Label.CL10034);
        entityStakeholderList.add(stakeholder11);

        EntityStakeholder__c stakeholder12 = Utils_TestMethods.createEntityStakeholder(bREE2.Id,newUser12.Id,Label.CL10033);
        entityStakeholderList.add(stakeholder12);


        Database.SaveResult[] entityStakeholderInsertResult = Database.insert(entityStakeholderList, false);
        for(Database.SaveResult sr: entityStakeholderInsertResult){
            if(!sr.isSuccess()){
                Database.Error err = sr.getErrors()[0];
                System.Debug('######## AP_09 Test Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
            }
        } 

        System.debug('*****stakeholder8*******'+stakeholder8);
        System.debug('**********Ultimate Account*****'+acc.UltiParentAcc__c+acc.Id);
        System.RunAs(newUser) {
        BusinessRiskEscalations__c brEscalation1 = Utils_TestMethods.createBusinessRiskEscalation(acc.Id,null,newUser.Id);

        brEscalation1.ResolutionOrganisation__c = bREE2.id;
        brEscalation1.OriginatingOrganisation__c = bREE3.id;
        brEscalation1.AffectedCustomer__c=acc.id;
        insert brEscalation1;
       // Database.SaveResult BusinessRiskEscalationInsertResult = Database.insert(brEscalation1, false);
         List<BusinessRiskEscalations__c> testlist = new List<BusinessRiskEscalations__c>();
        //testlist.add(breEntity );
         testlist.add(brEscalation1);
        AP09_BusinessRiskEscalation.populateExecutiveSponsor(testlist);
        
        BusinessRiskEscalationEntity__c bREExx1= new BusinessRiskEscalationEntity__c(Entity__c ='April Test Test 1', Location_Type__c='Country Front Office (verticalized)');
          insert bREExx1;
          BusinessRiskEscalationEntity__c bREExx2= new BusinessRiskEscalationEntity__c(Entity__c ='April Test Test 1', SubEntity__c= ' April Argentina1',Location_Type__c='Country Front Office (verticalized)');
          insert bREExx2;
        BusinessRiskEscalationEntity__c bREExx= new BusinessRiskEscalationEntity__c(); 
          bREExx.Entity__c ='April Test Test 1'; 
          bREExx.SubEntity__c= ' April Argentina1';
          bREExx.Location__c='April Loc2';
          bREExx.Location_Type__c='Country Front Office (verticalized)';
          insert bREExx;
    system.debug('Test 389 bREExx' + bREExx + ' bREExx.Entity__c' + bREExx.Entity__c + ' bREExx.SubEntity__c '+ bREExx.SubEntity__c);
            
          BusinessRiskEscalationEntity__c bREEyy1= new BusinessRiskEscalationEntity__c(Entity__c = 'April Power Global 1', Location_Type__c='Country Front Office (verticalized)');
          insert bREEyy1;
          
          BusinessRiskEscalationEntity__c bREEyy= new BusinessRiskEscalationEntity__c(); 
          bREEyy.Entity__c = 'April Power Global 1';
          bREEyy.SubEntity__c='April Low Voltage 1';
          bREEyy.Location__c='';
          bREEyy.Location_Type__c='Country Front Office (verticalized)';
          insert bREEyy;
          Country__c newCountry = Utils_TestMethods.createCountry();
               newCountry.Region__c=Label.CL00320;
              // newCountry.Region__c= 'Country 1';
             insert newCountry;
          BusinessRiskEscalations__c brEscalation2 = Utils_TestMethods.createBusinessRiskEscalation(acc.Id,newCountry.id,newUser2.Id);
           brEscalation2.OriginatingOrganisation__c   =bREExx.id;
           brEscalation2.ResolutionOrganisation__c    =bREEyy.id;
              brEscalation2.AffectedCustomer__c= acc.id;
              
           insert brEscalation2;
           system.debug(' test 403 brEscalation2 : ' + brEscalation2);
           
system.debug(' test 404 brEscalation2 : ' + brEscalation2.TECH_OriginatingEntity__c+' - '+brEscalation2.TECH_OriginatingSubEntity__c);
           
           List<BusinessRiskEscalations__c> testlist1 = new List<BusinessRiskEscalations__c>();
        //testlist.add(breEntity );
         testlist1.add(brEscalation2);
           
           // AP09_BusinessRiskEscalation.createInvolvedOrganization(testlist1 );
        
        Test.stopTest();
        }
    }
    }   

}