@IsTest
public class VFC_CaseUpdateWithFedSearchResults_TEST {
    
    @IsTest
    static void testUpdateCase() 
    {
        Account Acc = Utils_TestMethods.createAccount();
        Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'Jean Dupont');
        
        insert Acc;
        insert Ctct;
        
        Case Cse1 = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
        Insert Cse1;
        Cse1.AnswertoCustomer__c = 'Answer';
        Update Cse1;
        
        InquiraFAQ__c Inq = new InquiraFAQ__c();
        Inq.Title__c = 'Test1';
        Inq.Excerpt__c = 'Test1';
        Inq.URL__c = 'Test1';
        Inq.Case__c = Cse1.Id;
        Inq.Visibility__c = 'Public';
        Inq.SourceSystem__c = 'Other';
        Inq.ObjectType__c = 'Other';
        Insert(Inq);
        
        PageReference pageRef = Page.VFP_CaseUpdateWithFedSearchResults;
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(Cse1);
        VFC_CaseUpdateWithFedSearchResults controller = new VFC_CaseUpdateWithFedSearchResults(sc);
        String nextPage = controller.updateCase().getUrl();
        
        System.assert(nextPage.contains('/'+Cse1.Id));
        
        Case Cse2 = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
        Insert Cse2;
        Cse2.CustomerRequest__c = 'Detailed Customer Request';
        Cse2.AnswertoCustomer__c = '';
        for (Integer i=0; i<31999; i++){
            Cse2.AnswertoCustomer__c += '1';
        } 
        Update Cse2;
        
        Inq = new InquiraFAQ__c();
        Inq.Title__c = 'Test1';
        Inq.Excerpt__c = 'Test1';
        Inq.URL__c = 'Test1';
        Inq.Case__c = Cse2.Id;
        Inq.Visibility__c = 'Public';
        Inq.SourceSystem__c = 'Other';
        Inq.SourceSystem__c = 'Other';
        Insert(Inq);
        
        sc = new ApexPages.StandardController(Cse2);
        controller = new VFC_CaseUpdateWithFedSearchResults(sc);
        System.assertEquals(null,controller.updateCase());
    }
}