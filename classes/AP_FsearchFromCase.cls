/*
 * Author:Emerick
 * Purpose: Canvas supporter class for Federated search
 * */
public class AP_FsearchFromCase implements Canvas.CanvasLifecycleHandler{
    public Set<Canvas.ContextTypeEnum> excludeContextTypes(){
        Set<Canvas.ContextTypeEnum> excluded = new Set<Canvas.ContextTypeEnum>();        
        return excluded;
    }
    
    public void onRender(Canvas.RenderContext renderContext){      
        Canvas.ApplicationContext app = renderContext.getApplicationContext();
        Canvas.EnvironmentContext env = renderContext.getEnvironmentContext();    
        Map<String, Object> currentParams = (Map<String, Object>) JSON.deserializeUntyped(env.getParametersAsJSON());                        
        String caseId = (String) currentParams.get('caseId');                
        String caseSubject = '';

        if( caseId == null || caseId.equals('')){
            app.setCanvasUrlPath('/customermatrix/360g.jsp?infospot=TabInfospot');
        }
        else{
            // old version :
            // caseSubject  = EncodingUtil.urlEncode([SELECT Id,Subject FROM Case WHERE Id = :caseId].Subject , 'UTF-8');

            // new version :
            caseSubject = escapeSafely([SELECT Id,Subject FROM Case WHERE Id = :caseId].Subject);
            caseSubject  = EncodingUtil.urlEncode(EncodingUtil.urlEncode(caseSubject , 'UTF-8'),'UTF-8');

            app.setCanvasUrlPath('/customermatrix/360g.jsp?infospot=Sch_infospot#!/search?pql=query('+caseSubject+')');
                        
        }        
    }
    
    public static String escapeSafely(String value) {
       Matcher matcher = escapingPattern.matcher(value);
       if (matcher.find()) {
         value = matcher.replaceAll('\\\\$1');
       }
       return value;
    }
 
    private static final Pattern escapingPattern = Pattern.compile('(?<!\\\\)([\\{\\}\\(\\),])');
}