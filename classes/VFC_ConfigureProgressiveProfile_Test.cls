@isTest
public class VFC_ConfigureProgressiveProfile_Test {
    
    static TestMethod void UnitTest() {
        PRMProfileConfig__c pconfig = new PRMProfileConfig__c ();
        List<CS_PRM_ProgressiveProfileConfig__c > cs_PrgProf = new List<CS_PRM_ProgressiveProfileConfig__c >();
        CS_PRM_ProgressiveProfileConfig__c c1 = new CS_PRM_ProgressiveProfileConfig__c(Name='annualSales',FieldDataType__c='Picklist',FieldLabel__c='Your Account: Total Annual Sales',FieldSortOrder__c=29,MappedField__c='PRMAnnualSales__c',MappedObject__c='Account',CustomLabel__C='CLAPR15PRM088');
        CS_PRM_ProgressiveProfileConfig__c c2 = new CS_PRM_ProgressiveProfileConfig__c (Name='cTaxId',FieldDataType__c='Text',FieldLabel__c='Contact: Your Tax Id',FieldSortOrder__c=9,MappedField__c='PRMTaxId__c',MappedObject__c='Contact',CustomLabel__C='CLJUL15PRM028');
        CS_PRM_ProgressiveProfileConfig__c c3 = new CS_PRM_ProgressiveProfileConfig__c (Name='phoneNumber',FieldDataType__c='Text',FieldLabel__c='  Contact: Phone Number',FieldSortOrder__c=6,MappedField__c='PRMMobilePhone__c',MappedObject__c='Contact',CustomLabel__C='CLAPR15PRM007');
        CS_PRM_ProgressiveProfileConfig__c c4 = new CS_PRM_ProgressiveProfileConfig__c (Name='companyWebsite',FieldDataType__c='Text',FieldLabel__c='Your Account: Company Website',FieldSortOrder__c=14,MappedField__c='PRMWebsite__c',MappedObject__c='Account',CustomLabel__C='CLAPR15PRM038');
        CS_PRM_ProgressiveProfileConfig__c c5 = new CS_PRM_ProgressiveProfileConfig__c (Name='jobFunction',FieldDataType__c='Picklist',FieldLabel__c='  Contact: Job Function',FieldSortOrder__c=7,MappedField__c='PRMJobFunc__c',MappedObject__c='Contact',CustomLabel__C='CLAPR15PRM009');
        cs_PrgProf.add(c1);
        cs_PrgProf.add(c2);
        cs_PrgProf.add(c3);
        cs_PrgProf.add(c4);
        cs_PrgProf.add(c5);
        insert cs_PrgProf;
        
        Country__c country= Utils_TestMethods.createCountry();
        insert country;
        
        PRMCountry__c prmCountry = new PRMCountry__c();
        prmCountry.Name = 'Test Class PRMCountry';
        prmCountry.Country__c = country.Id;
        prmCountry.PLDatapool__c = 'DataPool';
        prmCountry.CountryPortalEnabled__c = true;
        insert prmCountry;
        
        PageReference pageRef = Page.VFP_ConfigureProgressiveProfile;
        Test.setCurrentPage(pageRef);
        Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(pconfig);
        ApexPages.currentPage().getParameters().put('cntryId',prmCountry.id);
        ApexPages.currentPage().getParameters().put('from','cntry');
        VFC_ConfigureProgressiveProfile thisConfigureProgProfile = new VFC_ConfigureProgressiveProfile(thisController);
        thisConfigureProgProfile.insertRow();
        thisConfigureProgProfile.getpicklistValues();
        thisConfigureProgProfile.Cancel();
        List<VFC_ConfigureProgressiveProfile.ccFPWrapperClass> wrapList= new List<VFC_ConfigureProgressiveProfile.ccFPWrapperClass>();
        VFC_ConfigureProgressiveProfile.ccFPWrapperClass wrap1 = new VFC_ConfigureProgressiveProfile.ccFPWrapperClass();
        wrap1.objField = 'PRMAnnualSales__c';
        wrap1.Mandatory = true;
        wrap1.objFieldValue = 'Account';
        wrapList.add(wrap1);
        VFC_ConfigureProgressiveProfile.ccFPWrapperClass wrap2 = new VFC_ConfigureProgressiveProfile.ccFPWrapperClass();
        wrap2.objField = 'PRMWebsite__c';
        wrap2.Mandatory = true;
        wrap2.objFieldValue = 'Account';
        wrapList.add(wrap2);
        thisConfigureProgProfile.ccpWrapLst = wrapList;
        thisConfigureProgProfile.counter = 0;
        thisConfigureProgProfile.removeRow();
        thisConfigureProgProfile.SaveProfileConfig();
        
        pconfig.Name='Test Profile Config';
        pconfig.PRMCountryClusterSetup__c = prmCountry.Id;
        insert pconfig;
        
        PRMProfileConfigInfo__c pinfo = new PRMProfileConfigInfo__c();
        pinfo.FieldAPIName__c = 'PRMAnnualSales__c';
        pinfo.Mandatory__c = TRUE;
        pinfo.ObjectAPIName__c = 'Account';
        pinfo.PRMProfileConfig__c = pconfig.Id;
        insert pinfo;
        
        Apexpages.Standardcontroller thisController1 = new Apexpages.Standardcontroller(pconfig);
        ApexPages.currentPage().getParameters().put('cntryId',prmCountry.id);
        ApexPages.currentPage().getParameters().put('from','Manage');
        ApexPages.currentPage().getParameters().put('prmConfigId',pconfig.Id);
        VFC_ConfigureProgressiveProfile thisConfigureProgProfile1 = new VFC_ConfigureProgressiveProfile(thisController1);
        List<VFC_ConfigureProgressiveProfile.ccFPWrapperClass> DelwrapList1= new List<VFC_ConfigureProgressiveProfile.ccFPWrapperClass>();
        VFC_ConfigureProgressiveProfile.ccFPWrapperClass wrap3 = new VFC_ConfigureProgressiveProfile.ccFPWrapperClass();
        wrap3.objField = 'PRMAnnualSales__c';
        wrap3.Mandatory = true;
        wrap3.objFieldValue = 'Account';
        wrap3.prmConfigInfoId = pinfo.Id;
        DelwrapList1.add(wrap3);
        thisConfigureProgProfile1.ccpWrapDeleteLst = DelwrapList1;
        thisConfigureProgProfile1.SaveProfileConfig();
        
    }
}