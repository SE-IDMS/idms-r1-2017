/**************************************************************************************
    Author: Fielo Team
    Date: 30/06/2015
***************************************************************************************/
@isTest
private class FieloPRM_AP_MemberSegmentTriggerTest{
  
    public static testMethod void testUnit1(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            /*Contact con = new Contact();
            con.AccountId = acc.Id;
            con.FirstName = 'test contact Fn';
            con.LastName  = 'test contact Ln';
            con.PRMCompanyInfoSkippedAtReg__c = false;
            con.PRMCountry__c = country.Id;
            insert con;*/

            FieloEE__Member__c member = FieloEE.MockUpFactory.createMember('Test member', '1234', 'DNI');
            //member.PRMUIMSId__c = 'asdqwe';
            //update member;
            Contact con = [SELECT id, PRMUIMSId__c  FROM Contact WHERE FieloEE__Member__c = : member.id limit 1 ];
            con.PRMUIMSId__c = 'asdqwe';
            update con;

            FieloPRM_Feature__c feature1 = new FieloPRM_Feature__c(
                F_PRM_FeatureCode__c = 'test1'
            );
            
            FieloPRM_Feature__c feature2 = new FieloPRM_Feature__c(
                F_PRM_FeatureCode__c = 'test2'
            );
            insert new List<FieloPRM_Feature__c>{feature1,feature2};
            
            FieloPRM_MemberFeature__c memberFeature1 = new FieloPRM_MemberFeature__c(
                F_PRM_IsActive__c = true,
                F_PRM_Member__c = member.Id,
                F_PRM_Feature__c = feature1.Id,
                PRMUIMSId__c = '703b33fe-64e4-4142-96a8-bd3d5ed4afd3'
            );
            insert memberFeature1;
            
            RecordType rt = [SELECT Id FROM RecordType WHERE Name = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'];
            
            FieloEE__RedemptionRule__c segment1 = new FieloEE__RedemptionRule__c(           
                Name = 'Test Segment1',
                FieloEE__isActive__c = true,
                RecordTypeId = rt.Id,
                F_PRM_SegmentFilter__c =  'se' + datetime.now(),
                F_PRM_Feature__c = feature1.Id
            );          

            FieloEE__RedemptionRule__c segment2 = new FieloEE__RedemptionRule__c(           
                Name = 'Test Segment2',
                FieloEE__isActive__c = true,
                RecordTypeId = rt.Id,
                F_PRM_SegmentFilter__c = 'se' +  datetime.now(),
                F_PRM_Feature__c = feature2.Id
            );
            
            insert new List<FieloEE__RedemptionRule__c>{segment1,segment2};

            test.startTest();
            
            FieloEE__MemberSegment__c memberSegment = new FieloEE__MemberSegment__c(
                FieloEE__Member2__c = member.Id,
                FieloEE__Segment2__c = segment1.Id           
            );

            FieloEE__MemberSegment__c memberSegment2 = new FieloEE__MemberSegment__c(
                FieloEE__Member2__c = member.Id,
                FieloEE__Segment2__c = segment2.Id           
            );


            insert new list<FieloEE__MemberSegment__c>{memberSegment,memberSegment2};
            
            
            delete memberSegment;
            
            test.stopTest();
        }
    
    }
}