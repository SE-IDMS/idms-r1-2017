public with sharing class VFC_EscalationDeEscalationForCase {
    public Case currentCase;
    public boolean blnEscalation;
    
    private static final  String PrimaryExpertise  = 'Primary';
    private static final  String AdvancedExpertise = 'Advanced';
    private static final  String ExpertExpertise   = 'Expert';

    private static final  String strPrimaryQueueId  = Label.CLOCT13CCC15;
    private static final  String strAdvancedQueueId = Label.CLOCT13CCC16;
    private static final  String strExpertQueueId   = Label.CLOCT13CCC17;
    
    
    public VFC_EscalationDeEscalationForCase(ApexPages.StandardController controller) {
        String strEscalation    = System.currentPageReference().getParameters().get('escalation'); // Get escalation paramater
        if(strEscalation.trim().equals('1'))
            blnEscalation    = true;
        else
            blnEscalation    = false;
        currentCase     =  (Case)controller.getRecord();
        currentCase      = [Select Id, Status, Team__c, LevelOfExpertise__c, PrimaryLastCaseTeam__c , ExpertLastCaseTeam__c , AdvancedLastCaseTeam__c, OwnerID  from Case where Id=:currentCase.Id limit 1];
        
    }
    public PageReference returnToCase() {
        PageReference returnPage = new PageReference('/' + currentCase.Id);
        return returnPage;
    }
    public PageReference performAction() {
        PageReference ResultPage;
        if(currentCase.LevelOfExpertise__c==null || currentCase.LevelOfExpertise__c.trim()=='')
            currentCase.LevelOfExpertise__c = PrimaryExpertise;
        if(blnEscalation){
            //if(currentCase.LevelOfExpertise__c.equalsIgnoreCase(PrimaryExpertise)){
            //    currentCase.OwnerID  = strAdvancedQueueId;
            //    currentCase.Status   = System.Label.CL00341;
            //}
            //else 
            if(currentCase.LevelOfExpertise__c.equalsIgnoreCase(AdvancedExpertise)){
                currentCase.OwnerID  = strExpertQueueId;
                currentCase.Status   = System.Label.CL00341;
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Case cannot be Escalated')); 
                return null;
            }   
        }
        else{
            if(currentCase.LevelOfExpertise__c.equalsIgnoreCase(AdvancedExpertise)){
                if(currentCase.PrimaryLastCaseTeam__c !=null){
                    //currentCase.OwnerID  = strPrimaryQueueId;
                    currentCase.Team__c  = currentCase.PrimaryLastCaseTeam__c;
                    currentCase.Status   = System.Label.CL00341; 
                }
                else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'No primary team is defined')); 
                	return null;
                }
            }
            else if(currentCase.LevelOfExpertise__c.equalsIgnoreCase(ExpertExpertise)){
                if(currentCase.AdvancedLastCaseTeam__c !=null){
                    //currentCase.OwnerID  = strAdvancedQueueId;
                    currentCase.Team__c  = currentCase.AdvancedLastCaseTeam__c;
                    currentCase.Status   = System.Label.CL00341;
            	}
                else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'No advanced team is defined')); 
                	return null;
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Case cannot be De-Escalated')); 
                return null;
            }
        }
        Database.SaveResult caseUpdateResult = Database.update(currentCase,false);
            if(!caseUpdateResult.isSuccess()){
                Database.Error DMLerror = caseUpdateResult.getErrors()[0];
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLerror.getMessage()));
                return null;     
            }
            else{
                ResultPage = new PageReference('/' + currentCase.Id);
            }
        return ResultPage;
    }
}