/********************************************************************
* Company: Fielo
* Created Date: 08/08/2016
* Description: 
********************************************************************/
@isTest
private class FieloPRM_Batch_LevelTeamMembersTest {

    public testMethod static void testUnit() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
            update program;

            Account acc = new Account(Name = 'acc');
            insert acc;
            Account acc2 = new Account(Name = 'acc2');
            insert acc2;

            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc.Id});
            //run again, so checks that it exists a team with this account and return
            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc.Id});

            FieloEE__Member__c member = new FieloEE__Member__c(FieloEE__LastName__c= 'Polo', FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()), FieloEE__Street__c = 'test', F_Account__c = acc.Id);
            insert member;

            member.F_Account__c = acc2.Id;
            update member;

            delete member;

            member = new FieloEE__Member__c(FieloEE__Program__c = program.id, FieloEE__LastName__c= 'Polo', FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()), FieloEE__Street__c = 'test', F_Account__c = acc.Id);
            insert member;
            
            test.starttest();
                                        
            Contact c = new contact();
            c.firstname = 'test';
            c.lastname = 'test2';
            c.accountId = acc.Id;
            c.FieloEE__Member__c = member.id; 
            insert c;
            
            FieloCH__Team__c team = new FieloCH__Team__c(Name = FieloPRM_Utils_ChTeam.TEAM_NAME_PREFIX + acc.Name , FieloCH__Program__c = Program.Id); //, F_PRM_Account__c = acc.Id
            insert team;

            FieloCH__TeamMember__c tm = new FieloCH__TeamMember__c(FieloCH__Team2__c = team.Id, FieloCH__Member2__c = member.Id, F_PRM_Status__c = 'PENDING');
            insert tm;

            FieloPRM_Utils_ChTeam.updateTeams(new List<Id>{acc.Id});
            
            /*List<FieloCH__TeamMember__c> tMembers = [SELECT Id FROM FieloCH__TeamMember__c];
            for (FieloCH__TeamMember__c tMemb: tMembers){
                tMemb.F_PRM_Status__c = 'PENDING';
            }
            update tMembers;*/
            
            CS_PRM_ApexJobSettings__c apexJob = new CS_PRM_ApexJobSettings__c(
                Name = 'FieloPRM_Batch_LevelTeamMembers',
                F_PRM_BatchSize__c = 200
            );
            insert apexJob;
            
            Id batchJobId = Database.executeBatch(new FieloPRM_Batch_LevelTeamMembers());
                        
            FieloPRM_Batch_LevelTeamMembers bat = new FieloPRM_Batch_LevelTeamMembers();
            bat.execute(null);
            
            FieloPRM_Batch_LevelTeamMembers.scheduleNow();
            
            test.stoptest();
            
        }
    }

}