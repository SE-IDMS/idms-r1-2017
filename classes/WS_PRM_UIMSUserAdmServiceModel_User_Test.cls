@isTest
public class WS_PRM_UIMSUserAdmServiceModel_User_Test {

    public static testMethod void testModel1() {
        WS_PRM_UIMSUserAdmServiceModel_User.RequestedEntryNotExistsException renee = new WS_PRM_UIMSUserAdmServiceModel_User.RequestedEntryNotExistsException();
        WS_PRM_UIMSUserAdmServiceModel_User.UnexpectedLdapResponseException   ulre = new WS_PRM_UIMSUserAdmServiceModel_User.UnexpectedLdapResponseException();
        WS_PRM_UIMSUserAdmServiceModel_User.getUserListResponse               gulr = new WS_PRM_UIMSUserAdmServiceModel_User.getUserListResponse();
        WS_PRM_UIMSUserAdmServiceModel_User.getUserList                        gul = new WS_PRM_UIMSUserAdmServiceModel_User.getUserList();        
        WS_PRM_UIMSUserAdmServiceModel_User.RequestedInternalUserException     riue = new WS_PRM_UIMSUserAdmServiceModel_User.RequestedInternalUserException();
        WS_PRM_UIMSUserAdmServiceModel_User.rejectUsers                        ru = new WS_PRM_UIMSUserAdmServiceModel_User.rejectUsers();
        WS_PRM_UIMSUserAdmServiceModel_User.identityWithStatus                 iws = new WS_PRM_UIMSUserAdmServiceModel_User.identityWithStatus();
        WS_PRM_UIMSUserAdmServiceModel_User.identity                           iden = new WS_PRM_UIMSUserAdmServiceModel_User.identity();
        WS_PRM_UIMSUserAdmServiceModel_User.IMSServiceSecurityCallNotAllowedException isscnae = new WS_PRM_UIMSUserAdmServiceModel_User.IMSServiceSecurityCallNotAllowedException();
        WS_PRM_UIMSUserAdmServiceModel_User.getUserListByInvitationUidResponse        gulbiur = new WS_PRM_UIMSUserAdmServiceModel_User.getUserListByInvitationUidResponse();
        WS_PRM_UIMSUserAdmServiceModel_User.LdapTemplateNotReadyException               ltnre = new WS_PRM_UIMSUserAdmServiceModel_User.LdapTemplateNotReadyException();
        WS_PRM_UIMSUserAdmServiceModel_User.getUserListByInvitationUid                 gulbiu = new WS_PRM_UIMSUserAdmServiceModel_User.getUserListByInvitationUid();
        WS_PRM_UIMSUserAdmServiceModel_User.SecuredImsException                           sie = new WS_PRM_UIMSUserAdmServiceModel_User.SecuredImsException();
        WS_PRM_UIMSUserAdmServiceModel_User.InvalidImsServiceMethodArgumentException iismae = new WS_PRM_UIMSUserAdmServiceModel_User.InvalidImsServiceMethodArgumentException();
    }
    
    public static testMethod void WS_PRM_UIMSUserAdmService_User_test(){
        
        WS_PRM_UIMSUserAdmService_User.UserAdministration_UIMSV2_ImplPort uimsPort = new WS_PRM_UIMSUserAdmService_User.UserAdministration_UIMSV2_ImplPort();
        uimsPort.endpoint_x       = System.Label.CLMAR16PRM052;
        uimsPort.clientCertName_x = System.label.CLAPR15PRM018;
        uimsPort.timeout_x        = Integer.valueOf(System.label.CLAPR15PRM021);
    
        String callerFid = System.label.CLAPR15PRM019;
        Boolean status = True;
        Boolean isTrustedAdmin = True;
        Boolean isPrimaryContact = True;
        
        Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());        
        WS_PRM_UIMSUserAdmserviceModel_User.identityWithStatus[] uimsResult = uimsPort.getUserList(callerFid, 'samlAssertion', 'companyId', True);
        WS_PRM_UIMSUserAdmserviceModel_User.identity[] uimsResult1 = uimsPort.getUserListByInvitationUid(callerFid, 'samlAssertion','invitationUid', isTrustedAdmin , isPrimaryContact);
           
    
    
    
    }

}