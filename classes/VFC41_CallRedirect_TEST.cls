/*
    Author          : ACN 
    Date Created    : 20/07/2011
    Description     : Test class for VFC41_CallRedirect class 
*/
@isTest
private class VFC41_CallRedirect_TEST 
{

    static testMethod void testlogACallRedirect() 
    {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
         Lead lead4;
         ApexPages.StandardController sc;
         VFC41_CallRedirect callRedirect;
         User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC41');        
         system.runas(user)
         {
            test.startTest();
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
            insert lead1;
            
            Lead lead2 = Utils_TestMethods.createLead(null, country.Id,100);
            insert lead2;
            
            lead1 = [select Status, subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c from lead where Id =: lead1.Id limit 1];
            System.assertEquals(lead1.CallBackLimit__c, 100);
            
            sc = new ApexPages.StandardController(lead1);
            callRedirect = new VFC41_CallRedirect(sc);
            callRedirect.schACallRedirect();
            callRedirect.logACallRedirect();

            lead1 = [select Status, CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c from lead where Id =: lead1.Id limit 1];            
            
            System.assertEquals(lead1.CallAttempts__c, 1);            
            System.assertEquals(lead1.Status, Label.CL00447);
            System.assertEquals(lead1.SubStatus__c, Label.CL00449);            
            System.assertEquals(lead1.LastCallAttemptDate__c, Date.today());            
            System.assertEquals(lead1.ScheduledCallbackDate__c, null);                        
            
            sc = new ApexPages.StandardController(lead1);
            callRedirect = new VFC41_CallRedirect(sc);
            callRedirect.schACallRedirect();
            
            Task task = Utils_TestMethods.createOpenActivity(null,lead1.Id,  'Not Started', Date.Today().addDays(2));
            insert task;
            
            lead1 = [select Status, CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c from lead where Id =: lead1.Id limit 1];            
            System.assertEquals(lead1.CallAttempts__c, 0);            
            System.assertEquals(lead1.Status, Label.CL00447);
            System.assertEquals(lead1.SubStatus__c, Label.CL00448);            
            System.assertEquals(lead1.LastCallAttemptDate__c, Date.today());            
            System.assertEquals(lead1.ScheduledCallbackDate__c, Date.Today().addDays(2));                        

            lead1.Priority__c = 101;
            update lead1;
            lead1 = [select Status, CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c from lead where Id =: lead1.Id limit 1];                        
            System.assertEquals(lead1.Callbacklimit__c, null);                        
            sc = new ApexPages.StandardController(lead1);
            callRedirect = new VFC41_CallRedirect(sc);
            callRedirect.validatePriority();
            callRedirect.logACallRedirect();
            callRedirect.schACallRedirect();  
            //Srinivas Canged for sep mkt added system.today() as last argument
            task = Utils_TestMethods.createOpenActivity(null,lead2.Id,  'Not Started', null);
            List<task> tsk = new List<task>();
            tsk.add(task);
            sc = new ApexPages.StandardController(lead2);
            callRedirect = new VFC41_CallRedirect(sc);
            callRedirect.insertTasks(tsk);  
         }
         
         User user1 = Utils_TestMethods.createStandardUserByPassVR(profiles[0].Id,'tU2VFC41');        
         system.runas(user1)
         {
            /* 
             lead4 = Utils_TestMethods.createLeadFrstName(null, null,100,'testLead');
             lead4.firstname='testfname1';
             insert lead4;
             system.debug('Created lead ###'+lead4);
             */
         }
         
         system.runas(user)
         {
             //Added to avoid the error coming due to the change in Lead OWD to private
             lead4 = Utils_TestMethods.createLeadFrstName(null, null,100,'testLead');
             lead4.firstname='testfname1';
             insert lead4;
             system.debug('Created lead ###'+lead4);
             //End of change
             lead4 = [select id from lead where id =: lead4.Id];
             sc = new ApexPages.StandardController(lead4);
             callRedirect = new VFC41_CallRedirect(sc);             
         }
       }
    }
}