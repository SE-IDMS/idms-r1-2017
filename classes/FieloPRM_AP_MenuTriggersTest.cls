@isTest
private class FieloPRM_AP_MenuTriggersTest {
  
  @isTest static void test_method_one() {
    
    User us = new user(id = userinfo.getuserId());
    us.BypassVR__c = true;
    update us;
    
    System.RunAs(us){
        
        FieloEE.MockUpFactory.setCustomProperties(false);

        String menuRecordTypeId = [SELECT id,DeveloperName FROM RecordType WHERE SobjectType = 'FieloEE__Menu__c' AND DeveloperName = 'Menu'].Id;

        FieloEE__Menu__c menu = new FieloEE__Menu__c(
            FieloEE__Placement__c = 'Main',
            //FieloEE__Program__c = programId,
            RecordTypeId = menuRecordTypeId,
            F_PRM_MenuFilter__c = 'menu' + datetime.now(),
            F_PRM_Type__c = 'Page',
            F_PRM_Status__c = 'Draft',
            FieloEE__ExternalName__c = 'menu' + datetime.now(),
            Name = 'TEST',
            FieloEE__visibility__c = 'Private'
        );

        insert menu;
        
        test.startTest();

        FieloEE__Menu__c menu1 = new FieloEE__Menu__c(
            FieloEE__Placement__c = 'Main',
            RecordTypeId = menuRecordTypeId,
            F_PRM_MenuFilter__c = 'menu' + datetime.now(),
            F_PRM_Type__c = 'Page',
            F_PRM_Status__c = 'Draft',
            Name = 'TEST',
            FieloEE__ExternalName__c = 'menu2' + datetime.now(),
            FieloEE__visibility__c = 'Private'
        );

        insert menu1;

        test.stopTest();
    }

  }
  
}