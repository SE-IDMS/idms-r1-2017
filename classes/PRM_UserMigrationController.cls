public class PRM_UserMigrationController{

    public PRM_UserMigration__c myUserMigrationSetting {get;set;}

    public PRM_UserMigrationController(){
        myUserMigrationSetting = PRM_UserMigration__c.getValues(System.Label.PRM_UserMigration);
    }

    @RemoteAction
    public static List<String> migrateContacts(List<String> contactIdString) {
        List<Id> contactIds = new List<Id>();
        List<String> returnList = new List<String>();
        Savepoint sp = Database.setSavepoint();
        String suffixUserName = '.bfo-portal.com';


        for(String stringId: contactIdString){
          if(stringId.trim().length()>18){
              contactIds.add(stringId.trim().substring(0,18));
          }else{
               if(stringId.trim()!=''){
                   contactIds.add(stringId.trim());
               }
          }
        }



        PRM_UserMigration__c myUserMigrationSetting = PRM_UserMigration__c.getValues(System.Label.PRM_UserMigration);
        if(myUserMigrationSetting==null){
            returnList.add('Fail: No configuration is associated with '+System.Label.PRM_UserMigration);
            return returnList;
        }

        Map<String,Object> valueBySetting = new Map<String,Object>();

        Map<String,Schema.SObjectField> fieldsMap =Schema.getGlobalDescribe().get('PRM_UserMigration__c').getDescribe().fields.getMap();
        for(Schema.SObjectField field : fieldsMap.values()) {
            DescribeFieldResult myDescribe = field.getDescribe();
            if(myDescribe.getName().endswith('__c')){
                valueBySetting.put(myDescribe.getLabel(),myUserMigrationSetting.get(myDescribe.getName()));
            }
        }
        system.debug('valueBySetting:'+valueBySetting );
        

        try{
            Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
            List<User> userToInsert = new List<User>();
            for(Contact c : [select Id,FirstName,LastName,PRMEmail__c,PRMUIMSId__c,OwnerId,Account.Name,PRMCountry__r.CountryCode__c from contact where id in:contactIds and id not in (select contactId from User)]){
                String alias =  (c.FirstName!=null && c.FirstName.length()>3)?c.FirstName.substring(0, 3):c.FirstName;
                alias +=  (c.LastName!=null && c.LastName.length()>4)?c.LastName.substring(0, 4):c.LastName;
                String nickName = c.FirstName+' '+c.LastName +' ('+c.Account.Name+')';
                if(nickName.length()>37){
                    nickName = nickName.subString(0,37);
                }
                nickName += Math.round(Math.random()*1000);
                system.debug('nickName:'+nickName );
                User u = new User(UserPermissionsChatterAnswersUser=false,UserPreferencesContentEmailAsAndWhen=true,Country__c=c.PRMCountry__r.CountryCode__c,UserPreferencesContentNoEmail=true,FirstName = c.FirstName, FederationIdentifier=c.PRMUIMSId__c,LastName = c.LastName, Email = c.PRMEmail__c, Username = c.PRMEmail__c + suffixUserName,Alias =alias, CommunityNickname = nickName,ProfileId = ProfileFieloId, ContactId = c.Id,UserPermissionsMobileUser = false,PRMRegistrationCompleted__c=true);
                for(String value:valueBySetting.keySet()){
                    u.put(value, valueBySetting.get(value));
                }
                userToInsert.add(u);
            }
            
            
            List<User> userToUpdate = new List<User>();
            String soql;
            soql  ='select Id,Name,UserPermissionsChatterAnswersUser,UserPreferencesContentEmailAsAndWhen,UserPreferencesContentNoEmail,ProfileId,PRMRegistrationCompleted__c,FederationIdentifier,contact.PRMUIMSId__c from User where contactId in:contactIds and contactId!=null';
            for(User u : database.query(soql)){
                u.UserPermissionsChatterAnswersUser=false;
                u.UserPreferencesContentEmailAsAndWhen=true;
                u.UserPreferencesContentNoEmail=true;
                u.ProfileId = ProfileFieloId;
                u.IsActive = true;
                u.PRMRegistrationCompleted__c = true;
                u.FederationIdentifier=u.contact.PRMUIMSId__c;
                for(String value:valueBySetting.keySet()){
                    u.put(value, valueBySetting.get(value));
                }
                userToUpdate.add(u);
                System.debug(u);
            }
            
            Database.DMLOptions dlo = new Database.DMLOptions();
            dlo.EmailHeader.triggerUserEmail = false;
            Database.SaveResult[] srList = Database.insert(userToInsert,dlo);

            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    returnList.add('User inserted:'+ sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        returnList.add('error insert user:'+err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }
            
            
            srList = Database.update(userToUpdate,dlo);

            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    returnList.add('User updated:'+ sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        returnList.add('error update user:'+err.getStatusCode() + ': ' + err.getMessage());
                    }
                }
            }
            
            return returnList;
        }catch(Exception ex){
            System.debug(ex);
            Database.rollback(sp);
            returnList = new List<String>();
            returnList.add('Fail: '+ex.getMessage());
            return returnList;
        }
    }
    
    @RemoteAction
    public static List<String> createFieloMember(List<String> contactIdString) {
        List<Id> allContactIds = new List<Id>();
        Savepoint sp = Database.setSavepoint();
        List<String> returnList = new List<String>();
        returnList.add('**Fielo Member***');

        for(String stringId: contactIdString){
          if(stringId.trim().length()>18){
              allContactIds.add(stringId.trim().substring(0,18));
          }else{
               if(stringId.trim()!=''){
                   allContactIds.add(stringId.trim());
               }
          }
        }
        Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
        List<Id> contactIds = new List<Id>();
        for(Contact c : [select Id from contact where id in:allContactIds and id in (select contactId from User where ProfileId=:ProfileFieloId)]){
            contactIds.add(c.Id);
        }

        for(Contact c : [select Id from contact where id in:allContactIds and id not in (select contactId from User where ProfileId=:ProfileFieloId)]){
            returnList.add(c.Id+' has not a User -> no member creation');
        }

        
        

        for(Contact c : [select Id,Name from contact where id in:allContactIds and id not in (select contactId from User where ProfileId=:ProfileFieloId)]){
            returnList.add('contact <a href=\'/'+c.Id+'\'>'+c.Name+'</a> don\'t have a User?');
        }

        try{
            Id programDefaultId = System.label.CLAPR15PRM149;
            Map<Id,String> errorsResult = FieloEE.ContactConvertion.convertContacts(contactIds,programDefaultId);
            System.debug('**** Fielo Result->'+errorsResult);
            
            if(errorsResult!=null)
                throw new AP_IllegalArgumentException(errorsResult.values().get(0));
            
            for(Contact c : [select Id,FirstName,LastName,name,Email,PRMUIMSId__c,OwnerId, FieloEE__Member__c,FieloEE__Member__r.OwnerId from contact where id in:contactIds]){
                returnList.add('member: '+c.FieloEE__Member__c+' (contact: '+c.Id+', '+c.Name+')' );
            }
            
            return returnList;
        }catch(Exception ex){
            System.debug(ex);
            Database.rollback(sp);
            returnList.add('Fail: '+ex.getMessage());
            return returnList;
        }
    }
    
    @RemoteAction
    public static List<String> addPermissionSet(List<String> contactIdString) {
        List<Id> allContactIds = new List<Id>();
        for(String stringId: contactIdString){
          if(stringId.trim().length()>18){
              allContactIds.add(stringId.trim().substring(0,18));
          }else{
               if(stringId.trim()!=''){
                   allContactIds.add(stringId.trim());
               }
          }
        }
        Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
        List<Id> contactIds = new List<Id>();
        for(Contact c : [select Id from contact where id in:allContactIds and email!='' and id in (select contactId from User where ProfileId=:ProfileFieloId)]){
            contactIds.add(c.Id);
        }
        List<String> returnList = new List<String>();
       
        returnList.add('**Permission Set***');
        PermissionSet r = [SELECT Id FROM PermissionSet WHERE Name = 'FieloPRM_SE' limit 1][0];
        List <PermissionSetAssignment> PSAssignments = new list <PermissionSetAssignment>();
        for(User u : [select Id,ContactId,Name from User where ContactId in:contactIds]){
            PSAssignments.add( new PermissionSetAssignment(AssigneeId = u.id,PermissionSetId = r.Id ) );
        }
        Database.SaveResult[] sResultList = Database.insert(PSAssignments,false);
        for(Database.SaveResult sr : sResultList){
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                returnList.add('Successfully inserted permission set: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    returnList.add(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }
        return returnList;
    }
    
    

}