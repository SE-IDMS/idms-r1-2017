@isTest
Public class Ap_sitelocation_Test{
    static testMethod void unittest(){
        Test.starttest();
      Country__c testCountry = Utils_TestMethods.createCountry();
            insert testCountry;
        string yyxxt;
        User us =Utils_TestMethods.createStandardUser('yyxxt');
         insert us;
        SVMXC__Skill__c  skill= new SVMXC__Skill__c(name='VIP_CUSTOMER',SVMXC__Active__c=true,Country__c=testCountry.id);
         insert skill;             
        Account testAccount = Utils_TestMethods.createAccount(us.Id, testCountry.Id);
         testAccount.Account_VIPFlag__c='VIP1';  
         insert testAccount;      
        SVMXC__Site__c loc= new SVMXC__Site__c(SVMXC__Parent__c=null,PrimaryLocation__c=true,SVMXC__Account__c=testAccount.id);
         insert loc;     
        Account testAccount2 = Utils_TestMethods.createAccount(us.Id, testCountry.Id);
         testAccount2.Account_VIPFlag__c=null;  
         insert testAccount2;
        SVMXC__Site__c loc2= new SVMXC__Site__c(PrimaryLocation__c=true,SVMXC__Account__c=testAccount2.id);
          insert loc2;        
         testAccount2.Account_VIPFlag__c='VIP1'; 
         update testAccount2; 
         Ap_sitelocation.VIPCertificaionfromacc(new List<Account>{testAccount2});
         Test.stoptest();              
    }
}