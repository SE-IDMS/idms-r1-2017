@isTest
public class PRM_FeatureCollabFile_Test {
    static testmethod void Test_FeatureTest() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            Id manualRecordType = [Select id,DeveloperName,Name,SobjectType FRom RecordType WHERE SobjectType = 'FieloPRM_MemberFeature__c' and developerNAme = 'Manual'].Id;
                
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'AKRAM UNIT TEST ';
            member.FieloEE__FirstName__c = 'AKRAM UNIT TEST ';
            member.FieloEE__Street__c = 'AKRAM STREET';
            member.F_Account__c = acc.id;
                 
            insert member;
            //Insert a community User
            Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
            Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',PRMUIMSSEContactId__c = '123456',PRMOnly__c = true, PRMUIMSID__c = '123TEST',
                Email = 'test@test.com',PRMEmail__c='test1@test.com');
            insert c1;

            String str =  string.valueof(System.now());
            str = str.remove('-');
            str = str.remove(' ');
            str = str.remove(':');
            User userCom = new User(FirstName = c1.FirstName,contactId = c1.Id, FederationIdentifier=c1.PRMUIMSId__c,LastName = c1.LastName, Email = c1.PRMEmail__c, Username = str+c1.PRMEmail__c +'.bfo.com',Alias ='teOo8_', CommunityNickname = c1.Name+'8_9',ProfileId = ProfileFieloId, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            insert userCom;

            list<FieloPRM_Feature__c> listFeatureinsert = new list<FieloPRM_Feature__c>();
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c(name = 'feature test',F_PRM_FeatureCode__c= 'feature api name',F_PRM_CustomLogicClass__c = 'PRM_collaborationFeature');
            listFeatureinsert.add(feature);

            insert listFeatureinsert;
            
            Test.startTest(); 

                list<FieloPRM_MemberFeature__c> listMemberFeature = new list<FieloPRM_MemberFeature__c>();
               
                FieloPRM_MemberFeature__c memberFeature = new FieloPRM_MemberFeature__c(F_PRM_Feature__c =  feature.id,F_PRM_Member__c =  member.id,F_PRM_IsActive__c  =  true,PRMUIMSId__c = '123TEST',RecordTypeId=manualRecordType);
                listMemberFeature.add(memberFeature);

                

                insert listMemberFeature;
            FieloPRM_Schedule_MemberFeature.scheduleNow(true, listMemberFeature);
            Test.stopTest();

            List<PermissionSetAssignment> psetAssignList = [select Id From PermissionSetAssignment where AssigneeId =:userCom.Id and  (PermissionSet.Name ='PRMRegularORFCommunity' or PermissionSet.Name ='PRMProjectRegistrationCommunity' or PermissionSet.Name ='PRMPassedToPartnerOpportunitiesCommunity' or PermissionSet.Name ='PRMLeadManagementCommunity' or PermissionSet.Name ='PRMServiceOpportunityCommunity' or PermissionSet.Name ='PRM_Collaboration')];
            //System.assertEquals(6, psetAssignList.size());

            
        }
    }
    
    static testmethod void Test_FeatureTestUpdate() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

                
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'AKRAM UNIT TEST ';
            member.FieloEE__FirstName__c = 'AKRAM UNIT TEST ';
            member.FieloEE__Street__c = 'AKRAM STREET';
            member.F_Account__c = acc.id;
                 
            insert member;
            //Insert a community User
            Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
            Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',PRMUIMSSEContactId__c = '123456',PRMOnly__c = true, PRMUIMSID__c = '123TEST2',
                Email = 'test@test.com',PRMEmail__c='test1@test.com');
            insert c1;

            String str =  string.valueof(System.now());
            str = str.remove('-');
            str = str.remove(' ');
            str = str.remove(':');
            User userCom = new User(FirstName = c1.FirstName, FederationIdentifier=c1.PRMUIMSId__c,LastName = c1.LastName, Email = str+c1.PRMEmail__c, Username = c1.PRMEmail__c + '002.bfo.com',Alias ='teOo8_', CommunityNickname = c1.Name+'8_9',ProfileId = ProfileFieloId, ContactId = c1.Id, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            insert userCom;

            list<FieloPRM_Feature__c> listFeatureinsert = new list<FieloPRM_Feature__c>();
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.name = 'feature test';
            feature.F_PRM_FeatureCode__c= 'feature api name';
            feature.F_PRM_CustomLogicClass__c = 'PRM_collaborationFeature';
            listFeatureinsert.add(feature);            
           
            insert listFeatureinsert;
            
            Test.startTest(); 
                Id manualRecordType = [Select id,DeveloperName,Name,SobjectType FRom RecordType WHERE SobjectType = 'FieloPRM_MemberFeature__c' and developerNAme = 'Manual'].Id;
            
                list<FieloPRM_MemberFeature__c> listMemberFeature = new list<FieloPRM_MemberFeature__c>();
               
                FieloPRM_MemberFeature__c memberFeature = new FieloPRM_MemberFeature__c();
                memberFeature.F_PRM_Feature__c =  feature.id;
                memberFeature.F_PRM_Member__c =  member.id;
                memberFeature.F_PRM_IsActive__c  =  true;
                memberFeature.PRMUIMSId__c = '123TEST2';
                memberFeature.RecordTypeId=manualRecordType;
                listMemberFeature.add(memberFeature);

                insert listMemberFeature;

                listMemberFeature[0].F_PRM_IsActive__c  = false;
                update listMemberFeature;
                FieloPRM_Schedule_MemberFeature.scheduleNow(false, listMemberFeature);
          
            
            Test.stopTest();

            List<PermissionSetAssignment> psetAssignList = [select Id From PermissionSetAssignment where AssigneeId =:userCom.Id and  (PermissionSet.Name ='PRMRegularORFCommunity' or PermissionSet.Name ='PRMProjectRegistrationCommunity' or PermissionSet.Name ='PRMPassedToPartnerOpportunitiesCommunity' or PermissionSet.Name ='PRMLeadManagementCommunity' or PermissionSet.Name ='PRMServiceOpportunityCommunity' or PermissionSet.Name ='PRM_Collaboration')];
            
            //System.assertEquals(3, psetAssignList.size());

            
        }
    }
}