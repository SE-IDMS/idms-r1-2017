@isTest
private class VFC80_SearchSellingCenter_TEST {

//*********************************************************************************
// Controller Name  : VFC80_SearchSellingCenter_TEST
// Purpose          : Test Controller class for Search Selling Center
// Created by       : Hari Krishna  Global Delivery Team
// Date created     : 
// Modified by      :
// Date Modified    :
// Remarks          : For Sales April 2012 Release
///********************************************************************************/

    static testMethod void VFC80_SearchSellingCenterTest() {
         //Start of Test Data preparation
         
        Profile profile = [select id from profile where name='System Administrator' LIMIT 1];
        User user = Utils_TestMethods.createStandardUser(profile.id, 'test');
        
        Profile profile1 = [select id from profile where name='System Administrator' LIMIT 1];    
        User user2 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse2');
        database.insert(user2);
            
        User user3 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse3');
        database.insert(user3);
        
        User user4 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse4');
        database.insert(user4);
        
        Country__c country1 = Utils_TestMethods.createCountry();
        insert country1;
    
    
    
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
            
        Opportunity oppty = Utils_TestMethods.createOpenOpportunity(account1.id);
        oppty.OwnerId = user3.id;      
        insert oppty;  
        
        
        Opp_SellingCenter__c sellcenter= Utils_TestMethods.createSellingCenter();
        sellcenter.Name = 'Test for VFC80_SearchSellingCenter';
        sellcenter.Business__c ='BD';
        sellcenter.Country__c = country1.id;
        insert  sellcenter;
        
        
    
        
        List<String> keywords = New List<String>();
        keywords.add('Test');
        List<String> filters = New List<String>();
        filters.add('BD');
        filters.add(country1.id);
        
        Utils_DataSource dataSource = Utils_Factory.getDataSource('SECS');
        Utils_DataSource.Result searchResult = dataSource.Search(keywords,filters);
        sObject tempsObject = searchResult.recordList[0];
        PageReference pageRef;
        VFC_ControllerBase basecontroller = new VFC_ControllerBase();        
        //End of Test Data preparation.
    
        system.runAs(user3)
        {        
            ApexPages.StandardController oc1 = new ApexPages.StandardController(new OPP_SellingCenter__c());        
            VFC80_SearchSellingCenter SearchSellingCenter = new VFC80_SearchSellingCenter(oc1);
            pageRef= Page.VFP80_SearchSellingCenter; 
            pageRef.getParameters().put('CF00NA000000559Lc_lkid', oppty.id);
            pageRef.getParameters().put('CF00NA000000559Lc', oppty.Name);
    
            System.debug('~~~~~~~~~~Start of Test~~~~~~~~~~');
            VCC06_DisplaySearchResults componentcontroller = SearchSellingCenter.resultsController; 
            SearchSellingCenter.searchText='Test';
            SearchSellingCenter.level1 = 'BD';
            SearchSellingCenter.level2 = country1.id;
            Test.setCurrentPageReference(pageRef);        
            SearchSellingCenter.PerformAction(tempsObject, SearchSellingCenter);
           // system.debug('********search****'+)
            
            
            SearchSellingCenter.search();
            SearchSellingCenter.clear();
            SearchSellingCenter.cancel();
            
            SearchSellingCenter.searchText='';
            SearchSellingCenter.level1 = Label.CL00355;
            SearchSellingCenter.level2 = Label.CL00355;
            SearchSellingCenter.search();
    
            for(Integer index=0; index<1000; index++)
            {
                SearchSellingCenter.searchText=SearchSellingCenter.searchText+'1234567890';
            }
            SearchSellingCenter.search();             
            /*
            OPP_OpportunityCompetitor__c opComp = new OPP_OpportunityCompetitor__c(TECH_CompetitorName__c=comp.id, OpportunityName__C = oppty.id);
            insert opComp;
            SearchSellingCenter.PerformAction(tempsObject, SearchSellingCenter);
            
            pageRef.getParameters().put(Label.CL00452, null);
            Test.setCurrentPageReference(pageRef);         
            SearchSellingCenter.cancel();    
            */       
        }
    
         pageRef = NULL;
    }
}