/**
  Author: Schneider Electric
  Date Of Creation: 17th Oct 2012
  Description : Controller for the Shipping Address Update in the Return Item Object on Click of Save Button.

**/

Public Class VFC_CreateReturnItem  extends VFC_ControllerBase {

   public RMA__c objRMA{get;set;}             // Return Material Authorization Object
   public RMA_Product__c objRI{get;set;}      // Return Item Object
   public Boolean IsRMA{get; set;}
   Public Boolean showme{get; set;}
   public sObject objRec;
   // *********************************************
   Public Boolean showOthersBlock{get; set;}
   Public Boolean showResultsBlock{get; set;}
   Public Boolean isCheck{get; set;}
   Public Boolean IsError{get; set;}
   Public Boolean IsErrorPL{get; set;}
   Public Boolean IsFirstReturnItem {get; set;}
   Public Boolean IsTEX {get; set;}
   Public Boolean canProceed {get; set;}
   //*********************************************
   public list<selectOption> lstselectOption{get;set;}
   public list<selectOption> lstselectOption2{get;set;}
    public list<selectOption> lstselectOption3{get;set;}
   Public string strCustomerResolutions{get;set;}
   public List<ProductLineQualityContactMapping__c> listobjPL{get;set;}
   public ProductLineQualityContactMapping__c objPL{get;set;}
   Public String NewObjId;
   public string RIGMRCode;
   public string RIProductline;
   public string RIProductline1;
   public string slist;
   Public string strCommercialReference;
   Public string strFamily;
   Public string strSubFamily;
   Public string strRMAAccCountry;
   Public boolean loopEnter;
   list<ResolutionOptionAndCapabilities__c> lstROC;
   list<ResolutionOptionAndCapabilities__c> lstResROC;
   list<RMAShippingToAdmin__c> lstLocalRtnCenters;
   list<RMAShippingToAdmin__c> lstGlobalRtnCenters;
   list<RMAShippingToAdmin__c> lstRtnCenters;
   list<BusinessRiskEscalationEntity__c> PLReturnCenters;
   PageReference pr1 = NULL;
   PageReference pr;
   public map<string,string> mapRCRT;
   
   public ApexPages.StandardController controller;
   
   // ========== Initilization for Display Results ===============
   public List<SelectOption> columns{get;set;}
   public Boolean DisplayResults{get;set;}
   public List<DataTemplate__c> fetchedRecords{get;set;} 
   public Long numberOfRecord{get;set;}
   private Boolean showButton; 
   Public string SelectTypevalue{get;set;}  
   //current page parameters
    public final Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
   // creation is not authorized when search result returns more then searchMaxSize
    private Integer searchMaxSize = Integer.valueOf(System.Label.CL00016);
   // indicates whether there are more records than the searchMaxSize.
      public Boolean hasNext {
        get {
            return ((numberOfRecord > searchMaxSize || numberOfRecord == 0)  && showButton );
            }
        set;
    }
   //==================================================================
   
  public VFC_CreateReturnItem(ApexPages.StandardController stdController)
    {
       // ========= for search display ================
        columns = new List<SelectOption>();        
        DisplayResults = true;
        showButton = true; 
        SelectTypevalue='Single';
        numberOfRecord=0;
        IsError= False;
        IsErrorPL= False;
        IsTEX = False;
        listobjPL= new List<ProductLineQualityContactMapping__c>();// As part of October 2015 Release BR-5320
        objPL=new ProductLineQualityContactMapping__c(); // As part of October 2015 Release BR-5320
        loopEnter = False; // as part of fix
       columns.add(new SelectOption('Field2__c',sObjectType.RMAShippingToAdmin__c.fields.ReturnCenter__c.label));// First Column
      
        columns.add(new SelectOption('Field1__c',sObjectType.RMAShippingToAdmin__c.fields.Capability__c.label));
        columns.add(new SelectOption('Field3__c',sObjectType.RMAShippingToAdmin__c.fields.RCStreet__c.label));
        columns.add(new SelectOption('Field4__c',sObjectType.RMAShippingToAdmin__c.fields.RCCity__c.label));
        columns.add(new SelectOption('Field5__c',sObjectType.RMAShippingToAdmin__c.fields.RCZipCode__c.label));
        columns.add(new SelectOption('Field6__c',sObjectType.RMAShippingToAdmin__c.fields.RCCountry__c.label));
         columns.add(new SelectOption('Field23__c',sObjectType.RMAShippingToAdmin__c.fields.Comments__c.label));//As part of April 2014 release.
        fetchedRecords = new List<DataTemplate__c>();
        DataTemplate__c dt = new DataTemplate__C(); 
        dt.field1__c = ' ';  
        dt.field7__c=''; 
        fetchedRecords.add(dt);
        fetchedRecords.clear();
        mapRCRT=new map<string,string>();
        
        //==============================================
       
       //showOthersBlock = False;
       //showResultsBlock = True;
       showOthersBlock = TRUE;
       this.objRMA = (RMA__c)stdController.getRecord();
       lstselectOption=new list<selectOption>();
       lstselectOption2=new list<selectOption>();
       lstselectOption3=new list<selectOption>();
       isCheck =  false;
       objRI = new RMA_Product__c ();
       lstROC = new List <ResolutionOptionAndCapabilities__c>();
       lstRtnCenters = new List <RMAShippingToAdmin__c>();
       lstLocalRtnCenters = new List <RMAShippingToAdmin__c>();
       lstGlobalRtnCenters = new List <RMAShippingToAdmin__c>();
       PLReturnCenters = new List<BusinessRiskEscalationEntity__c>();
        
      
       //======== To get Prefix ===================================================================
           Schema.DescribeSObjectResult RMASchemaReuslt = RMA__c.sObjectType.getDescribe();
           Schema.DescribeSObjectResult RISchemaResult =  RMA_Product__c.sObjectType.getDescribe();       
       //===========================================================================================
         NewObjId = System.CurrentPageReference().getparameters().get('newid');
         RIGMRCode = System.CurrentPageReference().getparameters().get('rigmrcode');
         RIProductline1 = System.CurrentPageReference().getparameters().get('Productline');
         if(RIProductline1 != null) {
            RIProductline = EncodingUtil.urlDecode(RIProductline1,'UTF-8');
         }
         else {
             RIProductline = RIProductline1;
         }
         system.debug('## RIProductline ##' + RIProductline );
         System.debug('## RIGMRCode ##' + RIGMRCode);
         System.debug('## OBject ID ##' + NewObjId);
         
       If (NewObjId.startsWith(RMASchemaReuslt.getKeyPrefix()))
       {
          // If NewId belongs to RMA
           system.debug('## PREFIX ##' + RMASchemaReuslt.getKeyPrefix() );
           IsFirstReturnItem = True;
           IsRMA = True;
           objRMA = [Select id, RecordType.DeveloperName,TEX__r.ExpertCenter__r.Location_Type__c,TEX__c,ProductCondition__c, TEX__r.ExpertCenter__c,TEX__r.ExpertCenter__r.Name,TECH_CR__c,case__r.TECH_GMRCode__c,CustomerResolution__c,TECH_CustomerResolution__c,case__r.quantity__c,case__r.DateCode__c,case__r.SerialNumber__c,Case__r.CommercialReference__c,  case__r.Account.Country__c, case__r.Family__c,case__r.ProductFamily__c,case__r.ProductLine__c from RMA__c where id=:NewObjId]; 
           system.debug('## 1. objRMA ##'+ objRMA );
           if(RIProductline==null){
           RIProductline=objRMA.case__r.ProductLine__c;}
           System.debug('## RIProductline ## '+ RIProductline);
           listobjPL= [SELECT ProductLine__c,QualityContact__r.name,QualityContact__r.IsActive, Id, BusinessUnit__c, AccountableOrganization__r.Name, AccountableOrganization__c from ProductLineQualityContactMapping__c where ProductLine__c=:RIProductline]; // As part of October 2015 Release BR-5320 objRMA.case__r.ProductLine__c
            system.debug('listobjPL'+listobjPL);
             If ( listobjPL.size()>0 && listobjPL[0]!=null )
                {
                    objPL=listobjPL[0];
                    system.debug('**objPL** + objPL');
                }else{IsErrorPL= True;}
           // Creation of New RI
           objRI.Quantity__c = objRMA.case__r.quantity__c;
           objRI.DateCode__c = objRMA.case__r.DateCode__c;
           objRI.SerialNumber__c = objRMA.case__r.SerialNumber__c;
           objRI.Tech_GMRCode__c=objRMA.case__r.TECH_GMRCode__c;
           system.debug('## 1. objRMA ##'+ objRMA ); 
           system.debug('##### objRI.CopyTEXExpertCenter__c ##'+ objRI.CopyTEXExpertCenter__c );
           system.debug('##  objRMA.TEX ##'+ objRMA.TEX__c ); 
           system.debug('## RecordTypeName ##'+ objRMA.RecordType.DeveloperName);
            List<CS_RIPickListValues__c> plvalues= CS_RIPickListValues__c.getall().values();
            plvalues.sort();
            //system.debug('## oto '+ oto);
           if(objRMA.TEX__c != null)
           {
             system.debug('##12  objRMA.TEX ##'+ objRMA.TEX__c ); 
              IsTEX=True;
              String TEXOverrideFlag = System.CurrentPageReference().getparameters().get('TEXOverrideFlag');
              if (TEXOverrideFlag == 'False')
                  objRI.CopyTEXExpertCenter__c=True;
                  for(CS_RIPickListValues__c cs: plvalues)
                  {
                          if( cs.Type__c=='TEX')
                          {
                            lstselectOption2.add(new selectOption(cs.PickListValue__c,cs.PickListLabel__c));
              
                          }
                          }
           }
           else
           {
              for(CS_RIPickListValues__c cs: plvalues)
                  {
                          if( cs.Type__c=='NonTEX')
                          {
                            lstselectOption3.add(new selectOption(cs.PickListValue__c,cs.PickListLabel__c));
              
                          }
                  }
           }

           if (objRMA.case__r.CommercialReference__c==null)
           {
              //objRI.name = Label.CLDEC12RR15;
             objRI.name = objRMA.case__r.Family__c;  // sukumar salla - 05AUG2014 - OCT2014Release - BR-5266
             objRI.Family__c=objRMA.case__r.Family__c ;

           }
           else
           {
              objRI.name=objRMA.case__r.CommercialReference__c ;
        //k: Add START
        objRI.commercialReference__c=objRMA.case__r.CommercialReference__c ;
        objRI.Family__c=objRMA.case__r.Family__c ;
        //k: Add END
           }
           
           objRI.ProductFamily__c=objRMA.case__r.ProductFamily__c;
           objRI.RMA__c=objRma.id;
           objRI.CustomerResolution__c=objRma.CustomerResolution__c;
           objRI.ProductCondition__c=objRma.ProductCondition__c;
           // To Manipulate the ProductConditon picklist on Intermediate page.
           lstselectOption.add(new selectOption(objRMA.ProductCondition__c,objRMA.ProductCondition__c));
       }
        else if (NewObjId.startsWith(RISchemaResult.getKeyPrefix()))
       {
           IsFirstReturnItem = False;
            IsRMA = False;
            if(RIProductline==null){
           RIProductline=objRMA.case__r.ProductLine__c;}
           System.debug('## RIProductline ## '+ RIProductline);
            objRI = [select id, Name,TECH_CR__c, ProductCondition__c, ShippingStreet__c ,ShippingStreetLocalLang__c ,CustomerResolution__c, ProductFamily__c,
                    RMA__c,  RMA__r.Case__r.Account.Country__c,ShippingAdditionalAddress__c, ShippingLocalAdditionalAddress__c, ShippingCity__c, 
                    ShippingLocalCity__c, ShippingZipCode__c, ShippingStateProvince__c, ShippingCounty__c, ShippingLocalCounty__c, ShippingCountry__c, ShippingPhone__c, ShippingFax__c, ShippingContactName__c,
                    ShippingPOBox__c, RMA__r.TEX__c,TECH_RequestType__c,ShippingPOBoxZip__c,Other__c, Tech_GMRCode__c, ContactEmail__c, CopyTEXExpertCenter__c,ProductLine__c from RMA_Product__c where Id=:NewObjId];
                    system.debug('**objRI**' + objRI);
                    listobjPL= [SELECT ProductLine__c,QualityContact__r.name,QualityContact__r.IsActive, Id, BusinessUnit__c, AccountableOrganization__r.Name, AccountableOrganization__c from ProductLineQualityContactMapping__c where ProductLine__c=:RIProductline]; // As part of October 2015 Release BR-5320 objRI.ProductLine__c
            system.debug('listobjPL'+listobjPL);
             If ( listobjPL.size()>0 && listobjPL[0]!=null )
                {
                    objPL=listobjPL[0];
                    system.debug('**objPL** + objPL');
                }else{IsErrorPL= True;}
                    
            if (objRI != null)
           {
             objRMA = [Select id, TEX__c,TEX__r.ExpertCenter__r.Location_Type__c,ProductCondition__c, TEX__r.ExpertCenter__c,TEX__r.ExpertCenter__r.Name,CustomerResolution__c,case__r.quantity__c,Case__r.CommercialReference__c,  case__r.Account.Country__c, case__r.Family__c from RMA__c where id=:objRI.RMA__c];
             system.debug ('## Customer Resolution Before ##'+ objRMA.CustomerResolution__c);
              if(RIGMRCode!=null)
               {
                  objRI.Tech_GMRCode__c = RIGMRCode;
               }
               if(objRMA.TEX__c != null)
                {
                     system.debug('##12  objRMA.TEX ##'+ objRMA.TEX__c ); 
                      IsTEX=True;
                      String TEXOverrideFlag = System.CurrentPageReference().getparameters().get('TEXOverrideFlag');
                      if (TEXOverrideFlag == 'False')
                      objRI.CopyTEXExpertCenter__c=True;
              }
            
           }         
           lstselectOption.add(new selectOption(objRI.ProductCondition__c,objRI.ProductCondition__c));
          // getRCbasedonRI();
       }
       
    } 
   
     public PageReference doSearchReturnCenters()
     {    
          If (IsFirstReturnItem == True){ getRCbasedonRMA();} 
          else if (IsFirstReturnItem == False)
          {             
               if (objRI.Other__c==True)
               {
                   showOthersBlock = False;
                   objRI.Other__c=False;
               }
              getRCbasedonRI();
          }
         return pr1;
     }

    Public Void getRCbasedonRMA ()
    {
        canProceed =True;
        if ( objRMA!=null)
        {
         lstROC = [Select id, ProductCondition__c, Type__c,CustomerResolution__c, ProductDestinationCapability__c from ResolutionOptionAndCapabilities__c where ProductCondition__c=:objRMA.ProductCondition__c AND CustomerResolution__c =: objRMA.CustomerResolution__c];
         system.debug ('## objRMA.CustomerResolution__c ##'+objRMA.CustomerResolution__c);
         
         system.debug('## 3. lstROC Size ##'+ lstROC.size() );
         set <String> stCapabilites = new set <String> ();
         If (lstROC!=null && lstROC.size()>0)
           {
           system.debug('~~~~~~~~~~~~~~objRI.TECH_RequestType__c##'+ objRI.TECH_RequestType__c );
               for (ResolutionOptionAndCapabilities__c varROC :lstROC )
               {               
                 system.debug('~~~~~~~~~~~~~~Type__Cc##'+ varROC.Type__c );  
                 stCapabilites.add(varROC.ProductDestinationCapability__c);
                 strCustomerResolutions = varROC.CustomerResolution__c ;
               }
              
                string slist='';
                for (String s: stCapabilites) 
                {
                    slist += '\'' + s + '\',';
                }
                slist = slist.substring (0,slist.length() -1);
                system.debug ( '## 4. stCapabilites  ## ' + slist );
                // Clear the Lists before assigning
                lstLocalRtnCenters.clear();
                lstRtnCenters.clear();
                // Get Return Centers
                lstLocalRtnCenters=getLocalRCs( objRMA, slist);
                system.debug ('## 5. lstLocalRtnCenters ###'+ lstLocalRtnCenters);
               
                //======================= Uniqeness Check on Display result ==============================
                lstRtnCenters = checkUniqueness(lstLocalRtnCenters);
                // created a new method checkUniqueness replacing the code - 15JULY2014 - Sukumar Salla
                //===================================================================end uniqueness==========
                system.debug ('## 6. lstRtnCenters ###' + lstRtnCenters);
                // call method to display Return Centers in the grid.
                displayRCs(lstRtnCenters);
           }
          
        }   
    }
    
 
   Public Void getRCbasedonRI ()
    {
        canProceed = true;
        if( objRI!=null )
        {
         lstROC = [Select id, ProductCondition__c, Type__c, CustomerResolution__c, ProductDestinationCapability__c from ResolutionOptionAndCapabilities__c where ProductCondition__c=:objRI.ProductCondition__c AND CustomerResolution__c =: objRI.CustomerResolution__c];
         system.debug('## 3. lstROC Size ##'+ lstROC.size() );
         set <String> stCapabilites = new set <String> ();
         If ( lstROC!=null && lstROC.size()>0)
           {
            system.debug('~~~~~~~~~~~~~~objRI.TECH_RequestType__c##'+ objRI.TECH_RequestType__c );
               for (ResolutionOptionAndCapabilities__c varROC :lstROC )
               {
               system.debug('~~~~~~~~~~~~~~Type__Cc##'+ varROC.Type__c ); 
                   stCapabilites.add(varROC.ProductDestinationCapability__c);
                   strCustomerResolutions = varROC.CustomerResolution__c ;
                   mapRCRT.put(varROC.CustomerResolution__c,varROC.Type__c);  
               }
              if( objRI.TECH_RequestType__c!=null){              
                    if(isCorrectRequestType()==False)
                    {
                       canProceed = false;
                       lstRtnCenters.clear();
                       displayRCs(lstRtnCenters);
                    }
                    else {canProceed = true;} 
              } 
              else{canProceed = True;}
               
               if( canProceed==True)
               {
               string slist='';
               for (String s: stCapabilites) 
               {
                    slist += '\'' + s + '\',';
               }
               slist = slist.substring (0,slist.length() -1);
               system.debug ( '## 4. stCapabilites  ## ' + slist );
                // Clear the Lists before assigning
                lstLocalRtnCenters.clear();
                lstRtnCenters.clear();
                // Get Return Centers
                lstLocalRtnCenters=getLocalRCs( objRI, slist);
                system.debug ('## 5. lstLocalRtnCenters ###'+ lstLocalRtnCenters);
                
                //======================= Uniqeness Check on Display result =============================
                lstRtnCenters = checkUniqueness(lstLocalRtnCenters);
                //===================================================================end uniqueness==========
                system.debug ('## 6. lstRtnCenters ###' + lstRtnCenters);
                // Call method to display Return Centers 
                displayRCs(lstRtnCenters);    
           }
         }
        }    
    }
     
    public PageReference displayRCs(List <RMAShippingToAdmin__c> lstRtnCenters)
    { 
           fetchedRecords.clear();           
           string newCR;
           string TechCR;
           if (IsRMA == true)
           {
               TechCR = objRMA.TECH_CR__c;
               newCR = objRMA.CustomerResolution__c;
           }
           else
           {
               TechCR = objRI.TECH_CR__c;
               newCR = objRI.CustomerResolution__c;
           }
          
           If ( (lstRtnCenters!=null && lstRtnCenters.size()>1) ||  (lstRtnCenters!=null && newCR!=TechCR)|| objRMA.TEX__c!=null)
               {
                   system.debug ('## SIZE ##'+ lstRtnCenters.size());
                   fetchedRecords.clear();
                   system.debug('==lstRtnCenters=='+lstRtnCenters);
              
                   for (RMAShippingToAdmin__c RC : lstRtnCenters )
                   {
                      set <String> stResolutions = new set <String> ();
                      numberOfRecord++;
                      if (RC!=null )
                        { 
                      
                            system.debug ( '## RC  ## ' + RC  );
                            system.debug ('***RC-GMRCode***' + RC.ProductFamily__r.TECH_PM0CodeInGMR__c );
                            DataTemplate__c dt = new DataTemplate__c();                  
                            dt.field2__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+RC.ReturnCenter__c+'" target="_blank">'+RC.ReturnCenter__r.Name+'</a>';
                            dt.field1__c = RC.Capability__c;
                            dt.field3__c = RC.RCStreet__c;
                            dt.field4__c = RC.RCCity__c;
                            dt.field5__c = RC.RCZipCode__c;
                            dt.field6__c = RC.RCCountry__c;
                            dt.field7__c = RC.ReturnCenter__r.RCCountry__c;
                            dt.field8__c = RC.RCAdditionalAddress__c; 
                            dt.field9__c = RC.RCPOBox__c;
                            dt.field10__c = RC.RCPOBoxZip__c;
                            dt.field11__c = RC.RCCounty__c; 
                            dt.field12__c = RC.ReturnCenter__r.RCStateProvince__c; 
                            dt.field13__c =RC.ReturnCenter__c;
                            dt.field14__c =RC.RCPhone__c;
                            dt.field15__c =RC.RCFax__c;
                            dt.field16__c =RC.RCContactName__c;
                            dt.field17__c =RC.RCStreetLocalLang__c;
                            dt.field18__c =RC.RCLocalAdditionalAddress__c;
                            dt.field19__c =RC.RCLocalCity__c;
                            dt.field20__c =RC.RCLocalCounty__c;
                            dt.field22__c =RC.ContactEmail__c;
                           dt.field23__c =RC.Comments__c;//As part of April 2014 Release.
                            if (IsRMA==True)
                            {
                            dt.field21__c = objRMA.CustomerResolution__c;
                            }
                            else
                            {
                             dt.field21__c = objRI.CustomerResolution__c;
                            }
                            fetchedRecords.add(dt);
                            
                            
                        }
                   }
                }
    
            else if ( lstRtnCenters!=null && lstRtnCenters.size() == 1)
            {
                // It should directly create RI and update the shipping address
                system.debug ('***RC-GMRCode***' + lstRtnCenters[0].ProductFamily__r.TECH_PM0CodeInGMR__c );
                
                objRI.ShippingStreet__c = lstRtnCenters[0].RCStreet__c;
                objRI.ShippingCity__c= lstRtnCenters[0].RCCity__c;
                objRI.ShippingZipCode__c = lstRtnCenters[0].RCZipCode__c;
                objRI.ShippingCountry__c = lstRtnCenters[0].ReturnCenter__r.RCCountry__c;
                objRI.ShippingAdditionalAddress__c = lstRtnCenters[0].RCAdditionalAddress__c; 
                objRI.ShippingCounty__c = lstRtnCenters[0].RCCounty__c;
                objRI.ShippingPOBox__c = lstRtnCenters[0].RCPOBox__c;
                objRI.ShippingPOBoxZip__c = lstRtnCenters[0].RCPOBoxZip__c;
                objRI.ShippingStateProvince__c = lstRtnCenters[0].ReturnCenter__r.RCStateProvince__c; 
                objRI.ReturnCenter__c=lstRtnCenters[0].ReturnCenter__c;
                objRI.ShippingPhone__c=lstRtnCenters[0].RCPhone__c;
                objRI.ShippingFax__c=lstRtnCenters[0].RCFax__c;
                objRI.ShippingContactName__c=lstRtnCenters[0].RCContactName__c;
                objRI.ShippingStreetLocalLang__c=lstRtnCenters[0].RCStreetLocalLang__c;
                objRI.ShippingLocalAdditionalAddress__c=lstRtnCenters[0].RCLocalAdditionalAddress__c;
                objRI.ShippingLocalCity__c=lstRtnCenters[0].RCLocalCity__c;
                objRI.ShippingLocalCounty__c=lstRtnCenters[0].RCLocalCounty__c;
                objRI.ContactEmail__c=lstRtnCenters[0].ContactEmail__c;
                objRI.Comments__c=lstRtnCenters[0].Comments__c;//Oct2014 Release - Sukumar Salla - 15JULY2014
                //objRI.CustomerResolution__c=objRMA.CustomerResolution__c;
                if (IsRMA==True)
                 {
                    objRI.CustomerResolution__c = objRMA.CustomerResolution__c;
                    objRI.Tech_GMRCode__c=objRMA.case__r.TECH_GMRCode__c;
                 }
                else
                 {
                    objRI.CustomerResolution__c = objRI.CustomerResolution__c;
                    objRI.Tech_GMRCode__c = RIGMRCode;
                 }
                Upsert objRI;
                pr1 = new PageReference('/'+ objRMA.id);
                system.debug('## pr1 ##'+ pr1);  
                pr1.setRedirect(true);          
            } 
        return pr1;
    }
    
    public list<RMAShippingToAdmin__c> getLocalRCs( sObject objR, string slist)
    {        
             If (IsFirstReturnItem == True)
             {
                 RMA__c objRMA = (RMA__c) objR;
                 system.debug(' ## FIRST objRMA ##' + objRMA);
                 strCommercialReference = objRMA.Case__r.CommercialReference__c;
                 System.debug('objRMA.case__r.Family__c'+objRMA.case__r.Family__c);
                 if (objRMA.case__r.Family__c!=null && objRMA.case__r.Family__c!='')
                 {
                    string newPF = objRMA.case__r.TECH_GMRCode__c ;
                    if (newPF!=null) 
                        { strFamily = newPF.substring(0,8);}
                    else
                        { strFamily = null;}
                 }
                 else
                    { strFamily = null; }
                strRMAAccCountry = objRMA.case__r.Account.Country__c;                 
                System.debug('****strFamily'+strFamily);
             }
             else
             {
                 RMA_Product__c objRI = (RMA_Product__c) objR;
                 system.debug(' ## RI ##' + objRI );
                 strCommercialReference = objRI.Name;
                 string newPF= RIGMRCode ;
                  system.debug(' ## newPF ##' + newPF );
                  if (newPF!=null)
                  {
                     strFamily = newPF.substring(0,8);
                  }
                  else
                  {
                      strFamily = null;
                  }
                 //strFamily=objRI.ProductFamily__c;
                 strRMAAccCountry = objRI.RMA__r.Case__r.Account.Country__c;
             }

              system.debug('## sl ##' + slist);
              String strQuery = 'select id, Comments__c, RCFax__c, RCContactName__c,ContactEmail__c, RCStreetLocalLang__c,RCLocalAdditionalAddress__c,RCLocalCity__c,RCLocalCounty__c,ReturnCenter__c,RCPhone__c,RCAdditionalAddress__c, TECH_ProductFamilyGMRCode__c, RCPOBox__c, RCPOBoxZip__c,ProductFamily__r.TECH_PM0CodeInGMR__c, ProductFamily__r.name, RCCounty__c, RCStateProvince__c,ReturnCenter__r.RCStateProvince__c,Country__c,RCStreet__c,RCCity__c,RCZipCode__c, RCCountry__c,ReturnCenter__r.RCCountry__c,ReturnCenter__r.RCCountry__r.Name,ReturnCenter__r.Name,Capability__c,CommercialReference__c, ProductFamily__c,RCShippingAddress__c,Ruleactive__c from RMAShippingToAdmin__c where Capability__c Includes (' + slist + ') AND Ruleactive__c=True';
              System.debug('strQuery =' + strQuery);
              System.debug('strCommercialReference = '+strCommercialReference);
              System.debug('strFamily = '+strFamily);
            if ( (strCommercialReference !=null) && (strFamily !=null ))
               {
               System.debug('strCommercialReference '+strCommercialReference );
               strQuery = strQuery + ' AND ((((CommercialReference__c= \''+strCommercialReference+'\') OR ( ((CommercialReference__c=\'\') AND TECH_ProductFamilyGMRCode__c= \''+ strFamily +'\')))'; 
                loopEnter = true;
               }
               else if ((strCommercialReference ==null ) && (strFamily ==null ))
               {
                  strQuery = strQuery + ' AND ((((CommercialReference__c=\'\') AND (TECH_ProductFamilyGMRCode__c=\'\'))';
                  loopEnter = true;
               }
             
               else if ((strCommercialReference ==null ) && (strFamily !=null ))
               {
                    System.debug('strFamily '+ strFamily );
                     strQuery = strQuery + ' AND (((CommercialReference__c=\'\') AND ( TECH_ProductFamilyGMRCode__c= \''+ strFamily +'\')';
                     loopEnter = true;
                    
               }
               System.debug('Entered the loop'+loopEnter);
               if(loopEnter == true) {
                   
                strQuery = strQuery +' AND  (Country__c=\'\' OR   Country__c=\''+strRMAAccCountry+'\'))';
                strQuery =  strQuery + 'OR ( (Country__c=\''+strRMAAccCountry+'\') AND (CommercialReference__c=\'\') AND (TECH_ProductFamilyGMRCode__c=\'\')))';
               }
               else if(loopEnter == false) {
                    strQuery = strQuery +' AND ( (Country__c=\'\' OR   Country__c=\''+strRMAAccCountry+'\')';
                    strQuery =  strQuery + 'OR ( (Country__c=\''+strRMAAccCountry+'\') AND (CommercialReference__c=\'\') AND (TECH_ProductFamilyGMRCode__c=\'\')))'; 
               }
               system.debug('Local Rule'+ strQuery );
               return database.query(strQuery);
        }
  
     public VCC06_DisplaySearchResults resultsController 
        {
        set;
        get
            {
                if(getcomponentControllerMap()!=null)
                {
                    VCC06_DisplaySearchResults displaySearchResults;
                    displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                    system.debug('--------displaySearchResults -------'+displaySearchResults );                
                    if(displaySearchResults!= null)
                    return displaySearchResults;
                }  
                return new VCC06_DisplaySearchResults();
            }
        }
        
    public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
     {   
         System.debug('**Started***');
         VFC_CreateReturnItem thisController = (VFC_CreateReturnItem)controllerBase;
         DataTemplate__c dt= (DataTemplate__c)obj;
         System.debug('****************** Check Point*********');
        
        objRI.ShippingStreet__c = dt.field3__c;
        objRI.ShippingCity__c= dt.field4__c;
        objRI.ShippingZipCode__c = dt.field5__c;
        objRI.ShippingCountry__c =dt.field7__c;
        objRI.ShippingAdditionalAddress__c = dt.field8__c;
        objRI.ShippingCounty__c = dt.field11__c;
        objRI.ShippingPOBox__c = dt.field9__c;
        objRI.ShippingPOBoxZip__c = dt.field10__c;
        objRI.ShippingStateProvince__c = dt.field12__c;
        objRI.ReturnCenter__c=dt.field13__c;
        objRI.ShippingPhone__c=dt.field14__c;
        objRI.ShippingFax__c=dt.field15__c;
        objRI.ShippingContactName__c=dt.field16__c;
        objRI.ShippingStreetLocalLang__c=dt.field17__c;
        objRI.ShippingLocalAdditionalAddress__c=dt.field18__c;
        objRI.ShippingLocalCity__c=dt.field19__c;
        objRI.ShippingLocalCounty__c=dt.field20__c;
        objRI.CustomerResolution__c=dt.field21__c;
        objRI.ContactEmail__c=dt.field22__c;
        objRI.Comments__c=dt.field23__c; // Oct2014 Release - Sukumar Salla - 15JULY2014
       
        system.debug('## objRI - Before insertion ##'+ objRI);        
        Upsert objRI;
         
        PageReference pr = new PageReference('/'+ objRMA.id);
        pr.setRedirect(true);               
        return pr;
                
     } 
Public Boolean isCorrectRequestType()
    {
        boolean pr;
        if(mapRCRT.containskey(objRI.CustomerResolution__c)){
           if(objRI.TECH_RequestType__c!=mapRCRT.get(objRI.CustomerResolution__c)){
                lstRtnCenters.clear();
                IsError=True;             
                string errmsg =Label.CLAPR14RRC19;
                if(objRI.TECH_RequestType__c==Label.CLAPR14RRC20){errmsg=Label.CLAPR14RRC22;}              
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,errmsg));
                pr =False;
             }
             else {pr=True;}
        }
      return pr;
    }
     Public Pagereference doSave()
     {
     pageReference pr;
     system.debug (' ## In Error ##' + objRI);
        if(objRI.ReturnCenter__c==null && objRI.Other__c==True)
          {
              IsError=True;  
               if (objRI.Other__c==True) 
               {   
                showme=True;
                }
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLOCT13RR59));
             
             pr = null;
          }
          else
          {
          
         system.debug (' ## before Update ##' + objRI);
         system.debug (' ## before Update - Return Center ##' + objRI.ReturnCenter__c);
         system.debug (' ## before Update - CopyTEXExpertCenter ##' + objRI.CopyTEXExpertCenter__c);

         if(objRI.CopyTEXExpertCenter__c==True)
         {
           // APRIL2014 RELEASE - SUKUMAR SALLA
           if(objRMA.TEX__r.ExpertCenter__r.Location_Type__c!='Return Center')
           {
               IsError=True;
               ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLAPR14I2P19));
               pr = null;
           }
           else
           {
                objRI.ReturnCenter__c=objRMA.TEX__r.ExpertCenter__c;
                //Upsert objRI;
                system.debug (' ## RI upon Save ##' + objRI);
                system.debug (' ## After Update - Return Center ##' + objRI.ReturnCenter__c);
                pr = new PageReference('/'+ objRMA.id);
                pr.setRedirect(true);
           }
         }
         else
         {
              pr = new PageReference('/'+ objRMA.id);
                pr.setRedirect(true);
         }
       }
         Upsert objRI;
         system.debug ('## pagref ##'+ pr);
         return pr;
     }
     
    Public Pagereference doCancel()
     {
         If (IsTEX==True)
         {
         string TEXId = objRMA.TEX__c;
         if(IsRMA==True)
         {
           delete objRMA;
           pr = new PageReference('/'+ TEXId);
         }
         else
         {
           delete objRI;
           string RMAId1= objRI.RMA__c;
           pr = new PageReference('/'+ RMAId1);
         }
         
         }
         ELSE If (IsRMA==True)
         {
         string CaseId = objRMA.Case__c;
         delete objRMA;
         pr = new PageReference('/'+ CaseId);
         }
         else
         {
         string RMAId= objRI.RMA__c;
         delete objRI;
         pr = new PageReference('/'+ RMAId);
         }
         pr.setRedirect(true);
         return pr;
     }
     
      Public List<RMAShippingToAdmin__c> checkUniqueness(List<RMAShippingToAdmin__c> preUniqunessCheckRCsList)
      {
         Map<String, RMAShippingToAdmin__c> keyObejctMap= new Map<String, RMAShippingToAdmin__c>();
                set <Id> LCids = new set <Id> ();
                Set <string> MasterKey = new set <string> ();
                for(RMAShippingToAdmin__c LCs: preUniqunessCheckRCsList)
                {
                    String key1;       
                    key1 =LCs.ReturnCenter__c;
                    system.debug('key1'+ key1);
                    String strCAP= LCs.Capability__c;
                    list <STRING> lsCapabilites = new list <string>();
                    lsCapabilites = strCAP.split(';');
                    for (String s: lsCapabilites )
                            {
                                string tempKey = key1+s;
                                MasterKey.add(tempKey);
                                LCids.add(LCs.Id);
                                keyObejctMap.put(tempKey , LCs);
                            }
                     
                    // system.debug('key'+key);
                    // keyObejctMap.put(key , LCs);
                }
                for( String mk: MasterKey )
                {
                        if(keyObejctMap.containsKey(mk))
                        {
                            RMAShippingToAdmin__c exObj = keyObejctMap.get(mk);
                            if ( LCids.contains(exObj.id))
                            {
                                 lstRtnCenters.add(keyObejctMap.get(mk));
                                 LCids.remove(exObj.id);
                            }
                        }    
                 }  
                return  lstRtnCenters;   
      }
    

}