@isTest
Public class VFC_AMPBubble_TEST
{
    public static testmethod void testBubble1()
    {
        AccountMasterProfile__c am=new AccountMasterProfile__c();
        Pagereference pg = new pagereference('/apex/VFP_AMPBubble'); 
        Test.setCurrentPageReference(pg);
        VFC_AMPBubble ampBubble = new VFC_AMPBubble(new ApexPages.StandardController(am)); 
        ampBubble.getBubbles();
    }
     public static testmethod void testBubble2()
     {   
        Account a = Utils_TestMethods.createAccount();
        insert a;
        AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600,Score__c=20);
        insert amp;
        Pagereference pg = new pagereference('/apex/VFP_AMPBubble?ampID='+amp.id); 
        Test.setCurrentPageReference(pg);
        VFC_AMPBubble ampBubble = new VFC_AMPBubble(new ApexPages.StandardController(amp)); 
        
        ampBubble.bTestFlag = false;
        ampBubble.getBubbles();
        ampBubble.getFlashVars();
        
        ampBubble.bTestFlag = true;
        ampBubble.getBubbles();
        ampBubble.getFlashVars();
    }
}