global class VFC_ReferentialDataMigration{
    public Integer intLmtNumber{get;set;}{intLmtNumber=50;}
    public Integer intBatchSize{get;set;}{intBatchSize=50;}
    
    public boolean canRunBatch{get;set;}{canRunBatch=false;}
    public Id batchId{get;set;}{batchId=null;}
    public Set<Id> setBatchId = new Set<Id>();
    public String updateStatus{get;set;}
    //public List<String> sObjectForMigration{get;set;}{sObjectForMigration=new List<String>();}
    public String sObjectForMigration{get;set;}
    

    public VFC_ReferentialDataMigration(){
        for(User user:[select id from User where Id=:UserInfo.getUserId() and BypassVR__c=True and BypassWF__c=True])
            canRunBatch=true;    
    }

    

    public void ProcessBatch(){
        if(canRunBatch)
            if(sObjectForMigration!=null && updateStatus !=null){
                /*
                if(sObjectForMigration.equals('Case')){    
                    batchId = Database.executeBatch(new Batch_ReferentialCaseDataMigration(updateStatus,intLmtNumber), intBatchSize);     
                    setBatchId.add(batchId); 
                }
                else if(sObjectForMigration.equals('Opportunity Line')){
                    batchId = Database.executeBatch(new Batch_ReferentialCaseDataMigration(updateStatus,intLmtNumber), intBatchSize);     
                    setBatchId.add(batchId);
                }
                */
                if(sObjectForMigration.equals('PAMandCompetition')){
                    batchId = Database.executeBatch(new Batch_PAMDataMigration(intLmtNumber), intBatchSize);     
                    setBatchId.add(batchId);
                }
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,'Select an Object and Status'));
            }
    }
    public void statusupdate(){
        getaaj();
    }
    public AsyncApexJob getaaj(){
        List<AsyncApexJob> jobs=[SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors 
                    FROM AsyncApexJob WHERE ID IN: setBatchId];
                    if(jobs.size()==1)
                    return jobs[0];
                    else
                    return null;
    }

    public List<SelectOption> getStatus() {
        List<SelectOption> options = new List<SelectOption>();
            if(sObjectForMigration!=null){
                if(sObjectForMigration.equals('Case')){
                    options.add(new SelectOption('Open','Open'));
                    options.add(new SelectOption('Close','Close'));
                    options.add(new SelectOption('All','All'));
                }
                else if(sObjectForMigration.equals('Opportunity Line')){
                    options.add(new SelectOption('1 - Understand Business Context','1 - Understand Business Context'));
                    options.add(new SelectOption('2 - Define Opportunity Portfolio','2 - Define Opportunity Portfolio'));
                    options.add(new SelectOption('3 - Identify & Qualify','3 - Identify & Qualify'));
                    options.add(new SelectOption('4 - Influence & Develop','4 - Influence & Develop'));
                    options.add(new SelectOption('5 - Prepare & Bid','5 - Prepare & Bid'));
                    options.add(new SelectOption('6 - Negotiate & Win','6 - Negotiate & Win'));
                    options.add(new SelectOption('7 - Deliver & Validate','7 - Deliver & Validate'));
                    options.add(new SelectOption('0 - Closed','0 - Closed'));
                }
                else 
                    options.add(new SelectOption('All','All'));
            }
            return options;
    }
    
    public List<SelectOption> getSObjects() {
        List<SelectOption> options = new List<SelectOption>();    
        options.add(new SelectOption('Case','Case'));
        options.add(new SelectOption('Opportunity Line','Opportunity Line'));
        options.add(new SelectOption('PAMandCompetition','PAMandCompetition'));
        return options;
    }
    
}