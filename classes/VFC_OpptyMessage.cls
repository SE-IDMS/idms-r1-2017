public with sharing class VFC_OpptyMessage {

    public boolean displayErrorMessage{get;set;}{displayErrorMessage=false;}
    public boolean displayOLCurrencyMismatchMessage{get;set;}{displayOLCurrencyMismatchMessage=false;}
    public boolean displayNoContactforVcpmessage{get;set;}{displayNoContactforVcpmessage=false;}
    public boolean displayPCTStage3Message{get;set;}{displayPCTStage3Message=false;}
    public boolean displayPCTStage4Message{get;set;}{displayPCTStage4Message=false;}
    public boolean displayPCTStage5Message{get;set;}{displayPCTStage5Message=false;}
    public boolean displayPCTStage6Message{get;set;}{displayPCTStage6Message=false;}
    public boolean displayPCTStage7Message{get;set;}{displayPCTStage7Message=false;}
    private Opportunity Opptyrecord=null;
    public VFC_OpptyMessage(ApexPages.StandardController controller) 
    {
        Opptyrecord=(Opportunity)controller.getRecord();                
        if(Opptyrecord != Null)
        {
            ID opptyAccount = Opptyrecord.AccountId;
            String opptyCurr = Opptyrecord.CurrencyIsoCode;
            String opptyStage = Opptyrecord.StageName;
            String opptyType = Opptyrecord.OpptyType__c;
            Decimal opptyEuroAmt = Opptyrecord.TECH_AmountEUR__c;
            String pctS0 = Opptyrecord.ProjectCategoryS0__c;
            String pctS1 = Opptyrecord.ProjectCategoryS1__c;
            String pctP0 = Opptyrecord.ProjectCategoryP0__c;
            
            // Display a warning message if the Oportunity Account has no Contact associated with it.
            List<Contact> con =[Select id From Contact Where AccountId=:opptyAccount ];
            if(con.size()<=0)
                    displayErrorMessage=true;
                    
             // Display a warning message for all the type of opportunity and all the stages except to be deleted status.
             Integer opptyLineCurrencyCheck = [SELECT count() FROM OPP_ProductLine__c WHERE Opportunity__c =:Opptyrecord.id AND CurrencyIsoCOde <> :opptyCurr AND LineStatus__c <> :System.Label.CL00139];
                 if(opptyLineCurrencyCheck>0)
                     displayOLCurrencyMismatchMessage=true;
             
             // Display a warning message if the Opportunity with atleast one Value Chain Player that has no contact associated with it.         
             //if(Opptyrecord.StageName == '5 - Prepare & Bid')
             Integer vcpContactCheck = [SELECT count() FROM OPP_ValueChainPlayers__c WHERE OpportunityName__c =:Opptyrecord.id AND Contact__c = NULL];
                 if(vcpContactCheck>0)  
                     displayNoContactforVcpmessage=true;
             
             // Display a warning message to enhance Project Category Tool usage
                 // Stage 3             
             if( (opptyStage == System.Label.CLMAR16SLS01) && (opptyType == System.Label.CL00240 || (opptyType == System.Label.CL00724 && opptyEuroAmt >= Integer.valueOf(Label.CLMAR16SLS57)))  && String.isBlank(pctS0) )
                  displayPCTStage3Message=true;
                 //Stage 4
             if( (opptyStage == System.Label.CLMAR16SLS02) && (opptyType == System.Label.CL00240 || (opptyType == System.Label.CL00724 && opptyEuroAmt >= Integer.valueOf(Label.CLMAR16SLS57))) && String.isBlank(pctS1) )
                  displayPCTStage4Message=true;  
                 //Stage 5
             if( (opptyStage == System.Label.CLMAR16SLS03) && (opptyType == System.Label.CL00240 || (opptyType == System.Label.CL00724 && opptyEuroAmt >= Integer.valueOf(Label.CLMAR16SLS57))) && String.isBlank(pctS1) )
                  displayPCTStage5Message=true;
                 //Stage 6
             if( (opptyStage == System.Label.CLMAR16SLS04) && (opptyType == System.Label.CL00240 || (opptyType == System.Label.CL00724 && opptyEuroAmt >= Integer.valueOf(Label.CLMAR16SLS57))) && String.isBlank(pctP0) )
                  displayPCTStage6Message=true;
                 //Stage 7
             if( (opptyStage == System.Label.CLMAR16SLS05) && (opptyType == System.Label.CL00240 || (opptyType == System.Label.CL00724 && opptyEuroAmt >= Integer.valueOf(Label.CLMAR16SLS57))) && String.isBlank(pctP0) )
                  displayPCTStage7Message=true;
        }
    }
}