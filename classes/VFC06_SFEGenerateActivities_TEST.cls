/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 22/10/2010
    Description     : Test class for Controller VFC06_SFEGenerateActivities.
*/
@isTest
private class VFC06_SFEGenerateActivities_TEST {
    static testMethod void myUnitTest() {   
        User runAsUser = Utils_TestMethods.createStandardUser('CAPTEST1');
        runAsUser.BypassVR__c=True;
        runAsUser.CAPRelevant__c=True;
        List<String> profilePrefixList=(System.Label.CL00632).split(',');//the valid profile prefix list is populated            
            if(profilePrefixList.size()>0){
            List<Profile> allProfiles=Database.query('select id from Profile where Name Like \''+profilePrefixList[0]+'%\' limit 1');
                if(allProfiles.size()>0)
                {
                    runAsUser.ProfileId=allProfiles[0].id;
                }
            }
        
        insert runAsUser;
        
        System.runAs(runAsUser){
            //SFE_IndivCAP__c indivCAP = Utils_TestMethods.createIndividualActionPlan(runAsUser.Id);   

            SFE_IndivCAP__c indivCAP = new SFE_IndivCAP__c(Name='TEST_Individual Commercial Action Plan', 
                                                    StartDate__c = date.newInstance(2011, 1, 15),
                                                    EndDate__c = date.newInstance(2012, 1, 15),
                                                    AssignedTo__c = runAsUser.Id, //assignedToId,
                                                    MktShareThreshold1__c = 90,
                                                    MktShareThreshold2__c = 50,
                                                    AttractiAxisThreshold1__c = 33,
                                                    AttractiAxisThreshold2__c = 66,                                                   
                                                    
                                                    S1VisitFreq__c = 1,
                                                    S2VisitFreq__c = 2,
                                                    S3VisitFreq__c = 3,
                                                    
                                                    G1VisitFreq__c = 1,
                                                    G2VisitFreq__c = 2,
                                                    G3VisitFreq__c = 3,
                                                    
                                                    Q1VisitFreq__c = 1,
                                                    Q2VisitFreq__c = 2,
                                                    Q3VisitFreq__c = 3,
                                                    
                                                    CriterWeightQu1__c = 5,
                                                    CriterWeightQu2__c = 4,
                                                    CriterWeightQu3__c = 5,
                                                    CriterWeightQu4__c = 4,
                                                    CriterWeightQu5__c = 5,
                                                    CriterWeightQu6__c = 4,
                                                    CriterWeightQu7__c = 5,
                                                    CriterWeightQu8__c = 4);            
            insert indivCAP;
            
            /* Modified by - Bhuvana Subramaniyan on 18/04/2011 for REQ-0016 - Platforming scoring improvement
            */
            SFE_IndivCAP__c indivCAP1 = Utils_TestMethods.createIndividualActionPlanShrtInterval(runAsUser.Id);
            insert indivCAP1;
            SFE_IndivCAP__c indivCAP2 = Utils_TestMethods.createIndividualActionPlanLngInterval(runAsUser.Id);
            insert indivCAP2;
    
            // End     
            SFE_IndivCAP__c ss = [select Id, OwnerId from SFE_IndivCAP__c where Id = : indivCAP.Id];
            System.debug('###' + ss.OwnerId + '_' + runAsUser.Id);
    
          
            
            List<Account> lacc = new List<Account>();  // 9 accounts
            for(Integer i = 0; i < 1; i ++)  
            {
                Account acc = Utils_TestMethods.createAccount();
                lacc.add(acc);
            }
            //insert lacc;
            Database.DMLOptions objDMLOptions = new Database.DMLOptions();
            objDMLOptions.DuplicateRuleHeader.AllowSave = true;
            List<Database.SaveResult> accountSaveResult = Database.insert(lacc,objDMLOptions); 
            system.debug('....lacc 0000....'+lacc.size());
        
            List<SFE_PlatformingScoring__c> lps = new List<SFE_PlatformingScoring__c>();
            Integer i = 0;
            // ensure we create data that covers all CustomerProfile quadrants (Q1, Q2 etc.)
            
            for(Account a : lacc)
            {
                if(i == 0)
                {
                    SFE_PlatformingScoring__c platformScoring1= new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                PlatformedAccount__c = a.Id, 
                                                                PAMPlaforming__c = 20000,
                                                                Q1Rating__c = '2',
                                                                Q2Rating__c = '2',
                                                                Q3Rating__c = '2',      
                                                                Q4Rating__c = '2',
                                                                Q5Rating__c = '2',  
                                                                Q6Rating__c = '2',
                                                                Q7Rating__c = '2',
                                                                Q8Rating__c = '2',
                                                                DirectSalesPlatforming__c = 4000,
                                                                MktShare__c=91,
                                                                Score__c=90);
                    //lps.add(platformScoring1);
                    insert platformScoring1;
                    SFE_PlatformingScoring__c platformScoring2=[select CustomerProfile__c from SFE_PlatformingScoring__c where id= :platformScoring1.id];
                    system.debug('...cust prof....'+platformScoring2.CustomerProfile__c);
                }
            }
            
            
            /***
            for(Account a : lacc)
            {
                system.debug('...value i...==='+i);
              if(i == 0)
              {
              system.debug('...in if...');
                    
                    
                    SFE_PlatformingScoring__c platformScoring1= new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                PlatformedAccount__c = a.Id, 
                                                                PAMPlaforming__c = 20000,
                                                                Q1Rating__c = '2',
                                                                Q2Rating__c = '2',
                                                                Q3Rating__c = '2',      
                                                                Q4Rating__c = '2',
                                                                Q5Rating__c = '2',  
                                                                Q6Rating__c = '2',
                                                                Q7Rating__c = '2',
                                                                Q8Rating__c = '2',
                                                                DirectSalesPlatforming__c = 4000);
                    lps.add(platformScoring1);
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 97, '0.9')); 
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP1.Id, a.Id, 97, '0.9'));                    
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP2.Id, a.Id, 97, '0.9'));                    
                    
                    // End
              }else if(i == 1){
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 97, '100'));                    
                  }else if(i == 2){
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 97, '100'));
                  }else if(i == 3){
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 99, '0.9'));
                  }else if(i == 4){
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 99, '10'));
                  }else if(i == 5){
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 99, '100'));
                  }else if(i == 6){
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 98, '0.9'));
                  }else if(i == 7){
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 98, '10'));
                  }else if(i == 8){
                    lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 98, '100'));
                  }else{
                lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 98, '100'));
              }
              i++;
            }
            ***/
            
           // insert lps;
            // initialise controller etc.
            PageReference pageRef = Page.VFP06_SFEGenerateActivities;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', indivCAP.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(indivCAP);
            VFC06_SFEGenerateActivities c = new VFC06_SFEGenerateActivities(sc);

    // Checking if parallel jobs are running before calling create events to avoid Hitting the limit of 5 
            
    Integer numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
    if(numBatchJobs<5 && test.isRunningTest())
//call that create events

            c.createEvents();
            c.bTestFlag = true;      // ensures code in SaveResult loop is covered even though no error thrown
            
   // Checking if parallel jobs are running before calling create events to avoid Hitting the limit of 5 
            
    numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
    if(numBatchJobs<5 && test.isRunningTest())
//call that create events
            
            // Commented by Ram to solve the multiple batch error --> c.createEvents();
            c.checkUserIsOwner();   // coverage of VF pages method reference by 'action' param in page tag.  
 
            /* Modified by - Bhuvana Subramaniyan on 18/04/2011 for REQ-0016 - Platforming scoring improvement
            */
 
            ApexPages.currentPage().getParameters().put('id', indivCAP1.Id);
            sc = new ApexPages.StandardController(indivCAP1);
            c = new VFC06_SFEGenerateActivities(sc);
            
     // Checking if parallel jobs are running before calling create events to avoid Hitting the limit of 5 
            
    numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
    if(numBatchJobs<5 && test.isRunningTest())
//call that create events 
            
            // Commented by Ram to solve the multiple batch error --> c.createEvents();
            c.bTestFlag = true;      // ensures code in SaveResult loop is covered even though no error thrown
          //  c.createEvents();
            c.checkUserIsOwner();   // coverage of VF pages method reference by 'action' param in page tag.  
 
            ApexPages.currentPage().getParameters().put('id', indivCAP2.Id);
            sc = new ApexPages.StandardController(indivCAP2);
            c = new VFC06_SFEGenerateActivities(sc);
            
   // Checking if parallel jobs are running before calling create events to avoid Hitting the limit of 5 
            
    numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
    if(numBatchJobs<5 && test.isRunningTest())
//call that create events
            // Commented by Ram to solve the multiple batch error --> c.createEvents();
            c.bTestFlag = true;      // ensures code in SaveResult loop is covered even though no error thrown
        //    c.createEvents();
            c.checkUserIsOwner();   // coverage of VF pages method reference by 'action' param in page tag.  
            //End                 
            // create another Individual Commercial Action with an assigned to user that has a AvTimeVisit__c time on their user record.  Needed to attain full coverage.
            User userNew = Utils_TestMethods.createStandardUser('tst');
            userNew.AvTimeVisit__c = 3;
            insert userNew;
            indivCAP = Utils_TestMethods.createIndividualActionPlan(userNew.Id);
            insert indivCAP;
            
            lacc = new List<Account>();
            for(i = 0; i < 15; i ++){
                Account acc = Utils_TestMethods.createAccount();
                lacc.add(acc);
            }
            Database.DMLOptions objDMLOptions1 = new Database.DMLOptions();
            objDMLOptions1.DuplicateRuleHeader.AllowSave = true;
            List<Database.SaveResult> accountSaveResults = Database.insert(lacc,objDMLOptions1); 
            //insert lacc;
        
            lps = new List<SFE_PlatformingScoring__c>();
            // ensure we create data that covers all CustomerProfile quadrants (Q1, Q2 etc.)
            for(Account a : lacc){
                lps.add(Utils_TestMethods.createPlatformScoring(indivCAP.Id, a.Id, 97, '0.9'));
            }
            
            ApexPages.currentPage().getParameters().put('id', indivCAP.Id);
            sc = new ApexPages.StandardController(indivCAP);
            c = new VFC06_SFEGenerateActivities(sc);
            
   // Checking if parallel jobs are running before calling create events to avoid Hitting the limit of 5 
            
    //numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
    //if(numBatchJobs<5 && test.isRunningTest())
//call that create events
            //c.createEvents();

            
            // return property bNoErrors for coverage
            //Boolean bTest = c.bNoErrors;
        }
    }
/*    static testMethod void myUnitTest1()
    {
        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        User runAsUser;
        System.runAs(sysadmin2)
        {
            UserandPermissionSets runAsUseruserandpermissionsetrecord=new UserandPermissionSets('Users','SE - Sales Manager');
             runAsUser= runAsUseruserandpermissionsetrecord.userrecord;
             runAsUser.BypassVR__c=True;
             runAsUser.CAPRelevant__c=True; 
             runAsUser.isActive=true;
            Database.insert(runAsUser);
        
            List<PermissionSetAssignment> permissionsetassignmentlstforrunAsUser=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:runAsUseruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforrunAsUser.add(new PermissionSetAssignment(AssigneeId=runAsUser.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforrunAsUser.size()>0)
            Database.insert(permissionsetassignmentlstforrunAsUser);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }


        // User runAsUser = Utils_TestMethods.createStandardUser('tst1');
        // runAsUser.BypassVR__c=True;
        // runAsUser.CAPRelevant__c=True; 
        // runAsUser.isActive=true;
        
        // List<String> profilePrefixList=(System.Label.CLSEP12SLS26).split(',');//the valid profile prefix list is populated            
        // if(profilePrefixList.size()>0)
        // {
        //     List<Profile> allProfiles=Database.query('select id from Profile where Name Like \''+profilePrefixList[0]+'%\' limit 1');           
        //     if(allProfiles.size()>0)
        //     {
        //         runAsUser.ProfileId=allProfiles[0].id;
        //     }
        // }
        User[] usr=[select id from User where User.Profile.Name='System Administrator' AND isActive=true limit 1 ];
        
  //      insert runAsUser;
        System.runAs(runAsUser)
        {
            List<Account> lacc = new List<Account>();
            Account acc = Utils_TestMethods.createAccount();
            lacc.add(acc);            
            insert lacc;
            
            SFE_IndivCAP__c indivCAP = Utils_TestMethods.createIndividualActionPlan(runAsUser.Id);
            indivCAP.G1VisitFreq__c = null;
            indivCAP.G2VisitFreq__c = null;
            indivCAP.G3VisitFreq__c = null;
            indivCAP.Q1VisitFreq__c = null;
            indivCAP.Q2VisitFreq__c = null;
            indivCAP.Q3VisitFreq__c = null;
            indivCAP.S1VisitFreq__c = null;
            indivCAP.S2VisitFreq__c = null;
            indivCAP.S3VisitFreq__c = null;
            indivCAP.OwnerId=usr[0].id;                 
            insert indivCAP;  
            
            
            
            
            PageReference pageRef = Page.VFP06_SFEGenerateActivities;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', indivCAP.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(indivCAP);
            VFC06_SFEGenerateActivities tempCntrlr = new VFC06_SFEGenerateActivities(sc);
            tempCntrlr.checkUserIsOwner();
            
            Account acc1 = Utils_TestMethods.createAccount();                     
            insert acc1;
            SFE_IndivCAP__c indivCAP1 = new SFE_IndivCAP__c(Name='TEST_Individual Commercial Action Plan', 
                                                    StartDate__c = date.newInstance(2012, 12, 1),
                                                    EndDate__c = date.newInstance(2012, 12, 29),
                                                    AssignedTo__c = runAsUser.Id,
                                                    MktShareThreshold1__c = 98,
                                                    MktShareThreshold2__c = 99,
                                                    AttractiAxisThreshold1__c = 2,
                                                    AttractiAxisThreshold2__c = 20,                                                    
                                                    PlatformingScoringsCompleted__c=true,
                                                    TECH_IsEventsGenerated__c=true);
        insert indivCAP1;   
        ApexPages.StandardController sc1 = new ApexPages.StandardController(indivCAP1);
        VFC06_SFEGenerateActivities tempCntrlr1 = new VFC06_SFEGenerateActivities(sc1);
        tempCntrlr1.checkUserIsOwner();
        
        
            
        }
    }*/
}