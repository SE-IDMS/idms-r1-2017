/**
* @author Agustín Gallardo
* @date 11/10/2014
* @description Class to test the menuCloneBackEndController
*/
@isTest
public class Fielo_MenuCloneBackEndControllerTest {
    
     public static testMethod void test(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
    
            //Create a member, a menu, subMenu, Sections and some components
            
            insert new CS_PRM_RichTextFields__c(
                Name = 'Banner-Share', 
                Fields__c = 'F_PRM_ContentFilter__c',
                ObjectAPIName__c ='FieloEE__Banner__Share'
            );
    
            FieloEE.MockUpFactory.setCustomProperties(false);
            Apexpages.currentPage().getParameters().put('parameterMenuTest', 'test');
            //first menu creation
            FieloEE__Menu__c menu1 = new FieloEE__Menu__c(
                Name = 'test menu 1', 
                FieloEE__Home__c = true, 
                FieloEE__Visibility__c ='Both', 
                FieloEE__Placement__c = 'Main',
                FieloEE__ExternalName__c = 'menu1' + datetime.now()
            );
            insert menu1;
    
            //second menu creation
            FieloEE__Menu__c menu2 = new FieloEE__Menu__c(
                Name = 'test menu 2', 
                FieloEE__Home__c = true, 
                FieloEE__Visibility__c ='Both', 
                FieloEE__Placement__c = 'Main',
                FieloEE__ExternalName__c = 'menu2' + datetime.now()
            );
            insert menu2;
    
            //first subMenu creation
            FieloEE__Menu__c subMenu1 = new FieloEE__Menu__c(
                Name = 'test subMenu 1', 
                FieloEE__Home__c = true, 
                FieloEE__Visibility__c ='Both', 
                FieloEE__Placement__c = 'Main', 
                FieloEE__Menu__c = menu1.Id,
                FieloEE__ExternalName__c = 'subMenu1' + datetime.now()
            );
            insert subMenu1;
    
            //second subMenu creation
            FieloEE__Menu__c subMenu2 = new FieloEE__Menu__c(
                Name = 'test subMenu 2', 
                FieloEE__Home__c = true, 
                FieloEE__Visibility__c ='Both', 
                FieloEE__Placement__c = 'Main', 
                FieloEE__Menu__c = menu2.Id,
                FieloEE__ExternalName__c = 'subMenu2' + datetime.now()
            );
            insert subMenu2;
            
            //first menu sections
            FieloEE__Section__c section1 = new FieloEE__Section__c(
                FieloEE__Menu__c = menu1.Id, 
                FieloEE__Type__c = '12'
            );
            insert section1;
            FieloEE__Section__c section2 = new FieloEE__Section__c(
                FieloEE__Menu__c = menu1.Id, 
                FieloEE__Type__c = '12', 
                FieloEE__Parent__c = section1.Id
            );
            insert section2;
            FieloEE__Section__c section3 = new FieloEE__Section__c(
                FieloEE__Menu__c = menu1.Id, 
                FieloEE__Type__c = '12', 
                FieloEE__Parent__c = section1.Id
            );
            insert section3;
            FieloEE__Component__c compBanner1 = new FieloEE__Component__c(
                FieloEE__Section__c = section2.Id, 
                FieloEE__Menu__c = menu1.Id
            );
            
            //second menu sections
            FieloEE__Section__c section4 = new FieloEE__Section__c(
                FieloEE__Menu__c = menu2.Id, 
                FieloEE__Type__c = '12'
            );
            insert section4;
            FieloEE__Section__c section5 = new FieloEE__Section__c(
                FieloEE__Menu__c = menu2.Id, 
                FieloEE__Type__c = '12', FieloEE__Parent__c = section4.Id
            );
            insert section5;
            FieloEE__Section__c section6 = new FieloEE__Section__c(
                FieloEE__Menu__c = menu2.Id, 
                FieloEE__Type__c = '12', 
                FieloEE__Parent__c = section4.Id
            );
            insert section6;
            FieloEE__Component__c compBanner2 = new FieloEE__Component__c(
                FieloEE__Section__c = section5.Id, 
                FieloEE__Menu__c = menu2.Id
            );
    
            //first menu components settings
            compBanner1.FieloEE__Mobile__c = true;
            compBanner1.FieloEE__Tablet__c = false;
            compBanner1.FieloEE__Desktop__c = false;
            insert compBanner1;
            
            FieloEE__Component__c compNews1 = new FieloEE__Component__c(
                FieloEE__Section__c = section3.Id, 
                FieloEE__Menu__c = menu1.Id
            );
            
            compNews1.FieloEE__Mobile__c = true;
            compNews1.FieloEE__Tablet__c = false;
            compNews1.FieloEE__Desktop__c = false;
            insert compNews1;
            
            //creates a content feed
            FieloEE__News__c news1 = new FieloEE__News__c(
                FieloEE__Category__c = 'test news 1', 
                FieloEE__Type__c = 'test', 
                FieloEE__isActive__c = true, 
                FieloEE__Component__c = compNews1.Id
            );
            insert news1;
    
            //creates a banner
            FieloEE__Banner__c ban1 = new FieloEE__Banner__c(
                Name = 'testBanner1', 
                FieloEE__Placement__c = 'Home', 
                FieloEE__Component__c = compBanner1.Id
            );
            insert ban1;
    
            //creates a banner with the /banner/top.jpg attachment, to simulate mockupBanner
            FieloEE__Banner__c ban2 = new FieloEE__Banner__c(
                Name = 'testBanner2', 
                FieloEE__Placement__c = 'Home', 
                FieloEE__Component__c = compBanner1.Id, 
                FieloEE__attachmentId__c = '/banner/top.jpg', 
                FieloEE__isActive__c = true
            );
            insert ban2;
            
            //attachs an image to the banner
            Attachment attachment1 = new Attachment(
                body = Blob.valueOf( 'this is an attachment test' ),
                name = 'fake attachment',
                parentId = ban1.id       
            );
            insert attachment1;
            
            //activate the banner
            ban1.FieloEE__attachmentId__c = attachment1.Id;
            ban1.FieloEE__isActive__c = true;
            update ban1;
    
            //second menu components settings
            compBanner2.FieloEE__Mobile__c = true;
            compBanner2.FieloEE__Tablet__c = false;
            compBanner2.FieloEE__Desktop__c = false;
            insert compBanner2;
            
            FieloEE__Component__c compNews2 = new FieloEE__Component__c(
                FieloEE__Section__c = section3.Id, 
                FieloEE__Menu__c = menu1.Id
            );
            
            compNews2.FieloEE__Mobile__c = true;
            compNews2.FieloEE__Tablet__c = false;
            compNews2.FieloEE__Desktop__c = false;
            insert compNews2;
            
            //creates a content feed
            FieloEE__News__c news2 = new FieloEE__News__c(
                FieloEE__Category__c = 'test news 2', 
                FieloEE__Type__c = 'test', 
                FieloEE__isActive__c = true, 
                FieloEE__Component__c = compNews2.Id
            );
            insert news2;
    
            //creates a banner
            FieloEE__Banner__c ban3 = new FieloEE__Banner__c(
                Name = 'testBanner3', FieloEE__Placement__c = 'Home', 
                FieloEE__Component__c = compBanner2.Id
            );
            insert ban3;
    
            //creates a banner with the /banner/top.jpg attachment, to simulate mockupBanner
            FieloEE__Banner__c ban4 = new FieloEE__Banner__c(
                Name = 'testBanner4', 
                FieloEE__Placement__c = 'Home', 
                FieloEE__Component__c = compBanner2.Id, 
                FieloEE__attachmentId__c = '/banner/top.jpg', 
                FieloEE__isActive__c = true
            );
            insert ban4;
            
            //attachs an image to the banner
            Attachment attachment2 = new Attachment(
                body = Blob.valueOf( 'this is an attachment test' ),
                name = 'fake attachment',
                parentId = ban3.id       
            );
            insert attachment2;
            
            //activate the banner
            ban3.FieloEE__attachmentId__c = attachment2.Id;
            ban3.FieloEE__isActive__c = true;
            update ban3;
    
            //create a memory clone of the first menu
            FieloEE__Menu__c menu1Clone = menu1.clone(false, true, false, false);
            menu1Clone.Name = 'newMenu';
            menu1Clone.FieloEE__ExternalName__c = 'menu1Clone' + datetime.now();
    
            //insert the clone
            insert menu1Clone;
    
            ApexPages.currentPage().getParameters().put('menuId', menu1.Id);
    
            Fielo_MenuCloneBackEndController controller = new Fielo_MenuCloneBackEndController();
    
            for(FieloEE__Menu__c menu: [SELECT Id, FieloEE__ExternalName__c FROM FieloEE__Menu__c]){
                system.debug('###menu.FieloEE__ExternalName__c: ' + menu.FieloEE__ExternalName__c);
            }
    
            controller.cloneMenu();
            
        }
    }
        
}