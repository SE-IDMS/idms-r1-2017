/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ServiceMaxCoveredProd_TEST {

    static testMethod void scenarioOneTest() {
    
      Account acc = Utils_TestMethods.createAccount();
      insert acc;
      Country__c c = Utils_TestMethods.createCountry(); 
      insert c;
        
        SVMXC__Site__c site1 = new SVMXC__Site__c(Name = 'TestBatchName',
                                                 SVMXC__City__c = 'Junction City',
                                                 SVMXC__State__c = 'Kansas',
                                                 SVMXC__Street__c = '704 S. Adams',
                                                 SVMXC__Zip__c = '66441',
                                                 LocationCountry__c = c.Id,
                                                 SVMXC__Location_Type__c='Building',
                                                 SVMXC__Account__c=acc.Id,
                                                 TimeZone__c='India');
        insert site1;
        
        SVMXC__Site__c site2 = new SVMXC__Site__c(Name = 'TestBatchName',
                                                 SVMXC__City__c = 'Junction City',
                                                 SVMXC__State__c = 'Kansas',
                                                 SVMXC__Street__c = '704 S. Adams',
                                                 SVMXC__Zip__c = '66441',
                                                 LocationCountry__c = c.Id,
                                                 SVMXC__Location_Type__c='Building',
                                                 SVMXC__Account__c=acc.Id,
                                                 TimeZone__c='India');
        insert site2;
        
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c(Name = 'testIp', SVMXC__Site__c = site1.Id,BrandToCreate__c='schneider-electric',DeviceTypeToCreate__c='Circuit Breaker',SVMXC__Company__c=acc.Id,RangeToCreate__c='Test1',SKUToCreate__c='Test2');
        insert ip1;
        
       SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(Name = 'testIp2', SVMXC__Site__c = site2.Id,BrandToCreate__c='schneider-electric',DeviceTypeToCreate__c='Circuit Breaker',SVMXC__Company__c=acc.Id,
       RangeToCreate__c='Test1',SKUToCreate__c='Test2');
        insert ip2;
        
        SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(Name = 'contract');
        insert contract;
        
        Test.startTest();
        SVMXC__Service_Contract_Products__c covProd = new SVMXC__Service_Contract_Products__c();
        covProd.SVMXC__Service_Contract__c = contract.Id;
        covProd.SVMXC__Installed_Product__c = ip1.Id;
        
        insert covProd;
        
        covProd = [SELECT Id, SVMXC__Installed_Product__c, AssetLocation__c FROM SVMXC__Service_Contract_Products__c WHERE Id = :covProd.Id LIMIT 1];
        
        system.assertEquals(site1.Id, covProd.AssetLocation__c);
        
        covProd.SVMXC__Installed_Product__c = ip2.Id;
        update covProd;
        
        covProd = [SELECT Id, SVMXC__Installed_Product__c, AssetLocation__c FROM SVMXC__Service_Contract_Products__c WHERE Id = :covProd.Id LIMIT 1];
        
        system.assertEquals(site2.Id, covProd.AssetLocation__c);
        
        Test.stopTest();
    }
}