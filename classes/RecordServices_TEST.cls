@IsTest(SeeAllData=true)
public class RecordServices_TEST {
    static TestMethod void testMethod1(){
        App__c app=new App__c();
        app.Name='TestApp';        
        app.Description__c = 'manual steps';
        app.ObjectShortcut__c = 'SDT';
        app.Status__c = 'Draft';
        insert app;
        Step__c step= new Step__c();
        step.app__c=app.ID;        
        step.Name = 'SDT01_STEP';
       // step.RecordType.Name = 'First Step';
        step.actionvalueforapprove__c = 'action1';
        step.actionvalueforreject__c = 'reject1';
        step.approvebuttonlabel__c = 'approve';
        step.rejectbuttonlabel__c= 'reject';
        step.ownerselectionforthisstep__c = 'Manual';
        step.statusvalueonentrytothisstep__c= 'draft';
        step.statusvalueonrejectonthisstep__c = 'reject';
        step.stepnumber__c = 1;
        Insert Step;
        Record__c rec=new Record__c();
        rec.Name='TestRec';
        rec.step__c=Step.Id;
        rec.app__c=App.Id;
        //RecordServices.saveRecord(rec);
        insert rec;
        Field__c fld=new Field__c();
        fld.app__c=app.Id;
        fld.step__c=Step.Id;
        fld.layoutposition__c=1;
        fld.appdatafieldmap__c='textfield25__c';
        Insert fld;
        ofwgroup__c gp=new ofwgroup__c();
        gp.App__c=app.Id;
        gp.TECH_MigrationUniqueID__c='blah';
        insert gp;
        groupappuserjunction__c gpu=new groupappuserjunction__c();
        gpu.Group__c=gp.Id;
        gpu.User__c=UserInfo.getUserId();
        gpu.TECH_MigrationUniqueID__c='blahblah';
        insert gpu;
        List<Step__c> testSteps=RecordServices.getSteps(app.Id);
        Map<Integer,Step__c> testStepsMap=RecordServices.getStepsByNumber(app.Id);
        List<Field__c> fieldList=RecordServices.getFields(Step.Id);   
        List<Field__c> StepNumberList=RecordServices.getFieldsByStepNumber(app.Id,1);
        String rec10=RecordServices.getEntryStatusByStepNumber(app.Id,1);
        String rec1=RecordServices.getEntryStatusByStepId(Step.Id);
        String rec2=RecordServices.getApproveActionByStepNumber(app.Id,1);
        String rec3=RecordServices.getApproveActionByStepId(Step.Id);
        String rec4=RecordServices.getApproveActionByRecordId(rec.Id);
        String rec5=RecordServices.getRejectStatusByRecordId(rec.Id);
        String rec6=RecordServices.getRejectActionByRecordId(rec.Id);
        String rec7=RecordServices.getRecordCurrentStepId(rec.Id);
        String rec8=RecordServices.getRecordCurrentStepId(rec.Id);
        String rec9=RecordServices.getStepTypeByStepId(Step.Id);
        Integer val=RecordServices.getRecordCountByAppId(app.Id);
        //String rec11=RecordServices.saveRecord(rec);
        List<Object> objLst=RecordServices.getQueryData('Select Id,Name,checkboxfield1__c,datefield9__c from Record__c');
        try{
            String rec11=RecordServices.getRecordInfo(rec.Id);
        }
        Catch(Exception ex){}
        User us=new User(Id=UserInfo.getUserId());
        List<User> uslist=new List<User>();
        uslist.add(us);
        String rec15= RecordServices.updateStep(Step.Id,UserInfo.getUserId(),'blah',rec.Id,uslist,uslist,null,gp.Id,false);
        String rec16=RecordServices.rejectTheRecord(Step.Id,rec.Id,'blah',uslist);
        RecordServices.RecordInfo rec12=new RecordServices.RecordInfo();
        Attachment attachment = new Attachment();
        Attachment att1=RecordServices.getAttachmentInfo(null);
        String encodedContentsString ='blah';
        attachment.Body = Blob.valueOf(encodedContentsString);
        attachment.Name = String.valueOf('test.txt');
        attachment.ParentId = app.Id;
        insert attachment;
        
        Attachment att=RecordServices.getAttachmentInfo(attachment.Id);
         List<Record__c> reclist1=RecordServices.getApproveRecords('1');
         List<Record__c> reclist2=RecordServices.getMyRecords(1);
         String rec13=RecordServices.deleteRecord(rec.Id);
         List<Record__c> reclist3=RecordServices.getMyRecycleBin();
         List<Record__c> reclist4=RecordServices.getAllRecycleBin();
         String rec14=RecordServices.undeleteRecord(rec.Id);
        
    }   
}