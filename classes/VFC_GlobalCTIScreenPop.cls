Public Class VFC_GlobalCTIScreenPop{
        Public String ANI;
        Public String CASENumber;
        Public String CFID;
        Public String Ivr{get;set;}
        Public String pinCode{get;set;}
        public Id casedetails{get;set;}
        public list<CTR_ValueChainPlayers__c> CVCPFound{get;set;}
        Public list<case> caserec{get;set;}
        Public String SfUrl{get;set;}
        public string ContactId{get;set;}
        Public String AccountId{get;set;}
        public string ContactRecognition{get;set;}
        Public String SubscriptionID{get;set;}
        Public String CtiSkill{get;set;}
        Public String CountryCode{get;set;}
        Public String SPOC_PhoneNo{get;set;}
  
  Public VFC_GlobalCTIScreenPop(){
        ANI =apexpages.currentpage().getparameters().get('ANI');
        CASENumber =apexpages.currentpage().getparameters().get('CaseId');
        CFID =apexpages.currentpage().getparameters().get('CFID');
        Ivr=apexpages.currentpage().getparameters().get('IVR');  
        pinCode=apexpages.currentpage().getparameters().get('LegacyPin');
        ContactId=apexpages.currentpage().getparameters().get('ContactId');
        AccountId=apexpages.currentpage().getparameters().get('AccountId');
        ContactRecognition=apexpages.currentpage().getparameters().get('ContactRecognition');
        SubscriptionID=apexpages.currentpage().getparameters().get('SubscriptionID');
        CtiSkill=apexpages.currentpage().getparameters().get('CtiSkill');
        CountryCode=apexpages.currentpage().getparameters().get('CountryCode');
        SPOC_PhoneNo=apexpages.currentpage().getparameters().get('SPOC_PhoneNo');
        
        SfUrl = URL.getSalesforceBaseUrl().toExternalForm();
        System.debug('sfurltest'+SfUrl);
    
   }
  Public pagereference pageaction(){
            system.debug('test'+CASENumber);
            system.debug('test'+ContactRecognition);
            system.debug('test'+CFID);
            system.debug('test'+pinCode);
            system.debug('test'+CtiSkill);
             if(CASENumber!=null&&CASENumber!=''){
                   
                    caserec = [select id,CaseNumber  from case where Id=:CASENumber];
                    if(caserec!=null&&!caserec.Isempty()){
                        casedetails = caserec[0].Id;
                        system.debug('CaseNumber'+caserec[0].CaseNumber);
                        
                        system.debug('Entered in CaseRedirect');
                        pagereference CasePageRef= new pagereference('/'+caserec[0].id);
                        CasePageRef.setredirect(true);
                        return CasePageRef;
                        
                   
                    }
                    
                     else{
                       system.debug('Entered in CASE Else');
                       pagereference pageRef= new pagereference('/apex/VFP79_GetContactInformation?ANI='+ANI);
                       pageRef.setredirect(true);
                       return pageRef;
                       
                      }
               }
                else if(pinCode!=null&&pinCode!=''){
                    If(pinCode!=null || pinCode!='' ){
                        List<CTR_ValueChainPlayers__c>  lstCVCP=[Select id,LegacyPIN__c,Contact__c,Account__c,Points__c from CTR_ValueChainPlayers__c where LegacyPIN__c =:pinCode];
                        System.debug('@@@CVCPlist'+lstCVCP);
                        System.debug('@@@CVCPlist'+lstCVCP.size());
                        if(lstCVCP.size()==1){
                            PageReference pageRef=new PageReference(SfUrl +'/'+lstCVCP[0].id);
                            pageRef.setRedirect(true);
                            return pageRef; 
                            system.debug('Entered in Cvcp');  
                        }                   
                        if(lstCVCP.size()==0||lstCVCP.size()>1){
                            PageReference pageRef=new PageReference(SfUrl +Label.CLMAY14CCC28+pinCode);
                            pageRef.setRedirect(true);
                            return pageRef;   
                            system.debug('Entered in CVCP Else ');
                        }
                    }  
                }
             
                else if(ContactRecognition!=null&ContactRecognition=='SingleMatch'){
                    system.debug('Entered in SingleMatch');
                    system.debug('teststttt'+ContactRecognition);
                    if(ContactId!=null&&ContactId!=''){
                        List<Contact>  ContactList=[Select id,name from Contact where Id=:ContactId];
                        System.debug('@@@ContactList'+ContactList);
                        System.debug('@@@ContactList'+ContactList.size());
                        if(ContactList.size()==1){
                            PageReference pageRef=new PageReference(SfUrl +'/'+ContactList[0].id);
                            pageRef.setRedirect(true);
                            return pageRef;   
                            
                        }
                        
                    }
                    else if(AccountId!=null&AccountId!=''){
                   
                    List<Account>  AccoutList=[Select id,name from Account where Id=:AccountId];
                        System.debug('@@@AccoutList'+AccoutList);
                        System.debug('@@@AccoutList'+AccoutList.size());
                        if(AccoutList.size()==1){
                            PageReference pageRef=new PageReference('/'+AccoutList[0].id);
                            pageRef.setRedirect(true);
                            return pageRef;   
                        }
                    }
                    else{
                       if(ANI!=null&ANI!=''){
                            system.debug('Entered in Acccount and ContactNull and ANI IS In');
                            pagereference pageRef= new pagereference('/apex/VFP_ContactSearch?ANI='+ANI);
                            pageRef.setredirect(true);
                            return pageRef;
                            
                        }
                    }
                }
       
                else if(ContactRecognition!=null&ContactRecognition=='NoMatch'){
                        system.debug('Entered in NoMatch');
                        system.debug('teststttt'+ContactRecognition);
                        if (ANI!=null&&ANI!=''){
                        pagereference pageRef= new pagereference('/apex/VFP_ContactSearch?ANI='+ANI);
                        pageRef.setredirect(true);
                        return pageRef;
                        
                        }
                }
                else if(ContactRecognition!=null&ContactRecognition=='NoMatch_TimeOut'){
                  if (ANI!=null&&ANI!=''){
                        system.debug('Entered in NoMatch_TimeOUt');
                        pagereference pageRef= new pagereference('/apex/VFP79_GetContactInformation?ANI='+ANI);
                        pageRef.setredirect(true);
                        return pageRef;
                        
                    }
                 }
                 else if(ContactRecognition!=null&ContactRecognition=='MultiMatch'){
                  if (ANI!=null&&ANI!=''){
                        system.debug('Entered in MultiMatch');
                        pagereference pageRef= new pagereference('/apex/VFP79_GetContactInformation?ANI='+ANI);
                        pageRef.setredirect(true);
                        return pageRef;
                        
                    }
                 }
                 else{
                 system.debug('Entered in MultiMatchElse');
                 pagereference pageRef= new pagereference('/apex/VFP79_GetContactInformation?ANI='+ANI);
                        pageRef.setredirect(true);
                        return pageRef;
                 }
           return null;
    }
}