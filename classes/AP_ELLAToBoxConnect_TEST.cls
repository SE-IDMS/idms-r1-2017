//Comited on autorabit
@istest
public class AP_ELLAToBoxConnect_TEST{
    
    
    static testMethod void folderCreationwithoutInsert(){
        //list<User> userList = AP_EllaProjectForecast_TEST.createUsers();

            User newUser = Utils_TestMethods.createStandardUser('yhrgiusd');
            Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
            newUser.BypassVR__c = TRUE;

            newUser.UserRoleID = rol_ID ;
            newUser.BypassTriggers__c='AP_Offer1;AP_Offer3';    
            //Database.SaveResult UserInsertResult = Database.insert(newUser, false);   

            //system.runas(newUser){

            CS_ELLABoxAccessToken__c cs = new CS_ELLABoxAccessToken__c(Name='Admin',AccessToken__c='DprxSfbropcMvXDOiftQOQYHe2ko516u',
            RefreshToken__c ='ub2Kfgp6mBqQjixI6RWcNVnf45z2CrGz3HJ729JUlahDC8GEvWuu9kS0x1qz6qMO' ,
            BoxAccessTokenExpires__c=datetime.now().addMinutes(45),BoxRefreshTokenExpire__c=datetime.now().adddays(60) );

            insert cs;
            //Offer_Lifecycle__c offerRec = [Select name from Offer_Lifecycle__c where BoxFolderId__c = null and BoxUserEmailId__c != null  limit 1];

            Test.startTest(); 
            Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());
            AP_ELLAToBoxConnect.folderStructureCreation ('string folderName','string parentFolderId','string CollobratorUserEmail' ); 
            //AP_ELLAToBoxConnect.OfferFolderCreationInBox(new set<String>{String.valueOf(offerRec.Id)});
            // system.debug('555555555555'+AP_ELLAToBoxConnect.mock);
            //set<String> projectId = new set<String>();
            //Test.stopTest();
            //Test.startTest(); 
            Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());
            AP_ELLAToBoxConnect.folderStructureCreation ('string folderName',null,'string CollobratorUserEmail1' ); 
            //AP_ELLAToBoxConnect.OfferFolderCreationInBox(new set<String>{String.valueOf(offerRec.Id)});
            // system.debug('555555555555'+AP_ELLAToBoxConnect.mock);
            //set<String> projectId = new set<String>();
            Test.stopTest();
       
    }
    
     static testMethod void folderCreationwithoutInsert1(){
        //list<User> userList = AP_EllaProjectForecast_TEST.createUsers();

            
            CS_ELLABoxAccessToken__c cs = new CS_ELLABoxAccessToken__c(Name='Admin',AccessToken__c='DprxSfbropcMvXDOiftQOQYHe2ko516u',
            RefreshToken__c ='ub2Kfgp6mBqQjixI6RWcNVnf45z2CrGz3HJ729JUlahDC8GEvWuu9kS0x1qz6qMO' ,
            BoxAccessTokenExpires__c=datetime.now().addMinutes(-45),BoxRefreshTokenExpire__c=datetime.now().adddays(60) );

            insert cs;
            //Offer_Lifecycle__c offerRec = [Select name from Offer_Lifecycle__c where BoxFolderId__c = null and BoxUserEmailId__c != null  limit 1];
            
            Test.startTest(); 
            
            
            Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImplNew_TEST(true,false));
            AP_ELLAToBoxConnect.boxCurrentAccessToken();
            
            Test.stopTest();
       
    }
    
     public static testMethod void folderCreationwithoutInsert2(){
        //list<User> userList = AP_EllaProjectForecast_TEST.createUsers();

            
            CS_ELLABoxAccessToken__c cs = new CS_ELLABoxAccessToken__c(Name='Admin',AccessToken__c='DprxSfbropcMvXDOiftQOQYHe2ko516u',
            RefreshToken__c ='ub2Kfgp6mBqQjixI6RWcNVnf45z2CrGz3HJ729JUlahDC8GEvWuu9kS0x1qz6qMO' ,
            BoxAccessTokenExpires__c=datetime.now().addMinutes(-45),BoxRefreshTokenExpire__c=datetime.now().adddays(60) );

            insert cs;
            //Offer_Lifecycle__c offerRec = [Select name from Offer_Lifecycle__c where BoxFolderId__c = null and BoxUserEmailId__c != null  limit 1];
            AP_ELLAToBoxConnect.miniFolderObject  test88= new AP_ELLAToBoxConnect.miniFolderObject();
            test88.type='test';
            test88.id='TEST1';
            test88.sequence_id='56hhh'; 
            test88.etag='78uh';
            test88.sha1='test';
            test88.name='tetstsh';
             AP_ELLAToBoxConnect.miniUserObject  minob= new AP_ELLAToBoxConnect.miniUserObject();
             minob.login='loh899';
             minob.name='name454';
             minob.id='teys67';
             minob.type='Tetst';
              AP_ELLAToBoxConnect.boxErrorObject  errorobj= new AP_ELLAToBoxConnect.boxErrorObject();
             errorobj.type='type--';
             errorobj.status=90;
             errorobj.code='code9oki';
             errorobj.help_url='help_url900';
             errorobj.message='messageiioo';
             errorobj.request_id='898090';
        
            
            Test.startTest(); 
             Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());
            AP_ELLAToBoxConnect.addBoxCollaboraFolder('box_FoldrID','string collbrEmailId') ;
            
            
            
            Test.stopTest();
       
    }
    
   
    
}