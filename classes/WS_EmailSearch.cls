/*
    Author          : Vimal Karunakaran - Global Delivery Team
    Date Created    : 26/02/2013
    Description     : Web Service for Email search on contacts including Consumer Account
                      CTI Integration module for  Oct 2013 Release
    -----------------------------------------------------------------
                              MODIFICATION HISTORY
    -----------------------------------------------------------------
    Modified By     : <>
    Modified Date   : <>
    Description     : <>
    -----------------------------------------------------------------
*/
global class WS_EmailSearch{
    //Define the Customer object that is exposed in the Web Service
    global class Customer{
        webservice String ServiceContractType;
        webservice String Preferred_Agent_Id;
        webservice String Preferred_Agent_Sesa;
        webservice String Preferred_Agent_Backup_Id;
        webservice String Preferred_Agent_Backup_Sesa;
        webservice Id Contact_Id;
        webservice String Contact_Name;
        webservice String Contact_Language;
        webservice String Contact_Support_Level;
        webservice String Contact_ServiceContractType;
        webservice String Contact_PreferredCCTeam;
        webservice String Contact_PTeamCTISkill;
        webservice String Contact_Email;
        webservice Id Account_Id;
        webservice String Account_Name;
        webservice String Account_Classification_Level_1;
        webservice String Account_LeadingBusiness;
        webservice String Account_Key_Account_Type;
        webservice String Account_Support_Level;
        webservice String Account_ServiceContractType;
        webservice String Account_PreferredTeam;
        webservice String Account_PTeamCTISkill;
        webservice String Account_EmailAutoResponse;
        
        //webservice String Account_Preferred_Skill;
        webservice String BFO_Identification;
        webservice Id Case_Id;
        webservice String Case_Number;
        webservice String Case_Owner_Sesa;
        //webservice boolean Account_Inbound_Email_Acknowledge_Opt_out;
        //webservice boolean Account_Case_Creation_Email_Acknowledge_Opt_out;
        webservice String ErrorCode;
        webservice String ErrorLabel;
    }
    
    //Webservice method for retreieving Customer Information
    //from the Email Id passed.
    webService static Customer getCustomerInfoFromEmail(String strEmail) {
        List<List<Sobject>> contactSearchResults = new List<List<Sobject>>();//To hold the SOSL search results for Contact objects
        List<Contact> matchedContacts = new List<Contact>();//To store Contact records from search results
        
        String bFOIdentification;
        Contact selectedContact = null;
        Customer identifiedCustomer = new Customer();
        System.Debug('**** WS_EmailSearch - getCustomerInfoFromEmail BEGIN ****');
        try{
            //Search for Email in Contacts including ConsumerAccount
            contactSearchResults = [FIND :strEmail IN Email FIELDS RETURNING 
            Contact(Id
                WHERE ( NOT Account.Name LIKE '%DO NOT USE%')
                ORDER BY
                Account.ToBeDeleted__c,
                Inactive__c,
                ServiceContractType__c DESC,
                Account.ServiceContractType__c DESC,
                ContactSupportLevel__c, 
                LastModifiedDate DESC limit 2)
            ];
            
            Set<Id> contactIds = new Set<Id>();
            if(contactSearchResults[0] != null){
                System.Debug('Matched contacts count - ' + contactSearchResults[0].size());
                for(Contact contactRecord : (list<Contact>)contactSearchResults[0])
                    contactIds.add(contactRecord.Id);
            }
                        
            matchedContacts = getContactDetails(contactIds);
            
            //1. Logic for matched Contact(s) from Contacts/ANI search
            if(matchedContacts.size() > 0){
                if(matchedContacts.size() > 1)
                    bFOIdentification = System.Label.CLMAY13CCC09;//Set value as 'MultiMatch'
                else
                    bFOIdentification = System.Label.CLMAY13CCC10;//Set value as 'SingleMatch'
                selectedContact = (Contact) matchedContacts[0]; //In case of two matches, this step selects Contact having higher priority
                identifiedCustomer = getContactInfo(selectedContact, bFOIdentification); 
            }
            
            System.Debug('**** WS_EmailSearch - getCustomerInfoFromEmail END ****');
            return identifiedCustomer;  
        }catch(Exception e){
            System.Debug('**** WS_EmailSearch - getCustomerInfoFromEmail Error - ' + e.getMessage() + '****');
            identifiedCustomer.ErrorLabel = System.Label.CLMAY13CCC05 + e.getMessage();//TODO: Validate this implementation
            return identifiedCustomer;
        }
    }
    
    //This method retrieves the Contact Details for the List of Contact IDs passed
    public static List<Contact> getContactDetails(Set<Id> contactIds){
        System.Debug('**** WS_EmailSearch - getContactDetails - BEGIN ****');
        List<Contact> matchedContacts = [SELECT
                Id, 
                Name,
                Email,
                Pagent__c,
                Pagent__r.Id,
                Pagent__r.Username,
                PBAAgent__c,
                PBAAgent__r.Id,
                PBAAgent__r.Username,
                CorrespLang__c,
                CreatedDate,
                AccountId,
                MobilePhone , 
                WorkPhone__c,
                LastModifiedDate,
                ServiceContractType__c,
                ContactSupportLevel__c,
                PreferredCCTeam__R.Name,
                PreferredCCTeam__R.CTI_Skill__c,
                Account.Id,
                Account.Name, 
                Account.AccountSupportLevel__c,
                Account.ServiceContractType__c,
                Account.Preferred_CC_Team__c,
                Account.Preferred_CC_Team__r.CTI_Skill__c,
                Account.EmailAutoResponse__c,
                Account.Pagent__c,
                Account.Pagent__r.Id,
                Account.Pagent__r.Username,
                Account.PBAAgent__c,
                Account.PBAAgent__r.Id,
                Account.PBAAgent__r.Username,
                Account.CreatedDate,
                Account.ClassLevel1__c,
                Account.UltiParentAcc__c,
                Account.UltiParentAcc__r.Name,
                Account.Type,
                Account.LeadingBusiness__c
                FROM Contact WHERE Id IN :contactIds
                ORDER BY
                ServiceContractType__c DESC,
                TECH_CTIRoutingPriority__c DESC,  
                LastModifiedDate DESC];
        System.Debug('**** WS_EmailSearch - getContactDetails - END ****');
        return matchedContacts;
    }
    
    //This method populates and returns the Customer object based on the Contact object 
    //identified for the Email Address  passed by CTI
    public static Customer getContactInfo(Contact selectedContact, String bfoIdentification){
        System.Debug('**** WS_EmailSearch - getContactInfo - BEGIN ****');
        Boolean isAccountVIC = false;
        //Populate Customer object
        Customer identifiedCustomer = new Customer();
        String sesaID = '';//Temporary variable to retrieve SESA from the username field
        
        //Search open cases related to Contact in case of 'SingleMatch'
        if (bFOIdentification == System.Label.CLMAY13CCC10){
            
            try{
                //Retrieve list of open cases that are assigned against the selected contact.
                List<Case> openCases = [SELECT Id, CaseNumber, Owner.Username FROM Case WHERE Contact.Id = :selectedContact.Id AND (Status != 'Closed' AND Status != 'Closed/ Pending Action') LIMIT 2];        
                //If single open case, populate Customer object Case fields
                if(!openCases.isEmpty() && openCases.size() == 1){
                    identifiedCustomer.Case_Id = openCases[0].Id;
                    identifiedCustomer.Case_Number = openCases[0].CaseNumber;
                    sesaID = openCases[0].Owner.Username;
                    identifiedCustomer.Case_Owner_Sesa = (sesaID != null && sesaID.contains('@')) ? sesaID.Left(sesaID.indexOf('@')).toUpperCase() : sesaID;
                }
            }
            catch(Exception e){
                identifiedCustomer.ErrorLabel = System.Label.CLMAY13CCC05 + e.getMessage();//TODO: Validate this implementation
                System.Debug('**** WS_EmailSearch - getContactInfo - Error retrieving case information ****');
            }
        }
        
        identifiedCustomer.Contact_Id = selectedContact.Id;
        identifiedCustomer.Contact_Name = selectedContact.Name;
        identifiedCustomer.Contact_Language = selectedContact.CorrespLang__c;
        identifiedCustomer.Contact_Support_Level = selectedContact.ContactSupportLevel__c;
        identifiedCustomer.Contact_PreferredCCTeam = selectedContact.PreferredCCTeam__R.Name;
        identifiedCustomer.Contact_PTeamCTISkill = selectedContact.PreferredCCTeam__R.CTI_Skill__c;
        identifiedCustomer.Contact_Email = selectedContact.Email;
        identifiedCustomer.Contact_ServiceContractType = selectedContact.ServiceContractType__c;
        
        //Set Account details in Customer object
        identifiedCustomer.Account_Id = selectedContact.Account.Id;
        identifiedCustomer.Account_Name = selectedContact.Account.Name;
        identifiedCustomer.Account_Classification_Level_1 = selectedContact.Account.ClassLevel1__c;
        identifiedCustomer.Account_LeadingBusiness = selectedContact.Account.LeadingBusiness__c;
        identifiedCustomer.Account_Key_Account_Type = selectedContact.Account.Type;
        identifiedCustomer.Account_Support_Level = selectedContact.Account.AccountSupportLevel__c;
        identifiedCustomer.Account_ServiceContractType = selectedContact.Account.ServiceContractType__c;


        identifiedCustomer.Account_PreferredTeam = selectedContact.Account.Preferred_CC_Team__c; 
        identifiedCustomer.Account_PTeamCTISkill = selectedContact.Account.Preferred_CC_Team__r.CTI_Skill__c;
        identifiedCustomer.Account_EmailAutoResponse = selectedContact.Account.EmailAutoResponse__c;
        identifiedCustomer.BFO_Identification = bFOIdentification;
                                          
        if((selectedContact.ServiceContractType__c == null || selectedContact.ServiceContractType__c == '')
           && selectedContact.Account.ServiceContractType__c != System.Label.CLMAY13CCC08){
            //Return standard Contact 
            identifiedCustomer.ServiceContractType = ''; //Return empty string
            System.Debug('**** WS_EmailSearch - getContactInfo - END ****');
            return identifiedCustomer;
        }
        else{
            //Check if Contact Service Contract Type is 'VIC'
            if(selectedContact.ServiceContractType__c == System.Label.CLMAY13CCC08){
                if(selectedContact.Pagent__c != null){
                    identifiedCustomer.ServiceContractType = System.Label.CLMAY13CCC08;//Return VIC account
                }
                else{
                    //Check if Account Service Contract Type is 'VIC'
                    if(selectedContact.Account.ServiceContractType__c == System.Label.CLMAY13CCC08){
                        isAccountVIC = true;//Set flag to indicate return of VIC account related to Contact
                        identifiedCustomer.ServiceContractType = System.Label.CLMAY13CCC08;//Return VIC account
                    }
                    else{
                        //Return standard Contact 
                        identifiedCustomer.ServiceContractType = ''; //Return empty string
                        System.Debug('**** WS_EmailSearch - getContactInfo - END ****');
                        return identifiedCustomer;
                    }
                }
            }
            //Check if Account Service Contract Type is 'VIC'
            else if(selectedContact.Account.ServiceContractType__c == System.Label.CLMAY13CCC08){
                isAccountVIC = true;//Set flag to indicate return of VIC account related to Contact
                identifiedCustomer.ServiceContractType = System.Label.CLMAY13CCC08;//Return VIC account
            }
            //Check if Contact Service Contract Type is 'DAC'
            else if(selectedContact.ServiceContractType__c == System.Label.CLMAY13CCC07){
                //Return DAC Contact 
                identifiedCustomer.ServiceContractType = System.Label.CLMAY13CCC07;//Return DAC Account
            }
            if(!isAccountVIC){
                //Return VIC Contact
                identifiedCustomer.Preferred_Agent_Id = selectedContact.Pagent__r.Id;
                sesaID = selectedContact.Pagent__r.Username;
                identifiedCustomer.Preferred_Agent_Sesa = (sesaID != null && sesaID.contains('@')) ? sesaID.Left(sesaID.indexOf('@')).toUpperCase() : sesaID;
                identifiedCustomer.Preferred_Agent_Backup_Id = selectedContact.PBAAgent__r.Id;
                sesaID = selectedContact.PBAAgent__r.Username;
                identifiedCustomer.Preferred_Agent_Backup_Sesa = (sesaID != null && sesaID.contains('@')) ? sesaID.Left(sesaID.indexOf('@')).toUpperCase() : sesaID;
            }
            else{
                //Return VIC account related to Contact
                identifiedCustomer.Preferred_Agent_Id = selectedContact.Account.Pagent__r.Id;
                sesaID = selectedContact.Account.Pagent__r.Username;
                identifiedCustomer.Preferred_Agent_Sesa = (sesaID != null && sesaID.contains('@')) ? sesaID.Left(sesaID.indexOf('@')).toUpperCase() : sesaID;
                identifiedCustomer.Preferred_Agent_Backup_Id = selectedContact.Account.PBAAgent__r.Id;
                sesaID = selectedContact.Account.PBAAgent__r.Username;
                identifiedCustomer.Preferred_Agent_Backup_Sesa = (sesaID != null && sesaID.contains('@')) ? sesaID.Left(sesaID.indexOf('@')).toUpperCase() : sesaID;
            }
            System.Debug('**** WS_EmailSearch - getContactInfo - END ****');
            return identifiedCustomer;
        }
    }
}