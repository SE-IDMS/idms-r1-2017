/*
    Author: Bhuvana Subramaniyan
    Description: Test class to return mock data for GMR Search 
*/
@isTest
global class WS_MOCK_GMRSearch_TEST implements WebServiceMock 
{
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) 
           {
               WS_GMRSearch.globalFilteredSearchResponse resp = new WS_GMRSearch.globalFilteredSearchResponse();               
               Map<String, WS_GMRSearch.globalFilteredSearchResponse> response_map_x = new Map<String, WS_GMRSearch.globalFilteredSearchResponse>();
               response_map_x.put('response_x', resp);                   
               response.put('response_x', resp);                      
           }
}