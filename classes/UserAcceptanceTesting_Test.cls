/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 23-08-2013
    Description     : Test Class for User Acceptance Testing
*/

@isTest
private class UserAcceptanceTesting_Test {
static testMethod void testUserAcceptance() {
User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('uat1');
        System.runAs(runAsUser){

//Use the PageReference Apex class to instantiate 
        PageReference pageRef = Page.UAT;
       
       //In this case, the Visualforce page named UAT is the starting point of this Test method. 
        Test.setCurrentPage(pageRef);
       

 //Instantiate and construct the controller class. 
    User_Acceptance_Test__c uat = new User_Acceptance_Test__c(Comments__c = 'Test',Data_Inputs__c='Test',Data_Outputs__c='Test',Description__c = 'Test', Status__c='Open');
    insert uat;
    UAT_Results__c ures = new UAT_Results__c(User_Acceptance_Test__c = uat.id, Test_Result__c='Passed');
    insert ures;
 // Invoke the Page
 
    ApexPages.StandardController stanPage= new ApexPages.StandardController(uat);
    User_Acceptance_Test__c uat1 = [select id, name from User_Acceptance_Test__c limit 1];
    
    system.debug('>>>>>>>> ' + uat1.Name);
    ApexPages.currentPage().getParameters().put('id',uat1.Name);
       
       UATController controller = new UATController (stanPage);

    

    controller.getRecid();
    controller.ReceiveRecord();
    controller.SetStatus();
    controller.getUATData();
    controller.getFilterList();
    controller.getFilterRelease();
    controller.FilterChange();
    controller.closePopup();
    controller.showPopup();
    controller.closePopup2();
    controller.Save();
    controller.Next();
    controller.Previous();
    controller.Close();
    controller.ViewHistory();
    controller.getDetTst();
    controller.DisplayTestCaseDetail();
    }
 }

}