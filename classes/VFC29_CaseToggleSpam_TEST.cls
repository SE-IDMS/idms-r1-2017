/*
    Author          : Abhishek (ACN) 
    Date Created    : 10/05/2011
    Description     : Test class for the apex class VFC29_CaseToggleSpam.
*/
@isTest
private class VFC29_CaseToggleSpam_TEST {
    
/* @Method <This method CaseToggleSpamTestMethod is used test the VFC29_CaseToggleSpam class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Throwing an Exception>
*/
    static testMethod void caseToggleSpamTestMethod()
    {          
        Account accounts = Utils_TestMethods.createAccount();
        insert accounts;
        list<Contact> contacts = new list<contact>();

     for(integer i=0;i<100;i++)
        {
        contacts.add(Utils_TestMethods.createContact(accounts.Id,'TestContact'+''+i));
        }
        database.insert(contacts);           
      list<case> cases1 = new list<case>();      
      List<case> cases2 = new List<case>();

      for(integer i=0;i<100;i++)
      {
        Case cases =Utils_TestMethods.createCase(accounts.id,contacts[i].id,'Closed');
        cases.Spam__c=true;
        cases1.add(cases);
      }        
       database.insert(cases1);
       
        //Profile profile = [select id from profile where name LIKE 'SE - Non-sales RITE Approver%'];
        User user = Utils_TestMethods.createStandardUser('TestUser');
                
       for(integer i=0;i<100;i++)
      {
         Case cases =Utils_TestMethods.createCase(accounts.id,contacts[i].id,'Closed');
         cases2.add(cases); 
      }  
       database.insert(cases2);   
       
       /*------------------------------*/
        test.startTest();
        
        System.runAs(user) 
        {
       list<case> caseid = new List<case>();                       
       for(case Spamcases:cases1)
       {                   
           caseid.add(spamcases);
       }           
       for(case Spamcases:cases2)
       {          
           caseid.add(spamcases);
       }   
           Pagereference p = new pagereference('/apex/VFP29_ToggleSpam?retURL=%2F500%3Ffcf%3D00BA0000006JE16'); 
           Test.setCurrentPageReference(p);         
          ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(caseid);
          sc1.setSelected(caseid);      
          VFC29_CaseToggleSpam caseToggle = new VFC29_CaseToggleSpam(sc1);
          caseToggle.toggleSpam();  
          //Tetsing the Null Url part
          Pagereference Page =  new pagereference('');
            Test.setCurrentPageReference(Page);         
          ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(caseid);
          sc2.setSelected(caseid);      
          VFC29_CaseToggleSpam caseToggler = new VFC29_CaseToggleSpam(sc2);
          caseToggler.toggleSpam();           
          //Testing the error part 
             Pagereference pages = new pagereference('/apex/VFP29_ToggleSpam?retURL=%2F500%3Ffcf%3D00BA0000006JE16'); 
           Test.setCurrentPageReference(pages);  
          ApexPages.StandardSetController sc3 = new ApexPages.StandardSetController(caseid); 
            VFC29_CaseToggleSpam caseToggled = new VFC29_CaseToggleSpam(sc3);
          try
          {
            caseToggled.toggleSpam();       
          }
          catch(Exception e)
          {
              system.assert(e.getMessage().contains(system.label.CL00342));              
          }                                                
            test.stopTest();
       }  
   }  
}