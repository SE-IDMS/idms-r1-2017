global class AP_ManageNotificationHandler{
    /*public static Map<String,Id> channelMap = new Map<String,Id>();
    public static Set<String> channelAF = new Set<String>();
    public static Set<String> segmentOnlyAlerts = new Set<String>();
    public static Set<String> channelOnlyAlert = new Set<String>();*/
    public static List<TrackSetupAlets__c> trackAlertsLst = new List<TrackSetupAlets__c>();
    /*public static List<ChannelSystemAlert__c> channelAlerts = new List<ChannelSystemAlert__c>();
    public static List<SegmentSystemAlert__c> segmentAlerts = new List<SegmentSystemAlert__c>();
    public static Set<String> alertsANDLst = new Set<String>();
    public static Map<String,String> segmentMap = new Map<String,String>();
    public static Set<String> segmentAlertLst = new Set<String>();
    public static Set<String> memberIdLst = new Set<String>();*/
    public static Map<String,FieloEE__Member__c> channelMemberANDMap = new Map<String,FieloEE__Member__c>();
    public static Map<String,FieloEE__Member__c> channelMemberORMap = new Map<String,FieloEE__Member__c>();
    public static Map<String,FieloEE__MemberSegment__c> segmentMemberANDMap = new Map<String,FieloEE__MemberSegment__c>();
    public static Map<String,FieloEE__MemberSegment__c> segmentMemberORMap = new Map<String,FieloEE__MemberSegment__c>();
    
    public static void insertNotifications(List<SetupAlerts__c> setupAlertsLst,Set<String> memberCountrySet,Map<String,String> memberCountryMap,List<FieloEE__Member__c> ChannelSetupAlertsLst, List<FieloEE__MemberSegment__c> SegmentSetupAlertsLst,Set<String> alertsANDLst,Map<String,Id> channelMap,Map<String,String> segmentMap){
        Set<Id> systemAlertIds = new Set<id>();
       if(setupAlertsLst.size() > 0 && !setupAlertsLst.isEmpty()){
            for(SetupAlerts__c s : setupAlertsLst){
                /*if(s.SegmentAlertsCount__c > 0 && s.ChannelAlertCount__c > 0)
                    alertsANDLst.add(s.Id);
                else if(s.SegmentAlertsCount__c > 0 && s.ChannelAlertCount__c == 0)
                    segmentOnlyAlerts.add(s.Id);
                else
                    channelOnlyAlert.add(s.Id);
                
                channelAlerts.addAll(s.Channel_System_Alerts__r);
                segmentAlerts.addAll(s.Segment_System_Alerts__r);
                */
                systemAlertIds.add(s.id);   
            }
        }
        /*
        for(ChannelSystemAlert__c c: channelAlerts){
            channelAF.add(c.CountryChannels__r.SubChannelCode__c);
            channelMap.put(c.CountryChannels__r.PRMCountry__r.CountryCode__c+c.CountryChannels__r.SubChannelCode__c,c.SystemAlert__c);
        }
        
        for(SegmentSystemAlert__c s : segmentAlerts){
            segmentMap.put(s.FieloPRM_Segment__r.F_PRM_Country__r.CountryCode__c+s.FieloPRM_Segment__c,s.SystemAlert__c);
            segmentAlertLst.add(s.FieloPRM_Segment__c);
        }
        */
        //Get All TrackSetupAlert
        Map<string,String> mapTrackAlert = new Map<String,String>();
        List<TrackSetupAlets__c> lstTrackSetupAlerts = [Select SetupAlerts__c,User__c from TrackSetupAlets__c where SetupAlerts__c in :systemAlertIds];
        //newly Added                                                   
        for(TrackSetupAlets__c tAlert :lstTrackSetupAlerts)
            mapTrackAlert.put(tAlert.SetupAlerts__c+','+tAlert.User__c,tAlert.User__c);         
        
        System.debug('**** mapTrackAlert ->'+mapTrackAlert); 
        
        
        /*List<FieloEE__Member__c> memberLst = new List<FieloEE__Member__c>([SELECT Id, F_PRM_Channels__c, F_AreaOfFocus__c, F_BusinessType__c, F_Country__c, F_PRM_PrimaryChannel__c,
                                                                           F_PRM_CountryCode__c, F_Country__r.CountryCode__c, FieloEE__User__c, FieloEE__User__r.ContactId FROM FieloEE__Member__c WHERE F_PRM_PrimaryChannel__c = :channelAF AND
                                                                           F_PRM_CountryCode__c = :memberCountrySet AND FieloEE__User__r.isActive = true]);
        */
        System.debug('>>>>memberLst'+ChannelSetupAlertsLst.size());
        //System.debug('>>>>Channel Map'+channelMap);
        //System.debug('****** SegMap->'+segmentMap);
             
        for(FieloEE__Member__c m : ChannelSetupAlertsLst){                                                               
           // memberIdLst.add(m.Id);
            String cntry = String.isNotBlank(memberCountryMap.get(m.F_Country__r.CountryCode__c))?memberCountryMap.get(m.F_Country__r.CountryCode__c):m.F_Country__r.CountryCode__c;
            if(channelMap.containsKey(cntry+m.F_PRM_PrimaryChannel__c)){
                if(!mapTrackAlert.containsKey(channelMap.get(cntry+m.F_PRM_PrimaryChannel__c)+','+m.FieloEE__User__c)){
                    if(alertsANDLst.contains(channelMap.get(cntry+m.F_PRM_PrimaryChannel__c)))
                        channelMemberANDMap.put(channelMap.get(cntry+m.F_PRM_PrimaryChannel__c)+','+m.FieloEE__User__c,m);
                    else
                        channelMemberORMap.put(channelMap.get(cntry+m.F_PRM_PrimaryChannel__c)+','+m.FieloEE__User__c,m);
                }
            }
        }
        
       /* List<FieloEE__MemberSegment__c> memberSegmentLst = new List<FieloEE__MemberSegment__c>([SELECT Id, FieloEE__Member2__c, FieloEE__Member2__r.F_PRM_CountryCode__c, FieloEE__Member2__r.F_Country__r.CountryCode__c, FieloEE__Member2__r.FieloEE__User__c, FieloEE__Member2__r.FieloEE__User__r.ContactId, FieloEE__Segment2__c 
                                                                                            FROM FieloEE__MemberSegment__c WHERE FieloEE__Member2__c = :memberIdLst AND FieloEE__Segment2__c = :segmentAlertLst]);
        
        List<FieloEE__MemberSegment__c> memberSegmentLst = new List<FieloEE__MemberSegment__c>([SELECT Id, FieloEE__Member2__c, FieloEE__Member2__r.F_PRM_CountryCode__c, FieloEE__Member2__r.F_Country__r.CountryCode__c, FieloEE__Member2__r.FieloEE__User__c, FieloEE__Member2__r.FieloEE__User__r.ContactId, FieloEE__Segment2__c 
                                                                                                    FROM FieloEE__MemberSegment__c WHERE FieloEE__Segment2__r.F_PRM_Country__r.CountryCode__c in :memberCountrySet
                                                                                                    AND FieloEE__Segment2__c = :segmentAlertLst]);
        */
        System.debug('>>>>memberSegment:'+SegmentSetupAlertsLst); 
        
        for(FieloEE__MemberSegment__c ms: SegmentSetupAlertsLst){
            System.debug('>>>>memberSegment Country Code:'+ms.FieloEE__Member2__r.F_PRM_CountryCode__c);
            System.debug('>>>>memberSegment :'+ms.FieloEE__Segment2__c);  
            String mCntry = string.isNotBlank(memberCountryMap.get(ms.FieloEE__Member2__r.F_Country__r.CountryCode__c))?memberCountryMap.get(ms.FieloEE__Member2__r.F_Country__r.CountryCode__c):ms.FieloEE__Member2__r.F_PRM_CountryCode__c;
            System.debug('>>>>memberSegment key:'+mCntry);
            if(segmentMap.containsKey(mCntry+ms.FieloEE__Segment2__c)){
                if(!mapTrackAlert.containsKey(segmentMap.get(mCntry+ms.FieloEE__Segment2__c)+','+ms.FieloEE__Member2__r.FieloEE__User__c)){
                    if(alertsANDLst.contains(segmentMap.get(mCntry+ms.FieloEE__Segment2__c))){
                        segmentMemberANDMap.put(segmentMap.get(mCntry+ms.FieloEE__Segment2__c)+','+ms.FieloEE__Member2__r.FieloEE__User__c,ms);
                        System.debug('**** inside1');
                    }    
                    else
                        segmentMemberORMap.put(segmentMap.get(mCntry+ms.FieloEE__Segment2__c)+','+ms.FieloEE__Member2__r.FieloEE__User__c,ms);
                }   
            }
        }
        
        System.debug('***** Channel map->'+channelMemberANDMap);
        System.debug('***** Segment map->'+segmentMemberANDMap);
        System.debug('***** Segment map->'+segmentMemberORMap);
        
        for(String s : channelMemberANDMap.keySet()){
            if(segmentMemberANDMap.containsKey(s)){
                TrackSetupAlets__c trackMemberChannelAlerts = new TrackSetupAlets__c();
                trackMemberChannelAlerts.User__c = channelMemberANDMap.get(s).FieloEE__User__c;
                trackMemberChannelAlerts.SetupAlerts__c = s.substringBefore(',');
                trackAlertsLst.add(trackMemberChannelAlerts);
            }
        }
        
        for(String s : channelMemberORMap.keyset()){
            TrackSetupAlets__c trackChannelORAlerts = new TrackSetupAlets__c();
            trackChannelORAlerts.User__c = channelMemberORMap.get(s).FieloEE__User__c;
            trackChannelORAlerts.SetupAlerts__c = s.substringBefore(',');
            trackAlertsLst.add(trackChannelORAlerts);
        }
        
        for(String s : segmentMemberORMap.keyset()){
            TrackSetupAlets__c trackMemberAlerts = new TrackSetupAlets__c();
            trackMemberAlerts.User__c = segmentMemberORMap.get(s).FieloEE__Member2__r.FieloEE__User__c;
            trackMemberAlerts.SetupAlerts__c = s.substringBefore(',');
            trackAlertsLst.add(trackMemberAlerts);
        }
        
        System.debug('>>>>trackAlertsLst:'+trackAlertsLst);
        if(trackAlertsLst.size() > 0 && !trackAlertsLst.isEmpty())
            if(!Test.isrunningTest())
                INSERT trackAlertsLst;
    }
    
   public static void revokeNotification(List<SetupAlerts__c> lstSetupAlerts){
       System.debug('******* System Alert ->'+lstSetupAlerts);
        Set<id> setAlertId = new set<id>();
        for(SetupAlerts__c aSysAlert :lstSetupAlerts)
            setAlertId.add(aSysAlert.id); 
            
         System.debug('*** Set ALertId->'+setAlertId);
        
        try{
            List<TrackSetupAlets__c> lstTrackSetupAlerts = [Select id,Deleted__c from TrackSetupAlets__c 
                                                                WHERE SetupAlerts__c in :setAlertId AND Deleted__c != true ];
                                                                
             System.debug('*** lstTrackSetupAlerts ->'+lstTrackSetupAlerts.size());                                                                
                                                                
            for(TrackSetupAlets__c aTrackAlert :lstTrackSetupAlerts)
                aTrackAlert.Deleted__c = true;
            
            
            if(lstTrackSetupAlerts.size() > 0){
               /* Database.SaveResult[] srList = Database.update(lstTrackSetupAlerts,false); 
                for(Database.SaveResult sr : srList){
                    if(!sr.isSuccess()){
                        Database.Error[] errs = sr.getErrors();
                         for(Database.Error err : errs)
                            System.debug('$#$#$#$' + err.getStatusCode() + ' - ' + err.getMessage());
                    }
                    else
                        System.debug('*** Updated ');
                        
                }*/
                
                update lstTrackSetupAlerts;
            }
            

        } catch (Exception e) {
            System.debug('**Exception **'  + e.getMessage() + e.getStackTraceString());
            
        }
    
    }
    
}