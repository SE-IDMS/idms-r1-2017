public class VCC07_OrderConnectorController extends VFC_ControllerBase
{
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/
    
    public String StandardObjectID{get;set;} // ID of related object
    public sObject RelatedRecordAttribute{get;set;} // Related object coming from the attribute of the component
    public sObject RelatedRecord{get;set;} // Related object (can be equal or different from the RelatedRecordAttribute)
    public String ImplementationType{get;set;} // Variable defining the context of use of the controller
    public Utils_OrderMethods OrderMethods{get;set;} // Implementation of the order interface
    public Boolean NewOrder{get;set;} // Boolean defining if the Order is being created or read
    public Utils_OrderDataSource OrderDataSource; // Data Source to access Web Services
    public List<sObject> FetchedRecords{get;set;} // List of records returned by the Order Data Source
    public List<sObject> DeliveryDetails{get;set;} // List of records returned by the Delivery Details Data Source
    public OPP_OrderLink__c SelectedOrder{get;set;} // Order Selected by clicking the "select" button on the display component
    public OPP_DeliveryDetails__c SelectedDelivery{get;set;} // Delivery detail Selected by clicking the "select" button on the display component
    public Account RelatedAccount{get;set;} // Related Account 
    public String RelatedBackOffice{get;set;}
    public String BackOffice{get;set;} // Selected Back office from the BO picklist
    public List<String> KeyForExistingRecords{get;set;} // Key computed to determine previously selected records
    public set<Object> existingOrders{get;set;} // Already selected orders
    public string Action2Label{get;set;} // Label of the second action button of the display results
    public Boolean CCFilters{get;set;} // Boolean defining differences between Sales and CCC
    public string orderNumber{get;set;} // Order Number
    public string PONumber{get;set;} // PO Number
    public string PartNo{get;set;} // Part Number
    public Boolean Sales{get;set;} // Boolean at yes if the controller is used in a "Sales" context
    public string OrderStatus{get;set;} // Status of the Order
    public List<SelectOption> Columns{get;set;} // List defining headers and fields displayed in the columns for the order list. Provided by the Data Sources
    public List<SelectOption> DeliveryColumns{get;set;} // List defining headers and fields displayed in the columns for the delivery detail list. Provided by the Data Sources
    public List<SelectOption> CreatedDate{get;set;} // Picklist enabling a quick selection of a date range
    public string DateRange{get;set;} // Selected value in the CreatedDate picklist
    public List<SelectOption> BackOffices{get;set;} // List of available back offices for the selected account
    public DataTemplate__c DateCapturer{get;set;} // Technical object allowing to capture Date and Lookup(Account) fields
    public Boolean DisplayResults{get;set;} // Display results of the Order search
    public String ShippingDetailsURL{get;set;}
    public String TabInFocus{get;set;} // Tab in focus : SearchOrder, OrderDetails, deliveryDetails
    public string TypeOfNewsObject{get;set;}
    public string Tab2ActionLabel{get;set;}
    public String OrderID{get;set;} // Order Id
    public String ActionLabel{get;set;} // Label of the first action button on the order results component
    public Boolean DisplayDeliveryDetails{get;set;} // Enable the display of delivery details for the order 
    public Boolean DisplayAdvancedCriteria{get;set;} // Display fields for advanced search (date range, Part number, status) 
    public Boolean EnableDetails{get;set;} // display the second tab
    public Boolean EnableDeliveryDetails{get;set;} // display the third tab
    public Boolean InitDone{get;set;} // Initialization is done
    public Boolean DisableSelectAction{get;set;} // Disable "Select Order" Button
    public Boolean disableSearch{get;set;} // Disable search button
    public Boolean ReadOnly{get;set;} // Read only mode : SearchOrder tab is not displayed, no select order action is allowed
    public Boolean DisableAllSelectAction{get;set;} // View mode: all select buttons are removed 
    public Boolean DisplayPage{get;set;} // Allow user to view the page
    public Boolean DisplayShippingTo{get;set;}
    public BOolean AccountReadOnly{get;set;}
    public Integer TabNumber{get;set;}
    
    public String RelatedAccountName
    {
        get{try{return [SELECT ID, Name FROM Account WHERE ID=:DateCapturer.Account__c].Name;}
            catch(Exception e){return Label.CL00577;}}
    }
       
    public String Tab1;
    public String Tab2{get{TabNumber=1; return null;}} 
    public String Tab3{get{TabNumber=2; return null;}} 
    
    public string getTab1()
    {
        TabNumber=0; return null;
    } 
       
    // Component displaying the result of the order search
    public VCC06_DisplaySearchResults OrderListController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('DisplayOrders');                
                if(displaySearchResults!= null)
                    return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }
    
    // Component displaying the result of the delivery details search 
    public VCC06_DisplaySearchResults deliveryListController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('DeliveryDetails');                
                if(displaySearchResults!= null)
                    return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }

    /*=======================================
      CONSTRUCTOR
    =======================================*/   
  
    // VCC07_OrderConnectorController default constructor  
    public VCC07_OrderConnectorController()
    {
        FetchedRecords = new List<sObject>();
        DeliveryDetails = new List<sObject>();
        KeyForExistingRecords = new List<String>();
        DateCapturer = new DataTemplate__c();        
        OrderDataSource = new Utils_OrderdataSource();   
        Columns = OrderDataSource.getcolumns();
        DeliveryColumns = OrderDataSource.getDetailsColumns();

        VariablesInitialization();
    }
    
    public void VariablesInitialization()
    {                  
        InitDone = false;       
        CCFilters = true;
        DisplayAdvancedCriteria = false;
        DisplayResults = false;
        EnableDetails = false;
        ReadOnly=false;
        DisableAllSelectAction = false;
        ImplementationType = '';
        EnableDeliveryDetails = false;
        DisplayShippingTo = false;
        Tab2ActionLabel = 'View Shipping to Details';
        TabNumber=0;    

        Action2Label = 'Select'; // Label for the Action 2 of the display results component
        
        InitDateRangePicklist(); // Initialization of the Date Range Picklist
        DateRange = '1'; // Default value is "Last 7 days"
        DateRangeRefresher(); // refresh "from" and "to" 
        NewOrder=True; // true when coming from the new button
        DisplayPage=true; // true when the user is allowed to see the page
    }
    
    /*=======================================
      METHODS
    =======================================*/   
    
    public void InitDateRangePicklist()
    {
        CreatedDate = new List<SelectOption>();
        CreatedDate.add(new SelectOption('1',Label.CL00503));
        CreatedDate.add(new SelectOption('2',Label.CL00504));
        CreatedDate.add(new SelectOption('3',Label.CL00505));
        CreatedDate.add(new SelectOption('4',Label.CL00506)); 
    }
    
    // Method called after the constructor to initialize the component after its attributes are defined
     public String getInit()    
     { 
       if(InitDone == true)
           return null;
                       
       OrderMethods.Init(this);
       
       InitDone = true;
       return null;     
     }
     
     // Switch the display of the advanced criteria panel 
     public void ToggleAdvancedCriteria()
     {
         TabNumber=0;
         DisplayAdvancedCriteria = !DisplayAdvancedCriteria;
     }
     
     // Search Method
     public PageReference SearchOrder()
     {             
         // BackOfficeRefresher();
         
         // If PONumber and Order Number are null and no advanced search is used, return an error message
         if(orderNumber=='' && PONumber == '' && DisplayAdvancedCriteria == false)
         {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00572));              
             displayresults = false;        
             return null;
         }

        try
        {    
             
             utils_DataSource.Result Results = new utils_DataSource.Result();
             Results = OrderDataSource.SearchOrder(this);
             FetchedRecords = Results.RecordList;
             RelatedAccount = [SELECT ID, Name FROM Account WHERE ID=:DateCapturer.Account__c];
             RelatedBackOffice = BackOffice;
  
             if(Results.returnMessage != null)
             {
                 if(Results.returnMessage == Label.CL00590)    
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00589));                 
                 else if(Results.returnMessage ==    Label.CL00591 && (BackOffice == Label.CL00501 || BackOffice == Label.CL00355) )
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00589));       
                 else if(Results.returnMessage == Label.CL00591 )
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00592));
                 else  
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00605));  
                  
                 DisplayResults = false;   
                 return null;
             } 
         
             EnableDetails = false;
             DisplayShippingTo = false;    
             DisplayResults = true;
             

             
             if(Results.numberOfRecord >= 50)
             {
                 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00573));  
             }
             
             TabNumber=0;   
             // If only One Order Is returned, the "view details" action is called
             if(FetchedRecords.size()==1)
             {
                this.ActionNumber=1;
                PerformAction(FetchedRecords[0],this);
             }
        }
        catch(Exception CalloutException)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00574+'--------'+CalloutException.getStackTraceString()+'--------'+CalloutException.getMessage()));              
        }
             
         return null;         
     }
     
     // Method to display details of an order
     public void DisplayDeliveryDetails()
     {

        try
        {
             utils_DataSource.Result Results = new utils_DataSource.Result();
             
             Results = OrderDataSource.SearchOrderDetails(this);
             DeliveryDetails = Results.RecordList;   
             tabnumber = 1;
             TabInfocus='OrderDetails';
             DisplayDeliveryDetails = true;
       
    
         }
         catch(Exception SearchOrderDetailsException)
         {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00575));      
             DisplayDeliveryDetails = false;        
         }
         
         
         try
         {
             String Key = SelectedOrder.SEOrderReference__c + SelectedOrder.BackOffice__c;
          
             DisableSelectAction = false;
             if(existingOrders!=null)            
             {
                 if(!existingOrders.isEmpty())
                 {
                     if(existingOrders.contains(Key))
                             DisableSelectAction = true;
                             
                 }
             }
         }
         catch(Exception SelectedOrderException)
         {
               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00576));     
                 DisplayDeliveryDetails = false; 
                 DisplayPage = false;
         }
     }
    
    // Perform action method handles clicks on command link of result components 
    public override Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
    {
        VCC07_OrderConnectorController OrderController = (VCC07_OrderConnectorController)ControllerBase; 

        System.debug('#### Display Details');
        System.debug('#### ActionNumber: '+ ControllerBase.ActionNumber);
        
        // If ActionNumber = 2 the "select" method is called
        if(ControllerBase.ActionNumber==2)
        {    
            OrderController.SelectedOrder = (OPP_OrderLink__c)obj;  
            TabNumber=1;
            return OrderMethods.PerformAction(this);
        }

        // If related object is an order link details of the order are displayed
        if(obj.getSObjectType() == OPP_OrderLink__c.sObjectType)
        {
            OrderController.SelectedOrder = (OPP_OrderLink__c)obj;  
            OrderController.DisplayDeliveryDetails();          
            EnableDetails = true;
           
            // OrderController.TabInFocus = 'OrderDetails';
 
        }
        else  // else delivery details are displayed
        {    
            EnableDeliveryDetails = true;
            SelectedDelivery = (OPP_DeliveryDetails__c)obj;
            ShippingDetailsURL = SelectedDelivery.mySEShippingUrl__c;
            DisplayShippingTo = true;
            tabnumber = 2;
            
            if(readonly==true)
                tabnumber = 1;
            
            OrderController.TabInFocus = 'ShippingTo';
            System.debug('#### Tab Modified');
               
        }
                
        return null;     
    }
         
     // Clear method: reset the component    
     public void clear()
     {
         VariablesInitialization();
         KeyForExistingRecords.clear();
         OrderMethods.Init(this);
         InitDone=true;   
         OrderNumber = null;
         partNo = null;
         PONumber = null;  
     }
     
     // Refresh method to set the "from" and "to" dates when a value is selected in the Date Range Picklist
     public void DateRangeRefresher()
     {
        DateCapturer.DateField2__c = Date.Today();
        if(DateRange=='1')
        {
            DateCapturer.DateField1__c = Date.Today()-7;    
        }
        else if(DateRange=='2')
        {
            DateCapturer.DateField1__c = Date.Today()-30;
        } 
        else if(DateRange=='3')
        {
            DateCapturer.DateField1__c = Date.Today()-180;
        }

     }

     // Method to refresh backoffices on change of the related account
     public void BackOfficeRefresher()
     {
        List<LegacyAccount__c> LegacyAccounts = [SELECT ID, Name, Account__c, LegacyName__c, LegacyNumber__c FROM LegacyAccount__c WHERE Account__c=:datecapturer.Account__c];

        BackOffices = new List<SelectOption>(); 
            
        for (LegacyAccount__c LegacyAccount:LegacyAccounts)
        {
            BackOffices.add(new SelectOption(LegacyAccount.LegacyName__c,LegacyAccount.LegacyName__c));
        } 
        
        if(BackOffices.size()==0 )
        {
             BackOffices.add(new SelectOption(Label.CL00355,Label.CL00355));  
             BackOffice = Label.CL00355;           
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00502)); 
        }
        
        if(BackOffices.size()==1)
        {        
            BackOffice = BackOffices[0].getValue(); 
        }  
              
        if(BackOffices.size()>1)
        {
             BackOffices.add(new SelectOption(Label.CL00501,Label.CL00501)); 
             BackOffice = Label.CL00501;
        }       
     }

     // If a custom value is set in the "from" or "to" date range fields, the Date Range displays "Custom"
     public void DateRefresher()
     {
         DateRange = '4';
         
     }
     
     // Generic action (implemented in order classes)
     public pagereference Action()
     {
         return OrderMethods.PerformAction(this);
     }
     
     // Cancel method (implemented in order classes)
     public pagereference cancel()
     {
         return OrderMethods.cancel(this);
     }
}