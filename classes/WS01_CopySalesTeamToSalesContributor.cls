/*
 Author: Amitava Dutta
 Date created:28/12/2010.
 Description: Basically a global class which is used to copy the Sales Team associated 
 with Opportunity as Sales Contributor List
 REF: SRO+ Release
*/

global with sharing class WS01_CopySalesTeamToSalesContributor
{
    // Region Static Members            
    public static List<OPP_SalesContributor__c> contributors= new List<OPP_SalesContributor__c>();    
    public static OPP_SalesContributor__c contributor =  new OPP_SalesContributor__c();
    public static integer result;
    public static string opportunityId{get;set;}
    public static List<String> UserIds = new List<String>();
    public static Map<String, Boolean> CreateContributorMap = new Map<String, Boolean>();
    // End Region    
 
    // Global method to Copy Existing Sales Team to Sales Contributor List
    webService static Integer CopySalesTeamToSalesContributor(string opportunityIds)
    {           
        opportunityId = opportunityIds;
        List<Opportunity> opportunity = [SELECT OWNERID FROM OPPORTUNITY WHERE ID=:opportunityId];
        
        
        List<OpportunityTeamMember> teamMembers= [Select CreatedById, CreatedDate, IsDeleted, OpportunityAccessLevel, OpportunityId, TeamMemberRole, UserId from OpportunityTeamMember WHERE OpportunityId =: opportunityId];
        
        for(OpportunityTeamMember oppMember: teamMembers){
            UserIds.add(oppMember.UserId);
        }
        
        OPP_SalesContributor__c[] salesContributors = [Select Id, User__c, Opportunity__c from OPP_SalesContributor__c WHERE User__c in: UserIds AND Opportunity__c =: opportunityId];
        
        for(OPP_SalesContributor__c salesContr: salesContributors){
            CreateContributorMap.put(salesContr.User__c, False);
        }
        for(Opportunity opmain : opportunity)
        {
           if(opmain.ownerid != userInfo.getUserId())
           {
               result = -100;
           }
           else
           {
            if(teamMembers.size() !=0)
            {
                for(OpportunityTeamMember opm : teamMembers)
                {
                    if(!CreateContributorMap.containsKey(opm.UserId))
                    {    
                        contributor = new OPP_SalesContributor__c();
                        contributor.Opportunity__c = opm.OpportunityId;
                        contributor.Role__c = opm.TeamMemberRole;
                        contributor.CurrencyIsoCode = UserInfo.getDefaultCurrency();
                        contributor.Contribution__c = 0;
                        contributor.User__c =opm.UserId;
                        contributors.add(contributor);
                    } 
                }     
            }
          }  
        }    
        if(contributors.size() > 0)
        {
             if(contributors.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
             {
                            System.Debug('######## AP05 Error Inserting : '+ Label.CL00264);
             }
             else
             {
                  Database.SaveResult[] ContributorInsertion = Database.insert(contributors, false);
                
                  for(Database.SaveResult ir: ContributorInsertion )
                  {
                        if(!ir.isSuccess())
                                    Database.Error err = ir.getErrors()[0];
                  }            
        
            
                  if(teamMembers.size()!=0)
                  {
                     if(contributors.size() == 0)
                     {
                          result = -1;
                     } 
                     else
                     {
                            result = 1;
                     }   
                 }
                 else
                 {
                            result = 0;
                 }
                    
                                  
                   
                 
           }      
        }      
        else
        {
               result = -100;
        }  
        return result;
       
    }    
}