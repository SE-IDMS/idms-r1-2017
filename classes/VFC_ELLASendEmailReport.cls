/****
Description :Send email As reports
Release :March 2016 

*****/


public class VFC_ELLASendEmailReport {
    
    
    public static String strdataOut;
   
    public String strEmail{get;set;}
    public String strUserEmail{get;set;}
    public String strEmailEncode{get;set;}
    public String strUserEmailEncode{get;set;}
    public String strReportid{get;set;}
    public String requestReportURL;
  
    
    public void VFC_ELLASendEmailReport () {
        strdataOut='';   
       
    }
 
   public String getOfferReportData() {
       
        extractReport();
        
        return strdataOut;
   }
  
    public void extractReport() {
    
        requestReportURL='/'+strReportid+label.CLMAR16ELLAbFO02;
        
        if(!Test.isRunningTest()) {
        
            strdataOut= new PageReference(requestReportURL).getContent().toString();
        }
        
    }
    
     public String getemailDecodeData() {
        
        emailDecode();      
        
        return strUserEmailEncode;
   }
    //Encodethechnages
    public void emailDecode() {
        if(strEmail!=null && strEmail!='') {
            
            Blob beforeblob = Blob.valueOf(strEmail);
            strUserEmailEncode = EncodingUtil.base64Encode(beforeblob);
            
        }
        if(strUserEmail!=null && strUserEmail!='' ) {
        
            Blob beforeblobUsr = Blob.valueOf(strUserEmail);
            strUserEmailEncode = EncodingUtil.base64Encode(beforeblobUsr);
        
        }
        
        
    }
 
}