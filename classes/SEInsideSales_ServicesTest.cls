@isTest
private class SEInsideSales_ServicesTest {
	
	@isTest static void getDataTest() {
		String fields = 'Id, Name';
		String sobjectName = 'Contact';
		String condition = 'AccountId != null';
		SEInsideSales_Services.getData(fields,sobjectName,condition,1);
		SEInsideSales_Services.getData('',sobjectName,condition,1);
	}
	
	@isTest static void getSalesEventsTest() {
		SEInsideSales_Services.getSalesEvents(true);
		SEInsideSales_Services.getSalesEvents(false);
	}

	@isTest static void getEventsForDateTest() {
		String today = system.now().format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');
		String yesterday = system.now().addDays(-1).format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');

		SEInsideSales_Services.getEventsForDate(yesterday,today);
	}

	@isTest static void eventsTest() {
		Account a = new Account(Name='Test Account',
			BillingCity ='SEATTLE',
			BillingState = 'WA',
			BillingCountry = 'US',
			BillingStreet = '701 PIKE ST, FLOOR SEVEN, SUITE 700',
			BillingPostalCode = '98101');
		insert a;

		Date today = Date.today();
		Date yesterday = Date.today().addDays(-1);

		Event te = new Event();
    	te.StartDateTime = yesterday;
    	te.EndDateTime = today;
    	te.Subject = 'Test Subject';
    	te.OwnerId = UserInfo.getUserId();
    	te.WhatId = a.Id; 

    	insert te; 				

		SEInsideSales_Services.getEventInformation(te.Id);

		Opportunity opp = Utils_TestMethods.createOpportunity(a.Id);
		insert opp;
		te.WhatId = opp.Id;
		update te;

		SEInsideSales_Services.getEventInformation(te.Id);

		SEInsideSales_Services.getEventDates(te.Id);
		SEInsideSales_Services.getEventRecordforEdit(te.Id);
		SEInsideSales_Services.saveEvent(te);

		List<Opportunity> oppList = new List<Opportunity>();
		oppList.add(opp);
		SEInsideSales_Services.updateOpportunityAmount(oppList);

		SEInsideSales_Services.getMyCampaigns();
		SEInsideSales_Services.getMyAccounts();
		SEInsideSales_Services.getMyOpportunites();
		SEInsideSales_Services.getMyLeads();
		SEInsideSales_Services.getAccountById(a.Id);

		String accountId = a.Id;

		delete a;
		system.debug('test id=' + accountId);
		SEInsideSales_Services.getAccountById(accountId);

	}
	


	
}