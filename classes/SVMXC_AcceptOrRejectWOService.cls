global class SVMXC_AcceptOrRejectWOService implements Messaging.InboundEmailHandler {
/**
     This class is used to take an inbound email that was sent to the FSR and accept or reject a Work Order Assignment
     
     Written November 16, 2015
    
*/
    private static string rejectionReason = 'Rejected via Email';
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {  
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Boolean saveSuccess = true;
        SVMXC__Service_Order__c workOrder = null;
        String emailSubject = null;
        
        if (email.Subject != null) {
            emailSubject = email.subject;
        } else {
            saveSuccess = false;
            notifyEmailResponse(email.fromAddress, 
                                'The subject line is blank, please respond without making any changes to the subject line',
                                'Work Order Action FAILED', null);
                    
        }
        
        Set<String> validOrderStatus = new Set<String>{'Customer Confirmed','Acknowledge FSE','WIP'};
        
        System.debug('#### emailSubject length ' + emailSubject.length());
    
        if (emailSubject != null && (emailSubject.contains('Accept') || emailSubject.contains('Reject') && emailSubject.length() >= 45)) {
    
            String action = emailSubject.substring(0,6);
            // look for record Id in subject line
            String techId = emailSubject.substring(8,26);
            String woId = emailSubject.substring(26,44);
            
            System.debug('Tech Record Id : ' + techId);
            System.debug('Work Order Id : ' + woId);
            
            Set<String> assignmentStatus = new Set<String>();
            
            if (action == 'Accept') {
                assignmentStatus.add('Assigned');
            } else if (action == 'Reject') {
                assignmentStatus.add('Assigned');
                assignmentStatus.add('Accepted');         
            }
            
            // query for Assignment record
            List<AssignedToolsTechnicians__c> wa = [SELECT Status__c, Id, WorkOrder__r.SVMXC__Order_Status__c, WorkOrder__r.Name,
                                                    WorkOrder__r.SVMXC__Company__r.Name, PrimaryUser__c 
                                                    FROM AssignedToolsTechnicians__c 
                                                    WHERE TechnicianEquipment__c =: techId AND WorkOrder__c =: woId
                                                    AND Status__c IN : assignmentStatus];                                                                   
            
            if (!wa.isEmpty()) {
                
                AssignedToolsTechnicians__c firstRecord = wa[0];
                
                String additionalBody = constructStandardMessageComponent(firstRecord);
                
                if (validOrderStatus.contains(firstRecord.WorkOrder__r.SVMXC__Order_Status__c)) {  
                
                    String fsrResponse = email.plainTextBody;
                    
                    if (fsrResponse != null && fsrResponse.length() >= 254)
                        fsrResponse = fsrResponse.substring(0,253);  
                
                    for (AssignedToolsTechnicians__c att : wa) {
                                        
                        if (action == 'Accept') {
                            att.Status__c = 'Accepted';
                            att.FSR_Response__c = null;
                        } else if (action == 'Reject') {
                            att.Status__c = 'Rejected';
                            att.FSR_Response__c = fsrResponse;
                            att.ReasonForRejection__c = rejectionReason;
                            att.PrimaryUser__c = false;
                        }              
                    }
                    
                    //update Assignment Record(s)
                    try {
                        update wa;

                    } catch (Exception e) {
                        saveSuccess = false;
                        //unhandled exception caught
                        notifyEmailResponse(email.fromAddress, 
                                'An unhandled exception was encountered recording your response to the WO.  Please contact the Administrator. ' + e, 'Work Order Action FAILED', additionalBody);
                    }
                        
                    if ((action == 'Reject' || action == 'Accept') && (saveSuccess)) {
                        notifyEmailResponse(email.fromAddress, 
                                'You have successfully recorded your response to this Work Order as ' + action,
                                'Work Order Action Success', additionalBody);
                                    
                    }
                } else {
                    // order status of WO is not actionable
                    notifyEmailResponse(email.fromAddress, 
                    'The current Order Status of the Work Order (' + firstRecord.WorkOrder__r.SVMXC__Order_Status__c + ') does not allow you to Accept or Reject the Work Order.',
                    'Work Order Action FAILED', null);
                
                }                 
                        
            } else {
                // record not found 
                notifyEmailResponse(email.fromAddress, 
                    'The record could not be found or you have already actioned this Work Order, please contact the Service Admin or login to your mobile device to action this Work Order.',
                    'Work Order Action FAILED', null);
            }
            
        } else {
            // bad email, missing subject
            notifyEmailResponse(email.fromAddress, 
                'The subject line from your email could not be interpreted, please contact the Service Admin or login to your mobile device to action this Work Order.',
                'Work Order Action FAILED', null);
        }
        return result;
    }
    
    private static void notifyEmailResponse(String userEmail, String message, String subject, String additionalMessageBody) {
        
        System.debug('####### userEmail : ' + userEmail);   
        User userDetails= [SELECT Id, Name, Email FROM User WHERE Email =: userEmail LIMIT 1];      
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();      
        String[] toAddresses = new String[] {userEmail}; 

        mail.setTargetObjectId(userDetails.Id);
        mail.setSenderDisplayName('Work Order Action Reply');
        mail.setSubject(subject);
        mail.setUseSignature(false);
        if (additionalMessageBody == null)
            mail.setHtmlBody(message); 
        else
            mail.setHtmlBody(message + additionalMessageBody);
        mail.setSaveAsActivity(false);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    private static String constructStandardMessageComponent(AssignedToolsTechnicians__c firstRecord) {
        
        String returnBody = null;
        
        if (firstRecord != null) {
            returnBody = '<BR/><BR/>This is related to Work Order # ' + firstRecord.WorkOrder__r.Name + ' and Account ' + firstRecord.WorkOrder__r.SVMXC__Company__r.Name;
        }
        
        return returnBody;  
    }   
}