/*
    Description: Test Class for VFC_ProjectSearch
*/
@isTest
public class VFC_ProjectSearch_Test {
     static testMethod void testOpportunitySearch()
    {
        ApexPages.StandardController controller;
        VFC_ProjectSearch prjSrch;   
        
         Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        Database.insert(acc); 
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        insert prj;
        test.startTest();
        PageReference pr = new PageReference('/'+Label.CLOCT15SLS15+'/e');
        pr.getParameters().put('OpptyType', Label.CLAPR14SLS25);
        Test.setCurrentPage(pr);
        controller = new ApexPages.StandardController(prj);
        prjSrch= new VFC_ProjectSearch(controller);
        prjSrch.sortExpression = 'Name';
        prjSrch.sortDirection = 'DESC';
        prj.ProjectEndUserInvestor__c=acc.Id;
        prjSrch.search();
        prjSrch.sortExpression = 'Name';
        prjSrch.sortDirection = 'ASC';
        prjSrch.openOpptys = true;
        prjSrch.search();        
        prjSrch.createProject();  
        prj.CountryOfDestination__c=null;
        prjSrch.populateCountryOfDestination();
        test.stopTest();                                                                
    }
}