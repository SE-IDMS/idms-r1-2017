/**
    Author          : Nitin Khunal
    Date Created    : 20/07/2016
    Description     : Test Class for VFC_PAHomePage apex class
*/
@isTest
public class VFC_PAHomePage_TEST {
    
    @testSetup static void testData() {
        List<Account> lstAccount = new List<Account>();
        List<Case> lstCase = new List<Case>();
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount1 = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount1.SEAccountID__c = '123456789';
        testAccount1.City__c = 'Bangalore';
        insert testAccount1;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.SEAccountID__c = '12345678';
        testAccount.parentid = testAccount1.Id;
        testAccount.City__c = 'Bangalore';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        insert testcontact;
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        CTR_ValueChainPlayers__c testCVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        testCVCP.legacypin__c ='1234';
        testCVCP.Contact__c = testcontact.Id;
        insert testCVCP;
        BusinessRiskEscalationEntity__c objOrg1 = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        objOrg1.Name=System.Label.CLQ316PA025;
        insert(objOrg1);
        Case testCase1 = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id, 'New');
        testCase1.ReportingOrganization__c = objOrg1.Id;
        testCase1.AnswerToCustomer__c = 'testing';
        testCase1.PACaseType__c =  system.label.CLQ316PA029;
        lstCase.add(testCase1);
        Case testCase2 = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id, 'New');
        testCase2.ReportingOrganization__c = objOrg1.Id;
        testCase2.AnswerToCustomer__c = 'testing';
        testCase2.PACaseType__c = system.label.CLQ316PA040;
        lstCase.add(testCase2);
        Case testCase3 = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id, 'New');
        testCase3.ReportingOrganization__c = objOrg1.Id;
        testCase3.AnswerToCustomer__c = 'testing';
        testCase3.PACaseType__c =  system.label.CLQ316PA029;
        lstCase.add(testCase3);
        Case testCase4 = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id, 'New');
        testCase4.ReportingOrganization__c = objOrg1.Id;
        testCase4.AnswerToCustomer__c = 'testing';
        testCase4.PACaseType__c = system.label.CLQ316PA040;
        lstCase.add(testCase4);
        Case testCase5 = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id, 'New');
        testCase5.ReportingOrganization__c = objOrg1.Id;
        testCase5.AnswerToCustomer__c = 'testing';
        testCase5.PACaseType__c =  system.label.CLQ316PA031;
        lstCase.add(testCase5);
        Case testCase6 = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id, 'New');
        testCase6.ReportingOrganization__c = objOrg1.Id;
        testCase6.AnswerToCustomer__c = 'testing';
        testCase6.PACaseType__c = system.label.CLQ316PA031;
        lstCase.add(testCase6);
        insert lstCase;
    }
    
    @isTest static void testMethod1() {
        VFC_PAHomePage controller = new VFC_PAHomePage();
        VFC_PAHomePage.myOpenCases();
    }
    
}