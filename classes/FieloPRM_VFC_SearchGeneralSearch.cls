global class FieloPRM_VFC_SearchGeneralSearch {

	public String autocompleteURL {get;set;}
	public String searchEncoding  {get;set;}
	public String searchImplementation {get;set;}
	public String submitURL {get;set;}
	public static String GSS_IMPLEMENTATION = 'GSS';
	public static String GSA_IMPLEMENTATION = 'GSA';
	public static Integer MAX_REDIRECTION=3;
	public static String ITRESELLERES_CHANNEL ='VR';
	//Adding APX search
	public Boolean isAPCEnabled {get;set;}
	public String baseURLAPC {get;set;}
	public String submitURLAPC {get;set;}
	public String autocompleteURLAPC {get;set;}
	public Boolean isAPCDefaultValue {get;set;}

	String category = 'all';

	public FieloPRM_VFC_SearchGeneralSearch(){
		Id memberId = FieloEE.MemberUtil.getmemberId();
		FieloEE__Member__c[] memberList =  [SELECT F_Country__c,F_Country__r.CountryCode__c,F_PRM_Channels__c,F_PRM_PrimaryChannel__c,F_PRM_Country__r.PLDataPool__c,F_PRM_Country__r.SearchSubmitURL__c,F_PRM_Country__r.SearchImplementation__c,F_PRM_Country__r.SearchEncoding__c,F_PRM_Country__r.Country__c,F_PRM_Country__r.CountryCode__c,F_PRM_Country__r.SearchAutocompleteURL__c,F_PRM_Country__r.SearchApcAutocompleteURL__c,F_PRM_Country__r.SearchApcSubmitURL__c,F_PRM_Country__r.SearchApcSiteURL__c FROM FieloEE__Member__c WHERE Id=: memberId];
		FieloEE__Member__c member;
		String countryCode;
		String language = UserInfo.getLocale();
	    if(language!=null && language.length()>2){
	        language = language.substring(0, 2);
	    }
		if(memberList.size()>0){
			member = memberList[0];
			countryCode = member.F_Country__r.CountryCode__c!=null?member.F_Country__r.CountryCode__c.toLowerCase():null;
			isAPCEnabled = member.F_PRM_Channels__c!=null && member.F_PRM_Channels__c.contains(ITRESELLERES_CHANNEL);
			isAPCDefaultValue = member.F_PRM_PrimaryChannel__c!=null && member.F_PRM_PrimaryChannel__c.startsWith(ITRESELLERES_CHANNEL);
			autocompleteURL = member.F_PRM_Country__r.SearchAutocompleteURL__c!=null?member.F_PRM_Country__r.SearchAutocompleteURL__c.replace(FieloPRM_C_PartnerLocatorFormController.LANGUAGE_REPLACE_TEXT,language).replace(FieloPRM_C_PartnerLocatorFormController.COUNTRY_CODE_REPLACE_TEXT,countryCode):null;
            searchEncoding = member.F_PRM_Country__r.SearchEncoding__c;
            searchImplementation = member.F_PRM_Country__r.SearchImplementation__c;
            submitURL = member.F_PRM_Country__r.SearchSubmitURL__c!=null?member.F_PRM_Country__r.SearchSubmitURL__c.replace(FieloPRM_C_PartnerLocatorFormController.LANGUAGE_REPLACE_TEXT,language).replace(FieloPRM_C_PartnerLocatorFormController.COUNTRY_CODE_REPLACE_TEXT,countryCode):null;
            baseURLAPC = member.F_PRM_Country__r.SearchApcSiteURL__c!=null?member.F_PRM_Country__r.SearchApcSiteURL__c.replace(FieloPRM_C_PartnerLocatorFormController.LANGUAGE_REPLACE_TEXT,language).replace(FieloPRM_C_PartnerLocatorFormController.COUNTRY_CODE_REPLACE_TEXT,countryCode):null;
            submitURLAPC = member.F_PRM_Country__r.SearchApcSubmitURL__c!=null?member.F_PRM_Country__r.SearchApcSubmitURL__c.replace(FieloPRM_C_PartnerLocatorFormController.LANGUAGE_REPLACE_TEXT,language).replace(FieloPRM_C_PartnerLocatorFormController.COUNTRY_CODE_REPLACE_TEXT,countryCode):null;
            autocompleteURLAPC = member.F_PRM_Country__r.SearchApcAutocompleteURL__c!=null?member.F_PRM_Country__r.SearchApcAutocompleteURL__c.replace(FieloPRM_C_PartnerLocatorFormController.LANGUAGE_REPLACE_TEXT,language).replace(FieloPRM_C_PartnerLocatorFormController.COUNTRY_CODE_REPLACE_TEXT,countryCode):null;
		}
	}

	@RemoteAction
	global static List<SearchItem> getAutocomplete(String autocompleteURL,String searchEncoding,String searchImplementation,String submitURL,String txt,Boolean isAPC, String baseURLAPC,String submitURLAPC,String autocompleteURLAPC ) {
		List<SearchItem> returnList = new List<SearchItem>();
		if(autocompleteURL==null || txt==null){
			return returnList;
		}
		String unEncodedTxt=txt;
		txt = EncodingUtil.urlEncode(txt,searchEncoding);

		HttpRequest req = new HttpRequest();
		req.setTimeout(30000);
		if(isAPC){
			req.setEndpoint(autocompleteURLAPC+txt+'*');
			req.setMethod('GET');
			Http http = new Http();
			HTTPResponse res = http.send(req);
			System.debug(res.getBody());
			
			JSONParser parser = JSON.createParser(res.getBody());
			while (parser.nextToken() != null) {
				if(parser.getText()=='dimensionSearchValues'){
					system.debug('parser: ' + parser);
					parser.nextToken();
					system.debug('parser: ' + parser);
					List<FieloPRM_VFC_SearchGeneralSearch.APCItem> itemList = (List<FieloPRM_VFC_SearchGeneralSearch.APCItem>) parser.readValueAs(List<FieloPRM_VFC_SearchGeneralSearch.APCItem>.class);
					system.debug('itemList: ' + itemList);
					for(APCItem item: itemList){
						returnList.add(new SearchItem(item.label,item.label,baseURLAPC+'search'+item.navigationState));
					}

				}
			}
		}
		else if(GSS_IMPLEMENTATION.equals(searchImplementation)){
			txt = txt.replace(' ','-');
			req.setEndpoint(autocompleteURL+txt);
			req.setMethod('GET');
			Http http = new Http();
			HTTPResponse res = http.send(req);
			Integer redirection=0;
			while((res.getStatusCode()==301 || res.getStatusCode()==302) && redirection<MAX_REDIRECTION){
				redirection++;
			    String endpoint ='';
			    if(res.getHeader('Location')!=null){
				    if(!res.getHeader('Location').startsWith('http')){
				       String regExp = '^(([a-zA-Z]+://)?[a-zA-Z0-9.-]+\\.[a-zA-Z]+(:d+)?)';
				        Pattern pattern1 = Pattern.compile(regExp);
				        Matcher matcher = pattern1.matcher(req.getEndpoint());
				        if(matcher.find()){
				           endpoint =matcher.group(1);
				           system.debug('**endpoint**'+endpoint);     
				        } 
				    }
				    req = new HttpRequest();
				    req.setTimeout(30000);
				    endpoint += res.getHeader('Location').replace(unEncodedTxt,txt);
				    req.setEndpoint(endpoint);
				    req.setMethod('GET');
				    res = http.send(req);
			    }else{
			    	//Nothing
			    }
			}
			System.debug(res.getBody());
			
			JSONParser parser = JSON.createParser(res.getBody());
			while (parser.nextToken() != null) {
	            if (parser.getCurrentToken() == JSONToken.START_ARRAY){
	                while(parser.nextToken() != null){
	                    if(parser.getCurrentToken() == JSONToken.START_OBJECT){
	                        GSSItem item = (GSSItem) parser.readValueAs(GSSItem.class);
	                        system.debug('Item: ' + item);
	                        if(item!=null && item.keywords!=null)
	                        	returnList.add(new SearchItem(item.keywords,item.keywords,getSubmitURL(searchImplementation,submitURL,item.keywords)));
	                    }
	                }
	            }
	        }
	    }else{
	    	req.setEndpoint(autocompleteURL+txt);
			req.setMethod('GET');
			Http http = new Http();
			HTTPResponse res = http.send(req);
			System.debug(res.getBody());
			JSONParser parser = JSON.createParser(res.getBody());
			
			while (parser.nextToken() != null && !(parser.getText()=='autn_hit'  && parser.getCurrentToken()==JSONToken.FIELD_NAME)){
			    while (parser.nextToken() != null) {
			        if (parser.getCurrentToken() == JSONToken.START_ARRAY){
			            while(parser.nextToken() != null && parser.getCurrentToken() != JSONToken.END_ARRAY){
			                system.debug('Item: ' +parser.getText()+' '+parser.getCurrentToken());
			                GSAItem item = (GSAItem) parser.readValueAs(GSAItem.class);
			                if(item!=null && item.autn_content!=null && item.autn_content.document!=null && item.autn_content.document.keywords!=null)
	                        	returnList.add(new SearchItem(item.autn_content.document.keywords,item.autn_content.document.keywords,item.autn_content.document.url));
			            }
			        }
			    }
			}
	    }

		return returnList;
	}

	public static String getSubmitURL(String searchImplementation,String submitURL,String txt){
		if(GSS_IMPLEMENTATION.equals(searchImplementation)){
			return submitURL+txt.replace(' ', '-');
		}else{
			return submitURL+txt;
		}
	}


	global class SearchItem{
		
		public String value;
		public String label;
		public String url;
		
		public SearchItem(String value,String label,String url){
			this.value=value;
			this.label=label;
			this.url=url;
		}
	}

	public class GSSItem{
		public String id {get;set;}
		public String keywords {get;set;}
	}

	public class GSAItem{
		public String autn_title {get;set;}
		public Autn_content autn_content {get;set;}
	}

	class Autn_content {
		public Document document {get;set;}
	}
	class Document {
		public String keywords {get;set;}
		public String url {get;set;}
	}


	public class APCItem{
		public String label {get;set;}
		public String navigationState {get;set;}
	}
	
}