/************************************************************
* Developer: Schwarzböck, Elena J.                           *
* Type: Test Class                                           *
* Created Date: 22.09.2014                                   *
* Tested Class: Fielo_InvoiceTriggers                        *
**************************************************************/

@isTest
public with sharing class Fielo_InvoiceTriggersTest {
  
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(FieloEE__Member__c = false);
        insert deactivate;
        
        /*Country__c country = new Country__c(
            Name = 'United Arab Emirates', 
            CountryCode__c = 'AE'
        );
        insert country;

        FieloEE__Member__c member = new FieloEE__Member__c(
            FieloEE__LastName__c = 'Test Last Name', 
            FieloEE__FirstName__c = 'Test Name',
            F_CompanyName__c = 'test',
            F_CompanyPhoneNumber__c = 'test',
            F_Country__c = country.Id,
            F_Address1__c = 'test',
            F_NearLandmark__c = 'test',
            F_City__c = 'test',
            F_POBox__c = 'test',
            F_CorporateHeadquarters__c = true,
            F_BusinessType__c = 'test',
            F_AreaOfFocus__c = 'test'
        );*/
        
        FieloEE__Member__c member = new FieloEE__Member__c(
            FieloEE__LastName__c = 'Test Last Name', 
            FieloEE__FirstName__c = 'Test Name'
        );        
        insert member;
        
        Fielo_Invoice__c invoice = new Fielo_Invoice__c(
            F_InvoiceDate__c = date.today(),
            F_Member__c = member.Id,
            F_Status__c = 'Pending'            
        );
        insert invoice;
        
    }
}