global class AccountResponseContext
{
    WebService String[] assessmentIDs;
    WebService String[] accountIDs;

    WebService Integer StartRow = 1;
    WebService Integer BatchSize = 10000;
     
    WebService DateTime LastModifiedDate1;
    WebService DateTime LastModifiedDate2;
    WebService DateTime CreatedDate;
    
    /**
    * A blank constructor
    */
    public AccountResponseContext() {
    }

    /** 
    * A constructor
    */
    public AccountResponseContext(List<String> assessmentIDs) {
        this.assessmentIDs = assessmentIDs;

    }

     public AccountResponseContext(List<String> assessmentIDs, List<String> accountIDs) {
        this.assessmentIDs = assessmentIDs;
        this.accountIDs = accountIDs;

    }
}