@isTest
global class WS_MockClassPRMPrivateRegistration_Test implements WebServiceMock{

    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
           
       
      if(request instanceOf uimsv2ServiceImsSchneiderComNew2.getUser){
          system.debug('**GetUser**');
          uimsv2ServiceImsSchneiderComNew2.getUserResponse respGetUser = new uimsv2ServiceImsSchneiderComNew2.getUserResponse();
          uimsv2ServiceImsSchneiderComNew2.userV5 uimsUser = new uimsv2ServiceImsSchneiderComNew2.userV5();
          uimsUser.federatedID = '188b80b1-c622-4f20-8b1d-3ae86af11c4a';
          uimsUser.email       = 'test1@test.com';
          uimsUser.firstName = 'Subhashish_Test1';
          uimsUser.lastName  = 'Sahu_Test1';
          uimsUser.phone     = '9999999999';     
            //uimsUser.cell
          uimsUser.jobTitle    = 'ZC';
          uimsUser.jobFunction = 'Z019';
          uimsUser.channel     = 'FI'; 
          uimsUser.subChannel  = 'FI3'; 
          uimsUser.primaryContact = False; //Not Always True
          uimsUser.companyId      = '6372c85d-123b-4ff4-b134-088c4077111d';  
        //uimsUser.street         = '';
        //uimsUser.addInfoAddress = ;
          uimsuser.localityName   = 'Bangalore'; 
            //uimsuser.postalCode;
          uimsUser.countryCode = 'IN';
          uimsUser.state= '10';
          
          respGetUser.return_x = uimsUser;
          response.put('response_x',respGetUser);
          system.debug('**Response-->getUser**'+response);
          
          
      }
      
      /*if(request instanceOf  uimsv2ServiceImsSchneiderComNew.getCompany){
          system.debug('**GetComapny**');
          uimsv2ServiceImsSchneiderComNew.getCompanyResponse respGetCompany = new uimsv2ServiceImsSchneiderComNew.getCompanyResponse();
          uimsv2ServiceImsSchneiderComNew.companyV3 companyInfo = new uimsv2ServiceImsSchneiderComNew.companyV3();
          
          companyInfo.organizationName = 'SchneiderTest';
          companyInfo.businessType     = 'FI';
          companyInfo.customerClass  = 'FI3';
          //companyInfo.marketServed;
          //companyInfo.employeeSize;
          companyInfo.street            = 'Elnath street';
          companyInfo.localityName      = 'Bangalore';
          companyInfo.postalCode        = '560103';
          companyInfo.federatedId       ='6372c85d-123b-4ff4-b134-088c4077111d';
          companyInfo.publicVisibility  = True;
          companyInfo.headQuarter       = 'True';
          respGetCompany.company  =  companyInfo;
          response.put('response_x',respGetCompany);
          system.debug('**Response-->getCompany**'+response);
      
      }*/
      
      if(request instanceOf  WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompany){
          system.debug('**GetComapny**');
          WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyResponse respGetCompany = new WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyResponse();
          WS_PRM_UIMSCompanyMgmtServiceModel_User.companyV3 companyInfo = new WS_PRM_UIMSCompanyMgmtServiceModel_User.companyV3();
          
          companyInfo.organizationName = 'SchneiderTest';
          companyInfo.businessType     = 'FI';
          companyInfo.customerClass  = 'FI3';
          //companyInfo.marketServed;
          //companyInfo.employeeSize;
          companyInfo.street            = 'Elnath street';
          companyInfo.localityName      = 'Bangalore';
          companyInfo.postalCode        = '560103';
          companyInfo.federatedId       ='6372c85d-123b-4ff4-b134-088c4077111d';
          companyInfo.publicVisibility  = True;
          companyInfo.headQuarter       = 'True';
          respGetCompany.company  =  companyInfo;
          response.put('response_x',respGetCompany);
          system.debug('**Response-->getCompany**'+response);
      
      }
      
      if(request instanceOf  WS_UIMSAdminService_GetModel.getAccessControl){
          system.debug('**GetComapny**');
          WS_UIMSAdminService_GetModel.getAccessControlResponse respGetAccessControl = new WS_UIMSAdminService_GetModel.getAccessControlResponse();
          WS_UIMSAdminService_GetModel.accessTree accesst = new WS_UIMSAdminService_GetModel.accessTree();
          WS_UIMSAdminService_GetModel.accessList_element accessLst = new WS_UIMSAdminService_GetModel.accessList_element();
          WS_UIMSAdminService_GetModel.accessElement accessElement = new WS_UIMSAdminService_GetModel.accessElement();
          List<WS_UIMSAdminService_GetModel.accessElement> accessElementLst = new List<WS_UIMSAdminService_GetModel.accessElement>();
          
          
          accessElement.type_x = 'PRM';
          accessElement.Id ='PRM';
          accessElementLst.add(accessElement);
          accessLst.access = accessElementLst;
          
          accesst.accessList= accessLst;
          
          respGetAccessControl.return_x = accesst;
          
          
          response.put('response_x',respGetAccessControl);
          system.debug('**Response-->getCompany**'+response);
      
      }

    }
}