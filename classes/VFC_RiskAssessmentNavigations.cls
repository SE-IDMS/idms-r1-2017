public with sharing class VFC_RiskAssessmentNavigations 
{
    public String urlValue{get;set;}
    public CFWRiskAssessment__c rskAssessment{get;set;}
    public boolean submittedForApproval{get;set;}
    public boolean renderIDCard{get;set;}
    public boolean renderBusiness{get;set;}    
    public boolean renderData{get;set;}
    public boolean renderApplication{get;set;}
    public boolean renderInfrastructure{get;set;}
    public boolean renderServDepl{get;set;}  
    public boolean renderRskAssessment{get;set;}              
    
    //CONSTRUCTOR
    public VFC_RiskAssessmentNavigations(ApexPages.StandardController controller) 
    {
        rskAssessment = (CFWRiskAssessment__c)controller.getRecord();
        rskAssessment = Database.query(queryAllFields());    
        if(rskAssessment.SubmittedforApproval__c)
            submittedForApproval = true; 

        if(ApexPages.currentPage()!=null && ApexPages.currentPage().getURL()!=null && ApexPages.currentPage().getURL().contains('VFP_RiskAssessment?id='))
            renderRskAssessment = true;
        else if(system.currentpagereference().getparameters().get('RecordType')==null)    
        {
            if(rskAssessment.recordTypeId == Label.CLOCT15CFW01) 
                renderIDCard = true;
            else if(rskAssessment.recordTypeId == Label.CLOCT15CFW02) 
                renderBusiness= true;
            else if(rskAssessment.recordTypeId == Label.CLOCT15CFW03) 
                renderData= true;
            else if(rskAssessment.recordTypeId == Label.CLOCT15CFW23) 
                renderApplication= true;
            else if(rskAssessment.recordTypeId == Label.CLOCT15CFW26) 
                renderInfrastructure= true;
            else if(rskAssessment.recordTypeId == Label.CLOCT15CFW27) 
                renderServDepl= true;
        }
        else if(system.currentpagereference().getparameters().get('RecordType')!=null)    
        {
            if(system.currentpagereference().getparameters().get('RecordType') == Label.CLOCT15CFW01) 
                renderIDCard = true;
            else if(system.currentpagereference().getparameters().get('RecordType') == Label.CLOCT15CFW02) 
                renderBusiness= true;
            else if(system.currentpagereference().getparameters().get('RecordType') == Label.CLOCT15CFW03) 
                renderData= true;
            else if(system.currentpagereference().getparameters().get('RecordType') == Label.CLOCT15CFW23) 
                renderApplication= true;
            else if(system.currentpagereference().getparameters().get('RecordType') == Label.CLOCT15CFW26) 
                renderInfrastructure= true;
            else if(system.currentpagereference().getparameters().get('RecordType') == Label.CLOCT15CFW27) 
                renderServDepl= true;
        }      
        
            
    }
    
    //REDIRECTS TO THE CORRESPONDING PAGE
    public pagereference urlPageRedirect()
    {
        if(urlValue.startsWith('012'))
            return new Pagereference('/apex/VFP_CFWDetailPage?RecordType='+urlValue+'&id='+rskAssessment.Id);        
        else if(urlValue == 'VFP_CertificationChecklist')
        {
            //CHECKS IF CERTIFICATION CHECKLIST RECORD ALREADY EXISTS, IF NOT, CREATES THE RECORD
            List<CertificationChecklist__c> certAssessments = [select Id,CertificationRiskAssessment__c from CertificationChecklist__c where CertificationRiskAssessment__c=:rskAssessment.Id];
            if(certAssessments!=null && certAssessments.size()>0)
                return new pagereference('/apex/'+urlValue+'?id='+certAssessments[0].Id);
            else
            {
                CertificationChecklist__c certChecklist = new CertificationChecklist__c();
                certChecklist.CertificationRiskAssessment__c = rskAssessment.Id;
                insert certChecklist;
                return new pagereference('/apex/'+urlValue+'?id='+certChecklist.Id);
            }
        }
        else if(urlValue == 'VFP_CFWGuidelinesPolicies')
        {
            //CHECKS IF GUIDELINES RECORD ALREADY EXISTS, IF NOT, CREATES THE RECORD
            List<CertificationGuidelines__c> certGuidelines= [select Id,CertificationRiskAssessment__c from CertificationGuidelines__c where CertificationRiskAssessment__c=:rskAssessment.Id];
            if(certGuidelines!=null && certGuidelines.size()>0)
                return new pagereference('/apex/'+urlValue+'?id='+certGuidelines[0].Id);
            else
            {
                CertificationGuidelines__c certGuideline= new CertificationGuidelines__c();
                certGuideline.CertificationRiskAssessment__c = rskAssessment.Id;
                insert certGuideline;
                return new pagereference('/apex/'+urlValue+'?id='+certGuideline.Id);
            }
        }
        else
            return new pagereference('/apex/'+urlValue+'?id='+rskAssessment.Id);
    }
    
    //QUERY ALL RISK ASSESSMENT FIELDS
    public String queryAllFields()
    {
        Schema.DescribeSObjectResult descRes =  CFWRiskAssessment__c.sObjectType.getDescribe();
        List<Schema.SObjectField> tempFields = descRes.fields.getMap().values();
        List<Schema.DescribeFieldResult> fields  = new List<Schema.DescribeFieldResult>();
        for(Schema.SObjectField sof : tempFields)
        {
            fields.add(sof.getDescribe());
        } 
        String query = 'select ';
        for(Schema.DescribeFieldResult dfr : fields)
        {
            query = query + dfr.getName() + ',';
        }
        query = query.subString(0,query.length() - 1);
        query = query + ' from ';
        query = query + descRes.getName();
        query = query +  ' where Id = \'';
        query = query + rskAssessment.Id + '\'';
        system.debug('Build Query == ' + query);
        return query;
    }

}