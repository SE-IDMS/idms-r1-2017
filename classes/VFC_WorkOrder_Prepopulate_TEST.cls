/*
    Author          : Shruti Karn
    Date Created    : 26/11/2012
    Description     : Test class for VFC_WorkOrder_Prepopulate class 
*/
@isTest(SeeAllData=true)
private class VFC_WorkOrder_Prepopulate_TEST {
    static testMethod void testWorkOrderPrepopulate(){
        
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        
        User usr1 = Utils_TestMethods.createStandardUser('alias');
        usr1.UserBusinessUnit__c='BD';
        usr1.Country__c='IN';
        insert usr1;
        
        case cse= new case(SupportCategory__c='6-General',AccountId=testAccount.id,
                Priority='Medium',
                Subject='Complaint',
                origin='Phone');
        insert cse;
        
        
        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm;
        RecordType[] rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c'];
        RecordType[] rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c'];  
          
         for(RecordType rt : rts) 
        {
            if(rt.DeveloperName == 'Technician')
           {
                 st = new SVMXC__Service_Group__c(
                                            RecordTypeId =rt.Id,
                                            SVMXC__Active__c = true,
                                            Name = 'Test Service Team'                                                                                        
                                            );
                insert st;
           } 
        }
        for(RecordType rt : rtssg) //Loop to take a record type at a time
        {
           if(rt.DeveloperName == 'Technican')
            {
                 sgm = new SVMXC__Service_Group_Members__c(
                                            RecordTypeId =rt.Id,
                                            Name = 'Test user',
                                            SVMXC__Service_Group__c =st.id,
                                            SVMXC__Role__c = 'Schneider Employee',
                                            SVMXC__Salesforce_User__c = usr1.Id                                         
                                            );
              insert sgm;              
            }
        }
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'Test Location';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = testAccount.id;
        site.PrimaryLocation__c = true;
        site.PreferredFSE__c = sgm.Id;
        insert site;
        
         SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = testAccount.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId1';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site.id;
        ip1.DeviceTypeToCreate__c = 'device1';
        ip1.LifeCycleStatusOfTheInstalledProduct__c = system.label.CLAPR14SRV05;
        ip1.DecomissioningDate__c = date.today();
        ip1.AssetCategory2__c =system.label.CLAPR14SRV01;
        ip1.SVMXC__Serial_Lot_Number__c = '1234';
        insert ip1;
        
         SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c(Name='test');
         insert sc;
         
        SVMXC__Service_Order__c workOrder = Utils_TestMethods.createWorkOrder(testAccount.Id);
        workOrder.SVMXC__Site__c = site.id;
        insert workOrder;
        
        WorkOrderGroup__c wog = New WorkOrderGroup__c(
                                                    Account__c =testAccount.Id,
                                                    SoldTo__c  =testAccount.Id,
                                                    ServiceMaintenanceContract__c=sc.Id 
                                                        );
        insert wog;
                
        ApexPages.StandardController controller = new ApexPages.StandardController(workOrder);
        VFC_WorkOrder_Prepopulate newObj = new VFC_WorkOrder_Prepopulate(controller);        
        PageReference pageRef = Page.VFP_WorkOrder_PrePopulate ;
        pageRef.getParameters().put(Label.CLMAY13SRV10, site.Id);       
        Test.setCurrentPage(pageRef);
        newObj.gotoeditpage();
        
        
        VFC_WorkOrder_Prepopulate newObj00 = new VFC_WorkOrder_Prepopulate(controller);        
        PageReference pageRef00 = Page.VFP_WorkOrder_PrePopulate ;
        pageRef00.getParameters().put(Label.CLDEC12SRV43, cse.Id);       
        Test.setCurrentPage(pageRef00);
        newObj00.gotoeditpage();
        
        
        
        
        
        //ApexPages.StandardController controller = new ApexPages.StandardController(workOrder);
        VFC_WorkOrder_Prepopulate newObj1 = new VFC_WorkOrder_Prepopulate(controller);        
        PageReference pageRef1 = Page.VFP_WorkOrder_PrePopulate ;
        pageRef1.getParameters().put(Label.CLDEC12SRV04, testAccount.Id);       
        Test.setCurrentPage(pageRef1);
        newObj1.gotoeditpage();
        
        VFC_WorkOrder_Prepopulate newObj2 = new VFC_WorkOrder_Prepopulate(controller);        
        PageReference pageRef2 = Page.VFP_WorkOrder_PrePopulate ;
        pageRef2.getParameters().put(Label.CLOCT13SRV03, wog.Id);       
        Test.setCurrentPage(pageRef2);
        newObj2.gotoeditpage();
        
        VFC_WorkOrder_Prepopulate newObj3 = new VFC_WorkOrder_Prepopulate(controller);        
        PageReference pageRef3 = Page.VFP_WorkOrder_PrePopulate ;
        pageRef1.getParameters().put(Label.CLMAY13SRV06, sc.Id);       
        Test.setCurrentPage(pageRef3);
        newObj3.gotoeditpage();
        
        VFC_WorkOrder_Prepopulate newObj4 = new VFC_WorkOrder_Prepopulate(controller);        
        PageReference pageRef4 = Page.VFP_WorkOrder_PrePopulate ;
        pageRef4.getParameters().put(Label.CLMAY13SRV04, ip1.Id);       
        Test.setCurrentPage(pageRef4);
        newObj4.gotoeditpage();
    }
}