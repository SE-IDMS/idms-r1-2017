/*
    Author          : Ramakrishna Singara (Schneider Electric)
    Date Created    : 14/12/2012
    Description     : Test class for the class PRJ_CreateDeepClone
*/


@isTest
    private class PRJ_CreateDeepClone_TEST
    {
         static testMethod void PRJ_CreateDeepCloneUnitTest()
        {   
            DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
            insert testDMTA1; 
                    
            PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.Description__c = 'Test';
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            insert testProj;
            testProj.NextStep__c = 'Created';
            testProj.BusinessTechnicalDomain__c = 'Supply Chain';
            Budget__c Budget = new Budget__c(ProjectReq__c =testProj.id,Active__c=true);
            Insert Budget ;
            //update testProj;
            DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
            insert testDMTA2; 
            
            testProj.NextStep__c = 'Valid';
            testProj.ExpectedStartDate__c = system.today();
            testProj.GoLiveTargetDate__c = system.today();
            testProj.AppServicesDecision__c = 'Yes';
            testProj.AppGlobalOpservdecisiondate__c = system.today();
            update testProj; 
            
            DMTAuthorizationMasterData__c testDMTA = new DMTAuthorizationMasterData__c();
            testDMTA.AuthorizedUser5__c = UserInfo.getUserId();
             testDMTA.AuthorizedUser6__c = UserInfo.getUserId();
            testDMTA.NextStep__c = 'quoted';
            testDMTA.Parent__c = testProj.Parent__c;
            testDMTA.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
            testDMTA.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
            insert testDMTA;  

            testProj.NextStep__c = 'Quoted';
            testProj.DecisionSecurityResponsible__c = 'Yes';
            testProj.DecisionSecurityResponsibleDate__c = system.today();
            testProj.DecisionControllerProposalValidation__c = 'Yes';
            testProj.DecisionProposalValidationDate__c = system.today();
            testProj.ScoreResultCalculated__c = '1 (Nice to have)';
            update testProj;

            DMTFundingEntity__c dmtfund = new DMTFundingEntity__c();
            dmtfund.ProjectReq__c = testProj.id;
            dmtfund.ParentFamily__c = 'Business';
            dmtfund.Parent__c = 'Buildings';
            dmtfund.GeographicZoneSE__c = 'Global';
            dmtfund.ActiveForecast__c = true;
            dmtfund.BudgetYear__c = '2012';
            dmtfund.ForecastYear__c = '2012';
            dmtfund.ForecastQuarter__c = 'Q4';
            insert dmtfund;
            dmtfund.CAPEXY0__c = 100;
            dmtfund.SelectedinEnvelop__c =true;
            dmtfund.HFMCode__c = 'Test';
            update dmtfund;
            test.startTest();
            
            DMTAuthorizationMasterData__c testDMTA3 = new DMTAuthorizationMasterData__c();
            testDMTA3.AuthorizedUser1__c = UserInfo.getUserId();
            testDMTA3.AuthorizedUser5__c = UserInfo.getUserId();
             testDMTA3.AuthorizedUser6__c = UserInfo.getUserId();
            testDMTA3.NextStep__c = 'Fundings Authorized';
            testDMTA3.Parent__c = testProj.Parent__c;
            testDMTA3.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
            testDMTA3.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
            insert testDMTA3; 
            
            testProj.NextStep__c = 'Fundings Authorized';
            testProj.AuthorizeFunding__c = 'Yes';
            testProj.ProjectClosureDate__c = system.today();
            testProj.EffectiveGoLiveDate__c = system.today();
            update testProj;  

            
            
            PRJ_CreateDeepClone.PRJ_createDeepClones(testProj.id);
            testProj.BusGeographicZoneIPO__c = 'Europe';
           List<DMTFundingEntity__c> fund=[select id,SelectedinEnvelop__c,ActiveForecast__c from DMTFundingEntity__c where ProjectReq__c = :testProj.id and ActiveForecast__c = true ];
            for(DMTFundingEntity__c f:fund){
                    f.SelectedinEnvelop__c =true;
                    update f;
            }
            update testProj;
            PRJ_CreateDeepClone.PRJ_createDeepClones(testProj.id);
            test.stopTest();
             
        }  
        static testMethod void PRJ_CreateDeepCloneUnitTest1()
        {
            PRJ_CreateDeepClone.PRJ_createDeepClones('');
            test.startTest();                    
                 PRJ_CreateDeepClone.PRJ_createCustomClone('');
            test.stopTest();
        } 
        
              static testMethod void PRJ_CreateDeepCloneUnitTest2()
        {
              DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
                insert testDMTA1;                 
              PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.Description__c = 'Test';
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            insert testProj;
            testProj.NextStep__c = 'Created';
            testProj.BusinessTechnicalDomain__c = 'Supply Chain';
            Budget__c Budget = new Budget__c(ProjectReq__c =testProj.id,Active__c=true);
            Insert Budget ;
            //update testProj;
            
            DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
            insert testDMTA2; 
            
            testProj.NextStep__c = 'Valid';
            testProj.ExpectedStartDate__c = system.today();
            testProj.GoLiveTargetDate__c = system.today();
            testProj.AppServicesDecision__c = 'Yes';
            testProj.AppGlobalOpservdecisiondate__c = system.today();
            update testProj; 
            
            
            test.startTest();  
            PRJ_CreateDeepClone.PRJ_createCustomClone(testProj.id);
               
            test.stopTest();
        } 
       
    }