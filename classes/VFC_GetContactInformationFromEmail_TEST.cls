@isTest
private with sharing class VFC_GetContactInformationFromEmail_TEST {

    static testMethod void verifyEmailIdTest() {
        String SfUrl = URL.getSalesforceBaseUrl().toExternalForm();
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        StateProvince__c st = Utils_TestMethods.createStateProvince(country.id);
        insert st;
        
        list<Account> newAccounts = new list<Account>();
        list<Contact> newContacts = new list<Contact>();
        list<ANI__c> newANIs = new list<ANI__c>();
        
        Account account = Utils_TestMethods.createAccount();
        account.Phone = '986435';
        account.Country__c = country.id;
        account.StateProvince__c = st.id;
        account.LeadingBusiness__c = 'Buildings';
        account.ClassLevel1__c = 'Retailer';
        account.Type = 'Prospect';
        //account.CurrencyIsoCode ='IND';
        account.City__c = 'bangalore';
        account.ZipCode__c= '452234';
        account.Street__c= 'aSDASDASD';
        account.POBox__c = '122344';
        newAccounts.add(account);
        system.debug('#######'+account);
        
        Account account2 = Utils_TestMethods.createAccount();
        account2.Phone = '426272';
        account2.Country__c = country.id;
        account2.StateProvince__c = st.id;
        account2.LeadingBusiness__c = 'Buildings';
        account2.ClassLevel1__c = 'Retailer';
        account2.Type = 'Prospect';
        //account.CurrencyIsoCode ='IND';
        account2.City__c = 'bangalore';
        account2.ZipCode__c= '452234';
        account2.Street__c= 'aSDASDASD';
        account2.POBox__c = '122344';
        newAccounts.add(account2); 
        system.debug('#######' + account2);
        
        Account account3 = Utils_TestMethods.createConsumerAccount(country.id);
        account3.PersonEmail= 'contact.two@schneider-electric.com';
        newAccounts.add(account3); 
        
        system.debug('#######' + account3);
        
        insert newAccounts;
                
        Contact con = Utils_TestMethods.createContact(account.id, 'test1');
        con.firstname= 'test55';
        con.LastName = 'asfsdf';
        con.WorkPhone__c= '723472349';
        con.Email = 'contact.one@schneider-electric.com';
        con.accountid=account.id;
        newContacts.add(con);
        system.debug('#######' + con);
        
        Contact con2 = Utils_TestMethods.createContact(account.id, 'test2');
        con2.firstname= 'test55';
        con2.LastName = 'asfsdf';
        con2.WorkPhone__c= '824162';
        con2.Email = 'contact.two@schneider-electric.com';
        con2.accountid=account2.id;
        newContacts.add(con2);
        system.debug('#######' + con2);
        
        Contact con3 = Utils_TestMethods.createContact(account.id, 'Contact 3');
        con3.firstname = 'Contact';
        con3.LastName = 'Test 3';
        con3.WorkPhone__c = '986435';
        con3.Email = 'contact.two@schneider-electric.com';
        con3.accountid = account2.id;
        newContacts.add(con3);
        system.debug('#######' + con3);
        
        insert newContacts;
        
        
        PageReference pageRef = Page.VFP_GetContactInformationFromEmail;
        Test.setCurrentPage(pageRef);
        
        VFC_GetContactInformationFromEmail controller = new VFC_GetContactInformationFromEmail();
        //No Email Id 
        controller.verifyEmailId();
        //No results
        
        
        controller.btestflag = true;
        controller.verifyEmailId();
        controller.btestflag = false;
        
        //only 2 charactors in the Phone Number
        ApexPages.currentPage().getParameters().put('emailId', 'cc');
        system.debug('#################' + ApexPages.currentPage().getParameters().get('emailId'));
        controller.verifyEmailId();
        
        
        //Only one Contact
        Id [] fixedSearchResults= new Id[2];
        fixedSearchResults.clear();
        fixedSearchResults.add(con.id);
        Test.setFixedSearchResults(fixedSearchResults);
        ApexPages.currentPage().getParameters().put('emailId', 'contact.one@schneider-electric.com');
        system.debug('#################'+ApexPages.currentPage().getParameters().get('emailId'));
        Pagereference conpage = controller.verifyEmailId();
        //system.assertEquals(conpage.getUrl(), SfUrl+'/' + con.id);
               
        
        //Two Contacts
        fixedSearchResults.clear();
        fixedSearchResults.add(con2.id);
        fixedSearchResults.add(con3.id);
        fixedSearchResults.add(account3.id);
        Test.setFixedSearchResults(fixedSearchResults);
        ApexPages.currentPage().getParameters().put('emailId', 'contact.two@schneider-electric.com');
        system.debug('#################' + ApexPages.currentPage().getParameters().get('emailId'));
        controller.verifyEmailId();
        //system.assertEquals(controller.lstAccWrap.size(), 2);        
            }
    static testMethod void verifyEmailIdTest1() {
    String SfUrl = URL.getSalesforceBaseUrl().toExternalForm();
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        StateProvince__c st = Utils_TestMethods.createStateProvince(country.id);
        insert st;
        
        list<Account> newAccounts = new list<Account>();
        list<Contact> newContacts = new list<Contact>();
        list<ANI__c> newANIs = new list<ANI__c>();
        
        Account account = Utils_TestMethods.createAccount();
        account.Phone = '986435';
        account.Country__c = country.id;
        account.StateProvince__c = st.id;
        account.LeadingBusiness__c = 'Buildings';
        account.ClassLevel1__c = 'Retailer';
        account.Type = 'Prospect';
        //account.CurrencyIsoCode ='IND';
        account.City__c = 'bangalore';
        account.ZipCode__c= '452234';
        account.Street__c= 'aSDASDASD';
        account.POBox__c = '122344';
        newAccounts.add(account);
        insert newAccounts;
        Contact con = Utils_TestMethods.createContact(account.id, 'test1');
        con.firstname= 'test55';
        con.LastName = 'asfsdf';
        con.WorkPhone__c= '723472349';
        con.Email = 'contact.one@schneider-electric.com';
        con.accountid=account.id;
        newContacts.add(con);
        insert newContacts;
        
        
        PageReference pageRef = Page.VFP_GetContactInformationFromEmail;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('email', 'cc');
        VFC_GetContactInformationFromEmail controller = new VFC_GetContactInformationFromEmail();
        controller.bTestFlag=true;
        controller.verifyEmailId();
        }
        
}