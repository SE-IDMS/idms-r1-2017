public class AP_RPDT_CreateNewRPDT {
   
    public static void createNewRPDT(List<RMAShippingToAdmin__c> RPDTs) {
    
    set <String> strCountry = new set <String> ();
    set <String> strCR = new set <String> ();
    set <String> strPF = new set <String> ();
    set <String> strRC = new set <String> ();
    Map<String,RMAShippingToAdmin__c> keyObejctMap= new Map<String,RMAShippingToAdmin__c >();
    
    for(RMAShippingToAdmin__c rpdt : RPDTs)
    {   
        if(rpdt.country__c!= null)
        strCountry.add(rpdt.Country__c);
        
        if (rpdt.CommercialReference__c!=null)
        strCR.add(rpdt.CommercialReference__c);
        
        if(rpdt.TECH_ProductFamily__c!=null)
        strPF.add(rpdt.TECH_ProductFamily__c);
        
        if(rpdt.ReturnCenter__c!=null)
        strRC.add(rpdt.ReturnCenter__c);
        
        // Copying value for the new text field
        rpdt.TECH_Capability__c = rpdt.Capability__c;
    
        /*
        strCountry.add(rpdt.Country__c);
        strCR.add(rpdt.CommercialReference__c);
        strPF.add(rpdt.TECH_ProductFamily__c);
        strRC.add(rpdt.ReturnCenter__c);     
        */ 
    }
    
        system.debug ('## strCountry ##'+ strCountry);
        system.debug ('## strCR ##'+ strCR );
        system.debug ('## strPF ##'+ strPF);
        system.debug ('## strRC ##'+ strRC);
    
    List<RMAShippingToAdmin__c> existingRecords =[ SELECT country__c, ReturnCenter__c,Capability__c,CommercialReference__c, TECH_ProductFamily__c from RMAShippingToAdmin__c where (Country__c in :strCountry) AND (CommercialReference__c in :strCR) AND  (TECH_ProductFamily__c  in :strPF ) AND  (ReturnCenter__c in :strRC )];
    
    system.debug ('## existingRecords ##'+ existingRecords);
    
    if(existingRecords!=null &&  existingRecords.size()>0)
    {
        
        for(RMAShippingToAdmin__c obj: existingRecords ){
            String key='';
            if(obj.Country__c != null)
            key +=obj.Country__c;
            if(obj.CommercialReference__c!=null)
            key +=obj.CommercialReference__c;
            if(obj.TECH_ProductFamily__c!=null)
            key +=obj.TECH_ProductFamily__c;
            if(obj.ReturnCenter__c!=null)
            key +=obj.ReturnCenter__c;
            
            system.debug ('## key1 ##'+ key );
            
            keyObejctMap.put(key , obj);
            
            system.debug ('## keyObejctMap ##'+ keyObejctMap);
        }
        
        for(RMAShippingToAdmin__c rtnC: RPDTs)
        {
            String key='';
            if(rtnC.Country__c != null)
            key +=rtnC.Country__c;
            if(rtnC.CommercialReference__c!=null)
            key +=rtnC.CommercialReference__c;
            if(rtnC.TECH_ProductFamily__c!=null)
            key +=rtnC.TECH_ProductFamily__c;
            if(rtnC.ReturnCenter__c!=null)
            key +=rtnC.ReturnCenter__c;
            
             
            String CurrentRecId=rtnC.Id;
            
            system.debug ('## key2 ##'+ key );
            
            if(keyObejctMap.containsKey(key)){
            
                system.debug ('## key Found ##');
                RMAShippingToAdmin__c exObj = keyObejctMap.get(key);
                if (CurrentRecId!=exObj.Id)
                {
                // forth field validation 
                String strCAP= exObj.Capability__c;
                system.debug ('## strCAP ##'+strCAP);
                Set <string> stCapabilites = new set <string> ();
                list <STRING> lsCapabilites = new list <string>();
                lsCapabilites = strCAP.split(';');
                for (String s: lsCapabilites )
                {
                    stCapabilites.add(s);
                }
                
                String strrtnCCAP = rtnC.Capability__c;
                system.debug ('## strrtnCCAP ##'+strrtnCCAP );
                list <STRING> lsrtnCCapabilites = new list <string>();
                lsrtnCCapabilites = strrtnCCAP.split(';');
                for (String s: lsrtnCCapabilites )
                {
                   if(stCapabilites.contains(s))
                        {
                            rtnC.AddError(System.Label.CLDEC12RR07);
                        }
                else {}
                }  
            }
           }
        }
         
    }
    }

}