@isTest(SeeAllData=true)
Public class AP_CustomerSafetyIssue_Test
{
    public static testMethod void CustomerSafetyIssue() {
        //Creating Data 
        // Creating users
        List<User> userList = new List<User>();
        User CSQUser1 = Utils_TestMethods.createStandardUser('CSQUser1'); 
        userList.add(CSQUser1);
        User CSQUser2 = Utils_TestMethods.createStandardUser('CSQUser2'); 
        userList.add(CSQUser2);
        Database.insert(userList);
        
        Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );
        
        
        //Creating BusinessRiskEscalationEntity__c
        List<BusinessRiskEscalationEntity__c> breList = new List<BusinessRiskEscalationEntity__c>();
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBRE(Label.CL00319,Null,Null,Null);  
        BusinessRiskEscalationEntity__c breEntitySubEntity = Utils_TestMethods.createBRE(Label.CL00319,'SubEntity',Null,Null);   
        insert breEntity;
        insert breEntitySubEntity;
        //Database.insert(breList); 
        //Database.insert(breEntity); 
        
        
        
        //Creating Stakeholder Data
        List<EntityStakeholder__c> eshList =  new List<EntityStakeholder__c>();
       // List<EntityStakeholder__c> eshListTwo =  new List<EntityStakeholder__c>();
        List<CS_CSI__c> csdata=createCustomSetting();
        //if(csdata != null && csdata.size()>0){
            System.debug('HKDebug-->> bre ids'+breEntity.Id+'  '+breEntitySubEntity.Id);
            Integer Count=0;
            For(CS_CSI__c cs:CS_CSI__c.getAll().values()){
                Count++;
                EntityStakeholder__c CSQManager1 = Utils_TestMethods.createEntityStakeholder(breEntity.Id,userList[0].Id,cs.Role__c);
                CSQManager1.TECH_UniqueField__c = Count+'Test';
                System.debug('HKDebug-->>'+CSQManager1);
                eshList.add(CSQManager1);
                EntityStakeholder__c CSQManager2 = Utils_TestMethods.createEntityStakeholder(breEntity.Id,userList[1].Id,cs.Role__c);
                 if(CSQManager1.BusinessRiskEscalationEntity__c != null)
                eshList.add(CSQManager2);
            }
            if(eshList  != null && eshList.size()>0){
                try{
                    Database.insert(eshList); 
                }
                catch(Exception ex){
                    System.debug('HKDebug-->'+ex);
                }
            }
            
      //  }
        for(EntityStakeholder__c  esh:eshList){
            System.debug('HKDebug-->>'+esh.Role__c);
             System.debug('HKDebug-->>'+esh);
        }
        //problem creation
          Problem__c prob=Utils_TestMethods.createProblem(breEntity.Id,12);
        Database.insert(prob);
        // Creating Accounts
        List<Account> accList = new List<Account>();
        
        Account accGSA = Utils_TestMethods.createAccount();
        accGSA.Type = 'GSA';
        accGSA.Account_VIPFlag__c = 'Gold';
        accGSA.ZipCode__c='201301';
        accList.add(accGSA);
        /*Account accNonGSA = Utils_TestMethods.createAccount();
        accNonGSA.ZipCode__c='201301';
        accList.add(accNonGSA);*/
        System.debug('------------>MSP'+accList);
        Database.insert(accList);
        AccountTeamMember accTMOne=Utils_TestMethods.createAccTeamMember(accList[0].id,Userinfo.getUserId());
        accTMOne.TeamMemberRole='ESP';
        string prefix='aa';
        user  sampleUser=Utils_TestMethods.createStandardUser('sample'+prefix);
        insert sampleUser;
        Database.insert(accTMOne);
        AccountTeamMember accTMTwo=Utils_TestMethods.createAccTeamMember(accList[0].id,Userinfo.getUserId());
        accTMTwo.TeamMemberRole='ESP';
        Database.insert(accTMTwo);
        AccountTeamMember accTMThree=Utils_TestMethods.createAccTeamMember(accList[0].id,sampleUser.id);
        accTMThree.TeamMemberRole='ESP';
        Database.insert(accTMThree);
    Contact objContact = Utils_TestMethods.createContact(accList[0].id, 'TestCCCContact');
        insert objcontact;
        //Case caseRec =  Utils_TestMethods.createCase(accList[1].id,contact.id,'Open');
        //Database.insert(caseRec ); 
        Set<String> stacc = new Set<String>();
        stacc.add(accList[0].Id);
        Test.startTest();
        AP_customerSafetyIssue.getExecutiveSponser(stacc);
        
        
        
        // Creating CustomerSafetyIssue
        List<CustomerSafetyIssue__c> csiList = new List<CustomerSafetyIssue__c>();
        CustomerSafetyIssue__c  csiHDRwithNonGSA=new CustomerSafetyIssue__c();
        csiHDRwithNonGSA.AffectedCustomer__c = accList[0].id;
        csiHDRwithNonGSA.CountryOfOccurrence__c = country.id;        
        csiHDRwithNonGSA.IssueInvestigationLeader__c = UserInfo.getUserId();
        //csiHDRwithNonGSA.RelatedbFOCase__c = caseRec.id;        
        csiHDRwithNonGSA.RelatedOrganization__c = breEntity.id;  
        csiHDRwithNonGSA.BodilyInjury__c = 'Occurrence Reported';
        csiHDRwithNonGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiHDRwithNonGSA.TypeOfInjury__c = 'Death by electrocution';
        csiHDRwithNonGSA.InjuredPartyAffiliation__c = 'Customer';
        csiHDRwithNonGSA.CauseOfInjury__c = 'Electric shock';
        csiHDRwithNonGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiHDRwithNonGSA.CauseOfPropertyDamage__c = 'Explosion';
        csiHDRwithNonGSA.HumanDeathReported__c = True;   
        //csiList.add(csiHDRwithNonGSA);     
       // Database.insert(csiHDRwithNonGSA); 
        
        CustomerSafetyIssue__c  csiHDRwithGSA=new CustomerSafetyIssue__c();
        csiHDRwithGSA.AffectedCustomer__c = accList[0].id;
        csiHDRwithGSA.CountryOfOccurrence__c = country.id;        
        csiHDRwithGSA.IssueInvestigationLeader__c = UserInfo.getUserId();          
        csiHDRwithGSA.RelatedOrganization__c = breEntity.id;  
        csiHDRwithGSA.BodilyInjury__c = 'Occurrence Reported';
        csiHDRwithGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiHDRwithGSA.TypeOfInjury__c = 'Death by electrocution';
        csiHDRwithGSA.InjuredPartyAffiliation__c = 'Customer';
        csiHDRwithGSA.CauseOfInjury__c = 'Electric shock';
        csiHDRwithGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiHDRwithGSA.CauseOfPropertyDamage__c = 'Explosion';
        csiHDRwithGSA.HumanDeathReported__c = True;  
        //csiList.add(csiHDRwithGSA);        
        Database.insert(csiHDRwithGSA);
        
        CustomerSafetyIssue__c  csiwithGSA=new CustomerSafetyIssue__c();
        csiwithGSA.AffectedCustomer__c = accList[0].id;
        csiwithGSA.CountryOfOccurrence__c = country.id;        
        csiwithGSA.IssueInvestigationLeader__c = UserInfo.getUserId();          
        csiwithGSA.RelatedOrganization__c = breEntity.id;  
        csiwithGSA.BodilyInjury__c = 'Occurrence Reported';
        csiwithGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiwithGSA.TypeOfInjury__c = 'Death by electrocution';
        csiwithGSA.InjuredPartyAffiliation__c = 'Customer';
        csiwithGSA.CauseOfInjury__c = 'Electric shock';
        csiwithGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiwithGSA.CauseOfPropertyDamage__c = 'Explosion';  
        csiList.add(csiwithGSA);         
        Database.insert(csiwithGSA);
        
        CustomerSafetyIssue__c  csiNonGSA=new CustomerSafetyIssue__c();
        csiNonGSA.AffectedCustomer__c = accList[0].id;
        csiNonGSA.CountryOfOccurrence__c = country.id;        
        csiNonGSA.IssueInvestigationLeader__c = UserInfo.getUserId();          
        csiNonGSA.RelatedOrganization__c = breEntity.id;  
        csiNonGSA.BodilyInjury__c = 'Occurrence Reported';
        csiNonGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiNonGSA.TypeOfInjury__c = 'Death by electrocution';
        csiNonGSA.InjuredPartyAffiliation__c = 'Customer';
        csiNonGSA.CauseOfInjury__c = 'Electric shock';
        csiNonGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiNonGSA.CauseOfPropertyDamage__c = 'Explosion';
        csiList.add(csiNonGSA);         
       // Database.insert(csiGSA);
        
  
       System.debug('----------CSILIst'+csiList);
      // Database.insert(csiList);
        
        /*csiList[2].HumanDeathReported__c = True; 
        csiList[3].AffectedCustomer__c = accList[0].id;        
        update csiList[2];
        update csiList[3];
        
        csiList[3].CSIStatus__c = 'Closed (no safety risk)';
       
        update csiList[3];*/
        //csiList[3].CSIStatus__c = 'Closed (no safety risk)';
        Test.stopTest();
        
        
    }
    public static testMethod void CustomerSafetyIssue2() {
        //Creating Data 
        // Creating users
        List<User> userList = new List<User>();
        User CSQUser1 = Utils_TestMethods.createStandardUser('CSQUser1'); 
        userList.add(CSQUser1);
        User CSQUser2 = Utils_TestMethods.createStandardUser('CSQUser2'); 
        userList.add(CSQUser2);
        Database.insert(userList);
        
        Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );
        
        
        //Creating BusinessRiskEscalationEntity__c
        List<BusinessRiskEscalationEntity__c> breList = new List<BusinessRiskEscalationEntity__c>();
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBRE(Label.CL00319,Null,Null,Null);  
        BusinessRiskEscalationEntity__c breEntitySubEntity = Utils_TestMethods.createBRE(Label.CL00319,'SubEntity',Null,Null);      
        insert breEntity;
        insert breEntitySubEntity;
       // Database.insert(breList); 
        //Database.insert(breEntity); 
        
        
        
        //Creating Stakeholder Data
        List<EntityStakeholder__c> eshList =  new List<EntityStakeholder__c>();
       // List<EntityStakeholder__c> eshListTwo =  new List<EntityStakeholder__c>();
        List<CS_CSI__c> csdata=createCustomSetting();
        //if(csdata != null && csdata.size()>0){
            System.debug('HKDebug-->> bre ids'+breEntity.Id+'  '+breEntitySubEntity.Id);
            System.debug('---------BREIDMS-------->'+breEntity.Id);
            Integer Count=0;
            For(CS_CSI__c cs:CS_CSI__c.getAll().values()){
                Count++;
                EntityStakeholder__c CSQManager1 = Utils_TestMethods.createEntityStakeholder(breEntity.Id,userList[0].Id,cs.Role__c);
                CSQManager1.TECH_UniqueField__c = Count+'Test';
                System.debug('HKDebug-->>'+CSQManager1);
                eshList.add(CSQManager1);
                EntityStakeholder__c CSQManager2 = Utils_TestMethods.createEntityStakeholder(breEntity.Id,userList[1].Id,cs.Role__c);
                 if(CSQManager1.BusinessRiskEscalationEntity__c != null)
                eshList.add(CSQManager2);
            }
            if(eshList  != null && eshList.size()>0){
                try{
                    Database.insert(eshList); 
                }
                catch(Exception ex){
                    System.debug('HKDebug-->'+ex);
                }
            }
            
      //  }
        for(EntityStakeholder__c  esh:eshList){
            System.debug('HKDebug-->>'+esh.Role__c);
            
        }
        System.debug('-------StakeHolderMohit-------->'+eshList);
        //problem creation
          Problem__c prob=Utils_TestMethods.createProblem(breEntity.Id,12);
        Database.insert(prob);
        // Creating Accounts
        List<Account> accList = new List<Account>();
        
        Account accGSA = Utils_TestMethods.createAccount();
        //accGSA.Type = 'GSA';
        accGSA.Account_VIPFlag__c = 'Gold';
        accGSA.ZipCode__c='201301';
        accList.add(accGSA);
       
        System.debug('------------>MSP'+accList);
        Database.insert(accList);
                string prefix='aa';
        user  sampleUser=Utils_TestMethods.createStandardUser('sample'+prefix);
        insert sampleUser;
        AccountTeamMember accTMOne=Utils_TestMethods.createAccTeamMember(accList[0].id,sampleUser.id);

    
        Database.insert(accTMOne);
        
       //Case caseRec =  Utils_TestMethods.createCase(accList[1].id,contact.id,'Open');
        //Database.insert(caseRec ); 
        
        
        // Creating CustomerSafetyIssue
        List<CustomerSafetyIssue__c> csiList = new List<CustomerSafetyIssue__c>();
        CustomerSafetyIssue__c  csiHDRwithNonGSA=new CustomerSafetyIssue__c();
        csiHDRwithNonGSA.AffectedCustomer__c = accList[0].id;
        csiHDRwithNonGSA.CountryOfOccurrence__c = country.id;        
        csiHDRwithNonGSA.IssueInvestigationLeader__c = UserInfo.getUserId();
        //csiHDRwithNonGSA.RelatedbFOCase__c = caseRec.id;        
        csiHDRwithNonGSA.RelatedOrganization__c = breEntity.id;  
        csiHDRwithNonGSA.BodilyInjury__c = 'Occurrence Reported';
        csiHDRwithNonGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiHDRwithNonGSA.TypeOfInjury__c = 'Death by electrocution';
        csiHDRwithNonGSA.InjuredPartyAffiliation__c = 'Customer';
        csiHDRwithNonGSA.CauseOfInjury__c = 'Electric shock';
        csiHDRwithNonGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiHDRwithNonGSA.CauseOfPropertyDamage__c = 'Explosion';
        csiHDRwithNonGSA.HumanDeathReported__c = True;   
        //csiList.add(csiHDRwithNonGSA);     
      Database.insert(csiHDRwithNonGSA); 
      
        
    /*    CustomerSafetyIssue__c  csiHDRwithGSA=new CustomerSafetyIssue__c();
        csiHDRwithGSA.AffectedCustomer__c = accList[0].id;
        csiHDRwithGSA.CountryOfOccurrence__c = country.id;        
        csiHDRwithGSA.IssueInvestigationLeader__c = UserInfo.getUserId();          
        csiHDRwithGSA.RelatedOrganization__c = breList[0].id;  
        csiHDRwithGSA.BodilyInjury__c = 'Occurrence Reported';
        csiHDRwithGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiHDRwithGSA.TypeOfInjury__c = 'Death by electrocution';
        csiHDRwithGSA.InjuredPartyAffiliation__c = 'Customer';
        csiHDRwithGSA.CauseOfInjury__c = 'Electric shock';
        csiHDRwithGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiHDRwithGSA.CauseOfPropertyDamage__c = 'Explosion';
        csiHDRwithGSA.HumanDeathReported__c = True;  
        //csiList.add(csiHDRwithGSA);        
        Database.insert(csiHDRwithGSA);
        
        CustomerSafetyIssue__c  csiwithGSA=new CustomerSafetyIssue__c();
        csiwithGSA.AffectedCustomer__c = accList[0].id;
        csiwithGSA.CountryOfOccurrence__c = country.id;        
        csiwithGSA.IssueInvestigationLeader__c = UserInfo.getUserId();          
        csiwithGSA.RelatedOrganization__c = breList[0].id;  
        csiwithGSA.BodilyInjury__c = 'Occurrence Reported';
        csiwithGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiwithGSA.TypeOfInjury__c = 'Death by electrocution';
        csiwithGSA.InjuredPartyAffiliation__c = 'Customer';
        csiwithGSA.CauseOfInjury__c = 'Electric shock';
        csiwithGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiwithGSA.CauseOfPropertyDamage__c = 'Explosion';  
        csiList.add(csiwithGSA);         
        Database.insert(csiwithGSA);
        
        CustomerSafetyIssue__c  csiNonGSA=new CustomerSafetyIssue__c();
        csiNonGSA.AffectedCustomer__c = accList[0].id;
        csiNonGSA.CountryOfOccurrence__c = country.id;        
        csiNonGSA.IssueInvestigationLeader__c = UserInfo.getUserId();          
        csiNonGSA.RelatedOrganization__c = breList[0].id;  
        csiNonGSA.BodilyInjury__c = 'Occurrence Reported';
        csiNonGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiNonGSA.TypeOfInjury__c = 'Death by electrocution';
        csiNonGSA.InjuredPartyAffiliation__c = 'Customer';
        csiNonGSA.CauseOfInjury__c = 'Electric shock';
        csiNonGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiNonGSA.CauseOfPropertyDamage__c = 'Explosion';
        csiList.add(csiNonGSA);         */
       // Database.insert(csiGSA);
        
  
       System.debug('----------CSILIst'+csiList);
      // Database.insert(csiList);
        
        /*csiList[2].HumanDeathReported__c = True; 
        csiList[3].AffectedCustomer__c = accList[0].id;        
        update csiList[2];
        update csiList[3];
        
        csiList[3].CSIStatus__c = 'Closed (no safety risk)';
       
        update csiList[3];*/
        //csiList[3].CSIStatus__c = 'Closed (no safety risk)';
        
        
        
    }
    public static testMethod void CustomerSafetyIssue3() {
        //Creating Data 
        // Creating users
        List<User> userList = new List<User>();
        User CSQUser1 = Utils_TestMethods.createStandardUser('CSQUser1'); 
         User SampleUser0 = Utils_TestMethods.createStandardUser('smplx'); 
         insert SampleUser0;
        userList.add(CSQUser1);
        User CSQUser2 = Utils_TestMethods.createStandardUser('CSQUser2'); 
        userList.add(CSQUser2);
        Database.insert(userList);
        
        //Creating System user
        User TestUser = Utils_TestMethods.createStandardUser('testing');
        TestUser.BypassTriggers__c = 'AP1000;AP1000_1;AP1000_2;AP1000_3;AP1001;AP1002;AP1003;AP1004;UserBeforeTrigger;AP_Case_CheckOpenCCCActions;AP_Case_CreateCaseActions;AP54;AP_ACC_PartnerAccountUpdate;AP_Contact_PartnerUserUpdate;AP_ACCCreateWithSEAccountId;AP_AccountRecordUpdate;AP_Case_CaseHandlerCaseAfterUpdate;AP_Case_CaseHandlerCaseBeforeInsert;AP_Case_CaseHandlerCaseBeforeUpdate;AP_CONCreateWithSEContactID;AP_ContactPreferredAgent;AP_InvolvedOrganization;AP_CustomerSafetyIssue_After;AP_ContactBeforeUpdateHandler;AP_CaseRelatedProduct_Handler;AP_Case_MajorIssue;AP_TechnicalExpertAssessment;AP_UserActions;AP_OrgBfUpdate;AP_OrgBfInsert;AP_Case_CaseHandler;CaseAfterInsert;CaseAfterUpdate;CaseBeforeInsert;CaseBeforeUpdate;AP_AccountBeforeInsertHandler;SyncObjects;AP_updateContactOwner';
        insert TestUser;
        System.runAs(TestUser){
        Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );
        
        
        //Creating BusinessRiskEscalationEntity__c
        List<BusinessRiskEscalationEntity__c> breList = new List<BusinessRiskEscalationEntity__c>();
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBRE(Label.CL00319,Null,Null,Null);  
        BusinessRiskEscalationEntity__c breEntitySubEntity = Utils_TestMethods.createBRE(Label.CL00319,'SubEntity',Null,Null);     
        insert breEntity;
        insert breEntitySubEntity;
        //System.debug('---breList--------->'+breList);
       // Database.insert(breList); 
        //Database.insert(breEntity); 
        
        
        
        //Creating Stakeholder Data
        List<EntityStakeholder__c> eshList =  new List<EntityStakeholder__c>();
       // List<EntityStakeholder__c> eshListTwo =  new List<EntityStakeholder__c>();
        List<CS_CSI__c> csdata=createCustomSetting();
        //if(csdata != null && csdata.size()>0){
            System.debug('HKDebug-->> bre ids'+breEntity.Id+'  '+breEntitySubEntity.Id);
            System.debug('---------BREIDMS-------->'+breEntity.Id);
            Integer Count=0;
          
            
            EntityStakeholder__c ES1 = Utils_TestMethods.createEntityStakeholder(breEntity.Id,userList[0].Id,label.CLSEP12I2P14);
                ES1.TECH_UniqueField__c = Count+'Trial1';
            EntityStakeholder__c ES2 = Utils_TestMethods.createEntityStakeholder(breEntity.Id,userList[1].Id,label.CLSEP12I2P16);
                ES2.TECH_UniqueField__c = Count+'Trial2';
            EntityStakeholder__c ES3 = Utils_TestMethods.createEntityStakeholder(breEntity.Id,SampleUser0.Id,label.CLSEP12I2P15);
                ES3.TECH_UniqueField__c = Count+'Trial3';
            EntityStakeholder__c ES4 = Utils_TestMethods.createEntityStakeholder(breEntity.Id,SampleUser0.Id,label.CLSEP12I2P18);
                ES4.TECH_UniqueField__c = Count+'Trial4';   
            EntityStakeholder__c ES5 = Utils_TestMethods.createEntityStakeholder(breEntity.Id,SampleUser0.Id,label.CLSEP12I2P17);
                ES5.TECH_UniqueField__c = Count+'Trial5';       
            eshList.add(ES1);
            eshList.add(ES2);
            eshList.add(ES3);
            eshList.add(ES4);
            eshList.add(ES5);
            if(eshList  != null && eshList.size()>0){
                try{
                    Database.insert(eshList); 
                }
                catch(Exception ex){
                    System.debug('HKDebug-->'+ex);
                }
            }
            
      //  }
        for(EntityStakeholder__c  esh:eshList){
            System.debug('HKDebug-->>'+esh.Role__c);
            
        }
        System.debug('-------StakeHolderMohit-------->'+eshList);
        //problem creation
          Problem__c prob=Utils_TestMethods.createProblem(breEntity.Id,12);
        Database.insert(prob);
        // Creating Accounts
        List<Account> accList = new List<Account>();
        
        Account accGSA = Utils_TestMethods.createAccount();
        //accGSA.Type = 'GSA';
        accGSA.Account_VIPFlag__c = 'Gold';
        accGSA.ZipCode__c='201301';
        accList.add(accGSA);
       
        System.debug('------------>MSP'+accList);
        Database.insert(accList);
                string prefix='aa';
        user  sampleUser=Utils_TestMethods.createStandardUser('sample'+prefix);
        insert sampleUser;
        AccountTeamMember accTMOne=Utils_TestMethods.createAccTeamMember(accList[0].id,sampleUser.id);

    
        Database.insert(accTMOne);
        Contact objContact = Utils_TestMethods.createContact(accList[0].id, 'TestCCCContact');
        insert objcontact;
        
       Case caseRec =  Utils_TestMethods.createCase(accList[0].id,objContact.id,'Open');
        Database.insert(caseRec ); 
        
        Case caseRec1 =  Utils_TestMethods.createCase(accList[0].id,objContact.id,'Open');
        Database.insert(caseRec1 );
        // Creating CustomerSafetyIssue
        List<CustomerSafetyIssue__c> csiList = new List<CustomerSafetyIssue__c>();
        CustomerSafetyIssue__c  csiHDRwithNonGSA=new CustomerSafetyIssue__c();
        csiHDRwithNonGSA.AffectedCustomer__c = accList[0].id;
        csiHDRwithNonGSA.CountryOfOccurrence__c = country.id;        
        csiHDRwithNonGSA.IssueInvestigationLeader__c = UserInfo.getUserId();
        csiHDRwithNonGSA.RelatedbFOCase__c = caseRec.id;        
        csiHDRwithNonGSA.RelatedOrganization__c = breEntity.id;  
        csiHDRwithNonGSA.BodilyInjury__c = 'Occurrence Reported';
        csiHDRwithNonGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiHDRwithNonGSA.TypeOfInjury__c = 'Death by electrocution';
        csiHDRwithNonGSA.InjuredPartyAffiliation__c = 'Customer';
        csiHDRwithNonGSA.CauseOfInjury__c = 'Electric shock';
        csiHDRwithNonGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiHDRwithNonGSA.CauseOfPropertyDamage__c = 'Explosion';
        csiHDRwithNonGSA.HumanDeathReported__c = True;   
        csiHDRwithNonGSA.CSIStatus__c = 'Confirmed as safety risk';
        //csiList.add(csiHDRwithNonGSA);     
        //Database.insert(csiHDRwithNonGSA); 
        
        CustomerSafetyIssue__c  csiHDRwithNonGSA1=new CustomerSafetyIssue__c();
        csiHDRwithNonGSA1.AffectedCustomer__c = accList[0].id;
        csiHDRwithNonGSA1.CountryOfOccurrence__c = country.id;        
        csiHDRwithNonGSA1.IssueInvestigationLeader__c = UserInfo.getUserId();
        csiHDRwithNonGSA1.RelatedbFOCase__c = caseRec.id;        
        csiHDRwithNonGSA1.RelatedOrganization__c = breEntity.id;  
        csiHDRwithNonGSA1.BodilyInjury__c = 'Occurrence Reported';
        csiHDRwithNonGSA1.PropertyDamage__c= 'Occurrence Reported';      
        csiHDRwithNonGSA1.TypeOfInjury__c = 'Death by electrocution';
        csiHDRwithNonGSA1.InjuredPartyAffiliation__c = 'Customer';
        csiHDRwithNonGSA1.CauseOfInjury__c = 'Electric shock';
        csiHDRwithNonGSA1.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiHDRwithNonGSA1.CauseOfPropertyDamage__c = 'Explosion';
        csiHDRwithNonGSA1.HumanDeathReported__c = True;  
        //csiList.add(csiHDRwithNonGSA1);     
        //Database.insert(csiHDRwithNonGSA1); 
        Utils_SDF_Methodology.removeFromRunOnce('AP_CustomerSafetyIssue_After');
        Utils_SDF_Methodology.removeFromRunOnce('AP_TechnicalExpertAssessment');
         csiHDRwithNonGSA1.RelatedbFOCase__c = caseRec1.id; 
        List<CustomerSafetyIssue__c> lstnewcsi = new List<CustomerSafetyIssue__c>();
        Map<Id,CustomerSafetyIssue__c> mapOldCSI = new Map<Id,CustomerSafetyIssue__c>();
        String strscenario = 'Update';
        mapOldCSI.put(csiHDRwithNonGSA1.Id,csiHDRwithNonGSA1);
        csiHDRwithNonGSA1.CSIStatus__c = 'Closed (no safety risk)';
        //update csiHDRwithNonGSA1;
        lstnewcsi.add(csiHDRwithNonGSA1);
        Test.startTest();
        AP_CustomerSafetyIssue.ScenarioCheck(lstnewcsi , mapOldCSI, strscenario  );
        Test.stopTest();
       System.debug('----------CSILIst'+csiList);
      // Database.insert(csiList);
        
        /*csiList[2].HumanDeathReported__c = True; 
        csiList[3].AffectedCustomer__c = accList[0].id;        
        update csiList[2];
        update csiList[3];
        
        csiList[3].CSIStatus__c = 'Closed (no safety risk)';
       
        update csiList[3];*/
        //csiList[3].CSIStatus__c = 'Closed (no safety risk)';
        //Utils_SDF_Methodology.removeFromRunOnce('AP_CustomerSafetyIssue_After');
        //Utils_SDF_Methodology.removeFromRunOnce('AP_TechnicalExpertAssessment');
        Database.insert(csiHDRwithNonGSA); 
        }
        
    }
    public static List<CS_CSI__c> createCustomSetting(){
        List<CS_CSI__c> ObjList = new List<CS_CSI__c>();
        return ObjList;
    }
    
    public static testMethod void CustomerSafetyIssue4() {
        
        User CSQUser1 = Utils_TestMethods.createStandardUser('CSQUser1'); 
        insert CSQUser1;
        
        Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );

        Account AccP = Utils_TestMethods.createAccount(); 
        AccP.OwnerId = CSQUser1.Id; 
        insert AccP; 
        Account Acc = Utils_TestMethods.createAccount(); 
        Acc.UltiParentAcc__c = AccP.Id;
        Acc.OwnerId = CSQUser1.Id; 
        insert Acc; 
            
        Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'TEST');              
        insert Ctct;
        
        Case Cse = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
        Cse.Priority = 'Normal';
        insert Cse;
        Case Cse1 = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
        Cse1.Priority = 'Normal';
        insert Cse1;
        
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBRE(Label.CL00319,Null,Null,Null);  
        BusinessRiskEscalationEntity__c breEntitySubEntity = Utils_TestMethods.createBRE(Label.CL00319,'SubEntity',Null,Null);   
        insert breEntity;
        insert breEntitySubEntity;
        
        CustomerSafetyIssue__c  csiHDRwithGSA=new CustomerSafetyIssue__c();
        csiHDRwithGSA.AffectedCustomer__c = Acc.id;
        csiHDRwithGSA.CountryOfOccurrence__c = country.id;        
        csiHDRwithGSA.IssueInvestigationLeader__c = UserInfo.getUserId();          
        csiHDRwithGSA.RelatedOrganization__c = breEntity.id;  
        csiHDRwithGSA.BodilyInjury__c = 'Occurrence Reported';
        csiHDRwithGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiHDRwithGSA.TypeOfInjury__c = 'Death by electrocution';
        csiHDRwithGSA.InjuredPartyAffiliation__c = 'Customer';
        csiHDRwithGSA.CauseOfInjury__c = 'Electric shock';
        csiHDRwithGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiHDRwithGSA.CauseOfPropertyDamage__c = 'Explosion';
        csiHDRwithGSA.HumanDeathReported__c = True;         
        Database.insert(csiHDRwithGSA);
        
        Set<String> newCaseIds = new Set<String>();
        newCaseIds.add(Cse.Id);
        Set<String> oldCaseIds = new Set<String>();
        oldCaseIds.add(Cse.Id);
        
        Test.startTest();
        AP_CustomerSafetyIssue.updateSeverityHighOnCaseAftrInset(newCaseIds);
        Cse.Priority = 'Normal';
        update Cse;
        newCaseIds.add(Cse.Id);
        oldCaseIds.add(Cse.Id);
        AP_CustomerSafetyIssue.updateSeverityHighOnCase(newCaseIds,oldCaseIds);
        newCaseIds.add(Cse.Id);
        oldCaseIds = new Set<String>();
        oldCaseIds.add(Cse1.Id);
        AP_CustomerSafetyIssue.updateSeverityHighOnCase(newCaseIds,oldCaseIds);
        
        map<id,CustomerSafetyIssue__c> mapentity = new map<id,CustomerSafetyIssue__c>();
        mapentity.put(csiHDRwithGSA.Id,csiHDRwithGSA);
        map<id,List<string>> mpROName = new map<id,List<string>>();
        mpROName.put(breEntity.id,new List<String> {Label.CL00319});
        Map<string,string> mapaccount = new Map<string,string>();
        mapaccount.put(Acc.Id,Acc.Id);
        string strstatus = 'strstatus';
        map<id,CustomerSafetyIssue__c> mapCSIScenario = new map<id,CustomerSafetyIssue__c>();
        mapCSIScenario.put(csiHDRwithGSA.Id,csiHDRwithGSA);
        string strtrigevnt = Label.CLSEP12I2P33;
        
        AP_CustomerSafetyIssue.cSISubEntity(mapentity,mpROName,mapaccount,strstatus,mapCSIScenario,strtrigevnt);
        strtrigevnt = 'strtrigevnt';
        AP_CustomerSafetyIssue.cSISubEntity(mapentity,mpROName,mapaccount,strstatus,mapCSIScenario,strtrigevnt);
        
        Map<id,CustomerSafetyIssue__c> mapCSIKamGam = new Map<id,CustomerSafetyIssue__c>();
        mapCSIKamGam.put(csiHDRwithGSA.Id,csiHDRwithGSA);
        set<id> notificationId = new set<id>();
        notificationId.add(csiHDRwithGSA.Id);
        Map<id,string> mamail = new Map<id,string>();
        mamail.put(CSQUser1.Id,CSQUser1.Id);
        Map<id,id> userstat = new Map<id,id>();
        userstat.put(CSQUser1.Id,CSQUser1.Id);
        set<string> stCSIEmailTemplate = new set<string>();
        EmailTemplate ET = [SELECT Id, Name FROM EmailTemplate WHERE Name =:'CSI_New'];
        System.debug('test ET : ' + ET);
        stCSIEmailTemplate.add(ET.Id);
        Map<id,set<string>> MapCSIIDEmailTemplate1 = new Map<id,set<string>>();
        MapCSIIDEmailTemplate1.put(csiHDRwithGSA.Id,stCSIEmailTemplate);
        Map<string,set<string>> mpEmailTempRoles1 = new Map<string,set<string>>();
        set<string> roles = new set<string>();
        roles.add(Label.CLSEP12I2P21);
        roles.add(label.CLSEP12I2P86);
        mpEmailTempRoles1.put(ET.Id,roles);
        Map<string,string> mpUserRole1 = new Map<string,string>();
        mpUserRole1.put(CSQUser1.Id,CSQUser1.Id);
        Map<id,set<id>> mapCSISuentityEntity1 = new Map<id,set<id>>();
        mapCSISuentityEntity1.put(csiHDRwithGSA.Id,notificationId);
        Map<Id,string> mapuserIDRole1 = new Map<Id,string>();
        mapuserIDRole1.put(CSQUser1.Id,'Account Managers / KAM / GAM');
        Map<string,list<AccountTeamMember>> mapaccountTeammember = new Map<string,list<AccountTeamMember>>();
        AccountTeamMember atm = Utils_TestMethods.createAccTeamMember(Acc.Id,CSQUser1.Id);
        list<AccountTeamMember> atmlst = new list<AccountTeamMember>();
        atmlst.add(atm);
        mapaccountTeammember.put(Acc.Id,atmlst);
        map<id,set<id>>  maprelatedOrganizationUSER1 = new map<id,set<id>>();
        set<Id> users = new set<Id>();
        users.add(CSQUser1.Id);
        maprelatedOrganizationUSER1.put(csiHDRwithGSA.Id,users);
        map<string,string> mapCSIAccount1 = new map<string,string>();
        mapCSIAccount1.put(csiHDRwithGSA.Id,Acc.Id);
        AP_CustomerSafetyIssue.SendNotification(mapCSIKamGam,notificationId,mamail,userstat,stCSIEmailTemplate,MapCSIIDEmailTemplate1,mpEmailTempRoles1,mpUserRole1,maprelatedOrganizationUSER1,mapCSISuentityEntity1,mapuserIDRole1,mapaccountTeammember,mapCSIAccount1);
        Test.stopTest();
    }
    
}