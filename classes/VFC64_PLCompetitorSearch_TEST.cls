@isTest
private class VFC64_PLCompetitorSearch_TEST{

    static testMethod void VFC64_PLCompetitorSearchTest() {
         //Start of Test Data preparation
         
    Profile profile = [select id from profile where name='System Administrator' LIMIT 1];
    User user = Utils_TestMethods.createStandardUser(profile.id, 't2est');
    
    Profile profile1 = [select id from profile where name like '%sales%' LIMIT 1];    
    User user2 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse2');
    database.insert(user2);
    
    Country__c country1 = Utils_TestMethods.createCountry();
    insert country1;
    
    Competitor__c comp = Utils_TestMethods.createCompetitor();
    insert comp;
    
    CompetitorCountry__c compCountry= Utils_TestMethods.createCompetitorCountry(comp.id, country1.id);
    insert compCountry;
    
    Account account1 = Utils_TestMethods.createAccount();
    insert account1;            
    
    PAMandCompetition__c pam= Utils_TestMethods.createPAMandCompetition(account1.id,'EN','ENIBS','ENSSO','');  
    insert pam;
    
    ProductLineCompetitor__c plc = Utils_TestMethods.createProductLineComp(pam.id,comp.id);
    insert plc; 
        
    List<String> keywords = New List<String>();
    keywords.add('test');
    List<String> filters = New List<String>();
    filters.add('BD');
    filters.add(country1.id);
    
        
    Utils_DataSource dataSource = Utils_Factory.getDataSource('COMP');
    Utils_DataSource.Result searchResult = dataSource.Search(keywords,filters);
    sObject tempsObject = searchResult.recordList[0];
    PageReference pageRef;
    VFC_ControllerBase basecontroller = new VFC_ControllerBase();        
    //End of Test Data preparation.
    ApexPages.StandardController oc1 = new ApexPages.StandardController(new ProductLineCompetitor__c());        
    VFC64_PLCompetitorSearch compSearch = new VFC64_PLCompetitorSearch(oc1);
    pageRef= Page.VFP64_PLCompetitorSearch; 
    pageRef.getParameters().put(CS007_StaticValues__c.getvalues('CS007_12').Values__c, pam.id);
    pageRef.getParameters().put(CS007_StaticValues__c.getvalues('CS007_11').Values__c, pam.Name);
    pageRef.getParameters().put(CS007_StaticValues__c.getvalues('CS007_14').Values__c,comp.id); 
    pageRef.getParameters().put(CS007_StaticValues__c.getvalues('CS007_13').Values__c,comp.Name);
    System.debug('~~~~~~~~~~Start of Test~~~~~~~~~~');
    system.runAs(user)
    {                
        VCC06_DisplaySearchResults componentcontroller = compSearch.resultsController; 
        compSearch.searchText='test';
        compSearch.level1 = 'BD';
        compSearch.level2 = country1.id;
        Test.setCurrentPageReference(pageRef);       
        compSearch.PerformAction(tempsObject, compSearch);
       // system.debug('********search****'+)
        compSearch.search();
        compSearch.clear();
        
        compSearch.searchText='';
        compSearch.level1 = Label.CL00355;
        compSearch.level2 = Label.CL00355;
        compSearch.search();

        for(Integer index=0; index<1000; index++)
        {
            compSearch.searchText=compSearch.searchText+'1234567890';
        }
        compSearch.search();            
        compSearch.PerformAction(tempsObject, compSearch);                       
    }
             
    system.runAs(user2)
    {    
        compSearch.searchText='test';
        compSearch.level1 = 'BD';
        compSearch.level2 = country1.id; 
        compSearch.search();  
        compSearch.clear();
        compSearch.PerformAction(tempsObject, compSearch);          
    }
   
}
}