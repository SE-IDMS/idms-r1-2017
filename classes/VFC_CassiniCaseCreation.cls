public class VFC_CassiniCaseCreation {
    
    @RemoteAction  
    public static List<ContractProductInformation__c> getProducts(String searchProduct){
        system.debug('searchProduct:'+searchProduct); 
        String strSearchKeyword='%'+searchProduct+'%';
        List<ContractProductInformation__c> lstCPI;
        List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){ List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){ List<Contract> lstContracts=[SELECT Id,Points__c,EndDate FROM Contract Where Id=:lstCVCPs[0].Contract__c and WebAccess__c =: 'Cassini' ORDER BY LastModifiedDate DESC LIMIT 1];
                if(!lstContracts.isEmpty()){ List<Package__c> lstPackage=[SELECT PackageTemplate__c FROM Package__c WHERE Contract__c = :lstContracts[0].Id AND PackageTemplate__r.PortalAccess__c = TRUE ORDER BY LastModifiedDate DESC LIMIT 1];
                    
                    if(!lstPackage.isEmpty()){ lstCPI=[SELECT Family__c,Id,ProductBU__c,ProductFamily__c,ProductLine__c FROM ContractProductInformation__c WHERE PackageTemplate__c = :lstPackage[0].PackageTemplate__c AND (Family__c LIKE :strSearchKeyword OR ProductFamily__c LIKE :strSearchKeyword OR ProductLine__c LIKE :strSearchKeyword OR ProductBU__c LIKE :strSearchKeyword)]; }
                }
            }
        }
        return lstCPI; 
    }
    
    @RemoteAction
    public static String linkedProductToCase(String prodId,String custReq){
        String strCaseAssingment;
       List<Package__c> lstPackage = new List<Package__c>();
        List <CTR_ValueChainPlayers__c> lstCVCPs=new List <CTR_ValueChainPlayers__c>();
        Case c = new Case();
        List<ContractProductInformation__c> lstContractProductInformation = [select id,
                                                  CommercialReference_lk__c, 
                                                  Family_lk__c,ProductFamily__c from  ContractProductInformation__c 
                                                  where Id =: prodId limit 1] ;
        List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId[0].ContactId!=null) lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
                                                                               
        lstPackage=[SELECT PackageTemplate__c,PackageTemplate__r.PortalAccess__c,
                                PackageTemplate__r.PointOfContact__r.Name,
                                PackageTemplate__r.PointOfContact__r.Priority1Routing__c,
                                PackageTemplate__r.PointOfContact__r.DefaultRoutingTeam__c,
                                PackageTemplate__r.PointOfContact__r.Organization__c   
                                FROM Package__c WHERE Contract__c = :lstCVCPs[0].Contract__c
                                AND PackageTemplate__r.PortalAccess__c = TRUE
                                
                                AND PackageTemplate__r.PointOfContact__r.ChannelType__c ='Portal' 
                                ORDER BY LastModifiedDate DESC LIMIT 1];
        if(!lstContractProductInformation.isEmpty()) strCaseAssingment = AP_CassiniPOCCaseAssingment.assingCase(lstContractProductInformation[0].ProductFamily__c);
        if(lstContactId[0].ContactId!=null) c.ContactId =lstContactId[0].ContactId;
            c.CustomerRequest__c =custReq;
            c.origin=System.Label.CLJUN16CSN101;
            if(lstPackage.size()>0){
                c.PortalId__c=lstPackage[0].PackageTemplate__r.PointOfContact__r.Name;
            }
            c.OwnerID=Label.CLOCT13CCC16;
            if(strCaseAssingment!=null) c.Team__c = strCaseAssingment;
            if(!lstCVCPs.isEmpty()) c.CTRValueChainPlayer__c=lstCVCPs[0].Id;
            if(!lstContractProductInformation.isEmpty()) c.Family_lk__c=lstContractProductInformation[0].Family_lk__c;
            Database.DMLOptions autoResponseOptions = new Database.DMLOptions(); 
            autoResponseOptions.EmailHeader.triggerAutoResponseEmail = true;
             database.insert(c,autoResponseOptions);
        
            return c.Id;
    }
}