@isTest
public class TestZuoraSoapHandler {
    static testMethod void test(){

        String parseString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
                                  '<soapenv:Body>'+
                                        '<ns1:updateResponse xmlns:ns1="http://api.zuora.com/">'+
                                              '<ns1:result>'+
                                                    '<ns1:Id>2c92c0f84ac82625014ae3e7d30c16a5</ns1:Id>'+
                                                    '<ns1:Success>true</ns1:Success>'+
                                              '</ns1:result>'+
                                        '</ns1:updateResponse>'+
                                  '</soapenv:Body>'+
                            '</soapenv:Envelope>';

        String sessionId = 'LiUBQFugxg2jJuCA==';

        String serviceActivationDate = '2011-01-01T03:18:09-08:00';

        Country__c cou = testObjectCreator.CreateCountry('Cou_1');

        Account acc = testObjectCreator.CreateAccount('Acc_1',cou.Id);
        
        Contact con = testObjectCreator.CreateContact('Con_1',acc.Id);

        Zuora__CustomerAccount__c ba = testObjectCreator.CreateBillingAccount('BillAcc_1', 'Con_1', acc.Id);

        zqu__Quote__c zq = testObjectCreator.createZQuote(acc.Id);

        Zuora_Request_Soap_Handler.loginToZuora();

        Zuora_Request_Soap_Handler.parse(parseString);

        Zuora_Request_Soap_Handler.updateZuoraAccount(sessionId, zq.zqu__ZuoraAccountID__c, ba.Zuora__BillToId__c, 'BillToId');

        Zuora_Request_Soap_Handler.createZuoraContact(sessionId, con, zq.zqu__ZuoraAccountID__c);

        Zuora_Request_Soap_Handler.updateZuoraSubscription(sessionId, zq.zqu__ZuoraSubscriptionID__c,'',serviceActivationDate);

        Zuora_Request_Soap_Handler.updateZuoraContact(sessionId, con, ba.Zuora__BillToId__c);

        Zuora_Request_Soap_Handler.deleteZuoraContact(sessionId, ba.Zuora__BillToId__c);

        
    }
}