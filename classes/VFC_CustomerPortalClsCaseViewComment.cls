public without sharing class VFC_CustomerPortalClsCaseViewComment {
    Id CaseId;
    Id CaseCommentId;
    Case caseDetail;
    List<CaseComment> caseComments = null;
    CaseComment caseCommentDetail;
    CaseComment cd;
    private String commentToAdd = null;
  
  
    public void setcommentToAdd(String s){
        this.commentToAdd = s;
    }

    public String getcommentToAdd(){
        return this.commentToAdd;
    }
    
    public Id getCaseId(){
        return this.CaseId;
    }
    
    public void setCaseId(Id idd){
        this.CaseId = idd;
    }
    
    public boolean getIsAuthenticated ()
{
  return (IsAuthenticated());
}

public boolean IsAuthenticated ()
{
  return (! UserInfo.getUserType().equalsIgnoreCase('Guest'));
}

public PageReference saveComment()
{
    if (isAuthenticated())
    {
    caseComment caseco = null;
    if(!(commentToAdd.equalsIgnoreCase(''))){
        caseco = new CaseComment(CommentBody=commentToAdd.substring(0,Math.min(commentToAdd.length(),4000)), ParentId=CaseId, isPublished=TRUE);
        
        // Specify DML options to ensure the assignment rules are executed
            
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.useDefaultRule  = true;
            dmlOpts.EmailHeader.triggerUserEmail         = true;
            //dmlOpts.EmailHeader.triggerAutoResponseEmail = true;
            dmlOpts.EmailHeader.triggerOtherEmail        = true;
            
            caseco.setOptions(dmlOpts); 
        
        insert(caseco);        
        
        pagereference pageref = new PageReference('/apex/VFP_CustomerPortalClsCaseDetails?Id='+CaseId);
        pageref.setRedirect(true);
        return pageref;
    }
    else {
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'To save a comment, the field should not be empty. Please enter a comment' );
        ApexPages.addMessage(msg);
        return null;}
    }
    else
    {
        return null;
    }
    }
    
  
public VFC_CustomerPortalClsCaseViewComment(){
    CaseId = ApexPages.currentPage().getParameters().get('Id');
     
    if (CaseId != null){
        caseDetail = [SELECT CaseNumber, Subject, Status, SupportCategory__c, CustomerRequest__c, IsClosed, ActionNeededFrom__c FROM Case WHERE  Id =: CaseId];   
        caseComments = [SELECT Id, CreatedById, CreatedDate, CommentBody, IsPublished, LastModifiedDate FROM CaseComment WHERE ParentId =: CaseId ORDER BY LastModifiedDate DESC];
        }
       }   

// Return the parameter to the page
    public Case getCaseDetail() 
    {    
        if (caseDetail != null) 
        {
          if      (caseDetail.status.equalsIgnoreCase('New'))       {caseDetail.status = 'New';}
          else if (caseDetail.status.equalsIgnoreCase('Cancelled')) {caseDetail.status = 'Closed';}
          else if (caseDetail.status.equalsIgnoreCase('Closed'))    {caseDetail.status = 'Closed';}
          else if (caseDetail.ActionNeededFrom__c != null && caseDetail.ActionNeededFrom__c.equalsIgnoreCase('Customer')) {caseDetail.status = 'Waiting for Customer';} 
          else caseDetail.status = 'In Progress';    
        }
        
        return caseDetail;   
    }

//=======Return the comments List. ===================
    public List<CaseComment> getCaseComments() {    
        return caseComments;   
    }        
    
                
    public pageReference getCaseDetailView()
    {
      if (isAuthenticated())
      {
        getCaseDetail(); 
      }
      
      return null;    
    }    
}