public class AP_EventRecordUpdate
{
    /*
        >> BR-BR-2240. MAY 2013 Sales Release <<
        - This function will Auto-copy the Account 'City' (from Physical adress) to Event 'Location' Field.
        - Copy the value only if Event Record Type is 'Customer Visit' AND Related To 'Account' AND Event 'Location' is null.
    */
    public static void updateEventLocation(List<Event> lstEvts)
    {
        List<Event> lstEvtToUpdate=new List<Event>();
        Map<ID,List<Event>> mapEvents=new Map<ID,List<Event>>();
        List<ID> lstAccIds=new List<ID>();
        for(Event evt : lstEvts)
        {            
           if(evt.RecordTypeId == Label.CLMAY13SLS01 &&  evt.WhatId != null && evt.Location == null)
           {
               if(mapEvents.containsKey(evt.WhatId))
               {                    
                    mapEvents.get(evt.WhatId).add(evt);                 
               }
               else
               {
                    mapEvents.put(evt.WhatId, new list<Event>{evt});
                    lstAccIds.add(evt.WhatId);
               }
           }           
        }
        if(!mapEvents.isEmpty())
        {
            String qry= 'select City__c, Id from Account where ID IN :lstAccIds limit '+ Limits.getLimitQueryRows();
            for(Account acc1 : Database.query(qry))
            {
                 if(acc1.City__c != null)
                 {
                    List<Event> lstEvtTemp = mapEvents.get(acc1.Id);
                    for(Event evt2:lstEvtTemp)
                    {   
                        evt2.Location = acc1.City__c;
                        lstEvtToUpdate.add(evt2);
                    }
                 }
            }
        }
        try
        {
            update lstEvtToUpdate;
        }
        catch (Exception e)
        {
             system.debug('Error in update Event Location = '+e.getmessage());
        }
    }
    
    /* Modified for BR-5608 */
    //Populates the Account of the corresponding contact
    public static void populateRelatedTo(List<Event> events)
    {
        List<Id> contactIds = new List<Id>();
        
        for(Event eve: events)
        {    
            contactIds.add(eve.whoId);
        }
        
        Map<Id, Contact> contacts = new Map<Id, Contact>([select AccountId from Contact where Id in:contactIds]);
        
        for(Event eve: events)
        {
            eve.whatId = contacts.get(eve.whoId).AccountId;            
        }
    }
    /* End of Modification for BR-5608 */
}