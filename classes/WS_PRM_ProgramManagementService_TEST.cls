/*
    Author          : Shruti Karn  / Srinivas Nallapati  
    Date Created    : 14/11/2013
    Description     : Test class for WS_PRM_ProgramManagementService
*/
@isTest (SeeAllData=true)
// Applying SeeAllData=true as this is a readonly web service
private class WS_PRM_ProgramManagementService_TEST {
     static testMethod void testmethod1() {
         
         WS_PRM_ProgramManagementService ws = new WS_PRM_ProgramManagementService();
         
         //Contact Program
         ContactProgramContext cpc = new ContactProgramContext();
         //cpc.StartRow = 0;
         cpc.batchSize = 100;
         cpc.CreatedDate = system.now() - 500;
         cpc.LastModifiedDate1  = system.now() - 500;
         cpc.LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getContactPrograms(cpc);
         
         //Addeded for APR14 Release
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalPRogram.recordtypeid = Label.CLMAY13PRM15;
        globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
        insert globalProgram;
    
        PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, null);
        insert countryProgram; 
        
        list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
        countryPartnerProgram = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
                
        
        if(!countryPartnerProgram.isEmpty())
        {
            countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
            update countryPartnerProgram;
          //  System.debug('*** Country ID :'+ countryPartnerProgram[0].country__c);
            
        }
         //End
        ProgramContext pc = new ProgramContext();
         //pc .StartRow = 0;
         pc .batchSize = 100;
         pc .CreatedDate = system.now() - 500;
         pc .LastModifiedDate1  = system.now() - 500;
         pc .LastModifiedDate2 = system.now() ;
         
        PartnerPRogram__c globalProgram1 = Utils_TestMethods.createPartnerProgram();
        globalPRogram1.recordtypeid = Label.CLMAY13PRM15;
        globalProgram1.ProgramStatus__C = Label.CLMAY13PRM47;
        insert globalProgram1;
        System.debug('*** Global Program:' + globalProgram1);

        PartnerProgram__c countryProgram1 = Utils_TestMethods.createCountryPartnerProgram(globalProgram1.Id, null);
        insert countryProgram1; 
        System.debug('*** Country Program:' + countryProgram1);

        list<PartnerProgram__c> countryPartnerProgram1 = new list<PartnerProgram__c>();
        countryPartnerProgram1 = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
                
        if(!countryPartnerProgram1.isEmpty()) {
            countryPartnerProgram1[0].ProgramStatus__C = Label.CLMAY13PRM47;
            update countryPartnerProgram1;
            System.debug('*** Country Program Update:' + countryProgram1);
            
        }
       
                 
         pc.progTypes = new ProgramType[] { ProgramType.CLUSTERPROGRAM, ProgramType.COUNTRYPROGRAM};
         pc.partnerProgramIDs = new String[]{countryPartnerProgram[0].id,countryPartnerProgram1[0].id}; //APR14Release
         
         List<PartnerProgram__c> lstPrgs = [SELECT Id, Name, Description__c, LastModifiedDate,ProgramStatus__c
                                 FROM PartnerProgram__c 
                                 WHERE Id IN :pc.partnerProgramIDs AND 
                                 RecordTypeId = :countryPartnerProgram[0].recordtypeid AND 
                                 LastModifiedDate > :pc.LastModifiedDate1 AND LastModifiedDate < :pc.LastModifiedDate2  AND 
                                 CreatedDate > :pc.CreatedDate
                                 ORDER BY LastModifiedDate //LIMIT 10000];
                                 LIMIT 100 ];                
         
         
         System.debug('****** partner program size '+lstPrgs.size());
         
         
         // Get defined Programs
         WS_PRM_ProgramManagementService.getPrograms(pc);
        
         // Get All Programs
         pc.partnerProgramIDs = null;
         WS_PRM_ProgramManagementService.getPrograms(pc);

         ProgramLevelContext plc = new ProgramLevelContext();
         //plc .StartRow = 0;
         plc .batchSize = 100;
         plc .CreatedDate = system.now() - 500;
         plc .LastModifiedDate1  = system.now() - 500;
         plc .LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getProgramLevels(plc);
         
         FeatureCatalogContext fcc = new FeatureCatalogContext();
         //fcc .StartRow = 0;
         fcc .batchSize = 100;
         fcc .CreatedDate = system.now() - 500;
         fcc .LastModifiedDate1  = system.now() - 500;
         fcc .LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getFeatures(fcc);
        /* 
         ContactProgramContext cpc = new ContactProgramContext();
         cpc .StartRow = 0;
         cpc .batchSize = 100;
         cpc .CreatedDate = system.now() - 500;
         cpc .LastModifiedDate1  = system.now() - 500;
         cpc .LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getContactPrograms(cpc ); */
         
         /*
         ContactFeatureContext cfc = new ContactFeatureContext();
         //cfc .StartRow = 0;
         cfc .batchSize = 100;
         cfc .CreatedDate = system.now() - 500;
         cfc .LastModifiedDate1  = system.now() - 500;
         cfc .LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getContactFeatures(cfc );
         
         AssessmentContext acon = new AssessmentContext();
         //acon.StartRow = 0;
         acon.batchSize = 100;
         acon.CreatedDate = system.now() - 500;
         acon.LastModifiedDate1  = system.now() - 500;
         acon.LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getAssessments(acon);
         
         AssessmentQuestionContext aqc = new AssessmentQuestionContext();
         //aqc.StartRow = 0;
         aqc.batchSize = 100;
         aqc.CreatedDate = system.now() - 500;
         aqc.LastModifiedDate1  = system.now() - 500;
         aqc.LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getAssessmentQuestions(aqc);
         
         AssessmentQuestionAnswerContext aqac = new AssessmentQuestionAnswerContext();
         AssessmentQuestionAnswerContext aqac21 = new AssessmentQuestionAnswerContext( new List<string>());
         AssessmentQuestionAnswerContext aqac31 = new AssessmentQuestionAnswerContext( new List<string>(), new List<string>());
         //aqac.StartRow = 0;
         aqac.batchSize = 100;
         aqac.CreatedDate = system.now() - 500;
         aqac.LastModifiedDate1  = system.now() - 500;
         aqac.LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getAssessmentQuestionAnswers(aqac);
         
         AccountResponseContext arc = new AccountResponseContext();
         arc.StartRow = 0;
         arc.batchSize = 100;
         arc.CreatedDate = system.now() - 500;
         arc.LastModifiedDate1  = system.now() - 500;
         arc.LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getAccountResponses(arc);
         
        Assessment a1 = new Assessment();
        AssessmentContext ac1 = new AssessmentContext();
        AssessmentQuestion aq1 = new AssessmentQuestion();
        AssessmentQuestionAnswer aqc1 = new AssessmentQuestionAnswer();
        AssessmentQuestionAnswerContext aqac1 = new AssessmentQuestionAnswerContext();
        AssessmentQuestionAnswerResult  aqac2= new AssessmentQuestionAnswerResult();
        AssessmentQuestionContext  aqsd= new AssessmentQuestionContext();
        AssessmentQuestionResult  ars= new AssessmentQuestionResult();
        AssessmentResult ardd = new AssessmentResult();
        AccountResponse  arew= new AccountResponse();
        AccountResponseContext ar123 = new AccountResponseContext();
        AccountResponseResult ar23 = new AccountResponseResult();
//Ap_PartnerProgram  ap12= new Ap_PartnerProgram();

        */

     }

     static testMethod void testmethod2() {
         
         WS_PRM_ProgramManagementService ws = new WS_PRM_ProgramManagementService();
         
         //Contact Program
         ContactProgramContext cpc = new ContactProgramContext();
         //cpc.StartRow = 0;
         cpc.batchSize = 100;
         cpc.CreatedDate = system.now() - 500;
         cpc.LastModifiedDate1  = system.now() - 500;
         cpc.LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getContactPrograms(cpc);
         
         //Addeded for APR14 Release
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalPRogram.recordtypeid = Label.CLMAY13PRM15;
        globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
        insert globalProgram;
    
        PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, null);
        insert countryProgram; 
        
        list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
        countryPartnerProgram = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
                
        
        if(!countryPartnerProgram.isEmpty())
        {
            countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
            update countryPartnerProgram;
          //  System.debug('*** Country ID :'+ countryPartnerProgram[0].country__c);
            
        }
         //End
        ProgramContext pc = new ProgramContext();
         //pc .StartRow = 0;
         pc .batchSize = 100;
         pc .CreatedDate = system.now() - 500;
         pc .LastModifiedDate1  = system.now() - 500;
         pc .LastModifiedDate2 = system.now() ;
         
        PartnerPRogram__c globalProgram1 = Utils_TestMethods.createPartnerProgram();
        globalPRogram1.recordtypeid = Label.CLMAY13PRM15;
        globalProgram1.ProgramStatus__C = Label.CLMAY13PRM47;
        insert globalProgram1;
        System.debug('*** Global Program:' + globalProgram1);

        PartnerProgram__c countryProgram1 = Utils_TestMethods.createCountryPartnerProgram(globalProgram1.Id, null);
        insert countryProgram1; 
        System.debug('*** Country Program:' + countryProgram1);

        list<PartnerProgram__c> countryPartnerProgram1 = new list<PartnerProgram__c>();
        countryPartnerProgram1 = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
                
        if(!countryPartnerProgram1.isEmpty()) {
            countryPartnerProgram1[0].ProgramStatus__C = Label.CLMAY13PRM47;
            update countryPartnerProgram1;
            System.debug('*** Country Program Update:' + countryProgram1);
            
        }
       
                 
         pc.progTypes = new ProgramType[] { ProgramType.CLUSTERPROGRAM, ProgramType.COUNTRYPROGRAM};
         pc.partnerProgramIDs = new String[]{countryPartnerProgram[0].id,countryPartnerProgram1[0].id}; //APR14Release
         
        /* 
         ContactProgramContext cpc = new ContactProgramContext();
         cpc .StartRow = 0;
         cpc .batchSize = 100;
         cpc .CreatedDate = system.now() - 500;
         cpc .LastModifiedDate1  = system.now() - 500;
         cpc .LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getContactPrograms(cpc ); */
         
         ContactFeatureContext cfc = new ContactFeatureContext();
         //cfc .StartRow = 0;
         cfc .batchSize = 100;
         cfc .CreatedDate = system.now() - 500;
         cfc .LastModifiedDate1  = system.now() - 500;
         cfc .LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getContactFeatures(cfc );
         
         AssessmentContext acon = new AssessmentContext();
         //acon.StartRow = 0;
         acon.batchSize = 100;
         acon.CreatedDate = system.now() - 500;
         acon.LastModifiedDate1  = system.now() - 500;
         acon.LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getAssessments(acon);
         
         AssessmentQuestionContext aqc = new AssessmentQuestionContext();
         //aqc.StartRow = 0;
         aqc.batchSize = 100;
         aqc.CreatedDate = system.now() - 500;
         aqc.LastModifiedDate1  = system.now() - 500;
         aqc.LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getAssessmentQuestions(aqc);
         
         AssessmentQuestionAnswerContext aqac = new AssessmentQuestionAnswerContext();
         AssessmentQuestionAnswerContext aqac21 = new AssessmentQuestionAnswerContext( new List<string>());
         AssessmentQuestionAnswerContext aqac31 = new AssessmentQuestionAnswerContext( new List<string>(), new List<string>());
         //aqac.StartRow = 0;
         aqac.batchSize = 100;
         aqac.CreatedDate = system.now() - 500;
         aqac.LastModifiedDate1  = system.now() - 500;
         aqac.LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getAssessmentQuestionAnswers(aqac);
         
         AccountResponseContext arc = new AccountResponseContext();
         arc.StartRow = 0;
         arc.batchSize = 100;
         arc.CreatedDate = system.now() - 100;
         arc.LastModifiedDate1  = system.now() - 100;
         arc.LastModifiedDate2 = system.now() ;
         WS_PRM_ProgramManagementService.getAccountResponses(arc);
         
        Assessment a1 = new Assessment();
        AssessmentContext ac1 = new AssessmentContext();
        AssessmentQuestion aq1 = new AssessmentQuestion();
        AssessmentQuestionAnswer aqc1 = new AssessmentQuestionAnswer();
        AssessmentQuestionAnswerContext aqac1 = new AssessmentQuestionAnswerContext();
        AssessmentQuestionAnswerResult  aqac2= new AssessmentQuestionAnswerResult();
        AssessmentQuestionContext  aqsd= new AssessmentQuestionContext();
        AssessmentQuestionResult  ars= new AssessmentQuestionResult();
        AssessmentResult ardd = new AssessmentResult();
        AccountResponse  arew= new AccountResponse();
        AccountResponseContext ar123 = new AccountResponseContext();
        AccountResponseResult ar23 = new AccountResponseResult();
//Ap_PartnerProgram  ap12= new Ap_PartnerProgram();



     }
     
     //Renjit
     static testMethod void AccountResponseContextMethod(){
        
        //Test.startTest();
        List<String>  assessmentIDs = new List<String>();
        List<String>  accountIDs = new List<String>();
 
        assessmentIDs.add('Test1');
        assessmentIDs.add('Test2');              
        accountIDs.add('Test1');
        accountIDs.add('Test2');              

        AccountResponseContext acntResp1 = new AccountResponseContext(assessmentIDs);
        AccountResponseContext acntResp2 = new AccountResponseContext(assessmentIDs,accountIDs);

    }
    static testMethod void AccountResponseResultTestMethod(){
                      
        AccountResponseResult acntRespRslt = new AccountResponseResult();
    }
    static testMethod void AccountResponseTestMethod(){
        //Test.startTest();
        AccountResponse acntResp = new AccountResponse();
    }
    
    static testMethod void ContactFeatureTestMethod(){
        ContactFeature ContactFeature1 = new ContactFeature();
    }
    
    static testMethod void ContactFeatureContextMethod(){
       
        List<String> FeatureIDs = new List<String>();
        List<String> ContactIDs = new List<String>();
        
        FeatureIDs.Add('Test1');
        FeatureIDs.Add('Test2');
        ContactIDs.Add('Contact1');
        ContactIDs.Add('Contact1');
        
        ContactFeatureContext ContactFeatureContext = new ContactFeatureContext(ContactIDs,FeatureIDs);
    }
    
    static testMethod void ContactFeatureResultTestMethod() {
        ContactFeatureResult ContactFeatureResult1 = new ContactFeatureResult();
    }
    
    //--------------------------------------------------------------
    
    static testMethod void ContactProgramTestMethod() {
     ContactProgram ContactProgram1 = new ContactProgram();
    }
    
    //---------------------------------------------------------------------
    static testMethod void ContactProgramContextTestMethod(){
        List<String> programIDs = new List<String>();
        List<String> ContactIDs = new List<String>();
        
        programIDs.Add('Test1');
        programIDs.Add('Test2');
        ContactIDs.Add('Contact1');
        ContactIDs.Add('Contact1');
        
        ContactProgramContext ContactProgramContext1 = new ContactProgramContext(ContactIDs,programIDs);
    }
    //----------------------------------------------------------------------------
    static testMethod void PartnerProgramTestMethod(){
        PartnerProgram PartnerProgram1 = new PartnerProgram();
    }
    
    //------------------------------------------------------
    static testMethod void PartnerProgramResultTestMethod(){
        PartnerProgramResult  PartnerProgramResult1 = new PartnerProgramResult();
    }
    
    static testMethod void ProgramTypeEnumTestMethod() {
        ProgramType pt = ProgramType.GLOBALPROGRAM;
        ProgramType pt2 = ProgramType.CLUSTERPROGRAM;
        ProgramType pt3 = ProgramType.COUNTRYPROGRAM;
    }
    
    //-------------------------------------------------------------------------------
    
  /*  static testMethod void ProgramContextTestMethod(){
        
       
        ProgramType Type;
        List<String> PrgIDs = new List<String>();
        
        
        
        PrgIDs.Add('Prg1');
        PrgIDs.Add('Prg2');
        
        ProgramContext ProgramContext1 = new ProgramContext(Type,PrgIDs);
        
        
    }*/

    //----------------------------------------------------------------------------
    
    static testMethod void ProgramLevelTestMethod(){
        ProgramLevel  ProgramLevel1 = new ProgramLevel();
    }

    //------------------------------------------------------------------
    static testMethod void ProgramLevelContextTestMethod(){
        List<String> PrgIDs = new List<String>();
        
        PrgIDs.Add('Prg1');
        PrgIDs.Add('Prg2');
        
        ProgramLevelContext ProgramLevelContext1 = new ProgramLevelContext(PrgIDs);
    }
    
    //--------------------------------------------------------------
    static testMethod void ProgramLevelResultTestMethod(){
        ProgramLevelResult  ProgramLevelResult1 = new ProgramLevelResult();
    }
    //--------------------------------------------------------------
    static testMethod void ProgramTypeTestMethod(){
        ProgramType  ProgramType1;
    }
    //--------------------------------------------------------------
}