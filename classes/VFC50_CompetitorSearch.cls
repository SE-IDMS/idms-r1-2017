/*
    Author          : Accenture Team
    Date Created    : 18/05/2011 
    Description     : Controller extensions for VFP50_CompetitorSearch.This class fetches the search 
                      parameters from the component controller, performs the search by calling factory methods  
*/

public with sharing class VFC50_CompetitorSearch extends VFC_ControllerBase{


    //Iniatialize variables
    private OPP_OpportunityCompetitor__c currentComp = new OPP_OpportunityCompetitor__c();
    public Utils_DataSource dataSource;
    public List<SelectOption> columns{get;set;}
    public List<DataTemplate__c> fetchedRecords{get;set;}
    public String interfaceType{get;set;}
    public List<String> keywords;
    public List<String> filters;
    public Boolean DisplayResults{get;set;}
    public Utils_DataSource.Result searchResult ;
    Public Utils_PicklistManager plManager;
    public string searchText{get;set;}       
    public String level1{set; get;}
    public String level2{get; set;}
    Public List<String> Labels{get;set;}
    public String level1Label{set; get{return Labels[0];}}
    public String level2Label{set; get{return Labels[1];}}
    public List<SelectOption> items1{set; get;}
    public List<SelectOption> items2{set; get;}
    List<DataTemplate__c> oppCompetitors = New List<DataTemplate__c>();
    public Boolean flag;
    public List<opportunityTeamMember> opptyTeam = New List<opportunityTeamMember>();
    public Boolean displayError{get;set;}
         
    
    public VCC06_DisplaySearchResults resultsController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                system.debug('--------displaySearchResults -------'+displaySearchResults );                
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }
     //Constructor
    public VFC50_CompetitorSearch(ApexPages.StandardController controller) 
    {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC50_CompetitorSearch******');            
        currentComp= (OPP_OpportunityCompetitor__c)controller.getRecord();
        keywords = new List<String>();
        filters = new List<String>();
        columns = new List<SelectOption>();
        Labels = new List<String>();
        interfaceType = 'COMP';
        flag = false;
        displayError = false;
                
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
        
        dataSource = Utils_Factory.getDataSource(interfaceType);
        columns = new List<SelectOption>();
        columns = dataSource.getColumns();
        plManager = Utils_Factory.getPickListManager(interfaceType);
        
        if( plManager != null)
        {
            Labels = plManager.getPickListLabels();
            items1 = plManager.getPicklistValues(1, null);
            items2 = plManager.getPicklistValues(2, null);
        }
        
        fetchedRecords = new List<DataTemplate__c>();
        System.Debug('****** Initializing the Properties and Variables Ends for VFC50_CompetitorSearch******');
    
    
    }
    
     public PageReference checkAccess()
    {
        for(Profile prof : [Select Id, Name from Profile where Name Like :System.Label.CL00498])                
        {
            if(userinfo.getProfileId() == prof.Id)
            {
                flag=true;                
            }
            else
                flag=False;
        }
        
        String oppId;
        if(system.currentpagereference().getParameters().get(Label.CL00452) != NULL)
        {
            oppId = system.currentpagereference().getParameters().get(Label.CL00452);
        }
        
        if(!flag && oppId != NULL)
        {
            if(userinfo.getuserId() == [Select OwnerId, Id from Opportunity where Id = :oppId LIMIT 1].OwnerId)
            {
                flag = True;
            }            
            else 
            {    
                opptyTeam = [select OpportunityID,userId from opportunityTeamMember where OpportunityID =: oppId and opportunityAccessLevel =:Label.CL00203];
            
                if(!opptyTeam.isEmpty())
                {
                    for(opportunityTeamMember team : opptyTeam)
                    {
                        if(userinfo.getuserId() == team.userId)
                            flag = True;
                    }
                }
            }            
        }
        
        if(!flag && oppId != NULL)
        {    
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00562+'. '+Label.CL00563));
            displayError = true;         
        }
        else
        {
            displayError = false;    
        }
        return NULL;
    }
    
    
    
    public PageReference  search() 
    {     
    /* This method will be called on click of "Search" Button.This method calls the factory methods
     * and fetches the search results
     */
       System.Debug('****** Searching Begins  for VFC50_CompetitorSearch******');
        if(resultsController.searchResults!=null)
            resultsController.searchResults.clear();

        fetchedRecords.clear();
        keywords = new List<String>();
        filters = new List<String>();
        if(searchText!=null)
            keywords.add(searchText.trim());
        if(level1!=null)
            filters.add(level1);
        if(level2!=null)
            filters.add(level2); 
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
        if(keywords.size()!=0 || filters.size()!=0)
        {
            if(!filters[0].equalsIgnoreCase(Label.CL00355) || !filters[1].equalsIgnoreCase(Label.CL00355) || keywords[0].length()>0)
            {
                try
                {
                    System.debug('#### Filters : ' + filters);
                    //Makes the Datasource call for search                    
                    searchResult = dataSource.Search(keywords,filters);
                    fetchedRecords = searchResult.recordList;
                    System.debug('Data source Result'+fetchedRecords);
                    System.debug(' Result'+searchResult);
                    DisplayResults = true;
                    
                    if(searchResult.NumberOfRecord > 100)
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00349));
                }
                catch(Exception exc)
                {
                    system.debug('Exception while calling Competitor Search '+ exc.getMessage() );
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00398));
                }                
            }
            else if(filters[0].equalsIgnoreCase(Label.CL00355) && filters[1].equalsIgnoreCase(Label.CL00355) && keywords[0].length()==0)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00454));

            }             
        }         
        System.Debug('****** Searching Ends  for VFC50_CompetitorSearch******');        
        return null;          
    }
    /* This method will be called on click of "Clear" Button.This method clears the value in the search text box
     * and the values in picklists
     */
    public PageReference clear() 
    {
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Begins  for VFC50_CompetitorSearch******');     
        fetchedRecords = new List<DataTemplate__c>();
        searchText = null;
        keywords = new List<String>();
        filters = new List<String>();        
        DisplayResults = false;
        level1 = null;
        level2 = null;
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Ends for VFC50_CompetitorSearch******');     
        return null;
    }
    /* This method will be called from the component controller on click of "Select" link.This method correspondingly inserts/updates
     * the competitor line.
     */
    public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
    {
        System.Debug('****** Insertion/Updation of Product Line Begins******');
        
        VFC50_CompetitorSearch thisController = (VFC50_CompetitorSearch)controllerBase;
        
        
        PageReference newOppCompPage = new PageReference('/'+SObjectType.OPP_OpportunityCompetitor__c.getKeyPrefix()+'/e');
        
        if(system.currentpagereference().getParameters().get(Label.CL00452)!=null)
        {
            newOppCompPage.getParameters().put(Label.CL00452, system.currentpagereference().getParameters().get(Label.CL00452));
            newOppCompPage.getParameters().put(Label.CL00330, system.currentpagereference().getParameters().get(Label.CL00452));
        }
        
        if(system.currentpagereference().getParameters().get(Label.CL00515)!=null)
            newOppCompPage.getParameters().put(Label.CL00515, system.currentpagereference().getParameters().get(Label.CL00515));
            
        if(system.currentpagereference().getParameters().get('saveURL')!=null)    
            newOppCompPage.getParameters().put('saveURL',system.currentpagereference().getParameters().get('saveURL'));
        
        if(system.currentpagereference().getParameters().get('cancelURL')!=null)    
            newOppCompPage.getParameters().put('cancelURL',system.currentpagereference().getParameters().get('cancelURL')); 
         if(system.currentpagereference().getParameters().get('saveURL')!=null && system.currentpagereference().getParameters().get('cancelURL')!=null)    
            newOppCompPage.getParameters().put('00NA0000009eEkM','1');

        oppCompetitors.add((DataTemplate__c)obj);
        
        for(DataTemplate__c dt : oppCompetitors)
        {
            if(dt.field1__c != null)
                newOppCompPage.getParameters().put(Label.CL00517, dt.field1__c);
        }
        
        newOppCompPage.getParameters().put(Label.CL00518, [Select Id, Name from Competitor__c where Name =:newOppCompPage.getParameters().get(Label.CL00517) Limit 1].Id);
        
        List<OPP_OpportunityCompetitor__c> oppComps = [Select OpportunityName__c, TECH_CompetitorName__c, TECH_CompetitorName__r.Name from OPP_OpportunityCompetitor__c where OpportunityName__c=:system.currentpagereference().getParameters().get(Label.CL00452) and TECH_CompetitorName__c=:newOppCompPage.getParameters().get(Label.CL00518) and TECH_CompetitorName__r.Name != :Label.CL00519];  
        
        newOppCompPage.getParameters().put('nooverride', '1');
        
        if(oppComps.size()>0)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00453));
            newOppCompPage = null;
        }        
        else
            newOppCompPage = newOppCompPage;
        
            
        System.Debug('****** Insertion/Updation of Competitor Ends******');
        return newOppCompPage;
    }
    public pagereference cancel()
    {
        System.Debug('****** Cancel Method Begins  for  VFC50_CompetitorSearch******');
        pagereference pg;
        if(system.currentpagereference().getParameters().get(Label.CL00452)!=null)
            pg = new pagereference('/'+system.currentpagereference().getParameters().get(Label.CL00452));
        system.debug('------Page reference -----'+pg);
        System.Debug('****** Cancel Method Ends for  VFC50_CompetitorSearch******');
        if(pg!=null)
            return pg;
        else
            return null;
    }           

}