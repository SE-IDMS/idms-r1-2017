public class AP_PRM_SSO_UserHandler implements Auth.SamlJitHandler {

    public User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
            System.debug('attributes:' + attributes);
            System.debug('assertion:' + assertion);
            userRegResult result = new userRegResult();
            User user = new User();
            List<UIMSCompany__c> lUIMSCmp = new List<UIMSCompany__c>();
            List<String> lFlds = new List<String>();
            federationIdentifier = federationIdentifier;
            if(System.Label.CLAPR15PRM410 == 'True') {
                System.Debug('*** Start createUser : federationIdentifier=' + federationIdentifier);
                System.Debug('**Assertion**'+assertion);
                Map<String,set<String>> attributeList = AP_PRMUtils.getApplications(EncodingUtil.base64Decode(assertion).toString());
                
                try {
                    lFlds = AP_PRMUtils.RegistrationTrackingFields();
                    AP_UserRegModel userModel = AP_PRMUtils.GetUIMSAccountContactInfo_Admin(federationIdentifier);
                    System.debug('**Response from GetUIMSAccountContactInfo_Admin**'+userModel);    
                    userModel.wechatId = (attributeList.get('wechatId') != null && attributeList.get('wechatId').size() > 0)?(new list<string>(attributeList.get('wechatId'))[0] ):'';
                    userModel.phoneId = (attributeList.get('phoneId')!= null && attributeList.get('phoneId').size() > 0)?(new list<string>(attributeList.get('phoneId'))[0] ):'';
                    userModel.termsAndConditions = false;
                    userModel.securityToken = assertion;
                    userModel.isPrimaryContact = (String.isNotBlank(userModel.UimsCompanyId) && String.isNotBlank(userModel.accountId))?false:true;
                    userModel.prmOriginSource = 'Private Registration';
                    String poolKey = String.isNotBlank(userModel.companyCountryISOCode)?(String.isNotBlank(userModel.businessType) && String.isNotBlank(userModel.areaOfFocus)?(userModel.companyCountryISOCode+userModel.businessType+userModel.areaOfFocus):userModel.companyCountryISOCode):'WW';
                    userModel.bFOPool = String.isNotBlank(poolKey) ? ((poolKey.length() == 2 && poolKey == 'WW') ? AP_PRMAppConstants.ProcessRegistrationPool.NOCOUNTRYNOCHANNELPOOL : ((poolKey.length() == 2 && poolKey != 'WW') ? AP_PRMAppConstants.ProcessRegistrationPool.COUNTRYPOOL : ((poolKey.length() > 2) ? AP_PRMAppConstants.ProcessRegistrationPool.CHANNELPOOL : AP_PRMAppConstants.ProcessRegistrationPool.NOCOUNTRYNOCHANNELPOOL))) : AP_PRMAppConstants.ProcessRegistrationPool.NOCOUNTRYNOCHANNELPOOL;
                    //Method to pick the Account, Contact, User and Member from the pool based on the poolKey generated above.
                    //For Nov 16 release we are passing 'WW' as the poolKey for Private Registration.
                    if(userModel.isPrimaryContact)
                        result = AP_PRMUtils.GetbFOAccountContactInfo(poolKey);
                    if(result != null){
                        userModel.accountId = String.isNotBlank(result.bFOAccountId)?result.bFOAccountId:null;
                        userModel.contactId = String.isNotBlank(result.ContactId)?result.ContactId:null;
                    }

                    UIMSCompany__c uimsCompany = AP_PRMUtils.createRegistrationTrackingHistory(userModel,userModel.uimsCompanyId,result);
                    lUIMSCmp.add(uimsCompany);
                    System.debug('*UIMS*'+result.UserRegistrationTrackingId);
                    System.debug('result:' + result);
                    lUIMSCmp[0].RegistrationType__c = 'Private Registration';
                    
                    PRM_FieldChangeListener.enableForAccount = false;          
                    PRM_FieldChangeListener.enableForContact = false;
                    AP_PRMAccountRegistration.JITProvisioning = true;                    
                    if(String.isBlank(result.bFOAccountId) || !userModel.isPrimaryContact)
                        result = AP_PRMAccountRegistration.CreatePRMRegistrationbFO(userModel,lUIMSCmp,false);
                    else
                    AP_PRMUtils.UpdatePRMRegistrationInbFO(lUIMSCmp[0]);
                    System.debug('result:' + result);
                    
                    if(result.Success)
                        user = [SELECT Id,PRMRegistrationCompleted__c,PRMTemporarySamlToken__c FROM User WHERE Id=:result.userId];
                        
                    System.debug('**Here??**'+userModel.federationId);
                    
                    System.Debug('****************************************** After User=' + user);
                    return user;        
                } catch (Exception e) {
                    if(lUIMSCmp.size()> 0){
                        lUIMSCmp[0].InformationMessage__c = e.getMessage() +e.getStackTraceString();
                        lUIMSCmp[0].RetryRegistration__c = true;
                    }
                    System.debug('*** createUser: An unexpected error has occurred: '+e.getMessage() +e.getStackTraceString());
                }  
                finally {
                    if (lUIMSCmp.size()> 0) UPDATE lUIMSCmp;
                    PRM_FieldChangeListener.enableForAccount = true;          
                    PRM_FieldChangeListener.enableForContact = true;
                }
            }
           return null;
    }

    public void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        System.debug('attributes:' + attributes);
        System.debug('assertion:' + assertion);
        try {
            System.Debug('*** start updateUser : federationIdentifier=' + federationIdentifier); 
            User currentUser = [SELECT id, PRMTemporarySamlToken__c,PRMRegistrationCompleted__c, CreatedById, ContactId, Contact.FieloEE__Member__c, 
                                    Contact.Name, Contact.PRMEmail__c, Contact.PRMCustomerClassificationLevel1__c, Contact.PRMCustomerClassificationLevel2__c,
                                    Contact.PRMCountry__c, Contact.PRMApplicationRequested__c, AccountId, Contact.Account.PRMCountry__c, 
                                    Contact.Account.Name, Contact.Account.PRMCompanyName__c FROM User WHERE id = :userId LIMIT 1 ];
            List<UIMSCompany__c> lUimsCompany = new List<UIMSCompany__c>([SELECT RegistrationType__c 
                                    FROM UIMSCompany__c 
                                    WHERE UserfederatedId__c = :federationIdentifier ORDER BY LastModifiedDate DESC LIMIT 2]);

            System.debug('**Current User**' + currentUser);
            System.debug('**User Reg Tracking returned**' + lUimsCompany);

            String oldAssertion = currentUser.PRMTemporarySamlToken__c;       
            String newAssertion = assertion;
            String authenticationToken = assertion;
            System.debug('*OldAssertion : newAssertion ' + oldAssertion + ':' + newAssertion);

            List<PermissionSetAssignment> lUsrPermissions = new List<PermissionSetAssignment> ([
                        SELECT AssigneeId, PermissionSetId 
                        FROM PermissionSetAssignment 
                        WHERE AssigneeId=:userId AND PermissionSetId=:Label.CLAPR15PRM182]);

            System.debug('**PerMission Set**' + lUsrPermissions);
            System.debug('**PerMission Set**' + lUsrPermissions.size());

            if (lUsrPermissions.size() <= 0) {
                PermissionSetAssignment tPSA = new PermissionSetAssignment(
                                AssigneeId = userId,
                                PermissionSetId = Label.CLAPR15PRM182
                        );
                        lUsrPermissions.add(tPSA);
                
                String sUsrPerms = JSON.serialize(lUsrPermissions);
                System.debug('**Json**' + sUsrPerms);
                AP_PRMAccountRegistration.AssignPermission(sUsrPerms);      
            }

            /*
            List<Contact> lUsrContact = new List<Contact>([SELECT Id,Name,PRMEmail__c,PRMCustomerClassificationLevel1__c,PRMCustomerClassificationLevel2__c,Account.PRMCountry__c,PRMCountry__c,PRMApplicationRequested__c FROM Contact WHERE Id = :currentUser.ContactId]);
            System.debug('**Contact**'+lUsrContact);
            List<IntegrationCalloutEvent__c> integrationCalloutEventLst = [SELECT Id, Contact__c, EventType__c, Status__c FROM IntegrationCalloutEvent__c WHERE Contact__c =:currentUser.ContactId AND EventType__c='emailChange' AND Status__c=False];            
            if(integrationCalloutEventLst != null && integrationCalloutEventLst.size() > 0) {
                uimsv2ImplServiceImsSchneiderComNew2.UserManager_UIMSV2_ImplPort uimsPort = new uimsv2ImplServiceImsSchneiderComNew2.UserManager_UIMSV2_ImplPort();
                uimsPort.timeout_x = Integer.valueOf(System.label.CLAPR15PRM021);
                uimsPort.clientCertName_x = System.Label.CLAPR15PRM018;
                uimsPort.endpoint_x = System.Label.CLAPR15PRM151;//'https://ims2w-int.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/UserManager';
                uimsv2ServiceImsSchneiderComNew2.userV5 uimsUser;
                if(!Test.isRunningTest())
                  uimsUser = uimsPort.getUser(System.label.CLAPR15PRM019, authenticationToken);
               
               system.debug('**Get User**' + uimsUser);
                
                //List<Contact> lUsrContact = new List<Contact>([SELECT Id,Name,PRMEmail__c FROM Contact WHERE Id = :currentUser.ContactId]);
                if (lUsrContact != null && !lUsrContact.isEmpty() && uimsUser != null) {
                    lUsrContact[0].PRMEmail__c = uimsUser.email;
                    UPDATE lUsrContact;
                 }
                if (currentUser != null && uimsUser != null) {
                    currentUser.Email = uimsUser.email;
                    currentUser.Username = uimsUser.email + Label.CLMAR13PRM05; 
                }
                integrationCalloutEventLst[0].Status__c = True;
                UPDATE integrationCalloutEventLst[0];
            }
            */

            if (String.isNotBlank(currentUser.ContactId)) {
                String countryLookup;

                if ((lUimsCompany != Null && lUimsCompany.size() > 0 && lUimsCompany[0].RegistrationType__c =='Private Registration') ||
                        currentUser.Contact.Account.PRMCompanyName__c.containsIgnoreCase(Label.CLJUL15PRM027)) {
                    countryLookup = currentUser.Contact.PRMCountry__c;
                    System.debug('**Contact PRMCountry**' + countryLookup);
                } else {    
                    countryLookup = currentUser.Contact.Account.PRMCountry__c;
                    System.debug('**Account PRMCountry**' + countryLookup);
                }
                
                if (lUimsCompany != Null && lUimsCompany.size() > 0 && String.isBlank(lUimsCompany[0].RegistrationType__c)) {
                    List<CountryChannels__c> lCountryChannels = new List<CountryChannels__c>([SELECT CompleteUserRegistrationonFirstLogin__c 
                                                                        FROM CountryChannels__c 
                                                                        WHERE ChannelCode__c = :currentUser.Contact.PRMCustomerClassificationLevel1__c 
                                                                        AND SubChannelCode__c = :currentUser.Contact.PRMCustomerClassificationLevel2__c 
                                                                        AND Country__c = :countryLookup AND Active__c = True]);
                    System.debug('**Country Channel*'+lCountryChannels);
                    if (lCountryChannels != Null && lCountryChannels.size() > 0 && !lCountryChannels[0].CompleteUserRegistrationonFirstLogin__c && !currentUser.PRMRegistrationCompleted__c) {
                        System.debug('**Channel Login**'+!lCountryChannels[0].CompleteUserRegistrationonFirstLogin__c);
                        currentUser.PRMRegistrationCompleted__c = True;
                    }
                }
            }  

            currentUser.PRMTemporarySamlToken__c = assertion;
            /*String currentUserId = currentUser.createdById;
            if(!currentUserId.contains(System.label.CLJUN16PRM110))
                currentUser.PRMRegistrationCompleted__c = True;*/
            UPDATE currentUser;
            
            //******* START FIELO: Add badges/Remove badges to contact:
            //******* EDIT AKRAM: NO MORE CALL FIELO -> ADD A MEMBER EXTERNAL PROPERTIES
            Map<String,Set<String>> currentApplications = new Map<String,Set<String>>();
            if(String.isNotBlank(oldAssertion))
                currentApplications =  AP_PRMUtils.getApplications(EncodingUtil.base64Decode(oldAssertion).toString());
                
            System.debug('**currentApplications**'+currentApplications);
            Map<String,Set<String>> newApplications =  AP_PRMUtils.getApplications(EncodingUtil.base64Decode(newAssertion).toString());
            System.debug('**newApplications**'+newApplications);
            
            //System.Debug('****************************************** Old Assertion DecodeBase64=' + EncodingUtil.base64Decode(oldAssertion).toString());                
            //System.Debug('****************************************** New Assertion DecodeBase64=' + EncodingUtil.base64Decode(newAssertion).toString());    
            //System.Debug('****************************************** Map attributes=' + attributes );    
            //System.Debug('****************************************** Current Application=' +  currentApplications);    
            //System.Debug('****************************************** New Application=' +  newApplications); 
            
            if (newApplications.get('Applications').size() >0) {
                Map<String,Set<String>> mapBadgeByContact =  new Map<String,Set<String>>();
                Set<String> listAdd = new Set<String>();
                mapBadgeByContact.put(federationIdentifier, listAdd);
            
                for(String na: newApplications.get('Applications')) {
                    system.debug('** na **'+na);
                    if (currentApplications.get('Applications').contains(na)) {
                      currentApplications.get('Applications').remove(na);
                    }
                    else {
                      System.Debug('*** FIELO Add :' + na);
                      listAdd.add(na);
                    }
                }
                PRM_MemberExternalPropertiesUtils.addMemberExternalPropertyByPRMUIMId('UIMS','ApplicationAccess',mapBadgeByContact);
            }  
            
            if (currentApplications.get('Applications').size() >0) {
                Map<String,Set<String>> mapBadgeByContact =  new Map<String,Set<String>>();
                Set<String> listRemove = new Set<String>();
                mapBadgeByContact.put(federationIdentifier, listRemove);
                            
              for(String ca: currentApplications.get('Applications')) {
                System.Debug('*** FIELO Remove :' + ca);
                listRemove.add(ca);               
              }
              PRM_MemberExternalPropertiesUtils.removeMemberExternalPropertyByPRMUIMId('UIMS','ApplicationAccess',mapBadgeByContact);
            }             
            //******* END FIELO: Add badges/Remove badges to contact
                       
            System.Debug('*** end updateUser');
             
        }  catch(Exception e) {
            System.debug('***updateUser : An unexpected error has occurred:'+e.getMessage() +e.getStackTraceString());
        }
    }
}