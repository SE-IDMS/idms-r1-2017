//*********************************************************************************
//Class Name :  AP_ProvideChatterDetails
// Purpose : It's a supporting  class for the batch class Schedule_BusinessRiskEscalationAlert  
// Created by : Shivdeep Gupta - Global Delivery Team
// Date created :  Feb 08, 2015
// Modified by :
// Date Modified :
// Remarks : For April 15 Release  
// ********************************************************************************

public class AP_ProvideChatterDetails{

    // setOfIDs = first get set of BRE IDs( or the IDs of object which is associated to chatter and we want to process) who's last modified is > system.now()-7 and status = 'Open'

    set<ID> setOfFeedItemParentIDs = new set<ID>();
    set<ID> setOfTaskWhatIds = new set<ID>();
    set<ID> setOfEventWhatIds = new set<ID>();
    set<ID> setOfIdsToExclude = new set<ID>();
    set<ID> setPIDs = new set<ID>();
    set<ID> setofIDsLongModified = new set<ID>();
    set<String> BREwithResolutionLeaderOrSponsor = new set<String>();

    List<FeedItem> lstFeedItem = new List<FeedItem>();
    List<Task> lstTask = new List<Task>();
    List<Event> lstEvent = new List<Event>(); 
    List<BusinessRiskEscalationStakeholder__c> lstBRESH = new List<BusinessRiskEscalationStakeholder__c>();
    List<BusinessRiskEscalations__c> lstBRE = new List<BusinessRiskEscalations__c>(); 
    List<Business_Risk_Escalation_Alert__c> lstBREAlert = new List<Business_Risk_Escalation_Alert__c>();
    List<Business_Risk_Escalation_Alert__c> lstBREAtoInsert = new List<Business_Risk_Escalation_Alert__c>();

    /*
    public set<ID> returnObjectSet(set<ID> setPIDs){

        //setOfFeedItemParentIDs = Now get those FeedItems who's parent ID is in set received above(setOfIDs)and lastModifiedDate is < system.now()-7, and now store these parentIDs in set setOfFeedItemParentIDs
        // CLAPR15I2P14 = 7
        lstFeedItem = [SELECT ID,CommentCount,ContentData,CreatedDate,LastModifiedDate,ParentId,RelatedRecordId,SystemModstamp,Type FROM FeedItem where  parentID in : setPIDs and LastModifiedDate >: system.now()-integer.valueof(System.Label.CLAPR15I2P14)];
        system.debug('@Test lstFeedItem : '+lstFeedItem); 
        for(FeedItem oFI : lstFeedItem){
            setOfFeedItemParentIDs.add(oFI.ParentId);
        }
        system.debug('@Test 2 : '+setOfFeedItemParentIDs); 
        
        
        //setOfTaskWhatIds = Now query on task and get those task with WhatID in setOfIDs and lastModifiedDate < system.now()-7. Store those task's parentID in setOfTaskWhatIds
        lstTask = [SELECT Id,LastModifiedDate,WhatId FROM Task where WhatId in : setPIDs and LastModifiedDate >: system.now()-integer.valueof(System.Label.CLAPR15I2P14)];
        for( Task oTask : lstTask){
            setOfTaskWhatIds.add(oTask.WhatId);
        }
        system.debug('@Test 3 : '+setOfTaskWhatIds); 
        //setOfEventWhatIds = Now query on Event and get those Event with parentID in setOfIDs and lastModifiedDate < system.now()-7. Store those task's parentID in setOfEventWhatIds

        lstEvent = [SELECT Id,LastModifiedDate,WhatId FROM Event where WhatId in : setPIDs and LastModifiedDate >: system.now()-integer.valueof(System.Label.CLAPR15I2P14)];
        for(Event oEvent : lstEvent){
            setOfEventWhatIds.add(oEvent.WhatId);
        }
        system.debug('@Test 4 : '+setOfEventWhatIds); 
        
        
        // Collect all the set of ID's which we need to exclude from setPIDs

         setOfIdsToExclude.addAll(setOfFeedItemParentIDs);
        //  setOfIdsToExclude.addAll(setOfTaskWhatIds);
        //  setOfIdsToExclude.addAll(setOfEventWhatIds);

        // setofIDsLongModified = setOfIDs - (setOfFeedItemParentIDs + setOfTaskWhatIds + setOfEventWhatIds )

        for(Id oID : setPIDs){
            if(!setOfIdsToExclude.contains(oID)){
                system.debug('setOfIdsToExclude : :'+oID);
                setofIDsLongModified.add(oID);
            }

        }
        system.debug('setofIDsLongModified : '+setofIDsLongModified);
        return setofIDsLongModified;
    }
    
    */
    public void createAlertForBRE_Stackholders(set<ID> setofIDsLongModified ) {
        
        lstBRE = [select id, AffectedCustomer__c, ResolutionLeader__r.email, BusinessRiskSponsor__r.email , LastModifiedDate, Actual_User_LastmodifiedDate__c, ResolutionLeader__c, BusinessRiskSponsor__c from BusinessRiskEscalations__c where id in : setofIDsLongModified];
        system.debug('lstBRE : '+lstBRE);
        Business_Risk_Escalation_Alert__c oBREAlt_RLeader;
        Business_Risk_Escalation_Alert__c oBREAlt_RSponser;
        for(BusinessRiskEscalations__c oBusinessRS : lstBRE ){
        system.debug(' ##### oBusinessRS.ResolutionLeader__c'+ oBusinessRS.ResolutionLeader__c);
             if( oBusinessRS.ResolutionLeader__c != NULL){
                oBREAlt_RLeader = new Business_Risk_Escalation_Alert__c();
                oBREAlt_RLeader.Business_Risk_Escalation__c = oBusinessRS.id;
              //  oBREAlt_RLeader.Resolution_Leader_Sponsor__c = oBusinessRS.ResolutionLeader__c;
                oBREAlt_RLeader.User_Email__c = oBusinessRS.ResolutionLeader__r.email;
                lstBREAlert.add(oBREAlt_RLeader);
             }
             system.debug(' ##### oBusinessRS.BusinessRiskSponsor__c'+ oBusinessRS.BusinessRiskSponsor__c);
             if(oBusinessRS.BusinessRiskSponsor__c != NULL){
                oBREAlt_RSponser = new Business_Risk_Escalation_Alert__c();
                oBREAlt_RSponser.Business_Risk_Escalation__c = oBusinessRS.id;
            //    oBREAlt_RSponser.Resolution_Leader_Sponsor__c = oBusinessRS.BusinessRiskSponsor__c;
                oBREAlt_RSponser.User_Email__c = oBusinessRS.BusinessRiskSponsor__r.email;
                lstBREAlert.add(oBREAlt_RSponser);
             }

        }
        String concateBREwithResolutionLS;
        system.debug('lstBREAlert'+ lstBREAlert);
        for(Business_Risk_Escalation_Alert__c  oBusiREA : lstBREAlert){
            system.debug('User Email ID :  '+ oBusiREA.User_Email__c);
            String userEmail = oBusiREA.User_Email__c;
            concateBREwithResolutionLS = oBusiREA.Business_Risk_Escalation__c +''+ userEmail;
            if(!BREwithResolutionLeaderOrSponsor.contains(concateBREwithResolutionLS)){
                BREwithResolutionLeaderOrSponsor.add(concateBREwithResolutionLS);
                lstBREAtoInsert.add(oBusiREA);
            }
        }
        system.debug('List  of BREA to insert : lstBREAtoInsert'+ lstBREAtoInsert);
        try{
            if(lstBREAtoInsert.size()>0){
                database.insert(lstBREAtoInsert, false);
            }
        }
        catch(Exception e){
            
            system.debug('Exception'+ e);
        }
    }
}