global class ProgramLevel {
    // default value set to BFO
    WebService string PRM_PROG_PS { get; set; }

    // bFO ID of Partner Program
    WebService string PRM_PROG_ID { get; set; }

    // bFO ID of Program Level
    WebService string LEVEL_ID { get; set; }
    
    // default value to UPSERT
    WebService string DML_ACTION { get; set; }
    
    // Name of the Partner Program
    WebService string NAME { get; set; }
    
    // Description of the Partner Program
    WebService string DESCRIPTION { get; set; }
    
    // LastUpdatetdDate timestamp in bFO
    WebService Datetime SDH_VERSION { get; set; }
    
    //START: Release APR14, BR-4753
    //Status of Partner Program 
     WebService string IS_ACTIVE {get; set;}
    
    //END: Release APR14
    
    public ProgramLevel() {
        PRM_PROG_PS = 'BFO';
        DML_ACTION = 'UPSERT';
    }
}