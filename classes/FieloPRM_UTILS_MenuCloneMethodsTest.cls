/********************************************************************
* Company: Fielo
* Created Date: 05/08/2016
* Description: Test class for class FieloPRM_UTILS_MenuCloneMethods
********************************************************************/
@isTest
public class FieloPRM_UTILS_MenuCloneMethodsTest {
    
     @testSetup static void testSetupMethodPRM() {
        
        Country__c testCountry = new Country__c(
            Name   = 'Global',
            CountryCode__c = 'WW'
        );
        insert testCountry;
       
        PRMCountry__c testPRmcountry = new PRMCountry__c(
            Name   = 'Global',
            Country__c = testCountry.id,
            PLDatapool__c = '123123test'       
        );
        insert testPRmcountry;
    }
    
    public static testMethod void testUnit(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c prog = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c= 'Polo',
                FieloEE__FirstName__c = 'Marco',
                FieloEE__Street__c = 'test',
                FieloEE__Program__c = prog.Id
            );
            insert member;
            
            Contact cont = new Contact(
                LastName = 'TEST',
                FieloEE__Member__c = member.Id
            );
            insert cont;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c(
                Name = 'Lucas Test',
                FieloEE__Title__c = 'asd',
                FieloEE__ExternalName__c = 'thisMenu' + datetime.now()
            );
            insert thisMenu;
                       
            Country__c testCountry = [SELECT Id FROM Country__c LIMIT 1];

            ClassificationLevelCatalog__c classLevel1 = new ClassificationLevelCatalog__c(
                Name  = 'EU',
                ClassificationLevelName__c = 'End User or Consumer'
            );
            insert classLevel1;

            ClassificationLevelCatalog__c classLevel2 = new ClassificationLevelCatalog__c(
                Name = 'EUB',
                ClassificationLevelName__c = 'Large Corporation',
                ParentClassificationLevel__c = classLevel1.Id
            );
            insert classLevel2;

            CountryChannels__c cntryChannels = new CountryChannels__c(
                Channel__c = classLevel1.Id,
                SubChannel__c = classLevel2.Id,
                Active__c = True,
                Country__c = testCountry.Id,
                AllowPrimaryToManage__c = True
            );
            insert cntryChannels;

            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
                        
            FieloEE__SegmentDomain__c segDomain = new FieloEE__SegmentDomain__c(
                FieloEE__Menu__c = thisMenu.id,
                FieloEE__Segment__c = segment.id            
            );
            insert segDomain;
            
            FieloEE__Section__c section = new FieloEE__Section__c(
                FieloEE__Menu__c = thisMenu.Id
            );
            insert section;
            
            FieloEE__Tag__c tag = new FieloEE__Tag__c(
                Name = 'test'
            );
            insert tag;
            
            FieloEE__Category__c category = FieloPRM_UTILS_MockUpFactory.createCategory(thisMenu.Id);
        
            
            FieloEE__Component__c comp = new FieloEE__Component__c(
                FieloEE__Section__c = section.Id,
                FieloEE__Menu__c = thisMenu.Id,
                FieloEE__Tag__c = tag.id
            );
            insert comp;
            
            FieloEE__News__c contentFeed = new FieloEE__News__c(
                FieloEE__IsActive__c = true,
                FieloEE__Component__c = comp.Id,
                FieloEE__CategoryItem__c = category.Id
            );
            insert contentFeed;
            
            FieloEE__Banner__c banner = new FieloEE__Banner__c(
                FieloEE__isActive__c = false,
                FieloEE__Component__c  = comp.Id,
                FieloEE__Category__c = category.Id,
                Name = 'test'
            );
            insert banner;
            
            FieloEE__TagItem__c tagItemB = new FieloEE__TagItem__c(
                FieloEE__Banner__c = banner.Id,
                FieloEE__Tag__c = tag.Id
            );
            insert tagItemB ;
            
            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c(
                FieloEE__News__c = contentFeed.Id,
                FieloEE__Tag__c = tag.Id
            );
            insert tagItem;
            
            test.startTest();
            
            FieloPRM_UTILS_MenuCloneMethods.createNewMenuUseTemplateContent(thisMenu.Id,'categorytest',thisMenu.name,cntryChannels);

            test.stopTest();
        }
    }
    
    public static testMethod void testUnitWithoutSegment(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c prog = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c= 'Polo',
                FieloEE__FirstName__c = 'Marco',
                FieloEE__Street__c = 'test',
                FieloEE__Program__c = prog.Id
            );
            insert member;
            
            Contact cont = new Contact(
                LastName = 'TEST',
                FieloEE__Member__c = member.Id
            );
            insert cont;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c(
                Name = 'Lucas Test',
                FieloEE__Title__c = 'asd',
                FieloEE__ExternalName__c = 'thisMenu1' + datetime.now()
            );
            insert thisMenu;

            FieloEE__Section__c section = new FieloEE__Section__c(
                FieloEE__Menu__c = thisMenu.Id
            );
            insert section;
            
            FieloEE__Tag__c tag = new FieloEE__Tag__c(
                name = 'test'
            );
            insert tag;
            
            FieloEE__Category__c category = FieloPRM_UTILS_MockUpFactory.createCategory(thisMenu.Id);
                   
            FieloEE__Component__c comp = new FieloEE__Component__c(
                FieloEE__Section__c = section.Id,
                FieloEE__Menu__c = thisMenu.Id,
                FieloEE__Tag__c = tag.id
            );
            insert comp;
            
            FieloEE__News__c contentFeed = new FieloEE__News__c(
                FieloEE__IsActive__c = true,
                FieloEE__Component__c = comp.id,
                FieloEE__CategoryItem__c = category.id
            );
            insert contentFeed;
            
            FieloEE__Banner__c banner = new FieloEE__Banner__c(
                FieloEE__isActive__c = false,
                FieloEE__Component__c  = comp.id,
                FieloEE__Category__c = category.id,
                Name = 'test'
            );
            insert banner;
            
            FieloEE__TagItem__c tagItemB = new FieloEE__TagItem__c(
                FieloEE__Banner__c = banner.id,
                FieloEE__Tag__c = tag.id
            );
            insert tagItemB;
            
            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c(
                FieloEE__News__c = contentFeed.id,
                FieloEE__Tag__c = tag.id
            );
            insert tagItem;
            
            test.startTest();
            
            FieloPRM_UTILS_MenuCloneMethods.createNewMenuUseTemplateContent(thisMenu.Id,'categorytest',thisMenu.name,null);
            
            test.stopTest();
        }
    }

    public static testMethod void testUnit2(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c prog = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c= 'Polo',
                FieloEE__FirstName__c = 'Marco',
                FieloEE__Street__c = 'test',
                FieloEE__Program__c = prog.Id
            );
            insert member;
            
            Contact cont = new Contact(
                LastName = 'TEST',
                FieloEE__Member__c = member.Id
            );
            insert cont;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c(
                Name = 'Lucas Test',
                FieloEE__Title__c = 'asd',
                FieloEE__ExternalName__c = 'thisMenu' + datetime.now()
            );
            insert thisMenu;

            Country__c testCountry = [SELECT Id FROM Country__c LIMIT 1];

            ClassificationLevelCatalog__c classLevel1 = new ClassificationLevelCatalog__c(
                Name = 'EU',
                ClassificationLevelName__c = 'End User or Consumer'
            );
            insert classLevel1;

            ClassificationLevelCatalog__c classLevel2 = new ClassificationLevelCatalog__c(
                Name = 'EUB',
                ClassificationLevelName__c = 'Large Corporation',
                ParentClassificationLevel__c = classLevel1.Id
            );
            insert classLevel2;

            CountryChannels__c cntryChannels = new CountryChannels__c(
                Channel__c = classLevel1.Id,
                SubChannel__c = classLevel2.Id,
                Active__c = True,
                Country__c = testCountry.Id,
                AllowPrimaryToManage__c = True
            );
            INSERT cntryChannels;

            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
                       
            FieloEE__SegmentDomain__c segDomain = new FieloEE__SegmentDomain__c(
                FieloEE__Menu__c  = thisMenu.id,
                FieloEE__Segment__c = segment.id
            );
            insert segDomain;
            
            FieloEE__Section__c section = new FieloEE__Section__c(
                FieloEE__Menu__c = thisMenu.Id
            );
            insert section;
            
            FieloEE__Tag__c tag = new FieloEE__Tag__c(
                name = 'test'
            );
            insert tag;
            
            FieloEE__Category__c category = FieloPRM_UTILS_MockUpFactory.createCategory(thisMenu.id);
                    
            FieloEE__Component__c comp = new FieloEE__Component__c(
                FieloEE__Section__c = section.Id,
                FieloEE__Menu__c = thisMenu.Id,
                FieloEE__Tag__c = tag.id
            );
            insert comp;
            
            FieloEE__News__c contentFeed = new FieloEE__News__c(
                FieloEE__IsActive__c = true,
                FieloEE__Component__c = comp.id,
                FieloEE__CategoryItem__c = category.id
            );
            insert contentFeed;
            
            FieloEE__Banner__c banner = new FieloEE__Banner__c(
                FieloEE__isActive__c = false,
                FieloEE__Component__c  = comp.id,
                FieloEE__Category__c = category.id,
                Name = 'test'
            );
            insert banner;
            
            FieloEE__TagItem__c tagItemB = new FieloEE__TagItem__c(
                FieloEE__Banner__c = banner.id,
                FieloEE__Tag__c = tag.id
            );
            insert tagItemB ;
            
            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c(
                FieloEE__News__c = contentFeed.id,
                FieloEE__Tag__c = tag.id
            );
            insert tagItem;
            
            test.startTest();

            string idNewMenuTemplate = FieloPRM_UTILS_MenuCloneMethods.cloneAsTemplateContent(thisMenu.id, thisMenu.name + ' Template', thisMenu.name,cntryChannels);
            
            test.stopTest();
        }
    }
    
}