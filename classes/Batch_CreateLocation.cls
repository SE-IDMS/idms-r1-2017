/*
    Author          : Hari Krishna
    Date Created    : 30/10/2013 November Release
    Description     : Batch Apex class Location for InstalleProducts 
              
*/
    global class Batch_CreateLocation implements Database.Batchable<sObject>{

     global final String Query;
     global String ErrorMessages ='';
     global string STANDARD_LOCATION = Label.CLNOV13SRV01;
     global string SE_READONLYSITE = Label.CLNOV13SRV02;



     global Database.QueryLocator start(Database.BatchableContext BC){ 
         // Commented for EUS#032552       
        //return Database.getQueryLocator([select id,SVMXC__Account__c,RecordTypeId,SVMXC__Street__c,SVMXC__Zip__c,StateProvince__c,PrimaryLocation__c,TECH_LocSendToBatch__c,SVMXC__Parent__c from SVMXC__Site__c where TECH_LocSendToBatch__c=true and SVMXC__Parent__c=null and (RecordTypeId=:SE_READONLYSITE or RecordTypeId=:STANDARD_LOCATION) and SVMXC__Account__c!=null]);
       return Database.getQueryLocator([select id,SVMXC__Account__c,RecordTypeId,SVMXC__Street__c,SVMXC__Zip__c,StateProvince__c,PrimaryLocation__c,TECH_LocSendToBatch__c,SVMXC__Parent__c,LocationCountry__c from SVMXC__Site__c where TECH_LocSendToBatch__c=true  and (RecordTypeId=:SE_READONLYSITE or RecordTypeId=:STANDARD_LOCATION) and SVMXC__Account__c!=null ]);
        //Testing
       //return Database.getQueryLocator([select id,SVMXC__Account__c,RecordTypeId,SVMXC__Street__c,SVMXC__Zip__c,StateProvince__c,PrimaryLocation__c,TECH_LocSendToBatch__c,SVMXC__Parent__c,LocationCountry__c from SVMXC__Site__c where  id in ('a2L110000000v9qEAA') ]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){           
        AP_LocationManager.ProcessLocations(scope);
        Database.update(scope,false);
    }


    global void finish(Database.BatchableContext BC){
        /*
        //Send an email to the User after your batch completes
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'harikrishna.singara@schneider-electric.com'};
        // String[] toAddresses = new String[] {'ramzi.haddad@accenture.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Batch_ProductIsEnriched Execution Report');
        String message = ErrorMessages;
        mail.setPlainTextBody(message);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        */

    }

}