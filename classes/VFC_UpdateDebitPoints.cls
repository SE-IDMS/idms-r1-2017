/*
 Created By   : Renjith Jose
 Created Date : 11-Oct-2013
 Description  : BR-4013, NOV13 release Update Debit Points 
 
*/
public class VFC_UpdateDebitPoints
{
    public Case currentCase{get;private set;}
    public Double remainPoints =-1;
    public boolean isError{get;set;}

    public VFC_UpdateDebitPoints(ApexPages.StandardController controller) {
    isError=false;
    }
    
    public boolean UpdateCase() {
    
        try{
            Database.SaveResult caseUpdateResult = Database.update(currentCase,false);
            if(caseUpdateResult.isSuccess()){
                if(remainPoints != -1){
                    List<Contract> lstContract = [Select id,Points__c from Contract where id = :currentCase.RelatedContract__c Limit 1];
                    lstContract[0].Points__c = remainPoints;
                    Database.SaveResult contractUpdateResult = Database.update(lstContract[0],false);
                    if(!contractUpdateResult.isSuccess()){
                        Database.Error err = contractUpdateResult.getErrors()[0];
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Error in Updating Remaining Points to Contract: '+err.getStatusCode()+' '+err.getMessage()));
                        System.Debug('****** Debit Point :Error Updating Contract: '+err.getStatusCode()+' '+err.getMessage());
                        isError=true;
                        return false;
                    }
                    
                }
            }
            else{
                Database.Error err = caseUpdateResult.getErrors()[0];
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Error in Updating case: '+err.getStatusCode()+' '+err.getMessage()));//Change to Label
                System.Debug('****** Debit Point :Error Updating Case: '+err.getStatusCode()+' '+err.getMessage());
                isError=true;
                return false;
            }
            
        }
        catch(Exception ex){
                    System.Debug('****** Debit Point Update Error ');
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Error in Saving of Case: '+ex.getMessage()));//Change to Label
                    isError=true;
                    return false;
        }
        
        return true;
    
    }
    
    public pagereference returnToCase(){
        PageReference casePage = new PageReference('/' + currentCase.Id);
        return casePage; 
    }
        
    public pagereference UpdateDebitPoints()
    {
    
      //Get Case record
      String strcaseID = ApexPages.currentPage().getParameters().get('caseId');
      List <Case> lstCase = [Select id, CaseNumber, ContactId,Team__r.Name,RelatedContract__c,RelatedContract__r.LegacyContractId__c,CTRValueChainPlayer__c,PointDebitReason__c,Points__c,FreeOfCharge__c,DebitPoints__c,TECH_DebitPoints__c,DebitPointsUpdateError__c, Family__c from Case where id = :strcaseID];
      
      currentCase = lstCase.get(0);
                  
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------
    //Debit Point Validation    
    //Return to Case if Free of Charge or Debit Point is already selected
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------  
            
        boolean authorisedTeam ;
        List<Package__c> lstPackageTemplate = new List<Package__c>();
        Set<String> setTemplateid = new Set<String>();
        String mandatoryFields = '';    
            
        if (currentCase.TECH_DebitPoints__c || currentCase.FreeOfCharge__c ){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLNOV13CCC10));//Debit Points/Free of charge is already selected for this case
           // UpdateCase();
           isError=true;
            return null;    
        } 
        else {
            if (currentCase.Team__r.Name != null ){  //Checking of Team is authorized
                List <String> lstTeam = System.Label.CLNOV13CCC09.split(',',0);
                for (String aTeam :lstTeam){
                    authorisedTeam = false;
                    String authTeam = aTeam.trim();
                    if (currentCase.Team__r.Name.left(authTeam.length()).trim() == authTeam ){
                        System.debug('**** Debit Point Update - Authorized Team :'+authTeam); 
                        authorisedTeam = true;
                        break;                    
                    }   
                }
                if(!authorisedTeam){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLNOV13CCC06));//'Case team is not authorized to perform this operation'   
                    isError=true;
                    return null; 
                }
            }
            else
                mandatoryFields= '<li>'+System.Label.CLNOV13CCC12+'</li>';
                
            if (!currentCase.DebitPoints__c)     
                 mandatoryFields= '<li>'+System.Label.CLNOV13CCC13+'</li>';
                
            if (currentCase.CTRValueChainPlayer__c == null)     
                 mandatoryFields= '<li>'+System.Label.CLNOV13CCC03+'</li>';
                 
            if (currentCase.RelatedContract__r.LegacyContractId__c == null)
                mandatoryFields=mandatoryFields+'<li>'+System.Label.CLNOV13CCC08+'</li>';
                
            //if (currentCase.Points__c == null || currentCase.Points__c == 0 )
            //    mandatoryFields=mandatoryFields+'<li>'+System.Label.CLNOV13CCC04+'</li>';
                
            //if (currentCase.PointDebitReason__c == null)
            //    mandatoryFields=mandatoryFields+'<li>'+System.Label.CLNOV13CCC05+'</li>';  

            /*
            //Related product(s) = not empty
            lstPackageTemplate = [Select id,PackageTemplate__c,PackageTemplate__r.LegacyTemplateId__c from Package__c where Contract__c = :currentCase.RelatedContract__c];
            if (lstPackageTemplate.size() >0) {
                System.debug('******* Update Debit Points -'+ lstPackageTemplate.size());
                Set<id> SetPkgTemplateid = new Set<id>();
                for(Package__c cTempid :lstPackageTemplate){
                    SetPkgTemplateid.add(cTempid.PackageTemplate__c);  
                    setTemplateid.add(cTempid.PackageTemplate__r.LegacyTemplateId__c);                      
                } 
                list<ContractProductInformation__c> lstProduct = [Select id from ContractProductInformation__c where packagetemplate__c in :SetPkgTemplateid limit 1];
                System.debug('******* Update Debit Points -'+ lstProduct.size());
                if (lstProduct.size() == 0)
                    mandatoryFields=mandatoryFields+'<li>'+System.Label.CLNOV13CCC14+'</li>';
            }
            else
                mandatoryFields=mandatoryFields+'<li>'+System.Label.CLNOV13CCC14+'</li>';                   
            //</Related product>
            */
        
            // Case Product must be defined
            
            if (currentCase.Family__c == null)
            {
              mandatoryFields=mandatoryFields+'<li>'+System.Label.CLNOV13CCC14+'</li>'; 
            }
            
            if (mandatoryFields.trim() <> ''){
            
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'<ul>'+mandatoryFields+'</ul>')); //'Related CVCP cannot be null'
                isError=true;
                return null;        
            }    
                
        }        
        
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------
     
     try{

            String username = System.Label.CLNOV13CCC07.substringBefore(','); //'bfo_sel_user';
            String password = System.Label.CLNOV13CCC07.substringAfter(',');//'B|0_s-l_u$er'; 
                
            Blob headerValue = Blob.valueOf(username + ':' + password);
            String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);
     
            WS_VantiveDebitPointDecreasePoint.DecreasePointInput input = new WS_VantiveDebitPointDecreasePoint.DecreasePointInput();
                //input.caseId = '500J0000001Cpgn';
                //input.numContract= 'A111111111';
                //input.packageId= '15';
                //input.reason= 'Test';
                //input.userId= '003J000000ATfzX';
                //input.point = 12;
    
            input.caseId = currentCase.Casenumber; //id;SFN: Case Number, not Id
            input.numContract= currentCase.RelatedContract__r.LegacyContractId__c;//'A111111111';//
            
            if (setTemplateid.size() ==1)
                input.packageId= new List<String>(setTemplateid)[0];                        
                
            else
                input.packageId= '0';
                    
            input.reason= currentCase.PointDebitReason__c;
            input.userId =  currentCase.ContactId; //'003J000000ATfzX' ;//
            input.point = currentCase.Points__c;
    
            System.debug('******** VFC_UpdateDebitPoints Input Values *********');
            System.debug('******** VFC_UpdateDebitPoints - Case ID :'+input.caseId);
            System.debug('******** VFC_UpdateDebitPoints - Contract ID :'+input.numContract);
            System.debug('******** VFC_UpdateDebitPoints - Package ID :'+input.packageId);
            System.debug('******** VFC_UpdateDebitPoints - Reason :'+ input.reason);
            System.debug('******** VFC_UpdateDebitPoints - User ID :'+  input.userId);
            System.debug('******** VFC_UpdateDebitPoints - Debit Point :'+  input.point);
    
            WS_VantiveDebitPointService.SelServicesHttpSoap11Endpoint newRequest = new WS_VantiveDebitPointService.SelServicesHttpSoap11Endpoint();
            newRequest.inputHttpHeaders_x = new Map<String, String>();
            newRequest.inputHttpHeaders_x.put('Authorization',authorizationHeader);
            // SFN: Use Endpoint defined in WS //
            newRequest.endpoint_x = System.Label.CLNOV13CCC11;
            //  newRequest.ClientCertName_x = System.Label.CLNOV13CCC16;
            newRequest.timeout_x=Integer.valueOf(System.Label.CLMAY13CCC39);
        
           //WS_VantiveDebitPointDecreasePoint.DecreasePointOutput wsResult = newRequest.decreasePoint(input);              
           
            WS_VantiveDebitPointDecreasePoint.DecreasePointOutput wsResult;
            
            if (!System.Test.isRunningTest())
                    wsResult= newRequest.decreasePoint(input);                
            
            if(wsResult != null){
                if(wsResult.returnCode == 0){
                    remainPoints = wsResult.remainPoint; 
                    currentCase.TECH_DebitPoints__c = true;
                    currentCase.DebitPointsUpdateError__c = 'Success';   
                } //wsResult.returnCode == 0    
                else {
                        currentCase.DebitPointsUpdateError__c = 'Error: '+wsResult.errorLabel ; //Change to Label    
                        System.Debug('****** Debit Point -> Error Updating Data ');             
                }
                if (!UpdateCase())
                    return null;
            }       
        }
        catch(CalloutException callOutEx){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error,callOutEx.getMessage()));//System.Label.CLNOV13CCC15));
                System.debug('Connection Error: '+callOutEx.getMessage());  
                isError=true;
                return null;
        } 
                 
        System.Debug('****** Debit Point Update Completed *********');
        PageReference casePage = new PageReference('/' + currentCase.Id);
        return casePage;        
    }
}