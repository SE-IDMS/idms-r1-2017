Public class AP_COIN_bFO_OfferLifeCycleForecast {

    public static boolean isOfferExt=false;

    public static Void UpdaeForecastBCAValue(Map<id,OfferLifeCycleForecast__c> mapNewOffer,Map<id,OfferLifeCycleForecast__c> mapOldOffer) {
        List<OfferLifeCycleForecast__c> listOffer = new List<OfferLifeCycleForecast__c>();
        Set<string> setOffId = new Set<string>() ;
        OfferLifeCycleForecast__c offForecast= new OfferLifeCycleForecast__c();
        
        Set<string> setOffProjectId = new Set<string>() ;
        
        Map<string,OfferLifeCycleForecast__c> MapOfferMonth = new Map<string,OfferLifeCycleForecast__c>();
        //To get ProjectId AND Forecast Year
        for(OfferLifeCycleForecast__c offObj:mapNewOffer.values()) {
        
            setOffId.add(offObj.ForcastYear__c);
            setOffProjectId.add(offObj.project__c);        
        
        }
        
        for( OfferLifeCycleForecast__c obj:[select Id,TECH_BuNew__c,month__c,project__c,ForcastYear__c,TECH_ForecastBUMonthDifference__c,TECH_ForecastActualMonthDifference__c,TECH_CountryNew__c,TECH_CountryOld__c,TECH_Buold__c,Actual__c,BU__c,TECH_ActualNew__c,TECH_ActualOld__c,Country__c from OfferLifeCycleForecast__c where project__c=:setOffProjectId and ForcastYear__c=:setOffId ]) {
            MapOfferMonth.put(Obj.month__c+obj.ForcastYear__c,obj);
            //listOffer1.add(obj); 
        }
        system.debug('qs@@@@@@@@@@@@----->'+MapOfferMonth);
        
    
        for(OfferLifeCycleForecast__c  obj:[select Id,month__c,ForcastYear__c,TECH_BuNew__c,project__c,TECH_ForecastBUMonthDifference__c,TECH_CountryNew__c,TECH_CountryOld__c,TECH_Buold__c,Actual__c,BU__c,TECH_ActualNew__c,TECH_ActualOld__c,Country__c from OfferLifeCycleForecast__c where Id=:mapNewOffer.keySet()]) {
            if(obj.BU__c!=null) {
                obj.TECH_BuNew__c=obj.BU__c;
                obj.TECH_Buold__c =0;
            }else {
                obj.TECH_BuNew__c=0;
                obj.TECH_Buold__c =0;
            
            }
            
            if(obj.Actual__c!=null) {
                obj.TECH_ActualNew__c=obj.Actual__c;
                obj.TECH_ActualOld__c =0;
            }else {
                obj.TECH_ActualNew__c=0;
                obj.TECH_ActualOld__c =0;
            
            }
            if(obj.Country__c!=null) {
                obj.TECH_CountryOld__c =0;
                obj.TECH_CountryNew__c =0;
                
            } else {
                obj.TECH_CountryOld__c =0;
                obj.TECH_CountryNew__c =0;
            
            }
            system.debug('outside----->'+Obj.month__c+obj.ForcastYear__c);
            if(MapOfferMonth.containsKey(Obj.month__c+obj.ForcastYear__c)) {
                
                system.debug('Inside----->'+Obj.month__c+obj.ForcastYear__c);
                
                if(Obj.ForcastYear__c==MapOfferMonth.get(Obj.month__c+obj.ForcastYear__c).ForcastYear__c &&Obj.project__c ==MapOfferMonth.get(Obj.month__c+obj.ForcastYear__c).project__c ) {
                
                    if(obj.month__c=='February') {
                        if(MapOfferMonth.containsKey('January'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('January'+obj.ForcastYear__c).BU__c;
                            obj.TECH_ForecastCountryMonthDifference__c=MapOfferMonth.get('January'+obj.ForcastYear__c).Country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('January'+obj.ForcastYear__c).Actual__c;
                       }
                    }
                    if(obj.month__c=='March') {
                        if(MapOfferMonth.containsKey('February'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('February'+obj.ForcastYear__c).BU__c;
                            obj.TECH_ForecastCountryMonthDifference__c=MapOfferMonth.get('February'+obj.ForcastYear__c).Country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('February'+obj.ForcastYear__c).Actual__c;
                        }

                    }
                    if(obj.month__c=='April') {
                        if(MapOfferMonth.containsKey('March'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('March'+obj.ForcastYear__c).BU__c;
                            obj.TECH_ForecastCountryMonthDifference__c =MapOfferMonth.get('March'+obj.ForcastYear__c).Country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('March'+obj.ForcastYear__c).Actual__c;
                        }    
                    }
                    if(obj.month__c=='May') {
                        if(MapOfferMonth.containsKey('April'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('April'+obj.ForcastYear__c).BU__c;
                            obj.TECH_ForecastCountryMonthDifference__c=MapOfferMonth.get('April'+obj.ForcastYear__c).Country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('April'+obj.ForcastYear__c).Actual__c;
                        }    
                    }
                    if(obj.month__c=='June') {
                        if(MapOfferMonth.containsKey('May'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('May'+obj.ForcastYear__c).BU__c;
                            obj.TECH_ForecastCountryMonthDifference__c=MapOfferMonth.get('May'+obj.ForcastYear__c).Country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('May'+obj.ForcastYear__c).Actual__c;
                        }
                    }
                    if(obj.month__c=='July') {
                        if(MapOfferMonth.containsKey('June'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('June'+obj.ForcastYear__c).BU__c;
                            obj.TECH_ForecastCountryMonthDifference__c=MapOfferMonth.get('June'+obj.ForcastYear__c).Country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('June'+obj.ForcastYear__c).Actual__c;
                       }
                    }
                    if(obj.month__c=='August') {
                        if(MapOfferMonth.containsKey('July'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('July'+obj.ForcastYear__c).BU__c;
                            obj.TECH_ForecastCountryMonthDifference__c=MapOfferMonth.get('July'+obj.ForcastYear__c).Country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('July'+obj.ForcastYear__c).Actual__c;
                        }
                    }
                    if(obj.month__c=='September') {
                        if(MapOfferMonth.containsKey('August'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('August'+obj.ForcastYear__c).TECH_ForecastBUMonthDifference__c;
                            obj.TECH_ForecastCountryMonthDifference__c=MapOfferMonth.get('August'+obj.ForcastYear__c).Country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('August'+obj.ForcastYear__c).Actual__c;
                        }
                    }
                    if(obj.month__c=='October') {
                        if(MapOfferMonth.containsKey('September'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('September'+obj.ForcastYear__c).BU__c;
                            obj.TECH_ForecastCountryMonthDifference__c=MapOfferMonth.get('September'+obj.ForcastYear__c).Country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('September'+obj.ForcastYear__c).Actual__c;
                        }
                    }
                    if(obj.month__c=='November') {
                        if(MapOfferMonth.containsKey('October'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('October'+obj.ForcastYear__c).BU__c;
                            obj.TECH_ForecastCountryMonthDifference__c=MapOfferMonth.get('October'+obj.ForcastYear__c).country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('October'+obj.ForcastYear__c).Actual__c;
                        }
                    }
                    if(obj.month__c=='December') {
                        if(MapOfferMonth.containsKey('November'+obj.ForcastYear__c)) {
                            obj.TECH_ForecastBUMonthDifference__c=MapOfferMonth.get('November'+obj.ForcastYear__c).BU__c;
                            obj.TECH_ForecastCountryMonthDifference__c=MapOfferMonth.get('November'+obj.ForcastYear__c).Country__c;
                            obj.TECH_ForecastActualMonthDifference__c =MapOfferMonth.get('November'+obj.ForcastYear__c).Actual__c;
                        }
                    }
                
                }
                
            }
            
            listOffer.add(obj);
        }
    
    
        if(listOffer.size() > 0) {
            isOfferExt=true;
            update listOffer;
        }
    
    
    }
    
    public static Void afterUpdateForecastBCAValue(Map<id,OfferLifeCycleForecast__c> mapNewOffer,Map<id,OfferLifeCycleForecast__c> mapOldOffer) {
       
        List<OfferLifeCycleForecast__c> listOffer = new List<OfferLifeCycleForecast__c>();
        Set<string> setOffId = new Set<string>() ;
       
        Set<string> setOffProjectId = new Set<string>() ;
        
        Map<string,OfferLifeCycleForecast__c> MapOfferMonth = new Map<string,OfferLifeCycleForecast__c>();
        Map<string,OfferLifeCycleForecast__c> MapOfferMonthUpdate = new Map<string,OfferLifeCycleForecast__c>();
        
        //To get ProjectId AND Forecast Year
        
        for(OfferLifeCycleForecast__c offObj:mapNewOffer.values()) {
        
            setOffId.add(offObj.ForcastYear__c);
            setOffProjectId.add(offObj.project__c);               
        }
        
        for( OfferLifeCycleForecast__c obj:[select Id,TECH_BuNew__c,month__c,project__c,ForcastYear__c,TECH_ForecastBUMonthDifference__c,TECH_ForecastActualMonthDifference__c,TECH_CountryNew__c,TECH_CountryOld__c,TECH_Buold__c,Actual__c,BU__c,TECH_ActualNew__c,TECH_ActualOld__c,Country__c from OfferLifeCycleForecast__c where project__c=:setOffProjectId and ForcastYear__c=:setOffId ]) {
            MapOfferMonth.put(Obj.month__c+obj.ForcastYear__c,obj);
            //listOffer1.add(obj); 
        }
        for(OfferLifeCycleForecast__c  obj:[select Id,month__c,ForcastYear__c,TECH_BuNew__c,project__c,TECH_ForecastBUMonthDifference__c,TECH_CountryNew__c,TECH_CountryOld__c,TECH_Buold__c,Actual__c,BU__c,TECH_ActualNew__c,TECH_ActualOld__c,Country__c from OfferLifeCycleForecast__c where Id=:mapNewOffer.keySet()]) {

            
                for(OfferLifeCycleForecast__c ocfObj:MapOfferMonth.values()) {
                    if(obj.ForcastYear__c==ocfObj.ForcastYear__c && obj.project__c==ocfObj.project__c) {
                        
                        if( Obj.month__c=='January' && ocfobj.month__c=='February'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                            ///listOffer.add(ocfObj);
                            
                        }
                        
                        if(Obj.month__c=='February' && ocfobj.month__c=='March'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                            //listOffer.add(ocfObj);
                           
                        }
                        if(Obj.month__c=='March' && ocfobj.month__c=='April'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                        
                            //listOffer.add(ocfObj);
                        }
                        if(Obj.month__c=='April' && ocfobj.month__c=='May'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                            //listOffer.add(ocfObj);
                           
                        }
                        if(Obj.month__c=='May' && ocfobj.month__c=='June'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                        //listOffer.add(ocfObj);
                           
                        }
                        if(Obj.month__c=='June' && ocfobj.month__c=='July'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                        //listOffer.add(ocfObj);
                           
                        }
                        if(Obj.month__c=='July' && ocfobj.month__c=='August'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                        //listOffer.add(ocfObj);
                           
                        }
                        if(Obj.month__c=='August' && ocfobj.month__c=='September'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                        
                           listOffer.add(ocfObj);
                        }
                        if(Obj.month__c=='September' && ocfobj.month__c=='October'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                        //listOffer.add(ocfObj);
                           
                        }
                        if(Obj.month__c=='October' && ocfobj.month__c=='November'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                        //listOffer.add(ocfObj);
                           
                        }
                        if(Obj.month__c=='November' && ocfobj.month__c=='December'  ) {
                       
                        
                            ocfObj.TECH_ForecastBUMonthDifference__c=obj.BU__c;
                            ocfObj.TECH_ForecastCountryMonthDifference__c=obj.Country__c;
                            ocfObj.TECH_ForecastActualMonthDifference__c =obj.Actual__c;
                            
                           
                        }                                               
                         
                    }
                    //listOffer.add(ocfObj);
                    MapOfferMonthUpdate.put(ocfObj.id,ocfObj);
                }
            
            

        }
        System.debug('TestList***********'+listOffer);
        
        if(listOffer.size() > 0) {
            isOfferExt=true;
            update MapOfferMonthUpdate.values();//listOffer; 
        }
        
       
    }
    Public static void toUpdatTotalBCA(Map<id,OfferLifeCycleForecast__c> mapNewOffer) {
        
        Map<id ,string> mapCheckBCEmpty = new Map<id,string> ();
        Map<id ,string> mapCheckCountryEmpty = new Map<id,string>(); 
        List<Milestone1_Project__c>  listMPForecast= new List<Milestone1_Project__c>();
        Set<string> setOffProjectId = new Set<string>() ;
        for(OfferLifeCycleForecast__c offObj:[select id,Month__c,project__c,BU__c,country__c,ForcastYear__c from OfferLifeCycleForecast__c where id=:mapNewOffer.keyset()]) {
            if(offObj.month__c=='Total by Year') {
                if(offobj.BU__c==0) {
                    mapCheckBCEmpty.put(offObj.project__c,'true');
                }if(offobj.Country__c==0) {
                    mapCheckCountryEmpty.put(offObj.project__c,'true');
                }               
                setOffProjectId.add(offObj.project__c);
                
            
            }
                          
        }
        Map<id,Milestone1_Project__c> listMP=new Map<id,Milestone1_Project__c>([select id,IsCountryForecostBlank__C,IsBUForecostBlank__c from Milestone1_Project__c where ID=:setOffProjectId]);
        system.debug('Tee---------->'+mapCheckCountryEmpty);
        system.debug('mapCheckBCEmpty---------->'+mapCheckBCEmpty);
        for(Milestone1_Project__c mpObj:listMP.values()){
            if(mapCheckBCEmpty.containsKey(mpObj.id) && mapCheckCountryEmpty.containsKey(mpObj.id) ) {
                mpObj.IsBUForecostBlank__c=true;
                mpObj.IsCountryForecostBlank__c=true;
            }else {

                if(mapCheckBCEmpty.containsKey(mpObj.id)) {
                    mpObj.IsBUForecostBlank__c=true;
                }else {  
                    mpObj.IsBUForecostBlank__c =false;
                }
                if(mapCheckCountryEmpty.containsKey(mpObj.id)) {
                    mpObj.IsCountryForecostBlank__c=true;
                }else {
                    mpObj.IsCountryForecostBlank__c=false;
                }

            }

            listMPForecast.add(mpObj);
            
        }  
         
        if(listMPForecast.size() > 0) {
            update listMPForecast;
            system.debug('Test---.'+listMPForecast);
        }
    
   }
   
   public static void ProjectLaunchbeforeupdate(Map<id,OfferLifeCycleForecast__c> mapNewOffer,Map<id,OfferLifeCycleForecast__c> mapOldOffer){
    Map<Id,OfferLifeCycleForecast__c> oldmap = new Map<Id,OfferLifeCycleForecast__c> ();
    //oldmap = Trigger.oldMap;

        for(OfferLifeCycleForecast__c offerforecast : mapNewOffer.values()){
                    OfferLifeCycleForecast__c oldofferforecast = new OfferLifeCycleForecast__c();
                    oldofferforecast = mapOldOffer.get(offerforecast.Id);
                    offerforecast.TECH_ActualOld__c = oldofferforecast.Actual__c;
                    offerforecast.TECH_Buold__c = oldofferforecast.BU__c;
                    offerforecast.TECH_CountryOld__c = oldofferforecast.Country__c;
                    System.debug('#####Old Value'+oldofferforecast.Actual__c);
                    System.debug('#####New Value'+offerforecast.Actual__c);
                    isOfferExt = true;
                    //System.debug('#####'+oldofferforecast.TECH_CountryOld__c);
        }                
    }






}//for end methods