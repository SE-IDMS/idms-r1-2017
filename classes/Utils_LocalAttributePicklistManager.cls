/*
    Author          : Accenture IDC Team 
    Date Created    : 11/02/2011
    Description     : Utility class to populate Picklist Filter values for Local Attribute search
                      extending abstract Data Source Class
*/

Public Class Utils_LocalAttributePicklistManager implements Utils_PicklistManager
{
    
    // Return name of the levels
    Public list<String> getPickListLabels()
    {
        List<String> Labels = new List<String>();
        Labels.add(sObjectType.Country__c.getLabel());
        Labels.add(sObjectType.LocalAttribute__c.fields.Category__c.getLabel());
        return Labels;
    } 

    // Return All Picklist Values 
    public List<SelectOption> getPicklistValues(Integer i, String S)
    {
       List<SelectOption> options = new  List<SelectOption>();
       
       options.add(new SelectOption(Label.CL00355,Label.CL00355)); 
 
       // Populate Business fields and Country when Page Loads
       if(i==1)
       {
            // Populate Country Picklist Values
            for(Country__c countries: [Select Id, Name from Country__c order by Name asc])
            {
                options.add(new SelectOption(countries.Id,countries.Name));
                
            }
       }
       if(i==2)
       {
            // Populate Category Picklist Values
            Schema.DescribeFieldResult category = Schema.sObjectType.LocalAttribute__c.fields.Category__c;
            for (Schema.PickListEntry PickVal : category.getPicklistValues())
            {
                options.add(new SelectOption(PickVal.getValue(),PickVal.getLabel()));
            }
       }  

       return  options;  
    }
}