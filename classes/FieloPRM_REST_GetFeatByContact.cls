/********************************************************************************************************
    Author: Fielo Team
    Date: 06/03/2015
    Description: REST API that receives a list of salesforce Contact Ids and returns a map with salesforce 
                 Contact id as key and a list of Features (feature external id) as value.
                 If a Contact is not found it's not included in the return map.
                 If a Contact is found but has no Features an empty list is return as value.
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestGetFeaturesByContact/*')
global class FieloPRM_REST_GetFeatByContact{   

    /**
    * [getFeaturesByContact returns features for a list of Contact Ids]
    * @method   getFeaturesByContact
    * @Pre-conditions  
    * @Post-conditions 
    * @param    List<String>               listContactIds  [list of salesforce Contact Ids]
    * @return   Map<String,List<String>>                   [map with salesforce Contact id as key and a list of Features (feature external id) as value]
    */
    @HttpPost
    global static Map<String,List<String>> getFeaturesByContact(List<String> listContactPRMUIMSIds){    
        
        return FieloPRM_UTILS_Features.getFeaturesByContact(listContactPRMUIMSIds); 
    
    }   
}