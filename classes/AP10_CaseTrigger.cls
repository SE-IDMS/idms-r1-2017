/*
    Author          : Accenture IDC Team 
    Date Created    : 19/05/2011
    Description     : Class utilised by triggers CaseBeforeUpdate.
    -----------------------------------------------------------------
                              MODIFICATION HISTORY
    -----------------------------------------------------------------
    Modified By     : Shruti Karn
    Modified Date   : 12 June 2012
    Description     : Added following method for September 2012 Release:
                      1. updateFirstAssignmentAge - to update 'First Assignment Age' when the Case ownership is taken
                                                    by an Agent for the first time.
                      2. Reset Contract Related Fields on Case when CVCP is removed from Case.
                      3. Update Contract Name on the Case
    -----------------------------------------------------------------                  
    Modified By     : Rakhi Kumar
    Modified Date   : 12 Feb 2013
    Description     : Added method updateContact for May 2013 Release. This method will assign a contact for the case
                      based on the Supplied Email or Supplied Phone number when a case origin is 'Web'.
   -----------------------------------------------------------------
   Modified By     : Renjith Jose
   Modified Date   : 04 Aug 2013
   Description     : BR-3139, Case Creation: Prohibit selection of ‘To be deleted Contacts’
                     
   ----------------------------------------------------------------- 
   Modified By     : Stephen Norbury
   Modified Date   : 25 October 2013
   Description     : BR-4212, Debit Points fields reset: Do not reset the Debit Point Reason                     
   
*/

public class AP10_CaseTrigger{
    /**Method Throws a Validation Error when Case is closed 
       with closed status andthere are pending actions
    */
    public static Map<Id,Group> groupMap=null;
        
    /**Method creates External Reference record based on User input 
       in Refrence fields in Case while creating/ updating a Case .Also 
       validates that ExternalReference__c is mandatory if any of the 
       reference fields are populated.
    */
    public static void insertExternalReference(Map<Id,Case> newCases){         
        System.Debug('*** InsertExternalReference method in CaseAddExternalReference class Start ***');
        List<CSE_ExternalReferences__c> lstExternalReference = new List<CSE_ExternalReferences__c>();
       
        List<Case> cases = new List<Case>();
        for(Case newCase : newCases.values()){  
            /*  Already handled in Before Insert and Update Triggers
            if(newCase.ExternalReference__c == Label.CL00686){
                if(newCase.AnswerToCustomer__c == null || (newCase .AnswerToCustomer__c != null && newCase.details__c != null && newCase.AnswerToCustomer__c.contains(newCase.details__c) == false)){
                    AP33_UpdateAnswerToCustomer.addStringToAnswerToCustomer(newCase, newCase.Details__c);                        
                }
            }
            */
            
            CSE_ExternalReferences__c objExternalReference = new CSE_ExternalReferences__c();
      
            objExternalReference.Case__c = newCase.id;
            objExternalReference.ClientReference__c = newCase.ClientReference__c;
            objExternalReference.Type__c = newCase.ExternalReference__c;
            objExternalReference.SystemReference__c = newCase.SystemReference__c;
            objExternalReference.Description__c = newCase.Details__c;
            objExternalReference.OtherType__c = newCase.OtherExternalReferenceType__c;
      
            lstExternalReference.add(objExternalReference);            
        }
        
        if(lstExternalReference.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                System.Debug('######## AP10 Error Insert: '+ Label.CL00264);
        }
        else{
            Database.saveResult [] lstExtnReferenceSaveResult = Database.insert(lstExternalReference);
            for(Database.SaveResult objExtnReferenceSaveResult: lstExtnReferenceSaveResult){
                if(!objExtnReferenceSaveResult.isSuccess()){
                    Database.Error err = objExtnReferenceSaveResult.getErrors()[0];
                    System.Debug('######## AP10 Error Insert: '+err.getStatusCode()+' '+err.getMessage());              
                }
            }
        }        
        System.Debug('***InsertExternalReference method in CaseAddExternalReference Class End***');                                         
    }
          
    /*Method changes the case status based on the Spam field*/
    public static void updtCaseStatus(Map<ID,case> newCase,Map<ID,case> oldCase){
        
        System.Debug('******updtCaseStatus method in ChangeCaseStatus Class Start****');    
        for(case cases:newCase.values()){                  
            /*If Spam status is true setting case status to closed & copying 
              prior the values in TECH_CaseStatus__c field on case object
            */   
            if(cases.spam__c==true){   
                cases.TECH_CaseStatus__c=oldcase.get(cases.id).status;                                              
                cases.status=system.label.CL00325;
            }
            /*If Spam status is false copying the values from TECH_CaseStatus__c 
            field on case object to case status to closed 
           */     
            else{             
                cases.status=cases.TECH_CaseStatus__c;                                            
            }      
        }
    }
    /*
    Class Not being Used. Replaced this functionality on AP_Case_CaseHandler
    Commented on April 2014 By Vimal K
    public static void populateOwnerMap(Map<Id,Case> oldCaseMap){
        if(groupMap==null){
            Set<Id> ownerIds=new Set<Id>();
            for(Case c:oldCaseMap.values())
            ownerIds.add(c.ownerId);     
            System.debug('#ownerIds#'+ownerIds);           
            groupMap=new Map<id,Group>([select name from Group where id in :ownerIds]);        
            System.debug('#groupMap#'+groupMap);
        }
    }
    
    
    public static void updateLastAgent(Map<Id,Case> newCaseMap,Map<Id,Case> oldCaseMap){
        System.debug('##Method updateLastAgent started');
        System.debug('#newCaseMap'+newCaseMap+'\n#oldCaseMap'+oldCaseMap);
    
        populateOwnerMap(oldCaseMap);
    
        for(Case Caseobj:newCaseMap.values()){
            if(groupMap!=null && groupMap.containsKey(oldCaseMap.get(caseObj.id).ownerId)){
                System.debug('#groupMap.get(oldCaseMap.get(caseObj.id).ownerId).Name'+groupMap.get(oldCaseMap.get(caseObj.id).ownerId).Name);
          
                if(groupMap.get(oldCaseMap.get(caseObj.id).ownerId).Name == System.Label.CL00669)
                    Caseobj.LastLevel1User__c=Caseobj.OwnerId;
                else if(groupMap.get(oldCaseMap.get(caseObj.id).ownerId).Name == System.Label.CL00670)
                    Caseobj.LastLevel2User__c=Caseobj.OwnerId;
                else if(groupMap.get(oldCaseMap.get(caseObj.id).ownerId).Name == System.Label.CL00671)
                    Caseobj.LastLevel3User__c=Caseobj.OwnerId; 
            }
        }
        System.debug('#newCaseMap'+newCaseMap+'\n#oldCaseMap'+oldCaseMap);     
        System.debug('##Method updateLastAgent ends');    
    }
    */
    
/*******************************************************************************************************************
    To update the First Assignment Age of the Case (when an Agent takes the ownership of the Case for the first time)
    Added for September 2012 Release
*********************************************************************************************************************/    
/*  
    Class Not being Used. Replaced this functionality on AP_Case_CaseHandler
    Commented on April 2014 By Vimal K

    public static Boolean isUpdateAge = false;
    public static void updateFirstAssignmentAge(list<Case> lstNewCase){
        for(Case currentCase : lstNewCase){
            String Ownerid = CurrentCase.ownerid;

            If(Ownerid != null){
                if(Ownerid.startsWith('005')  && (CurrentCase.FirstAssignmentAge__c == null )){//|| CurrentCase.FirstAssignmentAge__c == 0) )
                    if(CurrentCase.Origin =='Email' || CurrentCase.Origin == 'Web'){
                        isUpdateAge = true;
                    }
                }
                if(Ownerid.startsWith('00G')  && (isUpdateAge)){//|| CurrentCase.FirstAssignmentAge__c == 0) )
                    if(CurrentCase.Origin =='Email' || CurrentCase.Origin == 'Web'){
                        CurrentCase.FirstAssignmentAge__c = null;
                    }
                }
                else if(Ownerid.startsWith('005') && isUpdateAge){
                    if(CurrentCase.Origin =='Email' || CurrentCase.Origin == 'Web'){
                        if(CurrentCase.createddate != null){    
                            Double timeDiff = (Double)system.now().getTime()-CurrentCase.createddate.getTime() ;
                            CurrentCase.FirstAssignmentAge__c = (Double)timeDiff/3600000;
                        }              
                        else{
                            CurrentCase.FirstAssignmentAge__c = 0;
                        }
                    }
                }
            }      
        }
    }
 */
/*******************************************************************************************************************
    To reset the Points,Debit Points and Reason for debit fields when the Related Contract Value Chain Player is 
    removed
*********************************************************************************************************************/    
    public static void resetFields(list<case> lstNewCases , list<Case> lstOldCases){
        for(integer i=0;i<lstNewCases.size();i++){
            if(lstNewCases[i].CTRValueChainPlayer__c == null){
                lstNewCases[i].Points__c = null;
                lstNewCases[i].DebitPoints__c = false;
                //lstNewCases[i].PointDebitReason__c = '';
                //lstNewCases[i].ContractName__c = '';
                lstNewCases[i].RelatedContract__c = null;
                lstNewCases[i].ContractType__c = '';
            }
        }
    }
/*******************************************************************************************************************
    To check if the associated Product with the Contract is Free of Cost or not based on the Case's Product
*********************************************************************************************************************/   
    public static void checkFreeProduct(list<Case> lstNewCase){
         map<Id,Case> mapCVCPCase = new map<Id,Case>();
         map<Id,Id> mapContractCVCP = new map<Id,Id>();
         map<Id,Id> mapTemplateContract = new map<Id,Id>();
         map<Id,Boolean> mapTemplateCost = new map<Id,Boolean>();
        for(Case cse : lstNewCase){
            //if(cse.DebitPoints__c == true)
                mapCVCPCase.put(cse.CTRValueChainPlayer__c,cse);
        }
        if(mapCVCPCase.size()>0){
            list<CTR_ValueChainPlayers__c> lstCVCP = [Select id,contract__c from CTR_ValueChainPlayers__c where id in : mapCVCPCase.keySet() limit 50000];
            mapContractCVCP = new map<Id,Id>();
            for(CTR_ValueChainPlayers__c cvcp : lstCVCP )
                mapContractCVCP.put(cvcp.contract__c ,  cvcp.Id );
            lstCVCP.clear();
        }
        if(mapContractCVCP.size()>0){
            list<Package__c> lstPKG = [Select id, contract__c, PackageTemplate__c , PackageTemplate__r.FreeofCost__c from package__C where contract__c in : mapContractCVCP.keySet() limit 50000];
            mapTemplateContract = new map<Id,Id>();
            mapTemplateCost = new map<Id,Boolean>();
            for(PAckage__c pkg : lstPKG ){
                mapTemplateContract.put(pkg.PackageTemplate__c , pkg.contract__c);
                mapTemplateCost.put(pkg.PackageTemplate__c , pkg.PackageTemplate__r.FreeofCost__c);
               // mapTemplatePKG.put(template.id , template.package__c);
            }
            lstPKG.clear();   
        }     
        if(mapTemplateContract.size()>0){
            list<ContractProductInformation__c> lstProduct = [Select id, packageTemplate__C,ProductFamily__c from ContractProductInformation__c where packagetemplate__C in : mapTemplateContract.keySet() limit 50000];
            map<String,Id> mapProdTemplate = new map<String,Id>();
            for(ContractProductInformation__c prod : lstProduct )
                mapProdTemplate.put(prod.ProductFamily__c  , prod.packageTemplate__C );
            
            for(ContractProductInformation__c prod : lstProduct){
                if(prod.ProductFamily__c == mapCVCPCase.get(mapContractCVCP.get(mapTemplateContract.get(mapProdTemplate.get(prod.ProductFamily__c)))).ProductFamily__c){
                    if(mapTemplateCost.get(mapProdTemplate.get(prod.ProductFamily__c))){
                        mapCVCPCase.get(mapContractCVCP.get(mapTemplateContract.get(mapProdTemplate.get(prod.ProductFamily__c)))).FreeOfCharge__c = true;  
                        if(mapCVCPCase.get(mapContractCVCP.get(mapTemplateContract.get(mapProdTemplate.get(prod.ProductFamily__c)))).DebitPoints__c == true)
                            mapCVCPCase.get(mapContractCVCP.get(mapTemplateContract.get(mapProdTemplate.get(prod.ProductFamily__c)))).DebitPoints__c = false;  
                        //mapCVCPCase.get(mapContractCVCP.get(mapTemplateContract.get(mapProdTemplate.get(prod.ProductFamily__c)))).addError('Free case as per contract');  
                    }
                    else{
                         mapCVCPCase.get(mapContractCVCP.get(mapTemplateContract.get(mapProdTemplate.get(prod.ProductFamily__c)))).FreeOfCharge__c = false;
                    }  
                    break; //added on 18-11-2013, NOV13 Release
                }
                else
                    mapCVCPCase.get(mapContractCVCP.get(mapTemplateContract.get(mapProdTemplate.get(prod.ProductFamily__c)))).FreeOfCharge__c = false;  
            }
        }
    }

/*******************************************************************************************************************
    To update Contract Name when a CVCP is added to the Case. The method is @ future since case should be updated even
    if CVCP id specified at time of insert
*********************************************************************************************************************/    
    
    public static void updateContractField(list<Case> lstCase){
        map<Id,list<Case>> mapCVCPCase = new map<id,list<Case>>();
        map<id,CTR_ValueChainPlayers__c> mapCVCPContractId = new map<id,CTR_ValueChainPlayers__c>();
        for(Case c : lstCase){
            if(mapCVCPCase.containsKey(c.CTRValueChainPlayer__c))
                mapCVCPCase.get(c.CTRValueChainPlayer__c).add(c);
            else
                mapCVCPCase.put(c.CTRValueChainPlayer__c,new list<Case>{c});
        }
        
        list<CTR_ValueChainPlayers__c> lstCVCP = [Select Contract__c , id , Contract__r.Name,Contract__r.ContractType__c ,Contract__r.SupportContractLevel__c from CTR_ValueChainPlayers__c where id in : mapCVCPCase.keySet() limit 50000];
        for(CTR_ValueChainPlayers__c cvcp : lstCVCP){
            mapCVCPContractId.put(cvcp.id,cvcp);
        }
        Map<String, CS_SupportContractLevel__c > mapCustomSetting = CS_SupportContractLevel__c.getAll();
        map<String,String> mapSupportLevel = new map<String,String>();
        for(String level : mapCustomSetting.keySet())
            mapSupportLevel.put(mapCustomSetting.get(level).SupportContractLevel__c , level);
        for(Id cvcpID : mapCVCPContractId.keySet()){
            if(mapCVCPCase.containsKey(cvcpId)){
                for(Case c: mapCVCPCase.get(cvcpID)){
                    c.RelatedContract__c = mapCVCPContractId.get(cvcpId).Contract__c;
                    c.ContractType__c = mapCVCPContractId.get(cvcpId).Contract__r.ContractType__c;
                    //c.SupportContractLevel__c = mapCVCPContractId.get(cvcpId).Contract__r.SupportContractLevel__c;
                    if(mapSupportLevel.get(c.SupportContractLevel__c) > mapSupportLevel.get(mapCVCPContractId.get(cvcpId).Contract__r.SupportContractLevel__c) || c.SupportContractLevel__c == null)
                        c.SupportContractLevel__c = mapCVCPContractId.get(cvcpId).Contract__r.SupportContractLevel__c;
                }
            }
        }
    }

    /*******************************************************************************************************************
    To update the Case Contact (when a case originates from web)
    Added for May 2013 Release
    *********************************************************************************************************************/    
    public static void updateContact(List<Case> lstNewCase){
        try{
            if(lstNewCase != null && lstNewCase.size() == 1 && lstNewCase[0].Origin == 'Web'){
                List<List<Sobject>> searchEmailResults = new List<List<Sobject>>();//To hold the SOSL Email search results
                List<List<Sobject>> searchPhoneResults = new List<List<Sobject>>();//To hold the SOSL Phone search results
                Set<Contact> matchedContacts = new Set<Contact>();//List of matched contacts merged from the SOSL search results
                Case currentCase = lstNewCase[0];
                
                //Search on all Email fields
                if(currentCase.SuppliedEmail != null){
                  //<BR-3139 Release:Oct2013> 
                  //  searchEmailResults = [find :currentCase.SuppliedEmail IN EMAIL FIELDS RETURNING Contact];
                  //  searchEmailResults = [find :currentCase.SuppliedEmail IN EMAIL FIELDS RETURNING Contact (id WHERE ToBeDeleted__c = false )];
                  //  Contacts associated with PRM Account's should not be searched
                      searchEmailResults = [find :currentCase.SuppliedEmail IN EMAIL FIELDS RETURNING Contact (id WHERE ToBeDeleted__c = false  AND Account.RecordTypeId !=:System.Label.CLJUL15CCC01)];

                  //</BR-3139>  
                    if(searchEmailResults.size() > 0){
                        for(Contact contactRecord : (list<Contact>)searchEmailResults[0])
                            matchedContacts.add(contactRecord);
                    }
                }
                
                //<Phone No check removed as part of BR-7961>
                //Search on all Phone fields
                /*  if(currentCase.SuppliedPhone != null){
                 //<BR-3139 Release:Oct2013> 
                 //searchPhoneResults = [find :currentCase.SuppliedPhone IN PHONE FIELDS RETURNING Contact];
                    searchPhoneResults = [find :currentCase.SuppliedPhone IN PHONE FIELDS RETURNING Contact (id WHERE ToBeDeleted__c = false )];
                 //</BR-3139>  
                    if(searchPhoneResults.size() > 0){
                        for(Contact contactRecord : (list<Contact>)searchPhoneResults[0])
                            matchedContacts.add(contactRecord);
                    }
                } */
                System.debug('########################       Matched Contacts Count: ' + matchedContacts.size() + '        ###############################');
                //Verify if a single unique contact is identified
                if(matchedContacts.size() == 1){
                    System.debug('########################       Assigning Contacts       ########################');
                    //SET is an unordered collection. Hence loop and read the value although it contains a single contact object
                    for(Contact contactRecord : matchedContacts){
                        currentCase.ContactId = contactRecord.Id ;
                    }
                }
            }
        }
        catch(Exception ex){
            String strErrorMessage = ex.getTypeName()+' - '+ ex.getMessage();
            System.debug(strErrorMessage);
        }
    }
    
    //Added by Anil B (Q1/16 Release)
    //#Nov16 Release - Tailored Care BR-10699
    public static void vipIconEnhancment(List<Case> newCases){
        Set <Id> AccountIds        = new Set <Id>();
        Set <Id> ContactIds        = new Set <Id>();
        Map <Id,     Account>             AccountMap            = new Map <Id, Account>();
        Map <Id,     Contact>             ContactMap            = new Map <Id, Contact>();
        
        for (Case c : newCases){
            if (c.AccountId    != null) { AccountIds.add (c.AccountId); }
            if (c.AssetOwner__c!= null) { AccountIds.add (c.AssetOwner__c);}
            if (c.ContactId    != null) { ContactIds.add (c.ContactId);}
        }
        
        //#Nov16 Release - Tailored Care 
        if(AccountIds.size()>0){
            AccountMap = new Map<Id, Account>([Select Id, Account_VIPFlag__c, TailoredCare__c from Account where Id in :AccountIds]);
        }
        if(ContactIds.size()>0){
            ContactMap = new Map<Id, Contact>([select Id, AccountID, Account.Account_VIPFlag__c, Account.TailoredCare__c from Contact where Id in :ContactIds]);
        }
        
        for (Case c : newCases){
            if (c.ContactId!=null){
                if(ContactMap.containsKey(c.ContactId)){
                    
                    if(ContactMap.get(c.ContactId).Account.Account_VIPFlag__c == 'VIP1' ){
                         c.TECH_VIPStatus__c='VIP1';
                    }                    
                    else if(c.AssetOwner__c!=null){
                        if(AccountMap.containsKey(c.AssetOwner__c)){
                            c.TECH_VIPStatus__c = AccountMap.get(c.AssetOwner__c).Account_VIPFlag__c;
                        }
                    }
                    else{
                        c.TECH_VIPStatus__c = ContactMap.get(c.ContactId).Account.Account_VIPFlag__c;
                    }                                        
                    
                    //#Nov16 Release - Tailored Care
                    if(String.isEmpty(ContactMap.get(c.ContactId).Account.TailoredCare__c)){
                        c.TECH_TCStatus__c = ContactMap.get(c.ContactId).Account.TailoredCare__c;    
                    }
                    else if(c.AssetOwner__c!=null && AccountMap.containsKey(c.AssetOwner__c)){
                        c.TECH_TCStatus__c = AccountMap.get(c.AssetOwner__c).TailoredCare__c;
                    }                         
                }
            }
            else if(c.AssetOwner__c!=null ){
                if(AccountMap.containsKey(c.AssetOwner__c)){
                    c.TECH_VIPStatus__c = AccountMap.get(c.AssetOwner__c).Account_VIPFlag__c;
                    
                    //#Nov16 Release - Tailored Care 
                    c.TECH_TCStatus__c = AccountMap.get(c.AssetOwner__c).TailoredCare__c;
                }
            }
            else{
                c.TECH_VIPStatus__c='';
                
                //#Nov16 Release - Tailored Care 
                c.TECH_TCStatus__c='';
            }
        }
    }
    
    public static void legacySiteIdPopulation(List<Case> newCases){
        Map<Id,Case> IdCaseMap = new Map<Id,Case>();
        for(Case cseObj : newCases){
           IdCaseMap.Put(cseObj.site__c,cseObj);
        }
        Map<Id,GCSSite__c> idGcsSiteMap  = new Map<Id,GCSSite__c>( [Select id,LegacySiteId__c from GCSSite__c Where Id In :IdCaseMap.keySet()]);
        for(Case cseObj : newCases){
           cseObj.TECH_LegacySiteID__c = idGcsSiteMap.get(cseObj.Site__c).LegacySiteId__c;
        }
    } 
}