global class SVMX_UpdateTeamOnExpertiseAndProdServcd
{
    webservice static SVMXC.INTF_WebServicesDef.INTF_PageData SVMX_SampleMethod(SVMXC.INTF_WebServicesDef.INTF_TargetRecord reqTargetRec)
    {
        system.debug('reqTargetRec=='+reqTargetRec);
        SVMXC__Service_Group_Members__c objTechnician;
        SVMXC.INTF_WebServicesDef.INTF_TargetRecordObject TargetRecordObjectForHdr = new SVMXC.INTF_WebServicesDef.INTF_TargetRecordObject();
        List<SVMXC.INTF_WebServicesDef.INTF_Record> objHeaderRecordLst = new List<SVMXC.INTF_WebServicesDef.INTF_Record>();
        List<SVMXC.INTF_WebServicesDef.INTF_StringMap> lstObjHdrTargetRecordAsKeyValue = new List<SVMXC.INTF_WebServicesDef.INTF_StringMap>();
        SVMXC.INTF_WebServicesDef.INTF_PageData objPageDataResp = new SVMXC.INTF_WebServicesDef.INTF_PageData();
        
        if(reqTargetRec != NULL && reqTargetRec.headerRecord != NULL)
            TargetRecordObjectForHdr = reqTargetRec.headerRecord;
        
        if(TargetRecordObjectForHdr.records != NULL && TargetRecordObjectForHdr.records.size() > 0)
            objHeaderRecordLst = TargetRecordObjectForHdr.records;
        
        if(objHeaderRecordLst.size() > 0 && objHeaderRecordLst[0].targetRecordId != NULL && objHeaderRecordLst[0].targetRecordId.length() > 0 )
            objTechnician = [Select Id, Name, SVMXC__Service_Group__c, SVMXC__Service_Group__r.Name From SVMXC__Service_Group_Members__c Where id =: objHeaderRecordLst[0].targetRecordId];
        
        system.debug('objTechnician: ' + objTechnician);
        
        if(objTechnician != NULL && reqTargetRec.detailRecords != NULL && reqTargetRec.detailRecords.size() > 0)   
        {
            for(SVMXC.INTF_WebServicesDef.INTF_TargetRecordObject currDtlTargetRecordObject : reqTargetRec.detailRecords)
            {
                if(currDtlTargetRecordObject.records != NULL && currDtlTargetRecordObject.records.size() > 0)
                {
                    if(currDtlTargetRecordObject.records != NULL && currDtlTargetRecordObject.records.size() > 0)
                    {
                        for(SVMXC.INTF_WebServicesDef.INTF_Record currRec : currDtlTargetRecordObject.records)
                        {
                            if(currRec.targetRecordAsKeyValue != NULL && currRec.targetRecordAsKeyValue.size() > 0)
                            {
                                SVMXC.INTF_WebServicesDef.INTF_StringMap strMapTeam = new SVMXC.INTF_WebServicesDef.INTF_StringMap();
                                strMapTeam.key = 'SVMXC__Service_Group__c';
                                strMapTeam.value = objTechnician.SVMXC__Service_Group__c;
                                strMapTeam.value1 = objTechnician.SVMXC__Service_Group__r.Name;
                                currRec.targetRecordAsKeyValue.add(strMapTeam);
                                
                                SVMXC.INTF_WebServicesDef.INTF_StringMap strMapTech = new SVMXC.INTF_WebServicesDef.INTF_StringMap();
                                strMapTech.key = 'SVMXC__Group_Member__c';
                                strMapTech.value = objTechnician.Id;
                                strMapTech.value1 = objTechnician.Name;
                                currRec.targetRecordAsKeyValue.add(strMapTech);
                            }
                        }   
                   }   
                }
            }
        }
        
        objPageDataResp = SVMXC.INTF_WebServicesDef.INTF_BuildPageData_WS(reqTargetRec);
        objPageDataResp.response.success = true;
        return objPageDataResp;
    }
}