/********************************************************************
* Company: Fielo
* Created Date: 25/08/2016
* Description:
********************************************************************/
public class FieloPRM_FieldsML{
    public String fieldAttachment{get; set;}
    public String fieldLink{get; set;}
    public String fieldDescription{get; set;}
    public String fieldOverlayHTML{get; set;}
    public String fieldOverlayText{get; set;}
}