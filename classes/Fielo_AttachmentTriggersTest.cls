/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Created Date: 19/08/2014                                  *
************************************************************/
@isTest
public with sharing class Fielo_AttachmentTriggersTest {
    public static testMethod void testUnit(){
        
        Fielo_LoyaltyEligibleProduct__c prod = new Fielo_LoyaltyEligibleProduct__c();
        insert prod;
        
        Blob b = Blob.valueOf('Test Data');
        
        Attachment att = new Attachment(Name = 'Att', Body = b, ParentId = prod.Id );
        insert att;
    }
}