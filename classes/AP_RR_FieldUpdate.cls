public class AP_RR_FieldUpdate{

  Public static void updateRRStatus(Set<Id> RRIds)
  {
      
      Map<Id, List<RMA_Product__c>> mapRRIdRIs = new map<Id, List<RMA_Product__c>>();
      map<id, String> mapRRIdStatus = new map<id, String>();
      
      List<RMA_Product__c> lstRIs = [Select id, RMA__c, ApprovalStatus__c from RMA_Product__c where RMA__c in: RRIds];
      for(RMA_Product__c RI: lstRIs)
      {
         if(mapRRIdRIs.containskey(RI.RMA__c))
         {
            mapRRIdRIs.get(RI.RMA__c).add(RI);
         }
         else
         {
           mapRRIdRIs.put(RI.RMA__c, new List<RMA_Product__c>());
           mapRRIdRIs.get(RI.RMA__c).add(RI);
         }
      }
      
      for(Id RRId: mapRRIdRIs.keyset())
      {
         Boolean ApproveFlag = False;
         Boolean RejectFlag = False;
         Boolean PendingFlag = False;
          Boolean NoChangeFlag = False;
      
         List<RMA_Product__c> tmpRIs = mapRRIdRIs.get(RRId);
         for(RMA_Product__c RI: tmpRIs )
         {
            If( RI.ApprovalStatus__c == Label.CLMAY13RR07)
            {
               ApproveFlag = True;
            }
            else if (RI.ApprovalStatus__c == Label.CLMAY13RR14 || RI.ApprovalStatus__c == Label.CLOCT13RR57)
            {
               RejectFlag = True;
            }
            else if (RI.ApprovalStatus__c == Label.CLOCT13RR71)
            {
                PendingFlag = True;
            }
            else {NoChangeFlag  = True;}
         }
         
         system.debug('## ApproveFlag  ##'+ ApproveFlag);
         system.debug('## RejectFlag  ##'+ RejectFlag);
         system.debug('## PendingFlag  ##'+ PendingFlag);
         
         If (ApproveFlag == True  && RejectFlag == False && PendingFlag == False && NoChangeFlag == False )
         {
            mapRRIdStatus.put(RRId, Label.CLMAY13RR12); //validated
         }
         else if(RejectFlag == True && ApproveFlag == False && PendingFlag == False && NoChangeFlag == False)
         {
             mapRRIdStatus.put(RRId, Label.CLMAY13RR14); //rejected
         }
         else if(RejectFlag == True && ApproveFlag == True && PendingFlag == False && NoChangeFlag == False)
         {
             mapRRIdStatus.put(RRId, Label.CLMAY13RR12);  // Approved
         }
         else if((PendingFlag = True && NoChangeFlag == False && (ApproveFlag == True  || RejectFlag == True)) ||(PendingFlag = True && NoChangeFlag == False && (ApproveFlag == False  || RejectFlag == False)))
         {
             mapRRIdStatus.put(RRId,Label.CLMAY13RR13); //submitted
         }
      }
      
      List<RMA__c> lstRRs = [Select id,TEX__c, Status__c from RMA__c where id in: mapRRIdStatus.keyset() ];
     // updatingMaterialStatusOnTEX(lstRRs);
      List<RMA__c> lstReturnRequests =  new List<RMA__c>();
      for (RMA__c RR: lstRRs)
      {
         if(mapRRIdStatus.containskey(RR.id))
         {
            String RRStatus = mapRRIdStatus.get(RR.id);
            RR.Status__c = RRStatus;
            lstReturnRequests.add(RR);
         }
      }
      system.debug('## lstReturnRequests ##'+ lstReturnRequests);
      Update lstReturnRequests;
  }
  
  public static void updatingMaterialStatusOnTEX(List<RMA__c> ListOfRMA)
{
        System.debug('~~~~~~~~~ ListOfRMAItems:' + ListOfRMA);
        
        set<string> setTex=new set<string>();
        set<string> setTexFinal=new set<string>(); 
        for( RMA__c tempRR:ListOfRMA)
            {
                //if(tempRR.TEX__c !=null && tempRR.Status__c==Label.CLMAY13RR14 )
                //    {
                     setTex.add(tempRR.TEX__c);
                 //   }
            }
             System.debug('~~~~~~~~~ setTex:' + setTex);
        List<RMA__c> lstRRs = [Select id,TEX__c, Status__c from RMA__c where TEX__c in: setTex ];
            System.debug('~~~~~~~~~ lstRRs :' + lstRRs );
         for(RMA__c RR:lstRRs)
            {
              if(RR.Status__c!=Label.CLMAY13RR14)
                {
                 if(setTex.contains(RR.TEX__c)){
                 setTex.remove(RR.TEX__c);
                 }
                }
            }
            System.debug('~~~~~~~~~ setTexFinal:' + setTex);
            if(setTex.size()>0)
            {
                List<TEX__c> listTexFinal=new List<TEX__c>();
                List<TEX__c>listofTex=[SELECT ID,Material_Status__c from TEX__c where id in:setTex];
                System.debug('~~~~~~~~~ listofTex:' + listofTex);
                for(TEX__c temTEX:listofTex)
                    {
                        temTEX.Material_Status__c='';
                        listTexFinal.add(temTEX);
                    }
                    System.debug('~~~~~~~~~ listTexFinal:' + listTexFinal);
            update listTexFinal;  
            System.debug('~~~~~~~~~ listTexFinalAFTER:' + listTexFinal); 
            }
    } 
    //The below two methods created by Ram Chilukuri as part of April Release 2014 (connectors)
 public static void updateRequestTypeOnRi(List<RMA_Product__c> ListOfRi)
{
 Map<id,string> mapRiIdCR= new Map<Id,string>();
  Map<string,string> mapofCRType= new Map<string,string>();
// List<RMA_Product__c> ListOfRiNew=new  List<RMA_Product__c>();
map<string,RMA_Product__c> mapGmrcodeNRi=new map<string,RMA_Product__c>();
 for(RMA_Product__c Ri:ListOfRi)
    {
        mapRiIdCR.put(Ri.id,Ri.CustomerResolution__c);
        mapGmrcodeNRi.put(Ri.Tech_GMRCode__c,RI);
    }
     System.debug('~~~~~~~~~ mapGmrcodeNRi:' + mapGmrcodeNRi);
    if(mapGmrcodeNRi.size()>0){updateOppProductFielsOnRI(mapGmrcodeNRi);}
    
    System.debug('~~~~~~~~~ mapRiIdCR:' + mapRiIdCR);
    List<ResolutionOptionAndCapabilities__c> lstROC = [Select id, CustomerResolution__c, Type__c from ResolutionOptionAndCapabilities__c where CustomerResolution__c in: mapRiIdCR.values()];
        for(ResolutionOptionAndCapabilities__c cr:lstROC)
        {
         mapofCRType.put(cr.CustomerResolution__c,cr.Type__c);
        }
        System.debug('~~~~~~~~~ lstROC:' + lstROC);
        System.debug('~~~~~~~~~ mapofCRType:' + mapofCRType);
    for(RMA_Product__c tRi:ListOfRi)
    {
        tRi.TECH_RequestType__c= mapofCRType.get(mapRiIdCR.get(tRi.id));
        //ListOfRiNew.add(tRi);
    }
}
public static void updateRequestTypeOnRR(List<RMA_Product__c> ListOfRi)
{
    List<RMA__c> ListRMANew=new List<RMA__c>();
 map<string,string> mapRRids=new map<string,string>();
 for(RMA_Product__c Ri:ListOfRi)
    {
     mapRRids.put(Ri.RMA__c,Ri.TECH_RequestType__c);
    }
    System.debug('~~~~~~~~~ mapRRids:' + mapRRids);
    List<RMA__c> objRMA = [Select id, TECH_RequestType__c,  AccountCountry__r.Name  from RMA__c where id in:mapRRids.keyset()];
    System.debug('~~~~~~~~~ objRMA:' + objRMA);
    for(RMA__c rma:objRMA)
        {
            if(rma.TECH_RequestType__c==null)
                {
                //rma.BackOfficesystem__c=rma.AccountCountry__r.FrontOfficeOrganization__r.DefaultBackOfficeSystem__c;
                rma.CountryofBackOfficeSystem__c=rma.AccountCountry__r.Name;
                 rma.TECH_RequestType__c=mapRRids.get(rma.id);
                 ListRMANew.add(rma);
                }
        }
            System.debug('~~~~~~~~~ ListRMANew:' + ListRMANew);
    update ListRMANew;
}
 public static void updateRequestTypeOnRRDelete(List<RMA_Product__c> ListOfRi)
{
System.debug('~~~~~~~~~ ListOfRi:' + ListOfRi);
List<RMA__c> ListRMANew=new List<RMA__c>();
 set<string> setRRids=new set<string>();

 for(RMA_Product__c Ri:ListOfRi)
    {
     setRRids.add(Ri.RMA__c);
    }
    System.debug('~~~~~~~~~ setRRids:' + setRRids);
    List<RMA__c> objRMA = [Select id, TECH_RequestType__c from RMA__c where id in:setRRids];
    List<RMA_Product__c> objRI = [Select id, RMA__c from RMA_Product__c where RMA__c in:setRRids];
    for(RMA_Product__c tRi:objRI)
    {
     if (setRRids.contains(tRi.RMA__c))
        {
         setRRids.remove(tRi.RMA__c);
        }
    }
    System.debug('~~~~~~~~~ objRMA:' + objRMA);
    System.debug('~~~~~~~~~ objRI:' + objRI);
    for(RMA__c rma:objRMA)
        {
            if(setRRids.contains(rma.id))
                {
                 rma.TECH_RequestType__c=null;
                 ListRMANew.add(rma);
                }
        }
            System.debug('~~~~~~~~~ ListRMANew:' + ListRMANew);
    update ListRMANew;
}

/*Oct 15 Rel -- Divya M
        BR-7721 --> Add a Shipped From Customer field on the Return Request object.
The field will be of type Date, and it will automatically be calculated based on the latest Shipped From Customer date of the related Return Items and TEX. */
    public static void updateShippedFromCustomer(List<RMA_Product__c> ListOfRI){
        list<RMA__c> lstRMAtoUpdate = new list<RMA__c>();

        set<id> setRRids = new set<id>();
        for(RMA_Product__c RI:ListOfRI){
            setRRids.add(RI.RMA__c);
        }

        list<RMA__c> lstRMA = [Select id, ShippedFromCustomer__c from RMA__c where id in: setRRids];
        List<RMA_Product__c> lstRI = [Select id, RMA__c , RMA__r.TEX__c, ShippedFromCustomer__c from RMA_Product__c where RMA__c in:setRRids ORDER BY ShippedFromCustomer__c DESC];
        
        set<id> TEXIds = new set<id>();
        for(RMA_Product__c ri : lstRI){
            if(ri.RMA__r.TEX__c!=null){
                TEXIds.add(ri.RMA__r.TEX__c);
            }
        }
        for(RMA__c rr:lstRMA){
            list<Date> lstDate = new list<Date>();
            /*if(rr.ShippedFromCustomer__c != null){
                date dt = rr.ShippedFromCustomer__c.date();
                lstDate.add(dt);
            }*/
            for(RMA_Product__c ri : lstRI){
                if(ri.RMA__c == RR.ID && ri.ShippedFromCustomer__c != null){
                    lstDate.add(ri.ShippedFromCustomer__c);
                }               
            }
            lstDate.sort();
            if(lstDate.size()>=1){
                if(rr.ShippedFromCustomer__c !=null && rr.ShippedFromCustomer__c != lstDate[lstDate.size()-1]){
                    rr.ShippedFromCustomer__c = lstDate[lstDate.size()-1];
                    lstRMAtoUpdate.add(rr);
                } else if(rr.ShippedFromCustomer__c ==null){
                    rr.ShippedFromCustomer__c = lstDate[lstDate.size()-1];
                    lstRMAtoUpdate.add(rr);
                }
            }
        }
        if(lstRMAtoUpdate.size()>0){
            update lstRMAtoUpdate;
        }
        List<RMA__c> lstRR = [Select id, TEX__c , ShippedFromCustomer__c from RMA__c where TEX__c in:TEXIds ORDER BY ShippedFromCustomer__c DESC];
        list<TEX__c> lstTEX = [Select id, ShippedFromCustomer__c,ExpertAssessmentStarted__c from TEX__c where id in: TEXIds];
        list<TEX__c> lstTEXtoUpdate = new list<TEX__c>();
        for(TEX__c tex:lstTEX){
            list<Date> lstDate = new list<Date>();
            if(tex.ShippedFromCustomer__c != null){
                lstDate.add(tex.ShippedFromCustomer__c);
            }
            for(RMA__c rr : lstRR){
                if(rr.TEX__c == tex.ID && rr.ShippedFromCustomer__c != null){
                    lstDate.add(rr.ShippedFromCustomer__c);
                }               
            }
            lstDate.sort();
            system.debug('list of Return Request SFC dates are ==>'+lstDate);
            integer x;
            list<date> dateset = new list<date>();
            if(lstDate.size()>0){
                if(tex.ShippedFromCustomer__c !=null && tex.ShippedFromCustomer__c != lstDate[lstDate.size()-1]){
                    if(tex.ExpertAssessmentStarted__c != null && lstDate[lstDate.size()-1] > tex.ExpertAssessmentStarted__c){
                        for(x=lstDate.size()-1;x!=0;x--){
                            system.debug('list date value is ==>'+lstDate[x]+'== ExpertAssessmentStarted is ===>'+tex.ExpertAssessmentStarted__c);
                            if(!(lstDate[x]>tex.ExpertAssessmentStarted__c)){
                                dateset.add(lstDate[x]);//.date()
                        system.debug('updated field value is-->'+dateset);
                            }
                        }
                        system.debug('size of set is==>'+dateset.size());
                        if(dateset.size()>0){
                        dateset.sort();
                        tex.ShippedFromCustomer__c = dateset[dateset.size()-1];//datetime.newInstance(dateset[dateset.size()-1].year(), dateset[dateset.size()-1].month(),dateset[dateset.size()-1].day());//dateset[dateset.size()-1].DateTIme();
                        lstTEXtoUpdate.add(tex);
                        system.debug('updated field value is when it is greater-->'+tex.ShippedFromCustomer__c); 
                        }
                    }else{                      
                        tex.ShippedFromCustomer__c = lstDate[lstDate.size()-1];
                        lstTEXtoUpdate.add(tex);
                    }
                } else if(tex.ShippedFromCustomer__c ==null){
                    if(lstDate.size()>=1){
                    if(tex.ExpertAssessmentStarted__c != null && lstDate[lstDate.size()-1]>tex.ExpertAssessmentStarted__c){
                        for(x=lstDate.size()-1;x!=0;x--){
                            if(!(lstDate[x]>tex.ExpertAssessmentStarted__c)){
                                dateset.add(lstDate[x]);//.date()
                        system.debug('updated field value is-->'+dateset);
                            }
                        }
                        system.debug('size of set is==>'+dateset.size());
                        dateset.sort();
                        if(dateset.size()>0){
                        tex.ShippedFromCustomer__c = dateset[dateset.size()-1]; //datetime.newInstance(dateset[dateset.size()-1].year(), dateset[dateset.size()-1].month(),dateset[dateset.size()-1].day());//dateset[dateset.size()-1].DateTIme();
                        lstTEXtoUpdate.add(tex);
                        }
                        system.debug('updated field value is when it is greater-->'+tex.ShippedFromCustomer__c); 
                        
                    }else{
                        tex.ShippedFromCustomer__c = lstDate[lstDate.size()-1];
                        lstTEXtoUpdate.add(tex);
                    }
                    }
                }
            }
        }
        if(lstTEXtoUpdate.size()>0){
            update lstTEXtoUpdate;
        }           
    }
    public static void updateOppProductFielsOnRI(map<string,RMA_Product__c> mapGmrcodeNRi){
    list<RMA_Product__c> newRIlist=new list<RMA_Product__c>();
    
    List<OPP_Product__c> lstOPPProduct = new List<OPP_Product__c>([Select Id, Name, BusinessUnit__c,ProductLine__c,ProductFamily__c,Family__c, TECH_PM0CodeInGMR__c, HierarchyType__c From OPP_Product__c where TECH_PM0CodeInGMR__c IN :mapGmrcodeNRi.keyset()]);
     if(lstOPPProduct.size()>0){
     for(OPP_Product__c tOppProduct:lstOPPProduct){  
            if(mapGmrcodeNRi.containskey(tOppProduct.TECH_PM0CodeInGMR__c)){
              RMA_Product__c tRi=mapGmrcodeNRi.get(tOppProduct.TECH_PM0CodeInGMR__c);
              tRi.ProductLine__c=tOppProduct.ProductLine__c;
              newRIlist.add(tRi);
            }
     }
     if(newRIlist.size()>0){
    // update newRIlist;
     }
     
     }
    
    }
    public static void updateTEXShippedFromCustomer(List<RMA__c> ListOfRR){
        list<TEX__c> lstTEXtoUpdate = new list<TEX__c>();
        set<id> setTEXids = new set<id>();
        for(RMA__c RR:ListOfRR){
            setTEXids.add(RR.TEX__c);
        }
        list<TEX__c> lstTEX = [Select id, ShippedFromCustomer__c,ExpertAssessmentStarted__c from TEX__c where id in: setTEXids];
        List<RMA__c> lstRR = [Select id, TEX__c , ShippedFromCustomer__c from RMA__c where TEX__c in:setTEXids ORDER BY ShippedFromCustomer__c DESC];
        for(TEX__c tex:lstTEX){
            list<Date> lstDate = new list<Date>();
            if(tex.ShippedFromCustomer__c != null){
               lstDate.add(tex.ShippedFromCustomer__c);
            }
            for(RMA__c rr : lstRR){
                if(rr.TEX__c == tex.ID && rr.ShippedFromCustomer__c != null){
                    lstDate.add(rr.ShippedFromCustomer__c);
                }               
            }
            lstDate.sort();
            system.debug('list of Return Request SFC dates are ==>'+lstDate);
            integer x=0;
            list<date> dateset = new list<date>();
            if(lstDate.size()>=1){
                if(tex.ShippedFromCustomer__c !=null && tex.ShippedFromCustomer__c != lstDate[lstDate.size()-1]){
                    if(tex.ExpertAssessmentStarted__c != null && lstDate[lstDate.size()-1] > tex.ExpertAssessmentStarted__c){
                        for(x=lstDate.size()-1;x!=0;x--){
                            system.debug('list date value is ==>'+lstDate[x]+'== ExpertAssessmentStarted is ===>'+tex.ExpertAssessmentStarted__c);
                            if(!(lstDate[x]>tex.ExpertAssessmentStarted__c)){
                                dateset.add(lstDate[x]);
                        system.debug('updated field value is-->'+dateset);
                            }
                        }
                        system.debug('size of set is==>'+dateset.size());
                        if(dateset.size()>0){
                        dateset.sort();
                        tex.ShippedFromCustomer__c = dateset[dateset.size()-1];//datetime.newInstance(dateset[dateset.size()-1].year(), dateset[dateset.size()-1].month(),dateset[dateset.size()-1].day());//dateset[dateset.size()-1].DateTIme();
                        lstTEXtoUpdate.add(tex);
                        system.debug('updated field value is when it is greater-->'+tex.ShippedFromCustomer__c); 
                        }
                    }else{
                        
                        tex.ShippedFromCustomer__c = lstDate[lstDate.size()-1];
                        lstTEXtoUpdate.add(tex);
                    }
                    
                } else if(tex.ShippedFromCustomer__c ==null){
                    if(lstDate.size()>=1){
                    if(tex.ExpertAssessmentStarted__c != null && lstDate[lstDate.size()-1]>tex.ExpertAssessmentStarted__c){
                        for(x=lstDate.size()-1;x!=0;x--){
                            if(!(lstDate[x]>tex.ExpertAssessmentStarted__c)){
                                dateset.add(lstDate[x]);//.date()
                        system.debug('updated field value is-->'+dateset);
                            }
                        }
                        system.debug('size of set is==>'+dateset.size());
                        dateset.sort();
                        if(dateset.size()>0){
                        tex.ShippedFromCustomer__c = dateset[dateset.size()-1];//datetime.newInstance(dateset[dateset.size()-1].year(), dateset[dateset.size()-1].month(),dateset[dateset.size()-1].day());//dateset[dateset.size()-1].DateTIme();
                        lstTEXtoUpdate.add(tex);
                        }
                        system.debug('updated field value is when it is greater-->'+tex.ShippedFromCustomer__c); 
                        
                    }else{
                        
                        tex.ShippedFromCustomer__c = lstDate[lstDate.size()-1];
                        lstTEXtoUpdate.add(tex);
                    }
                    }
                }
            }
        }
        system.debug('list of TEX ===>'+lstTEXtoUpdate);
        if(lstTEXtoUpdate.size()>0){
            update lstTEXtoUpdate;
        }
    }
    
    // ends
}