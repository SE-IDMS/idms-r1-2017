/*
    Pooja Suresh
    Release     : Jul16 Release (BR-10118) - 
    Test Class for automated ownership re-assignment for accounts owned by external users : AP_ReassignExternalAccountOwner
 */
@isTest
private class AP_ReassignExternalAccountOwner_TEST
{    
    static testmethod void myUnitTest()
    {
        Id agentProfileId = [SELECT id FROM Profile WHERE Name='SE - Agent (Global - Community)'].id;
        Id agentPermSetId = [SELECT Id FROM PermissionSet WHERE Name='SA_Sales_Agent_Global' limit 1].Id;
        
        List<CS_StateCountyMapping__c> custSett = new List<CS_StateCountyMapping__c>();
        custSett.add(new CS_StateCountyMapping__c(Name='PD',County__c='ES43'));
        custSett.add(new CS_StateCountyMapping__c(Name='MN',County__c='ES48'));
        insert custSett;
        System.debug('custSett---->>>'+custSett);
        
        Country__c country = Utils_TestMethods.createCountry();
        country.CountryCode__c='IT';
        insert country;
        
        Country__c spain = Utils_TestMethods.createCountry();
        spain.CountryCode__c='ES';
        insert spain;
        
        StateProvince__c stateProv1 = new StateProvince__c(Name='Padova', Country__c=country.Id, CountryCode__c='IT', StateProvinceCode__c='PD');
        insert stateProv1;        
        StateProvince__c stateProv2 = new StateProvince__c(Name='Vizcaya', Country__c=spain.Id, CountryCode__c='ES', StateProvinceCode__c='48');
        insert stateProv2;
        
        Account communityAccount = Utils_TestMethods.createAccount(userInfo.getUserId(),country.id);
        communityAccount.City__c='test city';
        communityAccount.Street__c='test street';
        communityAccount.ZipCode__c='34534';
        insert communityAccount;
        
        Contact communityContact = new Contact(FirstName='Test',LastName='Agent',AccountId=communityAccount.Id);
        communityContact.Country__c=country.id;
        communityContact.Email='test@se.agent';
        insert communityContact;
        
       
        AccountOwnershipRule__c rule1 = new AccountOwnershipRule__c(ClassLevel1__c='RT', Country__c=country.id, County__c='ES43', AccountOwner__c=UserInfo.getUserId(), Functionality__c='EAO');  // For StateCode=PD
        insert rule1;
        AccountOwnershipRule__c rule2 = new AccountOwnershipRule__c(ClassLevel1__c='RT', Country__c=country.id, County__c='ES48', AccountOwner__c=UserInfo.getUserId(), Functionality__c='EAO');  // For StateCode=MN
        insert rule2;
                       
        User agent = new User(alias = 'test123', email='test123@noemail.com', emailencodingkey='UTF-8', lastname='Agent', languagelocalekey='en_US',
                localesidkey='en_US', profileid = agentProfileId, country='IT', IsActive=true, ContactId = communityContact.Id,
                timezonesidkey='America/Los_Angeles', username='sesa123321@noemail.com');
        insert agent;
        
        System.runAs(new User(Id = UserInfo.getUserId()))
        {
            PermissionSetAssignment agentAssignPermSet = new PermissionSetAssignment(PermissionSetId = agentPermSetId, AssigneeId = agent.Id);
            insert agentAssignPermSet;
            
        }
        
        System.runAs(agent)
        {
            Account agentAcc1 = Utils_TestMethods.createAccount(userInfo.getUserId(),country.id);
            agentAcc1.City__c='test city111';
            agentAcc1.Street__c='test street';
            agentAcc1.ZipCode__c='34534';
            agentAcc1.StateProvince__c=stateProv1.id;
            agentAcc1.ClassLevel1__c='RT';
            insert agentAcc1;
            Account agentAcc2 = Utils_TestMethods.createAccount(userInfo.getUserId(),country.id);
            agentAcc2.City__c='test city1143';
            agentAcc2.Street__c='test street23';
            agentAcc2.ZipCode__c='3453421';
            agentAcc2.ClassLevel1__c='RT';
            insert agentAcc2;
            Account agentAcc3 = Utils_TestMethods.createAccount(userInfo.getUserId(),spain.id);
            agentAcc3.City__c='test city113121';
            agentAcc3.Street__c='test street3241';
            agentAcc3.ZipCode__c='3452134';
            agentAcc3.StateProvince__c=stateProv2.id;
            agentAcc3.ClassLevel1__c='RT';
            insert agentAcc3;
        }
               
    }
    
}