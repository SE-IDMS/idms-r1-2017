@isTest
private class VFC_LocationCockpit_TEST {

    static testMethod void myUnitTest() {
        // create records for test
        Account a1 = Utils_TestMethods.createAccount();
        insert a1;
        SVMXC__Site__c loc = new SVMXC__Site__c(SVMXC__Zip__c ='90909090',SVMXC__Street__c ='ABC',SVMXC__State__c ='ABC',SVMXC__Country__c ='ABC',SVMXC__City__c ='ABC',SVMXC__Account__c=a1.Id);
        insert loc;    
        SVMXC__Site__c loc1 = new SVMXC__Site__c(SVMXC__Zip__c ='90909090',SVMXC__Street__c ='ABC',SVMXC__State__c ='ABC',SVMXC__Country__c ='ABC',SVMXC__City__c ='ABC',SVMXC__Account__c=a1.Id,SVMXC__Parent__c=loc.id);
        insert loc1;    
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c(Name = 'testIp', SVMXC__Site__c = loc.Id);
        insert ip1;
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(Name = 'testIp', SVMXC__Site__c = loc1.Id);
        insert ip2;

        PageReference pageRef=Page.VFP_LocationCockpit;
        Test.setCurrentPageReference(pageRef);
        VFC_LocationCockpit myController=new VFC_LocationCockpit(new ApexPages.StandardController(loc));
        myController.updopptyId = a1.Id;
        myController.updopptyaction = true;
        String str = myController.str_updopptyaction;
        myController.updopptyaction = false;
        str = myController.str_updopptyaction;
        str = myController.IPTooltip(loc.id);
        myController.updopptyId =null;
        //myController.updateOpportunity();
        myController=new VFC_LocationCockpit(new ApexPages.StandardController(loc));
        
    }
}