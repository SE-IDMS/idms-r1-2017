@isTest
Private class VFC_OpenBackOfficeContract_Test {
    static testMethod void myUnitTest() {
        
        PageReference pageRef = Page.VFP_OpenBackOfficeContract;
    		Test.setCurrentPage(pageRef);
      
        Account acc = Utils_TestMethods.createAccount();
        acc.name='test acc';
        insert acc;
        
        SVMXC__Service_Contract__c scc= new SVMXC__Service_Contract__c();
        scc.name=' test contract';
        scc.BackOfficeSystem__c='backsystem';
        scc.CountryofBackOffice__c ='USA';
        scc.SoldtoAccount__c= acc.id;
        insert scc;
        
        VFC_OpenBackOfficeContract  VFController1= new VFC_OpenBackOfficeContract(new ApexPages.StandardController(scc));
			VFController1.doRedirect();
	        
        
  }
}