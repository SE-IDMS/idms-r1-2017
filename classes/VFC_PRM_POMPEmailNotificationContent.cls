public class VFC_PRM_POMPEmailNotificationContent {

    public String IsOrfTemplate { get; set; }
    public String RecptId { get; set; }
    public String OpptyId { get; set; }
    public String OrfId { get; set; }
    public String RecptLanguage { get; set; }
    public String NotificationFor { get; set; }
    public Id RecordId { get; set; }
    public String sObjectType { get; set; }
    public Id Country {get;set;}
    public Boolean IsOrf {get; set; }
    public String email{
    get{
        if(Country != null){
            List<PRMCountry__c> pcountry = new List<PRMCountry__c>([SELECT Id, CountrySupportEmail__c FROM PRMCountry__c WHERE (MemberCountry1__c = :Country OR MemberCountry2__c = :Country OR
                                  MemberCountry3__c = :Country OR MemberCountry4__c = :Country OR MemberCountry5__c = :Country OR     
                                  Country__c = :Country) AND CountrySupportEmail__c != null]);
            
            if(!pcountry.isEmpty()) email = pcountry[0].CountrySupportEmail__c;
            else email = System.label.CLOCT15PRM024;    
        }
        return email;
    }
    set;}
    
    public Opportunity OpportunityToUse {
        get {
                return (Opportunity)getObjectInstance(sObjectType, RecordId);
        }
        private set;
    }
    
    public Contact ContactToUse {
        get{
                system.debug('ID is ' +  RecordId);
            return (Contact)getObjectInstance(sObjectType, RecordId);
            //system.debug('## value passesd id'+objInstance);
        }
        private set;
    }
     public PRMAccountChannelChangeHistory__c PRMAccountChannelChangeHistoryToUse {
        get{
                system.debug('ID is ' +  RecordId);
            return (PRMAccountChannelChangeHistory__c)getObjectInstance(sObjectType, RecordId);
            //system.debug('## value passesd id'+objInstance);
        }
        private set;
    }
    public OpportunityRegistrationForm__c OrfToUse {
        get {
            return (OpportunityRegistrationForm__c)getObjectInstance(sObjectType, RecordId);
        }
        private set;
    }

    public OPP_ValueChainPlayers__c VcpToUse {
        get {
             Opportunity o = OpportunityToUse;
             OPP_ValueChainPlayers__c v;
            if(o.Value_Chain_Players__r.size() > 1)
                v = o.Value_Chain_Players__r[0];
            else
                v = o.Value_Chain_Players__r;
            return v;
        }
        private set;
    }

    private static SObject objInstance;
    //private static SObject[] contacts_query_string;
    
    public VFC_PRM_POMPEmailNotificationContent(){
    }
    public static SObject getObjectInstance (String oType, String thisRecordId) { 
        String qryFilter;
        if (objInstance == null && string.isNotBlank(oType) && string.isNotBlank(thisRecordId)) { 
            System.Debug('The Related record Id ' + thisRecordId);
            //CS_PRM_ApexJobSettings__c getRelatedObjectFields = CS_PRM_ApexJobSettings__c.getValues(oType);  
            Map<String,CS_PRM_ApexJobSettings__c> getRelatedObjectFields = CS_PRM_ApexJobSettings__c.getAll();   
            system.debug('*** '+getRelatedObjectFields);      
            if(String.isNotBlank(thisRecordId) != null && getRelatedObjectFields != null) {
                if(oType == 'Contact') {
                    for(String i: getRelatedObjectFields.keySet()){
                        if(i.contains('PRMContactFieldList'))
                            qryFilter = String.isEmpty(qryFilter) ? getRelatedObjectFields.get(i).QueryFields__c : qryFilter + getRelatedObjectFields.get(i).QueryFields__c;
                    }
                }
                else  { 
                    qryFilter = getRelatedObjectFields.get(oType).QueryFields__c;
                    System.Debug('The Related record Id '+qryFilter);
                }
                //String queryString = 'SELECT '+qryFilter+' FROM '+ oType + ' WHERE Id = \'' + thisRecordId + '\'';
                SObject[] records = Database.query('SELECT '+qryFilter+' FROM '+ oType + ' WHERE Id = \'' + thisRecordId + '\'');
                if (records != null && records.size() > 0) { //done
                    Schema.SObjectType sObjectType = records[0].getSObjectType();
                    System.Debug('*** Input Object Type: ' + sObjectType);
                    System.debug(Type.forName('Contact'));
                    System.debug(Type.forName('Contact').equals(sObjectType));
                    String objectname = sObjectType + '';
                    objInstance = records[0];
                
                }           
        } 
        } 
        System.Debug('Check for value'+objInstance);
    return objInstance;
}
}