@isTest
public class AP_SRV_myFSUtils_Test{
 static testMethod void UtilsTest1() {
  
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
  ][0].Id;
  
  Country__c Country = new Country__c();
  Country.name= 'USA';
  Country.CountryCode__c = 'US';
  Insert Country;
  
  Account acc1 = new Account(Name = 'TestAccount',county__c=Country.id, Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
  insert cnct;
 
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];
  test.starttest();
  /*user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', 
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;*/
  
  //system.runAs(u) {

   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
   
   //Creating Work Order
   list<SVMXC__Service_Order__c> Wolist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrec= new SVMXC__Service_Order__c();
    newrec.SVMXC__Company__c=acc1.Id;
    newrec.SVMXC__Site__c=location1.Id;
    newrec.SVMXC__Contact__c=cnct.id;
    newrec.SVMXC__Scheduled_Date_Time__c=myDateTime;
    newrec.SVMXC__Order_Status__c = 'customer confirmed';
    Wolist.add(newrec);
   }
   insert wolist;
   list<SVMXC__Service_Order__c> WoUpcominglist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime1 = DateTime.newInstance(2016, 9, 20, 00, 00, 00);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrecUpcoming= new SVMXC__Service_Order__c();
    newrecUpcoming.SVMXC__Company__c=acc1.Id;
    newrecUpcoming.SVMXC__Site__c=location1.Id;
    newrecUpcoming.SVMXC__Contact__c=cnct.id;
    newrecUpcoming.SVMXC__Scheduled_Date_Time__c=myDateTime1;
    newrecUpcoming.SVMXC__Order_Status__c = 'Service Complete';
    WoUpcominglist.add(newrecUpcoming);
   }
   insert WoUpcominglist;
    DateTime myDateTime2 = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    SVMXC__Service_Order__c newrecpast= new SVMXC__Service_Order__c();
    newrecpast.SVMXC__Company__c=acc1.Id;
    newrecpast.SVMXC__Site__c=location1.Id;
    newrecpast.SVMXC__Contact__c=cnct.id;
    newrecpast.SVMXC__Scheduled_Date_Time__c=myDateTime2;
    newrecpast.SVMXC__Order_Status__c = 'Service Complete';
    insert  newrecpast;
   Attachment attach= new Attachment();
   attach.Name = 'testbFS___Service_Intervention_Report.pdf' ;
   attach.parentId = newrecpast.Id;
   attach.Description ='Work Order Output Doc';
   Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
   attach.body=bodyBlob;
   insert attach; 
   
   Date Contractstartdate = date.today().adddays(-60);
   Date Contractenddate = date.today().adddays(+1);
   SVMXC__Service_Contract__c ParentContract = New SVMXC__Service_Contract__c();
   ParentContract .SoldtoAccount__c = acc1.id;
   ParentContract .SVMXC__Service_Contract_Notes__c = 'Test';
   ParentContract .name= 'TestContract';
   ParentContract .Status__c= 'EXP';
   ParentContract .LeadingBusinessBU__c= 'GC';
   ParentContract .SVMXC__Start_Date__c = Contractstartdate ;
   ParentContract .SVMXC__end_Date__c = Contractenddate ;
   insert ParentContract ;
   
   SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
   Contract.SoldtoAccount__c = acc1.id;
   Contract.ParentContract__c = ParentContract.Id;
   Contract.SVMXC__Service_Contract_Notes__c = 'Test';
   Contract.name= 'TestContract';
   Contract.Status__c= 'EXP';
   Contract.LeadingBusinessBU__c= 'GC';
   Contract.SVMXC__Start_Date__c = Contractstartdate ;
   Contract.SVMXC__end_Date__c = Contractenddate ;
   insert contract;
   datetime fromdateformate = date.today();
   
   set<id> accountids= new set<id>();
   accountids.add(acc1.id);
   Set<string> Status = new set<String>();
   status.add('Service Complete');
   set<String> Sites = new set<String>();
   Sites.add(location1.id);
   String Userid = userinfo.getuserid();
   AP_SRV_myFSUtils Util = new AP_SRV_myFSUtils();
   AP_SRV_myFSUtils.getAuthorizedAccountIDsForUser(Userid );
   AP_SRV_myFSUtils.getAuthorizedAccountIDsForUser(Userid ,Country.CountryCode__C);
   AP_SRV_myFSUtils.getAuthorizedAccountIDsForUser(Userid ,Country.CountryCode__C,acc1.id);
   AP_SRV_myFSUtils.getAuthorizedLocationsForUser(Userid ,Country.CountryCode__C,acc1.id);
   //AP_SRV_myFSUtils.GetAuthorizedWorkOrdersforUser(Userid ,Country.CountryCode__C,acc1.id,1,'Service Complete',fromdateformate,fromdateformate,Sites);
   AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforUser(Userid ,Country.CountryCode__C,acc1.id,date.today(),date.today(),date.today());
   AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforAccount(Userid ,Country.CountryCode__C,string.valueof(acc1.id),string.valueof(acc1.id),date.today(),date.today(),date.today());
   AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforUser(Userid ,Country.CountryCode__C,string.valueof(acc1.id),date.today(),date.today(),date.today());
   AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforAccount(Userid ,Country.CountryCode__C,string.valueof(acc1.id),acc1.id,date.today(),date.today(),date.today());
   Test.stoptest();
  }
  
  static testMethod void UtilsTest2() {
  
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
  ][0].Id;
  
  Country__c Country = new Country__c();
  Country.name= 'USA';
  Country.CountryCode__c = 'US';
  Insert Country;
  
  Account acc1 = new Account(Name = 'TestAccount',county__c=Country.id, Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
  insert cnct;
 
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];
   test.starttest();
  /*user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
   // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;*/
  //system.runAs(u) {

   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
  
   //Creating Work Order
   list<SVMXC__Service_Order__c> Wolist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrec= new SVMXC__Service_Order__c();
    newrec.SVMXC__Company__c=acc1.Id;
    newrec.SVMXC__Site__c=location1.Id;
    newrec.SVMXC__Contact__c=cnct.id;
    newrec.SVMXC__Scheduled_Date_Time__c=myDateTime;
    newrec.SVMXC__Order_Status__c = 'customer confirmed';
    Wolist.add(newrec);
   }
   insert wolist;
   list<SVMXC__Service_Order__c> WoUpcominglist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime1 = DateTime.newInstance(2016, 9, 20, 00, 00, 00);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrecUpcoming= new SVMXC__Service_Order__c();
    newrecUpcoming.SVMXC__Company__c=acc1.Id;
    newrecUpcoming.SVMXC__Site__c=location1.Id;
    newrecUpcoming.SVMXC__Contact__c=cnct.id;
    newrecUpcoming.SVMXC__Scheduled_Date_Time__c=myDateTime1;
    newrecUpcoming.SVMXC__Order_Status__c = 'Service Complete';
    WoUpcominglist.add(newrecUpcoming);
   }
   insert WoUpcominglist;
    DateTime myDateTime2 = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    SVMXC__Service_Order__c newrecpast= new SVMXC__Service_Order__c();
    newrecpast.SVMXC__Company__c=acc1.Id;
    newrecpast.SVMXC__Site__c=location1.Id;
    newrecpast.SVMXC__Contact__c=cnct.id;
    newrecpast.SVMXC__Scheduled_Date_Time__c=myDateTime2;
    newrecpast.SVMXC__Order_Status__c = 'Service Complete';
    insert  newrecpast;
   Attachment attach= new Attachment();
   attach.Name = 'testbFS___Service_Intervention_Report.pdf' ;
   attach.parentId = newrecpast.Id;
   attach.Description ='Work Order Output Doc';
   Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
   attach.body=bodyBlob;
   insert attach; 
   
   Date Contractstartdate = date.today().adddays(-60);
   Date Contractenddate = date.today().adddays(+1);
   SVMXC__Service_Contract__c ParentContract = New SVMXC__Service_Contract__c();
   ParentContract .SoldtoAccount__c = acc1.id;
   ParentContract .SVMXC__Service_Contract_Notes__c = 'Test';
   ParentContract .name= 'TestContract';
   ParentContract .Status__c= 'EXP';
   ParentContract .LeadingBusinessBU__c= 'GC';
   ParentContract .SVMXC__Start_Date__c = Contractstartdate ;
   ParentContract .SVMXC__end_Date__c = Contractenddate ;
   insert ParentContract ;
   
   SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
   Contract.SoldtoAccount__c = acc1.id;
   Contract.ParentContract__c = ParentContract.Id;
   Contract.SVMXC__Service_Contract_Notes__c = 'Test';
   Contract.name= 'TestContract';
   Contract.Status__c= 'EXP';
   Contract.LeadingBusinessBU__c= 'GC';
   Contract.SVMXC__Start_Date__c = Contractstartdate ;
   Contract.SVMXC__end_Date__c = Contractenddate ;
   insert contract;
   
   Date startdate = date.Today().adddays(-40);
           Date  enddate = date.Today().adddays(+2);
           SVMXC__Installed_Product__c InstalledProduct = new SVMXC__Installed_Product__c();
           InstalledProduct.SVMXC__Company__c =acc1.Id;
           InstalledProduct.SVMXC__Site__c = location1.Id;
           InstalledProduct.UnderContract__c =True;
           InstalledProduct.SVMXC__Warranty_End_Date__c = enddate ;
           InstalledProduct.SVMXC__Warranty_End_Date__c = startdate ;
           Insert InstalledProduct; 
            
                    
           SVMXC__Service_Contract_Services__c ServiceFeature = new SVMXC__Service_Contract_Services__c();
           ServiceFeature.SVMXC__Service_Contract__c = contract.id;
           
           SVMXC__Service_Contract_Products__c CoveredInstalledProduct = new SVMXC__Service_Contract_Products__c();
           CoveredInstalledProduct.SVMXC__Service_Contract__c = contract.id;
           CoveredInstalledProduct.SVMXC__Installed_Product__c = InstalledProduct.id;
           CoveredInstalledProduct.AssetLocation__c = location1.id;
           insert CoveredInstalledProduct;
           
   
   set<id> accountids= new set<id>();
   accountids.add(acc1.id);
   Set<string> Status = new set<String>();
   status.add('Service Complete');
   set<Id> Sites = new set<Id>();
   Sites.add(location1.id);
   
   AP_SRV_myFSUtils Util = new AP_SRV_myFSUtils();
   String Userid =userinfo.getuserid();
   AP_SRV_myFSUtils.getAuthorizedExpiredContractsforUser(Userid ,Country.CountryCode__C,string.valueof(acc1.id),date.today(),date.today(),date.today(),1);
   AP_SRV_myFSUtils.getAuthorizedExpiredContractsforAccount(Userid ,Country.CountryCode__C,string.valueof(acc1.id),acc1.id,date.today(),date.today(),date.today(),1);
   AP_SRV_myFSUtils.getAuthorizedActiveContractsforUser(Userid ,Country.CountryCode__C,acc1.id,date.today(),date.today(),date.today(),1,Sites);
   AP_SRV_myFSUtils.getAuthorizedActiveContractsforAccount(Userid ,Country.CountryCode__C,acc1.id,acc1.id,date.today(),date.today(),date.today(),1,Sites);
  
   Test.stoptest();
  }
 }