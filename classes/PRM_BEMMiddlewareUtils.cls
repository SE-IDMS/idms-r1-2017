public with sharing class PRM_BEMMiddlewareUtils {
    public static final String RETURN_MESSAGE_ERROR ='E' ;

    public PRM_BEMMiddlewareUtils() {
        
    }

    public static WidgetBean getWidget(PRMCockpitComponentConfiguration__c cmp, ParameterBean parameter, Map<String,String> header){
        try{
            String wsURL = cmp.WSURL__c;
            Http h = new Http();
            // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
            HttpRequest req = new HttpRequest();
            req.setEndpoint(wsURL);
            req.setMethod('POST');
            req.setTimeout(120000);
            req.setClientCertificateName(System.Label.CL00616);
            req.setHeader('Content-Type', 'application/json');
            /*JSONGenerator generator = JSON.createGenerator(true);
            generator.writeStartObject();
            generator.writeObject(parameter);
            generator.writeEndObject();
            String JSONString = generator.getAsString();
            System.debug('JSONString:'+JSONString);
            req.setBody(JSONString);
            */
            /*User currUser = [select id, PRMTemporarySamlToken__c, FederationIdentifier from User where id = :Userinfo.getUserId()]; 
            String token = currUser.PRMTemporarySamlToken__c;
            token = token.replaceAll('\r\n', '').replaceAll('\n', '').replaceAll('\r', '');
            */
            req.setBody('{"securityToken":"'+parameter.securityToken+'","federatedId":"'+parameter.federatedId+'","context":"'+parameter.context+'","email":"'+parameter.email+'","countryCode":"'+parameter.countryCode+'"}');
            
            // Send the request, and return a response
            HttpResponse res = h.send(req);
            System.debug(res.getBody());
            JSONParser parser = JSON.createParser(res.getBody());
            PRM_BEMMiddlewareUtils.WidgetBean widget = (PRM_BEMMiddlewareUtils.WidgetBean) parser.readValueAs(PRM_BEMMiddlewareUtils.WidgetBean.class);
            System.debug(widget);
            return widget;
        }catch(Exception ex){
            return new WidgetBean(RETURN_MESSAGE_ERROR,'getLineNumber: '+ex.getLineNumber()+' getMessage '+ex.getMessage()+' getStackTraceString '+ex.getStackTraceString());
        }
    }

    public class ParameterBean {
        public String context{get;set;}
        public String securityToken{get;set;}
        public String federatedId{get;set;}
        public String email{get;set;}
        public String countryCode{get;set;}

        public ParameterBean(String context,String securityToken,String federatedId,String email,String countryCode){
            this.context = context;
            this.securityToken = securityToken;
            this.federatedId = federatedId;
            this.email =email;
            this.countryCode = countryCode;
        }

    }

    public class WidgetBean {
        public WidgetBean(String returnCode,String returnMessage){
            this.returnCode = returnCode;
            this.returnMessage = returnMessage;
        }
        public String name{get;set;}
        public String returnCode{get;set;}
        public String returnMessage{get;set;}
        public Long calculationTimeInMillisecondes{get;set;}
        public List<ElementBean> elements{get;set;}

    }

    public class ElementBean {
        public String name{get;set;}
        public String content{get;set;}
    }   
}