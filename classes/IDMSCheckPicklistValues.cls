/*
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  01/12/2016 
*   Thid class provides methods that check if the picklist values provided by UIMS are valid ones based on
*   BFO picklist referential to ensure data integrity
* */
Public Class IDMSCheckPicklistValues{
    
    //Check of the "JobFunction" value provided by UIMS is a valid one based on MapJobFunctionValues which is containing
    //all the valid "jobFunction" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidJobFunction(String jobFunction, Map<String,String> MapJobFunctionValues){
        String res;
        if(MapJobFunctionValues.containsKey(jobFunction)){
            system.debug(jobFunction+ ' is present in the picklist referential of jobFunction');
            res = jobFunction;
        } else{
            res = '';
        }
        return res;
    }
    
    //Check of the "jobTitle" value provided by UIMS is a valid one based on MapJobTitleValues which is containing
    //all the valid "jobTitle" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidJobTitle(String jobTitle, Map<String,String> MapJobTitleValues){
        string res;
        if(MapJobTitleValues.containsKey(jobTitle)){
            system.debug(jobTitle+ ' is present in the picklist referential of jobTitle');
            res = jobTitle;
        } else {
            res = '';
        }
        return res;
    }
    
    //Check of the "PreferedLang" value provided by UIMS is a valid one based on MapPreferedLangValues which is containing
    //all the valid "PreferedLang" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidPreferedLang(String PreferedLang, Map<String,String> MapPreferedLangValues){
        string res;        
        system.debug('PreferedLang:'+PreferedLang);
        String preferedLangUpper;
        if(string.isnotblank(PreferedLang)){
            preferedLangUpper = PreferedLang.toUpperCase();
        }
        if(MapPreferedLangValues.containsKey(preferedLangUpper )){
            system.debug(preferedLangUpper+ ' is present in the picklist referential of PreferedLang');
            res = preferedLangUpper;
        }else{
            res = '';
        }        
        return res;
    }
    
    //Check of the "Salutation" value provided by UIMS is a valid one based on MapSalutationValues which is containing
    //all the valid "Salutation" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidSalutation(String Salutation, Map<String,String> MapSalutationValues){
        string res;        
        if(MapSalutationValues.containsKey(Salutation)){
            system.debug(Salutation+ ' is present in the picklist referential of Salutation');
            res = Salutation;
        }else{
            res = '';
        }
        return res;
    }
    
    //Check of the "UserState" value provided by UIMS is a valid one based on MapUserStateValues which is containing
    //all the valid "UserState" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidUserState(String UserState, Map<String,String> MapUserStateValues){
        string res;        
        if(MapUserStateValues.containsKey(UserState)){
            system.debug(UserState+ ' is present in the picklist referential of UserState');
            res = UserState;
        }else{
            res = '';
        }
        return res;
    }
    
    //Check of the "UserCountry" value provided by UIMS is a valid one based on MapUserCountryValues which is containing
    //all the valid "UserCountry" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidUserCountry(String UserCountry, Map<String,String> MapUserCountryValues){
        String res;
        if(MapUserCountryValues.containsKey(UserCountry)){
            system.debug(UserCountry+ ' is present in the picklist referential of UserCountry');
            res = UserCountry;
        }else{
            res = 'US';
        }
        return res;
    }
    
    //Check of the "CompCountry" value provided by UIMS is a valid one based on MapCompCountryValues which is containing
    //all the valid "CompCountry" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidCompanyCountry(String CompCountry, Map<String,String> MapCompCountryValues){
        String res;
        if(MapCompCountryValues.containsKey(CompCountry)){
            system.debug(CompCountry+ ' is present in the picklist referential of jobFunction');
            res = CompCountry;
        } else{
            res = '';
        }
        return res;
    }
    
    //Check of the "classLevel1" value provided by UIMS is a valid one based on MapClassLevel1Values which is containing
    //all the valid "classLevel1" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidIDMSClassLevel1(String classLevel1, Map<String,String> MapClassLevel1Values){
        String res;
        if(MapClassLevel1Values.containsKey(classLevel1)){
            system.debug(classLevel1+ ' is present in the picklist referential of jobFunction');
            res = classLevel1;
        } else{
            res = '';
        }
        return res;
    }
    
    //Check of the "classLevel2" value provided by UIMS is a valid one based on MapClassLevel2Values which is containing
    //all the valid "classLevel2" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidIDMSClassLevel2(String classLevel2, Map<String,String> MapClassLevel2Values){
        String res;
        if(MapClassLevel2Values.containsKey(classLevel2)){
            system.debug(classLevel2+ ' is present in the picklist referential of jobFunction');
            res = classLevel2;
        } else{
            res = '';
        }
        return res;
    }
    
    //Check of the "IDMSCompanyNbrEmployees" value provided by UIMS is a valid one based on MapIDMSCompanyNbrEmployeesValues which is containing
    //all the valid "IDMSCompanyNbrEmployees" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidIDMSCompanyNbrEmployees(String IDMSCompanyNbrEmployees, Map<String,String> MapIDMSCompanyNbrEmployeesValues){
        String res;
        if(MapIDMSCompanyNbrEmployeesValues.containsKey(IDMSCompanyNbrEmployees)){
            system.debug(IDMSCompanyNbrEmployees+ ' is present in the picklist referential of jobFunction');
            res = IDMSCompanyNbrEmployees;
        } else{
            res = '';
        }
        return res;
    }
    
    //Check of the "IDMSCompanyMarketServed" value provided by UIMS is a valid one based on MapIDMSCompanyMarketServedValues which is containing
    //all the valid "IDMSCompanyMarketServed" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidIDMSCompanyMarketServed(String IDMSCompanyMarketServed, Map<String,String> MapIDMSCompanyMarketServedValues){
        String res;
        if(MapIDMSCompanyMarketServedValues.containsKey(IDMSCompanyMarketServed)){
            system.debug(IDMSCompanyMarketServed+ ' is present in the picklist referential of jobFunction');
            res = IDMSCompanyMarketServed;
        } else{
            res = '';
        }
        return res;
    }
    
    //Check of the "CompanyState" value provided by UIMS is a valid one based on MapCompanyStateValues which is containing
    //all the valid "CompanyState" values of BFO. If not valide, return a blank string. if valide, return the same value.
    public static String isValidCompanyState(String CompanyState, Map<String,String> MapCompanyStateValues){
        String res;
        if(MapCompanyStateValues.containsKey(CompanyState)){
            system.debug(CompanyState+ ' is present in the picklist referential of jobFunction');
            res = CompanyState;
        } else{
            res = '';
        }
        return res;
    }
    
    //Get BFO Job Function referential
    public static Map<String,String> getMapJobFunctionValues(){
        Map<String,String> MapJobFunctionValues= new Map<String,String>();
        Schema.DescribeFieldResult SchemaJobFunctionValues= User.Job_Function__c.getDescribe();
        List<Schema.PicklistEntry> PleJobFunctionValues= SchemaJobFunctionValues.getPicklistValues();
        for( Schema.PicklistEntry f : PleJobFunctionValues){
            MapJobFunctionValues.put(f.getValue(),f.getValue());
        }
        return MapJobFunctionValues;        
    }
    
    //Get BFO Job Title referential    
    public static Map<String,String> getMapJobTitleValues(){
        Map<String,String> MapJobTitleValues= new Map<String,String>();
        Schema.DescribeFieldResult SchemaJobTitleValues= User.Job_Title__c.getDescribe();
        List<Schema.PicklistEntry> PleJobTitleValues= SchemaJobTitleValues.getPicklistValues();
        for( Schema.PicklistEntry f : PleJobTitleValues){
            MapJobTitleValues.put(f.getValue(),f.getValue());
        }
        return MapJobTitleValues;        
    }
    
    //Get BFO Language code referential
    public static Map<String,String> getMapLanguageCode(){
        Map<String,String> MapLanguageCode= new Map<String,String>();
        Schema.DescribeFieldResult SchemaLanguageCodeValues= User.IDMS_PreferredLanguage__c.getDescribe();
        List<Schema.PicklistEntry> PleLanguageCodeValues= SchemaLanguageCodeValues.getPicklistValues();
        for( Schema.PicklistEntry f : PleLanguageCodeValues){
            MapLanguageCode.put(f.getValue(),f.getValue());
        }
        return MapLanguageCode;        
    }
    
    //Get BFO Salutation referential
    public static Map<String,String> getMapSalutation(){
        Map<String,String> MapSalutationValues= new Map<String,String>();
        Schema.DescribeFieldResult SchemaSalutationValues= User.IDMSSalutation__c.getDescribe();
        List<Schema.PicklistEntry> PleSalutationValues= SchemaSalutationValues.getPicklistValues();
        for( Schema.PicklistEntry f : PleSalutationValues){
            MapSalutationValues.put(f.getValue(),f.getValue());
        }
        return MapSalutationValues;        
    }
    
    //Get BFO User state  referential
    public static Map<String,String> getMapUserState(){
        Map<String,String> MapUserStateValues= new Map<String,String>();
        Schema.DescribeFieldResult SchemaUserStateValues= User.Statecode.getDescribe();
        List<Schema.PicklistEntry> PleUserStateValues= SchemaUserStateValues.getPicklistValues();
        for( Schema.PicklistEntry f : PleUserStateValues){
            MapUserStateValues.put(f.getValue(),f.getValue());
        }
        return MapUserStateValues;        
    }
    
    //Get BFO User country code referential
    public static Map<String,String> getMapUserCountryCode(){
        Map<String,String> MapUserCountryCodeValues= new Map<String,String>();
        Schema.DescribeFieldResult SchemaUserCountryCodeValues= User.Countrycode.getDescribe();
        List<Schema.PicklistEntry> PleUserCountryCodeValues= SchemaUserCountryCodeValues.getPicklistValues();
        for( Schema.PicklistEntry f : PleUserCountryCodeValues){
            MapUserCountryCodeValues.put(f.getValue(),f.getValue());
        }
        return MapUserCountryCodeValues;        
    }
    
    //Get BFO User company country code referential
    public static Map<String,String> getMapUserCompanyCountryCode(){
        Map<String,String> MapUserCompCountryCodeValues= new Map<String,String>();
        Schema.DescribeFieldResult SchemaUserCompCountryCodeValues= User.Company_Country__c.getDescribe();
        List<Schema.PicklistEntry> PleUserCountryCodeValues= SchemaUserCompCountryCodeValues.getPicklistValues();
        for( Schema.PicklistEntry f : PleUserCountryCodeValues){
            MapUserCompCountryCodeValues.put(f.getValue(),f.getValue());
        }
        return MapUserCompCountryCodeValues;        
    }
    
    //Get BFO Class Level 1 referential
    public static Map<String,String> getMapUserClassLevel1(){
        Map<String,String> MapUserClassLevel1Values= new Map<String,String>();
        Schema.DescribeFieldResult SchemaUserClassLevel1Values= User.IDMSClassLevel1__c.getDescribe();
        List<Schema.PicklistEntry> PleClassLevel1Values= SchemaUserClassLevel1Values.getPicklistValues();
        for( Schema.PicklistEntry f : PleClassLevel1Values){
            MapUserClassLevel1Values.put(f.getValue(),f.getValue());
        }
        return MapUserClassLevel1Values;        
    }
    
    //Get BFO Class Level 2 referential
    public static Map<String,String> getMapUserClassLevel2(){
        Map<String,String> MapUserClassLevel2Values= new Map<String,String>();
        Schema.DescribeFieldResult SchemaUserClassLevel2Values= User.IDMSClassLevel2__c.getDescribe();
        List<Schema.PicklistEntry> PleClassLevel2Values= SchemaUserClassLevel2Values.getPicklistValues();
        for( Schema.PicklistEntry f : PleClassLevel2Values){
            MapUserClassLevel2Values.put(f.getValue(),f.getValue());
        }
        return MapUserClassLevel2Values;        
    }
    
    //Get BFO Company Nbr of Employees referential
    public static Map<String,String> getMapUserCompanyNbrEmployees(){
        Map<String,String> MapUserCompanyNbrEmployeesValues= new Map<String,String>();
        Schema.DescribeFieldResult SchemaCompanyNbrEmployeesValues= User.IDMSCompanyNbrEmployees__c.getDescribe();
        List<Schema.PicklistEntry> PleCompanyNbrEmployeesValues= SchemaCompanyNbrEmployeesValues.getPicklistValues();
        for( Schema.PicklistEntry f : PleCompanyNbrEmployeesValues){
            MapUserCompanyNbrEmployeesValues.put(f.getValue(),f.getValue());
        }
        return MapUserCompanyNbrEmployeesValues;        
    }
    
    //Get BFO Market Served referential
    public static Map<String,String> getMapUserMarketServed(){
        Map<String,String> MapUserCompanyMarketServedValues= new Map<String,String>();
        Schema.DescribeFieldResult SchemaCompanyMarketServedValues= User.IDMSCompanyMarketServed__c.getDescribe();
        List<Schema.PicklistEntry> PleCompanyMarketServedValues= SchemaCompanyMarketServedValues.getPicklistValues();
        for( Schema.PicklistEntry f : PleCompanyMarketServedValues){
            MapUserCompanyMarketServedValues.put(f.getValue(),f.getValue());
        }
        return MapUserCompanyMarketServedValues;        
    }
    
    //Get BFO Company State referential
    public static Map<String,String> getMapUserCompanyState(){
        Map<String,String> MapUserCompanyStateValues= new Map<String,String>();
        Schema.DescribeFieldResult SchemaCompanyStateValues= User.Company_State__c.getDescribe();
        List<Schema.PicklistEntry> PleCompanyStateValues= SchemaCompanyStateValues.getPicklistValues();
        for( Schema.PicklistEntry f : PleCompanyStateValues){
            MapUserCompanyStateValues.put(f.getValue(),f.getValue());
        }
        return MapUserCompanyStateValues;        
    }
    
}