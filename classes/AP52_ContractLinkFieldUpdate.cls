public without sharing class AP52_ContractLinkFieldUpdate {
    public static void performCurrentUserUpdates(List<OPP_ContractLink__c> cls){
        for(OPP_ContractLink__c cl : cls){
            // set current approver
            if(cl.ContractStatus__c != label.CL00242)
            {
            if(cl.TECH_ApprovalStep__c == 10)
                cl.CurrentApprover__c = cl.Approver10__c;
            else if(cl.TECH_ApprovalStep__c == 9)
                cl.CurrentApprover__c = cl.Approver9__c;
            else if(cl.TECH_ApprovalStep__c == 8)
                cl.CurrentApprover__c = cl.Approver8__c;
            else if(cl.TECH_ApprovalStep__c == 7)
                cl.CurrentApprover__c = cl.Approver7__c;
            else if(cl.TECH_ApprovalStep__c == 6)
                cl.CurrentApprover__c = cl.Approver6__c;
            else if(cl.TECH_ApprovalStep__c == 5)
                cl.CurrentApprover__c = cl.Approver5__c;
            else if(cl.TECH_ApprovalStep__c == 4)
                cl.CurrentApprover__c = cl.Approver4__c;
            else if(cl.TECH_ApprovalStep__c == 3)
                cl.CurrentApprover__c = cl.Approver3__c;
            else if(cl.TECH_ApprovalStep__c == 2)
                cl.CurrentApprover__c = cl.Approver2__c;
            else if(cl.TECH_ApprovalStep__c == 1)
                cl.CurrentApprover__c = cl.Approver1__c;
            else
                cl.CurrentApprover__c = null;   
            }
            else
                cl.CurrentApprover__c = null;
        }
    }
    
    public static void performLastUserUpdates(List<OPP_ContractLink__c> cls){
        for(OPP_ContractLink__c cl : cls){
            // set last approver
            
            if(cl.Approver10__c != null)
                cl.LastApprover__c = cl.Approver10__c;
            else if(cl.Approver9__c != null)
                cl.LastApprover__c = cl.Approver9__c;
            else if(cl.Approver8__c != null)
                cl.LastApprover__c = cl.Approver8__c;
            else if(cl.Approver7__c != null)
                cl.LastApprover__c = cl.Approver7__c;
            else if(cl.Approver6__c != null)
                cl.LastApprover__c = cl.Approver6__c;
            else if(cl.Approver5__c != null)
                cl.LastApprover__c = cl.Approver5__c;
            else if(cl.Approver4__c != null)
                cl.LastApprover__c = cl.Approver4__c;
            else if(cl.Approver3__c != null)
                cl.LastApprover__c = cl.Approver3__c;
            else if(cl.Approver2__c != null)
                cl.LastApprover__c = cl.Approver2__c;
            else if(cl.Approver1__c != null)
                cl.LastApprover__c = cl.Approver1__c;
            else
                cl.LastApprover__c = null;
             
             
                
                
        }
    }    
    
}