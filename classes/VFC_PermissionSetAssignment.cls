public virtual class VFC_PermissionSetAssignment {

    public List<User> assigneeList {get;set;}
    public User relatedUser {get;set;}
    
    public PermissionSetGroup__c searchCriteria {get;set;}
    public String searchName {get;set;}
    public String searchStream {get;set;}
    public String searchType {get;set;}
    
    public List<SelectOption> searchStreamList {get;set;}
    public List<SelectOption> searchTypeList {get;set;}
    
    
    public Set<Id> existingPermissionGroupIdSet {get{return searchResultMap.keySet();}set;}
    public List<PermissionSetGroup__c> searchResult {get{return searchResultMap.values();}set;}
    public Map<Id, PermissionSetGroup__c> searchResultMap {get;set;}
    public List<PermissionSetGroup__c> existingAssignments {get{return existingAssignmentsMap.values();}set;}
    public Map<Id, PermissionSetGroup__c> initialAssignmentMap {get;set;}
    private Map<Id, PermissionSetGroup__c> existingAssignmentsMap;
    
    public PermissionSetGroup__c permissionSetGroupSearch {get;set;}
    public bFO_Object__c bfo {get;set;}
    public List<SelectOption> accessLevel {get;set;}
    
    public List<System.SelectOption> sObjectList {get;set;} 
    
    public List<SelectOption> availableUserList {get;set;}
    public List<SelectOption> selectedUserList {get;set;}
    
    public String selectedAccessLevel {get;set;}    
    public String objectName {get;set;} 
    
    protected Boolean massAssign = false;
    public Boolean searchIndividualPsOnly {get;set;}

    protected Map<String, String> statusPicklistLabelMap  = new Map<String, String>();
    protected Map<String, String> licensePicklistLabelMap = new Map<String, String>();
    
    public DaUserInfo__c daInfo {get;set;}

    protected void getPicklistValues() {
    
        Schema.DescribeFieldResult fieldResult = PermissionSetAssignmentRequests__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> pleList = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry ple:pleList) {
            statusPicklistLabelMap.put(ple.getValue(), ple.getLabel());
        }

        fieldResult = PermissionSetGroup__c.License__c.getDescribe();
        pleList = fieldResult.getPicklistValues();
        
        for(Schema.PicklistEntry ple:pleList) {
            licensePicklistLabelMap.put(ple.getLabel(), ple.getValue());
        }  
        
        fieldResult = PermissionSetGroup__c.Stream__c.getDescribe();
        pleList = fieldResult.getPicklistValues();
        searchStreamList = new List<SelectOption>();
        searchStreamList.add(new SelectOption('', '--None--'));
                
        for(Schema.PicklistEntry ple:pleList) {
            searchStreamList.add(new SelectOption(ple.getValue(), ple.getLabel()));
        }          

        fieldResult = PermissionSetGroup__c.Type__c.getDescribe();
        pleList = fieldResult.getPicklistValues();
        searchTypeList = new List<SelectOption>();
        searchTypeList.add(new SelectOption('', '--None--'));
                        
        for(Schema.PicklistEntry ple:pleList) {
            searchTypeList.add(new SelectOption(ple.getValue(), ple.getLabel()));
        }  
                     
    }
    
    public void retrieveDaUserInfo() {

        if(relatedUser.Id != null) {
          
            for(DaUserInfo__c daUserInfoQueried:[SELECT Id, UserId__c, Name FROM DaUserInfo__c
                                       WHERE UserId__c = :relatedUser.Id]) {
                daInfo = daUserInfoQueried;
            }                
            
            if(daInfo == null) {
                daInfo = new DaUserInfo__c(Name = relatedUser.Name, UserId__c = relatedUser.Id);
                insert daInfo;
            }

           daInfo = [SELECT Id, UserId__c, Name FROM DaUserInfo__c WHERE UserId__c = :relatedUser.Id LIMIT 1];
        }
    }
    
    public VFC_PermissionSetAssignment() {
    
        relatedUser = new User();
        searchCriteria = new PermissionSetGroup__c();
        
        searchIndividualPsOnly = false;
        bfo = new bFO_Object__c(); 
    
        
        searchResultMap = new Map<Id, PermissionSetGroup__c>();
        initialAssignmentMap = new Map<Id, PermissionSetGroup__c>();
        existingAssignmentsMap = new Map<Id, PermissionSetGroup__c> ();
        getPicklistValues();
        
        populateAccessLevel();
    }
    
    public PageReference editPermissions() {
    
        PageReference editPage = new PageReference('/apex/VFP_PermissionSetAssignment?Id='+relatedUser.Id);
        editPage.setRedirect(true);
        return editPage;
    }
    
    public VFC_PermissionSetAssignment(ApexPages.StandardController userController) {
        
        relatedUser = [SELECT Id, Name, FirstName, LastName, UserRole.Name, Profile.Name, IsActive, ProfileId,
                       (SELECT Id, PermissionSetGroup__c FROM PermissionSetAssignmentRequests__r) 
                       FROM User WHERE Id = :userController.getId()];

        searchIndividualPsOnly = false;
        bfo = new bFO_Object__c();          
        searchCriteria = new PermissionSetGroup__c();
        searchResultMap = new Map<Id, PermissionSetGroup__c>();
        initialAssignmentMap = new Map<Id, PermissionSetGroup__c>();
        
        populateAccessLevel();
        
        getPicklistValues();
        
        if(!massAssign) {
            getexistingAssignmentsMap();
            getUnassignedPermissionSets();        
        }
        
    }
    
    private void populateAccessLevel() {
        accessLevel = new List<SelectOption>{new SelectOption('', '--None--')}; 
        accessLevel.add(new SelectOption('Create','Create'));
        accessLevel.add(new SelectOption('Read','Read'));
        accessLevel.add(new SelectOption('Edit','Edit'));
        accessLevel.add(new SelectOption('Delete','Delete'));
        accessLevel.add(new SelectOption('ViewAll','View All'));
        accessLevel.add(new SelectOption('ModifyAll','Modify All'));
                                        
    }
    
    public void getUnassignedPermissionSets() {

        PermissionSetGroup__c unassignedPsGroup = new PermissionSetGroup__c(Name = 'Ungrouped Permissions',
                                                                            MemberList__c = '',
                                                                            Description__c = 'Permission Sets Assigned to the User that are not part of a Permission Set Group.');       
        Boolean unassignedPS = false;
        
        for(PermissionSetAssignment psAssignment:[SELECT Id, PermissionSetId, PermissionSet.Name, PermissionSet.Label, PermissionSet.Description FROM PermissionSetAssignment 
                                                          WHERE AssigneeId = :relatedUser.Id AND PermissionSet.IsOwnedByProfile = false]) {
            
            Boolean isInGroup = false;
            
            system.debug('psAssignment = ' + psAssignment);

            for(PermissionSetGroup__c psGroup:initialAssignmentMap.values()) {
                
                system.debug('psGroupMember = ' + psGroup.PermissionSetGroupMembers__r);
                
                for(PsGroupMember__c psGroupMember:psGroup.PermissionSetGroupMembers__r) {
                
                    if(psGroupMember.PermissionSetId__c == psAssignment.PermissionSetId) {
                        isInGroup = true;
                    }
                }
            }
            
            if(!isInGroup) {                
                unassignedPsGroup.MemberList__c += '<font size="0.65"><a href="/'+psAssignment.PermissionSetId+'">'+psAssignment.PermissionSet.Label +'</a><br/></font>';
                unassignedPS = true;
            }
        }    
        
        if(unassignedPS) {
            setHTML('Assigned', unassignedPsGroup, false);
            initialAssignmentMap.put(relatedUser.Id, unassignedPsGroup);
            existingAssignmentsMap.put(relatedUser.Id, unassignedPsGroup); 
        }
    }
        
    public void populateSObjectList() {

        List<String> objectNameList = new List<String>{'Account', 'Contact'};
        
        if(!Test.isRunningTest()) {
            
            objectNameList = new List<String>();
            
            AggregateResult[] groupedResults = [SELECT sObjectType, COUNT(Id) FROM ObjectPermissions GROUP BY sObjectType];
            
            for(AggregateResult aggResult:groupedResults) {
                objectNameList.add((String)aggResult.get('sObjectType'));
            }
        }
        
        List<bFO_Object__c> objWPermList = new List<bFO_Object__c>();
        
        system.debug('objectNameList = ' + objectNameList); 
             
        for(Schema.DescribeSObjectResult objectDescribe:Schema.describeSObjects(objectNameList)) {
            
            if(objectDescribe.getName().length() < 39) {
                bFO_Object__c bfoObject = new bFO_Object__c(Name = objectDescribe.getLabel(), Technical_API_Name__c = objectDescribe.getName());
                objWPermList.add(bfoObject);            
            }
        }        
     
        List<Database.upsertResult> upsertResults = Database.upsert(objWPermList, false);
    }
    
    public void populateHTMLFields(PermissionSetGroup__c permissionSetGroup) {
    
        if(permissionSetGroup != null) {
            
            permissionSetGroup.MemberList__c = '<font size="0.65">';
            
            for(PsGroupMember__c member:permissionSetGroup.PermissionSetGroupMembers__r) {
                permissionSetGroup.MemberList__c += '<a href="/'+member.PermissionSetId__c+'">'+member.Name +'</a><br/>';
                
                if(!permissionSetGroup.PSAssignmentRequests__r.isEmpty()) {
                    
                    system.debug('permissionSetGroup.PSAssignmentRequests__r[0].Status__c = ' + permissionSetGroup.PSAssignmentRequests__r[0].Status__c);
                    
                    String status = permissionSetGroup.PSAssignmentRequests__r[0].Status__c;
                    setHTML(status, permissionSetGroup, false);
  
                }            
            }
            
            permissionSetGroup.MemberList__c += '</font>';
        }
    }
    
    public void setHTML(String status, PermissionSetGroup__c permissionSetGroup, Boolean addition) {
        
        
        system.debug('status = ' + status);
        
        if(status == 'PendingValidation'
           || status == 'PendingActivation' || status ==  'PendingLicense' || status == 'PendingCPValidation') {
            permissionSetGroup.TECH_Status__c = '<img src="/img/msg_icons/warning16.png"/>&nbsp;'+statusPicklistLabelMap.get(status); 
        }
        else if(status == 'Assigned'){
            permissionSetGroup.TECH_Status__c = '<img src="/img/msg_icons/confirm16.png"/>&nbsp;'+statusPicklistLabelMap.get(status);                      
        }                                   
    }
    
    public void getExistingAssignmentsMap() {
    
        existingAssignmentsMap = new Map<Id, PermissionSetGroup__c>();
    
        for(PermissionSetGroup__c permissionSetGroup:AP_PermissionSetGroupMethods.getExistingAssignmentsMap(new Set<Id>{relatedUser.Id}).values()) {
            populateHTMLFields(permissionSetGroup);
            existingAssignmentsMap.put(permissionSetGroup.Id, permissionSetGroup);
            initialAssignmentMap.put(permissionSetGroup.Id, permissionSetGroup);
        }    
    }
    
    public void delGroup() {

        removeGroup();
        saveAssignment();

    }
    
    public void removeGroup() {

        String removedGroupId = ApexPages.currentPage().getParameters().get('removeId');
        
        for(PermissionSetGroup__c psGroup:searchResult ) {
            if(psGroup.Id == removedGroupId ) {
                psGroup.TECH_IsAssigned__c = false;
            }
        }
        
        
        existingAssignmentsMap.remove(removedGroupId);    
    }
    
    public PageReference viewRequest() {
        
        PageReference requestPage;
       
        String selectedGroupId = ApexPages.currentPage().getParameters().get('viewId');
        PermissionSetAssignmentRequests__c psaRequest = [SELECT Id, PermissionSetGroup__c FROM PermissionSetAssignmentRequests__c
                                                         WHERE (PendingAssigneeId__c = :relatedUser.Id OR Assignee__c = :relatedUser.Id)
                                                         AND PermissionSetGroup__c = :selectedGroupId LIMIT 1];
        
        ApexPages.standardController psAssignmentRequest = new ApexPages.standardController(psaRequest);
        return psAssignmentRequest.view();
    }
    
    public void selectGroup() {
    
        String selectedGroupId = ApexPages.currentPage().getParameters().get('selectId');
        
        if(!existingAssignmentsMap.keySet().contains(selectedGroupId)) {
            
            PermissionSetGroup__c selectedGroup = searchResultMap.get(selectedGroupId);
            
            Boolean canBeAssigned = true;
            
            if(selectedGroup.License__c != null && !massAssign) {
               String licenseLabel = selectedGroup.License__c;
               String licenseValue =  licensePicklistLabelMap.get(selectedGroup.License__c);               
               Map<Id, List<UserPackageLicense>> userLicenceMap = AP_PermissionSetGroupMethods.getUserLicences(new Set<Id>{relatedUser.Id}, new List<PermissionSetGroup__c>{selectedGroup});
               
               canBeAssigned = userLicenceMap.get(relatedUser.Id) != null; 
               
               if(!canBeAssigned) {
                    ApexPages.Message noSelectedCriteria = new ApexPages.Message(ApexPages.Severity.ERROR,'You must assign a '+licenseLabel+' license to the user before assigning this group of Permission Sets.');
                    ApexPages.addMessage(noSelectedCriteria);               
               }              
            }
            
            if(canBeAssigned) {
                List<PermissionSetAssignmentRequests__c> newAssignments = AP_PermissionSetGroupMethods.generateRequest(new List<PermissionSetGroup__c>{selectedGroup}, new List<User>{relatedUser}, 'IgnoreExisting');
                
                selectedGroup.TECH_IsAssigned__c = true;
                for(PermissionSetAssignmentRequests__c request:newAssignments ) {
                    selectedGroup.TECH_Status__c = request.Status__c;
                }
                
                setHTML(selectedGroup.TECH_Status__c, selectedGroup, true);
                
                if(selectedGroup != null) {
                    existingAssignmentsMap.put(selectedGroup.Id, selectedGroup);
                }            
            }         
        } 
    }
    
    public PageReference saveAssignment() {
    
        PageReference savePage;
    
        List<PermissionSetAssignmentRequests__c> newAssignments = AP_PermissionSetGroupMethods.generateRequest(existingAssignmentsMap.values(), new List<User>{relatedUser}, '');
        List<PermissionSetAssignmentRequests__c> deletedAssignments = new List<PermissionSetAssignmentRequests__c>();
        
        system.debug('newAssignments = ' + newAssignments );
        
        for(PermissionSetGroup__c permissionSetGroup:initialAssignmentMap.values()) {
            if(!permissionSetGroup.PSAssignmentRequests__r.isEmpty()) {
            
                if(existingAssignmentsMap.get(permissionSetGroup.Id) == null) {
                
                    for(PermissionSetAssignmentRequests__c psRequest:permissionSetGroup.PSAssignmentRequests__r) {
                        if(psRequest.Assignee__c == relatedUser.Id || psRequest.PendingAssigneeId__c == relatedUser.Id) {
                            deletedAssignments.add(psRequest);  
                        }                  
                    }                
                }            
            }
        }
        
        if(!newAssignments.isEmpty()) {
            insert newAssignments;        
        }

        if(!deletedAssignments.isEmpty()) {
            delete deletedAssignments;        
        }
        
        savePage = new PageReference('/apex/VFP_PermissionSetAssignmentRead?Id='+relatedUser.Id);
        savePage.setRedirect(true);
        return savePage;
    } 
    
    public PageReference backToRead() {
        return new PageReference('/apex/VFP_PermissionSetAssignmentRead?Id='+relatedUser.Id);
    }
    
    public void search() {
        
        Set<Id> PermissionSetId = new Set<Id>();
        Map<Id, PermissionSet> permissionSetIdMap = new Map<Id, PermissionSet>();
    
        String searchString = 'SELECT Id, Name, Description__c,RequiresValidation__c, MemberList__c,'
                            + ' Type__c, toLabel(License__c), Stream__c, TECH_IsAssigned__c, TECH_Status__c, '
                            + (relatedUser.Id != null ? ' (SELECT PermissionSetGroup__c, Status__c, Assignee__c FROM PSAssignmentRequests__r WHERE Assignee__c = \''+relatedUser.Id+'\'),' : '')
                            + ' (SELECT Id, Name, PermissionSetId__c FROM PermissionSetGroupMembers__r)'
                            + ' FROM PermissionSetGroup__c WHERE';
        
        Boolean additionalCriteria = false;
    
        if(bfo.bFO_Object__c != null ) {
           
            List<bFO_Object__c> lstbFOObjects = [SELECT Id, Technical_API_Name__c FROM bFO_Object__c WHERE Id = :bfo.bFO_Object__c];
            if(lstbFOObjects.size() == 1){
                objectName = lstbFOObjects[0].Technical_API_Name__c;
            }
            else
            {
                objectName = '';
            }            
            system.debug('objectName' +objectName);
            String objectPermissionString = 'SELECT Id, ParentId, Parent.Name, Parent.Label FROM ObjectPermissions' 
                                             +' WHERE sObjectType = :objectName' 
                                             +' AND Parent.IsOwnedByProfile = False';
            
            if(selectedAccessLevel != null && selectedAccessLevel != '') {
                    
                if(selectedAccessLevel == 'Create') {
                    objectPermissionString  += ' AND PermissionsCreate = True';
                }
                else if(selectedAccessLevel == 'Read') {
                    objectPermissionString  += ' AND PermissionsRead = True';
                }
                else if(selectedAccessLevel == 'Edit') {
                    objectPermissionString  += ' AND PermissionsEdit = True';
                }
                else if(selectedAccessLevel == 'Delete') {
                    objectPermissionString  += ' AND PermissionsDelete = True';
                }
                else if(selectedAccessLevel == 'ViewAll') {
                    objectPermissionString  += ' AND PermissionsViewAllRecords = True';
                }
                else if(selectedAccessLevel == 'ModifyAll') {
                    objectPermissionString  += ' AND PermissionsModifyAllRecords = True';
                }                               
            } 
            
            additionalCriteria = true;   
            
            if(searchCriteria.searchIndividualPsOnly__c) {
                
                searchResultMap.clear();
                Set<Id> permissionSetIdSet = new Set<Id>();
            
                for(ObjectPermissions objPermissions:database.query(objectPermissionString )) {
                    permissionSetIdSet.add(objPermissions.ParentId);
                }
            
                for(PermissionSet PermissionSet:[SELECT Id, Label, 
                                                 (SELECT PermissionsCreate, PermissionsRead, 
                                                 PermissionsEdit, PermissionsDelete, 
                                                 PermissionsViewAllRecords, PermissionsModifyAllRecords                                                 
                                                 FROM ObjectPerms WHERE sObjectType = :objectName)
                                                 FROM PermissionSet WHERE Id IN :permissionSetIdSet]) {
                    
                    String permissionSetDescription = '';
                    
                    for(ObjectPermissions objPerm:PermissionSet.ObjectPerms) {
                        permissionSetDescription = getDescription(objPerm);
                    }
                    
                    PermissionSetGroup__c permissionSetGroup = new PermissionSetGroup__c(Name          = PermissionSet.Label,
                                                                                         Description__c = permissionSetDescription, 
                                                                                         MemberList__c = '<font size="0.65"><a href="/'+PermissionSet.Id+'">'+PermissionSet.Label +'</a><br/></font>');                
                
                    searchResultMap.put(PermissionSet.Id, permissionSetGroup);
                }
            }
            else {
                for(ObjectPermissions objPermissions:database.query(objectPermissionString )) {
                    PermissionSetId.add(objPermissions.ParentId);
                }
                
                system.debug('PermissionSetId = ' + PermissionSetId);
                
                searchString += ' Id IN (SELECT PermissionSetGroup__c FROM PsGroupMember__c WHERE PermissionSetId__c IN :PermissionSetId)';           
                               
            }
        }
        
        if(searchName != null && searchName != '') {
            searchString += additionalCriteria ? ' AND' : '';
            searchString += ' Name LIKE \'%' + searchName + '%\'';
            additionalCriteria = true;
        }
    
        if(searchType != null && searchType != '') {
            searchString += additionalCriteria ? ' AND' : '';
            searchString += ' Type__c = \'' + searchType + '\'';
            additionalCriteria = true;
        }

        if(searchStream != null && searchStream != '') {
            searchString += additionalCriteria ? ' AND' : '';
            searchString += ' Stream__c = \'' + searchStream + '\'';
            additionalCriteria = true;
        }        
        
        system.debug('Query = ' + searchString);
        system.debug('additionalCriteria = ' + additionalCriteria);
        system.debug('selectedAccessLevel  = ' + selectedAccessLevel );
        
        if((selectedAccessLevel != null || selectedAccessLevel == '') && (objectName == null || objectName == '')) {
             ApexPages.Message noSelectedCriteria = new ApexPages.Message(ApexPages.Severity.ERROR,'You must select the object for which the level of access applies.');
             ApexPages.addMessage(noSelectedCriteria);
        }       
        else if(!additionalCriteria) {
             ApexPages.Message noSelectedCriteria = new ApexPages.Message(ApexPages.Severity.ERROR,'You must select at least one search criteria.');
             ApexPages.addMessage(noSelectedCriteria);
        }
        else if(!searchCriteria.searchIndividualPsOnly__c){
            
            searchResultMap.clear();
            List<PermissionSetGroup__c> psGroupList = Database.query(searchString);

            if(psGroupList.size() <= 20) {
                for(PermissionSetGroup__c permissionSetGroup:Database.query(searchString)) {
                    populateHTMLFields(permissionSetGroup);
                    permissionSetGroup.TECH_IsAssigned__c = existingAssignmentsMap.keySet().contains(permissionSetGroup.Id);
          
                    searchResultMap.put(permissionSetGroup.Id, permissionSetGroup);
                }              
            }
            else {
                ApexPages.Message noSelectedCriteria = new ApexPages.Message(ApexPages.Severity.ERROR,'Your search returned more than 20 results. Only the first 20 are displayed. Please refine search criteria.');
                ApexPages.addMessage(noSelectedCriteria);            
            }
        }
    }
    
    public String getDescription(ObjectPermissions objectPermission) {
        
        String description = '';
        Boolean existingValue = false;
        
        if(objectPermission.PermissionsCreate) {
            description  += 'Create';
            existingValue = true;
        }
        
        if(objectPermission.PermissionsRead) {
            description  += (existingValue ? ', ' : '') + 'Read';
            existingValue = true;
        }
        
        if(objectPermission.PermissionsEdit) {
            description  += (existingValue ? ', ' : '') + 'Edit';
            existingValue = true;
        }
        
        if(objectPermission.PermissionsDelete) {
            description  += (existingValue ? ', ' : '') + 'Delete';
            existingValue = true;
        }
        
        if(objectPermission.PermissionsViewAllRecords) {
            description += (existingValue ? ', ' : '') + 'View All Records';
            existingValue = true;
        }
        
        if(objectPermission.PermissionsModifyAllRecords) {
            description  += (existingValue ? ', ' : '') + 'Modify All Records';
            existingValue = true;
        }  
        
        return description;
    }
}