/*
Class Name       : VFC_MyCallsTodayRedirect_TEST
Purpose          : Test Class for VFC_MyCallsTodayRedirect
Created by       : Rakhi Kumar(Global Delivery Team)
Date created     : 13 Mar 2013
-----------------------------------------------------------------
                              MODIFICATION HISTORY
-----------------------------------------------------------------
Modified By     : <>
Modified Date   : <>
Description     : <>
-----------------------------------------------------------------
*/
@isTest
private class VFC_MyCallsTodayRedirect_TEST {
    static testMethod void testMyCallsTodayRedirect() {
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        
        //Set current page context    
        PageReference currentPage = Page.VFP_MyCallsTodayRedirect;
        Test.SetCurrentPage(currentPage);
        VFC_MyCallsTodayRedirect pageController = new VFC_MyCallsTodayRedirect();
        //No ANI Number parameter 
        System.Debug('##################           Unit Test - myCallsTodayRedirect - Begin          ##################');
        PageReference myCallsTodayPage = pageController.myCallsTodayRedirect();
        if(myCallsTodayPage != null)
            System.Debug('##### - Page URL - ' + myCallsTodayPage.getUrl() + ' #####');
        System.Debug('##################           Unit Test - myCallsTodayRedirect - End            ##################');
    }

}