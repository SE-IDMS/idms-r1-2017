// Created by Uttara PR - Nov 2016 Release - Escape Notes project

@isTest
global class VFC_GetStockDetails_Test {
    
    static testMethod void method1() {
                
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        insert runAsUser;
        Id userId = UserInfo.getUserId();
        
        System.runAs(runAsUser) {
            
            BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                accOrg1.Name='Test';
                accOrg1.Entity__c='Test Entity-2'; 
                insert  accOrg1;
            BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                    accOrg2.Name='Test';
                    accOrg2.Entity__c='Test Entity-2'; 
                    accOrg2.SubEntity__c='Test Sub-Entity 2';
                    insert  accOrg2;
            BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                    accOrg.Name='Test';
                    accOrg.Entity__c='Test Entity-2';  
                    accOrg.SubEntity__c='Test Sub-Entity 2';
                    accOrg.Location__c='Test Location 2';
                    accOrg.Location_Type__c='Design Center';
                    insert  accOrg;
            
            BackOfficesystem__c BOS = new BackOfficesystem__c();
            BOS.FrontOfficeOrganization__c = accOrg.Id;
            BOS.BackOfficeSystem__c = 'Oracle ITB';
            BOS.ERPKey__c = 'ITB_OrganizationID';
            BOS.ExternalID__c = '119';
            
            Problem__c prob = Utils_TestMethods.createProblem(accOrg.id);
            prob.Severity__c = 'Safety Related';        
            prob.RecordTypeId = Label.CLI2PAPR120014;
            insert prob;
            
            List<CommercialReference__c> crlst = new list<CommercialReference__c>();
            CommercialReference__c cr1 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family1234', ProductLine__c='1234ProductLine', CommercialReference__c='AR3100' );
            crlst.add(cr1);
            CommercialReference__c cr2 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family12345', ProductLine__c='1234ProductLine5', CommercialReference__c='AR3300' );
            crlst.add(cr2);
            CommercialReference__c cr3 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family123', ProductLine__c='123ProductLine', CommercialReference__c='' );
            crlst.add(cr3);
            CommercialReference__c cr4 = new CommercialReference__c(Problem__c = prob.id, Family__c= 'Family12', ProductLine__c='12ProductLine', CommercialReference__c='0J-0N-1439' );
            crlst.add(cr4);
            insert crlst;
            
            ApexPages.StandardSetController StockController = new ApexPages.StandardSetController(crlst);
            PageReference pageRef = Page.VFP_GetStockDetails; 
            ApexPages.currentPage().getParameters().put('Id', prob.id);
            StockController.setSelected(new CommercialReference__c []{crlst[0], crlst[1], crlst[2]});
            VFC_GetStockDetails displaystock = new VFC_GetStockDetails(StockController);
            
            
            VFC_GetStockDetails.Error err = new VFC_GetStockDetails.Error('testing');
            VFC_GetStockDetails.Organization org = new VFC_GetStockDetails.Organization('ABC','1234','DISTRIBUTION',accOrg.Name);
            List<VFC_GetStockDetails.Stock> lstStock1 = new List<VFC_GetStockDetails.Stock>();
            lstStock1.add(new VFC_GetStockDetails.Stock('testname','100'));
            List<VFC_GetStockDetails.Product> prod1 = new List<VFC_GetStockDetails.Product>();
            VFC_GetStockDetails.Product prod = new VFC_GetStockDetails.Product('test1', 'test2', org, lstStock1, err);
            prod1.add(prod);
            VFC_GetStockDetails.ResponseObtained RO = new VFC_GetStockDetails.ResponseObtained(prod1, 'testing');

            VFC_GetStockDetails.StockDisplayWrapper mywclist1 = new VFC_GetStockDetails.StockDisplayWrapper(prod);
            List<VFC_GetStockDetails.StockDisplayWrapper> mywclist = new List<VFC_GetStockDetails.StockDisplayWrapper>();
            mywclist.add(mywclist1);
            
            
            displaystock.Cancel(); 
            
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new GetStockDetailsMock());
            displaystock.Confirm();
            Test.stopTest();
            
            displaystock.AddImpactedOrganizations();
        }
    } 
    
    global class GetStockDetailsMock implements HttpCalloutMock {
        global HttpResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            
            String accessToken = Label.CL112016I2P20; // CL112016I2P20 = 'ad223e6d06938489aa2f29f74d0e4457';
            res.setHeader('Authorization','Bearer '+ accessToken);
            res.setHeader('Content-Type','application/json');
            res.setHeader('Accept',System.Label.CLOCT15FIO018);
            res.setHeader(System.Label.CLOCT15FIO043,System.Label.CLOCT15FIO044);
            res.setHeader(System.Label.CLOCT15FIO045,System.Label.CLOCT15FIO046);
            res.setHeader(System.Label.CLOCT15FIO047,System.Label.CLOCT15FIO048);
            res.setHeader(System.Label.CLOCT15FIO049,System.Label.CLOCT15FIO048);
            res.setStatusCode(200);
            res.setBody('\"Product:[error=null, organization=Organization:[FOOrg=GSC India - Logistic DC - Bangalore - BED, code=BED, id=2414, type_Z=DISTRIBUTION], selected=null, sku=AR3100, stock=(Stock:[currency1=null, name=NetQuantity, value=71]), targetSku=null]"');
            return res; 
        }
    }
}