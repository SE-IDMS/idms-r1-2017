/********************************************************************
* Company: Fielo
* Created Date: 25/08/2016
* Description: Test class for class FieloPRM_TeamMemberTrigger
********************************************************************/
@isTest
public with sharing class FieloPRM_TeamMemberTriggerTest{
  
    public static testMethod void testUnit1(){

        User us = new user(id = userinfo.getuserId());

        us.BypassVR__c = true;
        update us;
        
        System.runas(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            
            program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
            update program;
    
            Account acc = new Account(Name = 'acc');
            insert acc;
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c= 'Polo', 
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test', 
                F_Account__c = acc.Id
            );
            insert member;
            
            Contact c = new contact(
                Firstname = 'test',
                Lastname = 'test2',
                AccountId = acc.Id,
                FieloEE__member__c = member.id     
            );
            insert c;
    
            member = [select FieloEE__User__c, F_Account__c from fieloee__member__c where id =: member.Id]; 
    
            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc.Id});
    
            FieloCH__Team__c team = new FieloCH__Team__c(
                Name = FieloPRM_Utils_ChTeam.TEAM_NAME_PREFIX + acc.Name , 
                FieloCH__Program__c = Program.Id
            );
            insert team;
            
            FieloCH__TeamMember__c teamMember = new FieloCH__TeamMember__c(
                FieloCH__Team2__c = team.Id,
                FieloCH__Member2__c = member.Id
            );
            insert teamMember;
            
            try{
            
                FieloCH__TeamMember__c teamMember2 = new FieloCH__TeamMember__c(
                    FieloCH__Team2__c = team.Id,
                    FieloCH__Member2__c = member.Id
                );
                insert teamMember2;
            }catch(Exception e){
            }
                        
       }
       
    }
}