@isTest
private class FieloPRM_VPE_ChallengeCreateMenuExtTest {
    
    @isTest static void test_method_one() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            CountryChannels__c channels = new CountryChannels__c();
            insert channels;
            
            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;

            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c();
            parentMenu.FieloEE__Title__c = 'Test';
            parentMenu.F_PRM_Type__c = 'Template';
            parentMenu.FieloEE__Program__c = program.id;
            parentMenu.Name = 'My Programs';
            parentMenu.F_PRM_MenuFilter__c = 'P-US-test';
            insert parentMenu ;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.FieloEE__Title__c = 'Test';
            thisMenu.FieloEE__Program__c = program.id;
            thisMenu.FieloEE__ExternalName__c = 'MyPrograms';
            thisMenu.F_PRM_MenuFilter__c = 'P-US-test2';
            insert thisMenu;
            
            FieloPRM_MenuOptions__c options = new FieloPRM_MenuOptions__c();
            options.Name = 'Option1';
            options.F_PRM_Help__c = 'Big';
            options.F_PRM_Label__c = 'Lalala';
            insert options;
            
            List<FieloEE__Menu__c> menus = new List<FieloEE__Menu__c>();
            menus.add(thisMenu);
            
            FieloEE__Menu__c thisMenu2 = new FieloEE__Menu__c();
            thisMenu2.FieloEE__Title__c = 'Test';
            thisMenu2.F_PRM_Type__c = 'Template';
            insert thisMenu2;

            PRMCountry__c prmCountry = new PRMCountry__c();
            prmCountry.Name = 'Test';
            prmCountry.CountryPortalEnabled__c = true;
            prmCountry.PLDatapool__c = 'abcd';
            insert prmcountry;
            
            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c();
            challenge.FieloCH__Subscription__c = 'Global';
            challenge.Name = 'Challenge test';
            challenge.FieloCH__InitialDate__c = date.today();
            challenge.FieloCH__FinalDate__c = date.today().adddays(50);
            challenge.F_PRM_Country__c = prmcountry.id;
            challenge.F_PRM_ChallengeFilter__c = 'CH' + datetime.now();
            insert challenge;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(challenge);
            
            test.startTest();
            FieloPRM_VPE_ChallengeCreateMenuExt  thisExt = new FieloPRM_VPE_ChallengeCreateMenuExt (sc);     
            thisExt.getTemplatesMenu();
            System.assert(thisExt.nextStep() == null);
            thisExt.selectedoption = 'NewMenu';
            System.assert(thisExt.nextStep() != null);
            List<SelectOption> listOpt = thisExt.getItems();
            System.assert(listOpt.size()>0);
            test.stopTest();
        }
    
    }
    
    @isTest static void test_method_two() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            CountryChannels__c channels = new CountryChannels__c();
            insert channels;
            
            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c();
            parentMenu.FieloEE__Title__c = 'Test';
            parentMenu.F_PRM_Type__c = 'Template';
            parentMenu.FieloEE__Program__c = program.id;
            parentMenu.Name = 'My Programs';
            insert parentMenu ;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.FieloEE__Title__c = 'Test';
            thisMenu.FieloEE__Program__c = program.id;
            thisMenu.FieloEE__ExternalName__c = 'MyPrograms';
            insert thisMenu;
            
            FieloEE__Component__c compo = new FieloEE__Component__c(); 
            compo.FieloEE__Menu__c = thisMenu.id;
            insert compo;
            
            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            
            FieloPRM_MenuOptions__c options = new FieloPRM_MenuOptions__c();
            options.Name = 'Option1';
            options.F_PRM_Help__c = 'Big';
            options.F_PRM_Label__c = 'Lalala';
            insert options;
            
            List<FieloEE__Menu__c> menus = new List<FieloEE__Menu__c>();
            menus.add(thisMenu);
            
            FieloEE__Menu__c thisMenu2 = new FieloEE__Menu__c();
            thisMenu2.FieloEE__Title__c = 'Test';
            thisMenu2.F_PRM_Type__c = 'Template';
            insert thisMenu2;
            
            PRMCountry__c prmCountry = new PRMCountry__c();
            prmCountry.Name = 'Test';
            prmCountry.CountryPortalEnabled__c = true;
            prmCountry.PLDatapool__c = 'abcd';
            insert prmcountry;

            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c();
            challenge.FieloCH__Subscription__c = 'Global';
            challenge.Name = 'Challenge test';
            challenge.FieloCH__InitialDate__c = date.today();
            challenge.FieloCH__FinalDate__c = date.today().adddays(50);
            challenge.F_PRM_Country__c = prmcountry.id;
            challenge.F_PRM_ChallengeFilter__c = 'CH' + datetime.now();
            insert challenge;
            
            FieloEE__SegmentDomain__c segDomain = new FieloEE__SegmentDomain__c();
            segDomain.FieloCH__Challenge__c = challenge.id; 
            segDomain.FieloEE__Menu__c  = thisMenu.id ;
            segDomain.FieloEE__Segment__c = segment.id;
            insert segDomain;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(challenge);
            
            test.startTest();
            FieloPRM_VPE_ChallengeCreateMenuExt  thisExt = new FieloPRM_VPE_ChallengeCreateMenuExt (sc);     
            thisExt.getTemplatesMenu();
            System.assert(thisExt.nextStep() == null);
            thisExt.selectedMenu = thisMenu.Id;
            thisExt.selectedoption = 'CreateTemplateStructure';
            System.assert(thisExt.nextStep() != null);
            
            List<SelectOption> listOpt = thisExt.getItems();
            System.assert(listOpt.size()>0);
            
            test.stopTest();
        }
    
    }
    
    @isTest()
    static void test_method_trhee() {
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            CountryChannels__c channels = new CountryChannels__c();
            insert channels;
            
            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c();
            parentMenu.FieloEE__Title__c = 'Test';
            parentMenu.F_PRM_Type__c = 'Template';
            parentMenu.FieloEE__Program__c = program.id;
            parentMenu.Name = 'My Programs';
            insert parentMenu ;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.FieloEE__Title__c = 'Test';
            thisMenu.FieloEE__Program__c = program.id;
            thisMenu.FieloEE__ExternalName__c = 'MyPrograms';
            insert thisMenu;
            
            FieloEE__Category__c category = FieloPRM_UTILS_MockUpFactory.createCategory(thisMenu.id);
            
            FieloEE__Tag__c tag = new FieloEE__Tag__c();
            tag.name = 'test';
            insert tag;
            
            FieloEE__Component__c component = new FieloEE__Component__c();
            component.FieloEE__Menu__c = thisMenu.id;
            component.FieloEE__Tag__c = tag.id;
            insert component;
            
            FieloEE__News__c contentFeed = new FieloEE__News__c();
            contentFeed.FieloEE__IsActive__c = true;
            contentFeed.FieloEE__Component__c = component.id;
            contentFeed.FieloEE__CategoryItem__c = category.id;
            insert contentFeed;
            
            FieloEE__Banner__c banner = new FieloEE__Banner__c();
            banner.FieloEE__isActive__c = false;
            banner.FieloEE__Component__c  = component.id;
            banner.FieloEE__Category__c = category.id;
            banner.Name = 'test';
            insert banner;
            
            FieloEE__TagItem__c tagItemB = new FieloEE__TagItem__c();
            tagItemB.FieloEE__Banner__c = banner.id;
            tagItemB.FieloEE__Tag__c = tag.id;
            insert tagItemB ;
            
            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c();
            tagItem.FieloEE__News__c = contentFeed.id;
            tagItem.FieloEE__Tag__c = tag.id;
            insert tagItem;
            
            FieloEE__Component__c compo = new FieloEE__Component__c(); 
            compo.FieloEE__Menu__c = thisMenu.id;
            insert compo;
            
            FieloPRM_MenuOptions__c options = new FieloPRM_MenuOptions__c();
            options.Name = 'Option1';
            options.F_PRM_Help__c = 'Big';
            options.F_PRM_Label__c = 'Lalala';
            insert options;
            
            List<FieloEE__Menu__c> menus = new List<FieloEE__Menu__c>();
            menus.add(thisMenu);
            
            FieloEE__Menu__c thisMenu2 = new FieloEE__Menu__c();
            thisMenu2.FieloEE__Title__c = 'Test';
            thisMenu2.F_PRM_Type__c = 'Template';
            insert thisMenu2;
            
            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
           
            PRMCountry__c prmCountry = new PRMCountry__c();
            prmCountry.Name = 'Test1';
            prmCountry.CountryPortalEnabled__c = true;
            prmCountry.PLDatapool__c = 'abcd1';
            insert prmcountry;

            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c();
            challenge.FieloCH__Subscription__c = 'Global';
            challenge.Name = 'Challenge test';
            challenge.FieloCH__InitialDate__c = date.today();
            challenge.FieloCH__FinalDate__c = date.today().adddays(50);
            challenge.F_PRM_Country__c = prmcountry.id;
            challenge.F_PRM_ChallengeFilter__c = 'CH' + datetime.now();
            insert challenge;
            
            FieloEE__SegmentDomain__c segDomain = new FieloEE__SegmentDomain__c();
            segDomain.FieloCH__Challenge__c = challenge.id; 
            segDomain.FieloEE__Menu__c  = thisMenu.id ;
            segDomain.FieloEE__Segment__c = segment.id;
            insert segDomain;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(challenge);
            
            test.startTest();
            FieloPRM_VPE_ChallengeCreateMenuExt  thisExt = new FieloPRM_VPE_ChallengeCreateMenuExt (sc);
            thisExt.selectedMenu = thisMenu.Id;    
            thisExt.getTemplatesMenu();
            System.assert(thisExt.nextStep() == null);
            thisExt.selectedMenu = thisMenu.Id;
            thisExt.selectedoption = 'CreateTemplateStructureContent';
            System.assert(thisExt.nextStep() != null);
            
            List<SelectOption> listOpt = thisExt.getItems();
            System.assert(listOpt.size()>0);
            
            test.stopTest();
        }
    
    }
    
    @isTest static void test_method_four() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c();
            parentMenu.FieloEE__Title__c = 'Test';
            parentMenu.F_PRM_Type__c = 'Template';
            parentMenu.FieloEE__Program__c = program.id;
            parentMenu.Name = 'My Programs';
            insert parentMenu ;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.FieloEE__Title__c = 'Test';
            thisMenu.FieloEE__Program__c = program.id;
            thisMenu.FieloEE__ExternalName__c = 'MyPrograms';
            insert thisMenu;

            PRMCountry__c prmCountry = new PRMCountry__c();
            prmCountry.Name = 'Test';
            prmCountry.CountryPortalEnabled__c = true;
            prmCountry.PLDatapool__c = 'abcd';
            insert prmcountry;

            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c();
            challenge.FieloCH__Subscription__c = 'Global';
            challenge.Name = 'Challenge test';
            challenge.FieloCH__InitialDate__c = date.today();
            challenge.FieloCH__FinalDate__c = date.today().adddays(50);
            challenge.F_PRM_Country__c = prmcountry.id;
            challenge.F_PRM_ChallengeFilter__c = 'CH' + datetime.now();
            insert challenge;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(challenge);
            
            test.startTest();
            FieloPRM_VPE_ChallengeCreateMenuExt  thisExt = new FieloPRM_VPE_ChallengeCreateMenuExt (sc);     
            thisExt.getTemplatesMenu();
            thisExt.redirect();
            thisExt.skipButtom();
            thisExt.firstStep = true;
            thisExt.skipButtom();
            System.assert(thisExt.nextStep() == null);
            thisExt.selectedMenu = thisMenu.Id;
            thisExt.selectedoption = 'CreateTemplateStructureContent';
            System.assert(thisExt.nextStep() != null);
            List<SelectOption> listOpt = thisExt.getItems();
            System.assert(listOpt.size()>0);
            test.stopTest();
        }
    }
    
    @isTest static void test_method_five() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c();
            parentMenu.FieloEE__Title__c = 'Test';
            parentMenu.F_PRM_Type__c = 'Template';
            parentMenu.FieloEE__Program__c = program.id;
            parentMenu.Name = 'My Programs';
            insert parentMenu ;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.FieloEE__Title__c = 'Test';
            thisMenu.FieloEE__Program__c = program.id;
            thisMenu.FieloEE__ExternalName__c = 'MyPrograms';
            insert thisMenu;

            PRMCountry__c prmCountry = new PRMCountry__c();
            prmCountry.Name = 'Test';
            prmCountry.CountryPortalEnabled__c = true;
            prmCountry.PLDatapool__c = 'abcd';
            insert prmcountry;

            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c();
            challenge.FieloCH__Subscription__c = 'Global';
            challenge.Name = 'Challenge test';
            challenge.FieloCH__InitialDate__c = date.today();
            challenge.FieloCH__FinalDate__c = date.today().adddays(50);
            challenge.F_PRM_Country__c = prmcountry.id;
            challenge.F_PRM_ChallengeFilter__c = 'CH' + datetime.now();
            insert challenge;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(challenge);
            
            test.startTest();
            FieloPRM_VPE_ChallengeCreateMenuExt  thisExt = new FieloPRM_VPE_ChallengeCreateMenuExt (sc);     
            thisExt.getTemplatesMenu();
            thisExt.redirect();
            thisExt.firstStep = false;
            thisExt.skipButtom();
            thisExt.firstStep = true;
            thisExt.skipButtom();
            System.assert(thisExt.nextStep() == null);
            thisExt.selectedMenu = thisMenu.Id;
            thisExt.selectedoption = 'CreateTemplateStructureContent';
            System.assert(thisExt.nextStep() != null);
            List<SelectOption> listOpt = thisExt.getItems();
            System.assert(listOpt.size()>0);
            test.stopTest();
        }
    }
    
    @isTest static void test_method_six() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            CountryChannels__c channels = new CountryChannels__c();
            insert channels;
            
            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c();
            parentMenu.FieloEE__Title__c = 'Test';
            parentMenu.F_PRM_Type__c = 'Template';
            parentMenu.FieloEE__Program__c = program.id;
            parentMenu.Name = 'My Programs';
            insert parentMenu ;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.FieloEE__Title__c = 'Test';
            thisMenu.FieloEE__Program__c = program.id;
            thisMenu.FieloEE__ExternalName__c = 'MyPrograms';
            insert thisMenu;
            
            FieloEE__Component__c compo = new FieloEE__Component__c(); 
            compo.FieloEE__Menu__c = thisMenu.id;
            insert compo;
            
            FieloPRM_MenuOptions__c options = new FieloPRM_MenuOptions__c();
            options.Name = 'Option1';
            options.F_PRM_Help__c = 'Big';
            options.F_PRM_Label__c = 'Lalala';
            insert options;
            
            List<FieloEE__Menu__c> menus = new List<FieloEE__Menu__c>();
            menus.add(thisMenu);
            
            FieloEE__Menu__c thisMenu2 = new FieloEE__Menu__c();
            thisMenu2.FieloEE__Title__c = 'Test';
            thisMenu2.F_PRM_Type__c = 'Template';
            insert thisMenu2;
            
            PRMCountry__c prmCountry = new PRMCountry__c();
            prmCountry.Name = 'Test';
            prmCountry.CountryPortalEnabled__c = true;
            prmCountry.PLDatapool__c = 'abcd';
            insert prmcountry;

            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c();
            challenge.FieloCH__Subscription__c = 'Global';
            challenge.Name = 'Challenge test';
            challenge.FieloCH__InitialDate__c = date.today();
            challenge.FieloCH__FinalDate__c = date.today().adddays(50);
            challenge.F_PRM_Country__c = prmcountry.id;
            challenge.F_PRM_ChallengeFilter__c = 'CH' + datetime.now();
            insert challenge;
            
            ApexPages.StandardController sc = new ApexPages.StandardController(challenge);
            
            test.startTest();
            FieloPRM_VPE_ChallengeCreateMenuExt  thisExt = new FieloPRM_VPE_ChallengeCreateMenuExt (sc);     
            thisExt.getTemplatesMenu();
            System.assert(thisExt.nextStep() == null);
            thisExt.selectedMenu = thisMenu.Id;
            thisExt.selectedoption = 'CreateTemplateStructure';
            System.assert(thisExt.nextStep() != null);
            List<SelectOption> listOpt = thisExt.getItems();
            System.assert(listOpt.size()>0);
            test.stopTest();
        }
    }
}