@isTest//(SeeAllData=true)
public class AP_TechnicalExpertAssessment_Test{

    static TestMethod void testingupdateCSIFlagOnTex(){
        

        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.RecordTypeId = Label.CLOCT13ACC08;
        insert objAccount;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );
        
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCaseForAction;
        
        User newUser = Utils_TestMethods.createStandardUser('TestUser');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false); 
        
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
        breEntity.Entity__c = 'Test Entity1';
        Database.insert(breEntity);
        BusinessRiskEscalationEntity__c breSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
        breSubEntity.Entity__c = 'Test Entity1';
        Database.insert(breSubEntity);
        BusinessRiskEscalationEntity__c breLocationType = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation ();
        breLocationType.Entity__c = 'Test Entity1';
        Database.insert(breLocationType);
        
         
        List<TEX__C> Listtech=New List<TEX__C>();
        TEX__c temptech=createTEX(objCaseForAction.id,newUser.id);
        TEX__c temptech_new=createTEX(objCaseForAction.id,newUser.id);
        temptech.Material_Status__c='Delivered to Expert Center';
        temptech.ContactEmail__c='chilukurirams@gmail.com';
        temptech.Status__c='Expert Assessment to be started';
        temptech.Material_Status__c='In Transit to Expert Center';
        temptech.ExpertCenter__c = breEntity.Id;
        //Listtech.add(temptech); 
        //Listtech.add(temptech_new);       
        insert temptech; 
        insert temptech_new;
            
        Utils_SDF_Methodology.removeFromRunOnce('AP_TEXConfirmCheck');
        temptech.OfferExpert__c=UserInfo.getUserId();
        temptech.Status__c='Expert Assessment in progress';
        temptech.AssessmentType__c='Remote Expert Assessment';
        temptech.Confirm__c=True;
        temptech.ExpertCenter__c = breSubEntity.Id;
        System.debug('Test - temptech: '+temptech);
        update temptech;
        
        Set<String> vSetTEXId = new Set<String>();
        map<String,TEX__c> mapvSetTEXId = new map<String,TEX__c>();
        vSetTEXId.add(temptech.Id);
        vSetTEXId.add(temptech_new.Id);  
mapvSetTEXId.put(temptech.Id,temptech);     
mapvSetTEXId.put(temptech_new.Id,temptech_new);     
        
        RMA__c temRMA=createReturnRequest(objCaseForAction.Id,objAccount.id, country.Id,temptech_new.id);
        temRMA.TEX__c = temptech_new.Id;
        insert temRMA;
        system.debug('!!!! temRMA : ' + temRMA);
       
        AP_TechnicalExpertAssessment.updateOfferExpertonTex(new List<TEX__c> {temptech_new});
        temptech.OfferExpert__c=UserInfo.getUserId();
        temptech.Status__c='Expert Assessment in progress';
        temptech.AssessmentType__c='Remote Expert Assessment';
        temptech.Confirm__c=True;
        update temptech;         
        
        List<BusinessRiskEscalationEntity__c> breList = new List<BusinessRiskEscalationEntity__c>();
        BusinessRiskEscalationEntity__c breEntitySubEntity = new BusinessRiskEscalationEntity__c(Entity__c='er61Test Entity15234',SubEntity__c = 'SubEntity');      
        insert breEntitySubEntity;
        // insert breList; 
        breList.add(breEntity);
        breList.add(breEntitySubEntity);
        
        CustomerSafetyIssue__c tempCSI=createCustomerSafetyIssue(objCaseForAction.id,objAccount.id,country.id, breList[0].id);  
        
        insert tempCSI;
        tempCSI.RelatedbFOCase__c= objCaseForAction.Id;
        update tempCSI ;
        list<RMA_Product__c> listRtm=new list< RMA_Product__c>();
         
        RMA_Product__c tempRTM=createReturnItem(temRMA.id);        
        listRtm.add(tempRTM);      
        insert listRtm;        
        
        set<string> setRtid=new set<string>();
        setRtid.add(tempRTM.id);
        
        Test.startTest(); 
        AP_TechnicalExpertAssessment.updateCSIFlagOnTex(new List<TEX__c> {temptech_new});
        AP_TechnicalExpertAssessment.updateCSIFlagOnTexRunOnCSI(new List<CustomerSafetyIssue__c> {tempCSI});
      
        map<id,string> ListCSI = new map<id,string>();
        ListCSI.put(objCaseForAction.id,'false');
        
        AP_TechnicalExpertAssessment.updateCSIFlagOnTexRunOnCSIAfterUpadate(ListCSI);
        
        RMA__c temRMA1=createReturnRequest(objCaseForAction.Id,objAccount.id, country.Id,temptech_new.id);
        temRMA1.TEX__c=temptech.Id;
        temRMA1.Status__c='Validated';
        insert temRMA1;
        
        RMA_Product__c tempRTM1=createReturnItem(temRMA1.id); 
        tempRTM1.ApprovalStatus__c='Approved';
        List<RMA_Product__c> listRtm1 = new List<RMA_Product__c>();
                
        listRtm1.add(tempRTM1);      
        insert listRtm1;
        Utils_SDF_Methodology.removeFromRunOnce('AP_TechnicalExpertAssessment');
        temptech.Material_Status__c='Delivered to Expert Center';
        update temptech;
        AP_TechnicalExpertAssessment.getTEXList(vSetTEXId);
        AP_TechnicalExpertAssessment.updateMaterialStatusTEX(listRtm1);
               
        AP_TechnicalExpertAssessment.updateCaseOwner(new List<TEX__c> {temptech_new,temptech});
        
        AP_TechnicalExpertAssessment.RejectReturnRequests(new List<TEX__c> {temptech_new});      
        
        AP_TechnicalExpertAssessment.getRTItemList(new set<string> {listRtm[0].TECH_TEX__c});       
        AP_TechnicalExpertAssessment.getRTItemList(vSetTEXId );
        
        AP_TechnicalExpertAssessment.updateRILogisticsStatusStatusRunOnTEX(mapvSetTEXId);
               
        Map<string,string> mapofTexidMStus = new Map<string,string>();
        mapofTexidMStus.put(tempRTM.id,'Partially Received at Expert Center');
        AP_TechnicalExpertAssessment.getUpdatedTEXList(mapofTexidMStus,new List<TEX__c> {temptech_new});
          
       //AP_TechnicalExpertAssessment.updatingMaterialStatusOnTEX(new List<RMA_Product__c> {tempRTM});
        tempRTM.LogisticsStatus__c=label.CLOCT13RR15;
        update tempRTM;             
        //AP_TechnicalExpertAssessment.updateMaterialStatusOnTEXRunOnRI(setRtid);
        tempRtm.LogisticsStatus__c=label.CLOCT13RR05;
        RMA_Product__c RT1=new RMA_Product__c();
        RT1.Name='Testt';
        RT1.RMA__c=temRMA.id;
        RT1.ProductCondition__c='Defective';
        RT1.CustomerResolution__c='RFC - Return For Credit';
        RT1.Quantity__c=2;
        RT1.LogisticsStatus__c ='Tesr';
        RT1.ApprovalStatus__c ='Test';
        
        insert RT1;
        AP_TechnicalExpertAssessment.getRTItemList(new set<string> {RT1.TECH_TEX__c});
        Test.stopTest();
    }
   
    
    static TestMethod void testRejectReturnRequests(){
        Test.startTest(); 
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        objContact.Country__c= country.id ;
        insert objContact;
         
        
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCaseForAction;
        User newUser = Utils_TestMethods.createStandardUser('TestUser');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false); 
         
        List<TEX__C> Listtech=New List<TEX__C>();
        TEX__c temptech=createTEX(objCaseForAction.id,newUser.id);
        temptech.Material_Status__c='Delivered to Expert Center';
      
        Listtech.add(temptech);       
        insert Listtech; 
        AP_TechnicalExpertAssessment.RejectReturnRequests(Listtech); 
        }
        
    static TestMethod void testingCloseTEXandCase(){
        Test.startTest(); 

        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact1');
        insert objContact;
           Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );
        
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCaseForAction;
        User newUser = Utils_TestMethods.createStandardUser('TestUser1');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);          
        List<TEX__C> Listtech=New List<TEX__C>();
        TEX__c temptech=createTEX(objCaseForAction.id,newUser.id);        
        temptech.Status__c='Closed';
         Listtech.add(temptech);
        insert Listtech;    
         TEX__c temptech1=createTEX(objCaseForAction.id,newUser.id);  
         insert temptech1;
         temptech1.Status__c =Label.CLOCT13RR11;
        // update temptech1; 
        AP_TechnicalExpertAssessment.updateCaseOwner(Listtech);        
         
    }  
    
     
    public static TEX__c createTEX(id caseid,id userid){
            TEX__C tech=New TEX__C();
            tech.AssessmentType__c='Product Return to Expert Center';
            tech.Case__c=caseid;
            tech.Status__c='Expert Assessment to be started';
            tech.Prioritylevel__c='Normal';
            tech.FrontOfficeTEXManager__c=userid;
            tech.OfferExpert__c=null;
            return tech;
    }
         
    public static RMA__c createReturnRequest(id caseid,id acctid, id cuntryid,id techid) {
       
            RMA__c RMA = new RMA__c();
            RMA.case__c = caseid; 
            RMA.AccountName__c = acctid; 
            RMA.TEX__c=techid;

        //Account Address
            RMA.AccountStreet__c  = 'Account Street';
            RMA.AccountCity__c    = 'Account City';
            RMA.AccountZipCode__c = '12345';
            RMA.AccountCountry__c = cuntryid;
            RMA.AccountPOBox__c   = 'Account PO Box';
            RMA.ProductCondition__c='Defective';

        //Shipping Address
            RMA.ShippingStreet__c  = 'Shipping Street';
            RMA.ShippingCity__c    = 'Shipping City';
            RMA.ShippingZipCode__c = '67890';
            RMA.ShippingCountry__c = cuntryid; 
            RMA.ShippingPOBox__c   = 'Shipping PO Box';
            return RMA;
    }
    
    
    public static CustomerSafetyIssue__c createCustomerSafetyIssue(id caseid,id acctid, id cuntryid,id BREid) {
    
        CustomerSafetyIssue__c  csiHDRwithGSA=new CustomerSafetyIssue__c();
        csiHDRwithGSA.AffectedCustomer__c = acctid;
        csiHDRwithGSA.CountryOfOccurrence__c = cuntryid;        
        csiHDRwithGSA.IssueInvestigationLeader__c = UserInfo.getUserId();          
        csiHDRwithGSA.RelatedOrganization__c =BREid; 
        csiHDRwithGSA.RelatedbFOCase__c=caseid; 
        csiHDRwithGSA.BodilyInjury__c = 'Occurrence Reported';
        csiHDRwithGSA.PropertyDamage__c= 'Occurrence Reported';      
        csiHDRwithGSA.TypeOfInjury__c = 'Death by electrocution';
        csiHDRwithGSA.InjuredPartyAffiliation__c = 'Customer';
        csiHDRwithGSA.CauseOfInjury__c = 'Electric shock';
        csiHDRwithGSA.TypeOfPropertyDamage__c = 'Fire damage to contents';
        csiHDRwithGSA.CauseOfPropertyDamage__c = 'Explosion';
        csiHDRwithGSA.HumanDeathReported__c = True;  
        return csiHDRwithGSA;
        //csiList.add(csiHDRwithGSA);        
    }
    
    
    public static RMA_Product__c createReturnItem(id rmaID) { 
       
       RMA_Product__c RT=new RMA_Product__c();
       RT.Name='200201140';
       RT.RMA__c=rmaID;
       RT.ProductCondition__c='Defective';
       RT.CustomerResolution__c='RFC - Return For Credit';
       RT.Quantity__c=2;
       
       return RT;
    }  
    
    static testMethod void method1() {
        
        Test.startTest();
        String commRefTOTest='TEST_XBTGT5430';
        String gmrCodeToTest='02BF6DRG1234'; 
    
        User newUser = Utils_TestMethods.createStandardUser('TestUser');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
    
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.RecordTypeId = Label.CLOCT13ACC08;
        insert objAccount;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        Country__c country = Utils_TestMethods.createCountry();
        Database.insert(country );
        
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        objCaseForAction.CaseSymptom__c= 'Symptom';
        objCaseForAction.CaseSubSymptom__c= 'SubSymptom';
        objCaseForAction.CommercialReference__c = commRefTOTest;
        objCaseForAction.TECH_GMRCode__c = gmrCodeToTest;
        objCaseForAction.ProductBU__c = 'Business';
        objCaseForAction.ProductLine__c = 'ProductLine';
        objCaseForAction.ProductFamily__c ='ProductFamily';
        objCaseForAction.Family__c = 'Family';
        objCaseForAction.LevelOfExpertise__c = 'Advanced';
        insert objCaseForAction;
        
        Case objCaseForAction1 = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        objCaseForAction1.CaseSymptom__c= 'Symptom';
        objCaseForAction1.CaseSubSymptom__c= 'SubSymptom';
        objCaseForAction1.CommercialReference__c = commRefTOTest;
        objCaseForAction1.TECH_GMRCode__c = gmrCodeToTest;
        objCaseForAction1.ProductBU__c = 'Business';
        objCaseForAction1.ProductLine__c = 'ProductLine';
        objCaseForAction1.ProductFamily__c ='ProductFamily';
        objCaseForAction1.Family__c = 'Family';
        objCaseForAction1.LevelOfExpertise__c = 'Expert';
        insert objCaseForAction1;        
                      
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
        breEntity.Entity__c = 'Test Entity1';
        Database.insert(breEntity);
      EntityStakeholder__c testusr= new EntityStakeholder__c();
        testusr.BusinessRiskEscalationEntity__c=breEntity.id;
        testusr.User__c=newUser.id;
        testusr.Role__c=Label.CLOCT15I2P14;
        insert testusr;
        BusinessRiskEscalationEntity__c breSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
        breSubEntity.Entity__c = 'Test Entity1';
        Database.insert(breSubEntity);
        BusinessRiskEscalationEntity__c breLocationType = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation ();
        breLocationType.Entity__c = 'Test Entity1';
        Database.insert(breLocationType);
            
        Problem__c prob = Utils_TestMethods.createProblem(breLocationType.Id,12);
        prob.ProblemLeader__c = newUser.id;
        prob.ProductQualityProblem__c = false;
        prob.TECH_Symptoms__c = 'Symptom - SubSymptom'; 
        prob.RecordTypeid = Label.CLI2PAPR120014;
        prob.Sensitivity__c = 'Public';
        prob.Severity__c = 'Safety related';
        Database.insert(prob);
        
        OPP_Product__c testProduct = Utils_TestMethods.createProduct('Business', 'ProductLine', 'ProductFamily', 'Family');
        insert testProduct;
        ProductLineQualityContactMapping__c PLIS = new ProductLineQualityContactMapping__c();
        PLIS.Product__c = testProduct.Id;
        PLIS.AccountableOrganization__c = breEntity.Id;
        insert PLIS;
        
        List<TEX__C> Listtech=New List<TEX__C>();
        TEX__c temptech=createTEX(objCaseForAction.id,newUser.id);
        TEX__c temptech_new=createTEX(objCaseForAction1.id,newUser.id);
        temptech.Material_Status__c='In Transit to Expert Center';
        
        temptech.ContactEmail__c='chilukurirams@gmail.com';
        temptech.Status__c='Expert Assessment to be started';
        temptech.RelatedProblem__c=prob.Id;
        temptech_new.RelatedProblem__c=prob.Id;
        temptech_new.LineofBusiness__c = breEntity.Id;
        temptech.LineofBusiness__c = breEntity.Id;
        temptech_new.ExpertCenter__c = breEntity.Id;
        Listtech.add(temptech); 
        Listtech.add(temptech_new);       
        insert Listtech; 
      //  Listtech[0].Material_Status__c='Delivered to Expert Center';
      //  update Listtech[0];
        
        AP_TechnicalExpertAssessment.CaseProblemLink(Listtech);
        
        Map<Id,Case> caseIds = new Map<Id,Case>();
        caseIds.put(objCaseForAction.Id,objCaseForAction);
        AP_TechnicalExpertAssessment.updateLineOfBusinessOnTEX(Listtech);
        AP_TechnicalExpertAssessment.updateLineOfBusinessOnTEXfromCase(caseIds);
        
        AP_TechnicalExpertAssessment.insertTEXStakeholder(Listtech,'After Update');
        AP_TechnicalExpertAssessment.insertTEXStakeholder(Listtech,'After Insert');
        
        objCaseForAction.LevelOfExpertise__c = 'Advanced';
        update objCaseForAction;
        AP_TechnicalExpertAssessment.updateCase(Listtech);
        
        
        Utils_SDF_Methodology.removeFromRunOnce('AP_TEXConfirmCheck');
        TEX__c temptech_new1=createTEX(objCaseForAction1.id,newUser.id);
        temptech_new1.RelatedProblem__c=prob.Id;
        temptech_new1.ExpertCenter__c = breEntity.Id;
        temptech_new1.LineofBusiness__c = breEntity.Id;
        insert temptech_new1;
        temptech_new1.AssessmentType__c=Label.CLOCT13RR20;
        temptech_new1.Confirm__c=True;
        temptech_new1.ExpertCenter__c = breSubEntity.Id;
        System.debug('Test - temptech_new1: '+temptech_new1);
        update temptech_new1;
        
        
        Test.stopTest(); 
    }        

  
}