/******************************************************/
/*
/* Question of the Week; 
/*
/* Question Template Preview
/*
/* Created on: 03/09/2012 SFN
/*
/* Purpose:
/*  handle the QOTWs.
/*
/******************************************************/

public with sharing class VFC_QuestionTemplateController 
{    
  public List <String>       Choices       = new List <String>();
  public List <AnswerChoice> AnswerChoices = new List <AnswerChoice>();
  public List <SelectOption> Options       = new List <SelectOption>();
  
  public List <SelectOption> getChoices() 
  {
    return options; 
  }

  public List <AnswerChoice> getAnswerChoices ()    
  {
    return AnswerChoices;    
  }

  public String   Choice              { get; set;}

  public String   OtherAnswer         { get; set;}
  public boolean  OtherAnswerSelected { get; set;}
  public String   OtherRemarks        { get; set;}

  public class AnswerChoice 
  {
    public String  Answer   { get; set;}
    public boolean Selected { get; set;}    
  } 
    
  /* Constructor */
             
  public VFC_QuestionTemplateController (ApexPages.StandardController controller)   
  {      
    QuestionTemplate__c template = (QuestionTemplate__c) controller.getRecord();
    
    Choices = template != null ? template.Answers__c.split('\n') : null;
  
    if (Choices != null)
    {
      AnswerChoice answer = null;
      
      for (String Choice : Choices)
      {
        answer = new AnswerChoice();
        
        answer.Answer   = Choice;
        answer.Selected = false;
        
        AnswerChoices.add (answer);
        
        Options.add(new SelectOption(Choice,Choice));
      }
    }
    
    if (template.IncludeOther__c == true)
    {
      Options.add(new SelectOption('Other','Other'));
    }
  }        
}