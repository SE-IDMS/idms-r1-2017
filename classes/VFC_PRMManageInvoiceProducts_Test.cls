@isTest
public Class VFC_PRMManageInvoiceProducts_Test{

    @testSetup static void testSetupMethodManageInvoice() {
        Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
        INSERT testCountry;
        StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
        INSERT testStateProvince;
        
        PRMCountry__c prmCountry = new PRMCountry__c(CountryPortalEnabled__c = true,
                                      CountrySupportEmail__c = 'abc@xyz.com',
                                      PLDatapool__c = 'rn_US',
                                      Country__c = testCountry.id
                                      );
        insert prmCountry;
        
        FieloEE.MockUpFactory.setCustomProperties(false);          
                
        Account acc = new Account();
        acc.Name = 'test acc';
        acc.PRMParticipateinRewardsProgram__c = true;
        acc.PRMCountryClusterSetup__c = prmCountry.id;
        insert acc;
                
        Contact con = new Contact();
            con.AccountId = acc.Id;
            con.FirstName = 'test contact Fn';
            con.LastName  = 'test contact Ln';
            con.PRMCompanyInfoSkippedAtReg__c = false;
            con.PRMCountry__c = testCountry.Id;
            con.PRMFirstName__c = 'PRM test contact Fn'; 
            con.PRMLastName__c  = 'PRM test contact Fn';
        insert con;
                
        List<Profile> profiles = [select id from profile where name='SE - Channel Partner (Community)'];
        if(profiles.size()>0){
            UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
            User u = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            u.ContactId = con.Id;
            u.PRMRegistrationCompleted__c = false;
            u.BypassWF__c = false;
            u.BypassVR__c = true;
            u.FirstName ='Test User First';
            insert u;
        }
            
        FieloEE__Member__c member = new FieloEE__Member__c(
            FieloEE__LastName__c = 'Polo',
            FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
            FieloEE__Street__c = 'Calle Falsa',
            F_PRM_PrimaryChannel__c = 'TST',F_Country__c= testCountry.Id,
            F_Account__c=acc.Id
        );
        insert member;
        System.debug('Member123 :'+member.Id);
        FieloEE.MemberUtil.setMemberId(member.Id);
    }
    
    public static testMethod void testMethod1(){
        
        Account acc = [SELECT Id,PRMCountryClusterSetup__c FROM Account WHERE Name=: 'test acc'];
        FieloEE__Member__c member = [SELECT Id FROM FieloEE__Member__c WHERE FieloEE__LastName__c=:'Polo'];
        FieloPRM_Invoice__c fieloPRMInvoice = new FieloPRM_Invoice__c();
            fieloPRMInvoice.F_PRM_RetailerAccount__c = acc.Id;
            fieloPRMInvoice.F_PRM_Status__c = 'Pending';
            fieloPRMInvoice.F_PRM_Member__c = member.Id;
            fieloPRMInvoice.F_PRM_InvoiceRejectionReason__c = acc.PRMCountryClusterSetup__c;
        INSERT fieloPRMInvoice;
        Test.StartTest();
            Apexpages.currentpage().getparameters().put('id', fieloPRMInvoice.Id);
            VFC_PRMManageInvoiceProducts vfc_prm = new VFC_PRMManageInvoiceProducts();
            vfc_prm.checkDuplicateInvoice();
            vfc_prm.OperationStatus = 'Approve';
            vfc_prm.ApproveOrReject();
        Test.StopTest();
    }
    
    public static testMethod void testMethod2(){
    
        Account acc = [SELECT Id,PRMCountryClusterSetup__c FROM Account WHERE Name=: 'test acc'];
        FieloEE__Member__c member = [SELECT Id FROM FieloEE__Member__c WHERE FieloEE__LastName__c=:'Polo'];
        FieloPRM_Invoice__c fieloPRMInvoice = new FieloPRM_Invoice__c();
            fieloPRMInvoice.F_PRM_RetailerAccount__c = acc.Id;
            fieloPRMInvoice.F_PRM_Status__c = 'Pending';
            fieloPRMInvoice.F_PRM_Member__c = member.Id;
            fieloPRMInvoice.F_PRM_InvoiceRejectionReason__c = 'Rejection';
            fieloPRMInvoice.F_PRM_InvoiceRejectionReason__c = acc.PRMCountryClusterSetup__c;
        INSERT fieloPRMInvoice;
             
        Test.StartTest();
            Apexpages.currentpage().getparameters().put('id', fieloPRMInvoice.Id);
            VFC_PRMManageInvoiceProducts vfc_prm = new VFC_PRMManageInvoiceProducts();
            
            vfc_prm.OperationStatus = 'Reject';
            vfc_prm.ApproveOrReject();
            
        Test.StopTest();
    }
    
    public static testMethod void testMethod3(){
    
        Account acc = [SELECT Id,PRMCountryClusterSetup__c FROM Account WHERE Name=: 'test acc'];
        FieloEE__Member__c member = [SELECT Id FROM FieloEE__Member__c WHERE FieloEE__LastName__c=:'Polo'];
        
        FieloPRM_Invoice__c fieloPRMInvoice = new FieloPRM_Invoice__c();
            fieloPRMInvoice.F_PRM_RetailerAccount__c = acc.Id;
            fieloPRMInvoice.F_PRM_Status__c = 'Pending';
            fieloPRMInvoice.F_PRM_Member__c = member.Id;
            fieloPRMInvoice.F_PRM_InvoiceRejectionReason__c = 'Rejection';
            fieloPRMInvoice.F_PRM_InvoiceRejectionReason__c = acc.PRMCountryClusterSetup__c;
        INSERT fieloPRMInvoice;
        
        Test.StartTest();
            Apexpages.currentpage().getparameters().put('id', fieloPRMInvoice.Id);
            VFC_PRMManageInvoiceProducts vfc_prm = new VFC_PRMManageInvoiceProducts();
            vfc_prm.doCancel();
            vfc_prm.deleteAttachment();
            
        Test.StopTest();
    }
    
    public static testMethod void testMethod4(){
    
        Account acc = [SELECT Id,PRMCountryClusterSetup__c FROM Account WHERE Name=: 'test acc'];
        FieloEE__Member__c member = [SELECT Id FROM FieloEE__Member__c WHERE FieloEE__LastName__c=:'Polo'];
        FieloPRM_Invoice__c fieloPRMInvoice = new FieloPRM_Invoice__c();
            fieloPRMInvoice.F_PRM_RetailerAccount__c = acc.Id;
            fieloPRMInvoice.F_PRM_Status__c = 'Pending';
            fieloPRMInvoice.F_PRM_Member__c = member.Id;
            fieloPRMInvoice.F_PRM_InvoiceRejectionReason__c = 'Rejection';
            fieloPRMInvoice.F_PRM_InvoiceRejectionReason__c = acc.PRMCountryClusterSetup__c;
        INSERT fieloPRMInvoice;
             
        Attachment attach=new Attachment();
            attach.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach.body=bodyBlob;
            attach.parentId=fieloPRMInvoice.id;
        insert attach;
        
        FieloPRM_InvoiceDetail__c invDetail = new FieloPRM_InvoiceDetail__c();
        invDetail.F_PRM_Invoice__c = fieloPRMInvoice.Id;
        invDetail.F_PRM_Volume__c = 20;
        INSERT invDetail;
        
        Test.StartTest();
            Apexpages.currentpage().getparameters().put('id', fieloPRMInvoice.Id);
            VFC_PRMManageInvoiceProducts vfc_prm = new VFC_PRMManageInvoiceProducts();
                vfc_prm.DelAttachmentId = attach.Id;
                vfc_prm.deleteAttachment();
            
            
            VFC_PRMManageInvoiceProducts.getInvoiceDetails('InvoiceId');
            VFC_PRMManageInvoiceProducts.InvoiceDetails invDetails = new VFC_PRMManageInvoiceProducts.InvoiceDetails(); 
                invDetails.InvoiceId = fieloPRMInvoice.Id;
                invDetails.RetailerAccountId = acc.Id;
                invDetails.InvoiceName = 'Invoice Name';
                invDetails.InvoiceDate = String.valueOf(Date.Today());
            
            VFC_PRMManageInvoiceProducts.InvoiceProductDetails updProducts = new VFC_PRMManageInvoiceProducts.InvoiceProductDetails();
            VFC_PRMManageInvoiceProducts.InvoiceProductDetails[] updProductsLst = new VFC_PRMManageInvoiceProducts.InvoiceProductDetails[]{};     
                updProducts.tobeDeleted = 'true';
                updProducts.InvoiceProdId = invDetail.Id;
            updProductsLst.add(updProducts);
            
            VFC_PRMManageInvoiceProducts.saveInvoiceProducts(updProductsLst,invDetails);
            
        Test.StopTest();
    }
    
    public static testMethod void testMethod5(){
        
        Account acc = [SELECT Id,PRMCountryClusterSetup__c FROM Account WHERE Name=: 'test acc'];
        FieloEE__Member__c member = [SELECT Id FROM FieloEE__Member__c WHERE FieloEE__LastName__c=:'Polo'];
        
        FieloPRM_Invoice__c fieloPRMInvoice = new FieloPRM_Invoice__c();
            fieloPRMInvoice.F_PRM_RetailerAccount__c = acc.Id;
            fieloPRMInvoice.F_PRM_Status__c = 'Pending';
            fieloPRMInvoice.F_PRM_Member__c = member.Id;
            fieloPRMInvoice.F_PRM_InvoiceRejectionReason__c = 'Rejection';
            fieloPRMInvoice.F_PRM_InvoiceRejectionReason__c = acc.PRMCountryClusterSetup__c;
        INSERT fieloPRMInvoice;
             
        Attachment attach=new Attachment();
            attach.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            attach.body=bodyBlob;
            attach.parentId=fieloPRMInvoice.id;
        insert attach;
        
        FieloPRM_InvoiceDetail__c invDetail = new FieloPRM_InvoiceDetail__c();
        invDetail.F_PRM_Invoice__c = fieloPRMInvoice.Id;
        invDetail.F_PRM_Volume__c = 20;
        INSERT invDetail;
        
        Test.StartTest();
            VFC_PRMManageInvoiceProducts.InvoiceDetails invDetails = new VFC_PRMManageInvoiceProducts.InvoiceDetails(); 
                invDetails.InvoiceId = fieloPRMInvoice.Id;
                invDetails.RetailerAccountId = acc.Id;
                invDetails.InvoiceName = 'Invoice Name';
                invDetails.InvoiceDate = String.ValueOf(Date.Today());
            
            VFC_PRMManageInvoiceProducts.InvoiceProductDetails updProducts = new VFC_PRMManageInvoiceProducts.InvoiceProductDetails();
            VFC_PRMManageInvoiceProducts.InvoiceProductDetails[] updProductsLst = new VFC_PRMManageInvoiceProducts.InvoiceProductDetails[]{};     
                updProducts.tobeDeleted = 'false';
                updProducts.InvoiceProdId = invDetail.Id;
                updProducts.InvoiceId = fieloPRMInvoice.Id;
            updProductsLst.add(updProducts);
            
            VFC_PRMManageInvoiceProducts.saveInvoiceProducts(updProductsLst,invDetails);
        Test.StopTest();
    }
    
    public static testMethod void testMethod6(){
        Test.StartTest();
            VFC_PRMManageInvoiceProducts.InvoiceDetails invDetails = new VFC_PRMManageInvoiceProducts.InvoiceDetails(); 
            VFC_PRMManageInvoiceProducts.InvoiceProductDetails updProducts = new VFC_PRMManageInvoiceProducts.InvoiceProductDetails();
            VFC_PRMManageInvoiceProducts.InvoiceProductDetails[] updProductsLst = new VFC_PRMManageInvoiceProducts.InvoiceProductDetails[]{};     
            updProductsLst.add(updProducts);
            VFC_PRMManageInvoiceProducts.saveInvoiceProducts(updProductsLst,invDetails);
        Test.StopTest();
    }
    
    public static testMethod void testMethod7(){
        
        Test.StartTest();
            VFC_PRMManageInvoiceProducts.SearchProductModel searchProdModel = new VFC_PRMManageInvoiceProducts.SearchProductModel();
            searchProdModel.sku = 'sku';
            searchProdModel.productDesc = 'productDesc';
            searchProdModel.productFamily = 'productFamily';
            searchProdModel.invCountry = 'invCountry';
            VFC_PRMManageInvoiceProducts.productAdvancedSearch(searchProdModel);
        Test.StopTest();
    }
}