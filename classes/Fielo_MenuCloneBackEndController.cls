/**
* @author Pablo Cassinerio
* @date 18/05/2016
* @company Fielo
*/
public class Fielo_MenuCloneBackEndController {

    private String retURL;
    private Id newId;
    private Id menuId;
    
    public Fielo_MenuCloneBackEndController() {
        if(ApexPages.currentPage().getParameters().get('menuId') != null){
            menuId = (Id) ApexPages.currentPage().getParameters().get('menuId');
        }
    }
    
    public PageReference cloneMenu(){
        
        Boolean hasErrors = false;
        
        Savepoint sp = Database.setSavepoint();
        
        FieloPRM_CreateMenuChannelCountryExt.isClone = true;
        newId = FieloEE.MenuUtil.cloneMenu(menuId);
        FieloPRM_CreateMenuChannelCountryExt.isClone = false;

        List<FieloEE__Menu__c> listMenusUpdate = [SELECT Id, FieloEE__RedemptionRule__c FROM FieloEE__Menu__c WHERE Id =: newId OR FieloEE__Menu__c =: newId];
        if(!listMenusUpdate.isEmpty()){
            for(FieloEE__Menu__c menu: listMenusUpdate){
                menu.FieloEE__RedemptionRule__c = null;
            }
            update listMenusUpdate;
        }
        
        List<FieloEE__Banner__c> listBannersUpdate = [SELECT Id, FieloEE__RedemptionRule__c, FieloEE__HasSegments__c FROM FieloEE__Banner__c WHERE FieloEE__Component__r.FieloEE__Menu__c =: newId];
        if(!listBannersUpdate.isEmpty()){
            for(FieloEE__Banner__c banner: listBannersUpdate){
                banner.FieloEE__RedemptionRule__c = null;
                banner.FieloEE__HasSegments__c = false;
            }
            update listBannersUpdate;
        }

        List<FieloEE__News__c> listContentFeedsUpdate = [SELECT Id, FieloEE__RedemptionRule__c, FieloEE__HasSegments__c FROM FieloEE__News__c WHERE FieloEE__Component__r.FieloEE__Menu__c =: newId];
        if(!listContentFeedsUpdate.isEmpty()){
            for(FieloEE__News__c cf: listContentFeedsUpdate){
                cf.FieloEE__RedemptionRule__c = null;
                cf.FieloEE__HasSegments__c = false;
            }
            update listContentFeedsUpdate;
        }
        
        FieloPRM_UTILS_MenuMethods.createEntitySubscriptions(new Set<String>{newId,menuId});
                            
        if(!hasErrors && newId != null){
            PageReference pag = new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/' + newId + '/e?saveURL=' + '/' + newId + '&retURL=' + '/' + newId);
            pag.setRedirect(true);
            return pag;
        }else{
            return null;
        }
    }
}