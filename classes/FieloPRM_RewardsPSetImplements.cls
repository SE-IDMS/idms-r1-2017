/********************************************************************
* Company: Fielo
* Created Date: 27/03/2016
* Description:
********************************************************************/
global without sharing class FieloPRM_RewardsPSetImplements implements FieloPRM_FeatureCustomLogic{

    public static final string permissionSetName = 'PRM_RewardsFrontEnd';
    
    public FieloPRM_RewardsPSetImplements(){
    }
     
    public void promote(Set<String> PRMUIMSIdList){
       assignPermissionSet(PRMUIMSIdList,permissionSetName);
    }

    //@future //removed in Q3 Release after the introduction of the F_PRM_IsSynchronic__c field
    private static void assignPermissionSet(Set<String> PRMUIMSIdList, String psetName){
        PermissionSet pset1 = [SELECT Id FROM PermissionSet WHERE Name =: psetName];
        List<PermissionSetAssignment> psetAssignList = new List<PermissionSetAssignment>();
       for(User user : [SELECT Id FROM User WHERE Contact.PRMUIMSId__c IN: PRMUIMSIdList AND Contact.PRMUIMSId__c != null]){
            PermissionSetAssignment psetAssign = new PermissionSetAssignment(
                PermissionSetId = pset1.id,
                AssigneeId = user.Id
            );
            psetAssignList.add(psetAssign);
       }
       insert psetAssignList;
    }

    public void demote(Set<String> PRMUIMSIdList){
        removePermissionSet(PRMUIMSIdList,permissionSetName);
    }

    //@future //removed in Q3 Release after the introduction of the F_PRM_IsSynchronic__c field
    private static void removePermissionSet(Set<String> PRMUIMSIdList, String psetName){
        List<PermissionSetAssignment> psetAssignList = [SELECT Id FROM PermissionSetAssignment WHERE Assignee.Contact.PRMUIMSId__c IN: PRMUIMSIdList AND PermissionSet.Name =: psetName];
        delete psetAssignList;
    }

}