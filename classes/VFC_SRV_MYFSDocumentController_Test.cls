@isTest
Public Class VFC_SRV_MYFSDocumentController_Test{
  Static TestMethod Void  Unitest(){
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
   ][0].Id;
  
  Country__c Country = new Country__c();
  Country.name= 'USA';
  Country.CountryCode__c = 'US';
  Insert Country;
  Account acc1 = new Account(Name = 'TestAccount1',Country__c = Country.Id,Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '111');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname11');
  insert cnct;
  
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

 /* user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'EN', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
   // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;*/
  //system.runAs(u) {
  
   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
    test.starttest();
   //Creating Work Order
   list<SVMXC__Service_Order__c> Wolist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrec= new SVMXC__Service_Order__c();
    newrec.SVMXC__Company__c=acc1.Id;
    newrec.SVMXC__Site__c=location1.Id;
    newrec.SVMXC__Contact__c=cnct.id;
    newrec.SVMXC__Scheduled_Date_Time__c=myDateTime;
    newrec.SVMXC__Order_Status__c = 'customer confirmed';
    Wolist.add(newrec);
   }
   insert wolist;
   list<SVMXC__Service_Order__c> WoUpcominglist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime1 = date.today().adddays(+60);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrecUpcoming= new SVMXC__Service_Order__c();
    newrecUpcoming.SVMXC__Company__c=acc1.Id;
    newrecUpcoming.SVMXC__Site__c=location1.Id;
    newrecUpcoming.SVMXC__Contact__c=cnct.id;
    newrecUpcoming.SVMXC__Scheduled_Date_Time__c=myDateTime1;
    newrecUpcoming.SVMXC__Order_Status__c = 'Service Complete';
    WoUpcominglist.add(newrecUpcoming);
   }
   insert WoUpcominglist;
    DateTime myDateTime2 = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    SVMXC__Service_Order__c newrecpast= new SVMXC__Service_Order__c();
    newrecpast.SVMXC__Company__c=acc1.Id;
    newrecpast.SVMXC__Site__c=location1.Id;
    newrecpast.SVMXC__Contact__c=cnct.id;
    newrecpast.SVMXC__Scheduled_Date_Time__c=myDateTime2;
    newrecpast.SVMXC__Order_Status__c = 'Service Complete';
    insert  newrecpast;
   Attachment attach= new Attachment();
   attach.Name = 'testbFS___Service_Intervention_Report.pdf' ;
   attach.parentId = newrecpast.Id;
   attach.Description ='Work Order Output Doc';
   Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
   attach.body=bodyBlob;
   insert attach; 
   
   Date Contractstartdate = date.Today().adddays(-60);
   Date Contractenddate = date.today().adddays(+1);
   SVMXC__Service_Contract__c ParentContract = New SVMXC__Service_Contract__c();
   ParentContract .SoldtoAccount__c = acc1.id;
   ParentContract .SVMXC__Service_Contract_Notes__c = 'Test';
   ParentContract .name= 'TestContract';
   ParentContract .Status__c= 'EXP';
   ParentContract .LeadingBusinessBU__c= 'GC';
   ParentContract .SVMXC__Start_Date__c =Contractstartdate ;
   ParentContract .SVMXC__end_Date__c = Contractenddate ;
   insert ParentContract ;
   
   SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
   Contract.SoldtoAccount__c = acc1.id;
   Contract.ParentContract__c = ParentContract.Id;
   Contract.SVMXC__Service_Contract_Notes__c = 'Test';
   Contract.name= 'TestContract';
   Contract.Status__c= 'EXP';
   Contract.LeadingBusinessBU__c= 'GC';
   Contract.SVMXC__Start_Date__c =  Contractstartdate ;
   Contract.SVMXC__end_Date__c = Contractenddate ;
   insert contract;
   
   CSSRVMYFS_Product_Type__c Cs1 = new CSSRVMYFS_Product_Type__c();
   Cs1.Name = 'BD';
   Cs1.Product_Type_Code__c = 'BD';
   Cs1.Product_Type_Value__c = 'Building Management & Security';
   Insert Cs1;
   CSSRVMYFS_Product_Type__c Cs2 = new CSSRVMYFS_Product_Type__c();
   Cs2.Name = 'EN';
   Cs2.Product_Type_Code__c = 'EN';
   Cs2.Product_Type_Value__c = 'Electrical Distribution';
   Insert Cs2;
   
   CSSRVMYFS_INTEREST_TYPES__c CSInterest = new CSSRVMYFS_INTEREST_TYPES__c();
   CSInterest.Name = '1 phase power';
   CSInterest.Interest_Code__c = '1 phase power';
   CSInterest.Interest_Value__c = '1 phase power';
   CSInterest.Request_Type_Code__c = 'Product (IT)';
   Insert CSInterest;
   
   CSSRVMYFS_INTEREST_TYPES__c CSInterest1 = new CSSRVMYFS_INTEREST_TYPES__c();
   CSInterest1.Name = 'Industry Service';
   CSInterest1.Interest_Code__c = 'Industry Service';
   CSInterest1.Interest_Value__c = 'Industry Service';
   CSInterest1.Request_Type_Code__c = 'Service (ID)';
   Insert CSInterest1;
   
   VFC_SRV_MYFSDocumentController DocumentController= new VFC_SRV_MYFSDocumentController();
   DocumentController.size = 1;
   DocumentController.site=location1.id;
   DocumentController.ContractType= 'Expired';
   DocumentController.fromdate= string.valueof(Contractstartdate) ;
   DocumentController.todate = '';
   DocumentController.SelectedCountry = Country.CountryCode__c;
   DocumentController.UltimateParentAccount = acc1.id;
   DocumentController.ContractsandInterventions();
   DocumentController.FromDateSelector();
   //DocumentController.ToDateSelector();
   DocumentController.expiredcontracts();
   DocumentController.LocationPopulate();
   DocumentController.getInterestTypes();
   DocumentController.getProductTypeList();
  // DocumentController.insertOpportunityNotification();
   
  
   Test.stoptest();
 // }
 }
 Static TestMethod Void  Unittest(){
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
   ][0].Id;
  
  Country__c Country = new Country__c();
  Country.name= 'USA';
  Country.CountryCode__c = 'US';
  Insert Country;
  Account acc1 = new Account(Name = 'TestAccount1',Country__c = Country.Id,Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '111');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname11');
  insert cnct;
  
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

 /* user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'EN', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
   // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;*/
  //system.runAs(u) {
  
   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
    test.starttest();
   //Creating Work Order
   list<SVMXC__Service_Order__c> Wolist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrec= new SVMXC__Service_Order__c();
    newrec.SVMXC__Company__c=acc1.Id;
    newrec.SVMXC__Site__c=location1.Id;
    newrec.SVMXC__Contact__c=cnct.id;
    newrec.SVMXC__Scheduled_Date_Time__c=myDateTime;
    newrec.SVMXC__Order_Status__c = 'customer confirmed';
    Wolist.add(newrec);
   }
   insert wolist;
   list<SVMXC__Service_Order__c> WoUpcominglist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime1 = date.today().adddays(+60);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrecUpcoming= new SVMXC__Service_Order__c();
    newrecUpcoming.SVMXC__Company__c=acc1.Id;
    newrecUpcoming.SVMXC__Site__c=location1.Id;
    newrecUpcoming.SVMXC__Contact__c=cnct.id;
    newrecUpcoming.SVMXC__Scheduled_Date_Time__c=myDateTime1;
    newrecUpcoming.SVMXC__Order_Status__c = 'Service Complete';
    WoUpcominglist.add(newrecUpcoming);
   }
   insert WoUpcominglist;
    DateTime myDateTime2 = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    SVMXC__Service_Order__c newrecpast= new SVMXC__Service_Order__c();
    newrecpast.SVMXC__Company__c=acc1.Id;
    newrecpast.SVMXC__Site__c=location1.Id;
    newrecpast.SVMXC__Contact__c=cnct.id;
    newrecpast.SVMXC__Scheduled_Date_Time__c=myDateTime2;
    newrecpast.SVMXC__Order_Status__c = 'Service Complete';
    insert  newrecpast;
   Attachment attach= new Attachment();
   attach.Name = 'testbFS___Service_Intervention_Report.pdf' ;
   attach.parentId = newrecpast.Id;
   attach.Description ='Work Order Output Doc';
   Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
   attach.body=bodyBlob;
   insert attach; 
   
   Date Contractstartdate = date.Today().adddays(-60);
   Date Contractenddate = date.today().adddays(+1);
   SVMXC__Service_Contract__c ParentContract = New SVMXC__Service_Contract__c();
   ParentContract .SoldtoAccount__c = acc1.id;
   ParentContract .SVMXC__Service_Contract_Notes__c = 'Test';
   ParentContract .name= 'TestContract';
   ParentContract .Status__c= 'EXP';
   ParentContract .LeadingBusinessBU__c= 'GC';
   ParentContract .SVMXC__Start_Date__c =Contractstartdate ;
   ParentContract .SVMXC__end_Date__c = Contractenddate ;
   insert ParentContract ;
   
   SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
   Contract.SoldtoAccount__c = acc1.id;
   Contract.ParentContract__c = ParentContract.Id;
   Contract.SVMXC__Service_Contract_Notes__c = 'Test';
   Contract.name= 'TestContract';
   Contract.Status__c= 'EXP';
   Contract.LeadingBusinessBU__c= 'GC';
   Contract.SVMXC__Start_Date__c =  Contractstartdate ;
   Contract.SVMXC__end_Date__c = Contractenddate ;
   insert contract;
   
   CSSRVMYFS_Product_Type__c Cs1 = new CSSRVMYFS_Product_Type__c();
   Cs1.Name = 'BD';
   Cs1.Product_Type_Code__c = 'BD';
   Cs1.Product_Type_Value__c = 'Building Management & Security';
   Insert Cs1;
   CSSRVMYFS_Product_Type__c Cs2 = new CSSRVMYFS_Product_Type__c();
   Cs2.Name = 'EN';
   Cs2.Product_Type_Code__c = 'EN';
   Cs2.Product_Type_Value__c = 'Electrical Distribution';
   Insert Cs2;
   
   CSSRVMYFS_INTEREST_TYPES__c CSInterest = new CSSRVMYFS_INTEREST_TYPES__c();
   CSInterest.Name = '1 phase power';
   CSInterest.Interest_Code__c = '1 phase power';
   CSInterest.Interest_Value__c = '1 phase power';
   CSInterest.Request_Type_Code__c = 'Product (IT)';
   Insert CSInterest;
   
   CSSRVMYFS_INTEREST_TYPES__c CSInterest1 = new CSSRVMYFS_INTEREST_TYPES__c();
   CSInterest1.Name = 'Industry Service';
   CSInterest1.Interest_Code__c = 'Industry Service';
   CSInterest1.Interest_Value__c = 'Industry Service';
   CSInterest1.Request_Type_Code__c = 'Service (ID)';
   Insert CSInterest1;
   
   VFC_SRV_MYFSDocumentController DocumentController= new VFC_SRV_MYFSDocumentController();
   DocumentController.size = 1;
   DocumentController.site=location1.id;
   DocumentController.ContractType= 'Expired';
   DocumentController.fromdate= string.valueof(Contractstartdate) ;
   DocumentController.todate = '';
   DocumentController.SelectedCountry = Country.CountryCode__c;
   DocumentController.UltimateParentAccount = acc1.id;
  
   DocumentController.getProductTypeList();
   DocumentController.insertOpportunityNotification();
   
  
   Test.stoptest();
 // }
 }
 Static TestMethod Void  Unitest4(){
  
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
  ][0].Id;
  Country__c Country = new Country__c();
  Country.name= 'India';
  Country.CountryCode__c = 'IN';
  Insert Country;
  Account acc1 = new Account(Name = 'TestAccount',Country__c = Country.Id,Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
  insert cnct;
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

 
   
   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
    test.starttest();
   //Creating Work Order
   list<SVMXC__Service_Order__c> Wolist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime = date.today().adddays(-60);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrec= new SVMXC__Service_Order__c();
    newrec.SVMXC__Company__c=acc1.Id;
    newrec.SVMXC__Site__c=location1.Id;
    newrec.SVMXC__Contact__c=cnct.id;
    newrec.SVMXC__Scheduled_Date_Time__c=myDateTime;
    newrec.SVMXC__Order_Status__c = 'customer confirmed';
    Wolist.add(newrec);
   }
   insert wolist;
   list<SVMXC__Service_Order__c> WoUpcominglist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime1 = date.today().adddays(+60);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrecUpcoming= new SVMXC__Service_Order__c();
    newrecUpcoming.SVMXC__Company__c=acc1.Id;
    newrecUpcoming.SVMXC__Site__c=location1.Id;
    newrecUpcoming.SVMXC__Contact__c=cnct.id;
    newrecUpcoming.SVMXC__Scheduled_Date_Time__c=myDateTime1;
    newrecUpcoming.SVMXC__Order_Status__c = 'Service Complete';
    WoUpcominglist.add(newrecUpcoming);
   }
   insert WoUpcominglist;
    DateTime myDateTime2 = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    SVMXC__Service_Order__c newrecpast= new SVMXC__Service_Order__c();
    newrecpast.SVMXC__Company__c=acc1.Id;
    newrecpast.SVMXC__Site__c=location1.Id;
    newrecpast.SVMXC__Contact__c=cnct.id;
    newrecpast.SVMXC__Scheduled_Date_Time__c=myDateTime2;
    newrecpast.SVMXC__Order_Status__c = 'Service Complete';
    insert  newrecpast;
   Attachment attach= new Attachment();
   attach.Name = 'testbFS___Service_Intervention_Report.pdf' ;
   attach.parentId = newrecpast.Id;
   attach.Description ='Work Order Output Doc';
   Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
   attach.body=bodyBlob;
   insert attach; 
   
   Date Contractstartdate = date.today().adddays(-60);
   Date Contractenddate = date.today();
   SVMXC__Service_Contract__c ParentContract = New SVMXC__Service_Contract__c();
   ParentContract .SoldtoAccount__c = acc1.id;
   ParentContract .SVMXC__Service_Contract_Notes__c = 'Test';
   ParentContract .name= 'TestContract';
   ParentContract .Status__c= 'CAN';
   ParentContract .LeadingBusinessBU__c= 'GC';
   ParentContract .SVMXC__Start_Date__c = Contractstartdate ;
   ParentContract .SVMXC__end_Date__c = Contractenddate ;
   insert ParentContract ;
   
   SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
   Contract.SoldtoAccount__c = acc1.id;
   Contract.ParentContract__c = ParentContract.Id;
   Contract.SVMXC__Service_Contract_Notes__c = 'Test';
   Contract.name= 'TestContract';
   Contract.Status__c= 'CAN';
   Contract.LeadingBusinessBU__c= 'GC';
   Contract.SVMXC__Start_Date__c = Contractstartdate ;
   Contract.SVMXC__end_Date__c = Contractenddate ;
   insert contract;
   
   VFC_SRV_MYFSDocumentController DocumentController= new VFC_SRV_MYFSDocumentController();
   DocumentController.size = 1;
   DocumentController.site='View All';
   DocumentController.ContractType= 'Expired';
   DocumentController.fromdate= string.valueof(Contractstartdate );
   DocumentController.todate = string.valueof(ContractEnddate) ;
   DocumentController.UltimateParentAccount = acc1.id;
   DocumentController.SelectedCountry = Country.CountryCode__c;
   DocumentController.FromDateSelector();
   DocumentController.ToDateSelector();
   DocumentController.expiredcontracts();
   DocumentController.LocationPopulate();
   
  
   Test.stoptest();
  //}
 }
  static testmethod void  unitest2(){
   
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
  ][0].Id;
  Country__c Country = new Country__c();
  Country.name= 'India';
  Country.CountryCode__c = 'IN';
  Insert Country;
  Account acc1 = new Account(Name = 'TestAccount',country__c=country.id, Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
  insert cnct;
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

  /*user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'EN', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
   // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;
  system.runAs(u) {*/
  
   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
    test.starttest();
   //Creating Work Order
   list<SVMXC__Service_Order__c> Wolist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrec= new SVMXC__Service_Order__c();
    newrec.SVMXC__Company__c=acc1.Id;
    newrec.SVMXC__Site__c=location1.Id;
    newrec.SVMXC__Contact__c=cnct.id;
    newrec.SVMXC__Scheduled_Date_Time__c=myDateTime;
    newrec.SVMXC__Order_Status__c = 'customer confirmed';
    Wolist.add(newrec);
   }
   insert wolist;
   list<SVMXC__Service_Order__c> WoUpcominglist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime1 = date.today().adddays(+60);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrecUpcoming= new SVMXC__Service_Order__c();
    newrecUpcoming.SVMXC__Company__c=acc1.Id;
    newrecUpcoming.SVMXC__Site__c=location1.Id;
    newrecUpcoming.SVMXC__Contact__c=cnct.id;
    newrecUpcoming.SVMXC__Scheduled_Date_Time__c=myDateTime1;
    newrecUpcoming.SVMXC__Order_Status__c = 'Service Complete';
    WoUpcominglist.add(newrecUpcoming);
   }
   insert WoUpcominglist;
    DateTime myDateTime2 = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    SVMXC__Service_Order__c newrecpast= new SVMXC__Service_Order__c();
    newrecpast.SVMXC__Company__c=acc1.Id;
    newrecpast.SVMXC__Site__c=location1.Id;
    newrecpast.SVMXC__Contact__c=cnct.id;
    newrecpast.SVMXC__Scheduled_Date_Time__c=myDateTime2;
    newrecpast.SVMXC__Order_Status__c = 'Service Complete';
    insert  newrecpast;
   Attachment attach= new Attachment();
   attach.Name = 'testbFS___Service_Intervention_Report.pdf' ;
   attach.parentId = newrecpast.Id;
   attach.Description ='Work Order Output Doc';
   Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
   attach.body=bodyBlob;
   insert attach; 
   
   Date Contractstartdate = date.newInstance(2016, 2, 1);
   Date Contractenddate = date.today().adddays(+1);
   SVMXC__Service_Contract__c ParentContract = New SVMXC__Service_Contract__c();
   ParentContract .SoldtoAccount__c = acc1.id;
   ParentContract .SVMXC__Service_Contract_Notes__c = 'Test';
   ParentContract .name= 'TestContract';
   ParentContract .Status__c= 'EXP';
   ParentContract .LeadingBusinessBU__c= 'GC';
   ParentContract .SVMXC__Start_Date__c = Contractstartdate ;
   ParentContract .SVMXC__end_Date__c = Contractenddate ;
   insert ParentContract ;
   
   SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
   Contract.SoldtoAccount__c = acc1.id;
   Contract.ParentContract__c = ParentContract.Id;
   Contract.SVMXC__Service_Contract_Notes__c = 'Test';
   Contract.name= 'TestContract';
   Contract.Status__c= 'EXP';
   Contract.LeadingBusinessBU__c= 'GC';
   Contract.SVMXC__Start_Date__c = Contractstartdate ;
   Contract.SVMXC__end_Date__c = Contractenddate ;
   insert contract;
   
   VFC_SRV_MYFSDocumentController DocumentController= new VFC_SRV_MYFSDocumentController();
   DocumentController.size = 1;
   DocumentController.site='View All';
   DocumentController.ContractType= 'Expired';
   DocumentController.fromdate= string.valueof(Contractstartdate );
   DocumentController.todate = '';
   DocumentController.SelectedCountry = Country.CountryCode__c;
   DocumentController.UltimateParentAccount = acc1.id;
   DocumentController.ContractsandInterventions();
   DocumentController.expiredcontracts();
   DocumentController.LocationPopulate();
   DocumentController.TodateSelector();
   DocumentController.processInterventionReports();
     
   Test.stoptest();
 // }
 }
 static testmethod void  unitest3(){
  
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
  ][0].Id;
  Country__c Country = new Country__c();
  Country.name= 'India';
  Country.CountryCode__c = 'IN';
  Insert Country;
  Account acc1 = new Account(Name = 'TestAccount',country__c=country.id, Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
  insert cnct;
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

 /* user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
   EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'EN', LocaleSidKey = 'en_US',
   ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
   UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
   // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
   isActive = True, BypassVR__c = True, contactId = cnct.id);
  insert u;*/
  //system.runAs(u) {
   
   SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id,PrimaryLocation__c=True,ToDelete__c = False);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
   //Creating Work Order
   list<SVMXC__Service_Order__c> Wolist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrec= new SVMXC__Service_Order__c();
    newrec.SVMXC__Company__c=acc1.Id;
    newrec.SVMXC__Site__c=location1.Id;
    newrec.SVMXC__Contact__c=cnct.id;
    newrec.SVMXC__Scheduled_Date_Time__c=myDateTime;
    newrec.SVMXC__Order_Status__c = 'customer confirmed';
    Wolist.add(newrec);
   }
   insert wolist;
    test.starttest();
   list<SVMXC__Service_Order__c> WoUpcominglist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime1 = date.today().adddays(+60);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrecUpcoming= new SVMXC__Service_Order__c();
    newrecUpcoming.SVMXC__Company__c=acc1.Id;
    newrecUpcoming.SVMXC__Site__c=location1.Id;
    newrecUpcoming.SVMXC__Contact__c=cnct.id;
    newrecUpcoming.SVMXC__Scheduled_Date_Time__c=myDateTime1;
    newrecUpcoming.SVMXC__Order_Status__c = 'Service Complete';
    WoUpcominglist.add(newrecUpcoming);
   }
   insert WoUpcominglist;
    DateTime myDateTime2 = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    SVMXC__Service_Order__c newrecpast= new SVMXC__Service_Order__c();
    newrecpast.SVMXC__Company__c=acc1.Id;
    newrecpast.SVMXC__Site__c=location1.Id;
    newrecpast.SVMXC__Contact__c=cnct.id;
    newrecpast.SVMXC__Scheduled_Date_Time__c=myDateTime2;
    newrecpast.SVMXC__Order_Status__c = 'Service Complete';
    insert  newrecpast;
   Attachment attach= new Attachment();
   attach.Name = 'testbFS___Service_Intervention_Report.pdf' ;
   attach.parentId = newrecpast.Id;
   attach.Description ='Work Order Output Doc';
   Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
   attach.body=bodyBlob;
   insert attach; 
   
   Date Contractstartdate = date.newInstance(2016, 2, 1);
   Date Contractenddate = date.today().adddays(+1);
   SVMXC__Service_Contract__c ParentContract = New SVMXC__Service_Contract__c();
   ParentContract .SoldtoAccount__c = acc1.id;
   ParentContract .SVMXC__Service_Contract_Notes__c = 'Test';
   ParentContract .name= 'TestContract';
   ParentContract .Status__c= 'PCV';
   ParentContract .LeadingBusinessBU__c= 'GC';
   ParentContract .SVMXC__Start_Date__c = Contractstartdate ;
   ParentContract .SVMXC__end_Date__c = Contractenddate ;
   insert ParentContract ;
   
   SVMXC__Service_Contract__c Contract = New SVMXC__Service_Contract__c();
   Contract.SoldtoAccount__c = acc1.id;
   Contract.ParentContract__c = ParentContract.Id;
   Contract.SVMXC__Service_Contract_Notes__c = 'Test';
   Contract.name= 'TestContract';
   Contract.Status__c= 'PCV';
   Contract.LeadingBusinessBU__c= 'GC';
   Contract.SVMXC__Start_Date__c = Contractstartdate ;
   Contract.SVMXC__end_Date__c = Contractenddate ;
   insert contract;
   
   VFC_SRV_MYFSDocumentController DocumentController= new VFC_SRV_MYFSDocumentController();
   DocumentController.size = 1;
   DocumentController.site=location1.id;
   DocumentController.ContractType= 'Current';
   DocumentController.fromdate= '';
   DocumentController.todate = string.valueof(ContractEnddate);
   DocumentController.SelectedCountry = Country.CountryCode__c;
   DocumentController.UltimateParentAccount = acc1.id;
   DocumentController.TodateSelector();
   DocumentController.LocationPopulate();
   DocumentController.next();
       
   Test.stoptest();
 // }
 }
 static testMethod void UnitTest5() {
  
  Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
   and developerName = 'Customer'
   limit 1
  ][0].Id;
 
 Country__c Country = new Country__c();
  Country.name= 'USA';
  Country.CountryCode__c = 'US';
  Insert Country;
  
  Account acc1 = new Account(Name = 'TestAccount',Country__c=Country.Id,Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
  insert acc1;
  Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
  insert cnct;
  Profile p = [SELECT Id FROM Profile WHERE Name =:System.Label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];

    SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id);
   insert location1;

   RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
    and developerName = 'LocationContactRole'
    limit 1
   ]; //[0].Id;
   Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
   insert ro;
    test.starttest();
      list<SVMXC__Service_Order__c> Wolist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime = DateTime.newInstance(2016, 2, 20, 00, 00, 00);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrec= new SVMXC__Service_Order__c();
    newrec.SVMXC__Company__c=acc1.Id;
    newrec.SVMXC__Site__c=location1.Id;
    newrec.SVMXC__Contact__c=cnct.id;
    newrec.SVMXC__Scheduled_Date_Time__c=myDateTime;
    newrec.SVMXC__Order_Status__c = 'customer confirmed';
    Wolist.add(newrec);
   }
   insert wolist;
   list<SVMXC__Service_Order__c> WoUpcominglist = new list<SVMXC__Service_Order__c>();
    DateTime myDateTime1 = date.today().adddays(+60);
    for(integer i=1;i<10;i++){
    SVMXC__Service_Order__c newrecUpcoming= new SVMXC__Service_Order__c();
    newrecUpcoming.SVMXC__Company__c=acc1.Id;
    newrecUpcoming.SVMXC__Site__c=location1.Id;
    newrecUpcoming.SVMXC__Contact__c=cnct.id;
    newrecUpcoming.SVMXC__Scheduled_Date_Time__c=myDateTime1;
    newrecUpcoming.SVMXC__Order_Status__c = 'Service Complete';
    WoUpcominglist.add(newrecUpcoming);
   }
   insert WoUpcominglist;
   
   //VFC_SRV_MYFsPersonalInformatinController Test Coverage
   
   VFC_SRV_MYFSDocumentController DocumentController= new VFC_SRV_MYFSDocumentController();
   DocumentController.size = 1;
   DocumentController.site=location1.id;
   DocumentController.ContractType= 'Expired';
   DocumentController.fromdate= '2016-06-01';
   DocumentController.todate = '';
   DocumentController.LocationPopulate();
   DocumentController.todateselector();
  // DocumentController.ContractsandInterventions();
   
  // }
 }
}