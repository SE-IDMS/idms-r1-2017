@isTest
public class VFC_DisplayAccountableOrgDetails_TEST {

static testMethod void displyAcctDetlsTest() {
Country__c CountryObjLCR = Utils_TestMethods.createCountry();
        Insert CountryObjLCR;
        // Create User
        User LCRTestUser = Utils_TestMethods.createStandardUser('LCRRasg');
        Insert LCRTestUser ;
        User LCRTestUserCq = Utils_TestMethods.createStandardUser('LCROwMCQ');
        Insert LCRTestUserCq ;
        // Create Account
        Account accountObj = Utils_TestMethods.createAccount();
        accountObj.Country__c=CountryObjLCR.Id; 
        insert accountObj;
        // Create Contact to the Account
        Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'LCRContact');
        insert ContactObj;
        OPP_Product__c ProductObj =Utils_TestMethods.createProduct('Energy', 'INENP - IED ELEC NETWORKS PROT', 'PROTEC RELAY ACCESSORY', 'TEST BLOC P991 RANGE');
        //ProductObj.SDHID__c='abcd1234';
        insert ProductObj;
        system.debug('****Test ProductObj'+ ProductObj);
        Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
        
        CaseObj.SupportCategory__c = 'Troubleshooting*';
        CaseObj.Symptom__c =  'Installation/ Setup';
        CaseObj.SubSymptom__c = 'Hardware';    
        CaseObj.Quantity__c = 4;
        CaseObj.Family__c = 'ADVANCED PANEL';
        CaseObj.CommercialReference__c='xbtg5230';
        CaseObj.ProductLine__c='PROTEC RELAY ACCESSORY';
        CaseObj.TECH_GMRCode__c='02BF6D8U';
        Insert CaseObj;
         BusinessRiskEscalationEntity__c lCROrganzObj1 = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST Repair',
                                            Entity__c='LCR Entity-1',
                                            SubEntity__c='LCR Sub-Entity1',
                                            Location__c='LCR Location 1',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'LCR Street1',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City1',
                                            RCZipCode__c = '500225',
                                            ContactEmail__c ='SCH1@schneider.com'
                                            );
        Insert lCROrganzObj1;
       
        ProductLineQualityContactMapping__c lCROrganzObj = new ProductLineQualityContactMapping__c(
                                            AccountableOrganization__c=lCROrganzObj1.id,
                                            Product__c=ProductObj.id,
                                            QualityContact__c=LCRTestUser.id                                            
                                            );
        Insert lCROrganzObj; 
        
       
        
        BusinessRiskEscalationEntity__c lCROrganzObj2 = new BusinessRiskEscalationEntity__c();
        lCROrganzObj2.Entity__c = 'Mechanisburg';
        lCROrganzObj2.name ='Mechanisburg';
        insert lCROrganzObj2;
        BusinessRiskEscalationEntity__c lCROrganzObj3 = new BusinessRiskEscalationEntity__c();
          BusinessRiskEscalationEntity__c RCorg1 = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST2',
                                            Entity__c='RC Entity-2',
                                            SubEntity__c='RC Sub-Entity2',
                                            Location__c='RC Location2',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street2',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'RC City2',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg1;  
    list<RMAShippingToAdmin__c> tlist1=new list<RMAShippingToAdmin__c>();
  
    RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = CountryObjLCR.Id,
                                 Capability__c = 'Repair defective product',
                                 CommercialReference__c='xbtg5230',
                                 TECH_ProductFamily__c='HMI SCHNEIDER BRAND',
                                 ReturnCenter__c = lCROrganzObj1.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c='02BF6DU9'
                                 );
    Insert RPDT;
    tlist1.add(RPDT);
    
    RMAShippingToAdmin__c RPDT1 = new RMAShippingToAdmin__c (
                                 Country__c = CountryObjLCR.Id,
                                 Capability__c = 'Receive defective product',                                
                                 Ruleactive__c = True,
                                 CommercialReference__c='xbtg5230',
                                 TECH_ProductFamily__c='HMI SCHNEIDER BRAND',
                                 ReturnCenter__c = lCROrganzObj1.Id,
                                 TECH_GMRCode__c='02BF6DU9'
                                 );
    Insert RPDT1;
   tlist1.add(RPDT1); 
        ApexPages.currentPage().getParameters().put('Id', CaseObj.Id);
        ApexPages.StandardController updatesymptom = new ApexPages.StandardController(CaseObj);
        VFC_DisplayAccountableOrgDetails UpdteSymtm = new VFC_DisplayAccountableOrgDetails(updatesymptom); 
        list<RMAShippingToAdmin__c> tlist=UpdteSymtm.getLocalRCs(); 
        
        
        CaseObj.ProductLine__c=ProductObj.ProductLine__c;
        update CaseObj;
        
        ApexPages.StandardController C = new ApexPages.StandardController(CaseObj);
        VFC_DisplayAccountableOrgDetails displayContacts = new VFC_DisplayAccountableOrgDetails(C); 
        list<RMAShippingToAdmin__c> rclist =displayContacts.getLocalRCs(); 
        
        CaseObj.CommercialReference__c='xbtgT5230';
        CaseObj.ProductLine__c='';
        CaseObj.TECH_GMRCode__c='02BF6EI8';
        update CaseObj;
        
        ApexPages.StandardController C1 = new ApexPages.StandardController(CaseObj);
        VFC_DisplayAccountableOrgDetails displayContacts1 = new VFC_DisplayAccountableOrgDetails(C1); 
        list<RMAShippingToAdmin__c> rclist1 =displayContacts1.getLocalRCs(); 
        
}
}