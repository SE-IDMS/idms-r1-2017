/*************************************************************************************************************
    DATE          CREATED BY      PURPOSE..
    18-03-14     Deepak          For The Purpose of -Not allowing to create duplicate covered Product     
************************************************************************************************************ */
Public Class AP_CoveredProducttrigger
{   
    public static void AddError(List<SVMXC__Service_Contract_Products__c> cpList1)
    {   
       
        set<string> s = New set<string>();
        List<SVMXC__Service_Contract_Products__c> cpList2 = New List<SVMXC__Service_Contract_Products__c>();
        Set<String> existKey = new Set<String>();
        
        
        for(SVMXC__Service_Contract_Products__c cp: cpList1)
        {   
            if(cp.SVMXC__Installed_Product__c !=null && cp.IncludedService__c !=null && cp.SVMXC__Service_Contract__c !=null)
            {
                s.add(String.valueOf(cp.SVMXC__Service_Contract__c).substring(0,15)+'-'+String.valueOf(cp.IncludedService__c).substring(0,15)+'-'+String.valueOf(cp.SVMXC__Installed_Product__c).substring(0,15));
            }
        }
        for(SVMXC__Service_Contract_Products__c  c:[Select Id,Tech_SetOfId__c From SVMXC__Service_Contract_Products__c Where Tech_SetOfId__c in :s])
        {
            existKey.add(c.Tech_SetOfId__c );
        
        }
        
        //cpList2=[Select Id,Tech_SetOfId__c From SVMXC__Service_Contract_Products__c Where Tech_SetOfId__c in :s];
                
        
         for(SVMXC__Service_Contract_Products__c cp: cpList1)
        {
            if(cp.SVMXC__Installed_Product__c !=null && cp.IncludedService__c !=null && cp.SVMXC__Service_Contract__c !=null)
            {
                String st =String.valueOf(cp.SVMXC__Service_Contract__c).substring(0,15)+'-'+String.valueOf(cp.IncludedService__c).substring(0,15)+'-'+String.valueOf(cp.SVMXC__Installed_Product__c).substring(0,15);
                if(existKey.contains(st))
                {
                    cp.addError('Duplicate Covered Product Found');
                }
               // s.add(String.valueOf(cp.SVMXC__Service_Contract__c).substring(0,15)+'-'+String.valueOf(cp.IncludedService__c).substring(0,15)+'-'+String.valueOf(cp.SVMXC__Installed_Product__c).substring(0,15));
            }
            //cp.addError('Duplicate Covered Product Found');
        }
        
    } 
    
    
    public static void DuplicateError(List<SVMXC_ApplicableProduct__c > apList1)
    {
    
    set<string> str = New set<string>();
    Set<String> existKey1= new Set<String>();
    
    for(SVMXC_ApplicableProduct__c  app:apList1)
    {
    
    if(app.skill__c!=null && app.product__c!=null)
    {
    
    str.add(String.valueOf(app.skill__c).substring(0,15)+'-'+String.valueOf(app.product__c).substring(0,15));
    
            }
    
    }
     for(SVMXC_ApplicableProduct__c  ap1:[select id,name,Tech_KeyConcatenate__c from SVMXC_ApplicableProduct__c where Tech_KeyConcatenate__c in :str])
        {
        existKey1.add(ap1.Tech_KeyConcatenate__c);
        
        
        } 
        
        for(SVMXC_ApplicableProduct__c ap2:apList1)
    {
    
    if(ap2.skill__c!=null && ap2.product__c!=null){
    string strng =String.valueOf(ap2.skill__c).substring(0,15)+'-'+String.valueOf(ap2.product__c).substring(0,15);
            if(existKey1.contains(strng))  
            {
            ap2.addError('This Product is already under this Skill ');
            }
           }  
        }
    } 
    
    public static void warrentyDuplicateError(List<SVMXC__Service_Template_Products__c>appList)
    {
    
    set<string> strr = New set<string>();
    Set<String> existKey2 = new Set<String>();
    for(SVMXC__Service_Template_Products__c  app:appList)
    {
    
    if(app.SVMXC__Service_Template__c!=null && app.product__c!=null){
    
    strr.add(String.valueOf(app.SVMXC__Service_Template__c).substring(0,15)+'-'+String.valueOf(app.product__c).substring(0,15));
    
            }
         }
        for(SVMXC__Service_Template_Products__c  ap3:[select id,name,Tech_WarrentyConcatenate__c from SVMXC__Service_Template_Products__c where Tech_WarrentyConcatenate__c in :strr])
        {
        existKey2.add(ap3.Tech_WarrentyConcatenate__c);
        
        
        } 
    for(SVMXC__Service_Template_Products__c ap4:appList)
    {
    
    if(ap4.SVMXC__Service_Template__c!=null && ap4.product__c!=null)
        {
    string strg =String.valueOf(ap4.SVMXC__Service_Template__c).substring(0,15)+'-'+String.valueOf(ap4.product__c).substring(0,15);
    if(existKey2.contains(strg))
            {
            ap4.addError('This Product is already under this Warranty Term');
            }
        }
      }
    } 
    
}