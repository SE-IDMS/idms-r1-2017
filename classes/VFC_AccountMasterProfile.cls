/*
    Controller for VFP_ViewAccountMasterProfile and VFP_CreateEditAccountMasterProfile
    Customer CLassification and MarketSegment we have taken to label in SOQL since in create mode we will have to show translated values of these fields in UI
*/
public class VFC_AccountMasterProfile{
    public AccountMasterProfile__c amp{ get; set; }{amp=new AccountMasterProfile__c();}    //AccountMasterProfile__c
    public Id accID{get;set;}
    public Id ampID{get;set;}
    public Account acc{ get; set; }{acc=new Account ();}
    public Decimal SumOfBUSplit{get;set;}    // holds sum of BU Split PAM values 
    public Decimal sumofBUDirectSales{get;set;} // holds sum of BU Direct Sales values 
    public Decimal sumofBUInDirectsales{get;set;} // holds sum of BU Indirect Sales values 
    public Boolean warningFlag{get;set;}{warningFlag = false ;}
    public Boolean hasNoError{get;set;}{hasNoError=TRUE;}
    public Boolean hasEditAccess{get;set;}
    public Boolean hasError{get;set;}
    public List<wrapPAMDetails> wrapPAMdetailsList{get;set;}{wrapPAMdetailsList= new List<wrapPAMDetails>();}
    public List<wrapscoring> wrapscoringList{get;set;} {wrapscoringList=new List<wrapscoring>();}
    
    public VFC_AccountMasterProfile(ApexPages.StandardController controller) {
        List<CAPCountryWeight__c> capCountryWeightList=new List<CAPCountryWeight__c>();
        List<CAPGlobalMasterData__c> capQuesAnsList=new List<CAPGlobalMasterData__c>();
        List<CAPGlobalMasterData__c> capQuesAnsGlobalList=new List<CAPGlobalMasterData__c>();
        Map<String,List<CAPCountryWeight__c>> capCountryWeightMap=new Map<String,List<CAPCountryWeight__c>>();

        // fetch the accountId and Account master profileid
        accID=(ID)System.currentPageReference().getParameters().get( 'accID' );
        
        if(System.currentPageReference().getParameters().get( 'ampID' )!=null && System.currentPageReference().getParameters().get( 'ampID' )!='null' )
            ampID=(ID)System.currentPageReference().getParameters().get( 'ampID' );
          
        // add question sequence to scoring wrapper
        for(Integer i=0;i<8;i++){
            wrapscoring ws=new wrapscoring();
            ws.qseq='Q'+(i+1);
            wrapscoringList.add(ws);
        } 
        if(accID!=null && ampID==null) 
            amp.Account__c=accID; 
        //for Edit Mode fetch details - ampID not null 
        if(ampID!=null){
            amp=[SELECT Account__c,Account__r.Tech_CL1Updated__c,AccountMasterProfile__c,Q1Rating__c ,Q2Rating__c ,Q3Rating__c ,Q4Rating__c ,Q5Rating__c ,Q6Rating__c ,Q7Rating__c ,Q8Rating__c ,CreatedById,CreatedDate,CurrencyIsoCode,DirectSales__c,Id,FieldServicesPAM__c,IndirectSales__c,FieldServicesDirectSales__c,FieldServicesIndirectSales__c,MarketShare__c,Name,Score__c,TotalPAM__c,SumofBUPAM__c,SumOfBUDirectSales__c,SumofBUIndirectSales__c,AccountClassificationLevel1__c,AccountMarketSegment__c FROM AccountMasterProfile__c where Id=:ampID];          
            wrapscoringList[0].rating=amp.Q1Rating__c ;
            wrapscoringList[1].rating=amp.Q2Rating__c ;
            wrapscoringList[2].rating=amp.Q3Rating__c ;
            wrapscoringList[3].rating=amp.Q4Rating__c ;
            wrapscoringList[4].rating=amp.Q5Rating__c ;
            wrapscoringList[5].rating=amp.Q6Rating__c ;
            wrapscoringList[6].rating=amp.Q7Rating__c ;
            wrapscoringList[7].rating=amp.Q8Rating__c ;        
        }
        
        System.debug('accID>>>>>>'+accID);
        //fetch Account Classification,currency and marketsegment details 
        if(accID!=null){
              // Check if User has access to modify Account  
              List<UserRecordAccess> usac=[select HasEditAccess,RecordId from  UserRecordAccess where UserId=:UserInfo.getUserId() and RecordId=:accID];
              System.debug('>>>usac'+usac+'>>>accID'+accID);
                if(usac.size()>0){
                    if(usac[0].hasEditAccess == TRUE)
                        hasEditAccess=TRUE; 
            }
            if(hasEditAccess!=TRUE && ampID==null){
                hasError=True;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,System.Label.CLQ316SLS001));  
            }
            acc=[select toLabel(ClassLevel1__c),CurrencyISOCode,toLabel(MarketSegment__c),Country__c from account where Id=:accID];
            if(acc.ClassLevel1__c==null){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CLQ316SLS002); 
                ApexPages.addMessage(myMsg);
             }
         System.debug('>>>acc>>>'+acc);
         //fetch Country weight master data based on classification and Country.
         capCountryWeightList=[Select Country__c,CountryWeight__c,QuestionSequence__c,tolabel(ClassificationLevel1__c),tolabel(MarketSegment__c) from CAPCountryWeight__c where ToLabel(ClassificationLevel1__c)=:acc.ClassLevel1__c and Country__c=:acc.country__c ORDER BY QuestionSequence__c ASC];
         //fetch the Question interpretation, answer Interpretation and weight based on  Classification  level 
         List<CAPGlobalMasterData__c> cwgList=new List<CAPGlobalMasterData__c>();
         cwgList=[SELECT QuestionInterpretation__c,AnswerInterpretation1__c,AnswerInterpretation2__c,AnswerInterpretation3__c,AnswerInterpretation4__c,ClassificationLevel1__c,Weight__c,QuestionSequence__c from CAPGlobalMasterData__c where ToLabel(ClassificationLevel1__c)=:acc.ClassLevel1__c OR ClassificationLevel1__c='Default' ORDER BY QuestionSequence__c ASC];
         for(CAPGlobalMasterData__c cwg:cwgList){
             if(cwg.ClassificationLevel1__c=='Default')
                 capQuesAnsGlobalList.add(cwg);
             else
                capQuesAnsList.add(cwg);
         }
       }
       
       for(CAPCountryWeight__c cwt:capCountryWeightList){
            String ky=cwt.Country__c+'_'+cwt.ClassificationLevel1__c+'_'+cwt.MarketSegment__c;//marketsegment may or not have value
            if(capCountryWeightMap.containskey(ky))
                capCountryWeightMap.get(ky).add(cwt);
            else
                capCountryWeightMap.put(ky,new List<CAPCountryWeight__c>{(cwt)});       
        }
        String ky=acc.Country__c+'_'+acc.ClassLevel1__c+'_'+acc.MarketSegment__c;// first preference country weight based on classification and market segment
        if(!capCountryWeightMap.ContainsKey(ky))
            ky=acc.Country__c+'_'+acc.ClassLevel1__c+'_'+null; // without market segment fetch the value
         System.debug('*******capCountryWeightMap '+capCountryWeightMap+'>>>>>>ky '+ky);
        //fetch weight from country data based on CL and MKT    
        if(capCountryWeightMap.ContainsKey(ky) && capCountryWeightMap.get(ky).size()==8){
            for(Integer i=0;i<8;i++)
                wrapscoringList[i].wgt=capCountryWeightMap.get(ky)[i].CountryWeight__c;
        }
      
        //fetch weight from Global data based on classification
        else if(capQuesAnsList.size()==8){
         for(Integer i=0;i<8;i++)
            wrapscoringList[i].wgt=capQuesAnsList[i].Weight__c;
      }
      //Consider default weight if nothing found
      else if(capQuesAnsGlobalList.size()==8){
         for(Integer i=0;i<8;i++)
                wrapscoringList[i].wgt=capQuesAnsGlobalList[i].Weight__c;
      }
      
            
       //Question Interpretation and Answer Interpretation as per custom classification L1 from CAPGlobalMasterData
       if(capQuesAnsList.size()==8){
            for(Integer i=0;i<8;i++)
              wrapscoringList[i].ques=capQuesAnsList[i].QuestionInterpretation__c;

           for(Integer i=0;i<8;i++){
              wrapscoringList[i].ans1=capQuesAnsList[i].AnswerInterpretation1__c;
              wrapscoringList[i].ans2=capQuesAnsList[i].AnswerInterpretation2__c;
              wrapscoringList[i].ans3=capQuesAnsList[i].AnswerInterpretation3__c;
              wrapscoringList[i].ans4=capQuesAnsList[i].AnswerInterpretation4__c;
           }
       }
      
      //Fetch the Default Question and Answer Interpretation from CAPGlobalMasterData if its not available for the respective classification
      else if(capQuesAnsGlobalList.size()==8){
           for(Integer i=0;i<8;i++)
              wrapscoringList[i].ques=capQuesAnsGlobalList[i].QuestionInterpretation__c;

            for(Integer i=0;i<8;i++){
              wrapscoringList[i].ans1=capQuesAnsGlobalList[i].AnswerInterpretation1__c;
              wrapscoringList[i].ans2=capQuesAnsGlobalList[i].AnswerInterpretation2__c;
              wrapscoringList[i].ans3=capQuesAnsGlobalList[i].AnswerInterpretation3__c;
              wrapscoringList[i].ans4=capQuesAnsGlobalList[i].AnswerInterpretation4__c;
           }
       }
    
    // get active BU's From OPP_Product__c  
    List<OPP_Product__c> OppProdList=[select name,IsActive__c from OPP_Product__c where HierarchyType__c='PM0-BUSINESS-UNIT' and IsActive__c=TRUE ORDER BY NAME ASC]; 
    Set<String> buSet= new Set<String>();    
    for(OPP_Product__c prd:OppProdList)
        buSet.add(prd.name);
    System.debug('>>>>>>ampID '+ampID);
    if(ampID==null){
        //for creation
         for(OPP_Product__c bu:OppProdList){
             AccountMasterProfilePAM__c acc= new AccountMasterProfilePAM__c();
                 acc.GMRBusinessUnit__c=bu.Name;
                 wrapPAMdetailsList.add(new wrapPAMDetails(acc,bu.Name));
         }
     }
     else{
          System.debug('>>>>>>entered else loop');
          //for existing record
           List<AccountMasterProfilePAM__c > pamList=[Select PAM__c,GMRBusinessUnit__c,directsales__c,indirectsales__c from AccountMasterProfilePAM__c where AccountMasterProfile__c=:ampID and GMRBusinessUnit__c= :buSet and Active__c=TRUE Order by GMRBusinessUnit__c ASC]; 
           Map<String,AccountMasterProfilePAM__c > pamMap=new Map<String,AccountMasterProfilePAM__c >();
           System.debug('>>>>>>pamList '+pamList);
           System.debug('>>>>>>buSet '+buSet );
           for(AccountMasterProfilePAM__c apam:pamList){
                     if(!pamMap.Containskey(apam.GMRBusinessUnit__c))
                         pamMap.put(apam.GMRBusinessUnit__c,apam);
             }
             //Existing record for the active BU's
              for(String bu:buSet){
                  if(pamMap.Containskey(bu)){
                     wrapPAMdetailsList.add(new wrapPAMDetails(pamMap.get(bu),bu));
                  }
                   //New record - if new BU is added
                  else{ 
                     AccountMasterProfilePAM__c acc= new AccountMasterProfilePAM__c(GMRBusinessUnit__c=bu);
                     wrapPAMdetailsList.add(new wrapPAMDetails(acc,bu));
                 }
             }
         }
         System.debug('>>>>wrapPAMdetailsList'+wrapPAMdetailsList);
     
    }

     public class wrapPAMDetails{
        public AccountMasterProfilePAM__c rec {get; set;}
        public String BUName{get; set;}
        public wrapPAMDetails(AccountMasterProfilePAM__c re,String name) {
            rec = re;
            BUName=name;
        }
     }
     public class wrapscoring{
        public String qseq{get; set;}
        public String ques{get; set;}
        public String rating{get; set;}
        public String ans1{get; set;}
        public String ans2{get; set;}
        public String ans3{get; set;}
        public String ans4{get; set;}
        public Decimal wgt{get; set;}
        public wrapscoring(){
        }
     }
    public PageReference saveAMP(){
        try{
            SumOfBUSplit=0;
            sumofBUDirectSales=0;
            sumofBUInDirectsales=0;
            for(wrapPAMDetails wra:wrapPAMdetailsList){
                if(wra.rec.PAM__c != null)
                    SumOfBUSplit+=wra.rec.PAM__c;
                if(wra.rec.DirectSales__c != null)
                    sumofBUDirectSales+=wra.rec.DirectSales__c;
                if(wra.rec.IndirectSales__c != null)
                   sumofBUInDirectsales+=wra.rec.IndirectSales__c;
           }
           amp.Q1Rating__c =wrapscoringList[0].rating;
           amp.Q2Rating__c =wrapscoringList[1].rating;
           amp.Q3Rating__c =wrapscoringList[2].rating;
           amp.Q4Rating__c =wrapscoringList[3].rating;
           amp.Q5Rating__c =wrapscoringList[4].rating;
           amp.Q6Rating__c =wrapscoringList[5].rating;
           amp.Q7Rating__c =wrapscoringList[6].rating;
           amp.Q8Rating__c =wrapscoringList[7].rating;
             
            if(warningFlag != TRUE )
            {
                if(amp.TotalPAM__c < SumOfBUSplit) 
                {
                     warningFlag=TRUE;
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.CLQ316SLS034));
                 }
                 if(amp.DirectSales__c < sumofBUDirectSales)
                {
                     warningFlag=TRUE;
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.CLQ316SLS035));
                 }
                 if(amp.IndirectSales__c < sumofBUInDirectsales)
                {
                     warningFlag=TRUE;
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,Label.CLQ316SLS036));
                 }
                 if(warningFlag==TRUE)
                     return null;
            }
            System.debug('>>>>>amp.DirectSales__c>>'+amp.DirectSales__c+'>>>>amp.IndirectSales__c>>>'+amp.IndirectSales__c+'>>>>amp.TotalPAM__c'+amp.TotalPAM__c);
         
            upsert amp;//Upsert Account Master Profile

           List<AccountMasterProfilePAM__c> upsertaccPAMList=new List<AccountMasterProfilePAM__c >();
           for(wrapPAMDetails wp:wrapPAMdetailsList){
               AccountMasterProfilePAM__c apam=wp.rec;
               apam.AccountMasterProfile__c=amp.id;
               apam.CurrencyIsoCode=amp.CurrencyIsoCode;  // assigning currency of amp to ampPAM
               upsertaccPAMList.add(apam);
           }

        upsert upsertaccPAMList; // Upsert AccountMasterProfilePAM list
       //Redirect to View VFP
       PageReference pr = new PageReference(Site.getPathPrefix()+'/apex/VFP_ViewAccountMasterProfile?ampID='+amp.Id+'&accID='+accID);
       pr.setredirect(true); 
       return pr;
      }
      Catch(Exception ex){
           ApexPages.addMessages(ex);        
      } 
       return null;
   }
   //Redirect to Edit Page of existing data
    public PageReference editAMP(){
        if(accID==null)
            accID=System.currentPageReference().getParameters().get( 'accID' );
        if(accID!=null){
           PageReference pr = new PageReference(Site.getPathPrefix()+'/apex/VFP_CreateEditAccountMasterProfile?ampID='+amp.Id+'&accID='+accID);
           pr.setredirect(true); 
           return pr;
         }  
        else{
          System.debug('>>>>> accID'+accID);
          return null; 
      }   
    }
   
   public PageReference Cancel(){
       //Redirect to the Account - Create scenario
       if(ampId == null){
           PageReference pr = new PageReference(Site.getPathPrefix()+'/'+accID);
           pr.setredirect(true);
           return pr; 
       }
       else{
           //Edit scenario
           PageReference pr = new PageReference(Site.getPathPrefix()+'/apex/VFP_ViewAccountMasterProfile?ampID='+amp.Id+'&accID='+accID);
           pr.setredirect(true);
           return pr; 
     } 
   }
   
   public PageReference backToAccount(){
       PageReference pr = new PageReference(Site.getPathPrefix()+'/'+accID);
       pr.setredirect(true); 
       return pr;
   }
}