/*   Author: Nicolas PALITZYNE (Accenture)  
     Date Of Creation: 17/11/2011   
     Description : Class containing a static method which will update the answer to customer for the related case 
*/

public class AP33_UpdateAnswerToCustomer
{
    public static Boolean isCaseAlreadyUpdated=false;

    Public static void updateRelatedCases(List<CSE_ExternalReferences__c> aListOfExternalReferences)
    {
        if(aListOfExternalReferences != null && aListOfExternalReferences.size()>0)
        {
            // Map Case IDs with the record
            Map<String,Case> caseMap = new Map<String,Case>();
            // List of Cases to be updated
            List<Case> casesTobeUpdated = new List<Case>();
            // List of External Reference of type "P&A"
            List<CSE_ExternalReferences__c> PAexternalReference = new List<CSE_ExternalReferences__c>();
                       
            // Generate the set of Case IDs
            Set<String> caseIDSet = new Set<String>();        
            For(CSE_ExternalReferences__c anExternalRef:aListOfExternalReferences)
            {
                if(anExternalRef.Type__c == Label.CL00686)
                {
                    caseIDSet.add(anExternalRef.Case__c);
                    PAexternalReference.add(anExternalRef);
                }
            }
            
            // Put the ID and the case into the map
            For(Case aCase:[SELECT ID, AnswerToCustomer__c FROM Case WHERE ID IN :caseIDSet])
            {
                caseMap.put(aCase.ID,aCase); 
            } 
            
            // Populate the list of cases to be updated
            For(CSE_ExternalReferences__c anExternalRef:PAexternalReference)
            { 
                Case theParentCase = caseMap.get(anExternalRef.case__c);
            
                if(addStringToAnswerToCustomer(theParentCase, anExternalRef.Description__c))
                {
                    casesTobeUpdated.add(theParentCase);
                }
            }
            
            if(casesTobeUpdated != null && casesTobeUpdated.size()>0)
            {
                // Update the related case and log DML errors - Allow partial success
                List<Database.SaveResult> saveResultList = Database.update(casesTobeUpdated,false);
                
                For(Database.SaveResult aSaveresult:saveResultList)
                {
                    if(!aSaveresult.isSuccess())
                    {
                        System.debug('#### Insert failed: ' + aSaveresult.getErrors());
                    }
                }
            }
        }
    }
    
    Public static Boolean addStringToAnswerToCustomer(Case aCase, String aString)
    {
        Boolean result = false;

        if(!isCaseAlreadyUpdated)
        {
            if(aCase.AnswerToCustomer__c == null || (aCase.AnswerToCustomer__c != null && aString !=null && aCase.AnswerToCustomer__c.contains(aString) == false))
            {
                if(aCase != null && aString  != null)
                {
                    String caseAnswerToCustomer = aCase.AnswerToCustomer__c;
            
                    String textToAdd = '';
            
                    if(caseAnswerToCustomer  == null)
                    {
                        caseAnswerToCustomer  = '';
                    }
                    else
                    {
                         textToAdd = '<br><br>'; 
                    }   
                     
                     // Switch from plain Text to HTML by inserting correct tags 
                    if (aString.indexOf('\r\n')>-1) 
                    {
                        textToAdd += aString.replace('\r\n', '<br>');
                    }
                    else if(aString.indexOf('\n')>-1) 
                    {
                        textToAdd += aString.replace('\n', '<br>');            
                    }
                    else
                    {
                        textToAdd += aString;
                    }
        
                    caseAnswerToCustomer += textToAdd; 
                   
                    // the case will be updated if the Answer to Customer does not exceed the maximum length
                    if(caseAnswerToCustomer.length() <= Integer.ValueOf(Label.CL00619))
                    {
                            aCase.AnswerToCustomer__c = caseAnswerToCustomer  ;
                            result = true;
                            isCaseAlreadyUpdated = true;
                    }        
                }
            }
        }  
        return result;
    }
    //added by suhail for Q3release 16 start
     Public static void updateAnswerToCustomerFromWorkOrder(set<id> caseidset){
         list<Case> caselist = new list<Case>();
         list<Case> caselist2 = new list<Case>();
         
         caselist = [SELECT id,AnswerToCustomer__c from Case WHERE id in:caseidset];
         for(Case cs:caselist)  {
             if(cs.AnswerToCustomer__c != null){
                 string temp = cs.AnswerToCustomer__c;
                 cs.AnswerToCustomer__c = temp+'<br/>'+ string.valueof(System.now())+':'+' A work order has been created'+'|'; 
                 cs.status = 'Answer Provided to Customer';
                 caselist2.add(cs);
             }else{
             cs.AnswerToCustomer__c = string.valueof(System.now())+':'+' A work order has been created' ; 
             cs.status = 'Answer Provided to Customer';
             caselist2.add(cs);
             }
         }   
         database.update(caselist2);
         
         
     }
    
    //added by suhail for Q3release 16 end
}