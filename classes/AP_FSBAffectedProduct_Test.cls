@isTest
private class AP_FSBAffectedProduct_Test {
    
    static testMethod void Method1() {
        
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        runAsUser.BypassWF__c = true;
        runAsUser.BypassVR__c = true;
        insert runAsUser;
        
        System.runAs(runAsUser) {
            
            Product2 prod = Utils_TestMethods.createCommercialReference('commercialReference','strMaterialDescription','businessUnit','productLine','productFamily','family',false,false,false);
            insert prod;
            
            BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                    accOrg1.Name='Test';
                    accOrg1.Entity__c='Test Entity-2'; 
                    insert  accOrg1;
            BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                    accOrg2.Name='Test';
                    accOrg2.Entity__c='Test Entity-2'; 
                    accOrg2.SubEntity__c='Test Sub-Entity 2';
                    insert  accOrg2;
            BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                    accOrg.Name='Test';
                    accOrg.Entity__c='Test Entity-2';  
                    accOrg.SubEntity__c='Test Sub-Entity 2';
                    accOrg.Location__c='Test Location 2';
                    accOrg.Location_Type__c='Design Center';
                    insert  accOrg;
                    
            OPP_Product__c OPPprod = Utils_TestMethods.createProduct('businessUnit','productLine','','');
            insert OPPprod;
        
            ProductLineQualityContactMapping__c plis = new ProductLineQualityContactMapping__c();
            plis.Product__c = OPPprod.Id;
            plis.QualityContact__c = runAsUser.Id;
            plis.AccountableOrganization__c = accOrg.Id;
            insert plis;
                        
            FieldServiceBulletin__c FSB = new FieldServiceBulletin__c();
            FSB.Name = 'Test'; 
            FSB.Description__c = 'Test Description';
            FSB.ProductLine__c = OPPprod.Id;
            insert FSB;
            
            FSBAffectedProduct__c fsbAP = new FSBAffectedProduct__c();
            fsbAP.FieldServiceBulletin__c = FSB.Id;
            fsbAP.Product__c = prod.Id;
            insert fsbAP;
            
            System.debug('!!!! fsbAP (from test class) : ' + fsbAP);
            
            Test.StartTest();
            Product2 prod1 = Utils_TestMethods.createCommercialReference('CR','MD','BU','PL','PF','F',true,true,true);
            insert prod1;
            
            FSBAffectedProduct__c fsbAP1 = new FSBAffectedProduct__c();
            fsbAP1.FieldServiceBulletin__c = FSB.Id;
            fsbAP1.Product__c = prod1.Id;
            insert fsbAP1;  
            
            Set<String> setFSB = new Set<String>();
            setFSB.add(FSB.Id);
            List<FSBAffectedProduct__c> triggerNew = new List<FSBAffectedProduct__c>();
            triggerNew.add(fsbAP1);
            
            AP_FSBAffectedProduct.SameProductLineCheck(setFSB,triggerNew);
              
            Test.StopTest();
            
            
        }    
    }
}