/********************************************************************
* Company: Fielo
* Created Date: 29/08/2016
* Description:
********************************************************************/
@isTest
private class FieloPRM_SiteURLRewriteTest {

    @isTest(seeAllData=false) public static void testMenu() {
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Menu__c m = new FieloEE__Menu__c(Name = 'test', FieloEE__ExternalName__c = 'test');
        insert m;
        FieloPRM_SiteURLRewrite controller = new FieloPRM_SiteURLRewrite();

        PageReference inUrl = new PageReference('/Menu/test');
        inUrl.getParameters().put('testPar','test');
        controller.mapRequestUrl(inUrl);

        PageReference inUrl2 = new PageReference('/');
        inUrl2.getParameters().put('testPar','test');
        controller.mapRequestUrl(inUrl2);

        PageReference outUrl = new PageReference('/Page');
        outUrl.getParameters().put('idMenu', m.Id);
        outUrl.getParameters().put('testPar', 'test');
        controller.generateUrlFor(new List<PageReference>{outUrl});

        PageReference outUrl2 = new PageReference('/');
        controller.mapRequestUrl(outUrl2);


    }
}