/********************************************************************
* Company: Fielo
* Developer: Elena J. Schwarzböck
* Created Date: 09/03/2015
* Description: 
********************************************************************/

public without sharing class FieloPRM_AP_BadgeAccountTriggers{

    public static final string PARTNER_LOCATOR  = 'Partner Locator';

    public static boolean isRunning = false;

    public static void recalculatesPartenerLocatorAccount(List<FieloPRM_BadgeAccount__c> triggerNew, map<Id, FieloPRM_BadgeAccount__c> triggerOldMap){
        List<FieloPRM_BadgeAccount__c> listBadgeAccounts = triggerNew;
        
        Set<Id> setAccounts = new Set<Id>();
        for(FieloPRM_BadgeAccount__c baNewOld: listBadgeAccounts){
            if(trigger.isInsert || trigger.isDelete){
                if(baNewOld.F_PRM_Account__c != null){
                    setAccounts.add(baNewOld.F_PRM_Account__c);
                }
            }else if(trigger.isUpdate){
                if(baNewOld.F_PRM_Account__c != null && baNewOld.F_PRM_Account__c != ((FieloPRM_BadgeAccount__c)triggerOldMap.get(baNewOld.Id)).F_PRM_Account__c){
                    setAccounts.add(baNewOld.F_PRM_Account__c);
                }
            }
        }
        
        if(!setAccounts.isEmpty()){
            List<FieloPRM_BadgeAccount__c> listAllBadgeAccounts = [SELECT Id, F_PRM_Badge__c, F_PRM_Account__c FROM FieloPRM_BadgeAccount__c WHERE F_PRM_Account__c IN: setAccounts];
            if(!listAllBadgeAccounts.isEmpty()){
                Map<Id,Set<Id>> mapAccountBadges = new Map<Id,Set<Id>>();
                Set<Id> setPartnerLocatorBadges = new Set<Id>();
                Set<Id> setBadges = new Set<Id>();
                for(FieloPRM_BadgeAccount__c badgeAccount: listAllBadgeAccounts){
                    setBadges.add(badgeAccount.F_PRM_Badge__c);
                    if(!mapAccountBadges.containsKey(badgeAccount.F_PRM_Account__c)){
                        mapAccountBadges.put(badgeAccount.F_PRM_Account__c,new Set<Id>{badgeAccount.F_PRM_Badge__c});
                    }else{
                        mapAccountBadges.get(badgeAccount.F_PRM_Account__c).add(badgeAccount.F_PRM_Badge__c);
                    }
                }
                
                if(!mapAccountBadges.isEmpty()){
                    for(FieloPRM_BadgeFeature__c badgeFeature: [SELECT Id, F_PRM_Badge__c FROM FieloPRM_BadgeFeature__c WHERE F_PRM_Feature__r.Name =: PARTNER_LOCATOR AND F_PRM_Badge__c IN: setBadges]){
                        setPartnerLocatorBadges.add(badgeFeature.F_PRM_Badge__c);
                    }
                }
                
                List<Account> listAccountsToUpdate = new List<Account>();
                Boolean isPartenerLocator;
                for(Account acc: [SELECT Id, PLPartnerLocatorFeatureGranted__c FROM Account WHERE Id IN: setAccounts]){
                    isPartenerLocator = false;
                    if(mapAccountBadges.containsKey(acc.Id)){
                        for(Id badgeId: mapAccountBadges.get(acc.Id)){
                            if(setPartnerLocatorBadges.contains(badgeId)){
                                isPartenerLocator = true;
                                break;
                            }
                        }
                    }
                    if(acc.PLPartnerLocatorFeatureGranted__c != isPartenerLocator){
                        acc.PLPartnerLocatorFeatureGranted__c = isPartenerLocator;
                        listAccountsToUpdate.add(acc);
                    }
                }
                if(!listAccountsToUpdate.isEmpty()){
                    update listAccountsToUpdate;
                }
            }
        }
    }

    /**
    * @author Julio Enrique
    * @date 24/08/2015
    * @description active the badges from the top level based on the position of the challengeReward. Inactive the badges that are not on the top position
    */
    public static void checkActiveBadges(){
        Set<Id> challengesIds = new Set<Id>();
        Set<Id> accountsIds = new Set<Id>();

        for(FieloPRM_BadgeAccount__c record : [SELECT F_PRM_Account__c, F_PRM_Badge__c, F_PRM_Badge__r.F_PRM_Challenge__c, F_PRM_IsCurrentLevel__c FROM FieloPRM_BadgeAccount__c WHERE Id IN: trigger.New]){
            if(record.F_PRM_Badge__r.F_PRM_Challenge__c != null){
                challengesIds.add(record.F_PRM_Badge__r.F_PRM_Challenge__c);
            }
            accountsIds.add(record.F_PRM_Account__c);
        }
        
        List<Account> accounts = [SELECT Id, (SELECT Id, F_PRM_Account__c, F_PRM_Badge__c, F_PRM_Badge__r.F_PRM_Challenge__c, F_PRM_IsCurrentLevel__c FROM Badge_Accounts__r) FROM Account WHERE PRMUIMSID__c != null AND PRMAccount__c = true AND Id IN:accountsIds];
        
        //process active/inactive
        List<FieloPRM_BadgeAccount__c> badgeAccountsUpdate = new List<FieloPRM_BadgeAccount__c>();
        for(FieloCH__Challenge__c ch: [SELECT Id, (SELECT FieloCH__Challenge__c, FieloCH__Badge__c, FieloCH__Position__c FROM FieloCH__ChallengeRewards__r) FROM FieloCH__Challenge__c WHERE Id IN: challengesIds]){
            Map<Id, Decimal> challengesRewardBadgeMap = new Map<Id, Decimal>();
            for(FieloCH__ChallengeReward__c chr : ch.FieloCH__ChallengeRewards__r){
                challengesRewardBadgeMap.put(chr.FieloCH__Badge__c, chr.FieloCH__Position__c);
            }
            
            Decimal currentPositionToCheck = 1000;
            for(Account acc : accounts){
                //get the position of the badge
                for(FieloPRM_BadgeAccount__c badgeAcc : acc.Badge_Accounts__r){
                    if(badgeAcc.F_PRM_Badge__r.F_PRM_Challenge__c == ch.Id){
                        Decimal badgePosition = challengesRewardBadgeMap.get(badgeAcc.F_PRM_Badge__c);
                        if(badgePosition < currentPositionToCheck){
                            currentPositionToCheck = badgePosition;
                        }
                    }
                }
                //based on currentpositiontocheck, get the badge
                for(FieloPRM_BadgeAccount__c badgeAcc : acc.Badge_Accounts__r){
                    if(badgeAcc.F_PRM_Badge__r.F_PRM_Challenge__c == ch.Id){
                        if(!badgeAcc.F_PRM_IsCurrentLevel__c && challengesRewardBadgeMap.get(badgeAcc.F_PRM_Badge__c) == currentPositionToCheck){
                            badgeAcc.F_PRM_IsCurrentLevel__c = true;
                            badgeAccountsUpdate.add(badgeAcc);
                        }else{
                            if(badgeAcc.F_PRM_IsCurrentLevel__c && challengesRewardBadgeMap.get(badgeAcc.F_PRM_Badge__c) != currentPositionToCheck){
                                badgeAcc.F_PRM_IsCurrentLevel__c = false;
                                badgeAccountsUpdate.add(badgeAcc);
                            }
                        }
                    }
                }
            }
        }
        if(!badgeAccountsUpdate.isEmpty()){
            update badgeAccountsUpdate;
        }
    }
    
    /**
    * @author Pablo Cassinerio
    * @date 11/11/2016
    * @description set badge account name
    */
    public static void setBadgeAccountName(Set<Id> newTriggerIds){
        Set<Id> ids = new Set<Id>();
        if(Trigger.isInsert){
            ids = newTriggerIds;
        }else{
            for(Id ba: newTriggerIds){
                if(Trigger.oldMap.get(ba).get('F_PRM_Badge__c') != Trigger.newMap.get(ba).get('F_PRM_Badge__c')){
                    ids.add(ba);
                }
            }
        }
        
        if(!ids.isEmpty()){
            List<FieloPRM_BadgeAccount__c > badgeAccs = [SELECT Id, F_PRM_Badge__r.F_PRM_Challenge__r.Name, F_PRM_Badge__r.F_PRM_Challenge__r.F_PRM_ProgramCode__c, F_PRM_Badge__r.Name FROM FieloPRM_BadgeAccount__c WHERE Id IN :ids];
            for(FieloPRM_BadgeAccount__c ba: badgeAccs){
                String str = ba.F_PRM_Badge__r.F_PRM_Challenge__r.Name + ' - ' + ba.F_PRM_Badge__r.F_PRM_Challenge__r.F_PRM_ProgramCode__c + ' - ' + ba.F_PRM_Badge__r.Name;
                if(str.length() + 6 > 80){
                    ba.Name = str.substring(0, 80);
                }else{
                    ba.Name = str;
                }
            }
            update badgeAccs;
        }
    }

    public static void setBadgeAccountUnique(List<FieloPRM_BadgeAccount__c> triggerNew){
        System.debug('*** Badge Account ->'+triggerNew);
        List<FieloPRM_BadgeAccount__c> toUpdate = new List<FieloPRM_BadgeAccount__c>();
        for(FieloPRM_BadgeAccount__c bac: triggerNew){
            System.debug('*** Duplicate Badge Account ->'+bac.F_PRM_Badge__c + '-' + bac.F_PRM_Account__c);
            toUpdate.add(new FieloPRM_BadgeAccount__c(Id = bac.Id, F_PRM_BadgeAccount__c = bac.F_PRM_Badge__c + '-' + bac.F_PRM_Account__c));
        }
        Database.update(toUpdate, false);
    }
}