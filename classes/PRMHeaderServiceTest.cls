@IsTest
public class PRMHeaderServiceTest{

    public static testMethod void testJSONResponse(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
         
        req.requestURI = 'https://cs22.salesforce.com/services/apexrest/prm';
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = res;
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;

        System.runAs(us){    
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;
            
            FieloEE__Program__c prog = [SELECT Id,FieloEE__SiteURL__c,FieloEE__CustomPage__c FROM FieloEE__Program__c LIMIT 1];
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'RahlIsMyLastName';
            member.FieloEE__FirstName__c = 'Marco';
            member.FieloEE__Street__c = 'test';
            member.FieloEE__Program__c = prog.Id;
            insert member;
            
            Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
            insert france;

            Account acc1 = new Account(name='name',Street__c='Street',ZipCode__c='75020',PRMZipCode__c='75020',Country__c=france.id,PRMCountry__c=france.id,PRMUIMSID__c='ABCD');
            insert acc1;

            Contact cont = Utils_TestMethods.createContact(acc1.Id,'RahlIsMyLastName');
            cont.FieloEE__Member__c = member.Id;
            insert cont;
            
            User usr = Utils_TestMethods.createStandardUser('TestUser');
            usr.contactId = cont.Id;
            usr.ProfileId = System.label.CLAPR15PRM026;
            INSERT usr;
            
            member.FieloEE__User__c = usr.Id;
            update member;
            
            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c();
            thisMenu.FieloEE__Title__c = 'Meny';
            thisMenu.Name = 'MyCrazyMenu';
            insert thisMenu;
            
            FieloPRM_PartnerLocatorConfiguration__c config = new FieloPRM_PartnerLocatorConfiguration__c(F_PRM_ShowBusinessType2__c=true, F_PRM_LocatorParameter__c='distributor', F_PRM_BusinessTypeIncludeValues__c='WD', F_PRM_BusinessTypeDefaultValue__c='WD', F_PRM_ShowBusinessType__c=false, F_PRM_Subtitle2__c='FieloPRM_CL_32_NetworkSubtitle', F_PRM_DistanceValues__c='10\r\n20',F_PRM_ShowTwoLocator__c=true, F_PRM_Subtitle__c='FieloPRM_CL_029_DistributorSubtitle', F_PRM_LocatorParameter2__c='networking', F_PRM_ShowAreaOfFocus__c=false, F_PRM_AreaOfFocusIncludeValues__c='WD1\r\nWD4', F_PRM_Title2__c='FieloPRM_CL_31_NetworkTitle', F_PRM_BusinessTypeExcludeValues2__c='WD', F_PRM_BottomLabel2__c='FieloPRM_CL_33_NetworkBottom', F_PRM_Title__c='FieloPRM_CL_028_DistributorTitle',F_PRM_DistanceUnitValues__c='##DEF##mi\r\nkm', F_PRM_BottomLabel__c='FieloPRM_CL_30_DistributorBottom', F_PRM_ShowAreaOfFocus2__c=true, F_PRM_DistanceUnitValues2__c='##DEF##mi\r\nkm', F_PRM_DistanceValues2__c='10\r\n20',F_PRM_SearchFilterValues__c='MarketServes;AssociatedBrands',F_PRM_Country__c = france.id);
            insert config;
            FieloEE__Menu__c menu = new FieloEE__Menu__c(FieloEE__Title__c = 'Menu1',FieloEE__CustomURL__c='test');
            insert menu;
            FieloEE__Menu__c menu2 = new FieloEE__Menu__c(FieloEE__Title__c = 'Menu2',F_PRM_MultiLanguageCustomLabel__c='test2');
            insert menu2;
            FieloEE__Menu__c menu3 = new FieloEE__Menu__c(FieloEE__Title__c = 'Menu3',F_PRM_MultiLanguageCustomLabel__c='test2');
            insert menu3;
            
            FieloPRM_UTILS_MenuMethods.MenusContainer container =  new FieloPRM_UTILS_MenuMethods.MenusContainer();
            
            container.addMenu(menu);
            container.addMenu(menu2);
            container.addMenu(menu3);
            
            PRMHeaderService.fromFieloMenuContainerToMenuItemList(container,prog,false);
            PRMHeaderService.fromFieloMenuContainerToMenuItemList(container,prog,true);
            
            FieloEE__Section__c section = new FieloEE__Section__c();
            section.FieloEE__Menu__c = thisMenu.Id;
            insert section;
            
            FieloEE__Tag__c tag = new FieloEE__Tag__c();
            tag.name = 'test';
            insert tag;
            
            FieloEE__Category__c category = FieloPRM_UTILS_MockUpFactory.createCategory(thisMenu.Id);
            
            
            FieloEE__Component__c comp = new FieloEE__Component__c();
            comp.FieloEE__Section__c = section.Id;
            comp.FieloEE__Menu__c = thisMenu.Id;
            comp.FieloEE__Tag__c = tag.id;
            insert comp;
            
            FieloEE__News__c contentFeed = new FieloEE__News__c();
            contentFeed.FieloEE__IsActive__c = true;
            contentFeed.FieloEE__Component__c = comp.id;
            contentFeed.FieloEE__CategoryItem__c = category.id;
            insert contentFeed;
            
            FieloEE__Banner__c banner = new FieloEE__Banner__c();
            banner.FieloEE__isActive__c = false;
            banner.FieloEE__Component__c  = comp.id;
            banner.FieloEE__Category__c = category.id;
            banner.Name = 'test';
            insert banner;
            
            FieloEE__TagItem__c tagItemB = new FieloEE__TagItem__c();
            //tagItem.F_PRM_Category__c = category.id;
            tagItemB.FieloEE__Banner__c = banner.id;
            tagItemB.FieloEE__Tag__c = tag.id;
            insert tagItemB ;
            
            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c();
            //tagItem.F_PRM_Category__c = category.id;
            tagItem.FieloEE__News__c = contentFeed.id;
            tagItem.FieloEE__Tag__c = tag.id;
            insert tagItem;
            
            test.startTest();
            String results = PRMHeaderService.getMenuTree();
            System.assertEquals(true,results.contains('Unknown contact'));
            req.params.put('contactid',cont.id);
            req.params.put('plCfg',config.id+'_');
            results = PRMHeaderService.getMenuTree();
            System.assertEquals(true,results.contains('Marco'));
            //System.assertEquals(true,results.contains(prog.FieloEE__SiteURL__c));

            test.stopTest();
        }
    }

}