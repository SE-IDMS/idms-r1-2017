public  class Utils_DummyDataForTest  extends Utils_DataSource{
    
    public override List<SelectOption> getColumns(){
        List<SelectOption> Columns = new List<SelectOption>();
        Columns.add(new SelectOption('FieldCheckbox2__c','Corporate Headquaters'));
        Columns.add(new SelectOption('FieldCheckbox1__c','To Be Deleted'));
        Columns.add(new SelectOption('Field12__c','Street local'));
        Columns.add(new SelectOption('Field8__c','commercial Reference'));
        Columns.add(new SelectOption('Field9__c','Description'));
        Columns.add(new SelectOption('Field4__c','Family'));
        Columns.add(new SelectOption('Field3__c','Product Family'));
        Columns.add(new SelectOption('Field2__c','Product Line'));
        Columns.add(new SelectOption('Field1__c','Business Unit'));
        return Columns;
    }
    
    public override Utils_DataSource.Result Search(List<String> KeyWords, List<String> Filters){
        List<DataTemplate__c> Records = new List<DataTemplate__c>();
        for(integer i=0; i<2; i++){
            DataTemplate__c DT = new DataTemplate__c();              
            DT.Field1__c = 'Business Unit A';
            DT.Field2__c = ' ';
            DT.Field3__c = ' ';
            DT.Field4__c = ' ';
            DT.Field5__c = ' ' ;
            DT.Field6__c = '' ;
            Records.add(DT);
        }    
        
        for(integer i=1; i<4; i++){
            DataTemplate__c DT = new DataTemplate__c();              
            DT.Field1__c = 'Business Unit A';
            DT.Field2__c = 'Product Line ' + i;
            DT.Field3__c = ' ';
            DT.Field4__c = ' ';
            DT.Field5__c = ' '  ;
            DT.Field6__c =  '' ;
            Records.add(DT);
        } 
        
        for(integer i=4; i<15; i++){
            DataTemplate__c DT = new DataTemplate__c();
            DT.Field1__c = 'Business Unit A';
            DT.Field2__c = 'Product Line ' + i ;
            DT.Field3__c = 'Product Family ' + i;
            DT.Field4__c = 'Family ' + i;
            DT.Field5__c = ' ' + i;
            DT.Field6__c = ' ';
            DT.Field7__c = ' ' + i;
            DT.Field8__c = 'Commercial Reference XF-4 '+i;
            DT.Field9__c = 'Description of the product' + i;
            Records.add(DT);
        }
        
        Utils_DataSource.Result ReturnedResult = new Utils_DataSource.Result(); 
        ReturnedResult.recordList = Records;
        return ReturnedResult;  
    }
    public  static WS_GMRSearch.resultFilteredDataBean FilteredSearch(String strCommericalReference){
        WS_GMRSearch.resultFilteredDataBean result = new WS_GMRSearch.resultFilteredDataBean();
        List<WS_GMRSearch.gmrFilteredDataBean> lstFilteredDataBean = new List<WS_GMRSearch.gmrFilteredDataBean>();
        WS_GMRSearch.gmrFilteredDataBean objFilteredDataBean = new  WS_GMRSearch.gmrFilteredDataBean();

        WS_GMRSearch.labelValue objBU =new WS_GMRSearch.labelValue();
        objBU.label='BusinessUnit';
        objBU.value='00';

        WS_GMRSearch.labelValue objfamily =new WS_GMRSearch.labelValue();
        objfamily.label='FamilyName';
        objfamily.value='33';

        WS_GMRSearch.labelValue objProductGroup =new WS_GMRSearch.labelValue();
        objProductGroup.label='ProductGroup';
        objProductGroup.value='66';

        WS_GMRSearch.labelValue objProductLine =new WS_GMRSearch.labelValue();
        objProductLine.label='ProductLine';
        objProductLine.value='11';

        WS_GMRSearch.labelValue objProductSuccession =new WS_GMRSearch.labelValue();
        objProductSuccession.label='ProductSuccession';
        objProductSuccession.value='55';

        WS_GMRSearch.labelValue objProductFamily =new WS_GMRSearch.labelValue();
        objProductFamily.label='ProductFamily';
        objProductFamily.value='22';

        WS_GMRSearch.labelValue objSubFamily =new WS_GMRSearch.labelValue();
        objSubFamily.label='SubFamily';
        objSubFamily.value='44';

        objFilteredDataBean.businessLine = objBU;
        objFilteredDataBean.commercialReference = strCommericalReference;
        objFilteredDataBean.description = 'Dummy Commerical Reference';
        objFilteredDataBean.family = objfamily;
        objFilteredDataBean.productGroup = objProductGroup;
        objFilteredDataBean.productLine = objProductLine;
        objFilteredDataBean.productSuccession = objProductSuccession;
        objFilteredDataBean.strategicProductFamily = objProductFamily;
        objFilteredDataBean.subFamily = objSubFamily;
        lstFilteredDataBean.add(objFilteredDataBean);
        result.gmrFilteredDataBeanList = lstFilteredDataBean;
        return result;          
    }
    public class WebServiceException extends Exception {}
    
    public  static WS_Inquira.faqCKMSearchResponse searchFaq(boolean blnMessageRequired, boolean blnExceptionRequired){
        WS_Inquira.faqCKMSearchResponse objSearchFaqResponse = new WS_Inquira.faqCKMSearchResponse();
        List<WS_Inquira.faqCKMBean> lstfaqCKMBean = new List<WS_Inquira.faqCKMBean>();
        
        if(blnMessageRequired){
            objSearchFaqResponse.message = 'This is a Test Message from Utils_DummyDataForTest for Inquira' ;       
        }
        if(blnExceptionRequired){
            throw new WebServiceException('Exception from from Utils_DummyDataForTest for Inquira');
        }
        for(integer intCount=1; intCount<4; intCount++){
            WS_Inquira.faqCKMBean objfaqCKMBean = new WS_Inquira.faqCKMBean();
            objfaqCKMBean.date_x = Datetime.now();
            objfaqCKMBean.excerpt = intCount.format() + ' . This document contains the same information as the public version';
            objfaqCKMBean.id = 'FA15762' + intCount.format();
            objfaqCKMBean.public_URL = 'http://www.google.co.in';
            if(System.Math.Mod(intCount, 2)==1){
                objfaqCKMBean.securityLevel = 'Internal'  ;
                objfaqCKMBean.url =  '' ;
            }
            else{
                objfaqCKMBean.securityLevel = 'Public'  ;
                objfaqCKMBean.url =  'http://infomanager.schneider-electric.com/bfo/index?page=login_BFO&amp;country=ITB&amp;lang=en&amp;locale=en_US&amp;id=FA172652&amp;viewlocale=en_US' ;
            }
            
            objfaqCKMBean.title =  intCount.format() + ' . Test Title from Utils_DummyDataForTest for Inquira '  ;
            lstfaqCKMBean.add(objfaqCKMBean);
        } 
        
        objSearchFaqResponse.results = lstfaqCKMBean;
        return objSearchFaqResponse;
    }
    
    public static WS_Inquira.faqCKMCreateResponse createDraftFaq(boolean blnMessageRequired,boolean blnExceptionRequired){
        string faqid;
        WS_Inquira.faqCKMCreateResponse objCreateFAQResponse =new WS_Inquira.faqCKMCreateResponse();
        if(blnMessageRequired){
            objCreateFAQResponse.message = 'This is a Test Message from Utils_DummyDataForTest for Inquira' ;       
        }
        if(blnExceptionRequired){
            throw new WebServiceException('Exception from from Utils_DummyDataForTest for Inquira');
        }        
        double dummyid=system.math.random(); 
        decimal dummytestid= dummyid/0.01;
        integer dummyfaqid=dummytestid.intvalue();              
        faqid='FA1576'+ dummyfaqid.format();
        system.debug('-->>'+faqid);        
        objCreateFAQResponse.FAQIdCreated=faqid;
        return objCreateFAQResponse;
        }
        
}