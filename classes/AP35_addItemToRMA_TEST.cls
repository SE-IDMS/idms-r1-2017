/*
    Author          : Nicolas PALITZNE (Accenture)
    Date Created    : 07/12/2011
    Description     : AP35_AddItemToRMA Test Class
*/

@IsTest
private class AP35_addItemToRMA_TEST
{
      
    static testMethod void Method1() {
        
       Account Acc = Utils_TestMethods.createAccount();
       insert Acc;
       Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'TEST');
       insert Ctct;
       
       Country__c country1=new Country__c(CountryCode__c='US', Name='United States');  
       insert country1;
              
       Case Cse = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
       Cse.CommercialReference__c = 'ATV21DEMO';
       Cse.Quantity__c = 100;
       Cse.CCCountry__c = country1.Id;
       insert Cse;
       
       BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = country1.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
       Insert RCorg; 
       
       BusinessRiskEscalationEntity__c RCorg1 = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST1',
                                            Entity__c='RC Entity-2',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = country1.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
       Insert RCorg1 ; 
        
       RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = country1.Id,
                                 Capability__c = 'Technical Expert Assessment - In-House',
                                 CommercialReference__c='xbtg5230',
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c='02BF6D'
                                 );
       Insert RPDT;

       RMAShippingToAdmin__c RPDT1 = new RMAShippingToAdmin__c (
                                 Country__c = country1.Id,
                                 Capability__c = 'Technical Expert Assessment - In-House',
                                 CommercialReference__c='xbtg5230',
                                 ReturnCenter__c = RCorg1.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c='02BF6D'
                                 );
       Insert RPDT1;  
       
       List<RMA__c> lstRMA = new List<RMA__c>();
       RMA__c newRMA1 = new RMA__c(Case__c=Cse.ID, AccountStreet__c='aa', ReturnType__c='Commercial');
       lstRMA.add(newRMA1);
       RMA__c newRMA2 = new RMA__c(Case__c=Cse.ID, AccountStreet__c='aa', ReturnType__c='Commercial');
       lstRMA.add(newRMA2);
       insert lstRMA;
       
       AP35_addItemToRMA.RMAsMap.put(newRMA1.Id,newRMA1);
       AP35_addItemToRMA.RMAsMap.put(newRMA2.Id,newRMA2);
       
       AP35_addItemToRMA.RSTMap.put('US_Commercial_ProductFamily1',RPDT);
       AP35_addItemToRMA.RSTMap.put('US__ProductFamily1',RPDT);
       AP35_addItemToRMA.RSTMap.put('US_Commercial_',RPDT);
       AP35_addItemToRMA.RSTMap.put('US__',RPDT);
       System.debug('test RSTMap : '+ AP35_addItemToRMA.RSTMap);
       System.debug('test RMAsMap: '+ AP35_addItemToRMA.RMAsMap); 
       
       AP35_addItemToRMA.insertFirstReturnItem(lstRMA);   
       
       List<RMA_Product__c> lstRI = new List<RMA_Product__c>();
       RMA_Product__c newReturnItem1 = new RMA_Product__c(RMA__c=newRMA1.Id, ProductFamily__c='ProductFamily1', Quantity__c=1, ShippingCountry__c=country1.Id);
       lstRI.add(newReturnItem1);
       RMA_Product__c newReturnItem2 = new RMA_Product__c(RMA__c=newRMA2.Id, ProductFamily__c='ProductFamily2', Quantity__c=1, ShippingCountry__c=country1.Id);
       lstRI.add(newReturnItem2);
       insert lstRI;
      
       AP35_addItemToRMA.getRMAsMap(lstRI); 
       AP35_addItemToRMA.getRSTMap(lstRI);     
       
       
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'Street');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'AdditionalAddress');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'City');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'ZipCode');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'StateProvince');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'County');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'Country');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'POBox');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'POBoxZip');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'StreetLocalLang');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'LocalAdditionalAddress');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'LocalCity');
       AP35_addItemToRMA.getShippingAddress(newReturnItem1,'LocalCounty');
       
       AP35_addItemToRMA.updateShippingAddress(lstRI); 
       
       newRMA1.ShippingCountry__c=null;
       update newRMA1;
       newRMA2.ShippingCountry__c=null;
       update newRMA2;
       AP35_addItemToRMA.updateShippingAddress(lstRI);  
       
       AP35_addItemToRMA.getReturnItemsToDelete(lstRI); 
       AP35_addItemToRMA.updateReturnAddress(lstRI); 
       
       String str = '';
       AP35_addItemToRMA.CheckIsNull(str);
       str = 'str';
       AP35_addItemToRMA.CheckIsNull(str);
    }
    
}