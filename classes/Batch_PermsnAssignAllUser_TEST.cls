@isTest(SeeAllData=true)
private class Batch_PermsnAssignAllUser_TEST {

    static testMethod void testpermissionsetid123() 
    { Test.startTest(); 
    List<User> users1 = new List<User>();
        User testUser11 = Utils_TestMethods.createStandardUser('tst11');
        users1.add(testUser11);
         insert users1;
         list<id> psid=new list<id>();
        //psid=[SELECT Id FROM PermissionSet where id='0PSA0000000KyqbOAC'].id;
        list<PermissionSet> pslist1= new list<PermissionSet>();
       PermissionSet ps =new PermissionSet(name='testpermission',label='myps');
        pslist1.add(ps);
        psid.add(ps.id);
        insert pslist1;
        //list <PermissionSetAssignment> permissionsetlist1= new list <PermissionSetAssignment>();
         PermissionSetAssignment testps = new PermissionSetAssignment(AssigneeId=testuser11.id,PermissionSetid =ps.id);
        //permissionsetlist1.add(testps);
        //testps.AssigneeId=testuser11.id;
        //testps.PermissionSetid =ps.id;
     

        insert testps;
        SC_PermissionsetAssignmentToUser sh1 = new SC_PermissionsetAssignmentToUser();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Territory Check', sch, sh1);
           //SC_PermissionsetAssignmentToUser     sct=new SC_PermissionsetAssignmentToUser();
         
        
    Test.stopTest();
   
        
}


    static testMethod void testpermissionsetid() 
    { 
    Test.startTest(); 
    List<User> users1 = new List<User>();
        User testUser11 = Utils_TestMethods.createStandardUser('tst11');
        users1.add(testUser11);
         insert users1;
         list<id> psid=new list<id>();
        //psid=[SELECT Id FROM PermissionSet where id='0PSA0000000KyqbOAC'].id;
        list<PermissionSet> pslist1= new list<PermissionSet>();
       PermissionSet ps =new PermissionSet(name='testpermission',label='myps');
        //pslist1.add(ps);
        //psid.add(ps.id);
        insert ps;
        //list <PermissionSetAssignment> permissionsetlist1= new list <PermissionSetAssignment>();
         PermissionSetAssignment testps = new PermissionSetAssignment(AssigneeId=testuser11.id,PermissionSetid =ps.id);
        //permissionsetlist1.add(testps);
        //testps.AssigneeId=testuser11.id;
        //testps.PermissionSetid =ps.id;
     

        insert testps;
        
           //SC_PermissionsetAssignmentToUser  sct=new SC_PermissionsetAssignmentToUser();
         string query;
        //query='SELECT Id,Permission_sets_assigned__c  FROM user limit 1';
        query='SELECT Id,Permission_sets_assigned__c  FROM user where id=\''+testUser11.id +'\' limit 1'; 
          
       
   Batch_PermsnAssignAllUser b = new Batch_PermsnAssignAllUser(query);
    database.executebatch(b);
    Test.stopTest();
    
       
        
        
}
static testMethod void testpermissionsetid12() 
    {Test.startTest(); 
        User testUser111 = Utils_TestMethods.createStandardUser('tst11');
        
         insert testUser111;
         //string  query='SELECT Id,Permission_sets_assigned__c  FROM user limit 1';
         string  query='SELECT Id,Permission_sets_assigned__c  FROM user where id=\''+testUser111.id +'\'limit 1';
          
       
   Batch_PermsnAssignAllUser b = new Batch_PermsnAssignAllUser(query);
    database.executebatch(b);
       Test.stopTest();  
       }
}