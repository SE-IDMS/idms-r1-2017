/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Alexande Gonzalo                                                         |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | VFC101_CloneEUS_TEST                                                               |
|                       |                                                                                    |
|     - Object(s)       | End User Support                                                                   |
|     - Description     |   - Test method for cloning an EUS											     |
|                       |																					 |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | March, 30th 2012                                                                |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
@isTest
public class VFC101_CloneEUS_TEST
{
	static testMethod void VFC101_CloneEUS_TEST()
    {
    	EUS_EndUserSupport__c eus = new EUS_EndUserSupport__c();
    	eus.Title__c = 'Test method';
    	eus.Status__c = 'Under Investigation';
    	eus.ApprovalStatus__c = 'Rejected';
    	insert eus;
    	
    	ApexPages.StandardController sc = new ApexPages.StandardController(eus);
    	VFC101_CloneEUS vfc101 = new VFC101_CloneEUS(sc);
    	String url = vfc101.getClonePage().getUrl();
    	
    	System.assert(url.contains(eus.Id));
    	System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10241, 'UTF-8')));
    	System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10242, 'UTF-8')));
    	System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10243, 'UTF-8')));
    	System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10244, 'UTF-8')));
    	System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10245, 'UTF-8')));
    	System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10246, 'UTF-8')));
    	System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10247, 'UTF-8')));
    	   
	}
    
}