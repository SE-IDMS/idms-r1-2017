/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 24/11/2010
    Description     : Test class for AP02_QuoteLinkFieldUpdate class and related OPP_QuoteLink__c triggers.
*/
@isTest
private class AP02_QuoteLinkFieldUpdate_TEST {
    private static Account acc;
    private static Opportunity opp;
    
    static{
        // create test data        
        acc = Utils_TestMethods.createAccount();        
        insert acc;
        OppAgreement__c agreementObj= Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id);         
                agreementObj.PhaseSalesStage__c = '3 - Identify & Qualify';   
                agreementObj.AgreementValidFrom__c = Date.today();         
                agreementObj.FrameOpportunityInterval__c = 'Monthly';
                agreementObj.SignatureDate__c = Date.today();
                agreementObj.Agreement_Duration_in_month__c = '3';
                agreementObj.AgreementType__c = 'Frame Agreement';
                insert agreementObj;
        opp = Utils_TestMethods.createOpportunity(acc.Id);
        opp.AgreementReference__c = agreementObj.Id;
        insert opp;
    }
    static testMethod void testOpportunityUpdates() {
        // invoke before insert trigger
         
        OPP_QuoteLink__c ql = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, QuoteStatus__c = 'Approved', ActiveQuote__c = true);
        
        insert ql;  
        // coverage only for update trigger
        ql.Description__c = 'Make any change';
        update ql;
    }
    
    static testMethod void testQuoteLinkUpdates() {
        //coverage for current approver and last approver field populate
        OPP_QuoteLink__c q01 = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 1);
        OPP_QuoteLink__c q02 = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 2);
        OPP_QuoteLink__c q03 = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 3);
        OPP_QuoteLink__c q04 = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 4);
        OPP_QuoteLink__c q05 = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 5);
        OPP_QuoteLink__c q06 = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), Approver6__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 6);
        OPP_QuoteLink__c q07 = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), Approver6__c = UserInfo.getUserId(), Approver7__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 7);
        OPP_QuoteLink__c q08 = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), Approver6__c = UserInfo.getUserId(), Approver7__c = UserInfo.getUserId(), Approver8__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 8);
        OPP_QuoteLink__c q09 = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), Approver6__c = UserInfo.getUserId(), Approver7__c = UserInfo.getUserId(), Approver8__c = UserInfo.getUserId(), Approver9__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 9);
        OPP_QuoteLink__c q10 = new OPP_QuoteLink__c(OpportunityName__c = opp.Id, Approver1__c = UserInfo.getUserId(), Approver2__c = UserInfo.getUserId(), Approver3__c = UserInfo.getUserId(), Approver4__c = UserInfo.getUserId(), Approver5__c = UserInfo.getUserId(), Approver6__c = UserInfo.getUserId(), Approver7__c = UserInfo.getUserId(), Approver8__c = UserInfo.getUserId(), Approver9__c = UserInfo.getUserId(), Approver10__c = UserInfo.getUserId(), TECH_ApprovalStep__c = 10);
        List<OPP_QuoteLink__c> ql_list = new List<OPP_QuoteLink__c>();
        ql_list.add(q01);
        ql_list.add(q02);
        ql_list.add(q03);
        ql_list.add(q04);
        ql_list.add(q05);
        ql_list.add(q06);
        ql_list.add(q07);
        ql_list.add(q08);
        ql_list.add(q09);
        ql_list.add(q10);
        insert ql_list;
    }
}