@isTest
private class AP_UpdateORFFields_TEST {

    static testMethod void myUnitTest() {

          List<Profile> profiles = [select id from profile where name='System Administrator'];
          List<UserRole> userRoles = [SELECT Id FROM UserRole WHERE Name = 'IPO_APAC'];
        if(profiles.size()>0) {
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            user.BypassWF__c = false;
            user.ProgramAdministrator__c = true;
            user.ProgramManager__c = true;
            user.UserRoleId = userRoles[0].id;
            system.runas(user){
                Country__c country = Utils_TestMethods.createCountry();
                country.countrycode__c ='ASM';
                insert country; 
                
                Assessment__c asm = new Assessment__c();
                
                PartnerPRogram__c gp1 = new PartnerPRogram__c();
                gp1 = Utils_TestMethods.createPartnerProgram();
                gp1.TECH_CountriesId__c = country.id;
                gp1.recordtypeid = Label.CLMAY13PRM15;
                gp1.ProgramStatus__c = Label.CLMAY13PRM47;
                insert gp1;
                
                System.Debug('The global program is :'+gp1);
                
                ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
                prg1.levelstatus__c = 'active';
                insert prg1;
                
                System.Debug('The global program level is:'+prg1);
                
                PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
                cp1.ProgramStatus__c = 'active';
                insert cp1;
                
                System.Debug('The country partner program is:'+cp1);
                
                ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
                prg2.levelstatus__c = 'active';
                insert prg2;
                
                System.Debug('The country program level is:'+prg2);
                
                asm = Utils_TestMethods.createAssessment();
                // asm.AutomaticAssignment__c = true;
                asm.PartnerProgram__c = cp1.id;
                asm.ProgramLevel__c = prg2.id;
                asm.Name = 'Test Assessment';
                insert asm;
                
                OpportunityRegistrationForm__c orf1 = Utils_TestMethods.createORF(country.id);
                orf1.Status__c = 'draft';
                orf1.ProductType__c = 'Test';
                orf1.PartnerProgram__c = cp1.id;
                orf1.ProgramLevel__c = prg2.id;
                insert orf1;
                
                PartnerAssessment__c pasm = new PartnerAssessment__c();
                pasm.OpportunityRegistrationForm__c = orf1.id;
                pasm.assessment__c = asm.id;
                insert pasm;

                OpportunityRegistrationForm__c orf2 = Utils_TestMethods.createORF(country.id);
                orf2.Status__c = System.Label.CLOCT13PRM16;
                orf2.RecordTypeId = System.Label.CLFEB14PRM04;
                orf2.PartnerProgram__c = cp1.id;
                orf2.ProgramLevel__c = prg2.id;
                insert orf2;

                PartnerAssessment__c pasm2 = new PartnerAssessment__c();
                pasm2.OpportunityRegistrationForm__c = orf2.id;
                pasm2.assessment__c = asm.id;
                insert pasm2; 

                OpportunityRegistrationForm__c orf3 = Utils_TestMethods.createORF(country.id);
                orf3.Status__c = System.Label.CLFEB14PRM07;
                orf3.RecordTypeId = System.Label.CLFEB14PRM04;
                orf3.PartnerProgram__c = cp1.id;
                orf3.ProgramLevel__c = prg2.id;
                insert orf3;

                PartnerAssessment__c pasm3 = new PartnerAssessment__c();
                pasm3.OpportunityRegistrationForm__c = orf3.id;
                pasm3.assessment__c = asm.id;
                insert pasm3;                
            }
        }
    }
}