@isTest
private class Batch_AccountClassification_update_TEST {
    static testMethod void testAccountClassification_Update() 
	{
	
    List<Account> Accnss = new List<Account>(); 
    List<Account> parAccnss = new List<Account>(); 
        
         Account par_Accns_1 = Utils_TestMethods.createAccount();
         par_Accns_1.ClassLevel1__c='XX';
         par_Accns_1.MarketSegment__c='IT3';        
         parAccnss.add(par_Accns_1);   
        
         Account par_Accns_2 = Utils_TestMethods.createAccount();
         par_Accns_2.ClassLevel1__c='EU'; 
         par_Accns_2.MarketSegment__c='BDZ'; 
         parAccnss.add(par_Accns_2);   
        
         Account par_Accns_22 = Utils_TestMethods.createAccount();
         par_Accns_22.ClassLevel1__c='XX'; 
         par_Accns_22.MarketSegment__c='BDZ'; 
         parAccnss.add(par_Accns_22);
        
         Account Ulti_Accns_3 = Utils_TestMethods.createAccount();
         Ulti_Accns_3.ClassLevel1__c='EU'; 
         Ulti_Accns_3.MarketSegment__c='IT2'; 
         parAccnss.add( Ulti_Accns_3);
        
         Account Ulti_Accns_33 = Utils_TestMethods.createAccount();
         Ulti_Accns_33.ClassLevel1__c='XX'; 
         Ulti_Accns_33.MarketSegment__c='IT2'; 
         parAccnss.add( Ulti_Accns_33);
        
         Account par_Accns_4 = Utils_TestMethods.createAccount();
         par_Accns_4.ClassLevel1__c='XX'; 
         par_Accns_4.MarketSegment__c='IDZ'; 
         parAccnss.add(par_Accns_4); 
        
         Account par_Accns_5 = Utils_TestMethods.createAccount();
         par_Accns_5.ClassLevel1__c='XX'; 
         par_Accns_5.MarketSegment__c='ID1'; 
         parAccnss.add(par_Accns_5);
        
         Account par_Accns_6 = Utils_TestMethods.createAccount();
         par_Accns_6.ClassLevel1__c='XX'; 
         par_Accns_6.MarketSegment__c='BD1'; 
         parAccnss.add(par_Accns_6);
        
         Account par_Accns_7 = Utils_TestMethods.createAccount();
         par_Accns_7.ClassLevel1__c='XX'; 
         par_Accns_7.MarketSegment__c='IT1'; 
         parAccnss.add(par_Accns_7);
        
         insert parAccnss;
	
        Account Accns_1 = Utils_TestMethods.createAccount();
        Accns_1.ClassLevel1__c='XX';
        Accns_1.Type='SA';
        Accns_1.PRMAccount__c=false;
		Accnss.add(Accns_1);
        
        Account Accns_2 = Utils_TestMethods.createAccount();
        Accns_2.ClassLevel1__c='XX';
        Accns_2.Type='TA';
        Accns_2.PRMAccount__c=false;
		Accnss.add(Accns_2);
         
        Account Accns_3 = Utils_TestMethods.createAccount();
        Accns_3.ClassLevel1__c='XX';
        Accns_3.Type='NA';
        Accns_3.PRMAccount__c=false;
		Accnss.add(Accns_3); 
        
        Account Accns_4 = Utils_TestMethods.createAccount();
        Accns_4.ClassLevel1__c='XX';
        Accns_4.Type='GSA';
       	Accnss.add(Accns_4);	
                        
        
	    Account Accns_5 = Utils_TestMethods.createAccount();
        Accns_5.ClassLevel1__c='XX';
        Accns_5.ParentId=par_Accns_2.id;
       	Accnss.add(Accns_5);
        
        Account Accns_6 = Utils_TestMethods.createAccount();
        Accns_6.ClassLevel1__c='XX';
        Accns_6.ParentId=par_Accns_1.id;
        Accns_6.UltiParentAcc__c=Ulti_Accns_3.id;
       	Accnss.add(Accns_6);
        
        Account Accns_7 = Utils_TestMethods.createAccount();
        Accns_7.ClassLevel1__c='XX';
        Accns_7.UltiParentAcc__c=par_Accns_2.id;
       	Accnss.add(Accns_7);
        
        Account Accns_8 = Utils_TestMethods.createAccount();
        Accns_8.ClassLevel1__c='XX';
        Accns_8.MarketSegment__c='ID5';
       	Accnss.add(Accns_8);
        
        Account Accns_9 = Utils_TestMethods.createAccount();
        Accns_9.ClassLevel1__c='XX';
        Accns_9.MarketSegment__c='BDZ';
       	Accnss.add(Accns_9);
        
        Account Accns_10 = Utils_TestMethods.createAccount();
        Accns_10.ClassLevel1__c='XX';
        Accns_10.MarketSegment__c='IT2';
       	Accnss.add(Accns_10);
        
        Account Accns_11 = Utils_TestMethods.createAccount();
        Accns_11.ClassLevel1__c='XX';
        Accns_11.MarketSegment__c='IDZ';
       	Accnss.add(Accns_11);
        
        Account Accns_12 = Utils_TestMethods.createAccount();
        Accns_12.ClassLevel1__c='XX';
        Accns_12.MarketSegment__c='ID1';
       	Accnss.add(Accns_12);
        
        Account Accns_13 = Utils_TestMethods.createAccount();
        Accns_13.ClassLevel1__c='XX';
        Accns_13.MarketSegment__c='BD1';
       	Accnss.add(Accns_13);
        
        Account Accns_14 = Utils_TestMethods.createAccount();
        Accns_14.ClassLevel1__c='XX';
        Accns_14.MarketSegment__c='IT1';
       	Accnss.add(Accns_14);
        
        Account Accns_15 = Utils_TestMethods.createAccount();
        Accns_15.ClassLevel1__c='XX';
        Accns_15.ParentId=par_Accns_1.id;
       	Accnss.add(Accns_15);    
        
        Account Accns_16 = Utils_TestMethods.createAccount();
        Accns_16.ClassLevel1__c='XX';
        Accns_16.ParentId=par_Accns_22.id;
       	Accnss.add(Accns_16);
        
        Account Accns_17 = Utils_TestMethods.createAccount();
        Accns_17.ClassLevel1__c='XX';
        Accns_17.ParentId=Ulti_Accns_33.id;
       	Accnss.add(Accns_17);
        
        Account Accns_18 = Utils_TestMethods.createAccount();
        Accns_18.ClassLevel1__c='XX';
        Accns_18.ParentId=par_Accns_4.id;
       	Accnss.add(Accns_18);
        
        Account Accns_19 = Utils_TestMethods.createAccount();
        Accns_19.ClassLevel1__c='XX';
        Accns_19.ParentId=par_Accns_5.id;
       	Accnss.add(Accns_19);
        
        Account Accns_20 = Utils_TestMethods.createAccount();
        Accns_20.ClassLevel1__c='XX';
        Accns_20.ParentId=par_Accns_6.id;
       	Accnss.add(Accns_20);
        
        Account Accns_21 = Utils_TestMethods.createAccount();
        Accns_21.ClassLevel1__c='XX';
        Accns_21.ParentId=par_Accns_7.id;
       	Accnss.add(Accns_21);
        
        
                   
        Account Accns_22 = Utils_TestMethods.createAccount();
        Accns_22.ClassLevel1__c='XX';
        Accns_22.UltiParentAcc__c=par_Accns_1.id;
       	Accnss.add(Accns_22);    
        
        Account Accns_23 = Utils_TestMethods.createAccount();
        Accns_23.ClassLevel1__c='XX';
        Accns_23.UltiParentAcc__c=par_Accns_22.id;
       	Accnss.add(Accns_23);
        
        Account Accns_24 = Utils_TestMethods.createAccount();
        Accns_24.ClassLevel1__c='XX';
        Accns_24.UltiParentAcc__c=Ulti_Accns_33.id;
       	Accnss.add(Accns_24);
        
        Account Accns_25 = Utils_TestMethods.createAccount();
        Accns_25.ClassLevel1__c='XX';
        Accns_25.UltiParentAcc__c=par_Accns_4.id;
       	Accnss.add(Accns_25);
        
        Account Accns_26 = Utils_TestMethods.createAccount();
        Accns_26.ClassLevel1__c='XX';
        Accns_26.UltiParentAcc__c=par_Accns_5.id;
       	Accnss.add(Accns_26);
        
        Account Accns_27 = Utils_TestMethods.createAccount();
        Accns_27.ClassLevel1__c='XX';
        Accns_27.UltiParentAcc__c=par_Accns_6.id;
       	Accnss.add(Accns_27);
        
        Account Accns_28 = Utils_TestMethods.createAccount();
        Accns_28.ClassLevel1__c='XX';
        Accns_28.UltiParentAcc__c=par_Accns_7.id;
       	Accnss.add(Accns_28);
        
        Account Accns_29 = Utils_TestMethods.createAccount();
        Accns_29.ClassLevel1__c='XX';
        Accns_29.ACCTYPE__C='ID';
       	Accnss.add(Accns_29);
                          
        insert Accnss;
	
		Test.startTest();
        Batch_AccountClassification_update sls = new Batch_AccountClassification_update('SELECT AccType__c,ClassLevel1__c,Id,MarketSegment__c,ParentId,parent.MarketSegment__c,PRMAccount__c,Type,UltiParentAcc__c,UltiParentAcc__r.MarketSegment__c,parent.ClassLevel1__c,UltiParentAcc__r.ClassLevel1__c FROM Account where ClassLevel1__c=\'XX\'');
        Database.executeBatch(sls,100);
        Test.stopTest();
	
	}
	
}