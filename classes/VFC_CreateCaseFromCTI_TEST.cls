/*
Author          : Vimal Karunakaran Global Delivery Team - Bangalore
Date Created    : 
Description     : Test class for VFC_CreateCaseFromCTI class.
*/
@isTest 
private class VFC_CreateCaseFromCTI_TEST {
    static testMethod void testCasePrepopulateCTIMethod() { 
        // creating Test Data
        Account  acc= Utils_TestMethods.createAccount(userInfo.getUserId()); 
        insert acc;
        Contact   con= Utils_TestMethods.createContact(acc.id,'TestContact');
        insert con;
        Country__c country=Utils_TestMethods.createCountry();
        country.CountryCode__c='CN';
        insert country;
        //CustomerCareTeam__c ccteam=Utils_TestMethods.createCustomerCareTeam();
        //ccteam.cti_skill__c='testskill';
        //insert ccteam;
        
        PageReference pageRef = Page.VFP_CreateCaseFromCTI;
        Test.setCurrentPage(pageRef);
        Case c = new Case();
        VFC_CreateCaseFromCTI controller = new VFC_CreateCaseFromCTI(new ApexPages.StandardController(c));
        ApexPages.currentPage().getParameters().put('ContactId', con.id);
        ApexPages.currentPage().getParameters().put('ANI', '+333333333');
        ApexPages.currentPage().getParameters().put('CN', 'true');
        controller.init();
        ApexPages.currentPage().getParameters().put('CountryCode','CN');
        controller.init();
        country.CountryCode__c='IN';
        controller.init();
        ApexPages.currentPage().getParameters().put('Ctiskill','testskill');
        controller.init();
    }   
}