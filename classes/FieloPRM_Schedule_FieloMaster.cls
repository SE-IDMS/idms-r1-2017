/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 10/08/2016
* Description: 
********************************************************************/

global without sharing class FieloPRM_Schedule_FieloMaster implements Schedulable {
    
    global static void scheduleNow(){
        FieloPRM_Schedule_FieloMaster.scheduleNow(0, 65);
    }
    
    global static void scheduleNow(Integer plusMin, Integer plusSec){
        DateTime nowTime = datetime.now().addMinutes(plusMin != null ? plusMin : 0).addSeconds(plusSec != null ? plusSec : 0);
        String Seconds      = '0';
        String Minutes      = nowTime.minute() < 10 ? '0' + nowTime.minute() : String.valueOf(nowTime.minute());
        String Hours        = nowTime.hour()   < 10 ? '0' + nowTime.hour()   : String.valueOf(nowTime.hour());
        String DayOfMonth   = String.valueOf(nowTime.day());
        String Month        = String.ValueOf(nowTime.month());
        String DayOfweek    = '?';
        String optionalYear = String.valueOf(nowTime.year());
        String CronExpression = Seconds+' '+Minutes+' '+Hours+' '+DayOfMonth+' '+Month+' '+DayOfweek+' '+optionalYear;
        System.schedule('FieloPRM_Schedule_FieloMaster ' + system.now() + ':' + system.now().millisecond(), CronExpression, new FieloPRM_Schedule_FieloMaster());
    }
    
    global void execute(SchedulableContext sc){
        //Check custom settings and default values
        CS_PRM_ApexJobSettings__c configFM   = CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Schedule_FieloMaster');
        CS_PRM_ApexJobSettings__c configLTM  = CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Batch_LevelTeamMembers');
        CS_PRM_ApexJobSettings__c configBME  = CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Batch_BadgeMemberExpiration');
        CS_PRM_ApexJobSettings__c configEPB  = CS_PRM_ApexJobSettings__c.getInstance('FieloCH.EventPendingBatch');
        CS_PRM_ApexJobSettings__c configBMPB = CS_PRM_ApexJobSettings__c.getInstance('FieloCH.BadgeMemberPendingBatch');
        CS_PRM_ApexJobSettings__c configCMPB = CS_PRM_ApexJobSettings__c.getInstance('FieloCH.ChallengeMemberPendingBatch');
        CS_PRM_ApexJobSettings__c configMFP  = CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Batch_MemberFeatProcess');
        CS_PRM_ApexJobSettings__c configMPP  = CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Batch_MemberPropProcess');
        
        if(configFM == null){
            configFM = new CS_PRM_ApexJobSettings__c(Name = 'FieloPRM_Schedule_FieloMaster', NumberValue__c = 5); insert configFM;
        }
        if(configLTM == null){
            configLTM = new CS_PRM_ApexJobSettings__c(Name = 'FieloPRM_Batch_LevelTeamMembers', F_PRM_BatchSize__c = 100); insert configLTM;
        }
        if(configBME == null){
            configBME = new CS_PRM_ApexJobSettings__c(Name = 'FieloPRM_Batch_BadgeMemberExpiration', F_PRM_BatchSize__c = 100); insert configBME;
        }
        if(configEPB == null){
            configEPB = new CS_PRM_ApexJobSettings__c(Name = 'FieloCH.EventPendingBatch', F_PRM_BatchSize__c = 100); insert configEPB;
        }
        if(configBMPB == null){
            configBMPB = new CS_PRM_ApexJobSettings__c(Name = 'FieloCH.BadgeMemberPendingBatch', F_PRM_BatchSize__c = 100); insert configBMPB;
        }
        if(configCMPB == null){
            configCMPB = new CS_PRM_ApexJobSettings__c(Name = 'FieloCH.ChallengeMemberPendingBatch', F_PRM_BatchSize__c = 1); insert configCMPB;
        }
        if(configMFP == null){
            configMFP = new CS_PRM_ApexJobSettings__c(Name = 'FieloPRM_Batch_MemberFeatProcess', F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10, F_PRM_TryCount__c = 5); insert configMFP;
        }
        if(configMPP == null){
            configMPP = new CS_PRM_ApexJobSettings__c(Name = 'FieloPRM_Batch_MemberPropProcess', F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10, F_PRM_TryCount__c = 5); insert configMPP;
        }
        
        //Batch Level Team Member
        if(configLTM.LastRun__c == null || configLTM.LastRun__c.date() < system.today()){
            FieloPRM_Batch_LevelTeamMembers.scheduleNow();
            configLTM.LastRun__c = system.now();
            update configLTM;
        }
        
        //Batch Badge Member Expiration
        if(configBME.LastRun__c == null || configBME.LastRun__c.date() < system.today()){
            FieloPRM_Batch_BadgeMemberExpiration.scheduleNow();
            configBME.LastRun__c = system.now();
            update configBME;
        }
        
        //Batch Event Process
        if(configEPB.LastRun__c == null || configEPB.LastRun__c.date() < system.today()){
            Database.executeBatch(new FieloCH.EventPendingBatch());
            configEPB.LastRun__c = system.now();
            update configEPB;
        }
        
        //Batch Badge Member Pending
        if(configBMPB.LastRun__c == null || configBMPB.LastRun__c.date() < system.today()){
            Database.executeBatch(new FieloCH.BadgeMemberPendingBatch());
            configBMPB.LastRun__c = system.now();
            update configBMPB;
        }
        
        //Batch Challenge Member Pending
        if(configCMPB.LastRun__c == null || configCMPB.LastRun__c.date() < system.today()){
            Database.executeBatch(new FieloCH.ChallengeMemberPendingBatch());
            configCMPB.LastRun__c = system.now();
            update configCMPB;
        }
        
        //Batch Member Feat Process
        FieloPRM_Batch_MemberFeatProcess.schedule(null);
        configMFP.LastRun__c = system.now();
        update configMFP;
        
        //Batch Member Properties Process
        FieloPRM_Batch_MemberPropProcess.schedule(null);
        configMPP.LastRun__c = system.now();
        update configMPP;
        
        //Delete schedule logs "completed"
        for(CronTrigger cron: [SELECT Id, State, CronJobDetail.Name FROM CronTrigger WHERE CronJobDetail.Name LIKE 'FieloPRM_%' AND State = 'DELETED' LIMIT 10]){
            try{system.abortJob(cron.id);}catch(Exception e){}
        }
        
        //Update the Last Run in the Custom Settings
        configFM.LastRun__c = system.now();
        update configFM;
        
        //Re-Schedule every N minutes (depends on the value in the field configFM.NumberValue__c)
        FieloPRM_Schedule_FieloMaster.scheduleNow(configFM.NumberValue__c != null ? configFM.NumberValue__c.intValue() : 5, 0);
    }
    
}