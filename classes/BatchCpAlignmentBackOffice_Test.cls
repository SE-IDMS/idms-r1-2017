@isTest(SeeAllData=false)
Private class BatchCpAlignmentBackOffice_Test{
      static testMethod void testcpalignment() {
          recordtype slrt1 = [select id from recordtype where DeveloperName = 'ServiceLine' limit 1];
          
          StateProvince__c st= new StateProvince__c(name = 'testsate',CountryCode__c='IN',StateProvinceCode__c='HH');
          insert st;
          
           Account objAccount = new account(name= 'test account',Street__c='test city',City__c='test city1',
           StateProvince__c=st.id, ZipCode__c='ew324r');
          
            insert objAccount;
          
          SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c(RecordTypeId =slrt1.id, name='testsc');
          insert sc;
          
     
          SVMXC__Service_Contract_Products__c cp = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c=sc.id,InstalledAtAccount__c=objAccount.id,
         installedAtStreet__c= 'test city', installedAtCity__c ='test city1',installedAtStateProvinceCode__c ='test state',installedAtZipCode__c='ew324r');
          insert cp;
          
        
          
          test.starttest();
              BatchCpAlignmentBackOffice CoverPorduct = new BatchCpAlignmentBackOffice ();
                Database.executeBatch(CoverPorduct,1);
           test.stoptest();
      }

}