@isTest
private class VFC_Specialization_Test
{
    static testMethod void testVFC_Specializations()
    {
        List<Profile> profiles = [select id from profile where name='System Administrator'];
        
        if(profiles.size()>0){
            User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            usr.BypassVR__c = true;
            usr.ProgramAdministrator__c = true;
            usr.UserRoleID = '00EA0000000MiDK';
            System.runas(usr){ 
        List<User> lstPrgApprover = [select id from user where isActive=true and ProgramApprover__c=true limit 20];
        List<User> lstPrgAdmin = [select id from user where isActive=true and ProgramAdministrator__c=true limit 20];
        List<User> lstPrgManager = [select id from user where isActive=true and ProgramManager__c=true limit 20];
        List<User> lstPrgOwner = [select id from user where isActive=true and ProgramOwner__c=true limit 20];
        
        SpecializationCatalog__c spc1 = new SpecializationCatalog__c();
        spc1.name = 'Test Catalog 1';
        spc1.Active__c =true;
        spc1.ExpirationPeriod__c = '12';
        spc1.description__c = 'test 2';
        insert spc1;
        
        Specialization__c spe1 = new Specialization__c();
        spe1.name = 'spe 1';
        spe1.SpecializationCatalog__c = spc1.id;
        spe1.level__C= 'Basic';
        spe1.ExpirationPeriod__c = '12';
        spe1.status__c= 'Active';
        spe1.ProgramApprover__c = lstPrgApprover[0].id;
        spe1.ProgramAdministrator__c = lstPrgAdmin[0].id ;
        insert spe1;
        
        DomainsOfExpertiseCatalog__c doec = new DomainsOfExpertiseCatalog__c();
        doec.name = 'DE0001';
        doec.DomainsOfExpertiseName__c = 'Expertise 1';
        doec.Active__c = true;
        insert doec;
        
        Apexpages.Standardcontroller cont1 = new Apexpages.Standardcontroller(spe1);
        Pagereference pg1 = Page.VFP_SpecializationClassification; 
        Test.setCurrentPage(pg1);        
        VFC_SpecializationClassification rcp = new VFC_SpecializationClassification(cont1);
        
        Apexpages.Standardcontroller cont2 = new Apexpages.Standardcontroller(spe1);
        Pagereference pg2 = Page.VFP_SpecializationDomainsOfExpertise; 
        Test.setCurrentPage(pg2);        
        VFC_SpecializationDomainsOfExpertise rcp2 = new VFC_SpecializationDomainsOfExpertise(cont2);
        
        Apexpages.Standardcontroller cont3 = new Apexpages.Standardcontroller(spe1);
        Pagereference pg3 = Page.VFP_SpecializationTargetCountries; 
        Test.setCurrentPage(pg3);        
        VFC_SpecializationTargetCountries rcp3 = new VFC_SpecializationTargetCountries(cont3);
        
        Apexpages.Standardcontroller cont4 = new Apexpages.Standardcontroller(spe1);
        Pagereference pg4 = Page.VFP_SpecializationTargetMarket; 
        Test.setCurrentPage(pg4);        
        VFC_SpecializationTargetMarket rcp4 = new VFC_SpecializationTargetMarket(cont4);
        
        /////Classification
        List<ClassificationLevelCatalog__c> lscl = new List<ClassificationLevelCatalog__c>();
        ClassificationLevelCatalog__c cl1 = new ClassificationLevelCatalog__c();
        cl1.Active__c = true;
        cl1.ClassificationLevelName__c = 'Cl 1';
        lscl.add(cl1);
        ClassificationLevelCatalog__c cl2 = new ClassificationLevelCatalog__c();
        cl2.Active__c = true;
        cl2.ClassificationLevelName__c = 'Cl 2';
        lscl.add(cl2);
        insert lscl;
        
        List<ClassificationLevelCatalog__c> lsc2 = new List<ClassificationLevelCatalog__c>();
        ClassificationLevelCatalog__c cl3 = new ClassificationLevelCatalog__c();
        cl3.Active__c = true;
        cl3.ClassificationLevelName__c = 'Cl 3';
        cl3.ParentClassificationLevel__c = cl1.id;
        lsc2.add(cl3);
        ClassificationLevelCatalog__c cl4 = new ClassificationLevelCatalog__c();
        cl4.Active__c = true;
        cl4.ClassificationLevelName__c = 'Cl 4';
        cl4.ParentClassificationLevel__c = cl2.id;
        lsc2.add(cl4);
        insert lsc2;
        
        Apexpages.Standardcontroller contcl = new Apexpages.Standardcontroller(spe1);
        Pagereference pgcl = Page.VFP_AddRemoveSpecClassification ;
        pgcl.getParameters().put('type','Level1'); 
        pgcl.getParameters().put('id',spe1.id); 
        Test.setCurrentPage(pgcl);
               
        
        VFC_AddRemoveSpecClassification rcpcl = new VFC_AddRemoveSpecClassification(contcl );
        rcpcl.selClassificationLevel = rcpcl.allClassificationLevel;
        rcpcl.allClassificationLevel= new List<SelectOption>();
        rcpcl.updateSpecializationClassification();
        
        rcpcl= new VFC_AddRemoveSpecClassification(contcl );
        rcpcl.allClassificationLevel= rcpcl.selClassificationLevel ;
        rcpcl.selClassificationLevel = new List<SelectOption>();
        rcpcl.updateSpecializationClassification();
        
        rcpcl.selClassificationLevel = rcpcl.allClassificationLevel;
        rcpcl.allClassificationLevel= new List<SelectOption>();
        rcpcl.updateSpecializationClassification();
        
        //Testing Level 2
        pgcl = Page.VFP_AddRemoveSpecClassification ;
        pgcl.getParameters().put('type','Level2'); 
        pgcl.getParameters().put('id',spe1.id); 
        Test.setCurrentPage(pgcl);
        
        Apexpages.Standardcontroller contcl2 = new Apexpages.Standardcontroller(spe1);
        rcpcl= new VFC_AddRemoveSpecClassification (contcl2 );
        rcpcl.selClassificationLevel = rcpcl.allClassificationLevel;
        rcpcl.allClassificationLevel= new List<SelectOption>();
        rcpcl.updateSpecializationClassification();
        
        rcpcl= new VFC_AddRemoveSpecClassification (contcl2 );
        rcpcl.allClassificationLevel= rcpcl.selClassificationLevel ;
        rcpcl.selClassificationLevel = new List<SelectOption>();
        rcpcl.updateSpecializationClassification();
        ///////
       
        //Countries 
        List<Country__c> Countries = new list<Country__c>();
        For(integer c=0; c<10 ; c++)
        {
            Country__c country = new country__c(Name = 'TestCountry'+c,CountryCode__c ='XY'+c);
            Countries.add(country);
        }
        Database.SaveResult[] CountryListInsert= Database.insert(Countries, true);
        
        Apexpages.Standardcontroller conttc = new Apexpages.Standardcontroller(spe1);
        Pagereference pgtc = Page.VFP_AddRemoveSpecializationCountries;
        pgtc.getParameters().put('id',spe1.id); 
        Test.setCurrentPage(pgtc );
        
        VFC_AddRemoveSpecializationCountries rcptc = new VFC_AddRemoveSpecializationCountries(conttc );
        rcptc .selCountries= rcptc .allCountries;
        rcptc .allCountries= new List<SelectOption>();
        rcptc .updateCountries();
       
        rcptc = new VFC_AddRemoveSpecializationCountries(conttc );
        rcptc .allCountries= rcptc .selCountries;
        rcptc .selCountries= new List<SelectOption>();
        rcptc .updateCountries();
        
        rcptc .selCountries= rcptc .allCountries;
        rcptc .allCountries= new List<SelectOption>();
        rcptc .updateCountries();
        
        //Domains of expertise
        DomainsOfExpertiseCatalog__c doec1 = new DomainsOfExpertiseCatalog__c();
        doec1.name = 'DE0001';
        doec1.DomainsOfExpertiseName__c = 'Expertise 1';
        doec1.Active__c = true;
        insert doec1;
        
        DomainsOfExpertiseCatalog__c doec2 = new DomainsOfExpertiseCatalog__c();
        doec2.name = 'DE0002';
        doec2.DomainsOfExpertiseName__c = 'Expertise 2';
        doec2.Active__c = true;
        insert doec2;
        
        DomainsOfExpertiseCatalog__c doec3 = new DomainsOfExpertiseCatalog__c();
        doec3.name = 'DE0003';
        doec3.DomainsOfExpertiseName__c = 'Expertise 3';
        doec3.Active__c = true;
        doec3.ParentDomainsOfExpertise__c = doec1.id;
        insert doec3;
        
        DomainsOfExpertiseCatalog__c doec4 = new DomainsOfExpertiseCatalog__c();
        doec4.name = 'DE0004';
        doec4.DomainsOfExpertiseName__c = 'Expertise 4';
        doec4.Active__c = true;
        doec4.ParentDomainsOfExpertise__c = doec2.id;
        insert doec4;
        
        Apexpages.Standardcontroller contde = new Apexpages.Standardcontroller(spe1);
        Pagereference pgde = Page.VFP_UpdateSpeDomainsOfExpertise;
        pgde .getParameters().put('type','Level1'); 
        pgde .getParameters().put('id',spe1.id); 
        Test.setCurrentPage(pgde );
        
        VFC_UpdateSpeDomainsOfExpertise rcpde = new VFC_UpdateSpeDomainsOfExpertise(contde );
        rcpde .selDE = rcpde .allDE;
        rcpde .allDE = new List<SelectOption>();
        rcpde .Save();
        
        rcpde = new VFC_UpdateSpeDomainsOfExpertise(contde );
        rcpde .allDE = rcpde .selDE ;
        rcpde .selDE = new List<SelectOption>();
        rcpde .Save();
        
        rcpde .selDE = rcpde .allDE;
        rcpde .allDE = new List<SelectOption>();
        rcpde .Save();
        //////////
        
        //Market Segemtn
        list<MarketSegmentCatalog__c> lstMarketSeg = new list<MarketSegmentCatalog__c>();
        for(integer i=0;i<5;i++)
        {
            MarketSegmentCatalog__c segment = new MarketSegmentCatalog__c();
            segment.name = 'TEST'+i;
            segment.MarketSegmentName__c = 'TEST'+i;
            lstMarketSeg.add(segment);
        }
        insert lstMarketSeg;
        
        list<MarketSegmentCatalog__c> lstMarketSubSeg = new list<MarketSegmentCatalog__c>();
        for(integer i=0;i<5;i++)
        {
            MarketSegmentCatalog__c subsegment = new MarketSegmentCatalog__c();
            subsegment .name = 'TEST'+i;
            subsegment .MarketSegmentName__c = 'TEST'+i;
            subsegment.ParentMarketSegment__c = lstMarketSeg[i].id;
            lstMarketSubSeg .add(subsegment);
        }
        insert lstMarketSubSeg ;
        
        Apexpages.Standardcontroller contmse = new Apexpages.Standardcontroller(spe1);
        Pagereference pgms = Page.VFP_UpdateSpecializationTargetMarket;
        pgms .getParameters().put('type','Segment'); 
        pgms .getParameters().put('id',spe1.id); 
        Test.setCurrentPage(pgms );
        
        VFC_UpdateSpecializationTargetMarket rcpms = new VFC_UpdateSpecializationTargetMarket(contmse );
        rcpms .selMarketSeg= rcpms .allMarketSeg;
        rcpms .allMarketSeg= new List<SelectOption>();
        rcpms .updateMarketSegment();
        
        rcpms = new VFC_UpdateSpecializationTargetMarket(contmse );
        rcpms .allMarketSeg= rcpms .selMarketSeg;
        rcpms .selMarketSeg= new List<SelectOption>();
        rcpms .updateMarketSegment();
        
        rcpms .selMarketSeg= rcpms .allMarketSeg;
        rcpms .allMarketSeg= new List<SelectOption>();
        rcpms .updateMarketSegment();
        
        //Testing SubSegment2
        pgms = Page.VFP_UpdateSpecializationTargetMarket;
        pgms .getParameters().put('type','subsegment'); 
        pgms .getParameters().put('id',spe1.id); 
        Test.setCurrentPage(pgms );
        
        Apexpages.Standardcontroller contms2 = new Apexpages.Standardcontroller(spe1);
        rcpms = new VFC_UpdateSpecializationTargetMarket(contms2 );
        rcpms .selMarketSeg = rcpms .allMarketSeg;
        rcpms .allMarketSeg= new List<SelectOption>();
        rcpms .updateMarketSegment();
        
        rcpms = new VFC_UpdateSpecializationTargetMarket(contms2 );
        rcpms .allMarketSeg= rcpms .selMarketSeg;
        rcpms .selMarketSeg= new List<SelectOption>();
        rcpms .updateMarketSegment();
        
        ///////
        
        /////New Country Spec
         Recordtype acctSpeActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountSpecializationRequirement__c' limit 1];
        Recordtype acctPrgActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
       
       
        Recordtype SpeciliActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'SpecializationRequirement__c' limit 1];
        
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
           RequirementCatalog__c req = Utils_TestMethods.createRequirementCatalog();
            req.recordtypeid = recCatalogActivityReq.Id;
            insert req;
             
        SpecializationRequirement__c sr1 = new SpecializationRequirement__c();
        sr1.Active__c = false;
        sr1.RequirementCatalog__c = req.id;
        sr1.Specialization__c = spe1.id;
        insert sr1;
        
        sr1.Active__c = true;
        update sr1;
        
        
        
       Apexpages.Standardcontroller contncr = new Apexpages.Standardcontroller(spe1);
        pg1 = Page.VFP_NewCountrySpecialization ;
       
        pg1.getParameters().put('GlobalSpeId',spe1.id); 
        pg1.getParameters().put('type','cluster');  
        
        Test.setCurrentPage(pg1);
                
        VFC_NewCountrySpecialization rcpncr = new VFC_NewCountrySpecialization (contncr );
        rcpncr.createCountrySpecialization();
        
            RequirementCatalog__c req2 = Utils_TestMethods.createRequirementCatalog();
            req.recordtypeid = recCatalogActivityReq.Id;
            insert req2;
        
        SpecializationRequirement__c sr2 = new SpecializationRequirement__c();
        sr2.Active__c = false;
        sr2.RequirementCatalog__c = req2.id;
        sr2.Specialization__c = spe1.id;
        insert sr2;
        
        sr2.Active__c = true;
        update sr2;
        
        
        Apexpages.Standardcontroller contnc2 = new Apexpages.Standardcontroller(spe1);
        Pagereference pg6 = Page.VFP_NewCountrySpecialization ;
       
        pg6.getParameters().put('GlobalSpeId',spe1.id);
        pg6.getParameters().put('type','cluster');  
        Test.setCurrentPage(pg6);
                
        VFC_NewCountrySpecialization  rcpnc2 = new VFC_NewCountrySpecialization (contnc2 );
        rcpnc2 .createCountrySpecialization();
        ///
    }
    }    
    }//End of testMethod
}