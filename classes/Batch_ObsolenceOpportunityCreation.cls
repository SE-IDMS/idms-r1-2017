/*
    Author          : Deepak
    Date Created    : 27/01/2015 April Release
    Description     : Batch Apex class Opportunity Creation.
              
*/
    global class Batch_ObsolenceOpportunityCreation implements Database.Batchable<sObject> ,Schedulable{

        
        public String scheduledJobName = Label.CLOCT15SRV10;
        public Integer rescheduleInterval = Integer.ValueOf(Label.CLOCT15SRV07);
        public Integer querylimit = Integer.ValueOf(Label.CLOCT15SRV09);
        public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};
        global boolean debug = false;
        global Database.QueryLocator start(Database.BatchableContext BC){ 
         
        Integer obsolence = Integer.ValueOf(Label.CLAPR15SRV74);
        Set<String> buset = new Set<String>();
        Set<String> buset2 = new Set<String>();
        Set<String> buset3 = new Set<String>();
        List<CS_BUCountry__c>  listBuCountry = CS_BUCountry__c.getall().values();
        for(CS_BUCountry__c obj:listBuCountry ){
             if(obj.BatchType__c =='OBS'){
                buset.add(obj.BusinessUnit__c +obj.CountryCode__c);
               }
        }
    
        return Database.getQueryLocator([select id,EndOfWarrantyOpportunityGenerated__c,ObsolescenceOpportunityGenerated__c ,EndOfWarrantyOpportunityTriggered__c,ObsolescenceOpportunityTriggered__c,SVMXC__Product__r.Serviceability__c,Tech_IpAssociatedToSC__c,AssetCategory2__c,UnderContract__c,SVMXC__Company__c,SVMXC__Company__r.Name,SVMXC__Product__r.BusinessUnit__c,  SVMXC__Company__r.MarketSegment__c,SVMXC__Warranty_End_Date__c,SVMXC__Company__r.LeadingBusiness__c,SVMXC__Company__r.AccType__c,SVMXC__Product__r.ProductGDP__c, SVMXC__Company__r.Country__c,SKUToCreate__c,CommissioningDateInstallDate__c 
                                        from SVMXC__Installed_Product__c Where  ObsolescenceOpportunityGenerated__c=false AND ObsolescenceOpportunityTriggered__c =: obsolence AND SVMXC__Product__r.ProductGDP__c !=null  AND ProductBuCountry__c in :buset LIMIT :querylimit]);
                                       
                                 
       
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){ 
        system.debug('scope11'+scope.size());
        AP_OpportunityCreationFromIB.ProcessIPforObsolence(scope);
        //Database.update(scope,false);
    }


   global void finish(Database.BatchableContext BC){
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        System.debug('### scheduledJobDetailList: '+scheduledJobDetailList);

        if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) {
            
            List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
            System.debug('### scheduledJobList: '+scheduledJobList);
            if (scheduledJobList != null && scheduledJobList.size()>0) {
                canReschedule = False;
            }
        }
        if (canReschedule && !Test.isRunningTest() ) {
            System.scheduleBatch(new Batch_ObsolenceOpportunityCreation(), scheduledJobName, rescheduleInterval);
        }
    }
    global void execute(SchedulableContext sc) {
        Batch_ObsolenceOpportunityCreation OptyCreation = new Batch_ObsolenceOpportunityCreation();
        Database.executeBatch(OptyCreation);
    }   
}