global class ServiceMaxScheduleLocationValidation implements Schedulable {
	
	global void execute(SchedulableContext sc) {
        ServiceMaxBatchLocationValidation batch = new ServiceMaxBatchLocationValidation();
        Database.executeBatch(batch, 10);
    } 
    
    @isTest
    public static void test() 
    {
        ServiceMaxScheduleLocationValidation job = new ServiceMaxScheduleLocationValidation();
        job.execute(null);
    }

}