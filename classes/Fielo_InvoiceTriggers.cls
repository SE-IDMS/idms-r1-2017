/************************************************************
* Developer: Denis Aranda                                   *
* Created Date: 19/08/2014                                  *
************************************************************/
public without sharing class Fielo_InvoiceTriggers {
    
    public static void transactionCreater(){
        
        map<Id, FieloEE__Transaction__c> mapInvTran = new map<Id, FieloEE__Transaction__c>();
        
        for(Fielo_Invoice__c inv: (List<Fielo_Invoice__c>)trigger.new){
            if(inv.F_Transaction__c == null && inv.F_Member__c != null && Fielo_Invoice__c.F_Status__c != null){
                mapInvTran.put(inv.Id ,new FieloEE__Transaction__c(
                    FieloEE__Member__c = inv.F_Member__c,
                    FieloEE__Type__c = 'Invoice',
                    F_Invoice__c = inv.Id
                ));
            }
        }
        
        if(!mapInvTran.isEmpty()){
            try{insert mapInvTran.values();}catch(Exception e){}
            
            List<Fielo_Invoice__c> listInvoiceToUpdate = new List<Fielo_Invoice__c>();
            for(FieloEE__Transaction__c tra: mapInvTran.values()){
                Fielo_Invoice__c inv = new Fielo_Invoice__c(Id = tra.F_Invoice__c, F_Transaction__c = tra.Id);
                listInvoiceToUpdate.add(inv);
            }
            update listInvoiceToUpdate;
        }
    }
}