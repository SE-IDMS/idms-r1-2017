@isTest
private class PRJ_UpdateStatusController_TEST1
{
    static testMethod void test_PRJ_UpdateStatusController()
    {   
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
        insert testDMTA1; 
        
        PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
        testProj.ParentFamily__c = 'Business';
        testProj.Parent__c = 'Buildings';
        testProj.BusGeographicZoneIPO__c = 'Global';
        testProj.GeographicZoneSE__c = 'Global';
        testProj.Tiering__c = 'Tier 1';
        testProj.ProjectRequestDate__c = system.today();
        testProj.Description__c = 'Test';
        testProj.GlobalProcess__c = 'Customer Care';
        testProj.ProjectClassification__c = 'Infrastructure';
        testProj.BusinessTechnicalDomain__c = 'Supply Chain';
        insert testProj;    
        
        Budget__c testBud = Utils_TestMethods.createBudget(testProj,true);
        insert testBud; 
        DMTFundingEntity__c fund = new DMTFundingEntity__c();
        fund.ProjectReq__c = testProj.Id;
        insert fund;
        fund.ActiveForecast__c = true;
        fund.SelectedinEnvelop__c = true;
        fund.HFMCode__c = '2345';
        Update fund;
        

        DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
        insert testDMTA2;   
        testProj.NextStep__c='Created';
        update testProj;
        PRJ_UpdateStatusController vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
        vfCtrl.updateProjectRequest();
        vfCtrl.backToProjectRequest();
        
        test.startTest();
        
        DMTAuthorizationMasterData__c testD1 = new DMTAuthorizationMasterData__c();
        testD1.AuthorizedUser5__c = UserInfo.getUserId();
        testD1.AuthorizedUser6__c = UserInfo.getUserId();
        testD1.NextStep__c = 'Quoted';
        testD1.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
        insert testD1;
        
        testProj.NextStep__c = 'Valid';
        update testProj;
        vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
        vfCtrl.updateProjectRequest();
        vfCtrl.backToProjectRequest();
        
        
        DMTAuthorizationMasterData__c testDMTA5 = new DMTAuthorizationMasterData__c();
        testDMTA5.AuthorizedUser1__c = UserInfo.getUserId();
        testDMTA5.AuthorizedUser5__c = UserInfo.getUserId();
        testDMTA5.AuthorizedUser6__c = UserInfo.getUserId();
        testDMTA5.NextStep__c = 'Fundings Authorized';
        testDMTA5.Parent__c = testProj.Parent__c;
        testDMTA5.BusGeographicZoneIPO__c = testProj.BusGeographicZoneIPO__c;
        testDMTA5.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
        insert testDMTA5; 
        
        testProj.WorkType__c = 'Master Project';
        testProj.NextStep__c='Quoted';
        testProj.substatus__c = 'Working';
        system.debug('>>>>>' +testProj.NextStep__c); 
        update testProj;
        
        vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
        vfCtrl.updateProjectRequest();
        vfCtrl.backToProjectRequest();
        
        testProj.NextStep__c='Fundings Authorized';
        testProj.AuthorizeFunding__c = 'Yes';
        system.debug('>>>>>' +testProj.NextStep__c); 
        update testProj;
        vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
        vfCtrl.updateProjectRequest();
        vfCtrl.backToProjectRequest();
        
        
       
        
        test.stopTest();
        
        DMTAuthorizationMasterData__c testD11 = new DMTAuthorizationMasterData__c();
        testD11.AuthorizedUser5__c = UserInfo.getUserId();
        testD11.AuthorizedUser6__c = UserInfo.getUserId();
        testD11.NextStep__c = 'Quoted';
        testD11.BusinessTechnicalDomain__c = testProj.BusinessTechnicalDomain__c;
        insert testD11;
        
        testProj.PLImpact2014__c = 25000;
        testProj.NextStep__c='Quoted';
        testProj.subStatus__c = 'Partial';
        update testProj;
        vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
        vfCtrl.updateProjectRequest();
        vfCtrl.backToProjectRequest();
        
        testProj.subStatus__c = 'Complete';
        update testProj;
        vfCtrl = new PRJ_UpdateStatusController(new ApexPages.StandardController(testProj));
        vfCtrl.updateProjectRequest();
        vfCtrl.backToProjectRequest();
        
        
}
}