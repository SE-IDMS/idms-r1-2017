/*   Author: Nicolas PALITZYNE (Accenture)  
     Date Of Creation: 17/11/2011   
     Description : Class containing a static method which will update the answer to customer for the related case 
*/ 
 
@isTest
private class AP33_UpdateAnswerToCustomer_TEST
{
    static testMethod void testAP33() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;
               
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        insert Case1;
    
        // the AP33 contains an unique method updating case when inserting P&A external reference
        List<CSE_ExternalReferences__c> anExternalReferenceList = new List<CSE_ExternalReferences__c>();
        
        CSE_ExternalReferences__c anExternalReference = new CSE_ExternalReferences__c();
        anExternalReference.Type__c = 'P&A';
        anExternalReference.Case__c = Case1.Id;
        anExternalReference.Description__c = '*** TEST *** \n Line 1 \n Line 2 \n Line3';
        
        anExternalReferenceList.add(anExternalReference); 

        Case case2 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        case2.AnswerToCustomer__c = '---- Answer to Customer -----';
        insert Case2;

        CSE_ExternalReferences__c anExternalReference1 = new CSE_ExternalReferences__c();
        anExternalReference1.Type__c = 'P&A';
        anExternalReference1.Case__c = Case2.Id;
        anExternalReference1.Description__c = '*** TEST *** Line 1 Line 2 Line3';
        
        anExternalReferenceList.add(anExternalReference1);   
    
        Test.startTest();
        insert anExternalReferenceList;
        Test.stopTest();
    }
    //added by suhail for q3 2016 release start
    static testMethod void WorkOrderFromCase(){
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;
               
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        insert Case1;
        
        sVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(account1.id);
                     workOrder.Work_Order_Category__c='On-site';                 
                     workOrder.SVMXC__Order_Type__c='Maintenance';
                     workOrder.SVMXC__Problem_Description__c = 'BLALBLALABLA';
                     workOrder.SVMXC__Order_Status__c = 'UnScheduled';
                     workOrder.CustomerRequestedDate__c = Date.today();
                     workOrder.Service_Business_Unit__c = 'IT';
                     workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
                     workOrder.SendAlertNotification__c = true;
                     //workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
                     workOrder.BackOfficeReference__c = '111111';
                     //workOrder.Parent_Work_Order__c=workOrder0.id;
                     workOrder.Comments_to_Planner__c='testing';
                     //workOrder.SVMXC__Group_Member__c =Tech_Equip1.id;
                    // workOrder.SVMXC__Group_Member__c = sgm1.Id;
                    // workOrder.CustomerTimeZone__c = 'Pacific/Kiritimati';
                     //workOrder.SVMXC__Site__c = site.Id;
                     //workOrder.SVMXC__Service_Contract__c = testContract1.Id;
                     workOrder.SVMXC__Company__c  =account1.Id;
                     workOrder.SVMXC__Case__c = Case1.id;
                     //workOrder.SVMXC__Contact__c  =testContact.Id;
                    // workOrder.SVMXC__Component__c = ip1.id;
                    // workOrder.SVMXC__Case__c = testCase.Id;
                     insert workOrder;
        
        
    }
}