/*
    Author          : Accenture IDC Team 
    Date Created    : 8/08/2011
    Description     : Class utilised by triggers AccountBeforeUpdate and AccountBeforeInsert.
    
    CH01:Srikant:19/10/2011:SEPRelease:Channel Partner cannot become the Owner of any Account
    5/Nov/2012  Srinivas Nallapati  BR-2083 Copy Account address to Contact address
*/

public class AP30_UpdateAccountOwner
{

    /**Method automatically updates the Tech account owner field from owner ID*/
    public static void populateAccountOwner(List<account> accounts)
    {
    system.debug('populateAccountOwner method begins');
        for(account acc:accounts)
        {
            acc.Tech_AccountOwner__c=acc.ownerID;
        }
    system.debug('populateAccountOwner method Ends');
    }   
   
    //*********************************************************************************
    // Method Name : populateAccount LB
    // Purpose : To populate Account LB base on Level1, Level2, MarketSegment
    // Created by : Hari Krishna Singara - Global Delivery Team
    // Date created : 
    // Modified by :
    // Date Modified :
    // Remarks : For Sep - 12 Release
    ///*********************************************************************************/
    public static void populateAccountLB(List<account> accounts){
        
        String userLeadingBU=[Select id,UserBusinessUnit__c from User where id=:UserInfo.getUserId() limit 1].UserBusinessUnit__c ;
        
        for(Account a: accounts){
         //----START BR-5861: Default Consumer account Leading Business (BU) based on User Business field of the User---//    
            if(a.RecordTypeId==System.Label.CLDEC12ACC01)
            {
            //For Consumer Accounts
                  if(userLeadingBU!='' && userLeadingBU!=null && CS_OpportunityLeadingBUMapping__c.getAll().containsKey(userLeadingBU))
                  {
                      a.LeadingBusiness__c = CS_OpportunityLeadingBUMapping__c.getValues(userLeadingBU).OpportunityLeadingBU__c;
                  }
                  else
                  {   
                      //Default the consumer account leading BU to 'Partner' for all the User leading BU's that are not mapped in the custom setting CS_OpportunityLeadingBUMapping__c
                      a.LeadingBusiness__c = Label.CLAPR15ACC06; 
                  }   
            }
        //----END BR-5861: Default Consumer account Leading Business (BU) based on User Business field of the User---//
            else
            {
            //For Business Accounts
                String key1='',key2='',key3='';
                key1 = getKey(a.ClassLevel1__c, a.ClassLevel2__c, a.MarketSegment__c);
                key2 = getKey(a.ClassLevel1__c, null, a.MarketSegment__c);
                key3 = getKey(a.ClassLevel1__c, a.ClassLevel2__c , null);
               
                System.debug ('--- a.key1:::: '+key1+' key2:::: '+key2+' key3:::: '+key3);
                
                if(CS_AccountSimplification__c.getValues(getKey(a.ClassLevel1__c, a.ClassLevel2__c, a.MarketSegment__c)) != null){
                   
                    a.LeadingBusiness__c  = CS_AccountSimplification__c.getValues(getKey(a.ClassLevel1__c, a.ClassLevel2__c, a.MarketSegment__c)).LeadingBusiness__c;
                }
                else if(CS_AccountSimplification__c.getValues(getKey(a.ClassLevel1__c, null, a.MarketSegment__c)) != null)
                {
                     a.LeadingBusiness__c  = CS_AccountSimplification__c.getValues(getKey(a.ClassLevel1__c, null, a.MarketSegment__c)).LeadingBusiness__c;   
                }
                else if(CS_AccountSimplification__c.getValues(getKey(a.ClassLevel1__c, a.ClassLevel2__c , null)) != null){
                     a.LeadingBusiness__c  = CS_AccountSimplification__c.getValues(getKey(a.ClassLevel1__c, a.ClassLevel2__c , null)).LeadingBusiness__c;
                }
            
            System.debug ('--- a.ClassLevel1__c:::: '+a.ClassLevel1__c);
            System.debug ('--- a.ClassLevel2__c:::: '+a.ClassLevel2__c);
            System.debug ('--- a.MarketSegment__c:::: '+a.MarketSegment__c);
            System.debug ('--- a.LeadingBusiness__c :::: '+a.LeadingBusiness__c);
            }
            
        }
        
        
        
    }
    
    public Static string getKey(string CCL1, string CCL2, string MS)
    {
            String key='';
            if(CCL1 != null){
                key +=CCL1+'_';
            }
            if(CCL2 != null){
                key +=CCL2+'_';
            }
            else{key +='_';}
            if(MS  != null){
                key +=MS;
            }
            
            return key;
    }

    
    public static void CheckOwnerIsChannelPartner(List<Account> lstAccount,MAP<Id,Account> accmap){
        
        List<Id> lstOwner = new List<Id>{};
        List<Account> lstAccountWithError = new List<Account>();
       // Map<Id,String> mapOwnerToUserType = new Map<Id,String>{};
       Map<Id,Id> mapOwnerToProfileId=new Map<Id,Id>{};
       
        for(Account account : lstAccount){
        
            lstOwner.add(account.OwnerId);
            
        }
        
        
        for(User user : [SELECT Id, ProfileId FROM User WHERE Id IN :lstOwner]){
            mapOwnerToProfileId.put(user.Id,user.ProfileId);
        }
        
        for(Account account : lstAccount)
        {
            
            System.Debug('Profile ID: ' + mapOwnerToProfileId.get(account.OwnerId));
            
            if(mapOwnerToProfileId.get(account.OwnerId) == System.Label.CLSEP12PRM18) {
                System.Debug('User Type: ' + mapOwnerToProfileId.get(account.OwnerId) + ' cannot become account owner');    
                account.addError(System.Label.CLSEP12PRM19);
            }
        }
    }
    
    //BR-2083 This method changes the address of the all related contacts
    //BR-4924 Method modified in October14 Release to add functionality : When the account Geolocation field is changed, update the Geolocation field of all its related contacts 
    
    public static void updateContactAddress(set<id> AccIds)
    {   
        // Limit in a Label TODO
        List<Contact> lstContacts = new List<Contact>();
        lstContacts = [select name, UpdateAccountAddress__c, Accountid from Contact where AccountID in :AccIds and UpdateAccountAddress__c=true and ToBeDeleted__c=false limit :Integer.valueOf(System.Label.CLDEC12AC01)];
        
        Map<id, Account> mapAcc = new map<id,Account>([select name, StateProvince__c,ZipCode__c,LocalCounty__c,POBoxZip__c, POBox__c, Country__c, County__c, LocalCity__c, City__c,LocalAdditionalAddress__c, Street__c,StreetLocalLang__c,AdditionalAddress__c,AutomaticGeolocation__latitude__s,AutomaticGeolocation__longitude__s   from Account where id in :AccIds]);
        
        for(Contact con : lstContacts)
        {
            Account acc = mapAcc.get(con.AccountId);
            if(acc != null)
            {
                con.Street__c = acc.Street__c;
                con.StreetLocalLang__c = acc.StreetLocalLang__c;
                con.AdditionalAddress__c = acc.AdditionalAddress__c;
                con.LocalAdditionalAddress__c = acc.LocalAdditionalAddress__c;
                con.City__c = acc.City__c;
                con.LocalCity__c = acc.LocalCity__c;
                con.ZipCode__c = acc.ZipCode__c;
                con.StateProv__c = acc.StateProvince__c;
                con.Country__c = acc.Country__c;
                con.County__c = acc.County__c;
                con.LocalCounty__c = acc.LocalCounty__c;
                con.POBox__c = acc.POBox__c;
                con.POBoxZip__c = acc.POBoxZip__c;
                con.Geolocation__latitude__s=acc.AutomaticGeolocation__latitude__s;
                con.Geolocation__longitude__s=acc.AutomaticGeolocation__longitude__s; 
            }
        }
        
        update lstContacts;
    }
    //end of BR-2083

    //NZE April 2nd, 2014
/*    public static void updateLocationAddress(set<id> AccIds)
    {   
        List<SVMXC__Site__c> lstLocations = new List<SVMXC__Site__c>();
        lstLocations = [select id, name, SVMXC__Account__c from SVMXC__Site__c where SVMXC__Account__c in :AccIds limit :Integer.valueOf(System.Label.CLDEC12AC01)];
        
        Map<id, Account> mapAcc = new map<id,Account>([select name, StateProvince__c,ZipCode__c,LocalCounty__c,POBoxZip__c, POBox__c, Country__c, County__c, LocalCity__c, City__c,LocalAdditionalAddress__c, Street__c,StreetLocalLang__c,AdditionalAddress__c   from Account where id in :AccIds]);
        
        for(SVMXC__Site__c loc : lstLocations)
        {
            Account acc = mapAcc.get(loc.SVMXC__Account__c);
            if(acc != null)
            {
                loc.SVMXC__Street__c = acc.Street__c;
                loc.SVMXC__City__c = acc.City__c;
                loc.StateProvince__c = acc.StateProvince__c;
                loc.SVMXC__Zip__c = acc.ZipCode__c;
                loc.LocationCountry__c = acc.Country__c;
            }
        }
        
        update lstLocations;
    }
*/
    //end NZE April 2nd, 2014
    
    public static void checkTobeDeleted(List<Account> newAccounts)
    {
        //Set<Id> accIds = new Set<Id>();
        Map<Id, Id> accCasesIds = new Map<Id, Id>();
        Map<Id, Id> accLeadsIds= new Map<Id, Id>(); 
        Map<Id, Id> accCustSurIds= new Map<Id, Id>();         
        Integer cases;
        Integer leads;
        Integer custSur;
        
        // EXECUTE BEFORE UPDATE LOGIC
        /* These following lines checks if there are open cases, leads, or opportunities, active Account Assigned Programs. 
               If yes, the account can’t be deleted.    
        */

            //Queries the cases for the corresponding Account
            cases  = [select count() from Case where AccountId in: newAccounts and ((Status!=:Label.CLDEC13CCC04 and Status!=:Label.CL00326) or CreatedDate>:Date.today().addmonths(-6))];
            
            //Queries the Leads for the corresponding Account
            leads = [select count() from Lead where Account__c in: newAccounts];
                        
            //Queries the Customer Experience Feedback records for the corresponding Account
            custSur = [select count() from CustomerSatisfactionSurvey__c where Account__c in: newAccounts];
            
        //Displays an error if there are open cases, leads, or opportunities, active Account Assigned Programs. 
        for(Account a: newAccounts)
        {
                if(((cases>0) || (leads>0) || (custSur>0) || (a.TECH_OpenOpportunityCount__c>0) || (a.TECH_ActivePRMPrograms__c>0) || a.TECH_IsSVMXRecordPresent__c==true) && (a.ReasonForDeletion__c != 'Duplicate'))
                {    
                    a.addError(Label.CLOCT13ACC02);
                }
        }
    }
    
    //October15 Release - DBA Account
    //Method is used to create a contact for a DBA Account to enfore mandate "at least one contact should exist for every DBA Account"
    public static void createContactforDBA(List<Id> accIds)
    {
    System.debug('------ Contact Creation for DBA Account Starts ------');
        List<Contact> conToBeCreated = new List<Contact>();
        List<Account> acctLstDBA = [SELECT ConSalutation__c, ConFirstName__c, ConLastName__c, ConEmail__c, ConWorkPhone__c, ConMobile__c FROM Account where Id IN :accIds ];
        for(Account acc: acctLstDBA)
        {
            Contact con = new Contact();
            con.Salutation = acc.ConSalutation__c;
            con.FirstName = acc.ConFirstName__c;
            con.LastName = acc.ConLastName__c;
            con.Email = acc.ConEmail__c;
            con.WorkPhone__c = acc.ConWorkPhone__c;
            con.MobilePhone = acc.ConMobile__c;
            con.AccountID = acc.Id;
            con.UpdateAccountAddress__c = True;
            
            //Add to the list of contacts to be created
            conToBeCreated.add(con);
            
        }
        try
        {
            if(!conToBeCreated.isEmpty())
            {
                Database.DMLOptions objDMLOptions = new Database.DMLOptions();
                objDMLOptions.DuplicateRuleHeader.AllowSave = true;
                Database.SaveResult[] conInsertLst = Database.insert(conToBeCreated, objDMLOptions);
                System.debug('Contacts created : '+conToBeCreated);
    
                for(Database.SaveResult sr: conInsertLst)
                {
                    if(!sr.isSuccess()) {
                        Database.Error err = sr.getErrors()[0];                        
                        System.Debug('######## Error Inserting Contact : '+err.getStatusCode()+' '+err.getMessage());
                    }                    
                }
            }
        }
        catch(Exception e)
        {
            system.debug('#### Exception while creating Contact : ' + e);
        }
        System.debug('------ Contact Creation for DBA Account Ends ------');
    
    }
    
    public static void updateCustomAddrFields(List<Account> updateAcctCountryOrState, Set<String> countryCodeLst, Set<String> stateCodeLst)
    {
        Map<String,Country__c> countryMap = new Map<String,Country__c>();
        Map<String,StateProvince__c> stateMap = new Map<String,StateProvince__c>();
        system.debug('--countryCodeLst--'+ countryCodeLst );
        system.debug('--stateCodeLst--'+ stateCodeLst );
        
        for(Country__c ct : [SELECT ID, CountryCode__c, Name FROM Country__c WHERE CountryCode__c IN :countryCodeLst])
        {
            countryMap.put(ct.CountryCode__c,ct);
        }
        
        for(StateProvince__c st: [SELECT ID, Name, StateProvinceCode__c, CountryCode__c FROM StateProvince__c WHERE CountryCode__c IN :countryCodeLst AND StateProvinceCode__c IN :stateCodeLst])
        {
            String ctyStateKey = st.CountryCode__c + st.StateProvinceCode__c;
            stateMap.put(ctyStateKey,st);
        }
        
        for(Account acc:updateAcctCountryOrState)
        {
            if(String.isNotBlank(acc.BillingCountryCode) && countryMap.size()>0 )
                acc.Country__c = countryMap.get(acc.BillingCountryCode).Id;
            else
                acc.Country__c = null;
            if(String.isNotBlank(acc.BillingStateCode) && stateMap.size()>0)    
                acc.StateProvince__c = stateMap.get(acc.BillingCountryCode+acc.BillingStateCode).Id;
            else
                acc.StateProvince__c = null;           
        }
        
    }
    //BR-10632 November Release: The below method will update the 'Synchronisation with Marketo' field on Accounts
    public static void updateSynchwithmarketo(Set<Id> accId) {
        List<Account> accList = [Select Id,Synchronize_With_Marketo__c from Account where ID In: accId];
        List<Account> updateAccList = new List<Account>();
        for(Account acc:accList) {
            if(acc.Synchronize_With_Marketo__c != 1) {
                acc.Synchronize_With_Marketo__c = 1;
                updateAccList.add(acc);
            }
        }
        if(updateAccList.size()>0 && updateAccList.size() != null) {
            update updateAccList;
        }
    }
}