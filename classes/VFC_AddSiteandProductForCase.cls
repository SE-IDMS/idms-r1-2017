global with sharing class VFC_AddSiteandProductForCase{ 

    Public String SiteSearch{get;set;}
    Public list<GCSSite__c> sitelist{get;set;}
    Public Boolean SiteRendered{get;set;}
    Public string idselected{get;set;}
    Public String selectedRadio{get;set;}
    public list<SiteRadioWrapper> SiteWrapper{get;set;}
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    Public Boolean RecordCountText{get;set;}
    Public list<Product2> lstProd2{get;set;}
    
    
    public VFC_AddSiteandProductForCase(ApexPages.StandardController controller){  
     sitelist = new list<GCSSite__c>();
     SiteRendered = true;
     list<GCSSite__c> sitelistNolimit  = new list<GCSSite__c>();
     string str = '%'+SiteSearch+'%';
     if(!Test.IsRunningTest()){
       User UserObj =[select id,accountId from User where Id=:UserInfo.getUserId()] ;
       sitelistNolimit = [select id,name,active__c,LegacySiteId__c from GCSSite__c where Account__c=:UserObj.AccountId];
       sitelist = [select id,name,active__c,LegacySiteId__c from GCSSite__c where Account__c=:UserObj.AccountId and active__c=True limit 10];
       }
       else{
          sitelistNolimit = [select id,name,active__c,LegacySiteId__c from GCSSite__c];
          sitelist = [select id,name,active__c,LegacySiteId__c from GCSSite__c];
       }
       if(sitelistNolimit.size()>10){
          RecordCountText = True;
        }
       //sitelist = [select id,name,active__c,LegacySiteId__c from GCSSite__c where Account__c=:UserObj.AccountId and active__c=True limit 10];
       system.debug('sitelist'+sitelist);
       SiteWrapper = new list<SiteRadioWrapper>();
       for(GCSSite__c siteitr :sitelist){
       
         SiteWrapper.add(new SiteRadioWrapper(siteitr));
       }
           
    }
    //public VFC_AddSiteandProductForCase(ApexPages.StandardSetController controller){
      
    //}
    public String scriteria{get;set;}{    
            MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
                if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                    scriteria=pageParameters.get('commercialReference');
    }
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
            
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
            }
        }
        if(!Test.isRunningTest()){    
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        String query;
        system.debug('&&& business'+ business);
        if(business=='02'){
          query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c=:business';
          system.debug('query'+query);
        }
         else if(business=='02DK'){
          query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c=:business';
           system.debug('query'+query);
        }
        else if(business=='02DK__'){
          query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'\' ORDER BY Name ASC';
           system.debug('query'+query);
        }
       else{
          query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
           system.debug('query'+query);
       }     
        system.debug('*** @Remote - query'+ query);
        return Database.query(query);       
    }
    Public pagereference performCheckbox(){ 
        
        system.debug('*** checkbox - enter');
        String ProductName;
        String ProductId;
        String CommercialRef;
        
        if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');   
        system.debug('***jsonSelectString'+jsonSelectString);
        List<WS_GMRSearch.gmrDataBean> DBL= (List<WS_GMRSearch.gmrDataBean>) JSON.deserialize(jsonSelectString, List<WS_GMRSearch.gmrDataBean>.class);
        System.debug('DBL:'+DBL);
            
            User userObj= [Select id,ContactId,AccountId from User where id=:Userinfo.getUserId() limit 1];
            String contactName=userObj.ContactId;
            String accountName=userObj.AccountId;
            System.debug('accountName'+accountName);
            CommercialRef=DBL[0].CommercialReference;
            lstProd2 =[select id,name from Product2 where name=:CommercialRef];
            ProductId= lstProd2[0].Id; 
            ProductName= lstProd2[0].Name;  
              PageReference PageRef;
               if(ProductName!=null&&selectedRadio!=null){
                   PageRef= new PageReference('/VFP_PortletNewCasePage?CommercialReference='+CommercialRef+'&Contact='+contactName+'&account='+accountName+'&Site='+selectedRadio+'&ProductName='+ProductId); 
                   PageRef.setRedirect(true);
                  }
               else{
                   PageRef= new PageReference('/VFP_PortletNewCasePage?CommercialReference='+CommercialRef+'&Contact='+contactName+'&account='+accountName+'&ProductName='+ProductId);
                   PageRef.setRedirect(true);
               }
               return PageRef;
               
        
    } 
    Public Pagereference performCheckbox1(){
    system.debug('my method called');
    User userObj= [Select id,ContactId,AccountId from User where id=:Userinfo.getUserId() limit 1];
            String contactName=userObj.ContactId;
            String accountName=userObj.AccountId;
    PageReference pageResult = new PageReference('/VFP_PortletNewCasePage?Contact='+contactName+'&account='+accountName); 
                pageResult.setRedirect(true);
                return pageResult;
    }     
    
     public PageReference pageCancelFunction(){
        PageReference pageResult;
        pageResult = new PageReference('/apex/VFP_PortletCasesView');
        return pageResult;
     }
    Public Void SearchSites(){
     sitelist = new list<GCSSite__c>();
     RecordCountText =False;
     SiteRendered = true;
     string str = '%'+SiteSearch+'%';
     if(!Test.IsRunningTest()){
       User UserObj =[select id,accountId from User where Id=:UserInfo.getUserId()] ;
       sitelist = [select id,name,active__c,LegacySiteId__c from GCSSite__c where Name LIKE:str and Account__c=:UserObj.AccountId];
       }
       else{
       
       sitelist = [select id,name,active__c,LegacySiteId__c from GCSSite__c where Name LIKE:str];
        
       }
       
       system.debug('sitelist'+sitelist);
       SiteWrapper = new list<SiteRadioWrapper>();
       for(GCSSite__c siteitr :sitelist){
       
         SiteWrapper.add(new SiteRadioWrapper(siteitr));
       }
      
    }
    public class SiteRadioWrapper{
     public GCSSite__c siteobj{get;set;}
     public boolean radioselect{get;set;}
     
     public SiteRadioWrapper(GCSSite__c site){
       siteobj = site;
       radioselect = false;
     }
    }
    
    Public Void ActionFucntion1(){
      system.debug('param avalue'+idselected);
      selectedRadio = [select id,name from GCSSite__c where id=:idselected].Id;
      
    }
    
}