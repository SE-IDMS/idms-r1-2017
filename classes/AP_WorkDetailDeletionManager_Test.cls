@isTest (SeeAllData=true)
        public class AP_WorkDetailDeletionManager_Test {
         //version changed to 36 , suhail
            private static UserRole mgrUserRole;
            private static UserRole fsrUSerRole;
            private static User currentUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            private static User bFSManager;
            private static User bFSUser;
            private static Country__c countryFR;
            private static Account accountA;
            private static SVMXC__Site__c locationA;
            private static SVMXC__Service_Group__c serviceTeam;
            private static SVMXC__Service_Group_Members__c fsrTech;
            
            static {
                prepareCommonData();
            }
            
            private static void prepareCommonData() {
                
                SVMXC__Service_Order_Line__c wd;
                
                System.runAs (currentUser) {
                    mgrUSerRole = [select id, name from UserRole where name = 'INTNL_SERVICES'];
                    System.debug('mgrUSerRole: '+mgrUSerRole);
                    fsrUSerRole = [select id, name from UserRole where name = 'INTNL_FSR'];
                    System.debug('fsrUSerRole: '+fsrUSerRole);
                    
                    bFSManager = Utils_TestMethods.createStandardUser('bFSMgr');
                    bFSManager.ManagerId = UserInfo.getUserId();
                    bFSManager.UserRoleId = mgrUSerRole.Id;
                    insert bFSManager;
                    
                    bFSUser = Utils_TestMethods.createStandardUser('bFSUser');
                    bFSUser.ManagerId = bFSManager.Id;
                    bFSUser.UserRoleId = fsrUSerRole.Id;
                    bFSUser.BypassWF__c = true;
                    insert bFSUser;        
                    
                    List<Country__c> countryList = new List<Country__c>();
                    countryFR = new Country__c(Name='France', CountryCode__c='FR');
                    upsert countryList;
                    
                    accountA = new Account();
                    AccountA.recordTypeID = '012A0000000neLb';
                    accountA.Name='TestAccountA'; 
                    accountA.Country__c=CountryFR.id;
                    accountA.Street__c='New StreetA';
                    accountA.POBox__c='XXXA';
                    accountA.ZipCode__c = '12345';
                    accountA.City__c = 'City A';
                    accountA.BillingCity = 'City A';
                    accountA.BillingStreet = 'New StreetA';
                    accountA.BillingCountry = countryFR.CountryCode__c;
                    
                    insert AccountA;

                    locationA = new SVMXC__Site__c(
                        Name = accountA.Name + ' - SITE',
                        SVMXC__Location_Type__c = 'SITE',
                        PrimaryLocation__c = True,
                        SVMXC__Account__c = accountA.Id
                    );
                    insert locationA;
                    
                    serviceTeam = Utils_TestMethods.createServiceGrp();
                    insert serviceTeam;
                    
                    Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__Service_Group_Members__c;
                    Map <String, Schema.RecordTypeInfo> serviceGrpMbrRecordTypeInfo = dSobjres.getRecordTypeInfosByName();
                    Id fsrRecordTypeID = serviceGrpMbrRecordTypeInfo.get('FSR').getRecordTypeId();
            
                    fsrTech = Utils_TestMethods.createTech_Equip(UserInfo.getUserId(), serviceTeam.Id);
                    fsrTech.recordTypeId = fsrRecordTypeID;
                    insert fsrTech;
                    
                }
            }
            
            public static testMethod void myUnitTest() {
                
                SVMXC__Service_Order_Line__c wd;
                List<SVMXC__Service_Order_Line__c> wdList = new List<SVMXC__Service_Order_Line__c>();
                
                System.runAs (currentUser) {
                    List<SVMXC__Service_Order__c> workOrderList = new List<SVMXC__Service_Order__c>();            
                    SVMXC__Service_Order__c workOrder = createWorkOrder(countryFR, accountA, locationA, fsrTech, 1);
                    workOrderList.add(workOrder);
                    insert workOrderList;
                    System.debug('##### workOrderList: '+workOrderList);
                    
                    wd = createWorkDetail(workOrder, fsrTech);
                    wdList.add(wd);
                }
                
                test.startTest();
                System.runAs(bFSUser) {
                    insert wdList;
                    delete wdList;
                }
                test.stopTest();

                
            }
            /*
            public static testMethod void myBulkTest() {
                
                SVMXC__Service_Order_Line__c wd;
                List<SVMXC__Service_Order_Line__c> wdList = new List<SVMXC__Service_Order_Line__c>();
                
                System.runAs (currentUser) {
                    List<SVMXC__Service_Order__c> workOrderList = new List<SVMXC__Service_Order__c>();            
                    SVMXC__Service_Order__c workOrder = createWorkOrder(countryFR, accountA, locationA, fsrTech, 1);
                    workOrderList.add(workOrder);
                    insert workOrderList;
                    System.debug('##### workOrderList: '+workOrderList);
                    
                    for (Integer i=0;i<200;i++) {
                        wd = createWorkDetail(workOrder, fsrTech);
                        wdList.add(wd);
                    }
                }
                
                test.startTest();
                System.runAs(bFSUser) {
                    //insert wdList;
                    //delete wdList;
                }
                test.stopTest();

                
            }*/
            
            private static SVMXC__Service_Order__c createWorkOrder(Country__c aCountry, Account anAccount, SVMXC__Site__c aLocation, SVMXC__Service_Group_Members__c anFSR, Integer i) {
                SVMXC__Service_Order__c result;
                result = new SVMXC__Service_Order__c(
                    SVMXC__Company__c = anAccount.Id, 
                    SVMXC__Site__c = aLocation.Id,
                    SVMXC__Order_Status__c = 'New',
                    BackOfficeReference__c = 'BO ref '+i,
                    BackOfficeSystem__c = 'BO system '+i,
                    CountryOfBackOffice__c = aCountry.CountryCode__c,
                    SVMXC__Group_Member__c = anFSR.Id,
                    CustomerTimeZone__c = 'Europe/Paris',
                    CustomerRequestedDate__c = Date.today(),
                    Service_Business_Unit__c = 'IT',
                    SVMXC__Priority__c = 'Normal-Medium',
                    WOPriority__c = 'Normal-Medium',
                    Work_Order_Category__c = 'Onsite',
                    SVMXC__Order_Type__c = 'Repair',
                    IsBillable__c = 'Yes',
                    SVMXC__Problem_Description__c = 'Test WD Deletion '+i,
                    SVMXC__Completed_Date_Time__c = Datetime.now(),
                    CompliedWithSafetyRequirements__c = 'Yes',
                    SVMXC__Work_Performed__c = 'Did some repair',
                    CustomerApproval__c = 'Yes',
                    Technical_Approval__c = 'Yes',
                    OpportunityDetected__c = 'No',
                    SVMXC__Closed_By__c = UserInfo.getUserId(),
                    SVMXC__Closed_On__c = DateTime.now()
                );
                
                return result;
                    
            }
            
            private static SVMXC__Service_Order_Line__c createWorkDetail(SVMXC__Service_Order__c aWorkOrder, SVMXC__Service_Group_Members__c anFSR) {
                SVMXC__Service_Order_Line__c result;
                result = new SVMXC__Service_Order_Line__c(
                    RecordtypeId = Label.CLSSRV_WDD_RecordType,
                    SVMXC__Service_Order__c = aWorkOrder.Id,
                    SVMXC__Activity_Type__c = 'Service',
                    SVMXC__Line_Type__c = 'Labor',
                    HourType__c = '1',
                    IsBillable__c = 'Yes',
                    SVMXC__Group_Member__c = anFSR.Id,
                    SVMXC__Start_Date_and_Time__c = system.now().addHours(-5),
                    SVMXC__End_Date_and_Time__c = system.now()
                );

                return result;
            }
            
        }