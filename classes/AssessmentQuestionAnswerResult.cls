global class AssessmentQuestionAnswerResult
{
	// WS error codes & error message
    WebService Integer returnCode;
    WebService String message;
    WebService Integer totalNoOfAssessmentQuestionAnswers; 
    WebService AssessmentQuestionAnswer[] assessmentQuestionAnswers { get; set; }
}