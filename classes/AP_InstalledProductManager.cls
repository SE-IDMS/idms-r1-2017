/*
created as part of connectors for November release

*/
global class AP_InstalledProductManager{

    global static string STANDARD_LOCATION = Label.CLNOV13SRV01;
    // global static string SE_READONLYSITE = Label.CLNOV13SRV02;
    global static string SITESUFFIX = Label.CLFEB14SRV01;  
     global static string SPACESUFFIX = ' ';   

    global static void ProcessAssets(List<Sobject> scope)
    {              
        Map<Id,List<SVMXC__Installed_Product__c>> accountInstalledProductmap=new Map<Id,List<SVMXC__Installed_Product__c>>();
        Map<Id,SVMXC__Site__c> accountPrimaryLocationmap=new Map<Id,SVMXC__Site__c>();
        Map<Id,Account> accountInformation=new Map<Id,Account>();
        Map<Id,SVMXC__Site__c> newPrimaryLocations=new Map<Id,SVMXC__Site__c>();
        string INFORTECHNOBUSINESS =System.label.CLDEC15SRV05;
          string IT =System.label.CLDEC15SRV04;

        Map<String,List<SVMXC__Installed_Product__c>> mapInstalledProductsWithMissingProductInfo=new Map<String,List<SVMXC__Installed_Product__c>>();       

        List<Role__c> lstRolesTobeInserted=new List<Role__c>();            

        for(Sobject sobjectRecord:scope)
        {
            SVMXC__Installed_Product__c installedProductRecord=(SVMXC__Installed_Product__c)sobjectRecord;            
            if(installedProductRecord.SVMXC__Company__c!=null)
            {            
                if(accountInstalledProductmap.containsKey(installedProductRecord.SVMXC__Company__c))
                {
                    accountInstalledProductmap.get(installedProductRecord.SVMXC__Company__c).add(installedProductRecord);                      
                }
                else
                {
                    accountInstalledProductmap.put(installedProductRecord.SVMXC__Company__c,new List<SVMXC__Installed_Product__c>{installedProductRecord});  
                } 
                installedProductRecord.TECH_CreateFromWS__c=false;
            }
            //BR-7723
            if(installedProductRecord.SVMXC__Company__c==null)
            { 
                if(installedProductRecord.SVMXC__Site__c !=null)
                {
                    installedProductRecord.SVMXC__Site__c=null;
                }
                installedProductRecord.TECH_CreateFromWS__c=false;
            }
            //BR-7723 End  
            //BR-8097 Serial number decoding.Added by Anand
            if(installedProductRecord.SVMXC__Serial_Lot_Number__c!=null && installedProductRecord.ITBInstalled_Product__c == INFORTECHNOBUSINESS)
            { 
               if(installedProductRecord.AutoCalcManufacturingDate__c !=null)
               {
                    installedProductRecord.ManufacturingUnitDate__c = installedProductRecord.AutoCalcManufacturingDate__c;   
                    //installedProductRecord.TECH_CreateFromWS__c=false;
               }               

            }
            //End:BR-8097 Serial number decoding.Added by Anand
            if(installedProductRecord.SVMXC__Product__c==null && installedProductRecord.Brand2__c!=null && installedProductRecord.DeviceType2__c!=null && installedProductRecord.Category__c!=null)
            {
               // String concatenatedString=installedProductRecord.TECH_SDHCategoryId__c+'_'+installedProductRecord.TECH_SDHDEVICETYPEID__c+'_'+installedProductRecord.TECH_SDHBRANDID__c;
                String concatenatedString=installedProductRecord.TECH_SDHCategoryId__c+'_'+installedProductRecord.TECH_SDHBRANDID__c+'_'+installedProductRecord.TECH_SDHDEVICETYPEID__c;
                if(mapInstalledProductsWithMissingProductInfo.containsKey(concatenatedString))
                {
                    mapInstalledProductsWithMissingProductInfo.get(concatenatedString).add(installedProductRecord);
                }
                else
                {      
                    mapInstalledProductsWithMissingProductInfo.put(concatenatedString,new List<SVMXC__Installed_Product__c>{installedProductRecord});
                }         

            } 
            //Commented for BR-8304
            /*
            if(installedProductRecord.TECH_CreatedfromSFM__c == false){
                lstRolesTobeInserted.add(new Role__c(InstalledProduct__c = installedProductRecord.id, Account__c = installedProductRecord.SVMXC__Company__c, RecordTypeId = System.Label.CLNOV13SRV11 , Role__c = System.Label.CLOCT13SRV51 ));
            }*/
			installedProductRecord.TECH_CreateFromWS__c=false;

        }

        //update product info
        for(Product2 productrecord:[select id,ExtProductId__c from Product2 where ExtProductId__c in :mapInstalledProductsWithMissingProductInfo.keySet()])
        {
            for(SVMXC__Installed_Product__c installedProductRecord:mapInstalledProductsWithMissingProductInfo.get(productrecord.ExtProductId__c))
            {
                installedProductRecord.SVMXC__Product__c=productrecord.id;
            }       
        }
        //Commented for BR-8304
        /*
        //create new roles
        if(lstRolesTobeInserted.size()>0){
            for(Database.SaveResult sr:Database.insert(lstRolesTobeInserted,false))
            {
                if(!sr.isSuccess())
                    System.debug('*****************\nERROR\n Id:'+sr.getId()+'\nMessage :'+sr.getErrors()[0].message+'\n*****************');
                else
                {
                }
            }
        } */       

        System.debug('accountInstalledProductmap'+accountInstalledProductmap);
        if(accountInstalledProductmap.size()>0)
        {
            for(SVMXC__Site__c siteRecord:[select id,SVMXC__Account__c,PrimaryLocation__c from SVMXC__Site__c where  SVMXC__Account__c in :accountInstalledProductmap.keySet() and PrimaryLocation__c=true])
            { 
                accountPrimaryLocationmap.put(siteRecord.SVMXC__Account__c,siteRecord);                                       
            }

            accountInformation=new Map<Id,Account>([Select id, Name,Street__c,StateProvince__c,ZipCode__c,Country__c,City__c from Account where id in :accountInstalledProductmap.keySet()]);        

            for(Id accountId:accountInstalledProductmap.keySet())
            {            
                if(accountPrimaryLocationmap.containsKey(accountId))
                {
                //primary
                    System.debug('inside the primary search and populate part');                            
                    for(SVMXC__Installed_Product__c installedProductRecord:accountInstalledProductmap.get(accountId))
                    {
                        if(installedProductRecord.SVMXC__Site__c==null)
                            installedProductRecord.SVMXC__Site__c=accountPrimaryLocationmap.get(accountId).id;                
                    }                                                        
                }
                else
                {
                //we go for creation                    
                    // for(SVMXC__Installed_Product__c installedProductRecord:accountInstalledProductmap.get(accountId))
                    // {
                        if(accountInformation.containsKey(accountId))
                        {
                            Account accountRecord=accountInformation.get(accountId);
                            string accnametmp = accountRecord.Name; 
                            if (accnametmp.length() > 75){
                                accnametmp= accnametmp.substring(0, 75);
                            }
                            SVMXC__Site__c newsiterecord=new SVMXC__Site__c(Name=accnametmp+SPACESUFFIX+SITESUFFIX,RecordTypeId=STANDARD_LOCATION,SVMXC__Location_Type__c='Site',PrimaryLocation__c=true,StateProvince__c=accountRecord.StateProvince__c,SVMXC__Zip__c=accountRecord.ZipCode__c,SVMXC__Street__c=accountRecord.Street__c,SVMXC__City__c=accountRecord.City__c,LocationCountry__c=accountRecord.Country__c,SVMXC__Account__c=accountRecord.id);
                            newPrimaryLocations.put(accountId,newsiterecord);                    

                        }

                    // }                    
                }

            }
        }

        

        //insert the new primary records 
        if(newPrimaryLocations.values().size()>0)       
        {
            
            
            for(Database.SaveResult sr:Database.insert(newPrimaryLocations.values(),false))
            {
                if(!sr.isSuccess())
                    System.debug('*****************\nERROR\n Id:'+sr.getId()+'\nMessage :'+sr.getErrors()[0].message+'\n*****************');
                else
                {
                }
            }
            System.debug('newprimaryLocations.values()'+newPrimaryLocations.values());
            for(Id accountId:newPrimaryLocations.keySet())
            {
                if(accountInstalledProductmap.containsKey(accountId))
                {
                    for(SVMXC__Installed_Product__c installedProductRecord:accountInstalledProductmap.get(accountId))
                    {
                        if(installedProductRecord.SVMXC__Site__c==null)
                        installedProductRecord.SVMXC__Site__c=newPrimaryLocations.get(accountId).id;    
                    }                                        
                }            
            }
        }
    }         

    // Check GoldenAssetId duplicates
@testvisible Private static void checkAssetGoldenId(List<SVMXC__Installed_Product__c> newAssets)
{
 Map<String,SVMXC__Installed_Product__c> mapGoldenAssetIdtoIP=new Map<String,SVMXC__Installed_Product__c>();
 Integer remainingRecordsforLimit=Limits.getLimitQueryRows()-Limits.getQueryRows();

 for(SVMXC__Installed_Product__c AssetRecord:newAssets)
 {    
   if(AssetRecord.GoldenAssetId__c != null && AssetRecord.GoldenAssetId__c.trim() != ''){
       system.debug('### RHA enter map assets');
       mapGoldenAssetIdtoIP.put(AssetRecord.GoldenAssetId__c,AssetRecord);
   }

}    

for(SVMXC__Installed_Product__c existingAssetrecord :[select id,name,GoldenAssetId__c from SVMXC__Installed_Product__c where GoldenAssetId__c IN :mapGoldenAssetIdtoIP.keySet() limit :remainingRecordsforLimit])
{           
    if(mapGoldenAssetIdtoIP.containsKey(existingAssetrecord.GoldenAssetId__c))
        mapGoldenAssetIdtoIP.get(existingAssetrecord.GoldenAssetId__c).addError(Label.CLNOV13SRV13);            
}      
}

    //Trigger related function to checkAssetGoldenId
public static void beforeHandler(List<SVMXC__Installed_Product__c> newAssets, Map<Id,SVMXC__Installed_Product__c> newAssetsMap)
{       
       // if(UserInfo.getProfileId()==Id.valueOf(System.LABEL.CLNOV13SRV03))
        //{    
            // check the golden Location Id duplicates in case of SDH upload       
    AP_InstalledProductManager.checkAssetGoldenId(newAssets);
            // Rule for routing cordys user to route to trigger
    if(System.LABEL.CLNOV13SRV10=='FOR_SDH_REGULAR_UPDATES'){
       // AP_InstalledProductManager.ProcessAssets(newAssets);
    }
       // }       
}
 public static void TestCoverageMehod()
 {
     String st1 ;
    String st2  ;
    String st3  ;
    String st4  ;
    String st5  ;
    String st6  ;
    String st7  ;
    String st8  ;
    String st9  ;
    String st10 ;
    String st11 ;
    String st12 ;
    String st13 ;
    String st14 ;
    String st15 ;
    String st16 ;
    String st17 ;
    String st18 ;
    String st19 ;
    String st20 ;
    String st21 ;
    String st22 ;
    String st23 ;
    String st24 ;
    String st25 ;
    String st26 ;
    String st27 ;
    String st28 ;

     
 }
}