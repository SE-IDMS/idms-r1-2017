@isTest
private class AP_TeamProductMappingMethods_TEST {
    public static testMethod void VerifyDefaultProductFamilyTestMethod(){
        System.debug('#### AP_TeamProductMappingMethods_Test Started ####');
        
        Country__c country = Utils_TestMethods.createCountry();
        country.Name = 'India';
        insert country;
        
        CustomerCareTeam__c AgentAPrimaryTeam = new CustomerCareTeam__c(CCCountry__c=country.ID, Name='Agent A Primary Team',LevelOfSupport__c='Primary');
        Insert AgentAPrimaryTeam;
        
        List<OPP_Product__c> lstProduct = new List<OPP_Product__c>();
        
        OPP_Product__c objProduct1 = new OPP_Product__c();
        objProduct1.Name = 'TEST BU';
        objProduct1.BusinessUnit__c = 'TEST BU';
        objProduct1.ProductLine__c='TEST_PL';
        objProduct1.ProductFamily__c='TEST_PF';
        objProduct1.Family__c='TEST_FLMY';
        objProduct1.TECH_PM0CodeInGMR__c = '11223344';
        objProduct1.HierarchyType__c = 'PM0-FAMILY';
        objProduct1.IsActive__c = true;
        lstProduct.add(objProduct1);
        
        OPP_Product__c objProduct2 = new OPP_Product__c();
        objProduct2.Name = 'TEST BU2';
        objProduct2.BusinessUnit__c = 'TEST BU2';
        objProduct2.ProductLine__c='TEST_PL2';
        objProduct2.ProductFamily__c='TEST_PF2';
        objProduct2.Family__c='TEST_FLMY2';
        objProduct2.TECH_PM0CodeInGMR__c = '11223355';
        objProduct2.HierarchyType__c = 'PM0-FAMILY';
        objProduct2.IsActive__c = true;
        lstProduct.add(objProduct2);
        
        Database.insert(lstProduct);
        
        
       // List<TeamProductJunction__c> TeamList = new List<TeamProductJunction__c>();
        TeamProductJunction__c TeamProductFamily = new TeamProductJunction__c();
        TeamProductFamily.CCTeam__c = AgentAPrimaryTeam.id;
        TeamProductFamily.DefaultSupportedProductFamily__c = true;
        TeamProductFamily.ProductFamily__c = objProduct1.id;
        Insert TeamProductFamily;
       // TeamList.Add(TeamProductFamily);
        
        
        
        TeamProductJunction__c TeamProductFamily1 = new TeamProductJunction__c();
        TeamProductFamily1.CCTeam__c = AgentAPrimaryTeam.id;
        TeamProductFamily1.DefaultSupportedProductFamily__c = true;
        TeamProductFamily1.ProductFamily__c = objProduct2.id;
        Insert TeamProductFamily1;
        //TeamList.add(TeamProductFamily1);
        
       // Insert TeamList;
        
    }
}