Public class VFCI2PAPR12001_UpdateSymptomToProblem
{

    // Controller Name  : VFC_I2PUpdateSymptomToComplaintReqst
    // Purpose          : Controller class for Problem Symptom
    // Remarks          : For April 2014 -  Release
    ///********************************************************************************/

 /*============================================================================
        Variable 
    =============================================================================*/ 
    public list<CommercialReference__c> lstCommercialReference;
    public Map<string,CommercialReference__c> mpcrclREF;
    public ComplaintRequest__c objComplntRequestSymptom;
    public Problem__c objProblemUpdSymptom;
    public Set<string> setCompSymptom;
    public Set<string> setCompSymptom1;
    public Set<string> setProbSymptom;
   /*============================================================================
        Getters
    =============================================================================*/ 
    public List<WrapperGMR> lstwrpGMR{get;set;}
    public set<string> stGMRCode{get;set;}
    //public string strAction='';
    public string strAction{get; set;}
    public Id varId;
    public Id varCRId;
    public Id varPRBId;
    public boolean isDisable{get;set;}
    public boolean isDisable2{get;set;}
    public boolean isNoGMRDATA{get;set;}
    public Boolean allChecked { get; set; }
    Public Boolean iscancel{get;set;}
    Public Boolean isCR{get;set;}
    Public Boolean isPRB{get;set;}
    Public static string  strseltdParentSynt{get;set;}
    
    public Map<string, List<WrapperGMR> > mapwrpGMR{get;set;}
    public List<SelectOption> lstSelectChildSymp {get;set;}
    public Map<string, List<string> > mapSymChild{get;set;}
    public Map<string, RelatedSymptom__c > mapRS {get; set;}

   /*============================================================================
        Constructor
    =============================================================================*/ 
    Public  VFCI2PAPR12001_UpdateSymptomToProblem()
    {
         stGMRCode=new set<string>();
        //stCommercialReferenceGMRCode=new set<string>();
        setCompSymptom=new set<string>();
        setCompSymptom1=new set<string>();
        setProbSymptom=new set<string>();
        mpcrclREF=new map<string,CommercialReference__c>();
        mapRS = new map<string, RelatedSymptom__c>();
        isNoGMRDATA=true;
        isDisable=false;
        iscancel=false;
        isPRB=false;
        isCR=false;
        strAction=ApexPages.currentPage().getParameters().get(Label.CLI2PAPR120005);
        list<CommercialReference__c> temp=new list<CommercialReference__c>();
        if(ApexPages.currentPage().getParameters().get('crid')!='')
        {
        varId=ApexPages.currentPage().getParameters().get('crid'); //assigning ComplaintRequest Id
        }
        else if(ApexPages.currentPage().getParameters().get('pid')!='')
        {
        varId=ApexPages.currentPage().getParameters().get('pid'); 
        }
       // Utils_SDF_Methodology.log('START query: ');  
       // Utils_SDF_Methodology.startTimer();
       
             
       //======== To get Prefix ===================================================================
           Schema.DescribeSObjectResult ProblemSchemaResult = Problem__c.sObjectType.getDescribe();
           Schema.DescribeSObjectResult ComplaintRequestSchemaResult =  ComplaintRequest__c.sObjectType.getDescribe();       
       //===========================================================================================
       
       
       // This is in the case of Complaint Request object
       If(varId!= null && String.ValueOf(varId).length()>0)
       {
       If(string.ValueOf(varId).startsWith(ComplaintRequestSchemaResult.getKeyPrefix()))
       {
            objComplntRequestSymptom = [select id,TECH_SymptomsAvailable__c,Category__c,TECH_CRReceivedGMRCode__c,TECH_CROrderGMRCode__c,(select id, name,Symptom__c,SubSymptom__c,OtherSymptom__c from Related_Symptoms__r),CommercialReferenceReceived__c,FamilyReceived__c,FamilyOrdered__c,TECH_Symptoms__c from ComplaintRequest__c where Id=:varId];
            if(objComplntRequestSymptom!=null){
            
           // if(objComplntRequestSymptom.Category__c==label.CLAPR14I2P02)
            //{
                isCR=True;
                /*
                If(objComplntRequestSymptom.TECH_Symptoms__c!=null && objComplntRequestSymptom.TECH_Symptoms__c!='')
                {
                setCompSymptom.addall(objComplntRequestSymptom.TECH_Symptoms__c.split(Label.CLI2PAPR120004));
                } 
                */
                
                for( RelatedSymptom__c rs: objComplntRequestSymptom.Related_Symptoms__r )
                {
                        
                        setCompSymptom.add(rs.Symptom__c+' - '+rs.SubSymptom__c);
                        mapRS.put(rs.Symptom__c+' - '+rs.SubSymptom__c, rs);
                        //rs.OtherSymptom__c
                }
                system.debug('### setCompSymptom'+ setCompSymptom);
                //system.debug('###rs setCompSymptom1'+ setCompSymptom1);
                
                if(objComplntRequestSymptom.TECH_CROrderGMRCode__c!=null && objComplntRequestSymptom.FamilyOrdered__c!=null)
                { 
                //mpcrclREF.put(crgmrcode.GMRCode__c.substring(0,Integer.valueof(Label.CLI2PAPR120003)),crgmrcode);
                stGMRCode.add(objComplntRequestSymptom.TECH_CROrderGMRCode__c.substring(0,Integer.valueof(Label.CLI2PAPR120003)));
                system.debug('*** stGMRCode'+stGMRCode);
                } 
                
            //} 
          }   
       }
       else if(String.ValueOf(varId).startsWith(ProblemSchemaResult.getKeyPrefix()))
       {
            isPRB = True;
            objProblemUpdSymptom = [select id,(select id, name,Symptom__c,SubSymptom__c,OtherSymptom__c from Related_Symptoms__r),Symptoms__c,TECH_Symptoms__c from Problem__c where Id=:varId];
            system.debug('### objProblemUpdSymptom '+objProblemUpdSymptom );
            
             for( RelatedSymptom__c rs: objProblemUpdSymptom.Related_Symptoms__r )
                {   
                        setCompSymptom.add(rs.Symptom__c+' - '+rs.SubSymptom__c);
                        mapRS.put(rs.Symptom__c+' - '+rs.SubSymptom__c, rs);
                        //rs.OtherSymptom__c
                }
                system.debug('@@@ setCompSymptom'+ setCompSymptom);
                /*
                If(objProblemUpdSymptom.TECH_Symptoms__c!=null && objProblemUpdSymptom.TECH_Symptoms__c!='')
                {
                    
                    setCompSymptom.addall(objProblemUpdSymptom.TECH_Symptoms__c.split(Label.CLI2PAPR120004));
                    System.debug('@@@@@@@@@@@@@@@'+setCompSymptom);
                } 
                */
                for(CommercialReference__c crgmrcode:[select id ,GMRCode__c,ProductLine__c from CommercialReference__c where Problem__c=:varId])
                { 
                   If(crgmrcode.GMRCode__c !=null && crgmrcode.GMRCode__c != '')
                   {
                       mpcrclREF.put(crgmrcode.GMRCode__c.substring(0,Integer.valueof(Label.CLI2PAPR120003)),crgmrcode);
                       stGMRCode.add(crgmrcode.GMRCode__c.substring(0,Integer.valueof(Label.CLI2PAPR120003)));
                        system.debug('***Problem - stGMRCode'+stGMRCode);
                   }
                }  
       }
       If((isCR==true) || (isPRB==true) ) 
       {
           if(strAction == Label.CLI2PAPR120006)
           {
               isDisable=true;
               isDisable2=false;
               getlistsetSymptomMapping();
           }
           else
           { 
               isDisable2=true;
               isDisable=false;
               showMaping();
           }
      }
   }
  }
   public PageReference showMaping() 
    {
    
      //*********************************************************************************
       
        
        lstwrpGMR=new list<WrapperGMR>(); 
        mapwrpGMR = new map<string ,list<WrapperGMR>>();     
       
        Map<string,List<String> > mapStr = new map<string,List<String>> ();
        List<string> lstSpit =new List<string>();
        
        
        for(string varstr:setCompSymptom)
        {
            List<String> tesstr=varstr.split('-');
            
                for(string strgsym:tesstr) {
            //lstwrpGMR.add(new WrapperGMR ('varstr','',false,null,null,null));
            lstSpit.add(strgsym);
            }
        }
        
        if(lstSpit!=null && lstSpit.size() > 0) {
            system.debug('lstSpit---->'+lstSpit+'tesrr'+lstSpit.size());
            for(integer i=0;i<=lstSpit.size();i++) {
                if(mapStr.containsKey(lstSpit[i+i])) {
                    mapStr.get(lstSpit[i+i]).add(lstSpit[i+i+1]);
                    if(i+i+1==lstSpit.size()-1)
                    break;
                }else {
                    mapStr.put(lstSpit[i+i],new List<String> ());
                    mapStr.get(lstSpit[i+i]).add(lstSpit[i+i+1]);
                    if(i+i+1==lstSpit.size()-1)
                    break;
                }
            
            }
        }

        system.debug('Test1-->'+mapStr);
        system.debug('mapRS ==>'+ mapRS);
        String OS='';
        for (string mapstrOb:mapStr.keyset() ) {
            for(string strg:mapStr.get(mapstrOb) ) {
                 If(isCR){
                 system.debug('mapstrOb-strg ==>'+ mapstrOb+' - '+strg);
                 RelatedSymptom__c rsObj = mapRS.get(mapstrOb+'-'+strg);
                 OS = rsObj.OtherSymptom__c;
                 system.debug('OS ==>'+OS);
                 }
                 lstwrpGMR.add(new WrapperGMR (mapstrOb,'1',false,null,null,strg,OS));
            }
        }
      
        if(lstwrpGMR.size() ==0)
        {
         isNoGMRDATA=false;  
         ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, Label.CLI2PAPR120018));
         iscancel=true;
            
        }
        
        system.debug('## lstwrpGMR ==>'+lstwrpGMR);
       return  null;
    }
    public PageReference saveMaping() 
    {
    
      //*********************************************************************************
        
       list<CommercialReference__c> finalcR=new list<CommercialReference__c>();
        list<RelatedSymptom__c>  lstRelateSymptoms = new list<RelatedSymptom__c>();  // new code
        set<string> setSymptomMapping=new set<string>();
        string finalsymptmstring='';
        RecordType rt;
        system.debug('Test--------->'+lstwrpGMR);
        //Name ='Complaint/Request Related Symptom' and
        // get the list of RecordTypes from the RelatedSymptom__c
        list<RecordType> lstrt = [Select Id,SobjectType,Name From RecordType where  SobjectType ='RelatedSymptom__c'];
        if(lstrt.size()>0)
        {
          for(RecordType r: lstrt)
          {
            if (isCR==true && r.Name==Label.CLAPR14I2P21)  //Complaint/Request Related Symptom
            {
                 rt=r;
            }
            else if(isPRB==true && r.Name==Label.CLAPR14I2P22) //Problem Related Symptom
            {
                rt=r;
            }
          }
        }   

        for(WrapperGMR varPSMPTM:lstwrpGMR)
        {
           if(varPSMPTM.isSelected )
            {
            setSymptomMapping.add(varPSMPTM.propSymptmapping+' - '+varPSMPTM.strseltdChildSymt);
            system.debug('###setSymptomMapping' + setSymptomMapping);
            // new code - start
            RelatedSymptom__c rs = new RelatedSymptom__c();
            If(isCR==true)
            {
                rs.ComplaintRequest__c=objComplntRequestSymptom.id;
                if( varPSMPTM.propSymptmapping == 'Other' && varPSMPTM.strseltdChildSymt== 'Symptom not described above')
                {
                     if(varPSMPTM.strOtherSymptom!='' && varPSMPTM.strOtherSymptom.length()>Integer.valueOf(Label.CLOCT14I2P35))
                     {
                        rs.OtherSymptom__c=varPSMPTM.strOtherSymptom;
                     }
                     else
                     {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, '  Please enter the value for other symptom (min. 5 characters)'));
                        return null;
                     }
                }
               
            }
            else if(isPRB==True)
            {
                rs.Problem__c=objProblemUpdSymptom.id;
            }
            rs.Symptom__c=varPSMPTM.propSymptmapping;
            rs.SubSymptom__c=varPSMPTM.strseltdChildSymt;
            if(rt!=null) {rs.RecordTypeId= rt.Id;}            
            lstRelateSymptoms.add(rs);
            }
        }
        
        system.debug('** lstRelateSymptoms'+lstRelateSymptoms);
        if(lstRelateSymptoms.size()>0)
        {
           
          
          
           upsert lstRelateSymptoms;
        }
        
        if(!setSymptomMapping.isEmpty())
        {
            for (string str : setSymptomMapping)
            {
                finalsymptmstring+=str+Label.CLI2PAPR120004;
            }
            
            
            if(isCR==true)
            {
                if(objComplntRequestSymptom.TECH_Symptoms__c !=null && objComplntRequestSymptom.TECH_Symptoms__c!='')
                {
                //objComplntRequestSymptom.Symptoms__c=objComplntRequestSymptom.Symptoms__c+Label.CLI2PAPR120004+finalsymptmstring.substring(0,finalsymptmstring.lastIndexof(Label.CLI2PAPR120004));
                objComplntRequestSymptom.TECH_Symptoms__c=objComplntRequestSymptom.TECH_Symptoms__c+Label.CLI2PAPR120004+finalsymptmstring.substring(0,finalsymptmstring.lastIndexof(Label.CLI2PAPR120004));
                }
                else
                {
                //objComplntRequestSymptom.Symptoms__c=finalsymptmstring.substring(0,finalsymptmstring.lastIndexof(Label.CLI2PAPR120004));
                objComplntRequestSymptom.TECH_Symptoms__c=finalsymptmstring.substring(0,finalsymptmstring.lastIndexof(Label.CLI2PAPR120004));
                }
            }
            else if(isPRB==true)
            {
                if(objProblemUpdSymptom.TECH_Symptoms__c !=null && objProblemUpdSymptom.TECH_Symptoms__c!='')
                {
                    //objProblemUpdSymptom.Symptoms__c=objProblemUpdSymptom.Symptoms__c+Label.CLI2PAPR120004+finalsymptmstring.substring(0,finalsymptmstring.lastIndexof(Label.CLI2PAPR120004));
                    objProblemUpdSymptom.TECH_Symptoms__c=objProblemUpdSymptom.TECH_Symptoms__c+Label.CLI2PAPR120004+finalsymptmstring.substring(0,finalsymptmstring.lastIndexof(Label.CLI2PAPR120004));
                }
                else
                {
                    //objProblemUpdSymptom.Symptoms__c=finalsymptmstring.substring(0,finalsymptmstring.lastIndexof(Label.CLI2PAPR120004));
                    objProblemUpdSymptom.TECH_Symptoms__c=finalsymptmstring.substring(0,finalsymptmstring.lastIndexof(Label.CLI2PAPR120004));
                }
            }
             
             PageReference    pagereff =modifyProblem();
             return pagereff ;
        }
        else
        {
         ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, Label.CLI2PAPR120001));
         return null;
         }
    }
    Public Pagereference ModyfiMaping()
    {

            //*********************************************************************************
            // Name : ModyfiMaping
           
            string strData='';
            List<WrapperGMR>  lstTempWrapper=new List<WrapperGMR>();
            set<string>stSymptomsTobeRemoved = new set<string>();
            list<RelatedSymptom__c> lstRelSymptomsTobRemoved = new List<RelatedSymptom__c>();
            list<RelatedSymptom__c> lstRelatedSymptoms = new List<RelatedSymptom__c>();
            system.debug('TestModyfi-'+lstwrpGMR);
            For(WrapperGMR wr :lstwrpGMR)
            {
              if(!wr.isSelected )
              {  
                 System.debug('###Wrraper selected Records'+wr.isSelected);
                 strData=strData+wr.propSymptmapping+' - '+wr.strseltdChildSymt+Label.CLI2PAPR120004 ;
                 system.debug('## Other Symptoms'+ wr.strOtherSymptom);
              }
              else
              {
                lstTempWrapper.add(wr);
                stSymptomsTobeRemoved.add(wr.propSymptmapping.trim()+wr.strseltdChildSymt.trim());
              }
            }
            system.debug('$$ stSymptomsTobeRemoved'+stSymptomsTobeRemoved);
            if(stSymptomsTobeRemoved.size()>0)
            {
                if(isCR==true)
                {
                    lstRelatedSymptoms = [select id, symptom__c, subsymptom__c, ComplaintRequest__c from RelatedSymptom__c where ComplaintRequest__c =:objComplntRequestSymptom.id];
                }
                else if(isPRB==true)
                {
                    lstRelatedSymptoms = [select id, symptom__c, subsymptom__c, ComplaintRequest__c from RelatedSymptom__c where Problem__c=:objProblemUpdSymptom.id];
                }
                for(RelatedSymptom__c rs: lstRelatedSymptoms)
                {
                    system.debug('$$ rs.symptom__c-rs.subsymptom__c '+rs.symptom__c+rs.subsymptom__c);
                    if(stSymptomsTobeRemoved.contains(rs.symptom__c+rs.subsymptom__c))
                    {
                        system.debug('$$ rs'+rs);
                        lstRelSymptomsTobRemoved.add(rs);
                    }
                }
            }
            
            If(lstTempWrapper.size()>0)
            {
              System.debug('###strData###'+strData);
            if(isCR==true)
             {  
                if(strData.contains(Label.CLI2PAPR120004)) 
                {
                    //objComplntRequestSymptom.Symptoms__c=strData.substring(0,strData.lastIndexOf(Label.CLI2PAPR120004));
                    objComplntRequestSymptom.TECH_Symptoms__c=strData.substring(0,strData.lastIndexOf(Label.CLI2PAPR120004));
                }
                else
                {
                    //objComplntRequestSymptom.Symptoms__c=strData;
                    objComplntRequestSymptom.TECH_Symptoms__c=strData;
                }
             }
            else if(isPRB == true)
            {
              if(strData.contains(Label.CLI2PAPR120004)) 
              {
                //objProblemUpdSymptom.Symptoms__c=strData.substring(0,strData.lastIndexOf(Label.CLI2PAPR120004));
                objProblemUpdSymptom.TECH_Symptoms__c=strData.substring(0,strData.lastIndexOf(Label.CLI2PAPR120004));
              }
              else
              {
                //objProblemUpdSymptom.Symptoms__c=strData;
                objProblemUpdSymptom.TECH_Symptoms__c=strData;
              }
            }
              system.debug('$$ lstRelSymptomsTobRemoved'+lstRelSymptomsTobRemoved);
              delete lstRelSymptomsTobRemoved;
              Pagereference pg =modifyProblem();
              pg.setRedirect(true);
              return pg;
            }
            else
            {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, Label.CLI2PAPR120001));
             
             return null;
            }
    }
    public Pagereference   modifyProblem()
    {
            //*********************************************************************************
            // Method Name : Updating Symptom to Problem
           
         If(isCR){
           if(objComplntRequestSymptom.TECH_Symptoms__c!=null && objComplntRequestSymptom.TECH_Symptoms__c!='' )
            {
                objComplntRequestSymptom.TECH_SymptomsAvailable__c=True;
            }
         else{
                objComplntRequestSymptom.TECH_SymptomsAvailable__c=False; 
            } 
         update objComplntRequestSymptom;}
         else {update objProblemUpdSymptom;}
         
         PageReference pageRef = new PageReference('/'+ varId);
         pageRef.setRedirect(true);
         return pageRef ;
    }
    public  class WrapperGMR
    {
        public string propGMRCode{get;set;} //Property to Display GMRCode
        public String propSymptmapping{get;set;}  //Property to Set Symptom
        public Boolean isSelected{get;set;}       //Property to Check Select Or Deselect Symptom
        public string strProductGroup{get;set;}   //Property to Set Product Line
        public Symptom__c seleectedsmtp{get;set;} //Property For Symptom Object 
        Public string  strseltdChildSymt{get;set;}
        Public string strOtherSymptom{get; set;}
        public List<SelectOption> lstSelectChildSymps {get;set;}
        Public string strFlag{get; set;}
        public WrapperGMR(string stmmappname,string strgmrcode,boolean blnselected,Symptom__c smtp,List<SelectOption> lstChildSymps,string childSymt,string oSymp)
        {
            //*********************************************************************************
            // Method Name : Wrapper Constructor
            // Purpose : 
            // Created by : 
            // Modified by :
            // Date Modified :
            ///*********************************************************************************/
            // Remarks :For Apr Release
            propGMRCode=strgmrcode;
            propSymptmapping=stmmappname;
            isSelected=blnselected;
            seleectedsmtp=smtp;
            lstSelectChildSymps=lstChildSymps;
            strseltdChildSymt = childSymt;
            strOtherSymptom = oSymp;
            //strFlag = flg;
            //strProductGroup=strprdctGroup;
        }
    }
     public PageReference checkAll()
     {
        //*********************************************************************************
        // Method Name : checkAll
         
          for(WrapperGMR wrpGm : lstwrpGMR)
          {
            wrpGm.isSelected = allChecked;
          }
      
      return null;
     }
    public Pagereference   getlistsetSymptomMapping()
    {
    //*********************************************************************************
        // Method Name : getlistsetSymptomMapping
        
        lstwrpGMR=new list<WrapperGMR>();
        List<Symptom__c> symList =new List<Symptom__c>();
        for(Symptom__c varsmtpmap :[select name,SubSymptom__c,Symptom__c,GMRCode__c,IsActive__c from Symptom__c where GMRCode__c  IN :stGMRCode AND IsActive__c=true  order by Name Limit 1000])
        {
         if(varsmtpmap.Symptom__c!=null && varsmtpmap.SubSymptom__c!=null && !setCompSymptom.contains(varsmtpmap.Symptom__c+' - '+varsmtpmap.SubSymptom__c))
           
           symList.add(varsmtpmap);
        }
        system.debug('****symList'+symList);
        if(symList.size() ==0)
        {
        
         ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, Label.CLI2PAPR120020));
           iscancel=true;
          isNoGMRDATA=false;  
        }else {
      
            parentChlidSymt(symList);
        }
        return  null;
    }
    
    public PageReference canceladdsymptom() 
    {
        
         PageReference prRedirtoProblem= new PageReference('/'+varId);
         prRedirtoProblem.setRedirect(true);
         return prRedirtoProblem ;
    }
    
    
    
    Public void ParentChlidSymt(List<Symptom__c> lstSymt ) {
        //To Display Parent and Child Symptoms 
        lstwrpGMR=new list<WrapperGMR>();
        list<WrapperGMR> lstwrpParentGMR =new list<WrapperGMR>();
        //List<Symptom__c> lstSymt = [select id,Symptom__c,SubSymptom__c from Symptom__c];
        List<SelectOption> lstSelectParChildSymps = new  List<SelectOption> ();
        Map<string,List<String>>  mapListStr = new Map<string,List<String>> () ;
        
        for(Symptom__c symObj:lstSymt ) {
            if(mapListStr.containsKey(symObj.Symptom__c)) {
                mapListStr.get(symObj.Symptom__c).add(symObj.SubSymptom__c);
            
            }else {
                mapListStr.put(symObj.Symptom__c,new List<String> ());
                 mapListStr.get(symObj.Symptom__c).add(symObj.SubSymptom__c);
            }
        
        }
        system.debug('Test---------->'+mapListStr);
        
        List<string> srtSymptoms = new List<string>();
        for(string symp:mapListStr.keyset())
        {
            srtSymptoms.add(symp);
        }    
           srtSymptoms.sort();             
        for(string str:srtSymptoms) {
        
            lstwrpParentGMR.add(new WrapperGMR(str,'varsmtpmap.GMRCode__c',false,null,null,null,null));
        
        }
        for(WrapperGMR wrap:lstwrpParentGMR) {
         lstSelectParChildSymps = new  List<SelectOption> ();
          set<string> setSubSyptoms = new set<string>();
         for(string ss:mapListStr.get(wrap.propSymptmapping))
         {
            setSubSyptoms.add(ss);
         }
         List<string> lstSubSymptoms = new List<string>(setSubSyptoms);
         lstSubSymptoms.sort();
            for(string strwap:lstSubSymptoms) {
               
                lstSelectParChildSymps.add(new selectOption(strwap,strwap));
            }
        lstwrpGMR.add(new WrapperGMR(wrap.propSymptmapping,'varsmtpmap.GMRCode__c',false,null,lstSelectParChildSymps,null,null));
        }
 system.debug('Test---------->'+lstwrpGMR);
 
 }
 
}