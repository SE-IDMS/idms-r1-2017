/*
    Author          : Bhuvana Subramaniyan 
    Description     : Test Class for VFC_LightRITEForm.
*/
@isTest
private class VFC_LightRITEForm_TEST
{
        static testMethod void lightRITETest()
        {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2;

        //Inserts the Product Lines        
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            prodLines.add(pl1);
        }
        Database.SaveResult[] lsr = Database.Insert(prodLines);      
        
        OPP_QuoteLink__c  quoteLink = Utils_TestMethods.createQuoteLink(opp2.Id);
        quoteLink.TypeOfOffer__c = Label.CLAPR14SLS14; 
        quoteLink.Amount__c = 100;
        insert quoteLink;
        
        IPOLightRITE__c lightRITE = Utils_TestMethods.createlightRITE(quoteLink.Id);
        insert lightRITE;
        CS_TextAreaMappingFields__c textAreaMapp = new CS_TextAreaMappingFields__c();
        textAreaMapp.MappingTextArea__c = 'testTextArea__c';
        textAreaMapp.MappingTextArea1__c = 'testTextArea__c';
        textAreaMapp.SMEValues__c = 'testTextArea';
        textAreaMapp.Name = 'testTextArea';
        insert textAreaMapp;
        
        CS_FieldNameAPIName__c apiname = new CS_FieldNameAPIName__c();
        apiname.Name = 'testApiname';
        apiname.FieldFullName__c = 'testFullname';
        insert apiname;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(lightRITE );
        VFC_LightRITEForm lightRITECont = new VFC_LightRITEForm(controller);
        lightRITECont.save();
        
        controller = new ApexPages.StandardController(new IPOLightRITE__c());
        lightRITECont = new VFC_LightRITEForm(controller);
        lightRITECont.save();
                
        }
        
}