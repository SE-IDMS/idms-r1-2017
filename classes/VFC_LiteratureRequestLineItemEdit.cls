public class VFC_LiteratureRequestLineItemEdit {
 public LiteratureRequestLineItem__c commRef ;   
    public VFC_LiteratureRequestLineItemEdit(ApexPages.StandardController controller) {
        LiteratureRequestLineItem__c litlineitemobj = [SELECT Id, Name,LiteratureRequest__c,TECH_LTRStatus__c  FROM LiteratureRequestLineItem__c WHERE Id = :ApexPages.currentPage().getParameters().get('Id')];
        If(litlineitemobj.TECH_LTRStatus__c != 'Processed'){
        String url = '/'+litlineitemobj.Id+'/e?retURL='+litlineitemobj.LiteratureRequest__c;
                system.debug('url-->'+url); 
                PageReference pageRef = new PageReference(url);
                pageRef.setRedirect(true);
                //return pageRef;
        }
    }

}