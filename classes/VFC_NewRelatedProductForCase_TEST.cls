@isTest
private class VFC_NewRelatedProductForCase_TEST{
    static testMethod void NewRelatedProductForCase_TestMethod() {
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
               
        
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        CSE_RelatedProduct__c objCaseRelatedProduct = new CSE_RelatedProduct__c();
        Test.startTest(); 
              
        PageReference pageRef = Page.VFP_NewRelatedProductForCase;
        pageRef.getParameters().put('ID', objOpenCase.Id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController CaseRelatedProductStandardController = new ApexPages.StandardController(objCaseRelatedProduct);   
        VFC_NewRelatedProductForCase NewCaseRelatedProductPage = new VFC_NewRelatedProductForCase(CaseRelatedProductStandardController);
    
        NewCaseRelatedProductPage.redirect();
        Test.stopTest();
    }
}