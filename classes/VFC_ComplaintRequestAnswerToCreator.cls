public class VFC_ComplaintRequestAnswerToCreator {

    /*---------------
    Declare the Variables
    ----------------*/
    private final ComplaintRequest__c lcrRecord;
    public Boolean isRendered {get; set;}
    public Boolean isRenderedFail {get; set;}
    public Boolean isRenderedNoRecord {get; set;}
    public Boolean isCancel {get; set;}
    public Boolean isCancelled {get; set;}
    public String manualOverride {get; set;}
    public string strAccountableOrg; 
    
    public Boolean isErrValidation {get; set;}
    public Integer postcurent;
    
    public  List<Answertostack__c>  listCRSk = new List<Answertostack__c>();
    public  List<Answertostack__c>  listCRSkInsert = new List<Answertostack__c>();
    public VFC_ComplaintRequestAnswerToCreator (ApexPages.StandardController stdController) 
    {
     if (!Test.isRunningTest())
        {
            stdController.addFields(new List<String>{'Case__c','TECH_FirstEsDepartment__c','TECH_FirstEsOrganization__c','TECH_AnswerSackOrg__c','TECH_SwitchOffEmailNotification__c', 'TECH_FromOrg__c','ForceManualSelection__c','AccountableOrganization__c', 'Department__c', 'TECH_AnswerToOrganization__c','TECH_AnswerToDepartment__c','TECH_AnswerToIssueOwner__c','TECH_AnswerToIssueOwner__c', 'KeepInvolved__c','ForceManualSelectionReason__c', 'ReportingEntity__c', 'Category__c','Reason__c','SubReason__c','Status__c','Final_Resolution__c','ReportingDepartment__c'});
        } 
        isErrValidation=false;
        this.lcrRecord = (ComplaintRequest__c)stdController.getRecord();
        addretrivetheOrgToStack(lcrRecord.id);
    }
   
    public pagereference OnPageLoad(){  // Oct 2015 Release - Gayathri Shivakumar
    
       isErrValidation =false;
       PageReference acctPage=null;
        strAccountableOrg = this.lcrRecord.AccountableOrganization__c;  
                       
        if(listCRSk.size() >0)
                    {
                    
                        list<Answertostack__c> ansStock = new list<Answertostack__c>();
                        list<Answertostack__c> ansLatestStock = new list<Answertostack__c>();
                        integer clearInt;
                        postcurent=getLastKeepInvolved();
                        if(listCRSk.size() > 0) {
                            ansStock.add(listCRSk.get(postcurent));
                        
                        }
                        
                        system.debug('------->>>> ' + lcrRecord.Department__c  + '' + lcrRecord.TECH_AnswerToDepartment__c);
                        lcrRecord.AccountableOrganization__c =ansStock[0].Organization__c; //lcrRecord.TECH_AnswerToOrganization__c; 
                        lcrRecord.Department__c = ansStock[0].Department__c;                        
                        lcrRecord.IssueOwner__c = ansStock[0].IssueOwner__c;
                        lcrRecord.ContactEmailAccountableOrganization__c = ansStock[0].ContactEmailAccountableOrganization__c;
                        
                        //lcrRecord.KeepInvolved__c = False;                        
                        //lcrRecord.TECH_Answer__c = True;
                        if((lcrrecord.Final_Resolution__c == NULL || lcrrecord.Final_Resolution__c == '') && lcrrecord.AccountableOrganization__c == lcrrecord.ReportingEntity__c && lcrrecord.Department__c == lcrrecord.ReportingDepartment__c) {
                            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'CR Cannot be Reassigned to the Reporting Entity without updating the Final Resolution')); //'Please enter either Commerical Reference Received or Part Number Received.'
                            isErrValidation = True;
                            acctPage = null;
                        }                       
                        
                        //Clearing Stack
                        listCRSk=[select id,name,ComplaintRequest__c,KeepInvolved__c,Organization__c,Department__c,IssueOwner__c,ContactEmailAccountableOrganization__c  from Answertostack__c where ComplaintRequest__c=:lcrRecord.id order by TECH_StackCounter__c DESC];
        
                        clearInt=getLastKeepInvolved();
                        if(listCRSk.size() > 0) {
                            ansLatestStock.add(listCRSk[clearInt]);
                            lcrRecord.TECH_AnswerSackOrg__c=ansLatestStock[0].Organization__c;
                        }else {
                            lcrRecord.TECH_AnswerSackOrg__c=null;
                        }
                        
                        
                        isRendered = True;
                
                    }
                    else{
                   
                    // isRenderedFail = False;
                      isRendered = False;  
                    }
                    
            if(isRendered == True) {  
           
            try{
            
                System.debug('-------LctRecord'+lcrRecord);
                 lcrRecord.TECH_EventType__c=label.CLDEC13LCR10;
                 
                if((lcrrecord.Final_Resolution__c == NULL || lcrrecord.Final_Resolution__c == '') && lcrrecord.AccountableOrganization__c == lcrrecord.ReportingEntity__c && lcrrecord.Department__c == lcrrecord.ReportingDepartment__c) {
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'CR Cannot be Reassigned to the Reporting Entity without updating the Final Resolution')); //'Please enter either Commerical Reference Received or Part Number Received.'
                    isErrValidation = True;
                    acctPage = null;


                }
                if(!isErrValidation) {
                    update lcrRecord;                    
                    clearStackFromPosition(postcurent);             
                
                }
                if (Test.isRunningTest()) {
               
                    string testcoverage = null;
                    testcoverage.toLowerCase();
                }
            }
            catch(Exception Ex) {
           
                system.debug(Ex);
            }
           
           // update lcrRecord;
           if(!isErrValidation) {   
                 acctPage = new PageReference('/'+lcrRecord.Id);
                acctPage.setRedirect(true);
                 
            }
           return acctPage;
        }else{
       
            PageReference acctPage1 = new PageReference('/'+lcrRecord.Id);
            acctPage1.setRedirect(true);
            return acctPage1;
            }
                   
        }
        
    Public pagereference validateAnswerToCreator() {
   
       
       if(lcrRecord.Status__c==Label.CLOCT13RR57 )
        {   
            isCancel=false;
            isRenderedFail = true;
            isRenderedNoRecord = false; 
            isRendered = false;
            isCancelled=true;
        } 
        return null;
        
    }
    
     public void addretrivetheOrgToStack(string CompRequest) {
        
        listCRSk=[select id,name,ComplaintRequest__c,KeepInvolved__c,Organization__c,IssueOwner__c,ContactEmailAccountableOrganization__c,Department__c  from Answertostack__c where ComplaintRequest__c=:CompRequest order by TECH_StackCounter__c DESC];
        system.debug('STACK LIST------->'+listCRSk);
        
    }

    
    //get the Keep involve position 
     public Integer getLastKeepInvolved() {
        integer pos = -1;
        system.debug(pos);
        for (integer i =0 ; i <listCRSk.size() && pos < 0; i++) {
            if (listCRSk[i].KeepInvolved__c) {
                pos = i;
            }
        }
        system.debug('Position --->'+pos);
        return pos;
    }
    
    public void clearStackFromPosition(integer start){
        //for Delete the Stack
        List<Answertostack__c>  listClearCRSk = new List<Answertostack__c>();
        //start = start; //|| 0;
        system.debug('Delete --->'+start);
        if(start < listCRSk.size()) {
            for(integer i=0;i<listCRSk.size();i++) {
                    
                    listClearCRSk.add(listCRSk[i]);
                    if( i==start) {   
                       
                        break; 

                    }                
            }        
        }       
        if(listClearCRSk.size() > 0) {
         delete listClearCRSk;
        
        }    
    } 
    
}