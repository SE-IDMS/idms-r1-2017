/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 26/10/2010
    Description     : Test class for AP01_OpportunityFieldUpdate class and related Opportunity triggers.  
                        Triggers copy the owner Id into custom field TECH_OwnerId__c
*/
@isTest
private class AP01_OpportunityFieldUpdate_TEST {

    static testMethod void testPopulateTechOwnerId() {
        AP05_OpportunityTrigger.testClassFlag = false;
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        //Check Before insert trigger execution
        Opportunity opp = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp;
     
        //Check Before update trigger execution - change owner
        User tst1 = Utils_TestMethods.createStandardUser('tst1');
        insert tst1;
        opp.OwnerId = tst1.Id;
        opp.OpportunityScope__c = 'Power Products';
        update opp;
        
        Opportunity opp1 = Utils_TestMethods.createOpportunity(acc.Id);
        opp1.TECH_CrossProcessConversionAmount__c = 100;
        insert opp1;        
    }
    
    //Test MEthod that checks if the Opportunity Stage is Stage 0 and the Status is Account Not selected or Lost
    static testMethod void testOppClose()
    {
        AP05_OpportunityTrigger.testClassFlag = false;
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
         
        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.StageName = Label.CLOCT13SLS02;
        opp2.Status__c = Label.CLSEP12SLS13;     
        opp2.Reason__c = Label.CLJUN13MKT01;   
        insert opp2;
        
        Test.startTest();
        
        OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
        insert pl1;
        
        opp2.StageName = Label.CL00272;
        update opp2;
        
        
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='ABC';
        insert country;
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        insert prj;
        
        Opportunity opp1 = Utils_TestMethods.createOpportunity(acc.Id);
        opp1.Project__c=prj.Id;
        insert opp1; 
        
        prj.ProjectStatus__c='Completed';
        update  prj;

        try{
        opp2.ParentOpportunity__c=opp1.Id;
        update opp2;
       }
       Catch(Exception ex){}
       Test.stopTest();
    }
    //Test Method that Checks if the Opportunity is in Stage 0 and Status equals Cancelled
    static testMethod void testOppCloseCancelled()
    {
        AP05_OpportunityTrigger.testClassFlag = false;
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
         Opportunity opp1 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp1;
        
         Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
         opp2.ParentOpportunity__c=opp1.Id;
        insert opp2;
        
         Opportunity opp3 = Utils_TestMethods.createOpportunity(acc.Id);
        opp3.StageName = Label.CLOCT13SLS02;
        opp3.Status__c = Label.CLOCT13SLS08 ;        
        opp3.Reason__c = Label.CLJUN13MKT01;
        insert opp3;
        OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp3.Id);
        pl1.LineStatus__c= Label.CLJUN16SLS01;
        insert pl1; 
        OPP_ProductLine__c pl2 = Utils_TEstMethods.createProductLine(opp3.Id);
        insert pl2;
        
        Test.startTest();      
        opp3.StageName = Label.CL00272;
        update opp3;

        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='DEF';
        insert country;
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        insert prj;
        

        opp1.Project__c=prj.Id;
        update opp1; 
        
        prj.ProjectStatus__c='Completed';
        update  prj;

        try{
        opp3.ParentOpportunity__c=opp2.Id;
        update opp3;
       }
       Catch(Exception ex){}
       Test.stopTest();

    }
    
    //Test Method that Checks if the Opportunity is in Stage 7 and Status equals Won    
    static testMethod void testOppCloseWon()
    {
        AP05_OpportunityTrigger.testClassFlag = false;
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        Opportunity opp4 = Utils_TestMethods.createOpportunity(acc.Id);
        opp4.StageName = Label.CLOCT13SLS17;
        opp4.Status__c = Label.CLOCT13SLS10 ;        
        opp4.Reason__c = Label.CLJUN13MKT01;
        insert opp4;
        
        OPP_ProductLine__c pl2 = Utils_TestMethods.createProductLine(opp4.Id);
        pl2.LineStatus__c = Label.CLSEP12I2P84;
        insert pl2;
        
        OPP_SalesContributor__c sl = Utils_TEstMethods.createSalesContr(opp4.Id, userInfo.getUserId());
        sl.Contribution__c = 100;
        insert sl;
        
        opp4.StageName = Label.CL00272;
        opp4.Status__c = Label.CLSEP12SLS14;
        update opp4;
        Test.startTest();
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='GHI';
        insert country;
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        prj.ProjectStatus__c='Completed';
        insert prj;
        
        opp4.project__c=prj.Id;
        try{
        update opp4;
       }
       Catch(Exception ex){}
       Test.stopTest();
    }
}