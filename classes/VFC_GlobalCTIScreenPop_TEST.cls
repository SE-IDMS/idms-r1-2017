@isTest
Public Class VFC_GlobalCTIScreenPop_TEST{
     
    Public static testMethod void getCseInfo(){
          
          Country__c Contry = New Country__c ();
          Contry.Name='France';Contry .CountryCode__c='Fr';
          insert Contry;
          
          Account accounts = Utils_TestMethods.createAccount();
          Database.insert(accounts);
          
          contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
          Database.insert(con); 
          Con.Country__c = Contry.Id;
          update Con; 
          
          Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'New');
          Database.insert(caseobj);
          System.debug('caseobj:'+caseobj);
          System.debug('caseobj:'+caseobj.caseNumber);
          
          
         apexpages.currentpage().getparameters().Put('CaseId',caseObj.id);
          VFC_GlobalCTIScreenPop pageRef=  new VFC_GlobalCTIScreenPop();
          pageRef.pageaction();
    }
      static testMethod void unitTestWithCR(){
       Account accounts = Utils_TestMethods.createAccount();
          Database.insert(accounts);
          
          contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
          Database.insert(con); 
         
       // apexpages.currentpage().getparameters().Put('LegacyPin','112222');
        apexpages.currentpage().getparameters().Put('ContactRecognition','SingleMatch');
        apexpages.currentpage().getparameters().Put('ContactId',Con.id);
        VFC_GlobalCTIScreenPop pageRef=  new VFC_GlobalCTIScreenPop();
        pageRef.pageaction();
        
    }
    static testMethod void unitTestWithAccId(){
       Account accounts = Utils_TestMethods.createAccount();
          Database.insert(accounts);
        apexpages.currentpage().getparameters().Put('ContactRecognition','SingleMatch');
        apexpages.currentpage().getparameters().Put('AccountId',accounts.id);
        VFC_GlobalCTIScreenPop pageRef=  new VFC_GlobalCTIScreenPop();
        pageRef.pageaction();
        
    }
    static testMethod void getCseInfoWithPin(){
          Account accounts = Utils_TestMethods.createAccount();
          Database.insert(accounts);
          
          contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
          Database.insert(con); 
          
          Contract contractObj= Utils_TestMethods.createContract(accounts.Id,con.Id);
          insert contractObj;
          System.debug('contractObj'+contractObj);
        
          CTR_ValueChainPlayers__c  CVCPObj =Utils_TestMethods.createCntrctValuChnPlyr(contractObj.id);
          CVCPObj.legacypin__c='112222' ;
          insert CVCPObj; 
          System.debug('CVCPObj'+CVCPObj);
          apexpages.currentpage().getparameters().Put('LegacyPin','112222');
          apexpages.currentpage().getparameters().Put('IVR','TESTIVR');
          VFC_GlobalCTIScreenPop pageRef=  new VFC_GlobalCTIScreenPop();
          pageRef.pageaction();
        
    }
    static testMethod void getCseInfoWithoutPin(){
          Account accounts = Utils_TestMethods.createAccount();
          Database.insert(accounts);
          
          contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
          Database.insert(con); 
          
          Contract contractObj= Utils_TestMethods.createContract(accounts.Id,con.Id);
          insert contractObj;
          System.debug('contractObj'+contractObj);
        
          CTR_ValueChainPlayers__c  CVCPObj =Utils_TestMethods.createCntrctValuChnPlyr(contractObj.id);
          CVCPObj.legacypin__c='112222' ;
          insert CVCPObj; 
          System.debug('CVCPObj'+CVCPObj);
          apexpages.currentpage().getparameters().Put('LegacyPin','1122222');
          apexpages.currentpage().getparameters().Put('IVR','TESTIVR');
          VFC_GlobalCTIScreenPop pageRef=  new VFC_GlobalCTIScreenPop();
          pageRef.pageaction();
        
    }
    static testMethod void withANIandNomatch(){
        apexpages.currentpage().getparameters().Put('ContactRecognition','NoMatch');
        apexpages.currentpage().getparameters().Put('ANI','33000000');
        VFC_GlobalCTIScreenPop pageRef=  new VFC_GlobalCTIScreenPop();
        pageRef.pageaction();
        
    }
     static testMethod void withANINomatchtime(){
        apexpages.currentpage().getparameters().Put('ContactRecognition','NoMatch_TimeOut');
        apexpages.currentpage().getparameters().Put('ANI','330000001');
        VFC_GlobalCTIScreenPop pageRef=  new VFC_GlobalCTIScreenPop();
        pageRef.pageaction();
        
    }
    static testMethod void withANIMaultimatcht(){
        apexpages.currentpage().getparameters().Put('ContactRecognition','MultiMatch');
        apexpages.currentpage().getparameters().Put('ANI','330000001');
        VFC_GlobalCTIScreenPop pageRef=  new VFC_GlobalCTIScreenPop();
        pageRef.pageaction();
        
    }
    static testMethod void getCseInfoWithANI(){
        apexpages.currentpage().getparameters().Put('ANI','33000000');
        VFC_GlobalCTIScreenPop pageRef=  new VFC_GlobalCTIScreenPop();
        pageRef.pageaction();
        
    }
    static testMethod void getCseInfoWithnomatchcase(){
        apexpages.currentpage().getparameters().Put('CaseId','121211');
        apexpages.currentpage().getparameters().Put('ANI','33000000');
        VFC_GlobalCTIScreenPop pageRef=  new VFC_GlobalCTIScreenPop();
        pageRef.pageaction();
        
    }
}