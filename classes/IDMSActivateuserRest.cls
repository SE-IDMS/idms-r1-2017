/*
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  06/12/2016
*   This is a global class exposing the user activation API. It will be called by IFW
* */
@RestResource(urlMapping ='/ActivateUser')
global with sharing class IDMSActivateuserRest{
    
    //PUT method called by API which will activate the user. In case of mobile registration, it is returning the confirmation pin algorithm
    @HttpPut
    global static IDMSConfirmPinResponse doPut(User UserRecord){
        
        IDMSActivateuser activateUser   = new IDMSActivateuser(); 
        //String Id,String FederationIdentifier,string Profile_update_source
        IDMSConfirmPinResponse response = activateUser.Activateuser(UserRecord.Id,UserRecord.FederationIdentifier,UserRecord.IDMS_Registration_Source__c);
        RestResponse res                = RestContext.response;
        if(Response.Status.equalsignorecase('Error')){
            res.statuscode = Integer.valueOf(label.CLDEC16IDMS001);
            if(Response.Message.startsWith('Error in')){
                res.statuscode = Integer.valueOf(label.CLDEC16IDMS001);
            }
        }
        if(Response.Message.startsWith('User not')){
            res.statuscode = Integer.valueOf(label.CLDEC16IDMS002);
        }
        return Response;
    }
}