/**
* This is test class for IDMSInvitationEmailEncryption class. 
**/

@isTest
class IDMSInvitationEmailEncryptionTest{
    //This method test signature with randomly generated token. 
    static testmethod void testIDMSInvitationEmailEncryption(){
        IDMS_Send_Invitation__c IDMSSendInvitation= new IDMS_Send_Invitation__c(IDMSToken__c='testtoken',IDMSSignatureGenerationDate__c=system.now().date());
        insert IDMSSendInvitation; 
        
        String signature='test23';
        datetime dt=system.today().adddays(8);
        IDMSSendInvitation.IDMSToken__c='test23';
        IDMSSendInvitation.IDMSSignatureGenerationDate__c=System.now().date();
        update IDMSSendInvitation;
        
        IDMSInvitationEmailEncryption IDMSEmailEncryption=new IDMSInvitationEmailEncryption();
        IDMSInvitationEmailEncryption.EmailDecryption('test23',IDMSSendInvitation.Id);
        IDMSInvitationEmailEncryption.EmailEncryption(IDMSSendInvitation);
        IDMSInvitationEmailEncryption.generateRandomString(1234);
    }
}