/********************************************************************
* Company: Fielo
* Developer: Pablo Cassinerio
* Created Date: 23/08/2016
* Description: 
********************************************************************/
global class FieloPRM_Batch_MemberFeatProcess implements Database.Batchable<sObject>, Schedulable {

    private static CS_PRM_ApexJobSettings__c config= CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Batch_MemberFeatProcess');
  
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Set<string> setFieldsMemberFeat =  Schema.SObjectType.FieloPRM_MemberFeature__c.fields.getMap().keySet();
        List<string> listFieldsMemberFeat = new list<string>();
        listFieldsMemberFeat.addAll(setFieldsMemberFeat);
        String query = 'SELECT ' + String.join(listFieldsMemberFeat, ',') + ' FROM FieloPRM_MemberFeature__c WHERE (F_PRM_Status__c IN (\'TOREPROCESSACTIVE\', \'TOREPROCESSINACTIVE\')) OR (F_PRM_Status__c IN (\'TOPROCESSACTIVE\', \'TOPROCESSINACTIVE\') AND (F_PRM_IsSynchronic__c = FALSE OR F_PRM_TryCount__c > 0)) ORDER BY CreatedDate';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(isRunning(BC.getJobId())) return;
        process(scope);
    }
  
    global void finish(Database.BatchableContext bc) {
        //schedule(bc.getJobId());
    }
  
    global void execute(SchedulableContext sc){
        if(config == null){
            config = new CS_PRM_ApexJobSettings__c(Name = 'FieloPRM_Batch_MemberFeatProcess', F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10, F_PRM_TryCount__c = 5);
        }
        Database.executeBatch(new FieloPRM_Batch_MemberFeatProcess(), Integer.valueOf(config.F_PRM_BatchSize__c));
    }

    global static void runSync(List<FieloPRM_MemberFeature__c> mfs){
        if(config == null){
            config = new CS_PRM_ApexJobSettings__c(Name = 'FieloPRM_Batch_MemberFeatProcess', F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10, F_PRM_TryCount__c = 5);
        }
        Boolean isGuest = UserInfo.getUserType() == 'Guest';
        Id memId;
        try{
            memId = getMemberId();
        }catch(Exception e){}
        List<FieloPRM_MemberFeature__c> thisUserMfs = new List<FieloPRM_MemberFeature__c>();
        List<FieloPRM_MemberFeature__c> syncMfs = new List<FieloPRM_MemberFeature__c>();
        for(FieloPRM_MemberFeature__c mf: mfs){
            if(mf.F_PRM_Status__c != null && (mf.F_PRM_Status__c.equals('TOPROCESSACTIVE') || mf.F_PRM_Status__c.equals('TOPROCESSINACTIVE')) && mf.F_PRM_TryCount__c == 0){
                if(mf.F_PRM_IsSynchronic__c){
                    syncMfs.add(mf);
                }else if(isGuest || (memId != null && mf.F_PRM_Member__c == memId)){
                    thisUserMfs.add(mf);
                }
            }
        }
        if(syncMfs.size() > 0){
            process(syncMfs);
        }else if(thisUserMfs.size() > 0 && thisUserMfs.size() <= config.F_PRM_SyncAmount__c){
            process(thisUserMfs);
        }
    }

    global static void process(List<FieloPRM_MemberFeature__c> feats){
        List<FieloPRM_MemberFeature__c> toActivate = new List<FieloPRM_MemberFeature__c>();
        List<FieloPRM_MemberFeature__c> toInactivate = new List<FieloPRM_MemberFeature__c>();
        for(FieloPRM_MemberFeature__c feat: (List<FieloPRM_MemberFeature__c>)feats){
            if(feat.F_PRM_Status__c.equals('TOPROCESSACTIVE') || feat.F_PRM_Status__c.equals('TOREPROCESSACTIVE')){
                toActivate.add(feat);
            }else{
                toInactivate.add(feat);
            }
        }
        if(toActivate.size() > 0){
            FieloPRM_AP_MemberFeatureTriggers.executeCustomLogic(toActivate);
        }
        if(toInactivate.size() > 0){
            FieloPRM_AP_MemberFeatureTriggers.executeDemoteCustomLogic(toInactivate);
        }
    }
    
    global static void schedule(Id jobId){
        List<FieloPRM_MemberFeature__c> toProcess = [SELECT Id FROM FieloPRM_MemberFeature__c WHERE (F_PRM_Status__c IN ('TOREPROCESSACTIVE', 'TOREPROCESSINACTIVE')) OR (F_PRM_Status__c IN ('TOPROCESSACTIVE', 'TOPROCESSINACTIVE') AND (F_PRM_IsSynchronic__c = FALSE OR F_PRM_TryCount__c > 0)) LIMIT 1];
        if (!isRunning(jobId) && toProcess.size() > 0) {
            scheduleNow();
        }
    }

    global static Boolean isRunning(Id jobId){
        AsyncApexJob[] jobs = [SELECT Id 
                               FROM AsyncApexJob 
                               WHERE Status = 'Processing'
                               AND ApexClass.Name = 'FieloPRM_Batch_MemberFeatProcess'
                               AND JobType = 'BatchApex'
                               AND Id != :jobId];
        return jobs.size() > 0;
    }
    
    public static void scheduleNow(){
        DateTime nowTime = datetime.now().addSeconds(65);
        String Seconds      = '0';
        String Minutes      = nowTime.minute() < 10 ? '0' + nowTime.minute() : String.valueOf(nowTime.minute());
        String Hours        = nowTime.hour()   < 10 ? '0' + nowTime.hour()   : String.valueOf(nowTime.hour());
        String DayOfMonth   = String.valueOf(nowTime.day());
        String Month        = String.ValueOf(nowTime.month());
        String DayOfweek    = '?';
        String optionalYear = String.valueOf(nowTime.year());
        String CronExpression = Seconds+' '+Minutes+' '+Hours+' '+DayOfMonth+' '+Month+' '+DayOfweek+' '+optionalYear;
        System.schedule('FieloPRM_Batch_MemberFeatProcess '+system.now() + ':' + system.now().millisecond(), CronExpression, new FieloPRM_Batch_MemberFeatProcess());
    }

    public static Id getMemberId(){
        Id memId;
        try{
            List<FieloEE__Member__c> members = [SELECT Id, FieloEE__Program__c FROM FieloEE__Member__c WHERE FieloEE__User__c =: UserInfo.getUserId()];
            if(members.size() == 1){
                memId = members[0].Id;
            }else{
                Id programId = FieloEE.ProgramUtil.getProgramByDomain().Id;
                for(FieloEE__Member__c m : members){
                    if(m.FieloEE__Program__c == programId){
                        memId = m.Id;
                        break;
                    }
                }
            }
        }catch(Exception e){
            return null;
        }
        return memId;
    }
}