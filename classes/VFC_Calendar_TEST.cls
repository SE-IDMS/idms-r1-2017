/**
 *  About
 *  -----
 *  Author: Paul Roth <proth@salesforce.com>
 *  Created for: ICON Project
 *  Create date: Wed Aug 07 13:42:35 CDT 2013 @821 /Internet Time/
 *  
 *  Details
 *  -----
 *  Test class for IC_Calendar
 *  
 *  Update History
 *  -----
 *  Wed Aug 07 13:42:35 CDT 2013 @821 /Internet Time/ - initial version
 *  
 *  Issues / TODOs
 *  -----
 *  (There are a number of tasks, please see rally project)
 *  There is no creationutils class however...
**/
@isTest
private class VFC_Calendar_TEST {
    
    class TestSettings {
        
        //-- instance values
        user u;
        Account a;
        Contact c;
        Country__c country;
        list<Event> events;
        Task[] tasks;
        
        Event[] eventsThisMonth;
        Task[] tasksThisMonth;
        
        Integer numEventsThisMonth;
        Integer numTasksThisMonth;
        
        DateTime dtYesterday = DateTime.now().addDays(-1);
        DateTime now = DateTime.now();
        DateTime dtTomorrow = DateTime.now().addDays(1);
        
        Date lastMonth  = Date.newInstance(2013,07,15);
        Date startMonth = Date.newInstance(2013,08,01);
        Date lastWeek   = Date.newInstance(2013,08,08);
        Date yesterday  = Date.newInstance(2013,08,14);
        Date today      = Date.newInstance(2013,08,15);
        Date tomorrow   = Date.newInstance(2013,08,16);
        Date nextWeek   = Date.newInstance(2013,08,22);
        Date endMonth   = Date.newInstance(2013,08,31);
        Date nextMonth  = Date.newInstance(2013,09,15);
        
        String startMonthStr = null;
        String endMonthStr = null;
        
        String[] completedStatuses;
        
        public TestSettings(){
            String[] completedStatuses = new String[]{};
            completedStatuses.addAll( VFC_CalendarEvent.COMPLETED_STATUSES );
            //System.debug( 'testSettings.completedStatuses[' + completedStatuses + ']' );
            completedStatuses = new String[]{ 'Completed' };
            
            //-- global setup here
            u = Utils_TestMethods.createStandardUser( 'test'); //, IC_TestFactory.PROFILE_ICON_SA );
            insert u;
            
            a = Utils_TestMethods.createAccount();
            insert a;
            
            Country__c country = Utils_TestMethods.createCountry();
            insert country;
            
            c = Utils_TestMethods.createContact(a.id, country.id);
            insert c;

            DateTime testDT = DateTime.newInstance( startMonth, Time.newInstance(0,0,0,0) );
            startMonthStr = testDT.format( 'yyyy-MM-dd HH:mm:ss' );
            testDT = DateTime.newInstance( endMonth, Time.newInstance(0,0,0,0) );
            endMonthStr = testDT.format( 'yyyy-MM-dd HH:mm:ss' );
        }
        
        private Event createEvent( Account a, DateTime startTime ){
            Event e = Utils_TestMethods.createEvent(a.id, c.id, 'Planned' );
            e.StartDateTime = startTime;
            e.EndDateTime = startTime;
            return( e );
        }
        
        private Task createTask( Account a, Boolean isComplete, Date activityDate ){
            Task t = Utils_TestMethods.createTask(a.id, c.id, 'Planned');
            if( isComplete ){
                //-- @TODO: investigate why completedStatuses is null
                //t.Status = completedStatuses[0];
                t.Status = 'Completed';
            }
            t.activityDate = activityDate;
            return( t );
        }
        
        public void setupDefault(){
            //-- specific setup here
            this.eventsThisMonth = new Event[]{
                createEvent( a, DateTime.newInstance( lastWeek, Time.newInstance( 9,0,0,0) )),
                
                createEvent( a, DateTime.newInstance( yesterday, Time.newInstance( 9,0,0,0) )),
                createEvent( a, DateTime.newInstance( yesterday, Time.newInstance( 11,0,0,0) )),
                
                createEvent( a, DateTime.newInstance( today, Time.newInstance( 9,0,0,0) )),

                createEvent( a, DateTime.newInstance( tomorrow, Time.newInstance( 14,0,0,0) )),
                createEvent( a, DateTime.newInstance( tomorrow, Time.newInstance( 17,0,0,0) ))
            };
            events = new list<Event>();
            /*events = new Event[]{
                createEvent( a, DateTime.newInstance( lastMonth, Time.newInstance( 9,0,0,0) )), //-- not this month
                createEvent( a, DateTime.newInstance( nextMonth, Time.newInstance( 9,0,0,0) )) //-- not this month
            };*/
            events.addAll( eventsThisMonth );
            System.debug('----->>>>>> Events This month: --'+eventsThisMonth);
            insert events;
            
            tasksThisMonth = new Task[]{
                createTask( a, false, lastWeek ),
                createTask( a, true, yesterday ),
                
                createTask( a, false, today ),
                createTask( a, true, today ),
                
                createTask( a, false, tomorrow ),
                createTask( a, true, nextWeek )
            };
            tasks = new Task[]{
                createTask( a, false, lastMonth ),
                createTask( a, true, lastMonth ),
                createTask( a, false, nextMonth ),
                createTask( a, true, nextMonth )
            };
            tasks.addAll( tasksThisMonth );
            insert tasks;
            
            //-- due to workflow EVT_WF01_AppointmentReminder, this a task is also created for every event
            this.numEventsThisMonth = this.eventsThisMonth.size();
            this.numTasksThisMonth = this.tasksThisMonth.size() + this.numEventsThisMonth;
        }
        
        public void finalize(){
            
        }
    }
    
    //-- test test methods here
    static testMethod void runDefaultSetupWithoutException() {
        try {
            TestSettings ts = new TestSettings();
            ts.setupDefault();
            ts.finalize();
        } catch( Exception err ){
            System.debug( 'Exception must not be thrown:' + err.getMessage() );
        }
    }
    
    //-- test functionality here
    static testMethod void testUserCanBeCreated(){
        try {
            User u = Utils_TestMethods.createStandardUser( 'testuser'); //, IC_TestFactory.PROFILE_MAP.get( IC_TestFactory.PROFILE_ICON_SA_NAME ) );
            insert u;
        } catch( Exception err ){
            System.assert( false, 'Exception must not be thrown when creating a user:' + err.getMessage() );
        }
    }
    
    static testMethod void testProfileCreatedCorrectly(){
        User u = Utils_TestMethods.createStandardUser( 'testusr1'); //, IC_TestFactory.PROFILE_ICON_SA );
        insert u;
        //System.assertEquals( IC_TestFactory.PROFILE_ICON_SA, u.profileId, 'Profile Id must be found and match' );
        
        //Id expectedValue = IC_TestFactory.PROFILE_MAP.get( IC_TestFactory.PROFILE_ICON_SA_NAME );
        //Id results = u.profileId;
        //System.assertEquals( expectedValue, results, 'ProfileId must be found in the map' );
    }
    
    /*static testMethod void testIsProfile(){
        User u = IC_TestFactory.createUser( 'testuser',
            IC_TestFactory.PROFILE_MAP.get( IC_TestFactory.PROFILE_ICON_SA_NAME )
        );
        insert u;
        
        System.assertEquals( true, IC_TestFactory.isUserProfile( IC_TestFactory.PROFILE_ICON_SA_NAME, u ), 'IsUserProfile must match the appropriate profile' );
        System.assertEquals( false, IC_TestFactory.isUserProfile( IC_TestFactory.PROFILE_ICON_CORPORATE_NAME, u ), '!IsUserProfile must not match the appropriate profile' );
        

        System.runAs( u ){
            System.assertEquals( true, IC_TestFactory.isUserProfile( IC_TestFactory.PROFILE_ICON_SA_NAME ), 'isUserProfile for current user' );
            System.assertEquals( false, IC_TestFactory.isUserProfile( IC_TestFactory.PROFILE_ICON_CORPORATE_NAME ), 'isUserProfile for current user' );
        }
    }*/
    
    static testMethod void testEventsTasksCreated(){
        TestSettings ts = new TestSettings();
        ts.setupDefault();
        ts.finalize();
        
        Task[] tasksFound = [ select id, Subject from Task ];
        Event[] eventsFound = [ select id, Subject from Event ];
        //System.debug( 'testEventTasksCreated:' + tasksFound );
        //System.debug( 'testEventTasksCreated:' + eventsFound );
        
        System.assertEquals( ts.events.size(), eventsFound.size(), 'events must be found' );
        //-- due to workflow EVT_WF01_AppointmentReminder, this a task is also created for every event
        //System.assertEquals( ts.events.size() + ts.tasks.size(), tasksFound.size(), 'tasks must be found' );
    }
    
    static testMethod void testLocaleFindsGlobalizeLocale(){
        TestSettings ts = new TestSettings();
        ts.setupDefault();
        ts.finalize();
        
        System.runAs( ts.u ){
            String expectedLocale = 'en-US';
            String resultLocale = VFC_Calendar.getLocale();
            System.assertEquals( expectedLocale, resultLocale, 'Locales must not have underscores' );
        }
    }
    
    static testMethod void testLangFindsGlobalizeLocale(){
        TestSettings ts = new TestSettings();
        ts.setupDefault();
        ts.finalize();
        
        System.runAs( ts.u ){
            String expectedLocale = 'en-US';
            String resultLocale = VFC_Calendar.getLang();
            System.assertEquals( expectedLocale, resultLocale, 'Lang must not have underscores' );
        }
    }
    
    static testMethod void testNoEventsForInvalidDates(){
        TestSettings ts = new TestSettings();
        ts.setupDefault();
        ts.finalize();
        
        VFC_CalendarEvent[] results = VFC_Calendar.getEventsRange( null, ts.endMonth );
        System.assertEquals( 0, results.size(), 'No events for null,null events range:' + results );
        results = VFC_Calendar.getEventsRange( ts.startMonth, null );
        System.assertEquals( 0, results.size(), 'No events for null,null events range:' + results );
        results = VFC_Calendar.getEventsRange( ts.startMonth, ts.endMonth );
        System.assertNotEquals( 0, results.size(), 'Events must be found for valid dates:' + results );
    }
    
    static testMethod void testNoTasksForInvalidDates(){
        TestSettings ts = new TestSettings();
        ts.setupDefault();
        ts.finalize();
        
        VFC_CalendarEvent[] results = VFC_Calendar.getTasksRange( null, ts.endMonth );
        System.assertEquals( 0, results.size(), 'No events for null,null tasks range:' + results );
        results = VFC_Calendar.getTasksRange( ts.startMonth, null );
        System.assertEquals( 0, results.size(), 'No events for null,null tasks range:' + results );
        results = VFC_Calendar.getTasksRange( ts.startMonth, ts.endMonth );
        System.assertNotEquals( 0, results.size(), 'Tasks must be found for valid dates:' + results );
    }
    
    static testMethod void testEventsForMonthFindEvents(){
        TestSettings ts = new TestSettings();
        ts.setupDefault();
        ts.finalize();
        
        VFC_CalendarEvent[] results = VFC_Calendar.getEventsRange( ts.startMonth, ts.endMonth );
        System.assertEquals( ts.numEventsThisMonth, results.size(), 'Events should be found for current month:' + results );
        
        Map<Id,Event> eventMap = new Map<Id,Event>(ts.events);
        Event e = null;
        for( VFC_CalendarEvent evt : results ){
            e = eventMap.get( evt.Id );
            System.assertEquals( e.Subject, evt.Title, 'Title must match' );
        }
    }
    
    static testMethod void testTasksForMonthFindTasks(){
        TestSettings ts = new TestSettings();
        ts.setupDefault();
        ts.finalize();
        
        VFC_CalendarEvent[] results = VFC_Calendar.getTasksRange( ts.startMonth, ts.endMonth );
        //System.assertEquals( ts.numTasksThisMonth, results.size(), 'Events should be found for current month:' + results + '\n' );
        
        Map<Id,Task> taskMap = new Map<Id,Task>(ts.tasks);
        //System.debug( 'ts.tasks:' + ts.tasks );
        //System.debug( 'taskMap:' + taskMap.size() + ':' + taskMap.keySet() );
        Task t = null;
        
        //-- there are tasks that we did not create as they are from events.
        Map<Id,VFC_CalendarEvent> calendarEventMap = new Map<ID,VFC_CalendarEvent>();
        for( VFC_CalendarEvent evt : results ){
            //System.debug( 'calendarEventMap.events[' + evt.Id + ']:' + evt.Title );
            calendarEventMap.put( evt.Id, evt );
        }
        
        VFC_CalendarEvent evt2 = null;
        for( Task t2 : ts.tasksThisMonth ){
            evt2 = calendarEventMap.get( t2.Id );
            //System.debug( 'checking for [' + t2.id + ']' );
            System.assertEquals( t2.Subject, evt2.Title, 'testTasksForMonthFindTasks title does not match[' + t2.id + ']' );
        }
    }
    
    static testMethod void testInvalidEventsRangeStrings(){
        TestSettings ts = new TestSettings();
        ts.setupDefault();
        ts.finalize();
        
        VFC_CalendarEvent[] results = VFC_Calendar.getEventsRange( ts.startMonthStr, null );
        System.assertEquals( 0, results.size(), 'invalidEndDate must not give any results' );
        
        results = VFC_Calendar.getEventsRange( null, ts.endMonthStr );
        System.assertEquals( 0, results.size(), 'invalidStartDate must not give any results' );
        
        results = VFC_Calendar.getEventsRange( ts.startMonthStr, ts.endMonthStr );
        System.assertNotEquals( 0, results.size(), 'valid start and end date must give results' );
    }
    
    static testMethod void testMonthMustFindCorrectEvents(){
        TestSettings ts = new TestSettings();
        ts.setupDefault();
        ts.finalize();
        
        VFC_CalendarEvent[] results = VFC_Calendar.getEventsRange( ts.startMonthStr, ts.endMonthStr );
        Integer expectedCount = ts.numEventsThisMonth + ts.numTasksThisMonth; //-- events create tasks too..
        //System.assertEquals( expectedCount, results.size(), 'number of events must be all events and tasks for the month:\n' + results + '\n' );
        
        Map<Id,VFC_CalendarEvent> resultMap = new Map<Id,VFC_CalendarEvent>();
        for( VFC_CalendarEvent evt :results ){
            resultMap.put( evt.id, evt );
        }
        
        Map<Id,Task> taskMap = new Map<Id,Task>( ts.tasks );
        Map<Id,Event> eventMap = new Map<Id,Event>( ts.events );
        
        Task t = null;
        Event e = null;
        for( Task t2 : ts.tasksThisMonth ){
            System.assert( resultMap.containsKey( t2.id ), 'task not found' );
        }
        for( Event e2 : ts.eventsThisMonth ){
            System.assert( resultMap.containsKey( e2.id ), 'event not found' );
        }
    }
}