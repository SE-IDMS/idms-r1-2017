@isTest
private class ConnectTargetDashboardContCountry_Test{
    static testMethod void TargetDashboardCountry() {
    
       User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('conneEF');
       User runAsUser2 = Utils_TestMethods_Connect.createStandardConnectUser('conEEF');
        User runAsUser3 = Utils_TestMethods_Connect.createStandardConnectUser('conEEEF');
        System.runAs(runAsUser){
          Initiatives__c inti=new Initiatives__c(Name='initiative', Transformation__c = Label.ConnectTransPeople);
         insert inti;
          Initiatives__c inti1=new Initiatives__c(Name='initiative', Transformation__c = Label.ConnectTransCustomer);
         insert inti1;
          Initiatives__c inti2=new Initiatives__c(Name='initiative', Transformation__c = Label.ConnectTransEfficiency);
         insert inti2;
         Initiatives__c inti3=new Initiatives__c(Name='initiative', Transformation__c = Label.ConnectTransEverywhere);
         insert inti3;
         
           Project_NCP__c cp=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',User__c = runasuser.id, Program_Leader_2__c = runasuser2.id, Program_Leader_3__c = runasuser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp;          
         Project_NCP__c cp1=new Project_NCP__c(Program__c=inti1.id,Program_Name__c='cp2',User__c = runasuser2.id, Program_Leader_2__c = runasuser.id, Program_Leader_3__c = runasuser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp1;
         Project_NCP__c cp2=new Project_NCP__c(Program__c=inti2.id,Program_Name__c='cp3',User__c = runasuser2.id, Program_Leader_2__c = runasuser.id, Program_Leader_3__c = runasuser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp2;
         Project_NCP__c cp3=new Project_NCP__c(Program__c=inti3.id,Program_Name__c='cp4',User__c = runasuser2.id, Program_Leader_2__c = runasuser.id, Program_Leader_3__c = runasuser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp3;
                
         Global_KPI_Targets__c GKPIT=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp.id, Transformation__c = Label.ConnectTransPeople, CountryPickList__c='FR-France', KPI_Type__c = 'Country KPI');
         insert GKPIT; 
         Cascading_KPI_Target__c EKPI = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email,CountryPickList__c='FR-France');
         insert EKPI;
         
         Global_KPI_Targets__c GKPIT1=new Global_KPI_Targets__c(KPI_Name__c='testKPI1',KPI_Acronym__c = 'GPEC',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp1.id, Transformation__c = Label.ConnectTransCustomer,CountryPickList__c='FR-France',KPI_Type__c = 'Country KPI');
         insert GKPIT1;
         Cascading_KPI_Target__c EKPI1 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT1.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email,CountryPickList__c='FR-France' );
         insert EKPI1;
         Growth_Priority_Execution_Target__c GP=new Growth_Priority_Execution_Target__c(Entity_Target__c = EKPI1.id,Growth_Priority__c = 'test');
         
         insert GP;
        
          Global_KPI_Targets__c GKPIT4=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,CountryPickList__c='FR-France',KPI_Type__c = 'Country KPI');
         insert GKPIT4;
          Global_KPI_Targets__c GKPIT5=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,CountryPickList__c='FR-France',KPI_Type__c = 'Country KPI');
         insert GKPIT5;
         Global_KPI_Targets__c GKPIT6=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp2.id, Transformation__c = Label.ConnectTransEfficiency,CountryPickList__c='FR-France');
         insert GKPIT6;
         Cascading_KPI_Target__c EKPI6 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT6.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email,CountryPickList__c='FR-France' );
         insert EKPI6;
         
         Global_KPI_Targets__c GKPIT7=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email, Transformation__c = Label.ConnectTransNone,Global_Business__c = 'Industry',KPI_Type__c = 'Global KPI');
         insert GKPIT7;
         Cascading_KPI_Target__c EKPI7 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT7.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'Industry');
         insert EKPI7;
         
         Global_KPI_Targets__c GKPIT8=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',KPI_Acronym__c = 'DMLC',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email, Transformation__c = Label.ConnectTransNone,KPI_Type__c = 'Country KPI',CountryPickList__c='FR-France',Year__c = Label.ConnectYear);
         insert GKPIT8;
         Cascading_KPI_Target__c EKPI9 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT8.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, CountryPickList__c = 'FR-France');
         insert EKPI9;
         Global_KPI_Targets__c GKPIT9=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email, Transformation__c = Label.ConnectTransNone,KPI_Type__c = 'Country KPI',CountryPickList__c='FR-France',Year__c = Label.ConnectYear);
         insert GKPIT9;
         Cascading_KPI_Target__c EKPI10 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT9.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, CountryPickList__c = 'FR-France');
         insert EKPI10;
          
         
         
           PageReference pageRef = Page.CountryTargetDashboard;
           Test.setCurrentPage(pageRef);
           ConnectTargetDashboardCountryController controller = new ConnectTargetDashboardCountryController ();
       
       
    controller.Country = 'Global';       
    controller.getFilterList();
    controller.FilterChange();
    controller.RenderPDF();
    Controller.getConnect();
    controller.getConnectEverywhere();
    controller.getConnectToCustomers();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    controller.getFilterQuarter();
    controller.getFilterYear();
    controller.YearChange();
    controller.getGPE();
    
 
    
    
    controller.Country = 'FR-France';
    controller.getFilterList();
    controller.RenderPDF();
    Controller.getConnect();
    controller.FilterChange();
    controller.getConnectEverywhere();
    controller.getConnectToCustomers();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    controller.getFilterQuarter();
    controller.getFilterYear();
    controller.YearChange();
    controller.getGPE();
    
    controller.Country = 'FR-France';
    controller.Quarter = 'Q2';
    controller.getFilterList();
    controller.RenderPDF();
    Controller.getConnect();
    controller.FilterChange();
    controller.getConnectEverywhere();
    controller.getConnectToCustomers();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    controller.getFilterQuarter();
    controller.getFilterYear();
    controller.YearChange();
    controller.getGPE();
    
    controller.Country = 'FR-France';
    controller.Quarter = 'Q3';
    controller.getFilterList();
    controller.RenderPDF();
    Controller.getConnect();
    controller.FilterChange();
    controller.getConnectEverywhere();
    controller.getConnectToCustomers();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    controller.getFilterQuarter();
    controller.getFilterYear();
    controller.YearChange();
    controller.getGPE();
    
     controller.Country = 'FR-France';
    controller.Quarter = 'Q4';
    controller.getFilterList();
    controller.RenderPDF();
    Controller.getConnect();
    controller.FilterChange();
    controller.getConnectEverywhere();
    controller.getConnectToCustomers();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    controller.getFilterQuarter();
    controller.getFilterYear();
    controller.YearChange();
      controller.getGPE();
    
     
    }
    }
    }