global class SRV_MyFs_SSOHandler implements Auth.SamlJitHandler {
    
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
                        String federationIdentifier, Map<String, String> attributes, String assertion) {
        return null;
    }

    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
                            String federationIdentifier, Map<String, String> attributes, String assertion) {
        
        USerSAMLAssertion__c samlAssertion = new USerSAMLAssertion__c(RelatedUser__c = userId,
                                                                      UserId__c = userId,
                                                                      Assertion__c = assertion);
        
        Database.upsertResult uspertResult = Database.upsert(samlAssertion, USerSAMLAssertion__c.UserId__c); 
        
        system.debug('uspertResult = ' + uspertResult);   
    }    
}