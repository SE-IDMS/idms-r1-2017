public class VFC_CISCompetencyDelete
    {
    Id CisCompetencyId;
    Id UserId;
    list<CIS_ServiceCenter__c> lstServiceCenter = new list<CIS_ServiceCenter__c>();
    list<CIS_Competency__c> lstCISCompetency = new list<CIS_Competency__c>();
    string retURL;
    public VFC_CISCompetencyDelete(ApexPages.StandardController controller) {   
    }
    
    public void checkOwnerandUser(){
    CisCompetencyId=ApexPages.currentPage().getParameters().get('id');
    if(CisCompetencyId!=null)
        {       
            lstCISCompetency=[select id,CIS_ServiceCenter__c from CIS_Competency__c where id=:CisCompetencyId];
        }
    if(lstCISCompetency.size()>0)
        {
            lstServiceCenter=[select id,OwnerId from CIS_ServiceCenter__c where id=:lstCISCompetency[0].CIS_ServiceCenter__c];
        }
    UserId=UserInfo.getUserId();
    if(lstServiceCenter.size()>0){    
        if(UserId==lstServiceCenter[0].OwnerId){
            delete(lstCISCompetency);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,'The CIS Competency has been successfully deleted');
            ApexPages.addMessage(myMsg); 
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Sorry you cannot delete this record as you are not the Owner of this record.Please contact admin.');
            ApexPages.addMessage(myMsg); 
        }
      }
    }
    
    public pagereference gobacktoHome(){
    retURL=ApexPages.currentPage().getParameters().get('retURL');
    PageReference pageRef= new PageReference(retURL);
    pageRef.setRedirect(True);
    return pageRef;
        }
    }