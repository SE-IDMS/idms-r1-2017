/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: VF Page Controller                                  *
* Page: Fielo_InvoiceUpload2                                *
* Created Date: 21/08/2014                                  *
************************************************************/

public class Fielo_InvoiceUploadController {
    
    public static String hasAttach {get;set;}
    
    public String randomAttachmentName {get;set;}
    public Boolean isFirstTime {get;set;}
    public Boolean isSuccessful {get;set;}
    public Fielo_Invoice__c invoice {get;set;}
    public Boolean confirmLicence {get;set;}
    public String errorMessage {get;set;}
    
    private Id memId;
    
    //Contructor 
    public Fielo_InvoiceUploadController(){
        errorMessage = null;
        confirmLicence = false;
        isFirstTime = false;
        memId = FieloEE.MemberUtil.getMemberId();
        randomAttachmentName = String.valueOf(System.currentTimeMillis()) + String.valueOf(Math.Random());
        invoice = new Fielo_Invoice__c();
        hasAttach = 'false';
    }
    
    public void doSubmit(){
    
        system.debug('TOKEN hasAttach: ' + hasAttach);

        errorMessage = null;
        List<Document> documents = [SELECT Id, Name, Body FROM Document WHERE Name LIKE: randomAttachmentName + '%'];
        system.debug('cant. docs: ' + documents.size());
        /*if(isFirstTime){
            if(documents.size() == 0){
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Warning, Label.Fielo_InvoiceUploadControllerLincenceMand));
                return null;
            }else{
                 //create attachments and delete documents
                for(Document doc : documents){
                    String fileExtension = '';
                    if(doc.Name.contains('.')){
                        fileExtension = doc.Name.substring(doc.Name.lastIndexOf('.'), doc.Name.length() );
                    }
                    Attachment att = new Attachment();
                    att.Name = 'Licence';
                    att.Body = doc.body;
                    att.ParentId = memId;
                    insert att;
                }
                delete documents;
            }
            confirmLicence = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.Fielo_InvoiceUploadControllerUploadSuccess));
            return null;
        }else{*/
        if(documents.size() == 0){
            errorMessage = Label.Fielo_InvoiceUploadControllerInvoiceMand;
            //ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Warning, Label.Fielo_InvoiceUploadControllerInvoiceMand));
            return;
        }

        invoice.F_Member__c = memId;
        
        try{
            insert invoice;
        }catch(exception e){
            ApexPages.addMessage(new ApexPages.Message ( ApexPages.Severity.ERROR,e.getmessage()));
            return;
        }

        //create attachments and delete documents
        for(Document doc : documents){
            string fileExtension = '';
            if (doc.Name.contains('.')){
                fileExtension = doc.Name.substring( doc.Name.lastIndexOf('.'), doc.Name.length() );
            }
            Attachment att = new Attachment();
            att.Name = doc.Name.replace(randomAttachmentName, '');
            att.Body = doc.body;
            att.ParentId = invoice.Id;
            insert att;
        }
        delete documents;
        
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.Fielo_InvoiceUploadControllerInvoiceSuccess));
        
        isSuccessful = true;
        
        /*}*/
        //return null;
    }
        
    public PageReference doContinue(){
        return Fielo_Utilities.getMenuReference('Upload_Invoice');
    }
}