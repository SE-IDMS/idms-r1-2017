/**
12-June-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for PKGTemplateAfterUpdate 
 */
@isTest
private class AP_TemplateTriggerUtils_TEST {
  static testMethod void myUnitTest() {
        PackageTemplate__c pkgTemplate = Utils_TestMethods.createPkgTemplate();
        pkgTemplate.FreeOfCost__c = true;
        insert pkgTemplate;
        
        Account acct = Utils_TestMethods.createAccount();
        insert acct;
        Contract contract = Utils_TestMethods.createContract(Acct.Id,null);
        insert contract;
        contract.status = 'activated';
        update contract;
        Package__c pkg = Utils_TestMethods.createPackage(contract.Id , pkgTemplate.Id);
        insert pkg;
        Case c = Utils_TestMethods.createCase(Acct.Id,null,'Open');
        c.RelatedContract__c = contract.Id;
        insert c;
        
        pkgTemplate.FreeOfCost__c = false;
        update pkgTemplate;
        
        pkgTemplate.FreeOfCost__c = true;
        update pkgTemplate;
    }
}