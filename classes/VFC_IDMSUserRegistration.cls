/**
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*
*   Description: class used for as controller for user registeration vf pages. 
*   Modified by  Mohammad Naved | Date  "2017/01/19"   | Version V01   |  Ticket #  or   BR #  |  Purpose     
**/
public with sharing class VFC_IDMSUserRegistration{
    public String email{get; set;}
    public String MobilePhone{get; set;}
    public String FirstName{get; set;}
    public String LastName{get; set;}
    public String IDMS_Email_opt_in{get; set;}
    public String Country{get; set;}
    public String Street{get; set;}
    public String City{get; set;}
    public String PostalCode{get; set;}
    public String State{get; set;}
    public String IDMS_County{get; set;}
    public String IDMS_POBox{get; set;}
    public String IDMS_AdditionalAddress{get; set;}
    public String CompanyName{get; set;}
    public String Company_Address1{get; set;}
    public String Company_City{get; set;}
    public String Company_Postal_Code{get; set;}
    public String Company_State{get; set;}
    public String CompanyPoBox{get; set;}
    public String Company_Country{get; set;}
    public String Company_Address2{get; set;}
    public String ClassLevel1{get; set;}
    public String ClassLevel2{get; set;}
    public String MarketSegment{get; set;}
    public String MarketSubSegmen{get; set;}
    public String Phone{get; set;}
    public String Job_Title{get; set;}
    public String Job_Function{get; set;}
    public String JobDescription{get; set;}
    public String CompanyMarketServed{get; set;}
    public String CompanyNbrEmployees{get; set;}
    public boolean CompanyHeadquarters{get; set;}
    public String AnnualRevenue{get; set;}
    public String TaxIdentificationNumber{get; set;}
    public String MiddleName{get; set;}
    public String Company_Website{get; set;}
    public String Salutation{get; set;}
    public String Department{get; set;}
    public String Suffix{get; set;}
    public String Fax{get; set;}
    public String password{get; set;}
    public String cpassword{get; set;}
    public String reg1Check{get; set;}
    public Boolean checked{get;set;}
    public Boolean checkedEmailOpt{get;set;}
    public String regCheckEmailOpt{get; set;}
    public Boolean smsIdentity{get;set;}
    public String userId{get;set;}
    public String redirectToApp{get;set;}
    public String mobCntryCode{get;set;}
    public String workCntryCode{get;set;}
    public User usr1{get;set;}
    public String companyCounty{get;set;}
    public String ErrorMsgCode{get;set;}
    public string appid{get; set;}
    public string appDes{get; set;}
    public string appDesFooter{get; set;}
    public string appDesFooter1{get; set;}
    public string language{get; set;}
    public string appNameToDisplay{get; set;}
    public string appLogo{get; set;}
    public string appTabName{get;set;}
    public string appTabLogo{get; set;}
    public string appAndSE{get; set;}
    public string appBgrdImg{get; set;}    
    public string appMobileFooterImg{get; set;}
    public string registrationSource{get; set;}
    public string redirectToPage{get; set;}
    public string applicationId{get; set;}
    public Static Final String secretKey = Label.CLNOV16IDMS191;
    public Static Final String googleApi = Label.CLNOV16IDMS192;
    public String gridOut{get; set;}
    public List<SelectOption> statusOptionsCountry{get;set;}
    public List<SelectOption> statusOptionsState{get;set;}
    public List<SelectOption> statusOptionsCompState{get;set;}
    public List<SelectOption> statusOptionsCompAdd{get;set;}
    public List<SelectOption> statusOptionsCompIam1{get;set;}
    public List<SelectOption> statusOptionsCompIam2{get;set;}
    public List<SelectOption> statusOptionsCompIS{get;set;}
    public List<SelectOption> statusOptionsCompISS{get;set;}
    public List<SelectOption> statusOptionsCompJT{get;set;}
    public List<SelectOption> statusOptionsCompJF{get;set;}
    public List<SelectOption> statusOptionsCompIIS{get;set;}
    public List<SelectOption> statusOptionsST{get;set;}
    public Boolean hideAllFieldsHome{get;set;}
    public Boolean hideAllFieldsWork{get;set;}
    public Boolean showCaptcha{get;set;}
    public Boolean enterRecaptcha               = false;
    public Boolean enterRecaptchcorrect = false;
    private static String baseUrl               = Label.CLJUN16IDMS14;
    private static String privateKey    = Label.CLJUN16IDMS15;
    public boolean isLogoPresent{get;Set;}
    public String cookievalueURL{get; set;}
    
    
    public String publicKey { 
        get { return Label.CLJUN16IDMS16; }
    }
    
    // Create properties for the non-VF component input fields generated
    // by the reCAPTCHA JavaScript.
    public String challenge { 
        get {
            return ApexPages.currentPage().getParameters().get(Label.CLJUN16IDMS27);
        }
    }
    public String response  { 
        get {
            return ApexPages.currentPage().getParameters().get(Label.CLJUN16IDMS28);
        }
    }
    
    // Whether the submission has passed reCAPTCHA validation or not
    public Boolean verified { get; private set; }
    
    //added variables to show pages
    public boolean showHome{get; set;}
    public boolean showWork{get; set;}
    public Boolean isPwdIncluded{get;set;}
    public Boolean iscmpny{get;set;}
    public Boolean isconfirm{get;set;}
    public Boolean isHomeConfirm{get;set;}
    public Boolean isSession{get;set;}
    public string sessionURL{get;set;}
    public Boolean showConfirm{get;set;}
    public Boolean PwdIncluded{get;set;} // ApexPages.currentPage().getParameters().get('PwdIncluded');
    public string context;
    
    //Get invitation Id from url.
    public string invitationId = ApexPages.currentPage().getParameters().get('InvitationId');
    
    //Get application langauge from url
    public string appLanguage = ApexPages.currentPage().getParameters().get('lang');
    
    //controller user for fetching data from custom setting
    public VFC_IDMSUserRegistration(){
        smsIdentity                     =false;
        isPwdIncluded           =false;
        //added images if appid is present in url
        appBgrdImg                      = Label.CLNOV16IDMS130;
        appAndSE                        = Label.CLNOV16IDMS131;
        appLogo                         = Label.CLNOV16IDMS132;
        appTabName                      = Label.CLNOV16IDMS128;
        appTabLogo                      = Label.CLFEB17IDMS004;
        appid                           = ApexPages.currentPage().getParameters().get('app');
        usr1                            = new User();
        hideAllFieldsHome       = true;
        hideAllFieldsWork       = true;
        isSession                       = false;
        isLogoPresent=false;
        if(appid != null){
            User userSession;
            userSession = [SELECT id,usertype FROM User WHERE id =:UserInfo.getUserId()];
            system.debug('userSession ------'+userSession.usertype);
            if(userSession != null && userSession.usertype != 'GUEST'){
                isSession                       = true;
                hideAllFieldsHome       = false;
                hideAllFieldsWork       = false;
                sessionURL                      =Label.CLQ316IDMS099+appid;
            }
        }
        
        //code for getting context and app details 
        if(string.isnotblank(appid)){    
            IDMSApplicationMapping__c appMap;
            appMap = IDMSApplicationMapping__c.getInstance(appid);
            if(appMap != null){
                context                                 = (String)appMap.context__c;
                registrationSource                      = (String)appMap.get('Name');
                appNameToDisplay                        = (String)appMap.get('AppName__c');
                isPwdIncluded                           = (boolean)appMap.PwInRegPage__c;
                appDes                                  = (String)appMap.get('AppDescription__c');
                appDesFooter                            = (String)appMap.AppTextAndUrl__c;
                appLogo                                 = (String)appMap.get('AppLogo__c');
                appAndSE                                = (String)appMap.AppBrandOrLogo__c;
                appBgrdImg                              = (String)appMap.AppBackgroundVisual__c;
                appMobileFooterImg                      = (String)appMap.get('AppMobileFooterImage__c');
                appDesFooter1                           = (String)appMap.get('AppDescriptionFooter1__c');
                applicationId                           = (String)appMap.get('AppId__c');
                // added to show application specific TabName as in BFOID-523
                    appTabName = (String)appMap.get('AppTabName__c');
                    if(String.isBlank(AppTabName)){
                        AppTabName     = Label.CLNOV16IDMS128;
                    }
                   // added to show application specific TabLogo as in BFOID-523
                    appTabLogo         = (String)appMap.get('AppTabLogo__c');
                    if(String.isBlank(AppTabLogo)){
                        AppTabLogo     = Label.CLFEB17IDMS004;
                    } 
                //language                              = (String)appMap.get('Language__c');
            }
        }
        
        if(string.isnotblank(appAndSe)){
        isLogoPresent=true;
        }
        else{
        appAndSE                        = Label.CLR117IDMS004;
        }
         
        
        regCheckEmailOpt = Label.CLJUN16IDMS07;
        if(context!=null &&(context.equalsIgnoreCase('home')||context.equalsIgnoreCase('@home')) && appid!=null && !isSession){
            showHome                    = true;
            hideAllFieldsHome   = false;
            redirectToPage              = '/userregistrationhome';
        }
        
        if(context!=null && (context.equalsIgnoreCase('work') || context.equalsIgnoreCase('@work')) && appid!=null && !isSession){
            showWork                    = true;
            hideAllFieldsWork   = false;
            redirectToPage              = '/userregistrationwork';
        }
        
     /*   if(PwdIncluded !=null && PwdIncluded.equalsIgnoreCase('true')){
            isPwdIncluded = true;
        }*/
        
        //get Login URL from cookie
         Cookie urlCookie= ApexPages.currentPage().getCookies().get('LoginURLCookie'); 
         cookievalueURL=String.valueOf(urlCookie.getValue());
         if(String.isBlank(cookievalueURL)){
         cookievalueURL=Label.CLNOV16IDMS057+applicationId;
         }
        
        //country picklist
        Schema.DescribeFieldResult statusFieldCountry = Schema.User.Countrycode.getDescribe();
        statusOptionsCountry=new list<SelectOption>();
        statusOptionsCountry.add(new SelectOption(Label.CLJUN16IDMS06,Label.CLJUN16IDMS05));
        for (Schema.Picklistentry picklistCountry : statusFieldCountry.getPicklistValues())
        {
            if(picklistCountry.getValue()==Label.CLJUN16IDMS06){
                continue;
            }
            else{
                statusOptionsCountry.add(new SelectOption(picklistCountry.getValue(),picklistCountry.getLabel()));
            }
        }
        
        //state picklist
        Schema.DescribeFieldResult statusFieldState = Schema.User.Statecode.getDescribe();
        statusOptionsState = new list<SelectOption>();
        for (Schema.Picklistentry picklistState : statusFieldState.getPicklistValues())
        {
            if(statusOptionsState.size()>=1000){
                continue;
            }  
            statusOptionsState.add(new SelectOption(picklistState.getValue(),picklistState.getLabel()));
            
        }
        
        // Salutation picklist
        Schema.DescribeFieldResult statusFieldST = Schema.User.IDMSSalutation__c.getDescribe();
        statusOptionsST=new list<SelectOption>();
        for (Schema.Picklistentry picklistST : statusFieldST.getPicklistValues())
        {
            if(statusOptionsST.size()>=1000){
                continue;
            }  
            statusOptionsST.add(new SelectOption(picklistST.getValue(),picklistST.getLabel()));
        }    
        
        //captcha code to switch on or off
        this.verified                   = false;
        String toShowCaptcha    = Label.CLJUL16IDMS6; //should get from custom label
        if(toShowCaptcha.equalsIgnoreCase('true')){
            showCaptcha = true;
        } 
        else{
            showCaptcha = false;
        }
        
        //code for terms condition mapping with app
        String termUrl;
        IdmsTermsAndConditionMapping__c termMap;
        termMap = IdmsTermsAndConditionMapping__c.getInstance(registrationSource);
        if(termMap != null){
            termUrl = (String)termMap.get('url__c');
            if(termMap == null){
                termUrl = Label.CLJUN16IDMS118;
            }
        }
        else{
            termUrl             = Label.CLJUN16IDMS118;
        }
        
        String url      = '<a href='+ termUrl + ' target=_blank>Terms & Conditions</a>';
        gridOut         = 'color:white;background-color:green;background-image:none;;float:left;';
        reg1Check       = Label.CLJUN16IDMS19+' '+ +url+' '+ Label.CLJUN16IDMS20;    
        
        //Pre populate values from Send Invitation 
        if(invitationId != null){
            IDMS_Send_Invitation__c registerInvite = [select ID, Email__c,Mobile_Phone__c,FirstName__c,LastName__c,Country__c ,Street__c,City__c,IDMSCompanyCounty__c,
                                                      PostalCode__c,State__c,IDMS_County__c,IDMS_POBox__c,IDMS_AdditionalAddress__c ,CompanyName__c,
                                                      Company_Address1__c,Company_City__c,Company_Postal_Code__c,CompanyHeadquarters__c, Fax__c, Company_State__c,Company_Country__c,
                                                      Company_Address2__c,IDMSClassLevel1__c ,IDMSClassLevel2__c ,IDMSMarketSegment__c,IDMSMarketSubSegment__c,
                                                      IDMSWorkPhone__c,Job_Title__c,Job_Function__c,IDMSJobDescription__c,IDMSCompanyMarketServed__c,
                                                      IDMSTaxIdentificationNumber__c,IDMSMiddleName__c,Company_Website__c,IDMSSalutation__c,Department__c,IDMSSuffix__c,
                                                      IDMS_User_Context__c,IDMS_Registration_Source__c,CompanyPoBox__c,IDMSCompanyPoBox__c, IDMS_PreferredLanguage__c,IDMS_Email_opt_in__c from IDMS_Send_Invitation__c where ID =:invitationId];
            email                                       = registerInvite.Email__c;       
            MobilePhone                         = registerInvite.Mobile_Phone__c;  
            FirstName                           = registerInvite.FirstName__c; 
            LastName                            = registerInvite.LastName__c;
            IDMS_Email_opt_in           = registerInvite.IDMS_Email_opt_in__c;
            Country                             = registerInvite.Country__c;
            Street                                      = registerInvite.Street__c;
            City                                        = registerInvite.City__c;
            PostalCode                          = registerInvite.PostalCode__c;
            State                                       = registerInvite.State__c;
            IDMS_County                         = registerInvite.IDMS_County__c;
            IDMS_POBox                          = registerInvite.IDMS_POBox__c;
            IDMS_AdditionalAddress      = registerInvite.IDMS_AdditionalAddress__c;
            CompanyName                         = registerInvite.CompanyName__c;
            Company_Address1            = registerInvite.Company_Address1__c;
            Company_City                        = registerInvite.Company_City__c;
            Company_Postal_Code         = registerInvite.Company_Postal_Code__c;
            Company_State                       = registerInvite.Company_State__c;
            CompanyPoBox                        = registerInvite.IDMSCompanyPoBox__c;
            Company_Country             = registerInvite.Company_Country__c;
            Company_Address2            = registerInvite.Company_Address2__c;
            usr1.IDMSClassLevel1__c = registerInvite.IDMSClassLevel1__c;
            usr1.IDMSClassLevel2__c = registerInvite.IDMSClassLevel2__c;
            companyCounty                       = registerInvite.IDMSCompanyCounty__c;
            usr1.IDMSMarketSegment__c           = registerInvite.IDMSMarketSegment__c;
            usr1.IDMSMarketSubSegment__c        = registerInvite.IDMSMarketSubSegment__c;
            if(string.isnotblank(registerInvite.IDMSWorkPhone__c)){
                workCntryCode = registerInvite.IDMSWorkPhone__c.substring(0,3);
                Phone           = registerInvite.IDMSWorkPhone__c.substring(3,registerInvite.IDMSWorkPhone__c.length());
            }
            Job_Title           = registerInvite.Job_Title__c;
            Job_Function        = registerInvite.Job_Function__c;
            JobDescription      = registerInvite.IDMSJobDescription__c;
            usr1.IDMSCompanyMarketServed__c = registerInvite.IDMSCompanyMarketServed__c;
            
            TaxIdentificationNumber = registerInvite.IDMSTaxIdentificationNumber__c;
            MiddleName                          = registerInvite.IDMSMiddleName__c;
            Company_Website             = registerInvite.Company_Website__c;
            Salutation                          = registerInvite.IDMSSalutation__c;
            Department                          = registerInvite.Department__c;
            Suffix                                      = registerInvite.IDMSSuffix__c;
            Fax                                         = registerInvite.Fax__c;                                
        }
    }
    
    //captcha code verification method
    public PageReference verify() {
        enterRecaptcha=true;
        System.debug('reCAPTCHA verification attempt');
        // On first page load, form is empty, so no request to make yet
        if ( challenge == null || response == null ) { 
            System.debug('reCAPTCHA verification attempt with empty form');
            return null; 
        }
        
        HttpResponse r = makeRequest(baseUrl,
                                     'privatekey=' + privateKey + 
                                     '&remoteip='  + remoteHost + 
                                     '&challenge=' + challenge +
                                     '&response='  + response
                                    );
        if ( r!= null ) {
            this.verified = (r.getBody().startsWithIgnoreCase('true'));
        }
        if(this.verified) {
            // If they pass verification
            // Or simply return a PageReference to the "next" page
            enterRecaptchcorrect = true;
            return null;
        }
        else {
            // stay on page to re-try reCAPTCHA
            return null; 
        }
    }
    
    public PageReference reset() {
        return null; 
    }   
    
    /* Private helper methods */
    // reques for captcha
    private static HttpResponse makeRequest(string url, string body)  {
        HttpResponse response   = null;
        HttpRequest req                 = new HttpRequest();   
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody (body);
        try {
            Http http                   = new Http();
            response                    = http.send(req);
        } catch(System.Exception e) {
            System.debug('ERROR: ' + e);
        }
        return response;
    }   
    
    //method for calling captcha
    private String remoteHost { 
        get { 
            String ret = '127.0.0.1';
            // also could use x-original-remote-host 
            Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
            if (hdrs.get('x-original-remote-addr')!= null)
                ret =  hdrs.get('x-original-remote-addr');
            else if (hdrs.get('X-Salesforce-SIP')!= null)
                ret =  hdrs.get('X-Salesforce-SIP');
            return ret;
        }
    }
    
    // method to show company page
    public boolean nextToConfirm(){
        iscmpny         = false;
        showWork        = false;
        showConfirm     = true;
        return null;
    }
    
    //all the validation for @work before moving to company page 
    public boolean nextToCompany(){
        //phone validation check
        if(MobilePhone == '' ){
            MobilePhone = null;
        }
        
        if(String.isNotBlank(email)){
            //Code change to check inactive User by Mohammad Naved on 19/Jan/2016
            UTILS_IDMSCheckPreviousUser.checkPreviousUser(email);
            //validation check if email already exist
            List<User> existingContacts = [SELECT id, email FROM User WHERE email = :email];
            String username = email + Label.CLJUN16IDMS71;
            List<User> existingusername = [SELECT id, email FROM User WHERE username= :username];
            //check for UIMS table
            List<UIMS_User__c> existingUIMSEmail = [SELECT id FROM UIMS_User__c WHERE EMail__c = :email];
            if(existingContacts.size() > 0 || existingusername.size() > 0 || existingUIMSEmail.size()>0 )
            {
                ErrorMsgCode = 'error.email';
                return null;               
            }   
        }
        
        if( String.isNotBlank(MobilePhone) && String.isBlank(email)){
            if(MobilePhone.startsWith('+')){
                MobilePhone=MobilePhone.replace('+', '00');}
            List<User> existingMobile = [SELECT id FROM User WHERE MobilePhone= :MobilePhone];
            //UIMS table check
            List<UIMS_User__c> existingMobileUIMS = [SELECT id FROM UIMS_User__c WHERE phoneId__c= :MobilePhone];
            if(existingMobile.size() > 0 || existingMobileUIMS.size() > 0 ){
                ErrorMsgCode='error.mobile';
                return null;
            } 
            smsIdentity=true;
        }
        //password validation check
        
        if(!isPwdIncluded){
            password = null;
            cpassword = null; 
        }
        getCompanyPicklist();    
        
        iscmpny         = true;
        showWork        = false;
        return null;
    }
    
    //get all company picklist values
    public void getCompanyPicklist(){
        //picklist values of company fields
        usr1.countrycode        = Country;
        //added for statecode error
        usr1.statecode          = null;
        //Company Address (State/Province) picklist
        Schema.DescribeFieldResult statusFieldCompState = Schema.User.Statecode.getDescribe();
        statusOptionsCompState                                                  = new list<SelectOption>();
        for (Schema.Picklistentry picklistCompState : statusFieldCompState.getPicklistValues())
        {
            if(statusOptionsCompState.size() >= 1000){
                continue;
            } 
            statusOptionsCompState.add(new SelectOption(picklistCompState.getValue(),picklistCompState.getLabel()));
        }
        
        //Company Address (Country) picklist
        Schema.DescribeFieldResult statusFieldCompAdd   = Schema.User.countrycode.getDescribe();
        statusOptionsCompAdd                                                    = new list<SelectOption>();
        statusOptionsCompAdd.add(new SelectOption(Label.CLJUN16IDMS06,Label.CLJUN16IDMS05));
        for (Schema.Picklistentry picklistCompAdd : statusFieldCompAdd.getPicklistValues())
        {
            if(picklistCompAdd.getValue() == Label.CLJUN16IDMS06){
                continue;
            }  
            statusOptionsCompAdd.add(new SelectOption(picklistCompAdd.getValue(),picklistCompAdd.getLabel()));
        }
        
        //I am a 1 picklist
        Schema.DescribeFieldResult statusFieldCompIam1 = Schema.User.IDMSClassLevel1__c.getDescribe();
        statusOptionsCompIam1                                                   = new list<SelectOption>();
        for (Schema.Picklistentry picklistCompIam1 : statusFieldCompIam1.getPicklistValues())
        {
            statusOptionsCompIam1.add(new SelectOption(picklistCompIam1.getValue(),picklistCompIam1.getLabel()));   
        }  
        
        //I am a 2 picklist
        Schema.DescribeFieldResult statusFieldCompIam2  = Schema.User.IDMSClassLevel2__c.getDescribe();
        statusOptionsCompIam2                                                   =new list<SelectOption>();
        for (Schema.Picklistentry picklistCompIam2 : statusFieldCompIam2.getPicklistValues())
        {
            statusOptionsCompIam2.add(new SelectOption(picklistCompIam2.getValue(),picklistCompIam2.getLabel()));
        } 
        //My Industry segment picklist
        Schema.DescribeFieldResult statusFieldCompIS    = Schema.User.IDMSMarketSegment__c.getDescribe();
        statusOptionsCompIS                                                             = new list<SelectOption>();
        for (Schema.Picklistentry picklistCompIS : statusFieldCompIS.getPicklistValues())
        {
            statusOptionsCompIS.add(new SelectOption(picklistCompIS.getValue(),picklistCompIS.getLabel()));
        }  
        //My Industry sub-segment picklist
        Schema.DescribeFieldResult statusFieldCompISS   = Schema.User.IDMSMarketSubSegment__c.getDescribe();
        statusOptionsCompISS                                                    = new list<SelectOption>();
        
        for (Schema.Picklistentry picklistCompISS : statusFieldCompISS.getPicklistValues())
        {
            statusOptionsCompISS.add(new SelectOption(picklistCompISS.getValue(),picklistCompISS.getLabel()));   
        }  
        
        //Job Title picklist
        Schema.DescribeFieldResult statusFieldCompJT    = Schema.User.Job_Title__c.getDescribe();
        statusOptionsCompJT                                                             = new list<SelectOption>();
        for (Schema.Picklistentry picklistCompJT : statusFieldCompJT.getPicklistValues())
        {
            statusOptionsCompJT.add(new SelectOption(picklistCompJT.getValue(),picklistCompJT.getLabel()));
        } 
        
        //Job Function picklist
        Schema.DescribeFieldResult statusFieldCompJF    = Schema.User.Job_Function__c.getDescribe();
        statusOptionsCompJF                                                             = new list<SelectOption>();
        
        for (Schema.Picklistentry picklistCompJF : statusFieldCompJF.getPicklistValues())
        {
            statusOptionsCompJF.add(new SelectOption(picklistCompJF.getValue(),picklistCompJF.getLabel()));
        } 
        //Industries I serve multiselect picklist
        Schema.DescribeFieldResult statusFieldCompIIS   = Schema.User.IDMSCompanyMarketServed__c.getDescribe();
        statusOptionsCompIIS                                                    = new list<SelectOption>();
        for (Schema.Picklistentry picklistCompIIS : statusFieldCompIIS.getPicklistValues())
        {
            statusOptionsCompIIS.add(new SelectOption(picklistCompIIS.getValue(),picklistCompIIS.getLabel()));
        } 
    }
    
    // method for creating user based on inputs form VF page
    public pageReference submit(){
        //new I am not a robot captcha verification
        if(showCaptcha==true){
            String captcha_response = ApexPages.currentPage().getParameters().get('g-recaptcha-response');
            if(String.isBlank(captcha_response) || !IDMSValidateReCaptcha.validateReCaptcha(captcha_response)){
                ErrorMsgCode='captcha.error';
                return null;
            }
        }
        
        //set email opt in
        String emailOpt='false';
        if(checkedEmailOpt==true){
            emailOpt='Y';
        }
        else{
            emailOpt='U';
        }
        
        if(isPwdIncluded==false){
            password=null;
        }
        
        if(isPwdIncluded==true){
            redirectToApp='/identity/idp/login?app='+applicationId;
        }
        String languageUser                                             = Label.CLJUN16IDMS09;
        String planguageUser                                    = Label.CLJUN16IDMS10;
        String localeUser                                               = Label.CLJUN16IDMS11;
        String timezoneUser                                             = Label.CLJUN16IDMS22;
        String currencyUser                                             = Label.CLJUN16IDMS04;
        User usr                                                                = new User();
        usr.Email                                                               = Email;
        usr.MobilePhone                                                 = MobilePhone;
        usr.FirstName                                                   = FirstName;
        usr.LastName                                                    = LastName;
        usr.Country                                                             = Country;
        usr.Street                                                              = Street;
        usr.City                                                                = City;
        usr.PostalCode                                                  = PostalCode;
        usr.State                                                               = State;
        usr.IDMS_County__c                                              = IDMS_County;
        usr.IDMS_POBox__c                                               = IDMS_POBox;
        usr.IDMS_AdditionalAddress__c                   = IDMS_AdditionalAddress;
        usr.CompanyName                                                 = CompanyName;
        usr.Company_Address1__c                                 = Company_Address1;
        usr.Company_City__c                                             = Company_City;
        usr.Company_Postal_Code__c                              = Company_Postal_Code;
        usr.Company_State__c                                    = usr1.statecode;
        usr.IDMSCompanyPoBox__c                                 = CompanyPoBox;
        usr.Company_Country__c                                  = usr1.countrycode;
        usr.Company_Address2__c                                 = Company_Address2;
        usr.IDMSClassLevel1__c                                  = usr1.IDMSClassLevel1__c;  
        usr.IDMSClassLevel2__c                                  = usr1.IDMSClassLevel2__c; 
        usr.IDMSMarketSegment__c                                = usr1.IDMSMarketSegment__c;
        usr.IDMSMarketSubSegment__c                             = usr1.IDMSMarketSubSegment__c;
        usr.Phone                                                               = Phone;
        usr.Job_Title__c                                                = Job_Title;
        usr.Job_Function__c                                             = Job_Function;
        usr.IDMSJobDescription__c                               = JobDescription;
        usr.IDMSCompanyMarketServed__c                  = usr1.IDMSCompanyMarketServed__c;
        usr.IDMSCompanyCounty__c                                = companyCounty;
        usr.IDMSTaxIdentificationNumber__c              = TaxIdentificationNumber;
        usr.IDMSMiddleName__c                                   = MiddleName;
        usr.Company_Website__c                                  = Company_Website;
        usr.IDMSSalutation__c                                   = Salutation;
        usr.Department                                                  = Department;
        usr.IDMSSuffix__c                                               = Suffix;
        usr.IDMS_User_Context__c                                = context; 
        usr.IDMS_Registration_Source__c                 = registrationSource;
        usr.IDMS_PreferredLanguage__c                   = planguageUser;
        usr.TimeZoneSidKey                                              = timezoneUser;
        usr.LocaleSidKey                                                = localeUser;
        usr.DefaultCurrencyIsoCode                              = currencyUser;
        usr.IDMS_Email_opt_in__c                                = emailOpt;
        usr.IDMSPrimaryContact__c                               = true;
        usr.IdmsAppLanguage__c                                  = appLanguage;
        
        try{
            IDMSResponseWrapper data;
            //User creation async if identity is email and real time for mobile made by Otmane Haloui:
            if(!smsIdentity){
                System.enqueueJob(new IDMSCreateUserAsync(usr,password));
            }else{
                data= IdmsUserRest.doPost(usr,password);
            }
            isconfirm   = true;
            showWork    = false;
            iscmpny             = false;
            showConfirm = false;
            isHomeConfirm       = true;
            if(smsIdentity == true){
                userId = [select id from user where id=:data.IDMSUserRecord.Id limit 1].id;
                String suffixurl;
                if(isPwdIncluded != true){
                    suffixurl   = '?app='+appid+'&id='+userId+'&key='+Label.CLQ316IDMS004;
                } else {
                    suffixurl   = '?app='+appid+'&id='+userId+'&key='+Label.CLQ316IDMS006; 
                }
                redirectToApp   = Label.CLQ316IDMS112+suffixurl;
                system.debug('redirectToApp---------'+redirectToApp);
            }
            
            return null;
        }
        
        catch(Exception e){
            System.debug(e.getMessage());
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLQ316IDMS122);
            ApexPages.addMessage(msgntChkd );
            return null;
        }             
        return null; 
    }
    
    //back from company to userdetails page
    public void backToWork(){
        iscmpny         = false;
        showWork        = true;
    }
    //back from company to userdetails page
    public void backToCompany(){
        iscmpny         = true;
        showWork        = false;
        showConfirm     = false;
        isconfirm       = false;
    }
    
    //create @home user
    public pageReference submitHome(){

        //validation for Email to check correct format
        if(String.isNotBlank(email)){
            //Code change to check inactive User by Mohammad Naved on 19/Jan/2016
             UTILS_IDMSCheckPreviousUser.checkPreviousUser(email);
            //validation check if email already exist
            String username  = email+Label.CLJUN16IDMS71;
            List<User> existingusername = [SELECT id, email FROM User WHERE username= :username];
            //check in uims table
            List<UIMS_User__c> existingUIMSEmail = [SELECT id FROM UIMS_User__c WHERE EMail__c = :email];
            if(existingusername.size() > 0 || existingUIMSEmail.size() > 0  ){
                ErrorMsgCode = 'error.email';
                return null; 
            }   
        }
        //phone validation check
        if(String.isNotBlank(MobilePhone) && String.isBlank(email) ){
            if(MobilePhone.startsWith('+')){
                MobilePhone=MobilePhone.replace('+', '00');}
            List<User> existingMobile = [SELECT id FROM User WHERE MobilePhone= :MobilePhone];
            //UIMS table check
            List<UIMS_User__c> existingMobileUIMS = [SELECT id FROM UIMS_User__c WHERE phoneId__c= :MobilePhone];
            if(existingMobile.size() > 0 || existingMobileUIMS.size() > 0 ){
                ErrorMsgCode = 'error.mobile';
                return null;
            } 
            smsIdentity = true;
        }else{
            smsIdentity = false; 
        }
        //password validation check
        
        if(!isPwdIncluded){
            password    = null;
            cpassword   = null; 
        }
        
        //new I am not a robot captcha verification
        if(showCaptcha==true){
            String captcha_response = ApexPages.currentPage().getParameters().get('g-recaptcha-response');
            if(String.isBlank(captcha_response) || !IDMSValidateReCaptcha.validateReCaptcha(captcha_response)){
                ErrorMsgCode='captcha.error';
                return null;
            }}
        //set email opt in
        String emailOpt='false';
        if(checkedEmailOpt==true){
            emailOpt='Y';
        }else{
            emailOpt='U';
        }
        if(isPwdIncluded==true){
            redirectToApp='/identity/idp/login?app='+applicationId;
        }
        
        //redirect user after confirmation page for mobile registration
        String languageUser                             = Label.CLJUN16IDMS09;
        String planguageUser                    = Label.CLJUN16IDMS10;
        String localeUser                               = Label.CLJUN16IDMS11;
        String timezoneUser                             = Label.CLJUN16IDMS22;
        String currencyUser                             = Label.CLJUN16IDMS04;
        User usr                                                = new User();
        usr.Email                                               = Email;
        usr.MobilePhone                                 = MobilePhone;
        usr.FirstName                                   = FirstName;
        usr.LastName                                    = LastName;
        usr.Country                                             = Country;
        usr.IDMS_User_Context__c                = context; 
        usr.IDMS_Registration_Source__c = registrationSource;
        usr.IDMS_PreferredLanguage__c   = planguageUser;
        usr.TimeZoneSidKey                              = timezoneUser;
        usr.LocaleSidKey                                = localeUser;
        usr.IDMS_Email_opt_in__c                = emailOpt;
        usr.DefaultCurrencyIsoCode              = currencyUser;
        usr.IDMSPrimaryContact__c               = true;
        usr.IdmsAppLanguage__c                  = appLanguage;
        
        try{         
            IDMSResponseWrapper data;
            //User creation async for identity as email and real time if identity is mobile made by Otmane Haloui:
            if(!smsIdentity){
                System.enqueueJob(new IDMSCreateUserAsync(usr,password));
            } else {
                system.debug('SubMit Home for mobile identity');
                data= IdmsUserRest.doPost(usr,password); 
            }
            
            isconfirm=true; // To be detailed
            showHome=false;
            if(smsIdentity==true){
                userId=[select id from user where id=:data.IDMSUserRecord.Id limit 1].id;
                String suffixurl;
                if(isPwdIncluded != true){
                    suffixurl='?app='+appid+'&id='+userId+'&key='+Label.CLQ316IDMS004;
                } else {
                    suffixurl='?app='+appid+'&id='+userId+'&key='+Label.CLQ316IDMS006; 
                }
                redirectToApp=Label.CLQ316IDMS112+suffixurl;
                system.debug('redirectToApp---------'+redirectToApp);
            }
            showHome=false;
            isconfirm=true;
            
            return null;
        }
        
        catch(Exception e){
            System.debug(e.getMessage());
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLQ316IDMS122);
            ApexPages.addMessage(msgntChkd );   
            return null;
        }     
    }
    
}