/*
    Author          : Bhuvana Subramaniyan 
    Description     : Test Class for VFC_BOFFormRedirect.
*/
@isTest
private class VFC_BOFFormRedirect_TEST
{
        static testMethod void bofFormTest()
        {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2;

        //Inserts the Product Lines        
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            prodLines.add(pl1);
        }
        Database.SaveResult[] lsr = Database.Insert(prodLines);      
        
        OPP_QuoteLink__c  quoteLink = Utils_TestMethods.createQuoteLink(opp2.Id);
        quoteLink.TypeOfOffer__c = Label.CLAPR14SLS10; 
        insert quoteLink;
        
        BudgetaryOfferForm__c bof =Utils_TestMethods.createBOF(quoteLink.Id);
        insert bof;

        ApexPages.StandardController controller = new ApexPages.StandardController(quoteLink);
        VFC_BOFFormRedirect bofRedirect = new VFC_BOFFormRedirect(controller);
        bofRedirect.bofForm();
        
        
        OPP_QuoteLink__c  quoteLink1 = Utils_TestMethods.createQuoteLink(opp2.Id);
        quoteLink1.TypeOfOffer__c = Label.CLAPR14SLS10; 
        insert quoteLink1;
        
        ApexPages.StandardController controller1 = new ApexPages.StandardController(quoteLink1);
        VFC_BOFFormRedirect bofRedirect1 = new VFC_BOFFormRedirect(controller1);
        bofRedirect1.bofForm();
        
        OPP_QuoteLink__c  quoteLink2 = Utils_TestMethods.createQuoteLink(opp2.Id);
        quoteLink2.TypeOfOffer__c = Label.CLAPR14SLS14; 
        insert quoteLink2;
        
        ApexPages.StandardController controller2 = new ApexPages.StandardController(quoteLink2);
        VFC_BOFFormRedirect bofRedirect2 = new VFC_BOFFormRedirect(controller2);
        bofRedirect2.bofForm();
        
        }
 
        
}