/*
    Author          : Abhishek Yadav (ACN) 
    Date Created    : 10/05/2011
    Description     : Class utilised by VFP29_ToggleSpam.
*/
public class VFC29_CaseToggleSpam{
    public List<Case> spams = new List<Case>();    
    public String url;
    public Boolean flag{get;set;}
    public VFC29_CaseToggleSpam(ApexPages.StandardSetController controller) {
        url=ApexPages.currentPage().getParameters().get(system.label.CL00330);
        spams = controller.getSelected();        
    }    
        // Method is used to Enable agents to mark a Case Records as Spam or vice versa and doing the corresponding status
        
    public pageReference ToggleSpam(){
        System.debug('--ToggleSpam Method Begins--');
        Set<ID> caseIDs = new Set<ID>();
        flag = False;   
        if(spams.size()>0){        
            for(Case cases:spams){
                //adding ID'S of all the selected cases to a set
                caseIDs.add(cases.Id);
            }    
            //Querying case records based on se of case Id's 
            spams = [Select Id,Spam__c,status,Tech_CaseStatus__c FROM Case WHERE Id IN :CaseIDs];      
            for(Case cases:Spams){
                //If the status of selected case is unchecked then checking it and changing the status to Closed
                if(cases.Spam__c== false){
                    cases.Spam__c = true;
                }
                else{
                    //If the status of selected case is checked then unchecking it and changing the status to open            
                    cases.Spam__c=false;
                }
            }
            if(Spams.size()>0){         
                if(Spams.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                    System.Debug('######## VFC29 Error Insert: '+ Label.CL00264);
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00264)); 
                }
                else{                     
                    Database.SaveResult[] VFC29_SaveResults = Database.update(Spams,false); 
                    //updating records and chcking for successful updates.
                    for(Database.SaveResult sr: VFC29_SaveResults){
                        if(!sr.isSuccess()){
                        Database.Error err = sr.getErrors()[0];                    
                        }
                    }
                }
            }
            System.debug('--ToggleSpam Method Ends--');   
            return redirectToListView();          
        } 
        else {
            flag=True;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,system.label.CL00342));        
            return null;               
        }        
    }
    
    public PageReference redirectToListView(){    
        if(url!=null){       
            return new PageReference(Url);
        }
        else{
            return null;
        }            
    }
}