@isTest
public class VFC_AccountMasterProfileRedirection_Test{
    public static testMethod void Testrun1() {
    Account a = Utils_TestMethods.createAccount();
        insert a;  
    
    AccountMasterProfile__c amp1=new AccountMasterProfile__c();
        amp1.Account__c=a.id;
        amp1.Q1Rating__c=null;
        amp1.Q2Rating__c='2';
        amp1.Q3Rating__c='2';
        amp1.Q4Rating__c='4';
        amp1.Q5Rating__c='3';
        amp1.Q6Rating__c='2';
        amp1.Q7Rating__c='4';
        amp1.Q8Rating__c='2';
        amp1.DirectSales__c=200;
        amp1.IndirectSales__c=300;
        amp1.totalPam__c=600;
        insert amp1;
    Pagereference pg = new pagereference('/apex/VFP_AccountMasterProfileRedirection?accid='+a.id); 
        Test.setCurrentPageReference(pg);
        
          
        VFC_AccountMasterProfileRedirection  ampred = new VFC_AccountMasterProfileRedirection(new ApexPages.StandardController(amp1));  
    
    ampRed.doRedirection();
    }
    
 }