/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: Test class of page controller FieloPRM_UTILS_Wrappers
********************************************************************/

@isTest
public without sharing class FieloPRM_UTILS_WrappersTest {
    
    @testSetup
    static void setupTest() {
    }
    
    @isTest
    static void unitTest1(){
        new FieloPRM_UTILS_Wrappers.MemberWrapper();
        new FieloPRM_UTILS_Wrappers.MemberWrapper(new FieloEE__Member__c());
        new FieloPRM_UTILS_Wrappers.ContactWrapper();
        new FieloPRM_UTILS_Wrappers.ContactWrapper('p_name', 'p_email', 'p_mobilePhone', 'p_workPhone', 'p_jobTitle', 'p_address', 'p_conLanguage', 'p_accountName');
    }
}