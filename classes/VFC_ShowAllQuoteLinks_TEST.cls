@isTest
public class VFC_ShowAllQuoteLinks_TEST{
    static testMethod void unitTest() {
        // Data Creation 
        
        Account acc=Utils_TestMethods.createAccount();
        insert acc;
        Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.id);
        insert opp;
        OPP_QuoteLink__c oppQLink = Utils_TestMethods.createQuoteLink(opp.id);
        insert oppQLink ;
        
        PageReference pageRef = Page.VFP_ShowAllQuoteLinks;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('oppid', opp.id);
        
        ApexPages.StandardController sdtCon = new ApexPages.StandardController(opp);
        VFC_ShowAllQuoteLinks myPageCon = new VFC_ShowAllQuoteLinks(sdtCon);
        myPageCon.getAllQuoteLinks();
        myPageCon.getOpportunity();
        myPageCon.doCancel();
        
    }
}