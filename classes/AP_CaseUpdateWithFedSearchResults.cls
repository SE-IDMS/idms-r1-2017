/*
    Author          : Alexandre Gonzalo - alexandre.gonzalo@non.schneider-electric.com 
    Date Created    : 20/03/2015
    Description     : Class for Federated Search Pilot - PM: Eric Roger
                        This class contains the methods to update case 
                        fields with values coming from Inquira FAQ objects with dedicated field CM federated search
*/

public without sharing class AP_CaseUpdateWithFedSearchResults {
    
    // Based on AP12_CaseIntegrationMethods UpdateCaseWithInquiraFaQ
    // Attach Inquira FAQs to the case field "Answer To Customer" field
    // Contraty to AP12 method, tt takes into account SourceSystem__c and ObjectType__c.
    public static void executeUpdate(Case Cse)
    {
        //*********************************************************************************
        // Method Name : executeUpdate
        // Purpose : To populate upto Answer to Customer for the Case
        // Created by : Alexandre Gonzalo - alexandre.gonzalo@non.schneider-electric.com 
        // Date created : 20/03/2015
        ///*********************************************************************************/
        
        System.Debug('**** AP_CaseUpdateWithFedSearchResults executeUpdate Start ****');
        
        String InquiraFaQContent = '';
        List<InquiraFAQ__c> TobeInsertedFAQs;
        
        if(Cse.Id != null)
        {
            TobeInsertedFAQs = [Select Name,Excerpt__c,URL__c,Title__c,SourceSystem__c,ObjectType__c,Visibility__c from InquiraFAQ__c where Case__c =: Cse.Id 
                                //and (Visibility__c =: 'Public' or Visibility__c like : '%external%') 
                                and URL__c != null]; 
        }
        
        String[] acceptedVisibilities = System.Label.CLJUN15CCCFS01.split(',',-2);
               
        if(Cse.AnswerToCustomer__c != null)
        {
            InquiraFaQContent  = Cse.AnswerToCustomer__c+'<br />';
        }
               
        if(TobeInsertedFAQs != null && TobeInsertedFAQs.size()>0)
        {        
            for(InquiraFAQ__c inqobj : TobeInsertedFAQs)
            {
                for(String acceptedVisibility: acceptedVisibilities)
                {
                    if(inqobj.Visibility__c.containsIgnoreCase(acceptedVisibility))
                    {
                        if(InquiraFaQContent != null || InquiraFaQContent != '')
                        {  
                            if(!InquiraFaQContent.contains(inqobj.Name)) 
                            {                                      
                                InquiraFaQContent +=  'ID => '+inqobj.Name+ '<br />';
                                InquiraFaQContent +=  'Title => '+inqobj.Title__c+ '<br />';
                                //InquiraFaQContent +=  'Source Sytem => ' + (inqobj.SourceSystem__c==null?'cKM':inqobj.SourceSystem__c) + '<br />';
                                //InquiraFaQContent +=  'Object Type => ' + (inqobj.ObjectType__c==null?'FAQ':inqobj.ObjectType__c) + '<br />';
                                InquiraFaQContent +=  'URL => '+(inqobj.URL__c).replace('&','&amp;')+ '<br /><br />';
                            }
                        }
                    }
                }
            }
            if(InquiraFaQContent.length() <= 32000)
            {
                Cse.AnswerToCustomer__c = InquiraFaQContent;  
                Database.SaveResult CasetoUpdate = Database.Update(Cse);         
                List<Database.Error> DMLError = CasetoUpdate.getErrors(); 
                System.Debug('#### DML Errors : ' + DMLError ); 
                      
            }       
            else
            {
                throw new TooLongAnswerException (System.Label.CLAPR15CCCFS02);
            }
        }
        // Comment: no error if the ansprovided to customer is not updated
        /*else{
            throw new NoFSearchResultsException (System.Label.CLAPR15CCCFS01);
        }*/      

        System.Debug('**** AP_CaseUpdateWithFedSearchResults executeUpdate Finished ****');
    }
    
    public class TooLongAnswerException extends Exception {}
    
    //public class NoFSearchResultsException extends Exception {}
}