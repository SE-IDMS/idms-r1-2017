/*******
Comments :TEST CLASS
********/

    @isTest
    Public Class VFC_ProjectRegionCountrySelection_TEST {



    static Testmethod  void Test_VFC_ProjectRegionSeltion() { 
    
        User newUser = Utils_TestMethods.createStandardUser('ELLAMQ1');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);

        Offer_Lifecycle__c offerRec = createOffer(newUser.Id);
        insert offerRec;
          country__c c = new country__c(name = 'TestCountryHP',countrycode__c = 'ZZp',Region__c='APAC',SubRegion__c ='China - zone');
        insert c;
        Milestone1_Project__c projRec =createProject(offerRec.Id, c.Id, newUser.Id);
        
          
        insert projRec;
        
      
        Id projecWithdrawalRegon = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Withdrawal Region').getRecordTypeId();  
        //PageReference pg = Page.VFP_ProjectRegionCountrySelection;
        //Test.setCurrentPage(pg);
        //pg.getParameters().put('urlOfferId', projRec.Id);
        //pg.getParameters().put('urlOfferId', offerRec.Id);
        //pg.getParameters().put('RecordType', projecWithdrawalRegon);
        // pg.getParameters().put('RecordType', projecWithdrawalRegon);
        Milestone1_Project__c mpObj = new Milestone1_Project__c(); 
        //Construct your pagereference
        PageReference testPage = new pagereference('/apex/VFP_ProjectRegionCountrySelection');
        testPage.getParameters().put('id', projRec.Id);
        testPage.getParameters().put('urlOfferId', projRec.Id);
        testPage.getParameters().put('RecordType',projecWithdrawalRegon);


        Test.setCurrentPage(testPage);
        List<Milestone1_Project__c> mpObjList = new list<Milestone1_Project__c>(); 
        List<Offer_Lifecycle__c> mpObjOffer = new list<Offer_Lifecycle__c>(); 
        
      
        mpObjList.add(projRec);
        
    
        ProjectZoneCountry__c  Pzc= new ProjectZoneCountry__c();
        Pzc.Project__c=projRec.id;//MileProj.id;
        Pzc.Country__c=c.id;
        
        Insert Pzc;
       
        ApexPages.StandardController controller;
        ApexPages.StandardSetController controller2= new ApexPages.StandardSetController(mpObjList); 
        VFC_ProjectRegionCountrySelection prjRgSelection1 = new VFC_ProjectRegionCountrySelection(controller);
        prjRgSelection1.strRegn='APAC';
        prjRgSelection1.strZone='China - zone';
        prjRgSelection1.MileProj.name='1209TEST';
        prjRgSelection1.MileProj.Region__c='APAC';
        prjRgSelection1.MileProj.Zone__c ='China - zone';
        prjRgSelection1.listRegionCountry();
        prjRgSelection1.listWrapperCntry[0].blnIsSelect=true;
        prjRgSelection1.pgRefSaveProject(); 
        
        
        //ADD Remove  
        PageReference testPageAddRemove = new pagereference('/apex/VFP_ProjectRegionAddRemove');
        testPageAddRemove.getParameters().put('id', projRec.Id);
        testPageAddRemove.getParameters().put('urlOfferId', projRec.Id);
        testPageAddRemove.getParameters().put('RecordType',projecWithdrawalRegon);
        
        //ApexPages.StandardController controller;
        ApexPages.StandardSetController controller3= new ApexPages.StandardSetController(mpObjList); 
        VFC_ProjectRegionAddRemove prjaddRemforCountry = new VFC_ProjectRegionAddRemove(controller);
        VFC_ProjectRegionAddRemove prjaddRemforCountry1 = new VFC_ProjectRegionAddRemove();
        
        //prjRgSelection1.strRegn='China - zone';
        //prjRgSelection1.MileProj.name='1209TEST';
        prjaddRemforCountry.strRegn='APAC';
        prjaddRemforCountry.strZone ='China - zone';
        prjaddRemforCountry.listWrapperCntry[0].blnIsSelect=true;
        prjaddRemforCountry.listWrapperCntry[0].isToSelect=false;
        prjaddRemforCountry.pgRefSaveProject();
        
        prjaddRemforCountry.listRegionCountry();
        
        
            
    
    
    }
    
    public static Offer_Lifecycle__c createOffer(Id userId){
        Offer_Lifecycle__c offer = new Offer_Lifecycle__c(
            Name = 'TestOffer', 
            Offer_Name__c = 'Offet Test',
            Leading_Business_BU__c = 'Cloud Services',
            Description__c = 'Offer Description',
            Sales_Date__c = system.today(),
            Process_Type__c = 'PMP',
            Launch_Owner__c = userId,
            Forecast_Unit__c = 'Kilo Euro',
            Marketing_Director__c = userId,
            Offer_Status__c = 'Red',
            Launch_Master_Assets_Status__c = 'Green',
            Offer_Classification__c = 'Major'
            
        );
        //insert offer;
        return offer;
    }
    
    public static Milestone1_Project__c createProject(Id OfferId, Id cId, Id userId){
        Milestone1_Project__c pjct = new Milestone1_Project__c(
            name='Test Project1',
            offer_launch__c = OfferId,
            country__c=cId,
            Country_Launch_Leader__c=userId,
            Country_Announcement_Date__c=system.today(),
            region__c='APAC'
        );
        //insert pjct;
        return pjct;
    }



}