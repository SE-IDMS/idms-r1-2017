/*
    Author          : Yannick Tisserand 
    Date Created    : 06/08/2013
    Description     : Test Class for IB Product Data Source Utility class
*/
@Istest

class Utils_IBProductDataSource_TEST
{
  
    static
    {
    // create test data
    }
    
    
    static testMethod void IBProductData_TestMethod()
    {   
        Utils_DataSource IBProductDataSource = Utils_Factory.getDataSource('IB_PRODUCT');
        List<String> dummyList1 = new List<String>();
        List<String> dummyList2 = new List<String>();
        Utils_DataSource.Result results = IBProductDataSource.search(dummyList1, dummyList2);
    }
}