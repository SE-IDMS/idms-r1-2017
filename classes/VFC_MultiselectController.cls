public with sharing class VFC_MultiselectController
{
    //Variables Declaration
    public PartnerProgram__c  partProg{get;set;}
    public string fieldValue{get;set;}
    public SelectOption[] selPartnerProgs { get; set; }
    public SelectOption[] allPartnerProgs { get; set; }
    public List<PartnerProgramCountries__c> partProgCounts = new List<PartnerProgramCountries__c>();
    
    public VFC_MultiselectController(ApexPages.StandardController controller) 
    {
        selPartnerProgs = new List<SelectOption>();  
        allPartnerProgs = new List<SelectOption>();
        partProg = (PartnerProgram__c)controller.getRecord();
        fieldValue = system.currentpagereference().getparameters().get('fieldName');
        List<Country__c> countries = new list<Country__c>();
        if(fieldValue != null && fieldValue == Label.CLJUN13PRM10)
        {
            partProg = [select GlobalPartnerProgram__r.TECH_CountriesId__c,TargetCountriesTxt__c,TECH_CountriesId__c, OwnerId, ProgramAdministrator__c , ProgramApprover__c from PartnerProgram__c where Id=: partProg.Id];
            list<String> lstCountryIds = new list<String>();
            if(partProg.GlobalPartnerProgram__r.TECH_CountriesId__c !=null)
            {
                for (String s : partProg.GlobalPartnerProgram__r.TECH_CountriesId__c.split(',')) 
                {
                    lstCountryIds.add(s);
                }
            }
            if(!lstCountryIds.isEmpty())
                countries = [select Name, Id from Country__c where id in :lstCountryIds order by name ASC];
        }
        else
        {
            partProg = [select TargetCountriesTxt__c,TECH_CountriesId__c, OwnerId, ProgramAdministrator__c , ProgramApprover__c from PartnerProgram__c where Id=: partProg.Id];
            countries = [select Name, Id from Country__c order by name ASC];
        }    
        
        Map<Id, String> countriesIdName = new Map<Id, String>();
        for(Country__c c: countries)
        {
            countriesIdName.put(c.Id, c.Name);
        }
        Set<String> selOpts1 = new Set<String>();
        if(partProg.TECH_CountriesId__c !=null)
        {
            for (String s : partProg.TECH_CountriesId__c.split(',')) 
            {
                selPartnerProgs.add(new SelectOption(s, countriesIdName.get(s)));
                selOpts1.add(s);
            }  
        }  
system.debug('selPartnerProgs:'+selPartnerProgs);
system.debug('selOpts1:'+selOpts1);
system.debug('countries:'+countries);
        for(Country__c c : countries) 
        {
            if(!(selOpts1.contains(c.Id)))
                allPartnerProgs.add(new SelectOption(c.Id, c.Name));
        }
system.debug('allPartnerProgs:'+allPartnerProgs);
        Boolean isAdminUser = false;
        String profileName=[select Name from Profile where Id=:UserInfo.getProfileId()].Name;
        List<String> profilesToSKIP = (System.Label.CLMAY13PRM33).split(',');//the valid profile prefix list is populated
        for(String pName : profilesToSKIP)//The profile name should start with the prefix else the displayerrormessage flag is set to true
        {
            if(pName.trim() == profileName.trim())
            {
                isAdminUser = true;             
            }  
        }

        if(isAdminUser == false && UserInfo.getUserId() != partProg.OwnerId && UserInfo.getUserId() != partProg.ProgramApprover__c  && UserInfo.getUserId() != partProg.ProgramAdministrator__c)
        {
            hasError = true;    
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLMAY13PRM52));
            //return null; 
        }                
    }
    
    Public Boolean hasError {get;set;} {hasError= false;}
    //Save Method updates the selected countries to the Target Countries of Partner Program
    public PageReference save() 
    {
           

        partProg.TargetCountriesTxt__c = null;
        partProg.TECH_CountriesId__c= null;
        
        for(SelectOption so : selPartnerProgs) 
        {
            if(partProg.TargetCountriesTxt__c ==null)
                partProg.TargetCountriesTxt__c = so.getLabel();
            else
                partProg.TargetCountriesTxt__c = partProg.TargetCountriesTxt__c + ',' +so.getLabel();            
            if(partProg.TECH_CountriesId__c==null)
                partProg.TECH_CountriesId__c= so.getValue();
            else
                partProg.TECH_CountriesId__c= partProg.TECH_CountriesId__c+ ',' +so.getValue();            

        }
        Database.update(partProg);
        List<PartnerProgramCountries__c> partnerProgCnts = new List<PartnerProgramCountries__c>();
        partnerProgCnts = [select Country__c, PartnerProgram__c from PartnerProgramCountries__c where PartnerProgram__c =: partProg.Id];
        Set<String> existCounts = new Set<String>();
        for(PartnerProgramCountries__c  partProg : partnerProgCnts)
        {
            existCounts.add(partProg.COuntry__c);
        }
        delete partnerProgCnts;
        if(partProg.TECH_COuntriesId__c!=null)
        {
            for(String s: partProg.TECH_COuntriesId__c.split(','))
            {
                //if(!(existCounts.contains(s)))
                {
                    PartnerProgramCountries__c partProgs = new PartnerProgramCountries__c();
                    partProgs.Country__c = s;
                    partProgs.PartnerProgram__c = partProg.Id;
                    partProgCounts.add(partProgs);
                }
            }
            Database.Insert(partProgCounts);
        }
        return new pagereference('/'+partProg.Id);       
    }
}