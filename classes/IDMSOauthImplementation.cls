@RestResource(urlMapping='/App/*')
global with sharing class IDMSOauthImplementation {
    
    private static final String AUTHORIZE_ENDPOINT = '/services/oauth2/authorize';  
    
    @HttpGet
    global static void doGet() {
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String redirectUrl = '';
        String brand = '';
        String brandBefore = req.requestUri.remove('/App/');
        
        if (brandBefore.contains(AUTHORIZE_ENDPOINT)) {
            brand = brandBefore.remove(AUTHORIZE_ENDPOINT);
            redirectUrl = '/identity'+AUTHORIZE_ENDPOINT;
            //Retrieve the cookie Name linked to the brand
            IDMSApplicationMapping__c app = IDMSApplicationMapping__c.getValues(brand);
            String cookieName;
            if(App != null){
                cookieName = app.Name;
            }
            system.debug('cookie Name: '+cookieName);
            
            String bodyString = '<script>document.cookie = "apex__brand=' + cookieName + ';Path=/;"; ';
            bodyString += 'window.location = "' + redirectUrl + '?';
            for (String parameterName :  req.params.keySet() ) {
                bodyString += parameterName + '=' + EncodingUtil.urlEncode(req.params.get(parameterName), 'UTF-8') + '&';
            }
            bodyString += '";</script>';
            system.debug('bodyString: '+bodyString);
            res.addHeader('Content-Type', 'text/html');  
            res.statusCode = 200; 
            res.responseBody = Blob.valueOf(bodyString);
        } else {
            res.statusCode = 403; 
        }
        
    }
    

}