global class Batch_PRM_ProcessPendingQueueItems implements Database.Batchable<sObject>,Schedulable,Database.AllowsCallouts,Database.Stateful  {
    
    public AP_PRMAppConstants.ProcessPendingQueueItems processType;
    public String query; 
    List<Id> contIdLst = new List<Id>();
    List<sObject> sObjLst = new List<sObject>();
    Set<Id> reqBatchId = new Set<Id>();
    List<String> lUcmpSet = new List<String>();
    private List<UIMSCompany__c> toProcessImmediate;
    private Boolean updateUIMS = false;
    private static List<String> lQryFields = AP_PRMUtils.RegistrationTrackingFields();
    String key = '';
    String federationIdentifier;
    String assertion;

    Map<String,CS_PRM_ApexJobSettings__c> mapApexJobSettings = CS_PRM_ApexJobSettings__c.getall(); 
    
    Integer pompPermn = Integer.valueOf(mapApexJobSettings.get('POMPPERM').NumberValue__c);
    Integer prtlPermn = Integer.valueOf(mapApexJobSettings.get('PRTLPERM').NumberValue__c);
    Integer updIdUIMS = Integer.valueOf(mapApexJobSettings.get('UPDIDUIMS').NumberValue__c);
    Integer registration = Integer.valueOf(mapApexJobSettings.get('REGISTRATION COUNTER').NumberValue__c);
    Integer batchSize;

    global Batch_PRM_ProcessPendingQueueItems(AP_PRMAppConstants.ProcessPendingQueueItems processType) {
        System.debug('Process Type in Constructor****'+processType);
        this.processType = processType;
        this.batchSize = Integer.valueOf(mapApexJobSettings.get(String.valueof(processType)).NumberValue__c);
    }

    global Batch_PRM_ProcessPendingQueueItems(AP_PRMAppConstants.ProcessPendingQueueItems processType, List<sObject> sObjectLst) {
        System.debug('Process Type in Constructor****'+processType);
        this.processType = processType;
        this.batchSize = Integer.valueOf(mapApexJobSettings.get(String.valueof(processType)).NumberValue__c);
    }

    global Batch_PRM_ProcessPendingQueueItems(AP_PRMAppConstants.ProcessPendingQueueItems processType, List<sObject> sObjectLst, Boolean updateBeforeProcessing) {
        System.debug('Process Type in Constructor****'+processType);
        this.processType = processType;
        if(updateBeforeProcessing){
            this.toProcessImmediate = (List<UIMSCompany__c>)sObjectLst;
            System.debug('**Immediate** :'+toProcessImmediate);
            System.debug('**Immediate*** :'+toProcessImmediate.size());
            }
        else
            this.sObjLst = sObjectLst;
        this.updateUIMS = updateBeforeProcessing;
        this.batchSize = Integer.valueOf(mapApexJobSettings.get(String.valueof(processType)).NumberValue__c);
    }

    global Batch_PRM_ProcessPendingQueueItems(AP_PRMAppConstants.ProcessPendingQueueItems processType,String federationIdentifier,String assertion) {
        System.debug('Process Type in Constructor****'+processType);
        this.processType = processType;
        this.batchSize = Integer.valueOf(mapApexJobSettings.get(String.valueof(processType)).NumberValue__c);
        this.federationIdentifier = federationIdentifier;
        this.assertion = assertion;
    }
      
    global Database.QueryLocator start(Database.BatchableContext BC) {

        List<String> lFlds = AP_PRMUtils.RegistrationTrackingFields();
        DateTime thisTime = System.Now();
        String queryFilter = '';
        System.debug('*** Immediate process list:' + this.toProcessImmediate);
        System.debug('*** Update UIMS?: ' + this.updateUIMS);

        if (this.toProcessImmediate != null && this.toProcessImmediate.size() > 0 && this.updateUIMS) {    
            /*Map<Id, UIMSCompany__c> mToProcessImmediate = new Map<Id, UIMSCompany__c> (this.toProcessImmediate);
            Map<Id, UIMSCompany__c> mExisting = new Map<Id, UIMSCompany__c>([SELECT Id, RegistrationStatus__c, IMSRegistrationQueueSubmittedAt__c, UserFederatedId__c FROM UIMSCompany__c WHERE Id = :mToProcessImmediate.keySet()]);
            Map<Id, UIMSCompany__c> lToUpdate = new Map<Id, UIMSCompany__c>();*/
            List<String> lTrkIds = new List<String>();
            key = 'In Progress - ' + thisTime;
            System.debug('>><<key:'+key);
            for (UIMSCompany__c tCmp : this.toProcessImmediate) {
                //UIMSCompany__c tCmp = (UIMSCompany__c)mExisting.get(i);
                System.debug('*** tCmp' + tCmp);
                //if (mExisting.containsKey(i) && tCmp != null) {
                    tCmp.put('RegistrationStatus__c', key);
                    System.debug('>><<tCmp:'+tCmp);    
                    //lToUpdate.put (tCmp.Id, tCmp);
                    lTrkIds.add('\'' + tCmp.Id + '\'');
               // }
            }

            if (lTrkIds != null && lTrkIds.size() > 0) {
                UPDATE this.toProcessImmediate;
                System.debug('*** Tracking Ids:' + lTrkIds);
                queryFilter = 'Id IN ('+ String.Join(lTrkIds, ',') +')';
            }
            if (String.isBlank(queryFilter))
                queryFilter = 'Id = \'\'';

            query = 'SELECT Id, ' + String.Join(lFlds,',') + ' FROM UIMSCompany__c WHERE '+ queryFilter + ' AND RegistrationStatus__c = \'' + key + '\' AND bFOMember__c != \'\' AND ((UserFederatedId__c = \'\' OR UserFederatedId__c = null) OR ((UserFederatedId__c != \'\' OR UserFederatedId__c != null) AND (bFOAccount__r.PRMUIMSID__c = \'\' OR bFOAccount__r.PRMUIMSID__c = null))) ORDER BY LastModifiedDate DESC';
        }
        else if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.UPDATE_REQUEST_QUEUE && !this.updateUIMS){
            if(this.toProcessImmediate != null && this.toProcessImmediate.size() > 0)
                UPDATE this.toProcessImmediate;
            query = 'SELECT Id, ProcessingStatus__c FROM PRMRequestQueue__c WHERE Id = \'\'';
        }
        System.debug('batch query in start method : '+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {        
        System.debug('**** Execute'+processType);
        System.debug('**Scope**'+scope);
        sObjLst.addall(scope);
        if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.UPDATEBFOIDINIMS) {            
            System.debug('** Entered updatebFOIdInIMS**');
            PRM_ProcessPendingQueueItems.updatebFOIdInIMS(scope);            
        } 
        else if (this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION || this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.MASS_REGISTRATION) {
            Set<Id> contactsIds = PRM_ProcessPendingQueueItems.CreatePRMRegistrationbFO(scope); 
        } 
        else if (this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_POMP) {
            System.debug('** AssignPermissionPOMP**');
            PRM_ProcessPendingQueueItems.assignPermissionPOMPUser(scope);            
        }
        else if (this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_PARTNER) {        
            System.debug('** AssignPermissionPartner**');
            PRM_ProcessPendingQueueItems.assignPermissionPartnerUser(scope);
       }
       else if (this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION_IMS) {
            List<UIMSCompany__c> lUcmp;
            Map<String,UserRegResult> mapRegResult = new Map<String,UserRegResult>();
            List<FieloEE__ErrorLog__c> errLog = new List<FieloEE__ErrorLog__c>();
            try {
                lUcmp  = (List<UIMSCompany__c>)scope;
                if (lUcmp != null && lUcmp.size() > 0){
                    for (UIMSCompany__c u : lUcmp) {
                        try {
                            AP_UserRegModel data = (AP_UserRegModel)JSON.deserialize(u.UserRegistrationInformation__c, AP_UserRegModel.class);
                            UserRegResult usrRegResult = AP_PRMAccountRegistration.CreatePRMRegistrationInIMS(data);
                            if(usrRegResult != null){
                                mapRegResult.put(u.Id,usrRegResult);
                            }
                        }
                        catch(Exception e){
                            u.put('IMSRegistrationError__c',true);
                            u.put('RegistrationStatus__c','');
                            u.put('InformationMessage__c',e.getMessage() + ' ' + e.getStackTraceString());
                            UPDATE lUcmp;
                            FieloEE__ErrorLog__c errLog1 = new FieloEE__ErrorLog__c(FieloEE__Message__c = e.getMessage(),FieloEE__StackTrace__c = e.getStackTraceString(),FieloEE__Type__c = 'IMS Registration Async Failed For - '+u.Name,FieloEE__UserId__c = System.Label.CLJUN15PRM029);
                            errLog.add(errLog1);
                        }               
                    }
                    PRM_FieldChangeListener.enableForAccount=false;
                    PRM_FieldChangeListener.enableForContact=false;
                    AP_PRMUtils.UpdateRegistrationInfoInbFO(mapRegResult,lUcmp);
                }
            }
            catch(Exception e){
                System.debug('**Exception in the Outer Catch Block **');
                FieloEE__ErrorLog__c errLog2 = new FieloEE__ErrorLog__c(FieloEE__Message__c = e.getMessage(),FieloEE__StackTrace__c = e.getStackTraceString(),FieloEE__Type__c = 'IMS Registration Async Failed For - REGISTRATION_IMS',FieloEE__UserId__c = System.Label.CLJUN15PRM029);
                errLog.add(errLog2);
            }
            finally{
                if(errLog != null && errLog.size() > 0)
                    INSERT errLog;
            }
       }
       else if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.MEMBER_PROPERTIES) {
            for(sObject s : scope) {
                Id contId = (String)s.get('bFOContact__c');
                contIdLst.add(contId);
            }
            PRM_FieldChangeListener.enableForAccount=false;
            PRM_FieldChangeListener.enableForContact=true; 
            PRM_FieldChangeListener.checkInternalPropertiesForContacts(new List<Id>(contIdLst));
            PRM_FieldChangeListener.enableForContact=false;
       }
       else if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.PRIVATE_REGISTRATION) {
       
            try {
                Boolean grantaccess = AP_PRMUtils.grantApplicationAccess(federationIdentifier,System.label.CLMAR16PRM011);
                system.debug('**Response From GrantApplication**'+grantaccess);
                
                //Set<String> newApplications = AP_PRMUtils.getApplications(EncodingUtil.base64Decode(assertion).toString());
                //system.debug('** newApplications **'+newApplications);
                Map<String,set<String>> attributeMap = AP_PRMUtils.getApplications(EncodingUtil.base64Decode(assertion).toString());
                System.debug('**newApplications:' + attributeMap.get('Applications'));
                
                Map<String,Set<String>> mapBadgeByContact =  new Map<String,Set<String>>();
                if(attributeMap.get('Applications').size() >0) {
                    Set<String> listAdd = new Set<String>();
                    mapBadgeByContact.put(federationIdentifier, listAdd);
                    for(String na: attributeMap.get('Applications')) {
                        System.Debug('*** FIELO Add :' + na);
                        listAdd.add(na);
                    }
                }
            } 
            catch(Exception ex) {
                System.debug('** Error with grantApplication/ add Member External Property**'+ex.getMessage()+''+ex.getstacktracestring());
            }   
       }
       
    } 

    // Batch Finish Method
    global void finish(Database.BatchableContext BC) {
        System.debug('*Finish Method Called*** ' + BC);
        System.debug('*Finish Method Called*** ' + sObjLst);
        String qFilter = '';
        DateTime thisTime = System.Now();
        
        if (sObjLst != null && sObjLst.size() > 0 && this.processType != AP_PRMAppConstants.ProcessPendingQueueItems.UPDATE_REQUEST_QUEUE) {
            key = 'In Progress - ' + thisTime;
            for (sObject u : this.sObjLst){
                System.debug('*** Object Type:' + String.valueOf(u.getsObjectType()));
                if (String.valueOf(u.getsObjectType()) == 'UIMSCompany__c'){
                    if (this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION)
                        u.put('IMSRegistrationQueueSubmittedAt__c', thisTime);
                    u.put('RegistrationStatus__c',key);
                    lUcmpSet.add('\''+u.Id+'\'');
                }            
            }
            UPDATE sObjLst;
        }
        if(lUcmpSet != null && lUcmpSet.size() > 0)
            qFilter = 'Id IN ('+ String.Join(lUcmpSet,',') +')';
        else
            qFilter = 'Id = \'\'';
       // if (this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_PARTNER ||  
         //   this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_POMP) {
            // Database.executeBatch(new Batch_ProcessPRMRequestQueueItems(reqBatchId));
          if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION){
            if (AP_PRMUtils.CanAddNewApexJobToQueue()) {
                System.debug('>>>>this.batchSize:'+this.batchSize);
                AP_PRMAppConstants.ProcessPendingQueueItems regIMS = AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION_IMS;
                Batch_PRM_ProcessPendingQueueItems batch_ppsa = new Batch_PRM_ProcessPendingQueueItems(regIMS);
                this.batchSize = Integer.valueOf(mapApexJobSettings.get(String.valueof(regIMS)).NumberValue__c);
                batch_ppsa.query = 'SELECT Id,' + String.Join(lQryFields, ',') + ' FROM UIMSCompany__c WHERE ' + qFilter + ' AND RegistrationStatus__c = \'' + key + '\' AND bFOMember__c != \'\' AND UserFederatedId__c = \'\' ORDER BY LastModifiedDate DESC';
                System.debug('batch query : '+batch_ppsa.query);
                System.debug('batch size:' + this.batchSize + batch_ppsa.batchSize + batch_ppsa);
                Database.executebatch(batch_ppsa, this.batchSize);
            }
            //UPDATE u;
        }
        else if (this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION_IMS) {
            if (AP_PRMUtils.CanAddNewApexJobToQueue()) {
                AP_PRMAppConstants.ProcessPendingQueueItems memProp = AP_PRMAppConstants.ProcessPendingQueueItems.MEMBER_PROPERTIES;
                Batch_PRM_ProcessPendingQueueItems batch_ppsa = new Batch_PRM_ProcessPendingQueueItems(memProp);
                this.batchSize = Integer.valueOf(mapApexJobSettings.get(String.valueof(memProp)).NumberValue__c);
                batch_ppsa.query = 'SELECT Id, ' + String.Join(lQryFields, ',') + ' FROM UIMSCompany__c WHERE ' + qFilter + ' AND RegistrationStatus__c = \'' + key + '\' ORDER BY LastModifiedDate DESC';
                System.debug('batch query : ' + batch_ppsa.query);
                System.debug('batch size:' + this.batchSize + batch_ppsa.batchSize + batch_ppsa);
                Database.executebatch(batch_ppsa, this.batchSize);
            }            
        }
    }

    //Schedule method
    global void execute(SchedulableContext sc) {
        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerOtherEmail = false;
        dlo.EmailHeader.triggerUserEmail = false;
        dlo.optAllOrNone = false;
        List<CronTrigger> crnTrigger = new List<CronTrigger>();
        String batchQueryFilter = '%PRM Job - '+string.valueof(processType)+'-%';
        try{
            for(CronTrigger crn : [SELECT Id, State, CronJobDetail.Name, StartTime, NextFireTime FROM CronTrigger WHERE CronJobDetail.Name LIKE :batchQueryFilter LIMIT 100]){
                if(crn != null){
                    System.debug('>>>><<<<crn:'+crn);
                    if(crn.State == 'Deleted' && crn.NextFireTime == null && Integer.valueOf((System.Now().getTime() - crn.StartTime.getTime())/(1000*60)) >= Integer.valueOf(mapApexJobSettings.get('Time_Interval_To_Delete_Batch_Jobs').NumberValue__c)){
                        System.debug('>>>><<<<Inside if');
                        try{
                            System.debug('>>>><<<<try');
                            system.abortJob(crn.id);
                        }
                        catch(Exception e){
                            System.debug('Exception occurred : '+e.getMessage() + e.getStackTraceString());
                        }
                    }
                }
            }
            crnTrigger = new List<CronTrigger>([SELECT State, CronJobDetail.Name FROM CronTrigger WHERE CronJobDetail.Name LIKE :batchQueryFilter AND (State = 'Waiting' OR State = 'Acquired' OR State = 'Queued' OR State = 'Holding') LIMIT 100]);
            if(crnTrigger.isEmpty()){
                System.debug('**Batch Start Method called**');
                Datetime sysTime = System.now().addMinutes(Integer.valueOf(mapApexJobSettings.get(String.valueof(processType)).QueryFields__c));
                String cronExp = sysTime.format('s m H d M \'?\' yyyy');
                Batch_PRM_ProcessPendingQueueItems batch_ppsa = new Batch_PRM_ProcessPendingQueueItems(processType);
                System.schedule('PRM Job - '+string.valueof(processType)+'-'+sysTime, cronExp, batch_ppsa);
            }
        }
        catch(Exception e){
            FieloEE__ErrorLog__c errLog = new FieloEE__ErrorLog__c(FieloEE__Message__c = e.getMessage(),FieloEE__StackTrace__c = e.getStackTraceString(),FieloEE__Type__c = 'Cron Trigger Failed - '+string.valueof(processType)+' & Cron Trigger List Size : '+crnTrigger.size(),FieloEE__UserId__c = System.Label.CLJUN15PRM029);
            INSERT errLog;
        }
        key = '';
        try{
            List<String> lFlds = new List<String>();
            System.debug('Process Type in Schedule execute ****'+processType);            
            Batch_PRM_ProcessPendingQueueItems batch_ppsa = new Batch_PRM_ProcessPendingQueueItems(processType);
            String regQryFilter = '';
            regQryFilter = (this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION_IMS)?
                            ('(bFOMember__c != \'\' AND RegistrationStatus__c = \'\' AND IMSRegistrationError__c = true AND ((bFOContact__r.PRMPrimaryContact__c = true AND ((UserFederatedId__c = \'\' OR UserFederatedId__c = null) OR ((UserFederatedId__c != \'\' OR UserFederatedId__c != null) AND (bFOAccount__r.PRMUIMSID__c = \'\' OR bFOAccount__r.PRMUIMSID__c = null)))) OR (bFOContact__r.PRMPrimaryContact__c = false AND ((bFOAccount__c != \'\' OR bFOAccount__c != null) AND (bFOAccount__r.PRMUIMSID__c != \'\' OR bFOAccount__r.PRMUIMSID__c != null)) AND (UserFederatedId__c = \'\' OR UserFederatedId__c = null))))'):
                            ((this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION)?
                            ('((bFOAccount__c = \'\' OR bFOContact__c = \'\' OR bFOUser__c = \'\' OR bFOMember__c = \'\') OR (UserFederatedId__c != \'\' AND bFOContact__r.TECH_PrivateRegCompleted__c = false)) AND RetryRegistration__c = true AND RegistrationStatus__c = \'\' AND RunCounter__c <=: registration'):
                            ((this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.MASS_REGISTRATION)?
                            ('(bFOAccount__c = \'\' OR bFOContact__c = \'\' OR bFOUser__c = \'\' OR bFOMember__c = \'\') AND RunCounter__c <=: registration AND RecordTypeId =\''+System.Label.CLSEP16PRM001+'\''):''));
            System.debug('*** '+regQryFilter);               
            if(String.isNotBlank(regQryFilter)){
                lFlds = (AP_PRMUtils.RegistrationTrackingFields());
                List<UIMSCompany__c>  uimsCompanyLst = Database.query('SELECT Id,bFOContact__r.PRMPrimaryContact__c,bFOContact__r.TECH_PrivateRegCompleted__c ,bFOAccount__r.PRMUIMSID__c,'+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE '+regQryFilter+' ORDER BY LastModifiedDate DESC');
                DateTime dt = System.Now();
                key = 'In Progress - '+dt;
                for(UIMSCompany__c uims : uimsCompanyLst){                
                    uims.put('RegistrationStatus__c',key);
                    if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION_IMS)
                        uims.put('IMSRegistrationQueueSubmittedAt__c',System.Now());
                    if(!uims.bFOContact__r.PRMPrimaryContact__c){
                        AP_UserRegModel regModel = (AP_UserRegModel)JSON.deserialize(uims.UserRegistrationInformation__c, AP_UserRegModel.class);
                        if(String.isNotBlank(uims.bFOAccount__r.PRMUIMSId__c))
                            regModel.uimsCompanyId = regModel.companyFederatedId = uims.bFOAccount__r.PRMUIMSId__c;
                        uims.UserRegistrationInformation__c = (String)JSON.serialize(regModel);
                    }
                }
                Database.SaveResult[] SaveResult = database.UPDATE(uimsCompanyLst,dlo);         
                
                if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION_IMS){
                   // batch_ppsa.query = 'SELECT Id, '+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE (bFOContact__r.PRMPrimaryContact__c = true AND bFOMember__c != \'\' AND UserFederatedId__c = \'\' AND RegistrationStatus__c = \'' +key+'\' AND RecordTypeId =\''+System.Label.CLJUL16PRM014+'\') ORDER BY LastModifiedDate DESC';
                    //batch_ppsa.query = 'SELECT Id, bFOAccount__r.PRMUIMSID__c, bFOContact__r.PRMPrimaryContact__c, '+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE (RegistrationStatus__c = \''+key+'\' AND bFOMember__c != \'\' AND (RecordTypeId =\''+System.Label.CLJUL16PRM014+'\' OR (RecordTypeId =\''+System.Label.CLSEP16PRM001+'\' AND IMSRegistrationError__c = TRUE)) AND UserFederatedId__c = \'\' AND ((bFOContact__r.PRMPrimaryContact__c = true) OR (bFOContact__r.PRMPrimaryContact__c = false AND (bFOAccount__r.PRMUIMSID__c != \'\' OR bFOAccount__r.PRMUIMSID__c != null))))';
                    batch_ppsa.query = 'SELECT Id, bFOAccount__r.PRMUIMSID__c, bFOContact__r.PRMPrimaryContact__c, '+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE (RegistrationStatus__c = \''+key+'\' AND bFOMember__c != \'\' AND IMSRegistrationError__c = true AND ((bFOContact__r.PRMPrimaryContact__c = true AND ((UserFederatedId__c = \'\' OR UserFederatedId__c = null) OR ((UserFederatedId__c != \'\' OR UserFederatedId__c != null) AND (bFOAccount__r.PRMUIMSID__c = \'\' OR bFOAccount__r.PRMUIMSID__c = null)))) OR (bFOContact__r.PRMPrimaryContact__c = false AND ((bFOAccount__c != \'\' OR bFOAccount__c != null) AND (bFOAccount__r.PRMUIMSID__c != \'\' OR bFOAccount__r.PRMUIMSID__c != null)) AND (UserFederatedId__c = \'\' OR UserFederatedId__c = null))))';          
                }

                if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.REGISTRATION){
                    batch_ppsa.query = 'SELECT Id, bFOAccount__r.PRMUIMSID__c, bFOContact__r.PRMPrimaryContact__c, '+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE ((bFOAccount__c = \'\' OR bFOContact__c = \'\' OR bFOUser__c = \'\' OR bFOMember__c = \'\')OR (UserFederatedId__c != \'\' AND bFOContact__r.TECH_PrivateRegCompleted__c = false AND RegistrationType__c = \'Private Registration\'))  AND RetryRegistration__c = true AND RunCounter__c <=: registration  AND RegistrationStatus__c = \'' +key+'\' ORDER BY LastModifiedDate DESC';
                }

                if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.MASS_REGISTRATION){
                    batch_ppsa.query = 'SELECT Id, bFOAccount__r.PRMUIMSID__c, bFOContact__r.PRMPrimaryContact__c, '+String.Join(lFlds,',')+' FROM UIMSCompany__c WHERE (bFOAccount__c = \'\' OR bFOContact__c = \'\' OR bFOUser__c = \'\' OR bFOMember__c = \'\')  AND RunCounter__c <=: registration AND RecordTypeId =\''+System.Label.CLSEP16PRM001+'\' AND RegistrationStatus__c = \'' +key+'\' ORDER BY LastModifiedDate DESC';
                }
            }
            else{
                String qryFilter = '';
                qryFilter = (this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.UPDATEBFOIDINIMS)?
                            ('AND RequestType__c = \'UPDIDUIMS\' AND RunCounter__c <= :updIdUIMS'):
                            ((this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_POMP)?
                            ('AND RequestType__c = \'POMPPERM\' AND RunCounter__c <= :pompPermn'):
                            ((this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_PARTNER)?
                            ('AND RequestType__c = \'PRTLPERM\' AND RunCounter__c <= :prtlPermn'):
                            ((this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.MEMBER_PROPERTIES)?
                            ('AND RequestType__c = \'UPDMEMPROP\''):'')));
                List<PRMRequestQueue__c> prmRequestQueueLst = Database.query('SELECT AccountId__c,AccountId__r.PRMUIMSID__c,AccountId__r.PRMAreaOfFocus__c,AccountId__r.PRMBusinessType__c,AccountId__r.PRMCompanyName__c,AccountId__r.PRMStreet__c,AccountId__r.PRMCity__c,AccountId__r.MarketServed__c,AccountId__r.PRMCurrencyIsoCode__c,AccountId__r.PRMEmployeeSize__c,AccountId__r.PRMZipCode__c,AccountId__r.PRMIncludeCompanyInfoInSearchResults__c,AccountId__r.PRMCompanyPhone__c,AccountId__r.PRMWebsite__c,AccountId__r.PRMTaxId__c,AccountId__r.PRMAnnualSales__c,AccountId__r.PRMCorporateHeadquarters__c,AccountId__r.PRMAdditionalAddress__c,AccountId__r.StateProvince__r.StateProvinceCode__c, ContactId__c, ContactId__r.PRMUIMSID__c,ContactId__r.PRMEmail__c,ContactId__r.PRMFirstName__c,ContactId__r.PRMLastName__c,ContactId__r.PRMJobTitle__c,ContactId__r.PRMJobFunc__c,ContactId__r.PRMCustomerClassificationLevel1__c,ContactId__r.PRMCustomerClassificationLevel2__c,ContactId__r.PRMPrimaryContact__c,ContactId__r.PRMMobilePhone__c,ContactId__r.PRMWorkPhone__c,ContactId__r.PRMCountry__r.CountryCode__c,RunCounter__c FROM PRMRequestQueue__c WHERE ProcessingStatus__c LIKE \'Pending%\' '+ qryFilter);
                key = 'Pending - '+System.Now();
                for(PRMRequestQueue__c prmReq : prmRequestQueueLst){
                    prmReq.put('ProcessingStatus__c',key);
                }
                Database.SaveResult[] SaveResult = database.UPDATE(prmRequestQueueLst,dlo);         
                     
                if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.UPDATEBFOIDINIMS) {                
                    system.debug('** updatebFOIdInIMS**');
                    batch_ppsa.query = 'SELECT AccountId__c,AccountId__r.PRMUIMSID__c,AccountId__r.PRMAreaOfFocus__c,AccountId__r.PRMBusinessType__c,AccountId__r.PRMCompanyName__c,AccountId__r.PRMStreet__c,AccountId__r.PRMCity__c,AccountId__r.MarketServed__c,AccountId__r.PRMCurrencyIsoCode__c,AccountId__r.PRMEmployeeSize__c,AccountId__r.PRMZipCode__c,AccountId__r.PRMIncludeCompanyInfoInSearchResults__c,AccountId__r.PRMCompanyPhone__c,AccountId__r.PRMWebsite__c,AccountId__r.PRMTaxId__c,AccountId__r.PRMAnnualSales__c,AccountId__r.PRMCorporateHeadquarters__c,AccountId__r.PRMAdditionalAddress__c,AccountId__r.StateProvince__r.StateProvinceCode__c, ContactId__c, ContactId__r.PRMUIMSID__c,ContactId__r.PRMEmail__c,ContactId__r.PRMFirstName__c,ContactId__r.PRMLastName__c,ContactId__r.PRMJobTitle__c,ContactId__r.PRMJobFunc__c,ContactId__r.PRMCustomerClassificationLevel1__c,ContactId__r.PRMCustomerClassificationLevel2__c,ContactId__r.PRMPrimaryContact__c,ContactId__r.PRMMobilePhone__c,ContactId__r.PRMWorkPhone__c,ContactId__r.PRMCountry__r.CountryCode__c,RunCounter__c FROM PRMRequestQueue__c WHERE ProcessingStatus__c = \''+key+'\' AND RequestType__c = \'UPDIDUIMS\' AND RunCounter__c <= :updIdUIMS ';
               } 
                else if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_POMP) {                
                    system.debug('** AssignPermissionPOMP**');
                    batch_ppsa.query = 'SELECT ContactId__c,RunCounter__c FROM PRMRequestQueue__c WHERE ProcessingStatus__c = \''+key+'\' AND RequestType__c = \'POMPPERM\' AND RunCounter__c <= :pompPermn';
                } 
                else if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_PARTNER) {                
                    system.debug('** AssignPermissionPartner**');
                    batch_ppsa.query = 'SELECT ContactId__c,RunCounter__c FROM PRMRequestQueue__c WHERE ProcessingStatus__c = \''+key+'\' AND RequestType__c = \'PRTLPERM\' AND RunCounter__c <= :prtlPermn ';            
                }
                else if(this.processType == AP_PRMAppConstants.ProcessPendingQueueItems.MEMBER_PROPERTIES) {                
                    system.debug('** AssignMemberProperties**');
                    batch_ppsa.query = 'SELECT ContactId__c,RunCounter__c FROM PRMRequestQueue__c WHERE ProcessingStatus__c = \''+key+'\' AND RequestType__c = \'UPDMEMPROP\'';            
                }    
            }    
            Database.executebatch(batch_ppsa,this.batchSize);
        }
        catch(Exception ex){
            FieloEE__ErrorLog__c errLog = new FieloEE__ErrorLog__c(FieloEE__Message__c = ex.getMessage(),FieloEE__StackTrace__c = ex.getStackTraceString(),FieloEE__Type__c = 'Schedule Execute Failed - '+string.valueof(this.processType)+' & Cron Trigger Size : '+crnTrigger.size(),FieloEE__UserId__c = System.Label.CLJUN15PRM029);
            INSERT errLog;
        }
    }
}