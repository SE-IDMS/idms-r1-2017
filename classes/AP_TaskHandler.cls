public Class AP_TaskHandler{
    public static void updateLatestContact(List<Task> lstOrphanTasks){
        // Added by Anil Budha as part of Q1 2016 Release
        // Modified by Vimal , Moved the code from AfterInsert to BeforeInsert
        //BR-9404 Change to Remove RecentlyViewed SOQL and Use Case Last Modified. 
        //List<RecentlyViewed> lstRecentlyViewed =[Select Id,Name from RecentlyViewed where LastViewedDate!=null and Type='Contact' order by LastViewedDate DESC limit 1];
        //if(lstRecentlyViewed.size()>0 ){
        List<Case> lstCase = new List<Case>([SELECT ContactId from Case WHERE LastModifiedDate=TODAY AND LastModifiedById  =:UserInfo.getUserId()  AND ContactId != null ORDER BY LastModifiedDate DESC limit 1]);        
        if(lstCase.size()>0){
            for (Task objTaskForUpdate:lstOrphanTasks){
                objTaskForUpdate.WhoID =lstCase[0].ContactId;
                objTaskForUpdate.REP_IsWhoIDAssigned__c = true;
            }
        }        
    }

    public static void calculateFirstResponseOnOutboundCall(Set<Id> setCaseIdsForLogicalResponse){
        List<Case> lstCasesForLogicalResponseUpdate = new List<Case>();
        if(setCaseIdsForLogicalResponse.size()>0){
            lstCasesForLogicalResponseUpdate = new List<Case>([Select Id, FirstLogicalResponseDateTime__c from Case where Id in:setCaseIdsForLogicalResponse AND FirstLogicalResponseDateTime__c = null]);
            if(lstCasesForLogicalResponseUpdate.size()>0){
                for(Case objCaseForUpdate: lstCasesForLogicalResponseUpdate){
                    objCaseForUpdate.FirstLogicalResponseDateTime__c = System.now();
                }
                Database.SaveResult[] lstCaseLogicalResponseResult = Database.update(lstCasesForLogicalResponseUpdate, false);
            }
        }
    }
}