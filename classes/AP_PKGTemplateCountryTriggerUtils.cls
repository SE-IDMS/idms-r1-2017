/********************************************************************************************************************
    Created By : Shruti Karn
    Created Date : 17 July 2012
    Description : For September Release:
                 1. To update Available in Countries Field on related Package Template
    
********************************************************************************************************************/
public class AP_PKGTemplateCountryTriggerUtils {
    
    public static void updateTemplateCountry(map<Id,PackageTemplateCountry__c> mapTemplateCountry)
    {
        list<Id> lstTemplateID = new list<Id>();
        list<Id> lstCountryID = new list<Id>();
        for(Id tempId : mapTemplateCountry.keySet())
        {
            lstTemplateID.add(mapTemplateCountry.get(tempId).packagetemplate__c);
        }
        
        map<Id, PackageTemplate__c> mapPKGTemplate = new map<ID,PackageTemplate__c>([Select id, AvailableinCountries__c from PackageTemplate__C where id in :lstTemplateID limit 10000 ]);
        map<Id,PackageTemplateCountry__c> mapAllTemplateCountry = new map<Id,PackageTemplateCountry__c>([Select id,country__c,packagetemplate__c from PackageTemplateCountry__c where packagetemplate__c in : lstTemplateID limit 50000]);
        
        for(Id tempId : mapAllTemplateCountry.keySet())
        {
            lstCountryID.add(mapAllTemplateCountry.get(tempId).country__c);
        }
        map<Id, Country__c> mapCountry = new map<Id,Country__c>([Select id,Name from Country__c where id in : lstCountryID limit 50000]);
        for(Id tempId : mapPKGTemplate.keySet())
        {
            mapPKGTemplate.get(tempId).AvailableinCountries__c = '';
        }
        
        for(Id tempCountryId : mapAllTemplateCountry.keySet())
        {
            if(mapPKGTemplate.get(mapAllTemplateCountry.get(tempCountryId).packagetemplate__c).AvailableinCountries__c.trim() != null && mapPKGTemplate.get(mapAllTemplateCountry.get(tempCountryId).packagetemplate__c).AvailableinCountries__c.trim() != '')
                mapPKGTemplate.get(mapAllTemplateCountry.get(tempCountryId).packagetemplate__c).AvailableinCountries__c = mapPKGTemplate.get(mapAllTemplateCountry.get(tempCountryId).packagetemplate__c).AvailableinCountries__c + ','+mapCountry.get(mapAllTemplateCountry.get(tempCountryId).country__c).Name;
            else
                mapPKGTemplate.get(mapAllTemplateCountry.get(tempCountryId).packagetemplate__c).AvailableinCountries__c = mapCountry.get(mapAllTemplateCountry.get(tempCountryId).country__c).Name;                 
        }
            
        update mapPKGTemplate.values();
    }
}