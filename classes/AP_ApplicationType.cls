global without sharing class AP_ApplicationType{
    global String ApplicationId { get; set; }
    global String ApplicationName {get;set;}
    global String DisplayName {get;set;}
}