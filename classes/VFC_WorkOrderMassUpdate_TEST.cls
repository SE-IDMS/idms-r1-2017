@isTest
private class VFC_WorkOrderMassUpdate_TEST {

    static testMethod void VFC_WorkOrderMassUpdateTest() {
    
    User user1 = Utils_TestMethods.createStandardUser('User1');
    insert user1;

        Country__c Ctry = Utils_TestMethods.createCountry(); 
        insert Ctry;

        Account acc1 = Utils_TestMethods.createAccount();
        acc1.Name = 'Account 1';
        acc1.Country__c = Ctry.Id;
        acc1.City__c = 'Paris';
        acc1.BillingCity = acc1.City__c;
        insert acc1;        
        
        SVMXC__Site__c loc1 = Utils_TestMethods.createLocation(acc1.Id);
        loc1.Name = 'Account 1 - SITE';
        insert loc1;
        
        Contact contact1 = Utils_TestMethods.createContact(acc1.Id, 'Contact 1');
        contact1.Email = 'contact1@yopmail.com';
        contact1.WorkPhone__c = '+331234567890';
        insert contact1;

        CustomerLocation__c Cust_Location = Utils_TestMethods.createCustomerLocation(acc1.Id, Ctry.Id);
        insert Cust_Location;

        Account acc2 = Utils_TestMethods.createAccount();
        acc2.Name = 'Account 2';
        acc2.Country__c = Ctry.Id;
        acc2.City__c = 'Paris';
        acc2.BillingCity = acc2.City__c;
        insert acc2;
        
        SVMXC__Site__c loc2 = Utils_TestMethods.createLocation(acc2.Id);
        loc2.Name = 'Account 2 - SITE';
        insert loc2;

        Contact contact2 = Utils_TestMethods.createContact(acc2.Id, 'Contact 2');
        contact1.Email = 'contact2@yopmail.com';
        contact1.WorkPhone__c = '+332345678901';
        insert contact2;
    
    SVMXC__Territory__c territory1 = new SVMXC__Territory__c();
    territory1.name = 'VFC_WorkOrderMassUpdate_TEST - Territory 1';
    territory1.SVMXC__Active__c = true;
    territory1.SVMXC__Description__c = 'VFC_WorkOrderMassUpdate_TEST - Territory 1';
    territory1.SVMXC__Territory_Code__c = 'VFC_WorkOrderMassUpdate_TEST - Territory 1';
    territory1.ExternalId__c = 'VFC_WorkOrderMassUpdate_TEST - Territory 1';
    insert territory1;
        
        Test.startTest();
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc1.id);
        workOrder.Work_Order_Category__c='On-site';                 
        workOrder.SVMXC__Order_Type__c='Maintenance';
        workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
        workOrder.SVMXC__Order_Status__c = 'UnScheduled';
        workOrder.CustomerRequestedDate__c = Date.today();
        workOrder.Service_Business_Unit__c = 'Energy';
        workOrder.SVMXC__Priority__c = 'Normal-Medium';
        workOrder.Customer_Location__c = Cust_Location.Id;
        workOrder.SendAlertNotification__c = true;
        workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder.CustomerTimeZone__c = 'America/New_York';
        
        workOrder.BillToAccount__c = acc1.Id;
        workOrder.SoldToAccount__c = acc1.Id;
        
        workOrder.SVMXC__Company__c = acc1.Id;
        workOrder.SVMXC__Site__c = loc1.Id;
        workOrder.SVMXC__Contact__c = contact1.Id;
        

        try{
            //insert workOrder;
        }
        catch(Exception ex){
        }




        List<SVMXC__Service_Order__c> wolist = new List<SVMXC__Service_Order__c>();
        wolist.add(workOrder);
        insert wolist;
        PageReference pageRef = Page.VFP_WorkOrderMassUpdate;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(wolist);
        ssc.setSelected(wolist);
        VFC_WorkOrderMassUpdate controller = new VFC_WorkOrderMassUpdate(ssc);
        //controller.IsBillingTypeShow = true;
        SVMXC__Service_Order__c wo= new SVMXC__Service_Order__c();
        
        
        wo.SVMXC__Order_Status__c  = 'Scheduled';
        wo.RescheduleReason__c = 'Safety Issue' ;
        
        wo.SVMXC__Company__c = acc2.Id;
        wo.SVMXC__Site__c = loc2.Id;
        wo.SVMXC__Contact__c = contact2.Id;
        
        
        wo.CustomerRequestedDate__c = Date.today().addDays(10);
        wo.CountryOfBackOffice__c = 'Global';
        wo.BackOfficeSystem__c = 'ITB_ORA';
        wo.SVMXC__Order_Type__c = 'Preventive Maintenance';
        wo.WorkOrderSubType__c = 'Preventive parts replacement';
        wo.IsBillable__c = 'No';
        wo.SVMXC__Billing_Type__c = 'Warranty';
    controller.isSendCustomerConfirmationEmail = 'False';
    wo.SVMXC__Primary_Territory__c = territory1.Id;
    wo.CustomerTimeZone__c = 'America/Los_Angeles';
    wo.PlannerContact__c = user1.Id;
    wo.SVMXC__Special_Instructions__c = 'Special instructions';
    
    
        
        
        controller.wo = wo;
        controller.doSave();
        controller.call();
        controller.doCancel();
        Test.stopTest();
    
    }

}