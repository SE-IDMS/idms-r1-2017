global class Batch_PRM_UserMigration implements Database.Batchable<sObject>, Database.Stateful {
    
    public static final Integer MAXIMUM_STEPS_IN_CHAIN = 4;
    String query;
    Integer batchSize;
    Boolean createUser;
    Boolean createMember;
    Boolean createPsetAssign;
    Boolean checkMemberProperties;
    Boolean checkGamify;
    String hashTag;
    Integer attempt = 0;
    Integer attemptLimit;
    List<PRMPartnerMigration__c> scopeCopy;
    // step=0 : Create User
    // Step=1 : Create PsetAssign
    // Step=2 : Create Member
    // Step=3 : Create Member Properties and Activities (MPA)
    // Step=4 : Run Gamify = Generate output for Member Properties and Activities
    Integer step;

    global Batch_PRM_UserMigration(Boolean createUser,Boolean createPsetAssign,Boolean createMember,Boolean checkMemberProperties,Boolean checkGamify,String hashTag,Integer Step,Integer batchSize,Integer attemptLimit) {
        this.createUser = createUser;
        this.createMember = createMember;
        this.createPsetAssign = createPsetAssign;
        this.checkMemberProperties = checkMemberProperties;
        this.checkGamify = checkGamify;
        this.hashTag = hashTag;
        this.batchSize = batchSize;
        this.step = step;
        this.attemptLimit = attemptLimit;
        System.debug('Batch PRM UserMigration inputs: step='+this.Step+', bacth size='+this.batchSize+', createUser='+createUser+', createPsetAssign='+createPsetAssign+', createMember='+createMember+', checkMemberProperties='+checkMemberProperties+', checkGamify='+checkGamify+', hashTag='+hashTag);
        /*
        Construct the Where filter based on input parameters
        */
        String andWhere ='';
        if((step==0 ) && createUser){
            andWhere+=' and UserId__c=null';
        }else if((step==1) && createPsetAssign){
            andWhere+=' and PermissionSetAssingnId__c=null';
        }
        else if(step==2 && createMember){
            andWhere+=' and MemberId__c=null';
        }
        else if(step==3 && checkMemberProperties){
            andWhere+=' and MemberId__c!=null and MemberPropertiesStatus__c=\''+AP_PRMUserMigrationV2.MEMBER_PROPERTIES_STATUS_NOT_CHECKED+'\'';
        }else if(step==4 && checkGamify){
            andWhere+=' and MemberId__c!=null and MemberPropertiesStatus__c=\''+AP_PRMUserMigrationV2.MEMBER_PROPERTIES_STATUS_CHECKED_AND_MPA+'\'';
        }else{
            //Nothing to do 
            andWhere =' and Id=null';
        }
        andWhere+=hashTag!=null && hashTag!=''?' and (Hashtags__c like \'%'+hashTag+' %\' OR Hashtags__c like  \'%'+hashTag+'\')':'';

        String orderBy =' ORDER BY Priority__c DESC';
        query='SELECT Id,Contact__c,ErrorMessage__c,hasError__c,Hashtags__c,MemberId__c,UserId__c,PermissionSetAssingnId__c,MemberPropertiesStatus__c,TryCount__c from PRMPartnerMigration__c where hasError__c=false and Status__c!=\'Completed\'' + andWhere + orderBy;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<PRMPartnerMigration__c> scope) {
        System.debug('#Execute Batch_PRM_UserMigration for Step:'+this.Step);
        if(scope.size()>0){
            Map<Id,PRMPartnerMigration__c>  prmPartnerMigrationByContactId = new Map<Id,PRMPartnerMigration__c>();
            for(PRMPartnerMigration__c prmPartnerMigration: scope){
                prmPartnerMigrationByContactId.put(prmPartnerMigration.Contact__c,prmPartnerMigration);
                if(this.Step==0)
                    prmPartnerMigration.TryCount__c=prmPartnerMigration.TryCount__c==null?1:prmPartnerMigration.TryCount__c+1;
            }
        	try{
	            
	            if(this.Step==0){
	                AP_PRMUserMigrationV2.createUser(prmPartnerMigrationByContactId);
	            }else if(this.Step==1){
	                AP_PRMUserMigrationV2.createPermissionSet(prmPartnerMigrationByContactId);
	            }else if(this.Step==2){
	                AP_PRMUserMigrationV2.createFieloMember(prmPartnerMigrationByContactId);
	            }else if(this.Step==3){
	                AP_PRMUserMigrationV2.createMPA(prmPartnerMigrationByContactId);
	            }else if(this.Step==4){
	                AP_PRMUserMigrationV2.generateOutputForMPA(prmPartnerMigrationByContactId);		    		
	            }
	            if(Step!=0 && Step!=1){
	            	update prmPartnerMigrationByContactId.values();
	            }else{
                    this.scopeCopy = prmPartnerMigrationByContactId.values();
                }
            }
            catch(Exception e){
            	for(PRMPartnerMigration__c prmPartnerMigration: scope){
	                AP_PRMUserMigrationV2.createSingleErrorForPRMPartnerMigration(prmPartnerMigration,'#GlobalException: '+e.getMessage()+'\r\n'+e.getStackTraceString(),this.step);
	            }
	            if(Step==0 || Step==1){
	            	this.scopeCopy = prmPartnerMigrationByContactId.values();
	            }else{
                    update prmPartnerMigrationByContactId.values();
                }
            }
        }
        
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('scopeCopy:'+scopeCopy);
        if(this.scopeCopy!=null){
            update scopeCopy;
            this.scopeCopy =null;
        }
        if(step<MAXIMUM_STEPS_IN_CHAIN ){
            step++;
            System.debug('Batch PRM UserMigration is chained with inputs: step='+this.Step+', bacth size='+this.batchSize+', createUser='+createUser+', createPsetAssign='+createPsetAssign+', createMember='+createMember+', checkMemberProperties='+checkMemberProperties+', checkGamify='+checkGamify+', hashTag='+hashTag);
            Batch_PRM_UserMigration batchPRMUserMigration = new Batch_PRM_UserMigration(this.createUser,this.createPsetAssign,this.createMember,this.checkMemberProperties,this.checkGamify,this.hashTag,this.Step,this.batchSize,this.attempt); 
            if(!Test.isRunningTest()) Database.executebatch(batchPRMUserMigration);
        } else if(this.attempt<this.attemptLimit ){
            this.attempt++;
            this.step = 0;
            Batch_PRM_UserMigration batchPRMUserMigration = new Batch_PRM_UserMigration(this.createUser,this.createPsetAssign,this.createMember,this.checkMemberProperties,this.checkGamify,this.hashTag,this.Step,this.batchSize,this.attempt); 
            if(!Test.isRunningTest()) Database.executebatch(batchPRMUserMigration);
        }
    }
    
}