/*
    
    Author:Amitava Dutta
    Created On:29/12/2010.
    Purpose:To set the global constant value to be set accross the classes
    RE: SR0+ Prototype
*/

global with sharing class WS02_SetConstantValue
{
    //region Static Members
    public static string mstrAgreementID;
    public static boolean iscreateseries = false;
    public static integer totalcount = 0;
    //end region   
    
    //region Static Properties
    public static String AgreementID
    {
        get{return mstrAgreementID;}set{mstrAgreementID= value;}
    }   
    //end region
    
    
     
   
}