public with sharing class AppServices {    
    public Id newappid{get;set;}
    public String newappname{get;set;}
    public App__c currentApp;
    public App__c newApp{get;set;}
    
    public AppServices(ApexPages.StandardController controller){
        currentApp=(App__c)controller.getRecord();
        //dummy method
    }    
    
    without sharing class DeleteRecord {
        public void deleterecords(App__c app){
            List<Record__c> records = [select Id from Record__c where app__c=:app.Id and OwnerId=:UserInfo.getUserId()];
            if(records.size()>0)
                delete records;    
        }
    }
    
    public PageReference deleterecords(){
		DeleteRecord deleteRecordInstance = new DeleteRecord();
        deleteRecordInstance.deleterecords(currentApp);        
        return null;        
    }
    public PageReference cloneapp(){
        CurrentApp = [Select Id,Name,logo__c,ObjectShortcut__c, Description__c,Owner.Name, Status__c,wikilink__c,(select Id, Name, RecordTypeId, actionvalueforapprove__c, actionvalueforreject__c, approvebuttonlabel__c, entrycondition__c, ownerselectionforthisstep__c, predefinedgroup__c, predefineduser__c, rejectbuttonlabel__c, statusvalueonentrytothisstep__c, statusvalueonrejectonthisstep__c, stepnumber__c,sendtoprevioussteplabel__c from AppSteps__r),(select Id, Name, RecordTypeId, app__c, appdatafieldmap__c, controllingfield__c, displayname__c, fields__c, fieldtype__c, helptext__c, isdependent__c, layoutposition__c, isreadonly__c, position__c, referencefield__c, referencelookup__c, referencelookupfield__c, required__c, section__c, step__c, targetobject__c, updatablethroughactiononly__c, updateparentonchange__c from AppFields__r),(select Id, app__c,Name, listviewtranslated__c, listviewdefinition__c from AppViews__r ) from App__c where Id=:currentApp.Id];
        List<ofwgroup__c> groups = [Select Id,Name,(select User__c,Group__c from GroupAppUserJunction__r) from ofwgroup__c where app__c=:CurrentApp.Id];        
        newApp = CurrentApp.clone(false,true,true,true);
        newApp.Name +='_isCloned_'+Datetime.now();
        newApp.TECH_MigrationUniqueID__c=null;
        newApp.Status__c='Draft';
        newApp.OwnerId = UserInfo.getUserId();
        newApp.ObjectShortcut__c=newApp.ObjectShortcut__c!=null?newApp.ObjectShortcut__c+'_cln':'';
        insert newApp;
        Map<Id,Id> oldnewgroupId = new Map<Id,Id>();
        for(ofwgroup__c groupa : groups){
            ofwgroup__c groupclone = groupa.clone(false,true,true,true);
            groupclone.App__c=newApp.Id;
            insert groupclone;
            oldnewgroupId.put(groupa.Id,groupclone.Id);
            List<GroupAppUserJunction__c> junctions=new List<GroupAppUserJunction__c>();
            for(GroupAppUserJunction__c junction : groupa.GroupAppUserJunction__r)
            {
                GroupAppUserJunction__c groupjunc =  junction.clone(false,true,true,true);
                groupjunc.Group__c=groupclone.Id;
                junctions.add(groupjunc);
            }
            insert junctions;
        }
        List<Step__c> appsteps = new List<Step__c>();
        Map<Integer,Step__c> stepnumbermap=new Map<Integer,Step__c>();
        for(Step__c appstep: CurrentApp.AppSteps__r){
            Step__c newappstep = appstep.clone(false,true,true,true);
            newappstep.TECH_MigrationUniqueID__c=null;
            stepnumbermap.put(Integer.valueOf(appstep.stepnumber__c),appstep);
            newappstep.app__c = newApp.Id;
            if(newappstep.predefinedgroup__c!=null)
                newappstep.predefinedgroup__c=oldnewgroupId.get(appstep.predefinedgroup__c);
            appsteps.add(newappstep);
        }
        insert appsteps;
        Map<Id,Id> oldnewsteps = new Map<Id,Id>();
        for(Step__c appstep:appsteps){            
            oldnewsteps.put(stepnumbermap.get(Integer.valueOf(appstep.stepnumber__c)).Id,appstep.Id);            
        }
        List<Field__c> newfields = new List<Field__c>();
        Map<Id,Id> oldnewfieldsmap = new Map<Id,Id>();
        for(Field__c field: CurrentApp.AppFields__r){            
            if(field.fieldtype__c!='Reference'){
                Field__c newfield = field.clone(false,true,true,true);
	            newfield.TECH_MigrationUniqueID__c=null;
                newfield.step__c = oldnewsteps.get(newfield.step__c);
                newfield.app__c = newApp.Id;
                if(newfield.fieldtype__c=='Picklist' || newfield.fieldtype__c=='Multiselect Picklist' || newfield.fieldtype__c=='Lookup'){
                    insert newfield; 
                    oldnewfieldsmap.put(field.Id,newfield.Id);
                }
                else
                newfields.add(newfield);
            }
        }
        insert newfields;
        List<Field__c> morefields = new List<Field__c>();
        for(Field__c field: CurrentApp.AppFields__r){            
         	 if(field.fieldtype__c == 'Reference'){
                 Field__c newfield = field.clone(false,true,true,true);
                 newfield.TECH_MigrationUniqueID__c=null;
                 newfield.step__c = oldnewsteps.get(newfield.step__c);
                 newfield.app__c = newApp.Id;
                 newfield.referencelookupfield__c=oldnewfieldsmap.get(field.referencelookupfield__c);
                 morefields.add(newfield);
             }
        }
        System.debug(morefields);
        List<Database.SaveResult> sr = Database.insert(morefields);
        System.debug(sr);
        
        List<FieldValue__c> fieldvalues = new List<FieldValue__c>();
        for(FieldValue__c fieldvalue : [select id,name,isdefault__c,Field__c from FieldValue__c where Field__c in :oldnewfieldsmap.keyset()]){
            FieldValue__c fieldvalueclone = fieldvalue.clone(false,true,true,true);
            fieldvalueclone.Field__c = oldnewfieldsmap.get(fieldvalue.Field__c);
            fieldvalues.add(fieldvalueclone);
        }
        insert fieldvalues;
        

		List<View__c> views = new List<View__c>();
        for(View__c view: CurrentApp.AppViews__r){
           View__c newview =  view.clone(false,true,true,true);
            newview.app__c = newApp.Id;
            newview.TECH_MigrationUniqueID__c=null;
            views.add(newview);
        }      
        insert views;        
        return null;
    }
    
    @RemoteAction
    public static List<App__c> getApps(){
        return [SELECT Id,Name,logo__c, Description__c,Owner.Name, Status__c,wikilink__c,downloadablegroup__c   FROM App__c];
    } 
    
    @AuraEnabled
    public static List<App__c> getApps1(){
        return [SELECT Id,Name,logo__c, Description__c,Owner.Name, Status__c,wikilink__c,downloadableGroup__c   FROM App__c];
    }   
    
    @AuraEnabled
    public static List<View__c> getViews(String appId){
        return [SELECT Id,Name,app__r.downloadablegroup__c FROM View__c where app__c=:appId];
    }
    
    @AuraEnabled
    public static String getDataForView(String viewId){
        List<Record__c> results = new List<Record__c>();
        View__c view = [SELECT Id,  Name, listviewdefinition__c, app__c FROM View__c where Id=:viewId];        
        Type mytype = Type.forName('','ListViewDefinition');
        ListViewDefinition definition;
        if(view.listviewdefinition__c!=null)
            definition = (ListViewDefinition)JSON.deserialize(view.listviewdefinition__c,mytype);        
        //do some processing to build query
        List<Field__c> fielddefs = [select appdatafieldmap__c,displayname__c,fieldtype__c from Field__c where step__r.app__c =:view.app__c and appdatafieldmap__c!=null ];
        List<String> fieldsinquery = new List<String>();
        Map<String,Object> returnresult = new Map<String,Object>();
        for (Field__c field : fielddefs) {                
            fieldsinquery.add(field.appdatafieldmap__c);            
        } 
        String querytobeused = 'select Id,Name,status__c,iscurrentowner__c,step__r.RecordType.Name,LastModifiedById,LastModifiedBy.Name,LastModifiedDate,CreatedById,CreatedBy.Name,CreatedDate,action__c,';
        SimpleView sv;
        if(definition==null)
        {           
            querytobeused += String.join(fieldsinquery,',')+' from Record__c where app__c = \''+view.app__c+'\' order by LastModifiedDate DESC';                       
            System.debug(querytobeused);                        
            results = Database.query(querytobeused);            
            sv = new SimpleView(results,fielddefs);
        }
        else{            
            List<String> queryconditions = new List<String>();
            if(definition.statuscondition!=null)
                queryconditions.add('status__c=\''+definition.statuscondition+'\'');
            for(ListViewDefinition.Condition condition:definition.conditions)
            {
                if(condition.type=='equals'){
                    if(condition.fieldtype=='Number')
                    {
                        queryconditions.add(condition.appdatafieldmap+'='+condition.value);
                    }
                    else
                    {
                        queryconditions.add(condition.appdatafieldmap+'=\''+condition.value+'\'');
                    }
                }
                if(condition.type=='greaterthan'){
                    if(condition.fieldtype=='Number')
                    {
                        queryconditions.add(condition.appdatafieldmap+'>'+condition.value);
                    }                    
                }
            }
            queryconditions.add('app__c=\''+view.app__c+'\'');
            querytobeused += String.join(definition.columns, ',')+' from Record__c where '+ String.join(queryconditions,' AND ') + ' order by LastModifiedDate DESC';
            List<Field__c> fielddefssub = new List<Field__c>();
            Map<String,Field__c> appdatatofieldmap = new Map<String,Field__c>();
            for (Field__c field : fielddefs) {                
                appdatatofieldmap.put(field.appdatafieldmap__c,field);                  
            }
            for(String column : definition.columns){
                fielddefssub.add(appdatatofieldmap.get(column));     
            }
            System.debug(querytobeused);                        
            results = Database.query(querytobeused);            
            sv = new SimpleView(results,fielddefssub);
        }
        
        return JSON.serialize(sv);        
    }       
    
    public class SimpleView{
        public List<Record__c> data;
        public List<Field__c> fields;
        public SimpleView(List<Record__c> data,List<Field__c> fields){
            this.data = data;
            this.fields = fields;
        }
    }
    
    @AuraEnabled
    public static Boolean getDownloadAccess(Id appId,Id groupId){
        Integer hasuser = [select count() from groupappuserjunction__c where group__c=:groupId and user__c=:UserInfo.getUserId() and group__r.app__c=:appId];
        System.debug(hasuser);
        if(hasuser>0)
            return true;	    
        else
            return false;
    }
}