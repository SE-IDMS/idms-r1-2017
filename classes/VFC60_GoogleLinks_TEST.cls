/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | VFC60_GoogleLinks_TEST                                                             |
|                       |                                                                                    |
|     - Object(s)       | Account, Contact                                                                   |
|     - Description     |   - Test method for Google Map links                                               |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | October, 28th 2011                                                                 |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
@isTest
private class VFC60_GoogleLinks_TEST
{
    static testMethod void testVFC60_GoogleLinks()
    {
       System.debug('#### START test method for VFC60_GoogleLinks ####');

       //Create unit country 
       Country__c country = Utils_TestMethods.createCountry();
       insert country;

       //Create unit stateProv 
       StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
       insert stateProv;

       ///////////////////////////////////////////
       //1st test : Account - all fields are set//
       ///////////////////////////////////////////
       //Create account
       Account acc1 = new Account(Name               = 'TEST1',
                                  AccType__c         = 'PR',
                                  LeadingBusiness__c = 'EN',
                                  ClassLevel1__c     = 'XX',
                                  Street__c          = 'Street1',
                                  City__c            = 'CITY1',
                                  ZipCode__c         = 'ZIPCODE1',
                                  StateProvince__c   = stateProv.Id,
                                  Country__c         = country.Id,
                                  POBox__c           = 'POBOX1');
       Database.Saveresult resAccount1 = Database.insert(acc1);
       System.Debug('## >>>>>>>>>>>>> resAccount1 = ' + resAccount1);

       PageReference vfPage1 = Page.VFP60_GoogleMap;
       Test.setCurrentPage(vfPage1);

       // Add parameters to page URL
       ApexPages.currentPage().getParameters().put('Id', resAccount1.getId());
       ApexPages.currentPage().getParameters().put('sobject', 'Account');       

       VFC60_GoogleLinks vfCtrl1 = new VFC60_GoogleLinks();
       vfCtrl1.redirectToGoogleMap();

       //////////////////////////////////////////////////
       //2nd test : Account - only a few fields are set//
       //////////////////////////////////////////////////
       //Create account
       Account acc2 = new Account(Name               = 'TEST2',
                                  AccType__c         = 'PR',
                                  LeadingBusiness__c = 'EN',
                                  ClassLevel1__c     = 'XX',
                                  City__c            = 'CITY',
                                  ZipCode__c         = 'ZIPCODE',
                                  Country__c         = country.Id,
                                  POBox__c           = 'POBOX');
       Database.Saveresult resAccount2 = Database.insert(acc2);
       System.Debug('## >>>>>>>>>>>>> resAccount2 = ' + resAccount2);

       PageReference vfPage2 = Page.VFP60_GoogleMap;
       Test.setCurrentPage(vfPage2);

       // Add parameters to page URL
       ApexPages.currentPage().getParameters().put('Id', resAccount2.getId());
       ApexPages.currentPage().getParameters().put('sobject', 'Account');       

       VFC60_GoogleLinks vfCtrl2 = new VFC60_GoogleLinks();
       vfCtrl2.redirectToGoogleMap();

       ///////////////////////////////////////////
       //3rd test : Contact - all fields are set//
       ///////////////////////////////////////////
       //Create contact
       Contact con1 = new Contact(FirstName          = 'FIRSTNAME',
                                  LastName           = 'LASTNAME',
                                  Email              = 'firstname.lastname@yahoo.fr',
                                  AccountId          = resAccount1.getId(),
                                  CorrespLang__c     = 'FR',
                                  Street__c          = 'Street1',
                                  City__c            = 'CITY1',
                                  ZipCode__c         = 'ZIPCODE1',
                                  StateProv__c       = stateProv.Id,
                                  Country__c         = country.Id);
       Database.Saveresult resContact1 = Database.insert(con1);
       System.Debug('## >>>>>>>>>>>>> resContact1 = ' + resContact1);

       PageReference vfPage3 = Page.VFP60_GoogleMap;
       Test.setCurrentPage(vfPage3);

       // Add parameters to page URL
       ApexPages.currentPage().getParameters().put('Id', resContact1.getId());
       ApexPages.currentPage().getParameters().put('sobject', 'Contact');       

       VFC60_GoogleLinks vfCtrl3 = new VFC60_GoogleLinks();
       vfCtrl3.redirectToGoogleMap();

       //////////////////////////////////////////////////
       //4th test : Contact - address is not filled in //
       //////////////////////////////////////////////////
       //Create contact
       Contact con2 = new Contact(FirstName          = 'FIRSTNAME2',
                                  LastName           = 'LASTNAME2',
                                  Email              = 'firstname2.lastname2@yahoo.fr',
                                  AccountId          = resAccount1.getId(),
                                  CorrespLang__c     = 'FR');
       Database.Saveresult resContact2 = Database.insert(con2);
       System.Debug('## >>>>>>>>>>>>> resContact2 = ' + resContact2);

       PageReference vfPage4 = Page.VFP60_GoogleMap;
       Test.setCurrentPage(vfPage4);

       // Add parameters to page URL
       ApexPages.currentPage().getParameters().put('Id', resContact2.getId());
       ApexPages.currentPage().getParameters().put('sobject', 'Contact');       

       VFC60_GoogleLinks vfCtrl4 = new VFC60_GoogleLinks();
       vfCtrl4.redirectToGoogleMap();

       System.debug('#### END   test method for VFC60_GoogleLinks ####');
    }
}