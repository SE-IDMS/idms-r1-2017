@isTest
public class AppServices_TEST {
	private static testMethod void cloneapp_Test() {
		App__c objApp = new App__c();
		objApp.Name 				= 'TestAppName';
		objApp.Description__c 		= 'TestAppDescription';
		objApp.logo__c 				= 'https://salesforc.com';
		objApp.ObjectLabel__c 		= 'TestAppObjectLabelA';
		objApp.ObjectPluralLabel__c = 'TestAppObjectLabelAs';
		objApp.ObjectShortcut__c 	= 'XYZ';
		objApp.parentapp__c 		=  null;
		objApp.Status__c 			=  'Approved';
		objApp.wikilink__c			= 'https://mywiki.com';
		insert objApp; 
        
        Step__c objStep = new Step__c();
        objStep.app__c = objApp.Id;
        objStep.stepnumber__c = 1;
        objStep.Name ='testStep one';
        objStep.statusvalueonentrytothisstep__c ='Draft';
        objStep.actionvalueforapprove__c = 'started';
        
        insert objStep;
        
        Field__c txtfld=new Field__c();
        txtfld.step__c=objStep.Id;
        txtfld.layoutposition__c=1;
        txtfld.fieldtype__c = 'Text';
        Insert txtfld;
        
        txtfld.app__c = null;
        update txtfld;
        
        
        Field__c Picklistfield = new Field__c();
        Picklistfield.step__c = objStep.Id;
        Picklistfield.fieldtype__c='Picklist';
        Picklistfield.layoutposition__c = 1;
        Picklistfield.app__c = objApp.Id;
        insert Picklistfield;
        
        Field__c Textfield = new Field__c();
        Textfield.step__c = objStep.Id;
        Textfield.fieldtype__c='Text';
        Textfield.layoutposition__c = 1;
        Textfield.app__c = objApp.Id;
        insert Textfield;
        
        Field__c Lookupfield = new Field__c();
        Lookupfield.step__c = objStep.Id;
        Lookupfield.fieldtype__c='Lookup';
        Lookupfield.layoutposition__c = 1;
        Lookupfield.app__c = objApp.Id;
        insert Lookupfield;
        
        Field__c Referencefield = new Field__c();
        Referencefield.step__c = objStep.Id;
        Referencefield.fieldtype__c='Reference';
        Referencefield.layoutposition__c = 1;
        Referencefield.app__c = objApp.Id;
        insert Referencefield;
        
        Field__c Filefield = new Field__c();
        Filefield.step__c = objStep.Id;
        Filefield.fieldtype__c='File';
        Filefield.layoutposition__c = 1;
        Filefield.app__c = objApp.Id;
        insert Filefield;
        
        Field__c MultiselectPicklistfield = new Field__c();
        MultiselectPicklistfield.step__c = objStep.Id;
        MultiselectPicklistfield.fieldtype__c='Multiselect Picklist';
        MultiselectPicklistfield.layoutposition__c = 1;
        MultiselectPicklistfield.app__c = objApp.Id;
        insert MultiselectPicklistfield;        
        
        Field__c NumberField1 = new Field__c();
        NumberField1.step__c = objStep.Id;
        NumberField1.app__c = objApp.Id;
        NumberField1.fieldtype__c='Number';
        NumberField1.layoutposition__c = 1;
        insert NumberField1;
        
        Field__c NumberField2 = new Field__c();
        NumberField2.step__c = objStep.Id;
        NumberField2.app__c = objApp.Id;
        NumberField2.fieldtype__c='Number';
        NumberField2.layoutposition__c = 1;
        insert NumberField2;
        
         Field__c NumberField3 = new Field__c();
        NumberField3.step__c = objStep.Id;
        NumberField3.app__c = objApp.Id;
        NumberField3.fieldtype__c='Number';
        NumberField3.layoutposition__c = 1;
        insert NumberField3;
        
        Field__c DateField1 = new Field__c();
        DateField1.step__c = objStep.Id;
        DateField1.app__c = objApp.Id;
        DateField1.fieldtype__c='Date';
        DateField1.layoutposition__c = 1;
        insert DateField1;
        
        Field__c DateField2 = new Field__c();
        DateField2.step__c = objStep.Id;
        DateField2.app__c = objApp.Id;
        DateField2.fieldtype__c='Date';
        DateField2.layoutposition__c = 1;
        insert DateField2;
        
        Field__c TimeField1 = new Field__c();
        TimeField1.step__c = objStep.Id;
        TimeField1.app__c = objApp.Id;
        TimeField1.fieldtype__c='Date/Time';
        TimeField1.layoutposition__c = 1;
        insert TimeField1;
        
        Field__c TimeField2 = new Field__c();
        TimeField2.step__c = objStep.Id;
        TimeField2.app__c = objApp.Id;
        TimeField2.fieldtype__c='Date/Time';
        TimeField2.layoutposition__c = 1;
        insert TimeField2;
        
        Field__c CurrencyField1 = new Field__c();
        CurrencyField1.step__c = objStep.Id;
        CurrencyField1.app__c = objApp.Id;
        CurrencyField1.fieldtype__c='Currency';
        CurrencyField1.layoutposition__c = 1;
        insert CurrencyField1;
        
        Field__c CurrencyField2 = new Field__c();
        CurrencyField2.step__c = objStep.Id;
        CurrencyField2.app__c = objApp.Id;
        CurrencyField2.fieldtype__c='Currency';
        CurrencyField2.layoutposition__c = 1;
        insert CurrencyField2;
        
        Field__c CheckboxField1 = new Field__c();
        CheckboxField1.step__c = objStep.Id;
        CheckboxField1.app__c = objApp.Id;
        CheckboxField1.fieldtype__c='Checkbox';
        CheckboxField1.layoutposition__c = 1;
        insert CheckboxField1;
        
        Field__c CheckboxField2 = new Field__c();
        CheckboxField2.step__c = objStep.Id;
        CheckboxField2.app__c = objApp.Id;
        CheckboxField2.fieldtype__c='Checkbox';
        CheckboxField2.layoutposition__c = 1;
        insert CheckboxField2;
        
        Field__c TextareaField1 = new Field__c();
        TextareaField1.step__c = objStep.Id;
        TextareaField1.app__c = objApp.Id;
        TextareaField1.fieldtype__c='Textarea';
        TextareaField1.layoutposition__c = 1;
        insert TextareaField1;
        
        Field__c TextareaField2 = new Field__c();
        TextareaField2.step__c = objStep.Id;
        TextareaField2.app__c = objApp.Id;
        TextareaField2.fieldtype__c='Textarea';
        TextareaField2.layoutposition__c = 1;
        insert TextareaField2;
        
        ofwgroup__c group1 = new ofwgroup__c();
        group1.Name = 'Test group';
        group1.App__c = objApp.Id;
        Insert group1;
        
        groupappuserjunction__c groupjunction = new groupappuserjunction__c();
        groupjunction.group__c = group1.Id;
        groupjunction.User__c = UserInfo.getUserId();
        insert groupjunction;

        ApexPages.StandardController AppStandardController = new ApexPages.StandardController(objApp);   
    	AppServices objAppServices = new AppServices(AppStandardController);
    	PageReference objPageRefAppDeepClone = Page.VFP_AppDeepClone;
    	Test.setCurrentPage(objPageRefAppDeepClone);
    	objAppServices.cloneapp();

    	//AppServices.newappid=objApp.id;
    	//AppServices.newappname = 'TestAppName';

	}
    
	private static testMethod void getApps_Test() {
		App__c objApp = new App__c();
		objApp.Name 				= 'TestAppName';
		objApp.Description__c 		= 'TestAppDescription';
		objApp.logo__c 				= 'https://salesforc.com';
		objApp.ObjectLabel__c 		= 'TestAppObjectLabelA';
		objApp.ObjectPluralLabel__c = 'TestAppObjectLabelAs';
		objApp.ObjectShortcut__c 	= 'XYZ';
		objApp.parentapp__c 		=  null;
		objApp.Status__c 			=  'Approved';
		objApp.wikilink__c			= 'https://mywiki.com';
		insert objApp; 

		AppServices.getApps();
	}

	private static testMethod void getApps1_Test() {
		App__c objApp = new App__c();
		objApp.Name 				= 'TestAppName';
		objApp.Description__c 		= 'TestAppDescription';
		objApp.logo__c 				= 'https://salesforc.com';
		objApp.ObjectLabel__c 		= 'TestAppObjectLabelA';
		objApp.ObjectPluralLabel__c = 'TestAppObjectLabelAs';
		objApp.ObjectShortcut__c 	= 'XYZ';
		objApp.parentapp__c 		=  null;
		objApp.Status__c 			=  'Approved';
		objApp.wikilink__c			= 'https://mywiki.com';
		insert objApp; 

		AppServices.getApps1();
	}

	private static testMethod void getViews_Test() {
		App__c objApp = new App__c();
		objApp.Name 				= 'TestAppName';
		objApp.Description__c 		= 'TestAppDescription';
		objApp.logo__c 				= 'https://salesforc.com';
		objApp.ObjectLabel__c 		= 'TestAppObjectLabelA';
		objApp.ObjectPluralLabel__c = 'TestAppObjectLabelAs';
		objApp.ObjectShortcut__c 	= 'XYZ';
		objApp.parentapp__c 		=  null;
		objApp.Status__c 			=  'Approved';
		objApp.wikilink__c			= 'https://mywiki.com';
		insert objApp; 

		View__c objView = new View__c();
		objView.Name='MyTestView';
		objView.app__c = objApp.id;
		insert objView;

		AppServices.getViews(objApp.Id);
	}

	private static testMethod void getDataForView1_Test() {
		App__c objApp = new App__c();
		objApp.Name 				= 'TestAppName';
		objApp.Description__c 		= 'TestAppDescription';
		objApp.logo__c 				= 'https://salesforc.com';
		objApp.ObjectLabel__c 		= 'TestAppObjectLabelA';
		objApp.ObjectPluralLabel__c = 'TestAppObjectLabelAs';
		objApp.ObjectShortcut__c 	= 'XYZ';
		objApp.parentapp__c 		=  null;
		objApp.Status__c 			=  'Approved';
		objApp.wikilink__c			= 'https://mywiki.com';
		insert objApp; 

		View__c objView = new View__c();
		objView.Name='MyTestView';
		objView.app__c = objApp.id;
		objView.listviewdefinition__c ='{"conditions":[],"statuscondition":"Draft","columns":["textfield3__c"]}';
		insert objView;
        ListViewDefinition mydefinition = new ListViewDefinition();
        mydefinition.initialize();
        
		Step__c objStep = new Step__c();
		objStep.app__c = objApp.Id;
		objStep.stepnumber__c = 1;
		objStep.Name ='testStep one';
		objStep.statusvalueonentrytothisstep__c ='Draft';
		objStep.actionvalueforapprove__c = 'started';

		insert objStep;

 		Field__c objField=new Field__c();
        objField.app__c=objApp.Id;
        objField.step__c=objStep.Id;
        objField.layoutposition__c=1;
        objField.appdatafieldmap__c = 'textfield3__c';
        objField.fieldtype__c ='Picklist';
        objField.required__c=false;
        Insert objField;

		AppServices.getDataForView(objView.Id);
	}

	private static testMethod void getDataForView2_Test() {
		App__c objApp = new App__c();
		objApp.Name 				= 'TestAppName';
		objApp.Description__c 		= 'TestAppDescription';
		objApp.logo__c 				= 'https://salesforc.com';
		objApp.ObjectLabel__c 		= 'TestAppObjectLabelA';
		objApp.ObjectPluralLabel__c = 'TestAppObjectLabelAs';
		objApp.ObjectShortcut__c 	= 'XYZ';
		objApp.parentapp__c 		=  null;
		objApp.Status__c 			=  'Approved';
		objApp.wikilink__c			= 'https://mywiki.com';
		insert objApp; 

		View__c objView = new View__c();
		objView.Name='MyTestView';
		objView.app__c = objApp.id;
		//objView.listviewdefinition__c ='{"conditions":[],"columns":["textfield3__c"]}';
		insert objView;

		Step__c objStep = new Step__c();
		objStep.app__c = objApp.Id;
		objStep.stepnumber__c = 1;
		objStep.Name ='testStep One';
		objStep.statusvalueonentrytothisstep__c ='Draft';
		objStep.actionvalueforapprove__c = 'started';

		insert objStep;

 		Field__c objField=new Field__c();
        objField.app__c=objApp.Id;
        objField.step__c=objStep.Id;
        objField.layoutposition__c=1;
        objField.appdatafieldmap__c = 'textfield3__c';
        objField.fieldtype__c ='Picklist';
        objField.required__c=false;
        Insert objField;

        Record__c objRecord = new Record__c();
        objRecord.textfield1__c ='OM';
        objRecord.step__c = objStep.id;
        objRecord.app__c = objApp.Id;
        objRecord.name = 'OR-STEP1';
        insert objRecord;


		AppServices.getDataForView(objView.Id);
	}
    private static testMethod void deleteRecords_TEST() {
		App__c objApp = new App__c();
		objApp.Name 				= 'TestAppName';
		objApp.Description__c 		= 'TestAppDescription';
		objApp.logo__c 				= 'https://salesforc.com';
		objApp.ObjectLabel__c 		= 'TestAppObjectLabelA';
		objApp.ObjectPluralLabel__c = 'TestAppObjectLabelAs';
		objApp.ObjectShortcut__c 	= 'XYZ';
		objApp.parentapp__c 		=  null;
		objApp.Status__c 			=  'Approved';
		objApp.wikilink__c			= 'https://mywiki.com';
		insert objApp; 
        
        Step__c objStep = new Step__c();
        objStep.app__c = objApp.Id;
        objStep.stepnumber__c = 1;
        objStep.Name ='testStep one';
        objStep.statusvalueonentrytothisstep__c ='Draft';
        objStep.actionvalueforapprove__c = 'started';
        
        insert objStep;
        
        Field__c objField=new Field__c();
        objField.app__c=objApp.Id;
        objField.step__c=objStep.Id;
        objField.layoutposition__c=1;
        objField.appdatafieldmap__c = 'textfield3__c';
        objField.fieldtype__c ='Picklist';
        objField.required__c=false;
        Insert objField;

		Record__c record = new Record__c(Name='ofw-123');
        record.step__c = objStep.Id;
        record.app__c = objApp.Id;
        record.textfield3__c='sid';
        record.status__c = 'Draft';
        insert record;
        
		ApexPages.CurrentPage().getparameters().put('id', objApp.id);
        ApexPages.StandardController sc = new ApexPages.standardController(objApp);
        AppServices sic = new AppServices(sc);	
		sic.deleterecords();
	}
    
    

}