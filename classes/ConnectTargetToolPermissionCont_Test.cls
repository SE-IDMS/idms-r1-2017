/*
    Author          : Gayathri Shivakumar (Schneider Electric)
    Date Created    : 30-sep-2013
    Description     : Test Class for Connect Target Tool Permission
*/

@isTest
private class ConnectTargetToolPermissionCont_Test {
    static testMethod void testConnectTargetToolPermissionController() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('conP');
                System.runAs(runAsUser){
                
                 //Use the PageReference Apex class to instantiate 
        PageReference pageRef = Page.Targets_Permission_Report;
       
       //In this case, the Visualforce page named 'Targets_Permission_Report' is the starting point of this Test method. 
        Test.setCurrentPage(pageRef);
        
  // Insert Data to KPI Objects
  
   Global_KPI_Targets__c GKPIT=new Global_KPI_Targets__c(KPI_Name__c='testKPI', KPI_Acronym__c = 'DML',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email, Transformation__c = Label.ConnectTransPeople, Global_Business__c = 'Industry;Partner-Division;'+Label.Connect_Smart_Cities,Global_Functions__c='GM;GSC-Regional',Operations_Region__c='NA',GSC_Regions__c='NA',Smart_Cities_Division__c='test',Partner_Sub_Entity__c='test',Year__c='2013');
         insert GKPIT;
         
          Global_KPI_Targets__c GKPIT2=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email, Transformation__c = Label.ConnectTransPeople,Global_Business__c = 'Industry',Global_Functions__c='GM',Operations_Region__c='NA');
         insert GKPIT2;
         
         Cascading_KPI_Target__c EKPI = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target_Q4__c = '10', Scope__c = 'Global Business', Entity__c = 'Industry' );
         insert EKPI;
         Cascading_KPI_Target__c EKPI1 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Functions', Entity__c = 'GSC-Regional',GSC_Region__c='NA' );
         insert EKPI1;
         Cascading_KPI_Target__c EKPI2 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = Label.Connect_Smart_Cities,Smart_Cities_Division__c='test' );
         insert EKPI2;
         Cascading_KPI_Target__c EKPI3 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'Partner-Division',Partner_Sub_Entity__c='test' );
         insert EKPI3;
         
         Global_KPI_Targets__c GKPITC=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,CountryPickList__c = 'IN-INDIA',Year__c='2013');
         insert GKPITC;
         
         Global_KPI_Targets__c GKPIT4=new Global_KPI_Targets__c(KPI_Name__c='testKPI',KPI_Acronym__c = 'DMLC',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,KPI_Type__c = 'Country KPI', CountryPickList__c='NA-Canada;CN-China;NA-Mexico;IN-India');
         insert GKPIT4;
         Cascading_KPI_Target__c EKPI4 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT4.id,Entity_KPI_Target_Q4__c = '10',CountryPickList__c ='NA-Canada' );
         insert EKPI4;
         Cascading_KPI_Target__c EKPI5 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT4.id,Entity_KPI_Target_Q4__c = '10',CountryPickList__c ='CN-China' );
         insert EKPI5;

 //Instantiate and construct the controller class.   

        ConnectTargetToolPermissionController controller = new ConnectTargetToolPermissionController();
        controller.Type = 'Global KPI';
        controller.Acronym = 'DML';
        controller.getFilterList();
        controller.getFilterAcronym();
        controller.getFilterYear();
        controller.getFilterType();
        controller.FilterChange();
        controller.TypeChange();
        controller.AcronymChange();
        controller.YearChange();
        controller.ReportData();
        
        controller.Type = 'Country KPI';
        controller.Acronym = 'DMLC';
        controller.getFilterList();
        controller.getFilterAcronym();
        controller.getFilterYear();
        controller.getFilterType();
        controller.FilterChange();
        controller.TypeChange();
        controller.AcronymChange();
        controller.YearChange();
        controller.ReportData();
        
        
        }
    }
   }