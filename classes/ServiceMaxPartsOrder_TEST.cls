/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class ServiceMaxPartsOrder_TEST {

    static testMethod void test() {
        RecordType detailRT = [SELECT Id FROM RecordType WHERE SObjectType = 'SVMXC__Service_Order_Line__c' AND Name = 'Parts' LIMIT 1];
        RecordType poRT = [SELECT Id FROM RecordType WHERE SObjectType = 'SVMXC__RMA_Shipment_Order__c' AND Name = 'Shipment' LIMIT 1];
        RecordType poLineRT = [SELECT Id FROM RecordType WHERE SObjectType = 'SVMXC__RMA_Shipment_Line__c' AND Name = 'Schneider Material' LIMIT 1];
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
        insert wo;
        
        SVMXC__RMA_Shipment_Order__c po = new SVMXC__RMA_Shipment_Order__c(SVMXC__Service_Order__c = wo.Id, RecordTypeId = poRT.Id);
        insert po;
        
        Product2 prod2 = new Product2(Name = 'test2');
        insert prod2;
        Test.startTest();
        SVMXC__RMA_Shipment_Line__c poLine = new SVMXC__RMA_Shipment_Line__c(
            SVMXC__RMA_Shipment_Order__c = po.Id,
            SVMXC__Expected_Quantity2__c = 5,
            RecordTypeId = poLineRT.Id,
            CommercialReference__c = 'test2',
            RequestedDate__c = Date.Today()
        );
        
        insert poLine;
        
        Test.stopTest();
       
        List<SVMXC__Service_Order_Line__c> detailList = [SELECT Id, SVMXC__Product__c, ShippedQuantity_del__c FROM SVMXC__Service_Order_Line__c WHERE SVMXC__Service_Order__c = :wo.Id AND RecordTypeId = :detailRT.Id];
        /*
        system.assertEquals(1, detailList.size());
        system.assertEquals(5, detailList.get(0).ShippedQuantity_del__c);
        
        poLine.SVMXC__Expected_Quantity2__c = 1;
        poLine.SVMXC__Product__c = prod2.Id;
        update poLine;
        
        detailList = [SELECT Id, SVMXC__Product__c, ShippedQuantity_del__c FROM SVMXC__Service_Order_Line__c WHERE SVMXC__Service_Order__c = :wo.Id AND RecordTypeId = :detailRT.Id];
        
        system.assertEquals(1, detailList.size());
        system.assertEquals(1, detailList.get(0).ShippedQuantity_del__c);
        */
        delete poLine;
        
        detailList = [SELECT Id, SVMXC__Product__c, ShippedQuantity_del__c FROM SVMXC__Service_Order_Line__c WHERE SVMXC__Service_Order__c = :wo.Id AND RecordTypeId = :detailRT.Id];
        
        system.assertEquals(0, detailList.size());
        
    }
}