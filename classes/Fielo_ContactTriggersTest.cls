/************************************************************
* Developer: Marco Paulucci                                 *
* Type: Test Class                                          *
* Created Date: 22.09.2014                                  *
* Tested Class: Fielo_ContactTriggers                       *
************************************************************/
@isTest
public with sharing class Fielo_ContactTriggersTest{

    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;

        Country__c c = new Country__c(Name = 'United Arab Emirates', CountryCode__c = 'AE');
        insert c;

        FieloEE__Member__c i = new FieloEE__Member__c();
        i.FieloEE__LastName__c= 'Polo';
        i.FieloEE__FirstName__c = 'Marco';
        i.F_CompanyName__c = 'test';
        i.F_CompanyPhoneNumber__c = 'test';
        i.F_Country__c = c.Id;
        i.F_Address1__c = 'asd ave.';
        i.F_NearLandmark__c = 'test';
        i.F_City__c = 'Dubai';
        i.F_POBox__c = '7891';
        i.FieloEE__Street__c = 'asd. ave';
        i.F_CorporateHeadquarters__c = true;
        i.F_BusinessType__c = 'Installer';
        i.F_AreaOfFocus__c = 'test';
        
        insert i;
    }
}