/* 
    Author: Yannick TISSERAND (Accenture)
    Created date: 02/04/2012
    Description: Apex class containing test method for AP48_OpportunityPartnerAssignment
*/   
@isTest
private class AP48_OpportunityPartnerAssignment_TEST
{

  static testMethod void testOpportunityAssignment()
  {
  
  Map<String,ID> profiles = new Map<String,ID>();
         List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
         for(Profile p : ps)
          {
             profiles.put(p.name, p.id);
          }
    

   user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null limit 1];
  
   system.runas(admin){
  
       Country__c country= Utils_TestMethods.createCountry();
    	insert country; 
       
       Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', Country__c=country.id, ownerId=admin.Id);
        insert PartnerAcc;
        
        AccountTeamMember atm = Utils_TestMethods.createAccTeamMember(PartnerAcc.id,admin.Id);
           Insert atm;
        
        Contact PartnerCon = new Contact(
                FirstName='Test',
                LastName='lastname',
                AccountId=PartnerAcc.Id,
                JobTitle__c='Z3',
                CorrespLang__c='EN',
                WorkPhone__c='1234567890'
                );
                
                Insert PartnerCon;
   
   AP48_OpportunityPartnerAssignment.createContactMap(); 
  }
  
    Account account = Utils_TestMethods.createAccount();
    insert account;
    Country__c country = new Country__c(Name='FRCountry', CountryCode__c='FR');
    insert country;
    Opportunity opportunity = Utils_TestMethods.createOpportunity(account.id);

    User user = Utils_TestMethods.createStandardUser('testUser');
    insert user;     
    
    insert opportunity;
    opportunity.ownerId = user .id;
    update opportunity;
  }
}