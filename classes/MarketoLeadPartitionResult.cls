public class MarketoLeadPartitionResult {
	public String requestId;
    public Boolean success;
    public List<MarketoLeadPartition> result;
}