Public Class VFC_DisplayAccountableOrgDetails {


Public String CaseId; 
Public List<Case> RelatedCase;
public List<ProductLineQualityContactMapping__c> listobjPL{get;set;}
public ProductLineQualityContactMapping__c objPL{get;set;}
 public List<RMAShippingToAdmin__c> lstRCs {get; set;}
 Public boolean noRCs {get; set;}
public boolean isError{get;set;}

public boolean isCancel{get;set;}

Public VFC_DisplayAccountableOrgDetails(ApexPages.StandardController stdController) {

   CaseId= System.CurrentPageReference().getparameters().get('id');
   noRCs=false;
   
   If(CaseId!='')
   {
           //CaseId= CaseId.substring(1); 
            RelatedCase = [SELECT ID,Account.Country__c, CommercialReference__c,Family__c,CaseNumber, AccountID, ContactID, ProductLine__c,TECH_GMRCode__c   from Case WHERE ID=:CaseId]; 
   }
   
   system.debug('*** RelatedCase[0].ProductLine__c'+ RelatedCase[0].ProductLine__c);
   if(RelatedCase[0].ProductLine__c!=null && (RelatedCase[0].CommercialReference__c!=null || RelatedCase[0].Family__c!=null ) )
   {
     // Updated the query where clause - Oct2014 Release - Sukumar Salla - 16JULY2014
     system.debug('*** RelatedCase[0].TECH_GMRCode__c.substring(0,4)' + RelatedCase[0].TECH_GMRCode__c.substring(0,4));
     listobjPL= [SELECT ProductLine__c,QualityContact__r.name,QualityContact__r.IsActive, Id, BusinessUnit__c, AccountableOrganization__r.Name, AccountableOrganization__c, TECH_PLGMRCode__c from ProductLineQualityContactMapping__c where TECH_PLGMRCode__c =: RelatedCase[0].TECH_GMRCode__c.substring(0,Integer.valueof(Label.CLOCT14I2P08))];
     system.debug('listobjPL'+listobjPL);
     If ( listobjPL.size()>0 && listobjPL!=null )
       {
        objPL=listobjPL[0];
        system.debug('**objPL** + objPL');
       }
     else
       {
       isError=True; 
       ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Quality Contact Information is not available on Accountable Organization'));
       }
   }
   else 
   {
       isCancel=True; 
       ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLAPR14I2P32));
   }
   
   lstRCs = getLocalRCs();
   if (lstRCs.isempty())
    { noRCs = True;
     // ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Return Centers are not available'));
     }
   
}

public pagereference validate()
{
  /*  if(RelatedCase[0].id!=null)
        {            
          If( RelatedCase[0].CommercialReference__c==null && RelatedCase[0].Family__c==null)
            {
                IsCancel=True;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Case need to have either Commercial Refernce or Family'));
            }
        } */
    return null;
    
}

public list<RMAShippingToAdmin__c> getLocalRCs()
 {
    String strFamily;

    String strCommercialReference = RelatedCase[0].CommercialReference__c;
    System.debug('RelatedCase[0]'+RelatedCase[0].Family__c);
    if (RelatedCase[0].Family__c!=null && RelatedCase[0].Family__c!='')
       {
            string newPF = RelatedCase[0].TECH_GMRCode__c ;
            if (newPF!=null) 
            {
                strFamily = newPF.substring(0,Integer.valueof(Label.CLOCT14I2P03));    //CLOCT14I2P03 = 8;  Oct2014 Release - Sukumar Salla - 16july2014 

            }
            else
            {
                strFamily = null;

               
            }
       }
    else
       {
          strFamily = null;

       }
     String strRMAAccCountry = RelatedCase[0].Account.Country__c;
     System.debug('strFamily'+strFamily);

     String strQuery = 'select id, RCFax__c, RCContactName__c,Country__r.Name,ContactEmail__c, RCStreetLocalLang__c,RCLocalAdditionalAddress__c,RCLocalCity__c,RCLocalCounty__c,ReturnCenter__c,RCPhone__c,RCAdditionalAddress__c, TECH_ProductFamilyGMRCode__c, RCPOBox__c, RCPOBoxZip__c,ProductFamily__r.TECH_PM0CodeInGMR__c, ProductFamily__r.name, RCCounty__c, RCStateProvince__c,ReturnCenter__r.RCStateProvince__c,Country__c,RCStreet__c,RCCity__c,RCZipCode__c, RCCountry__c,ReturnCenter__r.RCCountry__c,ReturnCenter__r.RCCountry__r.Name,ReturnCenter__r.Name,Capability__c,CommercialReference__c, ProductFamily__c,RCShippingAddress__c,Ruleactive__c,Comments__c,TECH_ProductFamily__c, Family__c from RMAShippingToAdmin__c where Ruleactive__c=True';
            if ( (strCommercialReference !=null) && (strFamily !=null ))
               {
               System.debug('strCommercialReference '+strCommercialReference );
               strQuery = strQuery + ' AND ((((CommercialReference__c= \''+strCommercialReference+'\') OR ( ((CommercialReference__c=\'\') AND TECH_ProductFamilyGMRCode__c= \''+ strFamily +'\')))';             
               }
               else if ((strCommercialReference ==null ) && (strFamily ==null ))
               {
                  strQuery = strQuery + ' AND ((((CommercialReference__c=\'\') AND (TECH_ProductFamilyGMRCode__c=\'\'))';
               }
             
               else if ((strCommercialReference ==null ) && (strFamily !=null ))
               {
                    System.debug('strFamily '+ strFamily );

                     strQuery = strQuery + ' AND (((CommercialReference__c=\'\') AND ( TECH_ProductFamilyGMRCode__c= \''+ strFamily +'\')';
                    
               }
                strQuery = strQuery +' AND  (Country__c=\'\' OR   Country__c=\''+strRMAAccCountry+'\'))';
                strQuery =  strQuery + 'OR ( (Country__c=\''+strRMAAccCountry+'\') AND (CommercialReference__c=\'\') AND (TECH_ProductFamilyGMRCode__c=\'\')))';
               system.debug('Local Rule'+ strQuery );
               return database.query(strQuery);
 }
}