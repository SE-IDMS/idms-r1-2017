//Batch Job for Processing the Records
global class IDMSCreateUserReconciliationBatch implements Database.Batchable<SObject>,Database.AllowsCallouts{
    
    //Start Method
    global Database.Querylocator start (Database.BatchableContext BC) {
        Integer attemptsMaxLmt = integer.valueof(label.CLNOV16IDMS022);
        Integer lmtRecords = integer.valueof(label.CLNOV16IDMS023);
        //Query which will be determine the scope of Records fetching
        return Database.getQueryLocator([Select Id, AtCreate__c,AtUpdate__c, HttpMessage__c, IdmsUser__c, NbrAttempts__c from IDMSUserReconciliationBatchHandler__c where NbrAttempts__c <= :attemptsMaxLmt limit :lmtRecords]);
    }
    
    //Execute method
    global void execute (Database.BatchableContext BC, List<SObject> scope) {
        
        String BatchId = BC.getJobId();    
        List<IDMSUserReconciliationBatchHandler__c> recUsersToCreate = new List<IDMSUserReconciliationBatchHandler__c>();
        for (SObject userConcScope: scope){ 
            IDMSUserReconciliationBatchHandler__c usr = (IDMSUserReconciliationBatchHandler__c)userConcScope;
            if(usr.AtCreate__c == true){
                recUsersToCreate.add(usr);
            }
        }
        if(recUsersToCreate.size() > 0){
            //API call to create users in UIMS
            IdmsUimsReconcCreateUserBulk.createUimsUsers(recUsersToCreate, BatchId);
            System.debug('Idms users to Create *****: '+recUsersToCreate);
        }
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
        //Below code will fetch the job Id.
        AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors, a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById, a.CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];//get the job Id
        System.debug('$$$ Jobid is'+BC.getJobId());
        
        // schedule update user batch code starts
        system.debug('*****Inside First batch run******');
        IDMSUpdateUserReconciliationBatch updateUserBatch = new IDMSUpdateUserReconciliationBatch();
        ID batchprocessid = Database.executeBatch(updateUserBatch ,100);
    }
}