public with sharing class AP_AccountPlanConfidential_RecordCount
{
    public static void insertAPCIdInAP(List<AccountPlanConfidential__c> lstNewAccPlanConf)
    {
        Map<Id,List<AccountPlanConfidential__c>> parentToChildListMap=new Map<Id,List<AccountPlanConfidential__c>>();

        for(AccountPlanConfidential__c accplanConfidential : lstNewAccPlanConf){
            if(parentToChildListMap.containsKey(accplanConfidential.AccountPlan__c))
                parentToChildListMap.get(accplanConfidential.AccountPlan__c).add(accplanConfidential);
            else
            {
                List<AccountPlanConfidential__c> tempList=new List<AccountPlanConfidential__c>();
                tempList.add(accplanConfidential);
                parentToChildListMap.put(accplanConfidential.AccountPlan__c,tempList);
            }
            
        }

        List<AggregateResult> accplanConfidentialCount=[select count(id),AccountPlan__c from AccountPlanConfidential__c where AccountPlan__c in :parentToChildListMap.keySet() group by AccountPlan__c having count(id)>=1];

        for(AggregateResult aggresult : accplanConfidentialCount){
            System.debug('aggresult'+aggresult);
            System.debug('parentToChildListMap'+parentToChildListMap.get(aggresult.get('AccountPlan__c')+''));
            for(AccountPlanConfidential__c accPlanConfidential:parentToChildListMap.get(aggresult.get('AccountPlan__c')+''))
            {                               
                System.debug('accplanConfidential'+accplanConfidential);
                if(!Test.isRunningTest())
                    accPlanConfidential.addError(Label.CLDEC12SLS04);
            }                           
        }        
            
                
                
     }
     
       public static void checkAccessToAccount(List<AccountPlanConfidential__c> lstNewAccPlanConf)
     {
         Map<Id,List<AccountPlanConfidential__c>> accountPlanToAPCMap=new Map<Id,List<AccountPlanConfidential__c>>();
         for(AccountPlanConfidential__c apcRecord:lstNewAccPlanConf)
         {
             if(accountPlanToAPCMap.containsKey(apcRecord.AccountPlan__c))
             accountPlanToAPCMap.get(apcRecord.AccountPlan__c).add(apcRecord);
             else
             {
                 List<AccountPlanConfidential__c> tmpList=new List<AccountPlanConfidential__c>();
                 tmpList.add(apcRecord);
                 accountPlanToAPCMap.put(apcRecord.AccountPlan__c,tmpList);
             }
         }         
         Id userId=UserInfo.getUserId();
         List<Id> recordIds=new List<Id>();
         recordIds.addAll(accountPlanToAPCMap.keySet());
         
         for(UserRecordAccess userrecordaccesslpv: [SELECT RecordId, HasEditAccess     FROM UserRecordAccess     WHERE UserId =:UserInfo.getUserId()     AND RecordId in :recordIds])         
             if(!userrecordaccesslpv.HasEditAccess)     
                for(AccountPlanConfidential__c accountPlanConfRecord:accountPlanToAPCMap.get(userrecordaccesslpv.RecordId))
                    accountPlanConfRecord.addError('Insufficient previleges on the Account Plan');         
     }
            
}