global class IDMSUserReconHandlerClass{
    
    @future
    public static void userIdenEmailToRecon(string resErrorMsg, string idmsUserId){
        
        try{
            IDMSUserReconciliationBatchHandler__c userToReconciliate = new IDMSUserReconciliationBatchHandler__c();
            userToReconciliate.AtCreate__c = true;
            userToReconciliate.AtUpdate__c = false;
            userToReconciliate.HttpMessage__c = resErrorMsg;
            userToReconciliate.IdmsUser__c = idmsUserId;
            userToReconciliate.NbrAttempts__c = 1;
            insert userToReconciliate;
            
        }catch (DmlException e){
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
    
    public static void userIdentityPhoneToRecon(string resErrorMsg, string idmsUserId){
        
        try{
            IDMSUserReconciliationBatchHandler__c userToReconciliate = new IDMSUserReconciliationBatchHandler__c();
            userToReconciliate.AtCreate__c = true;
            userToReconciliate.AtUpdate__c = false;
            userToReconciliate.HttpMessage__c = resErrorMsg;
            userToReconciliate.IdmsUser__c = idmsUserId;
            userToReconciliate.NbrAttempts__c = 1;
            insert userToReconciliate;
        }catch (DmlException e){
            System.debug('The following exception has occurred in phone user: ' + e.getMessage());
        }
    }
    
    
}