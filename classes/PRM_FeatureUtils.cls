public  class PRM_FeatureUtils{
	
	public static PermissionSetAssignment getPermissionSetAssignementByName(String Name,Id UserId){
		PermissionSet pset1 = [select name,id from PermissionSet where name =: Name];
        PermissionSetAssignment psetAssign = new PermissionSetAssignment();
        psetAssign.PermissionSetId = pset1.id;
        psetAssign.AssigneeId = UserId;
        return psetAssign;
	}
}