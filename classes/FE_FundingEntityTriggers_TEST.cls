/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 03/19/2012
    Description     : Test class for the classes FE_FundingEntityTriggers and FE_FundingEntityUtils.
*/


@isTest
private class FE_FundingEntityTriggers_TEST
{
   static testMethod void BudgetTriggersUnitTest() 
    {
        
        User runAsUser = Utils_TestMethods.createStandardDMTUser('tst3');
        
        insert runAsUser;
        //System.runAs(runAsUser)
        //{
            List<DMT_FundingEntiity_BudgetLines__c> dmtfebl=new List<DMT_FundingEntiity_BudgetLines__c>();

            DMT_FundingEntiity_BudgetLines__c  one=new DMT_FundingEntiity_BudgetLines__c();
            one.Budget_Line__c = 'IT Cashout Small Enhancements OTC';
            one.Funding_Entity__c = 'SmallEnhancementsY1__c';
            one.Name =  'SmallEnhancementsY1__c';
            DMT_FundingEntiity_BudgetLines__c  two=new DMT_FundingEntiity_BudgetLines__c();
            two.Budget_Line__c = 'IT Cashout Small Enhancements OTC';
            two.Funding_Entity__c = 'SmallEnhancementsY2__c';
            two.Name =  'SmallEnhancementsY2__c';
            DMT_FundingEntiity_BudgetLines__c  thr=new DMT_FundingEntiity_BudgetLines__c();
            thr.Budget_Line__c = 'IT Cashout Small Enhancements OTC';
            thr.Funding_Entity__c = 'SmallEnhancementsY3__c';
            thr.Name =  'SmallEnhancementsY3__c';
            DMT_FundingEntiity_BudgetLines__c  fr=new DMT_FundingEntiity_BudgetLines__c();
            fr.Budget_Line__c = 'IT Cashout Small Enhancements OTC';
            fr.Funding_Entity__c = 'SmallEnhancementsY0__c';
            fr.Name =  'SmallEnhancementsY0__c';
            
            DMT_FundingEntiity_BudgetLines__c  fiv=new DMT_FundingEntiity_BudgetLines__c();
            fiv.Budget_Line__c = 'IT Cashout Running Costs Impact';
            fiv.Funding_Entity__c = 'RunningCostsImpactY0__c';
            fiv.Name =  'RunningCostsImpactY0__c';
            DMT_FundingEntiity_BudgetLines__c  fiv1=new DMT_FundingEntiity_BudgetLines__c();
            fiv1.Budget_Line__c = 'IT Cashout Running Costs Impact';
            fiv1.Funding_Entity__c = 'RunningCostsImpactY1__c';
            fiv1.Name =  'RunningCostsImpactY1__c';
            DMT_FundingEntiity_BudgetLines__c  fiv2=new DMT_FundingEntiity_BudgetLines__c();
            fiv2.Budget_Line__c = 'IT Cashout Running Costs Impact';
            fiv2.Funding_Entity__c = 'RunningCostsImpactY2__c';
            fiv2.Name =  'RunningCostsImpactY2__c';
            DMT_FundingEntiity_BudgetLines__c  fiv3=new DMT_FundingEntiity_BudgetLines__c();
            fiv3.Budget_Line__c = 'IT Cashout Running Costs Impact';
            fiv3.Funding_Entity__c = 'RunningCostsImpactY3__c';
            fiv3.Name =  'RunningCostsImpactY3__c';
            
            DMT_FundingEntiity_BudgetLines__c  six=new DMT_FundingEntiity_BudgetLines__c();
            six.Budget_Line__c = 'IT Cashout CAPEX';
            six.Funding_Entity__c = 'CAPEXY0__c';
            six.Name =  'CAPEXY0__c';
            DMT_FundingEntiity_BudgetLines__c  six1=new DMT_FundingEntiity_BudgetLines__c();
            six1.Budget_Line__c = 'IT Cashout CAPEX';
            six1.Funding_Entity__c = 'CAPEXY1__c';
            six1.Name =  'CAPEXY1__c';
            DMT_FundingEntiity_BudgetLines__c  six2=new DMT_FundingEntiity_BudgetLines__c();
            six2.Budget_Line__c = 'IT Cashout CAPEX';
            six2.Funding_Entity__c = 'CAPEXY2__c';
            six2.Name =  'CAPEXY2__c';
            DMT_FundingEntiity_BudgetLines__c  six3=new DMT_FundingEntiity_BudgetLines__c();
            six3.Budget_Line__c = 'IT Cashout CAPEX';
            six3.Funding_Entity__c = 'CAPEXY3__c';
            six3.Name =  'CAPEXY3__c';
            
            DMT_FundingEntiity_BudgetLines__c  sev=new DMT_FundingEntiity_BudgetLines__c();
            sev.Budget_Line__c = 'IT Cashout OPEX OTC,IPO Internal GD Costs,IPO Internal Regions,IPO Internal App Services / Cust Exp,IPO Internal Technology Services';
            sev.Funding_Entity__c = 'OPEXOTCY0__c';
            sev.Name =  'OPEXOTCY0__c';
            DMT_FundingEntiity_BudgetLines__c  sev1=new DMT_FundingEntiity_BudgetLines__c();
            sev1.Budget_Line__c = 'IT Cashout OPEX OTC,IPO Internal GD Costs,IPO Internal Regions,IPO Internal App Services / Cust Exp,IPO Internal Technology Services';
            sev1.Funding_Entity__c = 'OPEXOTCY1__c';
            sev1.Name =  'OPEXOTCY1__c';
            DMT_FundingEntiity_BudgetLines__c  sev2=new DMT_FundingEntiity_BudgetLines__c();
            sev2.Budget_Line__c = 'IT Cashout OPEX OTC,IPO Internal GD Costs,IPO Internal Regions,IPO Internal App Services / Cust Exp,IPO Internal Technology Services';
            sev2.Funding_Entity__c = 'OPEXOTCY2__c';
            sev2.Name =  'OPEXOTCY2__c';
            DMT_FundingEntiity_BudgetLines__c  sev3=new DMT_FundingEntiity_BudgetLines__c();
            sev3.Budget_Line__c = 'IT Cashout OPEX OTC,IPO Internal GD Costs,IPO Internal Regions,IPO Internal App Services / Cust Exp,IPO Internal Technology Services';
            sev3.Funding_Entity__c = 'OPEXOTCY3__c';
            sev3.Name =  'OPEXOTCY3__c';
            
            dmtfebl.add(one);
            dmtfebl.add(two);
            dmtfebl.add(thr);
            dmtfebl.add(fr);
            dmtfebl.add(fiv);
            dmtfebl.add(fiv1);
            dmtfebl.add(fiv2);
            dmtfebl.add(fiv3);
            dmtfebl.add(six);
            dmtfebl.add(six1);
            dmtfebl.add(six2);
            dmtfebl.add(six3);
            dmtfebl.add(sev);
            dmtfebl.add(sev1);
            dmtfebl.add(sev2);
            dmtfebl.add(sev3);
            insert dmtfebl; 
            
            
            DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','APAC');
            insert testDMTA1; 
            
            list<PRJ_ProjectReq__c> lsttestProjs = new list<PRJ_ProjectReq__c>();
            List<DMTFundingEntity__c> lstTestProj =  new List<DMTFundingEntity__c>();
            PRJ_ProjectReq__c testProj1 = new PRJ_ProjectReq__c();
            testProj1.Name ='test rec';
            testProj1.ParentFamily__c = 'Business';
            testProj1.Parent__c = 'Buildings';  
            testProj1.GeographicZoneSE__c = 'APAC';
            lsttestProjs.add(testProj1);
            PRJ_ProjectReq__c testProj = Utils_TestMethods.createPRJRequest();
            //lstTestProj.add(testProj);
            lsttestProjs.add(testProj);
            insert lsttestProjs;
             Integer count=[select count() from DMTFundingEntity__c where ProjectReq__c = :testProj.id];
            if(count>0){
                list<DMTFundingEntity__c> fund=[select id from DMTFundingEntity__c where ProjectReq__c = :testProj.id ];
             delete fund;
         } 
            Budget__c testBudget = new Budget__c();
            testBudget.Active__c = true;
            testBudget.ProjectReq__c = testProj.id;
            insert testBudget;
            PRJ_BudgetLine__c testBudLine1 = new PRJ_BudgetLine__c();
            testBudLine1.Budget__c = testBudget.id;
            testBudLine1.Amount__c = 1;
            insert testBudLine1;
           test.startTest();
            DMTFundingEntity__c testFE1 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj,'2012',false,'Business Units','Power','EMEAS','EMEAs','Q4','2011');
            testFE1.ActiveForecast__c = true;
            lstTestProj.add(testFE1);
            DMTFundingEntity__c testFE2 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj,'2012',false,'Business Units','Power','EMEAS','EMEAs','Q3','2011');
            lstTestProj.add(testFE2);
            DMTFundingEntity__c testFE3 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj,'2012',true,'Business Units','Power','EMEAS','EMEAs','Q2','2011');
            lstTestProj.add(testFE3);
            DMTFundingEntity__c testFE4 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj,'2012',true,'Business Units','Power','EMEAS','EMEAs','Q1','2011');
            lstTestProj.add(testFE4);
           
            insert lstTestProj;
            testProj.nextstep__c = 'Valid';
            testProj.GoLiveTargetDate__c = System.today()+1;
            testProj.ExpectedStartDate__c = System.today();
            testProj.BusinessTechnicalDomain__c = 'Industry';
            testProj.AppServicesDecision__c = 'Test';
            update testproj;
            testFE3.RunningCostsImpactY0__c=3;
            testFE3.SelectedinEnvelop__c=true;
            update testFE3;
            
            //check if testFE4 is active and testFE3 is inactive
            DMTFundingEntity__c testRetrievedFE = [Select ActiveForecast__c from DMTFundingEntity__c where Id = :testFE3.Id limit 1];
            //System.AssertEquals(false,testRetrievedFE.ActiveForecast__c);
            testRetrievedFE = [Select ActiveForecast__c from DMTFundingEntity__c where Id = :testFE4.Id limit 1];
            //System.AssertEquals(true,testRetrievedFE.ActiveForecast__c);  
            testFE2.ActiveForecast__c = true;
            update testFE2;
            //check if testFE2 is active      
            testRetrievedFE = [Select ActiveForecast__c from DMTFundingEntity__c where Id = :testFE2.Id limit 1];
            //System.AssertEquals(true,testRetrievedFE.ActiveForecast__c);    
            //check if funding entity record already exists
            DMTFundingEntity__c testFE5 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj,'2012',true,'Business Units','Power','EMEAS','EMEAs','Q1','2011');
            DMTFundingEntity__c testFE6 = Utils_TestMethods_DMT.createDMTFundingEntity(testProj,'2012');
            testFE6.ActiveForecast__c = true;
            insert testFE6;
            try
            {   
                delete testFE4;         
                insert testFE5;
            }
            catch(Exception exp)
            {
                System.Debug('The funding entity record already exists');
            }
                       
            /*
            PRJ_ProjectReq__c testProj1 = new PRJ_ProjectReq__c();
            testProj1.Name ='test rec';
            testProj1.ParentFamily__c = 'Business';
            testProj1.Parent__c = 'Buildings';  
            testProj1.GeographicZoneSE__c = 'APAC';
            insert testProj1;
            */
            Budget__c testBudget1 = new Budget__c();
            testBudget1.Active__c = true;
            testBudget1.ProjectReq__c = testProj1.id;
            insert testBudget1;
            List<PRJ_BudgetLine__c> budgetLineList= new List<PRJ_BudgetLine__c>();
            Set<String> types= DMTPastBudgetFields__c.getAll().keySet();
            
            for(String s:types){
                Integer py=System.today().year();
                for(Integer i=py;i<=py+2;i++){
                    PRJ_BudgetLine__c testBudLine = new PRJ_BudgetLine__c();
                    testBudLine.Type__c = s;
                    testBudLine.Year__c = i+'';
                    testBudLine.Budget__c = testBudget1.id;
                    testBudLine.Amount__c = 1;
                    budgetLineList.add(testBudLine );
                    
                }
            //}
            insert budgetLineList;
            
            /*
            PRJ_BudgetLine__c testBudLine = new PRJ_BudgetLine__c();
            testBudLine.Budget__c = testBudget1.id;
            insert testBudLine;
            */
            /*
            DMTFundingEntity__c testFe12 = new DMTFundingEntity__c();
            testFe12.ProjectReq__c = testProj1.id;
            
            testFe12.ActiveForecast__c = true;
            testFe12.ParentFamily__c = 'Business';
            testFe12.Parent__c = 'Buildings';  
            testFe12.GeographicZoneSE__c = 'APAC';
            testFe12.BudgetYear__c = '2012';
            testFe12.ForecastQuarter__c = 'Q1';
            testFe12.ForecastYear__c = '2011';
            insert testFe12; 
            
            system.debug('**************'+testFe12.id);
            */
              
            
        }
        test.stopTest();
        
    }
    static testMethod void BudgetTriggersUnitTest2() 
    {
        test.startTest();
            DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','APAC');
            insert testDMTA1; 
            PRJ_ProjectReq__c testProj1 = new PRJ_ProjectReq__c();
            testProj1.Name ='test rec';
            testProj1.ParentFamily__c = 'Business';
            testProj1.Parent__c = 'Buildings';  
            testProj1.GeographicZoneSE__c = 'APAC';
            testProj1.CurrencyISOCode ='INR';
            insert testProj1;
            
            Budget__c testBudget1 = new Budget__c();
            testBudget1.Active__c = true;
            testBudget1.ProjectReq__c = testProj1.id;
            testBudget1.CurrencyISOCode ='INR';
            insert testBudget1;
            
            
            List<DMTPastBudgetFields__c> bgFieldsList= new List<DMTPastBudgetFields__c>();
            
            DMTPastBudgetFields__c bgf1=new DMTPastBudgetFields__c();
            bgf1.name='IT Cashout CAPEX';
            bgf1.BUDGETFIELDAPINAME__C='PastBudgetCashoutCAPEXCumulative__c';
            bgFieldsList.add(bgf1);
            DMTPastBudgetFields__c bgf2=new DMTPastBudgetFields__c();
            bgf2.name='IT Cashout OPEX OTC';
            bgf2.BUDGETFIELDAPINAME__C='PastBudgetCashoutOPEXCumulative__c';
            bgFieldsList.add(bgf2);
            DMTPastBudgetFields__c bgf3=new DMTPastBudgetFields__c();
            bgf3.name='IT Cashout Small Enhancements OTC';
            bgf3.BUDGETFIELDAPINAME__C='PastBudgetSmallEnhancementsCumulativ__c';
            bgFieldsList.add(bgf3);
            DMTPastBudgetFields__c bgf4=new DMTPastBudgetFields__c();
            bgf4.name='IPO Internal GD Costs';
            bgf4.BUDGETFIELDAPINAME__C='PastBudgetGDCostsCumulative__c';
            bgFieldsList.add(bgf4);
            DMTPastBudgetFields__c bgf5=new DMTPastBudgetFields__c();
            bgf5.name='Business Cashout CAPEX';
            bgf5.BUDGETFIELDAPINAME__C='PastBudgetBusCashoutCAPEXCumulative__c';
            bgFieldsList.add(bgf5);
            DMTPastBudgetFields__c bgf6=new DMTPastBudgetFields__c();
            bgf6.name='Business Internal';
            bgf6.BUDGETFIELDAPINAME__C='PastBudgetBusinessInternalCumulative__c';
            bgFieldsList.add(bgf6);
            DMTPastBudgetFields__c bgf7=new DMTPastBudgetFields__c();
            bgf7.name='IT Costs Internal Non IPO';
            bgf7.BUDGETFIELDAPINAME__C='PastBudNonIPOITCostsIntCumulative__c';
            bgFieldsList.add(bgf7);
            DMTPastBudgetFields__c bgf8=new DMTPastBudgetFields__c();
            bgf8.name='IT Cashout Running Costs Impact';
            bgf8.BUDGETFIELDAPINAME__C='PastBudRunningITCostsImpCumulative__c';
            bgFieldsList.add(bgf8);
            DMTPastBudgetFields__c bgf9=new DMTPastBudgetFields__c();
            bgf9.name='Business Cashout OPEX OTC';
            bgf9.BUDGETFIELDAPINAME__C='PastBudBusCashoutOPEXOTCCumulative__c';
            bgFieldsList.add(bgf9);
            DMTPastBudgetFields__c bgf10=new DMTPastBudgetFields__c();
            bgf10.name='Business Internal charged back to IPO';
            bgf10.BUDGETFIELDAPINAME__C='PastBudIntChargedBacktoIPOCumulative__c';
            bgFieldsList.add(bgf10);
            DMTPastBudgetFields__c bgf11=new DMTPastBudgetFields__c();
            bgf11.name='Business Running Costs Impact';
            bgf11.BUDGETFIELDAPINAME__C='PastBudBusinessRunningCostCumulative__c';
            bgFieldsList.add(bgf11);
            DMTPastBudgetFields__c bgf12=new DMTPastBudgetFields__c();
            bgf12.name='P & L Impact';
            bgf12.BUDGETFIELDAPINAME__C='PastBudgetPLImpactCumulative__c';
            bgFieldsList.add(bgf12);
            DMTPastBudgetFields__c bgf13=new DMTPastBudgetFields__c();
            bgf13.name='IPO Internal Regions';
            bgf13.BUDGETFIELDAPINAME__C='PastBudgetRegionsCumulative__c';
            bgFieldsList.add(bgf13);
            DMTPastBudgetFields__c bgf14=new DMTPastBudgetFields__c();
            bgf14.name='IPO Internal Technology Services';
            bgf14.BUDGETFIELDAPINAME__C='PastBudOperServicesCumulative__c';
            bgFieldsList.add(bgf14);
            DMTPastBudgetFields__c bgf15=new DMTPastBudgetFields__c();
            bgf15.name='IPO Internal App Services / Cust Exp';
            bgf15.BUDGETFIELDAPINAME__C='PastBudgetASCECumulative__c';
            bgFieldsList.add(bgf15);
            insert bgFieldsList;
            Set<String> types= DMTPastBudgetFields__c.getAll().keySet();
            
            List<PRJ_BudgetLine__c> budgetLineList= new List<PRJ_BudgetLine__c>();
            
            Decimal count =1;
            for(String s:types){
                Integer py=System.today().year();
                for(Integer i=py;i<=py+2;i++){
                    PRJ_BudgetLine__c testBudLine = new PRJ_BudgetLine__c();
                    testBudLine.Type__c = s;
                    testBudLine.Year__c = i+'';
                    testBudLine.Budget__c = testBudget1.id;
                    testBudLine.Amount__c = count ;
                    testBudLine.CurrencyISOCode ='INR';
                    budgetLineList.add(testBudLine );
                    count ++;
                    
                }
            }
            insert budgetLineList;
           
            
            DMTFundingEntity__c testFe12 = new DMTFundingEntity__c();
            testFe12.ProjectReq__c = testProj1.id;
            testFe12.ActiveForecast__c = true;
            testFe12.ParentFamily__c = 'Business';
            testFe12.Parent__c = 'Buildings';  
            testFe12.GeographicZoneSE__c = 'APAC';
            testFe12.BudgetYear__c = '2012';
            testFe12.ForecastQuarter__c = 'Q1';
            testFe12.ForecastYear__c = '2011';
            testFe12.CurrencyISOCode ='INR';
            insert testFe12; 
        test.stopTest();
    }
}