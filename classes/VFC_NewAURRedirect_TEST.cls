@isTest
private class VFC_NewAURRedirect_TEST 
{
    static testmethod void testNewAURPrepopulation()
    {
        Country__c cntry1 = new Country__c();
        cntry1.Name='TestCountry1';
        cntry1.CountryCode__c='IT';
        insert cntry1;
        
        Account testaccount = Utils_TestMethods.createAccount();
        testaccount.Country__c=cntry1.id;
        insert testaccount;
        
        AccountUpdateRequest__c aur = new AccountUpdateRequest__c();
        AccountUpdateRequest__c aur1 = new AccountUpdateRequest__c(Account__c = testaccount.id,
                                                                 //RecordTypeId=System.Label.CLMAR16ACC01,
                                                                 AccountName__c = testaccount.name,
                                                                 City__c='Test City',
                                                                 Street__c='Test Street',
                                                                 Country__c=cntry1.id,
                                                                 ZipCode__c='12345');
        insert aur1;
                
        System.debug('--- Start Testing ---');
        PageReference pageRefpositive = Page.VFP_NewAURRedirect;
        Test.setCurrentPage(pageRefpositive);
        pageRefpositive.getParameters().put(Label.CLMAR16ACC18, testaccount.id);
        //pageRefpositive.getParameters().put('RecordType', Label.CLMAR16ACC01);
        pageRefpositive.getParameters().put('retURL', testaccount.id);
        
        ApexPages.StandardController controller=new ApexPages.StandardController(aur);
        VFC_NewAURRedirect valAURPrepop = new VFC_NewAURRedirect(controller);        
        
    }
}