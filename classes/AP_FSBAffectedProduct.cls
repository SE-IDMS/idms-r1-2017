// Added by Uttara - Q2 2016 Release - NotestobFO

public class AP_FSBAffectedProduct {
    
    public static void SameProductLineCheck(Set<String> setFSB, List<FSBAffectedProduct__c> triggerNew) {
    
        Set<String> stProdLine =  new Set<String>();
        
        for(FSBAffectedProduct__c fsbap : [SELECT ProductLine__c, FieldServiceBulletin__c FROM FSBAffectedProduct__c WHERE FieldServiceBulletin__c IN: setFSB]) {
        
            stProdLine.add(fsbap.ProductLine__c);
        }
        System.debug('!!!! stProdLine : ' + stProdLine);
        
        if(!stProdLine.isEmpty()) {
            for(FSBAffectedProduct__c fsbap : triggerNew) {
                for(Product2 prod : [SELECT ProductLine__c FROM Product2 WHERE Id =: fsbap.Product__c]) {
                    if(!stProdLine.contains(prod.ProductLine__c)) { //mspFSBProdLine.get(fsbap.FieldServiceBulletin__c) != prod.ProductLine__c) {
                        System.debug('!!!! Error thrown.');
                        fsbap.addError('FSB Affected Product with a different Product Line cannot be added.');
                    }
                }
            }
        }
    }
}