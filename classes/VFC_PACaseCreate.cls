/**
    Author          : Nitin Khunal
    Date Created    : 20/07/2016
    Description     : Controller for VFP_PACaseCreate  VF Page
*/
public without sharing class VFC_PACaseCreate {
    
    Public string contactName{get;set;}
    Public String accountName{get;set;}
    Public String sites{get;set;}
    public string sitesname{get;set;}
    Public String accId {get;set;} 
    Public String conId {get;set;}
    Public List<String> lstPriorityOptions {get;set;}
    Public String strPriorityOptions {get;set;}
    Public List<String> lstCaseTypeOptions{get;set;}
    Public String strCaseTypeOptions{get;set;}
    Public List<String> lstPlatformOptions{get;set;}
    Public String strPlatformOptions{get;set;}
    
    public VFC_PACaseCreate() {
       Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
        getCasePriority();
         getCaseType();
         getPlatform();
        
    }
    @RemoteAction
    public static List<OPP_Product__c> getOthers(String business) {
        system.debug('business'+business);
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        if(business.length() == 8){
          business='%'+business;
          query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__%\' ORDER BY Name ASC';
        }
        return Database.query(query);       
    }
    @RemoteAction
    public static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        system.debug(gmrcode.split(','));
        system.debug(gmrcode);
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
            
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
        filters.keyWordList=searchString.split(' '); //prepare the search string
        WS_GMRSearch.resultFilteredDataBean objResult = new WS_GMRSearch.resultFilteredDataBean();
        objResult.gmrFilteredDataBeanList = new list<WS_GMRSearch.gmrFilteredDataBean>();
        for(String str:gmrcode.split(',')){
            if(str != null){
                System.debug('str'+str);
                filters.businessLine = str.substring(0,2);            
                filters.productLine =  str.substring(2,4);
                /*if(gmrcode.length()==2){
                    filters.businessLine = gmrcode;            
                }
                else if(gmrcode.length()==4){
                                
                }
                else if(gmrcode.length()==6){
                    filters.businessLine = gmrcode.substring(0,2);            
                    filters.productLine = gmrcode.substring(2,4);
                    filters.strategicProductFamily = gmrcode.substring(4,6);
                }
                else if(gmrcode.length()==8){
                    filters.businessLine = gmrcode.substring(0,2);            
                    filters.productLine = gmrcode.substring(2,4);
                    filters.strategicProductFamily = gmrcode.substring(4,6); 
                    filters.Family = gmrcode.substring(6,8);
                }
                else if(gmrcode.length()>8){
                    filters.businessLine = gmrcode.substring(0,2);            
                    filters.productLine = gmrcode.substring(2,4);
                    filters.strategicProductFamily = gmrcode.substring(4,6); 
                    filters.Family = gmrcode.substring(6,8);
                    filters.subFamily = gmrcode.substring(8,11);
                }*/
            }
            if(!Test.isRunningTest()){    
                WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
                system.debug('results.gmrFilteredDataBeanList::'+results.gmrFilteredDataBeanList);
                if (results.gmrFilteredDataBeanList.size()>0) {
                    objResult.gmrFilteredDataBeanList.addAll(results.gmrFilteredDataBeanList);
                }
                
                //return results.gmrFilteredDataBeanList;    
            }
            else{
                return null;
            }
        } 
        return objResult.gmrFilteredDataBeanList;  
    }
    @remoteAction
    Public static list<SiteRadioWrapper> SearchSites(string strSite, String straccountId){
        list<GCSSite__c> sitelist = new list<GCSSite__c>();
        string str = '%'+strSite+'%';
        if(!Test.IsRunningTest()){
            //User UserObj =[select id,accountId from User where Id=:strUserId] ;
            sitelist = [select id,name,active__c,LegacySiteId__c from GCSSite__c where Name LIKE:str and Account__c=:straccountId AND active__c =: TRUE];
        }
        else{
            sitelist = [select id,name,active__c,LegacySiteId__c from GCSSite__c where Name LIKE:str];
        
        }
        system.debug('sitelist'+sitelist);
        list<SiteRadioWrapper>SiteWrapper = new list<SiteRadioWrapper>();
        for(GCSSite__c siteitr :sitelist){
            SiteWrapper.add(new SiteRadioWrapper(siteitr));
        }
        system.debug('SiteWrapper'+SiteWrapper);
        return SiteWrapper;
    }
    @remoteAction
    Public static String caseCreate(string strAccountId,
                                    string strContactId,
                                    string strSite, 
                                    String strUserId,
                                    String CommercialRef, 
                                    String Status, 
                                    String Priority, 
                                    String case_subject, 
                                    String case_Description,
                                    Boolean case_Subscribe,
                                    String casetype,
                                    String platformtype,
                                    String customerInternalCaseNumber,
                                    String dateOccured
                                    ){
        User userObj= [Select id,ContactId,AccountId, Contact.AccountId, Contact.Email, Email from User where id=:strUserId limit 1];
        list<Product2>lstProd2 =new list<Product2>([select id,name from Product2 where name=:CommercialRef]);
        caseCreationParameter objcaseCreationParameter = new caseCreationParameter();
        BusinessRiskEscalationEntity__c objBusiness = new BusinessRiskEscalationEntity__c ();
        List<CustomerCareTeam__c> lstCCCTeam = new List<CustomerCareTeam__c>();
        if(!test.isRunningTest()) {
            objBusiness =[SELECT Id,Name FROM BusinessRiskEscalationEntity__c WHERE Name =:system.label.CLQ316PA024 limit 1];
        }
        else{
            objBusiness =[SELECT Id,Name FROM BusinessRiskEscalationEntity__c where Location_Type__c='Plant' limit 1];
        }
        //PA_CaseTeamAssignment__c
        List<PA_CaseTeamAssignment__c> listCaseTeam= new List<PA_CaseTeamAssignment__c>() ;
        List<account> lstAccountCountry = new List<account>();
        lstAccountCountry = [select country__r.Name,country__c, country__r.CountryCode__c from Account where id =:strAccountId];
        list<CustomerCareTeam__c>lstCustCareTeam = new list<CustomerCareTeam__c>();
        if(lstAccountCountry.size()>0 && caseType =='Technical Support (TEC)' && PA_CaseTeamAssignment__c.getValues(lstAccountCountry[0].country__r.CountryCode__c) != null){
            listCaseTeam.add(PA_CaseTeamAssignment__c.getValues(lstAccountCountry[0].country__r.CountryCode__c));
        }
        if(caseType =='Product Enhancement Request (PER)'&& PA_CaseTeamAssignment__c.getValues(caseType) != null){
            listCaseTeam.add(PA_CaseTeamAssignment__c.getValues(caseType));

        }
        if(listCaseTeam.size()>0){
            lstCustCareTeam =[Select id,LevelofSupport__c from CustomerCareTeam__c where id =:listCaseTeam[0].Assigned_CC_Team__c AND inActive__c=FALSE];
        }
        if(listCaseTeam.isEmpty() ||lstCustCareTeam.isEmpty()){
            listCaseTeam.add(PA_CaseTeamAssignment__c.getValues('US'));
        }
        
        Case CaseObj = new Case();
        CaseObj.Status ='New';
        CaseObj.origin='Portal';
        CaseObj.contactId = strContactId;
        CaseObj.AccountId = strAccountId;  
        CaseObj.PACustomer_internal_case_number__c=customerInternalCaseNumber;
        if(strSite != ''){
           CaseObj.site__c = strSite;
        }
        if(!Test.isRunningTest()){  
          CaseObj.CommercialReference_lk__c = lstProd2[0].id;
        }
        if(dateOccured !=''){
            system.debug('DateTime.valueOf(dateOccured)==='+DateTime.valueOfGmt(dateOccured));
            system.debug('Now==='+DateTime.Now());
            system.debug('DateTime.valueOf(dateOccured)==='+DateTime.valueOf(dateOccured));
            CaseObj.Alarm_date_and_time__c = DateTime.valueOf(dateOccured);
        }
        CaseObj.Subject = case_subject;
        CaseObj.Priority = Priority;
        CaseObj.SupportCategory__c=system.label.CLOCT15CCC30;//'Troubleshooting*';
        CaseObj.PACaseType__c = casetype;
        CaseObj.PAPlatform__c= platformtype;
        caseObj.CCCountry__c= lstAccountCountry[0].country__c;
        caseObj.LevelOfExpertise__c='Expert';
        if(listCaseTeam!=null && !listCaseTeam.isEmpty() ){
            CaseObj.team__c=listCaseTeam[0].Assigned_CC_Team__c ;
        }
        CaseObj.CustomerRequest__c= case_Description;
        System.debug('------>>'+CaseObj);
        insert CaseObj;
        
        if(case_Subscribe) {
            Subscriber__c objSubscribe = new Subscriber__c();
            objSubscribe.Case__c = CaseObj.Id;
            objSubscribe.User__c = userinfo.getUserId();
            if(String.isBlank(userObj.ContactId)){
                objSubscribe.Email__c = userObj.Email;
            } else{
                objSubscribe.Email__c = userObj.Contact.Email;
                //objSubscribe.Account__c = userObj.Contact.AccountId;
            }
            insert objSubscribe;
        }
        system.debug('******CaseObj*******'+CaseObj);
        return CaseObj.Id;
    }
    @RemoteAction
    Public static List<accountPickList> pickaccountValue(){
        set<id> setAccountId = new set<Id>();
        set<id> setAccountManualSharedId = new set<Id>();
        list<Account>lstAccountId = new list<Account>();
        //User UserObj =[select id,accountId from User where Id=:userinfo.getUserId()] ;
        User UserObj = PA_UtilityClass.getUser(userinfo.getUserId());
        List<accountPickList> lstAccPick = new List<accountPickList>();
        setAccountId.add(UserObj.accountId);
        //for(Account objAcc:[select id,Name, BillingCity, BillingState, BillingCountry, StateProvince__r.StateProvinceCode__c from Account where Id =: UserObj.accountId]){
        for(Account objAcc : PA_UtilityClass.getAccountRecord(setAccountId, setAccountManualSharedId)) {
            accountPickList strobj= new accountPickList();
            strobj.strAccountName = PA_UtilityClass.concatenateAddress(objAcc.Name, objAcc.BillingCity, objAcc.StateProvince__r.StateProvinceCode__c, objAcc.BillingCountry );
            strobj.strAccountId = objAcc.id;
            lstAccPick.add(strobj);
        }
        /*for(AccountShare obj:[select id,AccountID from AccountShare where UserorGroupId=:UserInfo.getUserId()and AccountAccessLevel='Edit']){
            setAccountId.add(obj.AccountID);
        }*/
        setAccountManualSharedId = PA_UtilityClass.getAccountId(false, true, 'Edit', userinfo.getUserId());
        //for(Account objAcc:[select id,Name, BillingCity, BillingState, BillingCountry, StateProvince__r.StateProvinceCode__c from Account where Id IN: setAccountManualSharedId AND Id !=: UserObj.accountId]){
        for(Account objAcc : PA_UtilityClass.getAccountRecord(setAccountManualSharedId, setAccountId)) {
            accountPickList strobj= new accountPickList();
            strobj.strAccountName = PA_UtilityClass.concatenateAddress(objAcc.Name, objAcc.BillingCity, objAcc.StateProvince__r.StateProvinceCode__c, objAcc.BillingCountry );
            strobj.strAccountId = objAcc.id;
            lstAccPick.add(strobj);
        }
        return lstAccPick;
    }
    
    @RemoteAction
    Public static Contact userContact() {
        Contact contactObj = new Contact();
        User userObj = [select id,Contact.Name, contact.Id from User where Id = :userinfo.getUserId()];
        contactObj = userObj.Contact;
        return contactObj;
    }
    
    @RemoteAction
    Public static list<contact> pickContactValue(string strAccountId){
        return [select id,Name from Contact where AccountId = :strAccountId];
    }
    
    @RemoteAction
    public static list<contact> searchContact(String strSearchName, String strAccountId){
        list<contact>lstContact  = new list<contact>();
        strSearchName='%'+strSearchName+'%';
        return [select Id,Name from Contact where AccountId = :strAccountId and Name like:strSearchName];
    }
    
    @RemoteAction
    public static String getLoggedInUserDateTime(){
        String LongDate = PA_UtilityClass.userLocaleDateTimeConversionIncludeSec(System.now());
        return LongDate;
    }
    
    public void getCasePriority(){
       lstPriorityOptions  = new List<String>();
            
       lstPriorityOptions = PA_UtilityClass.getCasePriority();
       strPriorityOptions = JSON.serialize(lstPriorityOptions);     
    }
    
    public void getCaseType(){
       lstCaseTypeOptions = new List<String>();
       List<String> lstCaseTypeOption = new List<String>();
       lstCaseTypeOption = PA_UtilityClass.getCaseType();
       for(String f : lstCaseTypeOption){
           if(f != system.label.CLQ316PA031) {
              lstCaseTypeOptions.add(f);
          }
       }  
       strCaseTypeOptions= JSON.serialize(lstCaseTypeOptions);     
    }
    public void getPlatform(){
       lstPlatformOptions= new List<String>();
       lstPlatformOptions = PA_UtilityClass.getPlatform();
       strPlatformOptions= JSON.serialize(lstPlatformOptions);     
    }
    public class caseCreationParameter{
        public string strAccountId {get;set;}
        public string strContactId{get;set;}
        public string strCommercialReference{get;set;}
        public string strProductId {get;set;}
        public string strSite{get;set;}
        public caseCreationParameter(){}
    
    }
    public class SiteRadioWrapper{
         public GCSSite__c siteobj{get;set;}
         public boolean radioselect{get;set;}
         
         public SiteRadioWrapper(GCSSite__c site){
           siteobj = site;
           radioselect = false;
         }
    }
    
    public class accountPickList{
        String strAccountName{get; set;}
        String strAccountId {get;set;} 
        public accountPickList(){}   
    }
    
}