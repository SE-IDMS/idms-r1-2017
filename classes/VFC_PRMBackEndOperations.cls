public class VFC_PRMBackEndOperations {
    public String selectedObject{get;set;}
    public String selectedPermissionSet{get;set;}
    public String selectedTab{get;set;}
    public String apexScript{get;set;}
    public String userAccessToken{get;set;}
    public Boolean objReadSelected{get;set;}
    public ObjectPermissionWrapper displayPermissions{get;set;}
    public String ObjectsList{get;set;}
    
    public class ObjectListWrapper{
        public String ObjectName;
        public String ObjectAPIName;
    }
    
    public VFC_PRMBackEndOperations(){
        userAccessToken = AP_PRMUtils.getAccessToken(System.Label.CLQ416PRM035, System.Label.CLQ416PRM034, System.Label.CLQ416PRM033, System.Label.CLQ416PRM032);
        //selectedObject = 'PRMCountry__c';
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values(); 
        List<ObjectListWrapper> objWrapperLst = new List<ObjectListWrapper>();
        for(Schema.SObjectType f : gd){
            ObjectListWrapper objWrapper = new ObjectListWrapper();
            objWrapper.ObjectName = f.getDescribe().getLabel();
            objWrapper.ObjectAPIName = f.getDescribe().getName();
            objWrapperLst.add(objWrapper);
        }
        ObjectsList = JSON.serialize(objWrapperLst);
        System.debug('****ObjectsList****'+ObjectsList);
    }
    
    public static list<selectOption> getpermissionSetList() {
        List<SelectOption> permissionSetLst = new List<SelectOption>();
        List<PermissionSet> permSetLst = [SELECT Id, Name, Label FROM PermissionSet WHERE ProfileId = ''];
        permissionSetLst.add(new SelectOption('','--None--'));
        for(PermissionSet pm : permSetLst){
            permissionSetLst.add(new SelectOption(pm.Id,pm.Label));
        }
        permissionSetLst.sort();
        return permissionSetLst;
    }

    public PageReference doSavePermissions(){
        PageReference pageRef;
        try{
            if(displayPermissions != null){
                System.debug('>>>>displayPermissions:'+displayPermissions);
                List<ObjectPermissions> objPermission = new List<ObjectPermissions>();
                List<ObjectPermissions> deleteObjPermission = new List<ObjectPermissions>();
                if(displayPermissions.ObjRead){
                    ObjectPermissions objPermissionsUpsert = new ObjectPermissions(
                        Id = (displayPermissions.ObjId != null?displayPermissions.ObjId:''),
                        PermissionsRead = displayPermissions.ObjRead,
                        PermissionsEdit = displayPermissions.ObjEdit,
                        PermissionsCreate = displayPermissions.ObjCreate,
                        PermissionsDelete = displayPermissions.ObjDelete,
                        PermissionsModifyAllRecords = displayPermissions.ObjModifyAll,
                        PermissionsViewAllRecords = displayPermissions.ObjViewAll,
                        ParentId = selectedPermissionSet,
                        SObjectType = selectedObject
                    );
                    objPermission.add(objPermissionsUpsert);
                }
                else{
                    if(String.isNotBlank(displayPermissions.ObjId)){
                        ObjectPermissions objPermissionsDelete = new ObjectPermissions(Id = displayPermissions.ObjId);
                        deleteObjPermission.add(objPermissionsDelete);
                    }
                }
                List<FieldPermissions> fldsPermissions = new List<FieldPermissions>();
                List<FieldPermissions> deleteFieldPermissions = new List<FieldPermissions>();
                if(displayPermissions.ObjFLS != null && displayPermissions.ObjFLS.size() > 0){
                    for(FieldPermissionWrapper fls : displayPermissions.ObjFLS){
                        if(fls.ReadModified || fls.EditModified){
                            System.debug('****fls ****'+fls );
                            if(fls.FieldRead){
                                System.debug('>>>>displayPermissions.ObjFLS:'+fls);
                                FieldPermissions fld = new FieldPermissions(
                                    PermissionsRead = fls.FieldRead,
                                    PermissionsEdit = fls.FieldEdit,
                                    SObjectType = fls.ObjectAPIName,
                                    Field = fls.ObjectAPIName+'.'+fls.FieldAPIName,
                                    ParentId = selectedPermissionSet
                                );
                                if(String.isNotBlank(fls.FlsId))
                                    fld.Id = fls.FlsId;
                                fldsPermissions.add(fld);
                            }
                            else{
                                if(String.isNotBlank(fls.FlsId)){
                                    FieldPermissions deleteFld = new FieldPermissions(Id = fls.FlsId);
                                    deleteFieldPermissions.add(deleteFld);
                                }
                            }
                        }
                    }
                }
                if(deleteObjPermission != null && deleteObjPermission.size() > 0)
                    DELETE deleteObjPermission;
                if(deleteFieldPermissions != null && deleteFieldPermissions.size() > 0)
                    DELETE deleteFieldPermissions;
                if(objPermission != null && objPermission.size() > 0)
                    UPSERT objPermission;
                if(fldsPermissions != null && fldsPermissions.size() > 0)
                    UPSERT fldsPermissions;
            }
            pageRef = new PageReference('/apex/VFP_PRMBackEndOperations');
            pageRef.setRedirect(true);
        }
        catch(Exception e){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage() + ' '+e.getStackTraceString()));
        }
        return pageRef;
    }

    public static Map<String,String> ObjectFieldsMap(String objName){
        Map<String,String> fldMap = new Map<String,String>();
        SObjectType objectType = Schema.getGlobalDescribe().get(objName);
        Map<String, Schema.SObjectField> schemaFieldMap = objectType.getDescribe().fields.getMap();
        for(String s : schemaFieldMap.keyset()){
            schema.describefieldresult dField = schemaFieldMap.get(s).getDescribe();
            system.debug(dField.getName() + '>>>>'+ dField.getLabel());
            if(dField.isCustom())
                fldMap.put(dField.getName(),dField.getLabel());
        }
        return fldMap;
    }

    public PageReference displayObjectFieldPermissions(){
        System.debug('>>>>selectedObject:'+selectedObject);
        System.debug('>>>>selectedPermissionSet:'+selectedPermissionSet);
        Map<String,String> fieldsMap = ObjectFieldsMap(selectedObject);
        List<FieldPermissionWrapper> flsWrapper = new List<FieldPermissionWrapper>();
        displayPermissions = new ObjectPermissionWrapper();
        displayPermissions.ObjRead = displayPermissions.ObjEdit = displayPermissions.ObjCreate = displayPermissions.ObjDelete = displayPermissions.ObjModifyAll = displayPermissions.ObjViewAll = false;
        displayPermissions.ObjectAPIName = selectedObject;
        List<ObjectPermissions> objPermissions = [SELECT Id,ParentId,PermissionsCreate,PermissionsDelete,PermissionsEdit,PermissionsModifyAllRecords,PermissionsRead,PermissionsViewAllRecords,SobjectType FROM ObjectPermissions WHERE SobjectType = :selectedObject AND ParentId = :selectedPermissionSet];
        System.debug('>>>>objPermissions:'+objPermissions);
        if(objPermissions != null && objPermissions.size() > 0){
            displayPermissions.ObjRead = objPermissions[0].PermissionsRead;
            displayPermissions.ObjEdit = objPermissions[0].PermissionsEdit;
            displayPermissions.ObjCreate = objPermissions[0].PermissionsCreate;
            displayPermissions.ObjDelete = objPermissions[0].PermissionsDelete;
            displayPermissions.ObjModifyAll = objPermissions[0].PermissionsModifyAllRecords;
            displayPermissions.ObjViewAll = objPermissions[0].PermissionsViewAllRecords;
            displayPermissions.ObjId = objPermissions[0].Id;
        }
        List<FieldPermissions> fldPermissions = [SELECT Field,Id,ParentId,PermissionsEdit,PermissionsRead,SobjectType FROM FieldPermissions WHERE SobjectType = :selectedObject AND ParentId = :selectedPermissionSet];
        System.debug('>>>>fldPermissions:'+fldPermissions);
        Map<String,String> existingFLS = new Map<String,String>();
        for(FieldPermissions fls : fldPermissions){
            existingFLS.put(fls.Field.Replace(selectedObject+'.',''),fls.Field.Replace(fls.Field+'.',selectedObject));
            FieldPermissionWrapper fWrapper = new FieldPermissionWrapper();
                fWrapper.FlsId = fls.Id;
                fWrapper.ParentId = fls.ParentId;
                fWrapper.FieldRead = fls.PermissionsRead;
                fWrapper.FieldEdit = fls.PermissionsEdit;
                fWrapper.ObjectAPIName = fls.SobjectType;
                fWrapper.FieldAPIName = fls.Field.Replace(selectedObject+'.','');
                fWrapper.FieldLabel = fieldsMap.get(fWrapper.FieldAPIName);
            flsWrapper.add(fWrapper);
        }
        for(String s : fieldsMap.keyset()){
            if(!existingFLS.containsKey(s)){
                FieldPermissionWrapper fWrapper = new FieldPermissionWrapper();
                fWrapper.ParentId = selectedPermissionSet;
                fWrapper.FieldRead = fWrapper.FieldEdit = false;
                fWrapper.ObjectAPIName = selectedObject;
                fWrapper.FieldAPIName = s;
                fWrapper.FieldLabel = fieldsMap.get(fWrapper.FieldAPIName);
                fWrapper.FlsId = '';
                flsWrapper.add(fWrapper);
            }
        }
        if(flsWrapper != null && flsWrapper.size() > 0)
            displayPermissions.ObjFLS = flsWrapper;
        System.debug('>>>>displayPermissions:'+displayPermissions);
        return null;
    }
    
   /* @RemoteAction
    public static String ObjectsList(){
         Map<String,String> ObjMap = new Map<String,String>();
         Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
         for(String s : schemaMap.keyset())
            ObjMap.put(schemamap.get(s).getDescribe().getName(),schemamap.get(s).getDescribe().getLabel());
         return JSON.serialize(ObjMap);
    }*/
    
    public Class FieldPermissionWrapper{
        public Boolean FieldRead{get;set;}
        public Boolean FieldEdit{get;set;}
        public Boolean ReadModified{get;set;}
        public Boolean EditModified{get;set;}
        public String ParentId{get;set;}
        public String FieldAPIName{get;set;}
        public String FieldLabel{get;set;}
        public String ObjectAPIName{get;set;}
        public String FlsId{get;set;}
        public FieldPermissionWrapper(){
            this.FieldRead = this.FieldEdit = this.ReadModified = this.EditModified = false;
            this.FieldAPIName = this.FieldLabel = this.ObjectAPIName = this.ParentId = '';
        }
    }
    public Class ObjectPermissionWrapper{
        public Boolean ObjRead{get;set;}
        public Boolean ObjEdit{get;set;}
        public Boolean ObjCreate{get;set;}
        public Boolean ObjDelete{get;set;}
        public Boolean ObjModifyAll{get;set;}
        public Boolean ObjViewAll{get;set;}
        public Boolean ObjModified{get;set;}
        public String ObjectAPIName{get;set;}
        public String ObjectLabel{get;set;}
        public String ObjId{get;set;}
        public List<FieldPermissionWrapper> ObjFLS{get;set;}
        
        public ObjectPermissionWrapper(){
            this.ObjRead = this.ObjEdit = this.ObjCreate = this.ObjDelete = this.ObjModifyAll = this.ObjViewAll = this.ObjModified = false;
            this.ObjectAPIName = this.ObjectLabel = this.ObjId = '';
            ObjFLS = new List<FieldPermissionWrapper>();
        }
    }
}