/*

    Date Created    : 14/03/2014 November Release
    Description     : Batch Apex class Installed Product
              
*/
    global class Batch_IPUpdate implements Database.Batchable<sObject>{

     global final String Query;
     global String ErrorMessages ='';
     global string STANDARD_IP = Label.CLAPR14SRV09;
     global string SE_READONLYIP = Label.CLAPR14SRV10;
     global string NewQuery = Label.CLNOV16FS001; //added by suhail for november release 2016

     global Database.QueryLocator start(Database.BatchableContext BC)
     { 
       //return Database.getQueryLocator([select Id,Name,AssetCategory2__c,LifeCycleStatusOfTheInstalledProduct__c,SVMXC__Company__c,UnderContract__c,DecomissioningDate__c,SVMXC__Company__r.ClassLevel1__c From SVMXC__Installed_Product__c Where (RecordTypeId=:SE_READONLYIP or RecordTypeId=:STANDARD_IP)]);
       if(NewQuery == 'true'){
           return Database.getQueryLocator([select Id,Name,AssetCategory2__c,LifeCycleStatusOfTheInstalledProduct__c,SVMXC__Company__c,UnderContract__c,DecomissioningDate__c,SVMXC__Company__r.ClassLevel1__c  From SVMXC__Installed_Product__c
            where not(
            (AssetCategory2__c = 'Category 0' and LifeCycleStatusOfTheInstalledProduct__c in ('LOST','SCRAPPED'))  OR 
            (AssetCategory2__c = 'Category 0' and LifeCycleStatusOfTheInstalledProduct__c = 'NOT_USED' and DecomissioningDate__c <> null) OR
            (UnderContract__c = true and AssetCategory2__c = 'Category 3' ) OR
            (AssetCategory2__c = 'Category 2' and LifeCycleStatusOfTheInstalledProduct__c = 'IN_USE' )
                  )]);
        }
       else{
       return Database.getQueryLocator([select Id,Name,AssetCategory2__c,LifeCycleStatusOfTheInstalledProduct__c,SVMXC__Company__c,UnderContract__c,DecomissioningDate__c,SVMXC__Company__r.ClassLevel1__c From SVMXC__Installed_Product__c ]);
       }
     
     
     
     }

    global void execute(Database.BatchableContext BC, List<sObject> scope){           
        List<sObject> sobjList= Ap_Installed_Product.AssetCategory(scope);
        if(sobjList != null && sobjList.size()>0)
        Database.update(sobjList,false);
    }


    global void finish(Database.BatchableContext BC){
        

    }

}