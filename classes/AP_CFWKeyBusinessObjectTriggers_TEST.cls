/*
    Author          : Bhuvana S
    Description     : Test class for AP_CFWKeyBusinessObjectTriggers
*/
@isTest
private class AP_CFWKeyBusinessObjectTriggers_TEST 
{
    static testMethod void testudaBeforeUpdate()
    {
        test.startTest();
            
        CFWRiskAssessment__c cfwr = new CFWRiskAssessment__c();
        cfwr.ProjectName__c = 'testProjectName';
        insert cfwr;
        
        CFWApplicationUsers__c cau = new CFWApplicationUsers__c();
        cau.CertificationRiskAssessment__c = cfwr.Id;
        insert cau;
        
        CFWKeyBusinessObjects__c cbo = new CFWKeyBusinessObjects__c();
        cbo.CertificationRiskAssessment__c = cfwr.Id;
        insert cbo;
                
        test.stoptest();
    }
}