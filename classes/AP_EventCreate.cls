global class AP_EventCreate {



    // Asynchronous mode commented on September 13rd 2016 by Y. Tisserand to resolve UNABLE_TO_LOCK_ROW error
    //@future
    public static void CreateAssignedToolsTechnicians(Set<id> widset , String eventObjList, String eventType){   
        String dateTimeFormatTxt = 'dd MMM yyyy HH:mm';
        String dateTimeFormatNonTxt = 'dd/MM/yyyy HH:mm';
    
        List<Event> EventList =(List<Event>)JSON.deserialize(eventObjList,List<Event>.class);
        
        System.debug('**********************************************************');
        System.debug('\n HLog :'+EventList.size());
        
        List<SVMXC__Service_Order__c> woList= new List<SVMXC__Service_Order__c>();
        woList=[select id,SVMXC__Group_Member__c,CustomerTimeZone__c from SVMXC__Service_Order__c where id in :widset];
        Map<id,SVMXC__Service_Order__c> idwoMap = new Map<id,SVMXC__Service_Order__c>();
        idwoMap.putAll(woList);
        map<id,SVMXC__Service_Group_Members__c> techmap= new map<id,SVMXC__Service_Group_Members__c>();
        List<AssignedToolsTechnicians__c> TechToolList = new List<AssignedToolsTechnicians__c>();
        List<Id> UserList = new List<ID>();
        for(Event seventobj :EventList){
            UserList.add(seventobj.OwnerID);
            System.debug('\n HLog :'+seventobj);
        }
                
        Map<id,Boolean> widIsattMap = new Map<id,Boolean>();
        Map<id,Set<id>> widattsetMap = new Map<id,Set<id>>();
                
        List<AssignedToolsTechnicians__c> asToolTechList=[select id,PrimaryUser__c,TechnicianEquipment__c,WorkOrder__c from AssignedToolsTechnicians__c where WorkOrder__c in :wIdset and Status__c ='Assigned'];
        for(AssignedToolsTechnicians__c  att: asToolTechList){
            if(!widIsattMap.containsKey(att.WorkOrder__c )){
                widIsattMap.put(att.WorkOrder__c,true);
                widattsetMap.put(att.WorkOrder__c,new Set<Id>());
                widattsetMap.get(att.WorkOrder__c).add(att.id);
                
            }
            else{
                widattsetMap.get(att.WorkOrder__c).add(att.id);
            }
        }
                
        List<SVMXC__Service_Group_Members__c> techList = new List<SVMXC__Service_Group_Members__c>();
        if(EventList != null)       
        techList = [Select ID,SVMXC__Salesforce_User__c, SVMXC__Salesforce_User__r.TimeZoneSidKey 
            from SVMXC__Service_Group_Members__c where SVMXC__Salesforce_User__c in:UserList ];
        for(SVMXC__Service_Group_Members__c assignedFSR:techList){
            techmap.put(assignedFSR.SVMXC__Salesforce_User__c,assignedFSR);
        }
        
         for (Event sevent : EventList)
        {
           
               if(widattsetMap.containskey(sevent.WhatId)){
                    
                    Set<id> techidset = widattsetMap.get(sevent.WhatId);
                    if(!techidset.contains(techmap.get(sevent.OwnerID).id)){
                            if(techmap.containskey(sevent.OwnerID))
                            {
                                AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c();
                                AssignedTech.TechnicianEquipment__c = techmap.get(sevent.OwnerID).id;                  
                                AssignedTech.WorkOrder__c = sevent.WhatId;
                                AssignedTech.EventId__c= sevent.Id;
                                AssignedTech.startDateTime__c= sevent.StartDateTime;
                                AssignedTech.EndDateTime__c = sevent.EndDateTime;
                                AssignedTech.TECH_CustomerStartDateTime__c = sevent.StartDateTime.format(dateTimeFormatNonTxt, idwoMap.get(sevent.WhatId).CustomerTimeZone__c);
                                AssignedTech.TECH_CustomerEndDateTime__c  = sevent.EndDateTime.format(dateTimeFormatNonTxt, idwoMap.get(sevent.WhatId).CustomerTimeZone__c);
                                AssignedTech.TECH_FSRStartDateTime__c = sevent.StartDateTime.format(dateTimeFormatTxt, techmap.get(sevent.OwnerID).SVMXC__Salesforce_User__r.TimeZoneSidKey);
                                AssignedTech.TECH_FSREndDateTime__c  = sevent.EndDateTime.format(dateTimeFormatTxt, techmap.get(sevent.OwnerID).SVMXC__Salesforce_User__r.TimeZoneSidKey);
                                AssignedTech.Status__c = 'Assigned';
                                AssignedTech.FSR_Salesforce_User__c = sevent.OwnerID;
                                AssignedTech.DrivingTime__c = sevent.SVMXC__Driving_Time__c;
                                AssignedTech.DrivingTimeHome__c = sevent.SVMXC__Driving_Time_Home__c;
                                
                                if(idwoMap.Containskey(sevent.WhatId))
                                {
                                   if(idwoMap.get(sevent.WhatId).SVMXC__Group_Member__c == AssignedTech.TechnicianEquipment__c )
                                   AssignedTech.PrimaryUser__c = True;
                                }
                                
                                TechToolList.add(AssignedTech);
                            } 
                    }
                     
                }
                else{
                    // 
                    if(techmap.containskey(sevent.OwnerID))
                            {
                                AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c();
                                AssignedTech.TechnicianEquipment__c = techmap.get(sevent.OwnerID).id;                  
                                AssignedTech.WorkOrder__c = sevent.WhatId;
                                AssignedTech.EventId__c= sevent.Id;
                                AssignedTech.startDateTime__c= sevent.StartDateTime;
                                AssignedTech.Status__c = 'Assigned';
                                AssignedTech.FSR_Salesforce_User__c = sevent.OwnerID;
                                AssignedTech.EndDateTime__c = sevent.EndDateTime;
                                AssignedTech.TECH_CustomerStartDateTime__c = sevent.StartDateTime.format(dateTimeFormatNonTxt, idwoMap.get(sevent.WhatId).CustomerTimeZone__c);
                                AssignedTech.TECH_CustomerEndDateTime__c  = sevent.EndDateTime.format(dateTimeFormatNonTxt, idwoMap.get(sevent.WhatId).CustomerTimeZone__c);
                                AssignedTech.TECH_FSRStartDateTime__c = sevent.StartDateTime.format(dateTimeFormatTxt, techmap.get(sevent.OwnerID).SVMXC__Salesforce_User__r.TimeZoneSidKey);
                                AssignedTech.TECH_FSREndDateTime__c  = sevent.EndDateTime.format(dateTimeFormatTxt, techmap.get(sevent.OwnerID).SVMXC__Salesforce_User__r.TimeZoneSidKey);
                                AssignedTech.DrivingTime__c = sevent.SVMXC__Driving_Time__c;
                                AssignedTech.DrivingTimeHome__c = sevent.SVMXC__Driving_Time_Home__c;
                                if(idwoMap.Containskey(sevent.WhatId))
                                {
                                   if(idwoMap.get(sevent.WhatId).SVMXC__Group_Member__c == AssignedTech.TechnicianEquipment__c )
                                   AssignedTech.PrimaryUser__c = True;
                                }
                                TechToolList.add(AssignedTech);
                            }
                }
                
                       
        }
        if(TechToolList != null)
                Database.upsert(TechToolList);
        
        
    
    }
    @future
    public static void SVMXCreateAssignedToolsTechnicians(String eventObjList, set<id> evtid)
    {   
        String dateTimeFormatTxt = 'dd MMM yyyy HH:mm';
        String dateTimeFormatNonTxt = 'dd/MM/yyyy HH:mm';
        List<SVMXC__SVMX_Event__c> EventList =(List<SVMXC__SVMX_Event__c>)JSON.deserialize(eventObjList,List<SVMXC__SVMX_Event__c>.class);
        List<AssignedToolsTechnicians__c> TechToolList = new List<AssignedToolsTechnicians__c>();
        map<id,id> techmap= new map<id,id>();
        List<AssignedToolsTechnicians__c> techList = new List<AssignedToolsTechnicians__c>();
        if(evtid.size()>0)
        {
            techList=[Select Id,EventId__c from AssignedToolsTechnicians__c where EventId__c in:evtid ];
            for(AssignedToolsTechnicians__c t:techList)
            {
                techmap.put(t.EventId__c,t.id);
            }
        }
        for (SVMXC__SVMX_Event__c sevent : EventList)
        {
            if(techmap.containskey(sevent.id))
            {
                AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c(); 
                AssignedTech.TechnicianEquipment__c = sevent.SVMXC__Technician__c;                   
                AssignedTech.WorkOrder__c = sevent.SVMXC__WhatId__c;
                AssignedTech.EventId__c= sevent.Id;
                AssignedTech.startDateTime__c= sevent.SVMXC__StartDateTime__c;
                AssignedTech.EndDateTime__c = sevent.SVMXC__EndDateTime__c;
                AssignedTech.DrivingTime__c = sevent.SVMXC__Driving_Time__c;
                AssignedTech.DrivingTimeHome__c = sevent.SVMXC__Driving_Time_Home__c;
                TechToolList.add(AssignedTech);
            }
            else
            {
                AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c(); 
                AssignedTech.TechnicianEquipment__c = sevent.SVMXC__Technician__c;                   
                AssignedTech.WorkOrder__c = sevent.SVMXC__WhatId__c;
                AssignedTech.EventId__c= sevent.Id;
                AssignedTech.startDateTime__c= sevent.SVMXC__StartDateTime__c;
                AssignedTech.EndDateTime__c = sevent.SVMXC__EndDateTime__c;
                AssignedTech.TECH_CustomerStartDateTime__c = sevent.SVMXC__StartDateTime__c.format(dateTimeFormatNonTxt);
                AssignedTech.TECH_CustomerEndDateTime__c  = sevent.SVMXC__EndDateTime__c.format(dateTimeFormatNonTxt);
                AssignedTech.TECH_FSRStartDateTime__c = sevent.SVMXC__StartDateTime__c.format(dateTimeFormatTxt);
                AssignedTech.TECH_FSREndDateTime__c  = sevent.SVMXC__EndDateTime__c.format(dateTimeFormatTxt);             
                AssignedTech.DrivingTime__c = sevent.SVMXC__Driving_Time__c;
                AssignedTech.DrivingTimeHome__c = sevent.SVMXC__Driving_Time_Home__c;
                TechToolList.add(AssignedTech);
            }
        }
        if(TechToolList != null)
            Database.upsert(TechToolList);
    }
    @future
    public static void UpdateAssignedToolsTechnicians(String eventObjList ,set<id> evtid){
        String dateTimeFormatTxt = 'dd MMM yyyy HH:mm';
        String dateTimeFormatNonTxt = 'dd/MM/yyyy HH:mm';
        List<Event> EventList =(List<Event>)JSON.deserialize(eventObjList,List<Event>.class);
        List<AssignedToolsTechnicians__c> TechToolList = new List<AssignedToolsTechnicians__c>();
         map<id,id> techmap= new map<id,id>();
         List<AssignedToolsTechnicians__c> techList = new List<AssignedToolsTechnicians__c>();
                if(evtid.size()>0){
                    techList=[Select Id,EventId__c 
                        from AssignedToolsTechnicians__c where EventId__c in:evtid ];
                    for(AssignedToolsTechnicians__c t:techList){
                        techmap.put(t.EventId__c,t.id);
                    }
                }
                for (Event sevent : EventList)
                {
                    if(techmap.containskey(sevent.id))
                        {
                            AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c(id=techmap.get(sevent.id));  
                            AssignedTech.startDateTime__c= sevent.StartDateTime;
                            AssignedTech.EndDateTime__c = sevent.EndDateTime;        
                            AssignedTech.DrivingTime__c = sevent.SVMXC__Driving_Time__c;
                            AssignedTech.DrivingTimeHome__c = sevent.SVMXC__Driving_Time_Home__c;
                            TechToolList.add(AssignedTech);
                        }          
                }
                
                if(TechToolList != null)
                Database.upsert(TechToolList);
        
        
        
    }


}