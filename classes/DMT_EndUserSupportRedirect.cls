public with sharing class DMT_EndUserSupportRedirect {

    public PageReference PgRedirectOnLoad() {
        PageReference pr = new PageReference('http://2929it.schneider-electric.com');
        pr.setredirect(true);
        return pr;
    }

}