/* test class for VFC_ExecuteBatchforsObject  */
@istest
private class VFC_ExecuteBatchforsObject_Test {
        
    static testmethod void unitTest(){
    
        string toObject1;
        string fromObject1;
        boolean bln_Selected1;
        string conditionExpr1;
        list<VFC_ExecuteBatchforsObject.BatchReferentialDataWarap> listWarapRefdata;
        //Inserting ReferentialDataMapping__c Records
        List<ReferentialDataMapping__c> refDataMap = new List<ReferentialDataMapping__c>();
        ReferentialDataMapping__c ref1 = new ReferentialDataMapping__c();
            ref1.fromfield__c='parenthierarchy__r.BusinessUnit__c';
            ref1.fromobject__c='OPP_Product__c';
            ref1.tofield__c='TECH_CROrderBusiness__c';
            ref1.active__c=true;
            ref1.whereclausefromfield__c='name';
            ref1.whereclausetofield__c='CommercialReferenceOrdered__c';
            ref1.toobject__c='ComplaintRequest__c';
            
            refDataMap.add(ref1);
        ReferentialDataMapping__c ref2 = new ReferentialDataMapping__c();
            ref2.fromfield__c='BusinessUnit__c';
            ref2.fromobject__c='OPP_Product__c';
            ref2.tofield__c='Status__c';
            ref2.active__c=true;
            ref2.whereclausefromfield__c='name';
            ref2.whereclausetofield__c='CommercialReferenceOrdered__c';
            ref2.toobject__c='ComplaintRequest__c';
        
            refDataMap.add(ref2);
            
        insert refDataMap;
        
        OPP_Product__c OppPdct = new OPP_Product__c(name='REPARATIONS Error',productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR',
                            hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='02BQ8SZ2------0DZH', 
                            businessunit__c='Open',isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP',isold__c=false);
        insert OppPdct;
        
        List<OPP_Product__c> ListPdct=new List<OPP_Product__c>();
        OPP_Product__c OppPdct1 = new OPP_Product__c(name='REPARATIONS I H M',productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR',
                            hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY',businessunit__c='Open',
                            parenthierarchy__c=OppPdct.id,isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP',isold__c=false);
        ListPdct.add(OppPdct1);
        
        OPP_Product__c OppPdct2 = new OPP_Product__c(name='REPARATIONS I H M',productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR',
                            hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY',businessunit__c='INDUSTRIAL AUTOMATION', 
                            parenthierarchy__c=OppPdct.id,isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', isold__c=false);
        ListPdct.add(OppPdct2);
        
        insert ListPdct;
        
                Country__c Cntry = Utils_TestMethods.createCountry();
        Insert Cntry;

        BusinessRiskEscalationEntity__c BsnsRskEsc = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1',SubEntity__c='LCR Sub-Entity',Location__c='LCR Location',
                                            Location_Type__c= 'Return Center',Street__c = 'LCR Street',RCPOBox__c = '123456',RCCountry__c = Cntry.Id,RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',ContactEmail__c ='Test@schneider.com');
        Insert BsnsRskEsc; 

        List<ComplaintRequest__c> CompReq=new List<ComplaintRequest__c>();
        ComplaintRequest__c CompReq1 = new ComplaintRequest__c(
        CommercialReferenceOrdered__c='REPARATIONS I H M',
        letterofreserve__c=false, forcemanualselection__c=false, infotransport__c='No', infolac__c='No', tech_answer__c=false, category__c='Product Selection & Services', 
        leadstoproductqualityissue__c='No', status__c='Open', tech_symptomsavailable__c=false, tech_forward__c=false, needtolaunchtechnicalcomplaint__c='No', reportingentity__c=BsnsRskEsc.id, 
        tech_isblankcomment__c=true, doescustomerreturnproduct__c='No',  accountableorganization__c=BsnsRskEsc.id, accountableorganizationworking__c=false, infoquality__c='No', tech_external1__c=false, 
        reason__c='Catalog Selection', tech_accountable_org__c=false, qualitycheck__c=false, stockcheckrequired__c=false, priority__c='Normal', countforccl__c=false, 
        tech_ischeck__c=false, tech_isexternal__c='No', infoccc__c='No'
        );
        CompReq.add(CompReq1);
        
        ComplaintRequest__c CompReq2 = new ComplaintRequest__c(
        CommercialReferenceOrdered__c='REPARATIONS Error',
        letterofreserve__c=false, forcemanualselection__c=false, infotransport__c='No', infolac__c='No', tech_answer__c=false, category__c='Product Selection & Services', 
        leadstoproductqualityissue__c='No', status__c='Closed', tech_symptomsavailable__c=false, tech_forward__c=false, needtolaunchtechnicalcomplaint__c='No', reportingentity__c=BsnsRskEsc.id,
        tech_isblankcomment__c=true, doescustomerreturnproduct__c='No',  accountableorganization__c=BsnsRskEsc.id,accountableorganizationworking__c=false, infoquality__c='No', tech_external1__c=false, 
        reason__c='Catalog Selection', tech_accountable_org__c=false, qualitycheck__c=false, stockcheckrequired__c=false, priority__c='Normal', countforccl__c=false, 
        tech_ischeck__c=false, tech_isexternal__c='No', infoccc__c='No', SatisfactionOnQualityofAnswer__c='ABCDEFGHIJKLMNOPQRSTUVWXYZ', FinalResolution__c='ABCD', Final_Resolution__c='EFGH', SatisfactionOnSpeedofAnswer__c='ABCD'
        );
        CompReq.add(CompReq2);
        insert CompReq;
        
        VFC_ExecuteBatchforsObject excBtch =  new VFC_ExecuteBatchforsObject();
        VFC_ExecuteBatchforsObject.selectObjectName  = 'ComplaintRequest__c';
        VFC_ExecuteBatchforsObject.iSelectObjectName  = 'ComplaintRequest__c';
        
        VFC_ExecuteBatchforsObject.Batch_Status  wrprclass1 =  new VFC_ExecuteBatchforsObject.Batch_Status();
        VFC_ExecuteBatchforsObject.ReferentialDataWarap  wrprclass2 =  new VFC_ExecuteBatchforsObject.ReferentialDataWarap();
        VFC_ExecuteBatchforsObject.BatchReferentialDataWarap  wrprclass3 =  new VFC_ExecuteBatchforsObject.BatchReferentialDataWarap(toObject1,fromObject1,conditionExpr1);
        wrprclass3.toObject='ComplaintRequest__c';
        wrprclass3.fromObject='ReferentialDataMapping__c';
        wrprclass3.bln_Selected=true;
        wrprclass3.conditionExpr='';
        excBtch.listWarapRefdata =  new list<VFC_ExecuteBatchforsObject.BatchReferentialDataWarap>{wrprclass3};
        
        excBtch.RunBatch('ComplaintRequest__c',true);
        excBtch.Run_Batch();
        excBtch.callBacthClass();
        excBtch.getBatchJobsStatus();
    }
}