public class VFC_NewProgramRequirement
{
    /*=======================================
      CONSTRUCTOR
    =======================================*/   
    
    // VFC_NewProgramRequirement standard controller extension
    public VFC_NewProgramRequirement(ApexPages.StandardController controller){
        System.debug('VFC_NewProgramRequirement.Constructor.INFO - Constructor is called.');
        System.debug('VFC_NewProgramRequirement.Constructor.INFO - End of constructor.'); 
    }
    /*=======================================
      Action Method, Called from VF Page
     =======================================*/   
    
    public pagereference goToProgramReqEditPage(){
        System.Debug('VFCNewProgramReq goToProgramReqEditPage - Method is called.');
        // Create a new edit page for the Program Requirement 
        PageReference programReqNewEditPage = new PageReference('/'+SObjectType.ProgramRequirement__c.getKeyPrefix()+'/e' );

        try {
            String retURL = system.currentpagereference().getParameters().get('retURL'); // Get return URL paramater
            String prgLevel = System.currentpagereference().getParameters().get(Label.CLMAY13PRM01); // Get the Level Name
            String prgLevelID   = System.currentpagereference().getParameters().get(Label.CLMAY13PRM29); // Get the Level ID
            String recordTypeID = System.currentpagereference().getParameters().get(Label.CLMAY13PRM32); // Get the Record type id ID
            String prgId = System.currentpagereference().getParameters().get(Label.CLMAY13PRM31); // Get the Record type id ID
            String prgName = System.currentpagereference().getParameters().get(Label.CLMAY13PRM30); // Get the Record type id ID
            ProgramLevel__c ProgramLevel = new ProgramLevel__c();
            System.debug('### LevelName ###: ' +  prgLevel );
            System.debug('### LevelId ###:' + prgLevelID   );
            
            // Set the return URL to the edit page with the value from the current visual force page
            if(retURL  != null) {
                programReqNewEditPage.getParameters().put(Label.CL00330, retURL);  
                System.Debug('VFC_NewProgramRequirement.goToProgramReqEditPage.SUCCESS - Return URL (retURL) =' +retURL);
            }
            else if(prgLevelID!=null && prgLevelID!='' ){
                programReqNewEditPage.getParameters().put(Label.CL00330, '/'+ prgLevelID);  
                
               
                System.Debug('VFC_NewProgramRequirement.goToProgramReqEditPage.WARNING - Return URL is null, Return to Level, if Level Id is not null');
                
            }
            
            
            // Set the the Level ID for the next page with the ID get through the URL for the current page
            if(prgLevelID != null){
                programReqNewEditPage.getParameters().put(Label.CLMAY13PRM01, prgLevel);  
                programReqNewEditPage.getParameters().put(Label.CLMAY13PRM29, prgLevelID);
                ProgramLevel = [Select partnerprogram__c,partnerprogram__r.name from programlevel__c where id =:prgLevelID limit 1];
            }
            else {
                System.Debug('VFC_NewProgramRequirement.goToProgramReqEditPage.WARNING - Level id is null.');
            }
             if(recordTypeID != null){
                programReqNewEditPage.getParameters().put(Label.CLMAY13PRM32, recordTypeID );  
                
            }
            
            //PrePopulate Partner Program 
            if(ProgramLevel != null)
            {    
                programReqNewEditPage.getParameters().put(Label.CLMAY13PRM30,ProgramLevel.PartnerProgram__r.Name);
                programReqNewEditPage.getParameters().put(Label.CLMAY13PRM31,ProgramLevel.PartnerProgram__c);
            }
            if(prgId != null)
            {
                programReqNewEditPage.getParameters().put(Label.CLMAY13PRM30,prgName);
                programReqNewEditPage.getParameters().put(Label.CLMAY13PRM31,prgId );
            }
            // Disable override
            programReqNewEditPage.getParameters().put(Label.CL00690, Label.CL00691); 
            System.Debug('VFC_NewProgramRequirement.goToProgramReqEditPage.INFO - Override disabled.');
            
        }
        catch(Exception e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
                System.debug('VFC_NewProgramRequirement.programReqNewEditPage.'+e.getTypeName()+' - '+ e.getMessage());
                System.debug('VFC_NewProgramRequirement.programReqNewEditPage.'+e.getTypeName()+' - Line = '+ e.getLineNumber());
                System.debug('VFC_NewProgramRequirement.programReqNewEditPage.'+e.getTypeName()+' - Stack trace = '+ e.getStackTraceString());
        }
        System.Debug('VFC_NewProgramRequirement End of Method.');
        return programReqNewEditPage;
    }
}