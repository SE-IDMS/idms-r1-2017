/*
    Author          : Grégory GACKIERE (SFDC) 
    Date Created    : 
    Description     : 
*/
@isTest
private class TreeViewAjaxRespController_TEST {

    static testMethod void myUnitTest() {
        REF_OrganizationRecord__c bo = Utils_TestMethods.createOrganizationRecord('APAC CHINA TEST', 'Business Organization');
        insert bo; 
        REF_OrganizationRecord__c geo = Utils_TestMethods.createOrganizationRecord('APAC TEST', 'Geography');
        insert geo;
        REF_OrganizationRecord__c country = Utils_TestMethods.createOrganizationRecord('CHINA TEST', 'Country');
        insert country;
        REF_TeamRef__c team = Utils_TestMethods.createTeam();
        insert team; 
        REF_RoutingBOBS__c mapping = Utils_TestMethods.createRouting('APAC CHINA TEST', bo.Id, geo.Id, country.Id,null, team.Id ); 
        insert mapping;
        TreeViewAjaxRespController c = new TreeViewAjaxRespController();
        PageReference pageRef = Page.TreeViewAjaxResponder;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('root', bo.Id);
        Test.startTest();
        c.doSearch();
        c.getResult();
        Test.stopTest();
    }
    static testMethod void myUnitTest2() {
        REF_OrganizationRecord__c bo = Utils_TestMethods.createOrganizationRecord('APAC CHINA TEST', 'Business Organization');
        insert bo; 
        REF_OrganizationRecord__c geo = Utils_TestMethods.createOrganizationRecord('APAC TEST', 'Geography');
        insert geo;
        REF_OrganizationRecord__c country = Utils_TestMethods.createOrganizationRecord('CHINA TEST', 'Country');
        insert country;
        REF_TeamRef__c team = Utils_TestMethods.createTeam();
        insert team; 
        REF_RoutingBOBS__c mapping = Utils_TestMethods.createRouting('APAC CHINA TEST', bo.Id, geo.Id, country.Id,null, team.Id ); 
        insert mapping;
        TreeViewAjaxRespController c = new TreeViewAjaxRespController();
        PageReference pageRef = Page.TreeViewAjaxResponder;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('root', bo.Id);
        ApexPages.currentPage().getParameters().put('object.type','Account');
        ApexPages.currentPage().getParameters().put('core.apexpages.devmode.url','1');
        Test.startTest();
        c.doSearch();
        c.getResult();
        Test.stopTest();
    }
    static testMethod void myUnitTest3() {
        REF_OrganizationRecord__c bo = Utils_TestMethods.createOrganizationRecord('APAC CHINA TEST', 'Business Organization');
        insert bo; 
        REF_OrganizationRecord__c geo = Utils_TestMethods.createOrganizationRecord('APAC TEST', 'Geography');
        insert geo;
        REF_OrganizationRecord__c country = Utils_TestMethods.createOrganizationRecord('CHINA TEST', 'Country');
        insert country;
        REF_TeamRef__c team = Utils_TestMethods.createTeam();
        insert team; 
        REF_RoutingBOBS__c mapping = Utils_TestMethods.createRouting('APAC CHINA TEST', bo.Id, geo.Id, country.Id,null, team.Id ); 
        insert mapping;
        TreeViewAjaxRespController c = new TreeViewAjaxRespController();
        PageReference pageRef = Page.TreeViewAjaxResponder;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('root', bo.Id+'-'+geo.id);
        ApexPages.currentPage().getParameters().put('object.type','Account');
        ApexPages.currentPage().getParameters().put('core.apexpages.devmode.url','1');
        Test.startTest();
        c.doSearch();
        c.getResult();
        Test.stopTest();
    }
    static testMethod void myUnitTest4() {
        REF_OrganizationRecord__c bo = Utils_TestMethods.createOrganizationRecord('APAC CHINA TEST', 'Business Organization');
        insert bo; 
        REF_OrganizationRecord__c geo = Utils_TestMethods.createOrganizationRecord('APAC TEST', 'Geography');
        insert geo;
        REF_OrganizationRecord__c country = Utils_TestMethods.createOrganizationRecord('CHINA TEST', 'Country');
        insert country;
        REF_TeamRef__c team = Utils_TestMethods.createTeam();
        insert team; 
        REF_RoutingBOBS__c mapping = Utils_TestMethods.createRouting('APAC CHINA TEST', bo.Id, geo.Id, country.Id,null, team.Id ); 
        insert mapping;
        TreeViewAjaxRespController c = new TreeViewAjaxRespController();
        PageReference pageRef = Page.TreeViewAjaxResponder;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('root', bo.Id+'-'+geo.id+'-'+country.id);
        ApexPages.currentPage().getParameters().put('object.type','Account');
        ApexPages.currentPage().getParameters().put('core.apexpages.devmode.url','1');
        Test.startTest();
        c.doSearch();
        c.getResult();
        Test.stopTest();
    }
}