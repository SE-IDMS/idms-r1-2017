@isTest
private class SyncObjectsBatchTest {
	
	@isTest static void test_method_one() {
			ObjectSync__c conf = SyncObjectsTestUtil.createContactAccountBulk(null,null);
			SyncObjectsBatch synBatch = new SyncObjectsBatch(conf);

		Test.startTest();
	       
	       Database.executeBatch(synBatch);
	       Test.stopTest();
	}
	
	
}