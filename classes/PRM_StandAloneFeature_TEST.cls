/**
13-Sep-2013
Shruti Karn  
PRM Oct13 Release    
Initial Creation: test class for    
    1. AccountFeatureAfterUpdate , AccountFeatureAfterInsert,  AP_AAF_FeatureUpdateMethods
 

 */
@isTest
private class PRM_StandAloneFeature_TEST {
    static testMethod void myUnitTest() {
        
        Account acc;
        
        FeatureCatalog__c feature;
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        
        User u = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        u.ProgramAdministrator__c = true;
        
        system.runAs(u)
        {
            CS_AccountFeaturetoContactFeature__c cs = new CS_AccountFeaturetoContactFeature__c();
            cs.name = 'AssignmentComments__c';
            cs.ContactAssignedFeatureField__c = 'AssignmentComments__c';
            insert cs;
            
            CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
            newRecTypeMap.name = recCatalogActivityReq.Id;
            newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
            newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;
            insert newRecTypeMap;
                       
            feature  = Utils_TestMethods.createFeatureCatalog();
            feature.Enabled__c = 'Contact';
            feature.Visibility__c = 'Contact';
            feature.Description__c = 'Test';
            insert feature;
            
            RequirementCatalog__c req = Utils_TestMethods.createRequirementCatalog();
            req.ApplicableTo__c = 'Contact';
            req.recordtypeid = recCatalogActivityReq.Id;
            insert req;
            
            FeatureRequirement__c  newFeatureReq = Utils_TestMethods.createFeatureRequirement(feature.ID ,req.Id);
            insert newFeatureReq;
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            
            acc = Utils_TestMethods.createAccount();
            acc.Country__c=country.id;
            insert acc;
            acc.IsPartner  = true;
            update acc; 
            
            Contact contact1 = Utils_TestMethods.createContact(acc.Id, 'Test');
            contact1.PRMContact__c = true;
            insert contact1;
            
            Id standaloneFeatureRecordType = [Select id from RecordType where developername='StandAloneFeature'and SobjectType ='AccountAssignedFeature__c' limit 1].Id;            
            AccountAssignedFeature__c newStandAloneFeature = Utils_TestMethods.createAccountFeature(feature.ID , acc.Id);
            newStandAloneFeature.recordtypeid = standaloneFeatureRecordType;
            insert newStandAloneFeature;
            
            AccountAssignedFeature__c newStandAloneFeature2 = Utils_TestMethods.createAccountFeature(feature.ID , acc.Id);
            newStandAloneFeature2.recordtypeid = standaloneFeatureRecordType;
            Database.insert( newStandAloneFeature2, false);
            
            newStandAloneFeature.AssignmentComments__c = 'Test comments';
            update newStandAloneFeature;
        }
        
        
    }
}