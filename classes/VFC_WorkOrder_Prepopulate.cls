/*Created by: Deepak Kumar
Date:31/10/2012
For the purpose of Pre Populating the Customer Location Time Zone and Account Name for December 2012 release

Date Modified       Modified By         Comments
---------------------------------------------------------------------------------------------------------------------
29/07/2013      Jyotiranjan Singhlal    Pre Populating the Preferred FSE for October 2013 release while WO creation
*/

public class VFC_WorkOrder_Prepopulate
{
PageReference NewEditPage = new PageReference('/'+SObjectType.SVMXC__Service_Order__c.getKeyPrefix()+'/e' );
String Locationid;
String Locationname;
String UserBusinessunit;
String UserBusinessunitId;


 /*=======================================
      CONSTRUCTOR
    =======================================*/  


public VFC_WorkOrder_Prepopulate(ApexPages.StandardController controller)
{
}

/* This method will take the controle to editpage i.e and will pre populate the customer
 location, customer time zone and related Account*/

public Pagereference gotoeditpage()
{
    String RecordTypeId = System.currentpagereference().getParameters().get('RecordType');
    LocationName = System.currentpagereference().getParameters().get(Label.CLMAY13SRV09); 
    Locationid = System.currentpagereference().getParameters().get(Label.CLMAY13SRV10);
    String AccountId = System.currentpagereference().getParameters().get(Label.CLDEC12SRV04);
    String AccountName = System.currentpagereference().getParameters().get(Label.CLDEC12SRV03);
    String caseNumber = System.currentpagereference().getParameters().get(Label.CLDEC12SRV42);
    String caseId = System.currentpagereference().getParameters().get(Label.CLDEC12SRV43);
    String parentWorkOrderID = System.currentpagereference().getParameters().get(Label.CLDEC12SRV47);
    String parentWorkOrderNumber = System.currentpagereference().getParameters().get(Label.CLDEC12SRV46);
    String componentID = System.currentpagereference().getParameters().get(Label.CLMAY13SRV04);
    String componentName = System.currentpagereference().getParameters().get(Label.CLMAY13SRV03);
    String sContractID = System.currentpagereference().getParameters().get(Label.CLAPR15SRV48);
    String sContractName = System.currentpagereference().getParameters().get(Label.CLAPR15SRV49);
    String sWorkOrderGroupID = System.currentpagereference().getParameters().get(Label.CLOCT13SRV03);
    String sWorkOrderGroup = System.currentpagereference().getParameters().get(Label.CLOCT13SRV04);
    String PreferredFSEName = System.currentpagereference().getParameters().get(Label.CLOCT13SRV19);
    String PreferredFSEId = System.currentpagereference().getParameters().get(Label.CLOCT13SRV20);
    String sWorkOrderPMPlanID = System.currentpagereference().getParameters().get(Label.CLOCT13SRV28);
    String sWorkOrderPMPlan = System.currentpagereference().getParameters().get(Label.CLOCT13SRV29);
    
    NewEditPage.getParameters().put('RecordType',RecordTypeId);
    
    User u=[select id, name,UserBusinessUnit__c,Country__c from user where id=:UserInfo.getUserId()];
        
    if(Locationid !=null && Locationid !='' )
    {
        SVMXC__Site__c custl = new SVMXC__Site__c();
        custl = [Select SVMXC__Account__c, SVMXC__Account__r.Name,TimeZone__c,PreferredFSE__c,PreferredFSE__r.Name from SVMXC__Site__c 
        where id =: Locationid limit 1];
        
        List<SVMXC__Site__c> loc1 = new List<SVMXC__Site__c>();
        loc1 = [Select Id,PreferredFSE__c,PreferredFSE__r.Name from SVMXC__Site__c where SVMXC__Account__c =: custl.SVMXC__Account__c  AND PrimaryLocation__c =: True limit 1];
               
        if(custl.SVMXC__Account__c!= null)
        {
            NewEditPage.getParameters().put(Label.CLMAY13SRV09, LocationName );
            NewEditPage.getParameters().put(Label.CLMAY13SRV10, Locationid );
            NewEditPage.getParameters().put(Label.CLDEC12SRV03, custl.SVMXC__Account__r.Name);
            NewEditPage.getParameters().put(Label.CLDEC12SRV04, custl.SVMXC__Account__c);
            NewEditPage.getParameters().put(Label.CLDEC12SRV05, custl.TimeZone__c );
            NewEditPage.getParameters().put(Label.CLDEC12SRV51, custl.SVMXC__Account__r.Name);
        }
        else 
        {
            System.Debug('VPF_WorkOrder_PrePopulate.gotoeditpage.WARNING - LocationId is null.');
        }
        if(custl.PreferredFSE__c != null)
        {
            NewEditPage.getParameters().put(Label.CLOCT13SRV19, custl.PreferredFSE__r.Name);
            NewEditPage.getParameters().put(Label.CLOCT13SRV20, custl.PreferredFSE__c);
        }
        else if(loc1 != null & loc1.size()>0)
        {
            NewEditPage.getParameters().put(Label.CLOCT13SRV19, loc1[0].PreferredFSE__r.Name);
            NewEditPage.getParameters().put(Label.CLOCT13SRV20, loc1[0].PreferredFSE__c);
        }
     }
     else if(AccountId != null && AccountId.trim()!= '')
     {
        NewEditPage.getParameters().put(Label.CLDEC12SRV03, AccountName );
        NewEditPage.getParameters().put(Label.CLDEC12SRV04, AccountId );
     }
     else if(caseID != null && caseId.trim()!= '')
     {
         /*=======================================
           Added By Deepak for April 2015 Release
          =======================================*/
        
        Case case1 = new Case();
        case1 = [Select AccountId,Account.Name,SVMXC__Service_Contract__c,SVMXC__Service_Contract__r.Name,
                SVMXC__Component__c,SVMXC__Component__r.Name,SVMXC__SLA_Terms__c,SVMXC__SLA_Terms__r.Name,ServiceLine__c,
                SerialNumber__c,ServiceLine__r.Name From Case where id =: caseID];
                
                system.debug('case1:'+case1);
                
        if(case1.AccountId != null){
            List<SVMXC__Site__c> site = new   List<SVMXC__Site__c> ();
            site=[select id,Name,SVMXC__Account__c from SVMXC__Site__c where SVMXC__Account__c =:case1.AccountId and PrimaryLocation__c = true limit 1];
            if(site != null  && site.size()>0 )
            {
                 NewEditPage.getParameters().put(Label.CLMAY13SRV09, site[0].Name );
                 NewEditPage.getParameters().put(Label.CLMAY13SRV10, site[0].Id );
            }
            else
            {
            //Create a Primary Location for the Installed Account
                Set<id> accidSet = new Set<id>();
                if(case1.AccountId != null){
                    accidSet.add(case1.AccountId);
                    List<SVMXC__Site__c> siteList = Ap_Location.createLocation(accidSet);
                    if(siteList != null && siteList.size()>0)
                    {
                      insert siteList;
                      
                      NewEditPage.getParameters().put(Label.CLMAY13SRV09, siteList[0].Name );
                      NewEditPage.getParameters().put(Label.CLMAY13SRV10, siteList[0].Id );
                    }
                }
            }
        }
        NewEditPage.getParameters().put(Label.CLDEC12SRV42, caseNumber);
        NewEditPage.getParameters().put(Label.CLDEC12SRV43, caseId);
        NewEditPage.getParameters().put(Label.CLAPR15SRV13, case1.Account.Name);
        NewEditPage.getParameters().put(Label.CLAPR15SRV14, case1.AccountId);
        // for SLA Term
        NewEditPage.getParameters().put(Label.CLAPR15SRV15, case1.SVMXC__SLA_Terms__r.Name);
        NewEditPage.getParameters().put(Label.CLAPR15SRV16, case1.SVMXC__SLA_Terms__c);
        //for Service Contract
        NewEditPage.getParameters().put(Label.CLAPR15SRV21, case1.SVMXC__Service_Contract__r.Name);
        NewEditPage.getParameters().put(Label.CLAPR15SRV22, case1.SVMXC__Service_Contract__c);
        //for Installed Product
        NewEditPage.getParameters().put(Label.CLAPR15SRV19, case1.SVMXC__Component__r.Name);
        NewEditPage.getParameters().put(Label.CLAPR15SRV20, case1.SVMXC__Component__c);
        // for Service Line
        NewEditPage.getParameters().put(Label.CLAPR15SRV17, case1.ServiceLine__r.Name);
        NewEditPage.getParameters().put(Label.CLAPR15SRV18, case1.ServiceLine__c);
        // For Serial Number
       // NewEditPage.getParameters().put(Label.CLAPR15SRV21, case1.ServiceLine__r.Name);
        NewEditPage.getParameters().put(Label.CLAPR15SRV23, case1.SerialNumber__c);
        system.debug('11111:'+NewEditPage.getParameters().put(Label.CLAPR15SRV23, case1.SerialNumber__c));
         system.debug('22222:'+Label.CLAPR15SRV23);
         system.debug('22222:'+case1.SerialNumber__c);
        
     }
     else if(parentWorkOrderID != null && parentWorkOrderID.trim()!= '')
     {
        NewEditPage.getParameters().put(Label.CLDEC12SRV47, parentWorkOrderID);
        NewEditPage.getParameters().put(Label.CLDEC12SRV46, parentWorkOrderNumber);
     }
     //Start: May2013 Release BR#2488
     else if(componentID != null && componentID.trim()!= '')
     {
        NewEditPage.getParameters().put(Label.CLMAY13SRV04, componentID);
        NewEditPage.getParameters().put(Label.CLMAY13SRV03, componentName);
   
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
        ip = [Select SVMXC__Company__c, SVMXC__Company__r.Name,SVMXC__Site__c,
        SVMXC__Site__r.Name from SVMXC__Installed_Product__c where id =: componentID limit 1];
        
        if(ip.SVMXC__Company__c!= null)
         {
            NewEditPage.getParameters().put(Label.CLDEC12SRV03, ip.SVMXC__Company__r.Name);
            NewEditPage.getParameters().put(Label.CLDEC12SRV04, ip.SVMXC__Company__c);
         }
         if(ip.SVMXC__Site__c!= null)
         {
            NewEditPage.getParameters().put(Label.CLMAY13SRV09, ip.SVMXC__Site__r.Name );
            NewEditPage.getParameters().put(Label.CLMAY13SRV10, ip.SVMXC__Site__c );
         }
     }
     else if(sContractID != null && sContractID.trim()!= '')
     {
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c();
        sc = [Select Id,ParentContract__c,ParentContract__r.Name from SVMXC__Service_Contract__c where Id =: sContractID limit 1];
        NewEditPage.getParameters().put(Label.CLMAY13SRV06, sContractID);
        NewEditPage.getParameters().put(Label.CLMAY13SRV05, sContractName);
        //NewEditPage.getParameters().put(Label.CLMAY13SRV06, sc.ParentContract__c);
        //NewEditPage.getParameters().put(Label.CLMAY13SRV05, sc.ParentContract__r.Name);
     }
     else if(sWorkOrderGroupID != null && sWorkOrderGroupID.trim()!= '')
     {
        NewEditPage.getParameters().put(Label.CLOCT13SRV03, sWorkOrderGroupID);
        NewEditPage.getParameters().put(Label.CLOCT13SRV04, sWorkOrderGroup);
        
        
        WorkOrderGroup__c wog = new WorkOrderGroup__c();
        wog = [Select SoldTo__c, SoldTo__r.Name,Account__c,
        Account__r.Name from WorkOrderGroup__c where id =: sWorkOrderGroupID limit 1];
        
        if(wog.SoldTo__c!= null)
        {
            NewEditPage.getParameters().put(Label.CLOCT13SRV43, wog.SoldTo__c);
            NewEditPage.getParameters().put(Label.CLOCT13SRV44, wog.SoldTo__r.Name);
        }
        if(wog.Account__c!= null)
        {
            NewEditPage.getParameters().put(Label.CLOCT13SRV45, wog.Account__c);
            NewEditPage.getParameters().put(Label.CLOCT13SRV46, wog.Account__r.Name);
        }
     }
     else if(sWorkOrderPMPlanID != null && sWorkOrderPMPlanID.trim()!= '')
     {
        NewEditPage.getParameters().put(Label.CLOCT13SRV28, sWorkOrderPMPlanID);
        NewEditPage.getParameters().put(Label.CLOCT13SRV29, sWorkOrderPMPlan);
     }
     //Start: May2013 Release BR#2488
     if(u.UserBusinessUnit__c!= null)
     {
        //if(u.UserBusinessUnit__c=='BD' || u.UserBusinessUnit__c=='EN' || u.UserBusinessUnit__c=='ID'
        //|| u.UserBusinessUnit__c=='IT'|| u.UserBusinessUnit__c=='PS'
        //|| u.UserBusinessUnit__c=='PW' || u.UserBusinessUnit__c=='SI')
        //{   
            NewEditPage.getParameters().put(System.Label.CLMAY13SRV15,u.UserBusinessUnit__c);
       // }
     }//End: May2013 Release BR#2488
     if(u.Country__c!= null)
     {
        if(u.Country__c=='IN' || u.Country__c=='CA' /*|| u.Country__c=='ES'*/)
        {
            String BOSystem = u.Country__c + System.Label.CLMAY13SRV23;
            NewEditPage.getParameters().put(System.Label.CLMAY13SRV21,u.Country__c);
            NewEditPage.getParameters().put(System.Label.CLMAY13SRV22,BOSystem);
        }
        //Added for BR-7874 Oct 2015 Release
        else if(u.Country__c=='ES')
        {
            String BOSystem = System.Label.CLDEC12SRV60;
            NewEditPage.getParameters().put(System.Label.CLMAY13SRV21,u.Country__c);
            NewEditPage.getParameters().put(System.Label.CLMAY13SRV22,BOSystem);
        }
        //End:Added for BR-7874 Oct 2015 Release
     }//End: May2013 Release
     
     NewEditPage.getParameters().put('nooverride', '1'); 
     return NewEditPage;
    }
}