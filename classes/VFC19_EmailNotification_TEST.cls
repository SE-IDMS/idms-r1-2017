/*
    Author          : Karthik Shivprakash (ACN) 
    Date Created    : 11/03/2011
    Description     : Test class for VFC19_EmailNotification.
*/


@isTest

private class VFC19_EmailNotification_TEST {

  static testMethod void EmailNotificationTestMethod(){
    
    Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact');
        insert contact1;
    
    Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
    insert case1;
    
    Case case2 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
    insert case2;
    
    EmailMessage emailRecord = Utils_TestMethods.createEmailMessage(case1.Id);
    insert emailRecord;
    
    Case caseRecord = [select id,TECH_NumberOfUnreadEmails__c from case where id=:case1.Id];
    caseRecord.TECH_NumberOfUnreadEmails__c=1;
    update caseRecord;
    System.debug('Case Record'+caseRecord);
    
    ApexPages.StandardController sc1 = new ApexPages.StandardController(caseRecord);
    
    System.debug('case2'+case2);
    ApexPages.StandardController sc2 = new ApexPages.StandardController(case2);
     
    Test.startTest();
    
    VFC19_EmailNotification unReadEmailObj1 = new VFC19_EmailNotification(sc1);
    
    VFC19_EmailNotification unReadEmailObj2 = new VFC19_EmailNotification(sc2);
    
    unReadEmailObj1.UnReadMails();
    
    Test.stopTest();
  }
}