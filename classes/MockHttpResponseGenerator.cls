/*
Author: Sid (bFO Solution) & Polyspot team
description: https://www.salesforce.com/us/developer/docs/apexcode/Content/apex_classes_restful_http_testing_httpcalloutmock.htm
*/

@isTest

global class MockHttpResponseGenerator implements HttpCalloutMock {
    /*
    Author: Sid & Polyspot team.
    Function : testGetContent
    Parameters : req - HTTPRequest , comma seperated list of parameters to be sent to the polyspot server.
    Description : The below method provides a way to mock the request to be sent to the polyspot engine and return the json response back.
    */
    global HTTPResponse respond(HTTPRequest req) {

        //System.assertEquals(System.Label.PolyspotEndPoint, req.getEndpoint());
        System.assertEquals('POST', req.getMethod());

        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"polyspot_request":"polyspot_response"}');
        res.setStatusCode(200);
        return res;
    }
}