public class VFC83_newLiteratureRequest extends VFC_ControllerBase
{
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/
    Public String LiteratureID;
    public LiteratureRequest__c newLiteratureRequest{get;set;} // Current Literature Request
    public User relatedUser{get;set;} // Current User
    public Case relatedCase{get;set;} // Master Case
    public newLiteratureRequestGMRSearch LiteratureRequestGMR{get;set;} // Methods for the GMR Search
    public Account relatedAccount{get;set;} // Account related to the case
    public Contact relatedContact{get;set;} // Contact related to the case    
    public Boolean defineLegacyID{get;set;} // Boolean related to the display of the Legacy ID field
    public Boolean defineCommercialReference{get;set;} // Boolean related to the display of the PM0 Search to define the Commercial Reference
    
    public String orderSystem{get;set;} // Name of the selected Order System
    public List<SelectOption> orderSystemList{get;set;} // Picklist values for the Order Systems related to an account
    public String legacyNumber{get;set;} // Legacy ID of the related account in the selected Order System
    public List<SelectOption> legacyNumberList{get;set;} // List of available Legacy IDs
    public Map<String,List<SelectOption>> legacyNumberDependencies{get;set;} // Mapping of Legacy Name and available legacy IDs
    
    public Boolean enableOrderSystems{get;set;} // enable the Order System list when the account is selected
    public Boolean enableLegacyNumbers{get;set;} // enable the legacy number list when an order system is selected
    
    /*=======================================
      INNER CLASSES
    =======================================*/
    
   // Implementation of the Utils_GMRSearchMethods methods for the Literature Request
    public class newLiteratureRequestGMRSearch implements Utils_GMRSearchMethods
    {
       public Void Init(String ObjID, VCC08_GMRSearch Controller)
       {
            // Retrieve the new Literature Request from the related Page controller

            VFC83_newLiteratureRequest LiteratureRequestController = (VFC83_newLiteratureRequest)Controller.pageController; 
            
            // Pre-populate Search Keyword and Commercial Reference                                                    
            Controller.searchKeyword = LiteratureRequestController.newLiteratureRequest.CommercialReference__c;
            Controller.RelatedObject = (LiteratureRequest__c)LiteratureRequestController.newLiteratureRequest;
       }
       
       public Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
       {
           VCC08_GMRSearch GMRController = (VCC08_GMRSearch)ControllerBase;
           VFC83_newLiteratureRequest LiteratureRequestController = (VFC83_newLiteratureRequest)GMRController.PageController;

           return LiteratureRequestController.saveCommercialReference(obj);
       }
       
       public PageReference Cancel( VCC08_GMRSearch Controller)
       { 
            String ExtLiteratureID = system.currentpagereference().getparameters().get('LtrId');
            if(ExtLiteratureID != null && ExtLiteratureID != ''){
            PageReference LitPage = new Pagereference('/'+ ExtLiteratureID);
            return LitPage;
            }
            else{           
            VFC83_newLiteratureRequest LiteratureRequestController = (VFC83_newLiteratureRequest)Controller.PageController;
            PageReference CasePage = new ApexPages.StandardController((Case)LiteratureRequestController.relatedCase).view();
            return CasePage;
            }
       } 
    }   
 
    /*=======================================
      CONSTRUCTOR
    =======================================*/ 

    public  VFC83_newLiteratureRequest(ApexPages.StandardController controller)
    {
        
        LiteratureID = system.currentpagereference().getparameters().get('LtrId');
        
        System.debug('LiteratureID from Con:' + LiteratureID); 
        
        newLiteratureRequest = (LiteratureRequest__c)controller.getRecord();
        system.debug('newLiteratureRequest-->'+newLiteratureRequest);
        LiteratureRequestGMR = new newLiteratureRequestGMRSearch();
        relatedCase = New Case();
              
        // Display the edit page for the Legacy ID
        defineLegacyID = false;
        defineCommercialReference = true;
        
        // Pre-populate fields of the edit page
        if(LiteratureID != null && LiteratureID !=''){
            List<LiteratureRequest__c> lstLiteratureRequest = new List<LiteratureRequest__c>([Select id,CommercialReference__c  from LiteratureRequest__c where Id =: LiteratureID ]);
            if(lstLiteratureRequest.size()>0){
            newLiteratureRequest = lstLiteratureRequest[0];
            }
        }
        else{
        initializeLiteratureRequest(newLiteratureRequest );
        }
    }     

    /*=======================================
      METHODS
    =======================================*/ 
    
    public void initializeLiteratureRequest(LiteratureRequest__c aLiteratureRequest)
    {

        // Fetch the ID of the related case from the URL 
        String caseID = system.currentpagereference().getParameters().get(Label.CL00330);
            
        if (caseID != null)
        {
            caseID = caseID.substring(1); 
        } 

        // Retrieve related case
        List<Case> caseList = [SELECT ID, quantity__c,CaseNumber,ACC_ClassLevel1__c, AccountID, ContactID, CommercialReference__c  from Case WHERE ID=:caseID]; 
        
        // If ID not fetched through the URL, use ID of the newly created record
        if(caseList.size() == 0)
        {
            caseList = [SELECT ID, CaseNumber, quantity__c,AccountID, ContactID, CommercialReference__c,ACC_ClassLevel1__c from Case WHERE ID=:newLiteratureRequest .Case__c];    
        }         
        
        // Populate the Customer address with the address of the related Account
        if(caseList.size() == 1)
        {
            relatedCase = caseList[0];

            newLiteratureRequest.CommercialReference__c = relatedCase.CommercialReference__c;
            newLiteratureRequest.Classificationlevel1__c = relatedCase.ACC_ClassLevel1__c;
            newLiteratureRequest.Quantity__c = relatedCase.Quantity__c;

            List<Contact> ContactList = [SELECT ID, Name, Street__c, ZipCode__c, POBox__c,County__c,Country__c,Country__r.Name, City__c, StateProv__c,StateProv__r.Name, POBoxZip__c FROM Contact WHERE ID=:relatedCase.ContactID];
            
            if(ContactList.size() == 1)
            {
                newLiteratureRequest.Contact__c = ContactList[0].ID;             
                newLiteratureRequest.ShippingStreet__c = ContactList[0].Street__c; 
                newLiteratureRequest.ShippingCity__c = ContactList[0].City__c;
                newLiteratureRequest.ShippingPOBoxZip__c = ContactList[0].POBoxZip__c; 
                newLiteratureRequest.ShippingZipcode__c = ContactList[0].ZipCode__c;
                newLiteratureRequest.ShippingPOBox__c = ContactList[0].POBox__c; 
                newLiteratureRequest.ShippingCounty__c = ContactList[0].County__c; 
                newLiteratureRequest.ShippingStateProvince__c = ContactList[0].StateProv__r.Name; 
                newLiteratureRequest.ShippingCountry__c = ContactList[0].Country__r.Name;
                relatedContact = ContactList[0];
            }
        
            List<Account> accountList = [SELECT ID, Name, Street__c, City__c, POBox__c, ZipCode__c, StateProvince__c, 
                                                    County__c, Country__c,  POBoxZip__c  
                                                    FROM Account WHERE ID=:relatedCase.AccountID];

            System.debug('#### Acount List: ' + accountList );

            if(accountList.size()==1)
            {
                System.debug('#### Acount List Size: ' + accountList.size());
                System.debug('#### AccountList[0]: ' + AccountList[0]);
                
                if(AccountList[0].Name != null)
                {
                    newLiteratureRequest.Account__c = AccountList[0].ID;
                    relatedAccount = AccountList[0];
                }
                             
               // newLiteratureRequest.AccountAddress__c = '';             
                newLiteratureRequest.AccountStreet__c = accountList[0].Street__c;
                
                newLiteratureRequest.AccountCity__c = accountList[0].City__c;
                newLiteratureRequest.AccountZipCode__c = AccountList[0].ZipCode__c;
                
              // System.debug('#### newLiteratureRequest.AccountAddress__c: ' + newLiteratureRequest.AccountAddress__c);
                
                if(accountList[0].StateProvince__c != null)
                {
                    List<StateProvince__c> stateProvinceList = [SELECT Id, Name FROM StateProvince__c WHERE ID=:accountList[0].StateProvince__c];
                    if(stateProvinceList.size() == 1)
                    {
                        newLiteratureRequest.AccountStateProvince__c = stateProvinceList[0].Name;
                    }
                }
                
                if(accountList[0].County__c != null)
                {
                    newLiteratureRequest.AccountCounty__c = '\n' + accountList[0].County__c;
                }

                if(accountList[0].Country__c != null)
                {                
                    List<Country__c> countryList = [SELECT Id, Name FROM Country__c WHERE ID=:AccountList[0].Country__c];
                    if(countryList.size() == 1)
                    {
                        newLiteratureRequest.AccountCountry__c = countryList[0].Name;       
                    }
                }
                
                newLiteratureRequest.AccountPOBox__c = accountList[0].POBox__c;
                newLiteratureRequest.AccountPOBoxZip__c = accountList[0].POBoxZip__c;                     
            }
        }              
    }
 
    public Pagereference returnEditPage()
    {
        // Create a new edit page for the Literature Request
        Pagereference LiteratureRequestEditPage;
        
        if(newLiteratureRequest.CommercialReference__c != null)
        {
            LiteratureRequestEditPage = new PageReference('/'+SObjectType.LiteratureRequest__c.getKeyPrefix()+'/e' );
        
            // Set the return to the parent Case (retURL)
            LiteratureRequestEditPage.getParameters().put(Label.CL00330, newLiteratureRequest.Case__c);   
            
            // Disable override
            LiteratureRequestEditPage.getParameters().put(Label.CL00690, Label.CL00691); 
            
            // Pre-default the parent case with the ID (_lkid) and the CaseNumber  
            LiteratureRequestEditPage.getParameters().put(Label.CL00725, newLiteratureRequest.Case__c);
            LiteratureRequestEditPage.getParameters().put(Label.CL00726, relatedCase.CaseNumber); 
            
            // Adding Quantity from the Case
            if(relatedCase.Quantity__c != null)
            LiteratureRequestEditPage.getParameters().put(Label.CLDEC12CCC11, relatedCase.Quantity__c.format());     
            
            // Pre-default the Account 
            if(newLiteratureRequest.Account__c != null)
            {
                LiteratureRequestEditPage.getParameters().put(Label.CL00727, RelatedAccount.Name);
                LiteratureRequestEditPage.getParameters().put(Label.CL00728, newLiteratureRequest.Account__c);            
            }
    
            // Pre-default the Contact Name 
            if(newLiteratureRequest.Contact__c != null)
            {
                LiteratureRequestEditPage.getParameters().put(Label.CL00729, RelatedContact.Name);
                LiteratureRequestEditPage.getParameters().put(Label.CL00730, newLiteratureRequest.Contact__c);            
            }           
                   
         
        // Pre-default the Shipping address          
        if(newLiteratureRequest.ShippingStreet__c != null && newLiteratureRequest.ShippingStreet__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00757, newLiteratureRequest.ShippingStreet__c);
        }
        
        if(newLiteratureRequest.ShippingCity__c != null && newLiteratureRequest.ShippingCity__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00758, newLiteratureRequest.ShippingCity__c);
        }
        
        if(newLiteratureRequest.ShippingPOBox__c != null && newLiteratureRequest.ShippingPOBox__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00806, newLiteratureRequest.ShippingPOBox__c);
        }
        
        if(newLiteratureRequest.ShippingZipcode__c != null && newLiteratureRequest.ShippingZipcode__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00805, newLiteratureRequest.ShippingZipcode__c);
        }   
        
        if(newLiteratureRequest.ShippingCounty__c != null && newLiteratureRequest.ShippingCounty__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00807, newLiteratureRequest.ShippingCounty__c);
        } 
        
        if(newLiteratureRequest.ShippingPOBoxZip__c != null && newLiteratureRequest.ShippingPOBoxZip__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00759, newLiteratureRequest.ShippingPOBoxZip__c);
        }
        
        if(newLiteratureRequest.ShippingStateProvince__c != null && newLiteratureRequest.ShippingStateProvince__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00760, newLiteratureRequest.ShippingStateProvince__c);
        }
        
        if(newLiteratureRequest.ShippingCountry__c != null && newLiteratureRequest.ShippingCountry__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00761, newLiteratureRequest.ShippingCountry__c);
        }
        
        // Pre-default the CustomerAddress
        if(newLiteratureRequest.AccountStreet__c != null && newLiteratureRequest.AccountStreet__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00762, newLiteratureRequest.AccountStreet__c);
        }
        
        if(newLiteratureRequest.AccountCity__c != null && newLiteratureRequest.AccountCity__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00763, newLiteratureRequest.AccountCity__c);
        }
        
        if(newLiteratureRequest.AccountZipCode__c != null && newLiteratureRequest.AccountZipCode__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00764, newLiteratureRequest.AccountZipCode__c);
        }
        
        if(newLiteratureRequest.AccountStateProvince__c != null && newLiteratureRequest.AccountStateProvince__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00765, newLiteratureRequest.AccountStateProvince__c);
        }
        
        if(newLiteratureRequest.AccountCounty__c != null && newLiteratureRequest.AccountCounty__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00766, newLiteratureRequest.AccountCounty__c);
        }       
        
        if(newLiteratureRequest.AccountCountry__c != null && newLiteratureRequest.AccountCountry__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00767, newLiteratureRequest.AccountCountry__c);
        }
        
        if(newLiteratureRequest.AccountPOBox__c != null && newLiteratureRequest.AccountPOBox__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00768, newLiteratureRequest.AccountPOBox__c);
        }
        
        if(newLiteratureRequest.AccountPOBoxZip__c != null && newLiteratureRequest.AccountPOBoxZip__c != '')
        {
            LiteratureRequestEditPage.getParameters().put(Label.CL00769, newLiteratureRequest.AccountPOBoxZip__c);
        }
            


            LiteratureRequestEditPage.getParameters().put(Label.CL00733, newLiteratureRequest.CommercialReference__c);
            LiteratureRequestEditPage.getParameters().put(Label.CL00741,newLiteratureRequest.Classificationlevel1__c);
        }
        
        return LiteratureRequestEditPage;
    }
    
    public pagereference checkCommercialReference()
    {
    
      Pagereference PageResult=null;
      
      if(relatedCase.Commercialreference__c != null){
                newLiteratureRequest.CommercialReference__c = relatedCase.Commercialreference__c;
                PageResult = returnEditPage();
            }                     
        
        return PageResult;
    
    }
    
     // Save the commercial reference into the Literature Request
    public pagereference saveCommercialReference(sObject obj)
    {
        Pagereference PageResult;
    
        if(obj != null)
        {
            DataTemplate__c GMRResult = (DataTemplate__c)obj;
            
            if(GMRResult.Field8__c != null)
            {
                System.debug('LiteratureID:' + LiteratureID);   
                if(LiteratureID != null && LiteratureID !=''){                    
                    newLiteratureRequest.CommercialReference__c  = GMRResult.Field8__c;                   
                    Database.update(newLiteratureRequest);
                    PageResult = new Pagereference('/'+ newLiteratureRequest.Id);
                }
                else
                {
                newLiteratureRequest.CommercialReference__c = GMRResult.Field8__c;
                 PageResult = returnEditPage();
                }               
            }
        }
        
        return PageResult;
    }
}