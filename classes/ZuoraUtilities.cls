/********************************************************************
 * ZuoraUtilities
 *
 * Author: Kevin Fabregue - Zuora Europe
 ********************************************************************/

public with sharing class ZuoraUtilities {


    public static Zuora.zApi zuoraApiAccess() { 
    Zuora.zApi zuoraApi = new Zuora.zApi();
        if (!Test.isRunningTest()) {
            zuoraApi.zlogin();
        } else {
            // nothing
        }
        return zuoraApi;
    }


    public static List<Zuora.zObject> queryToZuora(Zuora.zApi zuoraApi, String query) {
        List<Zuora.zObject> queryResultList = new List<Zuora.zObject>();
        if (!Test.isRunningTest()) {
            queryResultList.addAll(zuoraApi.zquery(query));
        } else {
            Zuora.zObject res;
            
            if (query.contains('FROM Subscription')) {
                res = new Zuora.zObject('Subscription');
                res.setValue('OriginalId', '123456789');
                res.setValue('AccountId', '123456789');
                res.setValue('TermEndDate', Date.today().addMonths(8));
                res.setValue('SubscriptionStartDate', Date.today().addMonths(-1));
            } else if (query.contains('FROM InvoiceItem')) {
                res = new Zuora.zObject('InvoiceItem');
                res.setValue('SubscriptionId', '123456');
                res.setValue('InvoiceId', '123456');
                res.setValue('ChargeName', 'Partner Membership - Annual');
                res.setValue('ServiceEndDate', Date.today().addMonths(2));
            } else if (query.contains('TargetDate FROM Invoice')) {
                res = new Zuora.zObject('Invoice');
                res.setValue('Amount', 12);
                res.setValue('Balance', 0);
                res.setValue('TargetDate', Date.today().addMonths(1));
            } else if (query.contains('FROM InvoicePayment')) {
                res = new Zuora.zObject('InvoicePayment');
                res.setValue('Amount', 12);
                res.setValue('PaymentId', '123456');
            } else if (query.contains('FROM Invoice')) {
                res = new Zuora.zObject('Invoice');
                res.setValue('Amount', -6);
            } else if (query.contains('FROM Payment')) {
                res = new Zuora.zObject('Payment');
                res.setValue('Amount', 12);
                res.setValue('Type', 'Electronic');
            } else if (query.contains('FROM Contact')) {
                res = new Zuora.zObject('Contact');
                res.setValue('FirstName', 'John');
                res.setValue('LastName', 'Doe');
                res.setValue('WorkEmail', 'jd@test.com');
                res.setValue('WorkPhone', '010203040506');
                res.setValue('Address1', '1st Street');
                res.setValue('Address2', 'Courtyard');
                res.setValue('City', 'New York');
                res.setValue('PostalCode', '10001');
                res.setValue('State', 'New York');
                res.setValue('Country', 'United States');
            }

            res.setValue('Id','123456');

            queryResultList.add(res);
        }
        return queryResultList;
    }


    public static List<Zuora.zApi.SaveResult> createZuoraObjects(Zuora.zApi zuoraApi, List<Zuora.zObject> zuoraObjectsToCreateList) {
        List<Zuora.zApi.SaveResult> zuoraObjectsCreateResults = new List<Zuora.zApi.SaveResult>();
        if (!Test.isRunningTest()) {
            zuoraObjectsCreateResults = zuoraApi.zcreate(zuoraObjectsToCreateList);
        } else {
            Zuora.zApi.SaveResult res = new Zuora.zApi.SaveResult();
            res.Success = true;
            res.Id = '1234';
            zuoraObjectsCreateResults = new List<Zuora.zApi.SaveResult>{res};
        }
        return zuoraObjectsCreateResults;
    }


    public static List<Zuora.zApi.SaveResult> updateZuoraObjects(Zuora.zApi zuoraApi, List<Zuora.zObject> zuoraObjectsToUpdateList) {
        List<Zuora.zApi.SaveResult> zuoraObjectsUpdateResults = new List<Zuora.zApi.SaveResult>();
        if (!Test.isRunningTest()) {
            zuoraObjectsUpdateResults = zuoraApi.zupdate(zuoraObjectsToUpdateList);
        } else {
            Zuora.zApi.SaveResult res = new Zuora.zApi.SaveResult();
            res.Success = true;
            res.Id = '1234';
            zuoraObjectsUpdateResults = new List<Zuora.zApi.SaveResult>{res};
        }
        return zuoraObjectsUpdateResults;
    }


    @InvocableMethod
    public static void updateVatIdInZuora(List<Account> accountList) {
        List<String> sfdcAccountIdList = new List<String>();
        for (Account sfdcAcc : accountList) {
            sfdcAccountIdList.add(sfdcAcc.Id);
        }
        futureUpdateVatIdInZuora(sfdcAccountIdList);
    }

    @future(callout=true)
    public static void futureUpdateVatIdInZuora(List<String> sfdcAccountIdList) {

        List<Account> accountList = [SELECT Id, VATNumber__c FROM Account WHERE Id IN :sfdcAccountIdList];

        Map<String, String> sfdcAccIdToVatNumMap = new Map<String, String>();

        for(Account sfdcAcc : accountList) {
            sfdcAccIdToVatNumMap.put(sfdcAcc.Id, sfdcAcc.VATNumber__c);
        }
        System.debug('###### INFO: sfdcAccIdToVatNumMap = ' + sfdcAccIdToVatNumMap);

        List<Zuora__CustomerAccount__c> zuoraAccountList = [SELECT Zuora__Account__r.Id, Zuora__External_Id__c FROM Zuora__CustomerAccount__c WHERE Zuora__Account__c IN :accountList];

        System.debug('###### INFO: zuoraAccountList = ' + zuoraAccountList);

        List<Zuora.zObject> zuoraAccToUpdateList = new List<Zuora.zObject>();

        for (Zuora__CustomerAccount__c zuoraAcc : zuoraAccountList) {
                Zuora.zObject zuoraAccToUpdate = new Zuora.zObject('Account');
                zuoraAccToUpdate.setValue('Id', zuoraAcc.Zuora__External_Id__c);
                zuoraAccToUpdate.setValue('VATId', sfdcAccIdToVatNumMap.get(zuoraAcc.Zuora__Account__r.Id));
                zuoraAccToUpdateList.add(zuoraAccToUpdate);
        }

        System.debug('###### INFO: zuoraAccToUpdateList = ' + zuoraAccToUpdateList);

        Zuora.zApi zApi = zuoraApiAccess();
        List<Zuora.zApi.SaveResult> saveResultList = updateZuoraObjects(zApi, zuoraAccToUpdateList);

        for (Zuora.zApi.SaveResult saveResult : saveResultList) {
            System.debug('###### INFO: analyzeSaveResult(saveResult) = ' + analyzeSaveResult(saveResult));
        }
    }


    public static List<String> analyzeSaveResult(Zuora.zApi.SaveResult saveResult) {
        List<String> result = new List<String>(); 
        if ( !(saveResult.Success) ) {  
            for (Zuora.zObject error : saveResult.errors) {
                //String errorCode = (String)error.getValue('Code');
                //String message = (String)error.getValue('Message');
                result.add((String)error.getValue('Code'));
                result.add((String)error.getValue('Message'));
                //System.debug(errorCode);
                //System.debug(message);
            }
        }
        return result;
    }


    public static String dateZuoraFormat(Date someDate) {
        return String.valueOf(someDate)+'T00:00:00';
    }
}