@isTest
private class AP_ASM_AddAssessment_TEST{
     static testMethod void myTest(){
        User user1 = Utils_TestMethods.createStandardUserWithNoCountry('ASM');
        user1.ProgramAdministrator__c = true;

        system.runas(user1){
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='ASM';
        insert country;
        Country__c country1 = Utils_TestMethods.createCountry();
        country1.countrycode__c ='IN';
        insert country1;
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        Account acc = new Account();
        ACC_PartnerProgram__c accprg = new ACC_PartnerProgram__c();
        OpportunityRegistrationForm__c orf1 = new OpportunityRegistrationForm__c();
        OpportunityRegistrationForm__c orf2 = new OpportunityRegistrationForm__c();
        OpportunityRegistrationForm__c orf3 = new OpportunityRegistrationForm__c();
        Assessment__c asm = new Assessment__c();
        user1.country__c = country.countrycode__c;

        // creates global program and its program level

        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;

        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;

        SpecializationCatalog__c spct1 = Utils_TestMethods.createSpecializationCatalog();
        insert spct1;
        spct1.active__c = true;
        update spct1;

        Specialization__c sp1 = Utils_TestMethods.createSpecialization(spct1.id);
        insert sp1;
        sp1.status__c = 'active';
        update sp1;
        
        //creates country program and its program level
        
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;

        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;
        prg2.levelstatus__c = 'active';
        update prg2;

        ProgramLevel__c prg3 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg3;
        prg3.levelStatus__c = 'active';
        update prg3;

        Specialization__c sp2 = Utils_TestMethods.createCountrySpecialization(sp1.id, spct1.id);
        insert sp2;
        sp2.Status__c = 'active';
        update sp2;

        //creates a partner account

        acc = Utils_TestMethods.createAccount();
        acc.country__c = country.id;
        insert acc;
        acc.isPartner = true;
        acc.PRMAccount__c = true;
        update acc;

        System.Debug('Account Created'+acc);
        
        Specialization__c sp3 = Utils_TestMethods.createCountrySpecialization(sp1.id, spct1.id);
        insert sp3;
        sp3.Status__c = 'active';
        sp3.Country__c = country.id;
        update sp3;
        
        AccountSpecialization__c accspc = Utils_TestMethods.createAccountSpecialization(acc.id, sp3.id, spct1.id);
        insert accspc;
        accspc.Status__c = 'active';
        update accspc;
        
        System.Debug('The Account Specialization is :'+accspc);
        
        //creates an account program for the country program
                
        System.Debug('The country program country is:'+cp1.country__c);
        System.Debug('The id for country program is:'+cp1.id);
        System.Debug('The recordtypeid for cp1 is:'+cp1.recordtypeid);
        System.Debug('The program status for cp1 is:'+cp1.programStatus__c);
        system.debug('cp1:'+cp1);
        
        acc =[select id, country__c from account where id =:acc.id limit 1];
        system.debug('acc:'+Acc);
        test.startTest();
        accprg = Utils_TestMethods.createAccountProgram(prg2.id, cp1.id, acc.id);
        insert accprg;
        
        //creates an assessment
        
        asm = Utils_TestMethods.createAssessment();
        asm.Name = 'Test Assessment';
       asm.AutomaticAssignment__c = true;
        asm.Name = 'Sample Assessment';
        asm.PartnerProgram__c = cp1.id;
        asm.ProgramLevel__c = prg2.id;
        asm.specialization__c = sp2.id;
        
        insert asm;
        
        OpportunityAssessmentQuestion__c opassq = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'picklist', 4);
        insert opassq;
        
        System.Debug('Assessment question is:'+opassq);
     
        OpportunityAssessmentAnswer__c opassa = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq.id);
        insert opassa;
        
        System.Debug('Assessment Answer is:'+opassa);
        
        System.Debug('The updated assessment is:'+asm);
        
        orf1 = Utils_TestMethods.createORF(country.id);
        orf1.producttype__c = 'Test';
        orf1.PartnerProgram__c = cp1.id;
        orf1.ProgramLevel__c = prg2.id;
        insert orf1;
        
        asm.AutomaticAssignment__c = false;
        update asm;
     //  asm.AutomaticAssignment__c = true;
     //   update asm;
        
        System.Debug('The PartnerProgram and ProgramLevel for orf1 are:'+orf1.PartnerProgram__c+' '+orf1.ProgramLevel__c);
        System.Debug('The Opportunity Registration form:'+orf1);
        
        asm.ProgramLevel__c = prg3.id;
        update asm;
       
        OpportunityAssessmentQuestion__c opassq1 = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'picklist', 4);
        insert opassq1;
        System.Debug('Assessment question is:'+opassq1);
        
        OpportunityAssessmentAnswer__c opassa1 = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq1.id);
        insert opassa1;
        
        System.Debug('Assessment Answer is:'+opassa1);
        
        System.Debug('The updated assessment is:'+asm);
        
        orf2 = Utils_TestMethods.createORF(country.id);
        orf2.producttype__c = 'Test';
        orf2.PartnerProgram__c = cp1.id;
        orf2.ProgramLevel__c = prg3.id;
        insert orf2;
      //  test.startTest();
        asm.AutomaticAssignment__c = false;
        update asm;
      //  asm.AutomaticAssignment__c = true;
       // update asm;
        
        System.Debug('The PartnerProgram and ProgramLevel for orf2 are:'+orf2.PartnerProgram__c+' '+orf2.ProgramLevel__c);
        System.Debug('The Opportunity Registration form:'+orf2);
        
        asm.Specialization__c = sp3.id;
        update asm;
      //  test.stopTest();
        OpportunityAssessmentQuestion__c opassq2 = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'picklist', 4);
        insert opassq2;
        System.Debug('Assessment question is:'+opassq2);
        
        OpportunityAssessmentAnswer__c opassa2 = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq2.id);
        insert opassa2;
        
        System.Debug('Assessment Answer is:'+opassa2);
        
        System.Debug('The updated assessment is:'+asm);
        
        orf3 = Utils_TestMethods.createORF(country.id);
        orf3.producttype__c = 'Test';
        orf3.PartnerProgram__c = cp1.id;
        orf3.ProgramLevel__c = prg3.id;
        insert orf3;
        
        asm.AutomaticAssignment__c = false;
        update asm;
       // asm.AutomaticAssignment__c = true;
       // update asm;
        test.stopTest();
        System.Debug('The PartnerProgram and ProgramLevel for orf3 are:'+orf3.PartnerProgram__c+' '+orf3.ProgramLevel__c);
        System.Debug('The Opportunity Registration form:'+orf3);
              
        }
     
    }
   
   
    static testMethod void myUnitTest(){
        User user1 = Utils_TestMethods.createStandardUserWithNoCountry('ASM');
        user1.ProgramAdministrator__c = true;
        system.runas(user1){
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='ASM';
        insert country;
        Country__c country1 = Utils_TestMethods.createCountry();
        country1.countrycode__c ='AI';
        insert country1;
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        Account acc = new Account();
        Account acc1 = new Account();
        ACC_PartnerProgram__c accprg = new ACC_PartnerProgram__c();
        ACC_PartnerProgram__c accprg1 = new ACC_PartnerProgram__c();
        OpportunityRegistrationForm__c orf1 = new OpportunityRegistrationForm__c();
        OpportunityRegistrationForm__c orf2 = new OpportunityRegistrationForm__c();
        OpportunityRegistrationForm__c orf3 = new OpportunityRegistrationForm__c();
        Assessment__c asm = new Assessment__c();
        user1.country__c = country.countrycode__c;
        
        // creates global program and its program level
        
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;
       
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;
        
        SpecializationCatalog__c spct1 = Utils_TestMethods.createSpecializationCatalog();
        insert spct1;
        spct1.active__c = true;
        update spct1;
        
        Specialization__c sp1 = Utils_TestMethods.createSpecialization(spct1.id);
        insert sp1;
        sp1.status__c = 'active';
        update sp1;
        
        //creates country program and its program level
        
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;
        
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;
        prg2.levelstatus__c = 'active';
        update prg2;
        
                
        Specialization__c sp2 = Utils_TestMethods.createCountrySpecialization(sp1.id, spct1.id);
        insert sp2;
        sp2.Status__c = 'active';
        update sp2;
        
              
        //creates a partner account
        
        acc = Utils_TestMethods.createAccount();
        acc.country__c = country.id;
        insert acc;
                
        
        acc.isPartner = true;
        acc.PRMAccount__c = True;
        update acc;
        
        System.Debug('Account Created'+acc);
        
        acc1 = Utils_TestMethods.createAccount();
        acc1.country__c = country1.id;
        insert acc1;
        
        
        
        acc1.isPartner = true;
        acc1.PRMAccount__c = True;
        update acc1;
        
        System.Debug('Second Account created:'+acc1);
        System.Debug('The country program country is:'+cp1.country__c);
        System.Debug('The id for country program is:'+cp1.id);
        System.Debug('The recordtypeid for cp1 is:'+cp1.recordtypeid);
        System.Debug('The program status for cp1 is:'+cp1.programStatus__c);
        system.debug('cp1:'+cp1);
        
        acc =[select id, country__c from account where id =:acc.id limit 1];
        system.debug('acc:'+Acc);
        test.startTest();
        accprg = Utils_TestMethods.createAccountProgram(prg2.id, cp1.id, acc.id);
        insert accprg;
        
            
        //creates an assessment
    //  test.startTest();
        asm = Utils_TestMethods.createAssessment();
        asm.Name = 'Test';
        asm.AutomaticAssignment__c = false;
        asm.Name = 'Sample Assessment1';
        asm.PartnerProgram__c = cp1.id;
        asm.ProgramLevel__c = prg2.id;
        asm.specialization__c = sp2.id;
        insert asm;
        
        OpportunityAssessmentQuestion__c opassq = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'picklist', 4);
        insert opassq;
        
        System.Debug('Assessment question is:'+opassq);
        
        OpportunityAssessmentAnswer__c opassa = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq.id);
        insert opassa;
        System.Debug('Assessment Answer is:'+opassa);
        
        System.Debug('The updated assessment is:'+asm);
        
        orf1 = Utils_TestMethods.createORF(country.id);
        orf1.producttype__c = 'Test';
        orf1.PartnerProgram__c = cp1.id;
        orf1.ProgramLevel__c = prg2.id;
        insert orf1;
     //   test.startTest();
        asm.AutomaticAssignment__c = false;
        update asm;
        asm.AutomaticAssignment__c = true;
        update asm;
        
        System.Debug('The PartnerProgram and ProgramLevel for orf1 are:'+orf1.PartnerProgram__c+' '+orf1.ProgramLevel__c);
        System.Debug('The Opportunity Registration form:'+orf1);
        
            
        }
    
    }
    //----------------------------
     static testMethod void myUnitTest1(){
        User user1 = Utils_TestMethods.createStandardUserWithNoCountry('ASM');
        user1.ProgramAdministrator__c = true;
        system.runas(user1){
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='ASM';
        insert country;
        Country__c country1 = Utils_TestMethods.createCountry();
        country1.countrycode__c ='AI';
        insert country1;
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        Account acc = new Account();
        Account acc1 = new Account();
        ACC_PartnerProgram__c accprg = new ACC_PartnerProgram__c();
        ACC_PartnerProgram__c accprg1 = new ACC_PartnerProgram__c();
        OpportunityRegistrationForm__c orf1 = new OpportunityRegistrationForm__c();
        OpportunityRegistrationForm__c orf2 = new OpportunityRegistrationForm__c();
        OpportunityRegistrationForm__c orf3 = new OpportunityRegistrationForm__c();
        Assessment__c asm = new Assessment__c();
        user1.country__c = country.countrycode__c;
        
        // creates global program and its program level
        
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;
       
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;
        
        SpecializationCatalog__c spct1 = Utils_TestMethods.createSpecializationCatalog();
        insert spct1;
        spct1.active__c = true;
        update spct1;
        
        Specialization__c sp1 = Utils_TestMethods.createSpecialization(spct1.id);
        insert sp1;
        sp1.status__c = 'active';
        update sp1;
        
        //creates country program and its program level
        
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;
        
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;
        prg2.levelstatus__c = 'active';
        update prg2;
        
        PartnerProgram__c cp2 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country1.id);
        insert cp2;
        cp2.ProgramStatus__c ='active';
        update cp2;
        
        ProgramLevel__c prg3 = Utils_TestMethods.createCountryProgramLevel(cp2.id);
        insert prg3;
        prg3.levelStatus__c ='active';
        update prg3;
        
        ProgramLevel__c prg4 = Utils_TestMethods.createCountryProgramLevel(cp2.id);
        insert prg4;
        prg4.levelStatus__c = 'active';
        update prg4;
        
        Specialization__c sp2 = Utils_TestMethods.createCountrySpecialization(sp1.id, spct1.id);
        insert sp2;
        sp2.Status__c = 'active';
        update sp2;
        
                
        //creates a partner account
        
        acc = Utils_TestMethods.createAccount();
        acc.country__c = country.id;
        insert acc;
                
        
        acc.isPartner = true;
        acc.PRMAccount__c = True;
        update acc;
        
        System.Debug('Account Created'+acc);
        
        acc1 = Utils_TestMethods.createAccount();
        acc1.country__c = country1.id;
        insert acc1;
        
        
        
        acc1.isPartner = true;
        acc1.PRMAccount__c = True;
        update acc1;
        
        System.Debug('Second Account created:'+acc1);
        System.Debug('The country program country is:'+cp1.country__c);
        System.Debug('The id for country program is:'+cp1.id);
        System.Debug('The recordtypeid for cp1 is:'+cp1.recordtypeid);
        System.Debug('The program status for cp1 is:'+cp1.programStatus__c);
        system.debug('cp1:'+cp1);
        
        acc =[select id, country__c from account where id =:acc.id limit 1];
        system.debug('acc:'+Acc);
        test.startTest();
        accprg = Utils_TestMethods.createAccountProgram(prg2.id, cp1.id, acc.id);
        insert accprg;
        
        accprg1 = Utils_TestMethods.createAccountProgram(prg3.id, cp2.id, acc1.id);
        insert accprg1;
        
        //creates an assessment
    //  test.startTest();
        asm = Utils_TestMethods.createAssessment();
        asm.Name = 'Test';
        asm.AutomaticAssignment__c = false;
        asm.Name = 'Sample Assessment1';
        asm.PartnerProgram__c = cp1.id;
        asm.ProgramLevel__c = prg2.id;
        asm.specialization__c = sp2.id;
        insert asm;
        
        OpportunityAssessmentQuestion__c opassq = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'picklist', 4);
        insert opassq;
        
        System.Debug('Assessment question is:'+opassq);
        
        OpportunityAssessmentAnswer__c opassa = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq.id);
        insert opassa;
        System.Debug('Assessment Answer is:'+opassa);
        
        System.Debug('The updated assessment is:'+asm);
        
           
        asm.PartnerProgram__c = cp2.id;
        asm.ProgramLevel__c = prg3.id;
        update asm;
      //  test.stopTest();
        
        OpportunityAssessmentQuestion__c opassq1 = Utils_TestMethods.createOpportunityAssessmentQuestion(asm.Id, 'picklist', 4);
        insert opassq1;
        System.Debug('Assessment question is:'+opassq1);
        
        
        OpportunityAssessmentAnswer__c opassa1 = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq1.id);
        insert opassa1;
        System.Debug('Assessment Answer is:'+opassa1);
        
        System.Debug('The updated assessment is:'+asm);
        
        orf2 = Utils_TestMethods.createORF(country1.id);
        orf2.producttype__c = 'Test';
        orf2.PartnerProgram__c = cp2.id;
        orf2.ProgramLevel__c = prg3.id;
        insert orf2;
        
        asm.AutomaticAssignment__c = false;
        update asm;
        asm.AutomaticAssignment__c = true;
        update asm;
        test.stopTest();
        System.Debug('The PartnerProgram and ProgramLevel for orf2 are:'+orf2.PartnerProgram__c+' '+orf2.ProgramLevel__c);
        System.Debug('The Opportunity Registration form:'+orf2);
        
        }
     
    }
}