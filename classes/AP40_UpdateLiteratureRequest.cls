Public class AP40_UpdateLiteratureRequest
{
    public static void updateCustomerRequest(List<LiteratureRequest__c> aListOfLiteratureRequest)
    {
        
         //*********************************************************************************
        // Method Name : updateCustomerRequest
        // Purpose : To update Customer request from the Case
        // Created by : Ramesh Rajasekaran - Global Delivery Team
        // Date created : 24th February 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Apr - 12 Release
        ///*********************************************************************************/
       
        System.Debug('****AP40_UpdateLiteratureRequest updateCustomerRequest Started****'); 
        
        Map<ID,Case> caseMap = new Map<ID,Case>();
        
        // Build the set of Case IDs of Literature Requests
        For(LiteratureRequest__c aLiteratureRequest:aListOfLiteratureRequest)
        {
            caseMap.put(aLiteratureRequest.Case__c,null);
        }
        
        // Map IDs and Case Records
        For(Case aCase:[SELECT ID, CustomerRequest__c FROM Case WHERE ID IN :caseMap.keyset()])
        {
            caseMap.put(aCase.ID,aCase);
        }
        
        // Populate the Customer Request with the Customer Request coming from the Case
        For(LiteratureRequest__c aLiteratureRequest:aListOfLiteratureRequest)
        {
           // if(aLiteratureRequest.Case__c != null && caseMap.get(aLiteratureRequest.Case__c) != null)
           if(caseMap.containskey(aLiteratureRequest.Case__c)&& caseMap.get(aLiteratureRequest.Case__c) != null)
            {
              if(aLiteratureRequest.CustomerRequest__c != null)
                aLiteratureRequest.CustomerRequest__c = aLiteratureRequest.CustomerRequest__c +'<br/>'+ caseMap.get(aLiteratureRequest.Case__c).CustomerRequest__c; 
              else  
                aLiteratureRequest.CustomerRequest__c = caseMap.get(aLiteratureRequest.Case__c).CustomerRequest__c;
            }
        }
        
        System.Debug('****AP40_UpdateLiteratureRequest updateCustomerRequest Finished ****'); 
    }
    
    public static void InsertCommercialRefItem(List<LiteratureRequest__c> aListOfLiteratureRequest)
    {
      //*********************************************************************************
        // Method Name : InsertCommercialRefItem
        // Purpose : To Insert Commercial reference record
        // Created by : Ramesh Rajasekaran - Global Delivery Team
        // Date created : 10th October 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Dec - 12 Release
        ///*********************************************************************************/
      
      System.Debug('****AP40_UpdateLiteratureRequest InsertCommercialRefItem Started ****'); 
      
      List<LiteratureRequestLineItem__c> ltreqLIList = new List<LiteratureRequestLineItem__c>();
      If(!aListOfLiteratureRequest.isempty()){
          for(LiteratureRequest__c ltObj:aListOfLiteratureRequest){
              LiteratureRequestLineItem__c crrObj = new LiteratureRequestLineItem__c(
              CommercialReference__c = ltObj.CommercialReference__c,
              Quantity__c  = ltObj.Quantity__c,
              LiteratureRequest__c = ltObj.Id);
              ltreqLIList.add(crrObj);              
          }
      } 
      
      If(!ltreqLIList.isempty()){
      try{
          Insert ltreqLIList;
          }
          Catch(Exception Ex){System.debug('EXCEPTION'+Ex);}
      }
      
          
      System.Debug('****AP40_UpdateLiteratureRequest InsertCommercialRefItem Finished ****'); 
    }
}