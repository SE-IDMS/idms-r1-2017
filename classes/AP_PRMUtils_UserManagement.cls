public class AP_PRMUtils_UserManagement {
    public class InactiveContactList {
        @InvocableVariable(Label='lstContactId' description='List of PRM Contacts to be inactivated' Required=true) 
        public List<Id> lContactId;
    }

    @InvocableMethod(label='Get List of PRM Contact to be inactivated upon declining registration' description='This method calls toggle user asynchronously to inactivate users associated with the list of contacts')
    public static void InactivateUser (List<InactiveContactList> mUserList) {
        System.debug('**User List to InActivate :'+mUserList);
        System.debug('**User List to InActivate :'+mUserList[0].lContactId);
        AP_PRMUtils.toggleUser(mUserList[0].lContactId);
    }
}