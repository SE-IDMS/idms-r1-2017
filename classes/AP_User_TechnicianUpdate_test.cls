@isTest(SeeAllData=true)
Public Class AP_User_TechnicianUpdate_test
{
    static testMethod void Test() 
    {
        User usr1 = Utils_TestMethods.createStandardUser('alias');
        insert usr1;
        
        User usr = Utils_TestMethods.createStandardUser('alias');
        insert usr;
               
        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm;
                
        RecordType[] rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c'];
        RecordType[] rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c'];  
          
         for(RecordType rt : rts) 
        {
            if(rt.DeveloperName == 'Technician')
           {
                 st = new SVMXC__Service_Group__c(
                                            RecordTypeId =rt.Id,
                                            SVMXC__Active__c = true,
                                            Name = 'Test Service Team'                                                                                        
                                            );
                insert st;
           } 
        }
        for(RecordType rt : rtssg)
        {
           if(rt.DeveloperName == 'Technican')
            {
                sgm = new SVMXC__Service_Group_Members__c(RecordTypeId =rt.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = usr.Id);
                insert sgm;
            }
        }
         
        usr.Country__c = 'IN';       
        usr.Phone = '1234567';
        usr.Street = 'test1';
        usr.PostalCode = '112';
        usr.City = 'bangalore';
        usr.State = 'karnataka';
        usr.FirstName = 'DPK';
        usr.LastName = 'KRM';
        usr.UserBusinessUnit__c ='Energy';
        usr.FederationIdentifier = '234';  
        usr.MobilePhone ='12213';
        usr.ManagerId =usr1.Id;                        
        update usr;
                
        sgm.SVMXC__Street__c=usr.Street;
        sgm.SVMXC__Zip__c=usr.PostalCode;
        sgm.SVMXC__City__c=usr.City;
        sgm.SVMXC__State__c=usr.State;
        sgm.Name=usr.FirstName+' '+ usr.LastName;
        sgm.Business_Unit__c=usr.UserBusinessUnit__c;
        sgm.SESAID__c=usr.FederationIdentifier;  
        sgm.SVMXC__Phone__c = usr.MobilePhone;
        sgm.SVMXC__Email__c = usr.Email;
        sgm.Manager__c=usr.ManagerId;
        sgm.SVMXC__Role__c='Schneider Employee';
        update sgm;
        
    }
}