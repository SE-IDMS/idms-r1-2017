public class VFC_QuickANIUpdate{
    public Boolean isDesableAcPhone {get;set;}
    public Boolean isDesableworkphone {get;set;}
    public Boolean isDesableconmobile {get;set;}
    public Boolean isDesableconaltphone {get;set;}
    public Boolean isDesablecmdbutton {get;set;}
    public wrapper myWrapper{get;set;}
    public Boolean isAccountPhone{get;set;}
    public Boolean isContactWorkPhone{get;set;}
    public Boolean isContactMobile{get;set;}
    public Boolean isContactAltPhone{get;set;}
    public Boolean isContact{get;set;}
    public Boolean isAccount{get;set;}
    public Boolean isAninew{get;set;}
    public Boolean isAni{get;set;}
    
    String ContactId='';
    String AccountId='';
    String newANI;
   
    public  VFC_QuickANIUpdate(){
        isDesableAcPhone=false;
        isDesableworkphone =false;
        isDesableconmobile=false;
        isDesableconaltphone=false;
        isContact=false;
        isAninew=false;
        isAccount=false;
        isDesablecmdbutton=false;
        String newAninumberdecoded='';
        String newAninumber='';
               
               //getting the ANI Number from the URL
               
               if(System.currentPageReference().getParameters().containsKey('ANINumber')){
                   System.debug('*'+newAninumber);
                   System.debug('*Param:' + System.currentPageReference().getParameters().get('ANINumber'));
                   newAninumber=System.currentPageReference().getParameters().get('ANINumber');
                   System.debug('*'+newAninumber);
               }
               //newAninumber=EncodingUtil.urlDecode(newAninumber,'UTF-8');
                
               if(newAninumber==''  || newAninumber== null || newAninumber=='null'){
                    isAninew=true;
                    isDesablecmdbutton=true;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CLDEC13CCC25);
                    ApexPages.addMessage(myMsg);
                }
                
                  //getting the contactId from the URL
                if(System.currentPageReference().getParameters().containsKey('ContactId')){
                    ContactId=System.currentPageReference().getParameters().get('ContactId');
                    if(ContactId=='' ||ContactId=='null' || ContactId==null){
                        //isContact=true;
                        isDesablecmdbutton=true;    
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CLAPR14CCC25);
                    ApexPages.addMessage(myMsg);
                    }
               }
                System.debug('***contactid'+ContactId);
               //getting the AccountId from the URL    
                if(System.currentPageReference().getParameters().containsKey('AccountId')) {   
                AccountId=System.currentPageReference().getParameters().get('AccountId');
                    if(AccountId=='' ||AccountId=='null' || AccountId==null){
                        //isAccount=true;
                        isDesablecmdbutton=true;
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CLAPR14CCC25);
                    ApexPages.addMessage(myMsg);
                    }
                 }   
                System.debug('***accountid'+accountId);
           //In the context of Contact creating the New ANI 
                if(ContactId!='' && ContactId!=null ){
             
                String mobilephone='';
                String altphone='';
                String workphone='';
                String accphone='';
                String cname='';
                System.debug('*newAninumber:' + newAninumber);
                String anipass=newAninumber;
                string anipass1;
                // getting the contact phone fields,account phone field based on the  ContactID
                Contact con =[select id,Name,MobilePhone,Aphone__c,WorkPhone__c,Account.Phone from contact where id =: ContactId];
                // Default check box  Mechanism
                    if(con.WorkPhone__c == null)
                        isContactWorkPhone =true;
                    else
                        if(con.MobilePhone== null)
                            isContactMobile =  true;
                            else
                                if(con.Aphone__c== null)
                                isContactAltPhone =  true;
                                else
                                   isAni= true;
                             //appending the Braces and '+'  to the value  
                              
                                if(anipass!=null && anipass!='')
                                anipass1='('+anipass+')';
                                System.debug('*anipass:' + anipass);
                                System.debug('*anipass1:' + anipass1);
                                if(con.MobilePhone!=null &&con.MobilePhone!='')                                 
                                    mobilephone+='('+con.MobilePhone+')';
                                if(con.Aphone__c!=null &&con.Aphone__c!='')
                                    altphone+='('+ con.Aphone__c+')';
                                if(con.WorkPhone__c!=null &&con.WorkPhone__c!='')
                                    workphone+='('+ con.WorkPhone__c+')';
                                if(con.Account.Phone!=null &&con.Account.Phone!='')
                                    accphone+='('+ con.Account.Phone+')';                               
                                    if(con.name!=null&&con.name!='')
                                cname=con.name;
                              
                    Wrapper w = new Wrapper(mobilephone,workphone,altphone,accphone,cname,anipass1);
                    myWrapper = w;
            }
            //In the context of Account creating the New ANI 
        else{        
            if(AccountId!=null && AccountId!=''){
            isDesableworkphone =true;//Making the checkbox to not editable
            isDesableconmobile = true;//Making the checkbox to not editable
            isDesableconaltphone =true;//Making the checkbox to not editable
            
                List<Account> acc1=[select id,Name,phone from account where id=: AccountId ];
            
                if(acc1!=null&&acc1.size()>0){
                    for(Account Acc: acc1){
                        String accphone='';
                        String aname='';
                        String anipass=newAninumber;
                        String anipass1='';
                        if(acc.phone!=null)
                            isAni= true;
                            else
                                isAccountPhone=true;
                               
                           if(anipass!=null && anipass!='')
                              anipass1='('+anipass+')';
                            if(Acc.Phone!=null &&Acc.Phone!='')
                                accphone+='('+ Acc.Phone+')';
                            if(acc.name!=null&&acc.name!='')
                            aname=acc.name;
                              
                        Wrapper w = new Wrapper(accphone,aname,anipass1);
                        myWrapper = w;
                    }
                }
            }
        }
    }

    public PageReference Pageredirection() {
            String Url;
            ANI__c newaniobj = new ANI__c();
            newANI=System.currentPageReference().getParameters().get('ANINumber');
            if(ContactId!='' && ContactId!=null){
                Contact con =[select id,MobilePhone,Name,Aphone__c,WorkPhone__c,Account.Phone,Accountid from contact where id =: ContactId];
                System.debug('***'+con.accountid);
                Account Acobj=[select id,phone from account where id=:con.accountid];
                AccountID=Acobj.id;  
                System.debug('***'+Acobj);
                if(isContactAltPhone ==true){
                    con.Aphone__c=newANI;
                }
                else 
                    if(isContactMobile ==true){
                    con.MobilePhone=newANI;
                    }
                else
                    if(isContactWorkPhone==true){
                    con.WorkPhone__c=newANI;
                    } 
                       if(isContactWorkPhone==true||isContactMobile ==true||isContactAltPhone ==true) {
                            update con;
                    }
                    if(isAccountPhone==true){
                        Acobj.phone=newANI;
                          update  Acobj; 
                    }
                    if(isani==true&&ContactId!=null){
                        newaniobj.ANINumber__c=newANI;
                        newaniobj.Contact__c=ContactID;
                        insert newaniobj;
                    }   
            }
            else{
                if(AccountID!=null&& AccountId!=''){
                    Account acc=[select id,phone from account where id=: AccountId ];
                   
                   if(isAccountPhone==true){
                        acc.Phone=newANI;
                    }
                    update acc;
                  
                    if(isani==true && AccountID!='null' && AccountID!=null ){
                        newaniobj.ANINumber__c=newANI;
                        newaniobj.Account__c=AccountID;
                     
                        insert newaniobj;
                    }   
                    
                }
                System.debug('********'+newaniobj);
                System.debug('********'+newaniobj.id);
            }
            if(AccountID!=null &&AccountID!=''&&isani==false && isAccountPhone==true)
                url='/'+AccountID;  // Pageredirects to accout detail page according to the account id
                else 
            if(Contactid!=null && contactid!=''&&isani==false && isAccountPhone==false) 
                url='/'+contactid;// Pageredirects to Contact detail page according to the Contact id
            else
                if(newaniobj.id!=null && isani==true )
                    url='/'+newaniobj.id;//Pageredirects to ANI detail page according to the ANIid
                    
                    PageReference pageRef=new PageReference(url);
                    pageRef.setRedirect(true);
                    return pageRef;
             
    }
    /*
    // This PageRedirection is for Cancel Button will be redirected to contact search page
    public PageReference Pageredirection1() {
    
        Pagereference pageref=new PageReference('/apex/VFP_ContactSearch?sfdc.tabName=01rA0000000JuTL');
        pageRef.setRedirect(true);
        return pageRef;
        
    }
    */
    public class Wrapper{

        public String ANI { get; set; }
        public String ContactMobile { get; set; }
        public String ContactWorkPhone { get; set; }
        public String ContactAltPhone { get; set; }
        public String accountphone { get; set; }
        public String contactname{get;set;}
        public string AniPassed{get;set;}
        public String accountname{get;set;}
        Public String newANI{get;set;}
        public Wrapper (String cmobile,String CWorkPhone,String CAltPhone,String accphone,String cname,String anipass){
            this.contactmobile=cmobile;
            this.ContactWorkPhone=CWorkPhone;
            this.ContactAltPhone=CAltPhone;
            this.accountphone=accphone;
            this.contactname=cname;
            this.anipassed=anipass;
            
        }
        public Wrapper (String accphone,String aname,String anipass){
            this.accountphone=accphone;
            this.accountname=aname;
            this.anipassed=anipass;
        }
    }
   
}