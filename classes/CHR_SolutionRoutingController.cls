public with sharing class CHR_SolutionRoutingController {
	public String objectType{get;set;}
	public list<solutionGroupings> solutionGroupList {get;set;}
	public ApexPages.StandardController controller{get;set;}
	public CHR_ChangeReq__c oCHR {get;set;}
	public String selectedNodeId {get;set;}

	public CHR_SolutionRoutingController(ApexPages.StandardController controller){
		this.controller = controller;
		mainInit();
	}
	
	public void mainInit(){
		objectType = 'DMT_Application__c';
		oCHR = (CHR_ChangeReq__c)controller.getRecord();
		
		solutionGroupList = new list<solutionGroupings>();
		
		Schema.DescribeFieldResult solTypeDesc = DMT_Application__c.AM_IM__c.getDescribe();
			
		for(Schema.PicklistEntry pickValue:solTypeDesc.getPicklistValues()){
			AggregateResult[] groupedResults = [SELECT SolutionGroupL1__c from DMT_Application__c  where AM_IM__c = :pickValue.getLabel() Group By SolutionGroupL1__c order by SolutionGroupL1__c];
			Set<String> aggregatedValueSet = new set<String>();
			for (AggregateResult ar : groupedResults) {
				aggregatedValueSet.add((String)ar.get('SolutionGroupL1__c'));

			}
			solutionGroupings tempGrp = new solutionGroupings();
			tempGrp.solutionType = pickValue.getLabel();
			tempGrp.solutionGroupSet = aggregatedValueSet;
			
			solutionGroupList.add(tempGrp);
		}
		
		
	}
	
	public void parseUserSelection(){
		list<String> partList = selectedNodeId.split(';');
		oCHR.AppliInfra__c =null;
		oCHR.Module__c=null;
		oCHR.SubModule__c=null;
		
		
		//We get directly to the last Part
		if (partList.size()>0){
			String part;
			part = partList[partlist.size()-1];
			String partType = part.split(':')[0];
			String partId = part.split(':')[1];
			
			if (partType =='App'){
				oCHR.AppliInfra__c=[select id from DMT_Application__c where id =:partId].id;
			} else if (partType=='Module'){
				Module__c module=[select id,Name,Application__c from Module__c where id =:partId];
				oCHR.AppliInfra__c = module.Application__c;
				oCHR.Module__c = module.Id;
			} else if (partType=='SubModule'){
				Module__c module=[select id,Name,ParentModule__c,ParentModule__r.Application__c from Module__c where id =:partId];
				oCHR.AppliInfra__c = module.ParentModule__r.Application__c;
				oCHR.Module__c = module.ParentModule__c;
				oCHR.SubModule__c = module.id;
			}
		}
		
		//Based on the parsing, we lookup the best Solution and delivery team
		suggestTeamForApplication();
		
		
	}
	
		public void suggestTeamForApplication(){
		Id solutionTeamId;
		Id deliveryTeamId;
		if (oCHR.SubModule__c<>null){
			Module__c oModule = [select Id,SolutionTeam__c,DeliveryTeam__c from Module__c where id=:oCHR.SubModule__c];
			if (oModule.SolutionTeam__c <> null) solutionTeamId = oModule.SolutionTeam__c;
			if (oModule.DeliveryTeam__c <> null) deliveryTeamId = oModule.DeliveryTeam__c;
		}
		
		if (oCHR.Module__c<>null &&(solutionTeamId==null||deliveryTeamId==null)){
			Module__c oModule = [select Id,SolutionTeam__c,DeliveryTeam__c from Module__c where id=:oCHR.Module__c];
			if (oModule.SolutionTeam__c <> null && solutionTeamId==null) solutionTeamId = oModule.SolutionTeam__c;
			if (oModule.DeliveryTeam__c <> null && deliveryTeamId==null) deliveryTeamId = oModule.DeliveryTeam__c;
		}
		
		if (oCHR.AppliInfra__c<>null &&(solutionTeamId==null||deliveryTeamId==null)){
			DMT_Application__c oApp = [select Id,SolutionTeam__c,DeliveryTeam__c from DMT_Application__c where id=:oCHR.AppliInfra__c];
			if (oApp.SolutionTeam__c <> null && solutionTeamId==null) solutionTeamId = oApp.SolutionTeam__c;
			if (oApp.DeliveryTeam__c <> null && deliveryTeamId==null) deliveryTeamId = oApp.DeliveryTeam__c;
		}
		
		oCHR.SolutionTeam__c = solutionTeamId;
		oCHR.DeliveryTeam__c = deliveryTeamId;
	}
	
	
	
	public class solutionGroupings{
		public String solutionType {get;set;}
		public Set<String> solutionGroupSet {get;set;}		
	}


	public static testmethod void testSolutionRouting(){
		
	
		REF_TeamRef__c testTeam = new REF_TeamRef__c();
		testTeam.Name = 'Test Team';
		testTeam.Queue__c = [select Id from Group where Type='Queue' limit 1].Id;
		insert testTeam;
	
		DMT_Application__c testApplication1  = new  DMT_Application__c();
		testApplication1.Name = 'test';
		testApplication1.AM_IM__c = 'Applications';
		testApplication1.SolutionGroupL1__c = 'test';
		testApplication1.SolutionGroupL2__c = 'test1';
		testApplication1.DeliveryTeam__c=testTeam.Id;
		insert testApplication1;
		
		DMT_Application__c testApplication2  = new  DMT_Application__c();
		testApplication2.Name = 'test';
		testApplication2.AM_IM__c = 'Applications';
		testApplication2.SolutionGroupL1__c = 'test';
		testApplication2.SolutionGroupL2__c = 'test2';
		testApplication2.DeliveryTeam__c=testTeam.Id;
		insert testApplication2;
		
		Module__c testModule = new Module__c();
		testModule.Name = 'testModule';
		testModule.Application__c = testApplication2.Id;
		testModule.SolutionTeam__c = testTeam.Id;
		insert testModule;
		
		DMT_Application__c testApplication3  = new  DMT_Application__c();
		testApplication3.Name = 'test';
		testApplication3.AM_IM__c = 'Infrastructures';
		testApplication3.SolutionGroupL1__c = 'test';
		testApplication3.SolutionGroupL2__c = 'test1';
		insert testApplication3;
		
		Module__c testModule2 = new Module__c();
		testModule2.Name = 'testModule';
		testModule2.Application__c = testApplication3.Id;
		insert testModule2;
		
		Module__c testSubModule1 = new Module__c();
		testSubModule1.Name = 'testModule';
		testSubModule1.Application__c =  testApplication3.Id;
		testSubModule1.ParentModule__c = testModule2.id;
		insert testSubModule1;
		
		String rcId = Utils_TestMethods.getRecordTypeId('SCE01_EMEAS_01');
		CHR_ChangeReq__c testCHR = new CHR_ChangeReq__c();
		testCHR.Name = 'test';
		testCHR.recordTypeId=rcId;
		insert testCHR;
		
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(testCHR);
		Test.startTest();
			CHR_SolutionRoutingController routing = new CHR_SolutionRoutingController(sc);
			Test.setCurrentPageReference(new PageReference('Page.CHR_SolutionAjaxController')); 
			String selectedNode = 'solutionType:Applications;level1Group:test';
			System.currentPageReference().getParameters().put('root', selectedNode);
			CHR_SolutionAjaxRespController ajaxCtl = new CHR_SolutionAjaxRespController();
			ajaxCtl.doSearch();
			String result = ajaxCtl.getResult();
			selectedNode = 'solutionType:Applications;level1Group:test;level2Group:test1';
			System.currentPageReference().getParameters().put('root', selectednode);
			ajaxCtl.doSearch();
			result = ajaxCtl.getResult();
			
			selectedNode = 'solutionType:Applications;level1Group:test;level2Group:test2;App:'+testApplication3.Id;
			System.currentPageReference().getParameters().put('root', 'solutionType:Applications;level1Group:test;level2Group:test2;App:'+testApplication3.Id);
			ajaxCtl.doSearch();
			result = ajaxCtl.getResult();
			routing.selectedNodeId = selectedNode;
			routing.parseUserSelection();
			
			selectedNode = 'solutionType:Applications;level1Group:test;level2Group:test2;App:'+testApplication3.Id+';Module:'+testModule2.id;
			System.currentPageReference().getParameters().put('root', 'solutionType:Applications;level1Group:test;level2Group:test2;App:'+testApplication3.Id+';Module:'+testModule2.id);
			ajaxCtl.doSearch();
			result = ajaxCtl.getResult();
			routing.selectedNodeId = selectedNode;
			routing.parseUserSelection();
		
		
		test.stopTest();
		
		
		
		
	}

}