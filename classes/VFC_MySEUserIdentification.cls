/**
* @author: Sonal Dixit
* @created: August'16
* @description:Controller class for 'VFP_MySEUserIdentification'
* @release:Q3 2016 Release
*/


public without sharing class VFC_MySEUserIdentification{
    private ApexPages.StandardController controller;
    public Case caseRecord{get;set;}
    public list<DigitalToolsUserInformation__c> MySEinfo {get;set;}
    public list<DigitalToolsUserInformation__c> FinalMySEinfo = new list<DigitalToolsUserInformation__c>();
    public Boolean shouldDisplay {get;set;}
    public List<wrapperclass> mySEWrapper {get; set;}
    Id conID;
        
        public VFC_MySEUserIdentification(ApexPages.StandardController controller){
            mySEWrapper = new List<wrapperclass>();
            List<wrapperclass>mySEWrapper2 = new List<wrapperclass>();
            this.controller = controller;
            caseRecord=(Case)controller.getRecord();
            if(caseRecord.id!=null){
                caseRecord=[select Id, Contactid,LastAgentPromoted__c,CasePromoted__c from Case where id=:caseRecord.id];
                conID = caseRecord.Contactid;
                shouldDisplay=false;
                
            }
        
            MySEinfo=new List<DigitalToolsUserInformation__c>(); 
            //Querying MySE Object for all the fields to be updated
            MySEinfo =  [Select Id, Name,CasePromoted__c,CountOfPromotion__c, CustomerResponse__c, GranularAccess__c,IsActive__c,LastAccessRemoteAdminUpdate__c,LastAgentPromoted__c,LastPromotionDateTime__c,MySEBU__c,RemoteAdminName__c,Contact__c from DigitalToolsUserInformation__c where Contact__c  =: conID ORDER BY IsActive__c];
            for(DigitalToolsUserInformation__c xMySE : MySEinfo) {
                // If the MySE record is active put it above in the list and append the list with the inactive MySe records 
                if(xMySE.IsActive__c== true){
                mySEWrapper.add(new wrapperclass(xMySE));
                }
                else{
                // New list to contain all inactive MySE records
                    mySEWrapper2.add(new wrapperclass(xMySE));
                }    
            }   // Adding all in the list keeping active records above and inactive records below.
                mySEWrapper.AddAll(mySEWrapper2);
                if(MySEinfo.size()>0){
                    shouldDisplay=true;   
                }
            }
            
   
    public PageReference save(){
    
        MySEinfo = new list<DigitalToolsUserInformation__c>();
        FinalMySEinfo=new List<DigitalToolsUserInformation__c>();
        // Iterating the list and updating the Case only for those records where the checkbox is checked 
        for(wrapperclass xWC : mySEWrapper) {
            if(xWC.checkbox) {
                
                MySEinfo.add(xWC.mySE);
                caseRecord.LastAgentPromoted__c = UserInfo.getUserId();
                caseRecord.CasePromoted__c=  True;
               
            }
           
        }   
       
    
    try{
    
        database.update(caseRecord);
        if(Test.isRunningTest())
         integer intTest =1/0;
        
       }catch(Exception ex)// exception handling
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.error, ex.getMessage());
                ApexPages.addMessage(msg);
            }
       
    
      for(DigitalToolsUserInformation__c  temp: MySEinfo ){
            //Updating the MySE records
            temp.LastPromotionDateTime__c = System.now();
            temp.LastAgentPromoted__c= UserInfo.getUserId();
            temp.CasePromoted__c=caseRecord.Id;
            if(temp.CountOfPromotion__c==0 ||temp.CountOfPromotion__c==null ){
            temp.CountOfPromotion__c=1;
            }
            else{
            temp.CountOfPromotion__c=temp.CountOfPromotion__c+1;
            }
            FinalMySEinfo.add(temp);
        }
         try{
    
            database.update(FinalMySEinfo);
            if(Test.isRunningTest())
                    integer intTest =1/0;
        
        }catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            return null;
            }
         
         PageReference detailpage = new PageReference('/'+caseRecord.id);
            return detailpage;
    }
    
   
   
        
        // Wrapper class to update only the selected records on the page based on checkbox    
        public class wrapperclass {
            public boolean checkbox {get; set;}
            public DigitalToolsUserInformation__c mySE {get; set;}
            
            
            public wrapperclass (DigitalToolsUserInformation__c d) {
                mySE = d;
                checkbox = false;
            }
        }
    }