/**************************
    Author: Fielo Team
***************************/
@isTest
public with sharing class FieloPRM_VFC_RewardsLinkTermsCondCtTest{
  
    public static testMethod void testUnit1(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        User u = new User();
         
        FieloEE__Component__c comp;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test', 
                FieloEE__LastName__c = 'test'
            );
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc = new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;
        
            FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(
                FieloEE__MemberId__c = mem.Id
            );
            Insert memb;
            
            FieloEE__Member__c membertest = [SELECT Id, FieloEE__User__c FROM FieloEE__Member__c WHERE id =: mem.id ];

            u = [SELECT Id, UserName FROM User WHERE Id =: membertest.FieloEE__User__c ];
        }
        system.runAs(u){
            FieloPRM_VFC_RewardsLinkTermsCondCont controller = new FieloPRM_VFC_RewardsLinkTermsCondCont();
            controller.goToTermsConditions();
        }

    }
}