/*
    Author          : Bhuvana S
    Description     : Test class for AP_CFWRiskAssessmentAfterupdate
*/
@isTest
private class AP_CFWRiskAssessmentAfterupdate_TEST 
{
    static testMethod void testriskAfterUpdate()
    {
        test.startTest();
            
        CFWRiskAssessment__c cfwr = new CFWRiskAssessment__c();
        cfwr.ProjectName__c = 'testProjectName';
        insert cfwr;
        CertificationChecklist__c cc = new CertificationChecklist__c();
        cc.CertificationRiskAssessment__c = cfwr.Id;
        insert cc;
        cfwr.ApplicableforcustomMobileApp__c = 'No';
        update cfwr;
        
        test.stoptest();
    }
}