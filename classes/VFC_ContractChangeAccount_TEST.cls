/*******************************************************************
    Author       : Stephen Norbury
    Created Date : 24/07/2014
    
    Purpose      : Test Methods for the Change Contract Account controller
     
    ********************************************************************/

@isTest
private class VFC_ContractChangeAccount_TEST
{

  static testmethod void changeContractAccountTestMethod ()
  {
    Contract CustomerContract = null;
    Contract ClonedContract   = null;

    Country__c testCountry = Utils_TestMethods.createCountry();
    insert testCountry;
         
    Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
    testAccount.Name = 'Test';
    insert testAccount;
         
    Account testAccount2 = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
    testAccount2.Name = 'Test2';
    insert testAccount2;
                  
    Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
    testContact.Country__c = testCountry.Id;
    insert testContact;
         
    Contact testContact2 = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
    testContact2.Country__c = testCountry.Id;
    insert testContact2;
                        
    Contract testContract = Utils_TestMethods.createContract(testAccount.Id , testContact.Id);
    insert testContract ;
         
    testContract.webaccess__c = 'Eclipse';
    testContract.Status = 'Activated';    
    testContract.startdate = system.today();
    testContract.ContractTerm = 3;
    update testContract;    
         
    PackageTemplate__c testPkgTemplate = new PackageTemplate__c();
    testPkgTemplate.Name = 'TEST TEMPLATE2';
    insert testPkgTemplate;
                 
    Package__c testPackage = new Package__c();
    testPackage.Contract__c = testContract.Id;
    testPackage.startdate__C = system.today();
    testPackage.enddate__c = system.today() +3 ;
    testPackage.status__c = 'Active';
    testPackage.PackageTemplate__c = testPkgTemplate.Id;
    insert testPackage;
         
    CTR_ValueChainPlayers__c testcvcp1 = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
    testcvcp1.contact__c = testContact.id;
    testcvcp1.contactrole__C = 'Beneficiary';
    insert testcvcp1;
         
    CTR_ValueChainPlayers__c testcvcp = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
    testcvcp.contact__c = testcontact.id;
    testcvcp.contactrole__C = 'Admin';
    testcvcp.account_role__c = 'Agent';
    insert testcvcp;
         
    CTR_ValueChainPlayers__c testcvcp2 = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
    testcvcp2.contact__c = testcontact.id;
    testcvcp2.contactrole__C = 'Admin';
    testcvcp2.account_role__c = 'Distributors / Resellers';
    insert testcvcp2;
         
    CTR_ValueChainPlayers__c testcvcp3 = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
    testcvcp3.contact__c = testContact2.id;
    testcvcp3.contactrole__C = 'Beneficiary';
    insert testcvcp3;
       
    ApexPages.StandardController stdController = new ApexPages.StandardController(testContract);
        
    VFC_ContractChangeAccount extController = new VFC_ContractChangeAccount(stdController);
    
    extController.save();
    
    testContract.AccountId = null;
    
    extController.c = testContract;
          
    extController.save();
    
    testContract.AccountId = testAccount2.Id;
    
    extController.c = testContract;
          
    extController.save();
    
    testContract.ContactName__c = testContact.Id;
    
    extController.c = testContract;
          
    extController.save();
  }
}