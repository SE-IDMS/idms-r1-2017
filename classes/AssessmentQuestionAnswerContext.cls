global class AssessmentQuestionAnswerContext
{
    WebService String[] assessmentIDs;
    WebService String[] assessmentQuestionIDs;

    WebService Integer StartRow = 1;
    WebService Integer BatchSize = 10000;
     
    WebService DateTime LastModifiedDate1;
    WebService DateTime LastModifiedDate2;
    WebService DateTime CreatedDate;
    
    
    /**
    * A blank constructor
    */
    public AssessmentQuestionAnswerContext() {
    }

    /**
    * A constructor 
    */
    public AssessmentQuestionAnswerContext(List<String> assessmentIDs) {
        this.assessmentIDs = assessmentIDs;

    }

     public AssessmentQuestionAnswerContext(List<String> assessmentIDs, List<String> assessmentQuestionIDs) {
        this.assessmentIDs = assessmentIDs;
        this.assessmentQuestionIDs = assessmentQuestionIDs;

    }

}