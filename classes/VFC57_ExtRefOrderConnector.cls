public class VFC57_ExtRefOrderConnector
{
    public ExtRefOrder OrderImplementation{get;set;}
    public String StandardObjectID{get;set;}
    
    public VFC57_ExtRefOrderConnector(ApexPages.StandardController controller)
    {
        OrderImplementation = new ExtRefOrder();
        String StandardObjectID =  controller.getRecord().Id;           
    }

    // Implementation of the Order implementation for the Case Order Connector
    public class ExtRefOrder implements utils_OrderMethods
    {
        public CSE_ExternalReferences__c ExternalReference{get;set;}  // Related External Reference
    
        public void Init(VCC07_OrderConnectorController PageController)
        {
            List<CSE_ExternalReferences__c> ExternalReferences = [SELECT Id, Name, Case__c, ClientReference__c, SystemReference__c, TECH_BackOffice__c, TECH_Account__c 
                                                                  FROM CSE_ExternalReferences__c WHERE ID=:PageController.StandardObjectID LIMIT 1];     
            
            if(ExternalReferences.size()==1)
            {
                 PageController.RelatedRecord = [SELECT ID, CaseNumber, AccountId  FROM Case WHERE ID=:ExternalReferences[0].Case__c LIMIT 1];
                 ExternalReference = ExternalReferences[0];
            }            
        
            Case Cse = (Case)PageController.RelatedRecord;  
            PageController.ActionLabel = 'Select Order';   
            PageController.DateCapturer.Account__c = ExternalReference.TECH_Account__c;
          
            PageController.BackOfficeRefresher();  // Populate the back office list         

            // Key is made from the Order Number + The Back Office Name
            PageController.KeyForExistingRecords.add('SEOrderReference__c');
            PageController.KeyForExistingRecords.add('BackOffice__c'); 
             
            // Creation of the set of already existing external references 
            PageController.existingOrders = new Set<Object>();    
                         
            For(CSE_ExternalReferences__c ExtReference:[SELECT Id, Name, TECH_BackOffice__c, SystemReference__c, ClientReference__c, Case__c FROM CSE_ExternalReferences__c WHERE Case__c=:PageController.StandardObjectID]) 
            {
                String NewKey = ExtReference.SystemReference__c + ExtReference.TECH_BackOffice__c;
                PageController.existingOrders.add((String)NewKey);                              
            }
                             
            // If the user is viewing an already existing External Reference                                          
            if(ExternalReference!=null)
            {
                PageController.OrderNumber = ExternalReference.SystemReference__c;
                PageController.PONumber = ExternalReference.clientReference__c; 
                PageController.BackOffice = ExternalReference.TECH_BackOffice__c;
                PageController.DateCapturer.Account__c = ExternalReference.TECH_Account__c; 
                PageController.Action2Label = null;
                PageController.SearchOrder(); // The search is performed
                
                if(PageController.fetchedRecords.size()>0)
                {
                    PageController.SelectedOrder = (OPP_OrderLink__c)PageController.fetchedRecords[0];
                }
                
                PageController.DisplayDeliveryDetails();
                PageController.ReadOnly = true; // Read Only mode is selected
                PageController.EnableDetails = true; 
                PageController.TabInFocus = 'OrderDetails';   // The second tab is displayed                        
            } 
            
            System.debug('#### SELECTED ORDER: '  + PageController.SelectedOrder);   
        }
        
        public pagereference Cancel(VCC07_OrderConnectorController PageController)
        {
            Case Cse = (Case)PageController.RelatedRecord;
            PageReference CasePage =  new ApexPages.StandardController(Cse).cancel();
            return CasePage;
        }
        
        public pagereference PerformAction(VCC07_OrderConnectorController PageController){return null;}
    }
}