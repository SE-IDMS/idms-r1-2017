/* 
    Apex Class Name: Utils_GetObjectDetURL_Test
    Author:Amitava Dutta
    Date:25/01/2011.
    Description:This class is used to create URL for the new Objective  
    Setting Details.
*/

@isTest
private class VFC21_DesignObjectDetailsURL_Test
{    
  //Region Defining the Class Variables
    public static VFC21_DesignObjectDetailsURL objUrlGenerator = null; 
    public static ApexPages.StandardController objController;
    static List<ObjectivesSettings__c> lstObjectiveSettings = null;
  //End of Defining the Class Variables
    
  //Region Test Methods
       // ------ Check the New Objective Settings Details Creation URL
        static TestMethod void TestCheckObjectiveSettingsDetailsURL()
        {   
           
          //Region Constructor
            objUrlGenerator = new VFC21_DesignObjectDetailsURL();
          //End Constructor
            Test.startTest();   
            /* 
             // Test the URL For Objective Details for Monthly Objective Settings
             lstObjectiveSettings = [SELECT ID FROM ObjectivesSettings__c 
             WHERE PeriodType__c = 'Month' ORDER BY CREATEDDATE DESC LIMIT 1];
             if(lstObjectiveSettings.size()!=0)
             {
                 objUrlGenerator.ObjSettingsID = lstObjectiveSettings[0].ID; 
                 objUrlGenerator.GetObjDetailUrl();
             } 
             
             // Test the URL For Objective Details for Quarterly Objective Settings
             lstObjectiveSettings = [SELECT ID FROM ObjectivesSettings__c 
             WHERE PeriodType__c = 'Quarter' ORDER BY CREATEDDATE DESC LIMIT 1];
             if(lstObjectiveSettings.size()!=0)
             {
                 objUrlGenerator.ObjSettingsID = lstObjectiveSettings[0].ID; 
                 objUrlGenerator.GetObjDetailUrl();
             }
             
             // Test the URL For Objective Details for Yearly Objective Settings
             lstObjectiveSettings = [SELECT ID FROM ObjectivesSettings__c 
             WHERE PeriodType__c = 'Year' ORDER BY CREATEDDATE DESC LIMIT 1];
             if(lstObjectiveSettings.size()!=0)
             {
                 objUrlGenerator.ObjSettingsID = lstObjectiveSettings[0].ID; 
                 objUrlGenerator.GetObjDetailUrl();
             }     
            */
            Test.stopTest();
            
        }
  //Region End of Test Methods
}