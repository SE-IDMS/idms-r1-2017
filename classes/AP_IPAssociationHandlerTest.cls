@isTest(seealldata=true)
private class AP_IPAssociationHandlerTest {
    
    private static RecordType serviceContractRT = [select id, name from RecordType where Name = 'Service Contract' limit 1];
    private static RecordType serviceLineRT = [select id, name from RecordType where Name = 'Service Line' limit 1];
    
    private static Country__c countryFR;
    private static Country__c countryUS;
    private static Country__c countryIN;
    
    private static StateProvince__c spUS_CA;
    private static StateProvince__c spUS_RI;
    
    private static StateProvince__c spIN_10;
    private static StateProvince__c spIN_30;
    
    private static Account accountUS1;
    private static Account accountUS2;
    private static Account accountUS3;
    private static Account accountUS4;
    
    private static Account accountIN1;
    private static Account accountIN2;
    private static Account accountIN3;
    private static Account accountIN4;
    
    private static Account accountFR1;
    private static Account accountFR2;
    
    private static Brand__c brand1;
    private static Category__c range1;
    private static DeviceType__c deviceType1;
    
    private static Product2 product1;
    private static Product2 product2;
    private static Product2 product3;
    
    private static SVMXC__Installed_Product__c ipUS2A;
    private static SVMXC__Installed_Product__c ipUS2B;
    private static SVMXC__Installed_Product__c ipUS2C;
    
    private static SVMXC__Installed_Product__c ipUS3A;
    private static SVMXC__Installed_Product__c ipUS3B;
    
    private static SVMXC__Installed_Product__c ipIN2A;
    private static SVMXC__Installed_Product__c ipIN2B;
    private static SVMXC__Installed_Product__c ipIN2C;
    
    private static SVMXC__Installed_Product__c ipIN3A;
    private static SVMXC__Installed_Product__c ipIN3B;
    
    private static SVMXC__Installed_Product__c ipFR2A;
    private static SVMXC__Installed_Product__c ipFR2B;
    private static SVMXC__Installed_Product__c ipFR2C;
    
    private static SVMXC__Installed_Product__c ipFR3A;
    private static SVMXC__Installed_Product__c ipFR3B;
    
    static {
        prepareCommonData();
    }
    
    static testMethod void insertCoveredProducts() {
                
        // Creating Service Contracts Headers
        SVMXC__Service_Contract__c contractUS = new SVMXC__Service_Contract__c(RecordTypeId = serviceContractRT.Id, Name = 'Test Contract US', SoldtoAccount__c = accountUS1.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract 1');
        SVMXC__Service_Contract__c contractIN = new SVMXC__Service_Contract__c(RecordTypeId = serviceContractRT.Id, Name = 'Test Contract IN', SoldtoAccount__c = accountIN1.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract 1');
        SVMXC__Service_Contract__c contractFR = new SVMXC__Service_Contract__c(RecordTypeId = serviceContractRT.Id, Name = 'Test Contract FR', SoldtoAccount__c = accountFR1.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract 1');
        List<SVMXC__Service_Contract__c> contractHeaderList = new List<SVMXC__Service_Contract__c>();
        contractHeaderList.add(contractUS);
        contractHeaderList.add(contractIN);
        contractHeaderList.add(contractFR);
        
        insert contractHeaderList;
        
        
        List<SVMXC__Service_Contract__c> serviceLinesList = new List<SVMXC__Service_Contract__c>();
        
        SVMXC__Service_Contract__c contractUS2 = new SVMXC__Service_Contract__c(ParentContract__c = contractUS.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Contract US - SL1', SoldtoAccount__c = accountUS2.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract US - SL1');
        SVMXC__Service_Contract__c contractUS3 = new SVMXC__Service_Contract__c(ParentContract__c = contractUS.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Contract US - SL2', SoldtoAccount__c = accountUS3.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract US - SL2');
        //SVMXC__Service_Contract__c contractUS4 = new SVMXC__Service_Contract__c(ParentContract__c = contractUS.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Contract US - SL3', SoldtoAccount__c = accountUS4.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract US - SL3');
        ServiceLinesList.add(contractUS2);
        ServiceLinesList.add(contractUS3);
        
        
        SVMXC__Service_Contract__c contractIN2 = new SVMXC__Service_Contract__c(ParentContract__c = contractIN.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Contract IN - SL1', SoldtoAccount__c = accountIN2.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract IN - SL1');
        SVMXC__Service_Contract__c contractIN3 = new SVMXC__Service_Contract__c(ParentContract__c = contractIN.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Contract IN - SL2', SoldtoAccount__c = accountIN3.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract IN - SL2');
       
        ServiceLinesList.add(contractIN2);
        ServiceLinesList.add(contractIN3);
       
        
        SVMXC__Service_Contract__c contractFR2 = new SVMXC__Service_Contract__c(ParentContract__c = contractFR.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Contract FR - SL1', SoldtoAccount__c = accountFR1.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract FR - SL1');
        SVMXC__Service_Contract__c contractFR3 = new SVMXC__Service_Contract__c(ParentContract__c = contractFR.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Contract FR - SL2', SoldtoAccount__c = accountFR2.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract FR - SL2');
        //SVMXC__Service_Contract__c contractFR4 = new SVMXC__Service_Contract__c(ParentContract__c = contractFR.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Contract FR - SL3', SoldtoAccount__c = accountFR1.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract FR - SL3');
        ServiceLinesList.add(contractFR2);
        ServiceLinesList.add(contractFR3);
        //ServiceLinesList.add(contractFR4);
        System.debug('#### Inserting serviceLinesList: '+serviceLinesList);
        insert serviceLinesList;
        System.debug('#### Inserted serviceLinesList: '+serviceLinesList);
        
        // Creating covered products
        SVMXC__Service_Contract_Products__c covProdUS2A = createCoveredProduct(contractUS2, ipUS2A);
        SVMXC__Service_Contract_Products__c covProdUS2B = createCoveredProduct(contractUS2, ipUS2B);
        SVMXC__Service_Contract_Products__c covProdUS3A = createCoveredProduct(contractUS3, ipUS3A);
        SVMXC__Service_Contract_Products__c covProdUS3B = createCoveredProduct(contractUS3, ipUS3B);
        SVMXC__Service_Contract_Products__c covProdIN2A = createCoveredProduct(contractIN2, ipIN2A);
        SVMXC__Service_Contract_Products__c covProdIN2B = createCoveredProduct(contractIN2, ipIN2B);
        SVMXC__Service_Contract_Products__c covProdIN3A = createCoveredProduct(contractIN3, ipIN3A);
        SVMXC__Service_Contract_Products__c covProdIN3B = createCoveredProduct(contractIN3, ipIN3B);
        SVMXC__Service_Contract_Products__c covProdFR2A = createCoveredProduct(contractFR2, ipFR2A);
        SVMXC__Service_Contract_Products__c covProdFR2B = createCoveredProduct(contractFR2, ipFR2B);
        SVMXC__Service_Contract_Products__c covProdFR3A = createCoveredProduct(contractFR3, ipFR3A);
        SVMXC__Service_Contract_Products__c covProdFR3B = createCoveredProduct(contractFR3, ipFR3B);
        //covProdUS2A.ShippedSerialNumber__c='131232';
       // update covProdUS2A;
        
        List<SVMXC__Service_Contract_Products__c> listOfCoveredProd = new List<SVMXC__Service_Contract_Products__c>();
        listOfCoveredProd.add(covProdUS2A);
        listOfCoveredProd.add(covProdUS3A);
        listOfCoveredProd.add(covProdUS3B);
        listOfCoveredProd.add(covProdIN2A);
        listOfCoveredProd.add(covProdIN3A);
        listOfCoveredProd.add(covProdIN3B);
        listOfCoveredProd.add(covProdFR2A);
        listOfCoveredProd.add(covProdFR3A);
        listOfCoveredProd.add(covProdFR3B);
        AP_IPAssociationHandler.checkAddressConsistency(listOfCoveredProd);
        AP_IPAssociationHandler.attachIPForConcurrentOrders(listOfCoveredProd);
        
        database.insert (listOfCoveredProd,false);
        
        listOfCoveredProd.clear();
        listOfCoveredProd.add(covProdUS2B);
        listOfCoveredProd.add(covProdIN2B);
        listOfCoveredProd.add(covProdFR2B);
        System.debug('#### Inserting listOfCoveredProd: '+listOfCoveredProd);
        database.insert (listOfCoveredProd,false);
        System.debug('#### Inserted listOfCoveredProd: '+listOfCoveredProd);
        
    }
    
    static testMethod void insertIPtoOppLines() {
        
        // Creating Opportunties
        Opportunity oppUS = Utils_TestMethods.createOpenOpportunity(accountUS1.Id);
        Opportunity oppIN = Utils_TestMethods.createOpenOpportunity(accountIN1.Id);
        Opportunity oppFR = Utils_TestMethods.createOpenOpportunity(accountFR1.Id);
        
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(oppUS);
        oppList.add(oppIN);
        oppList.add(oppFR);
        System.debug('#### Inserting oppList: '+oppList);
        insert oppList;
        System.debug('#### Inserted oppList: '+oppList);
        
        // Creating Opp Lines
        List<OPP_ProductLine__c> oppLinesList = new List<OPP_ProductLine__c>();
        OPP_ProductLine__c oppLineUS2 = Utils_TestMethods.createProductLine(oppUS.Id);
        OPP_ProductLine__c oppLineUS3 = Utils_TestMethods.createProductLine(oppUS.Id);
        OPP_ProductLine__c oppLineIN2 = Utils_TestMethods.createProductLine(oppIN.Id);
        OPP_ProductLine__c oppLineIN3 = Utils_TestMethods.createProductLine(oppIN.Id);
        OPP_ProductLine__c oppLineFR2 = Utils_TestMethods.createProductLine(oppFR.Id);
        OPP_ProductLine__c oppLineFR3 = Utils_TestMethods.createProductLine(oppFR.Id);
        
        oppLinesList.add(oppLineUS2);
        oppLinesList.add(oppLineUS3);
        oppLinesList.add(oppLineIN2);
        oppLinesList.add(oppLineIN3);
        oppLinesList.add(oppLineFR2);
        oppLinesList.add(oppLineFR3);
        
        System.debug('#### Inserting oppLinesList: '+oppLinesList);
        insert oppLinesList;
        System.debug('#### Inserted oppLinesList: '+oppLinesList);
        
        // Creating associated installed products
        Installed_product_on_Opportunity_Line__c covProdUS2A = associateIPtoOppLine(oppLineUS2, ipUS2A);
        Installed_product_on_Opportunity_Line__c covProdUS2B = associateIPtoOppLine(oppLineUS2, ipUS2B);
        Installed_product_on_Opportunity_Line__c covProdUS3A = associateIPtoOppLine(oppLineUS3, ipUS3A);
        Installed_product_on_Opportunity_Line__c covProdUS3B = associateIPtoOppLine(oppLineUS3, ipUS3B);
        Installed_product_on_Opportunity_Line__c covProdIN2A = associateIPtoOppLine(oppLineIN2, ipIN2A);
        Installed_product_on_Opportunity_Line__c covProdIN2B = associateIPtoOppLine(oppLineIN2, ipIN2B);
        Installed_product_on_Opportunity_Line__c covProdIN3A = associateIPtoOppLine(oppLineIN3, ipIN3A);
        Installed_product_on_Opportunity_Line__c covProdIN3B = associateIPtoOppLine(oppLineIN3, ipIN3B);
        Installed_product_on_Opportunity_Line__c covProdFR2A = associateIPtoOppLine(oppLineFR2, ipFR2A);
        Installed_product_on_Opportunity_Line__c covProdFR2B = associateIPtoOppLine(oppLineFR2, ipFR2B);
        Installed_product_on_Opportunity_Line__c covProdFR3A = associateIPtoOppLine(oppLineFR3, ipFR3A);
        Installed_product_on_Opportunity_Line__c covProdFR3B = associateIPtoOppLine(oppLineFR3, ipFR3B);
        
        List<Installed_product_on_Opportunity_Line__c> listOfCoveredProd = new List<Installed_product_on_Opportunity_Line__c>();
        listOfCoveredProd.add(covProdUS2A);
        listOfCoveredProd.add(covProdUS3A);
        listOfCoveredProd.add(covProdUS3B);
        listOfCoveredProd.add(covProdIN2A);
        listOfCoveredProd.add(covProdIN3A);
        listOfCoveredProd.add(covProdIN3B);
        listOfCoveredProd.add(covProdFR2A);
        listOfCoveredProd.add(covProdFR3A);
        listOfCoveredProd.add(covProdFR3B);
        AP_IPAssociationHandler.checkAddressConsistency(listOfCoveredProd );
        insert listOfCoveredProd;
        listOfCoveredProd.clear();
        listOfCoveredProd.add(covProdUS2B);
        listOfCoveredProd.add(covProdIN2B);
        listOfCoveredProd.add(covProdFR2B);
        insert listOfCoveredProd;
        covProdUS2B.Installed_Product__c = ipUS3A.Id;
        covProdIN2B.Installed_Product__c = ipIN3A.Id;
        covProdFR2B.Installed_Product__c = ipFR3A.Id;
        listOfCoveredProd.clear();
        listOfCoveredProd.add(covProdUS2B);
        listOfCoveredProd.add(covProdIN2B);
        listOfCoveredProd.add(covProdFR2B);
        Database.SaveResult[] srList = Database.update(listOfCoveredProd, false);
    }
    
    /*static testMethod void processConcurrentOrders() {
        
        // Creating Service Contracts Headers
        SVMXC__Service_Contract__c contractConcurrentOrder1 = new SVMXC__Service_Contract__c(RecordTypeId = serviceContractRT.Id, Name = 'Test Concurrent Order Contract US', SoldtoAccount__c = accountUS1.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract 1');
        SVMXC__Service_Contract__c contractConcurrentOrder2 = new SVMXC__Service_Contract__c(RecordTypeId = serviceContractRT.Id, Name = 'Test Concurrent Order Contract IN', SoldtoAccount__c = accountIN1.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract 1');
        List<SVMXC__Service_Contract__c> contractHeaderList = new List<SVMXC__Service_Contract__c>();
        contractHeaderList.add(contractConcurrentOrder1);
        contractHeaderList.add(contractConcurrentOrder2);
        System.debug('#### Inserting contractHeaderList: '+contractHeaderList);
        insert contractHeaderList;
        System.debug('#### Inserted contractHeaderList: '+contractHeaderList);
        
        List<SVMXC__Service_Contract__c> serviceLinesList = new List<SVMXC__Service_Contract__c>();
        
        SVMXC__Service_Contract__c contractConcurrentOrder12 = new SVMXC__Service_Contract__c(ParentContract__c = contractConcurrentOrder1.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Concurrent Order Contract US - SL1', SoldtoAccount__c = accountUS2.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract US - SL1');
        SVMXC__Service_Contract__c contractConcurrentOrder13 = new SVMXC__Service_Contract__c(ParentContract__c = contractConcurrentOrder1.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Concurrent Order Contract US - SL2', SoldtoAccount__c = accountUS3.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract US - SL2');
        //SVMXC__Service_Contract__c contractConcurrentOrder14 = new SVMXC__Service_Contract__c(ParentContract__c = contractConcurrentOrder1.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Contract US - SL3', SoldtoAccount__c = accountUS4.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract US - SL3');
        ServiceLinesList.add(contractConcurrentOrder12);
        ServiceLinesList.add(contractConcurrentOrder13);
        //ServiceLinesList.add(contractConcurrentOrder14);
        
        SVMXC__Service_Contract__c contractConcurrentOrder22 = new SVMXC__Service_Contract__c(ParentContract__c = contractConcurrentOrder2.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Concurrent Order Contract IN - SL1', SoldtoAccount__c = accountIN2.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract IN - SL1');
        SVMXC__Service_Contract__c contractConcurrentOrder23 = new SVMXC__Service_Contract__c(ParentContract__c = contractConcurrentOrder2.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Concurrent Order Contract IN - SL2', SoldtoAccount__c = accountIN3.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract IN - SL2');
        //SVMXC__Service_Contract__c contractConcurrentOrder24 = new SVMXC__Service_Contract__c(ParentContract__c = contractConcurrentOrder2.Id, RecordTypeId = serviceLineRT.Id, Name = 'Test Contract IN - SL3', SoldtoAccount__c = accountIN4.Id, SVMXC__Start_Date__c = System.today()+7, SVMXC__End_Date__c = System.today()+365+7, SVMXC__Service_Contract_Notes__c = 'Test Contract IN - SL3');
        ServiceLinesList.add(contractConcurrentOrder22);
        ServiceLinesList.add(contractConcurrentOrder23);
        //ServiceLinesList.add(contractConcurrentOrder24);
        
        System.debug('#### Inserting serviceLinesList: '+serviceLinesList);
        insert serviceLinesList;
        System.debug('#### Inserted serviceLinesList: '+serviceLinesList);
        
        // Creating covered products
        SVMXC__Service_Contract_Products__c covProdUS2A = createCoveredProduct(contractConcurrentOrder12, ipUS2A);
        SVMXC__Service_Contract_Products__c covProdUS2B = createCoveredProduct(contractConcurrentOrder12, null);
        SVMXC__Service_Contract_Products__c covProdUS3A = createCoveredProduct(contractConcurrentOrder13, null);
        SVMXC__Service_Contract_Products__c covProdUS3B = createCoveredProduct(contractConcurrentOrder13, null);
        SVMXC__Service_Contract_Products__c covProdIN2A = createCoveredProduct(contractConcurrentOrder22, null);
        SVMXC__Service_Contract_Products__c covProdIN2B = createCoveredProduct(contractConcurrentOrder22, ipIN2B);
        SVMXC__Service_Contract_Products__c covProdIN3A = createCoveredProduct(contractConcurrentOrder23, ipIN3A);
        SVMXC__Service_Contract_Products__c covProdIN3B = createCoveredProduct(contractConcurrentOrder23, null);
        
        // Now setting some shipment Serial Numbers and Products befor launching the batch
        covProdUS2B.ShippedSerialNumber__c = ipUS2B.SVMXC__Serial_Lot_Number__c;
        covProdUS2B.SVMXC__Product__c = ipUS2B.SVMXC__Product__c;
        
        covProdUS3A.ShippedSerialNumber__c = ipUS3A.SVMXC__Serial_Lot_Number__c;
        //covProdUS3A.SVMXC__Product__c = ipUS3A.SVMXC__Product__c;
        
        //covProdUS3B.ShippedSerialNumber__c = ipUS3B.SVMXC__Serial_Lot_Number__c;
        covProdUS3B.SVMXC__Product__c = ipUS3B.SVMXC__Product__c;
        
        covProdIN2A.ShippedSerialNumber__c = ipIN2A.SVMXC__Serial_Lot_Number__c;
        covProdIN2A.SVMXC__Product__c = ipIN2A.SVMXC__Product__c;
        
        covProdIN3B.ShippedSerialNumber__c = ipIN3B.SVMXC__Serial_Lot_Number__c;
        covProdIN3B.SVMXC__Product__c = ipIN3B.SVMXC__Product__c;
        
        List<SVMXC__Service_Contract_Products__c> listOfCoveredProd = new List<SVMXC__Service_Contract_Products__c>();
        listOfCoveredProd.add(covProdUS2A);
        listOfCoveredProd.add(covProdUS2B);
        listOfCoveredProd.add(covProdUS3A);
        listOfCoveredProd.add(covProdUS3B);
        listOfCoveredProd.add(covProdIN2A);
        listOfCoveredProd.add(covProdIN2B);
        listOfCoveredProd.add(covProdIN3A);
        listOfCoveredProd.add(covProdIN3B);
        System.debug('#### Inserting listOfCoveredProd: '+listOfCoveredProd);
        database.insert(listOfCoveredProd,false);
        System.debug('#### Inserted listOfCoveredProd: '+listOfCoveredProd);
        
        // Now launching the Batch to process the IP Association
        System.debug('#### Launching batch');
        Test.startTest();
       // BatchSRVConcurrentOrderIPAssignment batch = new BatchSRVConcurrentOrderIPAssignment ();
        //ID batchprocessid = Database.executeBatch(batch);


        Test.stopTest();        

    }*/
    
    private static void prepareCommonData() {
        countryFR = new Country__c(Name='France', CountryCode__c='FR');
        countryUS = new Country__c(Name='USA', CountryCode__c='US');
        countryIN = new Country__c(Name='India', CountryCode__c='IN');
        
        List<Country__c> countryList = new List<Country__c>();
        countryList.add(countryFR);
        countryList.add(countryUS);
        countryList.add(countryIN);
        
        System.debug('#### Upserting countryList: '+countryList);
        upsert countryList CountryCode__c;
        System.debug('#### Upserted countryList: '+countryList);
        
        spUS_CA = new StateProvince__c(Name='California', Country__c=countryUS.Id, CountryCode__c=countryUS.countryCode__c, StateProvinceCode__c='CA', StateProvinceExternalId__c='USCA');
        spUS_RI = new StateProvince__c(Name='Rhode Island', Country__c=countryUS.Id, CountryCode__c=countryUS.countryCode__c, StateProvinceCode__c='RI', StateProvinceExternalId__c='USRI');
        
        spIN_10 = new StateProvince__c(Name='Karnataka', Country__c=countryIN.Id, CountryCode__c=countryIN.countryCode__c, StateProvinceCode__c='10', StateProvinceExternalId__c='IN10');
        spIN_30 = new StateProvince__c(Name='Delhi', Country__c=countryIN.Id, CountryCode__c=countryIN.countryCode__c, StateProvinceCode__c='30', StateProvinceExternalId__c='IN30');
        
        List<StateProvince__c> spList = new List<StateProvince__c>();
        spList.add(spUS_CA);
        spList.add(spUS_RI);
        spList.add(spIN_10);
        spList.add(spIN_30);
        
        System.debug('#### Upserting spList: '+spList);
        upsert spList StateProvinceExternalId__c;
        System.debug('#### Upserted spList: '+spList);
        
        accountUS1 = createAccount(countryUS, spUS_CA, 'Los Angeles');
        accountUS2 = createAccount(countryUS, spUS_CA, 'San Francisco');
        accountUS3 = createAccount(countryUS, spUS_RI, 'West Kingston');
        accountUS4 = createAccount(countryUS, spUS_RI, 'South Kingstown');
        
        accountIN1 = createAccount(countryIN, spIN_10, 'Bengaluru');
        accountIN2 = createAccount(countryIN, spIN_10, 'Mysuru');
        accountIN3 = createAccount(countryIN, spIN_30, 'New Delhi');
        accountIN4 = createAccount(countryIN, spIN_30, 'Mysuru');
        
        accountFR1 = createAccount(countryFR, null, 'Paris');
        accountFR2 = createAccount(countryFR, null, 'Grenoble');
        
        List<Account> accountList = new List<Account>();
        accountList.add(accountUS1);
        accountList.add(accountUS2);
        accountList.add(accountUS3);
        accountList.add(accountUS4);
        accountList.add(accountIN1);
        accountList.add(accountIN2);
        accountList.add(accountIN3);
        accountList.add(accountIN4);
        accountList.add(accountFR1);
        accountList.add(accountFR2);
        System.debug('#### Inserting accountList: '+accountList);
        insert accountList;
        System.debug('#### Inserted accountList: '+accountList);
        
        brand1 = createBrand('Pipo');
        System.debug('#### Inserting brand1: '+brand1);
        insert brand1;
        System.debug('#### Inserted brand1: '+brand1);
        
        // Create Range
        
        // Create Device Type
        deviceType1 = createDeviceType('Toto device type');
        System.debug('#### Inserting deviceType1: '+deviceType1);
        insert deviceType1;
        System.debug('#### Inserted deviceType1: '+deviceType1);
        
        // Create Products
        product1 = createProduct('Product 1', 'SKU1', brand1, null, deviceType1);
        product2 = createProduct('Product 2', 'SKU2', brand1, null, deviceType1);
        product3 = createProduct('Product 3', 'SKU3', brand1, null, deviceType1);
        List<Product2> listOfProducts = new List<Product2>();
        listOfProducts.add(product1);
        listOfProducts.add(product2);
        listOfProducts.add(product3);
        System.debug('#### Inserting products: '+listOfProducts);
        insert listOfProducts;
        System.debug('#### Inserted products: '+listOfProducts);
        
        // Creating Installed Products
        ipUS2A = createIP('IP US 2A', accountUS2, brand1, null, deviceType1);
        ipUS2B = createIP('IP US 2B', accountUS2, brand1, null, deviceType1);
        ipUS2C = createIP('IP US 2C', accountUS2, brand1, null, deviceType1);
        
        ipUS3A = createIP('IP US 3A', accountUS3, brand1, null, deviceType1);
        ipUS3B = createIP('IP US 3B', accountUS3, brand1, null, deviceType1);
        
        ipIN2A = createIP('IP IN 2A', accountIN2, brand1, null, deviceType1);
        ipIN2B = createIP('IP IN 2B', accountIN2, brand1, null, deviceType1);
        ipIN2C = createIP('IP IN 2C', accountIN2, brand1, null, deviceType1);
        
        ipIN3A = createIP('IP IN 3A', accountIN3, brand1, null, deviceType1);
        ipIN3B = createIP('IP IN 3B', accountIN3, brand1, null, deviceType1);
        
        ipFR2A = createIP('IP FR 2A', accountFR1, brand1, null, deviceType1);
        ipFR2B = createIP('IP FR 2B', accountFR1, brand1, null, deviceType1);
        ipFR2C = createIP('IP FR 2C', accountFR1, brand1, null, deviceType1);
        
        ipFR3A = createIP('IP FR 3A', accountFR2, brand1, null, deviceType1);
        ipFR3B = createIP('IP FR 3B', accountFR2, brand1, null, deviceType1);
        
        ipUS2B.SVMXC__Serial_Lot_Number__c = 'US-2B-12345';
        ipUS2B.SVMXC__Product__c = product1.Id;
        
        ipUS3A.SVMXC__Serial_Lot_Number__c = 'US-3A-67890';
        ipUS3A.SVMXC__Product__c = product2.Id;
        
        ipUS3B.SVMXC__Serial_Lot_Number__c = 'US-3B-54321';
        ipUS3B.SVMXC__Product__c = product3.Id;
        
        ipIN2A.SVMXC__Serial_Lot_Number__c = 'IN-XX-12345';
        ipIN2A.SVMXC__Product__c = product2.Id;
        
        ipIN3B.SVMXC__Serial_Lot_Number__c = 'IN-XX-12345';
        ipIN3B.SVMXC__Product__c = product3.Id;
        
        List<SVMXC__Installed_Product__c> listOfIPs = new List<SVMXC__Installed_Product__c>();
        listOfIPs.add(ipUS2A);
        listOfIPs.add(ipUS2B);
        listOfIPs.add(ipUS2C);
        listOfIPs.add(ipUS3A);
        listOfIPs.add(ipUS3B);
        listOfIPs.add(ipIN2A);
        listOfIPs.add(ipIN2B);
        listOfIPs.add(ipIN2C);
        listOfIPs.add(ipIN3A);
        listOfIPs.add(ipIN3B);
        listOfIPs.add(ipFR2A);
        listOfIPs.add(ipFR2B);
        listOfIPs.add(ipFR2C);
        listOfIPs.add(ipFR3A);
        listOfIPs.add(ipFR3B);
        System.debug('#### Inserting listOfIPs: '+listOfIPs);
        database.insert (listOfIPs,false);
        System.debug('#### Inserted listOfIPs: '+listOfIPs);
        
    }
    
    private static Brand__c createBrand(String aBrand) {
        return new Brand__c(Name=aBrand);
    }
    
    private static DeviceType__c createDeviceType(String aDeviceTypeName) {
        return new DeviceType__c(SDHDEVICETYPEID__c=aDeviceTypeName);
    }
    
    private static Category__c createRange(String aRangeName) {
        return new Category__c(Name=aRangeName, SDHCategoryID__c=aRangeName);
    }
    
    private static Product2 createProduct(String aName, String aSKU,Brand__c aBrand, Category__c aRange, DeviceType__c aDeviceType) {
        Product2 result = new Product2(Name = aName, SKU__c = aSKU);
        if (aBrand != null) {
            result.Brand2__c=aBrand.Id;
        }
        if (aRange != null) {
            result.CategoryId__c=aRange.Id;
        }
        if (aDeviceType != null) {
            result.DeviceType2__c=aDeviceType.Id;
        }
        return result;
    }
    
    private static SVMXC__Installed_Product__c createIP(String aName, Account anInstalledAt, Brand__c aBrand, Category__c aRange, DeviceType__c aDeviceType) {
        SVMXC__Installed_Product__c result = null;
        if(anInstalledAt != null) {
            result = new SVMXC__Installed_Product__c(Name = aName, SVMXC__Company__c = anInstalledAt.Id);
        }
        if (aBrand != null) {
            result.Brand2__c = aBrand.Id;
        }
        if (aRange != null) {
            result.Category__c = aRange.Id;
        }
        if (aDeviceType != null) {
            result.DeviceType2__c=aDeviceType.Id;
        }
        return result;
    }
    
    private static SVMXC__Service_Contract_Products__c createCoveredProduct(SVMXC__Service_Contract__c aServiceContract, SVMXC__Installed_Product__c anIP) {
        SVMXC__Service_Contract_Products__c result = new SVMXC__Service_Contract_Products__c();
        if (aServiceContract != null && aServiceContract.Id != null) {
            result.SVMXC__Service_Contract__c = aServiceContract.Id;
        }
        if (anIP != null && anIP.Id != null) {
            result.SVMXC__Installed_Product__c = anIP.Id;
        }
        return result;
    }    
    
    private static Installed_product_on_Opportunity_Line__c associateIPtoOppLine(OPP_ProductLine__c anOppLine, SVMXC__Installed_Product__c anIP) {
        Installed_product_on_Opportunity_Line__c result = new Installed_product_on_Opportunity_Line__c(Opportunity_Line__c = anOppLine.Id, Installed_Product__c = anIP.Id);
        return result;
    }    
    
    private static Account createAccount(Country__c aCountry, StateProvince__c aState, String aCity) {
        Account result = null;
        result = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', City__c = aCity);
        if (aCountry != null){
            result.Country__c = aCountry.Id;
        }
        if (aState != null){
            result.StateProvince__c = aState.Id;
        }
        return result;
    }
    
}