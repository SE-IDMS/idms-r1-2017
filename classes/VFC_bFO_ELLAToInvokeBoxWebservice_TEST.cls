@istest
public class VFC_bFO_ELLAToInvokeBoxWebservice_TEST{
  
   
    
     @testSetup static void setupOffer() {
        
         User newUser = Utils_TestMethods.createStandardUser('yhrgiusd');
            Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
            newUser.BypassVR__c = TRUE;

            newUser.UserRoleID = rol_ID ;
            newUser.BypassTriggers__c='AP_Offer1;AP_Offer3';    
          //  Database.SaveResult UserInsertResult = Database.insert(newUser, false);  
        // system.runas(newUser) {
             Test.startTest();
            CS_ELLABoxAccessToken__c cs = new CS_ELLABoxAccessToken__c(Name='Admin',AccessToken__c='DprxSfbropcMvXDOiftQOQYHe2ko516u',
            RefreshToken__c ='ub2Kfgp6mBqQjixI6RWcNVnf45z2CrGz3HJ729JUlahDC8GEvWuu9kS0x1qz6qMO' ,
            BoxAccessTokenExpires__c=datetime.now().addMinutes(45),BoxRefreshTokenExpire__c=datetime.now().adddays(60) );

            insert cs;
            
        List<offer_Lifecycle__c> offerRecList = new  List<offer_Lifecycle__c>();  
          Milestone1_Project__c projRec =new Milestone1_Project__c ();
           
           Milestone1_Project__c projRec1 =new Milestone1_Project__c ();
            offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
            offerRec.BoxFolderId__c = '12345';
             offerRec.Offer_Name__c='test123457eee';
            offerRec.BoxUserEmailId__c = 'abc@schneider-electric.com';
            
            
            offer_Lifecycle__c offerRec1 = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
            offerRec1.BoxFolderId__c = '12334';
            offerRec1.Offer_Name__c='test123457';
            offerRec1.BoxUserEmailId__c = 'abc@schneider-electric.com';
            
            offerRecList.add(offerRec);
            offerRecList.add(offerRec1);
            Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST()); 
            Utils_SDF_Methodology.removeFromRunOnce('AP_Offer1');
            Utils_SDF_Methodology.removeFromRunOnce('AP_Offer3');
            insert offerRecList;
            Utils_SDF_Methodology.removeFromRunOnce('AP_Offer1');
            Utils_SDF_Methodology.removeFromRunOnce('AP_Offer3');
            Utils_SDF_Methodology.removeFromRunOnce('AP_OrgBfInsert');
            
            country__c c = new country__c(name = 'TestCountryHP',countrycode__c = 'ZZp');
            insert c;
            projRec = AP_EllaProjectForecast_TEST.createProject(offerRecList[0].Id, c.Id, newUser.Id);
            //projRec.BoxFolderId__c = '';
            projRec.BoxUserEmailId__c = 'abc@schneider-electric.com';
            //insert projRec;
            string folderoffid;
            if(offerRecList[1].BoxFolderId__c!=null ) {
                folderoffid=offerRecList[1].Id;
            }
            if(offerRecList[0].BoxFolderId__c!=null) {
                folderoffid=offerRecList[0].Id; 
                
            }
          
            
             projRec = AP_EllaProjectForecast_TEST.createProject(offerRecList[0].Id, c.Id, newUser.Id);
           // projRec.BoxFolderId__c = '12334';
            projRec.name='Test Project3909';
            projRec.BoxUserEmailId__c = 'abc@schneider-electric.com';
            insert projRec;
            
             projRec1 = AP_EllaProjectForecast_TEST.createProject(offerRecList[0].Id, c.Id, newUser.Id);
            projRec.BoxFolderId__c = '12334';
            projRec1.name='Test Project390912343';
            projRec1.BoxUserEmailId__c = 'abc@schneider-electric.com';
            insert projRec1;
            
            
            
            
            
             Test.stopTest();
      //  }    
    }
    
    
    @istest 
   static void folderCreationwithoutInsert_BoxWebservice(){
        
           //system.runas(newUserUpd){ 
            Test.startTest(); 
             set<String> projectId = new set<String>();
            string withBoxid;
            string notwithBoxid;
           
            
            List<Milestone1_Project__c> projectRec = [select id from Milestone1_Project__c where  BoxUserEmailId__c!=null];
          
            VFC_bFO_ELLAToInvokeBoxWebservice  testRun = new  VFC_bFO_ELLAToInvokeBoxWebservice();  
           
          
         
            //projects
            ApexPages.currentPage().getParameters().put(label.CLAPR15ELLAbFO25,label.CLAPR15ELLAbFO22);//'CBRProject'
            ApexPages.currentPage().getParameters().put(label.CLAPR15ELLAbFO24,projectRec[0].id);//'fdrProject'
            Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());  
            testRun.InvokeBoxAPICall();//..addOfferBoxCollaborater
            //projects
            ApexPages.currentPage().getParameters().put(label.CLAPR15ELLAbFO25,label.CLAPR15ELLAbFO20);//'CBRProject'
            ApexPages.currentPage().getParameters().put(label.CLAPR15ELLAbFO24,projectRec[0].id);//'fdrProject'
            Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());  
            testRun.InvokeBoxAPICall();//..addOfferBoxCollaborate
              Test.stopTest(); 
          
           
            
        
        
    }
    
    @istest 
   static void folderCreationwithoutInsert_BoxWebservice12(){
        
           //system.runas(newUserUpd){ 
            Test.startTest(); 
             set<String> projectId = new set<String>();
            string withBoxid;
            string notwithBoxid;
           
            
            List<Milestone1_Project__c> projectRec = [select id from Milestone1_Project__c limit 4];
          
            VFC_bFO_ELLAToInvokeBoxWebservice  testRun = new  VFC_bFO_ELLAToInvokeBoxWebservice();  
           
          
         
            //projects
            ApexPages.currentPage().getParameters().put(label.CLAPR15ELLAbFO25,label.CLAPR15ELLAbFO22);//'CBRProject'
            ApexPages.currentPage().getParameters().put(label.CLAPR15ELLAbFO24,projectRec[0].id);//'fdrProject'
            Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());  
            testRun.InvokeBoxAPICall();//..addOfferBoxCollaborater
            //projects
            ApexPages.currentPage().getParameters().put(label.CLAPR15ELLAbFO25,label.CLAPR15ELLAbFO20);//'CBRProject'
            ApexPages.currentPage().getParameters().put(label.CLAPR15ELLAbFO24,projectRec[0].id);//'fdrProject'
            Test.setMock(HttpCalloutMock.class, new AP_BoxConnectImpl_TEST());  
            testRun.InvokeBoxAPICall();//..addOfferBoxCollaborate
              Test.stopTest(); 
          
           
            
        
        
    }
    
    
}