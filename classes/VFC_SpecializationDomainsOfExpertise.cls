public with sharing class VFC_SpecializationDomainsOfExpertise {
    
    public list<SpecializationDomainsOfExpertise__c> lstDomainsOfExpertiseLvl1 {get;set;}
   
    public VFC_SpecializationDomainsOfExpertise(ApexPages.StandardController controller) {
        //lstDomainsOfExpertiseLvl1 = [Select DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c from SpecializationDomainsOfExpertise__c where Specialization__c=:controller.getId() AND  DomainsOfExpertiseCatalog__r.ParentDomainsOfExpertise__c =null order by DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c limit 1000]; 
        lstDomainsOfExpertiseLvl1 = [Select DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c,  DomainsOfExpertiseCatalog__r.name from SpecializationDomainsOfExpertise__c where Specialization__c=:controller.getId() order by DomainsOfExpertiseCatalog__r.name limit 1000]; 
     
    }
}