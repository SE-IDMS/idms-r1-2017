Public class Ap_sitelocation
{
    Public static void VIPCertificaion(set<id> accset,set<id> locset)
    {

        String LocationRT = System.Label.CLJUN16SRV03;
        List<account>acclist= new List<account>();
        list<SVMXC__Skill__c> skilllist= new list<SVMXC__Skill__c> ();
            set<string>countryset= new set<string>();
            list<HabilitationRequirement__c> loccertlist= new list<HabilitationRequirement__c>();
            list<SVMXC__Site__c> loclist= new list<SVMXC__Site__c>();
            Map<id,HabilitationRequirement__c> locmap= new Map<id,HabilitationRequirement__c> ();
            
            if(locset!=null)
                loclist=[select id,name from SVMXC__Site__c where id in:locset];
        if(accset!=null)
        acclist=[select id,name,Account_VIPFlag__c,Country__c from account where id in:accset];
    
        for(account acc:acclist){
        
            if(acc.Account_VIPFlag__c=='VIP1' &&acc.Country__c!=null)
                    countryset.add(acc.Country__c);
    
        }
        skilllist = [select id,name,Country__c,SVMXC__Active__c from SVMXC__Skill__c where Country__c in:countryset and name='VIP_CUSTOMER' and SVMXC__Active__c=true];
            if(skilllist!=null)
                    for(SVMXC__Skill__c skill:skilllist)
                    {
                
                        HabilitationRequirement__c loccert= new HabilitationRequirement__c();
                                loccert.Skill__c=skill.id;
                                loccert.Country__c=skill.Country__c;
                                loccert.Location__c=loclist[0].id;
                                loccert.recordtypeid=LocationRT;
                                loccertlist.add(loccert);
                    }
                              
           if(loccertlist!=null)
                insert loccertlist;
    }
    Public static void VIPCertificaionfromacc(list<account> acclist)
    {
        Map<id,Account> accMap = new Map<id,Account>();
        
        If(acclist != null && acclist.size()>0){
            accMap.putAll(acclist );
            
            String LocationRT = System.Label.CLJUN16SRV03;
            list<SVMXC__Site__c> loclist= new list<SVMXC__Site__c>();
            list<SVMXC__Skill__c> skilllist= new list<SVMXC__Skill__c> ();
            list<HabilitationRequirement__c> loccertlist= new list<HabilitationRequirement__c>();
            set<string> countryset= new set<string>();
            
            loclist=[select id,name,PrimaryLocation__c from SVMXC__Site__c where SVMXC__Account__c in :accMap.keySet() and PrimaryLocation__c=true];
            if(loclist!=null && loclist.size()>0)
            {
                for(account acc:acclist)
                {
                    if(acc.Country__c!=null)
                    countryset.add(acc.Country__c);
                }
                if(countryset!=null)
                    skilllist = [select id,name,Country__c,SVMXC__Active__c from SVMXC__Skill__c where Country__c in:countryset and name='VIP_CUSTOMER' and SVMXC__Active__c=true];
                    if(skilllist!=null)
                        for(SVMXC__Skill__c skill:skilllist)
                        {
                            HabilitationRequirement__c loccert= new HabilitationRequirement__c();
                                    loccert.Skill__c=skill.id;
                                    loccert.Country__c=skill.Country__c;
                                    loccert.Location__c=loclist[0].id;
                                    loccert.recordtypeid=LocationRT;
                                     loccertlist.add(loccert);
                        }
            }
            if(loccertlist!=null)
                insert loccertlist;
        }
    }
}