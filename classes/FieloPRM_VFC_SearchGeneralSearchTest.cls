@IsTest
global class FieloPRM_VFC_SearchGeneralSearchTest {
    static testMethod void testGSASearch(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
            System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
            insert c;

            PRMCountry__c prmCountry = new PRMCountry__c(Country__c=c.Id,PLDataPool__c='en_US2',SearchImplementation__c='GSA');
            insert prmCountry;
            
            Account acc= new Account(
                Name = 'test',
                Street__c = 'Some Street',
                ZipCode__c = '012345',
                PLDataPool__c ='en_US2'
            );
            insert acc;
                    
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test',
                FieloEE__LastName__c= 'test',
                F_Country__c = c.Id,
                F_PRM_Country__c=prmCountry.Id
            );
            insert mem;
        
            FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
            insert memb;
            
            FieloEE.MemberUtil.setmemberId(mem.Id);


            FieloPRM_VFC_SearchGeneralSearch myController = new FieloPRM_VFC_SearchGeneralSearch();
            System.assertEquals(FieloPRM_VFC_SearchGeneralSearch.GSA_IMPLEMENTATION,myController.searchImplementation);
            Test.setMock(HttpCalloutMock.class, new MockSearchHttpResponseGenerator(200,
                                                     'Complete',
                                                     '{"autnresponse":{"xmlns:autn":"http://schemas.autonomy.com/aci/","action":"QUERY","responsedata":{"autn_numhits":10,"autn_hit":[{"autn_title":"Altivar 1000 - Schneider Electric Россия","autn_weight":58.25,"autn_id":1517926,"autn_links":"ALTIVAR*","autn_section":0,"autn_content":{"DOCUMENT":{"KEYWORDS":"Altivar 1000 - Частотнорегулируемый привод среднего напряжения мощностью от 0.5 до 10 MВт","URL":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2920-komplektnye-pc-nv-i-vv/2249-altivar-1000/"}},"autn_database":"OD_RU_RU","autn_reference":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2920-komplektnye-pc-nv-i-vv/2249-altivar-1000/"},{"autn_title":"Altivar 12 - ATV 12 - Schneider Electric Россия","autn_weight":58.25,"autn_id":1517938,"autn_links":"ALTIVAR*","autn_section":0,"autn_content":{"DOCUMENT":{"KEYWORDS":"Altivar 12 - ATV 12 - Преобразователь частоты Altivar 12 для 3-х фазных асинхронных электродвигателей мощностью от 0.18 до 4 кВт","URL":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2905-kompaktnye-pc/2253-altivar-12-atv-12/"}},"autn_database":"OD_RU_RU","autn_reference":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2905-kompaktnye-pc/2253-altivar-12-atv-12/"},{"autn_title":"Altivar 212 - Schneider Electric Россия","autn_weight":58.25,"autn_id":1520623,"autn_links":"ALTIVAR*","autn_section":0,"autn_content":{"DOCUMENT":{"KEYWORDS":"Altivar 212 - Преобразователь частоты для систем HVAC (вентиляторы и насосы) на мощности от 0,75 до 75 кВт","URL":"http://www.schneider-electric.com/products/ru/ru/1200-avtomatizacia-i-bezopasnost-zdanij/1265-specializirovannye-pc/60162-altivar-212/"}},"autn_database":"OD_RU_RU","autn_reference":"http://www.schneider-electric.com/products/ru/ru/1200-avtomatizacia-i-bezopasnost-zdanij/1265-specializirovannye-pc/60162-altivar-212/"},{"autn_title":"Altivar 312 - Schneider Electric Россия","autn_weight":58.25,"autn_id":1520069,"autn_links":"ALTIVAR*","autn_section":0,"autn_content":{"DOCUMENT":{"KEYWORDS":"Altivar 312 - Преобразователь частоты Altivar 312 для 3-х фазных асинхронных электродвигателей мощностью от 0.18 до 15 кВт","URL":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2905-kompaktnye-pc/2656-altivar-312/"}},"autn_database":"OD_RU_RU","autn_reference":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2905-kompaktnye-pc/2656-altivar-312/"},{"autn_title":"Altivar 31C - Schneider Electric Россия","autn_weight":58.25,"autn_id":1517920,"autn_links":"ALTIVAR*","autn_section":0,"autn_content":{"DOCUMENT":{"KEYWORDS":"Altivar 31C - Преобразователь частоты IP54 от 0,18 до 15 кВт","URL":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2945-specializirovannye-pc/7608-altivar-31c/"}},"autn_database":"OD_RU_RU","autn_reference":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2945-specializirovannye-pc/7608-altivar-31c/"},{"autn_title":"Altivar 32 - Schneider Electric Россия","autn_weight":58.25,"autn_id":1517942,"autn_links":"ALTIVAR*","autn_section":0,"autn_content":{"DOCUMENT":{"KEYWORDS":"Altivar 32 - Преобразователь частоты для 3-х фазных асинхронных и синхронных электродвигателей на мощности от 0,18 до 15кВт","URL":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2905-kompaktnye-pc/7609-altivar-32/"}},"autn_database":"OD_RU_RU","autn_reference":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2905-kompaktnye-pc/7609-altivar-32/"},{"autn_title":"Altivar 61 - Schneider Electric Россия","autn_weight":58.25,"autn_id":1520617,"autn_links":"ALTIVAR*","autn_section":0,"autn_content":{"DOCUMENT":{"KEYWORDS":"Altivar 61 - Преобразователь частоты Altivar 61 для 3-х фазных асинхронных электродвигателей мощностью от 0.75 до 800 кВт","URL":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2950-standartnye-pc/1422-altivar-61/"}},"autn_database":"OD_RU_RU","autn_reference":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2950-standartnye-pc/1422-altivar-61/"},{"autn_title":"Altivar 61 Plus - Schneider Electric Россия","autn_weight":58.25,"autn_id":1519391,"autn_links":"ALTIVAR*","autn_section":0,"autn_content":{"DOCUMENT":{"KEYWORDS":"Altivar 61 Plus - Преобразователь частоты от 90 до 2400 кВт","URL":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2920-komplektnye-pc-nv-i-vv/2374-altivar-61-plus/"}},"autn_database":"OD_RU_RU","autn_reference":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2920-komplektnye-pc-nv-i-vv/2374-altivar-61-plus/"},{"autn_title":"Altivar 61Q - Schneider Electric Россия","autn_weight":58.25,"autn_id":1517914,"autn_links":"ALTIVAR*","autn_section":0,"autn_content":{"DOCUMENT":{"KEYWORDS":"Altivar 61Q - Преобразователь частоты с водяным охлаждением от 110 до 800 кВт","URL":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2945-specializirovannye-pc/60844-altivar-61q/"}},"autn_database":"OD_RU_RU","autn_reference":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2945-specializirovannye-pc/60844-altivar-61q/"},{"autn_title":"Altivar 71 - Schneider Electric Россия","autn_weight":58.25,"autn_id":1517927,"autn_links":"ALTIVAR*","autn_section":0,"autn_content":{"DOCUMENT":{"KEYWORDS":"Altivar 71 - Частотнорегулируемый привод Altivar 71 для 3-х фазных асинхронных электродвигателей мощностью от 0.75 до 630 кВт","URL":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2950-standartnye-pc/1155-altivar-71/"}},"autn_database":"OD_RU_RU","autn_reference":"http://www.schneider-electric.com/products/ru/ru/2900-privodnaa-tehnika/2950-standartnye-pc/1155-altivar-71/"}]},"response":"SUCCESS"}}',
                                                     null));
            Test.startTest();
            List<FieloPRM_VFC_SearchGeneralSearch.SearchItem> listItems = FieloPRM_VFC_SearchGeneralSearch.getAutocomplete('http://google.com','UTF-8','GSA','http://google.com','hello',false,'http://google.com','http://google.com','http://google.com');
            System.assertEquals(10,listItems.size());
            Test.stopTest();
        }
    }

    static testMethod void testGSSSearch(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
            System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
            insert c;

            PRMCountry__c prmCountry = new PRMCountry__c(Country__c=c.Id,PLDataPool__c='en_US2',SearchImplementation__c='GSS');
            insert prmCountry;
            
            Account acc= new Account(
                Name = 'test',
                Street__c = 'Some Street',
                ZipCode__c = '012345',
                PLDataPool__c ='en_US2'
            );
            insert acc;
                    
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test',
                FieloEE__LastName__c= 'test',
                F_Country__c = c.Id,
                F_PRM_Country__c=prmCountry.Id
            );
            insert mem;
        
            FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
            insert memb;
            
            FieloEE.MemberUtil.setmemberId(mem.Id);


            FieloPRM_VFC_SearchGeneralSearch myController = new FieloPRM_VFC_SearchGeneralSearch();
            System.assertEquals(FieloPRM_VFC_SearchGeneralSearch.GSS_IMPLEMENTATION,myController.searchImplementation);
            Test.setMock(HttpCalloutMock.class, new MockSearchHttpResponseGenerator(200,
                                                     'Complete',
                                                     '[{"id":"Altira","keywords":"Altira","redirectUrl":""},{"id":"Altistart 01","keywords":"Altistart 01","redirectUrl":""},{"id":"Altistart 22","keywords":"Altistart 22","redirectUrl":""},{"id":"Altivar 12","keywords":"Altivar 12","redirectUrl":""},{"id":"Altivar 32","keywords":"Altivar 32","redirectUrl":""},{"id":"Altivar 212","keywords":"Altivar 212","redirectUrl":""},{"id":"Altivar 312","keywords":"Altivar 312","redirectUrl":""},{"id":"Altivar 31C","keywords":"Altivar 31C","redirectUrl":""},{"id":"Altivar 600","keywords":"Altivar 600","redirectUrl":""},{"id":"Altivar IMC","keywords":"Altivar IMC","redirectUrl":""}]',
                                                     null));
            Test.startTest();
            List<FieloPRM_VFC_SearchGeneralSearch.SearchItem> listItems = FieloPRM_VFC_SearchGeneralSearch.getAutocomplete('http://google.com','UTF-8','GSS','http://google.com','hello',false,'http://google.com','http://google.com','http://google.com');
            System.assertEquals(10,listItems.size());
            Test.stopTest();
        }
    }

    static testMethod void testGSSSearchRedirect(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
            insert c;

            PRMCountry__c prmCountry = new PRMCountry__c(Country__c=c.Id,PLDataPool__c='en_US2',SearchImplementation__c='GSS');
            insert prmCountry;
            
            Account acc= new Account(
                Name = 'test',
                Street__c = 'Some Street',
                ZipCode__c = '012345',
                PLDataPool__c ='en_US2'
            );
            insert acc;
                    
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test',
                FieloEE__LastName__c= 'test',
                F_Country__c = c.Id,
                F_PRM_Country__c=prmCountry.Id
            );
            insert mem;
        
            FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
            insert memb;
            
            FieloEE.MemberUtil.setmemberId(mem.Id);
            Map<String, String> responseHeaders = new Map<String, String>{'Location'=>'http://akram.com'};
            Test.setMock(HttpCalloutMock.class, new MockSearchHttpResponseGenerator(301,
                                                     'Complete',
                                                     '',
                                                     responseHeaders));
            Test.startTest();
            List<FieloPRM_VFC_SearchGeneralSearch.SearchItem> listItems = FieloPRM_VFC_SearchGeneralSearch.getAutocomplete('http://google.com','UTF-8','GSS','http://google.com','hello',false,'http://google.com','http://google.com','http://google.com');
            System.assertEquals(0,listItems.size());
            Test.stopTest();
        }
    }


    static testMethod void testGSASearchRedirect(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
            insert c;

            PRMCountry__c prmCountry = new PRMCountry__c(Country__c=c.Id,PLDataPool__c='en_US2',SearchImplementation__c='GSA');
            insert prmCountry;
            
            Account acc= new Account(
                Name = 'test',
                Street__c = 'Some Street',
                ZipCode__c = '012345',
                PLDataPool__c ='en_US2'
            );
            insert acc;
                    
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test',
                FieloEE__LastName__c= 'test',
                F_Country__c = c.Id,
                F_PRM_Country__c=prmCountry.Id
            );
            insert mem;
        
            FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
            insert memb;
            
            FieloEE.MemberUtil.setmemberId(mem.Id);
            Map<String, String> responseHeaders = new Map<String, String>{'Location'=>'http://akram.com'};
            Test.setMock(HttpCalloutMock.class, new MockSearchHttpResponseGenerator(301,
                                                     'Complete',
                                                     '',
                                                     responseHeaders));
            Test.startTest();
            List<FieloPRM_VFC_SearchGeneralSearch.SearchItem> listItems = FieloPRM_VFC_SearchGeneralSearch.getAutocomplete('http://google.com','UTF-8','GSA','http://google.com','hello',false,'http://google.com','http://google.com','http://google.com');
            System.assertEquals(0,listItems.size());
            Test.stopTest();
        }
    }


    

    global class MockSearchHttpResponseGenerator implements HttpCalloutMock {
        protected Integer code;
        protected String status;
        protected String bodyAsString;
        protected Blob bodyAsBlob;
        protected Map<String, String> responseHeaders;

        public MockSearchHttpResponseGenerator(Integer code, String status, String body,
                                         Map<String, String> responseHeaders) {
            this.code = code;
            this.status = status;
            this.bodyAsString = body;
            this.bodyAsBlob = null;
            this.responseHeaders = responseHeaders;
        }

        public MockSearchHttpResponseGenerator(Integer code, String status, Blob body,
                                         Map<String, String> responseHeaders) {
            this.code = code;
            this.status = status;
            this.bodyAsBlob = body;
            this.bodyAsString = null;
            this.responseHeaders = responseHeaders;
        }

        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(code);
            resp.setStatus(status);
            if (bodyAsBlob != null) {
                resp.setBodyAsBlob(bodyAsBlob);
            } else {
                resp.setBody(bodyAsString);
            }

            if (responseHeaders != null) {
                 for (String key : responseHeaders.keySet()) {
                resp.setHeader(key, responseHeaders.get(key));
                 }
            }
            return resp;
        }
    }
}