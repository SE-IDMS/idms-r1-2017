/*
    Author          : Global Delivery Team - Bangalore
    Date Created    : 26/05/2011
    Description     : Test class for AP1003_CorrectiveAction class.
*/
@isTest
private class AP1003_CorrectiveAction_Test {

    static testMethod void testCorrectiveAction() {
        // TO DO: implement unit test
        
    User newUser1 = Utils_TestMethods.createStandardUser('CrrUser1');         
    Database.insert(newUser1);   
    
    User newUser2 = Utils_TestMethods.createStandardUser('CrrUser2');         
    Database.insert(newUser2);       
    List<BusinessRiskEscalationEntity__c> breEntityList =new List<BusinessRiskEscalationEntity__c>();
    List<EntityStakeHolder__c> entityStakeHolderList =new List<EntityStakeHolder__c>();
    BusinessRiskEscalationEntity__c breEntity1 = Utils_TestMethods.createBusinessRiskEscalationEntity();
     breEntity1.Name='Test Name3';
            breEntity1.Entity__c='Test Entity3';
    //BusinessRiskEscalationEntity__c breEntity2 = Utils_TestMethods.createBusinessRiskEscalationEntity();
    
    
    breEntityList.add(breEntity1);
    //breEntityList.add(breEntity2);
    Database.insert(breEntityList);
    
    EntityStakeholder__c stakeholder1 = Utils_TestMethods.createEntityStakeholder(breEntity1.Id,newUser1.Id,Label.CL00309);
    EntityStakeholder__c stakeholder2 = Utils_TestMethods.createEntityStakeholder(breEntity1.Id,newUser2.Id,Label.CL00309);
    
    entityStakeHolderList.add(stakeholder1);
    entityStakeHolderList.add(stakeholder2);
    
    Database.insert(entityStakeHolderList);
    
    entityStakeHolderList.clear();
    stakeholder1.Role__c = 'CS&Q Manager';
    stakeholder2.Role__c = 'CS&Q Leader';
    
    entityStakeHolderList.add(stakeholder1);
    entityStakeHolderList.add(stakeholder2);
    
    Database.update(entityStakeHolderList);
    
    
    Problem__c Prblm = Utils_TestMethods.createProblem(breEntity1.id); 
    Database.insert(Prblm);
    
    RootCause__c rootCause = Utils_TestMethods.createRootCause(prblm.Id);
    Database.insert(rootCause);
    
    CorrectiveAction__c crrAct = Utils_TestMethods.createCorrectiveAction(rootCause.Id,breEntity1.id,prblm.Id, newuser1.id);
    Database.insert(crrAct);
     System.debug('##Vimal##:' + crrAct.Owner__c);
    system.assertEquals(crrAct.Owner__c,newuser1.id);           
    
    crrAct.Owner__c = newUser2.id ;
    //crrAct.AccountableOrganization__c =breEntity1.id;
    Database.update(crrAct);
    System.debug('##Vimal##:' + crrAct.Owner__c);
    system.assertEquals(crrAct.Owner__c,newUser2.id);
    }
}