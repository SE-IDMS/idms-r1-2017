/****
Last Modified By:Hanamanth asper Q1 release 
*****/
@isTest
public class AP_ELLA_TO_OPS_WebServcieWithdrawal_TEST{
    
    public static testMethod void checkWithdrawalPushTest() {
        //User Creation
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA123');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        // Offer Creation
        Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
        insert offerRec;
        
        //Withdrawal Creation
        list<WithdrawalReference__c> withdrawal = new list<WithdrawalReference__c>();
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'XBTG5230'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'VX5IM2195M1271'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = 'VX5IM2220V21'));
        withdrawal.add(new WithdrawalReference__c(Offer__c = offerRec.Id, OldCommercialReference__c = '19203403V509001R'));
        insert withdrawal;
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new AP_BasicHttpBinding_IOPSImportMock_TEST('Withdrawal'));
        AP_bFO_ELLAToInvokeOPSWebservice.invokeOPSWebservice(String.valueOf(offerRec.Id));  
        Offer_Lifecycle__c offer1 = [select name, WithdrawalOPSJobId__c from Offer_Lifecycle__c where Id =: offerRec.Id];
        AP_ELLA_TO_OPS_WebServcieWithdrawal.getOPSWithdrawalResult(String.valueOf(offer1.WithdrawalOPSJobId__c), String.valueOf(offerRec.Id));
        
        Test.StopTest();  
    }
    
    public static testMethod void invokeSubstitutionPushTest() {
        //User Creation
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA123');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        // Offer Creation
        Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
        insert offerRec;
        List<Substitution__c>  sublist = new List<Substitution__c>();
           Substitution__c subs1 = new Substitution__c(
          OldCommercialReference__c = 'EDB36060',
            SubstitutionBeginning__c = system.today(),
            Comment__c = 'en_GB',
            NewcommercialReference__c = 'VX5IM2195M1271',
            //NewRange__c = '2',
            QuantityNew__c = 3,
            
            Offer__c = offerRec.Id
        );
        sublist.add(subs1);
        
        Substitution__c subs2 = new Substitution__c(
          OldCommercialReference__c = 'XBTG52301',
            SubstitutionBeginning__c = system.today(),
            Comment__c = 'en_GB',
            NewcommercialReference__c = 'VX5IM2195M1271',
            NewRange__c = '2',
            QuantityNew__c = 3,
            GroupingOfSubstitutions__c='OR',
            Offer__c = offerRec.Id
        );
        sublist.add(subs2);
           Substitution__c subs3 = new Substitution__c(
          OldCommercialReference__c = 'XBTG52301',
            SubstitutionBeginning__c = system.today(),
            Comment__c = 'en_GB',
            NewcommercialReference__c = 'VX5IM2195M1271',
            NewRange__c = '2',
            QuantityNew__c = 3,
            Offer__c = offerRec.Id,
            GroupingOfSubstitutions__c='OR'
        );
         sublist.add(subs3);
           Substitution__c subs4 = new Substitution__c(
          OldCommercialReference__c = 'XBTG52301',
            SubstitutionBeginning__c = system.today(),
            Comment__c = 'en_GB',
            NewcommercialReference__c = 'VX5IM2195M1271',
            //NewRange__c = '2',
            QuantityNew__c = 3,
            GroupingOfSubstitutions__c='AND',
            Offer__c = offerRec.Id
        );
         sublist.add(subs4);
           Substitution__c subs5= new Substitution__c(
          OldCommercialReference__c = 'XBTG52301',
            SubstitutionBeginning__c = system.today(),
            Comment__c = 'en_GB',
            NewcommercialReference__c = 'VX5IM2195M1271',
            NewRange__c = '2',
            QuantityNew__c = 3,
            Offer__c = offerRec.Id
        );
         sublist.add(subs5);
        
        if(sublist.size() > 0) {
            insert sublist;
        }
        
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new AP_BasicHttpBinding_IOPSImportMock_TEST('Substitution'));        
        AP_bFO_ELLAToInvokeOPSWebservice.invokeSubstitutionOPSWebservice(String.valueOf(offerRec.Id));
        Offer_Lifecycle__c offer1 = [select name, SubstitutionOPSJobId__c from Offer_Lifecycle__c where Id =: offerRec.Id];
        AP_ELLA_TO_OPS_WebServcieWithdrawal.getOPSSubstitutionResult(String.valueOf(offer1.SubstitutionOPSJobId__c), String.valueOf(offerRec.Id));
        Test.StopTest();
    }
    /*
    public static testMethod void getWithdrawalJobStatusInOPS() {
        //User Creation
        User newUser = Utils_TestMethods.createStandardUser('yrrffrELLA123');         
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        // Offer Creation
        Offer_Lifecycle__c offerRec = AP_EllaProjectForecast_TEST.createOffer(newUser.Id);
        insert offerRec;
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new AP_BasicHttpBinding_IOPSImportMock_TEST('Status'));
        AP_bFO_ELLAToInvokeOPSWebservice.checkWithdrawalJobStatusInOPS(String.valueOf(offerRec.Id), 'Withdrawal');
        
        Test.StopTest();
    }*/
    
}