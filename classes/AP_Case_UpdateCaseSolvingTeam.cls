public class AP_Case_UpdateCaseSolvingTeam {
    private static final  String SystemInterfaceUserName            = 'System Interface';               // 'System Interface';
    private static final  String CustomerFollowupResponse           = 'Customer Follow-up Response';    // 'Customer Follow-up Response';
    private static final  String SystemInterfaceUserId              = System.Label.CLOCT14CCC24;        // 'System Interface User Id';
    private static final  String CustomerFollowupResponseUserId     = System.Label.CLNOV15CCC02;        // 'Customer Follow-up Response user Id';

    public static  boolean isTest = false;
        
    public static boolean UpdateCaseSolvingTeam(Map<ID,Case> mapOldCases,Map<ID,Case> mapNewCases, boolean blnIsInsert){
        String strErrorId ;
        String strErrorMessage;
        
        Set<Id> setTeamId           = new Set<Id>();
        Set<Id> setUserId           = new Set<Id>();
        Set<Id> setContactId        = new Set<Id>();
        Set<Id> setTeamMemberId     = new Set<Id>();
        
        Map<Id,CustomerCareTeam__c>     mapCustomerCareTeam         = new Map<Id,CustomerCareTeam__c>();
        Map<Id,User>                    mapOwner                    = new Map<Id,User>();
        Map<String,Id>                  mapPreviousOwner            = new Map<String,Id>();
        Map<Id,Contact>                 mapContact                  = new Map<Id,Contact>();
        Map<String,CaseTeamMember>      mapExistingCaseTeamMember   = new Map<String,CaseTeamMember>();
        Map<String,CaseTeamMember>      mapNewCaseTeamMember        = new Map<String,CaseTeamMember>();
        
        List <CaseTeamMember>   lstCaseTeamMemberToAdd      = new List <CaseTeamMember>();
        List <CaseTeamMember>   lstCaseTeamMemberToUpdate   = new List <CaseTeamMember>();
        
        
        for(Case objCase:mapNewCases.values()){
            if(blnIsInsert){
                // Called from Case After Insert Trigger
                if(objCase.Team__c !=null){
                    setTeamId.add(objCase.Team__c);
                }
                if(objCase.OwnerId !=null){
                    if(((!SystemInterfaceUserName.equalsIgnoreCase(UserInfo.getName())) || (!CustomerFollowupResponse.equalsIgnoreCase(UserInfo.getName()))) &&  ((String)objCase.OwnerId).startsWith(Schema.SObjectType.User.getKeyPrefix())){
                        setUserId.add(objCase.OwnerId);
                    }
                }
                if(objCase.ContactId !=null){
                    setContactId.add(objCase.ContactId);
                }
            }
            else{
                //Called from Case After Update Trigger
                if(objCase.Team__c !=null && objCase.Team__c != mapOldCases.get(objCase.Id).Team__c){
                    setTeamId.add(objCase.Team__c);
                }
                if(objCase.OwnerId !=null && objCase.OwnerId != mapOldCases.get(objCase.Id).OwnerId){
                    if(((!SystemInterfaceUserName.equalsIgnoreCase(UserInfo.getName())) || (!CustomerFollowupResponse.equalsIgnoreCase(UserInfo.getName()))) &&  ((String)objCase.OwnerId).startsWith(Schema.SObjectType.User.getKeyPrefix())){
                        setUserId.add(objCase.OwnerId);
                    }
                    /*
                    Fix for MCS Ticket #12742097
                    ------------------
                    To Add Old Case Owner to Case Team Member
                    */
                    if(((!SystemInterfaceUserId.equals(mapOldCases.get(objCase.Id).OwnerId)) || (!CustomerFollowupResponseUserId.equals(mapOldCases.get(objCase.Id).OwnerId))) &&  ((String)mapOldCases.get(objCase.Id).OwnerId).startsWith(Schema.SObjectType.User.getKeyPrefix())){
                        mapPreviousOwner.put(objCase.Id + '_' + objCase.OwnerId,mapOldCases.get(objCase.Id).OwnerId);
                    }

                }
                if(objCase.ContactId !=null && objCase.ContactId != mapOldCases.get(objCase.Id).ContactId){
                    setContactId.add(objCase.ContactId);
                }
            }
        }
        
        If(setTeamId.size()>0){
            mapCustomerCareTeam = new Map<Id, CustomerCareTeam__c>([Select Id,TeamLeader__c from CustomerCareTeam__c where Id IN:setTeamId]);
        }
        
        if(setUserId.size() >0){
            mapOwner = new Map<Id,User>([Select Id,ManagerId from User where Id IN:setUserId]);
        }
        
        if(setContactId.size() >0){
            mapContact = new Map<Id,Contact>([Select Id, Pagent__c, PBAAgent__c,Account.Pagent__c,Account.PBAAgent__c from Contact where Id IN:setContactId]);    
        }
        
        for(Case objCase:mapNewCases.values()){
            if(objCase.Team__c !=null && mapCustomerCareTeam.containsKey(objCase.Team__c)){
                if(mapCustomerCareTeam.get(objCase.Team__c).TeamLeader__c !=null){
                    // Case Team Role: CC Team Leader  (CLAPR15CCC17)
                    setTeamMemberId.add(mapCustomerCareTeam.get(objCase.Team__c).TeamLeader__c);
                    CaseTeamMember objCaseTeamMember = new CaseTeamMember(MemberId=mapCustomerCareTeam.get(objCase.Team__c).TeamLeader__c,ParentId=objCase.Id,TeamRoleId=System.Label.CLAPR15CCC17);
                    mapNewCaseTeamMember.put(objCase.Id + '_' + mapCustomerCareTeam.get(objCase.Team__c).TeamLeader__c, + objCaseTeamMember);
                }
            }
            if(objCase.ContactId !=null && mapContact.containsKey(objCase.ContactId)){
                //Pagent__c, PBAAgent__c,Account.Pagent__c,Account.PBAAgent__c
                // Case Team Role: Contact Preferred Agent  (CLAPR15CCC15)
                if(mapContact.get(objCase.ContactId).Pagent__c !=null){
                    setTeamMemberId.add(mapContact.get(objCase.ContactId).Pagent__c);
                    CaseTeamMember objCaseTeamMember = new CaseTeamMember(MemberId=mapContact.get(objCase.ContactId).Pagent__c,ParentId=objCase.Id,TeamRoleId=System.Label.CLAPR15CCC15);
                    mapNewCaseTeamMember.put(objCase.Id + '_' + mapContact.get(objCase.ContactId).Pagent__c, + objCaseTeamMember);
                }
                // Case Team Role: Contact Preferred Agent  (CLAPR15CCC15)
                if(mapContact.get(objCase.ContactId).PBAAgent__c !=null){
                    setTeamMemberId.add(mapContact.get(objCase.ContactId).PBAAgent__c);
                    CaseTeamMember objCaseTeamMember = new CaseTeamMember(MemberId=mapContact.get(objCase.ContactId).PBAAgent__c,ParentId=objCase.Id,TeamRoleId=System.Label.CLAPR15CCC15);
                    mapNewCaseTeamMember.put(objCase.Id + '_' + mapContact.get(objCase.ContactId).PBAAgent__c, + objCaseTeamMember);
                }
                // Case Team Role: Account Preferred Agent  (CLAPR15CCC14)
                if(mapContact.get(objCase.ContactId).Account.Pagent__c !=null){
                    setTeamMemberId.add(mapContact.get(objCase.ContactId).Account.Pagent__c);
                    CaseTeamMember objCaseTeamMember = new CaseTeamMember(MemberId=mapContact.get(objCase.ContactId).Account.Pagent__c,ParentId=objCase.Id,TeamRoleId=System.Label.CLAPR15CCC14);
                    mapNewCaseTeamMember.put(objCase.Id + '_' + mapContact.get(objCase.ContactId).Account.Pagent__c, + objCaseTeamMember);
                }
                // Case Team Role: Account Preferred Agent  (CLAPR15CCC14)
                if(mapContact.get(objCase.ContactId).Account.PBAAgent__c !=null){
                    setTeamMemberId.add(mapContact.get(objCase.ContactId).Account.PBAAgent__c);
                    CaseTeamMember objCaseTeamMember = new CaseTeamMember(MemberId=mapContact.get(objCase.ContactId).Account.PBAAgent__c,ParentId=objCase.Id,TeamRoleId=System.Label.CLAPR15CCC14);
                    mapNewCaseTeamMember.put(objCase.Id + '_' + mapContact.get(objCase.ContactId).Account.PBAAgent__c, + objCaseTeamMember);
                }
            }
            if(objCase.OwnerId !=null && mapOwner.containsKey(objCase.OwnerId)){
                if(mapOwner.get(objCase.OwnerId).ManagerId !=null){
                    // Case Team Role: Case Owner Manager  (CLAPR15CCC16)
                    setTeamMemberId.add(mapOwner.get(objCase.OwnerId).ManagerId);
                    CaseTeamMember objCaseTeamMember = new CaseTeamMember(MemberId=mapOwner.get(objCase.OwnerId).ManagerId,ParentId=objCase.Id,TeamRoleId=System.Label.CLAPR15CCC16);
                    mapNewCaseTeamMember.put(objCase.Id + '_' + mapOwner.get(objCase.OwnerId).ManagerId, + objCaseTeamMember);
                }
                // Case Team Role: Case Solving  (CLAPR15CCC05)
                /*
                Fix for MCS Ticket #12742097
                ------------------
                Remove Code which Add Case Owner to Case Team Member
                setTeamMemberId.add(objCase.OwnerId);
                CaseTeamMember objCaseTeamMember = new CaseTeamMember(MemberId=objCase.OwnerId,ParentId=objCase.Id,TeamRoleId=System.Label.CLAPR15CCC05);
                mapNewCaseTeamMember.put(objCase.Id + '_' + objCase.OwnerId, + objCaseTeamMember);
                */
            }
            /*
            Fix for MCS Ticket #12742097
            ------------------
            Adding Old Case Owner to Case Team Member
            ------------------
            */
            if(mapPreviousOwner!=null && mapPreviousOwner.size() > 0 && mapPreviousOwner.containsKey(objCase.Id + '_' + objCase.OwnerId)){
                setTeamMemberId.add(mapPreviousOwner.get(objCase.Id + '_' + objCase.OwnerId));
                CaseTeamMember objCaseTeamMember = new CaseTeamMember(MemberId=mapPreviousOwner.get(objCase.Id + '_' + objCase.OwnerId),ParentId=objCase.Id,TeamRoleId=System.Label.CLAPR15CCC05);
                mapNewCaseTeamMember.put(objCase.Id + '_' + mapPreviousOwner.get(objCase.Id + '_' + objCase.OwnerId), + objCaseTeamMember);
            }
        }       
        for (CaseTeamMember objCaseTeamMember : [SELECT Id, MemberId, ParentId,TeamRoleId FROM CaseTeamMember WHERE ParentId IN :mapNewCases.keySet() AND MemberId  IN: setTeamMemberId ]) {
            mapExistingCaseTeamMember.put(objCaseTeamMember.ParentId + '_' + objCaseTeamMember.MemberId,objCaseTeamMember);
        }
        
        for(Case objCase:mapNewCases.values()){
            for (CaseTeamMember objCaseTeamMember : mapExistingCaseTeamMember.values()){
                String strKeyId = objCase.Id + '_' + objCaseTeamMember.MemberId;
                if(mapNewCaseTeamMember.containsKey(strKeyId)){
                    objCaseTeamMember.TeamRoleId = mapNewCaseTeamMember.get(strKeyId).TeamRoleId;
                    lstCaseTeamMemberToUpdate.add(objCaseTeamMember);
                    mapNewCaseTeamMember.remove(strKeyId);
                }
            }
            /*
            Fix for MCS Ticket #12742097
            ------------------
            if Case Owner is already part of any Case Team Member, remove the Case Team member
            */
            String strKeyId = objCase.Id + '_' + objCase.OwnerId;
            if(mapNewCaseTeamMember.containsKey(strKeyId)){
                mapNewCaseTeamMember.remove(strKeyId);  
            }
        }
        lstCaseTeamMemberToAdd = mapNewCaseTeamMember.values();
        
        //To Insert New Case Team Members
        if(lstCaseTeamMemberToAdd != null && lstCaseTeamMemberToAdd.size() != 0){
            try{
                Database.SaveResult[] CaseSolvingTeamSaveResult = Database.insert(lstCaseTeamMemberToAdd,false);
                for(Database.SaveResult objSaveResult: CaseSolvingTeamSaveResult){
                    if(!objSaveResult.isSuccess()){
                        Database.Error err = objSaveResult.getErrors()[0];
                        strErrorId = objSaveResult.getId() + ':' + '\n';
                        strErrorMessage = err.getStatusCode()+' '+err.getMessage()+ '\n';
                        System.debug(strErrorId + ' ' + strErrorMessage);
                    }
                }
            
            }    
            catch(Exception ex){
                strErrorMessage += ex.getTypeName()+'- '+ ex.getMessage() + '\n';
                System.debug(strErrorMessage);
            }
        }
        if(lstCaseTeamMemberToUpdate != null && lstCaseTeamMemberToUpdate.size() != 0){
            try{
                Database.SaveResult[] CaseSolvingTeamUpdateResult = Database.update(lstCaseTeamMemberToUpdate,false);
                for(Database.SaveResult objSaveResult: CaseSolvingTeamUpdateResult){
                    if(!objSaveResult.isSuccess()){
                        Database.Error err = objSaveResult.getErrors()[0];
                        strErrorId = objSaveResult.getId() + ':' + '\n';
                        strErrorMessage = err.getStatusCode()+' '+err.getMessage()+ '\n';
                        System.debug(strErrorId + ' ' + strErrorMessage);
                    }
                }
            
            }    
            catch(Exception ex){
                strErrorMessage += ex.getTypeName()+'- '+ ex.getMessage() + '\n';
                System.debug(strErrorMessage);
            }
        }
        return true;
    }
}