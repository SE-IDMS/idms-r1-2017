public class VFC_PRMEmailsConfigration {
    public List<String> countryFields;
    public CountryChannels__c conChannel {get;set;}
    public List<WrapperEmailConfig> LstWrapperEmailConfig {get;set;}
    public List<SelectOption> EmailEnableDisableValues {get;set;}
    public boolean inLine{get;set;}
    public Boolean shouldRedirect {public get; private set;}
    public String redirectUrl {public get; private set;}
    
    private String[] CountryChannelFields = new String[] {
        'AccountOwner__c','Active__c','AllowPrimaryToManage__c','ChannelCode__c','Channel_Name__c','Channel__c',
        'CompleteUserRegistrationonFirstLogin__c','Country__c','F_PRM_CountryCode__c','F_PRM_Menu__c','PredictiveCompanyMatchEnabled__c',
        'PRMCountry__c','SubChannelCode__c','SubChannel__c','Sub_Channel__c','TechIsCloned__c','ID','Name'        
    };
    public VFC_PRMEmailsConfigration(ApexPages.StandardController stdController) {
        LstWrapperEmailConfig = new List<WrapperEmailConfig>();
        countryFields = new List<String> (CountryChannelFields);
        shouldRedirect = false;    
        if(!test.isRunningTest()){
            stdController.addFields(countryFields);
        }
        conChannel = (CountryChannels__c)stdController.getRecord();
        String conChn = apexpages.currentpage().getparameters().get('Name');
        System.debug('**** Do we get a value ****'+conChn);
        inLine = (conChn != NUll)? true:false;
        
        EmailEnableDisableValues = New List<SelectOption>();
        EmailEnableDisableValues.add(new SelectOption('Enable',''));
        EmailEnableDisableValues.add(new SelectOption('Disable',''));
        List<PRMConfigureEmailNotifications__c>  configEmailNotifications = [SELECT CountryChannel__c,EmailName__c,IsActiveChannel__c,
                                                                        IsNotificationEnable__c,TechChannelName__c,TechCountry__c,
                                                                        TechSubChannelName__c FROM PRMConfigureEmailNotifications__c 
                                                                        WHERE CountryChannel__c =:conChannel.Id Order By EmailName__c Asc];
        system.debug('**PRMConfigurableEmail**'+configEmailNotifications);
        List<CSPRMEmailNotification__c> EmailConfigFieldsList = [SELECT EmailID__c,ID,DisplayOrder__c,Name,EmailTemplate__c,Description__c,Configurable__c from CSPRMEmailNotification__c ORDER BY DisplayOrder__c ASC];
        for(CSPRMEmailNotification__c EmailConfigFields:EmailConfigFieldsList){
            for(PRMConfigureEmailNotifications__c configEmailNotification:configEmailNotifications){
                if(configEmailNotification.EmailName__c == EmailConfigFields.Name){
                    WrapperEmailConfig wrEmailConfig = new WrapperEmailConfig();
                    wrEmailConfig.EmailConfig = configEmailNotification;
                    wrEmailConfig.EmailTemplate = EmailConfigFields.EmailTemplate__c;
                    wrEmailConfig.Description = EmailConfigFields.Description__c;
                    wrEmailConfig.configurable = EmailConfigFields.Configurable__c;
                    wrEmailConfig.EmailId = EmailConfigFields.EmailID__c;
                    System.debug('***'+EmailConfigFields.EmailTemplate__c);
                    LstWrapperEmailConfig.add(wrEmailConfig);
                    System.debug('** List PRMConfigurableEmail**'+LstWrapperEmailConfig);
                }
            }
        }
        
    }
    
    public pageReference doSave(){
        pagereference pg = null;
        try{
            List<PRMConfigureEmailNotifications__c> updateconfigEmailNotif = new List<PRMConfigureEmailNotifications__c>();
            for(WrapperEmailconfig wrEmCon:LstWrapperEmailConfig){
                //wrEmCon.EmailConfig.IsNotificationEnable__c = (wrEmCon.selectedValue == 'Enable' ? true : false);
                updateconfigEmailNotif.add(wrEmCon.EmailConfig);
                system.debug('****'+wrEmCon.EmailConfig.IsNotificationEnable__c);
            }
            update updateconfigEmailNotif;
            pg = new pagereference('/'+conChannel.Id);
        }
        catch(exception ex){
            System.debug('**** There has been an error while trying to update the notifications configration ****'+ex.getMessage());
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));
        }
        return pg;
    }
    
    public pageReference goToEditMode(){
        shouldRedirect = true;
        redirectUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/VFP_PRMEmailsConfigration?id='+conChannel.Id+'&Name='+conChannel.Name;
        return null;
    } 
    
    public class WrapperEmailConfig{
        public String EmailTemplate {get;set;}
        public PRMConfigureEmailNotifications__c EmailConfig {get;set;}
        public String Description {get;set;}
        //public Boolean selectedValue {get;set;}
        public Boolean configurable {get;set;}
        public String EmailId {get;set;}
        public wrapperEmailconfig(){
            EmailConfig = new PRMConfigureEmailNotifications__c();
            configurable = false;
            EmailId = null;
        }
    }
}