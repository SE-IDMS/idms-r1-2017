/** 
* Author:Beerappa K
* Class: VCC_PAMultiAttachment Component class for File Upload.
* Description: objectId - The ID of the record uploaded documents will be attached to.
* Date: 25/07/2016
* 
*/
@isTest
 public class VCC_PAMultiAttachment_TEST{
     public static testMethod void TestAttachmentcontroller() { 
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.SEAccountID__c = '12345678';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        insert testcontact;
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        Case objCase = Utils_TestMethods.createCase(testAccount.Id, testcontact.Id,'testing');
        insert objCase;
        
        Attachment attach=new Attachment(); 
        attach.Name='Unit Test Attachment12345671'; 
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
        String strBodyBlob=String.ValueOf(EncodingUtil.base64Encode(bodyBlob));
        attach.body=bodyBlob; 
        attach.parentId=objCase.Id; 
        attach.ContentType = 'application/msword'; 
        attach.IsPrivate = false; 
        attach.Description = 'Test1234561'; 
        insert attach; 
        System.assert(attach!=null);
        
        test.startTest();
            //VCC_PAMultiAttachment attachmentPage=new VCC_PAMultiAttachment();
            //VCC_PAMultiAttachment.attachBlob(String.valueOf(objCase.Id),String.valueOf(attach.Id),attach.Name,attach.ContentType,strBodyBlob);
            VCC_PAMultiAttachment.attachBlob(String.valueOf(objCase.Id),String.valueOf(attach.Id),attach.Name,attach.ContentType,strBodyBlob);
            VCC_PAMultiAttachment.attachBlob(String.valueOf(objCase.Id),'',attach.Name,attach.ContentType,strBodyBlob);
            VCC_PAMultiAttachment.deleteAttachment(String.valueOf(attach.Id));
        test.stopTest();
    }
}