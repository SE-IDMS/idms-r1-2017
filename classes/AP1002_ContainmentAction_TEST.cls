@isTest
private class AP1002_ContainmentAction_TEST {
    
    public static BusinessRiskEscalationEntity__c BRE1;
    public static BusinessRiskEscalationEntity__c BRE2;
    public static BusinessRiskEscalationEntity__c BRE3;
    public static BusinessRiskEscalationEntity__c BRE4;
    public static User sampleUser;
    public static User sampleUser1;
    public static EntityStakeholder__c entityStakeHolder;
    public static EntityStakeholder__c entityStakeHolder1;
    public static EntityStakeholder__c entityStakeHolder2;
    public static EntityStakeholder__c entityStakeHolder3;
    public static Problem__c problem1; 
    public static Problem__c problem2; 
    public static ContainmentAction__c containmentActionSample;
    public static ContainmentAction__c containmentActionSample1;
    public static ContainmentAction__c containmentActionSample12;
    public static ContainmentAction__c XA;

    /*public static void populateRecords(String prefix)
    {	
        sampleUser=Utils_TestMethods.createStandardUser('sample'+prefix);
        insert sampleUser;
    }*/

    public testmethod static void populateContainmentActionOwner_TEST(){
        
        sampleUser=Utils_TestMethods.createStandardUser('sample');
    	insert sampleUser;
        
        BusinessRiskEscalationEntity__c brEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
        Database.insert(brEntity,false);
        BusinessRiskEscalationEntity__c brSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
        Database.insert(brSubEntity,false);
        
        BRE1 = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        BRE1.Location__c = 'Location 1234';
        Database.insert(BRE1);
                
        entityStakeHolder1=Utils_TestMethods.createEntityStakeholder(BRE1.id,sampleUser.id,Label.CL10037);
        insert entityStakeHolder1;
        
        BRE2 = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
        BRE2.Location__c = 'Location 2';
        BRE2.RelatedOrganizationtoNotifyofXA__c = BRE1.Id;
        Database.insert(BRE2);
        
        entityStakeHolder2=Utils_TestMethods.createEntityStakeholder(BRE2.id,sampleUser.id,Label.CL10036);
        insert entityStakeHolder2;
        
        //populateRecords('0');
        problem1 = Utils_TestMethods.createProblem(BRE1.id);
        insert problem1;
    
        containmentActionSample=Utils_TestMethods.createContainmentAction(problem1.id,BRE1.id);
        insert containmentActionSample;
        ContainmentAction__c containmentActionResult=[select id,Owner__c from ContainmentAction__c where Id=: containmentActionSample.id];      
        System.debug('$$$$$$$$$$$$$$A'+containmentActionResult);
        
        containmentActionSample.AccountableOrganization__c = BRE1.id;
        containmentActionSample.Owner__c =null;
        update containmentActionSample;
        containmentActionResult=[select id,Owner__c from ContainmentAction__c where Id=: containmentActionSample.id];
        System.debug('$$$$$$$$$$$$$$B'+containmentActionResult);
       
     	problem1.Severity__c='Safety related';
        update problem1;
   
   		containmentActionSample.status__c='Completed';
        
        Test.startTest();
 
     	containmentActionSample.recordtypeid=[select id,name from recordtype where Name =:Label.CLSEP12I2P09][0].id;
        update containmentActionSample;
        System.debug('@@@@@@@@@@@@@@@@@@'+containmentActionSample);
        ApexPages.StandardController conActionController = new ApexPages.StandardController(containmentActionSample);
        PageReference pageRef = Page.VFP_ContainmentActionSubmitForApproval; 
        ApexPages.currentPage().getParameters().put('id', containmentActionSample.id);
    
        VFC_ContainmentActionSubmitForApproval addConActionInMass = new VFC_ContainmentActionSubmitForApproval(conActionController);
        
        addConActionInMass.processDetails();
        addConActionInMass.doApprove();
        addConActionInMass.doCancel();
    
        problem1.Severity__c='Minor';
        update problem1;
        containmentActionSample1=Utils_TestMethods.createContainmentAction(problem1.id,BRE2.id);
        insert containmentActionSample1;
        //populateRecords('3');
        containmentActionSample1.status__c=Label.CLSEP12I2P84; //Status Pending
        //containmentActionSample1.StatusofValidation__c=label.CLSEP12I2P111;
        containmentActionSample1.recordtypeid=[select id,name from recordtype where Name =:Label.CLSEP12I2P09][0].id;
        update containmentActionSample1;
        
        ApexPages.StandardController conActionController1 = new ApexPages.StandardController(containmentActionSample1);
        PageReference pageRef1 = Page.VFP_ContainmentActionSubmitForApproval; 
        ApexPages.currentPage().getParameters().put('id', containmentActionSample1.id);

        VFC_ContainmentActionSubmitForApproval addConActionInMass1 = new VFC_ContainmentActionSubmitForApproval(conActionController1);
        
        addConActionInMass1 .processDetails();
        
        addConActionInMass1.doCancel();
    
     	containmentActionSample12=Utils_TestMethods.createContainmentAction(problem1.id,BRE2.id);
        insert containmentActionSample12;
        //populateRecords('2');
        containmentActionSample12.status__c=Label.CLSEP12I2P84; //Status Pending
        containmentActionSample12.StatusofValidation__c=label.CLSEP12I2P111;
        containmentActionSample12.recordtypeid=[select id,name from recordtype where Name =:Label.CLSEP12I2P09][0].id;
        update containmentActionSample12;
        
        ApexPages.StandardController conActionController2 = new ApexPages.StandardController(containmentActionSample12);
    	PageReference pageRef2 = Page.VFP_ContainmentActionSubmitForApproval; 
    	ApexPages.currentPage().getParameters().put('id', containmentActionSample12.id);

    	VFC_ContainmentActionSubmitForApproval addConActionInMass2 = new VFC_ContainmentActionSubmitForApproval(conActionController2);
    
    	addConActionInMass2 .processDetails();
    
        addConActionInMass2.doCancel();
        
        Test.stopTest();
    }
    
    public testmethod static void TestcreateAlertForXA_Stackholders() {
         Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
         User newUser = Utils_TestMethods.createStandardUser('TestUser');    
         newUser.UserRoleID = rol_ID ;
        
         Set<Id> XAids = new Set<Id>();
        
         BusinessRiskEscalationEntity__c brEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
         brEntity.Entity__c = 'Entity 12';
         Database.insert(brEntity);
         BusinessRiskEscalationEntity__c brSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
         brSubEntity.Entity__c = 'Entity 12';
         brSubEntity.SubEntity__c = 'SubEntity 12';
         Database.insert(brSubEntity);
         BusinessRiskEscalationEntity__c AccOrg = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();  
         AccOrg.Entity__c = 'Entity 12';
         AccOrg.SubEntity__c = 'SubEntity 12';
         Database.insert(AccOrg,false);
         
         Problem__c prob1 = Utils_TestMethods.createProblem(AccOrg.Id,12);
         prob1.ProblemLeader__c = newUser.Id;
         prob1.ProductQualityProblem__c = false;
         prob1.RecordTypeid = Label.CLI2PAPR120014;
         prob1.Sensitivity__c = 'Public';
         prob1.Severity__c = 'Safety related';
         Database.insert(prob1);
         
         List<ContainmentAction__c> lstXA = new List<ContainmentAction__c>();
         ContainmentAction__c XA1 = Utils_TestMethods.createContainmentAction(prob1.Id,AccOrg.Id);
         lstXA.add(XA1);
         ContainmentAction__c XA2 = Utils_TestMethods.createContainmentAction(prob1.Id,AccOrg.Id);
         lstXA.add(XA2);
         Database.insert(lstXA); 
        
         XAids.add(XA1.Id);
         XAids.add(XA2.Id);
        
         AP1002_ContainmentAction.createAlertForXA_Stackholders(XAids);
    }
}