/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 20/10/2010
    Description     : Test class for Controller Extension VFC05_SFECalculator that calculates Total Visits.
*/
@isTest
private class VFC05_SFECalculator_TEST {

    static testMethod void testCalcVisitTotal() {
        User runAsUser = Utils_TestMethods.createStandardUser('tst12');
        insert runAsUser;
        
        System.runAs(runAsUser){
            List<Account> lacc = new List<Account>();
            Account acc = Utils_TestMethods.createAccount();
            lacc.add(acc);
            Account acc1 = Utils_TestMethods.createAccount();
            lacc.add(acc1);
            Account acc2 = Utils_TestMethods.createAccount();
            lacc.add(acc2);
            Account acc3 = Utils_TestMethods.createAccount();
            lacc.add(acc3);
            Account acc4 = Utils_TestMethods.createAccount();
            lacc.add(acc4);
            Account acc5 = Utils_TestMethods.createAccount();
            lacc.add(acc5);
            Account acc6 = Utils_TestMethods.createAccount();
            lacc.add(acc6);
            Account acc7 = Utils_TestMethods.createAccount();
            lacc.add(acc7);
            Account acc8 = Utils_TestMethods.createAccount();
            lacc.add(acc8);
            insert lacc;
            SFE_IndivCAP__c indivCAP = Utils_TestMethods.createIndividualActionPlan(runAsUser.Id);
            indivCAP.G1VisitFreq__c = null;
            indivCAP.G2VisitFreq__c = null;
            indivCAP.G3VisitFreq__c = null;
            indivCAP.Q1VisitFreq__c = null;
            indivCAP.Q2VisitFreq__c = null;
            indivCAP.Q3VisitFreq__c = null;
            indivCAP.S1VisitFreq__c = null;
            indivCAP.S2VisitFreq__c = null;
            indivCAP.S3VisitFreq__c = null;
            insert indivCAP;        
            List<SFE_PlatformingScoring__c> lps = new List<SFE_PlatformingScoring__c>();
                                            
            SFE_PlatformingScoring__c ps1 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                            PlatformedAccount__c = acc.Id, 
                                                                            MktShare__c=10,Score__c=1   );      
            SFE_PlatformingScoring__c ps2 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                                        PlatformedAccount__c = acc1.Id, 
                                                                                        MktShare__c=10,Score__c=4   );      
            SFE_PlatformingScoring__c ps3 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                                        PlatformedAccount__c = acc2.Id, 
                                                                                        MktShare__c=10,Score__c=30  );          
            SFE_PlatformingScoring__c ps4 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                                        PlatformedAccount__c = acc3.Id, 
                                                                                        MktShare__c=98.5,Score__c=1 );
            SFE_PlatformingScoring__c ps5 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                                        PlatformedAccount__c = acc4.Id, 
                                                                                        MktShare__c=98.5,Score__c=4 );
            SFE_PlatformingScoring__c ps6 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                                        PlatformedAccount__c = acc5.Id, 
                                                                                        MktShare__c=98.5,Score__c=30    );
            SFE_PlatformingScoring__c ps7 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                                        PlatformedAccount__c = acc6.Id, 
                                                                                        MktShare__c=99.5,Score__c=1 );                                                  
            SFE_PlatformingScoring__c ps8 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                                        PlatformedAccount__c = acc7.Id, 
                                                                                        MktShare__c=99.5,Score__c=4 );
            SFE_PlatformingScoring__c ps9 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, 
                                                                            PlatformedAccount__c = acc8.Id, 
                                                                            MktShare__c=99.5,Score__c=30    );                                                                          
            lps.add(ps1);
            lps.add(ps2);
            lps.add(ps3);
            lps.add(ps4);
            lps.add(ps5);
            lps.add(ps6);
            lps.add(ps7);
            lps.add(ps8);
            lps.add(ps9);
            insert lps;
                                
            //Initialise controller etc.
            PageReference pageRef = Page.VFP05_SFECalculator;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', indivCAP.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(indivCAP);
            VFC05_SFECalculator c = new VFC05_SFECalculator(sc);
            Integer visits = c.totalVisits;     //retrieve Total Visits property that performs the calculation
        }
    }
}