@isTest
public class AP_AssignPRMPermissionQueueable_Test{

    @testSetup static void testSetupMethodPRMPermissionQueue() { 
        
        Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
        INSERT testCountry;
        StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
        INSERT testStateProvince; 
        
        
        Account acc = new Account();
            acc.Name = 'TestAccount';
            acc.RecordTypeId = System.Label.CLAPR15PRM025;
            acc.PRMCompanyName__c = 'Schneider';
            acc.PRMStreet__c      = 'Elnath Street';
            acc.PRMCompanyPhone__c= '987654321'; 
            acc.PRMAdditionalAddress__c = 'Marathalli';
            acc.PRMWebsite__c           = 'https://schneider-electric.com';
            acc.PRMCity__c              = 'Bangalore';
            //acc.PRMTaxId__c            
            acc.PRMStateProvince__c     = testStateProvince.Id; 
            acc.PRMCorporateHeadquarters__c = True;
            acc.PRMCountry__c               = testCountry.Id;
            acc.PRMIncludeCompanyInfoInSearchResults__c = True;
            acc.PRMZipCode__c                           = '560103';
            acc.PRMBusinessType__c = 'FI';
            acc.ClassLevel1__c     = 'FI';
            acc.PRMAreaOfFocus__c  = 'FI3';
            acc.ClassLevel2__c     = 'FI3';
        INSERT acc;
        
        Contact con = new Contact();
            
            con.FirstName = con.PRMFirstName__c = 'FirstName TestClass';
            con.LastName = con.PRMLastName__c  = 'LastName TestClass';
            con.PRMCustomerClassificationLevel1__c  = 'FI';
            con.PRMCustomerClassificationLevel2__c  = 'FI3';
            con.Email = con.PRMEmail__c          = 'testclass1@test.com'; 
            con.PRMLanguage__c = 'EN'; 
            con.AccountId = acc.Id;   
            con.PRMWorkPhone__c  = '123456';
            con.PRMMobilePhone__c = '987654321'; 
            con.PRMLanguage__c = 'EN';
        INSERT con;
    
    }

    public static testMethod void testMethodOne() {
    
        Contact con = [SELECT Id FROM Contact WHERE FirstName=:'FirstName TestClass'];
        
        ContactAssignedFeature__c contactAssignFeature = new ContactAssignedFeature__c();
        contactAssignFeature.Active__c = True;
        contactAssignFeature.Contact__c = con.Id;
        INSERT contactAssignFeature;
        
        List<Id> conIdLst = new List<Id>{con.Id};
        Test.startTest();
            ID jobID = System.enqueueJob(new AP_AssignPRMPermissionQueueable(conIdLst,'POMP',AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_POMP));       
        Test.stopTest();   
    
    }
    
     public static testMethod void testMethodTwo() {
        
        Country__c testCountry = [SELECT Id FROM Country__c WHERE Name = 'India'];
        User us = new user(id = userinfo.getuserId());
                us.BypassVR__c = true;
            update us;
            
        System.RunAs(us){
        
        
        
        FieloEE.MockUpFactory.setCustomProperties(false);          
                
           Account acc = new Account();
               acc.Name = 'test acc';
               insert acc;
                
           Contact con = new Contact();
                con.AccountId = acc.Id;
                con.FirstName = 'test contact Fn';
                con.LastName  = 'test contact Ln';
                con.PRMCompanyInfoSkippedAtReg__c = false;
                con.PRMCountry__c = testCountry.Id;
                con.PRMFirstName__c = 'PRM test contact Fn'; 
                con.PRMLastName__c  = 'PRM test contact Fn';
                insert con;
                
            List<Profile> profiles = [select id from profile where name='SE - Channel Partner (Community)'];
                if(profiles.size()>0){
                    UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
                    User u = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
                    u.ContactId = con.Id;
                    u.PRMRegistrationCompleted__c = false;
                    u.BypassWF__c = false;
                    u.BypassVR__c = true;
                    u.FirstName ='Test User First';
                    insert u;
                }
            
                
             FieloEE__Member__c member = new FieloEE__Member__c(
                    FieloEE__LastName__c = 'Polo',
                    FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                    FieloEE__Street__c = 'Calle Falsa',
                    F_PRM_PrimaryChannel__c = 'TST',F_Country__c= testCountry.Id,
                    F_Account__c=acc.Id
                );
                    insert member;
                System.debug('Member123 :'+member.Id);
                
              FieloEE.MemberUtil.setMemberId(member.Id);
        con.FieloEE__Member__c = member.Id;
        Update con;
        
        
        PRMRequestQueue__c prmReqQueue = new PRMRequestQueue__c(AccountId__c = acc.Id, ContactId__c = con.Id, RequestType__c = 'UPDMEMPROP');
        INSERT prmReqQueue;
        
        List<Id> conIdLst = new List<Id>{con.Id};
        Test.startTest();
            ID jobID = System.enqueueJob(new AP_AssignPRMPermissionQueueable(conIdLst,'PRTNRPRTL',AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_PARTNER));       
        Test.stopTest();   
        }
    }
    
   public static testMethod void testMethodThree() {
        
        Country__c testCountry = [SELECT Id FROM Country__c WHERE Name = 'India'];
        User us = new user(id = userinfo.getuserId());
                us.BypassVR__c = true;
            update us;
            
        System.RunAs(us){
        
        
        
        FieloEE.MockUpFactory.setCustomProperties(false);          
                
           Account acc = new Account();
               acc.Name = 'test acc';
               insert acc;
                
           Contact con = new Contact();
                con.AccountId = acc.Id;
                con.FirstName = 'test contact Fn';
                con.LastName  = 'test contact Ln';
                con.PRMCompanyInfoSkippedAtReg__c = false;
                con.PRMCountry__c = testCountry.Id;
                con.PRMFirstName__c = 'PRM test contact Fn'; 
                con.PRMLastName__c  = 'PRM test contact Fn';
                insert con;
                
            List<Profile> profiles = [select id from profile where name='SE - Channel Partner (Community)'];
                if(profiles.size()>0){
                    UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
                    User u = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
                    u.ContactId = con.Id;
                    u.PRMRegistrationCompleted__c = false;
                    u.BypassWF__c = false;
                    u.BypassVR__c = true;
                    u.FirstName ='Test User First';
                    insert u;
                }
            
                
             FieloEE__Member__c member = new FieloEE__Member__c(
                    FieloEE__LastName__c = 'Polo',
                    FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                    FieloEE__Street__c = 'Calle Falsa',
                    F_PRM_PrimaryChannel__c = 'TST',F_Country__c= testCountry.Id,
                    F_Account__c=acc.Id
                );
                    insert member;
                System.debug('Member123 :'+member.Id);
                
              FieloEE.MemberUtil.setMemberId(member.Id);
        con.FieloEE__Member__c = member.Id;
        Update con;
        
        List<Id> conIdLst = new List<Id>{con.Id};
        Test.startTest();
            ID jobID = System.enqueueJob(new AP_AssignPRMPermissionQueueable(conIdLst,'PRTNRPRTL',AP_PRMAppConstants.ProcessPendingQueueItems.ASSIGN_PERMISSION_PARTNER));       
        Test.stopTest();   
        }
    }
}