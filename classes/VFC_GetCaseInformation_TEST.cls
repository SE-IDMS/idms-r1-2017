@isTest
public class VFC_GetCaseInformation_TEST {
  
    static testMethod void getCseInfo(){
        
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        
        Account accObj = Utils_TestMethods.createAccount();
        accObj.Phone = '986435';
        accObj.LeadingBusiness__c = 'Buildings';
        accObj.ClassLevel1__c = 'Retailer';
        accObj.Type = 'Prospect';
        accObj.City__c = 'bangalore';
        accObj.ZipCode__c= '452234';
        accObj.Street__c= 'aSDASDASD';
        accObj.POBox__c = '122344';
        accObj.Country__c = testCountry.Id;
        insert accObj;
        
        Contact conObj = Utils_TestMethods.createContact(accObj.id, 'Contact 3');
        conObj.firstname = 'Contact';
        conObj.LastName = 'Test 3';
        conObj.WorkPhone__c = '986435';
        conObj.accountid = accObj.id;
        insert conObj;
        
        Contract controbj=Utils_TestMethods.createContract(accObj.id,conObj.id);
        insert controbj;
        
        CTR_ValueChainPlayers__c  CVCPObj =Utils_TestMethods.createCntrctValuChnPlyr(controbj.Id);
        CVCPObj.LegacyPIN__c = '123456'; 
        insert CVCPObj;
        
        Case objCase = Utils_TestMethods.createCase(accObj.Id, conObj.Id,'testing');
        insert objCase; 
        VFC_GetCaseInformation.getCaseInfo(objCase.CaseNumber);
        VFC_GetCaseInformation.getCVCPInfo(CVCPObj.LegacyPIN__c);
        
    }
 
}