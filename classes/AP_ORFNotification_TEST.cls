/**
21-June-2013
Shruti Karn  
PRM June13 Release    
Initial Creation: test class for    
    1.  AP_ORFNotification
    
 */
@isTest
private class AP_ORFNotification_TEST {
    static testMethod void myUnitTest() {
        
        Country__c newCountry = Utils_TestMethods.createCountry();
        newCountry.Name = 'FRANCE';
        insert newCountry;
        
        User u = Utils_TestMethods.createStandardUserWithNoCountry('ORF');
        u.BypassVR__c = true;
        u.country__c = newCountry.countrycode__c;
        //u.ProgramAdministrator__c = true;
        
        User u2 = Utils_TestMethods.createStandardUserWithNoCountry('ORF');
        u2.BypassVR__c = true;
        u2.country__c = newCountry.countrycode__c;
        insert u2;
        
        OpportunityRegistrationform__c ORF;
        Account acc;
        PartnerProgram__c countryPRG ;
        system.runAs(u)
        {
           
            PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
            globalProgram.TECH_CountriesId__c = newCountry.id;
            globalPRogram.recordtypeid = Label.CLMAY13PRM15;
            globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
            insert globalProgram;
            
            ApexPages.currentPage().getParameters().put('GlobalPrgId',globalProgram.Id);
            ApexPages.currentPage().getParameters().put('type',Label.CLJUN13PRM07);
            ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(globalProgram);
            VFC_NewCountryProgram myPageCon1 = new VFC_NewCountryProgram(sdtCon1);
            myPageCon1.goToCountryPRGEditPage();
            
            countryPRG = [Select id,recordtypeid from PartnerProgram__c where GlobalPartnerProgram__c = :globalProgram.Id limit 1];
            countryPRG.ProgramStatus__c = Label.CLMAY13PRM47;
            update countryPRG;

            ORF = Utils_TestMethods.CreateORF(newCountry.Id);
            ORF.ProductType__c = 'test';
            ORF.PArtnerProgram__C  = countryPRG.Id;
            insert ORF;
            
            acc = Utils_TestMethods.createAccount();
            insert acc;
            
            
        }
        
            Opportunity opp = Utils_TestMethods.createOpportunity(acc.id);
            insert opp;
            
            OpportunityTeamMember team = Utils_TestMethods.createOppTeamMember(opp.Id,u2.Id);
            insert team;
            
            ORF.Tech_LinkedOpportunity__c = opp.Id;
            update ORF;
            
            ORF.Status__c = Label.CLJUN13PRM01;
            update ORF;
            system.runAs(u)
            {
                OpportunityRegistrationform__c ORF2 = Utils_TestMethods.CreateORF(newCountry.Id);
                ORF2.ProductType__c = 'test';
                ORF2.PArtnerProgram__C  = countryPRG.Id;
                insert ORF2;
                
                ORFwithOpportunities__c newORFOpp = new ORFwithOpportunities__c();
                newORFOpp.Opportunity__c = opp.Id;
                newORFOpp.Opportunity_Registration_Form__c= ORF2.Id;
                insert newORFOpp;
                
                ORF2.Status__c = Label.CLJUN13PRM02;
                ORF2.RejectReason__c = 'test';
                update ORF2;
            }
    }
}