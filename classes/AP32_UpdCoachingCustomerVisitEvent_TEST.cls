/*
    Author          : Ramesh Rajasekaran - Global Delivery Team
    Date Created    : 11/10/2011
    Description     : Test class for AP32_UpdateCoachingCustomerVisitEvent class and related Coaching Visit triggers.  
                        
*/
@isTest
private class AP32_UpdCoachingCustomerVisitEvent_TEST {

    static testMethod void testCoachingVisit() {
    
        User tstUser = Utils_TestMethods.createStandardUser('tst1');
        Database.Insert(tstUser);
        
        User tstUser1 = Utils_TestMethods.createStandardUser('tst2');
        Database.Insert(tstUser1);
        
        Account accounts = Utils_TestMethods.createAccount();
        Database.Insert(accounts);  
       
        Contact contacts = Utils_TestMethods.createContact(accounts.Id,'TestContact');
        Database.insert(contacts);
       
        //Case cases = Utils_TestMethods.createCase(accounts.Id,contacts.id,'Open');
        //Database.insert(cases);
        
        Event Evt = Utils_TestMethods.createEvent(accounts.Id,contacts.Id,'New');
        Database.Insert(Evt);
        
        //Event Evt1 = Utils_TestMethods.createEvent(cases.id,contacts.Id,'New');
        //Database.Insert(Evt1);                
        
        CoachingVisit__c cv = Utils_TestMethods.createCoachingVisit(accounts.Id, tstUser.Id, tstUser1.Id, Evt.Id);
        Database.Insert(cv); 
        
        Evt.AssociatedCoachingVisitId__c  = cv.Id;
        Database.Update(Evt);
        
        //string ServerUrl = getURL();
        cv.AssociatedCustomerVisit__c = 'salesforce.com/'+Evt.id+'/d';
        Database.update(cv); 
        
       // cv.SalesRep__c = tstUser1.id;
       // Database.update(cvf); 
               
    }
}