/********************************************************************
* Company: Fielo
* Developer: Elena J. Schwarzböck
* Created Date: 12/09/2016
* Description: Test class of FieloPRM_UTILS_SegmentHybrid
*******************************************************************/
@isTest
public with sharing class FieloPRM_UTILS_SegmentHybridTest{
  
    public static testMethod void testUnit(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            Country__c testCountryLocal = new Country__c(
                Name   = 'Local Country Test',
                CountryCode__c = 'US',
                InternationalPhoneCode__c = '01',
                Region__c = 'TESTL'
            );
            insert testCountryLocal;

            PRMCountry__c prmCountryLocal = new PRMCountry__c(
                Name = 'Local Country Local Test',
                CountryPortalEnabled__c = true,
                Country__c = testCountryLocal.Id,
                PLDatapool__c = 'pagecontrol2',
                TECH_MigrationUniqueID__c = 'TestLocal'
            );
            insert prmCountryLocal;
            
            FieloEE__Member__c member = FieloEE.MockUpFactory.createMember('Test member', '1234', 'DNI');
            
            member.F_PRM_Country__c = prmCountryLocal.Id;
            
            update member;
            
            FieloEE__RedemptionRule__c newSegment = new FieloEE__RedemptionRule__c(
                FieloEE__isActive__c = true,
                Name = prmCountryLocal.CountryCode__c + ' - Test',
                F_PRM_Country__c = prmCountryLocal.Id,
                F_PRM_SegmentFilter__c = 'P-' + prmCountryLocal.CountryCode__c + '-Test',
                RecordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Manual' AND SObjectType = 'FieloEE__RedemptionRule__c'].Id,
                F_PRM_Type__c = 'Hybrid'               
            );                       
            insert newSegment;
                
            List<FieloEE__RedemptionRuleCriteria__c> listNewSegmentCriterias = new List<FieloEE__RedemptionRuleCriteria__c>();
                
            FieloEE__RedemptionRuleCriteria__c newSegmentCriteria = new FieloEE__RedemptionRuleCriteria__c(
                FieloEE__RedemptionRule__c = newSegment.Id,
                FieloEE__BooleanValue__c = false,
                FieloEE__FieldName__c = 'F_PRM_CountryCode__c',
                FieloEE__FieldType__c = 'Text',
                FieloEE__Operator__c = 'equals',               
                FieloEE__Values__c = prmCountryLocal.CountryCode__c
            );               
            insert newSegmentCriteria;

            test.startTest();
            
            List<FieloEE__RedemptionRule__c> listSegments = [SELECT Id, CountryCode__c, F_PRM_Country__r.F_PRM_IsRegion__c, F_PRM_Country__r.F_PRM_ITBRegion__c, (SELECT Id, FieloEE__RedemptionRule__c, FieloEE__BooleanValue__c, FieloEE__FieldName__c, FieloEE__FieldType__c, FieloEE__Operator__c, FieloEE__Values__c  FROM FieloEE__Redemption_Criterias__r) FROM FieloEE__RedemptionRule__c WHERE Id =: newSegment.Id];

            Map<Id,Set<Id>> segByMember = FieloPRM_UTILS_SegmentHybrid.segmentByMember(new List<FieloEE__Member__c>{member},listSegments);
                       
            test.stopTest();
            
        }

    }
    
}