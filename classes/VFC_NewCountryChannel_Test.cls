@isTest
public class VFC_NewCountryChannel_Test{
    static TestMethod void NewCountryChannel(){
        User testUser = Utils_TestMethods.createStandardUser('tChAdmin');
        testUser.BypassVR__c = true;
        UserRole uRole = [Select Id,Name,DeveloperName From UserRole where DeveloperName = 'CEO'];
        testUser.UserRoleId = uRole.Id;
        System.runAs(testUser) {
            List<CS_PRM_UserRegistration__c> cs_RecList = new List<CS_PRM_UserRegistration__c>();
            CS_PRM_UserRegistration__c cs_Rec = new CS_PRM_UserRegistration__c(AppliedTo__c = 1, FieldLabel__c = 'Contact: Email', FieldSortOrder__c = 1, MappedField__c = 'PRMEmail__c', MappedObject__c = 'Contact', Name = 'email');
            cs_RecList .add(cs_Rec);
            CS_PRM_UserRegistration__c cs_Rec2 = new CS_PRM_UserRegistration__c(AppliedTo__c = 1, FieldLabel__c = 'Contact: First Name', FieldSortOrder__c = 2, MappedField__c = 'PRMFirstName__c ', MappedObject__c = 'Contact', Name = 'firstName');
            cs_RecList .add(cs_Rec2);
            CS_PRM_UserRegistration__c cs_Rec3 = new CS_PRM_UserRegistration__c(AppliedTo__c = 1,FieldLabel__c = 'Contact: Email', FieldSortOrder__c = 3, MappedField__c = 'PRMLastName__c', MappedObject__c = 'Contact', Name = 'lastName');
            cs_RecList.add(cs_Rec3);
            CS_PRM_UserRegistration__c cs_Rec4 = new CS_PRM_UserRegistration__c(AppliedTo__c = 1,FieldLabel__c = 'Contact: Business Type', FieldSortOrder__c = 4, MappedField__c = 'PRMBusinessType__c', MappedObject__c = 'Account', Name = 'businessType');
            cs_RecList.add(cs_Rec4);
            CS_PRM_UserRegistration__c cs_Rec5 = new CS_PRM_UserRegistration__c(AppliedTo__c = 1,FieldLabel__c = 'Contact: Area of Focus', FieldSortOrder__c = 5, MappedField__c = 'PRMAreaOfFocus__c', MappedObject__c = 'Account', Name = 'areaOfFocus');
            cs_RecList.add(cs_Rec5);
            CS_PRM_UserRegistration__c cs_Rec6 = new CS_PRM_UserRegistration__c(AppliedTo__c = 1,FieldLabel__c = 'Your Account: Company Country', FieldSortOrder__c = 13, MappedField__c = 'PRMCountry__c', MappedObject__c = 'Account', Name = 'companyCountry');
            cs_RecList.add(cs_Rec6);
            CS_PRM_UserRegistration__c cs_Rec7 = new CS_PRM_UserRegistration__c(AppliedTo__c = 1,FieldLabel__c = 'Your Account: State / Province', FieldSortOrder__c = 17, MappedField__c = 'PRMStateProvince__c', MappedObject__c = 'Account', Name = 'companyStateProvince');
            cs_RecList.add(cs_Rec7);
        insert cs_RecList;
            CountryChannels__c cChannels = new CountryChannels__c();
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
            insert cLC;
            ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
            insert cLChild;
            ClassificationLevelCatalog__c cLChild1 = new ClassificationLevelCatalog__c(Name = 'FI2', ClassificationLevelName__c = 'Multi Lateral Financial', Active__c = true, ParentClassificationLevel__c = cLC.id);
            insert cLChild1;
            PRMCountry__c prmCountry = new PRMCountry__c(CountryPortalEnabled__c = true,
                                          CountrySupportEmail__c = 'abc@xyz.com',
                                          PLDatapool__c = 'rn_US',
                                          Country__c = country.id
                                          );
            insert prmCountry;
            PageReference pageRef = Page.VFP_NewCountryChannel;
            Test.setCurrentPage(pageRef);
             Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(cChannels);
             ApexPages.currentPage().getParameters().put('country',country.Name);
             ApexPages.currentPage().getParameters().put('prmCountry',prmCountry.Id);
             VFC_NewCountryChannel thisNewCountryChannel = new VFC_NewCountryChannel(thisController);
             thisNewCountryChannel.cChannelRecord.Channel__c = cLC.Id;
             thisNewCountryChannel.cChannelRecord.SubChannel__c = cLChild.id;
             thisNewCountryChannel.cChannelRecord.Country__c = country.Id;
             thisNewCountryChannel.cChannelRecord.Active__c = True;
             thisNewCountryChannel.cChannelRecord.CurrencyIsoCode = 'INR';
             thisNewCountryChannel.getsubChannels();
             thisNewCountryChannel.saveChannel();
             
        }
    }
}