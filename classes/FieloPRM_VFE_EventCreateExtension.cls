public with sharing class FieloPRM_VFE_EventCreateExtension {
    
    public FieloPRM_EventCatalog__c event{get;set;}
    public list<FieloPRM_EventCatalog__c> listEvent{get;set;}
    public FieloEE__Member__c member{get;set;}
    public list<CatalogWrapper> listCatalogWrapper{get;set;}
    public Boolean prevDisable {get;set;}
    public Boolean nextDisable {get;set;}
    public Integer pageSize {get;set;}
    public String selectedCountry{get;set;}
    private FieloPRM_UTIls_PagerGeneral pager;
    private map<string,Schema.SObjectField> eventSchemaMaps {get;set;} 
    public string showMessage {get;set;}
    public string severity {get;set;}

    public FieloPRM_VFE_EventCreateExtension(ApexPages.StandardController stdController) {

        this.member = (FieloEE__Member__c)stdController.getRecord();
        member = [SELECT id, F_Country__r.Name, F_PRM_CountryCode__c,Name,FieloEE__Name__c FROM FieloEE__Member__c WHERE id =: member.id LIMIT 1];
        event = new FieloPRM_EventCatalog__c();

        
        prevDisable = true;
        nextDisable = true;
        pageSize = 50;
        selectedCountry = 'All';
        eventSchemaMaps = Schema.SObjectType.FieloPRM_EventCatalog__c.fields.getMap();
        
        //generatePager();
        refreshtable();
        loadList();
    }


    public void refreshtable(){
    
        showMessage = null;
        
       // try{
            list<String> listAux = new list<String>();
            listAux.addAll(eventSchemaMaps.keySet());
            String queryCatalog = ' Select ' +  String.Join(listAux, ',') + ' FROM FieloPRM_EventCatalog__c WHERE F_PRM_isActive__c  = true AND Recordtype.developerName = \'Manual\' ';
            
            
            if(!string.isblank(event.Name)){
                queryCatalog += ' AND name like \'%' + event.Name + '%\' ';
            }
            if(!string.isblank(event.F_PRM_Type__c)){
                queryCatalog += ' AND F_PRM_Type__c like \'%' + event.F_PRM_Type__c + '%\' ';
            }
            if(!string.isblank(event.F_PRM_ValueText__c)){
                queryCatalog += ' AND F_PRM_ValueText__c like \'%' + event.F_PRM_ValueText__c + '%\' ';
            }
            if(!string.isblank(selectedCountry)){
                if(selectedCountry == 'All'){
                    queryCatalog += ' AND  ( F_PRM_Country__r.Name = \'Global\'  ' + ' OR F_PRM_Country__r.CountryCode__c = \'' + member.F_PRM_CountryCode__c + '\')  ' ;
                }else if(selectedCountry == 'Global'){
                    queryCatalog += ' AND  F_PRM_Country__r.Name = \'Global\'   ';
                }else{
                    queryCatalog += ' AND F_PRM_Country__r.CountryCode__c = \'' + selectedCountry + '\'  ' ;
                }
            }
            queryCatalog += ' ORDER BY Name';
            listCatalogWrapper = new list<CatalogWrapper>();
            system.debug('queryCatalog ' + queryCatalog );
            pager = new FieloPRM_UTIls_PagerGeneral(Database.getQueryLocator(queryCatalog) ,pageSize);
            system.debug('pager ' + pager );
            loadList(); 
            system.debug('listCatalogWrapper ' + listCatalogWrapper);
                
      //  }catch(DmlException ex){
       //     system.debug('carch' + ex);
      //      ApexPages.addMessages(ex);
      //  }
        
    }

    public void createEvents(){
        showMessage = null;
        list<FieloEE__Event__C> listEventToInsert = new list<FieloEE__Event__C>();
        for(CatalogWrapper cw : listCatalogWrapper){
            if(cw.selected){
                
                FieloEE__Event__C newEvent = new FieloEE__Event__C();
                newEvent.FieloEE__Member__c = member.id;
                newEvent.Name = cw.eventC.Name;
                newEvent.FieloEE__Value__c = cw.eventC.F_PRM_Value__c;
                newEvent.F_PRM_ValueText__c = cw.eventC.F_PRM_ValueText__c;
                newEvent.FieloEE__Type__c = cw.eventC.F_PRM_Type__c;
                newEvent.F_PRM_Points__c = cw.eventC.F_PRM_Points__c;
                listEventToInsert.add(newEvent);
            }
        }
        if(listEventToInsert.size() > 0 ){
            try{
                insert listEventToInsert;
                //ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.INFO, System.Label.FieloPRM_CL_66_EventCreated));
                severity = 'confirm';
                showMessage = System.Label.FieloPRM_CL_66_EventCreated;
            }catch(DmlException ex){
                severity = 'error';
                showMessage = ex.getMessage();
               // ApexPages.addMessages(ex);
            }
        }else{
            //ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.WARNING, System.Label.FieloPRM_CL_67_MissingEvent));
            severity = 'error';
            showMessage = System.Label.FieloPRM_CL_67_MissingEvent;
        }
        
    }

    
     
    private void loadList(){
        if(pager!=null){
            listCatalogWrapper = new list<CatalogWrapper>();
            list<FieloPRM_EventCatalog__c> listCatalog = (List<FieloPRM_EventCatalog__c>)pager.getRecords();
            system.debug('listCatalog : ' + listCatalog );
            for(FieloPRM_EventCatalog__c eventc: listCatalog){
                listCatalogWrapper.add(new CatalogWrapper(eventc));
            }
            if(listCatalog.size() <= 0){
                listCatalogWrapper = new list<CatalogWrapper>();
            }
            system.debug('listCatalogWrapper: ' + listCatalogWrapper);
            prevDisable = false;
            if(!pager.hasPrevious){prevDisable = true;}
            nextDisable = false;
            if(!pager.hasNext){nextDisable = true;}
        }
    }
     
    public void doPrevious() {
        pager.previous();
        loadList();
    }
     
    public void doNext(){
        pager.next();
        loadList();
    }   
    
    public List<SelectOption> getCountriesOptions() {
        List<SelectOption> countryOptions = new List<SelectOption>();
        countryOptions.add(new SelectOption('All','All'));
        countryOptions.add(new SelectOption('Global','Global'));
        if(member.F_PRM_CountryCode__c != null){
            countryOptions.add(new SelectOption(member.F_PRM_CountryCode__c,member.F_Country__r.Name));
        }
        

        return countryOptions;
    }


    public class CatalogWrapper {

        public FieloPRM_EventCatalog__c eventC {get; set;}
        public Boolean selected {get; set;}

        public CatalogWrapper(FieloPRM_EventCatalog__c event) {
            eventC = event;
            selected = false;
        }
    }
}