/*
    Author          : Accenture Team
    Date Created    : 28/07/2011 
    Description     : Controller extensions for VFP54_WarningMessage.This class displays the 
                      warning/reminder messages, if the appropriate fields are null.
    18-June-2012    Srinivas Nallapati      June12 Mkt Release  BR-1775:Warn lead qualifier to check lead for contact association consistency 
    26-Nov-2012     Srinivas Nallapati      Dec12 Mkt release: changed the Limit <  to <==
*/  
public class VFC54_WarningMessage extends VFC_ControllerBase
{
      
      //Variable Declaration
      public Lead lead{get;private set;}
      
      //Constructor
      public VFC54_WarningMessage(ApexPages.StandardController controller) 
      {
          lead= (Lead)controller.getRecord();
          //Srinivas  added TECH_CheckConsistency__c to the query
          lead = [select Status, SubStatus__c, CallAttempts__c,CallBackLimit__c,SERep__c, SEPartner__c,PurchasedFrom__c,OwnedByPRMContact__c,TECH_CheckConsistency__c, TECH_CheckAccountConsistency__c, PurchasedProduct__c, PurchasedDate__c, Value__c from Lead where id=:lead.Id]; 
      }
      
      /* This method checks if the appropriate fields have values. If not displays warning messages accordingly */
      public pagereference validateLeadFields()
      {
          //Checks if Callback limit is null
          if(lead.CallBackLimit__c==null)
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00516));    

          //Checks if the callback limit is lesser than the call attempts.   Dec12 changed
          if(lead.CallBackLimit__c <= lead.CallAttempts__c)
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00532));     

          //Checks if the SE Rep field is null, when the status is "Closed - Other" and Sub-Status is "Working with SE Rep"
          if(lead.SERep__c==null && lead.Status ==Label.CL00510 && lead.SubStatus__c ==Label.CL00511 && lead.OwnedByPRMContact__c == false)
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00514+' '+sObjectType.Lead.Fields.SERep__c.Label));     

          //Checks if the SE Partner field is null, when the status is "Closed - Other" and Sub-Status is "Working with SE Partner"              
          if(lead.SEPartner__c==null  && lead.Status ==Label.CL00510 && lead.SubStatus__c ==Label.CL00512)
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00514+' '+sObjectType.Lead.Fields.SEPartner__c.Label));     

          //Checks if the any of purchased fields are null, when the status is "Closed - Other" and Sub-Status is "Already Purchased"
          if((lead.PurchasedFrom__c==null ||lead.PurchasedProduct__c==null ||lead.PurchasedDate__c==null ||lead.Value__c==null ) && lead.Status ==Label.CL00510 && lead.SubStatus__c ==Label.CL00513)
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00514+' '+sObjectType.Lead.Fields.PurchasedFrom__c.Label+', '+sObjectType.Lead.Fields.PurchasedProduct__c.Label+', '+sObjectType.Lead.Fields.PurchasedDate__c.Label+', '+sObjectType.Lead.Fields.Value__c.Label));     
          
          //BR-1775:Warn lead qualifier to check lead for contact association consistency
          if(lead.TECH_CheckConsistency__c && !(lead.Status.contains('Closed')) )
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,Label.CLSEP12MKT04)); 
              
          //BR-1774:Warn lead qualifier to check lead for Account association consistency
          if(lead.TECH_CheckAccountConsistency__c && !(lead.Status.contains('Closed')) )
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,Label.CLSEP12MKT06));     
            
          return null;
      }
}