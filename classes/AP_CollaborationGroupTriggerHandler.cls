/*
Author: Siddharth Nagavarapu (GD Solutions)
Purpose: Prevent users from creating new groups
BusinessRequirement : BR-2569
Technical and Functional Specification : https://na7.salesforce.com/069A0000000t1sg
*/
public with sharing class AP_CollaborationGroupTriggerHandler { 

    public static void OnBeforeInsert(CollaborationGroup[] newCollaborationGroups){
        //Example usage        
        
        if(System.Label.CLMAR13SWT02!=null && (System.Label.CLMAR13SWT02+'').split('-').size()==3)
        {
            List<String> dateArray=(System.Label.CLMAR13SWT02+'').split('-');
            Integer year=Integer.valueOf(dateArray.get(2));
            Integer month=Integer.valueOf(dateArray.get(1));
            Integer day=Integer.valueOf(dateArray.get(0));      
            if(System.Today()>=Date.newInstance(year,month,day))
            {
                for(CollaborationGroup newGroup:newCollaborationGroups){
                    if(!Test.isRunningTest())
                    newGroup.name.addError(System.Label.CLMAR13SWT01);
                }
                    
            }       
        }
        

    }
}