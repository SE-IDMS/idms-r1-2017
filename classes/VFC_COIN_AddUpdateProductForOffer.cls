/* 
Purpose:for product information and create a record based on PM0 information

*/
global with sharing class VFC_COIN_AddUpdateProductForOffer {  
  public Offer_Lifecycle__c OfferRecord;
  public Offer_Lifecycle__c offer; 
  public PageReference pageResult;
  public boolean bool = false;
  public boolean IsError{get;set;}
  
  public Offer_Lifecycle__c getOfferRecord(){return this.OfferRecord;}
  public void setOfferRecord(Offer_Lifecycle__c OfferRecordd){this.OfferRecord= OfferRecordd;}

  public Offer_Lifecycle__c getOffer(){return this.Offer;}
  public void setOffer(Offer_Lifecycle__c offerr){this.Offer = offerr;}

  
  Opp_product__c BusinessO = new OPP_Product__c();
  OPP_Product__c ProdLineO = new OPP_Product__c();
  OPP_Product__c ProdFamilyO = new OPP_Product__c();
  OPP_Product__c FamilyO = new OPP_Product__c();

    public Opp_Product__c getBusinessO(){return this.BusinessO;}
    public Opp_Product__c getProdLineO(){return this.ProdLineO;}
    public Opp_Product__c getProdFamilyO(){return this.ProdFamilyO;}
    public Opp_Product__c getFamilyO(){return this.FamilyO;}

    public void setBusinessO(Opp_Product__c bu){this.BusinessO = bu;}
    public void setProdLineO(Opp_Product__c pl){this.ProdLineO = pl;}
    public void setProdFamilyO(Opp_Product__c pf){this.ProdFamilyO = pf;}
    public void setFamilyO(Opp_Product__c f){this.FamilyO = f;}


    public VFC_COIN_AddUpdateProductForOffer (ApexPages.StandardController controller)
    {   IsError = false;
        set<Id> userAccSet = new set<Id>(); 
        id profileId = [Select Id,Name from Profile where name = 'System Administrator'].id;
         OfferRecord = (Offer_Lifecycle__c)controller.getRecord();                                             
                if(OfferRecord.id != null){  
                    bool = true;
                    
                    setOffer([SELECT ID,ownerid,Segment_Marketing_VP__c,Launch_Owner__c,Launch_Area_Manager__c,Marketing_Director__c,Withdrawal_Owner__c FROM Offer_Lifecycle__c WHERE Id = : getOfferRecord().id][0]);
                    userAccSet = new set<Id>{Offer.ownerid,Offer.Segment_Marketing_VP__c,Offer.Launch_Area_Manager__c,Offer.Marketing_Director__c,Offer.Withdrawal_Owner__c};
                   }
                else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.warning,'You must select a valid Offer'));                      
                
                }
            if(!userAccSet.contains(Userinfo.getuserid()) && userinfo.getProfileid()!=profileId) {
                IsError=true;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLMAR16ELLAbFO11));
            }   
    }
        
  @RemoteAction
  global static List<OPP_Product__c> getOthers(String business) {
    String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
    return Database.query(query);       
    }  
    
//==================FUNCTION SELECT================================================================    
  public PageReference PageSelectedFunction(){
  
  //Offer_Lifecycle__c offer; 
  //PageReference pageResult;
  
      //================TO GET THE PRODUCT TECH_PM0CodeInGMR__c CODE===============================
    
    String Business = ApexPages.currentPage().getParameters().get('Business');
    String ProductLine = ApexPages.currentPage().getParameters().get('ProductLine');
    String ProductFamily = ApexPages.currentPage().getParameters().get('ProductFamily');
    String Family = ApexPages.currentPage().getParameters().get('Family');
    
     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Business'));
    
  
  if(getOfferRecord().id!=null){
            pageResult = new PageReference('/'+ getOfferRecord().id);
            
      try{      
        
            setBusinessO( [SELECT Name FROM OPP_Product__c WHERE IsActive__c = TRUE and TECH_PM0CodeInGMR__c =: Business LIMIT 1][0]) ;
            setProdLineO ([SELECT Name FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c =: ProductLine LIMIT 1][0]);
            setProdFamilyO( [SELECT Name FROM OPP_Product__c WHERE IsActive__c = TRUE and TECH_PM0CodeInGMR__c =: ProductFamily LIMIT 1][0]);
            setFamilyO( [SELECT Name FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c =: Family LIMIT 1][0]);      
       
            //setOffer([SELECT ID FROM Offer_Lifecycle__c WHERE Id = : getOfferRecord().id][0]);
            Offer.Product_BU__c = BusinessO.Name;
            Offer.Product_Line__c = ProdLineO.Name;
            Offer.Product_Family__c = ProdFamilyO.Name;
            offer.Family__c = FamilyO.Name;    
            update offer;  
            
    }//end catch
    catch(exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+e.getMessage()));
                }
  }// end if
    
    else{
           return new PageReference('/home.jsp');
        }
   
    return pageResult;        
  }
  

//============================FUNCTION CLEAR===============================  

public PageReference pageClearFunction(){
        PageReference pageResult;
        if(OfferRecord.id!=null){
            //pageResult = new PageReference('/'+ OfferRecord.id);
            try
            {
            
            }
            catch(exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+e.getMessage()));
            }   
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }


public PageReference pageCancelFunction(){
        PageReference pageResult;
        if(OfferRecord.id!=null){
            pageResult = new PageReference('/'+ OfferRecord.id);
            try
            {
               /* if(OfferRecord.status != 'Closed')
                {
                    caseRecord.TECH_LaunchGMR__c = false;
                    DataBase.SaveResult UpdateCase = Database.Update(caseRecord);
                }*/
                pageResult = new ApexPages.StandardController(OfferRecord).view();
            }
            catch(exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+e.getMessage()));
            }   
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
    
}