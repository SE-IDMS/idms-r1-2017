@isTest
public class VFC_UpdateTargetAndSegmentation_TEST{

    static testMethod void showProgramTargetAndSegmentationLvl1() {
        
        ClassificationLevelCatalog__c lvl1 = new ClassificationLevelCatalog__c();
        lvl1.ClassificationLevelName__c = 'Test Class Level1';
        //lvl1.Name='TEST1';
        lvl1.Active__c=true;
        insert lvl1;
        
        

        MarketSegmentCatalog__c sgmt = new MarketSegmentCatalog__c();
        sgmt.MarketSegmentName__c = 'Test Market Segment';
        sgmt.Name='SEG1';
        sgmt.Active__c=true;
        insert sgmt;

        DomainsOfExpertiseCatalog__c pdoe = new DomainsOfExpertiseCatalog__c();
        pdoe.Name='PDOE';
        pdoe.DomainsOfExpertiseName__c='Parent DOE';
        pdoe.Active__c=true;
        insert pdoe;

        DomainsOfExpertiseCatalog__c cdoe = new DomainsOfExpertiseCatalog__c();
        cdoe.Name='CDOE';
        cdoe.DomainsOfExpertiseName__c='Child DOE';
        cdoe.Active__c=true;
        cdoe.ParentDomainsOfExpertise__c=pdoe.Id;
        insert cdoe;

        PartnerProgram__c program = new PartnerProgram__c();
        program.Name ='Test Program';
        insert program;

        // INSERT PartnerProgram Clasifications
        PartnerProgramClassification__c ppc = new PartnerProgramClassification__c();
        ppc.PartnerProgram__c=program.Id;
        ppc.ClassificationLevelCatalog__c=lvl1.Id;
        insert ppc;

        // INSERT PartnerProgram Markets
        PartnerProgramMarket__c ppm = new PartnerProgramMarket__c();
        ppm.PartnerProgram__c=program.Id;
        ppm.MarketSegmentCatalog__c=sgmt.Id;
        insert ppm;


        ProgramDomainsOfExpertise__c ppdoe = new ProgramDomainsOfExpertise__c();
        ppdoe.PartnerProgram__c=program.Id;
        ppdoe.DomainsOfExpertiseCatalog__c=cdoe.Id;
        insert ppdoe;
        
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(program);
        PageReference pageRefWithretURL = Page.VFP_updateTargetAndSegmentation;
        pageRefWithretURL.getParameters().put(Label.CL00330, '/home/home.jsp');
        //pageRefWithretURL.getParameters().put('CLtype', Label.CLOCT13PRM80);
        pageRefWithretURL.getParameters().put('CLtype', Label.CLJUN13PRM03);
        //pageRefWithretURL.getParameters().put('MStype', Label.CLOCT13PRM81);
        pageRefWithretURL.getParameters().put('MStype', Label.CLOCT13PRM81);
        Test.setCurrentPage(pageRefWithretURL);
        VFC_updateTargetAndSegmentation prgProducts = new VFC_updateTargetAndSegmentation(sdtCon1);
        
        prgProducts.selClassificationLevel.add(new SelectOption(lvl1.Id, lvl1.ClassificationLevelName__c));
        prgProducts.selMarketSeg.add(new SelectOption(sgmt.Id, sgmt.MarketSegmentName__c));
        prgProducts.selDE.add(new SelectOption(cdoe.Id, cdoe.DomainsOfExpertiseName__c));
        //prgProducts.selPP.add(new SelectOption(newProd2.Id,newProd2.ProductName__c));
        prgProducts.save();
        
        ApexPages.StandardController sdtCon2 = new ApexPages.StandardController(program);
        PageReference pageRefWithretURL1 = Page.VFP_updateTargetAndSegmentation;
        pageRefWithretURL1.getParameters().put(Label.CL00330, '/home/home.jsp');
        pageRefWithretURL1.getParameters().put('CLtype', Label.CLOCT13PRM80);
        //pageRefWithretURL.getParameters().put('CLtype', Label.CLJUN13PRM03);
        //pageRefWithretURL.getParameters().put('MStype', Label.CLOCT13PRM81);
        pageRefWithretURL1.getParameters().put('MStype', Label.CLOCT13PRM81);
        Test.setCurrentPage(pageRefWithretURL1);
        VFC_updateTargetAndSegmentation prgProducts1 = new VFC_updateTargetAndSegmentation(sdtCon2);
    }
    
    static testMethod void showProgramTargetAndSegmentationLvl3() {
        
        ClassificationLevelCatalog__c lvl1 = new ClassificationLevelCatalog__c();
        lvl1.ClassificationLevelName__c = 'Test Class Level1';
        lvl1.Name='TEST1';
        lvl1.Active__c=true;
        insert lvl1;

        MarketSegmentCatalog__c sgmt = new MarketSegmentCatalog__c();
        sgmt.MarketSegmentName__c = 'Test Market Segment';
        sgmt.Name='SEG1';
        sgmt.Active__c=true;
        insert sgmt;

        DomainsOfExpertiseCatalog__c pdoe = new DomainsOfExpertiseCatalog__c();
        pdoe.Name='PDOE';
        pdoe.DomainsOfExpertiseName__c='Parent DOE';
        pdoe.Active__c=true;
        insert pdoe;

        DomainsOfExpertiseCatalog__c cdoe = new DomainsOfExpertiseCatalog__c();
        cdoe.Name='CDOE';
        cdoe.DomainsOfExpertiseName__c='Child DOE';
        cdoe.Active__c=true;
        cdoe.ParentDomainsOfExpertise__c=pdoe.Id;
        insert cdoe;
        
        PartnerProgram__c program1 = new PartnerProgram__c();
        program1.Name ='Test Program';
        program1.ProgramType__c = Label.CLOCT14PRM60;
        insert program1;

        // INSERT PartnerProgram Clasifications
        /*PartnerProgramClassification__c ppc1 = new PartnerProgramClassification__c();
        ppc1.PartnerProgram__c=program1.Id;
        ppc1.ClassificationLevelCatalog__c=lvl1.Id;
        insert ppc1;*/

        // INSERT PartnerProgram Markets
        PartnerProgramMarket__c ppm1 = new PartnerProgramMarket__c();
        ppm1.PartnerProgram__c=program1.Id;
        ppm1.MarketSegmentCatalog__c=sgmt.Id;
        insert ppm1;


        ProgramDomainsOfExpertise__c ppdoe1 = new ProgramDomainsOfExpertise__c();
        ppdoe1.PartnerProgram__c=program1.Id;
        ppdoe1.DomainsOfExpertiseCatalog__c=cdoe.Id;
        insert ppdoe1;
        
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(program1);
        PageReference pageRefWithretURL = Page.VFP_updateTargetAndSegmentation;
        pageRefWithretURL.getParameters().put(Label.CL00330, '/home/home.jsp');
        //pageRefWithretURL.getParameters().put('CLtype', Label.CLOCT13PRM80);
        pageRefWithretURL.getParameters().put('CLtype', Label.CLJUN13PRM03);
        //pageRefWithretURL.getParameters().put('MStype', Label.CLOCT13PRM81);
        pageRefWithretURL.getParameters().put('MStype', Label.CLOCT13PRM81);
        Test.setCurrentPage(pageRefWithretURL);
        VFC_updateTargetAndSegmentation prgProducts = new VFC_updateTargetAndSegmentation(sdtCon1);
        
        prgProducts.selClassificationLevel.add(new SelectOption(lvl1.Id, lvl1.ClassificationLevelName__c));
        prgProducts.selMarketSeg.add(new SelectOption(sgmt.Id, sgmt.MarketSegmentName__c));
        prgProducts.selDE.add(new SelectOption(cdoe.Id, cdoe.DomainsOfExpertiseName__c));
        //prgProducts.selPP.add(new SelectOption(newProd2.Id,newProd2.ProductName__c));
        prgProducts.save();
        
        ApexPages.StandardController sdtCon2 = new ApexPages.StandardController(program1);
        PageReference pageRefWithretURL1 = Page.VFP_updateTargetAndSegmentation;
        pageRefWithretURL1.getParameters().put(Label.CL00330, '/home/home.jsp');
        pageRefWithretURL1.getParameters().put('CLtype', Label.CLOCT13PRM80);
        //pageRefWithretURL.getParameters().put('CLtype', Label.CLJUN13PRM03);
        //pageRefWithretURL.getParameters().put('MStype', Label.CLOCT13PRM81);
        pageRefWithretURL1.getParameters().put('MStype', Label.CLOCT13PRM81);
        Test.setCurrentPage(pageRefWithretURL1);
        VFC_updateTargetAndSegmentation prgProducts1 = new VFC_updateTargetAndSegmentation(sdtCon2);
        prgProducts1.save();
    }

    static testMethod void showProgramTargetAndSegmentationLvl2() {
        
        List<ClassificationLevelCatalog__c> lstClassLvlCat = new List<ClassificationLevelCatalog__c>();
        ClassificationLevelCatalog__c lvl1 = new ClassificationLevelCatalog__c(
                Name='TEST1', ClassificationLevelName__c = 'Test Class Level1', Active__c=true );
        ClassificationLevelCatalog__c lvl2 = new ClassificationLevelCatalog__c(
                Name='TEST2', ClassificationLevelName__c = 'Test Class Level11', Active__c=true );

        lstClassLvlCat.add(lvl1);
        lstClassLvlCat.add(lvl2);
        insert lstClassLvlCat;

        List<MarketSegmentCatalog__c> lstMktSegment = new List<MarketSegmentCatalog__c>();
        MarketSegmentCatalog__c sgmt1 = new MarketSegmentCatalog__c(
                Name='SEG1', MarketSegmentName__c = 'Test Market Segment', Active__c=true );
        MarketSegmentCatalog__c sgmt2 = new MarketSegmentCatalog__c(
                Name='SEG2', MarketSegmentName__c = 'Test Market Segment2', Active__c=true );
        lstMktSegment.add(sgmt1);
        lstMktSegment.add(sgmt2);
        insert lstMktSegment;

        List<DomainsOfExpertiseCatalog__c> lstDOE = new List<DomainsOfExpertiseCatalog__c>();
        DomainsOfExpertiseCatalog__c pdoe = new DomainsOfExpertiseCatalog__c(
            Name='PDOE', DomainsOfExpertiseName__c='Parent DOE', Active__c=true);

        DomainsOfExpertiseCatalog__c cdoe = new DomainsOfExpertiseCatalog__c(
            Name='CDOE', DomainsOfExpertiseName__c='Child DOE', Active__c=true,  ParentDomainsOfExpertise__c=pdoe.Id);
        DomainsOfExpertiseCatalog__c cdoe2 = new DomainsOfExpertiseCatalog__c(
            Name='CDOE2', DomainsOfExpertiseName__c='Child DOE2', Active__c=true,  ParentDomainsOfExpertise__c=pdoe.Id);

        lstDOE.add(pdoe);
        lstDOE.add(cdoe);
        lstDOE.add(cdoe2);
        insert lstDOE;

        List<ProductLineCatalog__c> lstProducts = new List<ProductLineCatalog__c>();
        ProductLineCatalog__c newProd1 = new ProductLineCatalog__c(Name='ITTEST1',ProductName__c='Test Product1',Active__c=true);
        ProductLineCatalog__c newProd2 = new ProductLineCatalog__c(Name='ITTEST2',ProductName__c='Test Product2',Active__c=true);
        lstProducts.add(newProd1);
        lstProducts.add(newProd2);
        insert lstProducts;

        PartnerProgram__c program = new PartnerProgram__c();
        program.Name ='Test Program';
        insert program;

        // INSERT PartnerProgram Clasifications
        PartnerProgramClassification__c ppc = new PartnerProgramClassification__c();
        ppc.PartnerProgram__c=program.Id;
        ppc.ClassificationLevelCatalog__c=lvl1.Id;
        insert ppc;

        // INSERT PartnerProgram Markets
        PartnerProgramMarket__c ppm = new PartnerProgramMarket__c();
        ppm.PartnerProgram__c=program.Id;
        ppm.MarketSegmentCatalog__c=sgmt1.Id;
        insert ppm;

        ProgramDomainsOfExpertise__c ppdoe = new ProgramDomainsOfExpertise__c();
        ppdoe.PartnerProgram__c=program.Id;
        ppdoe.DomainsOfExpertiseCatalog__c=cdoe.Id;
        insert ppdoe;

        ProgramProduct__c pprd = new ProgramProduct__c();
        pprd.PartnerProgram__c=program.Id;
        pprd.ProductLineCatalog__c=newProd1.Id;
        insert pprd;

        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(program);
        PageReference pageRefWithretURL = Page.VFP_updateTargetAndSegmentation;
        pageRefWithretURL.getParameters().put(Label.CL00330, '/home/home.jsp');
        pageRefWithretURL.getParameters().put('CLtype', Label.CLJUN13PRM03);
        pageRefWithretURL.getParameters().put('MStype', Label.CLJUN13PRM05);
        Test.setCurrentPage(pageRefWithretURL);
        VFC_updateTargetAndSegmentation prgProducts = new VFC_updateTargetAndSegmentation(sdtCon1);

        prgProducts.selClassificationLevel.add(new SelectOption(lvl2.Id, lvl2.ClassificationLevelName__c));
        prgProducts.selMarketSeg.add(new SelectOption(sgmt2.Id, sgmt2.MarketSegmentName__c));
        prgProducts.selDE.add(new SelectOption(cdoe2.Id, cdoe2.DomainsOfExpertiseName__c));
        prgProducts.selPP.add(new SelectOption(newProd2.Id,newProd2.ProductName__c));
        prgProducts.save();
    }
}