/*
    Author          : Deepak
    Date Created    : 07/01/2016
    Description     : Test class for VFC11_ApplicationFreezeHomePage  class 
*/
@isTest(SeeAllData=true)
private class VFC11_ApplicationFreezeHomePage_Test {
    static testMethod void testApplicationFreezeHomePage(){
        
        User usr1 = Utils_TestMethods.createStandardUser('alias');
        usr1.UserBusinessUnit__c='BD';
        usr1.Country__c='IN';
        insert usr1;
        
        Test.startTest();
        VFC11_ApplicationFreezeHomePage.checkApplicationFreeze();
        
        Test.stopTest();
        
        
    }
}