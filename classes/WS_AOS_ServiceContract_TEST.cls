@isTest(SeeAllData=true)
private class WS_AOS_ServiceContract_TEST{
      
    static testMethod void AosServiceCon() 
    {  
         User user = new User(alias = 'user2', email='user2' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing2', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassTriggers__c = 'AP_WorkOrderTechnicianNotification',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12343455');
        insert user;
		Country__c testCountry = Utils_TestMethods.createCountry();
            insert testCountry;

        system.runAs(user)
        {   
			String ServiceContractRT = System.Label.CLAPR15SRV01;
			String ServiceLineRT = System.Label.CLAPR15SRV02;
    
            Account testAccount = Utils_TestMethods.createAccount(user.Id, testCountry.Id);
            testAccount.Name = 'Test';
            testAccount.MarketSegment__c='ID5';
            testAccount.MarketSubSegment__c='B43';
            insert testAccount;
              
			SVMXC__Service_Plan__c splan = new SVMXC__Service_Plan__c();
            splan.SVMXC__Active__c = true;
			splan.Name ='Ultimate';
            splan.SVMXC__Description__c = 'Test';
            insert splan;
        
            
            SVMXC__Service_Contract__c testContract0 = new SVMXC__Service_Contract__c();
            testContract0.SVMXC__Company__c = testAccount.Id;
            testContract0.SoldtoAccount__c=testAccount.Id;
            testContract0.Name = 'Test Contract0';  
            //testContract0.SVMXC__Contact__c = testContact.Id  ;    
            testContract0.SVMXC__Start_Date__c = system.today();
            testContract0.SVMXC__End_Date__c= system.today() + 10;
            testContract0.SVMXC__Renewed_From__c = null;
            testContract0.SVMXC__Service_Plan__c =splan.Id;
           // testContract0.LatestRenewalOpportunity__c =Opp.Id;
            testContract0.OpportunitySource__c ='Amendment';
            testContract0.LeadingBusinessBU__c ='BD';
            //testContract0.DefaultInstalledAtAccount__c=testAccount2.Id;
            testContract0.Tech_ContractCancel__c = true; 
            testContract0.SVMXC__Active__c   = false;
            testContract0.Status__c ='VAL';
            testContract0.RecordTypeId =System.Label.CLAPR15SRV01;
			testContract0.OwnerId = UserInfo.getUserId();
            //sclist.add(testContract0);
			            
            insert testContract0 ;
			List<SVMXC__Service_Contract__c> listresult = new List<SVMXC__Service_Contract__c>();
			        //Service Line
       
			SVMXC__Service_Contract__c testContract1 = new SVMXC__Service_Contract__c();
            testContract1.SVMXC__Company__c = testAccount.Id;
            testContract1.Name = 'Test Contract1';  
            testContract1.SVMXC__Start_Date__c = system.today();
            testContract1.SVMXC__Renewed_From__c = testContract0.Id;
            testContract1.ParentContract__c = testContract0.Id;
            testContract1.RecordTypeid =ServiceLineRT;
            testContract1.Tech_ContractCancel__c = true; 
            testContract1.SVMXC__Active__c   = false;
			testContract1.SVMXC__Service_Plan__c =splan.Id;
             
         
            insert testContract1 ;
            listresult.add(testContract1);
			
			 SVMXC__Site__c site1 = new SVMXC__Site__c();
				site1.Name = 'Test Location';
				site1.SVMXC__Street__c  = 'Test Street';
				site1.SVMXC__Account__c = testAccount.id;
				site1.PrimaryLocation__c = true;
				insert site1;
			
			
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
			ip.SVMXC__Company__c = testAccount.id;
			ip.Name = 'Test Intalled Product ';
			ip.SVMXC__Status__c= 'new';
			ip.GoldenAssetId__c = 'GoledenAssertId1';
			ip.BrandToCreate__c ='Test Brand';
			ip.SVMXC__Site__c = site1.id;
			ip.DeviceTypeToCreate__c ='Devicetp';
			insert ip;
			
			List<SVMXC__Service_Contract_Products__c> coplist = new List<SVMXC__Service_Contract_Products__c>();
			
			SVMXC__Service_Contract_Products__c cov = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c=testContract1.Id,SVMXC__Installed_Product__c=ip.id);
			Test.startTest();
			insert cov;
			coplist.add(cov);
			
			WS_AOS_ServiceContract.getServiceContractDetail(testContract0);
			WS_AOS_ServiceContract.getServiceLineDetails(listresult);
			WS_AOS_ServiceContract.getCoveredProductDetail(coplist);
			
			WS_AOS_ServiceContract.getAOSServiceContracts();
			WS_AOS_ServiceContract.updateAOSServiceContracts(WS_AOS_ServiceContract.getAOSServiceContracts());
        }
    }
}