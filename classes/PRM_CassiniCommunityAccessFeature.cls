public class PRM_CassiniCommunityAccessFeature extends PRM_FeatureCustomPermissionSet {

    public PRM_CassiniCommunityAccessFeature(){
      super();
    }


    public override String getPermissionSetName(){
        return 'CassiniRelatedObjectsCRE';
    }
}