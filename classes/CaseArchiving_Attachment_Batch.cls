global Class CaseArchiving_Attachment_Batch implements Database.batchable<Sobject>{
    
    global Database.QueryLocator start (Database.BatchableContext bc){
       
        String sqlQuery;
        Integer size = Integer.Valueof(System.Label.CLJUL14CCC04);
        
        if(!Test.isRunningTest()){
            if(size==1)
                sqlQuery='Select id,casenumber,ownerId from Case Where Status='+System.label.CLJUL14CCC01 + ' and ArchivedStatus__c ='+System.label.CLJUL14CCC09;
            else
                sqlQuery='Select id,casenumber,ownerId from Case Where ArchivedStatus__c ='+System.label.CLJUL14CCC09+' and Status='+System.label.CLJUL14CCC01 ;
        }
        else 
            sqlQuery='Select id,casenumber,ownerid from Case '; 
                
        System.debug('sqlQuery'+sqlQuery);
    
        return  Database.getQueryLocator(sqlQuery);
    }
    global void execute(Database.BatchableContext Bc,List<Sobject> Scope){
        
        List<ArchiveLog__c> lstArchiveLogs = new List<ArchiveLog__c>();
        List<Attachment> lstInsertAttach = new List<Attachment>();
        List<ArchiveCase__c> lstAcsesTobeDel = new List<ArchiveCase__c>();
        Set<id> aCseIdsToBeDel= new Set<id>();
        Set<id> cseIdsToBeDel= new Set<id>();
        Set<id> cseIdsHeapSize = new Set<id>();
        Set<id> cseIdsHeap = new Set<id>();
        Set<id> cseIdSet = new Set<id>();
        Set<id> cseownerIdSet = new Set<id>();
        Integer heapSize = Integer.ValueOf(System.Label.CLJUN16CCC08);
        Map<String, Case> cseNumRecMap = new Map<String, Case>(); 
        Map<String, ArchiveCase__c> aCseNumberRecMap = new Map<String, ArchiveCase__c>();
        Map<id,ArchiveCase__C> aCseIdRecMap = new Map<id,ArchiveCase__C>();  
        Map<id, User> inactiveUserMap = new Map<id, User>();
        Map<Id,case> cseIdRecMap = new Map<Id,case>();
        
        //User archiveUser = [select id, name from user where name ='ArchiveUser' limit 1];
        for(sObject sObj: Scope){
            Case c= (Case)sObj;
            cseownerIdSet.add(c.ownerId);
            System.debug('Scope case id : '+ c.id);
        }
        if(cseownerIdSet != null && cseownerIdSet.size()>0){
            List<User> ulist = [Select id,name,IsActive from user where id = : cseownerIdSet and IsActive = false];
            inactiveUserMap.putALL(ulist);
        }
        
        for(sObject genralisedObj: Scope){
            sObject sObj = genralisedObj;
            System.debug('Scope genralisedObj : '+genralisedObj);
            Case cseObj = (Case)genralisedObj;
            cseNumRecMap.put(cseObj.CaseNumber,cseObj);
            cseIdRecMap.put(cseObj.id,cseObj);
            cseIdSet.add(cseObj.id);
        }   
        
        if(Test.isRunningTest()){
            List<ArchiveCase__c>  Acasesobj=[Select id,name from ArchiveCase__c limit 1];
            Set<Id> acaseId = new Set<Id>();
             for(Archivecase__c acseobj: Acasesobj){
                aCseIdRecMap.put(acseobj.id,acseobj);
                aCseNumberRecMap.put(acseobj.name,acseobj);
                acaseId.add(acseobj.id);
            }
        }
        List<ArchiveCase__c>  lstAcases=[Select id,name from ArchiveCase__c where Name In:cseNumRecMap.keyset()];
            
            
            for(Archivecase__c acseobj: lstAcases){
                aCseIdRecMap.put(acseobj.id,acseobj);
                aCseNumberRecMap.put(acseobj.name,acseobj);
            }
       
         //Processing Attachments
    System.debug('-- Processing Attachments HeapSize : '+ Limits.getHeapSize());        
        System.debug('-- HeapSize ref : '+ heapSize);
        System.debug('-- HeapSize limit : '+ Limits.getLimitHeapSize());
        
        if(cseIdSet!=null && cseIdSet.size()>0){
            Integer size = 0;
            for(Attachment attachObj: [SELECT Body,BodyLength,ContentType,Description,IsDeleted,IsPrivate,Name,ParentId FROM Attachment where ParentId in:cseIdSet]){
                System.debug('--');
                System.debug('HeapSize : '+Limits.getHeapSize());
                size = size + attachObj.BodyLength;
                System.debug('Attachment size :'+attachObj.BodyLength);
                System.debug('Size :'+size);
                
                if(size > heapSize) {
                    System.debug('Heap Size Limit reached');
                    lstInsertAttach.clear();
                    cseIdsHeap.add(attachObj.ParentID);
                    break;
                }
                
                Attachment newAttachmentObj = new Attachment();
                newAttachmentObj=attachobj.clone(false,true,false,false);
                if(!Test.isRunningTest()){
                    if(cseIdRecMap.containskey(attachObj.ParentId)){
                        if(aCseNumberRecMap.containskey(cseIdRecMap.get(attachObj.ParentId).casenumber)){
                            newAttachmentObj.ParentId = aCseNumberRecMap.get(cseIdRecMap.get(attachObj.ParentId).casenumber).id;  
                        }
                    }   
                 }
                 else{
                    newAttachmentObj.ParentId =aCseNumberRecMap.get('12345').id;
                 }
                
                System.debug('HeapSize : '+Limits.getHeapSize());
                    
                lstInsertAttach.add(newAttachmentObj);
                /*
                if(lstInsertAttach.size() > Limits.getHeapSize())
                {
                    for(Attachment att:lstInsertAttach) {
                        cseIdsHeapSize.add(att.ParentID);
                    }
                    lstInsertAttach.clear();
                }
                */
            }
            
            System.debug('HeapSize : '+Limits.getHeapSize());
            /*
            System.debug('size->'+size);
            System.debug('HeapSize->'+Limits.getLimitHeapSize());
            if(size > Limits.getLimitHeapSize()) {
                System.debug('Heap Size Limit reached');
                lstInsertAttach.clear();
                cseIdsHeap.addAll(cseIdsHeapSize);
            }
            System.debug('Attachment list'+lstInsertAttach);
            */
            if(lstInsertAttach != null && lstInsertAttach.size()>0){
                Database.SaveResult[] sresults  =  Database.insert(lstInsertAttach, false); 
                for(Integer k=0;k<sresults.size();k++ ){
                    Database.SaveResult sr =sresults[k];
                    if(sr.isSuccess()&&!Test.isRunningTest())
                        System.debug('Successfully inserted ID: ' + sr.getId());
                    else{
                        String str = '';
                        String idstr;
                        ArchiveLog__c aLogObj = new ArchiveLog__c();
                        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
                        if(aCseIdRecMap.ContainsKey(lstInsertAttach[k].ParentId)){
                            if(cseNumRecMap.containsKey(aCseIdRecMap.get(lstInsertAttach[k].ParentId).name)){
                                idstr=cseNumRecMap.get(aCseIdRecMap.get(lstInsertAttach[k].ParentId).name).id;
                            }
                        }
                        for(Database.Error err : sr.getErrors()){
                            str =str+err.getMessage();
                        }
                        aLogObj.ErrorMessage__c=str;
                        aLogObj.Date__c=System.now();
                        aLogObj.FailedRecordName__c=lstInsertAttach[k].name;
                        String RecordURL =sfdcBaseURL + '/' +idstr;
                        aLogObj.RecordURL__c=RecordURL;
                        cseIdsToBeDel.add(idstr);
                        lstArchiveLogs.add(aLogObj);
                        aCseIdsToBeDel.add(lstInsertAttach[k].ParentID);
                    }
                }
            }
            if(lstArchiveLogs != null && lstArchiveLogs.size()>0)
                insert lstArchiveLogs;
            // To delete the ArchiveCases   
            for(Id aCseId : aCseIdsToBeDel){
                ArchiveCase__c acse = new ArchiveCase__c(Id=aCseId);
                lstAcsesTobeDel.add(acse);  
            }
            if(lstAcsesTobeDel!=null && lstAcsesTobeDel.size()>0  && !Test.isRunningTest())
                delete lstAcsesTobeDel;
            //Deleting the Cases which are failed from caseid set.  
            cseIdSet.removeAll(cseIdsToBeDel);
            //Update the cases which are remain in caseidset as success 
            List<Case>  lstcses = [select id,archivedStatus__c,archivedDate__c from case where Id in: CseIdSet ];
            for(case cseObj : lstCses){
                cseObj.ArchivedStatus__c='success';
                cseObj.ArchivedDate__c=system.now();
            }
            if(lstcses !=null && lstcses .size()>0)
                Database.SaveResult[] sresults  =  Database.update(lstcses , false);
            //Update the cases which are failed 
            if(cseIdsToBeDel!= null && cseIdsToBeDel.size()>0 ) {
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :cseIdsToBeDel];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Failed';
             
                if(lstFailedCases!=null && lstFailedCases.size()>0)
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
             }
            //Update the cases which failed due to heap size error
            if(cseIdsHeap!= null && cseIdsHeap.size()>0 ) {
                List<Case> lstFailedCases= [select id, ArchivedStatus__c from case where id in :cseIdsHeap];
                for(Case cobj:lstFailedCases)
                    cobj.ArchivedStatus__c = 'Attachment Heap Size';
             
                if(lstFailedCases!=null && lstFailedCases.size()>0)
                Database.SaveResult[] sresults  =  Database.update(lstFailedCases, false);
             }
             //Delete Archive Case which failed due to Heap size error
             if(cseIdsHeap!= null && cseIdsHeap.size()>0 ) {
                List<ArchiveCase__c> lstFailedArchiveCases= [select id from ArchiveCase__c where TECH_CaseID__c in :cseIdsHeap];
          
                if(lstFailedArchiveCases!=null && lstFailedArchiveCases.size()>0)
                delete lstFailedArchiveCases;
             }
        }
    }
    global void finish(Database.BatchableContext bc){
    
    }
    
}