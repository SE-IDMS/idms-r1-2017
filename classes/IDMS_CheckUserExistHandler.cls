/**
•   Created By: Otmane HALOUI
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class is used to check if user exist
**/
public class IDMS_CheckUserExistHandler {
    //Creats dummy data for uims user in UIMS table
    Public Static Void createDummyUIMSUser(String email, String Phone, String IdentyType){
        UIMS_User__c uimsUser = new UIMS_User__c();
        if(IdentyType == 'Email' && !String.IsBlank(email)){
            uimsUser.email__c = email;
        } else if (IdentyType == 'Mobile' && !String.IsBlank(Phone)){            
            uimsUser.phoneId__c = Phone;            
        } else {
            system.debug('Error in parameters');
        }
        
        if (!String.IsBlank(uimsUser.email__c ) || !String.IsBlank(uimsUser.phoneId__c)){
            insert uimsUser;
            system.debug(uimsUser);
        }
    }
    
    
}