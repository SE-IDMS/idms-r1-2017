/*
22-Jul-2013    Srinivas/Shruti    PRM Oct13 Release    Fun: Specialization 
*/
public with sharing class VFC_UpdateSpecializationTargetMarket {
    
    Specialization__c spec;
    list<SpecializationMarket__c> lstExistingMarketSeg = new list<SpecializationMarket__c>();
    list<SpecializationMarket__c> lstExistingSubSeg = new list<SpecializationMarket__c>();
    Set<Id> selOpts1 = new Set<Id>();
    
    public Boolean hasError{get;set;}
    public SelectOption[] selMarketSeg { get; set; }
    public SelectOption[] allMarketSeg{ get; set; }
    public String type {get;set;}
    
    public VFC_UpdateSpecializationTargetMarket(ApexPages.StandardController controller) {
        
        selMarketSeg = new List<SelectOption>();  
        allMarketSeg = new List<SelectOption>();
        set<String> setCL1 = new set<String>();
        type = ApexPages.currentPage().getParameters().get('type');
        spec = (Specialization__c )controller.getRecord();
        lstExistingMarketSeg = [Select MarketSegmentCatalog__r.Name, MarketSegmentCatalog__c,MarketSegmentCatalog__r.MarketSegmentName__c 
                from SpecializationMarket__c where Specialization__c = : spec.Id and MarketSegmentCatalog__r.ParentMarketSegment__c = null order by MarketSegmentCatalog__r.MarketSegmentName__c limit 1000];
        
        if(type == Label.CLJUN13PRM05)//Segment
        {
            if(lstExistingMarketSeg != null && !lstExistingMarketSeg.isEmpty())
            {
                for (SpecializationMarket__c  s : lstExistingMarketSeg ) 
                {
                    selMarketSeg.add(new SelectOption(s.MarketSegmentCatalog__c,s.MarketSegmentCatalog__r.MarketSegmentName__c));
                    selOpts1.add(s.MarketSegmentCatalog__c);
                    
                }  
            }
            
            list<MarketSegmentCatalog__c> lstallMarketSeg1 = new list<MarketSegmentCatalog__c>();
            lstallMarketSeg1 = [SELECT id,name,MarketSegmentName__c FROM MarketSegmentCatalog__c WHERE ParentMarketSegment__c = null order by MarketSegmentName__c limit 1000];        
            for(MarketSegmentCatalog__c MarketSegment : lstallMarketSeg1) 
            {
                if(!(selOpts1.contains(MarketSegment.Id)))
                    allMarketSeg.add(new SelectOption(MarketSegment.Id, MarketSegment.MarketSegmentName__c ));
            }
        }else if(type == Label.CLOCT13PRM81)//Sub-Segment
        {
            list<MarketSegmentCatalog__c> lstMarketSegment = new list<MarketSegmentCatalog__c>();
            if(lstExistingMarketSeg != null && !lstExistingMarketSeg.isEmpty())
            {
                for (SpecializationMarket__c  s : lstExistingMarketSeg ) 
                {
                    setCL1.add(s.MarketSegmentCatalog__r.Name);
                }
                lstMarketSegment = [SELECT id,name,MarketSegmentName__c FROM MarketSegmentCatalog__c WHERE ParentMarketSegment__r.name in:setCL1 order by MarketSegmentName__c limit 1000];
            }
            else
            {
                hasError = true; 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select Market Segment'));
            }
            lstExistingSubSeg = [Select id,MarketSegmentCatalog__c,MarketSegmentCatalog__r.MarketSegmentName__c from SpecializationMarket__c  WHERE Specialization__c  = : spec.Id and MarketSegmentCatalog__r.ParentMarketSegment__c != null order by MarketSegmentCatalog__r.MarketSegmentName__c limit 5000];
            for(SpecializationMarket__c  existingLvl2 : lstExistingSubSeg)
            {
                selMarketSeg.add(new SelectOption(existingLvl2.MarketSegmentCatalog__c,existingLvl2.MarketSegmentCatalog__r.MarketSegmentName__c));
                selOpts1.add(existingLvl2.MarketSegmentCatalog__c);
            }
            for(MarketSegmentCatalog__c allLvl2 : lstMarketSegment )
            {
                if(!(selOpts1.contains(allLvl2.Id)))
                    allMarketSeg.add(new SelectOption(allLvl2.Id, allLvl2.MarketSegmentName__c ));
            }
            
        }        
    }//End of Constructor
    
    
    public PageReference updateMarketSegment()
    {
        Savepoint sp =  Database.setSavepoint();
        try
        {
            set<Id> setSelIds = new set<ID>();
            list<SpecializationMarket__c> lstNewSpeMarket = new list<SpecializationMarket__c>();
            
            for(SelectOption so : selMarketSeg) 
            {
                SpecializationMarket__c speSegment = new SpecializationMarket__c();
                speSegment.Specialization__c= spec.Id;
                speSegment.MarketSegmentCatalog__c = so.getValue();
                lstNewSpeMarket.add(speSegment);
                setSelIds.add(so.getValue());
            }
            //Deleting the existing segment records
            if(type == Label.CLJUN13PRM05 && lstExistingMarketSeg != null && !lstExistingMarketSeg.isEmpty())
            {
                selOpts1.removeAll(setSelIds);
                lstExistingSubSeg = [Select id from SpecializationMarket__c WHERE Specialization__c= : spec.Id and MarketSegmentCatalog__r.ParentMarketSegment__c in:selOpts1 limit 5000];
                delete lstExistingMarketSeg;
                delete lstExistingSubSeg;
                //partProg.MarketSegment2__c = '';
            }
            else if(type == Label.CLJUN13PRM06 && lstExistingSubSeg != null && !lstExistingSubSeg.isEmpty())
            {
                delete lstExistingSubSeg;
            }
            lstExistingMarketSeg.clear();
            
            //Intsert new segment records
            if(lstNewSpeMarket != null && !lstNewSpeMarket.isEmpty())
                insert lstNewSpeMarket;
            
            
        }
        catch(DMLException e)
        {
            Database.rollback(sp);
            hasError = true;    
            for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),'')); System.debug(e.getDmlMessage(i)); 
            } 
                       
            return null;
        }
        
        return new pagereference('/'+spec.Id); 
    }
}