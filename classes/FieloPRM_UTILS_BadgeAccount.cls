/************************************************************
* Developer: Tomás García E                                 *
* Type: rest class                                          *                                       
* Created Date: 30/03/2015                                  *
************************************************************/
public class FieloPRM_UTILS_BadgeAccount{
     
    
    public static String addBadgeToAccount(String Type, Map<String ,List<String>> accToBadges){ 
    
        
        list<FieloPRM_ValidBadgeTypes__c> cusSettingList = new list<FieloPRM_ValidBadgeTypes__c>();
        cusSettingList = FieloPRM_ValidBadgeTypes__c.getall().values();//[SELECT id, FieloPRM_ApiExtId__c, FieloPRM_Type__c, name  FROM FieloPRM_ValidBadgeTypes__c ];
        map<String,map<String,FieloPRM_ValidBadgeTypes__c>> mapValidsTypes = new map<String,map<String,FieloPRM_ValidBadgeTypes__c>>();
        
        String Errors = '';

        list<String> extIdsBadgesList = new list<String>();
        
        if(cusSettingList.isEmpty()){
            return 'error';
        }else{
            for(FieloPRM_ValidBadgeTypes__c typ : cusSettingList ){
                if(!mapValidsTypes.containsKey(typ.FieloPRM_Type__c)){
                    map<String,FieloPRM_ValidBadgeTypes__c> mapApiExtIdListAux = new map<String,FieloPRM_ValidBadgeTypes__c>();
                    mapApiExtIdListAux.put(typ.FieloPRM_ApiExtId__c,typ);
                    mapValidsTypes.put(typ.FieloPRM_Type__c , mapApiExtIdListAux);          
                }else{
                    map<String,FieloPRM_ValidBadgeTypes__c> mapApiExtIdListAux = mapValidsTypes.get(typ.FieloPRM_Type__c);
                    mapApiExtIdListAux.put(typ.FieloPRM_ApiExtId__c, typ);
                    mapValidsTypes.put(typ.FieloPRM_Type__c , mapApiExtIdListAux);  
                }    
            }    
        }
        
        for(String extIdAcc : accToBadges.KeySet()){
            for(String extIdBad : accToBadges.get(extIdAcc)){
                extIdsBadgesList.add(extIdBad);    
            }
        }
        
        system.debug('mapValidsTypes: ' + mapValidsTypes);
        system.debug('Type: ' + Type);
        map<String,FieloPRM_ValidBadgeTypes__c> mapApiNames = new  map<String,FieloPRM_ValidBadgeTypes__c>();
        if(!mapValidsTypes.containsKey(Type)){
            return 'error';
        }else{
            mapApiNames = mapValidsTypes.get(Type);
        }
        
        system.debug('validos: ' + mapApiNames );

        
        set<String> setExternalIds = new set<String>(); // lo uso?
        set<String> extBadgesIds = new set<String>();
        set<String> accountsIds = new set<String>(); 
        map<String,FieloPRM_BadgeAccount__c> keyToBadgeAcc = new map<String,FieloPRM_BadgeAccount__c>();
        map<String,String> accExtIdToAccid = new map<String,String>();
        
        list<Account> Accounts = [SELECT id, PRMUIMSId__c FROM Account WHERE PRMUIMSId__c IN: accToBadges.KeySet()]; 
        
        System.debug('Accounts ' + Accounts );
        
        if(!Accounts.isEmpty()){
            for(Account acc : Accounts){
                accExtIdToAccid.put(acc.PRMUIMSId__c, acc.id); 
                accountsIds.add(acc.id);
            }
            for(String accIExtId : accToBadges.KeySet() ){
                for(String extBadgeId : accToBadges.get(accIExtId ) ){
                    extBadgesIds.add(extBadgeId);
                }
            }
        }   
        
        System.debug('extBadgesIds' + extBadgesIds);    
   
        list<FieloEE__Badge__c> listBadges = [SELECT id, F_PRM_BadgeAPIName__c, F_PRM_Type__c  FROM FieloEE__Badge__c WHERE F_PRM_BadgeAPIName__c IN: extBadgesIds AND F_PRM_Type__c =: Type];
        
        System.debug('listBadges ' + listBadges);
        
        map<String,FieloEE__Badge__c> uniqueNameToBadge = new map<String,FieloEE__Badge__c>(); 
        
        if(!listBadges.isEmpty()){
            for(FieloEE__Badge__c badge : listBadges){
                uniqueNameToBadge.put(badge.F_PRM_BadgeAPIName__c ,badge);
            }
        }
        
        list<FieloEE__Badge__c > listBadgesToInsert = new list<FieloEE__Badge__c >();
        
        // esto sirve para crear los badges que no existen (hay que insertar y desp agregar a la listBadges)
        
        for(String extBadgeId : extBadgesIds){
            system.debug('1 '+  extBadgeId );
            if(!uniqueNameToBadge.containsKey(extBadgeId) && mapApiNames.containskey(extBadgeId)){
                system.debug('2 ' );
                FieloEE__Badge__c badge = new FieloEE__Badge__c();
                badge.name = mapApiNames.get(extBadgeId).name;
                badge.F_PRM_Type__c = Type;
                badge.F_PRM_BadgeAPIName__c = extBadgeId; 
                listBadgesToInsert.add(badge); 
            } 
        }
        
        system.debug('3 listBadgesToInsert  '+ listBadgesToInsert );
        
        insert listBadgesToInsert;
        
        listBadges.addAll(listBadgesToInsert);
        
        if(!listBadges.isEmpty()){
            for(FieloEE__Badge__c badge : listBadges){
                uniqueNameToBadge.put(badge.F_PRM_BadgeAPIName__c ,badge);
            }
        }
        
        list<FieloPRM_BadgeAccount__c> badAccToDelete = new list<FieloPRM_BadgeAccount__c>();
        
        list<FieloPRM_BadgeAccount__c> listBadgeAcc = [SELECT id, F_PRM_Account__c, F_PRM_Badge__c FROM FieloPRM_BadgeAccount__c WHERE F_PRM_Badge__r.F_PRM_BadgeAPIName__c IN: extBadgesIds AND F_PRM_Account__c IN: accountsIds];
        
        System.debug('listBadgeAcc ' + listBadgeAcc );

        if(!listBadgeAcc.isEmpty()){
            for(FieloPRM_BadgeAccount__c badgeAcc : listBadgeAcc ){
                String accidAux = badgeAcc.F_PRM_Account__c;
                String badIdAux = badgeAcc.F_PRM_Badge__c;
                keyToBadgeAcc.put(accidAux+badIdAux ,badgeAcc );
            }
        }
        
        list<FieloPRM_BadgeAccount__c> badAccToInsert = new list<FieloPRM_BadgeAccount__c>();
        

        for(String accIExtId : accToBadges.KeySet() ){
            for(String extBadgeId : accToBadges.get(accIExtId ) ){
                if(uniqueNameToBadge.containsKey(extBadgeId)){
                    if(accExtIdToAccid.containsKey(accIExtId)){
                        String accidAux = accExtIdToAccid.get(accIExtId);
                        String badIdAux = uniqueNameToBadge.get(extBadgeId).id;
                        if(!keyToBadgeAcc.containsKey(accidAux+badIdAux )){
                            FieloPRM_BadgeAccount__c badAccAux = new FieloPRM_BadgeAccount__c();
                            badAccAux.F_PRM_Badge__c = uniqueNameToBadge.get(extBadgeId ).id;
                            badAccAux.F_PRM_Account__c = accExtIdToAccid.get(accIExtId);
                            badAccToInsert.add(badAccAux);       
                        }
                    }else{  
                        Errors += ' Acc with extId: ' + accIExtId + ' and type ' + Type + ' is not valid or cant be created.'; 
                    }
                }else{
                    Errors += ' Acc with extId: ' + accIExtId + ' and type ' + Type + ' is not valid or cant be created.';
                }   
            }
        }
        
        System.debug('badAccToInsert ' + badAccToInsert );
        
        Database.SaveResult[] srListBadgeMemInsert = Database.insert(badAccToInsert , false); 
        
         if(Errors.length() > 1){
            return Errors;
        }     
      
        return null;
    }
    
    

    public static String removeBadgeFromAccount(String Type, Map<String ,List<String>> accToBadges){ 
        set<String> setExternalIds = new set<String>(); // lo uso?
        set<String> extBadgesIds = new set<String>();
        set<String> accountsIds = new set<String>(); 
        map<String,FieloPRM_BadgeAccount__c> keyToBadgeAcc = new map<String,FieloPRM_BadgeAccount__c>();
        map<String,String> accExtIdToAccid = new map<String,String>();        
        
        list<Account> Accounts = [SELECT id, PRMUIMSId__c  FROM Account WHERE PRMUIMSId__c IN: accToBadges.KeySet()]; 
        
        if(!Accounts.isEmpty()){
            for(Account acc : Accounts){
                accExtIdToAccid.put(acc.PRMUIMSId__c, acc.id); 
                accountsIds.add(acc.id);
            }
            for(String accIExtId : accToBadges.KeySet() ){
                for(String extBadgeId : accToBadges.get(accIExtId ) ){
                    extBadgesIds.add(extBadgeId);
                }
            }
        }       
   
        list<FieloEE__Badge__c> listBadges = [SELECT id, F_PRM_BadgeAPIName__c, F_PRM_Type__c  FROM FieloEE__Badge__c WHERE F_PRM_BadgeAPIName__c IN: extBadgesIds AND F_PRM_Type__c =: Type];
        
        map<String,FieloEE__Badge__c> uniqueNameToBadge = new map<String,FieloEE__Badge__c>(); 
        
        if(!listBadges.isEmpty()){
            for(FieloEE__Badge__c badge : listBadges){
                uniqueNameToBadge.put(badge.F_PRM_BadgeAPIName__c ,badge);
            }
        }
        
        list<FieloPRM_BadgeAccount__c> badAccToDelete = new list<FieloPRM_BadgeAccount__c>();
        
        list<FieloPRM_BadgeAccount__c> listBadgeAcc = [SELECT id, F_PRM_Account__c, F_PRM_Badge__c FROM FieloPRM_BadgeAccount__c WHERE F_PRM_Badge__r.F_PRM_BadgeAPIName__c IN: extBadgesIds AND F_PRM_Account__c IN: accountsIds];


        if(!listBadgeAcc.isEmpty()){
            for(FieloPRM_BadgeAccount__c badgeAcc : listBadgeAcc ){
                String accidAux = badgeAcc.F_PRM_Account__c;
                String badIdAux = badgeAcc.F_PRM_Badge__c;
                keyToBadgeAcc.put(accidAux+badIdAux ,badgeAcc );
            }
        }
        
        if(!Accounts.isEmpty()){
            for(String accIExtId : accToBadges.KeySet() ){
                for(String extBadgeId : accToBadges.get(accIExtId ) ){
                    if(uniqueNameToBadge.containsKey(extBadgeId)){
                        String accidAux = accExtIdToAccid.get(accIExtId);
                        String badIdAux = uniqueNameToBadge.get(extBadgeId).id;
                        if(keyToBadgeAcc.containsKey(accidAux+badIdAux )){
                            badAccToDelete.add(keyToBadgeAcc.get(accidAux+badIdAux));    
                        }
                    }else{
                        return 'Badge with extId: ' + extBadgeId + ' and type ' + Type + 'dont exist.';
                    }
                   
                }
            }
        }

        Database.DeleteResult[] srListBadgeMemDelete = Database.delete(badAccToDelete , false);
        
        integer counterUp = 0;
              
        return null;
    }
    
    
}