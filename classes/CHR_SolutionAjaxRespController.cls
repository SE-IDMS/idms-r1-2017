public with sharing class CHR_SolutionAjaxRespController {

	public List<JsonObject> json {get;set;}
	
	public String getResult() {
        String output='';
        for (integer i=0;i<json.size();i++) {
        	if (i>0) {
        		output= output+',';
        	}
        	output= output+json[i].valueToString();
        }
        return output;
    }

	public CHR_SolutionAjaxRespController(){
		
	}
	
	public void doSearch(){
		//Represents the parameters sent by to this controller bt the jquery tree
    	Map<String, String> params = ApexPages.currentPage().getParameters();
    	//Represents the id hierarchy of a tree node
    	List<String> parts = new List<String>();
    	
    	json = new List<JsonObject>();
    	System.debug('root=>'+params.get('root'));
    	parts = (params.get('root')).split(';');
    	String predecessors='';
    	
    	String level0Name='';
    	String level1Name='';
    	String level2Name='';
    	
    	Integer partPosition=1;
    	for(String part:parts){
    		predecessors += (predecessors.length()==0?'':';')+part;
    		
    		System.debug('part=>'+part);
    		System.debug('predecessor=>'+predecessors);
    		String partType = part.split(':')[0];
    		String partId = part.split(':')[1];
    		
    		if (partType=='solutionType') level0Name = partId;
    		
    		//We need to search all the Level2 groups or all the applications
    		if (partType=='level1Group'){
    			level1Name = partId;
    			if(partPosition==parts.size()){
	    			AggregateResult[] groupedResults = [SELECT SolutionGroupL2__c from DMT_Application__c  where SolutionGroupL1__c = :partId Group By SolutionGroupL2__c order by SolutionGroupL2__c];
	    			Set<String> aggregatedValueSet = new set<String>();
					for (AggregateResult ar : groupedResults) {
						if ((String)ar.get('SolutionGroupL2__c')<>null){
							aggregatedValueSet.add((String)ar.get('SolutionGroupL2__c'));
							JsonObject json1 = createJsonObject((String)ar.get('SolutionGroupL2__c'), predecessors+';level2Group:'+(String)ar.get('SolutionGroupL2__c'), true);
		    				json.add(json1);
						}
					}
    			
				
					for(DMT_Application__c app:[select Id,Name,(select Id,Name from Modules__r) from DMT_Application__c where AM_IM__c =:level0Name and  SolutionGroupL1__c = :level1Name and SolutionGroupL2__c=null and SolutionGroupL1__c =:partId order by Name]){
						JsonObject json1 = createJsonObject(app.Name, predecessors+';App:'+app.Id,  (app.Modules__r.size()>0?true:false),'DMTApp');
	    				json.add(json1);
					}
    			}
    		}
    		
    		if (partType=='level2Group'){	
    			System.debug('partPosition=>'+partPosition);
    			System.debug('parts.size()=>'+parts.size());
    			System.debug('level0Name=>'+level0Name);
    			System.debug('level1Name=>'+level1Name);
    			System.debug('partId=>'+PartId);
    			if (partPosition==parts.size()){		
					for(DMT_Application__c app:[select Id,Name,(select Id,Name from Modules__r order by Name) from DMT_Application__c where AM_IM__c =:level0Name and  SolutionGroupL1__c = :level1Name and SolutionGroupL2__c=:partId order by Name]){
						JsonObject json1 = createJsonObject(app.Name, predecessors+';App:'+app.Id, (app.Modules__r.size()>0?true:false),'DMTApp');
	    				json.add(json1);
					}
    			}
    		}
    		
    		if (partType=='App'){
    			System.debug('App partPosition=>'+partPosition);
    			System.debug('App parts.size()=>'+parts.size());
    			if (partPosition==parts.size()){		
    				for(Module__c module:[select Id,Name,(select Id,Name from Modules__r order by Name) from Module__c where Application__c=:partId]){
						JsonObject json1 = createJsonObject(module.Name, predecessors+';Module:'+module.Id, (module.Modules__r.size()>0?true:false),'DMTModule');
	    				json.add(json1);
					}
    				
    			}
    		}
    		
    		if (partType=='Module'){
    			if (partPosition==parts.size()){		
    				for(Module__c module:[select Id,Name from Module__c where ParentModule__c=:partId order by Name]){
						JsonObject json1 = createJsonObject(module.Name, predecessors+';SubModule:'+module.Id, false,'DMTSubModule');
	    				json.add(json1);
					}
    				
    			}
    		}
    		
    		
    		partPosition++; 
    		
    	}
	}
	
	private JsonObject createJsonObject(String text, String id, boolean children) {
    	return createJsonObject(text, id, children, null);
    }
    	
    private JsonObject createJsonObject(String text, String id, boolean children, String classes) {
    	JsonObject jsonObj = new JsonObject();
    	jsonObj.putOpt('text', new JsonObject.value(text));
    	jsonObj.putOpt('id', new JsonObject.value(id));
    	jsonObj.putOpt('hasChildren', new JsonObject.value(children));
    	if (classes != null) {
    		jsonObj.putOpt('classes', new JsonObject.value(classes));
    	}
    	return jsonObj;
    }

}