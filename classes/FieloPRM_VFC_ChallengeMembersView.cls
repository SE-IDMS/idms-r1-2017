/********************************************************************
* Company: Fielo
* Created Date: 23/02/2015
* Description: Page Extension class for page FieloPRM_VFP_ChallengeMembersView 
********************************************************************/
global with sharing class FieloPRM_VFC_ChallengeMembersView{
    
    public string idChallenge{get;set;}
    public FieloCH__Challenge__c challenge{get;set;}
    public list<FieloCH__ChallengeMember__c> challengeMembersJunctionList{get;set;}
    public set<id> MemberIdsSet{get;set;}
    public list<FieloEE__Member__c> challengeMembersList{get;set;}
    
    // DELETE PAGE
    public integer memberSizeDelete{get;set;}
    public Boolean prevDisableDelete {get;set;}
    public Boolean nextDisableDelete {get;set;}
    public Integer pageSizeDelete {get;set;}
    public list<wrapper> wrapperList{get;set;}
    
    //ADD PAGE
    public integer memberSizeaAdd{get;set;}
    public Boolean prevDisableAdd {get;set;}
    public Boolean nextDisableAdd {get;set;}
    public Integer pageSizeAdd {get;set;}
    public list<FieloEE__Member__c> membersToAdd {get;set;} // members that CAN add
    public list<wrapper> wrapperListAdd{get;set;}
    
    // only to display the account realationship in the filters
    public Contact con{get;set;}
    
    // to display add/invite page or delete page.       3 possible values   none, add or delete
    public String mode {get;set;}
    
    // Filters //
    public String acc{get;set;}
    public String channel{get;set;}
    public String segment{get;set;}
    
    private FieloPRM_UTILS_PagerGeneral pagerAdd;
    private FieloPRM_UTILS_PagerGeneral pagerDelete;
        
    public FieloPRM_VFC_ChallengeMembersView(ApexPages.StandardController stdController){
    
        this.Challenge= (FieloCH__Challenge__c)stdController.getRecord();  
        this.idChallenge = challenge.id;
        this.challengeMembersJunctionList = new list<FieloCH__ChallengeMember__c>();
        this.MemberIdsSet = new set<id>(); 
        this.challengeMembersList = new list<FieloEE__Member__c>(); 
        this.acc = '';
        this.channel = ''; 
        this.segment = '';
        this.mode = 'none';
        this.wrapperList = new list <wrapper>();
        this.wrapperListAdd = new list <wrapper>();
        this.con = new Contact();
        
        System.Debug('idChallenge ' + idChallenge );
        
        membersToAdd = new list<FieloEE__Member__c>();
        
        challengeMembersJunctionList= [SELECT id, FieloCH__Challenge2__c, FieloCH__Member__c FROM FieloCH__ChallengeMember__c WHERE FieloCH__Challenge2__c =: idChallenge];
        System.Debug('cantidad d emuchos a mucho en controlador' + challengeMembersJunctionList.Size());
      
        if( !challengeMembersJunctionList.isEmpty() ){
            for(FieloCH__ChallengeMember__c fChallenge : challengeMembersJunctionList){
                MemberIdsSet.add(fChallenge.FieloCH__Member__c );        
            }
        }
        System.Debug('cantidad de members en el set controllador' + MemberIdsSet.Size());
            
        this.challengeMembersList= [SELECT id FROM FieloEE__Member__c WHERE id IN : MemberIdsSet];
       
        // add/ view part
        
        prevDisableAdd = true;
        nextDisableAdd = true;
        pageSizeAdd = 3;       
        generatePagerAdd();
        loadListAdd();
                
        // delete part
        this.memberSizeDelete = challengeMembersList.Size();
        prevDisableDelete = true;
        nextDisableDelete = true;
        pageSizeDelete = 4;         
        generatePagerDelete();
        loadListDelete();

    }
    
    private void generatePagerAdd(){
        String query = 'SELECT Id, FieloEE__Country__c, FieloEE__City__c, FieloEE__IsActive__c, FieloEE__LastName__c, FieloEE__Name__c FROM FieloEE__Member__c WHERE id NOT IN : MemberIdsSet';
               
        pagerAdd = new FieloPRM_UTILS_PagerGeneral(Database.getQueryLocator(query) ,pageSizeAdd);
    }
    
     private void generatePagerDelete(){
        String query = 'SELECT Id, FieloEE__Country__c, FieloEE__City__c, FieloEE__IsActive__c, FieloEE__LastName__c, FieloEE__Name__c  FROM FieloEE__Member__c WHERE id IN : MemberIdsSet';
        
        pagerDelete = new FieloPRM_UTILS_PagerGeneral(Database.getQueryLocator(query) ,pageSizeDelete);
    }
    
    private void loadListAdd(){
        if(pagerAdd!=null){
            membersToAdd = (List<FieloEE__Member__c >)pagerAdd.getRecords();
            system.debug('membersToAdd ' + membersToAdd.Size()  + ' ' + membersToAdd );
            
            if(!membersToAdd.isEmpty()){
                for(FieloEE__Member__c  mem : membersToAdd ){
                    wrapper aux = new wrapper(mem);
                    wrapperListAdd.add(aux);
                }
            }
  
            prevDisableAdd = false;
            if(!pagerAdd.hasPrevious){prevDisableAdd = true;}
            nextDisableDelete = false;
            if(!pagerAdd.hasNext){nextDisableAdd = true;}
        }
    }
     
    private void loadListDelete(){
        if(pagerDelete!=null){
            challengeMembersList = (List<FieloEE__Member__c >)pagerDelete.getRecords();
            
            system.debug('challengeMembersList  ' + challengeMembersList.Size()  + ' ' + challengeMembersList );
            
            if(!challengeMembersList.isEmpty()){
                for(FieloEE__Member__c  mem : challengeMembersList){
                    wrapper aux = new wrapper(mem);
                    wrapperList.add(aux);
                }
            }
  
            prevDisableDelete = false;
            if(!pagerDelete.hasPrevious){prevDisableDelete = true;}
            nextDisableDelete = false;
            if(!pagerDelete.hasNext){nextDisableDelete = true;}
        }
    }
     
    public void doPreviousDelete() {
        pagerDelete.previous();
        loadListDelete();
    }
     
    public void doNextDelete(){
        pagerDelete.next();
        loadListDelete();
    }   
    
     public void doPreviousAdd() {
        pagerAdd.previous();
        loadListAdd();
    }
     
    public void doNextAdd(){
        pagerAdd.next();
        loadListAdd();
    }   
    
    public void dummy(){  // replace - WAITING
       
    } 
    
    public void dummyRenderAddPage(){  // replace - WAITING
       mode = 'add';
    } 
    
    public void dummyRenderDeletePage(){  // replace - WAITING
        mode = 'delete';   
    } 
    
    global class wrapper{
        public FieloEE__Member__c member{get;set;}
        public Boolean isChecked{get;set;}
        
        public String country{get;set;}
        public String city {get;set;}
        public String isActive{get;set;}
        public String lastName{get;set;}
        public String firstName{get;set;} 
            
        public wrapper ( FieloEE__Member__c mem ){
             member = mem;
             isChecked= false;
             country = mem.FieloEE__Country__c ; 
             city = mem.FieloEE__City__c;
             isActive = mem.FieloEE__IsActive__c;
             lastName = mem.FieloEE__LastName__c;
             firstName= mem.FieloEE__Name__c;
             
        } 
    
    }     
}