@isTest
Public class Batch_ProductIsEnriched_TEST 
{
     static testMethod void testBatach()
     { 
         Brand__c brand = new Brand__c();
         brand.SDHBRANDID__c  ='TestSDHBRANDID';
         brand.name ='TestBrand';
         insert brand;
         
         DeviceType__c dt = new DeviceType__c();
         dt.name  = 'TestDeviceType';
         dt.SDHDEVICETYPEID__c = 'TestSDHDEVICETYPE';
         insert dt;
         
         Category__c category = new Category__c();
         category.name ='TestCategory';
         category.SDHCategoryID__c = 'TestSDHCategory';
         insert category;
         product2 prod = new Product2();
         prod.name = 'Test Product';
         prod.Brand2__c = brand.id;
         prod.DeviceType2__c = dt.id;
         prod.CategoryId__c = category.id;
         prod.TECH_IsEnriched__c = false;
         insert prod;
         
           Batch_ProductIsEnriched batch = new Batch_ProductIsEnriched();
          ID batchprocessid = Database.executeBatch(batch);
         
      
     }
}