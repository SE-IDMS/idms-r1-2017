/**
 * Apex Controller for Visualforce Page VFP03_OpptySearch
 * ~ Created by Adrian MODOLEA / Mohamed EL MOUSSAOUI
 * ~ SCHNEIDER SR.0 - RELEASE - November 4th 2010
**/
/* Modified By ACCENTURE IDC
   Modifed for December Release Business Requirement BR-923
   Modified Date: 10/11/2011
*/
 
public without sharing class VFC03_OpptySearch extends VFC_ControllerBase
{
        /*============================================================================
                V.A.R.I.A.B.L.E.S
        =============================================================================*/         
    // current opportunity used for the input search
    public Opportunity opportunity{ get; private set;}

    // current page parameters
    public final Map<String,String> pageParameters = ApexPages.currentPage().getParameters();
    
    // creation is not authorized when search result returns more then searchMaxSize
    private Integer searchMaxSize = Integer.valueOf(System.Label.CL00016);
    
    // collection of opportunities matching the search criteria
    //public List<Opportunity> opportunities {get; private set;}
    
    // indicates whether there are more records than the searchMaxSize.
    public Boolean hasNext {
        get {
            return ((numberOfRecord > searchMaxSize || numberOfRecord == 0)  && showButton );
        }
        set;
    }
    /*Modified By Abhishek*/
    Public String CaseID; 
    Public String PrevURL;// September 2012 Release
    Public string SelectTypevalue{get;set;} // September 2012 Release 
    public Boolean ShowOrfCancelButton { get; set; }// September 2012 Release 
    public Boolean ShowCancelButton { get; set; }// September 2012 Release 
    private Boolean showButton;    
    public Opportunity opp; 
    public Case cse; //store info coming from the case
    
    /*    Modified By ACCENTURE
          Modifed for September Release Business Requirement SLS_3
          Modified Date: 25/07/2011
    */
    
    public Boolean step1 {get{system.debug(step+'rrrrrrrrrr');if(step==1)return true; else return false;} set;}//    Indicates whether to display the search Screen
    public Boolean step2 {get{system.debug(step+'rrrrrrrrrr');if(step==1)return false; else return true;} set;}//    Indicates whether to display the Opportunity Type and Stage Screen
    public Integer step = 1;    
    
    public VCC05_DisplaySearchFields searchController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC05_DisplaySearchFields displaySearchFields;
                displaySearchFields = (VCC05_DisplaySearchFields)getcomponentControllerMap().get('searchComponent');
                system.debug('--------displaySearchFields-------'+displaySearchFields );
                if(displaySearchFields!= null)
                return displaySearchFields;
            } 
            return new VCC05_DisplaySearchFields();
        }
    }
    //    End of September Release Business Requirement SLS_3
    
    //Start of December Release Modification
    public Boolean DisplayResults{get;set;}
    public List<SelectOption> columns{get;set;}   
    public List<DataTemplate__c> fetchedRecords{get;set;} 
    public Long numberOfRecord{get;set;} 
    public VCC06_DisplaySearchResults resultsController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                system.debug('--------displaySearchResults -------'+displaySearchResults );                
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }
    //End of December Release Modification
    
        /*============================================================================
                C.O.N.S.T.R.U.C.T.O.R
        =============================================================================*/    
    public VFC03_OpptySearch(ApexPages.StandardController controller) {
    
        
        opportunity = (Opportunity)controller.getRecord();
        /* start - Sept 2012 Release */
        String Oppregid= ApexPages.CurrentPage().getParameters().get('oppRegFormId');
        if(ApexPages.currentPage().getParameters().get('oppName') !='null'  )
            opportunity.name= ApexPages.currentPage().getParameters().get('oppName');
            
            
        populateContOfDestination();
        If ( Oppregid!=null)
            {
                ShowOrfCancelButton = TRUE;
                ShowCancelButton = FALSE;
            }
        else
            {ShowOrfCancelButton = FALSE;
                 ShowCancelButton = TRUE;
            }    
        /* end - sept 2012 release */
        /*Modified By Abhishek*/
        CaseID= pageParameters.get(system.label.CL00329);
        /*Modified By Accenture IDC for December Release*/
        showButton = true; 
        columns = new List<SelectOption>();        
        DisplayResults = true;
        try
        {
                                 
            System.debug('##### Opportunity: ' + opportunity);
        }
        catch(Exception e)
        {
            System.debug('---- No default Country found');
        }
        //Start of December Release Modification
        numberOfRecord=0;
        columns.add(new SelectOption('Field8__c',sObjectType.opportunity.fields.name.label));// First Column
        columns.add(new SelectOption('Field7__c',sObjectType.opportunity.fields.CountryOfDestination__c.label));
        columns.add(new SelectOption('Field6__c',sObjectType.opportunity.fields.Location__c.label));
        columns.add(new SelectOption('Field5__c',sObjectType.opportunity.fields.StageName.label)); 
        columns.add(new SelectOption('Field4__c',sObjectType.opportunity.fields.CloseDate.label));
        columns.add(new SelectOption('Field3__c',sObjectType.opportunity.fields.Amount.label));
        columns.add(new SelectOption('Field2__c',sObjectType.opportunity.fields.OwnerId.label));
        columns.add(new SelectOption('Field1__c',sObjectType.opportunity.fields.AccountId.label));// Last Column
        columns.add(new SelectOption('Field11__c','Select'));
        fetchedRecords = new List<DataTemplate__c>();
        DataTemplate__c dt = new DataTemplate__C(); 
        dt.field1__c = ' ';   
        fetchedRecords.add(dt);
        //End of December Release Modification
        
         /* start - Sept 2012 release */
        if (Oppregid!=null )
        {
           SelectTypevalue='Single';
        }
        else
        {
           SelectTypevalue='none';
        }
        /* End - Sept 2012 release */
               
    }
        /*============================================================================
                M.E.T.H.O.D.S
        =============================================================================*/                 
    // returns the search string to be used in SOQL query
    public String getQueryString() {
        String strSearch = 'SELECT Id,Name,AccountId, Account.Name, OwnerId, Owner.Name, CountryOfDestination__c,CountryOfDestination__r.Name,Location__c,StageName,CloseDate,Amount FROM Opportunity';
        List<String> whereConditions = new List<String>();
        whereConditions.add('Name LIKE \'%'+Utils_Methods.escapeForWhere(opportunity.Name)+'%\'');
        if(opportunity.Location__c!=null && opportunity.Location__c!='')
                whereConditions.add('Location__c=\''+Utils_Methods.escapeForWhere(opportunity.Location__c)+'\'');
        if(opportunity.CountryOfDestination__c!=null) 
                whereConditions.add('CountryOfDestination__c=\''+opportunity.CountryOfDestination__c+'\'');
        if(whereConditions.size() > 0) {
            strSearch += ' WHERE ';
            for(String condition : whereConditions) {
                strSearch += condition + ' AND ';   
            }
            // delete last 'AND'
            strSearch = strSearch.substring(0, strSearch.lastIndexOf(' AND '));
        }
        strSearch += ' ORDER BY Name';       
        //BR-2756    
        strSearch += ' LIMIT ' + 120; // As no Limits required, we are using pagination to allow user to navigate all the records.
        return strSearch;   
    }
        
        // execute SOQL query
    public PageReference doSearchOpportunity(){
        // check for the opportunity name length
        if(opportunity.Name == null || opportunity.Name.trim().length() < 3) {
                opportunity.Name.addError(System.Label.CL00028);
                return null;
        }
        String s = getQueryString();  //execute soql query. Modification for DECEMBER release: sosl --> soql
        Utils_SDF_Methodology.log('START search: ', s);
 
        Utils_SDF_Methodology.startTimer();
        try {
        //Start of December Release Modifications
        fetchedrecords.clear();  
        numberOfRecord = 0;
        for(Opportunity o : Database.query(s))
        {
            numberOfRecord++;
            if (o!=null && fetchedRecords.size()<100)
            { 
                DataTemplate__c dt = new DataTemplate__c();
                dt.field11__c =o.Name;
                /////////////////////////////RAHUL THANKACHAN////////////////////////////////
                dt.field16__c =o.Id;
                /////////////////////////////RAHUL THANKACHAN////////////////////////////////
                
                dt.field8__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+o.Id+'" target="_blank">'+o.Name+'</a>';
                dt.field7__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+o.CountryOfDestination__c+'" target="_blank">'+o.CountryOfDestination__r.Name+'</a>';
                dt.field6__c = o.Location__c;            
                dt.field5__c = o.StageName;            
                dt.field4__c  = o.CloseDate.format();
                //dt.field3__c = String.valueOf(o.Amount);
                
                if(UserInfo.getUserType().equals('Standard')){
                  dt.field3__c = String.valueOf(o.Amount);
                }else{
                  dt.field3__c = '';
                }
                    
                dt.field2__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+o.OwnerId+'" target="_blank">'+o.Owner.Name+'</a>';
                dt.field1__c = '<a href="'+URL.getSalesforceBaseUrl().toExternalForm() +'/'+o.AccountId+'" target="_blank">'+o.Account.Name+'</a>';
                fetchedRecords.add(dt);
            }
        }
        if(numberOfRecord == 0)
            showButton = False;
        else
            showButton = True;
        if(numberOfRecord  > 100)
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00349)); 
        if(numberOfRecord==0)
        {
            DataTemplate__c dt = new DataTemplate__c();
            dt.field1__c = ' ';   
            fetchedRecords.add(dt);
        }
        //End of December Release Modifications
           if(Test.isRunningTest())
                throw new TestException();
           }
           catch(Exception ex){
                ApexPages.addMessages(ex);
        }
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END search');
        Utils_SDF_Methodology.Limits();
        return null;
    }

    // returns the standard opportunity new page with pre-filled information from the input search         
    public PageReference continueCreation(){
        PageReference newOppPage = new PageReference('/006/e');
         // add parameters from the initial context (except <save_new>)
        for(String param : pageParameters.keySet())
                newOppPage.getParameters().put(param, pageParameters.get(param));
        if(newOppPage.getParameters().containsKey('save_new'))
                newOppPage.getParameters().remove('save_new'); 

        /*  Modified By ACCENTURE
            Modifed for December Release Business Requirement BR-966
            Modified Date: 16/11/2011
       */    
       
       //Opportunity Type selection page will be skipped, if the Opportunity is created from case
        if(caseId!=null || (searchController.level1!=System.Label.CL00355 && searchController.level2!=System.Label.CL00355))
        {      
            
            // add parameters from the input search form
            newOppPage.getParameters().put('opp3', opportunity.Name);
            if(opportunity.CountryOfDestination__c!=null)
            {
                    newOppPage.getParameters().put(System.Label.CL00046, [SELECT Name FROM Country__c WHERE Id=:opportunity.CountryOfDestination__c].Name);
                    newOppPage.getParameters().put(System.Label.CL00047, opportunity.CountryOfDestination__c);
            }
        
        System.debug('*#### opportunity.AccountId: '+opportunity.AccountId);
        if(opportunity.AccountId!=null){
                newOppPage.getParameters().put('opp4', [SELECT Name FROM Account WHERE Id=:opportunity.AccountId].Name);
                newOppPage.getParameters().put('opp4_lkid',opportunity.AccountId );
                //Hari Start
                List<Account> laccount = new List<Account>();
                //Hari End
                laccount = [SELECT LeadingBusiness__c,MarketSegment__c,MarketSubSegment__c FROM Account WHERE Id=:opportunity.AccountId];
                
                if(laccount.size()>0)
                {    
                    newOppPage.getParameters().put(System.Label.CL00308, laccount.get(0).LeadingBusiness__c);
                    newOppPage.getParameters().put(System.Label.CL00306, laccount.get(0).MarketSegment__c);
                    newOppPage.getParameters().put(System.Label.CL00307, laccount.get(0).MarketSubSegment__c);
                }
        }
                     
        if(opportunity.Location__c!=null)    
              newOppPage.getParameters().put(System.Label.CL00048, opportunity.Location__c);            
        
        // do not longer override the creation action
        newOppPage.getParameters().put('nooverride', '1');
        
        //StageName Patch by SBN
        /*String SB_tmp=pageParameters.get('RecordType');
        if(SB_tmp==System.Label.CL00142||SB_tmp==System.Label.CL00143||SB_tmp==System.Label.CL00144)
            newOppPage.getParameters().put('opp11',System.Label.CL00146);
        else if(SB_tmp==System.Label.CL00145)
            newOppPage.getParameters().put('opp11',System.Label.CL00147);*/

       /*   Modified By ACCENTURE
            Modifed for December Release Business Requirement BR-966
            Modified Date: 16/11/2011
       */
        //Opportunity Type selection page will be skipped if the opportunity created by case
        if(caseId==null)
        {   
            //September Release Changes for Type and Stage Update
            //Hari Krishna Added Null check for july Release
            if(searchController.level1 != null){
                if(searchController.level1.equalsIgnoreCase(System.Label.CL00241))
                {
                    newOppPage.getParameters().put(System.Label.CL00216, System.Label.CL00548);
                    newOppPage.getParameters().put(System.Label.CL00549, System.Label.CL00241);
                    if(searchController.level2 != null){
                        if(searchController.level2.equalsIgnoreCase(System.Label.CL00146))
                        {
                            newOppPage.getParameters().put('opp11',System.Label.CL00146);                
                            newOppPage.getParameters().put('RecordType',System.Label.CL00551);
                        }
                        if(searchController.level2.equalsIgnoreCase(System.Label.CL00147))
                        {
                            newOppPage.getParameters().put('opp11',System.Label.CL00147);
                            newOppPage.getParameters().put('RecordType',System.Label.CL00552);
                        }
                        if(searchController.level2.equalsIgnoreCase(System.Label.CL00547))
                        {
                            newOppPage.getParameters().put('opp11',System.Label.CL00547);
                            newOppPage.getParameters().put('RecordType',System.Label.CL00553);
                        }
                    }
                    
                }
                if(searchController.level1.equalsIgnoreCase(System.Label.CL00240))
                {
                    newOppPage.getParameters().put(System.Label.CL00549, System.Label.CL00240);
                    if(searchController.level2 != null){
                        if(searchController.level2.equalsIgnoreCase(System.Label.CL00146))
                        {
                            newOppPage.getParameters().put('opp11',System.Label.CL00146);
                            newOppPage.getParameters().put('RecordType',System.Label.CL00144);
                        }
                        if(searchController.level2.equalsIgnoreCase(System.Label.CL00147))
                        {
                            newOppPage.getParameters().put('opp11',System.Label.CL00147);
                            newOppPage.getParameters().put('RecordType',System.Label.CL00145);
                        }
                    }
                    
                } 
                //September Release Changes for Type and Stage Update
                if(searchController.level1.equalsIgnoreCase(System.Label.CL00724))
                {
                    newOppPage.getParameters().put(System.Label.CL00216, System.Label.CL00548);
                    newOppPage.getParameters().put(System.Label.CL00549, System.Label.CL00724);
                    if(searchController.level2 != null){
                        if(searchController.level2.equalsIgnoreCase(System.Label.CL00146))
                        {
                            newOppPage.getParameters().put('opp11',System.Label.CL00146);                
                            newOppPage.getParameters().put('RecordType',System.Label.CL00551);
                        }
                        if(searchController.level2.equalsIgnoreCase(System.Label.CL00147))
                        {
                            newOppPage.getParameters().put('opp11',System.Label.CL00147);
                            newOppPage.getParameters().put('RecordType',System.Label.CL00552);
                        }
                        if(searchController.level2.equalsIgnoreCase(System.Label.CL00547))
                        {
                            newOppPage.getParameters().put('opp11',System.Label.CL00547);
                            newOppPage.getParameters().put('RecordType',System.Label.CL00553);
                        }
                    }
                    
                }
            }
            
                       
        }
        else
        {
            if(caseID!=null && opp!=null)
            {                
                newOppPage.getParameters().put(CS007_StaticValues__c.getValues('CS007_16').Values__c,opp.KeyCode__c);      //Prepopulates Opportunity Keycode                                                        
            }
            
           
           // Added this below code to prepopulate the Opportunity Source from the Case screen.
           //CL00721 - 00NA000000559YF 
            String StrPromotionScore = [SELECT PromotionSource__c FROM Case WHERE ID =: cse.ID].PromotionSource__c;
            if(StrPromotionScore != null ){
            Map<String, CS_CaseOppSourceMapping__c>  csMapping = CS_CaseOppSourceMapping__c.getAll();
                if(csMapping.containskey(StrPromotionScore)){
                    system.debug('StrPromotionScore ------------->'+StrPromotionScore );
                    newOppPage.getParameters().put(System.label.CL00721,CS_CaseOppSourceMapping__c.getValues(StrPromotionScore).OpportunitySource__c);
                }
            }
            
            
            
            newOppPage.getParameters().put(Label.CL00216, Label.CL00548);                                                  //Prepopulates Opportunity Category
            newOppPage.getParameters().put(Label.CL00549, Label.CL00241);                                                  //Prepopulates Opportunity Type
            newOppPage.getParameters().put('opp11',Label.CL00146);                                                         //Prepopulates Opportunity Stage
            newOppPage.getParameters().put('RecordType',Label.CL00551);                                                    //Prepopulates Record Type
            newOppPage.getParameters().put(CS007_StaticValues__c.getValues('CS007_15').Values__c,Label.CL00584);           //Prepopulates Included in Forecast
        }
        
        if(opp.accountID!=null){
            newOppPage.getParameters().put(system.label.CL00322,opp.accountID);
        }
        else
            if(caseID!=null)
            {   
                System.debug('*#### cse.accountId: '+cse.accountId);                                        
                newOppPage.getParameters().put('opp4_lkid',cse.accountID);                           
            }
        /* End of Modification */
        
        newOppPage.setRedirect(true);        
        }
        else
        {
            newOppPage = null;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Both Opportunity Type and Stage Need to be selected'));
        }
        /******************************************************************
            START: DEC 2012 REL for BR-2293
            Modified By: Hari Krishna 
            Date: 17 Oct 2012
        ******************************************************************/ 
        if(ApexPages.currentPage().getParameters().get(Label.CLDEC12SLS01) == null ){
            if(!Test.isRunningTest() &&newOppPage!=null)
             newOppPage.getParameters().put(Label.CLDEC12SLS02,Label.CL00004);   
        }
        /******************************************************************
            END: DEC 2012 REL for BR-2293
         ******************************************************************/         
        return newOppPage;
    }  
    
    public class TestException extends Exception {}    
    
    /*Modified By Abhishek on 28/05/2011*/   
    public opportunity getOpp()
    {
    system.debug('################################ appel getOpp');
        if (opp == null) {
            opp= new opportunity();
            if(opp.accountID==null)
            {
                if(caseID!=null)
                {                
                    /*   Modified By ACCENTURE
                         Modifed for December Release Business Requirement BR-966
                         Modified Date: 16/11/2011
                    */
                    //Gets the Case Query from the corresponding Custom Settings        
                    String query = CS005_Queries__c.getValues('CS005_15').StaticQuery__c +' where ID = \''+CaseId+'\'';    
                
                    //Queries the corresponding Quote Link
                    cse = Database.Query(query);  
    
                    if(cse!=null)
                    {
                        //opp.AccountId = cse.accountId; 
                        opp.KeyCode__c = cse.CampaignKeyCode__c;
                    } 
                    /* End of Modification */               
                    
                }
            }
        }
        return opp;
    }
    
     // September 2012 Release
     
     /******************************************************************
            START: SEP 2012 REL. ORF TO OPP
      ******************************************************************/
      public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
     {
         System.debug('**Started***');
         VFC03_OpptySearch thisController = (VFC03_OpptySearch)controllerBase;
         DataTemplate__c dt= (DataTemplate__c)obj;
         System.debug('******#######'+dt.field11__c);
         System.debug('****'+ApexPages.currentPage().getParameters().get('oppRegFormId'));
        // OpportunityRegistrationForm__c opregForm=[select id,Status__c, Name from OpportunityRegistrationForm__c where id=:ApexPages.currentPage().getParameters().get('oppRegFormId')];
        OpportunityRegistrationForm__c opregForm = new OpportunityRegistrationForm__c(Id = ApexPages.currentPage().getParameters().get('oppRegFormId'));
         //Anil - ORF Name should not be replaced by Opportunity Name
         //opregForm.Name = dt.field11__c;
         //Anil
         
         ///////////////////////////RAHUL THANKACHAN/////////////////////////////
         ORFwithOpportunities__c junctionObject=new ORFwithOpportunities__c();
         junctionObject.Opportunity__c=dt.field16__c;
         junctionObject.Opportunity_Registration_Form__c=opregForm.id;
         //junctionObject.name=opregForm.name;
         insert junctionObject;
         ///////////////////////////RAHUL THANKACHAN/////////////////////////////
         
         opregForm.Status__c= System.Label.CLSEP12PRM08; // CLSEP12PRM08 - 'Rejected'
         opregForm.RejectReason__c='Duplicate';
         update opregForm;
         
                 
         PageReference pr = new PageReference('/apex/VFP_ConvertORFToOpportunity?id='+opregForm.id);
        // pr .getParameters().put('opp', 'SKIP');
         pr.setRedirect(true);
         return pr;
     }
     
      /*  Overridding the standard Cancel Button  */  
        Public Pagereference doCancel()
        {
        
        system.debug('prevurl**'+ApexPages.currentPage().getParameters().get('PreviousPageURL'));
        PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('PreviousPageURL'));
        pr.setRedirect(true);
        return pr;
        }     
     /******************************************************************
            END: SEP 2012 REL. ORF TO OPP
      *****************************************************************/
  public PageReference goToNextStep()
    {
       /*   Modified By ACCENTURE
            Modifed for December Release Business Requirement BR-966
            Modified Date: 16/11/2011
       */
       
            Pagereference pg = null;
            
            if(caseId!=null)
                pg = continueCreation();
            else
                ++ step;
            
            system.debug('in goTo'+pg);    
            
        /******************************************************************
            START: SEP 2012 REL. ORF TO OPP
        ******************************************************************/    
       if(ApexPages.currentPage().getParameters().get('oppRegFormId')!=null)
       {
       string url='';
       OpportunityRegistrationForm__c opregForm = new OpportunityRegistrationForm__c(Id = ApexPages.currentPage().getParameters().get('oppRegFormId'));
       //string oppRegFromId = ApexPages.currentPage().getParameters().get('oppRegFormId');
       url ='/apex/VFP_ConvertORFToOpportunity?Id='+ opRegForm.Id+ '&skipOppCheck=true'+'&skipConCheck=true';
            pg = new PageReference(url);
            pg.setRedirect(true); 
        }
        /******************************************************************
                END: SEP 2012 REL. ORF TO OPP
        ******************************************************************/
       return pg;   // End of Modification
    }
    
    /******************************************************************
            START: DEC 2012 REL for BR-2290
            Modified By: Hari Krishna 
            Date: 17 Oct 2012
     ******************************************************************/ 
     public void populateContOfDestination(){
     
          if(ApexPages.currentPage().getParameters().get('oppCountry')!=null && ApexPages.currentPage().getParameters().get('oppCountry').length()>0){   
            opportunity.CountryOfDestination__c= ApexPages.currentPage().getParameters().get('oppCountry');
          }
          else{              
              
              List<User> users= [Select id, Country__c from user where id=:UserInfo.getUserId() limit 1];
              if(users.size()>0 && users[0].Country__c!= null ) 
              {             
                  List<Country__c> countries=[select id from Country__c where CountryCode__c=: users[0].Country__c limit 1];
                  if(countries.size()>0)
                  opportunity.CountryOfDestination__c = countries[0].id;
              }
              
              
          }
            
         
     }
     /******************************************************************
            END: DEC 2012 REL for BR-2290
     ******************************************************************/ 
    
               
}