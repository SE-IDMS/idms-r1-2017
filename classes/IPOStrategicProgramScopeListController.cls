/*
    Author              : Gayathri Shivakumar (Schneider Electric)Champions_Name__c
    Date Created        : 28-Feb-2012
    Modification Log    : Partner-Region Added on 11-May-2012, Added Action Column on 11-July-2012
    Description         : Controller to display Program Network 
*/

public with sharing class IPOStrategicProgramScopeListController {

    public IPOStrategicProgramScopeListController(ApexPages.StandardController controller) {
    
    }

public string getPageid(){
return Pageid;
}

public string Selectedscope{get;set;}
public string SelectedEntity{get;set;}
public pagereference Editpage{get;set;}

public string getprogram(){
return program;
}

// Declarations 

public List<IPO_Strategic_Deployment_Network__c> Scope = New List<IPO_Strategic_Deployment_Network__c>();
public string Pageid;
 
public List<ProgramNetwork> prgNetwork = new List<ProgramNetwork>();
public string program;
public List<ProgramNetwork> getProgramNetwork(){

  Pageid = ApexPages.currentPage().getParameters().get('Id');
    List<IPO_Strategic_Program__c> prg = [Select id, Name, Global_Functions__c, Global_Business__c, Operation__c from IPO_Strategic_Program__c where id = :pageid];
    program = prg[0].Name;

    Scope = [SELECT Id, Scope__c, Deployment_Leader__c,Deployment_Leader_Name__c, Champion__r.Firstname, Champion__r.Lastname, Entity__c from IPO_Strategic_Deployment_Network__c where IPO_Strategic_Program_Name__c = :program];
    
    
    // Loop to Check the Scope and the respective Deployment Leaders
    
    // Power Regions Scope
      
    Integer PW;  
    try{  
    if(!(Prg[0].Operation__c == null)){  
        for(String PR: Prg[0].Operation__c.Split(';')){
           PW = 0;
           for(IPO_Strategic_Deployment_Network__c Team:Scope){
            if(PR == Team.Entity__c && Team.Scope__c == Label.Connect_Power_Region && !(Team.Deployment_Leader_Name__c == null) && (Team.Champion__c != null)) {
                prgNetwork.add(new ProgramNetwork(Team.Id,Team.Scope__c,Team.Entity__c, Team.Deployment_Leader_Name__c, Team.Champion__r.Firstname +  ' ' + Team.Champion__r.Lastname));
                PW = 1;
                } // if Ends 
              if((PR == Team.Entity__c && Team.Scope__c == Label.Connect_Power_Region) && (Team.Champion__c != null) && (Team.Deployment_Leader_Name__c == null)){
                       prgNetwork.add(new ProgramNetwork(Team.Id,Label.Connect_Power_Region,PR,'',Team.Champion__r.Firstname +  ' ' + Team.Champion__r.Lastname));
                       PW=1;
                       } //if Ends
                       
             if(PR == Team.Entity__c && Team.Scope__c == Label.Connect_Power_Region && !(Team.Deployment_Leader_Name__c == null) && (Team.Champion__c == null)) {
                prgNetwork.add(new ProgramNetwork(Team.Id,Team.Scope__c,Team.Entity__c, Team.Deployment_Leader_Name__c, ''));
                PW = 1;
                } // if Ends 
                    
             } // For Loop Ends
                 If(PW == 0){
                     prgNetwork.add(new ProgramNetwork('',Label.Connect_Power_Region,PR,'',''));
                     } // If Ends
             } // For Loop Ends
            } // If Ends
           } // Try Ends
           catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
           
               
          // Global Functions Scope with atleast one GSC Region
            integer GFn;
          //try{
            //if((Prg[0].Global_Functions__c.Contains(Label.Connect_GSC)) && (Prg[0].GSC_Region__c != null)){
            //for(String GF: Prg[0].Global_Functions__c.Split(';')){                      
                           
             //for(String GSC: Prg[0].GSC_Region__c.Split(';')){ 
             //  GFn = 0; 
               //  for(IPO_Strategic_Deployment_Network__c Team:Scope){
                  //for(String T_GSC: Team.GSC_Region__c.Split(';')){
                 //  if(GF == Team.Entity__c && Team.Scope__c == Label.Connect_Global_Functions  && !(Team.Deployment_Leader_Name__c == null) && (Team.GSC_Region__c == GSC)) {
                  //  prgNetwork.add(new ProgramNetwork(Team.Id,Team.Scope__c,Team.Entity__c,Team.Deployment_Leader_Name__c, Team.Champions_Name__c));
                 //   GFn = 1;
                 // } // If Ends
               //  }  // For Loop Ends  
               // } // For Loop Ends   
                         
               //If(GFn == 0 && GF == Label.Connect_GSC){
                 //  prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Functions,GF,GSC,''));
                   // } // If Ends
                             
           //  } // For Loop Ends
             
           //  if(GFn == 0 && GF != Label.Connect_GSC){
            //  for(IPO_Strategic_Deployment_Network__c Team:Scope){
           //      if(GF == Team.Entity__c && Team.Scope__c == Label.Connect_Global_Functions  && !(Team.Deployment_Leader_Name__c == null)) {
           //         prgNetwork.add(new ProgramNetwork(Team.Id,Team.Scope__c,Team.Entity__c, Team.Deployment_Leader_Name__c, Team.Champions_Name__c));
           //         GFn = 1;
           //       } // If Ends
           //      } // For Ends
           //     if (GFn == 0)
            //      prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Functions,GF,'',''));
               //  } // If Ends 
              
           //  } //for Loop Ends
          //   } // If Ends
           // }//Try Ends
           // catch(Exception e){
           //   System.Debug('Error:Scope Selection ' + e);
           //   }// Catch Ends
      
       // Global Functions Scope with No GSC Region
           
          try{
            if(!(Prg[0].Global_Functions__c.Contains(Label.Connect_GSC))){          
                
              for(String GF: Prg[0].Global_Functions__c.Split(';')){
              GFn = 0;
                 for(IPO_Strategic_Deployment_Network__c Team:Scope){
                if(GF == Team.Entity__c && Team.Scope__c == Label.Connect_Global_Functions  && !(Team.Deployment_Leader_Name__c == null) && (Team.Champion__c != null) ) {
                    prgNetwork.add(new ProgramNetwork(Team.Id,Team.Scope__c,Team.Entity__c,Team.Deployment_Leader_Name__c, Team.Champion__r.Firstname +  ' ' + Team.Champion__r.Lastname));
                    GFn = 1;
                 } // If Ends
                 if((GF == Team.Entity__c && Team.Scope__c == Label.Connect_Global_Functions) && (Team.Champion__c != null) && (Team.Deployment_Leader_Name__c == null)){
                       prgNetwork.add(new ProgramNetwork(Team.Id,Label.Connect_Global_Functions,GF,'',Team.Champion__r.Firstname +  ' ' + Team.Champion__r.Lastname));
                       GFn=1;
                       } //if Ends
                       
                   if(GF == Team.Entity__c && Team.Scope__c == Label.Connect_Global_Functions  && !(Team.Deployment_Leader_Name__c == null) && (Team.Champion__c == null)) {
                    prgNetwork.add(new ProgramNetwork(Team.Id,Team.Scope__c,Team.Entity__c,Team.Deployment_Leader_Name__c, ''));
                    GFn = 1;
                 } // If Ends
                 
                }  // For Loop Ends
              
              If(GFn == 0 && GF == Label.Connect_GSC){
                    prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Functions,GF,'',''));
                    } // If Ends
                 else if(GFn == 0 && GF != Label.Connect_GSC){
                     prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Functions,GF,'',''));
                     } // If Ends
                
             
             } //for Loop Ends
             } // If Ends
            }//Try Ends
            catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
                   
              
          
          // // Global Business Scope with No Partner Region
          
          integer GBs; 
          try{           
          if(!(Prg[0].Global_Business__c == null) && (Prg[0].Global_Business__c != Label.Connect_Partner)){  
              for(String GB: Prg[0].Global_Business__c.Split(';')){
               GBs = 0;
               for(IPO_Strategic_Deployment_Network__c Team:Scope){
                if(GB == Team.Entity__c && Team.Scope__c == Label.Connect_Global_Business && !(Team.Deployment_Leader_Name__c == null) && (Team.Champion__c != null)) {
                    prgNetwork.add(new ProgramNetwork(Team.Id,Team.Scope__c,Team.Entity__c,Team.Deployment_Leader_Name__c, Team.Champion__r.Firstname +  ' ' + Team.Champion__r.Lastname));
                    GBs = 1;
                } // If Ends
                
                if((GB == Team.Entity__c && Team.Scope__c == Label.Connect_Global_Business ) && (Team.Champion__c != null) && (Team.Deployment_Leader_Name__c == null)){
                       prgNetwork.add(new ProgramNetwork(Team.Id,Label.Connect_Global_Business,GB,'',Team.Champion__r.Firstname +  ' ' + Team.Champion__r.Lastname));
                       GBs=1;
                       } //if Ends
                       
                       if(GB == Team.Entity__c && Team.Scope__c == Label.Connect_Global_Business && !(Team.Deployment_Leader_Name__c == null) && (Team.Champion__c == null)) {
                    prgNetwork.add(new ProgramNetwork(Team.Id,Team.Scope__c,Team.Entity__c,Team.Deployment_Leader_Name__c, ''));
                    GBs = 1;
                } // If Ends
                
               } // For Loop Ends
                 if((GBs == 0) && (GB != Label.Connect_Partner)){
                    prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Business,GB,'',''));
                     } // If Ends
              } // For Loop Ends
             } // If Ends
            }// Try Ends
             catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
              
     // Global Business With Partner Region
     integer PRn;
    // try{
      //      if((Prg[0].Global_Business__c.Contains(Label.Connect_Partner)) && (Prg[0].Partner_Sub_Entity__c != null)){
        //    for(String GB: Prg[0].Global_Business__c.Split(';')){                      
                           
          //   for(String PAR: Prg[0].Partner_Sub_Entity__c.Split(';')){ 
            //   PRn = 0; 
             //    for(IPO_Strategic_Deployment_Network__c Team:Scope){
            //      //for(String T_PAR: Team.Partner_Sub_Entity__c.Split(';')){
             //     if(GB == Team.Entity__c && Team.Scope__c == Label.Connect_Global_Business  && !(Team.Deployment_Leader_Name__c == null) && (Team.Partner_Sub_Entity__c == PAR)) {
             //       prgNetwork.add(new ProgramNetwork(Team.Id,Team.Scope__c,Team.Entity__c,Team.Deployment_Leader_Name__c, Team.Champions_Name__c));
             //       PRn = 1;
            //      } // If Ends
           //      }  // For Loop Ends     
                // } //For Loop Ends      
           //    If(PRn == 0 && GB == Label.Connect_Partner){
           //      prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Business,GB,'',''));
           //         } // If Ends
                             
            // } // For Loop Ends
             
             
           //  } //for Loop Ends
           //  } // If Ends
          //  }//Try Ends
          //  catch(Exception e){
           //   System.Debug('Error:Scope Selection ' + e);
            //  }// Catch Ends
           
    return prgNetwork;
  }

public pagereference AddDeploymentNetwork(){
string url;
//url = Label.IPOProgramNetworkADD + program  + '&retURL= ' + Pageid;
url =  Label.IPOProgramNetworkADD + Pageid;
PageReference pageRef = new PageReference(url);
pageRef.getParameters().put(Label.IPOProgramNetworkADDKey,program);
pageRef.setRedirect(true);
return pageRef;
}

/*
public pagereference EditDeploymentNetwork(){
string url;
//url = Label.IPOProgramNetworkADD + program  + '&retURL= ' + Pageid;
url =  Label.IPOProgramNetworkADD + Pageid;
PageReference pageRef = new PageReference(url);
pageRef.getParameters().put(Label.IPOProgramNetworkADDKey,program);
pageRef.getParameters().put(Label.IPOEditNetworkScope,Selectedscope);
pageRef.getParameters().put(Label.IPOEditNetworkEntity,Selectedentity);
Pageref.setRedirect(true);
EditPage=pageRef;
return Pageref;
}
 */


    public class ProgramNetwork{
    public String id{get;set;}
    public String Scope {get; set;}
    public String Entity {get; set;}
    public String Deployment_Leader {get;set;}
    public String Champions{get;set;}

    public ProgramNetwork(string id, string s,string e, string d, string c){
        this.id = id;
        this.Scope=s;
        this.Entity=e;
        this.Deployment_Leader=d;
        this.Champions=c;

    }
   }
}