public class AP_PRMCFL_HOME_Controller {
    
    public List<ContentWorkspace> getContentWorkspace() {
        String recordTypeId = Label.CLOCT15CF022;
        //String recordTypeId = '012q00000004bK8AAI';
        System.debug('recordtypeId = ' + recordTypeId);
        List<ContentWorkspace> myList1= [select id, Name, Description from ContentWorkspace Where DefaultRecordTypeId = :recordTypeId];
        //String myid = '058q00000000ZCAAA2';
        //List<ContentWorkspace> myList1= [select id, Name, Description from ContentWorkspace order by CreatedDate desc LIMIT 1];
        
        return myList1;
    }    
    
    public AP_PRMCFL_HOME_Controller () {
        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO, Label.CLOCT15CF026 ));
    }


    public PageReference redirectPageLogin(){
        system.debug('redirectPageLogin UserInfo.getSessionId(): ' + UserInfo.getSessionId());
        String sessionId = UserInfo.getSessionId();
        if ((sessionId==null) || (sessionId=='')) {   
            return new pageReference('/collaboration');    
        } else return null;

    }

}