@isTest
public Class AP_RelatedSymptom_Test{
    static testmethod void unitTest1(){
     test.startTest();
      BusinessRiskEscalationEntity__c breEntity = new BusinessRiskEscalationEntity__c (Name='Test Name', Entity__c='Test Entity-1', Location_Type__c='Plant');
        insert breEntity;
        Problem__c prblm = new Problem__c(Title__c = 'Test Problem - Divya', RecordTypeid =Label.CLI2PAPR120014, Sensitivity__c = 'Confidential', Status__c = 'New', AccountableOrganization__c = breEntity.id, Severity__c ='Minor', AffectedProductLines__c = '');  
         insert prblm;
         
        List<RelatedSymptom__c> relSymplst = new list<RelatedSymptom__c>();
        RelatedSymptom__c rs1 = new RelatedSymptom__c(Problem__c = prblm.id, Symptom__c = 'test symp4647', SubSymptom__c = 'test Sub symp4647'); 
        relSymplst.add(rs1);
         RelatedSymptom__c rs2 = new RelatedSymptom__c(Problem__c = prblm.id, Symptom__c = 'test symp464734', SubSymptom__c = 'test Sub symp464745'); 
         relSymplst.add(rs2);
          RelatedSymptom__c rs3 = new RelatedSymptom__c(Problem__c = prblm.id, Symptom__c = 'test symp464745', SubSymptom__c = 'test Sub symp464745'); 
          relSymplst.add(rs3);
          insert relSymplst;
          
          delete rs3;
         test.stopTest();
    }
}