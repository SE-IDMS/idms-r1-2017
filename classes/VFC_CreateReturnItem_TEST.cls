@isTest
private class VFC_CreateReturnItem_TEST {


static testMethod void myUnitTest() {

System.debug('#### START test method for VFC_CreateReturnItem ####');
    
    Country__c CountryObj = Utils_TestMethods.createCountry();
    Insert CountryObj;
    
    User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
    Insert RRTestUser ;
  /*  
    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=CountryObj.Id;
    */
    Account accountObj = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', Country__c=CountryObj.Id);
    
    insert accountObj;
    
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
   
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    
    CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';    
    CaseObj.Quantity__c = 4;
    //CaseObj.CommercialReference__c='xbtg5230';
    Insert CaseObj;
    
    Case CaseObj1= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    
    CaseObj1.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj1.Symptom__c =  'Installation/ Setup';
    CaseObj1.SubSymptom__c = 'Hardware';    
    CaseObj1.Quantity__c = 4;
    CaseObj1.CommercialReference__c='xbtg5230';
    CaseObj1.ProductFamily__c='HMI SCHNEIDER BRAND';
    Insert CaseObj1;
    
    
      ReturnItemsApprovalMatrix__c RIAM = new ReturnItemsApprovalMatrix__c(
                                 AccountCountry__c = CountryObj.Id,
                                 CommercialReference__c='xbtg5230',
                                 ProductFamily__c='HMI SCHNEIDER BRAND',
                                 CustomerResolution__c = 'REP',                                                             
                                 Category__c = '4 - Post-Sales Tech Support',  
                                 Reason__c = 'Installation/ Setup',
                                 SubReason__c ='Hardware'
                                 );
      Insert RIAM;
    
    BusinessRiskEscalationEntity__c RCorgEntity = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1'
                                            );
    Insert RCorgEntity;    
    BusinessRiskEscalationEntity__c RCorgSubEntity = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity'
                                            );
    Insert RCorgSubEntity;
    BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg;  
    
    BusinessRiskEscalationEntity__c RCorg1Entity = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST2',
                                            Entity__c='RC Entity-2'
                                            );
    Insert RCorg1Entity;
    BusinessRiskEscalationEntity__c RCorg1SubEntity = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST2',
                                            Entity__c='RC Entity-2',
                                            SubEntity__c='RC Sub-Entity2'
                                            );
    Insert RCorg1SubEntity;
    BusinessRiskEscalationEntity__c RCorg1 = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST2',
                                            Entity__c='RC Entity-2',
                                            SubEntity__c='RC Sub-Entity2',
                                            Location__c='RC Location2',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street2',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City2',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg1;  
   
   RMAShippingToAdmin__c RPDTNew = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Repair defective product',
                                 CommercialReference__c='xbtg5230',
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                              //    ProductFamily__c='HMI SCHNEIDER BRAND',
                                 TECH_GMRCode__c='02BF6D'
                                 );
    Insert RPDTNew;
   
    
    RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Repair defective product1',
                                 CommercialReference__c='xbtg5230',
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c='02BF6D'
                                 );
    Insert RPDT;
    RMAShippingToAdmin__c RPDT1 = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Repair defective product',                                
                                 Ruleactive__c = True,
                                 ReturnCenter__c = RCorg1.Id
                                 );
    Insert RPDT1;
    
    ResolutionOptionAndCapabilities__c ROC = new ResolutionOptionAndCapabilities__c (                                             
                                             ProductCondition__c = 'Defective',
                                             CustomerResolution__c = 'REP',
                                             ProductDestinationCapability__c = 'Repair defective product'
                                             );
    Insert ROC; 
    
    RMA__c RR = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
    Insert RR;
    
    Test.SetCurrentPageReference(New PageReference('Page.VFC_CreateReturnItem'));
    ApexPages.Standardcontroller SC = New ApexPages.StandardController(RR);
    System.CurrentPageReference().getParameters().put('newid',RR.Id);
    VFC_CreateReturnItem CRI = new VFC_CreateReturnItem(SC);
    
   
    CRI.doSearchReturnCenters();
    //CRI.doSave();
    
    //RR.ReturnedProductsCount__c=1;
    //RR.TECH_CheckReturnAddressOnRIequals__c=1;
   
   RMA__c RRNew = new RMA__c (
                Case__c= CaseObj1.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
    Insert RRNew;
   
   RMA__c RRi = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
    Insert RRi;
    Test.SetCurrentPageReference(New PageReference('Page.VFC_CreateReturnItem'));
    ApexPages.Standardcontroller SCri = New ApexPages.StandardController(RRi);
    System.CurrentPageReference().getParameters().put('newid',RRi.Id);
    VFC_CreateReturnItem newCRI = new VFC_CreateReturnItem(SCri);
    newCRI.doSearchReturnCenters();
    
     RMA_Product__c RIi = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name ='xbtg5230',
                        ProductFamily__c='HMI SCHNEIDER BRAND',
                        RMA__c=RRi.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1'
                        );
    Insert RIi;
   
    
    
    RMA_Product__c RI = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name ='xbtg5230',
                        ProductFamily__c='HMI SCHNEIDER BRAND',
                        RMA__c=RR.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1'
                        );
    Insert RI;  
    
    RRi.Status__c = 'Validated';
    update RRi; 
    test.startTest();

    Test.SetCurrentPageReference(New PageReference('Page.VFC_CreateReturnItem'));
    ApexPages.Standardcontroller SC1 = New ApexPages.StandardController(RR);
    System.CurrentPageReference().getParameters().put('newid',RI.Id);
    System.CurrentPageReference().getParameters().put('rigmrcode','02BF6DRG16NBX07632');
    
    VFC_CreateReturnItem CRI1 = new VFC_CreateReturnItem(SC1);
    VFC_ControllerBase basecontroller = new VFC_ControllerBase();
    VCC06_DisplaySearchResults componentcontroller1 =CRI1.resultsController; 
    CRI1.doSearchReturnCenters();  
    
    Boolean newRMA=False;

     sObject newRI = new DataTemplate__c (
                    
                    field1__c = 'Repair defective product',
                    field3__c = 'RC Street',
                    field4__c = 'RC City',
                    field5__c = '1234567',
                    field6__c = CountryObj.Id,
                    field7__c = CountryObj.Id,
                    field8__c = 'RC Additional address',
                    field9__c = '12345',
                    field10__c = '12345678',
                    field11__c = 'RC County',
                    field13__c = RPDT.ReturnCenter__c,
                    field14__c ='123456789',
                    field15__c = '123456789',
                    field16__c ='RC ContactName',
                    field17__c ='RC LocalStreet',
                    field18__c ='RC Additional Address',
                    field19__c ='RC LocalCity',
                    field20__c ='RC LocalCounty',
                    field21__c = RI.CustomerResolution__c
     );
     CRI1.PerformAction(newRI,CRI1.getThis());
     //CRI1.doSave();
     CRI1.doCancel();  
     
          
   RMA__c RR2 = [Select id, Status__c, Approver__c,ReturnedProductsCount__c, TECH_CheckReturnAddressOnRI__c from RMA__c where id=: RR.id];
   List<RMA_Product__c> RItest = [Select id, RMA__c,ProductFamily__c,ApprovalStatus__c, SentBacktoCustomer__c,CustomerRepairPONumber__c, AccountToBeInvoicedForTheRepair__c   from RMA_Product__c where RMA__c=: RR2.id];
   system.debug('##RItest ##'+RItest );
   List<RMA__c> RRlist = [Select id, Status__c,Case__c, Approver__c,ReturnedProductsCount__c, TECH_CheckReturnAddressOnRI__c from RMA__c where id=: RRNew.id];

   Test.SetCurrentPageReference(New PageReference('Page.VFC_CreateReturnItem'));
   ApexPages.Standardcontroller SC3 = New ApexPages.StandardController(RR2);
   

   VFC_ReturnRequestSubmitForApproval RRSubmitForApproval = new VFC_ReturnRequestSubmitForApproval(SC3);
  

       
   system.debug('##ReturnedProductsCount##'+RR2.ReturnedProductsCount__c);
   system.debug('##TECH_CheckReturnAddressOnRI##'+ RR2.TECH_CheckReturnAddressOnRI__c);
   system.debug('##Status##'+RR2.Status__c);
   system.debug('##Approver##'+ RR2.Approver__c);
   RRSubmitForApproval.processDetails();
   RRSubmitForApproval.doApprove();
   RRSubmitForApproval.doCancel(); 
   
   system.debug('##Status2##'+RR2.Status__c);
   ApexPages.Standardcontroller SC4 = New ApexPages.StandardController(RR2);
   VFC_ReturnRequestSubmitForApproval RRSubmitForApproval1 = new VFC_ReturnRequestSubmitForApproval(SC4);
   
   RRSubmitForApproval1.processDetails();
    test.stopTest();
/*   
   ApexPages.Standardcontroller SC5 = New ApexPages.StandardController(RRi);
   VFC_ReturnRequestSubmitForApproval RRSubmitForApproval2 = new VFC_ReturnRequestSubmitForApproval(SC5);
   
   RRSubmitForApproval2.processDetails();  
*/

/*
AP35_addItemToRMA.updateShippingAddress(RItest);
AP35_addItemToRMA.insertFirstReturnItem(RRlist);
*/

}
    
}