/*********
Release :OCT2015

**********/


public class VFC_CRReassignFourceManualSel{

    public string strComtRequestId;
    public Boolean validateErr{get; set;}
    public String selectedValue { get; set; }
    Public Boolean isTechicalComplaintRequest{get; set;}
    public  ComplaintRequest__c lcrRecord;
    public  ComplaintRequest__c compl{get;set;}
    public string accountOrgId{get;set;}
    public boolean isErrorMessg{get;set;}
    public Boolean isBusinessRiskPrasent {get; set;} 
    public list<wrapperForceManualOrg> forecManualActOrg{get;set;}
    public Boolean isTarOrgPresent {get; set;} 
    public string strAccountableOrg; 
    public string strDepartment;
    public List<ComplaintRequestRoutingRule__c> lstComRequestRule = new List<ComplaintRequestRoutingRule__c>();
    public map<id,ComplaintRequestRoutingRule__c> MapComRRRule  = new map<id,ComplaintRequestRoutingRule__c>();
    
    public string IskeepInvolved;
    public string strTargetOrga;
    public Boolean isErr {get; set;} 
    public string strAccountableOrgName{ get; set; } 
    public boolean KeepInvolvedFMS{ get; set; }
    
    public id useridOfIssueOwner{get;set;}
    public string CrEmailId{get;set;}
    public boolean InvolvedSelectable{get;set;}
   
     public VFC_CRReassignFourceManualSel(ApexPages.StandardController stdController) {
        isBusinessRiskPrasent=false;
        InvolvedSelectable=false;
        KeepInvolvedFMS=true;
        isErrorMessg=false;
        strComtRequestId = ApexPages.currentPage().getParameters().get('CRId');
        strTargetOrga = ApexPages.currentPage().getParameters().get('TargetOrgId');
        IskeepInvolved= ApexPages.currentPage().getParameters().get('SelectKeepInvd');
        system.debug('HANU'+strComtRequestId+'test'+strTargetOrga +'TEST6'+IskeepInvolved);
        
        if (!Test.isRunningTest())
        {
            stdController.addFields(new List<String>{'TECH_EventType__c','IssueOwner__c','ContactEmailAccountableOrganization__c','TECH_FinalResolutionUpdatable__c','Final_Resolution__c','TECH_Symptoms__c','CommercialReferenceOrdered__c','FamilyOrdered__c','FamilyReceived__c','PartNumberReceived__c','CommercialReferenceReceived__c','TECH_Forward__c','TECH_Answer__c','TECH_SwitchOffEmailNotification__c','ForceManualSelection__c','AccountableOrganization__c', 'ForceManualSelectionReason__c', 'ReportingEntity__c', 'Category__c','Reason__c','SubReason__c','Status__c','AccountableOrganization__r.Name','Department__c','TECH_FirstEsDepartment__c','TECH_FirstEsOrganization__c','ReportingDepartment__c','TECH_MaxStackCounter__c'});
        }        
        this.lcrRecord = (ComplaintRequest__c)stdController.getRecord();
        strAccountableOrg = this.lcrRecord.AccountableOrganization__c;  
        //compl = new ComplaintRequest__c(); 
        forecManualActOrg=new list<wrapperForceManualOrg>();
        lstComRequestRule = new List<ComplaintRequestRoutingRule__c>();
         isErr = false;
        strDepartment = this.lcrRecord.Department__c;
         
        useridOfIssueOwner =this.lcrRecord.IssueOwner__c;
        system.debug('HAna----------Stack'+strDepartment);
        CrEmailId =this.lcrRecord.ContactEmailAccountableOrganization__c;
         lcrRecord.ForceManualSelection__c = True;
        if(IskeepInvolved!=null && IskeepInvolved!='') {
          boolean.valueof(IskeepInvolved);
          InvolvedSelectable=boolean.valueof(IskeepInvolved);
        }
       
      // searchAcctOrganization();
    }
    
    
    public Void searchAcctOrganization() {
        isBusinessRiskPrasent=false;
        isErrorMessg=false;
        forecManualActOrg=new list<wrapperForceManualOrg>();
        lstComRequestRule = new List<ComplaintRequestRoutingRule__c>();
        List<BusinessRiskEscalationEntity__c> BRE = new List<BusinessRiskEscalationEntity__c>();
        if(accountOrgId!=null || accountOrgId!='' ) {
            system.debug('HAHAN-------->'+accountOrgId);
           
            BRE =  [select Name,Id,Country__c,RCCountry__c,ContactEmail__c,Location_Type__c,(Select id, Name, User__c, Role__c,Department__c, AdditionalInformation__c,BusinessRiskEscalationEntity__c from EntityStakeholders__r ORDER BY Department__c, AdditionalInformation__c, User__c ASC ), (Select id, Name, Owner__c,Role__c, ContactEmail__c,Department__c, AdditionalInformation__c,Organization__c from OrganizationStakeholdersForCR__r ORDER BY Department__c, AdditionalInformation__c,Owner__r.Name ASC) from BusinessRiskEscalationEntity__c where name=:accountOrgId];                    
            
            if(BRE.size() > 0) {
                for(BusinessRiskEscalationEntity__c breObj:BRE) {
                system.debug('Inside------->HANAA'+breObj.EntityStakeholders__r);
                      system.debug('Inside------->'+breObj);
                    forecManualActOrg.add(new wrapperForceManualOrg(MapComRRRule.get(breObj.id),breObj));//MapComRRRule.get(breObj.id),
                
                }
            }
              
             system.debug('List------->'+forecManualActOrg);
            
        
        } else {
        
            // ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLAPR14I2P29)); // 'Please NOTE that the Accountable Organization cannot be blank'
                 isErrorMessg = True;
                  // acctPage = null;
        
        }
        if(forecManualActOrg.size() > 0) {
        
        wrapperForceManualOrg forObj1=forecManualActOrg[0];
        
        
        
        List<EntityStakeholder__c>  esHolder=new List<EntityStakeholder__c> ();
        List<OrganizationStakeholdersForCR__c>  orgStkHolder=new List<OrganizationStakeholdersForCR__c> ();
         
         
            
            for(WrapperStackHolder sk: forObj1.wshlist) {
                if(sk.shl2!=null) {
                    esHolder.add(sk.shl2);
                }
                if(sk.shl!=null) {
                orgStkHolder.add(sk.shl);
                }
            }
            system.debug('******esHolder***'+esHolder);
            system.debug('******orgStkHolder***'+orgStkHolder);
            system.debug('******ContactEmail__c***'+forObj1.accntOrg.ContactEmail__c);
            if(orgStkHolder.size()> 0) {
                isBusinessRiskPrasent=true;
            }
            else if (forObj1.accntOrg.ContactEmail__c!=null) {
                isBusinessRiskPrasent=true;
            }else if(esHolder.size()> 0) {
                isBusinessRiskPrasent=true;
            }
            
            if(!isBusinessRiskPrasent) {
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLOCT15I2P52)); // 'Please NOTE that the Accountable Organization cannot be blank'
             isErrorMessg = True;
             isBusinessRiskPrasent=false;
            
            } 
         
        
            
        
        }else {
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLOCT15I2P53)); // 
             isErrorMessg = True;
             isBusinessRiskPrasent=false;
        }


    }   

    
     public class wrapperForceManualOrg 
    {
        public ComplaintRequestRoutingRule__c targOrg {get; set;}
        public BusinessRiskEscalationEntity__c accntOrg {get; set;}
        Public ComplaintsRequestsEscalationHistory__c hFromOrg {get; set;}
        public List<WrapperStackHolder> wshlist {get;set;}
        public boolean selected {get; set;}
        public boolean isEmailNotBlack {get; set;}
        Public String sFlag{get; set;}

        public wrapperForceManualOrg(complaintRequestRoutingRule__c toOrg, BusinessRiskEscalationEntity__c acctOrg)
        {
            System.debug('--------constructor'+acctOrg.EntityStakeholders__r);
            targOrg = toOrg;
            //hFromOrg = fromOrg;
            accntOrg = acctOrg;
            selected = false;
            isEmailNotBlack=false;
            wshlist  = new List<WrapperStackHolder>();
            System.debug('--------constructor'+acctOrg.OrganizationStakeholdersForCR__r);
            if(acctOrg.OrganizationStakeholdersForCR__r != null && acctOrg.OrganizationStakeholdersForCR__r.size()>0)
            {
                for(OrganizationStakeholdersForCR__c eshobj :acctOrg.OrganizationStakeholdersForCR__r ){
                    if(eshobj.Role__c=='Complaint/Request Owner')
                    {
                        WrapperStackHolder wsh = new WrapperStackHolder();
                        wsh.shl = eshobj ;
                        wsh.shid =eshobj.id; 
                        wsh.shpid = eshobj.Organization__c;
                        wshlist.add(wsh);
                        sFlag='OSCR';
                    }    
                }
                
            }
            
            if(wshlist.size()==0 && accntOrg.ContactEmail__c!=null) {
                isEmailNotBlack=true;
            }
             System.debug('--------EntityStakeholders__r'+acctOrg.EntityStakeholders__r.size());
            if(wshlist.size()==0 && isEmailNotBlack==false && acctOrg.EntityStakeholders__r.size()>0)
            {
                 for(EntityStakeholder__c eshobj :acctOrg.EntityStakeholders__r ){
                    if( eshobj.role__c=='CS&Q Manager') {
                        WrapperStackHolder wsh = new WrapperStackHolder();
                        wsh.shl2 = eshobj ;
                        wsh.shid =eshobj.id;
                        wsh.shpid = eshobj.BusinessRiskEscalationEntity__c;
                        wshlist.add(wsh);
                        sFlag='ES';
                    }
                }
             
            }
        }
    }
    
     public class WrapperStackHolder{    
        public Boolean isSelect{get;set;}
        public EntityStakeholder__c shl2{get;set;}
        public OrganizationStakeholdersForCR__c shl{get;set;}
        public ID shid{get;set;}
        public ID shpid {get; set;}
       
    }  
     
     public PageReference setFlag()
    {
          
            if(isErr)
            { 
                isErr=false;
            }
            return null;
    } 
    
     /** Custom save functionality used in the button SAVE. */
    public PageReference save()
    {   
        map<id,id> mapSH = new map<id,id>();
        isErr=false;

        lcrRecord.KeepInvolved__c =KeepInvolvedFMS; //boolean.valueof(IskeepInvolved);  // April 2015 release
        boolean IsSelectFinal=false;
        system.debug('**lcrRecord.category__c '+ forecManualActOrg);
        wrapperForceManualOrg targRecord ;
        PageReference acctPage;
      
            for(wrapperForceManualOrg cTarg : forecManualActOrg )
            {
                if(cTarg.selected == true)
                {
                    targRecord = cTarg;
                    
                }
            }          
            if((targRecord != null && targRecord.selected == true))
            {
                if(targRecord.accntOrg!= null){
                
                    //lcrRecord.AccountableOrganization__c =  targRecord.targOrg.TargetOrganization__c;  /** Switch the Account Org  to Targ Org on Selection of a Targ Org */ 
                    
                    if(targRecord.wshlist.size()>0 && selectedValue!=''){
                    
                    system.debug('@@@@@@@@@Save14');
                         for(WrapperStackHolder sk: targRecord.wshlist)
                         { system.debug('@@@@@@@@@Save5');
                             mapSH.put(sk.shid, sk.shpid);
                         }
                    
                    }                                      
                    
                    // If Generic email is not available at Org level  and stakeholders are available
                    If(mapSH.size()>0 && mapSH.get(selectedValue) == targRecord.accntOrg.id){
                      system.debug('@@@@@@@@@Save6');
                     system.debug('$$ SELECTED $$'+selectedValue);
                      system.debug('$$ Target Organization Id $$'+ targRecord.targOrg.TargetOrganization__c);
                    
                     
                    lcrRecord.AccountableOrganization__c =  targRecord.accntOrg.id;  /** Switch the Account Org  to Targ Org on Selection of a Targ Org */ 
                     system.debug('### targRecord.sFlag'+  targRecord.sFlag) ;
                     for(WrapperStackHolder sk: targRecord.wshlist)
                     {
                  
                         If(selectedValue !='' && selectedValue == sk.shid && targRecord.sFlag == 'OSCR')
                         {
                         system.debug('**** Stack Holders ****');
                         system.debug ('*** ');
                        system.debug('@@@@@@@@@Save7');
                         OrganizationStakeholdersForCR__c  Os= sk.shl;
                         system.debug('*** Os.ContactEmail__c ***'+ Os.ContactEmail__c);
                          system.debug( '*** STKHOLDER OWNER ***' + Os.Owner__c);
                          lcrRecord.Department__c=Os.Department__c;  // Oct2014 Release - Ram Chilukuri 10Sept2014
                          if(Os.ContactEmail__c!=null)
                          {system.debug('@@@@@@@@@Save8');
                           //lcrRecord.Department__c=null; // Ram chilukuri added 29th Sept2014 as part of Oct 2014 release.
                            lcrRecord.IssueOwner__c=null;
                            lcrRecord.ContactEmailAccountableOrganization__c=Os.ContactEmail__c;
                          }
                          else if(Os.Owner__c!=null)
                          {system.debug('@@@@@@@@@Save9');
                            lcrRecord.IssueOwner__c=Os.Owner__c;
                            lcrRecord.ContactEmailAccountableOrganization__c=null;
                          }
                         }
                         else if(selectedValue !='' && selectedValue == sk.shid && targRecord.sFlag == 'ES')
                         {system.debug('@@@@@@@@@Save10');
                            EntityStakeholder__c  es= sk.shl2;
                            lcrRecord.IssueOwner__c=es.User__c;
                            lcrRecord.Department__c = es.Department__c; // Added as part of April 2015 release
                            lcrRecord.ContactEmailAccountableOrganization__c=null;
                         }     
                        
                     }
                   }
                  //else if(selectedValue=='' || (selectedValue!=''&& mapSH.get(selectedValue) != targRecord.accntOrg.id))  // when stakeholders are available and without selecting any radio button if clicks on Select Button.
                         
                         else if( ((selectedValue==''&& targRecord.sFlag == 'ES') && (targRecord.accntOrg.ContactEmail__c==null)) ||((selectedValue=='' && targRecord.sFlag == 'OSCR') && (targRecord.accntOrg.ContactEmail__c!=null || targRecord.accntOrg.ContactEmail__c==null)) || (selectedValue!=''&& mapSH.get(selectedValue) != targRecord.accntOrg.id))
                         {system.debug('@@@@@@@@@Save12');
                             lcrRecord.AccountableOrganization__c = strAccountableOrg;
                             system.debug('***selectedValue'+selectedValue);
                             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please click to choose the department before clicking the Select button' ));//Label.CLAPR14I2P24
                            // isRadioSelected = false;
                             isErr= true;
                             IsSelectFinal=true;
                             acctPage = null;
                         }
                   
                   else if ( targRecord.accntOrg.ContactEmail__c!=null) 
                   {system.debug('@@@@@@@@@Save11');
                     // this is in the case when there is no stakeholders available and generic contact email available
                     system.debug('***targRecord.targOrg.ContactEmail__c'+targRecord.targOrg.ContactEmail__c);
                     lcrRecord.IssueOwner__c=null;
                     lcrRecord.Department__c=null; // Ram chilukuri added 29th Sept2014 oct2014 realtese
                     lcrRecord.ContactEmailAccountableOrganization__c = targRecord.accntOrg.ContactEmail__c;
                     lcrRecord.AccountableOrganization__c =  targRecord.accntOrg.id;  /** Switch the Account Org  to Targ Org on Selection of a Targ Org */ 
                     
                   }
        
                  
              }
               
            }
            system.debug('$$acctpage'+acctPage);
            System.debug('$$LctRecord'+lcrRecord);
            if(!isErr)
            {
            acctPage = new PageReference('/'+lcrRecord.Id);
            acctPage.setRedirect(true);
            }
      
          //Hanamanth B P 
          set<id> CRequsSet = new set<id>(); 
          CRequsSet.add(lcrRecord.id);
       
        
      // try {
       lcrRecord.TECH_EventType__c=label.CLDEC13LCR08;
       system.debug('1'+lcrRecord.AccountableOrganization__c);
       system.debug('strAccountableOrg'+strAccountableOrg);
       system.debug('lcrRecord.Department__c'+lcrRecord.Department__c);
       system.debug('=strDepartment'+strDepartment);
       if(lcrRecord.AccountableOrganization__c==strAccountableOrg && lcrRecord.Department__c==strDepartment) {
         ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'CR Cannot be Reassigned to the same Accountable  Organization and Department')); //'Please enter either Commerical Reference Received or Part Number Received.'
            isErr = True;
            acctPage = null;
       
       }
       if(lcrrecord.AccountableOrganization__c == lcrrecord.ReportingEntity__c && lcrrecord.Department__c == lcrrecord.ReportingDepartment__c  && (lcrrecord.Final_Resolution__c == null || lcrrecord.Final_Resolution__c == '') && isErr!=true ) {
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLOCT15I2P54)); //'Please enter either Commerical Reference Received or Part Number Received.'
            isErr = True;
            acctPage = null;
       }   
            System.debug('-------LctRecord'+lcrRecord);
            if(!isErr) {
             update lcrRecord;
               system.debug('** lcrRecord.Department__c'+ lcrRecord.Department__c+'** lcrRecord.Department__c'+ strDepartment);
               AP_Stack.addtheOrgToStack(lcrRecord.KeepInvolved__c,strAccountableOrg,string.valueof(lcrRecord.AccountableOrganization__c),lcrRecord.Department__c,CRequsSet,strDepartment,CrEmailId,useridOfIssueOwner,Integer.valueof(lcrRecord.TECH_MaxStackCounter__c));
        
            }              
     
        return acctPage;
    }

}