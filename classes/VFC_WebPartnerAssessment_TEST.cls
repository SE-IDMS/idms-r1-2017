/**
16-Sep-2013
Shruti Karn  
PRM Oct13 Release    
Initial Creation: test class for VFC_WebPartnerAssessment
 */
@isTest
private class VFC_WebPartnerAssessment_TEST {
     
     static testMethod void myUnitTest() {
          PartnerAssessment__c assessment = new PartnerAssessment__c();
          Account acc = Utils_TestMethods.createAccount();
          insert acc;
          assessment__c assmnt = new assessment__C();
          assmnt.Name = 'Test';
          insert assmnt;
          ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(assessment);
          ApexPages.currentPage().getParameters().put('Company','Test');
          ApexPages.currentPage().getParameters().put('companyid','Test');
          ApexPages.currentPage().getParameters().put('name','Test');
          ApexPages.currentPage().getParameters().put('account',acc.id);
          ApexPages.currentPage().getParameters().put('SEAccountID','Test');
          ApexPages.currentPage().getParameters().put('partnerPRGID','Test');
          ApexPages.currentPage().getParameters().put('programLevelID','Test');
          ApexPages.currentPage().getParameters().put('assessmentID',assmnt.Id);
          ApexPages.currentPage().getParameters().put('specializationID','Test');
          VFC_WebPartnerAssessment myPageCon1 = new VFC_WebPartnerAssessment(sdtCon1);
     }
}