public class AP_InfoQualifContact
{
    public static void setCompletion(List<InfoQualifContact__c> infoQualifContacts)
    {
        List<Id> contactsId = new List<Id>();

        for (InfoQualifContact__c infoQualifContact : infoQualifContacts) {
            contactsId.add(infoQualifContact.contact__c);
        }

        Map<Id, Contact> contacts = new Map<Id, Contact>([
            SELECT Id, AccountId, MarcomPrefPhone__c
            FROM Contact
            WHERE Id in : contactsId
        ]);

        List<Id> accountsId = new List<Id>();
        for (
            Contact contact : [
                SELECT Id, AccountId
                FROM Contact
                WHERE Id IN :contactsId
            ]
        ) {
            accountsId.add(contact.AccountId);
        }

        Map<Id, Account> accounts = new Map<Id, Account>([
            SELECT Id, ClassLevel1__c, Owner.UserBusinessUnit__c
            FROM Account
            WHERE Id IN :accountsId
        ]);

        List<InfoQualif__c> infoQualifs = [
            SELECT Id, account__c, classLevel1Approb__c, ClassLevel1ValidationDate__c
            FROM InfoQualif__c
            WHERE account__c IN :accountsId
        ];

        Map<Id, InfoQualif__c> infoQualifsByAccount = new Map<Id, InfoQualif__c>();
        for (InfoQualif__c infoQualif : infoQualifs) {
            infoQualifsByAccount.put(infoQualif.account__c, infoQualif);
        }

        for (InfoQualifContact__c infoQualifContact : infoQualifContacts) {
            Integer completionCount = 0;

            // Default fields
            Set<String> validationDateFields = new Set<String> {
                'FirstnameValidationDate__c',
                'SalutationValidationDate__c',
                'ReportsToIdValidationDate__c',
                'JobTitleValidationDate__c',
                'JobFunctionValidationDate__c',
                'WorkPhoneValidationDate__c',
                'EmailValidationDate__c',
                'ProfilingValidationDate__c',
                'CollectifsValidationDate__c',
                'CanauxCommValidationDate__c',
                'HeurePrefereeTelephoneValidationDate__c'
            };

            if (infoQualifContact.contact__c != null) {

                Account currentAccount = accounts.get(
                    (Id) contacts.get(infoQualifContact.contact__c).get('AccountId')
                );
                InfoQualif__c currentInfoQualif = infoQualifsByAccount.get(
                    (Id) contacts.get(infoQualifContact.contact__c).get('AccountId')
                );
                Contact currentContact = contacts.get(infoQualifContact.contact__c);

                if (currentAccount != null) {
                    // PPEB Tableautiers
                    if (AP_InfoQualif.isPpebTableautiers(currentAccount, currentInfoQualif)) {
                        validationDateFields.addAll(
                            new Set<String> {
                                'JobDescrValidationDate__c',
                                'ReseauxSociauxValidationDate__c',
                                'SchneiderPromoteurValidationDate__c',
                                'AnimValidationDate__c',
                                'GeekValidationDate__c',
                                'ACOValidationDate__c',
                                'CatDEValidationDate__c',
                                'CatBTValidationDate__c',
                                'CatINDValidationDate__c',
                                'CatPrismaValidationDate__c',
                                'CatNSXValidationDate__c',
                                'CatUEValidationDate__c'
                            }
                        );
                    }

                    // DIS
                    if (AP_InfoQualif.isDis(currentAccount, currentInfoQualif)) {
                        validationDateFields.addAll(
                            new Set<String> {
                                'DisChiffreurValidationDate__c',
                                'DisTypesClientsSuivisDateTime__c',
                                'DisReseauxExpertsValidationDate__c'
                            }
                        );

                        validationDateFields.remove('CollectifsValidationDate__c');
                        validationDateFields.remove('JobFunctionValidationDate__c');
                        validationDateFields.remove('CanauxCommValidationDate__c');
                        validationDateFields.remove('HeurePrefereeTelephoneValidationDate__c');
                    }

                    // IND System Integrators
                    if (AP_InfoQualif.isIndSystemIntegrators(currentAccount, currentInfoQualif)) {
                        validationDateFields.remove('CollectifsValidationDate__c');
                        validationDateFields.remove('HeurePrefereeTelephoneValidationDate__c');
                    }

                    // IND Tableautiers
                    if (AP_InfoQualif.isIndTableautiers(currentAccount, currentInfoQualif)) {
                        validationDateFields.remove('CollectifsValidationDate__c');
                        validationDateFields.remove('HeurePrefereeTelephoneValidationDate__c');
                    }

                    // IND OEMs
                    if (AP_InfoQualif.isIndOems(currentAccount, currentInfoQualif)) {
                        validationDateFields.remove('CollectifsValidationDate__c');
                        validationDateFields.remove('HeurePrefereeTelephoneValidationDate__c');
                    }

                    if (currentContact.MarcomPrefPhone__c == 'N') {
                        validationDateFields.remove('HeurePrefereeTelephoneValidationDate__c');
                    }

                    Datetime maxValidationDate = null;
                    for (String field : validationDateFields) {
                        if (infoQualifContact.get(field) != null) {
                            if (
                                maxValidationDate == null ||
                                (Datetime) infoQualifContact.get(field) > maxValidationDate
                            ) {
                                maxValidationDate = (Datetime) infoQualifContact.get(field);
                            }

                            completionCount++;
                        }
                    }

                    infoQualifContact.CompletionCount3__c = completionCount;
                    infoQualifContact.Completion3__c = (completionCount * 1.0 / validationDateFields.size()) * 100;
                    infoQualifContact.LastValidationDate__c = maxValidationDate;
                }
            }
        }
    }
}