/*
Author:Siddharth N(GD Solutions)
Purpose:Search GMR for product information and create a record based on selection
*/
public with sharing class VFC_AddUpdateProductForPartOrderLine {
    public List<ProductWrapper> theList{get;set;}{theList=new List<ProductWrapper>();}
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public SVMXC__RMA_Shipment_Line__c rmashippmentlinerecord{get;set;}
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    public VFC_AddUpdateProductForPartOrderLine(ApexPages.StandardController controller)
    {
        rmashippmentlinerecord=(SVMXC__RMA_Shipment_Line__c )controller.getRecord();                        
        if(rmashippmentlinerecord.id!=null)
        {
            rmashippmentlinerecord=[select Id,CommercialReference__c, GMRValidation__c  from SVMXC__RMA_Shipment_Line__c where id=:rmashippmentlinerecord.id];          
            if(rmashippmentlinerecord.CommercialReference__c!=null)
                pageParameters.put('commercialReference',rmashippmentlinerecord.CommercialReference__c);
        }        
    }
    
    public PageReference performSelect()
    {  
     if(jsonSelectString=='')    
        jsonSelectString=pageParameters.get('jsonSelectString');
        System.debug('jsonSelectString'+jsonSelectString);
    WS04_GMR.gmrDataBean DBL= (WS04_GMR.gmrDataBean) JSON.deserialize(jsonSelectString, WS04_GMR.gmrDataBean.class);       
       
        if(rmashippmentlinerecord.id!=null)
        {           
                 if(DBL.commercialReference != null)
                      rmashippmentlinerecord.CommercialReference__c = DBL.commercialReference;                        
                      rmashippmentlinerecord.GMRValidation__c = 'Validated';                          
        }
        else
        {
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Invalid Part Order line'));
           return null;
       }       

       try{
        upsert rmashippmentlinerecord;
        PageReference rmashippmentpage;            
        rmashippmentpage=new PageReference('/'+rmashippmentlinerecord.id);                        
        return rmashippmentpage;
    }
    catch(DmlException dmlexception)
    {
        for(integer i = 0;i<dmlexception.getNumDml();i++)
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,dmlexception.getDmlMessage(i)));    
        return null;                  
    }   
    catch(Exception ex)
    {
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));    
        return null;                  
    }  
}
public void performSelectFamily()
{   
}

}