global class CaseArchiving_Delete_Batch implements Database.batchable<Sobject>{

    global Database.QueryLocator start (Database.BatchableContext bc){
        String Query ;
        String strYear=System.Label.CLJUL14CCC02;
        integer intClsDate = Integer.Valueof(strYear);
        DateTime clsDate =System.today().addYears(intClsDate);  
        System.debug(clsDate);
        Integer size = Integer.Valueof(System.Label.CLJUL14CCC04);
         if(!Test.isRunningTest()){
                if(size==1)
                    Query ='select id ,CaseNumber from case where caseNumber = ' +System.label.CLJUL14CCC07+ ' ';
                else    
                    //Query = 'select id ,CaseNumber from case where archivedstatus__c ='+ System.Label.CLJUL14CCC05+ ' ';
                    Query = 'select id ,CaseNumber from case where archivedstatus__c ='+ System.Label.CLJUL14CCC05+ ' and ClosedDate<=:clsDate limit '+size;
         }
         else
             Query='Select id,casenumber from Case';
        return  Database.getQueryLocator(Query);
    }
    global void execute(Database.BatchableContext Bc,List<Sobject> Scope){
        set<Id> setCses = new set<Id>(); 
        List<case> lstCsesToBeDel = new List<case>(); 
        Map<String,case> cseNumberRecMap = new Map<String,case>();
        set<String> aCseNameSet = new set<String>();
        for(sObject genralisedObj: Scope){
            sObject sObj = genralisedObj;
            Case cseObj = (Case)genralisedObj;
            setCses.add(cseObj.id); 
            cseNumberRecMap.put(cseObj.CaseNumber,cseObj);
        }
        for(ArchiveCase__c ArchiveObj : [select id,Name from ArchiveCase__c where Name in : cseNumberRecMap.keySet()]){
            aCseNameSet.add(ArchiveObj.Name);       
        }
        List<case> lstCses=[select id,caseNumber from case where id=:setCses];
        for(case cseobj : lstCses){
            if(aCseNameSet.contains(cseObj.caseNumber)){
                lstCsesToBeDel.add(cseobj);
            }
        }
        if(lstCsesToBeDel!=null && lstCsesToBeDel.size()>0)
            delete lstCsesToBeDel;
    }
    global void finish(Database.BatchableContext bc){
    
    }
}