//Generated by wsdl2apex

public class WS_PRM_UIMSCompanyMgmtService_User {
    public class CompanyManager_UIMSV2_ImplPort {
        public String endpoint_x = 'https://ims1w-sqe.btsec.dev.schneider-electric.com/IMS-CompanyManager/UIMSV2/CompanyManagement';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://uimsv2.impl.service.ims.schneider.com/', 'WS_PRM_UIMSCompanyMgmtService_User', 'http://uimsv2.service.ims.schneider.com/', 'WS_PRM_UIMSCompanyMgmtServiceModel_User'};
       /* public Boolean removePrimaryContact(String callerFid,String samlAssertion) {
            WS_PRM_UIMSCompanyMgmtServiceModel_User.removePrimaryContact request_x = new WS_PRM_UIMSCompanyMgmtServiceModel_User.removePrimaryContact();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            WS_PRM_UIMSCompanyMgmtServiceModel_User.removePrimaryContactResponse response_x;
            Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.removePrimaryContactResponse> response_map_x = new Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.removePrimaryContactResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'removePrimaryContact',
              'http://uimsv2.service.ims.schneider.com/',
              'removePrimaryContactResponse',
              'WS_PRM_UIMSCompanyMgmtServiceModel_User.removePrimaryContactResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.success;
        } */
        public WS_PRM_UIMSCompanyMgmtServiceModel_User.companyV3 getCompany(String callerFid,String samlAssertionOrToken,String federatedId) {
            WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompany request_x = new WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompany();
            request_x.callerFid = callerFid;
            request_x.samlAssertionOrToken = samlAssertionOrToken;
            request_x.federatedId = federatedId;
            WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyResponse response_x;
            Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyResponse> response_map_x = new Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'getCompany',
              'http://uimsv2.service.ims.schneider.com/',
              'getCompanyResponse',
              'WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.company;
        }
      /*  public Boolean rejectCompanyMerge(String callerFid,String authentificationToken,String federatedId) {
            WS_PRM_UIMSCompanyMgmtServiceModel_User.rejectCompanyMerge request_x = new WS_PRM_UIMSCompanyMgmtServiceModel_User.rejectCompanyMerge();
            request_x.callerFid = callerFid;
            request_x.authentificationToken = authentificationToken;
            request_x.federatedId = federatedId;
            WS_PRM_UIMSCompanyMgmtServiceModel_User.rejectCompanyMergeResponse response_x;
            Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.rejectCompanyMergeResponse> response_map_x = new Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.rejectCompanyMergeResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'rejectCompanyMerge',
              'http://uimsv2.service.ims.schneider.com/',
              'rejectCompanyMergeResponse',
              'WS_PRM_UIMSCompanyMgmtServiceModel_User.rejectCompanyMergeResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.success;
        }
        public Boolean updateCompany(String callerFid,String samlAssertion,String federatedId,WS_PRM_UIMSCompanyMgmtServiceModel_User.companyV3 company) {
            WS_PRM_UIMSCompanyMgmtServiceModel_User.updateCompany request_x = new WS_PRM_UIMSCompanyMgmtServiceModel_User.updateCompany();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            request_x.federatedId = federatedId;
            request_x.company = company;
            WS_PRM_UIMSCompanyMgmtServiceModel_User.updateCompanyResponse response_x;
            Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.updateCompanyResponse> response_map_x = new Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.updateCompanyResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'updateCompany',
              'http://uimsv2.service.ims.schneider.com/',
              'updateCompanyResponse',
              'WS_PRM_UIMSCompanyMgmtServiceModel_User.updateCompanyResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.success;
        }
        public Boolean acceptCompanyMerge(String callerFid,String authentificationToken,String federatedId) {
            WS_PRM_UIMSCompanyMgmtServiceModel_User.acceptCompanyMerge request_x = new WS_PRM_UIMSCompanyMgmtServiceModel_User.acceptCompanyMerge();
            request_x.callerFid = callerFid;
            request_x.authentificationToken = authentificationToken;
            request_x.federatedId = federatedId;
            WS_PRM_UIMSCompanyMgmtServiceModel_User.acceptCompanyMergeResponse response_x;
            Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.acceptCompanyMergeResponse> response_map_x = new Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.acceptCompanyMergeResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'acceptCompanyMerge',
              'http://uimsv2.service.ims.schneider.com/',
              'acceptCompanyMergeResponse',
              'WS_PRM_UIMSCompanyMgmtServiceModel_User.acceptCompanyMergeResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.success;
        }
        public WS_PRM_UIMSCompanyMgmtServiceModel_User.companyV3[] searchCompany(String callerFid,String samlAssertion,WS_PRM_UIMSCompanyMgmtServiceModel_User.companyV3 template) {
            WS_PRM_UIMSCompanyMgmtServiceModel_User.searchCompany request_x = new WS_PRM_UIMSCompanyMgmtServiceModel_User.searchCompany();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            request_x.template = template;
            WS_PRM_UIMSCompanyMgmtServiceModel_User.searchCompanyResponse response_x;
            Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.searchCompanyResponse> response_map_x = new Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.searchCompanyResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'searchCompany',
              'http://uimsv2.service.ims.schneider.com/',
              'searchCompanyResponse',
              'WS_PRM_UIMSCompanyMgmtServiceModel_User.searchCompanyResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.companies;
        }
        public WS_PRM_UIMSCompanyMgmtServiceModel_User.companyV3 getCompanyByInvitationUid(String callerFid,String samlAssertionOrToken,String invitationUid) {
            WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyByInvitationUid request_x = new WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyByInvitationUid();
            request_x.callerFid = callerFid;
            request_x.samlAssertionOrToken = samlAssertionOrToken;
            request_x.invitationUid = invitationUid;
            WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyByInvitationUidResponse response_x;
            Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyByInvitationUidResponse> response_map_x = new Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyByInvitationUidResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'getCompanyByInvitationUid',
              'http://uimsv2.service.ims.schneider.com/',
              'getCompanyByInvitationUidResponse',
              'WS_PRM_UIMSCompanyMgmtServiceModel_User.getCompanyByInvitationUidResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.company;
        }
        public String createCompany(String callerFid,String samlAssertion,WS_PRM_UIMSCompanyMgmtServiceModel_User.companyV3 company) {
            WS_PRM_UIMSCompanyMgmtServiceModel_User.createCompany request_x = new WS_PRM_UIMSCompanyMgmtServiceModel_User.createCompany();
            request_x.callerFid = callerFid;
            request_x.samlAssertion = samlAssertion;
            request_x.company = company;
            WS_PRM_UIMSCompanyMgmtServiceModel_User.createCompanyResponse response_x;
            Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.createCompanyResponse> response_map_x = new Map<String, WS_PRM_UIMSCompanyMgmtServiceModel_User.createCompanyResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://uimsv2.service.ims.schneider.com/',
              'createCompany',
              'http://uimsv2.service.ims.schneider.com/',
              'createCompanyResponse',
              'WS_PRM_UIMSCompanyMgmtServiceModel_User.createCompanyResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.federatedId;
        }*/
    }
}