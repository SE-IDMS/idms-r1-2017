@isTest

private class FieloPRM_REST_GetProgramLevelsTest{

    
    static testMethod void unitTest(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'challengeTestLast';
            member.FieloEE__FirstName__c = 'challengeTestFirst';
            member.FieloEE__Street__c = 'test';
            insert member;
            
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            con1.FieloEE__Member__c = member.id;
            insert con1;

            /*
            Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
            con1.SEContactID__c = 'test';
            update con1;
            */
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Program Level';
            badgeAux.F_PRM_TypeSelection__c = 'Program Level';
            insert badgeAux;
            
            FieloEE__BadgeMember__c badMemAux = new FieloEE__BadgeMember__c();
            badMemAux.FieloEE__Badge2__c = badgeAux.id;
            badMemAux.FieloEE__Member2__c = member.id ;
            badMemAux.F_PRM_FromDate__c = Date.Today(); 
            badMemAux.F_PRM_ExpirationDate__c = badMemAux.F_PRM_FromDate__c.addDays(1);
            insert badMemAux;
            
            FieloCH__Challenge__c challenge =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
        
            FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission1.id );    
            FieloCH__MissionChallenge__c missionChallenge2  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission2.id );    
            
            FieloCH__ChallengeReward__c challengeReward  = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge );    
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badgeAux.id;
            
            update challengeReward;

            test.startTest();
            
            challenge = FieloPRM_UTILS_MockUpFactory.activateChallenge(challenge);


            
            FieloPRM_REST_GetProgramLevels rest = new FieloPRM_REST_GetProgramLevels();
            FieloPRM_REST_GetProgramLevels.getProgramLevels( );
            test.stopTest();

        }
      
    }  
    
     static testMethod void unitTest2(){
        
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            
            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'challengeTestLast';
            member.FieloEE__FirstName__c = 'challengeTestFirst';
            member.FieloEE__Street__c = 'test';
     
            insert member;
            
            Contact con1 = new Contact();
            con1.FirstName = 'conFirst';
            con1.LastName = 'conLast';
            con1.SEContactID__c = 'test';
            con1.PRMUIMSId__c = 'test';
            con1.FieloEE__Member__c = member.id;
            insert con1;

            /*
            Contact con1 = [SELECT id, SEContactID__c FROM Contact WHERE FieloEE__Member__c =: member.id limit 1];
            con1.SEContactID__c = 'test';
            update con1;
            */
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Program Level';
            badgeAux.F_PRM_TypeSelection__c = 'Program Level';
            insert badgeAux;
            
            FieloEE__BadgeMember__c badMemAux = new FieloEE__BadgeMember__c();
            badMemAux.FieloEE__Badge2__c = badgeAux.id;
            badMemAux.FieloEE__Member2__c = member.id ;
            badMemAux.F_PRM_FromDate__c = Date.Today(); 
            badMemAux.F_PRM_ExpirationDate__c = badMemAux.F_PRM_FromDate__c.addDays(1);
            insert badMemAux;
            
            FieloCH__Challenge__c challenge =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
        
            FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission1.id );    
            FieloCH__MissionChallenge__c missionChallenge2  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission2.id );    
            
            FieloCH__ChallengeReward__c challengeReward  = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge );    
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badgeAux.id;
            
            update challengeReward;
            test.startTest();
            challenge =    FieloPRM_UTILS_MockUpFactory.activateChallenge(challenge);
              
            
            FieloPRM_REST_GetProgramLevels rest = new FieloPRM_REST_GetProgramLevels();
            FieloPRM_REST_GetProgramLevels.getProgramLevels( );
            test.stopTest();
        }
      
    }  
    
}