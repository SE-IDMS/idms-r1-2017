//Description: Test Class for Batch_AccountMerge

@isTest
private class Batch_AccountMerge_TEST {
    static TestMethod void testMergeAccount(){
        //Get Business Account & PRM Account RecordType IDs
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Id partnerAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='PRMAccount' limit 1][0].Id;
        
        //Business Accounts
        Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
        insert c;
        
        Account acc1 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '11',City__c='Bangalore');
        insert acc1;
        Account acc2 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '12',City__c='Bangalore');
        insert acc2;
        Account acc3 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '13',City__c='Bangalore');
        insert acc3;
        
        //Partner Accounts
        Account par1 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = partnerAccRTId, PRMUIMSSEAccountId__c = '11', PRMUIMSID__c = '1100',City__c='Bangalore');
        insert par1;
        Account par2 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = partnerAccRTId, PRMUIMSSEAccountId__c = '12', PRMUIMSID__c = '1111',City__c='Bangalore');
        insert par2;
        
        acc1.isPartner = true;
        update acc1;
        acc2.isPartner = true;
        update acc2;
        acc3.SEAccountId__c = '12';
        acc3.isPartner = true;
        update acc3;
        
        Batch_AccountMerge batchAcc = new Batch_AccountMerge();
        database.executeBatch(batchAcc);
    }
}