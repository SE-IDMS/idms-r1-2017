@isTest
 public class VFC_IdmsMyProfileControllerTest{

public static testMethod void idmsMyProfilePage(){
    IdmsAppMapping__c appMap= new IdmsAppMapping__c();
       appMap.name = 'idms';
       appMap.AppURL__c= 'idms';
       insert appMap;

PageReference mypage= Page.UserProfile;
Test.setCurrentPage(mypage);


User currentUser = [ SELECT id,usertype, email, username, MobilePhone, firstname, lastname, Country__c, IDMS_Email_opt_in__c, IDMS_PreferredLanguage__c, DefaultCurrencyIsoCode, street,
City, PostalCode, IDMS_State__c, IDMS_POBox__c, Phone, localesidkey, extension, fax,LanguageLocaleKey,TimeZoneSidKey,IDMS_County__c 
FROM  User WHERE id =: UserInfo.getUserId()];
VFC_IdmsMyProfileController controller=new VFC_IdmsMyProfileController();
System.assertEquals(currentUser.Id, controller.getUser().Id, 'Did not successfully load the current user');

ApexPages.currentpage().getparameters().put('App' ,'idms');
controller.myemail='abc@accenture.com';
controller.setpwdFlag=true;
controller.pwdresetSuccess=true;
controller.getUser().IDMS_Profile_update_source__c='idms';
controller.getUser();
controller.getUser().Fax = '1234';
controller.save();    
controller.getUser().firstname= '';
controller.save();  
controller.getUser().firstname= 'test';
controller.getUser().email= 'abc';
controller.save();
controller.getUser().email= 'abc.xyz@accenture.com';
controller.preflang='en';
controller.save();
controller.getUser().IDMS_PreferredLanguage__c= null;
controller.save();
controller.getUser().IDMS_PreferredLanguage__c= 'None';
controller.save();
controller.getUser().IDMS_PreferredLanguage__c= 'en';
controller.save();
controller.getUser().Phone= 'abc';
controller.save();
controller.getUser().IDMS_PreferredLanguage__c=currentUser.email;
controller.getUser().Phone= '1234567890';
controller.save();
ApexPages.currentpage().getparameters().put('App' ,'idms');
controller.getUser().MobilePhone= 'abc';
controller.save();
controller.getUser().MobilePhone= '1234567890';
controller.save();

controller.getUser().username='test123test123test123.test123test123test123@accentureing.com';
controller.save();

controller.editEmail();
controller.getUser().email= 'abc.xyz@accenture.com';
controller.saveEmail();
controller.getUser().email= 'abc';
controller.saveEmail();
controller.getUser().email= '';
controller.saveEmail();
controller.getUser().email=currentUser.email;
controller.saveEmail();
controller.oldPassword ='test';
controller.newPassword ='test';
controller.verifyNewPassword='test';
controller.updatePassword();
controller.newPassword ='test123';
controller.verifyNewPassword='test';
controller.updatePassword();
controller.newPassword ='test@123';
controller.verifyNewPassword='test@123';
controller.updatePassword();
controller.oldPassword ='';
controller.updatePassword();
controller.getUser().email= 'abc.xyz@accenture.com';
controller.updateEmail();
controller.getUser().email= 'abc';
controller.updateEmail();
controller.getUser().email=currentUser.email;
controller.updateEmail();
controller.changePassword();
controller.getUser().email= 'abc.xyz@accenture.com';
controller.testClassVar=true;
controller.changePassword();
controller.updateEmail();
controller.updatePassword();
controller.saveEmail();
controller.testStringval='error';
controller.save();
controller.testClassVar=false;
controller.changePassword();
controller.updateEmail();
controller.updatePassword();
controller.saveEmail();
controller.testStringval='Success';
controller.save();
}

}