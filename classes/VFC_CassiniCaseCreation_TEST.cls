@isTest
public class VFC_CassiniCaseCreation_TEST{
     public static testMethod void TestCaseCreationcontroller() { 
        
        Map<String,ID> profiles = new Map<String,ID>();
         List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
         for(Profile p : ps)
          {
             profiles.put(p.name, p.id);
          }
          
         user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null and BypassVR__c= True limit 1];
  
  
        system.runas(admin){
        Id profilesId = System.label.CLMAR13PRM03;
               
        country__c ObjCountry=null;
        List<Country__c> countries =[select id,Name,CountryCode__c from country__c limit 1];
        if(countries.size()==1)
            ObjCountry=countries[0];
        else{               
            ObjCountry=new country__c(Name='India',CountryCode__c='IN');
            insert ObjCountry;    
        }
        
        String countryid = ObjCountry.id;
       
        Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345');
        insert PartnerAcc;
        
        Contact PartnerCon = new Contact(
                FirstName='Test',
                LastName='lastname',
                AccountId=PartnerAcc.Id,
                JobTitle__c='Z3',
                CorrespLang__c='EN',            
                WorkPhone__c='1234567890'
                );
               
                PartnerCon.Country__c = countryid ; 
                
                Insert PartnerCon;
        
       
       
       PermissionSet orfConvPermissionSet1 = [Select ID,UserLicenseId from PermissionSet where Name='PRMRegularORFCommunity' Limit 1];
          
       User   u1= new User(Username = 'testUserOne@schneider-electric.com', LastName = 'User11', alias = 'tuser1',
                        CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                        Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                        LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId, ContactID = PartnerCon.Id, UserPermissionsSFContentUser=true );
       insert u1; 
       
        Contract contract=new Contract();
        contract.StartDate=System.now().Date();
        contract.Points__c=109;
        contract.WebAccess__c='Cassini';
        contract.AccountId=PartnerAcc.Id;
        contract.ContactName__c=PartnerCon.Id;
        insert contract;
        System.assert(contract!=null);
        
        CTR_ValueChainPlayers__c ctrv=new CTR_ValueChainPlayers__c();
        ctrv.Contact__c=PartnerCon.Id;
        ctrv.Contract__c=contract.Id;
        insert ctrv;
        System.assert(ctrv!=null);
        
        Case objCase=new Case(Description='Test Desc12341',CustomerRequest__c='Test CustReq12341',
                              RelatedContract__c=contract.Id,ContactID=PartnerCon.Id);
        insert objCase;
        System.assert(objCase!=null);

        Test.startTest();
    
        PartnerAcc.TECH_AccountOwner__c=u1.Id;
        update PartnerAcc;
      
        system.runas(u1)
        {
            VFC_CassiniCaseCreation.getProducts('other');
            VFC_CassiniCaseCreation.linkedProductToCase('','Hiiiii');
        }
        Test.stopTest();
        }
    }

}