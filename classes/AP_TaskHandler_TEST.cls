@isTest
private class AP_TaskHandler_TEST {
    static testMethod void updateLatestContact_Test() {
    	Country__c objAccountCountry = Utils_TestMethods.createCountry();
        objAccountCountry.Name='AccCountryName'; 
        objAccountCountry.CountryCode__c='IN';
        Database.insert(objAccountCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objAccountCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
    	ApexPages.StandardController ContactStandardController = new ApexPages.StandardController(objContact);
        ContactStandardController.view();

        Task objTask = Utils_TestMethods.createTask(null,null,'Completed');
        objTask.CallType ='Inbound';

        insert objTask;
    }

    static testMethod void calculateFirstResponseOnOutboundCall_Test() {

    	Country__c objAccountCountry = Utils_TestMethods.createCountry();
        objAccountCountry.Name='AccCountryName'; 
        objAccountCountry.CountryCode__c='IN';
        Database.insert(objAccountCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objAccountCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);

        Case objCase = Utils_TestMethods.createCase(objAccount.Id,objContact.Id,'In Progress');
        Database.insert(objCase);

      	Task objTask = Utils_TestMethods.createTask(null,null,'Completed');
        objTask.WhatId = objCase.Id;
        objTask.CallType ='Outbound';
        objTask.CTICallType__c ='Outbound';
        
        insert objTask;
    }
}