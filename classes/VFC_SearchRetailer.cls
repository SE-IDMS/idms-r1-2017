global without sharing class VFC_SearchRetailer  {
    
    public transient static Boolean isOrCondition = false;
    public Boolean isMobile {get;set;}
    public Boolean isTablet {get;set;}
    
    global VFC_SearchRetailer(){
        
        //Setting "isMobile" & "isTablet"
        String userAgent = System.currentPageReference().getHeaders().get('User-Agent');
        String device = System.currentPageReference().getParameters().get('device');
        system.debug('userAgent: ' +userAgent );
        system.debug('device: ' +device );
        if (!Test.isRunningTest()) {
            isMobile = (userAgent.contains('iPhone') || device == 'mobile');
            isTablet = (userAgent.contains('iPad') || (userAgent.contains('AppleWebKit') && device != null && device != 'mobile'));
        }
     
    } 
    
    global class SearchAccountModel{
        global String compName;
        global String Condition1;
        
        global String compHeadQuarters; 
        global String Condition2;
        
        global String compAddress;
        global String Condition3;
        
        global String City;
        global String Condition4;
    
        global String zipPostalCode;
        global String Condition5;
        
        public String businessType;
        public String Condition6;
    
        global String Country;

        global String stateProvince;
    
    }
    
  @RemoteAction
    global static String SearchRetailer(SearchAccountModel accountSearch){
    system.debug('****'+accountSearch);
        
        
        User contUser  = [SELECT Id, Contact.PRMCountry__c FROM User WHERE id = :UserInfo.getUserId() Limit 1];
        String userCountry = contUser.Contact.PRMCountry__c;
        System.debug('*** User Country ->'+userCountry);
        
        List<Account> searchResults = new List<Account>();
        String query = Null;
        try {
            List<Account> lAccounts;
            if(accountSearch != Null){
                query = 'SELECT Id,Name,AccountLocalName__c,Street__c,City__c,LocalCity__c,ZipCode__c,RecordTypeId,'+ 
                        'StreetLocalLang__c FROM Account WHERE RecordTypeId = \''+System.Label.CLAPR15PRM274+'\' AND Country__c = \'' + userCountry + '\'  AND ';
                         
                System.debug('*** Query ->'+query);
                
                List<String> lstSearch = new List<String>();
                lstSearch.add('PRMParticipateinRewardsProgram__c = true');
                                                               
                if(String.isNotBlank(accountSearch.compName)){
                    accountSearch.compName = accountSearch.compName.replace('\'','\\\'');
                    accountSearch.compName = accountSearch.compName.replace('\"','\\\"');
                    lstSearch.add('((AccountLocalName__c LIKE \'%'+accountSearch.compName+'%\') OR (Name LIKE \'%'+accountSearch.compName+'%\'))');          
                }   
                
                if(String.isNotBlank(accountSearch.City)){
                    accountSearch.City = accountSearch.City.replace('\'','\\\'');
                    accountSearch.City = accountSearch.City.replace('\"','\\\"');   
                    lstSearch.add( '((LocalCity__c LIKE \'%'+accountSearch.City+'%\') OR (City__c LIKE \'%'+accountSearch.City+'%\'))');
                    
                }   
                   
                if(String.isNotBlank(accountSearch.zipPostalCode)) 
                    lstSearch.add('ZipCode__c = \''+accountSearch.zipPostalCode+'\'');
               
                query = query+ String.join(lstSearch, ' AND ');            
                query = query+' ORDER BY Name ASC Limit 100';
                System.debug('*** The Query ***'+query);
                lAccounts = Database.Query(query);
                searchResults.addAll(lAccounts);
            }   
            
        }
        catch(Exception e){
            System.debug('*** Error Searching Accounts with the given account name *** '+e.getMessage());
        }
        
        if (searchResults.size() > 0)
             return JSON.serialize(searchResults);
        else
            return null;
    
    } 

}