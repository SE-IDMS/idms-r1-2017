/*
    Author          : Bhuvana Subramaniyan (ACN) 
    Date Created    : 14/01/2011 
    Description     : Test Class for VFC15_BudgetView.
*/
@isTest
private class VFC15_BudgetView_TEST
{
    //Test method
    static testMethod void testBudgetView() 
    {
        //inserts Account
        Account acc = RIT_Utils_TestMethods.createAccount();
        insert acc;
        acc = [select id from Account where Id=:acc.Id];
        
        //inserts Opportunity
        Opportunity opp = RIT_Utils_TestMethods.createOpportunity(acc.Id);
        insert opp;
        opp = [select id from Opportunity where Id =: opp.Id];
        
        //inserts QuoteLink
        OPP_QuoteLink__c quoteLink = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
        insert quoteLink;        
        quoteLink = [select id from OPP_QuoteLink__c where Id=:quoteLink.Id];
        
        //inserts Budget
        RIT_Budget__c budget= RIT_Utils_TestMethods.createbudget(quoteLink.Id);
        insert budget;        
        budget= [select id,QuoteLink__c from RIT_Budget__c where Id=:budget.Id];
        
        //inserts Budget History
        RIT_Budget__History budHist = RIT_Utils_TestMethods.createBudgetHistory(budget.Id,'Sales__c');
        insert budHist;        
        budHist = [select ParentId,Field,CreatedDate from RIT_Budget__History where parentId=:budget.Id];
        
        ApexPages.StandardController con = new ApexPages.StandardController(budget);
        ApexPages.StandardController con1 = new ApexPages.StandardController(new RIT_Budget__c());
        //Test method Execution Begins
        Test.StartTest();
        
        VFC15_BudgetView budgetView= new VFC15_BudgetView(con);
        PageReference pageRef = Page.VFP15_BudgetView;
        budgetView.delButton();
        budgetView.backToRiteForm();
        
        budgetView= new VFC15_BudgetView(con1);
        budgetView.delButton();
        budgetView.backToRiteForm();
        VFC15_BudgetView.WrapperClass objWrap = new VFC15_BudgetView.WrapperClass(budHist,'clear');
        objWrap = new VFC15_BudgetView.WrapperClass(budHist,'no clear');
        List<VFC15_BudgetView.WrapperClass> wrapperclass= budgetView.getHistory();

        Test.StopTest();
        //Test method Execution Ends
    }
}