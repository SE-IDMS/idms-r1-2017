public class VFC_ReactivatePRMUsers{

       
    public Transient Contact con{get; set;} 
    
    public VFC_ReactivatePRMUsers(){
        System.Debug('VFC_ReactivatePRMUsers');
        con = new Contact();
    }    
    
    
    @RemoteAction
    public static List<Contact> DisplayPRMContacts(String prmUIMSId, String eml, String fromDate, String toDate ) {
        
        String queryStr = ' SELECT PRMFirstName__c, PRMLastName__c, PRMEmail__c, Account.Name, PRMUIMSID__c, PRMPrimaryContact__c, ' +
                            ' PRMRegistrationSubmissionDate__c, toLabel(PRMContactRegistrationStatus__c), PRMOrigin__c FROM Contact ' +
                            ' WHERE PRMContactRegistrationStatus__c =\'Registering\' and PRMUIMSID__c != null ';
                            
        String prmUIMSIdEsc = String.escapeSingleQuotes(prmUIMSId); 
        String emlEsc = String.escapeSingleQuotes(eml); 
        
        if(String.isNotBlank(prmUIMSIdEsc))
            queryStr += ' and PRMUIMSID__c = :prmUIMSIdEsc';
        
        if(String.isNotBlank(emlEsc))
            queryStr += ' and PRMEmail__c = :emlEsc ';
            
        System.Debug('DisplayPRMContacts-> PRM UIMS ID : ' + prmUIMSIdEsc + ' PRM Email: ' + emlEsc + ' From Date : ' + fromDate +' To Date : ' + toDate );
           
        if(String.isNotBlank(fromDate)) {
            Date fDate = Date.valueOf(fromDate); 
            queryStr += ' and PRMRegistrationSubmissionDate__c >= :fDate ';
            System.Debug('After converting From Date : ' + fDate ); 
        }
            
        if(String.isNotBlank(toDate )) {
            Date tDate = Date.valueOf(toDate).addDays(1);            
            queryStr += ' and PRMRegistrationSubmissionDate__c <= :tDate ';
            System.Debug(' After converting To Date : ' + tDate ); 
        }        
           
        System.Debug('Query : ' + queryStr );
                             
        List<Contact> lContact = Database.query(queryStr + ' LIMIT 200 ');
        return lContact;
        
    }
    
    @RemoteAction
    public static Map<String,String> ReactivateContacts(List<Contact> Cnts) {
    
        Map<String, String> regLinkMap = new Map<String, String>();
        
        Map<Id, Contact> mCnts = new Map<Id, Contact>(Cnts);
        
        List<PRMReactivationEmailHistory__c> lREH = new List<PRMReactivationEmailHistory__c>();
        
        Map<String, Id> mapStrId = new Map<String, Id>();
        
        List<UIMSCompany__c> lUimsComp = [select Id, UserFederatedId__c from UIMSCompany__c where bFOContact__c IN :mCnts.keySet()];
        for(UIMSCompany__c uimsComp : lUimsComp) {
            mapStrId.put(uimsComp.UserFederatedId__c , uimsComp.Id);        
        }
        
        for(Contact cnt : Cnts) {
             String result = '';
             String resp = '';
             System.Debug('ReactivateContacts-> PRM Email : ' + cnt.PRMEmail__c + ' PRM UIMS ID : ' + cnt.PRMUIMSID__c );
             System.Debug('ReactivateContacts-> REH.UIMS_Company_Id__c : ' + mapStrId.get(cnt.PRMUIMSID__c));
             
             PRMReactivationEmailHistory__c REH = new PRMReactivationEmailHistory__c();
             REH.UIMSCompanyId__c = mapStrId.get(cnt.PRMUIMSID__c);
             REH.RequestedTime__c = datetime.now();
             
             try {
                resp = AP_PRMIMSBOService.UIMSReactivateUser(cnt.PRMUIMSID__c);
                System.Debug('UIMSReactivateUser Result : ' + resp );   
                if((cnt.PRMEmail__c).equalsIgnoreCase(resp)) {
                    result = 'Success';   
                      
                } else {
                    result = 'Failed'; 
                    REH.FailureReason__c = resp;                    
                }                                
                
             } catch (System.CalloutException e) {
                System.Debug('UIMSReactivateUser CalloutException : ' + e.getMessage());
                result = 'Failed';                 
                REH.FailureReason__c = e.getMessage();
                               
             }  
             REH.Status__c = result;           
             lREH.add(REH);            
             regLinkMap.put(cnt.PRMEmail__c, result);
        }   
        try{
            insert lREH;
            System.Debug('PRMReactivationEmailHistory__c Insert Successful. ');
        } catch (System.DmlException e) {
            System.Debug('PRMReactivationEmailHistory__c Insert DmlException : ' + e.getMessage() );
        }     
        return regLinkMap;    
    }
    
}