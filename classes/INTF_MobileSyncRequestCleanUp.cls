global with sharing class INTF_MobileSyncRequestCleanUp implements Database.Batchable<sObject>,schedulable
{
    public static boolean isTestExecution = false;
    public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};//added by suhail for q3 2016
     public Integer rescheduleInterval = Integer.ValueOf(Label.CLQ316SRV01);
    String strJobName = 'ServiceMax Clean up Sync data'; //added by suhail 
    //Query Sync Request Records
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        DateTime dtToDelete;
        if(INTF_MobileSyncRequestCleanUp.isTestExecution != null && INTF_MobileSyncRequestCleanUp.isTestExecution)
            dtToDelete = Datetime.now();
        else        
            dtToDelete = Datetime.now().addHours(-2);
            
        system.debug(LoggingLevel.Debug, 'In Start');
        string strSyncSOQL = 'Select Id from SVMXC__Sync_Request__c where IsDeleted=false AND LastModifiedDate <=: dtToDelete';
        system.debug(LoggingLevel.Debug, 'Query prepared to delete Sync Request:- ' + strSyncSOQL);
        return Database.getQueryLocator(strSyncSOQL);
    }

    //Execute Delete
    global void execute(Database.BatchableContext BC, List<SVMXC__Sync_Request__c> lstSyncRequestToDelete)
    {
        system.debug(LoggingLevel.Debug, 'In execute');
        system.debug(LoggingLevel.Debug, 'No of records of SVMXC__Sync_Request__c object in current batch  = ' + lstSyncRequestToDelete.size());
        if(lstSyncRequestToDelete != null && lstSyncRequestToDelete.size() > 0){
            Database.DeleteResult[] delResult = Database.delete(lstSyncRequestToDelete, false);
            for(Database.DeleteResult eachDelRecord : delResult){
                if(!eachDelRecord.isSuccess()){
                    system.debug(LoggingLevel.Debug, 'Delete Failed');
                    for(Database.Error err : eachDelRecord.getErrors())
                    system.debug(LoggingLevel.Debug, 'Delete of SVMXC__Sync_Request__c Failed:' + err);
                }
            }
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        //added by suhail for q3 2016 start
        //checking SAYStart
        Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :strJobName limit 100];
        System.debug('### scheduledJobDetailList: '+scheduledJobDetailList);

        if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) {
            
            List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
            System.debug('### scheduledJobList: '+scheduledJobList);
            if (scheduledJobList != null && scheduledJobList.size()>0) {
                canReschedule = False;
            }
        }
        //checking SAYend
         if (canReschedule && !Test.isRunningTest() ) {
        System.scheduleBatch(new INTF_MobileSyncRequestCleanUp(),strJobName,rescheduleInterval);
         }
        
        system.debug(LoggingLevel.Debug, 'In finish');
    }
    global void execute(SchedulableContext sc) {
        INTF_MobileSyncRequestCleanUp cleanup = new INTF_MobileSyncRequestCleanUp();
        Database.executeBatch(cleanup);
    }  
//added by suhail for q3 2016 end   
}