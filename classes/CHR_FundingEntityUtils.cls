/*
            Author          : Priyanka Shetty (Schneider Electric)
            Date Created    : 13/04/2013
            Description     : Utils class for the class CHR_FundingEntityTriggers.                      
                              Deactivates the current active forecast funding entity record.
                              Checks if a funding entity record with the same combination of PF-BO-SEL1-SEL2-BY-FD already exists for a change request.
                              
        */
        public with sharing class CHR_FundingEntityUtils
        {
            public static void deactivateActiveFundingEntity(List<ChangeRequestFundingEntity__c> fundingentityListToProcessCONDITION1,boolean isUpdate)
            {
                Map<string,ChangeRequestFundingEntity__c> projectNewFundingEntityMap = new Map<string,ChangeRequestFundingEntity__c>();
                Map<string,ChangeRequestFundingEntity__c> projectCurrFundingEntityMap = new Map<string,ChangeRequestFundingEntity__c>();
                List<ChangeRequestFundingEntity__c> updateFundingEntityList = new List<ChangeRequestFundingEntity__c>();
                Map<Id,string> projectRequestMap = new Map<Id,string>();
                
                /* Get the list (lstA) of current active forecast funding entity records for the list of project req IDs (from the fundingentityListToProcessCONDITION1 list) */
                for(ChangeRequestFundingEntity__c oFE:fundingentityListToProcessCONDITION1)   
                {
                    projectNewFundingEntityMap.put(oFE.ChangeRequest__c + '-' + oFE.ParentFamily__c + '-' + oFE.Parent__c + '-' + oFE.GeographicZoneSE__c + '-' + oFE.GeographicalSEOrganization__c + '-' + oFE.BudgetYear__c,oFE);
                    if(!projectRequestMap.containsKey(oFE.ChangeRequest__c))
                    {
                        projectRequestMap.put(oFE.ChangeRequest__c,null);
                        System.Debug('projectRequestMap ID: ' + oFE.ChangeRequest__c);                    
                    }
                    

                }         
              
                /* Loop thru the list and Make a map of all the Change Request IDs (as key) and current active forecast funding entity record */
                for (ChangeRequestFundingEntity__c oFE:[select Id,ActiveForecast__c,ChangeRequest__c,BudgetYear__c,ParentFamily__c,Parent__c,GeographicZoneSE__c,GeographicalSEOrganization__c,ForecastDate__c from ChangeRequestFundingEntity__c where ChangeRequest__c in :projectRequestMap.keyset() and ActiveForecast__c = true])
                {
                    if(projectNewFundingEntityMap.containsKey(oFE.ChangeRequest__c + '-' + oFE.ParentFamily__c + '-' + oFE.Parent__c + '-' + oFE.GeographicZoneSE__c + '-' + oFE.GeographicalSEOrganization__c + '-' + oFE.BudgetYear__c))
                    {
                        projectCurrFundingEntityMap.put(oFE.ChangeRequest__c + '-' + oFE.ParentFamily__c + '-' + oFE.Parent__c + '-' + oFE.GeographicZoneSE__c + '-' + oFE.GeographicalSEOrganization__c + '-' + oFE.BudgetYear__c,oFE);
                        
                    }
                }

                /*
                // Loop thru the map
                //     for each current active forecast funding entity record (CAFE), set the active forecast flag = false
                //     get the new funding entity record (NAFE) corresponding to the ProjectID from the newMap
                // update NAFE list and CAFE list
                */
                for (string projIDfe:projectCurrFundingEntityMap.keyset())
                {
                    ChangeRequestFundingEntity__c currFE = projectCurrFundingEntityMap.get(projIDfe);
                    ChangeRequestFundingEntity__c newFE = projectNewFundingEntityMap.get(projIDfe);

                    currFE.ActiveForecast__c = false;

                    updateFundingEntityList.add(currFE);
                }
                
                try
                {        
                    if(updateFundingEntityList.size() > 0)
                        update(updateFundingEntityList);
                        
                    if(Test.isRunningTest())
                        throw new TestException('Test Exception');
                }
                catch(Exception exp)
                {
                    System.Debug('Update Failed');
                }
                                
                      
                
            }
            
            public static void checkFundingEntityExists(List<ChangeRequestFundingEntity__c> feList)
            {
                //check if a funding entity record with the same combinaton of PF-BO-SEL1-SEL2-BY-FD already exists
                //map of project request record ids against funding entity records
                Map<Id,ChangeRequestFundingEntity__c> feprjMap = new Map<Id,ChangeRequestFundingEntity__c>();
                //Map of project ids against list of funding entity records
                Map<Id,List<ChangeRequestFundingEntity__c>> prjfeMap = new Map<Id,List<ChangeRequestFundingEntity__c>>();
                
                for(ChangeRequestFundingEntity__c fe:feList)
                {
                    if(!feprjMap.containsKey(fe.ChangeRequest__c))
                        feprjMap.put(fe.ChangeRequest__c,fe);
                }
                
                
                for(ChangeRequestFundingEntity__c oFE:[select Id,ChangeRequest__c,BudgetYear__c,ParentFamily__c,Parent__c,GeographicZoneSE__c,GeographicalSEOrganization__c,ForecastQuarter__c,ForecastYear__c from ChangeRequestFundingEntity__c where ChangeRequest__c in :feprjMap.keySet()])        
                {
                    List<ChangeRequestFundingEntity__c> lstFe = new List<ChangeRequestFundingEntity__c>();
                    if(prjfeMap.containsKey(oFE.ChangeRequest__c))
                    {
                        lstFe = prjfeMap.get(oFE.ChangeRequest__c);
                    }
                    lstFe.add(oFE);
                    prjfeMap.put(oFE.ChangeRequest__c,lstFe);

                }
                
                
                for(Id idPrj:feprjMap.keySet())
                {
                    ChangeRequestFundingEntity__c fe = feprjMap.get(idPrj);
                    boolean isExist = false;
                    if(prjfeMap.containsKey(idPrj))
                    {
                        List<ChangeRequestFundingEntity__c> lstFeExisting = prjfeMap.get(idPrj);
                        string strLogicalKey1 = fe.ParentFamily__c + fe.Parent__c + fe.GeographicZoneSE__c + fe.GeographicalSEOrganization__c + fe.BudgetYear__c + fe.ForecastQuarter__c + fe.ForecastYear__c;
                        for(ChangeRequestFundingEntity__c feExisting:lstFeExisting)
                        {
                            string strLogicalKey2 = feExisting.ParentFamily__c + feExisting.Parent__c + feExisting.GeographicZoneSE__c + feExisting.GeographicalSEOrganization__c + feExisting.BudgetYear__c + feExisting.ForecastQuarter__c + feExisting.ForecastYear__c;
                            if(strLogicalKey2 <> null && strLogicalKey1 <> null)
                            {
                                if(strLogicalKey1.equals(strLogicalKey2))
                                {
                                    isExist = true;
                                    break;
                                }
                            }                    
                        }                
                    }
                    if(isExist)
                    {
                        fe.addError(System.Label.DMT_FundingEntityRecordExists);
                    }
                    
                }
                
                
            }   
            
            
            public class TestException extends Exception{}
            

            
            public static void fundentityBeforeInsert(List<ChangeRequestFundingEntity__c> newFundingEntities)
            {
                System.Debug('****** fund entity Before Insert Started ****');
               
                set<Id> prIdSet = new set<Id>();
                list<ChangeRequestFundingEntity__c> feActualList = new list<ChangeRequestFundingEntity__c>();
                Map<id, Change_Request__c> prIdMap = new Map<id, Change_Request__c>();
                Set<id> existFeSet=new Set<id>();
                list<Change_Request__c> ProjReqList=new list<Change_Request__c>();
                
                
                for(ChangeRequestFundingEntity__c fund:newFundingEntities){
                    prIdSet.add(fund.ChangeRequest__c);
                }
                if(prIdSet != null && prIdSet.size()>0){
                    ProjReqList =[select id, ParentFamily__c, Parent__c, GeographicZoneSE__c, GeographicalSEOrganization__c from Change_Request__c where id in: prIdSet];
                    for(ChangeRequestFundingEntity__c eFE: [select id,ChangeRequest__c from ChangeRequestFundingEntity__c where ChangeRequest__c in :prIdSet]){
                        existFeSet.add(eFE.ChangeRequest__c);
                    }
                }
                if(ProjReqList != null)
                {
                    prIdMap.putAll(ProjReqList);
                    
                    for(ChangeRequestFundingEntity__c fund:newFundingEntities){
                    
                        if(!existFeSet.contains(fund.ChangeRequest__c)){
                            if(prIdMap.containsKey(fund.ChangeRequest__c)){
                                Change_Request__c projReq = prIdMap.get(fund.ChangeRequest__c); 
                                   if(fund.ActiveForecast__c ==true){                           
                                    feActualList.add(fund);

                                }
                            }
                        }
                        
                    }
                }
                if(feActualList != null && feActualList.size()>0){
                    
                    System.Debug('****** Funding Entity TotalCost Capex Started ****'); 
                    UpdateTotalCostOnFundingEntity(feActualList);
                    
                }
                System.Debug('****** fund entity Before Insert End ****');
            }
    
           public static void UpdateTotalCostOnFundingEntity(List<ChangeRequestFundingEntity__c> newFundingEntities)
            {
            
                Map<Id,ChangeRequestCostsForecast__c> mapTypeToSobject = new Map<Id,ChangeRequestCostsForecast__c>();
                List<DMTCRCFTOCRFE__c> listLabels = DMTCRCFTOCRFE__c.getAll().values();                               
                List<Id> listProjReqs = new List<Id>();
                Map<String, Decimal> isoCodeConvMap = new Map<String, Decimal>();
                for(ChangeRequestFundingEntity__c fe : newFundingEntities){
                    listProjReqs.add(fe.ChangeRequest__c);
                }

                for(ChangeRequestCostsForecast__c budgetLine : [select id,Active__c,ChangeRequest__c,CurrencyIsoCode,NewFY0CashoutCAPEX__c,NewFY1CashoutCAPEX__c,NewFY2CashoutCAPEX__c,NewFY3CashoutCAPEX__c,NewFY0CashoutOPEX__c,NewFY1CashoutOPEX__c,NewFY2CashoutOPEX__c,NewFY3CashoutOPEX__c,FY0SmallEnhancements__c,FY1SmallEnhancements__c,FY2SmallEnhancements__c,FY3SmallEnhancements__c,FY0RunningITCostsImpact__c,FY1RunningITCostsImpact__c,FY2RunningITCostsImpact__c,FY3RunningITCostsImpact__c from ChangeRequestCostsForecast__c where ChangeRequest__c in :listProjReqs ]){
                    if(budgetLine.Active__c){ // Only if the Costs Forecasts record is active.
                    mapTypeToSobject.put(budgetLine.ChangeRequest__c,budgetLine);
                    }
                }
                
                for(DatedConversionRate dtCRList : [SELECT ConversionRate,IsoCode FROM DatedConversionRate where startDate<:System.Today().toStartOfMonth() and NextStartDate>=:System.Today()]){
                    isoCodeConvMap.put(dtCRList.IsoCode,dtCRList.ConversionRate);
                }
                for(ChangeRequestFundingEntity__c fe : newFundingEntities){

                    if(mapTypeToSobject.get(fe.ChangeRequest__c)!=null){
                        Decimal conversionFundEntity = isoCodeConvMap.get(fe.CurrencyIsoCode);
                        for(DMTCRCFTOCRFE__c DMTlabel : listLabels){
                        Decimal decamount;
                        
                        if((mapTypeToSobject.get(fe.ChangeRequest__c).get(DMTlabel.CostForecastField__c))!=null){
                                       // fe.put(DMTlabel.FundingEntityField__c,mapTypeToSobject.get(fe.ChangeRequest__c).get(DMTlabel.CostForecastField__c));
                                    if(decamount==null){                     
                                         decamount = ((decimal)(mapTypeToSobject.get(fe.ChangeRequest__c).get(DMTlabel.CostForecastField__c)))*(conversionFundEntity/isoCodeConvMap.get(mapTypeToSobject.get(fe.ChangeRequest__c).CurrencyIsoCode));
                                    }
                                    else{

                                        decamount = decamount+(decimal)(mapTypeToSobject.get(fe.ChangeRequest__c).get(DMTlabel.CostForecastField__c))*(conversionFundEntity/isoCodeConvMap.get(mapTypeToSobject.get(fe.ChangeRequest__c).CurrencyIsoCode));
                                        }
                            }
                         if(decamount !=null)
                                fe.put(DMTlabel.FundingEntityField__c,decamount); 
                    }
                }
            }
        }




            public static void updateProjectRequest(List<ChangeRequestFundingEntity__c> fundingentityList){
            
                List<Id> listProjRequestId = new List<Id>{};
                
                List<ChangeRequestFundingEntity__c> ListFeWithActiveSelected = new List<ChangeRequestFundingEntity__c>{};
                
                List<Change_Request__c> listProjReq  = new List<Change_Request__c>{};

                Map<Id,List<ChangeRequestFundingEntity__c>> mapProjReqToFeWithActiveSelected = new Map<Id,List<ChangeRequestFundingEntity__c>>(); 
                
                Map<Id,Change_Request__c> mapIdTOProjReq = new Map<Id,Change_Request__c>();
                
                Map<Id,Id> maplistProjReq = new Map<Id,Id>(); 
                
                for(ChangeRequestFundingEntity__c fe : fundingentityList){
                    listProjRequestId.add(fe.ChangeRequest__c);
                }
                
                for(Change_Request__c ProReq : [select id,CurrencyIsoCode,(select id,CurrencyIsoCode,ChangeRequest__c,TotalFY0__c,TotalFY1__c,TotalFY2__c,TotalFY3__c,BudgetYear__c from ChangeRequestFundingEntities__r Where ChangeRequest__c in : listProjRequestId and ActiveForecast__c = True and SelectedinEnvelop__c = True and id not in :fundingentityList)from Change_Request__c ]){
                    mapProjReqToFeWithActiveSelected.put(ProReq.Id,ProReq.ChangeRequestFundingEntities__r); 
                    mapIdTOProjReq.put(ProReq.Id,ProReq);
                }
                
                List<DatedConversionRate> dtCRList= [SELECT ConversionRate,IsoCode FROM DatedConversionRate where startDate<=:System.Today().toStartOfMonth() and NextStartDate>=:System.Today()];
 
                
                for(ChangeRequestFundingEntity__c fentity : fundingentityList){
                    
                    List<ChangeRequestFundingEntity__c> listFentityWithActiveSelected  = new List<ChangeRequestFundingEntity__c>{};
                    
                    Change_Request__c projReq =mapIdTOProjReq.get(fentity.ChangeRequest__c);
                    listFentityWithActiveSelected =  mapProjReqToFeWithActiveSelected.get(fentity.ChangeRequest__c);
                    if(fentity.ActiveForecast__c == True && fentity.SelectedinEnvelop__c == True)
                    listFentityWithActiveSelected.add(fentity);
                    projReq.TotalCostsFunded2012__c = 0.0;
                    projReq.TotalCostsFunded2013__c = 0.0;
                    projReq.TotalCostsFunded2014__c = 0.0;
                    projReq.TotalCostsFunded2015__c = 0.0;
                    projReq.TotalCostsFunded2016__c = 0.0;
                    projReq.TotalCostsFunded2017__c = 0.0;
                    Map<String, Decimal> isoCodeConvMap = new Map<String, Decimal>();
                    if(dtCRList!= null && dtCRList.size()>0){
                       for(DatedConversionRate dcr:dtCRList){
                        if(!isoCodeConvMap.containsKey(dcr.IsoCode))
                          {
                            if(dcr.ConversionRate !=null)
                            isoCodeConvMap.put(dcr.IsoCode,dcr.ConversionRate);
                          }
                       }
}
                    if(ListFentityWithActiveSelected != null){
                        for(ChangeRequestFundingEntity__c fe:ListFentityWithActiveSelected){
                            String feIso=FE.CurrencyIsoCode;
                            String PrIso=projReq.CurrencyIsoCode;
                            Decimal decCurIso;
                            if(isoCodeConvMap.get(PRIso)!=null && isoCodeConvMap.get(FeIso)!=null ){
                                decCurIso=((isoCodeConvMap.get(PRIso))/isoCodeConvMap.get(FeIso));
                            }else{
                                decCurIso=1;
                            }
                            //System.debug('--------------Curr------'+feIso+PrIso+(isoCodeConvMap.get(feIso))+(isoCodeConvMap.get(PrIso))+'----'+decCurIso);
                            if(fe.BudgetYear__c =='2011' ){
                                
                                //projReq.TotalCostsFunded2011__c = projReq.TotalCostsFunded2011__c+(fe.TotalFY0__c)*decCurIso;
                                projReq.TotalCostsFunded2012__c = projReq.TotalCostsFunded2012__c+(fe.TotalFY1__c)*decCurIso;
                                projReq.TotalCostsFunded2013__c = projReq.TotalCostsFunded2013__c+(fe.TotalFY2__c)*decCurIso;
                                projReq.TotalCostsFunded2014__c = projReq.TotalCostsFunded2013__c+(fe.TotalFY3__c)*decCurIso;
                            }
                            if(fe.BudgetYear__c =='2012' ){
                                
                                projReq.TotalCostsFunded2012__c = projReq.TotalCostsFunded2012__c+(fe.TotalFY0__c)*decCurIso;
                                projReq.TotalCostsFunded2013__c = projReq.TotalCostsFunded2013__c+(fe.TotalFY1__c)*decCurIso;
                                projReq.TotalCostsFunded2014__c = projReq.TotalCostsFunded2014__c+(fe.TotalFY2__c)*decCurIso;
                                projReq.TotalCostsFunded2015__c = projReq.TotalCostsFunded2015__c+(fe.TotalFY3__c)*decCurIso;
                                //System.debug('---------*****'+projReq.TotalCostsFunded2012__c+projReq.TotalCostsFunded2013__c);
                            }
                            if(fe.BudgetYear__c =='2013' ){
                                system.debug('-------Hello'+projReq.TotalCostsFunded2012__c+'----'+fe.TotalFY1__c+projReq.TotalCostsFunded2014__c+'-----'+fe.Id);
                                projReq.TotalCostsFunded2013__c = projReq.TotalCostsFunded2013__c+(fe.TotalFY0__c)*decCurIso;
                                projReq.TotalCostsFunded2014__c = projReq.TotalCostsFunded2014__c+(fe.TotalFY1__c)*decCurIso;
                                //system.debug('-------Hello'+projReq.TotalCostsFunded2014__c+'----'+(fe.TotalFY1__c);
                                projReq.TotalCostsFunded2015__c = projReq.TotalCostsFunded2015__c+(fe.TotalFY2__c)*decCurIso;
                                projReq.TotalCostsFunded2016__c = projReq.TotalCostsFunded2016__c+(fe.TotalFY3__c)*decCurIso;
                            }
                            
                            if(fe.BudgetYear__c =='2014' ){
                                system.debug('-------Hello'+projReq.TotalCostsFunded2012__c+'----'+projReq.TotalCostsFunded2014__c+'-----'+fe.Id);
                                projReq.TotalCostsFunded2014__c = projReq.TotalCostsFunded2014__c+(fe.TotalFY0__c)*decCurIso;
                                system.debug('-------Hello'+projReq.TotalCostsFunded2014__c+'----'+fe.TotalFY1__c);
                                projReq.TotalCostsFunded2015__c = projReq.TotalCostsFunded2015__c+(fe.TotalFY1__c)*decCurIso;
                                projReq.TotalCostsFunded2016__c = projReq.TotalCostsFunded2016__c+(fe.TotalFY2__c)*decCurIso;
                                projReq.TotalCostsFunded2017__c = projReq.TotalCostsFunded2017__c+(fe.TotalFY3__c)*decCurIso;
                            }
                        } 
                    }
                    if(!maplistProjReq.containsKey(projReq.Id)){
                        listProjReq.add(projReq);
                        maplistProjReq.put(projReq.Id,projReq.Id);
                    }
                }
                try {
                        update listProjReq;
                } 
                catch (DmlException e){
                }                   

        }

        //Calculate P&L if the Value is not changed and NOT null OR NOT 0 OR Calculating fields are changed.
        public static void updatePLFields(Map<Id,ChangeRequestFundingEntity__c> newMap,Map<Id,ChangeRequestFundingEntity__c> oldMap){
        
            Try{        
                for(ChangeRequestFundingEntity__c fe : newMap.Values()){
                    for(DMT_FundingEntiity_BudgetLines__c csInstance : DMT_FundingEntiity_BudgetLines__c.getAll().values() ){
                        if(fe.get(csInstance.name) == null)
                            fe.put(csInstance.name,0);
                    }
                    if((newMap.get(fe.id).PLFundEstimatedY0__c==oldMap.get(fe.id).PLFundEstimatedY0__c && 
                       (newMap.get(fe.id).OPEXOTCY0__c!=oldMap.get(fe.id).OPEXOTCY0__c ||
                        newMap.get(fe.id).RunningCostsImpactY0__c!=oldMap.get(fe.id).RunningCostsImpactY0__c ||
                        newMap.get(fe.id).SmallEnhancementsY0__c!=oldMap.get(fe.id).SmallEnhancementsY0__c  )) || fe.PLFundEstimatedY0__c==null)
                            fe.PLFundEstimatedY0__c = fe.OPEXOTCY0__c + fe.RunningCostsImpactY0__c + fe.SmallEnhancementsY0__c;// Autopopulate if the Value is not changed and NOT null OR Calculating fields are changed.
                    if((newMap.get(fe.id).PLFundEstimatedY1__c==oldMap.get(fe.id).PLFundEstimatedY1__c &&
                        (newMap.get(fe.id).CAPEXY0__c!=oldMap.get(fe.id).CAPEXY0__c ||
                        newMap.get(fe.id).OPEXOTCY1__c!=oldMap.get(fe.id).OPEXOTCY1__c ||
                        newMap.get(fe.id).SmallEnhancementsY1__c!=oldMap.get(fe.id).SmallEnhancementsY1__c ||
                        newMap.get(fe.id).RunningCostsImpactY1__c!=oldMap.get(fe.id).RunningCostsImpactY1__c  )) || fe.PLFundEstimatedY1__c==null)
                            fe.PLFundEstimatedY1__c = fe.OPEXOTCY1__c + fe.RunningCostsImpactY1__c + fe.SmallEnhancementsY1__c + fe.CAPEXY0__c/3;// Autopopulate if the Value is not changed and NOT null OR Calculating fields are changed.
                    if((newMap.get(fe.id).PLFundEstimatedY2__c==oldMap.get(fe.id).PLFundEstimatedY2__c &&
                        (newMap.get(fe.id).CAPEXY0__c!=oldMap.get(fe.id).CAPEXY0__c ||
                        newMap.get(fe.id).CAPEXY1__c!=oldMap.get(fe.id).CAPEXY1__c ||
                        newMap.get(fe.id).OPEXOTCY2__c!=oldMap.get(fe.id).OPEXOTCY2__c ||
                        newMap.get(fe.id).SmallEnhancementsY2__c!=oldMap.get(fe.id).SmallEnhancementsY2__c ||
                        newMap.get(fe.id).RunningCostsImpactY2__c!=oldMap.get(fe.id).RunningCostsImpactY2__c  )) || fe.PLFundEstimatedY2__c==null)
                            fe.PLFundEstimatedY2__c = fe.OPEXOTCY2__c + fe.RunningCostsImpactY2__c + fe.SmallEnhancementsY2__c + fe.CAPEXY0__c/3 + fe.CAPEXY1__c/3;// Autopopulate if the Value is not changed and NOT null OR Calculating fields are changed.
                    if((newMap.get(fe.id).PLFundEstimatedY3__c==oldMap.get(fe.id).PLFundEstimatedY3__c &&
                        (newMap.get(fe.id).CAPEXY0__c!=oldMap.get(fe.id).CAPEXY0__c ||
                        newMap.get(fe.id).CAPEXY2__c!=oldMap.get(fe.id).CAPEXY2__c ||
                        newMap.get(fe.id).CAPEXY1__c!=oldMap.get(fe.id).CAPEXY1__c ||
                        newMap.get(fe.id).OPEXOTCY3__c!=oldMap.get(fe.id).OPEXOTCY3__c ||
                        newMap.get(fe.id).SmallEnhancementsY3__c!=oldMap.get(fe.id).SmallEnhancementsY3__c ||
                        newMap.get(fe.id).RunningCostsImpactY3__c!=oldMap.get(fe.id).RunningCostsImpactY3__c  )) || fe.PLFundEstimatedY3__c==null)
                            fe.PLFundEstimatedY3__c = fe.OPEXOTCY3__c + fe.RunningCostsImpactY3__c + fe.SmallEnhancementsY3__c + + fe.CAPEXY0__c/3 + fe.CAPEXY1__c/3 + fe.CAPEXY2__c/3;// Autopopulate if the Value is not changed and NOT null OR Calculating fields are changed.
                }
            }
            Catch(Exception e){
                System.debug('------'+e);
            }   
        
        }
    }