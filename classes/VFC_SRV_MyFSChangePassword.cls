global class VFC_SRV_MyFSChangePassword extends VFC_SRV_MyFsLoginPage {

   global static boolean debugMode = false;  

   @RemoteAction
   global static ResetPasswordResult updatePasswordRemoting(String oldPassword, String password){
       
        ResetPasswordResult resetPasswordResult = new ResetPasswordResult();
        resetPasswordResult.Success = false;
        resetPasswordResult.ErrorMsg = '';   
        
        String callerId = Label.SRV_MyFs_CallerId;     
        
        Id loggedInUserId = UserInfo.getUserId();
        
        USerSAMLAssertion__c samlAssertion = [SELECT Id, Assertion__c, UserId__c FROM USerSAMLAssertion__c 
                                              WHERE UserId__c = :loggedInUserId];
        
        boolean result = false;
        
        try{
            if(samlAssertion != null ) {     
                
                uimsv2ImplServiceImsSchneiderComUM.UserManager_UIMSV2_ImplPort uimsPort = new uimsv2ImplServiceImsSchneiderComUM.UserManager_UIMSV2_ImplPort(); 
                uimsPort.endpoint_x = Label.SRV_MyFs_SetNewPasswordEndpoint;
                uimsPort.timeout_x = 120000;
                uimsPort.clientCertName_x = Label.SRV_MyFs_ClientCertName;
                
                result = uimsPort.updatePassword(callerId, samlAssertion.Assertion__c, oldPassword, password); 
                System.debug('setNewPassword result : '+result);  
        
                resetPasswordResult.Success = true;
                resetPasswordResult.ErrorMsg = Label.SRV_MyFs_Success;
            }
            else {
                resetPasswordResult.Success = false;
                resetPasswordResult.ErrorMsg = Label.SRV_MyFs_InvalidLinkErrorMessage;             
            }
        }         
        catch(exception e){
            handleException(e.getMessage(), resetPasswordResult);
        }       
        
        return resetPasswordResult;   
    }
    
    public static void handleException(String errorMessage, ResetPasswordResult resetPasswordResult) {
    
            resetPasswordResult.Success = false;
            
            if(debugMode) {
                 resetPasswordResult.ErrorMsg = 'Error: ' + errorMessage;                                                
            }
            else {
                if(errorMessage.contains('Unable to update the user password')) {
                    resetPasswordResult.ErrorMsg = 'The old password is not correct.';                                        
                }
                else {
                    resetPasswordResult.ErrorMsg = Label.SRV_MyFs_GenericErrorMessage;
                }                                                                    
            }
            
            System.debug('setNewPassword Exception: ' + errorMessage);    
    }
}