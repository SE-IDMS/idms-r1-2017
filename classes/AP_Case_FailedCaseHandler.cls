global class AP_Case_FailedCaseHandler{
    @future
    public static void doFutureCaseReconciliationProcess(String strCaseSerialized, String strEmailSerialized,String strEnvelopeSerialized) {
        Map<String,String> mapFailedCases = new Map<String,String>();
        mapFailedCases.put('TECH_TemplateName__c'       ,   'TECH_TemplateName__c'          );
        mapFailedCases.put('Origin'                     ,   'Origin__c'                     );
        mapFailedCases.put('Team__c'                    ,   'Team__c'                       );
        mapFailedCases.put('SupportCategory__c'         ,   'SupportCategory__c'            );
        mapFailedCases.put('CCCountry__c'               ,   'CCCountry__c'                  );
        mapFailedCases.put('CommercialReference_lk__c'  ,   'CommercialReference_lk__c'     );
        mapFailedCases.put('ContactId'                  ,   'Contact__c'                    );
        mapFailedCases.put('Description'                ,   'Description__c'                );
        mapFailedCases.put('TECH_EmailToCaseDebug__c'   ,   'TECH_EmailToCaseDebug__c'      );
        mapFailedCases.put('Family_lk__c'               ,   'Family_lk__c'                  );
        mapFailedCases.put('RelatedSupportCase__c'      ,   'RelatedSupportCase__c'         );
        mapFailedCases.put('Priority'                   ,   'Priority__c'                   );
        mapFailedCases.put('Symptom__c'                 ,   'Symptom__c'                    );
        mapFailedCases.put('Status'                     ,   'Status__c'                     );
        mapFailedCases.put('Subject'                    ,   'Subject__c'                    );
        mapFailedCases.put('TECH_SupportEmail__c'       ,   'TECH_SupportEmail__c'          );
        mapFailedCases.put('TECH_SPOC__c'               ,   'TECH_SPOC__c'                  );
        mapFailedCases.put('SuppliedEmail'              ,   'SuppliedEmail__c'              );
        mapFailedCases.put('CustomerRequest__c'         ,   'CustomerRequest__c'            );
        mapFailedCases.put('ReportingOrganization__c'   ,   'ReportingOrganizationId__c'    );
        mapFailedCases.put('AccountableOrganization__c' ,   'AccountableOrganizationId__c'  );
            

        Case objEmailCase = null;
        objEmailCase = (Case) JSON.deserialize(strCaseSerialized, SObject.class);
        System.debug('Deserialized in future:'+objEmailCase); 
        SObject objCase = objEmailCase;
        Schema.DescribeSObjectResult sobjDescribeResult = objCase.getsObjectType().getDescribe();
        
        String strDestinationObject ='FailedCase__c';
        Schema.SObjectType failedCaseSObjectType = Schema.getGlobalDescribe().get(strDestinationObject);
        SObject objFailedCase = failedCaseSObjectType.newSObject();
                
        for(Schema.SObjectField objField : sobjDescribeResult.fields.getMap().values()){
            if(objField.getDescribe().isAccessible() && objField.getDescribe().isCreateable() && !objField.getDescribe().isUnique() &&
                !objField.getDescribe().isAutoNumber() && !objField.getDescribe().isCalculated()){
                    if(mapFailedCases.containsKey(objField.getDescribe().getName())){
                        objFailedCase.put(mapFailedCases.get(objField.getDescribe().getName()),objCase.get(objField.getDescribe().getName()));
                    }
            }
        }      
        Database.SaveResult failedCaseSaveResult = Database.insert(objFailedCase,false);
        if(failedCaseSaveResult.IsSuccess()){
            Id faileCaseId = failedCaseSaveResult.getId();
            Messaging.InboundEmail objInboundEmail = null;
            objInboundEmail = (Messaging.InboundEmail) JSON.deserialize(strEmailSerialized, Messaging.InboundEmail.class);
            Messaging.InboundEnvelope objInboundEnvelope = null;
            objInboundEnvelope = (Messaging.InboundEnvelope) JSON.deserialize(strEnvelopeSerialized, Messaging.InboundEnvelope.class);
            
            if(objInboundEmail != null && faileCaseId != null){

                List<Attachment> lstAttachment = new List<Attachment>();
                if (objInboundEmail!=null && objInboundEmail.binaryAttachments != null && objFailedCase !=null && faileCaseId != null) {
                    for (integer i = 0 ; i < objInboundEmail.binaryAttachments.size() ; i++) {
                        // If the number of attachments is greater or equal to 100, display an error message for the user and exit the loop
                        if(i>=100){
                            break;
                        }
                        
                        Attachment objAttachment = new Attachment();
                        // If the attachment as null name, name it "Attachment i' by default
                        if(objInboundEmail.binaryAttachments[i].filename == null || objInboundEmail.binaryAttachments[i].filename.trim() == '') {
                            objAttachment.Name ='Attachment ' + i; 
                        }
                        else {
                            objAttachment.Name = objInboundEmail.binaryAttachments[i].filename;
                        }
                        
                        /* MODIFIED FOR BR-7931 - CHECKS IF THE ATTACHMENT NAME DOESNT HAVE AN EXTENSION. IF SO, APPENDS THE ATTACHMENT TYPE TO THE NAME*/
                        if(!(objAttachment.Name.contains('.'))){
                            if(objInboundEmail.binaryAttachments[i].mimeTypeSubType!=null && objInboundEmail.binaryAttachments[i].mimeTypeSubType.split('/')!=null && objInboundEmail.binaryAttachments[i].mimeTypeSubType.split('/').size()==2)
                                objAttachment.Name =  objAttachment.Name + '.'+ objInboundEmail.binaryAttachments[i].mimeTypeSubType.split('/')[1];
                        }
                        /* END OF MODIFICATION FOR BR-7931 */

                        // attach to the newly created contact record
                        objAttachment.ParentId = faileCaseId;
                        objAttachment.Body = objInboundEmail.binaryAttachments[i].body;
                        lstAttachment.add(objAttachment);
                    }
                    Database.DMLOptions DMLoption = new Database.DMLOptions();
                    DMLoption.optAllOrNone = false;
                    DMLoption.AllowFieldTruncation = true;
                    LIST<Database.SaveResult>  AttachmentSaveResult = DataBase.insert(lstAttachment,DMLoption);           
                }

                String strEmailHeader=null;
                for (Messaging.InboundEmail.Header header : objInboundEmail.headers) {
                    strEmailHeader += header.name + ' : ' + header.value + '\n';
                }
                
                // Put all to Addresses and all CC addresses in two strings
                String ToAddressList = AP24_EmailToApex.getProperAddressList(objInboundEmail.Toaddresses);
                String CCAddressList = AP24_EmailToApex.getProperAddressList(objInboundEmail.CCaddresses);
                // Assert that the from address is valid
                String strFromEmailAddress;
                if(objInboundEnvelope.fromaddress!=null && objInboundEnvelope.fromaddress !='' && Boolean.valueOf(System.Label.CLAPR15CCC04)){
                    String strEnvelopeFromAddress  = objInboundEnvelope.fromaddress;
                    String strEnvReturnFromAddress = AP24_EmailToApex.getEmailAddress(objInboundEnvelope.fromaddress);
                    if(strEnvelopeFromAddress.equalsIgnoreCase(strEnvReturnFromAddress)){
                        strFromEmailAddress = strEnvReturnFromAddress;    
                    }   
                }

                if((strFromEmailAddress==null || strFromEmailAddress=='') && (objInboundEmail.fromaddress!=null && objInboundEmail.fromaddress!='')){
                    strFromEmailAddress = AP24_EmailToApex.getEmailAddress(objInboundEmail.fromaddress);
                }

                
                String emailHTMLBody    = '';
                String emailPlainBody   = '';
                String emailSubject     = '';
                String strThreadIdExp = 'ref:\\w+\\.\\w+:ref';
                
                if(objInboundEmail.subject!=null){
                    emailSubject = AP24_EmailToApex.replaceTextUsingRegex(objInboundEmail.subject,strThreadIdExp,'');
                }
                else{
                    emailSubject= '';
                }
            
                if(objInboundEmail.plainTextBody!=null){
                    emailPlainBody = AP24_EmailToApex.replaceTextUsingRegex(objInboundEmail.plainTextBody,strThreadIdExp,'');
                }
                else{
                    emailPlainBody= '';
                }

                if(objInboundEmail.HTMLBody != null){
                    emailHTMLBody = AP24_EmailToApex.replaceTextUsingRegex(objInboundEmail.HTMLBody,strThreadIdExp,'');
                }
                else
                    emailHTMLBody = AP24_EmailToApex.ConvertPlainTextToHTML(emailPlainBody);
                /*
                if(!lstAttachment.isEmpty())
                {
                    emailHTMLBody = emailHTMLBody + '<br/> Following attachments are linked to this email:<br/>';
                    for(integer i=0;i<lstAttachment.size();i++)
                    {
                        Integer index = i+1;
                        emailHTMLBody = emailHTMLBody +index +'.'+'<a href=/servlet/servlet.FileDownload?file='+lstAttachment[i].Id+'>'+lstAttachment[i].Name+'</a><br/>'; 
                    }
                }
                else
                {
                    emailHTMLBody = emailHTMLBody + '<br/> No Attachments linked to the email <br/>';
                }
                */
                
                // Creates an EmailMessage from the Messaging.InboundEmail input
                FailedEmailMessage__c IncomingEmail = new FailedEmailMessage__c(FromAddress__c = strFromEmailAddress, 
                                                Incoming__c     = True, 
                                                ToAddress__c    = ToAddressList, 
                                                CcAddress__c    = CCAddressList, 
                                                Subject__c      = emailSubject, 
                                                TextBody__c     = emailPlainBody,
                                                Headers__c      = strEmailHeader,
                                                HTMLBody__c     = emailHTMLBody,
                                                ParentID__c     = faileCaseId, 
                                                MessageDate__c  = DateTime.now());
                
                // Add a DML option in order to automatically truncate fields
                Database.DMLOptions DMLoption = new Database.DMLOptions(); 
                DMLoption.allowFieldTruncation = true;
                DMLoption.optAllOrNone = false;  
                              
                DataBase.Saveresult IncomingEmail_SR = DataBase.insert(IncomingEmail,DMLoption );  
            }           
        }
    }
}