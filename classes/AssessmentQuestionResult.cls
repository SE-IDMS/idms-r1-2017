global class AssessmentQuestionResult
{
	// WS error codes & error message
    WebService Integer returnCode;
    WebService String message;
    WebService Integer totalNoOfAssessmentQuestions; 
    WebService AssessmentQuestion[] assessmentQuestions { get; set; }
}