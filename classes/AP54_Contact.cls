/*
07-Dec-2012 Srinivas Nallapati  Dec12 AC release    
*/
public without sharing class AP54_Contact {
    
    public static void  copyAddressFromAccount(List<Contact> lstCon, Set<Id> setAccIds, Set<String> countryCodeLst, Set<String> stateCodeLst)
    {
        Map<String,Country__c> countryMap = new Map<String,Country__c>();
        Map<String,StateProvince__c> stateMap = new Map<String,StateProvince__c>();
        
        Map<id,Account> mapAccs = new map<id,Account>([select StateProvince__c,POBoxZip__c, POBox__c, LocalCounty__c, County__c, Country__c, ZipCode__c, LocalCity__c, City__c, LocalAdditionalAddress__c, AdditionalAddress__c, Street__c, StreetLocalLang__c, AutomaticGeolocation__latitude__s,AutomaticGeolocation__longitude__s,BillingState,BillingCountry from account where id in :setAccIds]);
        
        for(Country__c ct : [SELECT ID, CountryCode__c, Name FROM Country__c WHERE CountryCode__c IN :countryCodeLst])
        {
            countryMap.put(ct.CountryCode__c,ct);
        }
        
        for(StateProvince__c st: [SELECT ID, Name, StateProvinceCode__c, CountryCode__c FROM StateProvince__c WHERE CountryCode__c IN :countryCodeLst AND StateProvinceCode__c IN :stateCodeLst])
        {
            String ctyStateKey = st.CountryCode__c + st.StateProvinceCode__c;
            stateMap.put(ctyStateKey,st);
        }
        
        for(Contact con: lstCon)
        {
           if(con.UpdateAccountAddress__c)
           {
                Account acc = mapAccs.get(con.AccountId);
                if(acc != null)
                {
                    con.Street__c = acc.Street__c;
                    con.StreetLocalLang__c = acc.StreetLocalLang__c;
                    con.AdditionalAddress__c = acc.AdditionalAddress__c;
                    con.LocalAdditionalAddress__c = acc.LocalAdditionalAddress__c;
                    con.City__c = acc.City__c;
                    con.LocalCity__c = acc.LocalCity__c;
                    con.ZipCode__c = acc.ZipCode__c;
                    con.StateProv__c = acc.StateProvince__c;
                    con.Country__c = acc.Country__c;
                    con.County__c = acc.County__c;
                    con.LocalCounty__c = acc.LocalCounty__c;
                    con.POBox__c = acc.POBox__c;
                    con.POBoxZip__c = acc.POBoxZip__c;
                    con.Geolocation__latitude__s=acc.AutomaticGeolocation__latitude__s;
                    con.Geolocation__longitude__s=acc.AutomaticGeolocation__longitude__s; 
                    
                    //BackToStandard Q2 2016 - Added below logic to standard prepopulate standard State and Country fields based on Account, when InheritAddress is checked.
                    con.OtherCountry=acc.BillingCountry;
                    con.OtherState=acc.BillingState;
                }
           }   
           //BackToStandard Q2 2016 - Added below logic to prepopulate custom State and Country fields for all contacts with InheritAddress checkbox unchecked.
           else
           {
               if(String.isNotBlank(con.OtherCountryCode))
                con.Country__c = countryMap.get(con.OtherCountryCode).Id;
               if(String.isNotBlank(con.OtherStateCode))    
                con.StateProv__c = stateMap.get(con.OtherCountryCode+con.OtherStateCode).Id;
           }
           System.debug('*******Contact -> ** '+con);
        }
    }//End of method
}