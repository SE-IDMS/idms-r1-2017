@isTest
private class VFC_CaseRelatedProductDelete_TEST{
    static Country__c country;
    static Account objAccount;
    static contact objContact;
    static
    {
        country = Utils_TestMethods.createCountry();
        Database.insert(country);
        
        objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = country.id;        
        Database.insert(objAccount);
        
        objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        objContact.Country__c = country.id;
        Database.insert(objContact);
    }
    static testMethod void CaseRelatedProductDeleteNonMaster_TestMethod() {
               
        List<OPP_Product__c> lstProduct = new List<OPP_Product__c>();
        OPP_Product__c objProduct1 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU','TEST_PL','TEST_PF','TEST_FLMY');
        OPP_Product__c objProduct2 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU2','TEST_PL2','TEST_PF2','TEST_FLMY2');
         objProduct1.CCCRelevant__c = true;
        objProduct2.CCCRelevant__c = true;
        lstProduct.add(objProduct1);
        lstProduct.add(objProduct2);
        
        Database.insert(lstProduct);
        
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        objOpenCase.Family_lk__c = objProduct1.Id;
        Database.insert(objOpenCase);
        
        Test.startTest();
        
        objOpenCase.Family_lk__c = objProduct2.Id;
        Database.update(objOpenCase);
        List<CSE_RelatedProduct__c> lstCaseRelatedProduct = new List<CSE_RelatedProduct__c>([Select Id,MasterProduct__c From CSE_RelatedProduct__c where Case__c =: objOpenCase.Id]);
        if(lstCaseRelatedProduct.size()>0){
            ApexPages.StandardController CaseRelatedProductStandardController = new ApexPages.StandardController(lstCaseRelatedProduct[0]);   
            VFC_CaseRelatedProductDelete DeleteCaseRelatedProductPage = new VFC_CaseRelatedProductDelete(CaseRelatedProductStandardController);
        
            PageReference pageRef = Page.VFP_CaseRelatedProductDelete;
            Test.setCurrentPage(pageRef);
            DeleteCaseRelatedProductPage.deleteCaseRelatedProduct();
        }
        Test.stopTest();
    }
    static testMethod void CaseRelatedProductDeleteMaster_TestMethod() {
        List<OPP_Product__c> lstProduct = new List<OPP_Product__c>();
        OPP_Product__c objProduct1 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU','TEST_PL','TEST_PF','TEST_FLMY');
        OPP_Product__c objProduct2 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU2','TEST_PL2','TEST_PF2','TEST_FLMY2');
        objProduct1.CCCRelevant__c = true;
        objProduct2.CCCRelevant__c = true;        
        lstProduct.add(objProduct1);
        lstProduct.add(objProduct2);
        
        Database.insert(lstProduct);
        
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        objOpenCase.Family_lk__c = objProduct1.Id;
        Database.insert(objOpenCase);

        CSE_RelatedProduct__c cseRelProd = new CSE_RelatedProduct__c();
        cseRelProd.Family__c = objProduct1.Id;
        cseRelProd.Case__c = objOpenCase.Id;
        Database.insert(cseRelProd);
        
        Test.startTest();
        
        objOpenCase.Family_lk__c = objProduct2.Id;
        Database.update(objOpenCase);
        List<CSE_RelatedProduct__c> lstCaseRelatedProduct = new List<CSE_RelatedProduct__c>([Select Id,MasterProduct__c From CSE_RelatedProduct__c where Case__c =: objOpenCase.Id]);
        if(lstCaseRelatedProduct.size()>0){
            ApexPages.StandardController CaseRelatedProductStandardController = new ApexPages.StandardController(lstCaseRelatedProduct[0]);   
            VFC_CaseRelatedProductDelete DeleteCaseRelatedProductPage = new VFC_CaseRelatedProductDelete(CaseRelatedProductStandardController);
        
            PageReference pageRef = Page.VFP_CaseRelatedProductDelete;
            Test.setCurrentPage(pageRef);
            try{
                DeleteCaseRelatedProductPage.deleteCaseRelatedProduct();
            }
            catch(Exception ex){
            }
            DeleteCaseRelatedProductPage.pageCancelFunction();
            
        }
        Test.stopTest();
    }
    
    static testMethod void CaseRelatedProductDeleteMasterOther_TestMethod() {
        List<OPP_Product__c> lstProduct = new List<OPP_Product__c>();
        OPP_Product__c objProduct1 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU','TEST_PL','TEST_PF','TEST_FLMY');
        OPP_Product__c objProduct2 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU2','TEST_PL2','TEST_PF2','TEST_FLMY2');
        objProduct1.CCCRelevant__c = true;
        objProduct2.CCCRelevant__c = true;        
        lstProduct.add(objProduct1);
        lstProduct.add(objProduct2);
        
        Database.insert(lstProduct);
        
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        objOpenCase.Family_lk__c = objProduct1.Id;
        Database.insert(objOpenCase);

        CSE_RelatedProduct__c cseRelProd = new CSE_RelatedProduct__c();
        cseRelProd.Family__c = objProduct1.Id;
        cseRelProd.Case__c = objOpenCase.Id;
        Database.insert(cseRelProd);
        
        Test.startTest();
        
        objOpenCase.Family_lk__c = objProduct2.Id;
        Database.update(objOpenCase);
        List<CSE_RelatedProduct__c> lstCaseRelatedProduct = new List<CSE_RelatedProduct__c>([Select Id,MasterProduct__c From CSE_RelatedProduct__c where Case__c =: objOpenCase.Id]);
        if(lstCaseRelatedProduct.size()>0){
            ApexPages.StandardController CaseRelatedProductStandardController = new ApexPages.StandardController(lstCaseRelatedProduct[1]);   
            VFC_CaseRelatedProductDelete DeleteCaseRelatedProductPage = new VFC_CaseRelatedProductDelete(CaseRelatedProductStandardController);
        
            PageReference pageRef = Page.VFP_CaseRelatedProductDelete;
            Test.setCurrentPage(pageRef);
            try{
                DeleteCaseRelatedProductPage.deleteCaseRelatedProduct();
            }
            catch(Exception ex){
            }
            DeleteCaseRelatedProductPage.pageCancelFunction();
            
        }
        Test.stopTest();
    }
}