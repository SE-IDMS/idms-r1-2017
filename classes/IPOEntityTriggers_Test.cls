@isTest
private class IPOEntityTriggers_Test {
    static testMethod void testIPOEntityTriggers() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPO10');
                System.runAs(runAsUser){
                        IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
            IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation__c='NA;APAC',Year__c='2013');
            insert IPOp;
            
            //+Ve
            Test.startTest();

            IPO_Strategic_Entity_Progress__c CEp1=new IPO_Strategic_Entity_Progress__c(IPO_Strategic_Program__c=IPOp.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM');
            insert CEp1;
            IPO_Strategic_Entity_Progress__c CEp2=new IPO_Strategic_Entity_Progress__c(IPO_Strategic_Program__c=IPOp.id,Scope__c=Label.Connect_Global_Business,Entity__c='ITB');
            insert CEp2;
            IPO_Strategic_Entity_Progress__c CEp3=new IPO_Strategic_Entity_Progress__c(IPO_Strategic_Program__c=IPOp.id,Scope__c=Label.Connect_Power_Region,Entity__c='NA');
            insert CEp3; 
            
             //-Ve

            IPO_Strategic_Entity_Progress__c CEp5=new IPO_Strategic_Entity_Progress__c(IPO_Strategic_Program__c=IPOp.id,Scope__c=Label.Connect_Global_Business,Entity__c='ITB');
             try{
            insert CEp5;
            }catch(DmlException e){
            e.getMessage();
            }
            CEp1.Entity__c='test';
            try{
            update CEp1;
            }catch(DmlException d)
            {
            d.getMessage();
            }           
                            
            CEp2.Entity__c='test';
            try{                
            update CEp2;
            }catch(DmlException x)
            {
            x.getMessage();
            }

            Test.stopTest();
            
            
            IPO_Strategic_Program__c IPOp1=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id,Year__c='2013');
            insert IPOp1;
            IPO_Strategic_Entity_Progress__c CEp8=new IPO_Strategic_Entity_Progress__c(IPO_Strategic_Program__c=IPOp1.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM');
            try{
            insert CEp8;
            }catch(DmlException dm)
            {
            dm.getMessage();
            }
            IPO_Strategic_Entity_Progress__c CEp9=new IPO_Strategic_Entity_Progress__c(IPO_Strategic_Program__c=IPOp1.id,Scope__c=Label.Connect_Global_Business,Entity__c='ITB');
            try{
            insert CEp9;
            }catch(DmlException ex)
            {
            ex.getMessage();
            }
            IPO_Strategic_Entity_Progress__c CEp10=new IPO_Strategic_Entity_Progress__c(IPO_Strategic_Program__c=IPOp1.id,Scope__c=Label.Connect_Power_Region,Entity__c='test');
            try{
            insert CEp10;
            }catch(DmlException ep)
            {
            ep.getMessage();
            }
            

            }
        }
}