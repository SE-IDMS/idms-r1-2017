@isTest
public class IPOStrtProgramScopeListController_TEST {

    static testMethod void IPOStratProgramScopeTest() {
        
         User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPO7');
        System.runAs(runAsUser){
             IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
            IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp;
            Champions__c Champ=new Champions__c(Scope__c='Global Functions',Entity__c='GM');
            insert champ;
            Champions__c Champ1=new Champions__c(Scope__c='Global Business',Entity__c='ITB');
            insert champ1;
            IPO_Strategic_Deployment_Network__c C=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Functions', Entity__c='GM', Champion__c = runAsUser.id, Deployment_Leader__c = runAsUser.id);
            insert C;
            IPO_Strategic_Deployment_Network__c C1=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Business', Entity__c='ITB');
            insert C1;
            IPO_Strategic_Deployment_Network__c C2=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Operation Regions', Entity__c='NA');
            insert C2;
            
            IPO_Strategic_Deployment_Network__c C3=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Functions', Entity__c='GM', Champion__c = runAsUser.id, Deployment_Leader__c = null);
            insert C3;
            
            IPO_Strategic_Deployment_Network__c C4=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Functions', Entity__c='GM', Champion__c = null, Deployment_Leader__c = runAsUser.id);
            insert C4;
    
            //Create the Instance of the Page and Set that Page as the page in current operation.
            PageReference vfPage = Page.IPO_Program_Network;
            Test.setCurrentPage(vfPage);
            
            //Add parameters to page URL
            ApexPages.currentPage().getParameters().put('Id',IPOp.id);
     
            //Create the Instance of the Controller and call its methods.
            IPOStrategicProgramScopeListController PN = new IPOStrategicProgramScopeListController(new ApexPages.StandardController(IPOp));
            Test.startTest();
            
            //Call the Controller methods with the newly created Global Program.
            PN.getPageid();       
            PN.getProgramNetwork();
            PN.getprogram();
            PN.AddDeploymentNetwork();
            
            //Update the created Global Program to meet alternate condition.
            IPOp.Global_Functions__c='GM';
             
            update IPop;
            
            //Call the Controller methods with the updated Global Program.
            PN.getPageid();       
            PN.getProgramNetwork(); 
           
              
            Test.stopTest();
        }
   }
}