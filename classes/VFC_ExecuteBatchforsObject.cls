Public class VFC_ExecuteBatchforsObject {
    
    public List<ReferentialDataMapping__c>  listrefDataMapping{get;set;}
    public map<string,string> mapToFromObject{get;set;}
    public map<string,string> mapLabelToObjectFromObject{get;set;}
    public static string  selectObjectName{get;set;}
    public static string  iSelectObjectName{get;set;}
    public Map<string,List<ReferentialDataWarap>> mapWarpRefDataMapping{get;set;}
    public Map<string,List<ReferentialDataMapping__c>>  mapRefDataMapping{get;set;}
    public boolean isSelected{get;set;}
    public boolean isErrorMessg{get;set;}
    public boolean isErrorRecord{get;set;}
    public boolean isBtachExceuteRerd{get;set;}
    public map<string,BatchReferentialDataWarap> mapWarapRefdata{get;set;}
    public list<BatchReferentialDataWarap> listWarapRefdata{get;set;}
    
    public VFC_ExecuteBatchforsObject() {
    system.debug('INside COn##########');
        isSelected=false;
        isErrorMessg=false;
        isErrorRecord =false;
        mapWarapRefdata= new map<string,BatchReferentialDataWarap>();
        listWarapRefdata = new list<BatchReferentialDataWarap>();
        listrefDataMapping = new List<ReferentialDataMapping__c>();
         mapRefDataMapping =new Map<string,List<ReferentialDataMapping__c>>();
        mapToFromObject = new map<string,string>(); 
        listrefDataMapping = [select id, Active__c, FromObject__c, ToObject__c, ToField__c, FromField__c, WhereClauseFromField__c, WhereClauseToField__c from ReferentialDataMapping__c where Active__c=true];
        
       
    
    }   
    public void RunBatch(string sumbObject, boolean isErorOrBatchRecords) {
          boolean bln =isErorOrBatchRecords; 
          try { 
          Batch_ProductGMTUpdate batchRef = new Batch_ProductGMTUpdate(bln,sumbObject); // call batch class
          id batchId=Database.executeBatch(batchRef);
         }catch(exception exp) {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,exp.getmessage());
                ApexPages.addMessage(myMsg); 
                isErrorMessg =true;
               
         
         }
          
    }
    public void Run_Batch() {
    
        mapWarpRefDataMapping = new Map<string,List<ReferentialDataWarap>>();
        mapWarapRefdata= new map<string,BatchReferentialDataWarap>();
        listWarapRefdata = new list<BatchReferentialDataWarap>();
        for(ReferentialDataMapping__c RefDat:listrefDataMapping) {
                ReferentialDataWarap refdataWp = new ReferentialDataWarap();
                mapToFromObject.put(RefDat.ToObject__c,RefDat.FromObject__c);
                if(mapRefDataMapping.containsKey(RefDat.ToObject__c)) {
                    mapRefDataMapping.get(RefDat.ToObject__c).add(RefDat);
                    
                }else {
                    mapRefDataMapping.put(RefDat.ToObject__c,new List<ReferentialDataMapping__c>{RefDat});
                    
                }
                if(mapWarpRefDataMapping.containsKey(RefDat.ToObject__c)) {
                    refdataWp.refdata=RefDat;
                    refdataWp.blnSelected=false;
                    mapWarpRefDataMapping.get(RefDat.ToObject__c).add(refdataWp);
                }else {
                    refdataWp.refdata=RefDat;
                    refdataWp.blnSelected=false;
                    mapWarpRefDataMapping.put(RefDat.ToObject__c,new List<ReferentialDataWarap>{refdataWp});
                }
                
                  mapWarapRefdata.put(RefDat.ToObject__c,new BatchReferentialDataWarap(RefDat.ToObject__c, RefDat.FromObject__c,''));
                           
        }
        if(mapWarapRefdata.size() > 0) {
            listWarapRefdata=mapWarapRefdata.values();
        }
    
    }
    public pageReference callBacthClass() {
        system.debug('Test-->'+selectObjectName);
        system.debug('Test-->!'+iSelectObjectName);
         system.debug('TEST *888888888***'+listWarapRefdata);
         Map<string,boolean> mapIsErrorRecord = new Map<string,boolean>();
         
         
        for(BatchReferentialDataWarap wrpObj:listWarapRefdata) {
            if(wrpObj.bln_Selected==true) {
                mapIsErrorRecord.put(wrpObj.toObject,wrpObj.bln_Selected);
            }
         
         }
         if(mapIsErrorRecord.size() >0)  {
            if(iSelectObjectName!=null && iSelectObjectName!='' && selectObjectName==iSelectObjectName) {
            If(selectObjectName!=null) {
                if(mapIsErrorRecord.get(selectObjectName)==true) {
                    RunBatch(selectObjectName,true);
                }
                return null;
            }


            }else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Please select sOBject for Job Execution');
                ApexPages.addMessage(myMsg); 
                isErrorMessg =true;
                return null;


            }   
         
         }else {
            if(iSelectObjectName!=null && iSelectObjectName!='' && selectObjectName==iSelectObjectName) {
                If(selectObjectName!=null) {
                   RunBatch(selectObjectName,false);
                    return null;
                }


            }else {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,label.CLDEC14REF07);
                ApexPages.addMessage(myMsg); 
                isErrorMessg =true;
                return null;


            }   
         
         }
           system.debug('Test-->!!!2!'+isBtachExceuteRerd);
        
    return null;
    }
    
  
    

public Id batchId;
Public void RunBtachJob(Id bacth_id ) {
    batchId=bacth_id;
    if(batchId!=null) {
        getBatchJobsStatus();
    }

}

 public List<Batch_Status> batchJobs;
    public Integer numberOfJobs {get; set;}

    public List<Batch_Status> getBatchJobsStatus(){
        // wrapper class.
        batchJobs = new List<Batch_Status>();

        //Job Size
        if(numberOfJobs== null || numberofJobs <= 0){
            numberofJobs =Integer.valueof(label.CLDEC14REF02);
        }

        map<string,string> bgColorMap=new map<string,string>();
        bgColorMap.put('Queued','#f8f8f8');
        bgColorMap.put('Processing','#f8f8f8');
        bgColorMap.put('Aborted','#551A8B');
        bgColorMap.put('Completed','#f8f8f8');
        bgColorMap.put('Failed','#9E0508');
        bgColorMap.put('Preparing','#f8f8f8');

        map<string,string> fgColorMap=new map<string,string>();
        fgColorMap.put('Queued','#F7B64B');
        fgColorMap.put('Processing','#F7B64B');
        fgColorMap.put('Aborted','#B23AEE');
        fgColorMap.put('Completed','#20F472');
        fgColorMap.put('Failed','#FFB6C1');
        fgColorMap.put('Preparing','#F7B64B');

        //Query the Batch jobs
        for(AsyncApexJob a : [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, ExtendedStatus, Id, CreatedDate, CreatedById, CompletedDate, ApexClassId, ApexClass.Name From AsyncApexJob WHERE JobType='BatchApex' and ApexClass.Name='Batch_ProductGMTUpdate'  order by CreatedDate desc limit :numberOfJobs]){
            Double itemsProcessed = a.JobItemsProcessed;
            Double totalItems = a.TotalJobItems;

            Batch_Status j = new Batch_Status();
            j.job = a;

            
            if(totalItems == 0){
                
                j.percentComplete = 0;
            }else{
                j.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
            }
            j.bgStatusColor=bgColorMap.get(a.Status);
            j.fgStatusColor=fgColorMap.get(a.Status);

            batchJobs.add(j);
        }
    return batchJobs;
}

    //wrapper class 
    public Class Batch_Status{
        public AsyncApexJob job {get; set;}
        public Integer percentComplete {get; set;}
        public string bgStatusColor {get;set;}
        public string fgStatusColor {get;set;}

        public Batch_Status(){
            this.job=null;
            this.percentComplete=0;
            bgStatusColor='';
            fgStatusColor='';
        }
    }
    Public class ReferentialDataWarap{
        public ReferentialDataMapping__c  refdata{get;set;}
        public boolean blnSelected{get;set;}
        public  ReferentialDataWarap() {
            this.refdata=null;
            this.blnSelected=false;
        
        }
    
    }
    Public class BatchReferentialDataWarap{
        public string toObject{get;set;}
        public string fromObject{get;set;}
        public boolean bln_Selected{get;set;}
        public string conditionExpr{get;set;}
        public  BatchReferentialDataWarap(string tObject,string fObject,string condition_Expr) {
            toObject=tObject;
            fromObject=fObject;
            conditionExpr=condition_Expr;
            bln_Selected =false;
        
        }
    
    }
}