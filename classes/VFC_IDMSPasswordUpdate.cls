//*******************************************************************************************
//Created by      : Ranjith
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : class used for updating user password
//
//*******************************************************************************************

global with sharing class VFC_IDMSPasswordUpdate{
    public String getAppUrl() {
        return null;
    }
    
    public boolean redirectToApp{ get; set; }
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;} 
    public String registrationSource , context ; 
    public string appid{get; set;} 
    public boolean successMsg {get;set;}
    public string appUrl{get;set;}
    public boolean incorrectAppId {get;set;}
    public string appMobileFooterImg{get; set;}
    public string redirectionId {get;set;}
    public Boolean errorMessage{get;set;}
    public String ErrorMsgCode {get;set;}
    public Boolean isSession{get;set;}
    public static string username1{get;set;}
    public string sessionURL{get;set;}
    public string pageErrorMsg{get;set;}
    public string language{get;set;}
    public string applicationId{get; set;}
    public string applicationName {get;set;} 
    public string appandse{get;set;}
    public boolean isLogoPresent{get;set;}
    public string redirectURL{get;set;}
    public string FirstName{get;set;}
    public string LastName{get;set;}
    public string appTabName{get;set;}
    public string appTabLogo{get; set;}

    public boolean expiresPassword{get; set;}
    
    //constructor used for redirections and fetching values from custom setting
    public VFC_IDMSPasswordUpdate() { 
        incorrectAppId      = false;
        successMsg          = false;
        isSession           = false ;
        expiresPassword     = true; 
        user user           = [select usertype,username ,name,email from user where id=:UserInfo.getUserId()];
        username1           = user.username;
        
        redirectURL=ApexPages.currentPage().getParameters().get('retURL');
        system.debug(redirectURL);
        System.debug('User details--> ' + user.usertype + '- - - ' + user.username ) ;
        
        if (user.usertype.equalsignorecase('Guest')) {
            throw new NoAccessException();
        }
        appid = ApexPages.currentPage().getParameters().get('app');
        system.debug('controller appid --> ' + appid ) ;  
        IDMSApplicationMapping__c appMap;
        appMap = IDMSApplicationMapping__c.getInstance(appid);
        appAndSE                        = Label.CLR117IDMS004;
        // added to show application specific TabName as in BFOID-523
        appTabName                      = Label.CLNOV16IDMS128;
        appTabName = (String)appMap.get('AppTabName__c');
                    if(String.isBlank(AppTabName)){
                        AppTabName     = Label.CLNOV16IDMS128;
                    }
          // added to show application specific TabLogo as in BFOID-523
          appTabLogo                      = Label.CLFEB17IDMS004;
          appTabLogo         = (String)appMap.get('AppTabLogo__c');
                    if(String.isBlank(AppTabLogo)){
                        AppTabLogo     = Label.CLFEB17IDMS004;
                    } 
            if(appMap != null){   
            context             = (String)appMap.get('context__c');
            registrationSource  = (String)appMap.get('name');
            appMobileFooterImg  = (String)appMap.get('AppMobileFooterImage__c');
            //language            = (String)appMap.get('Language__c');
            applicationId       = (String)appMap.get('AppProfileURL__c');
            applicationName     = (String)appMap.get('name');
            appAndSE            = (String)appMap.get('AppBrandOrLogo__c');
        }
        
        
        else{
            system.debug('inside else');
            incorrectAppId      = true;  
        }
        
        if(string.isnotblank(appAndSe)){
        isLogoPresent=true;
        }
        else{
        appAndSE                        = Label.CLR117IDMS004;
        }
        if(String.isBlank(appid)){
            incorrectAppId      = true;
        }
        
        if(ApexPages.currentPage().getUrl().contains('retURL')){
            incorrectAppId      = false;
            expiresPassword     =false;
        }
        
        //Prashanth
        //Added as a part of New Password Policy
        List<User> objUser=new List<User>();
        objUser=[Select Firstname,Lastname from User where Id=:UserInfo.getUserId()];
        if(!objUser.isEmpty()){
            FirstName= objUser[0].Firstname;
            Lastname= objUser[0].Lastname;
        }
        
        system.debug('controller registration source-->' + registrationSource  + ' ' + appid + ' ' + context) ;
    }
    
    //method used for updating user password
    public PageReference updatePassword() { 
        IdmsPasswordPolicy policy=new IdmsPasswordPolicy();
        PageReference successPg = new PageReference('/apex/IDMSPasswordUpdateSuccess'); 
        successPg.getParameters().put('app',appid) ;
        if(string.isnotblank(redirectURL)){
        successPg=new PageReference(redirectURL);
        } 
        IDMSPasswordService passwordService = new IDMSPasswordService();
        
        system.debug('userid--> ' + UserInfo.getUserId() + ' Session ' + UserInfo.getSessionId()) ; 
        system.debug('registration source-->' + registrationSource  + ' ' + appid + ' ' + context) ;
        User user ; 

        if(String.isBlank(oldPassword)) {
            return null;
        }
        else if(String.isBlank(newPassword)) {
            return null;
        }
        else if(String.isBlank(verifyNewPassword)) {
            return null;
        }
        else if(String.IsBlank(registrationSource)) {
            return null;
        }
        else if( verifyNewPassword != newPassword ) {
            return null;
        }
        //Prashanth
        //Modified as a part of new password policy 
        //else if(!Pattern.matches(Label.CLNOV16IDMS129, newPassword)){
        else if(!policy.checkPolicyMetOrNotWithFNLN(newPassword,FirstName,LastName)){
          return null;
             
        } 
        //else if(!Pattern.matches(Label.CLNOV16IDMS129, verifyNewPassword) ){
        else if(!policy.checkPolicyMetOrNotWithFNLN(verifyNewPassword,FirstName,LastName)){
         return null;       
        }
        else {      
            user = [select id,FederationIdentifier,username from user WHERE id =:UserInfo.getUserId() limit 1];
            system.debug('user size--> ' + user  ) ;
            if(user != null ) {
                try{
                    if( oldPassword != null && !(passwordService.LoginCheck(user.username,oldPassword))){
                        return errorMsgProcess('update.oldPassword');
                    }
                    if(oldPassword == newPassword ) {
                        return errorMsgProcess('update.oldNewPassword');
                    }
                    
                    Pagereference pg = Site.changePassword(newPassword, newPassword,oldPassword);
                    if(pg == null ) {
                        system.debug('pg--> '  + pg ); 
                        pageErrorMsg = 'Your Old Password Is Invalid';
                        return errorMsgProcess('update.catch');
                    }else if(pg != null || Test.isRunningTest()) {  
                        system.debug('pg--> ' + pg ) ;
                        IDMSApplicationMapping__c appMap1;
                        appMap1     = IDMSApplicationMapping__c.getInstance(appid);
                        if(string.isnotblank((string)appMap1.UrlRedirectAfterPwUpdate__c)){
                            appurl  = (string)appMap1.UrlRedirectAfterPwUpdate__c;
                        }else{
                            appurl  = '/identity/idp/login?app='+appMap1.appid__c;
                        }
                        
                        successMsg =true;
                        if(!registrationSource.equalsIgnoreCase(Label.CLQ316IDMS042)){         
                            user userFedId              = [select federationidentifier,IDMS_VU_NEW__c from user where id=:user.id];
                            IDMSEncryption encryption   = new IDMSEncryption();
                            String samlAssrestion       = encryption.ecryptedToken(userFedId.federationidentifier,userFedId.IDMS_VU_NEW__c);
                            
                            IDMSUIMSWebServiceCalls.updatePasswordUIMS(oldPassword,newPassword,samlAssrestion);
                        } else{ 
                        }
                        return successPg ;
                    }
                }
                catch(Exception e){
                    system.debug('exception --> ' + e.getmessage()); 
                    return errorMsgProcess('update.catch');
                }
                
            }
        }
        return null ; 
    }
    
    // redirecting user to profile page
    public PageReference profileRedirect() {
        PageReference redirectpg = new PageReference('/apex/UserProfile');
        redirectpg.getParameters().put('app',applicationName) ; 
        return redirectpg ; 
        
    }
         Public PageReference errorMsgProcess(String msgCode){
            errorMessage    = false;    
            ErrorMsgCode    = msgCode;
            return null;  
        }  
    
}