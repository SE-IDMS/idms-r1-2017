@isTest

public class FieloPRM_AP_CompletionPercentageConTest{
    
    static testMethod void unitTest(){
        
         User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            //FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloCH__Challenge__c challenge =  FieloPRM_UTILS_MockUpFactory.createChallenge('Challenge TEST', 'Competition', 'Objective');

            FieloEE__Menu__c menu = new FieloEE__Menu__c(Name = 'home', FieloEE__Home__c = true );
            insert menu;

            challenge.F_PRM_MenuOverview__c = menu.id ;

            update challenge;

            ApexPages.currentPage().getParameters().put('idMenu', menu.id);

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );

            FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission1.id );     
            FieloCH__MissionChallenge__c missionChallenge2  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission2.id );     
            
            FieloCH__ChallengeReward__c challengeReward  = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge );      
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            update challengeReward;

            challenge =  FieloPRM_UTILS_MockUpFactory.activateChallenge(challenge);


            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'LastName'+System.Label.CLMAR13PRM05;
            member.FieloEE__FirstName__c = 'FirstName'+ + String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;                
            
            insert member;  
                        
            FieloEE.MemberUtil.setMemberId(member.id);

            //FieloCH__ChallengeMember__c challengemember = FieloPRM_UTILS_MockUpFactory.createChallengeMember(challenge, member);

            FieloPRM_AP_CompletionPercentageCon completionClas = new FieloPRM_AP_CompletionPercentageCon();

            completionClas.challengeId = challenge.id;
            list<String> liststring =completionClas.fieldset;

            PageReference pageref = completionClas.JoinAction();
        }
    }

}