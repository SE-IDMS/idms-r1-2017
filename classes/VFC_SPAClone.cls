public class VFC_SPAClone
{
    public Boolean flag{get;set;}
    public Boolean interim{get;set;}
    public Boolean check{get;set;}
    public SPARequest__c spa{get;set;}{spa=new SPARequest__c();}
    public SPARequest__c spa1{get;set;}{spa1=new SPARequest__c();}
    public SPARequest__c clonedspa{get;set;}{clonedspa=new SPARequest__c();}
   // public static Savepoint sp;
        
    public VFC_SPAClone(ApexPages.StandardController controller)
    {
        System.debug('Constructor is called>>>>#####');
        interim=false;
        flag=false;
        spa= (SPARequest__c )controller.getRecord();
        SPARequest__c spa1=[Select Id,Account__c,BackOfficeSystemID__c,Contact__c,Channel__c,ChannelContact__c,ExceptionInformations__c,Opportunity__c,RequestedType__c,Salesperson__c,Salesperson__r.IsActive,SPAReason__c,CurrencyIsoCode from SPARequest__c where id =:spa.id];
        spa.Account__c=spa1.Account__c;
        spa.Contact__c=spa1.Contact__c;
        spa.Channel__c=spa1.Channel__c;
        spa.ChannelContact__c=spa1.ChannelContact__c;
        spa.BackOfficeSystemID__c=spa1.BackOfficeSystemID__c;
        spa.ExceptionInformations__c=spa1.ExceptionInformations__c;
        spa.Opportunity__c=spa1.Opportunity__c;
        spa.RequestedType__c=spa1.RequestedType__c;   
        if(spa1.Salesperson__r.IsActive)
            spa.Salesperson__c=spa1.Salesperson__c;
        spa.SPAReason__c=spa1.SPAReason__c;
        spa.CurrencyIsoCode =spa1.CurrencyIsoCode ;
        
        //Only SPA Request having 200 lines can be cloned
        Integer cnt=[select count() from SPARequestLineItem__c where SPARequest__c=:spa.id];
        System.debug('>>>>>Number of lines>>>>'+cnt+'#####Limit in bFO is ###'+Integer.valueOf(System.Label.CLOCT14SLS68));
        if(cnt>Integer.valueOf(System.Label.CLOCT14SLS68)){
            flag=true;
            interim=true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLOCT14SLS54));
        }
        else{
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.CLOCT14SLS55));
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.CLOCT14SLS66));
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,System.Label.CLOCT14SLS65));
       }
    }
     public pageReference proceedClone()
    {
        system.debug('proceedClone Method Begins');
        interim=true;
        return null;
     }
      public pageReference continueClone()
    {
        Transient Savepoint sp;
        /*For NL, Backoffice Account Number and Back Office channel Number is always populated from the Legacy Accounts.
        For HK, Back office account number usualy entered manually and Channel is not used by them*/
        if(spa.BackOfficeCustomerNumber__c==null||spa.BackOfficeCustomerNumber__c=='' ||spa.BackOfficeSystemID__c=='NL_SAP')
            populateBackofficeAccno();
        if(spa.Channel__c!=null && (spa.ChannelLegacyNumber__c==null || spa.ChannelLegacyNumber__c==''))
           populateChannelLegacyNumber();
        clonedspa=spa.clone();
        //Original SPA from where SPA is cloned.
        clonedspa.REP_OriginalSPA__c=spa.id;
        System.debug('#####continueClone######');
        String querySales1 = Utils_Methods.generateSelectAllQuery(new SPARequestLineItem__c());
        String querySales2 = ' '+'where SPARequest__c =\''+spa.id+'\'';
        String querySpalines = querySales1 + querySales2;
        System.debug('#### SPA Request Line Items Query: '+querySpalines );
         list<SPARequestLineItem__c>  currentlines= Database.Query(querySpalines);
         // DML is done only after the webservice is called
         if(currentlines.size()>0){
             System.debug('**** You have line Items to clone *****');
            list<SPARequestLineItem__c>  spaLines= new  list<SPARequestLineItem__c>();
            //Webservice variables
            WS_ProductandPricing.ProductAndPricingInformationPort  spaconnection=new WS_ProductandPricing.ProductAndPricingInformationPort();
            spaconnection.ClientCertName_x=System.Label.CL00616;
            spaconnection.timeout_x = 90000;
            WS_ProductandPricing.multipleProductAndPricingInformationSearchDataBean searchcriteria=new WS_ProductandPricing.multipleProductAndPricingInformationSearchDataBean();        
            WS_ProductandPricing.multipleProductAndPricingInformationResult Results;//results
            searchcriteria.endDate=clonedspa.EndDate__c;
            searchcriteria.legacyAccountId=clonedspa.BackOfficeCustomerNumber__c;
            searchcriteria.sdhLegacyPSName=clonedspa.BackOfficeSystemID__c;
            searchcriteria.legacyChannelId=clonedspa.ChannelLegacyNumber__c;
            searchcriteria.startDate=clonedspa.StartDate__c;    
            searchcriteria.productAndPricingLineList=new  List<WS_ProductandPricing.productAndPricingLineDataBean>();
            System.debug('>>>>>>'+currentlines.size());
            for(Integer i=0;i<currentlines.size();i++)
            {
                 SPARequestLineItem__c line= new SPARequestLineItem__c();
                  spaLines.add(line);
                  WS_ProductandPricing.productAndPricingLineDataBean tempRecord=new WS_ProductandPricing.productAndPricingLineDataBean();
                  tempRecord.catalogNumber=currentlines[i].CommercialReference__c;    
                  if(currentlines[i].RecordtypeId==System.Label.CLOCT14SLS52)
                           tempRecord.localPricingGroup=null;
                  else
                      tempRecord.localPricingGroup= currentlines[i].LocalPricingGroup__c;
                  tempRecord.requestedType=currentlines[i].requestedType__c;
                  tempRecord.requestedValue=currentlines[i].requestedValue__c;
                  tempRecord.quantity=Integer.valueOf(currentlines[i].TargetQuantity__c);
                  searchcriteria.productAndPricingLineList.add(tempRecord);
             }  
             System.debug('>>>>>>>>> searchcriteria'+searchcriteria);
             Results = spaconnection.getMultipleProductAndPricingInformation(searchcriteria);
             System.debug('>>>>>>>>> results'+Results);
             if(Results.returnInfo.returnCode=='S')
             {
             sp = Database.setSavepoint();
                if(clonedspa!=null)
                {
                    Database.saveResult sv = Database.insert(clonedspa,false); 
                    System.debug('#### New spa inserted: '+spa.id);
                    if(!sv.issuccess())
                     {
                        flag=true;
                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Cannot create SPA'));
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,sv.getErrors()[0].getMessage()));
                        return null;
                     }
               } 
               List<WS_ProductandPricing.productAndPricingInformationResult> resultList=Results.productAndPricingInfoList;
               for(Integer i=0;i<currentlines.size();i++)
                {
                   System.debug('resultList['+i+'].returnInfo.returnCode'+resultList[i].returnInfo.returnCode);
                   if(resultList[i].returnInfo.returnCode=='E'){
                             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,resultList[i].returnInfo.returnMessage));
                        flag=true;
                        database.rollback(sp);
                        return null;
                     }
                     else
                     {
                        spaLines[i].Business__c=currentlines[i].Business__c;
                        spaLines[i].CommercialReference__c=currentlines[i].CommercialReference__c;
                        spaLines[i].Compensation__c=currentlines[i].Compensation__c;
                        spaLines[i].SPARequest__c=clonedspa.Id;
                        spaLines[i].RecordtypeId=currentlines[i].RecordtypeId;
                        if(currentlines[i].RecordtypeId==System.Label.CLOCT14SLS52)
                            spaLines[i].LocalPricingGroup__c=null;
                        else{
                            spaLines[i].LocalPricingGroup__c=currentlines[i].LocalPricingGroup__c;
                            spaLines[i].ListPrice__c=currentlines[i].ListPrice__c;
                        }
                        spaLines[i].requestedType__c=currentlines[i].requestedType__c;
                        spaLines[i].requestedValue__c=currentlines[i].requestedValue__c;
                        spaLines[i].TargetQuantity__c=currentlines[i].TargetQuantity__c;
                        spaLines[i].CurrencyIsoCode=currentlines[i].CurrencyIsoCode;
                        System.debug('resultList['+i+'].productAndPricingInfo'+resultList[i].productAndPricingInfo);
                        if((resultList[i].productAndPricingInfo.catalogNumber+'')!='null' && resultList[i].productAndPricingInfo.localPricingGroup!=null && (resultList[i].productAndPricingInfo.localPricingGroup+'')!='null')
                             spaLines[i].LocalPricingGroup__c=String.valueOf(resultList[i].productAndPricingInfo.localPricingGroup);
                        if((resultList[i].productAndPricingInfo.catalogNumber+'')!='null' && resultList[i].productAndPricingInfo.standardPrice!=null && (resultList[i].productAndPricingInfo.standardPrice+'')!='null')
                            spaLines[i].StandardPrice__c=Double.valueOf(resultList[i].productAndPricingInfo.standardPrice+'');
                        if(resultList[i].productAndPricingInfo.standardDiscount!=null && (resultList[i].productAndPricingInfo.standardDiscount+'')!='null')
                           spaLines[i].StandardDiscount__c=Double.valueOf(resultList[i].productAndPricingInfo.standardDiscount+'');            
                        if(resultList[i].productAndPricingInfo.standardMulitplier!=null && (resultList[i].productAndPricingInfo.standardMulitplier+'')!='null')
                            spaLines[i].StandardMultiplier__c=Double.valueOf(resultList[i].productAndPricingInfo.standardMulitplier+'');
                        if((resultList[i].productAndPricingInfo.catalogNumber+'')!='null' && resultList[i].productAndPricingInfo.listPrice!=null)
                            spaLines[i].listPrice__c=Double.valueOf(resultList[i].productAndPricingInfo.listPrice);
                        if((resultList[i].productAndPricingInfo.catalogNumber+'')!='null' && (resultList[i].productAndPricingInfo.currencyCode+'')!='null' && resultList[i].productAndPricingInfo.currencyCode!=null)
                            spaLines[i].currencyIsoCode=resultList[i].productAndPricingInfo.currencyCode;            
                        if((resultList[i].productAndPricingInfo.compensation+'')!='null' && resultList[i].productAndPricingInfo.compensation!=null)
                            spaLines[i].Compensation__c=resultList[i].productAndPricingInfo.compensation;    
                        if((resultList[i].productAndPricingInfo.description+'')!='null' && resultList[i].productAndPricingInfo.description!=null)
                            spaLines[i].MaterialDescription__c=resultList[i].productAndPricingInfo.description;     
                        if(spaLines[i].requestedType__c==System.Label.CLOCT14SLS13 && spaLines[i].Compensation__c==null)
                            spaLines[i].Compensation__c=0; 
                        if(spaLines[i].RequestedType__c==System.Label.CLOCT14SLS11)
                           spaLines[i].requestedValue__c=100;          
                             
                         }
                    }
                     if(spaLines.size()>0)
                      {
                         try
                          {
                                Utils_Methods.insertRecords(spaLines);
                           }
                            catch(Exception e)
                            {
                                //Database.rollback(sp);
                                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getmessage()));
                                flag=true;
                                database.rollback(sp);
                                return null;
                            }
                        }
                        
                    }
                    else{
                        System.debug('#####ERROR#####');
                         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Results.returnInfo.returnMessage));
                         flag=true;
                          return null;
                  }
          }
          //If there are no SPA REquest Line Items
          else{
               System.debug('**** You have  NO line Items to clone *****');
              if(clonedspa!=null)
                {
                    Database.saveResult sv = Database.insert(clonedspa,false); 
                    System.debug('#### New spa inserted: '+spa.id);
                    if(!sv.issuccess())
                     {
                        flag=true;
                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Cannot create SPA'));
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,sv.getErrors()[0].getMessage()));
                        return null;
                     }
              }
          }
          return new pagereference('/'+clonedspa.id); 
             
    }
    
    public void populateBackofficeAccno()
    {  
        System.debug('#####Account Calculation#####'); 
        Id accId=null;
        if(spa.Account__c==null){
            Opportunity oppty=[select AccountId from Opportunity where id =:spa.Opportunity__c]; 
            accId=oppty.AccountId;
        }
        else
           accId=spa.Account__c; 
        System.debug('accId>>>>#####'+accId);
        if(accId!=null){                              
            for(Account acct:[select id,(select LegacyName__c,LegacyNumber__c,Account__c from Accounts_source_Legacy_ID__r) from Account where id =:accId]){        
                if(acct.Accounts_source_Legacy_ID__r.size()==1)
                {
                    if(acct.Accounts_source_Legacy_ID__r[0].LegacyName__c==spa.BackOfficeSystemId__c)
                       spa.BackOfficeCustomerNumber__c=acct.Accounts_source_Legacy_ID__r[0].LegacyNumber__c;                        
                    else if(spa.BackOfficeCustomerNumber__c==null || spa.BackOfficeCustomerNumber__c=='')
                       spa.addError(System.Label.CLSEP12SLS22);                           
                 }
                 else if(acct.Accounts_source_Legacy_ID__r.size()>1)
                 {
                     Integer iNumberOfLegacyAccounts = 0;
                     string strBackOfficeNumber = '';
                     for(LegacyAccount__c eachLegacyAccount:acct.Accounts_source_Legacy_ID__r)
                     {
                         If (eachLegacyAccount.LegacyName__c == spa.BackOfficeSystemID__c)
                          {
                              iNumberOfLegacyAccounts += 1;
                              strBackOfficeNumber = eachLegacyAccount.LegacyNumber__c;
                           }
                     }
                     if (iNumberOfLegacyAccounts == 1)
                     {
                          spa.BackOfficeCustomerNumber__c=strBackOfficeNumber;
                     }
                      else
                      {
                          spa.addError(System.Label.CLSEP12SLS21);
                       }
                    }
                        
                   else if(acct.Accounts_source_Legacy_ID__r.size()==0 && (spa.BackOfficeCustomerNumber__c==null || spa.BackOfficeCustomerNumber__c==''))
                            spa.addError(System.Label.CLSEP12SLS22);
              } 
          }           
    }
    
    public void populateChannelLegacyNumber()
    {   
        System.debug('#####ChannelCalculation#####');  
        for(Account acct:[select id,(select LegacyName__c,LegacyNumber__c,Account__c from Accounts_source_Legacy_ID__r) from Account where id=:spa.Channel__c]){        
            if(acct.Accounts_source_Legacy_ID__r.size()==1)
            {
                if(acct.Accounts_source_Legacy_ID__r[0].LegacyName__c==spa.BackOfficeSystemId__c)
                    spa.ChannelLegacyNumber__c=acct.Accounts_source_Legacy_ID__r[0].LegacyNumber__c; 
                else if(spa.ChannelLegacyNumber__c==null || spa.ChannelLegacyNumber__c=='')
                    spa.addError(System.Label.CLJUN13SLS03);                           
            }
            else if(acct.Accounts_source_Legacy_ID__r.size()>1)
            {
                Integer iNumberOfLegacyAccounts = 0;
                string strBackOfficeNumber = '';
                for(LegacyAccount__c eachLegacyAccount:acct.Accounts_source_Legacy_ID__r)
                {
                    If (eachLegacyAccount.LegacyName__c == spa.BackOfficeSystemID__c)
                    {
                        iNumberOfLegacyAccounts += 1;
                        strBackOfficeNumber = eachLegacyAccount.LegacyNumber__c;
                    }
                }
                if (iNumberOfLegacyAccounts == 1)
                {
                    spa.ChannelLegacyNumber__c=strBackOfficeNumber;
                } 
                else
                {
                    spa.addError(System.Label.CLJUN13SLS02);                  
                }
            }
            else if(acct.Accounts_source_Legacy_ID__r.size()==0 && (spa.ChannelLegacyNumber__c==null || spa.ChannelLegacyNumber__c==''))
                spa.addError(System.Label.CLJUN13SLS03);                         
        }

    }
   
}