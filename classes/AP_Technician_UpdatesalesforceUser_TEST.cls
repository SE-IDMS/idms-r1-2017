/*
    Author          : Deepak Kumar
    Date Created    : 25/09/2013
    Description     : Test class for AP_Technician_UpdatesalesforceUser class 
*/
@isTest(SeeAllData=true)
public class AP_Technician_UpdatesalesforceUser_TEST {
    static testMethod void testTechnician_UpdatesalesforceUser()
    {
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        
        User usr1 = Utils_TestMethods.createStandardUser('alias');
        insert usr1;
        
        
        User usr2 = Utils_TestMethods.createStandardUserWithManager('alias',usr1);
        usr2.FederationIdentifier = 'sesa250545';
        insert usr2;
        
        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm;
        RecordType[] rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c'];
        RecordType[] rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c'];  
          
         for(RecordType rt : rts) 
        {
            // Create Module Record
            if(rt.DeveloperName == 'Technician')
           {
                 st = new SVMXC__Service_Group__c(
                                            RecordTypeId =rt.Id,
                                            SVMXC__Active__c = true,
                                            Name = 'Test Service Team'                                                                                        
                                            );
                insert st;
                
            } 
        }
        for(RecordType rt : rtssg) //Loop to take a record type at a time
        {
            // Create Module Record
           if(rt.DeveloperName == 'Technican')
            {
                 sgm = new SVMXC__Service_Group_Members__c(
                                            RecordTypeId =rt.Id,
                                            SVMXC__Active__c = true,
                                            PrimaryAddress__c='Home',
                                            Name = 'Test user',
                                            SVMXC__Service_Group__c =st.id,
                                            SESAID__c =usr2.FederationIdentifier,
                                            SVMXC__Role__c = 'Schneider Employee',
                                            SVMXC__Salesforce_User__c = usr1.Id,                                            
                                            Manager__c = usr2.Id,
                                            SVMXC__Country__c='IN'
                                            );
              // insert sgm;
               
            } 
           
        }       
        
        
    
    }
}