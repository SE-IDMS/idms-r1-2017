/* 
Author:Siddharth Nagavarapu (sesa206167) 
purpose: Batch to update the product information 
*/ 

global class Batch_EnrichInstalledProduct implements Database.Batchable<sObject>,Schedulable { 

   global Batch_EnrichInstalledProduct(){ 

   } 

   global Database.QueryLocator start(Database.BatchableContext BC){   
      return Database.getQueryLocator([select id,TECH_CreateFromWS__c,SVMXC__Company__c,SVMXC__Product__c,Brand2__c,DeviceType2__c,Category__c,TECH_SDHCategoryId__c,TECH_SDHDEVICETYPEID__c,TECH_SDHBRANDID__c,TECH_CreatedfromSFM__c,SVMXC__Site__c,AutoCalcManufacturingDate__c,SVMXC__Serial_Lot_Number__c,ITBInstalled_Product__c,ManufacturingUnitDate__c,WarrantyTriggerDate__c from SVMXC__Installed_Product__c where TECH_CreateFromWS__c=true]); 
   } 

   global void execute(Database.BatchableContext BC, List<sObject> scope){     

    AP_InstalledProductManager.ProcessAssets(scope); 
	
	List<SVMXC__Installed_Product__c> WarrantyProcess = new List<SVMXC__Installed_Product__c>();
	
	for(Sobject sobjectRecord:scope){
		SVMXC__Installed_Product__c ip=(SVMXC__Installed_Product__c)sobjectRecord;
		if(ip.WarrantyTriggerDate__c  != null)
		{
			WarrantyProcess.add(ip);
		}
	}
	
    Database.update(scope,false); 
	if(WarrantyProcess != null && WarrantyProcess.size()>0){
		Ap_Installed_Product.ProductWarrantyCreation(WarrantyProcess);
	}
	
	
	
   }     
  
   global void execute(SchedulableContext sc) { 
    
   } 

   global void finish(Database.BatchableContext BC){ 
   } 
}