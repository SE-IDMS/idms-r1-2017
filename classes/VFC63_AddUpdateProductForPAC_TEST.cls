@isTest
private class VFC63_AddUpdateProductForPAC_TEST {

    static testMethod void VFC63_AddUpdateProductForPACTest() {
                    
        Profile profile = [select id from profile where name='System Administrator' LIMIT 1];
        User user = Utils_TestMethods.createStandardUser(profile.id, 'testpa');
        
        Profile profile1 = [select id from profile where name='System Administrator' LIMIT 1];    
        User user2 = Utils_TestMethods.createStandardUser(profile1.id,'TeUsep2');
        database.insert(user2);
            
        User user3 = Utils_TestMethods.createStandardUser(profile1.id,'TeUsep3');
        database.insert(user3);
        
        User user4 = Utils_TestMethods.createStandardUser(profile1.id,'TeUsep4');
        database.insert(user4);
        
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        contact1.Country__c= [Select Id, Name from Country__c Limit 1].Id;
        insert contact1;        
         
        OPP_Product__c prod =  Utils_TestMethods.createProduct('BD','BDPRD','OLD GDP',LABEL.CL00355);
        prod.name = 'test';
        insert prod;
                                 
            string [] searchfilterList= new string[]{};
            for(Integer token=0;token<17;token++){
                searchfilterList.add('a');          
            }  
            List<String> keywords = new List<String>();
            Utils_DummyDataForTest dummydata = new Utils_DummyDataForTest();
            Utils_DataSource.Result  tempresult = new Utils_DataSource.Result();
            tempresult = dummydata.Search(keywords,Keywords);
            
            sObject tempsObject = tempresult.recordList[0];
            //End of Test Data preparation.
    
            Test.startTest();
            
            PageReference pageRef= Page.VFP63_AddUpdateProductForPAC;             
            system.runAs(user)
            {             
            VFC_ControllerBase basecontroller = new VFC_ControllerBase();
    
            System.debug('~~~~~~~~~~Start of Test~~~~~~~~~~');
            ApexPages.StandardController cc1 = new ApexPages.StandardController(new PAMandCompetition__c());            
            VFC63_AddUpdateProductForPAC  ProductforProductLine = new VFC63_AddUpdateProductForPAC(cc1);                        
            VCC06_DisplaySearchResults componentcontroller1 =ProductforProductLine.resultsController;           
            VCC05_DisplaySearchFields componentcontroller = new VCC05_DisplaySearchFields();                         
            componentcontroller.pickListType = 'PM0_GMR';
            componentcontroller.init();   
            componentcontroller.SearchFilter = new String[4];
            componentcontroller.level1 = 'BD';
            componentcontroller.level2 = 'BDPRD';   
            componentcontroller.level3 = 'OLD GDP';
            componentcontroller.level4=label.cl00355; 
            componentcontroller.searchText = 'te';
            componentcontroller.key = 'searchComponent';
            componentcontroller.PageController = ProductforProductLine;             
            pageRef.getParameters().put('key','searchComponent');
            pageRef.getParameters().put(CS007_StaticValues__c.getvalues('CS007_10').Values__c,account1.Id);
            Test.setCurrentPageReference(pageRef);               
            ProductforProductLine.search();
            ProductforProductLine.clear();      
            ProductforProductLine.PerformAction(tempsObject, ProductforProductLine); 
            ProductforProductLine.clear(); 
            componentcontroller.searchText = '';
            componentcontroller.level1=label.cl00355;   
            ProductforProductLine.search();            
        }
        pageRef= null;
        system.runAs(user2)
        {
            ApexPages.StandardController cc2 = new ApexPages.StandardController(new PAMandCompetition__c());          
            VFC63_AddUpdateProductForPAC  ProductforProductLine = new VFC63_AddUpdateProductForPAC(cc2);  
            VCC06_DisplaySearchResults componentcontroller1 =ProductforProductLine.resultsController;           
            VCC05_DisplaySearchFields componentcontroller = new VCC05_DisplaySearchFields();                         
            componentcontroller.pickListType = 'PM0_GMR';
            componentcontroller.init();   
            componentcontroller.SearchFilter = new String[4];
            componentcontroller.level1 = 'BD';
            componentcontroller.level2 = 'BDPRD';   
            componentcontroller.level3 = 'OLD GDP';
            componentcontroller.level4=label.cl00355; 
            componentcontroller.searchText = 'te';
            componentcontroller.key = 'searchComponent';
            componentcontroller.PageController = ProductforProductLine;             
            pageRef= Page.VFP63_AddUpdateProductForPAC;  
            pageRef.getParameters().put('key','searchComponent');
            pageRef.getParameters().put(CS007_StaticValues__c.getvalues('CS007_10').Values__c,account1.Id);
            Test.setCurrentPageReference(pageRef);               
            ProductforProductLine.search();
            ProductforProductLine.clear();      
            ProductforProductLine.PerformAction(tempsObject, ProductforProductLine);    
            Test.stopTest();
        }
    }
}