@isTest
private class VFC_AcceptCCCAction_TEST {    
    static testMethod void testCCCActionAccept(){
        System.debug('#### VFC_AcceptCCCAction_TEST.testCCCActionAccept Started ####');

        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCaseForAction;
        
        CCCAction__c objAction = Utils_TestMethods.createCCCAction(objCaseForAction.Id);
        insert objAction;
        
       Pagereference pageRef = Page.VFP_AcceptCCCAction;
       Test.setCurrentPageReference(pageRef);

       // Add parameter to page URL
       //ApexPages.currentPage().getParameters().put('Id', objAction.id);

       VFC_AcceptCCCAction CCCActionAccept = new VFC_AcceptCCCAction(new ApexPages.StandardController(objAction));
       CCCActionAccept.acceptCurrentCCCAction();
       
       ApexPages.currentPage().getParameters().put('Id', objAction.id);
       CCCActionAccept = new VFC_AcceptCCCAction(new ApexPages.StandardController(objAction));
       CCCActionAccept.acceptCurrentCCCAction();
       
       System.debug('#### VFC_AcceptCCCAction_TEST.testCCCActionAccept Ended ####');
    }
}