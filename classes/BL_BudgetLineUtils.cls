/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 06/11/2012
    Description     : Utils class for the class BL_BudgetLineTriggers.
                      
                      
*/

public with sharing class BL_BudgetLineUtils
{

    public static void updateBudget(List<PRJ_BudgetLine__c> budgetLineListToProcessCONDITION1)
    {
        //Update the FY0, FY1, FY2, FY3 values for
        //1. IPO Internal Regions
        //2. IPO Internal App Services / Cust Exp
        //3. IPO Internal Technology Services
        //4. IPO Internal Non IPO IT Costs
        //5. IPO Internal Running IT Costs Impact (Only FY1 and FY2)
        //6. P & L Impact
        //7. Business Cashout CAPEX
        //8. Business Cashout OPEX OTC
        //9. Business Running Costs Impact
        //10. Business Internal
        //11. Business Internal charged back to IPO

        List<Budget__c> lstUpdateBudget = new List<Budget__c>();
        Map<Id,List<PRJ_BudgetLine__c>> budgetLinesMap = new Map<Id,List<PRJ_BudgetLine__c>>();
        Map<String,DMTCostItemFields__c> csMap = DMTCostItemFields__c.getAll();
        Map<Id,Budget__c> budgetMap = new Map<Id,Budget__c>();
        
        
        
        
        for(PRJ_BudgetLine__c budLine:budgetLineListToProcessCONDITION1)
        {
            List<PRJ_BudgetLine__c> lstBudgetLines = new List<PRJ_BudgetLine__c>();
            if(budgetLinesMap.containsKey(budLine.Budget__c))
            {
                lstBudgetLines = budgetLinesMap.get(budLine.Budget__c);              
            }
            lstBudgetLines.add(budLine);
            budgetLinesMap.put(budLine.Budget__c,lstBudgetLines);
        }
        //Modified By Ramakrishna For September Release
        for(Budget__c bud:[Select Id,FY0AppServicesCustExp__c,FY0BusinessCashoutCAPEX__c,FY0BusinessCashoutOPEXOTC__c,FY0BusinessInternal__c,FY0BusinessRunningCostsImpact__c,FY0BusInternalChargedbacktoIPO__c,FY0NonIPOITCostsInternal__c,FY0OperServices__c,FY0PLImpact__c,FY0Regions__c,FY1AppServicesCustExp__c,FY1BusinessCashoutCAPEX__c,FY1BusinessCashoutOPEXOTC__c,FY1BusinessInternal__c,FY1BusinessRunningCostsImpact__c,FY1BusInternalChargedbacktoIPO__c,FY1NonIPOITCostsInternal__c,FY1OperServices__c,FY1PLImpact__c,FY1Regions__c,FY1RunningITCostsImpact__c,FY2AppServicesCustExp__c,FY2BusinessCashoutCAPEX__c,FY2BusinessCashoutOPEXOTC__c,FY2BusinessInternal__c,FY2BusinessRunningCostsImpact__c,FY2BusInternalChargedbacktoIPO__c,FY2NonIPOITCostsInternal__c,FY2OperServices__c,FY2PLImpact__c,FY2Regions__c,FY2RunningITCostsImpact__c,FY3AppServicesCustExp__c,FY3BusinessCashoutCAPEX__c,FY3BusinessCashoutOPEXOTC__c,FY3BusinessInternal__c,FY3BusinessRunningCostsImpact__c,FY3BusInternalChargedbacktoIPO__c,FY3NonIPOITCostsInternal__c,FY3OperServices__c,FY3PLImpact__c,FY3Regions__c,FY3RunningITCostsImpact__c  FROM Budget__c where Id in :budgetLinesMap.keySet()])
        {
            if(!budgetMap.containsKey(bud.Id))
                budgetMap.put(bud.Id,bud);
        }

        for(Id budId:budgetLinesMap.keySet())
        {
            //List<PRJ_BudgetLine__c> lstBudgetLinesToProcess = budgetLinesMap.get(budId);
            Budget__c bud = budgetMap.get(budId);
            SObject sobjBud = (SObject)bud;
            for(PRJ_BudgetLine__c budLine:budgetLinesMap.get(budId))
            {                               
                for(DMTCostItemFields__c csInstance:csMap.values())
                {         
                    if(csInstance.Key__c == budLine.TECH_FY_Index__c + '-' + budLine.Type__c)
                    {
                        if(budLine.Amount__c != null && sobjBud!=null)
                            sobjBud.put(csInstance.APIName__c,budLine.Amount__c);
                    }
                }
            }
            
            lstUpdateBudget.add(bud);
            
        }
            
        if(lstUpdateBudget.size()>0)
        {
            try
            {
                Database.SaveResult[] results = Database.update(lstUpdateBudget,false);
                if (results != null)
                {
                    for (Database.SaveResult result : results) 
                    {
                        if (!result.isSuccess()) 
                        {
                            Database.Error[] errs = result.getErrors();
                            for(Database.Error err : errs)
                            System.debug(err.getStatusCode() + ' - ' + err.getMessage());
 
                        }
                    }
                }
            }
            catch(Exception exp)
            {
                System.debug(exp.getTypeName() + ' - ' + exp.getCause() + ': ' + exp.getMessage());
            }
        }           
            
            
        
    }
    
}