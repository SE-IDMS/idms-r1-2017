public  class AP_WorkOrderHandler {

    public static void fillInFooter(List<SVMXC__Service_Order__c> aWorkOrderList) {
        System.debug('### start of AP_WorkOrderHandler.fillInFooter');
        System.debug('### Work Order list: '+aWorkOrderList);
        System.debug('### Custom Setting (SIR / Country): ' + FS_ServiceInterventionReportCountry__c.getall().values());
        if (aWorkOrderList != null) {
            System.debug('### WorkOrder list is not null');
            String footer = null;
            for (SVMXC__Service_Order__c aWorkorder : aWorkOrderList) {
                System.debug('### currentWorkOrder: '+ aWorkOrder);
                System.debug('### Work Order Country: '+ aWorkorder.Tech_Workordercountry__c);
                if (aWorkOrderList != null && aWorkorder.Tech_Workordercountry__c != null && FS_ServiceInterventionReportCountry__c.getInstance(aWorkorder.Tech_Workordercountry__c) != null) {
                    footer = FS_ServiceInterventionReportCountry__c.getInstance(aWorkorder.Tech_Workordercountry__c).Service_Intervention_Report_Footer__c;
                    System.debug('### Updating the WO with footer: '+ footer);
                    aWorkorder.Tech_WOFooterPerCountry__c = footer;
                }else{
                    System.debug('### No setting found; Footer is empty');
                    aWorkorder.Tech_WOFooterPerCountry__c = null;
                }
            }
        }
     }

}