@IsTest
class AP15_ProductLineIntegrationMethods_TEST
{
    static testMethod void testAP15_ProductLineIntegrationMethods() 
    {
       Account Acc = Utils_TestMethods.createAccount();
       Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'Jean Dupont');
       Case Cse = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
       
       insert Acc;
       insert Ctct;
       insert Cse;
       
       DataTemplate__c GMRProduct = new DataTemplate__c();
       GMRProduct.Field1__c='Test1';
       GMRProduct.Field2__c='Test2';
       GMRProduct.Field3__c='Test3';
       GMRProduct.Field4__c='Test4';
       GMRProduct.Field5__c='Test5';
       GMRProduct.Field6__c='Test6';
       GMRProduct.Field7__c='Test7';
       GMRProduct.Field8__c='Test8';
       GMRProduct.Field9__c='Test9';
        
       List<DataTemplate__c> GMRProducts = new List<DataTemplate__c>();
        
       Opportunity Opp = Utils_TestMethods.createOpportunity(Acc.Id); 
       Opp.CurrencyIsoCode = 'EUR';
       insert Opp;
       OPP_ProductLine__c ProductLine = Utils_TestMethods.createProductLine(Opp.Id); 
       ProductLine.CurrencyIsoCode = 'EUR';
       insert ProductLine;
       
       Map<OPP_ProductLine__c,DataTemplate__c> ProductMap = new Map<OPP_ProductLine__c,DataTemplate__c>();
              
       ProductMap.put(ProductLine,GMRProduct);
       
       AP15_ProductLineIntegrationMethods.UpdatePL(ProductMap);
       
       Map<Opportunity,DataTemplate__c> ProductMap2 = new Map<Opportunity,DataTemplate__c>();
       ProductMap2.put(Opp,GMRProduct );
       
       AP15_ProductLineIntegrationMethods.InsertProductLine(ProductMap2);
  
    }
}