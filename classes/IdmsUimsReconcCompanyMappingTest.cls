@isTest
public class IdmsUimsReconcCompanyMappingTest {
     static testmethod void testmapIdmsCompanyUims(){
         test.starttest();
         IdmsUimsReconcCompanyMapping.mapIdmsCompanyUims(new user(IDMSCompanyHeadquarters__c=false));
         IdmsUimsReconcCompanyMapping.mapIdmsCompanyUims(new user(IDMSCompanyHeadquarters__c=true,IDMSMarketSubSegment__c='Test1'));
         test.stoptest();
     }
}