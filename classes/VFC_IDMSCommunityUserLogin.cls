/**
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  02/12/2016 
•   Description: class for i2p login process. 
**/

public class VFC_IDMSCommunityUserLogin{ 
    public String ErrorMsgCode {get;set;}
    public String password {get; set;}    
    public String username{get; set;}
    
    //method used for login process
    public pagereference login(){    
        ErrorMsgCode  = '';
        Pagereference pg = Site.login(username, password, null);                       
        if(Pg != null){
            pg.setredirect(true);        
        }else{
            ErrorMsgCode  ='auth.failed';
        }
        return pg;
    }
}