global with sharing class AP_PRMUserMigrationV2 {

    public static String MEMBER_PROPERTIES_STATUS_NOT_CHECKED ='Not Checked';
    public static String MEMBER_PROPERTIES_STATUS_CHECKED_AND_NO_MPA ='Checked and no MPA';
    public static String MEMBER_PROPERTIES_STATUS_CHECKED_AND_MPA ='Checked and MPA';
    public static String MEMBER_PROPERTIES_STATUS_CHECKED_AND_MPA_AND_OUTPUTS ='Checked and MPA and outputs';
    
    public static void createSingleErrorForPRMPartnerMigration(PRMPartnerMigration__c prmPartnerMigration,String text,Integer step){
        prmPartnerMigration.hasError__c=true;
        String formatText ='#ERROR# STEP #'+step+' DATE '+Datetime.now() + text;
        prmPartnerMigration.ErrorMessage__c=prmPartnerMigration.ErrorMessage__c!=null?formatText+'\r\n'+prmPartnerMigration.ErrorMessage__c:formatText;
        System.debug('#Error prmPartnerMigration:'+prmPartnerMigration +', text='+text);
    }
    // for creating User
    public static void createUser(Map<Id,PRMPartnerMigration__c>  prmPartnerMigrationByContactId) {
    	PRM_FieldChangeListener.enableForAccount=false;
	  	PRM_FieldChangeListener.enableForContact=false; 
        List<Id> contactIds = new List<Id>(prmPartnerMigrationByContactId.keySet());
        Savepoint sp = Database.setSavepoint();
        String suffixUserName = System.Label.CLMAR13PRM05;

        Map<String, PRM_UserMigration__c> myUserMigrationSettingMap = PRM_UserMigration__c.getAll();
        Map<String,Map<String,Object>> valueBySettingByCountryCode = new Map<String,Map<String,Object>>();
        Map<String,Schema.SObjectField> fieldsMap =Schema.getGlobalDescribe().get('PRM_UserMigration__c').getDescribe().fields.getMap();
            
        for(String countryCode:myUserMigrationSettingMap.keySet()){
            Map<String,Object> valueBySetting = new Map<String,Object>();
            PRM_UserMigration__c myUserMigrationSetting = myUserMigrationSettingMap.get(countryCode);
            for(Schema.SObjectField field : fieldsMap.values()) {
                DescribeFieldResult myDescribe = field.getDescribe();
                if(myDescribe.getName().endswith('__c')){
                    valueBySetting.put(myDescribe.getLabel(),myUserMigrationSetting.get(myDescribe.getName()));
                }
            }
            valueBySettingByCountryCode.put(countryCode,valueBySetting);
            System.debug('valueBySetting for '+countryCode+' is: '+valueBySetting );
        }
        System.debug('valueBySettingByCountryCode:'+valueBySettingByCountryCode );
        

        try{
        	Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
            List<User> userToInsert = new List<User>();
            for(Contact c : [select Id,FirstName,LastName,PRMEmail__c,PRMUIMSId__c,OwnerId,Account.Name,PRMCountry__r.CountryCode__c from contact where id in:contactIds and id not in (select contactId from User)]){
                if(!valueBySettingByCountryCode.containsKey(c.PRMCountry__r.CountryCode__c)){
                    createSingleErrorForPRMPartnerMigration(prmPartnerMigrationByContactId.get(c.Id),'ContactId='+c.Id+', CountryCode='+c.PRMCountry__r.CountryCode__c +' do not exist in the Custom Settings "PRM_UserMigration__c"',0);
                }else{
                    Map<String,Object> valueBySetting =valueBySettingByCountryCode.get(c.PRMCountry__r.CountryCode__c);
                    String alias =  (c.FirstName!=null && c.FirstName.length()>3)?c.FirstName.substring(0, 3):c.FirstName;
                    alias +=  (c.LastName!=null && c.LastName.length()>4)?c.LastName.substring(0, 4):c.LastName;
                    String nickName = c.FirstName+' '+c.LastName +' ('+c.Account.Name+')';
                    if(nickName.length()>37){
                        nickName = nickName.subString(0,37);
                    }
                    nickName += Math.round(Math.random()*1000);
                    system.debug('nickName:'+nickName );
                    User u = new User(UserPermissionsChatterAnswersUser=false,UserPreferencesContentEmailAsAndWhen=true,Country__c=c.PRMCountry__r.CountryCode__c,UserPreferencesContentNoEmail=true,FirstName = c.FirstName, FederationIdentifier=c.PRMUIMSId__c,LastName = c.LastName, Email = c.PRMEmail__c, Username = c.PRMEmail__c + suffixUserName,Alias =alias, CommunityNickname = nickName,ProfileId = ProfileFieloId, ContactId = c.Id,UserPermissionsMobileUser = false,PRMRegistrationCompleted__c=true);
                    for(String value:valueBySetting.keySet()){
                        u.put(value, valueBySetting.get(value));
                    }
                    userToInsert.add(u);
                }
            }
            
            
            List<User> userToUpdate = new List<User>();
            String soql;
            soql  ='select Id,Username,ContactId,Contact.PRMEmail__c,Contact.PRMUIMSId__c,Contact.PRMCountry__r.CountryCode__c,Name,UserPermissionsChatterAnswersUser,UserPreferencesContentEmailAsAndWhen,UserPreferencesContentNoEmail,ProfileId,PRMRegistrationCompleted__c from User where contactId in:contactIds and contactId!=null';
            for(User u : database.query(soql)){
                 if(!valueBySettingByCountryCode.containsKey(u.Contact.PRMCountry__r.CountryCode__c)){
                    createSingleErrorForPRMPartnerMigration(prmPartnerMigrationByContactId.get(u.ContactId),'ContactId='+u.ContactId+', CountryCode='+u.Contact.PRMCountry__r.CountryCode__c +' do not exist in the Custom Settings "PRM_UserMigration__c"',0);
                }else{
                    Map<String,Object> valueBySetting =valueBySettingByCountryCode.get(u.Contact.PRMCountry__r.CountryCode__c);
                    u.UserPermissionsChatterAnswersUser=false;
                    u.UserPreferencesContentEmailAsAndWhen=true;
                    u.UserPreferencesContentNoEmail=true;
                    u.ProfileId = ProfileFieloId;
                    u.IsActive = true;
                    u.PRMRegistrationCompleted__c = true;
                    u.FederationIdentifier=u.contact.PRMUIMSId__c;
                    for(String value:valueBySetting.keySet()){
                        u.put(value, valueBySetting.get(value));
                    }
                    userToUpdate.add(u);
                    System.debug(u);
                }
            }
            System.debug('userToInsert:'+userToInsert );
            Database.DMLOptions dlo = new Database.DMLOptions();
            dlo.EmailHeader.triggerUserEmail = false;
            if(userToInsert.size()>0){
                Database.SaveResult[] srList = Database.insert(userToInsert,dlo);
                for(Integer i=0;i<srList.size();i++) {
                    Id contactId = userToInsert[i].contactId;
                    PRMPartnerMigration__c prmPartnerMigration = prmPartnerMigrationByContactId.get(contactId);
                    if (srList[i].isSuccess()) {
                        prmPartnerMigration.UserId__c = srList[i].getId();
                    }
                    else {
                        // Operation failed, so get all errors                
                        String errorText ='';
                        for(Database.Error err : srList[i].getErrors()) {
                            errorText+='error insert  user'+err.getStatusCode() + ': ' + err.getMessage()+'\r\n';
                        }
                        createSingleErrorForPRMPartnerMigration(prmPartnerMigration,errorText,0);
                    }
                }
            }
            System.debug('userToUpdate:'+userToUpdate );
            if(userToUpdate.size()>0){
                Database.SaveResult[] srList = Database.update(userToUpdate,dlo);
                for(Integer i=0;i<srList.size();i++) {
                    Id contactId = userToUpdate[i].contactId;
                    PRMPartnerMigration__c prmPartnerMigration = prmPartnerMigrationByContactId.get(contactId);
                    if (srList[i].isSuccess()) {
                        prmPartnerMigration.UserId__c = srList[i].getId();
                    }
                    else {
                        // Operation failed, so get all errors                
                        String errorText ='';
                        for(Database.Error err : srList[i].getErrors()) {
                            errorText+='error insert  user'+err.getStatusCode() + ': ' + err.getMessage()+'\r\n';
                        }
                        createSingleErrorForPRMPartnerMigration(prmPartnerMigration,errorText,0);
                    }
                }
            }
            if(Test.isRunningTest()) throw new PRMUserMigrationException();
        }catch(Exception e){
            System.debug(e);
            Database.rollback(sp);
            throw new PRMUserMigrationException('Users are not created'+e.getMessage()+'\r\n'+e.getStackTraceString());
        }
        System.debug('End. prmPartnerMigrationByContactId:'+prmPartnerMigrationByContactId );
    }
    
    public static void updateErrorsInFuture(Map<Id,String> prmPartnerMigrationErrorMap){
    	List<PRMPartnerMigration__c> prmPartnerMigrationList = new List<PRMPartnerMigration__c>();
	    for(PRMPartnerMigration__c prmPartnerMigration: [SELECT Id,Contact__c,ErrorMessage__c,hasError__c,Hashtags__c,MemberId__c,UserId__c,PermissionSetAssingnId__c from PRMPartnerMigration__c where Contact__c in:prmPartnerMigrationErrorMap.keySet()]) {
	    	prmPartnerMigration.hasError__c=true;
	    	prmPartnerMigration.ErrorMessage__c =prmPartnerMigrationErrorMap.get(prmPartnerMigration.Contact__c);
	    	prmPartnerMigrationList.add(prmPartnerMigration);
	    }
	    update prmPartnerMigrationList;
    }

    //Create Permisssion Set
    public static void createPermissionSet(Map<Id,PRMPartnerMigration__c>  prmPartnerMigrationByContactId) {
    	PRM_FieldChangeListener.enableForAccount=false;
	  	PRM_FieldChangeListener.enableForContact=false; 
        List<Id> contactIds = new List<Id>(prmPartnerMigrationByContactId.keySet());
        List<Id> contactIdsHavingAlready = new List<Id>();
        List<Id> contactIdsToAssign = new List<Id>();
        Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
        PermissionSet r = [SELECT Id FROM PermissionSet WHERE Name = 'FieloPRM_SE' limit 1][0];
        Savepoint sp = Database.setSavepoint();
        try{
            for(PermissionSetAssignment pAssign : [select Id,Assignee.contactId from PermissionSetAssignment where PermissionSetId=:r.Id and  AssigneeId in (select Id from User where ProfileId=:ProfileFieloId and contactId in:contactIds)]){
                contactIdsHavingAlready.add(pAssign.Assignee.contactId);
                PRMPartnerMigration__c prmPartnerMigration = prmPartnerMigrationByContactId.get(pAssign.Assignee.contactId);
                prmPartnerMigration.PermissionSetAssingnId__c = pAssign.Id;
            }
            List <PermissionSetAssignment> PSAssignments = new list <PermissionSetAssignment>();
            for(User u : [select Id,ContactId,Name from User where ContactId in:contactIds and contactId not in:contactIdsHavingAlready ]){
                PSAssignments.add( new PermissionSetAssignment(AssigneeId = u.id,PermissionSetId = r.Id ) );
                contactIdsToAssign.add(u.contactId);
            }
            if(PSAssignments.size()>0){
                Database.SaveResult[] srList = Database.insert(PSAssignments,false);
                for(Integer i=0;i<srList.size();i++) {
                    Id contactId = contactIdsToAssign[i];
                    PRMPartnerMigration__c prmPartnerMigration = prmPartnerMigrationByContactId.get(contactId);
                    if (srList[i].isSuccess()) {
                        prmPartnerMigration.PermissionSetAssingnId__c = srList[i].getId();
                    }
                    else {
                        // Operation failed, so get all errors                
                        String errorText ='';
                        for(Database.Error err : srList[i].getErrors()) {
                            errorText+='error insert  user'+err.getStatusCode() + ': ' + err.getMessage()+'\r\n';
                        }
                        createSingleErrorForPRMPartnerMigration(prmPartnerMigration,errorText,2);
                    }
                }   
            }
            if(Test.isRunningTest()) throw new PRMUserMigrationException();
        }catch(Exception e){
            System.debug(e);
            Database.rollback(sp);
            throw new PRMUserMigrationException('Pset are not assigned'+e.getMessage()+'\r\n'+e.getStackTraceString());
        }
    }


    //CreateMember
    public static void createFieloMember(Map<Id,PRMPartnerMigration__c>  prmPartnerMigrationByContactId) {
    	PRM_FieldChangeListener.enableForAccount=false;
	  	PRM_FieldChangeListener.enableForContact=false; 
        List<Id> contactIds = new List<Id>(prmPartnerMigrationByContactId.keySet());
        Savepoint sp = Database.setSavepoint();
        Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
        
        try{

            Id programDefaultId = System.label.CLAPR15PRM149;
            Map<Id,String> errorsResult = FieloEE.ContactConvertion.convertContacts(contactIds,programDefaultId);
            System.debug('**** Fielo Result->'+errorsResult);
            
            for(Contact c : [select Id, FieloEE__Member__c from contact where id in:contactIds]){
                prmPartnerMigrationByContactId.get(c.Id).MemberId__c = c.FieloEE__Member__c;
            }
            if(errorsResult!=null){
                for(Id contactId:errorsResult.keySet()){
                	createSingleErrorForPRMPartnerMigration(prmPartnerMigrationByContactId.get(contactId),errorsResult.get(contactId),4);
                	if(prmPartnerMigrationByContactId.get(contactId).MemberId__c!=null){
                		prmPartnerMigrationByContactId.get(contactId).hasError__c=false;
                	}
                }
            }
            
            if(Test.isRunningTest()) throw new PRMUserMigrationException();
        }catch(Exception e){
            System.debug(e);
            Database.rollback(sp);
            throw new PRMUserMigrationException('Members are not created'+e.getMessage()+'\r\n'+e.getStackTraceString());
        }
    }

    //CreateMemberProperties
    public static void createMPA(Map<Id,PRMPartnerMigration__c>  prmPartnerMigrationByContactId) {
    	List<Id> contactIds = new List<Id>(prmPartnerMigrationByContactId.keySet());
    	PRM_FieldChangeListener.enableForAccount=false;
		PRM_FieldChangeListener.enableForContact=true; 
		Savepoint sp = Database.setSavepoint();
		try{
            PRM_FieldChangeListener.checkInternalPropertiesForContacts(contactIds);
    	 	for(MemberExternalProperties__c memberExternalProperty : [SELECT Id,Member__r.FieloEE__User__r.ContactId from MemberExternalProperties__c where DeletionFlag__c=false and ExternalPropertiesCatalog__r.RecordTypeId=:System.label.CLJUN16PRM057 and Member__c !=null and Member__r.FieloEE__User__r.ContactId IN: contactIds ]){
    	 		prmPartnerMigrationByContactId.get(memberExternalProperty.Member__r.FieloEE__User__r.ContactId).MemberPropertiesStatus__c =MEMBER_PROPERTIES_STATUS_CHECKED_AND_MPA;
        	}
		    for(PRMPartnerMigration__c prmPartnerMigration: prmPartnerMigrationByContactId.values()){
		    	if(MEMBER_PROPERTIES_STATUS_NOT_CHECKED.equals(prmPartnerMigration.MemberPropertiesStatus__c)){
		    		prmPartnerMigration.MemberPropertiesStatus__c = MEMBER_PROPERTIES_STATUS_CHECKED_AND_NO_MPA;
		    	}
		    }
            if(Test.isRunningTest()) throw new PRMUserMigrationException();
	    }catch(Exception e){
            System.debug(e);
            Database.rollback(sp);
            for(PRMPartnerMigration__c prmPartnerMigration: prmPartnerMigrationByContactId.values()){
            	prmPartnerMigration.MemberPropertiesStatus__c = MEMBER_PROPERTIES_STATUS_NOT_CHECKED;
            }
            throw new PRMUserMigrationException('MPA are not checked'+e.getMessage()+'\r\n'+e.getStackTraceString());
        }
    }

    public static void generateOutputForMPA(Map<Id,PRMPartnerMigration__c>  prmPartnerMigrationByContactId) {
    	Savepoint sp = Database.setSavepoint();
		List<Id> contactIds = new List<Id>(prmPartnerMigrationByContactId.keySet());
    	
		try{
    	 	Set<string> setFieldsMemberProp =  Schema.SObjectType.MemberExternalProperties__c.fields.getMap().keySet();
	        List<string> listFieldsMemberProp = new list<string>();
	        listFieldsMemberProp.addAll(setFieldsMemberProp);
	        String query = 'SELECT ' + String.join(listFieldsMemberProp, ',') + ' FROM MemberExternalProperties__c WHERE F_PRM_Status__c IN (\'TOPROCESS\') AND Member__r.FieloEE__User__r.ContactId in:contactIds ORDER BY CreatedDate';
	        FieloPRM_Batch_MemberPropProcess.process((List<MemberExternalProperties__c>)Database.query(query));
		    for(PRMPartnerMigration__c prmPartnerMigration: prmPartnerMigrationByContactId.values()){
		    	if(MEMBER_PROPERTIES_STATUS_CHECKED_AND_MPA.equals(prmPartnerMigration.MemberPropertiesStatus__c)){
		    		prmPartnerMigration.MemberPropertiesStatus__c = MEMBER_PROPERTIES_STATUS_CHECKED_AND_MPA_AND_OUTPUTS;
		    	}
		    }
		    if(Test.isRunningTest()) throw new PRMUserMigrationException();
	    }catch(Exception e){
            System.debug(e);
            Database.rollback(sp);
            for(PRMPartnerMigration__c prmPartnerMigration: prmPartnerMigrationByContactId.values()){
            	prmPartnerMigration.MemberPropertiesStatus__c = MEMBER_PROPERTIES_STATUS_CHECKED_AND_MPA;
            }
            throw new PRMUserMigrationException('MPA output are not generated'+e.getMessage()+'\r\n'+e.getStackTraceString());
        }
    }
    
    webservice static void uncheckHasError(String hashTag) {
    	String query = 'SELECT Id,Contact__c,ErrorMessage__c,hasError__c,Hashtags__c,MemberId__c,UserId__c,PermissionSetAssingnId__c from PRMPartnerMigration__c where hasError__c=true';
    	query +=hashTag!=null && hashTag!=''?' and (Hashtags__c like \'%'+hashTag+' %\' OR Hashtags__c like  \'%'+hashTag+'\')':'';
    	System.debug('query='+query);
    	List<PRMPartnerMigration__c> prmPartnerMigrationList = new List<PRMPartnerMigration__c>();
	    for(PRMPartnerMigration__c prmPartnerMigration: Database.query(query)) {
	    	prmPartnerMigration.hasError__c=false;
	    	prmPartnerMigrationList.add(prmPartnerMigration);
	    }
	    update prmPartnerMigrationList;
	}


	webservice static String runBatch(String hashTag) {

		List<AsyncApexJob> apexJobList = [select id,ApexClass.Name, Status, ExtendedStatus from AsyncApexJob where ApexClass.Name ='Batch_PRM_UserMigration' and status in ('Queued','Preparing','Processing')];    
        if(apexJobList.size()>0){
        	return 'Another job is already runninng: '+apexJobList[0].Id;
        }
		Batch_PRM_UserMigration batchPRMUserMigration = new Batch_PRM_UserMigration(true,true,true,true,true,hashTag,0,50,1); 
		Id jobId = Database.executebatch(batchPRMUserMigration);
		return 'Job is launched: '+jobId;
	}

    public class PRMUserMigrationException extends Exception{

    }
}