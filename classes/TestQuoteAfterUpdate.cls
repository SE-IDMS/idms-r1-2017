@isTest(seealldata=true)
public class TestQuoteAfterUpdate {
    static testMethod void test(){
        
        Set<String> zqActivationDateSet = new Set<String>(); 
        Set<String> zqProvisionDateSet = new Set<String>();    
        
        //Code before June 2016 Release  
        //Country__c cou = testObjectCreator.CreateCountry('Cou_1');

        //Country__c c = Utils_Testmethods.createCountry();
        //insert c;
        //End Code before June 2016 Release
        
        //New Code with June 2016 Release
        Country__c c = null;
        
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           c = countries[0];
        }
        else {
           c = new Country__c(Name='France', CountryCode__c='FR');
           insert c;
        }        
        //End Code with June 2016 Release
        
        Account acc = Utils_TestMethods.createAccount();
        acc.Country__c = c.Id;
        insert acc;
               
        Contact con = Utils_TestMethods.createContact(acc.Id, 'Contact');
        con.Country__c = c.Id;
        con.PRMUIMSID__c = 'Federated Id';
        insert con;

        Zuora__CustomerAccount__c ba = testObjectCreator.CreateBillingAccount('BillAcc_1', 'Con_1', acc.Id);
        ba.Zuora__Zuora_Id__c = '2c92c0f84b07957f014b11cbaac6685e';
        update ba;

        zqu__Quote__c zq = new zqu__Quote__c();
        zq.zqu__Account__c = acc.Id;
        zq.zqu__ZuoraAccountID__c = '2c92c0f84b07957f014b11cbaac6685e';
        zq.zqu__ZuoraSubscriptionID__c = '2c92c0f84b07957f014b2bbde6254c52';
        zq.zqu__Status__c = 'New';
        zq.zqu__ZuoraParentBillingAccountId__c = ba.Id;
        zq.Primary_User__c = con.Id;
        zq.zqu__InvoiceOwnerId__c = ba.Id;
        zq.zqu__InvoiceOwnerName__c = ba.Name;
        zq.zqu__SubscriptionType__c = 'Test';
        zq.zqu__InvoiceId__c = '123456';
        zq.zqu__GenerateInvoice__c = true;
        insert zq;

        zqu__ZProduct__c pr1 = new zqu__ZProduct__c(Name='Test Product', zqu__ZuoraId__c='1234', zqu__SKU__c='123', FilteringNeeded__c='N', Type__c='Primary', Z_ImmediateInvoice__c='Yes');
        zqu__ZProduct__c pr2 = new zqu__ZProduct__c(Name='Test Product', zqu__ZuoraId__c='5678', zqu__SKU__c='123', FilteringNeeded__c='N', Type__c='Standalone', Z_ImmediateInvoice__c='No');
        zqu__ZProduct__c pr3 = new zqu__ZProduct__c(Name='Test Product', zqu__ZuoraId__c='9012', zqu__SKU__c='123', FilteringNeeded__c='N', Type__c='Primary', Z_ImmediateInvoice__c='No');
 
        insert new List<zqu__ZProduct__c>{pr1, pr2, pr3};

        zqu__ProductRatePlan__c prp1 = new zqu__ProductRatePlan__c(Country_Rate_Plan__c='FR', DiscountProfile__c='123', zqu__ZuoraId__c='1234', zqu__ZProduct__c=pr1.Id);
        zqu__ProductRatePlan__c prp2 = new zqu__ProductRatePlan__c(Country_Rate_Plan__c='FR', DiscountProfile__c='123', zqu__ZuoraId__c='5678', zqu__ZProduct__c=pr2.Id);
        zqu__ProductRatePlan__c prp3 = new zqu__ProductRatePlan__c(Country_Rate_Plan__c='FR', DiscountProfile__c='123', zqu__ZuoraId__c='9012', zqu__ZProduct__c=pr3.Id);

        insert new List<zqu__ProductRatePlan__c>{prp1, prp2, prp3};

        zqu__QuoteAmendment__c qa = new zqu__QuoteAmendment__c(zqu__Quote__c=zq.Id);

        insert qa;

        zqu__QuoteRatePlan__c qrp1 = new zqu__QuoteRatePlan__c(zqu__Quote__c=zq.Id, zqu__ProductRatePlan__c=prp1.Id, zqu__QuoteAmendment__c=qa.Id);
        zqu__QuoteRatePlan__c qrp2 = new zqu__QuoteRatePlan__c(zqu__Quote__c=zq.Id, zqu__ProductRatePlan__c=prp2.Id, zqu__QuoteAmendment__c=qa.Id);
        zqu__QuoteRatePlan__c qrp3 = new zqu__QuoteRatePlan__c(zqu__Quote__c=zq.Id, zqu__ProductRatePlan__c=prp3.Id, zqu__QuoteAmendment__c=qa.Id);

        insert new List<zqu__QuoteRatePlan__c>{qrp1, qrp2, qrp3};

        zq.zqu__BillToContact__c = con.Id;
        zq.Z_ProvisionDate__c = date.today();
        zqActivationDateSet.add(zq.Id);
        zqProvisionDateSet.add(zq.Id);
        zq.zqu__Status__c = 'Sent to Z-Billing';

        try{
            update zq;
            zq.zqu__SoldToContact__c = con.Id;
            zq.zqu__Service_Activation_Date__c = date.today();
            zq.Trial__c = 'N';
            update zq; 
        }catch(Exception e){
        
        }
    }
}