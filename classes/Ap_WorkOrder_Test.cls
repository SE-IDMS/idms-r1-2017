@isTest(SeeAllData=true)
public class Ap_WorkOrder_Test{
    
    static testMethod void TestWrokOrder(){
        
            //try{
            
            Profile profile = [select id, name from profile where name = 'System Administrator' ];    
            //User creation
            User user2 = new User(alias = 'user2', email='user2' + '@Schneider-electric.com', 
            emailencodingkey='UTF-8', lastname='Testing2', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profile.Id,BypassVR__c = true, BypassWF__c = true,BypassTriggers__c = 'CaseAfterInsert;CaseBeforeInsert;AP54;AP_Contact_PartnerUserUpdate;SVMX21;SVMX05;AP_LocationManager;SVMX06;SVMX07;SVMX05;SVMX09',
            timezonesidkey='Europe/London', username='user2' + '@bridge-fo.com',UserBusinessUnit__c = 'EN');
            insert user2;
            
            
            User user = new User(alias = 'user', email='user' + '@Schneider-electric.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profile.Id,BypassVR__c = true, BypassWF__c = true,BypassTriggers__c = 'CaseAfterInsert;CaseBeforeInsert;AP54;AP_Contact_PartnerUserUpdate;SVMX21;SVMX05;AP_LocationManager;SVMX06;SVMX07;SVMX05;SRV04;SVMX09',
            timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',UserBusinessUnit__c = 'EN');
            insert user;
            
            
            
            system.runAs(user2){ 
            
            
                //Country__c testCountry = Utils_TestMethods.createCountry();
               // insert testCountry;
                Country__c testCountry = new Country__c (Name='test country',CountryCode__c='USA');
                insert testCountry;
            
                WorkOrderAssignmentRule__c woar = new WorkOrderAssignmentRule__c();
                woar.BusinessUnit__c ='IT';
                woar.Country__c = testCountry.Id;
                insert woar;
            
                Account acc = Utils_TestMethods.createAccount();
                acc.TECH_IsSVMXRecordPresent__c = true;
                acc.Country__c = testCountry.id;
                insert acc;
                
                SVMXC__Site__c site = new SVMXC__Site__c();
                site.Name = 'Test Location';
                site.SVMXC__Street__c  = 'Test Street';
                site.SVMXC__Account__c = acc.id;
                site.TimeZone__c = 'Test';
                site.PrimaryLocation__c = true;
                insert site;
                Role__c r = new Role__c();
                //r.name = 'Test Role';
                r.Role__c ='ROLE_27';
                r.Location__c = site.id;
                insert r;
                
                Contact testContact = New Contact(FirstName='Test',
                                                LastName='ContactName',
                                                AccountId=acc.Id,
                                                JobTitle__c='Z3',
                                                CorrespLang__c='EN',
                                                TECH_IsSVMXRecordPresentForContact__c = false,
                                                WorkPhone__c='1234567890'
                                               );
             
                // insert testContact;
                Case testCase = New Case(       //Status=StatusValue,
                                            AccountId=acc.Id,
                                            ContactId=testContact.Id,
                                            SupportCategory__c='6-General',
                                            Priority='Medium',
                                            Subject='Complaint',
                                            origin='Phone');
                 //insert testCase;
                
                String ServiceLineRT = System.Label.CLAPR15SRV02;
                String ServiceContRT = System.Label.CLAPR15SRV01;
                SVMXC__Service_Contract__c testContract0 = new SVMXC__Service_Contract__c();
                testContract0.SVMXC__Company__c = acc.Id;
                testContract0.Name = 'Test Contract0'; 
                testContract0.RecordTypeid =ServiceContRT;
                insert testContract0;
                SVMXC__Service_Contract__c testContract1 = new SVMXC__Service_Contract__c();
                testContract1.SVMXC__Company__c = acc.Id;
                testContract1.SVMXC__Start_Date__c = system.today();
                testContract1.ParentContract__c = testContract0.Id;
                testContract1.RecordTypeid =ServiceLineRT;
                insert testContract1 ; 
                SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
                ip1.SVMXC__Company__c = acc.id;
                ip1.Name = 'Test Intalled Product ';
                ip1.SVMXC__Status__c= 'new';
                ip1.GoldenAssetId__c = 'GoledenAssertId156467238142';
                ip1.BrandToCreate__c ='Test Brand';
                ip1.SVMXC__Site__c = site.id;
                insert ip1;
                set<Id> ipset = new set<Id>();
                ipset.add(ip1.Id);
                SVMXC__Service_Group__c st;
                SVMXC__Service_Group_Members__c sgm1;
                
                     Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__Service_Group__c; 
                    Map<String,Schema.RecordTypeInfo> PartOrderRecordTypeInfo = dSobjres.getRecordTypeInfosByName();
                    Id sg = PartOrderRecordTypeInfo.get('FSR').getRecordTypeId();
                    st = new SVMXC__Service_Group__c(
                                                RecordTypeId =sg,
                                                SVMXC__Active__c = true,
                                                Name = 'Test Service Team'                                                                                        
                                                );
                    insert st;
                    Schema.DescribeSObjectResult dSobjres2 = Schema.SObjectType.SVMXC__Service_Group_Members__c; 
                    Map<String,Schema.RecordTypeInfo> PartOrderRecordTypeInfo2 = dSobjres2.getRecordTypeInfosByName();
                    Id sgm = PartOrderRecordTypeInfo2.get('FSR').getRecordTypeId();
                    sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =sgm,SVMXC__Service_Group__c =st.id,
                                                                SVMXC__Role__c = 'Schneider Employee',
                                                                SVMXC__Salesforce_User__c = user.Id,
                                                                Send_Notification__c = 'Email',
                                                                SVMXC__Email__c = 'sgm1.test@company.com',
                                                                SVMXC__Phone__c = '12345'
                                                                );
                    insert sgm1;
                    SVMXC__Service_Order__c workOrder0= new SVMXC__Service_Order__c();
                    workOrder0.SVMXC__Problem_Description__c = 'BLALBLALAbbbb';
                    workOrder0.SVMXC__Company__c  =acc.Id;
                    //workOrder0.SVMXC__Contact__c  =testContact.Id;
                    insert workOrder0;
                    SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
                     workOrder.Work_Order_Category__c='On-site';                 
                     workOrder.SVMXC__Order_Type__c='Maintenance';
                     workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
                     workOrder.SVMXC__Order_Status__c = 'UnScheduled';
                     workOrder.CustomerRequestedDate__c = Date.today();
                     workOrder.Service_Business_Unit__c = 'IT';
                     workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
                     workOrder.SendAlertNotification__c = true;
                     //workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
                     workOrder.BackOfficeReference__c = '111111';
                     workOrder.Parent_Work_Order__c=workOrder0.id;
                     workOrder.Comments_to_Planner__c='testing';
                     //workOrder.SVMXC__Group_Member__c =Tech_Equip1.id;
                     workOrder.SVMXC__Group_Member__c = sgm1.Id;
                    // workOrder.CustomerTimeZone__c = 'Pacific/Kiritimati';
                     workOrder.SVMXC__Site__c = site.Id;
                     workOrder.SVMXC__Service_Contract__c = testContract1.Id;
                     workOrder.SVMXC__Company__c  =acc.Id;
                     //workOrder.SVMXC__Contact__c  =testContact.Id;
                    // workOrder.SVMXC__Component__c = ip1.id;
                    // workOrder.SVMXC__Case__c = testCase.Id;
                     insert workOrder;
                    SVMXC__RMA_Shipment_Order__c partsOrder = new SVMXC__RMA_Shipment_Order__c();
                     partsOrder.SVMXC__Company__c =acc.Id ;
                     partsOrder.SVMXC__Service_Order__c = workOrder.Id;
                     insert partsOrder;
                
                      List<SVMXC__Service_Order__c> wolist = New List<SVMXC__Service_Order__c>();
                     wolist.add(workOrder);
                     set<Id> setId =New Set<Id>();
                     setId.add(sgm1.Id);
                     set<Id> woId =New Set<Id>();
                     woId.add(workOrder.id);
                     set<Id> woaccId =New Set<Id>();
                     woaccId.add(workOrder.SVMXC__Company__c);
                    
                    // Ap_WorkOrder.WOScheduledTimeZone(wolist,setId);
                    // Ap_WorkOrder.WOPreferedTechnician(wolist);
                     
                     //Ap_WorkOrder.SerContFieldUpdate(wolist);
                     
                     //Ap_WorkOrder.WOAutoValidation(woId);
                     Test.startTest();
                     Ap_WorkOrder.Partsordersharing(woId);               
                     Ap_WorkOrder.IPCommissioning(ipset,woId);
                     //Ap_WorkOrder.LocationFromCase(woaccId,wolist);
                     try{
               
                             workOrder.SVMXC__Order_Status__c = 'Service Complete';
                                update workOrder;
                             workOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
                                 update workOrder;
                        }
                       catch(Exception e){
                            
                        }
                    Test.stopTest();
        
                 
            //}
            }
            //catch(exception ex){
                
           // }
        
    }
    static testMethod void TestWrokOrder2(){
        
            try{
            
            Profile profile = [select id, name from profile where name = 'System Administrator' ];    
            //User creation
            User user = new User(alias = 'user', email='user' + '@Schneider-electric.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profile.Id,BypassVR__c = true, BypassWF__c = true,BypassTriggers__c = 'CaseAfterInsert;CaseBeforeInsert;AP54;AP_Contact_PartnerUserUpdate;SVMX21;SVMX05;AP_LocationManager;SVMX06;SVMX07;SVMX05;SRV04;SVMX09',
            timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',UserBusinessUnit__c = 'EN');
            insert user;
            system.runAs(user){ 
                Account acc = Utils_TestMethods.createAccount();
                acc.TECH_IsSVMXRecordPresent__c = true;
                insert acc;
                SVMXC__Site__c site = new SVMXC__Site__c();
                site.Name = 'Test Location';
                site.SVMXC__Street__c  = 'Test Street';
                site.SVMXC__Account__c = acc.id;
                site.TimeZone__c = 'Test';
                site.PrimaryLocation__c = true;
                insert site;
                Role__c r = new Role__c();
                //r.name = 'Test Role';
                r.Role__c ='ROLE_27';
                r.Location__c = site.id;
                insert r;
                
                 Contact testContact = New Contact(FirstName='Test',
                                                LastName='ContactName',
                                                AccountId=acc.Id,
                                                JobTitle__c='Z3',
                                                CorrespLang__c='EN',
                                                TECH_IsSVMXRecordPresentForContact__c = true,
                                                WorkPhone__c='1234567890'
                                               );
             
                 insert testContact;
                  Case testCase = New Case(       //Status=StatusValue,
                                            AccountId=acc.Id,
                                            ContactId=testContact.Id,
                                            SupportCategory__c='6-General',
                                            Priority='Medium',
                                            Subject='Complaint',
                                            origin='Phone');
                 //insert testCase;
                
                String ServiceLineRT = System.Label.CLAPR15SRV02;
                String ServiceContRT = System.Label.CLAPR15SRV01;
                SVMXC__Service_Contract__c testContract0 = new SVMXC__Service_Contract__c();
                testContract0.SVMXC__Company__c = acc.Id;
                testContract0.Name = 'Test Contract0'; 
                testContract0.RecordTypeid =ServiceContRT;
                insert testContract0;
                SVMXC__Service_Contract__c testContract1 = new SVMXC__Service_Contract__c();
                testContract1.SVMXC__Company__c = acc.Id;
                testContract1.SVMXC__Start_Date__c = system.today();
                testContract1.ParentContract__c = testContract0.Id;
                testContract1.RecordTypeid =ServiceLineRT;
                insert testContract1 ; 
                SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
                ip1.SVMXC__Company__c = acc.id;
                ip1.Name = 'Test Intalled Product ';
                ip1.SVMXC__Status__c= 'new';
                ip1.GoldenAssetId__c = 'GoledenA13531ssertId1';
                ip1.BrandToCreate__c ='Test Brand';
                ip1.SVMXC__Site__c = site.id;
                insert ip1;
                set<Id> ipset = new set<Id>();
                ipset.add(ip1.Id);
                SVMXC__Service_Group__c st;
                SVMXC__Service_Group_Members__c sgm1;
                
                     Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__Service_Group__c; 
                    Map<String,Schema.RecordTypeInfo> PartOrderRecordTypeInfo = dSobjres.getRecordTypeInfosByName();
                    Id sg = PartOrderRecordTypeInfo.get('FSR').getRecordTypeId();
                    st = new SVMXC__Service_Group__c(
                                                RecordTypeId =sg,
                                                SVMXC__Active__c = true,
                                                Name = 'Test Service Team'                                                                                        
                                                );
                    insert st;
                    Schema.DescribeSObjectResult dSobjres2 = Schema.SObjectType.SVMXC__Service_Group_Members__c; 
                    Map<String,Schema.RecordTypeInfo> PartOrderRecordTypeInfo2 = dSobjres2.getRecordTypeInfosByName();
                    Id sgm = PartOrderRecordTypeInfo2.get('FSR').getRecordTypeId();
                    sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =sgm,SVMXC__Service_Group__c =st.id,
                                                                SVMXC__Role__c = 'Schneider Employee',
                                                                SVMXC__Salesforce_User__c = user.Id,
                                                                Send_Notification__c = 'Email',
                                                                SVMXC__Email__c = 'sgm1.test@company.com',
                                                                SVMXC__Phone__c = '12345'
                                                                );
                    insert sgm1;
                    SVMXC__Service_Order__c workOrder0= new SVMXC__Service_Order__c();
                    workOrder0.SVMXC__Problem_Description__c = 'BLALBLALAbbbb';
                    workOrder0.SVMXC__Company__c  =acc.Id;
                    workOrder0.SVMXC__Contact__c  =testContact.Id;
                    insert workOrder0;
                    SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
                     workOrder.Work_Order_Category__c='On-site';                 
                     workOrder.SVMXC__Order_Type__c='Maintenance';
                     workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
                     workOrder.SVMXC__Order_Status__c = 'UnScheduled';
                     workOrder.CustomerRequestedDate__c = Date.today();
                     workOrder.Service_Business_Unit__c = 'IT';
                     workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
                     workOrder.SendAlertNotification__c = true;
                    workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
                     workOrder.BackOfficeReference__c = '111111';
                     workOrder.Parent_Work_Order__c=workOrder0.id;
                     workOrder.Comments_to_Planner__c='testing';
                     //workOrder.SVMXC__Group_Member__c =Tech_Equip1.id;
                     workOrder.SVMXC__Group_Member__c = sgm1.Id;
                    workOrder.CustomerTimeZone__c = 'Pacific/Kiritimati';
                     workOrder.SVMXC__Site__c = site.Id;
                     workOrder.SVMXC__Service_Contract__c = testContract1.Id;
                     workOrder.SVMXC__Company__c  =acc.Id;
                     workOrder.SVMXC__Contact__c  =testContact.Id;
                    // workOrder.SVMXC__Component__c = ip1.id;
                    // workOrder.SVMXC__Case__c = testCase.Id;
                     insert workOrder;
                    SVMXC__RMA_Shipment_Order__c partsOrder = new SVMXC__RMA_Shipment_Order__c();
                     partsOrder.SVMXC__Company__c =acc.Id ;
                     partsOrder.SVMXC__Service_Order__c = workOrder.Id;
                     insert partsOrder;
                
                      List<SVMXC__Service_Order__c> wolist = New List<SVMXC__Service_Order__c>();
                     wolist.add(workOrder);
                     set<Id> setId =New Set<Id>();
                     setId.add(sgm1.Id);
                     set<Id> woId =New Set<Id>();
                     woId.add(workOrder.id);
                     set<Id> woaccId =New Set<Id>();
                     woaccId.add(workOrder.SVMXC__Company__c);
                     
                     Test.startTest();
                     Ap_WorkOrder.WOScheduledTimeZone(wolist,setId);
                     Ap_WorkOrder.WOPreferedTechnician(wolist);
                     
                     //Ap_WorkOrder.SerContFieldUpdate(wolist);
                     //Ap_WorkOrder.WOAutoValidation(woId);
                    
                    // Ap_WorkOrder.Partsordersharing(woId);               
                     //Ap_WorkOrder.IPCommissioning(ipset,woId);
                     Ap_WorkOrder.LocationFromCase(woaccId,wolist);
                     try{
               
                             workOrder.SVMXC__Order_Status__c = 'Service Complete';
                                update workOrder;
                             workOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
                                 update workOrder;
                        }
                       catch(Exception e){
                            
                        }
                    Test.stopTest();
        
                 
            }
            }
            catch(exception ex){
                
            }
        
    }
    static testMethod void TestWrokOrder3(){
        
            try{
            
            Profile profile = [select id, name from profile where name = 'System Administrator' ];    
            //User creation
            User user = new User(alias = 'user', email='user' + '@Schneider-electric.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = profile.Id,BypassVR__c = true, BypassWF__c = true,BypassTriggers__c = 'CaseAfterInsert;CaseBeforeInsert;AP54;AP_Contact_PartnerUserUpdate;SVMX21;SVMX05;AP_LocationManager;SVMX06;SVMX07;SVMX05;SRV04;SVMX09',
            timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',UserBusinessUnit__c = 'EN');
            insert user;
            system.runAs(user){ 
                Account acc = Utils_TestMethods.createAccount();
                acc.TECH_IsSVMXRecordPresent__c = true;
                insert acc;
                SVMXC__Site__c site = new SVMXC__Site__c();
                site.Name = 'Test Location';
                site.SVMXC__Street__c  = 'Test Street';
                site.SVMXC__Account__c = acc.id;
                site.TimeZone__c = 'Test';
                site.PrimaryLocation__c = true;
                insert site;
                Role__c r = new Role__c();
                //r.name = 'Test Role';
                r.Role__c ='ROLE_27';
                r.Location__c = site.id;
                insert r;
                
                 Contact testContact = New Contact(FirstName='Test',
                                                LastName='ContactName',
                                                AccountId=acc.Id,
                                                JobTitle__c='Z3',
                                                CorrespLang__c='EN',
                                                TECH_IsSVMXRecordPresentForContact__c = true,
                                                WorkPhone__c='1234567890'
                                               );
             
                 insert testContact;
                  Case testCase = New Case(       //Status=StatusValue,
                                            AccountId=acc.Id,
                                            ContactId=testContact.Id,
                                            SupportCategory__c='6-General',
                                            Priority='Medium',
                                            Subject='Complaint',
                                            origin='Phone');
                 //insert testCase;
                
                String ServiceLineRT = System.Label.CLAPR15SRV02;
                String ServiceContRT = System.Label.CLAPR15SRV01;
                SVMXC__Service_Contract__c testContract0 = new SVMXC__Service_Contract__c();
                testContract0.SVMXC__Company__c = acc.Id;
                testContract0.Name = 'Test Contract0'; 
                testContract0.RecordTypeid =ServiceContRT;
                insert testContract0;
                SVMXC__Service_Contract__c testContract1 = new SVMXC__Service_Contract__c();
                testContract1.SVMXC__Company__c = acc.Id;
                testContract1.SVMXC__Start_Date__c = system.today();
                testContract1.ParentContract__c = testContract0.Id;
                testContract1.RecordTypeid =ServiceLineRT;
                insert testContract1 ; 
                SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
                ip1.SVMXC__Company__c = acc.id;
                ip1.Name = 'Test Intalled Product ';
                ip1.SVMXC__Status__c= 'new';
                ip1.GoldenAssetId__c = '1353454656ssdfh';
                ip1.BrandToCreate__c ='Test Brand';
                ip1.SVMXC__Site__c = site.id;
                insert ip1;
                set<Id> ipset = new set<Id>();
                ipset.add(ip1.Id);
                SVMXC__Service_Group__c st;
                SVMXC__Service_Group_Members__c sgm1;
                
                     Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__Service_Group__c; 
                    Map<String,Schema.RecordTypeInfo> PartOrderRecordTypeInfo = dSobjres.getRecordTypeInfosByName();
                    Id sg = PartOrderRecordTypeInfo.get('FSR').getRecordTypeId();
                    st = new SVMXC__Service_Group__c(
                                                RecordTypeId =sg,
                                                SVMXC__Active__c = true,
                                                Name = 'Test Service Team'                                                                                        
                                                );
                    insert st;
                    Schema.DescribeSObjectResult dSobjres2 = Schema.SObjectType.SVMXC__Service_Group_Members__c; 
                    Map<String,Schema.RecordTypeInfo> PartOrderRecordTypeInfo2 = dSobjres2.getRecordTypeInfosByName();
                    Id sgm = PartOrderRecordTypeInfo2.get('FSR').getRecordTypeId();
                    sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =sgm,SVMXC__Service_Group__c =st.id,
                                                                SVMXC__Role__c = 'Schneider Employee',
                                                                SVMXC__Salesforce_User__c = user.Id,
                                                                Send_Notification__c = 'Email',
                                                                SVMXC__Email__c = 'sgm1.test@company.com',
                                                                SVMXC__Phone__c = '12345'
                                                                );
                    insert sgm1;
                    set<Id> techId = new set<Id>();
                    techId.add(sgm1.Id);
                    SVMXC__Service_Order__c workOrder0= new SVMXC__Service_Order__c();
                    workOrder0.SVMXC__Problem_Description__c = 'BLALBLALAbbbb';
                    workOrder0.SVMXC__Company__c  =acc.Id;
                    workOrder0.SVMXC__Contact__c  =testContact.Id;
                    insert workOrder0;
                    SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
                     workOrder.Work_Order_Category__c='On-site';                 
                     workOrder.SVMXC__Order_Type__c='Maintenance';
                     workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
                     workOrder.SVMXC__Order_Status__c = 'UnScheduled';
                     workOrder.CustomerRequestedDate__c = Date.today();
                     workOrder.Service_Business_Unit__c = 'IT';
                     workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
                     workOrder.SendAlertNotification__c = true;
                     //workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
                     workOrder.BackOfficeReference__c = '111111';
                     workOrder.Parent_Work_Order__c=workOrder0.id;
                     workOrder.Comments_to_Planner__c='testing';
                     //workOrder.SVMXC__Group_Member__c =Tech_Equip1.id;
                     workOrder.SVMXC__Group_Member__c = sgm1.Id;
                    // workOrder.CustomerTimeZone__c = 'Pacific/Kiritimati';
                     workOrder.SVMXC__Site__c = site.Id;
                     workOrder.SVMXC__Service_Contract__c = testContract1.Id;
                     workOrder.SVMXC__Company__c  =acc.Id;
                     workOrder.SVMXC__Contact__c  =testContact.Id;
                    // workOrder.SVMXC__Component__c = ip1.id;
                    // workOrder.SVMXC__Case__c = testCase.Id;
                     insert workOrder;
                    SVMXC__RMA_Shipment_Order__c partsOrder = new SVMXC__RMA_Shipment_Order__c();
                     partsOrder.SVMXC__Company__c =acc.Id ;
                     partsOrder.SVMXC__Service_Order__c = workOrder.Id;
                     insert partsOrder;
                
                    List<SVMXC__Service_Order__c> wolist = New List<SVMXC__Service_Order__c>();
                     wolist.add(workOrder);
                     set<Id> setId =New Set<Id>();
                     setId.add(sgm1.Id);
                     set<Id> woId =New Set<Id>();
                     woId.add(workOrder.id);
                     set<Id> woaccId =New Set<Id>();
                     woaccId.add(workOrder.SVMXC__Company__c);
                     
                     Test.startTest();
                     //Ap_WorkOrder.WOScheduledTimeZone(wolist,setId);
                    // Ap_WorkOrder.WOPreferedTechnician(wolist);
                     
                     Ap_WorkOrder.SerContFieldUpdate(wolist);
                     Ap_WorkOrder.WOAutoValidation(woId);
                    
                     Ap_WorkOrder.PrimaryFSRRemoving(woId);               
                     Ap_WorkOrder.PrimaryFSRAdding(woId);
                    // Ap_WorkOrder.LocationFromCase(woaccId,wolist);
                     try{
               
                             workOrder.SVMXC__Order_Status__c = 'Service Complete';
                                update workOrder;
                             workOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
                                 update workOrder;
                        }
                       catch(Exception e){
                            
                        }
                    Test.stopTest();
        
                 
            }
            }
            catch(exception ex){
                
            }
        
    }
    
    

}