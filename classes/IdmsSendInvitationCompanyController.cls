//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : Controller class of the VFP IdmsSendInvitationCompany
//
//*******************************************************************************************

public with sharing class IdmsSendInvitationCompanyController{
    //Class attributes
    public String email{get; set;}
    public String MobilePhone{get; set;}
    public String FirstName{get; set;}
    public String LastName{get; set;}
    public String IDMS_Email_opt_in{get; set;}
    public String Country{get; set;}
    public String Street{get; set;}
    public String City{get; set;}
    public String PostalCode{get; set;}
    public String State{get; set;}
    public String IDMS_County{get; set;}
    public String IDMS_POBox{get; set;}
    public String IDMS_AdditionalAddress{get; set;}
    public String CompanyName{get; set;}
    public String Company_Address1{get; set;}
    public String Company_City{get; set;}
    public String Company_Postal_Code{get; set;}
    public String Company_State{get; set;}
    public String CompanyPoBox{get; set;}
    public String Company_Country{get; set;}
    public String Company_Address2{get; set;}
    public String ClassLevel1{get; set;}
    public String ClassLevel2{get; set;}
    public String MarketSegment{get; set;}
    public String MarketSubSegmen{get; set;}
    public String Phone{get; set;}
    public String Job_Title{get; set;}
    public String Job_Function{get; set;}
    public String JobDescription{get; set;}
    public String CompanyMarketServed{get; set;}
    public String CompanyNbrEmployees{get; set;}
    public boolean CompanyHeadquarters{get; set;}
    public String AnnualRevenue{get; set;}
    public String TaxIdentificationNumber{get; set;}
    public String MiddleName{get; set;}
    public String Company_Website{get; set;}
    public String Salutation{get; set;}
    public String Department{get; set;}
    public String Suffix{get; set;}
    public String Fax{get; set;}
    public String password{get; set;}
    public String cpassword{get; set;}
    public String reg1Check{get; set;}
    public Boolean checked{get;set;}
    public String regCheckEmailOpt{get; set;}
    public Boolean smsIdentity{get;set;}
    public String userId{get;set;}
    public String redirectToApp{get;set;}
    public String mobCntryCode{get;set;}
    public String workCntryCode{get;set;}
    public User usr1{get;set;}
    public String companyCounty{get;set;}
    public string language{get; set;}
    public Boolean hideAllFields{get;set;}
    
    public String gridOut{get; set;}
    //added variables to implement logic
    public List<SelectOption> statusOptionsCountry{get;set;}
    public List<SelectOption> statusOptionsState{get;set;}
    public List<SelectOption> statusOptionsCompState{get;set;}
    public List<SelectOption> statusOptionsCompAdd{get;set;}
    public List<SelectOption> statusOptionsCompIam1{get;set;}
    public List<SelectOption> statusOptionsCompIam2{get;set;}
    public List<SelectOption> statusOptionsCompIS{get;set;}
    public List<SelectOption> statusOptionsCompISS{get;set;}
    public List<SelectOption> statusOptionsCompJT{get;set;}
    public List<SelectOption> statusOptionsCompJF{get;set;}
    public List<SelectOption> statusOptionsCompIIS{get;set;}
    public List<SelectOption> statusOptionsST{get;set;}
    
    //added variables for captcha
    public Boolean showCaptcha{get;set;}
    public Boolean enterRecaptcha=false;
    public Boolean enterRecaptchcorrect=false;
    private static String baseUrl = Label.CLJUN16IDMS14;
    String dummyAccount = Label.CLNOV16IDMS063;
    // The keys you get by signing up for reCAPTCHA for your domain
    private static String privateKey = Label.CLJUN16IDMS15;
    public String publicKey { 
        get { return Label.CLJUN16IDMS16; }
    }
    
    // Create properties for the non-VF component input fields generated
    // by the reCAPTCHA JavaScript.
    public String challenge { 
        get {
            return ApexPages.currentPage().getParameters().get(Label.CLJUN16IDMS27);
        }
    }
    public String response  { 
        get {
            return ApexPages.currentPage().getParameters().get(Label.CLJUN16IDMS28);
        }
    }
    // Whether the submission has passed reCAPTCHA validation or not
    public Boolean verified {get; private set;}
    
    //added variables to show pages
    public boolean showHome{get; set;}
    public boolean showWork{get; set;}
    public Boolean isPwdIncluded{get;set;}
    public Boolean iscmpny{get;set;}
    public Boolean isconfirm{get;set;}
    public Boolean isHomeConfirm{get;set;}
    
    public string appid=ApexPages.currentPage().getParameters().get('app');
    public string registrationSource;
    public string context;
    
    User u = [select ID, email, CompanyName, MobilePhone,FirstName, LastName, Country,Street, City, State,IDMS_County__c,IDMSCompanyPoBox__c, IDMS_POBox__c,IDMS_AdditionalAddress__c, 
              Company_Address1__c,Company_City__c,Company_Postal_Code__c,Company_State__c,Company_Country__c,Company_Address2__c, IDMSCompanyCounty__c, IDMSClassLevel1__c,
              IDMSClassLevel2__c, IDMSMarketSegment__c,IDMSMarketSubSegment__c,Phone,Job_Title__c,Job_Function__c,IDMSJobDescription__c, countrycode,
              IDMSCompanyMarketServed__c,IDMSTaxIdentificationNumber__c, IDMSMiddleName__c,Company_Website__c,IDMSSalutation__c, Department, statecode, 
              IDMSSuffix__c from User where Id=:UserInfo.getUserId() limit 1];
    
    //VFP Constructor
    public IdmsSendInvitationCompanyController(){
        
        usr1=new User();
        
        hideAllFields=true;
        
        User usr = [select Id, usertype from User where Id=:UserInfo.getUserId()];  
        // guest users should never be able to access this page
        if(usr.usertype.equalsignorecase('Guest')) {
            throw new NoAccessException();
        }
        //code for getting context and app details        
        IDMSApplicationMapping__c appMap;
        appMap = IDMSApplicationMapping__c.getInstance(appid);
        if(appMap!=null){
            context = (String)appMap.get('context__c');
            registrationSource = (String)appMap.get('AppName__c');
            //language=(String)appMap.get('Language__c');
        }
        regCheckEmailOpt=Label.CLJUN16IDMS07;
        if(context!=null &&(context.equalsIgnoreCase('home')||context.equalsIgnoreCase('@home'))){
            showHome=true;
            hideAllFields=false;
        }
        if(context!=null && (context.equalsIgnoreCase('work') || context.equalsIgnoreCase('@work'))){
            showWork=true;
            CompanyName = u.CompanyName;
            Company_Address1 = u.Company_Address1__c;
            Company_City = u.Company_City__c;
            Company_Postal_Code = u.Company_Postal_Code__c;
            usr1.state = u.Company_State__c;
            CompanyPoBox = u.IDMSCompanyPoBox__c;
            usr1.country = u.Company_Country__c;
            Company_Address2 = u.Company_Address2__c;
            usr1.IDMSClassLevel1__c = u.IDMSClassLevel1__c;  
            usr1.IDMSClassLevel2__c= u.IDMSClassLevel2__c; 
            usr1.IDMSMarketSegment__c = u.IDMSMarketSegment__c;
            usr1.IDMSMarketSubSegment__c = u.IDMSMarketSubSegment__c;
            if(string.isnotblank(u.Phone)){
                Phone = u.Phone.substring(3,u.phone.length());
                workCntryCode = u.Phone.substring(0,3);
            }
            Job_Title = u.Job_Title__c;
            Job_Function = u.Job_Function__c;
            JobDescription = u.IDMSJobDescription__c;
            CompanyMarketServed = usr1.IDMSCompanyMarketServed__c;
            companyCounty = u.IDMSCompanyCounty__c;
            TaxIdentificationNumber = u.IDMSTaxIdentificationNumber__c;
            MiddleName = u.IDMSMiddleName__c;
            Company_Website = u.Company_Website__c;
            Salutation = u.IDMSSalutation__c;
            Department = u.Department;
            Suffix = u.IDMSSuffix__c;
            
            hideAllFields=false;
        }
        
        //country picklist
        Schema.DescribeFieldResult statusFieldCountry = Schema.User.Countrycode.getDescribe();
        statusOptionsCountry=new list<SelectOption>();
        statusOptionsCountry.add(new SelectOption(Label.CLJUN16IDMS06,Label.CLJUN16IDMS05));
        for (Schema.Picklistentry picklistCountry : statusFieldCountry.getPicklistValues())
        {
            if(picklistCountry.getValue()==Label.CLJUN16IDMS06){
                continue;
            }
            else{
                statusOptionsCountry.add(new SelectOption(picklistCountry.getValue(),picklistCountry.getLabel()));
            }
        }
        
        //state picklist
        Schema.DescribeFieldResult statusFieldState = Schema.User.Statecode.getDescribe();
        statusOptionsState=new list<SelectOption>();
        for (Schema.Picklistentry picklistState : statusFieldState.getPicklistValues())
        {
            if(statusOptionsState.size()>=1000){
                continue;
            }  
            statusOptionsState.add(new SelectOption(picklistState.getValue(),picklistState.getLabel()));
            
        }
        
        // Salutation picklist
        Schema.DescribeFieldResult statusFieldST = Schema.User.IDMSSalutation__c.getDescribe();
        statusOptionsST=new list<SelectOption>();
        for (Schema.Picklistentry picklistST : statusFieldST.getPicklistValues())
        {
            if(statusOptionsST.size()>=1000){
                continue;
            }  
            statusOptionsST.add(new SelectOption(picklistST.getValue(),picklistST.getLabel()));
            
        }
        
        //captcha code to switch on or off
        this.verified = false;
        String toShowCaptcha=Label.CLJUL16IDMS6; //should get from custom label
        if(toShowCaptcha.equalsIgnoreCase('true')){
            showCaptcha=true;
        } 
        else{
            showCaptcha=false;
        }
        
        
        //code for terms condition mapping with app
        
        String termUrl;
        IdmsTermsAndConditionMapping__c termMap;
        termMap=IdmsTermsAndConditionMapping__c.getInstance(registrationSource);
        if(termMap!=null){
            termUrl= (String)termMap.get('url__c');
            
            if(termMap==null){
                termUrl=Label.CLJUN16IDMS118;
            }
        }else{
            termUrl=Label.CLJUN16IDMS118;
        }
        String url='<a href='+ termUrl + ' target=_blank>Terms & Conditions</a>';
        gridOut='color:white;background-color:green;background-image:none;;float:left;';
        reg1Check=Label.CLJUN16IDMS19+' '+ +url+' '+ Label.CLJUN16IDMS20;    
    }
    
    //complete captch code
    public PageReference verify(){
        enterRecaptcha=true;
        System.debug('reCAPTCHA verification attempt');
        // On first page load, form is empty, so no request to make yet
        if ( challenge == null || response == null ){ 
            System.debug('reCAPTCHA verification attempt with empty form');
            return null; 
        }
        HttpResponse r = makeRequest(baseUrl,
                                     'privatekey=' + privateKey + 
                                     '&remoteip='  + remoteHost + 
                                     '&challenge=' + challenge +
                                     '&response='  + response
                                    );
        if ( r!= null ){
            this.verified = (r.getBody().startsWithIgnoreCase('true'));
        }
        if(this.verified){
            // If they pass verification
            // Or simply return a PageReference to the "next" page
            enterRecaptchcorrect=true;
            return null;
        }else{
            // stay on page to re-try reCAPTCHA
            return null; 
        }
    }
    
    //Null redirection
    public PageReference reset() {
        return null; 
    }   
    
    /* Private helper methods */
    private static HttpResponse makeRequest(string url, string body){
        HttpResponse response = null;
        HttpRequest req = new HttpRequest();   
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody (body);
        try {
            Http http = new Http();
            response = http.send(req);
            System.debug('reCAPTCHA response: ' + response);
            System.debug('reCAPTCHA body: ' + response.getBody());
        } catch(System.Exception e) {
            System.debug('ERROR: ' + e);
        }
        return response;
    }   
    
    private String remoteHost { 
        get { 
            String ret = '127.0.0.1';
            // also could use x-original-remote-host 
            Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
            if (hdrs.get('x-original-remote-addr')!= null)
                ret =  hdrs.get('x-original-remote-addr');
            else if (hdrs.get('X-Salesforce-SIP')!= null)
                ret =  hdrs.get('X-Salesforce-SIP');
            return ret;
        }
    }
    
    //all the validation for @work before moving to company page 
    public boolean nextToCompany(){
        //validation for Email to check correct format
        if(!Pattern.matches(Label.CLJUN16IDMS30, email))
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS08)); 
            return null;
        }
        if(email!=null && email!=''){
            //validation check if email already exist
            List<User> existingContacts = [SELECT id, email FROM User WHERE email = :email];
            String username=email+Label.CLJUN16IDMS71;
            List<User> existingusername = [SELECT id, email FROM User WHERE username= :username];
            //check for UIMS table
            List<UIMS_User__c> existingUIMSEmail = [SELECT id FROM UIMS_User__c WHERE EMail__c = :email];
            if(existingContacts.size() > 0 || existingusername.size() > 0 || existingUIMSEmail.size()>0 )
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CLQ316IDMS114)); 
                return null;
            }   
        }
        
        //phone validation check
        if(( MobilePhone!='') &&  !Pattern.matches(Label.CLJUN16IDMS31, MobilePhone)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS13)); 
            return null;
        }
        if(( MobilePhone!='' &&  !Pattern.matches(Label.CLQ316IDMS113, mobCntryCode))){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CLQ316IDMS115)); 
            return null;
        }
        getCompanyPicklist();    
        iscmpny=true;
        showWork=false;
        return null;
    }
    
    //get all company picklist values
    public void getCompanyPicklist(){
        //picklist values of company fields
        usr1.countrycode=Country;
        //Company Address (State/Province) picklist
        Schema.DescribeFieldResult statusFieldCompState = Schema.User.Statecode.getDescribe();
        statusOptionsCompState=new list<SelectOption>();
        for (Schema.Picklistentry picklistCompState : statusFieldCompState.getPicklistValues())
        {
            if(statusOptionsCompState.size()>=1000){
                continue;
            } 
            statusOptionsCompState.add(new SelectOption(picklistCompState.getValue(),picklistCompState.getLabel()));
            
        }
        
        //Company Address (Country) picklist
        Schema.DescribeFieldResult statusFieldCompAdd = Schema.User.countrycode.getDescribe();
        statusOptionsCompAdd=new list<SelectOption>();
        statusOptionsCompAdd.add(new SelectOption(Label.CLJUN16IDMS06,Label.CLJUN16IDMS05));
        for (Schema.Picklistentry picklistCompAdd : statusFieldCompAdd.getPicklistValues())
        {
            if(picklistCompAdd.getValue()==Label.CLJUN16IDMS06){
                continue;
            }  
            statusOptionsCompAdd.add(new SelectOption(picklistCompAdd.getValue(),picklistCompAdd.getLabel()));            
        }
        
        //I am a 1 picklist
        Schema.DescribeFieldResult statusFieldCompIam1 = Schema.User.IDMSClassLevel1__c.getDescribe();
        statusOptionsCompIam1=new list<SelectOption>();
        for (Schema.Picklistentry picklistCompIam1 : statusFieldCompIam1.getPicklistValues())
        {
            statusOptionsCompIam1.add(new SelectOption(picklistCompIam1.getValue(),picklistCompIam1.getLabel()));
        }  
        
        
        //I am a 2 picklist
        Schema.DescribeFieldResult statusFieldCompIam2 = Schema.User.IDMSClassLevel2__c.getDescribe();
        statusOptionsCompIam2=new list<SelectOption>();
        for (Schema.Picklistentry picklistCompIam2 : statusFieldCompIam2.getPicklistValues())
        {         
            statusOptionsCompIam2.add(new SelectOption(picklistCompIam2.getValue(),picklistCompIam2.getLabel()));   
        } 
        //My Industry segment picklist
        Schema.DescribeFieldResult statusFieldCompIS = Schema.User.IDMSMarketSegment__c.getDescribe();
        statusOptionsCompIS=new list<SelectOption>();
        for (Schema.Picklistentry picklistCompIS : statusFieldCompIS.getPicklistValues())
        {            
            statusOptionsCompIS.add(new SelectOption(picklistCompIS.getValue(),picklistCompIS.getLabel()));    
        }  
        //My Industry sub-segment picklist
        Schema.DescribeFieldResult statusFieldCompISS = Schema.User.IDMSMarketSubSegment__c.getDescribe();
        statusOptionsCompISS=new list<SelectOption>();
        for (Schema.Picklistentry picklistCompISS : statusFieldCompISS.getPicklistValues())
        {
            statusOptionsCompISS.add(new SelectOption(picklistCompISS.getValue(),picklistCompISS.getLabel()));
        }  
        
        //Job Title picklist
        Schema.DescribeFieldResult statusFieldCompJT = Schema.User.Job_Title__c.getDescribe();
        statusOptionsCompJT=new list<SelectOption>();
        for (Schema.Picklistentry picklistCompJT : statusFieldCompJT.getPicklistValues())
        {
            statusOptionsCompJT.add(new SelectOption(picklistCompJT.getValue(),picklistCompJT.getLabel()));
        } 
        
        //Job Function picklist
        Schema.DescribeFieldResult statusFieldCompJF = Schema.User.Job_Function__c.getDescribe();
        statusOptionsCompJF=new list<SelectOption>();
        for (Schema.Picklistentry picklistCompJF : statusFieldCompJF.getPicklistValues())
        {
            statusOptionsCompJF.add(new SelectOption(picklistCompJF.getValue(),picklistCompJF.getLabel()));
        } 
        
        //Industries I serve multiselect picklist
        Schema.DescribeFieldResult statusFieldCompIIS = Schema.User.IDMSCompanyMarketServed__c.getDescribe();
        statusOptionsCompIIS=new list<SelectOption>();
        for (Schema.Picklistentry picklistCompIIS : statusFieldCompIIS.getPicklistValues())
        {            
            statusOptionsCompIIS.add(new SelectOption(picklistCompIIS.getValue(),picklistCompIIS.getLabel()));   
        } 
    }
    
    //Submit button on @Work invitation for completing invitation form
    public pageReference submit(){
        //captch call to verify
        if(showCaptcha==true){
            if (challenge!= null && (response!= null && response!='')) { 
                verify();
                if(enterRecaptchcorrect!=true){
                    ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLNOV16IDMS004);
                    ApexPages.addMessage(msgntChkd );
                    return null; 
                }
                else{
                    enterRecaptchcorrect=true;
                }
            }else{
                ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS32);
                ApexPages.addMessage(msgntChkd );
                return null;
            }
        }
        
        if(mobCntryCode!=''){
            mobCntryCode=mobCntryCode.replace('+', '00');
        }
        
        String languageUser=Label.CLJUN16IDMS09;
        String planguageUser=Label.CLJUN16IDMS10;
        String localeUser=Label.CLJUN16IDMS11;
        String timezoneUser=Label.CLJUN16IDMS22;
        String currencyUser=Label.CLJUN16IDMS04;
        try{
            IDMS_Send_Invitation__c objSendInvitation= new IDMS_Send_Invitation__c();
            objSendInvitation.Email__c=Email;
            objSendInvitation.Mobile_Phone__c= mobCntryCode+MobilePhone;
            objSendInvitation.FirstName__c=FirstName;
            objSendInvitation.LastName__c =LastName;
            objSendInvitation.Country__c =Country;
            objSendInvitation.Street__c=Street;
            objSendInvitation.City__c=City;
            objSendInvitation.PostalCode__c=PostalCode;
            objSendInvitation.State__c=State;
            objSendInvitation.IDMS_County__c=IDMS_County;
            objSendInvitation.IDMS_POBox__c=IDMS_POBox;
            objSendInvitation.IDMS_AdditionalAddress__c =IDMS_AdditionalAddress;
            objSendInvitation.CompanyName__c=CompanyName;
            objSendInvitation.Company_Address1__c=Company_Address1;
            objSendInvitation.Company_City__c=Company_City;
            objSendInvitation.Company_Postal_Code__c=Company_Postal_Code;
            objSendInvitation.Company_State__c=usr1.statecode;
            objSendInvitation.Company_Country__c=usr1.countrycode;
            objSendInvitation.IDMSCompanyPoBox__c=CompanyPoBox;
            objSendInvitation.Company_Address2__c=Company_Address2;
            objSendInvitation.IDMSClassLevel1__c =usr1.IDMSClassLevel1__c;    
            objSendInvitation.IDMSClassLevel2__c =usr1.IDMSClassLevel2__c;
            objSendInvitation.IDMSMarketSegment__c=usr1.IDMSMarketSegment__c;
            objSendInvitation.IDMSMarketSubSegment__c=usr1.IDMSMarketSubSegment__c;
            objSendInvitation.Phone__c=workCntryCode+Phone;
            objSendInvitation.Job_Title__c=Job_Title;
            objSendInvitation.Job_Function__c=Job_Function;
            objSendInvitation.IDMSJobDescription__c=JobDescription;
            objSendInvitation.IDMSCompanyMarketServed__c=usr1.IDMSCompanyMarketServed__c;
            objSendInvitation.IDMSCompanyCounty__c=companyCounty;
            objSendInvitation.IDMSTaxIdentificationNumber__c=TaxIdentificationNumber;
            objSendInvitation.IDMSMiddleName__c=MiddleName;
            objSendInvitation.Company_Website__c=Company_Website;
            objSendInvitation.IDMSSalutation__c=Salutation;
            objSendInvitation.Department__c=Department;
            objSendInvitation.IDMSSuffix__c=Suffix;
            objSendInvitation.IDMS_User_Context__c=context; 
            objSendInvitation.IDMS_Registration_Source__c=registrationSource;
            objSendInvitation.IDMS_PreferredLanguage__c=planguageUser;
            objSendInvitation.AppName__c = appid;
            objSendInvitation.IDMSToken__c = generateRandomString(20);
            objSendInvitation.IDMSSignatureGenerationDate__c = system.today();
            insert objSendInvitation;
            
            Contact invitedContact = new Contact();
            invitedContact.email = objSendInvitation.Email__c;
            invitedContact.accountId = [Select Id from Account where Name =: dummyAccount limit 1][0].Id; 
            invitedContact.firstName = objSendInvitation.FirstName__c;
            invitedContact.lastname = objSendInvitation.LastName__c;
            insert invitedContact;
            system.debug('invitedContact id: ' + invitedContact);
            sendemail(invitedContact,appid,objSendInvitation);
            String invitedContactJson = Json.serialize(invitedContact);
            IdmsSendInvitationController.deleteAsync(invitedContactJson);
            
            List<IDMS_Send_Invitation__c> sendInviteLst = [select ID, Email__c from IDMS_Send_Invitation__c where Id =:objSendInvitation.Id];            
            if(sendInviteLst.size()>0){
                iscmpny=false;
                showWork=false;
                isconfirm=true;
                isHomeConfirm=true;
                redirectToApp=Label.CLQ316IDMS325+appid;
            }else{
                
                ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error in sending invitation');
                ApexPages.addMessage(msgntChkd );
                return null;
            }
            return null; 
            
        }catch(Exception e){
            System.debug(e.getMessage());
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error in sending invitation, please try after some time.');
            ApexPages.addMessage(msgntChkd );
            return null;
        }     
    }
    
    //Delete asynchrounously invitation record
    @future
    public Static void deleteAsync(String contactJson){
        Contact cnt = (Contact)JSON.deserialize(contactJson,Contact.class);
        delete cnt;
    }
    
    //Send email to invited user
    public static void sendemail(Contact cnt,String appId, IDMS_Send_Invitation__c Invitation)
    {
        //code added to get url on the basis of application name START
        String url=IDMSEmailLinksApp.getEmailLinkApp(appId,3); 
        if(url==null){
            url=Label.CLQ316IDMS131;  
        }
        system.debug('url---'+url);  
        String token= Invitation.IDMSToken__c;
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String TemplateName='IDMS_Send_Invitation_Email';
        List<EmailTemplate> lstTemplate;
        lstTemplate = [select id,HtmlValue,Subject from emailtemplate where DeveloperName = :TemplateName];
        String subject = lstTemplate[0].Subject;
        String firstname=cnt.firstname; 
        String InvitationId = Invitation.Id;
        String AppName = Invitation.AppName__c;
        String emailLink = IDMSApplicationMapping__c.getValues(appName).EmailLinkInvitation__c;
        String rootUrl;
        if(emailLink != null){
            rootUrl = emailLink;
        }else{
            rootUrl = System.Label.CLQ316IDMS073;
        }
        Id TemplateId;
        if(lstTemplate  != null && lstTemplate .size() >0){
            TemplateId= lstTemplate[0].id;                
        }
        email.setTemplateId(TemplateId);                               
        String htmlBody = lstTemplate[0].HtmlValue;
        htmlBody = htmlBody.replace('{!firstname}',firstname); 
        htmlBody = htmlBody.replace('{!Token}',token);
        htmlBody = htmlBody.replace('{!InvitationId}',InvitationId); 
        htmlBody = htmlBody.replace('{!AppName}',AppName); 
        htmlBody = htmlBody.replace('{!rootUrl}',rootUrl); 
        
        email.setHtmlBody(htmlBody);
        email.setTargetObjectId(cnt.id);            
        email.saveAsActivity = false;
        email.setSubject(subject);  
        Messaging.SendEmailResult [] r = 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
        system.debug('---email---'+email);
    }
    
    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    } 
    //back from company to userdetails page
    public void backToWork(){
        iscmpny=false;
        showWork=true;
    }
    
    //Submit button on @home VFP for completing invitation form 
    public pageReference submitHome(){
        
        //validation for Email to check correct format
        if(email!=null && email!=''){
            if(!Pattern.matches(Label.CLJUN16IDMS30, email))
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS08)); 
                return null;
            }
            //validation check if email already exist
            List<User> existingContacts = [SELECT id, email FROM User WHERE email = :email];
            String username=email+Label.CLJUN16IDMS71;
            List<User> existingusername = [SELECT id, email FROM User WHERE username= :username];
            //check in uims table
            List<UIMS_User__c> existingUIMSEmail = [SELECT id FROM UIMS_User__c WHERE EMail__c = :email];
            if(existingContacts.size() > 0 || existingusername.size() > 0 || existingUIMSEmail.size() > 0  )
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CLQ316IDMS114)); 
                return null;
            }   
        }
        //phone validation check
        if(( MobilePhone!='') &&  !Pattern.matches(Label.CLJUN16IDMS31, MobilePhone)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS13)); 
            return null;
        }
        if(( MobilePhone!='' &&  !Pattern.matches(Label.CLQ316IDMS113, mobCntryCode))){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CLQ316IDMS115)); 
            return null;
        }
        
        //captch call to verify
        if(showCaptcha==true){
            if (challenge!= null && (response!= null && response!='')) { 
                verify();
                if(enterRecaptchcorrect!=true){
                    ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLNOV16IDMS004);
                    ApexPages.addMessage(msgntChkd );
                    return null; 
                }
                else{
                    enterRecaptchcorrect=true;
                }
            }
            else{
                ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS32);
                ApexPages.addMessage(msgntChkd );
                return null;
                
            }}
        
        String languageUser=Label.CLJUN16IDMS09;
        String planguageUser=Label.CLJUN16IDMS10;
        String localeUser=Label.CLJUN16IDMS11;
        String timezoneUser=Label.CLJUN16IDMS22;
        String currencyUser=Label.CLJUN16IDMS04;
        try{
            IDMS_Send_Invitation__c objSendInvitation= new IDMS_Send_Invitation__c();
            objSendInvitation.Email__c=Email;
            objSendInvitation.Mobile_Phone__c=mobCntryCode+MobilePhone;
            objSendInvitation.FirstName__c=FirstName;
            objSendInvitation.LastName__c =LastName;
            objSendInvitation.Country__c =Country;
            objSendInvitation.IDMS_User_Context__c=context; 
            objSendInvitation.IDMS_Registration_Source__c=registrationSource;
            objSendInvitation.IDMS_PreferredLanguage__c=planguageUser;
            objSendInvitation.AppName__c = appid;
            objSendInvitation.IDMSToken__c = generateRandomString(20);
            objSendInvitation.IDMSSignatureGenerationDate__c = system.today();
            insert objSendInvitation; 
            
            Contact invitedContact = new Contact();
            invitedContact.email = objSendInvitation.Email__c;
            invitedContact.accountId = [Select Id from Account where Name =: dummyAccount limit 1][0].Id; 
            invitedContact.firstName = objSendInvitation.FirstName__c;
            invitedContact.lastname = objSendInvitation.LastName__c;
            insert invitedContact;
            system.debug('invitedContact id: ' + invitedContact);
            sendemail(invitedContact,appid,objSendInvitation);
            String invitedContactJson = Json.serialize(invitedContact);
            IdmsSendInvitationController.deleteAsync(invitedContactJson);
            
            List<IDMS_Send_Invitation__c> sendInviteLst = [select ID, Email__c from IDMS_Send_Invitation__c where Id =:objSendInvitation.Id];
            if(sendInviteLst.size()>0){
                showHome=false;
                isconfirm=true;
                redirectToApp=Label.CLQ316IDMS325+appid;
            }
            else{
                
                ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error in sending invitation');
                ApexPages.addMessage(msgntChkd );
                return null;
            }
            return null; 
        }catch(Exception e){
            System.debug(e.getMessage());
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error in Sending invitation, please try after some time.');
            ApexPages.addMessage(msgntChkd );
            return null;
        }     
    }
}