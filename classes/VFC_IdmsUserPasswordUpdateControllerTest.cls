/*
* Class containing tests for VFC_IdmsUserPasswordUpdateController
*/
@isTest
public with sharing class VFC_IdmsUserPasswordUpdateControllerTest{
    
    //test method for user password update controller when old password is blank
    public static testMethod void testMethod1() {
        PageReference mypage= Page.UserPasswordReset;
        Test.setCurrentPage(mypage);
        String appUrl='adasd.com';
        ApexPages.currentpage().getparameters().put('App' ,'test');
        ApexPages.currentpage().getparameters().put('token' ,'1234');
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        string authToken=ApexPages.currentPage().getParameters().get('token');
        string registrationSource=ApexPages.currentPage().getParameters().get('App');
        
        VFC_IdmsUserPasswordUpdateController testUser= new VFC_IdmsUserPasswordUpdateController();
        //if old password is blank
        testUser.oldPassword = '';
        testUser.testStringvar='test';
        testUser.pwdresetSuccess=true;
    }
    //test method when old old password is correct but new password is not correct
    public static testMethod void testMethod2() {
        PageReference mypage= Page.UserPasswordReset;
        Test.setCurrentPage(mypage);
        String appUrl='adasd.com';
        ApexPages.currentpage().getparameters().put('App' ,'test');
        ApexPages.currentpage().getparameters().put('token' ,'1234');
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        string authToken=ApexPages.currentPage().getParameters().get('token');
        string registrationSource=ApexPages.currentPage().getParameters().get('App');
        
        VFC_IdmsUserPasswordUpdateController testUser= new VFC_IdmsUserPasswordUpdateController();
        
        //if new password does not match to password policy
        testUser.testStringvar='test';
        testUser.pwdresetSuccess=true;
        testUser.oldPassword = 'abc@123abc';
        testUser.newPassword = 'xyz';
        
        testUser.setPassword();
        testUser.usersetPassword();
    }
    // test method with multiple test cases
    public static testMethod void testMethod3() {
        PageReference mypage= Page.UserPasswordReset;
        Test.setCurrentPage(mypage);
        String appUrl='adasd.com';
        ApexPages.currentpage().getparameters().put('App' ,'test');
        ApexPages.currentpage().getparameters().put('token' ,'1234');
        IdmsAppMapping__c appMap= new IdmsAppMapping__c();
        appMap.name = 'test';
        appMap.AppURL__c= 'test';
        insert appMap;
        
        string authToken=ApexPages.currentPage().getParameters().get('token');
        string registrationSource=ApexPages.currentPage().getParameters().get('App');
        
        VFC_IdmsUserPasswordUpdateController testUser= new VFC_IdmsUserPasswordUpdateController();
        testUser.testStringvar='test';
        testUser.pwdresetSuccess=true;
        testUser.oldPassword = 'abc@123abc';
        //new password and confirm new password are not same
        testUser.newPassword = 'Welcome123';
        testUser.verifyNewPassword ='welcome';
        
        testUser.setPassword();
        testUser.usersetPassword();
        //Success case
        testUser.verifyNewPassword ='Welcome123';
        
        testUser.setPassword();
        testUser.usersetPassword();
        testUser.myemail='1234567890';
        testUser.setpwdFlag=true;
        testUser.resetpwdFlag=true;
        
        
        
        //reset password case
        testUser.myemail='';
        testUser.resetPassword();
        testUser.myemail='testuser@accenture.com';
        testUser.resetPassword();
        
        testUser.myemail='test123@accenture.com';
        testUser.resetPassword();
        
        testUser.resetPasswordMethod('1234');
        testUser.myemail='null';
        testUser.resetPasswordMethod('1234');
        testUser.myemail='test0123@accenture.com';
        testUser.newPassword = 'Welcome123';
        testUser.verifyNewPassword ='Welcome123';
        testUser.testclassvar=true;
        testUser.resetPassword();
        
        testUser.setPassword();
        testUser.usersetPassword();
        testUser.testclassvar=false;
        testUser.resetPassword();
        testUser.usersetPassword();
        testUser.setPassword();
        testUser.testStringvar=null;
        testUser.usersetPassword();
        
    }
}