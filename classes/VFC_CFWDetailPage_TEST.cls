/*
    Author          : Bhuvana Subramaniyan
    Description     : Test class for VFC_CFWDetailPage.
*/


@isTest
private class VFC_CFWDetailPage_TEST 
{
    static testMethod void testCFWDetailPage()
    {
        test.startTest();
        
        CFWRiskAssessment__c cfwr = new CFWRiskAssessment__c();
        cfwr.ProjectName__c = 'testProjectName';
        cfwr.SubmittedforApproval__c = true;
        insert cfwr;
        PageReference pageRef = Page.VFP_CFWDetailPage;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(cfwr);
        VFC_CFWDetailPage detailPage= new  VFC_CFWDetailPage(sc1);
        detailPage.SubmitRisk();
        
        ApexPages.currentPage().getParameters().put('RecordType',Label.CLOCT15CFW01);
        detailPage= new  VFC_CFWDetailPage(sc1);
        detailPage.EditPage();
        detailPage.SubmitRisk();
        detailPage.SaveAppView();
        detailPage.CancelPage();
        
        ApexPages.currentPage().getParameters().put('RecordType',Label.CLOCT15CFW02);
        detailPage= new  VFC_CFWDetailPage(sc1);

        ApexPages.currentPage().getParameters().put('RecordType',Label.CLOCT15CFW03);
        detailPage= new  VFC_CFWDetailPage(sc1);
        
        ApexPages.currentPage().getParameters().put('RecordType',Label.CLOCT15CFW23);
        detailPage= new  VFC_CFWDetailPage(sc1);
        detailPage.EditPage();
        
        ApexPages.currentPage().getParameters().put('RecordType',Label.CLOCT15CFW26);
        detailPage= new  VFC_CFWDetailPage(sc1);
        
        ApexPages.currentPage().getParameters().put('RecordType',Label.CLOCT15CFW27);
        detailPage= new  VFC_CFWDetailPage(sc1);
        test.stopTest();
    }
}