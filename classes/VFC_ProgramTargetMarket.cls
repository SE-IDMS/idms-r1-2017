public class VFC_ProgramTargetMarket
{

    public list<PartnerProgramMarket__c> lstMarketSegment {get;set;}
    public VFC_ProgramTargetMarket(ApexPages.StandardController controller) {
        lstMarketSegment = [Select MarketSegmentCatalog__r.MarketSegmentName__c from PartnerProgramMarket__c where PartnerProgram__c=:controller.getId() and MarketSegmentCatalog__r.parentmarketsegment__c = null order by MarketSegmentCatalog__r.MarketSegmentName__c limit 1000]; 
        
    }

    
}