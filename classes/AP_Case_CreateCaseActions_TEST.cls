@isTest
private class AP_Case_CreateCaseActions_TEST{
    public static testMethod void testCreateCCCActions(){
        System.debug('#### AP_Case_CreateCaseActions_Test.testCreateCCCActions Started ####');
        
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        List<Case> lstCase = new List<Case>();
        List<Case> lstCaseUpdate = new List<Case>();
        Case objCaseActionInternal = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open/ Waiting For Details - Internal');
        Case objCaseActionCustomer = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open/ Waiting For Details - Customer');
        
        Case objCaseActionInternal1 = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        Case objCaseActionCustomer1 = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        
        
        lstCase.add(objCaseActionInternal);
        lstCase.add(objCaseActionInternal1);
        lstCase.add(objCaseActionCustomer);
        lstCase.add(objCaseActionCustomer1);
        
        Database.SaveResult[] dbInsertResult = Database.Insert(lstCase, false);
        /*
        objCaseActionInternal1.Status ='Open/ Waiting For Details - Internal';
        objCaseActionCustomer1.Status ='Open/ Waiting For Details - Customer';
        
        lstCaseUpdate.add(objCaseActionInternal1);
        lstCaseUpdate.add(objCaseActionCustomer1);
        */
        CCCAction__c objAction = Utils_TestMethods.createAction(objCaseActionInternal1.Id,UserInfo.getUserId());
        
        Database.SaveResult dbCCActionInsertResult = Database.insert(objAction, false);
        
        System.debug('#### AP_Case_CreateCaseActions_Test.testCreateCCCActions Ends ####');
    }
}