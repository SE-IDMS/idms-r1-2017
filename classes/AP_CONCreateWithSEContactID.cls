public with sharing class AP_CONCreateWithSEContactID
{
    /*
        >> BR-2591. MAY 2013 Account / Contact Release <<
        - This function will restrict user from creating a new Contact with existing SE Contact Id.       
    */
    public static void checkSEContactID(List<Contact> lstCon)
    {
        Set<String> setSEContactIds=new Set<String>();      
        Set<String> setSEConIds=new Set<String>();
        List<String> lstSEConIds=new List<String>();
        for(Contact conToInsert : lstCon)
        {
            if(conToInsert.SEContactID__c != null && conToInsert.SEContactID__c != '' && conToInsert.SEContactID__c !=' ')
            {
                setSEContactIds.add(conToInsert.SEContactID__c);
                lstSEConIds.add(conToInsert.SEContactID__c);
            }
        }
        
        String qry= 'select id,name,SEContactID__c from Contact where SEContactID__c IN :lstSEConIds limit '+ Limits.getLimitQueryRows();

        for(Contact conExistsinSFDC : Database.query(qry))
        {           
            if(setSEContactIds.contains(conExistsinSFDC.SEContactID__c))
                setSEConIds.add(conExistsinSFDC.SEContactID__c);            
        }
        for(Contact conToInsert : lstCon)
        {
            if(setSEConIds.contains(conToInsert.SEContactID__c))
                conToInsert.addError(Label.CLMAY13ACC03);
        }

    }
}