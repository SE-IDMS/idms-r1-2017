public class IdmsUimsReconcUpdateUserBulk {
    static IDMSCaptureError errorLog=new IDMSCaptureError();
    public static void updateUimsUsers(List<IDMSUserReconciliationBatchHandler__c> recUsersToUpdate, String batchId){
        
        //retrieve and add idmsuser ids in set.
        Set<string> idmsUids = new Set<String>();
        Integer attemptsMaxLmt = integer.valueof(label.CLNOV16IDMS022);
        for(IDMSUserReconciliationBatchHandler__c userHandler : recUsersToUpdate){
            idmsUids.add(userHandler.IdmsUser__c);
        }
        //retrieve list of idms users
        List<User> idmsUsers = [Select Id, Email,MobilePhone,Phone,FirstName,LastName,IDMS_Email_opt_in__c,IDMS_User_Context__c,IDMSMiddleName__c,
                                Country,IDMS_PreferredLanguage__c,DefaultCurrencyIsoCode, Street,City,PostalCode,State,IDMS_County__c,Job_Title__c,
                                IDMS_POBox__c,FederationIdentifier,IDMS_Registration_Source__c,IDMS_AdditionalAddress__c,IDMSMiddleNameECS__c,Job_Function__c,
                                IDMSSalutation__c,IDMSSuffix__c,Fax,IDMSDelegatedIdp__c,IDMSIdentityType__c,IDMSJobDescription__c, IDMSPrimaryContact__c,  IDMS_VU_NEW__c,
                                IDMS_VU_OLD__c from User where Id IN:idmsUids];
        
        
        try{
            //stub class logic starts here
            IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort bulkService = new IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort(); 
            string callerFid = LAbel.CLJUN16IDMS104;
            bulkService.endpoint_x = Label.CLNOV16IDMS020;
            bulkService.clientCertName_x = System.Label.CLJUN16IDMS103;
            //Request to update contacts
            IdmsUsersResyncflowServiceIms.contactsUpdateRequest identityUpdateRequest = new  IdmsUsersResyncflowServiceIms.contactsUpdateRequest();
            identityUpdateRequest.contacts  = new IdmsUsersResyncflowServiceIms.createRequestBean();
            identityUpdateRequest.contacts.contactForUpdate = new List<IdmsUsersResyncflowServiceIms.ContactForUpdate>();
            
            //Response from UIMS            
            IdmsUsersResyncflowServiceIms.updateContactsResponse response = new IdmsUsersResyncflowServiceIms.updateContactsResponse();
            
            for(User usr: idmsUsers){
                IdmsUsersResyncflowServiceIms.contactForUpdate contactForUpdate = new IdmsUsersResyncflowServiceIms.contactForUpdate();
                contactForUpdate.federatedId = usr.FederationIdentifier;
                contactForUpdate.contact = new IdmsUsersResyncflowServiceIms.contactBean();
                IdmsUsersResyncflowServiceIms.contactBean uimsCntBean = IdmsUimsReconcUserMapping.mapIdmsUserUims(usr);
                contactForUpdate.contact = uimsCntBean;
                identityUpdateRequest.contacts.contactForUpdate.add(contactForUpdate);
            }
            
            //UIMS response. 
            response.return_x = bulkService.updateContacts(callerFid,identityUpdateRequest);
            IdmsUsersResyncflowServiceIms.contactUpdateReturnBean[] responseValues = response.return_x.contacts.contactUpdatedRes;
            
            Set<String> uimsFedIds = new Set<String>();
            if(responseValues.size() > 0){
                for(IdmsUsersResyncflowServiceIms.contactUpdateReturnBean responsevalue: responseValues){
                    if(responsevalue.federatedId!=null){
                    uimsFedIds.add(responsevalue.federatedId);
                    }
                }
            }
            
            List<User> updateIdmsUsers = new List<User>();
            List<IDMSReconciliationBatchLog__c> createBatchLogs= new List<IDMSReconciliationBatchLog__c>();
            List<IDMSUserReconciliationBatchHandler__c> handlerRecordsToDel = new List<IDMSUserReconciliationBatchHandler__c>();
            List<IDMSUserReconciliationBatchHandler__c> handlerRecordsToUpdate = new List<IDMSUserReconciliationBatchHandler__c>();
            //corresponding user ids set
            set<String> idmsUserIds = new set<String>();
            Map<String, User> mapUimsFedIdIdmsUser = new Map<String, User>();
            List<User> idmsUserList = [Select Id, Email,username, FederationIdentifier, IDMS_VU_NEW__c, IDMS_VU_OLD__c from User where ID IN: idmsUids AND FederationIdentifier IN:uimsFedIds ];
            System.debug('*****List of users****'+idmsUserList);
            for(User usr: idmsUserList){
                mapUimsFedIdIdmsUser.put(usr.FederationIdentifier, usr);
                idmsUserIds.add(usr.Id);
            }
            
            Map<String, IDMSUserReconciliationBatchHandler__c> mapUserIdHandler = new Map<String, IDMSUserReconciliationBatchHandler__c>();
            //corresponding User handler list
            List<IDMSUserReconciliationBatchHandler__c> userHandlerList = [select Id, IdmsUser__c, NbrAttempts__c, HttpMessage__c  from IDMSUserReconciliationBatchHandler__c where IdmsUser__c IN: idmsUserIds];
            for(IDMSUserReconciliationBatchHandler__c userHandler: userHandlerList){
                mapUserIdHandler.put(userHandler.IdmsUser__c, userHandler);
            }   
            
            for(IdmsUsersResyncflowServiceIms.contactUpdateReturnBean responsevalue: responseValues){
            if(responsevalue.federatedId!= null){
                User idmsUserUpdate = mapUimsFedIdIdmsUser.get(responsevalue.federatedId);
              
                if(responsevalue.returnCode == 0){
                    //successfully updated.
                    if(idmsUserUpdate!=null){
                        idmsUserUpdate.IDMS_VU_OLD__c  = idmsUserUpdate.IDMS_VU_NEW__c;
                        idmsUserUpdate.FederationIdentifier = responsevalue.federatedId;
                        updateIdmsUsers.add(idmsUserUpdate);
                    }
                    
                    //add to batch log. 
                    IDMSReconciliationBatchLog__c idmsBatchLog = IdmsReconcBatchLogsHandler.batchLogsForUpdateUser(idmsUserUpdate, responsevalue, batchId);
                    createBatchLogs.add(idmsBatchLog);     
                    System.debug('Batch log created for success users:'+createBatchLogs);
                    
                    //Add handler record in the list to delete.
                    IDMSUserReconciliationBatchHandler__c handlerRecToDelete = mapUserIdHandler.get(idmsUserUpdate.Id);
                    handlerRecordsToDel.add(handlerRecToDelete);
                }else {
                    //Update User handler record.
                    IDMSUserReconciliationBatchHandler__c handlerRecToUpdate = mapUserIdHandler.get(idmsUserUpdate.Id);
                    if(handlerRecToUpdate.NbrAttempts__c < attemptsMaxLmt){
                        handlerRecToUpdate.NbrAttempts__c = handlerRecToUpdate.NbrAttempts__c + 1;
                    }
                    handlerRecToUpdate.HttpMessage__c = 'FederatedId:'+responsevalue.federatedId+'\n'+'Has been updated:'+responsevalue.hasBeenUpdated+'\n'+'Message:'+responsevalue.message+'\n'+'Return Code:'+responsevalue.returnCode;
                    handlerRecordsToUpdate.add(handlerRecToUpdate);
                    
                    //add to batch log. 
                    IDMSReconciliationBatchLog__c idmsBatchFailedLog = IdmsReconcBatchLogsHandler.batchLogsForUpdateUser(idmsUserUpdate, responsevalue, batchId);
                    createBatchLogs.add(idmsBatchFailedLog);     
                }
              }
             }
            Update updateIdmsUsers;
            Insert createBatchLogs;
            Delete handlerRecordsToDel;
            Update handlerRecordsToUpdate;
        
        }catch(Exception e){
            System.debug('Exception occurs:'+e.getStackTraceString());  
            //insert exception in Idms error logs. 
            errorLog.IDMScreateLog('IDMSReconciliationBatch','IdmsUimsReconcBulkCalls','Bulk user Update service in UIMS','',Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),batchId,Datetime.now(),'','',false,'',false,null);
            
        }
    }
}