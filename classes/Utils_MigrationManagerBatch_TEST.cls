/*
Author: Pooja Gupta
Date Created: 07 August 2012
Description: Test Class for Utils_MigrationManagerBatch class
*/
@IsTest
private class Utils_MigrationManagerBatch_TEST
{
    static testmethod void Utils_MigrationManagerBatch()
    {     
        Database.executeBatch(new Utils_MigrationManagerBatch());        
    }
}