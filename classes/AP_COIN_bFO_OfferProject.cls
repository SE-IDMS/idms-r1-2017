/*********************

May Release 2014
hbp
**********************/

public Class AP_COIN_bFO_OfferProject {


    public static void UpdateEmailTempleCountry(list<Milestone1_Project__c> listMileStn) {
    
        set<id> setOfferId = new set<id>();
        set<id> setMilProjId = new set<id>();
        Map<id,Offer_Lifecycle__c> mapUpdate = new Map<id,Offer_Lifecycle__c>();
        map<string,list<Milestone1_Project__c>> mapMileStone= new map<string,list<Milestone1_Project__c>>();
        //
        
        for(Milestone1_Project__c mspObj:listMileStn) {
        
            setOfferId.add(mspObj.Offer_Launch__c); 
            setMilProjId.add(mspObj.id) ;
        }
        
        
        list<Milestone1_Project__c> listMilProj=[select id,Offer_Launch__c,Country__r.name from Milestone1_Project__c where id IN:setMilProjId ];
        for(Milestone1_Project__c msObj:[select id,Offer_Launch__c,Country__r.name from Milestone1_Project__c where Country__c!=null and id IN:setMilProjId ]) {
            if(mapMileStone.containskey(msObj.Offer_Launch__c)) {
                mapMileStone.get(msObj.Offer_Launch__c).add(msObj);
            }else {
                mapMileStone.put(msObj.Offer_Launch__c,new List<Milestone1_Project__c>());
                mapMileStone.get(msObj.Offer_Launch__c).add(msObj);
            }
        
        
        
        }
        list<Offer_Lifecycle__c> listOffer=[select id,TECH_Country__c,name from Offer_Lifecycle__c where id IN: setOfferId];
        
        for(Offer_Lifecycle__c offObj:listOffer) {
            if(mapMileStone.containskey(offObj.id)) {
                for(Milestone1_Project__c mpObj:mapMileStone.get(offObj.id)) {
                    if(offObj.id==mpObj.Offer_Launch__c) {
                        if(offObj.TECH_Country__c ==null) {
                            offObj.TECH_Country__c =mpObj.Country__r.name;
                            mapUpdate.put(offObj.id,offObj);
                        } else if(!offObj.TECH_Country__c.contains(mpObj.Country__r.name)) {
                            offObj.TECH_Country__c = offObj.TECH_Country__c+';'+mpObj.Country__r.name;
                            mapUpdate.put(offObj.id,offObj);
                        }              
                    }            
                }
            }        
        }
        if(mapUpdate.size() > 0) {
            Database.SaveResult[] offObjUpdate = Database.update(mapUpdate.values(),true);
            
        }
    
    }
   /* public static void UpdateOfferCountryOnDelete(list<Milestone1_Project__c> listMileDelete) {
    
        set<id> setOfferId = new set<id>();
        set<id> setMilProjId = new set<id>();
        Map<id,Offer_Lifecycle__c> mapUpdate = new Map<id,Offer_Lifecycle__c>();
        map<string,list<Milestone1_Project__c>> mapMileStone= new map<string,list<Milestone1_Project__c>>();
        //
        
        for(Milestone1_Project__c mspObj:listMileDelete) {
        
            setOfferId.add(mspObj.Offer_Launch__c); 
            setMilProjId.add(mspObj.id) ;
        }
        
        
        list<Milestone1_Project__c> listMilProj=[select id,Offer_Launch__c,Country__r.name from Milestone1_Project__c where id IN:setMilProjId ];
        for(Milestone1_Project__c msObj:[select id,Offer_Launch__c,Country__r.name from Milestone1_Project__c where Country__c!=null and id IN:setMilProjId ]) {
            if(mapMileStone.containskey(msObj.Offer_Launch__c)) {
                mapMileStone.get(msObj.Offer_Launch__c).add(msObj);
            }else {
                mapMileStone.put(msObj.Offer_Launch__c,new List<Milestone1_Project__c>());
                mapMileStone.get(msObj.Offer_Launch__c).add(msObj);
            }
        
        
        
        }
        list<Offer_Lifecycle__c> listOffer=[select id,TECH_Country__c,name from Offer_Lifecycle__c where id IN: setOfferId];
         for(Offer_Lifecycle__c offObj:listOffer) {
            //
            if(mapMileStone.containskey(offObj.id)) {
                for(Milestone1_Project__c mpObj:mapMileStone.get(offObj.id)) {
                    if(offObj.id==mpObj.Offer_Launch__c) {
                        //if(offObj.TECH_Country__c !=null && offObj.TECH_Country__c.contains(mpObj.Country__r.name)) {
                            //offObj.TECH_Country__c = offObj.TECH_Country__c.remove(mpObj.Country__r.name).removeEnd(';').removeStart(';');
                            offObj.TECH_Country__c = null;
                            mapUpdate.put(offObj.id,offObj);
                        //}              
                    }            
                }
            }
                    
        }
        if(mapUpdate.size() > 0) {
            Database.SaveResult[] offObjUpdate = Database.update(mapUpdate.values(),true);
        }
    
    }
    */
    //Divya M --  Update the Tech field
    public static void UpdateTechCountryOnPjctDelete(set<id> olcids) {
    
        Map<id,Offer_Lifecycle__c> mapUpdate = new Map<id,Offer_Lifecycle__c>();
        map<string,list<Milestone1_Project__c>> mapMileStone= new map<string,list<Milestone1_Project__c>>();
        
         for(Milestone1_Project__c msObj:[Select id,Name,Country__r.name,Offer_Launch__c from Milestone1_Project__c where Offer_Launch__c in:olcids]) {
         
            if(mapMileStone.containskey(msObj.Offer_Launch__c)) {
                mapMileStone.get(msObj.Offer_Launch__c).add(msObj);
            }else {
                mapMileStone.put(msObj.Offer_Launch__c,new List<Milestone1_Project__c>());
                mapMileStone.get(msObj.Offer_Launch__c).add(msObj);
            }
         
         }
         
        for(Offer_Lifecycle__c offObj:[Select id,name,TECH_Country__c from Offer_Lifecycle__c where id in:olcids]){
           system.debug('tech field is-->'+offObj.TECH_Country__c);
           offObj.TECH_Country__c=null;
           if(mapMileStone.containskey(offObj.id)) {
                for(Milestone1_Project__c mpObj:mapMileStone.get(offObj.id)){
                    
                        system.debug('--and Country is-->'+mpObj.Country__r.name);
                        if(offObj.TECH_Country__c ==null) {
                            offObj.TECH_Country__c =mpObj.Country__r.name;
                            mapUpdate.put(offObj.id,offObj);
                        } else if(offObj.TECH_Country__c !=null){
                            if(!offObj.TECH_Country__c.contains(mpObj.Country__r.name)) {
                                offObj.TECH_Country__c = offObj.TECH_Country__c+';'+mpObj.Country__r.name;
                                mapUpdate.put(offObj.id,offObj);
                            }    
                        }
                   
                }
            }
        }
        
        if(mapUpdate.size()>0){
            Database.SaveResult[] offObjUpdate = Database.update(mapUpdate.values(),true);
        }
    }
}//endof Main