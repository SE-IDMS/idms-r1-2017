/**
    * An apex page controller that takes the user to the right start page based on credentials or lack thereof
    */
    @IsTest public with sharing class CommunitiesLandingControllerTest {
    @IsTest(SeeAllData=true) public static void testCommunitiesLandingController() {
    // Instantiate a new controller with all parameters in the page
    CommunitiesLandingController controller = new CommunitiesLandingController();
    controller.forwardToStartPage();
    PageReference pageRef = Page.SocialUserRegistration;
    Test.setCurrentPage(pageRef);
    
    
    }
    
    @IsTest(SeeAllData=true) public static void testCommunitiesLandingController1() {
    // Instantiate a new controller with all parameters in the page
    
    // This code runs as the system user
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
    User u = new User(Alias = 'standt28', Email='standarduser28sept16@accenture.com', 
    EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', UserRoleId = r.Id,
    LocaleSidKey='en_US', ProfileId = p.Id, IDMS_Registration_Source__c = 'Facebook', Country= '', Country__c='',
    TimeZoneSidKey='America/Los_Angeles', UserName='standarduser28sept16@accenture.com');
    
    System.runAs(u) {
     // The following code runs as user 'u' 
     System.debug('Current User: ' + UserInfo.getUserName());
     System.debug('Current Profile: ' + UserInfo.getProfileId()); 
     User Sc_User = [select Id, IDMS_Registration_Source__c, Country, Country__c from User where Id=:u.Id limit 1];
     System.debug('Facebook' + Sc_User.IDMS_Registration_Source__c);
     CommunitiesLandingController controller = new CommunitiesLandingController();
    controller.forwardToStartPage();
    PageReference pageRef1 = new PageReference('/identity/SocialUserRegistration');
    Test.setCurrentPage(pageRef1);
    } 
    
    }
    }