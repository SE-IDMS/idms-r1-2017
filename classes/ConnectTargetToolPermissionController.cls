public with sharing class ConnectTargetToolPermissionController {

//Constructor

public ConnectTargetToolPermissionController(){
  
      Country = null;
      Type = 'Country KPI';
      Years = Label.ConnectYear;
      ShowPerm = False;
      }

//Declaration

public string Country {get;set;}
public string Years {get;set;}
public string Type {get;set;}
public string Acronym {get;set;}
public boolean ShowPerm{get;set;}
List<User> Usrname = new List<user>();
Map<id,string> Usr_Name = new Map<id,string>();
set<id> Grp = new Set<id>();
Map<id,string> Grp_Name = new Map<id,string>();

List<Cascading_KPI_Target__c> lstEntityKPI = new List<Cascading_KPI_Target__c>();
List<Cascading_KPI_Target__Share> ES2 = new List<Cascading_KPI_Target__Share>();

Set<id> KPI = new Set<id>();
set<id> Usr = new Set<id>();
Map<id,string> UsrCtry = new Map<id,string>();

List<Cascading_KPI_Target__Share> ES = new List<Cascading_KPI_Target__Share>();

//Contry Select Option List
    
    public List<SelectOption> getFilterList() 
    {
     Set<String> SetCountries = new Set<String>();
     Set<String> SetEntity = new Set<String>();
     lstEntityKPI = [SELECT id, KPI_Name__c, KPI_Acronym__c,Entity__c, CountryPickList__c FROM Cascading_KPI_Target__c where KPI_Type__c = :Type and KPI_Acronym__c = :Acronym and Year__c = :Years order by EntityOverallKPIOrder__c];      
     
    
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('','<Select a Entity/Country>'));
     
      for(Cascading_KPI_Target__c eKPI:lstEntityKPI)
       {

          if(eKPI.CountryPickList__c != null){
            SetCountries.add(eKPI.CountryPickList__c);           
            UsrCtry.put(eKPI.id, eKPI.CountryPicklist__c);
            }
            
         if(eKPI.Entity__c != null){
            SetEntity.add(eKPI.Entity__c);
            KPI.add(eKPI.id);
            UsrCtry.put(eKPI.id, eKPI.CountryPicklist__c);
            }
        }
          
       if(Type == 'Country KPI'){
        for(String ct:SetCountries)
            options.add(new SelectOption(ct,ct)); 
          }    
      
      if(Type == 'Global KPI'){
       options.clear();
       options.add(new SelectOption('','<Select a Entity/Country>'));
       for(String ct:SetEntity)
        options.add(new SelectOption(ct,ct)); 
          
       }    
        options.sort();          
        return options;
    }
    
 
 //Get All Acronyms
 
 public List<SelectOption> getFilterAcronym() 
    {
     Set<String> SetAcronym = new Set<String>();
    
     lstEntityKPI = [SELECT id, KPI_Name__c, KPI_Acronym__c,Entity__c, CountryPickList__c FROM Cascading_KPI_Target__c where KPI_Type__c = :Type and Year__c = :Years order by EntityOverallKPIOrder__c];      
     
    
     List<SelectOption> options = new List<SelectOption>();
     options.add(new SelectOption('','<Select KPI Acronym'));
     
      for(Cascading_KPI_Target__c eKPI:lstEntityKPI)
        SetAcronym.add(eKPI.KPI_Acronym__c);
          
        for(String ct:SetAcronym)
            options.add(new SelectOption(ct,ct)); 
          
        options.sort();          
        return options;
    }
    
    // Year Selection option
    
     public List<SelectOption> getFilterYear() 
    {
     List<SelectOption> Yearoptions = new List<SelectOption>();
     Yearoptions.add(new SelectOption('2013','2013')); 
     Yearoptions.add(new SelectOption('2014','2014')); 
          
     return Yearoptions;
     }
     
    public List<SelectOption> getFilterType() 
    {
     List<SelectOption> Typeoptions = new List<SelectOption>();
     Typeoptions.add(new SelectOption('Country KPI','Country KPI')); 
     Typeoptions.add(new SelectOption('Global KPI','Global KPI')); 
          
     return Typeoptions;
     }
     
public pagereference FilterChange()
    {
        if(PermData != null)
            PermData.clear();
        ShowPerm = True;
        return null;  
    }
    
public pagereference TypeChange()
    {
        if(PermData != null)
            PermData.clear();
        return null;  
    }
 
public pagereference AcronymChange()
    {
        if(PermData != null)
            PermData.clear();
        return null;  
    }
    
public pagereference YearChange()
    {
      if(PermData != null)
            PermData.clear();
        return null;  
    }

public List<UserPerm> PermData{get;set;}

    
public pagereference ReportData()
    {
    
    // All users with Sharing permissions
    KPI.clear();
    List<Cascading_KPI_Target__c> lstEntityKPIFil = new List<Cascading_KPI_Target__c>();
    if(Type == 'Country KPI')
     lstEntityKPIFil = [SELECT id, KPI_Name__c, KPI_Acronym__c,Entity__c, CountryPickList__c FROM Cascading_KPI_Target__c where KPI_Type__c = :Type and Year__c = :Years and KPI_Acronym__c = :Acronym and CountryPickList__c = :Country];      
    if(Type == 'Global KPI')
     lstEntityKPIFil = [SELECT id, KPI_Name__c, KPI_Acronym__c,Entity__c, CountryPickList__c FROM Cascading_KPI_Target__c where KPI_Type__c = :Type and Year__c = :Years and KPI_Acronym__c = :Acronym and Entity__c = :Country];      
     
     for(Cascading_KPI_Target__c eKPI:lstEntityKPIFil)
       {
           KPI.add(eKPI.id);   
        }
      
       
       system.debug('Permission of User '+  KPI.size());
       
      ES = [Select ParentID, RowCause,AccessLevel,UserOrGroupId from Cascading_KPI_Target__Share where ParentID in :KPI  and UserOrGroupId NOT IN(select id from Group) ];
      PermData = new List<UserPerm>();
      
      system.debug('Permission of User '+  ES.size());
      
     for(Cascading_KPI_Target__Share E:ES)
        Usr.add(E.UserOrGroupId);
    
    Usrname = [Select id, Name from User where Id in :Usr and IsActive = :True];

    For(User U:Usrname){
        Usr_Name.put(U.id,U.Name);
        }
         
     for(Cascading_KPI_Target__Share E:ES)
         PermData.add(new UserPerm(E.ParentID, Usr_Name.get(E.UserOrGroupId),E.AccessLevel,E.RowCause,'User'));     
         
    // All group users with sharing perissions
    
    ES2 = [Select ParentID, RowCause,AccessLevel,UserOrGroupId from Cascading_KPI_Target__Share where ParentID in :KPI  and UserOrGroupId IN(select id from Group) ];
    
    for(Cascading_KPI_Target__Share CS:ES2)
        Grp.add(CS.UserOrGroupId);
        
    for(Group G:[Select id,Name from Group where id in :Grp])
       Grp_Name.put(G.id,G.Name);
       
    List<GroupMember> groupMembers = [Select GroupId, UserOrGroupId From GroupMember Where GroupId In :Grp_Name.keySet()];
    
    Set<Id> user2 = new Set<Id>();
    for (GroupMember member : groupMembers)
        user2.add(member.UserOrGroupId);

    Map<Id, User> userMap = new Map<Id, User>([Select Id, Name
    From User Where Id In :user2 and IsActive = :True and id not in :usr]);
    
    Map<Id, List<User>> groupIdToUsers = new Map<Id, List<User>>();
    for (Id groupId : Grp_Name.keySet()) {
    groupIdToUsers.put(groupId, new List<User>());
    }
    
    for (GroupMember member : groupMembers) {
     if (userMap.containsKey(member.UserOrGroupId)) 
        groupIdToUsers.get(member.GroupId).add(userMap.get(member.UserOrGroupId));
    }
    integer i;
   for(Cascading_KPI_Target__Share E:ES2){
     
     for(User U: groupIdToUsers.get(E.UserOrGroupId))   
   
      PermData.add(new UserPerm(E.ParentID,U.Name,E.AccessLevel,E.RowCause,Grp_Name.get(E.UserOrGroupId)+' - Group')); 
      
        }
        
         return null;
    }
    
public class UserPerm{
    public Id Entid{get;set;}
    Public String usrname{get;set;}
    public String permtype{get;set;}
    public String permreason{get;set;}
    public String usertype{get;set;}
    

    public UserPerm(id id, string un, string pt, string pr,string ut){
        this.Entid = id;
        this.usrname = un;
        this.permtype=pt;
        this.permreason=pr;
        this.usertype =ut;
       }
   }
   
 
  }