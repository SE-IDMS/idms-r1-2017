@isTest
private class VFC_NewFieldServiceBulletin_Test {

    static testMethod void Method1() {
        
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        runAsUser.BypassWF__c = True;
        runAsUser.BypassVR__c = True;
        insert runAsUser;
        
        System.runAs(runAsUser) {
        
            BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                    accOrg1.Name='Test';
                    accOrg1.Entity__c='Test Entity-2'; 
                    insert  accOrg1;
            BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                    accOrg2.Name='Test';
                    accOrg2.Entity__c='Test Entity-2'; 
                    accOrg2.SubEntity__c='Test Sub-Entity 2';
                    insert  accOrg2;
            BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                    accOrg.Name='Test';
                    accOrg.Entity__c='Test Entity-2';  
                    accOrg.SubEntity__c='Test Sub-Entity 2';
                    accOrg.Location__c='Test Location 2';
                    accOrg.Location_Type__c='Design Center';
                    insert  accOrg;
            
            Problem__c prob = Utils_TestMethods.createProblem(accOrg.id);
            prob.Severity__c = 'Safety Related';
            prob.RecordTypeid = Label.CLI2PAPR120014;
            insert prob;
            
            OPP_Product__c OPPprod = Utils_TestMethods.createProduct('businessUnit','productLine','','');
            insert OPPprod;
        
            FieldServiceBulletin__c FSB = new FieldServiceBulletin__c();
            FSB.Name = 'Test'; 
            FSB.Description__c = 'Test Description';
            FSB.Status__c = '1. New';
            FSB.TypeofDocumenttoGenerate__c = 'FSB';
            FSB.ProductLine__c = OPPprod.Id;
            FSB.Problem__c = prob.Id;
            insert FSB;   
            
            ApexPages.StandardController controller = new ApexPages.StandardController(FSB);
            PageReference pageRef = Page.VFP_NewFieldServiceBulletin;
            ApexPages.currentPage().getParameters().put(Label.CLQ216I2P04 , prob.id);
            VFC_NewFieldServiceBulletin newFSB = new VFC_NewFieldServiceBulletin(controller);
            newFSB.goToEditPage();
        }
    }
    
    static testMethod void Method2() {
        
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        runAsUser.BypassWF__c = True;
        runAsUser.BypassVR__c = True;
        insert runAsUser;
        
        System.runAs(runAsUser) {
            
            OPP_Product__c OPPprod = Utils_TestMethods.createProduct('businessUnit','productLine','','');
            insert OPPprod;
        
            FieldServiceBulletin__c FSB = new FieldServiceBulletin__c();
            FSB.Name = 'Test'; 
            FSB.Description__c = 'Test Description';
            FSB.Status__c = '1. New';
            FSB.TypeofDocumenttoGenerate__c = 'FSB';
            FSB.ProductLine__c = OPPprod.Id;
            insert FSB;   
            
            ApexPages.StandardController controller1 = new ApexPages.StandardController(FSB);
            PageReference pageRef1 = Page.VFP_NewFieldServiceBulletin;
            ApexPages.currentPage().getParameters().put(Label.CLQ216I2P04 , null);
            VFC_NewFieldServiceBulletin newFSB1 = new VFC_NewFieldServiceBulletin(controller1);
            newFSB1.goToEditPage();
        }
    }
    
}