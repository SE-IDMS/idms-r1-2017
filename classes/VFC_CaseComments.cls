public with sharing class  VFC_CaseComments{
    
    Public List<CaseComment> relatedCaseComments{get;set;}
    public List<CSE_L2L3InternalComments__c> relatedCaseEscalationInternalComments{get;set;}
    public List<ExpertInternalComment__c> relatedCaseExpertInternalComments{get;set;}
    public List<EmailMessage>  relatedCaseEmailMessage{get;set;}
    
    public Integer noOfCaseComments{get;set;}
    public Integer noOfEscalationComments{get;set;}
    public Integer noOfExpertComments{get;set;}
    public Integer noOfEmails{get;set;}
    
    
    public VFC_caseComments(ApexPages.StandardController controller) {
        String caseId = Apexpages.currentPage().getParameters().get('Id');
        
        noOfCaseComments =0;
        noOfEscalationComments =0;
        noOfExpertComments=0;
        noOfEmails =0;
        
        relatedCaseComments= new List<CaseComment>([select id,CommentBody,IsPublished,parentId,CreatedDate,CreatedBy.name from CaseComment where parentId=:caseId order by createddate desc]);
        system.debug('---------relatedCaseComments------------'+relatedCaseComments);
        noOfCaseComments = relatedCaseComments.size();
        
        relatedCaseEscalationInternalComments = new List<CSE_L2L3InternalComments__c>([select id,name,Activity__c,Comments__c,Duration__c,createddate,CreatedBy.name from CSE_L2L3InternalComments__c where Case__c=:caseId order by createddate desc]);
        noOfEscalationComments = relatedCaseEscalationInternalComments.size();
        
        relatedCaseExpertInternalComments = new List<ExpertInternalComment__c>([select id,name,Comments__c,createddate,CreatedBy.name from ExpertInternalComment__c where Case__c =:caseId order by createddate desc]);
        noOfExpertComments = relatedCaseExpertInternalComments.size();
        
        relatedCaseEmailMessage = new List<EmailMessage>([select id,Subject,Status,ToAddress,TextBody,CreatedDate,CreatedBy.name from EmailMessage where ParentId=:caseId order by createddate desc]);
        for(EmailMessage  email:relatedCaseEmailMessage ){
            //email.Subject = email.Subject.substringBefore('['); 
        }
        noOfEmails = relatedCaseEmailMessage.size();
    }
    
  
    
  
   
}