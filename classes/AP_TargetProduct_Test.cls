@isTest
private class AP_TargetProduct_Test 
{ 
     public static testmethod void inactivepqa()
    {
       
       Product_Quality_Alert__c pqa1 = new Product_Quality_Alert__c();
       pqa1.Alert_Type__c = 'Request For Analysis'; 
       pqa1.ReturnReason__c = 'Request For Analysis Direct Shipment';
       pqa1.Start_Date__c = system.TODAY()-10 ;
       pqa1.End_Date__c = system.TODAY()+10;
       insert pqa1; 
       
       
       Product_Quality_Alert__c pqa2 = new Product_Quality_Alert__c();
       pqa2.Alert_Type__c = 'Request For Analysis'; 
       pqa2.ReturnReason__c = 'Request For Analysis Direct Shipment';
       pqa2.Start_Date__c = system.TODAY()-9 ;
       pqa2.End_Date__c = system.TODAY()+9;
       insert pqa2; 
       
       
       product2 proda = new product2();
       proda.Name = 'testProductA';
       insert proda;
       
       
       Target_Products__c tp1 = new Target_Products__c();
       tp1.Product__c = proda.id;
       tp1.Product_Quality_Alert__c = pqa1.id;
       insert tp1;
       
       
       Target_Products__c tp2 = new Target_Products__c();
       tp2.Product__c = proda.id;
       tp2.Product_Quality_Alert__c = pqa2.id;
       try{
       insert tp2;
       }
       catch(DmlException e){
           System.debug('The following exception has occurred: ' + e.getMessage());

       }
    }
    public static testmethod void activepqa()
    {
       
       Product_Quality_Alert__c pqa1 = new Product_Quality_Alert__c();
       pqa1.Alert_Type__c = 'Request For Analysis'; 
       pqa1.ReturnReason__c = 'Request For Analysis Direct Shipment';
       pqa1.Start_Date__c = system.TODAY()-10 ;
       pqa1.End_Date__c = system.TODAY()+10;
       insert pqa1; 
       
       
       Product_Quality_Alert__c pqa2 = new Product_Quality_Alert__c();
       pqa2.Alert_Type__c = 'Request For Analysis'; 
       pqa2.ReturnReason__c = 'Request For Analysis Direct Shipment';
       pqa2.Start_Date__c = system.TODAY()-9 ;
       pqa2.End_Date__c = system.TODAY()+9;
       insert pqa2; 
       
       
       product2 proda = new product2();
       proda.Name = 'testProductA';
       insert proda;
       
       
       Target_Products__c tp1 = new Target_Products__c();
       tp1.Product__c = proda.id;
       tp1.Product_Quality_Alert__c = pqa1.id;
       insert tp1;
       
       
       Target_Products__c tp2 = new Target_Products__c();
       tp2.Product__c = proda.id;
       tp2.Product_Quality_Alert__c = pqa2.id;
       
       BusinessRiskEscalationEntity__c org = new BusinessRiskEscalationEntity__c();
       org.name = 'TEST ORG';
       org.Entity__c = 'TORGENTITY';
       insert org;
       
       plant__c pl = new plant__c();
       pl.Name = 'TEST PLANT';
       insert pl;
       
       
       
       CoveredOrganization__c covOrg = new CoveredOrganization__c();
       covOrg.Product_Quality_Alert__c = pqa1.id;
       covOrg.Organization__c = org.id;
       covOrg.Plant__c = pl.id ; 
       insert covOrg;
       update pqa1;

      try{
       insert tp2;
       }
       catch(DmlException e){
           System.debug('The following exception has occurred: ' + e.getMessage());

       }
    }
}