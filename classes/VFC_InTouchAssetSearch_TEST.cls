@istest
private class VFC_InTouchAssetSearch_TEST {
    public static testmethod void testInTouchAssetService() {
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        insert objContact;
    
        Case objCaseForAsset = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'Open');
        insert objCaseForAsset;
        
        PageReference pageRef = Page.VFP_InTouchAssetSearch;
        Test.setCurrentPageReference(pageRef);
        
     // Add parameter to page URL
        ApexPages.currentPage().getParameters().put('caseId', objCaseForAsset.id);
       
        
       
     // Instantiate the controller with search parameters in the page
        VFC_InTouchAssetSearch inTouchAssetSearchController = new VFC_InTouchAssetSearch();
        
        
        inTouchAssetSearchController.clear();        
        inTouchAssetSearchController.SEAccountID='TESTSEACCOUNTID';
        inTouchAssetSearchController.search();

        inTouchAssetSearchController.createAsset();
        inTouchAssetSearchController.CommercialReference='BK500';
        inTouchAssetSearchController.createAssetInInTouch();
        if(inTouchAssetSearchController.InTouchAssetWrapperList.size()>0)
            inTouchAssetSearchController.InTouchAssetWrapperList[0].blnSelected=TRUE;
        inTouchAssetSearchController.SelectAsset();
        inTouchAssetSearchController.cancel();
        
    }
}