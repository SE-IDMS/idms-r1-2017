@isTest
private class Batch_SalesContributorDataMigration_TEST {
    static testMethod void testSalesContributorDataMigration() 
    {
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        List<Opportunity> opptys = new List<Opportunity>();
        List<OPP_SalesContributor__c> slsConts = new List<OPP_SalesContributor__c>();
        User tst1 = Utils_TestMethods.createStandardUser('SLSMgU1');
        insert tst1;
        
        Opportunity objOpp = Utils_TestMethods.createOpenOpportunity(objAccount.Id);
        objOpp.StageName  = '4 - Influence & Develop';
        objOpp.OwnerId = tst1.Id;
        opptys.add(objOpp);
        Opportunity objOpp1 = Utils_TestMethods.createOpenOpportunity(objAccount.Id);
        objOpp1.StageName  = '4 - Influence & Develop';
        objOpp.OwnerId = tst1.Id;        
        opptys.add(objOpp1);
        Opportunity objOpp2 = Utils_TestMethods.createOpenOpportunity(objAccount.Id);
        objOpp2.StageName  = '4 - Influence & Develop';
        
        opptys.add(objOpp2);
        Opportunity objOpp3 = Utils_TestMethods.createOpenOpportunity(objAccount.Id);
        objOpp3.StageName  = '4 - Influence & Develop';
        opptys.add(objOpp3);
        insert opptys;
        system.debug(opptys);
        OPP_SalesContributor__c objSales =  Utils_TestMethods.createSalesContr(opptys[0].Id, UserInfo.getUserId());
        objSales.Contribution__c = 10;
        OPP_SalesContributor__c objSales21 =  Utils_TestMethods.createSalesContr(opptys[0].Id, tst1.Id);
        objSales21.Contribution__c = 10;
        OPP_SalesContributor__c objSales22 =  Utils_TestMethods.createSalesContr(opptys[0].Id, tst1.Id);
        objSales22.Contribution__c = 10;
        slsConts.add(objSales);
        slsConts.add(objSales21);
        slsConts.add(objSales22);
        OPP_SalesContributor__c objSales1 =  Utils_TestMethods.createSalesContr(opptys[1].Id, UserInfo.getUserId());
        objSales1.Contribution__c = 50;
        slsConts.add(objSales1);
        OPP_SalesContributor__c objSales11 =  Utils_TestMethods.createSalesContr(opptys[1].Id, tst1.Id);
        objSales11.Contribution__c = 25;
        slsConts.add(objSales11);
        OPP_SalesContributor__c objSales12 =  Utils_TestMethods.createSalesContr(opptys[1].Id, tst1.Id);
        objSales12.Contribution__c = 25;
        slsConts.add(objSales12);
        OPP_SalesContributor__c objSales2 =  Utils_TestMethods.createSalesContr(opptys[2].Id, UserInfo.getUserId());
        objSales2.Contribution__c = 90;
        OPP_SalesContributor__c objSales31 =  Utils_TestMethods.createSalesContr(opptys[3].Id, UserInfo.getUserId());
        objSales31.Contribution__c = 100;
        slsConts.add(objSales31);
        OPP_SalesContributor__c objSales3 =  Utils_TestMethods.createSalesContr(opptys[2].Id, tst1.Id);
        objSales3.Contribution__c = 100;
        
        slsConts.add(objSales3);
        insert slsConts;
        OPP_ProductLine__c objOppLine = Utils_TestMethods.createProductLine(objOpp.Id);
        objOppLine.ProductBU__c = 'ProdBUA1';
        objOppLine.ProductLine__c = 'ProdLineA1';
        objOppLine.ProductFamily__c = 'ProdFamilyA1';
        objOppLine.Family__c = 'FamilyA1';
        objOppLine.TECH_CommercialReference__c = 'CMREF123';
        objOppLine.TECH_2014ReferentialUpdated__c =FALSE;
        insert objOppLine;
        
        Test.startTest();
        BATCH_SalesContributorDataMigration sls = new BATCH_SalesContributorDataMigration('select Id, OwnerId, TECH_TotalContributionPercentage__c, (select Id,Approved__c,Role__c,TECH_IsCloned__c,TECH_SalesContributorId__c,SplitAmount,SplitNote,OpportunityId,SplitTypeId,SplitPercentage,SplitOwnerId from OpportunitySplits),(select Id, Name,Approved__c,Contribution__c,ContributionAmount__c,InternalOrganization__c,Opportunity__c,Role__c,TECH_Error__c,DM_UserActive__c,TECH_IsCloned__c,TECH_IsMigrated__c,User__c,UserBU__c from SalesContributors__r where TECH_IsMigrated__c=false and Contribution__c>0) from Opportunity where StageName=\'2 - Define Opportunity Portfolio\' OR StageName=\'3 - Identify & Qualify\' OR StageName=\'4 - Influence & Develop\' OR StageName=\'5 - Prepare & Bid\' OR StageName=\'6 - Negotiate & Win\'');
        Database.executeBatch(sls,100);
        Test.stopTest();
        }
}