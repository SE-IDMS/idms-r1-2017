@isTest
public class FieloPRM_CockpitComponentControllerTest {

    public static testmethod void testCall() {

        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;

        PRMCountry__c prmFrance = new PRMCountry__c();
        prmFrance.Name = 'FR';
        prmFrance.CountryPortalEnabled__c = true;
        prmFrance.Country__c = france.Id;
        prmFrance.SupportedLanguage1__c = 'French';
        prmFrance.PLDatapool__c = 'FR_fr2';
        insert prmFrance;

        Account acc= new Account(
            Name = 'test',
            Street__c = 'Some Street',
            ZipCode__c = '012345',
            PLDataPool__c ='en_US2',
            City__c = 'Paris',
            Country__c = france.Id
        );
        insert acc;
                
        FieloEE__Member__c mem = new FieloEE__Member__c(
            FieloEE__FirstName__c = 'test',
            FieloEE__LastName__c= 'test',
            F_Country__c=france.Id,
            F_PRM_Country__c = prmFrance.id,
            F_Account__c  = acc.Id
        );
        insert mem;
    
        FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
        insert memb;
        //FieloEE.MemberUtil.setmemberId(memb.Id);
        FieloEE.MemberUtil.setmemberId(mem.Id);
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c(
            FieloEE__Title__c = 'test'
        );
        insert menu;

        Fielo_CRUDObjectSettings__c testCrud = new Fielo_CRUDObjectSettings__c(
            F_ObjectAPIName__c = 'Account'
        );
        insert testCrud;
    
        FieloEE__Component__c component = new FieloEE__Component__c(
            FieloEE__Menu__c = Menu.Id,
            F_CRUDObjectSettings__c = testCrud.Id
        );
        insert component;

        ComponentParameter__c compParameters = new  ComponentParameter__c(Component__c=component.Id,BottomLabel__c='',BottomLink__c='http://www.google.com', BusinessTypeValues__c='', ShowBusinessTypeAndAreaOfFocus__c=false, SubmitURL__c='http://www.google.com', Subtitle__c='subTitle',Title__c='Title',BusinessTypeDefaultValue__c='WD',DistanceUnitValues__c='##DEF##km\r\nml',DistanceValues__c='##DEF##50\r\n100',AreaOfFocusExcludeValues__c='impossibleValue\r\nimpossibleValue2');
        insert compParameters;

        PRMCockpitComponentConfiguration__c myCmp = new PRMCockpitComponentConfiguration__c(WSURL__c='http://www.google.com');
        myCmp.CountryCluster__c = prmFrance.Id;
        myCmp.Type__c = 'myProject';
        insert myCmp;

        List<String> strList = new List<String> { 'newLeads', 'actionRequest','ongoing'};

        List<PRMCockpitElementConfiguration__c> compElemList = new List<PRMCockpitElementConfiguration__c>();
        for (Integer i = 0; i < 3; i++) {
            PRMCockpitElementConfiguration__c elem = new PRMCockpitElementConfiguration__c();
            elem.PRM_CockpitComponent_Configuration__c = myCmp.Id;
            elem.Content__c = '' + i;
            elem.ContentFromWS__c = true;
            elem.Visible__c = true;
            elem.Label__c = strList[i];
            compElemList.add(elem);
        }
        insert compElemList;

        Test.setMock(HttpCalloutMock.class, new FieloPRM_CockpitHttpCall(200, 'Complete',
                                                    '{"name": "myProject","elements":    [{"name": "newLeads",         "content": "1"      },            {         "name": "actionRequest",         "content": "3"      },            {         "name": "ongoing",         "content": "10"      }   ]}',
                                                    null));
        Test.startTest();
        System.debug('Test begins ... '); 

        FieloPRM_C_CockpitComponentController myController = new FieloPRM_C_CockpitComponentController();
        myController.currentComponentId = component.Id;

        FieloPRM_C_CockpitComponentController.Widget widg = myController.getComponentConfig();

       // System.assertEquals(prmFrance.Id,widg.CountryCluster);
        //System.assertEquals(3,widg.elements.size());
        Test.stopTest();

    }


    public class FieloPRM_CockpitHttpCall implements HttpCalloutMock {

        protected Integer code;
        protected String status;
        protected String body;
        protected Map<String, String> responseHeaders;

        public FieloPRM_CockpitHttpCall(Integer code, String status, String body, Map<String, String> responseHeaders) {
            this.code = code;
            this.status = status;
            this.body = body;
            this.responseHeaders = responseHeaders;
        }

        public HTTPResponse respond(HTTPRequest req) {

            HttpResponse res = new HttpResponse();
            for (String key : this.responseHeaders.keySet()) {
                res.setHeader(key, this.responseHeaders.get(key));
            }
            res.setBody(this.body);
            res.setStatusCode(this.code);
            res.setStatus(this.status);
            return res;
        }

    }

}