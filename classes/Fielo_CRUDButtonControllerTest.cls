@isTest
public with sharing class Fielo_CRUDButtonControllerTest {
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
    
        Fielo_PageComponent__c component = new Fielo_PageComponent__c(
            F_ButtonName__c = Label.Fielo_CRUDButtonControllerButtonName
        );
        insert component;
    
        Fielo_CRUDButtonController con = new Fielo_CRUDButtonController();
        con.cButton = component;
        con.doAction();
    
    }
}