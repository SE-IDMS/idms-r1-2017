@isTest
public class SRV_MyFs_SSOHandler_Test {
    
    public static testMethod void testSSOHandler() {

        SRV_MyFs_SSOHandler ssoHandler = new SRV_MyFs_SSOHandler();
        Id userId = UserInfo.getUserId();

        ssoHandler.createUser(userId, userId, userId,
                              'federation_Id', new Map<String, String>(), 'SAML_Assertion');

        ssoHandler.updateUser(userId, userId, userId, userId,
                              'federation_Id', new Map<String, String>(), 'SAML_Assertion');

    }

}