@isTest(SeeAllData=true)
public class VFC_WorkOrderConnector_TEST{
    static testMethod void unitTest() {
        // Data Creation 
        
        Account acc =Utils_TestMethods.createAccount();
        insert acc;
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
        SVMXC__Site__c siteLocation = Utils_TestMethods.createLocation(acc.id);
        insert siteLocation;
        
        workOrder.BackOfficeSystem__c = 'BBO_AM';
        workOrder.CustomerTimeZone__c = 'Europe/Paris';
        workOrder.SoldToAccount__c = acc.id;
        workOrder.SVMXC__Site__c = siteLocation.id;
        insert workOrder;
        PageReference pageRef = Page.VFP_WorkOrderConnector;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('ID', workOrder.id);
        ApexPages.currentPage().getParameters().put('action',Label.CLDEC12SRV19);
        
        ApexPages.StandardController sdtCon = new ApexPages.StandardController(workOrder);
        VFC_WorkOrderConnector myPageCon = new VFC_WorkOrderConnector(sdtCon);
        myPageCon.redirectToMW();  
        Test.starttest();
        workOrder.BackOfficeReference__c = 'BOREF';
        update workOrder;
        myPageCon.redirectToMW();  
        
        workOrder.BackOfficeSystem__c = Label.CLAPR15SRV82;
        workOrder.SVMXC__Order_Status__c = 'Service Validated';
        update workOrder;
        myPageCon.redirectToMW();  
        Test.stoptest();
        VFC_WorkOrderConnector.formatURL2(workOrder,Label.CLDEC12SRV19);  

    }
    static testMethod void unitTest2() {
        // Data Creation 
        Account acc =Utils_TestMethods.createAccount();
        insert acc;
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
        SVMXC__Site__c siteLocation = Utils_TestMethods.createLocation(acc.id);
        insert siteLocation;

        workOrder.BackOfficeSystem__c = 'ITB_ORA';
        workOrder.CustomerTimeZone__c = 'Europe/Paris';
        workOrder.SoldToAccount__c = acc.id;
        workOrder.SVMXC__Order_Status__c='New';
        workOrder.OperationName__c = 'Test';
        workorder.IsBillable__c='Yes';
        workOrder.SVMXC__Site__c = siteLocation.id;
        workOrder.NumberOfPartOrderLinesNOTSynchedWithBO__c = 2;
        insert workOrder;
        
        PageReference pageRef = Page.VFP_WorkOrderConnector;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('ID', workOrder.id);
        ApexPages.currentPage().getParameters().put('action','SO.REL');
        
        ApexPages.StandardController sdtCon = new ApexPages.StandardController(workOrder);
        VFC_WorkOrderConnector myPageCon = new VFC_WorkOrderConnector(sdtCon);
        myPageCon.redirectToMW(); 
        workOrder.NumberOfPartOrderLinesNOTSynchedWithBO__c = 0;
        update workOrder;
        myPageCon.redirectToMW(); 
        VFC_WorkOrderConnector.formatURL(workOrder,Label.CLDEC12SRV19);
        myPageCon.formatURL3(workOrder,Label.CLDEC12SRV19);
    }
     static testMethod void unitTest3() {
        // Data Creation 
        Account acc =Utils_TestMethods.createAccount();
        insert acc;
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
        SVMXC__Site__c siteLocation = Utils_TestMethods.createLocation(acc.id);
        insert siteLocation;

        workOrder.BackOfficeSystem__c = 'BBO_EU';
        workOrder.CustomerTimeZone__c = 'Europe/Paris';
        workOrder.SoldToAccount__c = acc.id;
        workOrder.OperationName__c = 'Test';
        workOrder.SVMXC__Site__c = siteLocation.id;
         insert workOrder;
         
         PageReference pageRef = Page.VFP_WorkOrderConnector;
         Test.setCurrentPage(pageRef);
         ApexPages.currentPage().getParameters().put('ID', workOrder.id);
         ApexPages.currentPage().getParameters().put('action','WO.CREATION');
         
         ApexPages.StandardController sdtCon = new ApexPages.StandardController(workOrder);
         VFC_WorkOrderConnector myPageCon = new VFC_WorkOrderConnector(sdtCon);
         myPageCon.redirectToMW(); 
         myPageCon.formatURL3(workOrder,'WO.CREATION');
         
         //VFC_WorkOrderConnector wc = new VFC_WorkOrderConnector();
         VFC_WorkOrderConnector.formatURL2(workOrder,Label.CLDEC12SRV19);      
    }
    
    static testMethod void unitTest4() {
        // Data Creation 
        Account acc =Utils_TestMethods.createAccount();
        insert acc;
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
        SVMXC__Site__c siteLocation = Utils_TestMethods.createLocation(acc.id);
        insert siteLocation;

        workOrder.BackOfficeSystem__c = 'ES_SAP';
        workOrder.CustomerTimeZone__c = 'Europe/Paris';
        workOrder.SoldToAccount__c = acc.id;
        workOrder.OperationName__c = 'Test';
        workOrder.SVMXC__Site__c = siteLocation.id;
         insert workOrder;
         
         PageReference pageRef = Page.VFP_WorkOrderConnector;
         Test.setCurrentPage(pageRef);
         ApexPages.currentPage().getParameters().put('ID', workOrder.id);
         ApexPages.currentPage().getParameters().put('action', Label.CLDEC12SRV20);
         
         ApexPages.StandardController sdtCon = new ApexPages.StandardController(workOrder);
         VFC_WorkOrderConnector myPageCon = new VFC_WorkOrderConnector(sdtCon);
         myPageCon.redirectToMW(); 
         VFC_WorkOrderConnector.formatURL(workOrder,Label.CLDEC12SRV19);      
    }
    
    static testMethod void unitTest5() {
        // Data Creation 
        Account acc =Utils_TestMethods.createAccount();
        insert acc;
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
        SVMXC__Site__c siteLocation = Utils_TestMethods.createLocation(acc.id);
        insert siteLocation;

        workOrder.BackOfficeSystem__c = 'ES_SAP';
        workOrder.CustomerTimeZone__c = 'Europe/Paris';
        workOrder.SoldToAccount__c = acc.id;
        workOrder.OperationName__c = 'Test';
        workOrder.BackOfficeReference__c = 'BOREF';
        workOrder.SVMXC__Site__c = siteLocation.id;
         insert workOrder;
         
         PageReference pageRef = Page.VFP_WorkOrderConnector;
         Test.setCurrentPage(pageRef);
         ApexPages.currentPage().getParameters().put('ID', workOrder.id);
         ApexPages.currentPage().getParameters().put('action', 'SO.REL');
         
         ApexPages.StandardController sdtCon = new ApexPages.StandardController(workOrder);
         VFC_WorkOrderConnector myPageCon = new VFC_WorkOrderConnector(sdtCon);
         myPageCon.redirectToMW(); 
         VFC_WorkOrderConnector.formatURL(workOrder,Label.CLDEC12SRV19); 
         workOrder.BackOfficeSystem__c = null;
         update workOrder;
         ApexPages.currentPage().getParameters().put('action', Label.CLDEC12SRV19);
         myPageCon.redirectToMW();
    }
    
}