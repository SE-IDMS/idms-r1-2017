@isTest
private class VFC_AddRPDTProduct_TEST
{
    static testMethod void testVFC_AddRPDTProduct() 
    {
  		Country__c country = Utils_TestMethods.createCountry();
        insert country;

       
         BusinessRiskEscalationEntity__c RCorg1 = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = Country.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
        Insert RCorg1;  
        
        BusinessRiskEscalationEntity__c RCorg2 = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = Country.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
        Insert RCorg2;
        
        BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = Country.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
        Insert RCorg;  
        
        RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = Country.Id,
                                 Capability__c = 'Repair defective product',
                                 CommercialReference__c='xbtg5230',
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c='02BF6D',
                                 BusinessUnit__c = 'BusinessUnit__c',
                                 ProductLine__c = 'ProductLine__c',
                                 TECH_ProductFamily__c = 'TECH_ProductFamily__c',
                                 Family__c='Family__c'
                                 );
        Insert RPDT;
        
        //String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        OPP_Product__c oppPdct = new OPP_Product__c(Name = 'Name',
                                TECH_PM0CodeInGMR__c='GMR business__test',
                                IsActive__c = true
                                );
        insert oppPdct;
        
        //static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares)
       List<Object> rs8 = VFC_AddRPDTProduct.remoteSearch('searchString','gmrcode1',true,true,true,true,true);
        List<Object> rs6 = VFC_AddRPDTProduct.remoteSearch('searchString','gmrcod',true,true,true,true,true);
         List<Object> rs4 = VFC_AddRPDTProduct.remoteSearch('searchString','gmrc',true,true,true,true,true);
          List<Object> rs2 = VFC_AddRPDTProduct.remoteSearch('searchString','gm',true,true,true,true,true);
          
          // global static String getSearchText()
          string str =  VFC_AddRPDTProduct.getSearchText();
          
          //global static List<OPP_Product__c> getOthers(String business) 
        List<OPP_Product__c> listopp = VFC_AddRPDTProduct.getOthers('business');
          
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(RPDT);   
        VFC_AddRPDTProduct GMRSearchPage = new VFC_AddRPDTProduct(CaseStandardController );
        
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();
        GMRSearchPage.jsonSelectString=jsonSelectString;
       // GMRSearchPage.Mode='ordered';
        Test.startTest();
        PageReference pageRef = Page.VFP_AddRPDTProduct;
        pageRef.getParameters().put('jsonSelectString',jsonSelectString);
         pageRef.getParameters().put('Mode','ordered');
        Test.setCurrentPage(pageRef);
        //GMRSearchPage.ChangeProduct = true;
        GMRSearchPage.performSelect();
        GMRSearchPage.performSelectFamily();
        GMRSearchPage.pageCancelFunction();
       
       
       Test.stopTest(); 
    }
}