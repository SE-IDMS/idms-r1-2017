public with sharing class PRJ_ManageBudgetLines {

    public Map<String, Map<Integer,PRJ_BudgetLine__c>> budgetLinePerTypePerYear {get;set;}

    public Budget__c oBudget {get;set;}
    
    public ApexPages.StandardController controller;
    
    public List<PRJ_BudgetLine__c> budgetLines {get;set;}  
    
    private Integer CurrentYear;
    
    private String startYear;
    
    public String mapkey{get;set;}{mapkey='';}
    
    public List<String> alphaTypes {get; set;}
    
    public Map<string,string> colourMap {get;set;}
    
    public Set<String> budgetLineTypes {get;set;}
            
    public Map<String, String> tabindex {get;set;}
    
    public Set<String> yearsList {get;set;}
    public List<Integer> yearsInt {get;set;}
     List<string> lstBudgetFields {get;set;}
    
    //Added for the February, 2012 Release
    //begin
    public Map<String,String> pastbudgetFieldsMap{get;set;}
    {
        pastbudgetFieldsMap=new Map<String,String>();
        Map<String,DMTPastBudgetFields__c> myCustomSettingMap=DMTPastBudgetFields__c.getAll();
        for(String s:myCustomSettingMap.keySet()){
            pastbudgetFieldsMap.put(s,myCustomSettingMap.get(s).BudgetFieldAPIName__c);
            system.debug('value:' + s + myCustomSettingMap.get(s).BudgetFieldAPIName__c);
            system.debug('pastbudgetFieldsMap:' + pastbudgetFieldsMap);
            mapkey+=s+',';
            system.debug('mapkey:' + mapkey);
        }
            
            
    }    
    //end
    
    public Map<Integer,List<String>> keysforyear {get;set;}
    
    public Map<String,PRJ_BudgetLine__c> budgetlinesMap {get;set;}
    
    public PRJ_ManageBudgetLines (ApexPages.StandardController controller){ 
        System.debug('#### Entering Controller PRJ_ManageBudgetLines');
        this.controller = controller; 
        lstBudgetFields = new List<string>{'Payback__c','Type__c','CurrencyIsoCode','ProjectReq__c','Description__c','Year_Origin__c','PastBudgetAPACCumulative__c','PastBudgetCHINACumulative__c','PastBudgetNAMCumulative__c','PastBudgetISMACumulative__c','PastBudgetEUROPECumulative__c','PastBudBusCashoutOPEXOTCCumulative__c','PastBudBusinessRunningCostCumulative__c','PastBudgetASCECumulative__c','PastBudgetBusCashoutCAPEXCumulative__c','PastBudgetBusinessInternalCumulative__c','PastBudgetCashoutCAPEXCumulative__c','PastBudgetCashoutOPEXCumulative__c','PastBudgetGDCostsCumulative__c','PastBudgetPLImpactCumulative__c','PastBudgetRegionsCumulative__c','PastBudgetSmallEnhancementsCumulativ__c','PastBudIntChargedBacktoIPOCumulative__c','PastBudNonIPOITCostsIntCumulative__c','PastBudOperServicesCumulative__c','PastBudRunningITCostsImpCumulative__c'};
        System.debug('>>>>>>>>>lstBudgetFields'+lstBudgetFields);
        if(!Test.isRunningTest())   
        {
            controller.addFields(pastbudgetFieldsMap.values());  
            controller.addFields(lstBudgetFields);       
        }
        System.debug('>>>>>>>>>controller'+controller);
        oBudget = (Budget__c) controller.getRecord();
        alphaTypes = new List<String>();
        colourMap = new Map<String,String>();
        //By default, set the start year to the current year. In the following, this year will be compared
        //to the year of each budget line (if any) and be replaced if the current value is more recent.
        CurrentYear = datetime.now().year();        
        startYear  = CurrentYear.format().replace(',', '');
        startYear = startYear.trim();
        if (startYear.length()==5) {
            startYear = startYear.substring(0,1)+ startYear.substring(2);
        }
        //Obtains information related to the field Type__c of the object        
        Schema.DescribeFieldResult fieldResult = PRJ_BudgetLine__c.Type__c.getDescribe();
        //Retrieves the picklist values
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        budgetLineTypes = new Set<String>();
        for( Schema.PicklistEntry f : ple) {
            System.debug('brut de fonderie : ' + f.getValue());
            budgetLineTypes.add(f.getValue());
            alphaTypes.add(f.getValue());
            system.debug('alphaTypes:' + f.getValue());
        }
        
        for(String strType:alphaTypes)
        {
            for(DMTPastBudgetFields__c cs1:DMTPastBudgetFields__c.getAll().values())
            {
                if(!colourMap.containsKey(strType))
                    colourMap.put(cs1.Name,cs1.BackgroundColour__c);
            }
        }        
        mainInit();
        System.debug('#### Ending Controller PRJ_ManageBudgetLines');
    }
    
     private void mainInit(){ 
        Long starter=DateTime.now().getTime();
        yearsList = new Set<String>();
        yearsInt = new List<Integer>();
        keysforyear = new Map<Integer,List<String>>{};
        tabindex = new Map<String,String>();
        budgetLinePerTypePerYear = new Map<String, Map<Integer,PRJ_BudgetLine__c>>{};
        //Object Initialization 
        
        budgetlinesMap = new Map<String,PRJ_BudgetLine__c>{};
        
        budgetLines = [select id,name,Amount__c,Type__c,Year__c from PRJ_BudgetLine__c where Budget__c = :oBudget.Id limit 100]; //6 types multiplied by the number of years
        // oBudget=[select id,Year_Origin__c,ProjectReq__c,CurrencyISOCode,TotalPlannedCost__c,TotalCashoutCAPEX__c,TotalCashoutOPEX__c,TotalInternalCAPEX__c,TotalInternalOPEX__c,TotalSmallEnhancements__c, NewTotalCashoutCAPEX__c, NewTotalCashoutOPEX__c, NewTotalInternalCAPEX__c, NewTotalInternalOPEX__c, NewTotalSmallEnhancements__c,PastBudgetCashoutCAPEXCumulative__c,PastBudgetCashoutOPEXCumulative__c,PastBudgetInternalCAPEXCumulative__c,PastBudgetInternalOPEXCumulative__c,PastBudgetSmallEnhancementsCumulativ__c,PastBudgetGDCostsCumulative__c,PastBudgetRegionsCumulative__c,PastBudgetASCECumulative__c,PastBudOperServicesCumulative__c,PastBudNonIPOITCostsIntCumulative__c,PastBudRunningITCostsImpCumulative__c,PastBudgetPLImpactCumulative__c,PastBudgetBusCashoutCAPEXCumulative__c,PastBudBusCashoutOPEXOTCCumulative__c,PastBudBusinessRunningCostCumulative__c,PastBudgetBusinessInternalCumulative__c,PastBudIntChargedBacktoIPOCumulative__c,TotalITCostsInternal__c,TotalITCostsCashout__c from Budget__c where Id = :oBudget.Id];  
        oBudget=[select id,Year_Origin__c,ProjectReq__c,CurrencyISOCode,TotalPlannedCost__c,TotalCashoutCAPEX__c,TotalCashoutOPEX__c,TotalInternalCAPEX__c,TotalInternalOPEX__c,TotalSmallEnhancements__c, NewTotalCashoutCAPEX__c, NewTotalCashoutOPEX__c, NewTotalInternalCAPEX__c, NewTotalInternalOPEX__c, NewTotalSmallEnhancements__c,PastBudgetCashoutCAPEXCumulative__c,PastBudgetCashoutOPEXCumulative__c,PastBudgetNAMCumulative__c,PastBudgetISMACumulative__c,PastBudgetEUROPECumulative__c,PastBudgetCHINACumulative__c,PastBudgetAPACCumulative__c,PastBudgetInternalCAPEXCumulative__c,PastBudgetInternalOPEXCumulative__c,PastBudgetSmallEnhancementsCumulativ__c,PastBudgetGDCostsCumulative__c,PastBudgetRegionsCumulative__c,PastBudgetASCECumulative__c,PastBudOperServicesCumulative__c,PastBudNonIPOITCostsIntCumulative__c,PastBudRunningITCostsImpCumulative__c,PastBudgetPLImpactCumulative__c,PastBudgetBusCashoutCAPEXCumulative__c,PastBudBusCashoutOPEXOTCCumulative__c,PastBudBusinessRunningCostCumulative__c,PastBudgetBusinessInternalCumulative__c,PastBudIntChargedBacktoIPOCumulative__c,TotalITCostsInternal__c,TotalITCostsCashout__c,NewTotalRegions__c,NewTotalAppServicesCustExp__c,NewTotalOperServices__c,NewTotalRunningITCostsImpact__c,NewTotalGDCosts__c,NewTotalPLImpact__c,NewTotalNonIPOITCostsInternal__c,NewTotalBusinessCashoutCAPEX__c,NewTotalBusinessRunningCostsImpact__c,NewTotalBusinessCashoutOPEXOTC__c,NewTotalBusinessInternal__c,NewTotalBusInternalChargedBacktoIPO__c from Budget__c where Id = :oBudget.Id];  
        String tmpYear;
        for (PRJ_BudgetLine__c bl : budgetLines) {
            tmpYear = bl.Year__c;
            if (tmpYear.length() == 5) {
                tmpYear = tmpYear.substring(0,1) + tmpYear.substring(2);
            }
            //Checks if the year of the budget line record is older than the current one and replace it if it is the case
            if (Integer.valueOf(tmpYear) < Integer.valueOf(startYear)) {
                startYear = tmpYear;
            } 
            if (!budgetlinesMap.containsKey(bl.Type__c + tmpYear)) {
                System.debug('----------'+bl.Type__c + tmpYear+ ' '+bl);
                budgetlinesMap.put(bl.Type__c + tmpYear, bl);
            }                
        }
        
        generateKeysBasedOnStartYear();
        
        for (String keyone : budgetLineTypes) {
            for (String keytwo : yearsList){
                    if (!budgetlinesMap.containsKey(keyone+keytwo)) {
                        Integer FY_Index = Integer.valueOf(keytwo)-CurrentYear;
                        PRJ_BudgetLine__c tmp = new PRJ_BudgetLine__c(Budget__c = oBudget.Id,Type__c=keyone,Year__c=keytwo,CurrencyISOCode=oBudget.CurrencyISOCode,TECH_FY_Index__c=FY_Index,Amount__c = 0);
                        System.Debug('Type: ' + keyone + ',year: ' + keytwo);                        
                        budgetlinesMap.put(keyone+keytwo, tmp);
                    }
                    budgetLinePerTypePerYear.put(keyone, null);
                }
            }
            Long ending=DateTime.now().getTime()- starter;
            System.Debug('****************Execution time = ' + ending +  'ms');
     }
     /*
     private List<PRJ_BudgetLine__c> completeMissingTypesInList(List<PRJ_BudgetLine__c> elements) {
        System.debug('#### Entering completeMissingTypesInList');
        List<PRJ_BudgetLine__c> updatedElements = elements;
        Set<String> types= new Set<String>();
        String tmpYear = elements.get(0).Year__c;
        if (elements!=null) {    
            for( String f : budgetLineTypes) {
                types.add(f);
            }
            for (PRJ_BudgetLine__c bl : budgetLines) {
                types.remove(bl.Type__c);
            }
            for (String ty : types) {
                PRJ_BudgetLine__c tmp = new PRJ_BudgetLine__c(Budget__c = oBudget.Id,Type__c=ty,Year__c=tmpYear,CurrencyISOCode=oBudget.CurrencyISOCode);
                updatedElements.add(tmp);
            }    
        }
        System.debug('#### Ending completeMissingTypesInList');
        return updatedElements;
     } 
    */
    
    //Seperate button to calculate  P & L Functionality-- July 2013 release -- By Priyanka Shetty(Schneider Electric)
  public pagereference updatePLfields(){
        System.debug('#### Entering updatePLfields');
        List<PRJ_BudgetLine__c> toUpsert = new List<PRJ_BudgetLine__c>();
       
        for (String key : budgetlinesMap.keySet()) {
             PRJ_BudgetLine__c bls = budgetlinesMap.get(key);
                System.debug('Entered for loop of updateingPFfields for loop>>>>>>');
             if(bls.Amount__c == null && !key.contains('IT P & L Cost Impact')) 
             {            
                 bls.Amount__c = 0;
             } 
            /* 
            a) Automated functionality to work ONLY when P & L fields are cleared.
            P&L Impact Y0 = OTC Y0 + Run Y0 + SME Y0
            P&L Impact Y1 = OTC Y1 + Run Y1 + SME Y1 + CAPEX Y0/3
            P&L impact Y2 = IOTC Y2 + Run Y2 + SME Y2 + CAPEX Y0/3 + CAPEX Y1/3
            P&L impact Y3 =  OTC Y3 + Run Y3 + SME Y3 + CAPEX Y0/3 + CAPEX Y1/3 + CAPEX Y2/3
            */

            Try{
                Integer currentYear = Integer.Valueof(oBudget.Year_Origin__c);
                Decimal pLAmountCY =budgetlinesMap.get('IPO Internal Regions'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IPO Internal App Services / Cust Exp'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IPO Internal Technology Services'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IPO Internal GD Costs'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IT Costs Internal Non IPO'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IT Cashout Small Enhancements OTC'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IT Cashout OPEX OTC'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IT Cashout Running Costs Impact'+ currentYear).Amount__c  ;
                                    
                Decimal pLAmountCY1 = budgetlinesMap.get('IPO Internal Regions'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IPO Internal App Services / Cust Exp'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IPO Internal Technology Services'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IPO Internal GD Costs'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IT Costs Internal Non IPO'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Small Enhancements OTC'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IT Cashout OPEX OTC'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Running Costs Impact'+ (currentYear +1)).Amount__c  +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ currentYear).Amount__c/3 ;
                                    
                Decimal pLAmountCY2 = budgetlinesMap.get('IPO Internal Regions'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IPO Internal App Services / Cust Exp'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IPO Internal Technology Services'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IPO Internal GD Costs'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IT Costs Internal Non IPO'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Small Enhancements OTC'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IT Cashout OPEX OTC'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Running Costs Impact'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ currentYear).Amount__c/3 +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ (currentYear +1)).Amount__c/3 ;
                                    
                Decimal pLAmountCY3 = budgetlinesMap.get('IPO Internal Regions'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IPO Internal App Services / Cust Exp'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IPO Internal Technology Services'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IPO Internal GD Costs'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IT Costs Internal Non IPO'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Small Enhancements OTC'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IT Cashout OPEX OTC'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Running Costs Impact'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ currentYear).Amount__c/3 +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ (currentYear+1)).Amount__c/3 +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ (currentYear+2)).Amount__c/3 ;   
                
                
                if(key.contains('IT P & L Cost Impact'+currentYear)){   
                    //if(bls.Amount__c ==null)
                        bls.Amount__c = pLAmountCY;
                }
                if(key.contains('IT P & L Cost Impact'+ (currentYear +1))){ 
                   // if(bls.Amount__c ==null)
                        bls.Amount__c = pLAmountCY1;
                }
                if(key.contains('IT P & L Cost Impact'+(currentYear +2))){
                    //if(bls.Amount__c ==null )
                        bls.Amount__c = pLAmountCY2;
                }
                if(key.contains('IT P & L Cost Impact'+(currentYear +3))){  
                    //if(bls.Amount__c ==null )
                        bls.Amount__c = pLAmountCY3;
                } 
            }
            Catch(Exception e){
                System.debug('Error:'+e);
            }
            
            toUpsert.add(bls);         

        }
        List<Database.upsertResult> uResults = new List<Database.upsertResult>();
         uResults = Database.upsert(toUpsert,false);

         if(!uResults[0].isSuccess()){
              Database.Error [] err=uResults[0].getErrors();
               ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,err[0].getMessage());
                Apexpages.addMessage(msg);
          }        
        for(Database.upsertResult result:uResults) {
            if (result.isSuccess() && result.isCreated()) {
                System.debug('Record created or updated successfully : ' + result.getId());
            } else if (!result.isSuccess()){
                Database.Error err = result.getErrors()[0];
                System.debug('Record NOT deleted successfully : ' + err.getStatusCode()+' '+err.getMessage());
            }
        }
         mainInit();
         return null;
    }
    
    //END -- for July for 2013 release 
     public pagereference updateRegionFields(){
    System.debug('#### Entering updateRegionfields');
    List<PRJ_BudgetLine__c> toUpsert = new List<PRJ_BudgetLine__c>();
    for (String key : budgetlinesMap.keySet())
     {
             PRJ_BudgetLine__c bls = budgetlinesMap.get(key);
                System.debug('Entered for loop of updatingRegionfields for loop>>>>>>');
            if(bls.Amount__c == null && !key.contains('IPO Internal Regions')) 
            {            
                 bls.Amount__c = 0;
            } 
            try
            {
            Integer currentYear = Integer.Valueof(oBudget.Year_Origin__c);
            Decimal RegionAmountCY = budgetlinesMap.get('IPO Internal Regions APAC'+ currentYear).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions CHINA'+ currentYear).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions EUROPE'+ currentYear).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions ISMA'+ currentYear).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions NAM'+ currentYear).Amount__c ;
            
            Decimal RegionAmountCY1 = budgetlinesMap.get('IPO Internal Regions APAC'+ (currentYear+1)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions CHINA'+ (currentYear+1)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions EUROPE'+ (currentYear+1)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions ISMA'+ (currentYear+1)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions NAM'+(currentYear+1)).Amount__c ;
                                     
            Decimal RegionAmountCY2 = budgetlinesMap.get('IPO Internal Regions APAC'+ (currentYear+2)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions CHINA'+ (currentYear+2)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions EUROPE'+ (currentYear+2)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions ISMA'+ (currentYear+2)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions NAM'+(currentYear+2)).Amount__c ;

            Decimal RegionAmountCY3 =budgetlinesMap.get('IPO Internal Regions APAC'+ (currentYear+3)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions CHINA'+ (currentYear+3)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions EUROPE'+ (currentYear+3)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions ISMA'+ (currentYear+3)).Amount__c +
                                     budgetlinesMap.get('IPO Internal Regions NAM'+(currentYear+3)).Amount__c ; 
            if(key.contains('IPO Internal Regions'+currentYear)){   
                    //if(bls.Amount__c ==null)
                        bls.Amount__c = RegionAmountCY;
                }
                if(key.contains('IPO Internal Regions'+ (currentYear +1))){ 
                   // if(bls.Amount__c ==null)
                        bls.Amount__c = RegionAmountCY1;
                }
                if(key.contains('IPO Internal Regions'+(currentYear +2))){
                    //if(bls.Amount__c ==null )
                        bls.Amount__c = RegionAmountCY2;
                }
                if(key.contains('IPO Internal Regions'+(currentYear +3))){  
                    //if(bls.Amount__c ==null )
                        bls.Amount__c = RegionAmountCY3;
                }                        
            }
             Catch(Exception e){
                System.debug('Error:'+e);
            }
            toUpsert.add(bls);  
            
     }
     List<Database.upsertResult> uResults = new List<Database.upsertResult>();
         uResults = Database.upsert(toUpsert,false);

         if(!uResults[0].isSuccess()){
              Database.Error [] err=uResults[0].getErrors();
               ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,err[0].getMessage());
                Apexpages.addMessage(msg);
          }        
        for(Database.upsertResult result:uResults) {
            if (result.isSuccess() && result.isCreated()) {
                System.debug('Record created or updated successfully : ' + result.getId());
            } else if (!result.isSuccess()){
                Database.Error err = result.getErrors()[0];
                System.debug('Record NOT Created or Updated successfully : ' + err.getStatusCode()+' '+err.getMessage());
            }
        }
        mainInit();
    return null;
    }
    public pagereference updateBudgetLines(){
        System.debug('#### Entering updateBudgetLines');
        List<PRJ_BudgetLine__c> toUpsert = new List<PRJ_BudgetLine__c>();
        List<PRJ_BudgetLine__c> toDelete = new List<PRJ_BudgetLine__c>();
        Boolean flag;   
        //Populate the 2 lists of budget lines to upsert or delete
         
        //update oBudget;// By Srikant Joshi Jan Release 2013
         
        for (String key : budgetlinesMap.keySet()) {
             PRJ_BudgetLine__c bls = budgetlinesMap.get(key);
                System.debug('Entered for loop of updatebudgetlines>>>>>>');
             //Modified by Sreedevi Surendran (Schneider Electric)
             //start
             if(bls.Amount__c == null && !key.contains('IT P & L Cost Impact')) 
             {            
                 bls.Amount__c = 0;
             } 
            /* Added P & L New Functionality -- Nov Release 2012 -- By Srikant Joshi(Schneider Electric)
            a) Automated functionality to work ONLY when P & L fields are cleared.
            P&L Impact Y0 = OTC Y0 + Run Y0 + SME Y0
            P&L Impact Y1 = OTC Y1 + Run Y1 + SME Y1 + CAPEX Y0/3
            P&L impact Y2 = IOTC Y2 + Run Y2 + SME Y2 + CAPEX Y0/3 + CAPEX Y1/3
            P&L impact Y3 =  OTC Y3 + Run Y3 + SME Y3 + CAPEX Y0/3 + CAPEX Y1/3 + CAPEX Y2/3
            */
            //Commented for July 2013 release -- By Priyanka Shetty(Schneider Electric)
            /*
            Try{
                Integer currentYear = Integer.Valueof(oBudget.Year_Origin__c);
                Decimal pLAmountCY =budgetlinesMap.get('IPO Internal Regions'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IPO Internal App Services / Cust Exp'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IPO Internal Technology Services'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IPO Internal GD Costs'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IT Costs Internal Non IPO'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IT Cashout Small Enhancements OTC'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IT Cashout OPEX OTC'+ currentYear).Amount__c +
                                    budgetlinesMap.get('IT Cashout Running Costs Impact'+ currentYear).Amount__c  ;
                                    
                Decimal pLAmountCY1 = budgetlinesMap.get('IPO Internal Regions'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IPO Internal App Services / Cust Exp'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IPO Internal Technology Services'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IPO Internal GD Costs'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IT Costs Internal Non IPO'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Small Enhancements OTC'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IT Cashout OPEX OTC'+ (currentYear +1)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Running Costs Impact'+ (currentYear +1)).Amount__c  +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ currentYear).Amount__c/3 ;
                                    
                Decimal pLAmountCY2 = budgetlinesMap.get('IPO Internal Regions'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IPO Internal App Services / Cust Exp'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IPO Internal Technology Services'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IPO Internal GD Costs'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IT Costs Internal Non IPO'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Small Enhancements OTC'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IT Cashout OPEX OTC'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Running Costs Impact'+ (currentYear +2)).Amount__c +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ currentYear).Amount__c/3 +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ (currentYear +1)).Amount__c/3 ;
                                    
                Decimal pLAmountCY3 = budgetlinesMap.get('IPO Internal Regions'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IPO Internal App Services / Cust Exp'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IPO Internal Technology Services'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IPO Internal GD Costs'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IT Costs Internal Non IPO'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Small Enhancements OTC'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IT Cashout OPEX OTC'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IT Cashout Running Costs Impact'+ (currentYear +3)).Amount__c +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ currentYear).Amount__c/3 +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ (currentYear+1)).Amount__c/3 +
                                    budgetlinesMap.get('IT Cashout CAPEX'+ (currentYear+2)).Amount__c/3 ;   
                
                
                if(key.contains('IT P & L Cost Impact'+currentYear)){   
                    if(bls.Amount__c ==null)
                        bls.Amount__c = pLAmountCY;
                }
                if(key.contains('IT P & L Cost Impact'+ (currentYear +1))){ 
                    if(bls.Amount__c ==null)
                        bls.Amount__c = pLAmountCY1;
                }
                if(key.contains('IT P & L Cost Impact'+(currentYear +2))){
                    if(bls.Amount__c ==null )
                        bls.Amount__c = pLAmountCY2;
                }
                if(key.contains('IT P & L Cost Impact'+(currentYear +3))){  
                    if(bls.Amount__c ==null )
                        bls.Amount__c = pLAmountCY3;
                } 
            }
            Catch(Exception e){
                System.debug('Error:'+e);
            }
            // END -- Nov Release P & L Functionality.
            */ 
            //END-- Commented for July for 2013 release 
            toUpsert.add(bls);         
            

            /*                                        
             if (bls.Amount__c >= 0) {
                toUpsert.add(bls);
             } else if ((bls.Amount__c == 0 || bls.Amount__c ==null) && bls.Id != null) {
                toDelete.add(bls);
             } else {
                toUpsert.add(bls);
             } 
            */
            //end   
       }
         
             
       
        /* Perform the upsert. In this case the unique identifier for the
           insert or update decision is the Salesforce record ID. If the 
           record ID is null the row will be inserted, otherwise an update
           will be attempted. */  
        //Upsert records
          
        /*  
        //Modified By Santhanam Subramaniam during the DMT Migration to bFO on 11/17/2011
        //Code Starts  
          List<Database.upsertResult> uResults = new List<Database.upsertResult>();
         // yearsInt.sort();
           Integer Minyr = yearsInt[0];

        for(Integer i=0;i < yearsInt.size();i++)
        { 
        If(yearsInt[i] < Minyr){
            Minyr  = yearsInt[i];
        }
        }
          system.debug('#########'+ toUpsert[0].Year__c);
          //Bug fix by Sreedevi Surendran(Schneider Electric) for December, 2011 Release
          //begin
          if( Minyr >= CurrentYear)
          {
          uResults = Database.upsert(toUpsert,false); 
          }else{
              ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'This budget is locked and cannot be edited.');
                Apexpages.addMessage(msg);  
            }  
         //end
         // Code Ends */
         // Modified by Srikant Joshi for DEC Release 2012, Commented the above Code as Locking the budget functionality is decided to be cancelled.
         List<Database.upsertResult> uResults = new List<Database.upsertResult>();
         uResults = Database.upsert(toUpsert,false);
         // Added Code Ramakrishna Singara(Schneider Electric) for January Hot Fix Release
          // Start
         if(!uResults[0].isSuccess()){
              Database.Error [] err=uResults[0].getErrors();
               ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,err[0].getMessage());
                Apexpages.addMessage(msg);
          }
          //End
         // Code Ends
         
        for(Database.upsertResult result:uResults) {
            if (result.isSuccess() && result.isCreated()) {
                System.debug('Record created or updated successfully : ' + result.getId());
            } else if (!result.isSuccess()){
                Database.Error err = result.getErrors()[0];
                System.debug('Record NOT deleted successfully : ' + err.getStatusCode()+' '+err.getMessage());
            }
        }
        //Delete records
        if (!toDelete.isEmpty()) {
            Database.DeleteResult[] DR_Dels = Database.delete(toDelete);
            for(Database.DeleteResult result:DR_Dels) {
                if (result.isSuccess()) {
                    System.debug('Record deleted successfully : ' + result.getId());
                } else {
                    Database.Error err = result.getErrors()[0];
                    System.debug('Record NOT deleted successfully : ' + err.getStatusCode()+' '+err.getMessage());
                }
            }
        }
        mainInit();
        System.debug('#### Ending updateBudgetLines');        

      return null;
      }

    /**
    * Method managing the back button to the related/parent budget
    **/      
    public pagereference back(){
        PageReference pageRef = new PageReference('/'+oBudget.ProjectReq__c);
        pageRef.setRedirect(true);
        return pageRef;
    }
      
    /**
    * Populates a Map whose the Key is a Year and the value is a list of strings representing
    * all the keys used in the MAP containing the budget values per year
    * Remark : the format of the key is "'budget_line_type' + 'year'"
    **/
    private void generateKeysBasedOnStartYear() {
        List<String> concat = new List<String>();
        Integer iYear = Integer.valueOf(startYear.trim());
        Integer calcYear = 0;
        String tmpYear = null;
        Integer cnt = 1;
        for (integer i = 0;i<PRJ_Config__c.getInstance().NumberOfYearsForBudget__c;i++) {
            concat = new List<String>();
            calcYear = iYear + i;
            tmpYear = (calcYear).format().replace(',','');
            if (tmpYear.length()==5) {
                tmpYear = tmpYear.substring(0,1)+ tmpYear.substring(2);
            }
            yearsList.add(tmpYear);
            yearsInt.add(calcYear);
            for (String subkey : alphaTypes){
                tabindex.put(subkey+tmpYear, cnt.format());
                concat.add(subkey+tmpYear);
                cnt++;              
            }
            keysforyear.put(calcYear, concat);
        }
      }
}