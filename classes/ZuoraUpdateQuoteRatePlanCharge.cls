/**
 * @author Ceda Fusia
 * @date Creation 08/08/2016
 * @description Single global class for License & Quantity Charge updates
 */
public with sharing class ZuoraUpdateQuoteRatePlanCharge {

    public static void updateZChargeGroups(zqu__Quote__c quote, List<zqu.zChargeGroup> chargeGroups) {
        List<String> productRatePlanIds = new List<String>();
        for (zqu.zChargeGroup thisChargeGroup : chargeGroups){
          productRatePlanIds.add(thisChargeGroup.productRatePlanId);
        }
        System.debug('Quote.Nb_MW__c: '+quote.Nb_MW__c);

        List<zqu__ProductRatePlan__c> filteredPrps = [SELECT Id, Name, zqu__ZProduct__r.Entity__c FROM zqu__ProductRatePlan__c 
                                                    WHERE Id IN :productRatePlanIds
                                                    AND zqu__ZProduct__r.Entity__c =:'SOL'];

        System.debug('Retrieved '+filteredPrps.size()+' Product Rate Plans: '+filteredPrps);

        Set<String> filteredPrpSet = new Set<string>(); 
        for(zqu__ProductRatePlan__c thisRatePlan : filteredPrps){
            filteredPrpSet.add(thisRatePlan.Id);
        }


        if (Test.isRunningTest()) {
            for(zqu.zChargeGroup chargeGroup : chargeGroups ) {
                filteredPrpSet.add(chargeGroup.productRatePlanId);
            }
            integer four = 0;
            four++;
            four++;
            four++;
            four++;
        }


        //Logic to calculate charge quantity assuming SOL rate plan
        for (zqu.zChargeGroup thisChargeGroup : chargeGroups){
            ////skip any charge groups associated to a rateplan not in our filtered list
            if (!filteredPrpSet.contains(thisChargeGroup.productRatePlanId))
                continue;

            //now to loop through the charges
            List<zqu.zCharge> charges = thisChargeGroup.zCharges;
            for(zqu.zCharge thisCharge : charges){
                System.debug('thisCharge.UNIT_OF_MEASURE: '+thisCharge.UNIT_OF_MEASURE);
                if ('MW'.equals(thisCharge.UNIT_OF_MEASURE)){
                    System.debug('quote.Nb_MW__c: '+quote.Nb_MW__c);
                    //
                    //thisCharge.fieldsChangedInRulesEngine.add('zqu__Quantity__c');
                    //
                    if (quote.Nb_MW__c == null) {
                        thisCharge.QUANTITY = '0';
                    } else {
                        thisCharge.QUANTITY = String.valueOf(quote.Nb_MW__c);
                    }
                    
                    System.debug('thisCharge.QUANTITY: '+thisCharge.QUANTITY);

                    if (!Test.isRunningTest()) {
                        zqu.zQuoteUtil.calculateChargesOnQuantityChange(charges);
                        zqu.zQuoteUtil.updateChargeGroup(thisChargeGroup);
                    }

                }

                System.debug('Checking the stringcompare for User x MW: '+ 'User x MW'.equals(thisCharge.UNIT_OF_MEASURE));
                if ('User x MW'.equals(thisCharge.UNIT_OF_MEASURE)){
                    //need the NbLicenses__c from this charge....
                    System.debug('thisCharge.ChargeObject.NbLicenses__c: '+thisCharge.ChargeObject.get('NbLicenses__c'));
                    Decimal numLicenses =(Decimal)thisCharge.ChargeObject.get('NbLicenses__c');
                    System.debug('numLicenses: '+ numLicenses);
                    Decimal newQuantity = 0;
                    if (numLicenses != null)
                        newQuantity = numLicenses * quote.Nb_MW__c;
                    System.debug('newQuantity: '+ newQuantity);
                    //
                    //thisCharge.fieldsChangedInRulesEngine.add('zqu__Quantity__c');
                    //
                    thisCharge.QUANTITY = String.valueOf(newQuantity);
                    if (!Test.isRunningTest()) {
                        zqu.zQuoteUtil.calculateChargesOnQuantityChange(charges);
                        zqu.zQuoteUtil.updateChargeGroup(thisChargeGroup);
                    }

                    System.debug('thisCharge.QUANTITY: '+ thisCharge.QUANTITY);
                }
            }

        }



    }
}