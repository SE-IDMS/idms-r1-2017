@isTest
public class BatchSRVWorkOrderIPAssignment_Test {
    
    static testMethod void TestOne() {
    
        String WorkDetailRT = System.Label.CLAPR15SRV60;
        Product2 p2= new Product2(Name='testprod');
         insert p2;
        
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(Shipped_Serial_Number__c='120',SVMXC__Product__c=p2.id,SVMXC__Component__c=null); 
        insert wo;
                test.starttest();
        SVMXC__Service_Order_Line__c wd= new SVMXC__Service_Order_Line__c(Shipped_Serial_Number__c='100',SVMXC__Product__c=p2.id,RecordTypeId =WorkDetailRT,SVMXC__Service_Order__c =wo.id);
        insert wd;
        SVMXC__Installed_Product__c  ip= new SVMXC__Installed_Product__c (Name='testip',SVMXC__Serial_Lot_Number__c=wd.Shipped_Serial_Number__c,SVMXC__Product__c =p2.id);
        insert ip;
        
        
        BatchSRVWorkOrderIPAssignment SreBatch = New BatchSRVWorkOrderIPAssignment();
           Database.executeBatch(SreBatch);
        
        string CORN_EXP = '0 0 0 1 4 ?';
        string CORN_EXPS = '0 0 0 1 4 ?';
        
        BatchSRVWorkOrderIPAssignment ipbatch2 = new BatchSRVWorkOrderIPAssignment ();
        string jobid = system.schedule('my batch job', CORN_EXP, new BatchSRVWorkOrderIPAssignment());
        CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
        test.stoptest();
        
    }

}