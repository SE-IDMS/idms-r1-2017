// Interface providing methods depending from the context of use
public interface Utils_OrderMethods
{
   void Init(VCC07_OrderConnectorController PageController);
   pagereference Cancel(VCC07_OrderConnectorController PageController);
   pagereference PerformAction(VCC07_OrderConnectorController PageController);
}