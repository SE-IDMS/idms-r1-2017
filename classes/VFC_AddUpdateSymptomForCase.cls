global with sharing class VFC_AddUpdateSymptomForCase{ 
    
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public Case caseRecord{get;set;}
    
    public VFC_AddUpdateSymptomForCase(ApexPages.StandardController controller){
        caseRecord=(Case)controller.getRecord();                        
        if(caseRecord.id!=null){
            caseRecord=[Select Status, SupportCategory__c, CaseSymptom__c, CaseSubSymptom__c, CaseOtherSymptom__c , TECH_GMRCode__c, CheckedBYSymptom__c, TECH_LaunchSymptom__c from Case where id=:caseRecord.Id];
            if(caseRecord.SupportCategory__c!=null)
                pageParameters.put('supportCategory',caseRecord.SupportCategory__c);            
            if(caseRecord.CaseSymptom__c!=null)
                pageParameters.put('caseSymptom',caseRecord.CaseSymptom__c);            
            if(caseRecord.CaseSubSymptom__c!=null)
                pageParameters.put('caseSubSymptom',caseRecord.CaseSubSymptom__c);            
            if(caseRecord.CaseOtherSymptom__c!=null)
                pageParameters.put('caseOtherSymptom',caseRecord.CaseOtherSymptom__c);            
            if(caseRecord.TECH_GMRCode__c!=null)
                pageParameters.put('GMRCode',caseRecord.TECH_GMRCode__c);
            if(caseRecord.Status!=null)
                pageParameters.put('caseStatus',caseRecord.Status);    
            
            String strGMRCode='';
            if(pageParameters.containsKey('GMRCode'))
                strGMRCode = pageParameters.get('GMRCode');
            if(strGMRCode == '') {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,System.Label.CL00752));
            }
            if(caseRecord.Status=='Closed' || caseRecord.Status=='Cancelled' ){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,System.Label.CLAPR14CCC21));
            }            
        }        
    }
    
     
    @RemoteAction
    global static List<String> getCaseSymptom(String strGMRCode, String strCategory) {
        Set<String> setSymptom = new Set<String>();
        List<String> lstSortedSymptom = new List<String>();
        Boolean blnIsOtherSymptomAvailable =false;
        List<Symptom__c> lstSymptom = new List<Symptom__c>([Select Symptom__c from Symptom__c where GMRCode__c =:strGMRCode AND IsActive__c =:TRUE Order by Symptom__c]);
        for(Symptom__c objSymptom: lstSymptom){
            if(objSymptom.Symptom__c.trim()=='Other')
                blnIsOtherSymptomAvailable=true;
            else
                setSymptom.add(objSymptom.Symptom__c);
        }
        lstSortedSymptom.addAll(setSymptom);
        lstSortedSymptom.sort();    
        if(blnIsOtherSymptomAvailable)
            lstSortedSymptom.add('Other');
        return lstSortedSymptom;       
    }
    
    @RemoteAction    
    global static List<String> getCaseSubSymptom(String strGMRCode, String strCategory, String strCaseSymptom) {
        Set<String> setSubSymptom = new Set<String>();
        List<String> lstSortedSubSymptom = new List<String>();
        List<Symptom__c> lstSubSymptom =  new List<Symptom__c>([Select SubSymptom__c from Symptom__c where GMRCode__c =:strGMRCode AND Symptom__c =: strCaseSymptom AND IsActive__c =:TRUE Order by SubSymptom__c]);
        for(Symptom__c objSymptom: lstSubSymptom){
            setSubSymptom.add(objSymptom.SubSymptom__c);
        }
        lstSortedSubSymptom.addAll(setSubSymptom);
        lstSortedSubSymptom.sort();    
        return lstSortedSubSymptom;
        
    }
    
// Updated: Sukumar Salla - 04AUG2014 - OCT2014 Release: BR-4945
    // Query Symptom object to get the PotentialMajorIssue value and assign it back on Case
    @RemoteAction
    global static Database.SaveResult updateCaseSymptom(Id caseId,String strCaseSymptom,String strCaseSubSymptom,String strCaseOtherSymptom,String strGMRCode,String strCategory){
        Case objUpdateCase = new Case(Id = caseId);
        List<Symptom__c> cmiSymptom = new List<Symptom__c>([Select PotentialMajorIssue__c from Symptom__c where GMRCode__c =:strGMRCode AND Symptom__c =: strCaseSymptom AND SubSymptom__c =: strCaseSubSymptom AND IsActive__c =:TRUE]);
        objUpdateCase.CaseSymptom__c = strCaseSymptom;
        objUpdateCase.CaseSubSymptom__c = strCaseSubSymptom;
        objUpdateCase.CaseOtherSymptom__c = strCaseOtherSymptom;
        if(cmiSymptom.size()>0){
        objUpdateCase.TECH_IsSymptomMajor__c = cmiSymptom[0].PotentialMajorIssue__c;}
        Database.SaveResult objUpdateResult = Database.update(objUpdateCase,true);
        return objUpdateResult;
    }
}