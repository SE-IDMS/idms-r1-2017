/*
    Author          : Siddharatha Nagavarapu (Goblal Delivery Team)
    Date Created    : 03/11/2010
    Description     : Test class for batch apex BATCH04_CVKPIDeleteDailyRecords
*/
@isTest
private class BATCH04_CVKPIDeleteDailyRecords_TEST{

    static testMethod void myUnitTest() {
    //create a cap record with the condition
    User runAsUser = Utils_TestMethods.createStandardUser('tst1');
    runAsUser.BypassVR__c=True;
    runAsUser.CAPRelevant__c=True;
    insert runAsUser;
    System.runAs(runAsUser){
        Test.startTest();              
        CustomerVisitKPI__c kpirecord=new CustomerVisitKPI__c();
        kpirecord.isMonthlyRecord__c=False;
        kpirecord.date__c=System.Today().addDays((System.Today().day() -1)*-1).addMonths(-3);        
        insert kpirecord;                
        BATCH04_CVKPIDeleteDailyRecords customerVisitBatch=new BATCH04_CVKPIDeleteDailyRecords();
        customerVisitBatch.createBatches();
        Test.stopTest();
       }

    }
}