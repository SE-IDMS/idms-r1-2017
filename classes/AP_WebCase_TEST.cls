/*
Class Name       : AP_WebCase_TEST
Purpose          : Test Class for testing Web Case creation
Created by       : Rakhi Kumar(Global Delivery Team)
Date created     : 13 Apr 2013
-----------------------------------------------------------------
                              MODIFICATION HISTORY
-----------------------------------------------------------------
Modified By     : <>
Modified Date   : <>
Description     : <>
-----------------------------------------------------------------
*/
@isTest
private class AP_WebCase_TEST {
    static testMethod void testWebCaseContactIdentification() {
        //May 2013 Release - Test method updateContact(To update the Case Contact when the case originates from web)
        list<Contact> newContacts = new list<Contact>();
        list<Case> newCases = new list<Case>();
        
        //Create Account
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount; 
         
        //Create Contact with Phone number 9999999991
        Contact contactWithPhone1 = Utils_TestMethods.createContact(testAccount.Id, 'Contact-9999999991');
        contactWithPhone1.MobilePhone = '9999999991';
        newContacts.add(contactWithPhone1);
        
        //Create Contact with Phone number 9999999992
        Contact contactWithPhone2 = Utils_TestMethods.createContact(testAccount.Id, 'Contact-9999999992');
        contactWithPhone2.MobilePhone = '9999999992';
        newContacts.add(contactWithPhone2);
        
        //Create another contact with Phone number 9999999992
        Contact contactWithPhone3 = Utils_TestMethods.createContact(testAccount.Id, 'Contact-9999999992');
        contactWithPhone3.MobilePhone = '9999999992';
        newContacts.add(contactWithPhone3);
        
        //Create Contact with Email
        Contact contactWithEmail = Utils_TestMethods.createContact(testAccount.Id, 'ContactWithEmail');
        contactWithEmail.Email = 'test@test.com';
        newContacts.add(contactWithEmail);
        
        insert newContacts;
        
        Test.startTest();
        Id[] fixedSearchResults =  new Id[2];
        
        System.Debug('##################           Unit Test 1 - Begin          ##################');
        //Test case creation with origin as web and Supplied phone to identify contact
        Case webCase1 = Utils_TestMethods.createCase(testAccount.Id, contactWithPhone1.Id, 'Open');
        fixedSearchResults.add(contactWithPhone1.Id);
        Test.setFixedSearchResults(fixedSearchResults);
        webCase1.Origin = 'Web';
        webCase1.SuppliedPhone = '9999999991';
        webCase1.Contact = null;
        webCase1.CustomerRequest__c = 'WebCase Test 1';
        newCases.add(webCase1);
        AP10_CaseTrigger.updateContact(newCases);
        
        //Test case creation with origin as web and Supplied phone to identify contact
        Case webCase2 = Utils_TestMethods.createCase(testAccount.Id, contactWithPhone1.Id, 'Open');
        fixedSearchResults.add(contactWithEmail.Id);
        Test.setFixedSearchResults(fixedSearchResults);
        webCase2.Origin = 'Web';
        webCase2.SuppliedPhone = '9999999992';
        webCase2.Contact = null;
        webCase2.CustomerRequest__c = 'WebCase Test 2';
        newCases.clear();
        newCases.add(webCase2);
        
        
        //Test case creation with origin as web and Supplied email to identify contact
        Case webCase3 = Utils_TestMethods.createCase(testAccount.Id, contactWithPhone1.Id, 'Open');
        fixedSearchResults.clear();
        fixedSearchResults.add(contactWithPhone1.Id);
        webCase3.Origin = 'Web';
        webCase3.SuppliedEmail = 'test@test.com';
        webCase3.Contact = null;
        webCase3.CustomerRequest__c = 'WebCase Test 3';
        newCases.clear();
        newCases.add(webCase3);
        
        Test.stopTest();
    }
}