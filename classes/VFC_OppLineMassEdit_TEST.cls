@isTest
private class VFC_OppLineMassEdit_TEST{
     static TestMethod void plMassEditControllerTEST(){    
         Account account1 = Utils_TestMethods.createAccount();
         insert account1;
         Opportunity TestOpp = Utils_TestMethods.createOpenOpportunity(account1.Id);
         insert TestOpp;
         OPP_ProductLine__c pl = new OPP_ProductLine__c();
         pl.opportunity__c = TestOpp.id;
         insert pl;
                 
         List<OPP_ProductLine__c> TestPLList = new List<OPP_ProductLine__c>();
         TestPLList.add(pl);
         ApexPages.StandardSetController PLSetController = new ApexPages.StandardSetController(TestPLList);
         PLSetController.setSelected(TestPLList);
         VFC_OppLineMassEdit VFController = new VFC_OppLineMassEdit(PLSetController);
         VFController.CustomSave();
         
         /*Profile profile = [select id from profile where name='SE - Salesperson'];        
         User user = new User(alias = 'user', email='user' + '@accenture.com', emailencodingkey='UTF-8', 
         lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = profile.Id, 
         BypassWF__c = false, timezonesidkey='Europe/London', username='user' + '@bridge-fo.com'); 
         insert user;*/
         User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        User user;
        System.runAs(sysadmin2){
            UserandPermissionSets useruserandpermissionsetrecord=new UserandPermissionSets('Users','SE - Salesperson');
           user = useruserandpermissionsetrecord.userrecord;
            Database.insert(user);

            List<PermissionSetAssignment> permissionsetassignmentlstforuser=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:useruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforuser.add(new PermissionSetAssignment(AssigneeId=user.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforuser.size()>0)
            Database.insert(permissionsetassignmentlstforuser);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }
         
        AccountShare aShare = new AccountShare();
        aShare.accountId= account1.Id;
        aShare.UserOrGroupId = user.Id;
        aShare.accountAccessLevel = 'edit';
        aShare.OpportunityAccessLevel = 'read';
        aShare.CaseAccessLevel = 'edit';
        insert aShare; 
        
         system.runAs(user){ 
             VFController.CustomSave();
         }
     }
}