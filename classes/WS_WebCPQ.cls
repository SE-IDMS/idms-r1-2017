/*
Class : WS_WebCPQ
Purpose: Webservice connector to connect to webcpq interface
Author: Siddharth Nagavarapu(GD Solutions) 
*/

public class WS_WebCPQ {
    public class LinkOpportunityResponse_element {
        public WS_WebCPQ.LinkOpportunityResult_element LinkOpportunityResult;
        private String[] LinkOpportunityResult_type_info = new String[]{'LinkOpportunityResult','http://webcominc.com/','LinkOpportunityResult_element','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webcominc.com/','true','false'};
        private String[] field_order_type_info = new String[]{'LinkOpportunityResult'};
    }
    public class SearchQuotesFromSFResponse_element {
        public String SearchQuotesFromSFResult;
        private String[] SearchQuotesFromSFResult_type_info = new String[]{'SearchQuotesFromSFResult','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webcominc.com/','true','false'};
        private String[] field_order_type_info = new String[]{'SearchQuotesFromSFResult'};
    }
    public class NewQuoteFromSFResponse_element {
        public String NewQuoteFromSFResult;
        private String[] NewQuoteFromSFResult_type_info = new String[]{'NewQuoteFromSFResult','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webcominc.com/','true','false'};
        private String[] field_order_type_info = new String[]{'NewQuoteFromSFResult'};
    }
    public class xml_element {
        private String[] apex_schema_type_info = new String[]{'http://webcominc.com/','true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class performQuoteActionFromSFResponse_element {
        public String performQuoteActionFromSFResult;
        private String[] performQuoteActionFromSFResult_type_info = new String[]{'performQuoteActionFromSFResult','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webcominc.com/','true','false'};
        private String[] field_order_type_info = new String[]{'performQuoteActionFromSFResult'};
    }
    public class CpqApiSoap {
        public String endpoint_x = System.Label.CLJUN13SLS01;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://webcominc.com/', 'WS_WebCPQ'};
        public String SearchQuotesFromSF(String DomainName,String SFSessionId,String SFApiUrl,String SearchCriteriaXML) {
            WS_WebCPQ.SearchQuotesFromSF_element request_x = new WS_WebCPQ.SearchQuotesFromSF_element();
            WS_WebCPQ.SearchQuotesFromSFResponse_element response_x;
            request_x.DomainName = DomainName;
            request_x.SFSessionId = SFSessionId;
            request_x.SFApiUrl = SFApiUrl;
            request_x.SearchCriteriaXML = SearchCriteriaXML;
            Map<String, WS_WebCPQ.SearchQuotesFromSFResponse_element> response_map_x = new Map<String, WS_WebCPQ.SearchQuotesFromSFResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://webcominc.com/SearchQuotesFromSF',
              'http://webcominc.com/',
              'SearchQuotesFromSF',
              'http://webcominc.com/',
              'SearchQuotesFromSFResponse',
              'WS_WebCPQ.SearchQuotesFromSFResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.SearchQuotesFromSFResult;
        }
        public WS_WebCPQ.LinkOpportunityResult_element LinkOpportunity(String username,String password,String orderId,WS_WebCPQ.xml_element xml) {
            WS_WebCPQ.LinkOpportunity_element request_x = new WS_WebCPQ.LinkOpportunity_element();
            WS_WebCPQ.LinkOpportunityResponse_element response_x;
            request_x.username = username;
            request_x.password = password;
            request_x.orderId = orderId;
            request_x.xml = xml;
            Map<String, WS_WebCPQ.LinkOpportunityResponse_element> response_map_x = new Map<String, WS_WebCPQ.LinkOpportunityResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://webcominc.com/LinkOpportunity',
              'http://webcominc.com/',
              'LinkOpportunity',
              'http://webcominc.com/',
              'LinkOpportunityResponse',
              'WS_WebCPQ.LinkOpportunityResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.LinkOpportunityResult;
        }
        public String performQuoteActionFromSF(String domainName,String userName,String sessionId,String url,String compositeCartId,String opportunityId,String strDoc) {
            WS_WebCPQ.performQuoteActionFromSF_element request_x = new WS_WebCPQ.performQuoteActionFromSF_element();
            WS_WebCPQ.performQuoteActionFromSFResponse_element response_x;
            request_x.domainName = domainName;
            request_x.userName = userName;
            request_x.sessionId = sessionId;
            request_x.url = url;
            request_x.compositeCartId = compositeCartId;
            request_x.opportunityId = opportunityId;
            request_x.strDoc = strDoc;
            Map<String, WS_WebCPQ.performQuoteActionFromSFResponse_element> response_map_x = new Map<String, WS_WebCPQ.performQuoteActionFromSFResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://webcominc.com/performQuoteActionFromSF',
              'http://webcominc.com/',
              'performQuoteActionFromSF',
              'http://webcominc.com/',
              'performQuoteActionFromSFResponse',
              'WS_WebCPQ.performQuoteActionFromSFResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.performQuoteActionFromSFResult;
        }
        public String NewQuoteFromSF(String domainName,String sessionId,String sfAPIUrl,String newQuoteXML) {
            WS_WebCPQ.NewQuoteFromSF_element request_x = new WS_WebCPQ.NewQuoteFromSF_element();
            WS_WebCPQ.NewQuoteFromSFResponse_element response_x;
            request_x.domainName = domainName;
            request_x.sessionId = sessionId;
            request_x.sfAPIUrl = sfAPIUrl;
            request_x.newQuoteXML = newQuoteXML;
            Map<String, WS_WebCPQ.NewQuoteFromSFResponse_element> response_map_x = new Map<String, WS_WebCPQ.NewQuoteFromSFResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://webcominc.com/NewQuoteFromSF',
              'http://webcominc.com/',
              'NewQuoteFromSF',
              'http://webcominc.com/',
              'NewQuoteFromSFResponse',
              'WS_WebCPQ.NewQuoteFromSFResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.NewQuoteFromSFResult;
        }
    }
    public class NewQuoteFromSF_element {
        public String domainName;
        public String sessionId;
        public String sfAPIUrl;
        public String newQuoteXML;
        private String[] domainName_type_info = new String[]{'domainName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] sessionId_type_info = new String[]{'sessionId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] sfAPIUrl_type_info = new String[]{'sfAPIUrl','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] newQuoteXML_type_info = new String[]{'newQuoteXML','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webcominc.com/','true','false'};
        private String[] field_order_type_info = new String[]{'domainName','sessionId','sfAPIUrl','newQuoteXML'};
    }
    public class SearchQuotesFromSF_element {
        public String DomainName;
        public String SFSessionId;
        public String SFApiUrl;
        public String SearchCriteriaXML;
        private String[] DomainName_type_info = new String[]{'DomainName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] SFSessionId_type_info = new String[]{'SFSessionId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] SFApiUrl_type_info = new String[]{'SFApiUrl','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] SearchCriteriaXML_type_info = new String[]{'SearchCriteriaXML','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webcominc.com/','true','false'};
        private String[] field_order_type_info = new String[]{'DomainName','SFSessionId','SFApiUrl','SearchCriteriaXML'};
    }
    public class LinkOpportunityResult_element {
        private String[] apex_schema_type_info = new String[]{'http://webcominc.com/','true','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class performQuoteActionFromSF_element {
        public String domainName;
        public String userName;
        public String sessionId;
        public String url;
        public String compositeCartId;
        public String opportunityId;
        public String strDoc;
        private String[] domainName_type_info = new String[]{'domainName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] userName_type_info = new String[]{'userName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] sessionId_type_info = new String[]{'sessionId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] url_type_info = new String[]{'url','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] compositeCartId_type_info = new String[]{'compositeCartId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] opportunityId_type_info = new String[]{'opportunityId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] strDoc_type_info = new String[]{'strDoc','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webcominc.com/','true','false'};
        private String[] field_order_type_info = new String[]{'domainName','userName','sessionId','url','compositeCartId','opportunityId','strDoc'};
    }
    public class LinkOpportunity_element {
        public String username;
        public String password;
        public String orderId;
        public WS_WebCPQ.xml_element xml;
        private String[] username_type_info = new String[]{'username','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] password_type_info = new String[]{'password','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] orderId_type_info = new String[]{'orderId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] xml_type_info = new String[]{'xml','http://webcominc.com/','xml_element','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://webcominc.com/','true','false'};
        private String[] field_order_type_info = new String[]{'username','password','orderId','xml'};
    }
}