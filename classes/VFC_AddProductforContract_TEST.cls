@isTest(seeAllData=True)
private class VFC_AddProductforContract_TEST{

    static testMethod void AddUpdateProductforContractTestMethod() {
    //Start of Test Data preparation
        
        PackageTemplate__c objPackageTemplate = Utils_TestMethods.createPkgTemplate();
        Database.insert(objPackageTemplate);
        
        
        OPP_Product__c pdct = new OPP_Product__c(
                            Name='Name',
                            TECH_PM0CodeInGMR__c='GMR business__c',
                            IsActive__c =true
                            );
        insert pdct;
        
        ContractProductInformation__c objContractProductInfo = Utils_TestMethods.createContractProdInfo(objPackageTemplate.Id);

        ApexPages.StandardController controller = new ApexPages.StandardController(objPackageTemplate);
        VFC_AddProductforContract newGMRSearchForContract = new VFC_AddProductforContract(controller);
        
        List<WS_GMRSearch.resultFilteredDataBean> lst = new List<WS_GMRSearch.resultFilteredDataBean>();
        WS_GMRSearch.resultFilteredDataBean jsonSelectString = Utils_DummyDataForTest.FilteredSearch('BK500');
        
        WS_GMRSearch.resultFilteredDataBean result = new WS_GMRSearch.resultFilteredDataBean();
        List<WS_GMRSearch.gmrFilteredDataBean> lstFilteredDataBean = new List<WS_GMRSearch.gmrFilteredDataBean>();
        WS_GMRSearch.gmrFilteredDataBean objFilteredDataBean = new  WS_GMRSearch.gmrFilteredDataBean();

        WS_GMRSearch.labelValue objBU =new WS_GMRSearch.labelValue();
        objBU.label='BusinessUnit';
        objBU.value='00';

        WS_GMRSearch.labelValue objfamily =new WS_GMRSearch.labelValue();
        objfamily.label='FamilyName';
        objfamily.value='33';

        WS_GMRSearch.labelValue objProductGroup =new WS_GMRSearch.labelValue();
        objProductGroup.label='ProductGroup';
        objProductGroup.value='66';

        WS_GMRSearch.labelValue objProductLine =new WS_GMRSearch.labelValue();
        objProductLine.label='ProductLine';
        objProductLine.value='11';

        WS_GMRSearch.labelValue objProductSuccession =new WS_GMRSearch.labelValue();
        objProductSuccession.label='ProductSuccession';
        objProductSuccession.value='55';
        

        WS_GMRSearch.labelValue objProductFamily =new WS_GMRSearch.labelValue();
        objProductFamily.label='ProductFamily';
        objProductFamily.value='22';

        WS_GMRSearch.labelValue objSubFamily =new WS_GMRSearch.labelValue();
        objSubFamily.label='SubFamily';
        objSubFamily.value='44';
        
        WS_GMRSearch.labelValue objProductLine1 =new WS_GMRSearch.labelValue();
        objProductLine.label='businessLine';
        objProductLine.value='11';
        

        objFilteredDataBean.businessLine = objProductLine1;
        objFilteredDataBean.commercialReference = 'Name';
        objFilteredDataBean.description = 'Dummy Commerical Reference';
        objFilteredDataBean.family = objfamily;
        objFilteredDataBean.productGroup = objProductGroup;
        objFilteredDataBean.productLine = objProductLine;
        objFilteredDataBean.productSuccession = objProductSuccession;
        objFilteredDataBean.strategicProductFamily = objProductFamily;
        objFilteredDataBean.subFamily = objSubFamily;
        lstFilteredDataBean.add(objFilteredDataBean);
        //result.gmrFilteredDataBeanList = lstFilteredDataBean;
        
        system.debug('ANIL------->'+jsonSelectString);
        //lst.add(jsonSelectString);
        
        String jsonSelectString1= json.serialize(lstFilteredDataBean);
        Test.startTest();
        //newGMRSearchForContract.jsonSelectString=jsonSelectString1;
        PageReference pageRef=Page.VFP_AddProductforContract;
        pageRef.getParameters().put('jsonSelectString',jsonSelectString1);  
        Test.setCurrentPage(pageRef);
       
        Test.setMock(WebServiceMock.class, new WS_MOCK_GMRSearch_TEST());
        
        
        VFC_AddProductforContract.remoteSearch('12345','abcd1234',True,True,True,True,True);
        newGMRSearchForContract.jsonSelectString=jsonSelectString1;
        newGMRSearchForContract.performCheckbox();
        VFC_AddProductforContract.getOthers('business');
        newGMRSearchForContract.pageCancelFunction();
        
        Test.stopTest();
    }
}