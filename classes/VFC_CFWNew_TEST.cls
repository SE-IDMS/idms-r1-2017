/*
    Author          : Bhuvana Subramaniyan
    Description     : Test class for VFC_CFWNew .
*/


@isTest
private class VFC_CFWNew_TEST 
{
    static testMethod void testCFWNew()
    {
        test.startTest();
        CFWRiskAssessment__c cfwr = new CFWRiskAssessment__c();
        cfwr.ProjectName__c = 'testProjectName';
        cfwr.SubmittedforApproval__c = true;
        insert cfwr;
        ApexPages.StandardController sc1 = new ApexPages.StandardController(cfwr);
        VFC_CFWNew rskAssessment = new  VFC_CFWNew(sc1);
        rskAssessment.CreateCertification();        
        test.stopTest();
    }
}