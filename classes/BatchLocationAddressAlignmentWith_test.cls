@isTest
    private class BatchLocationAddressAlignmentWith_test {
        static testMethod void schedule_batch(){

            Test.StartTest();
            string CORN_EXP = '0 0 0 1 4 ?';
            BatchLocationAddressAlignmentWithAccount test1 = new BatchLocationAddressAlignmentWithAccount();
            string jobid = system.schedule('my batch job', CORN_EXP, new BatchLocationAddressAlignmentWithAccount() );
            
            CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
             Test.StopTest();
        }

}