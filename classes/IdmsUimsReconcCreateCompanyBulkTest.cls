@isTest
private class IdmsUimsReconcCreateCompanyBulkTest {

static testMethod void IdmsUimsReconcCreateCompany(){
         IdmsUimsReconcCreateCompanyBulk IdmsUimsReconcCreateCompanyBulkcls =new IdmsUimsReconcCreateCompanyBulk();   
         user userObj1 = new User(alias = 'user', email='test3127415' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                             timezonesidkey='Europe/London', username='test3127415' + '@accenture.com'+System.Label.CLJUN16IDMS71,Company_Postal_Code__c='12345',
                             Company_Phone_Number__c='9986995000',FederationIdentifier ='188b80b1-c622-4f20-8b1d-3ae86af11c4d',IDMS_Registration_Source__c = 'Test',
                             IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                             IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432', IDMS_VU_NEW__c = '1', IDMS_VU_OLD__c='1', IDMS_VC_NEW__c='1',IDMS_VC_OLD__c='1');
         insert userObj1;
         System.assertEquals(userObj1.email, 'test3127415' + '@accenture.com');
         System.assertEquals(userObj1.IDMS_User_Context__c , 'work');
         
         if(userObj1 != null){
         IDMSCompanyReconciliationBatchHandler__c handler=new IDMSCompanyReconciliationBatchHandler__c(AtCreate__c=true,AtUpdate__c=true,
                                                             HttpMessage__c='Test134',NbrAttempts__c=1,IdmsUser__c=userObj1.Id);
         insert handler; 
         
          System.assertEquals(handler.AtCreate__c, true);
        
         List<IDMSCompanyReconciliationBatchHandler__c> lstHandler=new List<IDMSCompanyReconciliationBatchHandler__c>();
         lstHandler.add(handler);
         String callFid = 'IDMSAdmin';
         
         try{
         Test.startTest();
             Test.setMock(WebServiceMock.class, new IdmsUsersResyncimplFlowServiceImsMock());
              Map<String, User> mapUimsEmailIdmsUser = new Map<String, User>();
              mapUimsEmailIdmsUser.put(userObj1.email, userObj1);
             IdmsUimsReconcCreateCompanyBulk.createUimsCompanies(lstHandler, 'batchId');
         }catch(exception e) {} 
         Test.stopTest();
         }
        }
         

    
    
   /* @testSetup static void setup() {
     List<IDMSCompanyReconciliationBatchHandler__c> lstHandler=new List<IDMSCompanyReconciliationBatchHandler__c>();
     user userObj1 = new User(alias = 'user', email='test3127415' + '@cognizant.com', 
                                 emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London', username='test1user' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='tej23415',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                 IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432');
        insert userObj1;
        //List<IDMSCompanyReconciliationBatchHandler__c> handler=new IDMSCompanyReconciliationBatchHandler__c(AtCreate__c=true,AtUpdate__c=true,
        //                                                                                              HttpMessage__c='Test134',NbrAttempts__c=1,IdmsUser__c=userObj1.Id);
        //insert handler; 
        
        IDMSCompanyReconciliationBatchHandler__c[] records= new List<IDMSCompanyReconciliationBatchHandler__c>();
        for(Integer i=0;i<3;i++) {
            IDMSCompanyReconciliationBatchHandler__c a = new IDMSCompanyReconciliationBatchHandler__c(AtCreate__c= true, 
                                    HttpMessage__c='San Francisco'+i,IdmsUser__c = userObj1.Id);
            records.add(a);
        }
        String recordsToSerialized = JSON.serialize(records);
        IdmsUimsReconcCreateCompanyBulkTest.callAPIUims(recordsToSerialized); 
    }
    
   
    
    @future
    private static void callAPIUims(String  recordsToSerialized) {
    List<IDMSCompanyReconciliationBatchHandler__c> recordsDeSerialised=  (List<IDMSCompanyReconciliationBatchHandler__c>)JSON.deserialize(recordsToSerialized,List<IDMSCompanyReconciliationBatchHandler__c>.class);
    Test.setMock(WebServiceMock.class, new IdmsUsersResyncimplFlowServiceImsMock());
        IdmsUimsReconcCreateCompanyBulk idmsUimsReconcCreateCompanyBulkcls=new IdmsUimsReconcCreateCompanyBulk();   
         Test.startTest();
            IdmsUimsReconcCreateCompanyBulk.createUimsCompanies(recordsDeSerialised,'Test123');
             Test.stopTest();
    }*/
   
   /* static testmethod void company2(){
    
     user userObj1 = new User(alias = 'user', email='userrec' + '@accenture.com', 
                                 emailencodingkey='UTF-8', lastname='Testlast', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London', username='test1user' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='Test-1234',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = 'work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                 IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432');
        insert userObj1;
      IDMSCompanyReconciliationBatchHandler__c handler=new IDMSCompanyReconciliationBatchHandler__c(AtCreate__c=true,AtUpdate__c=true,
                                                                                                      HttpMessage__c='Test134',NbrAttempts__c=1,IdmsUser__c=userObj1.Id);
       insert handler;  
       
        String env43 = System.Label.CLQ316IDMS043 ; 
        String env44 = System.Label.CLQ316IDMS044 ;
    Test.startTest();
    User usercmp= new User(alias = 'usercmp', email='usercmp' + '@accenture.com', 
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                    BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                    timezonesidkey='Europe/London', username='usercmp' + env44 + env43 ,Company_Postal_Code__c='12345',
                                    Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                    IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                    IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
            
        insert usercmp;
     
        
    
    IDMSCompanyReconciliationBatchHandler__c company1 = new IDMSCompanyReconciliationBatchHandler__c() ; 
        company1.IdmsUser__c = usercmp.id; 
        company1.AtCreate__c=true ;
        company1.AtUpdate__c=true;
        company1.HttpMessage__c='Test134';
        company1.NbrAttempts__c=1;                                                                             
        insert company1 ; 
        IDMSCompanyReconciliationBatchHandler__c company2 = new IDMSCompanyReconciliationBatchHandler__c() ; 
         company2.AtCreate__c=true ;
         company2.AtUpdate__c=true;
         company2.HttpMessage__c='Test134';
         company2.NbrAttempts__c=1;
        company2.IdmsUser__c = usercmp.id ; 
        company2.name='test123';
        insert company2; 
    List<IDMSCompanyReconciliationBatchHandler__c> lstHandler=new List<IDMSCompanyReconciliationBatchHandler__c>();
        lstHandler.add(company1);
        lstHandler.add(company2);
        
     
     Test.setMock(WebServiceMock.class, new IdmsUsersResyncimplFlowServiceImsMock());
     try{
     IdmsUimsReconcCreateCompanyBulk.createUimsCompanies(lstHandler,'123');   
     }catch(exception e){}
     Test.stopTest(); 
    }*/
    
}