@isTest(SeeAllData=true)
private class SVMXC_TimeEntryRequestTest {

    static testMethod void timeEntryTestApprove() {
    
        SVMXC__Service_Group__c team = new SVMXC__Service_Group__c(Name = 'Test team');                
        insert team;
        
        User mgrUserObj = Utils_TestMethods.createStandardUser('TestMgr');
        mgrUserObj.ManagerId = UserInfo.getUserId();
        mgrUserObj.BypassVR__c = true;
        insert mgrUserObj;
        
        User userObj = Utils_TestMethods.createStandardUser('Test');
        userObj.ManagerId = mgrUserObj.Id;
        userobj.BypassVR__c = true;
        insert userObj;
        
        BusinessHours bhs= [select Id from BusinessHours where IsDefault=true];
        
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = team.Id,
                    SVMXC__Salesforce_User__c = userObj.Id, SVMXC__Working_Hours__c = bhs.Id, SVMXC__Break_Hours__c = bhs.Id);
        
        insert tech;

        Date todaysDate = Date.today();
        Date weekStart = todaysDate.toStartofWeek();
        Time myTime = Time.newInstance(12,0,0,0);
        List<SVMXC_Time_Entry_Request__c> newTERs = new List<SVMXC_Time_Entry_Request__c>();
        
        DateTime startingTime = DateTime.newInstance(weekStart, myTime);
        
        // TER1
        SVMXC_Time_Entry_Request__c newTER1 = new SVMXC_Time_Entry_Request__c();
        newTER1.Technician__c = tech.Id;
        newTER1.Start_Date__c = startingTime.addDays(1);
        newTER1.End_Date__c = startingTime.addDays(5);
        newTER1.Activity__c = 'Time Off';
        newTER1.Submitted_for_Approval__c = true;
        newTER1.All_Day_Event__c = true;
        newTERs.add(newTER1);
        
        // TER2
        DateTime startingTime2 = DateTime.now();    
        SVMXC_Time_Entry_Request__c newTER2 = new SVMXC_Time_Entry_Request__c();
        newTER2.Technician__c = tech.Id;
        newTER2.Start_Date__c = startingTime2.addDays(14).addHours(2);
        newTER2.End_Date__c = startingTime2.addDays(14).addHours(3); 
        newTER2.Submitted_for_Approval__c = true;
        newTER2.Activity__c = 'Time Off';
        newTER2.Travel_From_Hours__c = 1;
        newTER2.Travel_To_Hours__c = 1; 
        newTER2.All_Day_Event__c = false;
        newTERs.add(newTER2);
        
        System.runAs(userObj) {         
             dataBase.insert( newTERs, false);
        }

        System.runAs(mgrUserObj) {
            
            List<SVMXC_Time_Entry_Request__c> queriedTERs = [SELECT Status__c FROM SVMXC_Time_Entry_Request__c 
                                                      WHERE Technician__c =: tech.Id];
            
            for (SVMXC_Time_Entry_Request__c ters : queriedTERs) {
                ters.Status__c = 'Approved';
                //ters.OwnerId = UserInfo.getUserId();
            }    
            //update queriedTERs;
            dataBase.update( queriedTERs, false);
        }
    }
    
    static testMethod void timeEntryTest2Reject() {
    
        SVMXC__Service_Group__c team = new SVMXC__Service_Group__c(Name = 'Test team');              
        insert team;
        
        User mgrUserObj = Utils_TestMethods.createStandardUser('TestMgr');
        mgrUserObj.ManagerId = UserInfo.getUserId();
        insert mgrUserObj;
        
        User userObj = Utils_TestMethods.createStandardUser('Test');
        userObj.ManagerId = mgrUserObj.Id;
        insert userObj;
        
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = team.Id,
                    SVMXC__Salesforce_User__c = userObj.Id);
        
        insert tech;
        
        DateTime startingTime = DateTime.now();
        
        SVMXC_Time_Entry_Request__c newTER = new SVMXC_Time_Entry_Request__c();
        //newTER.Technician__c = tech.Id;
        newTER.Start_Date__c = startingTime.addHours(2);
        newTER.End_Date__c = startingTime.addHours(3); 
        newTER.Submitted_for_Approval__c = true;
        newTER.Activity__c = 'Time Off';
        newTER.Travel_From_Hours__c = 1;
        newTER.Travel_To_Hours__c = 1; 
        newTER.All_Day_Event__c = false;
       
       System.runAs(userObj) {
       try{         
            insert newTER;
            }catch (exception e){}   
        }
          
        System.runAs(mgrUserObj) {
            
            //SVMXC_Time_Entry_Request__c queriedTER = [SELECT Status__c FROM SVMXC_Time_Entry_Request__c WHERE Id =: newTER.Id];
            newTER.Status__c = 'Rejected';
            try{ 
            update newTER;
             }catch (exception e){} 
            
        }
    }
}