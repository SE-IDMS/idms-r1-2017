@isTest
public class VFC_AssetRouting_Test{

     public static testMethod void UnitTestAssetRouting(){
    
         Test.startTest() ;
        PageReference pageRef = Page.VFP_AssetRouting;
        Test.setCurrentPage(pageRef);
        
        
       
        // Creating Data 
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.RecordTypeid = Label.CLOCT13ACC08;
        insert objAccount;
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = objAccount .id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        Country__c country= Utils_TestMethods.createCountry(); 
        country.CountryCode__c= 'hkh';   
        insert country;   
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        objContact.Country__c= country.id;
        insert objContact;
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        objCase.SVMXC__Site__c = site1.id;
        insert objCase;
        
         ApexPages.currentPage().getParameters().put('id', objCase.id);
         
         VFC_AssetRouting controller = new VFC_AssetRouting();
         controller.init();
    
    
    }


}