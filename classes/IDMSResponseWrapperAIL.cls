//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This class is handling the AIL Response from external system
//
//*******************************************************************************************

global with sharing class IDMSResponseWrapperAIL {
    public String Message;  
    public String Status; 
    public IDMSUserAIL__c IDMSUserAIL;        
    
    //Constructor per default
    public IDMSResponseWrapperAIL(){}
    
    //Constructor of a AIL response
    public IDMSResponseWrapperAIL(IDMSUserAIL__c IDMSUserAIL,string Status,string Message){
        this.IDMSUserAIL    = IDMSUserAIL;
        this.Status         = Status;
        this.Message        = Message;
    }
}