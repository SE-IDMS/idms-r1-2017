@isTest

class AP_ProblemComplaintRequestLink_Test {
    
    static testMethod void method1() {
        
        //Create Country
        Country__c CountryObjLCR = Utils_TestMethods.createCountry();
        Insert CountryObjLCR;
        
        // Create User
        User LCRTestUser = Utils_TestMethods.createStandardUser('LCROwner');
        Insert LCRTestUser ;
        
        // Create Account
        Account accountObj = Utils_TestMethods.createAccount();
        accountObj.Country__c=CountryObjLCR.Id; 
        insert accountObj;
        
        // Create Contact to the Account
        Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'LCRContact');
        insert ContactObj;
        
        //Case
        Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');        
        CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
        CaseObj.Symptom__c =  'Installation/ Setup';
        CaseObj.SubSymptom__c = 'Hardware';    
        CaseObj.Quantity__c = 4;
        CaseObj.Family__c = 'ADVANCED PANEL';
        CaseObj.CommercialReference__c='xbtg5230';
        CaseObj.ProductFamily__c='HMI SCHNEIDER BRAND';
        CaseObj.TECH_GMRCode__c='02BF6DRG16NBX07632';
        CaseObj.LevelOfExpertise__c ='Advanced';
        Insert CaseObj;
        
        
        BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1', SubEntity__c='LCR Sub-Entity',Location__c='LCR Location',Location_Type__c= 'Return Center',Street__c = 'LCR Street',RCPOBox__c = '123456',RCCountry__c = CountryObjLCR.Id,RCCity__c = 'LCR City',RCZipCode__c = '500055',ContactEmail__c ='SCH@schneider.com');
        insert lCROrganzObj;
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
            Database.insert(breEntity);
            BusinessRiskEscalationEntity__c breSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
            Database.insert(breSubEntity);
            BusinessRiskEscalationEntity__c breLocationType = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation ();
            Database.insert(breLocationType); 
        
        EntityStakeholder__c  LCROrgStakeHold= new EntityStakeholder__c(
                                User__c=LCRTestUser.id,
                                AdditionalInformation__c='TestLCR',
                                Department__c ='TestLCR',
                                BusinessRiskEscalationEntity__c =lCROrganzObj.id,
                                Role__c ='Complaint/Request Owner'
                                
                                );
        //Complaint/Request
        ComplaintRequest__c  CompRequestObj1 = new ComplaintRequest__c(
                    Status__c='Open',
                    Case__c  =CaseObj.id,
                    Category__c ='1 - Pre-Sales Support',
                    reason__c='Marketing Activity Response',
                    //AccountableOrganization__c =lCROrganzObj.id, 
                    ReportingEntity__c =lCROrganzObj.id
                    );
        insert CompRequestObj1; 
        ComplaintRequest__c  CompRequestObj2 = new ComplaintRequest__c(
                    Status__c='Open',
                    Case__c  =CaseObj.id,
                    Category__c ='1 - Pre-Sales Support',
                    reason__c='Marketing Activity Response',
                    //AccountableOrganization__c =lCROrganzObj.id, 
                    ReportingEntity__c =lCROrganzObj.id
                    );
        insert CompRequestObj2;   
       
        Problem__c prob = Utils_TestMethods.createProblem(breLocationType.Id,12);
            prob.ProblemLeader__c = LCRTestUser.id;
            prob.ProductQualityProblem__c = false;
            prob.TECH_Symptoms__c = 'Symptom - SubSymptom'; 
            prob.RecordTypeid = Label.CLI2PAPR120014;
            prob.Sensitivity__c = 'Public';
            prob.Severity__c = 'Safety related';
            Database.insert(prob);
        Problem__c prob1 = Utils_TestMethods.createProblem(breLocationType.Id,12);
            prob1.ProblemLeader__c = LCRTestUser.id;
            prob1.ProductQualityProblem__c = false;
            prob1.TECH_Symptoms__c = 'Symptom - SubSymptom'; 
            prob1.RecordTypeid = Label.CLI2PAPR120014;
            prob1.Sensitivity__c = 'Public';
            prob1.Severity__c = 'Safety related';
            Database.insert(prob1);     
             
            
            
        List<ProblemComplaintRequestLink__c> newPCRlink = new List<ProblemComplaintRequestLink__c>();  
        ProblemComplaintRequestLink__c PCR1 = new ProblemComplaintRequestLink__c(Problem__c = prob.Id,ComplaintRequest__c = CompRequestObj1.Id);                
        newPCRlink.add(PCR1);
        ProblemComplaintRequestLink__c PCR2 = new ProblemComplaintRequestLink__c(Problem__c = prob1.Id,ComplaintRequest__c = CompRequestObj2.Id);                
        newPCRlink.add(PCR2);
        insert newPCRlink;
        
        List<String> CRIds = new List<String>();
        CRIds.add(CompRequestObj1.Id);
        CRIds.add(CompRequestObj2.Id);
        
        AP_ProblemComplaintRequestLink.CasePrbLink(newPCRlink,CRIds);
    }
}