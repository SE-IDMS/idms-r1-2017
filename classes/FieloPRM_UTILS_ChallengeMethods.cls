/********************************************************************
* Company: Fielo
* Created Date: 23/02/2015
* Description: Utils class for object Challenge
********************************************************************/
public with sharing class FieloPRM_UTILS_ChallengeMethods {

    /**
     * [canSubscribe checks if a member can subscribe to a challenge program. If the member was subscribed, it will return false]
     * @method          canSubscribe
     * @Pre-conditions  {{member logged on site, challengeToSuscribe with the preconditions challenge lookup completed}}
     * @Post-conditions {{string}}
     * @param           {[type]}     FieloEE__Member__c                 fieloMember          [description]
     * @param           {[type]}     FieloCH__Challenge__c              challengeToSubscribe [description]
     * @param           {[type]}     List<FieloCH__ChallengeMission__c> challengeMissions    [description]
     * @return          {[boolean]}                                        [can create the challenge member or not]
     */

    public static boolean canSubscribe(FieloEE__Member__c fieloMember, FieloCH__Challenge__c challengeToSubscribe, List<FieloCH__ChallengeMission__c> challengeMissions){

        //check if its not suscribed
        if(!FieloPRM_UTILS_ChallengeMethods.isSubscribed(fieloMember.Id,challengeToSubscribe.Id)){

            //check if the member completed all the missions
            Map<Id,Decimal> missionsMap = FieloPRM_UTILS_ChallengeMethods.loadMissionsStatus(fieloMember, challengeToSubscribe, challengeMissions);
            for(Id missionId : missionsMap.keySet() ){
                if(missionsMap.get(missionId) < 100.0){
                    return false;
                }
            }
        }else{
            //the member was previously subscribed
            return true;
        }
        return true;
    }
    /**
     * [isSubscribed checks if a member is subscribed]
     * @method          isSubscribed
     * @Pre-conditions  {{string}}
     * @Post-conditions {{string}}
     * @param           {[type]}     FieloEE__Member__c    member           [description]
     * @param           {[type]}     FieloCH__Challenge__c challengeProgram [description]
     * @return          {Boolean}                          [description]
     */
    public static boolean isSubscribed(Id memberId, Id challengeProgramId){
        Integer q = [SELECT Count() FROM FieloCH__ChallengeMember__c WHERE FieloCH__Challenge2__c =: challengeProgramId AND FieloCH__Member__c =: memberId];
        return q > 0;
    }

    /**
     * [loadMissionsStatus returns the status % of every mission of the challenge]
     * @method          loadMissionsStatus
     * @Pre-conditions  
     * @Post-conditions 
     * @param           {[type]}           FieloEE__Member__c                 fieloMember       [current logged in member]
     * @param           {[type]}           FieloCH__Challenge__c              challenge         [the challenge selected]
     * @param           {[type]}           List<FieloCH__ChallengeMission__c> challengeMissions [challenge's missions]
     * @return          {[type]}                                              [map wit mission Id and it's current %]
     */
    public static Map<Id, Decimal> loadMissionsStatus(FieloEE__Member__c fieloMember, FieloCH__Challenge__c challenge, List<FieloCH__ChallengeMission__c> challengeMissions){
        
        RecordType recordType = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'WithObjective' LIMIT 1];
        
        Map<Id, Decimal> aux = new Map<Id, Decimal>();
        FieloCH__ChallengeMember__c member = new FieloCH__ChallengeMember__c();
        member.FieloCH__Member__r = fieloMember;

        for(FieloCH__ChallengeMission__c mission : challengeMissions){
            Decimal value = 0;
            for(FieloCH__ChallengeMissionMember__c record : mission.FieloCH__ChallengeMissionsMembers__r ){

                FieloCH__Mission__c missionAux = (FieloCH__Mission__c)mission.getSObject('FieloCH__Mission2__r');            
                                
                if(challenge.FieloCH__Mode__c == 'Competition'){
                    value = missionAux.RecordTypeId == recordType.Id ? value + calculateProgress(mission, record, member) : getPerformanceValue(record, missionAux.FieloCH__ObjectiveType__c);
                }else{
                    value = missionAux.RecordTypeId == recordType.Id ? value + calculateProgress(mission, record, null) : getPerformanceValue(record, missionAux.FieloCH__ObjectiveType__c);
                }
            }
            Id misId = (Id)mission.get('FieloCH__Mission2__c');
            aux.put(misId, value);
        }
        return aux;
    }

    private static Decimal getPerformanceValue(sObject record, String objectiveType){
        Decimal aux = 0;
        if(objectiveType == 'Counter' && record.get('FieloCH__Counter__c') != null){
            aux += (Decimal)record.get('FieloCH__Counter__c') ;
        }else if(objectiveType == 'Max Value' && record.get('FieloCH__MaxValue__c') != null){
            aux += (Decimal)record.get('FieloCH__MaxValue__c') ;
        }else if(objectiveType == 'Summary' && record.get('FieloCH__Summary__c') != null){
            aux += (Decimal)record.get('FieloCH__Summary__c') ;
        }else if(objectiveType == 'Average' && record.get('FieloCH__Average__c') != null){
            aux += (Decimal)record.get('FieloCH__Average__c') ;
        }
        return aux;
    }

    private static Decimal calculateProgress(FieloCH__ChallengeMission__c mission, SObject record, FieloCH__ChallengeMember__c member){
        FieloCH__Mission__c missionAux = (FieloCH__Mission__c)mission.getSObject('FieloCH__Mission2__r');
        Decimal objectiveValue = 0;
        if(missionAux.FieloCH__DynamicObjective__c){
            objectiveValue = (Decimal)member.getsObject('Member__r').get(missionAux.FieloCH__ObjectiveField__c );
        }else{
            objectiveValue = missionAux.FieloCH__ObjectiveValue__c;
        }
        if(missionAux.FieloCH__Operator__c == 'greater than'){
            objectiveValue += 1;
        }else if(missionAux.FieloCH__Operator__c == 'less than' && missionAux.FieloCH__ObjectiveValue__c > 1){
            objectiveValue -= 1;
        }
        
        if(objectiveValue < 1){ objectiveValue = 1;}

        Decimal value = 0;
        if(missionAux.FieloCH__ObjectiveType__c == 'Counter' && record.get('FieloCH__Counter__c') != null){
            value += ((Decimal)record.get('FieloCH__Counter__c') / objectiveValue) * 100;
        }else if(missionAux.FieloCH__ObjectiveType__c == 'Max Value' && record.get('FieloCH__MaxValue__c') != null){
            value += ((Decimal)record.get('FieloCH__MaxValue__c') / objectiveValue) * 100;
        }else if(missionAux.FieloCH__ObjectiveType__c == 'Summary' && record.get('FieloCH__Summary__c') != null){
            value += ((Decimal)record.get('FieloCH__Summary__c') / objectiveValue) * 100;
        }else if(missionAux.FieloCH__ObjectiveType__c == 'Average' && record.get('FieloCH__Average__c') != null){
            value += ((Decimal)record.get('FieloCH__Average__c') / objectiveValue) * 100;
        }

        if(value > 100){
            value = 100;
        }
        return value;
    }

}