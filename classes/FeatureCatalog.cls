global class FeatureCatalog{
    // default value set to BFO
    WebService string PS { get; set; }

    // bFO ID of Partner Program
    WebService string ID { get; set; }
    
    // Name of the Partner Program
    WebService string NAME { get; set; }
    
    // Description of the Partner Program
    WebService string DESCRIPTION { get; set; }

    // Description of the Partner Program
    WebService string CATEGORY { get; set; }
    
    // LastUpdatetdDate timestamp in bFO
    WebService Datetime SDH_VERSION { get; set; }
    
    public FeatureCatalog() {
        PS = 'BFO';
    }
}