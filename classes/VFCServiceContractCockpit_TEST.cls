@isTest
private class VFCServiceContractCockpit_TEST {  

    static testMethod void testone(){
    
         Account accountSobj = Utils_TestMethods.createAccount();
        insert accountSobj;
        
        SVMXC__Service_Contract__c sc1 = new SVMXC__Service_Contract__c();
        sc1.SVMXC__Company__c = accountSobj.Id;
        sc1.SVMXC__End_Date__c=date.today();
        sc1.SVMXC__Active__c = true;
        sc1.SVMXC__Start_Date__c=date.Today();
        insert sc1;
        SVMXC__Service_Contract__c sc2 = new SVMXC__Service_Contract__c();
        sc2.SVMXC__Company__c = accountSobj.Id;
        sc2.ParentContract__c = sc1.id;
        sc2.SVMXC__Active__c = true;
        sc2.SVMXC__End_Date__c=date.today();
        sc2.SVMXC__Start_Date__c=date.Today();
        insert sc2;
        SVMXC__Service_Contract__c sc3 = new SVMXC__Service_Contract__c();
        sc3.SVMXC__Company__c = accountSobj.Id;
        sc3.ParentContract__c = sc2.id;
        sc3.SVMXC__Active__c = true;
        sc3.SVMXC__End_Date__c=date.today();
        sc3.SVMXC__Start_Date__c=date.Today();
        insert sc3;
        SVMXC__Service_Contract__c sc4= new SVMXC__Service_Contract__c();
        sc4.SVMXC__Company__c = accountSobj.Id;
         sc4.ParentContract__c = sc3.id;
         sc4.SVMXC__Active__c = true;
        sc4.SVMXC__End_Date__c=date.today();
        sc4.SVMXC__Start_Date__c=date.Today();
        insert sc4;
        
        PageReference pageRef = Page.VFPServiceContractCockpit ;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stcontroller= new ApexPages.StandardController(sc4);
        VFCServiceContractCockpit cpit = new VFCServiceContractCockpit(stcontroller);
        
        
    
    }


}