/**************************************************************************************
    Author: Fielo Team
    Date: 29/06/2015
    Description: 
    Related Components: <Component1> / <Componente2>...
***************************************************************************************/
@isTest
public with sharing class FieloPRM_VFC_SegmentCreationTest{
  
    public static testMethod void testUnit1(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
        System.RunAs(us){

            RecordType rt = [SELECT Id FROM RecordType WHERE Name = 'Manual' AND SobjectType = 'FieloEE__RedemptionRule__c'];
            
            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();    

            ApexPages.StandardController sc = new ApexPages.StandardController(segment);
            FieloPRM_VFC_SegmentCreation testController = new FieloPRM_VFC_SegmentCreation(sc);
            testController.redirect();     
        }
    }
}