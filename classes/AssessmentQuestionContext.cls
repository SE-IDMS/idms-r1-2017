global class AssessmentQuestionContext
{
    WebService String[] assessmentIDs;

    WebService Integer StartRow = 1;
    WebService Integer BatchSize = 10000;
     
    WebService DateTime LastModifiedDate1;
    WebService DateTime LastModifiedDate2;
    WebService DateTime CreatedDate;
    
    /**
    * A blank constructor
    */
    public AssessmentQuestionContext() {
    }

    /**
    * A constructor based on an Program @param a Program
    */
    public AssessmentQuestionContext(List<String> assessmentIDs) {
        this.assessmentIDs = assessmentIDs;

    }
}