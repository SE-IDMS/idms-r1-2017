public class VFC_PRMLoginInitController{

    public Static CS_PRM_ApexJobSettings__c serveMinify{set;get{
        if(CS_PRM_ApexJobSettings__c.getInstance('SERVE_MINIFIED') != null)
            return CS_PRM_ApexJobSettings__c.getInstance('SERVE_MINIFIED');
        else
            return null;
    }}
}