global class BatchScheduleLocationTimezoneValidation implements Schedulable {
  
  global void execute(SchedulableContext sc) {
        BatchLocationTimezoneValidation batch = new BatchLocationTimezoneValidation();
        Database.executeBatch(batch, 10);
    } 
    
    @isTest
    public static void test() 
    {
        BatchScheduleLocationTimezoneValidation job = new BatchScheduleLocationTimezoneValidation ();
        job.execute(null);
    }

}