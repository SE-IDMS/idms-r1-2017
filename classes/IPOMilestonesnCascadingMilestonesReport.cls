global class IPOMilestonesnCascadingMilestonesReport{ 
    public IPOMilestonesnCascadingMilestonesReport(ApexPages.StandardController controller) {
        Years = '2014';
        ProgOwner = 'ALL';
        MilestoneReport();
    }

    public string ProgOwner{get;set;}
    public List<IPO_Strategic_Program__c> prgList = new List<IPO_Strategic_Program__c>();
    public List<IPO_Strategic_Milestone__c> milestoneList = new List<IPO_Strategic_Milestone__c>();
    public Map<Id, List<IPO_Strategic_Milestone__c>> prgIdMilestoneListMap { get; set; }
    List<Id> prgIds = new List<Id>();
    Map<Id, IPO_Strategic_Program__c> prgMap = new Map<Id, IPO_Strategic_Program__c>();
    public String Years{get;set;}
    public List<MilestoneWrapper> milestoneWrapperList { get; set; }
    
    public String getYears() {
        return Years;
    }

    public List<SelectOption> getFilterYear() {
        List<SelectOption> Yearoptions = new List<SelectOption>();
     //   Yearoptions.add(new SelectOption('0','Select Year'));
        Yearoptions.add(new SelectOption('2013','2013')); 
        Yearoptions.add(new SelectOption('2014','2014'));
        Yearoptions.add(new SelectOption('2015','2015')); 
        return Yearoptions;
    }
    
   public List<SelectOption> getFilterProgramOwnerList() 
    {
    Set<String> SetProgramOwner = new Set<String>();
    List<SelectOption> options = new List<SelectOption>();
    List<IPO_Strategic_Program__c> lstIPOPrg = new List<IPO_Strategic_Program__c>();
    lstIPOPrg = [Select id, Name, Program_Owner__r.FirstName, Program_Owner__r.LastName, Program_Owner__c from IPO_Strategic_Program__c WHERE Year__c = :Years ORDER BY Name ASC];
    
    //options.add(new SelectOption('','<Select a Program Owner>'));  
    options.add(new SelectOption('ALL','All Program Owners'));  
    for(IPO_Strategic_Program__c PO : lstIPOPrg)
     {
       if(PO.Program_Owner__r.FirstName != null)
         SetProgramOwner.add(PO.Program_Owner__r.FirstName + ' ' + PO.Program_Owner__r.LastName);
        
      }
    for(string pown : SetProgramOwner)
    {
       options.add(new SelectOption(pown,pown)); 
     }
     options.sort();  
    return(options);
    }
    
    public pagereference YearChange() {
        
        return null;
    }
    
    
    public pagereference MilestoneReport() { 
        prgIds.clear();
        prgMap.clear();
        prgList.clear();
        milestoneList.clear();    
        
        prgList = [SELECT Id, Name, Year__c, Program_Owner__r.FirstName, Program_Owner__r.LastName FROM IPO_Strategic_Program__c WHERE Year__c = :Years ORDER BY Name ASC];
        
        System.debug('Year:'+Years);
        System.debug('Program:'+prgList);
        prgIdMilestoneListMap = new Map<Id, List<IPO_Strategic_Milestone__c>>();
        milestoneWrapperList = new List<MilestoneWrapper>();
        if(prgList.size() > 0) {
            for(IPO_Strategic_Program__c mem : prgList) {
                System.debug('Inside FOR:'+prgList);
                string oname = mem.Program_Owner__r.Firstname + ' ' + mem.Program_Owner__r.Lastname;
                System.debug('oname :'+oname);
                System.debug('Owner:'+ProgOwner);
                if(ProgOwner != 'ALL') {
                System.debug('INSIDE FIRST IF:'+oname);
                if(oname == ProgOwner) {
                System.debug('>>>>>>>>Inside 2nd IF:'+oname);
                    prgIds.add(mem.Id);
                    prgMap.put(mem.Id, mem);
                System.debug('>>>>>prgIds:'+prgIds);
                System.debug('>>>>>prgMap:'+prgMap);
                }
             }
             else {
                 System.debug('Inside ELSE');
                 prgIds.add(mem.Id);
                 prgMap.put(mem.Id, mem);
                 System.debug('>>>>>Inside ELSE'+prgIds);
                 System.debug('>>><<<<<Inside ELSE'+prgMap);    
             }
            }
       
       System.debug('>>>>>prgIds:'+prgIds);
       if(ProgOwner != 'ALL') {    
            System.debug('>>>>>>Year in Milestones:'+Years);
            System.debug('>>>>>>Year in Milestones:'+ProgOwner); 
            milestoneList = [SELECT Id, Name, IPO_Strategic_Program_Name__c, Program_Owner__c, Current_Status__c, Due_Date__c, IPO_Strategic_Milestone_Name__c, Year__c, 
                                    (SELECT Id, Name, IPO_Strategic_Cascading_Milestone_Name__c, CurrentProgressSummary_Deployment_Leader__c, Due_Date__c, IPO_Strategic_Milestone__c, 
                                    IPO_Strategic_Milestone_Name__c, Year__c, Program_Name__c FROM IPO_Strategic_Cascading_Milestones__r ORDER BY Due_Date__c) 
                                    FROM IPO_Strategic_Milestone__c WHERE IPO_Strategic_Program_Name__c = :prgIds AND Program_Owner__c = :ProgOwner AND Year__c = :Years ORDER BY Due_Date__c,IPO_Strategic_Program_Name__r.Name ASC];
       System.debug('INSIDE Milestones:'+milestoneList);
       }
       if(ProgOwner == 'ALL')  { 
            System.debug('Year in Milestones:'+Years);   
            milestoneList = [SELECT Id, Name, IPO_Strategic_Program_Name__c, Program_Owner__c, Current_Status__c, Due_Date__c, IPO_Strategic_Milestone_Name__c, Year__c, 
                                    (SELECT Id, Name, IPO_Strategic_Cascading_Milestone_Name__c, CurrentProgressSummary_Deployment_Leader__c, Due_Date__c, IPO_Strategic_Milestone__c, 
                                    IPO_Strategic_Milestone_Name__c, Year__c, Program_Name__c FROM IPO_Strategic_Cascading_Milestones__r ORDER BY Due_Date__c) 
                                    FROM IPO_Strategic_Milestone__c WHERE IPO_Strategic_Program_Name__c IN : prgIds AND Year__c = :Years ORDER BY Due_Date__c,IPO_Strategic_Program_Name__r.Name ASC];    
           System.debug('>>>>>>>>>>INSIDE Milestones:'+milestoneList); 
       }
        }
        milestoneList.sort();
        if(milestoneList.size() > 0) {
            System.debug('INSIDE IF FOR Milestones:'+milestoneList);
            for(IPO_Strategic_Milestone__c miles : milestoneList) {
                System.debug('INSIDE FOR OF Milestones:'+milestoneList);
                if(!prgIdMilestoneListMap.containsKey(miles.IPO_Strategic_Program_Name__c)){
                    prgIdMilestoneListMap.put(miles.IPO_Strategic_Program_Name__c, new List<IPO_Strategic_Milestone__c>());
                    System.debug('prgIdMilestoneListMap:'+prgIdMilestoneListMap);
                }
                prgIdMilestoneListMap.get(miles.IPO_Strategic_Program_Name__c).add(miles);
                System.debug('OUTSIDE prgIdMilestoneListMap:'+prgIdMilestoneListMap);
            }
            for(Id mileId : prgIdMilestoneListMap.keySet()) {
                milestoneWrapperList.add(new MilestoneWrapper(prgMap.get(mileId), prgIdMilestoneListMap.get(mileId)));
                System.debug('INSIDE milestoneWrapperList:'+milestoneWrapperList);
            }
        }
        System.debug('OUTSIDE milestoneWrapperList:'+milestoneWrapperList);
        milestoneWrapperList.sort();
        return null;
    } 
    
    global class MilestoneWrapper implements Comparable{
        public IPO_Strategic_Program__c prg { get; set; }
        public List<IPO_Strategic_Milestone__c> milLst { get; set; }
        
        global MilestoneWrapper(IPO_Strategic_Program__c prg, List<IPO_Strategic_Milestone__c> milLst) {
            this.prg = prg;
            this.milLst = milLst;
        }
        global Integer compareTo(Object compareTo) {
            MilestoneWrapper milewrap = (MilestoneWrapper)compareTo;
            Integer returnValue;
            if(prg.Name == milewrap.prg.Name) {
                returnValue = 0;
            }
            else if(prg.Name > milewrap.prg.Name) {
                returnValue = 1;
            }
            else if(prg.Name < milewrap.prg.Name) {
                returnValue = -1;
            }
            return returnValue;
        }
       
    }
}