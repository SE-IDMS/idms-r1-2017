/**************************************************************************************
    Author: Fielo Team (Elena J. Schwarzböck)
    Date: 22/06/2015
    Description: 
    Related Components: <Component1> / <Componente2>...
***************************************************************************************/

global with sharing class FieloPRM_VFP_PartnerJourneyWidgetRLExten{

    public List<FieloEE__News__c> listCF {get;set;}
    public FieloEE__Component__c comp {get;set;}   
    public String errorMessage {get;set;}
    public Map<Id,FieloEE__Tag__c> mapCFTags {get;set;}

    public FieloPRM_VFP_PartnerJourneyWidgetRLExten(ApexPages.StandardController controller){
    
        if(!Test.isRunningTest()){   
            controller.addFields(new List<String>{'FieloEE__Tag__c','FieloEE__Category__c'}); 
        }
        
        comp = (FieloEE__Component__c) controller.getRecord();
        
        system.debug('###Category: ' + comp.FieloEE__Category__c);
        system.debug('###Tag: ' + comp.FieloEE__Tag__c);
        
        if(comp.FieloEE__Tag__c != null && comp.FieloEE__Category__c != null){
        
            List<FieloEE__TagItem__c> listTagItemsPJ = [SELECT Id, FieloEE__News__c, FieloEE__Tag__c, FieloEE__Tag__r.FieloEE__Order__c, FieloEE__Tag__r.Name FROM FieloEE__TagItem__c WHERE FieloEE__Tag__c =: comp.FieloEE__Tag__c];
        
            system.debug('###listTagItemsPJ: ' + listTagItemsPJ);
        
            Set<Id> setCF = new Set<Id>();
            List<FieloEE__Tag__c> listTags = new List<FieloEE__Tag__c>();
            mapCFTags = new Map<Id,FieloEE__Tag__c>();
            
            for(FieloEE__TagItem__c tagItem: listTagItemsPJ){
                setCF.add(tagItem.FieloEE__News__c);
                FieloEE__Tag__c tag = new FieloEE__Tag__c(
                    Name = tagItem.FieloEE__Tag__r.Name,
                    FieloEE__Order__c = tagItem.FieloEE__Tag__r.FieloEE__Order__c,
                    Id = tagItem.FieloEE__Tag__c
                );
                mapCFTags.put(tagItem.FieloEE__News__c,tag);
            }
            
            
            
            List<FieloEE__TagItem__c> listTagItems = [SELECT Id, FieloEE__News__c, FieloEE__Tag__c, FieloEE__Tag__r.FieloEE__Order__c, FieloEE__Tag__r.Name FROM FieloEE__TagItem__c WHERE FieloEE__News__c IN: setCF AND FieloEE__Tag__c !=: comp.FieloEE__Tag__c];

            system.debug('###listTagItems: ' + listTagItems);
            
            for(FieloEE__TagItem__c tagItem: listTagItems){
                setCF.add(tagItem.FieloEE__News__c);
                FieloEE__Tag__c tag = new FieloEE__Tag__c(
                    Name = tagItem.FieloEE__Tag__r.Name,
                    FieloEE__Order__c = tagItem.FieloEE__Tag__r.FieloEE__Order__c,
                    Id = tagItem.FieloEE__Tag__c
                );
                mapCFTags.put(tagItem.FieloEE__News__c,tag);
                listTags.add(tag);
            }
            
            system.debug('###setCF: ' + setCF);
            
            listCF = [SELECT Id, Name, FieloEE__Title__c, FieloEE__Order__c, FieloEE__IsActive__c FROM FieloEE__News__c WHERE Id IN: setCF AND FieloEE__CategoryItem__c =: comp.FieloEE__Category__c ORDER BY FieloEE__Order__c, FieloEE__Title__c ASC];

        }
        
        system.debug('###listCF: ' + listCF);
    }
    
    public PageReference doNewCF(){
    
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'FieloPRM_NewsWidget' AND SobjectType = 'FieloEE__News__c'];

        FieloEE__News__c newCF = new FieloEE__News__c(
            FieloEE__CategoryItem__c = comp.FieloEE__Category__c,
            RecordTypeId = rt.Id
        );
        try{insert newCF;}catch(DMLException e){errorMessage = e.getMessage();return null;}

        FieloEE__TagItem__c newTagItem = new FieloEE__TagItem__c(
            FieloEE__Tag__c = comp.FieloEE__Tag__c,
            FieloEE__News__c = newCF.Id
        );
        
        try{insert newTagItem;}catch(DMLException e){errorMessage = e.getMessage();return null;}
        
        PageReference pageRef = new PageReference ('/' + newCF.Id + '/e');
        pageRef.getParameters().put('retURL','/' + comp.Id);
        return pageRef;                
        
    }   

    public PageReference doDelete(){
    
        string cfId = ApexPages.currentpage().getParameters().get('cfId');
    
        List<FieloEE__TagItem__c> tagItem = [SELECT Id FROM FieloEE__TagItem__c WHERE FieloEE__News__c =: cfId];

        if(!tagItem.isEmpty()){
            try{delete tagItem;}catch(DMLException e){errorMessage = e.getMessage();return null;}
        }
        
        FieloEE__News__c cf = new FieloEE__News__c(
            Id = cfId
        );
        
        try{delete cf;}catch(DMLException e){errorMessage = e.getMessage();return null;}
                
        PageReference pageRef = new PageReference ('/' + comp.Id);
        return pageRef;                
        
    }
    
}