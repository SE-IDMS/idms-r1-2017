@isTest
private class AP_AccountBeforeDeleteHandler_Test {
  
  @isTest static void testCheckAccountDelete()
  {
    // Implement test code
        Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'India';
        insert ct;
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
        insert acc1;
        Account acc2 = new Account(Name='Acc2', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1236');      
        insert acc2;
        Contact con1 = Utils_TestMethods.createContact(acc1.Id,'TestContact');
        insert con1;
        Case case1 = Utils_TestMethods.createCase(acc1.Id, con1.Id, 'Open');
        insert case1;
        Opportunity opp1= Utils_TestMethods.createOpenOpportunity(acc1.Id);
        insert opp1;
        Lead lead1 = Utils_TestMethods.createLeadWithAcc(acc1.Id, ct.Id,100);
        insert lead1;
        CustomerSatisfactionSurvey__c css = new CustomerSatisfactionSurvey__c(GoldenCustomerSatisfactionSurveyId__c= 'CustomerSatisfactionSurveyId',Account__c = acc1.id);
        insert css;
        
        acc1.TobeDeleted__c = true;
        acc1.ReasonForDeletion__c = 'Created in error';
        
        test.startTest();
        try
        {
            delete acc1;
        }
        catch(Exception e)
        {
            System.debug('DmlException caught: ' + e.getMessage());
        }
        test.stopTest();    
        
  }    
}