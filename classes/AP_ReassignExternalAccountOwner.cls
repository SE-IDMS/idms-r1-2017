public without sharing class AP_ReassignExternalAccountOwner implements Queueable {

    List<Id> reassignExtOwnerAcctIds = new List<Id>();
    Map<String,AccountOwnershipRule__c> assignmentRules = new Map<String,AccountOwnershipRule__c>();
    public AP_ReassignExternalAccountOwner(List<Id> reassignExtOwnerAcctIds)
    {
        system.debug('---AP_ReassignExternalAccountOwner.reassignExtOwnerAcctIds (Constructor) -> '+reassignExtOwnerAcctIds);
        this.reassignExtOwnerAcctIds = reassignExtOwnerAcctIds;

        //Extract ownership rules and assigned internal owner details for functionality 'EAO'->External Account Ownership
        for(AccountOwnershipRule__c extOwnRule: [SELECT Id, TECH_CountryCode__c, ClassLevel1__c, County__c, TECH_StateProvinceCode__c, AccountOwner__c, TECH_CriteriaCode__c FROM AccountOwnershipRule__c WHERE Functionality__c='EAO' ])
        {
                assignmentRules.put(extOwnRule.TECH_CriteriaCode__c,extOwnRule);
        }
        system.debug('---AP_ReassignExternalAccountOwner.reassignExtOwnerAcctIds (Constructor) Ends---');
    }

    // Queue-able Execute Method
    public void execute(QueueableContext context) 
    {
        String county;  //Stores the county for a respective state
        String key;     //Stores the unique key used for obtaining the internal account owner
        String countryCode = Label.CLNOV16ACC002; //CLNOV16ACC002 = IT,ES,AD
        List<Account> updateAccountLst = new List<Account>();   //updates changes to owner and externalSalesResp fields on Account
        List<AccountTeammember> createAcctTeamMemlst = new List<AccountTeammember>(); //inserts AccountTeamMember and AccountShare records
        List<AccountShare> createAcctSharelst = new List<AccountShare>();    //inserts AccountShare 

        List<Account> reassignExtOwnerAcctLst =[SELECT Id, OwnerId, Owner.ProfileId, BillingCountryCode, BillingStateCode, ClassLevel1__c FROM Account WHERE Owner.ProfileId=:Label.CLJUL16ACC01 and Id IN :reassignExtOwnerAcctIds];
        system.debug('---AP_ReassignExternalAccountOwner.ExecuteContext -> '+context);
        system.debug('---AP_ReassignExternalAccountOwner.reassignExtOwnerAcctLst -> '+reassignExtOwnerAcctLst);
        List<CS_StateCountyMapping__c> stateCountyList = [SELECT Name,County__c from CS_StateCountyMapping__c];
        Map<String,String> stateCountyMap = new Map<String,String>();
        for(CS_StateCountyMapping__c st:stateCountyList) {
            stateCountyMap.put(st.Name,st.County__c);
        }
        
        try
        {   
            if(!reassignExtOwnerAcctLst.isEmpty()) 
            {
                for (Account acc : reassignExtOwnerAcctLst)
                {
                    //Reassign externally owned Italy accounts
                    if (acc.BillingCountryCode != null && acc.BillingCountryCode != '' && countryCode.contains(acc.BillingCountryCode)) 
                    {
                        //Populate external user in account field 'External Sales Responsible'
                        acc.ExternalSalesResponsible__c=acc.OwnerId;

                        //Add the External User to AccountTeam with Read/Write access to account, opportunity and case
                        AccountTeammember atm = new AccountTeammember(UserId = acc.OwnerId,AccountID = acc.Id,TeamMemberRole='ESR',AccountAccessLevel='Edit',OpportunityAccessLevel='Edit',CaseAccessLevel='Edit' );
                        createAcctTeamMemlst.add(atm);

                        //Assign account owner to an internal user based on Account Ownership Rules
                        if(acc.BillingStateCode != null && acc.BillingStateCode != '' && stateCountyMap.containsKey(acc.BillingStateCode))
                            county = CS_StateCountyMapping__c.getInstance(acc.BillingStateCode).County__c;
                        System.debug('key from custom setting'+county);
                        if(county == null && acc.BillingStateCode != null) {
                            key=acc.BillingCountryCode+acc.ClassLevel1__c+acc.BillingStateCode; // For county who don't have county
                        }
                        else if(county == null && acc.BillingStateCode == null) {
                            key=acc.BillingCountryCode+acc.ClassLevel1__c; // For country who don't have county or state/province
                        }
                        else if(county!=null) {
                            key=acc.BillingCountryCode+acc.ClassLevel1__c+county; // For country who have both county and state/province
                        }
                        System.debug('The Key is ---> '+key);
                        acc.OwnerId = assignmentRules.get(key).AccountOwner__c;
                        updateAccountLst.add(acc);
                        
                    }
                    /*
                    if(acc.BillingCountryCode == Label.CLNOV16ACC006 && acc.BillingStateCode !=null) {
                        //Populate external user in account field 'External Sales Responsible'
                        acc.ExternalSalesResponsible__c=acc.OwnerId;

                        //Add the External User to AccountTeam with Read/Write access to account, opportunity and case
                        AccountTeammember atm = new AccountTeammember(UserId = acc.OwnerId,AccountID = acc.Id,TeamMemberRole='ESR',AccountAccessLevel='Edit',OpportunityAccessLevel='Edit',CaseAccessLevel='Edit' );
                        createAcctTeamMemlst.add(atm);

                        //Assign account owner to an internal user based on Account Ownership Rules
                        //county = CS_StateCountyMapping__c.getInstance(acc.BillingStateCode).County__c;
                        key=acc.BillingCountryCode+acc.ClassLevel1__c+acc.BillingStateCode;
                        System.debug('The Key is ---> '+key);
                        acc.OwnerId = assignmentRules.get(key).AccountOwner__c;
                        updateAccountLst.add(acc);
                    }
                    
                    else if(countryCode.contains(acc.BillingCountryCode) && acc.BillingStateCode == null) {
                        //Populate external user in account field 'External Sales Responsible'
                        acc.ExternalSalesResponsible__c=acc.OwnerId;

                        //Add the External User to AccountTeam with Read/Write access to account, opportunity and case
                        AccountTeammember atm = new AccountTeammember(UserId = acc.OwnerId,AccountID = acc.Id,TeamMemberRole='ESR',AccountAccessLevel='Edit',OpportunityAccessLevel='Edit',CaseAccessLevel='Edit' );
                        createAcctTeamMemlst.add(atm);

                        //Assign account owner to an internal user based on Account Ownership Rules
                        //county = CS_StateCountyMapping__c.getInstance(acc.BillingStateCode).County__c;
                        key=acc.BillingCountryCode+acc.ClassLevel1__c;
                        System.debug('The Key is ---> '+key);
                        acc.OwnerId = assignmentRules.get(key).AccountOwner__c;
                        updateAccountLst.add(acc);
                    }
                    */
                }
                system.debug('---AP_ReassignExternalAccountOwner.updateAccountLst -> '+updateAccountLst);
                if(!updateAccountLst.isEmpty())
                    Database.update(updateAccountLst,false);
                
                system.debug('---AP_ReassignExternalAccountOwner.createAcctTeamMemlst -> '+createAcctTeamMemlst);
                if(!createAcctTeamMemlst.isEmpty())
                    Database.insert(createAcctTeamMemlst,false);          

            }
        }
        catch(Exception e)
        {
            System.debug('Error Message: '+e.getMessage());
            System.debug('Stack Trace: '+e.getStackTraceString());
        }
    }
}