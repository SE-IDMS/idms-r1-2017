/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :13-July-2012
    Modification Log    : 
    Description         : Test Method for Initiative Health Report
*/

@isTest
private class ConnectInitiativeHealthStatusReport_Test{
    static testMethod void InititativeHealthTest() {
    
       User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connectZ');
        System.runAs(runAsUser){
            
       
        initiatives__c inti1=Utils_TestMethods_Connect.createInitiative();
        inti1.Name = Label.ConnectInitEL;
        inti1.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti1.Initiative_Health_Status_by_Sponsors__c = '';
        inti1.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti1.Q4_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti1.year__c = Label.ConnectYear;
        insert inti1;
    
        initiatives__c inti2=Utils_TestMethods_Connect.createInitiative();
        inti2.Name = Label.ConnectInitEI;
        inti2.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti2.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti2.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti2.Q4_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
         inti2.year__c = Label.ConnectYear;
        insert inti2;
      
        initiatives__c inti3=Utils_TestMethods_Connect.createInitiative();
        inti3.Name = Label.ConnectInitEW;
        inti3.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti3.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti3.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti3.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
         inti3.year__c = Label.ConnectYear;
        insert inti3;
        
        initiatives__c inti4=Utils_TestMethods_Connect.createInitiative();
        inti4.Name = Label.ConnectInitGCI;
        inti4.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti4.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti4.Q3_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti4.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
         inti4.year__c = Label.ConnectYear;
        insert inti4;
        
        initiatives__c inti5=Utils_TestMethods_Connect.createInitiative();
        inti5.Name = Label.ConnectInitGC;
        inti5.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti5.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti5.Q3_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti5.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
         inti5.year__c = Label.ConnectYear;
        insert inti5;
        
        initiatives__c inti6=Utils_TestMethods_Connect.createInitiative();
        inti6.Name = Label.ConnectInitGE;
        inti6.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti6.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti6.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti6.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
         inti6.year__c = Label.ConnectYear;
        insert inti6;
        
        initiatives__c inti7=Utils_TestMethods_Connect.createInitiative();
        inti7.Name = Label.ConnectInitOE;
        inti7.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti7.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti7.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti7.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
         inti7.year__c = Label.ConnectYear;
        insert inti7;
    
        initiatives__c inti8=Utils_TestMethods_Connect.createInitiative();
        inti8.Name = Label.ConnectInitOM;
        inti8.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti8.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti8.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti8.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
         inti8.year__c = Label.ConnectYear;
        insert inti8;
      
        initiatives__c inti9=Utils_TestMethods_Connect.createInitiative();
        inti9.Name =Label.ConnectInitPE;
        inti9.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti9.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti9.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti9.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
         inti9.year__c = Label.ConnectYear;
        insert inti9;
        
        initiatives__c inti10=Utils_TestMethods_Connect.createInitiative();
        inti10.Name = Label.ConnectInitRE;
        inti10.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti10.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti10.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti10.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
         inti10.year__c = Label.ConnectYear;
        insert inti10;
        
        initiatives__c inti11=Utils_TestMethods_Connect.createInitiative();
        inti11.Name = Label.ConnectInitSE;
        inti11.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti11.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti11.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti11.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
         inti11.year__c = Label.ConnectYear;
        insert inti11;
        
        initiatives__c inti12=Utils_TestMethods_Connect.createInitiative();
        inti12.Name = Label.ConnectInitTSC;
        inti12.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti12.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti12.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti12.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
         inti12.year__c = Label.ConnectYear;
        insert inti12;
        
      
       //Use the PageReference Apex class to instantiate 
        PageReference pageRef = Page.Connect_Initiative_Health_Status_Report;
       
       //In this case, the Visualforce page named 'Connect_Initiative_Health_Status_Report' is the starting point of this Test method. 
        Test.setCurrentPage(pageRef);

 //Instantiate and construct the controller class.   
    ApexPages.StandardController stanPage= new ApexPages.StandardController(inti1);
        ConnectInitiativeHealthStatusController controller = new ConnectInitiativeHealthStatusController(stanPage);

 
   controller.Quarter = 'Q2';
 //Call the controller method
    controller.getFilterList();
    controller.getYear();
    controller.YearChange();
    controller.getFilterQuarter();
    controller.getQuarter();
    controller.RenderPDF();
    controller.QuarterChange();
    controller.getEL_Health();   
    controller.getEI_Health();
    controller.getEW_Health();
    controller.getGCI_Health();
    controller.getGC_Health();
    controller.getGE_Health();
    controller.getOE_Health();
    controller.getOM_Health();
    controller.getPE_Health();
    controller.getRE_Health();
    controller.getSE_Health();
    controller.getTSC_Health();
    controller.getLongDate();
    
    controller.Quarter = 'Q1';
    
    //Call the controller method
    controller.getFilterList();
    controller.getFilterQuarter();
    controller.getQuarter();
    controller.RenderPDF();
    controller.QuarterChange();
    controller.getEL_Health();   
    controller.getEI_Health();
    controller.getEW_Health();
    controller.getGCI_Health();
    controller.getGC_Health();
    controller.getGE_Health();
    controller.getOE_Health();
    controller.getOM_Health();
    controller.getPE_Health();
    controller.getRE_Health();
    controller.getSE_Health();
    controller.getTSC_Health();
    
    controller.Quarter = 'Q3';
    
    //Call the controller method
    controller.getFilterList();
    controller.getFilterQuarter();
    controller.getQuarter();
    controller.RenderPDF();
    controller.QuarterChange();
    controller.getEL_Health();   
    controller.getEI_Health();
    controller.getEW_Health();
    controller.getGCI_Health();
    controller.getGC_Health();
    controller.getGE_Health();
    controller.getOE_Health();
    controller.getOM_Health();
    controller.getPE_Health();
    controller.getRE_Health();
    controller.getSE_Health();
    controller.getTSC_Health();
    
    controller.Quarter = 'Q4';
    
    //Call the controller method
    controller.getFilterList();
    controller.getFilterQuarter();
    controller.getQuarter();
    controller.RenderPDF();
    controller.QuarterChange();
    controller.getEL_Health();   
    controller.getEI_Health();
    controller.getEW_Health();
    controller.getGCI_Health();
    controller.getGC_Health();
    controller.getGE_Health();
    controller.getOE_Health();
    controller.getOM_Health();
    controller.getPE_Health();
    controller.getRE_Health();
    controller.getSE_Health();
    controller.getTSC_Health();
    
      
    }
    
   }
}