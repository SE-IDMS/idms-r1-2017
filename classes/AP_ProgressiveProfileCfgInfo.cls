global Class AP_ProgressiveProfileCfgInfo{

    // Contact Information
    global String phoneNumber { get; set; }
    global Boolean phoneNumberRequired;

    global String phoneType { get; set; }
     
    global String jobFunction { get; set; }
    global boolean jobFunctionRequired;

    global String jobTitle { get; set; }
    global boolean jobTitleRequired;

    global String accountId {get; set; }
    global String contactId {get; set; }
    global String accOwnerID;
    global Boolean isPrimaryContact;
    global String userLanguage {get; set;}
    global String bFOLanguageLocaleKey;
    global String federationId { get; set;}
    global String companyFederatedId { get; set;}
    
    // Company Information
    global String companyPhone { get; set; }
    global boolean companyPhoneRequired;

    global String companyAddress1 { get; set; }
    global boolean companyAddress1Required;
    
    global String companyAddress2 { get; set; }
    global boolean companyAddress2Required;
    
    global String companyCity { get; set; }  
    global boolean companyCityRequired;
    
    global String companyCountryIsoCode;
    global String companyStateProvinceIsoCode;
    
    global String companyCountry { get; set; }
    global String companyCountryId {get; set;}
    global boolean companyCountryRequired;

    global String companyStateProvince {get; set; }
    global boolean companyStateProvinceRequired;

    global String companyZipcode {get; set; }
    global boolean companyZipcodeRequired;

    global boolean companyHeadQuarters {get; set; }
    global boolean companyHeadQuartersRequired;

    global String companyWebsite {get; set; }
    global boolean companyWebsiteRequired;

    global String taxId {get; set; }
    global boolean taxIdRequired;
    
    global String prefDistributor1;
    global boolean prefDistributor1Required;

    global String prefDistributor2;
    global boolean prefDistributor2Required;

    global String prefDistributor3;
    global boolean prefDistributor3Required;
    
    global String prefDistributor4;
    global boolean prefDistributor4Required;

    global String marketServed;
    global boolean marketServedRequired;

    global String  companySpeciality;
    global boolean companySpecialityRequired; 

    global String employeeSize;
    global boolean employeeSizeRequired;

    global String companyCurrency;
    global boolean companyCurrencyRequired;

    global String annualSales;
    global boolean annualSalesRequired;

    global String seAccountNumbers;
    global boolean seAccountNumbersRequired;
    
    global String  cTaxId;
    global boolean cTaxIdRequired;

    global Id memberId { get; set;}

    global AP_ProgressiveProfileCfgInfo(){
        companyHeadQuarters = false;
        phoneType = 'PhoneType';
        jobTitle = '';
        jobFunction = '';
        isPrimaryContact = false;
        cTaxIdRequired = seAccountNumbersRequired = annualSalesRequired = companyCurrencyRequired =employeeSizeRequired = companySpecialityRequired = false;
        marketServedRequired =  prefDistributor4Required = prefDistributor3Required = prefDistributor2Required = prefDistributor1Required= taxIdRequired = false;
        companyWebsiteRequired = companyHeadQuartersRequired = companyZipcodeRequired = companyStateProvinceRequired = companyCountryRequired = companyCityRequired = false;
        companyAddress2Required = companyAddress1Required = companyPhoneRequired = jobTitleRequired = jobFunctionRequired = phoneNumberRequired = false;
    }
}