/*
    Author          : Accenture IDC Team 
    Date Created    : 31/07/2011
    Description     : Test Class for Opportunity Type and Stage Picklist Manager Utility class
*/
@isTest
private class  Utils_OppStgRecdTypePicklistManager_TEST
{
       static testMethod void oppStageRecordTypePicklists_TestMethod()
       {
           Utils_PicklistManager Picklists = Utils_Factory.getPickListManager('OPP_REC');
           
           Picklists.getPickListLabels();
           Picklists.getPicklistValues(1, System.Label.CL00355);
           Picklists.getPicklistValues(1, '');
           Picklists.getPicklistValues(2, System.Label.CL00240);  
           Picklists.getPicklistValues(2, System.Label.CL00241);
           
       }   
}