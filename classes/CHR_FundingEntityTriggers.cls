/*
    Author          : Priyanka Shetty(Schneider Electric)
    Date Created    : 14/04/2013
    Description     : Class for triggers CHR_FundingEntityBeforeInsert and CHR_FundingEntityBeforeUpdate.
                      Creates a list of funding entity records to be processed by the CHR_FundingEntityUtils class
*/

public with sharing class CHR_FundingEntityTriggers
{
    public static void fundingentityBeforeUpdate(Map<Id,ChangeRequestFundingEntity__c> newMap,Map<Id,ChangeRequestFundingEntity__c> oldMap)
    {
        System.Debug('****** fundingentityBeforeUpdate Start ****'); 
        List<ChangeRequestFundingEntity__c> fundingentityListToProcessCONDITION1 = new List<ChangeRequestFundingEntity__c>();                
        
        for (ID feId:newMap.keyset())
        {
            ChangeRequestFundingEntity__c newFE = newMap.get(feId);
            ChangeRequestFundingEntity__c oldFE = oldMap.get(feId);
            
            if(newFE.ActiveForecast__c == true && oldFE.ActiveForecast__c == false)
            {
                fundingentityListToProcessCONDITION1.add(newFE);                
            }
            SYstem.debug('-----------Fe'+newFE);
                                    
        }
               
        if (fundingentityListToProcessCONDITION1.size()>0) 
            CHR_FundingEntityUtils.deactivateActiveFundingEntity(fundingentityListToProcessCONDITION1,true);
 
      CHR_FundingEntityUtils.updateProjectRequest(newMap.values());

        CHR_FundingEntityUtils.updatePLFields(newMap,oldMap);

        System.Debug('****** fundingentityBeforeUpdate End ****');             

    }   
 

    public static void fundingentityBeforeDelete(List<ChangeRequestFundingEntity__c> newFundingEntities){
        System.Debug('****** fundingentityAfterUpdate Start ****'); 
     
        CHR_FundingEntityUtils.updateProjectRequest(newFundingEntities);

        System.Debug('****** fundingentityAfterUpdate End ****');             

    }


    public static void fundingentityBeforeInsert(List<ChangeRequestFundingEntity__c> newFundingEntities)
    {
        System.Debug('****** fundingentityBeforeInsert Start ****'); 
        List<ChangeRequestFundingEntity__c> fundingentityListToProcessCONDITION1 = new List<ChangeRequestFundingEntity__c>();
        //List<ChangeRequestFundingEntity__c> fundingentityListToProcessCONDITION2 = new List<ChangeRequestFundingEntity__c>();
        
        for (ChangeRequestFundingEntity__c newFE:newFundingEntities)
        {                                  
            if(newFE.ActiveForecast__c == true)
            {
                fundingentityListToProcessCONDITION1.add(newFE);
                //fundingentityListToProcessCONDITION2.add(newFE);                
            }

        }
        
        
        if (fundingentityListToProcessCONDITION1.size()>0) 
            CHR_FundingEntityUtils.deactivateActiveFundingEntity(fundingentityListToProcessCONDITION1,false);

        if (newFundingEntities.size()>0) 
           CHR_FundingEntityUtils.checkFundingEntityExists(newFundingEntities);
            
        if(newFundingEntities!= null && newFundingEntities.size()>0)
            CHR_FundingEntityUtils.fundentityBeforeInsert(newFundingEntities);
            
        System.Debug('****** fundingentityBeforeInsert End ****');
    }   
    
}