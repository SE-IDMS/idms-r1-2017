public  class AP_ServiceContractInitialContract {
    
    public static void InitialContract(List<SVMXC__Service_Contract__c> aServiceContractList) {
        myDebug('start of AP_ServiceContractInitialContract.InitialContract');
        myDebug('Service Contract List: '+aServiceContractList);
        if (aServiceContractList != null) {
            myDebug('Service Contract list is not null');
            Id InitialContract;
            Set<Id> renewedFromIds = new Set<Id>();
            List<SVMXC__Service_Contract__c> nonRootContractsList = new List<SVMXC__Service_Contract__c>();
            
            for (SVMXC__Service_Contract__c aServiceContract : aServiceContractList) {
                myDebug('currentServiceContract: '+ aServiceContract);
                if (aServiceContract != null) {
                    if(aServiceContract.SVMXC__Renewed_From__c == null){
                        // Root level
                        myDebug('Root level: renewed from is null');
                        myDebug('Initial Contract name: '+ InitialContract);
                        aServiceContract.Initial_Contract__c = null;
                    }
                    else {
                        // We are not at the root level
                        renewedFromIds.add(aServiceContract.SVMXC__Renewed_From__c);
                        nonRootContractsList.add(aServiceContract);                        
                    }
                }
            }
            // All the root elements should have been processed by now.
            // Now, we will process the level 1+ elements
            
            Map<Id, SVMXC__Service_Contract__c> renewedFRomContractsMap = getRenewedFromContracts(renewedFromIds);
            SVMXC__Service_Contract__c currentRenewedFrom;
            for (SVMXC__Service_Contract__c currentContract : nonRootContractsList) {
                currentRenewedFrom = renewedFRomContractsMap.get(currentContract.SVMXC__Renewed_From__c);
                
                myDebug('currentContract: '+currentContract);
                myDebug('currentRenewedFrom: '+currentRenewedFrom);
                myDebug('currentRenewedFrom.SVMXC__Renewed_From__c: '+currentRenewedFrom.SVMXC__Renewed_From__c);
                if(currentRenewedFrom.SVMXC__Renewed_From__c ==null){
                    // Level 1
                    myDebug('Level 1: The renewed from of the renewed from is null');
                    myDebug('Level 1 - setting the initial contract to: '+currentRenewedFrom.SVMXC__Renewed_From__c);
                    currentContract.Initial_Contract__c = currentContract.SVMXC__Renewed_From__c;
                }
                else {
                    // Level 2+
                    myDebug('Level 2 or more: the renewed from of the renewed from is not null');
                    myDebug('Initial contract of the renewed from: '+currentRenewedFrom.Initial_Contract__c);
                    currentContract.Initial_Contract__c = currentRenewedFrom.Initial_Contract__c;
                }
                
            }
            
        }
    }
    
    private static Map<Id, SVMXC__Service_Contract__c> getRenewedFromContracts(Set<Id> aSetOfRenewedFrom) {
        Map<Id, SVMXC__Service_Contract__c> result = null;
        //if (aSetOfRenewedFrom != null) 
        if ( !aSetOfRenewedFrom.isempty() && aSetOfRenewedFrom.size() > 0 ) //Added by VISHNU C for TOO Many SOQL Error B2F SC Interface
        {
            result = new Map<Id, SVMXC__Service_Contract__c>([SELECT Id, name, SVMXC__Renewed_From__c, Initial_Contract__c from SVMXC__Service_Contract__c where ID in :aSetOfRenewedFrom]);
        }
        return result;
    }
    
    private static void myDebug(String aMessage) {
        Boolean isDebugEnabled = false;
        try {
            isDebugEnabled = Boolean.valueOf(Label.CLAPR14SRV20);
        }
        catch(Exception e) {
            System.debug('#### Error getting debug enabled status from Custom Label CLAPR14SRV20: '+e.getMessage());
        }
        if (isDebugEnabled) {
            System.debug('#### '+aMessage);
        }
    }
}