/* Controller for VFP_ProjectSearch
Monalisa - OCT 2015
*/
public class VFC_ProjectSearch{
    public OPP_Project__c prjRecord{get;set;}{prjRecord=new OPP_Project__c();}  
    public List<OPP_Project__c> results{get;set;}{results=new List<OPP_Project__c>();} 
    public boolean searchStep{get;set;}{searchStep=true;}
    public boolean disableCreateProjectbutton{get;set;}{disableCreateProjectbutton=true;}
    public PageReference newcustPrjPage = new PageReference('/'+System.Label.CLOCT15SLS15+'/e');
    public String sortExp = 'name';
    public List<Account> accountRecord=new List<Account>();
    public String sortDirection{get;set;} {sortDirection = 'ASC';}
    public Id accId;
    public boolean openOpptys{get;set;}
    public String sortExpression
    {
     get
     {
        return sortExp;
     }
     set
     {
       //if the column is clicked on then switch between Ascending and Descending modes
       if (value == sortExp)
         sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
       else
         sortDirection = 'ASC';
       sortExp = value;
     }
    }
    //Constructor
    public VFC_ProjectSearch(ApexPages.StandardController controller) {
        prjRecord=(OPP_Project__c)controller.getRecord();
        accId=System.currentPageReference().getParameters().get('accId');
        if(accId!=null)
           prjRecord.ProjectEndUserInvestor__c=accId;
        populateCountryOfDestination();
    } 
    
    public void populateCountryOfDestination()
    {
        //populate country of destination
        if(prjRecord.CountryOfDestination__c==null)
        {
            for(User userRecord:[Select id, Country__c from user where id=:UserInfo.getUserId() limit 1])
            {
                if(userRecord.Country__c!=null)
                {
                    for(Country__c countryRecord:[select id from Country__c where CountryCode__c=: userRecord.Country__c limit 1])
                    {
                        prjRecord.CountryOfDestination__c=countryRecord.id;
                    }
                }
            }             
        }        
    }

    public PageReference createProject()
    {
        searchStep=false;
         String prjName;
        if(prjRecord.Name!=null && prjRecord.Name!='')
             prjName = prjRecord.Name;    
        if(prjRecord.ProjectEndUserInvestor__c!=null){
              accountRecord = [SELECT Name FROM Account WHERE Id=:prjRecord.ProjectEndUserInvestor__c limit 1];
              System.debug('>>>accountRecord'+accountRecord);
            if(accountRecord.size()!=0)
            {   
               prjName = prjName + ' - '+ accountRecord[0].Name;         
               newcustPrjPage.getParameters().put(System.Label.CLOCT15SLS08, accountRecord[0].Name);           
                newcustPrjPage.getParameters().put(System.Label.CLOCT15SLS09,accountRecord[0].Id );
             }
         }
            if(prjRecord.CountryOfDestination__c!=null)
            {
                String countryName =   [SELECT Name FROM Country__c WHERE Id=:prjRecord.CountryOfDestination__c].Name;          
                newcustPrjPage.getParameters().put(System.Label.CLOCT15SLS11,countryName );
                newcustPrjPage.getParameters().put(System.Label.CLOCT15SLS10, prjRecord.CountryOfDestination__c);
                prjName = prjName + ' - '+ countryName;
            }
            newcustPrjPage.getParameters().put('Name', prjName);
            newcustPrjPage.getParameters().put('nooverride', '1');
            newcustPrjPage.getParameters().put('retURL', '/'+System.Label.CLOCT15SLS15+'/o');
        return newcustPrjPage;
    } 
     
      public String getSearchString()
    {
        if(prjRecord.Name!=null && prjRecord.Name.length()>40)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.warning,Label.CLOCT14SLS09);
            ApexPages.addMessage(myMsg);
        }    
        String sortFullExp = sortExpression + ' ' + sortDirection;
        string nullvaluesort;
        String searchString =  'FIND \'' + prjRecord.Name + '*\' IN NAME FIELDS ';  
            searchString += ' RETURNING OPP_Project__C(Id,Name,ProjectEndUserInvestor__c,CountryOfDestination__c,CountryOfDestination__r.Name,ProjectEndUserInvestor__r.Name,OwnerId, Owner.Name,EstimateProjectCloseDate__c,TotalProjectAmountForecastedWon__c,VIPIcon__c,ProjectStatus__c';
            List<String> whereConditions = new List<String>();
            if(prjRecord.ProjectEndUserInvestor__r!=null)      
               whereConditions.add('ProjectEndUserInvestor__c=\''+prjRecord.ProjectEndUserInvestor__c+'\'');
            if(prjRecord.CountryOfDestination__c!=null)      
                whereConditions.add('CountryOfDestination__c=\''+prjRecord.CountryOfDestination__c+'\'');
             if(openOpptys == true)
                whereConditions.add('ProjectStatus__c=\''+Label.CLOCT15SLS13+'\'');

            if(sortDirection == 'ASC')            
                nullvaluesort= ' NULLS LAST';
            if(sortDirection == 'DESC')            
                nullvaluesort = ' NULLS FIRST';    
            if(whereConditions.size() > 0) 
            {
                searchString += ' WHERE ';
                for(String condition : whereConditions) {
                    searchString += condition + ' AND ';   
                }
            // delete last 'AND'
                searchString = searchString.substring(0, searchString.lastIndexOf(' AND '));                
            }
            searchString+=' ORDER BY ' + sortFullExp + nullvaluesort + ')';           
        return searchString;    
    }
    public void search()
    {       
       String searchQuery=getSearchString();  

        try{
            List<List<Sobject>> searchResults=search.query(searchQuery);
            Utils_SDF_Methodology.log('START search: ', searchQuery);        
            if(searchResults.size()>0){
                Utils_SDF_Methodology.startTimer();
                results=(List<OPP_Project__C>)searchResults[0];
                
                if(results.size()>30)
                 disableCreateProjectbutton=true;
                else
                 disableCreateProjectbutton=false;
            }
        }
        catch(Exception ex){    
            Apexpages.addMessage(new ApexPages.Message (ApexPages.Severity.ERROR,ex.getmessage()));
        }
        finally{
            Utils_SDF_Methodology.stopTimer();
            Utils_SDF_Methodology.log('END search');
            Utils_SDF_Methodology.Limits();
        }
    
    }
}