@isTest
public class Schedule_BusinessRiskEsclationAlt_Test{

    public static testMethod void CreateData(){
    Test.startTest();
        set<BusinessRiskEscalations__c> setBRE1 = new  set<BusinessRiskEscalations__c>();
        set<ID> setBREId = new set<ID>();
        List<User> lstUser = new List<User>();
        Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
        User newUser = Utils_TestMethods.createStandardUser('TestUser');    
         newUser.UserRoleID = rol_ID ;
         system.debug(newUser.UserRoleID +'shiv@deep' + rol_ID);
         lstUser.add(newUser);

         User newUser2 = Utils_TestMethods.createStandardUser('TestUse2');  
            newUser2.BypassVR__c =TRUE;
            newUser2.UserRoleID = rol_ID ;
            lstUser.add(newUser2);

        Insert lstUser;
        
         Country__c newCountry = Utils_TestMethods.createCountry();
              newCountry.Region__c=Label.CL00320;
         Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
           
         if(!CountryInsertResult.isSuccess())
             Database.Error err = CountryInsertResult.getErrors()[0];
         
         Account acct = Utils_TestMethods.createAccount();
         acct.RecordtypeID = [SELECT Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' AND Name != 'Supplier' limit 1].Id;
         Database.SaveResult AccountInsertResult = Database.insert(acct, false);
           
        if(!AccountInsertResult.isSuccess())
            Database.Error err = AccountInsertResult.getErrors()[0];
        
         system.runas(newUser2){
             BusinessRiskEscalationEntity__c oResolutionBREE1 = new BusinessRiskEscalationEntity__c();
             oResolutionBREE1.SubEntity__c =  ''; 
             oResolutionBREE1.Location__c = '';
             oResolutionBREE1.Entity__c = 'TESTi2pEntity';
             oResolutionBREE1.Location_Type__c = 'Adaptation Center';
            BusinessRiskEscalationEntity__c oResolutionBREE = new BusinessRiskEscalationEntity__c();
             oResolutionBREE.SubEntity__c =  'TESTi2pSubEntity'; 
             oResolutionBREE.Location__c = '';
             oResolutionBREE.Entity__c = 'TESTi2pEntity';
             oResolutionBREE.Location_Type__c = 'Adaptation Center';
            
            BusinessRiskEscalationEntity__c oOriginatingBREE = new BusinessRiskEscalationEntity__c();
             oOriginatingBREE.SubEntity__c = 'TESTi2pSubEntity'; 
             oOriginatingBREE.Location__c = 'TESTi2pLocation';
             oOriginatingBREE.Entity__c = 'TESTi2pEntity';
             oOriginatingBREE.Location_Type__c = 'Country Front Office (verticalized)';
            
             list<BusinessRiskEscalationEntity__c> lstBREE1 = new list<BusinessRiskEscalationEntity__c>();
              lstBREE1.add(oResolutionBREE1);
             lstBREE1.add(oResolutionBREE);
             lstBREE1.add(oOriginatingBREE);
             insert lstBREE1;
             
             List<BusinessRiskEscalations__c> lsBRE = new List<BusinessRiskEscalations__c>();
             
             BusinessRiskEscalations__c BRE1 =  Utils_TestMethods.createBusinessRiskEscalation(acct.ID,newCountry.ID,newUser2.ID);
             
             BRE1.ResolutionOrganisation__c = oResolutionBREE.ID; 
             BRE1.OriginatingOrganisation__c =  oOriginatingBREE.ID; 
             BRE1.ResolutionLeader__c = newUser.ID;
             BRE1.Status__c='Open';
             BRE1.Actual_User_LastmodifiedDate__c=system.now()-8;
             BRE1.BusinessRiskSponsor__c = newUser.ID;  
             lsBRE.add(BRE1);
            
             BusinessRiskEscalations__c BRE4 =  Utils_TestMethods.createBusinessRiskEscalation(acct.ID,newCountry.ID,newUser2.ID);
             BRE4.ResolutionLeader__c = newUser.ID;
             BRE4.BusinessRiskSponsor__c = newUser.ID;
             BRE4.ResolutionOrganisation__c = oResolutionBREE.ID; 
             BRE4.OriginatingOrganisation__c =  oOriginatingBREE.ID; 
             BRE4.Actual_User_LastmodifiedDate__c=system.now()-8;
             lsBRE.add(BRE4);
             insert lsBRE;

        

             List<BusinessRiskEscalations__c> lsBREnew = [select id, ResolutionLeader__c, BusinessRiskSponsor__c from BusinessRiskEscalations__c];
             system.debug('#### lsBREnew'+lsBREnew);
             
             for(BusinessRiskEscalations__c oBRE11 :lsBREnew){
                 system.debug('oBRE11.ID'+oBRE11.ID);
                 setBREId.add(oBRE11.ID);
                 
             }
              system.debug('setBREId'+setBREId);
        }    
        
          Schedule_BusinessRiskEscalationAlert batch_BREA = new Schedule_BusinessRiskEscalationAlert(); 
        if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5)
            Database.executebatch(batch_BREA,1000);
        /*
        ProvideChatterDetails PCD = new ProvideChatterDetails();
        system.debug('Shiv deep setBREId'+ setBREId);
        PCD.createAlertForBRE_Stackholders(setBREId);
        PCD.returnObjectSet(setBREId);
        */
    Test.stopTest();
    }
    
    static testMethod void myUnitTest_schedule() {
            Test.startTest();
                    String CRON_EXP = '0 0 0 1 1 ? 2025';  
                    String jobId = System.schedule('Schduel Schedule_BusinessRiskEscalationAlert', CRON_EXP, new Schedule_BusinessRiskEscalationAlert() );

            Test.stopTest();
    }
}