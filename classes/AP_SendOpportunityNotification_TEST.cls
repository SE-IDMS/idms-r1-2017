/**
11-Oct-2013
Shruti Karn  
PRM Oct13 Release    
Initial Creation: test class for AP_SendOpportunityNotification
 */
@isTest
private class AP_SendOpportunityNotification_TEST {
  
    
     static testMethod void myUnitTest() {
     
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
         
        //Partner Account       
        Account Acct = Utils_TestMethods.createAccount();
        Acct.country__c = country.id;
        insert Acct;
        Acct.PRMAccount__c  = true;
        Acct.IsPartner = true;
        update Acct;
        
        //Partner Contact
        Contact PartnerCon = new Contact(
            FirstName='Test',
            LastName='lastname',
            AccountId=Acct.Id,
            JobTitle__c='Z3',
            CorrespLang__c='EN',            
            WorkPhone__c='1234567890'
        );
        
        PartnerCon.Country__c = country.id ; 
        Insert PartnerCon;
        
        PartnerCon.PRMContact__c = true;
        PartnerCon.PRMUser__c = true;
        
        Update PartnerCon;
        
        
        //Feature Catalog
        FeatureCatalog__c aFcat = new FeatureCatalog__c(Name=System.label.CLAPR14PRM51,Description__c = System.label.CLAPR14PRM51);
        insert aFcat;
        
        
        //Create Global Program
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalPRogram.recordtypeid = Label.CLMAY13PRM15;
        globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
        insert globalProgram;
    
        //Global Level
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(globalProgram.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;   
        
        //Global Feature
        ProgramFeature__c pf = Utils_TestMethods.createProgramFeature(globalProgram.id,prg1.id,aFcat.id);
        insert pf;  
        
        
        //Country Program
        PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, null);
        countryProgram.ProgramStatus__C = Label.CLMAY13PRM47;
        countryProgram.Country__c = country.id;
        insert countryProgram;      
        
        //creates a program level for the country partner program
        ProgramLevel__c prgLevel1 = Utils_TestMethods.createCountryProgramLevel(countryProgram.id);
        insert prgLevel1;
        prgLevel1.levelstatus__c = 'active';
        update prgLevel1;
        
        ProgramFeature__c pfc = Utils_TestMethods.createProgramFeature(countryProgram.id,prgLevel1.id,aFcat.id);
        insert pfc;
        
        
        //Account Assigned Program
        ACC_PartnerProgram__c accPrg = new ACC_PartnerProgram__c(Account__c = Acct.id,Active__c=true,PartnerProgram__c=countryProgram.id,ProgramLevel__c=prgLevel1.id);
        insert accPrg;
        
       
       Opportunity opp = Utils_TestMethods.createOpenOpportunity(Acct.Id);
        insert opp;
        
        OPP_ValueChainPlayers__c OppVP = new OPP_ValueChainPlayers__c(
                                        Contact__c=PartnerCon.id,OpportunityName__c=opp.id,ContactRole__c='Primary Customer Contact');
                                        
        List<User> pUser = new List<User>();                                
        pUser = [Select id from User where Contactid = :PartnerCon.id];
        
        if(pUser.size() > 0)
        {
            OpportunityTeamMember oppTeam = Utils_TestMethods.createOppTeamMember(opp.Id, pUser[0].Id);
            oppTeam.TeamMemberRole='Channel Partner';
            insert oppTeam;
        
        }
        
            
        
        opp.PartnerInvolvement__c = 'PassedToPartner';
        opp.StageName = 'Test';
        opp.Amount = 2222.0;
        update opp;
         
       User u = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        u.country__c = country.countrycode__c;
        insert u;
        
        OpportunityTeamMember mem = Utils_TestMethods.createOppTeamMember(opp.Id, u.Id);
        insert mem;
        
        /*opp.StageName = 'Test2';
        update opp;
        
        opp.Amount = 2222.0;
        update opp;*/
        
       // opp.Amount = 10000.0;
       // update opp; 
       
       List<Opportunity> lstOpportunity = new List<Opportunity>();
       lstOpportunity.add(opp);
        
       test.startTest();        
        AP_SendOpportunityNotification.notifyOppLeader_OppTeam(lstOpportunity,Label.CLOCT13PRM44);    
        test.stopTest();
        List<id> lstOpp = new List<id>();
        lstOpp.add(opp.id);
        AP_Opportunity_SendNotification.sendPendingOppNotification(lstOpp);
        AP_Opportunity_SendNotification.sendRevokeOppNotification(lstOpp);
        
        
     }
}