@isTest
Public Class VFC_PortletCasesView_TEST{
  Static TestMethod Void UnitTest1(){
       Integer max = 5;
    User RunningUser = [select id,name,IsActive,BypassVR__c, profile.Name from user where profile.Name = 'System Administrator' and BypassVR__c=true and IsActive=True limit 1];
      system.RunAs(RunningUser){
       UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        String randomString = EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(1,max).toUpperCase();
        Profile profile = [select id from profile where name='System Administrator'];
       User Usr =New User();
            Usr.UserRoleId =portalRole.Id; Usr.alias = 'testusr';Usr.email='testusr'+ '@accenture.com';Usr.emailencodingkey='UTF-8';Usr.lastname='Testing';Usr.languagelocalekey='en_US'; 
            Usr.localesidkey='en_US';Usr.profileid = profile.Id;Usr.BypassWF__c = true;
            Usr.timezonesidkey='Europe/London';Usr.username='testusr'+randomString+ '@bridge-fo.com';Usr . CAPRelevant__c=true;Usr . VisitObjDay__c=1;
            Usr.AvTimeVisit__c=3;Usr .WorkDaysYear__c=200;Usr .BusinessHours__c=8;Usr .UserBusinessUnit__c='Cross Business';Usr .Country__c='FR';
            Usr.BypassVR__c=true;//Usr.ContactId=con.Id;
        Insert Usr;
    
    Id uID= UserInfo.getUserId(); 
    account a = new account(name = 'name');
    a.OwnerId = Usr.Id;
    insert a;
    Contact c = new Contact(AccountId = a.Id, Workphone__c = '-12345',lastname='tectcontactportal');
    insert c;  
    List<Case> lcases = new List<case>();
    List<Case> lcases2 = new List<case>();
    List<Case> lcases3 = new List<case>();
    
    case cz1 = new case(AccountId=a.id,ContactId=c.id, subject='case1', status='New',ProductLine__c='IDFLD - FIELD DEVICES');
    insert CZ1;
    case cz2 = new case(AccountId=a.id,ContactId=c.id, subject='case2', status='New',ProductLine__c='IDFLD - FIELD DEVICES');
    insert CZ2;
    case cz3 = new case(AccountId=a.id,ContactId=c.id, subject='case3', status='Open/ Waiting For Details - Customer',ProductLine__c='IDFLD - FIELD DEVICES');
    insert CZ3;
    case cz4 = new case(AccountId=a.id,ContactId=c.id, subject='case4', status='Not Closed',ProductLine__c='IDFLD - FIELD DEVICES');
    insert CZ4;
    case cz5 = new case(AccountId=a.id,ContactId=c.id, subject='case5', status='Closed',ProductLine__c='IDFLD - FIELD DEVICES');
    insert CZ5;
    case cz6 = new case(AccountId=a.id,ContactId=c.id, subject='case6', status='Closed',ProductLine__c='IDFLD - FIELD DEVICES');
    insert CZ6;
    
    lcases.add(cz1); lcases.add(cz2);
    lcases2.add(cz3); lcases2.add(cz4);   
    lcases3.add(cz5); lcases3.add(cz5); 
    PageReference pageRef = Page.VFP_PortletCasesView;
     //ApexPages.VFP_CustomerPortalCls pageRef = new ApexPages.VFP_CustomerPortalCls; 
    Test.setCurrentPage(pageRef);
        
    VFC_PortletCasesView controller = new VFC_PortletCasesView(); 
    PageReference nextPage = controller.casesList();
    
    controller.getIsAuthenticated();
    
        
    controller.setusid(UserInfo.getUserId());
    controller.setcontid(c.id);
    controller.setaccid(a.id); 
    
     controller.setSelectedPcasesView('MOC');
     controller.setSelectedPcasesView('AMC');
    controller.getSelectedPcasesView(); 
     controller.setpcasesview('All My Open Cases');
     controller.setpcasesview('All My Cases');
     
     //controller.setSelected(lcases);
    controller.setCasess(lcases);
    

    //Testting closed cases    
    controller.setpcasesview('All My Closed Cases');    
    controller.setCasess(lcases2);
    //controller.setSelected(lcases2);
   // controller.getTickets();    
     
    
     // Testting Not closed cases
    controller.setpcasesview('All My Open Cases');    
    
    List<SelectOption> s = controller.getpcasesview ();  
    
    controller.setCasess(lcases3);
    controller.getrecordsDisplay();
    controller.setrecordsDisplay();
   
    //controller.setCon.setSelected(lcases3);  
   }      
    
  }
}