@isTest
public class VFC_PRMBackEndOperations_TEST{   
    public static testMethod void testMethodOne() {
        PageReference pageRef = new PageReference('VFP_PRMBackEndOperations'); 
        Test.setCurrentPage(pageRef);
        PermissionSet pSet = [SELECT Id FROM PermissionSet WHERE Name = 'PRMManageLocalCountryChannel'];
        VFC_PRMBackEndOperations prmSupport = new VFC_PRMBackEndOperations();
        VFC_PRMBackEndOperations.getpermissionSetList();
        VFC_PRMBackEndOperations.ObjectFieldsMap('CountryChannels__c');
        prmSupport.selectedObject = 'CountryChannels__c';
        prmSupport.selectedPermissionSet = pSet.Id;
        prmSupport.displayObjectFieldPermissions();
        prmSupport.doSavePermissions();
    }
}