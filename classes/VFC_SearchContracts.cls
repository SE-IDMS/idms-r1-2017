public class VFC_SearchContracts extends VFC_ControllerBase{
        public CTR_ValueChainPlayers__c newValueChainPlayer{get;set;}
        public list<CTR_ValueChainPlayers__c> lstValueChainPlayers {get;set;}
        public list<CTR_ValueChainPlayers__c> lstValueChainPlayers1 ;
        public list<CTR_ValueChainPlayers__c> lstValueChainPlayers2 ;
        public set<CTR_ValueChainPlayers__c> CVCPobjset = new Set<CTR_ValueChainPlayers__c>();
        public String actionLabel{get;set;} 
        public List<SelectOption> columns{get;set;} 
        public String ValueChainId{get;set;}
        public String caseId {get;set;}
        public List<DataTemplate__c> resultList{get;set;} // Results of the search
        public Boolean displayError {get;set;}
        public String PinNumber{get;set;}
        
        public VFC_SearchContracts.VFC_SearchContracts(ApexPages.StandardController controller)
        {
            displayError = false;
            resultList= new list<DataTemplate__c>();
            newValueChainPlayer = new CTR_ValueChainPlayers__c();
            newValueChainPlayer = (CTR_ValueChainPlayers__c) controller.getRecord();
            if(ApexPages.currentPage().getParameters().containsKey('CaseId'))
                caseId = ApexPages.currentPage().getParameters().get('CaseId');
           if(ApexPages.currentPage().getParameters().containsKey('PINNumber')&& ApexPages.currentPage().getParameters().get('PINNumber')!=null )
             newValueChainPlayer.LegacyPIN__c = ApexPages.currentPage().getParameters().get('PINNumber');
            if(ApexPages.currentPage().getParameters().get('accountid')!=null && ApexPages.currentPage().getParameters().get('accountid').trim() != '')
                newValueChainPlayer.account__c = ApexPages.currentPage().getParameters().get('accountid');
            if(ApexPages.currentPage().getParameters().get('contactid')!=null && ApexPages.currentPage().getParameters().get('contactid').trim() != '')
                newValueChainPlayer.contact__c = ApexPages.currentPage().getParameters().get('contactid');    
            actionLabel = Label.CL00815;
            
            System.debug('newValueChainPlayer.LegacyPIN'+newValueChainPlayer.LegacyPIN__c);
            columns = new List<SelectOption>();        
            columns.add(new SelectOption('field1__c', sObjectType.Account.fields.Name.getLabel()));       
            columns.add(new SelectOption('field2__c', sObjectType.Contract.fields.ContractNumber.getLabel()));
            columns.add(new SelectOption('field3__c', sObjectType.Contract.fields.Name.getLabel()));
            columns.add(new SelectOption('field4__c', sObjectType.CTR_ValueChainPlayers__c.fields.Points__c.getLabel()));
            columns.add(new SelectOption('field6__c', sObjectType.CTR_ValueChainPlayers__c.fields.LegacyPIN__c.getLabel()));
            columns.add(new SelectOption('field5__c', 'Contact Name'));
            searchContract();
        }
        
        public void searchContract()
        {
            system.debug('in search contract');
            system.debug('LegacyPin:'+newValueChainPlayer.LegacyPIN__c);
            lstValueChainPlayers = new list<CTR_ValueChainPlayers__c>();
            lstValueChainPlayers1 = new list<CTR_ValueChainPlayers__c>();
            lstValueChainPlayers2 = new list<CTR_ValueChainPlayers__c>();
            //resultList= new list<DataTemplate__c>();
            resultList.clear();   
            lstValueChainPlayers.clear();
            CVCPobjset.clear();
            
            //BR-7147 Grace Period
            //String queryString = 'Select id,LegacyPIN__c,name,Contract__c,contract__r.Name,Points__c,contract__r.AccountId,Account__r.Name,Contact__c,contact__r.Name, contract__r.ContractNumber from CTR_ValueChainPlayers__c where Contract__r.status='+'\'' +System.Label.CLSEP12CCC54+'\'' ;
            String queryString = 'Select id,LegacyPIN__c,name,Contract__c,contract__r.Name,Points__c,contract__r.AccountId,Account__r.Name,Contact__c,contact__r.Name, contract__r.ContractNumber from CTR_ValueChainPlayers__c where (Contract__r.status='+'\'' +System.Label.CLSEP12CCC54+'\' OR Contract__r.status='+'\'' +System.Label.CLJUN15CCC02+'\')' ;
            if(newValueChainPlayer.account__c != null && newValueChainPlayer.contact__c != null && newValueChainPlayer.LegacyPIN__c != null&& newValueChainPlayer.LegacyPIN__c !='')
                queryString = queryString + ' and account__c   ='+'\''+ newValueChainPlayer.account__c+'\''+' AND contact__c ='+ '\''+newValueChainPlayer.contact__c+'\''+' and LegacyPIN__c ='+'\'' + newValueChainPlayer.LegacyPIN__c+'\'' ;
            else if(newValueChainPlayer.account__c != null && newValueChainPlayer.contact__c != null)
                queryString = queryString + ' and account__c   ='+'\''+ newValueChainPlayer.account__c+'\''+' AND contact__c ='+'\''+ newValueChainPlayer.contact__c+'\'';
            else if(newValueChainPlayer.account__c != null && newValueChainPlayer.LegacyPIN__c != null&& newValueChainPlayer.LegacyPIN__c !='')
                queryString = queryString + ' and account__c   ='+'\''+ newValueChainPlayer.account__c+'\''+' AND LegacyPIN__c =' +'\'' +newValueChainPlayer.LegacyPIN__c+'\'';
            else if(newValueChainPlayer.contact__c != null && newValueChainPlayer.LegacyPIN__c != null&& newValueChainPlayer.LegacyPIN__c !='') 
                queryString = queryString + ' and contact__c ='+'\''+ newValueChainPlayer.contact__c+'\''+' AND LegacyPIN__c ='+'\'' + newValueChainPlayer.LegacyPIN__c+'\'';
            else if(newValueChainPlayer.account__c != null && newValueChainPlayer.LegacyPIN__c =='')
                queryString = queryString + ' and account__c   ='+'\''+ newValueChainPlayer.account__c+'\'';
            else if(newValueChainPlayer.contact__c != null && newValueChainPlayer.LegacyPIN__c =='')
                queryString = queryString + ' and contact__c ='+'\''+ newValueChainPlayer.contact__c+'\'';
            else if(newValueChainPlayer.LegacyPIN__c != null)
                queryString = queryString + ' and LegacyPIN__c ='+ '\'' +newValueChainPlayer.LegacyPIN__c+'\'' ;
            
           
            
            queryString = queryString + ' order by account__c, contract__r.ContractNumber';
            queryString = queryString + ' limit 100';
            system.debug('queryString :'+queryString );
            lstValueChainPlayers1 = database.query(queryString);
            system.debug('lstValueChainPlayers1 :'+lstValueChainPlayers1);
            system.debug('lstValueChainPlayerssize:'+lstValueChainPlayers1.size());
            
        if(lstValueChainPlayers1.size()<100){ 
            //BR-7147 Grace Period
            //String queryString1 = 'Select id,LegacyPIN__c,name,Contract__c,contract__r.Name,Points__c,contract__r.AccountId,Account__r.Name,Contact__c,contact__r.Name, contract__r.ContractNumber from CTR_ValueChainPlayers__c where Contract__r.status='+'\'' +System.Label.CLSEP12CCC54+'\'' ;
            String queryString1 = 'Select id,LegacyPIN__c,name,Contract__c,contract__r.Name,Points__c,contract__r.AccountId,Account__r.Name,Contact__c,contact__r.Name, contract__r.ContractNumber from CTR_ValueChainPlayers__c where (Contract__r.status='+'\'' +System.Label.CLSEP12CCC54+'\' OR Contract__r.status='+'\'' +System.Label.CLJUN15CCC02+'\')' ;
            if(newValueChainPlayer.account__c != null && newValueChainPlayer.contact__c != null && newValueChainPlayer.LegacyPIN__c != null&& newValueChainPlayer.LegacyPIN__c !='')
                queryString1 = queryString1 + ' and account__c ='+'\''+ newValueChainPlayer.account__c+'\''+' AND contact__c ='+ '\''+newValueChainPlayer.contact__c+'\''+' and LegacyPIN__c ='+'\'' + newValueChainPlayer.LegacyPIN__c+'\'' ;
            else if(newValueChainPlayer.account__c != null && newValueChainPlayer.contact__c != null)
                queryString1 = queryString1 + ' and (account__c  ='+'\''+ newValueChainPlayer.account__c+'\''+' OR contact__c ='+'\''+ newValueChainPlayer.contact__c+'\')';
            else if(newValueChainPlayer.account__c != null && newValueChainPlayer.LegacyPIN__c != null&& newValueChainPlayer.LegacyPIN__c !='')
                queryString1 = queryString1 + ' and (account__c ='+'\''+ newValueChainPlayer.account__c+'\''+' OR LegacyPIN__c =' +'\'' +newValueChainPlayer.LegacyPIN__c+'\')' ;
            else if(newValueChainPlayer.contact__c != null && newValueChainPlayer.LegacyPIN__c != null&& newValueChainPlayer.LegacyPIN__c !='') 
                queryString1 = queryString1 + ' and (contact__c ='+'\''+ newValueChainPlayer.contact__c+'\''+' OR LegacyPIN__c ='+'\'' + newValueChainPlayer.LegacyPIN__c+'\')' ;
            else if(newValueChainPlayer.account__c != null && newValueChainPlayer.LegacyPIN__c =='')
                queryString1 = queryString1 + ' and account__c  ='+'\''+ newValueChainPlayer.account__c+'\'';
            else if(newValueChainPlayer.contact__c != null && newValueChainPlayer.LegacyPIN__c =='')
                queryString1 = queryString1 + ' and contact__c ='+'\''+ newValueChainPlayer.contact__c+'\'';
            else if(newValueChainPlayer.LegacyPIN__c != null)
                
            queryString1 = queryString1 + ' order by account__c, contract__r.ContractNumber';
            queryString1 = queryString1 + ' limit 100';
            system.debug('queryString1 :'+queryString1 );
            lstValueChainPlayers2 = database.query(queryString1);
            system.debug('lstValueChainPlayers2:'+lstValueChainPlayers2);
        
            system.debug('lstValueChainPlayers:'+lstValueChainPlayers);     
            for(CTR_ValueChainPlayers__c cvcpobj: lstValueChainPlayers1 ){
                lstValueChainPlayers.add(cvcpobj);
                CVCPobjset.add(cvcpobj);
            }
            system.debug('lstValueChainPlayers:'+lstValueChainPlayers);  
            system.debug('CVCPobjset:'+CVCPobjset);
            system.debug('CVCPobjset size:'+CVCPobjset.size());
            for(CTR_ValueChainPlayers__c cvcpobj: lstValueChainPlayers2 ){
                if(!CVCPobjset.contains(cvcpobj))
                lstValueChainPlayers.add(cvcpobj);
            }
            
            system.debug('CVCPobjset:'+CVCPobjset);
            system.debug('CVCPobjsetSize:'+CVCPobjset.size());
            system.debug('lstValueChainPlayers:'+lstValueChainPlayers); 
            //integer differSize= lstValueChainPlayers2.size()-lstValueChainPlayers1.size();   
            if(lstValueChainPlayers != null && lstValueChainPlayers.size()>100){
                displayError = true;
                integer i=100;
                while(i<lstvaluechainplayers.size()){
                    lstvaluechainplayers.remove(i);
                    i++;                            
                }      
            }
        } 
            else{
                lstvaluechainplayers.addall(lstValueChainPlayers1);
                System.debug('-->>'+lstvaluechainplayers.size());
               
            }          
            if(lstValueChainPlayers != null && lstValueChainPlayers.size()>0){
                list<Id> lstAccountId = new list<Id>();
                for(CTR_ValueChainPlayers__c c:lstValueChainPlayers)
                { 
                    //system.debug('lstValueChainPlayers[0].contract__r.AccountId:'+c.contract__r.AccountId);
                    lstAccountId.add(c.Account__c);
                }
               
                map<Id,Account> mapAccount = new map<Id,Account>([Select id, name from Account where id in : lstAccountId limit 10000]);
              
                for(CTR_ValueChainPlayers__c ctrl:lstValueChainPlayers){
                    DataTemplate__c newResult = new DataTemplate__c();
                    if(mapAccount.containskey(ctrl.Account__c)){
                        newResult.field1__c = '<input type="hidden" value=\"'+mapAccount.get(ctrl.Account__c).Name+'\"><a href=\"/'+ctrl.Account__c+'\">'+mapAccount.get(ctrl.Account__c).Name+'</a>';
                    }               
                    newResult.field2__c = '<input type="hidden" value=\"'+ctrl.contract__r.ContractNumber+'\"><a href=\"/'+ctrl.contract__c+'\">'+ctrl.contract__r.ContractNumber+'</a>';
                    newResult.field3__c = '<input type="hidden" value=\"'+ctrl.contract__r.Name+'\"><a href=\"/'+ctrl.contract__c+'\">'+ctrl.contract__r.Name+'</a>';
                    newResult.field4__c = String.valueOf(ctrl.points__c);
                    newResult.field5__c = String.valueOF(ctrl.Contact__r.Name);
                    newResult.field6__c = String.valueOF(ctrl.LegacyPIN__c);
                    newResult.field7__c = ctrl.Id;
                    newResult.field8__c = ctrl.Account__c;
                    if(mapAccount.containskey(ctrl.Account__c))
                        newResult.field9__c = mapAccount.get(ctrl.Account__c).Name;
                    newResult.field10__c = ctrl.contact__c;
                    newResult.field11__c = ctrl.contact__r.Name;
                    newResult.field12__c = ctrl.Name;
                    newResult.field13__c = ctrl.contract__r.Name;
                    resultList.add(newResult);
                }
            }
       }
        public PageReference clearFilters(){
            newValueChainPlayer.contact__c = null;
            newValueChainPlayer.account__c = null;
            newValueChainPlayer.LegacyPIN__c = '';
            resultList.clear();
            return null;
        }
        
        public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase){ 
          Id valueChainID;
            Id accountID;
            Id contactId;
            String VCPName;
            String acctName;
            String contName;
            String vcpContract;
            
            DataTemplate__c newResult = new DataTemplate__c();
            newResult = (DataTemplate__c) obj;
            if(newResult.field7__c!= null){
                valueChainID = newResult.field7__c;
                VCPName = newResult.field12__c;
            }
            if(newResult.field8__c != null){
                accountID = newResult.field8__c;
                acctName = newResult.field9__c;
            }
            if(newResult.field8__c != null){
                contactId = newResult.field10__c;
                contName = newResult.field11__c;
            }   
            if(newResult.field13__c != null){
               vcpContract = newResult.field13__c;
            }  
            PageReference page;
            if(caseId != null && caseId.trim() != ''){
                Case c = [Select CTRValueChainPlayer__c from Case where id =: caseId limit 1];
                //c.ContractContact__c = vcpContract ;
                c.CTRValueChainPlayer__c = valueChainID ;
                //c.ContractName__c = vcpContract ;
                try{
                    update c;
                    page = new PageReference('/'+caseId);
                }
                catch(Exception e){
                    for (Integer i = 0; i < e.getNumDml(); i++) { 
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),'')); System.debug(e.getDmlMessage(i)); 
                    } 
                }
            }
            else{
                String url = '/500/e?';
                url = url + Label.CLSEP12CCC18+'='+VCPName+'&'+Label.CLSEP12CCC19+'='+valueChainID;//+'&'+Label.CLSEP12CCC32+'='+vcpContract+'&cas4='+acctName +'&cas4_lkid='+accountID;
                if(contactID != null)
                    url = url +'&cas3='+contName +'&cas3_lkid='+contactId;
                if(accountID != null)
                    url = url + '&cas4='+acctName +'&cas4_lkid='+accountID;
                page = new PageReference(url);
            } 
         return page ;  
       }
    }