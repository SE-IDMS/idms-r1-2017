@isTest

public class AP_ProblemCaseLink_TEST{

    public static testMethod void Testmethod1() {
        
        string prefix='aa';
        User  sampleUser=Utils_TestMethods.createStandardUser('sample'+prefix);
        Database.insert(sampleUser);
        
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='TCY'; 
        objCountry.CountryCode__c='TCY';
        Database.insert(objCountry);
        
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = objCountry.Id;
        Database.insert(objAccount);
        
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        String commRefTOTest='TEST_XBTGT5430';
        String gmrCodeToTest='02BF6DRG1234'; 
        
        Case objCase = Utils_TestMethods.createCase(objAccount.Id,objContact.Id,'In Progress');
        objCase.CaseSymptom__c= 'Symptom';
        objCase.CaseSubSymptom__c= 'SubSymptom';
        objCase.CommercialReference__c = '';
        objCase.TECH_GMRCode__c = gmrCodeToTest;
        objCase.ProductBU__c = 'Business';
        objCase.ProductLine__c = 'ProductLine';
        objCase.ProductFamily__c ='ProductFamily';
        objCase.Family__c ='Family';
        Database.insert(objCase);
           
        BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
        Database.insert(breEntity);
        BusinessRiskEscalationEntity__c breSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
        Database.insert(breSubEntity);
        BusinessRiskEscalationEntity__c breLocationType = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation ();
        Database.insert(breLocationType);
        
        List<CS_Severity__c> lstCS = new List<CS_Severity__c>();
        CS_Severity__c CS1 = new CS_Severity__c(Name = 'Minor', Value__c = 1);
        lstCS.add(CS1);
        CS_Severity__c CS2 = new CS_Severity__c(Name = 'Safety related', Value__c = 10);
        lstCS.add(CS2);
        Database.insert(lstCS);
         
        Problem__c prob = Utils_TestMethods.createProblem(breLocationType.Id,12);
        prob.ProblemLeader__c = sampleUser.id;
        prob.ProductQualityProblem__c = false;
        prob.TECH_Symptoms__c = 'Symptom - SubSymptom'; 
        prob.RecordTypeid = Label.CLI2PAPR120014;
        prob.Sensitivity__c = 'Public';
        Database.insert(prob); 
        
        CommercialReference__c crs = new CommercialReference__c(Problem__c = prob.Id, Business__c = 'Business', 
                                                                CommercialReference__c = commRefTOTest,Family__c = 'Family', 
                                                                GMRCode__c = gmrCodeToTest,ProductLine__c = 'ProductLine', 
                                                                ProductFamily__c ='ProductFamily');
                                                                
        ProblemCaseLink__c PCL = new ProblemCaseLink__c(Problem__c = prob.Id, Case__c = objCase.Id);
        insert PCL;
        delete PCL;
    }
}