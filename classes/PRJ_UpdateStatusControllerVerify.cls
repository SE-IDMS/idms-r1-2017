public without sharing class PRJ_UpdateStatusControllerVerify
{
    public static boolean updateProjectRequest(PRJ_ProjectReq__c oPR,Change_Request__c cHR,boolean isOwner,string strStatusOnLoad)
    {
        //Modified By Ramakrishna For June Release
        set<String> stSet = new set<String>();
        string st = System.Label.DMT_AuthorizedUserIDForValid;
        list<String> stList = st.split(',');
        for(String s: stList){
            stSet.add(s);
        }
        
        if(isOwner || UserInfo.getProfileId()== System.Label.CL00110 || stSet.contains(UserInfo.getUserId()))
        {
            if(oPR!=null)
                return(updateStatus(oPR,null,strStatusOnLoad));
            else
                return(updateStatus(null,cHR,strStatusOnLoad));
        }
        else
        {
            if(oPR.NextStep__c == System.Label.DMT_StatusProjectOpen || oPR.NextStep__c == System.Label.DMT_StatusCreated ||oPR.NextStep__c == System.Label.DMT_StatusValid || oPR.NextStep__c == strStatusOnLoad || oPR.NextStep__c == System.Label.DMT_StatusQuoted )
            {       
                if(oPR!=null)
                return(updateStatus(oPR,null,strStatusOnLoad));
            else
                return(updateStatus(null,cHR,strStatusOnLoad));
       
            }
            else
            {
                ApexPages.addMessages(new noMessageException(System.Label.DMT_MessageNotAuthorizedToChangeStatus));              
                return false;
            }
                
         }
         return false;
        
    }
    
    public static boolean updateStatus(PRJ_ProjectReq__c oPR,Change_Request__c cHR,string strStatusOnLoad)
    {
        try
        {   
            if(oPR!=null){           
                update oPR;
                //Modified for May, 2012 Release by Sreedevi Surendran (Schneider Electric)
                //begin 
                if(oPR.NextStep__c == System.Label.DMT_StatusCreated && oPR.NextStep__c <> strStatusOnLoad && (oPR.WorkType__c == 'Master Project' || oPR.WorkType__c == 'Sub Project'))  
                {
                    System.Debug('Email 2');                     
                    PRJ_ProjectUtils.sendCreatedNotification(new PRJ_ProjectReq__c[]{oPR},'Change of Status');
                }
                if(Test.isRunningTest()){
                    //Modified for July, 2012 Release by Srikant
                    //begin
                    //throw new noMessageException('Test Exception');  
                    oPR.ownerId = '012A0000000naZT';
                    update oPR;
                }
                //end                        
             }else{
                 update cHR;
             }
            return true;
        }
        catch(DmlException dmlexp)
        {
            for(integer i = 0;i<dmlexp.getNumDml();i++)
                ApexPages.addMessages(new noMessageException(dmlexp.getDmlMessage(i))); 
            return false;                     
        }
    /*    catch(Exception exp)
        {
            ApexPages.addMessages(new noMessageException(exp.getMessage()));
            return false;
        } */
                //End
    }
    
    public static boolean updatePriority(PRJ_ProjectReq__c oPR,Change_Request__c cHR)
    {
        try
        {
            if(oPR!=null){
                update oPR;
            }
            else
                update cHR;   
            return true;
        }
        catch(DmlException dmlexp)
        {
            for(integer i = 0;i<dmlexp.getNumDml();i++)
                ApexPages.addMessages(new noMessageException(dmlexp.getDmlMessage(i))); 
            return false;                     
        }
    /*    catch(Exception exp)
        {
            ApexPages.addMessages(new noMessageException(exp.getMessage()));
            return false;
        } */
               
    }
  /*  public static void sendSuccessEmailFunction(List<String> listEmails,String status,String projectName,String projectId){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(listEmails);
        email.setSubject('This is to inform you that the demand- '+projectName+' - is moved to the gate ' + status);
        email.setHtmlBody('<br>'+UserInfo.getFirstName() +' '+UserInfo.getLastName()+' has moved the demand - '+projectName+' - to the gate '+ status+ '<br><br>'+
                        'Please click the link to access the project request :' + system.label.CL10071 +ProjectId+'<br><br>* This is an automatic message from Demand Management tool. Please do not reply to this message.');
        email.SetccAddresses(new String[] { UserInfo.getUserEmail(),'joshisrik@gmail.com' });
        List<Messaging.SendEmailResult> results = 
            Messaging.sendEmail(new Messaging.Email[] { email });
        if (!results.get(0).isSuccess()) {
            System.StatusCode statusCode = results.get(0).getErrors()[0].getStatusCode();
            String errorMessage = results.get(0).getErrors()[0].getMessage();
        }
    }*/
    
    
    public class noMessageException extends Exception{} 
    
}