/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Alexande Gonzalo                                                         |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | VFC102_CloneTMTSession_TEST                                                        |
|                       |                                                                                    |
|     - Object(s)       | TMT Session                                                                        |
|     - Description     |   - Test method for cloning a TMT Session                                          |
|                       |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | April, 28th 2012                                                                |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
@isTest
public class VFC102_CloneTMTSession_TEST
{
    static testMethod void VFC102_CloneTMTSession_TEST()
    {
        TMT_Session__c session = new TMT_Session__c();
        session.Start__c = Date.today();
        session.Max__c = 10;
        insert session;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(session);
        VFC102_CloneTMTSession vfc102 = new VFC102_CloneTMTSession(sc);
        String url = vfc102.getClonePage().getUrl();
        
        System.assert(url.contains(session.Id));
        System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10241, 'UTF-8')));
        System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10242, 'UTF-8')));
        System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10243, 'UTF-8')));
        System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10248, 'UTF-8')));
        System.assert(url.contains(EncodingUtil.urlEncode(System.Label.CL10249, 'UTF-8')));
           
    }
    
}