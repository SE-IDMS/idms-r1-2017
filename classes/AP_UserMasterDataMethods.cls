public class AP_UserMasterDataMethods {

    public static List<String> accountFieldList = new List<String>{'Id', 'CurrencyIsoCode', 'Name', 'ClassLevel1__c', 'ClassLevel2__c', 'MarketServed__c',
                                                                   'EmployeeSize__c', 'AnnualRevenue', 'VATNumber__c', 'Street__c', 'AdditionalAddress__c',
                                                                   'City__c', 'ZipCode__c', 'StateProvince__c', 'Country__c','County__c'};

    public static List<String> contactFieldList = new List<String>{'Id', 'Name', 'Email', 'MobilePhone','WorkPhone__c','FirstName', 'LastName', 'Country__c', 'CorrespLang__c', 
                                                                   'CurrencyIsoCode', 'MidInit__c', 'Salutation', 'Department', 'Fax', 'RecordTypeId'};
    
    public static List<String> userFieldList = new List<String>{'CompanyName','IDMS_POBox__c','Email','MobilePhone','Phone','FirstName','LastName','UserName',
                                                                'IDMS_Email_opt_in__c','Country','IDMS_PreferredLanguage__c','DefaultCurrencyIsoCode',
                                                                'Street','City','PostalCode','State','IDMSClassLevel1__c','IDMSClassLevel2__c',
                                                                'IDMSAnnualRevenue__c','IDMSTaxIdentificationNumber__c','Company_Website__c','IDMSCompanyMarketServed__c',
                                                                'IDMSCompanyHeadquarters__c','IDMSMiddleName__c','IDMSCompanyNbrEmployees__c',
                                                                'IDMSSalutation__c','Department','fax','Company_State__c','Company_Address1__c','Company_City__c',
                                                                'Company_Country__c','IDMSCompanyCounty__c','Company_Postal_Code__c'};
    
    public Static List<Contact> updatedContacts = new List<Contact>();
    
    public Static Set<Id> toBeDeletedAccountIdSet = new Set<Id>();    
    public Static Set<Id> existingAccountUserId = new Set<Id>();
    
    public Static List<User> existingAccountUsers = new List<User>();
    public Static List<User> L2UsersToEnrich = new List<User>();
    
    public Static bFoMatchingResult__c[] bFOMatchResults;
    public Static bFoMatchingResult__c[] bFOResults;
    
    @future
    public static void UpdateAccountAndContact(Set<ID> Ids){
    
        List<User> userList = new List<User>(); 
        
        if(ids.Size()>0){
            userList = [select id,name,IDMS_Registration_Source__c,IDMSIdentityType__c,IDMSDelegatedIdp__c,IDMSCompanyFederationIdentifier__c,
                        Fax,IDMSSuffix__c,Department,IDMSSalutation__c,Company_Website__c,IDMSMiddleName__c,IDMSTaxIdentificationNumber__c,
                        IDMSAnnualRevenue__c,IDMSCompanyHeadquarters__c,IDMSCompanyMarketServed__c,Username,IDMSCompanyNbrEmployees__c,
                        IDMSJobDescription__c,Job_Function__c,Job_Title__c,IDMSClassLevel2__c,IDMSClassLevel1__c,Company_Address2__c,
                        Company_Country__c,IDMSCompanyPoBox__c,Company_State__c,Company_Postal_Code__c,Company_City__c,Company_Address1__c,
                        CompanyName,IDMS_AdditionalAddress__c,IDMS_POBox__c,IDMS_County__c,State,PostalCode,City,Street,DefaultCurrencyIsoCode,
                        IDMS_PreferredLanguage__c,Email,Country,IDMS_User_Context__c,IDMS_Email_opt_in__c,IDMSContactGoldenId__c,
                        LastName,FirstName,Phone,MobilePhone,Country__c,contactId,contact.AccountId,accountId,SEContactIDUser__c,IDMSCompanyCounty__c from User where id =: ids and ContactId != null];
        }
        
        user[] users;
        
        if(userList.Size() > 0){
            users = userList;   
        }       
        
       
        if(users != null){
            enrichMasterData(users);
        }        
    }    
    
    public static void enrichAccountAndContact(user[] uimsUser){
     
        Map<Id, User> existingUserMap = new Map<Id, User>();
        List<User> changedUserList = new List<User>();
        List<User> progressiveList = new List<User>();
        
        if(uimsUser!= null){   
            for(User user: uimsUser) {
                existingUserMap.put(user.Id, null);
            }
            
    
            for(User user:[SELECT Id,CompanyName,IDMS_POBox__c,Email,MobilePhone,Phone,FirstName,LastName,
                                        IDMS_Email_opt_in__c,Country,IDMS_PreferredLanguage__c,DefaultCurrencyIsoCode,
                                        Street,City,PostalCode,State,IDMSClassLevel1__c,IDMSClassLevel2__c,Username,
                                        IDMSAnnualRevenue__c,IDMSTaxIdentificationNumber__c,Company_Website__c,IDMSCompanyMarketServed__c,
                                        IDMSCompanyHeadquarters__c,IDMSMiddleName__c,IDMSCompanyCounty__c,IDMSCompanyNbrEmployees__c,
                                        IDMSSalutation__c,Department,fax,Company_Postal_Code__c,Company_City__c,Company_Address1__c,
                                        Company_Country__c,Company_State__c  FROM user WHERE ID IN :existingUserMap.keySet()]) {
                existingUserMap.put(user.Id, user);
            }
            
            Boolean BYPASS_FORCE_L1 = false;
          
            if(Test.isRunningTest()) { 
                BYPASS_FORCE_L1 = True;
            }
            
            Boolean FORCE_L1 = (!(BYPASS_FORCE_L1) && Label.ForceLevelOne == 'true'); 
            
            for(integer i = 0; i < uimsUser.size(); i++) {
            
                User oldUser = existingUserMap.get(uimsUser[i].ID);
                User newUser = uimsUser[i];
                Boolean isChanged = false;
                
                
                if(newUser.AccountId != null && newUser.contactid != null) {
                    if(AP_MatchingModule.isLevel2(oldUser)){
                        for(String field:userFieldList) {
                            if(oldUser.get(field) == null && newUser.get(Field) != null) {
                                isChanged = true;
                            }                
                        }
                        
                        if(isChanged) {
                            changedUserList.add(newUser);
                        }
                    }
                }
            }
            
            if(changedUserList.size()!= 0 && changedUserList!= null){
                
                for(user usr:changedUserList){
                    if(AP_MatchingModule.isLevel2(usr) && FORCE_L1 == false) {
                        progressiveList.add(usr);    
                    }            
                }
            }
            
            user[] userProg = progressiveList;
            
            if(userProg.size() >0 && userProg != null){
                enrichMasterData(userProg);
            }
        }
    }
        
    private static void enrichMasterData(User[] users) {
    
        if(users!= null){
            List<Account> enrichedAccountList = enrichAccount(users);
            List<Contact> enrichedContactList = enrichContact(users);        
    
            if(!enrichedContactList.isEmpty()) {
                List<Database.SaveResult> contactSaveResults = Database.update(enrichedContactList);
            } 
            
            if(!enrichedAccountList.isEmpty()) {
                List<Database.SaveResult> accountSaveResults = Database.update(enrichedAccountList);
            }
        }       
    }

    public static List<Account> enrichAccount(User[] users) {
        
        List<Account> updatedAccount = new List<Account>();
        if(users!= null){
            Account[] accounts = AP_UserMapping.buildAccounts(users);    
            Map<Id, Account> existingAccountMap = new Map<Id, Account>();            
            
            for(User user:users) {
                existingAccountMap.put(user.AccountId, null);
            }
    
            for(Account account:[SELECT Id, CurrencyIsoCode, Name, ClassLevel1__c, ClassLevel2__c, MarketServed__c,
                                        NumberOfEmployees, AnnualRevenue, VATNumber__c, Street__c, AdditionalAddress__c,
                                        City__c, ZipCode__c, StateProvince__c, Country__c,EmployeeSize__c,County__c               
                                        FROM Account WHERE ID IN :existingAccountMap.keySet() and recordtype.name != :Label.CLQ316IDMS539 and 
                                        ToBeDeleted__c = false and Owner.Profile.Name = :Label.CLQ316IDMS530 and Inactive__c = false]) {
                existingAccountMap.put(account.Id, account);
            }
            
            if(existingAccountMap.size() > 0 && accounts != null){
                for(Integer i=0; i<users.size(); i++) {
                
                    Account existingAccount = existingAccountMap.get(users[i].AccountId);
                    Account builtAccount    = accounts[i];
                    Boolean isChanged = false;
                    
                    if(existingAccount != null && builtAccount != null){
                        for(String field:accountFieldList) {
                            if(existingAccount.get(field) == null && builtAccount.get(Field) != null) {
                                existingAccount.put(field, builtAccount.get(Field));
                                isChanged = true;
                            }                        
                        }                
                    }
                    
                    if(isChanged) {
                        
                        if(AP_MatchingModule.isLevel2(users[i]) && existingAccount.name == Label.CLQ316IDMS531) {
                            existingAccount.name = users[i].companyName;
                        }
                        
                        updatedAccount.add(existingAccount);
                    }
                }
            }
        }    
        return updatedAccount;
    }   
    
    public static List<Contact> enrichContact(User[] users) {

        Map<Id, Contact> existingContactMap = new Map<Id, Contact>();
        
        List<Contact> updatedContactList = new List<Contact>();
        List<user> userList = new List<User>();
        
        if(users!= null){
    
            for(User user:users) {
                if(user.ContactId!= null){
                    existingContactMap.put(user.ContactId, null);
                    userList.add(user);
                }
            }
            
            User[] userArray =userList; 
            Contact[] contacts = AP_UserMapping.buildContacts(userArray);
                    
            for(Contact contact:[SELECT Id, Email, Name, MobilePhone, FirstName, LastName, Country__c, CorrespLang__c, 
                                        CurrencyIsoCode, MidInit__c, Salutation, Department, Fax, WorkPhone__c, RecordTypeId 
                                        FROM Contact WHERE ID IN :existingContactMap.keySet() and InActive__c = False And 
                                        ToBeDeleted__c = False And Account.recordType.name != :Label.CLQ316IDMS539 
                                        And owner.profile.name =: Label.CLQ316IDMS530]) {
                existingContactMap.put(contact.Id, contact);
            }    
            
            if(existingContactMap.size() > 0){
                for(Integer i=0; i<userArray.size(); i++) {
                    Contact existingContact = existingContactMap.get(userArray[i].ContactId);
                    Contact builtContact    = contacts[i];
                    Boolean isChanged = false;
                    
                    if(existingContact != null && builtContact != null){
                        for(String field:contactFieldList) {
                            
                            if(existingContact.get(field) == null && builtContact.get(Field) != null) {
                                existingContact.put(field, builtContact.get(Field));
                                isChanged = true;
                            }                
                        }
                    }
                    
                    if(isChanged) {
                        updatedContactList.add(existingContact);
                    }
                } 
            }    
        }
        return updatedContactList;   
    }

    @future    
    public static void matchAndRelink(Set<Id> Ids) {
    
        List<User> userList = new List<User>(); 
        
        if(ids.Size()>0){
            userList = [select id,name,IDMS_Registration_Source__c,IDMSIdentityType__c,IDMSDelegatedIdp__c,IDMSCompanyFederationIdentifier__c,
                        Fax,IDMSSuffix__c,Department,IDMSSalutation__c,Company_Website__c,IDMSMiddleName__c,IDMSTaxIdentificationNumber__c,
                        IDMSAnnualRevenue__c,IDMSCompanyHeadquarters__c,IDMSCompanyMarketServed__c,Username,IDMSCompanyNbrEmployees__c,
                        IDMSJobDescription__c,Job_Function__c,Job_Title__c,IDMSClassLevel2__c,IDMSClassLevel1__c,Company_Address2__c,
                        Company_Country__c,IDMSCompanyPoBox__c,Company_State__c,Company_Postal_Code__c,Company_City__c,Company_Address1__c,
                        CompanyName,IDMS_AdditionalAddress__c,IDMS_POBox__c,IDMS_County__c,State,PostalCode,City,Street,DefaultCurrencyIsoCode,
                        IDMS_PreferredLanguage__c,Email,Country,IDMS_User_Context__c,IDMS_Email_opt_in__c,IDMSContactGoldenId__c,IDMSCompanyCounty__c,
                        LastName,FirstName,Phone,MobilePhone,Country__c,contactId,contact.AccountId,accountId,SEContactIDUser__c from User where id =: ids and ContactId!=null];
        }
        
        user[] users = new User[userList.Size()];
        if(userList.Size() > 0){
            users = userList;   
        }       
        
        processMatchAndRelink(users);
    }
    
    public static void matchAndRelink(User[] uimsUser) {
        
        if(uimsUser!= null){
            List<User> rematchList     = new List<User>();    
            
            for(User user: uimsUser) {
                if(user.TECH_MatchAndRelink__c && user.contactid != null) {
                    rematchList.add(user); 
                    user.TECH_MatchAndRelink__c = false;
                }
            }
            
            user[] userRematch = rematchList;
            
            if(userRematch.size() >0 && userRematch != null){
                processMatchAndRelink(userRematch);
            }
        }            
    }

    @TestVisible 
    private static void processMatchAndRelink(User[] users) {
        
        AP_MatchingModule.BYPASS_FORCE_L1 = True;
    
        Map<Id, Contact> contactIdMap = new Map<Id, Contact>();
        
        if(users != null){       
            List <Contact> contacts = enrichContact(users);
            if(contacts!= null){                 
                List<Database.SaveResult> contactUpdateResults1 = Database.update(contacts);
            }             
        }

        for(User user:users) {
            contactIdMap.put(user.ContactId, null);
        }
        
        for(Contact contact:[SELECT Id, AccountId FROM Contact WHERE ID IN :contactIdMap.keySet()]) {
            contactIdMap.put(contact.Id, contact);
        }
        
        Savepoint sp = Database.setSavepoint();
        bFOMatchResults = AP_MatchingModule.preMatch(users); 
        
        Boolean shouldRollBack  = false;
        Boolean existingAccount = false;
        List<bFoMatchingResult__c> bFOResultsList = New List<bFoMatchingResult__c>();
        
        for(Integer j=0; j<users.size(); j++) {
        
            User user = users[j];
            bFoMatchingResult__c bFOMatchingResult = new bFoMatchingResult__c();
            bFOMatchingResult = bFOMatchResults[j];
            
            AP_MatchingModule.appendDebug(Label.CLQ316IDMS542, bFOMatchingResult);
            bFOMatchingResult.Context__c = Label.CLQ316IDMS551;
            
            try{  
            
                Contact relatedContact = contactIdMap.get(user.ContactId);

                AP_MatchingModule.appendDebug(Label.CLQ316IDMS558+ relatedContact.AccountId, bFOMatchingResult);
                AP_MatchingModule.appendDebug(Label.CLQ316IDMS559+ bFOMatchingResult.MatchingAccount__c, bFOMatchingResult);

                if(relatedContact.AccountId != bFOMatchingResult.MatchingAccount__c 
                    && bFOMatchingResult.IsExistingAccount__c && AP_MatchingModule.isLevel2(user)) {
                    
                    toBeDeletedAccountIdSet.add(relatedContact.AccountId);
                    relatedContact.AccountId = bFOMatchingResult.MatchingAccount__c;                    
                    updatedContacts.add(relatedContact);
                    existingAccountUsers.add(user);
                    
                    existingAccount      = true;
                    bFOMatchingResult.ID = null;
                    bFOMatchingResult.Contact__c         = user.contactId;
                    AP_MatchingModule.appendDebug(Label.CLQ316IDMS549, bFOMatchingResult);
                    
                    bFOResultsList.add(bFOMatchingResult);                                        
                }
                else {
                    bFOMatchingResult.MatchingAccount__c   = user.contact.AccountId;
                    bFOMatchingResult.IsExistingAccount__c = true;
                    
                    bFOMatchingResult.MatchingAccount__c = user.AccountId;                    
                    bFOMatchingResult.Contact__c         = user.contactId;
                    bFOMatchingResult.Id                 = null;
                    
                    if(AP_MatchingModule.isLevel2(user)) {
                        L2UsersToEnrich.add(user);    
                        AP_MatchingModule.appendDebug(Label.CLQ316IDMS557, bFOMatchingResult);                    
                    }
                    else {
                        AP_MatchingModule.appendDebug(Label.CLQ316IDMS550, bFOMatchingResult);                                                            
                    }
                    
                    bFOResultsList.add(bFOMatchingResult);
                }
            }
            catch(Exception e){                
                
                AP_MatchingModule.appendDebug(Label.CLQ316IDMS543, bFOMatchingResult);
                bFOMatchingResult.MatchingAccount__c = null;
                bFOMatchingResult.Contact__c         = null;
                bFOMatchingResult.Status__c          = 'Fail';
                bFOMatchingResult.errorMessage__c    = bFOMatchingResult.ErrorMessage__c == null ? 
                                                       Label.CLQ316IDMS540 + e.getMessage()+Label.CLQ316IDMS541+ e.getLineNumber() : bFOMatchingResult.ErrorMessage__c;
                
                bFOMatchingResult.ID = Null;
                bFOResultsList.add(bFOMatchingResult);
            }

            AP_MatchingModule.appendDebug(Label.CLQ316IDMS556, bFOMatchingResult);            
            shouldRollBack = True;
        }
        
        bFOResults = bFOResultsList;
        
        if(shouldRollBack){            
            Database.rollback(sp);   
        }
        
        if(L2UsersToEnrich!= null) {
            enrichMasterData(L2UsersToEnrich);
        }   
        
        if(!updatedContacts.isEmpty()) {
            
            List<Database.SaveResult> contactUpdateResults = Database.update(updatedContacts);

            List<Account> toBeDeletedAccounts = [SELECT Id FROM Account WHERE Id IN :toBeDeletedAccountIdSet and RecordType.name = :Label.CLQ316IDMS537]; // TODO : And RecordType is Digital account
            
            if(!toBeDeletedAccounts.isEmpty()) {
                List<Database.DeleteResult> deleteResults = Database.delete(toBeDeletedAccounts);                
            }     
        }        
        
        if(bFOResults!= null){    
            List<Database.upsertResult> matchResultInsert1 = Database.upsert(bFOResults);
        }
        
    }    
}