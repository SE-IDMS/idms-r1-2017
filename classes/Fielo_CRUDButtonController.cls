public class Fielo_CRUDButtonController {
    
    public Fielo_PageComponent__c cButton {get;set;}
    public Id cRecordId {get;set;}
    
    public PageReference doAction(){
        PageReference pageRet;
        if (cButton.F_ButtonName__c == Label.Fielo_CRUDButtonControllerButtonName){
            pageRet = new PageReference(Label.Fielo_CRUDButtonControllerURL);
            pageRet.setRedirect(true);
        }
        return pageRet;
    }
    
}