/*

*/

public with sharing class VFC10_OptyGridController{
    public boolean isPowerPartner {get{return Userinfo.getUserType()=='PowerPartner'? true: false;}}
    public String stageProbability{get;set;}
    public string stageForecastCategory{get;set;}    
    
    List<ID> OppIdList = new List<ID>();
    
    public class OppContainer{
        public Opportunity Opp {get; set;}
        public String UpdateStatus {get; set;}
        public String Link {get; set;}
        public String ErrorMessage {get; set;}
        public boolean displayForecasts{get;set;}    
        public OppContainer(Opportunity o, boolean recAccess){
            this.Opp = o;
            this.UpdateStatus = '';
            this.ErrorMessage = '';
            this.displayForecasts = recAccess;
            this.Link ='/'+this.Opp.id+'/e?retURL=%2F'+this.Opp.id;  
        }    
    }
    
    public List<Opportunity> OppList{get; set;}
    public boolean DisplayResult {get;set;}
    
    public List<Opportunity> RetrieveOppList(List<Opportunity> OppList){
        
        List<Opportunity> RetrievedOppList = new List<Opportunity>();
        if(OppList.size() > 200){
            this.DisplayResult = false;
        }
        else{
            this.DisplayResult = true;
            for(Opportunity o:OppList){
                OppIdList.add(o.id);
            } 
            RetrievedOppList = [SELECT ID, Name, ForecastCategory, ForecastCategoryName, StageName, Status__c,Reason__c, OpportunityCategory__c, CurrencyIsoCode, Amount, Location__c, Probability, IncludedInForecast__c, CloseDate, QuoteSubmissionDate__c, CustomerSelected__c, Account.Name, MarketSegment__c, MarketSubSegment__c FROM Opportunity WHERE ID IN: OppIdList];
        }
        return RetrievedOppList;
    }
    
    public Map<Id, OppContainer> OppId_OppWrap {get; set;}
    public List<OppContainer> OppWrapList {get; set;}
    
    public VFC10_OptyGridController (ApexPages.StandardSetController OppStdListController){
        this.OppList = (List<Opportunity>) OppStdListController.getSelected();
        this.OppWrapList = new List<OppContainer>();
        this.OppId_OppWrap = new Map<Id, OppContainer>();
        this.DisplayResult = false;
        this.OppList = RetrieveOppList(this.OppList);
        
        Set<Id> recAccess = new Set<Id>();
        
        List<UserRecordAccess> usrRecAccess = [SELECT RecordId fROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND HasEditAccess = true AND RecordId IN :OppIdList];
        
        for(UserRecordAccess ur: usrRecAccess)
        {
            recAccess.add(ur.RecordId);
        }
        system.debug(recAccess+'$$$$ Record Access $$$$');        
        
        for (Opportunity o : this.OppList)
        {
            OppContainer OppWrap;
            if(recAccess.contains(o.Id))
                OppWrap = new OppContainer(o, true);
            else
                OppWrap = new OppContainer(o, false);
            this.OppWrapList.add(OppWrap);
            this.OppId_OppWrap.put(o.id, OppWrap);
        }                
        stageProbability='';
        stageForecastCategory='';       
         
        for(OpportunityStage oppstage:[SELECT DefaultProbability,MasterLabel,ForecastCategoryName FROM OpportunityStage WHERE IsActive = true])
        {
            stageProbability+=oppstage.MasterLabel+':'+oppstage.DefaultProbability+',';            
            stageForecastCategory+=oppstage.MasterLabel+':'+oppstage.ForecastCategoryName+',';                            
        }
        stageProbability.subString(0,stageProbability.length()-1);
        stageForecastCategory.subString(0,stageForecastCategory.length()-1);        
    }
        public void updateProbability(){}
    public void CustomSave(){
        List<Opportunity> OppListTmp = new List<Opportunity>();
        for (OppContainer OppWrap : this.OppWrapList ){
            OppWrap.UpdateStatus = '';
            OppListTmp.add(OppWrap.opp);
        }
        List<Database.SaveResult> SRList = database.update(OppListTmp , false);
        this.OppList = RetrieveOppList(this.OppList);
        integer i = 0; // element # in the result list = Opp # in the OppWrapList
        while(i<SRList.size()){
            OppContainer OppWrap = this.OppId_OppWrap.get(this.OppList[i].Id);
            if (SRList[i].getErrors().size()>0){
                for(Database.Error err: SRList[i].getErrors())
                { 
                    OppWrap.ErrorMessage = err.getMessage();
                    system.debug('########## error: ' + err);
                    OppWrap.UpdateStatus = 'Not Ok';
                    OppWrap.Opp = this.OppList[i];
                } 
            }
            else{
                OppWrap.UpdateStatus = 'Ok';
                OppWrap.ErrorMessage = 'Record successfully updated';
                OppWrap.Opp = this.OppList[i];
            }         
            i++;
        }
    }
}