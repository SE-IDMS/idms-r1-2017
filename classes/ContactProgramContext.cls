global class ContactProgramContext {
    WebService String[] PartnerProgramIDs;
    WebService String[] ContactIDs;

    //WebService Integer StartRow = 1;
    WebService Integer BatchSize = 10000;
    
    WebService DateTime LastModifiedDate1;
    WebService DateTime LastModifiedDate2;
    WebService DateTime CreatedDate;
    
    /**
    * A blank constructor
    */
    public ContactProgramContext() {
    }

    /**
    * A constructor based on an Program @param a Program
    */
    public ContactProgramContext(string[] contactIDs, string[] programIDs) {
        this.PartnerProgramIDs = programIDs;
        this.ContactIDs = contactIDs;
    }
}