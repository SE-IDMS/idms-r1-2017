public with sharing class ConnectAdoptionController {


public PageReference FilterChange() {

   if (RFilter == 'Login History')
        RenBlock = 'H';
  // else If(RFilter == 'User Modified')
      // RenBlock = 'M';
   else If(RFilter == 'User Not Logged In')
       RenBlock = 'N';
         return null;
    }

public ConnectAdoptionController() {

    RenBlock = 'H';
     
    }

// Get Permission Set Information 

List<PermissionSetAssignment> lstUserAssignment1 = [SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE PermissionSet.Name = :'Connect_Program_Leaders'];
List<PermissionSetAssignment> lstUserAssignment2 = [SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE PermissionSet.Name = :'Connect_Champions'];
List<PermissionSetAssignment> lstUserAssignment3 = [SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE PermissionSet.Name = :'ConnectTargetKPIUsers'];
List<PermissionSetAssignment> lstUserAssignment4 = [SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE PermissionSet.Name = :'ConnectEntityKPIContributors'];
List<PermissionSetAssignment> lstUserAssignment5 = [SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE PermissionSet.Name = :'Connect_Country_Deployment_Leaders'];

// Declaration 

//List<Id> lstId1 = new List<Id>();
//List<Id> lstId2 = new List<Id>();
//List<Id> lstId3 = new List<Id>();
//List<Id> lstId4 = new List<Id>();
List<User> lstUser = new List<User>();


Public String RenBlock{get;set;}
Public List<UserAdoptionData> queryResults = new List<UserAdoptionData>();
Public List<User> lstUserModified = new list<User>();
Public string RFilter{get;set;}
Public Set<Id> AllUsers = new Set<Id>();
Public Set<Id> ActiveUsers = new Set<Id>();
Public Map<Id,string> Pset1 = new Map<Id,string>();
Public Map<Id,string> Pset2 = new Map<Id,string>();
Public Map<Id,string> Pset3 = new Map<Id,string>();
Public Map<Id,string> Pset4 = new Map<Id,string>();
Public Map<Id,string> Pset5 = new Map<Id,string>();
public List<ModifiedUserData> ModifiedUsers = new List<ModifiedUserData>();


Public void PopulatelstId(){

for(PermissionSetAssignment per:lstUserAssignment1)
        {
            //lstId1.add(per.AssigneeId);
            AllUsers.add(per.AssigneeId);
            Pset1.put(per.AssigneeId,'Connect Program Leader');
            
        }  
        
for(PermissionSetAssignment per:lstUserAssignment2)
        {
           // lstId2.add(per.AssigneeId);
            AllUsers.add(per.AssigneeId);
            Pset2.put(per.AssigneeId,'Connect Champion');
            
        } 
for(PermissionSetAssignment per:lstUserAssignment3)
        {
           // lstId3.add(per.AssigneeId);
            AllUsers.add(per.AssigneeId);
            Pset3.put(per.AssigneeId,'KPI Owner / Data Reporter');
            
        } 
for(PermissionSetAssignment per:lstUserAssignment4)
        {
            // lstId4.add(per.AssigneeId);
            AllUsers.add(per.AssigneeId);
            Pset4.put(per.AssigneeId,'Entity or Country KPI Viewer / Contributor');
            
        }       
for(PermissionSetAssignment per:lstUserAssignment5)
        {
            // lstId4.add(per.AssigneeId);
            AllUsers.add(per.AssigneeId);
            Pset5.put(per.AssigneeId,'Country Deployment Leader');
            
        }   
   }

Public List<User> getUserLoginInfo(){  
  
  PopulatelstId();
  return lstUser;
  }

 
public void AddqueryResults(){
try{
    PopulatelstId(); 
    
    queryResults = new List<UserAdoptionData>();
         
  // Get User History Information  for Program Leaders 
  
   List<User> lstUser1 = [SELECT Id,Name,Profile.Name,LastLoginDate FROM User WHERE Id in :AllUsers and IsActive = True];
   
   for (User Actuser : lstUser1)
    {
      ActiveUsers.add(Actuser.id);
    }
   
  
  AggregateResult[] LogHist1 = [SELECT UserID Usr, Calendar_Month(LoginTime) LoginMonth, Count(id) UserCount FROM LoginHistory Where UserID  in :ActiveUsers and Day_Only(LoginTime) >= : datetime.now().date().addDays(-30) group by UserID,Calendar_Month(LoginTime)];
    
   
   // Populate the Login History for Program Leaders
 
 Map<string,Integer> NumLogins = new Map<String, Integer>();
 Map<string,string> LoginMonth = new Map<String,String>();
 
 Map<string,string> Prgname = new Map<String,String>();
 List<Project_NCP__c> Prg = [Select Name,User__r.id,Program_Leader_2__r.id,Program_Leader_3__r.id from Project_NCP__c where Year__c = :Label.ConnectYear];
     for(Project_NCP__c P:Prg)
     {
       Prgname.Put(P.User__r.id, P.Name);
       Prgname.Put(P.Program_Leader_2__r.id, P.Name);
       Prgname.Put(P.Program_Leader_3__r.id, P.Name);
     }
     
   List<Team_Members__c> DL = [Select Program_Name__c,Team_Member__c from Team_Members__c where Year__c = :Label.ConnectYear];
   
   String Pname;
   for(Team_Members__c D:DL)
   {
    
    Pname = '';
   
     if(Prgname.get(D.Team_Member__c) == null)
        Prgname.put(D.Team_Member__c, D.Program_Name__c);
     
     else{ 
        Pname = Prgname.get(D.Team_Member__c)+ ',' + D.Program_Name__c;
        Prgname.put(D.Team_Member__c, Pname);
        }
        
    }
   
   
 
  for (AggregateResult his: LogHist1)
    {
        NumLogins.Put(String.ValueOf(his.get('Usr')),Integer.ValueOf(his.get('UserCount')));
        LoginMonth.Put(String.ValueOf(his.get('Usr')),monthName(Integer.ValueOf(his.get('LoginMonth'))));
     }
     
   for(User usr: lstUser1){
    UserAdoptionData usrObject = new UserAdoptionData();
   if(NumLogins.get(usr.ID) != null){ 
    usrObject.userid = usr.ID;
    UsrObject.NumOfLogins = NumLogins.get(usr.ID);
    UsrObject.username = usr.name;
    UsrObject.LastLogin = usr.LastLoginDate;
    UsrObject.Prof = usr.Profile.Name;
    UsrObject.LoginMonth = LoginMonth.get(usr.ID);
    UsrObject.ProgName = Prgname.get(usr.id);    
    UsrObject.PermSet = Pset1.get(usr.id); 
    UsrObject.Permset2 = Pset2.get(usr.id); 
    UsrObject.Permset3 = Pset3.get(usr.id); 
    UsrObject.Permset4 = Pset4.get(usr.id); 
    UsrObject.Permset5 = Pset5.get(usr.id); 
    system.debug('Permission Set ' + String.ValueOf(Pset1.get(usr.id)));
    //system.debug('Month ' + Integer.ValueOf(his.get('LoginMonth')));
   // system.debug('Num of Logins ' + Integer.ValueOf(his.get('UserCount')));
    queryResults.Add(usrObject);
   }
    
  Else {
      usrObject.userid = usr.ID;
      UsrObject.NumOfLogins = 0;
      UsrObject.username = usr.name;
      UsrObject.LastLogin = usr.LastLoginDate;
      UsrObject.Prof = usr.Profile.Name;
      UsrObject.ProgName = Prgname.get(usr.id);
      UsrObject.PermSet = Pset1.get(usr.id); 
      UsrObject.Permset2 = Pset2.get(usr.id); 
      UsrObject.Permset3 = Pset3.get(usr.id); 
      UsrObject.Permset4 = Pset4.get(usr.id); 
      UsrObject.Permset5 = Pset5.get(usr.id); 
      queryResults.Add(usrObject);
    } // Else Ends
   }  // For Ends
 
  
 }//Try Ends
            catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends



}

    
Public List<UserAdoptionData> getqueryResults(){

  if(RenBlock == 'H'){
      AddqueryResults();
      return (queryResults);
      }
  else return(null);
  }  


  
Public List<ModifiedUserData> getModifiedUsers(){


// Get List of Users who modified records in Connect    



Map<Id,Datetime> MapModified = new Map<Id,Datetime>();
Map<Id,string> MapObjectModified = new Map<Id,string>();
Set<Id> setModified = new Set<Id>();

/*
for(Cascading_Milestone__c cm:[SELECT LastModifiedById, LastModifiedDate FROM Cascading_Milestone__c])
{
        if((!setModified.contains(cm.LastModifiedById))){
            setModified.add(cm.LastModifiedById);
            MapModified.put(cm.LastModifiedById, cm.LastModifiedDate);
            system.debug(' Last Modified By ' + cm.LastModifiedById);
            MapObjectModified.put(cm.LastModifiedById, 'Cascading Milestone');
            }
}

for(Connect_Entity_Progress__c cep:[SELECT LastModifiedById,LastModifiedDate FROM Connect_Entity_Progress__c])
{
        if((!setModified.contains(cep.LastModifiedById))){
            setModified.add(cep.LastModifiedById);
            MapModified.put(cep.LastModifiedById, cep.LastModifiedDate);
            MapObjectModified.put(cep.LastModifiedById, 'Connect Entity Progress');
            }
}

for(Connect_Milestones__c cmile:[SELECT LastModifiedById,LastModifiedDate FROM Connect_Milestones__c])
{
        if(!setModified.contains(cmile.LastModifiedById)){
            setModified.add(cmile.LastModifiedById);  
            MapModified.put(cmile.LastModifiedById,cmile.LastModifiedDate);
            MapObjectModified.put(cmile.LastModifiedById, 'Global Milestone');
            }
}

for(Project_NCP__c program : [SELECT LastModifiedById,LastModifiedDate FROM Project_NCP__c])
{
        if(!setModified.contains(program.LastModifiedById)){  
            setModified.add(program.LastModifiedById); 
            MapModified.put(program.LastModifiedById,program.LastModifiedDate);
            MapObjectModified.put(program.LastModifiedById,'Global Program');
            }
}

for(Initiatives__c initiative:[SELECT LastModifiedById,LastModifiedDate FROM Initiatives__c])
{
        if(!setModified.contains(initiative.LastModifiedById)){
            setModified.add(initiative.LastModifiedById);
            MapModified.put(initiative.LastModifiedById,initiative.LastModifiedDate);
            MapObjectModified.put(initiative.LastModifiedById, 'Initiatives');
            }
}

System.Debug('setModified size: ' + setModified.size());

*/

List<User> lstUserModified = [SELECT Name, Profile.Name, LastLoginDate FROM User WHERE Id in :setModified];
List<ModifiedUserData> ModifiedUsrList = new List<ModifiedUserData>();

// Add user and modified date data to the Custom Class variable

/*
for(User usr: lstUserModified){
  system.debug('User ID ' + usr.id);
  system.debug( ' Map value  ' + MapObjectModified.get('005A0000001pvmDIAQ'));
  ModifiedUserData ModUsr = new ModifiedUserData();
  ModUsr.userid = usr.Id;
  ModUsr.username = usr.Name;
  ModUsr.PermSet = MapPermissionSets.get(usr.id);
  ModUsr.Prof = usr.Profile.Name;
  ModUsr.ObjectModifed = MapObjectModified.get(usr.id);
  ModUsr.ModifiedDate = String.ValueOf(MapModified.get(usr.id));
  ModUsr.LastLoginDate =String.ValueOf(usr.LastLoginDate);
  ModifiedUsrList.add(ModUsr);
  system.debug(' Last Modified -- 2 ' + ModifiedUsrList[0].ModifiedDate +  '  ' + ModifiedUsrList[0].ObjectModifed);
  }
*/
return ModifiedUsrList;
}


// Get List of Users Not Logged in

Public List<User> getUserNotLoggedIn(){

List<User> NotLoggedUser = [SELECT Name, Profile.Name, LastLoginDate FROM User WHERE LastLoginDate = :NULL and (id in :AllUsers)];

return NotLoggedUser;
}

// ModifiedUserData Custom Class Variable

Public Class ModifiedUserData
  {
    public string userid{get;set;}
    public string username{get;set;}
    public string PermSet{get;set;}
    public string Prof{get;set;}
    public string ObjectModifed{get;set;}
    public string ModifiedDate{get;set;}
    public string LastLoginDate{get;set;}
   
   }
   

// UserAdotionData Custom Class Variable

Public Class UserAdoptionData
   {
   
       public id userid{get;set;}
       public string username{get;set;}
       public string LoginMonth{get;set;}
       public integer NumOfLogins{get;set;}
       public datetime LastLogin{get;set;}
       public string PermSet{get;set;}
       public string PermSet2{get;set;}
       public string PermSet3{get;set;}
       public string PermSet4{get;set;}
       public string PermSet5{get;set;}
       public string Prof{get;set;}
       Public string Modified{get;set;}
       public string ProgName{get;set;}
       
     }
   
public List<SelectOption> getReportFilter() {
      List<SelectOption> options = new List<SelectOption>();
       options.add(new SelectOption('Login History','Login History Last 30 Days'));  
      // options.add(new SelectOption('User Modified','User Modified Connect Record'));     
       options.add(new SelectOption('User Not Logged In','User Not Logged In'));  
      
       return options;
     }
     
  // Translate month numbers to month name strings

    public List<String> MONTHS_OF_YEAR = new String[]
        {'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'};

    public String monthName(Integer monthNum) {
     if(monthNum > 0)
        return(MONTHS_OF_YEAR[monthNum - 1]);
     else return('');
    }
}