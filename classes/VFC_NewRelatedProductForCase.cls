public class VFC_NewRelatedProductForCase {
    public Id caseId;

    public VFC_NewRelatedProductForCase(ApexPages.StandardController controller) {
        CSE_RelatedProduct__c  objCaseRelatedProduct = (CSE_RelatedProduct__c)controller.getRecord(); 
        if(System.currentpagereference().getParameters().containsKey(Label.CLOCT14CCC13)){
            caseId = System.currentpagereference().getParameters().get(Label.CLOCT14CCC13);
        }   
    }
    public PageReference redirect(){
        PageReference ProductSearchPage;
        if(caseId!=null){
            ProductSearchPage  = Page.VFP_AddUpdateProductForCase;
            ProductSearchPage.getParameters().put('ID', caseId);
        }
        else{
            ProductSearchPage = null;
        }
        return ProductSearchPage;
    }
}