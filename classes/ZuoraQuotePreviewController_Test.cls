@isTest(seeAllData = true)
public class ZuoraQuotePreviewController_Test{
    
    private static testMethod void ZuoraQuotePreviewControllerTestMethod() {
        Country__c country = null;
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           country = countries[0];
        }
        else {
           country = new Country__c(Name='France', CountryCode__c='FR');
           insert country;
        }
        
        Account acc = new Account(Name='Test Acc', SEAccountID__c='123', Tech_CountryCode__c='FR', City__c='Paris', Country__c=country.Id, Street__c = 'street', ZipCode__c='123', Z_ProfileDiscount__c='1234');
        
        List<Account> accList = new List<Account>();
        accList.add(acc);
        
        insert accList;
        
        zqu__Quote__c quote1 = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = 'FIO');
        insert quote1;
        zqu__Quote__c quote2 = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = 'TEST');
        insert quote2;

        ZuoraQuotePreviewController zCon = new ZuoraQuotePreviewController();
        ApexPages.currentPage().getParameters().put('QuoteId', quote1.id);
        ZuoraSendTozBillingCheck__c zCSVal2 = new ZuoraSendTozBillingCheck__c(Name='TEST2', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='zqu__Account__c', Condition__c='Empty', Action__c='Warning');
        ZuoraSendTozBillingCheck__c zCSVal4 = new ZuoraSendTozBillingCheck__c(Name='TEST4', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='Installer__c', Condition__c='Filled', Action__c='Warning');
        zCon.ZuoraQuotePreviewCheck();
        ZuoraSendTozBillingCheck__c zCSVal1 = new ZuoraSendTozBillingCheck__c(Name='TEST1', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='Installer__r.Name', Condition__c='Empty', Action__c='Block');
        ZuoraSendTozBillingCheck__c zCSVal3 = new ZuoraSendTozBillingCheck__c(Name='TEST3', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='Add_onSKU__c', Condition__c='Filled', Action__c='Block');
        insert zCSVal1;
        insert zCSVal2;
        insert zCSVal3;
        insert zCSVal4;

        Test.startTest();
        zCon.ZuoraQuotePreviewCheck();
        ApexPages.currentPage().getParameters().put('QuoteId', quote2.id);
        ApexPages.currentPage().getParameters().put('//c.', '//c.');
        ApexPages.currentPage().getParameters().put('--c.', '--c.');
        ApexPages.currentPage().getParameters().put('Action', 'SubmitApproval');
        zCon.ZuoraQuotePreviewCheck();
        zCon.continueToSend();
        zCon.returnToQuote();
        Test.stopTest();
    }
       public static testMethod void ZuoraQuotePreviewControllerTestMethod2() {
        Country__c country = null;
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           country = countries[0];
        }
        else {
           country = new Country__c(Name='France', CountryCode__c='FR');
           insert country;
        }
        
        Account acc = new Account(Name='Test Acc', SEAccountID__c='123', Tech_CountryCode__c='FR', City__c='Paris', Country__c=country.Id, Street__c = 'street', ZipCode__c='123', Z_ProfileDiscount__c='1234');
        
        List<Account> accList = new List<Account>();
        accList.add(acc);
        
        insert accList;
        
        zqu__Quote__c quote1 = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = 'FIO');
        insert quote1;
        zqu__Quote__c quote2 = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = 'TEST');
        insert quote2;

        ZuoraQuotePreviewController zCon = new ZuoraQuotePreviewController();
        ApexPages.currentPage().getParameters().put('QuoteId', quote1.id);
        ZuoraSendTozBillingCheck__c zCSVal2 = new ZuoraSendTozBillingCheck__c(Name='TEST2', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='zqu__Account__c', Condition__c='Empty', Action__c='Warning');
        ZuoraSendTozBillingCheck__c zCSVal4 = new ZuoraSendTozBillingCheck__c(Name='TEST4', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='Installer__c', Condition__c='Filled', Action__c='Warning');

        //ZuoraSendTozBillingCheck__c zCSVal1 = new ZuoraSendTozBillingCheck__c(Name='TEST1', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='Installer__r.Name', Condition__c='Empty', Action__c='Block');
        //ZuoraSendTozBillingCheck__c zCSVal3 = new ZuoraSendTozBillingCheck__c(Name='TEST3', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='Add_onSKU__c', Condition__c='Filled', Action__c='Block');
        //insert zCSVal1;
        insert zCSVal2;
        //insert zCSVal3;
        insert zCSVal4;

        Test.startTest();
        zCon.ZuoraQuotePreviewCheck();
        ApexPages.currentPage().getParameters().put('QuoteId', quote2.id);
        ApexPages.currentPage().getParameters().put('//c.', '//c.');
        ApexPages.currentPage().getParameters().put('--c.', '--c.');
        ApexPages.currentPage().getParameters().put('Action', 'SubmitApproval');
        zCon.ZuoraQuotePreviewCheck();
        zCon.continueToSend();
        zCon.returnToQuote();
        Test.stopTest();
    }
    public static testMethod void ZuoraQuotePreviewControllerTestMethod3() {
        Country__c country = null;
        List<Country__c> countries =[select Id, Name, CountryCode__c from Country__c where CountryCode__c='FR'];
        if(countries.size()==1) {
           country = countries[0];
        }
        else {
           country = new Country__c(Name='France', CountryCode__c='FR');
           insert country;
        }
        
        Account acc = new Account(Name='Test Acc', SEAccountID__c='123', Tech_CountryCode__c='FR', City__c='Paris', Country__c=country.Id, Street__c = 'street', ZipCode__c='123', Z_ProfileDiscount__c='1234');
        
        List<Account> accList = new List<Account>();
        accList.add(acc);
        
        insert accList;
        
        zqu__Quote__c quote1 = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = 'FIO');
        insert quote1;
        zqu__Quote__c quote2 = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = 'TEST');
        insert quote2;

        ZuoraQuotePreviewController zCon = new ZuoraQuotePreviewController();
        ApexPages.currentPage().getParameters().put('QuoteId', quote1.id);
        ZuoraSendTozBillingCheck__c zCSVal2 = new ZuoraSendTozBillingCheck__c(Name='TEST2', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='zqu__Account__c', Condition__c='Filled', Action__c='Warning');
        ZuoraSendTozBillingCheck__c zCSVal4 = new ZuoraSendTozBillingCheck__c(Name='TEST4', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='Installer__c', Condition__c='Filled', Action__c='Warning');

        //ZuoraSendTozBillingCheck__c zCSVal1 = new ZuoraSendTozBillingCheck__c(Name='TEST1', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='Installer__r.Name', Condition__c='Empty', Action__c='Block');
        //ZuoraSendTozBillingCheck__c zCSVal3 = new ZuoraSendTozBillingCheck__c(Name='TEST3', Entity__c='FIO',  sObject__c='zqu__Quote__c', API_Name__c='Add_onSKU__c', Condition__c='Filled', Action__c='Block');
        //insert zCSVal1;
        insert zCSVal2;
        //insert zCSVal3;
        insert zCSVal4;

        Test.startTest();
        zCon.ZuoraQuotePreviewCheck();
        ApexPages.currentPage().getParameters().put('QuoteId', quote2.id);
        ApexPages.currentPage().getParameters().put('//c.', '//c.');
        ApexPages.currentPage().getParameters().put('--c.', '--c.');
        ApexPages.currentPage().getParameters().put('Action', 'SubmitApproval');
        zCon.ZuoraQuotePreviewCheck();
        zCon.continueToSend();
        zCon.returnToQuote();
        Test.stopTest();
    }
}