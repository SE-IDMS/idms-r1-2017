@isTest
public class VFC_ProgramClassificationLevel_TEST
{
    static testMethod void showProgramTargetMarket()
    {
        //PartnerPRogram__c program = new PartnerProgram__c();
        
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='ASM';
        insert country;

        //creates a global partner program
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;
        
        ClassificationLevelCatalog__c classCatalogue = new ClassificationLevelCatalog__c();
        classCatalogue.ClassificationLevelName__c = 'Sample Catalogue';
        classCatalogue.Active__c = true;
        insert classCatalogue;
        
        PartnerProgramClassification__c partnerPgmClassification = new PartnerProgramClassification__c();
        partnerPgmClassification.ClassificationLevelCatalog__c = classCatalogue.Id;
        partnerPgmClassification.PartnerProgram__c = gp1.Id;
        insert partnerPgmClassification;
        
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(gp1);
        VFC_ProgramClassificationLevel programClassLevel = new VFC_ProgramClassificationLevel(sdtCon1);
        programClassLevel.getlstClassificationLvl();
        PageReference pageRefWithretURL = Page.VFP_ProgramClassificationLevel;
        pageRefWithretURL.getParameters().put(Label.CL00330, '/home/home.jsp');
        Test.setCurrentPage(pageRefWithretURL);
    }
}