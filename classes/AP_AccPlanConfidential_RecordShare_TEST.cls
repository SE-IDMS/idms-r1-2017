@isTest
private class AP_AccPlanConfidential_RecordShare_TEST 
{
    private static Account acc;
    private static Opportunity opp;
    
    static
    {
        // create test data
        acc = Utils_TestMethods.createAccount();
        insert acc;
        Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'France';
        insert ct;
        opp = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp;
    }
    static testMethod void testAccPlanConfInsert()  
    {
        //User objUsr=[select id from user where isActive=true and profile.Name='System Administrator' limit 2];
        User[] objUsr=[select id from user where isActive=true limit 2];
        SFE_AccPlan__c objAccPlan=new SFE_AccPlan__c(name='ip account plan',Account__c=acc.id,AccountTurnover__c=100,AccountPlanOwner__c=objUsr[0].id);
        insert objAccPlan;
        AccountPlanConfidential__c objAPC=new AccountPlanConfidential__c(AccountPlan__c=objAccPlan.id,Strengths__c='strength');
        insert objAPC;      
        objAccPlan.AccountPlanOwner__c=objUsr[1].id;
        update objAccPlan;      
        objAPC.Strengths__c='strength123';
        update objAPC;      
        
    }
    
    
}