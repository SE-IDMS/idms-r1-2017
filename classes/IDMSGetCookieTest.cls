@isTest
public class IDMSGetCookieTest{
    static testmethod void cookieOne(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/oauth2/authorize/IDMSPartners';  
        req.httpMethod = 'Get';
        RestContext.request= req;
        RestContext.response= res;
        Test.startTest();
        IDMSGetCookie.doGet();
        Test.stopTest();
          
    }
}