public with sharing class VFC_ProgramDomainsOfExpertise {
    
    public list<ProgramDomainsOfExpertise__c> lstDomainsOfExpertiseLvl1 {get;set;}
   
    public VFC_ProgramDomainsOfExpertise(ApexPages.StandardController controller) {
       //lstDomainsOfExpertiseLvl1 = [Select DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c from ProgramDomainsOfExpertise__c where PartnerProgram__c=:controller.getId() AND  DomainsOfExpertiseCatalog__r.ParentDomainsOfExpertise__c =null order by DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c limit 1000]; 
       lstDomainsOfExpertiseLvl1 = [Select DomainsOfExpertiseCatalog__r.DomainsOfExpertiseName__c, DomainsOfExpertiseCatalog__r.name from ProgramDomainsOfExpertise__c where PartnerProgram__c=:controller.getId() order by DomainsOfExpertiseCatalog__r.name limit 1000]; 
   
   }

}