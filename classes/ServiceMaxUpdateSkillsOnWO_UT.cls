@isTest//(SeeAllData=true)
public class ServiceMaxUpdateSkillsOnWO_UT
{
    static testMethod void UnitTest()
    {    
        
        try{
            Account acct = Utils_TestMethods.createAccount();
            insert acct;
            
            SVMXC__Service_Order__c wor =  Utils_TestMethods.createWorkOrder(acct.id);
            wor.SVMXC__Order_Type__c='Maintenance';
            wor.CustomerRequestedDate__c = Date.today();
            wor.BackOfficeReference__c = '111111';
            
            insert wor;
            
            
            SVMXC__Skill__c  sk= new SVMXC__Skill__c();
            sk.SVMXC__Description__c  = 'Description ';
            sk.SVMXC__Skill_Category__c  = 'Installation';
            sk.Business_Unit__c = 'PW';
            sk.recordtypeId = Schema.SObjectType.SVMXC__Skill__c.getRecordTypeInfosByName().get('Expertise').getRecordTypeId();
            
            insert sk;
            
            Country__c cn = Utils_TestMethods.createCountry();
            insert cn;
            Set<Id> soId = new Set<Id>();
            soId.add(wor.Id);
            
            SVMXC__Skill__c sk1 = sk.clone();  
            sk1.recordtypeId = Schema.SObjectType.SVMXC__Skill__c.getRecordTypeInfosByName().get('Certification').getRecordTypeId();
            sk1.Country__c = cn.Id;
            sk1.Business_Unit__c = 'abc';
            
            insert sk1;
            
            Work_Order_Skills__c wos = new Work_Order_Skills__c();
            wos.Work_Order__c = wor.Id;
            wos.Skill__c = sk.Id;
            insert wos;
            wos.Skill__c = sk1.Id;
            
            update wos;
            
            Account acc = Utils_TestMethods.createAccount();
            insert acc;
            Country__c c = Utils_TestMethods.createCountry(); 
            c.CountryCode__c = '1-';
            insert c;
            
            
            SVMXC__Site__c loc = new SVMXC__Site__c(Name = 'TestBatchName',
                                                    SVMXC__City__c = 'Junction City',
                                                    SVMXC__State__c = 'Kansas',
                                                    SVMXC__Street__c = '704 S. Adams',
                                                    SVMXC__Zip__c = '66441',
                                                    LocationCountry__c = c.Id,
                                                    SVMXC__Location_Type__c='Building',
                                                    SVMXC__Account__c=acc.Id,
                                                    PrimaryLocation__c = true,
                                                    TimeZone__c='India');
            test.starttest(); 
            insert loc;
            
            SVMXC__Site__c loc1 = new SVMXC__Site__c(Name = 'TestBatchName',
                                                     SVMXC__City__c = 'Junction City',
                                                     SVMXC__State__c = 'Kansas',
                                                     SVMXC__Street__c = '704 S. Adams',
                                                     SVMXC__Zip__c = '66441',
                                                     LocationCountry__c = c.Id,
                                                     SVMXC__Location_Type__c='Building',
                                                     SVMXC__Account__c=acc.Id,
                                                     SVMXC__Parent__c = loc.id,
                                                     TimeZone__c='India');
            
            insert loc1;
            
            SVMXC__Site__c loc2 = new SVMXC__Site__c(Name = 'TestBatchName',
                                                     SVMXC__City__c = 'Junction City',
                                                     SVMXC__State__c = 'Kansas',
                                                     SVMXC__Street__c = '704 S. Adams',
                                                     SVMXC__Zip__c = '66441',
                                                     LocationCountry__c = c.Id,
                                                     SVMXC__Location_Type__c='Building',
                                                     SVMXC__Account__c=acc.Id,
                                                     SVMXC__Parent__c = loc1.id,
                                                     TimeZone__c='India');
            
            insert loc2;
            
            SVMXC__Site__c loc3 = new SVMXC__Site__c(Name = 'TestBatchName',
                                                     SVMXC__City__c = 'Junction City',
                                                     SVMXC__State__c = 'Kansas',
                                                     SVMXC__Street__c = '704 S. Adams',
                                                     SVMXC__Zip__c = '66441',
                                                     LocationCountry__c = c.Id,
                                                     SVMXC__Location_Type__c='Building',
                                                     SVMXC__Account__c=acc.Id,
                                                     SVMXC__Parent__c = loc2.id,
                                                     TimeZone__c='India');
            
            insert loc3;
            
            SVMXC__Site__c loc4 = new SVMXC__Site__c(Name = 'TestBatchName',
                                                     SVMXC__City__c = 'Junction City',
                                                     SVMXC__State__c = 'Kansas',
                                                     SVMXC__Street__c = '704 S. Adams',
                                                     SVMXC__Zip__c = '66441',
                                                     LocationCountry__c = c.Id,
                                                     SVMXC__Location_Type__c='Building',
                                                     SVMXC__Account__c=acc.Id,
                                                     SVMXC__Parent__c = loc3.id,
                                                     TimeZone__c='India');
            
            insert loc4;
            SVMXC__Site__c loc5 = new SVMXC__Site__c(Name = 'TestBatchName',
                                                     SVMXC__City__c = 'Junction City',
                                                     SVMXC__State__c = 'Kansas',
                                                     SVMXC__Street__c = '704 S. Adams',
                                                     SVMXC__Zip__c = '66441',
                                                     LocationCountry__c = c.Id,
                                                     SVMXC__Location_Type__c='Building',
                                                     SVMXC__Account__c=acc.Id,
                                                     SVMXC__Parent__c = loc4.id,
                                                     TimeZone__c='India');
            
            insert loc5;
            
            SVMXC__Site__c loc6 = new SVMXC__Site__c(Name = 'TestBatchName',
                                                     SVMXC__City__c = 'Junction City',
                                                     SVMXC__State__c = 'Kansas',
                                                     SVMXC__Street__c = '704 S. Adams',
                                                     SVMXC__Zip__c = '66441',
                                                     LocationCountry__c = c.Id,
                                                     SVMXC__Location_Type__c='Building',
                                                     SVMXC__Account__c=acc.Id,
                                                     SVMXC__Parent__c = loc5.id,
                                                     TimeZone__c='India');
            
            insert loc6;
            test.stoptest();
            List<SVMXC__Service_Order__c> solst = new List<SVMXC__Service_Order__c>();
            Set<Id> locId = new Set<Id>();
            locId.add(loc6.Id);
            
            HabilitationRequirement__c hr = new HabilitationRequirement__c();
            hr.Skill__c = sk1.Id;
            hr.Location__c = loc6.Id;
            hr.recordtypeId = Schema.SObjectType.HabilitationRequirement__c.getRecordTypeInfosByName().get('Location').getRecordTypeId();
            hr.Business_Unit__c ='abc';
            hr.Country__c = cn.Id;
            insert hr;
            Set<String> BU = new Set<String>();
            BU.add('abc');
            SVMXC__Service_Order__c wo= new SVMXC__Service_Order__c(SVMXC__Site__c=loc5.id,SVMXC__Country__c=cn.id);
            insert wo;
            wo.SVMXC__Site__c=loc6.id;
            update wo;
            solst.add(wo);     
            
            ServiceMaxUpdateSkillsOnWO.HabilitationRequirement(locId,BU,solst);
        }
        catch(Exception ex){
            
            
        }
        
    }
}