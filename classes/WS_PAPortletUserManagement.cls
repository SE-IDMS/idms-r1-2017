/**
Author:Nitin Khunal
Purpose: This class is for webservices to create user, enable/disable user, add/remove manual sharing
*/
global class WS_PAPortletUserManagement{
    
    //Apex Object for error code and message
    global class SiteResult{
        webservice String ErrorCode;
        webservice String ErrorMsg;
        webservice String UserId;
    }
    
    //Apex Object for User information
    global class UserInformation {
        webservice String strContactGoldenId;
        webservice String strAccountGoldenId;
        webservice String strCustomerFirstId;
        webservice String strWebUserId;
        webservice String strWebUserName;
    }
    
    //Apex object for Manual Sharing
    global class ManualSharingInformation {
        webservice String strAccountGoldenId;
        webservice String strWebUserId;
        webservice String strWebUserName;
        webservice String strSharingAccessLevel;
    }
    
    //Webservice method to deactivate a user
    webService static SiteResult createUser(UserInformation userInf) {
        Set<Id> setContractIds = new Set<Id>();
        Savepoint sp = Database.setSavepoint();
        SiteResult siteres = new SiteResult();
        String webUserName = '';
        if(checkOrgType()) {
            webUserName = userInf.strWebUserName + System.Label.CLQ316PA027 + UserInfo.getUserName().substringAfterLast('.com');
        }
        else{
            webUserName = userInf.strWebUserName +System.Label.CLQ316PA027;
        }
        
        List<Contact> lstContact = new List<Contact>();
        List<CTR_ValueChainPlayers__c> lstCVCP = new List<CTR_ValueChainPlayers__c>();
        List<Account> lstAccount = [select id, SEAccountID__c, IsPartner from Account where SEAccountID__c =: userInf.strAccountGoldenId];
        
        if(lstAccount.isEmpty()) {
            siteres.ErrorCode = '1';
            siteres.ErrorMsg = System.Label.CLQ316PA014 + ' ' + userInf.strAccountGoldenId;
            return siteres;
        }
        else if(lstAccount.size() > 1) {
            siteres.ErrorCode = '3';
            siteres.ErrorMsg = System.Label.CLQ316PA020 + ' ' + userInf.strAccountGoldenId;
            return siteres;
        }
        
        if(!String.isBlank(System.Label.CLQ316PA045)) {
            String strContractIds = System.Label.CLQ316PA045;
            if(strContractIds.contains(',')) {
                List<String> parts = strContractIds.split(',');
                for(String conId : parts) {
                    setContractIds.add(conId);
                }
            }
            else{
                setContractIds.add(strContractIds);
            }
        }
        
        if(!Test.isRunningTest()) {
            lstCVCP = [select Contract__r.Name, Contract__r.WebAccess__c, Contact__c, Contact__r.Account.IsPartner, Contact__r.Account.SEAccountID__c, Contact__r.Account.TimeZone__c,Contact__r.Id, Contact__r.Email, Contact__r.FirstName, Contact__r.LastName,Contact__r.TimeZone__c from CTR_ValueChainPlayers__c where LegacyPIN__c =: userInf.strCustomerFirstId  and Contract__c IN : setContractIds];
        }else
        {
            lstCVCP = [select Contract__r.Name, Contract__r.WebAccess__c, Contact__c, Contact__r.Account.IsPartner, Contact__r.Account.SEAccountID__c, Contact__r.Account.TimeZone__c,Contact__r.Id, Contact__r.Email, Contact__r.FirstName, Contact__r.LastName,Contact__r.TimeZone__c from CTR_ValueChainPlayers__c where LegacyPIN__c =: userInf.strCustomerFirstId];
        }
        if(lstCVCP.isEmpty()) {
            siteres.ErrorCode = '1';
            siteres.ErrorMsg = System.Label.CLQ316PA005 + ' '+ userInf.strCustomerFirstId +' ' + System.Label.CLQ316PA001;
            return siteres;
        } 
        else if(lstCVCP.size() > 1) {
            siteres.ErrorCode = '2';
            siteres.ErrorMsg = System.Label.CLQ316PA019 + ' ' + userInf.strCustomerFirstId;
            return siteres;
        }
        if(lstCVCP[0].Contact__r.Account.SEAccountID__c != userInf.strAccountGoldenId) {
            siteres.ErrorCode = '5';
            siteres.ErrorMsg = System.Label.CLQ316PA035 + ' ' + userInf.strCustomerFirstId + ' ' + System.Label.CLQ316PA036 + ' ' + userInf.strAccountGoldenId;
            return siteres;
        }
        if(String.isBlank(lstCVCP[0].Contact__r.Email)) {
            siteres.ErrorCode = '10';
            siteres.ErrorMsg = System.Label.CLQ316PA034;
            return siteres;
        }
        //List<Account> lstAccount = [select id from Account where SEAccountID__c =: userInf.strAccountGoldenId];
        List<User> lstUser = [select id from User where UserName =: webUserName];
        
        if(lstUser.size() > 0) {
            siteres.ErrorCode = '4';
            siteres.ErrorMsg = System.Label.CLQ316PA007;
            return siteres;
        }
        if(!lstAccount[0].IsPartner) {
            Account acc = lstAccount[0];
            acc.IsPartner = true;
            try{
                update acc;
            }
            catch(DMLException ex){
                Database.rollback(sp);
                system.debug('ex==='+ex);
                siteres.ErrorCode = '9';
                siteres.ErrorMsg = ex.getDmlMessage(0);
                return siteres;
            }
        }
        
        
        //Select community profile that you want to assign
        Profile portalProfile = [SELECT Id FROM profile WHERE Id =: system.label.CLQ316PA037 LIMIT 1];
        //Create user 
        User theUser = new User();
        theUser.Username = webUserName;
        theUser.ContactId =lstCVCP[0].Contact__c;
        theUser.ProfileId = portalProfile.Id;
        if(userInf.strWebUserName.length() > 8){
            theUser.Alias = userInf.strWebUserName.substring(0, 8);
        }else{
            theUser.Alias = userInf.strWebUserName;
        }
        theUser.Email = lstCVCP[0].Contact__r.Email;
        theUser.EmailEncodingKey = 'UTF-8';
        if(!String.isBlank(lstCVCP[0].Contact__r.FirstName)) {
            theUser.FirstName = lstCVCP[0].Contact__r.FirstName;
        }
        theUser.LastName = lstCVCP[0].Contact__r.LastName;
        theUser.CommunityNickname = userInf.strWebUserName;
        //theUser.IsPortalEnabled = true;
        if(lstCVCP[0].Contact__r.TimeZone__c!=null && lstCVCP[0].Contact__r.TimeZone__c!='' )
            theUser.TimeZoneSidKey = lstCVCP[0].Contact__r.TimeZone__c;
        else if(lstCVCP[0].Contact__r.Account.TimeZone__c!=null && lstCVCP[0].Contact__r.Account.TimeZone__c!='' )
            theUser.TimeZoneSidKey = lstCVCP[0].Contact__r.Account.TimeZone__c;
        else
            theUser.TimeZoneSidKey =system.label.CLQ316PA039;
        theUser.LocaleSidKey = 'en_US';
        theUser.LanguageLocaleKey = 'en_US';
        if(!Test.isRunningTest()) {
            theUser.IsPrmSuperUser = true;
        }
        try{
            insert theUser;
            if(!Test.isRunningTest()) {
                addPermissionSetandGroup(theUser.Id);
                addManualSharing(theUser.Id, lstAccount[0].Id);
            }
        }
        catch(DMLException ex){
            Database.rollback(sp);
            system.debug('ex==='+ex);
            siteres.ErrorCode = '9';
            siteres.ErrorMsg = ex.getDmlMessage(0);
            return siteres;
        }
        siteres.ErrorCode = '0';
        siteres.ErrorMsg = System.Label.CLQ316PA008;
        siteres.UserId = theUser.Id;
        return siteres;
    }

    //Webservice method to deactivate a user
    webService static SiteResult deactivateUser(String uName) {
        SiteResult siteres = new SiteResult();
        String userName = '';
        if(checkOrgType()) {
            userName = uName + System.Label.CLQ316PA027 + UserInfo.getUserName().substringAfterLast('.com');
        }
        else{
            userName = uName + System.Label.CLQ316PA027;
        }
        List<User> lstUser = [select Username, isActive from User where Username =: userName];
        if(lstUser.size() == 0) {
            siteres.ErrorCode = '1';
            siteres.ErrorMsg = System.Label.CLQ316PA009 + ' ' + userName;
        }else
        if(lstUser.size() == 1) {
            lstUser[0].IsActive = false;
            //lstUser[0].IsPortalEnabled = false;
            try{
                update lstUser;
            }
            catch(DMLException ex){
                system.debug('ex==='+ex);
                siteres.ErrorCode = '9';
                siteres.ErrorMsg = ex.getDmlMessage(0);
                return siteres;
            }  
            siteres.ErrorCode = '0';
            siteres.ErrorMsg = System.Label.CLQ316PA010;
        }else
        if(lstUser.size() > 1) {
            siteres.ErrorCode = '2';
            siteres.ErrorMsg = System.Label.CLQ316PA011 + ' '+userName;
        }
        return siteres;
    }
    
    //Webservice method to activate a user
    webService static SiteResult activateUser(String uName) {
        SiteResult siteres = new SiteResult();
        String userName = '';
        if(checkOrgType()) {
            userName = uName + System.Label.CLQ316PA027 + UserInfo.getUserName().substringAfterLast('.com');
        }else
        {
            userName = uName + System.Label.CLQ316PA027;
        }
        List<User> lstUser = [select Username, isActive from User where Username =: userName];
        if(lstUser.size() == 0) {
            siteres.ErrorCode = '1';
            siteres.ErrorMsg = System.Label.CLQ316PA009 + ' ' + userName;
        }else
        if(lstUser.size() == 1) {
            lstUser[0].IsActive = true;
            //lstUser[0].IsPortalEnabled = true;
            try{
                update lstUser;
            }
            catch(DMLException ex){
                system.debug('ex==='+ex);
                siteres.ErrorCode = '9';
                siteres.ErrorMsg = ex.getDmlMessage(0);
                return siteres;
            }  
            siteres.ErrorCode = '0';
            siteres.ErrorMsg = System.Label.CLQ316PA022;
        }else
        if(lstUser.size() > 1) {
            siteres.ErrorCode = '2';
            siteres.ErrorMsg = System.Label.CLQ316PA011 + ' '+userName;
        }
        return siteres;
    }
    
    //Webservice method to give manual sharing to user
    webService static SiteResult accountManualSharing(ManualSharingInformation manualSharingInfo) {
        String webUserName = '';
        SiteResult siteres = new SiteResult();
        List<User> lstUser = new List<User>();
        /*if(!String.isBlank(manualSharingInfo.strWebUserId)) {
            lstUser = [select id, isActive, UserName from User where Id =: manualSharingInfo.strWebUserId];
            webUserName = lstUser[0].UserName;
        }else*/
        if(!String.isBlank(manualSharingInfo.strWebUserName)) {
            if(checkOrgType()) {
                webUserName = manualSharingInfo.strWebUserName + System.Label.CLQ316PA027 + UserInfo.getUserName().substringAfterLast('.com');
            }else
            {
                webUserName = manualSharingInfo.strWebUserName + System.Label.CLQ316PA027;
            }
            //webUserName = manualSharingInfo.strWebUserName;
            lstUser = [select id, isActive, UserName from User where userName =: webUserName];
            
        }
        List<Account> lstAccount = [select id from Account where SEAccountID__c =: manualSharingInfo.strAccountGoldenId];
        if(lstUser.isEmpty()) {
            siteres.ErrorCode = '1';
            siteres.ErrorMsg = System.Label.CLQ316PA009 + ' ' + webUserName;
            return siteres;
        }else
        if(lstUser.size() > 1) {
            siteres.ErrorCode = '2';
            siteres.ErrorMsg = System.Label.CLQ316PA011 + ' '+webUserName;
            return siteres;
        }else
        if(!lstUser[0].isActive) {
            siteres.ErrorCode = '3';
            siteres.ErrorMsg = System.Label.CLQ316PA012 + ' ' + webUserName + ' ' + System.Label.CLQ316PA013;
            return siteres;
        }else
        if(lstAccount.isEmpty()) {
            siteres.ErrorCode = '4';
            siteres.ErrorMsg = System.Label.CLQ316PA014 + ' ' + manualSharingInfo.strAccountGoldenId;
            return siteres;
        }
        if(string.isBlank(manualSharingInfo.strSharingAccessLevel)) {
            siteres.ErrorCode = '4';
            siteres.ErrorMsg = System.Label.CLQ316PA046;
            return siteres;
        }
        if(manualSharingInfo.strSharingAccessLevel != 'Read' && manualSharingInfo.strSharingAccessLevel != 'Edit' && manualSharingInfo.strSharingAccessLevel != 'Delete') {
            siteres.ErrorCode = '4';
            siteres.ErrorMsg = System.Label.CLQ316PA047;
            return siteres;
        }
        List<AccountShare> lstAccountShare = [select UserOrGroupId, AccountAccessLevel, CaseAccessLevel from AccountShare where AccountId =: lstAccount[0].Id AND UserOrGroupId =: lstUser[0].Id];
        if(manualSharingInfo.strSharingAccessLevel == 'Read' || manualSharingInfo.strSharingAccessLevel == 'Edit') {
            if(lstAccountShare.isEmpty()) {
                AccountShare accountShare = new AccountShare();
                accountShare.AccountId = lstAccount[0].Id;
                accountShare.UserOrGroupId = lstUser[0].Id;
                accountShare.AccountAccessLevel = 'Read';
                accountShare.CaseAccessLevel = manualSharingInfo.strSharingAccessLevel;
                accountShare.OpportunityAccessLevel = 'None';
                //accountShare.RowCause = 'Manual';
                try{
                    insert accountShare;
                }
                catch(DMLException ex){
                    siteres.ErrorCode = '9';
                    siteres.ErrorMsg = ex.getDmlMessage(0);
                    return siteres;
                }  
            }else
            {
                if(lstAccountShare[0].CaseAccessLevel != manualSharingInfo.strSharingAccessLevel) {
                    //lstAccountShare[0].AccountAccessLevel = 'Read';
                    lstAccountShare[0].CaseAccessLevel = manualSharingInfo.strSharingAccessLevel;
                    try{
                        update lstAccountShare;
                    }
                    catch(DMLException ex){
                        siteres.ErrorCode = '9';
                        siteres.ErrorMsg = ex.getDmlMessage(0);
                        return siteres;
                    }  
                }
            }
            siteres.ErrorCode = '0';
            siteres.ErrorMsg = System.Label.CLQ316PA015;
            return siteres;
        } else
        if(manualSharingInfo.strSharingAccessLevel == 'Delete') {
            if(lstAccountShare.isEmpty()) {
                siteres.ErrorCode = '4';
                siteres.ErrorMsg = System.Label.CLQ316PA048;
                return siteres;
            }
            try{
                delete lstAccountShare;
            }
            catch(DMLException ex){
                siteres.ErrorCode = '9';
                siteres.ErrorMsg = ex.getDmlMessage(0);
                return siteres;
            }  
            siteres.ErrorCode = '0';
            siteres.ErrorMsg = System.Label.CLQ316PA026;
            return siteres;
        }
        return null;
    }
    
    //Webservice method to remove manual sharing from account
    webService static SiteResult removeManualSharing(String userList) {
        SiteResult siteres = new SiteResult();
        Set<String> setUserName = new Set<String>();
        Set<Id> setUserIds = new Set<ID>();
        List<AccountShare> lstAccShare = new List<AccountShare>();
        for(String xUser : userList.split(',')) {
            if(checkOrgType()) {
                xUser = xUser+ System.Label.CLQ316PA027 + UserInfo.getUserName().substringAfterLast('.com');
            }else
            {
                xUser = xUser + System.Label.CLQ316PA027;
            }
            setUserName.add(xUser);
        }
        for(User u :[select id from User where UserName In: setUserName]){
            
            setUserIds.add(u.id);
        }
        if(setUserIds.isEmpty()) {
            siteres.ErrorCode = '1';
            siteres.ErrorMsg = System.Label.CLQ316PA023;
            return siteres;
        }
        for(AccountShare xAccShare : [select Id from AccountShare where UserOrGroupId IN: setUserIds AND RowCause = 'Manual']) {
            lstAccShare.add(xAccShare);
        }
        try{
            delete lstAccShare;
        }
        catch(DMLException ex){
            siteres.ErrorCode = '9';
            siteres.ErrorMsg = ex.getDmlMessage(0);
            return siteres;
        }  
        siteres.ErrorCode = '0';
        siteres.ErrorMsg = System.Label.CLQ316PA026;
        return siteres;
    }
    
    public static boolean checkOrgType() {
        Organization org = [SELECT IsSandbox FROM Organization limit 1];
        return org.IsSandbox;
    }
    
    @future
    public static void addPermissionSetandGroup(String userId) {
        List<PermissionSetAssignment> lstPermissionSetToInsert = new List<PermissionSetAssignment>();
        List<GroupMember> GMlist = new List<GroupMember>();
        for(String xPermissionSetId : system.Label.CLQ316PA032.split(',')) {
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = xPermissionSetId, AssigneeId = userId);
            lstPermissionSetToInsert.add(psa);
        }
        insert lstPermissionSetToInsert;
        
        for(String groupId : system.Label.CLQ316PA033.split(',')) {
            GroupMember gMember = new GroupMember();
            gMember.GroupId = groupId;
            gMember.UserOrGroupId = userId;
            GMList.add(gMember);       
        }
        if(!GMList.isEmpty()) {
            System.debug('Group Member List is ' + GMList);
            insert GMList;
        }
        
    }
    
    @future
    public static void addManualSharing(String userId, String accountId) {
        AccountShare accountShare = new AccountShare();
        accountShare.AccountId = accountId;
        accountShare.UserOrGroupId = userId;
        accountShare.AccountAccessLevel = 'Read';
        accountShare.CaseAccessLevel = 'Edit';
        accountShare.OpportunityAccessLevel = 'None';
        insert accountShare;
    }
}