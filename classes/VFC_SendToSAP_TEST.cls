@isTest
Public Class VFC_SendToSAP_TEST{
    
      Private Static TestMethod Void  Method1(){
       
        Country__c countryObj= Utils_TestMethods.createCountry();
        Database.insert(countryObj);      
       
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = countryObj.Id;
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        objContact.Country__c = countryObj.Id;
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        CustomerLocation__c customerObj=Utils_TestMethods.createCustomerLocation(objAccount.id,countryObj.Id);
        Database.insert(customerObj);
        
        WorkOrderNotification__c wonObj=Utils_TestMethods.createWorkOrderNotification(objOpenCase.id,customerObj.id);
        Database.insert(wonObj);
        
        ApexPages.StandardController Ctrl =New ApexPages.StandardController(wonObj);
        VFC_SendToSAP sendToSAPObj = new VFC_SendToSAP(Ctrl);
        sendToSAPObj.CallSAP();
    }
    
}