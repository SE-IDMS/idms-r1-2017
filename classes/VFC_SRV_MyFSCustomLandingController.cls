/* An apex page controller that takes the user to the right start page based on credentials or lack thereof
*/
public without sharing class VFC_SRV_MyFSCustomLandingController {
    
    public String userId {  get;  set; }
    public List < Account > accountList {  get;  set; }
    Public integer companysize{get;set;}
    public Map < String, Map < String,String >> accountCountrySetMap {  get;  set; }
    Public String UltimateParentAccount{get;set;}
    Public Static integer AccountSize{get;set;}
    Public User usrInfo{get;set;}
    Public Boolean isInternalUser{get;set;}
    
    // Code we will invoke on page load.
    public PageReference forwardToCustomAuthPage() {
        
        System.debug('### Starting forwardToCustomAuthPage method');
        
        return new PageReference('/'+SObjectType.Contact.getKeyPrefix());
        
        if (UserInfo.getUserType() == 'Guest') {
            return new PageReference('/'+SObjectType.Contact.getKeyPrefix());
        } 
        else {
            return null;
        }
    } 
    
    public VFC_SRV_MyFSCustomLandingController() {  
        //  System.debug('### Starting VFC_SRV_MyFSCustomLandingController constructor');
        //  System.debug('Header: '+ ApexPages.currentPage().getHeaders());
        //  System.debug('Parameters: '+ ApexPages.currentPage().getParameters());
        // queryAccounts();
        
    }
    /* public VFC_SRV_MyFSCustomLandingController(VFC_SRV_MYFSSiteSelecitonController Ctrl) {  
//  System.debug('### Starting VFC_SRV_MyFSCustomLandingController constructor');
//  System.debug('Header: '+ ApexPages.currentPage().getHeaders());
//  System.debug('Parameters: '+ ApexPages.currentPage().getParameters());
queryAccounts();

}*/
    
    public Pagereference queryAccounts() {
        
        UltimateParentAccount = Apexpages.currentpage().getparameters().get('Params');
        system.debug('###UltimateParentAccount##'+UltimateParentAccount);
        Pagereference pr ;
        userId = Apexpages.currentpage().getparameters().get('UId');
        if(userId==null || userId ==''){
            userId = UserInfo.getUserId();
            system.debug('userid'+userId );
        }
        
        else{
            Cache.SessionPartition sessionPart = Cache.Session.getPartition('local.myFS');
            if (sessionPart.contains('User')) {
                sessionPart.remove('User');
                
            }
            
            system.debug('userid'+userId );
            isInternalUser = AP_SRV_myFSUtils.isInternalUser(UserInfo.getUserId());
            System.debug('isInternalUser'+isInternalUser);
            if(isInternalUser == false)
                return null;
            usrInfo = [select id,firstname,lastname from user where id=:userId ];          
            system.debug('userrecord++++'+usrInfo);
            system.debug('userfirstname+++++++'+usrInfo.firstname);
            system.debug('userlasttname+++++++'+usrInfo.lastname);
        }
        
        system.debug('userid'+userId );
        List < User > lstUser = new List < User > ();
        Set < String > accountIds = new Set < String > ();
        List < Account > ultimateParentaccountList = new List < Account > ();
        Set < String > locationsSet = new Set < String > ();
        Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.Role__c;
        Map < String, Schema.RecordTypeInfo > serviceRoleRecordTypeInfo = dSobjres.getRecordTypeInfosByName();
        Id serviceRoleRecordTypeId = serviceRoleRecordTypeInfo.get('Location - Contact Role').getRecordTypeId();
        accountCountrySetMap = new Map < String, Map < String,String >> (); 
        accountList = new List < Account >();
        //005J0000005lDTp
        //005J0000005lNEC
        //005J0000005lHmR
        //005J0000005lNDn - one account and country
        if (userId != null) {
            
            lstUser = [Select u.Id, u.ContactId from User u where u.Id = : userId];
            //  lstUser = [Select u.Id, u.ContactId from User u where u.Id = '005J0000005lNDn'];
            
            debug('lstUser: ' + lstUser);
        }
        
        if (lstUser != null && lstUser.size() > 0) {
            
            String contactId = lstUser[0].ContactId;
            if (contactId != null) {
                List < Role__c > serviceRolesList = [select Id, contact__c, Location__c, Location__r.SVMXC__Account__c, Location__r.SVMXC__Account__r.UltiParentAcc__c, Location__r.SVMXC__Account__r.UltiParentAcc__r.Name, Location__r.SVMXC__Account__r.Name, Location__r.SVMXC__Account__r.UltiParentAcc__r.AccountNumber, Location__r.SVMXC__Account__r.UltiParentAcc__r.LastModifiedDate, Location__r.SVMXC__Account__r.country__c, Location__r.SVMXC__Account__r.country__r.Name,Location__r.SVMXC__Account__r.country__r.CountryCode__c from Role__c where contact__c = : contactId and RecordTypeId = : serviceRoleRecordTypeId AND ToDelete__c = False];
                if (serviceRolesList != null && !serviceRolesList.isempty()) {
                    for (Role__c currentRole: serviceRolesList) {
                        // The account to which the current location is linked is not a root of the account hierarchies
                        if (currentRole.Location__r.SVMXC__Account__r.UltiParentAcc__c != null) {
                            accountIds.add(currentRole.Location__r.SVMXC__Account__r.UltiParentAcc__c);
                            
                            if (!accountCountrySetMap.containsKey(currentRole.Location__r.SVMXC__Account__r.UltiParentAcc__c)) {
                                Set < String > childAccountCountries = new Set < String > ();
                                childAccountCountries.add(currentRole.Location__r.SVMXC__Account__r.country__r.Name);
                                Map<String,String> childAccountCountriesMap = new Map<String,String>();
                                childAccountCountriesMap.put(currentRole.Location__r.SVMXC__Account__r.country__r.CountryCode__c,currentRole.Location__r.SVMXC__Account__r.country__r.Name);
                                accountCountrySetMap.put(currentRole.Location__r.SVMXC__Account__r.UltiParentAcc__c, childAccountCountriesMap);
                            } 
                            else {
                                Map<String,String> tempchildAccountCountriesMap = accountCountrySetMap.get(currentRole.Location__r.SVMXC__Account__r.UltiParentAcc__c);
                                if (tempchildAccountCountriesMap != null) {
                                    tempchildAccountCountriesMap.put(currentRole.Location__r.SVMXC__Account__r.country__r.CountryCode__c,currentRole.Location__r.SVMXC__Account__r.country__r.Name);
                                    accountCountrySetMap.put(currentRole.Location__r.SVMXC__Account__r.UltiParentAcc__c, tempchildAccountCountriesMap);
                                }
                            }
                        }
                        // The account to which the current location is linked is a root of the account hierarchies 
                        else {
                            accountIds.add(currentRole.Location__r.SVMXC__Account__c);
                            if (!accountCountrySetMap.containsKey(currentRole.Location__r.SVMXC__Account__c)) {
                                Map<String,String> childAccountCountriesMap = new Map<String,String>();
                                childAccountCountriesMap.put(currentRole.Location__r.SVMXC__Account__r.country__r.CountryCode__c,currentRole.Location__r.SVMXC__Account__r.country__r.Name);
                                accountCountrySetMap.put(currentRole.Location__r.SVMXC__Account__c, childAccountCountriesMap);
                            } else {
                                Map<String,String> tempchildAccountCountriesMap = accountCountrySetMap.get(currentRole.Location__r.SVMXC__Account__r.UltiParentAcc__c);
                                if (tempchildAccountCountriesMap  != null) {
                                    tempchildAccountCountriesMap.put(currentRole.Location__r.SVMXC__Account__r.country__r.CountryCode__c,currentRole.Location__r.SVMXC__Account__r.country__r.Name);
                                    accountCountrySetMap.put(currentRole.Location__r.SVMXC__Account__c, tempchildAccountCountriesMap);
                                }
                            }
                        }
                        
                    }
                    System.debug('accountIds'+accountIds);
                    System.debug('accountCountrySetMap'+accountCountrySetMap);
                }
                
                
            }
        }
        if (accountIds != null && !accountIds.isEmpty()) {
            accountList = [select Name, AccountNumber, LastModifiedDate from Account where Id In: accountIds];
            system.debug('accountList'+accountList);
            if(!accountList.isEmpty()){
                AccountSize = accountList.size();
            }
            else{
                AccountSize  = 0;
            }
            System.debug('accountCountrySetMap'+accountCountrySetMap);
        }
        System.debug('AccountSize'+AccountSize);
        
        if(accountList != null && accountList.size()==1){
            //   Pagereference pr ;
            //System.debug('countryMap'+countryMap);
            if(accountCountrySetMap != null && accountCountrySetMap.size() == 1 && accountCountrySetMap.get(accountList[0].id) != null && accountCountrySetMap.get(accountList[0].id).size()==1){
                Map<String,String> tempCountryMap = accountCountrySetMap.get(accountList[0].id);
                String countryCode;
                for(String key : tempCountryMap.keyset()){
                    countryCode = key;
                }
                
                pr = callpageredirect(countryCode,accountList[0].id);     
            }
            return pr;
        }
        
        else{
            return null;
        }
        
    }
    
    public Pagereference callpageredirect(String countryCode,String companyId){
        //   PageReference siteSelectionPage = new Pagereference('/apex/VFP_SRV_MYFSSiteSelecitonPage?Country='+countryCode + '&Params='+companyId);
        PageReference siteSelectionPage ;
        if(isInternalUser==True){
            
         //   siteSelectionPage = new Pagereference(System.label.CLSRVMYFS_CUSTOM_LANDING_REDIRECTION_URL+countryCode + '&Params='+companyId+'&UId='+userId);
            siteSelectionPage = new Pagereference(System.label.CLSRVMYFS_CUSTOM_LANDING_REDIRECTION_URL+'?Country='+countryCode + '&Params='+companyId+'&UId='+userId);
            Cookie UsrFrstname = new Cookie('UsrFirstname', '',null,0,false); // Note the 0 to delete the cookie
            ApexPages.currentPage().setCookies(new Cookie[]{UsrFrstname});
            Cookie Usrlstname = new Cookie('UsrLastname', '',null,0,false); // Note the 0 to delete the cookie
            ApexPages.currentPage().setCookies(new Cookie[]{Usrlstname});
            Cookie UsrFirstname = new Cookie('UsrFirstname',usrInfo.firstname,null,100000,false);
            Cookie UsrLastname = new Cookie('UsrLastname',usrInfo.lastname,null,100000,false);
            ApexPages.currentPage().setCookies(new Cookie[]{UsrFirstname});
            ApexPages.currentPage().setCookies(new Cookie[]{UsrLastname});
            system.debug('record'+usrInfo);
            system.debug('firstname'+usrInfo.firstname);
            system.debug('lasttname'+usrInfo.lastname);
            Cookie counter = ApexPages.currentPage().getCookies().get('UsrFirstname');
            system.debug('raghav'+counter.getValue());
            
        }
        else{
           // siteSelectionPage = new Pagereference(System.label.CLSRVMYFS_CUSTOM_LANDING_REDIRECTION_URL+countryCode +'&Params='+companyId);
              siteSelectionPage = new Pagereference(System.label.CLSRVMYFS_CUSTOM_LANDING_REDIRECTION_URL+'?Country='+countryCode +'&Params='+companyId);

        }
        siteSelectionPage.setRedirect(true);
        return siteSelectionPage;
    }
    
    private void debug(String aMessage) {
        System.debug('#### ' + aMessage);
    }
}