/*
    Description: Batch Class updates method updates Amount, Revenue Amount Converted, Business Mix , Is A multi-level Business, 
                 Is A Level 3 fields of Opportunity based on the Opportunity Lines
*/
global class BATCH_OpportunityLineUpdates implements Database.Batchable<sObject>,Schedulable
{
    //Variables Declaration
    String query;
    List<Opportunity> lstOpportunitesTobeUpdated = new List<Opportunity>();
    Map<Id,List<OPP_ProductLine__c>> opportunityAllProdLinesMap=new Map<Id,List<OPP_ProductLine__c>>();
    Map<String,CS_ProductLineBU__c> businessUnitMap = CS_ProductLineBU__c.getall();        

    //Start method
    global Database.querylocator start(Database.BatchableContext BC)
    {
        /*return Database.getQueryLocator([select IncludedinForecast__c,Opportunity__r.StageName, Opportunity__r.TECH_IsOpportunityUpdate__c,Id, Amount__c,LineAmount__c,CurrencyISOCode,IsaLevel3memberopptyLine__c, IsALevel2__c, ProductBU__c, LineStatus__c,Opportunity__c,ParentLine__c, LineType__c,BusinessMixValue__c from OPP_ProductLine__c where Opportunity__r.TECH_IsOpportunityUpdate__c = true]);*/
        //Changed query from Opportunity line to Opportunity to fix bug of oppty lines flowing in more than one batch and updating oppty with incorrect amt - March 2016 release change
        return Database.getQueryLocator([select StageName,TECH_IsOpportunityUpdate__c,Id from Opportunity where TECH_IsOpportunityUpdate__c = true]);
    }
    
    //The method updates method updates Amount, Revenue Amount Converted, Business Mix , Is A multi-level Business, 
    //Is A Level 3 fields of Opportunity based on the Opportunity Lines
    global void execute(Database.BatchableContext BC, List<Opportunity> opplst)
    {
        VFC61_OpptyClone.isOpptyClone = true;
        List<OPP_ProductLine__c> allProductLines= new  List<OPP_ProductLine__c>();
        Map<Id,List<OPP_ProductLine__c>> opportunityAllProdLinesMap=new Map<Id,List<OPP_ProductLine__c>>();
        List<Opportunity> oppToBeUpd = new List<Opportunity>();
        //inorder to use existing code add lines into allProductLines variable - March 2016 release change
        allProductLines=[select IncludedinForecast__c,Opportunity__r.StageName, Opportunity__r.TECH_IsOpportunityUpdate__c,Id, Amount__c,LineAmount__c,CurrencyISOCode,IsaLevel3memberopptyLine__c, IsALevel2__c, ProductBU__c, LineStatus__c,Opportunity__c,ParentLine__c, LineType__c,BusinessMixValue__c from OPP_ProductLine__c Where Opportunity__c in :opplst];
        for(OPP_ProductLine__c productLine:allProductLines)
        {
            if(opportunityAllProdLinesMap.containsKey(productLine.Opportunity__c))
                opportunityAllProdLinesMap.get(productLine.Opportunity__c).add(productLine);
            else
                opportunityAllProdLinesMap.put(productLine.Opportunity__c,new List<OPP_ProductLine__c>{productLine});
        }
        
        
        //Iterates through all the Product Lines
        for(Id oppId: opportunityAllProdLinesMap.keyset())
        {
            Decimal amount = 0, revAmount=0, revAmountEUR = 0;
            Decimal bussMixVal=0,level3 = 0,level3Else = 0, level2= 0,prevBussMix, prevLevel3;
            String BUMix;
            
            for(OPP_ProductLine__c productLine:opportunityAllProdLinesMap.get(oppId))
            {
                //calculate Amount
                String stgeName = productLine.Opportunity__r.StageName;
                if(stgeName == Label.CLOCT13SLS13 || stgeName == Label.CL00146 || stgeName == Label.CL00147 || stgeName == Label.CL00547 || stgeName == Label.CLOCT13SLS17 || stgeName == Label.CLSEP12SLS11)
                {
                    if(productLine.LineAmount__c!=null && productLine.LineType__c == Label.CLOCT13SLS07 &&(productLine.LineStatus__c == Label.CLOCT13PRM20 || productLine.LineStatus__c == Label.CLOCT13SLS05 || productLine.LineStatus__c == Label.CLSEP12I2P103 || productLine.LineStatus__c == Label.CLOCT13SLS28|| productLine.LineStatus__c == Label.CLOCT13SLS29))
                    {
                        amount+=productLine.LineAmount__c; 
                    }    
                }
                else if(stgeName == Label.CL00272)
                {
                    if(productLine.LineAmount__c!=null && productLine.LineType__c == Label.CLOCT13SLS07 && productLine.LineStatus__c !=Label.CL00139 && productLine.LineStatus__c !=Label.CLJUN16SLS01)    // added LineStatus__c != Service Contract Amendment for June 2016 Release BR - 9826
                    {
                        amount+=productLine.LineAmount__c; 
                    }    
                }
                
                //Calculates Business Mix
                if(productLine.LineType__c == Label.CLOCT13SLS07 && productLine.LineStatus__c!= Label.CL00139 && productLine.LineStatus__c!= Label.CLSEP12SLS14 && productLine.LineStatus__c!= Label.CL00326 && businessUnitMap.get(productLine.ProductBU__c)!=null)
                {                
                    if(BUMix==null)
                        BUMix=businessUnitMap.get(productLine.ProductBU__c).ProductBU__c+'';
                    else if(!(BUMix.contains(businessUnitMap.get(productLine.ProductBU__c).ProductBU__c)))
                        BUMix+=';'+businessUnitMap.get(productLine.ProductBU__c).ProductBU__c;
                }                 
                //Calculates IsAmultiLevelBusiness & TECH_IsALevel3
                if(productLine.LineType__c == Label.CLOCT13SLS07 && 
                    productline.LineStatus__c!= Label.CL00326 && 
                    productline.LineStatus__c!= Label.CLSEP12SLS14 && 
                    productline.LineStatus__c!= Label.CL00139)
                {
                    if(productline.IsaLevel3memberopptyLine__c ==1)
                    {
                        ++level3Else;
                    }                    
                    for(OPP_ProductLine__c productLine1:opportunityAllProdLinesMap.get(oppId))
                    {               
                        if(productLine1.Id!= productLine.Id && 
                        productLine1.LineType__c == Label.CLOCT13SLS07 && 
                        productline1.LineStatus__c!= Label.CL00326 && 
                        productline1.LineStatus__c!= Label.CLSEP12SLS14 && 
                        productline1.LineStatus__c!= Label.CL00139                     
                        )
                        {
                            /*if(productline.IsALevel2__c ==1 && productline1.IsALevel2__c ==1 && productLine.ProductBU__c!= productLine1.ProductBU__c)
                            {
                                ++bussMixVal;
                            }*/
                            if(productline.IsALevel2__c ==1 && productline1.IsALevel2__c ==1)
                            {
                                ++bussMixVal;
                            }
                            /*if(productline.IsaLevel3memberopptyLine__c ==1 && productline1.IsaLevel3memberopptyLine__c ==1 && productLine.ProductBU__c!= productLine1.ProductBU__c)
                            {
                                ++level3;
                            } */
                            
                            /*if(productline.IsALevel2__c ==1 && productline1.IsaLevel3memberopptyLine__c ==1 && productLine.ProductBU__c!= productLine1.ProductBU__c)
                            {
                                ++level2;
                            }*/                           
                            if(productline.IsALevel2__c ==1 && productline1.IsALevel2__c ==1 && productLine.BusinessMixValue__c!= productLine1.BusinessMixValue__c)
                            {
                                ++level2;
                            }                           
                                
                        }
                    } 
                }              
            }
            Integer bussMixValue=0, level3Value = 0;
            if(bussMixVal>=2)
                bussMixValue = 1;
            else
                bussMixValue = 0;                

            if(level2>=2)
                level3Value  = 1;
            else
                level3Value  = 0;
            /*
            if(level3<2)
            {
                if(level2>=1 && level3Else>=1)
                    level3Value = 1;
                else
                    level3Value = 0;
            }
            else
                level3Value = 1;
            */
            oppToBeUpd.add(new Opportunity(TECH_IsOpportunityUpdate__c=false,Id=oppId,
            BusinessMix__c = BUMix, Amount = amount, IsAMultiBusinessLevel2__c = bussMixValue, TECH_IsLevel3__c =level3Value));

          }
        //Updates the Opportunity
        Database.update(oppToBeUpd,false);
    }

    global void execute(SchedulableContext sc)
    {
        BATCH_OpportunityLineUpdates oppLineUpdates = new BATCH_OpportunityLineUpdates(); 
        database.executebatch(oppLineUpdates,Integer.ValueOf(Label.CLOCT14SLS71));
    }
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {

    }
}