@isTest
public class VFC_SRV_MyFSCustomLandingController_TEST {
    static testMethod void queryAccountsAndCountries() {
        test.starttest();
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
                              and developerName = 'Customer'
                              limit 1
                             ][0].Id;
        
        Account acc1 = new Account(Name = 'TestAccount', Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11');
        insert acc1;
        // Account acc2 = new Account(Name = 'TestAccountwithsameultimateparent account', Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '22');
        // insert acc2;
        Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
        insert cnct;
        
        Profile p = [SELECT Id FROM Profile WHERE Name = :System.label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];
        
       user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
                          EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                          ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = 'testuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
                          // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
                          isActive = True, BypassVR__c = True, contactId = cnct.id);
        insert u;
        system.runAs(u) {
            
            SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id);
            insert location1;
            // SVMXC__Site__c location2 = new SVMXC__Site__c(Name = 'TestLocation2', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc2.Id);
            //  insert location2;
            RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
                                                      and developerName = 'LocationContactRole'
                                                      limit 1
                                                     ]; //[0].Id;
            //Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
            //insert ro;
            //Role__c ro2 = new Role__c(Location__c = location2.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
            // insert ro2;
            PageReference pageRef = Page.VFP_SRV_MyFSCustomLanding;
            VFC_SRV_MyFSCustomLandingController contlr = new VFC_SRV_MyFSCustomLandingController();
            contlr.UltimateParentAccount = acc1.id;
            contlr.companysize = 0;
            contlr.queryAccounts();
            contlr.forwardToCustomAuthPage();
            
            //VFC_SRV_MYFsPersonalInformatinController Test Coverage
            
          /*  VFC_SRV_MYFsPersonalInformatinController PersonalInformation = new VFC_SRV_MYFsPersonalInformatinController();
            PersonalInformation.save();
            PersonalInformation.cancel();
            VFC_SRV_MYFsPersonalInformatinController.setContactFields(Cnct,u);*/
            Test.stoptest();
        }
    }
    
    
    static testMethod void testmethod2() {
       
        Account parentacc = new Account(name = 'parentacc');
        insert parentacc;
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
                              and developerName = 'Customer'
                              limit 1
                             ][0].Id;
        
        Account acc1 = new Account(Name = 'TestAccount', Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11', UltiParentAcc__c = parentacc.id);
        insert acc1;
        Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
        insert cnct;
        
        Profile p = [SELECT Id FROM Profile WHERE Name = :System.label.CLSRVMYFS_PRIFILENAMEFORTESTCLASS];
        test.starttest(); 
       user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
                          EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                          ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
                          // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
                          isActive = True, BypassVR__c = True, contactId = cnct.id);
        insert u;
        
        
     system.runAs(u) {
            
            
            SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id);
            insert location1;
            
            RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
                                                      and developerName = 'LocationContactRole'
                                                      limit 1
                                                     ]; //[0].Id;
            Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
            insert ro;
            
            PageReference pageRef = Page.VFP_SRV_MyFSCustomLanding;
            VFC_SRV_MyFSCustomLandingController contlr = new VFC_SRV_MyFSCustomLandingController();
            contlr.UltimateParentAccount = acc1.id;
            contlr.companysize = 0;          

            contlr.queryAccounts();
            contlr.forwardToCustomAuthPage();
            
            
            Test.stoptest();
       }
    }
    
     static testMethod void testmethod3() {
       
        Account parentacc = new Account(name = 'parentacc123');
        insert parentacc;
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType = 'Account'
                              and developerName = 'Customer'
                              limit 1
                             ][0].Id;
        
        Account acc1 = new Account(Name = 'TestAccount', Street__c = 'New Street', POBox__c = 'XXX', ZipCode__c = '12345', recordtypeId = businessAccRTId, SEAccountId__c = '11', UltiParentAcc__c = parentacc.id);
        insert acc1;
        Contact Cnct = new Contact(AccountId = acc1.id, email = 'UTCont@mail.com', phone = '1234567890', LastName = 'UTContact001', FirstName = 'Fname1');
        insert cnct;
        
      Profile p = [SELECT Id FROM Profile WHERE Name = 'SE - CCC Advanced User'];
        test.starttest(); 
       user u = new User(Alias = 'standt', Email = 'testprmuserlogin.login@schneider-electric.com',
                          EmailEncodingKey = 'UTF-8', FirstName = 'Test First', LastName = 'Testing', CommunityNickname = 'CommunityName123', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US',
                          ProfileId = p.Id, TimeZoneSidKey = 'America/Los_Angeles',
                          UserName = 'testprmuserlogin.login@schneider-electric.com', //UserRoleId = ur.Id,
                          // FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
                          isActive = True, BypassVR__c = True);
       insert u;
        
        
    // system.runAs(u) {
            
           // test.starttest();
            SVMXC__Site__c location1 = new SVMXC__Site__c(Name = 'TestLocation1', SVMXC__Location_Type__c = 'Alley', SVMXC__Account__c = acc1.Id);
            insert location1;
            Country__c country = new Country__c();
              // Country__c Country = new Country__c();
              Country.name = 'USA';
              Country.CountryCode__c = 'US';
              insert country;
            
            RecordType locationContactRecordTypeId = [SELECT Id from RecordType where SobjectType = 'Role__c'
                                                      and developerName = 'LocationContactRole'
                                                      limit 1
                                                     ]; //[0].Id;
            Role__c ro = new Role__c(Location__c = location1.Id, recordtypeId = locationContactRecordTypeId.id, Role__c = 'Site Manager', Contact__c = cnct.id);
            insert ro;
            
            PageReference pageRef = Page.VFP_SRV_MyFSCustomLanding;
            VFC_SRV_MyFSCustomLandingController contlr = new VFC_SRV_MyFSCustomLandingController();
            contlr.UltimateParentAccount = acc1.id;
            contlr.companysize = 0;
            contlr.userId = UserInfo.getUserid();
             ApexPages.currentPage().getParameters().put('UId',UserInfo.getUserid());

            contlr.queryAccounts();
         
           contlr.callpageredirect(Country.CountryCode__c,acc1.id);
            contlr.forwardToCustomAuthPage();
            
            Test.stoptest();
      // }
    }
    
}