public class VFC_AssessmentPrograms {

    private Assessment__c assess = null;
    private ID returnID;

    public List<ProgramAssessmentWrapper> ProgramAssessment { get;set;}

    public List<AssessementProgram__c> lstDeletedAssessments = new  List<AssessementProgram__c>();

    public VFC_AssessmentPrograms(ApexPages.StandardController controller) {

        SObject record = controller.getRecord();
        //System.Debug('***-- Record :'+record);
        ProgramAssessment = new List<ProgramAssessmentWrapper>();

        assess = ((Assessment__c)record);
        //System.Debug('***-- assess:'+assess);
        ProgramAssessment = getProgramFeatures();
        returnID = assess.id;
    }

    public void addNewRow() {

       ProgramAssessmentWrapper tmp = new ProgramAssessmentWrapper(); 

        //if(assess != null)
        {
            tmp.Assessment.Assessment__c = assess.Id;
            tmp.Assessment.Active__c = true;
            ProgramAssessment.add(tmp);
        }
    }

    public void deleteRow()
    {
        List<ProgramAssessmentWrapper> lstUndeletedAssesements = new List<ProgramAssessmentWrapper>();
        lstDeletedAssessments.clear();

        for(ProgramAssessmentWrapper awraperAssesement :ProgramAssessment)
        {
           if(awraperAssesement.ToBeDeleted == true) 
           {
               String recId = String.ValueOf(awraperAssesement.RecordId);
               //System.debug('RecordID-------'+recId);
               if(recId != null && recId != '')
                    if(assess != null)
                        lstDeletedAssessments.add(awraperAssesement.Assessment);
           }
           else
               lstUndeletedAssesements.add(awraperAssesement);
        }

        ProgramAssessment.clear();
        if(lstUndeletedAssesements.size() > 0)
            ProgramAssessment.addAll(lstUndeletedAssesements);
    }

    public class ProgramAssessmentWrapper {

        public Id RecordId { get; set; }
        public string UpdateStatus { get; set; }

        public AssessementProgram__c Assessment {get;set;} 

        public boolean ToBeDeleted { get; set; }

        public ProgramAssessmentWrapper() {
            Assessment = new AssessementProgram__c();
        }
    }

    public List<ProgramAssessmentWrapper> getProgramFeatures()
    {
        Map<Id, AssessementProgram__c> assessMap  = new Map<Id, AssessementProgram__c>([SELECT Id, PartnerProgram__c, ProgramLevel__c, Active__c FROM AssessementProgram__c WHERE Assessment__c = :assess.Id
                                                                        AND PartnerProgram__c != null ]);
        List<ProgramAssessmentWrapper> lProgAsess = new List<ProgramAssessmentWrapper>();

        if (assessMap != null && !assessMap.isEmpty())
        {
            for (String assessId : assessMap.keyset())
            {
                AssessementProgram__c asess= new  AssessementProgram__c(Id=assessId,
                                     PartnerProgram__c = assessMap.get(assessId).PartnerProgram__c,
                                     ProgramLevel__c = assessMap.get(assessId).ProgramLevel__c,
                                     Active__c = assessMap.get(assessId).Active__c
                                     
                                 );
                ProgramAssessmentWrapper tmp = new ProgramAssessmentWrapper();
                tmp.RecordId = assessId;
                tmp.Assessment = asess;

                lProgAsess.add(tmp);
            }
        }
        if (lProgAsess!= null && lProgAsess.size() <= 0)
        {
            ProgramAssessmentWrapper objPrgmAsessWrapper = new ProgramAssessmentWrapper();
            objPrgmAsessWrapper.Assessment = new AssessementProgram__c();
            lProgAsess.add(objPrgmAsessWrapper);
        }
        //System.Debug('*** Map:'+assessMap);
        return lProgAsess;
    }

    public pagereference doSave() 
    {  

        List<AssessementProgram__c> lstNewAssessments = new List<AssessementProgram__c>();
        boolean fsaveSuccess = true;
        integer i=0;
        try
        {
            for(ProgramAssessmentWrapper assesment: ProgramAssessment)
            {
                //Adding assesements
                String NewRecId = String.ValueOf(assesment.RecordId);
                if(NewRecId == null || NewRecId == '')
                {
                    //System.debug('NewRecId ' +NewRecId );
                    assesment.Assessment.Assessment__c = assess.Id;
                }
                lstNewAssessments.add(assesment.Assessment);
            }
            //if(lstNewAssessments.size() > 0)
                //Upsert lstNewAssessments;
                
            if(lstNewAssessments.size() > 0)
            { 
                //System.debug('lstNewAssessments' +lstNewAssessments);
                fsaveSuccess = true;
                Database.UpsertResult[] assessList = Database.upsert(lstNewAssessments, false);
                for (Database.UpsertResult assess : assessList) {
                    if (assess.isSuccess()) 
                    {
                        system.debug('***** Success ****');
                       // ProgramFeatureRequirements[i].UpdateStatus = 'Success';
                        ProgramAssessment[i].Assessment.id  = assess.getId();
                        ProgramAssessment[i].RecordId = assess.getId();
                    }
                    else
                    {
                        System.debug('Error: '+assess.getErrors()[0].getStatusCode() + ': ' + assess.getErrors()[0].getMessage());
                        ProgramAssessment[i].UpdateStatus = 'Error: '+assess.getErrors()[0].getMessage();
                        fsaveSuccess= false;
                    }

                    i=i+1;
                }
                    //if (!fsaveSuccess)
                        //Database.rollback(sp);
            }
            //System.debug('lstDeletedAssessments:' + lstDeletedAssessments);
            if(lstDeletedAssessments.size() >0)
            {
                Delete lstDeletedAssessments;
            }

            if (!fsaveSuccess)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.label.CLOCT14PRM12));
            }

        }
        catch(Exception e)
        {
            //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            system.debug('Exception in doSave:'+e.getmessage());
        }

        if(fsaveSuccess)
        {
            PageReference SaveOrfPage = new PageReference('/'+returnID);
            return SaveOrfPage ;
        }
        else
            return null ;
  }

    public pagereference doCancel(){ 

        PageReference orfPage = new PageReference('/'+returnID);
        return orfPage  ;
    }
}