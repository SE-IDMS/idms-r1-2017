/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 07 September 2012
    Description  : For September 2012 Release:
                   1. Update the FreeofCharge checkbox on Case if the Free of Charge checkbox is updated in Package Template
***************************************************************************************************************************/
public class AP_TemplateTriggerUtils
{
    public static void UpdateCase(map<Id,PackageTemplate__c> mapNewTemplate , map<Id,PackageTemplate__c> mapOldTemplate)
    {

        map<Id,PackageTemplate__c> mapTemplate = new map<Id,PackageTemplate__c>();
        for(Id templateID : mapNewTemplate.keySet())
        {
            if(mapNewTemplate.get(templateID).FreeOfCost__c != mapOldTemplate.get(templateID).FreeOfCost__c)
            {
                mapTemplate.put(templateID,mapNewTemplate.get(templateID));
            }
        }
        
        list<Package__c> lstPKG = [Select id, PackageTemplate__c, Contract__c from Package__c where packagetemplate__c in : mapTemplate.keySet() limit 50000];
        map<Id,Id> mapContractTemplateId = new map<Id,Id>();
        for(PAckage__c pkg :lstPKG)
            mapContractTemplateId.put(pkg.Contract__c , pkg.packagetemplate__c);
        list<Case> lstCase = [Select RElatedContract__c , FreeofCharge__c from CAse where relatedContract__c in : mapContractTemplateId.keySet() limit 1000];
        for(Case c : lstCase)
            c.FreeofCharge__c = mapTemplate.get(mapContractTemplateId.get(c.relatedContract__c)).FreeOfCost__c;
        try
        {
            update lstCase;
        }
        catch(Exception e)
        {
system.debug('Error in TemplateTriggerUtils:'+e.getMessage());
        }
    }
}