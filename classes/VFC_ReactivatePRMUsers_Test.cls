@isTest 
public class VFC_ReactivatePRMUsers_Test{

    static testMethod void testMe(){
        
        VFC_ReactivatePRMUsers RPU = new VFC_ReactivatePRMUsers();
        
        User u = [SELECT Id, Username FROM User WHERE Id = :UserInfo.getUserId()];
        u.BypassVR__c = true;
        
        System.runAs (u) {
    
            Account acc = new Account();
            acc.RecordTypeId = System.Label.CLAPR15PRM025;  
            acc.Name = 'account';
            acc.PRMCompanyName__c = 'NewCompanyName';
            acc.PRMStreet__c = 'Elnath Street ';
            acc.PRMAdditionalAddress__c = 'Marathalli';
            acc.PRMCity__c  = 'Bangalore';
          
            acc.PRMCompanyPhone__c = '987654321';
            acc.PRMWebsite__c = 'Https://schneider-electric.com';
            acc.PRMZipCode__c = '560103';
            acc.PRMCorporateHeadquarters__c = true;
            acc.PRMUIMSID__c = '1234';
            acc.PRMBusinessType__c = 'FI';
            acc.PRMAreaOfFocus__c = 'FI1';
            //acc.PRMCountry__c = testCountry.Id;
            INSERT acc;    
            
            Contact con1 = new Contact();
            con1.FirstName = 'firstNameContact1'; 
            con1.LastName = 'lastNameContact1';
            con1.PRMUIMSID__c = '123';
            con1.AccountId = acc.Id;
            con1.PRMContact__c = True;
            con1.PRMPrimaryContact__c = True;
            con1.Email = 'test123@abc.com';
            con1.PRMEmail__c = 'test123@abc.com';
            con1.PRMWorkPhone__c = '9876543211';
            
            con1.PRMFirstName__c = 'firstNameContact1'; 
            con1.PRMLastName__c = 'lastNameContact1';
            con1.PRMContactRegistrationStatus__c = 'Registering';
            INSERT con1;
            
            Contact con2 = new Contact();
            con2.FirstName = 'firstNameContact2'; 
            con2.LastName = 'lastNameContact2';
            con2.PRMUIMSID__c = '1234';
            con2.AccountId = acc.Id;
            con2.PRMContact__c = True;
            con2.PRMPrimaryContact__c = True;
            con2.Email = 'test1234@abc.com';
            con2.PRMEmail__c = 'test1234@abc.com';
            con2.PRMWorkPhone__c = '9876543212';
            
            con2.PRMFirstName__c = 'firstNameContact2'; 
            con2.PRMLastName__c = 'lastNameContact2';
            con2.PRMContactRegistrationStatus__c = 'Registering';
            INSERT con2;
            
            UIMSCompany__c uimsc = new UIMSCompany__c();
            uimsc.Street__c = 'abced';
            uimsc.AdditionalAddress__c = 'xyznbshx';
            uimsc.AreaofFocus__c= 'Internal1';
            uimsc.BusinessType__c = 'Internal';
            uimsc.City__c= 'city1';
            uimsc.CompanyName__c= 'Abc';
            uimsc.CompanyPhone__c= '78682837621';
            uimsc.CompanyWebsite__c = 'abc.com';
            //uimsc.Country__c = testCountry123.Id;
            uimsc.Email__c = 'test123@abc.com';
            uimsc.FirstName__c = 'j';
            uimsc.JobFunction__c = 'Z024'; 
            uimsc.JobTitle__c = 'ZO';
            uimsc.Language__c = 'en_US';
            uimsc.LastName__c = 'k';
            uimsc.Origin__c = 'Self Registration';
            uimsc.OriginSource__c = 'Tradeshow';
            uimsc.OriginSourceInfo__c='ahdsnx';
            uimsc.PhoneNumber__c = '9876543211';
            //uimsc.StateProvince__c = testStateProvince123.Id;
            uimsc.ZipCode__c= '9897665';
            uimsc.bFOAccount__c=acc.Id;
            uimsc.bFOContact__c=con1.Id;
            uimsc.AccountCreationStatus__c=true;
            insert uimsc;
                        
            DateTime NOW = datetime.now();
            DateTime LAST_2Days = NOW.addHours(-96);
            
            
            VFC_ReactivatePRMUsers.DisplayPRMContacts('123', 'test123@abc.com', string.valueof(LAST_2Days), string.valueof(NOW));
            
            String queryStr = ' SELECT PRMFirstName__c, PRMLastName__c, PRMEmail__c, Account.Name, PRMUIMSID__c, PRMPrimaryContact__c, ' +
                                ' PRMRegistrationSubmissionDate__c, toLabel(PRMContactRegistrationStatus__c), PRMOrigin__c FROM Contact ' +
                                ' WHERE PRMContactRegistrationStatus__c =\'Registering\' and PRMUIMSID__c != null and (PRMEmail__c =\'test123@abc.com\' or PRMEmail__c =\'test1234@abc.com\')';
            
            list<Contact> lCnts = Database.query(queryStr);        
        
            Test.StartTest();
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());        
            VFC_ReactivatePRMUsers.ReactivateContacts(lCnts);
            Test.StopTest();        
        }    
    }
}