/****************
To Select:Zone and Country

******************/



Public Class VFC_ProjectRegionCountrySelection {
    

    
    public Milestone1_Project__c MileProj{get;set;}
    public List<WrapperProjectCountry>  listWrapperCntry{get;set;}
    public string strRecordType;
    public string strOfferid;
    
    public string strRegn{get;set;}
    public string strZone{get;set;}
    public boolean IsEmpty{get;set;}
    public boolean IsCountrySel{get;set;}
    public boolean isrecordType{get;set;}
    public VFC_ProjectRegionCountrySelection(ApexPages.StandardController controller) {
        MileProj=new Milestone1_Project__c();
        isrecordType=false;
        listWrapperCntry= new List<WrapperProjectCountry>(); 
        strOfferid = ApexPages.currentPage().getParameters().get('urlOfferId');
        strRecordType =ApexPages.currentPage().getParameters().get('RecordType');
        Id projecIntrductionRegon = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Introduction Region').getRecordTypeId();
        Id projecSolutionRegon = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Launch Solution Region').getRecordTypeId(); 
        Id projecWithdrawalRegon = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Offer Withdrawal Region').getRecordTypeId();        
        if(strRecordType==projecIntrductionRegon || strRecordType==projecSolutionRegon ){
                isrecordType=true;
            
        }else if(strRecordType==projecWithdrawalRegon)   {
            isrecordType=false;
        }
        system.debug('TESTTRECR'+isrecordType);   
        MileProj.Offer_Launch__c=strOfferid;
         MileProj.RecordTypeid=strRecordType;
        IsEmpty=true;
        IsCountrySel=false;

    }   
  
    
    
    public PageReference pgRefSaveProject() {
        IsCountrySel=false;
        List<WrapperProjectCountry>  ListPrtCoutry = new List<WrapperProjectCountry>();
        list<ProjectZoneCountry__c>   listPrZone= new list<ProjectZoneCountry__c>();
        ProjectZoneCountry__c NewPrZoneCtry= new ProjectZoneCountry__c();
        boolean blnIsError=false;
        PageReference NewProjectEditPage;
        Database.SaveResult lsrMproject;        
        boolean isDup;
        system.debug(MileProj);
        //insert MileProj; 
        Try {
            MileProj.RecordTypeid=strRecordType;
            
            isDup=checkingDuplicateRegion(new List<Milestone1_Project__c> {MileProj} );
             if(isDup) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,label.CLMAR16ELLAbFO10));
                blnIsError=true; 
                 
             }else {
                lsrMproject = Database.insert(MileProj,false); 
                //insert MileProj;  
                if(!lsrMproject.isSuccess()) {
                    List<Database.Error> e=lsrMproject.getErrors();                 
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,e[0].getMessage()));
                    blnIsError=true;
                    
                }
            }
            
            
        }
        catch (Exception Exp) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Exp.getMessage()));
                blnIsError=true;
        }
        if(listWrapperCntry.size() > 0) {
            for(WrapperProjectCountry WrpObj:listWrapperCntry) {
                //IsCountrySel=false;   
                if(WrpObj.blnIsSelect) {
                        ListPrtCoutry.add(WrpObj);
                    IsCountrySel=true;
                }

            }   
        }
        if(!IsCountrySel) {
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,label.CLMAR16ELLAbFO08));
            blnIsError=true;    
            
        }
        
        if(ListPrtCoutry.size() > 0 && IsCountrySel && !isDup ) {
            for(WrapperProjectCountry validObj:ListPrtCoutry) {
                NewPrZoneCtry= new ProjectZoneCountry__c();
                NewPrZoneCtry.Project__c=lsrMproject.getId();//MileProj.id;
                NewPrZoneCtry.Country__c=validObj.Coutry.id;
                listPrZone.add(NewPrZoneCtry);
                
            
            }
            
        }
        
        if(listPrZone.size() > 0) {
            try {
            Database.SaveResult[] saveList = Database.insert(listPrZone,false);
            for(Database.SaveResult lsrMprObj:saveList) {
                if(!lsrMprObj.isSuccess()) {
                     List<Database.Error> de=lsrMprObj.getErrors();
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, de[0].getMessage()));
                    blnIsError=true;
                }
            }
                        
            } catch(Exception Exp) {
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, Exp.getMessage()));
                blnIsError=true;
            }
            

        }
             // project
        if(!blnIsError) {
            
             
            NewProjectEditPage = new PageReference('/'+MileProj.id+'/e?' );
            NewProjectEditPage.getParameters().put(label.CLMAR16ELLAbFO09, MileProj.id);    //'retURL'    
             
            NewProjectEditPage.setredirect(true);
            return NewProjectEditPage;
        }else {
            
            return null;    
        }        
        
        
        
        
    } 
    
    public void listRegionCountry() {
     system.debug(strRegn);
     system.debug(strZone);
        IsEmpty=false;
        listWrapperCntry= new List<WrapperProjectCountry>(); 
        if(strRegn!=null && strRegn!='' && strZone!=null && strZone!='' ) {
        
            List <Country__c > lstCoury = [SELECT id, Name,Region__c,SubRegion__c  FROM Country__c where Region__c=:strRegn and SubRegion__c=:strZone ]; 
            system.debug('List%%%%%%%%'+lstCoury );
            if(lstCoury.size() > 0) {               
            
                for(Country__c CotyObj:lstCoury) {              
                    listWrapperCntry.add(new WrapperProjectCountry(CotyObj,false) );
                }
            }
            
        }
        if(listWrapperCntry.size() >0) {
            IsEmpty=true;
        }
        
    }
    
    public class WrapperProjectCountry{    
        public Boolean blnIsSelect{get;set;}
        public Boolean isToSelect{get;set;}
        public Country__c Coutry{get;set;}
        
        public WrapperProjectCountry(Country__c prtCountry,boolean boolnToSelect) {
            blnIsSelect=false;
            isToSelect=boolnToSelect;
            Coutry=prtCountry;

        }
    }
    
    public boolean checkingDuplicateRegion( List<Milestone1_Project__c> triggerList ){
        List<String> regionList = new List<String>();
        List<String> zoneList = new List<String>();
        List<String> OfferList = new List<String>();
        boolean isduplicate=false;
        for( Milestone1_Project__c tm : triggerList ){ 
            if(tm.Region__c!=null && tm.Zone__c!=null && tm.Offer_Launch__c!=null) {            
            
                regionList.add(tm.Region__c );
                zoneList.add(tm.Zone__c);
                OfferList.add(tm.Offer_Launch__c); 
            }
        }
        Integer projectCount = [SELECT count() FROM Milestone1_Project__c WHERE Region__c IN: regionList and Zone__c IN:zoneList and Offer_Launch__c IN:OfferList  limit 1];

        
        if( projectCount > 0 ) {
                isduplicate=true;
        }
        return isduplicate; 

    }
    
    
}