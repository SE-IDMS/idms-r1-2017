public  class AP_AccountAssignedFeature {

    public static void checkDuplicateAAF(List<AccountAssignedFeature__c> lstNewAAF)
    {
        map<String,AccountAssignedFeature__c> mpAaf = new map<String,AccountAssignedFeature__c>();
        set<Id> accs = new set<id>();
        set<id> fCs = new set<id>();
        for(AccountAssignedFeature__c tAAF : lstNewAAF)
        {
            
            accs.add(tAAF.Account__c);
            fCs.add(tAAF.FeatureCatalog__c);
            String tstr = tAAF.Account__c+'-'+tAAF.FeatureCatalog__c;
            if(mpAaf.containsKey(tstr))
            {   
                mpAaf.get(tstr).addError(System.Label.CLAPR14PRM55);
                tAAF.addError(System.Label.CLAPR14PRM55);
            }
            else
            {
                mpAaf.put(tstr , tAAF);
            }
        }

        List<AccountAssignedFeature__c> lstEistAAF = [Select Account__c, FeatureCatalog__c From AccountAssignedFeature__c Where Account__c in:accs AND FeatureCatalog__c In:fCs AND id NOT IN:lstNewAAF];

        for(AccountAssignedFeature__c eAAF : lstEistAAF)
        {
            String tstr = eAAF.Account__c+'-'+eAAF.FeatureCatalog__c;
            if(mpAaf.containsKey(tstr) && mpAaf.get(tstr).id != eAAF.id )
            {   
                mpAaf.get(tstr).addError(System.Label.CLAPR14PRM56);
            }
        }

    }
}