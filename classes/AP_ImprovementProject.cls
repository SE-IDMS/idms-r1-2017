// Added by Uttara - Q3 2016 Release - Notes to bFO (I2P)

Public class AP_ImprovementProject {
    
    // Setting the Approver and Coach from the Accountable Organization of the IP
    Public static void SetProjectApprover(Map<String,CSQ_ImprovementProject__c> mapOrgIdIPrecord) {
        
        Map<String,CSQ_ImprovementProject__c> mapSubEntityIPrecord = new Map<String,CSQ_ImprovementProject__c>();
        Map<String,CSQ_ImprovementProject__c> mapEntityIPrecord = new Map<String,CSQ_ImprovementProject__c>();
        Map<String,CSQ_ImprovementProject__c> mapCSQMgrIPrecord = new Map<String,CSQ_ImprovementProject__c>();
        Integer CountLocation = 0;
        Integer CountSubEntity = 0;
        Integer CountEntity = 0;
    
        System.debug('!!!! Entered SetProjectApprover method');
        
        // Looping through the Organizations added in the look-up field (Detection Organization)
        for(EntityStakeholder__c OrgSH : [SELECT BusinessRiskEscalationEntity__c, BusinessRiskEscalationEntity__r.Name, BusinessRiskEscalationEntity__r.Location__c, BusinessRiskEscalationEntity__r.SubEntity__c, BusinessRiskEscalationEntity__r.Entity__c, Role__c, User__c FROM EntityStakeholder__c WHERE (BusinessRiskEscalationEntity__c IN: mapOrgIdIPrecord.keySet() AND Role__c = 'CI Deployment Leader')]) {
        
            mapOrgIdIPrecord.get(OrgSH.BusinessRiskEscalationEntity__c).CSQ_ProjectApprover__c = OrgSH.User__c; 
            mapOrgIdIPrecord.get(OrgSH.BusinessRiskEscalationEntity__c).CSQ_Coach__c = OrgSH.User__c; 
            CountLocation++;
        }  
        System.debug('!!!! CountLocation : ' + CountLocation);
        System.debug('!!!! mapOrgIdIPrecord.size() : ' + mapOrgIdIPrecord.size());
        System.debug('!!!! mapOrgIdIPrecord : ' + mapOrgIdIPrecord);
        
        // Filling the maps to look through SubEntity and Entity levels
        if(CountLocation != mapOrgIdIPrecord.size()) {
            for(BusinessRiskEscalationEntity__c Org : [SELECT SubEntity__c, Entity__c FROM BusinessRiskEscalationEntity__c WHERE Id IN: mapOrgIdIPrecord.keySet()]) {
                mapSubEntityIPrecord.put(Org.Entity__c+ ' - '+Org.SubEntity__c, mapOrgIdIPrecord.get(Org.Id));          
                mapEntityIPrecord.put(Org.Entity__c, mapOrgIdIPrecord.get(Org.Id));
            }                  
        
            // Looping through the SubEntity level Organizations of the look-up field (Detection Organization)
            for(EntityStakeholder__c OrgSH : [SELECT BusinessRiskEscalationEntity__c, BusinessRiskEscalationEntity__r.Name, BusinessRiskEscalationEntity__r.Location__c, BusinessRiskEscalationEntity__r.SubEntity__c, BusinessRiskEscalationEntity__r.Entity__c, Role__c, User__c FROM EntityStakeholder__c WHERE (BusinessRiskEscalationEntity__r.Name IN: mapSubEntityIPrecord.keySet() AND Role__c = 'CI Deployment Leader' AND BusinessRiskEscalationEntity__r.Location__c = null)]) {
                
                System.debug('!!!! Entered Sub-Entity loop'); 
                mapSubEntityIPrecord.get(OrgSH.BusinessRiskEscalationEntity__r.Name).CSQ_ProjectApprover__c = OrgSH.User__c;
                mapSubEntityIPrecord.get(OrgSH.BusinessRiskEscalationEntity__r.Name).CSQ_Coach__c = OrgSH.User__c;
                CountSubEntity++;   
            }
            System.debug('!!!! CountSubEntity : ' + CountSubEntity);
            System.debug('!!!! mapSubEntityIPrecord : ' + mapSubEntityIPrecord);
            
            if((CountLocation + CountSubEntity) < mapOrgIdIPrecord.size()) {
                // Looping through the Entity level Organizations of the look-up field (Detection Organization)
                for(EntityStakeholder__c OrgSH : [SELECT BusinessRiskEscalationEntity__c, BusinessRiskEscalationEntity__r.Name, BusinessRiskEscalationEntity__r.Location__c, BusinessRiskEscalationEntity__r.SubEntity__c, BusinessRiskEscalationEntity__r.Entity__c, Role__c, User__c FROM EntityStakeholder__c WHERE (BusinessRiskEscalationEntity__r.Name IN: mapEntityIPrecord.keySet() AND Role__c = 'CI Deployment Leader' AND BusinessRiskEscalationEntity__r.Location__c = null AND BusinessRiskEscalationEntity__r.SubEntity__c = null)]) { 
                    
                    System.debug('!!!! Entered Entity loop');
                    mapEntityIPrecord.get(OrgSH.BusinessRiskEscalationEntity__r.Name).CSQ_ProjectApprover__c = OrgSH.User__c;
                    mapEntityIPrecord.get(OrgSH.BusinessRiskEscalationEntity__r.Name).CSQ_Coach__c = OrgSH.User__c;
                    CountEntity++;
                }
                System.debug('!!!! CountEntity : ' + CountEntity);
                System.debug('!!!! mapEntityIPrecord : ' + mapEntityIPrecord);
                
                if((CountLocation + CountSubEntity + CountEntity) < mapOrgIdIPrecord.size()) {
                    // Looping through the Organizations added in the look-up field (Detection Organization) for CS&Q Manager 
                    for(EntityStakeholder__c OrgSH : [SELECT BusinessRiskEscalationEntity__c, Role__c, User__c FROM EntityStakeholder__c WHERE (BusinessRiskEscalationEntity__c IN: mapOrgIdIPrecord.keySet() AND Role__c = 'CS&Q Manager')]) { 
                        
                        System.debug('!!!! Entered CS&Q Manager loop');
                        mapOrgIdIPrecord.get(OrgSH.BusinessRiskEscalationEntity__c).CSQ_ProjectApprover__c = OrgSH.User__c; 
                        mapOrgIdIPrecord.get(OrgSH.BusinessRiskEscalationEntity__c).CSQ_Coach__c = OrgSH.User__c; 
                    }
                    System.debug('!!!! mapOrgIdIPrecord : ' + mapOrgIdIPrecord);
                }               
            }   
        }
    }
    
    public static void checkMeasures(Map<String,CSQ_ImprovementProject__c> mapIPid1 , Map<String,CSQ_ImprovementProject__c> mapIPid2) {
        
        for(CSQ_Measure__c m : [SELECT CSQ_Current__c, CSQ_Objective__c, CSQ_ImprovementProject__c, CSQ_Actual__c FROM CSQ_Measure__c WHERE CSQ_ImprovementProject__c IN: mapIPid1.keySet() OR CSQ_ImprovementProject__c IN: mapIPid2.keySet()]) {
            if(m.CSQ_Current__c == null || m.CSQ_Objective__c == null) {
                if(!mapIPid1.isEmpty()) {
                    if(!Test.isRunningTest())
                        mapIPid1.get(m.CSQ_ImprovementProject__c).addError(Label.CL112016I2P24);
                }    
            }
            if(m.CSQ_Actual__c == null) {
                if(!mapIPid2.isEmpty()) {
                    if(!Test.isRunningTest())
                        mapIPid2.get(m.CSQ_ImprovementProject__c).addError(Label.CL112016I2P25); 
                }    
            }  
        }
    }
}