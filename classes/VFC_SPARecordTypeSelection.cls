public with sharing class VFC_SPARecordTypeSelection {
public List<SelectOption> recordTypes{get;set;}
public String selectedRecordType{get;set;}
public Map<String,String> namedescription{get;set;}{namedescription=new Map<String,String>();}

    public VFC_SPARecordTypeSelection(ApexPages.StandardController controller) {
        
    }
    public PageReference generateRecordTypeOptions()
    {    
        Map<Id,Schema.RecordTypeInfo> rtMapById= Schema.SObjectType.SPARequestLineItem__c.getRecordTypeInfosById();
        recordTypes=new List<SelectOption>();
        for(RecordType rt:[select id,Name,Description from RecordType where SobjectType='SPARequestLineItem__c']){                
        if(rtMapById.get(rt.id).isAvailable()){            
            recordTypes.add(new SelectOption(rt.id+'',rt.name));
            if(rt.description==null)
            rt.description='';        
            namedescription.put(rt.name,rt.description);
          }
        }
        if(recordTypes.size()==1){
            selectedRecordType=recordTypes[0].getValue();        
            return  choose();
        }
        else{
            return null;
        }
    }
    public PageReference choose()
    {
        if(selectedRecordType!=null &&  selectedRecordType!='')
        {
            String paramString='';
            Map<String,String> paramMap=ApexPages.CurrentPage().getParameters();
            for(String key:paramMap.keySet())
            {
                paramString+=key+'='+paramMap.get(key)+'&';                
            }
            paramString=paramString.subString(0,paramString.length()-1);
            return new PageReference('/apex/VFP_SPALineItemNew?RecordType='+selectedRecordType+'&'+paramString);
        }        
        return null;
    }

}