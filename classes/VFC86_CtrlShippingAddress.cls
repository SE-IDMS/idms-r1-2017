/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | VFC86_CtrlShippingAddress                                                          |
|                       |                                                                                    |
|     - Object(s)       | RMA__c                                                                             |
|     - Description     |   - Check if the Shipping Address is defined                                       |
|                       |   - Display Warning message                                                        |
|                       |   - Redirect to RMA page                                                           |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | April, 27th 2012                                                                   |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
public with sharing class VFC86_CtrlShippingAddress
{
   //Variables
   private final  RMA__c   currentRMA;
   public         Boolean  ShippingAddressIsValid;
  
   //Default Controller
   public VFC86_CtrlShippingAddress(ApexPages.StandardController controller)
   {
      System.Debug('## >>> BEGIN: VFC86_CtrlShippingAddress(ApexPages.StandardController controller) <<<');

      this.currentRMA = (RMA__c) controller.getRecord();

      System.Debug('## >>>>>> this.currentRMA = ' + this.currentRMA + ' <<<');
      System.Debug('## >>> END:   VFC86_CtrlShippingAddress(ApexPages.StandardController controller) <<<');
   }

   //Check Shipping Address
   public Boolean getShippingAddressIsValid()
   {
      return (this.currentRMA.ShippingCountry__c != null);
   }

    //Add warning message
    public void AddWarningMsg()
    {
       ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning, system.label.CL10205));
    }
      
    //Redirect to RMA page
    public PageReference redirectToCurrentRMA()
    {
       this.currentRMA.TECH_CheckShippingAddress__c = false;
       update(this.currentRMA);
       PageReference l_RetUrl = new PageReference('/' + this.currentRMA.Id);
       return l_RetUrl;
    }
}