/********************************************************************************************************
    Author: Fielo Team
    Date: 11/03/2015
    Description: 
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestGetFeatures/*')
global class FieloPRM_REST_GetFeatures{   

    /**
    * [getFeatures returns all features]
    * @method   getFeatures
    * @Pre-conditions  
    * @Post-conditions 
    * @return   List<String>    [list of features]
    */
    @HttpGet
    global static List<String> getFeatures(){    
        
        return FieloPRM_UTILS_Features.getFeatures();
    
    }   
}