public class Utils_Query{ 
 
    // Returns a dynamic SOQL statement for the whole object, includes only creatable fields since we will be inserting a cloned result of this query
    public static string getCreatableFieldsSOQL(String objectName, Set<id> whereClause ){
        Map<Id, User> usermap = new Map<Id, User>([Select Lastname, Firstname from User where isactive = true]);
        for(LoginHistory lh : [Select UserId, LoginTime From LoginHistory limit 50]) {
            System.debug(' Lastname: ' + usermap.get(lh.UserId).Lastname + ' Firstname: ' + usermap.get(lh.UserId).Firstname + ' LoginTime:' + lh.LoginTime);
        }
        try{ 
        String selects = 'SELECT ';                 
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selects += fd.getName() + ',';
                    //selectFields.add(fd.getName());
                }
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
        }
        selects += ' FROM ' + objectName;
        if(whereClause.size() > 0 && !whereClause.isEmpty())
        {
            Integer k = 1;
            for(string st: whereClause)
            {                
                if(K != 1)
                selects += ', \'' + st + '\'';
                else{
                selects += ' Where Id IN (\'' + st + '\'';
                }
                k++;
            }
        selects += ')';
        return selects ;
        }
        else
        return Null;            
        }
        catch(Exception ex)
        {
            System.Debug('Exception: ' + ex.getMessage());
            return null;    
        }
            
    }
}