/*
 Author: Anjali Gopinath
 Date created: 17 October 2012
*/
public class AP_Opp_UpdateParentCloseDate
{
   /**
   Oppty 1 -- 
        ** Update Parent Opportunity Close Date **
        Update/Insert an Opporrunity
        - IF current opportunity close date greater than parent opportunity close date then update the parent close date with current opportunity date.
        - IF current opportunity close date is less than any of its child opportunity close date then throw an error that opportunity date should be greater than child opportunity date. 
        
        This function is called from :
        1. Opportunity Before Insert
        2. Opportunity Before Update
    */    
    public static void updateParentOpportunity(Map<Id,Opportunity> mapOppTriggerNew)
    {                   
System.debug('*********AP_Opp_UpdateParentCloseDate***** SOQL Queries till now '+Limits.getQueries());
        //get the list of parent Opportunites
        Map<Id,List<Opportunity>> mapParentIdChildOpportunites=new Map<Id,List<Opportunity>>();
        for(Opportunity oppty:mapOppTriggerNew.values()){
        if(oppty.ParentOpportunity__c!=null){
            if(mapParentIdChildOpportunites.containsKey(oppty.ParentOpportunity__c))                            
                mapParentIdChildOpportunites.get(oppty.ParentOpportunity__c).add(oppty);          
            else
            {
                List<Opportunity> oppts=new List<Opportunity>();
                oppts.add(oppty);
                mapParentIdChildOpportunites.put(oppty.ParentOpportunity__c,oppts); 
            }
            }
        }         
        //have a map of parentId to opportunites
        
        Map<Id,Opportunity> parentOpportunites=new Map<Id,Opportunity>();        
        if(!(mapParentIdChildOpportunites.keyset().isEmpty()))
        {
        for(Opportunity oppt:[select id,CloseDate,stageName from Opportunity where id in :mapParentIdChildOpportunites.keySet()]){
            System.debug('parentOpportunites set'+oppt.id);
            parentOpportunites.put(oppt.id,oppt);        
        }
        }
        
        //query all the child opportunites for all parents available
       // System.debug('count of opportunites'+[select count() from Opportunity where ParentOpportunity__c in :mapParentIdChildOpportunites.keySet()]);
       if(!(mapParentIdChildOpportunites.keySet().isEmpty()))
       {
        for(Opportunity oppt:[select id,CloseDate,ParentOpportunity__c from Opportunity where ParentOpportunity__c in :mapParentIdChildOpportunites.keySet() limit 90000])
        {
            if(mapParentIdChildOpportunites.get(oppt.ParentOpportunity__c)==null)                
            {                
                List<Opportunity> oppts=new List<Opportunity>();
                oppts.add(oppt);
                mapParentIdChildOpportunites.put(oppt.ParentOpportunity__c,oppts);        
            }
            else
            {
                mapParentIdChildOpportunites.get(oppt.ParentOpportunity__c).add(oppt);          
            }
         }   
        }
         //List to be updated
         if(parentOpportunites.size()>0)
         {
         Map<Id,Opportunity> tobeUpdatedOpportunites=new Map<id,Opportunity>();
         //now we have a final map of parent oppty and all the children.
         for(Id parentOpptyId : mapParentIdChildOpportunites.keySet())
         {
            for(Opportunity oppty:mapParentIdChildOpportunites.get(parentOpptyId))
            {                
                // Added Stage != 7 and stage !=0 for BR-8660 - March 2016 release
                if(parentOpportunites.containsKey(parentOpptyId) && oppty.CloseDate>parentOpportunites.get(parentOpptyId).CloseDate && parentOpportunites.get(parentOpptyId).stageName!=System.Label.CL00220 && parentOpportunites.get(parentOpptyId).stageName!=System.Label.CL00272){
                    parentOpportunites.get(parentOpptyId).CloseDate=oppty.CloseDate;
                    tobeUpdatedOpportunites.put(parentOpptyId,parentOpportunites.get(parentOpptyId));
                }
            }
         }
        
        
        //
        if(tobeUpdatedOpportunites.size()>0){
        List<Opportunity> tmpListOfOpptsToUpdate=new List<Opportunity>();
        tmpListOfOpptsToUpdate.addAll(tobeUpdatedOpportunites.values());
        update tmpListOfOpptsToUpdate;    
        }
        }
    }
    
    public static void checkForChildrenWithGreaterCloseDate(Map<Id,Opportunity> mapOppTriggerNew)
    {     
        List<AggregateResult> opptyCount=[select count(id),ParentOpportunity__c from Opportunity where ParentOpportunity__c in :mapOppTriggerNew.keySet() and ParentOpportunity__c!= null and TECH_IsCloseDateGreateThanParent__c=1  group by ParentOpportunity__c having count(id)>0];
        for(AggregateResult aggresult : opptyCount)           
           if(!Test.isRunningTest())    
               mapOppTriggerNew.get(aggresult.get('ParentOpportunity__c')+'').addError(Label.CLDEC12SLS03);
                 
    }
}