/*
Author:Shruti Karn
Description:This test class is for the the apex class AP50_CreateTaskforSME
Created Date: 17-04-2012
*/

@isTest
private class AP51_CreateTaskforSME_TEST{
    static testMethod void CreateTaskforSME_TestMethod()
    {
        test.StartTest();      
        Account accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts); 
        

        //Opportunity opp= Utils_TestMethods.createOpportunity(accounts.id);
        Country__c country1 = new Country__c();
        country1.CountryCode__c = 'IN';
        insert country1;
        Opportunity opp = new Opportunity(Name='Test Opp',  AccountId=accounts.Id, StageName='0 - Closed', CloseDate=Date.today(),Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=country1.Id, IncludedInForecast__c='Yes',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R');
        Database.insert(opp);             
        
        list<OPP_QuoteLink__c> quoteLink = new list<OPP_QuoteLink__c >();
        for(integer i=1;i<=200;i++)
        {
            OPP_QuoteLink__c newQuoteLink=Utils_TestMethods.createQuoteLink(opp.id);
            quoteLink.add(newQuoteLink);
        }
        Database.insert(quoteLink);
        
        RIT_SMETeam__c newSME = new RIT_SMETeam__c();
        newSME = Utils_TestMethods.createSME(quoteLink[0].Id,'Finance');
        Database.insert(newSME);
        
        newSME.MemberActivated__c = false;
        update newSME;
        
        newSME.MemberActivated__c = true;
        update newSME;
        
        // Profile profile = [select id from profile where name='SE - Non-sales RITE Approver'];    
        // user newuser = Utils_TestMethods.createStandardUser(profile.id,'Users');
        // Database.insert(newuser );
        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        User newuser;
        System.runAs(sysadmin2){
            UserandPermissionSets newuseruserandpermissionsetrecord=new UserandPermissionSets('Users','SE - Non-sales RITE Approver');
           newuser = newuseruserandpermissionsetrecord.userrecord;
            Database.insert(newuser);

            List<PermissionSetAssignment> permissionsetassignmentlstfornewuser=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:newuseruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstfornewuser.add(new PermissionSetAssignment(AssigneeId=newuser.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstfornewuser.size()>0)
            Database.insert(permissionsetassignmentlstfornewuser);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }
        newSME.TeamMember__c = newuser.Id;
        update newSME;
        
        

    }
}