/******************************************
* Developer: Fielo Team                   *
*******************************************/
@isTest
public class FieloPRM_AP_ProductPointListTriggersTest{
    
    public static testMethod void testUnit1(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        User u = new User();
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            Country__c country = new Country__c(
                CountryCode__c = 'US',
                CurrencyIsoCode = 'USD'
            );
            insert country;
            
            PRMCountry__c countrycluster = new PRMCountry__c(
                Country__c = country.id,
                TECH_Countries__c = 'US',
                PLDatapool__c = 'asd'
            );
            insert countrycluster;
            
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test', 
                FieloEE__LastName__c = 'test',
                F_Country__c = country.Id
            );
            Insert mem;
               
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc = new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;

            FieloPRM_Invoice__c invoice = new FieloPRM_Invoice__c(
                Name = 'test',
                F_PRM_InvoiceDate__c = date.today(),
                F_PRM_Member__c = mem.Id
            );
            
            insert invoice;

            FieloPRM_LoyaltyEligibleProduct__c loyaltyEligibleProduct1 = new FieloPRM_LoyaltyEligibleProduct__c(
                F_PRM_Country__c = countrycluster.Id,
                F_PRM_PointsInstaller__c = 1                      
            );

            FieloPRM_LoyaltyEligibleProduct__c loyaltyEligibleProduct2 = new FieloPRM_LoyaltyEligibleProduct__c(
                F_PRM_Country__c = countrycluster.Id,
                F_PRM_PointsInstaller__c = 2                      
            );
            
            insert new List<FieloPRM_LoyaltyEligibleProduct__c>{loyaltyEligibleProduct1,loyaltyEligibleProduct2};
            
            FieloPRM_ProductPointList__c pointList1 = new FieloPRM_ProductPointList__c(
                F_PRM_Country__c = countrycluster.Id,
                F_PRM_StartDate__c = date.today(),
                F_PRM_EndDate__c = date.today().addDays(30)
            );
            insert pointList1;

            FieloPRM_LoyaltyEligibleProduct__c loyaltyEligibleProduct3 = new FieloPRM_LoyaltyEligibleProduct__c(
                F_PRM_Country__c = countrycluster.Id,
                F_PRM_PointsInstaller__c = 2                      
            );
            
            FieloPRM_ProductPointValue__c pointValue1 = new FieloPRM_ProductPointValue__c(
                F_PRM_IsActive__c = true,
                F_PRM_PointList__c = pointList1.Id,
                F_PRM_PtValue__c = 100,
                F_PRM_LoyaltyEligibleProduct__c = loyaltyEligibleProduct3.Id
            );
            insert pointValue1;
            
            FieloPRM_ProductPointList__c pointList2 = new FieloPRM_ProductPointList__c(
                F_PRM_Country__c = countrycluster.Id,
                F_PRM_StartDate__c = date.today().addDays(60),
                F_PRM_EndDate__c = date.today().addDays(90),
                F_PRM_PointListToClone__c = pointList1.Id
            );
            insert pointList2;            

            pointList2.F_PRM_StartDate__c = date.today().addDays(61);
            
            update pointList2;
                       
            FieloPRM_ProductPointList__c pointList3 = new FieloPRM_ProductPointList__c(
                F_PRM_Country__c = countrycluster.Id,
                F_PRM_StartDate__c = date.today(),
                F_PRM_EndDate__c = date.today().addDays(30),
                F_PRM_PointListToClone__c = pointList1.Id
            );
            
            FieloPRM_ProductPointList__c pointList4 = new FieloPRM_ProductPointList__c(
                F_PRM_Country__c = countrycluster.Id,
                F_PRM_StartDate__c = date.today().addDays(200),
                F_PRM_EndDate__c = date.today().addDays(230),
                F_PRM_PointListToClone__c = pointList1.Id
            );
                        
            try{
                insert new List<FieloPRM_ProductPointList__c>{pointList3,pointList4};
            }catch(Exception e){
            }             
            
            
            
        
        }

    }    
}