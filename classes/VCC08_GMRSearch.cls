/*
    Author          : Accenture Team
    Date Created    : 18/05/2011 
    Description     : Controller extensions for VFP25_AddUpdateProductforCase.This class fetches the search 
                      parameters from the component controller, performs the search by calling factory methods  
*/
public class VCC08_GMRSearch extends VFC_ControllerBase 
{
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/
    public sObject RelatedObject; // Defined if related standard object is a case
    public String ObjID{get;set;} // [attribute] ID of the related object
    public String searchKeyword{get; set;} // String captured by the Search Criteria input field
    public List<String> keywords; // List of words entered in the SearchKeyword variable
    public List<String> filters{get; set;} // [attribute] Contains labels for the search filter picklist
    public Utils_GMRSearchMethods Actions{get;set;}
    //public Utils_DataSource dataSource; // Source of Data to access 
    public Utils_GMRDataSource dataSource; // Source of Data to access 
    public List<DataTemplate__c> selectedRecords{get;set;} // List of selected records if the multiple selection is enabled
    public List<SelectOption> columns{get;set;} // Columns for the result : Value = API name of the displayed field, Label = Column Header
    public List<DataTemplate__c> fetchedRecords{get;set;} // Records fetched by the WS callout
    public Boolean DisplayResults{get;set;} // display the result table
    public Utils_DataSource.Result searchResult; // Result class containing data returned by the WS 
    public VFC_ControllerBase PageController{get;set;} // Controller of the page in which the component is implemented
    public String Key{get;set;} // Key of the component within the page
    public Boolean Cleared=false; // Boolean to distinguish clear from initialization
    public String ActionType{get;set;} // Label for the action in the table of displayed results
    public Boolean SearchHasBeenLaunched{get;set;} // Boolean to distinguish search from initialization
      
    // Getter and setter for the search component containing inputfield for Search Criteria and Search Filters    
    public VCC05_DisplaySearchFields searchController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC05_DisplaySearchFields displaySearchFields;
                displaySearchFields = (VCC05_DisplaySearchFields)getcomponentControllerMap().get('searchComponent');
                if(displaySearchFields!= null)
                return displaySearchFields;
            } 
            return new VCC05_DisplaySearchFields();
        }
    } 
    
    // Getter and setter for the component displaying results with pagination methods
    public VCC06_DisplaySearchResults resultsController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                system.debug('--------displaySearchResults -------'+displaySearchResults );                                     
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }
    
    /*=======================================
      CONSTRUCTORS
    =======================================*/   

    public VCC08_GMRSearch(ApexPages.StandardController controller)  
    {
         GenericInit();
         ObjID = controller.getREcord().Id;
    }
        
    public VCC08_GMRSearch()  
    {
        GenericInit();
    }    
    
    /*=======================================
      METHODS
    =======================================*/    
    
    public String getInit()
    {
        Actions.Init(ObjID,this);  
        return null;
    }

    public void GenericInit()
    {
        //dataSource = Utils_Factory.getDataSource('GMR');
        dataSource = new Utils_GMRDataSource();
        columns = dataSource.getColumns();
        fetchedRecords = new List<DataTemplate__c>();   
        SearchHasBeenLaunched = false;
        keywords = new List<String>();
        filters = new List<String>();
        resultsController = new VCC06_DisplaySearchResults();   
    }    
    
    // Refresh the result component
    public String RefreshResults()
    {
        if(getcomponentControllerMap()!=null)
                resultsController = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
              
        return null;                   
    }
    
    // Method called when "multiple" mode is selected
    public pagereference AttachMultiple()
    {
       return Actions.PerformAction(null,this); 
    }
    
    // Search method
    public void search() 
    {             
         if(resultsController.searchResults!=null)
            resultsController.searchResults.clear();
            
        fetchedRecords.clear();
        keywords = new List<String>();
        filters = new List<String>();
        if(searchController.searchText!=null)
            searchKeyword = searchController.searchText.trim();
        if(searchKeyword != null)
            keywords = searchKeyword.split('\\s');
            
        filters.add(searchController.level1);
            if(searchController.level2!=null)
        filters.add(searchController.level2);
            if(searchController.level2!=null && searchController.level3!=null)
        filters.add(searchController.level3);
            if(searchController.level2!=null && searchController.level3!=null && searchController.level4!=null)
        filters.add(searchController.level4); 
        
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords ); 
        
        SearchHasBeenLaunched = true; 
        
        if(keywords.size()!=0 || filters.size()!=0)
        {
            if(filters[0] != Label.CL00355 || keywords[0].length()>0)
            {
               try
                {
                    //Makes the webservice call for search                    
                    //Makes the webservice call for search                    
                    if(!Test.isRunningTest())
                        //searchResult = dataSource.Search(keywords,filters);
                        searchResult = dataSource.FilteredSearch(keywords,filters);
                    else{
                        Utils_DummyDataForTest testDataSource = new Utils_DummyDataForTest();
                        searchResult = testDataSource.Search(keywords,filters);
                    }
                    
                    fetchedRecords = searchResult.recordList;
                    System.debug('Data source Result'+fetchedRecords);
                       
                    DisplayResults = true;
                    if(searchResult.NumberOfRecord>=Utils_GMRDataSource.GMR_RECORD_PER_PAGE && fetchedRecords.size()>1)
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00349));

                }

                catch(Exception exc)
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00398));
                    DisplayResults = false;
                }              
            }
            else if(filters[0] == Label.CL00355 && keywords[0].length()==0)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CL00347));
                DisplayResults = false;
            }             
        }
        System.debug('#### FILTERS : searchController.searchFilter: ' + searchController.searchFilter); 
              
    }
    
    /* This method will be called from the component controller on click of "Select" link.This method correspondingly updates
     * the case.
     */
    public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
    {
       return Actions.PerformAction(obj,controllerBase);
    }
    
    /* This method will be called on click of "Clear" Button.This method clears the value in the search text box
     * and the values in picklists
     */
    public PageReference clear() 
    {   
        Cleared = true;       
        fetchedRecords.clear();
        searchKeyword = null;
        keywords.clear();
        filters.clear();            
        searchController.init();
        ResultsController.searchResults.clear();   
        DisplayResults = false;     
        return null;
    }
    
    // Cancel method
    public pagereference cancel()
    {
        return Actions.Cancel(this);  
    }
}