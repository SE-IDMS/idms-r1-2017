Public class Utils_Stubs
{
    // Order Stub
    public static WS06_OrderList.orderStatusResult OrderStub()
    {
        WS06_OrderList.orderStatusResult DummyResults = new WS06_OrderList.orderStatusResult();
        List<WS06_OrderList.orderStatusDataBean> orderList = new List<WS06_OrderList.orderStatusDataBean>();
        
        For(Integer index=0;index<15; index++)
        {
             WS06_OrderList.orderStatusDataBean DummyOrder = new WS06_OrderList.orderStatusDataBean();
             
             DummyOrder.currencyCode = 'EUR';
             DummyOrder.enteringUserName = 'Jean Dupont';
             DummyOrder.expressDelivery = true;
             DummyOrder.mySEStatus = 'delivered';
             DummyOrder.orderNumber = 'P579-X'+index;
             DummyOrder.orderPlacedThrough = 'My SE';
             DummyOrder.poNumber = 'CS - 45' + index;
             DummyOrder.releaseDate = Date.Today() ;
             DummyOrder.requestedDeliveryDate = Date.Today();
             DummyOrder.shipToName = 'Schneider Electric';
             DummyOrder.soldToName = 'Jean Dupont';
             DummyOrder.legacyname = 'TU_CRM'; 
             DummyOrder.totalAmount = index+5;      
             orderList.add(DummyOrder);
        }  
        
        DummyResults.returninfo = new WS06_OrderList.returnDataBean();
        DummyResults.returninfo.returncode = 'S';
        
        DummyResults.orderList = orderList;
        return DummyResults;
    }
    
    // Order Detail Stub
    public static WS07_OrderDetails.orderStatusDetailResult OrderDetailStub()
    {
        WS07_OrderDetails.orderStatusDetailResult DummyResult = new WS07_OrderDetails.orderStatusDetailResult();
        WS07_OrderDetails.orderStatusDataBean DummyData = new WS07_OrderDetails.orderStatusDataBean();
        List<WS07_OrderDetails.orderStatusLineDataBean> DummyDataLines = new List<WS07_OrderDetails.orderStatusLineDataBean>();
        
        For(Integer index=0;index<15; index++)
        {
            WS07_OrderDetails.orderStatusLineDataBean DummyDataLine = new WS07_OrderDetails.orderStatusLineDataBean();
            
            DummyDataLine.catalogNumber = 'DN050A' + index;
            DummyDataLine.expressDelivery= true;
            DummyDataLine.extendedProfilePrice = 500;
            DummyDataLine.invoiceDate = DateTime.now();
            DummyDataLine.invoiceNumber = 'TH891' +index;
            DummyDataLine.itemHasShipped = true;
            if(index<3 && index < 7)
               DummyDataLine.itemHasShipped = false;
            DummyDataLine.itemNumber = String.valueof(index);
            DummyDataLine.mySEItemStatus = 'delivered';
            DummyDataLine.requestedDeliveryDate = DateTime.now();
            DummyDataLine.rescheduleDate = DateTime.now();
            DummyDataLine.totalQuantityOrdered = 1;
                
            DummyDataLines.add(DummyDataLine);
        }
        
        DummyResult.returnInfo = new WS07_OrderDetails.returnDataBean();
        DummyResult.returnInfo.returnCode = 'S';
                
        DummyData.lines = DummyDataLines;
        DummyResult.orderDetail = DummyData;
        
        return DummyResult ;
    } 

    // Order Detail Stub
    public static WS08_SimplePA.priceAndAvailabilityResult PAStub()
    {
        WS08_SimplePA.priceAndAvailabilityResult PAResult = new WS08_SimplePA.priceAndAvailabilityResult();
        WS08_SimplePA.priceAndAvailabilityDataBean priceAndAvailability = new WS08_SimplePA.priceAndAvailabilityDataBean();
                
        priceAndAvailability.asOf = DateTime.now();
        priceAndAvailability.catalogNumber = 'DN050A';
        priceAndAvailability.currencyCode = 'EUR';
        priceAndAvailability.listPrice = 990;
        priceAndAvailability.priceDate = DateTime.now();
        priceAndAvailability.quantity = 10;
        
        PAResult.returninfo = new WS08_SimplePA.returnDatabean();
        PAResult.returninfo.returnCode = 'S';
        PAResult.returninfo.ReturnMessage = 'More than one SDH Cross Ref';
        
        PAResult.priceAndAvailability = priceAndAvailability;
         
        return PAResult;
    }    
    
    //Text Method
    static testMethod void testUtils_Studs() 
    {
        Utils_Stubs.OrderDetailStub(); Utils_Stubs.OrderStub();  Utils_Stubs.PAStub();
    }//End of test method   
}