/*
Created By: Renjith Jose
Release: BR-6098, Automatic assignment of program type = Channel Feature

Batch_AssignProgramChannelFeature batch_ppsa = new Batch_AssignProgramChannelFeature(); 
batch_ppsa.query = 'Select ProgramStatus__c, RecordTypeId,ProgramType__c, LastModifiedDate from Partnerprogram__c Where ProgramType__c = \'Channel Feature\' and LastModifiedDate =Last_N_Days:1';
Database.executebatch(batch_ppsa);

*/
global class Batch_AssignProgramChannelFeature implements Database.Batchable<sObject>,Schedulable  {
    
    public String query;
    public static String CRON_EXP = '0 0 20 * * ? *'; // CRON JOB expression to run the batch job at 8PM everyday
    
    global Batch_AssignProgramChannelFeature() {
        
    }
    
     
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        Map<String, CS_PRM_ApexJobSettings__c> csApexJobSeting = CS_PRM_ApexJobSettings__c.getAll(); 
        String strLastRunDate='';
        
        if(csApexJobSeting.containsKey('AssignChannelFeatureByProgram'))
        {
            if(!csApexJobSeting.get('AssignChannelFeatureByProgram').InitialSync__c)//if the initial sync is not selected
            {
                if(String.valueOf(csApexJobSeting.get('AssignChannelFeatureByProgram').LastRun__c) != null)
                {
                    DateTime dt = csApexJobSeting.get('AssignChannelFeatureByProgram').LastRun__c;
                    strLastRunDate = ' AND LastModifiedDate > ' +dt.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                }
            }   
        }
       
        if(query == null)
            query = 'SELECT Id,ProgramStatus__c, RecordTypeId,ProgramType__c, LastModifiedDate FROM PartnerProgram__c Where ProgramStatus__c = \'' + System.label.CLMAY13PRM47 + '\' AND '+
                    'RecordTypeId = \'' + System.label.CLMAY13PRM05 + '\' AND ProgramType__c = \'' + System.label.CLOCT14PRM71 + '\''+strLastRunDate ;
                    
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Partnerprogram__c> scope) {
         List<Partnerprogram__c> lstPrg = new List<Partnerprogram__c>();
               
        for(Partnerprogram__c aPg : scope)
            lstPrg.add(aPg);
        
        if(lstPrg.size() > 0)   
            AP_AAP_AccountProgramUpdate.CreateAccountAssignedProgram(null, lstPrg);
    } 
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
   /* global static String ScheduleIt (String batchCronExpression) {
        Batch_AssignProgramChannelFeature bpcf = new Batch_AssignProgramChannelFeature();
        return System.schedule('Create Account Channel Feature Program', batchCronExpression, bpcf);
    } */
    
    //Schedule method
    global void execute(SchedulableContext sc)  {
        Batch_AssignProgramChannelFeature batch_ppsa = new Batch_AssignProgramChannelFeature(); 
        Database.executebatch(batch_ppsa);
    }
}