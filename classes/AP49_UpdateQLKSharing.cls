/*
 Author: Shruti Karn
 Date created: 03 April 2012
 Description: To update the sharing on Quote Link record. Called from 
                 1. After Update Trigger on Opportunity 
                 2. After Update and After Insert Trigger on Quote Link
*/
public class AP49_UpdateQLKSharing{

      
    // find the updated Opporunity and create a map of Opportunity Owner and the list of its Quote Links    
    public static void findUpdatedOppLeaders(list<opportunity> lstNewOpp , list<opportunity> lstOldOpp)
    {
        map<Id,Id> mapOppOwnerId = new map<Id,Id>();
        for(integer i=0;i<lstNewOpp.size();i++)
        {
            if(lstNewOpp[i].OwnerId != lstOldOpp[i].OwnerId)
               mapOppOwnerId.put(lstNewOpp[i].Id, lstNewOpp[i].OwnerId);
        }
        list<OPP_QuoteLink__c> lstQLK = new list<OPP_QuoteLink__c>();
        if(!(mapOppOwnerId.keySet().isEmpty()))
            lstQLK = [Select id,OpportunityName__c from OPP_QuoteLink__c where OpportunityName__c in : mapOppOwnerId.keySet() limit 10000];
        map<Id,list<Id>> mapOppLeaderQLKID = new map<Id,list<Id>>();
        if(!lstQLK.isempty())
        {
            for(OPP_QuoteLink__c QLK : lstQLK)
            {
                if(!mapOppLeaderQLKID.containsKey(mapOppOwnerId.get(QLK.OpportunityName__c)))
                    mapOppLeaderQLKID.put(mapOppOwnerId.get(QLK.OpportunityName__c),new list<Id>{QLK.ID});
                else
                    mapOppLeaderQLKID.get(mapOppOwnerId.get(QLK.OpportunityName__c)).add(QLK.ID);
            }
        }
        
        if(!mapOppLeaderQLKID.isEmpty())
            updateQLKSharing(mapOppLeaderQLKID);
        
    }
    
    // insert the sharing record    
    static void updateQLKSharing(map<ID,list<ID>> mapOppLeaderQLKID)
    {
        list<User> lstUsers = new List<User>();
        if(!(mapOppLeaderQLKID.keySet().isEmpty()))
        lstUsers = [Select Id from User where id in : mapOppLeaderQLKID.keySet() and (ExternalUserCategory__c =:Label.CL10238 or ExternalUserCategory__c =:Label.CL10239) limit 10000];
system.debug('lstUsers :'+lstUsers );
        for(User u : lstUsers)
        {
            if(mapOppLeaderQLKID.containsKey(u.Id))
                mapOppLeaderQLKID.remove(u.Id);
        }
        list<OPP_QuoteLink__Share> lstQLKShare = new list<OPP_QuoteLink__Share>();
        for(Id oppLeader: mapOppLeaderQLKID.keySet())
        {
            for(Id QLK : mapOppLeaderQLKID.get(oppLeader))
            {
                OPP_QuoteLink__Share QLKShare = new OPP_QuoteLink__Share();
                QLKShare.ParentId = QLK ;
                QLKShare.AccessLevel = Label.CL10240;
                QLKShare.UserOrGroupId = oppLeader;
                QLKShare.RowCause =  Schema.OPP_QuoteLink__Share.RowCause.Grant_Access_To_Opportunity_Leader__c;
                lstQLKShare.add(QLKShare);
            }
        }
        if(!lstQLKShare.isEmpty())
        {
            try
            {
                insert lstQLKShare;
                if(Test.isRunningTest())
                        throw new applicationException('Ensuring exception handling');   
            }
            catch(Exception e)
            {
system.debug('Exception in inserting QuoteShare Record:'+e);
            }
            
        }
    }
    
    //Find the list of updated Quote links and create the map of Opportunity Leader and its related Quote Links
    public static void findOppLeaders(list<OPP_QuoteLink__c> lstNewQLK , list<OPP_QuoteLink__c> lstOldQLK) 
    {
        list<OPP_QuoteLink__c> lstQLK = new list<OPP_QuoteLink__c>();
        map<Id,list<ID>> mapOppQLKId = new map<ID,list<ID>>();
        map<Id,list<ID>> mapOppLeaderQLKId = new map<ID,list<ID>>();
        
        if(lstOldQLK != null)
        {
            for(integer i=0;i<lstNewQLK.size();i++)
                if(lstNewQLK[i].OpportunityName__c != lstOldQLK[i].OpportunityName__c)
                    lstQLK.add(lstNewQLK[i]);
        }
        else
        {
            lstQLK.addAll(lstNewQLK);
        }
        for(integer i=0;i<lstQLK.size();i++)
        {
           
            if(!mapOppQLKId.containsKey(lstNewQLK[i].OpportunityName__c))
                mapOppQLKId.put(lstNewQLK[i].OpportunityName__c ,new list<Id>{lstNewQLK[i].Id});
             else
                 mapOppQLKId.get(lstNewQLK[i].OpportunityName__c).add(lstNewQLK[i].Id);
 
        }
        
        if(!mapOppQLKId.isEmpty())
        {
            map<Id,Opportunity> mapIDOpp = new map<Id,Opportunity>([Select Id,ownerId from Opportunity where id in :mapOppQLKId.keySet() limit 100000] );
            for(Id OppId: mapIDOpp.keySet())
            {
                if(mapOppQLKId.containsKey(OppId))
                      mapOppLeaderQLKId.put(mapIDOpp.get(OppId).OwnerId,mapOppQLKId.get(OppId));              
            }
            
        }
        if(!mapOppLeaderQLKId.isEmpty())
            updateQLKSharing(mapOppLeaderQLKId);
    }
    
    public class applicationException extends Exception {}
    
}