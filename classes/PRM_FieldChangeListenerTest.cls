@IsTest
public class PRM_FieldChangeListenerTest {
    static Integer testSize = 1;
    static String BFO_PROPERTIES_FIELD ='BFOProperties' ;

    public static Account getBusinessAccount(String name,Integer zipCode,Id countryId,Integer accOrder){
        Account acc = new Account(name=name,ZipCode__c=''+zipCode,PRMZipCode__c=''+zipCode,City__c='Paris',PRMCity__c='Paris',Street__c='Street',Country__c=countryId,PRMCountry__c=countryId,PRMUIMSID__c='ABCD'+accOrder+zipCode);
        return acc;
    }

    public static List<Account> getAccountsInMass(Integer numOfAcc,Id countryId,Integer zipCode){
        List<Account> accList = new List<Account>();
        for(Integer i=0;i<numOfAcc;i++){
                accList.add(getBusinessAccount('Akram '+i,zipCode+i,countryId,i));
        }
        insert accList;
        System.debug('accList inserted' + accList);
        return accList;
    }

    public static Contact getContact(Account acc) {
        Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc.Id,JobTitle__c='Z3',
        CorrespLang__c='EN',WorkPhone__c='1234567890',PRMUIMSSEContactId__c = '123456',PRMOnly__c = true, PRMUIMSID__c = acc.PRMUIMSID__c,
        Email = acc.Id+'testf468a@test.com',PRMEmail__c=acc.Id+'test1f1a56@test.com',PRMContact__c=true,PRMCountry__c=acc.PRMCountry__c);
        insert c1;
        System.debug('Contact inserted ' + c1);
        return c1;
    }

    public static FieloEE__Member__c getFieloEEMember(Id accountId) {

        FieloEE__Program__c prog = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
        FieloEE__Member__c member = new FieloEE__Member__c();
        member.FieloEE__LastName__c= 'RahlIsMyLastName';
        member.FieloEE__FirstName__c = 'Marco';
        member.FieloEE__Street__c = 'test';
        member.FieloEE__Program__c = prog.Id;
        member.F_Account__c = accountId;
        insert member;
        System.debug('member inserted ' + member);
        return member;
    }

    public static Country__c getCountry() {
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
        return france;
    }

    public static List<PRMBadgeBFOProperties__c> insertPRMBadgeProperties(Integer numberOfProp,String fieldName,Integer fieldValue,String countryFieldName,Id countryId,String property,String tableName){
        //Id recordTypeId = Label.CLJUN16PRM057;
        Id recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'ExternalPropertiesCatalog__c' AND DeveloperName ='BFOProperties'].Id;
        ExternalPropertiesCatalog__c  catalog = new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,TableName__c=tableName,Type__c=BFO_PROPERTIES_FIELD,PRMCountryFieldName__c=countryFieldName,FieldName__c=fieldName,SubType__c=property,PropertyName__c='123',MinimumLengthCheck__c =0);
        insert catalog;
        System.debug('insertPRMBadgeProperties ExternalPropertiesCatalog__c ' + catalog);
        List<PRMBadgeBFOProperties__c> propList = new List<PRMBadgeBFOProperties__c>();
        /*List<String> operators = new List<String>();
        operators.add('Equals');
        operators.add('Contains');
        operators.add('Starts with');
        operators.add('Ends with');
        operators.add('Not equal');*/
        
        for(Integer i=0;i<numberOfProp;i++){
                Integer fieldValueInt = fieldValue+i;
                propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Equals',Value__c=''+fieldValueInt,PRMCountry__c=countryId,externalPropertiesCatalog__c=catalog.Id));
        }
        insert propList;
        System.debug('insertPRMBadgeProperties List<PRMBadgeBFOProperties__c> ' + propList);
        return propList;
    }

     public static testMethod void testBadge(){

        User us = Utils_TestMethods.createStandardUser('12A!');
        us.BypassVR__c=True;
        insert us;

        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;

        Country__c france = getCountry();

        insertPRMBadgeProperties(5,'PRMZipCode__c',92000,'PRMCountry__c',france.Id,'Proficient_Consulting_Engineer_ZIP_CODE_LIST','Account');
        
        PRMBadgeBFOProperties__c[] bfoProp = [select id from PRMBadgeBFOProperties__c where value__c='92000' and ExternalPropertiesCatalog__r.fieldName__c='PRMZipCode__c' and PRMCountry__c=:france.Id ];
        //System.assertEquals(1,bfoProp.size());
        
        List<Account> accList = getAccountsInMass(testSize,france.Id,92000);
        Contact con = getContact(accList[0]);
        FieloEE__Member__c member = getFieloEEMember(accList[0].Id);
        con.FieloEE__Member__c=member.Id;
        update con;
        PRM_FieldChangeListener fieldchange = new PRM_FieldChangeListener();
        fieldchange.updateMatchList(true,con,bfoProp[0],'Account');
        //fieldchange.executeActionForAccount();
        fieldchange.clearBadgeList();
        System.runAs(us){    
            test.startTest();
            List<MemberExternalProperties__c> mepList2 = [SELECT Id FROM MemberExternalProperties__c WHERE PRMUIMSId__c=:con.PRMUIMSID__c];
           // System.assertEquals(1, mepList2.size());
            List<MemberExternalProperties__c> mepList = [SELECT Id FROM MemberExternalProperties__c WHERE Member__c=:member.Id];
           // System.assertEquals(1, mepList.size());
            test.stopTest();
        }
    }


    public static testMethod void testUpdate(){
        User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c=True;
        insert us;
        System.runAs(us){    
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
            insert france;
            Integer zipCodeNonExistant = 30909;
            Integer zipCodeExistant = 90909;
            String zipCodeExistantString = ''+zipCodeExistant;
            insertPRMBadgeProperties(testSize,'PRMZipCode__c',zipCodeExistant,'PRMCountry__c',france.Id,'Proficient_Consulting_Engineer_ZIP_CODE_LIST','Account');
            List<Account> newAcc = getAccountsInMass(testSize,france.Id,zipCodeExistant);
            List<Account> oldAcc = getAccountsInMass(testSize,france.Id,zipCodeNonExistant);

            FieloEE__Member__c member = getFieloEEMember(newAcc[0].Id);
            Contact con = getContact(newAcc[0]);
            con.FieloEE__Member__c=member.Id;
            update con;

            FieloEE__Member__c member2 = getFieloEEMember(oldAcc[0].Id);
            Contact con2 = getContact(oldAcc[0]);
            con2.FieloEE__Member__c=member2.Id;
            update con2;

            test.startTest();
            List<MemberExternalProperties__c> mepList = [SELECT Id,Member__c FROM MemberExternalProperties__c WHERE Member__c=:member.Id or Member__c=:member2.Id];
           /* System.assertEquals(1, mepList.size());
            System.assertEquals(member.Id, mepList[0].Member__c);*/
            test.stopTest();
        }
        /*
        Map<PRM_FieldChangeListener.KeyName,Set<PRM_FieldChangeListener.KeyValue>> mapTest = PRM_FieldChangeListener.getRepositoyByContext(oldAcc,newAcc,PRM_FieldChangeListener.getPRMBadgeBFOPropertiesSettingsByObjectName(PRM_FieldChangeListener.ACCOUNT_OBJECT_NAME));
        
        PRM_FieldChangeListener prmChangeListener = new PRM_FieldChangeListener();
        PRM_FieldChangeListener.KeyName myKeyName = new PRM_FieldChangeListener.KeyName('PRMZipCode__c','PRMCountry__c');
        for(Integer i=0;i<newAcc.size();i++){
                prmChangeListener.registerChangeAndOneValueInRepo(myKeyName, oldAcc[i],newAcc[i], mapTest.get(myKeyName), 'Proficient_Electrical_Contractor_ZIP_CODE_LIST');
                prmChangeListener.registerChangeAndOneValueInRepo(myKeyName, oldAcc[i],newAcc[i], mapTest.get(myKeyName), 'Proficient_Consulting_Engineer_ZIP_CODE_LIST');
        }
        System.assertEquals(testSize,prmChangeListener.mapAddBadgeByAccount.size());
        System.assertEquals(0,prmChangeListener.mapRemoveBadgeByAccount.size());
        System.assertEquals(1,prmChangeListener.mapAddBadgeByAccount.get('ABCD0').size());
        */

    }

    public static testMethod void testUpdate2(){
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
       
        Integer zipCodeNonExistant = 30909;
        Integer zipCodeExistant = 90909;
        String zipCodeExistantString = ''+zipCodeExistant;
        insertPRMBadgeProperties(testSize,'PRMZipCode__c',zipCodeExistant,'PRMCountry__c',france.Id,'Proficient_Consulting_Engineer_ZIP_CODE_LIST','Account');
        PRMBadgeBFOProperties__c[] bfoProp = [select id from PRMBadgeBFOProperties__c where value__c=:zipCodeExistantString and PRMCountry__c=:france.Id ];
       //System.assertEquals(1,bfoProp.size());
       List<ExternalPropertiesCatalog__c > lToInsert = new List<ExternalPropertiesCatalog__c> ();
        Id recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'ExternalPropertiesCatalog__c' AND DeveloperName = 'BFOProperties'].Id;
        ExternalPropertiesCatalog__c  catalog = new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId ,TableName__c='Contact',Type__c=BFO_PROPERTIES_FIELD,PRMCountryFieldName__c='PRMCountry__c',FieldName__c='PRMFirstName__c',SubType__c='ValidEmail',PropertyName__c='ValidEmail',MinimumLengthCheck__c =0,Active__c = TRUE);
        lToInsert.add(catalog);
        ExternalPropertiesCatalog__c  catalog1 = new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId ,TableName__c='Account',Type__c=BFO_PROPERTIES_FIELD,PRMCountryFieldName__c='PRMCountry__c',FieldName__c='PRMUIMSID__c',SubType__c='ValidZipcode',MinimumLengthCheck__c =0,Active__c = TRUE);
        lToInsert.add(catalog1);
        /*ExternalPropertiesCatalog__c  catalog2 = new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId ,TableName__c='Opportunity',Type__c=BFO_PROPERTIES_FIELD,PropertyName__c='TestOpp',MinimumLengthCheck__c =0,FieldName__c = 'Name',Active__c = TRUE);
        lToInsert.add(catalog2);*/
        insert lToInsert;
        Map<Id,ExternalPropertiesCatalog__c> fieldsAndBadgesAndProperties = PRM_FieldChangeListener.getExternalPropertiesCatalog('Account');


        List<Account> oldAcc = getAccountsInMass(testSize,france.Id,zipCodeExistant);
        List<Account> newAcc = getAccountsInMass(testSize,france.Id,zipCodeNonExistant);
        
        PRM_FieldChangeListener.enableForAccount =true;
        PRM_FieldChangeListener.enableForContact =true;
        PRM_FieldChangeListener prmChangeListener = new PRM_FieldChangeListener();
        Map<Id, Set<PRMBadgeBFOProperties__c>> repositoryMap = prmChangeListener.getRepositoy(oldAcc,newAcc,fieldsAndBadgesAndProperties,null);
        
        for(Integer i=0;i<newAcc.size();i++){
            oldAcc[i].PRMUIMSID__c = newAcc[i].PRMUIMSID__c;
           // System.assertEquals(oldAcc[i].PRMUIMSID__c,newAcc[i].PRMUIMSID__c);
            if(fieldsAndBadgesAndProperties != null && fieldsAndBadgesAndProperties.size() > 0){
                for(Id epcId :fieldsAndBadgesAndProperties.keySet()){
                    prmChangeListener.listenFieldUpdate(fieldsAndBadgesAndProperties.get(epcId), repositoryMap.get(epcId), oldAcc[i],newAcc[i]);
                }
            }
        }
        /*System.assertEquals(0,prmChangeListener.mapAddBadgeAccount.size());
        System.assertEquals(testSize,prmChangeListener.mapRemoveBadgeAccount.size());*/
        
    }


    public static testMethod void testInsertUpdateAccount(){
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
        Integer zipCodeNonExistant = 80000;
        Integer zipCodeExistant = 92000;

        /*List<PRMBadgeBFOPropertiesSettings__c> settingList = new List<PRMBadgeBFOPropertiesSettings__c>();
        settingList.add(new PRMBadgeBFOPropertiesSettings__c(Name='test',FieldName__c='PRMZipCode__c',PRMCountryFieldName__c='PRMCountry__c',TableName__c='Account',BFOProperties__c='Proficient_Electrical_Contractor_ZIP_CODE_LIST',isActive__c=true));
        settingList.add(new PRMBadgeBFOPropertiesSettings__c(Name='test2',FieldName__c='PRMZipCode__c',PRMCountryFieldName__c='PRMCountry__c',TableName__c='Account',BFOProperties__c='Proficient_Consulting_Engineer_ZIP_CODE_LIST',isActive__c=true));
        insert settingList;*/

        insertPRMBadgeProperties(testSize,'PRMZipCode__c',zipCodeExistant,'PRMCountry__c',france.Id,'Proficient_Consulting_Engineer_ZIP_CODE_LIST','Account');
        PRMBadgeBFOProperties__c[] bfoProp = [select id from PRMBadgeBFOProperties__c where value__c='92000' and PRMCountry__c=:france.Id ];
        //System.assertEquals(1,bfoProp.size());

        Map<Id,ExternalPropertiesCatalog__c> fieldsAndBadgesAndProperties = PRM_FieldChangeListener.getExternalPropertiesCatalog('Account');


        Test.startTest();
        List<Account> oldAcc = getAccountsInMass(testSize,france.Id,zipCodeExistant);
        
        Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
        Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=oldAcc[0].Id,JobTitle__c='Z3',
            CorrespLang__c='EN',WorkPhone__c='1234567890',PRMUIMSSEContactId__c = '123456',PRMOnly__c = true, PRMUIMSID__c = france.Id+'123',
            Email = france.Id+'testf468a@test.com',PRMEmail__c=france.Id+'test1f1a56@test.com',PRMContact__c=true);
        insert c1;
        String username = c1.PRMEmail__c + '.bfo.com';
        List<User> partnerUser = [select Id from User where Username =:username ];
        if(partnerUser.size()==0){
            User userCom = new User(FirstName = c1.FirstName, FederationIdentifier=c1.PRMUIMSId__c,LastName = c1.LastName, Email = c1.PRMEmail__c, Username = username,Alias ='teOo8_', CommunityNickname = c1.Name+'8_9',ProfileId = ProfileFieloId, ContactId = c1.Id, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            insert userCom;
        }

        //Assert Badges //TODO
        //List<MemberExternalProperties__c> insertedEvent = [select id from MemberExternalProperties__c where BFOId__c in:oldAcc and Type__c=:PartnerLocatorPushEventsService.PICKLIST_BFO_PROP order by CreatedDate ];
        //System.assertEquals(testSize,insertedEvent.size());

        List<Account> newAcc = [select id,Name,PRMCountry__c,PRMZipCode__c,PRMUIMSID__c from Account where id in:oldAcc];
        Integer i=0;
        for(Account acc:newAcc){
            Integer newZip = zipCodeNonExistant+i;
            i++;
            acc.PRMZipCode__c = ''+newZip;
        }
        PRM_FieldChangeListener.enableForAccount =true;
        update newAcc;


        Contact c2 = new Contact(FirstName='Tes21t',LastName = 'Test@Name',AccountId=oldAcc[testSize-1].Id,JobTitle__c='Z3',
            CorrespLang__c='EN',WorkPhone__c='123fea4567890',PRMUIMSSEContactId__c = '12eame23456',PRMOnly__c = true, PRMUIMSID__c =  france.Id+'12!23',
            Email =  france.Id+'testffa4121Q68a@test.com',PRMEmail__c= france.Id+'test1fZA1caa56@test.com',PRMContact__c=true);
        insert c2;
        username = c2.PRMEmail__c + '.bfo.com.unittest';
        partnerUser = [select Id from User where ContactId =:c2.Id];
        if(partnerUser.size()==0){
            User userCom = new User(FirstName = c2.FirstName, FederationIdentifier=c2.PRMUIMSId__c,LastName = c2.LastName, Email = c2.PRMEmail__c, Username = username,Alias ='teOo8_', CommunityNickname = c2.Name+'8_789',ProfileId = ProfileFieloId, ContactId = c2.Id, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            insert userCom;
        }

        c2.AccountId = c1.AccountId;
        update c2;
        //insertedEvent = [select id,name,ActionCode__c from MemberExternalProperties__c where BFOId__c in:newAcc and Type__c=:PartnerLocatorPushEventsService.PICKLIST_BFO_PROP order by CreatedDate ];
        //System.assertEquals(2*testSize,insertedEvent.size());
        
        Test.stopTest();
    }
    /*
    public static testMethod void testInsertUpdateAccount2(){
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
        Integer zipCodeNonExistant = 80000;
        Integer zipCodeExistant = 92000;
        insertPRMBadgeProperties(testSize,'PRMZipCode__c',zipCodeExistant,'PRMCountry__c',france.Id,'Proficient_Consulting_Engineer_ZIP_CODE_LIST','Account');
        PRMBadgeBFOProperties__c[] bfoProp = [select id from PRMBadgeBFOProperties__c where value__c='92000' and PRMCountry__c=:france.Id ];
        System.assertEquals(1,bfoProp.size());

        Map<Id,ExternalPropertiesCatalog__c> fieldsAndBadgesAndProperties = PRM_FieldChangeListener.getExternalPropertiesCatalog('Account');


        Test.startTest();
        List<Account> oldAcc = getAccountsInMass(testSize,france.Id,zipCodeExistant);
        for(Account acc:oldAcc){
            acc.prmuimsid__c=null;
        }
        insert oldAcc;
        
        Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
        Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=oldAcc[0].Id,JobTitle__c='Z3',
            CorrespLang__c='EN',WorkPhone__c='1234567890',PRMUIMSSEContactId__c = '123456',PRMOnly__c = true, PRMUIMSID__c = '123',
            Email = 'testv4a9e498@test.com',PRMEmail__c='test1v49avveea@test.com');
        insert c1;
        
        String username = c1.PRMEmail__c + '.bfo.com';
        List<User> partnerUser = [select Id from User where Username =:username ];
        if(partnerUser.size()==0){
            User userCom = new User(FirstName = c1.FirstName, FederationIdentifier=c1.PRMUIMSId__c,LastName = c1.LastName, Email = c1.PRMEmail__c, Username = c1.PRMEmail__c + '.bfo.com',Alias ='teOo8_', CommunityNickname = c1.Name+'8_9',ProfileId = ProfileFieloId, ContactId = c1.Id, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            insert userCom;
        }
        //Assert Badges //TODO
        //List<MemberExternalProperties__c> insertedEvent = [select id,name,ActionCode__c from MemberExternalProperties__c where BFOId__c in:oldAcc and Type__c=:PartnerLocatorPushEventsService.PICKLIST_BFO_PROP order by CreatedDate ];
        //System.assertEquals(0,insertedEvent.size());
            
        List<Account> newAcc = [select id,Name,PRMCountry__c,PRMZipCode__c,PRMUIMSID__c from Account where id in:oldAcc];
        Integer i=0;
        for(Account acc:newAcc){
            i++;
            acc.prmuimsid__c = 'ABCD'+i;
        }
        update newAcc;



        //insertedEvent = [select id,name,ActionCode__c from MemberExternalProperties__c where BFOId__c in:newAcc and Type__c=:PartnerLocatorPushEventsService.PICKLIST_BFO_PROP order by CreatedDate ];
        //System.assertEquals(testSize,insertedEvent.size());
        Test.stopTest();
    }
    
    static testmethod void Test_keys() {
       PRM_FieldChangeListener.KeyName myKey = new PRM_FieldChangeListener.KeyName('PRMZipCode__c','PRMCountry__c');
       System.assertEquals('PRMZipCode__c',myKey.getKey1());
       System.assertEquals('PRMCountry__c',myKey.getKey2());
       myKey.hashCode();

       PRM_FieldChangeListener.KeyName myKey2 = new PRM_FieldChangeListener.KeyName('PRMZipCode__c','PRMCountry__c');
       System.assertEquals(true,myKey.equals(myKey2));

       PRM_FieldChangeListener.KeyName myKey3 = new PRM_FieldChangeListener.KeyName(null,'PRMCountry__c');
       PRM_FieldChangeListener.KeyName myKey4 = new PRM_FieldChangeListener.KeyName('PRMCountry__c',null);
       System.assertEquals(false,myKey3.equals(myKey4));

       PRM_FieldChangeListener.KeyValue myKeyValue = new PRM_FieldChangeListener.KeyValue('PRMZipCode__c','PRMCountry__c','Prop');
       System.assertEquals('PRMZipCode__c',myKeyValue.getValue1());
       System.assertEquals('PRMCountry__c',myKeyValue.getValue2());
       System.assertEquals('Prop',myKeyValue.getValue3());
       myKeyValue.hashCode();
       PRM_FieldChangeListener.KeyValue myKeyValue1 = new PRM_FieldChangeListener.KeyValue('PRMZipCode__c','PRMCountry__c','Prop');
       System.assertEquals(true,myKeyValue.equals(myKeyValue1));


       PRM_FieldChangeListener.KeyValue myKeyValue2 = new PRM_FieldChangeListener.KeyValue(null,'1','2');
       PRM_FieldChangeListener.KeyValue myKeyValue3 = new PRM_FieldChangeListener.KeyValue('1',null,'2');
       PRM_FieldChangeListener.KeyValue myKeyValue4 = new PRM_FieldChangeListener.KeyValue('1','2',null);
       System.assertEquals(false,myKeyValue2.equals(myKeyValue3));
       System.assertEquals(false,myKeyValue3.equals(myKeyValue2));
       System.assertEquals(false,myKeyValue2.equals(myKeyValue4));
       System.assertEquals(false,myKeyValue4.equals(myKeyValue2));
       System.assertEquals(false,myKeyValue4.equals(myKeyValue3));
       System.assertEquals(false,myKeyValue3.equals(myKeyValue4));

    }
    */

     public static testMethod void testOpportunityProperty (){

        User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c=True;
        insert us;
        System.runAs(us){    
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c france = getCountry();

            insertPRMBadgeProperties(5,'LocalOpptyName__c',75020,'PRMCountry__c',france.Id,'OPP-Prop','Opportunity');
            
            
            List<Account> accList = getAccountsInMass(testSize,france.Id,92000);
            Contact con = getContact(accList[0]);
            FieloEE__Member__c member = getFieloEEMember(accList[0].Id);
            con.FieloEE__Member__c=member.Id;
            update con;

            Opportunity opp = Utils_TestMethods.createOpportunity(accList[0].Id);
            opp.LocalOpptyName__c ='75020';
            insert opp;

            test.startTest();
            opp = [SELECT Id,LocalOpptyName__c FROM Opportunity WHERE Id=:opp.Id];
            //System.assertEquals('75020', opp.LocalOpptyName__c);
            List<MemberExternalProperties__c> mepList = [SELECT Id FROM MemberExternalProperties__c WHERE Member__c=:member.Id];
            //System.assertEquals(1, mepList.size());
            test.stopTest();
        }
    }


    public static testMethod void testOperator (){
        User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c=True;
        insert us;

        System.runAs(us){  
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c france = getCountry();

            //Id recordTypeId = Label.CLJUN16PRM057;
            Id recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'ExternalPropertiesCatalog__c' AND DeveloperName ='BFOProperties'].Id;
            ExternalPropertiesCatalog__c  catalog = new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,TableName__c='Contact',Type__c=BFO_PROPERTIES_FIELD,PRMCountryFieldName__c='PRMCountry__c',FieldName__c='PRMFirstName__c',SubType__c='ValidEmail',PropertyName__c='ValidEmail',MinimumLengthCheck__c =0);
            insert catalog;
            List<PRMBadgeBFOProperties__c> propList = new List<PRMBadgeBFOProperties__c>();
            propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Starts with',Value__c='Starts with',PRMCountry__c=france.Id,externalPropertiesCatalog__c=catalog.Id));
            propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Contains',Value__c='Contains',PRMCountry__c=france.Id,externalPropertiesCatalog__c=catalog.Id));
            propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Ends with',Value__c='Ends with',PRMCountry__c=france.Id,externalPropertiesCatalog__c=catalog.Id));
            //propList.add(new PRMBadgeBFOProperties__c(AllowRetroCalculation__c=false,Operator__c='Not equal',Value__c='Not equal',PRMCountry__c=france.Id,externalPropertiesCatalog__c=catalog.Id));
            insert propList;

            test.startTest();
            List<Account> accList = getAccountsInMass(testSize,france.Id,92000);
            Contact con = getContact(accList[0]);
            FieloEE__Member__c member = getFieloEEMember(accList[0].Id);
            con.FieloEE__Member__c=member.Id;
            update con;

            con.PRMFirstName__c='test Contains test';
            update con;
            List<MemberExternalProperties__c> mepList = [SELECT Id FROM MemberExternalProperties__c WHERE Member__c=:member.Id ];
           // System.assertEquals(1, mepList.size());

            con.PRMFirstName__c='Starts with test';
            update con;
            mepList = [SELECT Id FROM MemberExternalProperties__c WHERE Member__c=:member.Id];
           // System.assertEquals(1, mepList.size());

            con.PRMFirstName__c='not Starts with test';
            update con;
            mepList = [SELECT Id FROM MemberExternalProperties__c WHERE Member__c=:member.Id];
            //System.assertEquals(0, mepList.size());

            con.PRMFirstName__c='test Ends with';
            update con;
            mepList = [SELECT Id FROM MemberExternalProperties__c WHERE Member__c=:member.Id ];
            //System.assertEquals(1, mepList.size());

            /*con.PRMFirstName__c='test Not equal test';
            update con;
            mepList = [SELECT Id FROM MemberExternalProperties__c WHERE Member__c=:member.Id and PRMBadgeBFOProperties__r.Operator__c='Not equal'];
            System.assertEquals(1, mepList.size());*/
            test.stopTest();
        }
    }

    public static testMethod void testCheckInternalPropertiesForContacts (){
        User us = Utils_TestMethods.createStandardUser('123!');
        us.BypassVR__c=True;
        insert us;

        System.runAs(us){  
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c france = getCountry();

            insertPRMBadgeProperties(5,'PRMZipCode__c',92000,'PRMCountry__c',france.Id,'Proficient_Consulting_Engineer_ZIP_CODE_LIST','Account');
            
            PRMBadgeBFOProperties__c[] bfoProp = [select id from PRMBadgeBFOProperties__c where value__c='92000' and ExternalPropertiesCatalog__r.fieldName__c='PRMZipCode__c' and PRMCountry__c=:france.Id ];
           // System.assertEquals(1,bfoProp.size());

            PRM_FieldChangeListener.enableForAccount=true;
            PRM_FieldChangeListener.enableForContact=true;
            List<Account> accList = getAccountsInMass(testSize,france.Id,92000);
            Contact con = getContact(accList[0]);
            FieloEE__Member__c member = getFieloEEMember(accList[0].Id);
            con.FieloEE__Member__c=member.Id;
            update con;

            test.startTest();
            List<MemberExternalProperties__c> mepList = [SELECT Id FROM MemberExternalProperties__c WHERE Member__c=:member.Id];
          //  System.assertEquals(0, mepList.size());
            PRM_FieldChangeListener.enableForContact=true;
            PRM_FieldChangeListener.checkInternalPropertiesForContacts(new List<Id>{con.Id});
            mepList = [SELECT Id,DeletionFlag__c FROM MemberExternalProperties__c WHERE Member__c=:member.Id];
           // System.assertEquals(1, mepList.size());
           if(mepList.size() > 0){
            mepList[0].DeletionFlag__c=true;
            update mepList;
            }
            test.stopTest();
        }
    }
}