/*
    Author          : ACN 
    Date Created    : 4/11/2011
    Description     : Test class for VFC61_OpptyClone class 
*/
@isTest
private class VFC61_OpptyClone_TEST 
{
private static profile pro;
// private static profile prof;
private static list<User> lstUser;
private static Account acc,acc2;
private static user deladmin;
private static User sysAdmin;
private static Opportunity opp,opp1,opp2,opp3;
private static OpportunityTeamMember oppteam;
private static OpportunityTeamMember oppteam2,oppteam3;
private static opportunityShare Sharing;
private static OPP_ProductLine__c prodLine,prodLine1;
private static Contact con;
private static OPP_ValueChainPlayers__c valueCp;
private static Competitor__c competitor;
private static OPP_OpportunityCompetitor__c comp;
private static VFC61_OpptyClone opptyclone,opptyclone1,opptyclone3;

static testmethod void cloneOpptOne()
    {
        //inserts User
        
        pro = [select id from profile where name like 'system%' limit 1];
        sysAdmin= Utils_TestMethods.createStandardUser(pro.id,'sysAdmin');
        
        insert sysAdmin;
        
        // prof = [select id from profile where name ='SE - Salesperson' limit 1];
       
        
        profile profl = [select id from profile where name like '%delegated%' limit 1];
        deladmin = Utils_TestMethods.createStandardUser(profl.id,'delAdmin');
        insert deladmin;
        
        lstUser = new list<User>();
        // for(integer i=0;i<4;i++)
        // {
        //     user users =Utils_TestMethods.createStandardUser(prof.id,'user'+i);
        //     lstUser.add(users);
        // }
        // insert lstUser;
        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;        
        System.runAs(sysadmin2){
            Map<String,User> aliasnametousermap=new Map<String,User>();   
            Map<String,UserandPermissionSets> aliasnametouserpermissionsetrecordmap=new Map<String,UserandPermissionSets>();   
            for(integer i=0;i<4;i++)
            {
                UserandPermissionSets newuserandpermissionsetrecord=new UserandPermissionSets('Users'+i,'SE - Salesperson');
                user newuser = newuserandpermissionsetrecord.userrecord;
                aliasnametouserpermissionsetrecordmap.put('Users'+i,newuserandpermissionsetrecord);
                aliasnametousermap.put('Users'+i,newuser);
            }
            Database.insert(aliasnametousermap.values());     
            lstUser.addAll(aliasnametousermap.values());
            List<PermissionSetAssignment> permissionsetassignmentlstforusers=new List<PermissionSetAssignment>();    
            for(String useralias:aliasnametouserpermissionsetrecordmap.keySet())
            {            
                for(Id permissionsetId:aliasnametouserpermissionsetrecordmap.get(useralias).permissionsetandprofilegrouprecord.permissionsetIds)
                    permissionsetassignmentlstforusers.add(new PermissionSetAssignment(AssigneeId=aliasnametousermap.get(useralias).id,PermissionSetId=permissionsetId));    
            } 
            if(permissionsetassignmentlstforusers.size()>0)
                Database.insert(permissionsetassignmentlstforusers);  
        }



        //inserts Account        
        acc = Utils_TestMethods.createAccount();
        Database.insert(acc,false);    
        
        //inserts Opportunity
        opp = Utils_TestMethods.createOpportunity(acc.Id);
        opp.ownerId=lstUser[3].id;
        opp.OpptyType__c =  System.Label.CL00241;
        opp.StageName = '3 - Identify & Qualify';
        insert opp;
        
        //inserts Opportunity
        opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.ownerId=lstUser[3].id;
        opp2.OpptyType__c =  System.Label.CL00241;
        opp2.StageName = '3 - Identify & Qualify';
        insert opp2;
        
        
        opp1 = Utils_TestMethods.createOpportunity(acc.Id);
        opp1.ownerId=lstUser[3].id;
        opp1.OpptyType__c =  System.Label.CL00241;
        opp1.StageName = '3 - Identify & Qualify';
        insert opp1;
        //inserts opportunityTeamMember
        oppteam = Utils_TestMethods.createOppTeamMember(opp.id,lstUser[0].id);
        insert oppteam;    
        
        oppteam2 = Utils_TestMethods.createOppTeamMember(opp.id,lstUser[1].id);
        insert oppteam2;  
        
        Sharing = new opportunityShare();
        sharing.OpportunityId=opp.id;
        sharing.UserOrGroupId= lstUser[0].id;
        sharing.OpportunityAccessLevel = Label.CL00203;   
        insert(sharing);   
        
        //insert sales contributor
        system.runas(lstUser[3])
        {
            OPP_SalesContributor__c slscontributor = Utils_TestMethods.createSalesContr(opp.id,lstUser[3].id);
            insert slscontributor;
        }
        //Test.startTest();
        //insert productline
        prodLine = Utils_TestMethods.createProductLine(opp.id);
        insert prodLine;
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true;
        Test.startTest();
        //insert contact
         con = Utils_TestMethods.createContact(acc.id,'testcontact1234');
        //insert con;
        Database.SaveResult sr2 = Database.insert(con , dml);

        
        //insert value chain player
         valueCp = Utils_TestMethods.createValueChainPlayer(acc.id,con.id,opp.id);
        insert valuecp;
        
        //insert competitor
        competitor = Utils_TestMethods.createCompetitor();
        insert competitor;
    
        //insert opportunity competitor
         comp = Utils_TestMethods.createOppCompetitor(competitor.id,opp.id);
        insert comp;       
        
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
       
        opptyclone = new VFC61_OpptyClone(sc); 
        opptyclone.selected = true;
        opptyclone.cloneOpportunity();  
        VCC05_DisplaySearchFields searchController = new VCC05_DisplaySearchFields();
        searchController.pickListType = 'OPP_REC';
        searchController.key = 'searchComponent';
        searchController.PageController = opptyclone;        
        searchController.init();
        searchController.level1 = System.Label.CL00355;
        searchController.level2 = System.Label.CL00355;   
        Pagereference pag = opptyclone.proceedChildSelection();   
        system.assertEquals(pag,null);
        searchController.level1 = System.Label.CL00241;
        searchController.level2 = System.Label.CL00146;                  
        
        opptyclone.proceedChildSelection();     
        list<VFC61_OpptyClone.ChildRecordSelection> check =  new list<VFC61_OpptyClone.ChildRecordSelection>();    
        for(VFC61_OpptyClone.ChildRecordSelection children : opptyclone.getChildRecords())
        {                 
               children.selected = true;  
               check.add(children);
        }
        opptyclone.getChildRecords().clear();
        opptyclone.getChildRecords().addAll(check);
        
        opptyclone.proceedClone();
        
        opptyclone.continueClone();
        
        OPP_ProductLine__c PL = Utils_TestMethods.createProductLine(opp.Id);
       Pl.ProductBU__c = 'BUILDING MANAGEMENT';
       insert PL; 
       OPP_ProductLine__c PL1 = Utils_TestMethods.createProductLine(opp.Id);
       Pl1.ProductBU__c = 'BUILDING MANAGEMENT';
       Pl1.ParentLine__c= PL.Id;
       insert PL1; 
       
      // opptyclone.continueClone();
          for(VFC61_OpptyClone.ChildRecordSelection children : opptyclone.getChildRecords())
        {                 
               children.selected =true; 
               children.records='Opportunity Line';
               check.add(children);
        }
        //opptyclone.continueClone();
        Test.stopTest();
        opptyclone.proceedClone();
        
		
        sc = new ApexPages.StandardController(opp2);       
        opptyclone = new VFC61_OpptyClone(sc); 
       // opptyclone.continueClone(); //nw
        opptyclone.proceedClone();
        
        // Test.stopTest();
    }

/* 
    static testmethod void cloneOpptOne2()
	{
	test.startTest();		
	//inserts Account        
        acc2 = Utils_TestMethods.createAccount();
        Database.insert(acc2,false);    
        
        //inserts Opportunity
        opp3 = Utils_TestMethods.createOpportunity(acc2.Id);
        //opp.ownerId=lstUser[3].id;
        //opp.OpptyType__c =  System.Label.CL00241;
        //opp.StageName = '3 - Identify & Qualify';
        insert opp3;
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(opp3);
	//sc = new ApexPages.StandardController(opp2);       
        opptyclone = new VFC61_OpptyClone(sc1); 
        opptyclone.continueClone(); //nw
	Test.stopTest();
}
*/
/*
   static testMethod void cloneOpportunityTest3() 
    {
       //cloneOpportunityTest2();
     
               
        system.runas(deladmin)
        {
            opptyclone.cloneOpportunity(); 
            opptyclone.proceedChildSelection(); 
            Test.startTest();           
            opptyclone.proceedClone();
            opptyclone.continueClone();
            Test.stopTest();
        }
       
    }
    
     static testMethod void cloneOpportunityTest4() 
    {
       //cloneOpportunityTest2();
     
        system.runas(sysadmin)
        {
            for(integer i=0;i<120;i++)
            {
            if(opp.name.length()<119)
            opp.name= opp.name+'i';
            }
            database.update(opp);         
            opptyclone.cloneOpportunity(); 
            Test.startTest(); 
            opptyclone.proceedChildSelection();           
            opptyclone.proceedClone();
            opptyclone.continueClone();
            Test.stopTest();
        }   
       
    }
    
    static testMethod void cloneOpportunityTest5() 
    {
   //    cloneOpportunityTest2();
     
        system.runas(lstUser[3])
        {
            opptyclone.cloneOpportunity(); 
            opptyclone.proceedChildSelection();
            Test.startTest();           
            opptyclone.proceedClone();
            opptyclone.continueClone();
            Test.stopTest();
        }
       
    }
    
    static testMethod void cloneOpportunityTest6() 
    {
     //  cloneOpportunityTest2();
     
        system.runas(lstUser[0])
        {    
            Test.startTest();  
            opptyclone.cloneOpportunity(); 
            opptyclone.proceedChildSelection();
                     
            opptyclone.proceedClone();
            Pagereference page= opptyclone.continueClone();
            Test.stopTest();
        }
       
    }*/
       
    }