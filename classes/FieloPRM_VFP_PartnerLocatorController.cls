public class FieloPRM_VFP_PartnerLocatorController {
    public static final String DEF_VALUE_PREFIX = '##DEF##';
    public Id currentComponentId{get;set;}
  
  PartnerLocatorComponentWrapper value;
  public PartnerLocatorComponentWrapper currentComponentWithParameter{get{
    if(value==null){
    Id memberId = FieloEE.MemberUtil.getmemberId();
    Account[] acc =  [select Id,Name,PLDataPool__c,ZipCode__c from Account where Id in (Select accountId from Contact where FieloEE__Member__c=:memberId)]; 
    String zipCode;
    String dataPool;
    if(acc.size()>0){
      zipCode=acc[0].ZipCode__c;
      dataPool=acc[0].PLDataPool__c;
    }
    
    List<ComponentParameter__c> cmp =  [select BottomLabel__c,BottomLink__c, BusinessTypeValues__c, DistanceUnitValues__c, DistanceValues__c, HeaderParameter__c, LocatorParameter__c,  ShowBusinessTypeAndAreaOfFocus__c, SubmitURL__c, Subtitle__c,Title__c,BusinessTypeIncludeValues__c,BusinessTypeExcludeValues__c,AreaOfFocusIncludeValues__c,AreaOfFocusExcludeValues__c,BusinessTypeDefaultValue__c,AreaOfFocusDefaultValue__c from ComponentParameter__c where Component__c=:currentComponentId limit 1]; 
    if(cmp.size()>0){
            value = new PartnerLocatorComponentWrapper(zipCode,dataPool,cmp.get(0));
        }else{
          value = new PartnerLocatorComponentWrapper(true,'No parameters are defined for the component: '+currentComponentId);
        }
        
    }
    return value;
  }}
  public String wsURL {get;set;}


  public FieloPRM_VFP_PartnerLocatorController(){
      areaOfFocusByBusinessType = getAreaOfFocusByBusinessType();
      FieloEE__Program__c program = FieloEE.ProgramUtil.getProgramByDomain();
      wsURL = program!=null?program.FieloEE__SiteURL__c+'services/apexrest/prm':'';
    }
 
    public Map<String,List<String>> areaOfFocusByBusinessType{get;set;}

    private Map<String,List<String>> getAreaOfFocusByBusinessType(){
        //TODO Call ValidFor via api // all this code have to be replaced by Chenda
        Map<String,List<String>> myAreaOfFocusByBusinessType = new  Map<String,List<String>>();
        Schema.DescribeFieldResult buTy = Account.PLBusinessType__c.getDescribe();
        Schema.DescribeFieldResult aofo = Account.PLAreaOfFocus__c.getDescribe();
        List<Schema.PicklistEntry> ple = buTy.getPicklistValues();
         for( Schema.PicklistEntry butyPl : ple)
         {
            List<String> myAreaOfFocus = new List<String>();
            List<Schema.PicklistEntry> aofPl = buTy.getPicklistValues();
           for( Schema.PicklistEntry aofPlValue : aofPl)
           {
              myAreaOfFocus.add(aofPlValue.getValue());
           }
           myAreaOfFocusByBusinessType.put(butyPl.getValue(),myAreaOfFocus);
         }  
        //Blank mapping
        List<String> myAreaOfFocus = new List<String>();
        myAreaOfFocus.add('');
        myAreaOfFocusByBusinessType.put('',myAreaOfFocus);
        
        return myAreaOfFocusByBusinessType;
    }

    


    public List<SelectOptionWithSelection> getDistanceValues()
    {
       return fromTextAreaToSelectOption(currentComponentWithParameter.distanceValues);
    }

    public List<SelectOptionWithSelection> getDistanceUnitValues()
    {
       return fromTextAreaToSelectOption(currentComponentWithParameter.distanceUnitValues);
    }
    
    //AreaOfFocus Picklist
    public List<SelectOption> getPLAreaOfFocus()
    {
      List<SelectOption> options = new List<SelectOption>();
      //HardCoded for POC
      if(currentComponentWithParameter.title!='FieloPRM_DistributoLocatorTitle'){
        Schema.DescribeFieldResult fieldResult = Account.PLAreaOfFocus__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
         for( Schema.PicklistEntry f : ple)
         {
            options.add(new SelectOption( f.getValue(),f.getLabel()));
         }     
         doSort(options, FieldToSort.Label);
       }
       return options;
    }
    
    //PLBusinessType__c Picklist
    public List<SelectOption> getPLBusinessType()
    {
      List<SelectOption> options = new List<SelectOption>();
      Schema.DescribeFieldResult fieldResult = Account.PLBusinessType__c.getDescribe();
      List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
      for( Schema.PicklistEntry f : ple)
      {
        //Hardcoded for POC
        //if((currentComponentWithParameter.title!='FieloPRM_DistributoLocatorTitle'&& f.getValue()!='WD') || (currentComponentWithParameter.title=='FieloPRM_DistributoLocatorTitle' && f.getValue()=='WD')){
        if(currentComponentWithParameter.businessTypeIncludeValues.isEmpty() && currentComponentWithParameter.businessTypeExcludeValues.isEmpty()){
           options.add(new SelectOption(f.getValue(),f.getLabel()));
        }else{
          if(!currentComponentWithParameter.businessTypeIncludeValues.isEmpty()){
              if(currentComponentWithParameter.businessTypeIncludeValues.contains(f.getValue())){
                options.add(new SelectOption(f.getValue(),f.getLabel()));
              }
          }else{
             if(!currentComponentWithParameter.businessTypeExcludeValues.contains(f.getValue())){
                options.add(new SelectOption(f.getValue(),f.getLabel()));
              }
          }
        }
      }       
      doSort(options, FieldToSort.Label);
      return options;
    }
    
    public enum FieldToSort {
        Label, Value
    }
    
    public static void doSort(List<Selectoption> opts, FieldToSort sortField) {
        
        Map<String, Selectoption> mapping = new Map<String, Selectoption>();
        // Suffix to avoid duplicate values like same labels or values are in inbound list 
        Integer suffix = 1;
        for (Selectoption opt : opts) {
            if (sortField == FieldToSort.Label) {
                mapping.put(  // Done this cryptic to save scriptlines, if this loop executes 10000 times
                        // it would every script statement would add 1, so 3 would lead to 30000.
                       (opt.getLabel() + suffix++), // Key using Label + Suffix Counter  
                       opt);   
            } else {
                mapping.put(  
                       (opt.getValue() + suffix++), // Key using Label + Suffix Counter  
                       opt);   
            }
        }
        
        List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();
        // clear the original collection to rebuilt it
        opts.clear();
        
        for (String key : sortKeys) {
            opts.add(mapping.get(key));
        }
    }

    public static List<SelectOptionWithSelection> fromTextAreaToSelectOption(String textArea){
       List<SelectOptionWithSelection> options = new List<SelectOptionWithSelection>();
      
        if(textArea!=null){
            for(String currentValue:textArea.split('\r\n', 0)){
                if(currentValue.startsWith(DEF_VALUE_PREFIX)){
                    options.add(new SelectOptionWithSelection(currentValue.subString(DEF_VALUE_PREFIX.length()),true));
                  }else{
                    options.add(new SelectOptionWithSelection(currentValue,false));
                  }
               
            }
        }

        return options;
    }


    public static String fromTextAreaToDefaultValue(String textArea){
      if(textArea!=null){
        Integer defIndex = textArea.indexOf(DEF_VALUE_PREFIX)+DEF_VALUE_PREFIX.length();
        if(textArea.subString(defIndex).indexOf('\r\n')<0)
               return textArea.subString(defIndex);
        return textArea.subString(defIndex,textArea.indexOf('\r\n',defIndex) );
      }
      return null;
     }

     public static Set<String> fromTextAreaToSet(String textArea){
      Set<String> returnList = new Set<String>();
      if(textArea!=null){
        for(String currentValue:textArea.split('\r\n', 0)){
          returnList.add(currentValue);
        }
      }
      return returnList;
     }

    
  
  public class PartnerLocatorComponentWrapper{
    public String dataPool{get;set;}
    public String zipCode{get;set;}
    public String title {get;set;}
    public String subTitle {get;set;}
    public Boolean showBuTyAoFo{get;set;}
    public String url{get;set;}
    public String header{get;set;}
    public String locator{get;set;}
    public String bottomTextValue{get;set;}
    public String bottomLink{get;set;}
    public Boolean hasError{get;set;}
    public String errorMessage{get;set;}
    public String distanceValues{get;set;}
    public String defaultDistanceValue{get;set;}
    public String distanceUnitValues{get;set;}
    public String defaultDistanceUnit{get;set;}
    public Set<String> areaOfFocusIncludeValues{get;set;}
    public Set<String> areaOfFocusExcludeValues{get;set;}
    public Set<String> businessTypeIncludeValues{get;set;}
    public Set<String> businessTypeExcludeValues{get;set;}
    public String businessTypeDefaultValue {get;set;}
    public String areaOfFocusDefaultValue {get;set;}

    
    public PartnerLocatorComponentWrapper(String zipCode,String dataPool,ComponentParameter__c componentParams){
      this.zipCode = zipCode;
      this.dataPool = dataPool;

      this.title = componentParams.Title__c;
      this.subTitle = componentParams.Subtitle__c;
      this.showBuTyAoFo =componentParams.ShowBusinessTypeAndAreaOfFocus__c;
      this.url = componentParams.SubmitURL__c;
      this.header = componentParams.HeaderParameter__c;
      this.locator = componentParams.LocatorParameter__c;
      this.bottomTextValue = componentParams.BottomLabel__c;
      this.bottomLink = componentParams.BottomLink__c;
      this.distanceValues = componentParams.DistanceValues__c;
      this.distanceUnitValues = componentParams.DistanceUnitValues__c;
      this.defaultDistanceValue =FieloPRM_VFP_PartnerLocatorController.fromTextAreaToDefaultValue(this.distanceValues);
      this.defaultDistanceUnit =FieloPRM_VFP_PartnerLocatorController.fromTextAreaToDefaultValue(this.distanceUnitValues);
      this.areaOfFocusIncludeValues = fromTextAreaToSet(componentParams.AreaOfFocusIncludeValues__c);
      this.areaOfFocusExcludeValues = fromTextAreaToSet(componentParams.AreaOfFocusExcludeValues__c);
      this.businessTypeIncludeValues = fromTextAreaToSet(componentParams.BusinessTypeIncludeValues__c);
      this.businessTypeExcludeValues = fromTextAreaToSet(componentParams.BusinessTypeExcludeValues__c);
      this.businessTypeDefaultValue = componentParams.BusinessTypeDefaultValue__c;
      this.areaOfFocusDefaultValue = componentParams.AreaOfFocusDefaultValue__c;

    }

    public PartnerLocatorComponentWrapper(Boolean hasError,String errorMessage){
        this.hasError =    hasError;
        this.errorMessage=errorMessage;
    }


    
  }

  public class SelectOptionWithSelection{
    public String value {get;set;}
    public Boolean selected {get;set;}

    public SelectOptionWithSelection(String value, Boolean selected){
      this.value=value;
      this.selected = selected;
    }
  }
}