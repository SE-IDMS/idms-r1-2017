@isTest
private class VFC_AddUpdateProductsForOpportunity_TEST {
    static Account account1;
    static oppAgreement__c objAgree2;
    static
    {
        account1 = Utils_TestMethods.createAccount();
        insert account1;
        objAgree2 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),account1.Id );         
            objAgree2.PhaseSalesStage__c = '3 - Identify & Qualify';   
            objAgree2.AgreementValidFrom__c = Date.today();         
            objAgree2.FrameOpportunityInterval__c = 'Monthly';
            objAgree2.SignatureDate__c = Date.today();
            objAgree2.Agreement_Duration_in_month__c = '3';
            insert objAgree2;
    }
    static testMethod void testVFC_AddUpdateProductsForOpportunity() { 
           
           Opportunity opportunity1 = Utils_TestMethods.createOpportunity(account1.Id);
           insert opportunity1;
           OPP_ProductLine__c ProductLine1 = Utils_TestMethods.createProductLine(opportunity1.Id);
           ProductLine1.TECH_CommercialReference__c= 'Commercial\\sReference\\stest';
           ProductLine1.ProductBU__c = 'Product BU';
           insert ProductLine1;
           OPP_Product__c testProduct = Utils_TestMethods.createProduct('Test','Test','Test','Test');
           testProduct.TECH_PM0CodeInGMR__c ='123456789';
           insert testProduct;
           CS_CurrencyDecimalSeperator__c cd= new CS_CurrencyDecimalSeperator__c(GroupingSeperator__c=',',DecimalSeperator__c='.',Name='en_US');
           insert cd;
           ApexPages.StandardSetController setCon;
           VFC_AddUpdateProductsForOpportunity obj=new VFC_AddUpdateProductsForOpportunity(setCon);
           obj.searchText='test';
           VFC_AddUpdateProductsForOpportunity.retrieveCurrencyInformation(opportunity1.id);
           String jsonSelectString='[{"gmrcode":null,"jsonkey":null,"unitprice":"1K","quantity":"1"}]';
           String jsonSelectString1='[{"gmrcode":"123456789","jsonkey":null,"unitprice":"1M","quantity":"1"}]'; 
           String jsonSelectString2='[{"gmrcode":"123456789","jsonkey":null,"unitprice":"1B","quantity":"1"}]';
           VFC_AddUpdateProductsForOpportunity.performSave(jsonSelectString,ProductLine1.id,'Change');
           VFC_AddUpdateProductsForOpportunity.performSave(jsonSelectString,ProductLine1.id,'donotChange');
           VFC_AddUpdateProductsForOpportunity.performSave(jsonSelectString,ProductLine1.id,'addProdFromAgreement');
           obj.cancel();
           
           Test.startTest();
           Test.setMock(WebServiceMock.class, new WS_MOCK_GMRSearch_TEST());
           VFC_AddUpdateProductsForOpportunity.remoteSearch('123456789','abc123',True,True,True);
           VFC_AddUpdateProductsForOpportunity.performSave(jsonSelectString1,opportunity1.id,'noChange');           
           //VFC_AddUpdateProductsForOpportunity.remoteSearch('123456789','ab',True,True,True);
           VFC_AddUpdateProductsForOpportunity.getNextLevel('test');
           //VFC_AddUpdateProductsForOpportunity.remoteSearch('123456789','abcd',True,True,True);
           //VFC_AddUpdateProductsForOpportunity.remoteSearch('123456789','abcd1234',True,True,True);
           VFC_AddUpdateProductsForOpportunity.getRecordDetails(ProductLine1.id);
           CS_CurrencyDecimalSeperator__c cd1= new CS_CurrencyDecimalSeperator__c(GroupingSeperator__c='.',DecimalSeperator__c=',',Name=UserInfo.getLocale());
           insert cd1;
           VFC_AddUpdateProductsForOpportunity.performSave(jsonSelectString1,ProductLine1.id,'Change');
           VFC_AddUpdateProductsForOpportunity.performSave(jsonSelectString2,ProductLine1.id,'donotChange');
           Test.stopTest();
    } 
    static testMethod void testGMRSearch()
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new WS_MOCK_GMRSearch_TEST());
        VFC_AddUpdateProductsForOpportunity.remoteSearch('12345','ab',True,True,True);
        Test.stopTest();
    }
    static testMethod void testGMRSearch1()
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new WS_MOCK_GMRSearch_TEST());
        VFC_AddUpdateProductsForOpportunity.remoteSearch('12345','abcd',True,True,True);
        Test.stopTest();
    }
    static testMethod void testGMRSearch2()
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new WS_MOCK_GMRSearch_TEST());
        VFC_AddUpdateProductsForOpportunity.remoteSearch('12345','abcd1234',True,True,True);
        Test.stopTest();
    }
    static testMethod void testGMRSearch3()
    {
        Test.startTest();
        PageReference pageRef = Page.VFP_AddUpdateProductsForOpportunity;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('agreementId',objAgree2.Id);
        ApexPages.currentPage().getParameters().put('mode','addProdFromAgreement');
        ApexPages.StandardSetController setCon;
        VFC_AddUpdateProductsForOpportunity obj=new VFC_AddUpdateProductsForOpportunity(setCon);
        obj.cancel();
        
        pageRef = Page.VFP_AddUpdateProductsForOpportunity;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('agreementId',objAgree2.Id);
        ApexPages.currentPage().getParameters().put('mode','change');
        obj=new VFC_AddUpdateProductsForOpportunity(setCon);
        obj.cancel();
        
        Test.stopTest();
    }
}