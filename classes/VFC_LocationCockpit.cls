public with sharing class VFC_LocationCockpit {

    
    private ID locationId;
    public String mData{get;set;}
    //public List<String> ipData{get;set;}
    public Boolean fCompliancy{get;set;}
    public List<dobbleWrapper>wOutput{get;set;}{wOutput=new List<dobbleWrapper>();}
    public Map<Id,List<String>> iplst{get;set;}
    public Map<Id,List<SVMXC__Installed_Product__c>> iplListMap{get;set;}
    public VFC_LocationCockpit(ApexPages.StandardController controller){
        locationId=controller.getId();
        cockpit_hierarchy();
        
    }
    public Map<Id,String> parentLocationIdSet {get;set;}
    public string profileImageUrl    { get; set; }
        public string updopptyId {get;set;}
    public boolean updopptyaction {get;set;}
    public string str_updopptyaction {get{return updopptyaction?'Yes':'No';}}
    public boolean SB_isok {get;set;}{SB_isok=true;}

    
    private void cockpit_hierarchy(){
        Boolean fTopIsOn=false;
        Integer fCount=0;
        parentLocationIdSet=new Map<Id,String>();
        //profileImageUrl=new Map<Id,String>();
        mData='data.addRows([';
        parentLocationIdSet.put(locationId,null);
        List<Integer>IntegerListToNullify=new List<Integer>();
        List<String> ListStringTemp1=new List<String>();
        List<String> ListStringTemp2=new List<String>();
        Integer secureBarrier=0;
        Integer i,iTemp;
        Integer IntegerRowLine=0;
        String lastSentenceJS='';
        List<SVMXC__Site__c> locationList;
        try{
            // Added for April Release 2014 , DEF - 4266 Started
            //locationList=[SELECT Id,SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__c , (SELECT Id FROM SVMXC__Locations__r) FROM SVMXC__Site__c WHERE Id=:locationId];
            locationList=[SELECT Id,SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__c ,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c, (SELECT Id FROM SVMXC__Locations__r) FROM SVMXC__Site__c WHERE Id=:locationId];
            if(locationList.size()==1){
                if(locationList[0].Id==locationList[0].SVMXC__Parent__c||locationList[0].Id==locationList[0].SVMXC__Parent__r.SVMXC__Parent__c)throw new BException(System.Label.CL00120);
                SVMXC__Site__c  myServiceCont=locationList[0];
                parentLocationIdSet.put(myServiceCont.id,null);
                if(myServiceCont.SVMXC__Parent__c!=null){
                    parentLocationIdSet.put(myServiceCont.SVMXC__Parent__c,null);
                    if(myServiceCont.SVMXC__Parent__r.SVMXC__Parent__c!=null)parentLocationIdSet.put(myServiceCont.SVMXC__Parent__r.SVMXC__Parent__c,null);
                        if(myServiceCont.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c != null )parentLocationIdSet.put(myServiceCont.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,null);
                             if(myServiceCont.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c != null )parentLocationIdSet.put(myServiceCont.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,null);
                                    if(myServiceCont.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c != null )parentLocationIdSet.put(myServiceCont.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__r.SVMXC__Parent__c,null);
                        
                      // Added for April Release 2014 , DEF - 4266 End
                        
                }
                for(SVMXC__Site__c  o:myServiceCont.SVMXC__Locations__r)parentLocationIdSet.put(o.Id,null);
            }
            for(SVMXC__Site__c   o:[SELECT SVMXC__Parent__c, Id FROM SVMXC__Site__c  WHERE SVMXC__Parent__c IN :parentLocationIdSet.keySet()])parentLocationIdSet.put(o.Id,null);
            if(locationList[0].SVMXC__Parent__r!=null)for(SVMXC__Site__c  o:[SELECT Id FROM SVMXC__Site__c  WHERE SVMXC__Parent__c IN :parentLocationIdSet.keySet()])parentLocationIdSet.put(o.Id,null);
            List<SVMXC__Site__c> SerConListForLoop=[Select SVMXC__Parent__c,id, SVMXC__Account__c ,SVMXC__Account__r.Name,Name ,OwnerId,owner.name,RecordTypeId,StateProvince__c,StateProvince__r.name,SVMXC__City__c,SVMXC__Country__c,LocationCountry__c,LocationCountry__r.name,SVMXC__State__c,SVMXC__Street__c,SVMXC__Zip__c,(select Id,name,SVMXC__Serial_Lot_Number__c from R00N70000001hzcqEAA__r )  FROM SVMXC__Site__c WHERE Id IN :parentLocationIdSet.keyset()];
            Map<id,user> uidphoturlmap = new Map<id,user>();
            Set<id> owneridset = new Set<id>();
            iplst = new Map<Id,List<String>>();
            iplListMap =  new Map<Id,List<SVMXC__Installed_Product__c>>();
            for(SVMXC__Site__c sc:SerConListForLoop){
                owneridset.add(sc.ownerid);
                iplst.put(sc.Id,new List<String>{});
                iplListMap.put(sc.Id,new List<SVMXC__Installed_Product__c>{});
                Integer count=0;
                for(SVMXC__Installed_Product__c ip: sc.R00N70000001hzcqEAA__r)
                    {
                        iplst.get(sc.Id).add(ip.Name);
                        // iplst.get(sc.Id).add(ip.SVMXC__Serial_Lot_Number__c);
                        if(count<10){
                            iplListMap.get(sc.Id).add(ip);
                        }
                        count ++;
                   }
            }
            system.debug('aaa=='+iplst);
            List<user> userList=[select SmallPhotoUrl from user where id in: owneridset];
            uidphoturlmap.putAll(userList);
            
            ListStringTemp1.add(null);
            secureBarrier=0;
            fCompliancy=true;
            while(secureBarrier<5000){
                i=0;
                for(SVMXC__Site__c o:SerConListForLoop){
                    //profileImageUrl =[select FullPhotoUrl from User where Id =o.OwnerId];  <div class="tooltip"><table width="100" border="1">'+IPTooltip(o.Id)+'</table></div>
                        for(String s:ListStringTemp1)if(o.SVMXC__Parent__c==s){
                        Boolean bIncludedInForecast = true;
                        system.debug('oId=='+o.Id);
                        mData+='[{v:\''+o.Id+'\',f:\'<div class="HOppty"><a title="'+System.Label.CLMAY13SRV38+'" href="/'+o.Id+'">'+esc(o.Name)+'</a></div><div class="HAccount"><a title="'+System.Label.CL00122+'" href="/'+o.SVMXC__Account__c+'">'+esc(o.SVMXC__Account__r.Name)+'</a></div>';
                        /*
                          +(o.SVMXC__Street__c!=null?'<div class="HStreet">Street: '+o.SVMXC__Street__c+'</div>':'<div></div>')+(o.SVMXC__City__c!=null?'<div class="HCity">City: '+o.SVMXC__City__c+'</div>':'<div></div>')+(o.SVMXC__Zip__c!=null?'<div class="Hzip">Zip: '+o.SVMXC__Zip__c+'</div>':'<div></div>')+(o.StateProvince__c!=null?'<div class="HStateProvince">State Province: '+o.StateProvince__r.name+'</div>':'<div></div>')+
                        */
                         mData+='<div class="HIP" id="'+o.Id+'" name="'+o.Id+'" onmouseover="openPopUp()" onMouseOut="closePopUp()"><a title="IP" href="#">'+iplst.get(o.Id).size()+' Installed Product(s)</a>'+(iplst.get(o.Id).size()>0?'<div class="tooltip"><table border="1"> <thead><tr><th><b>Installed Product Name</b></th><th><b>Serial/Lot Number</b></th></tr></thead>'+IPTooltip(o.Id)+'</table></div>':'<div></div>')+'</div><div class="HOwner"><a title="'+System.Label.CLMAY13SRV08+'" href="/'+o.OwnerId+'">'+esc(o.owner.name)+'</a><img src="'+uidphoturlmap.get(o.ownerid).SmallPhotoUrl+'" class="HPic"/></div><div class="CockpitFollow" id="follow1'+o.id+'"></div>\'},\''+(o.SVMXC__Parent__c!=null?o.SVMXC__Parent__c:o.Id)+'\',\''+esc(o.Name)+'\'],';
                        
                        lastSentenceJS+=bIncludedInForecast?'data.setRowProperty('+IntegerRowLine+',\'style\',\'border:5px solid green;\');':'data.setRowProperty('+IntegerRowLine+',\'style\',\'border:3px solid #888888;\');';
                        //if(o.TobeDeleted__c)lastSentenceJS+='data.setRowProperty('+IntegerRowLine+',\'style\',\'border: 3px solid red;\');';
                        ListStringTemp2.add(o.Id);
                        IntegerListToNullify.add(i);
                        IntegerRowLine++;
                    }
                    i++;
                }
                for(Integer j=IntegerListToNullify.size()-1;j>=0;j--)SerConListForLoop.remove(IntegerListToNullify.get(j));
                if(SerConListForLoop.isEmpty())break;
                ListStringTemp1=ListStringTemp2.clone();
                ListStringTemp2.clear();
                IntegerListToNullify.clear();
                secureBarrier++;
            }
            mData=mData.substring(0,mData.length()-1)+']);'+lastSentenceJS;
        }
        catch(Exception exc){ApexPages.addMessages(exc);}
    }
    
    public String IPTooltip(Id lId)
    {
        String ipData='';
        if(iplListMap.size()>0 && iplListMap.containsKey(lId))
            for(SVMXC__Installed_Product__c a: iplListMap.get(lId))
            { 
                if(ipData == null)
                {
                    if(a.SVMXC__Serial_Lot_Number__c != null)
                    {
                      ipData = '<tr><td>'+a.Name+'</td><td>'+a.SVMXC__Serial_Lot_Number__c+'</td></tr>';
                    }
                      else{ipData = '<tr><td>'+a.Name+'</td><td></td></tr>';}
                }               
                else
                    {
                        if(a.SVMXC__Serial_Lot_Number__c != null)
                        {
                        ipData += '<tr><td>'+a.Name+'</td><td>'+a.SVMXC__Serial_Lot_Number__c+'</td></tr>';
                        }
                        else{ipData += '<tr><td>'+a.Name+'</td><td></td></tr>';}
                    }
            }
           return ipData;
    }
    
    private string esc(string s){return s!=null?String.escapeSingleQuotes(s):null;}
    public class wrapperFeed{
        public String myPic{get;set;}
        public String myText{get; set;}
        public String myWhere_Name{get; set;}
        public String myWhere_Id{get; set;}
        public String myWhen{get;set;}
        public String myWho_Id{get;set;}
        public String myWho_Name{get;set;}
        public List<wrapperFeed>myComments {get;set;}
    }
    public class dobbleWrapper{
        public dobbleWrapper(List<wrapperFeed> w,String c){myWrapper=w;myClass=c;}
        public List<wrapperFeed> myWrapper{get; set;}
        public String myClass{get; set;}
    }
   
    public class BException extends Exception {} 

    

}