@isTest(seealldata=true)
public class SVMXC_WorkOrderNoteBefore_Test {
    
    static testMethod void testbefore() {
        
        SVMXC__Service_Order__c wo= new SVMXC__Service_Order__c(CheckedIn__c=true,CheckedbyGMR__c=true);
        insert wo;
        
        WorkOrderNote__c won= new WorkOrderNote__c(Notes__c='testrec',FSRUser__c=null,WorkOrder__c=wo.id);
        insert won;
    }

}