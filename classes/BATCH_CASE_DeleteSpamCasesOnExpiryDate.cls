/*
Author          : Rakhi Kumar - Global Delivery Team
Date Created    : 13/02/2013
Description     : May 2013 Release - BR-2175. Delete spam cases on expiry date
-----------------------------------------------------------------
                          MODIFICATION HISTORY
-----------------------------------------------------------------
Modified By     : Vimal K
Modified Date   : 23/11/2015
Description     : Oct 15 Release - BR-7545 Delete Orphan Cases on Expiry Date
-----------------------------------------------------------------
*/
global class BATCH_CASE_DeleteSpamCasesOnExpiryDate implements Database.Batchable<sObject>{
    global final Integer spamExpiryPeriod;
    global final Date dateMarkedAsSpam;

    global final Integer intOrphanExpiryPeriod;
    global final Date dateMarkedAsOrphan;

    private static final  String strOrphanQueueId   = Label.CLOCT15CCC12;
    
    global BATCH_CASE_DeleteSpamCasesOnExpiryDate(){
        spamExpiryPeriod = Integer.ValueOf(System.Label.CLMAY13CCC01);//Retrieve expiry period for spam cases
        dateMarkedAsSpam = System.Today().addDays(-spamExpiryPeriod );//Reverse calculate the date marked as spam(with reference to current date) based on the expiry period.
        
        // BR-7545
        intOrphanExpiryPeriod = Integer.ValueOf(System.Label.CLOCT15CCC13);//Retrieve expiry period for Orphan Cases
        dateMarkedAsOrphan = System.Today().addDays(-intOrphanExpiryPeriod );//Reverse calculate the date marked as Orphan(with reference to current date) based on the expiry period.

    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(' SELECT id FROM Case WHERE (TECH_DateMarkedAsSpam__c <=: dateMarkedAsSpam and Spam__c = true) OR (OwnerId =:strOrphanQueueId AND TECH_DateMarkedAsOrphan__c != null AND TECH_DateMarkedAsOrphan__c <=: dateMarkedAsOrphan)');
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        try {
        
            if(scope != null && scope.size()>0)
            {
                Database.DeleteResult[] results = Database.Delete(scope, false);
                if (results != null){
                    for (Database.DeleteResult result : results) {
                        if (!result.isSuccess()) {
                            Database.Error[] errs = result.getErrors();
                            for(Database.Error err : errs)
                                System.debug(err.getStatusCode() + ' - ' + err.getMessage());
                        }
                    }
                }
            }
        
        }
        catch (Exception e) {
            System.debug(e.getTypeName() + ' - ' + e.getCause() + ': ' + e.getMessage());
        }   
    }
    
    global void finish(Database.BatchableContext BC){
    }
}