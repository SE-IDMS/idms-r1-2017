@isTest
public class POMPUrlRewriter_Test{
    @testSetup static void testSetupPOMPUrlRerit(){
		CS_PRM_POMPUrlMapping__c POMPUrlMap = new CS_PRM_POMPUrlMapping__c(Name = '/NewOpportunityRegistration', Page__c = '/VFP_EnhancedOpportunityRegistration');
		insert POMPUrlMap;
    }
	static testMethod void testGenerateUrlFor() {
        POMPUrlRewriter rewriter = new POMPUrlRewriter();
		List<PageReference> GenerateUrlForList = new List<PageReference>();
		GenerateUrlForList.add(new PageReference('/VFP_EnhancedOpportunityRegistration'));
		rewriter.generateUrlFor(GenerateUrlForList);
    }
	static testMethod void testMapUrl() {
        POMPUrlRewriter rewriter = new POMPUrlRewriter();
		rewriter.mapRequestUrl(new PageReference('/NewOpportunityRegistration')).getUrl();
    }
	static testMethod void testMapNonVipUrl() {
        POMPUrlRewriter rewriter = new POMPUrlRewriter();
		rewriter.mapRequestUrl(new PageReference('/testUrl'));
    }
}