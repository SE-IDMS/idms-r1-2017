/*
Author:Siddharth
Purpose:Test Methods for the class VFC_MassEditSPALineItems
Methods:    Constructor,
            Private void setApprovers (List<SPALineItem> lstSPALineItems)
            Private boolean checkMinimumDealSizeMet ()
            private integer getThresholdLevelForProductLine (List<SPALineItem>lstSPALineItems)
            private void  SetPricingDeskMembers ()
            private void shareSPARecord()
            public List<User> lstApproversForSharing
            

*/
@isTest
private class VFC_MassEditSPALineItems_TEST
{

    static testMethod void runPositiveTestCases() {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;

        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
        
          //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
     
        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.TECH_ApprovalStep__c=1;
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount');
        insert lineitem;
        
        insert Utils_SPA.createPricingDeskMembers(sparequest.BackOfficeSystemID__c,lineitem.LocalPricingGroup__c);
        lineitem.ProductLineManager__c=UserInfo.getUserId();
        update lineitem;
        
        List<SPARequestLineItem__c> lineitemlist = new List<SPARequestLineItem__c>();
        ApexPages.StandardSetController controller= new ApexPages.StandardSetController(lineitemlist);            
        VFC_MassEditSPALineItems controllerinstance=new VFC_MassEditSPALineItems(controller);    
        
        ApexPages.currentPage().getParameters().put('id',sparequest.id);     
        controllerinstance=new VFC_MassEditSPALineItems(controller);    
        
        controllerinstance.lstWrapper[0].newcomments='test comments';
        controllerinstance.saveAll();
        
        controllerinstance.selectedoption='mylocalpricinggroup';
        controllerinstance.populateWrapper();
        controllerinstance.lstWrapper[0].lineitem.addError('some random error');
        controllerinstance.saveAll();
        
        controllerinstance.getPickValues(lineitem,'RecommendationStatus__c','None');

    }
  }