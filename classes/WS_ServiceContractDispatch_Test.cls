@isTest
public class WS_ServiceContractDispatch_Test {

@testSetup static void SetupMethodAccountcreation() {

  User user = new User(alias = 'user', email='user' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'InstalledProductBeforeupdate;SVMX10;AP30;SVMX_InstalledProduct2;SVMX22;AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                             timezonesidkey='Europe/London', username='InterfaceTestuser' + '@bridge-fo.com',FederationIdentifier = '12345',Company_Postal_Code__c='12345',Company_Phone_Number__c='9986995000');       
       insert user;
       system.runAs(user)
       {
       
       Country__c testCountryUS = new Country__c();
            testCountryUS.CountryCode__c = 'IT';
            testCountryUS.Name = 'US';
            insert testCountryUS;

Account testAccountUSNJ = new Account();
            testAccountUSNJ.Name='TestAccountUSNJ';
            testAccountUSNJ.Street__c='New Street';
            testAccountUSNJ.POBox__c='XXX';
            testAccountUSNJ.OwnerId = '00512000005c05G';//us.id;
            testAccountUSNJ.MarketSubSegment__c='B43';       
            testAccountUSNJ.Country__c=testCountryUS.id;
            //testAccountUSNJ.StateProvince__c=null;
            testAccountUSNJ.LeadingBusiness__c = 'EN';
            testAccountUSNJ.type = 'SA';
            testAccountUSNJ.PrimaryRelationshipLeader__c = 'Field Service';
            insert testAccountUSNJ; 
            }
    }
    static testMethod void myUnitTest1() {
    
     
        
            
       
            WS_ServiceContractDispatch.WS_bulkServiceContract bscd = new WS_ServiceContractDispatch.WS_bulkServiceContract();
            WS_ServiceContractDispatch.WS_UnitServiceContract wsc = getServiceContract();
            Account acc = [select id,name from account where name='TestAccountUSNJ' limit 1];
            
            SVMXC__Service_Contract__c Contract = new SVMXC__Service_Contract__c();
            Contract.SVMXC__Company__c = acc.Id;
            Contract.SoldtoAccount__c=acc.Id;
            Contract.Name = 'Test Contract';              
            Contract.SVMXC__Start_Date__c = system.today();
            Contract.SVMXC__End_Date__c= system.today() + 10;
            Contract.SVMXC__Renewed_From__c = null;            
            Contract.LeadingBusinessBU__c ='BD';
            Contract.DefaultInstalledAtAccount__c=acc.Id;
            Contract.Tech_ContractCancel__c = true; 
            Contract.SVMXC__Active__c   = true;
            Contract.BackOfficeReference__c = 'TestBOfficeRef-1';           
            Contract.BackOfficeSystem__c = 'ITB_ORA';
            insert Contract;
            SVMXC__Service_Contract__c Contract2 = new SVMXC__Service_Contract__c();
            Contract2.SVMXC__Company__c = acc.Id;
            Contract2.SoldtoAccount__c=acc.Id;
            Contract2.Name = 'Test Contract 2 ';              
            Contract2.SVMXC__Start_Date__c = system.today();
            Contract2.SVMXC__End_Date__c= system.today() + 10;
            Contract2.SVMXC__Renewed_From__c = null;            
            Contract2.LeadingBusinessBU__c ='BD';
            Contract2.DefaultInstalledAtAccount__c=acc.Id;
            Contract2.Tech_ContractCancel__c = true; 
            Contract2.SVMXC__Active__c   = true;
            Contract2.BackOfficeReference__c = 'TestBOfficeRef-2';          
            Contract2.BackOfficeSystem__c = 'ITB_ORA';
            insert Contract2;
            wsc.ShiptoAccount = acc.id;
            wsc.SoldtoAccount = acc.id;
            wsc.BackOfficeReference = 'TestBOfficeRef-3';
            WS_ServiceContractDispatch.WS_UnitServiceContract wsc4 = wsc.clone();
            wsc4.BackOfficeReference = 'TestBOfficeRef-4';
            wsc4.bFOContractID = Contract2.id;
            wsc4.ParentContract = 'Test Contract';
            wsc4.ContractEndDate = System.today();
        
            WS_ServiceContractDispatch.WS_UnitServiceContract wsc2 = wsc.clone();
            wsc2.BackOfficeReference = '';
            WS_ServiceContractDispatch.WS_UnitServiceContract wsc3 = wsc.clone();
            wsc3.BackOfficeReference = 'TestBOfficeRef-1';
            bscd.ServiceContract = new  List<WS_ServiceContractDispatch.WS_UnitServiceContract>{wsc,wsc2,wsc3,wsc4};
            WS_ServiceContractDispatch.WS_bulkServiceContractResults response = WS_ServiceContractDispatch.WS_ServiceContractDispatch(bscd,'ITB_ORA'); 
        
    }
    static testMethod void myUnitTest2() {
            WS_ServiceContractDispatch.WS_bulkServiceContract bscd = new WS_ServiceContractDispatch.WS_bulkServiceContract();
            WS_ServiceContractDispatch.WS_UnitServiceContract wsc = getServiceContract();
            //Account acc = Utils_TestMethods.createAccount();
            Account acc = [select id,name from account where name='TestAccountUSNJ' limit 1];
            
            SVMXC__Service_Contract__c Contract = new SVMXC__Service_Contract__c();
            Contract.SVMXC__Company__c = acc.Id;
            Contract.SoldtoAccount__c=acc.Id;
            Contract.Name = 'Test Contract';              
            Contract.SVMXC__Start_Date__c = system.today();
            Contract.SVMXC__End_Date__c= system.today() + 10;
            Contract.SVMXC__Renewed_From__c = null;            
            Contract.LeadingBusinessBU__c ='BD';
            Contract.DefaultInstalledAtAccount__c=acc.Id;
            Contract.Tech_ContractCancel__c = true; 
            Contract.SVMXC__Active__c   = true;
            Contract.BackOfficeReference__c = 'TestBOfficeRef-1';           
            Contract.BackOfficeSystem__c = 'ITB_ORA';
            insert Contract;
            SVMXC__Service_Contract__c Contract2 = new SVMXC__Service_Contract__c();
            Contract2.SVMXC__Company__c = acc.Id;
            Contract2.SoldtoAccount__c=acc.Id;
            Contract2.Name = 'Test Contract 2 ';              
            Contract2.SVMXC__Start_Date__c = system.today();
            Contract2.SVMXC__End_Date__c= system.today() + 10;
            Contract2.SVMXC__Renewed_From__c = null;            
            Contract2.LeadingBusinessBU__c ='BD';
            Contract2.DefaultInstalledAtAccount__c=acc.Id;
            Contract2.Tech_ContractCancel__c = true; 
            Contract2.SVMXC__Active__c   = true;
            Contract2.BackOfficeReference__c = 'TestBOfficeRef-2';          
            Contract2.BackOfficeSystem__c = 'ITB_ORA';
            insert Contract2;
            
            wsc.ShiptoAccount = acc.id;
            wsc.SoldtoAccount = acc.id;
            wsc.BackOfficeReference = 'TestBOfficeRef-3';
            List<WS_ServiceContractDispatch.WS_UnitServiceItem> usilist = new List<WS_ServiceContractDispatch.WS_UnitServiceItem>();
            for(Integer i=0; i<5; i++)
            {
                WS_ServiceContractDispatch.WS_UnitServiceItem  usi =    getServiceItem();       
                usi.ItemNumber = usi.ItemNumber+'_'+i;
                usilist.add(usi);           
            }
            wsc.ServiceItem = usilist;
            WS_ServiceContractDispatch.WS_UnitServiceContract wsc2 = wsc.clone();           
            wsc2.BackOfficeReference = 'TestBOfficeRef-2';
            wsc2.ServiceItem = usilist;
            bscd.ServiceContract = new  List<WS_ServiceContractDispatch.WS_UnitServiceContract>{wsc,wsc2};
            WS_ServiceContractDispatch.WS_bulkServiceContractResults response = WS_ServiceContractDispatch.WS_ServiceContractDispatch(bscd,'ITB_ORA');
            bscd.ServiceContract = new  List<WS_ServiceContractDispatch.WS_UnitServiceContract>{wsc2};
            WS_ServiceContractDispatch.WS_bulkServiceContractResults response2 = WS_ServiceContractDispatch.WS_ServiceContractDispatch(bscd,'ITB_ORA');
        
    }
    

    static testMethod void myUnitTest3() {
        
            
       
            WS_ServiceContractDispatch.WS_bulkServiceContract bscd = new WS_ServiceContractDispatch.WS_bulkServiceContract();
            WS_ServiceContractDispatch.WS_UnitServiceContract wsc = getServiceContract();
            //Account acc = Utils_TestMethods.createAccount();
             Account acc = [select id,name from account where name='TestAccountUSNJ' limit 1];
            
            SVMXC__Service_Contract__c Contract = new SVMXC__Service_Contract__c();
            Contract.SVMXC__Company__c = acc.Id;
            Contract.SoldtoAccount__c=acc.Id;
            Contract.Name = 'Test Contract';              
            Contract.SVMXC__Start_Date__c = system.today();
            Contract.SVMXC__End_Date__c= system.today() ;
            Contract.SVMXC__Renewed_From__c = null;            
            Contract.LeadingBusinessBU__c ='BD';
            Contract.DefaultInstalledAtAccount__c=acc.Id;
            Contract.Tech_ContractCancel__c = true; 
            Contract.SVMXC__Active__c   = true;
            Contract.BackOfficeReference__c = 'TestBOfficeRef-1';           
            Contract.BackOfficeSystem__c = 'ITB_ORA';
            insert Contract;
        
            List<ServiceItem__c> serviceItems = new List<ServiceItem__c>();
            for(Integer i=0; i<5; i++)
            {
                    ServiceItem__c siobj = new ServiceItem__c();
                    siobj.ServiceMaintenanceContract__c = Contract.id;
                    siobj.ItemNumber__c = 'ItemNumber'+'_'+i;
                    serviceItems.add(siobj);
            }
             insert serviceItems;
            wsc.ShiptoAccount = acc.id;
            wsc.SoldtoAccount = acc.id;
            wsc.BackOfficeReference = 'TestBOfficeRef-1';
            wsc.bFOContractID = Contract.id;
            List<WS_ServiceContractDispatch.WS_UnitServiceItem> usilist = new List<WS_ServiceContractDispatch.WS_UnitServiceItem>();
            for(Integer i=0; i<10; i++)
            {
                WS_ServiceContractDispatch.WS_UnitServiceItem  usi =    getServiceItem();       
                usi.ItemNumber = usi.ItemNumber+'_'+i;
                usilist.add(usi);           
            }
            wsc.ServiceItem = usilist;
            
            bscd.ServiceContract = new  List<WS_ServiceContractDispatch.WS_UnitServiceContract>{wsc};
            WS_ServiceContractDispatch.WS_bulkServiceContractResults response = WS_ServiceContractDispatch.WS_ServiceContractDispatch(bscd,'ITB_ORA');
            WS_ServiceContractDispatch.getIncludeService(getServiceItem(),Contract.id,'ITB_ORA');
    }
     static testMethod void myUnitTest4() {
        
             WS_ServiceContractDispatch.WS_bulkServiceContract bscd = new WS_ServiceContractDispatch.WS_bulkServiceContract();
             WS_ServiceContractDispatch.WS_UnitServiceContract wsc = new WS_ServiceContractDispatch.WS_UnitServiceContract();
             bscd.ServiceContract = new  List<WS_ServiceContractDispatch.WS_UnitServiceContract>();
             WS_ServiceContractDispatch.WS_bulkServiceContractResults response = WS_ServiceContractDispatch.WS_ServiceContractDispatch(bscd,'ITB_ORA');
            WS_ServiceContractDispatch.bulkCreateServiceContract(bscd.ServiceContract,'ITB_ORA');
            WS_ServiceContractDispatch.checkMandatoryFields(new WS_ServiceContractDispatch.WS_UnitServiceContract(),'WS_UnitServiceContract');
            WS_ServiceContractDispatch.checkMandatoryFields(new WS_ServiceContractDispatch.WS_UnitServiceItem(),'WS_UnitServiceItem');
            
    }
    
    static WS_ServiceContractDispatch.WS_UnitServiceContract getServiceContract(){
        
        WS_ServiceContractDispatch.WS_UnitServiceContract wsc = new WS_ServiceContractDispatch.WS_UnitServiceContract();
        wsc.ContractNumber = 'ContNumber';        
        wsc.ContractPrice = 100;
        wsc.currencyCode = 'INR';
        //webservice  ID SoldtoAccount;
        //webservice  ID ShiptoAccount;        
        wsc.PONumber = '100';
        wsc.PODate = system.today();
        wsc.Description = 'Test Description';
        wsc.ContractStartDate = system.today();
        wsc.ContractEndDate = system.today();
        wsc.Status = 'Status';
        wsc.CancellationNotes = 'CancellationNotes';
        wsc.CancelledOn = system.today();
        wsc.BOContractType = 'BOContractType';
        //webservice  ID ParentContract;
        wsc.ServiceContractOwner = userinfo.getUserId();
        //webservice  ID SalesRep;
        wsc.LeadingBU = 'LeadingBu';
        //webservice  String OpportunityType;
        //webservice  String OpportunityScope;
        wsc.CountryofBackOffice='India';
        //webservice  String BackOfficeSystem;
        wsc.BackOfficeReference = 'BackOfficeRefTestclassMethod';
        //wsc.BackOfficeSystem ='ITB_ORA';
        //webservice  ID bFOContractID;
        
        
        return wsc;
    }
    static WS_ServiceContractDispatch.WS_UnitServiceItem getServiceItem(){
        
        WS_ServiceContractDispatch.WS_UnitServiceItem wsi = new WS_ServiceContractDispatch.WS_UnitServiceItem();
        wsi.ItemNumber = 'ItemNumber';
        wsi.Material ='Material';
        wsi.Description = 'Test Description';
        wsi.ItemCategory = 'ItemCategory';
        wsi.NetValue = 100;
        wsi.currencyCode = 'INR';
        wsi.Quantity = 100;
        //webservice  ID Shipto;
        wsi.StartDate =system.today();
        wsi.EndDate = system.today();
        wsi.Reasonforrejection = 'reason for rejection' ;
        
        
        return wsi;
    }

    
    
}