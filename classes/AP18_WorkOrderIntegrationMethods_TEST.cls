@IsTest
class AP18_WorkOrderIntegrationMethods_TEST
{
    static testMethod void testAP18_WorkOrderIntegrationMethods() 
    {
       Account Acc = Utils_TestMethods.createAccount();
       SVMXC__Service_Order__c workOrder = Utils_TestMethods.createWorkOrder(Acc.Id);
       
       insert Acc;
       insert WorkOrder;
       
       DataTemplate__c GMRProduct = new DataTemplate__c();
       GMRProduct.Field1__c='Test1';
       GMRProduct.Field2__c='Test2';
       GMRProduct.Field3__c='Test3';
       GMRProduct.Field4__c='Test4';
       GMRProduct.Field5__c='Test5';
       GMRProduct.Field6__c='Test6';
       GMRProduct.Field7__c='Test7';
       GMRProduct.Field8__c='Test8';
       GMRProduct.Field9__c='Test9';
       
       List<DataTemplate__c> products = new List<DataTemplate__c>();
       products.add(GMRProduct);
       products.add(GMRProduct);
              
       AP18_WorkOrderIntegrationMethods.UpdateCommercialReferenceWorkOrder(workOrder ,GMRProduct);
       AP18_WorkOrderIntegrationMethods.UpdateAdditionalCommercialReferencesWorkOrder(workOrder, products);              
    }
}