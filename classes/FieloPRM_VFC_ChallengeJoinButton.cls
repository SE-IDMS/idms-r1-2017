/********************************************************************
* Company: Fielo
* Created Date: 31/07/2015
* Description:
********************************************************************/
public with sharing class FieloPRM_VFC_ChallengeJoinButton {
    
    //Public variables
    public FieloEE__Component__c component {get;set;}
    public FieloCH__Challenge__c challenge {get;set;}
    public String componentJoinButtonLabelField {get;set;}
    public String componentTextField {get;set;}
    public Boolean processSync {get; set;}
    public Boolean showButton {get;set;}
    
    public String challengeId{
        get{
            return this.challengeId;
        }
        set{
            this.challengeId = value;
            if(initialized == false){init();}
        }
    }
    
    //Private variables
    private FieloEE__Member__c member;
    private PageReference challengeMenu;
    
    //CONSTRUCTOR
    public FieloPRM_VFC_ChallengeJoinButton(){
        processSync = false;
    }
    
    //FAKE CONSTRUCTOR
    private Boolean initialized = false;
    public void init(){
        initialized = true;
        try{
            //Member query
            member = [SELECT Id, F_Account__r.F_PRM_ProgramTeam__c, F_PRM_ContactRegistrationStatus__c FROM FieloEE__Member__c WHERE Id =: FieloEE.MemberUtil.getMemberId()];
            
            //Challenge query
            challenge = [SELECT Id, FieloCH__IsActive__c, Name, FieloCH__Mode__c, F_PRM_MenuProgram__c, F_PRM_ProgramCode__c, F_PRM_MenuProgram__r.FieloEE__ExternalName__c FROM FieloCH__Challenge__c WHERE Id =: challengeId];
            
            //Set challengeMenu
            if(challenge.F_PRM_MenuProgram__c != null){
                challengeMenu = new PageReference('/Menu/' + challenge.F_PRM_MenuProgram__r.fieloEE__ExternalName__C);
            }else{
                challengeMenu = new PageReference('/Menu/MyPrograms');
            }
            
            //Multilang fields
            componentJoinButtonLabelField = Schema.SObjectType.FieloEE__Component__c.fields.getMap().containskey('F_PRM_JoinButtonLabel_' + FieloEE.OrganizationUtil.getLanguage() + '__c') ? 'F_PRM_JoinButtonLabel_' + FieloEE.OrganizationUtil.getLanguage() + '__c' : 'F_PRM_JoinButtonLabel__c';
            componentTextField = Schema.SObjectType.FieloEE__Component__c.fields.getMap().containskey('Text_' + FieloEE.OrganizationUtil.getLanguage() + '__c') ? 'Text_' + FieloEE.OrganizationUtil.getLanguage() + '__c' : 'FieloEE__Text__c';
            
            //Calculate show button (Member is enrolled?)
            showButton = true;
            if(member.F_PRM_ContactRegistrationStatus__c == 'Validated'){
                if(challenge.FieloCH__IsActive__c){
                    if(challenge.FieloCH__Mode__c == 'Teams' && member.F_Account__r.F_PRM_ProgramTeam__c != null){
                        //Team
                        //Check by Challenge Member
                        List<FieloCH__ChallengeMember__c> challengeMember = [SELECT Id, FieloCH__Status__c FROM FieloCH__ChallengeMember__c WHERE FieloCH__Team__c =: member.F_Account__r.F_PRM_ProgramTeam__c AND FieloCH__Challenge2__c =: challengeId LIMIT 1];
                        if((!challengeMember.isEmpty()) && challengeMember[0].FieloCH__Status__c != 'Unsubscribe'){ showButton = false; }
                    }else{
                        //Competition
                        //Check by Challenge Member
                        List<FieloCH__ChallengeMember__c> challengeMember = [SELECT Id, FieloCH__Status__c FROM FieloCH__ChallengeMember__c WHERE FieloCH__Member__c =: member.Id AND FieloCH__Challenge2__c =: challengeId LIMIT 1];
                        if((!challengeMember.isEmpty()) && challengeMember[0].FieloCH__Status__c != 'Unsubscribe'){ showButton = false; }
                        //Check by Event
                        list<FieloEE__Event__c> listEvent = [SELECT Id FROM FieloEE__Event__c WHERE FieloEE__Member__c =: member.id AND F_PRM_ValueText__c =: challenge.F_PRM_ProgramCode__c AND FieloEE__Type__c = : 'Registration' LIMIT 1];
                        if(!listEvent.isEmpty()){ showButton = false; }
                    }
                }else{
                    showButton = false;
                }
            }else{
                showButton = false;
            }
        }catch(Exception e){}
    }
    
    public PageReference joinAction(){
        Savepoint sp = Database.setSavepoint();
        
        PageReference pageRet;
        try{
            //Subscribe the team to the challenge
            if(challenge.FieloCH__Mode__c == 'Teams'){
                FieloPRM_Utils_ChTeam.addTeamsChallenge(challenge.Id, new Set<Id>{member.F_Account__c});
            }
            
            //Create Event related to the component
            if(component.F_PRM_CreateEnrollmentEvent__c){
                FieloEE__Event__c eventRegistration = new FieloEE__Event__c(
                    Name = 'Enrollment - ' + challenge.Name, 
                    FieloEE__Type__c = 'Enrollment',
                    FieloEE__Member__c = member.id,
                    F_PRM_ValueText__c = challenge.F_PRM_ProgramCode__c
                );
                insert eventRegistration;
            }
            
            //Refresh segmentation for the Member logged
            FieloEE.RedemptionUtil.refreshSegmentCookies();
            
            //Check if there are Badge Members for the Member logged not processed
            List<FieloEE__BadgeMember__c> bms = new List<FieloEE__BadgeMember__c>();
            Set<Id> badgeIds = new Set<Id>();
            for(FieloCH__ChallengeReward__c rew: [SELECT Id, FieloCH__Badge__c FROM FieloCH__ChallengeReward__c WHERE FieloCH__Challenge__c =: challengeId AND FieloCH__Badge__c != NULL]){
                badgeIds.add(rew.FieloCH__Badge__c);
            }
            
            //Check if status field exists
            bms = [SELECT Id FROM FieloEE__BadgeMember__c WHERE FieloEE__Member2__c =: member.Id AND FieloEE__Badge2__c IN: badgeIds AND FieloCH__Status__c = 'Pending'];
            
            if(!bms.isEmpty()){
                //Process pending records
                for(FieloEE__BadgeMember__c bm: bms){
                    bm.FieloCH__Status__c = 'To Process';
                }
                update bms;
            }
            
            pageRet = challengeMenu;
        }catch(Exception e){
            //Rollback
            Database.rollback(sp);
            
            //Error log
            FieloEE__ErrorLog__c errorL = new FieloEE__ErrorLog__c(FieloEE__Message__c = e.getMessage(), FieloEE__StackTrace__c = e.getStackTraceString(), FieloEE__Type__c = e.getTypeName(), FieloEE__LineNumber__c = e.getLineNumber(), FieloEE__UserId__c = UserInfo.getUserId());
            insert errorL;
            
            //Show the error in the component
            errorL = [SELECT Id, Name FROM FieloEE__ErrorLog__c WHERE Id =: errorL.Id];
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLQ316PRM009.replace('{0}', errorL.Name)));
            //Label CLQ316PRM009: "An error has occurred while processing your request. Error ID: {0}"
        }
        return pageRet;
    }
    
}