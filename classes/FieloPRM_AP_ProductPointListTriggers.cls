/******************************************
* Developer: Fielo Team                   *
*******************************************/
public with sharing class FieloPRM_AP_ProductPointListTriggers{

    //*****************
    // AFTER INSERT
    //*****************

    public static void createPointValues(){

        List<FieloPRM_ProductPointList__c> triggernew = trigger.new;
            
        Set<String> setExistingList = new Set<String>();
        Set<String> setNewPointListCountry = new Set<String>();
                
        for(FieloPRM_ProductPointList__c productPointList: triggernew){
            //point list values to clone
            if(productPointList.F_PRM_PointListToClone__c != null){
                setExistingList.add(productPointList.F_PRM_PointListToClone__c);
            //new point list values to create
            }else{
                setNewPointListCountry.add(productPointList.F_PRM_Country__c);
            }
        }
        
        Map<String,List<FieloPRM_ProductPointValue__c>> mapListPointValues = new Map<String,List<FieloPRM_ProductPointValue__c>>();
        
        if(!setExistingList.isEmpty()){
            List<FieloPRM_ProductPointValue__c> listExistingPointValues = [SELECT Id, F_PRM_IsActive__c, F_PRM_PointList__c, F_PRM_PtValue__c, F_PRM_LoyaltyEligibleProduct__c 
                                                                           FROM FieloPRM_ProductPointValue__c WHERE F_PRM_PointList__c IN: setExistingList AND F_PRM_IsActive__c = true];
            if(!listExistingPointValues.isEmpty()){
                for(FieloPRM_ProductPointValue__c pointValue: listExistingPointValues){
                    if(!mapListPointValues.containsKey(pointValue.F_PRM_PointList__c)){
                        mapListPointValues.put(pointValue.F_PRM_PointList__c,new List<FieloPRM_ProductPointValue__c>{pointValue});
                    }else{
                        mapListPointValues.get(pointValue.F_PRM_PointList__c).add(pointValue);
                    }
                }
            }
        }
        
        Map<String,List<FieloPRM_LoyaltyEligibleProduct__c>> mapCountryProducts = new Map<String,List<FieloPRM_LoyaltyEligibleProduct__c>>();
        
        if(!setNewPointListCountry.isEmpty()){
            List<FieloPRM_LoyaltyEligibleProduct__c> listLOyaltyEligbleProducts = [SELECT Id, F_PRM_Country__c FROM FieloPRM_LoyaltyEligibleProduct__c WHERE F_PRM_Country__c IN: setNewPointListCountry];

            if(!listLOyaltyEligbleProducts.isEmpty()){
                for(FieloPRM_LoyaltyEligibleProduct__c product: listLOyaltyEligbleProducts){
                    if(!mapCountryProducts.containsKey(product.F_PRM_Country__c)){
                        mapCountryProducts.put(product.F_PRM_Country__c,new List<FieloPRM_LoyaltyEligibleProduct__c>{product});
                    }else{
                        mapCountryProducts.get(product.F_PRM_Country__c).add(product);
                    }
                }
            }       
        }
        
        List<FieloPRM_ProductPointValue__c> listPointsValuesInsert = new List<FieloPRM_ProductPointValue__c>();
        
        FieloPRM_ProductPointValue__c newPointValue = null;
        
        for(FieloPRM_ProductPointList__c productPointList: triggernew){
            if(productPointList.F_PRM_PointListToClone__c != null){
                if(mapListPointValues.containsKey(productPointList.F_PRM_PointListToClone__c)){
                    for(FieloPRM_ProductPointValue__c existingPointValue: mapListPointValues.get(productPointList.F_PRM_PointListToClone__c)){
                        newPointValue = new FieloPRM_ProductPointValue__c(
                            F_PRM_PointList__c = productPointList.Id,
                            F_PRM_IsActive__c = true,
                            F_PRM_PtValue__c = existingPointValue.F_PRM_PtValue__c,
                            F_PRM_LoyaltyEligibleProduct__c = existingPointValue.F_PRM_LoyaltyEligibleProduct__c
                        );
                        listPointsValuesInsert.add(newPointValue);
                    }
                }
            }else{
                if(mapCountryProducts.containsKey(productPointList.F_PRM_Country__c)){
                    for(FieloPRM_LoyaltyEligibleProduct__c product: mapCountryProducts.get(productPointList.F_PRM_Country__c)){
                        newPointValue = new FieloPRM_ProductPointValue__c(
                            F_PRM_PointList__c = productPointList.Id,
                            F_PRM_IsActive__c = false,
                            F_PRM_PtValue__c = 0,
                            F_PRM_LoyaltyEligibleProduct__c = product.Id
                        );
                        listPointsValuesInsert.add(newPointValue);
                    }                
                
                }
            }
        }
        
        if(!listPointsValuesInsert.isEmpty()){
            insert listPointsValuesInsert;
        }         
    }
    
    //************************
    // BEFORE INSERT / UPDATE
    //************************

    public static void validatesPointListOverlappingDates(){
    
        List<FieloPRM_ProductPointList__c> triggernew = trigger.new;
        Map<Id,SObject> triggerOldMap = trigger.oldMap;
        
        Date minDate = null;
        Date maxDate = null;
        
        Set<String> setCountrys = new Set<String>();
        
        for(FieloPRM_ProductPointList__c pointList: triggernew){
            if(trigger.isInsert || (trigger.isUpdate && (pointList.F_PRM_Country__c != ((FieloPRM_ProductPointList__c)triggerOldMap.get(pointList.Id)).F_PRM_Country__c || pointList.F_PRM_StartDate__c != ((FieloPRM_ProductPointList__c)triggerOldMap.get(pointList.Id)).F_PRM_StartDate__c || pointList.F_PRM_EndDate__c != ((FieloPRM_ProductPointList__c)triggerOldMap.get(pointList.Id)).F_PRM_EndDate__c))){
                for(FieloPRM_ProductPointList__c pointList2: triggernew){
                    if(pointList != pointList2 && pointList.F_PRM_Country__c == pointList2.F_PRM_Country__c){
                        if((pointList2.F_PRM_StartDate__c >= pointList.F_PRM_StartDate__c && pointList2.F_PRM_StartDate__c <= pointList.F_PRM_EndDate__c) || (pointList2.F_PRM_EndDate__c >= pointList.F_PRM_StartDate__c && pointList2.F_PRM_EndDate__c <= pointList.F_PRM_EndDate__c) || (pointList2.F_PRM_StartDate__c <= pointList.F_PRM_StartDate__c && pointList2.F_PRM_EndDate__c >= pointList.F_PRM_EndDate__c)){
                            String list1 = pointList.Name + ' ' + string.valueOf(pointList.F_PRM_StartDate__c) + ' - ' + string.valueOf(pointList.F_PRM_EndDate__c);
                            String list2 = pointList2.Name + ' ' + string.valueOf(pointList2.F_PRM_StartDate__c) + ' - ' + string.valueOf(pointList2.F_PRM_EndDate__c);
                            pointList.addError('Error: Overlapping point list are being created:' + ' ' + list1 + ' -- ' + list2);
                        }
                    }
                
                }
                setCountrys.add(pointList.F_PRM_Country__c);           
                if(minDate == null || pointList.F_PRM_StartDate__c < minDate){
                    minDate = pointList.F_PRM_StartDate__c;
                }
                if(maxDate == null || pointList.F_PRM_EndDate__c < maxDate){
                    maxDate = pointList.F_PRM_EndDate__c;
                }
            }
        }
        
        if(!setCountrys.isEmpty() && minDate != null && maxDate != null){
        
            List<FieloPRM_ProductPointList__c> listExistingProductLists = [SELECT Id, F_PRM_Country__c, F_PRM_StartDate__c, F_PRM_EndDate__c, Name FROM FieloPRM_ProductPointList__c WHERE 
                                                                          ((F_PRM_StartDate__c >=: minDate AND F_PRM_StartDate__c <=: maxDate) OR
                                                                          (F_PRM_EndDate__c    >=: minDate AND F_PRM_EndDate__c   <=: maxDate) OR
                                                                          (F_PRM_StartDate__c  <=: minDate AND F_PRM_EndDate__c   >=: maxDate)) AND 
                                                                           F_PRM_Country__c IN: setCountrys];
                                                                           
            Map<String,List<FieloPRM_ProductPointList__c>> mapCountryPointLists = new Map<String,List<FieloPRM_ProductPointList__c>>();
            
            for(FieloPRM_ProductPointList__c pointList: listExistingProductLists){
                if(!mapCountryPointLists.containsKey(pointList.F_PRM_Country__c)){
                    mapCountryPointLists.put(pointList.F_PRM_Country__c, new List<FieloPRM_ProductPointList__c> {pointList});
                }else{
                    mapCountryPointLists.get(pointList.F_PRM_Country__c).add(pointList);
                }
            }
            
            for(FieloPRM_ProductPointList__c newPointList: triggernew){
                if(mapCountryPointLists.containsKey(newPointList.F_PRM_Country__c)){
                    for(FieloPRM_ProductPointList__c existingPointList: mapCountryPointLists.get(newPointList.F_PRM_Country__c)){
                        if(existingPointList.Id != newPointList.Id){
                            if((existingPointList.F_PRM_StartDate__c >= newPointList.F_PRM_StartDate__c && existingPointList.F_PRM_StartDate__c <= newPointList.F_PRM_EndDate__c) || (existingPointList.F_PRM_EndDate__c >= newPointList.F_PRM_StartDate__c && existingPointList.F_PRM_EndDate__c <= newPointList.F_PRM_EndDate__c) || (existingPointList.F_PRM_StartDate__c <= newPointList.F_PRM_StartDate__c && existingPointList.F_PRM_EndDate__c >= newPointList.F_PRM_EndDate__c)){
                                newPointList.addError('Error: Overlapping with' + ' ' + existingPointList.Name + ': ' + string.valueOf(existingPointList.F_PRM_StartDate__c) + ' - ' + string.valueOf(existingPointList.F_PRM_EndDate__c));
                            }
                        }
                    }
                
                }
            }
        }
                                                                       
    }

}