/**
10/03/15    
Bala CANDASSAMY  
APR 15 Release    
Initial Creation: test class for AP_ServiceContractInitialContract
 */
@isTest(SeeAllData=true)
private class AP_ServiceContractInitialContract_TEST {
 
 public static testMethod void myUnitTest() {
 
    //A country
 Country__c testCountry = [Select id,Name,Countrycode__c From Country__c where Countrycode__c =: 'FR'];
    
     
    //An Account     
    Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
    testAccount.Name = 'Test';
    insert testAccount;
    
    //A Contact              
    Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
    testContact.Country__c = testCountry.Id;
    insert testContact;
    
    //4 contracts
    
    SVMXC__Service_Contract__c testContract0 = new SVMXC__Service_Contract__c();
    testContract0.SVMXC__Company__c = testAccount.Id;
    testContract0.Name = 'Test Contract0';  
    testContract0.SVMXC__Contact__c = testContact.Id  ;    
    testContract0.SVMXC__Start_Date__c = system.today();
    testContract0.SVMXC__Renewed_From__c = null;
    insert testContract0 ;
    

    SVMXC__Service_Contract__c testContract1 = new SVMXC__Service_Contract__c();
    testContract1.SVMXC__Company__c = testAccount.Id;
    testContract1.Name = 'Test Contract1';  
    testContract1.SVMXC__Contact__c = testContact.Id  ;    
    testContract1.SVMXC__Start_Date__c = system.today();
    testContract1.SVMXC__Renewed_From__c = testContract0.Id;
     
    insert testContract1 ;
        
    
    SVMXC__Service_Contract__c testContract2 = new SVMXC__Service_Contract__c();
    testContract2.SVMXC__Company__c = testAccount.Id;
    testContract2.Name = 'Test Contract2';  
    testContract2.SVMXC__Contact__c = testContact.Id  ;    
    testContract2.SVMXC__Start_Date__c = system.today();
    testContract2.SVMXC__Renewed_From__c = testContract1.Id;
        
    insert testContract2 ;

    System.debug('###** CONTRACT 0 ID'+testContract0.id);
    System.debug('###** CONTRACT 1 ID'+testContract1.id); 
    System.debug('###** CONTRACT 2 ID'+testContract2.id); 
    System.debug('###** CONTRACT 1 renewed from: '+testContract1.SVMXC__Renewed_From__c+'/'+testContract0.id);
    System.debug('###** CONTRACT 1 initial contract: '+testContract1.Initial_Contract__c+'/'+testContract0.id);
    System.debug('###** CONTRACT 2 renewed from: '+testContract2.SVMXC__Renewed_From__c+'/'+testContract1.id);
    System.debug('###** CONTRACT 2 initial contract: '+testContract2.Initial_Contract__c+'/'+testContract0.id);
     
         
    /*
    List<SVMXC__Service_Contract__c> ServiceContractList = new List<SVMXC__Service_Contract__c>();
    ServiceContractList.add(testContract0);
    ServiceContractList.add(testContract1);
    ServiceContractList.add(testContract2);
     
    AP_ServiceContractInitialContract.InitialContract(ServiceContractList);
     
    System.debug('###** CONTRACT 0 ID'+testContract0.id);
    System.debug('###** CONTRACT 1 ID'+testContract1.id); 
    System.debug('###** CONTRACT 1 renewed from CONTRACT 0 '+testContract1.SVMXC__Renewed_From__c);
    System.debug('###** CONTRACT 1 initial contract IS CONTRACT 0'+testContract1.Initial_Contract__c);
    System.debug('###** CONTRACT 2 renewed from CONTRACT 1 '+testContract2.SVMXC__Renewed_From__c);
    System.debug('###** CONTRACT 2 initial contract IS CONTRACT 0'+testContract2.Initial_Contract__c);

    System.assertEquals(testContract1.SVMXC__Renewed_From__c,testContract1.Initial_Contract__c);
    System.assertEquals(testContract2.Initial_Contract__c,testContract1.Initial_Contract__c);
    */
 
}
}