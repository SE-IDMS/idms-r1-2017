/*
Author:Siddharth N(GD Solutions)
Purpose:Search GMR for product information and create a record based on selection

Modified By: Vimal K (GD)
Changed URL Parameter Values
Added RemoteFunction for VF Remoting

Modified By: Cecile L (Velvet)
Add a shopping cart
*/

global with sharing class VFC_AddUpdateProductForCase {    
    // Map of all existing related products on the case and new selected products from the search results.
    // Key :    field <TECH_UniqueId__c>
    public Map<String, CSE_RelatedProduct__c> map_ShoppingCartProducts {get;set;}
    // Return the values of the map map_ShoppingCartProducts, to be used in the pageDataBlock
    public List<CSE_RelatedProduct__c> getShoppingCartProducts() {
        List<CSE_RelatedProduct__c> lst_Products = map_ShoppingCartProducts.values();
        lst_Products.sort();
        return lst_Products;
    } 

    // Existing related products on Case, to be used as referential.
    // Key :    field <TECH_UniqueId__c> (CSE_RelatedProduct__c)
    // Value :  field <Id> (CSE_RelatedProduct__c)
    public Map<String,Id> map_currCaseExistingProductsTechId2Id {get;set;}
    public Set<Id> set_currCaseListExistingProducts {get;set;}
    
    // Removed related products, to be deleted at the end of process.
    public Set<Id> set_currCaseListRemovedProducts {get;set;}
    // Master from existing set of related products on Case, to be used as referential.
    public String oldMasterProduct {get;set;}
    
    // Field <TECH_UniqueId__c> of selected product   when click on "Remove" in a data row.
    public String selectedProductRemoved {get;set;}    
    // Field <TECH_UniqueId__c> of selected product when select a new master in a data row.
    public String selectedMasterProduct = '';
    public String getSelectedMasterProduct(){ return selectedMasterProduct; }
    public void setSelectedMasterProduct (String masterCr){
        if(masterCr != null && masterCr != '')  
            selectedMasterProduct  = masterCr;
    }
    // Field <TECH_UniqueId__c> of previous master product. Used to change the value of MasterProduct__c of previous master when select a new master.
    public String previousSelectedMasterProduct {get;set;}
    public Boolean errorOccurred {get;set;}
    
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public Case caseRecord{get;set;}
    public static String strGMRCode = '';
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    
    global class ResultWrapper{
        boolean blnReturnStatus;
        String  strReturnMessage;
      //  List<CSE_RelatedProduct__c> lst_SelectedRelatedProducts;
    }
    
    /************************
     *      CONTROLLER      *
     * **********************/
    public VFC_AddUpdateProductForCase(ApexPages.StandardController controller){
        map_ShoppingCartProducts = new Map<String, CSE_RelatedProduct__c>();
        caseRecord=(Case)controller.getRecord();                        
        if(caseRecord.id!=null){
            caseRecord=[select Status, SupportCategory__c,ProductBU__c ,ProductLine__c ,ProductFamily__c ,Family__c ,CommercialReference__c,TECH_LaunchGMR__c, TECH_GMRCode__c  from Case where id=:caseRecord.id];
            if(caseRecord.Id!=null)
                pageParameters.put('caseId',caseRecord.Id);
            if(caseRecord.ProductBU__c!=null)
                pageParameters.put('businessLine',caseRecord.ProductBU__c);            
            if(caseRecord.ProductLine__c!=null)
                pageParameters.put('productLine',caseRecord.ProductLine__c);            
            if(caseRecord.ProductFamily__c!=null)
                pageParameters.put('strategicProductFamily',caseRecord.ProductFamily__c);            
            if(caseRecord.Family__c!=null)
                pageParameters.put('family',caseRecord.Family__c);            
            if(caseRecord.CommercialReference__c!=null)
                pageParameters.put('commercialReference',caseRecord.CommercialReference__c);
            if(caseRecord.TECH_GMRCode__c!=null && caseRecord.TECH_GMRCode__c.length()>0){
                pageParameters.put('masterCR','true');
                strGMRCode = caseRecord.TECH_GMRCode__c;
            }
            else{
                pageParameters.put('masterCR','false');
            }
            String URLCR='';
            if(pageParameters.containsKey('commercialReference'))
                URLCR =pageParameters.get('commercialReference');
            /*         
            String URLParam ='';
            
            if(pageParameters.containsKey('origin'))
                URLParam =pageParameters.get('origin');
            
            if(URLParam == 'icr') {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'\"'+URLCR +'\" is not a valid commercial reference.'));
            }
               
            BR-4310 - Update Condition and Error Message
            else if(caseRecord.SupportCategory__c == '4 - Post-Sales Tech Support' && caseRecord.TECH_LaunchGMR__c) { 
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.warning,'You must select at least a family for post sales technical cases.'));
            }
            
            else 
            */
            if(caseRecord.SupportCategory__c !=NULL && System.Label.CLAPR14CCC11.contains(caseRecord.SupportCategory__c) && caseRecord.ProductBU__c ==null) { 
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Warning,System.Label.CLAPR14CCC10));
            }            
            
            List<CSE_RelatedProduct__c> existingProductsOnCase = [SELECT Id,ProductSuccession__c,ProductGroup__c,CommercialReference_lk__c,TECH_GMRCode__c,SubFamily__c,TECH_UniqueId__c,Family_lk__c,Name, ProductDescription__c, ProductLine__c, BusinessUnit__c, MasterProduct__c, CommercialReference__c, ProductFamily__c, Family__c FROM CSE_RelatedProduct__c WHERE Case__c = :caseRecord.Id];
            
            map_currCaseExistingProductsTechId2Id = new Map<String,Id>();
            set_currCaseListRemovedProducts = new Set<Id>();
            
            for(CSE_RelatedProduct__c relProduct : existingProductsOnCase ){
                map_ShoppingCartProducts.put(relProduct.TECH_UniqueId__c, relProduct);
                map_currCaseExistingProductsTechId2Id.put(relProduct.TECH_UniqueId__c,relProduct.Id);
                if(relProduct.MasterProduct__c){
                    selectedMasterProduct = relProduct.TECH_UniqueId__c;
                    oldMasterProduct = relProduct.TECH_UniqueId__c;
                }
            }
            
            set_currCaseListExistingProducts = new Set<Id>(map_currCaseExistingProductsTechId2Id.values());
            previousSelectedMasterProduct = selectedMasterProduct;
        }        
    }
    public String scriteria{get;set;}{    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
    }
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
    


        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
            }
        }
        if(!Test.isRunningTest() || System.Label.GMRIntegrationType != 'BEM') {
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            System.debug(results.gmrFilteredDataBeanList);
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
    
    @RemoteAction
    global static String getSearchText() {
        String scriteria='Vimal';
        /*
        System.debug('--Vimal--');
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
        System.debug('--Vimal--: ' + scriteria);
        */
        return scriteria; 
    }
    
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        return Database.query(query);       
    }
    
    public PageReference pageCancelFunction(){
        PageReference pageResult;
        if(caseRecord.id!=null){
            pageResult = new PageReference('/'+ caseRecord.id);
            try{
                if(caseRecord.status != 'Closed' && caseRecord.status != 'Cancelled' ){
                    caseRecord.TECH_LaunchGMR__c = false;
                    DataBase.SaveResult UpdateCase = Database.Update(caseRecord);
                }
                pageResult = new ApexPages.StandardController(caseRecord).view();
            }
            catch(exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+e.getMessage()));
            }   
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
        
    /*
     *  Checkbox : Master   
     *  Check the field MasterProduct__c of the new master product and uncheck the old one.
     *****/
    public PageReference selectMasterProduct() {
        selectedMasterProduct = Apexpages.currentPage().getParameters().get('selectedMasterProduct');
        
        if(map_ShoppingCartProducts.containskey(previousSelectedMasterProduct))
            map_ShoppingCartProducts.get(previousSelectedMasterProduct).MasterProduct__c = false;
        if(map_ShoppingCartProducts.containskey(selectedMasterProduct))
            map_ShoppingCartProducts.get(selectedMasterProduct).MasterProduct__c = true;
        
        previousSelectedMasterProduct = selectedMasterProduct;
        return null;
    }
        
    /*
     *  Link : remove   
     *  Remove the selected line from the shopping cart and add the related product to a list to be deleted at the end of process.
     *****/
    public PageReference removeLineShoppingCart() {
        // Remove the selected product from the shopping cart
        selectedProductRemoved = Apexpages.currentPage().getParameters().get('selectedProductRemoved');
        map_ShoppingCartProducts.remove(selectedProductRemoved);
        
        // If the selected product was part of the initial set of existing products on the case, 
        // add it to this list to be removed at the end of process.
        if(map_currCaseExistingProductsTechId2Id.containskey(selectedProductRemoved) )
            set_currCaseListRemovedProducts.add(map_currCaseExistingProductsTechId2Id.get(selectedProductRemoved));
        
        return null;
    }
        
    /*
     *  Button : Save   
     *  Add all products in the shopping cart to the case and delete removed products.
     *****/
    public PageReference saveShoppingCart() 
    {
        errorOccurred = false;
        Case currCase = new Case(Id = caseRecord.id);
        
        if(map_ShoppingCartProducts.values().size() != 0){
            // If a Master product is selected, copy its fields on the Case.
            // If not, display an error to the user
            if(map_ShoppingCartProducts.containsKey(selectedMasterProduct)){
                CSE_RelatedProduct__c masterProductOrFamily = map_ShoppingCartProducts.get(selectedMasterProduct);
                currCase.ProductBU__c              = masterProductOrFamily.BusinessUnit__c;
                currCase.ProductLine__c            = masterProductOrFamily.ProductLine__c;
                currCase.ProductFamily__c          = masterProductOrFamily.ProductFamily__c;
                currCase.Family__c                 = masterProductOrFamily.Family__c;
                currCase.Family_lk__c              = masterProductOrFamily.Family_lk__c;
                currCase.SubFamily__c              = masterProductOrFamily.SubFamily__c;
                currCase.ProductSuccession__c      = masterProductOrFamily.ProductSuccession__c;
                currCase.ProductGroup__c           = masterProductOrFamily.ProductGroup__c;
                currCase.ProductDescription__c     = masterProductOrFamily.ProductDescription__c;
                currCase.CommercialReference__c    = masterProductOrFamily.CommercialReference__c;
                currCase.CommercialReference_lk__c = masterProductOrFamily.CommercialReference_lk__c;
                currCase.TECH_GMRCode__c           = masterProductOrFamily.TECH_GMRCode__c;
                currCase.CheckedbyGMR__c           = TRUE;
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLDEC14CCC04));
                errorOccurred = true;
                return null;
            }
            
            try{
                // Delete the Related Products removed from the shopping cart 
                if(set_currCaseListRemovedProducts != null && set_currCaseListRemovedProducts.size() != 0){
                    List<Id> lst_currCaseListRemovedProducts = new List<Id>(set_currCaseListRemovedProducts);
                    Database.delete(lst_currCaseListRemovedProducts);
                }
                
                // Upsert the Related Products added to the shopping cart
                Database.upsert(map_ShoppingCartProducts.values(),Schema.CSE_RelatedProduct__c.TECH_UniqueId__c); 
                
                // Update the Case only if the Master Product has changed //
                if(oldMasterProduct != selectedMasterProduct){
                    AP_Case_CaseHandler.triggerPM0HierarchyOnChildObject=false;
                    Database.update(currCase);
                }
                
                // Return to the case //
                PageReference pageResult = new PageReference('/'+caseRecord.Id);
                return pageResult;
            }catch(Exception ex){
                system.debug('########## exception '+ex);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+ex.getMessage()));
                errorOccurred = true;
                return null;
            }               
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLDEC14CCC03));
            errorOccurred = true;
            return null;
        }
    }
    
    /*
     *  Button : Add product(s) 
     *  Add selected products from the search results to the shopping cart
     *****/
    public PageReference addProductToShoppingCart(){
        Set<String> setExistingGMRCode      = new Set<String>();
        Set<String> setGMRCodeTobeAdded     = new Set<String>();
        Set<String> setUniqueId             = new Set<String>();
        Set<String> setGMRCodeTobeQueried   = new Set<String>();
        
        // Retrieve the selected parameters on the page
        String selectedMasterCR = Apexpages.currentPage().getParameters().get('masterCR');
        String selected_products = Apexpages.currentPage().getParameters().get('selected_products');
        String selected_families = Apexpages.currentPage().getParameters().get('selected_families');
        
        if(selected_products != null && selected_products != '') {
            // Get the list of selected related products as GMRDataBean
            WS_GMRSearch.resultFilteredDataBean resultObjGMRDatBean_Products = (WS_GMRSearch.resultFilteredDataBean) JSON.deserialize(selected_products, WS_GMRSearch.resultFilteredDataBean.class);
            
            // Create a Case Related Product for each product in the list
            if(resultObjGMRDatBean_Products != null && resultObjGMRDatBean_Products.gmrFilteredDataBeanList != null) {            
                for(WS_GMRSearch.gmrFilteredDataBean objGMRDatBean_Product :resultObjGMRDatBean_Products.gmrFilteredDataBeanList){
                    // Create a Case Related Record with information of selected Product
                    CSE_RelatedProduct__c objCaseRelatedProduct = new CSE_RelatedProduct__c();
                    if(objGMRDatBean_Product.ProductIdbFO != null){
                        objCaseRelatedProduct.Case__c = caseRecord.Id;
                        objCaseRelatedProduct.CommercialReference_lk__c = objGMRDatBean_Product.ProductIdbFO.UnEscapeXML();
                        objCaseRelatedProduct.BusinessUnit__c = objGMRDatBean_Product.businessLine.label.UnEscapeXML();
                        objCaseRelatedProduct.ProductLine__c = objGMRDatBean_Product.productLine.label.UnEscapeXML();
                        objCaseRelatedProduct.ProductFamily__c = objGMRDatBean_Product.strategicProductFamily.label.UnEscapeXML();
                        objCaseRelatedProduct.Family__c = objGMRDatBean_Product.family.label.UnEscapeXML();
                        
                        if(objGMRDatBean_Product.ProductFamilybFO!=null)
                            objCaseRelatedProduct.Family_lk__c = objGMRDatBean_Product.ProductFamilybFO.UnEscapeXML();
                        objCaseRelatedProduct.SubFamily__c = objGMRDatBean_Product.subFamily.label.UnEscapeXML();
                        objCaseRelatedProduct.ProductSuccession__c = objGMRDatBean_Product.productSuccession.label.UnEscapeXML();
                        objCaseRelatedProduct.ProductGroup__c = objGMRDatBean_Product.productGroup.label.UnEscapeXML();
                        objCaseRelatedProduct.ProductDescription__c = objGMRDatBean_Product.description.UnEscapeXML();
                        objCaseRelatedProduct.CommercialReference__c = objGMRDatBean_Product.commercialReference.UnEscapeXML();
                        
                        if(objGMRDatBean_Product.businessLine.value != null){
                            objCaseRelatedProduct.TECH_GMRCode__c = objGMRDatBean_Product.businessLine.value;
                            objCaseRelatedProduct.Name            = objGMRDatBean_Product.businessLine.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.productLine.value != null){
                            objCaseRelatedProduct.TECH_GMRCode__c += objGMRDatBean_Product.productLine.value;
                            objCaseRelatedProduct.Name            = objGMRDatBean_Product.productLine.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.strategicProductFamily.label != null){
                            objCaseRelatedProduct.TECH_GMRCode__c += objGMRDatBean_Product.strategicProductFamily.value;
                            objCaseRelatedProduct.Name            = objGMRDatBean_Product.businessLine.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.family.label != null){
                            objCaseRelatedProduct.TECH_GMRCode__c += objGMRDatBean_Product.family.value;
                            objCaseRelatedProduct.Name            = objGMRDatBean_Product.family.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.subFamily.label != null){
                            objCaseRelatedProduct.TECH_GMRCode__c += objGMRDatBean_Product.subFamily.value;
                            objCaseRelatedProduct.Name            = objGMRDatBean_Product.subFamily.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.productSuccession.label != null){
                            objCaseRelatedProduct.TECH_GMRCode__c += objGMRDatBean_Product.productSuccession.value;
                            objCaseRelatedProduct.Name            = objGMRDatBean_Product.productSuccession.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.productGroup.label != null){
                            objCaseRelatedProduct.TECH_GMRCode__c += objGMRDatBean_Product.productGroup.value;
                            objCaseRelatedProduct.Name            = objGMRDatBean_Product.productGroup.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.commercialReference != null && objGMRDatBean_Product.commercialReference.trim()!=''){
                            objCaseRelatedProduct.Name            = objGMRDatBean_Product.commercialReference.UnEscapeXML();
                        }
                        objCaseRelatedProduct.TECH_UniqueId__c    = objCaseRelatedProduct.Case__c + '-' + objCaseRelatedProduct.CommercialReference_lk__c;
                        
                        setUniqueId.add(objCaseRelatedProduct.TECH_UniqueId__c);
                        
                        // Compare the selectedMasterCR to the Product commercial reference to see if this product has been selected as Master
                        if(selectedMasterCR!=null && selectedMasterCR == objGMRDatBean_Product.commercialReference)
                            selectedMasterProduct = objCaseRelatedProduct.TECH_UniqueId__c;
                        
                        // Check if the product was previously removed from the existing related products list
                        // If yes, remove it from the list to delete.
                        if(map_currCaseExistingProductsTechId2Id.containsKey(objCaseRelatedProduct.TECH_UniqueId__c)){
                            if(set_currCaseListRemovedProducts.contains(map_currCaseExistingProductsTechId2Id.get(objCaseRelatedProduct.TECH_UniqueId__c))){
                                set_currCaseListRemovedProducts.remove(map_currCaseExistingProductsTechId2Id.get(objCaseRelatedProduct.TECH_UniqueId__c));
                            }
                        }
                        
                        // CHeck if the product is not already in the Map, to avoid erase the ID
                        if(!map_ShoppingCartProducts.containsKey(objCaseRelatedProduct.TECH_UniqueId__c)) {
                            map_ShoppingCartProducts.put(objCaseRelatedProduct.TECH_UniqueId__c,objCaseRelatedProduct);
                        }
                        
                    } 
                    else 
                    {
                        String strGMRCodeTobeAdded ='';
                        if(objGMRDatBean_Product.Tech_GMRCode!=null){
                            strGMRCodeTobeAdded = objGMRDatBean_Product.Tech_GMRCode;
                            if(objGMRDatBean_Product.Tech_GMRCode.length()>=8)
                                strGMRCodeTobeAdded = objGMRDatBean_Product.Tech_GMRCode.subString(0,8);
                            setGMRCodeTobeAdded.add(strGMRCodeTobeAdded);
                        }
                    }
                }
            }
        }

        if(selected_families != null && selected_families != ''){   
            // Get the list of selected families as GMRDataBean
            WS_GMRSearch.resultFilteredDataBean resultObjGMRDatBean_Families= (WS_GMRSearch.resultFilteredDataBean) JSON.deserialize(selected_families, WS_GMRSearch.resultFilteredDataBean.class);
            
            // Create a Case Related Product for each family in the list   
            if(resultObjGMRDatBean_Families != null && resultObjGMRDatBean_Families.gmrFilteredDataBeanList != null)
            {            
                for(WS_GMRSearch.gmrFilteredDataBean objGMRDatBean_Family :resultObjGMRDatBean_Families.gmrFilteredDataBeanList)
                {
                    // Create a Case Related Record with information of selected Product
                    CSE_RelatedProduct__c objCaseRelatedProduct = new CSE_RelatedProduct__c();
                    
                    if(objGMRDatBean_Family.ProductFamilybFO!=null)
                    {
                        if(objGMRDatBean_Family.Tech_GMRCode !=null && ( objGMRDatBean_Family.Tech_GMRCode.length() >= 8 && !(setExistingGMRCode.contains(objGMRDatBean_Family.Tech_GMRCode.subString(0,8))) ) )
                        {
                            objCaseRelatedProduct.Case__c = caseRecord.Id;
                            objCaseRelatedProduct.Family_lk__c = objGMRDatBean_Family.ProductFamilybFO.UnEscapeXML();
                            objCaseRelatedProduct.BusinessUnit__c = objGMRDatBean_Family.businessLine.label.UnEscapeXML();
                            objCaseRelatedProduct.ProductLine__c = objGMRDatBean_Family.productLine.label.UnEscapeXML();
                            objCaseRelatedProduct.ProductFamily__c = objGMRDatBean_Family.strategicProductFamily.label.UnEscapeXML();
                            objCaseRelatedProduct.Family__c = objGMRDatBean_Family.family.label.UnEscapeXML();
                            objCaseRelatedProduct.SubFamily__c = null;
                            objCaseRelatedProduct.ProductSuccession__c = null;
                            objCaseRelatedProduct.ProductGroup__c = null;
                            objCaseRelatedProduct.ProductDescription__c = null;
                            objCaseRelatedProduct.CommercialReference__c = null;
                            objCaseRelatedProduct.CommercialReference_lk__c = null;
                            
                            String familyCode8 = objGMRDatBean_Family.Tech_GMRCode.length() >= 8 ? objGMRDatBean_Family.Tech_GMRCode.subString(0,8) : objGMRDatBean_Family.Tech_GMRCode;  
                            setExistingGMRCode.add(familyCode8);
                            
                            
                            if(objGMRDatBean_Family.businessLine.value != null)
                            {
                                objCaseRelatedProduct.TECH_GMRCode__c = objGMRDatBean_Family.businessLine.value;
                                objCaseRelatedProduct.Name            = objGMRDatBean_Family.businessLine.label.UnEscapeXML();
                            }
                            
                            if(objGMRDatBean_Family.productLine.value != null)
                            {
                                objCaseRelatedProduct.TECH_GMRCode__c += objGMRDatBean_Family.productLine.value;
                                objCaseRelatedProduct.Name            = objGMRDatBean_Family.productLine.label.UnEscapeXML();
                            }
                            
                            if(objGMRDatBean_Family.strategicProductFamily.label != null)
                            {
                                objCaseRelatedProduct.TECH_GMRCode__c += objGMRDatBean_Family.strategicProductFamily.value;
                                objCaseRelatedProduct.Name            = objGMRDatBean_Family.strategicProductFamily.label.UnEscapeXML();
                            }
                            
                            if(objGMRDatBean_Family.family.label != null)
                            {
                                objCaseRelatedProduct.TECH_GMRCode__c += objGMRDatBean_Family.family.value;
                                objCaseRelatedProduct.Name            = objGMRDatBean_Family.family.label.UnEscapeXML();
                            }
                            objCaseRelatedProduct.TECH_UniqueId__c    = objCaseRelatedProduct.Case__c + '-' + objCaseRelatedProduct.Family_lk__c;
                            setUniqueId.add(objCaseRelatedProduct.TECH_UniqueId__c);
                            
                            // Compare the selectedMasterCR to the Family GMR code to see if this family has been selected as Master
                            if(selectedMasterCR != null && selectedMasterCR.equalsIgnoreCase(familyCode8))
                                selectedMasterProduct = objCaseRelatedProduct.TECH_UniqueId__c;
                            
                            // Check if the Family was previously removed from the existing related products list
                            // If yes, remove it from the list to delete.
                            if(map_currCaseExistingProductsTechId2Id.containsKey(objCaseRelatedProduct.TECH_UniqueId__c)){
                                String existingFamilyTechId = map_currCaseExistingProductsTechId2Id.get(objCaseRelatedProduct.TECH_UniqueId__c);
                                if(set_currCaseListRemovedProducts.contains(existingFamilyTechId)){
                                    set_currCaseListRemovedProducts.remove(existingFamilyTechId);
                                }
                            }
                            
                            // CHeck if the product is not already in the Map, to avoid erase the ID
                            if(!map_ShoppingCartProducts.containsKey(objCaseRelatedProduct.TECH_UniqueId__c)){
                                map_ShoppingCartProducts.put(objCaseRelatedProduct.TECH_UniqueId__c,objCaseRelatedProduct);
                            }
                        }
                    } 
                    else 
                    {
                        String strGMRCodeTobeAdded ='';
                        if(objGMRDatBean_Family.Tech_GMRCode!=null){
                            strGMRCodeTobeAdded = objGMRDatBean_Family.Tech_GMRCode;
                            if(objGMRDatBean_Family.Tech_GMRCode.length()>=8)
                                strGMRCodeTobeAdded = objGMRDatBean_Family.Tech_GMRCode.subString(0,8);
                            setGMRCodeTobeAdded.add(strGMRCodeTobeAdded);
                        }
                    }
                }    
            }
        }

        if(setGMRCodeTobeAdded.size()>0){
            for(String strGMRCodeTobeAdded: setGMRCodeTobeAdded){
                if(!setExistingGMRCode.contains(strGMRCodeTobeAdded))
                    setGMRCodeTobeQueried.add(strGMRCodeTobeAdded);
            }
            
            if(setGMRCodeTobeQueried.size()>0){
                List<OPP_Product__c> lstOPPProduct = new List<OPP_Product__c>([Select Id, Name, BusinessUnit__c,ProductLine__c,ProductFamily__c,Family__c, TECH_PM0CodeInGMR__c, HierarchyType__c From OPP_Product__c where TECH_PM0CodeInGMR__c IN :setGMRCodeTobeQueried AND CCCRelevant__c=true AND TECH_PM0CodeInGMR__c!=null]);
                if(lstOPPProduct.size()>0){
                    for(OPP_Product__c objOPPProduct: lstOPPProduct){
                        CSE_RelatedProduct__c objCaseRelatedProduct = new CSE_RelatedProduct__c();
                        objCaseRelatedProduct.Case__c                   = caseRecord.Id;
                        objCaseRelatedProduct.Family_lk__c              = objOPPProduct.Id;
                        objCaseRelatedProduct.BusinessUnit__c           = objOPPProduct.BusinessUnit__c;
                        objCaseRelatedProduct.ProductLine__c            = objOPPProduct.ProductLine__c;
                        objCaseRelatedProduct.ProductFamily__c          = objOPPProduct.ProductFamily__c;
                        objCaseRelatedProduct.Family__c                 = objOPPProduct.Family__c;
                        objCaseRelatedProduct.TECH_GMRCode__c           = objOPPProduct.TECH_PM0CodeInGMR__c;
                        objCaseRelatedProduct.TECH_UniqueId__c          = objCaseRelatedProduct.Case__c + '-' + objCaseRelatedProduct.Family_lk__c;
                        setUniqueId.add(objCaseRelatedProduct.TECH_UniqueId__c);
                        objCaseRelatedProduct.Name                      = objOPPProduct.Name;
                            
                        objCaseRelatedProduct.SubFamily__c              = null;
                        objCaseRelatedProduct.ProductSuccession__c      = null;
                        objCaseRelatedProduct.ProductGroup__c           = null;
                        objCaseRelatedProduct.ProductDescription__c     = null;
                        objCaseRelatedProduct.CommercialReference__c    = null;
                        objCaseRelatedProduct.CommercialReference_lk__c = null;
                        
                        // Compare the selectedMasterCR to the Product GMR code
                        String familyCode8 = objOPPProduct.TECH_PM0CodeInGMR__c.length() >= 8 ? objOPPProduct.TECH_PM0CodeInGMR__c.subString(0,8) : objOPPProduct.TECH_PM0CodeInGMR__c;  
                        String selectedMasterCR8 = selectedMasterCR.length() >= 8 ? selectedMasterCR.subString(0,8) : selectedMasterCR;                      
                        if(selectedMasterCR8 !=null && selectedMasterCR8.equalsIgnoreCase(familyCode8))
                            selectedMasterProduct = objCaseRelatedProduct.TECH_UniqueId__c;
                        
                        if(objOPPProduct.HierarchyType__c != 'PM0-FAMILY')
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, System.Label.CLDEC14CCC02));
                            
                        map_ShoppingCartProducts.put(objCaseRelatedProduct.TECH_UniqueId__c,objCaseRelatedProduct);
                    }
                }
            }
        }
        
        // Uncheck the field MasterProduct__c of the previously Master product, and check it on the new one.
        if(selectedMasterProduct != null && selectedMasterProduct != '' && previousSelectedMasterProduct != null && map_ShoppingCartProducts.containsKey(selectedMasterProduct))
        {   
            if(map_ShoppingCartProducts.containsKey(previousSelectedMasterProduct))
                map_ShoppingCartProducts.get(previousSelectedMasterProduct).MasterProduct__c = false;
            
            map_ShoppingCartProducts.get(selectedMasterProduct).MasterProduct__c = true;
            previousSelectedMasterProduct = selectedMasterProduct;
        }
        
        return null;
    }
}