/*
    Author          : ACN 
    Date Created    : 8/11/2011
    Description     : Test class for VFC65_NewEditSMETeamMember class 
*/
@isTest(SeeAllData=true)
private class VFC65_NewEditSMETeamMember_TEST
{           
    static testMethod void testApprovedQuoteLink()
    {   
         Profile prof = [select id,Name from profile where name='System Administrator'];                    
         User systemAdmin = Utils_TestMethods.createStandardUser(prof.Id,'tU1VFC65');
         
         //inserts Account
         Account acc = RIT_Utils_TestMethods.createAccount();
         acc.ZipCode__c = '100001';
         insert acc;

         //inserts Opportunity
         Opportunity opp = RIT_Utils_TestMethods.createOpportunity(acc.Id);
         insert opp;
         
         //inserts QuoteLink
         OPP_QuoteLink__c quoteLink1 = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
         quoteLink1.QuoteStatus__c = Label.CL00242;
         insert quoteLink1;
                     
         quoteLink1 = [select id,CurrencyISOCode,QuoteStatus__c from OPP_QuoteLink__c where Id=:quoteLink1.Id];          
                 
         RIT_SMETeam__C sme = Utils_TestMethods.createSME(quoteLink1.id,'legal');                              
         sme.MemberActivated__c = true;
         sme.TeamMember__c = UserInfo.getUserId();
                      
         test.startTest();
         system.runas(systemAdmin)
         {
                 ApexPages.StandardController sc = new ApexPages.StandardController(sme);         
                 PageReference p = Page.VFP65_NewEditSMETEamMember;
                 Test.setCurrentPage(p);
                 p.getParameters().put(CS007_StaticValues__c.getValues('CS007_1').Values__c,quoteLink1.Id);      //Quote Link Field Id of SME Team Member                                                 
                 VFC65_NewEditSMETeamMember smeTeamMemb= new VFC65_NewEditSMETeamMember(sc);
                 smeTeamMemb.detailedComm = 'testSMEFeedback';                 
                 smeTeamMemb.checkQuoteLinkStatus();   
         }
         test.stoptest();
    }
    
    static testMethod void testPosScn()
    {
         // List<Profile> prof = [select id,Name from profile where name='SE - Sales Manager'];                    
         User salesManager1,salesManager2,salesManager3;
         Account acc;
         Opportunity opp;
         list<User> lstUser = new list<User>();
         /* Commented as part of Profile Redesign --Start */

         // for(Profile p: prof)
         // {
         //     if(p.name == 'SE - Sales Manager')
         //     {
         //            salesManager1 = Utils_TestMethods.createStandardUser(p.Id,'tU2VFC65');
         //            salesManager2 = Utils_TestMethods.createStandardUser(p.Id,'tU3VFC65');
         //            salesManager3 = Utils_TestMethods.createStandardUser(p.Id,'tU4VFC65');
         //            lstUser.add(salesManager1);
         //            lstUser.add(salesManager3);
         //            lstUser.add(salesManager2);                    
         //     }
         //     insert lstUser;
         // }
         /* Commented as part of Profile Redesign --ENd */

        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;        
        System.runAs(sysadmin2){
         Map<String,User> aliasnametousermap=new Map<String,User>();   
         Map<String,UserandPermissionSets> aliasnametouserpermissionsetrecordmap=new Map<String,UserandPermissionSets>();   
         for(integer i=0;i<3;i++)
         {
             System.debug('jobFunctionPermissionSetMap'+UserandPermissionSets.jobFunctionPermissionSetMap);
            UserandPermissionSets newuserandpermissionsetrecord=new UserandPermissionSets('Users'+i,'SE - Sales Manager');            
            user newuser = newuserandpermissionsetrecord.userrecord;     
            System.debug('newuserandpermissionsetrecord.userrecord'+newuserandpermissionsetrecord.userrecord);       
            aliasnametouserpermissionsetrecordmap.put('Users'+i,newuserandpermissionsetrecord);
            aliasnametousermap.put('Users'+i,newuser);
        }
        System.debug('aliasnametousermap'+aliasnametousermap);
        Database.insert(aliasnametousermap.values());     
        lstUser.addAll(aliasnametousermap.values());
        List<PermissionSetAssignment> permissionsetassignmentlstforusers=new List<PermissionSetAssignment>();    
        for(String useralias:aliasnametouserpermissionsetrecordmap.keySet())
        {            
            for(Id permissionsetId:aliasnametouserpermissionsetrecordmap.get(useralias).permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforusers.add(new PermissionSetAssignment(AssigneeId=aliasnametousermap.get(useralias).id,PermissionSetId=permissionsetId));    
        } 
        if(permissionsetassignmentlstforusers.size()>0)
            Database.insert(permissionsetassignmentlstforusers); 
        }
        salesManager1=lstUser[0];
        salesManager2=lstUser[1];
        salesManager3=lstUser[2];

         //inserts Account
         Account acc1 = RIT_Utils_TestMethods.createAccount();
         acc1.ZipCode__c = '100001';
         insert acc1;
        
         //inserts Opportunity
         Opportunity opp1 = RIT_Utils_TestMethods.createOpportunity(acc1.Id);
         insert opp1;
         list<AccountShare> lstAccountShare = new list<AccountShare>();
         for(integer i=0;i<lstUSer.size();i++)
         {
            AccountShare aShare = new AccountShare();
            aShare.accountId= acc1.Id;
            aShare.UserOrGroupId = lstUSer[i].Id;
            aShare.accountAccessLevel = 'edit';
            aShare.OpportunityAccessLevel = 'read';
            aShare.CaseAccessLevel = 'edit';
            //insert aShare;  
            lstAccountShare.add(aShare);
         }                  
         insert lstAccountShare; 
         RIT_SMETeam__C sme = new RIT_SMETeam__C();
         RIT_SMETeam__C sme1 = new RIT_SMETeam__C();         
         RIT_SMETeam__C sme2 = new RIT_SMETeam__C();
         OPP_QuoteLInk__c  quoteLink = new OPP_QuoteLInk__c();                 
         system.runas(salesManager1)
         {               
                 //inserts QuoteLink
                 quoteLink = RIT_Utils_TestMethods.createQuoteLink(opp1.Id);
                 insert quoteLink; 
            
                 sme = Utils_TestMethods.createSME(quoteLink.id,'legal');             
                 sme.DetailedComments__c = 'testDetComm';
                 insert sme;  
                 
                 sme1 = Utils_TestMethods.createSME(quoteLink.id,'legal');             
                 sme1.DetailedComments__c = 'testDetComm';
                 
                 sme2 = Utils_TestMethods.createSME(quoteLink.id,'legal');             
                 sme2.DetailedComments__c = 'testDetComm';
                 sme2.MemberActivated__c = true;
                 sme2.TeamMember__c = UserInfo.getUserId();
                 insert sme2;
         }
         test.startTest();
         system.runas(salesManager1)
         {               
                 ApexPages.StandardController sc = new ApexPages.StandardController(sme);         
                 sme = [select Id,DetailedComments__c,MemberActivated__c,MemberRole__c,QuoteLink__c,SMEFeedback__c, SMERecommendationStatus__c,
                        SMERecommendationStatusFormula__c,TeamMember__c,QuoteLink__r.Quotestatus__c,QuoteLink__r.OwnerId from RIT_SMETeam__c where Id =: sme.Id];
                 PageReference p = Page.VFP65_NewEditSMETEamMember;
                 Test.setCurrentPage(p);
                 p.getParameters().put(CS007_StaticValues__c.getValues('CS007_1').Values__c,quoteLink.Id);            //Quote Link Field Id of SME Team Member    
                 VFC65_NewEditSMETeamMember smeTeamMemb= new VFC65_NewEditSMETeamMember(sc);                 
                 smeTeamMemb.detailedComm = 'testSMEFeedback';
                 smeTeamMemb.checkQuoteLinkStatus();
                 smeTeamMemb.save();
                 
                 sc = new ApexPages.StandardController(sme1);  
                 smeTeamMemb= new VFC65_NewEditSMETeamMember(sc);
                 smeTeamMemb.detailedComm = 'testSMEFeedback';                 
                 smeTeamMemb.saveAndNew();

                 sc = new ApexPages.StandardController(sme2);         
                 smeTeamMemb= new VFC65_NewEditSMETeamMember(sc);
                 smeTeamMemb.detailedComm = 'testSMEFeedback';                 
                 smeTeamMemb.checkQuoteLinkStatus();
                 smeTeamMemb.save();
         }
         system.runas(salesManager3)
         {
             sme = Utils_TestMethods.createSME(quoteLink.id,'legal');             
             ApexPages.StandardController sc = new ApexPages.StandardController(sme);         
             PageReference p = Page.VFP65_NewEditSMETEamMember;
             Test.setCurrentPage(p);
             p.getParameters().put(CS007_StaticValues__c.getValues('CS007_1').Values__c,quoteLink.Id);                                  //Quote Link Field Id of SME Team Member    
             VFC65_NewEditSMETeamMember smeTeamMemb= new VFC65_NewEditSMETeamMember(sc);
             smeTeamMemb.checkQuoteLinkStatus();             

         } 
         system.runas(salesManager2)
         {
             sme = Utils_TestMethods.createSME(quoteLink.id,'legal');             
             ApexPages.StandardController sc = new ApexPages.StandardController(sme);         
             PageReference p = Page.VFP65_NewEditSMETEamMember;
             Test.setCurrentPage(p);
             p.getParameters().put(CS007_StaticValues__c.getValues('CS007_1').Values__c,quoteLink.Id);                                  //Quote Link Field Id of SME Team Member    
             VFC65_NewEditSMETeamMember smeTeamMemb= new VFC65_NewEditSMETeamMember(sc);
             smeTeamMemb.checkQuoteLinkStatus();
        }
        test.StopTest();
    }
    
    static testMethod void testNegativeScn()
    {
        Profile prof = [select id,Name from profile where name='SE - Delegated System Administrator'];                    
        User delegatedSysAdmin = Utils_TestMethods.createStandardUser(prof.Id,'tU5VFC65');
        OPP_QuoteLInk__c  quoteLink = new OPP_QuoteLInk__c();                          
        RIT_SMETeam__C sme2 = new RIT_SMETEam__c();
        
        //inserts Account
        Account acc2 = RIT_Utils_TestMethods.createAccount();
        acc2.ZipCode__c = '100001'; 
        insert acc2;
        
        //inserts Opportunity
        Opportunity opp2 = RIT_Utils_TestMethods.createOpportunity(acc2.Id);
        insert opp2;
        
        system.runas(delegatedSysAdmin)
        {
                 //inserts QuoteLink
                 quoteLink = RIT_Utils_TestMethods.createQuoteLink(opp2.Id);
                 insert quoteLink; 
                 
                 sme2 = Utils_TestMethods.createSME(quoteLink.id,'legal');                              
                 sme2.MemberActivated__c = true;
                 sme2.TeamMember__c = UserInfo.getUserId();
        }  
        test.StartTest();
        system.runas(delegatedSysAdmin)
        {
                 ApexPages.StandardController sc = new ApexPages.StandardController(sme2);         
                 Pagereference p = Page.VFP65_NewEditSMETEamMember;
                 Test.setCurrentPage(p);
                 p.getParameters().put(CS007_StaticValues__c.getValues('CS007_1').Values__c,quoteLink.Id);                                  //Quote Link Field Id of SME Team Member    
                 VFC65_NewEditSMETeamMember smeTeamMemb= new VFC65_NewEditSMETeamMember(sc);
                 smeTeamMemb.detailedComm = 'testSMEFeedback';                 
                 smeTeamMemb.checkQuoteLinkStatus();
                 smeTeamMemb.save(); 
        }  
        test.StopTest();
    }
    
    
     
     
}