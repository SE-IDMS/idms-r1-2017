/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: VF Controller                                       *
* VisualForce: Fielo_ListView                               *
* Created Date: 08.09.2014                                  *
* Description: Actions (C: Create, R: Read                  *                   
               U: Update, D: Delete)                        *
************************************************************/

public class Fielo_CRUDController {
    
    //Variables PUBLICAS
    public List<Fielo_PageSection__c> lstSections {get;set;}
    public Fielo_PageLayout__c playout {get;set;}
    public boolean privError {get;set;}
    public String recordId {get;set;}
    public Sobject record {get;set;}
    public string mode {get;set;}
    
    public Fielo_CRUDController(){
        
        //Obtengo los parametros
        mode = system.currentPageReference().getParameters().get('Mode');
        recordId = system.currentPageReference().getParameters().get('RecordId');
        
        Id memberId = FieloEE.memberUtil.getMemberId();
        privError = false;
        
        //Query al Fielo Component
        FieloEE__Component__c fComp = [SELECT F_CRUDObjectSettings__c FROM FieloEE__Component__c WHERE Id =: ApexPages.currentPage().getParameters().get('fComponentId')];
        
        //Query al CRUS SObject Settings
        String allFields1 = '';
        for(Schema.SObjectField nameAPIfield : Schema.SObjectType.Fielo_ListView__c.fields.getMap().values()){
            allFields1 += allFields1 == '' ? String.ValueOf(nameAPIfield): ', '+String.ValueOf(nameAPIfield);
        }
        Fielo_CRUDObjectSettings__c crudOS = Database.query('SELECT Id, F_ObjectApiName__c, F_FieldToDefinePageLayout__c FROM Fielo_CRUDObjectSettings__c WHERE Id = \''+fComp.F_CRUDObjectSettings__c+'\'');
                
        //Busco el Page Layout que le corresponde
        if(crudOS.F_FieldToDefinePageLayout__c != null){
            playout = [SELECT Id, F_RedirectBack__c, F_RedirectEdit__c, F_RedirectDelete__c, F_ObjectApiName__c, F_FieldMember__c, F_ButtonAccess__c, (SELECT Id, F_Name__c, F_ButtonName__c, RecordType.DeveloperName FROM F_CRUDPageComponents__r WHERE RecordType.DeveloperName ='Fielo_CRUDButton'), (SELECT Name, F_FieldSet__c, F_LinkField__c, F_PageSize__c, F_RedirectDelete__c, F_RedirectDeleteFieldCondition__c, F_RedirectDeleteValueCondition__c, F_RedirectDetails__c, F_RedirectEdit__c, F_RedirectEditFieldCondition__c, F_RedirectEditValueCondition__c, F_RedirectNew__c, F_RelatedListObjectAPIName__c, F_WhereCondition__c, RecordType.DeveloperName FROM F_RelatedLists__r) FROM Fielo_PageLayout__c WHERE F_CRUD_ObjectSettings__r.F_ObjectAPIName__c =: crudOS.F_ObjectApiName__c AND F_Type__c =: String.ValueOf(record.get(crudOS.F_FieldToDefinePageLayout__c)) LIMIT 1];
        }else{
            playout = [SELECT Id, F_RedirectBack__c, F_RedirectEdit__c, F_RedirectDelete__c, F_ObjectApiName__c, F_FieldMember__c, F_ButtonAccess__c, (SELECT Id, F_Name__c, F_ButtonName__c, RecordType.DeveloperName FROM F_CRUDPageComponents__r WHERE RecordType.DeveloperName ='Fielo_CRUDButton'), (SELECT Name, F_FieldSet__c, F_LinkField__c, F_PageSize__c, F_RedirectDelete__c, F_RedirectDeleteFieldCondition__c, F_RedirectDeleteValueCondition__c, F_RedirectDetails__c, F_RedirectEdit__c, F_RedirectEditFieldCondition__c, F_RedirectEditValueCondition__c, F_RedirectNew__c, F_RelatedListObjectAPIName__c, F_WhereCondition__c, RecordType.DeveloperName FROM F_RelatedLists__r) FROM Fielo_PageLayout__c WHERE F_CRUD_ObjectSettings__r.F_ObjectAPIName__c =: crudOS.F_ObjectApiName__c LIMIT 1];
        }
        
        //Armo las secciones del Page Layout
        if(mode == 'c' || mode == 'r' || mode == 'u'){
            lstSections = [SELECT Id, Name, F_Name__c, F_Columns__c, F_Visibility__c, (SELECT Id, F_Name__c, F_Required__c, F_ReadOnly__c, F_Type__c, F_HTMLContent__c, F_CSSContent__c, F_Visibility__c, RecordType.DeveloperName, F_WhereCondition__c FROM Page_Components__r ORDER BY F_Order__c ASC) FROM Fielo_PageSection__c WHERE F_PageLayout__c =: playout.Id ORDER BY F_Order__c ASC];
        }
        
        //Se arma un string con todos los campos del objeto
        String allFields = '';
        for(Schema.SObjectField nameAPIfield : Schema.getGlobalDescribe().get(crudOS.F_ObjectApiName__c).getDescribe().fields.getMap().values()){
            allFields += allFields == ''? String.ValueOf(nameAPIfield) : ', '+String.ValueOf(nameAPIfield);
        }
        
        //Query o creacion del Record
        if(mode == 'c'){
            //Create Mode
            record = Schema.getGlobalDescribe().get(crudOS.F_ObjectApiName__c).newSObject();
        }else if(mode == 'r' || mode == 'u'){
            //Read or Update Mode
            String queryRec = 'SELECT ' + allFields + ' FROM ' + crudOS.F_ObjectApiName__c + ' WHERE Id = \'' + recordId + '\'';
            try{
                record = DataBase.query(queryRec);
            }catch(Exception e){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                
            }
        }else if(mode == 'd'){
            //Delete Mode
            String queryRec = 'SELECT Id FROM ' + crudOS.F_ObjectApiName__c + ' WHERE Id = \'' + recordId + '\'';
            try{
                record = DataBase.query(queryRec);
            }catch(Exception e){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
        }
        
        //Error de privilegios, el registro no pertenece al member
        if(playout.F_FieldMember__c != NULL && record.get(playout.F_FieldMember__c) != memberId){
            privError = true;
        }
    }
    
    public pageReference doDelete(){
        try{
            DataBase.delete(record);
        }catch(System.DmlException e){
            for(Integer i = 0; i < e.getNumDml(); i++){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
            }
            return null;
        }
        return doBack();
    }
    
    public pageReference doSave(){
        try{
            DataBase.upsert(record);
        }catch(System.DmlException e){
            for(Integer i = 0; i < e.getNumDml(); i++){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));            
            }
            return null;
        }
        return doBack();
    }
    
    public pageReference doEdit(){
        string recordId = system.currentPageReference().getParameters().get('recordId');
        PageReference pageRet = Fielo_Utilities.getMenuReferenceById(playout.F_RedirectEdit__c);
        pageRet.getParameters().put('recordId', recordId);
        pageRet.getParameters().put('mode', 'u');
        pageRet.setRedirect(true);
        return pageRet;
    } 
    
    public pageReference doBack(){
        PageReference pageRet = Fielo_Utilities.getMenuReferenceById(playout.F_RedirectBack__c);
        pageRet.setRedirect(true);
        return pageRet;
    }
    
}