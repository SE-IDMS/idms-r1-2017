/*
    Author          : Accenture Team 
    Date Created    : 29/02/2012
    Description     : Test Class for Territory Data Source Utility class
*/
@Istest

class Utils_TerritoryDataSource_TEST
{
    private static Country__c country;
    private static SE_Territory__c territory;
    private static StateProvince__c stateprovince;
    
    static
    {
    // create test data
    country = Utils_TestMethods.createCountry();
    insert country;
    
    stateprovince = Utils_TestMethods.createStateProvince(country.id);
    insert stateprovince;
    
    territory =  Utils_TestMethods.createTerritory(country.id, stateprovince.id);
    insert territory;
    }
    
    static testMethod void competitorData_TestMethod()
    {
        Utils_DataSource TerritoryDataSource = Utils_Factory.getDataSource('TERR');
        List<SelectOption> Columns = TerritoryDataSource.getColumns();
        
        List<String> dummyList1 = new List<String>();
        List<String> dummyList2 = new List<String>();
        Integer counter = 0;
        
        for(Integer index=1;index<9;index++)
        {
            dummyList1.clear();
            dummyList2.clear();                
            counter = index;
            
            if(counter == 1)
            {
                dummyList1.add('TEST');
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(stateprovince.Id));
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = TerritoryDataSource.search(dummyList1, dummyList2);
            }
            
            if(counter == 2)
            {
                dummyList1.add('TEST');
                dummyList2.add('BD');
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = TerritoryDataSource.search(dummyList1, dummyList2);
            }
            
            if(counter == 3)
            {
                dummyList1.add('TEST');
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(stateprovince.Id));
                dummyList2.add(String.ValueOf(country.Id));                
                Utils_DataSource.Result Results = TerritoryDataSource.search(dummyList1, dummyList2);
            }
            
            if(counter == 4)
            {
                dummyList1.add('TEST');
                dummyList2.add(Label.CL00355);
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = TerritoryDataSource.search(dummyList1, dummyList2);
            }
            
            if(counter == 5)
            {
                dummyList1.add('');
                dummyList2.add('BD');
                dummyList2.add(String.ValueOf(stateprovince.Id));
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = TerritoryDataSource.search(dummyList1, dummyList2);
            }
            
            if(counter == 6)
            {
                dummyList1.add('');
                dummyList2.add('BD');
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = TerritoryDataSource.search(dummyList1, dummyList2);
            }
            
            if(counter == 7)
            {
                dummyList1.add('');
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country.Id));
                dummyList2.add(String.ValueOf(stateprovince.Id));
                Utils_DataSource.Result Results = TerritoryDataSource.search(dummyList1, dummyList2);
            }
            
            if(counter == 8)
            {
                dummyList1.add('');
                dummyList2.add(Label.CL00355);
                dummyList2.add(Label.CL00355);
                dummyList2.add(String.ValueOf(country.Id));
                Utils_DataSource.Result Results = TerritoryDataSource.search(dummyList1, dummyList2);
            }
        }
    }
}