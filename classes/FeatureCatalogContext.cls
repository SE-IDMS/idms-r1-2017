global class FeatureCatalogContext {
    WebService String[] featureCatalogIDs;

    WebService Integer BatchSize = 10000;
    
    WebService DateTime LastModifiedDate1;
    WebService DateTime LastModifiedDate2;
    WebService DateTime CreatedDate;
    
    /**
    * A blank constructor
    */
    public FeatureCatalogContext() {
    }

    /**
    * A constructor based on an Program @param a Program
    */
    public FeatureCatalogContext(List<String> featureCatalogIDs) {
        this.featureCatalogIDs= featureCatalogIDs;

    }
}