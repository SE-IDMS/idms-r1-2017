/* 
    Global Class Name: WS02_CreateOpportunitySeries_Test
    Author:Amitava Dutta
    Date:25/01/2011.
    Purpose:To create a Series of Opportunity on the basis of 
    the TimeSpan of Agreement and copy the existing details of Master 
    Opportunity to the Series.
    RE: SR0+ Prototype
*/

@isTest
private class WS02_SetConstantValue_Test
{ 
    
       // --- Test the global class for Create Opportunity Series --
        static TestMethod void TestGetAgreementInterval()
        {
            AP05_OpportunityTrigger.testClassFlag = False;
            AP04_AgreementTrigger.WS02TestClassFlag = False;       
            Date myDate = date.Today();
            Date myDate2 = date.newInstance(2011, 9, 12);
            
            
            User newUser = Utils_TestMethods.createStandardUser('TestUser');         
            Database.SaveResult UserInsertResult = Database.insert(newUser, false);
           
            if(!UserInsertResult.isSuccess())
                Database.Error err = UserInsertResult.getErrors()[0];
            
            System.runAs(newUser) {                            
                Test.startTest(); 
                List<Opportunity> oppList = new List<Opportunity>();
                List<OppAgreement__c> oppAggList = new List<OppAgreement__c>();
                
                Country__c newCountry = Utils_TestMethods.createCountry();
                Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
                   
                if(!CountryInsertResult.isSuccess())
                    Database.Error err = CountryInsertResult.getErrors()[0];
            
                OppAgreement__c objAgree = Utils_TestMethods.createFrameAgreement(newUser.Id);
                Database.SaveResult AgreementInsertResult = Database.insert(objAgree, false);
                    
                if(!AgreementInsertResult.isSuccess())
                    Database.Error err = AgreementInsertResult.getErrors()[0];               
                
                Account acc = Utils_TestMethods.createAccount();
                Database.SaveResult AccountInsertResult = Database.insert(acc, false);
                WS02_SetConstantValue.AgreementID = AgreementInsertResult.getId();
                if(!AccountInsertResult.isSuccess())
                    Database.Error err = AccountInsertResult.getErrors()[0];
                    
                Opportunity objOpp1 = Utils_TestMethods.createFrameOpporutnity(acc.Id, objAgree.Id, newCountry.Id);
                oppList.add(objOpp1);
                Opportunity objOpp2 = Utils_TestMethods.createFrameOpporutnity(acc.Id, objAgree.Id, newCountry.Id);
                oppList.add(objOpp2);
                
                Database.SaveResult[] OpportunityInsertResult = Database.insert(oppList, false);
                
                for(Database.SaveResult sr: OpportunityInsertResult){               
                    if(!sr.isSuccess()){
                        Database.Error err = sr.getErrors()[0]; 
                    }
                }       
                
                
                Test.StopTest();
            }            
        }      
  
}