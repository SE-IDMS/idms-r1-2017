@isTest
private class WS_RestfulFacadeDAService_TEST {
	static testMethod void testGet() {
	  //GET Request
	    RestRequest 	objReq = new RestRequest(); 
	    RestResponse 	objRes = new RestResponse();

	    objReq.requestURI 		= '/services/apexrest/da_rest_service';
	    objReq.httpMethod 		= 'GET';
	    RestContext.request 	= objReq;
	    RestContext.response 	= objRes;

	    WS_RestfulFacadeDAService.doGet();
	    
    }

    static testMethod void testPostPOMP() {
	  //POST Request
	    RestRequest 	objReq = new RestRequest(); 
	    RestResponse 	objRes = new RestResponse();

	    objReq.requestURI 		= '/services/apexrest/da_rest_service';
	    objReq.httpMethod 		= 'POST';
	    String strRequestBody 	= '<?xml version="1.0" encoding="UTF-8" ?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><Authenticate xmlns="urn:authentication.soap.sforce.com"><username>sampleuser@sample.org</username><password>myPassword99</password><sourceIp>1.2.3.4</sourceIp></Authenticate></soapenv:Body></soapenv:Envelope>';
	    objReq.requestBody 		= Blob.valueOf(strRequestBody);
	    RestContext.request 	= objReq;
	    RestContext.response 	= objRes;

	    WS_RestfulFacadeDAService.doPost(); 

    }
    static testMethod void testPostInvensys() {
	  //POST Request
	    RestRequest 	objReq = new RestRequest(); 
	    RestResponse 	objRes = new RestResponse();

	    objReq.requestURI 		= '/services/apexrest/da_rest_service';
	    objReq.httpMethod 		= 'POST';
	    String strRequestBody 	= '<?xml version="1.0" encoding="UTF-8" ?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><Authenticate xmlns="urn:authentication.soap.sforce.com"><username>sampleuser_invensys@bridge-fo.com.test</username><password>myPassword99</password><sourceIp>1.2.3.4</sourceIp></Authenticate></soapenv:Body></soapenv:Envelope>';
	    objReq.requestBody 		= Blob.valueOf(strRequestBody);
	    RestContext.request 	= objReq;
	    RestContext.response 	= objRes;

	    WS_RestfulFacadeDAService.doPost();
    }    
}