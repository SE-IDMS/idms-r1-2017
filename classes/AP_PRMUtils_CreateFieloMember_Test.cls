@isTest
public class AP_PRMUtils_CreateFieloMember_Test{

    public static testMethod void testMethodOne(){
    User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
    
    Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
        INSERT testCountry;
    
    Account acc = new Account();
          acc.Name = 'test acc';
          insert acc;
          
        Contact con = new Contact();
            con.AccountId = acc.Id;
            con.FirstName = 'test contact Fn';
            con.LastName  = 'test contact Ln';
            con.PRMCompanyInfoSkippedAtReg__c = false;
            con.PRMCountry__c = testCountry.Id;
            con.PRMFirstName__c = 'PRM test contact Fn'; 
            con.PRMLastName__c  = 'PRM test contact Fn';
            insert con;
            
        UIMSCompany__c uims = new UIMSCompany__c();  
            uims.bFOContact__c = con.Id;
        INSERT uims;   
            
            List<Profile> profiles = [select id from profile where name='SE - Channel Partner (Community)'];
            if(profiles.size()>0){
                UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
                system.debug('portalRole is ' + portalRole);
                
                User u = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
                u.ContactId = con.Id;
                u.PRMRegistrationCompleted__c = false;
                u.BypassWF__c = false;
                u.BypassVR__c = true;
                u.FirstName ='Test User First';
                insert u;
            }
            test.starttest();
                List<Id> contactIds = new List<Id>();
                contactIds.add(con.Id);
                
                List<AP_PRMUtils_CreateFieloMember.ContactIds> contIdsLst = new List<AP_PRMUtils_CreateFieloMember.ContactIds>();
                AP_PRMUtils_CreateFieloMember.ContactIds contIds = new AP_PRMUtils_CreateFieloMember.ContactIds();
                
                contIds.lContactId = contactIds;
                System.debug('**ContactId Instance**'+contIds);
                contIdsLst.add(contIds);
                System.debug('**ContactId List**'+contIdsLst);
                AP_PRMUtils_CreateFieloMember.CreateMember(contIdsLst);
            test.stoptest();
            
            } 
    
    
    }


}