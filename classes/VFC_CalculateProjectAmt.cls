/*
 Monalisa OCT 2015
 Calculate the SE Potential Project Amount and Total Project Amount-Forecasted+Won values.
We have to take entire hierarchy opportunities either old model data 3 levels(Master-Object-Simple) or
new model data 2 levels (Object - simple) under master project. 
*/

public class VFC_CalculateProjectAmt{

    public OPP_Project__c prj= new OPP_Project__c();
    public Double sumOfAllOpportunities=0;
    public Double totalProjectAmountForecastedOpenWon=0;
    public Double sumOfOpenOpportunities=0;
    public Double sumOfCancelledOpportunities=0;
    public Double sumOfLostOpportunities=0;
    public Double sumOfWonOpportunities=0;
    public Boolean errorDisplay{get;set;}{errorDisplay=False;}
    public String clickedStage;

    public VFC_CalculateProjectAmt(ApexPages.StandardController controller) {
        //Fetch the Master Project details
        prj=[select Id,TotalProjectAmountForecastedWon__c,SumOfAllOpportunities__c,currencyIsocode,Tech_SumOfOpenOpportunities__c,Tech_SumOfCancelledOpportunities__c,Tech_SumOfLostOpportunities__c,Tech_SumOfWonOpportunities__c from OPP_Project__c where Id=:System.currentPageReference().getParameters().get('id') limit 1];
        clickedStage=System.currentPageReference().getParameters().get('clickedStage');
        System.debug('>>>>proj'+prj+'><><'+clickedStage);      
    }
    public pageReference calculateProjectAmount(){
        List<Opportunity> opplstlevel1= new List<Opportunity>();
        List<Opportunity> opplstlevel2= new List<Opportunity>();
        List<Opportunity> opplstlevel3= new List<Opportunity>();
        List<Opportunity> opplst= new List<Opportunity>();
        Map<String,Decimal> currencyMap = new Map<String,Decimal>();     
        for(CurrencyType ct:[SELECT ConversionRate,DecimalPlaces,IsActive,IsoCode FROM CurrencyType WHERE IsActive = true])            
                currencyMap.put(ct.IsoCode,ct.ConversionRate);
                
        //First Level Opportunities
        opplstlevel1=[select Amount,currencyIsocode,ForecastCategoryName,IncludedInForecast__c,ToBeDeleted__c,StageName,Status__c from Opportunity where Project__c=:prj.Id];
         
        //Second level Opportunities      
        opplstlevel2=[select Amount,currencyIsocode,ForecastCategoryName,IncludedInForecast__c,ToBeDeleted__c,StageName,Status__c from Opportunity where ParentOpportunity__c in :opplstlevel1];
            
        //Third level Opportunities      
        opplstlevel3=[select Amount,currencyIsocode,ForecastCategoryName,IncludedInForecast__c,ToBeDeleted__c,StageName,Status__c from Opportunity where ParentOpportunity__c in :opplstlevel2 and IncludedInForecast__c=:System.Label.CLOCT15SLS07 and ToBeDeleted__c=False];
              
        if(opplstlevel1.size()>0){
            for(Opportunity opp:opplstlevel1){
              if(opp.IncludedInForecast__c==System.Label.CLOCT15SLS07 && opp.ToBeDeleted__c==False)
                    opplst.add(opp);
            }
        }
        
        if(opplstlevel2.size()>0){
            for(Opportunity opp:opplstlevel2){
                if(opp.IncludedInForecast__c==System.Label.CLOCT15SLS07 && opp.ToBeDeleted__c==False)
                    opplst.add(opp);
            }
        }
        
        if(opplstlevel3.size()>0)
            opplst.addAll(opplstlevel3);
               
        for(Opportunity opp:opplst){
           if(opp.Amount == null)
             opp.Amount =0;
             sumOfAllOpportunities+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
            if(opp.StageName!=System.Label.CLOCT15SLS77)
                  totalProjectAmountForecastedOpenWon+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
           if(opp.StageName==System.Label.CLOCT15SLS77 && (opp.Status__c==System.Label.CLOCT13SLS08 || opp.Status__c==System.Label.CLOCT13SLS09)){
               System.debug('>>>>Cancelled Amount>>'+opp.Amount);
               System.debug('>>>>Opportunity currency>>'+currencyMap.get(opp.CurrencyIsoCode)+'>>>>>'+'Oppty Currency code'+opp.CurrencyIsoCode);
               sumOfCancelledOpportunities+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
          } if(opp.StageName==System.Label.CLOCT15SLS77 && (opp.Status__c==System.Label.CLSEP12SLS13 || opp.Status__c==System.Label.CLSEP12SLS14))
               sumOfLostOpportunities+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
           if(opp.StageName==System.Label.CL00220)
               sumOfWonOpportunities+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
           if(opp.StageName!=System.Label.CL00220 && opp.StageName!=System.Label.CLOCT15SLS77)
               sumOfOpenOpportunities+= (Double)(opp.Amount / currencyMap.get(opp.CurrencyIsoCode));
         }
         System.debug('>>>>><<<prj'+ sumOfCancelledOpportunities+'>>>>>'+currencyMap.get(prj.CurrencyIsoCode));
        prj.SumOfAllOpportunities__c=sumOfAllOpportunities*currencyMap.get(prj.CurrencyIsoCode);
        prj.TotalProjectAmountForecastedWon__c=totalProjectAmountForecastedOpenWon*currencyMap.get(prj.CurrencyIsoCode);
        prj.Tech_SumOfOpenOpportunities__c=sumOfOpenOpportunities*currencyMap.get(prj.CurrencyIsoCode);
        prj.Tech_SumOfCancelledOpportunities__c=sumOfCancelledOpportunities*currencyMap.get(prj.CurrencyIsoCode);
        prj.Tech_SumOfLostOpportunities__c=sumOfLostOpportunities*currencyMap.get(prj.CurrencyIsoCode);
        prj.Tech_SumOfWonOpportunities__c =sumOfWonOpportunities*currencyMap.get(prj.CurrencyIsoCode);
        prj.Tech_AmoutUpdateByBatch__c=false;
       System.debug('>>>>><<<prj'+prj);
       Database.SaveResult prjUpdateResult = Database.update(prj,false);
            if(!prjUpdateResult.isSuccess()){
                errorDisplay=True;
                Database.Error DMLerror = prjUpdateResult.getErrors()[0];
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLerror.getMessage()));
                return null;     
            }
            else{
                    errorDisplay=False;
                    PageReference prjRedirect;
                    if(clickedStage==null || clickedStage=='')
                        prjRedirect= new ApexPages.StandardController(prj).view();
                    else{
                        System.debug('><><><'+clickedStage+'><><><'+System.currentPageReference().getParameters().get('id'));
                        prjRedirect= new PageReference('/apex/VFP_ProjectBarcodeDetailView');
                        prjRedirect.getParameters().put('id',System.currentPageReference().getParameters().get('id'));
                        prjRedirect.getParameters().put('clickedStage',clickedStage);
                    }
                    prjRedirect.setRedirect(true);
                    return prjRedirect;
            }
    }
}