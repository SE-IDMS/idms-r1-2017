/* DESCRIPTION: CONTROLLER FOR VFP_RiskAssessment. DISPLAYS RISK ASSESSMENT RELATED FIELDS */
public with sharing class VFC_RiskAssessment 
{
    public CFWRiskAssessment__c rskAssessment{get;set;}
    public Map<String, CS_RiskValues__c> rskValues{get;set;}
    public String urlValue{get;set;}
    public Map<String, String> rskRecommendationValues {get;set;}   
    public Map<String, String> rskAnswerValues {get;set;}   
    public String nonExhaustivenessAnswers{get;set;}
    public string comments_section1{get;set;}
    public string comments_section2_1_1{get;set;}
    public string comments_section2_1_2{get;set;}
    public string comments_section2_2{get;set;}
    public string comments_section3_1{get;set;}
    public string comments_section4_1_1{get;set;}
    public string comments_section4_1_2{get;set;}
    public string comments_section4_1_3{get;set;}
    public string comments_section4_2_1{get;set;}
    public string comments_section4_2_2{get;set;}
    public string comments_section4_3_1{get;set;}
    public string comments_section4_3_2{get;set;}
    public string comments_section4_4{get;set;}
    public string comments_section4_5{get;set;}
    public string comments_section4_6{get;set;}
    public string comments_section5_1{get;set;}
    public string comments_section5_2{get;set;}                                                        
    public string comments_section6_1_1{get;set;}
    public string comments_section6_1_2{get;set;}                                                        
    public string comments_section6_2{get;set;}                                                            
    public string comments_section7_1{get;set;}
    public string comments_section7_2{get;set;}                                                        
    public string comments_section7_3{get;set;}
    public string comments_section7_4{get;set;}                                                        
    public string comments_section8_1{get;set;}
    public string comments_section8_2{get;set;}                                                        
    public string comments_section9_1{get;set;}
    public string comments_section9_2{get;set;}                                                        
    public boolean dispRskButtonComp{get;set;}
    public boolean dispInputFields{get;set;}
    public boolean renderRskAssessment {get;set;}
    //CONSTRUCTOR
    public VFC_RiskAssessment(ApexPages.StandardController controller) 
    {
        rskAssessment = (CFWRiskAssessment__c)controller.getRecord();
        rskAssessment = Database.query(queryAllFields());
        rskValues = CS_RiskValues__c.getAll(); 
        nonExhaustivenessAnswers = rskAssessment.NonexhaustivenessAnswers__c;
        
        if(ApexPages.currentPage()!=null && ApexPages.currentPage().getURL()!=null && ApexPages.currentPage().getURL().contains('VFC_RiskAssessment'))
            renderRskAssessment = true;
        //CHECKS IF THE LOGGED IN USER IS MEMBER OF CORE TEAM
        List<Group> groups = [select Id,Name from Group where Name=:Label.CLOCT15CFW57];
        if(groups!=null && groups.size()==1)
        {
            List<GroupMember> groupMembs = [Select Id, UserOrGroupId From GroupMember Where GroupId = :groups[0].Id and UserOrGroupId =: UserInfo.getUserId()];
            if(groupMembs!=null && groupMembs.size()==1)
                dispRskButtonComp = true;
        }
    }
    //SAVE METHOD
    public pagereference save()
    {
        Pagereference pg;
        rskAssessment.NonexhaustivenessAnswers__c = nonExhaustivenessAnswers;

        //APPENDS EXISTING COMMENTS TO THE COMMENTS FIELD OF EACH SECTION
        if(comments_section1!=null && comments_section1.length()>0 && comments_section1!='&nbsp;')
        {
           if(rskAssessment.CommentsonNonexhaustiveness__c == null)
               rskAssessment.CommentsonNonexhaustiveness__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section1+' \r\n';
           else
               rskAssessment.CommentsonNonexhaustiveness__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section1+ ' \r\n'+rskAssessment.CommentsonNonexhaustiveness__c;     
        }
        if(comments_section2_1_1!=null && comments_section2_1_1.length()>0 && comments_section2_1_1!='&nbsp;')
        {
           if(rskAssessment.CommentsonLackofOwnership1__c == null)
               rskAssessment.CommentsonLackofOwnership1__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section2_1_1+' \r\n';
           else
               rskAssessment.CommentsonLackofOwnership1__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section2_1_1+ ' \r\n'+rskAssessment.CommentsonLackofOwnership1__c;     
        }
        if(comments_section2_1_2!=null && comments_section2_1_2.length()>0 && comments_section2_1_2!='&nbsp;')
        {
           if(rskAssessment.CommentsonLackofOwnership2__c == null)
               rskAssessment.CommentsonLackofOwnership2__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section2_1_2+' \r\n';
           else
               rskAssessment.CommentsonLackofOwnership2__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section2_1_2+ ' \r\n'+rskAssessment.CommentsonLackofOwnership2__c;     
        }
        if(comments_section2_2!=null && comments_section2_2.length()>0 && comments_section2_2!='&nbsp;')
        {
           if(rskAssessment.CommentsonInadequateServiceLevel__c == null)
               rskAssessment.CommentsonInadequateServiceLevel__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section2_2+' \r\n';
           else
               rskAssessment.CommentsonInadequateServiceLevel__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section2_2+ ' \r\n'+rskAssessment.CommentsonInadequateServiceLevel__c;     
        }
        if(comments_section3_1!=null && comments_section3_1.length()>0 && comments_section3_1!='&nbsp;')
        {   
           if(rskAssessment.CommentsonLackofPersonalDataProt__c == null)
               rskAssessment.CommentsonLackofPersonalDataProt__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section3_1+' \r\n';
           else
               rskAssessment.CommentsonLackofPersonalDataProt__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section3_1+ ' \r\n'+rskAssessment.CommentsonLackofPersonalDataProt__c;     
        }
        if(comments_section4_1_1!=null && comments_section4_1_1.length()>0 && comments_section4_1_1!='&nbsp;')
        {   
           if(rskAssessment.CommentsonCloudrelatedSecrisk1__c == null)
               rskAssessment.CommentsonCloudrelatedSecrisk1__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_1_1+' \r\n';
           else
               rskAssessment.CommentsonCloudrelatedSecrisk1__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_1_1+ ' \r\n'+rskAssessment.CommentsonCloudrelatedSecrisk1__c;     
        }
        if(comments_section4_1_2!=null && comments_section4_1_2.length()>0 && comments_section4_1_2!='&nbsp;')
        {   
           if(rskAssessment.CommentsonCloudrelatedSecrisk2__c == null)
               rskAssessment.CommentsonCloudrelatedSecrisk2__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_1_2+' \r\n';
           else
               rskAssessment.CommentsonCloudrelatedSecrisk2__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_1_2+ ' \r\n'+rskAssessment.CommentsonCloudrelatedSecrisk2__c;     
        }
        if(comments_section4_1_3!=null && comments_section4_1_3.length()>0 && comments_section4_1_3!='&nbsp;')
        {   
           if(rskAssessment.CommentsonCloudrelatedSecrisk3__c == null)
               rskAssessment.CommentsonCloudrelatedSecrisk3__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_1_3+' \r\n';
           else
               rskAssessment.CommentsonCloudrelatedSecrisk3__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_1_3+ ' \r\n'+rskAssessment.CommentsonCloudrelatedSecrisk3__c;     
        }
        if(comments_section4_2_1!=null && comments_section4_2_1.length()>0 && comments_section4_2_1!='&nbsp;')
        {   
           if(rskAssessment.CommentsonInternalHumanRisks1__c == null)
               rskAssessment.CommentsonInternalHumanRisks1__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_2_1+' \r\n';
           else
               rskAssessment.CommentsonInternalHumanRisks1__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_2_1+ ' \r\n'+rskAssessment.CommentsonInternalHumanRisks1__c;     
        }
        if(comments_section4_2_2!=null && comments_section4_2_2.length()>0 && comments_section4_2_2!='&nbsp;')
        {   
           if(rskAssessment.CommentsonInternalHumanRisks2__c == null)
               rskAssessment.CommentsonInternalHumanRisks2__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_2_2+' \r\n';
           else
               rskAssessment.CommentsonInternalHumanRisks2__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_2_2+ ' \r\n'+rskAssessment.CommentsonInternalHumanRisks2__c;     
        }
        if(comments_section4_3_1!=null && comments_section4_3_1.length()>0 && comments_section4_3_1!='&nbsp;')
        {   
           if(rskAssessment.CommentsonExternalHumanRisks1__c == null)
               rskAssessment.CommentsonExternalHumanRisks1__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_3_1+' \r\n';
           else
               rskAssessment.CommentsonExternalHumanRisks1__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_3_1+ ' \r\n'+rskAssessment.CommentsonExternalHumanRisks1__c;     
        }
        if(comments_section4_3_2!=null && comments_section4_3_2.length()>0 && comments_section4_3_2!='&nbsp;')
        {   
           if(rskAssessment.CommentsonExternalHumanRisks2__c == null)
               rskAssessment.CommentsonExternalHumanRisks2__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_3_2+' \r\n';
           else
               rskAssessment.CommentsonExternalHumanRisks2__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_3_2+ ' \r\n'+rskAssessment.CommentsonExternalHumanRisks2__c;     
        }
        if(comments_section4_4!=null && comments_section4_4.length()>0 && comments_section4_4!='&nbsp;')
        {   
           if(rskAssessment.CommentsonApplicativeRisks__c == null)
               rskAssessment.CommentsonApplicativeRisks__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_4+' \r\n';
           else
               rskAssessment.CommentsonApplicativeRisks__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_4+ ' \r\n'+rskAssessment.CommentsonApplicativeRisks__c;     
        }
        if(comments_section4_5!=null && comments_section4_5.length()>0 && comments_section4_5!='&nbsp;')
        {   
           if(rskAssessment.CommentsonMobileEnterpriseRisks__c == null)
               rskAssessment.CommentsonMobileEnterpriseRisks__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_5+' \r\n';
           else
               rskAssessment.CommentsonMobileEnterpriseRisks__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_5+ ' \r\n'+rskAssessment.CommentsonMobileEnterpriseRisks__c;     
        }
        if(comments_section4_6!=null && comments_section4_6.length()>0 && comments_section4_6!='&nbsp;')
        {   
           if(rskAssessment.CommentsonAdditionalSecurityReqs__c == null)
               rskAssessment.CommentsonAdditionalSecurityReqs__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_6+' \r\n';
           else
               rskAssessment.CommentsonAdditionalSecurityReqs__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section4_6+ ' \r\n'+rskAssessment.CommentsonAdditionalSecurityReqs__c;     
        }
        if(comments_section5_1!=null && comments_section5_1.length()>0 && comments_section5_1!='&nbsp;')
        {   
           if(rskAssessment.CommentsonPerformanceIssuesonApps__c == null)
               rskAssessment.CommentsonPerformanceIssuesonApps__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section5_1+' \r\n';
           else
               rskAssessment.CommentsonPerformanceIssuesonApps__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section5_1+ ' \r\n'+rskAssessment.CommentsonPerformanceIssuesonApps__c;     
        }
        if(comments_section5_2!=null && comments_section5_2.length()>0 && comments_section5_2!='&nbsp;')
        {   
           if(rskAssessment.CommentsonSaturationofNetwork__c == null)
               rskAssessment.CommentsonSaturationofNetwork__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section5_2+' \r\n';
           else
               rskAssessment.CommentsonSaturationofNetwork__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section5_2+ ' \r\n'+rskAssessment.CommentsonSaturationofNetwork__c;     
        }
        if(comments_section6_1_1!=null && comments_section6_1_1.length()>0 && comments_section6_1_1!='&nbsp;')
        {   
           if(rskAssessment.CommentsonLackofAlignment1__c == null)
               rskAssessment.CommentsonLackofAlignment1__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section6_1_1+' \r\n';
           else
               rskAssessment.CommentsonLackofAlignment1__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section6_1_1+ ' \r\n'+rskAssessment.CommentsonLackofAlignment1__c;     
        }
        if(comments_section6_1_2!=null && comments_section6_1_2.length()>0 && comments_section6_1_2!='&nbsp;')
        {   
           if(rskAssessment.CommentsonLackofAlignment2__c == null)
               rskAssessment.CommentsonLackofAlignment2__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section6_1_2+' \r\n';
           else
               rskAssessment.CommentsonLackofAlignment2__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section6_1_2+ ' \r\n'+rskAssessment.CommentsonLackofAlignment2__c;     
        }
        if(comments_section6_2!=null && comments_section6_2.length()>0 && comments_section6_2!='&nbsp;')
        {   
           if(rskAssessment.CommentsonIPOAlignment__c == null)
               rskAssessment.CommentsonIPOAlignment__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section6_2+' \r\n';
           else
               rskAssessment.CommentsonIPOAlignment__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section6_2+ ' \r\n'+rskAssessment.CommentsonIPOAlignment__c;     
        }
        if(comments_section7_1!=null && comments_section7_1.length()>0 && comments_section7_1!='&nbsp;')
        {   
           if(rskAssessment.CommentsonInadequateuseofBridge__c == null)
               rskAssessment.CommentsonInadequateuseofBridge__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section7_1+' \r\n';
           else
               rskAssessment.CommentsonInadequateuseofBridge__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section7_1+ ' \r\n'+rskAssessment.CommentsonInadequateuseofBridge__c;     
        }
        if(comments_section7_2!=null && comments_section7_2.length()>0 && comments_section7_2!='&nbsp;')
        {   
           if(rskAssessment.CommentsonInadequateuseofCordys__c == null)
               rskAssessment.CommentsonInadequateuseofCordys__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section7_2+' \r\n';
           else
               rskAssessment.CommentsonInadequateuseofCordys__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section7_2+ ' \r\n'+rskAssessment.CommentsonInadequateuseofCordys__c;     
        }
        if(comments_section7_3!=null && comments_section7_3.length()>0 && comments_section7_3!='&nbsp;')
        {   
           if(rskAssessment.CommentsonInadequateuseofbFO__c == null)
               rskAssessment.CommentsonInadequateuseofbFO__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section7_3+' \r\n';
           else
               rskAssessment.CommentsonInadequateuseofbFO__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section7_3+ ' \r\n'+rskAssessment.CommentsonInadequateuseofbFO__c;     
        }
        if(comments_section7_4!=null && comments_section7_4.length()>0 && comments_section7_4!='&nbsp;')
        {   
           if(rskAssessment.CommentsonInadequateuseoflocalInfr__c == null)
               rskAssessment.CommentsonInadequateuseoflocalInfr__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section7_4+' \r\n';
           else
               rskAssessment.CommentsonInadequateuseoflocalInfr__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section7_4+ ' \r\n'+rskAssessment.CommentsonInadequateuseoflocalInfr__c;     
        }
        if(comments_section8_1!=null && comments_section8_1.length()>0 && comments_section8_1!='&nbsp;')
        {   
           if(rskAssessment.CommentsonInabilitytoadequatelySE__c == null)
               rskAssessment.CommentsonInabilitytoadequatelySE__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section8_1+' \r\n';
           else
               rskAssessment.CommentsonInabilitytoadequatelySE__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section8_1+ ' \r\n'+rskAssessment.CommentsonInabilitytoadequatelySE__c;     
        }
        if(comments_section8_2!=null && comments_section8_2.length()>0 && comments_section8_2!='&nbsp;')
        {   
           if(rskAssessment.CommentsonMobiledatastorage__c == null)
               rskAssessment.CommentsonMobiledatastorage__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section8_2+' \r\n';
           else
               rskAssessment.CommentsonMobiledatastorage__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section8_2+ ' \r\n'+rskAssessment.CommentsonMobiledatastorage__c;     
        }
        if(comments_section9_1!=null && comments_section9_1.length()>0 && comments_section9_1!='&nbsp;')
        {   
           if(rskAssessment.CommentsonMobileCoding__c == null)
               rskAssessment.CommentsonMobileCoding__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section9_1+' \r\n';
           else
               rskAssessment.CommentsonMobileCoding__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section9_1+ ' \r\n'+rskAssessment.CommentsonMobileCoding__c;     
        }
        if(comments_section9_2!=null && comments_section9_2.length()>0 && comments_section9_2!='&nbsp;')
        {   
           if(rskAssessment.CommentsonEnterpriseMobility__c == null)
               rskAssessment.CommentsonEnterpriseMobility__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section9_2+' \r\n';
           else
               rskAssessment.CommentsonEnterpriseMobility__c = UserInfo.getName()+','+DateTime.now().format('dd/MM/yyyy HH:mm:ss') +': '+ comments_section9_2+ ' \r\n'+rskAssessment.CommentsonEnterpriseMobility__c;     
        }
        Database.SaveResult results = database.update(rskAssessment,false);
        if(!(results.issuccess()))
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,results.getErrors()[0].getMessage());
            ApexPages.addMessage(myMsg);
            pg = null;
        }    
        else
        {    
            pg = new pagereference('/apex/VFP_RiskAssessment?id='+rskAssessment.Id);
            pg.setRedirect(true);            
        }
        return pg;
    }
    //QUERY ALL RISK ASSESSMENT FIELDS
    public String queryAllFields()
    {
        Schema.DescribeSObjectResult descRes =  CFWRiskAssessment__c.sObjectType.getDescribe();
        List<Schema.SObjectField> tempFields = descRes.fields.getMap().values();
        List<Schema.DescribeFieldResult> fields  = new List<Schema.DescribeFieldResult>();
        for(Schema.SObjectField sof : tempFields)
        {
            fields.add(sof.getDescribe());
        } 
        String query = 'select ';
        for(Schema.DescribeFieldResult dfr : fields)
        {
            query = query + dfr.getName() + ',';
        }
        query = query.subString(0,query.length() - 1);
        query = query + ' from ';
        query = query + descRes.getName();
        query = query +  ' where Id = \'';
        query = query + rskAssessment.Id + '\'';
        system.debug('Build Query == ' + query);
        return query;
    }

    //UPDATES RISK ASSESSMENT STATUS TO COMPLETED
    public pagereference completeRiskAssessment()
    {
        Pagereference pg;
        rskAssessment.RiskAssessmentStatus__c = Label.CLOCT15CFW53;
        Database.SaveResult results = database.update(rskAssessment,false);
        if(!(results.issuccess()))
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,results.getErrors()[0].getMessage());
            ApexPages.addMessage(myMsg);
            pg = null;
        }    
        else    
        {    
            pg = new pagereference('/apex/VFP_RiskAssessment?id='+rskAssessment.Id);
            pg.setRedirect(true);            
        }
        return pg;
    }
    
    //RENDERS INPUT FEILDS
    public pagereference edit()
    {    
        dispInputFields = true;
        return null;    
    }
    
    //CANCEL BUTTON ACTION
    public pagereference Cancel()
    {
        pagereference pg = new pagereference('/apex/VFP_RiskAssessment?id='+rskAssessment.Id);        
        pg.setRedirect(true);            
        return pg;
    }
    
}