@istest
public class VFC_ExternalInfoDashboardController_Test {
    @testSetup static void testSetupMethodPRMAccountExtension() {
        List<CS_PRM_ApexJobSettings__c> jobSetList = new List<CS_PRM_ApexJobSettings__c>();
        CS_PRM_ApexJobSettings__c jobSet1= new CS_PRM_ApexJobSettings__c(); 
        jobSet1.name='PRMAccountFieldList1';
        jobSet1.InitialSync__c=True;
        jobSet1.QueryFields__c='Id,PRMCompanyName__c,PRMAccount__c,PRMCompanyPhone__c,PRMCorporateHeadquarters__c,PRMStreet__c,PRMIncludeCompanyInfoInSearchResults__c,PRMAdditionalAddress__c,PRMBusinessType__c,PRMCity__c,PRMAreaOfFocus__c,PRMCountry__c,PRMMarketServed__c,';
        jobSetList.add(jobSet1);
        CS_PRM_ApexJobSettings__c jobSet2= new CS_PRM_ApexJobSettings__c(); 
        jobSet2.name ='PRMAccountFieldList2';
        jobSet2.InitialSync__c=True;
        jobSet2.QueryFields__c='PRMStateProvince__c,PRMTaxId__c,PRMZipCode__c,PRMEmployeeSize__c,PRMSEAccountNumber__c,PRMCurrencyIsoCode__c,PRMPreferredDistributor1__c,PRMAccountRegistrationStatus__c,PRMPreferredDistributor2__c,PRMReasonForDecline__c,PRMPreferredDistributor3__c,';
        jobSetList.add(jobSet2);
        CS_PRM_ApexJobSettings__c jobSet3= new CS_PRM_ApexJobSettings__c(); 
        jobSet3.name='PRMAccountFieldList3';
        jobSet3.InitialSync__c=True;
        jobSet3.QueryFields__c='PRMAdditionalComments__c,PRMPreferredDistributor4__c,PRMRegistrationActivationDate__c,PRMUIMSID__c,PRMOrigin__c,PRMUIMSSEAccountId__c,PRMTermsAndConditionsAccepted__c,PRMIsActiveAccount__c,PRMIsEnrolledAccount__c,PRMAccountProfileComplete__c,';
        jobSetList.add(jobSet3);
        CS_PRM_ApexJobSettings__c jobSet4= new CS_PRM_ApexJobSettings__c(); 
        jobSet4.name='PRMAccountFieldList4';
        jobSet4.InitialSync__c=True;
        jobSet4.QueryFields__c='PRMExcludeFromReports__c,PRMAnnualSales__c,PRMRegistrationReviewedRejectedDate__c,PLCompanyName__c,PLMainContactFirstName__c,PLMainContactLastName__c,PLMainContactPhone__c,PLMainContactEmail__c,PLWebsite__c,PLCompanyDescription__c,PLStreet__c,';
        jobSetList.add(jobSet4);

        CS_PRM_ApexJobSettings__c jobSet5= new CS_PRM_ApexJobSettings__c(); 
        jobSet5.name='PRMAccountFieldList5';
        jobSet5.InitialSync__c=True;
        jobSet5.QueryFields__c='PLAdditionalAddress__c,PLStateProvince__c,PLCity__c,PLCountry__c,PLZipCode__c,PLShowInPartnerLocator__c,PLBusinessType__c,PLAreaOfFocus__c,PLMarketServed__c,PLPartnerLocatorFeatureGranted__c,PLShowInPartnerLocatorForced__c,PRMDomainsOfExpertise__c,';
        jobSetList.add(jobSet5);

        CS_PRM_ApexJobSettings__c jobSet6= new CS_PRM_ApexJobSettings__c(); 
        jobSet6.name='PRMAccountFieldList6';
        jobSet6.InitialSync__c=True;
        jobSet6.QueryFields__c='IsPersonAccount,PRMOriginSource__c,PRMOriginSourceInfo__c,PRMAccountCompletedData__c,PLDomainsOfExpertise__c';
        jobSetList.add(jobSet6);

        CS_PRM_ApexJobSettings__c jobSet7= new CS_PRM_ApexJobSettings__c(); 
        jobSet7.name='PRMAccountFieldList7';
        jobSet7.InitialSync__c=True;
        jobSet7.QueryFields__c=',AdditionalAddress__c,AnnualSales__c,ClassLevel2__c,ClassLevel1__c,City__c,Name,Phone,CorporateHeadquarters__c,Country__c,CurrencyIsoCode,EmployeeSize__c,MarketServed__c,SEAccountID__c,StateProvince__c,Street__c,ZipCode__c,PRMAccountCompletedProfile__c';
        jobSetList.add(jobSet7);

        CS_PRM_ApexJobSettings__c jobSet8= new CS_PRM_ApexJobSettings__c(); 
        jobSet8.name='PRMContactFieldList1';
        jobSet8.InitialSync__c=True;
        jobSet8.QueryFields__c='Id,PRMFirstName__c,PRMLastName__c,PRMEmail__c,PRMJobTitle__c,PRMJobFunc__c,PRMWorkPhone__c,PRMMobilePhone__c,PRMCountry__c,PRMCustomerClassificationLevel1__c,PRMCustomerClassificationLevel2__c,PRMOrigin__c,PRMLastLoginDate__c,PRMTaxId__c,PRMContact__c,';
        jobSetList.add(jobSet8);

        CS_PRM_ApexJobSettings__c jobSet9= new CS_PRM_ApexJobSettings__c(); 
        jobSet9.name='PRMContactFieldList2';
        jobSet9.InitialSync__c=True;
        jobSet9.QueryFields__c='PRMPrimaryContact__c,PRMUser__c,IsMasterSalesContact__c,PRMContactRegistrationStatus__c,PRMReasonForDecline__c,PRMRegistrationActivationDate__c,PRMSecondary1ClassificationLevel1__c,PRMSecondary2ClassificationLevel1__c,';
        jobSetList.add(jobSet9);
        
        CS_PRM_ApexJobSettings__c jobSet10 = new CS_PRM_ApexJobSettings__c(); 
        jobSet10.name='PRMContactFieldList3';
        jobSet10.InitialSync__c=True;
        jobSet10.QueryFields__c='PRMSecondary3ClassificationLevel1__c,PRMSecondary1ClassificationLevel2__c,PRMSecondary2ClassificationLevel2__c,PRMSecondary3ClassificationLevel2__c,PRMExcludeFromReports__c,PRMIsActiveContact__c,PRMUIMSID__c,AccountId,';
        jobSetList.add(jobSet10);

        CS_PRM_ApexJobSettings__c jobSet11= new CS_PRM_ApexJobSettings__c(); 
        jobSet11.Name='PRMContactFieldList4';
        jobSet11.InitialSync__c=True;
        jobSet11.QueryFields__c='PRMRegistrationReviewedRejectedDate__c,PRMApplicationRequested__c,PRMIsEnrolledContact__c,PRMOriginSource__c,PRMOriginSourceInfo__c,FieloEE__Member__c,PRMUIMSSEContactId__c,PRMDoNotHaveCompany__c,PRMOnly__c,FieloEE__Member__r.Name,FirstName,LastName,Email';
        jobSetList.add(jobSet11);
        
        CS_PRM_ApexJobSettings__c jobSet12= new CS_PRM_ApexJobSettings__c(); 
        jobSet12.Name='PAGESIZE_INTERNALUSER';
        jobSet12.QueryFields__c='5';
        jobSetList.add(jobSet12);
        
        insert jobSetList;
        
        Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
        INSERT testCountry;
        StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
        INSERT testStateProvince; 
        CS_PRM_NoCompanyHoldingAccount__c csDummyAcc = new CS_PRM_NoCompanyHoldingAccount__c();
            csDummyAcc.Name = 'DummyAccountInfo';
            csDummyAcc.AccountId__c = '00117000008vEH0AAM';
            csDummyAcc.ContactCount__c = 48;
        INSERT csDummyAcc;
        CS_ContactLanguageToUserLanguage__c csContactLang = new CS_ContactLanguageToUserLanguage__c(); 
            csContactLang.Name = 'EN';
            csContactLang.UserLanguageKey__c = 'en_US';
        INSERT csContactLang;
        
        PRMCountry__c countryCluster = new PRMCountry__c();
            countryCluster.Country__c = testCountry.Id;
            countryCluster.CountryPortalEnabled__c = True; 
            countryCluster.PLDatapool__c = 'IN_en2';
            countryCluster.SupportedLanguage1__c = 'en_US';
            countryCluster.SupportedLanguage2__c = 'hi';
            countryCluster.SupportedLanguage3__c = 'en_IN';
        INSERT countryCluster;
        
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
            insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI3', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
            insert cLChild;
        CountryChannels__c cChannels = new CountryChannels__c();
            cChannels.Active__c = true; cChannels.AllowPrimaryToManage__c = True; cChannels.Channel__c = cLC.Id; cChannels.SubChannel__c = cLChild.id;
            cChannels.Country__c = testCountry.id; cChannels.CompleteUserRegistrationonFirstLogin__c = true; 
            cChannels.PRMCountry__c = countryCluster.Id;
        insert cChannels;
        
        Account acc = new Account();
            acc.Name = 'TestAccount';
            //acc.RecordTypeId = System.Label.CLAPR15PRM025;
            acc.PRMCompanyName__c = 'Schneider';
            acc.PRMStreet__c      = 'Elnath Street';
            acc.PRMCompanyPhone__c= '987654321'; 
            acc.PRMAdditionalAddress__c = 'Marathalli';
            acc.PRMWebsite__c           = 'https://schneider-electric.com';
            acc.PRMCity__c              = 'Bangalore';
            //acc.PRMTaxId__c            
            acc.PRMStateProvince__c     = testStateProvince.Id; 
            acc.PRMCorporateHeadquarters__c = True;
            acc.PRMCountry__c               = testCountry.Id;
            acc.PRMIncludeCompanyInfoInSearchResults__c = True;
            acc.PRMZipCode__c                           = '560103';
            acc.PRMUIMSID__c = '788uuiyhhtgr68';
            acc.PRMBusinessType__c = 'FI';
            acc.PRMAreaOfFocus__c = 'FI3';
            acc.PLMarketServed__c = 'BD6;PW3';
            acc.PRMAccount__c = true;
        INSERT acc;
        Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.Id);
        Insert opp;
        Assessment__c ass = new Assessment__c(
                            Active__c = true, AutomaticAssignment__c = false, CountryChannels__c = cChannels.Id, 
                            Description__c = 'This is Test', LeadingBusiness__c = 'ID', MaximumPossibleQualification__c = 4, 
                            MaximumScore__c = 100, RecordTypeId = '01212000000oF4Z');//, PartnerProgram__c = , ProgramLevel__c = , );
        insert ass;
        
        OpportunityAssessment__c oppAss = new OpportunityAssessment__c(
                            Assessment__c = ass.Id, AssessmentScore__c = 100, Opportunity__c = opp.Id, ORFAssessment__c = true);
        insert oppAss;
        
        List<OpportunityAssessmentQuestion__c> oppAssQueList = new List<OpportunityAssessmentQuestion__c>();
        OpportunityAssessmentQuestion__c PicklistOppAss = new OpportunityAssessmentQuestion__c(AnswerType__c = 'Picklist', Assessment__c = ass.Id, Sequence__c = 1, Weightage__c = 25);
        oppAssQueList.add(PicklistOppAss);
        OpportunityAssessmentQuestion__c TextOppAss = new OpportunityAssessmentQuestion__c(AnswerType__c = 'Text', Assessment__c = ass.Id, Sequence__c = 1, Weightage__c = 25);
        oppAssQueList.add(TextOppAss);
        OpportunityAssessmentQuestion__c MultiSelectOppAss = new OpportunityAssessmentQuestion__c(AnswerType__c = 'MultiSelect', Assessment__c = ass.Id, Sequence__c = 1, Weightage__c = 25);
        oppAssQueList.add(MultiSelectOppAss);
        OpportunityAssessmentQuestion__c RadioOppAss = new OpportunityAssessmentQuestion__c(AnswerType__c = 'Radio Button', Assessment__c = ass.Id, Sequence__c = 1, Weightage__c = 25);
        oppAssQueList.add(RadioOppAss);
        insert oppAssQueList;
        
        List<OpportunityAssessmentAnswer__c> oppAssAnsList = new List<OpportunityAssessmentAnswer__c>();
        OpportunityAssessmentAnswer__c oppAssAnspick = new OpportunityAssessmentAnswer__c(AnswerOption__c = 'A;B;C', Question__c = PicklistOppAss.Id);
        oppAssAnsList.add(oppAssAnspick);
        OpportunityAssessmentAnswer__c oppAssAnsMult = new OpportunityAssessmentAnswer__c(AnswerOption__c = 'X;Y;Z', Question__c = MultiSelectOppAss.Id);
        oppAssAnsList.add(oppAssAnsMult);
        insert oppAssAnsList;
        
        List<OpportunityAssessmentResponse__c> oppAssResList = new List<OpportunityAssessmentResponse__c>();
        oppAssResList.add( new OpportunityAssessmentResponse__c(Account__c = acc.Id, Answer__c = oppAssAnspick.Id, OpportunityAssessment__c = oppAss.Id, Question__c = PicklistOppAss.Id));
        oppAssResList.add( new OpportunityAssessmentResponse__c(Account__c = acc.Id, Answer__c = oppAssAnsMult.Id, OpportunityAssessment__c = oppAss.Id, Question__c = MultiSelectOppAss.Id));
        oppAssResList.add( new OpportunityAssessmentResponse__c(Account__c = acc.Id, OpportunityAssessment__c = oppAss.Id, Question__c = TextOppAss.Id, ResponseText__c = 'This is test'));
        oppAssResList.add( new OpportunityAssessmentResponse__c(Account__c = acc.Id, OpportunityAssessment__c = oppAss.Id, Question__c = RadioOppAss.Id, ResponseText__c = 'This is test1'));
        insert oppAssResList;
        System.debug('*** '+acc.Id);
        List<Contact> conList = new List<Contact>();
        Contact con1 = new Contact();
            con1.FirstName = 'FirstName TestClass1';
            con1.LastName  = 'LastName TestClass1';
            //con1.PRMFirstName__c = 'FirstName TestClass1';
            //con1.PRMLastName__c  = 'LastName TestClass1';
            con1.PRMCustomerClassificationLevel1__c  = 'FI';
            con1.PRMCustomerClassificationLevel2__c  = 'FI3';
            con1.Email = 'testclass121@test.com';    
            //con.PRMJobFunc__c = 
            con1.PRMJobTitle__c = 'Z3';
            con1.AccountId            = acc.Id;
            con1.PRMPrimaryContact__c = true;
            con1.PRMContactRegistrationStatus__c = 'Validated';
            con1.PRMUIMSID__c = '98uiy786tyur7uyhj';
        conList.add(con1);
        Contact con = new Contact();
            con.FirstName = 'FirstName TestClass';
            con.LastName  = 'LastName TestClass';
            con.PRMFirstName__c = 'FirstName TestClass';
            con.PRMLastName__c  = 'LastName TestClass';
            con.PRMCustomerClassificationLevel1__c  = 'FI';
            con.PRMCustomerClassificationLevel2__c  = 'FI3';
            con.PRMEmail__c          = 'testclass1@test.com';    
            //con.PRMJobFunc__c = 
            con.PRMJobTitle__c = 'Z3';
            con.AccountId            = acc.Id;
            con.PRMPrimaryContact__c = false;
            con.PRMContactRegistrationStatus__c = 'Validated';
        conList.add(con);
        Contact con2 = new Contact();
            con2.FirstName = 'FirstName TestClass2';
            con2.LastName  = 'LastName TestClass2';
            //con1.PRMFirstName__c = 'FirstName TestClass1';
            //con1.PRMLastName__c  = 'LastName TestClass1';
            con2.PRMCustomerClassificationLevel1__c  = 'FI';
            con2.PRMCustomerClassificationLevel2__c  = 'FI3';
            con2.Email = 'testclass123@test.com';    
            //con.PRMJobFunc__c = 
            con2.PRMJobTitle__c = 'Z3';
            con2.AccountId            = acc.Id;
            con2.PRMPrimaryContact__c = false;
            con2.PRMContactRegistrationStatus__c = 'Validated';
            //con1.PRMUIMSID__c = '98uiy786tyur7uyhj';
        conList.add(con2);
        
        insert conList;
        
        System.debug('*** '+con.Id);
        Id profilesId = [select id from profile where name='SE - Channel Partner (Community)'].Id;      
        User   u1= new User(Username = 'testUserOne@schneider-electric.com'+Label.CLMAR13PRM05, LastName = 'User11', alias = 'tuser1',
                            CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                            Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                            PRMTemporarySamlToken__c = 'ASDHFTWEEXD121212', 
                            LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId,ContactID = con.Id, UserPermissionsSFContentUser=true, FederationIdentifier = '897dheuu37hsysh3sh3');        
        insert u1;
        User   u2= new User(Username = 'testUsertwo@schneider-electric.com'+Label.CLMAR13PRM05, LastName = 'User12', alias = 'tuser2',
                            CommunityNickName = 'testUser2', TimeZoneSidKey = 'America/Chicago', 
                            Email = 'testUsertwo@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                            PRMTemporarySamlToken__c = 'ASDHF897ui78657yXD121212', 
                            LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId,ContactID = con1.Id, UserPermissionsSFContentUser=true, FederationIdentifier = '98uiy786tyur7uyhj');        
        insert u2;
    }
    static testMethod void  ManageAccountExternalInfo_Test(){
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        System.runAs(u){
            PageReference pageRef = Page.VFP_ManageAccountExternalInfo;
            Test.setCurrentPage(pageRef);
            Account accnt = new Account();
            accnt = [Select Id From Account Where Name = 'TestAccount'];
            ApexPages.currentPage().getParameters().put('id',accnt.id);
            ApexPages.currentPage().getParameters().put('tab','company');
            ApexPages.currentPage().getParameters().put('prefill','true');
            VFC_ManageAccountExternalInfo thisManageAccExt = New VFC_ManageAccountExternalInfo();
            Boolean testPrefill = thisManageAccExt.Prefill;
            Boolean testIsDefaultAccount = thisManageAccExt.IsDefaultAccount;
            thisManageAccExt.UpdateChannelInfo();
            thisManageAccExt.UpdateLocatorListing();
            thisManageAccExt.DoCancel();
            thisManageAccExt.DoCancelLocator();
            thisManageAccExt.NewPrimaryContactId = [SELECT Id FROM Contact WHERE FirstName = 'FirstName TestClass' AND LastName  = 'LastName TestClass'].Id;
            thisManageAccExt.UpdatePrimaryContact();
            thisManageAccExt.DashboardInfo.AccountInfo.PLMarketServed__c = 'IT2;BD3';
            thisManageAccExt.Save();
            thisManageAccExt.DoNewRegistration();
        }
    }
    static testMethod void  ManageContactExternalInfo_Test(){
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        System.runAs(u){
            Test.startTest();
            PageReference pageRef = Page.VFP_ManageContactExternalInfo;
            Test.setCurrentPage(pageRef);
            Contact Con = [SELECT ID,PRMUIMSID__c FROM Contact WHERE FirstName = 'FirstName TestClass1' AND LastName  = 'LastName TestClass1'];
            System.debug('*** Fedid '+Con.PRMUIMSID__c);
            ApexPages.currentPage().getParameters().put('id',Con.id);
            ApexPages.currentPage().getParameters().put('tab','contact');
            //ApexPages.currentPage().getParameters().put('type','reset');
            VFC_ManageContactExternalInfo thisManageConExt = New VFC_ManageContactExternalInfo();
            String imsTestId = thisManageConExt.IMSApplicationID;
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
                thisManageConExt.ResetPassword();
                thisManageConExt.DoCancelContact();
            Test.stopTest();
        }
    }
    static testMethod void  ManageContactExternalInfo_Test2(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                     Email='test@schneider-electric.com', 
                     EmailEncodingKey='UTF-8',
                     FirstName ='Test First',      
                     LastName='Testing',
                     CommunityNickname='CommunityName123',      
                     LanguageLocaleKey='en_US', 
                     LocaleSidKey='en_US', 
                     ProfileId = p.Id,
                     TimeZoneSidKey='America/Los_Angeles', 
                     UserName='subhashish@test1.com',
                     userroleid = r.Id,
                     isActive = True,                       
                     BypassVR__c = True);
        System.runAs(u){
            Test.startTest();
            PageReference pageRef = Page.VFP_ManageContactExternalInfo;
            Test.setCurrentPage(pageRef);
            Contact Con = [SELECT ID,PRMUIMSID__c FROM Contact WHERE FirstName = 'FirstName TestClass2' AND LastName  = 'LastName TestClass2'];
            System.debug('*** Fedid '+Con.PRMUIMSID__c);
            ApexPages.currentPage().getParameters().put('id',Con.id);
            ApexPages.currentPage().getParameters().put('tab','contact');
            //ApexPages.currentPage().getParameters().put('type','reset');
            VFC_ManageContactExternalInfo thisManageConExt = New VFC_ManageContactExternalInfo();
            String imsTestId = thisManageConExt.IMSApplicationID;
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
                thisManageConExt.ResetPassword();
            Test.stopTest();
        }
    }
    static testMethod void  ManageContactExternalInfo_Test3(){
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        System.runAs(u){
            Test.startTest();
            PageReference pageRef = Page.VFP_ManageContactExternalInfo;
            Test.setCurrentPage(pageRef);
            Contact Con = [SELECT ID,PRMUIMSID__c FROM Contact WHERE FirstName = 'FirstName TestClass1' AND LastName  = 'LastName TestClass1'];
            System.debug('*** Fedid '+Con.PRMUIMSID__c);
            ApexPages.currentPage().getParameters().put('id',Con.id);
            ApexPages.currentPage().getParameters().put('tab','contact');
            VFC_ManageContactExternalInfo thisManageConExt = New VFC_ManageContactExternalInfo();
            String imsTestId = thisManageConExt.IMSApplicationID;
            Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
                thisManageConExt.doSave();
            Test.stopTest();
        }
    }
    static testMethod void  ManageContactExternalInfo_Test4(){
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        System.runAs(u){
            Test.startTest();
            PageReference pageRef = Page.VFP_ManageContactExternalInfo;
            Test.setCurrentPage(pageRef);
            Contact Con = [SELECT ID,PRMUIMSID__c FROM Contact WHERE FirstName = 'FirstName TestClass1' AND LastName  = 'LastName TestClass1'];
            System.debug('*** Fedid '+Con.PRMUIMSID__c);
            ApexPages.currentPage().getParameters().put('id',Con.id);
            ApexPages.currentPage().getParameters().put('tab','contact');
            ApexPages.currentPage().getParameters().put('type','reset');
            Test.setMock(WebServiceMock.class, new WS_MockClassPRMPrivateRegistration_Test());
                VFC_ManageContactExternalInfo thisManageConExt = New VFC_ManageContactExternalInfo();
            Test.stopTest();
        }
    }
    static testMethod void  ManageMemberExternalInfo_Test(){
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        System.runAs(u){
            Test.startTest();
            PageReference pageRef = Page.VFP_ManageMemberExternalInfo;
            Test.setCurrentPage(pageRef);
            Contact Con = [SELECT ID,PRMUIMSID__c FROM Contact WHERE FirstName = 'FirstName TestClass1' AND LastName  = 'LastName TestClass1'];
            ApexPages.currentPage().getParameters().put('id',Con.id);
            ApexPages.currentPage().getParameters().put('tab','member');
            VFC_ManageMemberExternalInfo thisManageMemExt = New VFC_ManageMemberExternalInfo();
            String ReturnToTabTest = thisManageMemExt.ReturnToTab;
            thisManageMemExt.doSave();
            //thisManageMemExt.GetFullUrl('member');
            Test.stopTest();
        }
    }
    static testMethod void  ExternalInfoDashboardController_Test(){
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
            User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        System.runAs(u){
            Test.startTest();
            PageReference pageRef = Page.VFP_ExternalInfoDashboardTemplate;
            Test.setCurrentPage(pageRef);
            Contact Con = [SELECT ID,PRMUIMSID__c,Country__c FROM Contact WHERE FirstName = 'FirstName TestClass1' AND LastName  = 'LastName TestClass1'];
            ApexPages.currentPage().getParameters().put('id',Con.id);
            ApexPages.currentPage().getParameters().put('tab','contact');
            VFC_ExternalInfoDashboardController thisExtInfoCon = New VFC_ExternalInfoDashboardController();
            thisExtInfoCon.BusinessTypeList = new List<SelectOption>();
            thisExtInfoCon.AreaOfFocusList = new List<SelectOption>();
            thisExtInfoCon.SelectedBusinessType = 'FI';
            thisExtInfoCon.SelectedAreaOfFocus = 'FI3';
            thisExtInfoCon.cntryId = Con.Country__c;
            thisExtInfoCon.PopulateAvailableChannels();
            thisExtInfoCon.PopulateAvailableSubChannels();
            Test.stopTest();
        }
    }
}