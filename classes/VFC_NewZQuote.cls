public class VFC_NewZQuote {
    public VFC_NewZQuote(ApexPages.StandardController controller) {
    }
    
    public PageReference redirectToNewZQuote() {

       String retURL = ApexPages.currentPage().getParameters().get('retURL');
       String Id = ApexPages.currentPage().getParameters().get(Label.CLJUN16FIO001);
       String AccountPrefix = Account.sObjectType.getDescribe().getKeyPrefix();
       PageReference pageRef;
       
       if (String.isNotEmpty(Id) && Id.substring(0,3) == AccountPrefix) {
          pageRef = new PageReference('/apex/zqu__quoteoption?crmAccountId='+Id+'&retURL='+retURL);
       }
       else {
          pageRef = new PageReference('/apex/zqu__quoteoption?retURL='+retURL);
       }
       return pageRef;
    }
}