Public without sharing class VFC_SRV_MyFSInternalUserLandingContrller{
Public String ultimateParentAccount{get;set;}
Public String countryCode{get;set;}
Public String accountId{get;set;}

Public VFC_SRV_MyFSInternalUserLandingContrller(){
//System.debug('krishna');
ultimateParentAccount = Apexpages.currentPage().getParameters().get('Params');
countryCode = Apexpages.currentPage().getParameters().get('Country');
accountId = Apexpages.currentPage().getParameters().get('accountId');
if((ultimateParentAccount == null || ultimateParentAccount == '') && (accountId != null && accountId != '')){
 ultimateParentAccount = accountId;
}
}

Public Pagereference validateLoggedInUser(){
Pagereference pr ;
 String userId = UserInfo.getUserId();
User userObj = [Select Id, Name,Profile.UserLicense.LicenseDefinitionKey,UserType 
    From User where Id = : userId];
//System.debug('raghav'+userObj.Profile.UserLicense.LicenseDefinitionKey);
if( userObj != null && userObj.Profile.UserLicense.LicenseDefinitionKey != null && userObj.Profile.UserLicense.LicenseDefinitionKey == System.label.CLSRVMYFS_SALESFORCE_LICENSE && countryCode != null && countryCode != '' && accountId != null && accountId != '' ){
SVMXC__Site__c site = new SVMXC__Site__c();
 site = [Select id,name,SVMXC__Account__c,SVMXC__Account__r.Country__r.name,SVMXC__Account__r.UltiParentAcc__c from SVMXC__Site__c where
                                         (SVMXC__Account__C =:accountId) and PrimaryLocation__c = True and SVMXC__Account__r.Country__r.CountryCode__c=:countryCode];
 
 if(site != null && site.id != null){
pr = callpageredirect(countryCode,ultimateParentAccount,site.id);
}
} 
return pr; 
  

}

  public Pagereference callpageredirect(String countryCode,String ultimateParentAccount,String site){
     //   PageReference siteSelectionPage = new Pagereference('/apex/VFP_SRV_MYFSSiteSelecitonPage?Country='+countryCode + '&Params='+companyId);
  //  PageReference siteSelectionPage = new Pagereference(System.label.CLSRVMYFS_CUSTOM_LANDING_REDIRECTION_URL+countryCode + '&Params='+ultimateParentAccount+'&Site='+site);
      //  PageReference siteSelectionPage = new Pagereference('/apex/VFP_SRV_MYFSSiteSelecitonPage?Country='+countryCode + '&Params='+ultimateParentAccount+'&Site='+site);
      PageReference siteSelectionPage = new Pagereference(System.label.CLSRVMYFS_CUSTOM_LANDING_REDIRECTION_URL+'?Country='+countryCode + '&Params='+ultimateParentAccount+'&Site='+site);


        siteSelectionPage.setRedirect(true);
        return siteSelectionPage;
    }
    
  /*  Public String getMessage(){
     return 'Hello';
    } */
}