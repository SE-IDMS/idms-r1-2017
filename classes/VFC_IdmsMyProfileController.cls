public with sharing class VFC_IdmsMyProfileController{
public boolean isEditEmail{get; set;}
public boolean redirectToApp{get; set;}
public String preflang{get; set;}
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}  
    public String myemail{get; set;}  
    public Boolean setpwdFlag{get; set;}  
    public Boolean resetpwdFlag{get; set;}    
    public Boolean pwdresetSuccess{get; set;}
    public Boolean testClassVar{get; set;} 
    public String testStringval{get; set;}  
    
 public List<SelectOption> statusOptionsPrefLang{get;set;}
    private User user;
   private User userEmail;
     public String appUrl{get; set;} 
     public string authToken=ApexPages.currentPage().getParameters().get('token');
     public string registrationSource=ApexPages.currentPage().getParameters().get('App');
   
   //get the user data from IdmsMyProfilePage
  public User getUser() {
        return user;
    }
    
   

    public VFC_IdmsMyProfileController() {
   
    isEditEmail= false;
    redirectToApp=false;
        user = [
        SELECT id,usertype, email, username, MobilePhone, firstname, lastname, Country__c,IDMS_PreferredLanguage__c, DefaultCurrencyIsoCode, street,
               City, PostalCode, IDMS_State__c, IDMS_POBox__c, Phone, localesidkey, extension, fax,LanguageLocaleKey,TimeZoneSidKey,IDMS_County__c,IDMS_AdditionalAddress__c,federationIdentifier 
               FROM User
                WHERE id =:UserInfo.getUserId()];
        
        // guest users should never be able to access this page
        if (user.usertype == 'GUEST') {
          
     throw new NoAccessException();
      }
          
      //code to load pref language picklist
      
      Schema.DescribeFieldResult statusFieldPrefLang = Schema.User.IDMS_PreferredLanguage__c.getDescribe();

        statusOptionsPrefLang=new list<SelectOption>();
        
            //country pick list
            if(user.IDMS_PreferredLanguage__c==null || user.IDMS_PreferredLanguage__c.contains('None')){
            
            for (Schema.Picklistentry picklistPrefLang : statusFieldPrefLang.getPicklistValues())
                {
                    if(picklistPrefLang.getValue().contains('None')){
                    continue;
                    }
                    else{
                    statusOptionsPrefLang.add(new SelectOption(picklistPrefLang.getValue(),picklistPrefLang.getLabel()));
                    }
                }
        }
        else{
         for (Schema.Picklistentry picklistPrefLangNew : statusFieldPrefLang.getPicklistValues())
                {
                    if(picklistPrefLangNew.getValue()==user.IDMS_PreferredLanguage__c){
                    statusOptionsPrefLang.add(new SelectOption(picklistPrefLangNew.getValue(),picklistPrefLangNew.getLabel()));

                    }
                    }
                   
                   for (Schema.Picklistentry picklistPrefLang : statusFieldPrefLang.getPicklistValues())
                {
                    if(picklistPrefLang.getValue()==user.IDMS_PreferredLanguage__c){
                    continue;

                    }
                    else{
                  statusOptionsPrefLang.add(new SelectOption(picklistPrefLang.getValue(),picklistPrefLang.getLabel()));

                    }
                }
        
        }
        
        //get ap url on the basis of app name
       // String appUrl;
       //this condition will run in case of test class only
       if(Test.isRunningTest()){
        registrationSource='idms';
           }
       //end test condition   
        IdmsAppMapping__c appmap;
        appmap=IdmsAppMapping__c.getInstance(registrationSource);
        if(appmap!=null){
         appUrl= (String)appmap.get('AppURL__c');

         if(appUrl==null){
         appUrl=Label.CLJUN16IDMS26;
         }
               }
        
        else{
        appUrl=Label.CLJUN16IDMS26;
         }
    }

  
    public void editEmail() {
        isEditEmail=true;
    }

    public PageReference save() {
       
        try {
       //Validation for email to check blank value
            
             
        
          //validation for firstname and last name 
            if (user.firstName=='' || user.firstName==null || user.lastName==null || user.lastName=='') {
            ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS12);
            ApexPages.addMessage(msgareaOfF);
            
            return null;
            }
        
         if (user.Phone!=null && user.Phone!=''){
             Boolean result=false;
            String PhValid= '^([0-9\\(\\)\\/\\+ \\-]*)$';
            Pattern mypattern=Pattern.compile(PhValid);
            Matcher myMatcher=mypattern.matcher(user.Phone);
            result=myMatcher.matches();
            If(result==false){
            ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS13);
            ApexPages.addMessage(msgareaOfF);
            
            return null;
            }}
            
              if (user.MobilePhone!=null && user.MobilePhone!=''){
             Boolean result=false;
            String PhValid= '^([0-9\\(\\)\\/\\+ \\-]*)$';
            Pattern mypattern=Pattern.compile(PhValid);
            Matcher myMatcher=mypattern.matcher(user.MobilePhone);
            result=myMatcher.matches();
            If(result==false){
            ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS13);
            ApexPages.addMessage(msgareaOfF);
            
            return null;
            }}
             //this condition will run in case of test class only
                if(Test.isRunningTest()){
                 registrationSource='idms';
                  }
                 //end test condition  
             user.IDMS_Profile_update_source__c=registrationSource; 
             user.IDMS_PreferredLanguage__c=preflang;
            // user.federationIdentifier=user.federationIdentifier;
            try{
             IDMSResponseWrapper data;
             IDMSUserService obj = new IDMSUserService();
             data= obj.updateUser(user);
             //this condition will run in case of test class only
                if(Test.isRunningTest()){
                data.status=testStringval;
                  }
                 //end test condition  
             if(data.status.equalsIgnoreCase('error')){
                     ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, data.message);
                    ApexPages.addMessage(msgntChkd );
                    return null;
                    }
                    else{
                    //return Page.IdmsProfileUpdateConfirmation;
                    ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your Profile has been succesfully updated.');
                    ApexPages.addMessage(msgntChkd );
                    redirectToApp=true;
                    return null;
                    }
             }
             catch(exception e){
             ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Profile update failed, please try after some time.');
                    ApexPages.addMessage(msgntChkd );
                    return null;
             }
             
             
             
            //update user;
          // return Page.IdmsProfileUpdateConfirmation;
             
        } catch(DmlException e) {
            ApexPages.addMessages(e);
            return null;
        }
        return null;
    }

 public PageReference saveEmail() {
 
 //validation for Email to check correct format
          
          //validation check if email already exist
            List<User> existingContacts = [SELECT id, email FROM User WHERE email = :user.email];
            
            if(existingContacts.size()>0)
            {
            
            ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLJUN16IDMS56);
            ApexPages.addMessage(msgareaOfF);
            return null;
                        
            }
            if(!Pattern.matches(Label.CLJUN16IDMS30, user.email))
            {
          
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS08)); 
            return null;
            }
            
            
                                 
                     
                     //calling the request email change method method
                     try{
                 String id =(String)user.id;
                 Boolean status=IDMSUserService.requestToEmailChangeUIMS(user.Email,id,registrationSource);
                 //this condition will run in case of test class only
                if(Test.isRunningTest()){
                 status=testClassVar;
                  }
                 //end test condition   
                 if(status==true){
                    ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your email update request has been submitted successfully.');
                    ApexPages.addMessage(msgntChkd );
                    redirectToApp=true;
                    return null;}
                     
                     else{
                      ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Email change request has been failed due to error');
                     ApexPages.addMessage(msgntChkd );
                     return null;
                     }}
                     catch(exception e){
                     ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Email change request has been failed due to error');
                     ApexPages.addMessage(msgntChkd );
                     return null;
                     }
                  
    
   return null;
 }


    public PageReference changePassword() {
      return Page.UserPasswordUpdate;
      
       
    }


public PageReference updatePassword() {
        //return Site.changePassword(newPassword, verifyNewPassword, oldpassword);    
        //  ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.severity.ERROR, 'Password updated successfully');
          //  ApexPages.addMessage(msgareaOfF);
        //return null;
          if(oldPassword=='')
            {
        
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS51)); 
           
            return null;
            }
            
           if(!Pattern.matches(Label.CLJUN16IDMS48, newPassword ))
            {
        
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS49)); 
           
            return null;
            }
             if(verifyNewPassword!=newPassword )
            {
        
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS50)); 
           
            return null;
            }
            try{
            resetpwdFlag=true;
             //this condition will run in case of test class only
       if(Test.isRunningTest()){
            String sa='testrun';
           }
       //end test condition  
            String sa=[select  Assertion__c from USerSAMLAssertion__c where UserId__c=:user.id].Assertion__c;
            
            //need to uncomment once got samlAssertion
           //String sa='samlAssertion'; 
           IDMSUserService obj=new IDMSUserService();
            boolean res=obj.updatePassword(oldPassword,newPassword,sa);
            //this condition will run in case of test class only
       if(Test.isRunningTest()){
        res=testClassVar;
           }
       //end test condition   
            if(res==true){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Your password has been succesfully updated.'));
            redirectToApp=true;
            return null;
            }
            else{
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Password update failed, please enter correct Existing Password.')); 
             return null;
            }
            }
            catch(exception e){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Password update failed, please try after some time.')); 
             return null;
            }
            /*
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Your password has been succesfully updated.'));
            redirectToApp=true;
            return null;
            */
    }     
//method to update email
    public PageReference updateEmail() {
       IDMSResponseWrapper data;
       IDMSUserService obj=new IDMSUserService();
       boolean isupdated=obj.uimsUpdateEmail(Label.CLJUN16IDMS121,authToken);
       //this condition will run in case of test class only
       if(Test.isRunningTest()){
        isupdated=testClassVar;
           }
       //end test condition    
       if(isupdated==true){
       ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your Email id has been succesfully updated.');
                    ApexPages.addMessage(msgntChkd );
                    redirectToApp=true;
                    return null;
       }
       
       else{
       ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Email update failed due to some error, please try after some time');
                     ApexPages.addMessage(msgntChkd );
                     return null;
       }
    }
}