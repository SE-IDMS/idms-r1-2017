public class AP_Partorder {
    
    Public static void cancelpartsorder(set<id> pid){
        
        list<SVMXC__RMA_Shipment_Line__c> plinelist= new list<SVMXC__RMA_Shipment_Line__c>();
        list<SVMXC__RMA_Shipment_Line__c> plinelist1= new list<SVMXC__RMA_Shipment_Line__c>();
        if(pid!=null)
            plinelist=[select id, name,SVMXC__RMA_Shipment_Order__c,ShippingStatus__c from SVMXC__RMA_Shipment_Line__c where SVMXC__RMA_Shipment_Order__c in :pid];
        
        for(SVMXC__RMA_Shipment_Line__c pline:plinelist){
            
            pline.ShippingStatus__c='Cancelled';
            plinelist1.add(pline);
            
        }
        if(plinelist1!=null && plinelist1.size()>0)
           update plinelist1;
    }
    
    Public static void cancelPartorderRMA(set<id> Rmaid){
        list<SVMXC__RMA_Shipment_Order__c> RMAlist= new list<SVMXC__RMA_Shipment_Order__c>();
        if(Rmaid!=null){
            
            for(SVMXC__RMA_Shipment_Order__c Rma:[select id, name,SVMXC__Order_Status__c from SVMXC__RMA_Shipment_Order__c where id in :Rmaid])
            {
                
                Rma.SVMXC__Order_Status__c='Cancelled';
                RMAlist.add(Rma);
            }
            
        }
        if(RMAlist!=null && RMAlist.size()>0)
            database.update (RMAlist,false);        
        
    }
    
}