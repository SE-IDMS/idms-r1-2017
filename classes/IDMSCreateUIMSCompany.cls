/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class implements queueable interface which allows callouts to update UIMS companies. 
**/
public class IDMSCreateUIMSCompany implements Queueable,Database.AllowsCallouts{
    
    User UserRecord;
    IDMSCaptureError Errorcls;
    static IDMSCaptureError errorLog = new IDMSCaptureError();
    public IDMSCreateUIMSCompany(){}
    
    public IDMSCreateUIMSCompany(USer UserRecord) {
        this.UserRecord= UserRecord;
    }
    //future callout method which accepts callerFid, saml assertion, company federation Id and Company object. It returns boolean as a response.
    @future (callout=true)
    public static void updateCompany(string UserRecordStr){
        user UserRecord = (user)JSON.deserialize(UserRecordStr,user.class);
        try{
            user u=[select IDMS_VC_NEW__c,idms_vc_old__c,IDMSCompanyFederationIdentifier__c from user where id =:userrecord.id];
            
            WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort cls    = new WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort();
            cls.clientCertName_x                                                    = System.Label.CLJUN16IDMS103;
            cls.endpoint_x                                                          = label.CLQ316IDMS127;
            WS_IDMSUIMSAuthCompanyService.companyV3 companyObj                      = new WS_IDMSUIMSAuthCompanyService.companyV3();
            companyObj.FederatedID                                                  = UserRecord.IDMSCompanyFederationIdentifier__c;
            companyObj.OrganizationName                                             = UserRecord.CompanyName;
            companyObj.GoldenId                                                     = UserRecord.IDMSCompanyGoldenId__c;
            companyObj.CountryCode                                                  = UserRecord.Company_Country__c;
            companyObj.CurrencyCode                                                 = UserRecord.DefaultCurrencyIsoCode;
            companyObj.CustomerClass                                                = UserRecord.IDMSClassLevel2__c;
            companyObj.LocalityName                                                 = UserRecord.Company_City__c;
            companyObj.PostalCode                                                   = UserRecord.Company_Postal_Code__c;
            companyObj.PostOfficeBox                                                = UserRecord.IDMSCompanyPoBox__c;
            companyObj.State                                                        = UserRecord.Company_State__c;
            companyObj.Street                                                       = UserRecord.Company_Address1__c;
            if(UserRecord.IDMSCompanyHeadquarters__c){
                companyObj.HeadQuarter                                              = 'true';
            }
            else
            {
                companyObj.HeadQuarter                                              = 'false';
            }
            companyObj.AddInfoAddress                                               = UserRecord.Company_Address2__c;
            companyObj.County                                                       = UserRecord.IDMSCompanyCounty__c;
            companyObj.WebSite                                                      = UserRecord.Company_Website__c;
            companyObj.MarketServed                                                 = UserRecord.IDMSCompanyMarketServed__c;
            companyObj.EmployeeSize                                                 = UserRecord.IDMSCompanyNbrEmployees__c;
            companyObj.businessType                                                 = UserRecord.IDMSClassLevel1__c;
            companyObj.TaxIdentificationNumber                                      = UserRecord.IDMSTaxIdentificationNumber__c; 
            string callerFid                                                        = Label.CLJUN16IDMS104;
            
            IDMSEncryption encrytp                                                  = new IDMSEncryption();
            String samlAssrestion                                                   = encrytp.ecryptedToken(UserRecord.FederationIdentifier,u.IDMS_VC_NEW__c);
            boolean UpdateRes = false;
            if(!test.isrunningtest()){
                UpdateRes                                                           = cls.updateCompany(callerFid,samlAssrestion,u.IDMSCompanyFederationIdentifier__c,companyObj);
            }
            system.debug(UpdateRes);
            if(UpdateRes){
                UserRecord.idms_vc_old__c                                           = u.IDMS_VC_NEW__c;
                update userrecord;
            }
            if(test.isrunningtest()){
                integer testint                                                     = 1/0;
            }
            
        }catch(Exception e ){
            system.debug('*****catch exception update company********'+e); 
            String ExceptionMessage = Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString();
            if(String.isNotEmpty(e.getMessage())){
                //add this user for reconciliation
                system.debug('Inside federation id check for user:'+UserRecord.id);
                Integer attemptsMaxLmt = integer.valueof(label.CLNOV16IDMS022);
                List<IDMSCompanyReconciliationBatchHandler__c> companyInfoToReconciliate = [select Id,AtCreate__c, AtUpdate__c, IdmsUser__c,HttpMessage__c, NbrAttempts__c
                                                                                            from IDMSCompanyReconciliationBatchHandler__c where IdmsUser__c =: UserRecord.id];
                if(companyInfoToReconciliate.size()>0)
                {
                    if(companyInfoToReconciliate[0].AtCreate__c == true){
                        companyInfoToReconciliate[0].AtUpdate__c = false;
                    }else{
                        companyInfoToReconciliate[0].AtUpdate__c = true;
                    }
                    companyInfoToReconciliate[0].HttpMessage__c = ExceptionMessage;
                    update companyInfoToReconciliate;
                }else{
                    system.debug('companyInfoToReconciliate id:'+companyInfoToReconciliate );
                    IDMSCompanyReconciliationBatchHandler__c companyToReconciliate  = new IDMSCompanyReconciliationBatchHandler__c();
                    companyToReconciliate.AtCreate__c                               = false;
                    companyToReconciliate.AtUpdate__c                               = true;
                    companyToReconciliate.HttpMessage__c                            = ExceptionMessage;
                    companyToReconciliate.IdmsUser__c                               = UserRecord.id;
                    companyToReconciliate.NbrAttempts__c                            = 1;
                    insert companyToReconciliate;
                }
            }
            errorLog.IDMScreateLog('IDMSCreateUIMSCompany','IdmsCompanyService','update company info of user UIMS web Service call',UserRecord.username,
                                   Label.CLJUN16IDMS99+e.getLineNumber()+Label.CLJUN16IDMS100+e.getMessage()+Label.CLJUN16IDMS101+e.getStackTraceString(),'',Datetime.now(),'',UserRecord.FederationIdentifier,false,'',false,null);
        }
    }
    
    public void execute(QueueableContext context) {}
}