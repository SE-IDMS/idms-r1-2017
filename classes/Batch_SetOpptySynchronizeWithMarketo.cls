global class Batch_SetOpptySynchronizeWithMarketo implements Database.Batchable<sObject>,Schedulable,Database.Stateful{
    public String query;
    public Static Datetime dt = System.now().addHours(-12);
    public static String CRON_EXP =  '0 0 * * * ?'; //'0 0 5,11,17,23 * * ?';
    global Batch_SetOpptySynchronizeWithMarketo(){
        
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        query = 'SELECT Id, Name FROM Opportunity WHERE Id IN (SELECT OpportunityId FROM OpportunityContactRole WHERE Contact.SynchronizeWithMarketo__c = 1 AND (Opportunity.SynchronizeWithMarketo__c = 0 OR Opportunity.SynchronizeWithMarketo__c = null) AND LastModifiedDate >= :dt)';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        System.debug('>><<>><<scope:'+scope);
        List<Opportunity> oppList = new List<Opportunity>();
        for(sObject s1 : scope){
           Opportunity opp = (Opportunity)(s1);
           opp.SynchronizeWithMarketo__c = 1;
           oppList.add(opp);
        }
        update oppList;
    }
    global void finish(Database.BatchableContext BC){        
    }
    
    global static String ScheduleIt () {
        Batch_SetOpptySynchronizeWithMarketo syncWithMktBatch = new Batch_SetOpptySynchronizeWithMarketo();
        return System.schedule('Opportunity Synchronize With Marketo',CRON_EXP, syncWithMktBatch);
    }
    
    global void execute(SchedulableContext sc) 
    {
        Batch_SetOpptySynchronizeWithMarketo syncWithMktBatch = new Batch_SetOpptySynchronizeWithMarketo(); 
        Database.executebatch(syncWithMktBatch);
    }
}