/*
    Author          : Abhishek 
    Date Created    : 24/05/2011
    Description     : Class utilised by triggers OppBeforeUpdate.
*/

public class AP11_UpdateQuoteLink
{
    public static Boolean updateQlkFromOpp = false;
    
    public static void isSynchNeededForOpp(Map<id,Opportunity> newOpp)
    {
        System.Debug('******isSynchNeededForOpp method in AP11_UpdateQuoteLink Class Start****');
        if (newOpp != null && newOpp.size() > 0) {
        Boolean Flag=false;
        List<OPP_QuoteLink__c> quoteLink=[select OpportunityName__c,TECH_IsSyncNeededForOpp__c,ActiveQuote__c,TECH_IsBoundToBackOffice__c,ActiveQuoteLinkUniqueness__c from OPP_QuoteLink__c where OpportunityName__c in:newOpp.keyset()];
        // and TECH_IsSyncNeededForOpp__c=False];
        List<OPP_QuoteLink__c> updtFldOnQuote= new List<OPP_QuoteLink__c>();
        List<Opportunity> errorOpportunities = new List<Opportunity>();
        Set<ID> solutionOpportunity = new set<Id>();
        //AggregateResult to query opportunities with more the one active QuoteLink 
        List<AggregateResult> quoteCount =[select count(id),OpportunityName__c from OPP_QuoteLink__c where OpportunityName__c in:newOpp.keyset() and ActiveQuote__c=True group by OpportunityName__c having count(id)>1];  

        //Creating a list of ID's the opportunities with more than 1 active Quote
        for(AggregateResult quote : quoteCount)
        {
            solutionOpportunity.add((id)quote.get('OpportunityName__c'));
        }
        
        for(OPP_QuoteLink__c updtQuoteLink:quoteLink)
        {            
           //if the opportunity is of type solution and it has more than 1 active quote add it to a list of opportunities used to throw error           
       /*     if(newopp.get(updtQuoteLink.OpportunityName__c)!= null && Label.CL00240 != null && Label.CL00240.equalsIgnoreCase(newopp.get(updtQuoteLink.OpportunityName__c).OpptyType__c) && solutionOpportunity.contains(updtQuoteLink.OpportunityName__c))
            {  
                errorOpportunities.add(newopp.get(updtQuoteLink.OpportunityName__c));
            }            
          //checks for active Quote and is quote bound to backoffice.
            else 
            { */
                updtQuoteLink.TECH_IsSyncNeededForOpp__c=true;
                updtFldOnQuote.add(updtQuoteLink);
        //    }                                             
        }
    /*    if(!errorOpportunities.isEmpty())
        {
        //Throwing Error.
           for(opportunity opp:errorOpportunities)  
           {
               opp.addError(label.CL00620);
           }  
        }  */      
          if(updtFldOnQuote.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
        {
                System.Debug('######## AP11 Error Insert: '+ Label.CL00264);
        }
            else
            {
                updateQlkFromOpp = true;
                Database.saveResult []svr = database.update(updtFldOnQuote);
                for(Database.SaveResult svrt:svr){
                    if(!svrt.isSuccess()){
                        Database.Error err = svrt.getErrors()[0];
                        System.Debug('######## AP11 Error Insert: '+err.getStatusCode()+' '+err.getMessage());              
                    }
                }
            }
        }
        System.debug('******isSynchNeededForOpp method in AP11_UpdateQuoteLink Class END****');  
    }
}