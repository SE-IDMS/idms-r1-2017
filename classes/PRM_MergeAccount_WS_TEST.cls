//Description: Test Class for PRM_MergeAccount_WS 

@isTest
private class PRM_MergeAccount_WS_TEST {

    @testSetup static void inserts(){
        CS_PRMTechnicalMergeFieldMapping__c techMerge1 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_ADDL_ADDR',ObjectName__c='Account',FieldAPIName__c='PRMAdditionalAddress__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge2 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_ADDL_CMNT',ObjectName__c='Account',FieldAPIName__c='PRMAdditionalComments__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge3 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_AF',ObjectName__c='Account',FieldAPIName__c='PRMAreaOfFocus__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge4 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_AS',ObjectName__c='Account',FieldAPIName__c='PRMAnnualSales__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge5 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_BT',ObjectName__c='Account',FieldAPIName__c='PRMBusinessType__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge6 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_CITY',ObjectName__c='Account',FieldAPIName__c='PRMCity__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge7 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_CMP_NAME',ObjectName__c='Account',FieldAPIName__c='PRMCompanyName__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge8 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_CMP_PHONE',ObjectName__c='Account',FieldAPIName__c='PRMCompanyPhone__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge9 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_CNTRY',ObjectName__c='Account',FieldAPIName__c='PRMCountry__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge10 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_DECLINE_RSN',ObjectName__c='Account',FieldAPIName__c='PRMReasonForDecline__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge11 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_DOE',ObjectName__c='Account',FieldAPIName__c='PRMDomainsOfExpertise__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge12 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_ES',ObjectName__c='Account',FieldAPIName__c='PRMEmployeeSize__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge13 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_EXCL_REP',ObjectName__c='Account',FieldAPIName__c='PRMExcludeFromReports__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge14 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_HQ',ObjectName__c='Account',FieldAPIName__c='PRMCorporateHeadquarters__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge15 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_INCL_PUB_SRCH',ObjectName__c='Account',FieldAPIName__c='PRMIncludeCompanyInfoInSearchResults__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge16 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_IS_ACTIV',ObjectName__c='Account',FieldAPIName__c='PRMIsActiveAccount__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge17 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_MS',ObjectName__c='Account',FieldAPIName__c='PRMMarketServed__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge18 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_ORIGIN',ObjectName__c='Account',FieldAPIName__c='PRMOrigin__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge19 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_ORIG_SRC',ObjectName__c='Account',FieldAPIName__c='PRMOriginSource__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge20 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_ORIG_SRC_INF',ObjectName__c='Account',FieldAPIName__c='PRMOriginSourceInfo__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge21 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_ADD_ADDRS',ObjectName__c='Account',FieldAPIName__c='PLAdditionalAddress__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge22 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_AF',ObjectName__c='Account',FieldAPIName__c='PLAreaOfFocus__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge23 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_BT',ObjectName__c='Account',FieldAPIName__c='PLBusinessType__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge24 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_CITY',ObjectName__c='Account',FieldAPIName__c='PLCity__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge25 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_CMP_DESC',ObjectName__c='Account',FieldAPIName__c='PLCompanyDescription__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge26 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_CMP_Name',ObjectName__c='Account',FieldAPIName__c='PLCompanyName__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge27 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_CNTRY',ObjectName__c='Account',FieldAPIName__c='PLCountry__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge28 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_CONT_FRST_NAME',ObjectName__c='Account',FieldAPIName__c='PLMainContactFirstName__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge29 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_CONT_LST_NAME',ObjectName__c='Account',FieldAPIName__c='PLMainContactLastName__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge30 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_CONT_PHNE',ObjectName__c='Account',FieldAPIName__c='PLMainContactPhone__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge31 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_DATAPOL',ObjectName__c='Account',FieldAPIName__c='PLDatapool__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge32 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_DOMN_EXP',ObjectName__c='Account',FieldAPIName__c='PLDomainsOfExpertise__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge33 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_FAX',ObjectName__c='Account',FieldAPIName__c='PLFax__c');
       
        CS_PRMTechnicalMergeFieldMapping__c techMerge34 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_LOCT_FEATR_GRNT',ObjectName__c='Account',FieldAPIName__c='PLPartnerLocatorFeatureGranted__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge35 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_MN_CONT_EMAIL',ObjectName__c='Account',FieldAPIName__c='PLMainContactEmail__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge36 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_MRKT_SRVD',ObjectName__c='Account',FieldAPIName__c='PLMarketServed__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge37 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_PATNR_LOC',ObjectName__c='Account',FieldAPIName__c='PLShowInPartnerLocator__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge38 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_STATE_PROV',ObjectName__c='Account',FieldAPIName__c='PLStateProvince__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge39 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_STREET',ObjectName__c='Account',FieldAPIName__c='PLStreet__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge40 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_WBSITE',ObjectName__c='Account',FieldAPIName__c='PLWebsite__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge41 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PL_ZIP',ObjectName__c='Account',FieldAPIName__c='PLZipCode__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge42 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PREF_DIST1',ObjectName__c='Account',FieldAPIName__c='PRMPreferredDistributor1__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge43 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PREF_DIST2',ObjectName__c='Account',FieldAPIName__c='PRMPreferredDistributor2__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge44 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PREF_DIST3',ObjectName__c='Account',FieldAPIName__c='PRMPreferredDistributor3__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge45 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PREF_DIST4',ObjectName__c='Account',FieldAPIName__c='PRMPreferredDistributor4__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge46 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PRG_TEAM',ObjectName__c='Account',FieldAPIName__c='F_PRM_ProgramTeam__c');
       
        CS_PRMTechnicalMergeFieldMapping__c techMerge47 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PRMACC',ObjectName__c='Account',FieldAPIName__c='PRMAccount__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge48 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PRM_CURRENCY',ObjectName__c='Account',FieldAPIName__c='PRMCurrencyIsoCode__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge49 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_PROF_CMPL',ObjectName__c='Account',FieldAPIName__c='PRMAccountProfileComplete__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge50 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_REG_ACTV_DT',ObjectName__c='Account',FieldAPIName__c='PRMRegistrationActivationDate__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge51 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_REG_REV_DAT',ObjectName__c='Account',FieldAPIName__c='PRMRegistrationReviewedRejectedDate__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge52 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_REG_STS',ObjectName__c='Account',FieldAPIName__c='PRMAccountRegistrationStatus__c');
        
        //CS_PRMTechnicalMergeFieldMapping__c techMerge53 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_SE_ACC',ObjectName__c='Account',FieldAPIName__c='PRMSEAccountNumber__c');
        CS_PRMTechnicalMergeFieldMapping__c techMerge53 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_SE_ACC',ObjectName__c='Account',FieldAPIName__c='PRMSEAccountNumber2__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge54 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_SHOW_PL',ObjectName__c='Account',FieldAPIName__c='PRMPLShowOnMap__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge55 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_STAT_PROV',ObjectName__c='Account',FieldAPIName__c='PRMStateProvince__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge56 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_STREET',ObjectName__c='Account',FieldAPIName__c='PRMStreet__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge57 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_TAX',ObjectName__c='Account',FieldAPIName__c='PRMTaxId__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge58 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_TNC',ObjectName__c='Account',FieldAPIName__c='PRMTermsAndConditionsAccepted__c');
       
        CS_PRMTechnicalMergeFieldMapping__c techMerge59 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_TYPE',ObjectName__c='Account',FieldAPIName__c='PRMAccType__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge60 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_UIMS',ObjectName__c='Account',FieldAPIName__c='PRMUIMSID__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge61 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_WEBSITE',ObjectName__c='Account',FieldAPIName__c='PRMWebsite__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge62 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_ACC_ZIP',ObjectName__c='Account',FieldAPIName__c='PRMZipCode__c');
        
        CS_PRMTechnicalMergeFieldMapping__c techMerge63 =new CS_PRMTechnicalMergeFieldMapping__c(Name='ACC_PRM_UIMSSEACCID',ObjectName__c='Account',FieldAPIName__c='PRMUIMSSEAccountId__c');
        
        
       
        List<CS_PRMTechnicalMergeFieldMapping__c> recList = new List<CS_PRMTechnicalMergeFieldMapping__c>();
        recList.add(techMerge1);
        recList.add(techMerge2);
        recList.add(techMerge3);
        recList.add(techMerge4);
        recList.add(techMerge5);
        recList.add(techMerge6);
        recList.add(techMerge7);
        recList.add(techMerge8);
        recList.add(techMerge9);
        recList.add(techMerge10);     
        recList.add(techMerge11);
        recList.add(techMerge12);
        recList.add(techMerge13);
        recList.add(techMerge14);
        recList.add(techMerge15);
        recList.add(techMerge16);
        recList.add(techMerge17);
        recList.add(techMerge18);
        recList.add(techMerge19);
        recList.add(techMerge20);
        recList.add(techMerge21);
        recList.add(techMerge22);
        recList.add(techMerge23);
        recList.add(techMerge24);
        recList.add(techMerge25);
        recList.add(techMerge26);
        recList.add(techMerge27);
        recList.add(techMerge28);
        recList.add(techMerge29);
        recList.add(techMerge30);
        recList.add(techMerge31);
        recList.add(techMerge32);     
        recList.add(techMerge33);
        recList.add(techMerge34);
        recList.add(techMerge35);
        recList.add(techMerge36);
        recList.add(techMerge37);
        recList.add(techMerge38);
        recList.add(techMerge39);
        recList.add(techMerge40);
        recList.add(techMerge41);
        recList.add(techMerge42);
        recList.add(techMerge43);
        recList.add(techMerge44);
        recList.add(techMerge45);
        recList.add(techMerge46);
        recList.add(techMerge47);
        recList.add(techMerge48);
        recList.add(techMerge49);
        recList.add(techMerge50); 
        recList.add(techMerge51);
        recList.add(techMerge52);
        recList.add(techMerge53);
        recList.add(techMerge54);
        recList.add(techMerge55);
        recList.add(techMerge56);
        recList.add(techMerge57);
        recList.add(techMerge58);
        recList.add(techMerge59);
        recList.add(techMerge60);
        recList.add(techMerge61);
        recList.add(techMerge62);
        recList.add(techMerge63);
        Insert recList;    
    }

    static TestMethod void testMergeAccount(){
        List<CS_PRMTechnicalMergeFieldMapping__c> merges = new List<CS_PRMTechnicalMergeFieldMapping__c>([select Name,ObjectName__c,FieldAPIName__c From CS_PRMTechnicalMergeFieldMapping__c where ObjectName__c='Account']);
        //Get Business Account & PRM Account RecordType IDs
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Id partnerAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='PRMAccount' limit 1][0].Id;
        
        //Business Accounts
        Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
        insert c;
        
        Account acc1 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '1',City__c='Bangalore');
        insert acc1;
        Account acc2 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '2',City__c='Bangalore');
        insert acc2;
        Account acc3 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '3',City__c='Bangalore');
        insert acc3;
        User dutProfile =  [SELECT Id FROM user WHERE profileid =: System.label.CLOCT15ACC08 and isActive=true limit 1]; 
        System.debug('User1235 :'+dutProfile.Id);
        System.RunAs(dutProfile){
            Test.startTest();
             
            //Partner Accounts
            Account par1 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                        recordtypeId = partnerAccRTId, PRMUIMSSEAccountId__c = '1', PRMUIMSID__c = '1100',City__c='Bangalore');
            insert par1;
            /*Account par2 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                        recordtypeId = partnerAccRTId, PRMUIMSSEAccountId__c = '2', PRMUIMSID__c = '1111');
            insert par2;*/
            
            Account par3 = new Account(Name='TestAccount3', Street__c='New Street', POBox__c='XXX', ZipCode__c='12346',
                        recordtypeId = partnerAccRTId, PRMUIMSSEAccountId__c = '3', PRMUIMSID__c = '1112',City__c='Bangalore');
            insert par3;

            Contact c4 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=par3.Id,JobTitle__c='Z3',
                    CorrespLang__c='EN',WorkPhone__c='1234567890',PRMOnly__c = false, PRMUIMSSEContactId__c = '123456789',
                    Email = 'testv19a53@test.com',PRMEmail__c='testf5a64@test.com');
            insert c4;
            
            acc3.SEAccountId__c = '2';
            update acc3;
            
              
       
            
            PRM_MergeAccount_WS.resultWrapper resWrap = new PRM_MergeAccount_WS.resultWrapper('S','',0);
            
            /*Task task = Utils_TestMethods.createOpenActivity(par2.Id, null,  'Not Started', Date.Today().addDays(2));
            insert task;*/
            resWrap = PRM_MergeAccount_WS.mergeAccount(new List<String>{acc1.SEAccountId__c, acc2.SEAccountId__c, acc3.SEAccountId__c});
            resWrap = PRM_MergeAccount_WS.mergeAccount(new List<String>{acc1.SEAccountId__c});
            //resWrap = PRM_MergeAccount_WS.mergeAccount(new List<String>{acc2.SEAccountId__c});
            acc3.SEAccountId__c = '3';
            update acc3;
            resWrap = PRM_MergeAccount_WS.mergeAccount(new List<String>{acc3.SEAccountId__c});

            //resWrap = PRM_MergeAccount_WS.mergeAccountSkipVerification(new List<String>{par3.PRMUIMSSEAccountId__c});
            //resWrap = PRM_MergeAccount_WS.mergeAccount(new List<String>{par3.PRMUIMSSEAccountId__c});
            Test.stopTest();
        }
    }
    
      static TestMethod void testMergeAccountLead(){
        //Get Business Account & PRM Account RecordType IDs
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Id partnerAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='PRMAccount' limit 1][0].Id;
        
        //Business Accounts
        Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
        insert c;
        List<CS_PRMTechnicalMergeFieldMapping__c> merges = new List<CS_PRMTechnicalMergeFieldMapping__c>([select Name,ObjectName__c,FieldAPIName__c From CS_PRMTechnicalMergeFieldMapping__c where ObjectName__c='Account']);

        Account par3 = new Account(Name='TestAccount3', Street__c='New Street', POBox__c='XXX', ZipCode__c='12346',
                    recordtypeId = businessAccRTId, SEAccountId__c = '20', Country__c = c.Id,City__c='Bangalore');
        insert par3;

        Account par4 = new Account(Name='TestAccount4', Street__c='New Street 2', POBox__c='XXX', ZipCode__c='12346',
                    recordtypeId = partnerAccRTId, Country__c = c.Id,PRMUIMSSEAccountId__c = '20',PRMUIMSID__c = '111457',City__c='Bangalore');
        insert par4;

        Contact c3 = new Contact(FirstName='Test3',LastName = 'TestName3',AccountId=par4.Id,JobTitle__c='Z3',Country__c=c.Id,
        CorrespLang__c='EN',WorkPhone__c='1234567890',PRMOnly__c = true,PRMUIMSID__c='AKRAc',
        Email = 'test5496AA@test.com',PRMEmail__c='test31862@test.com');
        insert c3;

        par4.recordtypeId = businessAccRTId;
        update par4;
        PRM_MergeAccount_WS.resultWrapper resWrap = new PRM_MergeAccount_WS.resultWrapper('S','',0);
        Test.startTest();
        Lead lead = Utils_TestMethods.createLeadWithAcc(par4.Id, c.Id, 5);
        insert lead;
        Case testCase = Utils_TestMethods.createCase(par4.Id,c3.Id,'In Progress');
        /*testCase.AnswerToCustomer__c='here is the answer';
        insert testCase;**/
        Task task1= Utils_TestMethods.createOpenActivity(par4.Id, c3.Id, 'Open',date.today());
        insert task1;
        /*Event event= new Event (Subject='Other',
                                  StartDateTime=system.now().addDays(1),
                                  EndDateTime=system.now().addDays(1),
                                  Whatid=par4.id,
                                  Ownerid=UserInfo.getUserId()                                                    
                                  );
        insert event;*/

        Opportunity opp = Utils_TestMethods.createOpportunity(par4.Id);
        insert opp;
        par4.recordtypeId = partnerAccRTId;
        update par4;
        User dutProfile =  [SELECT Id FROM user WHERE profileid =: System.label.CLOCT15ACC08 and isActive=true limit 1]; 
        System.RunAs(dutProfile){
            resWrap = PRM_MergeAccount_WS.mergeAccountSkipVerification(new List<String>{par4.PRMUIMSSEAccountId__c,par4.PRMUIMSSEAccountId__c});
            resWrap = PRM_MergeAccount_WS.mergeAccountAndTransfer(new List<String>{par4.PRMUIMSSEAccountId__c,par4.PRMUIMSSEAccountId__c});
            resWrap = PRM_MergeAccount_WS.mergeAccount(new List<String>{par3.SEAccountId__c});
            resWrap = PRM_MergeAccount_WS.mergeAccountAndTransfer(new List<String>{par3.SEAccountId__c});
            //resWrap = PRM_MergeAccount_WS.mergeAccount(new List<String>{par4.PRMUIMSSEAccountId__c});
            PRM_MergeAccount_WS.fromObjectToStringResizedForDisplay('hello');
            PRM_MergeAccount_WS.fromObjectToStringResizedForDisplay('hellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohello');
            Test.stopTest();
        }
    }

    static TestMethod void testMergeAccountVarification(){
        //Get Business Account & PRM Account RecordType IDs
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Id partnerAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='PRMAccount' limit 1][0].Id;
        
        //Business Accounts
        Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
        insert c;
        
        List<CS_PRMTechnicalMergeFieldMapping__c> merges = new List<CS_PRMTechnicalMergeFieldMapping__c>([select Name,ObjectName__c,FieldAPIName__c From CS_PRMTechnicalMergeFieldMapping__c where ObjectName__c='Account']);
        
        Account par3 = new Account(Name='TestAccount3', Street__c='New Street', POBox__c='XXX', ZipCode__c='12346',
                    recordtypeId = businessAccRTId, SEAccountId__c = '20', Country__c = c.Id,City__c='Bangalore');
        insert par3;

        Account par4 = new Account(Name='TestAccount4', Street__c='New Street 2', POBox__c='XXX', ZipCode__c='12346',
                    recordtypeId = partnerAccRTId, Country__c = c.Id,PRMUIMSSEAccountId__c = '20',PRMUIMSID__c = '111457',City__c='Bangalore');
        insert par4;

        Contact c3 = new Contact(FirstName='Test3',LastName = 'TestName3',AccountId=par4.Id,JobTitle__c='Z3',Country__c=c.Id,
        CorrespLang__c='EN',WorkPhone__c='1234567890',PRMOnly__c = true,PRMUIMSID__c='AKRAc',
        Email = 'tesfat5496AA@test.com',PRMEmail__c='test34c9a6@test.com');
        insert c3;

        par4.recordtypeId = businessAccRTId;
        update par4;
        PRM_MergeAccount_WS.resultWrapper resWrap = new PRM_MergeAccount_WS.resultWrapper('S','',0);
        Test.startTest();
       
        Task task1= Utils_TestMethods.createOpenActivity(par4.Id, c3.Id, 'Open',date.today());
        insert task1;
        /*Event event= new Event (Subject='WO-12343',
                                  StartDateTime=system.now().addDays(1),
                                  EndDateTime=system.now().addDays(1),
                                  Whatid=par4.id,
                                  Ownerid=UserInfo.getUserId()                                                    
                                  );
        insert event;*/

        Opportunity opp = Utils_TestMethods.createOpportunity(par4.Id);
        insert opp;
        par4.recordtypeId = partnerAccRTId;

        update par4;
        User dutProfile =  [SELECT Id FROM user WHERE profileid =: System.label.CLOCT15ACC08 and isActive=true limit 1]; 
        System.RunAs(dutProfile){
            resWrap = PRM_MergeAccount_WS.mergeAccount(new List<String>{par3.SEAccountId__c});
            delete opp;
            resWrap = PRM_MergeAccount_WS.mergeAccount(new List<String>{par3.SEAccountId__c});
            Test.stopTest();
        }
    }

    static TestMethod void testSkip(){
        PRM_MergeAccount_WS.mergeSingleAccount('give us some code coverage');
        PRM_MergeAccount_WS.mergeSingleAccountAndTransfer('pray for coverage');
    }


    static TestMethod void testCopyFields(){

        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Id partnerAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='PRMAccount' limit 1][0].Id;
        
        //Business Accounts
        Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
        insert c;
        
        List<CS_PRMTechnicalMergeFieldMapping__c> merges = new List<CS_PRMTechnicalMergeFieldMapping__c>([select Name,ObjectName__c,FieldAPIName__c From CS_PRMTechnicalMergeFieldMapping__c where ObjectName__c='Account']);
        
        Account par3 = new Account(Name='TestAccount3', Street__c='New Street', POBox__c='XXX', ZipCode__c='12346',
                    recordtypeId = businessAccRTId, SEAccountId__c = '20', Country__c = c.Id,City__c='Bangalore');
        insert par3;

        Account par4 = new Account(Name='TestAccount4', Street__c='New Street 2', POBox__c='XXX', ZipCode__c='12346',
                    recordtypeId = partnerAccRTId, Country__c = c.Id,PRMUIMSSEAccountId__c = '20',PRMUIMSID__c = '111457',City__c='Bangalore');
        
        DateTime dateTimeValue = DateTime.now();
        Date dateValue = Date.today();
        String stringValue ='S';
        String emailValue ='akram@test.com';
        String urlValue ='http://www.google.com';
    
        Map<String, Schema.SObjectField> fieldsDescribe = Account.SObjectType.getDescribe().fields.getMap();
        Set<String> fieldSet = new Set<String>();
        String selectFields ='Id';
        for(Schema.SObjectField field : fieldsDescribe.values()){
            String apiName = field.getDescribe().getName();
            if (apiName!='PRMUIMSSEAccountId__c' &&  apiName.startsWith('PRM') &&  field.getDescribe().isUpdateable() && !field.getDescribe().isCalculated() && !apiName.endsWith('__pc')) {
                fieldSet.add(apiName);
                Schema.DisplayType type = field.getDescribe().getType();
                Object defaultValue = field.getDescribe().getDefaultValue();
                if(type==Schema.DisplayType.String || type==Schema.DisplayType.Picklist){
                    par4.put(apiName, stringValue);
                    selectFields+=','+apiName;
                }else if(type==Schema.DisplayType.Datetime){
                    par4.put(apiName, dateTimeValue);
                    selectFields+=','+apiName;
                }else if(type==Schema.DisplayType.Date){
                    par4.put(apiName, dateValue);
                    selectFields+=','+apiName;
                }else if(type==Schema.DisplayType.Boolean){
                    par4.put(apiName, defaultValue!=null?!((Boolean)defaultValue):false);
                    selectFields+=','+apiName;
                }
                
            }
        }
        insert par4;
        PRM_MergeAccount_WS.mergeSingleAccount('20');
        String soql ='select '+selectFields+',PRMUIMSSEAccountId__c from Account where Id=\''+par3.Id+'\'';
        
        Account assertAcc = Database.query(soql);
        for(Schema.SObjectField field : fieldsDescribe.values()){
            String apiName = field.getDescribe().getName();
            if (apiName!='PRMUIMSSEAccountId__c' && apiName.startsWith('PRM') &&  field.getDescribe().isUpdateable() && !field.getDescribe().isCalculated() && !apiName.endsWith('__pc')) {
                fieldSet.add(apiName);
                Schema.DisplayType type = field.getDescribe().getType();
                if(type==Schema.DisplayType.String || type==Schema.DisplayType.Picklist){
                    String assertString = (String)assertAcc.get(apiName);
                   // System.assertEquals(apiName+':'+StringValue,apiName+':'+assertString);
                }else if(type==Schema.DisplayType.Datetime){
                    Datetime assertDateTime = (Datetime)assertAcc.get(apiName);
                   // System.assertEquals(apiName+': '+dateTimeValue.format(),apiName+': '+(assertDateTime!=null?assertDateTime.format():''));
                }else if(type==Schema.DisplayType.Date){
                    Date assertDate = (Date)assertAcc.get(apiName);
                   // System.assertEquals(apiName+': '+dateValue.format(),apiName+': '+(assertDate!=null?assertDate.format():''));
                }else if(type==Schema.DisplayType.Boolean){
                    Boolean assertBoolean = (Boolean)assertAcc.get(apiName);
                    //System.assertEquals(apiName+': '+assertBoolean,apiName+': '+assertBoolean);
                }
                
            }
        }

    } 
}