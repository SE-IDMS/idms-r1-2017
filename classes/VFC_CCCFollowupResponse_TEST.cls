@isTest
private class VFC_CCCFollowupResponse_TEST{
    static testMethod void CCCFollowupResponseAsYes_TestMethod() {
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        objContact.CorrespLang__c ='RU';
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        PageReference pageRef = Page.VFP_CCCFollowupResponse;
        pageRef.getParameters().put('caseId',objOpenCase.id);
        pageRef.getParameters().put('response','Yes');
        Test.setCurrentPage(pageRef);
        
        VFC_CCCFollowupResponse ResponseYesCasePage = new VFC_CCCFollowupResponse();
        Test.startTest();
        

        ResponseYesCasePage.updateCase();
        Test.stopTest();
    }
    static testMethod void CCCFollowupResponseAsNo_TestMethod() {
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        objContact.CorrespLang__c ='RU';
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        PageReference pageRef = Page.VFP_CCCFollowupResponse;
        pageRef.getParameters().put('caseId',objOpenCase.id);
        pageRef.getParameters().put('response','No');
        Test.setCurrentPage(pageRef);
        
        VFC_CCCFollowupResponse ResponseNoCasePage = new VFC_CCCFollowupResponse();
        Test.startTest();
        

        ResponseNoCasePage.updateCase();
        Test.stopTest();
    }
    static testMethod void CCCFollowupResponseAsNoOnClosedCase_TestMethod() {
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        objContact.CorrespLang__c ='RU';
        Database.insert(objContact);
               
        Case objClosedCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'Closed');
        objClosedCase.AnswerToCustomer__c = 'Followup EMail Send to Customer. Ready to be Closed';
        Database.insert(objClosedCase);
        
        
        PageReference pageRef = Page.VFP_CCCFollowupResponse;
        pageRef.getParameters().put('caseId',objClosedCase.id);
        pageRef.getParameters().put('response','No');
        Test.setCurrentPage(pageRef);
        
        VFC_CCCFollowupResponse ResponseNoOnClosedCasePage = new VFC_CCCFollowupResponse();
        Test.startTest();
        

        ResponseNoOnClosedCasePage.updateCase();
        Test.stopTest();
    }
}