public with sharing class IPOCascadingMilestoneTriggers
{
    Public Static void IPOCascadingMilestoneBeforeInsertUpdate(List<IPO_Strategic_Cascading_Milestone__c> IPOCMile)
      {
      
          System.Debug('****** CascadingMilestoneBeforeInsertUpdate Start ****'); 
          
          IPOCMile[0].Deployment_Leader__c = null;
          
          list<IPO_Strategic_Deployment_Network__c> IPOChampion = new List<IPO_Strategic_Deployment_Network__c>();
          IPOChampion = [select id,Scope__c, Entity__c,Champion__r.Firstname,Champion__r.Lastname , Deployment_Leader__r.FirstName, Deployment_Leader__r.LastName,Deployment_Leader__r.Email from IPO_Strategic_Deployment_Network__c where IPO_Strategic_Program_Name__c = :IPOCMile[0].Program_Name__c];
          for(IPO_Strategic_Cascading_Milestone__c CM:IPOCMile){
             for(IPO_Strategic_Deployment_Network__c C: IPOChampion){             
                       
             if((CM.Scope__c != null)&& (CM.Entity__c !=null)){
               If(CM.Scope__c.Contains(C.Scope__c) && (CM.Entity__c.Contains(C.Entity__c))) {
                  CM.Deployment_Leader__c = C.Deployment_Leader__r.id;
                 if(C.Champion__c != null)
                  CM.Champion_Name__c = C.Champion__r.Firstname + ' ' + C.Champion__r.Lastname;
                 }
   
                } // Scope Validation If Ends     
                   
               } // For loop 1 Ends
              } // For loop 2 Ends
              
                              
                 System.Debug('****** CascadingMilestoneBeforeInsertUpdate End ****'); 
                 
                } // Static Class Ends
                
      Public Static void IPOCascadingMilestoneBeforeUpdate(List<IPO_Strategic_Cascading_Milestone__c> IPOCMile, List<IPO_Strategic_Cascading_Milestone__c> IPOCMileold)
      {
      
          System.Debug('****** CascadingMilestoneBeforeInsertUpdate Start ****'); 
           
             integer Count;
             integer Countfor;
          IPOCMile[0].Champion_Name__c = '';
          list<IPO_Strategic_Deployment_Network__c> Champ = new List<IPO_Strategic_Deployment_Network__c>();
          Champ = [select id,Scope__c, Entity__c,Champion__r.Firstname,Champion__r.Lastname, Deployment_Leader__r.FirstName, Deployment_Leader__r.LastName,Deployment_Leader__r.Email from IPO_Strategic_Deployment_Network__c where IPO_Strategic_Program_Name__c = :IPOCMile[0].Program_Name__c];
          for(IPO_Strategic_Cascading_Milestone__c CMil:IPOCMile){
           CMil.ValidateDeployementLeader__c = false;
           countfor=0;
             for(IPO_Strategic_Deployment_Network__c Ch: Champ){
             Count=0;            
             
            If(CMil.Scope__c.Contains(Ch.Scope__c) && (CMil.Entity__c.Contains(Ch.Entity__c))) {      
             if((CMil.Scope__c != null)&& (CMil.Entity__c !=null)){
               If(CMil.Scope__c.Contains(Ch.Scope__c) && (CMil.Entity__c.Contains(Ch.Entity__c))) {
                if(Ch.Champion__c != null)
                   CMil.Champion_Name__c = Ch.Champion__r.Firstname + ' ' + Ch.Champion__r.Lastname; 
                                     
                    }
                
               if(!CMil.Entity__c.Contains(Label.Connect_GSC)&& (!CMil.Entity__c.Contains(Label.Connect_Partner))) {
             
                If(CMil.Scope__c.Contains(Ch.Scope__c) && (CMil.Entity__c.Contains(Ch.Entity__c))) {
                   //System.Debug('Entity --' + CMil.GSC_Region__c + '****' + Ch.GSC_Region__c + ' ' + CMil.Entity__c + ' '+ Ch.Entity__c + CMil.Program_Scope__c + ' ' + Ch.Team_Name__c + Ch.Team_Member__r.LastName);
                    
                     //checking for two or more deployment leaders for single entity
                    
                    if(CMil.Deployment_Leader__c != Ch.Deployment_Leader__r.id){
                    
                     //comparing cascading milestone deployment leader with Deployment Network(if we have 2 or more deployemnt leaders for entity) 
                     
                                  for(IPO_Strategic_Deployment_Network__c C2: Champ){
                                     if((CMil.Deployment_Leader__c == C2.Deployment_Leader__r.id) && (CMil.Entity__c.Contains(C2.Entity__c))){
                                        Count=Count+1;
                                       }
                                     }
                                     if(Count==0)
                                         {
                                        CMil.ValidateDeployementLeader__c = true;
                                       }
                                    }
                               }
                           }  
                                                               
                   countfor=countfor+1; 
                     } // Scope Validation If Ends 
                               
                    }
                  
                  } // For loop 1 Ends
                  if(Countfor==0 && CMil.Deployment_Leader__c !=null)
                  {
                   CMil.ValidateDeployementLeader__c = true;
                   }        
                  
                 } // For loop 2 Ends
                 
         list<IPO_Strategic_Deployment_Network__c> DL = new List<IPO_Strategic_Deployment_Network__c>();
       if(IPOCMile[0].Deployment_Leader__c != IPOCMileold[0].Deployment_Leader__c){  
                 
          DL = [select id,Scope__c, Entity__c,Champion__r.Firstname,Champion__r.Lastname, Deployment_Leader__r.FirstName, Deployment_Leader__r.LastName from IPO_Strategic_Deployment_Network__c where IPO_Strategic_Program_Name__c = :IPOCMile[0].Program_Name__c and Deployment_Leader__r.id = :IPOCMile[0].Deployment_Leader__r.id ];      
           if(DL.size() > 0)
            IPOCMile[0].Champion_Name__c = DL[0].Champion__r.Firstname + DL[0].Champion__r.Lastname;
            }  
     
                } // Static Class Ends
                
     
                
    
              
              Public Static void IPOCascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity(List<IPO_Strategic_Cascading_Milestone__c> CM)
              {
              
               
                  System.Debug('****** CascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity Start ****');   
                        
                  List<IPO_Strategic_Milestone__c> IPOMilestone = new List<IPO_Strategic_Milestone__c>();
                  IPOMilestone = [Select Name, IPO_Strategic_Milestone_Name__c, Global_Functions__c, Operation_Regions__c, Global_Business__c from IPO_Strategic_Milestone__c];
                  integer GF; integer GB; integer PR; 
                  for(IPO_Strategic_Cascading_Milestone__c C:CM){
                  GF = 0;
                  GB = 0;
                  PR = 0;
                  C.ValidateScopeonInsertUpdate__c = false;
                  for(IPO_Strategic_Milestone__c M: IPOMilestone){
                         if(C.IPO_Strategic_Milestone_Name__c == M.IPO_Strategic_Milestone_Name__c){
                           if(M.Global_Functions__c !=null){
                             if((C.Scope__c == Label.Connect_Global_Functions) && !(M.Global_Functions__c.Contains(C.Entity__c)))
                               GF = GF + 1;
                            }
                            else if((M.Global_Functions__c == null)&& (C.Scope__c == Label.Connect_Global_Functions))
                            GF = GF + 1;
                           
                           if(M.Global_Business__c !=null){
                             if((C.Scope__c == Label.Connect_Global_Business) && !(M.Global_Business__c.Contains(C.Entity__c)))
                               GB = GB + 1;
                             }
                             else if((M.Global_Business__c == null) && (C.Scope__c == Label.Connect_Global_Business))
                             GB = GB + 1;
                             
                           if(M.Operation_Regions__c !=null) { 
                               if((C.Scope__c == Label.Connect_Power_Region) && !(M.Operation_Regions__c.Contains(C.Entity__c)))
                               PR = PR + 1;
                              }
                               else if((M.Operation_Regions__c == null) && (C.Scope__c == Label.Connect_Power_Region))
                               PR = PR + 1;
                              
                              
                          
                               
                               } // if Ends
                               
                                 
                               //System.Debug('GSC Value ' + GSC + M.GSC_Regions__c + 'Entity ' + C.Entity__c);
                               if((GF > 0) ||  (GB > 0) ||  (PR > 0) )
                                   C.ValidateScopeonInsertUpdate__c = True;
                               
                              } // For Ends
                             } //For Ends
                             
                             
                System.Debug('****** CascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity Stop ****');                
                  
              }
              
           // Insert / Update Current Fields
           
      Public Static void IPOStrategicCascadingMilestoneBeforeInsert(List<IPO_Strategic_Cascading_Milestone__c> IPOMile)
       {
      
          System.Debug('****** ConnectCascadingMilestoneBeforeInsert Start ****');
          if (IPOMile[0].Progress_Summary_Deployment_Leader_Q4__c != null){
              IPOMile[0].CurrentProgressSummary_Deployment_Leader__c = IPOMile[0].Progress_Summary_Deployment_Leader_Q4__c;
              IPOMile[0].Current_Status__c = IPOMile[0].Status_in_Quarter_Q4__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q4__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q4__c;
              }
          else if (IPOMile[0].Progress_Summary_Deployment_Leader_Q3__c != null){
                  IPOMile[0].CurrentProgressSummary_Deployment_Leader__c = IPOMile[0].Progress_Summary_Deployment_Leader_Q3__c;
                  IPOMile[0].Current_Status__c = IPOMile[0].Status_in_Quarter_Q3__c;
                  IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q3__c;
                  IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q3__c;
                  }
          else if (IPOMile[0].Progress_Summary_Deployment_Leader_Q2__c != null){
                  IPOMile[0].CurrentProgressSummary_Deployment_Leader__c = IPOMile[0].Progress_Summary_Deployment_Leader_Q2__c;
                  IPOMile[0].Current_Status__c = IPOMile[0].Status_in_Quarter_Q2__c;
                  IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q2__c;
                  IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q2__c;
               }
          else if (IPOMile[0].Progress_Summary_Deployment_Leader_Q1__c != null){
              IPOMile[0].CurrentProgressSummary_Deployment_Leader__c = IPOMile[0].Progress_Summary_Deployment_Leader_Q1__c;
              IPOMile[0].Current_Status__c = IPOMile[0].Status_in_Quarter_Q1__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q1__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q1__c;
              }
            
          }
          
 Public Static void IPOStrategicCascadingMilestoneBeforeUpdate(List<IPO_Strategic_Cascading_Milestone__c> IPOMile, List<IPO_Strategic_Cascading_Milestone__c> IPOMileold)
      {
      // Set the Current Fields 
      
          System.Debug('****** ConnectCascadingMilestoneBeforeInsert Start ****');
              IPOMile[0].CurrentProgressSummary_Deployment_Leader__c = IPOMile[0].Progress_Summary_Deployment_Leader_Q1__c;
              IPOMile[0].Current_Status__c = IPOMile[0].Status_in_Quarter_Q1__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q1__c;         
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q1__c;
              
          if (IPOMile[0].Progress_Summary_Deployment_Leader_Q4__c != 'No Data' && (IPOMile[0].Progress_Summary_Deployment_Leader_Q4__c != null)){
              IPOMile[0].CurrentProgressSummary_Deployment_Leader__c = IPOMile[0].Progress_Summary_Deployment_Leader_Q4__c;
              IPOMile[0].Current_Status__c = IPOMile[0].Status_in_Quarter_Q4__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q4__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q4__c;
              }
          else if (IPOMile[0].Progress_Summary_Deployment_Leader_Q3__c != 'No Data' && IPOMile[0].Progress_Summary_Deployment_Leader_Q3__c != null){
                  IPOMile[0].CurrentProgressSummary_Deployment_Leader__c = IPOMile[0].Progress_Summary_Deployment_Leader_Q3__c;
                  IPOMile[0].Current_Status__c = IPOMile[0].Status_in_Quarter_Q3__c;
                  IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q3__c;
                  IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q3__c;
                  }
          else if (IPOMile[0].Progress_Summary_Deployment_Leader_Q2__c != 'No Data' && IPOMile[0].Progress_Summary_Deployment_Leader_Q2__c !=null){
                  IPOMile[0].CurrentProgressSummary_Deployment_Leader__c = IPOMile[0].Progress_Summary_Deployment_Leader_Q2__c;
                  IPOMile[0].Current_Status__c = IPOMile[0].Status_in_Quarter_Q2__c;
                  IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q2__c;
                  IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q2__c;
               }
          else if (IPOMile[0].Progress_Summary_Deployment_Leader_Q1__c != 'No Data' && IPOMile[0].Progress_Summary_Deployment_Leader_Q1__c != null){
              IPOMile[0].CurrentProgressSummary_Deployment_Leader__c = IPOMile[0].Progress_Summary_Deployment_Leader_Q1__c;
              IPOMile[0].Current_Status__c = IPOMile[0].Status_in_Quarter_Q1__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q1__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q1__c;
              }
           }   
     
     //auto updation of Cascading Milestone status in all future quarters.
     
     public static void IPOCascadingMilestoneUpdate_StatusUpdate(List<IPO_Strategic_Cascading_Milestone__c> CMiles){
         for(IPO_Strategic_Cascading_Milestone__c CM : CMiles){
             if(CM.Progress_Summary_Deployment_Leader_Q1__c != null && CM.Progress_Summary_Deployment_Leader_Q1__c == 'Completed'){
                 CM.Progress_Summary_Deployment_Leader_Q2__c = CM.Progress_Summary_Deployment_Leader_Q1__c;
                 CM.Progress_Summary_Deployment_Leader_Q3__c = CM.Progress_Summary_Deployment_Leader_Q1__c;
                 CM.Progress_Summary_Deployment_Leader_Q4__c = CM.Progress_Summary_Deployment_Leader_Q1__c;    
             }
             if(CM.Progress_Summary_Deployment_Leader_Q2__c != null && CM.Progress_Summary_Deployment_Leader_Q2__c == 'Completed'){
                 CM.Progress_Summary_Deployment_Leader_Q3__c = CM.Progress_Summary_Deployment_Leader_Q2__c;
                 CM.Progress_Summary_Deployment_Leader_Q4__c = CM.Progress_Summary_Deployment_Leader_Q2__c;
             }
             if(CM.Progress_Summary_Deployment_Leader_Q3__c != null && CM.Progress_Summary_Deployment_Leader_Q3__c == 'Completed'){
                 CM.Progress_Summary_Deployment_Leader_Q4__c = CM.Progress_Summary_Deployment_Leader_Q3__c;
             }
         }  
     }
     
}