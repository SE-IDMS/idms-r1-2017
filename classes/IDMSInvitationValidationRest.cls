//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This is Global class exposing Send Invitation custom REST API. 
//                  Apex REST End point with PUT method which accepts email id, invitation id and redirect url.  
//                  It returns http response with status code.
//
//*******************************************************************************************


@RestResource(urlMapping='/SendInvitation/*')

global with sharing class IDMSInvitationValidationRest {
    
    //This is http put method which take input from IFW and send Invitation to requested email with redirect Url. 
    @HttpPut  
    global static IDMSResponseWrapperInvitation  doPut(String email, String InvitationId, String RedirectUrl){
        IDMSInvitationValidation obj=new IDMSInvitationValidation(); 
        IDMSResponseWrapperInvitation response= obj.validateInvitation( email,  InvitationId,  RedirectUrl);
        
        RestResponse res = RestContext.response;
        //Error handling
        if(response.Message.startsWith('Email address in'))
        {
            res.statuscode=400;
        }
        if(response.Message.startsWith('Email, InvitationId')){
            res.statuscode=400;
        }
        if(response.Message.startsWith('This email id')){
            res.statuscode=400;
        }
        return response;
    }
}