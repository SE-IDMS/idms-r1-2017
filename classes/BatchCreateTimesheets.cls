global class BatchCreateTimesheets implements Database.Batchable<Sobject>, Database.Stateful {
/*
	Batchable Apex Class that takes incoming Technican Records and builds related TimeSheets.

	The class takes in a paramater of days from Today that is in the custom label SVMX_TimeSheet_Days_From_Today.  From this parameter
	todays date +/- this value is searched to see if a given technician has an existing TimeSheet for a period that falls on that day.

	If one is not found, a TimeSheet is created based on the Technicians ServiceMax Profile (determined from SFDC User Record Profile)

	If the Technician does not have a SFDC Profile or related SVMX Profile (they could be a Technician without a SFDC login), then the 
	custom field Manager is used to determine the Profile to be used.  

	If that is not found, then the Org-Wide Default Profile is used to determine what Timesheet to create.

*/
    
    public String query;
    public Integer daysFromToday;
    global Integer numFailures = 0; 
        
    //Start method that is called at the beginning of a batch apex job
    global database.querylocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    //Execute method is called for each batch of records
    global void execute(Database.BatchableContext BC, Sobject[] technicians) {
        
        //Checking the size of technicians
		if (technicians.size() > 0) {
            
            Set<Id> techIds = new Set<Id>();
            
            for (Sobject oTech : technicians) {

            	SVMXC__Service_Group_Members__c tech = (SVMXC__Service_Group_Members__c)oTech;
         
                // gather tech Ids
                techIds.add(tech.Id);             
            } 
           
            Set<Id> techIdsNeedingTimesheetsSet = BatchCreateTimesheetsHelper.techsNeedingTimesheet(techIds, Date.today().addDays(daysFromToday));
           
            if (!techIdsNeedingTimesheetsSet.isEmpty()) {
                
                // get SVMX Profiles
                Map<Id, SVMXC__ServiceMax_Config_Data__c> SVMXProfilesMap = BatchCreateTimesheetsHelper.returnSVMXProfiles();
                
                // get Active Org-Wide SVMX Profile Id
                Id orgWideSVMXProfileId = BatchCreateTimesheetsHelper.returnOrgWideProfileId(SVMXProfilesMap);
                
                // get Mapping between SFDC and SVMX Profile
                Map<Id, Id> SFDCSVMXMapping = BatchCreateTimesheetsHelper.returnSFDCToSVMXMapping(SVMXProfilesMap.keySet());
                
                // get Mapping between SVMX Profile and Timesheet Settings
                Map<Id, Map<String, String>> SVMXMappingToTimesheetSettings = BatchCreateTimesheetsHelper.returnSVMXProfileSettingMap (SVMXProfilesMap.keySet());
			
                List<SVMXC_Timesheet__c> newTimeSheets = new List<SVMXC_Timesheet__c>();
                
                for (Sobject oTech2 : technicians) {
                    
                    SVMXC__Service_Group_Members__c tech2 = (SVMXC__Service_Group_Members__c)oTech2;
                    
                    if (techIdsNeedingTimesheetsSet.contains(tech2.Id)) {
                        
                        SVMXC_Timesheet__c newTimeSheet = new SVMXC_Timesheet__c();
                        
                        // this tech needs a new timesheet
                        if (tech2.SVMXC__Salesforce_User__r.ProfileId != null &&
                            SVMXMappingToTimesheetSettings.containsKey(SFDCSVMXMapping.get(tech2.SVMXC__Salesforce_User__r.ProfileId))) {
                            
                            // build Timesheet using Settings for User SVMX Profile
                            newTimeSheet = BatchCreateTimesheetsHelper.buildTimeSheet(
                                	SVMXMappingToTimesheetSettings.get(SFDCSVMXMapping.get(tech2.SVMXC__Salesforce_User__r.ProfileId)),
                                	tech2.Id, Date.today().addDays(daysFromToday));
                                 
                        } else if (tech2.Manager__r.ProfileId != null && 
                                   SVMXMappingToTimesheetSettings.containsKey(SFDCSVMXMapping.get(tech2.Manager__r.ProfileId))) {
                        	
                        	// build Timesheet using Settings for Manager SVMX Profile
                        	newTimeSheet = BatchCreateTimesheetsHelper.buildTimeSheet(
                                	SVMXMappingToTimesheetSettings.get(SFDCSVMXMapping.get(tech2.Manager__r.ProfileId)),
                                	tech2.Id, Date.today().addDays(daysFromToday));
                                       
                        } else if (SVMXMappingToTimesheetSettings.containsKey(orgWideSVMXProfileId)) {
                           
                            // build Timesheet using Setting for Org-Wide Default Profile
                            newTimeSheet = BatchCreateTimesheetsHelper.buildTimeSheet(
                                	SVMXMappingToTimesheetSettings.get(orgWideSVMXProfileId),
                                	tech2.Id, Date.today().addDays(daysFromToday));
						}
                        
                        if (newTimeSheet.Start_Date__c != null)
                            newTimeSheets.add(newTimeSheet);
                        else 
                            System.debug('#### Timesheet will not be created because there was an issue creating it for Tech Id ' +tech2.Id);
                        
                    }
                } 
                if (!newTimesheets.isEmpty())
                    insert newTimeSheets;
            }
		}       
    }      
                                     
    global void finish(Database.BatchableContext BC) {
        //Method for post processing operations
        
        // Get the ID of the AsyncApexJob representing this batch job  
        // from Database.BatchableContext.  
        // Query the AsyncApexJob object to retrieve the current job's information.  
    
       AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
          TotalJobItems, CreatedBy.Email
          from AsyncApexJob where Id =
          :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.  
        
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {'scott.fawcett@servicemax.com'};
       mail.setToAddresses(toAddresses);
       mail.setSubject('BatchCreateTimesheets Job Status: ' + a.Status);
       mail.setPlainTextBody
       ('The batch Apex job processed ' + a.TotalJobItems +
       ' batches with '+ numFailures + ' record failures.');
       // must enable single email permissions for this to work.
       //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });        
    }    
}