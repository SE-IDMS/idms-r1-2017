@isTest
private class SyncObjectsTest {
    /*
      the syncObjects class queries from all the FROM records. no condition specified
    */
    static testMethod void testBulkQuering(){

    ObjectSync__c conf = SyncObjectsTestUtil.createContactAccountBulk(null,null);

    //correr syncronizacion
    SyncObjects instance = new SyncObjects(conf); //se instancia y ve los mapeos
    SyncObjectsResult configurationResult = instance.currentState; //resultado global de la cfg y los mapeos
    
    //assert 1: no errors on constructor
    System.assertEquals(configurationResult.error, false);

    boolean allOrNone = false; //misma funcionalidad que en el Database.Update
    SyncObjectsResult result = instance.syncronize(allOrNone); //sincroniza
    
    //assert 1: no errors on syncronize
    System.assertEquals(result.error, false);
    
    //asserts 2: matchs all records
    for (Contact con : [select firstname,lastname,title, account.name, account.site, account.sicDesc from Contact]) {
      System.assertEquals (con.firstName, con.account.name);
      System.assertEquals (con.Lastname, con.account.SicDesc);
    }
    Map<Id,sObject> oldMps = new Map<Id,sObject>();
    instance.filterRecords(oldMps);
    }

    /*
      the syncObjects recieves the records. no condition specified
    */
    static testMethod void testBulkBasedOnRecords(){

    ObjectSync__c conf = SyncObjectsTestUtil.createContactAccountBulk(null,null);

    //correr syncronizacion
    SyncObjects instance = new SyncObjects([select id from Account]); //se instancia y ve los mapeos
    SyncObjectsResult configurationResult = instance.currentState; //resultado global de la cfg y los mapeos
    
    //assert 1: no errors on constructor
    System.assertEquals(configurationResult.error, false);

    boolean allOrNone = false; //misma funcionalidad que en el Database.Update
    SyncObjectsResult result = instance.syncronize(allOrNone); //sincroniza
    
    //assert 1: no errors on syncronize
    System.assertEquals(result.error, false);

    //asserts 2: matchs all records
    for (Contact con : [select firstname,lastname,title, account.name, account.site, account.sicDesc from Contact]) {
      System.assertEquals (con.firstName, con.account.name);
      System.assertEquals (con.Lastname, con.account.SicDesc);
    }
    Map<Id,sObject> oldMps = new Map<Id,sObject>();
    instance.filterRecords(oldMps);
    }
    
    /*
      no objectSync found
    */
    static testMethod void noSyncFound(){
    //creates a syncObjects for the object account
    ObjectSync__c conf = SyncObjectsTestUtil.createContactAccountBulk('[{"fromApiField":"Name","toApiField":"FirstName","Nillable":false,"mappingEnabled":true},{"fromApiField":"SicDesc","toApiField":"incorrectField","Nillable":false,"mappingEnabled":true}]',null);

    //correr syncronizacion
    SyncObjects instance = new SyncObjects([select id from Contact]); //se instancia y ve los mapeos
    SyncObjectsResult configurationResult = instance.currentState; //resultado global de la cfg y los mapeos
    
    //assert 1: not found the sync
    System.assertEquals(configurationResult.error, true);
    }
    
    
    /*
      Empty mapping (reason: only disabled field)
    */
    static testMethod void emptyMapping(){
    //creates a syncObjects for the object account
    ObjectSync__c conf = SyncObjectsTestUtil.createContactAccountBulk('[{"fromApiField":"Name","toApiField":"FirstName","Nillable":false,"mappingEnabled":false}]',null);

    //correr syncronizacion
    SyncObjects instance = new SyncObjects([select id from Account]); //se instancia y ve los mapeos
    SyncObjectsResult configurationResult = instance.currentState; //resultado global de la cfg y los mapeos
    
    //assert 1: error on the sync
    System.assertEquals(configurationResult.error, true);
    //assert 2: only one log (the empty error) 
    System.assertEquals(configurationResult.logs.size(), 1);
    //assert 2: empty mapping
    System.assertEquals(SyncObjectsConstants.ERROR_EMPTY_MAPPING, configurationResult.logs[0].message);
    }

    /*
      Empty mapping (reason: no field found)
    */
    static testMethod void emptyMappingFieldNotFound(){
    //creates a syncObjects for the object account
    ObjectSync__c conf = SyncObjectsTestUtil.createContactAccountBulk('[{"fromApiField":"asdasdasdasd","toApiField":"FirstName","Nillable":false,"mappingEnabled":true},{"fromApiField":"name","toApiField":"accountId","Nillable":false,"mappingEnabled":true}]',null);

    //correr syncronizacion
    SyncObjects instance = new SyncObjects([select id from Account]); //se instancia y ve los mapeos
    SyncObjectsResult configurationResult = instance.currentState; //resultado global de la cfg y los mapeos
    
    }
    
    /*
      Records to sync.exceded (SyncObjectsConstants.LIMIT_ROWS_OPERATIONS) 
    */
    static testMethod void excededRecords(){
    //creates a syncObjects for the object account
    ObjectSync__c conf = SyncObjectsTestUtil.createContactAccountBulk(null,SyncObjectsConstants.LIMIT_ROWS_OPERATIONS + 10);

    //correr syncronizacion
    SyncObjects instance = new SyncObjects([select id from Account]); //se instancia y ve los mapeos
    
    SyncObjectsResult configurationResult = instance.syncronize(false);
    //assert 1: error on the sync
    System.assertEquals(configurationResult.error, true);
    //assert 2: only one log (the limit rows exceded) 
    System.assertEquals(configurationResult.logs.size(), 1);
    //assert 2: empty mapping
    System.assertEquals(SyncObjectsConstants.ERROR_ROWS_LIMIT_EXCEDED, configurationResult.logs[0].message);
    }


    
}