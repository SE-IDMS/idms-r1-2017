global class VFC_CompetitorSearch{
    
    ApexPages.StandardController scontroller;
    Final public String queryString;
    public string sourcePage = 'Standard';
    public String opptyId;
    public String opptyStatus;
    public String opptyReason;
    public String opptyAmount;
    
    public VFC_CompetitorSearch(ApexPages.StandardController controller) 
    {
        scontroller = controller;
        PageReference thisPage = ApexPages.currentPage();
        sourcePage = System.currentPageReference().getParameters().get( 'sourcePage' );
        opptyId = System.currentPageReference().getParameters().get( Label.CL00452 );
        opptyStatus = System.currentPageReference().getParameters().get( 'status' );
        opptyReason = System.currentPageReference().getParameters().get( 'reason' );
        opptyAmount = System.currentPageReference().getParameters().get( 'amount' );
        
        if(sourcePage == 'OpptyLost')
        {
            System.debug('--------');
            OPP_OpportunityCompetitor__c oppComp = (OPP_OpportunityCompetitor__c)scontroller.getRecord();
            oppComp.Winner__c = TRUE; 
        }
        List<String> url = new list<String>();
        url = thisPage.getUrl().split('\\?');
        if(url.size() > 1)
            queryString = url[1];
        System.debug('-->> queryString : '+queryString);
            
     }

    /*Global variables*/
    public String searchedRecordId { get; set; }
    public static list<ResultSet> searchedRecord {get;set;}   
     
    @RemoteAction
    global static ResultSet[] getRecords(String searchText) {
    
        searchedRecord = new list<ResultSet>();
        //SOSL Text should be more then one charecter
        if(searchText.length() >0){
            List<Competitor__c> searchList = Database.query('SELECT Name FROM Competitor__c WHERE Name like \'' + searchtext + '%\' AND Active__c=True order by Name');  //search.query(queryString);
     
            for(Competitor__c o:searchList){
                    searchedRecord.add(new VFC_CompetitorSearch.ResultSet(o));
                
            }
        }
        return searchedRecord;
    }
      
    /*Record Wrapper*/
    global class ResultSet{
        public String Id {get;set;} 
        public String Name{get;set;}
        
        public ResultSet(sObject s){
            this.Id = s.Id;
            this.Name = s.get('Name')+'';          
        } 
    }
    
    public PageReference SaveandNew()
    {
        //Modified Error handling for DEF-10045 - June 2016 - Q2 release
        try{
           /* Pagereference pr = scontroller.save();
            if(pr != null)
            {
                System.debug('------Redirect to page----');
                pr = new PageReference('/apex/VFP_CompetitorSearch?'+queryString);
                pr.setredirect(true); //setRedirect(true);  
                return pr;
            }
            else
            {
                System.debug('------Error on page----');
                return null;
            }*/
            OPP_OpportunityCompetitor__c oppComp = (OPP_OpportunityCompetitor__c)scontroller.getRecord();
            insert oppComp ;
            Pagereference pr = new PageReference(Site.getPathPrefix()+'/apex/VFP_CompetitorSearch?'+queryString);
            pr.setredirect(true); //setRedirect(true);  
            return pr;
        }
        catch(Exception ex)
        {
            System.debug('>>>>>>'+ex);
            List<Id> extComp=new List<Id>();
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                if(ex.getDmlStatusCode(i)==Label.CLJUN16SLS16)
                    extComp.add((Id)ex.getDmlMessage(i).Right(15));
                else
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getDmlMessage(i)));
           }
           if(extComp.size()>0)
           {
               List<OPP_OpportunityCompetitor__c> compList=new List<OPP_OpportunityCompetitor__c>();
               compList=[Select TECH_CompetitorName__c,TECH_CompetitorName__r.Name from OPP_OpportunityCompetitor__c where Id in :extComp];
               for(OPP_OpportunityCompetitor__c opp:compList)
                   ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLJUN16SLS15+opp.TECH_CompetitorName__r.Name+Label.CLJUN16SLS18));
            }      
            return null;
        }              
           
            
    }
    
    public PageReference save()
    {
        //Modified Error handling for DEF-10045 - June 2016 - Q2 release
        try{
            /* Pagereference pr = scontroller.save();
            
            if(pr != null && sourcePage == 'OpptyLost')  // Page redirection from VFP_OpportunityLost
            {
                System.debug('------Redirect to VFP_OpportunityLost page----');
                System.debug('opptyId '+opptyId+' opptyStatus '+opptyStatus+' opptyReason '+opptyReason+' opptyAmount '+opptyAmount);
                String url='/apex/VFP_OpportunityLost?';
                Pagereference pref = new Pagereference(url);
                If( opptyId!= null)
                    pref.getParameters().put( 'id', opptyId);
                if (opptyStatus!=null)
                    pref.getParameters().put('status', opptyStatus);
                if (opptyReason!=null)
                    pref.getParameters().put('reason', opptyReason); 
                if (opptyAmount!=null)
                    pref.getParameters().put('amount', opptyAmount);    
                    
                pr = pref;
                pr.setredirect(true);
                System.debug('---->>>> Save Method: From VFP_OpportunityLost :: '+pr.getUrl() );                        
            }
            else if(pr != null && sourcePage == 'Standard')
            {
                System.debug('------Redirect to Competitor Detail page----');    
            }
            return pr;*/
            
            OPP_OpportunityCompetitor__c oppComp = (OPP_OpportunityCompetitor__c)scontroller.getRecord();
            insert oppComp ;
            Pagereference pref;
             pref = new Pagereference(Site.getPathPrefix()+'/'+oppComp.Id);
            if(sourcePage == 'OpptyLost')  // Page redirection from VFP_OpportunityLost
            {
                System.debug('------Redirect to VFP_OpportunityLost page----');
                System.debug('opptyId '+opptyId+' opptyStatus '+opptyStatus+' opptyReason '+opptyReason+' opptyAmount '+opptyAmount);
                String url=Site.getPathPrefix()+'/apex/VFP_OpportunityLost?';
                pref = new Pagereference(url);
                If( opptyId!= null)
                    pref.getParameters().put( 'id', opptyId);
                if (opptyStatus!=null)
                    pref.getParameters().put('status', opptyStatus);
                if (opptyReason!=null)
                    pref.getParameters().put('reason', opptyReason); 
                if (opptyAmount!=null)
                    pref.getParameters().put('amount', opptyAmount);    
                    
                pref.setredirect(true);
                System.debug('---->>>> Save Method: From VFP_OpportunityLost :: '+pref.getUrl() );                              
            }
            else if(sourcePage == 'Standard')
            {
                System.debug('------Redirect to Competitor Detail page----');    
            }
            return pref; 
        }
        catch(Exception ex)
        {
            System.debug('>>>>>>'+ex);
            List<Id> extComp=new List<Id>();
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                if(ex.getDmlStatusCode(i)==Label.CLJUN16SLS16)
                    extComp.add((Id)ex.getDmlMessage(i).Right(15));
                else
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getDmlMessage(i)));
           }
           if(extComp.size()>0)
           {
               List<OPP_OpportunityCompetitor__c> compList=new List<OPP_OpportunityCompetitor__c>();
               compList=[Select TECH_CompetitorName__c,TECH_CompetitorName__r.Name from OPP_OpportunityCompetitor__c where Id in :extComp];
               for(OPP_OpportunityCompetitor__c opp:compList)
                   ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLJUN16SLS15+opp.TECH_CompetitorName__r.Name+Label.CLJUN16SLS18));
            }      
            return null;
        }            
           
    }
}