@istest
public class AP_ASHupdateEmail_Test{
    static testmethod void updateEmailUnitTest(){
        user u1 = [select id,name, email,LastName,UserName from user where id=:UserInfo.getUserID()];
       
        list<country__c> ctrylist = new list<country__c>();
        country__c c = new country__c(name = 'India',countrycode__c = 'IN');
        ctrylist.add(c);
        country__c c1 = new country__c(name = 'Italy',countrycode__c = 'IT');
        ctrylist.add(c1);
        Insert ctrylist;
          //Announcement Groups
        list<Announcement_Groups__c> annclist = new list<Announcement_Groups__c>();
        Announcement_Groups__c ag= new Announcement_Groups__c(Country__c=c.id,group_name__c ='Group-1');
        annclist.add(ag);
         Announcement_Groups__c ag1= new Announcement_Groups__c(Country__c=c1.id,group_name__c ='Group-2');
         annclist.add(ag1);
        Insert  annclist;
        // insert announcement Stakeholders
        list<AnnouncementStakeholder__c> anncSh = new list<AnnouncementStakeholder__c>();
        AnnouncementStakeholder__c ash1 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id, Email__c ='xyz@abc.com' );
        anncSh.add(ash1);
        AnnouncementStakeholder__c ash2 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag.id,Email__c ='xyz123@abc.com');
        anncSh.add(ash2);
        AnnouncementStakeholder__c ash3 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id, Email__c ='123xyz@abc.com');
        anncSh.add(ash3);
       AnnouncementStakeholder__c ash4 = new AnnouncementStakeholder__c(AnnouncementGroup__c = ag1.id, UserName__c =u1.id );
        anncSh.add(ash4);
        insert anncSh;
        
        ash3.Email__c = 'abcdefgh@xyz.com';
        update ash3;
    }
}