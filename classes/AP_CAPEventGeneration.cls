/* 
Called from VFP_AdjustVisit
logic copied frm VFC06_SFEGenerateActivities
*/
public class AP_CAPEventGeneration implements Queueable {
    Map<Id,Integer> adjneg=new Map<Id,Integer>();
    Map<Id,Integer> adjpos=new Map<Id,Integer>();
    Date dat;
    Id AssignedToId;
    public AP_CAPEventGeneration(Map<Id,Integer> adjneg,Map<Id,Integer> adjpos,Date dat, Id AssignedToId){
        this.adjneg=adjneg;
        this.adjpos=adjpos;
        this.dat=dat;
        this.AssignedToId=AssignedToId;
    } 
    public void execute(QueueableContext context) {
        List<Event> upsertEventList=new List<Event>();
           if(adjneg.size()>0){
              List<Event> eveList=[select AccountId,What.Name,WhatId,Status__c from Event where OwnerId=:AssignedToId and Activitydate=THIS_YEAR and What.Type = 'Account' and  RecordTypeId =:Label.CLMAY13SLS01  and Status__c!='Closed'  and  Status__c!='Cancelled' and AccountId in :adjneg.keyset() Order by EndDateTime DESC];
              System.debug('eveList>>>>***'+eveList);
              Map<Id,List<Event>> eveMap=new Map<Id,List<Event>>();
              for(Event ev:eveList){
                  if(eveMap.ContainsKey(ev.AccountId))
                      eveMap.get(ev.AccountId).add(ev);
                   else
                       eveMap.put(ev.AccountId,new List<Event>{(ev)});
              }
              System.debug('eveMap>>>>***'+eveMap);
              for(Id accId:adjneg.keyset()){
                  Integer adjust=adjneg.get(accId);
                  adjust=adjust*(-1);
                  System.debug('adjust>>>>***'+adjust);
                  System.debug('accId>>>>***'+accId);
                  System.debug('eveMap.ContainsKey(accId)>>>>***'+eveMap.ContainsKey(accId));
                  if(eveMap.ContainsKey(accId)){
                  for(Event evet:eveMap.get(accId)){
                  System.debug('adjustinsidefor>>>>***'+adjust);
                  System.debug('evet>>>>***'+evet);
                      if(adjust<=0){
                           System.debug('adjustinsideforif>>>>***'+adjust);
                          break;
                       }
                      evet.Status__c ='Cancelled';
                      upsertEventList.add(evet);
                      adjust--;
                      System.debug('upsertEventList-->>>>***'+upsertEventList);
                      System.debug('adjust-->>>>***'+adjust);
                  }
                }
              }
           }
           if(adjpos.size()>0){
               for(Id acc:adjpos.keySet()){
                        Date eventDate;
                        Date eventDateTemp; 
                        Integer DURATION = 60;
                        // days between start date and end date                    
                        Integer daysbtween = Date.Today().daysBetween(dat);
                        System.debug('...daysbtween...='+daysbtween);
                        Integer j=adjpos.get(acc);
                        for(Integer i=0;i<j;i++)
                        {
                            if(i==0) //first Event
                            {
                                if(date.Today().day()==1) 
                                {
                                    system.debug('...in if 1..');
                                    eventDate = date.today();
                                }
                                else
                                {
                                    system.debug('...in else  ..');
                                    eventDate = date.today().toStartOfMonth();
                                }
                                if(eventDate < date.today())
                                {
                                    system.debug('...in if 2..');
                                    eventDate = (eventDate).addMonths(1).toStartOfMonth();
                                }
                                System.debug('...eventDate 1...='+eventDate);
                                upsertEventList.add(new Event(Subject = System.Label.CL00002,StartDateTime = eventDate, WhatId = acc, DurationInMinutes = DURATION, RecordTypeId = Label.CLMAY13SLS01, Status__c = System.Label.CL00003,OwnerId=this.AssignedToId,SalesEventType__c = Label.CL00002)); 
                            }
                            else //from 2nd Event
                            {                               
                                Integer cntEvntDay = Integer.valueOf(i * (daysbtween/j));
                                system.debug('....cntEvntDay....'+cntEvntDay);
                                eventDate=(date.today()).addDays(cntEvntDay);
                                system.debug('....eventDate 00....'+eventDate);
                                if(eventDate.day()==1) 
                                {
                                    system.debug('...in if 1..');
                                }
                                else
                                {
                                    system.debug('...in else  ..');
                                    eventDate = eventDate.toStartOfMonth();
                                }
                                if(eventDate < date.today())
                                {
                                    system.debug('...in if 2..');
                                    eventDate = (eventDate).addMonths(1).toStartOfMonth();
                                }
                               
                                
                                System.debug('...eventDate else...='+eventDate);
                                
                                upsertEventList.add(new Event(Subject = System.Label.CL00002,StartDateTime = eventDate, WhatId = acc, DurationInMinutes = DURATION, RecordTypeId = Label.CLMAY13SLS01, Status__c = System.Label.CL00003,OwnerId=this.AssignedToId,SalesEventType__c = Label.CL00002)); 
                                system.debug(upsertEventList+'... Events List ... ');
                            }                           
                        }                       
                    }
                }
                if(!upsertEventList.isEmpty()){
                    Database.UpsertResult[] results=Database.upsert(upsertEventList, false);
                    system.debug(results + '... upserts results ... '); 
               }
        }
}