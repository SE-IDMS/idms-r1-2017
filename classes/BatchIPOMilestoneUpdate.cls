/*
    Author              : Priyanka Shetty (Schneider Electric)
    Date Created        :  26-Jul-2013
    Description         : Batch class to update IPO Milestones to make Current Quarter fields populated through workflows
*/
global class BatchIPOMilestoneUpdate implements Database.Batchable<sObject>{

global String query='select Id from IPO_Strategic_Milestone__c';

global database.querylocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query);}

global void execute(Database.BatchableContext BC, List<sObject> scope){
    List<IPO_Strategic_Milestone__c> ms = new List<IPO_Strategic_Milestone__c>();
    for(sObject s : scope){
            IPO_Strategic_Milestone__c a = (IPO_Strategic_Milestone__c)s;
            ms .add(a);
    }

update ms;   
}
global void finish(Database.BatchableContext BC){
}

}