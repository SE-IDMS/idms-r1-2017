@isTest
private class WS_SDH_Connector_TEST {
    static testMethod void SDH_ConnectorTest() {
        
        Country__c  county = Utils_TestMethods.createCountry();
        insert county;
        StateProvince__c sp = Utils_TestMethods.createStateProvince(county.id);
        insert sp;
        CustomerCareTeam__c  customercareTeam = Utils_TestMethods.CustomerCareTeam(county.id);
        insert customercareTeam ;
        Account account = Utils_TestMethods.createAccount(userinfo.getUserId());
        account.SEAccountID__c = '12345';
        //account.Preferred_CC_Team__c = customercareTeam.id;
        insert account;
        contact contact = Utils_TestMethods.createContact(account.id,'TestContact');
        contact.SEContactID__c = '98765';
        insert contact;
        LegacyAccount__c legacyAccount = Utils_TestMethods.createLegacyAccount(account.id);
        legacyAccount.LegacyKey__c ='345678';
        insert legacyAccount;       
        User user= Utils_TestMethods.createStandardUser('zxy') ;
        user.federationIdentifier = 'SESA000001';
        insert user;
        WS_SDH_Connector.GetInfoObject(new List<String>{account.id},'Account');
        WS_SDH_Connector.GetBfoCountryInfo();
        WS_SDH_Connector.GetBfoServerTime();
        WS_SDH_Connector.GetInfoObject(new List<String>{account.SEAccountID__c},'Account');
        WS_SDH_Connector.GetInfoObject(new List<String>{contact.id},'Contact');
        WS_SDH_Connector.GetInfoObject(new List<String>{legacyAccount.id},'LegacyAccount__c');
        WS_SDH_Connector.GetInfoObject(new List<String>{legacyAccount.LegacyKey__c},'LegacyAccount__c');
        WS_SDH_Connector.GetInfoObject(new List<String>{user.federationIdentifier},'User');
        WS_SDH_Connector.GetInfoObject(new List<String>{user.id},'User');        
        WS_SDH_Connector.GetInfoObject(new List<String>{sp.id+':'+county.id},'StateProvince__c');
       // WS_SDH_Connector.GetInfoObject(new List<String>{sp.id+':'},'StateProvince__c');
        WS_SDH_Connector.GetInfoObject(new List<String>{sp.StateProvinceCode__c+':'+county.CountryCode__c},'StateProvince__c');
        //WS_SDH_Connector.GetInfoObject(new List<String>{sp.StateProvinceCode__c+':'},'StateProvince__c');
        //negative testing
        
        
        
        
    }

}