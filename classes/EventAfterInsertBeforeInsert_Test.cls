/**

 *Test Class for below triggers
  
     * EventAfterInsert
     * EventBeforeDelete
     * SVMXEventAfterInsert
     * SVMXEventBeforeDelete
 
 */
@isTest(SeeAllData=true)
private class EventAfterInsertBeforeInsert_Test {

    static testMethod void myUnitTest() {
    
       system.runAs(getUser())
      {
       
       //Country__c country = [select id,name from country__c where CountryCode__c = 'FR'];
    
     Profile p1 = [select id from profile where name = 'System Administrator' limit 1];
        
        
       Account Acc = new Account    (           Name='ACC001',                                   
                                                    POBox__c= 'POBox1',
                                                    POBoxZip__c='POBoxZip',
                                                    ZipCode__c = '11111');
        insert Acc;
        
        Account ac1 = new Account   (
                                                    Name ='Account',
                                                    Street__c='ABC',
                                                    City__c='PQR ',
                                                    //country__c = country.id,
                                                    POBox__c= 'POBox2',
                                                    POBoxZip__c='POBoxZip2',
                                                    ZipCode__c = '11111'
                                     ); 
           
         insert ac1;
                                       
        //Contact test data
        
        Contact Cnct = new Contact(
                                                    AccountId=Acc.Id,
                                                    email='UTCont@mail.com',
                                                    CorrespLang__c='EN',
                                                    phone='1234567890',
                                                    LastName='UTContact001',
                                                    FirstName='Fname1');
        insert Cnct;   
        
        //Case test data
        
        Case cs1 = new Case(
                            
                                                    AccountId=Acc.id,
                                                    ContactId=Cnct.id,
                                                    Status='New',
                                                    Origin='Email');
        insert cs1;
        
         //Product test data
         
                   Product2 prod =    new Product2( ProductCode='UTProd001',
                                                    Name='UTProd001',
                                                    Family='Oncology',
                                                    SVMXC__Product_Line__c='Desktop',
                                                    IsActive=true);
        insert prod;
        
                    Product2 prod1 = new Product2(   ProductCode='UTProd002',
                                                     Name='UTProd002',
                                                     Family='Oncology',
                                                     SVMXC__Product_Line__c='Desktop',
                                                     IsActive=true);
        insert prod1;
        
         //Location test data
        
        /*SVMXC__Site__c loc1=    new SVMXC__Site__c(
                                                     AddressLine2__c='Add Lin 1',
                                                     SVMXC__City__c='Bangalore',                                                   
                                                     HealthAndSafety__c='Drug Test',
                                                     SVMXC__Account__c=ac1.id,
                                                     SVMXC__Zip__c='1234'    );
        insert loc1;*/
         //Service Team test data
        
        SVMXC__Service_Group__c st1= new SVMXC__Service_Group__c(
                                                     Name='St1',
                                                     SVMXC__Active__c=true,
                                                     SVMXC__Group_Type__c='Internal',
                                                     SVMXC__Group_Code__c='test213');
        insert st1;                                          

        User newUser = new User(alias = 'alias1', 
                                email = 'maxexpress@servicemax.com', 
                                emailencodingkey = 'UTF-8', 
                                lastname = 'ps.servicemax@gmail.com',
                                languagelocalekey = 'en_US',    
                                localesidkey = 'en_US', 
                                profileid = p1.id, 
                                timezonesidkey = 'America/Los_Angeles', 
                                username = 'svmx3@testorg.com'
                                //UserRoleId= ur.Id,  
                               );

        //insert newUser; 
        
        
        
        
        
        SVMXC__Territory__c ter1=           new SVMXC__Territory__c(
                                                     Name ='pter');
        insert ter1;                                             
                                                       
 
 SVMXC__Territory__c ter2=              new          SVMXC__Territory__c(
                                                     Name ='pter2'  );
        insert ter2;                                                                                                                                                        
                                                                                                                    
 SVMXC__Service_Group_Members__c tech1= new          SVMXC__Service_Group_Members__c(
                                                     SVMXC__Service_Group__c=st1.id,
                                                     Name='John',
                                                     SVMXC__Active__c=true,
                                                     SVMXC__Salesforce_User__c=newUser.id                                                     
                                                    );  
            
        insert tech1;
        
        
        

     

        
    WorkOrderNotification__c won1=   new            WorkOrderNotification__c(
                                                     InstalledAtAccount__c=ac1.id,
                                                     //Location__c=loc1.id,
                                                     Work_Order_Status__c='open',
                                                     ContactLastName__c='WNContact',
                                                     ContactPhoneNumber__c='984456565',
                                                     Scheduled_Date_Time__c=system.now(),   
                                                     Case__c=cs1.id );
        //insert won1 ;           
        
    SVMXC__Service_Order__c WO =     new            SVMXC__Service_Order__c(                                        
                                                     SVMXC__Company__c=Acc.Id,
                                                     SVMXC__Order_Status__c = 'Open', 
                                                     BackOfficeReference__c='123',  
                                                     SVMXC__Product__c = prod.id,
                                                     SVMXC__Contact__c = Cnct.Id, 
                                                     SVMXC__Primary_Territory__c=ter1.id,
                                                     RescheduleReason__c='Weather Related',
                                                     CustomerTimeZone__c= 'Europe/London',
                                                     Service_Business_Unit__c='Energy',
                                                     Parent_Work_Order__c = null,
                                                     Is_Billable__c=true,
                                                     SVMXC__Locked_By_DC__c=true,
                                                     SVMXC__Group_Member__c=tech1.id,
                                                     Work_Order_Notification__c=won1.id,
                                                     Customer_Service_Request_Time__c= system.now());
       // insert WO;
        
        
          SVMXC__Service_Order__c WO4 =      new SVMXC__Service_Order__c(                                       
                                                     SVMXC__Company__c=Acc.Id,
                                                     SVMXC__Order_Status__c = 'Waiting for PO', 
                                                     BackOfficeReference__c='123',  
                                                     SVMXC__Product__c = prod.id,
                                                     SVMXC__Contact__c = Cnct.Id, 
                                                     SVMXC__Group_Member__c=tech1.id, //uncommented on 09-09-11
                                                     RescheduleReason__c='Weather Related',
                                                     Service_Business_Unit__c='Energy',
                                                     CustomerTimeZone__c= 'Europe/London',
                                                     Parent_Work_Order__c = null,
                                                     Work_Order_Notification__c=won1.id,
                                                     Is_Billable__c=true,
                                                     SVMXC__Locked_By_DC__c=true,
                                                     Customer_Service_Request_Time__c= system.now());
       // insert WO4;
       
        
                  SVMXC__Service_Order__c WO5 =      new SVMXC__Service_Order__c(                                       
                                                     SVMXC__Company__c=Acc.Id,
                                                     SVMXC__Order_Status__c = 'Customer Confirmed', 
                                                     BackOfficeReference__c='123',  
                                                     SVMXC__Product__c = prod.id,
                                                     SVMXC__Contact__c = Cnct.Id, 
                                                    // SVMXC__Group_Member__c=tech4.id,
                                                     RescheduleReason__c='Weather Related',
                                                      CustomerTimeZone__c= 'Europe/London',
                                                     Service_Business_Unit__c='Energy',
                                                     Parent_Work_Order__c = null,
                                                     Is_Billable__c=true,
                                                     SVMXC__Locked_By_DC__c=true,
                                                     Work_Order_Notification__c=won1.id,
                                                     Customer_Service_Request_Time__c= system.now());
       // insert WO5;
        
        
   SVMXC__Service_Order__c WO2 =     new SVMXC__Service_Order__c(                                       
                                                     SVMXC__Company__c=Acc.Id,
                                                     SVMXC__Order_Status__c = 'Rescheduled', 
                                                     BackOfficeReference__c='1234', 
                                                     SVMXC__Product__c = prod.id,
                                                     SVMXC__Primary_Territory__c=ter2.id,
                                                     SVMXC__Contact__c = Cnct.Id, 
                                                    // SVMXC__Group_Member__c=tech4.id,
                                                     RescheduleReason__c='Weather Related',
                                                     CustomerTimeZone__c= 'Europe/London',
                                                     Service_Business_Unit__c='Energy',
                                                     Parent_Work_Order__c = null,
                                                     Is_Billable__c=true,
                                                     Frequency__c=2,
                                                     FrequencyUnit__c='Days',
                                                     SVMXC__Locked_By_DC__c=true,
                                                     RecurrenceEndDate__c=system.today() +4,
                                                    // Work_Order_Notification__c=won1.id,
                                                     Customer_Service_Request_Time__c= system.now().addDays(1));
        //try
       // {
       test.starttest();
          insert WO2;
       // }
       // catch(Exception e){system.debug('Exception'+e.getMessage());}
         
         //@SVMXC__Service_Order__c WORec =  [Select Id,Name from SVMXC__Service_Order__c Where Id = :WO2.Id];
        //@system.debug('Wo Name*******'+WO2.Id+'Work Order Name***'+WORec.Name);
        
        SVMXC__Service_Group__c st= new SVMXC__Service_Group__c(Name='ste');
            insert st;

            SVMXC__Service_Group_Members__c  tech= new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c=st.id,name='test',SVMXC__Salesforce_User__c=userinfo.getuserid());
            insert tech;
        
                                    Event Ev1= new Event (  
                                                    Subject='WO-12343',
                                                    StartDateTime=system.now(),
                                                    //Datetime.newInstance(2015,10,27,9,30,0),
                                                    EndDateTime=system.now().addDays(3),
                                                    //Datetime.newInstance(2015,12,27,9,30,0),
                                                    Whatid=WO2.id,
                                                    Ownerid=userinfo.getUserId()                                                    
                                                    );
        
        
       
     insert Ev1;
        //Ev1.Ownerid=newUser.id;
        Ev1.OwnerId=userinfo.getUserId();
        Ev1.StartDateTime=system.now().addDays(2);
        try{
        update Ev1;
        
        }
        catch(Exception ex){
            system.debug('****************'+ex.getMessage());
        }
     
     Event Ev2= new Event (  
                                                    Subject='WO-12343',
                                                    StartDateTime=system.now().addDays(1),
                                                    EndDateTime=system.now().addDays(1),
                                                    Whatid=WO2.id,
                                                    Ownerid= userinfo.getUserId()                                                 
                                                    );
                                                    
    try{
        insert Ev2;
          //Ev2.OwnerId=newUser.id;
          Ev2.OwnerId=userinfo.getUserId();
       
         update Ev2;
       }  
        catch(Exception ex){
            system.debug('****************'+ex.getMessage());
        }
        
        AssignedToolsTechnicians__c tec = new AssignedToolsTechnicians__c(EventId__c=Ev1.id,WorkOrder__c=WO2.id);
        try{
         insert tec;
         //update tec;
         }
         catch (exception e){}
         test.stoptest();
     //delete Ev2;
        
           /*  SVMXC__Service_Group_Members__c tech4= new SVMXC__Service_Group_Members__c(
                                                     SVMXC__Service_Group__c=st1.id,
                                                     Name='John',
                                                 `    SVMXC__Active__c=true,
                                                     SVMXC__Salesforce_User__c=Ev1.OwnerId                                                   
                                                    );  */
            
           // insert tech4;
            
             SVMXC__SVMX_Event__c SEv1= new SVMXC__SVMX_Event__c (  
                                                    Name=WO2.Name,
                                                    SVMXC__StartDateTime__c=system.now().addDays(1),
                                                    SVMXC__EndDateTime__c=system.now().addDays(1),
                                                    SVMXC__DurationInMinutes__c=60,
                                                    SVMXC__ActivityDate__c=system.today(),
                                                    //SVMXC__Technician__c=tech4.id,
                                                    SVMXC__ActivityDateTime__c=system.now().addHours(10),
                                                    SVMXC__Location__c='testloc1',
                                                    SVMXC__WhatId__c=wo2.id,
                                                    Ownerid=Ev1.OwnerId                                                 
                                                    );
           // insert SEv1;
           // update SEv1;
            
                         SVMXC__SVMX_Event__c SEv2= new SVMXC__SVMX_Event__c (  
                                                    Name=WO2.Name,
                                                    SVMXC__StartDateTime__c=system.now().addDays(2),
                                                    SVMXC__EndDateTime__c=system.now().addDays(2),
                                                    SVMXC__DurationInMinutes__c=60,
                                                    SVMXC__ActivityDate__c=system.today(),
                                                    SVMXC__ActivityDateTime__c=system.now().addHours(10),
                                                    SVMXC__Location__c='testloc1',
                                                    SVMXC__Technician__c=tech1.id,
                                                    SVMXC__WhatId__c=wo2.id,
                                                    OwnerID =tech1.SVMXC__Salesforce_User__c
                                                    //Ownerid=UserInfo.getUserId()                                                    
                                                    );
           // insert SEv2;
            //update SEv2;
            //delete SEv2;
            
            SVMXC__Service_Group_Members__c tech5=  new SVMXC__Service_Group_Members__c(
                                                     SVMXC__Service_Group__c=st1.id,
                                                     Name='John',
                                                     SVMXC__Active__c=true,
                                                     SVMXC__Salesforce_User__c=SEv1.OwnerId                                                      
                                                    );  
            
            //insert tech5;
        
     SVMXC__Service_Order__c WO3 =   new SVMXC__Service_Order__c(                                       
                                                     SVMXC__Company__c=Acc.Id,
                                                     SVMXC__Order_Status__c = 'Service Completed', 
                                                     BackOfficeReference__c='1234', 
                                                     SVMXC__Product__c = prod.id,
                                                     Parent_Work_Order__c = null,
                                                    // SVMXC__Group_Member__c=tech4.id,
                                                     SVMXC__Contact__c = Cnct.Id, 
                                                     Work_Order_Notification__c=won1.id,
                                                     RescheduleReason__c='Weather Related',
                                                     Service_Business_Unit__c='Energy',
                                                     Is_Billable__c=true,
                                                     SVMXC__Locked_By_DC__c=true,
                                                     Customer_Service_Request_Time__c= System.now().addDays(2));
       // insert WO3;      
        
       // Test of ServicemaxUtils1
      /* BusinessHours bh = [SELECT Id, MondayEndTime, MondayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime,
                    ThursdayEndTime, ThursdayStartTime, FridayStartTime, FridayEndTime, SaturdayEndTime, SaturdayStartTime, SundayStartTime, SundayEndTime FROM BusinessHours WHERE isActive = true AND isDefault = true LIMIT 1]; 
       ServiceMaxUtils1.getNextDate(system.now().date(), 1,'Days', bh);
       ServiceMaxUtils1.getNextDate(system.now().date(), 1, 'Weeks', null);
       ServiceMaxUtils1.getNextDate(system.now().date(), 1, 'Months', bh);*/
       Set<ID> woIdList = new Set<ID>();
       woIdList.add(WO2.id);
      string wherclause='WHERE id IN :'+woIdList +'LIMIT 5';
      try{
      //ServiceMaxUtils1.getCustomObjects('SVMXC__Service_Order__c','wherclause',woIdList);
      
      
        ServiceMaxUtils1.getCustomObjects('Account','where id ='+Acc.id,null);
      
      }
      catch(exception e){}
      }
    }
	
	static testMethod void myUnitTest2() {
	  system.runAs(getUser())
      {
		Account acc = Utils_TestMethods.createAccount();
       	insert acc;
     
        SVMXC__Service_Order__c wo =  Utils_TestMethods.createWorkOrder(acc.id);
        wo.SVMXC__Order_Type__c='Maintenance';
        wo.CustomerRequestedDate__c = Date.today();
        wo.BackOfficeReference__c = '111111';
        wo.CustomerTimeZone__c = 'Europe/Paris';
        insert wo;
		
		User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = System.label.CLDEC14SRV01, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP30;AP_WorkOrderTechnicianNotification;SVMX05;AP54;AP_Contact_PartnerUserUpdate',
        timezonesidkey='Europe/London', username='user111' + '@bridge-fo.com',FederationIdentifier = '123456');
        insert user;
		SVMXC__Service_Group__c st= new SVMXC__Service_Group__c(Name='ste');
        insert st;
        SVMXC__Service_Group_Members__c  tech= new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c=st.id,name='test',SVMXC__Salesforce_User__c=user.id);
        insert tech;
		Event Ev1= new Event (  
			Subject=wo.name,
			StartDateTime=system.now(),
			EndDateTime=system.now().addDays(3),
			Whatid=wo.id,
			Ownerid=user.id                                                    
		);
		insert Ev1;
		AssignedToolsTechnicians__c att = new AssignedToolsTechnicians__c(EventId__c=Ev1.id,WorkOrder__c=wo.id,Status__c ='Assigned',PrimaryUser__c = true);
		insert att;
		Ev1.StartDateTime = system.now().addDays(1);
		Ev1.Ownerid = UserInfo.getUserId();
		update Ev1;
		
		
	  
	  
	  }
	
	}
    
    public static user getUser(){
        
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = System.label.CLDEC14SRV01, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP30;AP_WorkOrderTechnicianNotification;SVMX05;AP54;AP_Contact_PartnerUserUpdate',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
        return user;
    }

}