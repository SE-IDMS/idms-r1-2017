/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  01-Oct-2012
    Description         : Class for Connect Global KPI Target Triggers Assigns owner and Sharing permission for users
*/

public class ConnectGlobalKPITargetTriggers
{
        
      Public Static void ConnectGlobalKPITargetAfterInsert(List<Global_KPI_Targets__c> GKPI)
      {
                 
      // Add permission to data reporter
      
         List<Global_KPI_Targets__Share>  GlobalKPIShare = new List<Global_KPI_Targets__Share>();
         
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           
           gshare.ParentId = globalkpi.id;
           gshare.UserOrGroupId = globalkpi.Global_Data_Reporter__c;
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Corporate_Data_Reporter__c;
           GlobalKPIShare.add(gshare);
          }
         Database.insert(GlobalKPIShare,false);
         
          // Add share permission for KPI Owner
         
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           
           gshare.ParentId = globalkpi.id;
           gshare.UserOrGroupId = globalkpi.Global_KPI_Owner__c;
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Corporate_KPI_Owner__c;
           GlobalKPIShare.add(gshare);
          }
         Database.insert(GlobalKPIShare,false);
         
         // Add share permission for KPI Administrator
         
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           
           gshare.ParentId = globalkpi.id;
           gshare.UserOrGroupId = Label.CorporateKPIAdminGroup;
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Corporate_KPI_Administrators__c;
           GlobalKPIShare.add(gshare);
          }
         Database.insert(GlobalKPIShare,false);  
         
        // Add share permission for Program Leaders
         
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           system.debug('Program Name ' + globalkpi.Connect_Global_Program__c);
          if(globalkpi.Connect_Global_Program__c != null){
           gshare.ParentId = globalkpi.id;
           system.debug('Program Leader Name ' + globalkpi.Program_Leader_1__c);
           gshare.UserOrGroupId = globalkpi.Program_Leader_1__c;       
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c;
           GlobalKPIShare.add(gshare);          
           
           }
          }
         Database.insert(GlobalKPIShare,false);    
         
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           system.debug('Program Name ' + globalkpi.Connect_Global_Program__c);
          if(globalkpi.Connect_Global_Program__c != null){
           gshare.ParentId = globalkpi.id;
           system.debug('Program Leader Name ' + globalkpi.Program_Leader_2__c);
           gshare.UserOrGroupId = globalkpi.Program_Leader_2__c;       
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c;
           GlobalKPIShare.add(gshare);          
           
           }
          }
         Database.insert(GlobalKPIShare,false);  
         
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           system.debug('Program Name ' + globalkpi.Connect_Global_Program__c);
          if(globalkpi.Connect_Global_Program__c != null){
           gshare.ParentId = globalkpi.id;
           system.debug('Program Leader Name ' + globalkpi.Program_Leader_3__c);
           gshare.UserOrGroupId = globalkpi.Program_Leader_3__c;       
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c;
           GlobalKPIShare.add(gshare);          
           
           }
          }
         Database.insert(GlobalKPIShare,false);     
         
        for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();  
          if(globalkpi.Connect_Global_Program__c != null){
           gshare.ParentId = globalkpi.id;           
           gshare.UserOrGroupId = globalkpi.Initiative_Sponsor_1__c;      
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Initiative_Sponsor__c;
           GlobalKPIShare.add(gshare);          
           
           }
          }
         Database.insert(GlobalKPIShare,false);   
         
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();  
          if(globalkpi.Connect_Global_Program__c != null){
           gshare.ParentId = globalkpi.id;           
           gshare.UserOrGroupId = globalkpi.Initiative_Sponsor_2__c;      
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Initiative_Sponsor__c;
           GlobalKPIShare.add(gshare);          
           
           }
          }
         Database.insert(GlobalKPIShare,false);       
         
       }
       
    Public Static void ConnectGlobalKPITargetAfterUpdate(List<Global_KPI_Targets__c> GKPI, Global_KPI_Targets__c GKPIold)
      {
      
      System.Debug('Global KPI Updates Starts ');
      
      List<Global_KPI_Targets__Share>  GlobalKPIShare = new List<Global_KPI_Targets__Share>();
      List<Cascading_KPI_Target__Share>  EntityKPIShare = new List<Cascading_KPI_Target__Share>();
      List<Id> lstId1 = new List<Id>();
      List<Id> lstId2 = new List<Id>();
      List<Id> lstId3 = new List<Id>();
      
      // Update share permission for Data Reporter
        if(GKPIold.Global_Data_Reporter__c != GKPI[0].Global_Data_Reporter__c){  
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           
           gshare.ParentId = globalkpi.id;
           gshare.UserOrGroupId = globalkpi.Global_Data_Reporter__c;
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Corporate_Data_Reporter__c;
           GlobalKPIShare.add(gshare);
          }
         Database.insert(GlobalKPIShare,false);
         }
       
        // Update share permission for KPI Owner
       if(GKPIold.Global_KPI_Owner__c != GKPI[0].Global_KPI_Owner__c){  
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           
           gshare.ParentId = globalkpi.id;
           gshare.UserOrGroupId = globalkpi.Global_KPI_Owner__c;
           gshare.AccessLevel = 'edit';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Corporate_KPI_Owner__c;
           GlobalKPIShare.add(gshare);
          }
         Database.insert(GlobalKPIShare,false);
         }
         
     // Add Data Reporter to Entity KPI Target 
   
 if(GKPIold.Global_Data_Reporter__c != GKPI[0].Global_Data_Reporter__c){
   For(Cascading_KPI_Target__c EKPI : [Select Id from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id])
   {
  lstId1.add(EKPI.Id);
  Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
           cshare.ParentId = EKPI.id;           
           cshare.UserOrGroupId = GKPI[0].Global_Data_Reporter__c;          
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Corporate_Data_Reporter__c;
           EntityKPIShare.add(cshare);           
          }
         Database.insert(EntityKPIShare,false); 
         }
         
      // Add share permission for Program Leaders
      if(GKPI[0].Connect_Global_Program__c != null){
       if(GKPI[0].Program_Leader_1__c != GKPIold.Program_Leader_1__c){
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           system.debug('Program Name ' + globalkpi.Connect_Global_Program__c);
          
           gshare.ParentId = globalkpi.id;
           system.debug('Program Leader Name ' + globalkpi.Program_Leader_1__c);
           gshare.UserOrGroupId = globalkpi.Program_Leader_1__c;          
           gshare.AccessLevel = 'read';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c;
           GlobalKPIShare.add(gshare);      
           
          }
         Database.insert(GlobalKPIShare,false);          
                 
        }
       } 
        
     // Program Leader  1 Permission for Entity KPIs
   if(GKPI[0].Connect_Global_Program__c != null){
       if(GKPI[0].Program_Leader_1__c != GKPIold.Program_Leader_1__c){  
     For(Cascading_KPI_Target__c EKPI : [Select Id from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id])
     {
  lstId2.add(EKPI.Id);
  //system.debug('Entity KPI ' + EKPI.Id + GKPI[0].Program_Leader_1__c);
  Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
           cshare.ParentId = EKPI.id;           
           cshare.UserOrGroupId = GKPI[0].Program_Leader_1__c;          
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Program_Leaders__c;
           EntityKPIShare.add(cshare);           
          }
         Database.insert(EntityKPIShare,false); 
         }
       }
          
       
    // Program Leader 2 Permissions
    
     if(GKPI[0].Connect_Global_Program__c != null && GKPI[0].Program_Leader_2__c != null ){
       if(GKPI[0].Program_Leader_2__c != GKPIold.Program_Leader_2__c){
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           system.debug('Program Name ' + globalkpi.Connect_Global_Program__c);
          
           gshare.ParentId = globalkpi.id;
           system.debug('Program Leader Name ' + globalkpi.Program_Leader_2__c);
           gshare.UserOrGroupId = globalkpi.Program_Leader_2__c;          
           gshare.AccessLevel = 'read';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c;
           GlobalKPIShare.add(gshare);           
          }
         Database.insert(GlobalKPIShare,false); 
    
        }
       } 
          
    // Program Leader  2 Permission for Entity KPIs
   if(GKPI[0].Connect_Global_Program__c != null){
       if(GKPI[0].Program_Leader_2__c != GKPIold.Program_Leader_2__c){  
     For(Cascading_KPI_Target__c EKPI : [Select Id from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id])
     {
   lstId2.add(EKPI.Id);
  //system.debug('Entity KPI ' + EKPI.Id + GKPI[0].Program_Leader_1__c);
  Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
           cshare.ParentId = EKPI.id;           
           cshare.UserOrGroupId = GKPI[0].Program_Leader_2__c;          
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Program_Leaders__c;
           EntityKPIShare.add(cshare);           
          }
         Database.insert(EntityKPIShare,false); 
         }
       }
     
       // Program Leader 3 Permissions
    
     if(GKPI[0].Connect_Global_Program__c != null && GKPI[0].Program_Leader_3__c != null ){
       if(GKPI[0].Program_Leader_3__c != GKPIold.Program_Leader_3__c){
         for(Global_KPI_Targets__c globalkpi : GKPI){
           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           system.debug('Program Name ' + globalkpi.Connect_Global_Program__c);
           gshare.ParentId = globalkpi.id;
           system.debug('Program Leader Name ' + globalkpi.Program_Leader_3__c);
           gshare.UserOrGroupId = globalkpi.Program_Leader_3__c;          
           gshare.AccessLevel = 'read';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c;
           GlobalKPIShare.add(gshare);           
          }
         Database.insert(GlobalKPIShare,false); 
         }
       } 
          
    // Program Leader  3 Permission for Entity KPIs
   if(GKPI[0].Connect_Global_Program__c != null){
       if(GKPI[0].Program_Leader_3__c != GKPIold.Program_Leader_3__c){  
     For(Cascading_KPI_Target__c EKPI : [Select Id from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id])
     {
       lstId2.add(EKPI.Id);
  //system.debug('Entity KPI ' + EKPI.Id + GKPI[0].Program_Leader_1__c);
  Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
           cshare.ParentId = EKPI.id;           
           cshare.UserOrGroupId = GKPI[0].Program_Leader_2__c;          
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Program_Leaders__c;
           EntityKPIShare.add(cshare);           
          }
         Database.insert(EntityKPIShare,false); 
         }
       } 
       
  // Add share permission for Initiative Sponsor
      if(GKPI[0].Connect_Global_Program__c != null){       
         for(Global_KPI_Targets__c globalkpi : GKPI){           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           gshare.ParentId = globalkpi.id;
           gshare.UserOrGroupId = globalkpi.Initiative_Sponsor_1__c;          
           gshare.AccessLevel = 'read';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Initiative_Sponsor__c;
           GlobalKPIShare.add(gshare);  
         
          }
         Database.insert(GlobalKPIShare,false);          
                 
        }
      // Add share permission for Initiative Sponsor 2
         
     if(GKPI[0].Connect_Global_Program__c != null){       
         for(Global_KPI_Targets__c globalkpi : GKPI){           
           Global_KPI_Targets__Share gshare = new Global_KPI_Targets__Share();
           gshare.ParentId = globalkpi.id;
           gshare.UserOrGroupId = globalkpi.Initiative_Sponsor_2__c;          
           gshare.AccessLevel = 'read';
           gshare.RowCause = Schema.Global_KPI_Targets__Share.RowCause.Initiative_Sponsor__c;
           GlobalKPIShare.add(gshare);  
         
          }
         Database.insert(GlobalKPIShare,false);          
                 
        }
        
      // Initiative Sponsor  1 Permission for Entity KPIs
   if(GKPI[0].Connect_Global_Program__c != null){
      
     For(Cascading_KPI_Target__c EKPI : [Select Id from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id])
     {
       lstId3.add(EKPI.Id);
  
  Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
           cshare.ParentId = EKPI.id;           
           cshare.UserOrGroupId = GKPI[0].Initiative_Sponsor_1__c;          
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Initiative_Sponsor__c;
           EntityKPIShare.add(cshare);
              
          }
         Database.insert(EntityKPIShare,false); 
         }
     
     
     
      // Initiative Sponsor  2 Permission for Entity KPIs
   if(GKPI[0].Connect_Global_Program__c != null){
      
     For(Cascading_KPI_Target__c EKPI : [Select Id from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id])
     {
       lstId3.add(EKPI.Id);
  
  Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
           cshare.ParentId = EKPI.id;           
           cshare.UserOrGroupId = GKPI[0].Initiative_Sponsor_2__c;          
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Initiative_Sponsor__c;
           EntityKPIShare.add(cshare);
              
          }
         Database.insert(EntityKPIShare,false); 
         } 
        
       
   // Add KPI Owner permission for related Entity KPI        
 if(GKPIold.Global_KPI_Owner__c != GKPI[0].Global_KPI_Owner__c){
  For(Cascading_KPI_Target__c EKPI : [Select Id from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id])
   {
  lstId1.add(EKPI.Id);
  Cascading_KPI_Target__Share cshare = new Cascading_KPI_Target__Share();
           
           cshare.ParentId = EKPI.id;           
           cshare.UserOrGroupId = GKPI[0].Global_KPI_Owner__c;          
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Cascading_KPI_Target__Share.RowCause.Corporate_KPI_Owner__c;
           EntityKPIShare.add(cshare);           
          }
         Database.insert(EntityKPIShare,false); 
         }
       
       // Delete old Data Reporters
       
     if(GKPIold.Global_Data_Reporter__c != GKPI[0].Global_Data_Reporter__c){ 
     For(Global_KPI_Targets__Share  GlobalKPISharedelete :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Global_KPI_Targets__Share where ParentId = :GKPIold.Id and UserOrGroupId = :GKPIold.Global_Data_Reporter__c and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Corporate_Data_Reporter__c ]){
     if(GlobalKPISharedelete.Parentid != null)              
      Database.delete(GlobalKPISharedelete,false);
      }
     }
    // Delete old KPI Owenrs
    if(GKPIold.Global_KPI_Owner__c != GKPI[0].Global_KPI_Owner__c){
     For(Global_KPI_Targets__Share  GlobalKPISharedeleteowner :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Global_KPI_Targets__Share where ParentId = :GKPIold.Id and UserOrGroupId = :GKPIold.Global_KPI_Owner__c and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Corporate_KPI_Owner__c ]){
     if(GlobalKPISharedeleteowner.Parentid != null)              
      Database.delete(GlobalKPISharedeleteowner,false);
      }
     }
   
   if(GKPI[0].Program_Leader_1__c != GKPIold.Program_Leader_1__c){
    for( Global_KPI_Targets__Share GlobalKPISharedeletePL1 :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Global_KPI_Targets__Share where ParentId = :GKPIold.Id and UserOrGroupId = :GKPIold.Program_Leader_1__c and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c]){
     if(GlobalKPISharedeletePL1.Parentid != null)              
      Database.delete(GlobalKPISharedeletePL1,false);
      }
     }
     
    if(GKPI[0].Program_Leader_2__c != GKPIold.Program_Leader_2__c){
    for( Global_KPI_Targets__Share GlobalKPISharedeletePL2 :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Global_KPI_Targets__Share where ParentId = :GKPIold.Id and UserOrGroupId = :GKPIold.Program_Leader_2__c and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c]){
     if(GlobalKPISharedeletePL2.Parentid != null)              
      Database.delete(GlobalKPISharedeletePL2,false);
      }
     }
   // Delete Share permission on Entity KPI Target for removed program leaders
   if(GKPI[0].Program_Leader_1__c != GKPIold.Program_Leader_1__c){
   for( Cascading_KPI_Target__Share EntityKPISharedeletePLdelete :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId in :lstId2 and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c and UserOrGroupId = :GKPIold.Program_Leader_1__c ]){
     if(EntityKPISharedeletePLdelete.Parentid != null)              
      Database.delete(EntityKPISharedeletePLdelete,false);
     }   
     }
     
 // Delete Share permission on Entity KPI Target for removed program leaders
   if(GKPI[0].Program_Leader_2__c != GKPIold.Program_Leader_2__c){
   for( Cascading_KPI_Target__Share EntityKPISharedeletePLdelete :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId in :lstId2 and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c and UserOrGroupId = :GKPIold.Program_Leader_2__c ]){
     if(EntityKPISharedeletePLdelete.Parentid != null)              
      Database.delete(EntityKPISharedeletePLdelete,false);
      }   
     }
     
  // Delete the Sharing for Program Leaders if the Global Program Reference is removed
  
   if(GKPI[0].Connect_Global_Program__c == null){
    for( Global_KPI_Targets__Share GlobalKPISharedeletePLNone :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Global_KPI_Targets__Share where ParentId = :GKPIold.Id and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c]){
     if(GlobalKPISharedeletePLNone.Parentid != null)              
      Database.delete(GlobalKPISharedeletePLNone,false);
      }
      for( Global_KPI_Targets__Share GlobalKPISharedeletePLNone :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Global_KPI_Targets__Share where ParentId = :GKPIold.Id and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Initiative_Sponsor__c]){
     if(GlobalKPISharedeletePLNone.Parentid != null)              
      Database.delete(GlobalKPISharedeletePLNone,false);
      }
     }
       // Delete the Sharing for Program Leaders in Entity KPI if the Global Program Reference is removed
     
     if(GKPI[0].Connect_Global_Program__c == null){      
     For(Cascading_KPI_Target__c EKPI : [Select Id from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id])
      {
       lstId2.add(EKPI.Id);
      }
     for( Cascading_KPI_Target__Share EntityKPISharedeletePLdelete :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId in :lstId2 and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Program_Leaders__c and (UserOrGroupId = :GKPIold.Program_Leader_1__c or UserOrGroupId = :GKPIold.Program_Leader_2__c or UserOrGroupId = :GKPIold.Program_Leader_3__c)]){
     if(EntityKPISharedeletePLdelete.Parentid != null)              
      Database.delete(EntityKPISharedeletePLdelete,false);
      }   
         
    for( Cascading_KPI_Target__Share EntityKPISharedeleteINsdelete :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId in :lstId2 and RowCause = :Schema.Global_KPI_Targets__Share.RowCause.Initiative_Sponsor__c and (UserOrGroupId = :GKPIold.Initiative_Sponsor_1__c or UserOrGroupId = :GKPIold.Initiative_Sponsor_2__c)]){
     if(EntityKPISharedeleteINsdelete.Parentid != null)              
      Database.delete(EntityKPISharedeleteInsdelete,false);
      }   
     }
    }
     
        
 // Replicate Record Type change to Child records
    
 Public Static void ConnectGlobalKPITargetAfterUpdate_RecordTypeChange(List<Global_KPI_Targets__c> GKPI, Global_KPI_Targets__c GKPIold)
 {
   List<Cascading_KPI_Target__c> EntityKPIUpdate = new List<Cascading_KPI_Target__c>();
   if(GKPI[0].KPI_Type__c != GKPIold.KPI_Type__c){
   
     For(Cascading_KPI_Target__c EKPI : [Select Id, RecordType.ID from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id])
       {
         
          if(GKPI[0].KPI_Type__c == Label.ConnectGlobalKPI)
             EKPI.RecordTypeId = Label.Country_KPI_Record_Type;  
          if(GKPI[0].KPI_Type__c == Label.ConnectCountryKPI)
             EKPI.RecordTypeId = Label.GlobalKPIRecordType;  
             
             EntityKPIUpdate.add(EKPI);
           }
             Database.update(EntityKPIUpdate);
        }  
    }
    
   Public Static void ConnectCascadingKPITargetBeforeUpdate(list<Global_KPI_Targets__c> GKPI)
      {
      
      System.Debug('****** ConnectCascadingKPITargetBeforeInsertUpdate Start ****'); 
          
         // copy Description and Comment
        list<Cascading_KPI_Target__c> CM= new list<Cascading_KPI_Target__c>();
        for(Cascading_KPI_Target__c C:[Select id,Comments__c, KPI_Description__c from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id]){
        
          C.Comments__c = GKPI[0].Comments__c;
          C.KPI_Description__c = GKPI[0].KPI_Description__c;  
          CM.add(C);
          }
          Database.update(CM);        
             
       System.Debug('****** ConnectCascadingKPITargetBeforeInsertUpdate End ****');
      }
      
      Public Static void ConnectGlobalKPITargetbeforeUpdate(List<Global_KPI_Targets__c> GKPI, Global_KPI_Targets__c GKPIold)
      {
          List<Id> lstId2 = new List<Id>();
        if((GKPIold.Global_KPI_Owner__c != GKPI[0].Global_KPI_Owner__c) || (GKPIold.Global_Data_Reporter__c != GKPI[0].Global_Data_Reporter__c)){
           For(Cascading_KPI_Target__c EKPI : [Select Id from Cascading_KPI_Target__c where Global_KPI_Target__c = :GKPI[0].id])
         {
          lstId2.add(EKPI.Id);
          }
         }
         
        if((GKPIold.Global_KPI_Owner__c != GKPI[0].Global_KPI_Owner__c)){
          for( Cascading_KPI_Target__Share EntityKPISharedeleteowndelete :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId in :lstId2 and RowCause = :Schema.Cascading_KPI_Target__Share.RowCause.Corporate_KPI_Owner__c ]){
          if(EntityKPISharedeleteowndelete.Parentid != null)              
          Database.delete(EntityKPISharedeleteowndelete,false);
          }  
         }
        if(GKPIold.Global_Data_Reporter__c != GKPI[0].Global_Data_Reporter__c){
         for( Cascading_KPI_Target__Share EntityKPISharedeleterepdeleteCD :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Cascading_KPI_Target__Share where ParentId in :lstId2 and RowCause = :Schema.Cascading_KPI_Target__Share.RowCause.Corporate_Data_Reporter__c ]){
         if(EntityKPISharedeleterepdeleteCD.Parentid != null)              
          Database.delete(EntityKPISharedeleterepdeleteCD,false);
          }
        }
       }
   }