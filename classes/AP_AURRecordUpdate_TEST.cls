/*
  Author: Pooja Suresh
  Release: May 2013
*/
@isTest
private class AP_AURRecordUpdate_TEST {

  static testmethod void testAURupdate(){
      
      Country__c cntry1 = new Country__c();
      cntry1.Name='TestCountry1';
      cntry1.CountryCode__c='IT';
      insert cntry1;
    
      Account testaccount = Utils_TestMethods.createAccount();
      testaccount.Country__c=cntry1.id;
      insert testaccount;
      
      List<AccountUpdateRequest__c> lstaur = new List<AccountUpdateRequest__c>();
    
      AccountUpdateRequest__c aur = new AccountUpdateRequest__c(Account__c = testaccount.id,
                                                             AccountName__c = testaccount.name,
                                                             City__c='Test City',
                                                             Street__c='Test Street',
                                                             Country__c=cntry1.id,
                                                             ZipCode__c='012345',
                                                             CorporateHeadquarters__c=true,
                                                             Inactive__c=true,
                                                             InactiveReason__c='Company Closed'
                                                             );
                                                             
      insert aur;
      
      aur.ApprovalStatus__c='Approved';
      update aur;
  }
 
}