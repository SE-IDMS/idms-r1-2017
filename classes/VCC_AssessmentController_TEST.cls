/**
29-April-2013
Shruti Karn  
PRM May13 Release    
Initial Creation: test class for    
    1. VCC_AssessmentController
        

 */
@isTest
private class VCC_AssessmentController_TEST {
    static testMethod void myUnitTest() {
        
        List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0){
            User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
            usr.BypassWF__c = false;
            usr.ProgramAdministrator__c = true;
            System.runas(usr){
                Country__c country = Utils_TestMethods.createCountry();
                country.countrycode__c ='ASM';
                insert country;
                
                PartnerPRogram__c gp1 = new PartnerPRogram__c();
                Account acc = new Account();
                ACC_PartnerProgram__c accprg = new ACC_PartnerProgram__c();
                
                usr.country__c = country.countrycode__c;
                
                //creates an account and sets the country code
                acc = Utils_TestMethods.createAccount();
                acc.country__c = country.id;
                insert acc;
                
                //makes the account a partner account
                acc.isPartner = true;
                update acc;
                
               
                //creates a global program
                gp1 = Utils_TestMethods.createPartnerProgram();
                gp1.TECH_CountriesId__c = country.id;
                gp1.recordtypeid = Label.CLMAY13PRM15;
                gp1.ProgramStatus__c = Label.CLMAY13PRM47;
                insert gp1;
                
                //creates a program level for the global program
                ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
                insert prg1;
                prg1.levelstatus__c = 'active';
                update prg1;
                
               
                
                //creates a country partner program
                PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
                insert cp1;
                cp1.ProgramStatus__c = 'active';
                update cp1;
                
                //creates a program level for the country partner program
                ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
                insert prg2;
                prg2.levelstatus__c = 'active';
                update prg2;
                Assessment__c newAssessment = Utils_TestMethods.createAssessment();
                newAssessment.Name = 'Test Assessment';
                newAssessment.PartnerProgram__C = cp1.Id;
                newAssessment.ProgramLevel__C = prg2.Id;
                newAssessment.AutomaticAssignment__c = true;
                insert newAssessment;
                OpportunityAssessmentQuestion__c opassq = Utils_TestMethods.createOpportunityAssessmentQuestion(newAssessment.Id, 'picklist', 4);
                insert opassq;
                
                OpportunityAssessmentAnswer__c opassa = Utils_TestMethods.createOpportunityAssessmentAnswer(opassq.id);
                insert opassa;
                PartnerAssessment__c newPArnterAssessment = new PartnerAssessment__c();
                //
                VCC_AssessmentController controller = new VCC_AssessmentController();
                controller.obj = newPArnterAssessment;
                controller.lstQueryFieldValue = new list<String>();
                controller.lstQueryFieldValue.add('PartnerProgram__c:'+cp1.Id);
                controller.lstQueryFieldValue.add('ProgramLevel__c:'+prg2.Id);
                controller.getPopulateAssessment();
                newPArnterAssessment.assessment__c = newAssessment.Id;
                controller.obj = newPArnterAssessment;
                controller.getPopulateAssessment();
                controller.retURL = '/home/home.jsp';
                controller.doSave();
                controller.doCancel();
                controller.getPopulateAssessment();
                controller.doSave();
            }
        }
    }
}