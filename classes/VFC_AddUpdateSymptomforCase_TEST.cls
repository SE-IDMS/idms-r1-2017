@isTest

private class VFC_AddUpdateSymptomforCase_TEST
{
    static testMethod void testVFP_AddUpdateSymptomforCase_TestMethod() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
       Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
       insert contact1;
       
        CaseClassification__c objCaseClassification = Utils_TestMethods.createCaseClassificationWithOutSubReason();
        insert objCaseClassification;
        
        Symptom__c  objSymptom = Utils_TestMethods.createSymptom(objCaseClassification.Id);
        insert objSymptom;
        
        Case objCase = Utils_TestMethods.createCase(account1.Id,contact1.Id,'Open');
        objCase.CommercialReference__c='testCR';
        objCase.Family__c='TestFamily';
        objCase.SupportCategory__c = Label.CL00737;
        objCase.CheckedBySymptom__c = true;
        insert objCase;
        
        objCase.CaseSymptom__c='Security';
        objCase.CaseSubSymptom__c='Improper Authorization';
        objCase.CaseOtherSymptom__c='CaseOtherSymptom';
        objCase.TECH_GMRCode__c='TECH_GMRCode__c';
        objCase.Status = 'Closed';
        update objCase;
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objCase);
        VFC_AddUpdateSymptomforCase SymptomforCase = new VFC_AddUpdateSymptomforCase(CaseStandardController);
        system.debug('test cr--->'+SymptomforCase.caseRecord);
        system.debug('test--->'+SymptomforCase.caseRecord.CaseSymptom__c);
        system.debug('test--->'+SymptomforCase.caseRecord.CaseSubSymptom__c);
        String strGMRCode='test GMR Code String';
        String strCategory='test Category';
        String strCaseSymptom='test Symptom'; //9035184170
        String strCaseSubSymptom='test SubSymptom';
        String strCaseOtherSymptom='test OtherSymptom';
        List<string> lstGetCaseSymptom = new list<string>();
        list<String> lstgetCaseSubSymptom =new list<string>();
        Database.SaveResult lstupdateCaseSymptom =Database.update(objCase);
        lstGetCaseSymptom=VFC_AddUpdateSymptomForCase.getCaseSymptom(strGMRCode,strCategory);
        lstgetCaseSubSymptom=VFC_AddUpdateSymptomForCase.getCaseSubSymptom(strGMRCode,strCategory,strCaseSymptom);
        lstupdateCaseSymptom=VFC_AddUpdateSymptomForCase.updateCaseSymptom(objCase.id,strCaseSymptom,strCaseSubSymptom,strCaseOtherSymptom,strGMRCode,strCategory );
     }
}