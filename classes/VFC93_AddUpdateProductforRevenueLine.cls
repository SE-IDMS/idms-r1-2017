/*
    Author          : Accenture Team
    Date Created    : 16/04/2012
    Description     : Controller extensions for VFP93_AddUpdateProductforRevenueLine.This class fetches the search 
                      parameters from the component controller, performs the search by calling factory methods  
*/
public class VFC93_AddUpdateProductforRevenueLine extends VFC_ControllerBase 
{
    public GMRSearch_ITB_RevenueLine ITB_GMRSearch{get;set;}
    public boolean displayPage {get;set;}
    //Iniatialize variables
    private Revenue_Line__c currentRevenueLine;
    private Opportunity opp = new Opportunity();  
    private Map<Revenue_Line__c, DataTemplate__c> updateRL = new Map<Revenue_Line__c, DataTemplate__c>();  
    private Map<Opportunity, DataTemplate__c> insertRL = new Map<Opportunity, DataTemplate__c>();          
    private List<Revenue_Line__c> revLines; 

     /*=======================================
      INNER CLASSES
    =======================================*/ 
        
    // GMR implementation for ITB revenue Lines
    public class GMRSearch_ITB_RevenueLine implements Utils_GMRSearchMethods
    {
        public Void Init(String ObjID, VCC08_GMRSearch Controller)
        {
            VCC08_GMRSearch GMRController = (VCC08_GMRSearch)Controller;
            VFC93_AddUpdateProductforRevenueLine thisController = (VFC93_AddUpdateProductforRevenueLine)GMRController.pageController;
            system.debug('Current Revenue Line: ' + ObjID);
            List<Revenue_Line__c> RLList;
            
            if (ObjID!= null){
                RLList = [SELECT ID, TECH_CommercialReference__c, Opportunity__c,ProductBU__c, ProductLine__c, ProductFamily__c, Family__c FROM Revenue_Line__c WHERE ID=: ObjID LIMIT 1];
                system.debug('Current Revenue Line: ' + RLList);
                
                if(RLList[0] != null){
                    Controller.filters.add(RLList[0].ProductBU__c);
                    Controller.filters.add(RLList[0].ProductLine__c);
                    Controller.filters.add(RLList[0].ProductFamily__c);
                    Controller.filters.add(RLList[0].Family__c);
                    
                    if(RLList[0].TECH_CommercialReference__c!=null)
                        Controller.searchKeyword = RLList[0].TECH_CommercialReference__c;
                    if(Controller.searchKeyword != null)  
                        Controller.keywords = Controller.searchKeyword.split(' '); 
                        
                    system.debug('filters: '+Controller.filters);
                    system.debug('keywords: '+Controller.keywords);
                }
            }
        }
        
        // Action performed when clicking on "Select" on the PM0 search results
        public Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
        {
            System.Debug('VFC93.PerformAction.METHOD_ENTRY');
        
            VCC08_GMRSearch GMRController = (VCC08_GMRSearch)controllerBase;
            VFC93_AddUpdateProductforRevenueLine thisController = (VFC93_AddUpdateProductforRevenueLine)GMRController.pageController;       
           
            
            pagereference pg;
            Id recordId;
            Boolean flag;
            
            if(thisController.currentRevenueLine.Id == null)
            {
                System.Debug('VFC93.PerformAction.INFO - Related Revenue Line  null ');
                
                //String parentOppID = system.currentpagereference().getParameters().get('retURL').replace('/','');
                
                //Modif JFA - Save&New
                String parentOppID = '';
                String parentObjID = system.currentpagereference().getParameters().get('retURL'); //fetch the related object id from the URL
                system.debug('#### parentObjID: ' + parentObjID );
                if (parentObjID != null)
                    parentObjID = system.currentpagereference().getParameters().get('retURL').replace('/',''); // delete the "/"
                List<Revenue_Line__c> RLList = [SELECT ID, Opportunity__c FROM Revenue_Line__c WHERE ID=: parentObjID];
                if(RLList.size()==1){ // if related object is an order, fetch its related opportunity
                    parentOppID = RLList[0].Opportunity__c;
                }
                else{
                    List<Opportunity> OppList = [SELECT ID, OwnerID FROM Opportunity WHERE ID=:parentObjID LIMIT 1];
                    if(OppList.size()==1){    // if related object is an Opportunity assign it directly
                        parentOppID = OppList[0].ID;
                    } 
                } 
                //end of Modif
            
                System.Debug('#### Opp ID '+ parentOppID );
                if(parentOppID != null)
                {
                    thisController.opp = [select Id, CurrencyISOCode from Opportunity where Id=:parentOppID];
                    System.Debug('VFC93.PerformAction.INFO - Parent Opportunity = '+ thisController.opp );
                    thisController.insertRL.put(thisController.opp,(dataTemplate__c)obj);
                    System.Debug('VFC93.PerformAction.INFO - Insert Product Lines Map = '+ thisController.insertRL);
                }
    
                List<String> revLineIds ;
                //Inserts Product Line
                if(!thisController.insertRL.isempty())
                {
                
                    System.Debug('VFC93.PerformAction.INFO - Insert Product Lines is not empty.');
                    
                    try
                    {
                        revLineIds = AP50_RevenueLineIntegrationMethods.InsertRevenueLine(thisController.insertRL);        
                    }
                    catch(DMLException e)
                    {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00414));
                        pg = null;
                    }
                    
                    system.debug('------prodLineIds-----'+revLineIds );
                    if(revLineIds!=null && revLineIds.size()==1)
                    recordId = revLineIds[0];
                    if(recordId!=null)
                    pg = new pagereference('/'+recordId+'/e?retURL=%2F'+recordId);
                }
            }
            else
            {
                system.debug('VFC93.PerformAction.INFO - Related Revenue Line isnt null.');
                integer size = thisController.revLines.size();
                system.debug('#### revLines size: ' + size);
                if(thisController.revLines.size() == 1) {
                    system.debug('VFC93.PerformAction.INFO - Related Revenue List is equal to 1.');
                    thisController.updateRL.put(thisController.revLines[0], (dataTemplate__c)obj);
                }
                
                //Updates Product Line
                if(thisController.updateRL.size()>0)
                {
                    system.debug('-----In Update -----');
                    try
                    {
                        AP50_RevenueLineIntegrationMethods.UpdateRL(thisController.updateRL);   
                    }
                    catch(DMLException e)
                    {
                        flag = true;
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00414));
                        pg = null;
                    }
                }
                if(thisController.revLines.size() == 1)
                    recordId = thisController.revLines[0].Id;
                if(recordId!=null && flag != true)
                    pg = new pagereference('/'+recordId+'/e?retURL=%2F'+recordId) ;                    
            }
            
            System.Debug('VFC93.PerformAction.METHOD_EXIT');

            return pg;
        
        }
        
        // Cancel method returns the associated Case detail page
        public PageReference Cancel( VCC08_GMRSearch Controller)
        { 
            System.Debug('****** Cancel Method Begins  for  VFC93_AddUpdateProductforRevenueLine******');
            String parentObjID = system.currentpagereference().getParameters().get('retURL'); //fetch the related object id from the URL
            if(parentObjID == null){
                parentObjID = system.currentpagereference().getParameters().get('ID');    //when modif from the revenue line
            }
            system.debug('#### parentObjID: ' + parentObjID );
            if (parentObjID != null){
                parentObjID = parentObjID.replace('/',''); // delete the "/"
            }
            pagereference pg;
            Id recordId;
                            
            if(parentObjID !=null)
            {
                pg = new pagereference('/'+parentObjID);
                return pg;
            }
            else
            {  
                return null;
            }          
        }    
            
            
            
  
    }
    
    /*=======================================
      CONSTRUCTOR
    =======================================*/          

    public VFC93_AddUpdateProductforRevenueLine(ApexPages.StandardController controller)
    {
        this.DisplayPage = false;
        this.revLines = new List<Revenue_Line__c>();
        this.currentRevenueLine = new Revenue_Line__c();
        this.currentRevenueLine = (Revenue_Line__c)controller.getRecord();
        
        if(this.currentRevenueLine.Id!=null)
        {  
            this.revLines = [Select Id, TECH_CommercialReference__c, Opportunity__c,ProductBU__c, ProductLine__c, ProductFamily__c, Family__c from Revenue_Line__c where Id=:this.currentRevenueLine.Id LIMIT 1];
            system.debug('#### revLines : ' + revLines );
            integer size = this.revLines.size();
            system.debug('#### revLines size: ' + size);
        }        
        
                
        ITB_GMRSearch = new GMRSearch_ITB_RevenueLine();
    }
    
    public void CheckAccess(){
    
        String profileName=[select Name from Profile where Id=:UserInfo.getProfileId()].Name;//the profile name of the current User is retreived
        List<String> profilePrefixList=(System.Label.CL00632).split(',');//the valid profile prefix list is populated
        for(String profilePrefix : profilePrefixList)//The profile name should start with the prefix else the displayerrormessage flag is set to true
        {
            if((profileName+'').startsWith(profilePrefix)){
                this.displayPage = true;
                break;              
            }  
        }
        
        if(!this.displayPage){
            String parentOppID =  '';
            String parentObjID = system.currentpagereference().getParameters().get('retURL'); //fetch the related object id from the URL
            if(parentObjID == null){
                parentObjID = system.currentpagereference().getParameters().get('ID');    //when modif from the revenue line
            }
            system.debug('#### parentObjID: ' + parentObjID );
            if (parentObjID != null){
                parentObjID = parentObjID.replace('/',''); // delete the "/"
            }
            List<Revenue_Line__c> RLList = [SELECT ID, Opportunity__c FROM Revenue_Line__c WHERE ID=: parentObjID];
            if(RLList.size()==1){ // if related object is an order, fetch its related opportunity
                parentOppID = RLList[0].Opportunity__c;
            }
            else{
                List<Opportunity> OppList = [SELECT ID, OwnerID FROM Opportunity WHERE ID=:parentObjID LIMIT 1];
                if(OppList.size()==1){    // if related object is an Opportunity assign it directly
                    parentOppID = OppList[0].ID;
                } 
            } 
            
            system.debug('related opp'+parentOppID );
            for(UserRecordAccess record: [Select RecordId,HasEditAccess from UserRecordAccess where RecordId=:parentOppID and UserId=:UserInfo.getUserId()])            
                if(record.HasEditAccess)
                    this.displayPage = true;
            /*
            List<OpportunityShare> oppShareList = [Select Id, OpportunityId, UserOrGroupId, OpportunityAccessLevel FROM OpportunityShare WHERE OpportunityId =: parentOppID ];
            system.debug('toto2'+oppShareList );
            for(OpportunityShare oppShare : oppShareList){
                system.debug('#### oppShare.OpportunityAccessLevel: ' + oppShare.OpportunityAccessLevel);
                if(userinfo.getuserId() == oppShare.UserOrGroupId && (oppShare.OpportunityAccessLevel == 'edit' || oppShare.OpportunityAccessLevel == 'all')){
                    this.displayPage = true;
                }
            }*/
        }
    }         
}