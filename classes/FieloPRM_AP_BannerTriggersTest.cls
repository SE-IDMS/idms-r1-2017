/********************************************************************
* Company: Fielo
* Created Date: 29/11/2016
* Description: Test class for class FieloPRM_AP_BannerTriggers
********************************************************************/

@isTest
public with sharing class FieloPRM_AP_BannerTriggersTest{
  
    public static testMethod void testUnit1(){
    	FieloEE.MockUpFactory.setCustomProperties(false);

    	FieloEE__PublicSettings__c ps = new FieloEE__PublicSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), CacheEnabled__c = true);
    	insert ps;

    	FieloEE__Menu__c menu1 = new FieloEE__Menu__c(
            Name = 'test menu 1', 
            FieloEE__Home__c = true, 
            FieloEE__Visibility__c ='Both', 
            FieloEE__Placement__c = 'Main',
            FieloEE__ExternalName__c = 'menu1' + datetime.now()
        );
        insert menu1;

        FieloEE__Section__c section1 = new FieloEE__Section__c(
            FieloEE__Menu__c = menu1.Id, 
            FieloEE__Type__c = '12'
        );
        insert section1;

        FieloEE__Section__c section2 = new FieloEE__Section__c(
            FieloEE__Menu__c = menu1.Id, 
            FieloEE__Type__c = '12', 
            FieloEE__Parent__c = section1.Id
        );
        insert section2;
        
        FieloEE__Component__c compBanner1 = new FieloEE__Component__c(
            FieloEE__Section__c = section2.Id, 
            FieloEE__Menu__c = menu1.Id
        );
        compBanner1.FieloEE__Mobile__c = true;
        compBanner1.FieloEE__Tablet__c = false;
        compBanner1.FieloEE__Desktop__c = false;
        insert compBanner1;

        FieloEE__Banner__c ban1 = new FieloEE__Banner__c(
            Name = 'testBanner1', 
            FieloEE__Placement__c = 'Home', 
            FieloEE__Component__c = compBanner1.Id
        );
        insert ban1;

        delete ban1;

    }
}