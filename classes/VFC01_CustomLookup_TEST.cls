/*
    Author          : Grégory GACKIERE (SFDC) 
    Date Created    : 
    Description     : 
*/
@isTest
private class VFC01_CustomLookup_TEST {

    static testMethod void testSearch() { 
        System.Debug('#########################');
        System.Debug('### testSearch  ');
        System.Debug('#########################');
        Group testGroup = Utils_TestMethods.createStandardGroup('testcoverage'); 
        insert testGroup;
        Group testGroup1 = Utils_TestMethods.createStandardGroup('testcoverage1'); 
        insert testGroup1;
        Group testGroup2 = Utils_TestMethods.createStandardGroup('testcoverage2'); 
        insert testGroup2;
        
        VFC01_CustomLookup c = new VFC01_CustomLookup();
        c.searchValue = 'testcov';
        c.objectType = 'Group';
        Test.startTest();
        c.search();
        c.hideResults();
        Test.stopTest();
    }
    
    static testMethod void testSearch1() { 
        System.Debug('#########################');
        System.Debug('### testSearch  ');
        System.Debug('#########################');
        Group testGroup = Utils_TestMethods.createStandardGroup('testcoverage'); 
        insert testGroup;
        
        VFC01_CustomLookup c = new VFC01_CustomLookup();
        c.searchValue = 'testcov';
        c.objectType = 'Group';
        Test.startTest();
        c.search();
        c.selectValue();
        Test.stopTest();
    }
}