/**
* @author Anil Sistla
* @date 05/11/2016
* @description Iterator class encapuslating the functionality for registration pool
*               used as custom iterator in batch. Implements Iterator interface
*/
public class RegistrationPoolIterator
        implements Iterator<AP_PRMRegistrationPoolingService.RegistrationPoolInfo> {

    private AP_PRMAppConstants.ProcessRegistrationPool typeToProcess;
    
    List<AP_PRMRegistrationPoolingService.RegistrationPoolInfo> lPools { get; set; }
    Integer i { get; set; }


    /**
    * @author Anil Sistla
    * @date 05/11/2016
    * @description Constructor, invokes RegistrationPoolingService.calculateActivePools
    *               to prepare the list of pool items to be processed
    * @return None
    */
    public RegistrationPoolIterator () {
        Map<String, AP_PRMRegistrationPoolingService.RegistrationPoolInfo> mPools =
                                        AP_PRMRegistrationPoolingService.calculateActivePools();
        lPools = mPools.values();
        if (lPools != null && lPools.size() > 0) i = 0;
        System.debug('*** Pools:' + lPools);
    }

    public RegistrationPoolIterator(AP_PRMAppConstants.ProcessRegistrationPool typeToProcess){
        this.typeToProcess = typeToProcess;
        if (this.typeToProcess != null && this.typeToProcess == AP_PRMAppConstants.ProcessRegistrationPool.ASSIGN_POOLPERMSET) {
            lPools = AP_PRMRegistrationPoolingService.calculatePSAssignmentPools();
        }
        else {
            Map<String, AP_PRMRegistrationPoolingService.RegistrationPoolInfo> mPools =
                                            AP_PRMRegistrationPoolingService.calculateActivePools();

            if (mPools != null && !mPools.isEmpty()) lPools = mPools.values();
        }

        if (lPools != null && lPools.size() > 0) i = 0;
        System.debug('*** Pools for type: ' + typeToProcess + ','  + lPools);
    }

    /**
    * @author Anil Sistla
    * @date 05/11/2016
    * @description Iterator interface method to move to next item in the list
    * @return None
    */
    public boolean hasNext() {
        System.debug('*** Iterator HasNext');
        System.debug('*** Next Count:' + i);
        if ((i != null && i >= lPools.size()) || i == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
    * @author Anil Sistla
    * @date 05/11/2016
    * @description Iterator interface method to move to return the instance of next item
    * @return None
    */
    public AP_PRMRegistrationPoolingService.RegistrationPoolInfo next() {
        System.debug('*** Iterator Next');
        System.debug('*** Next Count:' + i);
        if ((i != null && i >= lPools.size()) || i == null) { return null; }
        i++;
        return lPools[i - 1];
    }
}