/*
09-Apr-2013     Srinivas Nallapati  May13 Mkt - UAT change
*/
public class VFC_CloseLeadsNoActionTaken {

    public VFC_CloseLeadsNoActionTaken(ApexPages.StandardController controller) {

    }
    public User LoggedInUser {get;set;} {LoggedInUser = [select name, isActive, LeadCleanup__c, ProfileId, UserRoleId, UserRole.name, Profile.name,UserPermissionsMarketingUser from User where id =:UserInfo.getUserId()];}
    public Boolean hasError {get;set;} {hasError = false;}
    public Boolean hasUpdateError {get;set;} {hasUpdateError=false;}
    
    public List<LeadWrapper> lstResultLeadWraps {get;set;} {lstResultLeadWraps = new List<LeadWrapper>();}
    public List<Lead> lstLeads {get;set;} {lstLeads = new List<Lead>();}
    
    public VFC_CloseLeadsNoActionTaken(ApexPages.StandardSetController controller) {
            if(!Test.isRunningTest())
            controller.addFields(new List<String> {
                        'Name', 'Status', 'Account__c', 'Contact__c', 'SubStatus__C','TECH_CampaignName__c', 'Timeframe__c',
                        'SolutionInterest__c', 'PromoTitle__c', 'Priority__c','CreatedDate', 'ClosedDate__c', 'Category__c',
                        'OwnerId','ResponseDate__c','CampaignName__c','keycode__c', 'ConvertedDate', 'MarcomType__c', 'Media__c', 'Tech_date__c' 
                     });
           lstLeads = controller.getSelected();
           
		Boolean isAdminUser = false;
        String profileName=[select Name from Profile where Id=:UserInfo.getProfileId()].Name;//the profile name of the current User is retreived
        //TODO - logic to check User access to this page
        if(LoggedInUser.LeadCleanup__c == false && isAdminUser == false)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.CLMAY13MKT009 ));
            hasError = true;
            //return null;
        }
        
        List<String> profilesToSKIP = (System.Label.CLMAY13MKT010).split(',');//the valid profile prefix list is populated
        for(String pName : profilesToSKIP)//The profile name should start with the prefix else the displayerrormessage flag is set to true
        {
            if(pName.trim() == profileName.trim())
            {
                isAdminUser = true;              
            }  
        }



        //if no Lead Selected
        if(hasError == false && lstLeads.size() == 0){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.CLDEC12MKT06));
            hasError = true;
            //return null;
        } 
           
    }
    
    //Close selected as No Action Taken
    public PageReference closeSelected()
    {

        map<id,Lead> mapUnchangedLeads = new map<id,Lead>();
               

        for(Lead ls : lstLeads )
        {
            mapUnchangedLeads.put(ls.id, ls.clone(true));
            ls.Status = System.Label.CL00510;
            ls.SubStatus__C = System.Label.CLMAY13MKT008;
        }
        
        Database.Saveresult[] lstResult =  Database.update(lstLeads, false);
        for(Integer i = 0; i<lstLeads.size() ; i++)
        {
            Database.Saveresult res = lstResult[i];
            if(!res.isSuccess())
            {
                LeadWrapper tmpLW =  new LeadWrapper(mapUnchangedLeads.get(lstLeads[i].id));
                for(Database.Error er : res.getErrors())
                {
                  tmpLW.errorMessage = er.getMessage();
                }
                lstResultLeadWraps.add(tmpLW);
                hasUpdateError = true;
            }
        }
        
        if(hasUpdateError)
        {
            PageReference pg = page.VFP_CloseLeadsNoActionTaken_Result;
            String retUrl = ApexPages.currentPage().getParameters().get('retURL');
            pg.getParameters().put('retURL', retUrl);
            return pg;
        }
        else
        {
            String retUrl = ApexPages.currentPage().getParameters().get('retURL');
            PageReference pg = new PageReference(retUrl);
            return pg;
        }    

        
    }//End of Close method

    //Wrapper class for showing failures
    public class LeadWrapper{
        public boolean isSelected {get;set;} {isSelected = false;}
        public Lead Ld {get;set;}
        public String errorMessage {get;set;}
        
        public LeadWrapper(Lead ld){
            this.Ld = ld;
        }
    }
}