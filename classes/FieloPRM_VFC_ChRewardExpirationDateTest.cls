/*
*@author Pablo Cassinerio
*@date 30/09/2015
*
*/
@isTest 
private class FieloPRM_VFC_ChRewardExpirationDateTest{  
    
    static testMethod void unitTest1(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Program Level';
            insert badgeAux;
            
            
            FieloCH__Challenge__c chal = FieloPRM_UTILS_MockUpFactory.createChallenge('Test Challenge', 'Competition Type', 'Edit_Competition_Type');

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
        
            FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(chal.id , mission1.id );    
            FieloCH__MissionChallenge__c missionChallenge2  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(chal.id , mission2.id );    
            
            FieloCH__ChallengeReward__c challengeReward  = FieloPRM_UTILS_MockUpFactory.createChallengeReward(chal);    
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badgeAux.id;
            
            update challengeReward;
            
            
            chal = FieloPRM_UTILS_MockUpFactory.activateChallenge(chal);
            
            Account acc = new Account();
            acc.Name = 'acc';
            insert acc;
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'Polo'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'Marco'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.Id;
            insert member;
                    
            ApexPages.StandardController sc = new ApexPages.StandardController(chal);
            FieloPRM_VFC_ChRewardExpirationDate testClass = new FieloPRM_VFC_ChRewardExpirationDate(sc);
            
            testClass.save();
        }
    }

   static testMethod void unitTest2(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Program Level';
            insert badgeAux;
                      
            FieloCH__Challenge__c chal = FieloPRM_UTILS_MockUpFactory.createChallenge('Test Challenge', 'Competition Type', 'Edit_Competition_Type');

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
        
            FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(chal.id , mission1.id );    
            FieloCH__MissionChallenge__c missionChallenge2  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(chal.id , mission2.id );    
            
            FieloCH__ChallengeReward__c challengeReward  = FieloPRM_UTILS_MockUpFactory.createChallengeReward(chal);    
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badgeAux.id;
            
            update challengeReward;
                       
            chal = FieloPRM_UTILS_MockUpFactory.activateChallenge(chal);
            
            Account acc = new Account();
            acc.Name = 'acc';
            insert acc;
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'Polo'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'Marco'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.Id;
            insert member;
            
            chal.FieloCH__IsActive__c = false;
            
            update chal;
                    
            ApexPages.StandardController sc = new ApexPages.StandardController(chal);
            FieloPRM_VFC_ChRewardExpirationDate testClass = new FieloPRM_VFC_ChRewardExpirationDate(sc);
            
            testClass.save();
        }
    }


    static testMethod void unitTest3(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Program Level';
            insert badgeAux;
                      
            FieloCH__Challenge__c chal = FieloPRM_UTILS_MockUpFactory.createChallenge('Test Challenge', 'Competition Type', 'Edit_Competition_Type');

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria1 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'name', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
        
            FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(chal.id , mission1.id );    
            FieloCH__MissionChallenge__c missionChallenge2  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(chal.id , mission2.id );    
                                  
            Account acc = new Account();
            acc.Name = 'acc';
            insert acc;
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'Polo'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'Marco'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.Id;
            insert member;
                               
            ApexPages.StandardController sc = new ApexPages.StandardController(chal);
            FieloPRM_VFC_ChRewardExpirationDate testClass = new FieloPRM_VFC_ChRewardExpirationDate(sc);
            
            testClass.save();
        }
    }

    static testMethod void unitTest4(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Badge__c badgeAux = new FieloEE__Badge__c();
            badgeAux.name =  'testnane';                                      
            badgeAux.F_PRM_BadgeAPIName__c = 'test';
            badgeAux.F_PRM_Type__c = 'Program Level';
            insert badgeAux;
                      
            FieloCH__Challenge__c chal = FieloPRM_UTILS_MockUpFactory.createChallenge('Test Challenge', 'Competition Type', 'Edit_Competition_Type'); 
                                  
            Account acc = new Account();
            acc.Name = 'acc';
            insert acc;
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'Polo'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__FirstName__c = 'Marco'+ String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.Id;
            insert member;
                               
            ApexPages.StandardController sc = new ApexPages.StandardController(chal);
            FieloPRM_VFC_ChRewardExpirationDate testClass = new FieloPRM_VFC_ChRewardExpirationDate(sc);
            
            testClass.save();
        }
    }

   
}