global class CountryChannelInfo {
    global String CountryId;
    
    global CountryChannelInfo () {
        this ('');
    }
    
    global CountryChannelInfo (String cntryId) {
        CountryId = cntryId;
    }
}