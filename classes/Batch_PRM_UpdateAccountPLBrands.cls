global class Batch_PRM_UpdateAccountPLBrands implements Database.Batchable<sObject>,Schedulable{
    public String query;
    global Database.QueryLocator start(Database.BatchableContext BC) {
       
        query='select FieloEE__Badge2__r.F_PRM_ProgramLevel__c,Id from FieloEE__BadgeMember__c where FieloEE__Badge2__r.F_PRM_ProgramLevel__c!=null';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<FieloEE__BadgeMember__c> scope) {
        List<Id> scopeIds=new List<Id>();
        System.debug('Scoper : '+scope);
        for(FieloEE__BadgeMember__c s :scope){
            scopeIds.add(s.Id);            
        }
        System.debug('ScopeIds :'+scopeIds);
        System.debug('ScopeIds :'+scopeIds.size());
        AP_PRM_UpdateAccountPLBrands.UpdateAccount(scopeIds);
        
    } 
    
    global void finish(Database.BatchableContext BC) {
        
        
    }
     //Schedule method
    global void execute(SchedulableContext sc) {      
        
        Batch_PRM_UpdateAccountPLBrands  batchUpdatePrgLvlAcc = new Batch_PRM_UpdateAccountPLBrands();
        Database.executeBatch(batchUpdatePrgLvlAcc); 
        
        //String jobID = system.schedule('Batch Account Update', '0 35 17 * * ?', new Batch_PRM_UpdateAccountPLBrands());        
           
    }

    
}