public with sharing class  Utils_TestAccessRights
{
    public static Boolean TestRightsOnOpportunity(ID OppID)
    {
        Opportunity OppForRights = new Opportunity();
        List<Opportunity> OppList = [SELECT ID, OwnerID FROM Opportunity WHERE ID=: OppID  LIMIT 1];
        if(OppList.size()==1) // if related object is an Opportunity assign it directly
        {    
            OppForRights = OppList[0];
        }
        
        Id CurrentUserID = (Id) UserInfo.getUserId();
        Id CurrentProfileID = (Id) UserInfo.getProfileId();
        List <Profile> SysAdmList = [SELECT id FROM profile WHERE name=:Label.CL00498 LIMIT 1];
        
        if(CurrentUserID == OppForRights.OwnerID){
            return true;
        }
        if (CurrentProfileID == SysAdmList[0].Id){  // user is system administrator               
            return true;
        }
        else{
            List<OpportunityTeamMember> OppTeam= [SELECT UserId, OpportunityAccessLevel FROM OpportunityTeamMember WHERE UserId =: CurrentUserID and OpportunityID =: OppForRights.ID LIMIT 1];
            if(OppTeam.size()>0){  // current user is in the team
                if(OppTeam[0].OpportunityAccessLevel == 'Edit'){  // current user has edit rights                       
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        //better method but not working...
        /*Savepoint sp = Database.setSavepoint();
        try
        {
            DataBase.SaveResult SR = DataBase.Update(OppForRights);
            Database.rollback(sp);
            return true;
        }
        catch(exception e){ //not enough rights
            Database.rollback(sp);
            return false;
        }
        
        return true;*/    
    }
    
    public static Boolean TestRightsOnAccount(Account Acc)
    {
        Account AccForRights = new Account();
        List<Account> AccList = [SELECT ID, OwnerID FROM Account WHERE ID=: Acc.ID LIMIT 1];
        if(AccList.size()==1) // if related object is an Opportunity assign it directly
        {    
            Acc = AccList[0];
        }
        
        Id CurrentUserID = (Id) UserInfo.getUserId();
        Id CurrentProfileID = (Id) UserInfo.getProfileId();
        List <Profile> SysAdmList = [SELECT id FROM profile WHERE name=:Label.CL00498 LIMIT 1];
        
        if(CurrentUserID == Acc.OwnerID){
            return true;
        }
        if (CurrentProfileID == SysAdmList[0].Id){  // user is system administrator               
            return true;
        }
        else{
            List<AccountTeamMember> AccountTeam= [SELECT UserId, AccountAccessLevel FROM AccountTeamMember WHERE UserId =: CurrentUserID and AccountID =: Acc.ID LIMIT 1];
            if(AccountTeam.size()>0){  // current user is in the team
                if(AccountTeam[0].AccountAccessLevel == 'Edit'){  // current user has edit rights                       
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
    }
}