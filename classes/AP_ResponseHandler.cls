/*
    Created By: Akila Subramanyam
    Description: Creates Account Assigned Program on submission of Assessment.
    Release    : October 2014
   --------------------------------------------- 
*/
public class AP_ResponseHandler{
    public static void checkResponseAssessment(List<OpportunityAssessmentResponse__c> lstOppRes){
        Map<Id,Id> accasmt = new Map<Id,Id>();
        Set<Id> asmtId = new Set<Id>();
        Set<Id> accId = new Set<Id>();
        Set<Id> pasmtId = new Set<Id>();
        Map<Account,List<ProgramLevel__c>> accprgm = new Map<Account,List<ProgramLevel__c>>();
        List<AccountAssessment__c> accountasmts = new List<AccountAssessment__c>();
        
        for(OpportunityAssessmentResponse__c oppRes : lstOppRes) {
        
            if(!pasmtId.contains(oppRes.PartnerAssessment__c))
                pasmtId.add(oppRes.PartnerAssessment__c);
               
        }
        
       
        RecordType applRecType = [Select id from RecordType where DeveloperName = 'PRMProgramApplication' Limit 1];
        //User can submit only Program application
        List<PartnerAssessment__c> pasmts = [SELECT Id, Name, Assessment__c, Account__c,SEAccountID__c, Account__r.Country__c, 
                                              Assessment__r.RecordTypeId, Assessment__r.PartnerProgram__c, Assessment__r.ProgramLevel__c,  AccountAssignedProgram__c 
                                              FROM PartnerAssessment__c WHERE Id = :pasmtId 
                                              and Assessment__r.RecordTypeId = :applRecType.id
                                              and Assessment__r.AutomaticAssignment__c = true 
                                              and Assessment__r.Active__c = true];     
        
        
        
        Set<Id> prgmId = new Set<Id>();
        Set<Id> prgmlvlId = new Set<Id>();
        Set<Id> prgmids = new Set<Id>();
        Set<String> setSEAccountID = new Set<string>();//BR-5255
        
        System.Debug('PartnerAssessment:::::::'+pasmts);
        
        for(PartnerAssessment__c pasment : pasmts) {
          //  accasmt.put(pasment.Account__c,pasment.Assessment__c);
            prgmId.add(pasment.Assessment__r.PartnerProgram__c);
            System.debug('prgmId--------'+prgmId);
            System.Debug('PartnerAssessment:::::::'+pasment.Account__c);
            if(pasment.Assessment__r.ProgramLevel__c == null)
            {
                System.debug('pasment.Assessment__r.PartnerProgram__c--------'+pasment.Assessment__r.PartnerProgram__c);
                prgmids.add(pasment.Assessment__r.PartnerProgram__c);
            }
            else
                prgmlvlId.add(pasment.Assessment__r.ProgramLevel__c);
                
            //Added for Account ID
            //Adding SE AccountID if AccountID is null
            //BR-5255
            if(pasment.account__c != null)
                 accId.add(pasment.account__c);
            else if (pasment.SEAccountID__c != null && pasment.SEAccountID__c.trim() != '')
                setSEAccountID.add(pasment.SEAccountID__c);
            
        }
        System.debug('prgmids--------'+prgmids);
        System.debug('prgmlvlId--------'+prgmlvlId);
        System.debug('accId--------'+accId);
        if(prgmId.size() > 0 && (accId.size() > 0 || setSEAccountID.size() > 0))
        {
          
           
        //  List<Assessment__c> asments = [SELECT Id, Name, RecordTypeId, PartnerProgram__c, ProgramLevel__c FROM Assessment__c WHERE Id = :asmtId AND RecordtypeId = :Label.CLOCT14PRM14];
            Map<Id,Id> maps = new Map<Id,Id>();
            List<ProgramLevel__c> gbllvl = [SELECT Id, Name, PartnerProgram__c FROM ProgramLevel__c WHERE PartnerProgram__c = :prgmids and Hierarchy__c = '1'];
       
       System.debug('gbllvl.....'+gbllvl);
            for(ProgramLevel__c lvls : gbllvl)
                prgmlvlId.add(lvls.Id);     
       
       
            List<Account> acc = [SELECT Id, Name, Country__c, Country__r.Name, SEAccountID__c, PRMAccount__c, isPartner FROM Account WHERE Id = :accId OR seaccountid__c in :setSEAccountID]; //BR-5255
       
       
            //Country program for checking the country ID with Account Country ID
            List<PartnerProgram__c> cntryprgm = [SELECT Id, Name, Country__c, ProgramLevel__c, GlobalPartnerProgram__c 
                                            FROM PartnerProgram__c 
                                            WHERE GlobalPartnerProgram__c = :prgmId OR Id = :prgmId];
                                            
        
            Map<Id,Id> cntprgmmap = new Map<Id,Id>();
            for(PartnerProgram__c cprgm : cntryprgm){
                cntprgmmap.put(cprgm.Id,cprgm.Country__c);
            }                                   
                                            
            //------------------------------------------------------------------------------------------------------
            List<ProgramLevel__c> cntprgmlvl = [SELECT Id, Name, PartnerProgram__c, PartnerProgram__r.Name, PartnerProgram__r.Country__c, 
                                             PartnerProgram__r.Country__r.Name, GlobalProgramLevel__c 
                                           FROM ProgramLevel__c WHERE GlobalProgramLevel__c = :prgmlvlId OR Id =:prgmlvlId];    
    
       
            Map<String,ProgramLevel__c> newmap = new Map<String,ProgramLevel__c>();
            Map<String,ACC_PartnerProgram__c> newaccmap = new Map<String,ACC_PartnerProgram__c>();
            List<Account> updateacc = new List<Account>();
            List<ACC_PartnerProgram__c> newaccprogram = new List<ACC_PartnerProgram__c>();
      
       
            //  System.Debug('Assessment__c:'+asments);
            System.Debug('Account:'+acc);
            //   System.Debug('PartnerProgram__c:'+cntryprgm);
            System.Debug('ProgramLevel__c:'+cntprgmlvl);
        
       
        
            System.Debug('cntprgmmap:'+cntprgmmap);
            map<String,id> mapSEAccount = new map<String,id>();
        
            for(Account account : acc) {
                if(!account.PRMAccount__c) {
                    account.PRMAccount__c = True;
                    updateacc.add(account);
                }
               
                mapSEAccount.put(Account.seaccountid__c, Account.id);           
                for(ProgramLevel__c prgmlvs : cntprgmlvl) {
                    System.Debug('>>>>>>Inside Partner Program for' +prgmlvs.PartnerProgram__c);
                    System.Debug('>>>>>>Inside Partner Program for' + account.Country__r.Name);
                    System.Debug('>>>>>>Inside Partner Program for' + cntprgmmap.get(prgmlvs.PartnerProgram__c));

                    if(account.Country__c != null && cntprgmmap.containskey(prgmlvs.PartnerProgram__c) && account.Country__c == cntprgmmap.get(prgmlvs.PartnerProgram__c)){
               
                        accprgm.put(account, new List<ProgramLevel__c>{(prgmlvs)});
                        //  newmap.put(account + ' - ' + prgmlvs, prgmlvs);
                    }
                }
            }
            update updateacc;
        
            System.Debug('Account updated:'+acc);
            System.Debug('accprgm:'+accprgm);
        
            List<ACC_PartnerProgram__c> accprgms = [SELECT Id, Name, PartnerProgram__c, ProgramLevel__c, Account__c FROM ACC_PartnerProgram__c 
                                               WHERE Account__c = :accprgm.keyset() AND ProgramLevel__c = :cntprgmlvl];
        
            System.Debug('ACC_PartnerProgram__c:'+accprgms );
        
            for(ACC_PartnerProgram__c aprgm : accprgms) {
                string name = aprgm.Account__c + ' - ' + aprgm.ProgramLevel__c;
                newaccmap.put(name,aprgm);
            }
        
            System.Debug('newaccmap:'+newaccmap);
        
            for(Account accIds : accprgm.keyset()) {
               
                for(ProgramLevel__c plvl : accprgm.get(accIds)) {
                    if(!accIds.isPartner) {
                        AccountAssessment__c acamt = new AccountAssessment__c();
                        acamt.Account__c = accIds.Id;
                        acamt.PartnerProgram__c = plvl.PartnerProgram__c;
                        acamt.ProgramLevel__c = plvl.Id;
                        accountasmts.add(acamt);
                    }
                    else if(!newaccmap.containskey(accIds.Id + ' - ' + plvl.Id)) {
                        ACC_PartnerProgram__c apprgm = new ACC_PartnerProgram__c(Account__c = accIds.Id, PartnerProgram__c = plvl.PartnerProgram__c, ProgramLevel__c = plvl.Id);
                        newaccprogram.add(apprgm);
                    }   
                }
            }
            System.Debug('>>>>>>newaccprogram' +newaccprogram);
            insert accountasmts;
            insert newaccprogram;
        }
    }    
    
}