@IsTest
public with sharing class AP_PRMUserMigrationV2_TEST {
    public static testMethod void testBatch(){
        Test.startTest();
        AP_PRMUserMigrationV2.runBatch('#hashTag');
        Test.stopTest();
    }
    
    public static testMethod void testCreateUser(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			Map<Id,PRMPartnerMigration__c> mig =Batch_PRM_UserMigration_TEST.prepareScenario();
			Test.startTest();
            try{
                AP_PRMUserMigrationV2.createUser(mig);
            }catch(AP_PRMUserMigrationV2.PRMUserMigrationException e){
                
            }
			Test.stopTest();
		}
    }
    
    public static testMethod void testCreatePermissionSet(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			Map<Id,PRMPartnerMigration__c> mig =Batch_PRM_UserMigration_TEST.prepareScenario();
			Test.startTest();
            try{
                AP_PRMUserMigrationV2.createPermissionSet(mig);
            }catch(AP_PRMUserMigrationV2.PRMUserMigrationException e){
                
            }
			Test.stopTest();
		}
    }
    
    public static testMethod void testCreateFieloMember(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			Map<Id,PRMPartnerMigration__c> mig =Batch_PRM_UserMigration_TEST.prepareScenario();
			Test.startTest();
            try{
	        	AP_PRMUserMigrationV2.createFieloMember(mig);
            }catch(AP_PRMUserMigrationV2.PRMUserMigrationException e){
                
            }
			Test.stopTest();
		}
    }
    
    public static testMethod void testgenerateOutputForMPA(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			Map<Id,PRMPartnerMigration__c> mig =Batch_PRM_UserMigration_TEST.prepareScenario();
			Test.startTest();
            try{
                AP_PRMUserMigrationV2.generateOutputForMPA(mig);
            }catch(AP_PRMUserMigrationV2.PRMUserMigrationException e){
                
            }
			Test.stopTest();
		}
    }
    
    public static testMethod void testcreateMPA(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			Map<Id,PRMPartnerMigration__c> mig =Batch_PRM_UserMigration_TEST.prepareScenario();
			Test.startTest();
            try{
                AP_PRMUserMigrationV2.createMPA(mig);
            }catch(AP_PRMUserMigrationV2.PRMUserMigrationException e){
                
            }
			Test.stopTest();
		}
    }
    
    
    public static testMethod void testUpdateErrorsInFuture(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			Map<Id,PRMPartnerMigration__c> mig =Batch_PRM_UserMigration_TEST.prepareScenario();
			Test.startTest();
            AP_PRMUserMigrationV2.updateErrorsInFuture(new Map<Id,String>{mig.values()[0].Contact__c=>'Test Error'});
			Test.stopTest();
		}
    }
    
    public static testMethod void testUncheck(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			Map<Id,PRMPartnerMigration__c> mig =Batch_PRM_UserMigration_TEST.prepareScenario();
            mig.values()[0].hasError__c=true;
            update mig.values()[0];
			Test.startTest();
            AP_PRMUserMigrationV2.uncheckHasError(null);
            Test.stopTest();
		}
    }
}