/*
* Test for WS_IDMSUIMSCompanyService and WS_IDMSUIMSCompanyServiceImpl
* */
@isTest

public class WS_IDMSUIMSCompanyServiceImplTest{
    // Test1 : Calling searchPublicCompany method from WS_IDMSUIMSCompanyServiceImpl 
    static testmethod void searchPublicCompanyTest(){
        try {
            WS_IDMSUIMSCompanyService.companyV3 template = new WS_IDMSUIMSCompanyService.companyV3() ; 
            WS_IDMSUIMSCompanyServiceImpl.AuthenticatedCompanyManager_UIMSV2_ImplPort test = new WS_IDMSUIMSCompanyServiceImpl.AuthenticatedCompanyManager_UIMSV2_ImplPort ();
            test.searchPublicCompany('callerFid',template); 
        }catch(exception e){}
    }
    // Test1 : Calling createCompany method from WS_IDMSUIMSCompanyServiceImpl 
    static testmethod void createCompanyTest(){
        try {
            WS_IDMSUIMSCompanyService.companyV3 template = new WS_IDMSUIMSCompanyService.companyV3() ; 
            WS_IDMSUIMSCompanyServiceImpl.AuthenticatedCompanyManager_UIMSV2_ImplPort test = new WS_IDMSUIMSCompanyServiceImpl.AuthenticatedCompanyManager_UIMSV2_ImplPort ();
            test.createCompany('callerFid','federatedId',template) ;
        }catch(exception e){}
    }
    // Test1 : Calling subclass from WS_IDMSUIMSCompanyService
    static testmethod void allService(){
        WS_IDMSUIMSCompanyService.companyV3 companyV3  = new WS_IDMSUIMSCompanyService.companyV3(); 
        WS_IDMSUIMSCompanyService.RequestedInternalUserException RequestedInternalUserException = new WS_IDMSUIMSCompanyService.RequestedInternalUserException (); 
        WS_IDMSUIMSCompanyService.InvalidImsServiceMethodArgumentException InvalidImsServiceMethodArgumentException = new WS_IDMSUIMSCompanyService.InvalidImsServiceMethodArgumentException (); 
        WS_IDMSUIMSCompanyService.RequestedEntryNotExistsException RequestedEntryNotExistsException = new WS_IDMSUIMSCompanyService.RequestedEntryNotExistsException (); 
        WS_IDMSUIMSCompanyService.createCompanyResponse createCompanyResponse = new WS_IDMSUIMSCompanyService.createCompanyResponse (); 
        WS_IDMSUIMSCompanyService.createCompany createCompany = new WS_IDMSUIMSCompanyService.createCompany (); 
        WS_IDMSUIMSCompanyService.searchPublicCompanyResponse searchPublicCompanyResponse = new WS_IDMSUIMSCompanyService.searchPublicCompanyResponse (); 
        WS_IDMSUIMSCompanyService.UnexpectedLdapResponseException UnexpectedLdapResponseException = new WS_IDMSUIMSCompanyService.UnexpectedLdapResponseException (); 
        WS_IDMSUIMSCompanyService.IMSServiceSecurityCallNotAllowedException IMSServiceSecurityCallNotAllowedException = new WS_IDMSUIMSCompanyService.IMSServiceSecurityCallNotAllowedException (); 
        WS_IDMSUIMSCompanyService.LdapTemplateNotReadyException LdapTemplateNotReadyException = new WS_IDMSUIMSCompanyService.LdapTemplateNotReadyException (); 
        WS_IDMSUIMSCompanyService.searchPublicCompany searchPublicCompany = new WS_IDMSUIMSCompanyService.searchPublicCompany (); 
        
    }
    
}