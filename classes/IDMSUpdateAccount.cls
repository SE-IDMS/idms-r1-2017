/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class is used to update account of a user
**/
public without sharing class IDMSUpdateAccount{
    
    static user userdetails          = new user();
    static IDMSCaptureError errorLog = new IDMSCaptureError();
    
    //This is a future method to update account 
    @future
    public static void updateAccount(String personContactId1,string userperacc2) {
        user userPerAcc1 = (user)JSON.deserialize(userperacc2,user.class);

        Schema.FieldSet personAccField= Schema.SObjectType.Account.fieldSets.getMap().get(Label.CLJUN16IDMS105);
        List<Schema.FieldSetMember> personAccFieldObj=personAccField.getFields();
        IDMS_field_Mapping__c userfieldname;
        String SOQLquery = 'select id';
        for(Schema.FieldSetMember personAccFieldObjItr:personAccFieldObj){
            SOQLquery = SOQLquery + ',' + personAccFieldObjItr.getFieldPath();
            system.debug('---SOQLquery ---'+SOQLquery );
        }
        
        system.debug(personContactId1);
        SOQLquery = SOQLquery + ' from account WHERE PersonContactId =:personContactId1 limit 1';
        account userAcc = Database.query(SOQLquery);
        system.debug(userAcc);
    
        for(Schema.FieldSetMember personAccFieldObjItr:personAccFieldObj){
            String accountfieldAPIname = personAccFieldObjItr.getFieldPath();
            system.debug('accountfieldAPIname+++++ '+accountfieldAPIname); 
            userfieldname = IDMS_field_Mapping__c.getInstance(accountfieldAPIname);
            system.debug('user firld name '+userfieldname);
            String userfieldAPIname = (String)userfieldname.get('userfield__c');
            system.debug('fields values+++++'+userfieldAPIname);
            
            if(String.isBlank((String)(userAcc.get(accountfieldAPIname)))){
                     if(userfieldAPIname.equalsignorecase(Label.CLJUN16IDMS110) || userfieldAPIname.equalsignorecase('country')){
                    if(userAcc.country__pc == null)
                    { 
                        userAcc.country__pc = getCountryIdUpdate(userPerAcc1.country);
                    }
                    if(userAcc.country__c == null){ 
                        userAcc.country__c=getCountryIdUpdate(userPerAcc1.country);
                    }
                    if(string.isblank(userAcc.StateProvince__c)){
                        userAcc.StateProvince__c=getStateId(userPerAcc1.state,userPerAcc1.country__c);
                    }
                }                           
                else{
                    userAcc.put(accountfieldAPIname,userPerAcc1.get(userfieldAPIname));
                }
            }  
            
            system.debug('--skipped blank----');
            system.debug(userAcc);
        }
        if(userPerAcc1.email != null && userPerAcc1.email != ''){
            userAcc.IDMS_Contact_UserExternalId__pc = userPerAcc1.email+Label.CLJUN16IDMS71;
            userAcc.IDMS_Account_UserExternalId__c  = userPerAcc1.email+Label.CLJUN16IDMS71;
        }
        system.debug(userAcc);
        try{
            update userAcc;
            system.debug(userAcc);
        }
        catch(Exception e){
            system.debug(e.getMessage());
            errorLog.IDMScreateLog('IDMSUserRest','IdmsUserService','updateAccount Future',userdetails.username,e.getMessage(),'',Datetime.now(),'',
                                   userdetails.federationIdentifier,false,'',false,'Step 5: Error in account update');
        }    
    }
    //This method is used to get country id to update
    static id getCountryIdUpdate(string countrycode){
        country__c cid = [select id from country__c where countrycode__c = :countrycode limit 1];
        return cid.id;
    } 
    
    //This method is used to get state id on the basis of state and country code
    static id getStateID(string statecode,string countrycode){
        if(String.isnotblank(statecode)){ 
            List<StateProvince__c> state=[select id from StateProvince__c where 
                                          StateProvinceCode__c=:statecode and CountryCode__c = :countrycode ];
            if(state != null && state.size() >0){
                system.debug('state id '+state[0].id);
                return state[0].id;
            }
        }
        return null;           
    }
}