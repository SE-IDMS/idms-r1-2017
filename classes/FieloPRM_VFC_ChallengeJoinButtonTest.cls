/********************************************************************
* Company: Fielo
* Created Date: 31/07/2015
* Description:
********************************************************************/
@isTest
private class FieloPRM_VFC_ChallengeJoinButtonTest {
    
    @isTest static void test_method_1() {
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(FieloEE__Member__c = false);
            insert deactivate;

            Country__c country = Utils_TestMethods.createCountry();
            insert country;

            Account acc = new Account(Name = 'acc');
            insert acc;
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c= 'Polo', 
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test', 
                F_Account__c = acc.Id, 
                F_Country__c = country.Id,
                F_PRM_ContactRegistrationStatus__c = 'Validated'
            );
            insert member;

            member = [select FieloEE__User__c, F_Account__c from fieloee__member__c where id=:member.Id]; 

            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;

            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc.Id});

            FieloEE__Member__c member2 = FieloEE.MockUpFactory.createMember('Test2 member2', '56789', 'DNI');
            
            member2.F_PRM_ContactRegistrationStatus__c = 'Validated';
            update member2;

            FieloCH__Challenge__c ch = FieloPRM_UTILS_MockUpFactory.createChallenge('testCH', 'Competition','Objective');
            FieloCH__Challenge__c ch2 = FieloPRM_UTILS_MockUpFactory.createChallenge('testCH', 'Competition','Objective');
            ch2.FieloCH__Mode__c = 'Teams';
            update ch2;
            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__BadgeMember__c', 'count', 'name', 'Equals or greater than', 1);
            FieloCH__MissionCriteria__c missionCriteria = FieloPRM_UTILS_MockUpFactory.createCriteria('text', 'F_PRM_BadgeName__c', 'equals', 'Test', null, null, false, mission1.Id);

            FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(ch.id , mission1.id);
            FieloCH__MissionChallenge__c missionChallenge2  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(ch2.id , mission1.id);
            FieloEE__Badge__c badge = new FieloEE__Badge__c (name='test', F_PRM_Type__c = 'Program Level', F_PRM_BadgeAPIName__c = '1');
            FieloEE__Badge__c badge2 = new FieloEE__Badge__c (name='test2', F_PRM_Type__c = 'Program Level', F_PRM_BadgeAPIName__c = '2');

            List<FieloEE__Badge__c> badges = new List<FieloEE__Badge__c>{badge, badge2};
            insert badges;
            
            FieloCH__ChallengeReward__c chr1 = new FieloCH__ChallengeReward__c(FieloCH__LogicalExpression__c = '1', FieloCH__Challenge__c = ch.Id, FieloCH__Badge__c= badges[0].id);
            FieloCH__ChallengeReward__c chr2 = new FieloCH__ChallengeReward__c(FieloCH__LogicalExpression__c = '1',FieloCH__Challenge__c = ch.Id, FieloCH__Badge__c= badges[1].id);

            FieloCH__ChallengeReward__c chr3 = new FieloCH__ChallengeReward__c(FieloCH__LogicalExpression__c = '1', FieloCH__Challenge__c = ch2.Id, FieloCH__Badge__c= badges[0].id);
            FieloCH__ChallengeReward__c chr4 = new FieloCH__ChallengeReward__c(FieloCH__LogicalExpression__c = '1',FieloCH__Challenge__c = ch2.Id, FieloCH__Badge__c= badges[1].id);

            List<FieloCH__ChallengeReward__c> chRewards = new List<FieloCH__ChallengeReward__c>{chr1,chr2,chr3,chr4};
            insert chRewards;
            ch.FieloCH__isActive__c = true; 
            update ch; 

            ch2.FieloCH__isActive__c = true; 
            update ch2; 

            FieloEE__Menu__c menu = new FieloEE__Menu__c();
            insert menu;
            
            FieloEE.MemberUtil.setMemberId(member.Id);

            FieloPRM_VFC_ChallengeJoinButton controller = new FieloPRM_VFC_ChallengeJoinButton();
            
            FieloEE__Component__c component = new FieloEE__Component__c();
            component.F_PRM_CreateEnrollmentEvent__c = true;
            component.FieloEE__Menu__c = menu.id;
            insert component;
                            
            controller.component = component; 
            controller.challengeId = ch.Id;
            controller.joinAction();
            controller.challengeId = ch.Id;
      
            FieloPRM_VFC_ChallengeJoinButton controller2 = new FieloPRM_VFC_ChallengeJoinButton();
            
            FieloEE__Component__c components2 = new FieloEE__Component__c();
            components2.F_PRM_CreateEnrollmentEvent__c = true;
            components2.FieloEE__Menu__c = menu.id;

            insert components2;
                        
            controller2.component = components2; 
            controller2.challengeId = ch2.Id;

        }
    }
    
    @isTest static void test_method_2() {
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        
        update us;
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(FieloEE__Member__c = false);
            insert deactivate;

            Country__c country = Utils_TestMethods.createCountry();
            insert country;

            Account acc = new Account(Name = 'acc');
            insert acc;
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c= 'Polo', 
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()), 
                FieloEE__Street__c = 'test', 
                F_Account__c = acc.Id, 
                F_Country__c = country.Id,
                F_PRM_ContactRegistrationStatus__c = 'Validated'
            );
            insert member;

            member = [select FieloEE__User__c, F_Account__c from fieloee__member__c where id=:member.Id]; 

            FieloEE__Program__c program = new FieloEE__Program__c();
            program.Name = 'PRM Is On';
            program.FieloEE__SiteURL__c = 'http://www.google.com';
            program.FieloEE__RecentRewardsDays__c = 18;
            insert program;

            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc.Id});

            FieloEE__Member__c member2 = FieloEE.MockUpFactory.createMember('Test2 member2', '56789', 'DNI');
            
            member2.F_PRM_ContactRegistrationStatus__c = 'Validated';
            update member2;

            FieloEE__Menu__c menu = new FieloEE__Menu__c();
            insert menu;

            FieloCH__Challenge__c ch = FieloPRM_UTILS_MockUpFactory.createChallenge('testCH', 'Competition','Objective');
            FieloCH__Challenge__c ch2 = FieloPRM_UTILS_MockUpFactory.createChallenge('testCH', 'Competition','Objective');
            
            ch2.FieloCH__Mode__c = 'Teams';
            ch.F_PRM_MenuProgram__c = menu.Id;
            update new List<FieloCH__Challenge__c>{ch,ch2};
            
            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__BadgeMember__c', 'count', 'name', 'Equals or greater than', 1);
            FieloCH__MissionCriteria__c missionCriteria = FieloPRM_UTILS_MockUpFactory.createCriteria('text', 'F_PRM_BadgeName__c', 'equals', 'Test', null, null, false, mission1.Id);

            FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(ch.id , mission1.id);
            FieloCH__MissionChallenge__c missionChallenge2  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(ch2.id , mission1.id);
            FieloEE__Badge__c badge = new FieloEE__Badge__c (name='test', F_PRM_Type__c = 'Program Level', F_PRM_BadgeAPIName__c = '1');
            FieloEE__Badge__c badge2 = new FieloEE__Badge__c (name='test2', F_PRM_Type__c = 'Program Level', F_PRM_BadgeAPIName__c = '2');

            List<FieloEE__Badge__c> badges = new List<FieloEE__Badge__c>{badge, badge2};
            insert badges;
            
            FieloCH__ChallengeReward__c chr1 = new FieloCH__ChallengeReward__c(FieloCH__LogicalExpression__c = '1', FieloCH__Challenge__c = ch.Id, FieloCH__Badge__c= badges[0].id);
            FieloCH__ChallengeReward__c chr2 = new FieloCH__ChallengeReward__c(FieloCH__LogicalExpression__c = '1',FieloCH__Challenge__c = ch.Id, FieloCH__Badge__c= badges[1].id);

            FieloCH__ChallengeReward__c chr3 = new FieloCH__ChallengeReward__c(FieloCH__LogicalExpression__c = '1', FieloCH__Challenge__c = ch2.Id, FieloCH__Badge__c= badges[0].id);
            FieloCH__ChallengeReward__c chr4 = new FieloCH__ChallengeReward__c(FieloCH__LogicalExpression__c = '1',FieloCH__Challenge__c = ch2.Id, FieloCH__Badge__c= badges[1].id);

            List<FieloCH__ChallengeReward__c> chRewards = new List<FieloCH__ChallengeReward__c>{chr1,chr2,chr3,chr4};
            insert chRewards;
            ch.FieloCH__isActive__c = true; 
            update ch; 

            ch2.FieloCH__isActive__c = true; 
            update ch2; 
            
            FieloEE.MemberUtil.setMemberId(member.Id);

            FieloPRM_VFC_ChallengeJoinButton controller = new FieloPRM_VFC_ChallengeJoinButton();
            
            FieloEE__Component__c component = new FieloEE__Component__c();
            component.F_PRM_CreateEnrollmentEvent__c = true;
            component.FieloEE__Menu__c = menu.id;
            insert component;
                            
            controller.component = component; 
            controller.challengeId = ch.Id;
            controller.joinAction();
            controller.challengeId = ch.Id;
      
            FieloPRM_VFC_ChallengeJoinButton controller2 = new FieloPRM_VFC_ChallengeJoinButton();
            
            FieloEE__Component__c components2 = new FieloEE__Component__c();
            components2.F_PRM_CreateEnrollmentEvent__c = true;
            components2.FieloEE__Menu__c = menu.id;

            insert components2;
                        
            controller2.component = components2; 
            controller2.challengeId = ch2.Id;

        }

    }    
    
}