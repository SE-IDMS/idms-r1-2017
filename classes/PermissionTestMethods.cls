@isTest
public class PermissionTestMethods {

    public static List<PermissionSetGroup__c> createPermissionSetGroups() {
        
        List<PermissionSet> psList = createPermissionSets(5);
        
        List<PermissionSetGroup__c> psGroupList = new List<PermissionSetGroup__c>();
        
        for(Integer i = 0; i < 5; i++) {
            psGroupList.add(new PermissionSetGroup__c(Name = 'Ps Group ' + i,
                                                      Description__c = 'Description i',
                                                      Stream__c = 'Service',
                                                      Type__c = 'Core'));
        }     
        
        insert psGroupList;
        
        List<PsGroupMember__c> psGroupMemberList = new List<PsGroupMember__c>();
        
        for(PermissionSetGroup__c psGroup:[SELECT Id FROM PermissionSetGroup__c]) {
            
            for(PermissionSet ps:psList) {
                psGroupMemberList.add(new PsGroupMember__C(PermissionSetGroup__c = psGroup.Id,
                                                           PermissionSetId__c = ps.Id,
                                                           Name = ps.Label));
            }
        } 
        
        insert psGroupMemberList;
        
        return psGroupList;
    }
    
    public static List<PermissionSet> createPermissionSets(Integer psNumber) {
    
        List<PermissionSet> psList = new List<PermissionSet>();
    
        for(Integer i = 0; i < psNumber; i++) {
            PermissionSet ps = new PermissionSet(Name = 'TestPS' +  i, Label = 'Test PS ' + i);
            psList.add(ps);
        }
        
        insert psList;

        List<ObjectPermissions> objPermissions = new List<ObjectPermissions>();

        for(PermissionSet ps:[SELECT Id FROM PermissionSet WHERE Id IN :psList]) {
            
            objPermissions.add(new ObjectPermissions(ParentId = ps.Id, 
                                                     SobjectType = 'Account',
                                                     PermissionsRead = true,
                                                     PermissionsCreate = true,
                                                     PermissionsDelete = true,
                                                     PermissionsEdit = true,
                                                     PermissionsModifyAllRecords = true,
                                                     PermissionsViewAllRecords = true));

            objPermissions.add(new ObjectPermissions(ParentId = ps.Id, 
                                                     SobjectType = 'Contact',
                                                     PermissionsRead = true,
                                                     PermissionsCreate = true,
                                                     PermissionsDelete = true,
                                                     PermissionsEdit = true,
                                                     PermissionsModifyAllRecords = true,
                                                     PermissionsViewAllRecords = true));
            
        }
        
        insert objPermissions;
        
        return psList;
    }
}