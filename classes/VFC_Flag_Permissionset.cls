/*
 * Description : The relation ship between user and permission set is through permsision set assignment object.
 * PermissionSetAssignment - PermissionSetId,UserId
 * PermissionSetGroup - Name,Type,Strean,description
 * PermissionSetGroupMembers - Name (name of the permission set.)
 * PermissionSetGroupAssignmentRequest - Assignee,Status,PermissionSetGroup
 * query the permissiongroup which requires validation , now get the list of permission sets that require validation.
 * query the permissionsetgroupassignmentrequest - where assignee is that user , from the ones which require validation which of them they raised a request.
 * permission sets group by status.
 * */
 
public with sharing class VFC_Flag_Permissionset{
    private Id userId;
    public List<AssignmentInfo> assignments {get;set;}        
    public Boolean removepermissions {get;set;}
    public String errorassignments {get;set;}
    public VFC_Flag_Permissionset(ApexPages.StandardController psGroupController) {          
          User userrecord = (User)psGroupController.getRecord();         
          userId = userrecord.Id;   
          assignments = new List<AssignmentInfo>();
          removepermissions=false;
          errorassignments='';
    }

    /*
    * Description : Get the list of names of permission sets that require validation.
    */
    public Set<String> getPermissionSetsThatRequireValidation(){
        Set<String> permissionsetnames = new Set<String>();
        for(PermissionSetGroup__c grouprecord : [select Id,(select Name from PermissionSetGroupMembers__r) from PermissionSetGroup__c where requiresvalidation__c = true])
        {
            for(psGroupMember__c groupmember:grouprecord.PermissionSetGroupMembers__r)
            permissionsetnames.add(groupmember.Name);
        }
        return permissionsetnames;
    }


    /*
    * Description : Get the list of names of permission sets when we pass a list of permission set groups
    */
    public Set<String> getPermissionSetNamesForGroups(Set<String> permissionsetgroupsset){
        List<String> permissionsetgroups = new List<String>();
        permissionsetgroups.addAll(permissionsetgroupsset);
        Set<String> permissionsetnames = new Set<String>();
        for(PermissionSetGroup__c grouprecord : [select Id,(select Name from PermissionSetGroupMembers__r) from PermissionSetGroup__c where Name in :permissionsetgroups])
        {
            for(psGroupMember__c groupmember:grouprecord.PermissionSetGroupMembers__r)
            permissionsetnames.add(groupmember.Name);
        }
        return permissionsetnames;
    }

    /*
    * get users current assigned permission sets   
    */
    public Map<String,PermissionSetAssignment> getAssignedPermissions(){
        Map<String,PermissionSetAssignment> permissionsetnamesmap = new Map<String,PermissionSetAssignment>();
        for(User userrecord_temp: [select Id,Name,(select PermissionSet.Label,Id from PermissionSetAssignments where PermissionSet.isOwnedByProfile=false) from User where id = :userId])
        {
            for(PermissionSetAssignment assignmentrecord : userrecord_temp.PermissionSetAssignments){
                permissionsetnamesmap.put(assignmentrecord.PermissionSet.Label,assignmentrecord);
            }
        }   
        return permissionsetnamesmap;       
    }

    /*
    * When we pass the list of permission set names, determine which ones are having a request raised grouped by status
    */
    public PageReference validatePermissions(){
        List<PermissionSetAssignmentRequests__c> permissionsetassignmentrequestsforuser = [select PermissionSetGroup__r.Name,assignee__c,status__c from PermissionSetAssignmentRequests__c where assignee__c = :userId and status__c='Assigned'];
        Set<String> permissionsetgroupnames = new Set<String>();
        for(PermissionSetAssignmentRequests__c assignmentrequest : permissionsetassignmentrequestsforuser)
        {
            permissionsetgroupnames.add(assignmentrequest.PermissionSetGroup__r.Name);
        }
        Set<String> permissionsetwithrequests = getPermissionSetNamesForGroups(permissionsetgroupnames);
        Map<String,PermissionSetAssignment> assignedpermissionsmap = getAssignedPermissions();
        System.debug(assignedpermissionsmap);
        Set<String> requiresvalidationpermissionsets = getPermissionSetsThatRequireValidation(); 
                            
        //determine the ones which are assigned but without requests        
        Set<String> permissionsassignedwithoutrequest = new Set<String>();
        for(String assignedpermission : assignedpermissionsmap.keyset())
        {
            if(!permissionsetwithrequests.contains(assignedpermission))
            permissionsassignedwithoutrequest.add(assignedpermission);
        }
        
        Set<String> permissionsetnamesdelete = new Set<String>();
        List<String> errorassignmentslist = new List<String>();
        for(String requriedpermissionset : requiresvalidationpermissionsets){
            if(permissionsassignedwithoutrequest.contains(requriedpermissionset))
            {                   
                errorassignmentslist.add(requriedpermissionset);
                assignments.add(new AssignmentInfo(requriedpermissionset,assignedpermissionsmap.get(requriedpermissionset).Id,'No request found , the assignment is deleted'));
            }
        }
        if(assignments.size()>0){
             errorassignments = String.join(errorassignmentslist,',');
             removepermissions=true;
        }
        return null;
    }

    /*
    * Remove unauthorized permissions would delete the assignments
    */

    public PageReference removeunauthorizedassignments(){
        if(removepermissions){
        List<PermissionSetAssignment> permissionsetassignments = new List<PermissionSetAssignment>();
        for(AssignmentInfo info : assignments){
            permissionsetassignments.add(new PermissionSetAssignment(Id = info.permissionsetassignmentId));
        }
        List<Database.DeleteResult> deleteresult = Database.delete(permissionsetassignments,true);
        }
        return null;
            
    }

    /*
    * AssignmentInfo is an inner class to capture the error message
    */

    public class AssignmentInfo {

        public String permissionsetname {get;set;}
        public String permissionsetassignmentId {get;set;}
        public String errormessage {get;set;}

        public AssignmentInfo(String permissionsetname,String permissionsetassignmentId,String errormessage){
            this.permissionsetname=permissionsetname;
            this.permissionsetassignmentId=permissionsetassignmentId;
            this.errormessage=errormessage;
        }
    }

}