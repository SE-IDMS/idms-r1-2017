/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  06-Feb-2013
    Description         : Class for Connect Country Cascading Milestone Sharing Permission
*/

public class ConnectCountryCascadingMilestoneTriggers
{
        
            Public Static void ConnectCountryCascadingMilestoneBeforeInsertUpdate(List<Country_Cascading_Milestone__c> CCMs)
     {
    
        list<Cascading_Milestone__c> CasMile=new list<Cascading_Milestone__c>();
        CasMile=[select Name,Program_Scope__c,Entity__c,Countries__c from Cascading_Milestone__c where Name=:CCMs[0].CascadingMilestoneName__c];
            CCMs[0].ValidateScopeonInsertUpdate__c=false;             
            for(Cascading_Milestone__c CM:CasMile){   
            if(CCMs[0].CascadingMilestoneName__c == CM.Name && !(CM.Countries__c == null)){                   
             if(!(CM.Countries__c.Contains(CCMs[0].Country__c)))
                 CCMs[0].ValidateScopeonInsertUpdate__c=true;                      
            }
            if(CCMs[0].CascadingMilestoneName__c == CM.Name && (CM.Countries__c == null))         
            CCMs[0].ValidateScopeonInsertUpdate__c=true;
            }
         }
         
   Public Static void ConnectCountryCascadingMilestoneAfterInsert(List<Country_Cascading_Milestone__c> CCM)
      {
      
    // Add permission to Country Deployment Leader
      
         List<Country_Cascading_Milestone__Share>  CCMShare = new List<Country_Cascading_Milestone__Share>();
         List<Country_Champions__c> CChamp = new List<Country_Champions__c>();
         
         CChamp = [Select User__c, Country__c from Country_Champions__c where Country__c = :CCM[0].Country__c];
         
         
         for(Country_Cascading_Milestone__c CCMile : CCM){
           
           Country_Cascading_Milestone__Share cshare = new Country_Cascading_Milestone__Share();
           
           cshare.ParentId = CCMile.id;
           cshare.UserOrGroupId = CCMile.Country_Deployment_Leader__c;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Country_Cascading_Milestone__Share.RowCause.SRCountry_Cascading_Milestone__c;
           CCMShare.add(cshare);
          }
         Database.insert( CCMShare,false);
         
      //Add share permission for Country Champions
      
       for(Country_Cascading_Milestone__c CCMile : CCM){
          for(Country_Champions__c  CC:CChamp){
            
            Country_Cascading_Milestone__Share cshare = new Country_Cascading_Milestone__Share();
           
            cshare.ParentId = CCMile.id;
            cshare.UserOrGroupId = CC.User__c;
            cshare.AccessLevel = 'edit';
            cshare.RowCause = Schema.Country_Cascading_Milestone__Share.RowCause.Country_Champs__c;
            CCMShare.add(cshare);
          }
         }
         Database.insert( CCMShare,false);      
                
      // Add share permission for Program Leaders and Deployment Leaders
         
         for(Country_Cascading_Milestone__c CCMile : CCM){
           
            Country_Cascading_Milestone__Share cshare = new Country_Cascading_Milestone__Share();
           
           cshare.ParentId = CCMile.id;
           cshare.UserOrGroupId = Label.ConnectCountryGroup;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Country_Cascading_Milestone__Share.RowCause.Connect_Leaders__c;
           CCMShare.add(cshare);
          }
         Database.insert(CCMShare,false);  
       }
       
 Public Static void CountryCascadingMilestoneAfterUpdate(List<Country_Cascading_Milestone__c> CCM, Country_Cascading_Milestone__c CCMold)
      {
      
      System.Debug('Country Cascading Milestone Updates Starts ');
      
      List<Country_Cascading_Milestone__Share>  CCMShare = new List<Country_Cascading_Milestone__Share>();
      List<Country_Champions__c> CChamp = new List<Country_Champions__c>();
         
      CChamp = [Select User__c, Country__c from Country_Champions__c where Country__c = : CCM[0].Country__c];
      system.debug('Champion Name ******** ' + CCM.size());   
          
      List<Id> lstId1 = new List<Id>();
      
      // Update share permission for Data Reporter
        if(CCMold.Country_Deployment_Leader__c != CCM[0].Country_Deployment_Leader__c){  
         for(Country_Cascading_Milestone__c CCMile : CCM){
           
           Country_Cascading_Milestone__Share cshare = new Country_Cascading_Milestone__Share();
           
           cshare.ParentId = CCMile.id;
           cshare.UserOrGroupId = CCMile.Country_Deployment_Leader__c;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Country_Cascading_Milestone__Share.RowCause.SRCountry_Cascading_Milestone__c;
           CCMShare.add(cshare);
          }      
         
         Database.insert(CCMShare,false);   
         
        // Add share permission for Program Leaders and Deployment Leaders
         
                
         for(Country_Cascading_Milestone__c CCMile : CCM){
           
            Country_Cascading_Milestone__Share cshare = new Country_Cascading_Milestone__Share();
           
           cshare.ParentId = CCMile.id;
           cshare.UserOrGroupId = Label.ConnectCountryGroup;
           cshare.AccessLevel = 'edit';
           cshare.RowCause = Schema.Country_Cascading_Milestone__Share.RowCause.Connect_Leaders__c;
           CCMShare.add(cshare);
          }
         Database.insert(CCMShare,false);                 
        }
         
     // Delete existing Champions Permissions
     
        if(CCMold.Country__c != CCM[0].Country__c){  
         For(Country_Cascading_Milestone__Share  CCMSharedeleteCDL :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Country_Cascading_Milestone__Share where ParentId = :CCMold.Id and RowCause = :Schema.Country_Cascading_Milestone__Share.RowCause.Country_Champs__c ]){
         if(CCMSharedeleteCDL.Parentid != null)              
          Database.delete(CCMSharedeleteCDL,false);
           }
          } 
      //Add share permission for Country Champions    
       for(Country_Cascading_Milestone__c CCMile : CCM){
         
          for(Country_Champions__c  CC:CChamp){
           
            Country_Cascading_Milestone__Share cshare = new Country_Cascading_Milestone__Share();           
            cshare.ParentId = CCMile.id;
            cshare.UserOrGroupId = CC.User__c;
            cshare.AccessLevel = 'edit';
            cshare.RowCause = Schema.Country_Cascading_Milestone__Share.RowCause.Country_Champs__c;
            CCMShare.add(cshare);
          }
         }
         Database.insert( CCMShare,false);  
         
        
     if(CCMold.Country_Deployment_Leader__c != CCM[0].Country_Deployment_Leader__c){  
     For(Country_Cascading_Milestone__Share  CCMSharedeleteDL :[Select ParentId, UserOrGroupId, AccessLevel, RowCause from Country_Cascading_Milestone__Share where ParentId = :CCMold.Id and UserOrGroupId = :CCMold.Country_Deployment_Leader__c and RowCause = :Schema.Country_Cascading_Milestone__Share.RowCause.SRCountry_Cascading_Milestone__c ]){
     if(CCMSharedeleteDL.Parentid != null)              
      Database.delete(CCMSharedeleteDL,false);
       }
      } 
      
     
       
     }
    Public Static void ConnectCountryCMBeforeInsertUpdate(List<Country_Cascading_Milestone__c> CCM)
      {
      
          // Update Program ID of all the records
       
          CCM[0].Global_Program_id__c = CCM[0].Global_Program_Number__c;
          List<Country_Champions__c> CChamp = new List<Country_Champions__c>();
         
          CChamp = [Select User__c, Country__c from Country_Champions__c where Country__c = : CCM[0].Country__c and IsDefault__c = :'Yes'];
          if(CChamp.size() > 0)
              CCM[0].Champion_Name__c = CChamp[0].User__c;
          else
               CCM[0].Champion_Name__c = null;
        }
       
       // Auto update Progress Summary on Completion of Country Milestones
 
 Public Static void CountryMilestoneBeforeUpdate_ProgressSummaryUpdate(List<Country_Cascading_Milestone__c> CMile) {
             
               for(Country_Cascading_Milestone__c M:CMile){
               
                   if ((M.Progress_Summary_Q1_Country__c !=null) && M.Progress_Summary_Q1_Country__c.Contains('Completed')){
                        M.Progress_Summary_Q2_Country__c = M.Progress_Summary_Q1_Country__c;
                        M.Progress_Summary_Q3_Country__c = M.Progress_Summary_Q1_Country__c;
                        M.Progress_Summary_Q4_Country__c = M.Progress_Summary_Q1_Country__c;
                        }
                        
                   if ( (M.Progress_Summary_Q2_Country__c != null) && M.Progress_Summary_Q2_Country__c.Contains('Completed')){
                        M.Progress_Summary_Q3_Country__c = M.Progress_Summary_Q2_Country__c;
                        M.Progress_Summary_Q4_Country__c = M.Progress_Summary_Q2_Country__c;
                        }
                        
                   if ( (M.Progress_Summary_Q3_Country__c !=null ) && M.Progress_Summary_Q3_Country__c.Contains('Completed')){
                        M.Progress_Summary_Q4_Country__c = M.Progress_Summary_Q3_Country__c;
                        }
                   
                    } // For Loop Ends
                  }
     
          
    }