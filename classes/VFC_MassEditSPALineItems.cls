public class VFC_MassEditSPALineItems {
private final SPARequest__c sparequest;
public List<Wrapper> lstWrapper{get;set;}{lstWrapper=new List<Wrapper>();}
public List<selectOption> selectoptions{get;set;}{selectoptions=new List<selectOption>();}
public String selectedoption{get;set;}{selectedoption='all';}
public List<selectOption> viewselectoptions{get;set;}
{
    viewselectoptions=new List<SelectOption>();
    viewselectoptions.add(new selectOption('all','All')); //add the first option
    viewselectoptions.add(new selectOption('mylocalpricinggroup','My Local Pricing Group')); //add the second option
}    

private Id sparequestid;
public boolean isAuthorized{get;set;}{isAuthorized=true;}
private List<SPARequestPricingDeskMember__c> requestpricingdeskmembers=new List<SPARequestPricingDeskMember__c>();
    public VFC_MassEditSPALineItems(ApexPages.StandardSetController controller) {
        if(ApexPages.currentPage().getParameters().containsKey('id'))
        {
            
            sparequestid=ApexPages.currentPage().getParameters().get('id');            
            checkAuthorization();   
            populateWrapper();                     
       }
       else
       {
       ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,System.Label.CLSEP12SLS25));
       }
    }
    
    public void populateWrapper()
    {
        List<SPARequestLineItem__c> lineitems=new List<SPARequestLineItem__c>();
       if(selectedoption=='all')       
           lineitems=[SELECT ProductLineManager__c,ApprovalComments__c,Business__c,CommercialReference__c,LocalPricingGroup__c,Name,NetLineAmount__c,RecommendationStatus__c,StandardPriceLineAmount__c,TargetQuantity__c FROM SPARequestLineItem__c where SPARequest__c=:sparequestid];        
       else if(selectedoption=='mylocalpricinggroup')
           lineitems=[SELECT ProductLineManager__c,ApprovalComments__c,Business__c,CommercialReference__c,LocalPricingGroup__c,Name,NetLineAmount__c,RecommendationStatus__c,StandardPriceLineAmount__c,TargetQuantity__c FROM SPARequestLineItem__c where SPARequest__c=:sparequestid and ProductLineManager__c=:UserInfo.getUserId()];                
           
       lstWrapper.clear();    
       for(SPARequestLineItem__c lineitem:lineitems)
        {
           if(selectoptions.size()==0)
           selectoptions=getPickValues(lineitem,'RecommendationStatus__c',null);        
           Wrapper tempwrapper=new Wrapper(lineitem);
           lstWrapper.add(tempwrapper);
        }       
    }
    
    class Wrapper
    {
        public SPARequestLineItem__c lineitem{get;set;}
        public String status{get;set;}
        public String newcomments{get;set;}{newcomments='';}        
        public String existingcomments{get;set;}{existingcomments='';}        
        public boolean canEditStatus{get;set;}{canEditStatus=false;}
        public Wrapper(SPARequestLineItem__c lineitem)
        {
            this.lineitem=lineitem;
            this.status=lineitem.RecommendationStatus__c ;
            if(lineitem.ApprovalComments__c!=null)
            this.existingcomments=lineitem.ApprovalComments__c;                         
            if(lineitem.ProductLineManager__c==UserInfo.getUserId())
            this.canEditStatus=true;            
        }
    }
    
    private void checkAuthorization()
    {
      requestpricingdeskmembers= [select id from SPARequestPricingDeskMember__c where SPARequest__c=:sparequestid and SPAPricingDesk__r.Member__c=:UserInfo.getUserId()];
      if(requestpricingdeskmembers.size()==0)
      isAuthorized=false;      
    }          
    
    public PageReference saveAll()
    {
        boolean haserror=false;
        List<SPARequestLineItem__c> tobeupdatedlineitems=new List<SPARequestLineItem__c>();
        for(Wrapper wrp:lstWrapper)
        {            
            if(wrp.lineitem.ApprovalComments__c!=(wrp.existingcomments+wrp.newcomments) || wrp.lineitem.RecommendationStatus__c!=wrp.status)
            {
                wrp.existingcomments=wrp.existingcomments.trim();
                wrp.newcomments=wrp.newcomments.trim();                
                if(wrp.newcomments!='')
                wrp.lineitem.ApprovalComments__c=wrp.existingcomments+'<br> <a href="/'+UserInfo.getUserId()+'">'+UserInfo.getName()+'</a> : '+wrp.newcomments;
                                
                wrp.lineitem.RecommendationStatus__c=wrp.status;
                tobeupdatedlineitems.add(wrp.lineitem);
            }
        }
        List<Database.SaveResult> saveresults=Database.update(tobeupdatedlineitems,false);
        for(Database.SaveResult sr:saveresults)
        {
            if(!sr.isSuccess()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.ERROR,sr.getErrors()[0].getMessage()));
            haserror=true;
            }
        }        
        if(haserror)
        return null;
        else
           return new PageReference('/'+sparequestid);
    }
    
    public List<selectOption> getPickValues(Sobject object_name, String field_name, String first_val) {
      List<selectOption> options = new List<selectOption>(); //new list for holding all of the picklist options
      if (first_val != null) { //if there is a first value being provided
         options.add(new selectOption(first_val, first_val)); //add the first option
      }
      Schema.sObjectType sobject_type = object_name.getSObjectType(); //grab the sobject that was passed
      Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
      Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
      List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
      for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
         options.add(new selectOption(a.getLabel(), a.getValue())); //add the value and label to our final list
      }
      return options; //return the List
   }
   

}