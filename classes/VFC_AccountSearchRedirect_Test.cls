/**

 */
@isTest
private class VFC_AccountSearchRedirect_Test {

    static testMethod void myUnitTest() {
        PageReference vfPage = Page.VFP_AccountSearchRedirect;
        Test.setCurrentPage(vfPage);
         Account acc = new Account();
        VFC_AccountSearchRedirect vfc = new VFC_AccountSearchRedirect(new ApexPages.StandardController(acc ));
        vfc.redirect();
        
        Id profilesId = [select id from profile where name='SE - Agent (Global)'].Id;

             Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345');
            insert PartnerAcc;
            
            Contact PartnerCon = new Contact(
                    FirstName='Test',
                    LastName='lastname',
                    AccountId=PartnerAcc.Id,
                    JobTitle__c='Z3',
                    CorrespLang__c='EN',
                    WorkPhone__c='1234567890'
                    );
            
            Insert PartnerCon;
            User   u1= new User(Username = 'testUserOne@schneider-electric.com', LastName = 'User11', alias = 'tuser1',
                    CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                    Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId, ContactID = PartnerCon.Id, UserPermissionsSFContentUser=true );
   insert u1;  
     PartnerAcc.TECH_AccountOwner__c=u1.Id;
   update PartnerAcc;
  
            
            List<Profile> profiles = [select id from profile where name='SE - Agent (Global)'];
        if(profiles.size()>0)
        {
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19'); 
   
            system.runas(u1)
            {
                
                VFC_AccountSearchRedirect vfc2 = new VFC_AccountSearchRedirect(new ApexPages.StandardController(acc ));
                vfc2.redirect();
            }
        }  
    }
}