@isTest
private class AP_ITB_Assets_PrimaryAssetCheck_TEST{
    public static testMethod void testITBAssetPrimaryAssetCheck(){
        System.debug('#### AP_ITB_Assets_PrimaryAssetCheck_Test.testITBAssetPrimaryAssetCheck Started ####');
        
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        Case objCaseForITBAsset = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCaseForITBAsset;
        
        ITB_Asset__c objAsset = Utils_TestMethods.createITBAsset(objCaseForITBAsset.id);
        List<ITB_Asset__c> lstAsset = new List<ITB_Asset__c>();
        objAsset.Address__c  = 'TestAddress';
        objAsset.AssetComments__c  = 'TestComments';
        objAsset.City__c  = 'TestCity';
        objAsset.CommercialReference__c  = 'BK500';
        objAsset.LegacyAccountID__c  = '1-242343';
        objAsset.LegacyAccountName__c  = 'TestAccount';
        objAsset.LegacyAssetId__c  = '1-22222';
        objAsset.LegacyAssetNumber__c  = '1-22222';
        objAsset.LegacyName__c  = 'IT_SB';
        objAsset.SerialNumber__c  = 'SFSEDFSDFDFDS';
        objAsset.StateProvince__c  = 'TESTSTATE';
        objAsset.TECH_AssetRefrenceLinkID__c  = '34545345345';
        objAsset.PrimaryFlag__c  = TRUE;
        objAsset.ZipCode__c ='342342';
        lstAsset.add(objAsset);
        
        
        ITB_Asset__c objSecondAsset = Utils_TestMethods.createITBAsset(objCaseForITBAsset.id);
        objSecondAsset.Address__c  = 'TestAddress';
        objSecondAsset.AssetComments__c  = 'TestComments';
        objSecondAsset.City__c  = 'TestCity';
        objSecondAsset.CommercialReference__c  = 'BK500';
        objSecondAsset.LegacyAccountID__c  = '1-242343';
        objSecondAsset.LegacyAccountName__c  = 'TestAccount';
        objSecondAsset.LegacyAssetId__c  = '1-22234';
        objSecondAsset.LegacyAssetNumber__c  = '1-2224535';
        objSecondAsset.LegacyName__c  = 'IT_SB';
        objSecondAsset.SerialNumber__c  = 'S345324FDFDS';
        objSecondAsset.StateProvince__c  = 'TESTSTATE';
        objSecondAsset.TECH_AssetRefrenceLinkID__c  = '234245345';
        objSecondAsset.ZipCode__c ='342342';
        lstAsset.add(objSecondAsset);
        Database.insert(lstAsset);
        
        System.debug('## objAsset.PrimaryFlag__c ## '  + objAsset.PrimaryFlag__c);
        System.debug('## objSecondAsset.PrimaryFlag__c ##' + objSecondAsset.PrimaryFlag__c);
        
        ITB_Asset__c objThirdAsset = Utils_TestMethods.createITBAsset(objCaseForITBAsset.id);
        objThirdAsset.Address__c  = 'TestAddress';
        objThirdAsset.AssetComments__c  = 'TestComments';
        objThirdAsset.City__c  = 'TestCity';
        objThirdAsset.CommercialReference__c  = 'BK500';
        objThirdAsset.LegacyAccountID__c  = '1-242343';
        objThirdAsset.LegacyAccountName__c  = 'TestAccount';
        objThirdAsset.LegacyAssetId__c  = '1-12264';
        objThirdAsset.LegacyAssetNumber__c  = '1-2298835';
        objThirdAsset.LegacyName__c  = 'IT_SB';
        objThirdAsset.SerialNumber__c  = 'S3453546FDFDS';
        objThirdAsset.StateProvince__c  = 'TESTSTATE';
        objThirdAsset.TECH_AssetRefrenceLinkID__c  = '234245345';
        objThirdAsset.ZipCode__c ='342342';
        
        Database.insert(objThirdAsset);
        
        objThirdAsset.PrimaryFlag__c  = TRUE;
        Database.update(objThirdAsset);
        
        
        List<ITB_Asset__c> lstAssetAfterUpdate = new List<ITB_Asset__c>([Select Id,LegacyAssetId__c,PrimaryFlag__c from ITB_Asset__c where Case__c =:objCaseForITBAsset.id ]);
        
        System.debug('@@lstAssetAfterUpdate@@:' + lstAssetAfterUpdate);
        System.debug('#### AP_ITB_Assets_PrimaryAssetCheck_Test.testITBAssetPrimaryAssetCheck Ends ####');
    }
}