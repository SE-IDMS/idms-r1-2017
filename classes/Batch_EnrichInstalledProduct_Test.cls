/*
    Author              : Hari
    Date Created        : 22-Oct-2014
    Description         : Test Class for Batch_EnrichInstalledProduct
*/

@isTest 
private class Batch_EnrichInstalledProduct_Test{

        static testMethod void classTest(){
            
             Account objAccount = Utils_TestMethods.createAccount();
            objAccount.RecordTypeid = Label.CLOCT13ACC08;
            insert objAccount;
            SVMXC__Site__c site1 = new SVMXC__Site__c();
            site1.Name = 'Test Location';
            site1.SVMXC__Street__c  = 'Test Street';
            site1.SVMXC__Account__c = objAccount .id;
            site1.PrimaryLocation__c = true;
            insert site1;
            
            Country__c country= Utils_TestMethods.createCountry(); 
            country.CountryCode__c= 'hkh';   
            insert country;   
            Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
            objContact.Country__c= country.id;
            insert objContact;
            Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
            objCase.SVMXC__Site__c = site1.id;
            insert objCase;
            
            Brand__c brand1_1 = new Brand__c();
    
            brand1_1.Name ='Brand 1-1';
            brand1_1.SDHBRANDID__c = 'Test_BrandSDHID';
            brand1_1.IsSchneiderBrand__c = true;
            insert brand1_1 ;
            
            DeviceType__c dt1_1 = new DeviceType__c();
            dt1_1.name = 'Device Type 1-1';
            dt1_1.SDHDEVICETYPEID__c = 'Test_DeviceTypeSDHID1-1';
            insert dt1_1 ;
            
            DeviceType__c dt1_2 = new DeviceType__c();
            dt1_2.name = 'Device Type 1-2';
            dt1_2.SDHDEVICETYPEID__c = 'Test_DeviceTypeSDHID1-2';
            insert dt1_2;
            
            DeviceTypesPerBrand__c dtperbrand1_1 = new DeviceTypesPerBrand__c();
            dtperbrand1_1.Brand__c = brand1_1.id;
            dtperbrand1_1.DeviceType__c =dt1_1.id;
            insert dtperbrand1_1;
            
            DeviceTypesPerBrand__c dtperbrand1_2 = new DeviceTypesPerBrand__c();
            dtperbrand1_2.Brand__c = brand1_1.id;
            dtperbrand1_2.DeviceType__c =dt1_2.id; 
            insert dtperbrand1_2 ;
            
            Category__c c1_1 = new Category__c();
            c1_1.Name =  'Range 1-1';
            c1_1.CategoryType__c = 'RANGE';
            c1_1.SDHCategoryID__c = 'Test_RangeSDHID1-1';
            
            insert c1_1;
            Category__c c1_2 = new Category__c();
            c1_2.Name =  'Range 1-2';
            c1_2.SDHCategoryID__c = 'Test_RangeSDHID1-2';
            c1_2.CategoryType__c = 'RANGE';
            insert c1_2 ;
            
            RangesPerDeviceAndBrand__c rpdandb1_1 = new RangesPerDeviceAndBrand__c();
            rpdandb1_1.Brand__c = brand1_1.id;
            rpdandb1_1.DeviceType__c = dt1_1.id;
            rpdandb1_1.Category__c = c1_1.id;
            insert rpdandb1_1;
            
            List<SVMXC__Installed_Product__c> ipobjlist = new List<SVMXC__Installed_Product__c>();
            
            for(integer i=0;i<10;i++){
            
                SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
                ip1.SVMXC__Company__c = objAccount.id;
                ip1.Name = 'Test Intalled Product '+i;
                ip1.SVMXC__Status__c= 'new';
                ip1.GoldenAssetId__c = 'GoledenAssertId'+i;
                ip1.BrandToCreate__c ='Test Brand';
                ip1.SVMXC__Site__c = site1.id;
                ip1.TECH_CreateFromWS__c = true;
                //ip1.RecordTypeId = Label.CLAPR14SRV09;
                //ip1.SchneiderCommercialReference__c = 'TestSechneiderCommercialReference'+i;
                //ip1.SVMXC__Serial_Lot_Number__c = 'TestSerialLotNumber'+i;
                ipobjlist.add(ip1);
            
            
            }
            for(integer i=10;i<20;i++){
            
                SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
                ip1.SVMXC__Company__c = objAccount.id;
                ip1.Name = 'Test Intalled Product '+i;
                ip1.SVMXC__Status__c= 'new';
                ip1.GoldenAssetId__c = 'GoledenAssertId'+i;
                ip1.BrandToCreate__c ='Test Brand';
                ip1.SVMXC__Site__c = site1.id;
                ip1.Brand2__c = brand1_1.id;
                ip1.DeviceType2__c = dt1_1.id;
                ip1.Category__c = c1_1.id;
                 ip1.TECH_CreateFromWS__c = true;
                
                //ip1.SchneiderCommercialReference__c = 'TestSechneiderCommercialReference'+i;
                //ip1.SVMXC__Serial_Lot_Number__c = 'TestSerialLotNumber'+i;
                ipobjlist.add(ip1);
            
            
            }
            insert ipobjlist ;
            
             id bid = Database.executeBatch(new   Batch_EnrichInstalledProduct());
            
           
            
        
        }

}