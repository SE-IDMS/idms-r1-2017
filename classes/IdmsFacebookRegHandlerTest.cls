/**
* This is test class IdmsFacebookRegHandler class.
**/

@isTest

private class IdmsFacebookRegHandlerTest{
    //This method test to create User with required fields for work user. 
    static testMethod void testCreateUser() { 
        IdmsFacebookRegHandler handler = new IdmsFacebookRegHandler();
        
        IDMS_Profile_Mapping__c  ProfileMap = new IDMS_Profile_Mapping__c();
        ProfileMap.Name = 'Home';
        ProfileMap.Profile_Name__c = 'SE - IDMS Home';
        insert ProfileMap;
        
        IDMS_Profile_Mapping__c  ProfileMapwork = new IDMS_Profile_Mapping__c();
        ProfileMapwork.Name = 'Work';
        ProfileMapwork.Profile_Name__c = 'SE - IDMS - Standard B2B User';
        Insert ProfileMapwork;
        
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                     'testFirst testLast', 'testuser@accenture.com', null, 'testuserlong', 'en_US', 'facebook',
                                                     null, new Map<String, String>{'language' => 'en_US', 'Profile_Name__c' => 'SE - IDMS Home'});
        
        Auth.UserData sampleData2 = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                      'testFirst testLast', 'testuser9@accenture.com', null, 'testuserlong', 'en_US', 'facebook',
                                                      null, new Map<String, String>{'language' => 'en_US', 'Profile_Name__c' => 'SE - IDMS - Standard B2B User'});
        
        
        handler.canCreateUser(sampleData);
        
        String appid = '0sp210000008ODD';
        String appwork = '989IUHY900';
        String context1 = 'Home';
        String context2 = 'Work';
        String registrationSource;
        String context;
        
        //Custom setting to retrieve an application Id.
        IdmsSocialUser__c cust_obj = new IdmsSocialUser__c();
        cust_obj.Name = 'IdmsKey';
        cust_obj.AppValue__c = appid;
        Insert cust_obj;
        
        //code for getting context and app details        
        IDMSApplicationMapping__c appMap = new IDMSApplicationMapping__c();
        appMap.Name = appid ;
        appMap.context__c = context1;
        insert appMap;
        
        handler.createUser(null, sampleData);
        
        delete cust_obj;
        delete appMap;
        
        IdmsSocialUser__c cust_obj1 = new IdmsSocialUser__c();
        cust_obj1.Name = 'IdmsKey';
        cust_obj1.AppValue__c = appid;
        Insert cust_obj1;
        
        IDMSApplicationMapping__c appMap2 = new IDMSApplicationMapping__c();
        appMap2.Name = appwork;
        appMap2.context__c = context2;
        insert appMap2;
        
        handler.createUser(null, sampleData2);
        
        
    }
    
    //This method test create user with required fields for home user. 
    static testMethod void testUpdateUser() {
        IdmsFacebookRegHandler handler = new IdmsFacebookRegHandler();
        
        ID profileId = [SELECT Id FROM Profile WHERE Name= 'SE - SRV Advanced User' limit 1].Id; 
        ID UserRoleID = [SELECT Id, Name FROM UserRole WHERE Name='CEO'].Id;
        
        User u = new User(firstName='testFirst',ProfileID=profileId , UserRoleId = UserRoleID , lastName='testLast', email ='testuser@accenture.com', userName = 'testuser@accenture.com'+Label.CLJUN16IDMS71, 
                          alias = 'testnewu', languagelocalekey = 'en_US',  localesidkey = 'en_US', emailEncodingKey = 'UTF-8', timeZoneSidKey = 'America/Los_Angeles');
        insert u;
        
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
                                                     'testFirst testLast', 'testuser@accenture.com', null, 'testuserlong', 'en_US', 'facebook',
                                                     null, new Map<String, String>{'language' => 'en_US'});
        handler.updateUser(u.Id, null, sampleData);
        
        User updatedUser = [SELECT userName, email, firstName, lastName, alias FROM user WHERE id=:u.id];
        System.assertEquals('testuser@accenture.com', updatedUser.email);
        System.assertEquals('testLast', updatedUser.lastName);
        System.assertEquals('testFirst', updatedUser.firstName);
        System.assertEquals('testnewu', updatedUser.alias);
        
    }    
}