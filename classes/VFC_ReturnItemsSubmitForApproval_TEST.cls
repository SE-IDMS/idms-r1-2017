@isTest
private class VFC_ReturnItemsSubmitForApproval_TEST {

        /*
        String commRef='xbtg5230';
        String prodFamily='HMI SCHNEIDER BRAND';
        String techGMR='02BF6D';
        String family='PERFORMANCE PANEL';
        */
    
        static String commRef='TEST_XBTGT5430';
        static String prodFamily='HMI SCHNEIDER BRAND';
        static String techGMR='02BF6DRG16N---7632';
        static String family='PERFORMANCE PANEL';


static testMethod void myUnitTest() {


    system.debug('Method1');
    Country__c CountryObj = Utils_TestMethods.createCountry();
    Insert CountryObj;
    
    User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
    Insert RRTestUser ;
    
    User TestUser1 = Utils_TestMethods.createStandardUser('RRuser1');
    Insert TestUser1 ;
    
    User TestUser2 = Utils_TestMethods.createStandardUser('RRuser2');
    Insert TestUser2 ;
    
    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=CountryObj.Id;
    insert accountObj;
    
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
   
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';   
    Insert CaseObj;

    
    ReturnItemsApprovalMatrix__c RIAM = new ReturnItemsApprovalMatrix__c(
                                 AccountCountry__c = CountryObj.Id,
                                 CommercialReference__c=commRef,
                                 ProductFamily__c=prodFamily,
                                 TECH_CustomerResolution__c = 'REP',                                                             
                                 Category__c = '4 - Post-Sales Tech Support', 
                                 TECH_GMRCode__c = techGMR,   
                                 Reason__c = 'Installation/ Setup',
                                 SubReason__c ='Hardware',
                                 RuleActive__c = True,
                                 ApproverLevel1__c= TestUser1.id,
                                 ApproverLevel2__c= TestUser2.id
                                 );
    Insert RIAM;

     BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg; 
    
    RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Repair defective product',
                                 CommercialReference__c=commRef,
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c=techGMR
                                 );
    Insert RPDT;
    
    ResolutionOptionAndCapabilities__c ROC = new ResolutionOptionAndCapabilities__c (                                             
                                             ProductCondition__c = 'Defective',
                                             CustomerResolution__c = 'REP',
                                             ProductDestinationCapability__c = 'Repair defective product'
                                             );
    Insert ROC;

    RMA__c RR = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
    Insert RR;
    
    RMA_Product__c RI = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name =commRef,
                        ProductFamily__c=prodFamily,
                        RMA__c=RR.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1',
                        WarrantyStatus__c = 'Standard warranty',
                        ApproverLevel1__c= TestUser1.id,
                        TECH_GMRCode__c=techGMR
                        );
    Insert RI;

      RMA__c RRNew = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
    Insert RRNew;
    
    RMA_Product__c RINew = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name =commRef,
                        ProductFamily__c=prodFamily,
                        RMA__c=RRNew.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1',
                        WarrantyStatus__c = 'Standard warranty',
                        ApproverLevel1__c= TestUser1.id,
                        TECH_GMRCode__c=techGMR,
                        ApprovalStatus__c=null //k:Added
                        );
    Insert RINew;

   List<RMA_Product__c> lstRIs = [Select id, Name, RMA__c from RMA_Product__c where RMA__c =: RR.id];  
   System.debug('### lstRIs ###'+ lstRIs);
   
    ApexPages.Standardcontroller SC = New ApexPages.StandardController(RR);
   // VFC_ReturnItemsSubmitForApproval RISubmitForApproval = new VFC_ReturnItemsSubmitForApproval(SC);
     
  /*  RISubmitForApproval.processDetails();
    RISubmitForApproval.doApprove();
    RISubmitForApproval.doCancel();
   */  
    RIAM.CustomerResolution__c = null;
    update RIAM;

    ApexPages.Standardcontroller SCNew = New ApexPages.StandardController(RRNew);
    VFC_ReturnItemsSubmitForApproval RISubmitForApproval_New = new VFC_ReturnItemsSubmitForApproval(SCNew);

    RISubmitForApproval_New.processDetails();
    RISubmitForApproval_New.doApprove();

}


// Test Method 5

static testMethod void myUnitTest5() {

      system.debug('Method5');

    Country__c CountryObj = Utils_TestMethods.createCountry();
    Insert CountryObj;
    
    User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
    Insert RRTestUser ;
    
    User TestUser1 = Utils_TestMethods.createStandardUser('RRuser1');
    Insert TestUser1 ;
    
    User TestUser2 = Utils_TestMethods.createStandardUser('RRuser2');
    Insert TestUser2 ;
    
    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=CountryObj.Id;
    insert accountObj;
    
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
   
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';   
    Insert CaseObj;

    ReturnItemsApprovalMatrix__c RIAM = new ReturnItemsApprovalMatrix__c(
                                 AccountCountry__c = CountryObj.Id,
                                 CommercialReference__c=commRef,
                                 ProductFamily__c=prodFamily,
                                 TECH_CustomerResolution__c = 'REP',                                                             
                                 //Category__c = '4 - Post-Sales Tech Support', 
                                 Category__c = null, 
                                 TECH_GMRCode__c = techGMR,   
                                 //Reason__c = 'Installation/ Setup',
                                 //SubReason__c ='Hardware',
                                  Reason__c = null,
                                 SubReason__c = null,
                                 RuleActive__c = True,
                                 ApproverLevel1__c= TestUser1.id,
                                 ApproverLevel2__c= TestUser2.id
                                 );
    Insert RIAM;

     ReturnItemsApprovalMatrix__c RIAM1 = new ReturnItemsApprovalMatrix__c(
                                 AccountCountry__c = CountryObj.Id,
                                 CommercialReference__c=commRef,
                                 ProductFamily__c=prodFamily,
                                 TECH_CustomerResolution__c = 'REP',                                                             
                                 Category__c = '4 - Post-Sales Tech Support', 
                                 //Category__c = null, 
                                 TECH_GMRCode__c = techGMR,   
                                 //Reason__c = 'Installation/ Setup',
                                 //SubReason__c ='Hardware',
                                  Reason__c = null,
                                 SubReason__c = null,
                                 RuleActive__c = True,
                                 ApproverLevel1__c= TestUser1.id,
                                 ApproverLevel2__c= TestUser2.id
                                 );
    Insert RIAM1;

     BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg; 
    
    RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Repair defective product',
                                 CommercialReference__c=commRef,
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c=techGMR
                                 );
    Insert RPDT;
    
    ResolutionOptionAndCapabilities__c ROC = new ResolutionOptionAndCapabilities__c (                                             
                                             ProductCondition__c = 'Defective',
                                             CustomerResolution__c = 'REP',
                                             ProductDestinationCapability__c = 'Repair defective product'
                                             );
    Insert ROC;

       RMA__c RR3 = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
    Insert RR3;
    
     RMA_Product__c RI3 = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name =commRef,
                        ProductFamily__c='XYZF',
                        RMA__c=RR3.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1',
                        TECH_GMRCode__c=techGMR,
                        WarrantyStatus__c = 'Standard warranty'
                        );
    Insert RI3;
    //======================

     RMA__c RR4 = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
    Insert RR4;
    
     RMA_Product__c RI4 = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name ='XYZ123',
                        ProductFamily__c='XYZF123',
                        RMA__c=RR4.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1',
                        TECH_GMRCode__c='000001',
                        WarrantyStatus__c = 'Standard warranty'
                        );
    Insert RI4;

    //=============================

    RMA__c RR5 = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
     //           TEX__c=tempTex.id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
    Insert RR5;
    
     RMA_Product__c RI5 = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name ='XYZ123',
                        ProductFamily__c='XYZF123',
                        RMA__c=RR5.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1',
                        TECH_GMRCode__c='000001',
                        WarrantyStatus__c = 'Standard warranty',
                        ApprovalStatus__c = '',
                        ApproverLevel1__c = TestUser1.id
                        );
    Insert RI5;

    //==============================================

      RMA_Product__c RI6 = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name ='XYZ123',
                        ProductFamily__c='XYZF123',
                        RMA__c=RR5.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1',
                        TECH_GMRCode__c='000001',
                        ApprovalStatus__c = '',
                        WarrantyStatus__c = 'Out of warranty'
                        );
    Insert RI6;

  
    ApexPages.Standardcontroller SC4 = New ApexPages.StandardController(RR3);
  VFC_ReturnItemsSubmitForApproval RISubmitForApproval4 = new VFC_ReturnItemsSubmitForApproval(SC4);
   
  RISubmitForApproval4.processDetails();
   RISubmitForApproval4.doApprove();
/*   
   ApexPages.Standardcontroller SC5 = New ApexPages.StandardController(RR4);
  VFC_ReturnItemsSubmitForApproval RISubmitForApproval5 = new VFC_ReturnItemsSubmitForApproval(SC5);
   
  // RISubmitForApproval5.processDetails();
   RISubmitForApproval5.doApprove();
  
  ApexPages.Standardcontroller SC6 = New ApexPages.StandardController(RR5);
  VFC_ReturnItemsSubmitForApproval RISubmitForApproval6 = new VFC_ReturnItemsSubmitForApproval(SC6);
   
  RISubmitForApproval6.processDetails();
  
  
  RI6.ApprovalStatus__c='Rejected';
  Update RI6;
  */
}

// Test Method 6

static testMethod void myUnitTest6() {

      system.debug('Method6');

    Country__c CountryObj = Utils_TestMethods.createCountry();
    Insert CountryObj;
    
    User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
    Insert RRTestUser ;
    
    User TestUser1 = Utils_TestMethods.createStandardUser('RRuser1');
    Insert TestUser1 ;
    
    User TestUser2 = Utils_TestMethods.createStandardUser('RRuser2');
    Insert TestUser2 ;
    
    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=CountryObj.Id;
    insert accountObj;
    
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
   
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';   
    Insert CaseObj;

    ReturnItemsApprovalMatrix__c RIAM = new ReturnItemsApprovalMatrix__c(
                                 AccountCountry__c = CountryObj.Id,
                                 CommercialReference__c=commRef,
                                 ProductFamily__c=prodFamily,
                                 TECH_CustomerResolution__c = 'REP',                                                             
                                 //Category__c = '4 - Post-Sales Tech Support', 
                                 Category__c = null, 
                                 TECH_GMRCode__c = techGMR,   
                                 //Reason__c = 'Installation/ Setup',
                                 //SubReason__c ='Hardware',
                                  Reason__c = null,
                                 SubReason__c = null,
                                 RuleActive__c = True,
                                 ApproverLevel1__c= TestUser1.id,
                                 ApproverLevel2__c= TestUser2.id
                                 );
    Insert RIAM;

     BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg; 
    
    RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Repair defective product',
                                 CommercialReference__c=commRef,
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c=techGMR
                                 );
    Insert RPDT;
    
    ResolutionOptionAndCapabilities__c ROC = new ResolutionOptionAndCapabilities__c (                                             
                                             ProductCondition__c = 'Defective',
                                             CustomerResolution__c = 'REP',
                                             ProductDestinationCapability__c = 'Repair defective product'
                                             );
    Insert ROC;

       RMA__c RR3 = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
    Insert RR3;
    
     RMA_Product__c RI3 = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name =commRef,
                        ProductFamily__c='XYZF',
                        RMA__c=RR3.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1',
                        TECH_GMRCode__c=techGMR,
                        WarrantyStatus__c = 'Standard warranty'
                        );
    Insert RI3;
   
  
    ApexPages.Standardcontroller SC4 = New ApexPages.StandardController(RR3);
  VFC_ReturnItemsSubmitForApproval RISubmitForApproval4 = new VFC_ReturnItemsSubmitForApproval(SC4);
   
  RISubmitForApproval4.processDetails();
   RISubmitForApproval4.doApprove();

}

// Test Method 6

static testMethod void myUnitTest7() {

      system.debug('Method7');

    Country__c CountryObj = Utils_TestMethods.createCountry();
    Insert CountryObj;
    
    User RRTestUser = Utils_TestMethods.createStandardUser('RRuser');
    Insert RRTestUser ;
    
    User TestUser1 = Utils_TestMethods.createStandardUser('RRuser1');
    Insert TestUser1 ;
    
    User TestUser2 = Utils_TestMethods.createStandardUser('RRuser2');
    Insert TestUser2 ;
    
    Account accountObj = Utils_TestMethods.createAccount();
    accountObj.Country__c=CountryObj.Id;
    insert accountObj;
    
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
   
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';   
    Insert CaseObj;

    ReturnItemsApprovalMatrix__c RIAM = new ReturnItemsApprovalMatrix__c(
                                 AccountCountry__c = CountryObj.Id,
                                 CommercialReference__c=commRef,
                                 ProductFamily__c=prodFamily,
                                 TECH_CustomerResolution__c = 'REP',                                                             
                                 //Category__c = '4 - Post-Sales Tech Support', 
                                 Category__c = null, 
                                 TECH_GMRCode__c = techGMR,   
                                 //Reason__c = 'Installation/ Setup',
                                 //SubReason__c ='Hardware',
                                  Reason__c = null,
                                 SubReason__c = null,
                                 RuleActive__c = True,
                                 ApproverLevel1__c= TestUser1.id,
                                 ApproverLevel2__c= TestUser2.id
                                 );
    Insert RIAM;

     BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg; 
    
    RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Repair defective product',
                                 CommercialReference__c=commRef,
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c=techGMR
                                 );
    Insert RPDT;
    
    ResolutionOptionAndCapabilities__c ROC = new ResolutionOptionAndCapabilities__c (                                             
                                             ProductCondition__c = 'Defective',
                                             CustomerResolution__c = 'REP',
                                             ProductDestinationCapability__c = 'Repair defective product'
                                             );
    Insert ROC;

       RMA__c RR3 = new RMA__c (
                Case__c= CaseObj.Id,
                Approver__c = RRTestUser.Id,
                ProductCondition__c='Defective',
                CustomerResolution__c='REP' ,
                AccountStreet__c = 'Test street'
                
                );
    Insert RR3;
    
     RMA_Product__c RI3 = new RMA_Product__c(
                        ProductCondition__c = 'Defective',
                        CustomerResolution__c = 'REP',
                        Quantity__c=1,
                        Name =commRef,
                        ProductFamily__c='XYZF',
                        RMA__c=RR3.Id,
                        CustomerRepairPONumber__c = '12345',
                        AccountToBeInvoicedForTheRepair__c = '1',
                        TECH_GMRCode__c='12345678',
                        WarrantyStatus__c = 'Standard warranty'
                        );
    Insert RI3;
    
    
   // sukumar - 20dec2013
   List<RMA_Product__c> lstRIs = [Select id, RMA__c,TECH_CaseID__c from RMA_Product__c where id=: RI3.id];   
   AP_RI_FieldUpdate.updateCaseOwner(lstRIs);
  
    ApexPages.Standardcontroller SC4 = New ApexPages.StandardController(RR3);
  VFC_ReturnItemsSubmitForApproval RISubmitForApproval4 = new VFC_ReturnItemsSubmitForApproval(SC4);
   
  RISubmitForApproval4.processDetails();
   RISubmitForApproval4.doApprove();

}



}