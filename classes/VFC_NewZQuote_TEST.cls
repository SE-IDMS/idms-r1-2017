@isTest
private class VFC_NewZQuote_TEST {
   static testMethod void testNewZQuote() {
      test.startTest();
      
      PageReference pageRef = Page.VFP_NewZQuote;
      test.setCurrentPage(pageRef);
      
      zqu__Quote__c zquote = new zqu__Quote__c();
      ApexPages.StandardController sc = new ApexPages.standardController(zquote);
      
      VFC_NewZQuote ctrl = new VFC_NewZQuote(sc);
      ctrl.redirectToNewZQuote();
      
      test.stopTest();
   }

   static testMethod void testNewZQuoteFromAccount() {
      test.startTest();
      
      PageReference pageRef = Page.VFP_NewZQuote;
      Account testAccount = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345');
      insert testAccount;
      
      pageRef.getParameters().put(Label.CLJUN16FIO001, testAccount.Id);
      test.setCurrentPage(pageRef);
      
      zqu__Quote__c zquote = new zqu__Quote__c();
      ApexPages.StandardController sc = new ApexPages.standardController(zquote);
      
      VFC_NewZQuote ctrl = new VFC_NewZQuote(sc);
      ctrl.redirectToNewZQuote();
      
      test.stopTest();
   }
}