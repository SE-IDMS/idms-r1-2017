public with sharing class AP_CassiniPOCCaseAssingment {
    public static List<TeamProductJunction__c>lstTeamProductJunction;
    public static List<Package__c> lstPackage ;
    public AP_CassiniPOCCaseAssingment() {
        
    }
    public static String assingCase(String strProductFamily){
        lstTeamProductJunction = new List<TeamProductJunction__c>();
        lstPackage  = new List<Package__c>();
        List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){
            List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){
                List<Contract> lstContracts=[SELECT Id,Points__c,EndDate FROM Contract Where Id=:lstCVCPs[0].Contract__c and WebAccess__c =: 'Cassini' ORDER BY LastModifiedDate DESC LIMIT 1];
                if(!lstContracts.isEmpty()){
                     lstPackage=[SELECT PackageTemplate__c, 
                                PackageTemplate__r.PortalAccess__c, 
                                PackageTemplate__r.PointOfContact__c,   
                                PackageTemplate__r.PointOfContact__r.Priority1Routing__c, 
                                PackageTemplate__r.PointOfContact__r.DefaultRoutingTeam__c,
                                PackageTemplate__r.PointOfContact__r.Organization__c   
                                FROM Package__c WHERE Contract__c = :lstContracts[0].Id 
                                AND PackageTemplate__r.PortalAccess__c = TRUE
                                
                                AND PackageTemplate__r.PointOfContact__r.ChannelType__c ='Portal' 
                                ORDER BY LastModifiedDate DESC LIMIT 1];
                    
                }
            }
        }
        // Check for Null pointer Exception - Vimal K  Revew Comment
        if(!lstPackage.isEmpty() &&lstPackage[0].PackageTemplate__c!=null &&
                                     lstPackage[0].PackageTemplate__r.PointOfContact__c!=null &&
                                     lstPackage[0].PackageTemplate__r.PointOfContact__r.Organization__c!=null){
                                     System.debug('1:'+lstPackage[0].PackageTemplate__r.PointOfContact__r.Organization__c);
                                     System.debug('1:'+lstPackage[0].PackageTemplate__r.PointOfContact__r.Organization__c);
            if(lstPackage[0].PackageTemplate__r.PointOfContact__r.Priority1Routing__c == 'Product Family Support Team') {
                lstTeamProductJunction =[select id, 
                    ProductFamily__c,
                    DefaultSupportedProductFamily__c, 
                    CCTeam__c
                    from TeamProductJunction__c  
                    where 
                    CCTeam__r.ManagingOrganization__c =: 
                    lstPackage[0].PackageTemplate__r.PointOfContact__r.Organization__c 
                    AND PM0_ProductFamily__c =: strProductFamily ];
                   System.debug('lstTeamProductJunction'+lstTeamProductJunction); 
                   System.debug(' strProductFamily'+strProductFamily);
                if(!lstTeamProductJunction.isEmpty()){
                    if(lstTeamProductJunction.size() == 1){ 
                        return lstTeamProductJunction[0].CCTeam__c ;
                    }
                    else {
                        id strCCId =null;
                        for(TeamProductJunction__c t : lstTeamProductJunction){
                            if(t.DefaultSupportedProductFamily__c){
                                strCCId = t.CCTeam__c;
                                break;
                            }
                        }
                        if(strCCId!= null )
                            return strCCId;
                    }
                }  
                return lstPackage[0].PackageTemplate__r.PointOfContact__r.DefaultRoutingTeam__c ;
            }
            else{
                return lstPackage[0].PackageTemplate__r.PointOfContact__r.DefaultRoutingTeam__c;
            }
        }
        else {
            return null;
        }                                  
    }
}