@isTest
public with sharing class Utils_WSDummyTestData{
    public static WS_InTouch_WONService.createWONDataBean createDummySRForWONService(){
        WS_InTouch_WONService.createWONDataBean objDummyinTouchActivitiesResult = new WS_InTouch_WONService.createWONDataBean();
        
        objDummyinTouchActivitiesResult.displayMessage='Operation Successful';
        objDummyinTouchActivitiesResult.returnCode ='0';
                           
        objDummyinTouchActivitiesResult.caseWONActivityActivityBookmark = '';    
        objDummyinTouchActivitiesResult.caseWONActivityActualEnd = Datetime.valueOf('2013-04-24 00:00:00');
        objDummyinTouchActivitiesResult.caseWONActivityActualStart = Datetime.valueOf('2013-01-24 00:00:00');
        objDummyinTouchActivitiesResult.caseWONActivityAttachments = 'Yes';
        objDummyinTouchActivitiesResult.caseWONActivityComments = 'This is a Test Case WON Activity Comments';
        objDummyinTouchActivitiesResult.caseWONActivityDescription = 'This is a Test Case WON Activity Description';
        objDummyinTouchActivitiesResult.caseWONActivityNumber = '1-1XR0N1C';
        objDummyinTouchActivitiesResult.caseWONActivityOwner = 'BFOCCCADMIN';
        objDummyinTouchActivitiesResult.caseWONActivityPlannedEnd = Datetime.valueOf('2013-02-24 00:00:00');
        objDummyinTouchActivitiesResult.caseWONActivityPlannedStart = Datetime.valueOf('2013-01-01 00:00:00');
        objDummyinTouchActivitiesResult.caseWONActivityRMANumber = '03444384';
        objDummyinTouchActivitiesResult.caseWONActivityServiceRegion = 'Rhone-Alpes';
        objDummyinTouchActivitiesResult.caseWONActivityStatus = 'Done';
        objDummyinTouchActivitiesResult.caseWONActivityType = 'Test';
        return objDummyinTouchActivitiesResult;
    }
    
    public static WS_InTouch_AssetService.assetReponseDataBean createDummyAssetForAssetService(){
        WS_InTouch_AssetService.assetReponseDataBean objDummyinTouchAssetResult = new WS_InTouch_AssetService.assetReponseDataBean();
        
        objDummyinTouchAssetResult.displayMessage='Operation Successful';
        objDummyinTouchAssetResult.returnCode ='0';
        
        WS_InTouch_AssetService.assetDataBean objDummyAsset                     = new WS_InTouch_AssetService.assetDataBean();
        WS_InTouch_AssetService.warrantyDataBean objDummyWarranty               = new WS_InTouch_AssetService.warrantyDataBean();
        WS_InTouch_AssetService.entitlementDataBean objDummyEntitlement         = new WS_InTouch_AssetService.entitlementDataBean();
                
        List<WS_InTouch_AssetService.assetDataBean>lstDummyAsset                = new List<WS_InTouch_AssetService.assetDataBean>();
        List<WS_InTouch_AssetService.warrantyDataBean>lstDummyWarranty          = new List<WS_InTouch_AssetService.warrantyDataBean>();
        List<WS_InTouch_AssetService.entitlementDataBean>lstDummyEntitlement    = new List<WS_InTouch_AssetService.entitlementDataBean>();
        
        
        WS_InTouch_AssetService.AssetList_element objAssetElement               = new WS_InTouch_AssetService.AssetList_element();
        WS_InTouch_AssetService.Warranties_element objWarrantyElement           = new WS_InTouch_AssetService.Warranties_element();
        WS_InTouch_AssetService.Entitlements_element objEntitlementsElement     = new WS_InTouch_AssetService.Entitlements_element();
        
        
        objDummyWarranty.daysLeft   =   214;
        objDummyWarranty.endDate    =   Date.valueOf('2013-10-12 00:00:00');
        objDummyWarranty.legacyWarrantyId   =   '1-1XQRJMG';
        objDummyWarranty.startDate  =   Date.valueOf('2011-10-14 00:00:00');
        objDummyWarranty.warrantyName   =   'Standard Single Phase Two-Year Factory Warranty';
        objDummyWarranty.warrantyType   =   'Warranty';
        
        lstDummyWarranty.add(objDummyWarranty);
        objWarrantyElement.Warranty=lstDummyWarranty;
        
        objDummyEntitlement.agreementNumber =   '1-2350482982';
        objDummyEntitlement.endDate         =   Date.valueOf('2013-10-12 00:00:00');
        objDummyEntitlement.entitlementName =   'Start-Up Service 5x8 for ISX Install Only - 1';
        objDummyEntitlement.entitlementType = 'Installation Services';
        objDummyEntitlement.legacyEntitlementId = ' 1-12VG3JP';
        objDummyEntitlement.relatedContactFirstName =   'JOHN';
        objDummyEntitlement.relatedContactLastName  =   'ADAMS';
        objDummyEntitlement.relatedContactMobilePhone= '2109255260';
        objDummyEntitlement.relatedContactWorkPhone='2109255259';
        objDummyEntitlement.renewalStatus   =   'New';
        objDummyEntitlement.serviceHours    =   '5 x 8';
        objDummyEntitlement.startDate   =   Date.valueOf('2011-10-14 00:00:00');
        
        lstDummyEntitlement.add(objDummyEntitlement);
        objEntitlementsElement.Entitlement=lstDummyEntitlement;
        
        
        objDummyAsset.assetBookmark='1-H8HJP2';
        objDummyAsset.assetComments='Warranty not applied as warranty template based on Install Date was not found - 02/17/2013 10:16:45.';
        objDummyAsset.city='Warwick';
        objDummyAsset.countryCode='US';
        objDummyAsset.installedDate = DateTime.valueOf('2013-10-12 00:00:00');
        objDummyAsset.legacyAccountId='1-DU73-16002';
        objDummyAsset.legacyAccountName='NEWEDGE USA';
        objDummyAsset.legacyAssetNumber='1-1042180598';
        objDummyAsset.legacyName='ITB_SB';
        objDummyAsset.MACAddress='MACTEST';
        objDummyAsset.manufacturedDate=Date.valueOf('2013-10-12 00:00:00');
        objDummyAsset.PONumber='TESTPONUM';
        objDummyAsset.primaryFlag=false;
        objDummyAsset.purchasedDate=Date.valueOf('2013-10-12 00:00:00');
        objDummyAsset.registeredDate=Date.valueOf('2013-10-12 00:00:00');
        objDummyAsset.relatedProductCommercialReference='AP9470';
        objDummyAsset.SEAccountId='TESTSEACC';
        objDummyAsset.salesOrderNumber='TESTSONUMBER';
        objDummyAsset.serialNumber='fa0831105110';
        objDummyAsset.stateProvince='Rhode Island';
        objDummyAsset.street='550 WEST JACKSON 3rd fl';
        objDummyAsset.zipcode='02886';
        
        objDummyAsset.Warranties=objWarrantyElement;
        objDummyAsset.Entitlements=objEntitlementsElement;
        
        lstDummyAsset.add(objDummyAsset);
        objAssetElement.Asset = lstDummyAsset;
        objDummyinTouchAssetResult.AssetList = objAssetElement;
        return objDummyinTouchAssetResult;
    }
    public static string createDummyJSONString(){
        String strJSONString;
        
        WS04_GMR.labelValue businessLine=new WS04_GMR.labelValue();
        businessLine.label='Test Business Line';
        businessLine.value='11';
    
        WS04_GMR.labelValue family=new WS04_GMR.labelValue();
        family.label='Test Family';
        family.value='44';
        
        WS04_GMR.labelValue productGroup=new WS04_GMR.labelValue();
        productGroup.label='Test Product Group';
        productGroup.value='55';
        
        WS04_GMR.labelValue productLine=new WS04_GMR.labelValue();
        productLine.label='Test Product Line';
        productLine.value='22';
        
        WS04_GMR.labelValue productSuccession=new WS04_GMR.labelValue();
        productSuccession.label = 'Test Product Succession';
        productSuccession.value ='66';
        
        WS04_GMR.labelValue strategicProductFamily=new WS04_GMR.labelValue();
        strategicProductFamily.label='Test Product Family';
        strategicProductFamily.value='33';
        
        WS04_GMR.labelValue subFamily=new WS04_GMR.labelValue();
        subFamily.label='Test Sub Family';
        subFamily.value='77';
        
        WS04_GMR.gmrDataBean dummyGMRDataBean= new WS04_GMR.gmrDataBean();
        dummyGMRDataBean.businessLine=businessLine;
        dummyGMRDataBean.family=family;
        dummyGMRDataBean.productGroup=productGroup;
        dummyGMRDataBean.productLine=productLine;
        dummyGMRDataBean.productSuccession=productSuccession;
        dummyGMRDataBean.strategicProductFamily=strategicProductFamily;
        dummyGMRDataBean.subFamily=subFamily;
        dummyGMRDataBean.commercialReference='Test Commercial Reference';
        dummyGMRDataBean.description='Test Description';
        strJSONString=JSON.serialize(dummyGMRDataBean);
        return strJSONString;
    }
}