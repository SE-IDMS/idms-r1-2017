/**
16-Sep-2013
Shruti Karn  
PRM Oct13 Release    
Initial Creation: test class for VFC_NewPartnerProgram
 */
@isTest
private class VFC_NewPartnerProgram_TEST {
  
    
     static testMethod void myUnitTest() {
          PartnerPRogram__c program = new PartnerProgram__c();
          
          ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(program);
          VFC_NewPartnerProgram myPageCon1 = new VFC_NewPartnerProgram(sdtCon1);
          PageReference pageRefWithretURL = Page.VFP_NewPartnerProgram;
          pageRefWithretURL.getParameters().put(Label.CL00330, '/home/home.jsp');
          Test.setCurrentPage(pageRefWithretURL);
          myPageCon1.redirectToNewPartnerProgram();
     }
}