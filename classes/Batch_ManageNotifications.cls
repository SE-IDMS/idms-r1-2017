global class Batch_ManageNotifications implements Database.Batchable<sObject>,Schedulable,Database.Stateful{
    public String query;
    public static String CRON_EXP = '0 0 19 * * ? *'; // CRON JOB expression to run the batch job at 10PM everyday
    public Set<String> memberCountrySet = new Set<String>();
    public Map<String,String> memberCountryMap = new Map<String,String>();
    public List<SetupAlerts__c> SetupAlertsLst = new List<SetupAlerts__c>();
    public List<FieloEE__Member__c> ChannelSetupAlertsLst = new List<FieloEE__Member__c>();
    public List<FieloEE__MemberSegment__c> SegmentSetupAlertsLst = new List<FieloEE__MemberSegment__c>();
    global Map<String,Id> channelMap = new Map<String,Id>();
    public Set<String> channelAF = new Set<String>();
    public Set<String> segmentOnlyAlerts = new Set<String>();
    public Set<String> channelOnlyAlert = new Set<String>();
    public List<ChannelSystemAlert__c> channelAlerts = new List<ChannelSystemAlert__c>();
    public List<SegmentSystemAlert__c> segmentAlerts = new List<SegmentSystemAlert__c>();
    public Set<String> alertsANDLst = new Set<String>();
    public Map<String,String> segmentMap = new Map<String,String>();
    public Set<String> segmentAlertLst = new Set<String>();
    public Set<String> memberIdLst = new Set<String>();
    public Set<Id> systemAlertIds = new Set<id>();
    
    global Batch_ManageNotifications(){
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        List<RecordType> recordTypeLst = new List<RecordType>([SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Notification' LIMIT 1]);
        CS_PRM_ApexJobSettings__c mc1 = CS_PRM_ApexJobSettings__c.getValues('BatchProcessNotifications');
        DateTime dateTimeFormat = mc1.LastRun__c;
        Id recTypeId = recordTypeLst[0].Id;
        if(query == null){
           query = 'SELECT Id, Name, Active__c, Content__c, Display__c, ChannelAlertCount__c, SegmentAlertsCount__c, FromDate__c, PRMCountryClusterSetup__r.Country__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry1__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry2__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry3__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry4__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry5__r.CountryCode__c, Status__c, ToDate__c,(SELECT Id, Name, CountryChannels__c, CountryChannels__r.Active__c, CountryChannels__r.ChannelCode__c, CountryChannels__r.PRMCountry__c,CountryChannels__r.PRMCountry__r.CountryCode__c, CountryChannels__r.SubChannelCode__c, CountryChannels__r.Country__r.CountryCode__c, SystemAlert__c FROM Channel_System_Alerts__r),(SELECT Id, Name, FieloPRM_Segment__c, FieloPRM_Segment__r.F_PRM_Country__r.CountryCode__c, SystemAlert__c FROM Segment_System_Alerts__r) FROM SetupAlerts__c WHERE RecordtypeId = \''+recTypeId+ '\' AND Status__c = \'Published\' AND Processed__c = false';
           setupAlertsLst = Database.query(query);
        }
        else{
            setupAlertsLst = Database.query(query);
        }
            //CS_PRM_ApexJobSettings__c mc = CS_PRM_ApexJobSettings__c.getValues('BatchProcessNotifications');
            if(setupAlertsLst.size() > 0 && !setupAlertsLst.isEmpty()){
            for(SetupAlerts__c s : setupAlertsLst){
                if(s.SegmentAlertsCount__c > 0 && s.ChannelAlertCount__c > 0)
                    alertsANDLst.add(s.Id);
                    else if(s.SegmentAlertsCount__c > 0 && s.ChannelAlertCount__c == 0)
                        segmentOnlyAlerts.add(s.Id);
                    else
                        channelOnlyAlert.add(s.Id);
                    
                    channelAlerts.addAll(s.Channel_System_Alerts__r);
                    segmentAlerts.addAll(s.Segment_System_Alerts__r);
                    System.debug('>>>>channelAlerts:'+channelAlerts+'>>>>segmentAlerts:'+segmentAlerts);
                    systemAlertIds.add(s.id);
                    
                    if(string.isNotBlank(s.PRMCountryClusterSetup__r.Country__r.CountryCode__c))
                        memberCountrySet.add(s.PRMCountryClusterSetup__r.Country__r.CountryCode__c);
                    if(string.isNotBlank(s.PRMCountryClusterSetup__r.MemberCountry1__r.CountryCode__c)){
                        memberCountrySet.add(s.PRMCountryClusterSetup__r.MemberCountry1__r.CountryCode__c);
                        memberCountryMap.put(s.PRMCountryClusterSetup__r.MemberCountry1__r.CountryCode__c,s.PRMCountryClusterSetup__r.Country__r.CountryCode__c);
                    }
                    if(string.isNotBlank(s.PRMCountryClusterSetup__r.MemberCountry2__r.CountryCode__c)){
                        memberCountrySet.add(s.PRMCountryClusterSetup__r.MemberCountry2__r.CountryCode__c);
                        memberCountryMap.put(s.PRMCountryClusterSetup__r.MemberCountry2__r.CountryCode__c,s.PRMCountryClusterSetup__r.Country__r.CountryCode__c);
                    }
                    if(string.isNotBlank(s.PRMCountryClusterSetup__r.MemberCountry3__r.CountryCode__c)){
                        memberCountrySet.add(s.PRMCountryClusterSetup__r.MemberCountry3__r.CountryCode__c);
                        memberCountryMap.put(s.PRMCountryClusterSetup__r.MemberCountry3__r.CountryCode__c,s.PRMCountryClusterSetup__r.Country__r.CountryCode__c);
                    }
                    if(string.isNotBlank(s.PRMCountryClusterSetup__r.MemberCountry4__r.CountryCode__c)){
                        memberCountrySet.add(s.PRMCountryClusterSetup__r.MemberCountry4__r.CountryCode__c);
                        memberCountryMap.put(s.PRMCountryClusterSetup__r.MemberCountry4__r.CountryCode__c,s.PRMCountryClusterSetup__r.Country__r.CountryCode__c);
                    }          
                    if(string.isNotBlank(s.PRMCountryClusterSetup__r.MemberCountry5__r.CountryCode__c)){
                        memberCountrySet.add(s.PRMCountryClusterSetup__r.MemberCountry5__r.CountryCode__c);
                        memberCountryMap.put(s.PRMCountryClusterSetup__r.MemberCountry5__r.CountryCode__c,s.PRMCountryClusterSetup__r.Country__r.CountryCode__c);
                    }
                }
            }
            
            for(ChannelSystemAlert__c c: channelAlerts){
                channelAF.add(c.CountryChannels__r.SubChannelCode__c);
                channelMap.put(c.CountryChannels__r.PRMCountry__r.CountryCode__c+c.CountryChannels__r.SubChannelCode__c,c.SystemAlert__c);
            }
            
            for(SegmentSystemAlert__c s : segmentAlerts){
                segmentMap.put(s.FieloPRM_Segment__r.F_PRM_Country__r.CountryCode__c+s.FieloPRM_Segment__c,s.SystemAlert__c);
                segmentAlertLst.add(s.FieloPRM_Segment__c);
            }
            System.debug('>>>>memberCountryMap:'+memberCountryMap+'>>>>memberCountrySet:'+memberCountrySet+'>>>>ChannelAF:'+channelAf+'>>>>channelMap:'+channelMap+'>>>>segmentMap:'+segmentMap+'>>>>segmentAlertsLst:'+segmentAlertLst);
            if(alertsANDLst.size() > 0 && !alertsANDLst.isEmpty()){
                setupAlertsLst.clear();
                ChannelSetupAlertsLst = [SELECT Id, F_PRM_Channels__c, F_AreaOfFocus__c, F_BusinessType__c, F_Country__c, F_PRM_PrimaryChannel__c,F_PRM_CountryCode__c, F_Country__r.CountryCode__c, FieloEE__User__c, FieloEE__User__r.ContactId FROM FieloEE__Member__c WHERE F_PRM_PrimaryChannel__c = :channelAF AND F_PRM_CountryCode__c = :memberCountrySet AND FieloEE__User__r.isActive = true];
                SegmentSetupAlertsLst = [SELECT Id, FieloEE__Member2__c, FieloEE__Member2__r.F_PRM_CountryCode__c, FieloEE__Member2__r.F_Country__r.CountryCode__c, FieloEE__Member2__r.FieloEE__User__c, FieloEE__Member2__r.FieloEE__User__r.ContactId, FieloEE__Segment2__c FROM FieloEE__MemberSegment__c WHERE FieloEE__Segment2__r.F_PRM_Country__r.CountryCode__c in :memberCountrySet AND FieloEE__Segment2__c = :segmentAlertLst];
                query = 'SELECT Id, Name, Active__c, Content__c, Display__c, ChannelAlertCount__c, SegmentAlertsCount__c, FromDate__c, PRMCountryClusterSetup__r.Country__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry1__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry2__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry3__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry4__r.CountryCode__c,PRMCountryClusterSetup__r.MemberCountry5__r.CountryCode__c, Status__c, ToDate__c,(SELECT Id, Name, CountryChannels__c, CountryChannels__r.Active__c, CountryChannels__r.ChannelCode__c, CountryChannels__r.PRMCountry__c,CountryChannels__r.PRMCountry__r.CountryCode__c, CountryChannels__r.SubChannelCode__c, CountryChannels__r.Country__r.CountryCode__c, SystemAlert__c FROM Channel_System_Alerts__r),(SELECT Id, Name, FieloPRM_Segment__c, FieloPRM_Segment__r.F_PRM_Country__r.CountryCode__c, SystemAlert__c FROM Segment_System_Alerts__r) FROM SetupAlerts__c WHERE RecordtypeId = \''+recordTypeLst[0].Id + '\' AND Status__c = \'Published\' AND Processed__c = false';
                System.debug('>><<>><<ChannelSetupAlertsLst:'+ChannelSetupAlertsLst); 
            }
            else if(channelAlerts.size() > 0 && !channelAlerts.isEmpty())
                query = 'SELECT Id, F_PRM_Channels__c, F_AreaOfFocus__c, F_BusinessType__c, F_Country__c, F_PRM_PrimaryChannel__c,F_PRM_CountryCode__c, F_Country__r.CountryCode__c, FieloEE__User__c, FieloEE__User__r.ContactId FROM FieloEE__Member__c WHERE F_PRM_PrimaryChannel__c = :channelAF AND F_PRM_CountryCode__c = :memberCountrySet AND FieloEE__User__r.isActive = true';
            else
                query = 'SELECT Id, FieloEE__Member2__c, FieloEE__Member2__r.F_PRM_CountryCode__c, FieloEE__Member2__r.F_Country__r.CountryCode__c, FieloEE__Member2__r.FieloEE__User__c, FieloEE__Member2__r.FieloEE__User__r.ContactId, FieloEE__Segment2__c FROM FieloEE__MemberSegment__c WHERE FieloEE__Segment2__r.F_PRM_Country__r.CountryCode__c in :memberCountrySet AND FieloEE__Segment2__c = :segmentAlertLst';
        
        System.debug('>>>>query:'+query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        //List<SetupAlerts__c> SetupAlertsLst = new List<SetupAlerts__c>();     
        System.debug('>><<>><<scope:'+scope);
        for(sObject s1 : scope){
            
            if(string.valueOf(s1.getsObjectType()) == 'SetupAlerts__c'){
                SetupAlerts__c s = (SetupAlerts__c)(s1);
                SetupAlertsLst.add(s);
            } 
            else if(string.valueOf(s1.getsObjectType()) == 'FieloEE__Member__c'){
                FieloEE__Member__c s2 = (FieloEE__Member__c)(s1);
                ChannelSetupAlertsLst.add(s2);
            }
            else{
                FieloEE__MemberSegment__c s3 = (FieloEE__MemberSegment__c)(s1);
                SegmentSetupAlertsLst.add(s3); 
            }
        }
        
        
        System.debug('>><<>><<ChannelSetupAlertsLst1:'+ChannelSetupAlertsLst);
       // if(SetupAlertsLst.size() > 0 && !SetupAlertsLst.isEmpty()){
            AP_ManageNotificationHandler.insertNotifications(SetupAlertsLst,memberCountrySet,memberCountryMap,ChannelSetupAlertsLst,SegmentSetupAlertsLst,alertsANDLst,channelMap,segmentMap);
            
            CS_PRM_ApexJobSettings__c ms = CS_PRM_ApexJobSettings__c.getValues('BatchProcessNotifications');
            System.debug('>>>>'+ms);
            ms.LastRun__c = System.Now();
            update ms;
            
            for(SetupAlerts__c sAlert :SetupAlertsLst){
                sAlert.Processed__c = true;
                System.debug('>>>>sAlert.Processed__c'+sAlert.Processed__c);
            }
            System.debug('>><<>><<>><<>><<SetupAlertsLst'+SetupAlertsLst);
            Update SetupAlertsLst;
      //  }
    }
    
    global void finish(Database.BatchableContext BC){        
    }
    
    global static String ScheduleIt () {
        Batch_ManageNotifications batchNotifications = new Batch_ManageNotifications();
        return System.schedule('Create Notification', CRON_EXP, batchNotifications);
    }
    
    global void execute(SchedulableContext sc) 
    {
        Batch_ManageNotifications batchManageNotifications = new Batch_ManageNotifications(); 
        Database.executebatch(batchManageNotifications);
    }
}