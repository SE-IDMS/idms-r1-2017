/**
29-April-2013
Shruti Karn  
PRM May13 Release    
Initial Creation: test class for    
    1. AccountProgramBeforeInsert , AccountProgramAfterInsert, AccountProgramAfterUpdate , AP_AAP_AccountProgramUpdate
    2. ContactProgramAfterInsert, ContactProgramAfterUpdate , AP_CON_ContactProgramUpdate
    3. AARAfterUpdate, AP_AAR_UpdateRequirmentFeature
    

 */
@isTest
private class Account_ContactPartnerProgram_TEST {
    static testMethod void myUnitTest() {
                         
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        User u = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
        u.BypassVR__c = true;
        u.country__c = country.countrycode__c;
       // u.ProgramAdministrator__c = true;
        insert u;
        
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalProgram.TECH_CountriesId__c = country.id;
        globalPRogram.recordtypeid = Label.CLMAY13PRM15;
        globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
        insert globalProgram;
                
        ApexPages.currentPage().getParameters().put('GlobalPrgId',globalProgram.Id);
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(globalProgram);
        VFC_NewCountryProgram myPageCon1 = new VFC_NewCountryProgram(sdtCon1);
        
        Recordtype levelRecordType = [Select id from RecordType where developername =  'GlobalProgramLevel' limit 1];
        Recordtype featureRecordType = [Select id from RecordType where developername =  'GlobalFeature' limit 1];
        
        Recordtype countryLevelRecordType = [Select id from RecordType where developername =  'CountryProgramLevel' limit 1];
        Recordtype countryFeatureRecordType = [Select id from RecordType where developername =  'CountryFeature' limit 1];
        
        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        Recordtype programActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ProgramRequirement__c' limit 1];
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        
        CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
        newRecTypeMap.name = recCatalogActivityReq.Id;
        newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
        newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;
        
        insert newRecTypeMap;
        
        list<ProgramLevel__c> lstPRGLevel = new list<ProgramLevel__c>();
        list<FeatureCatalog__c> lstFeatureCat = new list<FeatureCatalog__c>();
        list<RequirementCatalog__c> lstReqCat = new list<RequirementCatalog__c>();
        list<FeatureRequirement__c> lstFeatureReq = new list<FeatureRequirement__c>();
        list<ProgramFeature__c> lstPRGFtr = new list<ProgramFeature__c>();
        list<ProgramRequirement__c> lstPRGReq = new list<ProgramRequirement__c>();
       
        
        system.runAs(u)
        {
            
            ProgramLevel__c newLevel = Utils_TestMethods.createProgramLevel(globalProgram.Id);
            newLevel.LevelStatus__c = Label.CLMAY13PRM19;
            newLevel.recordtypeid = levelRecordType.Id;
            lstPRGLevel.add(newLevel);
            
            ProgramLevel__c newLevel2 = Utils_TestMethods.createProgramLevel(globalProgram.Id);
            newLevel2.LevelStatus__c = Label.CLMAY13PRM19;
            newLevel2.recordtypeid = levelRecordType.Id;
            lstPRGLevel.add(newLevel2);
            
            insert lstPRGLevel;
            
            FeatureCatalog__c feature = Utils_TestMethods.createFeatureCatalog();
            feature.Enabled__c = 'Contact';
            feature.Visibility__c = 'Contact';
            lstFeatureCat.add(feature);
            insert lstFeatureCat;
            
            RequirementCatalog__c req = Utils_TestMethods.createRequirementCatalog();
            req.ApplicableTo__c = 'Contact';
            req.recordtypeid = recCatalogActivityReq.Id;
            lstReqCat.add(req);
            
            RequirementCatalog__c req2 = Utils_TestMethods.createRequirementCatalog();
            req2.ApplicableTo__c = 'Account';
            req2.recordtypeid = recCatalogActivityReq.Id;
            lstReqCat.add(req2);
            insert lstReqCat;
            
            FeatureRequirement__c  newFeatureReq2 = Utils_TestMethods.createFeatureRequirement(feature.ID ,req2.Id);
            lstFeatureReq.add(newFeatureReq2);
                                  
            FeatureRequirement__c  newFeatureReq = Utils_TestMethods.createFeatureRequirement(feature.ID ,req.Id);
            lstFeatureReq.add(newFeatureReq);
            insert lstFeatureReq;
            
            ProgramFeature__c newFeature = Utils_TestMethods.createProgramFeature(globalProgram.Id , newLevel.Id, feature.Id);
            newFeature.FeatureStatus__c = Label.CLMAY13PRM09;
            newFeature.recordtypeid = featureRecordType.Id ;
            lstPRGFtr.add(newFeature);
            
            ProgramRequirement__c newRequirement = Utils_TestMethods.createProgramRequirement(globalProgram.Id , newLevel.Id, req.ID);
            newRequirement.Active__c = true;
            newRequirement.recordtypeid = programActivityReq.ID;
            lstPRGReq.add(newRequirement);
            
            ProgramRequirement__c newRequirement1 = Utils_TestMethods.createProgramRequirement(globalProgram.Id , newLevel.Id, req2.ID);
            newRequirement1.Active__c = true;
            newRequirement1.recordtypeid = programActivityReq.ID;
            lstPRGReq.add(newRequirement1);
            
            
            ProgramFeature__c newFeature2 = Utils_TestMethods.createProgramFeature(globalProgram.Id , newLevel2.Id, feature.Id);
            newFeature2.FeatureStatus__c = Label.CLMAY13PRM09;
            //newFeature2.Enabled__c = Label.CLMAY13PRM11;
            newFeature2.recordtypeid = featureRecordType.Id;
            lstPRGFtr.add(newFeature2);
            insert lstPRGFtr;
            
            ProgramRequirement__c newRequirement2 = Utils_TestMethods.createProgramRequirement(globalProgram.Id , newLevel2.Id, req.ID);
            newRequirement2.Active__c = true;
            newRequirement2.recordtypeid = programActivityReq.ID;
            lstPRGReq.add(newRequirement2);
            
            ProgramRequirement__c newRequirement3 = Utils_TestMethods.createProgramRequirement(globalProgram.Id , newLevel2.Id, req2.ID);
            newRequirement3.Active__c = true;
            newRequirement3.recordtypeid = programActivityReq.ID;
            lstPRGReq.add(newRequirement3);
            insert lstPRGReq;           
           
            myPageCon1.goToCountryPRGEditPage();
            PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, country.Id);
            system.debug('countryProgram:'+countryProgram);
            insert countryProgram;
            list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
            countryPartnerProgram = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
            system.debug('countryPartnerProgram234 :'+countryPartnerProgram );
            if(!countryPartnerProgram.isEmpty())
            {
                countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
                update countryPartnerProgram;
            
                list<ProgramLevel__c> lstProgramLevel = [Select id,levelstatus__c from ProgramLevel__c where partnerprogram__c = :globalProgram.Id limit 10];
                list<ProgramLevel__c> lstCountryLevel = new list<ProgramLevel__c>();
                
                for(PRogramLevel__c globalLevel : lstProgramLevel )
                {
                    ProgramLevel__c countryLevel = new ProgramLevel__c();
                    countryLevel.levelstatus__C = Label.CLMAY13PRM19;
                    countryLevel.recordtypeid = countryLevelRecordType.Id;
                    countryLevel.partnerprogram__c = countryPartnerProgram[0].Id;
                    countryLevel.globalprogramlevel__C = globalLevel.Id;
                    lstCountryLevel.add(countryLevel);
                }
                insert lstCountryLevel;
                
                //Adding Country Feature
                list<ProgramFeature__c> lstgProgramFeature = [Select id,FeatureStatus__c from ProgramFeature__c where partnerprogram__c = :globalProgram.Id limit 1];
                list<ProgramFeature__c> lstCountryFeature = new list<ProgramFeature__c>();
                
                for(ProgramFeature__c globalFeature : lstgProgramFeature )
                {
                    ProgramFeature__c countryFeature = new ProgramFeature__c();
                    countryFeature.FeatureStatus__c = Label.CLMAY13PRM19;
                    //countryFeature.Enabled__c = Label.CLMAY13PRM11;
                    countryFeature.recordtypeid = countryFeatureRecordType.Id;
                    countryFeature.partnerprogram__c = countryPartnerProgram[0].Id;
                    countryFeature.GlobalProgramFeature__c = globalFeature.Id;

                    lstCountryFeature.add(countryFeature);
                }
                insert lstCountryFeature;               
                    
                //
                
                list<ProgramRequirement__c> lstProgramReq = [Select id, active__c,programlevel__c,RequirementCatalog__c from ProgramRequirement__c where partnerprogram__c = :globalProgram.Id limit 1];
                list<ProgramRequirement__c> lstCountryReq = new list<ProgramRequirement__c>();
                for(ProgramRequirement__c globalReq : lstProgramReq )
                {
                    ProgramRequirement__c countryReq = Utils_TestMethods.createProgramRequirement(countryPartnerProgram[0].Id , lstCountryLevel[0].Id, globalReq.RequirementCatalog__c);
                    countryReq.active__C = true;
                    countryReq.recordtypeid = programActivityReq.ID;
                    lstCountryReq.add(countryReq);
                }
               insert lstCountryReq;
                
                         
                Account acc = Utils_TestMethods.createAccount();
                acc.country__c = countryPartnerProgram[0].country__c;
                insert acc;
                
                
                Contact contact1 = Utils_TestMethods.createContact(acc.Id, 'Test');
                contact1.PRMContact__c = true;
                insert contact1;
                ContactAssignedFeature__c cf1 = new ContactAssignedFeature__c();
                cf1.Contact__c = contact1.id;
                cf1.FeatureCatalog__c =feature.id;
                cf1.Active__c = true;
                insert cf1;

                test.startTest();
                ACC_PartnerProgram__c accPRogram = Utils_TestMethods.createAccountProgram(lstCountryLevel[0].Id , countryPartnerProgram[0].Id,acc.Id);
                try
               {
                    insert accPRogram;
                }
               catch(Exception e){}
                acc.IsPartner  = true;
                acc.PRMAccount__c = True;
                update acc;
                insert accPRogram;

                ID levelID = accPRogram.programlevel__c;
                list<contactassignedprogram__c> lstcontact = [Select id, AccountAssignedProgram__c,Contact__c,PartnerProgram__c from ContactAssignedProgram__c where ProgramLevel__c =:levelID ];
                for(contactassignedprogram__c caft : lstcontact)
                {
                    caft.active__c=true;
                }

                list<ProgramFeature__c> lstProgramFeature = [Select id, featurestatus__c,programlevel__c,featurecatalog__c from ProgramFeature__c where partnerprogram__c = :globalProgram.Id limit 10];
                list<ProgramFeature__c> lstCountryProgramFeature = new list<ProgramFeature__c>();
               // for(ProgramFeature__c globalFeature : lstProgramFeature )
               // {
                    ProgramFeature__c countryFeature = Utils_TestMethods.createProgramFeature(countryPartnerProgram[0].Id , lstCountryLevel[0].Id, lstProgramFeature[0].featurecatalog__c);
                    countryFeature.FeatureStatus__C = Label.CLMAY13PRM09;
                    countryFeature.globalprogramfeature__c = lstProgramFeature[0].Id;
                    countryFeature.recordtypeid = countryFeatureRecordType.Id;
                    lstCountryProgramFeature.add(countryFeature);
               // }
               insert lstCountryProgramFeature;
                
                accPRogram.active__c = false;
                update accPRogram;         
                
                List<AccountAssignedRequirement__c> lsaar = [select id from AccountAssignedRequirement__c where     PartnerProgram__c=:countryPartnerProgram[0].Id];
                
                for(AccountAssignedRequirement__c aar : lsaar)
                    aar.RequirementStatus__c = 'Waived';
                update lsaar; 
                
                for(AccountAssignedRequirement__c aar : lsaar)
                    aar.RequirementStatus__c = Label.CLOCT13PRM24;
                update lsaar;
                List<ContactAssignedRequirement__c> lscar = [select id,FeatureRequirement__c from ContactAssignedRequirement__c where     PartnerProgram__c=:countryPartnerProgram[0].Id];
                
                 System.debug('**** Cont Assigned Req '+lscar.size());
                
                for(ContactAssignedRequirement__c car : lscar)
                {
                    System.debug('**** Cont Assigned Req '+car.FeatureRequirement__c);
                    car.FeatureRequirement__c = newFeatureReq2.id;
                    car.RequirementStatus__c = 'Waived';
                    car.ContactAssignedFeature__c = cf1.id;
                    System.debug('**** Cont Assigned Req '+car.FeatureRequirement__c);
                    
                }
                
                System.debug('**** completed ->');
                update lscar; 
                
                ContactAssignedRequirement__c contFeatureReq = lscar[0];
                contFeatureReq.RequirementStatus__c = 'Expired';
                update contFeatureReq;
                
                
                                
                if(lstCountryLevel.size() ==2)
                {
                    accProgram.ProgramLevel__C =lstCountryLevel[1].ID;
                    update  accProgram;
                } 
                AccountAssignedRequirement__c accountreq = new AccountAssignedRequirement__c();
                accountreq.account__C = acc.id;
                accountreq.RequirementStatus__c = Label.CLOCT13PRM24;
                insert accountreq;
                
                
                               
                             
                test.stopTest();
                
                
                
                
            }
          /* */
        }
    }
    
}