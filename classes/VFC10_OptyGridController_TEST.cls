@isTest
private class VFC10_OptyGridController_TEST
{
    static Account account1;
    static Opportunity TestOpp;
    static List<Opportunity> TestOppList = new List<Opportunity>();
    static User user;
    static 
    {
         // Profile profile = [select id from profile where name='SE - Salesperson'];        
         // user = new User(alias = 'user', email='user' + '@accenture.com', emailencodingkey='UTF-8', 
         // lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = profile.Id, 
         // BypassWF__c = false, timezonesidkey='Europe/London', username='user' + '@bridge-fo.com'); 
         // insert user;

        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        // User user;
        System.runAs(sysadmin2){
            UserandPermissionSets useruserandpermissionsetrecord=new UserandPermissionSets('user','SE - Salesperson');
           user = useruserandpermissionsetrecord.userrecord;
            Database.insert(user);

            List<PermissionSetAssignment> permissionsetassignmentlstforuser=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:useruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforuser.add(new PermissionSetAssignment(AssigneeId=user.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforuser.size()>0)
            Database.insert(permissionsetassignmentlstforuser);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }

        account1 = Utils_TestMethods.createAccount();
         insert account1;
         TestOpp = Utils_TestMethods.createOpportunity (account1.Id);
         insert TestOpp;
         TestOppList.add(TestOpp);
    }
    static TestMethod void OptyGridControllerTEST()
    {    
         ApexPages.StandardSetController OppSetController = new ApexPages.StandardSetController(TestOppList);
         OppSetController.setSelected(TestOppList);
         VFC10_OptyGridController VFController = new VFC10_OptyGridController(OppSetController);
         VFController.CustomSave();         
    
         AccountShare aShare = new AccountShare();
        aShare.accountId= account1.Id;
        aShare.UserOrGroupId = user.Id;
        aShare.accountAccessLevel = 'edit';
        aShare.OpportunityAccessLevel = 'read';
        aShare.CaseAccessLevel = 'edit';
        insert aShare;  
         system.runAs(user){ 
             VFController.CustomSave();
         }
     }
}