@isTest
public class VFC_GroupOverview_TEST {

    static testMethod void displayPublicGroup() {
        
        List<Group> groupList = new List<Group>();
        
        groupList.add(Utils_TestMethods.createRegularGroup('Test Group 1'));        
        groupList.add(Utils_TestMethods.createRegularGroup('Test Group 2'));
        
        insert groupList;
        
        UserRole userRole1 = new UserRole(DeveloperName = 'UserRole1', Name = 'Role 1');
        insert userRole1;
        
        UserRole userRole2 = new UserRole(DeveloperName = 'UserRole2', Name = 'Role 2', ParentRoleId = userRole1.Id);
        insert userRole2;
        
        List<User> userList = new List<User>();
        
        for(Integer i=0; i<10; i++) {
            User user = Utils_TestMethods.createStandardUser('user'+i);
            
            if(i == 0) {
                user.UserRoleId = userRole1.Id;
            }
            else if(i == 1) {
                user.UserRoleId = userRole2.Id;
            }
            
            userList.add(user);
        }
        
        insert userList;
        
        Group role1Group = [SELECT Id, RelatedId FROM Group 
                            WHERE RelatedId = :userRole1.Id and Type = 'RoleAndSubordinates'];
                            
        List<GroupMember> groupMemberList = new List<GroupMember>();
        groupMemberList.add(new GroupMember(GroupId = groupList[0].Id, UserOrGroupId = userList[3].Id));
        groupMemberList.add(new GroupMember(GroupId = groupList[0].Id, UserOrGroupId = userList[0].Id));
        groupMemberList.add(new GroupMember(GroupId = groupList[0].Id, UserOrGroupId = userList[4].Id));

        groupMemberList.add(new GroupMember(GroupId = groupList[0].Id, UserOrGroupId = groupList[1].Id));
        groupMemberList.add(new GroupMember(GroupId = groupList[1].Id, UserOrGroupId = userList[0].Id));
        groupMemberList.add(new GroupMember(GroupId = groupList[1].Id, UserOrGroupId = userList[4].Id));
        
        groupMemberList.add(new GroupMember(GroupId = groupList[0].Id, UserOrGroupId = role1Group.Id));
        
        groupMemberList.add(new GroupMember(GroupId = groupList[0].Id, UserOrGroupId = '00GA0000000z5HnMAI'));
        
        insert groupMemberList;
        
        PageReference publicGroupDisplay = Page.VFP_GroupOverview;
        VFC_GroupOverview groupOverviewController = new VFC_GroupOverview(new ApexPages.StandardController(groupList[0]));
        groupOverviewController.getGroupMembers();
        
        User testUser = Utils_TestMethods.createStandardUser('testuser');
        testUser.SESAExternalID__c = 'SESA00001';
        insert testUser;
        
        groupOverviewController.addUsers();
        groupOverviewController.userSesaList = 'SESA00001';
        
        groupOverviewController.importUsers();
        groupOverviewController.backToOverview();
        groupOverviewController.displayGroup();
        groupOverviewController.editGroup();         
        
        groupOverviewController.exportData();
        groupOverviewController.getNewLine();
        groupOverviewController.getQuote();        
    }
}