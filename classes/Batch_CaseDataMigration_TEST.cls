@isTest
private class Batch_CaseDataMigration_TEST {
    static testMethod void testCaseDataMigration() {
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
                
        Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        Database.insert(objContact);
        
        
        Case objCase = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        objCase.SupportCategory__c = '4 - Post-Sales Tech Support';
        objCase.Symptom__c='Troubleshooting*';
        objCase.SubSymptom__c='Hardware Product*';
        objCase.CaseSymptom__c ='Abnormal Condition - AFC not fuctional';
        insert objCase;
        
        
        CS_CaseSymptomMapping__c objCaseSymotomMapping = new CS_CaseSymptomMapping__c(Name='Abnormal Condition - AFC not fuctional',NewCaseSubSymptom__c='AFC not fuctional', NewCaseSymptom__c='Abnormal Condition');
        insert objCaseSymotomMapping;
        
        CS_CaseCategoryMapping__c objCaseCategoryMapping = new CS_CaseCategoryMapping__c(Name='57',NewCategory__c = 'Troubleshooting*', NewReason__c = '', OldCategory__c = '4 - Post-Sales Tech Support', OldReason__c = 'Troubleshooting*', OldSubReason__c = 'Hardware Product*', UniqueId__c ='4 - POST-SALES TECH SUPPORT : TROUBLESHOOTING* : HARDWARE PRODUCT*');
        insert objCaseCategoryMapping;
        

        
        Test.startTest();
        String strCaseStatus = 'Open';
        Batch_CaseDataMigration objBatchJob = new  Batch_CaseDataMigration(strCaseStatus,1);
        Database.executeBatch(objBatchJob);
        Test.stopTest();
    }
}