public class AP_PRF_BeforeHandler {


    public static boolean checkDuplicateFeature(List<ProgramFeature__c> lstPrgFeature)
    {
        //Release : FEB14 Release
        //Created by :Renjith Jose
        //BR-4496 , The Program Level cannot be assigned with both Simple ORF & ORF features

        //Get all new feature for the program level
        Set<id> setPrgLevels = new Set<id>();
        Set<id> setFeatureID = new Set<id>();

        Map<id,String> mapFeatures = new Map<id,String>();
        Map<String,ProgramFeature__c> mapNewPrgLevelFeature = new Map<String,ProgramFeature__c>();
        Map<String,ProgramFeature__c> mapLevelFeature = new Map<String,ProgramFeature__c>(); //OCT14 Release



        for(ProgramFeature__c pgF :lstPrgFeature)
        {
                setPrgLevels.add(pgF.ProgramLevel__c);
                setFeatureID.add(pgF.FeatureCatalog__c);

                //OCT14 Release
                //Checking newly added feature has duplicate entry
                if(mapLevelFeature.containsKey(pgF.ProgramLevel__c+'-'+pgF.FeatureCatalog__c))
                    pgF.addError(System.label.CLOCT14PRM23);
                else
                    mapLevelFeature.put(pgF.ProgramLevel__c+'-'+pgF.FeatureCatalog__c,pgF);                    

        }

        List<FeatureCatalog__c> lstFeatureCatalog = [Select id,Category__c from FeatureCatalog__c where id in :setFeatureID];
        for(FeatureCatalog__c af :lstFeatureCatalog){
            for(String orfFeature : System.Label.CLFEB14PRM18.Split(','))
            {
                //if (af.Category__c != null && ((af.Category__c.contains('Project') || af.Category__c.contains('Opportunity Registration'))))
                  if (af.Category__c != null && af.Category__c.contains(orfFeature))
                        mapFeatures.put(af.id, af.Category__c);
            }       


        }


        for(ProgramFeature__c pgl1 :lstPrgFeature)
        {
            if(mapFeatures.containsKey(pgl1.FeatureCatalog__c)){
                String Category = mapFeatures.get(pgl1.FeatureCatalog__c);
                for(String ct :Category.split(';'))
                {
                    //if(ct == 'Project' || ct == 'Opportunity Registration')
                    if(System.label.CLFEB14PRM18.contains(ct))
                    {
                        if(mapNewPrgLevelFeature.containsKey(pgl1.ProgramLevel__c))
                        {   pgl1.addERROR(System.Label.CLFEB14PRM06);
                            return false;
                        }   
                        else    
                            mapNewPrgLevelFeature.put(pgl1.ProgramLevel__c,pgl1);
                    }               
                }
            }
            //else
                //return true;
        }


       //get all 
        system.debug('*****size->'+setPrgLevels.size());
        if(setPrgLevels.size()>0)
        {
            List<ProgramFeature__c> lstPrgmLvls = [Select id, ProgramLevel__c,FeatureCatalog__c,FeatureCatalog__r.Category__c from ProgramFeature__c where ProgramLevel__c = :setPrgLevels];
            system.debug('*****size1->'+lstPrgmLvls.size());

            Map<String,String> mapPrgLevelFeature = new Map<String,String>();
            if(lstPrgmLvls.size()>0)
            {
                for(ProgramFeature__c aPgl :lstPrgmLvls){
                    if(aPgl.FeatureCatalog__r.Category__c != null)
                    {
                        for(String afeture :aPgl.FeatureCatalog__r.Category__c.split(';'))
                        {
                           // if(afeture == 'Project' || afeture == 'Opportunity Registration')
                            if(System.label.CLFEB14PRM18.contains(afeture))
                                mapPrgLevelFeature.put(aPgl.ProgramLevel__c,afeture);
                        }       
                    } 

                    //Start:OCT14 Release
                    if(mapLevelFeature.containsKey(aPgl.ProgramLevel__c+'-'+aPgl.FeatureCatalog__c))
                     {
                        if(mapLevelFeature.get(aPgl.ProgramLevel__c+'-'+aPgl.FeatureCatalog__c).id != aPgl.id)
                        {
                            mapLevelFeature.get(aPgl.ProgramLevel__c+'-'+aPgl.FeatureCatalog__c).addError(System.Label.CLOCT14PRM23);
                            return false;
                        }
                     }

                    //End:OCT14 Release

                }           
            }

            System.debug('******size2'+mapPrgLevelFeature.size());
            for(String aPrgLevel :mapNewPrgLevelFeature.keyset())
            {
                if(mapPrgLevelFeature.containsKey(aPrgLevel))
                {
                    mapNewPrgLevelFeature.get(aPrgLevel).addERROR(System.Label.CLFEB14PRM06);
                    return false;
                }   
            }
        }

        return true;
    }
  //-----------------------------------------------------------------------------------------------------------------
}