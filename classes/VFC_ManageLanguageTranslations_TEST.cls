@isTest
private class VFC_ManageLanguageTranslations_TEST{
     /*@ testSetup static void methodName() {
      List<CS_PRM_FrontendLabels__c> MLTList = new List<CS_PRM_FrontendLabels__c>();
        List<ExternalString> ExtStrList = new List<ExternalString>();
        CS_PRM_FrontendLabels__c MLT01 = new CS_PRM_FrontendLabels__c(Name = 'CLTest001',Category__c = 'CLTestCatagory001',Value__c = 'This is a test Customlabel 001',Usage__c ='for Test Class');
        MLTList.add(MLT01);
        CS_PRM_FrontendLabels__c MLT02 = new CS_PRM_FrontendLabels__c(Name = 'CLTest002',Category__c = 'CLTestCatagory002',Value__c = 'This is a test Customlabel 002',Usage__c ='for Test Class');
        MLTList.add(MLT02);
        insert MLTList; 
        
      
    }*/
 
    @isTest(seeAllData = true) static void Translations_TEST() {
        //List<sObject> ls = Test.loadData(ExternalString.sObjectType, 'testExternalString');
        //System.assert(ls.size() == 2);
        /*
        List<CS_PRM_FrontendLabels__c> MLTList = new List<CS_PRM_FrontendLabels__c>();
        CS_PRM_FrontendLabels__c MLT01 = new CS_PRM_FrontendLabels__c(Name = 'CLOct15PRMTest001',Category__c = 'CLTestCatagory001',Usage__c ='for Test Class');
        MLTList.add(MLT01);
        CS_PRM_FrontendLabels__c MLT02 = new CS_PRM_FrontendLabels__c(Name = 'CLOct15PRMTest002',Category__c = 'CLTestCatagory002',Usage__c ='for Test Class');
        MLTList.add(MLT02);
        insert MLTList;
        */
        Test.setMock(WebServiceMock.class, new ReadCategoryLangTranslationsMockImpl());
        
         PageReference pageRef = Page.VFP_ManageLanguageTranslations;
        Test.setCurrentPage(pageRef); 
       
         String strRecordTypeId = [SELECT ID  FROM RecordType Where SobjectType = 'PRMFrontEndLabels__c' and developerName = 'PartnerPortal'].Id;     
         String str1RecordTypeId = [SELECT ID FROM RecordType Where SobjectType = 'PRMFrontEndLabels__c' and developerName = 'POMP'].Id;
     
        List<PRMFrontEndLabels__c> lData = new List<PRMFrontEndLabels__c>();
        PRMFrontEndLabels__c f1 = new PRMFrontEndLabels__c(Name = 'CLOct15PRMTest001', Category__c = 'CLTestCatagory001',Usage__c ='for Test Class', ComponentAPIName__c = 'AccountName__c', ComponentType__c = 'CustomField', ObjectAPIName__c ='Account', RecordTypeId=strRecordTypeId, Active__c = True);
        lData.add(f1);
        PRMFrontEndLabels__c f2 = new PRMFrontEndLabels__c(Name = 'CLOct15PRMTest002', Category__c = 'CLTestCatagory002',Usage__c ='for Test Class', ComponentAPIName__c = 'ProductName__c', ComponentType__c ='CustomField', ObjectAPIName__c ='Account', RecordTypeId=str1RecordTypeId, Active__c = True);
        lData.add(f2);
        
        insert lData;
        system.debug('@@@Idata'+lData);
        //calling wrapper class
        VFC_ManageLanguageTranslations.TranslationWrapper oWrapper = new VFC_ManageLanguageTranslations.TranslationWrapper();
        oWrapper.LabelName = '';
        oWrapper.MasterValue = '';
        oWrapper.ComponentType = '';
        oWrapper.ComponentAPIName = '';
        oWrapper.ObjectAPIName = '';
        oWrapper.ExternalStringId = '';
        oWrapper.ExternalStringLocalizationId = '';
        
        Test.startTest();
        //ExternalString extStrController= new ExternalString();
        Test.SetCurrentPageReference(New PageReference('Page.VFP_ManageLanguageTranslations'));
        VFC_ManageLanguageTranslations thisManageLanguageTranslations = new VFC_ManageLanguageTranslations();
        thisManageLanguageTranslations.LabelCategoryList = null;
        
        thisManageLanguageTranslations.SelectedLanguage = 'en_US';
        thisManageLanguageTranslations.SelectedApplication = strRecordTypeId;
        thisManageLanguageTranslations.SelectedCategory = 'CLTestCatagory001';
        List<SelectOption> testSelOpt = thisManageLanguageTranslations.LabelCategoryList;
        thisManageLanguageTranslations.getSelCategoryLangTranslations();
        thisManageLanguageTranslations.doSave();
        thisManageLanguageTranslations.cancel();
         Test.stopTest();
    }
    @isTest(seeAllData = true) static void ExternalMethod() {
         PageReference pageRef = Page.VFP_ManageLanguageTranslations;
        Test.setCurrentPage(pageRef); 
       
         String strRecordTypeId = [SELECT ID  FROM RecordType Where SobjectType = 'PRMFrontEndLabels__c' and developerName = 'PartnerPortal'].Id;     
         String str1RecordTypeId = [SELECT ID FROM RecordType Where SobjectType = 'PRMFrontEndLabels__c' and developerName = 'POMP'].Id;
     
        List<PRMFrontEndLabels__c> lexData = new List<PRMFrontEndLabels__c>();
        PRMFrontEndLabels__c f1 = new PRMFrontEndLabels__c(Name = 'CLOct15PRMTest001', Category__c = 'CLTestCatagory001',Usage__c ='for Test Class', ComponentAPIName__c = 'AccountName__c', ObjectAPIName__c ='Account', RecordTypeId=strRecordTypeId, Active__c = True);
        lexData.add(f1);
        PRMFrontEndLabels__c f2 = new PRMFrontEndLabels__c(Name = 'CLOct15PRMTest002', Category__c = 'CLTestCatagory002',Usage__c ='for Test Class', ComponentAPIName__c = 'ProductName__c', ObjectAPIName__c ='Account', RecordTypeId=str1RecordTypeId, Active__c = True);
        lexData.add(f2);
        
        insert lexData;
        
        Test.startTest();
        //ExternalString extStrController= new ExternalString();
        Test.SetCurrentPageReference(New PageReference('Page.VFP_ManageLanguageTranslations'));
        VFC_ManageLanguageTranslations thisManageLanguageTranslations = new VFC_ManageLanguageTranslations();
        thisManageLanguageTranslations.LabelCategoryList = null;
        
        thisManageLanguageTranslations.SelectedLanguage = 'en_US';
        thisManageLanguageTranslations.SelectedApplication = strRecordTypeId;
        thisManageLanguageTranslations.SelectedCategory = 'CLTestCatagory001';
        List<SelectOption> testSelOpt = thisManageLanguageTranslations.LabelCategoryList;
        thisManageLanguageTranslations.getSelCategoryLangTranslations();
        thisManageLanguageTranslations.doSave();
        thisManageLanguageTranslations.cancel();
         Test.stopTest();
    
    }
    @isTest(seeAllData = true) public static void categorySaveCancel(){
         String strRecordTypeId = [SELECT ID  FROM RecordType Where SobjectType = 'PRMFrontEndLabels__c' and developerName = 'PartnerPortal'].Id;     
         String str1RecordTypeId = [SELECT ID FROM RecordType Where SobjectType = 'PRMFrontEndLabels__c' and developerName = 'POMP'].Id;
     
        List<PRMFrontEndLabels__c> extData = new List<PRMFrontEndLabels__c>();
        PRMFrontEndLabels__c f1 = new PRMFrontEndLabels__c(Name = 'CLOct15PRMTest001', Category__c = 'CLTestCatagory001',Usage__c ='for Test Class', ComponentAPIName__c ='AccountName__c', ComponentType__c = 'CustomField', ObjectAPIName__c ='OpportunityRegistrationForm__c', RecordTypeId=strRecordTypeId, Active__c = True);
        extData.add(f1);
        PRMFrontEndLabels__c f2 = new PRMFrontEndLabels__c(Name = 'CLOct15PRMTest002', Category__c = 'CLTestCatagory002',Usage__c ='for Test Class', ComponentAPIName__c ='ProductName__c', ComponentType__c ='CustomField', ObjectAPIName__c ='OpportunityRegistrationProducts__c', RecordTypeId=str1RecordTypeId, Active__c = True);
        extData.add(f2);
        
        insert extData;
     
      VFC_ManageLanguageTranslations sc = new VFC_ManageLanguageTranslations();
        VFC_ManageLanguageTranslations.TranslationWrapper oWrapper = new VFC_ManageLanguageTranslations.TranslationWrapper();
        sc.SelectedLanguage = 'en_US';
          sc.SelectedApplication = strRecordTypeId;
          sc.SelectedCategory = 'CLTestCatagory001';
          sc.doSave();
          sc.cancel();
   }
    public static testMethod void method() {
        Test.setMock(WebServiceMock.class, new ReadPicklistTranslationMockImpl());
        
        PageReference pageRef = Page.VFP_ManageLanguageTranslations;
        Test.setCurrentPage(pageRef);  
        String strRecordTypeId = [Select Id From RecordType Where SobjectType = 'PRMLovs__c' and developerName = 'BT'].Id;
        String str1RecordTypeId = [Select Id From RecordType Where SobjectType = 'PRMLovs__c' and developerName = 'AF'].Id;
        List<PRMLovs__c> newList = new List<PRMLovs__c>();
        PRMLovs__c val = new PRMLovs__c(RecordTypeId =strRecordTypeId, ObjectAPIName__c ='Account', FieldAPIName__c='PRMBusinessType__c'); 
        newList.add(val);
        
        PRMLovs__c val1 = new PRMLovs__c(RecordTypeId =str1RecordTypeId, ObjectAPIName__c ='Account', FieldAPIName__c='PRMAreaOfFocus__c');
        newList.add(val1);
        INSERT newList;
         
        
        Test.startTest();
        //ExternalString extStrController= new ExternalString();    
        Test.SetCurrentPageReference(New PageReference('Page.VFP_ManageLanguageTranslations'));
        //Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(extStrController); 
        VFC_ManageLanguageTranslations cc = new VFC_ManageLanguageTranslations();
        
       
        
            cc.SelectedPicklistType = 'BT';
            system.debug('@@selectedpicklisttype'+ cc.SelectedPicklistType);
            cc.SelectedPicklistLanguage  = 'en_US';
            system.debug('@@selectedpicklistlang'+ cc.SelectedPicklistLanguage);
            cc.getSelPicklistTypeLangTranslations();
            cc.doSavePicklistTypes();
            cc.doCancelPicklistTypes();
            
            //Country__c country1 = Utils_TestMethods.createCountry();
            //insert country1;
              Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
            INSERT testCountry;
        
        StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
           INSERT testStateProvince; 
            
             cc.SelectedPicklistType = System.Label.CLMAR16PRM003 ;
             cc.SelectedPicklistLanguage  = 'en_US'; 
             cc.getSelPicklistTypeLangTranslations();
             cc.doSavePicklistTypes();
             cc.doCancelPicklistTypes();
             
        
            cc.SelectedPicklistType = System.Label.CLMAR16PRM004;
            cc.SelectedPicklistLanguage  = 'en_US'; 
            cc.StateProvinceCountry = testCountry.Id;
            cc.getSelPicklistTypeLangTranslations();  
            cc.doSavePicklistTypes();
            cc.doCancelPicklistTypes();
        
             List<CountryStateLanguageTranslation__c> newConLang = new List<CountryStateLanguageTranslation__c>();
        CountryStateLanguageTranslation__c consLan = new CountryStateLanguageTranslation__c();
        consLan.Country__c = testCountry.Id;
        consLan.LanguageCode__c = 'en_US';
        consLan.LanguageText__c = 'English';
        consLan.StateProvinces__c = testStateProvince.Id;
        INSERT consLan;
        
      
       Test.stopTest();
    }
    @isTest(seeAllData = true) public static  void OptionMethods(){
        Test.SetCurrentPageReference(New PageReference('Page.VFP_ManageLanguageTranslations'));
        //Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(extStrController); 
        VFC_ManageLanguageTranslations cc = new VFC_ManageLanguageTranslations();
        
        
           List<SelectOption> options = cc.fieldPickList;
         Map<Id,Country__c> oopCon = cc.Countries;
         List<SelectOption> temp = cc.CountryPicklist;
         List<SelectOption> temp2 = cc.ApplicationList;
    }
     
     public static testmethod void AlertNotifications(){
        PageReference pageRef = Page.VFP_ManageLanguageTranslations;
        Test.setCurrentPage(pageRef);
        
        Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
        INSERT testCountry;
        
        PRMCountry__c countryCluster = new PRMCountry__c();
            countryCluster.Country__c = testCountry.Id;
            countryCluster.CountryPortalEnabled__c = True; 
            countryCluster.PLDatapool__c = 'IN_en2';
            countryCluster.SupportedLanguage1__c = 'en_US';
            countryCluster.SupportedLanguage2__c = 'hi';
            countryCluster.SupportedLanguage3__c = 'en_IN';
        INSERT countryCluster;
        
        SetupAlerts__c setAlert = new SetupAlerts__c();
        setAlert.Active__c = True;
        //setAlert.Contact__c
        setAlert.PRMCountryClusterSetup__c = countryCluster.Id;
        INSERT setAlert; 
        
        
        List<AlertTranslations__c> AlertNot = new List<AlertTranslations__c>();
        AlertTranslations__c AN1 = new AlertTranslations__c(Name = 'aHnJ00000008OPBtest', TitleTranslation__c ='testTranslations', Translation__c ='testtrans', SystemAlert__c = setAlert.Id);
        AlertNot.add(AN1);
        AlertTranslations__c AN2 = new AlertTranslations__c(Name = 'aHnJ00000008ONFtest', TitleTranslation__c ='testTranslations1', Translation__c ='testtrans1', SystemAlert__c = setAlert.Id);
        AlertNot.add(AN2);
        
        INSERT AlertNot;
        
        Test.startTest();
        //ExternalString extStrController= new ExternalString();    
        Test.SetCurrentPageReference(New PageReference('Page.VFP_ManageLanguageTranslations'));
        //Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(extStrController); 
        VFC_ManageLanguageTranslations AN = new VFC_ManageLanguageTranslations();
        
        List<SelectOption> temp = AN.LanguagesList;
        List<SelectOption> temp2 = AN.AlertLanguagesList;
        List<SelectOption> temp3 = AN.GlobalSystemAlertList; 
        
        AN.SelectedGlobalSystemAlert = 'aHnJ00000008OJrKAM';
        AN.SelectedAlertLanguage = 'en_US';
        AN.getGlobalSystemAlerts();
        AN.doSaveAlertTranslations();
        AN.doCancelAlertTranslations();
        Test.stopTest();
     } 
}