/********************************************************************
* Company: Fielo
* Created Date: 05/08/2016
* Description:
********************************************************************/
global without Sharing class FieloPRM_SiteURLRewrite implements Site.UrlRewriter {
    
    global PageReference mapRequestUrl(PageReference friendlyUrl) {
        try {
            FieloEE__Program__c program = FieloEE.ProgramUtil.getProgramByDomain();            
            String url = friendlyUrl.getUrl().toLowerCase();
            System.debug('>>>>url:'+friendlyUrl.getHeaders().get('Referrer'));
            System.debug('url:'+url);            
            PageReference retPageRef;
            if(url.startsWithIgnoreCase('/Menu/')){
                url = url.replaceFirst('/menu/', '');
                Integer position = url.contains('?') ? url.indexOf('?') : url.length();
                String menuName = url.substring(0, position);                
                if(!String.isBlank(menuName)){
                    FieloEE__Menu__c menu = [SELECT Id, Name, FieloEE__CustomURL__c, FieloEE__Menu__r.FieloEE__ExternalName__c FROM FieloEE__Menu__c WHERE FieloEE__ExternalName__c =: menuName AND FieloEE__Program__c =: program.Id LIMIT 1];
                    String pageName =  '';
                    if(menu.FieloEE__CustomURL__c != null){
                        pageName = menu.FieloEE__CustomURL__c;
                    }else{
                        if(menuName.equalsIgnoreCase('MyPartnership') || (String.isNotBlank(menu.FieloEE__Menu__r.FieloEE__ExternalName__c) && menu.FieloEE__Menu__r.FieloEE__ExternalName__c.equalsIgnoreCase('MyPartnership'))){
                            pageName = '/apex/FieloPRM_VFP_MyPartnership';
                        }else if(menuName.equalsIgnoreCase('Login') || menuName.equalsIgnoreCase('pomplogin')){
                            pageName = '/apex/FieloPRM_VFP_Login';                        
                        }else if(menuName.equalsIgnoreCase('LoginInit')){
                            pageName = '/apex/VFP_PRMLoginInit';
                        }else if(menuName.equalsIgnoreCase('MyPrograms')){
                            pageName = '/apex/FieloPRM_VFP_PageMyPrograms';
                        }else if(menuName.equalsIgnoreCase('Support')){
                            pageName = '/apex/FieloPRM_VFP_PageSupport';
                        }else if(menuName.equalsIgnoreCase('News')){
                            pageName = '/apex/FieloPRM_VFP_PageNews';
                        }else if(menuName.equalsIgnoreCase('Products')){
                            pageName = '/apex/FieloPRM_VFP_PageProducts';
                        }else if(menuName.equalsIgnoreCase('Documents')){
                            pageName = '/apex/FieloPRM_VFP_PageDocuments';
                        }else{
                            pageName = '/apex/FieloPRM_VFP_Page';                               
                        }
                    }                    
                    retPageRef = new PageReference(pageName);
                    retPageRef.getParameters().put('idMenu', menu.Id);                      
                }else{
                    retPageRef = new PageReference('/apex/FieloPRM_VFP_MyPartnership');
                }
                retPageRef.setRedirect(true);
                return retPageRef;
            }else if(url == '/'){//.endsWithIgnoreCase('/partners') || url.endsWithIgnoreCase('/partners/')
                retPageRef = new PageReference('/apex/FieloPRM_VFP_MyPartnership');
                retPageRef.setRedirect(true);
                return retPageRef;
            }               
        }catch(Exception e){
            System.debug('exception:' + e.getMessage());
        }
        return null;
    }

    global List<PageReference> generateUrlFor(List<PageReference> salesforceUrls) {
        //A list of pages to return after all the links have been evaluated
        List<PageReference> friendlyUrls = new List<PageReference>();
        //a list of all the Ids in the urls
        Set<Id> menuIds = new Set<Id>();
        
            
        FieloEE__Program__c program = FieloEE.ProgramUtil.getProgramByDomain();
        // Loop through all the urls to find all the valid ids
        for(PageReference url : salesforceUrls){
            //Get the Id from the url
            if(url.getParameters().containsKey('idMenu')){
                menuIds.add(url.getParameters().get('idMenu'));
            }
        }
        // Get all the names in bulk
        Map<Id,FieloEE__Menu__c> menusMap = new Map<Id,FieloEE__Menu__c>();
        if(!menuIds.isEmpty())
            menusMap = new Map<Id,FieloEE__Menu__c>([SELECT FieloEE__ExternalName__c FROM FieloEE__Menu__c WHERE Id IN : menuIds]);

        // it is important to go through all the urls again, so that the order of the urls in the list is maintained.
        for(PageReference url : salesforceUrls){
            if(url.getParameters().containsKey('idMenu')){
                Id menuId = url.getParameters().get('idMenu');
                if(menusMap.containsKey(menuId)){
                    String externalName = menusMap.get(menuId).FieloEE__ExternalName__c != null ? menusMap.get(menuId).FieloEE__ExternalName__c : '';
                    pageReference redirectPageRef = new PageReference('/Menu/' + externalName);
                    redirectPageRef.getParameters().putAll(url.getParameters());
                    redirectPageRef.getParameters().remove('idMenu');                    
                    friendlyUrls.add(redirectPageRef);
                }
            }/*else if(url.getUrl().startsWith('FieloPRM_VFP_Login'){
                friendlyUrls.add(new PageReference('/Menu/Login'));                
            }*/
            else{
                friendlyUrls.add(url);
            }
        }
        return friendlyUrls;
    }
}