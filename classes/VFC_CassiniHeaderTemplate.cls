global class VFC_CassiniHeaderTemplate {

   public integer points {get;set;}
   //public integer contractDays {get;set;}
   public Date contractEndDate {get;set;}
   public static String strError {get;set;}
   public static List<Package__c> lstPackage;
   public static List<Contract > lstContracts;
   public boolean pointsFlag {get;set;}

   public VFC_CassiniHeaderTemplate(){ 
      points=0;
      //contractDays=0;
      pointsFlag=false;
      //List<Package__c> lstPackageToAccessPoints=new List<Package__c>();
      List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){ List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){ List<Contract> lstContract=[SELECT Id,Points__c,EndDate FROM Contract Where Id=:lstCVCPs[0].Contract__c and WebAccess__c =: 'Cassini' LIMIT 1];
               if(!lstContract.isEmpty()){ points=integer.valueOf(lstContract[0].Points__c); contractEndDate=lstContract[0].EndDate; List<Package__c> lstPackageToAccessPoints=[SELECT Id FROM Package__c WHERE Contract__c = :lstContract[0].Id AND PackageTemplate__r.PortalAccess__c = TRUE AND PackageTemplate__r.PointOfContact__r.ChannelType__c ='Portal' ORDER BY LastModifiedDate DESC LIMIT 1];
                    if(!lstPackageToAccessPoints.isEmpty()) pointsFlag=true;
                }
            }
        }
   }
  @RemoteAction
    public static string ContractdayCheck(){
       lstContracts = new  List<Contract >();
      Integer contractDays =0;
      lstPackage  = new List<Package__c>();
        List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){ List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){ lstContracts=[SELECT Id,Points__c,EndDate,Status FROM Contract Where Id=:lstCVCPs[0].Contract__c and WebAccess__c =: 'Cassini' LIMIT 1];
               if(!lstContracts.isEmpty()){ 
               if(!Test.isRunningTest()) contractDays=System.Today().daysbetween(lstContracts[0].EndDate); 
               lstPackage=[SELECT PackageTemplate__c,PackageTemplate__r.PortalAccess__c,PackageTemplate__r.PointOfContact__c,PackageTemplate__r.PointOfContact__r.Priority1Routing__c,PackageTemplate__r.PointOfContact__r.DefaultRoutingTeam__c,PackageTemplate__r.PointOfContact__r.Organization__c FROM Package__c WHERE Contract__c = :lstContracts[0].Id AND PackageTemplate__r.PortalAccess__c = TRUE AND PackageTemplate__r.PointOfContact__r.ChannelType__c ='Portal' ORDER BY LastModifiedDate DESC LIMIT 1]; 
               }
            }
        }
        if(lstContracts.isEmpty()){ return 'no_access' ; }
        if(!lstPackage.isEmpty()  &&( lstContracts[0].Status =='Grace' ||(lstContracts[0].Status =='Activated' &&  system.now() < lstContracts[0].EndDate.addDays(30)))){ return 'unableFaq_casecreate'; }
        if(lstPackage.isEmpty()  &&( lstContracts[0].Status =='Grace' ||(lstContracts[0].Status =='Activated' &&  system.now()< lstContracts[0].EndDate.addDays(30)))){ return 'unablefaq'; }
        else{ return 'no_access'; }
    }
    
    public void checkContract(){
        lstContracts = new  List<Contract >();
        Integer contractDays=0;
        lstPackage  = new List<Package__c>();
        List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){ List<CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){ lstContracts=[SELECT Id,Points__c,EndDate,Status FROM Contract Where Id=:lstCVCPs[0].Contract__c and WebAccess__c =: 'Cassini' LIMIT 1];
                  if(!lstContracts.isEmpty()){ 
                  if(!Test.isRunningTest()) contractDays=System.Today().daysbetween(lstContracts[0].EndDate); 
                  lstPackage=[SELECT PackageTemplate__c,PackageTemplate__r.PortalAccess__c,PackageTemplate__r.PointOfContact__c,PackageTemplate__r.PointOfContact__r.Priority1Routing__c,PackageTemplate__r.PointOfContact__r.DefaultRoutingTeam__c,PackageTemplate__r.PointOfContact__r.Organization__c FROM Package__c WHERE Contract__c = :lstContracts[0].Id AND PackageTemplate__r.PortalAccess__c = TRUE AND PackageTemplate__r.PointOfContact__r.ChannelType__c ='Portal' ORDER BY LastModifiedDate DESC LIMIT 1]; }
            }
        }
        if(lstContracts.isEmpty()){ strError ='no_access'; return ; }
        else if(!lstPackage.isEmpty()  &&( lstContracts[0].Status =='Grace' ||(lstContracts[0].Status =='Activated' &&  system.now() < lstContracts[0].EndDate.addDays(30)))){ strError ='unableFaq_casecreate'; }
        else if(lstPackage.isEmpty()  &&( lstContracts[0].Status =='Grace' ||(lstContracts[0].Status =='Activated' &&  system.now()< lstContracts[0].EndDate.addDays(30)))){ strError = 'unablefaq'; }
        else { strError ='no_access'; }    
    }
}