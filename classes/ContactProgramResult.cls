global class ContactProgramResult{
    // WS error codes & error message
    WebService Integer returnCode;
    WebService String message;
    WebService Integer totalNoOfContactPrograms; 
     
    WebService ContactProgram[] ContactPrograms { get; set; }
    WebService Datetime BatchMaxLastModifiedDate {get;set;}
}