@isTest(SeeAllData=true)
public with sharing class VFC_WorkDetailsInter_Test {

    static TestMethod void WorkDetailsInterTest() 
    {
        PageReference pageRef = Page.VFP_WorkDetailsInter;
        
        
           Account acc = Utils_TestMethods.createAccount();
             insert acc;
        Country__c Ctry = Utils_TestMethods.createCountry(); 
        insert Ctry;
        
        User caseCreationUser = Utils_TestMethods.createStandardUser('TestUser');
        caseCreationUser.BypassVR__c =true;
        caseCreationUser.UserBusinessUnit__c = 'EN';
        Insert caseCreationUser;
        
        SVMXC__Service_Group__c stg= new SVMXC__Service_Group__c();
        stg.name='teststg';
        insert stg;
        
        
        
    
        
       
       SVMXC__Service_Group__c st;
       SVMXC__Service_Group_Members__c sgm1;
        
         RecordType[] rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c'];
         RecordType[] rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c'];  
          for(RecordType rt : rts) 
        {
            if(rt.DeveloperName == 'Technician')
           {
                 st = new SVMXC__Service_Group__c(
                                            RecordTypeId =rt.Id,
                                            SVMXC__Active__c = true,
                                            Name = 'Test Service Team'                                                                                        
                                            );
               
           } 
        }
        insert st;
        for(RecordType rt : rtssg)
        {
           if(rt.DeveloperName == 'Technican')
            {
                sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =rt.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            /*SVMXC__Salesforce_User__c = userinfo.getuserId(),*/
                                                            SVMXC__Salesforce_User__c = caseCreationUser.Id,
                                                            Send_Notification__c = 'Email',
                                                            SVMXC__Email__c = 'sgm1.test@company.com',
                                                            SVMXC__Phone__c = '12345'
                                                            );
                
            }
        }
        insert sgm1;
        
         SVMXC__Service_Order__c testWorkOrder = Utils_TestMethods.createWorkOrder(acc.Id);
         testWorkOrder.SVMXC__Service_Group__c=stg.id;
         insert testWorkOrder;
         
        SVMXC__Service_Order_Line__c workOrderLine = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c = testWorkOrder.Id,
        SVMXC__End_Date_and_Time__c=date.today()+30,SVMXC__Group_Member__c=sgm1.Id,
        SVMXC__Service_Group__c=st.Id);
        database.insert (workOrderLine,false);
         
         
         Test.setCurrentPage(pageRef);
         ApexPages.StandardController sc = new ApexPages.StandardController(workOrderLine );
         VFC_WorkDetailsInter controller = new VFC_WorkDetailsInter (sc );
         pageRef.getParameters().put(Label.CLMAY13SRV44, sgm1.Id);
        pageRef.getParameters().put('save_new','test');
         controller.init();
         
        // ApexPages.StandardController controller = new ApexPages.StandardController(oppon);
        VFC_WorkDetailsInter newObj = new VFC_WorkDetailsInter(sc);
        
        PageReference pageRef1 = Page.VFP_WorkDetailsInter ;
        pageRef1.getParameters().put(Label.CLMAY13SRV44, sgm1.Id);
        Test.setCurrentPage(pageRef1);
        newObj.Init();
        
    }

}