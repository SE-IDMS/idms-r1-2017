/*
    Author          : Bhuvana Subramaniyan (ACN) 
    Date Created    : 13/01/2011
    Description     : Test Class for VFC17_ProjInfoTranslationValues.
*/
@isTest
private class VFC17_ProjInfoTranslationValues_TEST
{
    //Test method
    static testMethod void testProjInfoTranslationValues() 
    {
        //inserts Account
        Account acc = RIT_Utils_TestMethods.createAccount();
        insert acc;
        acc = [select id from Account where Id=:acc.Id];
        
        //inserts Opportunity
        Opportunity opp = RIT_Utils_TestMethods.createOpportunity(acc.Id);
        insert opp;
        opp = [select id from Opportunity where Id =: opp.Id];
        
        //inserts QuoteLink
        OPP_QuoteLink__c quoteLink = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
        insert quoteLink;        
        quoteLink = [select id from OPP_QuoteLink__c where Id=:quoteLink.Id];
        
        //inserts Project Information
        RIT_ProjectInformation__c projInfo = RIT_Utils_TestMethods.createProjInfo(quoteLink.Id);
        insert projInfo ;        
        projInfo = [select id from RIT_ProjectInformation__c where Id=:projInfo.Id];
        
        ApexPages.StandardController con = new ApexPages.StandardController(projInfo);
        
        PageReference pageRef = Page.VFP17_ProjInfoTranslationValues;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', projInfo.id);

        //Test method Execution Begins
        Test.StartTest();
        VFC17_ProjInfoTranslationValues projInfoTransValues = new VFC17_ProjInfoTranslationValues(con);
        Test.StopTest();
        //Test method Execution Ends
    }
}