@isTest
public class VFC_PRMTermsAndConditions_TEST{   
    public static testMethod void testMethodOne() {
        FieloEE.MockUpFactory.setCustomProperties(false); 
            CS_PRM_ApexJobSettings__c jobSet =new CS_PRM_ApexJobSettings__c(Name='SERVE_MINIFIED',InitialSync__c=True);
            insert jobSet;
        
            Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
            INSERT testCountry;
            
            StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
            INSERT testStateProvince;
            
            Account acc = new Account(Name='TestAccount1',PRMCorporateHeadquarters__c = True,PRMAdditionalAddress__c = 'New Street Additional Address',
                                                PRMStreet__c ='New Street',PRMCity__c = 'Bangalore', POBox__c='XXX', PRMZipCode__c = '12345', ZipCode__c = '1234',PRMStateProvince__c = testStateProvince.Id,
                                                PRMCompanyName__c='TestAccount1',PRMCountry__c= testCountry.id,PRMBusinessType__c='FI1',OwnerId=userinfo.getuserId());
            INSERT acc; 
            
            Contact con = new Contact();
            con.AccountId = acc.Id;
            con.FirstName = 'test contact Fn';
            con.LastName  = 'test contact Ln';
            con.PRMCompanyInfoSkippedAtReg__c = false;
            con.PRMCountry__c = testCountry.Id;
            con.PRMFirstName__c = 'PRM test contact Fn'; 
            con.PRMLastName__c  = 'PRM test contact Fn';
            insert con;
            User u1;        
            List<Profile> profiles = [select id from profile where name='SE - Channel Partner (Community)'];
            if(profiles.size()>0){
                UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
                u1 = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
                u1.ContactId = con.Id;
                u1.PRMRegistrationCompleted__c = false;
                u1.BypassWF__c = false;
                u1.BypassVR__c = true;
                u1.FirstName ='Test User First';
                Insert u1;
            }   
            
            test.startTest();
                   
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo',
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'Calle Falsa',
                F_PRM_PrimaryChannel__c = 'TST',F_Country__c= testCountry.Id,
                F_Account__c=acc.Id
            );
            Insert member;
            
            FieloEE__Menu__c menu = new FieloEE__Menu__c(Name = 'test');
            insert menu;
            
            //creates a component
            RecordType rt = [SELECT ID FROM RecordType WHERE DeveloperName = 'Banner'];
            FieloEE__Component__c comp = new FieloEE__Component__c(RecordTypeId = rt.Id, FieloEE__Menu__c = menu.Id);
            insert comp;
            
            con.FieloEE__Member__c = member.Id;
            UPDATE con;           
            
            System.runAs(u1){
                VFC_PRMTermsAndConditions tnc = new VFC_PRMTermsAndConditions();
                tnc.currentMember = member;
                tnc.ComponentId = comp.Id;
                Boolean testBool = tnc.ServeMinified;
                String tncValue = tnc.IsTnCAgreed;
                //system.assertEquals(tnc.ServeMinified, true);
                tnc.getTnCText();
                VFC_PRMTermsAndConditions.UpdateTnC('AGREE');
            }
            test.stopTest();
        }    
}