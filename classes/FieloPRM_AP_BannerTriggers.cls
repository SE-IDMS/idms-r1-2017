public class FieloPRM_AP_BannerTriggers{

    public static void removeCache(List<FieloEE__Banner__c> banners){               
        Set<Id> tagIds = new Set<Id>();
        for(FieloEE__TagItem__c ti : [SELECT FieloEE__Tag__c FROM FieloEE__TagItem__c WHERE FieloEE__Banner__c in : banners]){
            tagIds.add(ti.FieloEE__Tag__c);
        }

        Set<Id> categoryIds = new Set<Id>();
        Set<Id> componentIds = new Set<Id>();
        for(FieloEE__Banner__c banner : banners){
            if(banner.FieloEE__Category__c != null){
                categoryIds.add(banner.FieloEE__Category__c);
            }

            if(banner.FieloEE__Component__c != null){
                componentIds.add(banner.FieloEE__Component__c);
            }
        }

        if(!tagIds.isEmpty() || !categoryIds.isEmpty() || !componentIds.isEmpty()){
            String whereCondition = '';
            
            if(!tagIds.isEmpty()){
                whereCondition += 'OR FieloEE__Tag__c in : tagIds ';
            }
            if(!categoryIds.isEmpty()){
                whereCondition += 'OR FieloEE__Category__c in : categoryIds ';
            }
            if(!componentIds.isEmpty()){
                whereCondition += 'OR Id in : componentIds ';
            }
            whereCondition = whereCondition.removeStart('OR');
            String query = 'SELECT Id, FieloEE__Section__r.FieloEE__Parent__c FROM FieloEE__Component__c WHERE ' + whereCondition;
            List<FieloEE__Component__c> components = Database.query(query); 
            
            Set<Id> parentSectionIds = new Set<Id>();
            for(FieloEE__Component__c component : components){                  
                parentSectionIds.add(component.FieloEE__Section__r.FieloEE__Parent__c);
            }

            List<FieloEE__Section__c> parentSections = new List<FieloEE__Section__c>();
            for(Id sectionId : parentSectionIds){
                parentSections.add(new FieloEE__Section__c(Id = sectionId));
            }
            update parentSections;
        }
    }
}