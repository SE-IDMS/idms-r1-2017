/*
    Author          : Anoop Innocent (SFDC) 
    Date Created    : 10/08/2016
    Description     : Test Class created for MatchingModule class
*/
@isTest

public class AP_UserMapping_Test
{

//pass L1 and L2 users
static testMethod void PassSingleMatchAccAndCon()
{

//create a test country  
  Country__c  TestCountry = new Country__c (Name ='India' ,CountryCode__c ='IN');
  insert TestCountry;
  
  list<Country__c> countryList = new list<Country__c>(); 
                       
  
 countryList =  [select id,Name,CountryCode__c from Country__c where name =: 'India' limit 1];
 
 //create test state
 //create a test country  
  StateProvince__c TestState = new StateProvince__c (Name ='Karnataka' ,CountryCode__c =countryList [0].CountryCode__c ,
  Country__c= countryList [0].id,StateProvinceCode__c ='10',StateProvinceExternalId__c =  'IN10');
  insert TestState ;
  
  list<StateProvince__c > StateList = new list<StateProvince__c >(); 
                       
 StateList =  [select id,Name from StateProvince__c  where StateProvinceCode__c  =: '10' limit 1];
 
  //Insert fresh Account    and contact   
  Account acc =new Account (ClassLevel1__c ='SI' ,Name='TestCompany',Street__c='TestStreet',City__c='testCity',
  Country__c = countryList [0].id,StateProvince__c=StateList [0].id,TECH_RunIDMSAccountDuplicateRule__c = True);
  insert acc;
  
  Account accAssert = [Select Id,Name from Account where Id =: acc.Id];
  system.assertequals(accAssert.Name,'TestCompany');
  
  Contact con =new Contact (Email='alias456@accenture.com',MobilePhone='97345',LastName='Testing',
  Country__c= countryList [0].id,CorrespLang__c='',
  CurrencyIsoCode='INR',AccountId= acc.id,Department='IT',Fax='',StateProv__c =StateList [0].id);
  insert con;
  

Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' limit 1]; 
User user1 = new User(alias = 'alias456', email='alias456@accenture.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, BypassWF__c = True,  
            timezonesidkey='Europe/London', username='alias456@bridge-fo.com', CAPRelevant__c=true, VisitObjDay__c=1,
            AvTimeVisit__c=3,WorkDaysYear__c=200,BusinessHours__c=8,UserBusinessUnit__c='Cross Business',Country__c= countryList [0].id,
            IDMSClassLevel1__c = 'SI',Street = 'TestStreet',City = 'testCity',IDMS_POBox__c = '12341',Company_Website__c = '',
            IDMS_State__c ='Karnataka',CompanyName='TestCompany',state= StateList[0].id );

  system.assertEquals(user1.Street,acc.Street__c)  ; 
    system.assertEquals(user1.CompanyName,acc.Name)  ;
      system.assertEquals(user1.state,acc.StateProvince__c)  ;
        system.assertEquals(user1.City ,acc.City__c)  ;     
   
 User[] usersL2 = new List<User>{user1};

 AP_MatchingModule.preMatch(usersL2); 
 
 
 }


           }