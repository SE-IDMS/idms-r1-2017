// Test Class for Utils_OrderDataSource
@istest
private class Utils_OrderDataSource_Test{

    static testMethod void testUtils_OrderDataSource() {
    
    
        Account acc = Utils_TestMethods.createAccount();
        acc.Name='Test Account';
        insert acc;
        VCC07_OrderConnectorController occ = new VCC07_OrderConnectorController(); 
        WS06_OrderList.orderStatusListSearchDataBean OrderSearchCriteria = new WS06_OrderList.orderStatusListSearchDataBean();
        //OrderSearchCriteria.bfoAccountId = acc.Id;
        occ.PartNo = '0001';
        occ.OrderNumber = '0002';   
        occ.PONumber = '1111';  
        occ.DisplayAdvancedCriteria = true; 
        occ.DateCapturer.Account__c = acc.id;  
        Utils_OrderDataSource Utilds = new Utils_OrderDataSource();
        Utilds.test = true;
        Utilds.getColumns();
        Utilds.getDetailsColumns();
        Utilds.SearchOrder(occ);
        Utilds.SearchOrderDetails(occ);        
    }
}