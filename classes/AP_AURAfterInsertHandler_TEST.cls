/*
  Author:   Bhuvana Subramaniyan    
  Release:  Oct 2013
*/
@isTest
private class AP_AURAfterInsertHandler_TEST 
{
    static testmethod void updateApprover()
    {
        User dataStewdUser = Utils_TestMethods.createStandardUser([select Id,Name from Profile where Name=:'SE - Data Steward'].Id, 'DStdUsr');   
        insert dataStewdUser;
        Country__c cntry1 = new Country__c();
        cntry1.Name='TestCountry1';
        cntry1.CountryCode__c='IT';
        cntry1.DataSteward__c = dataStewdUser.Id;
        insert cntry1;
        
        Account testaccount = Utils_TestMethods.createAccount(); 
        testaccount.Country__c = cntry1.Id;
        insert testaccount;
    
        List<AccountUpdateRequest__c> lstAURs = new List<AccountUpdateRequest__c>();
        AccountUpdateRequest__c aur1 = new AccountUpdateRequest__c(Account__c = testaccount.id,
                                                                 //RecordTypeId=System.Label.CLMAR16ACC01,
                                                                 AccountName__c = testaccount.name,
                                                                 City__c='Test City',
                                                                 Street__c='Test Street',
                                                                 Country__c=cntry1.id,
                                                                 ZipCode__c='12345');
        lstAURs.add(aur1);
                                                                 
        AccountUpdateRequest__c aur2 = new AccountUpdateRequest__c(Account__c = testaccount.id,
                                                                 //RecordTypeId=System.Label.CLMAR16ACC02,
                                                                 AccountName__c = testaccount.name
                                                                 );
        lstAURs.add(aur2);
        
        insert lstAURs;
    
    }
}