/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest//(SeeAllData=true)
private class ServiceMaxEventAfterInsert_TEST {

    /**
      *  Author: Chris Hurd
      *  Purpose: Test creation of recurring events when dispatching
      *  Code Located: EventAfterInsert.trigger
      */
    /*static testmethod void recurringEventsTest()
    {
        SVMXC__Service_Order__c WORec = new SVMXC__Service_Order__c(
            RecurrenceEndDate__c = system.today().addDays(6), 
            Frequency__c = 1, 
            FrequencyUnit__c = 'Days', 
            SkipWeekends__c = false 
        );
        
        insert WORec;
        
        WORec = [SELECT Id, Name FROM SVMXC__Service_Order__c WHERE Id = :WORec.Id LIMIT 1];
        
        Event e = new Event(
            Subject=WORec.Name,
            StartDateTime=Datetime.newInstance(2025, 20 , 12, 00, 00, 00),
            EndDateTime=Datetime.newInstance(2025, 20 , 12, 01, 00, 00),
            ActivityDateTime = Datetime.newInstance(2025, 20 , 12, 00, 00, 00),
            DurationInMinutes = 60,
            Whatid=WORec.id,
            Ownerid=UserInfo.getUserId() 
        );        
        
        insert e;
        
        List<Event> eList = [SELECT Id FROM Event WHERE WhatId = :WORec.Id];
        
        //system.assertEquals(7, eList.size());
    }
    */
    static testmethod void recurringSVMXEventsTest()
    {
        list<BusinessHours> lBH = [select WednesdayStartTime, WednesdayEndTime, TuesdayStartTime, TuesdayEndTime, ThursdayStartTime, ThursdayEndTime, SundayStartTime, SundayEndTime, SaturdayStartTime, SaturdayEndTime, MondayStartTime, MondayEndTime, IsDefault, IsActive, Id, FridayStartTime, FridayEndTime from BusinessHours where isDefault = true limit 1];
        SVMXC__Service_Order__c WORec = new SVMXC__Service_Order__c(
            RecurrenceEndDate__c = system.today().addDays(6), 
            Frequency__c = 1, 
            FrequencyUnit__c = 'Days', 
            SkipWeekends__c = false ,
           
            SVMXC__Order_Status__c ='Cancelled'
        );
        
        insert WORec;
        
        WORec = [SELECT Id, Name FROM SVMXC__Service_Order__c WHERE Id = :WORec.Id LIMIT 1];
        
        SVMXC__SVMX_Event__c e = new SVMXC__SVMX_Event__c(
            Name=WORec.Name,
            SVMXC__StartDateTime__c=system.now(),
            SVMXC__EndDateTime__c=system.now().addHours(4),
            SVMXC__WhatId__c=WORec.id,
            SVMXC__WhoId__c = UserInfo.getUserId(),
            Ownerid=UserInfo.getUserId() 
        );
            SVMXC__Service_Group__c st= new SVMXC__Service_Group__c(Name='ste');
            insert st;
            SVMXC__Service_Group_Members__c  tech= new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c=st.id,name='test',SVMXC__Salesforce_User__c=userinfo.getuserid(), SVMXC__Working_Hours__c= lBH[0].Id);
            insert tech;
            
             User user = new User(alias = 'user', email='user' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLFEB14SRV02    , BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX05;AP54;AP_Contact_PartnerUserUpdate',
                             timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
            AssignedToolsTechnicians__c AssignedTech = new AssignedToolsTechnicians__c();
            AssignedTech.EventId__c =e.id;
            AssignedTech.WorkOrder__c = WORec.id;        
           insert AssignedTech;
        
        
            //system.runAs(user)
            {
                insert e;
                e.SVMXC__StartDateTime__c=system.now().addHours(1);
                Utils_SDF_Methodology.removeFromRunOnce('SVMX03');
                Utils_SDF_Methodology.removeFromRunOnce('SVMX01_REC_SVMXEvent');
                e.Name = 'WO-';
                update e;
                
            }
        
        
        //List<SVMXC__SVMX_Event__c> eList = [SELECT Id FROM SVMXC__SVMX_Event__c WHERE SVMXC__WhatId__c = :WORec.Id];
        
        //system.assertEquals(7, eList.size());
    }
    static testMethod void myUnitTest() {
        
       // system.runAs(getUser()){
            Country__c countrySobj =Utils_TestMethods.createCountry();
            insert countrySobj ;
            Account accountSobj = Utils_TestMethods.createAccount();
            accountSobj.SEAccountID__c ='SEAccountID';
            accountSobj.RecordTypeId  = System.Label.CLOCT13ACC08;
            accountSobj.Country__c= countrySobj.id;
            insert accountSobj;
            Contact contactSobj = Utils_TestMethods.createContact(accountSobj.id, 'contact LastName');
            contactSobj.Country__c = countrySobj.id;
            contactSobj.SEContactID__c ='SEContactID1';
            contactSobj.FirstName = 'CFirstName';
            contactSobj.LastName= 'CLastName';
            contactSobj.LocalFirstName__c= 'CLFirstName';
            contactSobj.MidInit__c ='TMid';
            contactSobj.LocalMidInit__c='TLMid';
            contactSobj.LocalLastName__c='CLLastName';
            contactSobj.Email='test@test.com';
            contactSobj.mobilePhone ='9999999999';
            contactSobj.WorkPhone__c='9999999999';
            insert contactSobj;
            SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(accountSobj.id);
            workOrder.Work_Order_Category__c='On-site';                 
            workOrder.SVMXC__Order_Type__c='Maintenance';
            workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
            workOrder.SVMXC__Order_Status__c = 'UnScheduled';
            workOrder.CustomerRequestedDate__c = Date.today();
            workOrder.Service_Business_Unit__c = 'IT';
            workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
            workOrder.SendAlertNotification__c = true;            
            workOrder.BackOfficeReference__c = '111111';            
            workOrder.Comments_to_Planner__c='testing'; 
            workOrder.SVMXC__Company__c  =accountSobj.Id;
            workOrder.SVMXC__Contact__c  =contactSobj.Id;
            workOrder.CustomerTimeZone__c= 'Europe/London';      
            insert workOrder;
            Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__Service_Group__c; 
            Map<String,Schema.RecordTypeInfo> ServiceGroup = dSobjres.getRecordTypeInfosByName();
            //Id Technician = ServiceGroup.get('Technician').getRecordTypeId();
            SVMXC__Service_Group__c sg = new SVMXC__Service_Group__c();
            sg.name = 'Test Service Team';
            sg.SVMXC__Active__c = true;
            sg.RecordTypeId = '012A0000000nYLvIAM';
            insert sg;
            
            Schema.DescribeSObjectResult dSobjres2 = Schema.SObjectType.SVMXC__Service_Group_Members__c; 
            Map<String,Schema.RecordTypeInfo> FSRRT = dSobjres2.getRecordTypeInfosByName();
            //Id fsr = FSRRT.get('FSR').getRecordTypeId();
            SVMXC__Service_Group_Members__c sgm = new SVMXC__Service_Group_Members__c();          
            sgm.RecordTypeId = '012A0000000nYNhIAM';
            sgm.SVMXC__Service_Group__c =sg.id;
            sgm.SVMXC__Role__c = 'Schneider Employee';            
            sgm.SVMXC__Salesforce_User__c = userinfo.getUserId();
            sgm.Send_Notification__c = 'Email';
            sgm.SVMXC__Email__c = 'test.test@company.com';
            sgm.SVMXC__Phone__c = '123456789';
            insert sgm;
            
            
            Event event= new Event (Subject=workOrder.name,
                                  StartDateTime=system.now().addDays(1),
                                  EndDateTime=system.now().addDays(1),
                                  Whatid=workOrder.id,
                                  
                                  Ownerid=UserInfo.getUserId()                                                    
                                  );
               try{                   
            insert event;
            } catch (exception e){}
            AssignedToolsTechnicians__c att = Utils_TestMethods.CreateAssignedTools_Technician(WorkOrder.id, sgm.id);
            att.Status__c = 'Accepted';
            att.EventId__c = event.id;
            insert att;
            set<id> eventset= new set<id>();
            String EventString = JSON.serialize(new list<Event>{event});
            AP_EventCreate.CreateAssignedToolsTechnicians(new Set<id>{workOrder.id},EventString,'');
            AP_EventCreate.UpdateAssignedToolsTechnicians(EventString,new Set<id>{att.id});
            SVMXC__SVMX_Event__c sevent = New SVMXC__SVMX_Event__c(
                                                        Name=workOrder.name,
                                                        SVMXC__Service_Team__c=sg.Id,
                                                        SVMXC__Technician__c=sgm.Id,
                                                        SVMXC__StartDateTime__c=system.now(),
                                                        SVMXC__EndDateTime__c=system.now()+1,
                                                        SVMXC__WhatId__c = workOrder.Id
                                                     );
                                                     
             
           
            insert sevent;
            eventset.add(sevent.id);
  
               String EventStrng = JSON.serialize(new list<SVMXC__SVMX_Event__c >{sevent});
           try{ 
            AP_EventCreate.SVMXCreateAssignedToolsTechnicians(EventStrng,eventset);  
             }catch (exception e){}
       // }
        
        
    }
    
}