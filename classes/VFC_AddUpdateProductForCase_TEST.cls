@isTest
private class VFC_AddUpdateProductForCase_TEST{

    static testMethod void AddUpdateProductforCaseTestMethod() {
    //Start of Test Data preparation
    
        country__c samplecountry=null;
        List<Country__c> countries =[select id,Name,CountryCode__c from country__c limit 1];
        if(countries.size()==1)
            samplecountry=countries[0];
        else{               
            samplecountry=new country__c(Name='TestCountry',CountryCode__c='IN');
            insert samplecountry;    
        }
               
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = samplecountry.Id;
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        objContact.Country__c = samplecountry.Id;
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        // Create several Products
        List<Product2> productToInsert = new List<Product2>();
        Integer usecase = 0;
        for(Integer i=0 ; i <= 15 ; i++){ //(String commercialReference,String strMaterialDescription, String businessUnit, String productLine, String productFamily, String family,
            if(usecase == 1){
                Product2 objCommercialReference = Utils_TestMethods.createCommercialReference('VFCPROD'+i,'MaterialDescription For VFCPROD'+i,'BU-VFCPROD'+i, 'PL-VFCPROD'+i, 'PF-VFCPROD'+i, 'F-VFCPROD1',false, false, false);
                productToInsert.add(objCommercialReference);
                usecase = 0;
            }else{
                Product2 objCommercialReference = Utils_TestMethods.createCommercialReference('VFCPROD'+i,'MaterialDescription For VFCPROD'+i,'BU-VFCPROD'+i, 'PL-VFCPROD'+i, 'PF-VFCPROD'+i, 'F-VFCPROD0',false, false, false);
                productToInsert.add(objCommercialReference);
                usecase = 1;
            }
        }
        Product2 objCommercialReference1 = Utils_TestMethods.createCommercialReference('VFCPROD22','MaterialDescription For VFCPROD','BU-VFCPROD', 'PL-VFCPROD', 'PF-VFCPROD', 'F-VFCPROD3',false, false, false);
        productToInsert.add(objCommercialReference1);
        Product2 objCommercialReference2 = Utils_TestMethods.createCommercialReference('VFCPROD33','MaterialDescription For VFCPROD','BU-VFCPROD', 'PL-VFCPROD', 'PF-VFCPROD', 'F-VFCPROD4',false, false, false);
        productToInsert.add(objCommercialReference2);
        Database.insert(productToInsert);
        
        List<Product2> searchresults = [Select id,Name,ProductFamilyId__c,ProductGDP__r.HierarchyType__c,ProductGDP__r.HierarchyTypeoftheParent__c from Product2];
        String debug = '';
        for(Product2 prod : searchResults){
            debug += prod.ProductFamilyId__c +','+prod.ProductGDP__r.HierarchyType__c+','+prod.ProductGDP__r.HierarchyTypeoftheParent__c+' || ';
        }
        system.debug('######## searchresults debug '+debug);
        // Create 2 Case Related Product
        objCommercialReference1 = [Select id,ProductGDP__r.HierarchyType__c,ProductGDP__r.HierarchyTypeoftheParent__c,ProductCode,Name,ProductGDP__c,CommercialReference__c,ProductGDP__r.BusinessUnit__c,ProductGDP__r.ParentHierarchy__c,ProductGDP__r.FamilyID__c,ProductGDP__r.ProductLine__c,ProductGDP__r.ProductFamily__c,ProductGDP__r.Family__c,ProductGDP__r.SubFamily__c,ProductGDP__r.Series__c,ProductGDP__r.GDP__c,ProductGDP__r.TECH_PM0CodeInGMR__c,GDPGMR__c,toLabel(CommercialStatus__c),ProductFamilyId__c from Product2 where Id =:objCommercialReference1.Id];  
        objCommercialReference2 = [Select id,ProductGDP__r.HierarchyType__c,ProductGDP__r.HierarchyTypeoftheParent__c,ProductCode,Name,ProductGDP__c,CommercialReference__c,ProductGDP__r.BusinessUnit__c,ProductGDP__r.ParentHierarchy__c,ProductGDP__r.FamilyID__c,ProductGDP__r.ProductLine__c,ProductGDP__r.ProductFamily__c,ProductGDP__r.Family__c,ProductGDP__r.SubFamily__c,ProductGDP__r.Series__c,ProductGDP__r.GDP__c,ProductGDP__r.TECH_PM0CodeInGMR__c,GDPGMR__c,toLabel(CommercialStatus__c),ProductFamilyId__c from Product2 where Id =:objCommercialReference2.Id];  
        
        system.debug('######## objCommercialReference1 '+objCommercialReference1.ProductFamilyId__c+'/'+objCommercialReference1.ProductGDP__r.HierarchyType__c+'/'+objCommercialReference1.ProductGDP__r.HierarchyTypeoftheParent__c);
        
        system.debug('######## objCommercialReference2 '+objCommercialReference2.ProductFamilyId__c+'/'+objCommercialReference2.ProductGDP__r.HierarchyType__c+'/'+objCommercialReference2.ProductGDP__r.HierarchyTypeoftheParent__c);
        
        CSE_RelatedProduct__c objCaseRelatedProduct1 = new CSE_RelatedProduct__c();
        objCaseRelatedProduct1.Case__c = objOpenCase.Id;
        objCaseRelatedProduct1.CommercialReference__c = objCommercialReference1.Name;
        objCaseRelatedProduct1.CommercialReference_lk__c = objCommercialReference1.Id;
        objCaseRelatedProduct1.Family_lk__c = objCommercialReference1.ProductFamilyId__c;
        objCaseRelatedProduct1.BusinessUnit__c = objCommercialReference1.ProductGDP__r.BusinessUnit__c;
        objCaseRelatedProduct1.ProductLine__c = objCommercialReference1.ProductGDP__r.ProductLine__c;
        objCaseRelatedProduct1.ProductFamily__c = objCommercialReference1.ProductGDP__r.ProductFamily__c;
        objCaseRelatedProduct1.Family__c = objCommercialReference1.ProductGDP__r.Family__c;
        objCaseRelatedProduct1.TECH_GMRCode__c = objCommercialReference1.ProductGDP__r.TECH_PM0CodeInGMR__c;
        objCaseRelatedProduct1.TECH_UniqueId__c = objCaseRelatedProduct1.Case__c + '-' + objCaseRelatedProduct1.CommercialReference_lk__c;
        objCaseRelatedProduct1.Name = objCommercialReference1.Name;
        objCaseRelatedProduct1.MasterProduct__c = true;
        Database.insert(objCaseRelatedProduct1);
        
        CSE_RelatedProduct__c objCaseRelatedProduct2 = new CSE_RelatedProduct__c();
        objCaseRelatedProduct2.Case__c = objOpenCase.Id;
        objCaseRelatedProduct2.Family_lk__c = objCommercialReference2.ProductFamilyId__c;
        objCaseRelatedProduct2.BusinessUnit__c = objCommercialReference2.ProductGDP__r.BusinessUnit__c;
        objCaseRelatedProduct2.ProductLine__c = objCommercialReference2.ProductGDP__r.ProductLine__c;
        objCaseRelatedProduct2.ProductFamily__c = objCommercialReference2.ProductGDP__r.ProductFamily__c;
        objCaseRelatedProduct2.Family__c = objCommercialReference2.ProductGDP__r.Family__c;
        objCaseRelatedProduct2.TECH_GMRCode__c = objCommercialReference2.ProductGDP__r.TECH_PM0CodeInGMR__c;
        objCaseRelatedProduct2.TECH_UniqueId__c = objCaseRelatedProduct2.Case__c + '-' + objCaseRelatedProduct2.Family_lk__c;
        objCaseRelatedProduct2.Name = objCommercialReference2.Name;
        objCaseRelatedProduct2.MasterProduct__c = false;
        objCaseRelatedProduct2.SubFamily__c = null;
        objCaseRelatedProduct2.ProductSuccession__c = null;
        objCaseRelatedProduct2.ProductGroup__c = null;
        objCaseRelatedProduct2.ProductDescription__c = null;
        objCaseRelatedProduct2.CommercialReference__c = null;
        objCaseRelatedProduct2.CommercialReference_lk__c = null;
        Database.insert(objCaseRelatedProduct2);
        
        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objOpenCase);   
        VFC_AddUpdateProductForCase GMRSearchPage = new VFC_AddUpdateProductForCase(CaseStandardController );
            
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();
        GMRSearchPage.jsonSelectString=jsonSelectString;
        Test.startTest();
        PageReference pageRef = Page.VFP_AddUpdateProductForCase;
        Test.setCurrentPage(pageRef);
        System.debug('########## set_currCaseListExistingProducts '+GMRSearchPage.set_currCaseListExistingProducts);
        //WS_GMRSearch.resultFilteredDataBean results;
        VFC_AddUpdateProductForCase.getOthers(objCommercialReference1.GDPGMR__c.subString(0,2));
        VFC_AddUpdateProductForCase.getOthers(objCommercialReference1.GDPGMR__c.subString(0,4));
        VFC_AddUpdateProductForCase.getOthers(objCommercialReference1.GDPGMR__c.subString(0,6));
        VFC_AddUpdateProductForCase.getOthers(objCommercialReference1.GDPGMR__c.subString(0,8));

        /** 
            Test : Function remoteSearch 
        **/
        List<Object>  lstFilteredDataBean = VFC_AddUpdateProductForCase.remoteSearch('',objCommercialReference1.GDPGMR__c.subString(0,2),false,false,false,false,false);
        lstFilteredDataBean = VFC_AddUpdateProductForCase.remoteSearch('VFCPROD',objCommercialReference1.GDPGMR__c.subString(0,4),false,false,false,false,false);
        lstFilteredDataBean = VFC_AddUpdateProductForCase.remoteSearch('VFCPROD',objCommercialReference1.GDPGMR__c.subString(0,6),false,false,false,false,false);
        lstFilteredDataBean = VFC_AddUpdateProductForCase.remoteSearch('VFCPROD',objCommercialReference1.GDPGMR__c.subString(0,8),false,false,false,false,false);
        lstFilteredDataBean = VFC_AddUpdateProductForCase.remoteSearch('VFCPROD',objCommercialReference1.GDPGMR__c,false,false,false,false,false);
        lstFilteredDataBean = VFC_AddUpdateProductForCase.remoteSearch('','',true,true,true,true,true);

        // Get the list of results as WS_GMRSearch.gmrFilteredDataBean 
        List<WS_GMRSearch.gmrFilteredDataBean> lstGmrFilteredDataBeanCR_case1 = new List<WS_GMRSearch.gmrFilteredDataBean>();
        List<WS_GMRSearch.gmrFilteredDataBean> lstGmrFilteredDataBeanFamily_case1 = new List<WS_GMRSearch.gmrFilteredDataBean>();
        
        List<WS_GMRSearch.gmrFilteredDataBean> lstGmrFilteredDataBeanCR_case2 = new List<WS_GMRSearch.gmrFilteredDataBean>();
        List<WS_GMRSearch.gmrFilteredDataBean> lstGmrFilteredDataBeanFamily_case2 = new List<WS_GMRSearch.gmrFilteredDataBean>();
        
        List<WS_GMRSearch.gmrFilteredDataBean> lstGmrFilteredDataBeanCR_case3 = new List<WS_GMRSearch.gmrFilteredDataBean>();
        List<WS_GMRSearch.gmrFilteredDataBean> lstGmrFilteredDataBeanFamily_case3 = new List<WS_GMRSearch.gmrFilteredDataBean>();
        
        String masterCR1 = '';
        String masterCR2 = '';
        String masterCR3 = '';
        String masterFamily1 = '';
        String masterFamily2 = '';
        String masterFamily3 = '';
        String masterCR0 = '';
        
        for(Object objObject:lstFilteredDataBean){
            WS_GMRSearch.gmrFilteredDataBean objGmrFilteredDataBean = (WS_GMRSearch.gmrFilteredDataBean) objObject;
            if(objGmrFilteredDataBean.family.label.UnEscapeXML() == 'F-VFCPROD0'){
                if(objGmrFilteredDataBean.businessLine.label.UnEscapeXML().contains('0') || objGmrFilteredDataBean.businessLine.label.UnEscapeXML().contains('4') || objGmrFilteredDataBean.businessLine.label.UnEscapeXML().contains('6') ){
                    objGmrFilteredDataBean.ProductFamilybFO = objCommercialReference1.ProductGDP__r.ParentHierarchy__c;
                    objGmrFilteredDataBean.ProductIdbFO  = objCommercialReference1.Id; 
                    masterCR1 = objGmrFilteredDataBean.commercialReference;
                    masterCR0 = objOpenCase.Id + '-' + objGmrFilteredDataBean.ProductIdbFO;
                }else{
                    masterCR2 = objGmrFilteredDataBean.commercialReference;
                    objGmrFilteredDataBean.ProductFamilybFO = null;
                    objGmrFilteredDataBean.ProductIdbFO  = null; 
                }
                lstGmrFilteredDataBeanCR_case1.add(objGmrFilteredDataBean);
                objGmrFilteredDataBean.Tech_GMRCode = objGmrFilteredDataBean.Tech_GMRCode.subString(0,8);
                lstGmrFilteredDataBeanFamily_case1.add(objGmrFilteredDataBean);
            }else if(objGmrFilteredDataBean.family.label.UnEscapeXML() == 'F-VFCPROD1'){
                if(objGmrFilteredDataBean.businessLine.label.UnEscapeXML().contains('1') || objGmrFilteredDataBean.businessLine.label.UnEscapeXML().contains('3') || objGmrFilteredDataBean.businessLine.label.UnEscapeXML().contains('5') ){
                    objGmrFilteredDataBean.ProductFamilybFO = objCommercialReference2.ProductGDP__r.ParentHierarchy__c;
                    objGmrFilteredDataBean.ProductIdbFO  = objCommercialReference2.Id;
                    masterFamily1 = objGmrFilteredDataBean.Tech_GMRCode.length() >= 8 ? objGmrFilteredDataBean.Tech_GMRCode.subString(0,8) : objGmrFilteredDataBean.Tech_GMRCode;  
                }else{
                    masterFamily2 = objGmrFilteredDataBean.Tech_GMRCode.length() >= 8 ? objGmrFilteredDataBean.Tech_GMRCode.subString(0,8) : objGmrFilteredDataBean.Tech_GMRCode;  
                    objGmrFilteredDataBean.ProductFamilybFO = null;
                    objGmrFilteredDataBean.ProductIdbFO  = null; 
                }
                
                lstGmrFilteredDataBeanCR_case2.add(objGmrFilteredDataBean);
                objGmrFilteredDataBean.Tech_GMRCode = objGmrFilteredDataBean.Tech_GMRCode.subString(0,8);
                lstGmrFilteredDataBeanFamily_case2.add(objGmrFilteredDataBean);
            }else{
                if(objGmrFilteredDataBean.family.label.UnEscapeXML() == 'F-VFCPROD3'){
                    masterCR3 = objGmrFilteredDataBean.commercialReference;  
                    lstGmrFilteredDataBeanCR_case3.add(objGmrFilteredDataBean);                    
                }else{
                    masterFamily3 = objGmrFilteredDataBean.Tech_GMRCode.length() >= 8 ? objGmrFilteredDataBean.Tech_GMRCode.subString(0,8) : objGmrFilteredDataBean.Tech_GMRCode;
                    lstGmrFilteredDataBeanFamily_case3.add(objGmrFilteredDataBean);
                }
            }
        }
        
        /****** @Cecile LARTAUD     December 2014 Release   START *******/
        
        /** 
            Test : Change the master product 
        **/
            String newMaster = objCaseRelatedProduct2.TECH_UniqueId__c;
            String prevMaster = GMRSearchPage.previousSelectedMasterProduct;
            ApexPages.currentPage().getParameters().put('selectedMasterProduct',newMaster);
            GMRSearchPage.selectMasterProduct();
            
            // Check results 
            System.assertEquals(GMRSearchPage.selectedMasterProduct, newMaster);
            System.assertEquals(GMRSearchPage.oldMasterProduct, objCaseRelatedProduct1.TECH_UniqueId__c);
            System.assertEquals(GMRSearchPage.previousSelectedMasterProduct, newMaster);
            System.assertEquals(TRUE, GMRSearchPage.map_ShoppingCartProducts.get(newMaster).MasterProduct__c);
            System.assertEquals(FALSE, GMRSearchPage.map_ShoppingCartProducts.get(prevMaster).MasterProduct__c);
        
        /** 
            Test : AddProductsToShoppingCart
        **/
            /* Case 1.1 : A family with BFO Id as master */
                // Create a JSON with the list of results below
                WS_GMRSearch.resultFilteredDataBean results_product = new WS_GMRSearch.resultFilteredDataBean();
                results_product.gmrFilteredDataBeanList = lstGmrFilteredDataBeanCR_case2;
                String results_product_json_case2 = json.serialize(results_product);
                
                WS_GMRSearch.resultFilteredDataBean results_family = new WS_GMRSearch.resultFilteredDataBean();
                results_family.gmrFilteredDataBeanList = lstGmrFilteredDataBeanFamily_case2;
                String results_family_json_case2 = json.serialize(results_family);
                
                // Set the parameters on the page
                ApexPages.currentPage().getParameters().put('masterCR',masterFamily1);
                ApexPages.currentPage().getParameters().put('selected_products',results_product_json_case2);
                ApexPages.currentPage().getParameters().put('selected_families',results_family_json_case2);
                
                // Call the function
                GMRSearchPage.addProductToShoppingCart();
            
            /* Case 1.2 : A family without BFO Id as master */
                // Set the parameters on the page
                ApexPages.currentPage().getParameters().put('masterCR',masterFamily2);
                ApexPages.currentPage().getParameters().put('selected_products',results_product_json_case2);
                ApexPages.currentPage().getParameters().put('selected_families',results_family_json_case2);
                
                // Call the function
                GMRSearchPage.addProductToShoppingCart();
                
            
            /* Case 2.1 : A product without BFO Id as master */
                // Create a JSON with the list of results below
                results_product = new WS_GMRSearch.resultFilteredDataBean();
                results_product.gmrFilteredDataBeanList = lstGmrFilteredDataBeanCR_case1;
                String results_product_json_case1 = json.serialize(results_product);
                
                results_family = new WS_GMRSearch.resultFilteredDataBean();
                results_family.gmrFilteredDataBeanList = lstGmrFilteredDataBeanFamily_case1;
                String results_family_json_case1 = json.serialize(results_family);
        
                // Set the parameters on the page
                ApexPages.currentPage().getParameters().put('masterCR',masterCR2);
                ApexPages.currentPage().getParameters().put('selected_products',results_product_json_case1);
                ApexPages.currentPage().getParameters().put('selected_families',results_family_json_case1);
                
                // Call the function
                GMRSearchPage.addProductToShoppingCart();
        
            /* Case 2.2 : A product with BFO Id as master */
                
                // Set the parameters on the page
                ApexPages.currentPage().getParameters().put('masterCR',masterCR1);
                ApexPages.currentPage().getParameters().put('selected_products',results_product_json_case1);
                ApexPages.currentPage().getParameters().put('selected_families',results_family_json_case1);
                
                // Call the function
                GMRSearchPage.addProductToShoppingCart();
                
        /** 
            Test : Remove an existing Product from shoping cart
        **/
            String productLineToRemoved = objCaseRelatedProduct1.TECH_UniqueId__c;
            ApexPages.currentPage().getParameters().put('selectedProductRemoved', productLineToRemoved);
            GMRSearchPage.removeLineShoppingCart();
            
            // Check results 
            System.assertEquals(TRUE,GMRSearchPage.set_currCaseListExistingProducts.contains(objCaseRelatedProduct1.Id));
            System.assertEquals(TRUE,GMRSearchPage.set_currCaseListRemovedProducts.contains(objCaseRelatedProduct1.Id));
            System.assertEquals(GMRSearchPage.selectedProductRemoved, productLineToRemoved);
            System.assertEquals(FALSE, GMRSearchPage.map_ShoppingCartProducts.containsKey(productLineToRemoved));
        
        /** 
            Test : AddProductsToShoppingCart add a previously removed existing product
        **/
            // Create a JSON with the list of results below
            results_product = new WS_GMRSearch.resultFilteredDataBean();
            results_product.gmrFilteredDataBeanList = lstGmrFilteredDataBeanCR_case3;
            String results_product_json_case3 = json.serialize(results_product);
            
            results_family = new WS_GMRSearch.resultFilteredDataBean();
            results_family.gmrFilteredDataBeanList = lstGmrFilteredDataBeanFamily_case3;
            String results_family_json_case3 = json.serialize(results_family);
                
            // Set the parameters on the page
            ApexPages.currentPage().getParameters().put('masterCR',masterCR3);
            ApexPages.currentPage().getParameters().put('selected_products',results_product_json_case3);
            ApexPages.currentPage().getParameters().put('selected_families',null);
        
            // Call the function
            GMRSearchPage.addProductToShoppingCart();
            System.assertEquals(FALSE, GMRSearchPage.set_currCaseListRemovedProducts.contains(GMRSearchPage.map_currCaseExistingProductsTechId2Id.get(objCaseRelatedProduct1.TECH_UniqueId__c)));
            
            // Set the parameters on the page        
            productLineToRemoved = objCaseRelatedProduct2.TECH_UniqueId__c;
            ApexPages.currentPage().getParameters().put('selectedProductRemoved', productLineToRemoved);
            GMRSearchPage.removeLineShoppingCart();
        
            ApexPages.currentPage().getParameters().put('masterCR',masterFamily3);
            ApexPages.currentPage().getParameters().put('selected_products',null);
            ApexPages.currentPage().getParameters().put('selected_families',results_family_json_case3);
            
            // Call the function
            GMRSearchPage.addProductToShoppingCart();         
            System.assertEquals(FALSE, GMRSearchPage.set_currCaseListRemovedProducts.contains(GMRSearchPage.map_currCaseExistingProductsTechId2Id.get(objCaseRelatedProduct2.TECH_UniqueId__c)));
                    
            // Delete another existing product        
            productLineToRemoved = objCaseRelatedProduct1.TECH_UniqueId__c;
            ApexPages.currentPage().getParameters().put('selectedProductRemoved', productLineToRemoved);
            GMRSearchPage.removeLineShoppingCart();
        /** 
            Test : Getters & Setters
        **/
            GMRSearchPage.getSelectedMasterProduct();
            GMRSearchPage.getShoppingCartProducts();
            GMRSearchPage.setSelectedMasterProduct(objCaseRelatedProduct2.TECH_UniqueId__c);
        
        /** 
            Test : Save the shoping cart
        **/
            /* Case 1 : SUCCESS */        
            system.debug('######### GMRSearchPage.set_currCaseListRemovedProducts '+GMRSearchPage.set_currCaseListRemovedProducts.size());
            GMRSearchPage.saveShoppingCart();
            system.assertEquals(FALSE, GMRSearchPage.errorOccurred);
            Integer countProductsOnCase = [SELECT Count() FROM CSE_RelatedProduct__c WHERE Case__c = :objOpenCase.Id];
            System.assertEquals(GMRSearchPage.map_ShoppingCartProducts.values().size(), countProductsOnCase);
            
            /* Case 2 : ERROR : Master Product has been removed */
            ApexPages.currentPage().getParameters().put('selectedProductRemoved', GMRSearchPage.getSelectedMasterProduct());
            GMRSearchPage.removeLineShoppingCart();
            GMRSearchPage.saveShoppingCart();
            system.assertEquals(TRUE, GMRSearchPage.errorOccurred);
            
            /* Case 3 : ERROR : Shopping cart is empty */
            GMRSearchPage.map_ShoppingCartProducts.clear();
            GMRSearchPage.saveShoppingCart();
            system.assertEquals(TRUE, GMRSearchPage.errorOccurred);  
        
        /** 
            Test : Cancel 
        **/
            GMRSearchPage.pageCancelFunction();
        
        /****** @Cecile LARTAUD     December 2014 Release   END *******/
        
        Test.stopTest();
    }
}