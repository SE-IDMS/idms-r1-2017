@isTest
public class VFC_CustomerPortalClsTests {

public static testMethod void VFC_CustomerPortalCls() {
    
//    Set up all conditions for testing.
    PageReference pageRef = Page.VFP_CustomerPortalCls;
     //ApexPages.VFP_CustomerPortalCls pageRef = new ApexPages.VFP_CustomerPortalCls; 
    Test.setCurrentPage(pageRef);
        
    VFC_CustomerPortalCls controller = new VFC_CustomerPortalCls(); 
    PageReference nextPage = controller.casesList();
    
    controller.getIsAuthenticated ();
    
    // Verify that page fails without parameters
       System.assertEquals(null, nextPage);         

    account a = new account(name = 'name');
    Contact c = new Contact(AccountId = a.Id, Workphone__c = '-12345');
    
    controller.setusid(UserInfo.getUserId());
    controller.setcontid(c.id);
    controller.setaccid(a.id); 

    System.assertEquals(UserInfo.getUserId(), controller.getusid());
    System.assertEquals(c.id, controller.getcontid());
    System.assertEquals(a.id, controller.getaccid());

    List<Case> lcases = new List<case>();
    List<Case> lcases2 = new List<case>();
    List<Case> lcases3 = new List<case>();
    
    case cz1 = new case(ContactId=c.id, subject='case1', status='New');
    case cz2 = new case(ContactId=c.id, subject='case2', status='New');
    
    case cz3 = new case(ContactId=c.id, subject='case3', status='Open/ Waiting For Details - Customer');
    case cz4 = new case(ContactId=c.id, subject='case4', status='Not Closed');
    
    case cz5 = new case(ContactId=c.id, subject='case5', status='Closed');
    case cz6 = new case(ContactId=c.id, subject='case6', status='Closed');
    
    lcases.add(cz1); lcases.add(cz2);
    lcases2.add(cz3); lcases2.add(cz4);   
    lcases3.add(cz5); lcases3.add(cz5); 
    
    
    
      /*Id Id1 = '0001';
      String CaseNumber1 = '01010101';
      String Subject1 = 'Ticket de test';
      String Status1 = 'New';
      String CreatedDate1 = null;
      String ClosedDate1  = null;
      controller.ticket ti1 = new controller.ticket(Id1, CaseNumber1, Subject1, status1, CreatedDate1, ClosedDate1); */
      
      
    //case cz2 = new case(ContactId=c.id, subject='case2', status='New');
    
// Testing Open cases    
Test.starttest(); 
    
    controller.setSelectedPcasesView('MOC');
    controller.getSelectedPcasesView(); 
    
    controller.setpcasesview('My Open Cases');    
        
    controller.setCon.setSelected(lcases);
    controller.setCasess(lcases);
    System.assertEquals(controller.casesList(), controller.casesList());   
    System.assertEquals(controller.getTickets(), controller.getTickets()); 

    //Testting closed cases    
    controller.setpcasesview('My Closed Cases');    
    controller.setCasess(lcases2);
    controller.setCon.setSelected(lcases2);
    controller.getTickets();    
    System.assertEquals(controller.casesList(), controller.casesList());
    System.assertEquals(controller.getTickets(), controller.getTickets()); 
        
    // Testting Not closed cases
    controller.setpcasesview('My Open Cases');    
    
    List<SelectOption> s = controller.getpcasesview ();
    //String s = controller.getpcasesview ();
    
    controller.setCasess(lcases3);
    controller.setCon.setSelected(lcases3);  
    System.assertEquals(controller.casesList(), controller.casesList());
    System.assertEquals(controller.getTickets(), controller.getTickets());     

    Test.stoptest(); 
    
}


}