public with sharing class VFC_GetCustomerInformation {	
    public String customerCode{get;set;}{customerCode='';}
    public List<Account> allAccounts{get;set;}{allAccounts=new List<Account>();}//To store Account records from search results
    public Boolean bTestFlag = false;  
    
    //Method to search the Accouts and Contacts for the given phone number
    public PageReference verifyCustomerCode(){
		//*********************************************************************************
        // Method Type : verifyCustomerCode
        // Purpose : To populate the Account(s) based on the Customer Code from CTI URL
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 23rd May 2013
        // Modified by :
        // Date Modified :
        // Remarks : For June - 13 Release
        ///*********************************************************************************/
        try{
            if(ApexPages.currentPage().getParameters().containsKey('customerCode')){ //customerCode passed as parameter on URL
				customerCode=ApexPages.currentPage().getParameters().get('customerCode');
              
				if(Test.isRunningTest() && bTestFlag)
					throw new TestException ();             //used during tests only to achieve coverage
                allAccounts = new List<Account>([Select Id,Name, City__c,StateProvince__c,Country__c,phone,MarketSegment__c,ClassLevel1__c,ClassLevel2__c,Ownerid,Owner.firstName,Owner.LastName from Account where ID in (Select Account__c from LegacyAccount__c where LegacyNumber__c=:customerCode AND LegacyName__c LIKE 'CN_%')]);
                   
                String SfUrl = URL.getSalesforceBaseUrl().toExternalForm();
                
				if(allAccounts.size()==0){
				   ApexPages.Message objMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CL10080);
				   ApexPages.addMessage(objMsg);
					customerCode = EncodingUtil.urlEncode(customerCode, 'UTF-8');
					PageReference objPageReference=new PageReference(SfUrl + Label.CLJAN13CCC01 +customerCode);
					objPageReference.setRedirect(true);
					return objPageReference;
				}
				else if(allAccounts.size()==1){ //if only one Account found for the customer Code
					PageReference objPageReference=new PageReference(SfUrl +'/'+allAccounts[0].id);
					objPageReference.setRedirect(true);
					return objPageReference;
				}
				else if(allAccounts.size() > 1){//if more than one Accounts found for the customer Code
					return null;
				}
				else // Sceanrio will never happen
					return null;
			}
            else{   //Throwing error if customer Code is null
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CL10082);
                ApexPages.addMessage(myMsg);
                return null;
            }    
        }
		catch(Exception e){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Following unexpected error occured. Please contact Adminstrator : '+e.getMessage());
			ApexPages.addMessage(myMsg);
			return null;
		}
    }//End of verifycustomerCode()
    
    public class TestException extends Exception {}
}//End of Class