/*
 * @author      Marco Paulucci
 * @date        16.03.2015
 * @description Test Class
 */
@isTest
public with sharing class Fielo_ManageSegmentsControllerTest {

    private static testMethod void unit () {
        
        FieloEE.MockUpFactory.setCustomProperties(false);

        // create initial object
        FieloEE__Banner__c banner = new FieloEE__Banner__c(Name = 'My Banner');
        insert banner;
        ApexPages.currentPage().getParameters().put('Id', banner.Id);

        // create segments
        List<FieloEE__RedemptionRule__c> rr = new List<FieloEE__RedemptionRule__c>();
        for(Integer i = 0; i < 50; i++) {
           rr.add(new FieloEE__RedemptionRule__c(Name = 'Test' + String.valueOf(i)));
        }
        insert rr;

        // create object-related custom setting
        insert new Fielo_SegmentManager__c(
            Name = 'FieloEE__Banner__c',
            F_LookupField__c = 'FieloEE__Banner__c'
        );
        
        // create global-related custom setting
        insert new Fielo_SegmentManagerSettings__c(
            F_PagerSize__c = 10,
            F_Fieldset__c = 'Fielo_SegmentManager'
        );
        
        ApexPages.currentPage().getParameters().put('index', '5');
        
        system.test.startTest();
        
            Fielo_ManageSegmentsController MS = new Fielo_ManageSegmentsController();
            
            MS.toSearch = 'Nothing will be found from this text string';
            MS.doSearch();
            
            MS.toSearch = 'Te';
            MS.doSearch();
            
            for (Integer i = 0; i < MS.segmentsNot.size(); i++) {
                MS.segmentsNot[i].checked = math.mod(i, 2) == 0;
            }
            
            MS.doNext();
            
            for (Integer i = 0; i < MS.segmentsNot.size(); i++) {
                MS.segmentsNot[i].checked = math.mod(i, 2) == 0;
            }
            
            MS.doBack();
            
            MS.doPage();
            MS.segmentsNot[0].checked = true;
            
            MS.doAdd();
            
            MS.doQuickSave();
            
            for (Integer i = 0; i < MS.segmentsYes.size(); i++) {
                MS.segmentsYes[i].checked = math.mod(i, 2) == 0;
            }
            
            MS.doQuickSave();
            
            MS.segmentsNot[0].checked = true;
            
            MS.doNext();
            
            MS.toSearch = 'st';
            MS.doSearch();
            MS.doSearch();
            
            MS.doAddAll();
            
            MS.getPages();
            MS.getObjectName();
            
            MS.doSave();
            
            String bId = banner.Id;
            Fielo_ManageSegmentsController.generateSegmentURL('.com/' + bId);
            Fielo_ManageSegmentsController.generateSegmentURL('testURL.com/?randomparam=test&id=' + bId);
            Fielo_ManageSegmentsController.generateSegmentURLacion('.com/' + bId);
            Fielo_ManageSegmentsController.generateSegmentURLacion('testURL.com/?randomparam=test&id=' + bId);
            
        system.test.stopTest();
    }
    
}