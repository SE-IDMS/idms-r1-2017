/****************************************************
 Class       : VFC_SRV_MYFSDocumentController
 Author      : Dwarak S
 Description : Handle Document Display (Contracts, interventions, etc.).
****************************************************/ 
Public without sharing Class VFC_SRV_MYFSDocumentController{
    Public Map<string,string> selectoptionMap{get;set;}
    Public String SlectedValue{get;set;}
    Public String UltimateParentAccount {get;set;}
    Public String SelectedCountry {get;set;}
    Public list<SVMXC__Service_Order__c> WOList{get;set;}
    Public String Site{get;set;}
    Public String FromDate{get;set;}
    Public string ToDate{get;set;}
    public Integer size {get; set;}
    Public List<SVMXC__Service_Contract__c> ExpContractlst{get;set;}
    Public list<SVMXC__Service_Contract__c> ActiveContracts{get;set;}
    Public List<SVMXC__Service_Contract__c> Contractslst{get;set;}
    Public map<id,string> locaccountmap{get;set;}
    Public Date mydate{get;set;}
    Public integer Totalsize{get;set;} 
    Public String ContractType{get;set;}
    Public list<Attachment> attachmentList{get;set;}
    Public Map<String,SVMXC__Service_Order__c> woMap{get;set;}
    Public Map<String,List<Attachment>> woAttachmentMap{get;set;}
    Public List<String> woIdList{get;set;}
    Public Integer Size1{get;set;}
    Public String cccPhoneNo{get;set;}
    Public Map<String,String> productTypeMap{get;set;}
    //Public Map<String,String> interestTypeMap{get;set;}
    //Public Map<String,String> typeMap{get;set;}
    Public String productType{get;set;}
    Public Map<String,String> serviceInterestMap{get;set;}
    Public Map<String,String> productInterestMap{get;set;}
    Public String requestType{get;set;}
    Public String description{get;set;}
    Public Map<String,String> tempServiceInterestMap{get;set;}
    Public Map<String,String> tempProductInterestMap{get;set;}
    Public String quoteSite{get;set;}
    Public String currentUserLanguage{get;set;}
    Public Boolean isMoreSites{get;set;}
    Public String UserId{get;set;}
    Public set<id> sitesIdSet{get;set;}
     Public User usrInfo{get;set;}
 
    Public VFC_SRV_MYFSDocumentController(){
       
        selectoptionMap= new map<string,string>();
        locaccountmap = new map<id,string>();
        selectoptionMap.put(System.label.CLSRVMYFS_ViewAll,System.label.CLSRVMYFS_ViewAll);
        UltimateParentAccount = Apexpages.currentpage().getparameters().get('Params');
        SelectedCountry = Apexpages.currentpage().getparameters().get('country');
        SlectedValue = Apexpages.currentpage().getparameters().get('Site');
        DateTime TodayDate = system.now();
        list<SVMXC__Site__c> lstSites = new list<SVMXC__Site__c>();
         currentUserLanguage = UserInfo.getLanguage();
        //@TODO: add in the where clause a filter based on the ServiceRole table to avoid hacking. Otherwise, a user could write a URL and pass parameters that will give access to data that are normally not available
        /*
list<SVMXC__Site__c> lstSites = [Select id,name,SVMXC__Account__c,SVMXC__Account__r.Country__r.name,SVMXC__Account__r.UltiParentAcc__c from SVMXC__Site__c where
(SVMXC__Account__r.UltiParentAcc__c =:UltimateParentAccount OR SVMXC__Account__C =:UltimateParentAccount ) and PrimaryLocation__c = True and SVMXC__Account__r.Country__r.CountryCode__c=:SelectedCountry ORDER BY name Asc];
*/
        
        UserId = Apexpages.currentpage().getparameters().get('UId');
        if(userId==null || userId ==''){
         userId =UserInfo.getUserId();
         
        }
      
        String currContactId = AP_SRV_myFSUtils.getContactId(userId);
        Id serviceRoleContactRecordTypeId = AP_SRV_myFSUtils.getServiceRoleContactRecordTypeId();
        System.debug('currContactId: '+currContactId);
        System.debug('serviceRoleContactRecordTypeId: '+serviceRoleContactRecordTypeId);
        
        if(!Test.isRunningTest()){
           lstSites = AP_SRV_myFSUtils.getAuthorizedSitesForUser(userId, SelectedCountry, UltimateParentAccount);
        }
        else{
          lstSites = [Select id,name,SVMXC__Account__c,SVMXC__Account__r.Country__r.name,SVMXC__Account__r.UltiParentAcc__c from SVMXC__Site__c where       
                      (SVMXC__Account__r.UltiParentAcc__c =:UltimateParentAccount OR SVMXC__Account__C =:UltimateParentAccount ) and PrimaryLocation__c = True and SVMXC__Account__r.Country__r.CountryCode__c=:SelectedCountry ORDER BY name Asc];
        }
        System.debug('Filtered lstSites.size(): '+lstSites.size());
        System.debug('Filtered lstSites: '+lstSites);
        
        for(SVMXC__Site__c Sites :lstSites){
            selectoptionMap.put(sites.Id,sites.name);
            locaccountmap.put(sites.Id,sites.SVMXC__Account__c);
        }
        if(selectoptionMap != null && selectoptionMap.size() > 2 ){
            isMoreSites = true;
        }else{
            selectoptionMap.remove(System.label.CLSRVMYFS_ViewAll);
        }
        size=20; 
        size1=3; 
        Site = Apexpages.currentpage().getparameters().get('Site'); 
        System.debug('Current site: '+Site);
        if(site == null){
            site=System.label.CLSRVMYFS_ViewAll;
        }
        Datetime  today = date.today();
        mydate= date.valueof(today).adddays(+120);
        cccPhoneNo = AP_SRV_myFSUtils.getCountryPhoneNumber(SelectedCountry);
       
    }
   Public Void ContractsandInterventions(){       
        Set<String> parentIdSet = new Set<String>();         
        ContractType =System.Label.CLSRVMYFS_ACTIVE;
        Datetime  today = date.today();
        mydate= date.valueof(today).adddays(+120);
        Date todayDate = date.valueof(today);
        WOList = new list<SVMXC__Service_Order__c>();  
        woMap = new Map<String,SVMXC__Service_Order__c>(); 
        woAttachmentMap = new Map<String, List<Attachment>>(); 
        woIdList = new List<String>();
        Set<Id> SitesIdSet = new Set<Id>();
        attachmentList = new List<attachment>();  
        DateTime TodayDateTime = system.now();
        Integer Expiredsize1=1;
        ExpContractlst = new List<SVMXC__Service_Contract__c>();
        DateTime fromdateFormat;
        DateTime todateFormat;
        Date fromdateFormat1;
        Date todateFormat1;
        
        
        if(site==System.label.CLSRVMYFS_ViewAll){
             //  WOList = [select id,name,createddate,SVMXC__Scheduled_Date_Time__c,SVMXC__Order_Status__c,SVMXC__Site__r.name,SVMXC__Scheduled_Date__c,SVMXC__Problem_Description__c,SubStatus__c,SVMXC__Order_Type__c,Technical_Approval__c,RecommendationsToClient__c,(SELECT Id, Name,CreatedDate,ParentId, LastModifiedDate FROM Attachments where Name like '%.pdf%' and (Name like :system.label.CLSRVMYFS_SRVINTERVENTION or Description =:system.label.CLSRVMYFS_WODescription)) from SVMXC__Service_Order__c  where SVMXC__Site__c =:selectoptionMap.keyset() and (SVMXC__Company__r.UltiParentAcc__c =:UltimateParentAccount OR SVMXC__Company__C=:UltimateParentAccount) and (SVMXC__Order_Status__c =:System.label.CLSRVMYFS_ServiceDelivered or SVMXC__Order_Status__c =:system.label.CLSRVMYFS_SERVICECOMPLETE or SVMXC__Order_Status__c = :system.label.CLSRVMYFS_SERVICEVALIDATED) and SVMXC__Scheduled_Date_Time__c <:TodayDate ORDER BY SVMXC__Scheduled_Date_Time__c DESC NULLS LAST]; 
             //WOList = AP_SRV_myFSUtils.getPastInterventionReports(selectoptionMap.keyset(),ultimateParentAccount,todayDateTime);
             WOList = AP_SRV_myFSUtils.getAuthorizedInterventionReportsforUser(userId, SelectedCountry, UltimateParentAccount,todayDateTime,fromdateFormat,todateFormat,SitesIdSet);
             processInterventionReports();
             ExpContractlst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateFormat1,todateFormat1,Expiredsize1);
             ActiveContracts = AP_SRV_myFSUtils.getAuthorizedActiveContractsforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateFormat1,todateFormat1,size1,sitesIdSet);
             System.debug('Active Contracts'+ActiveContracts);
             System.debug('Active Contracts'+ExpContractlst );
         } 
         else{
             
            // WOList = [select id,name,createddate,SVMXC__Scheduled_Date_Time__c,SVMXC__Order_Status__c,SVMXC__Site__r.name,SVMXC__Scheduled_Date__c,SVMXC__Problem_Description__c,SubStatus__c,SVMXC__Order_Type__c,Technical_Approval__c,RecommendationsToClient__c,(SELECT Id, Name,CreatedDate,ParentId, LastModifiedDate FROM Attachments where Name like '%.pdf%' and (Name like :system.label.CLSRVMYFS_SRVINTERVENTION or Description =:system.label.CLSRVMYFS_WODescription)) from SVMXC__Service_Order__c  where SVMXC__Site__c =:site and (SVMXC__Company__r.UltiParentAcc__c =:UltimateParentAccount OR SVMXC__Company__C=:UltimateParentAccount) and (SVMXC__Order_Status__c =:System.label.CLSRVMYFS_ServiceDelivered or SVMXC__Order_Status__c =:system.label.CLSRVMYFS_SERVICECOMPLETE or SVMXC__Order_Status__c = :system.label.CLSRVMYFS_SERVICEVALIDATED) and SVMXC__Scheduled_Date_Time__c <:TodayDate ORDER BY SVMXC__Scheduled_Date_Time__c DESC NULLS LAST];
            Set<String> singleSiteSet = new Set<String>();
            singleSiteSet.add(site);
            // WOList = AP_SRV_myFSUtils.getPastInterventionReports(singleSiteSet,ultimateParentAccount,todayDateTime);
            WOList = AP_SRV_myFSUtils.getAuthorizedInterventionReportsforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDateTime,fromdateFormat,todateFormat,sitesIdSet); 
            processInterventionReports();
            ExpContractlst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateFormat1,todateFormat1,Expiredsize1);
            ActiveContracts = AP_SRV_myFSUtils.getAuthorizedActiveContractsforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateFormat1,todateFormat1,size1,sitesIdSet);
            System.debug('Active Contracts'+ActiveContracts);
            System.debug('Active Contracts'+ExpContractlst );
         }
    }
    //Called on chanage of location picklist and retrives WO
    Public Void LocationPopulate(){
        size1= 20; 
        WOList = new list<SVMXC__Service_Order__c>();
        Set<String> parentIdSet = new Set<String>();  
        woMap = new Map<String,SVMXC__Service_Order__c>(); 
        woAttachmentMap = new Map<String, List<Attachment>>(); 
        woIdList = new List<String>();
        attachmentList = new List<attachment>();  
        DateTime TodayDateTime = system.now();
        ContractType =System.Label.CLSRVMYFS_ACTIVE;
        system.debug('####ContractType####'+ContractType);
        Datetime  today = date.today();
        mydate= date.valueof(today).adddays(+120);
        Date todayDate = date.valueof(today);
        Date fromdateFormat1;
        Date todateFormat1;
        Set<Id> sitesIdSet = new Set<Id>();
        
        if(site==System.label.CLSRVMYFS_ViewAll){
           
            if(ContractType==System.Label.CLSRVMYFS_EXPIREDCON){
                
                Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateFormat1,todateFormat1,size1);
                Totalsize = AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateFormat1,todateFormat1);
                system.debug('##Expired TotalRecords##'+TotalSize );
            }
            else{
                 Contractslst = AP_SRV_myFSUtils.getAuthorizedActiveContractsforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateFormat1,todateFormat1,size1,sitesIdSet);
                 Totalsize = AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateFormat1,todateFormat1);
                 system.debug('##Size From Page##'+Size);
            }
        }
        else{
            
           
            if(ContractType==System.Label.CLSRVMYFS_EXPIREDCON){
               Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateFormat1,todateFormat1,size1);
               Totalsize =    AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateFormat1,todateFormat1);
              
            }
            else{
               
               Contractslst  = AP_SRV_myFSUtils.getAuthorizedActiveContractsforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateFormat1,todateFormat1,size1,sitesIdSet);
               Totalsize = AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateFormat1,todateFormat1);
               
                             
           }
       }
    }
     
    Public Void expiredcontracts(){
        ContractType =System.Label.CLSRVMYFS_EXPIREDCON;
        DateTime TodayDateTime = system.now();
        system.debug('+++++++site++++'+site);
        system.debug('+++++++contract++++'+contracttype);
        Datetime  today = date.today();
        mydate= date.valueof(today).adddays(+120);
        Date todayDate = date.valueof(today);
        Date fromdateFormat;
        Date todateFormat;
        integer size = 20;
        if(site==System.label.CLSRVMYFS_ViewAll){
              Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforUser(userId, SelectedCountry, UltimateParentAccount,todayDate ,fromdateFormat,todateFormat,size);
              Totalsize = AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforUser(userId, SelectedCountry, UltimateParentAccount,todayDate ,fromdateFormat,todateFormat);
                                  
        }
        else{
             
            Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforAccount(userId, SelectedCountry, UltimateParentAccount, locaccountmap.get(site),todayDate ,fromdateFormat,todateFormat,size);
            Totalsize =    AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforAccount(userId, SelectedCountry, UltimateParentAccount, locaccountmap.get(site),todayDate ,fromdateFormat,todateFormat);
        }
    }
    public void next(){
          System.debug('size'+size);
        this.size+=20;
        System.debug('thissize'+size);
        if((fromdate!=null && fromdate!='') && (todate!=null && todate!='')){
            FromDateSelector();
            system.debug('called fromdate selector');
        }
        else if(fromdate!=null && fromdate!='' ){
            FromDateSelector();
            system.debug('called fromdatesecltormethod');
        }
        else if(todate!=null && todate!=''){
            ToDateSelector();
            system.debug('called todateselector');
        }
        else  if(ContractType!= null && ContractType== 'Expired'){
            
             expiredcontracts();
             System.debug('####Expired####'+ContractType );
        }  
        else{
            locationpopulate();
            System.debug('####Active####'+ContractType );
            system.debug('location called');
        }
        
    }
    Public void FromDateSelector(){
        Set<Id> sitesIdSet  = new Set<Id>();
        system.debug('todate'+todate);
        date fromdateformate;
        date todateformate;
        system.debug('fromdate'+fromdate);
        if(fromdate!=''){
            fromdateformate= date.valueof(fromdate);
        }
        system.debug('Fromdateformate'+fromdateformate);
        
        if(todate!=''){
            todateformate= date.valueof(todate);  
        }   
        
        DateTime TodayDateTime = system.now();
        
        Datetime  today = date.today();
        
        Date todayDate = date.valueof(today);
        
        if(site==System.label.CLSRVMYFS_ViewAll){
           
            if(fromdateformate!=null&& todateformate==null){
                system.debug('++++contracttype++++'+contracttype);
                if(ContractType!=null && ContractType=='Expired' ){
                   
                   
                    Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforUser(userId, SelectedCountry,UltimateParentAccount,todayDate,fromdateformate,todateformate,size);
                    System.debug('###FromDateFilter###'+Contractslst);
                    Totalsize = AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforUser(userId, SelectedCountry,UltimateParentAccount,todayDate,fromdateformate,todateformate);
                    
                }
                else{
                  
                    Contractslst = AP_SRV_myFSUtils.getAuthorizedActiveContractsforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateformate,todateformate,size,sitesIdSet);
                    Totalsize = AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateformate,todateformate);
                                     
                } 
            }
            else{
                if(fromdateformate!=null && todateformate!=null){
                    if(ContractType!=null && ContractType=='Expired' ){
                         Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforUser(userId, SelectedCountry,UltimateParentAccount,todayDate,fromdateformate,todateformate,size);
                         Totalsize = AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforUser(userId, SelectedCountry,UltimateParentAccount,todayDate,fromdateformate,todateformate);
                    
                    }
                    else{
                    
                        Contractslst = AP_SRV_myFSUtils.getAuthorizedActiveContractsforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateformate,todateformate,size,sitesIdSet);
                        Totalsize =AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateformate,todateformate);
                    }
                
                }
            }
        }
        else{
            if(fromdateformate!=null&& todateformate==null){
               if(ContractType!=null && ContractType=='Expired' ){
                   
                    Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforAccount(userId, SelectedCountry,UltimateParentAccount, locaccountmap.get(site),todayDate,fromdateformate,todateformate,size);
                    System.debug('###FromDateFilter###'+Contractslst);
                    Totalsize = AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforAccount(userId, SelectedCountry,UltimateParentAccount, locaccountmap.get(site),todayDate,fromdateformate,todateformate); 
                   
                }
                else{
                   
                   Contractslst = AP_SRV_myFSUtils.getAuthorizedActiveContractsforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateformate,todateformate,size,sitesIdSet);
                   Totalsize = AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateformate,todateformate);
                                     
                } 
            }
            else{
                if(fromdateformate!=null && todateformate!=null){
                    if(ContractType!=null && ContractType=='Expired' ){
                        
                    Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforAccount(userId, SelectedCountry,UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateformate,todateformate,size);
                    Totalsize = AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforAccount(userId, SelectedCountry,UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateformate,todateformate);
                    }
                    
                }
                else{
                    
                   Contractslst = AP_SRV_myFSUtils.getAuthorizedActiveContractsforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateformate,todateformate,size,sitesIdSet);
                   Totalsize =  AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateformate,todateformate);
                   
                }
            }
        }  
    }
    Public void ToDateSelector(){
        Set<Id> sitesIdSet = new Set<Id>();
        set<string> CompletedStatus = new set<String>();
        set<string> Confirmstatus= new set<String>();
        system.debug('todate'+todate);
        date fromdateformate;
        date todateformate;
        system.debug('fromdate'+fromdate);
        if(fromdate!=''){
            fromdateformate= date.valueof(fromdate);
        }
        system.debug('Fromdateformate'+fromdateformate);
        
        if(todate!=''){
            todateformate= date.valueof(todate); 
            system.debug('todateformate'+todateformate); 
        }   
        datetime TodayDateTime = system.now();
        
        Datetime  today = date.today();
        
        Date todayDate = date.valueof(today);
        if(site==system.label.CLSRVMYFS_ViewAll){
            system.debug('+++++ContractType+++++'+ContractType);

            if(fromdateformate==null&& todateformate!=null){
                if(ContractType!=null && ContractType=='Expired' ){
                        
                   system.debug('todatefromdateCalled'); 
                   Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforUser(userId, SelectedCountry,UltimateParentAccount,todayDate,fromdateformate,todateformate,size);
                   TotalSize = AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforUser(userId, SelectedCountry,UltimateParentAccount,todayDate,fromdateformate,todateformate);

                    
                }
                else{
                   system.debug('todatefromdateCalled');  
                   Contractslst = AP_SRV_myFSUtils.getAuthorizedActiveContractsforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateformate,todateformate,size,sitesIdSet);
                   TotalSize = AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateformate,todateformate);
                }
            }  
            else{
                if(fromdateformate!=null && todateformate!=null){
                    if(ContractType!=null && ContractType=='Expired' ){
                            
                        system.debug('todatefromdateCalled'); 
                         Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateformate,todateformate,size);
                        TotalSize =  AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateformate,todateformate);
                        system.debug('Contractslst'+Contractslst ); 
                        
                    }
                
                    else{
                    
                       system.debug('todatefromdateCalled'); 
                       Contractslst = AP_SRV_myFSUtils.getAuthorizedActiveContractsforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateformate,todateformate,size,sitesIdSet);
                       TotalSize =AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforUser(userId, SelectedCountry, UltimateParentAccount,todayDate,fromdateformate,todateformate);
                       system.debug('Contractslst'+Contractslst ); 
                    }
                
                }
            }
        }
        else{
            if(fromdateformate==null&& todateformate!=null){
                if(ContractType!=null && ContractType=='Expired' ){
                   Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforAccount(userId, SelectedCountry,UltimateParentAccount, locaccountmap.get(site),todayDate,fromdateformate,todateformate,size);
                   TotalSize = AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforAccount(userId, SelectedCountry,UltimateParentAccount, locaccountmap.get(site),todayDate,fromdateformate,todateformate);
                                     
                }
                else{
                 Contractslst = AP_SRV_myFSUtils.getAuthorizedActiveContractsforAccount(userId, SelectedCountry,UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateformate,todateformate,size,sitesIdSet); 
                 TotalSize = AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforAccount(userId, SelectedCountry,UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateformate,todateformate); 
                }
            }
            else{
                if(fromdateformate!=null && todateformate!=null){
                    if(ContractType!=null && ContractType=='Expired' ){
                            
                       Contractslst = AP_SRV_myFSUtils.getAuthorizedExpiredContractsforAccount(userId, SelectedCountry,UltimateParentAccount, locaccountmap.get(site),todayDate,fromdateformate,todateformate,size);
                       TotalSize = AP_SRV_myFSUtils.getAuthorizedExpiredContractsCountforAccount(userId, SelectedCountry,UltimateParentAccount, locaccountmap.get(site),todayDate,fromdateformate,todateformate);
                    }
                
                    else{
                    
                       Contractslst = AP_SRV_myFSUtils.getAuthorizedActiveContractsforAccount(userId, SelectedCountry,UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateformate,todateformate,size,SitesIdSet);
                       TotalSize = AP_SRV_myFSUtils.getAuthorizedActiveContractsCountforAccount(userId, SelectedCountry,UltimateParentAccount,locaccountmap.get(site),todayDate,fromdateformate,todateformate);
                        
                    }
                
                }
            }
        }
    }
    
    private void debug(String aMessage) {
        System.debug('#### ' + aMessage);
    }
    
    Public void processInterventionReports(){
           if(WOList != null && !Wolist.isempty()){
               if(!Test.isRunningTest()){ 
                  AP_SRV_myFSUtils.WorkorderSharing(WOList,userId);
                }
                for(SVMXC__Service_Order__c eachWO : WOList){
                    if(eachWO.TECH_CustomerScheduledDateTimeECH__c == null && eachWO.CustomerTimeZone__c != null && eachWO.SVMXC__Scheduled_Date_Time__c != null){
                        eachWO.TECH_CustomerScheduledDateTimeECH__c= eachWO.SVMXC__Scheduled_Date_Time__c.format('dd MMM yyyy HH:mm:ss', eachWO.CustomerTimeZone__c);
                    }   
                    List<Attachment> listOfAttachment = new List<Attachment>();
                    if(eachWO.Attachments != null && !(eachWO.Attachments).isEmpty()){
                        woIdList.add(eachWO.Id);
                        woMap.put(eachWO.Id,eachWO);
                        for(Attachment eachAttachment :eachWO.Attachments){
                            attachmentList.add(eachAttachment);
                            listOfAttachment.add(eachAttachment);
                            
                            if(attachmentList != null && attachmentList.size() == 10){
                              break;
                            }
                        }
                        woAttachmentMap.put(eachWO.Id,listOfAttachment);
                        if(attachmentList != null && attachmentList.size() == 10){
                        break;
                        }
                     }
                  }
            }        
    }
    
    /* Quote code starts here */
 public void getProductTypeList() {       

 
        productTypeMap = new Map<String,String>();
        // Find all the product types in the custom setting

        Map<String, CSSRVMYFS_Product_Type__c> productTypes = CSSRVMYFS_Product_Type__c.getAll();         


        List<String> producTypeNames = new List<String>();

        producTypeNames.addAll(productTypes.keySet());       

         

        for (String productTypeName : producTypeNames) {

            CSSRVMYFS_Product_Type__c productType = productTypes.get(productTypeName);
         // if(currentUserLanguage != null && (currentUserLanguage == 'EN' || currentUserLanguage.startsWithIgnoreCase('en_'))){

            productTypeMap.put(productType.Product_Type_Code__c, productType.Product_Type_Value__c);
          // }else{
          //   Sobject sobj = (Sobject)productType;
          //  String temp = currentUserLanguage + '_Product_Type_Value__c';
              // productTypeMap.put(productType.Product_Type_Code__c, productType.FR_Product_Type_Value__c);
          //    productTypeMap.put(productType.Product_Type_Code__c, String.valueOf(sobj.get(temp)));
 
          //  }
        }

    }
   

 Public void getInterestTypes(){
     serviceInterestMap = new Map<String,String>();
     productInterestMap = new Map<String,String>();
     tempServiceInterestMap = new Map<String,String>();
     tempProductInterestMap = new Map<String,String>();
     Map<String, CSSRVMYFS_INTEREST_TYPES__c> allInterestServiceTypes = CSSRVMYFS_INTEREST_TYPES__c.getAll();
   Map<String, CSSRVMYFS_INTEREST_TYPES__c> interestServiceTypes = new Map<String, CSSRVMYFS_INTEREST_TYPES__c>();
   for(CSSRVMYFS_INTEREST_TYPES__c interestServiceType : allInterestServiceTypes.values()) {

            if (interestServiceType.Request_Type_Code__c == 'Service (' + this.productType + ')' ) {

                interestServiceTypes.put(interestServiceType.name, interestServiceType);

            } 

        }
    List<String> interestServiceTypeNames = new List<String>();

        interestServiceTypeNames.addAll(interestServiceTypes.keySet());
        for (String requestTypeName : interestServiceTypeNames) {

            CSSRVMYFS_INTEREST_TYPES__c interestServiceType = interestServiceTypes.get(requestTypeName);
          //   if(currentUserLanguage != null && (currentUserLanguage == 'EN' || currentUserLanguage.startsWithIgnoreCase('en_'))){
            serviceInterestMap.put(interestServiceType.Interest_Code__c, interestServiceType.Interest_Value__c);
          //   }else{
           //      Sobject sobj = (Sobject)interestServiceType;
            //String tempVal = currentUserLanguage + '_Interest_Value__c';
              // productTypeMap.put(productType.Product_Type_Code__c, productType.FR_Product_Type_Value__c);
          //    serviceInterestMap.put(interestServiceType.Interest_Code__c, String.valueOf(sobj.get(tempVal)));
         //   }
            tempServiceInterestMap.put(interestServiceType.Interest_Code__c,'Service');

        }

   Map<String, CSSRVMYFS_INTEREST_TYPES__c> allInterestProductTypes = CSSRVMYFS_INTEREST_TYPES__c.getAll();
   Map<String, CSSRVMYFS_INTEREST_TYPES__c> interestProductTypes = new Map<String, CSSRVMYFS_INTEREST_TYPES__c>();
   for(CSSRVMYFS_INTEREST_TYPES__c interestProductType : allInterestProductTypes.values()) {

            if (interestProductType.Request_Type_Code__c == 'Product ('+ this.productType+ ')') {

                interestProductTypes.put(interestProductType.name, interestProductType);

            }

        }
    List<String> interestProductTypeNames = new List<String>();

        interestProductTypeNames.addAll(interestProductTypes.keySet());
        for (String requestTypeName : interestProductTypeNames) {

            CSSRVMYFS_INTEREST_TYPES__c interestProductType = interestProductTypes.get(requestTypeName);
           // if(currentUserLanguage != null && (currentUserLanguage == 'EN' || currentUserLanguage.startsWithIgnoreCase('en_'))){
            productInterestMap.put(interestProductType.Interest_Code__c, interestProductType.Interest_Value__c);
           // }else{
           //  Sobject sobj = (Sobject)interestProductType;
        //    String tempVal = currentUserLanguage + '_Interest_Value__c';
              // productTypeMap.put(productType.Product_Type_Code__c, productType.FR_Product_Type_Value__c);
         //    productInterestMap.put(interestProductType.Interest_Code__c, String.valueOf(sobj.get(tempVal)));
         //   }
            tempProductInterestMap.put(interestProductType.Interest_Code__c,'Product');

        }   

        
 }  
 Public void insertOpportunityNotification(){
 Savepoint sp = Database.setSavepoint();
try{
    List<SVMXC__Site__c> siteList = new List<SVMXC__Site__c>();
     System.debug('site'+site);
    System.debug('quoteSite'+quoteSite);
    if(quoteSite == null || quoteSite == ''){
    siteList = [select name from SVMXC__Site__c where Id = : site];
    }else{
    siteList = [select name from SVMXC__Site__c where Id = : quoteSite];
    }    
    String siteName = '';
    if(siteList != null && !siteList.isEmpty())
    siteName = siteList[0].name;
     Map<String,String> bfsProductTypeMap = new Map<String,String>();
     Schema.DescribeFieldResult buResult = InterestOnOpportunityNotif__c.Leading_Business_BU__c.getDescribe();
     List<Schema.PicklistEntry> plEntry = buResult.getPicklistValues();
        
   for( Schema.PicklistEntry f : plEntry)
   {
      bfsProductTypeMap.put(f.getValue(),f.getLabel());
   } 
    RecordType opptyNotificationRecordType = [SELECT Id from RecordType where SobjectType = 'OpportunityNotification__c' and developerName = 'Mobile'limit 1];
    List<Account> accList = [select name,Country__c from Account where Id = : UltimateParentAccount];
    String accountName = accList[0].name;
     String opportunityNotificationName = System.label.CLSRVMYFS_OPPORTUNITY_NOTIFICATION_NAME +  siteName + '-' +accountName + '-' + bfsProductTypeMap.get(productType);
    String userCurrencyCode = UserInfo.getDefaultCurrency(); 
    OpportunityNotification__c opptyNotification = new OpportunityNotification__c();
    if(opportunityNotificationName != null && opportunityNotificationName.length() > 80){
       opptyNotification.Name=opportunityNotificationName.subString(0,79);
       }
     else{
     opptyNotification.Name=opportunityNotificationName;
     }  
    opptyNotification.Status__c = System.label.CLSRVMYFS_OPPTY_NOTIFICATION_STATUS;
    opptyNotification.Amount__c = 0;
    opptyNotification.CurrencyIsoCode = userCurrencyCode;
    opptyNotification.AccountForConversion__c = UltimateParentAccount;
    opptyNotification.LeadingBusinessBU__c = productType;
    opptyNotification.CountryDestination__c = accList[0].Country__c;
    opptyNotification.Description__c = description;
    //opptyNotification.Stage__c = 
    Date  today = date.today();
    Date calculatedDate= date.valueof(today).adddays(+30);
    opptyNotification.CloseDate__c=calculatedDate;
     opptyNotification.IncludedinForecast__c = System.label.CLSRVMYFS_OPPTY_NOTIFICATION_INCL_FORECAST;
     opptyNotification.OpportunityCategory__c = System.label.CLSRVMYFS_OPPTY_NOTIFICATION_CATEGORY;
     opptyNotification.OpportunityType__c = System.label.CLSRVMYFS_OPPTY_NOTIFICATION_TYPE;
     opptyNotification.OpportunityDetector__c = userId;
     opptyNotification.SVMXPriority__c = System.label.CLSRVMYFS_OPPTY_NOTIFICATION_PRIORITY;
    opptyNotification.From_Customer_Portal__c = true;
     opptyNotification.recordTypeId = opptyNotificationRecordType.id;
    insert opptyNotification;
    if(opptyNotification != null && opptyNotification.Id != null){
    InterestOnOpportunityNotif__c interestObj = new InterestOnOpportunityNotif__c();
    interestObj.Leading_Business_BU__c = productType;
    String interestType ='';
    Map<String,String> sfProductTypeMap = new Map<String,String>();
 Schema.DescribeFieldResult fieldResult = InterestOnOpportunityNotif__c.Leading_Business_BU__c.getDescribe();
 List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
   for( Schema.PicklistEntry f : ple)
   {
      sfProductTypeMap.put(f.getValue(),f.getLabel());
   }      
    Map<String, CSSRVMYFS_Request_Types__c> allRequestTypes = CSSRVMYFS_Request_Types__c.getAll();
    Map<String, String> requestTypesMap = new Map<String, String>();
    for(CSSRVMYFS_Request_Types__c requestTypeRecord : allRequestTypes.values()) {          
    
    requestTypesMap.put(requestTypeRecord.Request_Type_Code__c, requestTypeRecord.Request_Type_Value__c);                

    } 
    String key = '';
    if(tempServiceInterestMap != null && tempServiceInterestMap.containsKey(requestType)){
     key =  tempServiceInterestMap.get(requestType) + ' (' + productType + ')' ;
    }else if(tempProductInterestMap != null && tempProductInterestMap.containsKey(requestType)){
     key =  tempProductInterestMap.get(requestType) + ' (' + productType + ')' ;
    }   
    System.debug('key'+key);                
    if(requestType != null && productType != null && requestTypesMap != null && key != null && key !='' && requestTypesMap.containsKey(key) ){
        interestType = requestTypesMap.get(key); 
    }
      System.debug('interestType'+interestType);
    interestObj.Type__c = interestType;
    interestObj.Interest__c = requestType;
    interestObj.OpportunityNotification__c = opptyNotification.Id;
    insert interestObj;
    }
    }catch(DMLException excep){
        apexpages.addmessages(excep);
        Database.rollback(sp);

    }
}
}