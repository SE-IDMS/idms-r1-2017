//Added for May2013 release by Harikrishna Singara to override the new button and populate the user id and user BU

public with sharing class VFC_OppNotificationInter {
    
    // current page parameters
    public final Map<String,String> pageParameters = ApexPages.currentPage().getParameters();

    public VFC_OppNotificationInter(ApexPages.StandardController controller) 
    {
       
    }
    public PageReference Init()
    {
        User u=[select id, name,UserBusinessUnit__c from user where id=:UserInfo.getUserId()];
        String url='/a2h/e';
        String WOName = System.currentpagereference().getParameters().get(Label.CLMAY13SRV17);
        String WOId = System.currentpagereference().getParameters().get(Label.CLMAY13SRV18);
        PageReference pref = new PageReference(url);
          // add parameters from the initial context (except <save_new>)
        for(String param : pageParameters.keySet())
            pref.getParameters().put(param, pageParameters.get(param));
        if(pref.getParameters().containsKey('save_new'))
            pref.getParameters().remove('save_new');      
        //pref.getParameters().put('retURL',ApexPages.currentPage().getParameters().get('retURL'));
        pref.getParameters().put(System.Label.CLMAY13SRV13,u.Name);
        pref.getParameters().put(System.Label.CLMAY13SRV14,u.id);
        if(u.UserBusinessUnit__c!= null)
        {
            //if(u.UserBusinessUnit__c=='BD' || u.UserBusinessUnit__c=='EN' || 
            //u.UserBusinessUnit__c=='ID'|| u.UserBusinessUnit__c=='IT'|| 
            //u.UserBusinessUnit__c=='PS'|| u.UserBusinessUnit__c=='PW' || u.UserBusinessUnit__c=='SI')
            //{   
                pref.getParameters().put(System.Label.CLMAY13SRV16,u.UserBusinessUnit__c);
           // }
        }
        if(WOId != null && WOId.trim()!= '')
        {
            SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
             wo = [Select SVMXC__Company__c, SVMXC__Company__r.Name, SVMXC__Site__c,SVMXC__Site__r.Name,SVMXC__Site__r.Id from SVMXC__Service_Order__c 
             where id =: WOId limit 1]; 
             String OpnName;
             if(wo.SVMXC__Company__c != null && wo.SVMXC__Site__c != null)
             {
                OpnName = wo.SVMXC__Site__r.Name + '-' + wo.SVMXC__Company__r.Name;
             }
             else
             {
                OpnName = '';
             }
             pref.getParameters().put('Name', OpnName);
             pref.getParameters().put(System.Label.CLMAY13SRV20,wo.SVMXC__Site__r.Id);
             pref.getParameters().put(System.Label.CLMAY13SRV19,wo.SVMXC__Site__r.Name);
        }//End: May2013 Release BR#2488 and BR#2766
        
       //<OCT13 Release>
        Case caseSource = new Case();
        String CaseOppNo = System.currentpagereference().getParameters().get(Label.CLOCT13CCC03); //CF00NK000000118rT
        System.debug('******** VFC_OppNotificationInter -- URL : '+System.currentpagereference().getUrl()+'-?'+CaseOppNo);
        if(CaseOppNo != null && CaseOppNo.trim() != ''){
           // List<Case> lstCase = [SELECT id,ProductLine__c,CCCountry__r.Name,Account.Name,Account.LeadingBusiness__c,Owner.Name FROM Case WHERE CaseNumber = :CaseOppNo limit 1];
           // caseSource = [SELECT id,ProductLine__c,CCCountry__r.Name,Account.Name,Account.LeadingBusiness__c,Owner.Name FROM Case WHERE CaseNumber = :CaseOppNo.trim() limit 1];
            List<Case> lstCase = [SELECT id,ProductLine__c,CCCountry__c,CCCountry__r.Name,Accountid,Account.Name,Account.LeadingBusiness__c,Ownerid,Owner.Name FROM Case WHERE CaseNumber = :CaseOppNo limit 1];
            if (lstCase.size() > 0)
            {
                caseSource = lstCase[0];
              if (caseSource.CCCountry__r.Name != null){
                  pref.getParameters().put(System.Label.CLOCT13CCC04,caseSource.CCCountry__r.Name);//Country of Destination - CF00NA0000009h5c5
                  pref.getParameters().put(System.Label.CLOCT13CCC20,caseSource.CCCountry__c);
                  }
                 
                pref.getParameters().put(System.Label.CLOCT13CCC05,caseSource.Account.Name); //Account Name - CF00NA0000009h5c0
                pref.getParameters().put(System.Label.CLOCT13CCC21,caseSource.Accountid); //Account ID
                
                pref.getParameters().put(System.Label.CLOCT13CCC06,caseSource.Owner.Name ); //Owner Name - CF00NA0000009eElK
                pref.getParameters().put(System.Label.CLOCT13CCC22,caseSource.Ownerid ); //Owner ID - CF00NA0000009eElK
               
                if (caseSource.Account.LeadingBusiness__c != null)
                  pref.getParameters().put(System.Label.CLOCT13CCC08,caseSource.Account.LeadingBusiness__c ); //Leading BU - 00NA0000009h5c6

                if (caseSource.ProductLine__c != null)
                   pref.getParameters().put(System.Label.CLOCT13CCC09,caseSource.ProductLine__c.left(5));
                System.debug('********** VFC_OppNotificationInter Product Line****'+caseSource.ProductLine__c);
            }   
            
        }
        //</OCT13 Release>
        
        pref.getParameters().put('nooverride','1');
        pref.setRedirect(true);
        
        return pref ;
    }

}