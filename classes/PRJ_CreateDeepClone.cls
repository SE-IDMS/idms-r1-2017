/*    
      Author          : Srikant Joshi (Schneider Electric)    
      Date Created    : 14/12/2012    
      Description     : Controller class for the BUTTON on Project Request 'Update Fund Auth'.                      
                        Creates a CLONE of Project Request along with Related Lists after checking whether the logged-in user has the authority to do so.
*/                      

global without sharing class PRJ_CreateDeepClone{

    //Create a Copy of Project Request along with its Related Lists objects; Budget;Budget Lines(ACTIVE) & Funding Entity.
    Webservice static String PRJ_createDeepClones(String stringId){
        Savepoint sp = Database.setSavepoint();// Create a savepoint
        Try{
            List<DMTAuthorizationMasterData__c> objAuth = new List<DMTAuthorizationMasterData__c>();
            Set<string> setUser = new Set<string>();
            String projWhereClause = 'Id = '+ '\''+ stringId+ '\'';
            String soql = getCreatableFieldsSOQL('PRJ_ProjectReq__c',projWhereClause);
            PRJ_ProjectReq__c curProjReq = (PRJ_ProjectReq__c)Database.query(soql);
            
            if(curProjReq.BusGeographicZoneIPO__c == 'Global'){ 
                System.Debug('Condition 4');  
                objAuth = [SELECT AuthorizedUser1__c,AuthorizedUser2__c,AuthorizedUser3__c,AuthorizedUser4__c,AuthorizedUser5__c,AuthorizedUser6__c,AuthorizedUser7__c,AuthorizedUser8__c,AuthorizedUser9__c,AuthorizedUser10__c,BusGeographicZoneIPO__c,BusinessTechnicalDomain__c,Id,NextStep__c,ParentFamily__c,Parent__c FROM DMTAuthorizationMasterData__c where NextStep__c =:System.Label.DMT_StatusProjectOpen AND BusinessTechnicalDomain__c =:curProjReq.BusinessTechnicalDomain__c and BusGeographicZoneIPO__c ='Global' limit 1];   
            }
            else{
                System.Debug('Condition 5');
                objAuth = [SELECT AuthorizedUser1__c,AuthorizedUser2__c,AuthorizedUser3__c,AuthorizedUser4__c,AuthorizedUser5__c,AuthorizedUser6__c,AuthorizedUser7__c,AuthorizedUser8__c,AuthorizedUser9__c,AuthorizedUser10__c,BusGeographicZoneIPO__c,BusinessTechnicalDomain__c,Id,NextStep__c,ParentFamily__c,Parent__c FROM DMTAuthorizationMasterData__c where NextStep__c =:System.Label.DMT_StatusProjectOpen AND BusGeographicZoneIPO__c =:curProjReq.BusGeographicZoneIPO__c limit 1];                  
            }
            
            if(objAuth.size() > 0 && (curProjReq.Nextstep__c == 'Fundings Authorized' || curProjReq.Nextstep__c == System.Label.DMT_Project_Status)){ 
                System.Debug('objAuth not null');
                SObject sobjectAuth =(SObject)objAuth[0];
                for(Integer i=1;i<11;i++){
                    if(sobjectAuth.get('AuthorizedUser'+i+'__c') !=null)
                        setUser.add(sobjectAuth.get('AuthorizedUser'+i+'__c')+'');//Get the set of Financial Controller
                }
                if(setUser.contains(UserInfo.getUserId())){ // Check if the Logged User is the Financial Controller for the Project Request
                    PRJ_ProjectReq__c newProjReq = curProjReq.clone(false, true,true,true);
                    // NOV Release 2013 - Added the function - Srikant Joshi
                    newProjReq.Nextstep__c = 'ARCHIVE';
                    //END
                    newProjReq.name = '(ARCHIVE)'+ newProjReq.name;
                    insert newProjReq;//Insert a CLONE of Project Request.
                    newProjReq.Nextstep__c = 'ARCHIVE';
                    update newProjReq;
                    
                    RecordType[] projRecType = [SELECT id FROM RecordType WHERE Name LIKE '%Develop IT Project Proposal%' limit 1];
                    If(projRecType.size()>0)
                    curProjReq.RecordTypeid = projRecType[0].Id;// Query and provide the record Type for Valid Project.
                    
                    list<Budget__c> lstBudget = new list<Budget__c>{};
                    String budgetWhereClause = 'ProjectReq__r.Id = '+ '\''+ stringId+ '\' and Active__c = True';
                    String budgetsoql = getCreatableFieldsSOQL('Budget__c',budgetWhereClause);
                    List<Budget__c> budRecords = Database.query(budgetsoql);
                    for(Budget__c budgetRec : budRecords ){
                        Budget__c budget = budgetRec.clone(false,true,true,true);
                        budget.ProjectReq__c = newProjReq.Id;
                        lstBudget.add(budget);
                    }
                    insert lstBudget;//Insert a CLONE of Active Budget.
                    
                    list<PRJ_BudgetLine__c> lstBudgetlines = new list<PRJ_BudgetLine__c>{};
                    String budgetLinesWhereClause = 'Budget__r.ProjectReq__r.Id = '+ '\''+ stringId+ '\' and Budget__r.Active__c = True';
                    String budgetLinessoql = getCreatableFieldsSOQL('PRJ_BudgetLine__c',budgetLinesWhereClause);
                    List<PRJ_BudgetLine__c> budLinesRecords = Database.query(budgetLinessoql);
                    for(PRJ_BudgetLine__c budgetLinesRec : budLinesRecords ){
                        PRJ_BudgetLine__c budgetLine = budgetLinesRec.clone(false, true,true,true);
                        budgetLine.Budget__c = lstBudget[0].Id;
                        lstBudgetlines.add(budgetLine);
                        
                    }
                    insert lstBudgetlines;// Insert a CLONE of Budget Line Records of ACTIVE budget.
                    
                    list<DMTFundingEntity__c> lstfundEnt = new list<DMTFundingEntity__c>{};
                    list<DMTFundingEntity__c> lstToUpdatefundEnt = new list<DMTFundingEntity__c>{};
                    String fundEntWhereClause = 'ProjectReq__r.Id = '+ '\''+ stringId+ '\'';
                    String fundEntsoql = getCreatableFieldsSOQL('DMTFundingEntity__c',fundEntWhereClause);
                    List<DMTFundingEntity__c> fundEntRecords = Database.query(fundEntsoql);
                    for(DMTFundingEntity__c fundEntRec : fundEntRecords ){
                        DMTFundingEntity__c fundEnt = fundEntRec.clone(false, true,true,true);
                        fundEnt.ProjectReq__c = newProjReq.Id;
                        fundEnt.ActiveForecast__c = false;
                        lstfundEnt.add(fundEnt);
                    }
                    insert lstfundEnt;//Insert a CLONE of Funding Entity..
                    
                    for(DMTFundingEntity__c fundEntRec : lstfundEnt){
                        fundEntRec.ActiveForecast__c = True; 
                        lstToUpdatefundEnt.add(fundEntRec);
                    }
                    
                    Update lstToUpdatefundEnt;
                    
                    list<DMTFundingEntity__c> lstCurfundEnt = new list<DMTFundingEntity__c>{};
                    for(DMTFundingEntity__c fundEntRec : fundEntRecords ){
                        fundEntRec.SelectedinEnvelop__c = false;//Project back in Valid must be de selected..
                        lstCurfundEnt.add(fundEntRec);
                    }
                    Update lstCurfundEnt;//Project back in Valid must be de selected..
                
                    curProjReq.Nextstep__c = 'Valid';
                    curProjReq.FinancialController__c = UserInfo.getUserId();
                    update curProjReq;//Send an email through workflow - PRJ_WF35_SendEmailWhenBackToCreated

                    return 'true'; // Return the confirmation that Records are processed successfully.
                }
            }
        } 
        catch (System.DmlException e) {
            Database.rollback(sp);//Rollback to the Savepoint
            for (Integer i = 0; i < e.getNumDml(); i++) {
                return e.getDmlMessage(i);// Return a Error message to the button.
            } 
        }

        Catch(Exception e){
            Database.rollback(sp);//Rollback to the Savepoint
            return 'false';// Return the error that Admin needs to look into what has exactly gone wrong.
        }
        return null;
    }
    
    
    /*Create a Copy of Project Request along with its Custom CLONE of the Project Request.
        1. Link the cloned PR automatically as the Sub Project of the Cloned PR.
        2. The Cloned Project will remain at the same STATUS as Parent record..
    */
    // Oct Release 2013 - Added the function - Srikant Joshi
    Webservice static String PRJ_createCustomClone(String stringId){ 
        Savepoint sp = Database.setSavepoint();// Create a savepoint
            Try{    
                Set<string> setUser = new Set<string>();
                List<DMTAuthorizationMasterData__c> objAuth = new List<DMTAuthorizationMasterData__c>();
                String projWhereClause = 'Id = '+ '\''+ stringId+ '\'';
                String soql = getCreatableFieldsSOQL('PRJ_ProjectReq__c',projWhereClause);
                PRJ_ProjectReq__c curProjReq = (PRJ_ProjectReq__c)Database.query(soql); 
                
                System.Debug('Condition 2a');            
                objAuth = [SELECT AuthorizedUser1__c,AuthorizedUser2__c,AuthorizedUser3__c,AuthorizedUser4__c,AuthorizedUser5__c,AuthorizedUser6__c,AuthorizedUser7__c,AuthorizedUser8__c,AuthorizedUser9__c,AuthorizedUser10__c,BusGeographicZoneIPO__c,BusinessTechnicalDomain__c,Id,NextStep__c,ParentFamily__c,Parent__c FROM DMTAuthorizationMasterData__c where NextStep__c =:System.Label.DMT_StatusValid AND BusinessTechnicalDomain__c =:curProjReq.BusinessTechnicalDomain__c limit 1];                    
                
                SObject sobjectAuth =(SObject)objAuth[0];
                for(Integer i=1;i<11;i++)
                {
                    if(sobjectAuth.get('AuthorizedUser'+i+'__c') !=null)
                    setUser.add(sobjectAuth.get('AuthorizedUser'+i+'__c')+'');
                    System.debug('users:'+setUser);
                }
                
                if(setUser.contains(UserInfo.getUserId())){ // Check if the Logged User is the Financial Controller for the Project Request
                    PRJ_ProjectReq__c newProjReq = curProjReq.clone(false, true,true,true);
                    newProjReq.Master_Project__c = curProjReq.Id;
                    newProjReq.WorkType__c = 'Sub Project';
                    // NOV Release 2013 - Added the function - Srikant Joshi
                    newProjReq.Nextstep__c = 'ARCHIVE';
                    //END
                    insert newProjReq;//Insert a CLONE of Project Request.

                    list<Budget__c> lstBudget = new list<Budget__c>{};
                    String budgetWhereClause = 'ProjectReq__r.Id = '+ '\''+ stringId+ '\' and Active__c = True';
                    String budgetsoql = getCreatableFieldsSOQL('Budget__c',budgetWhereClause);
                    List<Budget__c> budRecords = Database.query(budgetsoql);
                    for(Budget__c budgetRec : budRecords ){
                        Budget__c budget = budgetRec.clone(false,true,true,true);
                        budget.ProjectReq__c = newProjReq.Id;
                        lstBudget.add(budget);
                    }
                    insert lstBudget;//Insert a CLONE of Active Budget.
                    
                    newProjReq.nextstep__c = curProjReq.nextstep__c;
                    update newProjReq;                    
                    
                    return 'true';
                }
                else
                { 
                    return System.Label.DMTCustomCloneError;
                              
                }
            }
            catch (System.DmlException e) {
                Database.rollback(sp);//Rollback to the Savepoint
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    return e.getDmlMessage(i);// Return a Error message to the button.
                } 
            }

            Catch(Exception e){
                Database.rollback(sp);//Rollback to the Savepoint
                return 'false';// Return the error that Admin needs to look into what has exactly gone wrong.
            }
        return null;
    }
    
    
    // Returns a dynamic SOQL statement for the whole object, includes only creatable fields since we will be inserting a cloned result of this query
    public static string getCreatableFieldsSOQL(String objectName, String whereClause){
    
        String selects = '';
        if (whereClause == null || whereClause == ''){ return null; }
        
        // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
        
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
            selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
        }
        
        return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
    }
}