/*
    Author          : Srikant Joshi (Schneider Electric)
    Date Created    : 02/05/2013
    Description     : Test class for the Change Request
*/

@isTest
private class CHR_ChangeRequest_TEST{
    static testMethod void CHRCostForecastFundingEntity(){
    
        Change_Request__c chr = new Change_Request__c(name='test');
        insert chr;
        //creating Change request cost forecost1
        ChangeRequestCostsForecast__c crcf1 = new ChangeRequestCostsForecast__c();
        crcf1.ChangeRequest__c = chr.id;
        crcf1.Active__c = True;
        crcf1.LastActiveofBudgetYear__c = False;
        crcf1.Description__c = 'Test data';
        insert crcf1;
        //creating change request cost forecost2


        DMTCRCostForecastCostItemsFields__c pcfCs = new DMTCRCostForecastCostItemsFields__c();
        pcfCs.Name ='Test 1';
        pcfCs.CostItemFieldLabel__c = 'IT Cashout CAPEXY0';
        pcfCs.CostItemFieldAPIName__c = 'FY0CashoutCAPEX__c';
        pcfCs.Type__c = 'IT Cashout CAPEX';
        pcfCs.TypeOrder__c = 'Type 1';
        pcfCs.BackgroundColour__c = 'background-color:MistyRose';
        insert pcfCs;
        DMTCRCostForecastCostItemsFields__c pcfCs2 = new DMTCRCostForecastCostItemsFields__c();
        pcfCs2.Name ='Test 2';
        pcfCs2.CostItemFieldLabel__c = 'IT Cashout CAPEXY1';
        pcfCs2.CostItemFieldAPIName__c = 'FY1CashoutCAPEX__c';
        pcfCs2.Type__c = 'IT Cashout CAPEX';
        pcfCs2.TypeOrder__c = 'Type 1';
        pcfCs2.BackgroundColour__c = 'background-color:MistyRose';
        insert pcfCs2;
                  
        CHR_ManageProgramCostsForecasts pagePCF = new CHR_ManageProgramCostsForecasts(new ApexPages.StandardController(crcf1));
                  
        pagePCF.updateCostForecast();
        pagePCF.backToCostForecast();

        
        ChangeRequestFundingEntity__c ferec1 = new ChangeRequestFundingEntity__c();
        ferec1.ParentFamily__c = 'Business';
        ferec1.Parent__c = 'Buildings';
        ferec1.GeographicZoneSE__c = 'EMEAS';
        ferec1.GeographicalSEOrganization__c = 'All Europe';
        ferec1.CurrencyISOCode = 'EUR';
        ferec1.ChangeRequest__c = chr.id;
        ferec1.ActiveForecast__c = true;
        ferec1.BudgetYear__c = '2012';
        ferec1.ForecastQuarter__c = 'Q1';
        ferec1.ForecastYear__c = '2012';
        ferec1.CAPEXY0__c = 9;
        ferec1.SelectedinEnvelop__c = true;
        ferec1.HFMCode__c = '111';
        insert ferec1;

        ChangeRequestFundingEntity__c ferec2 = new ChangeRequestFundingEntity__c();
        ferec2.ParentFamily__c = 'Business';
        ferec2.Parent__c = 'Buildings';
        ferec2.GeographicZoneSE__c = 'APAC';
        ferec2.GeographicalSEOrganization__c = 'All Europe';
        ferec2.CurrencyISOCode = 'EUR';
        ferec2.ChangeRequest__c = chr.id;
        ferec2.ActiveForecast__c = false;
        ferec2.BudgetYear__c = '2012';
        ferec2.ForecastQuarter__c = 'Q1';
        ferec2.ForecastYear__c = '2012';
        ferec2.CAPEXY0__c = 9;
        ferec2.SelectedinEnvelop__c = true;
        ferec2.HFMCode__c = '111';
        insert ferec2;
        ferec2.ActiveForecast__c = true;
        update ferec2;
        delete ferec2;
        
       
        DMTAuthorizationMasterData__c testDMTA1 = new DMTAuthorizationMasterData__c(NextStep__c = 'Fundings Authorized',Platform__c = 'Integration', AuthorizedUser1__c=UserInfo.getUserId() );
        insert testDMTA1;
        
        DMTAuthorizationMasterData__c testDMTA2 = new DMTAuthorizationMasterData__c(NextStep__c = 'Valid',BusinessTechnicalDomain__c = 'Integration', AuthorizedUser1__c=UserInfo.getUserId() );
        insert testDMTA2;
       
        Test.starttest(); 
        chr.nextstep__c = 'Fundings Authorized';
        chr.BusinessTechnicalDomain__c = 'Integration';
        chr.Platform__c = 'Integration';
        update chr;

        CHR_UpdatePriorityController vfCtrl = new CHR_UpdatePriorityController(new ApexPages.StandardController(chr));
        //testProj = new PRJ_ProjectReq__c();      
        vfCtrl.updatechangeReqRequest();
        vfCtrl.backToChangeRequest();

        CHR_UpdateStatusController vfCtr2 = new CHR_UpdateStatusController(new ApexPages.StandardController(chr));
        //testProj = new PRJ_ProjectReq__c();      
        vfCtr2.updateProjectRequest();
        vfCtr2.backToProjectRequest();

        chr.nextstep__c = 'Valid';
        update chr;
        CHR_UpdatePriorityController vfCtr3 = new CHR_UpdatePriorityController(new ApexPages.StandardController(chr));
        //testProj = new PRJ_ProjectReq__c();      
        vfCtr3.updatechangeReqRequest();
        vfCtr3.backToChangeRequest();

        CHR_UpdateStatusController vfCtr4 = new CHR_UpdateStatusController(new ApexPages.StandardController(chr));
        //testProj = new PRJ_ProjectReq__c();      
        vfCtr4.updateProjectRequest();
        vfCtr4.backToProjectRequest();

        CHR_DraftValidationErrorsController vfCtr6 = new CHR_DraftValidationErrorsController(new ApexPages.StandardController(chr));
        //testProj = new PRJ_ProjectReq__c();      
        vfCtr6.updateProject();
        //vfCtr6.backToProjectRequest();

        chr.nextstep__c = 'Valid';
        update chr;

        User runAsUser = Utils_TestMethods.createStandardDMTUser('tsty');
        insert runAsUser;
        System.runAs(runAsUser){

        chr.GoLiveTargetDate__c = system.today();
        chr.ExpectedStartDate__c = chr.GoLiveTargetDate__c + 1;

        //CHR_UpdatePriorityController vfCtr3 = new CHR_UpdatePriorityController(new ApexPages.StandardController(chr));
        //testProj = new PRJ_ProjectReq__c();      
        vfCtr3.updatechangeReqRequest();
        vfCtr3.backToChangeRequest();

        //CHR_UpdateStatusController vfCtr4 = new CHR_UpdateStatusController(new ApexPages.StandardController(chr));
        //testProj = new PRJ_ProjectReq__c();      
        vfCtr4.updateProjectRequest();
        vfCtr4.backToProjectRequest();

        //CHR_DraftValidationErrorsController vfCtr6 = new CHR_DraftValidationErrorsController(new ApexPages.StandardController(chr));
        //testProj = new PRJ_ProjectReq__c();      
        vfCtr6.updateProject();
        //vfCtr6.backToProjectRequest();
        }

        chr.nextstep__c = 'Cancelled';
        update chr;

        Test.stoptest();
         
    }
}