/* DESCRIPTION: UPDATES RISK ASSESSEMENT RECORD TYPE TO CLOCT15CFW03 WHEN USER DATA ACCESS RECORD IS SAVED*/
public class AP_CFWUserDataAccessBeforeUpdateHandler
{
    public static void updateRiskAssessment(List<CFWUserDataAccess__c> userDataAccess)
    {
        List<Id> riskAssessmentIds = new List<Id>();
        Map<Id, CFWUserDataAccess__c> duaRiskAss = new Map<Id, CFWUserDataAccess__c>();
        for(CFWUserDataAccess__c uda: userDataAccess)
        {
            if(uda.CertificationRiskAssessment__c!=null)
            {
                riskAssessmentIds.add(uda.CertificationRiskAssessment__c);
                duaRiskAss.put(uda.CertificationRiskAssessment__c,uda);
            }
        }
         List<RecordType> recordTypes = [select Id,Name From RecordType where sobjecttype = 'CFWRiskAssessment__c'];
         Id dataViewRecordTypeId;
         for(RecordType rt: recordTypes)
         {
             if(rt.Name == 'Data View')
                 dataViewRecordTypeId = rt.Id;
         }
         if(riskAssessmentIds!=null && riskAssessmentIds.size()>0)
         {
            List<CFWRiskAssessment__c> riskAssessments = [select Id,RecordTypeId from CFWRiskAssessment__c where id in:riskAssessmentIds];
            List<CFWRiskAssessment__c> rskToUpdate = new List<CFWRiskAssessment__c>();
            for(CFWRiskAssessment__c cfw: riskAssessments)
            {
                if(cfw.RecordTypeId != dataViewRecordTypeId)
                {
                    cfw.RecordTypeId = dataViewRecordTypeId;
                    rskToUpdate.add(cfw);
                }
            }
            if(!(rskToUpdate.isEmpty()))
            {
                List<Database.saveresult> sr = Database.update(rskToUpdate,false);
                for(Database.saveresult ds: sr)
                {
                    if(!(ds.isSuccess()))
                    {
                        system.debug(ds.getErrors()[0].getMessage());
                        if(ds.getId()!=null && duaRiskAss.get(ds.getId())!=null && ds.getErrors()[0]!=null)
                            duaRiskAss.get(ds.getId()).addError(ds.getErrors()[0].getMessage());
                    }
                }
            }
        }
    }
}