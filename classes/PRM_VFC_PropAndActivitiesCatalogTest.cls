@isTest
public class PRM_VFC_PropAndActivitiesCatalogTest{

    public static testMethod void testInternalProperties(){
        PageReference pageRef = Page.VFP_PropertiesAndActivitiesCatalogStep1;
        Test.setCurrentPage(pageRef);
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='IN';
        insert country;
        
        Country__c country1 = Utils_TestMethods.createCountry();
        country1.countrycode__c ='US';
        insert country1;
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;

        //creates a program level for the global program
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;

        //creates a country partner program
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;

        //creates a program level for the country partner program
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;
        
        pageRef.getParameters().put('ProgramLevelName',prg2.Id);
        pageRef.getParameters().put('ProgramLevelId',prg2.Id);
        Id recordTypeId = [select id from RecordType where DeveloperName ='BFOProperties' and SobjectType='ExternalPropertiesCatalog__c'].Id;
        ExternalPropertiesCatalog__c myEPC = new ExternalPropertiesCatalog__c(RecordTypeId = recordTypeId,FieldName__c ='PRMZipCode__c',TableName__c ='Account',PRMCountryFieldName__c ='PRMCountry__c',PropertyName__c='test prop',Subtype__c='test prop');
        
        
        ApexPages.StandardController stdCntroller = new ApexPages.StandardController(myEPC);
        PRM_VFC_PropertiesAndActivitiesCatalog myController = new PRM_VFC_PropertiesAndActivitiesCatalog(stdCntroller);
        /*System.assertEquals('',myController.externalSystemType);
        System.assertEquals(true,myController.internalRecordType);
        System.assertEquals(null,myController.programLevelId);
        System.assertNotEquals(null,myController.createEventCatalogLink);
        System.assertEquals(false,myController.externalActivitiesRecordType);*/
        myController.allowRetroCalculation = true;
        //myController.internalRecordType = true;
        myController.step1();
        myController.step2();
        myController.addEventCatalog();
        myController.saveWithEvents();
        myController.removeEventCatalog();
        myController.refreshtable();
        myController.retrieveEvent();

        myController.step1();
        myController.step2();
        myController.addEventCatalog();
        myController.saveWithEvents();
        myController.removeEventCatalog();
        myController.retrieveEvent();
        myController.refreshtable();       
    }
    
    public static testMethod void testExternalProperties(){
        PageReference pageRef = Page.VFP_PropertiesAndActivitiesCatalogStep1;
        Test.setCurrentPage(pageRef);
        Id recordTypeId = [select id from RecordType where DeveloperName ='ExternalProperties' and SobjectType='ExternalPropertiesCatalog__c'].Id;
        ExternalPropertiesCatalogType__c epcExternalSystemType = new ExternalPropertiesCatalogType__c(Source__c='UIMS',Type__c='ApplicationAccess',TypeOfSource__c='BFO',PropertiesAndCatalogRecordType__c='External Properties');
        insert epcExternalSystemType;
        ExternalPropertiesCatalog__c myEPC =  new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,ExternalPropertiesCatalogType__c=epcExternalSystemType.Id,PropertyName__c='test prop',Subtype__c='test prop');
        pageRef.getParameters().put('Source','UIMS');
        ApexPages.StandardController stdCntroller = new ApexPages.StandardController(myEPC);
        PRM_VFC_PropertiesAndActivitiesCatalog myController = new PRM_VFC_PropertiesAndActivitiesCatalog(stdCntroller);
        /*System.assertEquals('External Properties',myController.externalSystemType);
        System.assertEquals(false,myController.internalRecordType);
        System.assertEquals(null,myController.programLevelId);
        System.assertNotEquals(null,myController.createEventCatalogLink);
        System.assertEquals(false,myController.externalActivitiesRecordType);*/
        myController.allowRetroCalculation = true;
        String test1 = myController.createEventCatalogLink;
        myController.step1();
        myController.step2();
        myController.addEventCatalog();
        myController.saveWithEvents();
        myController.removeEventCatalog();
        myController.refreshtable();
        myController.retrieveEvent();
        List<SelectOption> options = myController.sourceList; 
        options = myController.typeList;
        
    }


    public static testMethod void testExternalActivities(){
        PageReference pageRef = Page.VFP_PropertiesAndActivitiesCatalogStep1;
        Test.setCurrentPage(pageRef);
        Id recordTypeId = [select id from RecordType where DeveloperName ='ExternalActivities' and SobjectType='ExternalPropertiesCatalog__c'].Id;
        ExternalPropertiesCatalogType__c epcExternalSystemType = new ExternalPropertiesCatalogType__c(Source__c='UIMS',Type__c='ApplicationAccess',TypeOfSource__c='BFO',PropertiesAndCatalogRecordType__c='External Activities');
        insert epcExternalSystemType;
        ExternalPropertiesCatalog__c myEPC =  new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,ExternalPropertiesCatalogType__c=epcExternalSystemType.Id,PropertyName__c='test prop',Subtype__c='test prop');
        ApexPages.StandardController stdCntroller = new ApexPages.StandardController(myEPC);
        PRM_VFC_PropertiesAndActivitiesCatalog myController = new PRM_VFC_PropertiesAndActivitiesCatalog(stdCntroller);
        /*System.assertEquals('External Activities',myController.externalSystemType);
        System.assertEquals(false,myController.internalRecordType);
        System.assertEquals(null,myController.programLevelId);
        System.assertNotEquals(null,myController.createEventCatalogLink);
        System.assertEquals(true,myController.externalActivitiesRecordType);*/

        myController.step1();
        myController.step2();
        myController.addEventCatalog();
        myController.saveWithEvents();
        myController.removeEventCatalog();
        myController.refreshtable();
        myController.retrieveEvent();
        List<SelectOption> options = myController.sourceList; 
        options = myController.typeList;
    }


    public static testMethod void testExternalPropertiesMigration(){
        
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='IN';
        insert country;
        
        Country__c country1 = Utils_TestMethods.createCountry();
        country1.countrycode__c ='US';
        insert country1;

        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
        newRecTypeMap.name = recCatalogActivityReq.Id;
        newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
        newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;

        insert newRecTypeMap;
        
        //creates a global partner program
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;

        //creates a program level for the global program
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;

        //creates a country partner program
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;

        //creates a program level for the country partner program
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;

        
        PageReference pageRef = Page.VFP_PropertiesAndActivitiesCatalogStep1;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('ProgramLevelName',prg2.Id);
        pageRef.getParameters().put('ProgramLevelId',prg2.Id);
        
        Id recordTypeId = [select id from RecordType where DeveloperName ='ExternalProperties' and SobjectType='ExternalPropertiesCatalog__c'].Id;
        ExternalPropertiesCatalogType__c epcExternalSystemType = new ExternalPropertiesCatalogType__c(Source__c='BFO',Type__c='MigrationToProgramV2',TypeOfSource__c='BFO',PropertiesAndCatalogRecordType__c='External Properties');
        insert epcExternalSystemType;
        ExternalPropertiesCatalog__c myEPC =  new ExternalPropertiesCatalog__c(RecordTypeId=recordTypeId,ExternalPropertiesCatalogType__c=epcExternalSystemType.Id,PropertyName__c='test prop',Subtype__c='test prop');
        ApexPages.StandardController stdCntroller = new ApexPages.StandardController(myEPC);
        PRM_VFC_PropertiesAndActivitiesCatalog myController = new PRM_VFC_PropertiesAndActivitiesCatalog(stdCntroller);

        FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Event__c', 'Counter', 'name', 'equals', decimal.valueof(1));
        FieloCH__MissionCriteria__c missionCriteria1 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'FieloEE__Type__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
        FieloCH__MissionCriteria__c missionCriteria2 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_ValueText__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
        myController.step1();
        myController.step2();
        myController.step3();
        myController.addEventCatalog();
        myController.saveWithEvents();
        myController.removeEventCatalog();
        myController.refreshtable();
        myController.currentMissionCriteria =missionCriteria1;
        myController.retrieveEvent();
        myController.currentMissionCriteria =missionCriteria2;
        myController.retrieveEvent();



    }

}