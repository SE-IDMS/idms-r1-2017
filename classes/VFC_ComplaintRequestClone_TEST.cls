@isTest
public class VFC_ComplaintRequestClone_TEST {
static testMethod void ComplaintRequestClone() {
 Country__c CountryObjLCR = Utils_TestMethods.createCountry();
        Insert CountryObjLCR;
        // Create User
        User LCRTestUser = Utils_TestMethods.createStandardUser('LCRRasg');
        Insert LCRTestUser ;
        User LCRTestUserCq = Utils_TestMethods.createStandardUser('LCROwMCQ');
        Insert LCRTestUserCq ;
        Account accountObj = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', Country__c=CountryObjLCR.Id);
    
    insert accountObj;
    
    Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'testContact');
    insert ContactObj;
   
    Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
    
    CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
    CaseObj.Symptom__c =  'Installation/ Setup';
    CaseObj.SubSymptom__c = 'Hardware';    
    CaseObj.Quantity__c = 4;
    //CaseObj.CommercialReference__c='xbtg5230';
    Insert CaseObj;
       
        CS_ComplaintRequestCloneFields__c CR0221= new CS_ComplaintRequestCloneFields__c(Name = 'CR0221', CRFieldApiName__c = 'Case__c',CRFieldId__c ='CF00NA000000AM2LT_lkid' ,CRFieldLabel__c = 'Case');
        CS_ComplaintRequestCloneFields__c CR0222= new CS_ComplaintRequestCloneFields__c(Name = 'CR0222', CRFieldApiName__c = 'Case__r.CaseNumber',CRFieldId__c ='CF00NA000000AM2LT' ,CRFieldLabel__c = 'Case');
        CS_ComplaintRequestCloneFields__c CR0223= new CS_ComplaintRequestCloneFields__c(Name = 'CR0223', CRFieldApiName__c = 'Category__c',CRFieldId__c ='00NA000000AM2LU' ,CRFieldLabel__c = 'Category');
        CS_ComplaintRequestCloneFields__c CR0224= new CS_ComplaintRequestCloneFields__c(Name = 'CR0224', CRFieldApiName__c = 'TECH_DueDate__c',CRFieldId__c ='00N1200000B1Ifm' ,CRFieldLabel__c = 'Due Date',DataType__c='DateTime');
        
        list<CS_ComplaintRequestCloneFields__c> lstCS = new list<CS_ComplaintRequestCloneFields__c>{CR0221, CR0222, CR0223, CR0224};
        insert lstCS;
        
        BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST SCH',
                                            Entity__c='LCR Entity-1',
                                            SubEntity__c='LCR Sub-Entity',
                                            Location__c='LCR Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'LCR Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',
                                            ContactEmail__c ='SCH@schneider.com'
                                            );
        Insert lCROrganzObj; 
        BusinessRiskEscalationEntity__c lCROrganzObj1 = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST Repair',
                                            Entity__c='LCR Entity-1',
                                            SubEntity__c='LCR Sub-Entity1',
                                            Location__c='LCR Location 1',
                                            Location_Type__c= 'LCR',
                                            Street__c = 'LCR Street1',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City1',
                                            RCZipCode__c = '500225',
                                            ContactEmail__c ='SCH1@schneider.com'
                                            );
        Insert lCROrganzObj1;
        
        BusinessRiskEscalationEntity__c lCROrganzObj2 = new BusinessRiskEscalationEntity__c();
        lCROrganzObj2.Entity__c = 'Mechanisburg';
        lCROrganzObj2.name ='Mechanisburg';
        insert lCROrganzObj2;
        BusinessRiskEscalationEntity__c lCROrganzObj3 = new BusinessRiskEscalationEntity__c();
        
       
        
        ComplaintRequest__c  CrObj = new ComplaintRequest__c(                
                Status__c=Label.CLOCT13RR57,
                TECH_DueDate__c=DateTime.Now(),
                Category__c =Label.CLAPR14I2P02,
                reason__c='Marketing Activity Response',
                AccountableOrganization__c =lCROrganzObj.id, 
                ReportingEntity__c =lCROrganzObj1.id
                );
                
        Insert CrObj; 
        
        ComplaintRequest__c  CrObj1 = new ComplaintRequest__c(                
                Status__c=Label.CLOCT13RR57,              
                Category__c =Label.CLAPR14I2P02,
                TECH_DueDate__c=DateTime.Now(),
                reason__c='Marketing Activity Response',
                AccountableOrganization__c =lCROrganzObj.id, 
                ReportingEntity__c =lCROrganzObj1.id,
                Case__c=CaseObj.id,
                RecordTypeId='012A0000000o0Qv'
                );
                
        Insert CrObj1; 
system.debug('***cr**'+CrObj1);
        ApexPages.currentPage().getParameters().put('CRId',CrObj.id);               
        ApexPages.currentPage().getParameters().put('isdtp','isdtp');
        VFC_ComplaintRequestClone vfCtrl = new VFC_ComplaintRequestClone(new ApexPages.StandardController(CrObj));
        vfCtrl.CRCloneLink();
        ApexPages.currentPage().getParameters().put('CRId',CrObj1.id);              
        ApexPages.currentPage().getParameters().put('isdtp','isdtp');
        VFC_ComplaintRequestClone vfCtrl1 = new VFC_ComplaintRequestClone(new ApexPages.StandardController(CrObj1));
        vfCtrl1.CRCloneLink();
}}