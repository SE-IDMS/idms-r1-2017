/********************************************************************
* Company: Fielo
* Created Date: 01/11/2015
* Description:
********************************************************************/
@istest
public with sharing class  FieloPRM_DeliveryRewardCustomTest {

    public static testMethod void testUnit(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
                     
            Id idRecordTypeSegment = [Select id,developerName, sobjecttype  from RecordType WHERE sobjecttype   = 'FieloEE__RedemptionRule__c' and developerName = 'Manual'].id;
            
            List<FieloEE__Member__c> members = new List<FieloEE__Member__c>();
            
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'Polo';
            member.FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime());
            member.FieloEE__Street__c = 'test';
            members.add(member);
            
            FieloEE__Member__c member2 = new FieloEE__Member__c();
            member2.FieloEE__LastName__c= 'Polo2';
            member2.FieloEE__FirstName__c = 'Marco2' +  String.ValueOf(DateTime.now().getTime());
            member2.FieloEE__Street__c = 'test';
            members.add(member2);
            
            insert members;
            
            FieloEE__RedemptionRule__c segment = FieloPRM_UTILS_MockUpFactory.createSegmentManual();
            
            List<FieloEE__MemberSegment__c> domains = new List<FieloEE__MemberSegment__c>();
            
            FieloEE__MemberSegment__c domainm = new FieloEE__MemberSegment__c();
            domainm.FieloEE__Member2__c = member.Id;
            domainm.FieloEE__Segment2__c = segment.Id;
            domains.add(domainm);
            
            FieloEE__MemberSegment__c domainm2 = new FieloEE__MemberSegment__c();
            domainm2.FieloEE__Member2__c = member2.Id;
            domainm2.FieloEE__Segment2__c = segment.Id;
            domains.add(domainm2);
            
            insert domains;
            
            List<FieloEE__Badge__c> listbadge = new list<FieloEE__Badge__c>();
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c();
            badge.name = 'Badge';
            badge.F_PRM_Type__c = 'Program Level';
            badge.F_PRM_Segment__c =  segment.id;
            badge.F_PRM_BadgeAPIName__c = 'apinametest';
            listbadge.add(badge) ;

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c();
            badge2.name = 'Badge';
            badge2.F_PRM_Type__c = 'BFOProperties';
            badge2.F_PRM_Segment__c =  segment.id;
            badge2.F_PRM_BadgeAPIName__c = 'apinametest2';
            listbadge.add(badge2) ;

            insert listbadge;
            
            Date fecha = system.today();
            
            List<FieloCH__Challenge__c> challenges = new List<FieloCH__Challenge__c>();
            
            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(Name = 'Challenge TEST', FieloCH__Mode__c = 'Competition', FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Global', FieloCH__InitialDate__c = fecha.addDays(-5), FieloCH__FinalDate__c = fecha.addDays(5), 
                FieloCH__SubscribeFrom__c = null, FieloCH__SubscribeTo__c = null, F_PRM_ChallengeFilter__c = 'Ch' + datetime.now());
            FieloCH__Challenge__c challenge2 = new FieloCH__Challenge__c(Name = 'Challenge TEST', FieloCH__Mode__c = 'Teams', FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Global', FieloCH__InitialDate__c = fecha.addDays(-5), FieloCH__FinalDate__c = fecha.addDays(5), 
                FieloCH__SubscribeFrom__c = null, FieloCH__SubscribeTo__c = null, F_PRM_ChallengeFilter__c = 'Ch' + datetime.now());
            FieloCH__Challenge__c challenge3 = new FieloCH__Challenge__c(Name = 'Challenge TEST', FieloCH__Mode__c = 'Collaborative', FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Global', FieloCH__InitialDate__c = fecha.addDays(-5), FieloCH__FinalDate__c = fecha.addDays(5), 
                FieloCH__SubscribeFrom__c = null, FieloCH__SubscribeTo__c = null, F_PRM_ChallengeFilter__c = 'Ch' + datetime.now());
            
            challenges.add(challenge);
            challenges.add(challenge2);
            challenges.add(challenge3);
            insert challenges;
            
            List<FieloEE__SegmentDomain__c> segDoms = new List<FieloEE__SegmentDomain__c>();
            
            FieloEE__SegmentDomain__c domain3 = new FieloEE__SegmentDomain__c();
            domain3.FieloCH__Challenge__c = challenge3.Id;
            domain3.FieloEE__Segment__c = segment.Id;
            segDoms.add(domain3);
            
            FieloEE__SegmentDomain__c domain2 = new FieloEE__SegmentDomain__c();
            domain2.FieloCH__Challenge__c = challenge2.Id;
            domain2.FieloEE__Segment__c = segment.Id;
            segDoms.add(domain2);
            
            FieloEE__SegmentDomain__c domain = new FieloEE__SegmentDomain__c();
            domain.FieloCH__Challenge__c = challenge.Id;
            domain.FieloEE__Segment__c = segment.Id;
            segDoms.add(domain);
            
            insert segDoms;
         

            FieloCH__ChallengeReward__c challengeReward  = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge);       
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badge.id;
            challengeReward.F_PRM_CustomRewardClass__c = 'FieloPRM_DeliveryRewardCustom.Teams';
            
            update challengeReward;
            
            system.debug('PRM_CustomRewardClass' + challengeReward);
            

            FieloCH__ChallengeReward__c challengeReward2  = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge2);     
            challengeReward2.FieloCH__LogicalExpression__c = '(1)';
            challengeReward2.FieloCH__Badge__c = badge.id;
            challengeReward2.F_PRM_CustomRewardClass__c = 'FieloPRM_DeliveryRewardCustom.Teams';
            
            update challengeReward2;

            Account acc= new Account(
                Name = 'test',
                Street__c = 'Some Street',
                ZipCode__c = '012345',
                PLPartnerLocatorFeatureGranted__c = true,
                City__c = 'test'
            );
            insert acc;

            FieloCH__Team__c team = new FieloCH__Team__c();
            team.FieloCH__Program__c = member.FieloEE__Program__c;
            //team.F_PRM_Account__c = acc.Id;
            insert team;
            
            
            list<FieloCH__ChallengeMember__c> listChallengeMembers = new list<FieloCH__ChallengeMember__c>();
            
            FieloCH__ChallengeMember__c challengemember = FieloPRM_UTILS_MockUpFactory.createChallengeMember(challenge2, member2);
            challengemember.FieloCH__Team__c = team.Id;
            challengemember.FieloCH__ChallengeReward__c = challengeReward.id;
            upsert challengemember;
            listChallengeMembers.add(challengemember);
            
            FieloCH__ChallengeMember__c challengemember2 = FieloPRM_UTILS_MockUpFactory.createChallengeMember(challenge, member);
            challengemember2.FieloCH__Team__c = team.Id;
            challengemember2.FieloCH__ChallengeReward__c = challengeReward2.id;
            upsert challengemember2;
            listChallengeMembers.add(challengemember2 );
            
            test.startTest();

            try{            
                FieloPRM_DeliveryRewardCustom testDelivery = new FieloPRM_DeliveryRewardCustom();
                testDelivery.deliveryRewards(listChallengeMembers);   
            }catch (Exception e){
            }
            
            test.stopTest();
        }
    }

}