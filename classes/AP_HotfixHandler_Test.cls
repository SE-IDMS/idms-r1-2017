@isTest
public class AP_HotfixHandler_Test {
    
    testmethod static void testHotfixApproved() 
    {
        Hotfix__c testHotfix1=new Hotfix__c();     
        insert testHotfix1;    
        Test.startTest();    
            testHotfix1.status__c='Approved';    
            update testHotfix1;    
        Test.stopTest();   
    }
    
    testmethod static void testHotfixMigrated() 
    {
    
        Hotfix__c testHotfix1=new Hotfix__c();     
        insert testHotfix1;    
        Test.startTest();    
            testHotfix1.status__c='Migrated';    
            update testHotfix1;    
        Test.stopTest();   
    }
    
}