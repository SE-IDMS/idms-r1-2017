/* 
Class       : VFC_SRV_MYFSCaseDetailController
Author      : Raghavender Katta
Description : An apex page controller that takes the user to Case Detail page.
*/
Public without sharing class VFC_SRV_MYFSCaseDetailController{
    
    Public Case caseDetails{get;set;}
    Public String caseId{get;set;}
    //Public String caseStatus{get;set;}
    Public String ultimateParentAccount {get;set;}
    Public String selectedCountry {get;set;}
    Public String Site{get;set;}
    Public String userId{get;set;}    
    
    Public VFC_SRV_MYFSCaseDetailController(){
        caseId = Apexpages.currentpage().getparameters().get('Id');
        ultimateParentAccount = Apexpages.currentpage().getparameters().get('Params');
        selectedCountry = Apexpages.currentpage().getparameters().get('country');
        Site = Apexpages.currentpage().getparameters().get('site');
        System.debug('caseId'+caseId);
        getCaseDetails();
        userId = Apexpages.currentpage().getparameters().get('UId');
        if(userId==null || userId ==''){
         userId = UserInfo.getUserId();     
        }
    }
    
    Public void getCaseDetails(){
        caseDetails = new Case();
        Set<String> unUsedWorkOrderStatus = new Set<String>();
        unUsedWorkOrderStatus.add(System.label.CLSRVMYFS_SCHDEDULED);
        unUsedWorkOrderStatus.add(System.label.CLSRVMYFS_NEW);
        unUsedWorkOrderStatus.add(System.label.CLSRVMYFS_UNSCHEDULED);
        unUsedWorkOrderStatus.add(System.label.CLSRVMYFS_CANCELLED);
        //(SVMXC__Order_Status__c = 'Customer Confirmed' or SVMXC__Order_Status__c = 'Acknowledge FSE')
        if(caseId != null && caseId != '')
            caseDetails = [select Subject,CreatedDate,LastModifiedDate,CaseNumber,Status,Account.name,AssetOwner__r.name,CommercialReference_lk__r.name,ProductFamily__c,Contact.name,(select SVMXC__Scheduled_Date_Time__c ,SVMXC__Order_Type__c,TECH_CustomerScheduledDateTimeECH__c,SVMXC__Order_Status__c,SVMXC__Group_Member__r.Name,SVMXC__Component__c,Name from SVMXC__Service_Order__r where SVMXC__Order_Status__c NOT IN :unUsedWorkOrderStatus ORDER BY SVMXC__Scheduled_Date_Time__c 
                  DESC NULLS LAST) from Case  where Id = :caseId];
        System.debug('caseDetails'+caseDetails.SVMXC__Service_Order__r); 
        
    }
    
    
}