public without sharing class VFC_PartnerLeadAcceptReject {

    public Task partnerTask{get;set;}
    private String type ;
    public PartnerOpportunityStatusHistory__c plStatus;
    public String reqType{get;set;}
    public String RejectReason{get;set;}
    String urlStr;
    
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public VFC_PartnerLeadAcceptReject(ApexPages.StandardController stdController) {
        this.partnerTask = [SELECT Id, Tech_LeadId__c, OwnerId, RecordTypeId,RejectReason__c FROM Task WHERE 
                            id =:ApexPages.currentPage().getParameters().get('taskid') LIMIT 1];

        this.plStatus = [SELECT Id, Lead__c, User__c, Status__c,RejectReason__c FROM PartnerOpportunityStatusHistory__c WHERE
                            (Lead__c = :this.partnerTask.Tech_LeadId__c AND User__c = :this.partnerTask.OwnerId AND
                            Status__c = 'Assigned') LIMIT 1];
                            
      //  pOppStatus = this.plStatus;
                             
        system.debug('PartnerTask :' + this.partnerTask );
        system.debug('PartnerTask Id:' + this.partnerTask.Id);
        system.debug('plstatus :' + this.plStatus);
        
        this.type = ApexPages.currentPage().getParameters().get('type') ;
        system.debug('typec:' + type);
        
        urlStr = '/';
        System.debug(Site.getPrefix());
        System.debug(Site.getCurrentSiteUrl());
        if(Site.getPrefix() != null && (Site.getPrefix() == '/partners' || Site.getPrefix() == '/pomp'))
           urlStr = Site.getCurrentSiteUrl();
           
    }
    
    
    public PageReference updateTask() {
        Pagereference pageref;
        system.debug('type:' + this.type);
        this.plStatus.Status__c = type + 'ed';
        this.partnerTask.recordtypeid = System.Label.CLOCT13PRM14;
        this.partnerTask.Status = System.Label.CL00327;
        pageref = new pagereference(urlStr + (this.type == 'Accept' ? this.partnerTask.Tech_LeadId__c : this.partnerTask.Id));
        Lead ld = [select ownerId, Tech_LeadTransferredBy__c FROM Lead Where id= :this.partnerTask.Tech_LeadId__c limit 1];
        if (this.type == 'Reject') {
        
            if(this.partnerTask.RejectReason__c == null)
            {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.CLAPR14PRM22));
                return null;
            }   
        
            //Lead ld = new Lead(Id = this.partnerTask.Tech_LeadId__c, ownerId=ld.Tech_LeadTransferredBy__c);
            if(ld.Tech_LeadTransferredBy__c != null)
            {
                ld.ownerId=ld.Tech_LeadTransferredBy__c;
                System.Debug ('Lead: ' + ld);
                update ld;
            }
            
            this.plStatus.RejectReason__c = this.partnerTask.RejectReason__c;
                
        }

        System.Debug ('Partner Lead Task: ' + this.partnerTask);
        update this.partnerTask;
        
        System.Debug ('Partner Lead History Status: ' + this.plStatus);
        update this.plStatus;
        return pageref;
        
    }
    
    public String getreqType()
    {
        System.debug('Requested Type ->'+this.type);        
        return this.type;   
    
    }
    
    
        
    public PageReference doCancel()
    {   
        Pagereference pageref = new pagereference(urlStr + this.partnerTask.Id);
        return pageref;     
    }
    
    
     public pagereference isApproved()         
     {
        
        PageReference redirectPg;  
        if(this.type == 'Accept'){
            redirectPg = updateTask();
            if(redirectPg == null)
               return null;
            redirectPg.setRedirect(true);
            return redirectPg;
        }   
        else
            return null;
        
    
    }   
    
    
    
}