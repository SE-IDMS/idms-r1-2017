/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        : 10-July-2013
    Description         : Class for IPO Program Progress update
*/

public class IPOProgramProgressTrigger
{

Public Static void IPOProgramProgressAfterInsertUpdate(List<Program_Progress_Snapshot__c> PPnew)
      {
      
      List<Program_Progress_Snapshot__c> PP = new  List<Program_Progress_Snapshot__c>();  
      
      PPnew[0].ValidateInsertUpdate__c = False;
      PP = [select id from Program_Progress_Snapshot__c where IPO_Program__c = :PPnew[0].IPO_Program__c and Current_Year__c = :PPnew[0].Current_Year__c];
      if(PP.size() > 0)
        PPnew[0].ValidateInsertUpdate__c = True;
      }
}