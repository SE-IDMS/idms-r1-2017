public with sharing class VFC_WorkDetailsInter {
    
    // current page parameters
        public final Map<String,String> pageParameters = ApexPages.currentPage().getParameters();

    public VFC_WorkDetailsInter(ApexPages.StandardController controller) {

    }
    public PageReference Init(){
    
        
        //User u=[select id, name from user where id=:UserInfo.getUserId()];
        String url='/a2G/e';
        PageReference pref = new PageReference(url);
          // add parameters from the initial context (except <save_new>)
        for(String param : pageParameters.keySet())
            pref.getParameters().put(param, pageParameters.get(param));
        if(pref.getParameters().containsKey('save_new'))
            pref.getParameters().remove('save_new');      
        //pref.getParameters().put('retURL',ApexPages.currentPage().getParameters().get('retURL'));
       
        pref.getParameters().put('nooverride','1');
        try{
            SVMXC__Service_Group_Members__c  sgm= [select id, Name ,SVMXC__Salesforce_User__c from SVMXC__Service_Group_Members__c where SVMXC__Salesforce_User__c =:UserInfo.getUserId() limit 1 ];
            if(sgm!= null){
                pref.getParameters().put(System.label.CLMAY13SRV43,sgm.Name);
                pref.getParameters().put(System.label.CLMAY13SRV44,sgm.id);
            }
        }
        catch(Exception e){
            
        }
        
        
         
        pref.setRedirect(true);
        
        return pref ;
    }

}