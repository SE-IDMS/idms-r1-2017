/**

*/

@isTest
private class VFC_AccountSearchByPartner_Test{

    static testMethod void testVFC_AccountSearchByPartner() {
        System.debug('#### START test method for VFC02_AccountSearch ####');
        // create unit country 
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        // create unit stateProv 
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;
        
        // start test
        PageReference vfPage = Page.VFP_AccountSearchByPartner;
        Test.setCurrentPage(vfPage);
        
       // Add parameters to page URL  
        ApexPages.currentPage().getParameters().put('retURL', '/001');
        ApexPages.currentPage().getParameters().put('save_new', '1');               
        Account account = new Account();
        
        VFC_AccountSearchByPartner vfCtrl = new VFC_AccountSearchByPartner(new ApexPages.StandardController(account));
        //Start of December Release Modification
        VFC_ControllerBase basecontroller = new VFC_ControllerBase();
        VCC06_DisplaySearchResults componentcontroller1 =vfCtrl.resultsController; 
        //End of December Release Modification
        System.debug('vfCtrl.hasNext: ' + vfCtrl.hasNext);
        
        // 1/ test methods without setting the account fields
        vfCtrl.doSearchAccount();
        
        // 2/ test methods with all account fields
        account.Name = 'TEST VFC02_AccountSearch';
        account.AccountLocalName__c = 'Account Local Name';
        account.Street__c = 'Street';
        account.ZipCode__c = 'zipcode';
        account.City__c = 'city';
        account.StateProvince__c = stateProv.Id;
        account.Country__c = country.Id;
        account.POBox__c = 'pobox';
        account.POBoxZip__c = 'poboxzip';
        List<Account> accs = new List<Account>();
        for(Integer i=0;i<20;i++)
        {
        Account a = new Account();
        a.Name = 'TEST VFC02_AccountSearch';
        a.AccountLocalName__c = 'Account Local Name';
        a.Street__c = 'Street';
        a.ZipCode__c = 'zipcode';
        a.City__c = 'city';
        a.StateProvince__c = stateProv.Id;
        a.Country__c = country.Id;
        a.POBox__c = 'pobox';
        a.POBoxZip__c = 'poboxzip';
            accs.add(a);
            
        }
        insert accs;
        vfCtrl.doSearchAccount();
        ApexPages.currentPage().getParameters().put('soql', '1');
        vfCtrl.getSearchString(); //Modified for December Release
        vfCtrl.doSearchAccount();
        
        vfCtrl.continueCreation();
        System.debug('#### END test method for VFC02_AccountSearch ####');
    }
}