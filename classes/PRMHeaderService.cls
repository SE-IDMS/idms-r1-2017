@RestResource(urlMapping='/prm/*')
                       
global without sharing class PRMHeaderService {

   
    global static string CFG_SEPARATOR ='_';
   @HttpGet
 global static String getMenuTree() {
        RestResponse res = RestContext.response;
        res.addHeader('Access-Control-Allow-Origin', '*');
        res.addHeader('Content-Type', 'application/json');
        String contactId = RestContext.request.params.get('contactid'); 
        Contact currentContact = null;
        
        FieloEE__Program__c program = null;
        String size;
         //soql = ''+results.size();
        if(contactId!=null && (contactId.length() == 15 || contactId.length() == 18) && Pattern.matches('^[a-zA-Z0-9]*$', contactId)) {
            Contact[] contacts = [Select Id,FieloEE__Member__r.FieloEE__FirstName__c,FieloEE__Member__r.FieloEE__LastName__c,Name,FieloEE__Member__r.F_PRM_MemberFeatures__c,FieloEE__Member__r.FieloEE__Points__c,FieloEE__Member__r.FieloEE__User__c from Contact where Id=:contactId];
            size= contacts.size()+' ';
            if(contacts.size()>0){
                currentContact = contacts[0];
                program = FieloPRM_UTILS_ProgramMethods.getProgramByContact(contactId);
            }
        }

        
        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartObject();
         if(currentContact!=null){
            String partnersName = currentContact.FieloEE__Member__r.FieloEE__FirstName__c + ' ' + currentContact.FieloEE__Member__r.FieloEE__LastName__c;
            if(partnersName.length() > 20){
                partnersName = currentContact.FieloEE__Member__r.FieloEE__FirstName__c;
                if(partnersName.length() > 20){
                    partnersName = currentContact.FieloEE__Member__r.FieloEE__FirstName__c.abbreviate(20);
                }
            } 
             generator.writeStringField('Name',partnersName);
             if(currentContact.FieloEE__Member__r.F_PRM_MemberFeatures__c!= null && currentContact.FieloEE__Member__r.F_PRM_MemberFeatures__c.contains(label.CLPRMMAR16009)){
                    generator.writeBooleanField('isReward',true);
                    generator.writeStringField('Points',String.valueOf(currentContact.FieloEE__Member__r.FieloEE__Points__c)+' '+System.Label.CLPRMMAR16007);
                }else{
                    generator.writeBooleanField('isReward',false);
                }
             generator.writeStringField('checkNotificationCL', System.Label.CLAPR16PRM02);
             Integer notifCount = currentContact.FieloEE__Member__r.FieloEE__User__c!=null?AP_PRMUtils.GetNotifications(currentContact.FieloEE__Member__r.FieloEE__User__c).count:0;
             generator.writeNumberField('notifCount',notifCount);
        }else{
            generator.writeBooleanField('isReward',false);
            generator.writeStringField('Name','Unknown contact 2: '+ contactId+ ' '+ ' '+size);
        }

        String plCfg = RestContext.request.params.get('plCfg');
        if(plCfg!=null && plCfg.contains(CFG_SEPARATOR)){
            List<String> parts = plCfg.split(CFG_SEPARATOR, 2);
            plCfg = parts.get(0);
            String locator=parts.get(1); //Should be nothing or 2

            if(plCfg!=null && (plCfg.length() == 15 || plCfg.length() == 18) && Pattern.matches('^[a-zA-Z0-9]*$', plCfg)) {
                
                List<FieloPRM_PartnerLocatorConfiguration__c> cmp =  [select id,F_PRM_BusinessTypeIncludeValues__c,F_PRM_BusinessTypeExcludeValues__c,F_PRM_AreaOfFocusIncludeValues__c,F_PRM_AreaOfFocusExcludeValues__c,F_PRM_BusinessTypeIncludeValues2__c,F_PRM_BusinessTypeExcludeValues2__c,F_PRM_AreaOfFocusIncludeValues2__c,F_PRM_AreaOfFocusExcludeValues2__c,F_PRM_DistanceValues__c,F_PRM_DistanceValues2__c,F_PRM_SearchFilterValues__c,F_PRM_SearchFilterValues2__c from FieloPRM_PartnerLocatorConfiguration__c where id=:plCfg limit 1]; 
                system.debug('#cmpSize:'+cmp.size());
                List<String> buty = new List<String>();
                List<String> myAreaOfFocus = new List<String>();
                List<String> distanceValues = new List<String>();
                List<String> searchFilterList = new List<String>();

                if(cmp.size()==1){
                    System.debug('cmp[0]: '+cmp[0]);
                    if(cmp[0].get('F_PRM_BusinessTypeIncludeValues'+locator+'__c')!=null){
                        Set<String> businessTypeIncludeValues = FieloPRM_C_PartnerLocatorFormController.fromTextAreaToSet((String)cmp[0].get('F_PRM_BusinessTypeIncludeValues'+locator+'__c'));
                        Schema.DescribeFieldResult buTyDescribe = Account.PLBusinessType__c.getDescribe();
                        for(Schema.PicklistEntry butyPl : buTyDescribe.getPicklistValues())
                        {
                            System.debug('butyPl Value: '+butyPl.getValue());
                            System.debug(businessTypeIncludeValues+' Contains: '+butyPl.getValue() + ' = '+businessTypeIncludeValues.contains(butyPl.getValue()));
                            if(!businessTypeIncludeValues.contains(butyPl.getValue())){
                                 buty.add(butyPl.getValue());                 
                            }
                        }
                    }else{
                        if(cmp[0].get('F_PRM_BusinessTypeExcludeValues'+locator+'__c')!=null){
                            Set<String> businessTypeExcludeValues = FieloPRM_C_PartnerLocatorFormController.fromTextAreaToSet((String)cmp[0].get('F_PRM_BusinessTypeExcludeValues'+locator+'__c'));
                            System.debug('#businessTypeExcludeValues: '+businessTypeExcludeValues);
                            buty.addAll(businessTypeExcludeValues);
                        }
                    }

                    if(cmp[0].get('F_PRM_AreaOfFocusIncludeValues'+locator+'__c')!=null){
                        Set<String> areaOfFocusIncludeValues = FieloPRM_C_PartnerLocatorFormController.fromTextAreaToSet((String)cmp[0].get('F_PRM_AreaOfFocusIncludeValues'+locator+'__c'));
                        Schema.DescribeFieldResult aofoDescribe = Account.PLAreaOfFocus__c.getDescribe();
                        for(Schema.PicklistEntry aofoPl : aofoDescribe.getPicklistValues())
                        {
                            if(!areaOfFocusIncludeValues.contains(aofoPl.getValue())){
                                 myAreaOfFocus.add(aofoPl.getValue());                 
                            }
                        }
                    }else{
                        if(cmp[0].get('F_PRM_AreaOfFocusExcludeValues'+locator+'__c')!=null){
                            Set<String> areaOfFocusExcludeValues = FieloPRM_C_PartnerLocatorFormController.fromTextAreaToSet((String)cmp[0].get('F_PRM_AreaOfFocusExcludeValues'+locator+'__c'));
                            myAreaOfFocus.addAll(areaOfFocusExcludeValues);
                        }
                    }
                    //Distance values
                    if(cmp[0].get('F_PRM_DistanceValues'+locator+'__c')!=null){
                        List<FieloPRM_C_PartnerLocatorFormController.SelectOptionWithSelection> distanceValuesCfg = FieloPRM_C_PartnerLocatorFormController.fromTextAreaToSelectOption((String)cmp[0].get('F_PRM_DistanceValues'+locator+'__c'));
                        for(FieloPRM_C_PartnerLocatorFormController.SelectOptionWithSelection distValue: distanceValuesCfg){
                            distanceValues.add(distValue.value);
                        }
                    }
                    //SearchFilter
                    if(cmp[0].get('F_PRM_SearchFilterValues'+locator+'__c')!=null){
                        String[] tmpString = ((String)cmp[0].get('F_PRM_SearchFilterValues'+locator+'__c')).split(';');
                        for(String s : tmpString){
                           searchFilterList.add(s);
                        }
                    }

                }
                generator.writeObjectField('butyExclude',buty);
                generator.writeObjectField('aofoExclude',myAreaOfFocus);
                generator.writeObjectField('distanceValues',distanceValues);
                generator.writeObjectField('advFilters',searchFilterList);
                
            }
        }
        
        
        generator.writeDateField('Date',date.today());

        String homeUrl = program!=null?System.label.CLAPR15PRM308 +'/':'';
        generator.writeStringField('HomeUrl',homeUrl);

        String backgroundImg = 'https://partner.schneider-electric.com/partners/resource/1430634413000/FieloPRM_images/BGDefault.jpg';
        generator.writeStringField('BackgroundImg',backgroundImg);

        generator.writeFieldName('MenuTree');
        FieloPRM_UTILS_MenuMethods.MenusContainer menuContainer = FieloPRM_UTILS_MenuMethods.getMenus('Main',contactId);
        List<MenuTreeItem> mainMenu = fromFieloMenuContainerToMenuItemList(menuContainer,program,false);   
        generator.writeObject(mainMenu);
        
        generator.writeFieldName('MenuTop');
        FieloPRM_UTILS_MenuMethods.MenusContainer menuTopContainer = FieloPRM_UTILS_MenuMethods.getMenus('Top',contactId);
        List<MenuTreeItem> topMenu = fromFieloMenuContainerToMenuItemList(menuTopContainer,program,false);
        generator.writeObject(topMenu);

        generator.writeFieldName('MenuFooter');
        FieloPRM_UTILS_MenuMethods.MenusContainer menuFooterContainer = FieloPRM_UTILS_MenuMethods.getMenus('Footer',contactId);
        List<MenuTreeItem> footerMenu = fromFieloMenuContainerToMenuItemList(menuFooterContainer,program,false);
        generator.writeObject(footerMenu);

        generator.writeFieldName('MenuSocial');
        FieloPRM_UTILS_MenuMethods.MenusContainer menuSocialContainer = FieloPRM_UTILS_MenuMethods.getMenus('Social',contactId);
        List<MenuTreeItem> socialMenu = fromFieloMenuContainerToMenuItemList(menuSocialContainer,program,true);
        generator.writeObject(socialMenu);
        
        generator.writeEndObject();


        return generator.getAsString();
         
    }
    
    public static  List<MenuTreeItem> fromFieloMenuContainerToMenuItemList(FieloPRM_UTILS_MenuMethods.MenusContainer menuContainer,FieloEE__Program__c program,boolean isImg){
        List<MenuTreeItem> myMenuTreeList = new List<MenuTreeItem>();
        if(menuContainer!=null && menuContainer.parentMenus!=null){
            for(FieloEE__Menu__c menu: menuContainer.parentMenus){
                MenuTreeItem myMenuTree = createMenuItemFromFieloContext(menu,program,isImg);
                if(String.isNotBlank(myMenuTree.name)) {
                    myMenuTreeList.add(myMenuTree);
                    if(menuContainer.childMenusMap!=null && menuContainer.childMenusMap.get(menu.Id)!=null){
                        List<FieloEE__Menu__c> childMenuList= menuContainer.childMenusMap.get(menu.Id);
                        if(childMenuList.size() > 0){
                            for(FieloEE__Menu__c childMenu: childMenuList)
                                myMenuTree.addChild(createMenuItemFromFieloContext(childMenu,program,isImg));
                            }
                        }
                }
            }
        } 
         return myMenuTreeList;
    }

    static MenuTreeItem  createMenuItemFromFieloContext(FieloEE__Menu__c fieloMenu,FieloEE__Program__c program,boolean isImg){
            string url;
            if(fieloMenu.FieloEE__CustomURL__c!=null){
                if(fieloMenu.FieloEE__CustomURL__c.startsWith('http') ){
                    //An external Url -> not Relative
                    url=fieloMenu.FieloEE__CustomURL__c;
                }else{
                    //relative URL
                    url = System.label.CLAPR15PRM308 +'/Menu/'+fieloMenu.FieloEE__ExternalName__c;
                }
            }else{
                url = fieloMenu.FieloEE__ExternalName__c!=null?System.label.CLAPR15PRM308 +'/Menu/'+fieloMenu.FieloEE__ExternalName__c:System.label.CLAPR15PRM308+program.FieloEE__CustomPage__c+'?idmenu='+fieloMenu.Id;
            }
            
            String menuName;
            String acceptedlanguage;
            String language;
            if(fieloMenu.F_PRM_MultiLanguageCustomLabel__c!=null){
                //The Crazy code because there is no Label API :-(
                Pattern patternLabelAPI = Pattern.compile('<labelApi>(.+?)</labelApi>');
                Pagereference pr = Page.PRM_CustomLabel;
                if(RestContext.request != null){
                    acceptedlanguage = RestContext.request.headers.get('Accept-Language');
                    if(acceptedlanguage!=null && acceptedlanguage.length()>5){
                       acceptedlanguage=acceptedlanguage.substring(0, 5);
                    }
                }
                else
                    acceptedlanguage = userinfo.getLanguage();
                
                system.debug('acceptedlanguage:' + acceptedlanguage);  
                pr.getParameters().put('core.apexpages.devmode.url', '1');
                pr.getParameters().put('labelName', fieloMenu.F_PRM_MultiLanguageCustomLabel__c);
                pr.getParameters().put('ulang', acceptedlanguage);
                String xmlContent = Test.isRunningTest()?'<labelApi>'+fieloMenu.F_PRM_MultiLanguageCustomLabel__c+'</labelApi>':pr.getContent().toString();
                system.debug('-----'+xmlContent);
                Matcher mchr = patternLabelAPI.matcher(xmlContent);
                if(mchr.find()) {
                    System.Debug('*** Label: ' +mchr.group(1));
                    menuName = mchr.group(1);
                }else{
                    throw new PRMHeaderException('Problem with the label: '+fieloMenu.F_PRM_MultiLanguageCustomLabel__c);
                }
            }else{
                system.debug('inside else');
                if(RestContext.request != null)
                    language = RestContext.request.headers.get('Accept-Language');
                else
                    language = userinfo.getLanguage();
                    
                    if(language!=null && language.length()>2){
                       language=language.substring(0, 2);
                    }else if(language==null){
                        language ='en';
                    }
                
                system.debug('language:' + language);
                    
                menuName = (String)fieloMenu.get('Name_'+language+'__c'); //Name_RU__c for example
            } 
            if(isImg && fieloMenu.FieloEE__AttachmentId__c!=null){
                menuName = '<img style="margin:5px;height:30px;" src="'+System.label.CLAPR15PRM308 +'/'+'servlet/servlet.ImageServer?id='+fieloMenu.FieloEE__AttachmentId__c+'&oid='+UserInfo.getOrganizationId()+'" />';
            }

            return new MenuTreeItem(menuName,url,fieloMenu.FieloEE__ExternalName__c);
    }
    
    global class MenuTreeItem {
        global String name { get; set; }
        global String url { get; set; }
        global String externalName { get; set; }
        global List<MenuTreeItem> childs { get; set; }
        
        public MenuTreeItem(String name, String url){
            this.name=name;
            this.url = url;
            this.childs = new List<MenuTreeItem> ();
        }

        public MenuTreeItem(String name, String url,String externalName){
            this.name=name;
            this.url = url;
            this.externalName = externalName;
            this.childs = new List<MenuTreeItem> ();
        }
        
        global void addChild(MenuTreeItem child){
            this.childs.add(child);
        }
    }


    global class PRMHeaderException extends Exception{

    }
   
    
}