/*
Author : Shruti Karn
Date : 2 Dec 2012
Description : To update all the Technician having same Salesforce User with Accept/Reject if one of the Assigned Tools/Technician is updated.
BR-7889 : Oct 2015
    1, WO status is updated (or remains) to “Customer Confirmed”
    2, WO sub-status is updated to “To be rescheduled”
    3, Rescheduled reason is updated to “FSR rejected”
    4, FSR response is updated to “Reject”
    5, WO owner is updated to the owner prior to Customer Confirmed status change

*/

public class AP_ATT_UpdateTechnician
{
    public static void updateTechnicianStatus(map<Id,AssignedToolsTechnicians__c> mapATT){}
   /* public static void updateTechnicianStatus(map<Id,AssignedToolsTechnicians__c> mapATT)
    {
       
        
        map<Id, AssignedToolsTechnicians__c> mapOwnerATT = new map<Id,AssignedToolsTechnicians__c>();
        set<Id> setWorkOrderID = new set<Id>();
        set<Id> setWorkOrderRejectedID = new set<Id>();
        list<AssignedToolsTechnicians__c> lstUpdateATT = new list<AssignedToolsTechnicians__c>();
        try
        {
            list<AssignedToolsTechnicians__c> lstAllATT = [Select WorkOrder__c,status__c,DrivingTime__c,DrivingTimeHome__c,ReasonforRejection__c, TechnicianEquipment__r.SVMXC__Salesforce_User__c from AssignedToolsTechnicians__c where id in : mapATT.keySet() limit 10000];
            
            for(AssignedToolsTechnicians__C technician : lstAllATT )
            {
                mapOwnerATT.put(technician.TechnicianEquipment__r.SVMXC__Salesforce_User__c , mapATT.get(technician.Id));
                setWorkOrderID.add(technician.WorkOrder__c);
                
            }
            if(lstAllATT != null && lstAllATT.size() == 1){
                
                System.debug(' \n ************************************************* Test ***************************************');
                // BR-7889 Oct 2015 Start
                if(mapATT.get(lstAllATT[0].id).status__c == 'Rejected')
                {
                
                    System.debug(' \n ************************************************* Rejected ***************************************');
                    setWorkOrderRejectedID.add(mapATT.get(lstAllATT[0].id).WorkOrder__c) ;  
                    
                    SVMXC__Service_Order__c workorder = [select id,SVMXC__Group_Member__c, AcceptedFSRIds__c, SubStatus__c,SVMXC__Dispatch_Response__c,IsRejected__c,TechPreviousOwner__c,Tech_PreviousOwner__c,SVMXC__Order_Status__c ,RescheduleReason__c from SVMXC__Service_Order__c where id = : mapATT.get(lstAllATT[0].id).WorkOrder__c ];
                                   
                    //Added for BR-8537
                    if(mapATT.get(lstAllATT[0].id).PrimaryUser__c == true)
                    {
                        if(workorder.SVMXC__Order_Status__c == 'Acknowledge FSE')
                         {                         
                            workorder.SVMXC__Order_Status__c  = 'Customer Confirmed' ;
                            if(workorder.TechPreviousOwner__c != null)
                            {
                                system.debug('\n '+id.valueOf(workorder.TechPreviousOwner__c));
                                workorder.OwnerId = id.valueOf(workorder.TechPreviousOwner__c);
                            }
                             System.debug('\n workorder.OwnerId '+workorder.OwnerId);
                         }
                     
                         workorder.SVMXC__Dispatch_Response__c = 'Rejected';
                        // /*workorder.SubStatus__c = 'FSR Rejection'; //DEF-8239 
                         //workorder.RescheduleReason__c = 'FSR Rejected'; // Def-8295
                         //workorder.Reason_for_Rejection__c  = mapATT.get(lstAllATT[0].id).ReasonforRejection__c;
                        // workorder.SVMXC__Group_Member__c = null;
                        // workorder.IsRejected__c = true;
                         //workorder.SVMXC__Group_Member__c = null;
                        // workorder.AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder.AcceptedFSRIds__c,mapATT.get(lstAllATT[0].id).FSR_Salesforce_User__c, 'remove');                  
                         //update workorder;
                     } else {
                        // we still want to update the WO even if its not the primary to remove the userid from the AcceptedFSRIds field
                        workorder.AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder.AcceptedFSRIds__c,mapATT.get(lstAllATT[0].id).FSR_Salesforce_User__c, 'remove');
                        //update workorder;
                     }
                     //workorder.SVMXC__Dispatch_Response__c = 'Rejected';
                     workorder.AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder.AcceptedFSRIds__c,mapATT.get(lstAllATT[0].id).FSR_Salesforce_User__c, 'remove');                       
                     workorder.SubStatus__c = 'FSR Rejection'; //DEF-8239 
                     //workorder.RescheduleReason__c = 'FSR Rejected'; // Def-8295
                     //workorder.Reason_for_Rejection__c  = mapATT.get(lstAllATT[0].id).ReasonforRejection__c;
                     //workorder.SVMXC__Group_Member__c = null;
                     workorder.IsRejected__c = true;
                     System.debug('**************************Hari**********************************' +workorder);
                    update workorder;
                    //String workorderString = JSON.serialize(workorder);
                    //updateWorkOrder(workorderString);                  
                    System.debug('**************************Hari**********************************' +workorder);
                     
                     //End: BR-8537
                     //Delete Events and SVMX Events
                     Id sfId = lstAllATT[0].TechnicianEquipment__r.SVMXC__Salesforce_User__c;
                     List<Event> Evntlst = [Select Id from Event where WhatId =:workorder.Id AND OwnerId=:sfId];
                     if(Evntlst.size()>0) delete Evntlst;
                    
                     List<SVMXC__SVMX_Event__c> svmxent = [Select Id from SVMXC__SVMX_Event__c where SVMXC__WhatId__c =:workorder.Id AND OwnerId=:sfId];
                     if(svmxent.size()>0) delete svmxent;
                     //End:Delete Events and SVMX Events
                     
                     //AP_WorkOrderTechnicianNotification.ReNotifyRejectedTechnicians(lstAllATT[0]);
                    
                }
                else if(mapATT.get(lstAllATT[0].id).status__c == 'Accepted')
                {
                
                    System.debug(' \n ************************************************* Accepted ***************************************');                    
                    List<SVMXC__Service_Order__c> workorder = [select id,AcceptedFSRIds__c, SubStatus__c,SVMXC__Dispatch_Response__c,IsRejected__c,Tech_PreviousOwner__c,SVMXC__Order_Status__c ,RescheduleReason__c from SVMXC__Service_Order__c where id = : mapATT.get(lstAllATT[0].id).WorkOrder__c  ];
                    if(workorder != null && workorder.size()>0) 
                    {                        
                        workorder[0].IsRejected__c = false;     
                        workorder[0].AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder[0].AcceptedFSRIds__c,mapATT.get(lstAllATT[0].id).FSR_Salesforce_User__c, 'add');               
                        update workorder;
                        }
                }
                 // BR-7889 Oct 2015 End
                
            }
            lstUpdateATT = [Select Status__c, WorkOrder__c, reasonforrejection__c, TechnicianEquipment__r.SVMXC__Salesforce_User__c from AssignedToolsTechnicians__c where TechnicianEquipment__r.SVMXC__Salesforce_User__c in : mapOwnerATT.keySet() and WorkOrder__c in :setWorkOrderID and id not in :mapATT.keySet() limit 10000];
            for(AssignedToolsTechnicians__c tech : lstUpdateATT )
            {
                tech.Status__c = mapOwnerATT.get(tech.TechnicianEquipment__r.SVMXC__Salesforce_User__c).status__c;
                tech.reasonforrejection__c = mapOwnerATT.get(tech.TechnicianEquipment__r.SVMXC__Salesforce_User__c).reasonforrejection__c;
            }
            //update lstUpdateATT; //BR-7889 Commented
            
            
        }
        catch(Exception e)
        {
            System.debug('##### Exception ' + e);
            for(Id attId : mapATT.keySet())
                mapATT.get(attId).addError(e.getMessage());   
        }
    }*/
    
    public static void updateTechnicianStatus2(map<Id,AssignedToolsTechnicians__c> mapATT)
    {
       
        
        map<Id, AssignedToolsTechnicians__c> mapOwnerATT = new map<Id,AssignedToolsTechnicians__c>();
        set<Id> setWorkOrderID = new set<Id>();
        set<Id> setWorkOrderRejectedID = new set<Id>();
        list<AssignedToolsTechnicians__c> lstUpdateATT = new list<AssignedToolsTechnicians__c>();
        //list<AssignedToolsTechnicians__c> RejectedATT = new list<AssignedToolsTechnicians__c>();
        //list<AssignedToolsTechnicians__c> AcceptedATT = new list<AssignedToolsTechnicians__c>();
        Map<id,SVMXC__Service_Order__c> woMap = new Map<id,SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order__c> workorders = new List<SVMXC__Service_Order__c>();
        //List<SVMXC__Service_Order__c> workOrdersToUpdate = new List<SVMXC__Service_Order__c>();
        Set<id> widset = new Set<id>();
        Set<id> useridset = new Set<id>();
                
        //DEF-10708 (Q3 Sept 2016) Accept/Reject new functionality issue
        List<Event> eventsToDelete = new List<Event>(); 
        List<SVMXC__SVMX_Event__c> sMaxEventsToDelete = new List<SVMXC__SVMX_Event__c>();
        
        try
        {
            list<AssignedToolsTechnicians__c> lstAllATT = [Select EventID__c, WorkOrder__c,PrimaryUser__c,status__c,DrivingTime__c,DrivingTimeHome__c,ReasonforRejection__c, TechnicianEquipment__r.SVMXC__Salesforce_User__c from AssignedToolsTechnicians__c where id in : mapATT.keySet() limit 10000];
            Map<id,AssignedToolsTechnicians__c> attmap = new Map<id,AssignedToolsTechnicians__c>();
            for(AssignedToolsTechnicians__C technician : lstAllATT )
            {
                mapOwnerATT.put(technician.TechnicianEquipment__r.SVMXC__Salesforce_User__c , mapATT.get(technician.Id));
                setWorkOrderID.add(technician.WorkOrder__c);                
                attmap.put(technician.id,technician);
            }
            if(setWorkOrderID != null && setWorkOrderID.size()>0){
                workorders = [select id,SVMXC__Group_Member__c, AcceptedFSRIds__c, SubStatus__c,SVMXC__Dispatch_Response__c,IsRejected__c,TechPreviousOwner__c,Tech_PreviousOwner__c,SVMXC__Order_Status__c ,RescheduleReason__c from SVMXC__Service_Order__c where id in : setWorkOrderID];
                woMap.putAll(workorders);
            }
            
        	// DEF-10874 (AMO - Q3 Sept 2016) : don't remove the Primary FSR if partial rejections
        	Integer acceptedPrimaryFSR = 0;
        	Integer rejectedPrimaryFSR = 0;
        	SVMXC__Service_Order__c workorder = null;
            
            for(AssignedToolsTechnicians__c att: mapATT.values())
            {            	
                AssignedToolsTechnicians__c attobj = attmap.get(att.id);
                workorder = woMap.get(attobj.WorkOrder__c);
                System.debug('#### AMO : attobj.status__c : ' + attobj.status__c); 
                if(attobj.status__c == 'Rejected')
                {               
                    System.debug('#### AMO : attobj.PrimaryUser__c : ' + attobj.PrimaryUser__c);  
                    //Added for BR-8537
                    if(attobj.PrimaryUser__c == true)
                    {
                    	System.debug('#### AMO : workorder.SVMXC__Order_Status__c : ' + workorder.SVMXC__Order_Status__c); 
                        /*
                        if(workorder.SVMXC__Order_Status__c == 'Acknowledge FSE')
                         {  
                         	// DEF-10874 (AMO - Q3 Sept 2016) : don't remove the Primary FSR if partial rejections                       
                            workorder.SVMXC__Order_Status__c  = 'Customer Confirmed' ;
                            System.debug('#### AMO : workorder.TechPreviousOwner__c : ' + workorder.TechPreviousOwner__c); 
                            if(workorder.TechPreviousOwner__c != null)
                            {
                                system.debug('\n '+id.valueOf(workorder.TechPreviousOwner__c));
                                workorder.OwnerId = id.valueOf(workorder.TechPreviousOwner__c);
                            }
                            System.debug('\n workorder.OwnerId '+workorder.OwnerId);
                         }
                         */
                         
                         workorder.SVMXC__Dispatch_Response__c = 'Rejected';
                         
                         // DEF-10874 (AMO - Q3 Sept 2016) : don't remove the Primary FSR if partial rejections
                         //workorder.SVMXC__Group_Member__c = null;
                         rejectedPrimaryFSR++;
                         
                         //workorder.AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder.AcceptedFSRIds__c,attobj.FSR_Salesforce_User__c, 'remove');
                    }
                    else{
                         //workorder.AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder.AcceptedFSRIds__c,attobj.FSR_Salesforce_User__c, 'remove');
                    }
                    //workorder.AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder.AcceptedFSRIds__c,att.FSR_Salesforce_User__c, 'remove');  
                    workorder.AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder.AcceptedFSRIds__c, att.FSR_User_Id_via_Tech_Object__c, 'remove'); 
                    workorder.SubStatus__c = 'FSR Rejection'; //DEF-8239
                    useridset.add(attobj.TechnicianEquipment__r.SVMXC__Salesforce_User__c);
                    workorder.Reason_for_Rejection__c = att.ReasonforRejection__c; 
                    workorder.RescheduleReason__c = 'FSR Rejected';
                    workorder.SVMXC__Dispatch_Response__c = null; 
                    widset.add(workorder.id);
                    
                    //DEF-10708 (Q3 Sept 2016) Accept/Reject new functionality issue
                    if(attobj.EventID__c != null) {
                    	if(attobj.EventID__c.substring(0,3) == '00U')
                    		eventsToDelete.add(new Event(Id=attobj.EventID__c));
                    	else
                    		sMaxEventsToDelete.add(new SVMXC__SVMX_Event__c(Id=attobj.EventID__c));
                    }
                }
                else if(attobj.status__c == 'Accepted'){     
                	
                	System.debug('#### AMO : attobj.PrimaryUser__c : ' + attobj.PrimaryUser__c); 
                		
                	// DEF-10874 (AMO - Q3 Sept 2016) : don't remove the Primary FSR if partial rejections
                	if(attobj.PrimaryUser__c == true)
                		acceptedPrimaryFSR++;
                		          
                    workorder.IsRejected__c = false; 
                    //workorder.SVMXC__Dispatch_Response__c = null;                    
                    //workorder.AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder.AcceptedFSRIds__c,att.FSR_Salesforce_User__c, 'add');  
                    workorder.AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder.AcceptedFSRIds__c, att.FSR_User_Id_via_Tech_Object__c, 'add');  
                
                }
                // workOrdersToUpdate.add(workorder);
                
            }//end for
 
            // DEF-10874 (AMO - Q3 Sept 2016) : don't remove the Primary FSR if partial rejections
            System.debug('### AMO acceptedPrimaryFSR : ' + acceptedPrimaryFSR);
            System.debug('### AMO rejectedPrimaryFSR : ' + rejectedPrimaryFSR);
            if(acceptedPrimaryFSR == 0 && rejectedPrimaryFSR > 0)
            {
            	workorder.SVMXC__Group_Member__c = null;
            	//workorder.AcceptedFSRIds__c = updateAcceptFSRIdsField(workorder.AcceptedFSRIds__c, att.FSR_User_Id_via_Tech_Object__c, 'remove'); 
	            if(workorder.SVMXC__Order_Status__c == 'Acknowledge FSE')
	             { 
	             	workorder.SVMXC__Order_Status__c  = 'Customer Confirmed' ;
	                System.debug('#### AMO : workorder.TechPreviousOwner__c : ' + workorder.TechPreviousOwner__c); 
	                if(workorder.TechPreviousOwner__c != null)
	                {
	                    system.debug('\n '+id.valueOf(workorder.TechPreviousOwner__c));
	                    workorder.OwnerId = id.valueOf(workorder.TechPreviousOwner__c);
	                }
	                System.debug('\n workorder.OwnerId '+workorder.OwnerId);
	             }
            }            	

            
            System.debug('*************************************************');
            //System.debug('** \n'+woMap.values());
            //System.debug('** \n ' + workOrdersToUpdate);
            //DataBase.update(woMap.values(),false);
            //update workOrdersToUpdate;
            //update woMap.values // Commented on 02-03-2016
            Database.update(woMap.values(), false);
            //System.debug('**(after)** \n ' + workOrdersToUpdate);
            //System.debug('** \n'+woMap.values());
            System.debug('*************************************************');
            //End: BR-8537
            
           	//DEF-10708 (Q3 Sept 2016) Accept/Reject new functionality issue
            	// => no longer delete events based on the WO id, it should delete only events for the rejected ATT            
            //Delete Events and SVMX Events
            //if(useridset != null && useridset.size()>0 && widset != null && widset.size()>0)
            //{
                //List<Event> Evntlst = [Select Id from Event where WhatId in :widset AND OwnerId in:useridset];
                     //if(Evntlst.size()>0) delete Evntlst;
                //List<SVMXC__SVMX_Event__c> svmxent = [Select Id from SVMXC__SVMX_Event__c where SVMXC__WhatId__c in:widset AND OwnerId in:useridset];
                //if(svmxent.size()>0) delete svmxent;
            //} 
            
            //DEF-10708 (Q3 Sept 2016) Accept/Reject new functionality issue
            // => no longer delete events based on the WO id, it should delete only events for the rejected ATT
            // Need to be in Database because the related events may already be deleted
            if(eventsToDelete.size()>0)
            	Database.delete(eventsToDelete, false); 
            if(sMaxEventsToDelete.size() > 0)
            	Database.delete(sMaxEventsToDelete, false);
            
            //End:Delete Events and SVMX Events      
            
            // BR-7889 Oct 2015 End
            
        }
        catch(Exception e)
        {
            System.debug('##### Exception ' + e);
            for(Id attId : mapATT.keySet())
                mapATT.get(attId).addError(e.getMessage());   
        }
    }
    
   // @future
    /*public static void updateWorkOrder(String wokorderStr){
         List<SVMXC__Service_Order__c> woList =(List<SVMXC__Service_Order__c>)JSON.deserialize(wokorderStr,List<SVMXC__Service_Order__c>.class);
         update woList;
    }*/
    
    @TestVisible private static String updateAcceptFSRIdsField(String currentField, String userId, String operation) {
        
        System.debug('#### incoming userId ' + userId);
        System.debug('#### incoming currentField ' + currentField);
        System.debug('#### operation ' + operation);
        String returnString = currentField;
        
        if (returnString == null)
            returnString = '';
        
        if (operation == 'remove') {
            if (returnString.contains(userId))
                returnString = returnString.remove(userId);            
        } else if (operation == 'add') {
            if (!returnString.contains(userId))
                returnString += userId;
        }
        
        System.debug('#### returnString ' + returnString);
        return returnString;
    }
    
    public static void updatePrimaryFSR(list<AssignedToolsTechnicians__c >attlist){
        for(AssignedToolsTechnicians__c  att:attlist){
           if(att.TechnicianEquipment__c==att.WorkorderPrimaryFSR__c)
                   att.PrimaryUser__c=true;
       }
    }
}