public with sharing class ScorecardDMLQuestionnaireCont {

    public ScorecardDMLQuestionnaireCont(ApexPages.StandardController controller) {
    isEdit = False;
    isView = True;
    isSave = True;
    SelectedQuarter = 'Q1';
    }
    
 public string Answer{get;set;}
 public boolean isEdit{get;set;}
 public boolean isView{get;set;}
 public boolean isSave{get;set;}
 public string pageid{get;set;}
 public string SelectedQuarter{get;set;}
 public  List<Cascading_KPI_Target__c> Cntry = new List<Cascading_KPI_Target__c>();
 public  List<Cascading_KPI_Target__c> CntryKPI = new List<Cascading_KPI_Target__c>();
 public String year;
 public List<DMLAnswers> SelAns = new List<DMLAnswers>();
 public List<DMLAnswers> getDMLAnswers(){
    
 List<DMLAnswers> DMLAns = new List<DMLAnswers>();
    
 List<DML_Questionnaire__c> DMLQs = new List<DML_Questionnaire__c>();
 List<DML_Answers__c> DMLAs = new List<DML_Answers__c>();
 
 Pageid = ApexPages.currentPage().getParameters().get('Id');
 CntryKPI = [Select Name, Year__c from Cascading_KPI_Target__c where id = :Pageid];
 year = CntryKPI[0].Year__c;
 DMLQs = [Select id,Name, Question__c,Other_Owner__c, category__c from DML_Questionnaire__c WHERE Year__c = :year order by Question_Numder__c];
 
  //  Pageid = ApexPages.currentPage().getParameters().get('Id');
    
    Cntry = [Select Name, CountryPickList__c from Cascading_KPI_Target__c where id = :Pageid];

    System.debug('CountryPicklist:'+Cntry[0].CountryPickList__c);
    System.debug('Quarter:'+SelectedQuarter);
    System.debug('Entity Target:'+pageid);

    DMLAs = [Select Name, Question_ID__c, Answerpicklist__c, Remarks__c from DML_Answers__c where Entity__c = :Cntry[0].CountryPickList__c and Quarter__c = :SelectedQuarter and Entity_Target__r.id = :Pageid];
    System.debug('DMLAs:'+DMLAs);
    integer Ansfound;
    string Qowner;
    For(DML_Questionnaire__c Qs : DMLQs)
    {
      AnsFound = 0;
      
     Qowner = Qs.Other_Owner__c;     
     For(DML_Answers__c Asw : DMLAs){
     System.debug('Answer:'+Asw);
      if ((Asw.Question_ID__c == Qs.Name)){
      System.Debug('Question:'+Qs.Name);
        if(Asw.Answerpicklist__c == 'Yes')
          DMLAns.add(new DMLAnswers(Qs.id,Qs.Name, Qs.Question__c, 'Yes', SelectedQuarter,Qowner,Asw.Remarks__c,Qs.Category__c));
        else  if(Asw.Answerpicklist__c == 'NA')
         DMLAns.add(new DMLAnswers(Qs.id,Qs.Name, Qs.Question__c, 'NA', SelectedQuarter,Qowner,Asw.Remarks__c,Qs.Category__c));
        else 
         DMLAns.add(new DMLAnswers(Qs.id,Qs.Name, Qs.Question__c, 'No', SelectedQuarter,Qowner,Asw.Remarks__c,Qs.Category__c));
         AnsFound = 1;
       }// if Ends
      }
      
      // No answer fond 
      
       if (AnsFound == 0)
        DMLAns.add(new DMLAnswers(Qs.id,Qs.Name, Qs.Question__c, 'No',SelectedQuarter,Qowner,'',Qs.Category__c ));
        SelAns = DMLAns;
    }
    System.debug('AnsFound:'+AnsFound);
    return DMLAns;
  }
  
 Public pagereference QuarterChanged(){
  return null;
  }
    
  public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Yes','Yes'));
            options.add(new SelectOption('No','No'));
            options.add(new SelectOption('NA','NA'));
            return options;
        }
        
  public List<SelectOption> getQuarter() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Q1','Q1'));
            options.add(new SelectOption('Q2','Q2'));
            options.add(new SelectOption('Q3','Q3'));
            options.add(new SelectOption('Q4','Q4'));
            return options;
        }
        
        public void change()
         {
           isEdit = True;
           isView = False;
           isSave = False;
          }
        
        public pagereference cancel()
         {
           isEdit = False;
           isView = True;
           isSave = True;
           return null;
          }
          
       public pagereference save()
        {
           List<DML_Answers__c> NewAns = new List<DML_Answers__c>();           
           List<DML_Answers__c> DelAns = new List<DML_Answers__c>();           
           isEdit = False;
           isView = True;
           isSave = True;
          if(Cntry[0].CountryPickList__c !=null)
          {
           DelAns = [Select id,Name, Question_ID__c, Answerpicklist__c from DML_Answers__c where Entity__c = :Cntry[0].CountryPickList__c and Quarter__c = :SelectedQuarter  and Entity_Target__r.id = :Pageid];
          For(DML_Answers__c Asw : DelAns){
          
             Delete Asw;
          
           }
          
             for(DMLAnswers nAns:SelAns)
              {
                DML_Answers__c insAns = new DML_Answers__c();
               
                insAns.Question_ID__c = nAns.Qsid;
                insAns.DML_Questionnaire__c = nAns.Qid;
                insAns.Quarter__c = nAns.Qrt;
                insAns.Remarks__c = nAns.Remark;
                insAns.Entity__c = Cntry[0].CountryPickList__c;
                insAns.Entity_Target__c = Pageid;
                if (nAns.Ans1 == 'Yes')
                 insAns.Answerpicklist__c = 'Yes';
                else  if (nAns.Ans1 == 'NA')
                 insAns.Answerpicklist__c = 'NA'; 
                else  
                  insAns.Answerpicklist__c = 'No'; 
                 
                 NewAns.add(insAns);               
              }
                
           }
                 insert NewAns;
                 
           return null;
         }
    
     Public class DMLAnswers{
     Public Id Qid{get;set;}
     Public String Qsid{get;set;}
     Public String Quest{get;set;}
     Public String Ans1{get;set;}
     Public String Qrt{get;set;}
     Public String Owner{get;set;}
     Public String Remark{get;set;}
     Public String Categ{get;set;}
     
     Public DMLAnswers(Id qstid, string qid, string qst, string answ1,string qtr,string own, string rem,string cat){
      this.Qsid = qid;
      this.Quest = qst;
      this.Ans1 = answ1;
      this.Qrt = qtr;
      this.Owner = own;
      this.Remark = rem;
      this.Categ = cat;
      this.Qid = qstid;
      
      }
     }
   }