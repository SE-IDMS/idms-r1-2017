/*
    Author          : ACN
    Date Created    : 24/07/2011
    Description     : Class utilized by LeadBeforeInsert, LeadBeforeUpdate and LeadAfterUpdate  triggers.
    18/Apr/2012    Srinivas Nallapati    June Mkt    Added new method "AssociateRelatedLeadsToContact()"
    14-Mar-2013    Pooja Gupta           May13 Mkt   Added new method 'AP19_ReassignLeadsToContact()'
*/
public class AP19_Lead
{
     public static boolean isConvertion = false;
    /* This method is called from LeadBeforeInsert & LeadBeforeUpdate Triggers.
     * This method queries the Marketing Callback Limits for that lead's priority and fetches the CallBack Limits
     * for the corresponding priority.
     */
    public static void populateCallBack(Set<Lead> leads, set<Decimal> leadPriority)
    {
        system.debug('****** Start of populateCallBack method ******');

        Map<Decimal, Decimal> markPriCallBack = new Map<Decimal, Decimal>();
        
        if(isConvertion == false){

        //Queries the Marketing Callback limit for the Lead's priority
        List<MarketingCallBackLimit__c> markCallBack = [select Priority__c, CallbackLimit__c from  MarketingCallBackLimit__c  where Priority__c in: leadPriority];
        for(MarketingCallBackLimit__c markCall: markCallBack)
        {
            markPriCallBack.put(markCall.Priority__c, markCall.CallBackLimit__c);
        }
        for(Lead lead:leads)
        {
            if((lead.CallBackLimit__c==null || lead.CallBackLimit__c!= markPriCallBack.get(Integer.valueOf(lead.Priority__c))))
            {
                lead.CallBackLimit__c = markPriCallBack.get(Integer.valueOf(lead.Priority__c));            //Populates the callback limit
            }
        }
       }
        system.debug('****** End of populateCallBack method ******');
    }

    /* This method is called from LeadBeforeUpdate & LeadBeforeInsert Trigger. This method populates the TECH Fields of Leads
     * Opportunity Owner in TECH_OpportunityOwner__c  , Country in TECH_Country__c
     */
    public static void populateTECHfields(set<Lead> leads)
    {
        system.debug('****** Start of populateTECHfields method ******');
        Map<Id,Id> oppOwner = new Map<Id, Id>();
        List<id> oppIds = new List<Id>();
       if(isConvertion == false){
        for(Lead lead: leads)
        {
            oppIds.add(lead.Opportunity__c);
        }
        List<Opportunity> opps = [select OwnerId from Opportunity where Id in:oppIds];
        for(Opportunity opp:opps)
        {
            oppOwner.put(opp.Id, opp.OwnerId);
        }
        for(Lead lead: leads)
        {
            //This population of Opportunity Owner is used in Workflow rule to send email alert to Opportunity Owner, when lead is
            //associated with existing Opportunity.
            if(oppOwner.containskey(lead.Opportunity__c))
                lead.TECH_OpportunityOwner__c = oppOwner.get(lead.Opportunity__c);

            if(String.isNotBlank(lead.Tech_W2LCountryCode__c) && lead.Country__c == null){
                lead.Country__c = lead.Tech_W2LCountryCode__c ; 
                lead.StateProvince__c = String.isNotBlank(lead.Tech_W2LStateProvinceCode__c) ? lead.Tech_W2LStateProvinceCode__c : lead.StateProvince__c;
                system.debug('**** '+lead.Country__c);
            }

            if(lead.Country__c!=null)
                lead.TECH_Country__c = lead.Country__c; //This population is used for mapping Lead Country with Opportunity Country
          }

        }
        system.debug('****** End of populateTECHfields method ******');
    }

    /* This method is called from LeadBeforeUpdate, LeadBeforeInsert triggers. This method checks if the lead has any open activities.
     * If not, displays an error message.
     */
    public static void validateLeadStatus(Map<Id,Lead> leadIds,String triggerAction)
    {
        system.debug('****** Start of validateLeadStatus method ******');
        Map<Id, Id> taskLeadId  = new Map<Id, Id>();
        if(triggerAction == 'Update'&& isConvertion == false)
        {
            //Queries the task where Activity Date > today and lead status not equals to completed.
            List<Task> tasks = [select Id,whoId from Task where  Status !=:Label.CL00327 and whoId in: leadIds.keyset() and ActivityDate>=TODAY];
            for(Task task: tasks)
            {
                taskLeadId.put(task.whoId , task.Id);
            }
        }
        for(Lead lead:leadIds.values())
        {
            if(taskLeadId.size()==0 || taskLeadId.get(lead.Id)==null)
                lead.SubStatus__c.addError(Label.CL00539);
        }
        system.debug('****** End of validateLeadStatus method ******');
    }

    /* This method is called from LeadBeforeUpdate Trigger. This method updates the Opportunity__c, Account__c,
     * Contact__c with the converted field values & TECH_IsConverted__c to TRUE.This method also displays an error
     * message, if the user is trying to convert a Lead which is already associated with an Opportunity.
     */
    public static void updateConvertedFields(set<Lead> leads)
    {
        system.debug('****** Start of updateConvertedFields method ******');
        for(Lead lead: leads)
        {
            //Displays an error message, if the Lead is already associated with an Opportunity and the user is trying to convert
            if(lead.Opportunity__c!=null && lead.TECH_IsConverted__c == false)
                lead.addError(Label.CL00531);
            else
            {
                if(lead.IsConverted==true)
                {
                    lead.Opportunity__c = lead.convertedOpportunityId;
                    lead.TECH_IsConverted__c = true;   //This field is used in Lead mapping
                    lead.Account__c = lead.convertedAccountId;
                    lead.Contact__c = lead.convertedContactId;
                    //lead.LeadContact__c = null; //Srinivas Nallapati
                }
            }
        }
        system.debug('****** End of updateConvertedFields method ******');
    }

    /* This method is called from LeadBeforeUpdate Trigger. This method checks the fields which are required for Opportunity
     * are filled and throws the appropriate error message.
     */
    public static void validateLeadConversionFields(set<Lead> leads)
    {
        system.debug('****** Start of validateLeadConversionFields method ******');
        for(Lead lead: leads)
        {
            //Displays an error message, if any of the following fields is not filled-Opportunity Scope, Classification Level 1, Suspected Value, Leading Business, Zip Code, City, Opportunity Finder.
            if(!lead.OpportunityDoesNotExist__c && (lead.LeadingBusiness__c==null || lead.OpportunityType__c==null || lead.OpportunityFinder__c == null || lead.SuspectedValue__c ==null))
               lead.addError(Label.CL00593);
            else if(Lead.Account__C ==null && (lead.City__c ==null || lead.ClassificationLevel1__c ==null || lead.PreferredLanguage__c ==null ||(lead.Street__c == null &&  lead.POBox__c==null))) 
               lead.addError(Label.CLMAR16MKT005);

            if(lead.OwnedByPRMContact__c && (lead.ProgramLevel__c == null || lead.PartnerProgram__c == null) )
            {
                lead.addError(Label.CLAPR14PRM52);
            }
        }
        system.debug('****** End of validateLeadConversionFields method ******');
    }

    /* This method is called from LeadBeforeUpdate Trigger. This method,when a lead is converted which associated to a Lead Contact,
       Removes the Relation between Lead  and that "Lead Contact",
       if that Lead Contact has any leads associated with it, Moves all those leads to Newly created Contact.
       after this The Lead Contact will have no Associated leads.

    */
    public static void AssociateRelatedLeadsToContact(list<Lead> leads)
    {
        map<id,Lead> mapIdtoLead = new  map<id,Lead>(leads);
        map<id, list<Lead>> mapLeadContactIdToAssociatedLeads = new map<id, list<Lead>>();
        set<id> setLeadContactIds = new set<id>();
        map<id,id> mapLCidtoLeadid = new map<id,id>();


        for(Lead led : leads)
        {
            if(led.isConverted && led.LeadContact__c != null)
            {
                setLeadContactIds.add(led.LeadContact__c);
                mapLCidtoLeadid.put(led.LeadContact__c, led.id);
                led.LeadContact__c = null; // Removing LC from leads
            }
        }

        if(setLeadContactIds.size() > 0)
        {
            List<Lead> lstRelatedLeads = new List<Lead>();
            lstRelatedLeads  = [select id, LeadContact__c, Status, Contact__c from Lead where LeadContact__c in :setLeadContactIds and id NOT in:mapIdtoLead.keyset() ];

            for(Lead ld : lstRelatedLeads){

                //DEF-0654
                ld.Account__c = mapIdtoLead.get(mapLCidtoLeadid.get(ld.LeadContact__c)).Account__c;
                ld.Contact__c = mapIdtoLead.get(mapLCidtoLeadid.get(ld.LeadContact__c)).Contact__c;
                ld.LeadContact__c = null;// removing LC from related leads
            }
            try{
               update lstRelatedLeads;
            }catch(system.exception e){
                for(Lead led : leads)
                {
                    led.AddError('The following error occured while assoiciation the related leads to the new Contact. Please contact your admin \n'+e.getMessage());
                }
            }
        }
    }

    //Srinivas Nallapati    Sep12 Mkt Release   BR-1772
    //Method to update lead currency to the currency of the Country
    public static void updateLeadCurrency(list<Lead> lstLeads)
    {
        set<id> CountryIds = new set<id>();  //Set of country ids
        list<Country__c> lstCountry = new list<Country__c>(); //List of countries
        map<id, Country__c> mapCountry = new map<id, Country__c>();  // map of country id to Country object.
        for(Lead ld : lstLeads)
        {
             if(ld.country__c != null)
                  CountryIds.add(ld.country__c);
        }

        lstCountry = [select CurrencyIsoCode, name from Country__c where id in :CountryIds];

        if(lstCountry.size() > 0)
            mapCountry = new map<id, Country__c>(lstCountry);

        for(Lead ld : lstleads)
        {
            if(mapCountry.containsKey(ld.country__c) && ld.SuspectedValue__c == null)
                ld.CurrencyIsoCode = mapCountry.get(ld.country__c).CurrencyIsoCode;
        }

    }
    //End of Change for BR-1772

    //Srinivas Nallapati    Sep12 Mkt Release   BR-1897
    //BR-1897: If the lead status is changed by a lead qualifier, he/she should automatically become the lead owner.
    public static void changeLeadOwner(map<id,Lead> newMap, map<id,Lead> oldMap)
    {
        Boolean isAdminUser = false;

        String profileName=[select Name from Profile where Id=:UserInfo.getProfileId()].Name;//the profile name of the current User is retreived

        List<String> profilesToSKIP = (System.Label.CLSEP12MKT03).split(',');//the valid profile prefix list is populated
        for(String pName : profilesToSKIP)//The profile name should start with the prefix else the displayerrormessage flag is set to true
        {
            if(pName.trim() == profileName.trim())
            {
                return;
            }
        }
        AP_CustomPermissionsReader cpr = new AP_CustomPermissionsReader();
        Boolean hasPermissionForAllowCloseLeads = cpr.hasPermission('AllowCloseLeads');
        if(hasPermissionForAllowCloseLeads)
        {
        return;
        }
        system.debug(isAdminUser);
        for(Lead ld: newMap.values()) {
            if(ld.IsConverted == false)
            if( (ld.Status != oldMap.get(ld.id).Status) || (ld.SubStatus__c != oldMap.get(ld.id).SubStatus__c) ) //Status is changed
            {
                if( !(oldMap.get(ld.id).Status == System.Label.CL00447 &&  oldMap.get(ld.id).SubStatus__c == 'Qualified') )  //if Old status is not 'Working Qualified'
                    ld.OwnerId = Userinfo.getUserId(); //Change owner to logged in user
            }
        }

    }
    //End of Change for BR-1897

    //Srinivas Nallapati    Sep12 Mkt Release   BR-1774
    //BR-1774: Unlink realted Account and Contact on lead if lead account details are modified
    public static void UnlinkLeadLookups(map<id,Lead> newMap, map<id,Lead> oldMap)
    {

       for(Lead ld : newMap.values())
       {
            //if(ld.Account__c != null)
                if(ld.Company != oldMap.get(ld.id).Company || ld.Street__c != oldMap.get(ld.id).Street__c || ld.City__c != oldMap.get(ld.id).City__c
                      || ld.StateProvince__c != oldMap.get(ld.id).StateProvince__c || ld.Country__c != oldMap.get(ld.id).Country__c || ld.ZipCode__c != oldMap.get(ld.id).ZipCode__c)
                {
                    ld.Account__c = null;
                    ld.Contact__c = null;
                }
       }

    }
    //End of Change for BR-1774
    //Srinivas Nallapati    Dec12 Mkt Release   Uat Jac
    // associate to generaic campaign if non marcom campaign id is invalid
    public static void validateNonMarcomCampaignId(Set<String> NonMarcomCamids, list<lead> lstLeads)
    {
        if(NonMarcomCamids.size() > 0)
        {
            List<Campaign> GerericCampaign = new  List<Campaign>();
            GerericCampaign = [select id from Campaign where name = :System.Label.CLDEC12MKT11];

            boolean foundGenericCampaign = (GerericCampaign.size() > 0) ? true : false;

            List<Campaign> lstNonMarcomCam = [select id from Campaign where id in :NonMarcomCamids];
            Set<string> setValidCampaignIds = new set<String>();
            for(Campaign c: lstNonMarcomCam )
            {
                setValidCampaignIds.add(c.id);
                String strId = c.id;
                setValidCampaignIds.add(strId.substring(0,15));
            }

            for(Lead lead: lstLeads)
            {

                    if(!setValidCampaignIds.contains(lead.Tech_NonMarcomCampaignId__c) ) //if id not found
                    {
                        if(foundGenericCampaign)
                           lead.NonMarcommCampaign__c = GerericCampaign[0].id;
                        else
                           lead.NonMarcommCampaign__c = null;
                    }
                    else
                        lead.NonMarcommCampaign__c = lead.Tech_NonMarcomCampaignId__c;

            }
        }
     }

     /* Marketing - May 2013 Release Start
        Author: Pooja Gupta
        Description: BR-2841 : When there are multiple leads associated to a lead contact and the lead qualifier
                               is able to make a manual match to an existing bFO account or contact the rest of
                               the leads (under the lead contact) should update automatically to that bFO Account
                               or Contact.
     */
    public static void AP19_ReassignLeadsToContact(Map<id,Lead> leadContactToLeadMap)
    {
       if(isConvertion == false)
       {
           List<Lead> tobeUpdatedleads=new List<Lead>();
           Integer limitvalue=Limits.getLimitQueryRows()-Limits.getQueryRows();


           for(Lead loopcontrollead:[Select leadcontact__C,Account__c,Contact__c,IsConverted,id from Lead where IsConverted=false and leadcontact__c in: leadContactToLeadMap.keySet()  and id NOT IN:leadContactToLeadMap.values() limit :limitvalue])
           {
               loopcontrollead.Account__c=leadContactToLeadMap.get(loopcontrollead.leadcontact__C).Account__c;
               loopcontrollead.contact__c=leadContactToLeadMap.get(loopcontrollead.leadcontact__C).contact__c;
               loopcontrollead.leadcontact__c=leadContactToLeadMap.get(loopcontrollead.leadcontact__C).Leadcontact__c;
               tobeUpdatedleads.add(loopcontrollead);
           }

           update tobeUpdatedleads;
        }
    }
     /* Marketing - May 2013 Release End */

    //AP19_Lead - For Updating Contact ID

    public static void UpdateWithContactIdOfConsumerAccount(Map<Id, List<Lead>> leads) {
    List<Account> accounts = [SELECT Id, PersonContactId FROM Account WHERE Id IN :leads.keySet()];
    System.Debug('Consumer Accounts: ' + accounts);
    for (Account acc : accounts) {
        List<Lead> l = leads.get(acc.Id);
        for (Lead l1: l) {
            l1.Contact__c = acc.PersonContactId;
        }
    }
   }

   //AP19_Lead - For updating Account ID

   public static void UpdateWithAccountIdOfConsumerAccount(Map<Id, List<Lead>> leads) {
    List<Contact> Contacts = [SELECT Id, Accountid FROM Contact WHERE Id IN :leads.keySet()];
    System.Debug('Consumer Contacts: ' + contacts);
    for (Contact con : Contacts) {
        List<Lead> l = leads.get(con.Id);
        for (Lead l1 : l) {
            l1.Account__c = con.Accountid;
        }
    }
   }

   /*
   /// Oct13 Mkt Srinivas Nallapti - For RM Cordis Integarion
   @future
   public static void updateWithAssignmentRules(set<id> setleadIds)
   {
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= true;

        List<Lead> lstleadstoUpdate = new List<Lead>();
        for(Id leadid : setleadIds)
        {
            Lead ld = new Lead(id=leadid, Tech_AssignmentRuleHasRun__c = true);
            //ld.setOptions(dmo);
            lstleadstoUpdate.add(ld);
        }

        Database.update(lstleadstoUpdate, dmo);
   }*/

   //Apr14 Prm  Passed to partner leads
   public static void processPassedtoPartnerLeads(List<Lead> leads) {

        set<id> leadOwnerIds = new set<id>();
        for(Lead ld : leads)
            leadOwnerIds.add(ld.ownerId);
        List<User> lstUser = [SELECT ContactId, Contact.PRMContact__c, Contact.AccountId FROM User WHERE ID IN: leadOwnerIds AND ContactId != null ];
        Map<id,User> mpUser = new Map<id,User>(lstUser);

        List<PartnerOpportunityStatusHistory__c> lstNewStatusRec= new list<PartnerOpportunityStatusHistory__c>();
        List<Task> lstPartnerTasks = new List<Task>();
        list<Messaging.SingleEmailMessage> lstMail = new list<Messaging.SingleEmailMessage>();
        String templateID= System.Label.CLAPR14PRM29;

        set<id> ledIdsToUpdate = new set<id>();

        for(Lead ld : leads) {
            if(mpUser.containsKey(ld.OwnerId) && mpUser.get(ld.OwnerId).Contact.PRMContact__c == true) {
                PartnerOpportunityStatusHistory__c leadHistory = new PartnerOpportunityStatusHistory__c();
                leadHistory.Lead__C = ld.id;
                leadHistory.Status__c = 'Assigned';
                leadHistory.User__c = ld.OwnerId;

                lstNewStatusRec.add(leadHistory);

                System.debug('*** Lead Task: ' + ld);

                Task newTask = new Task(
                    OwnerId = ld.OwnerId,
                    Subject = 'Lead ' + ld.FirstName + ' ' + ld.LastName + ' ' + System.Label.CLOCT13PRM08,
                    Description = System.Label.CLOCT13PRM09,
                    RecordTypeId = System.Label.CLAPR14PRM21);

                newTask.Tech_LeadId__c = ld.Id;
                lstPartnerTasks.add(newTask);

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.settargetObjectId(ld.OwnerId);

                mail.setorgWideEmailAddressId(Label.CLJUN13PRM04);
                mail.settemplateId(templateID);
                mail.setwhatId(ld.Id);
                mail.setSaveAsActivity(false);
                lstMail.add(mail);

                //
                ledIdsToUpdate.add(ld.id);
            }
        }

        List<PartnerOpportunityStatusHistory__c> lsOldPosh = [Select Status__c, Lead__C FROM PartnerOpportunityStatusHistory__c WHERE Status__c='Assigned' AND Lead__C IN:ledIdsToUpdate];
        for(PartnerOpportunityStatusHistory__c pso : lsOldPosh)
        {
            pso.Status__c = 'Re-Assigned';
        }
        update lsOldPosh;

        Insert lstNewStatusRec;

        Insert lstPartnerTasks;

        if(!lstMail.isEmpty())
            Messaging.sendEmail(lstMail);
   }
   
   
   // June 2016 Release - Recycle Contacts when the associated Transacational Lead status is set to "3-Close Others"
   
   public static void RecycleContactonTransactionLeadClosure(Map<Id,Lead> leadIds,String triggerAction)
    {
    system.debug('****** Start of RecycleContactonTransactionLeadClosure method ******');
     Map<id,String> MapRsn = new Map<id,String>();
     List<Contact> conList = new List<Contact>();
     if(triggerAction == 'Update')
        {
        
        // Get List of Contacts associated to the Closed bMA Transactional Lead
           for(Id lid:leadIds.keyset())
            {
             MapRsn.put(lid,leadIds.get(lid).SubStatus__c);
             }
             system.debug('******* Contact IDs to be updated ' + MapRsn);
             Map<string,CS_bMATransactionalContactRecycle__c> CSr = CS_bMATransactionalContactRecycle__c.getall();
             
            List<Contact> ConRecycle = [Select id from Contact where id in :leadIds.keyset() and LeadStatus__c = :Label.CL00555];
            
            for(Contact c:ConRecycle){
            Contact con = (Contact)(c);
            If(CSr.ContainsKey(MapRsn.get(con.id))){
            con.LeadStatus__c = Label.CLJUN16MKT001;
            con.RecycleReason__c = CSr.get(MapRsn.get(con.id)).ContactRecycleReason__c;           
            conList.add(con);
            }
            
            update conList;
         }
       }
   }
}