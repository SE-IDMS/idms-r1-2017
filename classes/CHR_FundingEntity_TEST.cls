/*
    Author          : Priyanka Shetty (Schneider Electric)
    Date Created    : 02/05/2013
    Description     : Test class for the Change Request Funding Entity
*/

@isTest
private class CHR_FundingEntity_TEST{
    static testMethod void CHRFundingEntityTest(){

        Change_Request__c chr = new Change_Request__c(name='testCR',BusinessDomain__c='ERP Rationalization and Migration',Platform__c='Bridge');
        insert chr;
        chr.NextStep__c='Cancelled';
       update chr;
        Change_Request__c chr1 = new Change_Request__c(name='testCR1',BusinessDomain__c='ERP Rationalization and Migration',Platform__c='Bridge');
        insert chr1;
       chr1.NextStep__c='Cancelled';
       update chr1;
 //Change Request Funding entity 
      
           DMTFundingEntityCostItemsFields__c crfe = new DMTFundingEntityCostItemsFields__c();
           crfe.Name ='Testfe 1';
           crfe.CostItemFieldAPIName__c = 'CAPEXY0__c';
           crfe.Type__c = 'IT FUNDING CAPEX';
           crfe.TypeOrder__c = 'Type 1';
           insert crfe;     
           DMTCRCFTOCRFE__c crfe1 = new DMTCRCFTOCRFE__c();
           crfe1.Name ='Testcr2';
           crfe1.CostForecastField__c = 'NewFY0CashoutCAPEX__c';
           crfe1.FundingEntityField__c = 'CAPEXY0__c';
           insert crfe1;        
           
        ChangeRequestCostsForecast__c oBudget = new ChangeRequestCostsForecast__c();
        oBudget.CurrencyIsoCode = 'EUR';
        oBudget.ChangeRequest__c = chr.id;
        oBudget.TECHTotalPlannedITCosts__c = 10;
        //oBudget.TotalPlannedCost__c = 100;
        oBudget.Active__c = true;
        insert oBudget;
        Integer count=[select count() from ChangeRequestFundingEntity__c where ChangeRequest__c=:chr.id];
        if(count>0){
             
            list<ChangeRequestFundingEntity__c> fund=[select id from ChangeRequestFundingEntity__c where ChangeRequest__c=:chr.id ];
             delete fund;
         } 

        ChangeRequestFundingEntity__c ferec = new ChangeRequestFundingEntity__c();
        ferec.ParentFamily__c = 'Business';
        ferec.Parent__c = 'Buildings';
        ferec.GeographicZoneSE__c = 'EMEAS';
        ferec.GeographicalSEOrganization__c = 'All Europe';
        ferec.CurrencyISOCode = 'EUR';
        ferec.ChangeRequest__c = chr.id;
        ferec.ActiveForecast__c =true;
        ferec.SelectedinEnvelop__c = True;
        ferec.BudgetYear__c = '2013';
        ferec.ForecastQuarter__c = 'Q2';
        ferec.ForecastYear__c = '2013';
        ferec.CAPEXY1__c=100;
        insert ferec;
         

        ChangeRequestFundingEntity__c ferec1 = new ChangeRequestFundingEntity__c();
        ferec1.ParentFamily__c = 'Business';
        ferec1.Parent__c = 'Buildings';
        ferec1.GeographicZoneSE__c = 'EMEAS';
        ferec1.GeographicalSEOrganization__c = 'All Europe';
        ferec1.CurrencyISOCode = 'EUR';
        ferec1.ChangeRequest__c = chr1.id;
        ferec1.ActiveForecast__c =false;
        ferec1.BudgetYear__c = '2012';
        ferec1.ForecastQuarter__c = 'Q2';
        ferec1.ForecastYear__c = '2012';
        insert ferec1;
        test.startTest();
        ferec1.CAPEXY1__c=100;
        ferec1.SelectedinEnvelop__c = True;
        update ferec1;


        ChangeRequestFundingEntity__c ferec2 = new ChangeRequestFundingEntity__c();
        ferec2.ParentFamily__c = 'Business';
        ferec2.Parent__c = 'Buildings';
        ferec2.GeographicZoneSE__c = 'APAC';
        ferec2.GeographicalSEOrganization__c = 'All Europe';
        ferec2.CurrencyISOCode = 'EUR';
        ferec2.ChangeRequest__c = chr.id;
        ferec2.ActiveForecast__c = false;
        ferec2.BudgetYear__c = '2012';
        ferec2.ForecastQuarter__c = 'Q1';
        ferec2.ForecastYear__c = '2012';
        insert ferec2;
        ferec2.ActiveForecast__c = true;
        update ferec2;
        delete ferec2;
        
        
        CHR_ManageFundingEntity pageFE2 = new CHR_ManageFundingEntity(new ApexPages.StandardController(ferec));
        
            pageFE2.updateFundingEntity();
            pageFE2.backToFundingEntity();
            ferec1.SelectedinEnvelop__c = true;
            CHR_UpdateSelectionInBudgetEnvelop vfCtr = new  CHR_UpdateSelectionInBudgetEnvelop(new ApexPages.StandardController(ferec));
            vfCtr.updateFundingEntity();
            vfCtr.backToFundingEntity();
            User runAsUser = Utils_TestMethods.createStandardDMTUser('testch');
            insert runAsUser;   
            System.runAs(runAsUser){
            DMTAuthorizationMasterData__c testDMTA = new DMTAuthorizationMasterData__c(AuthorizedUser1__c = UserInfo.getUserId(),NextStep__c = 'Created',Platform__c = 'Bridge');
            insert testDMTA;
            ferec1.SelectedinEnvelop__c = true;
            CHR_UpdateSelectionInBudgetEnvelop vfCtr2 = new  CHR_UpdateSelectionInBudgetEnvelop(new ApexPages.StandardController(ferec));
            vfCtr2.updateFundingEntity();
            vfCtr2.backToFundingEntity();           
            Delete testDMTA;
            vfCtr2.updateFundingEntity();
            vfCtr2.backToFundingEntity();

        }  
         test.stopTest();             
    }
}