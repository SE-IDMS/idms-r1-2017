public with sharing class VFC_CommunityCaseSearch{

   public string searchStr;    
    public String ErrorPage;
    Public List<Case> CseList {get;set;}
    public PageReference returnPage;
    Public boolean showerror {get;set;}
    
    public VFC_CommunityCaseSearch() {
        searchStr = ApexPages.currentPage().getParameters().get('searchString');
        ErrorPage= ApexPages.currentPage().getParameters().get('errorURL');
        showerror=false;
    }
    public PageReference returnPage() {
        if(ErrorPage !=null && ErrorPage !='')
            returnPage= new PageReference(ErrorPage);
        else
            returnPage= new PageReference('/home/home.jsp'); 
        return returnPage;
    }
    public PageReference searchCaseNumber() {
        if(searchStr!=null || searchStr !=''){
            String soql = 'select id,CaseNumber,subject,contact.name,accountId from Case where';
            String whereCondition = '';
            if (searchStr != null && searchStr != '') {
                searchStr = searchStr.replaceAll('\\*','%');
                whereCondition = whereCondition +' CaseNumber LIKE \'' + '%'+searchStr+'%\' OR Subject LIKE \'' + '%'+searchStr+'%\' OR contact.name LIKE \'' + '%'+searchStr+'%\' ';
            }
            soql = soql + whereCondition +' limit 20';
            System.debug('soql'+soql);
            try{
                cseList = database.query(soql);
              if(cseList.size()>0){
                 showerror=true;
               }
               else{
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.CLMAY15I2P03));
               }
                System.debug('cseList'+cseList);    
            }
            catch(Exception e){
                System.debug('Exception'+e.getMessage());
            }
        }
        return null;

    }
}