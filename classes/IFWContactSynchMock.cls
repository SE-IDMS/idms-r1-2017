@isTest
global class IFWContactSynchMock implements HttpCalloutMock{
    global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('fail');
        res.setStatusCode(500);
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{\"fault\":{\"code\":\"101504\",\"type\":\"Status report\",\"message\":\"Runtime Error\",\"description\":\"Send timeout\"}}');
        return res;
    }
}