public with sharing class VFC72_ProdLineCloning 
{

    //Variable Declaration
    public String pgeMsg{get;set;}
    public boolean rndrCnclButtn{get;set;}
    public boolean rndrCnfmButtn{get;set;}    
    public String cnclButtnLbl{get;set;}
    public String sevrity{get;set;}
    public OPP_ProductLine__c prodline = new OPP_ProductLine__c();
    public OPP_ProductLine__c clonedProdLine = new OPP_ProductLine__c();                     
    private String query;
    private String query1;           
    public PageReference pg = NULL;

    //Constructor
    public VFC72_ProdLineCloning(ApexPages.StandardController controller) 
    {
        prodline = (OPP_ProductLine__c)controller.getRecord();
    }
    
    /* This method is called on the load of VFP72_ProdLineCloning page. Clones the ProductLine record and if user has Read/Write permission
    ** on the parent Opportunity, then save the cloned ProductLine record and Redirect user to the detail page of cloned ProductLine.       
    */
    public pagereference cloneProdLine()
    {
        pgeMsg = Label.CL00703;
        cnclButtnLbl =  Label.CL00005.toUpperCase();
        rndrCnfmButtn = true;
        rndrCnclButtn = true;
        sevrity = 'confirm';
        return pg;
    }
    
    public pagereference continueClone()
    {
        if(prodline.Id != NULL)
        {        
            query = Utils_Methods.generateSelectAllQuery(prodline);
            query1 = ' from OPP_ProductLine__c';
            query = query.replace(query1, ',Opportunity__r.CurrencyISOCode'+query1);
            
            query = query +' where Id=\''+prodline.Id+'\'';            
            prodline = Database.Query(query);  
        
            Savepoint sp = Database.setSavepoint();                 //Create a savepoint before cloning the Product Line            
            clonedProdLine = prodline.clone();                     //Clones the Product line Record              
            
            IF(clonedProdLine.CurrencyISOCode != prodline.Opportunity__r.CurrencyISOCode) // Defaulting to Opportunity Currency if it's not matching
                clonedProdLine.CurrencyISOCode = prodline.Opportunity__r.CurrencyISOCode;
                  
            Database.saveResult recIns= Database.Insert(clonedProdLine, false);
            if(!recIns.isSuccess())
            {
                //Displays an error message, if the logged-in user doesn't have appropriate permissions to clone Quote Link.
                pgeMsg = recIns.getErrors()[0].getMessage()+'. '+'\n\n'+Label.CL00414;
                //pgeMsg = Label.CL00414;
                rndrCnclButtn = true;
                cnclButtnLbl = Label.CL00010.toUpperCase();
                sevrity = 'error';
                rndrCnfmButtn = false;
            }
            else
                pg = New PageReference('/'+recIns.getId());
          }
          return pg;
    }

}