global with sharing class VFC_AddProductForLiteratureReq{

    // Map of existing master product on the case and new selected products from the search results.
    // Key :    field <TECH_UniqueId__c>
    public Map<String, LiteratureRequestLineItem__c> map_ShoppingCartProducts {get;set;}
    
    // Return the values of the map map_ShoppingCartProducts, to be used in the pageDataBlock
    public List<LiteratureRequestLineItem__c> getShoppingCartProducts() {
        List<LiteratureRequestLineItem__c> lst_Products = map_ShoppingCartProducts.values();
        System.debug('VFC_AddProductForLiteratureReq.map_ShoppingCartProducts --- ' + lst_Products);
        lst_Products.sort();
        return lst_Products;
    } 

    // Existing related products on Literature Request, to be used as referential.
    public Map<String,Id> map_currCaseExistingProductsTechId2Id {get;set;}
    
    // Removed related products, to be deleted at the end of process.
    public Set<Id> set_currCaseListRemovedProducts {get;set;}

    // Master from existing set of related products on Literature Request, to be used as referential.
    public String oldMasterProduct {get;set;}
    
    // Field <TECH_UniqueId__c> of selected product  when click on "Remove" in a data row.
    public String selectedProductRemoved {get;set;}

    // Field <TECH_UniqueId__c> of selected product when select a new master in a data row.
    public String selectedMasterProduct = '';    
    public String getSelectedMasterProduct(){ return selectedMasterProduct; }
    public void setSelectedMasterProduct (String masterCr){
        if(masterCr != null && masterCr != '')  
            selectedMasterProduct  = masterCr;
    }

    // Field <TECH_UniqueId__c> of previous master product. Used to change the value of MasterProduct__c of previous master when select a new master.
    public String previousSelectedMasterProduct {get;set;}
    public Boolean errorOccurred {get;set;}
    
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public Case caseRecord{get;set;}
    public LiteratureRequest__c litReqRecord{get;set;}
    public static String strGMRCode = '';

    //When 'Change Product Link' is clicked from LiteratureRequest record, its recordID is stored in LiteratureID
    public String LiteratureID = null;
    public String pageSaveURL {get;set;}
    //public String getpageSaveURL(){ return pageSaveURL; }
    Decimal initialQuantity;    //store the initial Quantity of the Literature request
    /************************
     *      CONTROLLER      *
     * **********************/
    public VFC_AddProductForLiteratureReq(ApexPages.StandardController controller){

        map_currCaseExistingProductsTechId2Id = new Map<String,Id>();
        set_currCaseListRemovedProducts = new Set<Id>();
        pageSaveURL = 'Initialize';
        //Store value of existing literature request, when Change Product link is clicked
        LiteratureID = system.currentpagereference().getparameters().get('id');
        
        map_ShoppingCartProducts = new Map<String, LiteratureRequestLineItem__c>();
        if(!Test.isRunningTest())
            controller.addFields(new List<String>{'Case__c','CommercialReference__c','Quantity__c'});
        litReqRecord=(LiteratureRequest__c)controller.getRecord();
        System.debug('VFC_AddProductForLiteratureReq.Existing Literature request --- ' + litReqRecord);

        if(LiteratureID!=null)
        {
            System.debug('VFC_AddProductForLiteratureReq.LiteratureID --- ' + LiteratureID);
            LiteratureRequestLineItem__c existingMasterOnLitReq = new LiteratureRequestLineItem__c();
            List<LiteratureRequestLineItem__c> existingLitItems = [SELECT Id,CommercialReference_lk__c,TECH_GMRCode__c,TECH_UniqueId__c,Name, ProductDescription__c, ProductLine__c, BusinessUnit__c, MasterProduct__c, CommercialReference__c, ProductFamily__c, Family__c,Quantity__c,LiteratureRequest__c FROM LiteratureRequestLineItem__c  WHERE LiteratureRequest__c = :LiteratureID];        // get only the master product of the Literature Request
            
            if(existingLitItems.size() > 0)
            {

                for(LiteratureRequestLineItem__c relProduct : existingLitItems )
                {
                    // Display existing Master literature Item in the shopping cart
                    map_ShoppingCartProducts.put(relProduct.TECH_UniqueId__c, relProduct);
                    
                    map_currCaseExistingProductsTechId2Id.put(relProduct.TECH_UniqueId__c,relProduct.Id);

                    if(relProduct.MasterProduct__c){
                        selectedMasterProduct = relProduct.TECH_UniqueId__c;
                        oldMasterProduct = relProduct.TECH_UniqueId__c;
                        existingMasterOnLitReq = relProduct;
                        initialQuantity = relProduct.Quantity__c;
                        System.debug('VFC_AddProductForLiteratureReq.existingMasterOnLitReq - with LitReqID --- '+existingMasterOnLitReq);
                    }
                }
                System.debug('VFC_AddProductForLiteratureReq.selectedMasterProduct - with LitReqID --- '+selectedMasterProduct);
                System.debug('VFC_AddProductForLiteratureReq.map_currCaseExistingProductsTechId2Id - with LitReqID --- '+map_currCaseExistingProductsTechId2Id);
	            
                if(selectedMasterProduct!=null)
                	previousSelectedMasterProduct = selectedMasterProduct;
                else
                	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Master product of Literature Request is not entered in Literature Item.'));
                
                System.debug('VFC_AddProductForLiteratureReq.previousSelectedMasterProduct - with LitReqID --- '+previousSelectedMasterProduct);

                // initialize values for prepopulation in Breadcrum
                if(existingMasterOnLitReq.BusinessUnit__c!=null)
                    pageParameters.put('businessLine',existingMasterOnLitReq.BusinessUnit__c);            
                if(existingMasterOnLitReq.ProductLine__c!=null)
                    pageParameters.put('productLine',existingMasterOnLitReq.ProductLine__c);            
                if(existingMasterOnLitReq.ProductFamily__c!=null)
                    pageParameters.put('strategicProductFamily',existingMasterOnLitReq.ProductFamily__c);            
                if(existingMasterOnLitReq.Family__c!=null)
                    pageParameters.put('family',existingMasterOnLitReq.Family__c);            
                if(existingMasterOnLitReq.CommercialReference__c!=null)
                    pageParameters.put('commercialReference',existingMasterOnLitReq.CommercialReference__c);            
                if(existingMasterOnLitReq.TECH_GMRCode__c!=null && existingMasterOnLitReq.TECH_GMRCode__c.length()>0)
                {
                    pageParameters.put('masterCR','true');
                    strGMRCode = existingMasterOnLitReq.TECH_GMRCode__c;
                }
                else
                    pageParameters.put('masterCR','false');

            }            
        }
        else if(LiteratureID==null && litReqRecord.Case__c!=null)
        {
            System.debug('VFC_AddProductForLiteratureReq.litReqRecord.Case__c --- ' + litReqRecord.Case__c );
            caseRecord=[select Status, SupportCategory__c,ProductBU__c, ProductLine__c, ProductFamily__c, Family__c, TECH_LaunchGMR__c, TECH_GMRCode__c, CommercialReference__c, ACC_ClassLevel1__c, AccountID, ContactID, CaseNumber from Case where id=:litReqRecord.Case__c];

            // initialize values for prepopulation of 4 levels in Breadcrum

            if(caseRecord.ProductBU__c!=null)
                pageParameters.put('businessLine',caseRecord.ProductBU__c);            
            if(caseRecord.ProductLine__c!=null)
                pageParameters.put('productLine',caseRecord.ProductLine__c);            
            if(caseRecord.ProductFamily__c!=null)
                pageParameters.put('strategicProductFamily',caseRecord.ProductFamily__c);            
            if(caseRecord.Family__c!=null)
                pageParameters.put('family',caseRecord.Family__c);            
            if(caseRecord.CommercialReference__c!=null)
                pageParameters.put('commercialReference',caseRecord.CommercialReference__c);
            if(caseRecord.TECH_GMRCode__c!=null && caseRecord.TECH_GMRCode__c.length()>0)
            {
                pageParameters.put('masterCR','true');
                strGMRCode = caseRecord.TECH_GMRCode__c;
            }
            else
                pageParameters.put('masterCR','false');

            // initialize values for the insert edit page of literature request

            litReqRecord.Classificationlevel1__c = caseRecord.ACC_ClassLevel1__c;

            List<Contact> ContactList = [SELECT ID, Name, Street__c, ZipCode__c, POBox__c,County__c,Country__c,Country__r.Name, City__c, StateProv__c,StateProv__r.Name, POBoxZip__c FROM Contact WHERE ID=:caseRecord.ContactID];
            
            if(ContactList.size() == 1)
            {
                litReqRecord.Contact__c = ContactList[0].ID;             
                litReqRecord.ShippingStreet__c = ContactList[0].Street__c; 
                litReqRecord.ShippingCity__c = ContactList[0].City__c;
                litReqRecord.ShippingPOBoxZip__c = ContactList[0].POBoxZip__c; 
                litReqRecord.ShippingZipcode__c = ContactList[0].ZipCode__c;
                litReqRecord.ShippingPOBox__c = ContactList[0].POBox__c; 
                litReqRecord.ShippingCounty__c = ContactList[0].County__c; 
                litReqRecord.ShippingStateProvince__c = ContactList[0].StateProv__r.Name; 
                litReqRecord.ShippingCountry__c = ContactList[0].Country__r.Name;
                //relatedContact = ContactList[0];
            }
        
            List<Account> accountList = [SELECT ID, Name, Street__c, City__c, POBox__c, ZipCode__c, StateProvince__c, 
                                                    County__c, Country__c,  POBoxZip__c  
                                                    FROM Account WHERE ID=:caseRecord.AccountID];

            System.debug('VFC_AddProductForLiteratureReq.AccountList --- ' + accountList );

            if(accountList.size()==1)
            {
                System.debug('VFC_AddProductForLiteratureReq.AccountList Size --- ' + accountList.size());
                System.debug('VFC_AddProductForLiteratureReq.AccountList[0] --- ' + AccountList[0]);
                
                if(AccountList[0].Name != null)
                {
                    litReqRecord.Account__c = AccountList[0].ID;
                    //relatedAccount = AccountList[0];
                }
                             
               // litReqRecord.AccountAddress__c = '';             
                litReqRecord.AccountStreet__c = accountList[0].Street__c;
                
                litReqRecord.AccountCity__c = accountList[0].City__c;
                litReqRecord.AccountZipCode__c = AccountList[0].ZipCode__c;
                
              // System.debug('#### litReqRecord.AccountAddress__c: ' + litReqRecord.AccountAddress__c);
                
                if(accountList[0].StateProvince__c != null)
                {
                    List<StateProvince__c> stateProvinceList = [SELECT Id, Name FROM StateProvince__c WHERE ID=:accountList[0].StateProvince__c];
                    if(stateProvinceList.size() == 1)
                    {
                        litReqRecord.AccountStateProvince__c = stateProvinceList[0].Name;
                    }
                }
                
                if(accountList[0].County__c != null)
                {
                    litReqRecord.AccountCounty__c = '\n' + accountList[0].County__c;
                }

                if(accountList[0].Country__c != null)
                {                
                    List<Country__c> countryList = [SELECT Id, Name FROM Country__c WHERE ID=:AccountList[0].Country__c];
                    if(countryList.size() == 1)
                    {
                        litReqRecord.AccountCountry__c = countryList[0].Name;       
                    }
                }
                
                litReqRecord.AccountPOBox__c = accountList[0].POBox__c;
                litReqRecord.AccountPOBoxZip__c = accountList[0].POBoxZip__c;                     
            }

            List<CSE_RelatedProduct__c> existingProductOnCase = [SELECT Id,ProductSuccession__c,ProductGroup__c,CommercialReference_lk__c,TECH_GMRCode__c,SubFamily__c,TECH_UniqueId__c,Family_lk__c,Name, ProductDescription__c, ProductLine__c, BusinessUnit__c, MasterProduct__c, CommercialReference__c, ProductFamily__c, Family__c FROM CSE_RelatedProduct__c WHERE Case__c = :litReqRecord.Case__c and MasterProduct__c=true];        // get only the master product of the case
            
            for(CSE_RelatedProduct__c relProduct : existingProductOnCase ){
                // Create a literature Item only if commerical reference field is not blank, to display existing case product in the shopping cart
                if(String.isNotBlank(relProduct.CommercialReference__c))
                {
                    LiteratureRequestLineItem__c litItem = new LiteratureRequestLineItem__c();
                    litItem.MasterProduct__c = relProduct.MasterProduct__c;
                    litItem.TECH_GMRCode__c = relProduct.TECH_GMRCode__c;
                    litItem.TECH_UniqueId__c = relProduct.TECH_UniqueId__c;
                    litItem.BusinessUnit__c = relProduct.BusinessUnit__c;
                    litItem.ProductLine__c = relProduct.ProductLine__c;
                    litItem.ProductFamily__c = relProduct.ProductFamily__c;
                    litItem.Family__c = relProduct.Family__c;
                    litItem.ProductDescription__c = relProduct.ProductDescription__c;
                    litItem.CommercialReference__c = relProduct.CommercialReference__c;
                    litItem.CommercialReference_lk__c = relProduct.CommercialReference_lk__c;
                    litItem.Quantity__c = 1;
                    map_ShoppingCartProducts.put(litItem.TECH_UniqueId__c, litItem);
                    
                    map_currCaseExistingProductsTechId2Id.put(relProduct.TECH_UniqueId__c,relProduct.Id);

                    if(relProduct.MasterProduct__c)
                    {
                        selectedMasterProduct = relProduct.TECH_UniqueId__c;
                        oldMasterProduct = relProduct.TECH_UniqueId__c;
                    }
                }
            }
            System.debug('VFC_AddProductForLiteratureReq.selectedMasterProduct - withOUT LitReqID --- '+selectedMasterProduct);
            System.debug('VFC_AddProductForLiteratureReq.map_currCaseExistingProductsTechId2Id - withOUT LitReqID --- '+map_currCaseExistingProductsTechId2Id);

            previousSelectedMasterProduct = selectedMasterProduct;
            System.debug('VFC_AddProductForLiteratureReq.previousSelectedMasterProduct - withOUT LitReqID --- '+previousSelectedMasterProduct);
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'A Case is not associated to the Literature Request.Please create the Literature Request from a Case.'));
        }
    }
    public String scriteria{get;set;}
    {    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
    }
    
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
    


        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
            }
        }
        if(!Test.isRunningTest() || System.Label.GMRIntegrationType != 'BEM') {
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            System.debug('VFC_AddProductForLiteratureReq.remoteSearch.gmrFilteredDataBeanList --- '+results.gmrFilteredDataBeanList);
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
    
    @RemoteAction
    global static String getSearchText() {
        String scriteria='';
        
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
        
        return scriteria; 
    }
    
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        return Database.query(query);       
    }
    
    public PageReference pageCancelFunction(){
        PageReference pageResult;
        if(LiteratureID!=null)
        {
            pageResult = new ApexPages.StandardController(litReqRecord).view();
        }
        else if(caseRecord!=null)
        {
            pageResult = new ApexPages.StandardController(caseRecord).view();
        }
        else{
           return new PageReference('/home.jsp');
        }
        pageResult.setRedirect(true);
        return pageResult;
    }
        
    /*
     *  Checkbox : Master   
     *  Check the field MasterProduct__c of the new master product and uncheck the old one.
     *****/
    public PageReference selectMasterProduct() {
        selectedMasterProduct = Apexpages.currentPage().getParameters().get('selectedMasterProduct');
        
        System.debug('VFC_AddProductForLiteratureReq.selectMasterProduct().selectedMasterProduct --- '+ selectedMasterProduct);
        System.debug('VFC_AddProductForLiteratureReq.selectMasterProduct().previousSelectedMasterProduct --- '+ selectedMasterProduct);
        
        if(map_ShoppingCartProducts.containskey(previousSelectedMasterProduct))
            map_ShoppingCartProducts.get(previousSelectedMasterProduct).MasterProduct__c = false;
        if(map_ShoppingCartProducts.containskey(selectedMasterProduct))
            map_ShoppingCartProducts.get(selectedMasterProduct).MasterProduct__c = true;
        
        previousSelectedMasterProduct = selectedMasterProduct;
        return null;
    }
        
    /*
     *  Link : remove   
     *  Remove the selected line from the shopping cart and add the related product to a list to be deleted at the end of process.
     *****/
    public PageReference removeLineShoppingCart() {
        // Remove the selected product from the shopping cart
        selectedProductRemoved = Apexpages.currentPage().getParameters().get('selectedProductRemoved');
        map_ShoppingCartProducts.remove(selectedProductRemoved);
        
        // If the selected product was part of the initial set of existing products on the case, 
        // add it to this list to be removed at the end of process.
        if(map_currCaseExistingProductsTechId2Id.containskey(selectedProductRemoved) )
            set_currCaseListRemovedProducts.add(map_currCaseExistingProductsTechId2Id.get(selectedProductRemoved));

        System.debug('VFC_AddProductForLiteratureReq.removeLineShoppingCart().set_currCaseListRemovedProducts  --- '+set_currCaseListRemovedProducts);
        return null;
    }
        
    /*
     *  Button : Save   
     *  Add all products in the shopping cart to the case and delete removed products.
     *****/
    public PageReference saveShoppingCart() 
    {
        PageReference pageSaveResult;
        errorOccurred = false;
        
        if(map_ShoppingCartProducts.values().size() != 0)
        {
            // If a Master product is selected, copy its fields on the Literature Request.
            // If not, display an error to the user
            if(map_ShoppingCartProducts.containsKey(selectedMasterProduct))
            {
                LiteratureRequestLineItem__c masterProductOrFamily = map_ShoppingCartProducts.get(selectedMasterProduct);
                litReqRecord.CommercialReference__c    = masterProductOrFamily.CommercialReference__c;
                litReqRecord.Quantity__c = masterProductOrFamily.Quantity__c;
            } 
            else 
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLDEC14CCC04));
                errorOccurred = true;
                //return null;
            }
            
            try
            {
                // Delete the Related Products removed from the shopping cart 
                System.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().set_currCaseListRemovedProducts [Existing Products to be deleted] --- '+set_currCaseListRemovedProducts);
                if(set_currCaseListRemovedProducts != null && set_currCaseListRemovedProducts.size() != 0){
                    List<Id> lst_currCaseListRemovedProducts = new List<Id>(set_currCaseListRemovedProducts);
                    Database.delete(lst_currCaseListRemovedProducts);
                }

                Database.Saveresult srLitReq;

                if(LiteratureID!=null)
                {
                    // Update existing LiteratureReq only if the Master Product has changed OR if the quantity of master product changed
                    if(oldMasterProduct != selectedMasterProduct || initialQuantity != litReqRecord.Quantity__c ){
                        System.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().LiteratureRequest Product being udpated  --- '+litReqRecord);
                        Database.update(litReqRecord);
                    }
                    
                    
                    System.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().map_ShoppingCartProducts Values being upserted --- '+map_ShoppingCartProducts.values());
                    
                    //Insert the new master product to Literature Item 
                    for(LiteratureRequestLineItem__c relProduct : map_ShoppingCartProducts.values() ){
                        System.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().relProduct.LiteratureRequest__c --- '+ relProduct.LiteratureRequest__c);
                        System.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().selectedMasterProduct --- '+ selectedMasterProduct);
                        System.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().previousSelectedMasterProduct --- '+ previousSelectedMasterProduct);
                        if(selectedMasterProduct != null && selectedMasterProduct != '' && previousSelectedMasterProduct != null && map_ShoppingCartProducts.containsKey(selectedMasterProduct))
                        {
                            System.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().selectedMasterProduct --- '+ map_ShoppingCartProducts.get(selectedMasterProduct));
                            System.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().previousSelectedMasterProduct --- '+ map_ShoppingCartProducts.get(previousSelectedMasterProduct));
                            if(map_ShoppingCartProducts.containsKey(previousSelectedMasterProduct))                            
                                map_ShoppingCartProducts.get(previousSelectedMasterProduct).MasterProduct__c = false;
                            
                            map_ShoppingCartProducts.get(selectedMasterProduct).MasterProduct__c = true;

                        }

                        if(relProduct.LiteratureRequest__c!=litReqRecord.id)
                            relProduct.LiteratureRequest__c=litReqRecord.id;    // get 18 digit ID of literature request record by using expression => (new LiteratureRequest__c(ID=LiteratureID)).Id
                        relProduct.TECH_UniqueId__c= litReqRecord.id + '-' + relProduct.CommercialReference_lk__c;
                    }
                    Database.upsert(map_ShoppingCartProducts.values(),Schema.LiteratureRequestLineItem__c.Id);

                    // Redirect to Literature Request Edit page 
                    pageSaveResult = new ApexPages.StandardController(litReqRecord).view();

                }
                // From Case Page ( New Literature Request creation)
                else
                {
                    if(selectedMasterProduct!=null)
                        srLitReq = Database.insert(litReqRecord);

                    // Upsert the Literature Items added to the shopping cart
                    for(LiteratureRequestLineItem__c relProduct : map_ShoppingCartProducts.values() ){
                        relProduct.LiteratureRequest__c=srLitReq.getId();
                        relProduct.TECH_UniqueId__c= srLitReq.getId() + '-' + relProduct.CommercialReference_lk__c;
                    }
                    system.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().Literature Items to be inserted --- '+ map_ShoppingCartProducts.values());
                    Database.upsert(map_ShoppingCartProducts.values(),Schema.LiteratureRequestLineItem__c.TECH_UniqueId__c); 
                    
                    // Redirect to Literature Request Edit page 
                    pageSaveResult = new ApexPages.StandardController(new LiteratureRequest__c(ID=srLitReq.getId())).edit(); //new PageReference('/'+srLitReq.getId());
                    pageSaveResult.getParameters().put(Label.CL00330, srLitReq.getId());
                    
                }

            }
            catch(Exception ex)
            {
                system.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().Exception --- '+ex);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+ex.getMessage()));
                errorOccurred = true;
                pageSaveResult=null;
            }
        }
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CLDEC14CCC03));
            errorOccurred = true;
            pageSaveResult=null;
        }
        //pageSaveURL = (String)pageSaveResult.getURL();
        //system.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().pageSaveURL --- '+ pageSaveURL);
        system.debug('VFC_AddProductForLiteratureReq.saveShoppingCart().pageSaveResult --- '+ pageSaveResult);
        return pageSaveResult; 
        
    }
    
    /*
     *  Button : Add product(s) 
     *  Add selected products from the search results to the shopping cart
     *****/
    public PageReference addProductToShoppingCart(){
        Set<String> setExistingGMRCode      = new Set<String>();
        Set<String> setGMRCodeTobeAdded     = new Set<String>();
        Set<String> setUniqueId             = new Set<String>();
        Set<String> setGMRCodeTobeQueried   = new Set<String>();
        
        // Retrieve the selected parameters on the page
        String selectedMasterCR = Apexpages.currentPage().getParameters().get('masterCR');
        String selected_products = Apexpages.currentPage().getParameters().get('selected_products');        
        
        if(selected_products != null && selected_products != '') {
            // Get the list of selected related products as GMRDataBean
            WS_GMRSearch.resultFilteredDataBean resultObjGMRDatBean_Products = (WS_GMRSearch.resultFilteredDataBean) JSON.deserialize(selected_products, WS_GMRSearch.resultFilteredDataBean.class);
            
            // Create a Literature Item for each product in the list
            if(resultObjGMRDatBean_Products != null && resultObjGMRDatBean_Products.gmrFilteredDataBeanList != null) {            
                for(WS_GMRSearch.gmrFilteredDataBean objGMRDatBean_Product :resultObjGMRDatBean_Products.gmrFilteredDataBeanList){
                    // Create a Literature Item with information of selected Product
                    LiteratureRequestLineItem__c objLiteratureItem = new LiteratureRequestLineItem__c();
                    if(objGMRDatBean_Product.ProductIdbFO != null){

                        objLiteratureItem.BusinessUnit__c = objGMRDatBean_Product.businessLine.label.UnEscapeXML();
                        objLiteratureItem.ProductLine__c = objGMRDatBean_Product.productLine.label.UnEscapeXML();
                        objLiteratureItem.ProductFamily__c = objGMRDatBean_Product.strategicProductFamily.label.UnEscapeXML();
                        objLiteratureItem.Family__c = objGMRDatBean_Product.family.label.UnEscapeXML();
                        objLiteratureItem.ProductDescription__c = objGMRDatBean_Product.description.UnEscapeXML();
                        objLiteratureItem.CommercialReference__c = objGMRDatBean_Product.commercialReference.UnEscapeXML();
                        objLiteratureItem.CommercialReference_lk__c = objGMRDatBean_Product.ProductIdbFO.UnEscapeXML();
                        
                        if(objGMRDatBean_Product.businessLine.value != null){
                            objLiteratureItem.TECH_GMRCode__c = objGMRDatBean_Product.businessLine.value;
                            //objLiteratureItem.Name            = objGMRDatBean_Product.businessLine.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.productLine.value != null){
                            objLiteratureItem.TECH_GMRCode__c += objGMRDatBean_Product.productLine.value;
                            //objLiteratureItem.Name            = objGMRDatBean_Product.productLine.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.strategicProductFamily.label != null){
                            objLiteratureItem.TECH_GMRCode__c += objGMRDatBean_Product.strategicProductFamily.value;
                            //objLiteratureItem.Name            = objGMRDatBean_Product.businessLine.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.family.label != null){
                            objLiteratureItem.TECH_GMRCode__c += objGMRDatBean_Product.family.value;
                            //objLiteratureItem.Name            = objGMRDatBean_Product.family.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.subFamily.label != null){
                            objLiteratureItem.TECH_GMRCode__c += objGMRDatBean_Product.subFamily.value;
                            //objLiteratureItem.Name            = objGMRDatBean_Product.subFamily.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.productSuccession.label != null){
                            objLiteratureItem.TECH_GMRCode__c += objGMRDatBean_Product.productSuccession.value;
                            //objLiteratureItem.Name            = objGMRDatBean_Product.productSuccession.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.productGroup.label != null){
                            objLiteratureItem.TECH_GMRCode__c += objGMRDatBean_Product.productGroup.value;
                            //objLiteratureItem.Name            = objGMRDatBean_Product.productGroup.label.UnEscapeXML();
                        }
                        
                        if(objGMRDatBean_Product.commercialReference != null && objGMRDatBean_Product.commercialReference.trim()!=''){
                            objLiteratureItem.CommercialReference__c            = objGMRDatBean_Product.commercialReference.UnEscapeXML();
                        }

                        if(LiteratureID!=null)
                            objLiteratureItem.TECH_UniqueId__c    = litReqRecord.Id + '-' + objLiteratureItem.CommercialReference_lk__c;
                        else
                            objLiteratureItem.TECH_UniqueId__c    = litReqRecord.Case__c + '-' + objLiteratureItem.CommercialReference_lk__c;

                        System.debug('VFC_AddProductForLiteratureReq.addProductToShoppingCart().TECH_UniqueId__c --- '+objLiteratureItem.TECH_UniqueId__c);
                        setUniqueId.add(objLiteratureItem.TECH_UniqueId__c);
                        
                        System.debug('VFC_AddProductForLiteratureReq.addProductToShoppingCart().Quantity__c --- '+objLiteratureItem.Quantity__c);
                        if(objLiteratureItem.Quantity__c==null || objLiteratureItem.Quantity__c < 1)
                            objLiteratureItem.Quantity__c = 1;
                        System.debug('VFC_AddProductForLiteratureReq.addProductToShoppingCart().Quantity__c after assign --- '+objLiteratureItem.Quantity__c);

                        // Compare the selectedMasterCR to the Product commercial reference to see if this product has been selected as Master
                        if(selectedMasterCR!=null && selectedMasterCR == objGMRDatBean_Product.commercialReference)
                            selectedMasterProduct = objLiteratureItem.TECH_UniqueId__c;
                        
                        System.debug('VFC_AddProductForLiteratureReq.addProductToShoppingCart().selectedMasterProduct --- '+selectedMasterProduct);
                        // Check if the product was previously removed from the existing related products list
                        // If yes, remove it from the list to delete.
                        System.debug('VFC_AddProductForLiteratureReq.addProductToShoppingCart().map_currCaseExistingProductsTechId2Id --- '+map_currCaseExistingProductsTechId2Id);
                        if(map_currCaseExistingProductsTechId2Id.containsKey(objLiteratureItem.TECH_UniqueId__c)){
                            if(set_currCaseListRemovedProducts.contains(map_currCaseExistingProductsTechId2Id.get(objLiteratureItem.TECH_UniqueId__c))){
                                set_currCaseListRemovedProducts.remove(map_currCaseExistingProductsTechId2Id.get(objLiteratureItem.TECH_UniqueId__c));
                            }
                        }
                        
                        // CHeck if the product is not already in the Map, to avoid erase the ID
                        if(!map_ShoppingCartProducts.containsKey(objLiteratureItem.TECH_UniqueId__c)) {
                            map_ShoppingCartProducts.put(objLiteratureItem.TECH_UniqueId__c,objLiteratureItem);
                        }
                        
                    }
                }
            }
        }
        
        // Uncheck the field MasterProduct__c of the previously Master product, and check it on the new one.
        if(selectedMasterProduct != null && selectedMasterProduct != '' && previousSelectedMasterProduct != null && map_ShoppingCartProducts.containsKey(selectedMasterProduct))
        {   
            if(map_ShoppingCartProducts.containsKey(previousSelectedMasterProduct))
                map_ShoppingCartProducts.get(previousSelectedMasterProduct).MasterProduct__c = false;
            
            map_ShoppingCartProducts.get(selectedMasterProduct).MasterProduct__c = true;
            previousSelectedMasterProduct = selectedMasterProduct;
        }
        
        return null;
    }
}