public class VFC_DownloadData {
    public SimpleView sv {get;set;}
    public Boolean hasAccess {get;set;}{hasAccess=false;}
	public VFC_DownloadData(ApexPages.StandardController controller){        
		View__c viewrecord=(View__c)controller.getRecord();
        List<Record__c> results = new List<Record__c>();
        View__c view = [SELECT Id,  Name, listviewdefinition__c, app__c,app__r.downloadablegroup__c FROM View__c where Id=:viewrecord.Id];        
		Integer hasuser = [select count() from groupappuserjunction__c where group__c=:view.app__r.downloadablegroup__c and user__c=:UserInfo.getUserId()];
        if(hasuser>0){
            hasAccess=true;       
           
        
        Type mytype = Type.forName('','ListViewDefinition');
        ListViewDefinition definition;
        if(view.listviewdefinition__c!=null)
            definition = (ListViewDefinition)JSON.deserialize(view.listviewdefinition__c,mytype);        
        //do some processing to build query
        List<Field__c> fielddefs = [select appdatafieldmap__c,displayname__c,fieldtype__c from Field__c where step__r.app__c =:view.app__c and appdatafieldmap__c!=null ];
        Map<String,Field__c> fieldmap = new Map<String,Field__c>();
        List<String> fieldsinquery = new List<String>();
        List<String> fieldlabels = new List<String>();
        Map<String,Object> returnresult = new Map<String,Object>();
        for (Field__c field : fielddefs) {    
            if(field.fieldtype__c!='Textarea'){
			fieldmap.put(field.appdatafieldmap__c,field);            
            fieldsinquery.add(field.appdatafieldmap__c);            
            fieldlabels.add('"'+field.displayname__c+'"');
            }
        } 
        String querytobeused = 'select Id,Name,status__c,iscurrentowner__c,step__r.RecordType.Name,CreatedDate,LastModifiedDate,CreatedBy.Name,LastModifiedBy.Name,action__c,';       
        if(definition==null)
        {           
            querytobeused += String.join(fieldsinquery,',')+' from Record__c where app__c = \''+view.app__c+'\' order by LastModifiedDate DESC';                       
            System.debug(querytobeused);                        
            results = Database.query(querytobeused);
            List<String> dataset = new List<String>();
            for(Record__c result : results){
                List<String> recorddata = new List<String>();
                recorddata.add('"'+result.Name+'"');
                recorddata.add('"'+result.action__c+'"');
                recorddata.add('"'+result.status__c+'"');
                for(String field:fieldsinquery){					
                    recorddata.add(result.get(field)!=null?((fieldmap.get(field).fieldtype__c=='Date'?((Date)result.get(field)).format():result.get(field))+'').escapeCsv():'');
                }
                recorddata.add('"'+result.LastModifiedBy.Name+'"');
                recorddata.add('"'+result.LastModifiedDate.format()+'"');
                recorddata.add('"'+result.CreatedBy.Name+'"');
                recorddata.add('"'+result.CreatedDate.format()+'"');
			    dataset.add(String.join(recorddata,','));            
            }
			sv = new SimpleView(dataset,String.join(fieldlabels,',')+'');            
        }
        else{            
            List<String> queryconditions = new List<String>();
            if(definition.statuscondition!=null)
                queryconditions.add('status__c=\''+definition.statuscondition+'\'');
            for(ListViewDefinition.Condition condition:definition.conditions)
            {
                if(condition.type=='equals'){
                    if(condition.fieldtype=='Number')
                    {
                        queryconditions.add(condition.appdatafieldmap+'='+condition.value);
                    }
                    else
                    {
                        queryconditions.add(condition.appdatafieldmap+'=\''+condition.value+'\'');
                    }
                }
                if(condition.type=='greaterthan'){
                    if(condition.fieldtype=='Number')
                    {
                        queryconditions.add(condition.appdatafieldmap+'>'+condition.value);
                    }                    
                }
                
                if(condition.type=='lessthan'){
                    if(condition.fieldtype=='Number')
                    {
                        queryconditions.add(condition.appdatafieldmap+'<'+condition.value);
                    }                    
                }
            }
            queryconditions.add('app__c=\''+view.app__c+'\'');
            querytobeused += String.join(definition.columns, ',')+' from Record__c where '+ String.join(queryconditions,' AND ') + ' order by LastModifiedDate DESC';			
            System.debug(querytobeused);                        
            results = Database.query(querytobeused);
			List<String> dataset = new List<String>();
            List<String> columnlabels = new List<String>();
            for(String column : definition.columns){
                if(fieldmap.containsKey(column))
                columnlabels.add('"'+fieldmap.get(column).displayname__c+'"');
            }
            for(Record__c result : results){
                List<String> recorddata = new List<String>();                
                recorddata.add('"'+result.Name+'"');
                recorddata.add('"'+result.action__c+'"');
                recorddata.add('"'+result.status__c+'"');
                for(String column : definition.columns){
                    if(fieldmap.containsKey(column))
                    recorddata.add(result.get(column)!=null?((fieldmap.get(column).fieldtype__c=='Date'?((Date)result.get(column)).format():result.get(column))+'').escapeCsv():'');
                }
                recorddata.add('"'+result.LastModifiedBy.Name+'"');
                recorddata.add('"'+result.LastModifiedDate.format()+'"');
                recorddata.add('"'+result.CreatedBy.Name+'"');
                recorddata.add('"'+result.CreatedDate.format()+'"');
                
			    dataset.add(String.join(recorddata,','));            
            }
			sv = new SimpleView(dataset,String.join(columnlabels,','));                            
        }      
        }
        else{
           sv = new SimpleView(null,'you don\'t have download access to this view');                             
        }
    }
     public class SimpleView{
         public List<String> data{get;set;}
         public String fields{get;set;}
        public SimpleView(List<String> data,String fields){
            this.data = data;
            this.fields = '"Name","Last Action",Status",'+fields+',"LastModifiedBy","LastModifiedDate","CreatedBy","CreatedDate"';
            
        }
    }  
}