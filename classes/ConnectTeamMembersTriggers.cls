/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  03-Feb-2012
    Modification Log    :   Added Partner on 10-May-2012
    Description         : Class for Connect Team Members Triggers, Assigns Champion Names for Team Members. The Validate ScopeEntity class validates if the selected
    // Scope and Entity are part of the Parent Program. Also Update Program Network on Record Deletion
*/

public with sharing class ConnectTeamMembersTriggers
{
    Public Static void TeamMembersBeforeInsert(List<Team_Members__c> Team)
      {
      
          System.Debug('****** TeamMembersBeforeInsert Start ****'); 
          
          // Update Champions
          
          list<Champions__c> Champion = new List<Champions__c>();
          Champion = [select Scope__c, Entity__c,Year__c, Smart_Cities_Division__c, GSC_Regions__c,Partner_Division__c,Champion_Name__r.FirstName, Champion_Name__r.LastName from Champions__c where IsDefault__c = :'Yes'];
          for(Team_Members__c T:Team){
             for(Champions__c C: Champion){
              if(!(T.Entity__c == Label.Connect_Smart_Cities) && !(T.Entity__c == Label.Connect_Partner) && !(T.Entity__c == Label.Connect_GSC)){
               If(T.Team_Name__c.Contains(C.Scope__c) && (T.Entity__c.Contains(C.Entity__c)) && (T.Year__c == C.Year__c)) {
                   T.Champion_Name__c = C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName;
                    }// If Ends
                    } //If Ends
                if((T.Entity__c == Label.Connect_Smart_Cities)){
                   If(T.Team_Name__c.Contains(C.Scope__c) && (T.Entity__c.Contains(C.Entity__c)) && (T.Year__c == C.Year__c) && (T.Smart_Cities_Division__c == C.Smart_Cities_Division__c)) {
                   T.Champion_Name__c = C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName;
                    }// If Ends
                    }//If Ends
                    
                if((T.Entity__c == Label.Connect_GSC)){
                   If(T.Team_Name__c.Contains(C.Scope__c) && (T.Entity__c.Contains(C.Entity__c)) && (T.Year__c == C.Year__c) && (T.GSC_Region__c == C.GSC_Regions__c)) {
                   T.Champion_Name__c = C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName;
                    }// If Ends
                    }//If Ends
                if((T.Entity__c == Label.Connect_Partner)){
                   If(T.Team_Name__c.Contains(C.Scope__c) && (T.Entity__c.Contains(C.Entity__c)) && (T.Year__c == C.Year__c) && (T.Partner_Region__c == C.Partner_Division__c)) {
                    T.Champion_Name__c = C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName;
                    }// If Ends
                    }//If Ends
                    
                  } // For loop 1 Ends
                 } // For loop 2 Ends
                 
                 System.Debug('****** TeamMembersBeforeInsert End ****'); 
                 
              
                 
             } // Static Class Ends
                
      Public Static void TeamMembersBeforeUpdate(List<Team_Members__c> Team)
         {
      
          System.Debug('****** TeamMembersBeforeUpdate Start ****'); 
          
          list<Champions__c> Champion = new List<Champions__c>();
          Champion = [select Scope__c, Entity__c, Year__c, Smart_Cities_Division__c, GSC_Regions__c,Partner_Division__c,Champion_Name__r.FirstName, Champion_Name__r.LastName from Champions__c where IsDefault__c = :'Yes'];
          for(Team_Members__c T:Team){
             for(Champions__c C: Champion){
              if(!(T.Entity__c == Label.Connect_Smart_Cities) && !(T.Entity__c == Label.Connect_Partner) && !(T.Entity__c == Label.Connect_GSC)){
               If(T.Team_Name__c.Contains(C.Scope__c) && (T.Entity__c.Contains(C.Entity__c)) && (T.Year__c == C.Year__c)) {
                   T.Champion_Name__c = C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName;
                    }// If Ends
                    } //If Ends
                if((T.Entity__c == Label.Connect_Smart_Cities)){
                   If(T.Team_Name__c.Contains(C.Scope__c) && (T.Entity__c.Contains(C.Entity__c)) && (T.Year__c == C.Year__c) && (T.Smart_Cities_Division__c == C.Smart_Cities_Division__c)) {
                   T.Champion_Name__c = C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName;
                    }// If Ends
                    }//If Ends
                    
                if((T.Entity__c == Label.Connect_GSC)){
                   If(T.Team_Name__c.Contains(C.Scope__c) && (T.Entity__c.Contains(C.Entity__c)) && (T.Year__c == C.Year__c) && (T.GSC_Region__c == C.GSC_Regions__c)) {
                   T.Champion_Name__c = C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName;
                    }// If Ends
                    }//If Ends
                if((T.Entity__c == Label.Connect_Partner)){
                   If(T.Team_Name__c.Contains(C.Scope__c) && (T.Entity__c.Contains(C.Entity__c)) && (T.Year__c == C.Year__c) && (T.Partner_Region__c == C.Partner_Division__c)) {
                    T.Champion_Name__c = C.Champion_Name__r.FirstName + ' ' + C.Champion_Name__r.LastName;
                    }// If Ends
                    }//If Ends
                    
                  } // For loop 1 Ends
                 } // For loop 2 Ends
                 
                 
                   // Update Program Network
                
            List<Team_Members__c> T = new List<Team_Members__c>();         
            List<Team_Members__c> lstUpdate = new List<Team_Members__c>();
            
            T = [Select Name, Program_Network__c, Program_Name__c from Team_Members__c where Program_Name__c = :Team[0].Program_Name__c and Name != :Team[0].Name];
                 
            For(Team_Members__c Tm : T) {
               Tm.Program_Network__c = False;
               lstUpdate.add(Tm);
               System.Debug('Team name: ' + Tm.Name);
               }
               Update lstUpdate;  
                 System.Debug('****** TeamMembersBeforeUpdate End ****'); 
                 
                } // Static Class Ends
                
             // Class to Validate Scope and Entity with the Parent Program
             
            
             
             Public Static void TeamMembersBeforeInsert_ValidateScopeEntity(List<Team_Members__c> Team)
              {
                  List<Project_NCP__c> Program = new List<Project_NCP__c>();
                  Program = [Select Name, Global_Functions__c, Power_Region__c, Global_Business__c, GSC_Regions__c, Partner_Region__c, Smart_Cities_Division__c from Project_NCP__c where Year__c = :Team[0].Year__c];
                  integer GF; integer GB; integer PR; integer GSC; integer PAR; integer SC;
                  for(Team_Members__c T:Team){
                  GF = 0;
                  GB = 0;
                  PR = 0;
                  GSC = 0;
                  PAR = 0;
                  SC = 0;
                     for(Project_NCP__c P: Program){
                         if(T.Program_Name__c == P.Name){
                           if(((P.Global_Functions__c == null)&& (T.Team_Name__c == Label.Connect_Global_Functions)) ||((P.Global_Business__c == null)&& (T.Team_Name__c == Label.Connect_Global_Business)) ||((P.Power_Region__c == null)&& (T.Team_Name__c == Label.Connect_Power_Region))||((P.GSC_Regions__c == null)&& (T.Team_Name__c == Label.Connect_GSC)))
                              T.ValidateScopeonInsertUpdate__c = True;
                              
                             if((T.Team_Name__c == Label.Connect_Global_Functions) && (P.Global_Functions__c != null) && !(P.Global_Functions__c.Contains(T.Entity__c)))
                               GF = GF + 1;
                             
                             if((T.Team_Name__c == Label.Connect_Global_Business) && (P.Global_Business__c != null) && !(P.Global_Business__c.Contains(T.Entity__c)))
                               GB = GB + 1;
                               
                               if((T.Team_Name__c == Label.Connect_Power_Region) && (P.Power_Region__c != null) && !(P.Power_Region__c.Contains(T.Entity__c)))
                               PR = PR + 1;
                               
                               if(T.GSC_Region__c != null)
                               if((T.Entity__c == Label.Connect_GSC) && (P.GSC_Regions__c != null) && !(P.GSC_Regions__c.Contains(T.GSC_Region__c)))
                               GSC = GSC + 1;
                               
                               if(T.Partner_Region__c != null)
                               if((T.Entity__c == Label.Connect_Partner) && (P.Partner_Region__c != null) && !(P.Partner_Region__c.Contains(T.Partner_Region__c)))
                               PAR = PAR + 1;
                               
                               if(T.Smart_Cities_Division__c != null)
                               if((T.Entity__c == Label.Connect_Smart_Cities) && (P.Smart_Cities_Division__c != null) && !(P.Smart_Cities_Division__c.Contains(T.Smart_Cities_Division__c)))
                               SC = SC + 1;
                               
                               } // if Ends
                               
                               if((GF > 0) || (GB > 0) || (PR > 0) || (GSC > 0) || (PAR > 0)|| (SC > 0))
                                   T.ValidateScopeonInsertUpdate__c = True;
                              } // For Ends
                             } //For Ends
                             
   
                  
              }
              
   // Change Deployment Network Status Before Deleting a Deployment Network Record    
   
   Public Static void TeamMembersBeforeDelete(List<Team_Members__c> T)
      {
      
          System.Debug('****** TeamMembersBeforeDelete Start ****'); 
          List<Team_Members__c> Team = new List<Team_Members__c>();
          //System.Debug('Team Delete ' + T[0].Program_Name__c);
          List<Team_Members__c> lstUpdate = new List<Team_Members__c>();
         Team = [Select Name, Program_Network__c, Program_Name__c from Team_Members__c where Program_Name__c = :T[0].Program_Name__c and Year__c = :T[0].Year__c];
          System.Debug('Team size: ' + Team.size());         
            For(Team_Members__c Tm : Team) {
               Tm.Program_Network__c = False;
               lstUpdate.add(Tm);
               System.Debug('Team name: ' + Tm.Name);
               }
               Update lstUpdate;
            System.Debug('****** TeamMembersBeforeDelete Stop ****'); 
    }     
                             
                 
              }