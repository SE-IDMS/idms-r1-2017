//Test class for AP_ProjectTeam_SharingAccessToProject - OCT 2015 Release
@isTest
public class AP_ProjectTeam_SharingAccessToPrj_TEST{
     static testMethod void addDeletePrjTeamMemeber(){
         Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        Database.insert(acc); 
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        insert prj;
        
        OPP_ProjectTeam__c  prjTeam = Utils_TestMethods.createMasterProjectTeamMember(prj.Id,UserInfo.getUserId());
        insert prjTeam;
        delete prjTeam;    
     }
}