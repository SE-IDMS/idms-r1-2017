/*
    Author          : Nicolas Palitzyne (Accenture) 
    Date Created    : 21/03/2011
    Description     : Session Review Controller
*/

public class VFC02_Session 
{
    public VFC02_Session (ApexPages.StandardController stdController)
    {
        this.session = (TMT_Session__c)stdController.getRecord();
    }

    /*============================================================================
      Variables
    =============================================================================*/         
    TMT_Session__c session;
    public List<RegistrationsWrapper> RegiList = new List<RegistrationsWrapper>();
    List<TMT_Registration__c> MissedRegistrations = new List<TMT_Registration__c>();
    List<TMT_Registration__c> PassedRegistrations = new List<TMT_Registration__c>();
    List<TMT_Registration__c> AttendedRegistrations = new List<TMT_Registration__c>();
    List<TMT_Registration__c> NotAttendedRegistrations = new List<TMT_Registration__c>();
    public Integer sizeR;
    
    /*============================================================================
      Inner Class
    =============================================================================*/      
    
    public class RegistrationsWrapper
    {
        public TMT_Registration__c Regi{get;set;}
        public Boolean Attended{get;set;}
        public Boolean Passed{get;set;}
        public Boolean NotAttended{get;set;}
        
        public RegistrationsWrapper(TMT_Registration__c R)
        {
            Regi = R;
            Attended=false;
            Passed=false;
            NotAttended=false;
            
            If(Regi.Status__c=='Fail')
                Attended = true;
            If(Regi.Status__c=='Pass')    
                Passed = true;
            If(Regi.Status__c=='Did not attend')    
                NotAttended = true;
        }
    }
    
    /*============================================================================
      Getters
    =============================================================================*/      
  
    // Get the number of related registrations
    public Integer getSizeR()
    {
        List<RegistrationsWrapper> LR = GetRegistrations();
        Integer SR = LR.size();
        return SR;
    }  
  
    // Registrations Wrapper Getter  
    Public List<RegistrationsWrapper> GetRegistrations()
    {
        List<TMT_Registration__c> RList = new List<TMT_Registration__c>();
        RList = [SELECT Id, Name, Trainee__c, Status__c, Session__c, Comment__c FROM TMT_Registration__c WHERE Session__c=:session.id]; 
        
        for(Integer i=0; i<Rlist.size(); i++)
        {
            // Check if the value has not already been added
            TMT_Registration__c R = new TMT_Registration__c();
            R = RList[i];
            if(RegiList.size()>0)
            {
                if(RegiList[0].Regi.Name == R.Name)
                    return RegiList;
            }
            
            RegiList.add(new RegistrationsWrapper(R));
        }

        return RegiList;
    }
    
    // Session Getter
    public TMT_Session__c getSession() 
    {
        TMT_Session__c session_SOQL = [SELECT Id, Name, Curriculum__c, Start__c, Date__c, Time__c FROM TMT_Session__c Where ID=:Session.id];
        return session_SOQL;
    }
    
    // Curriculum Getter
    public TMT_Curriculum__c getCurriculum()
    {
        TMT_Session__c session_SOQL = [SELECT Id, Name, Curriculum__c FROM TMT_Session__c Where ID=:session.id limit 1];
        TMT_Curriculum__c Curriculum_SOQL = [SELECT Id, Name FROM TMT_Curriculum__c Where ID=:session_SOQL.Curriculum__c limit 1];
        
        return Curriculum_SOQL;
    }
    
    // List of Registrations included in the Wrapper Getter
    public List<RegistrationsWrapper> GetRegiList()
    {
        return RegiList;
    }
    
    // Passed Registrations Wrapper
    public PageReference getPassed()
    {
        PassedRegistrations.clear();
        for(RegistrationsWrapper RegiWrapper : RegiList)
        {
            if(RegiWrapper.passed == true)
                PassedRegistrations.add(RegiWrapper.Regi);
            
            if(RegiWrapper.attended == true || RegiWrapper.NotAttended == true)
            {
                RegiWrapper.attended = false;
                RegiWrapper.NotAttended = false;
            }
        }
    
        return null;   
    }
    
    // Attended Registrations Wrapper
    public PageReference getAttended()
    {
        AttendedRegistrations.clear();
        for(RegistrationsWrapper RegiWrapper : RegiList)
        {
            if(RegiWrapper.Attended == true)
                AttendedRegistrations.add(RegiWrapper.Regi);
                
            if(RegiWrapper.passed == true || RegiWrapper.NotAttended == true)
            {
                RegiWrapper.passed = false;
                RegiWrapper.NotAttended = false;
            }
        }
        return null;
    }
    
    // Not Attended Registrations Wrapper
    public PageReference getNotAttended()
    {
        NotAttendedRegistrations.clear();
        for(RegistrationsWrapper RegiWrapper : RegiList)
        {
            if(RegiWrapper.NotAttended == true)
                NotAttendedRegistrations.add(RegiWrapper.Regi);
                
            if(RegiWrapper.attended == true || RegiWrapper.passed == true)
            {
                RegiWrapper.attended = false;
                RegiWrapper.passed = false;
            }
        }
        return null;
    }
    
    // Dummy method for empty table display
    public void GetEmpty(){}
    
    /*============================================================================
      Functions
    =============================================================================*/          
    
    // Save Method
    public PageReference save()
    {
       System.debug('#### RegiWrapper: '+ RegiList);
       System.debug('#### Attended: '+ AttendedRegistrations);
       System.debug('#### Passed: '+ PassedRegistrations);
       System.debug('#### NotAttended: '+ NotAttendedRegistrations);
           
        // Get Attended Registrations
        AttendedRegistrations.clear();
        for(RegistrationsWrapper RegiWrapper : RegiList)
        {
            if(RegiWrapper.Attended == true )
                AttendedRegistrations.add(RegiWrapper.Regi);
        }

        // Change status to attended for all Registration within the Attended Registrations List
        for(TMT_Registration__c R:AttendedRegistrations)
        {
            R.Status__c = 'Fail';
        }
        
         // Get Passed Registrations
        PassedRegistrations.clear();
        for(RegistrationsWrapper RegiWrapper : RegiList)
        {
            if(RegiWrapper.passed == true)
                PassedRegistrations.add(RegiWrapper.Regi);
        }
        
        // Change status to Passed for all Registration within the Passed Registrations List
        for(TMT_Registration__c R:PassedRegistrations)
        {
            R.Status__c = 'Pass';
        }
        
         // Get Not Attended Registrations
        NotAttendedRegistrations.clear();
        for(RegistrationsWrapper RegiWrapper : RegiList)
        {
            if(RegiWrapper.notattended == true)
                NotAttendedRegistrations.add(RegiWrapper.Regi);
        }
        
        // Change status to Did not attend for all Registration within the Not Attended Registrations List
        for(TMT_Registration__c R:NotAttendedRegistrations)
        {
            R.Status__c = 'Did not attend';
        }

        MissedRegistrations.clear();
        for(RegistrationsWrapper RegiWrapper : RegiList)
        {
            if(RegiWrapper.Regi.Status__c == 'Registered')
                MissedRegistrations.add(RegiWrapper.Regi);
        }
        
        // Change status to Missed for all Registration within the Missed Registrations List
        for(TMT_Registration__c R:MissedRegistrations)
        {
            R.Status__c = 'Miss';
        }
        
        // Update the Missed Record
        List<Database.SaveResult> VFC02_SessionSR0 = Database.Update(MissedRegistrations);
        // Update the Attended Record (which will eventually overwrite the missed one)
        List<Database.SaveResult> VFC02_SessionSR1 = Database.Update(AttendedRegistrations);
        // Update the Passed Record (which will eventually overwrite the attended one)
        List<Database.SaveResult> VFC02_SessionSR2 = Database.Update(PassedRegistrations);
        // Update the Not Attended Record (which will eventually overwrite the attended one)
        List<Database.SaveResult> VFC02_SessionSR3 = Database.Update(NotAttendedRegistrations);
        
        // Clear Lists       
        PassedRegistrations.clear();
        AttendedRegistrations.clear(); 
        NotAttendedRegistrations.clear();
        RegiList.clear();    
       
        // Return the default review page
        PageReference SessionPage = new ApexPages.StandardController(session).view();
        SessionPage.setRedirect(true);
        return SessionPage;
    }
    
    // Cancel Methode
    public PageReference Cancel()
    {
        PageReference SessionPage = new ApexPages.StandardController(session).view();
        SessionPage.setRedirect(true);
        return SessionPage;
    }
    
}