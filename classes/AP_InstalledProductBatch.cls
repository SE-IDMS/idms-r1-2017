/*
    Author          : Deepak
    Date Created    : 
    Description     : Batch Apex class For Product
              
*/
global class AP_InstalledProductBatch{
       
    global static void ProcessProduct(List<Sobject> scope)
    {
        List<SVMXC__Installed_Product__c> prolist = New List<SVMXC__Installed_Product__c> ();   
        List<SVMXC__Installed_Product__c> proRcordlist = New List<SVMXC__Installed_Product__c> ();
        
        for(Sobject sobjectRecord :scope)
        {
            SVMXC__Installed_Product__c proRecord = (SVMXC__Installed_Product__c)sobjectRecord;
            prolist.add(proRecord );
        }
        
        if(prolist.size()>0){
              Integer i = 0;
            for(SVMXC__Installed_Product__c pr :prolist){
             
                if(pr.Batch_RecordUpdate__c == False)
                {
                    
                    pr.Batch_RecordUpdate__c = true;
                    proRcordlist.add(pr);
                    system.debug('proRcordlist11:'+proRcordlist);
               }
            }
        }
        
        if(proRcordlist.size()>0){
            update proRcordlist;
            system.debug('proRcordlist:'+proRcordlist);
            
        }
    } 
}