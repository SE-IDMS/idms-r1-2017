/*
    Author          : Abhishek (ACN) 
    Date Created    : 26/05/2011
    Description     : Test class for the apex class AP17_SharingAccessToSMETeam.
*/


@isTest

private class AP17_SharingAccessToSMETeam_TEST {
    
/* @Method <This method CaseTriggerTestMethod is used test the AP17_SharingAccessToSMETeam>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
*/
    static testMethod void GrantAcessToQteLink_TestMethod()
    {
        Account accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts); 
        
        Opportunity opportunity= Utils_TestMethods.createOpportunity(accounts.id);
        Database.insert(opportunity);                      
              
        OPP_QuoteLink__c newQuoteLink=Utils_TestMethods.createQuoteLink(opportunity.id);
        Database.insert(newQuoteLink);
        
        user newUser=Utils_TestMethods.createStandardUser('test');
        Database.insert(newUser);
        
        RIT_SMETeam__C sme = Utils_TestMethods.createSME(newQuoteLink.id,'legal');       
        Database.insert(sme);           
        sme.TeamMember__c=newUser.id;
        Database.Update(sme);   
             
        RIT_SMETeam__C smeNew = Utils_TestMethods.createSME(newQuoteLink.id,'legal');
        smeNew.TeamMember__c=newUser.id; 
        database.insert(smeNew);         
        database.delete(smeNew);                                
      
        sme.TeamMember__c=accounts.id;   
        try
        {
            database.Update(sme);
        }
        catch(Exception e)
        {
        }
        
    }
}