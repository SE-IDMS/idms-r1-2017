/*
    Author          : ACN 
    Date Created    : 20/07/2011
    Description     : Test class for VFC43_LeadsAscWithOpp class 
*/
@isTest
private class VFC43_LeadsAscWithOpp_TEST 
{

    static testMethod void testdisplayLeads() 
    {
             
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        
        User sysAdmin = Utils_TestMethods.createStandardUser('sysAdmin');
        insert sysAdmin;
        
        if(profiles.size()>0)
        {
         User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC43');                
         user.byPassVR__C = true;
         user.byPassWF__C = false;
         system.runas(user)
         {
            test.startTest();          
            
            Lead lead2 = Utils_TestMethods.createLead(null, null,100);
            insert lead2;

            lead2.ClassificationLevel1__c='LC';
            lead2.LeadingBusiness__c='BD';
            lead2.SuspectedValue__c=100;
            lead2.ZipCode__c='123789';
            lead2.OpportunityFinder__c ='Lead Agent';
            lead2.City__c='US';
            lead2.Street__c = 'testStreet';
            update lead2;
            
            Database.LeadConvert leadConv = new database.LeadConvert();
            leadConv.setLeadId(lead2.id);
            leadConv.setConvertedStatus('3 - Closed - Opportunity Created');
             System.RunAs(sysAdmin)
            {
                Database.LeadConvertResult lcr = Database.convertLead(leadConv);
            }
                        
            lead2 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,opportunity__c from lead where Id =: lead2.Id limit 1];            
            
            System.assertEquals('3 - Closed - Opportunity Created',lead2.Status);
            //System.assertEquals('Opportunity Created',lead2.SubStatus__c);            
            System.assertEquals(lead2.convertedOpportunityId,lead2.Opportunity__c);            
            Opportunity opp = [select Id,TECH_CreatedFromLead__c from Opportunity where Id=:lead2.ConvertedOpportunityId limit 1];
            
            Lead lead1 = Utils_TestMethods.createLead(opp.Id, null,100);
            insert lead1;            
            lead1 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            System.assertEquals(opp.Id,lead1.Opportunity__c);

            ApexPages.StandardController sc = new ApexPages.StandardController(opp);
            VFC43_LeadsAscWithOpp leadsAscWithOpp= new VFC43_LeadsAscWithOpp(sc);
            leadsAscWithOpp.displayLeads();
            test.stopTest();
         }
       }
    }
}