/*
    Author          : Abhishek 
    Date Created    : 22/07/2011
    Description     : Class utilised to override Standard Clone Button.
*/
/*
Modification History:
---------------------
Date: 2/11/2011
Author: Siddharatha Nagavarapu(Global Delivery Team)
Purpose: UAT imporvement for the DEF- 113

Date: 30/01/2015
Purpose: Added filters Primary Coverage Model as “Sales Rep” or “Inside Sales" and Tobedeleted as false. 
*/
public class VFC49_CloneCAP 
{
    public SFE_IndivCAP__c indivCAP{get;set;}{indivCAP  = new SFE_IndivCAP__c();}
    public List<SFE_IndivCAP__c> indivCAPs  = new List<SFE_IndivCAP__c>();   
    public SFE_IndivCAP__c  clonedIndivCAP= new SFE_IndivCAP__c();
    public List<SFE_PlatformingScoring__c> clonedPlatformingScor = new List<SFE_PlatformingScoring__c>();   
    public List<SFE_PlatformingScoring__c> platformingScor = new List<SFE_PlatformingScoring__c>();       
    public SFE_PlatformingScoring__c platformScoring = new SFE_PlatformingScoring__c();
    public String capFilter;
    public String platformFilter;
    public boolean displayErrorMessage{get;set;}{displayErrorMessage=true;}
    public Integer processhere=1450;
    public Integer BatchSize=2000;
    private List<Id> tobeprocessedinfuturemethod=new List<Id>();
    private List<Id> tobeprocessedfortargetting=new List<Id>();
  //  public Integer futuremethodmincounter{get;set;}{futuremethodmincounter=0;}
    
 //   public Integer nooffuturemethodsrunning{get;set;}{nooffuturemethodsrunning=0;}
    
    /*
    Method Name: VFC_49CloneCAP
    Modified by: Siddharatha Nagavarapu(Global Delivery Team)
    
    */
    public VFC49_CloneCAP(ApexPages.StandardController controller) 
    {          
        indivCAP=(SFE_IndivCAP__c)controller.getRecord();                 
    }
    
    /*
    Method Name: cloneCAP
    Modified by: Siddharatha Nagavarapu(Global Delivery Team)
    Modification: A new condition is introduced to process the existing logic based on the displayerrormessage variable
    if the variable is true then the message is displayed.
    */      
    public pagereference cloneCAP()
    {
        system.debug('cloneCAP method Begins');
//        indivCAP=[SELECT AssignedTo__c,AttractiAxisThreshold1__c,AttractiAxisThreshold2__c,Comments__c,Country__c,CreatedById,CreatedDate,CriterWeightQu1__c,CriterWeightQu2__c,CriterWeightQu3__c,CriterWeightQu4__c,CriterWeightQu5__c,CriterWeightQu6__c,CriterWeightQu7__c,CriterWeightQu8__c,CurrencyIsoCode,CustPlatfViewAs__c,EndDate__c,G1VisitFreq__c,G2VisitFreq__c,G3VisitFreq__c,Id,IsDeleted,LastModifiedById,LastModifiedDate,MktShareThreshold1__c,MktShareThreshold2__c,Name,NbrVisit__c,OwnerId,PlatformingScoringsCompleted__c,Q1VisitFreq__c,Q2VisitFreq__c,Q3VisitFreq__c,S1VisitFreq__c,S2VisitFreq__c,S3VisitFreq__c,StartDate__c,Status__c,SystemModstamp,TECH_IsEventsGenerated__c,TECH_SendEmail__c FROM SFE_IndivCAP__c where id=:indivCAP.id];        
        indivCAP=[SELECT OwnerId,id FROM SFE_IndivCAP__c where id=:indivCAP.id];
        System.debug('after indivcap query--Limits.getHeapSize'+Limits.getHeapSize()+'------Limits.getLimitHeapSize--'+Limits.getLimitHeapSize());
        if(indivCAP.ownerID != UserInfo.getUserId()){
            String profileName=[select Name from Profile where Id=:UserInfo.getProfileId()].Name;//the profile name of the current User is retreived
            List<String> profilePrefixList=(System.Label.CL00632).split(',');//the valid profile prefix list is populated
            for(String profilePrefix : profilePrefixList)//The profile name should start with the prefix else the displayerrormessage flag is set to true
            {
                if((profileName+'').startsWith(profilePrefix))
                displayErrorMessage=false;
            }
        }
        else                
        displayErrorMessage=false;
        if(displayErrorMessage){
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,System.Label.CL00633));                  
        return null;
        }
        else
        {
            String indivCAPQuery;
            String platformScoringQuery;
            String existingCAPOwner;
            if(indivCAP!=null && indivCAP.Id!=null)        
            {
            capFilter ='WHERE Id =\'' +indivCAP.Id+'\' limit 1';
            }                        
            indivCAPs  = sObjectFields(indivCAP,capFilter);                                    
            if(!indivCAPs.isEmpty())
            {
                clonedIndivCAP  = indivCAPs[0].clone(false,true);
                existingCAPOwner = indivCAPs[0].assignedTo__c;
                clonedIndivCAP.Status__c=Label.Cl00450;     
                /* Modif for uat */   
                clonedIndivCAP.PlatformingScoringsCompleted__c = false;
                clonedIndivCAP.TECH_IsEventsGenerated__c = false;
                clonedIndivCAP.Name = indivCAPs[0].Name + '(Cloned)';
                  /* End of modif */
                Database.SaveResult sr= database.insert(clonedIndivCAP);
                if(!sr.isSuccess())
                System.Debug('######## VFC49 Error Inserting: '+sr.getErrors()[0].getStatusCode()+' '+sr.getErrors()[0].getMessage());                                     
            }
            //for handling heap exception
             indivCAPs.clear();             
                                                                                                          
            if(indivCAP!=null && indivCAP.Id!=null)
            platformFilter = 'WHERE IndivCAP__c =\''+indivCAP.Id +'\'and PlatformedAccount__r.OwnerId = \''+existingCAPOwner+'\' and PlatformedAccount__r.ToBeDeleted__c=false and PlatformedAccount__r.Inactive__c=false and ( PlatformedAccount__r.PrimaryRelationshipLeader__c=\''+Label.CLDEC14SLS02 +'\' OR PlatformedAccount__r.PrimaryRelationshipLeader__c=\''+Label.CLDEC14SLS03+'\' ) limit 1450';            
            platformingScor = sObjectFields(platformScoring,platformFilter);             
    
            for(SFE_PlatformingScoring__c platScore : platformingScor)
            {
                        SFE_PlatformingScoring__c platformScore = new SFE_PlatformingScoring__c();
                        platformScore = platScore.clone(false,true);
                        platScore=null;
                        platformScore.IndivCAP__c= clonedIndivCAP.id;  
                        platformScore.TECH_UniqueKey__c='';
                        clonedPlatformingScor.add(platformScore);  
                        tobeprocessedfortargetting.add(platformScore.PlatformedAccount__c);
            }
        //    futuremethodmincounter=clonedPlatformingScor.size();
            if(clonedPlatformingScor.size()==processhere)
            {
                for(SFE_PlatformingScoring__c sfps:[select id,PlatformedAccount__c from SFE_PlatformingScoring__c where id not in :clonedPlatformingScor and IndivCAP__c =:indivCAP.Id and PlatformedAccount__r.ToBeDeleted__c=false and PlatformedAccount__r.Inactive__c=false and (PlatformedAccount__r.PrimaryRelationshipLeader__c=:Label.CLDEC14SLS02 OR PlatformedAccount__r.PrimaryRelationshipLeader__c=:Label.CLDEC14SLS03) limit 20000]){
                tobeprocessedinfuturemethod.add(sfps.id);
                tobeprocessedfortargetting.add(sfps.PlatformedAccount__c);
                }
                
                if(tobeprocessedinfuturemethod.size()>0)
                callFutureMethods(tobeprocessedinfuturemethod);
                
                
            } 
                          
              
               
            if(clonedPlatformingScor.Size() > 0)
            {
                if(clonedPlatformingScor.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
                {
                    System.Debug('######## vfc49 Error Inserting : '+ Label.CL00264);
                }
                else
                {
                    Database.SaveResult[] scoring=database.insert(clonedPlatformingScor);
                    for(Database.SaveResult sr: scoring)
                    {
                        if(!sr.isSuccess())
                            System.Debug('######## vfc49 Error Inserting: '+sr.getErrors()[0].getStatusCode()+' '+sr.getErrors()[0].getMessage());                    
                    }
                }                         
            }                                                   
          //  ApexPages.currentPage().getParameters().put('id', clonedIndivCAP.id);
          //  VFC04_SFETargetting psc = new VFC04_SFETargetting();
          //  psc.createPlatformScorings();
             /* Modif for UAT */
        /*    if(clonedPlatformingScor.size()==processhere)
            return null;            
            else*/            
            VFC04_SFETargetting psc = new VFC04_SFETargetting();
            psc.createPlatformScoringsFromClone(tobeprocessedfortargetting,clonedIndivCAP);
             
            return new pagereference('/'+clonedIndivCAP.id+'/e?retURL=%2F'+clonedIndivCAP.id);                
            
            /* End of Modif */
        }
    }  
    /*
    public PageReference hasprocessdall()
    {
       if(futuremethodmincounter>=0 && futuremethodmincounter<processhere)
       {
           return null;
       }
       else if(futuremethodmincounter==processhere)
       {
           nooffuturemethodsrunning=[SELECT count() FROM AsyncApexJob WHERE JobType = 'Future' AND ApexClassId = '01pA0000002hIIu' AND MethodName = 'performInsertForPlaftormAccounts' AND CompletedDate = null];
           if(nooffuturemethodsrunning>0)
           return null;
           else
           return new pagereference('/'+clonedIndivCAP.id+'/e?retURL=%2F'+clonedIndivCAP.id);                                   
       }
       return new pagereference('/'+clonedIndivCAP.id+'/e?retURL=%2F'+clonedIndivCAP.id);                                   
                
    }
      */ 
    
    public void callFutureMethods(List<Id> platformingscoringlist)
    {
    List<Id> apexasyncIds=new List<Id>();
    Integer batchcounter=0;  
    Set<Id> platscoreIds=new Set<Id>();  
    
        for(Id platscoreid: platformingscoringlist)
        {            
            platscoreIds.add(platscoreid);
            batchcounter++;
            if(batchcounter==BatchSize)
            {                
                batchcounter=0;
                String idslst='(';
                for(Id pid:platscoreIds)
                idslst+='\''+pid+'\',';
                idslst=idslst.substring(0,idslst.length()-1);
                idslst+=')';
                VFC49_CloneCAP.performInsertForPlaftormAccounts(idslst,clonedIndivCAP.id);
                platscoreIds.clear();                
            }
        }
        if(platscoreIds.size()>0){
        String idslst='(';
                for(Id pid:platscoreIds)
                idslst+='\''+pid+'\',';
                idslst=idslst.substring(0,idslst.length()-1);
                idslst+=')';
        VFC49_CloneCAP.performInsertForPlaftormAccounts(idslst,clonedIndivCAP.id);
        platscoreIds.clear();     
        }           
    }   
    
     @future
    static void performInsertForPlaftormAccounts(String idslst,Id Capid){
        String errorLog='Errors in creating Platforming Accounts while cloning';
        Integer errorcount=0;                                
         List<SFE_PlatformingScoring__c> tobeinserted=new  List<SFE_PlatformingScoring__c>();
         
         List<SFE_PlatformingScoring__c> platformingScor=new List<SFE_PlatformingScoring__c>();
         SFE_PlatformingScoring__c platformScoring = new SFE_PlatformingScoring__c();
        if(idslst.length()>3)
        {
            String platformFilter = 'WHERE id in '+idslst+' limit 2000';
            platformingScor = sObjectFields(platformScoring,platformFilter);  
        }
        //   
       //     
        
         if(!platformingScor.isEmpty())    
            {          
                for(SFE_PlatformingScoring__c platScore : platformingScor)
                {              
                            SFE_PlatformingScoring__c platformScore = new SFE_PlatformingScoring__c();
                            platformScore = platScore.clone(false,true);
                            platformScore.IndivCAP__c= Capid;  
                            platformScore.TECH_UniqueKey__c='';
                            tobeinserted.add(platformScore);                 
                }  
            }               
        
        List<Database.SaveResult> lsr=Database.insert(tobeinserted,false);
        for(Database.SaveResult sr:lsr)
        {            
           if(!sr.isSuccess())
           {
              if(sr.getErrors()[0].getStatusCode() != StatusCode.DUPLICATE_VALUE)
              {
                  errorLog+='-'+sr.getErrors()[0];
                  errorcount++;
              }
           }   
        }
        if(errorcount>0 || Test.isRunningTest())
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> mails=new List<String>();
            mails.add('siddharatha.n@gmail.com');
            mail.setToAddresses(mails);
            mail.setSubject('Error Log while cloning a record and creating Platforming accounts for a commercial action plan record '+capId);
            mail.setPlainTextBody(errorLog);
            
            if(!Test.isRunningTest())                          // ensure we never actually send an email during //a Test Run
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       }
        
    }
    
    public static List<Sobject> sObjectFields(Sobject sobj, String filterCriteria)
    {
    System.debug('Sobject Fields method start--Limits.getHeapSize'+Limits.getHeapSize()+'------Limits.getLimitHeapSize--'+Limits.getLimitHeapSize());               
        Schema.SobjectType sobjType = sobj.getSobjectType();
        List<Sobject> sobjects = new List<Sobject>();
        List<String> sObjectFields = new List<String>{};  
        String fields;  
        String query;    
        if(sobjType != null)
        {
            sObjectFields.addAll(sobjType.getDescribe().fields.getMap().keySet());
        }

        for (Integer i=0;i <sObjectFields.size();i++)
        {
            if(i == 0)
                fields = sObjectFields.get(i);
            else
                fields+= ', ' + sObjectFields.get(i);
        } 
        query = 'SELECT ' + fields + ' FROM '+sobjType;         
        if(filterCriteria!=null)
        {
        query = query + ' '+ filterCriteria;      
        }
        System.debug('the query'+query);
        sObjectFields.clear();
      try
      {  
            sobjects = Database.query(query);            
      }
      catch (exception e)
      {
      System.debug('Exception in getting the complete sobjects'+e.getMessage()+'-----'+e);
          //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,e.getMessage()));                  
      }
      System.debug('Sobject Fields method end--Limits.getHeapSize'+Limits.getHeapSize()+'------Limits.getLimitHeapSize--'+Limits.getLimitHeapSize());            
        return sobjects;
    }                
}