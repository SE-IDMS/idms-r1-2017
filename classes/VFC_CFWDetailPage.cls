public with sharing class VFC_CFWDetailPage {

// Declarations 
public Boolean isIDCardView{get;set;}
public Boolean isBusinessView{get;set;}
public Boolean isDataView{get;set;}
public Boolean isAppView{get;set;}
public Boolean isInfView{get;set;}
public Boolean isserdepView{get;set;}
public CFWRiskAssessment__c CFWrecord{get;set;}
public string ParaRecType;
private ApexPages.StandardController stdcontroller;


public VFC_CFWDetailPage(ApexPages.StandardController stdcontroller) {
    
        
    // Initialization

      isIDCardView =False;
      isBusinessView=False;
      isDataView = False;
      isAppView = False;
      isInfView = False;
    
      
    CFWRecord = (CFWRiskAssessment__c)stdcontroller.getRecord(); 
   // CFWRecord= Database.query(queryAllFields());
    this.stdcontroller =  stdcontroller;
         
    ParaRecType = ApexPages.currentPage().getParameters().get('RecordType');
    
    if(ParaRecType == Label.CLOCT15CFW01){
      isIDCardView=True;
       }
    else if(ParaRecType == Label.CLOCT15CFW02){     
      isBusinessView=True;         
    }
   else if(ParaRecType == Label.CLOCT15CFW03){      
      isDataView = True;    
    }    
    else if(ParaRecType == Label.CLOCT15CFW23){
      isAppView = True;    
    }
    else if(ParaRecType == Label.CLOCT15CFW26){
      isInfView = True;    
    }
    else if(ParaRecType == Label.CLOCT15CFW27){
      isserdepview = True;    
    }
    

   }
   
   public pagereference EditPage()
   {
    pagereference pg;
    if(ParaRecType != Label.CLOCT15CFW23)
     pg = new pagereference('/' + CFWrecord.id + '/e?RecordType=' + ParaRecType + '&retURL=' + CFWrecord.id);
    else
     pg = new pagereference('/apex/VFP_CertificationApplicationView?id=' + CFWrecord.id);
    pg.setredirect(true);
    return pg;
    }
    
    public pagereference SubmitRisk()
   {
   
    string RecType;
    
    if(ParaRecType != null)
      RecType = ParaRecType;
    else
     RecType = Label.CLOCT15CFW27;
     
    pagereference pg = new pagereference('/apex/VFP_CFWDetailPage?id='+ CFWrecord.id + '&RecordType=' + RecType);
    pg.setredirect(true);
    CFWRecord.RiskAssessmentStatus__c = 'Submitted';
    CFWRecord.SubmittedforApproval__c = True;
    update CFWRecord;
    
    return pg;
    }
    
   //Save Application View Data 
   public pagereference SaveAppView()
   {
    CFWRecord.RecordTypeID = Label.CLOCT15CFW23;
    stdcontroller.save();
    pagereference pg;  
    pg = new pagereference('/apex/VFP_CFWDetailPage?id=' + CFWrecord.id + '&RecordType=' + Label.CLOCT15CFW23);
    pg.setredirect(true);
    return pg;
   
    }
    
    // Cancel Page Button
    
    public pagereference CancelPage(){
    pagereference pg;  
    pg = new pagereference('/apex/VFP_CFWDetailPage?id=' + CFWrecord.id + '&RecordType=' + Label.CLOCT15CFW23);
    pg.setredirect(true);
    return pg;
    }
     
  }