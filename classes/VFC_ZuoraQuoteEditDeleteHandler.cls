public class VFC_ZuoraQuoteEditDeleteHandler{
     public String quoteId;
     public List<zqu__Quote__c> zQuote = new List<zqu__Quote__c>(); 
     
     public VFC_ZuoraQuoteEditDeleteHandler(ApexPages.StandardController controller) {
         quoteId = ApexPages.currentPage().getParameters().get('id');
         zQuote = [SELECT Id, Name, zqu__Status__c, zqu__Account__c, zqu__SubscriptionType__c, IsLocked, ApprovalStatus__c FROM zqu__Quote__c WHERE Id = :quoteId];
         
       
    }
    public PageReference checkEditStatus() {
        
        PageReference pageRef;
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>');
        if(zQuote.size() >0)
            for(zqu__Quote__c q : zQuote) { 
                if((q.zqu__Status__c == 'Sent to Z-Billing') || (q.zqu__Status__c == 'Closed') || (q.ApprovalStatus__c == 'Internally Approved')|| q.IsLocked) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLAPR15FIO002));
                    pageRef = null;
                }
                else if((q.zqu__Status__c == 'New') || (q.zqu__Status__c == 'Created') && !q.IsLocked) {
                    if(q.zqu__SubscriptionType__c == 'Cancel Subscription')
                        pageRef = new PageReference('/apex/zqu__ZQCancellation?Id='+quoteId);
                    else
                        pageRef = new PageReference('/apex/zqu__EditQuoteDetail?Id='+quoteId);
                }
            }
        System.debug('PageRef:'+pageRef);           
       // pageRef.setredirect(true);
        return pageRef;    
    }    
    
    public PageReference checkDeleteStatus() {
        
        PageReference pageRef;
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>');
        if(zQuote.size() >0)
            for(zqu__Quote__c q : zQuote) { 
                if((q.zqu__Status__c == 'Sent to Z-Billing') || (q.zqu__Status__c == 'Closed')|| (q.ApprovalStatus__c == 'Internally Approved') || q.IsLocked) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLAPR15FIO001));
                    pageRef = null;
                }
                else if(((q.zqu__Status__c == 'New') || (q.zqu__Status__c == 'Created')) && !q.IsLocked) {
                    delete q;
                    pageRef = new PageReference(ApexPages.currentPage().getParameters().get('retURL')); 
                }
            }
        System.debug('PageRef:'+pageRef);           
        // pageRef.setredirect(true);
        return pageRef;    
    }
    
    //Method to go back on the Account when clicking the 'OK' button if an error is raised
    public pageReference back(){
        pageReference pageRef = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        return pageRef;
    } 
}