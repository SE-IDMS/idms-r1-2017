/*

*/
@IsTest
private class VFC_CopyCampaignScript_Test {
    static testMethod void testVFC_CopyCampaignScript() 
    {    
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {    
            Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
            Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            Account Acc = Utils_TestMethods.createAccount();
            insert Acc;
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con1;
            
            AP53_Campaign ap = new AP53_Campaign();
            Campaign cm = new Campaign();
            cm.Name = 'TestCampaign '+system.now();
            cm.IsActive = true;
            cm.EndDate = system.today() + 10;
            cm.Global__c = true;
            cm.ProductFocus__c = 'All Products';
            cm.BUFocus__c = 'All Business Units';
            cm.CampaignLeader__c = UserInfo.getUserId();
            cm.Script__c = 'test script';
            
            insert cm;
             
            List<Lead> leads = new List<Lead>();
            for(Integer i=0;i<20;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead') != null)
                    lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                    
                lead1.Contact__C = con1.id; 
                lead1.Account__c = Acc.id;
                lead1.NonMarcommCampaign__c = cm.id;
                leads.add(lead1);   
            }
            Database.SaveResult[] leadsInsert= Database.insert(leads, true);
            
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(leads[0]);
            VFC_CopyCampaignScript CopyScritPage = new VFC_CopyCampaignScript(sc1);
            
            CopyScritPage.copyScript();
            CopyScritPage.goToLead();
            
            
            //////////////////////////
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
            user.UserPermissionsMarketingUser  = true;
                   
            system.runas(user)
            {
                
            }   
       }
    }
    static testMethod void testVFC_CopyCampaignScript2() 
    {    
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {    
            Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
            Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            Account Acc = Utils_TestMethods.createAccount();
            insert Acc;
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con1;
            
            AP53_Campaign ap = new AP53_Campaign();
            Campaign cm = new Campaign();
            cm.Name = 'TestCampaign '+system.now();
            cm.IsActive = true;
            cm.EndDate = system.today() + 10;
            cm.Global__c = true;
            cm.ProductFocus__c = 'All Products';
            cm.BUFocus__c = 'All Business Units';
            cm.CampaignLeader__c = UserInfo.getUserId();
            cm.Script__c = 'Test Script';
            
            insert cm;
            
            List<Lead> leads = new List<Lead>();
            for(Integer i=0;i<10;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead') != null)
                    lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                    
                lead1.Contact__C = con1.id; 
                lead1.Account__c = Acc.id;
                
                
                leads.add(lead1);   
            }
            Database.SaveResult[] leadsInsert= Database.insert(leads, true);
            Database.SaveResult[] leadsupdate;
            Database.SaveResult[] leadsupdate4;
            
            leads[1].NonMarcommCampaign__c = cm.id;
            // leads[1].NonMarcommCampaign__r.Script__c = null;
            leads[1].Status = 'New';
            leads[1].QualificationInfo__c = cm.Script__c;
            update leads[1];
            leadsupdate = Database.update(leads, false);
            
            leads[2].NonMarcommCampaign__c = cm.id;
            leads[2].Status = 'New';
            leads[2].QualificationInfo__c = leads[2].QualificationInfo__c + 'xyz';
            update leads[2];
            leadsupdate4 = Database.update(leads, false);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(leads[1]);
            VFC_CopyCampaignScript CopyScritPage1 = new VFC_CopyCampaignScript(sc1);
            
            CopyScritPage1.copyScript();
            CopyScritPage1.goToLead();
            
            ApexPages.StandardController sc4 = new ApexPages.StandardController(leads[2]);
            VFC_CopyCampaignScript CopyScritPage4 = new VFC_CopyCampaignScript(sc4);
            
            CopyScritPage4.copyScript();
            CopyScritPage4.goToLead();
            
            //////////////////////////
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
            user.UserPermissionsMarketingUser  = true;
                   
            system.runas(user)
            {
                
            }   
       }
    }
    
    static testMethod void testVFC_CopyCampaignScript3() 
    {    
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {    
            Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
            Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            Account Acc = Utils_TestMethods.createAccount();
            insert Acc;
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con1;
            
            AP53_Campaign ap = new AP53_Campaign();
            Campaign cm = new Campaign();
            cm.Name = 'TestCampaign '+system.now();
            cm.IsActive = true;
            cm.EndDate = system.today() + 10;
            cm.Global__c = true;
            cm.ProductFocus__c = 'All Products';
            cm.BUFocus__c = 'All Business Units';
            cm.CampaignLeader__c = UserInfo.getUserId();
            cm.Script__c = 'Test Script';
            
            insert cm;
            
            List<Lead> leads = new List<Lead>();
            for(Integer i=0;i<10;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead') != null)
                    lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                    
                lead1.Contact__C = con1.id; 
                lead1.Account__c = Acc.id;
                
                
                leads.add(lead1);   
            }
            Database.SaveResult[] leadsInsert= Database.insert(leads, true);
            Database.SaveResult[] leadsupdate1;
            
            leads[5].NonMarcommCampaign__c = null;
            
            update leads[5];
            leadsupdate1 = Database.update(leads, false);
            
            ApexPages.StandardController sc5 = new ApexPages.StandardController(leads[5]);
            VFC_CopyCampaignScript CopyScritPage5 = new VFC_CopyCampaignScript(sc5);
            
            CopyScritPage5.copyScript();
            CopyScritPage5.goToLead();
            
            
            //////////////////////////
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
            user.UserPermissionsMarketingUser  = true;
                   
            system.runas(user)
            {
                
            }   
       }
    }
    
    static testMethod void testVFC_CopyCampaignScript4() 
    {    
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {    
            Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
            Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
            
            Country__c country= Utils_TestMethods.createCountry();
            insert country;
            Account Acc = Utils_TestMethods.createAccount();
            insert Acc;
            MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
            insert markCallBack;
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=Acc.id, email = 'textt@test.com', Country__c = country.id);
            insert con1;
            
            AP53_Campaign ap = new AP53_Campaign();
            Campaign cm = new Campaign();
            cm.Name = 'TestCampaign '+system.now();
            cm.IsActive = true;
            cm.EndDate = system.today() + 10;
            cm.Global__c = true;
            cm.ProductFocus__c = 'All Products';
            cm.BUFocus__c = 'All Business Units';
            cm.CampaignLeader__c = UserInfo.getUserId();
            cm.Script__c = null;
            
            insert cm;
            
            List<Lead> leads = new List<Lead>();
            for(Integer i=0;i<10;i++)
            {
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                if(mapRecordType.get('Marcom Lead') != null)
                    lead1.RecordTypeId = mapRecordType.get('Marcom Lead').getRecordTypeId();
                    
                lead1.Contact__C = con1.id; 
                lead1.Account__c = Acc.id;
                
                
                leads.add(lead1);   
            }
            Database.SaveResult[] leadsInsert= Database.insert(leads, true);
           
           Database.SaveResult[] leadsupdate2;
            
            leads[2].NonMarcommCampaign__c = cm.id;
            //leads[2].Status = '3-Closed-Opportunity-Created';
            update leads[2];
            leadsupdate2 = Database.update(leads, false);
            
            ApexPages.StandardController sc2 = new ApexPages.StandardController(leads[2]);
            VFC_CopyCampaignScript CopyScritPage2 = new VFC_CopyCampaignScript(sc2);
            
            CopyScritPage2.copyScript();
            CopyScritPage2.goToLead();
           
            //////////////////////////
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
            user.UserPermissionsMarketingUser  = true;
                   
            system.runas(user)
            {
                
            }   
       }
    }
    
}