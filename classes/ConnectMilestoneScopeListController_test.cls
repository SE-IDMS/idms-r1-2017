@isTest
private class ConnectMilestoneScopeListController_test{
    static testMethod void myTest() {
    
       User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connect5');
        System.runAs(runAsUser){
            
        List<Connect_Milestones__c> LstConMil= new List<Connect_Milestones__c>();
        Initiatives__c inti=Utils_TestMethods_Connect.createInitiative();
            insert inti;
        Project_NCP__c globalProgram = new Project_NCP__c(Program__c=Inti.Id,name='Test',Global_Business__c='Power;GB;Partner-Division;'+Label.Connect_Smart_Cities,Global_Functions__c='Test;GF',Power_Region__c='EMEAS;PR',Smart_Cities_Division__c='test',Partner_Region__c='test');
        Insert globalProgram;
        Connect_Milestones__c ConMil = new Connect_Milestones__c(Program_Number__c=globalProgram.id,Global_Business__c='Power;Partner-Division;'+Label.Connect_Smart_Cities,Milestone_Name__c='Test',Global_Functions__c='Test',Smart_Cities_Division__c='test',Power_Region__c='EMEAS',Partner_Region__c='test');  
        LstConMil.add(ConMil);
        Insert LstConMil;
        Champions__c Champ1=new Champions__c(Champion_Name__c=Label.Connect_Email,Scope__c='Global Business',Entity__c='Power');
        insert champ1;
        Team_Members__c Teammembers1=new Team_Members__c(Program__c=globalProgram.id,Team_Name__c='Global Business', Entity__c='Power',Team_Member__c=Label.Connect_Email);
        insert Teammembers1;
        Team_Members__c Teammembers4=new Team_Members__c(Program__c=globalProgram.id,Team_Name__c='Global Business', Entity__c='Partner-Division',Partner_Region__c='test',Team_Member__c=Label.Connect_Email);
        insert Teammembers4;
        Team_Members__c Teammembers14=new Team_Members__c(Program__c=globalProgram.id,Team_Name__c='Global Business', Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test',Team_Member__c=Label.Connect_Email);
        insert Teammembers14;
        system.debug(';;;;DL-----'+Teammembers1.Deployment_Leaders__c);
        Team_Members__c Teammembers2=new Team_Members__c(Program__c=globalProgram.id,Team_Name__c='Global Functions', Entity__c='Test',Team_Member__c=Label.Connect_Email);
        insert Teammembers2;
        Team_Members__c Teammembers3=new Team_Members__c(Program__c=globalProgram.id,Team_Name__c='Operation Region', Entity__c='PR',Team_Member__c=Label.Connect_Email);
        insert Teammembers3; 
                
        //Use the PageReference Apex class to instantiate a pageConMil 
       PageReference pageRef = Page.Connect_Milestone_Network;
       
       //In this case, the Visualforce page named 'Connect_Milestone_Network' is the starting point of this Test method. 
       Test.setCurrentPage(pageRef);
     
       //Instantiate and construct the controller class.   
       ApexPages.StandardController stanPage= new ApexPages.StandardController(ConMil );
       ConnectMilestoneScopeListController controller = new ConnectMilestoneScopeListController(stanPage);

       //Add parameters to page URL
       ApexPages.currentPage().getParameters().put('Id', ConMil.id); 
       
       //Call the controller method with the newly created connnect milestone.
       controller.getMilestoneNetwork(); 
       
       //Update the Global Program to provide for alternate conditions.
       globalProgram.Global_Functions__c='GSC-Regional;Test;GF';
       globalProgram.GSC_Regions__c='Test';
       ConMil.Global_Functions__c='GSC-Regional;Test';
       ConMil.GSC_Regions__c='Test';
       ConMil.Power_Region__c='EMEAS;PR';
       Teammembers2.Entity__c='GSC-Regional';
       Teammembers2.GSC_Region__c='Test';
       update globalProgram;
       
       update ConMil;
         
       update Teammembers2;
    
      
       //Calls to Methods with the updated Connect Milestone.
       controller.getMilestoneNetwork(); 
       controller.getprogram();
       controller.getPageid();
       }
       }
}