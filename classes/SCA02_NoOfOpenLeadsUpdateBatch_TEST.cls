/*     Author: Pooja Gupta
       Date: 2-May-13
       Description: Test Class for SCA02_NoOfOpenLeadsUpdateBatch
       Release: May 2013
*/

@isTest
private class SCA02_NoOfOpenLeadsUpdateBatch_TEST
{
    static testMethod void myUnitTest()
    { 
        Test.StartTest();
        SCA02_NoOfOpenLeadsUpdateBatch sh1 = new SCA02_NoOfOpenLeadsUpdateBatch ();
        string sche='0 0 23 * * ?';
        system.schedule('test',sche,sh1); 
        Test.stopTest();
    }
}