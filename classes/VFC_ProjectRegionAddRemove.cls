/****************
To Select:Zone and Country

******************/



Public Class VFC_ProjectRegionAddRemove{
    

    
    public Milestone1_Project__c MileProj{get;set;}
    public List<WrapperProjectCountry>  listWrapperCntry{get;set;}
    public string strRecordType;
    public string strOfferid;
    
    public string strRegn{get;set;}
    public string strZone{get;set;}
    public boolean IsEmpty{get;set;}
    public boolean IsCountrySel{get;set;}
     public boolean blnIsAccs{get;set;}
    public VFC_ProjectRegionAddRemove(ApexPages.StandardController controller) {
        MileProj=new Milestone1_Project__c();
        set<Id> userAccSet = new set<Id>(); 
        blnIsAccs=false;
        strOfferid = ApexPages.currentPage().getParameters().get('Id');
        id profileId = [Select Id,Name from Profile where name = 'System Administrator'].id;
        
        List<Milestone1_Project__c> listMP=[select id,name,ownerid,Offer_Launch__c,Region_Introduction_Owner__c ,SegmentManager__c,ForecastValidator__c,Country_Withdrawal_Leader__c,RegionWithdrawalManager__c,Country_Launch_Leader__c,Marketing_Leader__c,TECH_Offeruser__c,Country_Announcement_Date__c,Region__c,Zone__c from Milestone1_Project__c where id=:strOfferid];
        
        strRecordType =ApexPages.currentPage().getParameters().get('RecordType');
        MileProj=listMP[0];
        userAccSet = new set<Id>{MileProj.ownerid,MileProj.Country_Launch_Leader__c,MileProj.Region_Introduction_Owner__c,MileProj.Marketing_Leader__c,MileProj.Country_Withdrawal_Leader__c,MileProj.RegionWithdrawalManager__c,MileProj.SegmentManager__c,MileProj.ForecastValidator__c};
        
        if(MileProj.TECH_Offeruser__c!=null) {
            list<string> stroff=MileProj.TECH_Offeruser__c.split('\\,');
             for(string strUr:stroff) {
                userAccSet.add(strUr); 
                                
            }
            
            
        }
        
        if(!userAccSet.contains(Userinfo.getuserid()) && userinfo.getProfileid()!=profileId) {
            blnIsAccs=true;
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLMAR16ELLAbFO11));//'You dont have access to create Project'
        }
        
        IsEmpty=true;
        IsCountrySel=false;
        projectSelectedCountry(MileProj.id);
        
        

    }   
  
    //Constructor  
    public VFC_ProjectRegionAddRemove() {
    
        listWrapperCntry= new List<WrapperProjectCountry>(); 
        MileProj=new Milestone1_Project__c();
        strOfferid = ApexPages.currentPage().getParameters().get('urlOfferId');
        strRecordType =ApexPages.currentPage().getParameters().get('RecordType');
        MileProj.Offer_Launch__c=strOfferid;
        IsEmpty=true;
        IsCountrySel=false;     
    }   
    
    public PageReference pgRefSaveProject() {
        List<WrapperProjectCountry>  ListPrtCoutry = new List<WrapperProjectCountry>();
        list<ProjectZoneCountry__c>   listPrZone= new list<ProjectZoneCountry__c>();
        ProjectZoneCountry__c NewPrZoneCtry= new ProjectZoneCountry__c();
        
        IsCountrySel=false;
        boolean IsSelectedContry=false;
         boolean blnIsError=false;
        Set<string>  DeletSetCountryid =new Set<string>();
        
        if(listWrapperCntry.size() > 0) {
            for(WrapperProjectCountry WrpObj:listWrapperCntry) {
                //IsSelectedContry=false;   
                if(WrpObj.blnIsSelect && !WrpObj.isToSelect ) {
                        ListPrtCoutry.add(WrpObj);
                       //IsCountrySel=true;
                    
                }else if(!WrpObj.blnIsSelect && WrpObj.isToSelect) {
                    
                        DeletSetCountryid.add(WrpObj.Coutry.id);
                }
                if(WrpObj.blnIsSelect) {
                        IsSelectedContry=true;
                }

            }   
        }
        if(!IsSelectedContry) {
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,label.CLMAR16ELLAbFO08 ));//'Please Select a country'
                blnIsError=true;    
                
        }
        
        if(ListPrtCoutry.size() > 0)  {
            for(WrapperProjectCountry validObj:ListPrtCoutry)  {
                IsCountrySel=false;
                if(validObj.blnIsSelect) {
                    IsCountrySel=true;
                }   
                
            }
            
            
            
            if(ListPrtCoutry.size() > 0) {
                for(WrapperProjectCountry validObj:ListPrtCoutry) {
                    NewPrZoneCtry= new ProjectZoneCountry__c();
                    NewPrZoneCtry.Project__c=MileProj.id;
                    NewPrZoneCtry.Country__c=validObj.Coutry.id;
                    listPrZone.add(NewPrZoneCtry);
                    
                
                }
                
            }
        }
        if(listPrZone.size() > 0) {
            Database.SaveResult[] saveList = Database.insert(listPrZone,false); 

        }
        
        if(DeletSetCountryid.size() > 0 && IsSelectedContry) {
            
            List <ProjectZoneCountry__c > lstCouryDelete = [SELECT id,Project__c,Country__c FROM ProjectZoneCountry__c where Country__c=:DeletSetCountryid and Project__c=:MileProj.id];
            if(lstCouryDelete.size() > 0) {
                
                delete lstCouryDelete;
                
            }    
            
        }
             // project
        if(!blnIsError) {
            PageReference NewProjectEditPage = new PageReference('/'+MileProj.id);

            NewProjectEditPage.setredirect(true);  
        
        return NewProjectEditPage;
        }else {
                return null;
            
        }    
        
        
        
    } 
    
    public void listRegionCountry() {
     system.debug(strRegn);
     system.debug(strZone);
        IsEmpty=false;
        listWrapperCntry= new List<WrapperProjectCountry>(); 
        if(strRegn!=null && strRegn!='' && strZone!=null && strZone!='' ) {
        
            List <Country__c > lstCoury = [SELECT id, Name,Region__c,SubRegion__c  FROM Country__c where Region__c=:strRegn and SubRegion__c=:strZone ]; 
            system.debug('List%%%%%%%%'+lstCoury );
            if(lstCoury.size() > 0) {               
            
                for(Country__c CotyObj:lstCoury) {              
                    listWrapperCntry.add(new WrapperProjectCountry(CotyObj,false) );
                }
            }
            
        }
        if(listWrapperCntry.size() >0) {
            IsEmpty=true;
        }
        
    }
    
    
    
    public void projectSelectedCountry(string OffProjectId) {
        
        List<WrapperProjectCountry> listWrapperCntry1= new List<WrapperProjectCountry>(); 
        listWrapperCntry= new List<WrapperProjectCountry>(); 
        if(OffProjectId!=null && OffProjectId!='') {            
        
            Set<String>   SetCountryId = new Set<String>();

            List <ProjectZoneCountry__c > lstCoury = [SELECT id,Project__c,Country__c FROM ProjectZoneCountry__c where Project__c=:OffProjectId]; 

            for(ProjectZoneCountry__c  CotyObj:lstCoury) {

                SetCountryId.add(CotyObj.Country__c);  

            }
               
            List <Country__c > lstCoury1 = [SELECT id, Name,Region__c,SubRegion__c  FROM Country__c where id In:SetCountryId];
            system.debug('List%%%%%%%%'+lstCoury1 );
            if(lstCoury.size() > 0) {               

                for(Country__c  CotyObj:lstCoury1) {              
                    listWrapperCntry1.add(new WrapperProjectCountry(CotyObj,true) );
                }
            }
            List <Country__c > lstCoury2 = new List <Country__c >(); 
            if(SetCountryId.size() > 0) {


                lstCoury2 = [SELECT id, Name,Region__c,SubRegion__c  FROM Country__c where Region__c=:MileProj.region__c and SubRegion__c=:MileProj.Zone__c  and id!=:SetCountryId]; 
                system.debug('List%%%%%%%%'+lstCoury );

            }
            else {
                lstCoury2 = [SELECT id, Name,Region__c,SubRegion__c  FROM Country__c where Region__c=:MileProj.region__c and SubRegion__c=:MileProj.Zone__c]; 
                system.debug('List%%%%%%%%'+lstCoury );

            }
            if(lstCoury2.size() > 0) {               

                for(Country__c CotyObj:lstCoury2) {              
                    listWrapperCntry1.add(new WrapperProjectCountry(CotyObj,false) );
                }
            }
        
        }
        
        if(listWrapperCntry1.size() > 0) {
            for(WrapperProjectCountry wrapObj:listWrapperCntry1) {
                
                if(wrapObj.isToSelect) {
                    
                    wrapObj.blnIsSelect=true;   
                }
                listWrapperCntry.add(wrapObj);
                
            }
        }
        
        
    }    
    
    public class WrapperProjectCountry{    
        public Boolean blnIsSelect{get;set;}
        public Boolean isToSelect{get;set;}
        public Country__c Coutry{get;set;}
        
        public WrapperProjectCountry(Country__c prtCountry,boolean boolnToSelect) {
            blnIsSelect=false;
            isToSelect=boolnToSelect;
            Coutry=prtCountry;

        }
    }
    
   

}