/**
    Author          : Nitin Khunal
    Date Created    : 20/07/2016
    Description     : Test Class for WS_PAPortletUserManagement web service class.
*/
@isTest
public class WS_PAPortletUserManagement_TEST {
    @testSetup static void testData() {
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.City__c = 'Bangalore';
        testAccount.SEAccountID__c = '12345678';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        insert testcontact;
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        CTR_ValueChainPlayers__c testCVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        testCVCP.legacypin__c ='1234';
        testCVCP.Contact__c = testcontact.Id;
        insert testCVCP;
    }

    @isTest static void testMethod1() {
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId='12345'; 
        uInfo.strAccountGoldenId='1234567';
        uInfo.strCustomerFirstId='';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInv';
        WS_PAPortletUserManagement.createUser(uInfo);
        uInfo.strAccountGoldenId='12345678'; 
        system.runAs(new user(id = userinfo.getUserId())) {
            WS_PAPortletUserManagement.SiteResult userresult = WS_PAPortletUserManagement.createUser(uInfo);
            system.assertEquals('0', userresult.ErrorCode);
        }

        WS_PAPortletUserManagement.SiteResult userresult1 = WS_PAPortletUserManagement.deactivateUser('test');
        system.assertEquals('1', userresult1.ErrorCode);
        
        WS_PAPortletUserManagement.SiteResult userresult5 = WS_PAPortletUserManagement.activateUser('test');
        system.assertEquals('1', userresult5.ErrorCode);
        
        system.runAs(new User(id = userinfo.getUserId())){
            WS_PAPortletUserManagement.SiteResult userresult2 = WS_PAPortletUserManagement.deactivateUser('testInv');
            system.assertEquals('0', userresult2.ErrorCode);

            WS_PAPortletUserManagement.ManualSharingInformation manualShareInfo = new WS_PAPortletUserManagement.ManualSharingInformation();
            manualShareInfo.strAccountGoldenId = '12345678';
            manualShareInfo.strSharingAccessLevel = 'Edit';
            manualShareInfo.strWebUserName = 'testInv';

            WS_PAPortletUserManagement.SiteResult userresult3 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo);
            system.assertEquals('3', userresult3.ErrorCode);
            
            WS_PAPortletUserManagement.SiteResult userresult4 = WS_PAPortletUserManagement.activateUser('testInv');
            system.assertEquals('0', userresult4.ErrorCode);
            
            WS_PAPortletUserManagement.SiteResult userresult6 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo);
            system.assertEquals('0', userresult6.ErrorCode);
        }
        
        String userNames = 'testInv';
        WS_PAPortletUserManagement.SiteResult userresult7 = WS_PAPortletUserManagement.removeManualSharing(userNames);
        system.assertEquals('0', userresult7.ErrorCode);
        
        
        userNames = 'testInv1,testInv2';
        WS_PAPortletUserManagement.SiteResult userresult8 = WS_PAPortletUserManagement.removeManualSharing(userNames);
        system.assertEquals('1', userresult8.ErrorCode);
        
        WS_PAPortletUserManagement.ManualSharingInformation manualShareInfo1 = new WS_PAPortletUserManagement.ManualSharingInformation();
        manualShareInfo1.strAccountGoldenId = '1234567';
        manualShareInfo1.strSharingAccessLevel = 'Edit';
        manualShareInfo1.strWebUserName = 'testInvv';

        WS_PAPortletUserManagement.SiteResult userresult4 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo1);
        system.assertEquals('1', userresult4.ErrorCode);
    }
    
    @isTest static void testMethod2() {
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId=''; 
        uInfo.strAccountGoldenId='12345678';
        uInfo.strCustomerFirstId='123';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInvvv';
        WS_PAPortletUserManagement.createUser(uInfo);
        uInfo.strCustomerFirstId='1234'; 
        system.runAs(new user(Id = userinfo.getUserId())) {
            WS_PAPortletUserManagement.SiteResult userresult = WS_PAPortletUserManagement.createUser(uInfo);
            system.assertEquals('0', userresult.ErrorCode);
        }

        WS_PAPortletUserManagement.ManualSharingInformation manualShareInfo = new WS_PAPortletUserManagement.ManualSharingInformation();
        manualShareInfo.strAccountGoldenId = '12345678';
        manualShareInfo.strSharingAccessLevel = 'Edit';
        manualShareInfo.strWebUserName = 'testInvvv';

        WS_PAPortletUserManagement.SiteResult userresult1 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo);
        system.assertEquals('0', userresult1.ErrorCode);  

        manualShareInfo.strSharingAccessLevel = 'Edit';

        WS_PAPortletUserManagement.SiteResult userresult11 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo);
        system.assertEquals('0', userresult11.ErrorCode);

        WS_PAPortletUserManagement.ManualSharingInformation manualShareInfo1 = new WS_PAPortletUserManagement.ManualSharingInformation();
        manualShareInfo1.strAccountGoldenId = '1234567';
        manualShareInfo1.strSharingAccessLevel = 'Edit';
        manualShareInfo1.strWebUserName = 'testInvvv';

        WS_PAPortletUserManagement.SiteResult userresult2 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo1);
        system.assertEquals('4', userresult2.ErrorCode);

        WS_PAPortletUserManagement.ManualSharingInformation manualShareInfo2 = new WS_PAPortletUserManagement.ManualSharingInformation();
        manualShareInfo2.strAccountGoldenId = '12345678';
        manualShareInfo2.strSharingAccessLevel = 'Readd';
        manualShareInfo2.strWebUserName = 'testInvvv';

        WS_PAPortletUserManagement.SiteResult userresult3 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo2);
        system.assertEquals('4', userresult3.ErrorCode);

        manualShareInfo2.strSharingAccessLevel = 'Read';

        WS_PAPortletUserManagement.SiteResult userresult33 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo2);
        system.assertEquals('0', userresult33.ErrorCode);
        
        manualShareInfo2.strSharingAccessLevel = 'Delete';
        
        WS_PAPortletUserManagement.SiteResult userresult34 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo2);
        system.assertEquals('0', userresult34.ErrorCode);
        
        WS_PAPortletUserManagement.SiteResult userresult35 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo2);
        system.assertEquals('4', userresult35.ErrorCode);
        
        manualShareInfo2.strSharingAccessLevel = '';
        
        WS_PAPortletUserManagement.SiteResult userresult36 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo2);
        system.assertEquals('4', userresult36.ErrorCode);

        WS_PAPortletUserManagement.SiteResult userresult4 = WS_PAPortletUserManagement.createUser(uInfo);
        system.assertEquals('4', userresult4.ErrorCode);
    }
    
    @isTest static void testMethod3() {
        Country__c testCountry = [select id from Country__c limit 1];
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.Name = 'TestAccount2';
        testAccount.City__c = 'KR';
        testAccount.SEAccountID__c = '123456789';
        insert testAccount;
        
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId='12345'; 
        uInfo.strAccountGoldenId='123456789';
        uInfo.strCustomerFirstId='';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInv'; 
        WS_PAPortletUserManagement.SiteResult userresult = WS_PAPortletUserManagement.createUser(uInfo);
        system.assertEquals('5', userresult.ErrorCode);
        
        testAccount.SEAccountID__c = '12345678';
        update testAccount;
        
        WS_PAPortletUserManagement.UserInformation uInfo1 = new WS_PAPortletUserManagement.UserInformation();
        uInfo1.strContactGoldenId='12345'; 
        uInfo1.strAccountGoldenId='12345678';
        uInfo1.strCustomerFirstId='';
        uInfo1.strWebUserId='';
        uInfo1.strWebUserName='testInv'; 
        WS_PAPortletUserManagement.SiteResult userresult1 = WS_PAPortletUserManagement.createUser(uInfo1);
        system.assertEquals('3', userresult1.ErrorCode);
    }
    
    @isTest static void testMethod4() {
        Contract testContract = [select id from contract limit 1];
        CTR_ValueChainPlayers__c  CVCPObj =Utils_TestMethods.createCntrctValuChnPlyr(testContract.id);
        CVCPObj.MasterPIN__c = true;
        CVCPObj.legacypin__c='1234';
        insert CVCPObj;
        
        
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId='12345'; 
        uInfo.strAccountGoldenId='12345678';
        uInfo.strCustomerFirstId='1234';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInv'; 
        WS_PAPortletUserManagement.SiteResult userresult = WS_PAPortletUserManagement.createUser(uInfo);
        system.assertEquals('2', userresult.ErrorCode);
    }
    
    @isTest static void testMethod5() {
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId='12345'; 
        uInfo.strAccountGoldenId='12345678'; 
        uInfo.strCustomerFirstId='';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInv';
        WS_PAPortletUserManagement.SiteResult userresult;
        system.runAs(new user(id = userinfo.getUserId())) {
            userresult = WS_PAPortletUserManagement.createUser(uInfo);
            system.assertEquals('0', userresult.ErrorCode);
        }
        WS_PAPortletUserManagement.addPermissionSetandGroup(userresult.UserId);
    }
    
    @isTest static void testMethod6() {
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId='12345'; 
        uInfo.strAccountGoldenId='12345678'; 
        uInfo.strCustomerFirstId='';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInv';
        WS_PAPortletUserManagement.SiteResult userresult;
        system.runAs(new user(id = userinfo.getUserId())) {
            userresult = WS_PAPortletUserManagement.createUser(uInfo);
            system.assertEquals('0', userresult.ErrorCode);
        }
        List<Account> lstAccount = [select id from Account where SEAccountID__c =: '12345678' limit 1];
        WS_PAPortletUserManagement.addManualSharing(userresult.UserId, lstAccount[0].id);
    }
    
}