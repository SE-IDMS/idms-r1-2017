/*
    Author          : Bhuvana S
    Description     : Test class for AP_ProductLineAfterInsertHandler
*/
@isTest
private class AP_ProductLineAfterInsertHandler_TEST 
{
    static testMethod void testProdLine()
    {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2;

        //Inserts the Product Lines        
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            prodLines.add(pl1);
        }
        Database.SaveResult[] lsr = Database.Insert(prodLines);        

        //Checks for the Parent Line Error
        OPP_ProductLine__c pl2 = Utils_TestMethods.createProductLine(opp2.Id);
        pl2.Quantity__c = 10;
        pl2.ParentLine__c = lsr[0].getId();
        insert pl2;
        if(lsr!=null && (!(lsr.isEmpty())) && lsr[0]!=null && lsr[0].getId()!=null)
        {
            OPP_ProductLine__c pl3 = [select Id,ParentLine__c from OPP_ProductLine__c where Id=: lsr[0].getId()];
            pl3.ParentLine__c = lsr[1].getId();
            try
            {
                update pl3;
            }
            catch(Exception e)
            {
                
            }
        }
    }
    
    static testMethod void testProdLine1()
    {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();
        List<CS_ProductLineBU__c> plBus= new List<CS_ProductLineBU__c>();
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        //Inserts the Custom Setting Records
        CS_ProductLineBU__c plBU = new CS_ProductLineBU__c();
        plBU.Name =      'BUILDING MANAGEMENT';
        plBU.ProductBU__c = 'BD';
        plBus.add(plBU);

        CS_ProductLineBU__c plBU1 = new CS_ProductLineBU__c();
        plBU1.Name =      'INFOR TECHNO. BUSINESS';
        plBU1.ProductBU__c = 'IT';
        plBus.add(plBU1);

        insert plBUs;
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2;
        //Inserts Opportunity Lines
        OPP_ProductLine__c pl2 = Utils_TestMethods.createProductLine(opp2.Id);
        pl2.Quantity__c = 10;
        pl2.ProductBU__c = 'BUILDING MANAGEMENT';
        pl2.ProductLine__c = 'BDBMS - BUILDING SYSTEM LEVEL2';
        prodLines.add(pl2);
        
        OPP_ProductLine__c pl3 = Utils_TestMethods.createProductLine(opp2.Id);
        pl3.Quantity__c = 10;
        pl3.ProductBU__c = 'INFOR TECHNO. BUSINESS';
        pl3.ProductLine__c = 'ITSL3 - ITB LEVEL 3 SOLUTIONS';
        prodLines.add(pl3);
        
        OPP_ProductLine__c pl5 = Utils_TestMethods.createProductLine(opp2.Id);
        pl5.Quantity__c = 10;
        pl5.ProductBU__c = 'INFOR TECHNO. BUSINESS';
        pl5.ProductLine__c = 'ITSL2 - ITB Enterp Syst Level2';
        prodLines.add(pl5);
        
        Database.insert(prodLines);

        OPP_ProductLine__c pl4 = Utils_TestMethods.createProductLine(opp2.Id);
        pl4.Quantity__c = 10;
        pl4.ProductBU__c = 'BUILDING MANAGEMENT';
        pl4.ProductLine__c = 'BDSL3 - BUILDING SYSTEM LEVEL3';
        insert pl4;
    
    }
}