@isTest
private class VCC07_OrderConnectorController_TEST
{ 
    static TestMethod void OrderConnectorTest() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Country__c objCountry = Utils_TestMethods.createCountry();
        objCountry.Name='FranCountry1';
        objCountry.CountryCode__c='TKU';
        insert objCountry;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        contact1.Country__c= objCountry.Id;
        insert contact1;
        
        Contact contact2 = Utils_TestMethods.createContact(account1.Id,'TestContact2');
        insert contact2;
        
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        case1.CommercialReference__c= 'Commercial\\sReference\\stest';
        case1.ProductBU__c = 'Business';
        case1.ProductLine__c = 'ProductLine__c';
        case1.ProductFamily__c ='family';
        insert case1;    
        
        Opportunity opp1 = Utils_TestMethods.createOpportunity (account1.Id);
        insert opp1;
        OPP_OrderLink__c OrdLk =  Utils_TestMethods.createOrderLink(opp1.Id);
        insert OrdLk; 
        
        /*
        //Profile profile = [select id from profile where name='SE - Customer Care Agent Level 1']; 
        // Modified this below line by GD Team ,CLCCCMAR120001 holds "SE - Customer Care Agent - Primary"  as value.    
        Profile profile = [select id from profile where name like: Label.CLCCCMAR120001];        
        User user = new User(alias = 'user', email='user' + '@accenture.com', emailencodingkey='UTF-8', 
        lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = profile.Id, 
        BypassWF__c = false, timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');                   
        */
        
        VCC07_OrderConnectorController OrderController = new VCC07_OrderConnectorController();
        OrderController.OrderDataSource.test = true;
        OrderController.OrderListController = new VCC06_DisplaySearchResults();
        OrderController.OrderListController.Key = 'DisplayOrders';
        OrderController.OrderListController.PageController = OrderController;
        
        OrderController.deliveryListController = new VCC06_DisplaySearchResults();
        OrderController.deliveryListController.Key = 'DeliveryDetails';
        OrderController.deliveryListController.PageController = OrderController;

        OrderController.orderNumber='';
        OrderController.PONumber='';
        OrderController.DisplayAdvancedCriteria = false;
        
        OrderController.StandardObjectID = Case1.ID;
        OrderController.SearchOrder();
        
        OrderController.ToggleAdvancedCriteria();
        OrderController.SearchOrder();        
         // Added by Hari Krishna : GD       
        OrderController.DateCapturer.Account__c=account1.id;
        OrderController.BackOfficeRefresher();
        OrderController.DateRefresher();
        OrderController.DisplayDeliveryDetails();        
    }
}