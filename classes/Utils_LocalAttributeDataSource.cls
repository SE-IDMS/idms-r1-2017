/*
    Author          : Accenture Team
    Date Created    : 31/10/2011
    Description     : Utility class for Local Attribute Data Source
*/
/*
Modification History
    Author          : Siddharatha Nagavarapu (GD)
    Date Modified   : 17/02/2012
    Description     : added a new filter type 'attributetype' to recognize if the request is from a contact attribute or a account attribute.
*/
Public Class Utils_LocalAttributeDataSource extends Utils_DataSource
{
 //    Query Parameters - Main Query and Different cluases which will be used conditionally
        static String query1 = 'SELECT Id, Name, Category__c, (SELECT Id, Country__c, Country__r.Name FROM LocalAttributeCountries__r) FROM LocalAttribute__c';
        static String query2 = ' where ';
        static String query3 = ' AND ';
        static String query4 = 'Name like \'%';
        static String query5 = '%\'';
        static String query6 = 'Category__c =:category';
        static String query7 = 'Id IN (select LocalAttribute__c from LocalAttributeCountry__c where country__c = \'';
        static String query8 = '\')';
        static String query9 = ' ORDER BY Name';
     //    End of Query String cretaion
                          
     // Define Columns Label and displayed field  
     public override List<SelectOption> getColumns()
     {
        List<SelectOption> Columns = new List<SelectOption>();
        
        Columns.add(new SelectOption('Field1__c',sObjectType.LocalAttribute__c.fields.Name.getLabel())); // First Column
        Columns.add(new SelectOption('Field2__c',sObjectType.LocalAttribute__c.fields.Category__c.getLabel())); 
        Columns.add(new SelectOption('Field3__c',sObjectType.Country__c.getLabelPlural())); // Last Column
        return Columns;
     }
        
     //    Main Search Method
     
public override Result Search(List<String> keyWords, List<String> filters)
     {
        //    Initializing Variables      
        String searchText; 
        String category;
        String country;
String attributeType;
        List<sObject> results = New List<sObject>();
        List<sObject> tempResults = New List<sObject>();
        Utils_Sorter sorter = new Utils_Sorter();
        List<Id> laId = New List<Id>();
        Utils_DataSource.Result returnedResult = new Utils_DataSource.Result();   
        returnedResult.recordList = new List<DataTemplate__c>();
        Map<Object, String> countries = New Map<Object, String>();
        Map<String,sObject> resultData = New Map<String,sObject>();
        Map<Object, String> pickList = New Map<Object, String>();
        Map<Integer,DataTemplate__c> resultMap = new Map<Integer,DataTemplate__c>();
        List<Object> countryId = New List<Object>();
        List<sObject> resultset = New List<sObject>();
        //    End of Variable Initialization
        
        //    Assigning the User input Search Filter and Search criteria to the Variables
        if(!(filters[0].equalsIgnoreCase(Label.CL00355)))
            country = filters[0];
        if(!(filters[1].equalsIgnoreCase(Label.CL00355)))
            category = filters[1];
        if(filters.size()>2)    
        attributeType= filters[2];

        if(keywords != null && keywords.size()>0)
            searchText = keyWords[0];
        else 
            searchText = '';
        //    End of Assignment
        
        System.debug('#### Local Attribute Search Parameters #####');
        System.debug('#### Search Text: ' + searchText);
        System.debug('#### Country: ' + country);
        System.debug('#### Category: ' + category);
                          
        //    Searching the Local Attribute Table to get all the combination the List of Local Attributes
        //    and all the Country they are associated to
         //    Creation of Map of SFDC ID of the Country and Country Name
String queryString='';
         if(searchText != NULL && searchText!= '')
        {
            if(category != NULL)
            {
                if(country != NULL)
                   queryString=query1+query2+query4+searchText+query5+query3+query6+query3+ attributeType +query3+query7+country+query8+query9;                    
                else
                   queryString=query1+query2+query4+searchText+query5+query3+query6+query3+ attributeType +query9;
            }
            else
            {
                if(country != NULL)
                   queryString=query1+query2+query4+searchText+query5+query3+ attributeType +query3+query7+country+query8+query9;
                else
                   queryString=query1+query2+query4+searchText+query5+query3+ attributeType +query9;
            }
        }
        else
        {
            if(category != NULL)
            {
                if(country != NULL)
                   queryString=query1+query2+query6+query3+ attributeType +query3+query7+country+query8+query9;
                else
                   queryString=query1+query2+query6+query3+ attributeType +query9;
            }
            else
            {
                if(country != NULL)
                   queryString=query1+query2+query7+country+query8+query3+ attributeType +query9;
                else
                   queryString=query1+query2 + attributeType +query9;
            }
        }
        System.debug('#### query: ' +  queryString ); 
        results=Database.query(queryString);
        System.debug('#### results: ' +  results );
        //    End of Search
                
        //    Creation of Map of Category Picklist Value and corresponding Label
        
        Schema.DescribeFieldResult categoryPickVal = Schema.sObjectType.LocalAttribute__c.fields.Category__c;
        for (Schema.PickListEntry PickVal : categoryPickVal.getPicklistValues())
        {
            pickList.put((Object)PickVal.getValue(),PickVal.getLabel());
        }
        //    End of Picklist Map creation 
         for(Country__c cons : [Select Id, Name from Country__c])
        {
                countries.put((Object)cons.Id, cons.Name);
        }
               
        system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'+countries);   
        //    End of Country Map creation     
       
        //    Start of processing the Search Result to prepare the final result set
        tempResults.clear();
        for (sObject obj : results)
        { 
            System.debug('############## results:');
            LocalAttribute__c records = (LocalAttribute__c)obj;
            DataTemplate__c la = New DataTemplate__c ();
             la.put('Field1__c',records.get('Name'));
            if(records.get('Category__c') != null)
             la.put('Field2__c',records.get('Category__c'));
            
           /* Modified By ACCENTURE IDC
              Modifed for Defect - DEF-0362
              Modified Date: 07/12/2011
            */
            if(records.get('Id') != null)
             la.put('Field5__c',records.get('Id'));
             //End of Modification        
            
            // Prevent a null relationship from being accessed  
            sObject[] countryOfLA = obj.getSObjects('LocalAttributeCountries__r');
            if (countryOfLA != null) 
            {
                for (sObject sCountries : countryOfLA)
                { 
                    if(la.Field3__c == null)
                    {
                        if(sCountries.get('Country__c') != null)
                        {
                            la.put('Field3__c', countries.get(sCountries.get('Country__c')));
                            la.put('Field4__c', sCountries.get('Country__c'));
                        }
                    }
                    else
                    {
                        if(sCountries.get('Country__c') != null)
                        {
                            la.put('Field3__c', la.Field3__c+'; '+countries.get(sCountries.get('Country__c')));
                            la.put('Field4__c', la.Field4__c+'; '+sCountries.get('Country__c'));
                        }
                    } 
                }
            }
            tempResults.add(la);              
        }
        System.debug('#### Results without Country Filter: ' +  tempResults);
        returnedResult.recordList.clear();
        returnedResult.recordList.addAll(tempResults);
        
        //    End of Processing and prepaation of the final result Set
        
        returnedResult.recordList = (List<sObject>)sorter.getSortedList(returnedResult.recordList, 'Field1__c', true);
        System.debug('#### Final Result :' + returnedResult);
        
        resultset.addAll(returnedResult.recordList);
        returnedResult.numberOfRecord = resultset.size();

        if(returnedResult.numberOfRecord >100)
        {
            returnedResult.recordList.clear();
            for(Integer count=0; count< 100; count++)
            {
                returnedResult.recordList.add(resultset[count]);      
            }
        }
        
        return returnedResult;
     }

}