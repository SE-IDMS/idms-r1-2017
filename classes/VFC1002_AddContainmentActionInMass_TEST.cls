@isTest
Private with sharing class VFC1002_AddContainmentActionInMass_TEST {
  
    static testMethod void VFC1002_AddContainmentActionInMass_TEST()
  {
    User runAsUser = Utils_TestMethods.createStandardUser('test');
    insert runAsUser;
    Id userId = UserInfo.getUserId();     
    String role1 = 'CS&Q Manager';
    String role2 = 'Default Action Owner';
    
    System.runAs(runAsUser) {
    
        BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                accOrg1.Name='Test';
                accOrg1.Entity__c='Test Entity-2'; 
                insert  accOrg1;
        BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                accOrg2.Name='Test';
                accOrg2.Entity__c='Test Entity-2'; 
                accOrg2.SubEntity__c='Test Sub-Entity 2';
                insert  accOrg2;
        BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                accOrg.Name='Test';
                accOrg.Entity__c='Test Entity-2';  
                accOrg.SubEntity__c='Test Sub-Entity 2';
                accOrg.Location__c='Test Location 2';
                accOrg.Location_Type__c='Design Center';
                insert  accOrg;
        
        Problem__c prob = Utils_TestMethods.createProblem(accOrg.id);
        prob.Severity__c = 'Safety Related';        
        prob.RecordTypeId = Label.CLI2PAPR120014;
        insert prob;
        
        OSACMeeting__c  OSRec=new OSACMeeting__c();
        OSRec.Start__c = Date.today();
        OSRec.Problem__c = prob.id ;        
        OSRec.Decision__c = 'Pending';
        OSRec.ObjectiveOfMeeting__c='test';
        OSRec.Participants__c='test';
        OSRec.End__c = system.today();
        Insert OSRec;
        OSRec.Decision__c='No-Go';
        OSRec.MeetingFacilitator__c='test';
        OSRec.DecisionSummary__c='test2';
        OSRec.ProblemSummary__c='test';
        OSRec.ProblemLeader__c=runAsUser.id;
        OSRec.CSQManager__c=runAsUser.id;
        OSRec.End__c = system.today();
        update OSRec;  
        
        EntityStakeholder__c es1 = Utils_TestMethods.createEntityStakeholder(accOrg.id, userId, role1);
        insert es1;
        
        EntityStakeholder__c es2 = Utils_TestMethods.createEntityStakeholder(accOrg.id, userId, role2);
        insert es2;
        
        List<ContainmentAction__c> lstXA = new List<ContainmentAction__c>();
        ContainmentAction__c testConAction1 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);
        insert testConAction1;  
        ContainmentAction__c testConAction2 = new ContainmentAction__c();
        testConAction2.Title__c = 'Test Internal';
        testConAction2.RelatedProblem__c = prob.Id;
        testConAction2.AccountableOrganization__c = accOrg.Id;
        testConAction2.Status__c = 'Not Started';
        testConAction2.DueDate__c = System.TODAY();
        testConAction2.Shipments__c = 'Hold';
        insert testConAction2;  
        
        ContainmentAction__c testConAction3 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);
        ContainmentAction__c testConAction4 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);       
        lstXA.add(testConAction3); 
        lstXA.add(testConAction4); 
        
        ApexPages.StandardController conActionController = new ApexPages.StandardController(lstXA[1]);
        PageReference pageRef = Page.VFP1002_AddContainmentActionInMass; 
        ApexPages.currentPage().getParameters().put('id', testConAction2.id);
        ApexPages.currentPage().getParameters().put('pid', prob.id);
        //ApexPages.currentPage().getParameters().put('source', 'holdsource');
        VFC_ControllerBase basecontroller = new VFC_ControllerBase();
        VFC1002_AddContainmentActionInMass addConActionInMass = new VFC1002_AddContainmentActionInMass(conActionController);
            
        addConActionInMass.LocType = 'Design Center';
        addConActionInMass.getOptions();
        addConActionInMass.searchString = 'Test Location 2';
        addConActionInMass.getQueryString();
        addConActionInMass.doSearchAccountableOrgs();
        
        addConActionInMass.blnstatus1 = true;
        addConActionInMass.APsPresent = true;
        addConActionInMass.source='holdsource';
        addConActionInMass.accountableOrg= new BusinessRiskEscalationEntity__c();
        addConActionInMass.searchLocation='';
        addConActionInMass.title= 'Test Locat';
        addConActionInMass.errorString='';
        
        
        
        VFC1002_AddContainmentActionInMass.MyWrapper mywclist1 = new VFC1002_AddContainmentActionInMass.MyWrapper(accOrg.Entity__c,accOrg.SubEntity__c,accOrg.Location__c,accOrg.Id,es1);
        
        List<VFC1002_AddContainmentActionInMass.MyWrapper> mywclist=new List<VFC1002_AddContainmentActionInMass.MyWrapper>();
        mywclist.add(mywclist1);        
        
        String commRefTOTest='TEST_XBTGT5430';
        String gmrCodeToTest='02BF6DRG';
        CommercialReference__c crs = new CommercialReference__c(Problem__c = prob.Id, Business__c = 'Business', 
                                                                CommercialReference__c = commRefTOTest,Family__c = 'Family', 
                                                                GMRCode__c = gmrCodeToTest,ProductLine__c = 'ProductLine', 
                                                                ProductFamily__c ='ProductFamily');
        insert crs;
        
        VFC1002_AddContainmentActionInMass.WrapperClass WC = new VFC1002_AddContainmentActionInMass.WrapperClass(crs);
        
        List<VFC1002_AddContainmentActionInMass.WrapperClass> lstWC = new List<VFC1002_AddContainmentActionInMass.WrapperClass>();
        lstWC.add(WC);
                
        addConActionInMass.setAccOrgData(mywclist);
        mywclist = addConActionInMass.getAccOrgData();
        System.debug('****asdf'+mywclist.size() );
        mywclist[0].isSelected = true;         
        addConActionInMass.setAccOrgData(mywclist);
        addConActionInMass.getDisplayAffectedProducts();
        lstWC[0].checkbox = true; 
        addConActionInMass.lstWrapperClass[0].checkbox = true;
        addConActionInMass.conAction.Title__c='test'; 
        addConActionInMass.insertContainmentActioninBulk(lstXA);
        addConActionInMass.continueCreation();
        addConActionInMass.hidecolumn();
        addConActionInMass.docancel();
        
        System.assertEquals(addConActionInMass.hasNext,false);
        
        System.debug('asdf2'+accOrg);
     }
   }  
    static testMethod void VFC1002_AddContainmentActionInMass_TEST2()
  {
    User runAsUser = Utils_TestMethods.createStandardUser('test');
    insert runAsUser;
    Id userId = UserInfo.getUserId();     
    String role1 = 'CS&Q Manager';
    String role2 = 'Default Action Owner';
    
    System.runAs(runAsUser) {
    
        BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                accOrg1.Name='Test';
                accOrg1.Entity__c='Test Entity-2'; 
                insert  accOrg1;
        BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                accOrg2.Name='Test';
                accOrg2.Entity__c='Test Entity-2'; 
                accOrg2.SubEntity__c='Test Sub-Entity 2';
                insert  accOrg2;
        BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                accOrg.Name='Test';
                accOrg.Entity__c='Test Entity-2';  
                accOrg.SubEntity__c='Test Sub-Entity 2';
                accOrg.Location__c='Test Location 2';
                accOrg.Location_Type__c='Design Center';
                insert  accOrg;
        
        Problem__c prob = Utils_TestMethods.createProblem(accOrg.id);
        prob.Severity__c = 'Safety Related';        
        prob.RecordTypeId = Label.CLI2PAPR120014;
        insert prob;
        
        OSACMeeting__c  OSRec=new OSACMeeting__c();
        OSRec.Start__c = Date.today();
        OSRec.Problem__c = prob.id ;        
        OSRec.Decision__c = 'Pending';
        OSRec.ObjectiveOfMeeting__c='test';
        OSRec.Participants__c='test';
        OSRec.End__c = system.today();
        Insert OSRec;
        OSRec.Decision__c='No-Go';
        OSRec.MeetingFacilitator__c='test';
        OSRec.DecisionSummary__c='test2';
        OSRec.ProblemSummary__c='test';
        OSRec.ProblemLeader__c=runAsUser.id;
        OSRec.CSQManager__c=runAsUser.id;
        OSRec.End__c = system.today();
        update OSRec;  
        
        EntityStakeholder__c es1 = Utils_TestMethods.createEntityStakeholder(accOrg.id, userId, role1);
        insert es1;
        
        EntityStakeholder__c es2 = Utils_TestMethods.createEntityStakeholder(accOrg.id, userId, role2);
        insert es2;
        
        List<ContainmentAction__c> lstXA = new List<ContainmentAction__c>();
        ContainmentAction__c testConAction1 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);
        insert testConAction1;  
        ContainmentAction__c testConAction2 = new ContainmentAction__c();
        testConAction2.Title__c = 'Test Internal';
        testConAction2.RelatedProblem__c = prob.Id;
        testConAction2.AccountableOrganization__c = accOrg.Id;
        testConAction2.Status__c = 'Not Started';
        testConAction2.DueDate__c = System.TODAY();
        testConAction2.Shipments__c = 'Hold';
        insert testConAction2;  
        
        ContainmentAction__c testConAction3 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);
        ContainmentAction__c testConAction4 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);       
        lstXA.add(testConAction3); 
        lstXA.add(testConAction4); 
        
        ApexPages.StandardController conActionController = new ApexPages.StandardController(lstXA[1]);
        PageReference pageRef = Page.VFP1002_AddContainmentActionInMass; 
        ApexPages.currentPage().getParameters().put('id', testConAction2.id);
        ApexPages.currentPage().getParameters().put('pid', prob.id);
        ApexPages.currentPage().getParameters().put('source', 'holdsource');
        VFC_ControllerBase basecontroller = new VFC_ControllerBase();
        VFC1002_AddContainmentActionInMass addConActionInMass = new VFC1002_AddContainmentActionInMass(conActionController);
            
        addConActionInMass.LocType = 'Design Center';
        addConActionInMass.getOptions();
        addConActionInMass.searchString = 'Test Location 2';
        addConActionInMass.getQueryString();
        //addConActionInMass.doSearchAccountableOrgs();
        
        addConActionInMass.blnstatus1 = true;
        addConActionInMass.APsPresent = true;
        addConActionInMass.source='holdsource';
        addConActionInMass.accountableOrg= new BusinessRiskEscalationEntity__c();
        addConActionInMass.searchLocation='';
        addConActionInMass.title= 'Test Locat';
        addConActionInMass.errorString='';
        
        
        
        VFC1002_AddContainmentActionInMass.MyWrapper mywclist1 = new VFC1002_AddContainmentActionInMass.MyWrapper(accOrg.Entity__c,accOrg.SubEntity__c,accOrg.Location__c,accOrg.Id,es1);
        
        List<VFC1002_AddContainmentActionInMass.MyWrapper> mywclist=new List<VFC1002_AddContainmentActionInMass.MyWrapper>();
        mywclist.add(mywclist1);        
        
        String commRefTOTest='TEST_XBTGT5430';
        String gmrCodeToTest='02BF6DRG';
        CommercialReference__c crs = new CommercialReference__c(Problem__c = prob.Id, Business__c = 'Business', 
                                                                CommercialReference__c = commRefTOTest,Family__c = 'Family', 
                                                                GMRCode__c = gmrCodeToTest,ProductLine__c = 'ProductLine', 
                                                                ProductFamily__c ='ProductFamily');
        insert crs;
        
        VFC1002_AddContainmentActionInMass.WrapperClass WC = new VFC1002_AddContainmentActionInMass.WrapperClass(crs);
        
        List<VFC1002_AddContainmentActionInMass.WrapperClass> lstWC = new List<VFC1002_AddContainmentActionInMass.WrapperClass>();
        lstWC.add(WC);
                
        addConActionInMass.setAccOrgData(mywclist);
        mywclist = addConActionInMass.getAccOrgData();
        System.debug('****asdf'+mywclist.size() );
        mywclist[0].isSelected = true;         
        addConActionInMass.setAccOrgData(mywclist);
        addConActionInMass.getDisplayAffectedProducts();
        lstWC[0].checkbox = true; 
        addConActionInMass.lstWrapperClass[0].checkbox = true;
        addConActionInMass.conAction.Title__c='test'; 
        addConActionInMass.insertContainmentActioninBulk(lstXA);
        addConActionInMass.continueCreation();
        addConActionInMass.hidecolumn();
        addConActionInMass.docancel();
        
        //System.assertEquals(addConActionInMass.hasNext,false);
        
        System.debug('asdf2'+accOrg);
     }
  }
}