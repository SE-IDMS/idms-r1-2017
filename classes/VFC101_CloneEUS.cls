/*
+-----------------------+---------------------------------------------------------------------------------------+
| Author                | Alexandre Gonzalo                                                                     |
+-----------------------+---------------------------------------------------------------------------------------+
| Description           |                                                                                       |
|                       |                                                                                       |
|     - Component       | VFC101_CloneEUS                                                                       |
|                       |                                                                                       |
|     - Object(s)       | End User Support                                                                      |
|     - Description     |   - Override the Clone button and set the EUS Status to 'New'                         |
|                       |     > Default Controller: VFC101_CloneEUS(ApexPages.StandardController stdController) |
|                       |     > Method:             getClonePage()                                              |
|                       |                                                                                       |
|                       |                                                                                       |
+-----------------------+---------------------------------------------------------------------------------------+
| Delivery Date         | March, 30th 2012                                                                   |
+-----------------------+---------------------------------------------------------------------------------------+
| Modifications         |                                                                                       |
+-----------------------+---------------------------------------------------------------------------------------+
| Governor informations |                                                                                       |
+-----------------------+---------------------------------------------------------------------------------------+
*/

public class VFC101_CloneEUS
{

    private final EUS_EndUserSupport__c Eus; 
    
    public VFC101_CloneEUS(ApexPages.StandardController stdController)
    {
        Eus = (EUS_EndUserSupport__c)stdController.getRecord();
    }

    public PageReference getClonePage()
    {
        PageReference clonepage = new PageReference('/' + Eus.Id + '/e?');
        clonepage.getParameters().put(System.Label.CL10241, System.Label.CL10243);
        clonepage.getParameters().put(System.Label.CL10242, System.Label.CL10243);
        clonepage.getParameters().put(System.Label.CL10244, System.Label.CL10245);
        clonepage.getParameters().put(System.Label.CL10246, System.Label.CL10247);
        clonepage.getParameters().put(System.Label.CL00330, Eus.Id);
        return clonepage;
    }
}