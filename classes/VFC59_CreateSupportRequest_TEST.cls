/*    Author          : Accenture Team    
      Date Created    : 18/05/2011     
      Description     : Test Class for VFC59_CreateSupportRequest 
*/
@isTest
private class VFC59_CreateSupportRequest_TEST {
    
/* @Method <This method deletePlatformingsTestMethod is used test the VFC58_TransferOpportunityOwnership class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Throwing an Error>
*/
    static testMethod void createSupportRequestTestMethod()
    {
        Profile profile = [select id from profile where name='System Administrator'];    
        User user1 = Utils_TestMethods.createStandardUser(profile.id,'TestUse1');
        database.insert(user1);
        
        Profile profile1 = [select id from profile where name='System Administrator'];    
        User user2 = Utils_TestMethods.createStandardUser(profile1.id,'TestUse2');
        database.insert(user2);
        
        Profile profile2 = [select id from profile where name='System Administrator'];    
        User user3 = Utils_TestMethods.createStandardUser(profile2.id,'TestUse3');
        database.insert(user3);
        // Profile profile3 = [select id from profile where name='SE - Salesperson'];
        // User user4 = Utils_TestMethods.createStandardUser(profile3.id,'TestUse4');
        // database.insert(user4);

        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        User user4;
        System.runAs(sysadmin2){
            UserandPermissionSets user4userandpermissionsetrecord=new UserandPermissionSets('user','SE - Salesperson');
           user4 = user4userandpermissionsetrecord.userrecord;
            Database.insert(user4);

            List<PermissionSetAssignment> permissionsetassignmentlstforuser4=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:user4userandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforuser4.add(new PermissionSetAssignment(AssigneeId=user4.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforuser4.size()>0)
            Database.insert(permissionsetassignmentlstforuser4);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }
        
        Account accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts); 
    
        Opportunity opportunity= Utils_TestMethods.createOpportunity(accounts.id);
        opportunity.OpptyPriorityLevel__c='standard';
        opportunity.OwnerId = user1.id; 
        Database.insert(opportunity);
        
        opportunityTeamMember opportunityTeam1 = new opportunityTeamMember(OpportunityID=opportunity.id,userId=user1.id);
        database.insert(opportunityTeam1);
        
        opportunityTeamMember opportunityTeam2 = new opportunityTeamMember(OpportunityID=opportunity.id,userId=user2.id);
        database.insert(opportunityTeam2); 
        opportunityTeamMember opportunityTeam3 = new opportunityTeamMember(OpportunityID=opportunity.id,userId=user4.id);
        database.insert(opportunityTeam3); 
        
        opportunityShare Sharing = new opportunityShare();
        sharing.OpportunityId=opportunity.id;
        sharing.UserOrGroupId= user2.id;
        sharing.OpportunityAccessLevel = Label.CL00203;   
        database.insert(sharing);
        
        opportunityShare Sharing2 = new opportunityShare();
        Sharing2.OpportunityId=opportunity.id;
        Sharing2.UserOrGroupId= user2.id;
        Sharing2.OpportunityAccessLevel = Label.CL00203;   
        database.insert(Sharing2);
        
              
        String Id =[select id from recordtype where name=:Label.Cl00538 and sObjectType=:'OPP_SupportRequest__c'].id;
        String Id1 =[select id from recordtype where name!=:Label.Cl00538 and sObjectType=:'OPP_SupportRequest__c' LIMIT 1].id;
        /*OPP_supportRequest__c supReq = Utils_TestMethods.createSupportRequest(opportunity.Id);
        supReq.recordTypeID=id;
        supReq.AssignedToUser__c=user1.id;
        //database.insert(supReq);*/
        
        test.startTest();
        
        system.runAs(user1)
        {
        ApexPages.StandardController sc1= new ApexPages.StandardController(new OPP_SupportRequest__c());
        PageReference pageRef= page.VFP59_CreateSupportRequest; 
        pageRef.getParameters().put(Label.CL00550, opportunity.id);
        pageRef.getParameters().put(Label.CL00550.substring(17), opportunity.Name);
        pageRef.getParameters().put('RecordType',Id);
        pageRef.getParameters().put('save_new','1');
        Test.setCurrentPageReference(pageRef); 
        VFC59_CreateSupportRequest cls = New VFC59_CreateSupportRequest(sc1);
        cls.checkAccess();
        cls.cancel();
        }
        
        system.runAs(user2)
        {
        ApexPages.StandardController sc2= new ApexPages.StandardController(new OPP_SupportRequest__c());
        PageReference pageRef2= page.VFP59_CreateSupportRequest; 
        pageRef2.getParameters().put(Label.CL00550, opportunity.id);
        pageRef2.getParameters().put(Label.CL00550.substring(17), opportunity.Name);
        pageRef2.getParameters().put('RecordType',Id1);
        Test.setCurrentPageReference(pageRef2); 
        VFC59_CreateSupportRequest cls2 = New VFC59_CreateSupportRequest(sc2);
        cls2.checkAccess();
        cls2.cancel();
        }
        
        system.runAs(user3)
        {
        ApexPages.StandardController sc3= new ApexPages.StandardController(new OPP_SupportRequest__c());
        PageReference pageRef3= page.VFP59_CreateSupportRequest; 
        pageRef3.getParameters().put(Label.CL00550, opportunity.id);
        pageRef3.getParameters().put(Label.CL00550.substring(17), opportunity.Name);
        pageRef3.getParameters().put('RecordType',Id1);
        Test.setCurrentPageReference(pageRef3); 
        VFC59_CreateSupportRequest cls3 = New VFC59_CreateSupportRequest(sc3);
        cls3.checkAccess();
        cls3.cancel();
        }
        
        system.runAs(user4)
        {
        ApexPages.StandardController sc4= new ApexPages.StandardController(new OPP_SupportRequest__c());
        PageReference pageRef4= page.VFP59_CreateSupportRequest; 
        pageRef4.getParameters().put(Label.CL00550, opportunity.id);
        pageRef4.getParameters().put(Label.CL00550.substring(17), opportunity.Name);
        pageRef4.getParameters().put('RecordType',Id1);
        Test.setCurrentPageReference(pageRef4); 
        VFC59_CreateSupportRequest cls4 = New VFC59_CreateSupportRequest(sc4);
        cls4.checkAccess();
        cls4.cancel();
        }
        
        
        test.stopTest();        
    }
}