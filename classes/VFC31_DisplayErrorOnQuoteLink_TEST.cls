/*
    Author          : Abhishek (ACN) 
    Date Created    : 10/05/2011
    Description     : Test class for the apex class VFC31_DisplayErrorOnQuoteLink .
*/
@isTest
private class VFC31_DisplayErrorOnQuoteLink_TEST {
    
/* @Method <This method DisplayErrorOnQuoteLinkTestMethod is used test the VFC31_DisplayErrorOnQuoteLink class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Throwing an Exception>
*/
    static testMethod void DisplayErrorOnQuoteLinkTestMethod()
    {
        Account accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts); 
        
        Opportunity opportunity= Utils_TestMethods.createOpportunity(accounts.id);
        Database.insert(opportunity);         
        
        OPP_QuoteLink__c newQuoteLink=Utils_TestMethods.createQuoteLink(opportunity.id);
        newQuoteLink.TECH_IsSyncNeededForOpp__c=true;
        newQuoteLink.TECH_IsBoundToBackOffice__c=true;
        Database.insert(newQuoteLink);
        
        Test.startTest();
        ApexPages.StandardController sc1= new ApexPages.StandardController(newQuoteLink);
        Pagereference p = page.VFP31_DisplayErrorOnQuoteLink;
        Test.setCurrentPageReference(p); 
        VFC31_DisplayErrorOnQuoteLink displayErrorOnQuoteLink= new VFC31_DisplayErrorOnQuoteLink(sc1);
        displayErrorOnQuoteLink.displayError();
        newQuoteLink.QuoteStatus__c='denied';
        newQuoteLink.TECH_IsSynchronizationNeeded__c=true;
        database.Update(newQuoteLink);
        ApexPages.StandardController sc2= new ApexPages.StandardController(newQuoteLink);
        VFC31_DisplayErrorOnQuoteLink displayErrors= new VFC31_DisplayErrorOnQuoteLink(sc2);
        displayErrors.displayError();
        }
}