/********************************************************************
* Company: Fielo
* Created Date: 29/11/2016
* Description: Test class for class FieloPRM_AP_SectionTriggers
********************************************************************/

@isTest
public with sharing class FieloPRM_AP_SectionTriggersTest{
  
    public static testMethod void testUnit1(){
    	FieloEE.MockUpFactory.setCustomProperties(false);

    	FieloEE__PublicSettings__c ps = new FieloEE__PublicSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), CacheEnabled__c = true);
    	insert ps;

    	FieloEE__Menu__c menu1 = new FieloEE__Menu__c(
            Name = 'test menu 1', 
            FieloEE__Home__c = true, 
            FieloEE__Visibility__c ='Both', 
            FieloEE__Placement__c = 'Main',
            FieloEE__ExternalName__c = 'menu1' + datetime.now()
        );
        insert menu1;

        FieloEE__Section__c section1 = new FieloEE__Section__c(
            FieloEE__Menu__c = menu1.Id, 
            FieloEE__Type__c = '12'
        );
        insert section1;

        delete section1;
    }
}