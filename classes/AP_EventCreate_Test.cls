@isTest
public class AP_EventCreate_Test {
    
    static testMethod void myUnitTest() {
        
       // system.runAs(getUser()){
            Country__c countrySobj =Utils_TestMethods.createCountry();
            insert countrySobj ;
            Account accountSobj = Utils_TestMethods.createAccount();
            accountSobj.SEAccountID__c ='SEAccountID';
            accountSobj.RecordTypeId  = System.Label.CLOCT13ACC08;
            accountSobj.Country__c= countrySobj.id;
            insert accountSobj;
            Contact contactSobj = Utils_TestMethods.createContact(accountSobj.id, 'contact LastName');
            contactSobj.Country__c = countrySobj.id;
            contactSobj.SEContactID__c ='SEContactID1';
            contactSobj.FirstName = 'CFirstName';
            contactSobj.LastName= 'CLastName';
            contactSobj.LocalFirstName__c= 'CLFirstName';
            contactSobj.MidInit__c ='TMid';
            contactSobj.LocalMidInit__c='TLMid';
            contactSobj.LocalLastName__c='CLLastName';
            contactSobj.Email='test@test.com';
            contactSobj.mobilePhone ='9999999999';
            contactSobj.WorkPhone__c='9999999999';
            insert contactSobj;
            SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(accountSobj.id);
            workOrder.Work_Order_Category__c='On-site';                 
            workOrder.SVMXC__Order_Type__c='Maintenance';
            workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
            workOrder.SVMXC__Order_Status__c = 'UnScheduled';
            workOrder.CustomerRequestedDate__c = Date.today();
            workOrder.Service_Business_Unit__c = 'IT';
            workOrder.SVMXC__Priority__c = 'Normal-Medium';                 
            workOrder.SendAlertNotification__c = true;            
            workOrder.BackOfficeReference__c = '111111';            
            workOrder.Comments_to_Planner__c='testing'; 
            workOrder.SVMXC__Company__c  =accountSobj.Id;
            workOrder.SVMXC__Contact__c  =contactSobj.Id;
            workOrder.CustomerTimeZone__c= 'Europe/London';      
            insert workOrder;
            Schema.DescribeSObjectResult dSobjres = Schema.SObjectType.SVMXC__Service_Group__c; 
            Map<String,Schema.RecordTypeInfo> ServiceGroup = dSobjres.getRecordTypeInfosByName();
            //Id Technician = ServiceGroup.get('Technician').getRecordTypeId();
            SVMXC__Service_Group__c sg = new SVMXC__Service_Group__c();
            sg.name = 'Test Service Team';
            sg.SVMXC__Active__c = true;
            sg.RecordTypeId = '012A0000000nYLvIAM';
            insert sg;
            
            Schema.DescribeSObjectResult dSobjres2 = Schema.SObjectType.SVMXC__Service_Group_Members__c; 
            Map<String,Schema.RecordTypeInfo> FSRRT = dSobjres2.getRecordTypeInfosByName();
            //Id fsr = FSRRT.get('FSR').getRecordTypeId();
            SVMXC__Service_Group_Members__c sgm = new SVMXC__Service_Group_Members__c();          
            sgm.RecordTypeId = '012A0000000nYNhIAM';
            sgm.SVMXC__Service_Group__c =sg.id;
            sgm.SVMXC__Role__c = 'Schneider Employee';            
            sgm.SVMXC__Salesforce_User__c = userinfo.getUserId();
            sgm.Send_Notification__c = 'Email';
            sgm.SVMXC__Email__c = 'test.test@company.com';
            sgm.SVMXC__Phone__c = '123456789';
            insert sgm;
            
            
            Event event= new Event (Subject=workOrder.name,
                                  StartDateTime=system.now().addDays(1),
                                  EndDateTime=system.now().addDays(1),
                                  Whatid=workOrder.id,
                                  
                                  Ownerid=UserInfo.getUserId()                                                    
                                  );
               try{                   
            insert event;
            } catch (exception e){}
            AssignedToolsTechnicians__c att = Utils_TestMethods.CreateAssignedTools_Technician(WorkOrder.id, sgm.id);
            att.Status__c = 'Accepted';
            att.EventId__c = event.id;
            insert att;
            set<id> eventset= new set<id>();
            String EventString = JSON.serialize(new list<Event>{event});
            try{
            AP_EventCreate.CreateAssignedToolsTechnicians(new Set<id>{workOrder.id},EventString,'');
            }catch (exception e){}
            AP_EventCreate.UpdateAssignedToolsTechnicians(EventString,new Set<id>{att.id});
            SVMXC__SVMX_Event__c sevent = New SVMXC__SVMX_Event__c(
                                                        Name=workOrder.name,
                                                        SVMXC__Service_Team__c=sg.Id,
                                                        SVMXC__Technician__c=sgm.Id,
                                                        SVMXC__StartDateTime__c=system.now(),
                                                        SVMXC__EndDateTime__c=system.now()+1,
                                                        SVMXC__WhatId__c = workOrder.Id
                                                     );                                                     
             
           
            insert sevent;
            eventset.add(sevent.id);
  
               String EventStrng = JSON.serialize(new list<SVMXC__SVMX_Event__c >{sevent});
           try{ 
            AP_EventCreate.SVMXCreateAssignedToolsTechnicians(EventStrng,eventset);  
             }catch (exception e){}
 
    }
}