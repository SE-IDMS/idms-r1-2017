@isTest
global class WS_FacadeDelegatedAuthService_Mock implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		WS_FacadeDelegatedAuthService.AuthenticateResult objAuthenticateResult = new WS_FacadeDelegatedAuthService.AuthenticateResult();
		objAuthenticateResult.Authenticated = true;
		response.put('response_x', objAuthenticateResult); 
   }
}