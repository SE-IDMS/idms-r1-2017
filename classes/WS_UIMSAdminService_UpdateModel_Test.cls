@isTest
public class WS_UIMSAdminService_UpdateModel_Test {
    
    public static testMethod void testModels_1() {
        WS_UIMSAdminService_UpdateModel.userUpdateRequest uur1 = new WS_UIMSAdminService_UpdateModel.userUpdateRequest();
        WS_UIMSAdminService_UpdateModel.contactBean cb1 = new WS_UIMSAdminService_UpdateModel.contactBean();
        WS_UIMSAdminService_UpdateModel.accountUpdateRequest aur1 = new WS_UIMSAdminService_UpdateModel.accountUpdateRequest();
        WS_UIMSAdminService_UpdateModel.updateContactsResponse ucr1 = new WS_UIMSAdminService_UpdateModel.updateContactsResponse();
        WS_UIMSAdminService_UpdateModel.accountBean ab1 = new WS_UIMSAdminService_UpdateModel.accountBean();
        WS_UIMSAdminService_UpdateModel.updateReturnBean urb1 = new WS_UIMSAdminService_UpdateModel.updateReturnBean();
        WS_UIMSAdminService_UpdateModel.updateAccountsResponse uar1 = new WS_UIMSAdminService_UpdateModel.updateAccountsResponse();
        WS_UIMSAdminService_UpdateModel.updateProgramsResponse upr1 = new WS_UIMSAdminService_UpdateModel.updateProgramsResponse();
        WS_UIMSAdminService_UpdateModel.updateAccounts ua1 = new WS_UIMSAdminService_UpdateModel.updateAccounts();
        WS_UIMSAdminService_UpdateModel.updateContacts uc1 = new WS_UIMSAdminService_UpdateModel.updateContacts ();
    }
    
    public static testMethod void testUpdtContact() {
        Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
        WS_UIMSAdminService_UpdateModel.userUpdateRequest upm = new WS_UIMSAdminService_UpdateModel.userUpdateRequest();
        WS_UIMSAdminService_Update.BFOIOServiceImplPort Cont = new WS_UIMSAdminService_Update.BFOIOServiceImplPort();
        cont.updateContacts(upm);
        
    }
    
     public static testMethod void testUpdateAccount() {
        Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
        WS_UIMSAdminService_UpdateModel.accountUpdateRequest upm = new WS_UIMSAdminService_UpdateModel.accountUpdateRequest();
        WS_UIMSAdminService_Update.BFOIOServiceImplPort acc = new WS_UIMSAdminService_Update.BFOIOServiceImplPort();
        acc.updateAccounts(upm);
        
    }
    
}