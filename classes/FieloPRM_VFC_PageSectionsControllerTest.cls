/********************************************************************
* Company: Fielo
* Created Date: 29/08/2016
* Description: Test Class for Class FieloPRM_VFC_PageSectionsController
********************************************************************/
@isTest
public class FieloPRM_VFC_PageSectionsControllerTest{
    
    public static testMethod void testUnit1(){

        FieloEE.MockUpFactory.setCustomProperties(false);
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c(
            Name = 'test', 
            FieloEE__Home__c = true
        );
        insert menu;
        
        FieloPRM_VFC_PageSectionsController controller = new FieloPRM_VFC_PageSectionsController();
             
        FieloEE__Section__c section1 = new FieloEE__Section__c(
            FieloEE__Menu__c = menu.Id, 
            FieloEE__Type__c = '12'
        );
        insert section1;
        
        FieloEE__Section__c section2 = new FieloEE__Section__c(
            FieloEE__Menu__c = menu.Id, 
            FieloEE__Type__c = '12', 
            FieloEE__Parent__c = section1.Id
        );
        insert section2;
        
        FieloEE__Section__c section3 = new FieloEE__Section__c(
            FieloEE__Menu__c = menu.Id, 
            FieloEE__Type__c = '12', 
            FieloEE__Parent__c = section1.Id
        );
        insert section3;

        List<RecordType> rtComponent = [SELECT Id FROM RecordType WHERE (DeveloperName = 'Banner' OR DeveloperName = 'News') AND sObjectType = 'FieloEE__Component__c'];
        
        FieloEE__Component__c comp1 = new FieloEE__Component__c(
            FieloEE__Section__c = section2.Id, 
            FieloEE__Menu__c = menu.Id,
            FieloEE__Mobile__c = true,
            FieloEE__Tablet__c = false,
            FieloEE__Desktop__c = false,
            RecordTypeId = rtComponent[0].Id
        );

        FieloEE__Component__c comp2 = new FieloEE__Component__c(
            FieloEE__Section__c = section2.Id, 
            FieloEE__Menu__c = menu.Id,
            FieloEE__Mobile__c = true,
            FieloEE__Tablet__c = false,
            FieloEE__Desktop__c = false,
            RecordTypeId = rtComponent[1].Id
        );
        
        insert new List<FieloEE__Component__c>{comp1,comp2};
        
        ApexPages.currentPage().getParameters().put('sectionId', section1.Id);
        
        FieloPRM_VFC_PageSectionsController controller2 = new FieloPRM_VFC_PageSectionsController();
              
        FieloPRM_VFC_PageSectionsController.getDocURL();
        
        FieloPRM_VFC_PageSectionsController.fieldsML = null;
        
        FieloPRM_FieldsML aux = FieloPRM_VFC_PageSectionsController.fieldsML;
        
    }

}