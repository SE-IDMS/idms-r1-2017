global with sharing class FieloPRM_VFC_CookieSegmentRefreshCtrl{
    
    @RemoteAction
    global static String getSegments(Id memberId) {
        List<String> rules = FieloEE.RedemptionUtil.lookForMatchingRedemptionRules(new FieloEE__Member__c(Id = memberId));
        return String.join(rules, '%2C');
    }
    
}