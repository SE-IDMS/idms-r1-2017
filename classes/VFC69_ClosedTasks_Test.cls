@isTest
private class VFC69_ClosedTasks_Test{

        public static testMethod void closedTaskTestMethod() {
            User tuser=new User();
            tuser.FirstName='FirstName';
            tuser.LastName ='Last Name';
            tuser.Email='closetedtasktest@schneider-electric.com';
            tuser.EmailEncodingKey='ISO-8859-1';
            tuser.LanguageLocaleKey='en_US';
            tuser.LocaleSidKey='en_US';
            tuser.TimeZoneSidKey='GMT';
            tuser.Username='closetedtasktest@schneider-electric.com';            
            tuser.Alias='eueea';
            tuser.CommunityNickname='something';
            tuser.ProfileId=UserInfo.getProfileId();
            insert tuser;
            
            Account acc=Utils_TestMethods.createAccount();            
            insert acc;            
            Contact con=Utils_TestMethods.createContact(acc.id,'last name');            
            insert con;            
            Case c=Utils_TestMethods.createCase(acc.id,con.id,'closed');            
            insert c;
            Task t=Utils_TestMethods.createTask(c.id,con.id,'Completed');
            t.OwnerId=tuser.id;
            Insert t;
        
          
            PageReference pageRef = Page.VFP69_ClosedTasks;
            Test.setCurrentPage(pageRef);
            VFC69_ClosedTasks controller = new VFC69_ClosedTasks();
            //controller.userName();
            controller.getTasks();
            
            
            
        }

}