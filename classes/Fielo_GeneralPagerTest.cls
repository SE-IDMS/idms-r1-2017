/************************************************************
*                   Company: ValueNet                       *
*-----------------------------------------------------------*
* Type: Test Class                                          *
* Tested Class: Fielo_GeneralPagerTest                      *
* Developer: Ramiro Ichazo                                  *
************************************************************/

@isTest
public with sharing class Fielo_GeneralPagerTest {
    
    static testmethod void test(){
        //AggregateResult[] results = [Select count(id) total from User ];
        Integer totalUsers = 100;
        Fielo_GeneralPager paginatedList = new Fielo_GeneralPager('Select id from User where name like \'%sesa%\' limit 100',1);
         
        paginatedList.getRecords();
        paginatedList.getItemsDisplaying();
        paginatedList.setPageNumber(1);
        paginatedList.getTotalPages();
        paginatedList.getPageSize();
/*        system.assertEquals(totalUsers,paginatedList.getResultSize());
        system.assertEquals(1, paginatedList.getPageNumber());
        system.assertEquals(false,paginatedList.hasPrevious);*/
        if(totalUsers>1){
            system.assertEquals(true,paginatedList.hasNext);
            paginatedList.next();
            system.assertEquals(true,paginatedList.hasPrevious);
            system.assertEquals(2, paginatedList.getPageNumber());
            paginatedList.previous();
            system.assertEquals(1, paginatedList.getPageNumber());
            paginatedList.last();
/*            system.assertEquals(totalUsers,paginatedList.getPageNumber());*/
            system.assertEquals(false,paginatedList.hasNext);
            paginatedList.first();
            system.assertEquals(1, paginatedList.getPageNumber());
             
        }else{
            system.assertEquals(false, paginatedList.hasNext);
        }
    }

}