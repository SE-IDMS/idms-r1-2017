public class AP38_createSolutionCenterCountry
{
    public static void createNewSolutionCenterCountry(List<SolutionCenter__c> newSolutionCenter)
    {
       //*********************************************************************************
        // Method Name      : createNewSolutionCenterCountry
        // Purpose          : To create a Solution Center Country on Creation Solution Center
        // Created by       : Hari Krishna - Global Delivery Team
        // Date created     : 
        // Modified by      :
        // Date Modified    :
        // Remarks          : For April Sales - 12 Release
        ///********************************************************************************/
       List<SolutionCenterCountry__c> scctoCreate=new List<SolutionCenterCountry__c>();
       for(SolutionCenter__c sc:newSolutionCenter){
           if(sc.CountryOfLocation__c!= null ){
               SolutionCenterCountry__c scc=new SolutionCenterCountry__c();
               scc.Country__c = sc.CountryOfLocation__c;
               scc.Solution_Center__c = sc.id;
               scctoCreate.add(scc);
           }
       }
       if(scctoCreate!= null && scctoCreate.size()>0){
       		insert scctoCreate;
       }
    }

}