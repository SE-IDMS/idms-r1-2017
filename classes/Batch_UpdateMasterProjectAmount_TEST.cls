//Test class for Batch_UpdateMasterProjectAmount - OCT 2015 release
@isTest
public class Batch_UpdateMasterProjectAmount_TEST {
     static testMethod void testBatch_UpdateMasterProjectAmount() {
         Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        Database.insert(acc); 
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        insert prj;
        prj.CurrencyIsoCode = 'INR';
        update prj;  
        
        Opportunity opp1 = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp1.Project__c=prj.Id;
        Insert opp1;
        
        Opportunity opp2 = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp2.ParentOpportunity__c=opp1.Id;
        Insert opp2;
        
        Opportunity opp3 = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp3.ParentOpportunity__c=opp2.Id;
        Insert opp3;
            
        Test.startTest();
        Batch_UpdateMasterProjectAmount prjUpdates = new Batch_UpdateMasterProjectAmount(); 
        database.executebatch(prjUpdates,Integer.ValueOf(Label.CLOCT15SLS78));
         Test.stopTest();
         
     }
}