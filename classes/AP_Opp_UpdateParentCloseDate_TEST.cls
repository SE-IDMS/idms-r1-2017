//Test class for AP_Opp_UpdateParentCloseDate
@isTest
private class AP_Opp_UpdateParentCloseDate_TEST 
{
    private static Account acc; 
    private static Country__c cntry;
    
    static
    {
        // create test data        
        acc = Utils_TestMethods.createAccount();
        insert acc;
        
        
    }
    
    static testMethod void testOpportunityParentCloseDateUpdation()  
    {    
        
        cntry = new Country__c(name='India',CountryCode__c='IN');
        insert cntry; 
        
        Date date1 = date.today();            
        Date date2 = date1.addDays(1);
        Date date3 = date1.addDays(2);        
        
        Opportunity oppParent = new Opportunity(Name='Test Opp Parent',RecordTypeId='012A0000000nYNa', AccountId=acc.Id, StageName='3 - Identify & Qualify',Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=cntry.id , IncludedInForecast__c='No',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R', OpptyType__c='Standard',ParentOpportunity__c=null, closedate=date1);      
        insert oppParent;
        
        
        Opportunity oppChild = new Opportunity(Name='Test Opp Child',RecordTypeId='012A0000000nYNa',  AccountId=acc.Id, StageName='3 - Identify & Qualify',Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=cntry.id , IncludedInForecast__c='No',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R',OpptyType__c='Standard', ParentOpportunity__c=oppParent.Id,closedate=date2);     
        insert oppChild; 
        Test.startTest();
        
        date3 = date3.addDays(1);
        oppChild.closedate=date3; 
        update oppChild;
        Test.stopTest();
                      
      
    }
    
    static testMethod void testOpportunityParentCloseDateUpdation_1()  
    {
    
        try
        {
            Country__c cntry1 = new Country__c(name='India',CountryCode__c='IN');
            insert cntry1; 
            
            Date date11 = date.today();            
            Date date21 = date11.addDays(5);
            Date date31 = date11.addDays(3);        
            List<Opportunity> opp= new list<Opportunity>();
            Opportunity oppParent1 = new Opportunity(Name='Test Opp Parent',RecordTypeId='012A0000000nYNa', AccountId=acc.Id, StageName='3 - Identify & Qualify',Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=cntry1.id , IncludedInForecast__c='No',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R', OpptyType__c='Standard', closedate=date11);      
            insert oppParent1;
            
            Test.startTest();
            Opportunity oppChild1 = new Opportunity(Name='Test Opp Child',RecordTypeId='012A0000000nYNa',  AccountId=acc.Id, StageName='3 - Identify & Qualify',Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=cntry1.id , IncludedInForecast__c='No',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R',OpptyType__c='Standard', ParentOpportunity__c=oppParent1.Id,closedate=date21);     
            insert oppChild1;       
            Opportunity oppChild2 = new Opportunity(Name='Test Opp Child2',RecordTypeId='012A0000000nYNa',  AccountId=acc.Id, StageName='3 - Identify & Qualify',Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=cntry1.id , IncludedInForecast__c='No',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R',OpptyType__c='Standard',ParentOpportunity__c=oppParent1.Id,closedate=date21);     
            insert oppChild2;  
            oppParent1.closedate=date31;
            update oppParent1;
            oppChild2.closedate=date31+1;
            oppChild1.closedate=date31+1;
            opp.add(oppChild2);
            opp.add(oppChild1);
            update opp;
            Test.stopTest();
        }
        catch(Exception e1)
        {
            system.debug('....error ....'+e1);
        }
        
    }
    static testMethod void testOpportunityParentCloseDateUpdation_2()  
    {
       
        try
        {
            Country__c cntry2 = new Country__c(name='India',CountryCode__c='IN');
            insert cntry2; 
            
            Date date12 = date.today();            
            Date date22 = date12.addDays(2);
            Date date32 = date12.addDays(3);   
             
            
            Opportunity oppParent2 = new Opportunity(Name='Test Opp Parent',RecordTypeId='012A0000000nYNa', AccountId=acc.Id, StageName='3 - Identify & Qualify',Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=cntry2.id , IncludedInForecast__c='No',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R', OpptyType__c='Standard', closedate=date12);      
            insert oppParent2;
            
             Test.startTest();
            Opportunity oppChild2 = new Opportunity(Name='Test Opp Child',RecordTypeId='012A0000000nYNa',  AccountId=acc.Id, StageName='3 - Identify & Qualify',Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=cntry2.id , IncludedInForecast__c='No',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R',OpptyType__c='Standard', ParentOpportunity__c=oppParent2.Id,closedate=date22);     
            insert oppChild2;       
            
            Opportunity oppGrandChild2 = new Opportunity(Name='Test Opp Grand Child',RecordTypeId='012A0000000nYNa',  AccountId=acc.Id, StageName='3 - Identify & Qualify',Amount=1500 , LeadingBusiness__c='EN' , CountryOfDestination__c=cntry2.id , IncludedInForecast__c='No',  OpportunityScope__c='ENSL2--Energy Systems - Level 2', OpportunityCategory__c='Simple',MarketSegment__c ='OEM' ,MarketSubSegment__c= 'HVAC&R',OpptyType__c='Standard', ParentOpportunity__c=oppChild2.Id,closedate=date32);     
            insert oppGrandChild2;             
            
           Test.stopTest(); 
            
        }
        catch(Exception e2)
        {
            system.debug('....error ....'+e2);
        }
        
    }
    
}