@isTest
public class ProgramSnapshotController_Test{
    static testMethod void testProgramSnapshotController(){
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPOAt'); 
        System.runAs(runAsUser){
            PageReference pageRef = Page.ProgramSnapshot;
            Test.setCurrentPage(pageRef); 
            string period = 'Q1-2014'; 
            string year = period.right(4); 
            System.Debug('Period:'+period);  
            System.Debug('Year:'+year); 
            
            IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative'); 
            insert IPOinti; 
            
            IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',
                                                                       IPO_Strategic_Program_Leader_1__c = runasuser.id,
                                                                       IPO_Strategic_Program_Leader_2__c = runasuser.id, 
                                                                       Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',
                                                                       Operation__c='NA;APAC'); 
            insert IPOp;
            
            IPO_Strategic_Program__c IPOp1=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1', 
                                                                        IPO_Strategic_Program_Leader_1__c = runasuser.id,
                                                                        IPO_Strategic_Program_Leader_2__c = runasuser.id,
                                                                        Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power', 
                                                                        Operation__c='NA;APAC'); 
            insert IPOp1; 
            
            Program_Progress_Snapshot__c PPS=new Program_Progress_Snapshot__c(IPO_Program__c=IPOp.id,IPO_Program_Health_Q1__c='On Time Delivered'); 
            PPS.Current_Year__c = '2013'; 
            insert PPS;  
            
            System.Debug('PPS:'+PPS);
            
            Program_Progress_Snapshot__c PPS1=new Program_Progress_Snapshot__c(IPO_Program__c=IPOp.id,IPO_Program_Health_Q1__c='On Time Delivered',IPO_Program_Health_Q3__c='On Time Delivered',IPO_Program_Health_Q4__c='On Time Delivered');  
            // PPS.Current_Year__c = Year; 
            PPS1.Current_Year__c = '2014';  
            insert PPS1;            
            
            System.Debug('PPS1:'+PPS1);    
            period = 'Q2-2014';
            year = period.right(4);
            
            Program_Progress_Snapshot__c PPS4=new Program_Progress_Snapshot__c(IPO_Program__c=IPOp1.id, IPO_Program_Health_Q2__c='On Time Delivered');    
            PPS4.Current_Year__c = Year;  
            // PPS4.Current_Year__c = '2014';   
            insert PPS4;                      
            
            IPO_Strategic_Milestone__c IPOM=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile',Year__c = year,IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation_Regions__c='NA;APAC',Due_Date__c=Date.ValueOf(Year + label.IPO_Q4_Start));  
        //    IPOM.Year__c = year;
            Insert IPOM; 
            
             IPO_Strategic_Milestone__c IPOM3=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile3',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(Year + label.IPO_Q1_Start),Progress_Summary_Q2__c ='In Progress',Status_in_Quarter_Q2__c='TestQ2',Roadblocks_Q2__c='Test1Q2',Decision_Review_Outcomes_Q2__c = 'testQ2');  
            IPOM3.Year__c = year;
            Insert IPOM3;  

            IPO_Strategic_Milestone__c IPOM4=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile4',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(Year + label.IPO_Q3_Start),Progress_Summary_Q3__c ='In Progress',Status_in_Quarter_Q3__c='TestQ3',Roadblocks_Q3__c='Test1Q3',Decision_Review_Outcomes_Q3__c = 'testQ3'); 
            IPOM4.Year__c = year; 
            Insert IPOM4;   

            IPO_Strategic_Milestone__c IPOM5=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile5',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(Year + label.IPO_Q4_Start),Progress_Summary_Q4__c ='In Progress',Status_in_Quarter_Q4__c='TestQ4',Roadblocks_Q4__c='Test1Q4',Decision_Review_Outcomes_Q4__c = 'testQ4');  
            IPOM5.Year__c = year;  
            Insert IPOM5;
            
             IPO_Strategic_Cascading_Milestone__c IPOcas1= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOM.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Global Funtions',Entity__c='GM',Progress_Summary_Deployment_Leader_Q1__c='In Progress',Roadblocks_Q1__c='test',Progress_Summary_Deployment_Leader_Q4__c='None',Progress_Summary_Deployment_Leader_Q3__c='None',Progress_Summary_Deployment_Leader_Q2__c=null);
            insert IPOcas1;
            IPO_Strategic_Cascading_Milestone__c IPOcas2= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOM.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Global Business',Entity__c='ITB',Status_in_Quarter_Q2__c='test',Roadblocks_Q2__c='test',Progress_Summary_Deployment_Leader_Q2__c = 'None',Progress_Summary_Deployment_Leader_Q1__c = null,Progress_Summary_Deployment_Leader_Q3__c = null,Progress_Summary_Deployment_Leader_Q4__c = null,Decision_Review_Outcomes_Q2__c='test');
            insert IPOcas2;
            
            IPOcas1.Progress_Summary_Deployment_Leader_Q1__c='Completed';
            update IPOcas1;
            
            IPOcas2.Progress_Summary_Deployment_Leader_Q1__c='In Progress';
            update IPOcas2;
            
            IPOcas2.Progress_Summary_Deployment_Leader_Q1__c='Completed';
            update IPOCas2;
            
            IPOM.Progress_Summary_Q1__c ='Completed';
            update IPOM; 
            
            IPOM5.Progress_Summary_Q1__c ='Completed';
            update IPOM5;
            
             ApexPages.currentPage().getparameters().put('Id',IPOp.id);  
            
            ApexPages.StandardController stanPage = new ApexPages.StandardController(IPOp); 
            ProgramSnapshotController controller = new ProgramSnapshotController(stanPage);  
            
            controller.getpageid(); 
            controller.getPrograms();   
            controller.getPrgProgressSnapShot();  
           // controller.getProgramSnapshot();    
            controller.period='Q1-2014';       
            controller.EditRecord();          
            
            controller.ViewRecord();        
            controller.getStatus();       
            controller.getQuarter();    
            controller.getRYear();       
            controller.YearChange();    
            controller.AddSnapshot();
            controller.getProgramSnapshot();  
            controller.getMilestoneSummary();  
            controller.save();
            
            
            controller.period='Q2-2014';
            controller.EditRecord();
            controller.getMilestoneSummary();  
           
           // controller.getProgramSnapshot();   
          //  controller.getMilestoneSummary();    
            
            controller.period='Q3-2014';  
            controller.EditRecord();
            controller.getMilestoneSummary();  
            
            controller.period='Q4-2014'; 
            controller.EditRecord();       
            controller.getMilestoneSummary();  
           
          //  controller.getProgramSnapshot();   
          //  controller.getMilestoneSummary(); 
            controller.save();
            controller.period = 'Q2';
            controller.save();
            controller.cancel(); 
            controller.period='Q1-2014';  
            controller.getProgramSnapshot();  
            controller.getMilestoneSummary(); 
            controller.save();
            controller.period='Q1-2014';       
          //  controller.EditRecord();          
            controller.AddSnapshot();
            controller.getProgramSnapshot();
          //  controller.ViewRecord();        
            controller.getStatus();   
            controller.save();
            
            ApexPages.currentPage().getparameters().put('Id',IPOp1.id);   
            
            ApexPages.StandardController stanPage4 = new ApexPages.StandardController(IPOp1);   
            ProgramSnapshotController controller4 = new ProgramSnapshotController(stanPage4);     
            
            controller4.getpageid();        
            controller4.getPrograms();       
            controller4.getPrgProgressSnapShot();     
            controller4.period='Q2-2014';         
            controller4.EditRecord();      
            controller4.ViewRecord();        
            
            controller4.getStatus();       
            controller4.getQuarter();       
            controller4.getRYear();     
            controller4.YearChange();    
            controller4.AddSnapshot();    
            controller4.getProgramSnapshot();   
            controller4.save();           
            controller4.cancel();      
            controller4.getMilestoneSummary();  
            
            controller4.period='Q3';
         
            controller4.getStatus();       
            controller4.getQuarter();       
            controller4.getRYear();     
            controller4.YearChange();    
            controller4.AddSnapshot();    
            controller4.getProgramSnapshot();   
            controller4.save();           
            controller4.cancel();      
            controller4.getMilestoneSummary(); 
            
            ApexPages.currentPage().getparameters().put('Id',IPOp1.id);   
            
            ApexPages.StandardController stanPage5 = new ApexPages.StandardController(IPOp1);   
            ProgramSnapshotController controller5 = new ProgramSnapshotController(stanPage5);     
            
            controller5.getpageid();        
            controller5.getPrograms();       
            controller5.getPrgProgressSnapShot();     
            controller5.period='Q3-2014';         
            controller5.EditRecord();      
            controller5.ViewRecord();        
            
            controller5.getStatus();       
            controller5.getQuarter();       
            controller5.getRYear();     
            controller5.YearChange();    
            controller5.AddSnapshot();    
            controller5.getProgramSnapshot();   
            controller5.save();           
            controller5.cancel();      
            controller5.getMilestoneSummary();
            controller5.period='Q3-2014';         
            controller5.EditRecord();      
            controller5.ViewRecord();        
            
            controller5.getStatus(); 
            controller5.save();

            ApexPages.currentPage().getparameters().put('Id',IPOp1.id);   
            
            ApexPages.StandardController stanPage6 = new ApexPages.StandardController(IPOp1);   
            ProgramSnapshotController controller6 = new ProgramSnapshotController(stanPage6);     
            
            controller6.isAdd = False;
            controller6.getpageid();        
            controller6.getPrograms();       
            controller6.getPrgProgressSnapShot();     
            controller6.period='Q1-2014';         
            controller6.EditRecord();      
            controller6.ViewRecord();        
            
            controller6.getStatus();       
            controller6.getQuarter();       
            controller6.getRYear();     
            controller6.YearChange();    
            controller6.AddSnapshot();    
            controller6.getProgramSnapshot();   
            controller6.save();           
            controller6.cancel();      
            controller6.getMilestoneSummary();
            
            controller6.isAdd = False;
            controller6.period='Q2-2014'; 
            controller6.getStatus();       
            controller6.getQuarter();       
            controller6.getRYear();     
            controller6.YearChange();    
            controller6.AddSnapshot();    
            controller6.getProgramSnapshot();   
            controller6.save();           
            controller6.cancel();      
            controller6.getMilestoneSummary();
            
            controller6.isAdd = False;
            controller6.period='Q3-2014'; 
            controller6.getStatus();       
            controller6.getQuarter();       
            controller6.getRYear();     
            controller6.YearChange();    
            controller6.AddSnapshot();    
            controller6.getProgramSnapshot();   
            controller6.save();           
            controller6.cancel();      
            controller6.getMilestoneSummary();
            
            controller6.isAdd = False;
            controller6.period='Q4-2014'; 
            controller6.getStatus();       
            controller6.getQuarter();       
            controller6.getRYear();     
            controller6.YearChange();    
            controller6.AddSnapshot();    
            controller6.getProgramSnapshot();   
            controller6.save();           
            controller6.cancel();      
            controller6.getMilestoneSummary();  
            
            ApexPages.currentPage().getparameters().put('Id',IPOp.id); 
            
            ApexPages.StandardController stanPage1 = new ApexPages.StandardController(IPOp);   
            ProgramSnapshotController controller1 = new ProgramSnapshotController(stanPage1);
            
            controller1.isAdd=True;   
            controller1.getpageid(); 
            controller1.getPrograms();
            controller1.getPrgProgressSnapShot(); 
            controller1.period='Q1-2015';       
            controller1.EditRecord();         
            controller1.ViewRecord();         
            controller1.getMilestoneSummary();
            controller1.getStatus();         
            controller1.getQuarter();        
            controller1.getRYear();          
            controller1.YearChange();        
            controller1.AddSnapshot();
            controller1.getProgramSnapshot();  
            controller1.save();         
            
            controller1.period='Q2-2015';  
            controller1.EditRecord(); 
            controller1.AddSnapshot();
            controller1.getProgramSnapshot();      
            controller1.save();           
            
            controller1.period='Q3-2015';   
            controller1.EditRecord();  
            controller1.AddSnapshot();
            controller1.getProgramSnapshot();      
            controller1.save();          
            
            controller1.period='Q4-2015';    
            controller1.EditRecord();
            controller1.AddSnapshot();
            controller1.isAdd = True;
            controller1.getProgramSnapshot();    
            controller1.save();       
            
            controller1.period='Q4-2015';
            controller1.AddSnapshot();
            controller1.isAdd = True;
            controller1.getProgramSnapshot();
            controller1.save();
            
            controller1.period='Q2';
            controller1.save();
            
            controller1.period = 'Q1-2016';
            controller1.EditRecord();    
            controller1.save();
            
            controller1.period = 'Q3-2016';
            controller1.EditRecord();    
            controller1.save();
            controller1.getMilestoneSummary(); 
            controller1.cancel();    
            
            controller1.period = 'Q3-2016';
            controller1.EditRecord();    
            controller1.AddSnapshot();
            controller1.getProgramSnapshot();
            controller1.save();
            
        
        }         
    }
}