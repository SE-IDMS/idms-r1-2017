@isTest(SeeAllData=true)
public class SVMX_UpdateTeamExpertiseAndProdServcd_UT
{
    static TestMethod void myUnitTest()
    {
       
        SVMXC__Service_Group__c sg = new SVMXC__Service_Group__c();
        sg.name =' Service Group Test';
        sg.SVMXC__Active__c = true;
        insert sg;
        User user = Utils_TestMethods.createStandardUser('test');
        insert user;
        SVMXC__Service_Group_Members__c sgm = new SVMXC__Service_Group_Members__c();
        sgm.name  = 'Service Group Member Test';
        sgm.SVMXC__Service_Group__c = sg.id;
        sgm.SVMXC__Role__c = 'Schneider Employee';
        sgm.SESAID__c = 'SESA';
        sgm.SVMXC__Salesforce_User__c = userinfo.getUserId();
        sgm.PrimaryAddress__c = 'Home';
        sgm.SVMXC__Country__c = 'India';
        sgm.DepotStreet__c = 'DepotStreet';
        sgm.DepotZip__c = 'DepotZip';
       
        insert sgm;
        
        SVMXC__Skill__c  skill = new SVMXC__Skill__c();
        skill.SVMXC__Description__c  = 'Description ';
        skill.SVMXC__Skill_Category__c  = 'Installation';
        skill.Business_Unit__c = 'PW';
        insert skill;
        // SVMXC__Service_Group_Members__c tech = [Select Id from SVMXC__Service_Group_Members__c limit 1];
         
        
        //SVMXC__Skill__c sk = [select Id from SVMXC__Skill__c limit 1];
        SVMXC.INTF_WebServicesDef.INTF_TargetRecord request = new SVMXC.INTF_WebServicesDef.INTF_TargetRecord();
        SVMXC.INTF_WebServicesDef.INTF_TargetRecordObject rObject = new SVMXC.INTF_WebServicesDef.INTF_TargetRecordObject();
            rObject.objName = 'SVMXC__Service_Group_Members__c';
        List<SVMXC.INTF_WebServicesDef.INTF_Record> recordList = new List<SVMXC.INTF_WebServicesDef.INTF_Record>();
        
        List<SVMXC.INTF_WebServicesDef.INTF_StringMap> recordsAsKeyValue = new List<SVMXC.INTF_WebServicesDef.INTF_StringMap>();
        recordsAsKeyValue.add(new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMXC__Group_Member__c', sgm.Id));
        recordsAsKeyValue.add(new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMXC__Skill__c', skill.Id));
        recordsAsKeyValue.add(new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMXC__Availability_Start_Date__c', '2013-07-19'));
        recordsAsKeyValue.add(new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMXC__Availability_End_Date__c', '2013-07-19'));
        SVMXC.INTF_WebServicesDef.INTF_Record record = new SVMXC.INTF_WebServicesDef.INTF_Record();
        record.setTargetRecordAsKeyValue(recordsAsKeyValue);
        record.targetRecordId = sgm.Id;
        recordList.add(record);
        rObject.setRecords(recordList);
        request.setHeaderRecord(rObject);
        
        List<SVMXC.INTF_WebServicesDef.INTF_TargetRecordObject> detailRecords = new List<SVMXC.INTF_WebServicesDef.INTF_TargetRecordObject>();
        detailRecords.add(rObject);
        request.setDetailRecords(detailRecords); 
        
        SVMX_UpdateTeamOnExpertiseAndProdServcd.SVMX_SampleMethod(request);
    }
}