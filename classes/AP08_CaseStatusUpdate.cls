/*
    Author          : Ankit Badhani (ACN) 
    Date Created    : 01/02/2011
    Description     : This class is used to update the case status based on CaseID and workOrderNotification.
*/

public class AP08_CaseStatusUpdate{
    //List to hold the cases on which the work order notification is created.
    // CMD -1 private static Map<Id,Case> CasesToUpdate = new Map<Id,Case>();
    
/* @Method <This method updateCaseStatus is used to update the case status when work order
            notification is created for that particular case>
   @param <Taking set of caseId's on which work order notification is created>
   @return <void> - <Method not returning anything>
   @throws exception - <Method is not throwing any exception>
*/

    public static void updateCaseStatus(Set<Id> CaseIds){
        // CMD -1 Comment Reason - This Method is just being called, and updating the Case, No functionality
        
        // CMD -1 System.debug('Case Status updation on creation of WorkOrderNotification starts here');
        // CMD -1 for(Case caseObject : [SELECT Status,AnswerToCustomer__c FROM Case where Id IN :CaseIds]){
            //caseObject.status = Label.CL00230;
            //Commented as per DEF-3132, OCT13 Release, 30-Sep-2013
            /*
            if(caseObject.AnswerToCustomer__c!=null){
               // caseObject.AnswerToCustomer__c=caseObject.AnswerToCustomer__c+' '+Label.CL00266;    
                if (caseObject.AnswerToCustomer__c.Indexof(Label.CL00266) == -1) //DEF-3132, OCT13 Release
                    caseObject.AnswerToCustomer__c=caseObject.AnswerToCustomer__c+'<br />'+Label.CL00266; 
            }
            else{
                caseObject.AnswerToCustomer__c=Label.CL00266;   
            } */
            // CMD -1 CasesToUpdate.put(caseObject.id,caseObject);
        // CMD -1 }
        /* // CMD -1 
        if(CasesToUpdate.size()>0){
            if(CasesToUpdate.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                System.Debug('######## AP01 Error updating : '+ Label.CL00173);
            }
            else{
                Database.SaveResult[] lup = Database.update(CasesToUpdate.values(),true);
                for(Database.SaveResult up : lup)
                {
                    if(!up.isSuccess())
                    {
                        Database.Error err = up.getErrors()[0];
                        System.Debug('######## AP01 Error updating : '+err.getStatusCode()+' '+err.getMessage());
                    }
                }
            }
        }
        System.debug('Case Status updation on creation of WorkOrderNotification ends here');    
        // CMD -1 */
    }
 
  /***********************************************************************************
     Author          : Renjith Jose(GD ) 
     Date Created    : 22 August 2013
     Release         : OCT13 
     Description     : Automatically Close the Case when all the associated 
                       Work Order Notifications are Closed or Cancelled    
 **************************************************************************************/
 public static void CaseAutoClosure(List<WorkOrderNotification__c> lstNewWON, Map<Id,WorkOrderNotification__c> OldMapWON){
    
        System.Debug('######## CaseAutoClosure - Started - ####');
        Set<ID> CaseIds = new Set<ID>();
        String WONCloseStatusLabel = System.Label.CLOCT13CCC07;    
        List<String> lstCloseSts = WONCloseStatusLabel.split(',',0);
             
        String CaseClosureMsg = System.Label.CLOCT13CCC18;
        Set<ID> caseIDtoClose = new Set<ID>();

        for(WorkOrderNotification__c won :lstNewWON){
            String wonStatus = won.Work_Order_Status__c ;
                    
          if((wonStatus != null) && (WONCloseStatusLabel.Contains(wonStatus)) && (OldMapWON != null && OldMapWON.get(won.id).Work_Order_Status__c != wonStatus))
             CaseIds.add(won.case__c);        
     
        }
       System.Debug('######## CaseAutoClosure - '+CaseIds.size());

        if(CaseIds.size() > 0){
              List<WorkOrderNotification__c> lstWON = [Select Case__c from WorkOrderNotification__c WHERE Case__c in :caseIDs and Work_Order_Status__c not in :lstCloseSts];
      
            System.Debug('######## CaseAutoClosure - OpenWON :'+lstWON.size());        
            for(String caseno :caseIDs){
           // System.Debug('######## Started - CaseAutoClosure - Case ID :-> '+caseno);
               String CaseToClose = 'Yes';
                for(WorkOrderNotification__c won :lstWON){
                   if (won.Case__c == caseno) {
                      CaseToClose = 'No'; 
               //       System.Debug('######## Started - CaseAutoClosure - Open WON => '+won.id);
                      break;          
                    }
                }
                

                if( CaseToClose == 'Yes')
                   caseIDtoClose.add(caseno);           
    
            } 
        
             System.Debug('######## CaseAutoClosure - Number of Case to Close:- '+caseIDtoClose.size());
        
            if (caseIDtoClose.size() > 0) {
                 List<Case> lstCasetoClose = [SELECT id,AnswerToCustomer__c from Case WHERE ID in :caseIDtoClose and Status != 'Closed'];
                 //Changing Case status
                 if(lstCasetoClose.size() > 0) {
                    for(Case cCase :lstCasetoClose){
                        cCase.Status = 'Closed';
                        //if(String.isBlank(cCase.AnswerToCustomer__c.trim()))
                        if(cCase.AnswerToCustomer__c != null) 
                           // cCase.AnswerToCustomer__c = cCase.AnswerToCustomer__c+'<br />'+'Case has been Closed automatically.';
                            cCase.AnswerToCustomer__c = cCase.AnswerToCustomer__c+'<br />'+CaseClosureMsg; 
                        else
                           // cCase.AnswerToCustomer__c = 'Case has been Closed automatically.';
                           cCase.AnswerToCustomer__c = CaseClosureMsg;
                    }    
                    Database.SaveResult[] lstUpdateRecords = Database.update(lstCasetoClose,false);
            
                    for(Database.SaveResult updtRec :lstUpdateRecords){
                        if(!updtRec.isSuccess()){
                            Database.Error err = updtRec.getErrors()[0];
                            System.Debug('######## CaseAutoClosure Error : '+err.getStatusCode()+' '+err.getMessage());
                        }
                    }
                }   
             }

        }
        System.Debug('######## CaseAutoClosure - Completed - ####');
    }
    //----------------------------------------------------------------------------------------------------------
}