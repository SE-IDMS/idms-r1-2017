/*
    Author          : Srikant Joshi
    Date Created    : 11/07/2012
    Description     : Class for triggers Prog_ProgramBeforeUpdate.
                      Creates a list of Programs to be processed.
*/

public with sharing class Prog_ProgramTriggers {

    public static void budgetBeforeUpdate(List<DMTProgram__c> listProgramNew){
        Prog_ProgramUtils.updateTotalsFields(listProgramNew);
    }
        
}