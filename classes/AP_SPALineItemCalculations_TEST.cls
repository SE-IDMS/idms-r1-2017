@isTest
private class AP_SPALineItemCalculations_TEST
{

    static testMethod void runPositiveTestCases() {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,UserInfo.getUserId());
        insert param;
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        List<SPAQuotaQuarter__c> quotaquarters=Utils_SPA.createQuotaQuarters(quota.Id);
        insert quotaquarters;
        
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
        
        //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount1=Utils_SPA.createThresholdDiscount('HK_SAP','Final Multiplier');
        insert thresholddiscount1;
        
       SPAThresholdDiscount__c thresholddiscount2=Utils_SPA.createThresholdDiscount('HK_SAP','Compensation%');
        insert thresholddiscount2;
        
       //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        
        
        SPARequest__c sparequest2=Utils_SPA.createSPARequest('Advised Discount %',opp.id);
        insert sparequest2;
        SPARequestLineItem__c lineitem2=Utils_SPA.createSPARequestLineItem(sparequest2.id,'Advised Discount %');
        insert lineitem2;
        SPARequestLineItem__c lineitem3=Utils_SPA.createSPARequestLineItem(sparequest2.id,'Advised Discount %');
        insert lineitem3;
        
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Multiplier',opp.id);
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Multiplier');
        insert lineitem;

        insert Utils_SPA.createPricingDeskMembers(sparequest.BackOfficeSystemID__c,lineitem.LocalPricingGroup__c);
    }
    
}