Public class VFC_COINbFO_BCAChartByMonth {

        private string projectId;
        public Offer_Lifecycle__c offerlife{get;set;}
        public Set<string> setMProject = new Set<string>(); 
        public List<Milestone1_Project__c> listmProject{get;set;}
        public Milestone1_Project__c countryProj{get;set;}
        public Set<string> setYears = new Set<string>(); 
        public List<offerCycleForecastData> listWarpperData{get;set;}
        public List<offerCycleForecastData> listWarpperDataOld{get;set;}
        public static boolean blMontdisplay{get;set;}
        public DateTime forecastdate{get;set;}
        //Constructor 
        public VFC_COINbFO_BCAChartByMonth(ApexPages.StandardController controller) {
        
            projectId=System.currentPageReference().getParameters().get('id');
            //forecastdate = [select Forecast_Snapshot_Date__c from Milestone1_Project__c where Id =:projectId][0].Forecast_Snapshot_Date__c;
            //System.debug('$$$$'+forecastdate);
           
           Set<String> setProjFields = new Set<String>{'Name','TECH_IsForecastByMonth__c','Country_End_of_Commercialization_Date__c','TECH_IsForecastByYear__c','TECH_IsForecastByQuarter__c','Business_Dev__r.name','Forecast_Unit__c','Offer_Launch__c','Country_Announcement_Date__c','Country_Launch_Leader__r.name','TECH_IsForecastCreated__c','Forecast_Unit__c','Country__r.name','Family__c'};
            if (!Test.isRunningTest()){
            
                controller.addFields(new List<String>(setProjFields));
            }
            countryProj=(Milestone1_Project__c)controller.getRecord();
            listWarpperData = new List<offerCycleForecastData>();
            listWarpperDataOld = new  List<offerCycleForecastData>();
            if(countryProj.TECH_IsForecastByMonth__c==true) {
                 listWarpperData=getForecastData();
            }
            if(countryProj.TECH_IsForecastByYear__c==true) {
                listWarpperData=chartdisplayByYear();
            }
            if(countryProj.TECH_IsForecastByQuarter__c==true) {
                listWarpperData=chartDisplayByQuarter();//chartdisplayByYear();
            }
            
            if(listWarpperData.size() > 0 && listWarpperData!=null) {
                blMontdisplay=true;
            }
            
            if(listWarpperDataOld.size() > 0 && listWarpperDataOld!=null) {
                blMontdisplay=true;
            }
            

        }

        public  List<AggregateResult>  listOfferCycleResults() {     
            List<AggregateResult> aggresults = [SELECT SUM(Actual__c) act,SUM(BU__c)bu,SUM(Country__c)cout,Month__c,ForcastYear__c FROM OfferLifeCycleForecast__c where Project__c =:projectId and Month__c!='Total by Year' GROUP BY  ForcastYear__c,Month__c];
            system.debug('Test------->'+aggresults);
            return aggresults;
        }
        
        Public Map<string,OfferLifeCycleForecast__c> MapofferCycleResults() {
             
            Map<string,OfferLifeCycleForecast__c> mapOffer= new Map<string,OfferLifeCycleForecast__c>();
            //ByMonthChart 
            List<OfferLifeCycleForecast__c> listOffResults = [SELECT Actual__c ,BU__c,Country__c,Month__c,ForcastYear__c FROM OfferLifeCycleForecast__c where Project__c =:projectId and Month__c!='Total by Year' ];
            system.debug('Test--->'+listOffResults );
            
            for(OfferLifeCycleForecast__c offObj:listOffResults ) {
                mapOffer.put(offObj.Month__c+offObj.ForcastYear__c,offObj); 
                setYears.add(offObj.ForcastYear__c);
            }       
            
            return mapOffer;
        
        }
        
        public List<offerCycleForecastData> getForecastData() {
            Integer offcount = 0;
            Integer strStartYear;
            Integer Inmonth;
            Integer inmonthFirstYr;
             Map<string,OfferLifeCycleForecast__c> mapOfferForecast= new Map<string,OfferLifeCycleForecast__c>();
            List<offerCycleForecastData> theResults = new List<offerCycleForecastData>();
            if(countryProj.Country_Announcement_Date__c!=null){
                strStartYear =countryProj.Country_Announcement_Date__c.year();
                Inmonth =countryProj.Country_Announcement_Date__c.month();
            
           
            Set<String> setListYears = new Set<String> (); 
            List<String> listListYears = new list<String> (); 
            Integer forCastYears = 0;
            string stryear;
            List<AggregateResult> listCycleResults = new List<AggregateResult>();
            
            listCycleResults =listOfferCycleResults();
            mapOfferForecast=MapofferCycleResults(); 
            
            for(integer i=0; i< setYears.size() ;i++) {
               
                forCastYears = strStartYear  + i;
               
                
                stryear=(forCastYears).format().replace(',','');
                if (stryear.length()==5) {
                    stryear = stryear.substring(0,1)+ stryear.substring(2);
                }
                
                setListYears.add(stryear);
                listListYears.add(stryear);
            }   
            
           
            
            if(mapOfferForecast.size() > 0 ) {
                blMontdisplay=true;
            }
               
            
            for(string stryr:listListYears) {
                if(stryr==string.valueof(strStartYear)) {
                    inmonthFirstYr=Inmonth; 
                }else {
                    inmonthFirstYr=1;
                } 
                  for (Integer month = inmonthFirstYr ; month <= MONTHS_OF_YEAR.size() ; month++) {
                      offerCycleForecastData  offerCyclewarp=null;
                          System.debug('Hanu--------->'+monthName(month)+stryr);
                         
                              if(offcount < mapOfferForecast.size()) {
                              if(mapOfferForecast.containsKey(monthName(month)+stryr)) {
                                  if(stryr==mapOfferForecast.get(monthName(month)+stryr).ForcastYear__c) {
                                    string prastMonth = string.valueof(mapOfferForecast.get(monthName(month)+stryr).month__c.substring(0,3));
                                            System.debug('TestBp--------->'+prastMonth);
                                        //if(monthName(month)==prastMonth)  {
                                            offerCyclewarp = new offerCycleForecastData(mapOfferForecast.get(monthName(month)+stryr).month__c.substring(0,3)+stryr);
                                            
                                            offerCyclewarp.forCstBU=mapOfferForecast.get(monthName(month)+stryr).BU__c;
                                            offerCyclewarp.forCstCountry =mapOfferForecast.get(monthName(month)+stryr).Country__c;
                                            offerCyclewarp.forCstActual =mapOfferForecast.get(monthName(month)+stryr).Actual__c;
                                           
                                            if(offerCyclewarp.forCstCountry!=0 && offerCyclewarp.forCstCountry!=null && offerCyclewarp.forCstActual!=0 && offerCyclewarp.forCstActual!=null) {
                                                offerCyclewarp.monthlyRevenue =(100 * offerCyclewarp.forCstActual) / offerCyclewarp.forCstCountry;
                                            }
                                            offerCyclewarp.RevenuePercent=string.valueof(offerCyclewarp.monthlyRevenue)+'%';
                                             System.debug('Test-offerCyclewarp--------->'+offerCyclewarp.monthlyRevenue);
                                            offcount++;
                                        //}
                                    }   
                           
                              }
                          
                          if(offerCyclewarp != null) {

                            theResults.add(offerCyclewarp);

                        }
                        
                    }
                }
           }
           }
           //For RAMDOWN
           if(countryProj.Country_End_of_Commercialization_Date__c!=null){
                strStartYear =countryProj.Country_End_of_Commercialization_Date__c.year();
                Inmonth =countryProj.Country_End_of_Commercialization_Date__c.month();
            
           
            Set<String> setListYears = new Set<String> (); 
            List<String> listListYears = new list<String> (); 
            Integer forCastYears = 0;
            string stryear;
            List<AggregateResult> listCycleResults = new List<AggregateResult>();
            
            listCycleResults =listOfferCycleResults();
            mapOfferForecast=MapofferCycleResults(); 
            
            for(integer i=0; i< setYears.size() ;i++) {
               
                forCastYears = strStartYear  - i;
               
                
                stryear=(forCastYears).format().replace(',','');
                if (stryear.length()==5) {
                    stryear = stryear.substring(0,1)+ stryear.substring(2);
                }
                
                setListYears.add(stryear);
                listListYears.add(stryear);
            }   
            
           
            
            if(mapOfferForecast.size() > 0 ) {
                blMontdisplay=true;
            }
            listListYears.sort();
           for(string stryr:listListYears) {
                if(stryr==string.valueof(strStartYear)) {
                    inmonthFirstYr=Inmonth; 
                     for (Integer month = inmonthFirstYr ; month <= MONTHS_OF_YEAR.size() ; month--) {
                        offerCycleForecastData  offerCyclewarp=null;
                         
                        if(month!=0)  {
                            if(offcount < mapOfferForecast.size()) {
                                if(mapOfferForecast.containsKey(monthName(month)+stryr)) {
                                    if(stryr==mapOfferForecast.get(monthName(month)+stryr).ForcastYear__c) {
                                        string prastMonth = string.valueof(mapOfferForecast.get(monthName(month)+stryr).month__c.substring(0,3));
                                        System.debug('TestBp--------->'+prastMonth);
                                        
                                        offerCyclewarp = new offerCycleForecastData(mapOfferForecast.get(monthName(month)+stryr).month__c.substring(0,3)+stryr);

                                        offerCyclewarp.forCstBU=mapOfferForecast.get(monthName(month)+stryr).BU__c;
                                        offerCyclewarp.forCstCountry =mapOfferForecast.get(monthName(month)+stryr).Country__c;
                                        offerCyclewarp.forCstActual =mapOfferForecast.get(monthName(month)+stryr).Actual__c;

                                        if(offerCyclewarp.forCstCountry!=0 && offerCyclewarp.forCstCountry!=null && offerCyclewarp.forCstActual!=0 && offerCyclewarp.forCstActual!=null) {
                                            offerCyclewarp.monthlyRevenue =(100 * offerCyclewarp.forCstActual) / offerCyclewarp.forCstCountry;
                                        }
                                        offerCyclewarp.RevenuePercent=string.valueof(offerCyclewarp.monthlyRevenue)+'%';
                                        System.debug('Test-offerCyclewarp--------->'+offerCyclewarp.monthlyRevenue);
                                        offcount++;
                                        
                                    }   

                                }                 
                                if(offerCyclewarp != null) {
                                theResults.add(offerCyclewarp);
                                }

                            }
                        }else  {
                            break;
                            system.debug('TEST Break');
                        }
                    }
                }else {
                    inmonthFirstYr=1;
                     for (Integer month = inmonthFirstYr ; month <= MONTHS_OF_YEAR.size() ; month++) {
                        offerCycleForecastData  offerCyclewarp=null;
                          System.debug('Hanu--------->'+monthName(month)+stryr);
                         
                            if(offcount < mapOfferForecast.size()) {
                                if(mapOfferForecast.containsKey(monthName(month)+stryr)) {
                                    if(stryr==mapOfferForecast.get(monthName(month)+stryr).ForcastYear__c) {
                                        string prastMonth = string.valueof(mapOfferForecast.get(monthName(month)+stryr).month__c.substring(0,3));
                                            System.debug('TestBp--------->'+prastMonth);
                                        //if(monthName(month)==prastMonth)  {
                                            offerCyclewarp = new offerCycleForecastData(mapOfferForecast.get(monthName(month)+stryr).month__c.substring(0,3)+stryr);
                                            
                                            offerCyclewarp.forCstBU=mapOfferForecast.get(monthName(month)+stryr).BU__c;
                                            offerCyclewarp.forCstCountry =mapOfferForecast.get(monthName(month)+stryr).Country__c;
                                            offerCyclewarp.forCstActual =mapOfferForecast.get(monthName(month)+stryr).Actual__c;
                                           
                                            if(offerCyclewarp.forCstCountry!=0 && offerCyclewarp.forCstCountry!=null && offerCyclewarp.forCstActual!=0 && offerCyclewarp.forCstActual!=null) {
                                                offerCyclewarp.monthlyRevenue =(100 * offerCyclewarp.forCstActual) / offerCyclewarp.forCstCountry;
                                            }
                                            offerCyclewarp.RevenuePercent=string.valueof(offerCyclewarp.monthlyRevenue)+'%';
                                             System.debug('Test-offerCyclewarp--------->'+offerCyclewarp.monthlyRevenue);
                                            offcount++;
                                        //}
                                    }   
                           
                                }   
                          
                                if(offerCyclewarp != null) {
                                theResults.add(offerCyclewarp);
                                }
                        
                            }
                    }
                } 
                 
            }
                
                 System.debug('Test---------->'+theResults);
        
            
           
            } return theResults;
        }
        
        public List<offerCycleForecastData> chartdisplayByYear() {
            
            List<offerCycleForecastData> theResultsYeaer = new List<offerCycleForecastData>();
            Map<string,OfferLifeCycleForecast__c> mapOffer= new Map<string,OfferLifeCycleForecast__c>();
            //ByMonthChart 
            List<OfferLifeCycleForecast__c> listOffResults = [SELECT Actual__c ,BU__c,Country__c,Month__c,ForcastYear__c FROM OfferLifeCycleForecast__c where Project__c =:projectId and Month__c='Total by Year' ];
            system.debug('Test--->'+listOffResults );
            
            for(OfferLifeCycleForecast__c offObj:listOffResults) {
                mapOffer.put(offObj.ForcastYear__c,offObj); 
                setYears.add(offObj.ForcastYear__c);
            }    
            //sorting years 
            List<String> yearList = new List<String>();
            yearList.addAll(mapOffer.keySet());
            yearList.sort();
            for(string yearStr:yearList) {
                //for(OfferLifeCycleForecast__c projetForstObj:mapOffer.values()) {
                    OfferLifeCycleForecast__c projetForstObj=mapOffer.get(yearStr);
                    offerCycleForecastData  projectForecYearWarp=null;
                    projectForecYearWarp = new offerCycleForecastData(projetForstObj.ForcastYear__c);
                    projectForecYearWarp.forCstBU =projetForstObj.BU__c;
                    projectForecYearWarp.forCstCountry=projetForstObj.country__c;          
                    projectForecYearWarp.forCstActual=projetForstObj.Actual__c;
                    if(projectForecYearWarp.forCstCountry!=0 && projectForecYearWarp.forCstCountry!=null && projectForecYearWarp.forCstActual!=0 && projectForecYearWarp.forCstActual!=null) {
                        projectForecYearWarp.monthlyRevenue =(100 * projectForecYearWarp.forCstActual) / projectForecYearWarp.forCstCountry;
                    }
                    theResultsYeaer.add(projectForecYearWarp);
                //}
            }
            
            return theResultsYeaer;
        
        }
        
        public List<offerCycleForecastData> chartDisplayByQuarter() {
            List<offerCycleForecastData> theResultsYeaer = new List<offerCycleForecastData>();
            
            Map<string,OfferLifeCycleForecast__c> mapOfferQuarter= new Map<string,OfferLifeCycleForecast__c>();
            
            //ByQuarterChart 
            List<OfferLifeCycleForecast__c> listOffResults = [SELECT Actual__c ,BU__c,ForecastQuarter__c,Country__c,Month__c,ForcastYear__c FROM OfferLifeCycleForecast__c where Project__c =:projectId and Month__c=null];
            system.debug('Test--->'+listOffResults );
            
            for(OfferLifeCycleForecast__c forcastObj:listOffResults) {
                
                if(mapOfferQuarter.containskey(forcastObj.ForcastYear__c+forcastObj.ForecastQuarter__c)) {
                    OfferLifeCycleForecast__c offCyObj=mapOfferQuarter.get(forcastObj.ForcastYear__c+forcastObj.ForecastQuarter__c);
                    offCyObj.Actual__c=offCyObj.Actual__c+forcastObj.Actual__c;
                    offCyObj.Country__c=offCyObj.Country__c+forcastObj.Country__c;
                    offCyObj.BU__c =offCyObj.BU__c+forcastObj.BU__c;
                    offCyObj.ForecastQuarter__c=forcastObj.ForecastQuarter__c;
                    offCyObj.ForcastYear__c=forcastObj.ForcastYear__c;
                     system.debug('Test-offCyObj-->'+offCyObj );
                    mapOfferQuarter.put(forcastObj.ForcastYear__c+forcastObj.ForecastQuarter__c,offCyObj);

                }else {
                    mapOfferQuarter.put(forcastObj.ForcastYear__c+forcastObj.ForecastQuarter__c,forcastObj);
                    system.debug('Test-offCyObj-2->'+mapOfferQuarter );

                }
               
                setYears.add(forcastObj.ForcastYear__c);
            }  
            //sorting years 
            List<String> quarterList = new List<String>();
            quarterList.addAll(mapOfferQuarter.keySet());
            quarterList.sort();
            
            for(string strQuarter:quarterList) {
                //for() {
                    OfferLifeCycleForecast__c projetForstObj=mapOfferQuarter.get(strQuarter);
                    offerCycleForecastData  projectForecYearWarp=null;
                    projectForecYearWarp = new offerCycleForecastData(projetForstObj.ForecastQuarter__c+projetForstObj.ForcastYear__c);
                    projectForecYearWarp.forCstBU =projetForstObj.BU__c;
                    projectForecYearWarp.forCstCountry=projetForstObj.country__c;          
                    projectForecYearWarp.forCstActual=projetForstObj.Actual__c;
                    if(projectForecYearWarp.forCstCountry!=0 && projectForecYearWarp.forCstCountry!=null && projectForecYearWarp.forCstActual!=0 && projectForecYearWarp.forCstActual!=null) {
                        projectForecYearWarp.monthlyRevenue =(100 * projectForecYearWarp.forCstActual) / projectForecYearWarp.forCstCountry;
                    }
                    theResultsYeaer.add(projectForecYearWarp);
                //}
            }
           // theResultsYeaer.sort();
            return theResultsYeaer;
        
        
        
        
        }

       

       // To Get the English month string

        private final List<String> MONTHS_OF_YEAR = new String[]{'January','February','March','April','May','June','July','August','September','October','November','December'};


        private String monthName(Integer monthNum) {

            return(MONTHS_OF_YEAR[monthNum - 1]);

        }

        public class offerCycleForecastData {

            public String theMonth { get; set; }
            public decimal forCstBU { get; set; }
            public decimal forCstCountry { get; set; }
            public decimal forCstActual{ get; set; }
            public Double monthlyRevenue { get; set; }
            public string RevenuePercent{ get; set; }


            public offerCycleForecastData (String mon) {

                this(mon, 0, 0, 0,0,'0');
            }

            public offerCycleForecastData(String mon, decimal BU, decimal Ctry, decimal CstActual,Double monthRev,string monthVal ) {

            this.theMonth = mon;
            this.forCstBU = Bu;
            this.forCstCountry = Ctry;
            this.forCstActual =CstActual;
            this.monthlyRevenue = monthRev;
            this.RevenuePercent=monthVal ;

            }
        }   
    }