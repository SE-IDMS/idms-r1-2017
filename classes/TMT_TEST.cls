@isTest
Private class TMT_TEST
{
    public static testMethod void TMT_TestMethod() 
    {
        // Create a Trainee profile
        // Profile p = [SELECT Id FROM profile WHERE name='SE - Trainee'];        
        User T1 = new User(alias = 'newUser', email='newuser@testorg.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', // profileid = p.Id,
        timezonesidkey='America/Los_Angeles', username='newuser@testorg456.com');
        
        // Create a Curriculum
        TMT_Curriculum__c Curriculum = new TMT_Curriculum__c(Name='Training', Duration__c=18);
        TMT_Location__c Location = new TMT_Location__c(Name='Place', Address__c='55 rue Louis Treize',City__c='PARIS');
        
        Database.SaveResult CSR = Database.insert(Curriculum);       
        Database.SaveResult LSR = Database.insert(Location);
        
        
        // Create a Session
        TMT_Session__c Session = new TMT_Session__c(Notify__c=false, Curriculum__c=Curriculum.id, Location__c=Location.id, Start__c=date.today(), End__c=datetime.newInstance(2013, 12, 1), max__c=55);
        Database.SaveResult SSR = Database.insert(Session);
        
        // Create related Registration
        List<TMT_Registration__c> Registrations = new List<TMT_Registration__c>();
        For(Integer i=0; i<10; i++)
        {
            TMT_Registration__c R = new TMT_Registration__c(Session__c = Session.id, Trainee__c = UserInfo.getUserId());
            Registrations.add(R);
        }
        
        Registrations[0].Status__c = 'Fail';
        Registrations[1].Status__c = 'Pass';
        Registrations[2].Status__c = 'Pass';
        
        List<Database.SaveResult> RSR = Database.insert(Registrations);
        
        // ***********
        // Start Test
        
         Test.StartTest();
         
        //Active the registration to notify users
        Session.Notify__c=true;
        Database.SaveResult SSR1b = Database.update(Session);
         
        // Reschedule the session
        Session.Start__c=date.today()+1;
        Database.SaveResult SSR2 = Database.update(Session);
        
        // Cancel a registration
        Database.deleteResult RDR = Database.delete(Registrations[8]); 
                   
        Test.setCurrentPage(Page.VFP23_SessionReview);
        ApexPages.StandardController SessionController = new ApexPages.StandardController(Session);
      
        VFC02_Session ReviewController = new VFC02_Session(SessionController);
        
       
        // Test Standard review functions
        
        // Get Regisrations twice to verify that same registrations are not displayed twice
        ReviewController.GetRegistrations();
        ReviewController.GetRegistrations();
 
        ReviewController.getSession();
        ReviewController.getCurriculum();
        ReviewController.getEmpty();
        ReviewController.getRegilist();
              
        // User checks "Fail" for the first registration    
        ReviewController.RegiList[0].attended=true;
        ReviewController.getAttended();
        
        // User checks "Pass" for the first registration 
        ReviewController.RegiList[0].passed=true;
        ReviewController.getPassed();
        
        // User checks "Fail" for the second registration    
        ReviewController.RegiList[1].passed=true;
        ReviewController.getPassed();
        
         // User checks "Fail" for the fourst registration    
        ReviewController.RegiList[4].attended=true;
        ReviewController.getAttended();
        
  
        ReviewController.RegiList[6].Attended=true;
        ReviewController.RegiList[5].passed=true;
        
        ReviewController.Save();
        ReviewController.getSizeR();
        ReviewController.Cancel();
        
        // Delete the Session
        Database.deleteResult SDR = Database.delete(Session);
        
        Test.StopTest();
        
        // End Test
        // **********
    }
}