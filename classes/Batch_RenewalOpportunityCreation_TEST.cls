/**
Deepak
 */
@isTest
private class Batch_RenewalOpportunityCreation_TEST {

       
    public static testMethod void myUnitTest() {
        
       User user = new User(alias = 'user', email='user' + '@Schneider-electric.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = System.label.CLAPR15SRV37, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'SVMX10;SVMX_InstalledProduct2;SVMX22;SRV_APR_001;SVMX18;AP_WorkOrderTechnicianNotification;SRV_APR_001;SVMX16;SVMX14;SVMX19;AP30;AP_ACC_PartnerAccountUpdate',
                timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = 'SESA000000',SESAExternalID__c ='SESA000000');
                insert user;
 
    system.runAs(user){
            
    
       //slrt4 = [select id from recordtype where DeveloperName = 'ServiceLine' limit 1];
        //recordtype scrt4 = [select id from recordtype where DeveloperName = 'ServiceContract' limit 1];

        String Amendment = System.Label.CLAPR15SRV26;
         //String ServiceContractRT = System.Label.CLAPR15SRV01;
         String ServiceContractRT = System.Label.CLAPR15SRV01;
        //A country
        Country__c testCountry = Utils_TestMethods.createCountry();
            insert testCountry;
        //User 
        string yyxxt;
        User us =Utils_TestMethods.createStandardUser('yyxxt');
        insert us;
        //An Account     
         Account testAccount = Utils_TestMethods.createAccount(us.Id, testCountry.Id);
            testAccount.Name = 'Test';
            testAccount.MarketSegment__c='ID5';
            testAccount.MarketSubSegment__c='B43';
            insert testAccount;
            
         Account testAccount3 = Utils_TestMethods.createAccount(us.Id, testCountry.Id);
            testAccount3.Name = 'Test3';
            testAccount3.MarketSegment__c='ID5';
            testAccount3.MarketSubSegment__c='B43';
            insert testAccount3;    
            
        Account testAccount2 = Utils_TestMethods.createAccount(us.Id, testCountry.Id);
            testAccount2.Name = 'Test2';
            insert testAccount2;
        
        //A Contact              
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
            testContact.Country__c = testCountry.Id;
            insert testContact;
        
       
        
        
         Opportunity opp = new Opportunity();
            DateTime dT = System.now();
            opp.Probability =0;
            opp.StageName = '2 - Define Opportunity Portfolio';
            opp.OpportunityCategory__c ='Simple';
            opp.OpptyPriorityLevel__c='Standard';
            opp.IncludedInForecast__c = 'Yes';
            opp.MarketSegment__c=testAccount.MarketSegment__c;
            opp.MarketSubSegment__c=testAccount.MarketSubSegment__c;
            opp.StageName = '7 – Deliver & Validate';//Custom Label
            opp.Probability =100;
            opp.Status__c ='Won - Deliver & Validate by Schneider';//Custom Label
            opp.name = 'test_opportunity';
            opp.CloseDate = date.today() +30;
            //opp.ServiceContract__c = testContract0.Id;
            
            insert opp;
        
        //Service Contract
        list<SVMXC__Service_Contract__c> sclist = New list<SVMXC__Service_Contract__c>();
        set<Id> scid = new set<Id>();
        set<Id> opidset = new set<Id>();
        SVMXC__Service_Contract__c testContract0 = new SVMXC__Service_Contract__c();
            testContract0.SVMXC__Company__c = testAccount.Id;
            testContract0.SoldtoAccount__c=testAccount2.Id;
            testContract0.Name = 'Test Contract0';  
            testContract0.SVMXC__Contact__c = testContact.Id  ;    
            testContract0.SVMXC__Start_Date__c = system.today();
            testContract0.SVMXC__End_Date__c= system.today() + 10;
            testContract0.SVMXC__Renewed_From__c = null;
            testContract0.OpportunitySource__c =Amendment;
            testContract0.LeadingBusinessBU__c ='BD';
            testContract0.DefaultInstalledAtAccount__c=testAccount3.Id;
            testContract0.Tech_ContractCancel__c = true; 
            testContract0.SVMXC__Active__c   = false;
            testContract0.Tech_RenewalOpportunityCreat__c = true;
            testContract0.EarliestEndDate__c = Date.Today();
            testContract0.SVMXC__Sales_Rep__c = us.Id;
            testContract0.OpportunityType__c = 'Standard';
            testContract0.SendCustomerRenewalNotification__c = true;
            testContract0.IsEvergreen__c = true;
            testContract0.RecordTypeId =ServiceContractRT;
            testContract0.LatestRenewalOpportunity__c = opp.Id;
            //OpportunitySource__c   
            //sclist.add(testContract0);
            
            insert testContract0 ;
            
            list<SVMXC__Service_Contract__c> sclisttoupdate = New list<SVMXC__Service_Contract__c>();
            
        /*
            for(SVMXC__Service_Contract__c sc :sclist){
                  sc.LatestRenewalOpportunity__c = opp.Id;
                  sc.OpportunitySource__c ='';
                  sclisttoupdate.add(sc);
            }
            
            if(sclisttoupdate.size()>0)
                update sclisttoupdate;*/
           Test.startTest();
        
            Batch_RenewalOpportunityCreation scbatch  = new Batch_RenewalOpportunityCreation();
            ID batchprocessid = Database.executeBatch(scbatch);
            
            Test.stopTest();
         }
    }
}