/**
    Author          : Bhuvana Subramaniyan (ACN) 
    Date Created    : 03/01/2010 
    Description     : Test Class for RIT_Utils_TestMethods.
*/
@isTest
private class RIT_Utils_TestMethods_TEST
{
    //Test Method
    static testMethod void testUtils()
    {
        //setup Test Data
        Account acc = RIT_Utils_TestMethods.createAccount();
        insert acc;
        acc = [select id from Account where Id=:acc.Id];
        
        //inserts Opportunity
        Opportunity opp = RIT_Utils_TestMethods.createOpportunity(acc.Id);
        insert opp;
        opp = [select id from Opportunity where Id =: opp.Id];
        
        //inserts QuoteLink
        OPP_QuoteLink__c quoteLink = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
        insert quoteLink;        
        quoteLink = [select id from OPP_QuoteLink__c where Id=:quoteLink.Id];
        
        //inserts QuoteLink
        OPP_QuoteLink__c quoteLink1 = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
        insert quoteLink1;        
        quoteLink1 = [select id from OPP_QuoteLink__c where Id=:quoteLink1.Id];
        
        //inserts Financial Environment record for quoteLink 
        RIT_FinancialEnv__c finEnv = RIT_Utils_TestMethods.createFinEnv(quoteLink.Id);
        insert finEnv;
        finEnv = [select id from RIT_FinancialEnv__c where id=:finEnv.Id];
        
        //inserts Contractual Environment
        RIT_ContractualEnv__c conEnv = RIT_Utils_TestMethods.createConEnv(quoteLink.Id);
        insert conEnv ;
        conEnv = [select id from RIT_ContractualEnv__c where id=:conEnv.Id];

        //inserts Liquidated Damages
        RIT_LiquidatedDamages__c liqDam= RIT_Utils_TestMethods.createLiqDam(conEnv.Id);
        insert liqDam;
        liqDam= [select id from RIT_LiquidatedDamages__c where id=:liqDam.Id];

        //inserts Project Information
        RIT_ProjectInformation__c projInfo= RIT_Utils_TestMethods.createProjInfo(quoteLink.Id);
        insert projInfo;
        projInfo=  [select id from RIT_ProjectInformation__c where id=:projInfo.Id];
        
        //inserts Project Information with Consortium
        projInfo= RIT_Utils_TestMethods.createProjInfowithConsort(quoteLink.Id);
        insert projInfo;
        projInfo=  [select id from RIT_ProjectInformation__c where id=:projInfo.Id];
        
        //inserts Techical & Manufacturing
        RIT_TechManScope__c techManf= RIT_Utils_TestMethods.createTechManf(quoteLink.Id);
        insert techManf;
        techManf=  [select id from RIT_TechManScope__c where id=:techManf.Id];
        
        //inserts Commercial & Competitive Environment
        RIT_CommCompEnv__c commComp= RIT_Utils_TestMethods.createCommComp(quoteLink.Id);
        insert commComp;
        commComp=  [select id from RIT_CommCompEnv__c where id=:commComp.Id];
        
         //inserts Project Information
        RIT_ProjectManagement__c projMgnt = RIT_Utils_TestMethods.createProjMgnt(quoteLink.Id);
        insert projMgnt;
        projMgnt =  [select id from RIT_ProjectManagement__c where id=:projMgnt.Id];
        
        //inserts Bank Guarantees for Financial Environment
        RIT_BankGuarantee__c bankGuarantees = RIT_Utils_TestMethods.createBankGuar(finEnv.Id);
        insert bankGuarantees;
        bankGuarantees =  [select id from RIT_BankGuarantee__c where id=:bankGuarantees.Id];
        
        //inserts Budget
        RIT_Budget__c budget= RIT_Utils_TestMethods.createbudget(quoteLink.Id);
        insert budget;
        budget=  [select id from RIT_Budget__c where id=:budget.Id];

        //inserts Budget Line
        RIT_BudgetLine__c budgetline= RIT_Utils_TestMethods.createbudgetLine(budget.Id,'PW');
        insert budgetLine;
        
        //inserts Terms and Means of Payment for Financial Environment
        RIT_TermsAndMeansOfPayment__c termsAndMeans = RIT_Utils_TestMethods.createTermsAndMeans(finEnv.Id);
        insert termsAndMeans;
        termsAndMeans =  [select id from RIT_TermsAndMeansOfPayment__c where id=:termsAndMeans.Id];
        
         //inserts Sub Contracters & OG Suppliers
        RIT_SubContractOGSupp__c subContract= RIT_Utils_TestMethods.createSubContr(quoteLink.Id);
        insert subContract;
        subContract=  [select id from RIT_SubContractOGSupp__c where id=:subContract.Id];
        
        //inserts Risk for Project Management
        RIT_Risks__c risk =  RIT_Utils_TestMethods.createProjMgntRisks(quoteLink.Id,projMgnt.Id);
        insert risk ;
        risk =  [select id from RIT_Risks__c where id=:risk.Id];
        
        //inserts Risk for Comm & Comp
        RIT_Risks__c risk1 =  RIT_Utils_TestMethods.createCommCompRisks(quoteLink.Id,commComp.Id);
        insert risk1 ;
        risk1 =  [select id from RIT_Risks__c where id=:risk1.Id];
        
        //inserts Risk for Tech Manf
        RIT_Risks__c risk2 =  RIT_Utils_TestMethods.createTechManfRisks(quoteLink.Id,techManf.Id);
        insert risk2 ;
        risk2 =  [select id from RIT_Risks__c where id=:risk2.Id];
        
        //inserts Risk for Sub Contracters
        RIT_Risks__c risk3 =  RIT_Utils_TestMethods.createSubContractersRisks(quoteLink.Id,subContract.Id);
        insert risk3 ;
        risk3 =  [select id from RIT_Risks__c where id=:risk3.Id];
        
        //inserts Risk for Financial Env
        RIT_Risks__c risk4 =  RIT_Utils_TestMethods.createFinEnvRisks(quoteLink.Id,finEnv.Id);
        insert risk4 ;
        risk4 =  [select id from RIT_Risks__c where id=:risk4.Id];
        
        //inserts Risk for COntractual Env
        RIT_Risks__c risk5 =  RIT_Utils_TestMethods.createConEnvRisks(quoteLink.Id,conEnv.Id);
        insert risk5 ;
        risk5 =  [select id from RIT_Risks__c where id=:risk5.Id];
                
        //insert Note
        Note note= RIT_Utils_TestMethods.createNotes(finEnv.Id);
        insert note;
        note=  [select id from Note  where id=:note.Id];
        
        //inserts Budget History
        RIT_Budget__History budHist = RIT_Utils_TestMethods.createBudgetHistory(budget.Id,'Sales__c');
        insert budHist;        
        budHist = [select ParentId,Field from RIT_Budget__History where parentId=:budget.Id];
        
    }
}