/*
    Author          : Bhuvana Subramaniyan 
    Description     : Test Class for VFC_LightRITEFormRedirect.
*/
@isTest
private class VFC_LightRITEFormRedirect_TEST
{
        static testMethod void lightRITETest()
        {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp2;

        //Inserts the Product Lines        
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            prodLines.add(pl1);
        }
        Database.SaveResult[] lsr = Database.Insert(prodLines);      
        
        OPP_QuoteLink__c  quoteLink = Utils_TestMethods.createQuoteLink(opp2.Id);
        quoteLink.TypeOfOffer__c = Label.CLAPR14SLS14; 
        quoteLink.Amount__c = 100;
        insert quoteLink;
        
        IPOLightRITE__c bof = Utils_TestMethods.createlightRITE(quoteLink.Id);
        insert bof;

        ApexPages.StandardController controller = new ApexPages.StandardController(quoteLink);
        VFC_LightRITEFormRedirect lightRITERedirect = new VFC_LightRITEFormRedirect(controller);
        lightRITERedirect.lightRITEForm();
        
        OPP_QuoteLink__c  quoteLink1 = Utils_TestMethods.createQuoteLink(opp2.Id);
        quoteLink1.TypeOfOffer__c = Label.CLAPR14SLS14; 
        quoteLink1.Amount__c = 100;
        insert quoteLink1;
        
        ApexPages.StandardController controller1 = new ApexPages.StandardController(quoteLink1);
        VFC_LightRITEFormRedirect lightRITERedirect1 = new VFC_LightRITEFormRedirect(controller1);
        lightRITERedirect1.lightRITEForm();
        
        OPP_QuoteLink__c  quoteLink2 = Utils_TestMethods.createQuoteLink(opp2.Id);
        quoteLink2.TypeOfOffer__c = Label.CLAPR14SLS10; 
        insert quoteLink2;
        
        ApexPages.StandardController controller2 = new ApexPages.StandardController(quoteLink2);
        VFC_LightRITEFormRedirect lightRITERedirect2 = new VFC_LightRITEFormRedirect(controller2);
        lightRITERedirect2.lightRITEForm();
        
        }
 
        
}