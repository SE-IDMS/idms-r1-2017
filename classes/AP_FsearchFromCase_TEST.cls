@IsTest 
public class AP_FsearchFromCase_TEST {
    
    @IsTest
    public static void testexcludeContextTypes() {
        AP_FsearchFromCase fsc = new AP_FsearchFromCase();
        System.assert(fsc.excludeContextTypes() != null);
        
    }
    
    @IsTest
    public static void testonRender() {
        AP_FsearchFromCase fsc = new AP_FsearchFromCase();
        
        // Set some application context data in a Map
        Map<String,String> appValues = new Map<String,String>();
        appValues.put(Canvas.Test.KEY_NAMESPACE,'alternateNamespace');
        appValues.put(Canvas.Test.KEY_VERSION,'3.0');
        // Set some environment context data in a MAp
        Map<String,String> envValues = new Map<String,String>();
        envValues.put(Canvas.Test.KEY_DISPLAY_LOCATION,'Chatter');
        envValues.put(Canvas.Test.KEY_LOCATION_URL,Label.CL10071+'/_ui/core/chatter/ui/ChatterPage');
        envValues.put('caseSubject','Subject');
        // Create a mock RenderContext using the test application and environment context data Maps
        Canvas.RenderContext mock = Canvas.Test.mockRenderContext(appValues,envValues);
        
        
        
        fsc.onRender(mock);
        System.assert(true);
    }
    
    @IsTest
    public static void testonRenderwithcaseid() {
        AP_FsearchFromCase fsc = new AP_FsearchFromCase();
        
        // Set some application context data in a Map
        Map<String,String> appValues = new Map<String,String>();
        appValues.put(Canvas.Test.KEY_NAMESPACE,'alternateNamespace');
        appValues.put(Canvas.Test.KEY_VERSION,'3.0');
        // Set some environment context data in a MAp
        Map<String,String> envValues = new Map<String,String>();
        envValues.put(Canvas.Test.KEY_DISPLAY_LOCATION,'Chatter');
        envValues.put(Canvas.Test.KEY_LOCATION_URL,Label.CL10071+'/_ui/core/chatter/ui/ChatterPage');
        
        Country__c country = Utils_TestMethods.createCountry();
        country.InternationalPhoneCode__c='1340';
        insert country;
        
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;
        
        PageReference vfPage = Page.VFP_FSearch;            
        Contact c= new Contact();
        c.FirstName='conFirstName';
        c.LastName='conLastName';
        c.LocalFirstName__c='conLocalFirstName';
        c.LocalLastName__c='conLocalLastName';
        c.Street__c='Street';
        c.StreetLocalLang__c='StreetLocalLang';
        c.Country__c = country.Id;
        c.ZipCode__c='ZipCode';
        c.City__c='City';
        c.LocalCity__c='LocalCity';
        c.StateProv__c=stateProv.Id;
        c.POBox__c='POBox';
        c.email='abc@gmail.com';
        c.WorkPhone__c='+91123456';
        c.WorkPhoneExt__c='123456';
        c.MobilePhone='123456';
        try {
        insert c;
        }
        catch(Exception e) {
        
        }
        
        //insert business account
        Account a = new Account();
        a.Name = 'TEST VFC_ContactSearch';
        a.AccountLocalName__c = 'Account Local Name';
        a.Street__c = 'Street';
        a.ZipCode__c = 'zipcode';
        a.City__c = 'city';
        a.StateProvince__c = stateProv.Id;
        a.Country__c = country.Id;
        a.POBox__c = 'pobox';
        a.POBoxZip__c = 'poboxzip';
        a.StreetLocalLang__c = 'streetlocal';
        a.ToBeDeleted__c = false;
        a.CorporateHeadquarters__c = true;
        a.LeadingBusiness__c = 'BD';
        a.MarketSegment__c = 'BDZ';        
        insert a;
        Case Caserecord=Utils_TestMethods.createCase(a.Id,c.Id,'New');
        insert Caserecord;
        
        envValues.put('caseId',Caserecord.Id);
        // Create a mock RenderContext using the test application and environment context data Maps
        Canvas.RenderContext mock = Canvas.Test.mockRenderContext(appValues,envValues);
        
        
        
        fsc.onRender(mock);
        System.assert(true);
    }
    
    @IsTest
    public static void testEscapeSafely() {
        String textToEscape = 'A (text) with content, to {escape}';
        String textEscapedCorrect = 'A \\(text\\) with content\\, to \\{escape\\}';
        
        String textEscapedToCheck = AP_FsearchFromCase.escapeSafely(textToEscape);    
        
        System.assertEquals(textEscapedCorrect, textEscapedToCheck);
    }
}