public class VFC_Announcement {

        public list<Milestone1_Project__c> pjctlist {get;set;}
        public id offerid {get;set;}
        public id announcementid {get;set;}
        public list<announcement_groups__c> announcegrplist {get;set;}
        public list<AnnouncementStakeholder__c> ashlist {get;set;}
        public boolean checkbox {get;set;}
        public list<Innerclass> wrappervar {get;set;}
        public boolean checkbox1 {get;set;}
        public boolean checkbox2 {get;set;}
        public boolean checkbox3 {get;set;}
        public map<string,LIST<Innerclass3> > wrappervar3 {get;set;}
        public list<innerclass3> listwrappervar3 {get;set;}
        public boolean iserror {get;set;}
        public boolean iserror1 {get;set;}
        public list<string> countrylist {get;set;}
        public String anngroupid{get;set;}
        public list<string> selctry {get;set;}
        public string toemailslist {get;set;}
        public string ccemailslist {get;set;}
        public list<string> emails {get; set;}
        public list<string> grplist1 {get;set;}
        public list<string> ctrylist1 {get;set;}
        public list<string> CCEmailsList1 {get;set;}
        public string offeremail {get;set;}
        public string tempalateid {get;set;}
        public integer size {get;set;}
        public string grpcountries {get;set;}
        public string ctryldr {get;set;}
        public string selGroupname {get;set;}
        public integer count {get;set;}
        public integer sizeofid {get;set;}
        public list<string> leaderemail {get;set;}
        public list<string> listofccemails {get;set;}
        public set<string> setofccemails {get;set;}
        public string templatename;
        public map<string,LIST<Innerclass2> > wrappervar2 {get;set;}
        public string offerid1 {get;set;}
        public map<string, List<string>> mapGroupCouty = new map<string, List<string>>();
        public string strgroupcountry;
        public list<string> grpCntryList {get;set;}
        public map<string,list<innerclass3>> newsortwrapper3 {get;set;}
        
       //Hanamanth
        public string accmtpriview;
        public VFC_Announcement(){
            listofccemails = new list<string>();
            setofccemails = new set<string>();
            CCEmailsList1 = new list<String>();
            grpCntryList = new list<string>();
           if(ApexPages.currentPage().getParameters().get('id') != ''){
            offerid = ApexPages.currentPage().getParameters().get('id');
            }
            system.debug('offer id value is ------->'+offerid);
           try{ 
               announcementid = ApexPages.currentPage().getParameters().get('annid');
               accmtpriview =ApexPages.currentPage().getParameters().get('accPriview');
            if(offerid  != Null){
                offerid1 = offerid;
                sizeofid = offerid1.Length();
                countrylist = new list<string>();
                iserror=false;
                checkbox = false;
                wrappervar = new list<Innerclass>();
                selctry = new list<string>();
                grplist1 = new list<string>();
                emails = new list<string>();
               set<string> setCount = new set<string>();
                
                Pjctlist= [select name, Country__c, Country__r.name,Country_Launch_leader__C,Country_Launch_Leader__r.Email,Country_Launch_Leader__r.Name,Offer_Launch__r.Launch_Owner__r.email,Offer_Launch__r.RecordTypeID,Offer_Launch__r.RecordType.Name,Country_Withdrawal_Leader__r.email,Country_Withdrawal_Leader__r.Name,Country_Withdrawal_Leader__c,Offer_Launch__r.Withdrawal_Owner__c,Offer_Launch__r.Withdrawal_Owner__r.email from Milestone1_Project__c where Offer_Launch__c =: offerid ];
                        
                for(Milestone1_Project__c m : Pjctlist){
                    setCount.add( m.country__c); 
                } 
                announcegrplist = new list<announcement_groups__c>();
                announcegrplist = [select Group_Name__c,country__r.name,id,OwnerId,country__c from Announcement_Groups__c where OwnerId=:UserInfo.getUserID() and country__c in: setcount];
                map<string,string> listgroup = new map<string,string>();
                set<string> setusergroups = new set<string>();
                for(Announcement_Groups__c ag:announcegrplist){
                    if(ag.OwnerId == UserInfo.getUserID()){
                        listgroup.put(ag.OwnerId,ag.Group_Name__c );
                         if(mapGroupCouty.containskey(ag.Group_Name__c)) {
                            mapGroupCouty.get(ag.Group_Name__c).add(ag.country__r.name);
                         }else {
                            mapGroupCouty.put(ag.Group_Name__c,new list<string>()) ;
                            mapGroupCouty.get(ag.Group_Name__c).add(ag.country__r.name);
                            
                            
                         }
                    }
                  if(listgroup.get(ag.OwnerId) != Null){
                    setusergroups.add(listgroup.get(ag.OwnerId));
                    }
                }
                list<string> usergroups = new list<string>();
                for(string g : setusergroups){
                    usergroups.add(g);
                }
                usergroups.sort();
                system.debug('=========>'+usergroups);
                if(usergroups.size()<1){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,label.CLOCT14COINbFO06);
                            ApexPages.addMessage(myMsg); 
                            iserror1=true;
                }
                ashlist = [select id, name,Country__c,AnnouncementGroup__c,Tech_Email__c,Email__c,GroupName__c,Role__c,UserName__c,UserName__r.Name,OwnerId,Tech_GroupCountry__c from AnnouncementStakeholder__c where OwnerId=:UserInfo.getUserID() and GroupName__c in: usergroups ORDER BY Tech_Email__c];
                                
                wrappervar2 = new map<string,List<Innerclass2>>();
                wrappervar3 = new map<string,List<Innerclass3>>();
                listwrappervar3 = new List<Innerclass3>();
                newsortwrapper3  = new map<string,list<innerclass3>>();
                checkbox2 = false;
                checkbox3 = false;
                for(string s : usergroups){
                    grpcountries = '';
                    strgroupcountry = '';
                    //Innerclass3 inobj3;
                        map<string,string> groupctry = new map<string,string>();
                        set<string> listctry = new set<string>();
                        map<string,List<string>> grpids = new map<string,List<string>>();
                        system.debug('group name ===>'+s);
                        for(Announcement_Groups__c ag:announcegrplist){
                            if(ag.OwnerId==UserInfo.getUserID() && ag.Group_Name__c == s){
                                groupctry.put(ag.Group_Name__c,ag.country__r.name );
                                listctry.add(ag.country__r.name);
                                if(grpids.containskey(ag.Group_Name__c)) {
                                grpids.get(ag.Group_Name__c).add(ag.country__r.name);
                                }else {
                                    grpids.put(ag.Group_Name__c,new list<string>{ag.country__r.name});
                                } 
                            }
                            system.debug('list of groups--->'+listctry); 
                          system.debug('list of groups--->'+groupctry.get(ag.Group_Name__c));  
                            if(groupctry.get(ag.Group_Name__c) != null){
                                if(grpcountries != ''){
                                grpcountries = grpcountries + ','+ groupctry.get(ag.Group_Name__c) ;
                                } else if(grpcountries == ''){
                                    grpcountries = groupctry.get(ag.Group_Name__c);
                                }
                            } else {
                                grpcountries = grpcountries;
                            }
                        }
                    system.debug('countries for each group--->'+ grpcountries);
                    Innerclass inobj = new innerclass(s,checkbox,grpcountries);
                    wrappervar.add(inobj);
                    list<string> ctrylist = new list<string>();
                    for(string c : listctry){
                        ctrylist.add(c);
                    }
                    ctrylist.sort();
                    system.debug('=========>'+ctrylist);
                    for(string ids : grpids.keyset()){
                    for(string sc : grpids.get(ids)){
                        strgroupcountry = s + SC;
                        ctryldr = '';
                        for(Milestone1_Project__c mp: Pjctlist){
                        system.debug('Country name---->'+sc); 
                            if(mp.country__r.name == sc){
                               if(mp.Offer_Launch__r.RecordType.Name == Label.CLOCT14COINbFO02 || mp.Offer_Launch__r.RecordType.Name == Label.CLOCT14COINbFO03){
                                    if(ctryldr == ''){
                                        ctryldr=mp.Country_Launch_Leader__r.Email;
                                    } else if(ctryldr.contains(mp.Country_Launch_Leader__r.Email)){
                                        ctryldr = ctryldr;
                                    }else {
                                        ctryldr=ctryldr+' , '+mp.Country_Launch_Leader__r.Email;
                                    }
                                } else if(mp.Offer_Launch__r.RecordType.Name == Label.CLOCT14COINbFO04){
                                    if(ctryldr == ''){
                                        ctryldr=mp.Country_Withdrawal_Leader__r.Email;
                                    } else if(ctryldr.contains(mp.Country_Withdrawal_Leader__r.Email)){
                                        ctryldr = ctryldr;
                                    }else {
                                        ctryldr=ctryldr+' , '+mp.Country_Withdrawal_Leader__r.Email;
                                    }
                                }   
                            }
                        }
                        system.debug('Country leader name list --->'+ctryldr);
                        Innerclass2 inobj2 = new innerclass2(sc,checkbox2,ctryldr,strgroupcountry);
                        grpCntryList.add(strgroupcountry);
                        if(wrappervar2.containskey(s)) {
                            wrappervar2.get(s).add(inobj2);
                            
                        }else {
                            wrappervar2.put(s,new List<innerclass2>());
                            wrappervar2.get(s).add(inobj2);                           
                        }
                        //apr 2015 rel -- Divya M 
                        /*  for(AnnouncementStakeholder__c ash : ashlist){
                                if(ash.OwnerId==UserInfo.getUserID() && ash.GroupName__c == s && ash.Country__c == SC){
                                        inobj3 = new Innerclass3(ash.UserName__r.Name,ash.Tech_GroupCountry__c,ash.Tech_Email__c,checkbox3);
                                        listwrappervar3.add(inobj3);
                                    if(wrappervar3.containskey(strgroupcountry)) {
                                        wrappervar3.get(strgroupcountry).add(inobj3);
                                       //system.debug('wrappervar3 vals =====>'+wrappervar3); 
                                    }else {
                                        wrappervar3.put(strgroupcountry,new List<innerclass3>());
                                        wrappervar3.get(strgroupcountry).add(inobj3);                           
                                    }
                                }
                            }
                            system.debug('wrappervar3 vals =====>'+wrappervar3);  */       
                        //end
                    }
                  }  
                    system.debug('Wrappervar2 list---->'+wrappervar2);
                              
                }
          
                system.debug('list of group Countries==>'+grpCntryList);
                for(string strgc : grpCntryList){
                checkbox3 = false;
                    for(AnnouncementStakeholder__c ash : ashlist){
                        if(ash.OwnerId==UserInfo.getUserID() && ash.Tech_GroupCountry__c == strgc){
                                
                                innerclass3 inobj3 = new Innerclass3(ash.UserName__r.Name,ash.Tech_GroupCountry__c,ash.Tech_Email__c,checkbox3,false);
                                listwrappervar3.add(inobj3);
                                
                            if(wrappervar3.containskey(strgc)) {
                                wrappervar3.get(strgc).add(inobj3);
                                
                               //system.debug('wrappervar3 vals =====>'+wrappervar3); 
                            }else {
                                wrappervar3.put(strgc,new List<innerclass3>());
                                wrappervar3.get(strgc).add(inobj3);                           
                            }
                        }
                    }
                    
                }
                system.debug('wrappervar3 vals =====>'+wrappervar3);
                Map<string,map<string,List<innerclass3>>> mapli = new Map<string,map<string,List<innerclass3>>> (); 
                for(string keyin : wrappervar3.keyset()){
                    for(innerclass3 strwrp3 : wrappervar3.get(keyin)){
                        if(mapli.containskey(keyin)) {
                            mapli.get(keyin).put(strwrp3.email,new List<innerclass3>());
                            mapli.get(keyin).get(strwrp3.email).add(strwrp3);
                        }else {
                            mapli.put(keyin,new map<string,List<innerclass3>>());
                            mapli.get(keyin).put(strwrp3.email,new List<innerclass3>());
                            mapli.get(keyin).get(strwrp3.email).add(strwrp3);
                        }
                    }
                }
                for(string strkey:mapli.keyset()) {
                    for(string emailkey : mapli.get(strkey).keyset()){
                  for(innerclass3 innn3:mapli.get(strkey).get(emailkey)) {
                        if(newsortwrapper3.containskey(strkey)) {
                            newsortwrapper3.get(strkey).add(innn3);
                        }else {
                            newsortwrapper3.put(strkey,new List<innerclass3>());
                            //innn3.sort();
                            newsortwrapper3.get(strkey).add(innn3);
                        }
                  
                  }
                }
                }
              //  newsortwrapper3.values().sort();
                //map<string,List<innerclass3>> =newsortwrapper3;
                
                
                system.debug('newsortwrapper3 vals =====>'+newsortwrapper3); 
                //Hanu
                if(accmtpriview=='emailpreview') {
                    continuemethod();
                }// Hanamanth ENDS
        
          }else if(offerid  == Null || offerid == ''){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,label.CLOCT14COINbFO05);
                    ApexPages.addMessage(myMsg); 
                    iserror1=true;
            }
            
            }catch(Exception e) {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
          }
          
        }
        
        public class innerclass{
            public string groups {get;set;}
            public boolean chk{get;set;}
            public string country{get;set;}
            public innerclass(string group1, boolean check, string grpCountry){
                this.chk = check;
                this.groups = group1;
                this.country = grpcountry;
            }
        }
      
        
        public class innerclass2{
            public string Country {get;set;}
            public boolean chk2{get;set;}
            public string CntryLeader{get;set;}
            public String Additionalinformation{get;set;}
            public string anngrpid{get;set;}
            public string groupCountry {get;set;}
            public innerclass2(string Cntry, boolean check2,string ctryldr,string groupCountry1){ 
                this.chk2 = check2;
                this.Country = Cntry;
                this.CntryLeader = ctryldr;
                this.groupCountry = groupCountry1;
            } 
        }
        // Apr 2015 Release starts
         public class innerclass3{
            public string UserName {get;set;}
            public boolean chk3{get;set;}
            public string grpcntry{get;set;}
            public String email{get;set;}
            public boolean isnullemail{get;set;}
            public innerclass3(string UserName1, string grpcntry1,string email1, boolean check3,boolean isnullemail1){ 
                this.chk3 = check3;
                this.UserName = UserName1;
                this.grpcntry = grpcntry1;
                this.email = email1;
                this.isnullemail = isnullemail1;
            } 
            /*public Integer compareTo(Object ObjToCompare) {
            return email.CompareTo(((innerclass3)ObjToCompare).email);
            }*/
        }
        // end
        public PageReference continuemethod() {
            //iserror = false;
            CCEmailsList1 = new list<string>();
            grplist1= new list<string>();
            for(innerclass i : wrappervar){
                if(i.chk == true){
                    grplist1.add(i.groups);
                }
            }
            system.debug('selected groups========='+grplist1);
        
            ctrylist1 = new list<string>();
         if(accmtpriview!='emailpreview') {
            if(wrappervar2 != Null){
             for(string strObj:wrappervar2.keyset()) {
                for(innerclass2 i2 : wrappervar2.get(strObj)){
                   system.debug('selected123 get values ------>'+i2 );
                        if(i2.chk2 == true){
                            ctrylist1.add(i2.Country);
                        }
                    }
              }
            }
            if(newsortwrapper3 != null){
                system.debug('innerclass3 keyset ------>'+newsortwrapper3.keyset() );
                system.debug('innerclass3 ----->'+newsortwrapper3);
                for(string strObj:newsortwrapper3.keyset()){
                    system.debug('innerclass3 keyset ------>'+strObj );
                    for(innerclass3 i3 : newsortwrapper3.get(strObj)){
                        system.debug('innerclass3 values ------>'+i3 );
                        if(i3 != null){
                            if(i3.chk3 == true){
                            system.debug('Selected email is--->'+i3.email);
                            CCEmailsList1.add(i3.email);
                            }
                        }
                    }
                }
            }
            
            system.debug('selected Countries========='+ctrylist1);
                iserror=false;
                system.debug('test values id:-------------->'+offerid);
                system.debug('selected group --->'+grplist1);
                if(grplist1.size()<1 && ctrylist1.size()<1){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,label.CLOCT14COINbFO01);
                    ApexPages.addMessage(myMsg); 
                    iserror=true;//iserror
                    return null;
                }else{
                    // April 2015 Release -- to restrict not to select more than 10 Announcement Stakeholders -- Divya M
                    if(CCEmailsList1.size()>10){
                        ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.Error,label.CLAPR15ELLAbFO03);
                        ApexPages.addMessage(myMsg1); 
                        iserror=true;//iserror
                        return null;
            
                    } else{
                    // ends 
                    for(string sg : grplist1){
                        for(Announcement_Groups__c ag:announcegrplist){
                            if(ag.Group_Name__c == sg){
                                Countrylist.add(ag.country__r.name);
                            } 
                        }
                    }
                    system.debug('list of countries--->'+Countrylist);
                    set<string> setCount = new set<string>();
                    for(Milestone1_Project__c m : Pjctlist){
                        setCount.add( m.country__r.name); 
                    } 
                    for(string sc : ctrylist1){
                        for(string c: setcount){
                            if(sc == c){
                                selctry.add(sc);
                            }
                        }
                    }
                    system.debug('list of selected countries related to offer--->'+selctry);
                    list<Milestone1_Project__c> selctrylist = new list<Milestone1_Project__c>();
                    for(string c : selctry){
                            for(Milestone1_Project__c mp : Pjctlist){
                                if(mp.Country__r.name == c){
                                    selctrylist.add(mp );
                                }
                            }
                    }
                    system.debug('size is --->'+selctrylist.size()); 
                    //Hanamanth
                    list<Milestone1_Project__c> selgrplist = new list<Milestone1_Project__c>();
                    for(string c : Countrylist){
                            for(Milestone1_Project__c mp : selctrylist){
                                if(mp.Country__r.name == c){
                                    selgrplist.add(mp );
                                }
                            }
                    }
                    system.debug('size is --->'+selgrplist.size()); 
                      toemailslist = '';
                      ccemailslist = '';
                  if(Pjctlist[0].Offer_Launch__r.RecordType.Name == Label.CLOCT14COINbFO02 || Pjctlist[0].Offer_Launch__r.RecordType.Name == Label.CLOCT14COINbFO03){
                            system.debug('selected group country list size is --->'+selgrplist.size()); 
                            offeremail=Pjctlist[0].Offer_Launch__r.Launch_Owner__r.email;
                           if(selgrplist.size()>0 && selctry.size()<1){
                                for(Milestone1_Project__c p : selgrplist){
                                    system.debug('emails---->'+p.Country_Launch_Leader__r.email);
                                    emails.add(p.Country_Launch_Leader__r.email);
                                }
                           } else if(selgrplist.size()<1 && selctrylist.size()>0){
                                for(Milestone1_Project__c p1 : selctrylist){
                                    system.debug('emails---->'+p1.Country_Launch_Leader__r.email);
                                    emails.add(p1.Country_Launch_Leader__r.email);
                                }
                            }else  if(selgrplist.size()>0 && selctrylist.size()>0){
                                for(Milestone1_Project__c p1 : selctrylist){
                                    system.debug('emails---->'+p1.Country_Launch_Leader__r.email);
                                    emails.add(p1.Country_Launch_Leader__r.email);
                                }
                                for(Milestone1_Project__c p : selgrplist){
                                    system.debug('emails---->'+p.Country_Launch_Leader__r.email);
                                    emails.add(p.Country_Launch_Leader__r.email);
                                }
                            }
                            system.debug('list of emails added---->'+emails);
                        }else if(Pjctlist[0].Offer_Launch__r.RecordType.Name == Label.CLOCT14COINbFO04){
                            system.debug('selected group country list size is --->'+selgrplist.size()); 
                            offeremail=Pjctlist[0].Offer_Launch__r.Withdrawal_Owner__r.email;
                            if(selgrplist.size()>0 && selctrylist.size()<1){
                                for(Milestone1_Project__c p:selgrplist){
                                    system.debug('emails---->'+p.Country_Withdrawal_Leader__r.email);
                                    emails.add(p.Country_Withdrawal_Leader__r.email);
                                }
                           }else if(selgrplist.size()<1 && selctrylist.size()>0){
                                for(Milestone1_Project__c p1 : selctrylist){
                                    system.debug('emails---->'+p1.Country_Withdrawal_Leader__r.email);
                                    emails.add(p1.Country_Withdrawal_Leader__r.email);
                                }
                            }else  if(selgrplist.size()>0 && selctrylist.size()>0){
                                for(Milestone1_Project__c p1 : selctrylist){
                                    system.debug('emails---->'+p1.Country_Withdrawal_Leader__r.email);
                                    emails.add(p1.Country_Withdrawal_Leader__r.email);
                                }
                                for(Milestone1_Project__c p : selgrplist){
                                    system.debug('emails---->'+p.Country_Withdrawal_Leader__r.email);
                                    emails.add(p.Country_Withdrawal_Leader__r.email);
                                }
                            }
                            system.debug('list of emails added---->'+emails);
                        }
                       // ccemailslist = offeremail;
                             for(string s:emails){
                            system.debug('test emails====='+s);
                            if(s != null){
                                if(toemailslist == ''){
                                toemailslist = s;
                                }else if(toemailslist.contains(s)){
                                        toemailslist = toemailslist ;
                                }else{
                                    toemailslist = toemailslist +','+s;
                                }
                             } else if(s == null){
                                 toemailslist = offeremail;
                             }
                        }
                       
                            if(CCEmailsList1 != null){
                            for(string e:CCEmailsList1){
                                setofccemails.add(e);
                            }
                            for(string e: setofccemails){
                                //system.debug('test emails====='+e);
                                if(e != null){
                                    if(ccemailslist == ''){
                                    ccemailslist = e;
                                    }else if(ccemailslist.contains(e)){
                                            ccemailslist = ccemailslist ;
                                    }else{
                                        ccemailslist = ccemailslist +','+e;
                                    }
                                } 
                            }
                            if(ccemailslist != null){
                                if(ccemailslist.contains(offeremail)){
                                    ccemailslist = ccemailslist ;
                                }else{
                                ccemailslist = ccemailslist +','+ offeremail;
                                }
                            }else if(ccemailslist == null){
                                ccemailslist = offeremail ;
                            }
                            
                        system.debug('list emails added to CC ---->'+ccemailslist);
                        }  else if(CCEmailsList1 == null){
                            ccemailslist = offeremail;
                        }                       
                    }
                    }
                     }
                    system.debug('toemailslist========>'+toemailslist);
                       // if(!Test.isRunningTest()){
                            map<string,CS_Announcement__c> cs = CS_Announcement__c.getall();
                            for(CS_Announcement__c cs1 : cs.values()){
                                if(cs1.Offer_Record_Type__c == Pjctlist[0].Offer_Launch__r.RecordType.name){
                                    templatename = cs1.EmailTemplate__c; 
                                    list<EmailTemplate> Templates = [Select Id, Name From EmailTemplate Where developername =: templatename  limit 1];
                                    tempalateid = Templates[0].id;  
                                    system.debug('template id --->'+tempalateid);
                                }
                            }
                     //   }
                       
            pagereference pg= new pagereference(Label.CLOCT14COINbFO20+announcementid +Label.CLOCT14COINbFO21+announcementid +Label.CLOCT14COINbFO22+toemailslist+Label.CLOCT14COINbFO23+ccemailslist+Label.CLOCT14COINbFO24+tempalateid +Label.CLOCT14COINbFO25);
                return pg;
        }
        
        public PageReference Cancelmethod() {
            pagereference pg1= new pagereference('/'+announcementid);
            return pg1;
        }
        public pagereference AnncEmailPreview() {
             string announcementid = ApexPages.currentPage().getParameters().get('PreviewId');
             offerid = ApexPages.currentPage().getParameters().get('id');
             Pjctlist= [select name, Country__c, Country__r.name,Country_Launch_leader__C,Country_Launch_Leader__r.Email,Country_Launch_Leader__r.Name,Offer_Launch__r.Launch_Owner__r.email,Offer_Launch__r.RecordTypeID,Offer_Launch__r.RecordType.Name,Country_Withdrawal_Leader__r.email,Country_Withdrawal_Leader__r.Name,Country_Withdrawal_Leader__c,Offer_Launch__r.Withdrawal_Owner__c,Offer_Launch__r.Withdrawal_Owner__r.email from Milestone1_Project__c where Offer_Launch__c =: offerid ];
           //  if(!Test.isRunningTest()){
                map<string,CS_Announcement__c> cs = CS_Announcement__c.getall();
                            for(CS_Announcement__c cs1 : cs.values()){
                                if(cs1.Offer_Record_Type__c == Pjctlist[0].Offer_Launch__r.RecordType.name){
                                    templatename = cs1.EmailTemplate__c; 
                                    list<EmailTemplate> Templates = [Select Id, Name From EmailTemplate Where developername =: templatename  limit 1];
                                    tempalateid = Templates[0].id;  
                                    system.debug('template id --->'+tempalateid);
                                }
                            }
            //}
        pagereference pg= new pagereference(Label.CLOCT14COINbFO20+announcementid +Label.CLOCT14COINbFO21+announcementid +Label.CLOCT14COINbFO22+userinfo.getUserEmail()+Label.CLOCT14COINbFO24+tempalateid +Label.CLOCT14COINbFO25);
            pg.setRedirect(true);   
            return pg;
        
        }
        
    }