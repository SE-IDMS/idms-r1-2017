//@Author :- Ashish Sharma
//@Description :- Test class to test the CIS Intervention trigger function.
//@ Date 10/09/2014
@isTest
public class AP_CIS_InterventionCostHandler_TEST{
    
    //Method to initilize CIS date for this test class.
    public static void initInsert(){
        //Initilizing Country object data
        List<Country__c> countryList = new  List<Country__c>();
        
        Country__c countryInstance1 = new Country__c();
        countryInstance1.Name = 'India';
        countryInstance1.CountryCode__c ='IN';
        countryList.add(countryInstance1);
        
        Country__c countryInstance2 = new Country__c();
        countryInstance2.Name = 'France';
        countryInstance2.CountryCode__c ='FR';
        countryList.add(countryInstance2);
       
        Database.insert(countryList);
        
        //Initilizing Correspondent Team Data
        List<CIS_CorrespondentsTeam__c> correspondentTeamList = new List<CIS_CorrespondentsTeam__c>();
        
        CIS_CorrespondentsTeam__c cisCorrespondentTeamInstance1 = new CIS_CorrespondentsTeam__c();
        cisCorrespondentTeamInstance1.CIS_FieldsOfActivity__c='ED: Low Voltage';
        cisCorrespondentTeamInstance1.CIS_ScopeOfCountries__c ='France';
        correspondentTeamList.add(cisCorrespondentTeamInstance1);
        
        CIS_CorrespondentsTeam__c cisCorrespondentTeamInstance2 = new CIS_CorrespondentsTeam__c();
        cisCorrespondentTeamInstance2.CIS_FieldsOfActivity__c='ED: Medium Voltage';
        cisCorrespondentTeamInstance2.CIS_ScopeOfCountries__c='India';
        correspondentTeamList.add(cisCorrespondentTeamInstance2);
        
        Database.insert(correspondentTeamList);
        
        //Bulk initilizing ISR records
        List<CIS_ISR__c> isrList = new List<CIS_ISR__c>();
        
       
            CIS_ISR__c isrInstance1 = new CIS_ISR__c();
            isrInstance1.Name = 'Test ISR 1';
            isrInstance1.CIS_FieldOfActivity__c ='ED: Low Voltage'; 
            //isrInstance1.CIS_EndUserCountry__c = countryInstance1.Id;
            isrInstance1.CIS_CorrectiveActions__c ='This Needs Mannual Test';
            isrList.add(isrInstance1);
       
            CIS_ISR__c isrInstance2 = new CIS_ISR__c();
            isrInstance2.Name = 'Test ISR 2';
            isrInstance2.CIS_FieldOfActivity__c ='ED: Medium Voltage'; 
            //isrInstance2.CIS_EndUserCountry__c = countryInstance2.Id;
            isrInstance2.CIS_CorrectiveActions__c ='This Needs Mannual Test';
            isrList.add(isrInstance2);
       
        
        Database.insert(isrList);    
        
        List<CIS_CountryListPrice__c> countryCostList = new List<CIS_CountryListPrice__c>();
        
        CIS_CountryListPrice__c countryCost1 = new CIS_CountryListPrice__c();
        countryCost1.CIS_CostType__c ='CIS administrative costs';
        countryCost1.CIS_Country__c =countryInstance1.id;
        countryCost1.CIS_FieldOfActivity__c='ED: Medium Voltage';
        countryCost1.CIS_ListPrice__c=1000;
        countryCostList.add(countryCost1);
        
        CIS_CountryListPrice__c countryCost2 = new CIS_CountryListPrice__c();
        countryCost2.CIS_CostType__c ='Delivery costs';
        countryCost2.CIS_Country__c =countryInstance2.id;
        countryCost2.CIS_FieldOfActivity__c='ED: Low Voltage';
        countryCost2.CIS_ListPrice__c=2000;
        countryCostList.add(countryCost2);
        
        Database.insert(countryCostList);
        
        List<CIS_InterventionCosts__c> cisInterventionCostList = new List<CIS_InterventionCosts__c>();
        
        for(Integer i = 0;i<20;i++){
            CIS_InterventionCosts__c interventionCost = new CIS_InterventionCosts__c();
            interventionCost.CIS_ISR__c = isrList[0].id;
            interventionCost.CIS_CostType__c ='CIS administrative costs';
            interventionCost.CIS_CostPerUnit__c =Decimal.valueOf(2000);
            cisInterventionCostList.add(interventionCost);
        }
        Database.insert(cisInterventionCostList);
        
        
        for(CIS_InterventionCosts__c interventionRecord:[select id,name from CIS_InterventionCosts__c where id in:cisInterventionCostList]){
            interventionRecord.CIS_CostType__c  ='Delivery costs';    
        }
        
        Database.update(cisInterventionCostList);
        
    }
    
    @isTest
    public static void myUnitTest1() {
        initInsert();
    }
   
    
}