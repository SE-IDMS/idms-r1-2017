public with sharing class VFC1002_AddContainmentActionInMass extends VFC_ControllerBase { 

    //*********************************************************************************
    // Controller Name  : VFC1002_AddContainmentActionInMass
    // Purpose          : Controller class for creating Containment Action in mass
    // Created by       : Shukti Das Global Delivery Team
    // Date created     : November, 2011
    // Modified by      :
    // Date Modified    :
    // Remarks          : For December - 11 Release
    ///********************************************************************************/
        
    /*============================================================================
        V.A.R.I.A.B.L.E.S
    =============================================================================*/ 
    // collection of accountable organizations matching the search criteria
    public List<BusinessRiskEscalationEntity__c> accOrgs {get; set;}
    
    // current containment action for search
    public ContainmentAction__c conAction { get; set; } 
  
    Public list<selectOption> lstSelectOption{get;set;}
    public boolean  blnstatus1{get;set;}
    Public boolean blnRecordtype{get;set;}
    public string strDecision{get;set;}
    
    public string strSeverity{get;set;}
    public boolean blnexe{get;set;}
    public BusinessRiskEscalationEntity__c accountableOrg { get; set; }
  
    // creation is not authorized when search result returns more than searchMaxSize 100 
    private Integer searchMaxSize = Integer.valueOf(Label.CL10076);
    
    // Search string input by the user 
    public String searchString { get;set; }
  
    // Location type of accountable organization input by user
    public String LocType{get;set;}
  
    public String searchLocation { get;set; }
  
    public String title { get; set; }
    //Accountable organization added by Ram Chilukur as pert of November 2013 Release 
    //public String AccountableOrganization { get; set; }
    // The query string to search in database
    public String QueryString {get;set;}
    {
        QueryString = '';
    } 
    public String errorString {get; set;}
    Public boolean blnstatus{get;set;}
    map<string,string> mpRecordType;
  
    public String Prbid {get; set;}
  
    public String source {get; set;}
    public Set<String> stAccOrgs {get; set;}
    public Map<Id,List<ShipHoldRequest__c>> mapBOShipHold {get; set;}
  
    /*============================================================================
        C.O.N.S.T.R.U.C.T.O.R
    =============================================================================*/   
   
    public VFC1002_AddContainmentActionInMass(ApexPages.StandardController controller) 
    {
        APsPresent = false;
        Prbid = ApexPages.currentPage().getParameters().get('pid');
        System.debug('!!!! Prbid : ' + Prbid);          
          
        // Added by Uttara PR - Nov 2016 Release - RIN Project            
        source = ApexPages.currentPage().getParameters().get('source');
        mapBOShipHold = new Map<Id,List<ShipHoldRequest__c>>();
        System.debug('!!!! source : ' + source);  
                
        if(source == Label.CL112016I2P17) {    // CL112016I2P17 = 'holdsource'       
                 
            stAccOrgs = new Set<String>();
            
            for(ShipHoldRequest__c HS : [SELECT BackOfficeSystem__c, BackOfficeSystem__r.FrontOfficeOrganization__c, Problem__c, ContainmentAction__c, Status__c FROM ShipHoldRequest__c WHERE (Problem__c =: PrbId AND ContainmentAction__c = null AND Status__c != 'Error')]) {
                stAccOrgs.add(HS.BackOfficeSystem__r.FrontOfficeOrganization__c);
                if(HS.BackOfficeSystem__c != null) {
                    if(mapBOShipHold.containskey(HS.BackOfficeSystem__c)){
                    	mapBOShipHold.get(HS.BackOfficeSystem__c).add(HS);
                	}
                    else{
                        mapBOShipHold.put(HS.BackOfficeSystem__c, new List<ShipHoldRequest__c>());            
                        mapBOShipHold.get(HS.BackOfficeSystem__c).add(HS);
                    }
                }
            }
            if(stAccOrgs.isEmpty()) {
                blnexe=true;
                ApexPages.Message insertErrorMessage = new ApexPages.message(ApexPages.severity.ERROR,Label.CL112016I2P08);
                ApexPages.addMessage(insertErrorMessage);
                blnRecordtype=false;
                blnstatus=true;
                blnstatus1 =false;
            }
            else {
                blnRecordtype=false;
                blnstatus=false;
                blnstatus1 =true;   
            }              
        }   
        else {
            blnRecordtype=true;
            blnstatus1=false;
            blnstatus=false;
        }
        lstSelectOption=new list<selectOption>();
        mpRecordType=new map<string,string>();
  
        for(RecordType RT:[select id, Name from RecordType where sobjectType=:Label.CLSEP12I2P99 and RecordType.Name!=:label.CLMAY13I2P09])//added by Nikhil
        { 
            lstSelectOption.add(new selectOption(RT.Id,RT.Name));
            mpRecordType.put(RT.Id,RT.Name);
        }
        
        List<Problem__c> probList;
       
        List<ContainmentAction__c> ContainmentList;
        
        hasNext=true;
        System.debug ('controller ' + controller);
        
        if(ApexPages.currentPage().getParameters().get('id') != null )
        {
            System.debug('Testing  --- in Id ');
            blnRecordtype=false;  
            blnstatus1=true; 
        
            List<String> fields=new List<String>{'AccountableOrganization__c','Title__c','RecordType','Description__c','RelatedProblem__c','ContainmentActionType__c','ContainmentActionTypes__c','ProductReturnInstructions__c'};
            if(!Test.isRunningTest())
                controller.addFields(fields);
           
        }
        
        conAction = (ContainmentAction__c)controller.getRecord(); 
        system.debug('## conAction =>'+ conAction);
        conAction.Status__c='Not started'; 
        if(source == Label.CL112016I2P17) {    // CL112016I2P17 = 'holdsource'    
            conAction.Shipments__c = 'Hold!'; 
            conAction.Recordtypeid = Label.CL112016I2P04;
        }
        else
            conAction.Shipments__c=Label.CLOCT15I2P25; //CLOCT15I2P25 = 'Not Affected'; 
        conAction.OrderIntake__c=Label.CLOCT15I2P25; 
        conAction.Production__c=Label.CLOCT15I2P25;
        conAction.OtherTemporaryActions__c=Label.CLOCT15I2P26;//CLOCT15I2P26 = 'Not Required'; 
        conAction.StockSortQuarantine__c=Label.CLOCT15I2P26; 
        conAction.StockRework__c=Label.CLOCT15I2P26; 
        conAction.StockScrap__c=Label.CLOCT15I2P26; 
        conAction.StockReturn__c=Label.CLOCT15I2P26; 
        conAction.OtherPermanentActions__c=Label.CLOCT15I2P26;
    
        if(ApexPages.currentPage().getParameters().get('id') != null )
        {
            If(mpRecordType.get(conAction.Recordtypeid) == Label.CLSEP12I2P98) 
                blnstatus=false;  // This variable is setting Containment Action Type Picklist Visibility and Containment Action Block
            else
                blnstatus=true;
        
            ContainmentList = [select id, Tech_ProblemSeverity__c,TECH_LatestDecisionPRB__c,AccountableOrganization__c from ContainmentAction__c where id =:ApexPages.currentPage().getParameters().get('id')];
                if (ContainmentList.size()>0) {
                    System.debug('ContainmentList----->'+ContainmentList);
                    strDecision=ContainmentList[0].TECH_LatestDecisionPRB__c;
                    strSeverity=ContainmentList[0].Tech_ProblemSeverity__c;
                }
        }
        
        System.debug('---- >'+ApexPages.currentPage().getParameters().get('id'));
        if(ApexPages.currentPage().getParameters().get('pid')!=null ){
            probList = [select id,name,LatestOSACDecision__c ,Tech_SafetyRelated__c,Severity__c from Problem__c where id =:ApexPages.currentPage().getParameters().get('pid')];
            if (probList.size()>0) {
                System.debug('probList----->'+probList);
                strDecision=probList[0].LatestOSACDecision__c;
                strSeverity=probList[0].Severity__c ;
                if(!Test.isRunningTest())
                    conAction.RelatedProblem__c = probList[0].id ;
            }
        }      
        System.debug ('conAction -->' + conAction);
    
        count = [SELECT count() FROM CommercialReference__c WHERE Problem__c =: Prbid];
        if(count > 0)
            APsPresent = true;  
            
    }
        
    public List<MyWrapper> mywrapperlist = new List<MyWrapper>();
        
    public List<MyWrapper> getAccOrgData(){
        return  mywrapperlist;
    }
    
    public void setAccOrgData(List<MyWrapper> wlist){
        mywrapperlist = wlist;
    }   
    
    //*********************************************************************************
        // Method Name      : doSearchAccountableOrgs
        // Purpose          : frame query string using getQuerySting and display accountable org search results on the screen
        // Created by       : Shukti Das  Global Delivery Team
        // Date created     : November, 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Dec - 11 Release
    ///********************************************************************************/         
    public PageReference doSearchAccountableOrgs(){  
        hasNext=true;
        mywrapperlist.clear();
        accOrgs = null;        
        String s = null;

        s = getQueryString();
        System.debug('ENTERED doSearchAccountableOrgs **' + s);

        Utils_SDF_Methodology.log('START query: ', s);  
        Utils_SDF_Methodology.startTimer();
        try {  
            if(source == Label.CL112016I2P17) {    // CL112016I2P17 = 'holdsource'    
                if(!stAccOrgs.isEmpty())
                    accOrgs = [SELECT Name, Entity__c, SubEntity__c, Location__c FROM BusinessRiskEscalationEntity__c WHERE Id IN: stAccOrgs];  
                System.debug('!!!!  : ' + accOrgs);
            }
            else {  
            accOrgs = Database.query(s);
            System.debug('!!!!  : ' + accOrgs);
            }
            
            if(accOrgs != null && accOrgs.size() > 0){
                Map<ID,EntityStakeholder__c[]>  stakeholderMap = new Map<Id,EntityStakeholder__c[]>();
                Map<ID,String>  orgRoleMap = new Map<Id,String>();
                Map<ID,String>  orgRoleMapCSQ = new Map<Id,String>();
                Map<ID,EntityStakeholder__c>  orgEntityStakeHolderMap = new Map<ID,EntityStakeholder__c>();
                List<EntityStakeholder__c> esList = null;
                EntityStakeholder__c csqManager = null;
                Boolean foundDefaultOwner = false;

                for(BusinessRiskEscalationEntity__c  bree: accOrgs )
                    orgRoleMap.put(bree.id , Label.CL10075); 
                    
                stakeholderMap = Utils_P2PMethods.retrieveOrganizationUserDetails(orgRoleMap);
                Utils_SDF_Methodology.log('StakeHolderList : ', stakeholderMap.values().size()); 
                
                
                for(BusinessRiskEscalationEntity__c  bree: accOrgs){ 
                    if(stakeholderMap.containsKey(bree.id)){
                     orgEntityStakeHolderMap.put(bree.Id, stakeholderMap.get(bree.id)[0]); 
                    }
                    else{
                        orgRoleMapCSQ.put(bree.id , Label.CL10056); 
                    }
                }
                stakeholderMap = Utils_P2PMethods.retrieveOrganizationUserDetails(orgRoleMapCSQ);
                for(BusinessRiskEscalationEntity__c  bree: accOrgs){ 
                    if(stakeholderMap.containsKey(bree.id)){
                     orgEntityStakeHolderMap.put(bree.Id, stakeholderMap.get(bree.id)[0]); 
                    }
                }
                MyWrapper mywrapp;
                for(BusinessRiskEscalationEntity__c  bree: accOrgs){
                    if(orgEntityStakeHolderMap.containsKey(bree.id))
                        mywrapp = new MyWrapper (bree.Entity__c, bree.SubEntity__c, bree.location__c, bree.id, orgEntityStakeHolderMap.get(bree.id));
                    else
                        mywrapp = new MyWrapper (bree.Entity__c, bree.SubEntity__c, bree.location__c, bree.id, new EntityStakeholder__c());
                    mywrapperlist.add(mywrapp);
                }
            }   
            System.debug ('Calling Database with ' + accOrgs);
            Utils_SDF_Methodology.log('Query Result Size: ', accOrgs.size());         
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
        System.debug ('SIZE OF ACCORGS = '+ accOrgs.size());
        if (accOrgs.size() != 0){
            System.debug ('*** RETURNED list is not null ***');
        }
        if(mywrapperlist.size()>0){
          hasNext=false;
        }
        Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.log('END search');
        Utils_SDF_Methodology.Limits();
        return null;
    }
    
    // indicates whether there are more records than the searchMaxSize.
    //public Boolean hasNext{get;set;}
  
    public Boolean hasNext {
        get 
        {
            return (accOrgs == null || (accOrgs != null && accOrgs.size() > searchMaxSize));
        }
        set;
    }
    
    //*********************************************************************************
        // Method Name      : getQueryString
        // Purpose          : This is used to frame the SOQL by using the user input
        // Created by       : Shukti Das,  Global Delivery Team
        // Date created     : November, 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Dec - 11 Release
        ///********************************************************************************/
    public String getQueryString() 
    { 
        System.debug ('ENTERED getQueryString');
        System.debug ('PRINTING STRING ' + searchString);
      
        searchString = String.escapeSingleQuotes(searchString);
        searchString = '%'+searchString+'%';
     
        System.debug('The search string is: >>>> ' + searchString);    
                    
        //QueryString  = 'select name, Entity__c, location__c, ID, (SELECT User__r.Name FROM EntityStakeholders__r where role__c = \'CS&Q Manager\' LIMIT 1) from BusinessRiskEscalationEntity__c where name like \''+searchString+'\' and location_type__c =\'Country Front Office (verticalized)\'';
        QueryString  = 'Select name, Entity__c, SubEntity__c, location__c, ID from BusinessRiskEscalationEntity__c where name like \''+searchString+'\' and location_type__c =\''+ LocType +'\'';
      
        QueryString += ' ORDER BY Name';
        QueryString += ' LIMIT ' + (Integer)(searchMaxSize);
        return QueryString; 
    }
    
        //*********************************************************************************
        // Method Name      : getOptions
        // Purpose          : This is used to get the location types for any Organization
        // Created by       : Shukti Das  Global Delivery Team
        // Date created     : 8th December 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Dec - 11 Release
        ///********************************************************************************/
    public List<SelectOption> getOptions() {
        List<SelectOption> locationTypeList = new List<SelectOption>();      
        Schema.DescribeFieldResult LocationType = Schema.sObjectType.BusinessRiskEscalationEntity__c.fields.location_type__c;      
        for (Schema.PickListEntry PickVal : LocationType.getPicklistValues())
            if(PickVal.getValue()!= Label.CLMAY13I2P10) // Sukumar Salla -  MAY2013RELEASE - Added the Check to remove the Return Center from the Picklist values On Containment Action.
            {
                locationTypeList.add(new SelectOption(PickVal.getValue(), PickVal.getLabel()));
            }
        return locationTypeList;
   }
    
    //*********************************************************************************
        // Method Name      : continueCreation
        // Purpose          : This will  allow the user to create a Containment Action, after user done with search
        // Created by       : Shukti Das  Global Delivery Team
        // Date created     : November, 2011
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Dec - 11 Release
        ///********************************************************************************/ 
    public PageReference continueCreation()
    {
        Boolean isValid=true;
        PageReference newPbPage ;
        System.debug ('** continueCreation **');
        System.debug ('** conActionss **'+conAction);
      
        List<ContainmentAction__c> conlist = new List<ContainmentAction__c>();
        for(MyWrapper w:mywrapperlist) {
            if(w.isSelected ) {
                System.debug ('** THIS RECORD IS SELECTED **');
                ContainmentAction__c con = new ContainmentAction__c();
                con.Mass__c=true;
                con.Title__c = conAction.Title__c;
                con.Description__c = conAction.Description__c;
                con.Status__c = conAction.Status__c;
                con.RelatedProblem__c = conAction.RelatedProblem__c;
                con.StartDate__c = conAction.StartDate__c;
                con.ContainmentActionType__c = conAction.ContainmentActionType__c;
                con.DueDate__c = conAction.DueDate__c;
                con.FieldServiceBulletin__c = conAction.FieldServiceBulletin__c;
                con.AdditionalContact__c = w.conAction.AdditionalContact__c;
                con.Cascadedby__c= conAction.AccountableOrganization__c;
                con.AccountableOrganization__c = w.accOrgId ;
                //The ProductsStockedInQuarantine__c and ProductReturnInstructions__c fields added as part of December2013 release and BR is BR-4164
                con.ProductsStockedInQuarantine__c= conAction.ProductsStockedInQuarantine__c;
                con.ProductReturnInstructions__c= conAction.ProductReturnInstructions__c;
                system.debug('record type id is==>'+conAction.Recordtypeid);
                // Oct 15 Release BR-7364 -- Divya M 
                if( mpRecordType.get(conAction.Recordtypeid) != label.CLSEP12I2P09 && conAction.Shipments__c==Label.CLOCT15I2P25 && conAction.OrderIntake__c==Label.CLOCT15I2P25 && conAction.Production__c == Label.CLOCT15I2P25 && conAction.OtherTemporaryActions__c == Label.CLOCT15I2P26 && conAction.StockSortQuarantine__c ==Label.CLOCT15I2P26 && conAction.StockRework__c==Label.CLOCT15I2P26 && conAction.StockScrap__c==Label.CLOCT15I2P26 && conAction.StockReturn__c==Label.CLOCT15I2P26 && conAction.OtherPermanentActions__c==Label.CLOCT15I2P26){ //CLOCT15I2P26 = 'Not Required' and CLOCT15I2P25 = 'Not Affected'; 
                    isValid=false;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLOCt15I2P28)); //CLOCt15I2P28 = 'At least one type of containment action must be requested.'  
                    //return null;
                }
                else {
                    con.Shipments__c = conAction.Shipments__c;
                    con.OrderIntake__c = conAction.OrderIntake__c;
                    con.Production__c = conAction.Production__c;
                    con.OtherTemporaryActions__c = conAction.OtherTemporaryActions__c;
                    con.StockSortQuarantine__c = conAction.StockSortQuarantine__c;
                    con.StockRework__c = conAction.StockRework__c;
                    con.StockScrap__c = conAction.StockScrap__c;
                    con.StockReturn__c = conAction.StockReturn__c;
                    con.OtherPermanentActions__c = conAction.OtherPermanentActions__c;
                }
                // Ends BR-7364
                if(mpRecordType.get(conAction.Recordtypeid) != Label.CLSEP12I2P98) {
                    con.recordtypeId=conAction.Recordtypeid;
                    con.ContainmentActionTypes__c=conAction.ContainmentActionTypes__c;  
                }
                else
                    con.ContainmentActionType__c = conAction.ContainmentActionType__c;
                 
                Utils_SDF_Methodology.log('Action Owner ', w.conAction.Owner__c);  
                 
                if(w.conAction.Owner__c != null)
                    con.Owner__c = w.conAction.Owner__c;
                else {
                    if (w.stakeholder.User__c!= null)
                        con.Owner__c = w.stakeholder.User__c;
                    else {
                        isValid = false;
                        ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.ERROR, Label.CL10079 );
                        ApexPages.addMessage(errorMessage);
                    }
                }  
                conlist.add (con);
            }  
        } // end of for
        System.debug('strSeverity--- > '+strSeverity);
        System.debug('strDecision--- > '+strDecision);
        System.debug('record type is -->'+mpRecordType.get(conAction.Recordtypeid));
        if (conlist.size() == 0)
        {
            isValid = false;
            ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.ERROR, 'Please select at least one record' );
            ApexPages.addMessage(errorMessage);
        }
        //June '12 Release
        else if (conAction.Status__c != 'Not started' )
        {
            isValid = false;
            ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.ERROR, 'Containment Actions can only be created in "Not Started" status' );
            ApexPages.addMessage(errorMessage);
        }
        //OCT 15 RELEASE -- BR-7369 -- Divya M 
        else If((strDecision == 'No-go' ||( strDecision =='' && strDecision == null)) && mpRecordType.get(conAction.Recordtypeid) == Label.CLSEP12I2P09 && (strSeverity=='Safety related' || strSeverity=='Yes' )) // CLSEP12I2P09 = Customer Containment
        {
            isValid = false;
            ApexPages.Message errorMessage1 = new ApexPages.message(ApexPages.severity.ERROR,Label.CLOCT15I2P27);//CLOCT15I2P27 =  'Customer Containment Actions cannot be created for an Offer Safety Alert unless the Offer Safety Alert Committee (OSAC) has made a Go decision.'
            ApexPages.addMessage(errorMessage1);
        }
        // Ends BR-7369
        else If(strDecision != Label.CLSEP12I2P11 && strDecision!=Label.CLSEP12I2P106 && mpRecordType.get(conAction.Recordtypeid) != Label.CLSEP12I2P98 && (strSeverity=='Safety related' || strSeverity=='Yes' ))
        {
            isValid = false;
            ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.ERROR, Label.CLSEP12I2P107);
            ApexPages.addMessage(errorMessage);
        }
        //End of June '12 Release
        else if (conlist != null && conlist.size()>0 && isValid == true) {
            System.debug ('conlist has ' + conlist.size() + ' records to insert');
            insertContainmentActioninBulk(conlist);
        }
        /*else
        {
            ApexPages.Message errorMessage = new ApexPages.message(ApexPages.severity.ERROR, Label.CL10079 );
            ApexPages.addMessage(errorMessage);
        }*/
        System.debug ('BEFORE PAGE REDIRECT, PROBLEM ID is :'+ conAction.RelatedProblem__c);
        System.debug('isValid==>'+isValid+'====blnexe====>'+blnexe);
        if(isValid && blnexe!=true){
            String redirectPage = '/' + conAction.RelatedProblem__c;
            return newPbPage = new PageReference(redirectPage);
        }
        else
            return null;      
    }
    
    //Inserts containment actions into database based on selected organizations
    public boolean insertContainmentActioninBulk(List<ContainmentAction__c> ContainmentToInsertList){
        blnexe = false;
        System.Debug('****VFC1002_AddContainmentActionInMass insertContainmentActioninBulk Started****');
        if(ContainmentToInsertList.size()> 0){
            if(ContainmentToInsertList.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                System.Debug('######## VFC1002 Error Inserting : '+ Label.CL00264);
            }
            else{
                try
                {
                    Database.SaveResult[] containmentInsertResult = Database.insert(ContainmentToInsertList, true);
                                        
                    for(Database.SaveResult sr: containmentInsertResult){
                        if(sr.isSuccess()) {
                                insertedXAs.add(sr.getId());
                        }
                        if(!sr.isSuccess()){
                            Database.Error err = sr.getErrors()[0];
                            System.Debug('######## VFC1002 Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                            ApexPages.Message insertErrorMessage = new ApexPages.message(ApexPages.severity.ERROR, err.getMessage() );
                            ApexPages.addMessage(insertErrorMessage);
                        }
                    }
                }
                Catch(Exception e)
                {   blnexe=true;
                    ApexPages.Message insertErrorMessage = new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()+'Please contact System Admin.');
                    ApexPages.addMessage(insertErrorMessage);
                }
                
                // Added by Uttara - Notes2bFO - Q2 2016 Release
                lstxaAP = new List<XAAffectedProduct__c>();   
                if(blnstatus1 && APsPresent) {
                    for(WrapperClass wc : lstWrapperClass) {            
                        if(wc.checkbox == true) {
                            for(Id conId : insertedXAs) {
                                xaaf = new XAAffectedProduct__c();
                                xaaf.AffectedProduct__c = wc.selectedAffProd.Id;
                                xaaf.ContainmentAction__c = conId;
                                lstxaAP.add(xaaf);
                                System.debug('!!!! xaaf : ' + xaaf);
                            }
                        }   
                    }
                    
                    System.debug('!!!! lstxaAP.size() : ' + lstxaAP.size());
                    System.debug('!!!! lstxaAP : ' + lstxaAP);
                    if(!lstxaAP.isEmpty())   
                         INSERT lstxaAP;
                }  

                // Added by Uttara - Notes2bFO - Nov 2016 Release
                if(source == Label.CL112016I2P17) {    // CL112016I2P17 = 'holdsource'    
                    List<ContainmentAction__c> lstXA = [SELECT AccountableOrganization__c FROM ContainmentAction__c WHERE Id IN: insertedXAs];
                    for(BackOfficesystem__c BO : [SELECT Id, FrontOfficeOrganization__c FROM BackOfficesystem__c WHERE FrontOfficeOrganization__c IN: stAccOrgs]) {
                        if(mapBOShipHold.containsKey(BO.Id)) {
                            for(ContainmentAction__c xa : lstXA) {
                                if(BO.FrontOfficeOrganization__c == xa.AccountableOrganization__c) {
                                    for(ShipHoldRequest__c shr : mapBOShipHold.get(BO.Id))
                                        shr.ContainmentAction__c = xa.Id;
                                }
                            }   
                        }
                    }
                    try {
                        List<ShipHoldRequest__c> lstSHR = new List<ShipHoldRequest__c>();
                        for(List<ShipHoldRequest__c> lstshr1 : mapBOShipHold.values()) {
                        		lstSHR.addall(lstshr1);   
                        }
                        UPDATE lstSHR;
                    }
                    Catch(Exception e) {
                        blnexe=true;
                        ApexPages.Message BOupdateErrorMessage = new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()+'Please contact System Admin.');
                        ApexPages.addMessage(BOupdateErrorMessage);
                        System.Debug('!!!! Error during Hold Source update : ' + e.getMessage());
                    }   
                }
            }    
        }
        System.Debug('****VFC1002_AddContainmentActionInMass insertContainmentActioninBulk Finished****');
        return blnexe;
    }
    
    /*====================================Code Added By Mohit===================================*/
    Public PageReference hidecolumn()
    {
        Boolean isValid=true;
        blnRecordtype=false;  
        blnstatus1=true;                             //This variable set visibility of Searh componant
        system.debug('record tpe is==>'+ mpRecordType.get(conAction.Recordtypeid)+'== Severity is==>'+strSeverity+'== lastest OSAC decision is===>'+strDecision);
        If(mpRecordType.get(conAction.Recordtypeid) == Label.CLSEP12I2P98)
        {
            blnstatus=false; // This variable is setting Containment Action Type Picklist Visibility and Containment Action Block
            //return null;
        }
        else if(mpRecordType.get(conAction.Recordtypeid) == label.CLSEP12I2P09 && ((strDecision == '' || strDecision == null)|| strDecision == 'No-go' ) && strSeverity == 'Safety related' ){

            isValid=false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,label.CLOCT15I2P27)); //CLOCT15I2P27 = 'Customer Containment Actions cannot be created for an Offer Safety Alert unless the Offer Safety Alert Committee (OSAC) has made a Go decision.'
            blnRecordtype = true;
            blnstatus1=false; 
        }  
        else
            blnstatus=true;
    
        return null;
    }   

    Public PageReference docancel()    //Method to Redirect to Problem Record 
    {
        PageReference newPbPage;
        String redirectPage = '/' + conAction.RelatedProblem__c;
        return newPbPage = new PageReference(redirectPage);
    }
    /*============================================================================================*/
    
    // Wrapper class that is displayed in search results
    public class MyWrapper
    {
        public Boolean isSelected {get;set;}
        public String accOrgId {get;set;}
        public String entity {get;set;}
        public String subEntity {get; set;}
        public String location {get;set;}
        public String idstr {get;set;}
        public EntityStakeholder__c stakeholder {get; set;}
        public ContainmentAction__c conAction {get; set;}
   
        public MyWrapper (String ent, String subEnt, String loc, String aoid, EntityStakeholder__c stakeholder ) {
            this.isSelected = false;
            this.entity = ent;
            this.subEntity = subEnt;
            this.location = loc;
            this.accOrgId = aoid;
            this.stakeholder = stakeholder;
            this.conAction = new ContainmentAction__c();
        }
    }
    
    // Added by Uttara - Notes2bFO - Q2 2016 Release
    
    public List<WrapperClass> lstWrapperClass {get; set;}
    public List<XAAffectedProduct__c> lstxaAP {get; set;}
    public XAAffectedProduct__c xaaf {get; set;}
    Set<Id> insertedXAs = new Set<Id>();
    public Boolean APsPresent {get; set;}
    public Integer count = 0;
        
    public List<WrapperClass> getDisplayAffectedProducts() {
        
        lstWrapperClass = new List<WrapperClass>();
        System.debug('!!!! Prbid : ' + Prbid);    
        for(CommercialReference__c AP : [SELECT Id, Problem__c, DateCodesSerialNumber__c, ProductLine__c, CommercialReference__c, ProductFamily__c, Family__c FROM CommercialReference__c WHERE Problem__c =: Prbid])  {   
            lstWrapperClass.add(new WrapperClass(AP)) ;   
        }    
        System.debug('!!!! lstWrapperClass : ' + lstWrapperClass);
        
        return lstWrapperClass;
    }
    
    public class WrapperClass {
        public CommercialReference__c selectedAffProd {get; set;}
        public Boolean checkbox {get; set;}
        
        //constructor
        public WrapperClass(CommercialReference__c singleAP) {
            selectedAffProd = singleAP;
            checkbox = false;
        }
    } 
}