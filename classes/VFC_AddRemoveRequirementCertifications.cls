public with sharing class VFC_AddRemoveRequirementCertifications {
    RequirementCatalog__c reqc;
    list<RequirementCertification__c> lstExistingCertification = new list<RequirementCertification__c>();
    Set<Id> selOpts1 = new Set<Id>();
    
    public Boolean hasError{get;set;}
    public SelectOption[] selCert { get; set; }
    public SelectOption[] allCert { get; set; }
    
    public VFC_AddRemoveRequirementCertifications(ApexPages.StandardController controller) {
        
        selCert = new List<SelectOption>();  
        allCert = new List<SelectOption>();
        set<String> setCL1 = new set<String>();
  
        reqc = (RequirementCatalog__c)controller.getRecord();
        
        lstExistingCertification = [Select name,CertificationCatalog__c, CertificationCatalog__r.name, CertificationCatalog__r.CertificationName__c  from RequirementCertification__c WHERE RequirementCatalog__c =:reqc.id];
        
        if(lstExistingCertification != null && !lstExistingCertification.isEmpty())
        {
            for (RequirementCertification__c s : lstExistingCertification ) 
            {
                selCert.add(new SelectOption(s.CertificationCatalog__c,s.CertificationCatalog__r.CertificationName__c  ));
                selOpts1.add(s.CertificationCatalog__c);
                
            }  
        }
        
        list<CertificationCatalog__c> lstallCert = new list<CertificationCatalog__c>();
        lstallCert = [SELECT id, name, CertificationName__c   FROM CertificationCatalog__c order by name limit 1000];        
        for(CertificationCatalog__c cert: lstallCert ) 
        {
            if(!(selOpts1.contains(cert.Id)))
                allCert.add(new SelectOption(cert.Id, cert.CertificationName__c));
        }
    }//End of Constructor
    
    Public PageReference updateRequirementCertification()
    {    
        Savepoint sp =  Database.setSavepoint();
        try
        {
            set<Id> setSelIds = new set<ID>();
            list<RequirementCertification__c > lstNewReqCert = new list<RequirementCertification__c >();
            for(SelectOption so : selCert ) 
            {
                RequirementCertification__c reqCert = new RequirementCertification__c();
                reqCert.RequirementCatalog__c = reqc.Id;
                reqCert.CertificationCatalog__c = so.getValue();
                lstNewReqCert.add(reqCert);
                setSelIds.add(so.getValue());
            }
            
            Delete [select id from RequirementCertification__c  where RequirementCatalog__c = :reqc.id];
            
            insert lstNewReqCert ;
            
        }
        catch(DMLException e)
        {
            Database.rollback(sp);
            hasError = true;    
            for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),'')); System.debug(e.getDmlMessage(i)); 
            } 
                       
            return null;
        }
        return new PageReference('/'+reqc.id);
    }//End of method

}//End of class