/**
* Date: 22-Feb-15
* Created by: Nabil ZEGHACHE
* Purpose: This class is used to manage Business Rules related to Installed Products association
* with contracts (Covered Products) or Opportunity Lines        
*/
public class AP_IPAssociationHandler {
    
    private static Boolean isDebugEnabled = false;
    static {
        try {
            isDebugEnabled = Boolean.valueOf(Label.CLAPR14SRV20);
        }
        catch(Exception e) {
            System.debug('#### Error getting debug enabled status from Custom Label CLAPR14SRV20: '+e.getMessage());
        }        
    }
    
    private static void myDebug(String aMsg) {
        if (isDebugEnabled) {
            System.debug('#### '+aMsg);
        }
    }
    
    public static void checkAddressConsistency(List<SVMXC__Service_Contract_Products__c> aListOfCoveredProduct) {
        myDebug('Starting checkAddressConsistency: '+aListOfCoveredProduct);
        if (aListOfCoveredProduct != null) {
            // First, we create a list of InstalledProductAssociation to store the junction object information, the IP information will be added after
            List<InstalledProductAssociation> ipAssociationList = new List<InstalledProductAssociation>();
            //List<Id> listOfServiceLines = new List<Id>();
            Set<Id> setOfIPs = new Set<Id>(); // This set will collect of the Installed Products that are referenced by the junction object (coming from the caller of the method or from the database)
            Set<Id> setOfServiceLines = new Set<Id>(); // This set will collect of the Service Lines (contracts) that are referenced by the junction object (coming from the caller of the method or from the database)
            //Map<Id, sObject> coveredProductsMap = new Map<Id, sObject>(aListOfCoveredProduct);
            Map<Id, List<InstalledProductAssociation>> mapIPAssociationbyParentObj = new Map<Id, List<InstalledProductAssociation>>();
            Map<Id, List<InstalledProductAssociation>> mapIPAssociationbyParentObjDB = new Map<Id, List<InstalledProductAssociation>>();
            InstalledProductAssociation currentIPAssociation;
            for (SVMXC__Service_Contract_Products__c currentCovProd : aListOfCoveredProduct) {
                myDebug('currentCovProd:'+currentCovProd);
                currentIPAssociation = new InstalledProductAssociation(currentCovProd, currentCovProd.SVMXC__Installed_Product__c);
                ipAssociationList.add(currentIPAssociation);
                setOfServiceLines.add(currentCovProd.SVMXC__Service_Contract__c);
                setOfIPs.add(currentCovProd.SVMXC__Installed_Product__c);
                addToIPAssociationByParentObjMap(currentIPAssociation, mapIPAssociationbyParentObj);
            }
            myDebug('mapIPAssociationbyParentObj: '+mapIPAssociationbyParentObj);
            myDebug('setOfServiceLines: '+setOfServiceLines);
            // Now, let's retrieve all the other junction objects that may already exist in DB for the given parent objects where the Installed Products are not null an
            mydebug('select id, name, SVMXC__Service_Contract__c, SVMXC__Installed_Product__c from SVMXC__Service_Contract_Products__c where SVMXC__Service_Contract__c in '+setOfServiceLines +' and SVMXC__Installed_Product__c != null and Id not in '+aListOfCoveredProduct);
            List<SVMXC__Service_Contract_Products__c> coveredProductsInDB = [select id, name, SVMXC__Service_Contract__c, SVMXC__Installed_Product__c from SVMXC__Service_Contract_Products__c where SVMXC__Service_Contract__c in :setOfServiceLines and SVMXC__Installed_Product__c != null and Id not in :aListOfCoveredProduct];
            
            myDebug('coveredProductsInDB: '+coveredProductsInDB);
            Map<Id, SVMXC__Service_Contract_Products__c> mapServiceContracts = new Map<Id, SVMXC__Service_Contract_Products__c>(coveredProductsInDB);
            
            List<InstalledProductAssociation> ipAssociationListFromDB = new List<InstalledProductAssociation>();
            if(coveredProductsInDB != null) {
                myDebug('coveredProductsInDB is not null');
                for (SVMXC__Service_Contract_Products__c currentCovProdinDB : coveredProductsInDB) {
                    myDebug('currentCovProdinDB: '+currentCovProdinDB);
                    currentIPAssociation = new InstalledProductAssociation(currentCovProdinDB, currentCovProdinDB.SVMXC__Installed_Product__c);
                    ipAssociationListFromDB.add(currentIPAssociation);
                    setOfIPs.add(currentCovProdinDB.SVMXC__Installed_Product__c);
                    addToIPAssociationByParentObjMap(currentIPAssociation, mapIPAssociationbyParentObjDB);
                    myDebug('mapIPAssociationbyParentObjDB: '+mapIPAssociationbyParentObjDB);
                }
            }
            
            // Now let's retrieve the Installed Products details from the database
            Map<Id, SVMXC__Installed_Product__c> mapAllInstalledProducts = new Map<Id, SVMXC__Installed_Product__c>([select id, name, SVMXC__Company__c, SVMXC__Company__r.Country__c, SVMXC__Company__r.Country__r.CountryCode__c, SVMXC__Company__r.StateProvince__c, SVMXC__Company__r.City__c from SVMXC__Installed_Product__c where Id in :setOfIPs]);
            myDebug('mapAllInstalledProducts: '+mapAllInstalledProducts);
            
            // Now let's enrich the association objects in both lists: received in parameter (from a trigger for instance) and from the data base (DB)
            enrichIPAssociationWithIPData(ipAssociationList, mapAllInstalledProducts);
            enrichIPAssociationWithIPData(ipAssociationListFromDB, mapAllInstalledProducts);
            
            myDebug('ipAssociationList: '+ipAssociationList);
            myDebug('ipAssociationListFromDB: '+ipAssociationListFromDB);
            
            checkAddressConsistency(mapIPAssociationbyParentObj, mapIPAssociationbyParentObjDB);
        }
    }
    
    public static void checkAddressConsistency(List<Installed_product_on_Opportunity_Line__c> aListOfCoveredProduct) {
        myDebug('Starting checkAddressConsistency: '+aListOfCoveredProduct);
        if (aListOfCoveredProduct != null) {
            // First, we create a list of InstalledProductAssociation to store the junction object information, the IP information will be added after
            List<InstalledProductAssociation> ipAssociationList = new List<InstalledProductAssociation>();
            //List<Id> listOfOppLines = new List<Id>();
            Set<Id> setOfIPs = new Set<Id>(); // This set will collect of the Installed Products that are referenced by the junction object (coming from the caller of the method or from the database)
            Set<Id> setOfOppLines = new Set<Id>(); // This set will collect of the Service Lines (contracts) that are referenced by the junction object (coming from the caller of the method or from the database)
            //Map<Id, sObject> coveredProductsMap = new Map<Id, sObject>(aListOfCoveredProduct);
            Map<Id, List<InstalledProductAssociation>> mapIPAssociationbyParentObj = new Map<Id, List<InstalledProductAssociation>>();
            Map<Id, List<InstalledProductAssociation>> mapIPAssociationbyParentObjDB = new Map<Id, List<InstalledProductAssociation>>();
            InstalledProductAssociation currentIPAssociation;
            for (Installed_product_on_Opportunity_Line__c currentCovProd : aListOfCoveredProduct) {
                myDebug('currentCovProd: '+currentCovProd);
                currentIPAssociation = new InstalledProductAssociation(currentCovProd, currentCovProd.Installed_Product__c);
                ipAssociationList.add(currentIPAssociation);
                setOfOppLines.add(currentCovProd.Opportunity_Line__c);
                setOfIPs.add(currentCovProd.Installed_Product__c);
                addToIPAssociationByParentObjMap(currentIPAssociation, mapIPAssociationbyParentObj);
            }
            myDebug('mapIPAssociationbyParentObj: '+mapIPAssociationbyParentObj);
            myDebug('setOfOppLines: '+setOfOppLines);
            // Now, let's retrieve all the junction objects that may already exist in DB for the given parent objects
            List<Installed_product_on_Opportunity_Line__c> coveredProductsInDB = [select id, name, Opportunity_Line__c, Installed_Product__c from Installed_product_on_Opportunity_Line__c where Opportunity_Line__c in :setOfOppLines and Installed_Product__c != null];
            
            myDebug('coveredProductsInDB: '+coveredProductsInDB);
            Map<Id, Installed_product_on_Opportunity_Line__c> mapServiceContracts = new Map<Id, Installed_product_on_Opportunity_Line__c>(coveredProductsInDB);
            
            List<InstalledProductAssociation> ipAssociationListFromDB = new List<InstalledProductAssociation>();
            if(coveredProductsInDB != null) {
                myDebug('coveredProductsInDB is not null');
                for (Installed_product_on_Opportunity_Line__c currentCovProdinDB : coveredProductsInDB) {
                    myDebug('currentCovProdinDB: '+currentCovProdinDB);
                    currentIPAssociation = new InstalledProductAssociation(currentCovProdinDB, currentCovProdinDB.Installed_Product__c);
                    ipAssociationListFromDB.add(currentIPAssociation);
                    setOfIPs.add(currentCovProdinDB.Installed_Product__c);
                    addToIPAssociationByParentObjMap(currentIPAssociation, mapIPAssociationbyParentObjDB);
                    myDebug('mapIPAssociationbyParentObjDB: '+mapIPAssociationbyParentObjDB);
                }
            }
            
            // Now let's retrieve the Installed Products details from the database
            Map<Id, SVMXC__Installed_Product__c> mapAllInstalledProducts = new Map<Id, SVMXC__Installed_Product__c>([select id, name, SVMXC__Company__c, SVMXC__Company__r.Country__c, SVMXC__Company__r.Country__r.CountryCode__c, SVMXC__Company__r.StateProvince__c, SVMXC__Company__r.City__c from SVMXC__Installed_Product__c where Id in :setOfIPs]);
            myDebug('mapAllInstalledProducts: '+mapAllInstalledProducts);
            
            // Now let's enrich the association objects in both lists: received in parameter (from a trigger for instance) and from the data base (DB)
            enrichIPAssociationWithIPData(ipAssociationList, mapAllInstalledProducts);
            enrichIPAssociationWithIPData(ipAssociationListFromDB, mapAllInstalledProducts);
            
            myDebug('ipAssociationList: '+ipAssociationList);
            myDebug('ipAssociationListFromDB: '+ipAssociationListFromDB);
            
            checkAddressConsistency(mapIPAssociationbyParentObj, mapIPAssociationbyParentObjDB);
        }
    }
    
    private static void addToIPAssociationByParentObjMap(InstalledProductAssociation aJunctionObject, Map<Id, List<InstalledProductAssociation>> aMapIPAssociationbyParentObj) {
        myDebug('addToIPAssociationByParentObjMap aJunctionObject: '+aJunctionObject);
        if (aJunctionObject != null && aJunctionObject.getParentObjectId() != null && aMapIPAssociationbyParentObj != null) {
            if (aMapIPAssociationbyParentObj.get(aJunctionObject.getParentObjectId()) != null) {
                aMapIPAssociationbyParentObj.get(aJunctionObject.getParentObjectId()).add(aJunctionObject);
            }
            else {
                List<InstalledProductAssociation> listOfJunctionObjects = new List<InstalledProductAssociation>();
                listOfJunctionObjects.add(aJunctionObject);
                aMapIPAssociationbyParentObj.put(aJunctionObject.getParentObjectId(), listOfJunctionObjects);
            }
        }
    }
    
    private static void enrichIPAssociationWithIPData(List<InstalledProductAssociation> aListOfJunctionObjects, Map<Id, SVMXC__Installed_Product__c> aMapOfInstalledProducts) {
        myDebug('enrichIPAssociationWithIPData aListOfJunctionObjects: '+aListOfJunctionObjects);
        if (aListOfJunctionObjects != null && aMapOfInstalledProducts != null) {
            for (InstalledProductAssociation currentJunctionObj: aListOfJunctionObjects) {
                currentJunctionObj.setInstalledProduct(aMapOfInstalledProducts.get(currentJunctionObj.getInstalledProductId()));
            }
        }
    }
    
    private static void checkAddressConsistency(Map<Id, List<InstalledProductAssociation>> mapIPAssociationbyParentObj, Map<Id, List<InstalledProductAssociation>> mapIPAssociationbyParentObjDB) {
        myDebug('checkAddressConsistency mapIPAssociationbyParentObj: '+mapIPAssociationbyParentObj+' - mapIPAssociationbyParentObjDB: '+mapIPAssociationbyParentObjDB);
        if (mapIPAssociationbyParentObj != null) {
            for (Id currentParentId: mapIPAssociationbyParentObj.keySet()) {
                InstalledProductAssociation ipAssociationRef;
                // we check if this parent (Service Line or Opportunity Line) has already associated Installed Products stored in the DB
                // If this is the case, we should find this parent Id in the mapIPAssociationbyParentObjDB key set
                if (mapIPAssociationbyParentObjDB != null 
                    && mapIPAssociationbyParentObjDB.containsKey(currentParentId) 
                    && mapIPAssociationbyParentObjDB.get(currentParentId) != null 
                    && mapIPAssociationbyParentObjDB.get(currentParentId).size()>0) {
                        // We will take the first element in the list retrieved from the database as a reference to check consistency
                        ipAssociationRef = mapIPAssociationbyParentObjDB.get(currentParentId).get(0);
                        myDebug('ipAssociationRef (from DB): '+ipAssociationRef);
                    }
                else {
                    // We will take the first element with a non null Installed Product in the list received as an argument (from the trigger for instance) as a reference
                    if (mapIPAssociationbyParentObj.get(currentParentId) != null 
                        && mapIPAssociationbyParentObj.get(currentParentId).size()>0) {
                            for (Integer i=0; i < mapIPAssociationbyParentObj.get(currentParentId).size(); i++) {
                                ipAssociationRef = mapIPAssociationbyParentObj.get(currentParentId).get(i);
                                if (ipAssociationRef != null && ipAssociationRef.getInstalledProductId() != null) {
                                    break;
                                }
                            }
                        }
                        myDebug('ipAssociationRef (from batch): '+ipAssociationRef);
                }
                if (ipAssociationRef != null) {
                    myDebug('Now checking for address consistency with: '+ipAssociationRef);
                    for (InstalledProductAssociation currentIPAssociation: mapIPAssociationbyParentObj.get(currentParentId)) {
                        myDebug('Current ipAssociation to check: '+currentIPAssociation);
                        if (!ipAssociationRef.isAddressConsistentWith(currentIPAssociation)) {
                            myDebug('Not consistent!');
                            currentIPAssociation.getJunctionObject().addError(Label.CLAPR15SRV57);
                        }
                        else {
                            myDebug('Consistent!');
                        }
                    }
                }
            }
        }
    }
    
    public static void attachIPForConcurrentOrders(List<SVMXC__Service_Contract_Products__c> aListOfCoveredProduct) {
        list<SVMXC__Service_Contract_Products__c> updatedcovlist= new list<SVMXC__Service_Contract_Products__c>();
        if (aListOfCoveredProduct.size()>0) {
            Set<String> serialNumbers = new Set<String>();
            String prodSNSeparator = '@#@#@';
            // First we retrieve the set of serial numbers that we have
            for (SVMXC__Service_Contract_Products__c currentCoveredProduct : aListOfCoveredProduct) {
                if (currentCoveredProduct.ShippedSerialNumber__c != null) {
                    serialNumbers.add(currentCoveredProduct.ShippedSerialNumber__c);
                }
            }
            myDebug('Set of serial numbers: '+serialNumbers);
            
            Map<String, SVMXC__Installed_Product__c> ipMapSNProd = new Map<String, SVMXC__Installed_Product__c>();
            
            for (SVMXC__Installed_Product__c currentIP : [SELECT id, name, SVMXC__Serial_Lot_Number__c, SVMXC__Product__c from SVMXC__Installed_Product__c where SVMXC__Serial_Lot_Number__c in :serialNumbers and SVMXC__Product__c != null]) {
                myDebug('currentIP: '+currentIP);
                ipMapSNProd.put(currentIP.SVMXC__Product__c + prodSNSeparator + currentIP.SVMXC__Serial_Lot_Number__c, currentIP);
            }
            myDebug('Map of IP (Product'+prodSNSeparator+'SerialNumber): '+ipMapSNProd);
            
            for (SVMXC__Service_Contract_Products__c currentCoveredProduct : aListOfCoveredProduct) {
                if (currentCoveredProduct.ShippedSerialNumber__c != null && currentCoveredProduct.SVMXC__Product__c != null && ipMapSNProd.get(currentCoveredProduct.SVMXC__Product__c+prodSNSeparator+currentCoveredProduct.ShippedSerialNumber__c) != null) {
                    currentCoveredProduct.SVMXC__Installed_Product__c = ipMapSNProd.get(currentCoveredProduct.SVMXC__Product__c+prodSNSeparator+currentCoveredProduct.ShippedSerialNumber__c).Id;
                	updatedcovlist.add(currentCoveredProduct);
                }
            }
            if(updatedcovlist.size()>0)
            	update updatedcovlist;
        }
    }
   
    /**
     * The purpose of this class is
     * to avoid duplicating the logic of managing Address consistency of Installed Products related to Service Lines and Opportunity Lines
     * by gathering in one place the information about the parent record (service line or appotunity line), the junction object and the Installed Product.
     * This single class can be used for both use cases, and thus will enable to have one single logic implemented
     */ 
    public class InstalledProductAssociation {
        private sObject junctionObject; // the covered product or Installed_product_on_Opportunity_Line__c, i.e. the junction object
        private Id parentObject; // Id of the service contract (Service line) or opportunity line
        private Id installedProdID; // Id of the service contract (Service line) or opportunity line
        private SVMXC__Installed_Product__c installedProd; // The installed product instance
        
        public InstalledProductAssociation(SVMXC__Service_Contract_Products__c aCoveredProduct, Id anIPId) {
            if (aCoveredProduct != null) {
                junctionObject = aCoveredProduct;
                parentObject = aCoveredProduct.SVMXC__Service_Contract__c;
                installedProdID = anIPId;
            }
        }
        
        public InstalledProductAssociation(Installed_product_on_Opportunity_Line__c anIPOnOppLine, Id anIPId) {
            if (anIPOnOppLine != null) {
                junctionObject = anIPOnOppLine;
                parentObject = anIPOnOppLine.Opportunity_Line__c;
                installedProdID = anIPId;
            }
        }
        
        public sObject getJunctionObject() {
            return junctionObject;
        }
        public Id getParentObjectId() {
            return parentObject;
        }
        public Id getInstalledProductId() {
            return installedProdID;
        }

        public void setInstalledProduct(SVMXC__Installed_Product__c anIP) {
            installedProd = anIP;
            if (anIP != null) {
                installedProdID = anIP.Id;
            }
            else {
                installedProdID = null;
            }
        }
        public Id getCountry() {
            Id result = null;
            if (installedProd != null && installedProd.SVMXC__Company__c != null) {
                result = installedProd.SVMXC__Company__r.Country__c;
            }
            return result;
        }
        public String getCountryCode() {
            String result = null;
           if (installedProd != null && installedProd.SVMXC__Company__c != null && installedProd.SVMXC__Company__r.Country__c != null) {
                result = installedProd.SVMXC__Company__r.Country__r.CountryCode__c;
            }
            return result;
        }
        public Id getStateProvince() {
            Id result = null;
            if (installedProd != null && installedProd.SVMXC__Company__c != null) {
                result = installedProd.SVMXC__Company__r.StateProvince__c;
            }
            return result;
        }
        public String getCity() {
            String result = null;
            if (installedProd != null && installedProd.SVMXC__Company__c != null) {
                result = installedProd.SVMXC__Company__r.City__c;
            }
            return result;
        }
        
        /**
         * This method compares the addresses of an Installed Product Assocation with another.
         * A. If the associated parent is not the same (2 different opportunity lines or 2 different service lines), the result is <code>true</code>.
         * B. If the associated parents are the same (same service line or same opportunity line),
         *    B.1. If the coutries are the same, then we check if the country requires more check (state and city)
         *       B.1.1. In case of countries requiring deeper check, we check that the States/Provinces are the same
         *          B.1.1.1. If the states/provinces are the same, we check that the cities are the sames (either both null or equals ignoring case)
         */ 
        public Boolean isAddressConsistentWith(InstalledProductAssociation anInstalledProdAssoc) {
            myDebug('isAddressConsistentWith: '+this+' - '+anInstalledProdAssoc);
            Boolean result = false;
            if (anInstalledProdAssoc != null) {
                if (this == anInstalledProdAssoc) {
                    myDebug('I am comparing to myself ==> consistent');
                    result = true;
                }
                else if (this.getParentObjectId()!=aninstalledProdAssoc.getParentObjectId()) {
                    myDebug('Different parent object ==> consistent');
                    result = true;
                }
                else if (this.getInstalledProductId() == null || aninstalledProdAssoc.getInstalledProductId() == null) {
                    myDebug('One of the Installed product is null ==> consistent');
                    result = true;
                }
                else {
                    myDebug('Same parents object ==> checking countries');
                    if (this.getCountry() != null && (this.getCountry() == anInstalledProdAssoc.getCountry()) ) {
                        myDebug('Same country ==> is country requiring deeper check');
                        // Case 1: we need to check the state and city
                        if (Label.CLAPR15SRV58.containsIgnoreCase(this.getCountryCode())) {
                            myDebug('Country requires deeper check');
                            // Check state/province
                            if (this.getStateProvince() != anInstalledProdAssoc.getStateProvince()) {
                                myDebug('Different states / provinces ==> NOT consistent');
                                result = false;
                            }
                            else { // same state/province, check city
                                myDebug('Same state / province ==> checking city');
                                if (
                                    (this.getCity() == null && anInstalledProdAssoc.getCity() == null)
                                    ||
                                    (this.getCity() != null && this.getCity().equalsIgnoreCase(anInstalledProdAssoc.getCity()))
                                ){
                                    myDebug('Same city ==> consitent');
                                    result = true;
                                }
                            }
                        }
                        // Case 2: the country is enough
                        else {
                            myDebug('Country does NOT requires deeper check ==> consistent');
                            result = true;
                        }
                    }
                }
            }
            myDebug('isConsistent: '+result);
            return result;
        }
    } 
    
    
}