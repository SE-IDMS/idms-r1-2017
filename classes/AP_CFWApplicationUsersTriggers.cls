Public class AP_CFWApplicationUsersTriggers
{
    /** This method is used for Create the User Accessing Application to be displayed in the Data View of the Certification & Risk Assessment Tool*/
    public Static void CreateUserAccessingApp (map<Id,CFWApplicationUsers__c> mapAppUsr)
    {
     List<id> CFWId = new List<id>();
     
     
     For(Id key:mapAppUsr.KeySet()){
     CFWId.add(mapAppUsr.get(key).CertificationRiskAssessment__c);    
     }
          
     List<CFWKeyBusinessObjects__c> KBO = [Select id,BusinessObject__c,CertificationRiskAssessment__c from CFWKeyBusinessObjects__c where CertificationRiskAssessment__c in :CFWid];
     
     List<CFWUserDataAccess__c> InsCFWDa = new List<CFWUserDataAccess__c>();
     
     if(!KBO.isEmpty()){ 
     For(Id key:mapAppUsr.KeySet()){
       For(CFWKeyBusinessObjects__c U:KBO){
         CFWUserDataAccess__c CFWDa = new CFWUserDataAccess__c();
         CFWDa.ApplicationUsers__c = key;
         //CFWDa.UserRole__c = mapAppUsr.get(key).UserRole__c;
         CFWDa.CertificationRiskAssessment__c = U.CertificationRiskAssessment__c;
         CFWDa.BusinessObjects__c = U.id;
        // CFWDa.BusinessObject__c = U.BusinessObject__c;
         InsCFWDa.add(CFWDa);
         }
        } 
         Insert InsCFWDa;    
    }
   }
  }