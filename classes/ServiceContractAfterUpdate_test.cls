@isTest 
Public class ServiceContractAfterUpdate_test
{
static testMethod void SrvcTest3(){

    String ServiceContractRT = System.Label.CLAPR15SRV01;
    String ServiceLineRT = System.Label.CLAPR15SRV02;
     Account Acc = new Account    (Name='ACC001',POBox__c= 'POBox1',POBoxZip__c='POBoxZip',ZipCode__c = '11111');        
            insert Acc;
    Contact Cnct = new Contact(AccountId=Acc.Id,email='UTCont@mail.com',CorrespLang__c='EN',phone='1234567890',LastName='UTContact001', FirstName='Fname1');                               
        insert Cnct;   
    Opportunity opp = New Opportunity (Name='tet',AccountCountry__c='test',StageName='test', CloseDate=date.today()+1);
     insert opp;                
     update opp;
      SVMXC__Service_Contract__c contract2  = New SVMXC__Service_Contract__c (Name = 'test',Tech_EndDate__c = true,
                                                                           //SVMXC__Company__c = Acc.Id,
                                                                           SVMXC__End_Date__c= system.today()+2,
                                                                            
                                                                          // SVMXC__Start_Date__c = date.today(),
                                                                          // SVMXC__Contact__c = Cnct.Id,
                                                                          // InitialOpportunity__c=opp.Id,
                                                                            Recordtypeid=ServiceContractRT,
                                                                       status__c ='EXP',SVMXC__Active__c = False//Tech_ContractCancel__c =TRUE
                                                                       );
  

 Test.starttest();
insert contract2  ;
     contract2.SVMXC__End_Date__c= system.today()+4;
     contract2.OwnerId=userinfo.getuserid();
update contract2;
Test.stoptest();

}
static testMethod void SrvcTest(){ 
    String ServiceContractRT = System.Label.CLAPR15SRV01;
    String ServiceLineRT = System.Label.CLAPR15SRV02;
     Account Acc = new Account    (Name='ACC001',POBox__c= 'POBox1',POBoxZip__c='POBoxZip',ZipCode__c = '11111');        
            insert Acc;
    Contact Cnct = new Contact(AccountId=Acc.Id,email='UTCont@mail.com',CorrespLang__c='EN',phone='1234567890',LastName='UTContact001', FirstName='Fname1');                               
        insert Cnct;   
    Opportunity opp = New Opportunity (Name='tet',AccountCountry__c='test',StageName='test', CloseDate=date.today()+1);
     insert opp;                
     update opp;
SVMXC__Service_Contract__c contract  = New SVMXC__Service_Contract__c (Name = 'test',
                                                                           SVMXC__Company__c = Acc.Id,
                                                                           SVMXC__Start_Date__c = date.today(),
                                                                           SVMXC__Contact__c = Cnct.Id,
                                                                           InitialOpportunity__c=opp.Id,
                                                                            LatestRenewalOpportunity__c=opp.Id,
                                                                            Recordtypeid=ServiceContractRT,
                                                                            SVMXC__Renewed_From__c =null,
                                                                       status__c ='PCANC',SVMXC__Active__c = True,Tech_ContractCancel__c =TRUE
                                                                       );
  

Test.startTest();
insert contract;
     contract.SVMXC__Contact__c=null;
     contract.SVMXC__Active__c=False;
update contract;
Test.stoptest();

 
}
 static testMethod void SrvcTest1(){  
   
        String ServiceContractRT = System.Label.CLAPR15SRV01;
        String ServiceLineRT = System.Label.CLAPR15SRV02;
      
 Account Acc = new Account    (           Name='ACC001',                                   
                                                    POBox__c= 'POBox1',
                                                    POBoxZip__c='POBoxZip',
                                                    ZipCode__c = '11111');
        insert Acc;
Contact Cnct = new Contact(
                                            AccountId=Acc.Id,
                                            email='UTCont@mail.com',
                                            CorrespLang__c='EN',
                                            phone='1234567890',
                                            LastName='UTContact001',
                                            FirstName='Fname1');
insert Cnct;   

Opportunity opp = New Opportunity (Name='tet',
                                  // Account=contract.SVMXC__Company__c,
                                   AccountCountry__c='test',
                                   StageName='test',
                                   CloseDate=date.today()+1
                                    ); 
insert opp;
update opp;         
         
         SVMXC__Service_Level__c sla= new SVMXC__Service_Level__c(name='testsla');
         insert sla;
         SVMXC__Service_Level__c sla1= new SVMXC__Service_Level__c(name='testsla12');
         insert sla1;
         SVMXC__Service_Plan__c sp= new SVMXC__Service_Plan__c(name='testplan');
         insert sp;
         SVMXC__Service_Plan__c sp1= new SVMXC__Service_Plan__c(name='testplan1');
         insert sp1;

         SVMXC__Service_Contract__c contract3  = New SVMXC__Service_Contract__c(Name = 'test11',Recordtypeid=ServiceContractRT,Status__c = 'CAN');
        //insert contract3;
        // update contract3;
              
     

     SVMXC__Service_Contract__c contract1  = New SVMXC__Service_Contract__c (Name = 'test',SVMXC__Service_Level__c=sla.id,
                                                                           SVMXC__Company__c = Acc.Id,SVMXC__Active__c=true,
                                                                           SVMXC__Start_Date__c = date.today(),
                                                                           SVMXC__Contact__c = Cnct.Id,
                                                                           InitialOpportunity__c=opp.Id,
                                                                            Recordtypeid=ServiceLineRT,
                                                                             SVMXC__End_Date__c= system.today()+2,
                                                                             SVMXC__Contract_Price2__c=100,
                                                                             SVMXC__Renewed_From__c =null,
                                                                             SVMXC__Service_Plan__c=sp.id
                                                                             
                                                                       );
  
   Test.starttest(); 
    insert contract1  ;
        contract1.SVMXC__End_Date__c=system.today()+4;
        contract1.SVMXC__Active__c=False;
        contract1.SVMXC__Contract_Price2__c=200;
        contract1.SVMXC__Service_Level__c=sla1.id;
        contract1.SVMXC__Service_Plan__c=sp1.id;
        contract1.SVMXC__Contact__c=null; 
                update contract1;
               Test.stopTest();
    
 }
    static testMethod void SrvcTest2(){
        String ServiceLineRT = System.Label.CLAPR15SRV02;
        
        Account Acc = new Account (Name='ACC001');
        insert acc;

     SVMXC__Service_Contract__c contract4  = New SVMXC__Service_Contract__c (Name = 'test',IsEvergreen__c = False,SoldtoAccount__c=acc.id,
                                                                          BOContractType__c='MNT',LeadingBusinessBU__c='BD',
                                                                          SVMXC__Active__c=false,Recordtypeid=ServiceLineRT); 
      Test.starttest();                                                    
    insert contract4;
    contract4.SVMXC__Active__c=True;
    update contract4;
    Test.stoptest();
    }  
}