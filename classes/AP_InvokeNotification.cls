/* Author          : Sukumar Salla - GD Team
    Date Created    : 21st July 2014
    Description     : Oct2014 Release - BR-4549 - Class to Invoke Case - Major Issues Email Notifications.
   
                      List of Stakeholders Notified for PMI:
                      1. CS&Q Manager(From account country we identify Front Office Organization) 
                      2. If product information is available, Quality Contact is notified.
                      
                      List of Stakeholders Notified for CMI:
                      1. CS&Q Manager(From account country we identify Front Office Organization)
                      2. Quality Contact for the Product Line
                      3. CS&Q Manager for the Organization from Product Line in Scope
                      4. Account Manager(Owner) related to the Case(Not sent if the Account owner is CC Agent)
                      5. For GSA and NSA, Account Owner for Ultimate Parent Account
                      6. Public Group for Major Issues
                      7. For GSA, GSA CS&Q Manager
                      8. CMI Informed for the Organization from Product Line in Scope

  ============================================================================================================= */
public class AP_InvokeNotification{

    // CLOCT14I2P15 CMI Informed
    // CLOCT14I2P14 Quality Contact
    // CLOCT14I2P16 GSA CS&Q Manager
    // CLOCT14I2P17 GSA
    // CLOCT14I2P09 CMI
    // CLOCT14I2P10 PMI
    //  CLOCT14I2P31 KAM/GAM
    // CLOCT14I2P30 Account Manager

    //CLOCT14I2P07 CS&Q Manager
    //CLOCT15I2P03 Potential Safety Issue

  @future
  Public Static void invokePMINotifications(List<String> lstPMICaseIds, map<String,string> mapPMIChangePLCaseIds ){
  
     system.debug('** invokePMINotifications **');
     System.debug('** lstPMICaseIds ==> '+ lstPMICaseIds);
     // Added by Ram Start
     List<CaseStakeholders__c> lstPLChangeCSHs = new List<CaseStakeholders__c>();
     // Added by Ram End
     List<CaseStakeholders__c> lstCSHs = new List<CaseStakeholders__c>();
     List<CaseStakeholders__c> lstEntityCSH = new List<CaseStakeholders__c>();// As part of April 2015 release BR-6923
     map<string, map<string, id>> mapCaseAccOwnerId = new map<string, map<string, id>>(); // As part of April 2015 release BR-6923
     set<string> setEntyGSAOrgMnger= new set<string>(); // As part of April 2015 release BR-6923
     List<String> lstValidOrgIds = new List<String>();
     map<string, map<string,List<id>>> mapOrg1 = new map<string, map<string,List<id>>>();
     map<string, List<CaseStakeholders__c>> mapNewStakeholders = new map<string, List<CaseStakeholders__c>>();
     Map<string,Map<string,List<EntityStakeholder__c>>> mapCaseIdOrgSHs = new Map<string,Map<string,List<EntityStakeholder__c>>>();
     map<string, List<String>> mapCaseValidOrgIds = new map<string, List<string>>();
     List<CS_CaseRoleMapping__c> roleMappings = new List<CS_CaseRoleMapping__c>();
     map<string,id>caseQtyAccorg=new map<string,id>();
     map<string,id>caseIdCsqManager=new map<string,id>();
     mapOrg1 = getRelatedOrg(lstPMICaseIds);
     system.debug('**PMI - mapOrg1'+ mapOrg1);
     map<string, case> mapCaseObj = new map<string, case>();
     mapCaseObj=getCaseMap(lstPMICaseIds);
     if(mapOrg1!=null)
     {
          for(string cid:  mapOrg1.keyset())
          { 
          
               for(string oid: mapOrg1.get(cid).keyset())
                {   id accOrgid=oid;
                    for( id qid: mapOrg1.get(cid).get(oid))
                    {
                        if(qid!=null){
                          caseQtyAccorg.put(cid,accOrgid);
                          lstCSHs.add(prepareCaseStakeholder(cid, Label.CLOCT14I2P10, Label.CLOCT14I2P14,qid,accOrgid));
                          }
                        else
                        {
                            // create a of map <caseid, list<validorgids>> 
                            if(mapCaseValidOrgIds.containskey(cid))
                             {
                               mapCaseValidOrgIds.get(cid).add(oid);
                             }
                             else
                             {
                               mapCaseValidOrgIds.put(cid, new List<string>());
                               mapCaseValidOrgIds.get(cid).add(oid);
                             }
                        }
                    }
                }
            
           }
        } 
         
   
         // create stakeholders from the validorgs
          If(!mapCaseValidOrgIds.isEmpty()) { mapCaseIdOrgSHs = getStakeHolders(mapCaseValidOrgIds);}
          system.debug('**** PMI - mapCaseIdOrgSHs '+ mapCaseIdOrgSHs );
          
          // get the Custom setting values
          roleMappings = CS_CaseRoleMapping__c.getall().values();
          system.debug('**** roleMappings '+ roleMappings );
          
        If(!mapCaseIdOrgSHs.isEmpty()) {
          for(string cid: mapCaseIdOrgSHs.keyset())
          {  id accOrgid=caseQtyAccorg.get(cid);
                if( label.CLOCT14I2P17 == mapCaseObj.get(cid).Account.TECH_I2PNotification__c ){  
                    for(CS_CaseRoleMapping__c csr1: roleMappings)
                    {
                        if(csr1.Type__c==label.CLOCT14I2P10 && csr1.Role__c==label.CLOCT14I2P39){
                                    lstCSHs.add(prepareCaseStakeholder(cid, csr1.Type__c, label.CLOCT14I2P39, null,accOrgid));
                                }}}
             for(string oid: mapCaseIdOrgSHs.get(cid).keyset())
             {
                 for(EntityStakeholder__c esh: mapCaseIdOrgSHs.get(cid).get(oid))
                 {
                    for(CS_CaseRoleMapping__c cs: roleMappings)

                    { 
                      if( label.CLOCT15I2P03 == mapCaseObj.get(cid).Severity__c )//CLOCT15I2P03-Potential Safety Issue. The 'If' is added as part of October 2015 release
                        {//Oct2015 start
                            if(cs.Type__c==label.CLOCT14I2P10 && esh.role__c==cs.Role__c)
                                {   if(cs.Role__c== label.CLOCT15I2P05 ){caseIdCsqManager.put(cid,esh.user__c);
                                lstCSHs.add(prepareCaseStakeholder(cid, cs.Type__c, esh.role__c,esh.user__c,accOrgid));
                                } //CLOCT15I2P05=Potential Safety Issue Reviewer                                    
                                }
                        }//Oct2015 end
                    else{

                            if(cs.Type__c==label.CLOCT14I2P10 && esh.role__c==cs.Role__c)
                                {   if(cs.Role__c== label.CLOCT14I2P07){caseIdCsqManager.put(cid,esh.user__c);
                                lstCSHs.add(prepareCaseStakeholder(cid, cs.Type__c, esh.role__c,esh.user__c,accOrgid));

                                }
                                }
                        }

                    }                 
                 }
             }
          }
         }
// Start April 2015 Release 
         mapCaseAccOwnerId = getAccountStakeholders(lstPMICaseIds);
          for(string cid: mapCaseAccOwnerId.keyset())
          { id accOrgid=caseQtyAccorg.get(cid);
              for(string r: mapCaseAccOwnerId.get(cid).keyset() )
              {
                 if(mapCaseAccOwnerId.get(cid).get(r)!=null && string.valueof(mapCaseAccOwnerId.get(cid).get(r))!='')
                 lstCSHs.add(prepareCaseStakeholder(cid, label.CLOCT14I2P10, r, mapCaseAccOwnerId.get(cid).get(r),accOrgid));
              }       
          }
         if(caseQtyAccorg.size()>0)
         {
            lstEntityCSH=createEnitityGSACSQMgrRecd(caseQtyAccorg,label.CLOCT14I2P10);
                if(lstEntityCSH.size()>0){
                        lstCSHs.addAll(lstEntityCSH);
                        for(CaseStakeholders__c csh:lstEntityCSH){ 
                            setEntyGSAOrgMnger.add(string.valueof(csh.Case__c)+string.valueof(csh.User__c));
                            }
                            system.debug('@@@@@@setEntyGSAOrgMnger***'+setEntyGSAOrgMnger);
                    }
            }
            if(mapCaseObj.size()>0)
         {system.debug('@@@@@@StartlstEntityCSH***'+lstEntityCSH);
            lstEntityCSH.clear();
            system.debug('@@@@@@StartlstEntityCSHClear***'+lstEntityCSH);
            system.debug('@@@@@@mapCaseObj***'+mapCaseObj);
            lstEntityCSH=createUPABUEnitityGSACSQMgrRecd(mapCaseObj,caseQtyAccorg,label.CLOCT14I2P10);
            system.debug('@@@@@@StartlstEntityCSHAgarin***'+lstEntityCSH);
                if(lstEntityCSH.size()>0){
                        lstCSHs.addAll(lstEntityCSH);
                    }
            }
// End April 2015 Release 
             
         system.debug('**** PMI - lstCSHs '+ lstCSHs );
         
         // Insert Case stakeholders.
         if(lstCSHs.size()>0)
         { for(CaseStakeholders__c ci:lstCSHs){ 
          
          if(caseIdCsqManager.containskey(ci.Case__c))
          {ci.CSQManager__c=caseIdCsqManager.get(ci.Case__c);}}
         // Added by Ram Start 
         
         for(CaseStakeholders__c ci:lstCSHs){ 
         if(mapPMIChangePLCaseIds.containskey(ci.Case__c)&& ci.Role__c== label.CLOCT14I2P14 )//'Quality Contact')
         {
         lstPLChangeCSHs.add(ci);
         }
         else if(mapPMIChangePLCaseIds.containskey(ci.Case__c)&& setEntyGSAOrgMnger.contains(string.valueof(ci.Case__c)+string.valueof(ci.User__c))&& ci.Role__c== label.CLOCT14I2P16 )//GSA CS&Q Manager
         {
         lstPLChangeCSHs.add(ci);
         }
         
         }
         
         If (lstPLChangeCSHs.size()>0)
         {
         insertCaseStakeHolders(lstPLChangeCSHs);
         }
         else
         insertCaseStakeHolders(lstCSHs);
// Added by Ram End


         }  
  }
  
  @future
  Public Static void invokeCMINotifications(List<String> lstCMICaseIds,map<String,string> mapCMIChangePLCaseIds ){
   
    map<string, map<string, id>> mapCaseAccOwnerId = new map<string, map<string, id>>();
    List<CaseStakeholders__c> lstEntityCSH = new List<CaseStakeholders__c>();// As part of April 2015 release BR-6923
    set<string> setEntyGSAOrgMnger= new set<string>(); // As part of April 2015 release BR-6923 
          // Added by Ram Start
          List<CaseStakeholders__c> lstPLChangeCSHs = new List<CaseStakeholders__c>();
          // Added by Ram  End
     List<CaseStakeholders__c> lstCSHs = new List<CaseStakeholders__c>();
     List<String> lstValidOrgIds = new List<String>();
     map<string, map<string,List<id>>> mapOrg1 = new map<string, map<string,List<id>>>();
     map<string, List<CaseStakeholders__c>> mapNewStakeholders = new map<string, List<CaseStakeholders__c>>();
     Map<string,Map<string,List<EntityStakeholder__c>>> mapCaseIdOrgSHs = new Map<string,Map<string,List<EntityStakeholder__c>>>();
     map<string, List<String>> mapCaseValidOrgIds = new map<string, List<string>>();
     List<CS_CaseRoleMapping__c> roleMappings = new List<CS_CaseRoleMapping__c>();
     Set<String> setPLOrgIds = new set<string>();
     map<string, id> mapOrg3 = new map<string,Id>(); 
     map<string,id>caseQtyAccorg=new map<string,id>();
     map<string,id>caseIdCsqManager=new map<string,id>();
     map<string,id>caseIdFroOrgid=new map<string,id>();
     mapOrg1 = getRelatedOrg(lstCMICaseIds);
     system.debug('*** CMI - mapOrg1'+ mapOrg1);
      mapOrg3=getReportingOrgonCase(lstCMICaseIds);//Start. Nov 2016 release BR-10591 Invensys , added by Ram Chilukuri
  
  
     if(!mapOrg1.isEmpty())
     {
          for(string cid:  mapOrg1.keyset())
          {                           
                 for(string oid: mapOrg1.get(cid).keyset())
                 { id accOrgid=oid;
                    for( id qid: mapOrg1.get(cid).get(oid))
                    {if(!mapOrg3.containskey(oid)){
                        if(qid!=null){
                         caseQtyAccorg.put(cid,accOrgid);
                          lstCSHs.add(prepareCaseStakeholder(cid, label.CLOCT14I2P09, label.CLOCT14I2P14,qid,accOrgid));
                          setPLOrgIds.add(oid);
                          }else{
                              
                                caseIdFroOrgid.put(cid,oid);
                    }}
                            // create a of map <caseid, list<validorgids>>
                            if(mapCaseValidOrgIds.containskey(cid))
                             {
                               mapCaseValidOrgIds.get(cid).add(oid);
                             }
                             else
                             {
                               mapCaseValidOrgIds.put(cid, new List<string>());
                               mapCaseValidOrgIds.get(cid).add(oid);
                             }
                    }
                 }
                 system.debug('*******caseIdFroOrgid***'+caseIdFroOrgid);
          }
        } 
      
        system.debug('** CMI - mapCaseValidOrgIds'+ mapCaseValidOrgIds);      

          // create stakeholders from the validorgs           
          If(!mapCaseValidOrgIds.isEmpty()) { mapCaseIdOrgSHs = getStakeHolders(mapCaseValidOrgIds); }
          
          // get the Custom setting values
          roleMappings = CS_CaseRoleMapping__c.getall().values();
          system.debug('**** roleMappings '+ roleMappings );
           
          List<String> lstCids = new List<string>();
          map<string, case> mapCaseObj = new map<string, case>();
          for(string cid: mapCaseIdOrgSHs.keyset()) { lstCids.add(cid); }
           mapCaseObj = getCaseMap(lstCids);
          
          for(string cid: mapCaseIdOrgSHs.keyset())
          { id accOrgid=caseQtyAccorg.get(cid);
                    for(CS_CaseRoleMapping__c csr1: roleMappings)
                    {
                        if(csr1.Type__c==label.CLOCT14I2P09 && csr1.Role__c==label.CLOCT14I2P37){
                                    lstCSHs.add(prepareCaseStakeholder(cid, csr1.Type__c, label.CLOCT14I2P37, null,accOrgid));
                                }}
             for(string oid: mapCaseIdOrgSHs.get(cid).keyset())
             {
                 for(EntityStakeholder__c esh: mapCaseIdOrgSHs.get(cid).get(oid))
                 {
                    for(CS_CaseRoleMapping__c csr: roleMappings)
                    {
                       if(csr.Type__c==label.CLOCT14I2P09 && esh.role__c==csr.Role__c)
                       {
                           if(csr.Role__c== label.CLOCT14I2P15)
                           {
                            
                                  lstCSHs.add(prepareCaseStakeholder(cid, csr.Type__c, esh.role__c, esh.user__c,accOrgid));


                           }
                           else if( csr.Role__c== label.CLOCT14I2P16 && csr.AccountType__c == mapCaseObj.get(cid).Account.TECH_I2PNotification__c && csr.AccountType__c == label.CLOCT14I2P17 && label.CLOCT15I2P03 != mapCaseObj.get(cid).Severity__c)
                           {
                               lstCSHs.add(prepareCaseStakeholder(cid, csr.Type__c, esh.role__c, esh.user__c,accOrgid)); 
                           }                           
                           else
                           { 
                            if( label.CLOCT15I2P03 == mapCaseObj.get(cid).Severity__c )//CLOCT15I2P03-Potential Safety Issue. The 'If' is added as part of October 2015 release
                            {//Oct2015 start
                                if(csr.Role__c== label.CLOCT15I2P05)
                                    {    if(oid==caseIdFroOrgid.get(cid)){
                                        system.debug('*******caseIdFroOrgid***'+accOrgid+'***'+caseIdFroOrgid.get(cid));
                                        caseIdCsqManager.put(cid,esh.user__c);
                                        }

                                        lstCSHs.add(prepareCaseStakeholder(cid, csr.Type__c, esh.role__c, esh.user__c,accOrgid));

                                    }
                            } // Oct2015 end
                            else{
                                    if(csr.Role__c== label.CLOCT14I2P07)
                                    {    if(oid==caseIdFroOrgid.get(cid)){
                                        system.debug('*******caseIdFroOrgid***'+accOrgid+'***'+caseIdFroOrgid.get(cid));
                                        caseIdCsqManager.put(cid,esh.user__c);
                                        }

                                        lstCSHs.add(prepareCaseStakeholder(cid, csr.Type__c, esh.role__c, esh.user__c,accOrgid));
                                    }
                                     
                            }}

                       }
                      /* if(csr.Type__c==label.CLNOV16I2P01 && esh.role__c==csr.Role__c )//Start. Nov 2016 release BR-10591 Invensys , added by Ram Chilukuri
                        {
                           if(csr.Role__c== label.CLOCT14I2P15)
                           {                            
                                  lstCSHs.add(prepareCaseStakeholder(cid,csr.Type__c, esh.role__c, esh.user__c,accOrgid));
                           }                           
                       } //End. Nov 2016 release BR-10591 Invensys , added by Ram Chilukuri */
                    }                 
                 }
             }
          }
          
          
          mapCaseAccOwnerId = getAccountStakeholders(lstCMICaseIds);
          for(string cid: mapCaseAccOwnerId.keyset())
          { id accOrgid=caseQtyAccorg.get(cid);
              for(string r: mapCaseAccOwnerId.get(cid).keyset() )
              {if(mapCaseAccOwnerId.containskey(cid)  && mapCaseObj.containskey(cid)) {
                 if(mapCaseAccOwnerId.get(cid).get(r)!=null && string.valueof(mapCaseAccOwnerId.get(cid).get(r))!='' && label.CLOCT15I2P03 != mapCaseObj.get(cid).Severity__c )//Oct2015
                 lstCSHs.add(prepareCaseStakeholder(cid, label.CLOCT14I2P09, r, mapCaseAccOwnerId.get(cid).get(r),accOrgid));
              }  }     
          }
// Start April 2015 Release 
          if(caseQtyAccorg.size()>0)
         {
            lstEntityCSH=createEnitityGSACSQMgrRecd(caseQtyAccorg,label.CLOCT14I2P09);
                if(lstEntityCSH.size()>0){
                       // lstCSHs.addAll(lstEntityCSH);
                        for(CaseStakeholders__c csh:lstEntityCSH){ 
                            if(label.CLOCT15I2P03 != mapCaseObj.get(csh.Case__c).Severity__c){//Oct2015
                                setEntyGSAOrgMnger.add(string.valueof(csh.Case__c)+string.valueof(csh.User__c));
                                lstCSHs.add(csh);
                            }
                            }
                    }
            }
            if(mapCaseObj.size()>0)
         {
            lstEntityCSH.clear();
            lstEntityCSH=createUPABUEnitityGSACSQMgrRecd(mapCaseObj,caseQtyAccorg,label.CLOCT14I2P09);
                if(lstEntityCSH.size()>0){

                    for(CaseStakeholders__c csh:lstEntityCSH){
                    if( label.CLOCT15I2P03 != mapCaseObj.get(csh.Case__c).Severity__c){//Oct2015
                        lstCSHs.add(csh);}
                    }

                    }
            }
// End April 2015 Release 
          system.debug('** CMI - lstCSHs'+ lstCSHs);
          system.debug('** caseIdCsqManager - lstCSHs'+ caseIdCsqManager);
          if(lstCSHs.size()>0)
          {     for(CaseStakeholders__c ci:lstCSHs){ 
          
          if(caseIdCsqManager.containskey(ci.Case__c))
          {ci.CSQManager__c=caseIdCsqManager.get(ci.Case__c);}}
          system.debug('** CMINew - lstCSHs'+ lstCSHs);
          
          
          // Added by Ram Start 
                 
         for(CaseStakeholders__c ci:lstCSHs){ 
         if(mapCMIChangePLCaseIds.containskey(ci.Case__c)&& ci.Role__c=='Quality Contact')
         {
         lstPLChangeCSHs.add(ci);
         }
         else if(mapCMIChangePLCaseIds.containskey(ci.Case__c)&& setEntyGSAOrgMnger.contains(string.valueof(ci.Case__c)+string.valueof(ci.User__c))&& ci.Role__c== label.CLOCT14I2P16 )//GSA CS&Q Manager
         {
         lstPLChangeCSHs.add(ci);
         }
         }
         
         If (lstPLChangeCSHs.size()>0)
         {
         insertCaseStakeHolders(lstPLChangeCSHs);
         }
          else 
          // Added by Ram End
                insertCaseStakeHolders(lstCSHs); 
          }
          
  }

  
  public static map<string, map<string,List<id>>> getRelatedOrg(List<String> lstPMICaseIds){
  
  map<string, map<string,List<id>>> mapOrg = new map<string, map<string,List<Id>>>();
  map<string, List<string>> mapPLCaseObj = new map<string, List<string>>();
  /*
  for(Case cs: [select id, Account.Name, AccountId, AssetOwner__c,Account.Country__r.FrontOfficeOrganization__c, Account.Owner.Name, Account.UltiParentAcc__r.Owner.Name,AssetOwner__r.Country__r.FrontOfficeOrganization__c, AssetOwner__r.Owner.Name, AssetOwner__r.UltiParentAcc__r.Owner.Name, Type, TECH_GMRCode__c from Case where id in:lstPMICaseIds ]) */
  
  for(case cs: getCaseMap(lstPMICaseIds).values())
  {
      if(cs.AssetOwner__c!=null)
      {
          system.debug('** Installed Account');
          if(mapOrg.containskey(cs.id))
          {
              if(mapOrg.get(cs.id).containskey(cs.AssetOwner__r.Country__r.FrontOfficeOrganization__c))
              {
                  mapOrg.get(cs.id).get(cs.AssetOwner__r.Country__r.FrontOfficeOrganization__c).add(null);
              }
              else
              {
                  mapOrg.get(cs.id).put(cs.AssetOwner__r.Country__r.FrontOfficeOrganization__c, new List<id>());
                  mapOrg.get(cs.id).get(cs.AssetOwner__r.Country__r.FrontOfficeOrganization__c).add(null);
              }
          }
          else
          {
              mapOrg.put(cs.id, new map<string, List<id>>());
              mapOrg.get(cs.id).put(cs.AssetOwner__r.Country__r.FrontOfficeOrganization__c, new List<id>());
              mapOrg.get(cs.id).get(cs.AssetOwner__r.Country__r.FrontOfficeOrganization__c).add(null);
          }
          
      }
      else if(cs.AccountId!=null)
      {
          system.debug('** Account');
          if(mapOrg.containskey(cs.id))
          {
              if(mapOrg.get(cs.id).containskey(cs.Account.Country__r.FrontOfficeOrganization__c))
              {
                  mapOrg.get(cs.id).get(cs.Account.Country__r.FrontOfficeOrganization__c).add(null);
              }
              else
              {
                  mapOrg.get(cs.id).put(cs.Account.Country__r.FrontOfficeOrganization__c, new List<id>());
                  mapOrg.get(cs.id).get(cs.Account.Country__r.FrontOfficeOrganization__c).add(null);
              }
          }
          else
          {
              mapOrg.put(cs.id, new map<string, List<id>>());
              mapOrg.get(cs.id).put(cs.Account.Country__r.FrontOfficeOrganization__c, new List<id>());
              mapOrg.get(cs.id).get(cs.Account.Country__r.FrontOfficeOrganization__c).add(null);
          }
      }
      
      // Prepare map of PL-GMRCode and List<Case> Object.
      if(cs.TECH_GMRCode__c!=null){
          If(mapPLCaseObj.containskey(cs.TECH_GMRCode__c.substring(0,4)))
          {
              mapPLCaseObj.get(cs.TECH_GMRCode__c.substring(0,4)).add(cs.id);
          }
          else
          {
              mapPLCaseObj.put(cs.TECH_GMRCode__c.substring(0,4), new List<string>());
              mapPLCaseObj.get(cs.TECH_GMRCode__c.substring(0,4)).add(cs.id);
          }
         }
         if(cs.ReportingOrganization__c!=null){//Start. Nov 2016 release BR-10591 Invensys , added by Ram Chilukuri
             if(cs.ReportingOrganization__r.VerticalisedCustomerCare__c==True){
              if(mapOrg.get(cs.id).containskey(cs.ReportingOrganization__c))
              {
                  mapOrg.get(cs.id).get(cs.ReportingOrganization__c).add(null);
              }
              else
              {
                  mapOrg.get(cs.id).put(cs.ReportingOrganization__c, new List<id>());
                  mapOrg.get(cs.id).get(cs.ReportingOrganization__c).add(null);
              }
         }}//End. Nov 2016 release BR-10591 Invensys , added by Ram Chilukuri
  }
   system.debug('**** mapOrg1'+ mapOrg);
   
  for(ProductLineQualityContactMapping__c pl: [select id, User__c,QualityContact__c, AccountableOrganization__c, TECH_PLGMRCode__c from ProductLineQualityContactMapping__c  where  TECH_PLGMRCode__c in: mapPLCaseObj.keyset() ])
  {       
       system.debug('** user'+pl.user__c);
       for(string cid:  mapPLCaseObj.get(pl.TECH_PLGMRCode__c))
       {
           if(mapOrg.containskey(cid))
           {              
              if(mapOrg.get(cid).containskey(pl.AccountableOrganization__c))
              {
                  mapOrg.get(cid).get(pl.AccountableOrganization__c).add(pl.QualityContact__c);
              }
              else
              {
                  mapOrg.get(cid).put(pl.AccountableOrganization__c, new List<id>());
                  mapOrg.get(cid).get(pl.AccountableOrganization__c).add(pl.QualityContact__c);
              }
           }
           else
           {
              mapOrg.put(cid, new map<string, List<id>>());
              mapOrg.get(cid).put(pl.AccountableOrganization__c, new List<id>());
              mapOrg.get(cid).get(pl.AccountableOrganization__c).add(pl.QualityContact__c);
           }
       } 
       
       
  } 
  system.debug('**** mapOrg2'+ mapOrg);
  /* 
  System.debug('** Case List'+ cs );
  system.debug('**  FrontOfficeOrg'+ cs[0].Account.Country__r.FrontOfficeOrganization__c);
  system.debug('** Installed Acc' + cs[0].AssetOwner__c);
  system.debug('** Account Owner'+ cs[0].Account.Owner.Name); */
  return mapOrg;
  }
public static map<string, id> getReportingOrgonCase(List<String> lstPMICaseIds){//Start. Nov 2016 release BR-10591 Invensys , added by Ram Chilukuri
  
  map<string, id> mapOrg = new map<string,Id>();  
  for(case cs: getCaseMap(lstPMICaseIds).values())
  {
         if(cs.ReportingOrganization__c!=null){
             
                  mapOrg.put(cs.ReportingOrganization__c, cs.ReportingOrganization__c);
                  }
  }
   system.debug('**** mapOrg5'+ mapOrg);
  
  return mapOrg;
  }//End. Nov 2016 release BR-10591 Invensys , added by Ram Chilukuri

 public static map<string, map<string,List<EntityStakeholder__c>>> getStakeHolders(map<string, List<String>> mapCaseValidOrgIds) 
 {
    List<string> lstFinalOids = new List<String>();
    set<String> stFinalOids =  new Set<String>();
    for(List<string> lstOids : mapCaseValidOrgIds.values())
    {
        lstFinalOids.addall(lstOids);        
    }
    if(lstFinalOids!=null && lstFinalOids.size()>0)
    {
       for(string oid: lstFinalOids)
       {
           stFinalOids.add(oid);
       }
    }
    
    system.debug('*****lstFinalOids'+lstFinalOids);
    system.debug('*****stFinalOids'+stFinalOids);
    
    
   // Query BREStakeholders and prepare the map
   map<string, List<EntityStakeholder__c>> mapOrgSH = new  map<string, List<EntityStakeholder__c>>();
   for(EntityStakeholder__c breSH: [Select id, role__c, BusinessRiskEscalationEntity__c,User__c from EntityStakeholder__c where BusinessRiskEscalationEntity__c in: stFinalOids])
   {
        if(mapOrgSH.containskey(breSH.BusinessRiskEscalationEntity__c))
        {
            mapOrgSH.get(breSH.BusinessRiskEscalationEntity__c).add(breSH);
        }
        else
        {
            mapOrgSH.put(breSH.BusinessRiskEscalationEntity__c, new List<EntityStakeholder__c >());
            mapOrgSH.get(breSH.BusinessRiskEscalationEntity__c).add(breSH);
        }
   }
   system.debug('***mapOrgSH'+mapOrgSH);
   
   // loop through the case ids 
   map<string, map<string,List<EntityStakeholder__c>>> mapCaseIdBRESHs = new  map<string, map<string,List<EntityStakeholder__c>>>();
   for(string cid: mapCaseValidOrgIds.keyset())
   {
       //List<string> lstTempOrgids = new List<string>();
       //lstTempOrgids  = mapCaseValidOrgIds.get(cid);
       for(string tempOrgid: mapCaseValidOrgIds.get(cid))
       {
           if(mapOrgSH.containskey(tempOrgid))
           {
               if(mapCaseIdBRESHs.containskey(cid))
               {
                   //mapCaseIdBRESHs.get(cid).add(mapOrgSH.get(tempOrgid))
                   if(mapCaseIdBRESHs.get(cid).containskey(tempOrgid))
                   {
                       mapCaseIdBRESHs.get(cid).get(tempOrgid).addall(mapOrgSH.get(tempOrgid));
                   }
                   else
                   {
                       mapCaseIdBRESHs.get(cid).put(tempOrgid, new List<EntityStakeholder__c>());
                       mapCaseIdBRESHs.get(cid).get(tempOrgid).addall(mapOrgSH.get(tempOrgid));
                   }
               }
               else
               {
                   mapCaseIdBRESHs.put(cid, new map<string, List<EntityStakeholder__c>>());
                   mapCaseIdBRESHs.get(cid).put(tempOrgid, new List<EntityStakeholder__c>());
                   mapCaseIdBRESHs.get(cid).get(tempOrgid).addall(mapOrgSH.get(tempOrgid));
               }
           }
       }
   }
   system.debug('*** mapCaseIdBRESHs'+ mapCaseIdBRESHs);
   return mapCaseIdBRESHs;
 }
 
 Public Static map<string, map<string, Id>> getAccountStakeholders(List<string> lstCMICaseIds)
 {
      map<string, map<string, Id>> mapCaseAccOwnerId = new map<string, map<string, Id>>();
      
     
      
     // for(Case cs: [select id, AssetOwner__c, Account.Owner.Id, Account.UltiParentAcc__r.Owner.Id, AssetOwner__r.Owner.Id, 
     // AssetOwner__r.UltiParentAcc__r.Owner.Id, Type from Case where id in: lstCMICaseIds])
      for(case cs: getCaseMap(lstCMICaseIds).values())
      {
          if(cs.AssetOwner__c!=null)
          {
              If(!mapCaseAccOwnerId.containskey(cs.id))
              {
                 mapCaseAccOwnerId.put(cs.id, new map<string, id>());
                 mapCaseAccOwnerId.get(cs.id).put(label.CLOCT14I2P30,cs.AssetOwner__r.Owner.Id);
                 mapCaseAccOwnerId.get(cs.id).put(label.CLOCT14I2P31, cs.AssetOwner__r.UltiParentAcc__r.Owner.Id);
              }   
          }
          else if(cs.AccountId!=null)
          {
               If(!mapCaseAccOwnerId.containskey(cs.id))
              {
                 mapCaseAccOwnerId.put(cs.id, new map<string, id>());
                 mapCaseAccOwnerId.get(cs.id).put(label.CLOCT14I2P30,cs.Account.Owner.Id);
                 mapCaseAccOwnerId.get(cs.id).put(label.CLOCT14I2P31, cs.Account.UltiParentAcc__r.Owner.Id);
              } 
          }
      }
     return mapCaseAccOwnerId;
 }
 
 public static void insertCaseStakeHolders(List<CaseStakeholders__c> lstCSHs)
 {
    Insert lstCSHs;
 }
 
 Public Static CaseStakeholders__c prepareCaseStakeholder ( string cid, string Type, string r, id userid,id accOrgid )
 {
    CaseStakeholders__c csh = new CaseStakeholders__c();
    id caseId = cid;
    csh.Case__c = caseId;
    csh.Type__c = Type;
    csh.Role__c = r;
    csh.User__c = userid;
    csh.AccountableOrganization__c = accOrgid;
    return csh;
 }
 
 public static map<string, case> getCaseMap (List<string> caseIds)
 {
    map<string, case> mapCase = new Map<string, case>();
    if( caseIds.size()>0 && caseIds!=null){
    for(case c: [select id, Severity__c, Account.Name, AccountId, AssetOwner__c,Account.Country__r.FrontOfficeOrganization__c, Account.Owner.Id, Account.UltiParentAcc__r.Owner.Id,AssetOwner__r.Country__r.FrontOfficeOrganization__c, AssetOwner__r.Owner.Id, AssetOwner__r.UltiParentAcc__r.Owner.Id,TECH_GMRCode__c, Account.TECH_I2PNotification__c,AssetOwner__r.UltiParentAcc__r.LeadingBusiness__c, Account.UltiParentAcc__r.LeadingBusiness__c,ReportingOrganization__r.VerticalisedCustomerCare__c,ReportingOrganization__c from Case where id in:caseIds])
    {
        mapCase.put(c.id, c);
    }
    }
    return mapCase;
 }
 /*
    The below method added by Ram chilukuri as part of April 2015 release BR-6923
 */
 Public Static List<CaseStakeholders__c> createEnitityGSACSQMgrRecd( map<string,id> caseQtyAccorg, string type)
 { 
    List<CaseStakeholders__c> listCaseStedrs= new List<CaseStakeholders__c>();
    map<id, string> qtyOrgIDEntityName = new map<id, string>();
   for(BusinessRiskEscalationEntity__c tempEntity: [Select id, Entity__c from BusinessRiskEscalationEntity__c where id in: caseQtyAccorg.values()])
   {
        qtyOrgIDEntityName.put(tempEntity.id,tempEntity.Entity__c);
   }   
   if(qtyOrgIDEntityName.size()>0){
        map<string, List<EntityStakeholder__c>> mapOrgSH = new  map<string, List<EntityStakeholder__c>>();
         mapOrgSH =getEnitityGSACSQMgrRecds(qtyOrgIDEntityName.values());
         for(string tcaseId:caseQtyAccorg.keyset())
         {      if(mapOrgSH.containskey(qtyOrgIDEntityName.get(caseQtyAccorg.get(tcaseId)))){
             for(EntityStakeholder__c entystholder:mapOrgSH.get(qtyOrgIDEntityName.get(caseQtyAccorg.get(tcaseId))))
             {
               listCaseStedrs.add(prepareCaseStakeholder(tcaseId, type, entystholder.role__c, entystholder.User__c,caseQtyAccorg.get(tcaseId)));
             }}
         }
   }
   return listCaseStedrs;
}
Public Static List<CaseStakeholders__c> createUPABUEnitityGSACSQMgrRecd( map<string, case> mapCaseObj,map<string,id> caseQtyAccorg,string type)
 { 
    List<CaseStakeholders__c> listCaseStedrs= new List<CaseStakeholders__c>();
    map<string, string> mapofCaseBUentity=new map<string, string>();
    for(case tcase:mapCaseObj.values())
        {
            if(tcase.AssetOwner__c!=null)
          {
                mapofCaseBUentity.put(tcase.id,tcase.AssetOwner__r.UltiParentAcc__r.LeadingBusiness__c);
          }
          else if(tcase.AccountId!=null)
          {
             mapofCaseBUentity.put(tcase.id,tcase.Account.UltiParentAcc__r.LeadingBusiness__c);
          }
        }
   if(mapofCaseBUentity.size()>0){
        map<string, List<EntityStakeholder__c>> mapOrgSH = new  map<string, List<EntityStakeholder__c>>();
        map<string, string> mapLBUEnityNOrgName = new  map<string, string>();
        mapLBUEnityNOrgName=getEnitityOrgNames(mapofCaseBUentity.values());// Map added as part of October 2015 release BR-7781
        if(mapLBUEnityNOrgName.size()>0){
         mapOrgSH =getEnitityGSACSQMgrRecds(mapLBUEnityNOrgName.values());
         for(string tcaseId:mapCaseObj.keyset())
         { 
            if(mapOrgSH.containskey(mapLBUEnityNOrgName.get(mapofCaseBUentity.get(tcaseId)))){
             for(EntityStakeholder__c entystholder:mapOrgSH.get(mapLBUEnityNOrgName.get(mapofCaseBUentity.get(tcaseId))))
             {
               listCaseStedrs.add(prepareCaseStakeholder(tcaseId, type, entystholder.role__c, entystholder.User__c,caseQtyAccorg.get(tcaseId)));
             }}

         }}

   }
   system.debug('*** listCaseStedrs'+ listCaseStedrs);
   return listCaseStedrs;
   }
Public Static map<string,List<EntityStakeholder__c>> getEnitityGSACSQMgrRecds(list<string> qtyOrgIDEntityName)
 {
    /*set<id> qtyOrgIDs= new set<id>();
   for(BusinessRiskEscalationEntity__c tempEntity: [Select id, Entity__c from BusinessRiskEscalationEntity__c where Name in: qtyOrgIDEntityName])
   {
        qtyOrgIDs.add(tempEntity.id);
   } */  
   map<string, List<EntityStakeholder__c>> mapOrgSH = new  map<string, List<EntityStakeholder__c>>();
    for(EntityStakeholder__c breSH: [Select id, role__c, BusinessRiskEscalationEntity__r.Name,User__c from EntityStakeholder__c where BusinessRiskEscalationEntity__r.Name in: qtyOrgIDEntityName AND role__c=:Label.CLOCT14I2P16])
            {
                if(mapOrgSH.containskey(breSH.BusinessRiskEscalationEntity__r.Name))
                    {
                        mapOrgSH.get(breSH.BusinessRiskEscalationEntity__r.Name).add(breSH);
                    }
                else
                    {
                        mapOrgSH.put(breSH.BusinessRiskEscalationEntity__r.Name, new List<EntityStakeholder__c >());
                        mapOrgSH.get(breSH.BusinessRiskEscalationEntity__r.Name).add(breSH);
                    }
            }
            system.debug('*** mapOrgSH1'+ mapOrgSH);
        return mapOrgSH;
 }
 public Static map<string,string> getEnitityOrgNames(list<string> lBUEntityName)//As part of October 2015 rlease added this method BR-5335.
 {
    map<string, string> mapLBUEnityNOrgName = new map<string, string>();
   for(BusinessRiskEscalationEntity__c tempEntity: [Select id, Name ,BusinessUnit__c from BusinessRiskEscalationEntity__c where BusinessUnit__c in: lBUEntityName])
   {
        mapLBUEnityNOrgName.put(tempEntity.BusinessUnit__c,tempEntity.Name);
   } 
system.debug('*** mapLBUEnityNOrgName'+ mapLBUEnityNOrgName);   
 return mapLBUEnityNOrgName;
 }
 public static void checkCSI(Map <string, Case> mapCases)// Oct2015 Release BR-7781
 {      set<string> setCaseIds=new set<string>();
        List<CustomerSafetyIssue__c> ListCSI=[SELECT Id,RelatedbFOCase__c FROM CustomerSafetyIssue__c where RelatedbFOCase__c in: mapCases.keyset()];
        for(CustomerSafetyIssue__c tempCSI:ListCSI)
            {
                setCaseIds.add(String.valueOf(tempCSI.RelatedbFOCase__c));
            }
            System.debug('@@@@@ mapCases'+ mapCases);
            System.debug('@@@@@ setCaseIds'+ setCaseIds);
        for(string tempCaseid:mapCases.keyset())
            {
             if(!setCaseIds.contains(tempCaseid))
             {
                case tcase=mapCases.get(tempCaseid);
                tcase.addError(label.CLOCT15I2P09); 
             }
            }
 }
}