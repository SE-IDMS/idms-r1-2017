/*
    Author          : Nicolas Palitzyne ~ nicolas.palitzyne@accenture.com 
    Date Created    : 12/05/2011
    Description     : Update a Product Line in the DataBase
*/

public class AP15_ProductLineIntegrationMethods 
{
    /** Convention for Product as DataTamplate__c :
        Field1__c : Business Unit
        Field2__c : Product Line
        Field3__c : Product Family
        Field4__c : Family 
        Field5__c : Sub Family
        Field6__c : Product Succession
        Field7__c : Product Group
        Field8__c : Commercial Reference
        Field9__c : Product Description
    **/

    // Return dependent picklist value 
    public static List<String> UpdatePL(Map<OPP_ProductLine__c, DataTemplate__c> ObjMap)
    {
        List<OPP_ProductLine__c> Products = new List<OPP_ProductLine__c>();
    
        For(OPP_ProductLine__c Product:ObjMap.keyset())
        {
            DataTemplate__c InputProduct = (DataTemplate__c)ObjMap.get(Product);
    
            if(InputProduct.field1__c != null)
                Product.ProductBU__c = InputProduct.field1__c;
                
            if(InputProduct.field2__c != null)
                Product.ProductLine__c = InputProduct.field2__c;  
                
            if(InputProduct.field3__c != null)
                Product.ProductFamily__c = InputProduct.field3__c;  
                
            if(InputProduct.field4__c != null)
                Product.Family__c = InputProduct.field4__c;   
            
            if(InputProduct.field8__c != null)
               Product.TECH_CommercialReference__c = InputProduct.field8__c;  
    
            if(InputProduct.field9__c != null)
               Product.ProductDescription__c = InputProduct.field9__c; 
            
            if(InputProduct.field10__c != null)
            {
                List<OPP_Product__c> RelatedProduct = [SELECT Id, Name, BusinessUnit__c, ProductLine__c, ProductFamily__c, Family__c, TECH_PM0CodeInGMR__c FROM OPP_Product__c WHERE TECH_PM0CodeInGMR__c=:InputProduct.field10__c LIMIT 1]; 
                if(RelatedProduct.size() == 1)
                {
                    Product.Product__c = RelatedProduct[0].Id;
                     System.debug('#### RelatedProduct : ' + RelatedProduct );
                   
                    List<SupplyingPlant__c> SupPlants = [SELECT Id, Name FROM SupplyingPlant__c WHERE product__c=:RelatedProduct[0].Id];
                    if(SupPlants.size() == 1 )
                    {
                        Product.SupplyingPlant__c = SupPlants[0].Id;
                         System.debug('#### SupPlants : ' + SupPlants);   
                    }
                    else
                    {
                        Product.SupplyingPlant__c = null;
                    }
                        
                }
            }     
            
            Products.add(Product);    
       }
       
       List<Database.SaveResult> ProductLineInsert = Database.Update(Products);
       
       List<String> ProductIDs = new List<String>();
       For(Database.SaveResult DSR:ProductLineInsert)
       {
           ProductIDs.add(DSR.getID());
       }
       
       return ProductIDs;
    }
    
     public static List<String> InsertProductLine(Map<Opportunity, DataTemplate__c> ObjMap)
    {
        List<OPP_ProductLine__c> Products = new List<OPP_ProductLine__c>();
    
        For(Opportunity Obj:ObjMap.keyset())
        {
            DataTemplate__c InputProduct = (DataTemplate__c)ObjMap.get(Obj);
            OPP_ProductLine__c Product = new OPP_ProductLine__c();
    
            Product.Opportunity__c = Obj.Id;
        
            if(InputProduct.field1__c != null)
                Product.ProductBU__c = InputProduct.field1__c;
                
            if(InputProduct.field2__c != null)
                Product.ProductLine__c = InputProduct.field2__c;  
                
            if(InputProduct.field3__c != null)
                Product.ProductFamily__c = InputProduct.field3__c;  
                
            if(InputProduct.field4__c != null)
                Product.Family__c = InputProduct.field4__c;   
            
            if(InputProduct.field8__c != null)
                Product.TECH_CommercialReference__c = InputProduct.field8__c;  
    
            if(InputProduct.field9__c != null)
                Product.ProductDescription__c = InputProduct.field9__c;   
                
            List<OPP_Product__c> OPPProducts = [SELECT Id FROM OPP_Product__c WHERE TECH_PM0CodeInGMR__c =:InputProduct.Field10__c LIMIT 1 ];
            If(OPPProducts.size() > 0)
            {
                 Product.Product__c = OPPProducts[0].Id;   
            }
            
            Product.CurrencyIsoCode = Obj.CurrencyIsoCode;
            
            Products.add(Product);    
       }
       
       List<Database.SaveResult> ProductLineInsert = Database.Insert(Products);
       
       List<String> ProductIDs = new List<String>();
       For(Database.SaveResult DSR:ProductLineInsert)
       {
           ProductIDs.add(DSR.getID());
       }
       
       return ProductIDs;
    } 
}