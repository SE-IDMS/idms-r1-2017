global class ProgramLevelContext {
    WebService String[] PartnerProgramIDs;
    //WebService Integer StartRow = 1;
    WebService Integer BatchSize = 10000;
    
    WebService DateTime LastModifiedDate1;
    WebService DateTime LastModifiedDate2;
    WebService DateTime CreatedDate;
    
    /**
    * A blank constructor
    */
    public ProgramLevelContext() {
    }

    /**
    * A constructor based on an Program @param a list of ProgramIDs
    */
    public ProgramLevelContext(List<String> programIDs) {
        this.PartnerProgramIDs = programIDs;
    }
}