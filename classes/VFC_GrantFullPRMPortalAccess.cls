public with sharing class VFC_GrantFullPRMPortalAccess {

    // This is a controller class that is invoked by the page when the user performs the action
    // Grant Full Portal Access. VF page invokes the action "GrantFullPortalAccess"

    // Error enumeration & Static message list
    // Check if the MessageList can be made transient as it can always be loaded 
    // during postback / partial postback

    private AP_PRMAppConstants.RegistrationValidationErrors RegErrEnum;
    public Map<String, String> MessageList { get; private set; }

    public Set<String> ValidationMessages { get; private set; }
    public String SaveStatus { get; set; }

    public VFC_GrantFullPRMPortalAccess () {  
        MessageList = AP_ValidationMessageService.ErrorMessages; 
        ValidationMessages = new Set<String>();
    }  

    // This method is called on pageLoad and this method consumes the service method
    // PromotePOMPOnlyUserToPortal which does the following:
    // 1. Grant PRM Application access in IMS
    // 2. Create the account in IMS, Update account with UIMSID in bFO if required
    // 3. Create Fielo Member
    // 4. Invoke queue-able job to assign PRM Portal permissions
    public PageReference GrantFullPortalAccess() {
        PageReference pr = null;
        try {
            this.ValidateInputData();

            if (!this.ValidationMessages.isEmpty()) {
				SaveStatus = 'valErrors';
				System.debug('*** Validation Errors:' + ValidationMessages);
                return pr;
            }
          
            Id cntID = (ID)ApexPages.CurrentPage().getParameters().get('id');
            AP_PRMAccountRegistration.PromotePOMPOnlyUserToPortal(cntID);
                      
            SaveStatus = 'success';
        }
        catch (Exception ex){
            SaveStatus = 'valErrors';
            ValidationMessages.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_UNKNOWN.name());
        }
        return pr;
    }

    // This is a helper method that shall be invoked to perform the validation on the data
    // before performing the requested operation
    // 1. If account's BT / AF is blank, populate the error message with the link to Edit Company Information
    // 2. If account's country is CountryPortal is not enabled, 
    //    populate the error message Country portal for the selected account's country is not active
    // 3. If account's BT / AF is not in the active list of channels (CountryChannels__c), 
    //    populate the error message, selected account's Channel is not active
    private void ValidateInputData() {
        ValidationMessages.clear();

        String contactID = ApexPages.CurrentPage().getParameters().get('id');
        if (String.isBlank(contactID)) {
            ValidationMessages.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_CONT_ID_REQUIRED.name());
            return;
        }

        List<Contact> lC = new List<Contact>([SELECT Id, Account.PRMCompanyName__c, Account.PRMCountry__c, Account.PRMBusinessType__c, Account.PRMAreaOfFocus__c FROM Contact WHERE Id = :contactID]);
        if (!lc.isEmpty()) {
            Contact tC = lC[0];
        	if (String.isEmpty(tC.Account.PRMCountry__c) || String.isEmpty(tC.Account.PRMBusinessType__c) || String.isEmpty(tC.Account.PRMAreaOfFocus__c))
				ValidationMessages.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_CNTRY_CHNL_INFO_MISSING.name());

            List<PRMCountry__c> lPRMCntry = new List<PRMCountry__c>([SELECT Id, Name, (SELECT Id, ChannelCode__c, SubChannelCode__C FROM CountryChannels__r WHERE ChannelCode__c = :tC.Account.PRMBusinessType__c AND SubChannelCode__c = :tC.Account.PRMAreaOfFocus__c AND Active__c = True) FROM PRMCountry__c WHERE ((Country__c = :tC.Account.PRMCountry__c OR MemberCountry1__c = :tC.Account.PRMCountry__c OR MemberCountry2__c = :tC.Account.PRMCountry__c OR MemberCountry3__c = :tC.Account.PRMCountry__c OR MemberCountry4__c = :tC.Account.PRMCountry__c OR MemberCountry5__c = :tC.Account.PRMCountry__c) AND CountryPortalEnabled__c = True)]);
            if (lPRMCntry.isEmpty())
                ValidationMessages.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_PORTAL_NOT_DEPLOYED.name());
            else if (lPRMCntry[0].CountryChannels__r.isEmpty()){
                ValidationMessages.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_CHNL_NOTVALID.name());
            }
        }
        else
            ValidationMessages.add(AP_PRMAppConstants.RegistrationValidationErrors.ERR_CNT_INVALID.name());
    }
}