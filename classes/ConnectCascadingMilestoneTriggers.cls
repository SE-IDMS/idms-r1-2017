/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :  03-Feb-2012
    Modified Log        : Added Partner-Region on 09-May-2012
    Description         : Class for Connect CascadingMilestone Triggers, Assigns Champion Names  and Deployment Leader for Cascading Milestones
*/

public with sharing class ConnectCascadingMilestoneTriggers
{
    Public Static void CascadingMilestoneBeforeInsertUpdate(List<Cascading_Milestone__c> CMile)
      {
      
          System.Debug('****** CascadingMilestoneBeforeInsertUpdate Start ****'); 
          
          list<Team_Members__c> Champion = new List<Team_Members__c>();
          Champion = [select id,Team_Name__c, Entity__c,Champion_Name__c, Team_Member__r.FirstName, Team_Member__r.LastName,Team_Member__r.Email,GSC_Region__c, Partner_Region__c,Smart_Cities_Division__c from Team_Members__c where Program__c = :CMile[0].Program_Number__c and Year__c = :CMile[0].Year__c];
          for(Cascading_Milestone__c CM:CMile){
             for(Team_Members__c C: Champion){
             
                       
             if((CM.Program_Scope__c != null)&& (CM.Entity__c !=null)){
               If(CM.Program_Scope__c.Contains(C.Team_Name__c) && (CM.Entity__c.Contains(C.Entity__c))) {
                   CM.Champion_Name__c = C.Champion_Name__c; 
                                     
                    }
                    
                  // GSC-Region Deployment Leader Assignment
                  
              if(CM.Entity__c.Contains(Label.Connect_GSC) && (CM.GSC_Region__c !=null)) {
               
               If(CM.Program_Scope__c.Contains(C.Team_Name__c) && (CM.Entity__c.Contains(C.Entity__c)) && CM.GSC_Region__c.Contains(C.GSC_Region__c) ) {
                    System.Debug('Entity --' + CM.GSC_Region__c + '****' + C.GSC_Region__c + ' ' + CM.Entity__c + ' '+ C.Entity__c + CM.Program_Scope__c + ' ' + C.Team_Name__c + C.Team_Member__r.LastName);
                   CM.Deployment_User_Name__c = C.Team_Member__r.id;
                   //CM.Other_Deployment_Leader_Emailid__c = C.Team_Member__r.Email ;
                    }
                   }  
                   
                   if(CM.Entity__c.Contains(Label.Connect_Smart_Cities) && (CM.Smart_Cities_Division__c !=null)) {
               
               If(CM.Program_Scope__c.Contains(C.Team_Name__c) && (CM.Entity__c.Contains(C.Entity__c)) && CM.Smart_Cities_Division__c.Contains(C.Smart_Cities_Division__c) ) {
                    System.Debug('Entity --' + CM.GSC_Region__c + '****' + C.GSC_Region__c + ' ' + CM.Entity__c + ' '+ C.Entity__c + CM.Program_Scope__c + ' ' + C.Team_Name__c + C.Team_Member__r.LastName);
                   CM.Deployment_User_Name__c = C.Team_Member__r.id;
                   //CM.Other_Deployment_Leader_Emailid__c = C.Team_Member__r.Email ;
                    }
                   }  
                 
               if(!CM.Entity__c.Contains(Label.Connect_GSC)&& (!CM.Entity__c.Contains(Label.Connect_Partner))&& (!CM.Entity__c.Contains(Label.Connect_Smart_Cities))) {
             
               If(CM.Program_Scope__c.Contains(C.Team_Name__c) && (CM.Entity__c.Contains(C.Entity__c))) {
                   System.Debug('Entity --' + CM.GSC_Region__c + '****' + C.GSC_Region__c + ' ' + CM.Entity__c + ' '+ C.Entity__c + CM.Program_Scope__c + ' ' + C.Team_Name__c + C.Team_Member__r.LastName);
                    CM.Deployment_User_Name__c = C.Team_Member__r.id;
                   //CM.Other_Deployment_Leader_Emailid__c = C.Team_Member__r.Email ;
                    }
                   }  
                   
                 // Partner-Region Deployment Leader Assignment
                   
            if(CM.Entity__c.Contains(Label.Connect_Partner) && (CM.Partner_Region__c !=null)) {
             System.Debug('Entity --' + CM.Partner_Region__c + '****' + C.Partner_Region__c + ' ' + CM.Entity__c + ' '+ C.Entity__c + CM.Program_Scope__c + ' ' + C.Team_Name__c + C.Team_Member__r.LastName);
               If(CM.Program_Scope__c.Contains(C.Team_Name__c) && (CM.Entity__c == C.Entity__c) && CM.Partner_Region__c.Contains(C.Partner_Region__c) ) {
                   
                   System.Debug('Deployment Leader ' + C.Team_Member__r.FirstName);
                    CM.Deployment_User_Name__c = C.Team_Member__r.id;
                  // CM.Other_Deployment_Leader_Emailid__c = C.Team_Member__r.Email ;
                    }
                   }  
            
            /*if(CM.Entity__c.Contains(Label.Connect_GlobalOps) && (CM.Global_Operation_Regions__c !=null) && (C.Global_Operation_Regions__c !=null)) {
             
               If(CM.Program_Scope__c.Contains(C.Team_Name__c) && (CM.Entity__c.Contains(C.Entity__c)) && CM.Global_Operation_Regions__c.Contains(C.Global_Operation_Regions__c) ) {
                   System.Debug('Entity --' + CM.Global_Operation_Regions__c + '****' + C.Global_Operation_Regions__c + ' ' + CM.Entity__c + ' '+ C.Entity__c + CM.Program_Scope__c + ' ' + C.Team_Name__c + C.Team_Member__r.LastName);
                   System.Debug('Deployment Leader ' + C.Team_Member__r.FirstName);
                    CM.Deployment_User_Name__c = C.Team_Member__r.id;
                  // CM.Other_Deployment_Leader_Emailid__c = C.Team_Member__r.Email ;
                    }
                   } */
                   
                   
                 
               if(!CM.Entity__c.Contains(Label.Connect_Partner)&& (!CM.Entity__c.Contains(Label.Connect_GSC))&& (!CM.Entity__c.Contains(Label.Connect_Smart_Cities))) {
             
               If(CM.Program_Scope__c.Contains(C.Team_Name__c) && (CM.Entity__c.Contains(C.Entity__c))) {
                  CM.Deployment_User_Name__c = C.Team_Member__r.id;
                   //CM.Other_Deployment_Leader_Emailid__c = C.Team_Member__r.Email ;
                    }
                   }  
                   
                   } // Scope Validation If Ends              
                  } // For loop 1 Ends
                 } // For loop 2 Ends
                 
                 System.Debug('****** CascadingMilestoneBeforeInsertUpdate End ****'); 
                 
                } // Static Class Ends
                
                      Public Static void CascadingMilestoneBeforeUpdate(List<Cascading_Milestone__c> CMile)
      {
      
          System.Debug('****** CascadingMilestoneBeforeInsertUpdate Start ****'); 
           
             integer Count;
             integer Countfor;
          list<Team_Members__c> Champ = new List<Team_Members__c>();
          Champ = [select id,Team_Name__c, Entity__c,Champion_Name__c, Team_Member__r.FirstName, Team_Member__r.LastName,Team_Member__r.Email,GSC_Region__c, Partner_Region__c,Smart_Cities_Division__c  from Team_Members__c where Program__c = :CMile[0].Program_Number__c];
          for(Cascading_Milestone__c CMil:CMile){
           CMil.ValidateDeploymentLeader__c = false;
           countfor=0;
             for(Team_Members__c Ch: Champ){
             Count=0;
             
             
                If(CMil.Program_Scope__c.Contains(Ch.Team_Name__c) && (CMil.Entity__c.Contains(Ch.Entity__c))) {      
             if((CMil.Program_Scope__c != null)&& (CMil.Entity__c !=null)){
               If(CMil.Program_Scope__c.Contains(Ch.Team_Name__c) && (CMil.Entity__c.Contains(Ch.Entity__c))) {
                   CMil.Champion_Name__c = Ch.Champion_Name__c; 
                                     
                    }
                    
                  // GSC-Region Deployment Leader Assignment
                  
              if(CMil.Entity__c.Contains(Label.Connect_GSC) && (CMil.GSC_Region__c !=null)) {
               
               If(CMil.Program_Scope__c.Contains(Ch.Team_Name__c) && (CMil.Entity__c.Contains(Ch.Entity__c)) && CMil.GSC_Region__c.Contains(Ch.GSC_Region__c) ) {
                    System.Debug('Entity --' + CMil.GSC_Region__c + '****' + Ch.GSC_Region__c + ' ' + CMil.Entity__c + ' '+ Ch.Entity__c + CMil.Program_Scope__c + ' ' + Ch.Team_Name__c + Ch.Team_Member__r.LastName);
                         
                          //checking for two or more deployment leaders for single entity
                         
                         if(CMil.Deployment_User_Name__c != Ch.Team_Member__r.id){
                                  for(Team_Members__c C1: Champ){
                                  
                                  //comparing cascading milestone deployment leader with Deployment Network(if we have 2 or more deployemnt leaders for entity) 
                                  
                                     if((CMil.Deployment_User_Name__c == C1.Team_Member__r.id) && (CMil.Entity__c.Contains(C1.Entity__c))&& CMil.GSC_Region__c.Contains(C1.GSC_Region__c)){
                                        Count=Count+1;
                                       }
                                     }
                                     if(Count==0)
                                          {
                                           CMil.ValidateDeploymentLeader__c = true;
                                            }
                            }                                      
                                                 }
                     
                   }  
                   
                     // SCD-Region Deployment Leader Assignment
                  
              if(CMil.Entity__c.Contains(Label.Connect_Smart_Cities) && (CMil.Smart_Cities_Division__c !=null)) {
               
               If(CMil.Program_Scope__c.Contains(Ch.Team_Name__c) && (CMil.Entity__c.Contains(Ch.Entity__c)) && CMil.Smart_Cities_Division__c.Contains(Ch.Smart_Cities_Division__c) ) {
                    System.Debug('Entity --' + CMil.GSC_Region__c + '****' + Ch.GSC_Region__c + ' ' + CMil.Entity__c + ' '+ Ch.Entity__c + CMil.Program_Scope__c + ' ' + Ch.Team_Name__c + Ch.Team_Member__r.LastName);
                         
                          //checking for two or more deployment leaders for single entity
                         
                         if(CMil.Deployment_User_Name__c != Ch.Team_Member__r.id){
                                  for(Team_Members__c C5: Champ){
                                  
                                  //comparing cascading milestone deployment leader with Deployment Network(if we have 2 or more deployemnt leaders for entity) 
                                  
                                     if((CMil.Deployment_User_Name__c == C5.Team_Member__r.id) && (CMil.Entity__c.Contains(C5.Entity__c))&& CMil.Smart_Cities_Division__c.Contains(C5.Smart_Cities_Division__c)){
                                        Count=Count+1;
                                       }
                                     }
                                     if(Count==0)
                                          {
                                           CMil.ValidateDeploymentLeader__c = true;
                                            }
                            }                                      
                                                 }
                     
                   }  
                 
               if(!CMil.Entity__c.Contains(Label.Connect_GSC)&& (!CMil.Entity__c.Contains(Label.Connect_Partner))&& (!CMil.Entity__c.Contains(Label.Connect_Smart_Cities))) {
             
               If(CMil.Program_Scope__c.Contains(Ch.Team_Name__c) && (CMil.Entity__c.Contains(Ch.Entity__c))) {
                   System.Debug('Entity --' + CMil.GSC_Region__c + '****' + Ch.GSC_Region__c + ' ' + CMil.Entity__c + ' '+ Ch.Entity__c + CMil.Program_Scope__c + ' ' + Ch.Team_Name__c + Ch.Team_Member__r.LastName);
                    
                     //checking for two or more deployment leaders for single entity
                    
                    if(CMil.Deployment_User_Name__c != Ch.Team_Member__r.id){
                    
                     //comparing cascading milestone deployment leader with Deployment Network(if we have 2 or more deployemnt leaders for entity) 
                     
                                  for(Team_Members__c C2: Champ){
                                     if((CMil.Deployment_User_Name__c == C2.Team_Member__r.id) && (CMil.Entity__c.Contains(C2.Entity__c))){
                                        Count=Count+1;
                                       }
                                     }
                                     if(Count==0)
                                         {
                                        CMil.ValidateDeploymentLeader__c = true;
                                            }
                            }
                    }
                   }  
                   
                 // Partner-Region Deployment Leader Assignment
                   
            if(CMil.Entity__c.Contains(Label.Connect_Partner) && (CMil.Partner_Region__c !=null)) {
             
                If((CMil.Program_Scope__c == Ch.Team_Name__c) && (CMil.Entity__c == Ch.Entity__c) && (CMil.Partner_Region__c == Ch.Partner_Region__c) ) {
                   System.Debug('Entity --' + CMil.Partner_Region__c + '****' + Ch.Partner_Region__c + ' ' + CMil.Entity__c + ' '+ Ch.Entity__c + CMil.Program_Scope__c + ' ' + Ch.Team_Name__c + Ch.Team_Member__r.LastName);
                   System.Debug('Deployment Leader ' + Ch.Team_Member__r.FirstName);
                  
                   //checking for two or more deployment leaders for single entity
                    
                    if(CMil.Deployment_User_Name__c != Ch.Team_Member__r.id){
                    
                    //comparing cascading milestone deployment leader with Deployment Network(if we have 2 or more deployemnt leaders for entity) 
                                  
                                  for(Team_Members__c C3: Champ){
                                     if((CMil.Deployment_User_Name__c == C3.Team_Member__r.id) && (CMil.Entity__c.Contains(C3.Entity__c))&& CMil.Partner_Region__c.Contains(C3.Partner_Region__c)){
                                        Count=Count+1;
                                       }
                                     }
                                     if(Count==0)
                                         {
                                          CMil.ValidateDeploymentLeader__c = true;
                                          }
                            }
                    }
                   }  
                
                 // Global Operations Region Deployment Leader Assignment
                   
           /* if(CMil.Entity__c.Contains(Label.Connect_GlobalOps) && (CMil.Global_Operation_Regions__c !=null)) {
             
               If(CMil.Program_Scope__c.Contains(Ch.Team_Name__c) && (CMil.Entity__c.Contains(Ch.Entity__c)) && CMil.Global_Operation_Regions__c.Contains(Ch.Global_Operation_Regions__c) ) {
                   System.Debug('Entity --' + CMil.Global_Operation_Regions__c + '****' + Ch.Global_Operation_Regions__c + ' ' + CMil.Entity__c + ' '+ Ch.Entity__c + CMil.Program_Scope__c + ' ' + Ch.Team_Name__c + Ch.Team_Member__r.LastName);
                   System.Debug('Deployment Leader ' + Ch.Team_Member__r.FirstName);
                  
                   //checking for two or more deployment leaders for single entity
                    
                    if(CMil.Deployment_User_Name__c != Ch.Team_Member__r.id){
                    
                    //comparing cascading milestone deployment leader with Deployment Network(if we have 2 or more deployemnt leaders for entity) 
                                  
                                  for(Team_Members__c C3: Champ){
                                     if((CMil.Deployment_User_Name__c == C3.Team_Member__r.id) && (CMil.Entity__c.Contains(C3.Entity__c))&& CMil.Global_Operation_Regions__c.Contains(C3.Global_Operation_Regions__c)){
                                        Count=Count+1;
                                       }
                                     }
                                     if(Count==0)
                                         {
                                          CMil.ValidateDeploymentLeader__c = true;
                                          }
                            }
                    }
                   }  */
                
                 
               if(!CMil.Entity__c.Contains(Label.Connect_Partner)&& (!CMil.Entity__c.Contains(Label.Connect_GSC))&& (!CMil.Entity__c.Contains(Label.Connect_Smart_Cities))) {
             
               If(CMil.Program_Scope__c.Contains(Ch.Team_Name__c) && (CMil.Entity__c.Contains(Ch.Entity__c))) {
               
               //checking for two or more deployment leaders for single entity
               
               if(CMil.Deployment_User_Name__c != Ch.Team_Member__r.id){
               
              //comparing cascading milestone deployment leader with Deployment Network(if we have 2 or more deployemnt leaders for entity) 
              
              for(Team_Members__c C4: Champ){
              if((CMil.Deployment_User_Name__c == C4.Team_Member__r.id) && (CMil.Entity__c.Contains(C4.Entity__c))){
              Count=Count+1;
              }
              }
              if(Count==0)
                  {
                   CMil.ValidateDeploymentLeader__c = true;
                   }        
              }
                  
                    }
                   } 
                           
                   countfor=countfor+1; 
                   } // Scope Validation If Ends 
                               
                  }
                  
                  } // For loop 1 Ends
                  if(Countfor==0 && CMil.Deployment_User_Name__c !=null)
                  {
                   CMil.ValidateDeploymentLeader__c = true;
                   }        
                  
                 } // For loop 2 Ends
                 
                 System.Debug('****** CascadingMilestoneBeforeInsertUpdate End ****'); 
                 
                } // Static Class Ends
                
     
                
    /*Public Static void CascadingMilestoneBeforeUpdate_AppendTextColoumns(List<Cascading_Milestone__c> CMile) {
         
             System.Debug('****** CascadingMilestoneBeforeInsert_AppendTextColoumns Start ****');   
             
             
            List<Cascading_Milestone__c> CMilestones = new List<Cascading_Milestone__c>();
              CMilestones = [select Name, Roadblocks_Decision_to_be_taken_Summary__c,Achievements_in_past_quarter__c,Achievements_in_past_quarter_Summary__c from Cascading_Milestone__c];
               for(Cascading_Milestone__c M:CMile){
               
                            
                  if(M.Roadblock_decision_to_be__c == null)
                    M.Roadblock_decision_to_be__c = '';
                               
                   if(M.Achievements_in_past_quarter__c == null)
                     M.Achievements_in_past_quarter__c = '';
           
              for(Cascading_Milestone__c CM:CMilestones ){
              
               // Appends Roadblock / Decision to be taken column with the Roadblock /Decision to be (short) column 
              // if it is updated in the list view 
             
                   if(CM.Name == M.Name){
                        if( M.Roadblocks_Decision_to_be_taken_Summary__c != null){
                               M.Roadblock_decision_to_be__c = M.Roadblock_decision_to_be__c + '\n' + ' - ' + M.Roadblocks_Decision_to_be_taken_Summary__c;
                               M.Roadblocks_Decision_to_be_taken_Summary__c = null;
                               } // if Ends
                               
                       // Appends Achievement in past quarter column with the Achievements in past quarter summary column 
                       // if it is updated in the list view 
                 
                         if( M.Achievements_in_past_quarter_Summary__c != null){
                               M.Achievements_in_past_quarter__c = M.Achievements_in_past_quarter__c + '\n' + ' - ' + M.Achievements_in_past_quarter_Summary__c;
                               M.Achievements_in_past_quarter_Summary__c = null;
                               } // if Ends
                               
                            } // if Ends
             
                       } // For Loop Ends
                 
                       } // For Loop Ends
                       
                        System.Debug('****** CascadingMilestoneBeforeInsert_AppendTextColoumns Stop ****');   
              
              }*/
              
              Public Static void CascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity(List<Cascading_Milestone__c> CM)
              {
              
               
                  System.Debug('****** CascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity Start ****');   
                        
                  List<Connect_Milestones__c> Milestone = new List<Connect_Milestones__c>();
                  Milestone = [Select Name, Milestone_Name__c, Global_Functions__c, Power_Region__c, Global_Business__c, GSC_Regions__c,Partner_Region__c,Smart_Cities_Division__c from Connect_Milestones__c ];
                  integer GF; integer GB; integer PR; integer GSC; integer PAR; integer SCDs;
                  for(Cascading_Milestone__c C:CM){
                  GF = 0;
                  GB = 0;
                  PR = 0;
                  GSC = 0;
                  PAR = 0;
                  SCDs = 0;
                  C.ValidateScopeonInsertUpdate__c = false;
                  for(Connect_Milestones__c M: Milestone){
                         if(C.Milestones_Name__c == M.Milestone_Name__c){
                         System.Debug('Cascading Milestone ......!');
                           if(M.Global_Functions__c !=null){
                             if((C.Program_Scope__c == Label.Connect_Global_Functions) && !(M.Global_Functions__c.Contains(C.Entity__c)))
                               GF = GF + 1;
                            }
                            else if((M.Global_Functions__c == null)&& (C.Program_Scope__c == Label.Connect_Global_Functions))
                            GF = GF + 1;
                           
                           if(M.Global_Business__c !=null){
                              System.Debug(' Debug ... ! ' + M.Global_Business__c + ''  + C.Entity__c);
                             if((C.Program_Scope__c == Label.Connect_Global_Business) && !(M.Global_Business__c.Contains(C.Entity__c)))
                                GB = GB + 1;
                                
                             }
                             else if((M.Global_Business__c == null) && (C.Program_Scope__c == Label.Connect_Global_Business))
                             GB = GB + 1;
                             
                         
                             
                           if(M.Power_Region__c !=null) { 
                               if((C.Program_Scope__c == Label.Connect_Power_Region) && !(M.Power_Region__c.Contains(C.Entity__c)))
                               PR = PR + 1;
                              }
                               else if((M.Power_Region__c == null) && (C.Program_Scope__c == Label.Connect_Power_Region))
                               PR = PR + 1;
                              
                              
                              // GSC Region Validation
                              
                             if((M.GSC_Regions__c !=null)&& (C.GSC_Region__c !=null)){
                               if((C.Program_Scope__c == Label.Connect_Global_Functions) && (C.Entity__c == Label.Connect_GSC) && !(M.GSC_Regions__c.Contains(C.GSC_Region__c)))
                               GSC = GSC + 1;
                              } 
                               else if((M.GSC_Regions__c == null) && (C.Entity__c == Label.Connect_GSC) )
                               GSC = GSC + 1;
                               
                                // SCD Region Validation
                              
                             if((M.Smart_Cities_Division__c !=null)&& (C.Smart_Cities_Division__c !=null)){
                               if((C.Program_Scope__c == Label.Connect_Global_Business) && (C.Entity__c == Label.Connect_Smart_Cities) && !(M.Smart_Cities_Division__c.Contains(C.Smart_Cities_Division__c)))
                               SCDs = SCDs + 1;
                              } 
                               else if((M.Smart_Cities_Division__c == null) && (C.Entity__c == Label.Connect_Smart_Cities) )
                               SCDs = SCDs + 1;
                                                            
                               // Partner Region Validation
                             if((M.Partner_Region__c !=null)&& (C.Partner_Region__c !=null)){
                               if((C.Program_Scope__c == Label.Connect_Global_Business) && (C.Entity__c == Label.Connect_Partner) && !(M.Partner_Region__c.Contains(C.Partner_Region__c)))
                               PAR = PAR + 1;
                              } 
                               else if((M.Partner_Region__c == null) && (C.Entity__c == Label.Connect_Partner) )
                               PAR = PAR + 1;
                                                              
                               //Global Operation Region Validation
                             
                            /* if((M.Global_Operation_Regions__c !=null)&& (C.Global_Operation_Regions__c !=null)){
                               if((C.Program_Scope__c == Label.Connect_Power_Region) && (C.Entity__c == Label.Connect_GlobalOps) && !(M.Global_Operation_Regions__c.Contains(C.Global_Operation_Regions__c)))
                               GOR = GOR + 1;
                              } 
                               else if((M.Global_Operation_Regions__c == null) && (C.Entity__c == Label.Connect_GlobalOps) )
                               GOR = GOR + 1;*/
                               
                               } // if Ends
                               
                               System.Debug(' The Value of GB ... !' + GB);
                               System.Debug('GSC Value ' + GSC + M.GSC_Regions__c + 'Entity ' + C.Entity__c);
                               System.Debug('GF, GB,PR Value ' + GF + '' + GB + '' + PR + '' + GSC + '' + PAR + '' + SCDs + 'Entity ' + C.Entity__c);
                               if((GF > 0) ||  (GB > 0) ||  (PR > 0) || (GSC > 0) || (PAR > 0) || (SCDs > 0))
                                   C.ValidateScopeonInsertUpdate__c = True;
                               
                              } // For Ends
                               system.debug('Validate Scope ... ! ' + C.ValidateScopeonInsertUpdate__c);  
                             } //For Ends
                          
                             
                System.Debug('****** CascadingMilestoneBeforeInsertUpdate_ValidateScopeEntity Stop ****');                
                  
              }
              
               // Auto update Progress Summary on Completion of Cascading Milestones
             
             Public Static void CascadingMilestoneBeforeUpdate_ProgressSummaryUpdate(List<Cascading_Milestone__c> CMile) {
             
               for(Cascading_Milestone__c M:CMile){
               
                   if ((M.Progress_Summary_by_Champions__c !=null) && M.Progress_Summary_by_Champions__c.Contains('Completed')){
                        M.Progress_Summary_Deployment_Leader_Q2__c = M.Progress_Summary_by_Champions__c;
                        M.Progress_Summary_Deployment_Leader_Q3__c = M.Progress_Summary_by_Champions__c;
                        M.Progress_Summary_Deployment_Leader_Q4__c = M.Progress_Summary_by_Champions__c;
                        }
                        
                   if ( (M.Progress_Summary_Deployment_Leader_Q2__c != null) && M.Progress_Summary_Deployment_Leader_Q2__c.Contains('Completed')){
                        M.Progress_Summary_Deployment_Leader_Q3__c = M.Progress_Summary_Deployment_Leader_Q2__c;
                        M.Progress_Summary_Deployment_Leader_Q4__c = M.Progress_Summary_Deployment_Leader_Q2__c;
                        }
                        
                   if ( (M.Progress_Summary_Deployment_Leader_Q3__c !=null ) && M.Progress_Summary_Deployment_Leader_Q3__c.Contains('Completed')){
                        M.Progress_Summary_Deployment_Leader_Q4__c = M.Progress_Summary_Deployment_Leader_Q3__c;
                        }
                   
                    } // For Loop Ends
                  }
                           
              }