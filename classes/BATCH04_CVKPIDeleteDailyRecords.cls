/*********************************************************************************
 Class Name       : BATCH04_CVKPIDeleteDailyRecords
 Purpose          : Batch Apex to delete extra daily records that are not used for reporting. This batch will run on the 5 of every month
 Created by       : Siddharatha Nagavarapu Global Delivery Team
 Date created     : 25th October 2011
 Modified by      :
 Date Modified    :
 Remarks          : For Dec - 11 Release
 ********************************************************************************/

global class BATCH04_CVKPIDeleteDailyRecords implements Database.Batchable<sObject>,Schedulable {
/*=======================================
  VARIABLES AND PROPERTIES
=======================================*/
    private String queryString;
    private Id batchprocessid;
/*********************************************************************************
    Method Name      : execute
    Purpose          : This is the execute method of the schedulable interface which is used to Create a querystring for selecting all events
    with the following conditions IsMonthlyRecord = FALSE and run the batch with this query
    Created by       : Siddharatha Nagavarapu Global Delivery Team
    Date created     : 25th October 2011
    Modified by      :
    Date Modified    :
    Remarks          : For Dec - 11 Release
 ********************************************************************************/    
    
    global void execute(SchedulableContext sc){     
    
        createBatches();
    }
    
    global void createBatches()
    {
        Date test1=System.Today().addDays((System.Today().day() -1)*-1).addMonths(-2);
        if(Test.isRunningTest())                
        queryString='select id from CustomerVisitKPI__c where isMonthlyRecord__c=FALSE and date__c < '+(test1+'').substring(0,10)+' LIMIT 2';    
        else
        queryString='select id from CustomerVisitKPI__c where isMonthlyRecord__c=FALSE and date__c < '+(test1+'').substring(0,10);    
        BATCH04_CVKPIDeleteDailyRecords customerVisitBatch=new BATCH04_CVKPIDeleteDailyRecords(queryString);
        batchprocessid = Database.executeBatch(customerVisitBatch);
    }
/*********************************************************************************
    Method Name      : start
    Parameters       : Database.BatchableContext
    Purpose          : The start method is called at the beginning of a batch Apex job.This method collects the records or objects to be passed to the interface method execute.
                       This method returns either a Database.QueryLocator object or an iterable that contains the records or objects being 
                       passed into the job.
    Created by       : Siddharatha Nagavarapu Global Delivery Team
    Date created     : 25th October 2011
    Modified by      :
    Date Modified    :
    Remarks          : For Dec - 11 Release
 ********************************************************************************/    
       global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(queryString);
    }


    global BATCH04_CVKPIDeleteDailyRecords(){
    }
    
    global BATCH04_CVKPIDeleteDailyRecords(String query){
        this.queryString=query;
    }
/*********************************************************************************
    Method Name      : execute
    Parameters       : String
    Purpose          : The execute method is called for each batch of records passed to the method
                       This method takes the following:
                       It deletes the records which match the scope criteria.
    Created by       : Siddharatha Nagavarapu Global Delivery Team
    Date created     : 25th October 2011
    Modified by      :
    Date Modified    :
    Remarks          : For Dec - 11 Release
 ********************************************************************************/ 
    
       global void execute(Database.BatchableContext BC, List<sObject> scope){
           if(scope.size()>0)
            delete scope;         
    }    
/*********************************************************************************
    Method Name      : start
    Parameters       : Database.BatchableContext
    Purpose          : The finish method is called after all batches are processed. 
                       A detailed mail is sent to the mail id specified in the custom label CL00081 regarding the batch execution.
    Created by       : Siddharatha Nagavarapu Global Delivery Team
    Date created     : 25th October 2011
    Modified by      :
    Date Modified    :
    Remarks          : For Dec - 11 Release
 ********************************************************************************/       
    global void finish(Database.BatchableContext BC){
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems from AsyncApexJob where Id = :BC.getJobId()];
        //send email with any errors 
        if(a.NumberOfErrors > 0 || Test.isRunningTest()){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(System.Label.CL00081.split(','));
            mail.setSubject('report Regarding the monthly update of customer visit KPI\'s');
            mail.setPlainTextBody(System.Label.CL00084 + a.TotalJobItems + '/' + a.NumberOfErrors + '. ' + System.Label.CL00085 );
            
            if(!Test.isRunningTest())                          // ensure we never actually send an email during //a Test Run
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
     }
     
}