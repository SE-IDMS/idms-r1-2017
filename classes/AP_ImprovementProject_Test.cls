// Added by Uttara PR - NotesTobFO - Q3 2016 Release

@isTest
Private class AP_ImprovementProject_Test {

    static testMethod void createIP() {
        
        Map<String,CSQ_ImprovementProject__c> mapOrgIPrecord = new Map<String,CSQ_ImprovementProject__c>();
        
        BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                accOrg1.Name='Test';
                accOrg1.Entity__c='Test Entity-2'; 
                accOrg1.Operations__c = True;
                insert  accOrg1;
        BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                accOrg2.Name='Test';
                accOrg2.Entity__c='Test Entity-2'; 
                accOrg2.SubEntity__c='Test Sub-Entity 2';
                accOrg2.Operations__c = True;
                insert  accOrg2;
        BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                accOrg.Name='Test';
                accOrg.Entity__c='Test Entity-2';  
                accOrg.SubEntity__c='Test Sub-Entity 2';
                accOrg.Location__c='Test Location 2';
                accOrg.Location_Type__c='Design Center';
                accOrg.Operations__c = True;
                insert  accOrg;
        
        User runAsUser = Utils_TestMethods.createStandardUser('test111');
        insert runAsUser;
        
        CSQ_Profile__c CP = new CSQ_Profile__c();
                CP.Name = runAsUser.Name;
                CP.CSQ_bFOUser__c = runAsUser.Id;
                CP.CSQ_DefaultOrganization__c = accOrg.Id;
                insert CP;
        
        EntityStakeholder__c OrgSH = Utils_TestMethods.createEntityStakeholder(accOrg.Id,runAsUser.Id,'CI Deployment Leader');
        insert OrgSH;
        
        CSQ_ImprovementProject__c IP1 = new CSQ_ImprovementProject__c();
                IP1.CSQ_IPTitle__c = 'test1';
                IP1.CSQ_DetectionOrganization__c = accOrg.Id;
                IP1.CSQ_AccountableOrganization__c = accOrg.Id;
                IP1.CSQ_PointsOfDetection__c = 'Customer';
                IP1.CSQ_WhatIsTheProblem__c = 'what';
                IP1.CSQ_WhyIsItaProblem__c = 'why';
                IP1.CSQ_HowWasTheProblemDetected__c = 'how';
                IP1.CSQ_WhenWasTheProblemDetected__c = 'when';
                IP1.CSQ_WhoDetectedTheProblem__c = 'who';
                IP1.CSQ_WhereWasTheProblemDetected__c = 'where';
                IP1.CSQ_Severity__c = 'Minor';
                IP1.CSQ_ProjectStartDate__c = System.TODAY();
                insert IP1;
                
        mapOrgIPrecord.put(accOrg.Id,IP1);
        AP_ImprovementProject.SetProjectApprover(mapOrgIPrecord);
        
        Delete OrgSH;
        EntityStakeholder__c OrgSH2 = Utils_TestMethods.createEntityStakeholder(accOrg2.Id,runAsUser.Id,'CI Deployment Leader');
        insert OrgSH2;
        
        CSQ_ImprovementProject__c IP2 = new CSQ_ImprovementProject__c();
                IP2.CSQ_IPTitle__c = 'test1';
                IP2.CSQ_DetectionOrganization__c = accOrg.Id;
                IP2.CSQ_AccountableOrganization__c = accOrg.Id;
                IP2.CSQ_PointsOfDetection__c = 'Customer';
                IP2.CSQ_WhatIsTheProblem__c = 'what';
                IP2.CSQ_WhyIsItaProblem__c = 'why';
                IP2.CSQ_HowWasTheProblemDetected__c = 'how';
                IP2.CSQ_WhenWasTheProblemDetected__c = 'when';
                IP2.CSQ_WhoDetectedTheProblem__c = 'who';
                IP2.CSQ_WhereWasTheProblemDetected__c = 'where';
                IP2.CSQ_Severity__c = 'Minor';
                IP2.CSQ_ProjectStartDate__c = System.TODAY();
                insert IP2; 

        mapOrgIPrecord.put(accOrg.Id,IP2);
        AP_ImprovementProject.SetProjectApprover(mapOrgIPrecord);               
                
        Delete OrgSH2;
        
        EntityStakeholder__c OrgSH1 = Utils_TestMethods.createEntityStakeholder(accOrg1.Id,runAsUser.Id,'CI Deployment Leader');
        insert OrgSH1;
        
        CSQ_ImprovementProject__c IP3 = new CSQ_ImprovementProject__c();
                IP3.CSQ_IPTitle__c = 'test1';
                IP3.CSQ_DetectionOrganization__c = accOrg.Id;
                IP3.CSQ_AccountableOrganization__c = accOrg.Id;
                IP3.CSQ_PointsOfDetection__c = 'Customer';
                IP3.CSQ_WhatIsTheProblem__c = 'what';
                IP3.CSQ_WhyIsItaProblem__c = 'why';
                IP3.CSQ_HowWasTheProblemDetected__c = 'how';
                IP3.CSQ_WhenWasTheProblemDetected__c = 'when';
                IP3.CSQ_WhoDetectedTheProblem__c = 'who';
                IP3.CSQ_WhereWasTheProblemDetected__c = 'where';
                IP3.CSQ_Severity__c = 'Minor';
                IP3.CSQ_ProjectStartDate__c = System.TODAY();
                insert IP3;
                
        mapOrgIPrecord.put(accOrg.Id,IP3);
        AP_ImprovementProject.SetProjectApprover(mapOrgIPrecord);   
                
        Delete OrgSH1;
        
        EntityStakeholder__c OrgSHcsq = Utils_TestMethods.createEntityStakeholder(accOrg.Id,runAsUser.Id,'CS&Q Manager');
        insert OrgSHcsq;
        
        CSQ_ImprovementProject__c IP4 = new CSQ_ImprovementProject__c();
                IP4.CSQ_IPTitle__c = 'test1';
                IP4.CSQ_DetectionOrganization__c = accOrg.Id;
                IP4.CSQ_AccountableOrganization__c = accOrg.Id;
                IP4.CSQ_PointsOfDetection__c = 'Customer';
                IP4.CSQ_WhatIsTheProblem__c = 'what';
                IP4.CSQ_WhyIsItaProblem__c = 'why';
                IP4.CSQ_HowWasTheProblemDetected__c = 'how';
                IP4.CSQ_WhenWasTheProblemDetected__c = 'when';
                IP4.CSQ_WhoDetectedTheProblem__c = 'who';
                IP4.CSQ_WhereWasTheProblemDetected__c = 'where';
                IP4.CSQ_Severity__c = 'Minor';
                IP4.CSQ_ProjectStartDate__c = System.TODAY();
                insert IP4; 
                
        mapOrgIPrecord.put(accOrg.Id,IP4);
        AP_ImprovementProject.SetProjectApprover(mapOrgIPrecord);  
        
        CP.CSQ_DefaultOrganization__c = accOrg1.Id;
        update CP;
        IP4.CSQ_DetectionOrganization__c = accOrg1.Id;
        IP4.CSQ_AccountableOrganization__c = accOrg1.Id;
        update IP4;
        
        Test.startTest();
        CSQ_Measure__c ms = new CSQ_Measure__c(CSQ_ImprovementProject__c = IP1.Id, Name = 'testMeasure', CSQ_Unit__c = 'm', CSQ_Type__c = 'Improve Efficiency');  
        insert ms;
        IP1.CSQ_Measure_MilestoneReviewCompleted__c = true;
        IP1.CSQ_ControlMilestoneReviewCompleted__c = true;
        update IP1;
        Map<String,CSQ_ImprovementProject__c> mapIPid1 = new Map<String,CSQ_ImprovementProject__c>();
        Map<String,CSQ_ImprovementProject__c> mapIPid2 = new Map<String,CSQ_ImprovementProject__c>();
        mapIPid1.put(IP1.Id,IP1);
        mapIPid2.put(IP1.Id,IP1);
        AP_ImprovementProject.checkMeasures(mapIPid1, mapIPid2);
		Test.stopTest();        
    }
}