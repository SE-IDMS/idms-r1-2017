public with sharing class VFC_ProgramTargetCountries {
    
    public List<PartnerProgramCountries__c> lstTargetedCountries {get;set;} {lstTargetedCountries = new List<PartnerProgramCountries__c>();}
    public VFC_ProgramTargetCountries(ApexPages.StandardController cont) 
    {
        lstTargetedCountries = [SELECT country__c, PartnerProgram__c, Country__r.name, PartnerProgram__r.name FROM PartnerProgramCountries__c WHERE PartnerProgram__c=:cont.getId() ORDER BY country__r.name ASC];
        
    }//End of constructor

}//End of class