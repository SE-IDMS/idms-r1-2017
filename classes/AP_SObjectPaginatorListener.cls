global interface AP_SObjectPaginatorListener {
    void handlePageChange(List<SObject> newPage);
}