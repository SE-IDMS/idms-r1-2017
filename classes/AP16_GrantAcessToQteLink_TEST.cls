/*
    Author          : Abhishek (ACN) 
    Date Created    : 25/05/2011
    Description     : Test class for the apex class AP16_GrantAcessToQteLink.
*/


    @isTest

    private class AP16_GrantAcessToQteLink_TEST {

/* @Method <This method CaseTriggerTestMethod is used test the AP16_GrantAcessToQteLink>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
*/
   static testMethod void GrantAcessToQteLink_TestMethod()
   {
     User sysadmin1=Utils_TestMethods.createStandardUser('sys1');
       sysadmin1.isActive=true;
     User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
       sysadmin2.isActive=true;
     Opportunity opportunity;
     List<User> user=new List<User>();
     User Approver;
     System.runAs(sysadmin1){
   // insert new Country__c(Name='France',CountryCode__c='FR');
        Account accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts); 

       opportunity= Utils_TestMethods.createOpportunity(accounts.id);
        Database.insert(opportunity);        

     //   Profile profile = [select id from profile where name='SE - Non-sales RITE Approver'];
    }
    System.runAs(sysadmin2){
        UserandPermissionSets delegatedApproveruserandpermissionsetrecord=new UserandPermissionSets('user','SE - Non-sales RITE Approver');
        User delegatedApprover = delegatedApproveruserandpermissionsetrecord.userrecord;
        Database.insert(delegatedApprover);

        List<PermissionSetAssignment> permissionsetassignmentlstfordelegatedapprover=new List<PermissionSetAssignment>();    
        for(Id permissionsetId:delegatedApproveruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
            permissionsetassignmentlstfordelegatedapprover.add(new PermissionSetAssignment(AssigneeId=delegatedApprover.id,PermissionSetId=permissionsetId));
        if(permissionsetassignmentlstfordelegatedapprover.size()>0)
    Database.insert(permissionsetassignmentlstfordelegatedapprover);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  

       // user delegatedApprover = Utils_TestMethods.createStandardUser(profile.id,'1Test');
       // database.insert(delegatedApprover);

Map<String,User> aliasnametousermap=new Map<String,User>();   
Map<String,UserandPermissionSets> aliasnametouserpermissionsetrecordmap=new Map<String,UserandPermissionSets>();   
for(integer i=1;i<=11;i++)
{
    UserandPermissionSets newuserandpermissionsetrecord=new UserandPermissionSets('Users'+i,'SE - Non-sales RITE Approver');
    user newuser = newuserandpermissionsetrecord.userrecord;
    newuser.delegatedApproverId=delegatedApprover.id;
    aliasnametouserpermissionsetrecordmap.put('Users'+i,newuserandpermissionsetrecord);
    aliasnametousermap.put('Users'+i,newuser);
}
Database.insert(aliasnametousermap.values());     
user.addAll(aliasnametousermap.values());
List<PermissionSetAssignment> permissionsetassignmentlstforusers=new List<PermissionSetAssignment>();    
for(String useralias:aliasnametouserpermissionsetrecordmap.keySet())
{            
    for(Id permissionsetId:aliasnametouserpermissionsetrecordmap.get(useralias).permissionsetandprofilegrouprecord.permissionsetIds)
        permissionsetassignmentlstforusers.add(new PermissionSetAssignment(AssigneeId=aliasnametousermap.get(useralias).id,PermissionSetId=permissionsetId));    
} 
if(permissionsetassignmentlstforusers.size()>0)
    Database.insert(permissionsetassignmentlstforusers);  

        //user Approver = Utils_TestMethods.createStandardUser(profile.id,'Testing');
       // Approver.delegatedApproverId=user[0].id;        
       // database.insert(Approver);

UserandPermissionSets Approveruserandpermissionsetrecord=new UserandPermissionSets('appr','SE - Non-sales RITE Approver');
Approver = Approveruserandpermissionsetrecord.userrecord;
Database.insert(Approver);

List<PermissionSetAssignment> permissionsetassignmentlstforApprover=new List<PermissionSetAssignment>();    
for(Id permissionsetId:Approveruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
    permissionsetassignmentlstforApprover.add(new PermissionSetAssignment(AssigneeId=Approver.id,PermissionSetId=permissionsetId));
if(permissionsetassignmentlstforApprover.size()>0)
    Database.insert(permissionsetassignmentlstforApprover);  
}
System.runAs(sysadmin1){
    test.StartTest();
    list<OPP_QuoteLink__c> quoteLink = new list<OPP_QuoteLink__c >();
    for(integer i=1;i<=200;i++)
    {
        OPP_QuoteLink__c newQuoteLink=Utils_TestMethods.createQuoteLink(opportunity.id);
        newQuoteLink.approver1__c=user[0].id;
        newQuoteLink.approver2__c=user[1].id;
        newQuoteLink.approver3__c=user[2].id;
        newQuoteLink.approver4__c=user[3].id;
        newQuoteLink.approver5__c=user[4].id;
        newQuoteLink.approver6__c=user[5].id;
        newQuoteLink.approver7__c=user[6].id;
        newQuoteLink.approver8__c=user[7].id;
        newQuoteLink.approver9__c=user[8].id;
        newQuoteLink.approver10__c=approver.id;        
        quoteLink.add(newQuoteLink);
    }
    Database.insert(quoteLink);

    list<OPP_QuoteLink__c> qteLinkToUpdt= new list<OPP_QuoteLink__c>();
    for(OPP_QuoteLink__c qteLink:quoteLink)
    {
        qteLink.approver1__c=user[9].id;
        qteLink.approver2__c=user[8].id;
        qteLink.approver3__c=user[7].id;
        qteLink.approver4__c=user[6].id;
        qteLink.approver5__c=user[5].id;
        qteLink.approver6__c=user[4].id;
        qteLink.approver7__c=user[3].id;
        qteLink.approver8__c=user[2].id;
        qteLink.approver9__c=user[1].id;
        qteLink.approver10__c=user[0].id;   
        qteLinkToUpdt.add(qteLink);   
    }
    Database.Update(qteLinkToUpdt);

                    //Following conditions are commented because the too many statements are performed and the governor limits are reached.
                    //No additional class have been created because the class code coverage is not impacted by this change.
                    //Please let me know for any remark.
                    //Jules Faligot (jules.faligot@accenture.com)

                    /*list<OPP_QuoteLink__c> qteLinkUpdt= new list<OPP_QuoteLink__c>();
                    for(OPP_QuoteLink__c qteLink:quoteLink)
                    {
                    qteLink.approver1__c=user[9].id;
                    qteLink.approver2__c=user[9].id;
                    qteLink.approver3__c=user[10].id;   
                    qteLink.approver5__c=approver.id;        
                    qteLinkUpdt.add(qteLink);   
                    }
                    Database.Update(qteLinkUpdt);*/
                    test.StopTest(); 
        }
    }
    
    static testMethod void GrantAcessToQteLink_TestMethod1() {
        Account accounts = Utils_TestMethods.createAccount();
        Database.insert(accounts); 

        Opportunity opportunity = Utils_TestMethods.createOpportunity(accounts.id);
        Database.insert(opportunity);
        
        User sysadmin1=Utils_TestMethods.createStandardUser('sys1');
        sysadmin1.isActive=true;
        Database.insert(sysadmin1);
        User sysadmin2=Utils_TestMethods.createStandardUser('sys1');
        sysadmin2.isActive=true;
        Database.insert(sysadmin2);
       
        OPP_QuoteLink__c newQuoteLink=Utils_TestMethods.createQuoteLink(opportunity.id);
        newQuoteLink.approver1__c=sysadmin1.id;
        newQuoteLink.TypeOfOffer__c =null;
        Database.insert(newQuoteLink);
        
        OPP_supportRequest__c supReq = Utils_TestMethods.createSupportRequest(opportunity.Id);
        supReq.PMTender__c= sysadmin1.Id;
        supReq.AssignedToUser__c=sysadmin2.Id;
        supReq.QuoteLink__c = newQuoteLink.Id;
        supReq.AppDevEngineer__c=sysadmin2.Id;
        supReq.SolutionArchitect__c=sysadmin2.Id;
        supReq.SupportUser1__c=sysadmin2.Id;
        supReq.SupportUser2__c=sysadmin2.Id;
        supReq.TypeOfOffer__c ='Bid to Bid';
        Database.insert(supReq);
        test.starttest();
        newQuoteLink.TypeOfOffer__c =null;
        Database.update(newQuoteLink);
        
        supReq.PMTender__c= UserInfo.getUserId();
        supReq.AssignedToUser__c=UserInfo.getUserId();
        supReq.AppDevEngineer__c=UserInfo.getUserId();
        supReq.SolutionArchitect__c=UserInfo.getUserId();
        supReq.SupportUser1__c=UserInfo.getUserId();
        supReq.SupportUser2__c=UserInfo.getUserId();
        supReq.TypeOfOffer__c ='Bid to Bid';
        update supReq; 
        test.stoptest();   
    }
}