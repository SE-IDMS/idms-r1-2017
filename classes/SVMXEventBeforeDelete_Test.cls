@isTest(SeeAllData=true)
class SVMXEventBeforeDelete_Test{

    static testMethod void myUnitTest() {
    test.starttest();
   Profile p1 = [select id from profile where name = 'System Administrator' limit 1];    
        
        
       Account Acc = new Account    (           Name='ACC001',                                   
                                                    POBox__c= 'POBox1',
                                                    POBoxZip__c='POBoxZip',
                                                    ZipCode__c = '11111');
        insert Acc;
                                       
        //Contact test data
        
        Contact Cnct = new Contact(
                                                    AccountId=Acc.Id,
                                                    email='UTCont@mail.com',
                                                    CorrespLang__c='EN',
                                                    phone='1234567890',
                                                    LastName='UTContact001',
                                                    FirstName='Fname1');
        insert Cnct;   
        
         //Location test data
        
        SVMXC__Site__c loc1=    new SVMXC__Site__c(
                                                     AddressLine2__c='Add Lin 1',
                                                     SVMXC__City__c='Bangalore',                                                   
                                                     HealthAndSafety__c='Drug Test',
                                                     SVMXC__Account__c=Acc.id,
                                                     SVMXC__Zip__c='1234'    );
        insert loc1;
         //Service Team test data
        
        SVMXC__Service_Group__c st1= new SVMXC__Service_Group__c(
                                                     Name='St1',
                                                     SVMXC__Active__c=true,
                                                     SVMXC__Group_Type__c='Internal',
                                                     SVMXC__Group_Code__c='test213');
        insert st1;                                          

        User newUser = new User(alias = 'alias1', 
                                email = 'maxexpress@servicemax.com', 
                                emailencodingkey = 'UTF-8', 
                                lastname = 'ps.servicemax@gmail.com',
                                languagelocalekey = 'en_US',    
                                localesidkey = 'en_US', 
                                profileid = p1.id, 
                                timezonesidkey = 'America/Los_Angeles', 
                                username = 'svmx3@testorg.com'
                               
                               );

      insert newUser; 
        
                                                                                                      
 SVMXC__Service_Group_Members__c tech1= new          SVMXC__Service_Group_Members__c(
                                                     SVMXC__Service_Group__c=st1.id,
                                                     Name='John',
                                                     SVMXC__Active__c=true,
                                                    // SVMXC__Salesforce_User__c=newUser.id,
                                                     RecordTypeName__c='tech'                                                    
                                                    );  
            
        insert tech1;
        
       
SVMXC__Service_Order__c WO2 =     new SVMXC__Service_Order__c(                                       
                                                     SVMXC__Company__c=Acc.Id,
                                                     SVMXC__Order_Status__c = 'Rescheduled', 
                                                     BackOfficeReference__c='1234', 
                                                     SVMXC__Contact__c = Cnct.Id, 
                                                     SVMXC__Group_Member__c=tech1.id,
                                                     RescheduleReason__c='Weather Related',
                                                     Service_Business_Unit__c='Energy',
                                                     Parent_Work_Order__c = null,
                                                     Is_Billable__c=true,
                                                     SVMXC__Locked_By_DC__c=true,
                                                    
                                                     Customer_Service_Request_Time__c= system.now().addDays(1));
       
          insert WO2;  
          
 
   
  AssignedToolsTechnicians__c assTech = New AssignedToolsTechnicians__c( WorkOrder__c=WO2.Id,
                                                                         TechnicianEquipment__c=tech1.Id,
                                                                         Status__c='test',
                                                                         PrimaryUser__c=true
                                                                        );
                                                                        
//Insert  assTech; 
  
SVMXC__Dispatcher_Access__c dAccs = New SVMXC__Dispatcher_Access__c( SVMXC__Dispatcher__c= newUser.Id,
                                                                     SVMXC__Service_Team__c=st1.Id,
                                                                     Read_Only__c=true
                                                                      ) ;
insert  dAccs;
update dAccs;

          SVMXC__SVMX_Event__c sevent = New SVMXC__SVMX_Event__c(
                                                        Name='WO-1233',
                                                        SVMXC__Service_Team__c=st1.Id,
                                                        SVMXC__Technician__c=tech1.Id,
                                                        SVMXC__WhatId__c = WO2.Id
                                                        );
   insert sevent;
   update sevent;                                                                 
       
    delete sevent;
  
  test.stoptest();    
    }

}