/*
    09-Jun-2016
    Pooja Suresh  
    Q2 2016 Release
    Test Class for B2S Data fillup batch classes : BATCH_B2SUpdate_Account_Re_Populate, BATCH_B2SUpdate_Contact_Re_Populate
*/

@isTest
private class BATCH_B2SUpdate_TEST {
    static testMethod void myUnitTest() {
                         
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
        insert c;
        
        Account acc1 = new Account(Name='TestAccount1', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '11');
        insert acc1;
        Account acc2 = new Account(Name='TestAccount2', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '12');
        insert acc2;
        Account acc3 = new Account(Name='TestAccount3', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id, SEAccountId__c = '13');
        insert acc3;

        BATCH_B2SUpdate_Account_Re_Populate batchAcc = new BATCH_B2SUpdate_Account_Re_Populate('Select City__c, Street__c, ZipCode__c from Account where Country__c!=null'); //TECH_SDH_StateProvCode__c, TECH_SDH_CountryCode__c
        database.executeBatch(batchAcc);

        Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc1.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',SEContactId__c = '123456',PRMOnly__c = true, PRMUIMSID__c = '123',
                email='test.test@test.com');
        insert c1;
        
        Contact c2 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc1.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',PRMOnly__c = false, PRMUIMSSEContactId__c = '123456',
                email='test.test@test.com');
        insert c2;

        BATCH_B2SUpdate_Contact_Re_Populate batchCon = new BATCH_B2SUpdate_Contact_Re_Populate('Select City__c, Street__c, ZipCode__c from Contact where Country__c!=null'); //TECH_SDH_StateProvCode__c, TECH_SDH_CountryCode__c
        database.executeBatch(batchCon);

    }

}