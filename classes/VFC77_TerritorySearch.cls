/*
    Author          : Accenture Team
    Date Created    : 6/2/2012
    Description     : Controller extensions for VFC77_TerritorySearch.This class fetches the search 
                      parameters from the component controller, performs the search by calling factory methods  
*/

public with sharing class VFC77_TerritorySearch extends VFC_ControllerBase{

    //Iniatialize variables
    //private SE_Territory__c currentTerr = new SE_Territory__c();
    private Account currentAcc = new Account();
    public String currentAccountCountry {get;set;}
    public String currentCountryID {get;set;}
    public Utils_DataSource dataSource;
    public List<SelectOption> columns{get;set;}
    public List<DataTemplate__c> fetchedRecords{get;set;}
    public String interfaceType{get;set;}
    public List<String> keywords{get; set;} 
    public List<String> filters{get; set;}    
    public String searchKeyword{get; set;}
    public Boolean DisplayResults{get;set;}
    public Utils_DataSource.Result searchResult ;
    Public Utils_PicklistManager plManager;
    public string searchText{get;set;}       
    public String level1{set; get;}
    public String level2{get; set;}

    Public List<String> Labels{get;set;}
    public String level1Label{set; get{return Labels[0];}}
    public String level2Label{set; get{return Labels[1];}}

    public List<SelectOption> items1{set; get;}
    public List<SelectOption> items2{set; get;}

    List<DataTemplate__c> Territory= New List<DataTemplate__c>();
    public Boolean flag;
    
    public Boolean displayError{get;set;} 
    public List<accountTeamMember> accTeam = New List<accountTeamMember>();     
      
    
    public VCC06_DisplaySearchResults resultsController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                system.debug('--------displaySearchResults -------'+displaySearchResults );                
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }
     
     
     //Constructor

    public VFC77_TerritorySearch(ApexPages.StandardController controller) {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC77_TerritorySearch******');
        currentAcc = (Account)controller.getRecord();
        
        // Retrieve the ID of the related Account through the URL
        String ACCOUNTID = system.currentpagereference().getParameters().get('id');  
        System.Debug('####################### ACCOUNTID : '+ACCOUNTID );
        
        currentCountryID = [SELECT ID, Country__c FROM Account WHERE ID=:ACCOUNTID ].Country__c ;
        currentAccountCountry = [SELECT ID, NAME FROM Country__c WHERE ID=:currentCountryID ].NAME;
        
        if(ACCOUNTID  != null)
        {
            
            System.debug('#### ACCOUNTID = ' + ACCOUNTID );
        }    
        
        keywords = new List<String>();
        filters = new List<String>(); 
        columns = new List<SelectOption>();
        Labels = new List<String>();
        interfaceType = 'TERR';
        
        flag = false;
        displayError = false;
                
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
        
        dataSource = Utils_Factory.getDataSource(interfaceType);
        columns = new List<SelectOption>();
        columns = dataSource.getColumns();
        plManager = Utils_Factory.getPickListManager(interfaceType);
        
        if( plManager != null)
        {
            Labels = plManager.getPickListLabels();
            items1 = plManager.getPicklistValues(1, null);
            items2 = plManager.getPicklistValues(2, currentCountryID );
        }
        
        fetchedRecords = new List<DataTemplate__c>();
        System.Debug('****** Initializing the Properties and Variables Ends for VFC77_TerritorySearch******');
        
    }
    
    
    public PageReference checkAccess()
    {
        /*for(Profile prof : [Select Id, Name from Profile where Name Like :System.Label.CL00498])                
        {
            if(userinfo.getProfileId() == prof.Id)
            {
                flag=true;                
            }
            else
                flag=False;
        }*/
        String profileName=[select Name from Profile where Id=:UserInfo.getProfileId()].Name;//the profile name of the current User is retreived
        system.debug('profile name :' + profileName);
        List<String> profilePrefixList=(System.Label.CL00632).split(',');//the valid profile prefix list is populated
        for(String profilePrefix : profilePrefixList)//The profile name should start with the prefix else the displayerrormessage flag is set to true
        {
            system.debug('prefix :' + profilePrefix);
            if((profileName+'').startsWith(profilePrefix)){
                flag=true;
                break;              
            }  
            else{
                flag=False;
            }
            system.debug('flag: '+ flag);
        }
        String accId = system.currentpagereference().getParameters().get('ID');  
        system.debug('################# userinfo.getuserId '+ userinfo.getuserId());
        system.debug('################# owneraccountid '+ [Select OwnerId, Id from Account where Id = :accId LIMIT 1].OwnerId);
        system.debug('################# acc Team '+ [select accountID,userId from accountTeamMember where AccountID =: accId and accountAccessLevel =:Label.CL00203]);
                
        if(!flag && accId != NULL)
        {
            if(userinfo.getuserId() == [Select OwnerId, Id from Account where Id = :accId LIMIT 1].OwnerId)
            {
                
                flag = True;
            }            
            else 
            {    
                accTeam = [select accountID,userId from accountTeamMember where AccountID =: accId and accountAccessLevel =:Label.CL00203];
            
                if(!accTeam.isEmpty())
                {
                    for(accountTeamMember team : accTeam)
                    {
                        if(userinfo.getuserId() == team.userId)
                            flag = True;
                    }
                }
            }            
        }
        
        if(!flag && accId != NULL)
        {    
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00562+'. '+Label.CL00563));
            displayError = true;         
        }
        else
        {
            displayError = false;    
        }
        return NULL;
    }
    
    
    
    public PageReference  search() 
    {     
    /* This method will be called on click of "Search" Button.This method calls the factory methods
     * and fetches the search results
     */
       System.Debug('****** Searching Begins  for VFC77_TerritorySearch******');
        if(resultsController.searchResults!=null)
            resultsController.searchResults.clear();

        fetchedRecords.clear();
        keywords = new List<String>();
        filters = new List<String>();
        if(searchText!=null)
            keywords.add(searchText.trim());
        if(level1!=null)
            filters.add(level1);
        if(level2!=null)
            filters.add(level2);
            
        filters.add(currentCountryID);    
            
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
        if(keywords.size()!=0 || filters.size()!=0)
        {
            if(!filters[0].equalsIgnoreCase(Label.CL00355) || !filters[1].equalsIgnoreCase(Label.CL00355) || keywords[0].length()>0)
            {
                try
                {
                    System.debug('#### Filters : ' + filters);
                    //Makes the Datasource call for search                    
                    searchResult = dataSource.Search(keywords,filters);
                    fetchedRecords = searchResult.recordList;
                    System.debug('Data source Result'+fetchedRecords);
                    System.debug(' Result'+searchResult);
                    DisplayResults = true;
                    
                    if(searchResult.NumberOfRecord > 100)
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00349));
                }
                catch(Exception exc)
                {
                    system.debug('Exception while calling Competitor Search '+ exc.getMessage() );
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00398));
                }                
            }
            else if(filters[0].equalsIgnoreCase(Label.CL00355) && filters[1].equalsIgnoreCase(Label.CL00355) && keywords[0].length()==0)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL10105));

            }             
        }         
        System.Debug('****** Searching Ends  for VFC50_CompetitorSearch******');        
        return null;          
    }
    
    /* This method will be called on click of "Clear" Button.This method clears the value in the search text box
     * and the values in picklists
     */
    public PageReference clear() 
    {
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Begins  for VFC77_TerritorySearch******');     
        fetchedRecords = new List<DataTemplate__c>();
        searchText = null;
        keywords = new List<String>();
        filters = new List<String>();        
        DisplayResults = false;
        level1 = null;
        level2 = null;
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Ends for VFC77_TerritorySearch******');     
        return null;
    }
    
    
     /* This method will be called from the component controller on click of "Select" link.This method correspondingly inserts/updates
     * the competitor line.
     */
    public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
    {
        System.Debug('****** Insertion/Updation of Territory Begins******');
    
        VFC77_TerritorySearch thisController = (VFC77_TerritorySearch)controllerBase;
        
        DataTemplate__c  Territory = (DataTemplate__c)Obj;
         
        if(Territory.field6__c != null)
            {
            currentAcc.SE_Territory__c=Territory.field6__c;
            }
            
        update currentAcc;
        System.Debug('****** Insertion/Updation of Territory Ends******');
    return new pagereference('/'+thisController.currentAcc.Id);
    }
    
    public pagereference cancel()
    {
        System.Debug('****** Cancel Method Begins  for  VFC77_TerritorySearch******');
        
        pagereference pg;
        if(system.currentpagereference().getParameters().get('ID')!=null)
            pg = new pagereference('/'+system.currentpagereference().getParameters().get('ID'));
        system.debug('------Page reference -----'+pg);
        System.Debug('****** Cancel Method Ends for  VFC77_TerritorySearch******');
        if(pg!=null)
            return pg;
        else
            return null;
    } 
    
}