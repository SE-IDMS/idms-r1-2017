/************************************************************************************
    Created By : Nicolas PALITZYNE
    Created Date : 28 November 2016
    Description : Apex controller for the PRM to Business Account conversion screen. 
    
    Release    : November Release 2016, BR-8145, CCC Feature
*************************************************************************************/
public class VFC_ConvertLevel1 {

    public Account account {get;set;}
    public Contact contact {get;set;}    

    public VFC_ConvertLevel1() {
        
        Id contactId = ApexPages.currentPage().getParameters().get('contactId');

        contact = [SELECT Id, FirstName, AccountId, 
                   LastName, Name, MobilePhone, Email 
                   FROM Contact WHERE Id = :contactId LIMIT 1];
                   
        account = [SELECT Name, Street__c, AdditionalAddress__c, ClassLevel1__c, ClassLevel2__c, Phone, 
                   MarketSegment__c, PRMStreet__c, PRMAdditionalAddress__c, PRMAreaOfFocus__c, PRMBusinessType__c,
                   PRMCity__c, PRMCompanyName__c, PRMCompanyPhone__c, PRMWebsite__c, PRMCountry__c, 
                   PRMCurrencyIsoCode__c, PRMEmployeeSize__c, PRMCorporateHeadquarters__c, PRMMarketServed__c, PRMAccType__c,                    
                   PRMStateProvince__c, PRMTaxId__c, PRMAnnualSales__c, PRMZipCode__c, LeadingBusiness__c,                   
                   ZipCode__c, isPersonAccount, City__c, ConWorkPhone__c, Website, EmployeeSize__c, 
                   CorporateHeadquarters__c, AccType__c, VATNumber__c, AnnualSales__c, 
                   Country__c, StateProvince__c FROM Account WHERE Id = :contact.AccountId];        
                   
        account = AP_AccountConvMapping.prmToBusinessAccount(account);
    }

    public PageReference back() {
                
                PageReference cancelPage = new PageReference('/apex/VFP_ContactSearch');
                
                String retURL = system.currentpagereference().getParameters().get('retURL');
                
                if(retURL != null) {
                    cancelPage = new PageReference(retURL);
                }
                
                return cancelPage;
    }

    public PageReference convertAccount() {
        
        PageReference returnedPage;
        
        String saveURL = system.currentpagereference().getParameters().get('saveURL');
                
        account.RecordTypeId = '012A0000000neLb';
        Database.SaveResult accountSaveResult = Database.update(account, false);
        
        if(accountSaveResult.isSuccess()) {
            returnedPage = new PageReference(saveURL);
        }
        else {
            for(Database.Error err : accountSaveResult.getErrors()) {
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, err.getMessage()));                
            }                
        }

        return returnedPage;
    }
}