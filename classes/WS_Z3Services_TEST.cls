/*
    Author          : Shruti Karn/Vimal Karunakarn/Ramesh Rajasekaran   
    Date Created    : 16/10/2012
    Description     : Test class for WS_Z3Services and Z3ResultClass
*/
@isTest
private class WS_Z3Services_TEST {
    static testMethod void testmethod1() {
        Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c = 'TST';
        insert country;
        
        List<Account> lstAccount = new List<Account>();
        
        Account testAccount = Utils_TestMethods.createAccount();
        testAccount.Country__c = country.id;
        testAccount.Name = 'Test';
        lstAccount.add(testAccount);
        
        Account testAccount1 = Utils_TestMethods.createAccount();
        testAccount1.PostalCountry__c = country.id;
        testAccount1.Name = 'Test1';
        lstAccount.add(testAccount1);
        
        Account testAccount2 = Utils_TestMethods.createAccount();
        testAccount2.Country__c = country.id;
        testAccount2.Name = 'Test';
        lstAccount.add(testAccount2);
        
        insert lstAccount;
        testAccount2.ParentId = testAccount1.Id;
        
        update testAccount2;
        
        List<LegacyAccount__c> lstLegacyAccount = new List<LegacyAccount__c>();
        LegacyAccount__c testLegacyAccount = Utils_TestMethods.createLegacyAccount(testAccount.Id);
        testLegacyAccount.LegacyName__c ='US_VANTIVE';
        testLegacyAccount.LegacyAccountType__c= 'DC';
        testLegacyAccount.LegacyKey__c = 'CU1234';
        testLegacyAccount.LegacyNumber__c = 'CU1234';
        lstLegacyAccount.add(testLegacyAccount);
        
        LegacyAccount__c testLegacyAccount1 = Utils_TestMethods.createLegacyAccount(testAccount1.Id);
        testLegacyAccount1.LegacyName__c ='US_VANTIVE';
        testLegacyAccount1.LegacyAccountType__c= 'DC';
        testLegacyAccount1.LegacyKey__c = 'CU4567';
        testLegacyAccount1.LegacyNumber__c = 'CU4567';
        lstLegacyAccount.add(testLegacyAccount1);
        
        LegacyAccount__c testLegacyAccount2 = Utils_TestMethods.createLegacyAccount(testAccount1.Id);
        testLegacyAccount2.LegacyName__c ='US_VANTIVE';
        testLegacyAccount2.LegacyAccountType__c= 'DC';
        testLegacyAccount2.LegacyKey__c = 'SI4567';
        testLegacyAccount2.LegacyNumber__c = 'SI4567';
        lstLegacyAccount.add(testLegacyAccount2);
        
        insert lstLegacyAccount;
        
        List<Contact> lstContact = new  List<Contact>();
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
        lstContact.add(testContact);
                    
        Contact testContact2 = Utils_TestMethods.createContact(testAccount.Id , 'TestContact2');
        testContact2.SEContactID__c = '1234567';
        lstContact.add(testContact2);

        Contact testContact3 = Utils_TestMethods.createContact(testAccount.Id , 'TestContact3');
        testContact3.SEContactID__c = '1237667';
        lstContact.add(testContact3);

        Contact testContact4 = Utils_TestMethods.createContact(testAccount.Id , 'TestContact4');
        testContact4.SEContactID__c = '1211167';
        lstContact.add(testContact4);


        Contact testContact5 = Utils_TestMethods.createContact(testAccount2.Id , 'TestContact5');
        testContact5.SEContactID__c = '1255567';
        lstContact.add(testContact5);

        insert lstContact;
        List<Contract> lstContract = new List<Contract>();
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id , testContact.Id);
        testContract.startdate = system.today();
        testContract.ContractTerm = 1;
        lstContract.add(testContract);

        Contract testContract1 = Utils_TestMethods.createContract(testAccount2.Id , testContact5.Id);
        testContract1.startdate = system.today();
        testContract1.ContractTerm = 1;
        lstContract.add(testContract1);



        insert lstContract ;

        testContract.webaccess__c = 'Eclipse';
        testContract.Status = 'Activated';  
            

        testContract1.webaccess__c = 'Eclipse';
        testContract1.Status = 'Activated';
            

        update lstContract;    

        List<CTR_ValueChainPlayers__c> lstCVCP = new List<CTR_ValueChainPlayers__c>();
        CTR_ValueChainPlayers__c testcvcp = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        testcvcp.contact__c = testcontact2.id;
        testcvcp.contactrole__C = 'Admin';
        testcvcp.account_role__c = 'Agent';
        lstCVCP.add(testcvcp);

        CTR_ValueChainPlayers__c testcvcp1 = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        testcvcp1.contact__c = testcontact3.id;
        testcvcp1.contactrole__C = 'Admin';
        testcvcp1.account_role__c = 'Distributors / Resellers';
        lstCVCP.add(testcvcp1);

        CTR_ValueChainPlayers__c testcvcp2 = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        testcvcp2.contact__c = testcontact4.id;
        testcvcp2.contactrole__C = 'Primary Partner Contact';
        testcvcp2.account_role__c = 'Distributors / Resellers';
        lstCVCP.add(testcvcp2);

        CTR_ValueChainPlayers__c testcvcp3 = Utils_TestMethods.createCntrctValuChnPlyr(testContract1.Id);
        testcvcp3.contact__c = testcontact2.id;
        testcvcp3.contactrole__C = 'Admin';
        testcvcp3.account_role__c = 'Agent';
        lstCVCP.add(testcvcp3);

        CTR_ValueChainPlayers__c testcvcp4 = Utils_TestMethods.createCntrctValuChnPlyr(testContract1.Id);
        testcvcp4.contact__c = testcontact3.id;
        testcvcp4.contactrole__C = 'Admin';
        testcvcp4.account_role__c = 'Distributors / Resellers';
        lstCVCP.add(testcvcp4);

        CTR_ValueChainPlayers__c testcvcp5 = Utils_TestMethods.createCntrctValuChnPlyr(testContract1.Id);
        testcvcp5.contact__c = testcontact4.id;
        testcvcp5.contactrole__C = 'Primary Partner Contact';
        testcvcp5.account_role__c = 'Distributors / Resellers';
        lstCVCP.add(testcvcp5);

        insert lstCVCP;

        PackageTemplate__c template = Utils_TestMethods.createPkgTemplate();
        template.portalaccess__c = true;
        insert template;
        List<Package__c> lstPackage = new List<Package__c>();
        Package__c pkg = Utils_TestMethods.createPackage(testContract.Id,template.Id);
        pkg.status__c = 'Active';
        pkg.Active__c = True;   
        lstPackage.add(pkg);
        
        Package__c pkg1 = Utils_TestMethods.createPackage(testContract1.Id,template.Id);
        pkg1.status__c = 'Active';
        pkg1.Active__c = True;
        lstPackage.add(pkg1);
        
        
        insert lstPackage;

        //getContactDetails()
        WS_Z3Services.ContactObj contact = new WS_Z3Services.ContactObj();
        contact.ContactID = null;
        contact.SEContactID = null;
        Z3ResultClass.ContactResult result1 = WS_Z3Services.getContactDetails(contact);
        contact.ContactID = '1234567890987';
        result1 = WS_Z3Services.getContactDetails(contact);
        contact.ContactID = testcontact2.id;
        contact.SEContactID = testcontact2.SEContactID__c;
        result1 = WS_Z3Services.getContactDetails(contact);
        contact.ContactID = testcontact2.id;
        contact.SEContactID = null;
        result1 = WS_Z3Services.getContactDetails(contact);
        contact.ContactID = null;
        contact.SEContactID = testcontact2.SEContactID__c;
        result1 = WS_Z3Services.getContactDetails(contact);

        //getExpiringContract()
        WS_Z3Services.ContractObj contract = new WS_Z3Services.ContractObj();
        contract.AccountCountryUS = null;
        Z3ResultClass.ContractResult result2  = WS_Z3Services.getExpiringContract(contract);
        contract.AccountCountryUS = 'TST';
        result2  = WS_Z3Services.getExpiringContract(contract);
system.debug('result2  :'+result2  );
        //createContractNote()
        WS_Z3Services.ContractNoteObj note= new WS_Z3Services.ContractNoteObj();
        note.ContractID = null;
        note.Title = null;
        note.Body = null;
        note.NoteTime = null;
        Z3ResultClass.ContractNoteResult result3 = WS_Z3Services.createContractNote(note);
        note.ContractID = '123456789098765';
        note.Title = null;
        note.Body = null;
        note.NoteTime = null;
        result3 = WS_Z3Services.createContractNote(note);
        note.ContractID = testcontract.id;
        note.Title = 'Test title';
        note.Body = 'TEst Body';
        note.NoteTime = system.now();
        result3 = WS_Z3Services.createContractNote(note);

        //getValidCustomerContracts()
        
        testcontract.contactname__C = testcontact2.id;
        update testcontract;
        WS_Z3Services.CustomerContractObj contract2 = new WS_Z3Services.CustomerContractObj();
        contract2.ContactID = null;
        contract2.ContractNumber = null;
        Z3ResultClass.CustomerContractResult result4 = WS_Z3Services.getValidCustomerContracts(contract2);
        contract2.ContactID = testcontact2.id;
        result4 = WS_Z3Services.getValidCustomerContracts(contract2);
        contract2.ContractNumber = result2.lstContract[0].contractnumber;
        result4 = WS_Z3Services.getValidCustomerContracts(contract2);
        

        //getContactPackageList
        WS_Z3Services.RAContact contactObj = new WS_Z3Services.RAContact();
        contactObj.ContactId =  testContact.Id;
        List<Z3ResultClass.ContactAgreementListResult> resultr1 =  new  List<Z3ResultClass.ContactAgreementListResult>();
        resultr1 = WS_Z3Services.getContactPackageList(contactObj);

        //getContactList
        WS_Z3Services.ContactInfo objContactInfo = new WS_Z3Services.ContactInfo();
        
        List<Z3ResultClass.ContactWithAgreementResult> getContactListResultSet =  new  List<Z3ResultClass.ContactWithAgreementResult>();
        getContactListResultSet = WS_Z3Services.getContactList(objContactInfo);
        objContactInfo.AccountId =  testAccount.Id;
        getContactListResultSet = WS_Z3Services.getContactList(objContactInfo);
        objContactInfo.ContactID =  testContact.Id;
        getContactListResultSet = WS_Z3Services.getContactList(objContactInfo);
        
        //getChangesSince
        Z3ResultClass.ChangesSinceResult objChangesSinceResult =  new  Z3ResultClass.ChangesSinceResult();
        WS_Z3Services.CalendarObj objCalendar = new WS_Z3Services.CalendarObj();
        objChangesSinceResult = WS_Z3Services.getChangesSince(objCalendar);
        Datetime dt= DateTime.newInstance(Date.today(),Time.newInstance(0,0,0,0));
        objCalendar.LastModifiedDateTime =  dt;
        objChangesSinceResult = WS_Z3Services.getChangesSince(objCalendar);
        
        //getLegacyAccount
        WS_Z3Services.LegacyAccountInfo objLegacyAccountInfo = new WS_Z3Services.LegacyAccountInfo();
        
        List<Z3ResultClass.LegacyAccountInfoResult> legacyAccountInfoResultSet = new List<Z3ResultClass.LegacyAccountInfoResult>();
        legacyAccountInfoResultSet = WS_Z3Services.getLegacyAccount(objLegacyAccountInfo);
        objLegacyAccountInfo.AccountId = testAccount.Id;
        legacyAccountInfoResultSet = WS_Z3Services.getLegacyAccount(objLegacyAccountInfo);
        objLegacyAccountInfo.AccountId = testAccount2.Id;
        legacyAccountInfoResultSet = WS_Z3Services.getLegacyAccount(objLegacyAccountInfo); 
        
        //getContactPackageList
        WS_Z3Services.RAContact objRAContact = new WS_Z3Services.RAContact();
        objRAContact.ContactId = testContact.Id;
        List<Z3ResultClass.ContactAgreementListResult> resultra1 =  new  List<Z3ResultClass.ContactAgreementListResult>();
        resultra1 = WS_Z3Services.getContactPackageList(objRAContact);  

        //getPackageTemplateList   
        WS_Z3Services.PackageTemplateObj objPackageTemplate = new WS_Z3Services.PackageTemplateObj();
        objPackageTemplate.CreatedDate = system.datetime.now();
        objPackageTemplate.LastModifieddate = system.datetime.now();
        
        WS_Z3Services.PackageTemplateObj objPackageTemplate1 = new WS_Z3Services.PackageTemplateObj();
        objPackageTemplate.CreatedDate = null;
        objPackageTemplate.LastModifieddate = system.datetime.now();
        
        WS_Z3Services.PackageTemplateObj objPackageTemplate2 = new WS_Z3Services.PackageTemplateObj();
        objPackageTemplate.CreatedDate = system.datetime.now();
        objPackageTemplate.LastModifieddate = null;
        
        List<Z3ResultClass.PackageTemplateResult> resultra2 =  new  List<Z3ResultClass.PackageTemplateResult>();
        resultra2 = WS_Z3Services.getPackageTemplateList(objPackageTemplate);
        resultra2 = WS_Z3Services.getPackageTemplateList(objPackageTemplate1);
        resultra2 = WS_Z3Services.getPackageTemplateList(objPackageTemplate2);
        
        //getCompanyListForPackageTemplates   
        WS_Z3Services.RAPackageTemplateId objRAPackageTemplateId = new WS_Z3Services.RAPackageTemplateId();
        objRAPackageTemplateId.packageTemplateId = template.Id;
        List<Z3ResultClass.AccountResult> resultra3 =  new  List<Z3ResultClass.AccountResult>();
        resultra3 = WS_Z3Services.getCompanyListForPackageTemplates(objRAPackageTemplateId);    
        
        //getAccountForParentAccount      
        WS_Z3Services.RAAccount objRAAccount = new WS_Z3Services.RAAccount();
        objRAAccount.bFOAccountId = testAccount.Id;
        List<Z3ResultClass.ChildAccountResult> resultra4 =  new  List<Z3ResultClass.ChildAccountResult>();
        resultra4 = WS_Z3Services.getAccountForParentAccount(objRAAccount);
        
    }
}