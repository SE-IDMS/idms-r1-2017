/**
23-June-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for VFC_ShowCaseRelatedDetails
 */
@isTest
private class VFC_ShowCaseRelatedDetails_TEST {
      
      static testMethod void myUnitTest() {
        //Test converage for the myPage visualforce page

        PageReference pageRef = Page.VFP_ShowCaseRelatedDetails;
        
        Test.setCurrentPageReference(pageRef);
        
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id,'test contact');
        insert testcontact;
        
        Case testCase = Utils_TestMethods.createCase(testAccount.Id,testcontact.Id,'Open');
        
        ApexPages.StandardController sdtCon = new ApexPages.StandardController(testCase );
        VFC_ShowCaseRelatedDetails myPageCon = new VFC_ShowCaseRelatedDetails(sdtCon);
        
        insert testCase ;
        
        
        list<CCCAction__c> lstCCCAction = new list<CCCAction__c>();
        for(integer i=0; i<200;i++)
        {
            CCCAction__c testAction = Utils_TestMethods.createAction(testCase.Id,UserInfo.getUserId());
            lstCCCAction.add(testAction );
        }
        insert lstCCCAction;
        
        list<Task> lstTask = new list<Task>();
        for(integer i=0; i<200;i++)
        {
            Task testTask = Utils_TestMethods.createTask(testCase.Id,testcontact.id,'Open');
            lstTask.add(testTask);
        }
        insert lstTask;
        ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(testCase );
        VFC_ShowCaseRelatedDetails myPageCon1 = new VFC_ShowCaseRelatedDetails(sdtCon1);
       
      }
}