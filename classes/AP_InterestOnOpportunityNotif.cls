/**
 * @author: Nabil ZEGHACHE
 * Date Created (YYYY-MM-DD): 2016-06-16
 * Test class: 
 * Description: This class manages the Business logic on Interest on Opportunity Notification object
 * **********
 * Scenarios:
 * **********
 * 1.  Set the amount and currency of the interest based on the relevant Interest Amounts records
 * 
 * -----------------------------------------------------------------
 *                     MODIFICATION HISTORY
 * -----------------------------------------------------------------
 * Modified By: Authors Name
 * Modified Date: Date
 * Description: Brief Description of Change + BR/Hotfix
 * -----------------------------------------------------------------
 */

public class AP_InterestOnOpportunityNotif {
    
    /**
     * @author: Nabil ZEGHACHHE
     * Date Created: 2016-06-16
     * Description: this method receives a list of Interest records as a parameter and computes the interest amounts related to those interests.
     * It then updates the interest records with the corresponding amounts and currencies if there are matching interests amount records (country + BU + Type + Interest)
     * @param aListOfInterests A list of Interest records to be processed.
     * -----------------------------------------------------------------
     *                    MODIFICATION HISTORY
     * -----------------------------------------------------------------
     * Modified By: Authors Name
     * Modified Date: Date
     * 
     * Description: Brief Description of Change + BR/Hotfix
     * -----------------------------------------------------------------
     */
    public static void updateInterestAmount(List<InterestOnOpportunityNotif__c> aListOfInterests) {
        System.debug('#### Starting to update interest amounts for list: '+aListOfInterests);
        updateInterestAmount(aListOfInterests, true);
    }
    
    
    /**
     * @author: Nabil ZEGHACHHE
     * Date Created: 2016-06-16
     * Description: this method receives a list of Interest records as a parameter and computes the interest amounts related to those interests.
     * It then updates the interest records with the corresponding amounts and currencies if there are matching interests amount records (country + BU + Type + Interest)
     * @param aListOfInterests A list of Interest records to be processed.
     * @param aIsResetAmounts a Boolean indicating the if the amount values of the interest must be reset if no match is found in the Interest Amount reference table. If the value is NULL it is defaulted to true.
     * -----------------------------------------------------------------
     *                    MODIFICATION HISTORY
     * -----------------------------------------------------------------
     * Modified By: Authors Name
     * Modified Date: Date
     * 
     * Description: Brief Description of Change + BR/Hotfix
     * -----------------------------------------------------------------
     */
    public static void updateInterestAmount(List<InterestOnOpportunityNotif__c> aListOfInterests, Boolean aIsResetAmounts) {
        System.debug('#### Starting to update interest amounts for list: '+aListOfInterests+' - aIsResetAmounts: '+aIsResetAmounts);
        if (aListOfInterests != null) {
            
            Boolean isResetAmount = ((aIsResetAmounts == null) ? true : aIsResetAmounts);
            
            Set<Id> oppNotifIdSet = new Set<Id>();
            Set<String> searchKeySet = new Set<String>();
            Map<InterestOnOpportunityNotif__c, String> interestSearchKeyMap = new Map<InterestOnOpportunityNotif__c, String>(); // map containing the search key for each interest
            
            for (InterestOnOpportunityNotif__c currentInterest : aListOfInterests) {
                oppNotifIdSet.add(currentInterest.OpportunityNotification__c);
            }
            
            System.debug('#### Set of related Opp Notif IDs: '+oppNotifIdSet);
            
            Map<Id, OpportunityNotification__c> oppNotifMap = new Map<Id, OpportunityNotification__c>([SELECT Id, Name, CountryDestination__c from OpportunityNotification__c where Id in :oppNotifIdSet]);
            
            updateInterestAmount(aListOfInterests, oppNotifMap, isResetAmount);
            
        }
    }


        /**
     * @author: Nabil ZEGHACHHE
     * Date Created: 2016-06-16
     * Description: this method receives a list of Interest records as a parameter and computes the interest amounts related to those interests.
     * It then updates the interest records with the corresponding amounts and currencies if there are matching interests amount records (country + BU + Type + Interest)
     * @param aListOfInterests A list of Interest records to be processed.
     * -----------------------------------------------------------------
     *                    MODIFICATION HISTORY
     * -----------------------------------------------------------------
     * Modified By: Authors Name
     * Modified Date: Date
     * 
     * Description: Brief Description of Change + BR/Hotfix
     * -----------------------------------------------------------------
     */
    public static void updateInterestAmount(List<InterestOnOpportunityNotif__c> aListOfInterests, Map<Id, OpportunityNotification__c> anOppNotifMap) {
        System.debug('#### Starting to update interest amounts for list and Map');
        // Calling the method to update interest amounts and resetting the amounts if not match found
        updateInterestAmount(aListOfInterests, anOppNotifMap, true);
    }

    
    
    /**
     * @author: Nabil ZEGHACHHE
     * Date Created: 2016-06-16
     * Description: this method receives a list of Interest records as a parameter and computes the interest amounts related to those interests.
     * It then updates the interest records with the corresponding amounts and currencies if there are matching interests amount records (country + BU + Type + Interest)
     * @param aListOfInterests A list of Interest records to be processed.
     * @param aIsResetAmounts a Boolean indicating the if the amount values of the interest must be reset if no match is found in the Interest Amount reference table. If the value is NULL it is defaulted to true.
     * -----------------------------------------------------------------
     *                    MODIFICATION HISTORY
     * -----------------------------------------------------------------
     * Modified By: Authors Name
     * Modified Date: Date
     * 
     * Description: Brief Description of Change + BR/Hotfix
     * -----------------------------------------------------------------
     */
    public static void updateInterestAmount(List<InterestOnOpportunityNotif__c> aListOfInterests, Map<Id, OpportunityNotification__c> anOppNotifMap, Boolean aIsResetAmounts) {
        System.debug('#### Starting to update interest amounts with List, Map and Boolean');
        System.debug('#### aListOfInterests: '+aListOfInterests);
        System.debug('#### anOppNotifMap: '+anOppNotifMap);
        System.debug('#### aIsResetAmounts: '+aIsResetAmounts);
        if (aListOfInterests != null && anOppNotifMap != null) {
            
            Boolean isResetAmount = ((aIsResetAmounts == null) ? true : aIsResetAmounts);
            System.debug('#### Will interests amounts be reset? isResetAmount: '+isResetAmount);
            
            if (isResetAmount) {
                System.debug('#### Resetting amounts and currencies');
                for (InterestOnOpportunityNotif__c currentInterest : aListOfInterests) {
                    currentInterest.Amount__c = 0.0;
                    currentInterest.CurrencyIsoCode = UserInfo.getDefaultCurrency();
                }
            }
            
            //Set<Id> oppNotifIdSet = new Set<Id>();
            Set<String> searchKeySet = new Set<String>();
            Map<InterestOnOpportunityNotif__c, String> interestSearchKeyMap = new Map<InterestOnOpportunityNotif__c, String>(); // map containing the search key for each interest
                        
            String tmpSearchKey = null;
            for (InterestOnOpportunityNotif__c currentInterest : aListOfInterests) {
                if (anOppNotifMap.get(currentInterest.OpportunityNotification__c) != null && anOppNotifMap.get(currentInterest.OpportunityNotification__c).CountryDestination__c != null) {
                    tmpSearchKey = anOppNotifMap.get(currentInterest.OpportunityNotification__c).CountryDestination__c+'#'+currentInterest.Leading_Business_BU__c+'#'+currentInterest.Type__c+'#'+currentInterest.Interest__c;
                    searchKeySet.add(tmpSearchKey);
                    interestSearchKeyMap.put(currentInterest, tmpSearchKey);
                }
            }

            System.debug('#### Search ket set: '+searchKeySet);
            System.debug('#### Interest search ket map: '+interestSearchKeyMap);
            
            if (searchKeySet != null && searchKeySet.size() > 0) {
                List<InterestAmountOnOpportunityNotif__c> interestAmountList = new List<InterestAmountOnOpportunityNotif__c>([SELECT Id, name, searchKey01__c, Country__c, Leading_Business_BU__c, Type__c, Interest__c, Amount__c, CurrencyIsoCode FROM InterestAmountOnOpportunityNotif__c WHERE searchKey01__c in :searchKeySet]);
                if (interestAmountList != null && interestAmountList.size() >0) {
                    System.debug('#### Interest amounts: '+interestAmountList);
                    Map<String, InterestAmountOnOpportunityNotif__c> interestAmountMap = new Map<String, InterestAmountOnOpportunityNotif__c>();
                    for (InterestAmountOnOpportunityNotif__c currentInterestAmount : interestAmountList) {
                        if (currentInterestAmount.searchKey01__c != null) {
                            interestAmountMap.put(currentInterestAmount.searchKey01__c, currentInterestAmount);
                        }
                    }
                    
                    String currentSearchKey = null;
                    for (InterestOnOpportunityNotif__c currentInterest : aListOfInterests) {
                        currentSearchKey = interestSearchKeyMap.get(currentInterest);
                        if (interestAmountMap != null && currentSearchKey != null && interestAmountMap.get(currentSearchKey) != null) {
                            System.debug('#### Found a matching Interest amount: '+interestAmountMap.get(currentSearchKey));
                            currentInterest.Amount__c = interestAmountMap.get(currentSearchKey).Amount__c;
                            currentInterest.CurrencyIsoCode = interestAmountMap.get(currentSearchKey).CurrencyIsoCode;
                        }
                    }
                    
                }
            }
        }
    }
    
    

}