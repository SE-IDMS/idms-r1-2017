/* 
Class       : VFC_SRV_MYFSWODetailController
Author      : Raghavender Katta
Description : An apex page controller that takes the user to WO Detail page.
*/
Public without sharing class VFC_SRV_MYFSWODetailController{
    
    Public SVMXC__Service_Order__c woDetails{get;set;}
    Public Id woId{get;set;}
    Public String woStatus{get;set;}
    Public String ultimateParentAccount {get;set;}
    Public String selectedCountry {get;set;}
    Public String site{get;set;}
    Public List<SVMXC__Installed_Product__c> ipList{get;set;}
    Public String userId{get;set;}    
    Public VFC_SRV_MYFSWODetailController(){
        woId = Apexpages.currentpage().getparameters().get('Id');
        site= Apexpages.currentpage().getparameters().get('site');
        ultimateParentAccount = Apexpages.currentpage().getparameters().get('Params');
        selectedCountry = Apexpages.currentpage().getparameters().get('country');
        System.debug('woId'+woId);
        userId = Apexpages.currentpage().getparameters().get('UId');
        if(userId==null || userId ==''){
         userId = UserInfo.getUserId();     
        }
        getWODetails();
    }
    
    Public void getWODetails(){
        
        woDetails = new SVMXC__Service_Order__c();
        list<SVMXC__Service_Order__c> WOlistToShare = new list<SVMXC__Service_Order__c>();
        ipList = new List<SVMXC__Installed_Product__c>();
        //(SVMXC__Order_Status__c = 'Customer Confirmed' or SVMXC__Order_Status__c = 'Acknowledge FSE')
        if(woID != null){
            woDetails = [select SerialNumber__c,WOPriority__c,SVMXC__Company__c,SVMXC__Company__r.name,SoldToAccount__c,SoldToAccount__r.name,SVMXC__Billing_Type__c,
            SVMXC__Component__r.name,SVMXC__Product__r.name,PlannerContact__r.name,QuotationNumber__c,SVMXC__Case__c,ServiceLine__r.name,Sales_Order_Number__c,
            Customer_Reference_Number__c,SVMXC__Closed_By__r.name,SVMXC__Priority__c,SVMXC__Closed_On__c,
            WorkOrderSubType__c,id,TECH_CustomerScheduledDateTimeECH__c,name,createddate,SVMXC__Scheduled_Date_Time__c,SVMXC__Order_Status__c,SVMXC__Site__r.name,
            SVMXC__Scheduled_Date__c,SubStatus__c,tolabel(SVMXC__Order_Type__c),SVMXC__Group_Member__r.Name,SVMXC__Case__r.CaseNumber,CustomerTimeZone__c,
            (SELECT Id, Name,CreatedDate,ParentId, LastModifiedDate FROM Attachments where 
            Name like :'%' + System.label.CLSRVMYFS_PDFEXTENSION +'%' and ((Name like :system.label.CLSRVMYFS_SRVINTERVENTION or Description =:system.label.CLSRVMYFS_WODescription) or (Name like :system.label.CLSRVMYFS_WO_TECH_REPORT) ) limit 2) 
            from SVMXC__Service_Order__c  where Id = :woId];
         List<SVMXC__Installed_Product__c> workOrderIPList = new List<SVMXC__Installed_Product__c>();
         List<SVMXC__Installed_Product__c> workOrderDetailIPList = new List<SVMXC__Installed_Product__c>();
         workOrderIPList = [select id,name,SVMXC__Serial_Lot_Number__c,SVMXC__Product__c,SVMXC__Product__r.name,Brand2__c,Brand2__r.name,Category__r.name,Category__c,DeviceType2__r.name,DeviceType2__c from SVMXC__Installed_Product__c where (Id in (Select SVMXC__Component__c from SVMXC__Service_Order__c where Id =: woId)) ];   
 
         workOrderDetailIPList = [select id,name,SVMXC__Serial_Lot_Number__c,SVMXC__Product__c,SVMXC__Product__r.name,Brand2__c,Brand2__r.name,Category__r.name,Category__c,DeviceType2__r.name,DeviceType2__c from SVMXC__Installed_Product__c where (Id in (Select SVMXC__Serial_Number__c from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c =: woId and RecordTypeId = '012A0000000nYLx')) ];
         
         List<SVMXC__Installed_Product__c> mergedIPList = new List<SVMXC__Installed_Product__c>();
         if(workOrderIPList != null && ! workOrderIPList.isEmpty())
            mergedIPList.addAll(workOrderIPList);
         if(workOrderDetailIPList != null && ! workOrderDetailIPList.isEmpty())
            mergedIPList.addAll(workOrderDetailIPList);
         Set<sobject> installedProductsSet = new Set<sobject>();
         if(mergedIPList != null && ! mergedIPList.isEmpty())
            for (SVMXC__Installed_Product__c c : mergedIPList) {
                
                if (installedProductsSet.add(c)) {
                    
                    ipList.add(c);
                    
                }
                
            }
        }
        System.debug('woDetails'+woDetails);
        if(woDetails != null && woDetails.TECH_CustomerScheduledDateTimeECH__c == null && woDetails.CustomerTimeZone__c != null && woDetails.SVMXC__Scheduled_Date_Time__c != null){
         woDetails.TECH_CustomerScheduledDateTimeECH__c= woDetails.SVMXC__Scheduled_Date_Time__c.format('dd MMM yyyy HH:mm:ss', woDetails.CustomerTimeZone__c);
        }
        if(woDetails!= null && woDetails.Attachments != null && woDetails.Attachments.size() > 0 ) {
System.debug('woListAttachments'+woDetails.Attachments[0].id);
System.debug('woListAttachments name'+woDetails.SVMXC__Closed_By__r.name);
System.debug('woListAttachments'+woDetails.Attachments[0].id);
System.debug('woListAttachments name'+woDetails.Attachments[0].name);
}
         System.debug('ipList'+ipList); 
        if(woDetails != null){
        WOlistToShare.add(woDetails);
      /*   if(!WOlistToShare.isEmpty() && WOlistToShare!=null){
         if(!Test.isRunningTest()){
          AP_SRV_myFSUtils.WorkorderSharing(WOlistToShare,UserInfo.getUserId());
         } 
       }*/
            if(woDetails.SVMXC__Order_Status__c != null && (woDetails.SVMXC__Order_Status__c == system.label.CLSRVMYFS_CUSTOMER_CONFIRMED || woDetails.SVMXC__Order_Status__c == system.label.CLSRVMYFS_ACKNOWLEDGE_FSE)){
                woStatus =system.label.CLSRV_MYFS_CONFIRMED; 
            }else if(woDetails.SVMXC__Order_Status__c != null &&(woDetails.SVMXC__Order_Status__c == system.label.CLSRVMYFS_ServiceDelivered || woDetails.SVMXC__Order_Status__c == system.label.CLSRVMYFS_SERVICECOMPLETE || woDetails.SVMXC__Order_Status__c == system.label.CLSRVMYFS_SERVICEVALIDATED || woDetails.SVMXC__Order_Status__c == system.label.CLSRVMYFS_INTERVENTION_CLOSED)){
                woStatus = system.label.CLSRVMYFS_COMPLETE;
            }
           
        }
    }
    
    Public void shareWorkOrders(list<SVMXC__Service_Order__c> WOlistToShare){
         if(!WOlistToShare.isEmpty() && WOlistToShare!=null){
         if(!Test.isRunningTest()){
          AP_SRV_myFSUtils.WorkorderSharing(WOlistToShare,userId);
         }
    }
    
    }
    
}