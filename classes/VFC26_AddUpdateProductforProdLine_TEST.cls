@isTest
private class VFC26_AddUpdateProductforProdLine_TEST {
    static User user;
    static Account account1;
    static Contact contact1;
    static Opportunity opportunity1;    
    static OPP_ProductLine__c ProductLine1,ProductLine2,ProductLine3;
    static string [] searchfilterList= new string[]{};
    static List<String> keywords = new List<String>();
    static Utils_DummyDataForTest dummydata;
    static Utils_DataSource.Result  tempresult;
    static sObject tempsObject;
    static VCC05_DisplaySearchFields componentcontroller  = new VCC05_DisplaySearchFields(); 
    static PageReference pageRef= Page.VFP26_AddUpdateProductforProductLine; 
    static VFC26_AddUpdateProductforProductLine  ProductforProductLine; 
    static VFC26_AddUpdateProductforProductLine  ProductforCase2;
    static VFC26_AddUpdateProductforProductLine  ProductforProductLine3;
    static 
    {
        // Profile profile = [select id from profile where name='SE - Sales Manager'];
        // user = new User(alias = 'user', email='user' + '@accenture.com', 
        // emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        // localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
        // timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        // User user;
        System.runAs(sysadmin2){
            UserandPermissionSets useruserandpermissionsetrecord=new UserandPermissionSets('user','SE - Sales Manager');
           user = useruserandpermissionsetrecord.userrecord;
            Database.insert(user);

            List<PermissionSetAssignment> permissionsetassignmentlstforuser=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:useruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforuser.add(new PermissionSetAssignment(AssigneeId=user.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforuser.size()>0)
            Database.insert(permissionsetassignmentlstforuser);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }

        system.runAs(user)
        { 
        
            Country__c testcountry=new Country__c(Name='SampleCountry1',CountryCode__c='SC1');
            insert testcountry;
            System.debug('---->>>>>>>>>'+testcountry.Id); 
            
            account1 = Utils_TestMethods.createAccount();
            account1.Country__c = testcountry.Id;
            insert account1;
            
            contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');               
            //contact1.UpdateAccountAddress__c= false;      
            //contact1.Country__c= testcountry.Id;
            insert contact1;

            opportunity1 = Utils_TestMethods.createOpportunity(account1.Id);
            insert opportunity1;

            ProductLine1 = Utils_TestMethods.createProductLine(opportunity1.Id);
            ProductLine1.TECH_CommercialReference__c= 'Commercial\\sReference\\stest';
            ProductLine1.ProductBU__c = 'Product BU';
            insert ProductLine1;
        
            ProductLine2 = Utils_TestMethods.createProductLine(opportunity1.Id);
            ProductLine1.TECH_CommercialReference__c= 'Commercial\\sReference\\stest';
            ProductLine1.ProductBU__c = 'Product BU';
           // insert ProductLine2;
            
            ProductLine3 = Utils_TestMethods.createProductLine(opportunity1.Id);
            ProductLine1.TECH_CommercialReference__c= 'Commercial\\sReference\\stest';
            ProductLine1.ProductBU__c = 'Product BU';
            
            for(Integer token=0;token<17;token++)
            {
                searchfilterList.add('a');          
            }  
        }
        
        dummydata = new Utils_DummyDataForTest();
        tempresult = new Utils_DataSource.Result();
        tempresult = dummydata.Search(keywords,keywords);
        
        tempsObject = tempresult.recordList[0];
        //End of Test Data preparation.
    }
    
    static testMethod void VFC26_AddUpdateProductforProductLineTest() 
    {
        //Test.startTest();
        ApexPages.StandardController cc1 = new ApexPages.StandardController(ProductLine1);
        ApexPages.StandardController cc2 = new ApexPages.StandardController(ProductLine2);
        ApexPages.StandardController cc3 = new ApexPages.StandardController(ProductLine3);
        
        pageRef= Page.VFP26_AddUpdateProductforProductLine; 
        VFC_ControllerBase basecontroller = new VFC_ControllerBase();

        System.debug('~~~~~~~~~~Start of Test~~~~~~~~~~');
        ProductforProductLine = new VFC26_AddUpdateProductforProductLine (cc1);
        ProductforProductLine3 = new VFC26_AddUpdateProductforProductLine (cc3);        
        ProductforCase2 = new VFC26_AddUpdateProductforProductLine (cc2);
        VCC06_DisplaySearchResults componentcontroller1 =ProductforProductLine.resultsController; 
      
        componentcontroller = new VCC05_DisplaySearchFields(); 
        
        componentcontroller.pickListType = 'PM0_GMR';
        componentcontroller.SearchFilter = new String[4];
        componentcontroller.SearchFilter[0] = '00';
        componentcontroller.SearchFilter[1] = Label.CL00355;    
        componentcontroller.searchText = 'search\\stest';
        componentcontroller.key = 'searchComponent';
        componentcontroller.PageController = ProductforProductLine;
        componentcontroller.init();
        
        componentcontroller.searchText = 'search';
        componentcontroller.level1='level1';
        componentcontroller.level2='level2';
        componentcontroller.level3='level3';
        componentcontroller.level4='level4';
        //componentcontroller.setlevel2('level4');
        pageRef.getParameters().put('key','searchComponent');
        pageRef.getParameters().put('CF00NA000000559Xm_lkid',opportunity1.Id);
        pageRef.getParameters().put('retURL',ProductLine1.id);
        test.setCurrentPageReference(pageRef); 
        system.debug('******Controller***'+ProductforProductLine);
        ProductforProductLine.PerformAction(tempsObject, ProductforProductLine);
        ProductforCase2.PerformAction(tempsObject, ProductforCase2);
        componentcontroller.searchText = 'search\\stest';
        ProductforProductLine.search();
        ProductforProductLine.clear();
        ProductforProductLine.cancel();
        ProductforCase2.dataSource = (Utils_DataSource)dummydata;
        ProductforCase2.cancel();
        
        PageReference pageRef2 = Page.VFP26_AddUpdateProductforProductLine;
        pageRef2.getParameters().put('key','searchComponent');
        pageRef.getParameters().put(System.Label.CL00334,null);
        test.setCurrentPageReference(pageRef2);
        ProductforProductLine3.cancel();
        pageRef2.getParameters().put('retURL',ProductLine1.id);
        ProductforProductLine3.PerformAction(tempsObject, ProductforProductLine);
        pageRef2.getParameters().put('retURL',null);
        ProductforProductLine3.cancel();
        //Pr.oductforProductLine3.PerformAction(tempsObject, ProductforProductLine3);
    }    
}