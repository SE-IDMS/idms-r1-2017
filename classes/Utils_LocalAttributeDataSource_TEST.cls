/*
    Author          : Accenture IDC Team 
    Date Created    : 07/12/2011
    Description     : Test Class for Local Attribute Data Source Utility class
    Last Modified By: Pooja Gupta (Date: 08/08/2012)
*/
@Istest

class Utils_LocalAttributeDataSource_TEST
{
   static testMethod void LocalAttributeDataSource_TestMethod()
    {
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        List<String> dummyList1 = new List<String>();
        List<String> dummyList2 = new List<String>();
        Utils_DataSource LocalAttributeDataSource = Utils_Factory.getDataSource('LA');
        List<SelectOption> Columns = LocalAttributeDataSource.getColumns();
        
        List<LocalAttribute__c> LAList = new List<LocalAttribute__c>();
        LocalAttribute__c locatt = Utils_TestMethods.createLocalAttribute();
        locatt.account__c=TRUE;
        insert locatt;
        LAlist.add(locatt);
        
        Integer counter = 0;
        for(Integer index=1;index<9;index++)
        {
            dummyList1.clear();
            dummyList2.clear();                
            counter = index;
            if(counter == 1)
            {
                dummyList2.add(String.ValueOf(country.Id));
                dummyList2.add(Label.CL00355);
                dummyList2.add(Schema.sObjectType.ACC_LocalAttribute__c.fields.Account__c.Name+'=TRUE'); 
                Utils_DataSource.Result Results = LocalAttributeDataSource.Search(dummyList1, dummyList2);  
            }
            if(counter == 2)
            {
                dummyList1.add('TEST');
                dummyList2.add(String.ValueOf(country.Id));
                dummyList2.add(Label.CL00355);
                dummyList2.add(Schema.sObjectType.ACC_LocalAttribute__c.fields.Account__c.Name+'=TRUE'); 
                Utils_DataSource.Result Results = LocalAttributeDataSource.Search(dummyList1, dummyList2);
            } 
            if(counter == 3)
            {
                dummyList1.add('');
                dummyList2.add(String.ValueOf(country.Id));
                dummyList2.add('BD');
                dummyList2.add(Schema.sObjectType.ACC_LocalAttribute__c.fields.Account__c.Name+'=TRUE'); 
                Utils_DataSource.Result Results = LocalAttributeDataSource.Search(dummyList1, dummyList2);
            }
            if(counter == 4)
            {
                dummyList1.add('TEST');
                dummyList2.add(String.ValueOf(country.Id));
                dummyList2.add('BD');
                dummyList2.add(Schema.sObjectType.ACC_LocalAttribute__c.fields.Account__c.Name+'=TRUE'); 
                Utils_DataSource.Result Results = LocalAttributeDataSource.Search(dummyList1, dummyList2);
            }
            if(counter == 5)
            {
                dummyList1.add('TEST');
                dummyList2.add(Label.CL00355);
                dummyList2.add('BD');
                dummyList2.add(Schema.sObjectType.ACC_LocalAttribute__c.fields.Account__c.Name+'=TRUE'); 
                Utils_DataSource.Result Results = LocalAttributeDataSource.Search(dummyList1, dummyList2);   
            }
            if(counter == 6)
            {
                dummyList1.add('');
                dummyList2.add(Label.CL00355);
                dummyList2.add('BD');
                dummyList2.add(Schema.sObjectType.ACC_LocalAttribute__c.fields.Account__c.Name+'=TRUE'); 
                Utils_DataSource.Result Results = LocalAttributeDataSource.Search(dummyList1, dummyList2);
            }
            if(counter == 7)
            {
                dummyList1.add('');           
                dummyList2.add(String.ValueOf(country.Id));
                dummyList2.add(Label.CL00355);
                dummyList2.add(Schema.sObjectType.ACC_LocalAttribute__c.fields.Account__c.Name+'=TRUE'); 
                Utils_DataSource.Result Results = LocalAttributeDataSource.Search(dummyList1, dummyList2);
            }
             if(counter == 8)
            {
                dummyList1.add('TEST');
                dummyList2.add(Label.CL00355);
                dummyList2.add(Label.CL00355);
                dummyList2.add(Schema.sObjectType.ACC_LocalAttribute__c.fields.Account__c.Name+'=TRUE'); 
                Utils_DataSource.Result Results = LocalAttributeDataSource.Search(dummyList1, dummyList2);
            }
            if(counter == 9)
            {
                dummyList1.add('');
                dummyList2.add(Label.CL00355);
                dummyList2.add(Label.CL00355);
                dummyList2.add(Schema.sObjectType.ACC_LocalAttribute__c.fields.Account__c.Name+'=TRUE'); 
                Utils_DataSource.Result Results = LocalAttributeDataSource.Search(dummyList1, dummyList2);
            }  
            
         }
         Utils_DataSource.Result Results = LocalAttributeDataSource.Search(dummyList1, dummyList2);
}
}