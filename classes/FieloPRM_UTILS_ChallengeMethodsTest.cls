/********************************************************************
* Company: Fielo
* Developer: Pablo Cassinerio
* Created Date: 14/04/2015
* Description: Test class for class FieloPRM_UTILS_ChallengeMethods
********************************************************************/
@isTest
public class FieloPRM_UTILS_ChallengeMethodsTest {

    public static testMethod void testUnit(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            List<FieloEE__Member__c> members = new List<FieloEE__Member__c>();
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo',
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'test'
            );
            members.add(member);
            
            FieloEE__Member__c member2 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo2',
                FieloEE__FirstName__c = 'Marco2' +  String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'test'
            );
            members.add(member2);
            
            insert members;

            List<FieloEE__Badge__c> listbadge = new list<FieloEE__Badge__c>();
            
            FieloEE__Badge__c badge = new FieloEE__Badge__c(
            name = 'Badge',
            F_PRM_Type__c = 'Program Level',
            F_PRM_BadgeAPIName__c = 'apinametest'
            );
            listbadge.add(badge) ;

            FieloEE__Badge__c badge2 = new FieloEE__Badge__c(
                name = 'Badge',
                F_PRM_Type__c = 'BFOProperties',
                F_PRM_BadgeAPIName__c = 'apinametest2'
            );
            listbadge.add(badge2) ;

            insert listbadge;
                        
            Date fecha = system.today();
            
            List<FieloCH__Challenge__c> challenges = new List<FieloCH__Challenge__c>();
            
            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(
                Name = 'Challenge TEST', 
                FieloCH__Mode__c = 'Competition', 
                FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Global', 
                FieloCH__InitialDate__c = fecha.addDays(-5), 
                FieloCH__FinalDate__c = fecha.addDays(5), 
                FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, 
                F_PRM_ChallengeFilter__c = 'Ch' + datetime.now()
            );
            
            FieloCH__Challenge__c challenge2 = new FieloCH__Challenge__c(
                Name = 'Challenge TEST', 
                FieloCH__Mode__c = 'Teams', 
                FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Global', 
                FieloCH__InitialDate__c = fecha.addDays(-5), 
                FieloCH__FinalDate__c = fecha.addDays(5), 
                FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, 
                F_PRM_ChallengeFilter__c = 'Ch' + datetime.now()
            );
            
            FieloCH__Challenge__c challenge3 = new FieloCH__Challenge__c(
                Name = 'Challenge TEST', 
                FieloCH__Mode__c = 'Collaborative', 
                FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Global', 
                FieloCH__InitialDate__c = fecha.addDays(-5), 
                FieloCH__FinalDate__c = fecha.addDays(5), 
                FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, 
                F_PRM_ChallengeFilter__c = 'Ch' + datetime.now()
            );
            
            challenges.add(challenge);
            challenges.add(challenge2);
            challenges.add(challenge3);
            insert challenges;

            FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Average', 'name', 'equals', decimal.valueof(1));
            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithoutObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter');
            FieloCH__MissionCriteria__c missionCriteria1 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_BadgeName__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission1.id );
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_BadgeName__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );

            FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(challenge.id , mission1.id );         

            FieloCH__ChallengeReward__c challengeReward  = FieloPRM_UTILS_MockUpFactory.createChallengeReward(challenge);       
            challengeReward.FieloCH__LogicalExpression__c = '(1)';
            challengeReward.FieloCH__Badge__c = badge.id;
            
            update challengeReward;
            
            test.StartTest();
            
            challenge = FieloPRM_UTILS_MockUpFactory.activateChallenge(challenge);
          
            List<FieloCH__ChallengeMission__c> chalMissions = Database.query('SELECT Id, FieloCH__Challenge2__c, FieloCH__Mission2__c, FieloCH__Mission2__r.FieloCH__ObjectiveType__c, FieloCH__Mission2__r.RecordTypeId FROM FieloCH__ChallengeMission__c WHERE FieloCH__Challenge2__c = \'' + challenge.Id + '\'');

            challenge = [SELECT Id, FieloCH__Mode__c, (SELECT Id, Name, FieloCH__Team__c, FieloCH__Member__c FROM FieloCH__ChallengeMembers2__r), (SELECT Id FROM FieloCH__MissionsChallenge__r) FROM FieloCH__Challenge__c WHERE Id = :challenge.Id];           
                        
            FieloPRM_UTILS_ChallengeMethods.canSubscribe(member,challenge,chalMissions);

            FieloPRM_UTILS_ChallengeMethods.loadMissionsStatus(member,challenge,chalMissions);
            
            test.StopTest();
            
        }
    }

    public static testMethod void testUnit2(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            List<FieloEE__Member__c> members = new List<FieloEE__Member__c>();
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo',
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'test'
            );
            members.add(member);
            
            FieloEE__Member__c member2 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo2',
                FieloEE__FirstName__c = 'Marco2' +  String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'test'
            );
            members.add(member2);
            
            insert members;
            
            FieloEE__Program__c program = FieloPRM_UTILS_MockUpFactory.createProgram();
            
            Date fecha = system.today();

            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(
                Name = 'Challenge TEST', 
                FieloCH__Mode__c = 'Competition', 
                FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Global', 
                FieloCH__InitialDate__c = fecha.addDays(-5), 
                FieloCH__FinalDate__c = fecha.addDays(5), 
                FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, 
                F_PRM_ChallengeFilter__c = 'Ch' + datetime.now(),
                FieloCH__Program__c = program.Id
            );
            insert challenge;

            FieloCH__Mission__c mission = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Average', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_BadgeName__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission.Id );

            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_BadgeName__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
            
            FieloCH__Mission__c mission3 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Max Value', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria3 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_BadgeName__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission3.Id );
            
            FieloCH__Mission__c mission4 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__Transaction__c', 'Summary', 'name', 'equals', decimal.valueof(1));
            FieloCH__MissionCriteria__c missionCriteria4 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_BadgeName__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission4.Id ); 
            
            FieloCH.ChallengeUtil.getMapRewardsByPosition(new fieloch__Challenge__c());

            FieloCH__ChallengeMember__c challengeMember = new FieloCH__ChallengeMember__c(
                FieloCH__Challenge2__c = challenge.Id,
                FieloCH__Member__c = member.Id
            );
            insert challengeMember;
            
            FieloCH__ChallengeMission__c challengeMission = new FieloCH__ChallengeMission__c(
                FieloCH__Challenge2__c = challenge.Id 
            );
            challengeMission.put('FieloCH__Mission2__c', mission.Id);

            FieloCH__ChallengeMission__c challengeMission2 = new FieloCH__ChallengeMission__c(
                FieloCH__Challenge2__c = challenge.Id 
            );
            challengeMission2.put('FieloCH__Mission2__c', mission2.Id);

            FieloCH__ChallengeMission__c challengeMission3 = new FieloCH__ChallengeMission__c(
                FieloCH__Challenge2__c = challenge.Id 
            );
            challengeMission3.put('FieloCH__Mission2__c', mission3.Id);

            FieloCH__ChallengeMission__c challengeMission4 = new FieloCH__ChallengeMission__c(
                FieloCH__Challenge2__c = challenge.Id 
            );
            challengeMission4.put('FieloCH__Mission2__c', mission4.Id);

            insert new List<FieloCH__ChallengeMission__c>{challengeMission,challengeMission2,challengeMission3,challengeMission4};
            
            test.StartTest();
            
            FieloCH__ChallengeMissionMember__c challengeMissionMember = new FieloCH__ChallengeMissionMember__c(
                FieloCH__Counter__c = 1,
                FieloCH__MaxValue__c = 1,
                FieloCH__ChallengeMember__c = challengeMember.Id,
                FieloCH__ChallengeMission__c = challengeMission.Id,
                FieloCH__Summary__c = 1,
                FieloCH__AlternativeKey__c = string.valueOf(challengeMission.Id) + string.valueOf(challengeMember.FieloCH__Member__c)
            );

            FieloCH__ChallengeMissionMember__c challengeMissionMember2 = new FieloCH__ChallengeMissionMember__c(
                FieloCH__Counter__c = 1,
                FieloCH__MaxValue__c = 1,
                FieloCH__ChallengeMember__c = challengeMember.Id,
                FieloCH__ChallengeMission__c = challengeMission2.Id,
                FieloCH__Summary__c = 1,
                FieloCH__AlternativeKey__c = string.valueOf(challengeMission2.Id) + string.valueOf(challengeMember.FieloCH__Member__c)
            );

            FieloCH__ChallengeMissionMember__c challengeMissionMember3 = new FieloCH__ChallengeMissionMember__c(
                FieloCH__Counter__c = 1,
                FieloCH__MaxValue__c = 1,
                FieloCH__ChallengeMember__c = challengeMember.Id,
                FieloCH__ChallengeMission__c = challengeMission3.Id,
                FieloCH__Summary__c = 1,
                FieloCH__AlternativeKey__c = string.valueOf(challengeMission3.Id) + string.valueOf(challengeMember.FieloCH__Member__c)
            );

            FieloCH__ChallengeMissionMember__c challengeMissionMember4 = new FieloCH__ChallengeMissionMember__c(
                FieloCH__Counter__c = 1,
                FieloCH__MaxValue__c = 1,
                FieloCH__ChallengeMember__c = challengeMember.Id,
                FieloCH__ChallengeMission__c = challengeMission4.Id,
                FieloCH__Summary__c = 1,
                FieloCH__AlternativeKey__c = string.valueOf(challengeMission4.Id) + string.valueOf(challengeMember.FieloCH__Member__c)
            );
            
            insert new List<FieloCH__ChallengeMissionMember__c>{challengeMissionMember,challengeMissionMember2,challengeMissionMember3,challengeMissionMember4};
            
            String query = '';
            query += 'SELECT Id, FieloCH__Challenge2__c, FieloCH__Mission2__c, FieloCH__Mission2__r.FieloCH__ObjectiveType__c, FieloCH__Mission2__r.RecordTypeId, ';
            query += 'FieloCH__Mission2__r.FieloCH__DynamicObjective__c, FieloCH__Mission2__r.FieloCH__ObjectiveField__c, FieloCH__Mission2__r.FieloCH__ObjectiveValue__c, ';
            query += 'FieloCH__Mission2__r.FieloCH__Operator__c, ';
            query += '(SELECT Id, FieloCH__Counter__c, FieloCH__MaxValue__c, FieloCH__Summary__c, FieloCH__Average__c FROM FieloCH__ChallengeMissionsMembers__r) ';
            query += 'FROM FieloCH__ChallengeMission__c WHERE Id = \'' + challengeMission.Id + '\' OR Id = \'' + challengeMission2.Id + '\' OR Id = \'' + challengeMission3.Id + '\' OR Id = \'' + challengeMission4.Id + '\'';

            List<FieloCH__ChallengeMission__c> listChallengeMission = Database.query(query);
                    
            Map<Id,Decimal> mapResults = FieloPRM_UTILS_ChallengeMethods.loadMissionsStatus(member,challenge,listChallengeMission);

            test.StopTest();
            
        }

    }

    public static testMethod void testUnit3(){

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            List<FieloEE__Member__c> members = new List<FieloEE__Member__c>();
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo',
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'test'
            );
            members.add(member);
            
            FieloEE__Member__c member2 = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo2',
                FieloEE__FirstName__c = 'Marco2' +  String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'test'
            );
            members.add(member2);
            
            insert members;
            
            FieloEE__Program__c program = FieloPRM_UTILS_MockUpFactory.createProgram();
            Date fecha = system.today();

            FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(
                Name = 'Challenge TEST', 
                FieloCH__Mode__c = 'Competition', 
                FieloCH__CompetitionType__c = 'Objective', 
                FieloCH__Subscription__c = 'Global', 
                FieloCH__InitialDate__c = fecha.addDays(-5), 
                FieloCH__FinalDate__c = fecha.addDays(5), 
                FieloCH__SubscribeFrom__c = null, 
                FieloCH__SubscribeTo__c = null, 
                F_PRM_ChallengeFilter__c = 'Ch' + datetime.now(),
                FieloCH__Program__c = program.Id
            );
            insert challenge;

            FieloCH__Mission__c mission = FieloPRM_UTILS_MockUpFactory.createMissionWithoutObjective('Mission 1', 'FieloEE__Transaction__c', 'Average');
            FieloCH__MissionCriteria__c missionCriteria =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_BadgeName__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission.Id );

            FieloCH__Mission__c mission2 = FieloPRM_UTILS_MockUpFactory.createMissionWithoutObjective('Mission 1', 'FieloEE__Transaction__c', 'Counter');
            FieloCH__MissionCriteria__c missionCriteria2 = FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_BadgeName__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission2.id );
            
            FieloCH__Mission__c mission3 = FieloPRM_UTILS_MockUpFactory.createMissionWithoutObjective('Mission 1', 'FieloEE__Transaction__c', 'Max Value');
            FieloCH__MissionCriteria__c missionCriteria3 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_BadgeName__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission3.Id );
            
            FieloCH__Mission__c mission4 = FieloPRM_UTILS_MockUpFactory.createMissionWithoutObjective('Mission 1', 'FieloEE__Transaction__c', 'Summary');
            FieloCH__MissionCriteria__c missionCriteria4 =  FieloPRM_UTILS_MockUpFactory.createCriteria('String', 'F_PRM_BadgeName__c', 'equals', '1', decimal.valueof(1), date.today(), true, mission4.Id );            


            FieloCH.ChallengeUtil.getMapRewardsByPosition(new fieloch__Challenge__c());

            FieloCH__ChallengeMember__c challengeMember = new FieloCH__ChallengeMember__c(
                FieloCH__Challenge2__c = challenge.Id,
                FieloCH__Member__c = member.Id
            );
            insert challengeMember;
            
            FieloCH__ChallengeMission__c challengeMission = new FieloCH__ChallengeMission__c(
                FieloCH__Challenge2__c = challenge.Id 
            );
            challengeMission.put('FieloCH__Mission2__c', mission.Id);

            FieloCH__ChallengeMission__c challengeMission2 = new FieloCH__ChallengeMission__c(
                FieloCH__Challenge2__c = challenge.Id 
            );
            challengeMission2.put('FieloCH__Mission2__c', mission2.Id);

            FieloCH__ChallengeMission__c challengeMission3 = new FieloCH__ChallengeMission__c(
                FieloCH__Challenge2__c = challenge.Id 
            );
            challengeMission3.put('FieloCH__Mission2__c', mission3.Id);

            FieloCH__ChallengeMission__c challengeMission4 = new FieloCH__ChallengeMission__c(
                FieloCH__Challenge2__c = challenge.Id 
            );
            challengeMission4.put('FieloCH__Mission2__c', mission4.Id);

            insert new List<FieloCH__ChallengeMission__c>{challengeMission,challengeMission2,challengeMission3,challengeMission4};
            
            test.StartTest();
            
            FieloCH__ChallengeMissionMember__c challengeMissionMember = new FieloCH__ChallengeMissionMember__c(
                FieloCH__Counter__c = 1,
                FieloCH__MaxValue__c = 1,
                FieloCH__ChallengeMember__c = challengeMember.Id,
                FieloCH__ChallengeMission__c = challengeMission.Id,
                FieloCH__Summary__c = 1,
                FieloCH__AlternativeKey__c = string.valueOf(challengeMission.Id) + string.valueOf(challengeMember.FieloCH__Member__c)
            );

            FieloCH__ChallengeMissionMember__c challengeMissionMember2 = new FieloCH__ChallengeMissionMember__c(
                FieloCH__Counter__c = 1,
                FieloCH__MaxValue__c = 1,
                FieloCH__ChallengeMember__c = challengeMember.Id,
                FieloCH__ChallengeMission__c = challengeMission2.Id,
                FieloCH__Summary__c = 1,
                FieloCH__AlternativeKey__c = string.valueOf(challengeMission2.Id) + string.valueOf(challengeMember.FieloCH__Member__c)
            );

            FieloCH__ChallengeMissionMember__c challengeMissionMember3 = new FieloCH__ChallengeMissionMember__c(
                FieloCH__Counter__c = 1,
                FieloCH__MaxValue__c = 1,
                FieloCH__ChallengeMember__c = challengeMember.Id,
                FieloCH__ChallengeMission__c = challengeMission3.Id,
                FieloCH__Summary__c = 1,
                FieloCH__AlternativeKey__c = string.valueOf(challengeMission3.Id) + string.valueOf(challengeMember.FieloCH__Member__c)
            );

            FieloCH__ChallengeMissionMember__c challengeMissionMember4 = new FieloCH__ChallengeMissionMember__c(
                FieloCH__Counter__c = 1,
                FieloCH__MaxValue__c = 1,
                FieloCH__ChallengeMember__c = challengeMember.Id,
                FieloCH__ChallengeMission__c = challengeMission4.Id,
                FieloCH__Summary__c = 1,
                FieloCH__AlternativeKey__c = string.valueOf(challengeMission4.Id) + string.valueOf(challengeMember.FieloCH__Member__c)
            );

            insert new List<FieloCH__ChallengeMissionMember__c>{challengeMissionMember,challengeMissionMember2,challengeMissionMember3,challengeMissionMember4};
            
            String query = '';
            query += 'SELECT Id, FieloCH__Challenge2__c, FieloCH__Mission2__c, FieloCH__Mission2__r.FieloCH__ObjectiveType__c, FieloCH__Mission2__r.RecordTypeId, ';
            query += 'FieloCH__Mission2__r.FieloCH__DynamicObjective__c, FieloCH__Mission2__r.FieloCH__ObjectiveField__c, FieloCH__Mission2__r.FieloCH__ObjectiveValue__c, ';
            query += 'FieloCH__Mission2__r.FieloCH__Operator__c, ';
            query += '(SELECT Id, FieloCH__Counter__c, FieloCH__MaxValue__c, FieloCH__Summary__c, FieloCH__Average__c FROM FieloCH__ChallengeMissionsMembers__r) ';
            query += 'FROM FieloCH__ChallengeMission__c WHERE Id = \'' + challengeMission.Id + '\' OR Id = \'' + challengeMission2.Id + '\' OR Id = \'' + challengeMission3.Id + '\' OR Id = \'' + challengeMission4.Id + '\'';

            List<FieloCH__ChallengeMission__c> listChallengeMission = Database.query(query);

            Map<Id,Decimal> mapResults = FieloPRM_UTILS_ChallengeMethods.loadMissionsStatus(member,challenge,listChallengeMission);

            test.StopTest();
            
        }

    }

}