/*
    Author: Bhuvana Subramaniyan
    Description: The class redirects user to BOF form if the criteria is satisfied
*/
public with sharing class VFC_LightRITEFormRedirect 
{
    //Variable Declaration
    public OPP_QuoteLink__c quoteLnk{get;set;}
    public String pgMsg{get;set;}
    public boolean isLightRITE{get;set;}
    public string lightRITEId{get;set;}
    
    public List<IPOLightRITE__c> lightRITEForm = new List<IPOLightRITE__c>();    
    public Pagereference pg;
    
    //Constructor
    public VFC_LightRITEFormRedirect(ApexPages.StandardController controller) 
    {
        quoteLnk = (OPP_QuoteLink__c)controller.getRecord();
        //Queries the Quote Link
        quoteLnk = [select TypeOfOffer__c,Name,CurrencyISOCode, Amount__c,OpportunityName__r.MarketSegment__c, OpportunityName__r.TECH_AmountEUR__c,OpportunityName__r.LeadingBusiness__c, CreatedDate from OPP_QuoteLink__c where Id=: quoteLnk.Id];
        
        //Queries the Light RITE
        lightRITEForm = [select ExclusionOfDamagesPlan__c,ExclusionOfDamages__c,ExclusionOfDamagesSME__c,DamageLimitedPlan__c,DamageLimited__c,DamageLimitedSME__c,LegalEntityPlan__c,LegalEntity__c,LegalEntitySME__c,ParentCompanyPlan__c,ParentCompany__c,ParentCompanySME__c,AccountName__c,AppplicableLawJurisdiction__c,AppplicableLawJurisdictionAction__c,AppplicableLawJurisdictionSME__c,BankGuarReqByClient__c,BankGuarReqByClientAction__c,BankGuarReqByClientSME__c,CCO__c,
        ClientLatePayment__c,ClientLatePaymentAction__c,ClientLatePaymentSME__c,ComingIntoForceOfContract__c,ComingIntoForceOfContractAction__c,ComingIntoForceOfContractSME__c,CommChartChangeOrder__c,CommChartChangeOrderAction__c,CommChartChangeOrderSME__c,ContractPrevailOtherContrt__c,ContractPrevailOtherContrtAction__c,ContractPrevailOtherContrtSME__c,CountryOfDestChanges__c,CountryOfDestChangesAction__c,CountryOfDestChangesSME__c,CountryOfDestination__c,CriticalMarketSegment__c,
        CriticalMarketSegmentPlan__c,CustomizedDesignInOffer__c,CustomizedDesignInOfferAction__c,CustomizedDesignInOfferSME__c,DamageToCustomerProperty__c,DamageToCustomerPropertyPlan__c,DownPaymentProgressPayments__c,DownPaymentProgressPaymentsAction__c,DownPaymentProgressPaymentsSME__c,EndUser__c,ExposureToCurrencyVariation__c,ExposureToCurrencyVariationAction__c,ExposureToCurrencyVariationSME__c,GeneralComments__c,LDForDelayPerfCapped__c,LDForDelayPerfCappedAction__c,LDForDelayPerfCappedSME__c,
        MajorOGSuppliersBackDev__c,MajorOGSuppliersBackDevPlan__c,MarketSegmentBU__c,OfferAmount__c,OfferMadeToNewCustomer__c,OfferMadeToNewCustomerPlan__c,OfferNewRenewedContract__c,OfferNewRenewedContractAction__c,OfferNewRenewedContractSME__c,OfferPartOfEndUserCoreProcess__c,OfferPartOfEndUserCoreProcessPlan__c,OGSuppBindingOffers__c,OGSuppBindingOffersAction__c,OGSuppBindingOffersSME__c,OutsideGrpSupplierScope__c,OwnershipOfIntellectualProp__c,OwnershipOfIntellectualPropAction__c,
        OwnershipOfIntellectualPropSME__c,PaymentTermsCreditInsurance__c,PaymentTermsCreditInsuranceAction__c,PaymentTermsCreditInsuranceSME__c,PrevLitigationOrBadDebt__c,PrevLitigationOrBadDebtPlan__c,ProjectDesc__c,ProjectName__c,ProjGrossMarginAboveMinGross__c,ProjGrossMarginAboveMinGrossAction__c,ProjGrossMarginAboveMinGrossSME__c,ProjInvExportOfGoods__c,ProjInvExportOfGoodsAction__c,ProjInvExportOfGoodsSME__c,ProjInvInnovativeFin__c,ProjInvInnovativeFinAction__c,ProjInvInnovativeFinSME__c,
        QuoteLink__c,ReqSchToCommitPerfComm__c,ReqSchToCommitPerfCommAction__c,ReqSchToCommitPerfCommSME__c,SEContent__c,SELiableForThirdParty__c,SELiableForThirdPartyAction__c,SELiableForThirdPartySME__c,SEOFMajorSuppliers__c,SEOFMajorSuppliersAction__c,SEOFMajorSuppliersSME__c,TechnicalPerformanceGuarantee__c,TechnicalPerformanceGuaranteePlan__c, Name, Id from IPOLightRITE__c where QuoteLink__c =: quoteLnk.Id];
                
    }
    //Action method invoked when the VFP_LightRITEFormRedirect page loads
    public pagereference lightRITEForm()
    {
        //Checks if the Quote Link is created after Apr 2014
        Datetime quteDateTime = DateTime.valueOf(Label.CLAPR14SLS62);
        Decimal quoteLinkAmountEUR;
        if(quoteLnk.Amount__c!=null)
            quoteLinkAmountEUR = Utils_Methods.convertCurrencyToEuroforDatedCurrency(quoteLnk.CurrencyIsoCode , quoteLnk.Amount__c);
        if(quoteLnk.createdDate > quteDateTime)
        {
            //Checks if the Quote Link is Budgetary or Non Budgetary and renders the message accordingly
            if(((quoteLnk.TypeOfOffer__c == Label.CLAPR14SLS14 || quoteLnk.TypeOfOffer__c == Label.CLAPR14SLS15 || quoteLnk.TypeOfOffer__c == Label.CLAPR14SLS16) && quoteLinkAmountEUR < 2000000)  || (quoteLnk.TypeOfOffer__c==null))
            {            
                //Redirects to Light RITE Detail page
                if((!(lightRITEForm.isEmpty())) && lightRITEForm[0].Id!=null)
                {
                    lightRITEId = lightRITEForm[0].Id;   
                    pg = new pagereference('/'+lightRITEId);        
                }
                else
                {
                    //Redirects to New Light RITE Form Page prepopulating Quote Link
                    pgMsg = Label.CLAPR14SLS19;
                    isLightRITE = true;            
                    lightRITEId = IPOLightRITE__c.SObjectType.getDescribe().getKeyPrefix()+'/e?retURL=%2F'+quoteLnk.Id+'&quoteLink='+quoteLnk.Id+'&' +Label.CLAPR14SLS54+'='+quoteLnk.OpportunityName__r.LeadingBusiness__c+'&' +Label.CLAPR14SLS57+'='+quoteLnk.OpportunityName__r.MarketSegment__c;                                            
                }          
            }
            else
            {
                pgMsg =Label.CLAPR14SLS18; 
            }
        }
        else
        {
            pgMsg = Label.CLAPR14SLS32;
        }
        return pg;             
    }    
}