public class FieloPRM_C_PartnerLocatorFormController {

    public transient static final String DEF_VALUE_PREFIX = '##DEF##';
    public transient static final String LANGUAGE_REPLACE_TEXT = '{language}';
    public transient static final String COUNTRY_CODE_REPLACE_TEXT = '{countryCode}';

    public transient Id currentComponentId{get;set;}
    public transient string cityzipcodeLabel {get;set;}
    public transient string cityzipcodeErrLabel {get;set;}

    public transient List<PartnerLocatorComponentWrapper> plConfig {get;set;}
     
    transient PartnerLocatorComponentWrapper value;
    public PartnerLocatorComponentWrapper currentComponentWithParameter{
        get{
            if(value==null){
                value = calculateLocator();
            }
            return value;
        }
    }
    public transient String wsURL {get;set;}
    public transient Account fakeAccount {get;set;}
    public transient Id contactId{get;set;}

    transient Boolean switchableLocator =false;
    transient Boolean switchLocator =false;

    public transient Map<String,List<String>> areaOfFocusByBusinessType{get;set;}

    public FieloPRM_C_PartnerLocatorFormController(){
        //areaOfFocusByBusinessType = getAreaOfFocusByBusinessType();
        FieloEE__Program__c program = FieloEE.ProgramUtil.getProgramByDomain();
        wsURL = program!=null?program.FieloEE__SiteURL__c+'services/apexrest/prm':'';
        fakeAccount = new Account();
    }
    
    PartnerLocatorComponentWrapper calculateLocator(){
        PartnerLocatorComponentWrapper returnValue;
        Id memberId = FieloEE.MemberUtil.getmemberId();
        Id categoryId;
        Id tagId;
        FieloEE__Member__c member;

        FieloEE__Member__c[] memberList =  [SELECT F_Country__c,F_Country__r.CountryCode__c,F_Account__c FROM FieloEE__Member__c WHERE Id =: memberId];
        
        if(memberList.size()>0){
            member = memberList[0];
        }
        
        String countryCode;
        if(member!=null)
            countryCode= member.F_Country__r.CountryCode__c!=null?member.F_Country__r.CountryCode__c.toLowerCase():'w';       

        PRMCountry__c prmCountry;
        if(member!=null &&  member.F_Country__c!=null){
          PRMCountry__c[] prmCountryList = [select id,PLDataPool__c,PartnerLocatorSubmitURL__c from PRMCountry__c where Country__c=:member.F_Country__c or MemberCountry1__c=:member.F_Country__c  or MemberCountry2__c=:member.F_Country__c or MemberCountry3__c=:member.F_Country__c or MemberCountry4__c=:member.F_Country__c or MemberCountry5__c=:member.F_Country__c order by CountryPortalEnabled__c desc limit 1];
          if(prmCountryList.size()>0){
              prmCountry = prmCountryList[0];
          }else{
            return new PartnerLocatorComponentWrapper(true,'No country cluster retrieved for this member '+memberId + ',member: '+member );
          }
        }else{
          return new PartnerLocatorComponentWrapper(true,'No member connected or no country defined! MemberId: '+memberId + ',member: '+member );
        }

        List<FieloEE__Component__c> componentList = [select FieloEE__Category__c,FieloEE__Menu__r.F_PRM_Category__c,FieloEE__Tag__c from FieloEE__Component__c where id=:currentComponentId];
        if(componentList.size()==1){
          categoryId = componentList[0].FieloEE__Category__c !=null?componentList[0].FieloEE__Category__c:(componentList[0].FieloEE__Menu__r.F_PRM_Category__c!=null?componentList[0].FieloEE__Menu__r.F_PRM_Category__c:ApexPages.currentPage().getParameters().get('idCategory'));
          tagId = componentList[0].FieloEE__Tag__c !=null?componentList[0].FieloEE__Tag__c:ApexPages.currentPage().getParameters().get('idTag');
        }else{
          return new PartnerLocatorComponentWrapper(true,'No component? '+currentComponentId);
        }
       
        String dataPool = prmCountry.PLDataPool__c;
        String submitURL = prmCountry.PartnerLocatorSubmitURL__c;
        
        List<FieloPRM_PartnerLocatorConfiguration__c> cmp =  [select id,F_PRM_BottomLabel__c,F_PRM_BottomLabel2__c,F_PRM_ShowTwoLocator__c,F_PRM_DistanceUnitValues__c,
                        F_PRM_DistanceValues__c,F_PRM_LocatorParameter__c,F_PRM_ShowBusinessType__c,F_PRM_ShowAreaOfFocus__c,F_PRM_Subtitle__c,F_PRM_Title__c,
                        F_PRM_BusinessTypeIncludeValues__c,F_PRM_BusinessTypeExcludeValues__c,F_PRM_AreaOfFocusIncludeValues__c,F_PRM_AreaOfFocusExcludeValues__c,
                        F_PRM_BusinessTypeDefaultValue__c,F_PRM_AreaOfFocusDefaultValue__c,F_PRM_DistanceUnitValues2__c,F_PRM_DistanceValues2__c,F_PRM_LocatorParameter2__c,
                        F_PRM_ShowBusinessType2__c,F_PRM_ShowAreaOfFocus2__c,F_PRM_Subtitle2__c,F_PRM_Title2__c,F_PRM_BusinessTypeIncludeValues2__c,
                        F_PRM_BusinessTypeExcludeValues2__c,F_PRM_AreaOfFocusIncludeValues2__c,F_PRM_AreaOfFocusExcludeValues2__c,
                        F_PRM_BusinessTypeDefaultValue2__c,F_PRM_AreaOfFocusDefaultValue2__c,F_PRM_LocatorDefault__c
                        from FieloPRM_PartnerLocatorConfiguration__c 
                        where (F_PRM_CountryCluster__c=:prmCountry.Id AND F_PRM_Category__c=null AND F_PRM_Tag__c=null) OR
                               (F_PRM_CountryCluster__c=:prmCountry.Id AND F_PRM_Category__c=:categoryId AND F_PRM_Tag__c=null) OR 
                            (F_PRM_CountryCluster__c=:prmCountry.Id AND F_PRM_Category__c=:categoryId AND F_PRM_Tag__c=:tagId) 
                        order by F_PRM_Tag__c NULLS LAST,F_PRM_Category__c NULLS LAST limit 1]; 
                        
        system.debug('#cmpSize:'+cmp.size());
       
        if(cmp.size()>0){ 
          
          Account[] acc =  [select Id,Name,PRMZipCode__c,PRMCity__c,PRMStateProvince__r.Name from Account where Id =:member.F_Account__c]; 
                    
          String zipCode;        
          if(acc != null && acc.size() > 0){
            
             if(cmp[0].F_PRM_LocatorDefault__c != null && cmp[0].F_PRM_LocatorDefault__c == 'Zipcode'){
                        zipCode  = acc[0].PRMZipCode__c;
                        cityzipcodeLabel = System.label.FieloPRM_CL_023_ZipCode;
                        cityzipcodeErrLabel = System.label.FieloPRM_CL_52_RequiredField;
                    }
                    else{

              if(!String.isBlank(acc[0].PRMCity__c))
                zipCode  = acc[0].PRMCity__c;
              
              if(!String.isBlank(cmp[0].F_PRM_LocatorDefault__c) && cmp[0].F_PRM_LocatorDefault__c == 'CityState' && !String.isBlank(acc[0].PRMStateProvince__r.Name)){
                            if(!String.isBlank(zipCode))
                  zipCode = zipCode+', '+acc[0].PRMStateProvince__r.Name;
                            else
                                zipCode = acc[0].PRMStateProvince__r.Name;
                        }
              
              cityzipcodeLabel = System.label.CLOCT15PRM004;   
                        cityzipcodeErrLabel = System.label.CLOCT15PRM008;

            }
                  
          }    
        
          system.debug('#cmp:'+cmp[0]);
          system.debug('#cmp Labels:'+cmp[0].F_PRM_Title__c+ ' '+cmp[0].F_PRM_Subtitle__c + ' '+cmp[0].F_PRM_BottomLabel__c+ ' '+cmp[0].F_PRM_Title2__c+ ' '+cmp[0].F_PRM_Subtitle2__c+ ' '+cmp[0].F_PRM_BottomLabel2__c);
          switchableLocator = cmp.get(0).F_PRM_ShowTwoLocator__c;
          returnValue = new PartnerLocatorComponentWrapper(zipCode,dataPool,submitURL,countryCode,cmp.get(0));
          PartnerLocatorComponentWrapper tplConfig = new PartnerLocatorComponentWrapper(zipCode,dataPool,submitURL,countryCode,cmp.get(0));
          tplConfig.SwitchLocator(true);
          plConfig = new List<PartnerLocatorComponentWrapper>{returnValue,tplConfig};                     
          
        }else{
          returnValue = new PartnerLocatorComponentWrapper(true,'No parameters are defined for the component: '+currentComponentId);
        }
        return returnValue;
    }
        
    public Boolean getSwitchableLocator(){
        return switchableLocator;
    }

    public PageReference switchSecondLocator() {
        switchLocator = !switchLocator;
        currentComponentWithParameter.switchLocator(switchableLocator && switchLocator);
        return null;
    }

    public enum FieldToSort {
        Label, Value
    }
        
    /*public static void doSort(List<Selectoption> opts, FieldToSort sortField) {
        
        Map<String, Selectoption> mapping = new Map<String, Selectoption>();
        // Suffix to avoid duplicate values like same labels or values are in inbound list 
        Integer suffix = 1;
        for (Selectoption opt : opts) {
            if (sortField == FieldToSort.Label) {
                mapping.put(  // Done this cryptic to save scriptlines, if this loop executes 10000 times
                        // it would every script statement would add 1, so 3 would lead to 30000.
                       (opt.getLabel() + suffix++), // Key using Label + Suffix Counter  
                       opt);   
            } else {
                mapping.put(  
                       (opt.getValue() + suffix++), // Key using Label + Suffix Counter  
                       opt);   
            }
        }
        
        List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();
        // clear the original collection to rebuilt it
        opts.clear();
        
        for (String key : sortKeys) {
            opts.add(mapping.get(key));
        }
    }*/

    public static List<SelectOptionWithSelection> fromTextAreaToSelectOption(String textArea){
       List<SelectOptionWithSelection> options = new List<SelectOptionWithSelection>();
      
        if(textArea!=null){
            for(String currentValue:textArea.split('\r\n', 0)){
                if(currentValue.startsWith(DEF_VALUE_PREFIX)){
                    options.add(new SelectOptionWithSelection(currentValue.subString(DEF_VALUE_PREFIX.length()),true));
                  }else{
                    options.add(new SelectOptionWithSelection(currentValue,false));
                  }
               
            }
        }

        return options;
    }

    public static String fromTextAreaToDefaultValue(String textArea){
      if(textArea!=null){
        Integer defIndex = textArea.indexOf(DEF_VALUE_PREFIX)+DEF_VALUE_PREFIX.length();
        if(textArea.subString(defIndex).indexOf('\r\n')<0)
               return textArea.subString(defIndex);
        return textArea.subString(defIndex,textArea.indexOf('\r\n',defIndex) );
      }
      return null;
    }

    public static Set<String> fromTextAreaToSet(String textArea){
        Set<String> returnList = new Set<String>();
        if(textArea!=null){
            for(String currentValue:textArea.split('\r\n', 0)){
                returnList.add(currentValue);
            }
        }
        return returnList;
    }

    public class PartnerLocatorComponentWrapper{
        public String dataPool{get;set;}
        public String zipCode{get;set;}
        public String title {get;set;}
        public String subTitle {get;set;}
        public Boolean showBuTy{get;set;}
        public Boolean showAoFo{get;set;}
        public String url{get;set;}
        public String plCfg{get;set;}
        public String locator{get;set;}
        public String bottomTextValue{get;set;}
        public String bottomLink{get;set;}
        public Boolean hasError{get;set;}
        public String errorMessage{get;set;}
        public String distanceValues{get;set;}
        public String defaultDistanceValue{get;set;}
        public String distanceUnitValues{get;set;}
        public String defaultDistanceUnit{get;set;}
        public Set<String> areaOfFocusIncludeValues{get;set;}
        public Set<String> areaOfFocusExcludeValues{get;set;}
        public Set<String> businessTypeIncludeValues{get;set;}
        public Set<String> businessTypeExcludeValues{get;set;}
        public String businessTypeDefaultValue {get;set;}
        public String areaOfFocusDefaultValue {get;set;}
        public String locale {get;set;}
        private FieloPRM_PartnerLocatorConfiguration__c componentParams;
    
        public PartnerLocatorComponentWrapper(String zipCode,String dataPool,String submitURL,String countryCode,FieloPRM_PartnerLocatorConfiguration__c componentParams){
          this.zipCode = zipCode;
          this.componentParams=componentParams;
          this.locale = UserInfo.getLanguage();
          String language;
          //TODO convert language to HTTP Accept-language compatible: http://www.metamodpro.com/browser-language-codes
          if(this.locale!=null){
              this.locale = this.locale.replace('_','-');
              language = this.locale.substring(0, 2);
          }
          this.dataPool = dataPool!=null?dataPool.replace(FieloPRM_C_PartnerLocatorFormController.LANGUAGE_REPLACE_TEXT,language).replace(FieloPRM_C_PartnerLocatorFormController.COUNTRY_CODE_REPLACE_TEXT,countryCode):null;
          this.url = submitURL!=null?submitURL.replace(FieloPRM_C_PartnerLocatorFormController.LANGUAGE_REPLACE_TEXT,language).replace(FieloPRM_C_PartnerLocatorFormController.COUNTRY_CODE_REPLACE_TEXT,countryCode):'';
          switchLocator(false);
        }

        public void switchLocator(Boolean switchLocator){
          if(switchLocator){
            this.title = componentParams.F_PRM_Title2__c;
            this.subTitle = componentParams.F_PRM_Subtitle2__c;
            this.showBuTy =componentParams.F_PRM_ShowBusinessType2__c;
            this.showAoFo =componentParams.F_PRM_ShowAreaOfFocus2__c;
            this.locator = componentParams.F_PRM_LocatorParameter2__c;
            this.bottomTextValue = componentParams.F_PRM_BottomLabel2__c;
            this.distanceValues = componentParams.F_PRM_DistanceValues2__c;
            this.distanceUnitValues = componentParams.F_PRM_DistanceUnitValues2__c;
            this.defaultDistanceValue =FieloPRM_C_PartnerLocatorFormController.fromTextAreaToDefaultValue(this.distanceValues);
            this.defaultDistanceUnit =FieloPRM_C_PartnerLocatorFormController.fromTextAreaToDefaultValue(this.distanceUnitValues);
            this.areaOfFocusIncludeValues = fromTextAreaToSet(componentParams.F_PRM_AreaOfFocusIncludeValues2__c);
            this.areaOfFocusExcludeValues = fromTextAreaToSet(componentParams.F_PRM_AreaOfFocusExcludeValues2__c);
            this.businessTypeIncludeValues = fromTextAreaToSet(componentParams.F_PRM_BusinessTypeIncludeValues2__c);
            this.businessTypeExcludeValues = fromTextAreaToSet(componentParams.F_PRM_BusinessTypeExcludeValues2__c);
            this.businessTypeDefaultValue = componentParams.F_PRM_BusinessTypeDefaultValue2__c;
            this.areaOfFocusDefaultValue = componentParams.F_PRM_AreaOfFocusDefaultValue2__c;
            this.plCfg = componentParams.Id+'_2';
          }else{
            this.title = componentParams.F_PRM_Title__c;
            this.subTitle = componentParams.F_PRM_Subtitle__c;
            this.showBuTy =componentParams.F_PRM_ShowBusinessType__c;
            this.showAoFo =componentParams.F_PRM_ShowAreaOfFocus__c;
            this.locator = componentParams.F_PRM_LocatorParameter__c;
            this.bottomTextValue = componentParams.F_PRM_BottomLabel__c;
            this.distanceValues = componentParams.F_PRM_DistanceValues__c;
            this.distanceUnitValues = componentParams.F_PRM_DistanceUnitValues__c;
            this.defaultDistanceValue =FieloPRM_C_PartnerLocatorFormController.fromTextAreaToDefaultValue(this.distanceValues);
            this.defaultDistanceUnit =FieloPRM_C_PartnerLocatorFormController.fromTextAreaToDefaultValue(this.distanceUnitValues);
            this.areaOfFocusIncludeValues = fromTextAreaToSet(componentParams.F_PRM_AreaOfFocusIncludeValues__c);
            this.areaOfFocusExcludeValues = fromTextAreaToSet(componentParams.F_PRM_AreaOfFocusExcludeValues__c);
            this.businessTypeIncludeValues = fromTextAreaToSet(componentParams.F_PRM_BusinessTypeIncludeValues__c);
            this.businessTypeExcludeValues = fromTextAreaToSet(componentParams.F_PRM_BusinessTypeExcludeValues__c);
            this.businessTypeDefaultValue = componentParams.F_PRM_BusinessTypeDefaultValue__c;
            this.areaOfFocusDefaultValue = componentParams.F_PRM_AreaOfFocusDefaultValue__c;
            this.plCfg = componentParams.Id+'_';
          }
        }

        public PartnerLocatorComponentWrapper(Boolean hasError,String errorMessage){
            this.hasError =    hasError;
            this.errorMessage=errorMessage;
        }       
    }

    public class SelectOptionWithSelection{
        public String value {get;set;}
        public Boolean selected {get;set;}

        public SelectOptionWithSelection(String value, Boolean selected){
          this.value=value;
          this.selected = selected;
        }
    }
}