/* RESCHEDULES OPPORTUNITY SERIES CLOSE DATE */
public with sharing class VFC_RescheduleOpportunitys
{
    public Id agreementId;
    public List<Opportunity> opptys = new List<Opportunity>();
    public String pgMsg{get;set;}
    public Integer totalOpps;
    public List<OpportunityList> dispSeriesOpptys{get;set;}
    public OppAgreement__c agreement {get;set;}
    public boolean dispOKButton {get;set;}
    public boolean disableResh{get;set;}
    Date dtInternal,agreementCloseDate,agreementValidFromDate;
    integer AgreementDaysinEnd,daystobeaddedforlastdate;        
    string agreementtimespan;
    decimal intervalsPeriod;
    VFC20_CreateSeries createSeries;
        
    //CONSTRUCTOR - QUERIES AGREEMENT RELATED FIELDS
    public VFC_RescheduleOpportunitys(ApexPages.StandardSetController controller) 
    {
        agreementId = system.currentPagereference().getParameters().get('id');
        agreement = [select Id, Name,Agreementvalidfrom__c, TECH_IsSeriesCreated__c, Agreement_Duration_in_month__c,AgreementvalidUntil__c, FrameOpportunityInterval__c, AgreementTimeSpan__c from OppAgreement__c where Id=:agreementId];        
        opptys = [select Id, CloseDate, Name,AgreementReference__c,StageName from Opportunity where AgreementReference__c =: agreementId ORDER BY CreatedDate];        
        totalOpps = opptys.size();
        dispSeriesOpptys = new List<OpportunityList>();
        
        createSeries = new VFC20_CreateSeries(new ApexPages.StandardController(agreement));  
        agreementtimespan = createSeries.GetAgreementInterval(new List<OppAgreement__c>{agreement},totalOpps);
        intervalsPeriod= decimal.valueOf(agreementtimespan);            
        for(opportunity opp: opptys)
        {
            if(opp.StageName!=Label.CL00272)
                dispSeriesOpptys.add(new OpportunityList(opp, true));
            else
            {
                dispSeriesOpptys.add(new OpportunityList(opp, false));                        
                disableResh = true;
            }
        }
        if(totalOpps<=1)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,label.CLAPR15SLS62);
            ApexPages.addMessage(myMsg);
            dispOKButton = true;         
        }
        else if(intervalsPeriod!=totalOpps)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,label.CLAPR15SLS84);
            ApexPages.addMessage(myMsg);
            disableResh = true;

        }
    }
    
    //DISPLAYS THE OPPORTUNITY SERIES CLOSED DATE ACCORDING TO MODIFIED AGREEMENT'S CLOSE DATE
    Public void displaySeries()
    {
        dispSeriesOpptys = new List<OpportunityList>();        
        
        if(agreementValidFromDate!=null && agreementValidFromDate!=agreement.Agreementvalidfrom__c)
        {
            agreement.AgreementvalidUntil__c = date.newInstance((agreement.Agreementvalidfrom__c).year()+((agreement.Agreementvalidfrom__c).month()+Integer.valueOf((agreement.Agreement_Duration_in_month__c)))/12,
            Integer.valueOf((Math.MOD((agreement.Agreementvalidfrom__c.month())+Integer.valueOf((agreement.Agreement_Duration_in_month__c)),12))),1
            )-1;
        }
        agreementValidFromDate = agreement.Agreementvalidfrom__c;
        if(totalOpps>1)
        {
            //UPDATES CLOSE DATE FOR MASTER OPPORTUNITY
            Opportunity masterOpportunity = opptys[0];
            if(agreement.FrameOpportunityInterval__c == Label.CL00221)
            {
                dtInternal = createSeries.UpdateCloseDateForMasterOpportunity(agreement.Agreementvalidfrom__c, agreement.Agreementvaliduntil__c, agreement.FrameOpportunityInterval__c);
                AgreementDaysinEnd = date.daysInMonth(agreement.Agreementvaliduntil__c.year(),agreement.Agreementvaliduntil__c.month());
                daystobeaddedforlastdate = AgreementDaysinEnd  - agreement.Agreementvaliduntil__c.day();
                agreementCloseDate=agreement.Agreementvaliduntil__c.addDays(daystobeaddedforlastdate);
                if(agreementCloseDate <  dtInternal )
                {
                    masterOpportunity.CloseDate = agreementCloseDate;    
                }   
                else
                {
                    masterOpportunity.CloseDate = createSeries.UpdateCloseDateForMasterOpportunity(agreement.Agreementvalidfrom__c, agreement.Agreementvaliduntil__c, agreement.FrameOpportunityInterval__c);
                }             
            } 
            else
            {
                masterOpportunity.CloseDate = createSeries.UpdateCloseDateForMasterOpportunity(agreement.Agreementvalidfrom__c, agreement.Agreementvaliduntil__c, agreement.FrameOpportunityInterval__c);
            }    
            masterOpportunity.Name = changeOpptyName(masterOpportunity);
            if(masterOpportunity.StageName!=Label.CL00272)
                dispSeriesOpptys.add(new OpportunityList(masterOpportunity, true));
            else
                dispSeriesOpptys.add(new OpportunityList(masterOpportunity, false));                        

            //UPDATES CLOSE DATE OF SERIES OPPORTUNITY
            for(Integer i=1;i<=intervalsPeriod - 1;i++)
            {
                Opportunity objOppr= opptys[i];
                if(i == (intervalsPeriod - 1))
                {
                    /*if(agreement.FrameOpportunityInterval__c == Label.CL00225)
                    {
                        integer mintTotDays = date.daysInMonth(agreement.Agreementvaliduntil__c.year(),agreement.Agreementvaliduntil__c.month());
                        integer mintTotDaysToBeAdded = mintTotDays  - agreement.Agreementvaliduntil__c.day();
                        Date dtCloseDateLast = agreement.Agreementvaliduntil__c.addDays(mintTotDaysToBeAdded);
                        objOppr.CloseDate = dtCloseDateLast; //GetCloseDate(agreements .Agreementvaliduntil__c,mstrIntervalPeriod,i);
                    }
                    else
                    {*/
                        objOppr.CloseDate = createSeries.GetCloseDate(masterOpportunity.CloseDate,agreement.FrameOpportunityInterval__c,i);                                        
                    //}
                }
                else
                {
                    objOppr.CloseDate = createSeries.GetCloseDate(masterOpportunity.CloseDate,agreement.FrameOpportunityInterval__c,i);                                        
                }
                objOppr.Name = changeOpptyName(objOppr);
                if(objOppr.StageName!=Label.CL00272)
                    dispSeriesOpptys.add(new OpportunityList(objOppr, true));
                else
                    dispSeriesOpptys.add(new OpportunityList(objOppr, false));                        
                system.debug(dispSeriesOpptys + '$$$$$$$$ Display Series');        
            }
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,label.CLAPR15SLS62);
            ApexPages.addMessage(myMsg);
            dispOKButton = true;
        }        
    }
    //UPDATES THE CLOSE DATE OF OPPTY SERIES & AGREEMENT START DATE  & REDIRECTS BACK TO AGREEMENT PGAE
    public pagereference save()
    {        
        ApexPages.Message myMsg;
        Database.SaveResult agreementToUpdate = Database.update(agreement, false);
        
            if(!agreementToUpdate.isSuccess())
            {
                Database.Error err = agreementToUpdate.getErrors()[0];
                System.Debug('######## Error Inserting: '+err.getStatusCode()+' '+err.getMessage());   
                myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage());
                ApexPages.addMessage(myMsg);             
            }
        List<Opportunity> opptysToBeUpdated = new List<Opportunity>();
        for(OpportunityList o: dispSeriesOpptys)
        {
            Opportunity opp = o.opp;
            opp.Name = changeOpptyName(opp);
            opptysToBeUpdated.add(opp);
        }
        Database.SaveResult[] opptysToUpdate= Database.update(opptysToBeUpdated, false);
        for(Database.SaveResult sr: opptysToUpdate)
        {
            if(!sr.isSuccess())
            {
                Database.Error err = sr.getErrors()[0];
                System.Debug('######## Error Inserting: '+err.getStatusCode()+' '+err.getMessage());   
                myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage());
                ApexPages.addMessage(myMsg);             
            }
        }  
        if(myMsg != null)
        {
            return null;
        }
        else
        return new pagereference('/'+agreementId);
    }
    //REDIRECTS BACK TO AGREEMENT PGAE
    public pagereference cancel()
    {
        return new pagereference('/'+agreementId);
    }
    //UPDATES OPPORTUNITY NAME WITH THE UPDATED CLOSE DATE
    public String changeOpptyName(Opportunity opp)
    {
         integer mintPos = opp.Name.lastIndexOf('_');
         String actualName,masterCloseMonth;           
         if(mintPos != -1)
         {
             actualName= opp.Name.substring(0,mintPos);
         }
         else
         {
             actualName= opp.Name;
         }
         if(opp.CloseDate.Month() >= 10)
         {
             masterCloseMonth = String.valueOf(opp.CloseDate.Month());
         }
         else
         {
             masterCloseMonth ='0' +  String.valueOf(opp.CloseDate.Month());
         }
         opp.Name = actualName+'_'+String.valueOf(opp.CloseDate.day()) + '/'+ masterCloseMonth  + '/' + String.valueOf(opp.CloseDate.Year()) ;
         return opp.Name;
    }
    
    public class OpportunityList
    {
        public Opportunity opp{get;set;}
        public Boolean rendoppCloseDate{get;set;}
        public OpportunityList(Opportunity o, Boolean rendCloseDate)
        {
            opp = o;
            rendoppCloseDate= rendCloseDate;
            if(rendCloseDate == false)
            {
                ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Info,Label.CLAPR15SLS83);        
                ApexPages.addMessage(myMsg2);                    
            }
        }
    }
}