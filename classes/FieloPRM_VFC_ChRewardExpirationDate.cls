public with sharing class FieloPRM_VFC_ChRewardExpirationDate {

    public list<FieloCH__ChallengeReward__c> challengeRewards{get;set;}
    public FieloCH__Challenge__c challenge{get;set;}
    public boolean showForm{get;set;}


    public FieloPRM_VFC_ChRewardExpirationDate(ApexPages.StandardController stdController) {
        
        //stdController.addFields(new List<String>{'FieloCH__IsActive__c'});
        
        challenge = (FieloCH__Challenge__c)stdController.getRecord();
        
        List<FieloCH__Challenge__c> listChallenge = [SELECT Id, FieloCH__IsActive__c FROM FieloCH__Challenge__c WHERE Id =: challenge.Id];
        
        challenge = listChallenge[0];
        
        showForm = true;
        
        if(challenge.FieloCH__IsActive__c){
            
            showForm = false;
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, label.FieloPRM_CL_61_ChRewardsExpDateInactiveChallenge));
            
            
        }else{
                     
            List<FieloCH__MissionChallenge__c> missionChallenge = [SELECT Id FROM FieloCH__MissionChallenge__c WHERE FieloCH__Challenge__c =: challenge.Id LIMIT 1];
            
            if(missionChallenge.isEmpty()){
                
                showForm = false;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, label.FieloPRM_CL_62_ChRewardsExpDateNoMissions));       
            
            }else{
                      
                String soql = 'SELECT ';
                for(String fieldName : Schema.getGlobalDescribe().get('FieloCH__ChallengeReward__c').getDescribe().fields.getMap().keySet()) {
                    soql += fieldName + ', ';
                }
                soql += 'FieloCH__Badge__r.Name' + ', ';
                soql = soql.left(soql.lastIndexOf(','));
                soql += ' FROM ' + 'FieloCH__ChallengeReward__c' + ' WHERE FieloCH__Challenge__c = \'' + challenge.id + '\' AND F_TypeReward__c = \'Level\' ORDER BY FieloCH__Position__c';
                
                challengeRewards = (List<FieloCH__ChallengeReward__c>) Database.query(soql);
                
                if(challengeRewards.isEmpty()){     
                    showForm = false;        
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, label.FieloPRM_CL_63_ChRewardsExpDateNoLevelRewards));
                }
            }
                
        }
         
    }

    public PageReference save(){

        try{
            update challengeRewards;
        }catch(Exception e){
             ApexPages.addMessages(e);
             return  null;
        }
        return new PageReference('/' + challenge.id);
    }
}