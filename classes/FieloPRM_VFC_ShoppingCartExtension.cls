/************************************************************
* Developer: Fielo Team                                     *
* Type: VF Page Extension                                   *
* Page: FieloPRM_VFP_ShoppingCart                           *
************************************************************/
public with sharing class FieloPRM_VFC_ShoppingCartExtension{

    public Boolean showMessageSuccess {get;set;}
       
    private FieloEE.ShoppingCartItemsController sp_controller;
    
    public FieloPRM_VFC_ShoppingCartExtension(FieloEE.ShoppingCartItemsController controller){       
        sp_controller = controller;               
    }
    
    public PageReference doVoucherCustom(){
        showMessageSuccess = false;
        if(sp_controller.doVoucher() != null){
            showMessageSuccess = true;
        }
        return null;
    }   
    
}