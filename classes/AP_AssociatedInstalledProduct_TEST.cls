/**
    17/Dec/13     Deepak     Test class for the VFC_CreateCoveredProduct  controller
 */
@isTest
public class AP_AssociatedInstalledProduct_TEST 
{
    static testMethod void VFC_Test() 
    {   
       // test.StartTest();
        Account accountSobj = Utils_TestMethods.createAccount();
        insert accountSobj;
        
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c();
        sc.SVMXC__Company__c = accountSobj.Id;
        sc.SVMXC__End_Date__c=date.today();
        sc.SVMXC__Start_Date__c=date.Today();
        insert sc;
        
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = accountSobj.id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = accountSobj.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId1';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        ip1.DeviceTypeToCreate__c='test';
        insert ip1;
        
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
        ip2.SVMXC__Company__c = accountSobj.id;
        ip2.Name = 'Test Intalled Product ';
        ip2.SVMXC__Status__c= 'new';
        ip2.GoldenAssetId__c = 'GoledenAssertId132';
        ip2.BrandToCreate__c ='Test Brand';
        ip2.SVMXC__Site__c = site1.id;
        ip2.DeviceTypeToCreate__c='test';
        insert ip2;
         SVMXC__Installed_Product__c ip3 = new SVMXC__Installed_Product__c();
        ip3.SVMXC__Company__c = accountSobj.id;
        ip3.Name = 'Test Intalled Product3 ';
        ip3.SVMXC__Status__c= 'new';
        ip3.GoldenAssetId__c = 'GoledenAssertId132111';
        ip3.BrandToCreate__c ='Test Brand';
        ip3.SVMXC__Site__c = site1.id;
        ip3.DeviceTypeToCreate__c='test';
        insert ip3;
        AssociatedInstalledProduct__c aip = New AssociatedInstalledProduct__c();
        aip.InstalledProduct__c = ip1.Id;
        aip.ServiceMaintenanceContract__c = sc.Id;
        
        insert aip;
        
        AssociatedInstalledProduct__c aip1 = New AssociatedInstalledProduct__c();
        aip1.InstalledProduct__c = ip2.Id;
        aip1.ServiceMaintenanceContract__c = sc.Id;
        
        insert aip1;
        list<AssociatedInstalledProduct__c > aiplist = New List<AssociatedInstalledProduct__c >();
        aiplist.add(aip);
        AP_AssociatedInstalledProduct.AddError(aiplist);
        //aip.TECH_UniqueSCIB__c='test';
        aip.InstalledProduct__c = ip3.Id;
        update aip;
        
          }
}