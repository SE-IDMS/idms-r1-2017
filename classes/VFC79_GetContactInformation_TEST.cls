//*********************************************************************************
// Class Name       : VFC79_GetContactInformation_TEST
// Purpose          : Test Class for VFC79_GetContactInformation
// Created by       : Srinivas Nallapati
// Date created     : 17th Feb 2012
// Modified by      :
// Date Modified    :
// Remarks          : 
// ********************************************************************************
// -----------------------------------------------------------------
//                               MODIFICATION HISTORY
// -----------------------------------------------------------------
// Modified By     : Rakhi Kumar - Global Delivery Team
// Modified Date   : 11/03/2013
// Description     : May 2013 Release: Test cases fors additional 
//                   search on ANI object implemented(CTI Integration)
// -----------------------------------------------------------------
@isTest
private with sharing class VFC79_GetContactInformation_TEST {
    
    @testSetup static void testData() {
        String SfUrl = URL.getSalesforceBaseUrl().toExternalForm();
        
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        
        list<Account> newAccounts = new list<Account>();
        list<Contact> newContacts = new list<Contact>();
        list<ANI__c> newANIs = new list<ANI__c>();
        
        Account account = Utils_TestMethods.createAccount();
        account.Phone = '986435';
        account.LeadingBusiness__c = 'Buildings';
        account.ClassLevel1__c = 'Retailer';
        account.Type = 'Prospect';
        //account.CurrencyIsoCode ='IND';
        account.City__c = 'bangalore';
        account.ZipCode__c= '452234';
        account.Street__c= 'aSDASDASD';
        account.POBox__c = '122344';
        account.Country__c = testCountry.Id;
        newAccounts.add(account);
        system.debug('#######'+account);
        
        Account account4 = Utils_TestMethods.createAccount();
        account4.Phone = '986';
        account4.LeadingBusiness__c = 'Buildings';
        account4.ClassLevel1__c = 'Retailer';
        account4.Type = 'Prospect';
        account4.City__c = 'bangalore';
        account4.ZipCode__c= '452234';
        account4.Street__c= 'aSDArSDASD';
        account4.POBox__c = '1223443';
        account4.Country__c = testCountry.Id;
        newAccounts.add(account4);
        system.debug('#######'+account4);
        
        Account account5 = Utils_TestMethods.createAccount();
        account5.Phone = '986';
        account5.LeadingBusiness__c = 'Buildings';
        account5.ClassLevel1__c = 'Retailer';
        account5.Type = 'Prospect';
        account5.City__c = 'bangalore';
        account5.ZipCode__c= '4522344';
        account5.Street__c= 'aSDASjDASD';
        account5.POBox__c = '1223434';
        account5.Country__c = testCountry.Id;
        newAccounts.add(account5);
        system.debug('#######'+account5);
        
        
        Account account2 = Utils_TestMethods.createAccount();
        account2.Phone = '426272';
        account2.LeadingBusiness__c = 'Buildings';
        account2.ClassLevel1__c = 'Retailer';
        account2.Type = 'Prospect';
        account2.City__c = 'bangalore';
        account2.ZipCode__c= '452234';
        account2.Street__c= 'aSDASDASD';
        account2.POBox__c = '122344';
        account2.Country__c = testCountry.Id;
        newAccounts.add(account2); 
        system.debug('#######' + account2);
        
        Account account3 = Utils_TestMethods.createAccount();
        account3.Phone = '99999997';
        account3.LeadingBusiness__c = 'Buildings';
        account3.ClassLevel1__c = 'Retailer';
        account3.Type = 'Prospect';
        account3.City__c = 'bangalore';
        account3.ZipCode__c= '452234';
        account3.Street__c= 'aSDASDASD';
        account3.POBox__c = '122344';
        account3.Country__c = testCountry.Id;
        newAccounts.add(account3); 
        system.debug('#######' + account3);
        
        insert newAccounts;
                
        Contact con = Utils_TestMethods.createContact(account.id, 'test1');
        con.firstname= 'test55';
        con.LastName = 'asfsdf';
        con.WorkPhone__c= '723472349';
        con.accountid=account.id;
        newContacts.add(con);
        system.debug('#######' + con);
        
        Contact con2 = Utils_TestMethods.createContact(account.id, 'test2');
        con2.firstname= 'test55';
        con2.LastName = 'asfsdf';
        con2.WorkPhone__c= '986435';
        con2.accountid=account.id;
        newContacts.add(con2);
        system.debug('#######' + con2);
        
        Contact con3 = Utils_TestMethods.createContact(account.id, 'Contact 3');
        con3.firstname = 'Contact';
        con3.LastName = 'Test 3';
        con3.WorkPhone__c = '986435';
        con3.accountid = account2.id;
        newContacts.add(con3);
        system.debug('#######' + con3);
        
        insert newContacts;
        
        Case objCase = Utils_TestMethods.createCase(account.Id, con3.Id,'testing');
        insert objCase;
        
        //Create ANI object related to Contact con - No. 99999997
        Ani__c ani1 = new Ani__c();
        ani1.ANINumber__c = '781535';
        ani1.Contact__c = con.id;
        newANIs.add(ani1);
        
        //Create another ANI object with  No. 99999997 related to Con2 contact
        Ani__c ani2 = new Ani__c();
        ani2.ANINumber__c = '986435';
        ani2.Contact__c = con2.id;
        newANIs.add(ani2);
        
        //Create another ANI object with  No. 99999997 related to Con2 contact
        Ani__c ani3 = new Ani__c();
        ani3.ANINumber__c = '99999997';
        ani3.Account__c = account.id;
        newANIs.add(ani3);
        
        insert newANIs;
        Contract controbj=Utils_TestMethods.createContract(account.id,con2.id);
        insert controbj;
        CTR_ValueChainPlayers__c  CVCPObj =Utils_TestMethods.createCntrctValuChnPlyr(controbj.Id);
        CVCPObj.LegacyPIN__c = '123456'; 
        insert CVCPObj;
    }

    
    @isTest static void verifyPhoneNoTest() {
        List<Id> fixedSearchResults= new List<Id>();
        for(Account a :[select id from Account]) {
            fixedSearchResults.add(a.Id);
        }
        for(Contact c : [select id from contact]) {
            fixedSearchResults.add(c.Id);
        }
        for(Ani__c a : [select id from Ani__c]) {
            fixedSearchResults.add(a.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        VFC79_GetContactInformation.verifyPIN('123456');
        VFC79_GetContactInformation.findPhone('986435');
        VFC79_GetContactInformation.findPhone('99999997');
    }
    
}