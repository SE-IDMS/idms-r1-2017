public with sharing class VFC_RequirementCertifications {
    public list<RequirementCertification__c> lstReqCert {get;set;}
    public VFC_RequirementCertifications(ApexPages.StandardController controller) {
        lstReqCert = [Select name, CertificationCatalog__c, CertificationCatalog__r.Name,CertificationCatalog__r.CertificationName__c from RequirementCertification__c WHERE RequirementCatalog__c=:controller.getId() ];
    }

}