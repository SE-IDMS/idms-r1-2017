global class CaseArchiving_ProcessRelatedCaseSchedule implements Schedulable {
    global void execute(SchedulableContext SC) {
        Integer size = Integer.Valueof(System.Label.CLJUL14CCC08);
        CaseArchiving_ProcessRelatedCase testh= new CaseArchiving_ProcessRelatedCase();
        Database.executeBatch(testh,size);
    }
}