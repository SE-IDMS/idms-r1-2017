public class VFC_ImportPRMContacts{
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    public List<WrapperClass> contactGridToDisplay{get;set;}
    public List<WrapperClass> contactsGridToDisplay{get;set;}
    public List<WrapperClass> contactGridToExport{get;set;}
    public Set<String> countryLst = new Set<String>();
    public Set<String> statesLst = new Set<String>();
    public Set<String> channelLst = new Set<String>();
    public List<UIMSCompany__c> contactsProcessed = new List<UIMSCompany__c>();
    //public List<ExportWrapperClass> recordsToExport{get;set;}
    public Boolean showProcess{get;set;}
    public Boolean ShowGenerateLink{get;set;}
    public String prmOriginSource{get;set;}
    public String originSourceInfo{get;set;}
    public Map<String,List<WrapperClass>> cntryMap = new Map<String,List<WrapperClass>>();
    public Map<String,List<WrapperClass>> cntryStateMap = new Map<String,List<WrapperClass>>();
    public Map<String,List<WrapperClass>> cntryChannelMap = new Map<String,List<WrapperClass>>();
    public Map<String,List<WrapperClass>> stateValidateMap = new Map<String,List<WrapperClass>>();
    public Map<String,List<WrapperClass>> ChannelValidateMap = new Map<String,List<WrapperClass>>();
    public Map<String,String> CountryNameMap= new Map<String,String>();
    public Map<String,String> StateNameMap= new Map<String,String>();
    public Set<String> stateSet = new Set<String>();
    public Set<String> emailLst = new Set<String>();
    public Set<String> businessTypeSet = new Set<String>();
    public Set<String> areaOfFocusSet = new Set<String>();
    public Map<String,WrapperClass> finalRecordsLst = new Map<String,WrapperClass>();
    public Integer validRecordsCount{get;set;}
    public Set<String> duplicateEmail{get;set;}
    public String batchId{get;set;}
    public String ExportFileName {get;set;}
    
    public VFC_ImportPRMContacts(ApexPages.StandardController controller) {
        showProcess = false;
        showGenerateLink = false;
        validRecordsCount = 0;
        
    }
    
    public List<SelectOption> getOriginSourceOptions() {
        List<SelectOption> originOptions= new List<SelectOption>();
        originOptions.add(new SelectOption('','--NONE--'));
        Schema.DescribeFieldResult fieldResult = UIMSCompany__c.OriginSource__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
            if(f.getLabel() != 'Other')
                originOptions.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        originOptions.sort();
        originOptions.add(new SelectOption('Other','Other'));
        return originOptions;
    }
    
    public pagereference ReadFile() {
        
        System.debug('Inside read');
        nameFile=contentFile.toString();
        System.debug('<<>>'+nameFile);
        filelines = nameFile.split('\n');
        cntryMap.clear();
        cntryStateMap.clear();
        cntryChannelMap.clear();
        contactGridToDisplay = new List<WrapperClass>();
        contactsGridToDisplay = new List<WrapperClass>();
        System.debug('final list size 1:'+contactsGridToDisplay.size());
        System.debug('final list size 2:'+contactGridToDisplay.size());
        contactGridToDisplay.clear();
        contactsGridToDisplay.clear();
        System.debug('>>>>validRecordsCount:'+validRecordsCount);
        validRecordsCount = 0;
        System.debug('>>>>validRecordsCount:'+validRecordsCount);
        duplicateEmail = new Set<String>();
        //wrapperMap = new Map<String,WrapperClass>();
        WrapperClass w;
        //String pcode='';
        System.debug('>>>>size'+filelines.size());
        String EmailRegex = '([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))';
        List<String> headersLst = new List<String>{'Email','First Name','Last Name','Country','Language','Business Type','Area of Focus','Phone Number','Phone Type','Job Function','Job Title','Contact Tax Id','T & C Accepted?','Company Name','Do Not Have Company?','Headquarters','Company Phone Number','Company Website','Address 1','Address 2','City','State / Province','Zip / Postal Code','Include Company Info In Public Search','Company Tax Id'};
        System.debug('headersLst:'+headersLst.size());
        
        try{
        if(filelines.size() > 0) {
            
            List<String> fileHeaders = filelines[0].split(',');
            fileHeaders[fileHeaders.size() - 1] = fileHeaders[fileHeaders.size() - 1].normalizeSpace();
            Map<String,String> valuesMap;
            if(fileHeaders == headersLst){
                
            for (Integer i=1;i<filelines.size();i++) {
                //String code ='';
                System.debug('Inside For');
                String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');
                UIMSCompany__c a = new UIMSCompany__c();
                AP_UserRegModel usrModel = new AP_UserRegModel();
                w = new WrapperClass();
                valuesMap = new Map<String,String>{'Email' => string.valueOf(inputvalues[0]),'FirstName' => string.valueOf(inputvalues[1]),'LastName' => string.valueOf(inputvalues[2]) ,'Country' => string.valueOf(inputvalues[3]),'Language' => string.valueOf(inputvalues[4]),'BusinessType' => string.valueOf(inputvalues[5]),'AreaofFocus' => string.valueOf(inputvalues[6]),'PhoneNumber' => inputvalues[7],'PhoneType' => string.valueOf(inputvalues[8]),'JobFunction' => string.valueOf(inputvalues[9]),'JobTitle' => string.valueOf(inputvalues[10]),'ContactTaxId' => string.valueOf(inputvalues[11]),'T&CAccepted?' => string.valueOf(inputvalues[12]),'CompanyName' => string.valueOf(inputvalues[13]),'DoNotHaveCompany?' => string.valueOf(inputvalues[14]),'Headquarters' => string.valueOf(inputvalues[15]),'CompanyPhoneNumber' => string.valueOf(inputvalues[16]),'CompanyWebsite' => string.valueOf(inputvalues[17]),'Address1' => string.valueOf(inputvalues[18]),'Address2' => string.valueOf(inputvalues[19]),'City' => string.valueOf(inputvalues[20]),'StateProvince' => string.valueOf(inputvalues[21]),'ZipPostalCode' => string.valueOf(inputvalues[22]),'IncludeCompanyInfoInPublicSearch' => string.valueOf(inputvalues[23]),'CompanyTaxId' => string.valueOf(inputvalues[24])};
                System.debug('>>>>valuesMap'+valuesMap);
                a.Email__c = usrModel.email = usrModel.confirmEmail = valuesMap.get('Email');
                a.FirstName__c = usrModel.firstName = valuesMap.get('FirstName');       
                a.LastName__c = usrModel.lastName = valuesMap.get('LastName');
                a.Language__c = usrModel.bFOLanguageLocaleKey = valuesMap.get('Language');
                w.BusinessType = a.BusinessType__c = usrModel.businessType = valuesMap.get('BusinessType');
                w.AreaOfFocus = a.AreaofFocus__c = usrModel.areaOfFocus = valuesMap.get('AreaofFocus');
                a.PhoneNumber__c = usrModel.phoneNumber = valuesMap.get('PhoneNumber');
                w.CountryCode = a.CountryCode__c = usrModel.companyCountryIsoCode = valuesMap.get('Country');
                w.StateCode = a.StateProvinceCode__c = usrModel.companyStateProvinceIsoCode = valuesMap.get('StateProvince');
                a.Origin__c = usrModel.prmOrigin = 'Self Registration';
                a.TargetApplRequested__c = usrModel.targetApplicationRequested = 'PRTNRPRTL';
                a.CompanyTaxId__c = usrModel.taxId = valuesMap.get('CompanyTaxId');
                usrModel.userLanguage = String.isNotBlank(usrModel.bFOLanguageLocaleKey) ? (usrModel.bFOLanguageLocaleKey.contains('_') ? usrModel.bFOLanguageLocaleKey.substring(0,2) :usrModel.bFOLanguageLocaleKey): '';
                if(string.isNotBlank(a.Email__c) && !duplicateEmail.contains(a.Email__c)){
                    duplicateEmail.add(a.Email__c);
                    w.ValidEmail = Pattern.matches(EmailRegex, a.Email__c);
                }
                if(string.isNotBlank(a.FirstName__c))
                    w.ValidFirstName = true;                
                if(string.isNotBlank(a.LastName__c))
                    w.ValidLastName = true;
                if(string.isBlank(valuesMap.get('StateProvince')))
                    w.ValidState = true;
                System.debug('>>>><<<<'+w);
                
                if(valuesMap.get('PhoneType') == 'Mobile Phone') {
                    a.PhoneType__c = usrModel.phoneType = 'Mobile';
                }
                else {                    
                    a.PhoneType__c = usrModel.phoneType = 'Work';
                }
                
                a.JobFunction__c = usrModel.jobFunction = valuesMap.get('JobFunction');
                a.JobTitle__c = usrModel.jobTitle = valuesMap.get('JobTitle');
                a.ContactTaxId__c = usrModel.cTaxId = valuesMap.get('ContactTaxId');
                
                if(string.isNotBlank(valuesMap.get('T&CAccpeted?')) && valuesMap.get('T&CAccpeted?') == 'true')
                    a.TermsAndConditionsAccepted__c = usrModel.termsAndConditions = true;
                else
                    a.TermsAndConditionsAccepted__c = usrModel.termsAndConditions = false;
                
                a.CompanyName__c = usrModel.companyName = valuesMap.get('CompanyName');
                
                if(string.isNotBlank(valuesMap.get('DoNotHaveCompany')) && valuesMap.get('DoNotHaveCompany') == 'true')
                    a.DoNotHaveCompany__c = usrModel.doNotHaveCompany = true;
                else
                    a.DoNotHaveCompany__c = usrModel.doNotHaveCompany = false;
                
                if(String.isBlank(a.CompanyName__c) && !a.DoNotHaveCompany__c)
                    a.SkippedCompanyInfoStep__c = usrModel.skippedCompanyStep = true;

                if(string.isNotBlank(valuesMap.get('Headquarters')) && valuesMap.get('Headquarters') == 'true')
                    a.CompanyHeadQuarters__c = usrModel.companyHeaderQuarters = true;
                else
                    a.CompanyHeadQuarters__c = usrModel.companyHeaderQuarters = false;
                
                a.CompanyPhone__c = usrModel.companyPhone = valuesMap.get('CompanyPhoneNumber');
                a.CompanyWebsite__c = usrModel.companyWebsite = valuesMap.get('CompanyWebsite');
                a.Street__c = usrModel.companyAddress1 = valuesMap.get('Address1');
                a.AdditionalAddress__c = usrModel.companyAddress2 = valuesMap.get('Address2');
                a.City__c = usrModel.companyCity = valuesMap.get('City');
                a.ZipCode__c = usrModel.companyZipcode = valuesMap.get('ZipPostalCode');
                System.debug('>>>>a.Zipcode__c:'+a.ZipCode__c+'map:'+valuesMap.get('ZipPostalCode'));
                if(string.isNotBlank(valuesMap.get('IncludeCompanyInfoInPublicSearch')) && valuesMap.get('IncludeCompanyInfoInPublicSearch') == 'true')
                    a.IncludeCompanyInfoInSearchResults__c = usrModel.includeCompanyInfoInPublicSearch = true;
                else
                    a.IncludeCompanyInfoInSearchResults__c = usrModel.includeCompanyInfoInPublicSearch = false;
                
                System.debug('****'+a);
                //countrySet.add(w.CountryCode);
                stateSet.add(w.StateCode);
                businessTypeSet.add(w.BusinessType);
                areaOfFocusSet.add(w.AreaOfFocus);
                w.contactsToDisplay = a;
                w.userRegModel = usrModel;
                contactsGridToDisplay.add(w);
                
                if(!finalRecordsLst.containsKey(inputvalues[0]))
                    finalRecordsLst.put(inputvalues[0],w);
                
                if(!cntryMap.containskey(valuesMap.get('Country')))
                    cntryMap.put(valuesMap.get('Country'),new List<WrapperClass>{w});
                else
                    cntryMap.get(valuesMap.get('Country')).add(w);
                
                if(!cntryStateMap.containsKey(valuesMap.get('Country')+valuesMap.get('StateProvince')))
                    cntryStateMap.put(valuesMap.get('Country')+valuesMap.get('StateProvince'),new List<WrapperClass>{w});
                else
                    cntryStateMap.get(valuesMap.get('Country')+valuesMap.get('StateProvince')).add(w);
                
                if(!cntryChannelMap.containsKey(valuesMap.get('Country')+valuesMap.get('BusinessType')+valuesMap.get('AreaofFocus')))
                    cntryChannelMap.put(valuesMap.get('Country')+valuesMap.get('BusinessType')+valuesMap.get('AreaofFocus'),new List<WrapperClass>{w});
                else
                    cntryChannelMap.get(valuesMap.get('Country')+valuesMap.get('BusinessType')+valuesMap.get('AreaofFocus')).add(w);
            }
            System.debug('cntryMap>>>>'+cntryMap);
            
            
            for(Country__c country : [SELECT Id, CountryCode__c, Name FROM Country__c WHERE CountryCode__c = :cntryMap.keyset()]){
                countryLst.add(country.CountryCode__c);
                CountryNameMap.put(country.CountryCode__c,Country.Name);
                
                if(cntryMap.containsKey(country.CountryCode__c)){
                    for(WrapperClass wr : cntryMap.get(country.CountryCode__c)){
                        wr.ValidCountry = true;
                        wr.contactsToDisplay.Country__c = country.Id;
                        wr.userRegModel.companyCountry = country.Id;
                    }
                }
            }
            for(StateProvince__c state : [SELECT Id, CountryCode__c, StateProvinceCode__c, Name FROM StateProvince__c WHERE CountryCode__c = :cntryMap.keyset() AND StateProvinceCode__c = :stateSet]){
                statesLst.add(state.CountryCode__c+state.StateProvinceCode__c);
                StateNameMap.put(state.StateProvinceCode__c,state.Name);
                if(cntryStateMap.containsKey(state.CountryCode__c+state.StateProvinceCode__c)){
                    for(WrapperClass ws : cntryStateMap.get(state.CountryCode__c+state.StateProvinceCode__c)){
                        ws.ValidState = true;
                        ws.contactsToDisplay.StateProvince__c = state.Id;
                        ws.userRegModel.companyStateProvince = state.Id;
                    }
                }
            }          
            for(CountryChannels__c channels : [SELECT Id,Active__c,Channel__r.Name,Channel__c,SubChannel__c,SubChannel__r.Name,Country__c,Country__r.CountryCode__c,
                                               PRMCountry__c,PRMCountry__r.CountryPortalEnabled__c,PRMCountry__r.SupportedLanguage1__c,PRMCountry__r.SupportedLanguage2__c,
                                               PRMCountry__r.SupportedLanguage3__c,PRMCountry__r.TECH_Countries__c,PRMCountry__r.Country__r.CountryCode__c FROM CountryChannels__c 
                                               WHERE Active__c = true AND PRMCountry__r.CountryPortalEnabled__c = true AND Country__r.CountryCode__c = :cntryMap.keyset()
                                               AND Channel__r.Name IN :businessTypeSet AND SubChannel__r.Name IN :areaOfFocusSet]){
                channelLst.add(channels.Country__r.CountryCode__c+channels.Channel__r.Name+channels.SubChannel__r.Name);
                
                if(cntryChannelMap.containsKey(channels.Country__r.CountryCode__c+channels.Channel__r.Name+channels.SubChannel__r.Name)){
                    for(WrapperClass wc : cntryChannelMap.get(channels.Country__r.CountryCode__c+channels.Channel__r.Name+channels.SubChannel__r.Name)){
                        wc.ValidChannel = true;
                        wc.contactsToDisplay.UserRegistrationInformation__c = JSON.serialize(wc.userRegModel);
                        System.debug('>>>><<<<wc:'+wc);
                        System.debug('>>>><<<<cntryChannelMap:'+cntryChannelMap);
                        System.debug('>>>><<<<cntryChannelMap:'+cntryChannelMap.size());
                        System.debug('>>>><<<<<validRecordsCount:'+validRecordsCount);
                        if(wc.ValidEmail && wc.ValidFirstName && wc.validLastName && wc.ValidCountry && wc.ValidState && wc.ValidChannel){
                            System.debug('>>>><<<<<validRecordsCount:'+validRecordsCount);
                            validRecordsCount = validRecordsCount + 1;
                            System.debug('>>>><<<<<validRecordsCount:'+validRecordsCount);
                            wc.DataComplete = true;
                        }
                    }
                }
            }
        
        System.debug('>>>>validRecordsCount:'+validRecordsCount);
        System.debug('final list:'+contactsGridToDisplay);
        System.debug('final list size 1:'+contactsGridToDisplay.size());
        System.debug('final list size 2:'+contactGridToDisplay.size());
        contactGridToDisplay.addAll(contactsGridToDisplay);
        showProcess = true;
        //ShowGenerateLink = true;
        System.debug('contactGridToDisplay&&&&'+contactGridToDisplay);
        }
        else{
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid CSV template uploaded. Please upload the correct template and try again.')); 
        }
        }
        return null;
    }
        catch(Exception e){
            System.debug('>>>>'+e);
            showProcess = false;
            showGenerateLink = false;
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid CSV template uploaded. Please upload the correct template and try again.'));
            return null;
        }
    }
    public pageReference processRecords() {
        
        if(string.isBlank(prmOriginSource) || string.isBlank(originSourceInfo)){
            showGenerateLink = false;
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Enter the values for Origin Source and Origin Source Info before proceeding.')); 
            return null; 
        }
        showGenerateLink = true;
        showProcess = false;
        User u = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        if(contactGridToDisplay.size() > 0 && !contactGridToDisplay.isEmpty()){
            BatchImport__c batchImported = new BatchImport__c();
            batchImported.ImportedBy__c = u.Id;
            batchImported.ImportedDate__c = System.NOW();
            
            insert batchImported;
            batchId = batchImported.Id;
            
            contactGridToExport = new List<WrapperClass>();
            WrapperClass exports ;
            String domain='';
            
            List<FieloEE__Program__c> prgm = new List<FieloEE__Program__c>([SELECT FieloEE__SiteDomain__c FROM FieloEE__Program__c WHERE Name = 'PRM Is On']);
            if(prgm.size() > 0 && !prgm.isEmpty())
                domain = prgm[0].FieloEE__SiteDomain__c;        
            
            List<Recordtype> recordTypeLst = new List<Recordtype>([SELECT Id, Name, DeveloperName FROM RecordType WHERE DeveloperName = 'MassRegistration' LIMIT 1]);
            UIMSCompany__c contactsToBeProcessed;
            for(WrapperClass w : contactGridToDisplay){
                if(w.DataComplete == true){
                    w.contactsToDisplay.BatchImported__c = batchImported.Id;
                    w.contactsToDisplay.RecordTypeId = recordTypeLst[0].Id;
                    w.contactsToDisplay.OriginSource__c = prmOriginSource;
                    w.contactsToDisplay.OriginSourceInfo__c = originSourceInfo;
                    if(String.isNotBlank(w.contactsToDisplay.UserRegistrationInformation__c)){
                        w.userRegModel.batchImportId = batchImported.Id;
                        System.debug(w.userRegModel.batchImportId);
                        w.contactsToDisplay.UserRegistrationInformation__c = (String)JSON.serialize(w.userRegModel);
                    }

                    contactsProcessed.add(w.contactsToDisplay);
                }
                emailLst.add(w.contactsToDisplay.Email__c);
            }
            if(contactsProcessed.size() > 0 && !contactsProcessed.isEmpty())
                insert contactsProcessed;        
        
            List<UIMSCompany__c> pcontactsUploaded = new List<UIMSCompany__c>([SELECT Name,Email__c,FirstName__c,Language__c,BatchImported__c,BatchImported__r.Name,LastName__c,Country__c,BusinessType__c,AreaofFocus__c,countryCode__c FROM UIMSCompany__c WHERE RecordTypeId = :recordTypeLst[0].Id AND Email__c IN :emailLst AND BatchImported__c = :batchId]);
            
            if(pcontactsUploaded.size() > 0 && !pcontactsUploaded.isEmpty()){
                System.debug('LstContactsUploaded'+pcontactsUploaded[0]);
                for(UIMSCompany__c p : pcontactsUploaded){
                    ExportFileName = p.BatchImported__r.Name+'_'+System.Now();
                    exports = new WrapperClass();
                    exports.contactsToDisplay = p;
                    exports.regLink = string.isNotBlank(p.language__c)?('https://'+domain+'/Menu/Registration?country='+p.countryCode__c+'&language='+p.language__c+'&leadId='+p.Name+'&leadSource=prm&bt='+p.BusinessType__c+'&af='+p.AreaOfFocus__c):('https://'+domain+'/Menu/Registration?country='+p.countryCode__c+'&leadId='+p.Name+'&leadSource=prm&bt='+p.BusinessType__c+'&af='+p.AreaOfFocus__c);
                    //exports.regLink = string.isNotBlank(p.language__c)?('https://'+domain+'/Menu/Registration?country='+p.countryCode__c+'&language='+p.language__c+'&leadId='+p.Name+'&leadSource=prm'):('https://'+domain+'/Menu/Registration?country='+p.countryCode__c+'&leadId='+p.Name+'&leadSource=prm');
                    System.debug('&^&^'+exports);
                    contactGridToExport.add(exports);
                }
            }
            System.debug('????'+contactGridToExport);
            }
            PageReference reRend = new PageReference('/apex/VFP_PRMMassContactsExport');
            reRend.setRedirect(false);
            return reRend;
        //return null;
    }
        
    public class WrapperClass {
        public UIMSCompany__c contactsToDisplay{get;set;}
        public AP_UserRegModel userRegModel{get;set;}
        public Map<String,String> errorMsgs = new Map<String,String>{'Email' => 'Invalid / Duplicate Email', 'FirstName' => 'Please enter a First Name', 'LastName' => 'Please enter a Last Name', 'Country' => 'Please enter a valid Country', 'Channel' => 'Please enter a valid Channel', 'State' => 'Please enter a valid State'};
        public Boolean ValidEmail{get;set;}
        public Boolean ValidFirstName{get;set;}
        public Boolean ValidLastName{get;set;}
        public Boolean ValidCountry{get;private set;}
        public Boolean ValidState{get;private set;}
        public Boolean ValidChannel{get;private set;}
        public Boolean DataComplete{get;private set;}
        public String CountryCode{get;set;}
        public String StateCode{get;set;}
        public String BusinessType{get;set;}
        public String AreaOfFocus{get;set;}
        public String regLink{get;set;}
        
        public String Message{
            get{
                return ((!ValidEmail?errorMsgs.get('Email')+'\n':'')+(!ValidFirstName?errorMsgs.get('FirstName')+'\n':'')+(!ValidLastName?errorMsgs.get('LastName')+'\n':'')+(!ValidCountry?errorMsgs.get('Country')+'\n':'')+
                        (!ValidState?errorMsgs.get('State')+'\n':'')+(!ValidChannel?errorMsgs.get('Channel')+'\n':''));
            }
            private set;}
    
    
        public WrapperClass() {
            contactsToDisplay = new UIMSCompany__c();
            userRegModel = new AP_UserRegModel();
            ValidEmail = false;
            ValidFirstName = false;
            ValidLastName = false;
            ValidCountry = false;
            ValidState = false;
            ValidChannel = false;
            DataComplete = false;
            regLink = '';
        }
        
    } 
}