@isTest
public class Schedule_ContainmentActionAlert_TEST{

    public static testMethod void CreateData(){
    Test.startTest();
        set<ContainmentAction__c> setXA1 = new  set<ContainmentAction__c>();
        set<ID> setXAId = new set<ID>();
        Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
        User newUser2 = Utils_TestMethods.createStandardUser('TestUse2');  
           newUser2.BypassVR__c =TRUE;
           newUser2.BypassTriggers__c = 'AP1002';
           newUser2.UserRoleID = rol_ID ;
        Database.insert(newUser2);
                   
       System.runas(newUser2) {
           BusinessRiskEscalationEntity__c brEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
           insert brEntity;
           BusinessRiskEscalationEntity__c brSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
           insert brSubEntity;
           BusinessRiskEscalationEntity__c AccOrg = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation();
           insert AccOrg;
             
           List<ContainmentAction__c> lsXA = new List<ContainmentAction__c>();
             
           Problem__c prob = Utils_TestMethods.createProblem(AccOrg.Id,12);
           prob.ProblemLeader__c = newUser2.id;
           prob.ProductQualityProblem__c = false;
           prob.TECH_Symptoms__c = 'Symptom - SubSymptom'; 
           prob.RecordTypeid = Label.CLI2PAPR120014;
           prob.Sensitivity__c = 'Public';
           prob.Severity__c = 'Safety related';
           Database.insert(prob); 
             
           ContainmentAction__c XA1 =  Utils_TestMethods.createContainmentAction(prob.Id,AccOrg.Id);
           XA1.ActualUserLastmodifiedDate__c = System.now() - 30; 
           XA1.TECH_ReminderNoUpdateDate__c = null;
           lsXA.add(XA1);
           setXAId.add(XA1.Id);
           
           ContainmentAction__c XA2 =  Utils_TestMethods.createContainmentAction(prob.Id,AccOrg.Id);
           XA2.ActualUserLastmodifiedDate__c = system.now() - 30; 
           XA2.TECH_ReminderNoUpdateDate__c = System.now() - 7;
           lsXA.add(XA2);
           setXAId.add(XA2.Id);
           
           Database.insert(lsXA);

           System.debug('--------------->'+lsXA);

        }    
        
        Schedule_ContainmentActionAlert batch_XA = new Schedule_ContainmentActionAlert(); 
        if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5)
            Database.executebatch(batch_XA,1000);
        Test.stopTest();
    }
    
    static testMethod void myUnitTest_schedule() {
        Test.startTest();
            String CRON_EXP = '0 0 0 1 1 ? 2025';  
            String jobId = System.schedule('Schduel Schedule_ContainmentActionAlert', CRON_EXP, new Schedule_ContainmentActionAlert() );
        Test.stopTest();
    }
}