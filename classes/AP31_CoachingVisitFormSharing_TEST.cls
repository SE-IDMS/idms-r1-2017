/*
    Author          : Ramesh Rajasekaran - Global Delivery Team
    Date Created    : 11/10/2011
    Description     : Test class for AP31_CoachingVisitFormSharing  class and related Coaching Visit Form triggers.  
                        
*/
@isTest
private class AP31_CoachingVisitFormSharing_TEST {

    static testMethod void testCoachingVisitFormSharing() {
    
        User tstUser = Utils_TestMethods.createStandardUser('tst1');
        Database.Insert(tstUser);
        
        Account acc = Utils_TestMethods.createAccount();
        Database.Insert(acc);
        
        SFE_CoachingVisitForm__c cvf = Utils_TestMethods.createCoachingVisitForm(tstUser.Id,acc.Id);
        Database.Insert(cvf);
        
        User tstUser1 = Utils_TestMethods.createStandardUser('tst2');
        Database.Insert(tstUser1);
        
        cvf.SalesRep__c = tstUser.id;
        Database.update(cvf); 
        
        cvf.SalesRep__c = tstUser1.id;
        Database.update(cvf);        
    }
}