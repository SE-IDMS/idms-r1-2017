@isTest
private class IPODeploymentNetworkTriggers_Test {
    static testMethod void testIPODeploymentNetworkTriggers() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPO2');
        System.runAs(runAsUser){
            IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
            IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp;
            IPO_Strategic_Deployment_Network__c C=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Functions', Entity__c='GM');
            insert C;
            IPO_Strategic_Deployment_Network__c C1=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Global Business', Entity__c='ITB');
            insert C1;
            IPO_Strategic_Deployment_Network__c C2=new IPO_Strategic_Deployment_Network__c(IPO_Strategic_Program__c=IPOp.id,Scope__c='Operation Regions', Entity__c='NA');
            insert C2;
            C.Entity__c='HR';
            update C;
            delete C1;
            }
            }
            }