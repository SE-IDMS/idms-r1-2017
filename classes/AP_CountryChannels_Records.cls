public Class AP_CountryChannels_Records{
    Public Static Map<String,CS_PRM_UserRegistration__c> userRegCS = CS_PRM_UserRegistration__c.getAll();
    Public Static Map<String,CSPRMEmailNotification__c> emailConfigCS = CSPRMEmailNotification__c.getAll();
    public Static Map<Id,Map<Id,Set<Id>>> getAllChannels(){
        List<CountryChannels__c> cChannels = [Select Id,Name,Channel__c,SubChannel__c,Active__c,Country__c From CountryChannels__c Where Active__c = True And Country__c != Null Limit 50000];
        Map<Id,Map<Id,Set<Id>>> conChMap = new Map<Id,Map<Id,Set<Id>>>();
        Map<Id,Set<Id>> conNoSubMap = new Map<Id,Set<Id>>();
        for(CountryChannels__c cCh:cChannels){
            
            if(conChMap.containsKey(cCh.Country__c)){
                if(conChMap.get(cCh.Country__c).containsKey(cCh.Channel__c))
                    conChMap.get(cCh.Country__c).get(cCh.Channel__c).add(cCh.SubChannel__c);
                else 
                    conChMap.get(cCh.Country__c).put(cCh.Channel__c,new Set<Id>{cCh.SubChannel__c});
            }
            else{
                Map<Id,Set<Id>> conSubChaCountry = New Map<Id,Set<Id>>();
                if(cCh.Channel__c != Null){
                    conSubChaCountry.put(cCh.Channel__c, new Set<Id>{cCh.SubChannel__c});
                    conChMap.put(cCh.Country__c,conSubChaCountry);
                }
            }
            System.debug('The total channels are '+cCh.Country__c+' '+cCh.Channel__c+' '+cCh.SubChannel__c);
        }
        return conChMap;
    }
    
    public static void createCountryChannelDefaultUserRegFields(list<CountryChannels__c> lstCountryChannels){
        
        List<CountryChannelUserRegistration__c> lstUserRegFlds = new list<CountryChannelUserRegistration__c>();
        CountryChannelUserRegistration__c aFld;
        Map<String, Map<String, Schema.SObjectField>> sObjectFieldsMap = new Map<String, Map<String, Schema.SObjectField>>();
        Map<String,String> fieldLabelsMap = new Map<String,String>();
        
        // Do a globel describe, process the custom setting "CS_PRM_UserRegistration__c" to do a getDescribe().fields.getMap() for all the objects in the CS and store them in a Map<ObjectAPI, Map<Fieldapi, Schema.SObjectField>> 
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        for(CS_PRM_UserRegistration__c cSValues:userRegCS.Values()){
            if(sObjectFieldsMap.isEmpty()){
                Map<String,Schema.SObjectField> FielMap = schemaMap.get(cSValues.MappedObject__c).getDescribe().fields.getMap();
                sObjectFieldsMap.put(cSValues.MappedObject__c, FielMap);
            }
            else if(!sObjectFieldsMap.containsKey(cSValues.MappedObject__c)){
                Map<String,Schema.SObjectField> FielMap = schemaMap.get(cSValues.MappedObject__c).getDescribe().fields.getMap();
                sObjectFieldsMap.put(cSValues.MappedObject__c, FielMap);
            }
        }
        
        // Process the custom setting "CS_PRM_UserRegistration__c" to do a getDescribe().getLabel() and store the values in a Map<FieldAPI, FieldLabel>
        for(CS_PRM_UserRegistration__c cSValues:userRegCS.Values()){
            fieldLabelsMap.put(cSValues.MappedField__c, sObjectFieldsMap.get(cSValues.MappedObject__c).get(cSValues.MappedField__c).getDescribe().getLabel());
        }
        
        for(CountryChannels__c aCntryChanel:lstCountryChannels){
            if(!aCntryChanel.TechIsCloned__c){
                for(CS_PRM_UserRegistration__c cSValues:userRegCS.Values()){
                    aFld = new CountryChannelUserRegistration__c();
                    aFld.CountryChannels__c = aCntryChanel.id;
                    aFld.Mandatory__c = cSValues.AlwaysMandatory__c;
                    aFld.MandatoryOnProfile__c = cSValues.MandatoryOnProfile__c;
                    aFld.UserRegFieldName__c = cSValues.Name;
                    aFld.TECH_UserRegFieldName__c = fieldLabelsMap.get(cSValues.MappedField__c);
                    if(aFld.UserRegFieldName__c == 'companyCurrency')
                    aFld.DefaultValue__c = aCntryChanel.CurrencyIsoCode;
                    lstUserRegFlds.add(aFld);
                }
            }
        }
        try{
            System.debug('** Fields being inserted:' + lstUserRegFlds);
            if(!lstUserRegFlds.isEmpty())
            insert lstUserRegFlds;  
        
        }
        catch(DMLException e)
        {
            for (Integer j = 0; j < e.getNumDml(); j++) 
            {   
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(j),'')); 
                System.debug(e.getDmlMessage(j)); 
            } 
            
                                  
        }
        
    
    
    }
    
    public static void createDefaultEmailConfigFields(list<CountryChannels__c> lstCountryChannels){
    List<PRMConfigureEmailNotifications__c> configEmailUpdateList = new List<PRMConfigureEmailNotifications__c>();
    for(CountryChannels__c aCntryChanel :lstCountryChannels){
             for(CSPRMEmailNotification__c cSValues:emailConfigCS.Values()){
                 PRMConfigureEmailNotifications__c configEmail = new PRMConfigureEmailNotifications__c(
                 CountryChannel__c = aCntryChanel.Id,
                 EmailName__c = cSValues.Name,
                 IsNotificationEnable__c = cSValues.Default__c,
                 AppliedTo__c = cSValues.AppliedTo__c
                 );
                 configEmailUpdateList.add(configEmail);
             }
    }
    try{
        
            if(!configEmailUpdateList.isEmpty())
            insert configEmailUpdateList;  
        
        }
        catch(DMLException e)
        {
            for (Integer j = 0; j < e.getNumDml(); j++) 
            {   
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(j),'')); 
                System.debug(e.getDmlMessage(j)); 
            }                     
        }
    
    }
    
    public Static Schema.DescribeFieldResult getFieldResults(String fieldApiName){
        if(userRegCS.containsKey(fieldApiName)){
            String sObjectName =  userRegCS.get(fieldApiName).MappedObject__c;
            String fieldName = userRegCS.get(fieldApiName).MappedField__c;
            Schema.DescribeSobjectResult results = Schema.getGlobalDescribe().get(sObjectName).getDescribe();
            Schema.DescribeFieldResult fieldResults = results.fields.getMap().get(fieldName).getDescribe();
            return fieldResults;
        }
        else return Null;
          
    }
    public Static Void updateFieldsOnChannelChange(Map<Id,CountryChannels__c> conChanMap){
        List<CountryChannelUserRegistration__c> conChUsReg = [SELECT Id, Name, UserRegFieldName__c,DefaultValue__c,CountryChannels__c  FROM CountryChannelUserRegistration__c WHERE CountryChannels__c IN :conChanMap.keySet()];
        System.debug('The size of the userreg' +conChUsReg.Size());
        List<CountryChannelUserRegistration__c> updateConChList = New List<CountryChannelUserRegistration__c>();
        for(CountryChannelUserRegistration__c conch:conChUsReg){
            
            if(conch.UserRegFieldName__c == 'businessType')
            conch.DefaultValue__c = conChanMap.get(conch.CountryChannels__c).ChannelCode__c;
            if(conch.UserRegFieldName__c == 'areaOfFocus')
            conch.DefaultValue__c = conChanMap.get(conch.CountryChannels__c).SubChannelCode__c;
            if(conch.UserRegFieldName__c == 'companyCountry')
            conch.DefaultValue__c = conChanMap.get(conch.CountryChannels__c).Country__c;
            
            System.debug('The Userreg and Default Values are' +conch.UserRegFieldName__c+' '+conch.DefaultValue__c);
            updateConChList.add(conch);
        }
        
        try{
            if(!updateConChList.isEmpty())
            update updateConChList;
        }
        catch(DMLException e){
            for (Integer j = 0; j < e.getNumDml(); j++) {   
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(j),'')); 
                System.debug(e.getDmlMessage(j)); 
            }
        }     
    }
    
    @InvocableMethod(label='Get Configured Email Notifications' description= 'Return List of Configured Email Notifications to this Contact.')
    public Static List<String> GetConfiguredEmailNotifications(List<Contact> Contactlist){
        Contact thisContact = Contactlist[0];
        List<PRMConfigureEmailNotifications__c> emailNotifyList = [SELECT CountryChannel__c,EmailName__c,Id,IsNotificationEnable__c,
                                                                    Name,TechChannelName__c,TechCountry__c,TechSubChannelName__c FROM 
                                                                    PRMConfigureEmailNotifications__c WHERE TechChannelName__c = :thisContact.PRMCustomerClassificationLevel1__c 
                                                                    AND TechSubChannelName__c = :thisContact.PRMCustomerClassificationLevel2__c AND IsNotificationEnable__c = True 
                                                                    AND TechCountry__c = :thisContact.PRMCountry__c AND IsActiveChannel__c = true];
        List<String> eamilAlertNameList = new list<String>();
        
        for(PRMConfigureEmailNotifications__c emailNotify:emailNotifyList){
            eamilAlertNameList.add(emailNotify.EmailName__c);
        }
        return eamilAlertNameList;
    }
}