//Test class for VFC_ProjectBarcode - MAR 2016 release
@isTest
public class VFC_ProjectBarcode_TEST {
     static testMethod void testMethodVFC_ProjectBarcode() {
  Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        Database.insert(acc); 
        
        OPP_Project__c prj =Utils_TestMethods.createMasterProject(acc.Id,country.Id);
        insert prj;
        prj.CurrencyIsoCode = 'INR';
        update prj;  
        
        Opportunity opp1 = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp1.Project__c=prj.Id;
        Insert opp1;
        
        Opportunity opp2 = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp2.ParentOpportunity__c=opp1.Id;
        Insert opp2;
        
        Opportunity opp3 = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp3.ParentOpportunity__c=opp2.Id;
        Insert opp3;
        
        test.startTest();
        
         CS_CurrencyDecimalSeperator__c cd= new CS_CurrencyDecimalSeperator__c(GroupingSeperator__c='.',DecimalSeperator__c=',',Name=UserInfo.getLocale());
         insert cd;
        
        Opportunity opp4 = Utils_TestMethods.createOpenOpportunity(acc.Id);
        opp4.Project__c=prj.Id;
        Insert opp4;
   
        PageReference myVfPage = Page.VFP_CalculateProjectAmt;
        Test.setCurrentPageReference(myVfPage);
        ApexPages.currentPage().getParameters().put('id',prj.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(prj);
        VFC_CalculateProjectAmt extController = new VFC_CalculateProjectAmt(sc);
        extController.prj=prj;
        extController.calculateProjectAmount();
        test.stopTest();
        VFC_ProjectBarcode.getOppData(prj.Id);
        VFC_ProjectBarcode.getProjectData(prj.Id); 
      }

}