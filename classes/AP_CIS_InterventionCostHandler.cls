public class  AP_CIS_InterventionCostHandler{
     
    
    public static void beforeCISInterventionCostInsert(List<CIS_InterventionCosts__c> isrInterventionCostList){
        //Getting All the Related ISR Ids and dumping it to a set
        Set<Id> isrIds = new Set<Id>();
        Set<String> costTypeSet = new Set<String>();
        for(CIS_InterventionCosts__c isrInterventionInstance:isrInterventionCostList){
            isrIds.add(isrInterventionInstance.CIS_ISR__c);
            costTypeSet.add(isrInterventionInstance.CIS_CostType__c);      
        }
         
        //Query All related ISR End User Country and Field of actvity and store it in am map 
        Map<Id,CIS_ISR__c> mapRelatedIsrIdAndInfo = new Map<Id,CIS_ISR__c>();
        Set<Id> isrEUCountryIds = new Set<Id>();
        Set<String> fieldOfActivitySet = new Set<String>();
        for(CIS_ISR__c isrInstance:[select id,CIS_EndUserCountry__c,CIS_FieldOfActivity__c from CIS_ISR__c where id in :isrIds]){
            mapRelatedIsrIdAndInfo.put(isrInstance.Id,isrInstance);
            isrEUCountryIds.add(isrInstance.CIS_EndUserCountry__c); 
            fieldOfActivitySet.add(isrInstance.CIS_FieldOfActivity__c);    
        }
        
        //Query relevant country list price records according to Isr End User country,Field of activity and Intervention cost cost type
        Boolean isEntered = false;
        for(CIS_CountryListPrice__c countryListPriceInstance:[select id ,CIS_CostType__c,CurrencyIsoCode,CIS_Country__c,CIS_FieldOfActivity__c,CIS_ListPrice__c from CIS_CountryListPrice__c where CIS_Country__c in:isrEUCountryIds and CIS_FieldOfActivity__c in:fieldOfActivitySet and CIS_CostType__c in:costTypeSet ]){ 
            isEntered = true;
            for(CIS_InterventionCosts__c isrInterventionInstance:isrInterventionCostList){ 
               isrInterventionInstance.CIS_CostPerUnit__c = null;
               if((mapRelatedIsrIdAndInfo.get(isrInterventionInstance.CIS_ISR__c).CIS_EndUserCountry__c == countryListPriceInstance.CIS_Country__c) && (mapRelatedIsrIdAndInfo.get(isrInterventionInstance.CIS_ISR__c).CIS_FieldOfActivity__c == countryListPriceInstance.CIS_FieldOfActivity__c)&& (isrInterventionInstance.CIS_CostType__c ==countryListPriceInstance.CIS_CostType__c )){
                    isrInterventionInstance.CIS_CostPerUnit__c = countryListPriceInstance.CIS_ListPrice__c; 
                    isrInterventionInstance.CurrencyIsoCode = countryListPriceInstance.CurrencyIsoCode;
               }
           }    
        }
        if(isEntered == false){
             for(CIS_InterventionCosts__c isrInterventionInstance:isrInterventionCostList){ 
                //isrInterventionInstance.CIS_CostPerUnit__c = null;
             }
        }
        
       

  }
  
  
}