/*
    Author          : Ramakrishna Singara (Schneider Electric) 
    Date Created    : 11/07/2012
    Description     : Class utilised by triggers ProgramCostForecastTriggerAfterUpdate .
                        Custom field Program costs forecast Data Auto populated in the Program.
*/

public with sharing class PROG_CostForecastTriggers {

    public static void ProgCostForecastAfterUpdate(List<ProgramCostsForecasts__c> ProgCostForecastListToProcessCONDITION1){
        
        list<ProgramCostsForecasts__c> progforecastList = new list<ProgramCostsForecasts__c>();
        for(ProgramCostsForecasts__c progCosts : ProgCostForecastListToProcessCONDITION1){
            if(progCosts.Active__c ==true){
                progforecastList.add(progCosts);
            }
        }
        PROG_CostForecastUtils.progCostsForecastAfterUpdate(progforecastList);
        
    }
    public static void ProgCostForecastAfterInsert(List<ProgramCostsForecasts__c> ProgCostForecastListToProcessCONDITION1){
        list<ProgramCostsForecasts__c> progforecastList = new list<ProgramCostsForecasts__c>();
        for(ProgramCostsForecasts__c progCosts : ProgCostForecastListToProcessCONDITION1){
            if(progCosts.Active__c ==true){
                progforecastList.add(progCosts);
            }
        }
        PROG_CostForecastUtils.progCostsForecastAfterInsert(progforecastList);
    }
    //Modified by Srikant Joshi January Release 2013.
    /* 
    public static void ProgCostForecastAfterDelete(List<ProgramCostsForecasts__c> ProgCostForecastListToProcessCONDITION1){
        list<ProgramCostsForecasts__c> progforecastList = new list<ProgramCostsForecasts__c>();
        for(ProgramCostsForecasts__c progCosts : ProgCostForecastListToProcessCONDITION1){
            if(progCosts.Active__c ==true){
                progforecastList.add(progCosts);
            }
        }
        PROG_CostForecastUtils.progCostsForecastAfterDelete(progforecastList);
    }
    */
    public static void ProgCostForecastAfterDelete(List<ProgramCostsForecasts__c> ProgCostForecastListToProcessCONDITION1)
    {
        System.Debug('****** ProgCostForecastAfterDelete Start ****'); 
        List<ProgramCostsForecasts__c> progListToProcessCONDITION1 = new List<ProgramCostsForecasts__c>();
        
        for (ProgramCostsForecasts__c oldBud:ProgCostForecastListToProcessCONDITION1)
        {                                  
            if(oldBud.Active__c == true)
            {
                progListToProcessCONDITION1.add(oldBud);                
            }
        }
        
        if(ProgCostForecastListToProcessCONDITION1.size()>0)
            PROG_CostForecastUtils.updateProgramFieldsonDelete(progListToProcessCONDITION1);            
            
        System.Debug('****** ProgCostForecastAfterDelete End ****');         
        
    }
    
    
    
    public static void ProgCostForecastbeforeupdate(Map<Id,ProgramCostsForecasts__c > newMap,Map<Id,ProgramCostsForecasts__c > oldMap)
    {
        System.Debug('****** ProgCostForecastbeforeUpdate Start ****'); 
        List<ProgramCostsForecasts__c > budgetListToProcessCONDITION1 = new List<ProgramCostsForecasts__c >();
        List<ProgramCostsForecasts__c > budgetListActiveToProcessCONDITION1 = new List<ProgramCostsForecasts__c >();
        
        for (ID budId:newMap.keyset())
        {
            ProgramCostsForecasts__c  newBud = newMap.get(budId);
            ProgramCostsForecasts__c  oldBud = oldMap.get(budId);
            
            if(newBud.Active__c == true && oldbud.Active__c == false)
            {
                budgetListToProcessCONDITION1.add(newBud);                
            }
            if(newBud.Active__c == true)
            {
                budgetListActiveToProcessCONDITION1.add(newBud);//// Added by Srikant Joshi on January Release 2013.            
            }
        }
        
        
        if (budgetListToProcessCONDITION1.size()>0) 
            PROG_CostForecastUtils.deactivateActiveProgramCostForecast(budgetListToProcessCONDITION1,true);
        
        if (budgetListActiveToProcessCONDITION1.size()>0)
            PROG_CostForecastUtils.updateProgramFieldsonInsertUpdate(budgetListActiveToProcessCONDITION1); //// Added by Srikant Joshi on January Release 2013.
       
        System.Debug('****** ProgCostForecastbeforeUpdate End ****'); 
             
    }
    
    public static void ProgCostForecastbeforeInsert(List<ProgramCostsForecasts__c> newBudgets)
    {
        System.Debug('****** ProgCostForecastbeforeInsert Start ****'); 
        List<ProgramCostsForecasts__c> budgetListToProcessCONDITION1 = new List<ProgramCostsForecasts__c>();
        
        for (ProgramCostsForecasts__c newBud:newBudgets)
        {                                  
            if(newBud.Active__c == true)
            {
                budgetListToProcessCONDITION1.add(newBud);                
            }
        }
        
        
        if (budgetListToProcessCONDITION1.size()>0) 
            PROG_CostForecastUtils.deactivateActiveProgramCostForecast(budgetListToProcessCONDITION1,false);
            PROG_CostForecastUtils.updateProgramFieldsonInsertUpdate(budgetListToProcessCONDITION1);// Added by Srikant Joshi on January Release 2013.
         
            
        System.Debug('****** ProgCostForecastbeforeInsert End ****');         
        
    }
    
    
    
}