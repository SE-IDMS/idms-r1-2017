/*
Author : Shruti Karn
Date : 12 April 2012
Descritpion : To create a Task for SME Team members having 'Member Activated' checked.
*/
public class AP51_CreateTaskforSME {
    
//To insert the Task for created SME Team Member
    //public static void createTask(list<RIT_SMETeam__c> lstSMETeamMembers)
    public static void createTask(map<Id,RIT_SMETeam__c> mapSMETeamMembers)
    {
             
        list<RIT_SMETeam__c> lstSMETeamMembers = new list<RIT_SMETeam__c>();
        if(!mapSMETeamMembers.isEmpty())
             lstSMETeamMembers = [Select TeamMember__c, QuoteLink__c, QuoteLink__r.QuoteSubmissionDate__c, QuoteLink__r.OpportunityName__r.Name from RIT_SMETeam__c where id in :mapSMETeamMembers.keySet() limit 10000];
        if(!lstSMETeamMembers.isempty())
        {
            list<Task> lstTask = new list<task>();
            for(RIT_SMETeam__c SMETeamMem : lstSMETeamMembers)
            {
                Task newTask = new Task();
                newTask.Subject = SMETeamMem.QuoteLink__r.OpportunityName__r.Name;
                newTask.ActivityDate = SMETeamMem.QuoteLink__r.QuoteSubmissionDate__c;
                newTask.OwnerId = SMETeamMem.TeamMember__c; 
                newTask.WhatId = SMETeamMem.QuoteLink__c;
                lstTask.add(newTask);
            }
            
            if(!lstTask.isEmpty())
            {
                Database.SaveResult[] lsr = Database.insert(lstTask,false);
                // Iterate through the Save Results 
                for(Database.SaveResult sr:lsr){
                   if(!sr.isSuccess())
                      Database.Error err = sr.getErrors()[0];
                }
            }
        }
    }
    

public static void updateTask(map<Id,RIT_SMETeam__c> mapNewSME , map<Id,RIT_SMETeam__c> mapOldSME)
{
    list<Task> lstTasktoUpdate = new list<Task>(); // to hold the list of tasks whose owner needs to updated with new Team Member
    list<Task> lstTasktoDelete= new list<Task>(); // to hold the list of tasks needs to be deleted for the Team members for whom 'Member Activated' is uncheckd.
    //list<RIT_SMETeam__c> lstSMETeamtoInsert = new list<RIT_SMETeam__c> ();
    map<Id,RIT_SMETeam__c> mapSMETeamtoInsert = new map<Id,RIT_SMETeam__c> ();// to hold the map of SMEs for which new Tasks should be inserted
    map<Id, ID> mapSMETeamMemId = new map<Id, Id> (); // to holds the SME ID and the SME Team Member ID
    map<Id, ID> mapQLKTeamMemId = new map<Id, Id> (); // holds the QLK ID and the Team Member ID
    list<ID> lstSMETeamtoUpdate = new list<ID> (); // list of SME Team members for whom the Task should be updated
    list<ID> lstQLK= new list<ID> (); // to get the list QLK Ids for querying the Task
    map<Id,Task> mapOwnerIdTask = new map<Id,Task>(); // to hold the Task Owner id and the Task details
    
    for(Id SMEId : mapNewSME.keySet())
    {
        // check if the SME Team memeber is updated and Member Activated is true
        if(mapNewSME.get(SMEId).MemberActivated__c && mapNewSME.get(SMEId).TeamMember__c != mapOldSME.get(SMEId).TeamMember__c)
        {
            mapSMETeamMemId.put(SMEId,mapOldSME.get(SMEId).TeamMember__c);
            lstQLK.add(mapNewSME.get(SMEId).QuoteLink__c);
        }
        // check if Member activated has been updated to true
        else if(mapNewSME.get(SMEId).MemberActivated__c != mapOldSME.get(SMEId).MemberActivated__c && mapNewSME.get(SMEId).MemberActivated__c)
        {
            //lstSMETeamtoInsert.add(mapNewSME.get(SMEId));
            mapSMETeamtoInsert.put(SMEId , mapNewSME.get(SMEId));
        }
        //check if Team member is updated and theMember Activated has been updated to false.
        if(mapNewSME.get(SMEId).MemberActivated__c != mapOldSME.get(SMEId).MemberActivated__c && mapNewSME.get(SMEId).MemberActivated__c == false)
        {
            mapQLKTeamMemId.put(mapNewSME.get(SMEId).QuoteLink__c,mapOldSME.get(SMEId).TeamMember__c);
            
        }
    }
    
    if(!mapSMETeamtoInsert.isEmpty())
        createTask(mapSMETeamtoInsert);// calls createTask method to insert new Tasks for the activated members
    if(!mapSMETeamMemId.isEmpty())
    {
        //query the list of tasks for the updated SME Team Member and the parent QLK ID
        lstTasktoUpdate =  [Select id , OwnerID from Task where WhatId in : lstQLK and ownerID in :mapSMETeamMemId.values() limit 50000];
        for(Task t : lstTasktoUpdate)
        {
            mapOwnerIdTask.put(t.OwnerId , t);
        }
        
        for(ID SMEId : mapSMETeamMemId.keySet())
        {
            mapOwnerIdTask.get(mapSMETeamMemId.get(SMEId)).ownerID = mapNewSME.get(SMEId).TeamMember__c;
        }
        update mapOwnerIdTask.values();
        
    }
    if(!mapQLKTeamMemId.isEmpty())
    {
        lstTasktoDelete = [Select id , OwnerID from Task where WhatId in : mapQLKTeamMemId.keySet() and ownerID in :mapQLKTeamMemId.values() limit 50000];
        delete lstTasktoDelete;
    }
    
}




}