/*
Author:Siddharth
Purpose:Test Methods for the class VFC_SubmitSPAforApproval
Methods:    Constructor,
            Private void setApprovers (List<SPALineItem> lstSPALineItems)
            Private boolean checkMinimumDealSizeMet ()
            private integer getThresholdLevelForProductLine (List<SPALineItem>lstSPALineItems)
            private void  SetPricingDeskMembers ()
            private void shareSPARecord()
            public List<User> lstApproversForSharing
            

*/
@isTest
private class VFC_SubmitSPAforApproval_TEST
{ 
   static testMethod void SubmitSPAforApprovalmethod() {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        
        User l2user=Utils_TestMethods.createStandardUser('l2');
        insert l2user;
        User l1user=Utils_TestMethods.createStandardUser('l1');        
        l1user.ManagerId=l2user.id;
        insert l1user;
        
        User runasthisuser=Utils_TestMethods.createStandardUser('sp');
        runasthisuser.ManagerId=l1user.Id;
        insert runasthisuser;        
        
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,runasthisuser.id);
        insert param;
        
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        List<SPAQuotaQuarter__c> quotaquarters=Utils_SPA.createQuotaQuarters(quota.Id);
        insert quotaquarters;
        
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        SPAThresholdDiscount__c thresholddiscount1=Utils_SPA.createThresholdDiscount('HK_SAP','Final Multiplier');
        insert thresholddiscount1;

        //Additional Discount
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.Channel__c=acc.id;
        sparequest.salesperson__c=runasthisuser.id;
        insert sparequest;
        SPARequestLineItem__c lineitem=Utils_SPA.createSPARequestLineItem(sparequest.id,'Discount');
        insert lineitem;
        SPARequestLineItem__c lineitem1=Utils_SPA.createSPARequestLineItem(sparequest.id,'Multiplier');
        insert lineitem1;
                 
        Test.startTest();
        insert Utils_SPA.createPricingDeskMembers(sparequest.BackOfficeSystemID__c,lineitem.LocalPricingGroup__c);
        //just to make sure all the roll ups have values        
        sparequest=[select SPAQuotaQuarter__r.Quarter__c,SPAQuotaQuarter__r.SPAQuota__c,SPAQuotaQuarter__r.CurrencyIsoCode,SPAQuotaQuarter__r.QuotaRemaining__c,Account__c,ApprovalStatus__c,Approver1__c,Approver2__c,Approver3__c,BackOfficeCustomerNumber__c,BackOfficeSystemID__c,CreatedById,CreatedDate,CurrencyIsoCode,CustomerPricingGroup__c,DealSize__c,EndDate__c,ExceptionInformations__c,Id,Name,NetDiscountAmount__c,Opportunity__c,OwnerId,QuotaExceeded__c,RecordTypeId,RequiredApprovalLevel__c,Salesperson__c,SPAQuotaQuarter__c,SPASalesGroup__c,StartDate__c,(select ApprovalComments__c,Business__c,CommercialReference__c,CurrencyIsoCode,ListPriceLineAmount__c,ListPrice__c,LocalPricingGroup__c,NetLineAmount__c,ProductLineManager__c,RecommendationStatus__c,AdditionalDiscount__c,FinalMultiplier__c,NetPrice__c,RequestedType__c,RequestedValue__c,RequiredApprovalLevel__c,StandardDiscount__c,StandardMultiplier__c,StandardPriceLineAmount__c,StandardPrice__c,TargetQuantity__c from SPARequestLineItems__r where id in (:lineitem.id,:lineitem1.id) ) from SPARequest__c where id=:sparequest.id];
        System.debug('request'+sparequest);
        ApexPages.StandardController controller=new ApexPages.StandardController(sparequest);
        VFC_SubmitSPAforApproval controllerinstance=new VFC_SubmitSPAforApproval(controller);    
        controllerinstance.start();  
        controllerinstance.proceedwitherror();
        //controllerinstance.bypassQuota=true;
        controllerinstance.start();
        
        sparequest.ApprovalStatus__c='Approved';
        Update  sparequest;
        controllerinstance.start(); 

        Test.stopTest();       
    }
     static testMethod void SubmitSPAforApprovalmethod2() {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        
        User l2user=Utils_TestMethods.createStandardUser('l2');
        insert l2user;
        User l1user=Utils_TestMethods.createStandardUser('l1');        
        l1user.ManagerId=l2user.id;
        insert l1user;
        
        User runasthisuser=Utils_TestMethods.createStandardUser('sp');
        runasthisuser.ManagerId=l1user.Id;
        insert runasthisuser;        
        
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,runasthisuser.id);
        insert param;
        
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        List<SPAQuotaQuarter__c> quotaquarters=Utils_SPA.createQuotaQuarters(quota.Id);
        insert quotaquarters;
        
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
       
        SPAThresholdDiscount__c thresholddiscount2=Utils_SPA.createThresholdDiscount('HK_SAP',System.Label.CLOCT14SLS14);
        insert thresholddiscount2;
        Test.startTest();
        SPARequest__c sparequest1=Utils_SPA.createSPARequest(System.Label.CLOCT14SLS13,opp.id);
        sparequest1.salesperson__c=runasthisuser.id;
        insert sparequest1;
        SPARequestLineItem__c lineitem2=Utils_SPA.createSPARequestLineItem(sparequest1.id,System.Label.CLOCT14SLS13);
        lineitem2.TargetQuantity__c=3;
        lineitem2.Compensation__c=15;
        lineitem2.ListPrice__c =1000;
        insert lineitem2;
        insert Utils_SPA.createPricingDeskMembers(sparequest1.BackOfficeSystemID__c,lineitem2.LocalPricingGroup__c);
        //just to make sure all the roll ups have values  
        sparequest1=[select RequestedType__c,SPAQuotaQuarter__r.Quarter__c,SPAQuotaQuarter__r.SPAQuota__c,SPAQuotaQuarter__r.CurrencyIsoCode,SPAQuotaQuarter__r.QuotaRemaining__c,Account__c,ApprovalStatus__c,Approver1__c,Approver2__c,Approver3__c,BackOfficeCustomerNumber__c,BackOfficeSystemID__c,CreatedById,CreatedDate,CurrencyIsoCode,CustomerPricingGroup__c,DealSize__c,EndDate__c,ExceptionInformations__c,Id,Name,NetDiscountAmount__c,Opportunity__c,OwnerId,QuotaExceeded__c,RecordTypeId,RequiredApprovalLevel__c,Salesperson__c,SPAQuotaQuarter__c,SPASalesGroup__c,StartDate__c,(select ApprovalComments__c,Business__c,CommercialReference__c,CurrencyIsoCode,ListPriceLineAmount__c,ListPrice__c,LocalPricingGroup__c,NetLineAmount__c,ProductLineManager__c,RecommendationStatus__c,AdditionalDiscount__c,FinalMultiplier__c,NetPrice__c,RequestedType__c,RequestedValue__c,RequiredApprovalLevel__c,StandardDiscount__c,StandardMultiplier__c,StandardPriceLineAmount__c,StandardPrice__c,TargetQuantity__c,Compensation__c from SPARequestLineItems__r where id =:lineitem2.id) from SPARequest__c where id=:sparequest1.id];
        ApexPages.StandardController controller1=new ApexPages.StandardController(sparequest1);
        VFC_SubmitSPAforApproval controllerinstance1=new VFC_SubmitSPAforApproval(controller1); 
        //controllerinstance1.start();
        controllerinstance1.proceedwitherror();
        controllerinstance1.start();
         Test.stopTest();
    }
}