public with sharing class FieloPRM_AP_MenuTriggers{
  

    public static void emptyMenuFilter(){

        List<FieloEE__Menu__c> triggerNew = trigger.new;
        set<String> setMenuFiltersMenu = new set<String>();
        
        for(FieloEE__Menu__c menu: triggerNew){
            if(menu.F_PRM_MenuFilter__c != null){
                setMenuFiltersMenu.add(menu.F_PRM_MenuFilter__c);
            }
        }
      
        list<FieloEE__Menu__c> listMenus = [SELECT id, F_PRM_MenuFilter__c FROM  FieloEE__Menu__c WHERE F_PRM_MenuFilter__c IN : setMenuFiltersMenu ];
        map<String,FieloEE__Menu__c> mapMenuFilterToMenu = new map<String,FieloEE__Menu__c>();
        
        if(listMenus .size() > 0){
            for(FieloEE__Menu__c menu: listMenus){
                if(menu.F_PRM_MenuFilter__c != null){
                    mapMenuFilterToMenu.put(menu.F_PRM_MenuFilter__c,menu);
                }
            }
        }
        
        for(FieloEE__Menu__c menu: triggerNew){
            if(menu.F_PRM_MenuFilter__c != null){
                if(mapMenuFilterToMenu.containsKey(menu.F_PRM_MenuFilter__c)){
                    menu.F_PRM_MenuFilter__c = '';
                }
            }
        }

    }
    
    public static void avoidDuplicateRule(){
    
        if(FieloPRM_CreateMenuChannelCountryExt.isClone == true || FieloPRM_VFC_RewardsCloneExtension.isClone == true){
        
            List<FieloEE__Menu__c> triggerNew = trigger.new;
            
            Integer auxNumber = 0;
            
            Set<String> setExternalNames = new Set<String>();

            for(FieloEE__Menu__c menu: triggerNew){               
                menu.FieloEE__ExternalName__c = 'isClone' + FieloPRM_UTILS_MenuMethods.getDateTimeString() + menu.FieloEE__ExternalName__c;
                if(menu.FieloEE__ExternalName__c.length() > 80){
                    menu.FieloEE__ExternalName__c = menu.FieloEE__ExternalName__c.left(80); 
                }
                if(setExternalNames.contains(menu.FieloEE__ExternalName__c)){
                    menu.FieloEE__ExternalName__c += string.valueOf(auxNumber);
                    auxNumber++;
                }
                setExternalNames.add(menu.FieloEE__ExternalName__c);                       
            }
            
            Set<String> setExistingExternalNames = new Set<String>();
             
            for(FieloEE__Menu__c menu: [SELECT Id, FieloEE__ExternalName__c FROM FieloEE__Menu__c WHERE FieloEE__ExternalName__c IN: setExternalNames]){
                setExistingExternalNames.add(menu.FieloEE__ExternalName__c);
            }
            
            if(!setExistingExternalNames.isEmpty()){                   
                for(FieloEE__Menu__c menu: triggerNew ){ 
                    if(setExistingExternalNames.contains(menu.FieloEE__ExternalName__c)){ 
                        menu.FieloEE__ExternalName__c += string.valueOf(auxNumber);
                        auxNumber++;
                    }
                }
            }
        }
        
    }

}