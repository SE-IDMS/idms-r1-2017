@isTest
private class FieloPRM_VFE_EventCreateExtensionTest {
	
	@isTest static void test_method_one() {
		
		User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

        	FieloEE.MockUpFactory.setCustomProperties(false);

                
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;

            Country__c testCountry = new Country__c();
            testCountry.Name   = 'Global';
            testCountry.CountryCode__c = 'WW';


	        INSERT testCountry;
	       
	        PRMCountry__c testPRmcountry = new PRMCountry__c();
	            testPRmcountry.Name   = 'Global';
	            testPRmcountry.Country__c = testCountry.id;
	            testPRmcountry.PLDatapool__c = '123123test';
	        INSERT testPRmcountry;


            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
            member.F_Country__c = testCountry.id;
                 
            insert member;

            
        	FieloPRM_EventCatalog__c eventcat = new FieloPRM_EventCatalog__c();
        	eventcat.name = 'event test';
        	eventcat.F_PRm_isactive__c = true;
        	eventcat.F_PRM_Country__c = testPRmcountry.id;
        	
        	insert eventcat;

        	ApexPages.StandardController sc = new ApexPages.StandardController(member);

			FieloPRM_VFE_EventCreateExtension eventc = new FieloPRM_VFE_EventCreateExtension(sc);
			system.debug(eventc.listCatalogWrapper[0]);
			eventc.listCatalogWrapper[0].selected = true;
			eventc.createEvents();
			eventc.doPrevious();
			eventc.doNext();
			List<SelectOption> aux = eventc.getCountriesOptions();
			
		}
	}
	
	
	
}