Public Class Utils_IBProductPicklistManager implements Utils_PicklistManager
{
    public static String selectedBrand;
    
    // Return name of the levels
    Public list<String> getPickListLabels()
    {
        List<String> Labels = new List<String>();
        Labels.add('Brand');
        Labels.add('Device Type');
        Labels.add('Range');
        //Labels.add('Type');
        //Labels.add('Sub-Type');
        return Labels;
    } 

    // Return dependent picklist value 
    public List<SelectOption> getPicklistValues(Integer i, String S)
    {
             
       List<SelectOption> options = new  List<SelectOption>();
       
       If(S == Label.CL00355)
           return null;
          
       options.add(new SelectOption(Label.CL00355,Label.CL00355)); 
 
       // Populate Brand fields
       if(i==1)
       {
           
           For(Brand__c B:[SELECT id, Name FROM Brand__c order by Name])
           {
                options.add(new SelectOption(B.id,B.Name));    
           } 
       } 
       
       // Select a device type
       if(i==2)
       {          
           
           For(DeviceTypesPerBrand__c D:[SELECT DeviceType__r.id, DeviceType__r.Name FROM DeviceTypesPerBrand__c WHERE Brand__c=:S order by DeviceType__r.Name])
           {       
               options.add(new SelectOption(S+'_'+D.DeviceType__r.id, D.DeviceType__r.Name));    
           }

       } 
    
       // Select a Range
       
       
       if(i==3)
       {
           String[] key = S.split('_');
           String brandId = key[0];
           String devicetypeId = key[1];
           System.debug('#### brandId: '+brandId +', devicetypeId: '+devicetypeId );
           
           
           For(RangesPerDeviceAndBrand__c P:[SELECT category__r.id, category__r.Name FROM RangesPerDeviceAndBrand__c WHERE Brand__r.id=:brandId AND DeviceType__r.id=:devicetypeId and category__c <> '' order by category__r.Name])
           {
                options.add(new SelectOption(P.category__r.id,P.category__r.Name));         
           } 
                 
       } 

       return  options;  
    }
}