public class dummyLanguage{

    Public String userAgent{get;set;}
    public string languageCode{get;set;}
    public string selectedVal{get;set;}
    public string langCode{get;set;}
    public string currentPageURL{get;set;}
    
    public dummyLanguage(){
        
        
        string languageCode1=ApexPages.currentPage().getParameters().get('lang');
         
        if(string.isnotblank(languageCode1)){
        
        languageCode=languageCode1;
        
        }
        currentPageURL=ApexPages.currentPage().getUrl();
        system.debug(currentPageURL);
        /*
        userAgent =ApexPages.currentPage().getHeaders().get('Accept-Language');
        
        system.debug(userAgent);
        if(userAgent.contains(',')){
        integer indexofComma=useragent.indexof(',');
        
        system.debug(indexofComma);
        
        languageCode=userAgent.substring(0,indexofComma);
        }
        else{
        languageCode=userAgent;
        }
        
        */
        
        }
        
        
        public List<SelectOption> getoptns(){
        
        List<IDMSAvailableLanguages__c> appmap= [select language__c,name from IDMSAvailableLanguages__c];
        list<string> list1=new list<string>();
        list1.add('Select Language');
        for (integer i=0;i<appmap.size();i++){
            IDMSAvailableLanguages__c obj= appmap.get(i);
            list1.add(obj.language__c);
        }
        
        List<SelectOption> optns = new List<Selectoption>();
        for(integer i=0;i<list1.size();i++){
        optns.add(new SelectOption(list1.get(i),list1.get(i)));
        
            }
        return optns;
        }


        public pagereference save(){
        
        system.debug(selectedVal);
        
        
        langCode=[select name from IDMSAvailableLanguages__c where language__c=:selectedVal].name;
        
        system.debug(langCode);
        system.debug(currentPageURL);
        
        pagereference pg=new pagereference(currentPageURL);
        pg.getParameters().put('lang',langCode);
        pg.setRedirect(true);
        system.debug(pg);
        return pg;
        
        }



}