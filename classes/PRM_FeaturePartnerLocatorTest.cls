@IsTest
public class PRM_FeaturePartnerLocatorTest {
	static testmethod void Test_FeatureTest() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

                
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.PLPartnerLocatorFeatureGranted__c=false;
            insert acc;
            
            Account accTo= new Account();
            acc.PLPartnerLocatorFeatureGranted__c=false;
            accTo.Name = 'test acc to';
            insert accTo;

            List<FieloEE__Member__c> memberList = new List<FieloEE__Member__c>();
            FieloEE__Member__c member1 = new FieloEE__Member__c(FieloEE__LastName__c= 'AKRAM UNIT TEST ',FieloEE__FirstName__c = 'AKRAM UNIT TEST ',FieloEE__Street__c = 'AKRAM STREET',F_Account__c = acc.id);
            memberList.add(member1);

            FieloEE__Member__c member2 = new FieloEE__Member__c(FieloEE__LastName__c= 'AKRAM UNIT TEST 2',FieloEE__FirstName__c = 'AKRAM UNIT TEST 2',FieloEE__Street__c = 'AKRAM STREET 2',F_Account__c = acc.id);
           	memberList.add(member2);

            FieloEE__Member__c member3 = new FieloEE__Member__c(FieloEE__LastName__c= 'AKRAM UNIT TEST 3',FieloEE__FirstName__c = 'AKRAM UNIT TEST 3',FieloEE__Street__c = 'AKRAM STREET 3',F_Account__c = accTo.id);
            memberList.add(member3);
            
            insert memberList;

            List<Contact> contactList = new List<Contact>();
            Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
            Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',PRMOnly__c = true, PRMUIMSID__c = '123TEST1',
                Email = 'test1@test.com',PRMEmail__c='test1@test.com',FieloEE__Member__c=member1.Id);
            contactList.add(c1);
            

            Contact c2 = new Contact(FirstName='Test2',LastName = 'TestName2',AccountId=acc.Id,JobTitle__c='Z4',
                CorrespLang__c='EN',WorkPhone__c='1234567890',PRMOnly__c = true, PRMUIMSID__c = '123TEST2',
                Email = 'test2@test.com',PRMEmail__c='test2@test.com',FieloEE__Member__c=member2.Id);
            contactList.add(c2);

            Contact c3 = new Contact(FirstName='Test3',LastName = 'TestName3',AccountId=accTo.Id,JobTitle__c='Z5',
                CorrespLang__c='EN',WorkPhone__c='1234567890',PRMOnly__c = true, PRMUIMSID__c = '123TEST3',
                Email = 'test3@test.com',PRMEmail__c='test3@test.com',FieloEE__Member__c=member3.Id);
            contactList.add(c3);

            insert contactList;
            
            List<User> userList = new List<User>();

            User userCom = new User(FirstName = c1.FirstName,contactId = c1.Id, FederationIdentifier=c1.PRMUIMSId__c,LastName = c1.LastName, Email = c1.PRMEmail__c, Username = c1.PRMEmail__c + '.bfo.com',Alias ='teOo8_', CommunityNickname = c1.Name+'8_9',ProfileId = ProfileFieloId, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            userList.add(userCom);

            User user2 = new User(FirstName = c2.FirstName,contactId = c2.Id, FederationIdentifier=c2.PRMUIMSId__c,LastName = c2.LastName, Email = c2.PRMEmail__c, Username = c2.PRMEmail__c + '.bfo.com',Alias ='teOfa_', CommunityNickname = c2.Name+'3_9',ProfileId = ProfileFieloId, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            userList.add(user2);

            User user3 = new User(FirstName = c3.FirstName,contactId = c3.Id, FederationIdentifier=c3.PRMUIMSId__c,LastName = c3.LastName, Email = c3.PRMEmail__c, Username = c3.PRMEmail__c + '.bfo.com',Alias ='te1fv8_', CommunityNickname = c3.Name+'1_9',ProfileId = ProfileFieloId, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
            userList.add(user3);

            insert userList;

            FieloPRM_Feature__c feature = new FieloPRM_Feature__c(name = 'PartnerLocator',F_PRM_FeatureCode__c = 'PartnerLocator',F_PRM_CustomLogicClass__c = 'PRM_FeaturePartnerLocator');
            insert feature;
            
            Test.startTest(); 

                List<FieloPRM_MemberFeature__c> listMemberFeature = new list<FieloPRM_MemberFeature__c>();
               
                FieloPRM_MemberFeature__c memberFeature1 = new FieloPRM_MemberFeature__c(F_PRM_Feature__c =  feature.id,F_PRM_Member__c =  member1.id,F_PRM_IsActive__c  =  true,PRMUIMSId__c = '123TEST1');
                listMemberFeature.add(memberFeature1);

                FieloPRM_MemberFeature__c memberFeature2 = new FieloPRM_MemberFeature__c(F_PRM_Feature__c =  feature.id,F_PRM_Member__c =  member2.id,F_PRM_IsActive__c  =  true,PRMUIMSId__c = '123TEST2');
                listMemberFeature.add(memberFeature2);

                FieloPRM_MemberFeature__c memberFeature3 = new FieloPRM_MemberFeature__c(F_PRM_Feature__c =  feature.id,F_PRM_Member__c =  member3.id,F_PRM_IsActive__c  =  true,PRMUIMSId__c = '123TEST3');
                listMemberFeature.add(memberFeature3);

                insert listMemberFeature;
				
                Account[] accList = [select id,PLPartnerLocatorFeatureGranted__c from Account where id=:acc.Id or id=:accTo.Id];
                System.assertEquals(true,accList[0].PLPartnerLocatorFeatureGranted__c);
                System.assertEquals(true,accList[1].PLPartnerLocatorFeatureGranted__c);
                
                List<FieloPRM_MemberFeature__c> listMemberFeatureToDelete = new List<FieloPRM_MemberFeature__c>{memberFeature2,memberFeature3};
                delete listMemberFeatureToDelete;

                acc = [select id,PLPartnerLocatorFeatureGranted__c from Account where id=:acc.Id];
                System.assertEquals(true,acc.PLPartnerLocatorFeatureGranted__c);

                accTo = [select id,PLPartnerLocatorFeatureGranted__c from Account where id=:accTo.Id];
                System.assertEquals(false,accTo.PLPartnerLocatorFeatureGranted__c);
            	
            Test.stopTest();

            
        }
    }
}