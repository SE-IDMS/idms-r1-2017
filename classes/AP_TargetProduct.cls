public class AP_TargetProduct{
        public static void ExistCheck(list<Target_Products__c > TPlist){
        system.debug('start Exist CHECK');
        system.debug('tplist: '+tplist);
         Map<String,String> mapProdTPid = new Map<String,String>();
         list<Target_Products__c>  tempTPlist = new list<Target_Products__c>();
    for(Target_Products__c tp : [SELECT Product__r.Name,name,id,Product_Quality_Alert__r.name FROM Target_Products__c WHERE Product_Quality_Alert__r.status__c ='Active']){
        
            mapProdTPid.put(tp.product__c,tp.id);
    }
    

    for(Target_Products__c tpa: TPlist ){
     if(mapProdTPid.containsKey(tpa.product__C)){
        tempTPlist= [SELECT id,name,Product_Quality_Alert__r.Name,Product_Quality_Alert__r.Start_Date__c,Product_Quality_Alert__r.End_Date__c FROM Target_Products__c where Product__c =: tpa.product__c ];
        system.debug('*********************dates');
        system.debug(tempTPlist[0].Product_Quality_Alert__r.Start_Date__c);
        system.debug(tpa.Product_Quality_Alert__r.Start_Date__c);
        system.debug(tpa.Product_Quality_Alert__r.End_Date__c);
        system.debug(tempTPlist[0].Product_Quality_Alert__r.End_Date__c);   
        if(((tempTPlist[0].Product_Quality_Alert__r.Start_Date__c < tpa.TECH_PQAstartdate__c)&&(tpa.TECH_PQAstartdate__c < tempTPlist[0].Product_Quality_Alert__r.End_Date__c))||((tempTPlist[0].Product_Quality_Alert__r.Start_Date__c < tpa.TECH_PQAenddate__c)&&(tpa.TECH_PQAenddate__c < tempTPlist[0].Product_Quality_Alert__r.End_Date__c))){
            
         tpa.adderror('This product is already under an active Product Quality Alert: '+tempTPlist[0].Product_Quality_Alert__r.Name);
            
         system.debug(mapProdTPid);
        }
     }
    }
         //add below
         Map<String,String> mapProdTPidINACTIVE = new Map<String,String>();
         list<Target_Products__c>  tempTPlistINACTIVE = new list<Target_Products__c>();
    for(Target_Products__c tpinactive : [SELECT Product__r.Name,name,id,Product_Quality_Alert__r.name FROM Target_Products__c WHERE Product_Quality_Alert__r.status__c ='Inactive']){
        
            mapProdTPidINACTIVE.put(tpinactive.product__c,tpinactive.id);
    }
    

    for(Target_Products__c tpaINACTIVE: TPlist ){
     if(mapProdTPidINACTIVE.containsKey(tpaINACTIVE.product__C)){
        tempTPlistINACTIVE= [SELECT id,name,Product_Quality_Alert__r.Name,Product_Quality_Alert__r.Start_Date__c,Product_Quality_Alert__r.End_Date__c FROM Target_Products__c where Product__c =: tpaINACTIVE.product__c ];
        system.debug('*********************datesINACTIVE');
        system.debug(tempTPlistINACTIVE[0].Product_Quality_Alert__r.Start_Date__c);
        system.debug(tpaINACTIVE.Product_Quality_Alert__r.Start_Date__c);
        system.debug(tpaINACTIVE.Product_Quality_Alert__r.End_Date__c);
        system.debug(tempTPlistINACTIVE[0].Product_Quality_Alert__r.End_Date__c);   
        if(((tempTPlistINACTIVE[0].Product_Quality_Alert__r.Start_Date__c < tpaINACTIVE.TECH_PQAstartdate__c)&&(tpaINACTIVE.TECH_PQAstartdate__c < tempTPlistINACTIVE[0].Product_Quality_Alert__r.End_Date__c))||((tempTPlistINACTIVE[0].Product_Quality_Alert__r.Start_Date__c < tpaINACTIVE.TECH_PQAenddate__c)&&(tpaINACTIVE.TECH_PQAenddate__c < tempTPlistINACTIVE[0].Product_Quality_Alert__r.End_Date__c))){
            
         tpaINACTIVE.adderror('This product is already under a Product Quality Alert in the same time range, Product Quality Alert: '+tempTPlistINACTIVE[0].Product_Quality_Alert__r.Name);
            
         system.debug(mapProdTPidINACTIVE);
        }
         //add above
     }
        
    }

   
    
  }
 }