/************************************************************
* Developer: Marco Paulucci                                 *
* Type: Test Class                                          *
* Created Date: 14.04.2014                                  *
* Tested Class: FieloPRM_AP_NewsController                  *
************************************************************/

@isTest
public without sharing class FieloPRM_AP_NewsControllerTest {
  
    public static testMethod void testUnit(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            Id rtComp = [SELECT Id, DeveloperName, SObjectType FROM RecordType WHERE DeveloperName = 'FieloPRM_C_News'].Id;
            
            //Id rtNews = [SELECT Id, DeveloperName, SObjectType FROM RecordType WHERE DeveloperName = 'Fielo_Event' AND SobjectType = 'FieloEE__News__c'].Id;

            FieloEE__Category__c cat = FieloPRM_UTILS_MockUpFactory.createCategory();

            FieloEE__News__c news = new FieloEE__News__c(
                //RecordTypeId = rtNews, 
                FieloEE__Title__c = 'Test HTML Editor', 
                FieloEE__QuantityComments__c = 0, 
                FieloEE__IsActive__c = true,
                FieloEE__Body__c = 'Testing', 
                FieloEE__QuantityLikes__c = 0, 
                FieloEE__HasSegments__c = false,
                FieloEE__CategoryItem__c = cat.Id
            );
            insert news;

            FieloEE__Tag__c tag = new FieloEE__Tag__c(
                Name = 'tag'
            );
            insert tag;

            FieloEE__TagItem__c tagItem = new FieloEE__TagItem__c(
                FieloEE__Tag__c = tag.Id,
                FieloEE__News__c = news.Id
            );
            insert tagItem;

            FieloEE__Menu__c menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Menu'
            );
            insert menu;

            FieloEE__Component__c comp = new FieloEE__Component__c(
                Name = 'test',
                RecordtypeId = rtComp,
                FieloEE__Menu__c = menu.Id
            );
            insert comp;
            
            
            
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            FieloPRM_AP_NewsController C = new FieloPRM_AP_NewsController();
        }
    }
}