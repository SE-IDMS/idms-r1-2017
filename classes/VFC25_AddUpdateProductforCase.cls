/*
    Author          : Accenture Team
    Date Created    : 18/05/2011 
    Description     : Controller extensions for VFP25_AddUpdateProductforCase.This class fetches the search 
                      parameters from the component controller, performs the search by calling factory methods  
*/
public class VFC25_AddUpdateProductforCase extends VFC_ControllerBase 
{
    public caseGMRSearch GMRsearchForCase{get;set;}
    private Case currentCase = new Case();
  
     /*=======================================
      INNER CLASSES
    =======================================*/ 
        
    // GMR implementation for Cases
    public class caseGMRSearch implements Utils_GMRSearchMethods
    {
        public Void Init(String ObjID, VCC08_GMRSearch Controller)
        {
            VFC25_AddUpdateProductforCase thisController = (VFC25_AddUpdateProductforCase)Controller.pageController;
                
            //Pre-populates if there are existing values for Comm Ref & PM0 Heirachy
           if(!Controller.Cleared && !Controller.SearchHasBeenLaunched) // This is the first initialization
           {
                if(thisController.currentCase.Id !=null)
                {
                    Controller.filters.add(thisController.currentCase.ProductBU__c);
                    Controller.filters.add(thisController.currentCase.ProductLine__c);
                    Controller.filters.add(thisController.currentCase.ProductFamily__c);
                    Controller.filters.add(thisController.currentCase.Family__c);                                                
                    Controller.searchKeyword = thisController.currentCase.CommercialReference__c;
                    // Added by GD team to populate the User entered commercial reference for DECEMBER Release 2012
                    If(Controller.searchKeyword == null)
                    Controller.searchKeyword = ApexPages.currentPage().getParameters().get('CR');
                }
            }
        }
        
        // Action performed when clicking on "Select" on the PM0 search results
        public Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
        {
            System.Debug('****** Updation of Case Begins  for VFC25_AddUpdateProductforCase******');  
            
            VCC08_GMRSearch GMRController = (VCC08_GMRSearch)controllerBase;
            VFC25_AddUpdateProductforCase thisController = (VFC25_AddUpdateProductforCase)GMRController.pageController;  
            
            System.Debug('VFC25.PerformAction.INFO - Action Number = ' + thisController.ActionNumber);
                
            if(GMRController.ActionNumber==1){
            
                System.debug('VFC25.PerformAction.INFO - Action  1 is called');
                System.debug('VFC25.PerformAction.INFO - Current Case ID = ' +thisController.currentCase.Id);
                
                if(thisController.currentCase.Id!=null)
                {
                    AP12_CaseIntegrationMethods.UpdateCasePM0(thisController.currentCase,(DataTemplate__c)obj);
                    System.Debug('****** Updation of Case Ends  for VFC25_AddUpdateProductforCase******');
                    return new pagereference('/'+thisController.currentCase.Id);
                }
                else
                {
                    System.Debug('****** Updation of Case Fails  for VFC25_AddUpdateProductforCase******');
                    return ApexPages.currentPage();
                }
            }
            if(GMRController .ActionNumber==2){
            
                System.debug('VFC25.PerformAction.INFO - Action  2 is called');
                System.debug('VFC25.PerformAction.INFO - Current Case ID = ' +thisController.currentCase.Id);
            
                if(thisController.currentCase.Id!=null)
                {
                    AP12_CaseIntegrationMethods.UpdateCaseFamily(thisController.currentCase,(DataTemplate__c)obj);
                    System.Debug('****** Updation of Case Ends  for VFC25_AddUpdateProductforCase******');
                    return new pagereference('/'+thisController.currentCase.Id);
                }
                else
                {
                    System.Debug('****** Updation of Case Fails  for VFC25_AddUpdateProductforCase******');
                    return ApexPages.currentPage();
                }
            }
            else{
                System.Debug('****** Updation of Case Fails  for VFC25_AddUpdateProductforCase******');
                return ApexPages.currentPage();
            }
        }
        
        // Cancel method returns the associated Case detail page
        public PageReference Cancel( VCC08_GMRSearch Controller)
        { 
            VFC25_AddUpdateProductforCase CaseController = (VFC25_AddUpdateProductforCase)Controller.pageController;
            PageReference pageResult;
            
            try
            {
                if(CaseController.CurrentCase.status != 'Closed')
                {
                    CaseController.CurrentCase.TECH_LaunchGMR__c = false;
                    DataBase.SaveResult UpdateCase = Database.Update(CaseController.CurrentCase);
                }
                pageResult = new ApexPages.StandardController(CaseController.CurrentCase).view();
            }
            catch(exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+e.getMessage()));
            }

            return pageResult;
        }   
    }
    
    /*=======================================
      CONSTRUCTOR
    =======================================*/          

    public VFC25_AddUpdateProductforCase(ApexPages.StandardController controller)
    {
        currentCase = (Case)controller.getRecord();
        
        List<Case> relatedCaseList  = [Select Id, Status, SupportCategory__c, CommercialReference__c, ProductBU__c, ProductLine__c, ProductFamily__c, Family__c, TECH_LaunchGMR__c from Case where Id=:currentCase.Id limit 1];
        
        String URLParam = ApexPages.currentPage().getParameters().get('origin');
        String URLCR = ApexPages.currentPage().getParameters().get('CR');
               
        if(relatedCaseList.size() == 1) {
            currentCase = relatedCaseList[0];  
        }
        
        System.debug('VFC25.Constructor.INFO - URL parameter = ' + URLParam );
        System.debug('VFC25.Constructor.INFO - Launch GMR = ' + currentCase.TECH_LaunchGMR__c);
        
        if(URLParam == 'icr') {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'\"'+URLCR +'\" is not a valid commercial reference.'));
        }
        else if(currentCase.SupportCategory__c == '4 - Post-Sales Tech Support' && currentCase.TECH_LaunchGMR__c) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.warning,'You must select at least a family for post sales technical cases.'));
        }

        GMRsearchForCase = new caseGMRSearch();
    }

   
}