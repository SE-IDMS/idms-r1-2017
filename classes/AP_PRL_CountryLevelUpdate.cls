/*
Shruti Karn             Mar2013    PRM May13 Release    -    Create Country Program Level when new Global Program Level is created
                                                        -    Update Country Program Level when new Global Program Level is updated
Srinivas Nallapati      Mar2013    PRM May13 Release    -   Level heirarchy validation
*/
public class AP_PRL_CountryLevelUpdate {
    
    public static void creatCountryLevel(map<Id,list<ProgramLevel__c>> mapGlobalProgramLevel , set<Id> setProgramLevelID)
    {
        //list<PartnerProgram__c> lstCountryProgram = new list<PartnerProgram__c>();
        list<PRogramLevel__c> lstCountryLevel = new list<ProgramLevel__c>();
        list<ProgramLevel__c> lstExistingLevel = new list<ProgramLevel__c>();
        String prgDecomSts = System.label.CLMAY13PRM34;
                            
        map<Id,PartnerProgram__c> mapCountryProgram = new map<ID,PartnerProgram__c>([Select id,globalpartnerprogram__c from PartnerProgram__c where globalpartnerprogram__c in : mapGlobalProgramLevel.keySet() and ProgramStatus__c != :prgDecomSts limit 50000]);
        if(setProgramLevelID != null)
        {
            lstExistingLevel = [Select id,partnerprogram__c,globalprogramlevel__c from ProgramLevel__c where recordtypeid =: Label.CLMAY13PRM03 and partnerprogram__c in :mapCountryProgram.keySet() and globalprogramlevel__c in :setProgramLevelID limit 50000];
            if(!lstExistingLevel.isEmpty())
            {
                for(ProgramLevel__c level : lstExistingLevel)
                    mapCountryProgram.remove(level.partnerprogram__c);
            }

        }
        for(PartnerProgram__c countryPRG : mapCountryProgram.values())
        {
            if(mapGlobalProgramLevel.containsKey(countryPRG.globalpartnerprogram__c))
            {
                for(ProgramLevel__c prgLevel : mapGlobalProgramLevel.get(countryPRG.globalpartnerprogram__c))
                {
                    ProgramLevel__c level = new ProgramLevel__c();
                    level = prgLevel.clone(false,true);
                    level.partnerprogram__c = countryPRG.Id;
                    level.globalprogramlevel__c = prgLevel.Id;
                    level.LevelStatus__c = Label.CLMAY13PRM02;
                    level.recordtypeid = Label.CLMAY13PRM03;
                    lstCountryLevel.add(level);
                    
                }
            }
            
        }
        try
        {
            system.debug('Insert Country Level: ' + lstCountryLevel);
            insert lstCountryLevel;
            
        }
        catch(Exception e)
        {
            system.debug('Excpetion:'+e);
            mapGlobalProgramLevel.values().get(0).get(0).addError(e.getMessage()); 
            System.debug(e.getMessage()); 
             
        }
        
         
    }
    
    //public static void updateCountryFeature(list<ProgramFeature__c> lstNewPRGFeature)
    public static void updateCountryLevel(map<Id,ProgramLevel__c> mapNewGlobalPRGLevel , map<Id,ProgramLevel__c> mapOldGlobalPRGLevel)
    {
      list<ProgramLevel__c> lstChildLevel = new list<ProgramLevel__c>();
      String strFields = '';
      map<Id,ProgramLevel__c> mapGlobalChildLevel = new map<Id,ProgramLevel__c>();
      list<Id> lstInactiveLevel = new list<Id>();
      try
      {        
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.ProgramLevel__c.fields.getMap();
                   
        if (mapFields != null)
        {
            for (Schema.SObjectField ft : mapFields.values())// loop through all field tokens (ft)
            { 
                if (ft.getDescribe().isUpdateable())// field is updateable
                { 
                    strFields += ft.getDescribe().getName()+',';
                }
            }
        }
         
        if (strFields.endsWith(','))
        {
            strFields = strFields.substring(0,strFields.lastIndexOf(','));
        }
     
        set<Id> setGlobalLevelID = new set<ID>();
        list<ProgramLevel__c> lstLevelToUpdate = new list<ProgramLevel__c>();
        setGlobalLevelID.addAll(mapNewGlobalPRGLevel.keySet());
        String prgDecomSts = System.label.CLMAY13PRM34;
        String childLevelQuery = 'Select '+ strFields + ',PArtnerProgram__r.PRogramStatus__c FROM ProgramLevel__c WHERE GlobalProgramLevel__c in :setGlobalLevelID and PArtnerProgram__r.PRogramStatus__c != :prgDecomSts limit 10000';
  
        lstChildLevel = database.query(childLevelQuery);
             
        if(!lstChildLevel.isEmpty())
         {
            //System.debug('******* List Child Level ******'+lstChildLevel.size());
            
           for(PRogramLevel__c childLevel : lstChildLevel)
            {   
                //System.debug('******* List Child Level ******'+childLevel.PArtnerProgram__r.PRogramStatus__c);
                
                for (String str : mapFields.keyset()) 
                {
                    if(mapNewGlobalPRGLevel.get(childLevel.globalProgramLevel__C).recordtypeid == Label.CLMAY13PRM18 &&  mapFields.get(str).getDescribe().isUpdateable() && mapNewGlobalPRGLevel.get(childLevel.globalProgramLevel__C).get(str) != mapOldGlobalPRGLevel.get(childLevel.globalProgramLevel__C).get(str))
                    {
                       //if(!(str == Label.CLMAY13PRM22 && mapNewGlobalPRGLevel.get(childLevel.globalProgramLevel__C).get(str) == Label.CLMAY13PRM19 && childLevel.PArtnerProgram__r.PRogramStatus__c != Label.CLMAY13PRM21))// Should not update Draft Country Program's status to Active
                        if(str == Label.CLMAY13PRM22 && mapNewGlobalPRGLevel.get(childLevel.globalProgramLevel__C).get(str) == Label.CLMAY13PRM21)
                        {
                            
                            lstInactiveLevel.add(childLevel.globalProgramLevel__C);
                            childLevel.put('TECH_InactiveGlobalLevel__c', true);
                            lstLevelToUpdate.add(childLevel);
                            
                            //lstInactiveLevel.add(childLevel.id);
                        }
                        //if(!(str == Label.CLMAY13PRM22))
                        if(CS_GlobalLeveltoCountryLevel__c.getValues(str.toLowerCase()) == null)
                        {
                            childLevel.put(str, mapNewGlobalPRGLevel.get(childLevel.globalProgramLevel__C).get(str));
                            lstLevelToUpdate.add(childLevel);
                        }
                    }
                }
            }
          }  
          else
          {
              for(Id prgLevelID : mapNewGlobalPRGLevel.keySET())
              {
                  if(mapNewGlobalPRGLevel.get(prgLevelID).LevelStatus__c != mapOldGlobalPRGLevel.get(prgLevelID).LevelStatus__c && mapNewGlobalPRGLevel.get(prgLevelID).LevelStatus__c == Label.CLMAY13PRM21)
                      lstInactiveLevel.add(prgLevelID);
              }
          }
          
        if(!lstLevelToUpdate.isEmpty())
            update lstChildLevel;
        list<ProgramFeature__c> lstProgramFeature = new list<ProgramFeature__c>();
        lstProgramFeature = [Select id,FeatureStatus__C from programfeature__c where programlevel__c in :lstInactiveLevel limit 10000];
        if(!lstProgramFeature.isEmpty())
        {
            for(ProgramFeature__c prgFeature : lstProgramFeature)
            {
                prgFeature.FeatureStatus__c = Label.CLMAY13PRM07;
            }
            
            update lstProgramFeature;
        }
      }
      catch(Exception e)
      {

            mapNewGlobalPRGLevel.values().get(0).addError(Label.CLMAY13PRM42);
            System.debug(e.getMessage());
      } 
    }
    

    
    
}//End of class