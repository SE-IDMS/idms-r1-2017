/***
Test clss for VFC_COINbFO_BCAChartByMonth;
***/

    @isTest
    public class VFC_COINbFO_BCAChartByMonth_TEST {
    
        public static testMethod void myUnitTest() {
            Offer_Lifecycle__c inOffLif= new Offer_Lifecycle__c(); 
           // inOffLif.Name='Test Schneider';
            //inOffLif.Leading_Business_BU__c ='IT';
           // inOffLif.Offer_Classification__c ='Major';
            inOffLif.Forecast_Unit__c='Kilo Euro';
           // inOffLif.Launch_Master_Assets_Status__c='Red';
           // inOffLif.Stage__c ='0 - Define';
            
           // Insert inOffLif; 
            Country__c cotryObj= new Country__c();
            cotryObj.Name ='France';
            cotryObj.CountryCode__c='1FR';
            Insert cotryObj;
            Schema.DescribeSObjectResult d = Schema.SObjectType.Milestone1_Project__c;    
            Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();  
            String MPRecordTypeId = rtMapByName.get('Offer Introduction Project').getRecordTypeId();
            Milestone1_Project__c mileProject= new Milestone1_Project__c(); 
            mileProject.RecordTypeId=MPRecordTypeId ;
            mileProject.Name ='By Country Project ';
           // mileProject.Offer_Launch__c=inOffLif.id;
            mileProject.Country__c=cotryObj.id;
            mileProject.Country_Announcement_Date__c=Date.today();
            mileProject.Country_End_of_Commercialization_Date__c = Date.today();
            mileProject.TECH_IsForecastByMonth__c = true;
            mileProject.TECH_IsForecastByYear__c = true;
            mileProject.TECH_IsForecastByQuarter__c = true;
            
            insert mileProject ;
            
            List<OfferLifeCycleForecast__c> ListoffForecast= new List<OfferLifeCycleForecast__c>();
            OfferLifeCycleForecast__c  offForecast = new OfferLifeCycleForecast__c();
            for(integer i=1; i<MONTHS_OF_YEAR.size(); i++ ) {
                offForecast = new OfferLifeCycleForecast__c();
                offForecast.project__c=mileProject.id;
                offForecast.month__c=monthName(i);
                offForecast.ForcastYear__c=string.valueof(mileProject.Country_Announcement_Date__c.year());
                offForecast.Actual__c =1000;
                offForecast.BU__c =2000;
                offForecast.Country__c = 4000;
                offForecast.Country2__c='France';
                if(i <=3) {
                offForecast.ForecastQuarter__c='Q1';
                }else if( i > 3 && i <=6) {
                    offForecast.ForecastQuarter__c='Q2';
                }else if( i > 6 && i <=9) {
                    offForecast.ForecastQuarter__c='Q3';
                }
                else if( i > 9 && i <=12) {
                    offForecast.ForecastQuarter__c='Q4';
                }
                
                ListoffForecast.add(offForecast);
            
            
            }
            offForecast = new OfferLifeCycleForecast__c();
            offForecast.project__c=mileProject.id;
            //offForecast.month__c=monthName(i);Total by Year
            offForecast.ForcastYear__c=string.valueof(mileProject.Country_Announcement_Date__c.year());//'2014';
            offForecast.Actual__c =1000;
            offForecast.BU__c =2000;
            offForecast.Country__c = 4000;
            offForecast.Country2__c='France';
            offForecast.ForecastQuarter__c='Q4';             
                
            ListoffForecast.add(offForecast);
            
            OfferLifeCycleForecast__c offForecast1 = new OfferLifeCycleForecast__c();
            offForecast1.project__c=mileProject.id;
            offForecast1.month__c='Total by Year';
            offForecast1.ForcastYear__c=string.valueof(mileProject.Country_Announcement_Date__c.year());
            offForecast1.Actual__c =1000;
            offForecast1.BU__c =2000;
            offForecast1.Country__c = 4000;
            offForecast1.Country2__c='France';
            offForecast1.ForecastQuarter__c='Q4';             
                
            ListoffForecast.add(offForecast1);
            
            insert  ListoffForecast;
            ApexPages.currentPage().getParameters().put('Id', mileProject.Id);
            ApexPages.StandardController controller = new ApexPages.StandardController(mileProject);       
            VFC_COINbFO_BCAChartByMonth  MPForecast = new VFC_COINbFO_BCAChartByMonth(controller);
            MPForecast.MapofferCycleResults();
           // MPForecast.MapofferCycleResultsolddata();
           // MPForecast.chartDisplayByQuarterOlddata();
            MPForecast.chartDisplayByQuarter();
           // MPForecast.chartdisplayByYearOldData();
            MPForecast.chartdisplayByYear();
           // MPForecast.getForecastDataOlddata();
            MPForecast.getForecastData();
           // MPForecast.listOfferCycleResultsOldData();
            MPForecast.listOfferCycleResults();
            //MPForecast.monthName(2);
        
        }
    private static final List<String> MONTHS_OF_YEAR = new String[]{'January','February','March','April','May','June','July','August','September','October','November','December','Total by Year'};


    private static String monthName(Integer monthNum) {

        return(MONTHS_OF_YEAR[monthNum - 1]);

    }
    
    
    
    }