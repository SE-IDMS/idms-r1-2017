@isTest
Public Class VFC_PortletNewCasePage_TEST{
  
  Private Static TestMethod Void  Method1(){
           
      
      Integer max = 5;
      
      User RunningUser = [select id,name,IsActive,BypassVR__c from user where profile.Name = 'System Administrator' and BypassVR__c=true and IsActive=True limit 1];
      system.RunAs(RunningUser){
       UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        String randomString = EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(1,max).toUpperCase();
        Profile profile = [select id from profile where name='System Administrator'];
       User Usr =New User();
            Usr.UserRoleId =portalRole.Id; Usr.alias = 'testusr';Usr.email='testusr'+ '@accenture.com';Usr.emailencodingkey='UTF-8';Usr.lastname='Testing';Usr.languagelocalekey='en_US'; 
            Usr.localesidkey='en_US';Usr.profileid = profile.Id;Usr.BypassWF__c = true;
            Usr.timezonesidkey='Europe/London';Usr.username='testusr'+randomString+ '@bridge-fo.com';Usr . CAPRelevant__c=true;Usr . VisitObjDay__c=1;
            Usr.AvTimeVisit__c=3;Usr .WorkDaysYear__c=200;Usr .BusinessHours__c=8;Usr .UserBusinessUnit__c='Cross Business';Usr .Country__c='FR';
            Usr.BypassVR__c=true;
        Insert Usr;
      usr.isPortalEnabled=true;
      update usr;
      /*User UsrObj = Utils_TestMethods.createStandardUser('UsrPorta');
      //UsrObj.Account.Id = accounts.Id;
      UsrObj.ContactId= con.Id;
      //UsrObj.IsPortalEnabled= True;
      insert UsrObj; */
      Test.StartTest();
      Account accounts = Utils_TestMethods.createAccount();
      accounts.OwnerId = Usr.Id;
      Database.insert(accounts);
      
      Country__c Contry = New Country__c ();
      Contry.Name='France';Contry .CountryCode__c='Fr';
      insert Contry;
      
      contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
      
      Database.insert(con); 
      Con.Country__c = Contry.Id;
      update Con; 
       
       BusinessRiskEscalationEntity__c Organization = new BusinessRiskEscalationEntity__c();
       Organization.name = 'testOrganization'; 
       Organization.Entity__c ='TESTEntitity';
       Organization.SubEntity__c = 'SubEntity';
       Organization.Location_Type__c='plant'; 
       Organization.Location__c = 'India'; 
       insert Organization ;     
                   
       PointOfContact__c createPointOfContact =Utils_TestMethods.createPointOfContact(Organization.Id,Contry.Id,'TestPOC','Phone'); 
       createPointOfContact.IsActive__c =True;
       insert createPointOfContact;             
        CustomerCareTeam__c CCTeam =Utils_TestMethods.CustomerCareTeam(Contry.Id); 
        CCTeam.Name = 'Foxboro -L1'; 
        insert CCTeam ;
      
        
       Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'New');
      Database.insert(caseobj);
       Test.StopTest();
      
      ApexPages.StandardController Ctrl =New ApexPages.StandardController(caseobj);
      VFC_PortletNewCasePage PortletNewCase = New VFC_PortletNewCasePage(Ctrl);
      PortletNewCase.Status = 'New';
      PortletNewCase.SaveRec();
      PortletNewCase.goBack();
      PortletNewCase.CancelRec();
      }
  }
}