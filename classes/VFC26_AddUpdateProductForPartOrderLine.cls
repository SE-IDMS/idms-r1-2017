/*
    Author          : Chris Hurd
    Date Created    : 22/03/2013 
    Description     : Controller extensions for VFC26_AddUpdateProductForPartOrderLine.This class fetches the search 
                      parameters from the component controller, performs the search by calling factory methods  
*/
public class VFC26_AddUpdateProductForPartOrderLine extends VFC_ControllerBase
{
    public poLineGMRSearch GMRsearchForPOLine{get;set;}
    private SVMXC__RMA_Shipment_Line__c currentPOLine = new SVMXC__RMA_Shipment_Line__c();
  
     /*=======================================
      INNER CLASSES
    =======================================*/ 
        
    // GMR implementation for Cases
    public class poLineGMRSearch implements Utils_GMRSearchMethods
    {
        public Void Init(String ObjID, VCC08_GMRSearch Controller)
        {
            VFC26_AddUpdateProductForPartOrderLine thisController = (VFC26_AddUpdateProductForPartOrderLine)Controller.pageController;
                
            //Pre-populates if there are existing values for Comm Ref & PM0 Heirachy
           if(!Controller.Cleared && !Controller.SearchHasBeenLaunched) // This is the first initialization
           {
                if(thisController.currentPOLine.Id !=null)
                {
                    Controller.searchKeyword = thisController.currentPOLine.CommercialReference__c;
                    // Added by GD team to populate the User entered commercial reference for DECEMBER Release 2012
                    If(Controller.searchKeyword == null)
                    Controller.searchKeyword = ApexPages.currentPage().getParameters().get('CR');
                }
            }
        }
        
        // Action performed when clicking on "Select" on the PM0 search results
        public Pagereference PerformAction(sObject obj, VFC_ControllerBase ControllerBase)
        {
            System.Debug('****** Updation of Case Begins  for VFC26_AddUpdateProductForPartOrderLine******');  
            
            VCC08_GMRSearch GMRController = (VCC08_GMRSearch)controllerBase;
            VFC26_AddUpdateProductForPartOrderLine thisController = (VFC26_AddUpdateProductForPartOrderLine)GMRController.pageController;  
            
            if(GMRController.ActionNumber==1){
            
                System.debug('VFC25.PerformAction.INFO - Action  1 is called');
                System.debug('VFC25.PerformAction.INFO - Current PO Line ID = ' +thisController.currentPOLine.Id);
                
                if(thisController.currentPOLine.Id!=null)
                {
                    thisController.currentPOLine.CommercialReference__c = ((DataTemplate__c)obj).field8__c; 
                    DataBase.SaveResult currentPOLine_SR = DataBase.Update(thisController.currentPOLine,false);
            
		            if(!currentPOLine_SR.isSuccess()) {
		                DataBase.Error DMLerror = currentPOLine_SR.getErrors()[0];
		                System.debug('AP12.UpdateCasePM0.DML_EXCEPTION - Error when updating the po line.');
		                System.debug('AP12.UpdateCasePM0.DML_EXCEPTION - Error Message: ' + DMLerror.getMessage());
		            }
		            else {
		                System.debug('AP12.UpdateCasePM0.SUCCESS - Case updated successfully. ');
		            }  
                    return new pagereference('/'+thisController.currentPOLine.Id);
                }
                else
                {
                    System.Debug('****** Updation of Case Fails  for VFC26_AddUpdateProductForPartOrderLine******');
                    return ApexPages.currentPage();
                }
            }
            else{
                System.Debug('****** Updation of Parts Order Line Fails  for VFC26_AddUpdateProductForPartOrderLine******');
                return ApexPages.currentPage();
            }
        }
        
        // Cancel method returns the associated Case detail page
        public PageReference Cancel( VCC08_GMRSearch Controller)
        { 
            VFC26_AddUpdateProductForPartOrderLine poLineController = (VFC26_AddUpdateProductForPartOrderLine)Controller.pageController;
            PageReference pageResult;
            
            try
            {
                DataBase.SaveResult UpdateCase = Database.Update(poLineController.currentPOLine);
                pageResult = new ApexPages.StandardController(poLineController.currentPOLine).view();
            }
            catch(exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+e.getMessage()));
            }

            return pageResult;
        }   
    }
    
    /*=======================================
      CONSTRUCTOR
    =======================================*/          

    public VFC26_AddUpdateProductForPartOrderLine(ApexPages.StandardController controller)
    {
        currentPOLine = (SVMXC__RMA_Shipment_Line__c)controller.getRecord();
        
        List<SVMXC__RMA_Shipment_Line__C> relatedPOLineList  = [Select Id, CommercialReference__c from SVMXC__RMA_Shipment_Line__c where Id=:currentPOLine.Id limit 1];
        
        String URLParam = ApexPages.currentPage().getParameters().get('origin');
        String URLCR = ApexPages.currentPage().getParameters().get('CR');
               
        if(relatedPOLineList.size() == 1) {
            currentPOLine = relatedPOLineList[0];  
        }
        
        if(URLParam == 'icr') {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,'\"'+URLCR +'\" is not a valid commercial reference.'));
        }

        GMRsearchForPOLine = new poLineGMRSearch();
    }

   
}