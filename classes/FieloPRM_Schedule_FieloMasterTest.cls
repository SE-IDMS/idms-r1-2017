/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: Test class of schedulable class FieloPRM_Schedule_FieloMaster
********************************************************************/

@isTest
public without sharing class FieloPRM_Schedule_FieloMasterTest {
    
    @testSetup
    static void setupTest() {}
    
    @isTest
    static void unitTest1(){
        system.test.startTest();
        FieloPRM_Schedule_FieloMaster.scheduleNow();
        system.test.stopTest();
    }
}