/**
    Author          : Nitin Khunal
    Date Created    : 03/10/2016
    Description     : Utility class for PA
*/
Public class PA_UtilityClass {
    //To retrieve All Account record Id visiable to loggedin user 
    public static Set<Id> getAccountId(Boolean userAccount, Boolean manualShared, String caseAccessLevel, String userId) {
        Set<Id> setAccId = new Set<Id>();
        if(userAccount) {
            User UserObj =[select id,contactId, contact.accountId from User where Id=: userId];
            if(UserObj.contactId != null) {
                setAccId.add(UserObj.contact.accountId);
            }
        }
        if(manualShared) {
            String query = 'select id,AccountID from AccountShare where UserorGroupId =: userId';
            if(caseAccessLevel == 'Edit') {
                query += ' AND CaseAccessLevel = \'Edit\'';
            }
            system.debug('query==='+query);
            for(AccountShare xAccShare : Database.query(query)) {
                setAccId.add(xAccShare.AccountID);
            }
        }
        return setAccId;
    }
    
    //To retrieve Account records with set of Id
    public static List<Account> getAccountRecord(Set<Id> setAccId, Set<Id> setAccId1) {
        List<Account> lstAccount = new List<Account>();
        for(Account objAcc : [select id,Name, Country__r.Name, BillingCity, BillingState, BillingCountry, StateProvince__r.StateProvinceCode__c from Account where Id IN : setAccId AND Id NOT IN : setAccId1]){
            lstAccount.add(objAcc);
        }
        return lstAccount;
    }
    
    //To retrieve Account share records by account Id
    public static Set<Id> getAccountShareUserId(String accId) {
        Set<Id> setUserId = new Set<Id>();
        for(AccountShare u :[SELECT UserOrGroupId FROM AccountShare WHERE AccountId =: accId AND (RowCause = 'Owner' OR RowCause = 'Manual')]){
            setUserId.add(u.UserOrGroupId);
         }
         return setUserId;
    }
    
    //To retrieve User record
    Public static User getUser(String strUserId) {
        List<User> UserObj =[select id,contactId, accountId, contact.accountId, contact.account.Name, contact.account.BillingCity, contact.account.BillingState, 
                        contact.account.BillingCountry, contact.account.StateProvince__r.StateProvinceCode__c from User where Id=:strUserId];
        if(UserObj.size() > 0) {
            return UserObj[0];
        }
        return null;
    }
    
    //To get reporting organization from custom label CLQ316PA021
    Public static Set<Id> getReportingOrganisation() {
        Set<Id> setOrgIds = new Set<Id>();
        if(!String.isBlank(System.Label.CLQ316PA021)) {
            String strOrg = System.Label.CLQ316PA021;
            if(strOrg.contains(',')) {
                List<String> parts = strOrg.split(',');
                for(String xOrgId : parts) {
                    setOrgIds.add(xOrgId);
                }
            }else
            {
                setOrgIds.add(strOrg);
            }
        }
        return setOrgIds;
    }
    
    //To get PA Case Type field picklist values from case object
    public static List<String> getCaseType(){
       List<String> lstCaseTypeOptions= new List<String>();
       Schema.DescribeFieldResult fieldResult = Case.PACaseType__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       for( Schema.PicklistEntry f : ple){
           lstCaseTypeOptions.add(f.getValue());
       }  
       return lstCaseTypeOptions;
    }
    
    //To get Priority field picklist value from case object
    public static List<String> getCasePriority(){
       List<String> lstPriorityOptions  = new List<String>();
            
       Schema.DescribeFieldResult fieldResult = Case.Priority.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       //lstPriorityOptions.add('--None--');
       for( Schema.PicklistEntry f : ple)
       {
          lstPriorityOptions.add(f.getValue());
       }  
        return lstPriorityOptions;
    }
    
    //To get Platform field picklist value from case object
    public static List<String> getPlatform(){
       List<String> lstPlatformOptions= new List<String>();
       Schema.DescribeFieldResult fieldResult = Case.PAPlatform__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       lstPlatformOptions.add('--None--');
       for( Schema.PicklistEntry f : ple){
          lstPlatformOptions.add(f.getValue());
       }  
       return lstPlatformOptions;
    }
    
    //Concatenate Address with Account
    public static String concatenateAddress(String accountName, string city, string state, string country) {
        if(country != null) {
            accountName = country + ' - ' + accountName;
        }
        accountName += ' (';
        if(city != null) {
            accountName += city + ', ';
        }
        if(state != null) {
            accountName += state;
        }
        accountName += ')';
        if(accountName.endsWith(', )')) {
            accountName = accountName.replace(', )', ')');
        }
        accountName = accountName.replace(' ()', '');
        return accountName;
    }
    
    //Concatenate Address with Account without Country
    public static String concatenateAddressWithoutCountry(String accountName, string city, string state) {
        accountName += ' (';
        if(city != null) {
            accountName += city + ', ';
        }
        if(state != null) {
            accountName += state;
        }
        accountName += ')';
        if(accountName.endsWith(', )')) {
            accountName = accountName.replace(', )', ')');
        }
        accountName = accountName.replace(' ()', '');
        return accountName;
    }
    
    //Concatenate Address without Account
    public static String concatenateAddressWithoutAccount(string country, string city, string state) {
        string address = '';
        if(city != null) {
            address += city + ', ';
        }
        if(state != null) {
            address += state + ', ';
        }
        if(country != null) {
            address += country;
        }
        address = address.removeEnd(', ');
        return address;
    }
    
    public static Set<String> getOpenCaseOptionSet() {
        Set<String> setString = new Set<String>();
        if(!String.isBlank(System.Label.CLQ316PA041)) {
            String strOrg = System.Label.CLQ316PA041;
            if(strOrg.contains(',')) {
                List<String> parts = strOrg.split(',');
                for(String xOrgId : parts) {
                    setString.add(xOrgId);
                }
            }else
            {
                setString.add(strOrg);
            }
        }
        return setString;
    }
    
    public static Boolean accountSharedCheck(String accId) {
        List<Accountshare> lstAccountShare = [select id from Accountshare WHERE AccountId =: accId AND UserOrGroupId =: userinfo.getUserId() AND (RowCause = 'Owner' OR RowCause = 'Manual')];
        if(lstAccountShare.size() > 0) { 
            return true;
        }
        return false;
    }
    
    public static String userLocaleDateTimeConversion(DateTime cDT) {
        String strDateTime = cDT.format('MMM d, yyyy : HH:mm');
        return strDateTime;
    }
    
    public static String userLocaleDateTimeConversionIncludeSec(DateTime cDT) {
        String strDateTime = cDT.format('yyyy-MM-dd HH:mm:ss');
        return strDateTime;
    }
}