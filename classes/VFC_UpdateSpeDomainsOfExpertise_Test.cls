@isTest
Private Class VFC_UpdateSpeDomainsOfExpertise_Test
{
    static testMethod void testUpdateSpeDomains()
    {
        List<User> lstPrgApprover = [select id from user where isActive=true and ProgramApprover__c=true limit 20];
        List<User> lstPrgAdmin = [select id from user where isActive=true and ProgramAdministrator__c=true limit 20];
        List<User> lstPrgManager = [select id from user where isActive=true and ProgramManager__c=true limit 20];
        List<User> lstPrgOwner = [select id from user where isActive=true and ProgramOwner__c=true limit 20];
        
        SpecializationCatalog__c spc1 = new SpecializationCatalog__c();
        spc1.name = 'Test Catalog 1';
        spc1.Active__c =true;
        spc1.ExpirationPeriod__c = '12';
        spc1.description__c = 'test 2';
        insert spc1;
        
        Specialization__c spe1 = new Specialization__c();
        spe1.name = 'spe 1';
        spe1.SpecializationCatalog__c = spc1.id;
        spe1.level__C= 'Basic';
        spe1.ExpirationPeriod__c = '12';
        spe1.status__c= 'Active';
        spe1.ProgramApprover__c = lstPrgApprover[0].id;
        spe1.ProgramAdministrator__c = lstPrgAdmin[0].id ;
        insert spe1;
        
        DomainsOfExpertiseCatalog__c doec = new DomainsOfExpertiseCatalog__c();
        doec.name = 'DE0001';
        doec.DomainsOfExpertiseName__c = 'Expertise 1';
        doec.Active__c = true;
        insert doec;
        
        DomainsOfExpertiseCatalog__c doec2 = new DomainsOfExpertiseCatalog__c();
        doec2.name = 'DE0002';
        doec2.DomainsOfExpertiseName__c = 'Expertise 2';
        doec2.Active__c = true;
        insert doec2;
        
        DomainsOfExpertiseCatalog__c doec3 = new DomainsOfExpertiseCatalog__c();
        doec3.name = 'DE0003';
        doec3.DomainsOfExpertiseName__c = 'Expertise 3';
        doec3.Active__c = true;
        doec3.ParentDomainsOfExpertise__c = doec.id;
        insert doec3;
        
        DomainsOfExpertiseCatalog__c doec4 = new DomainsOfExpertiseCatalog__c();
        doec4.name = 'DE0004';
        doec4.DomainsOfExpertiseName__c = 'Expertise 4';
        doec4.Active__c = true;
        doec4.ParentDomainsOfExpertise__c = doec2.id;
        insert doec4;
        
        Apexpages.Standardcontroller cont1 = new Apexpages.Standardcontroller(spe1);
        Pagereference pg1 = Page.VFP_UpdateSpeDomainsOfExpertise;
        pg1.getParameters().put('type','Level1'); 
        pg1.getParameters().put('id',spe1.id); 
        Test.setCurrentPage(pg1);
                
        VFC_UpdateSpeDomainsOfExpertise rcp = new VFC_UpdateSpeDomainsOfExpertise(cont1);
        rcp.selDE = rcp.allDE;
        rcp.allDE = new List<SelectOption>();
        rcp.Save();
        
        rcp = new VFC_UpdateSpeDomainsOfExpertise(cont1);
        rcp.allDE = rcp.selDE ;
        rcp.selDE = new List<SelectOption>();
        rcp.Save();
        
        rcp.selDE = rcp.allDE;
        rcp.allDE = new List<SelectOption>();
        rcp.Save();
        
        //Testing Level 2
        pg1 = Page.VFP_UpdateSpeDomainsOfExpertise;
        pg1.getParameters().put('type','Level2'); 
        pg1.getParameters().put('id',spe1.id); 
        Test.setCurrentPage(pg1);
        Apexpages.Standardcontroller cont2 = new Apexpages.Standardcontroller(spe1);
        rcp = new VFC_UpdateSpeDomainsOfExpertise(cont2);
        rcp.selDE = rcp.allDE;
        rcp.allDE = new List<SelectOption>();
        rcp.Save();
        
        rcp = new VFC_UpdateSpeDomainsOfExpertise(cont1);
        rcp.allDE = rcp.selDE ;
        rcp.selDE = new List<SelectOption>();
        rcp.Save();
        
    }//End of method
    
}//End of class