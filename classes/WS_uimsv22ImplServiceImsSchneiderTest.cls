@isTest 
public class WS_uimsv22ImplServiceImsSchneiderTest{
 
      static testmethod void testUpdateUser(){
            try {
            WS_uimsv22ServiceImsSchneider.userV6 user6 = new WS_uimsv22ServiceImsSchneider.userV6() ; 
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort test = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            test.updateUser('callerFid','samlAssertion',user6); 
            }catch(exception e){}
        }
      static testmethod void testcreateIdentityWithPhoneId(){
            try {
            WS_uimsv22ServiceImsSchneider.userV6 user6 = new WS_uimsv22ServiceImsSchneider.userV6() ;
            WS_uimsv22ServiceImsSchneider.accessElement access = new WS_uimsv22ServiceImsSchneider.accessElement();
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort test = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            test.createIdentityWithPhoneId('callerFid',user6,access); 
            }catch(exception e){}
        }
        static testmethod void testcreateIdentityWithPassword(){
            try {
            WS_uimsv22ServiceImsSchneider.userV6 user6 = new WS_uimsv22ServiceImsSchneider.userV6() ;
            WS_uimsv22ServiceImsSchneider.accessElement access = new WS_uimsv22ServiceImsSchneider.accessElement();
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort test = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            test.createIdentityWithPassword('callerFid',user6,access,'password');
            }catch(exception e){}
        }
        static testmethod void testresetPasswordWithSms(){
            try {
            WS_uimsv22ServiceImsSchneider.userV6 user6 = new WS_uimsv22ServiceImsSchneider.userV6() ;
            WS_uimsv22ServiceImsSchneider.accessElement access = new WS_uimsv22ServiceImsSchneider.accessElement();
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort test = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            test.resetPasswordWithSms('callerFid','federatedId',access);
            }catch(exception e){}
        }
        static testmethod void testcreateIdentity(){
            try {
            WS_uimsv22ServiceImsSchneider.userV6 user6 = new WS_uimsv22ServiceImsSchneider.userV6() ;
            WS_uimsv22ServiceImsSchneider.accessElement access = new WS_uimsv22ServiceImsSchneider.accessElement();
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort test = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            test.createIdentity('callerFid',user6,access);
            }catch(exception e){}
        }
        static testmethod void testresetPassword(){
            try {
            WS_uimsv22ServiceImsSchneider.userV6 user6 = new WS_uimsv22ServiceImsSchneider.userV6() ;
            WS_uimsv22ServiceImsSchneider.accessElement access = new WS_uimsv22ServiceImsSchneider.accessElement();
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort test = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            test.resetPassword('callerFid','federatedId',access);
            }catch(exception e){}
        }
        static testmethod void testsearchUser(){
            try {
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort test = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            test.searchUser('callerFid','email');
            }catch(exception e){}
        }
          static testmethod void testreactivate(){
            try {
            WS_uimsv22ServiceImsSchneider.accessElement access = new WS_uimsv22ServiceImsSchneider.accessElement();
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort test = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            test.reactivate('callerFid','federatedId',access); 
            }catch(exception e){}
        }
        
          static testmethod void testsearchUserByPhoneId(){
            try {
            WS_uimsv22ServiceImsSchneider.accessElement access = new WS_uimsv22ServiceImsSchneider.accessElement();
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort test = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            test.searchUserByPhoneId('callerFid','phoneId');
            }catch(exception e){}
        }
        
        
          static testmethod void testgetUser(){
            try {
            WS_uimsv22ServiceImsSchneider.accessElement access = new WS_uimsv22ServiceImsSchneider.accessElement();
            WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort test = new WS_uimsv22ImplServiceImsSchneider.AuthenticatedUserManager_UIMSV22_ImplPort();
            test.getUser('callerFid','federatedId'); 
            }catch(exception e){}
        }
        static testmethod void allService(){
            WS_uimsv22ServiceImsSchneider.userV6 userV6  = new WS_uimsv22ServiceImsSchneider.userV6() ;
            WS_uimsv22ServiceImsSchneider.resetPasswordWithSmsResponse resetPasswordWithSmsResponse = new WS_uimsv22ServiceImsSchneider.resetPasswordWithSmsResponse() ;
            WS_uimsv22ServiceImsSchneider.ImsMailerException ImsMailerException = new WS_uimsv22ServiceImsSchneider.ImsMailerException() ;
            WS_uimsv22ServiceImsSchneider.getUserResponse getUserResponse = new WS_uimsv22ServiceImsSchneider.getUserResponse () ;
            WS_uimsv22ServiceImsSchneider.userV5 userV5 = new WS_uimsv22ServiceImsSchneider.userV5 () ;
            WS_uimsv22ServiceImsSchneider.resetPassword resetPassword = new WS_uimsv22ServiceImsSchneider.resetPassword () ;
            WS_uimsv22ServiceImsSchneider.createdIdentityReport createdIdentityReport = new WS_uimsv22ServiceImsSchneider.createdIdentityReport () ;
            WS_uimsv22ServiceImsSchneider.getUser getUser = new WS_uimsv22ServiceImsSchneider.getUser () ;
            WS_uimsv22ServiceImsSchneider.entry_element entry_element = new WS_uimsv22ServiceImsSchneider.entry_element () ;
            WS_uimsv22ServiceImsSchneider.InvalidImsServiceMethodArgumentException InvalidImsServiceMethodArgumentException = new WS_uimsv22ServiceImsSchneider.InvalidImsServiceMethodArgumentException () ;
            WS_uimsv22ServiceImsSchneider.resetPasswordWithSms resetPasswordWithSms = new WS_uimsv22ServiceImsSchneider.resetPasswordWithSms () ;
            WS_uimsv22ServiceImsSchneider.createIdentityResponse createIdentityResponse = new WS_uimsv22ServiceImsSchneider.createIdentityResponse () ;
            WS_uimsv22ServiceImsSchneider.RequestedInternalUserException RequestedInternalUserException = new WS_uimsv22ServiceImsSchneider.RequestedInternalUserException () ;
            WS_uimsv22ServiceImsSchneider.identity identity = new WS_uimsv22ServiceImsSchneider.identity () ;
            WS_uimsv22ServiceImsSchneider.searchUserByPhoneId searchUserByPhoneId = new WS_uimsv22ServiceImsSchneider.searchUserByPhoneId () ;
            WS_uimsv22ServiceImsSchneider.resetPasswordResponse resetPasswordResponse = new WS_uimsv22ServiceImsSchneider.resetPasswordResponse () ;
            WS_uimsv22ServiceImsSchneider.officialsIds officialsIds = new WS_uimsv22ServiceImsSchneider.officialsIds () ;
            WS_uimsv22ServiceImsSchneider.LdapTemplateNotReadyException LdapTemplateNotReadyException = new WS_uimsv22ServiceImsSchneider.LdapTemplateNotReadyException () ;
            WS_uimsv22ServiceImsSchneider.ids_element ids_element = new WS_uimsv22ServiceImsSchneider.ids_element () ;
            WS_uimsv22ServiceImsSchneider.createIdentityWithPhoneId createIdentityWithPhoneId = new WS_uimsv22ServiceImsSchneider.createIdentityWithPhoneId () ;
            WS_uimsv22ServiceImsSchneider.createIdentityWithPhoneIdResponse createIdentityWithPhoneIdResponse = new WS_uimsv22ServiceImsSchneider.createIdentityWithPhoneIdResponse () ;
            WS_uimsv22ServiceImsSchneider.updateUserResponse updateUserResponse = new WS_uimsv22ServiceImsSchneider.updateUserResponse () ;
            WS_uimsv22ServiceImsSchneider.SecuredImsException SecuredImsException = new WS_uimsv22ServiceImsSchneider.SecuredImsException () ;
            WS_uimsv22ServiceImsSchneider.createIdentityWithPasswordResponse createIdentityWithPasswordResponse = new WS_uimsv22ServiceImsSchneider.createIdentityWithPasswordResponse () ;
            WS_uimsv22ServiceImsSchneider.reactivate reactivate = new WS_uimsv22ServiceImsSchneider.reactivate () ;
            WS_uimsv22ServiceImsSchneider.RequestedEntryNotExistsException RequestedEntryNotExistsException = new WS_uimsv22ServiceImsSchneider.RequestedEntryNotExistsException () ;
            WS_uimsv22ServiceImsSchneider.UnexpectedLdapResponseException UnexpectedLdapResponseException = new WS_uimsv22ServiceImsSchneider.UnexpectedLdapResponseException () ;
            WS_uimsv22ServiceImsSchneider.updateUser updateUser = new WS_uimsv22ServiceImsSchneider.updateUser () ;
            WS_uimsv22ServiceImsSchneider.createIdentityWithPassword createIdentityWithPassword = new WS_uimsv22ServiceImsSchneider.createIdentityWithPassword () ;
            WS_uimsv22ServiceImsSchneider.IOException IOException = new WS_uimsv22ServiceImsSchneider.IOException () ;
            WS_uimsv22ServiceImsSchneider.childs_element childs_element = new WS_uimsv22ServiceImsSchneider.childs_element () ;
            WS_uimsv22ServiceImsSchneider.searchUser searchUser = new WS_uimsv22ServiceImsSchneider.searchUser () ;
            WS_uimsv22ServiceImsSchneider.searchUserResponse searchUserResponse = new WS_uimsv22ServiceImsSchneider.searchUserResponse () ;
            WS_uimsv22ServiceImsSchneider.IMSServiceSecurityCallNotAllowedException IMSServiceSecurityCallNotAllowedException = new WS_uimsv22ServiceImsSchneider.IMSServiceSecurityCallNotAllowedException () ;
            WS_uimsv22ServiceImsSchneider.InactiveUserImsException InactiveUserImsException = new WS_uimsv22ServiceImsSchneider.InactiveUserImsException () ;
            WS_uimsv22ServiceImsSchneider.searchUserByPhoneIdResponse searchUserByPhoneIdResponse = new WS_uimsv22ServiceImsSchneider.searchUserByPhoneIdResponse () ;
            WS_uimsv22ServiceImsSchneider.accessElement accessElement = new WS_uimsv22ServiceImsSchneider.accessElement () ;
            WS_uimsv22ServiceImsSchneider.userFederatedIdAndType userFederatedIdAndType = new WS_uimsv22ServiceImsSchneider.userFederatedIdAndType () ; 
            WS_uimsv22ServiceImsSchneider.InvalidImsPropertiesFileException InvalidImsPropertiesFileException = new WS_uimsv22ServiceImsSchneider.InvalidImsPropertiesFileException () ;
            WS_uimsv22ServiceImsSchneider.reactivateResponse reactivateResponse = new WS_uimsv22ServiceImsSchneider.reactivateResponse () ;
            WS_uimsv22ServiceImsSchneider.createIdentity createIdentity = new WS_uimsv22ServiceImsSchneider.createIdentity () ;
        }
        
        
        
}