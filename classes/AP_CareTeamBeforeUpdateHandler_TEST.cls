@isTest
private class AP_CareTeamBeforeUpdateHandler_TEST
{
    @isTest static void testCheckCareTeamInactive()
    {
        Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'India';
        insert ct;
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
        insert acc1;
        Contact con1 = Utils_TestMethods.createContact(acc1.Id,'TestContact');
        insert con1;
        BusinessRiskEscalationEntity__c objPOCOrganization = Utils_TestMethods.createBRE('USAPOCEntity','USAPOCSubEntity','USAPOCLocation','Customer Care Center');
        insert objPOCOrganization; 

        
        CustomerCareTeam__c AgentAPrimaryTeam = new CustomerCareTeam__c(CCCountry__c=ct.ID, Name='Agent A Primary Team',LevelOfSupport__c='Primary');
        insert AgentAPrimaryTeam;
        
        Case case1 = Utils_TestMethods.createCase(acc1.Id, con1.Id, 'Open');
        case1.Team__c = AgentAPrimaryTeam.id;
        insert case1;          
            
        PointOfContact__c poc1 = Utils_TestMethods.createPointOfContact(objPOCOrganization.Id, ct.Id, 'testcustomercare@bridge-fo.com','Email');
        poc1.Priority1Routing__c = 'Account Preferred Team';
        poc1.DefaultRoutingTeam__c = AgentAPrimaryTeam.id;
        insert poc1;
        
        TeamAgentMapping__c teamMem1 = new TeamAgentMapping__c (CCAgent__c = UserInfo.getUserId(), CCTeam__c = AgentAPrimaryTeam.Id, DefaultTeam__c = true);
        insert teamMem1;
        
        test.startTest();
        
        AgentAPrimaryTeam.Inactive__c = true;
        
        System.debug('-----Update CC Team Check Starts-----');
        try
        {
            update AgentAPrimaryTeam;
        }
        catch(Exception e)
        {
            String excep;
            System.debug('----->>>>>'+e.getMessage());
            if(e.getMessage().contains(Label.CLOCT15CCC02+' '+Label.CLOCT15CCC03+' '+Label.CLOCT15CCC05+' '+Label.CLOCT15CCC04+' '+Label.CLOCT15CCC06))
            {
                excep = 'YES';
            }
            system.assertEquals(excep, 'YES');
        }
        System.debug('-----Update CC Team Check Ends-----');
        
        test.stopTest();
    }
}