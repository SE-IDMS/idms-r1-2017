/**
@Author: Prashanth GS
Description: This covers IdmsSendInvitationController class
Release: Sept 2016
Test class of IdmsSendInvitation controller
**/

@isTest
class IdmsSendInvitationControllerTest{
    
    //Test method
    static testmethod void testIdmsSendInvitationController(){
        IdmsTermsAndConditionMapping__c custSets=new IdmsTermsAndConditionMapping__c(Name='IDMS',
                                                                                     url__c='http://www2.schneider-electric.com/sites/corporate/en/general/legal-information/terms-of-use.page');
        insert custSets;
        ApexPages.currentPage().getParameters().put('app','IDMS');
        IdmsSendInvitationController controller=new IdmsSendInvitationController();
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.getCompanyPicklist();
        controller.email='idmstesttoto@accenture.com';
        controller.nextToCompany();
        controller.reset();
        controller.submit();
        controller.submitHome();
        controller.verify();
        ApexPages.currentPage().getParameters().put(Label.CLJUN16IDMS27,'True');
        ApexPages.currentPage().getParameters().put(Label.CLJUN16IDMS28,'True');
        controller.submitHome();
        controller.verify();
        String strPublicKey=controller.publicKey;
        controller.showCaptcha=false;
        controller.submitHome();
        IdmsSendInvitationController.generateRandomString(10);
        controller.email='12345test';
        controller.submitHome(); 
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.showCaptcha=true;
        controller.submit();
        controller.backToWork();
        controller.email='p@gmail.com';
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.MarketSegment = 'gsgdg';
        controller.MarketSubSegmen = 'dufs';
        controller.CompanyHeadquarters = true;
        controller.smsIdentity = true;
        controller.showCaptcha=false;
        controller.submitHome();
        controller.MobilePhone='709698665';
        controller.mobCntryCode='+33';
        controller.showCaptcha=false;
        controller.submit();
        controller.backToWork();
        controller.email='p6666@gmail.com';
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.Fax= '6666666';
        controller.password= 'Welcome1';
        controller.cpassword='Welcome1';
        controller.reg1Check= 'skfzekfj';
        controller.checked= true;
        controller.showCaptcha=false;
        controller.submitHome();
        controller.email='p@gmail.com';
        controller.MobilePhone='7096985685';
        controller.mobCntryCode='+91';
        controller.showCaptcha=true;
        controller.submitHome(); 
        controller.mobCntryCode='+91';
        controller.showCaptcha=false;
        controller.workCntryCode = 'fr';
        controller.companyCounty = 'fr';
        controller.Fax = '667777777';
        controller.MiddleName = 'tzayaz';
        controller.Company_Website = 'www.se.com';
        controller.submit();
        controller.backToWork();
        IDMSApplicationMapping__c idmsmap = new IDMSApplicationMapping__c();
        idmsmap.EmailLinkInvitation__c = 'www.schnider.com';
        idmsmap.AppName__c = 'appIddummy';
        idmsmap.context__c = 'work';
        idmsmap.Name = 'idmsapp';    
        insert idmsmap;
        String dummyAccount = Label.CLNOV16IDMS063;
        
        string Id = [SELECT id from RecordType where Name ='Business Account'].Id;
        Country__c obj = new Country__c();
        obj.name= 'France';
        obj.CountryCode__c = 'fr';
        insert obj;
        
        Contact invitedContact = new Contact(firstName='firstname',lastname='lastname', email = 'abcd777888@accenture.com', WorkPhone__c = '999887665544');
        IDMS_Send_Invitation__c objSendInvitation= new IDMS_Send_Invitation__c(FirstName__c='firstname', LastName__c='lastname', Email__c='semocktest99@accenture.com');
        insert objSendInvitation;
        
        System.assert(controller.statusOptionsCountry.size()>0);
        IdmsSendInvitationController.generateRandomString(10);
        controller.backToWork();
    }
}