/*
*   Created By: 
*   Created Date: 01/06/2016
*   Modified by:  01/12/2016 
*   Below class and method are generated from WSDL
* */
public class WS_IDMSUIMSCompanyServiceImpl {
    public class AuthenticatedCompanyManager_UIMSV2_ImplPort {
        public String endpoint_x = 'https://ims-int.btsec.dev.schneider-electric.com/IMS-CompanyManager/UIMSV2/2WAuthenticated-CompanyManagement';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://uimsv2.impl.service.ims.schneider.com/', 'WS_IDMSUIMSCompanyService', 'http://uimsv2.service.ims.schneider.com/', 'WS_IDMSUIMSCompanyService'};
            //Below Method used to search public company in UIMS
            public WS_IDMSUIMSCompanyService.companyV3[] searchPublicCompany(String callerFid,WS_IDMSUIMSCompanyService.companyV3 template) {
                WS_IDMSUIMSCompanyService.searchPublicCompany request_x = new WS_IDMSUIMSCompanyService.searchPublicCompany();
                request_x.callerFid = callerFid;
                request_x.template = template;
                WS_IDMSUIMSCompanyService.searchPublicCompanyResponse response_x;
                Map<String, WS_IDMSUIMSCompanyService.searchPublicCompanyResponse> response_map_x = new Map<String, WS_IDMSUIMSCompanyService.searchPublicCompanyResponse>();
                response_map_x.put('response_x', response_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        '',
                        'http://uimsv2.service.ims.schneider.com/',
                        'searchPublicCompany',
                        'http://uimsv2.service.ims.schneider.com/',
                        'searchPublicCompanyResponse',
                        'WS_IDMSUIMSCompanyService.searchPublicCompanyResponse'}
                );
                response_x = response_map_x.get('response_x');
                return response_x.companies;
            }
        // Below Method used to create company in UIMS
        public String createCompany(String callerFid,String federatedId,WS_IDMSUIMSCompanyService.companyV3 company) {
            WS_IDMSUIMSCompanyService.createCompany request_x = new WS_IDMSUIMSCompanyService.createCompany();
            request_x.callerFid = callerFid;
            request_x.federatedId = federatedId;
            request_x.company = company;
            WS_IDMSUIMSCompanyService.createCompanyResponse response_x;
            Map<String, WS_IDMSUIMSCompanyService.createCompanyResponse> response_map_x = new Map<String, WS_IDMSUIMSCompanyService.createCompanyResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                this,
                request_x,
                response_map_x,
                new String[]{endpoint_x,
                    '',
                    'http://uimsv2.service.ims.schneider.com/',
                    'createCompany',
                    'http://uimsv2.service.ims.schneider.com/',
                    'createCompanyResponse',
                    'WS_IDMSUIMSCompanyService.createCompanyResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.companyId;
        }
    }
}