/**************************************************************************************
    Author: Fielo Team
    Date: 13/04/2016
    Description: 
    Related Components: 
***************************************************************************************/

public without sharing class FieloPRM_AP_ComponentTriggers{
    
    /*Execution: Before update*/
    public static void topSectionCheckChangeLayout(List<FieloEE__Component__c> triggerNew, map<Id, FieloEE__Component__c> triggerOldMap){
        Id IdRTComponentTopSection = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'FieloPRM_C_TopSection'].Id;
        Set<Id> componentsChangingLayoutToSlider = new set<Id>();
        for(FieloEE__Component__c comp: triggerNew){
            //Si el campo Layout del componente cambia de "Slider" para "Grid" agrego el Id a un set
            if(comp.RecordTypeId == IdRTComponentTopSection && comp.FieloEE__Layout__c == 'Slider' && triggerOldMap.get(comp.Id).FieloEE__Layout__c == 'Grid'){
                componentsChangingLayoutToSlider.add(comp.Id);
            }
        }
        
        if(!componentsChangingLayoutToSlider.isEmpty()){
            set<Id> componentsWhitError = new set<Id>();
            //Consulto los registros de Content Feed asociados a los componentes guardados en el set "componentsChangingLayoutToSlider" que esten activos y tengan el campo Style igual a "Background Image"
            for(FieloEE__News__c cfeed: [SELECT Id, FieloEE__Component__c FROM FieloEE__News__c WHERE FieloEE__Component__c IN: componentsChangingLayoutToSlider AND FieloEE__IsActive__c = true AND F_PRM_Style__c != 'Background Image']){
                //Si existe algun registro de Content Feed con esas caracteristicas para esos componentes los agrego a un set para luego agregarles un error
                componentsWhitError.add(cfeed.FieloEE__Component__c);
            }
            if(!componentsWhitError.isEmpty()){
                for(FieloEE__Component__c comp: triggerNew){
                    if(componentsWhitError.contains(comp.Id)){
                        //Agrego un error a los registros de Component que tienen conflicto
                        //Label.CLAPR16PRM01: You cannot configure a Sliding Top Section component if it has active content feeds which style is different than "Top Section: Background Image + Welcome Message”
                        comp.addError(Label.CLAPR16PRM01);
                    }
                }
            }
        }
    }
    
    public static void removeCache(List<FieloEE__Component__c> components){        
        Set<Id> childSectionIds = new Set<Id>();
        for(FieloEE__Component__c component : components){      
            childSectionIds.add(component.FieloEE__Section__c);
        }
        
        //remove cache for Akamai and sections platform cache
        Set<Id> parentSectionIds = new Set<Id>();
        for(FieloEE__Section__c section : [SELECT FieloEE__Parent__c FROM FieloEE__Section__c WHERE Id in : childSectionIds]){
            parentSectionIds.add(section.FieloEE__Parent__c);
        }
        
        List<FieloEE__Section__c> parentSections = new List<FieloEE__Section__c>();
        for(Id sectionId : parentSectionIds){
            parentSections.add(new FieloEE__Section__c(Id = sectionId));
        }

        update parentSections;
    }
    
}