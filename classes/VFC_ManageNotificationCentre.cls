global class VFC_ManageNotificationCentre{ 
    
    global String notificationValue {get;set;}
    
    global VFC_ManageNotificationCentre(){
    
        notificationValue = ApexPages.currentPage().getParameters().get('notifi');
    }
    @RemoteAction
    global Static AP_PRMUtils.NotificationInfo searchNotificationAlerts(){
        
       
        system.debug('UserID:' + userInfo.getUserId());
        
        AP_PRMUtils.NotificationInfo wrapVar = AP_PRMUtils.GetNotifications(userInfo.getUserId());
        system.debug('wrapvar:' + wrapVar);
        return wrapVar;
    }
    @RemoteAction
    global static Boolean ModifyNotification(String notificationType, String notificationId){
        List<TrackSetupAlets__c> notification = new List<TrackSetupAlets__c>([SELECT Id,Deleted__c,Read__c FROM TrackSetupAlets__c WHERE Id = :notificationId]);
        System.debug('>>>>'+notification+'<<<<'+notificationtype);
        Boolean retValue = (string.isNotBlank(notificationType) && notificationType == 'delete')? notification[0].Deleted__c = true:(string.isNotBlank(notificationType) && notificationType == 'read')?notification[0].Read__c = true:false;
        System.debug('****'+retValue);
        if(retValue)
            update notification;
        return retValue;
    }    
}