@isTest
private class VFC25_AddUpdateProductforCase_TEST{

    static testMethod void VFC25_AddUpdateProductforCaseTest() {
    //Start of Test Data preparation
    
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;
               
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        case1.CommercialReference__c= 'Commercial\\sReference\\stest';
        case1.ProductBU__c = 'Business';
        case1.ProductLine__c = 'ProductLine__c';
        case1.ProductFamily__c ='ProductFamily';
        case1.Family__c ='Family';
        insert case1;

        ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(case1);   
        VFC25_AddUpdateProductforCase GMRSearchPage = new VFC25_AddUpdateProductforCase(CaseStandardController );
        
        VCC08_GMRSearch GMRSearchComponent = new VCC08_GMRSearch();
        GMRSearchComponent.pageController = GMRSearchPage;
        
        Test.startTest();
        GMRSearchPage.GMRsearchForCase.Init('TEST',GMRSearchComponent );
        GMRSearchComponent.actionNumber=1;
        GMRSearchPage.GMRsearchForCase.PerformAction(new DataTemplate__c(),GMRSearchComponent );
        GMRSearchComponent.actionNumber=2;
        GMRSearchPage.GMRsearchForCase.PerformAction(new DataTemplate__c(),GMRSearchComponent );
        GMRSearchPage.GMRsearchForCase.Cancel(GMRSearchComponent);   
        
        Test.stopTest();

    }
}