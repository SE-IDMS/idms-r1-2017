/*
Controller for VFP_AccountMasterProfileRedirection
*/
public class VFC_AccountMasterProfileRedirection {

    public Id accID{get;set;}
    public Id ampID{get;set;}        
    public VFC_AccountMasterProfileRedirection(ApexPages.StandardController controller) {}
    
    
    public PageReference doRedirection(){
        List<AccountMasterProfile__c> amplst= new List<AccountMasterProfile__c>();
        ampID=null;
        accID=(ID)System.currentPageReference().getParameters().get( 'accID' );
        
        //Added for view Page
        id amId=(ID)System.currentPageReference().getParameters().get( 'Id' );
        System.debug('>>>>RedirectionPage>>>'+amId+'>>>>'+accID);
        if(amId!=null && accID==null && !(String.valueOf(amId).startsWith('001')))
            amplst=[Select Account__c,AccountClassificationLevel1__c from AccountMasterProfile__c where Id=:amId];
        else 
            amplst=[Select Account__c,AccountClassificationLevel1__c from AccountMasterProfile__c where Account__c=:accID];
        if(amplst.size()>0){
            ampID=amplst[0].Id;
            accID=amplst[0].Account__c;
        }
        PageReference pr;
        if(ampID==null)
            pr = new PageReference(Site.getPathPrefix()+'/apex/VFP_createEditAccountMasterProfile?accID='+accId+'&ampID='+ampID); //new record
        else
            pr = new PageReference(Site.getPathPrefix()+'/apex/VFP_ViewAccountMasterProfile?accID='+accId+'&ampID='+ampID); // record already exists

       pr.setredirect(true); 
       return pr;
    }
}