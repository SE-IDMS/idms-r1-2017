global class WS_SaveAdvancedPA{
    
    global class AdvancedPADataBean{
        //DataBean to hold PartNumber details from mySE.
        /*
        webService String   ListPrice;
        webService DateTime PriceDate;
        webService Integer  Quantity;
        webService String   CatalogNumber;
        */
        webService String   PartNumber;
        webService AvailabilityDataBean[] availabilityDataBeanList;
        webService Integer  QuantityAsked ;
        webService String   UnitNetPrice; 
        webService Integer  MinimumOrderQuantiy;
        webService String   TotalNetPrice;
        webService Integer  NetPriceUnit;
        webService String   NetPriceCurrency;     
    }
    global class AvailabilityDataBean{
        //DataBean to hold the availability Information for each PartNumber
        webService DateTime AvailableDate;
        webService Integer  PromiseQuantity;
    }
    global class ReturnDataBean{
        //DataBean to hold the Response to mySE
        webService String returnCode;
        webService String returnMessage;
    }
    
    global class PARequestDataBean{
        //DataBean to hold the request from mySE
        webService AdvancedPADataBean[] advancedPADataBeanList;
        webService String CaseID;
        webService String bFOAccountID;
    }
    
    
    webService static returnDataBean SaveAdvancedPA(PARequestDataBean objPARequestDataBean){
        //*********************************************************************************
        // Method Name : SaveAdvancedPA
        // Purpose : To insert Multiple P&A request from mySE to bFO External Reference
        //            Add more fields to the P&A save method
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 10th June 2014
        // Modified by :
        // Date Modified :
        // Remarks : For July - 2014 Release  
        // BR Details: BR-2315, BR-2141
        ///*********************************************************************************/
        
        // Logic to Update P&A to Exteral Reference Object
        //Return Status to MySE
        List<CSE_ExternalReferences__c> lstExternalReference = new List<CSE_ExternalReferences__c>();
        if(objPARequestDataBean!=null && objPARequestDataBean.advancedPADataBeanList!=null && objPARequestDataBean.advancedPADataBeanList.size()>0){
            for(AdvancedPADataBean objAdvancedPADataBean : objPARequestDataBean.advancedPADataBeanList){
                CSE_ExternalReferences__c ExternalReference = new CSE_ExternalReferences__c();
                ExternalReference.Case__c = objPARequestDataBean.CaseID;  
                ExternalReference.Type__c = 'P&A';
                
                String delimiter = ': ';
                String newline = '\n';
                String commaSeperator =' , ';
                String LineSeperator ='-------------';
                
                ExternalReference.Description__c ='';
                ExternalReference.Description__c += 'Part No.' + delimiter + objAdvancedPADataBean.PartNumber; 
                ExternalReference.Description__c += newline + 'Quantity Asked' + delimiter + objAdvancedPADataBean.QuantityAsked;
                ExternalReference.Description__c += newline + 'Unit Net Price' + delimiter + objAdvancedPADataBean.UnitNetPrice;
                ExternalReference.Description__c += newline + 'Minimum Order Quantiy' + delimiter + objAdvancedPADataBean.MinimumOrderQuantiy; 
                ExternalReference.Description__c += newline + 'Total Net Price' + delimiter + objAdvancedPADataBean.TotalNetPrice;
                ExternalReference.Description__c += newline + 'Net Price Unit' + delimiter + objAdvancedPADataBean.NetPriceUnit;
                ExternalReference.Description__c += newline + 'Net Price Currency' + delimiter + objAdvancedPADataBean.NetPriceCurrency;

                if(objAdvancedPADataBean!=null && objAdvancedPADataBean.availabilityDataBeanList!=null && objAdvancedPADataBean.availabilityDataBeanList.size()>0){
                    ExternalReference.Description__c += newline  + LineSeperator ;
                    for(AvailabilityDataBean objAvailabilityDataBean: objAdvancedPADataBean.availabilityDataBeanList){
                        ExternalReference.Description__c += newline + 'Available Date' + delimiter + objAvailabilityDataBean.AvailableDate; 
                        ExternalReference.Description__c += commaSeperator + 'Promise Quantity' + delimiter + objAvailabilityDataBean.PromiseQuantity; 
                    }
                }
                
                ExternalReference.Description__c = Replace(ExternalReference.Description__c,'TRY','YTL');
                ExternalReference.Description__c = Replace(ExternalReference.Description__c,'EUR','&#8364;');
                ExternalReference.Description__c = Replace(ExternalReference.Description__c,'USD','&#36;');
                
                lstExternalReference.add(ExternalReference);
            }
        }
        returnDataBean objReturnDataBean = new returnDataBean();
        objReturnDataBean.returnCode='E';
        objReturnDataBean.returnMessage='';
        
        try{
            Database.DMLOptions DMLoption = new Database.DMLOptions(); 
            DMLoption.allowFieldTruncation = false;
            DMLoption.optAllOrNone = true;      
            Database.SaveResult[] ExtrnlRefInsertResult = Database.insert(lstExternalReference, DMLoption);
            for(Database.SaveResult objSaveResult: ExtrnlRefInsertResult){
                if(objSaveResult.issuccess())
                    objReturnDataBean.returnCode = 'S';
                else
                    objReturnDataBean.returnMessage='Save P&A has failed';
            }
        }
        catch(Exception e){
            objReturnDataBean.returnMessage=e.getmessage();     
        }
        return objReturnDataBean;
    }
    
    public static String Replace(String StringToHandle, String ReplacingValue, String ValueToBeReplaced){
        List<String> ExtRefSplit =  StringToHandle.split(ValueToBeReplaced,-1);
        if(ExtRefSplit.size()>1){
            StringToHandle = ExtRefSplit[0];
            StringToHandle += ReplacingValue;
            StringToHandle += ExtRefSplit[1];
        } 
        return StringToHandle;
    }
}