/*     
@Author: Deepak
Created Date: 19-12-2013
Description: Test class for AP_CaseLine .
**********
*/
@isTest(SeeAllData=true)
Public Class AP_CaseLine_test
{
    static testMethod void Test() 
    {
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        
        Account testAccount = Utils_TestMethods.createAccount();
        testAccount.Country__c = testCountry.Id;
        insert testAccount;
        
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id,'testContact');
        testContact.Country__c = testCountry.Id;
        insert testContact;
        
        Case testCase = Utils_TestMethods.createCase(testAccount.Id,testContact.Id,'Open');
        insert testCase;
        
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = testAccount.id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = testAccount.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId1';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        ip1.DeviceTypeToCreate__c = 'device1';
        insert ip1;
        
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = testAccount.id;
        ip1.Name = 'Test Intalled Product22 ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId1';
        ip1.BrandToCreate__c ='Test Brand2';
        ip1.SVMXC__Site__c = site1.id;
        ip1.DeviceTypeToCreate__c = 'device12';
        insert ip2 ;
        
        SVMXC__Case_Line__c cl = New SVMXC__Case_Line__c();
        cl.SVMXC__Case__c = testCase.Id;
        cl.SVMXC__Installed_Product__c = ip1.Id;
        insert cl;
        update cl;
        
        List<SVMXC__Case_Line__c> cline = New List<SVMXC__Case_Line__c>();
        cline.add(cl);
        AP_CaseLine.CLValidation(cline);
        
      /*  cl.SVMXC__Case__c = testCase.Id;
        cl.SVMXC__Installed_Product__c = ip2.Id;
        update cl;
        List<SVMXC__Case_Line__c> cline2 = New List<SVMXC__Case_Line__c>();
        cline2.add(cl);
        AP_CaseLine.CLValidation(cline2);
            */
    }
}