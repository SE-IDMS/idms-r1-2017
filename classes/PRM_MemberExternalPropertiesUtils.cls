public class PRM_MemberExternalPropertiesUtils {
    /*
    private static boolean isDescribeDone =false;
    private static Set<String> validTypeList;
    private static Set<String> validSubTypeList;

    private static void fillValidTypeAndValidSubType(){
        Schema.DescribeFieldResult typeDescribe = MemberExternalProperties__c.Type__c.getDescribe();
        validTypeList = new Set<String>();
        for(Schema.PicklistEntry typeVal : typeDescribe.getPicklistValues())
        {
            validTypeList.add(typeVal.getValue());
        }

        //SubType
        Schema.DescribeFieldResult subTypeDescribe = MemberExternalProperties__c.SubType__c.getDescribe();
        validSubTypeList = new Set<String>();
        for(Schema.PicklistEntry subTypeVal : subTypeDescribe.getPicklistValues())
        {
            validSubTypeList.add(subTypeVal.getValue());
        }
        isDescribeDone=true;
    }

    private static boolean validateType(String value){
        if(!isDescribeDone){
            fillValidTypeAndValidSubType();
        }
        System.debug('##valueToValidate: '+value);
        System.debug('##validTypeList: '+validTypeList);
        return validTypeList.contains(value);
    }

    private static boolean validateSubType(String value){
        if(!isDescribeDone){
            fillValidTypeAndValidSubType();
        }
        System.debug('##subValueToValidate: '+value);
        System.debug('##validSubTypeList: '+validSubTypeList);
        System.debug('##validSubTypeList.contains(value): '+validSubTypeList.contains(value));
        return validSubTypeList.contains(value);
    }*/
    
    public static void removeMemberExternalPropertyByPRMUIMId(String source,String type, Map<String ,Set<String>> subtypeByContact){  
        List<MemberExternalProperties__c> toDeleteList = new List<MemberExternalProperties__c>();  
        for(MemberExternalProperties__c memberExternalProperty : [SELECT Id,PRMUIMSID__c,Type__c,ExternalPropertySubType__c from MemberExternalProperties__c where PRMUIMSID__c IN: subtypeByContact.KeySet() and (ExternalPropertiesCatalog__r.ExternalPropertiesCatalogType__r.Type__c=:type and ExternalPropertiesCatalog__r.ExternalPropertiesCatalogType__r.Source__c=:source)]){
            Set<String> subTypeSet = subtypeByContact.get(memberExternalProperty.PRMUIMSID__c);
            if(subTypeSet.contains(memberExternalProperty.ExternalPropertySubType__c)){
                toDeleteList.add(memberExternalProperty);
            }
        }
        delete toDeleteList;
    }

    public static void addMemberExternalPropertyByPRMUIMId(String source,String type, Map<String ,Set<String>> badgesByContact){    

        List<MemberExternalProperties__c> memberExternalPropertiesList = new List<MemberExternalProperties__c>();
        Map<String,Id> memberByPRMUIMSId = new Map<String,Id>();
        for(Contact con: [SELECT id, PRMUIMSID__c,FieloEE__member__c FROM contact where PRMUIMSID__c in:badgesByContact.keySet()]){
            memberByPRMUIMSId.put(con.PRMUIMSID__c,con.FieloEE__member__c);
        }
        
        for(String key:badgesByContact.keySet()){
            for(String subType:badgesByContact.get(key)){
                    MemberExternalProperties__c myMemExtProp = new MemberExternalProperties__c();
                    myMemExtProp.Member__c = memberByPRMUIMSId.get(key);
                    myMemExtProp.PRMUIMSId__c = key;
                    myMemExtProp.FunctionalKey__c = source+type+subType+key;
                    myMemExtProp.ExternalPropertiesCatalog__r = new ExternalPropertiesCatalog__c(FunctionalKey__c=source+type+subType);
                    memberExternalPropertiesList.add(myMemExtProp);
            }
        }
        Database.SaveResult[] lsr = Database.insert(memberExternalPropertiesList,false);
        System.debug('Insert MembExtProp: '+lsr);
    }


    public static void addMemberInternalPropertyByPRMUIMId(Map<Contact ,Set<PRMBadgeBFOProperties__c>> prmbadgesByContact){
        List<MemberExternalProperties__c> memberExternalPropertiesList = new List<MemberExternalProperties__c>();
        for(Contact con:prmbadgesByContact.keySet()){
            for(PRMBadgeBFOProperties__c prmBadgeBFOProperties:prmbadgesByContact.get(con)){
                       MemberExternalProperties__c myMemExtProp = new MemberExternalProperties__c();
                        myMemExtProp.DeletionFlag__c=false;
                        myMemExtProp.Member__c = con.FieloEE__Member__c;
                        myMemExtProp.PRMUIMSId__c = con.PRMUIMSId__c;
                        myMemExtProp.PRMBadgeBFOProperties__c = prmBadgeBFOProperties.Id;
                        myMemExtProp.FunctionalKey__c = PartnerLocatorPushEventsService.PICKLIST_BFO_PROP+prmBadgeBFOProperties.SubType__c+con.PRMUIMSId__c;
                        myMemExtProp.ExternalPropertiesCatalog__r = new ExternalPropertiesCatalog__c(FunctionalKey__c=PartnerLocatorPushEventsService.PICKLIST_BFO_PROP+prmBadgeBFOProperties.SubType__c);
                        memberExternalPropertiesList.add(myMemExtProp);
            }
        }
        Database.upsert(memberExternalPropertiesList, Schema.MemberExternalProperties__c.Fields.FunctionalKey__c, false);
        //Database.insert(memberExternalPropertiesList,false);
    }

    public static void removeMemberInternalPropertyByPRMUIMId(Map<Contact ,Set<PRMBadgeBFOProperties__c>> prmbadgesByContact){
        Map<String,Set<Id>> badgeByContact = new Map<String,Set<Id>>();
        List<Id> allPRMBadgeBFOPropertiesId = new List<Id>();
        for(Contact con: prmbadgesByContact.keySet()){
            Set<Id> idSet = new Set<Id>();
            for(PRMBadgeBFOProperties__c prmBadge: prmbadgesByContact.get(con)){
                idSet.add(prmBadge.Id);
            }
            allPRMBadgeBFOPropertiesId.addAll(idSet);
            badgeByContact.put(con.PRMUIMSID__c,idSet);
        }
        List<MemberExternalProperties__c> toDeleteList = new List<MemberExternalProperties__c>();  
        for(MemberExternalProperties__c memberExternalProperty : [SELECT Id,PRMUIMSID__c,Type__c,ExternalPropertySubType__c,PRMBadgeBFOProperties__c from MemberExternalProperties__c where PRMUIMSID__c IN: badgeByContact.KeySet() and PRMBadgeBFOProperties__c in:allPRMBadgeBFOPropertiesId and ExternalPropertiesCatalog__r.RecordTypeId=:System.label.CLJUN16PRM057]){
            Set<Id> subTypeSet = badgeByContact.get(memberExternalProperty.PRMUIMSID__c);
            if(subTypeSet.contains(memberExternalProperty.PRMBadgeBFOProperties__c)){
                memberExternalProperty.DeletionFlag__c = true;
                toDeleteList.add(memberExternalProperty);
            }
        }
        update toDeleteList;
        //delete toDeleteList;
    }


     public static void addMemberExternalPropertyByContactId(String source,String type, Map<Id ,Set<String>> badgesByContact){
        List<MemberExternalProperties__c> memberExternalPropertiesList = new List<MemberExternalProperties__c>();
        for(Contact con: [select id,PRMUIMSId__c,FieloEE__Member__c from contact where id in:badgesByContact.keySet()]){
            for(String subType:badgesByContact.get(con.Id)){
                    MemberExternalProperties__c myMemExtProp = new MemberExternalProperties__c();
                    myMemExtProp.Member__c = con.FieloEE__Member__c;
                    myMemExtProp.PRMUIMSId__c = con.PRMUIMSId__c;
                    myMemExtProp.FunctionalKey__c = source+type+subType+con.PRMUIMSId__c;
                    myMemExtProp.ExternalPropertiesCatalog__r = new ExternalPropertiesCatalog__c(FunctionalKey__c=source+type+subType);
                    memberExternalPropertiesList.add(myMemExtProp);
            }
        }
        Database.SaveResult[] lsr = Database.insert(memberExternalPropertiesList,false);
        System.debug('Insert MembExtProp: '+lsr);
    }

    public static void removeMemberExternalPropertyByContactId(String source,String type, Map<Id ,Set<String>> subtypeByContact){  
        Map<String,Id> contactIdByPRMUIMSID = new Map<String,Id>();
        for(Contact con:[select id,PRMUIMSId__c from Contact where id in:subtypeByContact.keySet() and PRMUIMSId__c!=null]){
            contactIdByPRMUIMSID.put(con.PRMUIMSId__c,con.Id);
        }
        List<MemberExternalProperties__c> toDeleteList = new List<MemberExternalProperties__c>();  
        for(MemberExternalProperties__c memberExternalProperty : [SELECT Id,PRMUIMSID__c,Type__c,ExternalPropertySubType__c from MemberExternalProperties__c where PRMUIMSID__c IN: contactIdByPRMUIMSID.keySet() and (ExternalPropertiesCatalog__r.ExternalPropertiesCatalogType__r.Type__c=:type and ExternalPropertiesCatalog__r.ExternalPropertiesCatalogType__r.Source__c=:source)]){
            Set<String> subTypeSet = subtypeByContact.get(contactIdByPRMUIMSID.get(memberExternalProperty.PRMUIMSID__c));
            if(subTypeSet.contains(memberExternalProperty.ExternalPropertySubType__c)){
                toDeleteList.add(memberExternalProperty);
            }
        }
        delete toDeleteList;
    }

    public class InvalidExternalPropertiesCatalogException extends Exception{

    }

}