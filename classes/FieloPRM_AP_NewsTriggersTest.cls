/********************************************************************
* Company: Fielo
* Created Date: 29/11/2016
* Description: Test class for class FieloPRM_AP_NewsTriggers
********************************************************************/

@isTest
public with sharing class FieloPRM_AP_NewsTriggersTest{
  
    public static testMethod void testUnit1(){
    	FieloEE.MockUpFactory.setCustomProperties(false);

    	FieloEE__PublicSettings__c ps = new FieloEE__PublicSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), CacheEnabled__c = true);
    	insert ps;

    	FieloEE__Menu__c menu1 = new FieloEE__Menu__c(
            Name = 'test menu 1', 
            FieloEE__Home__c = true, 
            FieloEE__Visibility__c ='Both', 
            FieloEE__Placement__c = 'Main',
            FieloEE__ExternalName__c = 'menu1' + datetime.now()
        );
        insert menu1;

        FieloEE__Section__c section1 = new FieloEE__Section__c(
            FieloEE__Menu__c = menu1.Id, 
            FieloEE__Type__c = '12'
        );
        insert section1;

        FieloEE__Section__c section2 = new FieloEE__Section__c(
            FieloEE__Menu__c = menu1.Id, 
            FieloEE__Type__c = '12', 
            FieloEE__Parent__c = section1.Id
        );
        insert section2;
        
        FieloEE__Component__c compNews1 = new FieloEE__Component__c(
            FieloEE__Section__c = section2.Id, 
            FieloEE__Menu__c = menu1.Id
        );
        
        compNews1.FieloEE__Mobile__c = true;
        compNews1.FieloEE__Tablet__c = false;
        compNews1.FieloEE__Desktop__c = false;
        insert compNews1;

        FieloEE__News__c news1 = new FieloEE__News__c(
            FieloEE__Type__c = 'test', 
            FieloEE__isActive__c = true, 
            FieloEE__Component__c = compNews1.Id
        );
        insert news1;

        delete news1;
    }
}