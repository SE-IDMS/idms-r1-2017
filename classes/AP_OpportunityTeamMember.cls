/*
    01-Apr-2013 Srinivas Nallapati  PRM Apr13   DEF-1462
*/
public class AP_OpportunityTeamMember{

    public static void checkOpportunityPartnerInvolvement(List<OpportunityTeamMember> lsOTM)
    {
        set<id> opptyIdsToCheck = new set<id>();
        set<Id> memberUserIds = new set<id>();
        for(OpportunityTeamMember otm : lsOTM)
        {
            OpptyIdsToCheck.add(otm.OpportunityId);
            memberUserIds.add(otm.UserId);
        }
        Map<Id, User> mpUser = new Map<Id, User>([select name, IsActive, ContactId, ProfileId  from User where id IN :memberUserIds]);
        Map<id, Opportunity> mpOppty = new Map<id, Opportunity>([select name,(select name, Contact__C, ContactRole__c from Value_Chain_Players__r where Contact__c != null and ContactRole__c = :System.Label.CLMAY13PRM40) ,PartnerInvolvement__c from Opportunity where id IN :OpptyIdsToCheck and PartnerInvolvement__c=:System.Label.CLMAY13PRM41]);

        for(OpportunityTeamMember otm : lsOTM)
        {
            if(mpUser.containsKey(otm.UserId) && mpUser.get(otm.UserId).ContactId != null &&  mpUser.get(otm.UserId).ProfileId == System.Label.CLMAR13PRM03)
            {
                if(mpOppty.containsKey(otm.OpportunityId) && mpOppty.get(otm.OpportunityId).Value_Chain_Players__r.size() > 0)
                {
                    otm.TECH_PartnerInvolvedDate__c = System.now();
                }
            }
        }

    }
    
    // Anil Sistla - OCT13 PRM - Create task when Opportunity is passed to partner
    public static Task createTask(Opportunity op, Id userId) {
        Task newTask = new Task(
                OwnerId = userId,
                Subject = 'Opportunity ' + op.Name +' '+ System.Label.CLOCT13PRM08,
                Description = System.Label.CLOCT13PRM09,
                RecordTypeId = System.Label.CLOCT13PRM07,
                TECH_OpportunityId__c = op.Id);
        return newTask;
    }
    
    public static void updateTECH_WorkingPartner(List<OpportunityTeamMember> lstOTMOld,List<OpportunityTeamMember> lsOTM) {
  
        set<Id> memberUserIds = new set<id>();
        set<Id> oldmemberUserIds = new set<Id>();
        
        List<Opportunity> lOppty = new List<Opportunity>();
        for(OpportunityTeamMember otm : lsOTM) {
            memberUserIds.add(otm.UserId);
            system.debug('**Memeber Id**'+memberUserIds);
        }
        if(lstOTMOld != null && lstOTMOld.size() >0 ){
            for(OpportunityTeamMember om : lstOTMOld) {
                oldmemberUserIds.add(om.UserId);
            }
        }
        
        Map<Id, User> mpUser = new Map<Id, User>([select name, LastName, IsActive, ContactId, ProfileId  FROM User WHERE id IN :memberUserIds]);
        
        
        Map<Id, User> oldmpUser = new Map<Id,User>([select name, LastName, IsActive, ContactId, ProfileId  FROM User WHERE id IN :oldmemberUserIds]);
        
        for(OpportunityTeamMember otm : lsOTM) {
            system.debug('**OTM**'+otm);
            
            /*system.debug('**ContainsKey**'+mpUser.containsKey(otm.UserId));
            system.debug('**ContainsKey**'+mpUser.get(otm.UserId).ContactId != null);
            system.debug('**Profile**'+String.valueOf(mpUser.get(otm.UserId).ProfileId).contains(System.Label.CLMAR13PRM03));
            system.debug('**ContainsKey**'+otm.TeamMemberRole == System.Label.CLOCT13SLS20);*/
            
            if(mpUser.containsKey(otm.UserId) && mpUser.get(otm.UserId).ContactId != null &&  String.valueOf(mpUser.get(otm.UserId).ProfileId).contains(System.Label.CLMAR13PRM03)  && otm.TeamMemberRole == System.Label.CLOCT13PRM13) { // Channel Partner
                if(oldmpUser.keyset().size() > 0){
                    if(oldmpUser.get(otm.UserId).LastName != mpUser.get(otm.UserId).LastName ) {
                        Opportunity tOpp = new Opportunity (Id = otm.OpportunityId, TECH_WorkingPartner__c = mpUser.get(otm.UserId).LastName);
                        lOppty.add(tOpp);
                        system.debug('**lOppty**'+lOppty);
                    }
               }
            }
        } 
        if(lOppty != Null && lOppty.size()>0)
            UPDATE lOppty;
            
    }
}