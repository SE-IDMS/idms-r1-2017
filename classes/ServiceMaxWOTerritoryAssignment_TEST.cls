/**
* @author Adrian MODOLEA (adrian.modolea@servicemax.com)
* @date 03/02/2016 (dd/mm/yyyy)
* @description This class contains unit tests for validating the behavior of ServiceMaxWOTerritoryAssignment
*
* VERSION HISTORY 
* @author FULL_NAME (EMAIL)
* @date DD/MM/YYYY (dd/mm/yyyy)
* @description SHORT FUNCTIONAL DESCRIPTION
*/
@isTest
private class ServiceMaxWOTerritoryAssignment_TEST
{
	static testMethod void testApexClass()
	{ 
		System.debug('## >>>>> testing class ServiceMaxWOTerritoryAssignment_TEST start <<<<<');
        
		System.debug('## Prepare data..');

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        System.runAs(u) {
        	
		System.debug('## ServiceMax_TerritoryMapAttributes custom setting..');
		insert new ServiceMax_TerritoryMapAttributes__c(Name='Country', WO_Field_Name__c='SVMXC__Country__c');
		
		System.debug('## Territories..');
		SVMXC__Territory__c t1 = new SVMXC__Territory__c(Name='Territory 1');
		SVMXC__Territory__c t2 = new SVMXC__Territory__c(Name='Territory 2');
		insert new List<SVMXC__Territory__c> {t1, t2};
		
		System.debug('## Territory coverages..');
		SVMXC__Territory_Coverage__c tc1 = new SVMXC__Territory_Coverage__c(SVMXC__Territory__c=t1.Id, SVMXC__Type__c='Country', SVMXC__Value__c='France', SVMXC__Active__c=true);
		SVMXC__Territory_Coverage__c tc2 = new SVMXC__Territory_Coverage__c(SVMXC__Territory__c=t2.Id, SVMXC__Type__c='Country', SVMXC__Value__c='Argentina', SVMXC__Active__c=true);
		insert new List<SVMXC__Territory_Coverage__c> {tc1, tc2};
				
		System.debug('## Accounts..');
		Account account1 = Utils_TestMethods.createAccount();
		account1.Name = 'Testaccount1'; 
		account1.BillingCity = 'city';
		account1.BillingCountry = 'RO';
		account1.BillingStreet = 'street';
		insert account1;
		
		System.debug('## Work orders..');
		List<SVMXC__Service_Order__c> workOrderList = new List<SVMXC__Service_Order__c>();
		SVMXC__Service_Order__c wo1 = Utils_TestMethods.createWorkOrder(account1.Id);
		SVMXC__Service_Order__c wo2 = Utils_TestMethods.createWorkOrder(account1.Id);
		wo1.SVMXC__Country__c='France';
		wo2.SVMXC__Country__c='France'; 

		Test.startTest();
        
        System.debug('## Insert trigger..');
        insert new List<SVMXC__Service_Order__c>{wo1, wo2}; 
        
        System.assertEquals(t1.Id, [SELECT SVMXC__Primary_Territory__c FROM SVMXC__Service_Order__c WHERE Id=:wo1.Id].SVMXC__Primary_Territory__c);
        System.assertEquals(t1.Id, [SELECT SVMXC__Primary_Territory__c FROM SVMXC__Service_Order__c WHERE Id=:wo2.Id].SVMXC__Primary_Territory__c);
        
        System.debug('## Update trigger..');
		wo1.SVMXC__Country__c = 'Argentina';
		wo2.SVMXC__Country__c = 'Argentina';
		ServiceMaxWOTerritoryAssignment.territoriesMap = null;
		update new List<SVMXC__Service_Order__c>{wo1, wo2};
		
		Test.stopTest();      
		System.assertEquals('Argentina', [SELECT SVMXC__Country__c FROM SVMXC__Service_Order__c WHERE Id=:wo1.Id].SVMXC__Country__c);
        System.assertEquals(t2.Id, [SELECT SVMXC__Primary_Territory__c FROM SVMXC__Service_Order__c WHERE Id=:wo1.Id].SVMXC__Primary_Territory__c);
        System.assertEquals(t2.Id, [SELECT SVMXC__Primary_Territory__c FROM SVMXC__Service_Order__c WHERE Id=:wo2.Id].SVMXC__Primary_Territory__c);
        }  

		System.debug('## >>>>> testing class ServiceMaxWOTerritoryAssignment_TEST end <<<<<');
	}        
}