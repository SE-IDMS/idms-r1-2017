/*****
Created By :Hanamanth
Comments   :To get the Shared Link of Box

*****/

Public class VFC_boxFolderSharedLink {

    public string shareUrl{get;set;}
    public Offer_Lifecycle__c offerObj{get;set;}
    private string offerId;
    public Milestone1_Project__c projectObj{get;set;}
    private string projectId;
    public boolean blnIsFolderId{get;set;}
    public boolean IsIframfdrId{get;set;}
    
    public VFC_boxFolderSharedLink(ApexPages.StandardController controller) { 
        blnIsFolderId=false;
        IsIframfdrId=false;
        system.debug('blnIsFolderId'+blnIsFolderId);
        system.debug('IsIframfdrId'+IsIframfdrId);
    }
    public void offerBoxSharelink() {
        blnIsFolderId=false;
        IsIframfdrId=false;
         shareUrl=label.CLAPR15ELLAbFO08;//'https://app.box.com/embed_widget/files/0/f/';
        offerId=ApexPages.currentPage().getParameters().get('Id');   
        offerObj= new Offer_Lifecycle__c();//Box_Shared_Link__c 
        offerObj=[select id,BoxFolderId__c,BoxSharedLink__c from Offer_Lifecycle__c where id=:offerId ];
        shareUrl+=offerObj.BoxFolderId__c;
        if(offerObj.BoxFolderId__c==null) {
                blnIsFolderId=true;
        }else {
            IsIframfdrId=true;
        
        }
       
    
    }
    public void projectBoxSharelink() {
          shareUrl=label.CLAPR15ELLAbFO08;//'https://app.box.com/embed_widget/files/0/f/';
        projectId=ApexPages.currentPage().getParameters().get('Id');   
        projectObj= new Milestone1_Project__c();//Box_Shared_Link__c 
        projectObj=[select id,BoxFolderId__c,BoxSharedLink__c from Milestone1_Project__c where id=:projectId ];
        shareUrl+=projectObj.BoxFolderId__c;
        system.debug('Offer'+projectObj);
    
    }
    
}