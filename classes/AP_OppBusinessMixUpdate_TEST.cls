/*
    Author: Pooja Gupta
    Date of Creation: 08-01-2013
    Description: Test Class for AP_OppBusinessMixUpdate which is utilized by Product Line Triggers

*/

@isTest

Private class AP_OppBusinessMixUpdate_TEST
{
    static testMethod void testPopulateOppBusinessMix()
    {
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        Country__c country= Utils_TestMethods.createCountry();
        country.Name='France';
        insert country; 
        
        //Check After insert trigger execution
        Opportunity opp = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp;
     
       CS_ProductLineBU__c PLBU = New CS_ProductLineBU__c(Name='BUILDING MANAGEMENT',ProductBU__c='Buildings');
       insert PLBU;
       
       OPP_ProductLine__c PL = Utils_TestMethods.createProductLine(opp.Id);
       Pl.ProductBU__c = 'BUILDING MANAGEMENT';
       insert PL;
       
       OPP_ProductLine__c PL1 = Utils_TestMethods.createProductLine(opp.Id);
       Pl1.ProductBU__c = 'BUILDING MANAGEMENT';
       insert PL1;
       
       delete PL;
      
       
    }

}