/*   Author: Nicolas PALITZYNE (Accenture)  
     Date Of Creation: 07/12/2011  
     Description : Static method to add a first Return Item to the RMA
*/

public class AP35_addItemToRMA
{
    public static void insertFirstReturnItem(List<RMA__c> aRMAList)
    {
        //*********************************************************************************
        // Method Name : insertFirstReturnItem
        // Purpose : To Insert first RMA Item
        // Created by : Ramesh Rajasekaran - Global Delivery Team
        // Date created : 24th February 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Apr - 12 Release
        ///*********************************************************************************/
        
        System.Debug('****AP35_addItemToRMA insertFirstReturnItem Started****'); 
        
        if(aRMAList != null && aRMAList.size()>0)
        {
            
            Map<ID,Case> relatedCasesMap = new Map<ID,Case>(); // Map of Cases with their ID
            Set<ID> CaseIDSet = new Set<ID>(); // Set of related Case IDs
            
            For(RMA__c aRMA:aRMAList)
            {
                if(aRMA.Case__c != null)
                {
                    CaseIDSet.add(aRMA.Case__c);
                }    
            }
            
            For(Case aCase:[SELECT ID, CommercialReference__c, Quantity__c, ProductFamily__c, SerialNumber__c, DateCode__c FROM Case WHERE ID IN :CaseIDSet])
            {
                relatedCasesMap.put(aCase.ID,aCase);
            }
            
            // Create the list of new return Products to be inserted
            List<RMA_Product__c> newReturnItems = new List<RMA_Product__c>();
            
            For(RMA__c aRMA:aRMAList)
            {
                if(   relatedCasesMap.get(aRMA.Case__c).CommercialReference__c != null
                   && relatedCasesMap.get(aRMA.Case__c).Quantity__c != null)
                {
                    RMA_Product__c newReturnItem = new RMA_Product__c(RMA__c=aRMA.ID);

                    newReturnItem.Name = relatedCasesMap.get(aRMA.Case__c).CommercialReference__c;
                    newReturnItem.Quantity__c = relatedCasesMap.get(aRMA.Case__c).Quantity__c;
                    newReturnItem.ProductFamily__c = relatedCasesMap.get(aRMA.Case__c).ProductFamily__c;

                    newReturnItem.Serialnumber__c = relatedCasesMap.get(aRMA.Case__c).SerialNumber__c;
                    newReturnItem.DateCode__c = relatedCasesMap.get(aRMA.Case__c).DateCode__c;

                    //Shipping Address is updated thanks to the BeforeInsert Trigger

                    newReturnItems.add(newReturnItem); 
                }
            }
            
            try
            {
                List<DataBase.SaveResult> SR = Database.insert(newReturnItems);
            }
            catch(Exception e)
            {
                System.debug('---- DML Exception: ' + e.getMessage());
            }          
        }
        
         System.Debug('****AP35_addItemToRMA insertFirstReturnItem Finished ****'); 
    }


    ////////////////////////////////////////////
    // Code for updating the Shipping Address //
    ////////////////////////////////////////////
    /*=======================================
      VARIABLES AND PROPERTIES
    =======================================*/
    public static Map<Id, RMA__c>                    RMAsMap = new Map<Id, RMA__c>();                     //RMAs related to Return Items
    public static Map<String, RMAShippingToAdmin__c> RSTMap  = new Map<String, RMAShippingToAdmin__c>();  //Set of RMA Shipping To Admin records

     /*=======================================
        PRIVATE METHODS
     =======================================*/ 
     /** This method is used to retrieve RMAs information **/
     public static Map<Id, RMA__c> getRMAsMap(List<RMA_Product__c> ReturnItems)
     {
        system.debug('>>>>> INFO: getRMAsMap(List<RMA_Product__c> ReturnItems) method begins');

        Map<Id, RMA__c> lRMAMap = new Map<Id, RMA__c>();  //RMAs Map to return

        try
        {
          //1 - Generate the set of RMA IDs of RMAs associated to Return Items
          Set<Id> lRMAIdSet = new Set<Id>();

          for(RMA_Product__c aReturnItem : ReturnItems)
          {
             lRMAIdSet.add(aReturnItem.RMA__c);
          }
          system.debug('>>>>> lRMAIdSet = ' + lRMAIdSet);

          //2 - Query RMAs
          List<RMA__c> lRMAs = [SELECT Case__r.CCCountry__r.CountryCode__c,ReturnType__c,ShippingStreet__c,ShippingAdditionalAddress__c,ShippingCity__c,ShippingZipCode__c,ShippingStateProvince__c,ShippingCounty__c,ShippingCountry__c,ShippingPOBox__c,ShippingPOBoxZip__c,ShippingStreetLocalLang__c,ShippingLocalAdditionalAddress__c,ShippingLocalCity__c,ShippingLocalCounty__c,Status__c FROM RMA__c WHERE Id IN :lRMAIdSet];
          system.debug('>>>>> lRMAs = ' + lRMAs);

          //3 - Put the ID and the RMA into a map
          for(RMA__c aRMA : lRMAs)
          {
             lRMAMap.put(aRMA.Id, aRMA);
          }
          system.debug('>>>>> lRMAMap = ' + lRMAMap);
        }
        catch(System.DmlException Ex)
        {
           system.debug('>>>>> ERROR in getRMAsMap(List<RMA_Product__c> ReturnItems) method: ' + Ex.getDmlMessage(0));
        }

        system.debug('>>>>> INFO: getRMAsMap(List<RMA_Product__c> ReturnItems) method ends');
        return lRMAMap;
     }

     /** This method is used to retrieve RMA Shipping To Admin records **/
     public static Map<String, RMAShippingToAdmin__c> getRSTMap(List<RMA_Product__c> ReturnItems)
     {
        system.debug('>>>>> INFO: getRSTMap(List<RMA_Product__c> ReturnItems) method begins');

        Map<String, RMAShippingToAdmin__c> lRSTMap = new Map<String, RMAShippingToAdmin__c>(); //RMA Shipping To Admin records to return

        try
        {
          //1 - Generate the set of RMA Shipping To Admin Keys
          Set<String> lRSTKeySet = new Set<String>();

          for(RMA_Product__c aReturnItem : ReturnItems)
          {
             String sCountry = null;
             String sReturnT = null;

             if (RMAsMap.get(aReturnItem.RMA__c) != null)
             {
                sCountry = RMAsMap.get(aReturnItem.RMA__c).Case__r.CCCountry__r.CountryCode__c;
                sReturnT = RMAsMap.get(aReturnItem.RMA__c).ReturnType__c;
             }

             if (sCountry != null)
             {
                //CCC Country is at least required to find records in the RMA Shipping To Admin table
                //Return Type can be filled in or empty in the RMA Shipping To Admin table (but required on the RMA)
                //Product Family can be filled in or empty in the RMA Shipping To Admin table and on the RMA
                //User Key in the RMA Shipping To Admin table is: CCC Country + '_' + Return Type + '_' + Product Family

                //First combination to find: CCC Country + Return Type + Product Family (full matching)
                lRSTKeySet.add(sCountry + '_' + sReturnT + '_' + aReturnItem.ProductFamily__c);

                //Second combination to find: CCC Country + <empty> + Product Family (to retrieve records when the Return Type doesn't match)
                lRSTKeySet.add(sCountry + '__' + aReturnItem.ProductFamily__c);

                //Third combination to find: CCC Country + Return Type + <empty> (to retrieve records when the Product Family doesn't match)
                lRSTKeySet.add(sCountry + '_' + sReturnT + '_');

                //Fourth combination to find: CCC Country + <empty> + <empty> (to retrieve records when the Product Family and the Return Type don't match)
                lRSTKeySet.add(sCountry + '__');
             }
             else
             {
               system.debug('>>>> [Return Item].[RMA].[Case].[CCC Country] is null ([' + aReturnItem.Id + '])');
             }
          }
          system.debug('>>>>> lRSTKeySet = ' + lRSTKeySet);

          //2 - Query RMA Shipping To Admin
          List<RMAShippingToAdmin__c> lRSTs = [SELECT Id,CCCountry__r.CountryCode__c,ProductFamily__c,ReturnType__c,Street__c,AdditionalAddress__c,City__c,ZipCode__c,StateProvince__c,County__c,Country__c,POBox__c,POBoxZip__c,StreetLocalLang__c,LocalAdditionalAddress__c,LocalCity__c,LocalCounty__c,TECH_Uniqueness__c FROM RMAShippingToAdmin__c WHERE TECH_Uniqueness__c IN :lRSTKeySet];
          system.debug('>>>>> lRSTs = ' + lRSTs);

          //3 - Put the Key and the RMA Shipping To Admin into a map
          for(RMAShippingToAdmin__c aRST : lRSTs)
          {
             lRSTMap.put(aRST.TECH_Uniqueness__c, aRST);
          }
          system.debug('>>>>> lRSTMap = ' + lRSTMap);
        }
        catch(System.DmlException Ex)
        {
           system.debug('>>>>> ERROR in getRSTMap(List<RMA_Product__c> ReturnItems) method: ' + Ex.getDmlMessage(0));
        }

        system.debug('>>>>> INFO: getRSTMap(List<RMA_Product__c> ReturnItems) method ends');
        return lRSTMap;
     }

     /** This method is used to retrieve the Shipping Address for a Return Item **/
     public static String getShippingAddress(RMA_Product__c aReturnItem, String aField)
     {
        system.debug('>>>>> INFO: getShippingAddress(RMA_Product__c aReturnItem, String aField) method begins');

        String lStreet = null;
        String lAdditionalAddress = null;
        String lCity = null;
        String lZipCode = null;
        String lStateProvince = null;
        String lCounty = null;
        String lCountry = null;
        String lPOBox = null;
        String lPOBoxZip = null;
        String lStreetLocalLang = null;
        String lLocalAdditionalAddress = null;
        String lLocalCity = null;
        String lLocalCounty = null;

        String lReturnValue = null;

        try
        {
           String sCountry = null;
           String sReturnT = null;

           if (RMAsMap.get(aReturnItem.RMA__c) != null)
           {
              sCountry = RMAsMap.get(aReturnItem.RMA__c).Case__r.CCCountry__r.CountryCode__c;
              sReturnT = RMAsMap.get(aReturnItem.RMA__c).ReturnType__c;
           }

           if(sCountry != null)   //CCC Country is at least required
           {
              String RSTKey_form1 = sCountry + '_' + sReturnT + '_' + aReturnItem.ProductFamily__c;
              String RSTKey_form2 = sCountry + '__' + aReturnItem.ProductFamily__c;
              String RSTKey_form3 = sCountry + '_' + sReturnT + '_';
              String RSTKey_form4 = sCountry + '__';
               
              System.debug('RSTKey_form1 : '+ RSTKey_form1);
              System.debug('RSTKey_form2 : '+ RSTKey_form2);
              System.debug('RSTKey_form3 : '+ RSTKey_form3);
              System.debug('RSTKey_form4 : '+ RSTKey_form4);
              System.debug('RSTMap : '+ RSTMap);
              System.debug('RMAsMap: '+ RMAsMap); 
               

              if(RSTMap.get(RSTKey_form1) != null)
              {
                 //Record found with full matching (Country and Return Type and Product Family)
                 system.debug('>>> UserKey = CCC Country + Return Type + Product Family');
                 
                 lStreet = RSTMap.get(RSTKey_form1).Street__c;
                 lAdditionalAddress = RSTMap.get(RSTKey_form1).AdditionalAddress__c;
                 lCity = RSTMap.get(RSTKey_form1).City__c;
                 lZipCode = RSTMap.get(RSTKey_form1).ZipCode__c;
                 lStateProvince = RSTMap.get(RSTKey_form1).StateProvince__c;
                 lCounty = RSTMap.get(RSTKey_form1).County__c;
                 lCountry = RSTMap.get(RSTKey_form1).Country__c;
                 lPOBox = RSTMap.get(RSTKey_form1).POBox__c;
                 lPOBoxZip = RSTMap.get(RSTKey_form1).POBoxZip__c;
                 lStreetLocalLang = RSTMap.get(RSTKey_form1).StreetLocalLang__c;
                 lLocalAdditionalAddress = RSTMap.get(RSTKey_form1).LocalAdditionalAddress__c;
                 lLocalCity = RSTMap.get(RSTKey_form1).LocalCity__c;
                 lLocalCounty = RSTMap.get(RSTKey_form1).LocalCounty__c;
              }
              else
              {
                 if(RSTMap.get(RSTKey_form2) != null)
                 {
                    system.debug('>>> UserKey = CCC Country + <empty> + Product Family');

                    //Record found with partial matching (Country and Product Family)
                    lStreet = RSTMap.get(RSTKey_form2).Street__c;
                    lAdditionalAddress = RSTMap.get(RSTKey_form2).AdditionalAddress__c;
                    lCity = RSTMap.get(RSTKey_form2).City__c;
                    lZipCode = RSTMap.get(RSTKey_form2).ZipCode__c;
                    lStateProvince = RSTMap.get(RSTKey_form2).StateProvince__c;
                    lCounty = RSTMap.get(RSTKey_form2).County__c;
                    lCountry = RSTMap.get(RSTKey_form2).Country__c;
                    lPOBox = RSTMap.get(RSTKey_form2).POBox__c;
                    lPOBoxZip = RSTMap.get(RSTKey_form2).POBoxZip__c;
                    lStreetLocalLang = RSTMap.get(RSTKey_form2).StreetLocalLang__c;
                    lLocalAdditionalAddress = RSTMap.get(RSTKey_form2).LocalAdditionalAddress__c;
                    lLocalCity = RSTMap.get(RSTKey_form2).LocalCity__c;
                    lLocalCounty = RSTMap.get(RSTKey_form2).LocalCounty__c;
                 }
                 else
                 {
                    if(RSTMap.get(RSTKey_form3) != null)
                    {
                       //Record found with partial matching (Country and Return Type)
                       system.debug('>>> UserKey = CCC Country + Return Type + <empty>');

                       lStreet = RSTMap.get(RSTKey_form3).Street__c;
                       lAdditionalAddress = RSTMap.get(RSTKey_form3).AdditionalAddress__c;
                       lCity = RSTMap.get(RSTKey_form3).City__c;
                       lZipCode = RSTMap.get(RSTKey_form3).ZipCode__c;
                       lStateProvince = RSTMap.get(RSTKey_form3).StateProvince__c;
                       lCounty = RSTMap.get(RSTKey_form3).County__c;
                       lCountry = RSTMap.get(RSTKey_form3).Country__c;
                       lPOBox = RSTMap.get(RSTKey_form3).POBox__c;
                       lPOBoxZip = RSTMap.get(RSTKey_form3).POBoxZip__c;
                       lStreetLocalLang = RSTMap.get(RSTKey_form3).StreetLocalLang__c;
                       lLocalAdditionalAddress = RSTMap.get(RSTKey_form3).LocalAdditionalAddress__c;
                       lLocalCity = RSTMap.get(RSTKey_form3).LocalCity__c;
                       lLocalCounty = RSTMap.get(RSTKey_form3).LocalCounty__c;
                    }
                    else
                    {
                       if(RSTMap.get(RSTKey_form4) != null)
                       {
                          //Record found with partial matching (Country)
                          system.debug('>>> UserKey = CCC Country + <empty> + <empty>');

                          lStreet = RSTMap.get(RSTKey_form4).Street__c;
                          lAdditionalAddress = RSTMap.get(RSTKey_form4).AdditionalAddress__c;
                          lCity = RSTMap.get(RSTKey_form4).City__c;
                          lZipCode = RSTMap.get(RSTKey_form4).ZipCode__c;
                          lStateProvince = RSTMap.get(RSTKey_form4).StateProvince__c;
                          lCounty = RSTMap.get(RSTKey_form4).County__c;
                          lCountry = RSTMap.get(RSTKey_form4).Country__c;
                          lPOBox = RSTMap.get(RSTKey_form4).POBox__c;
                          lPOBoxZip = RSTMap.get(RSTKey_form4).POBoxZip__c;
                          lStreetLocalLang = RSTMap.get(RSTKey_form4).StreetLocalLang__c;
                          lLocalAdditionalAddress = RSTMap.get(RSTKey_form4).LocalAdditionalAddress__c;
                          lLocalCity = RSTMap.get(RSTKey_form4).LocalCity__c;
                          lLocalCounty = RSTMap.get(RSTKey_form4).LocalCounty__c;
                       }
                    }
                 }
              }
           }

           system.debug('>>> lStreet = ' + lStreet);
           system.debug('>>> lAdditionalAddress = ' + lAdditionalAddress);
           system.debug('>>> lCity = ' + lCity);
           system.debug('>>> lZipCode = ' + lZipCode);
           system.debug('>>> lStateProvince = ' + lStateProvince);
           system.debug('>>> lCounty = ' + lCounty);
           system.debug('>>> lCountry = ' + lCountry);
           system.debug('>>> lPOBox = ' + lPOBox);
           system.debug('>>> lPOBoxZip = ' + lPOBoxZip);
           system.debug('>>> lStreetLocalLang = ' + lStreetLocalLang);
           system.debug('>>> lLocalAdditionalAddress = ' + lLocalAdditionalAddress);
           system.debug('>>> lLocalCity = ' + lLocalCity);
           system.debug('>>> lLocalCounty = ' + lLocalCounty);

           //Define returned value
           if(aField == 'Street')
           {
              lReturnValue = lStreet;
           }

           if(aField == 'AdditionalAddress')
           {
              lReturnValue = lAdditionalAddress;
           }

           if(aField == 'City')
           {
              lReturnValue = lCity;
           }

           if(aField == 'ZipCode')
           {
              lReturnValue = lZipCode;
           }

           if(aField == 'StateProvince')
           {
              lReturnValue = lStateProvince;
           }

           if(aField == 'County')
           {
              lReturnValue = lCounty;
           }

           if(aField == 'Country')
           {
              lReturnValue = lCountry;
           }

           if(aField == 'POBox')
           {
              lReturnValue = lPOBox;
           }

           if(aField == 'POBoxZip')
           {
              lReturnValue = lPOBoxZip;
           }

           if(aField == 'StreetLocalLang')
           {
              lReturnValue = lStreetLocalLang;
           }

           if(aField == 'LocalAdditionalAddress')
           {
              lReturnValue = lLocalAdditionalAddress;
           }

           if(aField == 'LocalCity')
           {
              lReturnValue = lLocalCity;
           }

           if(aField == 'LocalCounty')
           {
              lReturnValue = lLocalCounty;
           }
        }
        catch(Exception Ex)
        {
           system.debug('>>>>> ERROR in getShippingAddress(RMA_Product__c aReturnItem, String aField) method: ' + Ex.getMessage());
        }

        system.debug('>>>>> INFO: getShippingAddress(RMA_Product__c aReturnItem, String aField) method ends');
        return lReturnValue;
     }

    /*=======================================
      PUBLIC METHODS
    =======================================*/
    /** This method is used to update the Shipping Address **/
    public static void updateShippingAddress(List<RMA_Product__c> ReturnItems)
    {
       system.debug('>>>>> INFO: updateShippingAddress(List<RMA_Product__c> ReturnItems) method begins');
       system.debug('>>>>> INFO: ReturnItems before update: ' + ReturnItems);

       //1 - Get RMAMap
       RMAsMap = getRMAsMap(ReturnItems);
       system.debug('>>>>> RMAsMap = ' + RMAsMap);

       //2 - Get records in the RMA Shipping To Admin reference table
       RSTMap = getRSTMap(ReturnItems);
       system.debug('>>>>> RSTMap = ' + RSTMap);

       //3 - Update Shipping Address
       List<RMA__c> RMA2Upd = new List<RMA__c>();     //List of RMAs to update (only those with an empty Shipping Address or those for which the Shipping Address is different)

       for(RMA_Product__c aReturnItem : ReturnItems)
       {
          aReturnItem.ShippingStreet__c = getShippingAddress(aReturnItem, 'Street');
          aReturnItem.ShippingAdditionalAddress__c = getShippingAddress(aReturnItem, 'AdditionalAddress');
          aReturnItem.ShippingCity__c = getShippingAddress(aReturnItem, 'City');
          aReturnItem.ShippingZipCode__c = getShippingAddress(aReturnItem, 'ZipCode');
          aReturnItem.ShippingStateProvince__c = getShippingAddress(aReturnItem, 'StateProvince');
          aReturnItem.ShippingCounty__c = getShippingAddress(aReturnItem, 'County');
          aReturnItem.ShippingCountry__c = getShippingAddress(aReturnItem, 'Country');
          aReturnItem.ShippingPOBox__c = getShippingAddress(aReturnItem, 'POBox');
          aReturnItem.ShippingPOBoxZip__c = getShippingAddress(aReturnItem, 'POBoxZip');
          aReturnItem.ShippingStreetLocalLang__c = getShippingAddress(aReturnItem, 'StreetLocalLang');
          aReturnItem.ShippingLocalAdditionalAddress__c = getShippingAddress(aReturnItem, 'LocalAdditionalAddress');
          aReturnItem.ShippingLocalCity__c = getShippingAddress(aReturnItem, 'LocalCity');
          aReturnItem.ShippingLocalCounty__c = getShippingAddress(aReturnItem, 'LocalCounty');

          String RMAShippingStreet = null;
          String RMAShippingAdditionalAddress = null;
          String RMAShippingCity = null;
          String RMAShippingZipCode = null;
          String RMAShippingStateProvince = null;
          String RMAShippingCounty = null;
          String RMAShippingCountry = null;
          String RMAShippingPOBox = null;
          String RMAShippingPOBoxZip = null;
          String RMAShippingStreetLocalLang = null;
          String RMAShippingLocalAdditionalAddress = null;
          String RMAShippingLocalCity = null;
          String RMAShippingLocalCounty = null;

          if(RMAsMap.get(aReturnItem.RMA__c) != null)
          {
             RMAShippingStreet = RMAsMap.get(aReturnItem.RMA__c).ShippingStreet__c;
             RMAShippingAdditionalAddress = RMAsMap.get(aReturnItem.RMA__c).ShippingAdditionalAddress__c;
             RMAShippingCity = RMAsMap.get(aReturnItem.RMA__c).ShippingCity__c;
             RMAShippingZipCode = RMAsMap.get(aReturnItem.RMA__c).ShippingZipCode__c;
             RMAShippingStateProvince = RMAsMap.get(aReturnItem.RMA__c).ShippingStateProvince__c;
             RMAShippingCounty = RMAsMap.get(aReturnItem.RMA__c).ShippingCounty__c;
             RMAShippingCountry = RMAsMap.get(aReturnItem.RMA__c).ShippingCountry__c;
             RMAShippingPOBox = RMAsMap.get(aReturnItem.RMA__c).ShippingPOBox__c;
             RMAShippingPOBoxZip = RMAsMap.get(aReturnItem.RMA__c).ShippingPOBoxZip__c;
             RMAShippingStreetLocalLang = RMAsMap.get(aReturnItem.RMA__c).ShippingStreetLocalLang__c;
             RMAShippingLocalAdditionalAddress = RMAsMap.get(aReturnItem.RMA__c).ShippingLocalAdditionalAddress__c;
             RMAShippingLocalCity = RMAsMap.get(aReturnItem.RMA__c).ShippingLocalCity__c;
             RMAShippingLocalCounty = RMAsMap.get(aReturnItem.RMA__c).ShippingLocalCounty__c;
          }                                  

          //If the Shipping Address is null on the parent RMA, update it on the RMA
          if(RMAShippingCountry == null && aReturnItem.ShippingCountry__c != null)
          {
             RMA__c tmpRMA = new RMA__c(Id = aReturnItem.RMA__c,ShippingStreet__c  = aReturnItem.ShippingStreet__c,ShippingAdditionalAddress__c = aReturnItem.ShippingAdditionalAddress__c,ShippingCity__c = aReturnItem.ShippingCity__c,ShippingZipCode__c = aReturnItem.ShippingZipCode__c,ShippingStateProvince__c = aReturnItem.ShippingStateProvince__c,ShippingCounty__c = aReturnItem.ShippingCounty__c,ShippingCountry__c = aReturnItem.ShippingCountry__c,ShippingPOBox__c = aReturnItem.ShippingPOBox__c,ShippingPOBoxZip__c = aReturnItem.ShippingPOBoxZip__c,ShippingStreetLocalLang__c = aReturnItem.ShippingStreetLocalLang__c,ShippingLocalAdditionalAddress__c = aReturnItem.ShippingLocalAdditionalAddress__c,ShippingLocalCity__c = aReturnItem.ShippingLocalCity__c,ShippingLocalCounty__c = aReturnItem.ShippingLocalCounty__c);
             RMA2Upd.add(tmpRMA);
          }
          else
          {
             //Shipping Address on the RMA is not empty
             String RMAShippingAddress = RMAShippingStreet + RMAShippingAdditionalAddress + RMAShippingCity + RMAShippingZipCode + RMAShippingStateProvince + RMAShippingCounty + RMAShippingCountry + RMAShippingPOBox + RMAShippingPOBoxZip + RMAShippingStreetLocalLang + RMAShippingLocalAdditionalAddress + RMAShippingLocalCity + RMAShippingLocalCounty;

             String ReturnItemShippingAddress = aReturnItem.ShippingStreet__c + aReturnItem.ShippingAdditionalAddress__c + aReturnItem.ShippingCity__c + aReturnItem.ShippingZipCode__c + aReturnItem.ShippingStateProvince__c + aReturnItem.ShippingCounty__c + aReturnItem.ShippingCountry__c + aReturnItem.ShippingPOBox__c + aReturnItem.ShippingPOBoxZip__c + aReturnItem.ShippingStreetLocalLang__c + aReturnItem.ShippingLocalAdditionalAddress__c + aReturnItem.ShippingLocalCity__c + aReturnItem.ShippingLocalCounty__c;

             system.debug('>>> RMAShippingAddress = ' + RMAShippingAddress);
             system.debug('>>> ReturnItemShippingAddress = ' + ReturnItemShippingAddress);

             if(aReturnItem.ShippingCountry__c != null && RMAShippingAddress != ReturnItemShippingAddress)
             {
                //Shipping Address on Return Item is different and not null: need to advise user
                RMA__c tmpRMA = new RMA__c(Id                           = aReturnItem.RMA__c,
                                           TECH_CheckShippingAddress__c = true);
                RMA2Upd.add(tmpRMA);
             }
          }
       }
       update(RMA2Upd);

       system.debug('>>>>> INFO: ReturnItems after update: ' + ReturnItems);
       system.debug('>>>>> INFO: updateShippingAddress(List<RMA_Product__c> ReturnItems) method ends');
    }

    //////////////////////////////
    // Code for deleting record //
    //////////////////////////////
    /** This method is used to define Return Items to delete **/
    public static List<RMA_Product__c> getReturnItemsToDelete(List<RMA_Product__c> ReturnItems)
    {
       system.debug('>>>>> INFO: getReturnItemsToDelete(List<RMA_Product__c> ReturnItems) method begins');

       //1 - Get RMAMap
       RMAsMap = getRMAsMap(ReturnItems);
       system.debug('>>>>> RMAsMap = ' + RMAsMap);

       //2 - Define Return Items to delete
       List<RMA_Product__c> ReturnItemsToDelete = new List<RMA_Product__c>();

       for(RMA_Product__c aReturnItem : ReturnItems)
       {
          if(RMAsMap.get(aReturnItem.RMA__c) != null)
          {
             if(RMAsMap.get(aReturnItem.RMA__c).Status__c != Label.CL10236)
             {
                ReturnItemsToDelete.add(aReturnItem);
             }
             else
             {
                aReturnItem.addError(Label.CL10235);
             }
          }
       }

       system.debug('>>>>> INFO: getReturnItemsToDelete(List<RMA_Product__c> ReturnItems) method ends');
       return ReturnItemsToDelete;
    }
    // Update Return Address -  Gayathri Shivakumar Oct 2015 Release
    
    public static void updateReturnAddress(List<RMA_Product__c> ReturnItems)
    {
     List<RMA_Product__c> RMAUpd = new List<RMA_Product__c>(); 
     String RMALocal;
     String RMAGlobal;
     String RMAAddr;
     //String ChrLine = String.fromCharArray( new List<integer> { 196 } );
     String ChrLine = Label.CLOCT15I2P20;
     for(RMA_Product__c aReturnItem : ReturnItems)
       {
         //RMALocal = aReturnItem.ReturnCenterLocalStreet__c + '<br>' + aReturnItem.ReturnCenterLocalAdditionalInfo__c + '<br>' + aReturnItem.ReturnCenterZipCode__c + ',' + aReturnItem.ReturnCenterLocalCity__c + '<br>' + aReturnItem.ReturnCenterLocalCounty__c ;
         RMALocal = CheckIsNull(aReturnItem.ReturnCenterLocalStreet__c) + CheckIsNull(aReturnItem.ReturnCenterLocalAdditionalInfo__c) + CheckisNULL(aReturnItem.ReturnCenterZipCode__c) + CheckIsNull(aReturnItem.ReturnCenterLocalCity__c) + checkisNull(aReturnItem.ReturnCenterLocalCounty__c);
        // RMAGlobal = aReturnItem.ReturnCenterStreet__c + '<br>' + aReturnItem.ReturnCenterAdditionalInformation__c + '<br>' + aReturnItem.ReturnCenterZipCode__c + ',' + aReturnItem.ReturnCenterCity__c + '<br>' + aReturnItem.ReturnCenterStateProvince__c + '<br>' + aReturnItem.ReturnCenterCountry__c + '<br>' + aReturnItem.ReturnCenterPOBox__c + '<br>' + aReturnItem.ReturnCenterPOBoxZipCode__c + '<br>' + aReturnItem.ReturnCenterContactName__c + '<br>' + aReturnItem.ContactEmail__c + '<br>' + aReturnItem.ReturnCenterPhone__c + '<br>' + aReturnItem.ReturnCenterFax__c ;
         RMAGlobal = CheckIsNull(aReturnItem.ReturnCenterStreet__c) + CheckIsNULL(aReturnItem.ReturnCenterAdditionalInformation__c) + CheckIsNULL(aReturnItem.ReturnCenterZipCode__c) + CheckIsNULL(aReturnItem.ReturnCenterCity__c) + CheckIsNULL(aReturnItem.ReturnCenterStateProvince__c) + CheckIsNULL(aReturnItem.ReturnCenterCountry__c) + CheckIsNULL(aReturnItem.ReturnCenterPOBox__c) + CheckIsNULL(aReturnItem.ReturnCenterPOBoxZipCode__c) + CheckisNULL(aReturnItem.ReturnCenterContactName__c) + checkisNULL(aReturnItem.ContactEmail__c) + checkisNULL(aReturnItem.ReturnCenterPhone__c) + checkisNULL(aReturnItem.ReturnCenterFax__c);
         if(RMALocal != '') {
            RMAAddr =  RMAGlobal;
            for(integer i=1; i<=Integer.ValueOf(Label.CLOCT15I2P21); i++){
              RMAAddr = RMAAddr + ChrLine  ;
              }
             RMAAddr = RMAAddr + '<br>' + RMALocal;
            } 
         else
           RMAAddr =  RMAGlobal ;
           
         
         aReturnItem.ReturnCenterAddressAll__c = RMAAddr;
         RMAUpd.add(aReturnItem);
       }
       
    }
  
  
    
   public static string CheckIsNull(string inputStr){
   
    if(inputStr == null){
      string rval = '';
      return rval;
      }
     else
       return inputStr + '<br>';
      }
   
}