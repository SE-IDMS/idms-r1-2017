@isTest
private class VFC_CopySalesTeam_Test
{
    static testMethod void test_VFC_CopySalesTeam() 
    {    
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.Country__c=country.id;
        insert acc; 
                
        Opportunity opp1 = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp1;
        
        Pagereference pref = Page.VFP_CopySalesTeam;
        pref.getParameters().put('oppid',opp1.id);
        Test.setCurrentPage(pref);
        
        VFC_CopySalesTeam copyTeam = new VFC_CopySalesTeam(new ApexPages.StandardController(opp1));
        copyTeam.copySalesTeam();
        
        User user1 =  Utils_TestMethods.createStandardUser('TestUser');
        insert user1;
        
        OpportunityTeamMember op1 = Utils_TestMethods.createOppTeamMember(opp1.Id, user1.Id);
        insert op1;
  
        VFC_CopySalesTeam copyTeam2 = new VFC_CopySalesTeam(new ApexPages.StandardController(opp1));
        copyTeam2.copySalesTeam();
        
        //delete op1;
        
        //VFC_CopySalesTeam copyTeam3 = new VFC_CopySalesTeam(new ApexPages.StandardController(opp1));
        //copyTeam3.copySalesTeam();     
      
    }
}