global class Batch_ReferentialCaseDataMigration implements Database.Batchable<sObject>, Database.Stateful{
    global final String strQueryString;
    global Integer intLimitNumber;
    global List<Case> toBeUpdatedCases=new List<Case>();

    global Batch_ReferentialCaseDataMigration(){}
    global Batch_ReferentialCaseDataMigration(String strQuery,Integer intLmtNumber){
        strQueryString=strQuery;
    
        if(intLmtNumber==0 || intLmtNumber==null)
            intLimitNumber=2000000;
        intLimitNumber=intLmtNumber;
   }
    

  

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(strQueryString);
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){      
        Map<Id,Case> caseMap=new Map<Id,Case>();    
        for(sObject objScope : scope){
            Case objCase=(Case)objScope;   
            caseMap.put(objCase.id,objCase);
        }
        updateReferntialDataOnCase(caseMap);
    }

    global void updateReferntialDataOnCase(Map<Id,Case> caseMap){
        Map<Id,Case>                    mapCasesWithCR      = new Map<Id,Case>();
         Map<Id,Case>                   mapCasesWithOutCR   = new Map<Id,Case>();
        Set<Id>                         CommRefIdSet          = new Set<Id>();
        Set<String>                     OldGMRCodeSet       = new Set<String>();
        Map<Id,Product2>                mapCommRefAllLevel  = new Map<Id,Product2>();
        Map<String,TECH_GMRMapping__c>  mapGMRMapping       = new Map<String,TECH_GMRMapping__c>();
        
        String strErrorId='';
        String strErrorMessage='';
        String strErrorType='';
        String strErrorStackTrace='';
        
        for(Case objCase:caseMap.values()){
            if(objCase.CommercialReference_lk__c !=null){
                mapCasesWithCR.put(objCase.Id,objCase);
                CommRefIdSet.add(objCase.CommercialReference_lk__c);
            }
            else if(objCase.TECH_GMRCode__c !=null && objCase.TECH_GMRCode__c!='' && objCase.TECH_GMRCode__c.length() ==8){
                mapCasesWithOutCR.put(objCase.Id,objCase);
                OldGMRCodeSet.add(objCase.TECH_GMRCode__c);
            }
        }
        if(mapCasesWithCR.size()>0 || mapCasesWithOutCR.size()>0 ) {
            if(mapCasesWithCR.size()>0){
                mapCommRefAllLevel = new Map<Id, Product2>([Select id, ProductCode, Name, ProductGDPGMR__c, ProductGDP__r.BusinessUnit__c,ProductGDP__r.ProductLine__c,ProductGDP__r.ProductFamily__c,ProductGDP__r.Family__c,ProductGDP__r.SubFamily__c,ProductGDP__r.Series__c,ProductGDP__r.GDP__c,ProductGDP__r.TECH_PM0CodeInGMR__c,GDPGMR__c,ProductFamilyId__c from Product2 where Id IN:CommRefIdSet]);
                if(mapCommRefAllLevel.size()>0){
                    for(Case objCase: mapCasesWithCR.values()){
                        if(mapCommRefAllLevel.containsKey(objCase.CommercialReference_lk__c)){
                            objCase.CommercialReference_lk__c       = mapCommRefAllLevel.get(objCase.CommercialReference_lk__c).Id;
                            objCase.ProductBU__c                    = mapCommRefAllLevel.get(objCase.CommercialReference_lk__c).ProductGDP__r.BusinessUnit__c;
                            objCase.ProductLine__c                  = mapCommRefAllLevel.get(objCase.CommercialReference_lk__c).ProductGDP__r.ProductLine__c;
                            objCase.ProductFamily__c                = mapCommRefAllLevel.get(objCase.CommercialReference_lk__c).ProductGDP__r.ProductFamily__c;
                            objCase.Family__c                       = mapCommRefAllLevel.get(objCase.CommercialReference_lk__c).ProductGDP__r.Family__c;
                            objCase.Family_lk__c                    = mapCommRefAllLevel.get(objCase.CommercialReference_lk__c).ProductFamilyId__c;
                            objCase.SubFamily__c                    = mapCommRefAllLevel.get(objCase.CommercialReference_lk__c).ProductGDP__r.SubFamily__c;
                            objCase.ProductSuccession__c            = mapCommRefAllLevel.get(objCase.CommercialReference_lk__c).ProductGDP__r.Series__c;
                            objCase.ProductGroup__c                 = mapCommRefAllLevel.get(objCase.CommercialReference_lk__c).ProductGDP__r.GDP__c;
                            objCase.TECH_GMRCode__c                 = mapCommRefAllLevel.get(objCase.CommercialReference_lk__c).ProductGDPGMR__c;
                            objCase.TECH_2015ReferentialUpdated__c  = true;
                            toBeUpdatedCases.add(objCase);
                        }
                    }
                }
            }
            if(mapCasesWithOutCR.size()>0){
                List<TECH_GMRMapping__c> lstGMRMapping = new List<TECH_GMRMapping__c>([Select Id, Name, OldProductID__c, NewBusinessUnit__c, NewProductLine__c, NewProductFamily__c, NewFamily__c, NewGMRCode__c from TECH_GMRMapping__c where Name IN:OldGMRCodeSet ]);
                if(lstGMRMapping.size()>0){
                    for(TECH_GMRMapping__c objGMRMapping: lstGMRMapping){
                        mapGMRMapping.put(objGMRMapping.Name,objGMRMapping);
                    }             
                }
                if(mapGMRMapping.size()>0){
                    for(Case objCase: mapCasesWithOutCR.values()){
                        if(mapGMRMapping.containsKey(objCase.TECH_GMRCode__c)){
                            objCase.ProductBU__c = mapGMRMapping.get(objCase.TECH_GMRCode__c).NewBusinessUnit__c;
                            objCase.ProductLine__c = mapGMRMapping.get(objCase.TECH_GMRCode__c).NewProductLine__c;
                            objCase.ProductFamily__c = mapGMRMapping.get(objCase.TECH_GMRCode__c).NewProductFamily__c;
                            objCase.Family__c = mapGMRMapping.get(objCase.TECH_GMRCode__c).NewFamily__c;
                            objCase.Family_lk__c = mapGMRMapping.get(objCase.TECH_GMRCode__c).OldProductID__c;
                            objCase.TECH_GMRCode__c = mapGMRMapping.get(objCase.TECH_GMRCode__c).NewGMRCode__c;
                            objCase.TECH_2015ReferentialUpdated__c=true;
                            toBeUpdatedCases.add(objCase);                      
                        }
                    }
                }
            }
        }
        if(toBeUpdatedCases.size()>0){
            try{
                Database.SaveResult[] CaseUpdateResult = Database.update(toBeUpdatedCases, false);
                for(Database.SaveResult objSaveResult: CaseUpdateResult){
                    if(!objSaveResult.isSuccess()){
                        Database.Error err = objSaveResult.getErrors()[0];
                        strErrorId = objSaveResult.getId() + ':' + '\n';
                        strErrorMessage = err.getStatusCode()+' '+err.getMessage()+ '\n';
                    }
                }
            }    
            catch(Exception ex){
                strErrorMessage += ex.getTypeName()+'- '+ ex.getMessage() + '\n';
                strErrorType += ex.getTypeName()+'- '+ ex.getLineNumber() + '\n';
                strErrorStackTrace += ex.getTypeName()+'- '+ ex.getStackTraceString() + '\n';
            }
            finally{
                if(strErrorId.trim().length()>0 || Test.isRunningTest()){
                    try{
                        DebugLog__c objDebugLog = new DebugLog__c(ExceptionType__c=strErrorType,Description__c=strErrorMessage,StackTrace__c=strErrorStackTrace, DebugLog__c= strErrorId );
                        Database.DMLOptions DMLoption = new Database.DMLOptions(); 
                        DMLoption.optAllOrNone = false;
                        DMLoption.AllowFieldTruncation = true;
                        DataBase.SaveResult debugLog_SR = Database.insert(objDebugLog, DMLoption);
                    }
                    catch(Exception ex){
                    }
                }
            }
        }
    }

    global void finish(Database.BatchableContext BC){
    }
}