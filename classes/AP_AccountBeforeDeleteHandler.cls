/*
    Author: Bhuvana Subramaniyan
    Purpose: Merged different functionalities of account before delete to a single class
*/
public class AP_AccountBeforeDeleteHandler
{
    Set<Id> accIds = new Set<Id>();
    Map<Id, Id> accCasesIds = new Map<Id, Id>();
    Map<Id, Id> accLeadsIds= new Map<Id, Id>();   
    Map<Id, Id> accCustSurIds= new Map<Id, Id>();   
    List<Case> cases = new List<Case>();
    List<Lead> leads = new List<Lead>();
    List<CustomerSatisfactionSurvey__c> custSur = new List<CustomerSatisfactionSurvey__c>();
    
    public void OnBeforeDelete(List<Account> newAccounts,Map<Id,Account> newAccountsMap)
    {
        //Q3 SLS Added Account Master Profile deletion logic
        List<AccountMasterProfile__c> amplst=[select Id from AccountMasterProfile__c where Account__c in :newAccountsMap.keyset()];
        if(amplst.size()>0){
            Database.DeleteResult[] delAMPlist= Database.delete(amplst, true);
            for(Database.DeleteResult dr: delAMPlist){
                if(!dr.isSuccess()){
                    Database.Error err = dr.getErrors()[0];
                    System.Debug('########AP_AccountBeforeDeleteHandler: '+err.getStatusCode()+' '+err.getMessage());
                 }    
            }
         }
        // EXECUTE BEFORE DELETE LOGIC
        /* These following lines checks if there are open cases, leads, or opportunities, active Account Assigned Programs. 
               If yes, the account can’t be deleted.    
        */
        //Queries the cases for the corresponding Account
        cases  = [select Id,AccountId,CreatedDate,Status from Case where AccountId in: newAccountsMap.keyset() and ((Status!=:Label.CLDEC13CCC04 and Status!=:Label.CL00326) or CreatedDate>:Date.today().addmonths(-6))];
        
        for(Case c: cases)
        {
            accCasesIds.put(c.AccountId,c.Id);
        }
        
        //Queries the Leads for the corresponding Account
        leads = [select Id,Account__c from Lead where Account__c in: newAccountsMap.keyset()];
        for(Lead l: leads)
        {
            accLeadsIds.put(l.Account__c, l.Id);
        }
        
        //Queries the Customer Experience Feedback records for the corresponding Account
        custSur = [select Id,Account__c from CustomerSatisfactionSurvey__c where Account__c in: newAccountsMap.keyset()];
        for(CustomerSatisfactionSurvey__c csat: custSur)
        {
            accCustSurIds.put(csat.Account__c, csat.Id);
        }
        
        //Displays an error if there are open cases, leads, or opportunities, active Account Assigned Programs. 
        for(Account a: newAccounts)
        {
            if(((accCasesIds.containsKey(a.Id)) || (accLeadsIds.containsKey(a.Id)) || (accCustSurIds.containsKey(a.Id)) || (a.TECH_OpenOpportunityCount__c>0) || (a.TECH_ActivePRMPrograms__c>0) || a.TECH_IsSVMXRecordPresent__c==true) && (a.ReasonForDeletion__c!=Label.CL00521))
            {    
                    a.addError(Label.CLOCT13ACC01);
            }
            
        }        
    }
}