@isTest(seealldata=true)
public class VFC_Flag_Permissionset_Test
{

    static testMethod void runTestForPermissionSets(){
        User userRecord;
        List<PermissionSet> permissionsetList;
        List<PermissionSetAssignment> userPermissionSetAssignmentRecords;

        userRecord = [select Id,(select Id,PermissionSet.Label from PermissionSetAssignments) from User where Profile.Name like '%CCC%' and isActive=true limit 1];

        PermissionSetGroup__c permissionSetGroup =  new PermissionSetGroup__c(Name = 'Valid Group',Stream__c = 'Service',Type__c = 'Core',RequiresValidation__c=true);
        insert permissionSetGroup;
        List<PsGroupMember__c> psGroupMemberList = new List<PsGroupMember__c>();
        for(PermissionSetAssignment PermissionSetAssignmentrecord: userRecord.PermissionSetAssignments)
        {
            psGroupMemberList.add(new PsGroupMember__C(PermissionSetGroup__c = permissionSetGroup.Id,Name = PermissionSetAssignmentrecord.PermissionSet.Label));
        }
        psGroupMemberList.remove(0);        
        insert psGroupMemberList;
        

        System.runAs (userRecord)
        {

           ApexPages.StandardController userController = new ApexPages.StandardController(userRecord);

           VFC_Flag_Permissionset Flag_PermissionSet = new VFC_Flag_Permissionset(userController);
           Flag_PermissionSet.validatePermissions();
           Flag_PermissionSet.removeunauthorizedassignments();
            
           //now assign the permission set
           PermissionSetAssignmentRequests__c assignmentRequest = new PermissionSetAssignmentRequests__c(status__c='Assigned',PermissionSetGroup__c=permissionSetGroup.Id,assignee__c=userRecord.Id); 
           insert assignmentRequest;
           Flag_PermissionSet.validatePermissions();            
       }      

   }
}