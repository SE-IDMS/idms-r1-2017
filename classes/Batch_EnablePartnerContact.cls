global class Batch_EnablePartnerContact implements Database.Batchable<AggregateResult>, Database.stateful, Schedulable{

    global DateTime startTime;
    global DateTime endTime;
    global List<PRM_Technical_Partner_Record__c> listRecords = new List<PRM_Technical_Partner_Record__c>();
    
    global Iterable<AggregateResult> start(Database.BatchableContext BC) {
        return new AggregateResultIterable();
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        startTime = datetime.now();
        Map<string,integer> elligibleContMap = new Map<string,integer>();
        //Process all AggregateResult and store them in a Map with the count of each group
        for (Sobject so : scope)  {
            AggregateResult ar = (AggregateResult)so;
            elligibleContMap.put(string.valueof(ar.get('SEContactID__c')),Integer.valueOf(ar.get('expr0')));
        }
        System.debug('##########################elligibleContMap = ' + elligibleContMap);
        
        //Enable elligible contacts to partner & create community users
        List<User> listUserToInsert = new List<User>();
        //Id RoleFieloId = [SELECT Id FROM UserRole WHERE DeveloperName =: 'FieloPartnerUser1' limit 1][0].Id;
        Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name =: 'SE - Channel Partner (Community)' limit 1][0].Id;
        
        //Survivor User
        Map<String, Id> SurvivorUserContactMap = new Map<String, Id>();
        for(User u : [SELECT Id, ContactId, Contact.SEContactId__c FROM User WHERE ContactId <> null AND Contact.SEContactId__c IN: elligibleContMap.keySet()]){
            SurvivorUserContactMap.put(u.Contact.SEContactId__c, u.Id);
        }
        
        for(Contact c : [SELECT Id, FirstName, LastName, Email, SEContactId__c, PRMUIMSID__c, PRMOnly__c
        FROM Contact WHERE SEContactId__c IN: elligibleContMap.keySet()]){
            if(c.PRMUIMSID__c == null && c.PRMOnly__c == false){
                PRM_Technical_Partner_Record__c p = new PRM_Technical_Partner_Record__c();
                try{
                    if(!SurvivorUserContactMap.containsKey(c.SEContactId__c)){
                        User u = new User(FirstName = c.FirstName, LastName = c.LastName, Email = c.Email, Username = c.Email + '.portal',
                        Alias = c.LastName.substring(0, 4), CommunityNickname = c.LastName.substring(0, 4) + date.today().day(),
                        ProfileId = ProfileFieloId, ContactId = c.Id, CurrencyIsoCode = 'USD',
                        TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',
                        UserPermissionsMobileUser = false);
                        listUserToInsert.add(u);
                        p.Name = c.SEContactID__c;
                        p.PRMUIMS__c = c.PRMUIMSID__c;
                        p.Record_Id__c = c.Id;
                        p.Object__c = 'Contact';
                        p.Success__c = true;
                        listRecords.add(p);
                    }
                }
                catch (Exception e){
                    p.Name = c.SEContactID__c;
                    p.PRMUIMS__c = c.PRMUIMSID__c;
                    p.Record_Id__c = c.Id;
                    p.Object__c = 'Contact';
                    p.Error__c = true;
                    p.Error_Message__c = e.getMessage();
                    listRecords.add(p);
                }
            }
        }
        insert listUserToInsert;
        endTime = datetime.now();
    }
    
    global void finish(Database.BatchableContext BC) {
        PRMTechnicalMergeHistory__c contactMergeHistory = new PRMTechnicalMergeHistory__c(Name = BC.getJobId(),
        Batch_Start_Time__c = startTime, Batch_End_Time__c = endTime);
        insert contactMergeHistory;
        
        for(PRM_Technical_Partner_Record__c r: listRecords){
            r.Batch_Id__c = contactMergeHistory.Id;
        }
        insert listRecords;
    }
    
    //Schedule method
    global void execute(SchedulableContext sc) {
        Batch_EnablePartnerContact batchCont = new Batch_EnablePartnerContact();
        database.executeBatch(batchCont);
    }
}