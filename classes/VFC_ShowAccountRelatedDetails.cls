/*
    Author          : Shruti Karn
    Date Created    : 02/05/2012
    Description     : Controller searches the Contacts and Cases related to the Account.
    
*/
public class VFC_ShowAccountRelatedDetails
{
    public list<contact> lstContact{get;set;}
    public list<Case> lstCases {get;set;}
    public Account acct {get;set;}
    public Boolean showMoreContacts {get;set;}
    public Boolean showMoreCases {get;set;}
    public VFC_ShowAccountRelatedDetails(ApexPages.StandardController standardController)
    {
        acct = (Account)standardController.getRecord();
        showMoreContacts = false;
        showMoreCases = false;
        if(acct.Id == null)
        {
            ApexPages.Message msg1 = new ApexPages.Message(ApexPages.Severity.WARNING,'No related records');
            ApexPages.addmessage(msg1); 
        
        }
        else
        {
            lstContact = new list<contact>();
            lstCases = new list<Case>();
            String queryContact = 'Select id, Name,phone,email from Contact where accountid = '+'\'' + acct.id +'\''+ 'limit '+ System.Label.CLSEP12CCC20 ;
            lstContact = database.query(queryContact);
            if(lstContact.size() == Integer.valueOf(System.Label.CLSEP12CCC20))
            {
                lstContact.remove((Integer.valueOf(System.Label.CLSEP12CCC20) -1));
                showMoreContacts = true;
            }
            String queryCases = 'Select casenumber , status, subject, contactid from Case where accountid = ' +'\'' + acct.id +'\''+' order by createddate desc limit '+ System.Label.CLSEP12CCC20 ;
            lstCases = database.query(queryCases);
            if(lstCases.size() == Integer.valueOf(System.Label.CLSEP12CCC20))
            {
                lstCases.remove(Integer.valueOf(System.Label.CLSEP12CCC20) -1);
                showMoreCases = true;
            }
        }
    }
}