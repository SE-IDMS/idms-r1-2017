global class ProgramLevelResult {
    // WS error codes & error message
    WebService Integer returnCode;
    WebService String message;

    WebService Integer TotalLevelCount { get; set; }

    WebService ProgramLevel[] Levels { get; set; }
    WebService Datetime BatchMaxLastModifiedDate{get;set;}
}