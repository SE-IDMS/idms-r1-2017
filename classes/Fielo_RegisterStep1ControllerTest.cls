/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: Test Class                                          *
* Created Date:                                             *
* Tested Class: Fielo_RegisterStep1Controller               *
************************************************************/

@isTest
public with sharing class Fielo_RegisterStep1ControllerTest{
  
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        Country__c  country = new Country__c (Name = 'United Arab Emirates', CountryCode__c = '001');
        insert country;
        Fielo_RegisterStep1Controller.doAssignPhoneExtension(country.Id);
        country = new Country__c (Name = 'Bahrain', CountryCode__c = '002');
        insert country;
        Fielo_RegisterStep1Controller.doAssignPhoneExtension(country.Id);
        country = new Country__c (Name = 'Kuwait', CountryCode__c = '003');
        insert country;
        Fielo_RegisterStep1Controller.doAssignPhoneExtension(country.Id);
        country = new Country__c (Name = 'Oman', CountryCode__c = '004');
        insert country;
        Fielo_RegisterStep1Controller.doAssignPhoneExtension(country.Id);
        country = new Country__c (Name = 'Qatar', CountryCode__c = '005');
        insert country;
        Fielo_RegisterStep1Controller.doAssignPhoneExtension(country.Id);
        Fielo_RegisterStep1Controller.doBusinessType('Installer'); 
        
        
        Fielo_RegisterStep1Controller test = new Fielo_RegisterStep1Controller();
        test.doToMenu();
        
        Fielo_RegisterStep1Controller test1 = new Fielo_RegisterStep1Controller(new FieloEE.RegisterStep1Controller());
        List<SelectOption> selOptTest = test1.getCountryValues();
        
        test1.selectedCountry = null;
        test1.doSubmit();
        
        test1.selectedCountry = country.Id;
        test1.doSubmit();
        
        test1.controller.member.F_BusinessType__c = 'SC';
        test1.controller.member.F_PurchaseSEProductsBefore__c = null;
        test1.doSubmit();
        
        test1.controller.member.F_BusinessType__c = null;
        test1.acceptTermsConditions = false;
        test1.doSubmit();
        
        test1.acceptTermsConditions = true;
        test1.selectedCountry = [SELECT Id, Name FROM Country__c WHERE Name = 'United Arab Emirates'].Id;
        test1.controller.member.F_City__c = null;
        test1.doSubmit();
        
        test1.controller.member.F_City__c = 'Dubai';
        test1.doSubmit();
    }
}