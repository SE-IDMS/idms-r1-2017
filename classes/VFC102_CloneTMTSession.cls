/*
+-----------------------+---------------------------------------------------------------------------------------+
| Author                | Alexandre Gonzalo                                                                     |
+-----------------------+---------------------------------------------------------------------------------------+
| Description           |                                                                                       |
|                       |                                                                                       |
|     - Component       | VFC102_CloneTMTSession                                                                |
|                       |                                                                                       |
|     - Object(s)       | TMT Session                                                                           |
|     - Description     |   - Override the Clone button and set the Notify registered users checkbox to 'FALSE' |
|                       |     > Default Controller: VFC102_CloneTMTSession(ApexPages.StandardController stdController) |
|                       |     > Method:             getClonePage()                                              |
|                       |                                                                                       |
|                       |                                                                                       |
+-----------------------+---------------------------------------------------------------------------------------+
| Delivery Date         | April, 28th 2012                                                                      |
+-----------------------+---------------------------------------------------------------------------------------+
| Modifications         |                                                                                       |
+-----------------------+---------------------------------------------------------------------------------------+
| Governor informations |                                                                                       |
+-----------------------+---------------------------------------------------------------------------------------+
*/

public class VFC102_CloneTMTSession
{

    private final TMT_Session__c Session; 
    
    public VFC102_CloneTMTSession(ApexPages.StandardController stdController)
    {
        Session = (TMT_Session__c)stdController.getRecord();
    }

    public PageReference getClonePage()
    {
        PageReference clonepage = new PageReference('/' + Session.Id + '/e?');
        clonepage.getParameters().put(System.Label.CL10241, System.Label.CL10243);
        clonepage.getParameters().put(System.Label.CL10242, System.Label.CL10243);
        clonepage.getParameters().put(System.Label.CL10248, System.Label.CL10249);
        clonepage.getParameters().put(System.Label.CL00330, Session.Id);
        return clonepage;
    }
}