Public class Utils_PADataSource
{
    public Boolean Test;

    public Utils_DataSource.Result SearchPA(VFC34_PriceAndAvailabilitySearch PAController)
    {
        // --- PRE TREATMENT ----
        WS08_SimplePA.priceAndAvailabilitySearchDataBean PASearchCriteria = new WS08_SimplePA.priceAndAvailabilitySearchDataBean();
        PASearchCriteria.bfoAccountId = PAController.DateCapturer.Account__c;
        PASearchCriteria.catalogNumber = PAController.PartNo.toUpperCase();
        PASearchCriteria.priceDate = PAController.DateCapturer.DateField1__c;
        PASearchCriteria.quantity = PAController.Quantity;
        PASearchCriteria.sdhLegacyPSName = PAController.BackOffice;
             
        // ---- WEB SERVICE CALLOUT -----    
        WS08_SimplePA.PriceAndAvailabilityPort PAConnector = new WS08_SimplePA.PriceAndAvailabilityPort();
        PAConnector.endpoint_x = Label.CL00566;
        PAConnector.timeout_x = 40000;
        PAConnector.ClientCertName_x = Label.CL00616;
        
        WS08_SimplePA.priceAndAvailabilityResult  PA_WS = new WS08_SimplePA.priceAndAvailabilityResult ();
        
        if(Test==true)
            PA_WS = Utils_Stubs.PAStub();
        else 
            PA_WS = PAConnector.priceAndAvailability(PASearchCriteria);
        
        // ---- POST TREATMENT -----
        Utils_DataSource.Result Result = new Utils_DataSource.Result();
        
        Result.ReturnMessage = PA_WS.returnInfo.returnMessage;
        
        if(PA_WS.priceAndAvailability != null)
        {
            DataTemplate__c PA_sObject = new DataTemplate__c();
            if(PA_WS.priceAndAvailability.asOf !=null)
                PA_sObject.Datefield1__c = PA_WS.priceAndAvailability.asOf.date();
            else
                PA_sObject.Datefield1__c = Date.today();
            
            PA_sObject.field2__c = PA_WS.priceAndAvailability.catalogNumber;
            PA_sObject.field3__c = String.valueof(PA_WS.priceAndAvailability.listPrice) + ' ' + PA_WS.priceAndAvailability.currencyCode ;
            PA_sObject.field4__c = String.valueof(PA_WS.priceAndAvailability.priceDate.date());
            PA_sObject.field5__c = String.valueof(PA_WS.priceAndAvailability.quantity); 
            
            List<DataTemplate__c> PAList =  new List<DataTemplate__c>{PA_sObject};        
            Result.RecordList = PAList ;
        }
        
        return Result;
    }
}