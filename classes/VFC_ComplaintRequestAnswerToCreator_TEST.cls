/*
Hanamanth
Description  :Test class for VFC_ComplaintRequestAnswerToCreator
*/

@isTest(SeeAllData=false)
Public class VFC_ComplaintRequestAnswerToCreator_TEST {

    static TESTMETHOD VOID Test_RequestAnswerToCreator() {

        Country__c CountryObjLCR = Utils_TestMethods.createCountry();
        Insert CountryObjLCR;
        // Create User
        User LCRTestUser = Utils_TestMethods.createStandardUser('LCRRasg');
        Insert LCRTestUser ;
        User LCRTestUserCq = Utils_TestMethods.createStandardUser('LCROwMCQ');
        Insert LCRTestUserCq ;
        // Create Account
        Account accountObj = Utils_TestMethods.createAccount();
        accountObj.Country__c=CountryObjLCR.Id; 
        insert accountObj;
        
        
        // Create Contact to the Account
        /*
        Contact ContactObj=Utils_TestMethods.CreateContact(accountObj.Id,'LCRContact');
        insert ContactObj;
        Case CaseObj= Utils_TestMethods.createCase(accountObj.Id,ContactObj.Id,'Open');
        
        CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
        CaseObj.Symptom__c =  'Installation/ Setup';
        CaseObj.SubSymptom__c = 'Hardware';    
        CaseObj.Quantity__c = 4;
        CaseObj.Family__c = 'ADVANCED PANEL';
        CaseObj.CommercialReference__c='xbtg5230';
        
        Insert CaseObj;
        */
        
        insertCSStakeholders();
       
        BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST SCH',
                                            Entity__c='LCR Entity-1',
                                            SubEntity__c='LCR Sub-Entity',
                                            Location__c='LCR Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'LCR Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',
                                            ContactEmail__c ='SCH@schneider.com'
                                            );
        Insert lCROrganzObj; 
        BusinessRiskEscalationEntity__c lCROrganzObj1 = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST Repair',
                                            Entity__c='LCR Entity-1',
                                            SubEntity__c='LCR Sub-Entity1',
                                            Location__c='LCR Location 1',
                                            Location_Type__c= 'LCR',
                                            Street__c = 'LCR Street1',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City1',
                                            RCZipCode__c = '500225',
                                            ContactEmail__c ='SCH1@schneider.com'
                                            );
        Insert lCROrganzObj1;
        
        BusinessRiskEscalationEntity__c lCROrganzObj2 = new BusinessRiskEscalationEntity__c();
        lCROrganzObj2.Entity__c = 'Mechanisburg';
        lCROrganzObj2.name ='Mechanisburg';
        insert lCROrganzObj2;
        BusinessRiskEscalationEntity__c lCROrganzObj3 = new BusinessRiskEscalationEntity__c();
        
       
        
        ComplaintRequest__c  CRAnswerToCreator = new ComplaintRequest__c(
                //Status__c='Open',
                Status__c=Label.CLOCT13RR57,
               // Case__c  =CaseObj.id,
                //Category__c ='1 - Pre-Sales Support',
                Category__c =Label.CLAPR14I2P02,
                reason__c='Marketing Activity Response',
                AccountableOrganization__c =lCROrganzObj.id, 
                ReportingEntity__c =lCROrganzObj1.id
                );
                
        Insert CRAnswerToCreator; 
        ComplaintsRequestsEscalationHistory__c cREsHistry2 = new ComplaintsRequestsEscalationHistory__c(
                From__c='Mechanisburg',
                ComplaintRequest__c =CRAnswerToCreator.id
                ); 
        insert cREsHistry2;
        ComplaintsRequestsEscalationHistory__c cREsHistry1 = new ComplaintsRequestsEscalationHistory__c(
                From__c='LCR TEST SCH',
                ComplaintRequest__c =CRAnswerToCreator.id,
                To__c='Mechanisburg'
                ); 
        insert cREsHistry1;
        EntityStakeholder__c EntityStack1 = new EntityStakeholder__c();
                EntityStack1.Role__c = 'Complaint/Request creator';
                EntityStack1.User__c =UserInfo.getUserId();
                EntityStack1.BusinessRiskEscalationEntity__c =lCROrganzObj1.Id;
                
                insert EntityStack1;
               
        complaintRequest__c  CRAwithCase = new ComplaintRequest__c(
                Status__c='Open',
                //Case__c  =CaseObj.id,
                Category__c ='1 - Pre-Sales Support',
                reason__c='Marketing Activity Response',
                //AccountableOrganization__c =lCROrganzObj.id 
                ReportingEntity__c =lCROrganzObj1.id
                );
    insert CRAwithCase; 
    complaintRequest__c  CRAwithCase1 = new ComplaintRequest__c(
                Status__c='Open',
                //Case__c  =CaseObj.id,
                Category__c ='1 - Pre-Sales Support',
                reason__c='Marketing Activity Response',
                AccountableOrganization__c =lCROrganzObj.id, 
                ReportingEntity__c =lCROrganzObj1.id
                );
    insert CRAwithCase1; 
        Answertostack__c InsertStackNw = new Answertostack__c();

            InsertStackNw.ComplaintRequest__c=CRAnswerToCreator.id;
            InsertStackNw.KeepInvolved__c =true;
            InsertStackNw.Department__c ='strDepartment';
            //InsertStackNw.IssueOwner__c =cRIsueUsrId;
            InsertStackNw.ContactEmailAccountableOrganization__c='h@schneider-electric.com';
            InsertStackNw.Organization__c=lCROrganzObj1.id;
            InsertStackNw.TECH_StackCounter__c=Integer.ValueOf(label.CLOCT15I2P74);
            
            insert InsertStackNw; 
        
        ApexPages.currentPage().getParameters().put('Id', CRAnswerToCreator.Id);
        ApexPages.StandardController CRASTOCRT = new ApexPages.standardController(CRAnswerToCreator);
        VFC_ComplaintRequestAnswerToCreator  CRAnsToCreator = new VFC_ComplaintRequestAnswerToCreator(CRASTOCRT);
        CRAnsToCreator.isRendered=true;
       // CRAnsToCreator.save();
       // CRAnsToCreator.getBackToRecord();
       CRAnsToCreator.OnPageLoad();
        CRAnsToCreator.validateAnswerToCreator();
        ApexPages.StandardController CRASTOCRT1 = new ApexPages.standardController(CRAwithCase);
        VFC_ComplaintRequestAnswerToCreator  CRAnsToCreator1 = new VFC_ComplaintRequestAnswerToCreator(CRASTOCRT1);
        
        ApexPages.StandardController CRASTOCRT2 = new ApexPages.standardController(CRAwithCase1);
        VFC_ComplaintRequestAnswerToCreator  CRAnsToCreator2 = new VFC_ComplaintRequestAnswerToCreator(CRASTOCRT2);
        
    }
    
    
    
    
    
    static testmethod void insertCSStakeholders (){

    List<CS_Stakeholders__c> CS_StakeholdersList = new List<CS_Stakeholders__c>();
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager', objectname__c='XA__c', name='ContainmentAction5', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='CS&Q Manager', objectname__c='XA__c', name='ContainmentAction6', escalationdays__c=3, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='CS&Q Manager', objectname__c='XA__c', name='ContainmentAction7', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager+1', objectname__c='XA__c', name='ContainmentAction8', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='CA__c', name='Corrective Action0', escalationdays__c=1, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='CA__c', name='Corrective Action1', escalationdays__c=2, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='CA__c', name='Corrective Action2', escalationdays__c=3, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='ComplaintRequest__c', name='ComplaintRequest0', escalationdays__c=1, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='ComplaintRequest__c', name='ComplaintRequest1', escalationdays__c=2, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='ComplaintRequest__c', name='ComplaintRequest2', escalationdays__c=3, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='ComplaintRequest__c', name='ComplaintRequest3', escalationdays__c=8, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager', objectname__c='ComplaintRequest__c', name='ComplaintRequest4', escalationdays__c=3, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager', objectname__c='ComplaintRequest__c', name='ComplaintRequest5', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='CS&Q Manager', objectname__c='ComplaintRequest__c', name='ComplaintRequest6', escalationdays__c=3, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='CS&Q Manager', objectname__c='ComplaintRequest__c', name='ComplaintRequest7', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager+1', objectname__c='ComplaintRequest__c', name='ComplaintRequest8', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='XA__c', name='ContainmentAction0', escalationdays__c=1, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='XA__c', name='ContainmentAction1', escalationdays__c=2, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='XA__c', name='ContainmentAction2', escalationdays__c=3, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='XA__c', name='ContainmentAction3', escalationdays__c=8, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager', objectname__c='XA__c', name='ContainmentAction4', escalationdays__c=3, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='CA__c', name='Corrective Action3', escalationdays__c=8, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager', objectname__c='CA__c', name='Corrective Action4', escalationdays__c=3, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager', objectname__c='CA__c', name='Corrective Action5', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='CS&Q Manager', objectname__c='CA__c', name='Corrective Action6', escalationdays__c=3, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='CS&Q Manager', objectname__c='CA__c', name='Corrective Action7', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager+1', objectname__c='CA__c', name='Corrective Action8', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='PA__c', name='Preventive Action0', escalationdays__c=1, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='PA__c', name='Preventive Action1', escalationdays__c=2, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='PA__c', name='Preventive Action2', escalationdays__c=3, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Owner', objectname__c='PA__c', name='Preventive Action3', escalationdays__c=8, emailtemplate__c='EmailTemplate1', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager', objectname__c='PA__c', name='Preventive Action4', escalationdays__c=3, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager', objectname__c='PA__c', name='Preventive Action5', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='CS&Q Manager', objectname__c='PA__c', name='Preventive Action6', escalationdays__c=3, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='CS&Q Manager', objectname__c='PA__c', name='Preventive Action7', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    CS_StakeholdersList.add(new CS_Stakeholders__c(role__c='Manager+1', objectname__c='PA__c', name='Preventive Action8', escalationdays__c=8, emailtemplate__c='Template2', currencyisocode='INR'
    ));
    
    insert CS_StakeholdersList;
}

}