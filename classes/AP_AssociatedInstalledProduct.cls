/*     
@Author: Deepak
Created Date: 17-12-2013
Description: This will show error message "You have already added this installed product to the contract"
**********

*/

Public Class AP_AssociatedInstalledProduct
{   
    public static void AddError(List<AssociatedInstalledProduct__c> aipList1)
    {   
        set<string> s = New set<string>(); 
        List<AssociatedInstalledProduct__c> aipList2 = New List<AssociatedInstalledProduct__c>();
        for(AssociatedInstalledProduct__c aip: aipList1)
        {   
            if(aip.InstalledProduct__c !=null && aip.ServiceMaintenanceContract__c !=null)
            {
                s.add(String.valueOf(aip.ServiceMaintenanceContract__c).substring(0,15)+'-'+String.valueOf(aip.InstalledProduct__c).substring(0,15));
            }
        }
        aipList2=[SELECT Tech_SetOfId__c FROM AssociatedInstalledProduct__c Where Tech_SetOfId__c in :s];
        set<String> extStr = new Set<String>();
        
        if(aipList2.size()>0)
        {
          for(AssociatedInstalledProduct__c aip: aipList1)
            {
                aip.addError('You have already added this installed product to the contract');
            }
        }
    } 
}