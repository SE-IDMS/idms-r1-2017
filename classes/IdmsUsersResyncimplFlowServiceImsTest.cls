@isTest
public class IdmsUsersResyncimplFlowServiceImsTest {
    static testmethod void testcreateAccounts(){
        try{ 
            test.Starttest();
                IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort bulkInputServiceImplPortcls=new IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort();
                bulkInputServiceImplPortcls.createAccounts('',new IdmsUsersResyncflowServiceIms.accountCreateRequest());
            test.Stoptest();
        }
    catch(Exception e){}
    }
    
    static testmethod void testcreateContacts(){
        try{ 
            test.Starttest();
                IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort bulkInputServiceImplPortcls=new IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort();
                bulkInputServiceImplPortcls.createContacts('',new IdmsUsersResyncflowServiceIms.userCreateRequest(),new IdmsUsersResyncflowServiceIms.accessModelBean(),new IdmsUsersResyncflowServiceIms.accessNodeBean(),false);
            test.Stoptest();
        }
    catch(Exception e){}
    }
    
    static testmethod void testupdateAccounts(){
        try{ 
            test.Starttest();
                IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort bulkInputServiceImplPortcls=new IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort();
                bulkInputServiceImplPortcls.updateAccounts('',new IdmsUsersResyncflowServiceIms.accountsUpdateRequest());
            test.Stoptest();
        }
    catch(Exception e){}
    }
    
    static testmethod void testupdateContacts(){
        try{ 
            test.Starttest();
                IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort bulkInputServiceImplPortcls=new IdmsUsersResyncimplFlowServiceIms.BulkInputServiceImplPort();
                bulkInputServiceImplPortcls.updateContacts('',new IdmsUsersResyncflowServiceIms.contactsUpdateRequest());
            test.Stoptest();
        }
    catch(Exception e){}
    }
    
}