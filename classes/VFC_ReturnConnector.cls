/*
    Author          : Ram Chilukuri
    Date Created    : 08/March/2013
    Description     : Class utilised by Synchronize Button.
*/

public class VFC_ReturnConnector 
 {
    public String ExternalReferencesId;
    public String userID;
    public CSE_ExternalReferences__c extReferece;
    public ITB_Asset__c itbasset;
    Public Boolean flag{get;set;}
    public List<String> newuser;
    public String RRURL;
        
        public VFC_ReturnConnector (ApexPages.StandardController controller)
            {
                ExternalReferencesId=Controller.getID();         
            }
            
        public pageReference redirectToextRefereceExternalSystem()
            {
                System.Debug('***** VFC_SynchronizeExternalReference Class Start****');    
                                if(ExternalReferencesId!=null)
                                    {
                                        extReferece = [select IDinBackOfficeSystem__c,name,case__c, CountryOfBackOfficeSystem__c,BackOfficeSystem__c,Case__r.CaseSymptom__c, Type__c from CSE_ExternalReferences__c where ID=:ExternalReferencesId];                    
                                         System.debug(' extReferece '+ extReferece );
                                    }
                                    
                                 if (!(extReferece.Type__c==Label.CLMAY13ITBRR04 && extReferece.CountryOfBackOfficeSystem__c == Label.CLMAY13ITBRR05 && extReferece.BackOfficeSystem__c== Label.CLMAY13ITBRR06 ))
                                     {
                                        flag=true;                                           
                                       ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLMAY13ITBRR07));          
                                        return null;
                                     }
                                    
                                if (extReferece.IDinBackOfficeSystem__c!=null)
                                    {
                                        flag=true;    
                                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLMAY13ITBRR03));          
                                        return null; 
                                    }
                                 else if (extReferece.Case__r.CaseSymptom__c==null)
                                    {
                                        flag=true;    
                                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLMAY13ITBRR01));          
                                        return null; 
                                    }
                                else 
                                    {
                                        List<ITB_Asset__c> itbassetlst = [select case__c from ITB_Asset__c where Case__c=:extReferece.case__c and PrimaryFlag__c=:true];
                                         System.Debug('***** itbassetlst.size**'+itbassetlst.size());    
                                        if (itbassetlst.size()!=0)
                                            {
                                                userID=[Select UserName from user where ID=:UserInfo.getUserID()].userName;                    
                                                if(userID!=null)
                                                    {
                                                        newuser=UserID.split('@');           
                                                    } 
                                                     RRURL = (System.label.CLMAY13ITBRR09+
                                                     '?UserId='+newuser[0]+
                                                     '&externalReferenceId='+extReferece.id +
                                                     '&backOfficeSystem='+extReferece.BackOfficeSystem__c +
                                                     '&countryOfBackOfficeSystem='+extReferece.CountryOfBackOfficeSystem__c +
                                                     '&caseId='+extReferece.Case__c+
                                                     '&type='+extReferece.Type__c+
                                                     '&bfoSessionId='+ UserInfo.getSessionId() +
                                                     '&bfoServerUrl=' + Label.CL00597);
                                                       System.debug(' RRURL '+ RRURL );
                                                    return new pageReference(RRURL+'&view=Yes');
                                            }
                                        else 
                                            {
                                                flag=true;    
                                                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,Label.CLMAY13ITBRR02));          
                                                return null;
                                                
                                            }
                                    }
             }
        
         
}