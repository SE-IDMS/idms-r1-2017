@isTest
public class VFC_ViewEditCAPPlatformingScoring_TEST{
    
    @testSetup static void setUpData() {
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
                
        Country__c Ctry = Utils_TestMethods.createCountry(); 
        insert Ctry;
        
        String user_id = UserInfo.getUserId();       
        
        SFE_IndivCAP__c objIndivCap=new SFE_IndivCAP__c(Name='test12',Country__c=Ctry.Id,Status__c='In Progress', StartDate__c=system.now().date(),
                                                      AssignedTo__c=user_id,G1VisitFreq__c=12,G2VisitFreq__c=13,G3VisitFreq__c=14,Q1VisitFreq__c=11,
                                                      Q2VisitFreq__c=15,Q3VisitFreq__c=16,S1VisitFreq__c=17,S2VisitFreq__c=18,S3VisitFreq__c=19);
        insert objIndivCap;  
        acc.ClassLevel1__c = 'EU';
        Update acc;
        
        SFE_PlatformingScoring__c  objPlatformScore=new SFE_PlatformingScoring__c(PlatformedAccount__c=acc.Id,IndivCAP__c=objIndivCap.Id,
                                                                Score__c=51,MktShare__c=24,PAMPlaforming__c=23,DirectSalesPlatforming__c=6383,
                                                                IndirectSalesPlatforming__c=4392,Q5OpenCommRelationshipSE__c='Unwilling to do business',Q1Rating__c='2',Q2Rating__c='3',Q3Rating__c='2',Q4Rating__c='1',
                                                                Q5Rating__c='1',Q6Rating__c='2',Q7Rating__c='2',Q8Rating__c='1');
        insert objPlatformScore;
    }

    static testMethod void ViewEditCAPPlatformingScoringtest1() {
        List<CAPGlobalMasterData__c> listCAPGlobalMasterData = new List<CAPGlobalMasterData__c>();
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c objCAPG =new CAPGlobalMasterData__c(QuestionInterpretation__c='TestQuestionInterpretation',
                                                                       AnswerInterpretation1__c='TestAnswerInterpretation1',
                                                                       AnswerInterpretation2__c='TestAnswerInterpretation2',
                                                                       AnswerInterpretation3__c='TestAnswerInterpretation3',
                                                                       AnswerInterpretation4__c='TestAnswerInterpretation4',
                                                                       ClassificationLevel1__c='EU'
                                                                       );
            objCAPG.QuestionSequence__c='Q'+(i+1);
            listCAPGlobalMasterData.add(objCAPG);                                                          
        }
        
        if(!listCAPGlobalMasterData.isEmpty()){
            insert listCAPGlobalMasterData;
        system.AssertEquals(8,listCAPGlobalMasterData.size());
        }            
        SFE_PlatformingScoring__c  objPlatformScore = [select PlatformedAccount__c,IndivCAP__c,
                                                                Score__c,MktShare__c,PAMPlaforming__c,DirectSalesPlatforming__c,
                                                                IndirectSalesPlatforming__c,Q5OpenCommRelationshipSE__c,Q1Rating__c,Q2Rating__c,Q3Rating__c,Q4Rating__c,
                                                                Q5Rating__c,Q6Rating__c,Q7Rating__c,Q8Rating__c from SFE_PlatformingScoring__c];
        test.Starttest();
        ApexPages.StandardController controller = new ApexPages.StandardController(objPlatformScore);
        ApexPages.currentPage().getParameters().put('Id',objPlatformScore.Id);
        VFC_ViewEditCAPPlatformingScoring ViewEditCAPPlatformingScoring= new VFC_ViewEditCAPPlatformingScoring(controller);
        ViewEditCAPPlatformingScoring.EditPS();
        ViewEditCAPPlatformingScoring.Save();
        ViewEditCAPPlatformingScoring.Deleteps();
        test.Stoptest();
     }
     static testMethod void ViewEditCAPPlatformingScoringtest2() {
    
        List<CAPGlobalMasterData__c> listCAPGlobalMasterData = new List<CAPGlobalMasterData__c>();
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c objCAPG =new CAPGlobalMasterData__c(QuestionInterpretation__c='TestDef_QuestionInterpretation',
                                                                       AnswerInterpretation1__c='TestDef_AnswerInterpretation1',
                                                                       AnswerInterpretation2__c='TestDef_AnswerInterpretation1',
                                                                       AnswerInterpretation3__c='TestDef_AnswerInterpretation1',
                                                                       AnswerInterpretation4__c='TestDef_AnswerInterpretation1',
                                                                       ClassificationLevel1__c='Default');
            objCAPG.QuestionSequence__c='Q'+(i+1);
            listCAPGlobalMasterData.add(objCAPG);                                                           
        }
        if(!listCAPGlobalMasterData.isEmpty()){
            insert listCAPGlobalMasterData;
        }
        system.AssertEquals(8,listCAPGlobalMasterData.size());
        SFE_PlatformingScoring__c  objPlatformScore = [select PlatformedAccount__c,IndivCAP__c,
                                                                Score__c,MktShare__c,PAMPlaforming__c,DirectSalesPlatforming__c,
                                                                IndirectSalesPlatforming__c,Q5OpenCommRelationshipSE__c,Q1Rating__c,Q2Rating__c,Q3Rating__c,Q4Rating__c,
                                                                Q5Rating__c,Q6Rating__c,Q7Rating__c,Q8Rating__c from SFE_PlatformingScoring__c];
         test.Starttest();
         ApexPages.StandardController controller = new ApexPages.StandardController(objPlatformScore);
         ApexPages.currentPage().getParameters().put('Id',objPlatformScore.Id);
         VFC_ViewEditCAPPlatformingScoring ViewEditCAPPlatformingScoring= new VFC_ViewEditCAPPlatformingScoring(controller);    
         ViewEditCAPPlatformingScoring.Save(); 
         test.Stoptest();
     }   
}