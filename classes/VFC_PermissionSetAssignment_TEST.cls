@isTest(seealldata=false)
public class VFC_PermissionSetAssignment_TEST {

     static testMethod void testPsAssignments() {
  
       User adminUser = [SELECT Id FROM User WHERE ProfileId = '00eA0000000uVHP' AND isActive = true LIMIT 1];
        System.runAs (adminUser) {
            Test.startTest();

            List<PermissionSetGroup__c> permissionSetGroupList =  permissionTestMethods.createPermissionSetGroups();        
            ApexPages.StandardController userController = new ApexPages.StandardController(adminUser);
            
            VFC_PermissionSetAssignment viewGroupPermissionController = new VFC_PermissionSetAssignment(userController);        
            
            // Populate sObject List
            viewGroupPermissionController.populateSObjectList();
            
            // Delegated Admin User Info setup
            viewGroupPermissionController.retrieveDaUserInfo();
            viewGroupPermissionController.delGroup();
            viewGroupPermissionController.editPermissions();
            viewGroupPermissionController.backToRead();
            // Search by Name
            viewGroupPermissionController.searchName            = permissionSetGroupList.get(0).Name;
            viewGroupPermissionController.searchType            = permissionSetGroupList.get(0).Type__c;
            viewGroupPermissionController.searchStream          = permissionSetGroupList.get(0).Stream__c;
            viewGroupPermissionController.objectName            = 'Contact';
           
           
            viewGroupPermissionController.selectedAccessLevel   = 'Read';
                        
            viewGroupPermissionController.search();
            
            ApexPages.currentPage().getParameters().put('selectId', permissionSetGroupList.get(0).Id);
            
            viewGroupPermissionController.selectGroup();

            ApexPages.currentPage().getParameters().put('removeId', permissionSetGroupList.get(0).Id);
            
            viewGroupPermissionController.removeGroup();

            ApexPages.currentPage().getParameters().put('selectId', permissionSetGroupList.get(0).Id);
            
            viewGroupPermissionController.selectGroup();
            
            viewGroupPermissionController.saveAssignment();

            ApexPages.currentPage().getParameters().put('viewId', permissionSetGroupList.get(0).Id);            
            viewGroupPermissionController.viewrequest();
            
            viewGroupPermissionController.delGroup();
            
            
            Test.stopTest();   
        }  
     }
     
    static testMethod void testSearchIndividual() {
 
        User adminUser = [SELECT Id FROM User WHERE ProfileId = '00eA0000000uVHP' AND isActive = true LIMIT 1];

        System.runAs (adminUser) {
            Test.startTest();

            List<PermissionSetGroup__c> permissionSetGroupList =  permissionTestMethods.createPermissionSetGroups();        
            ApexPages.StandardController userController = new ApexPages.StandardController(adminUser);
            
            VFC_PermissionSetAssignment viewGroupPermissionController = new VFC_PermissionSetAssignment();
            viewGroupPermissionController = new VFC_PermissionSetAssignment(userController);        
            
            // Populate sObject List
            viewGroupPermissionController.populateSObjectList();
            
            // Delegated Admin User Info setup
            viewGroupPermissionController.retrieveDaUserInfo();
            
            // Search by Name
            viewGroupPermissionController.searchCriteria.searchIndividualPsOnly__c = true;
            viewGroupPermissionController.objectName      = 'Contact';
            
            viewGroupPermissionController.selectedAccessLevel   = 'Read';                        
            viewGroupPermissionController.search();
            
            viewGroupPermissionController.selectedAccessLevel   = 'Create';                        
            viewGroupPermissionController.search();
            
            viewGroupPermissionController.selectedAccessLevel   = 'Edit';                        
            viewGroupPermissionController.search();
            
            viewGroupPermissionController.selectedAccessLevel   = 'Delete';                        
            viewGroupPermissionController.search();
            
            viewGroupPermissionController.selectedAccessLevel   = 'ViewAll';                        
            viewGroupPermissionController.search();
            
            viewGroupPermissionController.selectedAccessLevel   = 'ModifyAll';                        
            viewGroupPermissionController.search();                                                

            Test.stopTest();   
        }  
    }   

    static testMethod void testSearchIndividual1() {
    
    User adminUser = [SELECT Id FROM User WHERE ProfileId = '00eA0000000uVHP' AND isActive = true LIMIT 1];
      bFO_Object__c bfoObject1 = new bFO_Object__c(Name='Case',Technical_API_Name__c ='Case');
      list<bFO_Object__c> bfoObject = new List<bFO_Object__c>();
      bfoObject.add(bfoObject1);
      insert bfoObject;
   
        System.runAs (adminUser) {
            Test.startTest();

            List<PermissionSetGroup__c> permissionSetGroupList =  permissionTestMethods.createPermissionSetGroups();        
            ApexPages.StandardController userController = new ApexPages.StandardController(adminUser);
            
            VFC_PermissionSetAssignment viewGroupPermissionController = new VFC_PermissionSetAssignment();
            viewGroupPermissionController = new VFC_PermissionSetAssignment(userController);        
            
            // Populate sObject List
            viewGroupPermissionController.populateSObjectList();
            
            // Delegated Admin User Info setup
            viewGroupPermissionController.retrieveDaUserInfo();
            viewGroupPermissionController.bfo.bFO_Object__c     = bfoObject[0].id;
            // Search by Name
            viewGroupPermissionController.searchCriteria.searchIndividualPsOnly__c = true;
            viewGroupPermissionController.objectName      = 'Case';
            
            viewGroupPermissionController.selectedAccessLevel   = 'Read';                        
            viewGroupPermissionController.search();
            
            viewGroupPermissionController.selectedAccessLevel   = 'Create';                        
            viewGroupPermissionController.search();
            
            viewGroupPermissionController.selectedAccessLevel   = 'Edit';                        
            viewGroupPermissionController.search();
            
            viewGroupPermissionController.selectedAccessLevel   = 'Delete';                        
            viewGroupPermissionController.search();
            
            viewGroupPermissionController.selectedAccessLevel   = 'ViewAll';                        
            viewGroupPermissionController.search();
            
            viewGroupPermissionController.selectedAccessLevel   = 'ModifyAll';                        
            viewGroupPermissionController.search();                                                

            Test.stopTest();
    
        }
    
    
    }
    
}