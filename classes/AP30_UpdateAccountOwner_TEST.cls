/*
    Author          : Accenture Team
    Date Created    : 08/08/2011
    Description     : Test class for AP30_UpdateAccountOwner class.
*/
@isTest
private class AP30_UpdateAccountOwner_TEST 
{
   static testmethod void testCheckAccountUpdate()
    {
    // Implement test code
        test.startTest();
        List<BypassTriggersOnLeadConversion__c> BypassTriggersOnLeadConversion = new List<BypassTriggersOnLeadConversion__c>{
            new BypassTriggersOnLeadConversion__c(Name='Fielo_ContactTriggers'),
            new BypassTriggersOnLeadConversion__c(Name='AP_CONCreateWithSEContactID'),
            new BypassTriggersOnLeadConversion__c(Name='AP_ContactPreferredAgent'),
            new BypassTriggersOnLeadConversion__c(Name='AP_Contact_PartnerUserUpdate'),
            new BypassTriggersOnLeadConversion__c(Name='AP_updateContactOwner')
            };
        insert BypassTriggersOnLeadConversion;
        Utils_SDF_Methodology.BypassOnLeadConvertion = true;
        Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'India';
        insert ct;
        List<Account> accList = new List<Account>();
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
        accList.add(acc1);
        Account acc2 = new Account(Name='Acc2', ClassLevel1__c='SC', City__c='city', ZipCode__c='12345', Country__c=ct.id,Type='GMO',ConFirstName__c='TestConFirst', ConLastName__c='TestConLast', ConEmail__c='poo@gmail.com', ConWorkPhone__c='123456', ConMobile__c='76543' );
        accList.add(acc2);
        insert accList;
        
        Account acc3 = new Account(Name='Acc2', ClassLevel1__c='SC', City__c='city', ZipCode__c='12345', Country__c=ct.id,Type='GMO',ConFirstName__c='TestConFirst', ConLastName__c='TestConLast', ConEmail__c='poo@gmail.com', ConWorkPhone__c='123456', ConMobile__c='76543' );
        
        //Contact con1 = Utils_TestMethods.createContact(acc1.Id,'TestContact');
        //insert con1;
        //Case case1 = Utils_TestMethods.createCase(acc1.Id, con1.Id, 'Open');
        //insert case1;
        //Opportunity opp1= Utils_TestMethods.createOpenOpportunity(acc1.Id);
        //insert opp1;
        //Lead lead1 = Utils_TestMethods.createLeadWithAcc(acc1.Id, ct.Id,1);
        //insert lead1;
        CustomerSatisfactionSurvey__c css = new CustomerSatisfactionSurvey__c(GoldenCustomerSatisfactionSurveyId__c= 'CustomerSatisfactionSurveyId',Account__c = acc1.id);
        insert css;
        acc1.TobeDeleted__c = true;
        acc1.ReasonForDeletion__c = 'Created in error';
        try
        {
            update acc1;
        }
        catch(Exception e)
        {
            String excep;
            if(e.getMessage().contains(Label.CLOCT13ACC02))
            {
                excep = 'YES';
            }
            system.assertEquals(excep, 'YES');
        }
        try{
            insert acc3;
        }
        catch(Exception ex)
        {
            System.debug('Contact not created. This is desired result');
        }
        test.stopTest();        
  }
   
   static testMethod void populateAccountOwnerTestMethod() 
   {  
        Profile profile = [select id from profile where name='System Administrator'];    
        User user1 = Utils_TestMethods.createStandardUser(profile.id,'TestUsr1');
        user1.UserBusinessUnit__c='BD';
        user1.BypassVR__c=true;
        database.insert(user1);
        
        Country__c ct = Utils_TestMethods.createCountry();
        insert ct;        
        
        list<account> newAccount = new list<account>();
        for(integer i= 0;i<5;i++)
        {
            Account accounts = Utils_TestMethods.createAccount();
            newAccount.add(accounts);
        }
        Database.insert(newAccount); 
                
        
        list<account> newAccounts = new list<account>();
        for(account acc:newAccount)
        {
            acc.OwnerId=user1.id;
            newAccounts.add(acc);
        }
        database.update(newAccounts);
        Contact cont = Utils_TestMethods.createContact(newAccounts[0].Id,'Contact1');
        cont.OwnerId = '005A0000001PC0pIAG';
        insert cont;
        list<CS_AccountSimplification__c> asList = new List<CS_AccountSimplification__c>();
        if(CS_AccountSimplification__c.getValues('EU__EN1')== null){
        
            CS_AccountSimplification__c as0= new CS_AccountSimplification__c();
            as0.Name = 'EU__EN1';
            as0.ClassificationLevel1__c ='EU';
            as0.MarketSegment__c ='EN1';
            as0.LeadingBusiness__c = 'EN';
            asList.add(as0);
        }
        if(CS_AccountSimplification__c.getValues('WD__') == null){
            CS_AccountSimplification__c as1= new CS_AccountSimplification__c();
            as1.Name = 'WD__'; 
            as1.ClassificationLevel1__c ='WD';   
            as1.LeadingBusiness__c  ='PW';
            asList.add(as1);
        }
        if(CS_AccountSimplification__c.getValues('WD_WD1_') == null){
            CS_AccountSimplification__c as2= new CS_AccountSimplification__c();
            as2.Name = 'WD_WD1_';
            as2.ClassificationLevel1__c ='WD';
            as2.ClassificationLevel2__c ='WD1';
            as2.LeadingBusiness__c = 'PW';
            asList.add(as2);
        } 
            
        if(asList!= null && asList.size()>0)
        Database.insert(asList); 
    
        
        
        list<account> accList = new list<account>();
        
        Account account1 = Utils_TestMethods.createAccount();    
        account1.ClassLevel1__c = 'EU'; 
        account1.MarketSegment__c = 'EN1';
        
        Account account2 = Utils_TestMethods.createAccount();    
        account2.ClassLevel1__c = 'WD'; 
        
        Account account3 = Utils_TestMethods.createAccount();    
        account3.ClassLevel1__c = 'WD'; 
        account3.ClassLevel2__c = 'WD1';
            
        Account account4 = Utils_TestMethods.createAccount();    
        account4.ClassLevel1__c = 'WD'; 
        account4.ClassLevel2__c = 'WD1';
        account4.MarketSegment__c = 'EN1';
            
        accList.add(account1);
        accList.add(account2);  
        accList.add(account3);
        accList.add(account4);
       
        test.startTest();
        
        System.runAs(user1)
        {
            Account consumerAccount1  = Utils_TestMethods.createConsumerAccount(ct.Id);
            insert consumerAccount1; 
            String leadingBU1 = [Select LeadingBusiness__c from Account where id = :consumerAccount1.id].LeadingBusiness__c;
            System.debug('--->> '+leadingBU1); 
            
        }
        
        user1.UserBusinessUnit__c='TST';
        update user1;
        System.runAs(user1)
        {
            Account consumerAccount2  = Utils_TestMethods.createConsumerAccount(ct.Id);
            consumerAccount2.OwnerId = user1.Id;
            insert consumerAccount2;
            
            String leadingBU2 = [Select LeadingBusiness__c from Account where id = :consumerAccount2.id].LeadingBusiness__c;
            System.debug('--->> '+leadingBU2); 
            
        } 
        
        
        Database.insert(accList); 
        System.debug('Account Leading BU in the end:'+account1.LeadingBusiness__c);
        System.debug('Account Leading BU in the end:'+account2.LeadingBusiness__c);
        System.debug('Account Leading BU in the end:'+account3.LeadingBusiness__c);
        System.debug('Account Leading BU in the end:'+account4.LeadingBusiness__c);   
        
        test.stopTest();
    }
    
    static testmethod void updatecontactAddress() 
    {
         Country__c ct = Utils_TestMethods.createCountry();
            ct.name = 'India';
            insert ct;
            
               User[] usr =[select id from User where isActive=true limit 2];
            
        Account acc = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234',Pagent__c=usr[0].id,PBAAgent__c=usr[1].id);      
        insert acc;
    
        Contact cont = Utils_TestMethods.createContact(Acc.Id,'Contact1');
        insert cont;
    
        acc.Street__c='street';
        update acc;
    }
    static testmethod void updatemarketosync() {
        Country__c country = Utils_TestMethods.createCountry();
        country.CountryCode__c='GB';
        insert country;
        
        Country__c china = Utils_TestMethods.createCountry();
        country.CountryCode__c='CN';
        insert china;
        
        Account acc1 = Utils_TestMethods.createAccount(userInfo.getUserId(),country.id);
        acc1.City__c='test city111';
        acc1.Street__c='test street1';
        acc1.ZipCode__c='34534';
        acc1.ClassLevel1__c='RT';
    	insert acc1;
        
        Contact cont = Utils_TestMethods.createContact(acc1.Id,'Contact1');
        cont.Email = 'swap@swap.com';
        cont.Country__c = country.id;
        cont.MarcomPrefEmail__c = 'Y';
        insert cont;
        
        Set<id> accId = new Set<id>();
        accId.add(acc1.id);
        AP30_UpdateAccountOwner.updateSynchwithmarketo(accId);
        List<Account> accList = new List<Account>();
        accList.add(acc1);
        Set<String> countryCodeLst = new Set<String>();
        Set<String> stateCodeLst = new Set<String>();
        countryCodeLst.add('GB');
        stateCodeLst.add('AB');
        AP30_UpdateAccountOwner.updateCustomAddrFields(accList,countryCodeLst,stateCodeLst);
    }
}