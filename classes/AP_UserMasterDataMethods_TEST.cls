/*
Author          : Anoop Innocent(SFDC) 
Date Created    : 10/08/2016
Description     : Test Class created for MatchingModule class
*/
@isTest

public class AP_UserMasterDataMethods_TEST
{
    
    public static user runUser = Utils_TestMethods.createStandardUser('runUser');
    
    @testSetup static void setup() { 
        
        //User runUser = Utils_TestMethods.createStandardUser('runUser');
        
        //insert runUser;
        
        //Query For the profile SE – Unqualified Account Holder
        Id profileId2 = [select id from profile where name='SE – Unqualified Account Holder'].id;
        //Query For the Role of the OwnerUser
        Id roleId1 = [select id from userRole where name='CEO'].id;
        
        // create account owner user with profile SE – Unqualified Account Holder
        User ownUser = Utils_TestMethods.createStandardUser('OwnUser'); 
        ownUser.ProfileId     = profileId2;
        ownUser.useRroleid    = roleId1;
        insert ownUser;
        
        
        System.runAs(runUser){
            Country__c country = Utils_TestMethods.createCountry();
            insert country ;
            StateProvince__c state = Utils_TestMethods.createStateProvince(country.id);
            state .StateProvinceExternalId__c='FR01';
            insert state ;
            
            //Query the Digital Account Record type
            ID RecTypeId = [Select id from recordType where DeveloperName =: Label.CLQ316IDMS533 And SobjectType =: Label.CLQ316IDMS534].id;
            
            List<Account> AccList = New List<Account>();
            List <Contact> conList = new List<Contact>();
            
            Account acc = Utils_TestMethods.createAccount();
            acc.Name = 'TestCompany';
            acc.ClassLevel1__c ='SI';
            acc.City__c='testCity';
            acc.Country__c = country.id;
            acc.ownerid = ownUser.id; 
            acc.Street__c = 'TestStreet';
            acc.StateProvince__c  = state.id;
            
            AccList.add(acc);
            
            Account acc1 =new Account (Name='Unknown',ownerid = ownUser.id,recordTypeId =RecTypeId);
            accList.add(acc1);
            
            insert accList;
            
            Contact con = Utils_TestMethods.createContact(acc.id,'IDMS');
            con.Email='IDMS@accenture.com';
            con.MobilePhone='8885720489';
            con.StateProv__c = state.id;
            con.ownerid = ownUser.id;
            conList.add(con);
            
            Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=acc1.id,ownerid = ownUser.id);
            conList.add(con1);
            insert conList;
            
            //Query for the External Identity User profile
            Id profileId = [select id from profile where name='External Identity User'].id;
            
            List<User> usersList = New List<User>();
            User user = new User(alias = 'test123', email='anoop.it@non.schneider-electric.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profileId,IsActive =true,
                                 ContactId = con.id,
                                 CompanyName = 'TestCompany',
                                 IDMSClassLevel1__c = 'SI',
                                 IDMSClassLevel2__c = '',
                                 //IDMSCompanyMarketServed__c = 'Test',
                                 IDMSCompanyNbrEmployees__c = '100 to 999',
                                 //IDMSAnnualRevenue__c = 50000,
                                 //newAccount.VATNumber__c = IDMSTaxIdentificationNumber__c,
                                 Company_Address1__c = 'TestStreet',
                                 //IDMS_AdditionalAddress__c = '',
                                 Company_City__c = 'testCity',
                                 Company_Postal_Code__c = '560066',
                                 Company_State__c= '01',
                                 //IDMS_State__c = 'Karnataka',
                                 Company_Country__c = 'FR',
                                 //IDMS_POBox__c = '12341',
                                 //Company_Website__c = '',
                                 
                                 
                                 MobilePhone = '8885720489',
                                 FirstName = 'tmanu',
                                 //LastName = 'Test',
                                 
                                 //Country__c = 'IN',
                                 IDMS_PreferredLanguage__c = 'en',
                                 DefaultCurrencyIsoCode = 'INR',
                                 //TECH_MatchAndRelink__c = true,
                                 //IDMSMiddleName__c = 'Time',
                                 IDMS_User_Context__c = '@Work',
                                 IDMSSalutation__c = 'Z003',
                                 //Department ='Test',
                                 //Fax = '',
                                 //IDMSContactGoldenId__c= '1234',  
                                 timezonesidkey='America/Los_Angeles', username='tester123.partcreate@noemail.com',
                                 IsPortalSelfRegistered = True);
            usersList.add(user);
            User user5 = new User(alias = 'test123', email='anoop.ito@non.schneider-electric.com',
                                  emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                  localesidkey='en_US', profileid = profileId,IsActive =true,
                                  TECH_MatchAndRelink__c = true,
                                  ContactId = con1.id,
                                  CompanyName = 'TestCompany',
                                  FirstName = 'tmanu',
                                  isIDMSUser__c = true,
                                  DefaultCurrencyIsoCode = 'INR',
                                  IDMS_User_Context__c = '@Work',  
                                  timezonesidkey='America/Los_Angeles', username='tester1234.partcreate@noemail.com',
                                  IsPortalSelfRegistered = True);
            
            usersList.add(user5);
            
            insert usersList;
            
        }
        
        
    }
    static testmethod void method1()
    {
        
        
        
        //Test.StartTest() ; 
        //User user4 = [select id,TECH_MatchAndRelink__c   from user where profile.name = 'System Administrator' and isActive = true limit 1];
        System.runAs(runUser){
            test.StartTest();
            User user3 = [select id,TECH_MatchAndRelink__c   from user where email = 'anoop.it@non.schneider-electric.com' limit 1];
            user3.TECH_MatchAndRelink__c = true;
            //user3.Company_Website__c = 'WWW.google.com';
            //user3.IDMSMiddleName__c = 'Sam';
            update user3;
            
            //user2.Company_Website__c = 'WWW.google.com';
            //update user2;
            
            Test.StopTest();
        }        
    }
    
    static testmethod void method2()
    {
        
        
        
        //Test.StartTest() ; 
        //User user4 = [select id,TECH_MatchAndRelink__c   from user where profile.name = 'System Administrator' and isActive = true limit 1];
        System.runAs(runUser){
            test.StartTest();
            AP_MatchingModule.FORCE_L1 = false;
            User user3 = [select id,TECH_MatchAndRelink__c   from user where email = 'anoop.it@non.schneider-electric.com' limit 1];
            user3.TECH_MatchAndRelink__c = false;
            user3.IDMSCompanyNbrEmployees__c = '100 to 999';
            user3.IDMSMiddleName__c = 'Sam';
            //user3.IDMS_POBox__c  = '1234';
            //user3.IDMSClassLevel1__c = 'SI';
            //user3.Company_Website__c = 'WWW.google.com';
            update user3;
            
            //user2.Company_Website__c = 'WWW.google.com';
            //update user2;
            
            Test.StopTest();
        }        
    }
    static testmethod void method3()
    {
        
        
        
        //Test.StartTest() ; 
        //User user4 = [select id,TECH_MatchAndRelink__c   from user where profile.name = 'System Administrator' and isActive = true limit 1];
        System.runAs(runUser){
            test.StartTest();
            User user6 = [select id,TECH_MatchAndRelink__c   from user where profile.name = 'SE – Unqualified Account Holder' limit 1];
            user6.TECH_MatchAndRelink__c = true;
            user6.Company_Website__c = 'WWW.google.com';
            user6.IDMSMiddleName__c = 'Sam';
            user6.IDMSClassLevel1__c = 'SI';
            
            List<User> usersList = new List<User>();
            usersList.add(user6);
            
            user[] users1 = usersList;
            //AP_UserMasterDataMethods.syncAccountAndContact(user6);
            AP_UserMasterDataMethods.processMatchAndRelink(users1);
            
            Test.StopTest();
        }        
    }
    static testmethod void method4()
    {
        
        
        
        //Test.StartTest() ; 
        //User user4 = [select id,TECH_MatchAndRelink__c   from user where profile.name = 'System Administrator' and isActive = true limit 1];
        System.runAs(runUser){
            test.StartTest();
            User user7 = [select id,TECH_MatchAndRelink__c,contactid,AccountId,Company_Postal_Code__c,Company_City__c,Company_Address1__c,
                          Company_Country__c,Company_State__c,CompanyName,IDMSClassLevel1__c from user where email = 'anoop.it@non.schneider-electric.com' limit 1];
            user7.TECH_MatchAndRelink__c = false;
            user7.IDMSCompanyNbrEmployees__c = '100 to 999';
            user7.IDMSMiddleName__c = 'Sam';
            user7.Department = 'test';
            //user7.IDMSClassLevel1__c = 'SI';
            user7.IDMSAnnualRevenue__c = 1500;
            user7.Company_Website__c = 'WWW.google.com';
            //update user3;
            
            List<User> usersList = new List<User>();
            usersList.add(user7);
            
            user[] users2 = usersList;
            
            AP_UserMasterDataMethods.enrichAccountAndContact(users2);
            //user2.Company_Website__c = 'WWW.google.com';
            //update user2;
            
            Test.StopTest();
        }        
    }
    static testmethod void method5()
    {
        
        
        
        //Test.StartTest() ; 
        //User user4 = [select id,TECH_MatchAndRelink__c   from user where profile.name = 'System Administrator' and isActive = true limit 1];
        System.runAs(runUser){
            test.StartTest();
            User user7 = [select id,TECH_MatchAndRelink__c,contactID,Company_Postal_Code__c,Company_City__c,Company_Address1__c,
                          Company_Country__c,Company_State__c,CompanyName,IDMSClassLevel1__c from user where email = 'anoop.ito@non.schneider-electric.com' limit 1];
            user7.TECH_MatchAndRelink__c = true;
            user7.IDMSCompanyNbrEmployees__c = '100 to 999';
            user7.IDMSMiddleName__c = 'Sam';
            user7.IDMSClassLevel1__c = 'SI';
            user7.IDMSAnnualRevenue__c = 1500;
            user7.Company_Address1__c = 'TestStreet';
            user7.Company_City__c = 'testCity';
            user7.Company_Postal_Code__c = '560066';
            user7.Company_State__c= '01';
            user7.Company_Country__c = 'FR';
            //update user7;
            
            List<User> usersList = new List<User>();
            usersList.add(user7);
            
            user[] users2 = usersList;
            
            AP_UserMasterDataMethods.matchAndRelink(users2);
            
            Test.StopTest();
        }        
    }
    static testmethod void method6()
    {
        
        
        
        //Test.StartTest() ; 
        //User user4 = [select id,TECH_MatchAndRelink__c   from user where profile.name = 'System Administrator' and isActive = true limit 1];
        System.runAs(runUser){
            test.StartTest();
            User user7 = [select id,TECH_MatchAndRelink__c,contactID,Company_Postal_Code__c,Company_City__c,Company_Address1__c,
                          Company_Country__c,Company_State__c,isIDMSUser__c, CompanyName,IDMSClassLevel1__c from user where email = 'anoop.ito@non.schneider-electric.com' limit 1];
            user7.TECH_MatchAndRelink__c = true;
            user7.IDMSCompanyNbrEmployees__c = '100 to 999';
            user7.IDMSMiddleName__c = 'Sam';
            //user7.isIDMSUser__c = true;
            user7.IDMSClassLevel1__c = 'SI';
            user7.IDMSAnnualRevenue__c = 1500;
            user7.Company_Address1__c = 'TestStreet';
            user7.Company_City__c = 'testCity';
            user7.Company_Postal_Code__c = '560066';
            user7.Company_State__c= '01';
            user7.Company_Country__c = 'FR';
            update user7;
            
            
            Test.StopTest();
        }        
    }
}