@isTest
private class SEInsideSales360_ServicesTest {
  
  @isTest static void test() {
    Account a = new Account(Name='Test Account',
      BillingCity ='SEATTLE',
      BillingState = 'WA',
      BillingCountry = 'US',
      BillingStreet = '701 PIKE ST, FLOOR SEVEN, SUITE 700',
      BillingPostalCode = '98101');
    insert a;
    
    Contact c = new Contact(LastName='Contact', 
      FirstName='Test',
      WorkPhone__c='(000) 000-0000',
      Email='test@salesforce.com',
      MobilePhone='(111) 111-1111');

    c.AccountId = a.Id;
    insert c;

    String userId = SEInsideSales360_Services.getCurrentUserId();
    System.assertNotEquals(userId,null,'getCurrentUserId: User id is null');

    String contactId = c.Id;
    Account acc = SEInsideSales360_Services.getAccountByContact(contactId);
    System.assertEquals(acc.Id,a.Id,'getAccountByContact: Did not got the same account');

  }

  @isTest static void testNulls() {
    String nullId = null;
    Account acc1 = SEInsideSales360_Services.getAccountById(nullId);
    System.assertEquals(acc1,null,'getAccountById: Account is not null');

    Account acc2 = SEInsideSales360_Services.getAccountByContact(nullId);
    System.assertEquals(acc2,null,'getAccountByContact: Account is not null');
  }
  
    @isTest static void testrecordtype()
    {
        SEInsideSales360_Services.getEventRecordTypeId('Event');
    }
}