global class VFC_ContactsToUIMS {
    String contacts{get;set;}
    //String updateContacts {get;set;}
    ResponseResult resultToUpdate = new ResponseResult();
    
    public VFC_ContactsToUIMS(ApexPages.StandardController controller){
        string contactid = ApexPages.currentPage().getParameters().get('id');
        contacts = contactid;
        ResponseResult resp = new ResponseResult();
        resp = resultToUpdate = contactsToIFW(contacts);
        //if(resp.federatedId != '' && resp.federatedId != null)
        //  updateContacts = resp.federatedId;
        System.debug('>>>>'+resp);
        ApexPages.Message myMsg;
        if(resp.statusCode != null && resp.statusCode != '' && resp.statusCode == '200' && resp.responseCode != '' && resp.responseCode != null && resp.responseCode == '200')
            myMsg = new ApexPages.Message(ApexPages.Severity.INFO, System.Label.CLOCT15FIO056);
        else
            myMsg = new ApexPages.Message(ApexPages.Severity.INFO, System.Label.CLOCT15FIO057);
        
        ApexPages.addmessage(myMsg);
    }
        
    public void UpdateContact() {
        ApexPages.Message myMsg1;
        System.debug('Inside Update Method');
        List<Contact> contactLst = new List<Contact>([SELECT Id, PRMUIMSID__c FROM Contact WHERE Id = :contacts]);
        System.debug('Inside Update Method contactLst'+contactLst);
        List<Contact> contactsLstUpdate = new List<Contact>();
        if(resultToUpdate.federatedId != null && resultToUpdate.federatedId != '' && (contactLst[0].PRMUIMSID__c == null || contactLst[0].PRMUIMSID__c == '')) {
            contactLst[0].PRMUIMSID__c = resultToUpdate.federatedId;
        }
        contactLst[0].LastContactSyncDate__c = System.NOW();
        if(!(string.isBlank(resultToUpdate.responseMessage)))
            contactLst[0].ContactSyncResults__c = resultToUpdate.responseCode + ',' + resultToUpdate.responseMessage + ',' + resultToUpdate.description;
        else
            contactLst[0].ContactSyncResults__c = resultToUpdate.responseCode + ',' + resultToUpdate.description;

        contactsLstUpdate.add(contactLst[0]);
        System.debug('Inside Update Method contactsLstUpdate'+contactsLstUpdate);
        try{
            update contactsLstUpdate;
        }
        catch(Exception e) {
            contactLst[0].PRMUIMSID__c = '';
            contactLst[0].LastContactSyncDate__c = System.NOW();
            contactLst[0].ContactSyncResults__c = e.getMessage();
          //  contactsLstUpdate.add(contactLst[0]);
              myMsg1 = new ApexPages.Message(ApexPages.Severity.INFO, System.Label.CLOCT15FIO057);
            update contactLst;
        }
    }
    
    public ResponseResult contactsToIFW(string contactId) {
        ResponseResult result = new ResponseResult();
        List<Contact> c = new List<Contact>([SELECT Id, PRMUIMSID__c, FirstName, LastName, Email, Country__r.CountryCode__c, Z_PortalAdmin__c, Fax, JobDescr__c, JobFunc__c, 
                                PRMPrimaryContact__c, Phone, JobTitle__c, MobilePhone, CorrespLang__c, Salutation, Account.Name, Account.City__c, Account.Country__r.CountryCode__c,
                                Account.CurrencyIsoCode, Account.MarketSegment__c, Account.StateProvince__r.StateProvinceCode__c, Account.Street__c, Account.Website,
                                Account.ZipCode__c, Z_SEPADebitAdmin__c FROM Contact WHERE Id = :contactId]);
        User u = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()];
        List<Contact> lstContacts = new List<Contact>();
        String token = VFC_ProvisionSubscription.GenerateIFWAccessToken();
        String requestPart1='';
        
        //String requestPart1 = System.Label.CLOCT15FIO019+c[0].Id+System.Label.CLOCT15FIO058+c[0].AccountId+System.Label.CLOCT15FIO059+c[0].firstName+System.Label.CLOCT15FIO020+c[0].lastName+System.Label.CLOCT15FIO021+c[0].Email+System.Label.CLOCT15FIO022+c[0].Country__r.CountryCode__c+System.Label.CLOCT15FIO023+c[0].CorrespLang__c+System.Label.CLOCT15FIO024+c[0].Z_PortalAdmin__c+System.Label.CLOCT15FIO025+c[0].Id+System.Label.CLOCT15FIO026+c[0].Fax+System.Label.CLOCT15FIO027+c[0].JobDescr__c+System.Label.CLOCT15FIO028;
        //String requestPart2 = System.Label.CLOCT15FIO030+c[0].MobilePhone+System.Label.CLOCT15FIO031+c[0].PRMPrimaryContact__c+System.Label.CLOCT15FIO032+c[0].Phone+System.Label.CLOCT15FIO033+c[0].Salutation+System.Label.CLOCT15FIO060+c[0].Account.Name+System.Label.CLOCT15FIO034+c[0].Account.City__c+System.Label.CLOCT15FIO035+c[0].Account.Country__r.CountryCode__c+System.Label.CLOCT15FIO036+c[0].Account.CurrencyISOCode+System.Label.CLOCT15FIO037;
        //String requestPart3 = System.Label.CLOCT15FIO038+c[0].Account.StateProvince__r.StateProvinceCode__c+System.Label.CLOCT15FIO039+c[0].Account.Street__c+System.Label.CLOCT15FIO040+c[0].Account.Website+System.Label.CLOCT15FIO041+c[0].Account.ZipCode__c+System.Label.CLOCT15FIO042;
        /*
        if(String.isNotBlank(c[0].JobFunc__c)){
            toAddJFReq = System.Label.CLNOV15FIO001 + c[0].JobFunc__c + System.Label.CLOCT15FIO029;
            requestPart1 = requestPart1 + toAddJFReq;
        }
        if(String.isNotBlank(c[0].JobTitle__c)){
            toAddJTReq = System.Label.CLNOV15FIO002 + c[0].JobTitle__c + System.Label.CLNOV15FIO003;
            requestPart2 = toAddJTReq + requestPart2;
        }
        
        if(String.isNotBlank(c[0].Account.MarketSegment__c)){
            toAddMSReq = System.Label.CLNOV15FIO004 + c[0].Account.MarketSegment__c + System.Label.CLNOV15FIO005;
            requestPart3 = toAddMSReq + requestPart3;
        }*/
        if(!c.isEmpty() && c.size() > 0){
            requestPart1 = System.Label.CLMAR16FIO028 + System.Label.CLOCT15FIO019;
            if(string.isNotBlank(c[0].Id))
                requestPart1 += System.Label.CLMAR16FIO001+c[0].Id+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].AccountId))
                requestPart1 += System.Label.CLMAR16FIO021+c[0].AccountId+System.Label.CLMAR16FIO019 + System.Label.CLMAR16FIO023;
            if(string.isNotBlank(c[0].FirstName))
                requestPart1 += System.Label.CLMAR16FIO025+c[0].FirstName+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].LastName))
                requestPart1 += System.Label.CLMAR16FIO002+c[0].LastName+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Email))
                requestPart1 += System.Label.CLMAR16FIO003+c[0].Email+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Country__r.CountryCode__c))
                requestPart1 += System.Label.CLMAR16FIO004 +c[0].Country__r.CountryCode__c+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].CorrespLang__c))
                requestPart1 += System.Label.CLMAR16FIO005+c[0].CorrespLang__c+System.Label.CLMAR16FIO019;
             requestPart1 += System.Label.CLMAR16FIO006+c[0].Z_PortalAdmin__c+System.Label.CLMAR16FIO019+System.Label.CLMAR16FIO007+c[0].Id+System.Label.CLMAR16FIO019;
             requestPart1 += System.Label.CLNOV16FIO002+c[0].Z_SEPADebitAdmin__c+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Fax))
                requestPart1 += System.Label.CLMAR16FIO008+c[0].Fax+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].JobDescr__c))
                requestPart1 += System.Label.CLMAR16FIO009+c[0].JobDescr__c+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].JobFunc__c))
                requestPart1 += System.Label.CLNOV15FIO002+c[0].JobFunc__c+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].JobTitle__c))
                requestPart1 += System.Label.CLNOV15FIO001+c[0].JobTitle__c+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].MobilePhone))
                requestPart1 += System.Label.CLOCT15FIO030+c[0].MobilePhone+System.Label.CLMAR16FIO019;
            requestPart1 += System.Label.CLMAR16FIO010+c[0].PRMPrimaryContact__c+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Phone))
                requestPart1 += System.Label.CLMAR16FIO011+c[0].Phone+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Salutation))
                requestPart1 += System.Label.CLMAR16FIO012+c[0].Salutation+System.Label.CLMAR16FIO019;
            requestPart1 = requestPart1.substring(0, requestPart1.length()-2);
            requestPart1 += System.Label.CLOCT15FIO060;
                    
            if(string.isNotBlank(c[0].Account.Name))
                requestPart1 += c[0].Account.Name+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Account.City__c))
                requestPart1 += System.Label.CLMAR16FIO013+c[0].Account.City__c+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Account.Country__r.CountryCode__c))
                requestPart1 += System.Label.CLMAR16FIO004+c[0].Account.Country__r.CountryCode__c+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Account.CurrencyIsoCode))
                requestPart1 += System.Label.CLMAR16FIO015+c[0].Account.CurrencyIsoCode+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Account.MarketSegment__c))
                requestPart1 += System.Label.CLNOV15FIO004+c[0].Account.MarketSegment__c+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Account.StateProvince__r.StateProvinceCode__c))
                requestPart1 += System.Label.CLOCT15FIO038+c[0].Account.StateProvince__r.StateProvinceCode__c+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Account.Street__c))
            {
                String street = String.valueOf(c[0].Account.Street__c);
                street= street.replace('\r\n', ' ');
                street = street.replace('\n', ' ');
                street = street.replace('\r', ' ');
                requestPart1 += System.Label.CLMAR16FIO016+street+System.Label.CLMAR16FIO019;
            }
            if(string.isNotBlank(c[0].Account.Website))
                requestPart1 += System.Label.CLMAR16FIO017+c[0].Account.Website+System.Label.CLMAR16FIO019;
            if(string.isNotBlank(c[0].Account.ZipCode__c))
                requestPart1 += System.Label.CLMAR16FIO018+c[0].Account.ZipCode__c;
            
            requestPart1 += Label.CLNOV16FIO001 + System.Label.CLMAR16FIO006+c[0].Z_PortalAdmin__c+System.Label.CLMAR16FIO019;
            requestPart1 += System.Label.CLNOV16FIO002+c[0].Z_SEPADebitAdmin__c+System.Label.CLMAR16FIO019;
            requestPart1 = requestPart1.substring(0, requestPart1.length()-2);
            requestPart1 += System.Label.CLOCT15FIO042;
            
        }
        
        
        //finalRequest = requestPart1 + requestPart2 + requestPart3;
        Http h1 = new Http();
        HttpRequest req1 = new HttpRequest();
        req1.setEndpoint(System.Label.CLOCT15FIO017);
        req1.setHeader('Authorization','Bearer '+token);
        req1.setHeader('Content-Type','application/json');
        req1.setHeader('Accept',System.Label.CLOCT15FIO018);
        req1.setHeader(System.Label.CLOCT15FIO043,System.Label.CLOCT15FIO044);
        req1.setHeader(System.Label.CLOCT15FIO045,System.Label.CLOCT15FIO046);
        req1.setHeader(System.Label.CLOCT15FIO047,System.Label.CLOCT15FIO048);
        req1.setHeader(System.Label.CLOCT15FIO049,System.Label.CLOCT15FIO048);
        req1.setMethod('POST');
        req1.setTimeout(120000);
        //req1.setBody(System.Label.CLOCT15FIO019+c[0].Id+System.Label.CLOCT15FIO058+c[0].AccountId+System.Label.CLOCT15FIO059+c[0].firstName+System.Label.CLOCT15FIO020+c[0].lastName+System.Label.CLOCT15FIO021+c[0].Email+System.Label.CLOCT15FIO022+c[0].Country__r.CountryCode__c+System.Label.CLOCT15FIO023+c[0].CorrespLang__c+System.Label.CLOCT15FIO024+c[0].Z_PortalAdmin__c+System.Label.CLOCT15FIO025+c[0].Id+System.Label.CLOCT15FIO026+c[0].Fax+System.Label.CLOCT15FIO027+c[0].JobDescr__c+System.Label.CLOCT15FIO028+c[0].JobFunc__c+System.Label.CLOCT15FIO029+c[0].JobTitle__c+System.Label.CLOCT15FIO030+c[0].MobilePhone+System.Label.CLOCT15FIO031+c[0].PRMPrimaryContact__c+System.Label.CLOCT15FIO032+c[0].Phone+System.Label.CLOCT15FIO033+c[0].Salutation+System.Label.CLOCT15FIO060+c[0].Account.Name+System.Label.CLOCT15FIO034+c[0].Account.City__c+System.Label.CLOCT15FIO035+c[0].Account.Country__r.CountryCode__c+System.Label.CLOCT15FIO036+c[0].Account.CurrencyISOCode+System.Label.CLOCT15FIO037+c[0].Account.MarketSegment__c+System.Label.CLOCT15FIO038+c[0].Account.StateProvince__r.StateProvinceCode__c+System.Label.CLOCT15FIO039+c[0].Account.Street__c+System.Label.CLOCT15FIO040+c[0].Account.Website+System.Label.CLOCT15FIO041+c[0].Account.ZipCode__c+System.Label.CLOCT15FIO042);
        //req1.setBody('{"registerUser": {"publisherCode": "BFO","publisherUserId": "003q000000RETgYAAX","publisherAccountId": "001q000000QW8yJAAT","user": {"firstName": "Ana","lastName": "Boughues","emailAddress": "ama002-bougues@yopmail.com","countryCode": "NL","languageCode": "EN","isTrustedAdmin": "false","bfoId": "003q000000RETgYAAX","prmPrimaryContact": "false","salutationCode": "Z001"},"mailActivationGroup": {"accessId": "goDigital","accessType": "APPLICATION"},"account": {"name": "AMA002&Bougues-NL","city": "Amsterdam","countryCode": "NL","currencyCode": "EUR","marketSegment": "EN1","street": "Stadsregio101,","zipCode": "1015"}}}');
        req1.setBody(requestPart1);
        System.debug('****'+req1);
        System.debug('<><><><>'+req1.getBody());
        HttpResponse res1 = new HttpResponse();
        res1 = h1.send(req1);
        System.debug('&&^^'+res1);
        System.debug('****&&&&'+res1.getBody());
        
        String code = String.valueOf(res1.getStatusCode());
        Map<String,Object> responseMap = new Map<String,Object>();  
        if(String.isNotBlank(res1.getBody())){
            String responseReturned = res1.getBody();
            responseMap =   (Map<String,Object>)JSON.deserializeUntyped(res1.getBody());
            System.debug('** IFW AUTH RESPONSE:' + responseMap);
        
           // if(responseReturned.startswithignorecase('{')){
            if(responseMap != null){
                Object registerResponse = responseMap.get('registerUserResponse');
                if(registerResponse != null) {
                    Map<String, Object> fedId = (Map<String, Object>) registerResponse;
                    String federatedId = (String)fedId.get('federatedId');
                    System.debug('federatedId'+federatedId);
                    List<Contact> cntLst = new List<Contact>([SELECT Id FROM Contact WHERE PRMUIMSID__c = :federatedId AND Id != :contactId]);
                    if(cntLst.size() > 0){
                        result.statusCode = 'Duplicate';
                        result.description = '';
                        result.responseMessage = '';
                        result.responseCode = 'Duplicate';
                        result.federatedId = federatedId;
                    }
                    else{
                        result.statusCode = code;
                        result.description = '';
                        result.responseMessage = 'OK';
                        result.responseCode = code;
                        result.federatedId = federatedId;
                    }                   
                }
                else{
                        
                            Object detailMap = responseMap.get('Fault');
                            if(detailMap != null) {
                                Map<String, Object> messageCode;
                                Object messageCodeInfo;
                                Map<String, Object> message;
                                String FaultMessageCode = '';
                                Object businessException;
                                Object technicalException;
                                Map<String, Object> fedId = (Map<String, Object>) detailMap;
                                    Object detailMapInfo = fedId.get('detail');
                                    Map<String, Object> faultDetailsInfo = (Map<String, Object>) detailMapInfo;
                                    Object LocalMsgs = faultDetailsInfo.get('FaultDetails');
                                    if(LocalMsgs != null)
                                        messageCode = (Map<String, Object>) LocalMsgs;
                                    if(messageCode != null)
                                        messageCodeInfo = messageCode.get('LocalizableMessage');
                                    if(messageCodeInfo != null){
                                        message = (Map<String, Object>) messageCodeInfo;
                                        FaultMessageCode = (String)message.get('MessageCode');
                                    }
                                    if(faultDetailsInfo != null){
                                        businessException = faultDetailsInfo.get('businessException');
                                        technicalException = faultDetailsInfo.get('technicalException');
                                    }
                                    result.responseMessage = FaultMessageCode;
                                    result.statusCode = code;
                                    result.federatedId = '';
                                    if(businessException != null) {
                                        Map<String, Object> businessExceptionInfo = (Map<String, Object>) businessException;
                                        String businessExceptioncode = (String)businessExceptionInfo.get('code');
                                        String businessExceptiondescription = (String)businessExceptionInfo.get('description');
                                        result.description = businessExceptiondescription;
                                        result.responseCode = businessExceptioncode;
                                    }
                                    else if(technicalException != null){
                                        Map<String, Object> technicalExceptionInfo = (Map<String, Object>) technicalException;
                                        String technicalExceptioncode = (String)technicalExceptionInfo.get('code');
                                        String technicalExceptiondescription = (String)technicalExceptionInfo.get('description');
                                        result.description = technicalExceptiondescription;
                                        result.responseCode = technicalExceptioncode;
                                    }
                                    else {
                                        Object faultException = faultDetailsInfo.get('FaultRelatedException');
                                        result.description = (String)faultException;
                                        result.responseCode = '';
                                    }
                            }
                            else {
                                    Object detailfaultMap = responseMap.get('fault');
                                    if(detailfaultMap != null) {
                                         Map<String, Object> faultReturned = (Map<String, Object>) detailfaultMap;
                                         result.description = (String)faultReturned.get('description');
                                         result.responseCode = result.statusCode = string.valueof(faultReturned.get('code'));
                                         result.federatedId = '';
                                         result.responseMessage = (String)faultReturned.get('message');
                                    }
                                } 
                            }
                    }
               /* }
                
                else{
                    String xml = res1.getBody();
                    System.debug('Inside Else');
                    result.statusCode = code;
                    result.federatedId = '';
                    Dom.Document doc = new Dom.Document();
                    System.debug('Inside Else'+doc);
                    
                    doc.load(xml);
                    DOM.XMLNode root = doc.getRootElement();
                    System.debug('><><'+root.getName());
                    System.debug('><><'+root.getNodeType());
                    System.debug('><><'+root.getChildren());
                    for(Dom.XmlNode c1: root.getChildElements()) {
                        if(c1.getName() == 'code')
                            result.responseCode = c1.getText();
                        if(c1.getName() == 'message')
                            result.responseMessage = c1.getText();
                        if(c1.getName() == 'description')
                            result.description = c1.getText();
                    }
                }*/
               }
               else {
                    result.responseMessage = System.Label.CLNOV15FIO006;
                    result.federatedId = result.responseCode = result.statusCode = result.description = '';
                }
              
                              
        return result;
    }
    
    public pageReference back() {
        PageReference pageRef = new PageReference('/'+contacts);
        return pageRef;
    }
    
    public class ResponseResult {
        public String statusCode;
        public String description;
        public String responseMessage;
        public String responseCode;
        public String federatedId;
    }
}