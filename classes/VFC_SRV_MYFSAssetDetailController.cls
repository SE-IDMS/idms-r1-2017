Public Class VFC_SRV_MYFSAssetDetailController{
  Public SVMXC__Installed_Product__c AssetRecord{get;set;}
  Public Boolean UnderWarranty{get;set;}
  Public String assetId{get;set;}
  Public String site{get;set;}
  Public String UltimateParentAccount{get;set;}
  Public String SelectedCountry{get;set;}
  Public list<SVMXC__Service_Contract__c> ServiceLineslst{get;set;}
  Public List<SVMXC__Service_Order__c> workOrdersList{get;set;}
  Public list<SVMXC__Service_Contract_Products__c> CoveredProducts{get;set;}
  Public Date mydate{get;set;}
  Public String UserId{get;set;}
  
  Public VFC_SRV_MYFSAssetDetailController(){
    assetId = Apexpages.currentpage().getparameters().get('Id');
    UltimateParentAccount = Apexpages.currentpage().getparameters().get('Params');
    SelectedCountry= Apexpages.currentpage().getparameters().get('country');
    Site= Apexpages.currentpage().getparameters().get('site');
    UserId = Apexpages.currentpage().getparameters().get('UId');
    if(userId==null || userId ==''){
     userId = UserInfo.getUserId();
     
    }
  }

   Public void AssetMethod(){
       
        
        mydate= date.today().adddays(+120);
        Set<id> contractidset = new Set<id>();
        Date todayDate = Date.today();
        Set<String> Status = New Set<String>();
        workOrdersList = new List<SVMXC__Service_Order__c>();
        system.debug('###assetId'+assetId);
        if(assetId != null && assetId != ''){
        Status.add(System.Label.CLSRVMYFS_CONTRACTACTIVESTARUTSVAL);
        Status.add(System.Label.CLSRVMYFS_CONTRACTACTIVESTARUTSPCV);
        Status.add(System.Label.CLSRVMYFS_CONTRACTACTIVESTARUTSRQCAN);
        Status.add(System.Label.CLSRVMYFS_CONTRACTACTIVESTARUTSPCANC);
        AssetRecord = [select id,name,SVMXC__Serial_Lot_Number__c,SVMXC__Product__c,SVMXC__Product__r.name,Brand2__c,Brand2__r.name,Category__r.name,Category__c,DeviceType2__r.name,DeviceType2__c,UnderContract__c,SVMXC__Site__c,SVMXC__Site__r.name,ManufacturingDate__c,PurchasedDate__c,SVMXC__Date_Shipped__c,CommissioningDateInstallDate__c
                       ,SVMXC__Warranty_Start_Date__c,SVMXC__Warranty_End_Date__c,EndOfInstalledProductLifeDate__c,ServiceObsolecenseDate__c,DecomissioningDate__c from SVMXC__Installed_Product__c where Id=:assetId ];
        system.debug('AssetRecord'+AssetRecord);
        if(AssetRecord.SVMXC__Warranty_Start_Date__c<=date.today() && AssetRecord.SVMXC__Warranty_End_Date__c>=date.today() )
        {
            UnderWarranty =True;
            system.debug('UnderWaranty'+UnderWarranty);
        }
        CoveredProducts = [ select id,name,SVMXC__Service_Contract__c,SVMXC__Service_Contract__r.SVMXC__Active__c,SVMXC__Service_Contract__r.ParentContract__c from 
                            SVMXC__Service_Contract_Products__c where SVMXC__Installed_Product__c =:assetId and SVMXC__Service_Contract__r.ParentContract__r.Status__c 
                            IN :Status  and  SVMXC__Service_Contract__r.SVMXC__Start_Date__c<=:todayDate AND  SVMXC__Service_Contract__r.SVMXC__End_Date__c>=:todayDate 
                          ];
        system.debug('ContractsIdSet'+contractidset);
        if(CoveredProducts.size()>0 && CoveredProducts!=null)
        {
           for(SVMXC__Service_Contract_Products__c  CoveredProd : CoveredProducts ){
                 contractidset.add(CoveredProd.SVMXC__Service_Contract__r.ParentContract__c);
           }
        }
        
        system.debug('ContractsIdSet'+contractidset);
        ServiceLineslst = [select id,name,SVMXC__Active__c,SVMXC__Start_Date__c,SVMXC__End_Date__c,ParentContract__r.id,toLabel(ParentContract__r.status__c) from SVMXC__Service_Contract__c
                           where id IN:contractidset];
        
        //system.debug('ServiceLineslst'+ServiceLineslst[0].ParentContract__r.id );
      }
        if(assetId != null && assetId != ''){
                Set<Id> workDetailWO = new Set<Id>();
                Set<Id> workDetailSet = new Set<Id>();
                Set<Id> workOrderSet = new Set<Id>();
                List<SVMXC__Service_Order__c> processedWOList = new List<SVMXC__Service_Order__c>();
                workOrdersList = new List<SVMXC__Service_Order__c>();
                List<SVMXC__Service_Order_Line__c> ipWorkDetailList = new List<SVMXC__Service_Order_Line__c>();
                Set<String> unUsedWorkOrderStatus = new Set<String>();
                unUsedWorkOrderStatus.add(System.label.CLSRVMYFS_SCHDEDULED);
                unUsedWorkOrderStatus.add(System.label.CLSRVMYFS_NEW);
                unUsedWorkOrderStatus.add(System.label.CLSRVMYFS_UNSCHEDULED);
                unUsedWorkOrderStatus.add(System.label.CLSRVMYFS_CANCELLED);
                workOrderSet = new Map<Id, SObject>(Database.query('Select SVMXC__Component__c,Name from SVMXC__Service_Order__c where (SVMXC__Order_Status__c NOT IN :unUsedWorkOrderStatus) And (SVMXC__Component__c = :assetId)')).keySet();
                ipWorkDetailList = [select SVMXC__Service_Order__c,SVMXC__Serial_Number__c,Name from SVMXC__Service_Order_Line__c where (SVMXC__Service_Order__r.SVMXC__Order_Status__c NOT IN :unUsedWorkOrderStatus) And (SVMXC__Serial_Number__c = :assetId)]; 
                if(ipWorkDetailList != null && !ipWorkDetailList.isEmpty()){
                 for (SVMXC__Service_Order_Line__c eachWorkDetail:ipWorkDetailList){
                 workDetailSet.add(eachWorkDetail.SVMXC__Service_Order__c);
                 }
                }
                if(workOrderSet != null)
                workDetailWO.addAll(workOrderSet);
                if(workDetailSet != null)
                workDetailWO.addAll(workDetailSet);
                if(workDetailWO != null && !workDetailWO.isEmpty()){
                list<SVMXC__Service_Order__c> tempWOList = new list<SVMXC__Service_Order__c>();
                workOrdersList = new list<SVMXC__Service_Order__c>();
                 tempWOList = [select SVMXC__Scheduled_Date_Time__c ,SVMXC__Order_Type__c,TECH_CustomerScheduledDateTimeECH__c,SVMXC__Order_Status__c,SVMXC__Group_Member__r.Name,SVMXC__Component__c,Name,CustomerTimeZone__c,(SELECT Id, Name,CreatedDate,ParentId, LastModifiedDate FROM Attachments where 
                                   Name like :System.label.CLSRVMYFS_PDFEXTENSION and (Name like :system.label.CLSRVMYFS_SRVINTERVENTION or Description =:system.label.CLSRVMYFS_WODescription) limit 1) from SVMXC__Service_Order__c where Id IN :workDetailWO ORDER BY SVMXC__Scheduled_Date_Time__c 
                                   DESC NULLS LAST];
                  workOrdersList = AP_SRV_myFSUtils.getWOTechScheduledDateTime(tempWOList);
                if(workOrdersList != null && !workOrdersList.isEmpty()){
                 AP_SRV_myFSUtils.WorkorderSharing(workOrdersList,UserId);
                }
                
                }          
           
           System.debug('workOrdersList'+workOrdersList);
        }
    }
}