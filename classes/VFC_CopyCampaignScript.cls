/*
29-Oct-2012    Srinivas Nallapati    Initial Creation: Dec12 Mkt Release
*/
public with sharing class VFC_CopyCampaignScript {

    public Lead led {get;set;} {led= new Lead();}
    
    public VFC_CopyCampaignScript(ApexPages.StandardController controller) {
        led = (Lead)controller.getRecord();
        led = [select name, status, QualificationInfo__c, NonMarcommCampaign__c, NonMarcommCampaign__r.Script__C from Lead where id =:led.id];
    }
    
    public Pagereference copyScript()
    {
        //May13 MKT Change - Srinivas Nallapati
        if(!( VFC_ConvertLead.hasEditOnLead(Userinfo.getUserId(), led.Id)) )
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLMAY13MKT004));
            return null;
        }
        //End of MAY13 MKt - Srinivas


        if(led.NonMarcommCampaign__c != null)
        {
            if(led.NonMarcommCampaign__r.Script__c != null)
            {
                if(led.Status == Label.CL00554 || led.Status == Label.CL00447) //if New or Working
                {  
                    if(led.QualificationInfo__c == null)
	                {
	                	
	                    try{  
		                    led.QualificationInfo__c = led.NonMarcommCampaign__r.Script__c;
		                    update led;
		                    
		                    return new Pagereference('/'+led.id);
	                    }Catch(Exception e){
	                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage())); 
	                        return null;
	                    }
	                }else
	                {
	                	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLDEC12MKT01)); 
                        return null;
	                }    
                }
                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLDEC12MKT02)); 
                return null;
                
            }
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLDEC12MKT03)); 
            return null;
        }
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLDEC12MKT04)); 
        return null;
    }
    
    public Pagereference goToLead(){
        
        return new Pagereference('/'+led.id);
    }

}