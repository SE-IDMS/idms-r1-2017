/*******************************************/
//
// Customer Self-Service Portal (Compass)
//
// Stephen Norbury and Brice Nofiele
//
//
/*******************************************/

public with sharing class VFC_CustomerPortalClsProfile 
{
    
    // Contact Details
    
    public String ContactName         {get; set;}
    public String ContactEmail        {get; set;}
    public String ContactFax          {get; set;}
    public String ContactPhone        {get; set;}
    public String ContactMobile       {get; set;}
    public String ContactStreet       {get; set;}
    public String ContactCity         {get; set;}
    public String ContactZip          {get; set;}
    public String ContactState        {get; set;}
    public String ContactCountry      {get; set;}
    public String AccountName         {get; set;}
         
    Id ContactId = null;
    Id AccountId = null;
    Id UserId    = null;

    User    CustomerUser     = null;
    Contact CustomerContact  = null;
    Account CustomerAccount  = null;
    
    public boolean getIsAuthenticated ()
    {
      return (IsAuthenticated ());
    }
    
    public boolean IsAuthenticated ()
    {
      return (! UserInfo.getUserType().equalsIgnoreCase('Guest'));
    }
        
    public VFC_CustomerPortalClsProfile () 
    {
      if (isAuthenticated())
      {           
        UserId = UserInfo.getUserId();
      
        if (UserId != null)
        {
          CustomerUser = [select ContactId from User where Id = :UserId][0];
        
          if (CustomerUser != null)
          {
            ContactId = CustomerUser.ContactId;
      
            if (ContactId != null)
            {
              CustomerContact = [select Id, Salutation, Name, AccountId, Email, Fax, WorkPhone__c, MobilePhone, Street__c, City__c, ZipCode__c, StateProv__r.Name, Country__r.Name from Contact where Id = :ContactId][0];
            
              if (CustomerContact != null)
              {
                AccountId = CustomerContact.AccountId;
              
                CustomerAccount = [select Name, Street__c, City__c, ZipCode__c, StateProvince__r.Name, Country__r.Name from Account where Id = :AccountId][0];
              }
            
              if (CustomerAccount != null)
              {
                AccountName    = CustomerAccount.Name;
                ContactStreet  = CustomerAccount.Street__c;
                ContactCity    = CustomerAccount.City__c;
                ContactZip     = CustomerAccount.ZipCode__c;
                ContactState   = CustomerAccount.StateProvince__r.Name;
                ContactCountry = CustomerAccount.Country__r.Name;
              }
      
              if (CustomerContact != null)
              {
                ContactName    = CustomerContact.Name;
                ContactEmail   = CustomerContact.Email;
                ContactFax     = CustomerContact.Fax;
                ContactPhone   = CustomerContact.WorkPhone__c; 
                ContactMobile  = CustomerContact.MobilePhone;
                ContactStreet  = CustomerContact.Street__c;
                ContactCity    = CustomerContact.City__c;
                ContactZip     = CustomerContact.ZipCode__c;
                ContactState   = CustomerContact.StateProv__r.Name;
                ContactCountry = CustomerContact.Country__r.Name;
              }
            }
          }
        }            
      }
    }
        
    // End     
}