@isTest
private class VFC_NewFrameOpportunity_Test
{
    static List<OppAgreement__c> aggList = new List<OppAgreement__c>();
    static ProductLineForAgreement__c[] objProdLine = new List<ProductLineForAgreement__c>();
    static User user,newUser,newUser2 ;
    static Country__c newCountry;
    static OppAgreement__c objAgree;
    static Account acc;
    static Opportunity objOpp,opp2,opp3,opp4;
    static OppAgreement__c objAgree2,objAgree3;
    static List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();   
    static List<OppAgreement__c> oa = new List<OppAgreement__c>();
    static OPP_Product__c  prod = new OPP_Product__c();
    static
    {
        VFC61_OpptyClone.isOpptyClone = true;
        acc = Utils_TestMethods.createAccount();
        insert acc;
        
        opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.StageName = '3 - Identify & Qualify';
        insert opp2; 
        objAgree2 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree2.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree2.AgreementValidFrom__c = Date.today();         
        objAgree2.FrameOpportunityInterval__c = 'Monthly';
        objAgree2.SignatureDate__c = Date.today();
        objAgree2.Opportunity_Type__c = 'Standard';             
        objAgree2.Agreement_Duration_in_month__c = '3';
        insert objAgree2;
        
        
        objAgree3 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree3.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree3.AgreementValidFrom__c = Date.today();    
        objAgree3.Opportunity_Type__c = 'Solutions';     
        objAgree3.FrameOpportunityInterval__c = Label.CL00221;
        objAgree3.SignatureDate__c = Date.today();
        objAgree3.Agreement_Duration_in_month__c = '2';
        insert objAgree3;        
    }     

    static testmethod void newFrameOppty()
    {   
        Pagereference pref = Page.VFP_NewFrameOpportunity;
        pref.getParameters().put('oppAgreeId',objAgree2.id);
        Test.setCurrentPage(pref);
        
        VFC_NewFrameOpportunity frameOppty = new VFC_NewFrameOpportunity(new ApexPages.StandardController(objAgree2));
        frameOppty.populateFrameOppURL();
        
    }
    
}