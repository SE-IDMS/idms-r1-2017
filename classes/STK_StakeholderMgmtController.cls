public with sharing class STK_StakeholderMgmtController {
        
        public SObject parentObject{get;set;}
        
        //Collections
        public list<cStakeHolder> stkList {get;set;}
        public list<cSearchResult> resultList{get;set;}
        
        //Search criteria setup
        public list<SelectOption> roleList{get;set;}
        public String roleCriteria {get;set;}
        public String nameCriteria {get;set;}
        public String firstNameCriteria {get;set;}
        
        //Boolean
        public boolean isChangeRequest{get;set;}
        public boolean searchPerformed {get;set;}
        public Boolean displayQuickCreate {get;set;}
        
        //Object
        public DMTStakeholder__c oNewContact {get;set;}
        
        //Dynamic getters
        public Integer resultListSize{get{return resultList.size();}}
        public Integer stkListSize{get{return stkList.size();}}
        public list<String> missingRoles{get{return getMissingStkForStep();}}
        public Integer missingRolesSize{get{return missingRoles.size();}}
        
        
        
        

        /*===================
        CONSTRUCTOR
        =====================*/
        public STK_StakeholderMgmtController(){
                String relatedObjectId;
                if (System.currentPageReference().getParameters().containskey('Id')){
                        relatedObjectId = System.currentPageReference().getParameters().get('Id');
                }
                
                resultList = new list<cSearchResult>();
                stkList = new List<cStakeHolder>();
                roleList = new List<SelectOption>();
                Schema.DescribeFieldResult res;
                searchPerformed = false;
                displayQuickCreate = false;
                
                if (CHR_ChangeReq__c.SobjectType.getDescribe().getKeyPrefix() == relatedObjectId.substring(0,3)){
                        isChangeRequest = true;
                        parentObject = [select id,Name,StepManual__c from CHR_ChangeReq__c where id = :relatedObjectId];
                        for (ChangeRequestStakeholder__c oStk:[select Id,Role__c,ActorNew__c,ActorNew__r.FirstName__c,ActorNew__r.LastName__c,ActorName__c from ChangeRequestStakeholder__c where ChangeRequest__c=:relatedObjectId]){
                                stkList.add(new cStakeholder(oStk,this,oStk.ActorNew__r.FirstName__c,oStk.ActorNew__r.LastName__c));
                        }
                        
                        res = ChangeRequestStakeholder__c.Role__c.getDescribe();
                        
                } else if (PRJ_ProjectReq__c.SobjectType.getDescribe().getKeyPrefix() == relatedObjectId.substring(0,3)){
                        isChangeRequest =false;
                        parentObject = [select id,Name,ActualStep__c,ParentFamily__c,XRM__c,ProjectPortfolioManager__c,Requester__c,Tiering__c,BusGeographicZoneIPO__c from PRJ_ProjectReq__c where id = :relatedObjectId];
                        for (PRJ_Stakeholder__c oStk:[select Id,Role__c,ActorNew__c,ActorName__c,ActorNew__r.FirstName__c,ActorNew__r.LastName__c,ActorFirstName__c,ActorLastName__c,ActorNew__r.Name from PRJ_Stakeholder__c where ProjectReq__c=:relatedObjectId]){
                                stkList.add(new cStakeholder(oStk,this,oStk.ActorNew__r.FirstName__c,oStk.ActorNew__r.LastName__c));
                        }
                        
                        res = PRJ_Stakeholder__c.Role__c.getDescribe();
                        
                } else return;
                
                roleList.add(new SelectOption('','-None-'));
                for (Schema.Picklistentry entry:res.getPicklistValues()){                       
                        roleList.add(new SelectOption(entry.getValue(),entry.getLabel()));
                }
                
                
        }
        
        
        public void switchDisplay(){
                displayQuickCreate = !displayQuickCreate;
                oNewContact = new DMTStakeholder__c();
        }
        
        public void createContactAndAddStk(){
                //oNewContact.AccountId = ApplicationCfg__c.getInstance().AccountId__c;
                insert oNewContact;
                
                SObject newStk = initNewStkObject();
                newStk.put('ActorNew__c',oNewContact.Id);
                stkList.add(new cStakeholder(newStk,this));
                
                switchDisplay();
        }
        
        public void searchStk(){
                
                String actorQueryString = 'Select Id,FirstName__c,LastName__c, email__c,phone__c,L1__c,L2__c,L3__c,L4__c from DMTStakeholder__c where isDeleted=false ';
                
                resultList.clear();
                
                //resultList = new List<cResult>();
                String userFilterQuery='';
                if (nameCriteria<>'')     userFilterQuery = ' and LastName__c like \'%'+NameCriteria+'%\'';
                if (firstNameCriteria<>'')        userFilterQuery += ' and FirstName__c like \'%'+firstNameCriteria+'%\'';
                if (roleCriteria<>null){
                                if (isChangeRequest) userFilterQuery += ' and ChangeReqRoles__c includes (\''+roleCriteria+'\')';
                                else userFilterQuery += ' and ProjectRoles__c includes (\''+roleCriteria+'\')';
                }
                
                if (userFilterQuery.length()==0) {
                        ApexPages.addMessages(new noCriteriaException('You must fill at least one criteria'));
                        return;
                }
                actorQueryString =actorQueryString +  userFilterQuery ;
                actorQueryString += ' order by Name limit 1000';
                
                List<Sobject> sortedResults= new List<SObject>();
                try{
                        sortedResults = Database.query(actorQueryString);
                        searchPerformed = true;
                } catch (Exception e){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                }
                System.debug('Requete => '+actorQueryString);

                for (SObject foundObject:sortedResults){
                        DMTStakeholder__c oContact = (DMTStakeholder__c)foundObject;
                        resultList.add(new cSearchResult(oContact));            
                }
                
                searchPerformed=true;
                
                
        }
        
        public SObject initNewStkObject(){
                SObject obj;
                if (isChangeRequest) obj = new ChangeRequestStakeholder__c(ChangeRequest__c=parentObject.Id);
                else obj = new PRJ_Stakeholder__c(ProjectReq__c=parentObject.Id);
                return obj;
        }
        
        public void addResultToStkList(){
                
                for (cSearchResult result:resultList){
                        if (result.selected){
                                SObject newStk = initNewStkObject();
                                newStk.put('ActorNew__c',result.oContact.Id);
                                //newStk.put('ActorFirstName__c',result.oContact.FirstName__c);
                                //newStk.put('ActorLastName__c',result.oContact.LastName__c);
                                stkList.add(new cStakeholder(newStk,this,result.oContact.FirstName__c,result.oContact.LastName__c));
                        }
                        
                }
                //PageReference pr = new PageReference('/apex/STK_StakeholderMgmt?Id='+ ApexPages.currentPage().getParameters().get('Id'));
                //pr.setRedirect(true);
                //return pr;
                
        }
        
        public list<String> getMissingStkForStep(){
                Map<String,Set<String>> confValues = new Map<String,Set<String>>();
                String stepName;
                boolean isRequired = false;
                if(!isChangeRequest)
                {
                        
                        for (PRJ_ReqStakeholderCfg__c cfg:PRJ_ReqStakeholderCfg__c.getAll().Values())
                        {
                                isRequired = false;
                                if ((cfg.isIPO__c && (String)parentObject.get('ParentFamily__c') == 'IPO') || (cfg.isBusiness__c && parentObject.get('ParentFamily__c')<>'IPO' ))
                                {
                                    //Added by Sreedevi Surendran (Schneider Electric) for the February, 2012 Release
                                    //begin
                                    if(cfg.StakeholderRole__c == System.Label.DMT_GlobalApplicationsServicesCustomerExperience || cfg.StakeholderRole__c == System.Label.DMT_GlobalOperationsServices)
                                    {
                                        if(!((String)parentObject.get('Tiering__c') == 'Tier 4' || (String)parentObject.get('ParentFamily__c') == 'IPO' || ((String)parentObject.get('ParentFamily__c') == 'Global Functions' && (String)parentObject.get('BusGeographicZoneIPO__c') == 'Global')))
                                            isRequired = true;
                                        /*  
                                        if((String)parentObject.get('Tiering__c') <> 'Tier 4')
                                            isRequired = true;
                                        if((String)parentObject.get('ParentFamily__c') <> 'IPO')
                                            isRequired = true;
                                        if(!((String)parentObject.get('ParentFamily__c') == 'Global Functions' && (String)parentObject.get('BusGeographicZoneIPO__c') == 'Global'))
                                            isRequired = true
                                        */
                                        
                                    }
                                    else if(cfg.StakeholderRole__c == System.Label.DMT_EntityFinancialController || cfg.StakeholderRole__c == System.Label.DMT_ProjectPortfolioManager || cfg.StakeholderRole__c == System.Label.DMT_XRelationshipManager)
                                    {
                                        if((String)parentObject.get('ParentFamily__c') <> 'IPO')
                                            isRequired = true;
                                    }
                                    else if(cfg.StakeholderRole__c == System.Label.DMT_ProcessOwner)
                                    {
                                        if((String)parentObject.get('Tiering__c') == 'Tier 1' || (String)parentObject.get('Tiering__c') == 'Tier 2')
                                            isRequired = true;
                                    }
                                    else
                                        isRequired = true;
                                }               
                                if(isRequired)
                                {
                                    set<String> tempRoleSet = new Set<String>();
                                    if (confValues.containsKey(cfg.Step__c)) tempRoleSet = confValues.get(cfg.Step__c);
                                    tempRoleSet.add(cfg.StakeholderRole__c);
                                    confValues.put(cfg.Step__c,tempRoleSet);                

                                }
                                //end
                        }
                        
                        
                        stepName = (String)parentObject.get('ActualStep__c');
                        
                        
                
                        
                } else {
                        for (CHR_ReqStakeholderCfg__c cfg:CHR_ReqStakeholderCfg__c.getAll().Values()){
                                set<String> tempRoleSet = new Set<String>();
                                if (confValues.containsKey(cfg.Step__c)) tempRoleSet = confValues.get(cfg.Step__c);
                                tempRoleSet.add(cfg.StakeholderRole__c);
                                confValues.put(cfg.Step__c,tempRoleSet);                
                                                
                        }
                        stepName = (String)parentObject.get('StepManual__c');
                        
                }
                
                set<String> currentRoles = new Set<String>();
                for (cStakeHolder stk:stkList){
                        currentRoles.add((String)stk.oStakeholder.get('Role__c'));
                }
                
                System.debug('Current Role=>'+currentRoles);
                System.debug('Step name=>'+stepName);
                System.debug('confValues=>'+confValues);
                list<String> missingRoles = new list<String>();
                if (confValues.containsKey(stepName)){
                        
                        for(String requiredRole:confValues.get(stepName)){
                                System.debug('requiredRole=>'+requiredRole);
                                
                                //if (!currentRoles.contains(requiredRole)) missingRoles.add(requiredRole);

                                //EUS change request 000011 enhancement July 26, 2011 begin
                                if (requiredRole == System.Label.DMT_GlobalApplicationsServicesCustomerExperience)
                {
                    if(!currentRoles.contains(System.Label.DMT_GlobalApplicationsServices) && !currentRoles.contains(System.Label.DMT_CustomerExperience))
                        missingRoles.add(requiredRole);
                            
                }
                else 
                {
                    if(!currentRoles.contains(requiredRole)) 
                        missingRoles.add(requiredRole);               
                }                                       
                //EUS change request 000011 enhancement July 26, 2011 end
                        }
                }
        
                return missingRoles;
                
        }
        
        public PageReference CommitAndReturn(){
                commitChanges();



                
                return new PageReference('/'+parentObject.Id);
        }
        
        public void commitChanges(){
                List<SObject> objToDeleteList = new List<SObject>();
                List<SObject> objToInserttList = new List<SObject>();
                List<SObject> objToUpdatetList = new List<SObject>();
                PRJ_ProjectReq__c prj = new PRJ_ProjectReq__c();
                CHR_ChangeReq__c chr = new CHR_ChangeReq__c();
                
                
                
                if(!isChangeRequest)
                {
                    prj = (PRJ_ProjectReq__c)parentObject;
                    prj.XRM__c = '';
                    prj.ProjectPortfolioManager__c = '';
                    prj.Requester__c = '';
                    
                }
                else
                {
                    chr = (CHR_ChangeReq__c)parentObject;
                }

                for (cStakeholder stk:stkList){

                //updateParentObject(stk);
                    if(!isChangeRequest)
                    {
                
                        if((String)stk.oStakeholder.get('Role__c') == 'X Relationship Manager')
                            prj.XRM__c += (String)stk.oStakeholder.get('ActorName__c') + ';';
                        else if((String)stk.oStakeholder.get('Role__c') == 'Project Portfolio Manager')
                            prj.ProjectPortfolioManager__c += (String)stk.oStakeholder.get('ActorName__c') + ';';
                        else if((String)stk.oStakeholder.get('Role__c') == 'Requester')    
                            prj.Requester__c += (String)stk.oStakeholder.get('ActorName__c') + ';';

                    }
                    else
                    {
                    
                    }
                
                    if (stk.isdeleted) objToDeleteList.add(stk.oStakeholder);
                    else if (stk.oStakeholder.Id==null) objToInserttList.add(stk.oStakeholder);
                    else objToUpdatetList.add(stk.oStakeholder);
                }
                
                if (objToDeleteList.size()>0) delete objToDeleteList;
                if (objToInserttList.size()>0) insert objToInserttList;
                if (objToUpdatetList.size()>0) update objToUpdatetList;
                if(!isChangeRequest) 
                    update prj; 
                else 
                    update chr;
                
        }
        
        public void updateParentObject(cStakeholder stk)
        {
                PRJ_ProjectReq__c prj = (PRJ_ProjectReq__c)parentObject;
                
                if((String)stk.oStakeholder.get('Role__c') == 'X Relationship Manager')
                    prj.XRM__c = ' ' + (String)stk.oStakeholder.get('ActorName__c');
                else if((String)stk.oStakeholder.get('Role__c') == 'Project Portfolio Manager')
                    prj.ProjectPortfolioManager__c = ' ' + (String)stk.oStakeholder.get('ActorName__c');
                else if((String)stk.oStakeholder.get('Role__c') == 'Requester')    
                    prj.Requester__c = ' ' + (String)stk.oStakeholder.get('ActorName__c');
                    
            
        }
        
        public class cSearchResult{
                public DMTStakeholder__c oContact {get;set;}
                public Boolean selected {get;set;}
                
                
                public cSearchResult(DMTStakeholder__c oContact){
                        this.oContact = oContact;
                        selected = false;
                }
                
        }
        
        public class cStakeholder{
                public SObject oStakeholder{get;set;}
                public Boolean isDeleted{get;set;}
                private STK_StakeholderMgmtController parent;
                public string StakeholderFirstName{get;set;}
                public string StakeholderLastName{get;set;}
                public SObject DMTS{get;set;}

                
                public cStakeholder(SObject oStakeholder,STK_StakeholderMgmtController parent,string fname,string lname){
                        this.oStakeholder = oStakeholder;
                        this.parent = parent;
                        this.isDeleted = false;
                        //this.StakeholderName = String.valueof(oStakeholder.get('ActorNew__c'));
                        //this.StakeholderName = (String)oStakeholder.get('ActorNew__r.FirstName__c');
                        //List<SObject> lstActorDetails = oStakeholder.getSObjects('ActorNew__c');
                        //DMTS = oStakeholder.getSObject('ActorNew__r');
                        //StakeholderName = (String)DMTS.get('FirstName__c');
                        StakeholderFirstName = fname;
                        StakeholderLastName = lname;
                        
                }

                public cStakeholder(SObject oStakeholder,STK_StakeholderMgmtController parent){
                        this.oStakeholder = oStakeholder;
                        this.parent = parent;
                        this.isDeleted = false;
                        //this.StakeholderName = String.valueof(oStakeholder.get('ActorNew__c'));
                        //this.StakeholderName = (String)oStakeholder.get('ActorNew__r.FirstName__c');
                        //List<SObject> lstActorDetails = oStakeholder.getSObjects('ActorNew__c');
                        //DMTS = oStakeholder.getSObject('ActorNew__r');
                        //StakeholderName = (String)DMTS.get('FirstName__c');
                        
                }

                
                
                
                public void removeStk(){
                        
                        for (integer i=0;i<parent.stkList.size();i++){
                                cStakeholder stk = parent.stkList[i];
                                if (stk==this){
                                          parent.stkList.remove(i);
                                          if (oStakeholder.id<>null )delete oStakeholder;
                                          break;
                                }
                        }
                        
                        this.isDeleted = true;
                }
                
        }
        
        public class noCriteriaException extends Exception{}
        
        
        public static testmethod void testSTKManagement(){
                List<DMTStakeholder__c> ContactList = new List<DMTStakeholder__c>();
                List<PRJ_Stakeholder__c> actorList = new List<PRJ_Stakeholder__c>();
                
                Account testAccount = new Account(name='Schneider',Street__c='New Street', POBox__c='XXX');
                insert testAccount;
                
               DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
                insert testDMTA1; 
                PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
                testProj.ParentFamily__c = 'Business';
                testProj.Parent__c = 'Buildings';
                testProj.BusGeographicZoneIPO__c = 'Global';
                testProj.BusGeographicalIPOOrganization__c = 'CIS';
                testProj.GeographicZoneSE__c = 'Global';
                testProj.Tiering__c = 'Tier 1';
                testProj.ProjectRequestDate__c = system.today();
                testProj.Description__c = 'Test';
                testProj.GlobalProcess__c = 'Customer Care';
                testProj.ProjectClassification__c = 'Infrastructure';
                insert testProj;
                
                Schema.DescribeFieldResult F = PRJ_Stakeholder__c.Role__c.getDescribe();
                REF_OrganizationRecord__c oOrg = Utils_TestMethods.createOrganizationRecord('Test','Business Organization')     ;
                insert oOrg;
                        
                for(Schema.PicklistEntry pickValue:f.getPicklistValues()){
                        DMTStakeholder__c testContact= new DMTStakeholder__c(LastName__c='Test',firstName__c = 'Prenom',email__c='test@hotmail.com',ProjectRoles__c=pickValue.getValue(),L1__c =oOrg.Id);
                        contactList.add(testContact);
                }
                
                insert contactList;
                
                Integer countContact = 0;
                for(Schema.PicklistEntry pickValue:f.getPicklistValues()){      
                        PRJ_STakeholder__c actor = new PRJ_Stakeholder__c();
                        actor.ProjectReq__c = testProj.id;
                        actor.ActorNew__c = contactList[countContact].Id;
                        actor.Role__c = pickValue.getValue();
                        actorList.add(actor);
                }
                insert actorList;
                
                
                If (PRJ_Config__c.getInstance()==null || PRJ_Config__c.getInstance().NotificationTemplate__c==null){
                        PRJ_Config__c testConfig = new PRJ_Config__c();
                        testConfig.NotificationTemplate__c = [select developerName  from EmailTemplate limit 1].developerName;
                        upsert testConfig;
                }
                
                test.StartTest();
                
                test.setCurrentPageReference(new PageReference('/apex/STK_StakeholderMmt?Id='+testProj.Id));
                STK_StakeholderMgmtController ctl = new STK_StakeholderMgmtController();
                
                ctl.roleCriteria = contactList[0].ProjectRoles__c;
                ctl.firstNameCriteria = contactList[0].FirstName__c;
                ctl.nameCriteria = contactList[0].LastName__c;
                
                ctl.searchStk();
                
                System.assert(ctl.resultlistsize>0);
                
                for (cSearchResult result:ctl.resultList){
                        result.selected=true;
                }
                ctl.addResultToStkList();
                
                for (cStakeholder oStk:ctl.stkList){
                        oStk.oStakeholder.put('Role__c', ctl.roleCriteria);
                }
                
                
                
                
                ctl = new STK_StakeholderMgmtController();
                System.assert(ctl.stkListSize>0);
                List<String> roles = ctl.missingRoles;
                
                ctl.stkList[0].removeStk();
                ctl.commitChanges();
                ctl.CommitAndReturn();
                ctl.roleCriteria = contactList[0].ProjectRoles__c;
                ctl.firstNameCriteria = contactList[0].FirstName__c;
                ctl.nameCriteria = contactList[0].LastName__c;
                
                ctl.searchStk();
                
                System.assert(ctl.resultlistsize>0);
                
                for (cSearchResult result:ctl.resultList){
                        result.selected=true;
                }
                ctl.addResultToStkList();
                
                for (cStakeholder oStk:ctl.stkList){
                        oStk.oStakeholder.put('Role__c', ctl.roleCriteria);
                }
                
                
                ctl.commitChanges();
                
                System.assert(ctl.stkListSize>0);
                
                ctl.stkList[0].oStakeholder.put('Role__c', ctl.roleCriteria);
                ctl.commitChanges();
                
                //start june 14, 2011
                
                ctl = new STK_StakeholderMgmtController();
        ctl.roleCriteria = null;
        ctl.firstNameCriteria = '';
        ctl.nameCriteria = '';
        
        ctl.searchStk();
        
        System.assert(ctl.searchPerformed <> true);

        ctl = new STK_StakeholderMgmtController();
        ctl.oNewContact = new DMTStakeholder__c();
        ctl.oNewContact.LastName__c = 'Test';
        ctl.oNewContact.ProjectRoles__c = 'Requester';
        
        ctl.createContactAndAddStk(); 
        ctl.switchDisplay();
        ctl.commitChanges();
                
                test.setCurrentPageReference(new PageReference('/apex/STK_StakeholderMmt?Id='+oOrg.Id));
        ctl = new STK_StakeholderMgmtController();
                
                //end june 14, 2011                             
                
                test.stopTest();
                
        }
        
        //added on June 14, 2011
        public static testmethod void testCHRSTKManagement()
    {
        List<DMTStakeholder__c> ContactList = new List<DMTStakeholder__c>();
        List<ChangeRequestStakeholder__c> CHRActorList = new List<ChangeRequestStakeholder__c>();
               
        
        Account testAccount = new Account(name='Schneider',Street__c='New Street', POBox__c='XXX');
        insert testAccount;

                
        CHR_ChangeReq__c testCHR = new CHR_ChangeReq__c();
        testCHR.Name = 'Test CHR';       
        insert testCHR;       
        
               
        
        Schema.DescribeFieldResult F = PRJ_Stakeholder__c.Role__c.getDescribe();
        REF_OrganizationRecord__c oOrg = Utils_TestMethods.createOrganizationRecord('Test','Business Organization') ;
        insert oOrg;
            
        for(Schema.PicklistEntry pickValue:f.getPicklistValues()){
            DMTStakeholder__c testContact= new DMTStakeholder__c(LastName__c='Test',firstName__c = 'Prenom',email__c='test@hotmail.com',ChangeReqRoles__c=pickValue.getValue(),L1__c =oOrg.Id);
            contactList.add(testContact);
        }
        
        insert contactList;


                
        Integer countCHRContact = 0;
        for(Schema.PicklistEntry pickValue:f.getPicklistValues())
        {  
            ChangeRequestStakeholder__c CHRactor = new ChangeRequestStakeholder__c();
            CHRactor.ChangeRequest__c = testCHR.id;
            CHRactor.ActorNew__c = contactList[countCHRContact].Id;
            CHRactor.Role__c = pickValue.getValue();
            CHRActorList.add(CHRactor);
        }
        insert CHRActorList;
        
        
        test.StartTest();

        test.setCurrentPageReference(new PageReference('/apex/STK_StakeholderMmt?Id='+testCHR.Id));
        STK_StakeholderMgmtController ctl = new STK_StakeholderMgmtController();
        ctl.roleCriteria = contactList[0].ChangeReqRoles__c;
        ctl.firstNameCriteria = contactList[0].FirstName__c;
        ctl.nameCriteria = contactList[0].LastName__c;
        
        ctl.searchStk();
        
        System.assert(ctl.resultlistsize>0);

        ctl = new STK_StakeholderMgmtController();
        System.assert(ctl.stkListSize>0);
        List<String> roles = ctl.missingRoles;


        
        test.stopTest();

       
    }


}