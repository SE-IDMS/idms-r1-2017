@isTest(SeeAllData=true)
Public class AP_IpUndrContractFldUpdate_TEST 
{   
    static testMethod void testATT()
    {
            Country__c ctry = new Country__c(Name = 'Test', CountryCode__c = 'TS');
            insert ctry;
                      
            Account acc = Utils_TestMethods.createAccount();
            insert acc;
            SVMXC__Site__c site = new SVMXC__Site__c(Name = 'TestBatchName',
                                                 SVMXC__City__c = 'Junction City',
                                                 SVMXC__State__c = 'Kansas',
                                                 SVMXC__Street__c = '704 S. Adams',
                                                 SVMXC__Zip__c = '66441',
                                                 LocationCountry__c = ctry.Id,
                                                 SVMXC__Location_Type__c='Building',
                                                 SVMXC__Account__c=acc.Id,
                                                 TimeZone__c='India'
                                                 );
            insert site;
            
            SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(Name = 'Test', 
            SVMXC__Company__c = acc.Id,SVMXC__Active__c=true);
            insert contract;
            
            SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'Test',
                SVMXC__Site__c = site.Id, SVMXC__Company__c = acc.Id,RangeToCreate__c='test',
                SKUToCreate__c='Test2',BrandToCreate__c='schneider-electric',
                DeviceTypeToCreate__c='Circuit Breaker',OwnerId=UserInfo.getUserId(),UnderContract__c=true);
                insert ip;
        SVMXC__Service_Contract_Products__c cov= New SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c=contract.Id,
                                                                                         SVMXC__Installed_Product__c=ip.Id,
                                                                                         SVMXC__End_Date__c=date.today()
                                                                                         );
        insert cov;
    }
}