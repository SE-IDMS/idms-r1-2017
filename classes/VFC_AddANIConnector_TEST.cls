//*********************************************************************************
// Class Name       : VFC_AddANIConnector_TEST
// Purpose          : Test Class for VFC_AddANIConnector_TEST
// Created by       : Rakhi Kumar(Global Delivery Team)
// Date created     : 06 Mar 2013
// Modified by      :
// Date Modified    :
// Remarks          : 
///*********************************************************************************
@isTest
private class VFC_AddANIConnector_TEST {

    static testMethod void testAddANINumberForContact() {
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        
        //Create test records
        
        //Create Account
        Account accountRecord1 = Utils_TestMethods.createAccount();
        insert accountRecord1;
        
        //Create Contact
        Contact contactRecord1 = Utils_TestMethods.createContact(accountRecord1.Id, 'TestContact1');
        contactRecord1.Country__c = Utils_TestMethods.createCountry().Id;
        insert contactRecord1;
               
        
        //Create ANI record
        ANI__c aniRecord1 = new ANI__c(ANINumber__c= '+9000000001', Contact__c = contactRecord1.Id);
        Insert aniRecord1;
        
        VFC_AddANIConnector pageController = new VFC_AddANIConnector();
        ApexPages.currentPage().getParameters().put('ContactId', contactRecord1.Id);
        ApexPages.currentPage().getParameters().put('ANI', '+9000000001');
        pageController.AddANINumber();
        
        VFC_AddANIConnector pageController1 = new VFC_AddANIConnector();
        ApexPages.currentPage().getParameters().put('ContactId', contactRecord1.Id);
        ApexPages.currentPage().getParameters().put('ANI', '+9000000002');
        pageController1.AddANINumber();
        
               
     
    }
        static testMethod void testAddANINumberForAccount() {
        
        Account accountRecord1 = Utils_TestMethods.createAccount();
        insert accountRecord1;
        
        ANI__c aniRecord3 = new ANI__c(ANINumber__c= '+9000000001', Account__c = accountRecord1.Id);
        Insert aniRecord3;
        
        VFC_AddANIConnector pageController = new VFC_AddANIConnector();
        ApexPages.currentPage().getParameters().put('accountID', accountRecord1.Id);
        ApexPages.currentPage().getParameters().put('ANI', '+9000000001');
        pageController.AddANINumber();
        
        VFC_AddANIConnector pageController1 = new VFC_AddANIConnector();
        ApexPages.currentPage().getParameters().put('accountID', accountRecord1.Id);
        ApexPages.currentPage().getParameters().put('ANI', '+9000000002');
        pageController1.AddANINumber();
               
    }  
}