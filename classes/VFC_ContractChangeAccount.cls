/* 
    Author        : Stephen Norbury
    Created date  : 22/07/2014
    Release       : October 2014
    
    Description   : Page Controller for the Contract Account Change feature (Contract cloning)
    
    The Controler will allow the Account of an activated Contract to be changed by cloning the Contract, 
    setting the Account to the new Account and then re-activating the clone and Terminating the original Contract.
    All child records of the original Contract are then re-parented to the new Contract.
    
    Modified By   : 
    Modified Date : 
    Release       : 
    
    Description   :
    
*/  

public class VFC_ContractChangeAccount 
{
    public Contract c { get; set; }
    
    Id ContractAccountId = null;
    Id ContractContactId = null;
    
    // Constructor - get the Contract 
     
    public VFC_ContractChangeAccount (ApexPages.standardController stdController) 
    {
      c = (Contract) stdController.getRecord();
      
      ContractAccountId = c.AccountId;
      ContractContactId = c.ContactName__c;
    }
    
    public PageReference save ()
    {
      Id ContractId = c.Id;
      Id AccountId  = c.AccountId;
      Id ContactId  = c.ContactName__c;
      
      PageReference page = null;
      
      if (AccountId != null && AccountId != ContractAccountId)
      {
        // Account has changed
        
        Id CloneId = null;
      
        try
        {
          CloneId = deepCloneContract (ContractId, AccountId);
          System.debug('-->>CloneId'+CloneId);
        }
        catch (Exception e)
        {
          ApexPages.addMessages(e); return null;
        }
      
        if (CloneId != null)
        {
          Contract ClonedContract = Database.query ('select Id from Contract where Id = \'' + CloneId + '\'');
        
          if (ContactId != null && ContactId != ContractContactId)
          {
            ClonedContract.ContactName__c = ContactId;
          
            update ClonedContract;  
          }
        
          page = new ApexPages.StandardController(ClonedContract).view();
        }
        else
        {
          return null;
        }
      }
      
      else if (ContactId != null && ContactId != ContractContactId)
      {
        // Contact-only has changed
        
        c.ContactName__c = ContactId;
          
        update c; 
        
        page = new ApexPages.StandardController(c).view();  
      }
            
      else
      {
        page = new ApexPages.StandardController(c).view();
      }
          
      return page;
    }
    
    // Contract Management - change Contract Account
    
    public Id deepCloneContract (Id ContractId, Id AccountId)
    {     
      AP_ContractManagement.changeContractAccountResult result = new AP_ContractManagement.changeContractAccountResult();
          
      try
      {
        result = AP_ContractManagement.changeContractAccount (ContractId, AccountId);
      }
      catch (Exception e)
      {
        throw (e);
          
      }
      
      return result.ClonedContractId;
    }
    
       
    // End     
}