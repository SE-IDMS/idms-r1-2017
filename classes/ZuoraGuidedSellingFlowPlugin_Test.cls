/**
 * @author Benjamin LAMOTTE
 * @date Creation 24/05/2016
 * @description Test class for ZuoraGuidedSellingFlowPlugin
 */
@isTest(seeAllData = true)
private class ZuoraGuidedSellingFlowPlugin_Test {

	/**
	 * @author Benjamin LAMOTTE
	 * @date Creation 24/05/2016
	 * @description Test method for getFlows method
	 */
	@isTest
	private static void getFlowsTest() {
		List<Country__c> countryList = [SELECT Id from Country__c WHERE Name = 'France'];

		Id countryId = countryList.get(0).Id;

		Account acc = new Account(Name='Test Acc', SEAccountID__c='123', Tech_CountryCode__c='FR', City__c='Paris', Country__c=countryId, Street__c = 'street', ZipCode__c='123', Z_ProfileDiscount__c='1234');

		insert acc;

		zqu__Quote__c quote = new zqu__Quote__c(zqu__Account__c = acc.Id, BasePrp__c='1234', zqu__InvoiceOwnerId__c='1234', Entity__c = 'FIO');
		quote.zqu__SubscriptionType__c = 'New Subscription';
		quote.Type__c = 'Regular';
		
		insert quote;


		Test.startTest();
		ZuoraGuidedSellingFlowPlugin zGSFlowPlugin = new ZuoraGuidedSellingFlowPlugin();
		zGSFlowPlugin.getFlows(quote.Id);
		Test.stopTest();
		
	}
}