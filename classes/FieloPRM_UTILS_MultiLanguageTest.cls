/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 13/04/2016
* Description: Test class of FieloPRM_UTILS_MultiLanguage
*******************************************************************/

@isTest
public with sharing class FieloPRM_UTILS_MultiLanguageTest{
  
    public static testMethod void testUnit(){
        System.assertEquals(FieloPRM_UTILS_MultiLanguage.existFieldInObject(new Account(), 'Name'), true);
        System.assertEquals(FieloPRM_UTILS_MultiLanguage.existFieldInObject(new Account(), 'NameTest'), false);
        
        System.assertEquals(FieloPRM_UTILS_MultiLanguage.existFieldsInObject(new Account(), new set<String>{'Id','Name','No__existing_Field'}).size(), 1);
        
        System.assertEquals(FieloPRM_UTILS_MultiLanguage.getFieldsetLanguage(new Account(), 'Id', 'ES'), 'Id');
        System.assertEquals(FieloPRM_UTILS_MultiLanguage.getFieldsetLanguage(new FieloEE__News__c(), 'FieloEE__Title__c', 'EN'), 'Title_EN__c');
    }
    
}