public with sharing class ServicemaxUtils1 {
  
  public static Date getNextDate(Date currentDate, Decimal freq, String freUnit, BusinessHours bh)
  {
    Date nextDate = currentDate;
    if (freUnit == 'Weeks')
    {
      nextDate = nextDate.addDays((Integer) freq * 7);
    }
    else if (freUnit == 'Months')
    {
      nextDate = nextDate.addMonths((Integer)freq);
    }
    else
    {
      nextDate = nextDate.addDays((Integer)freq);
    }
    
    if (bh != null)
    {
      while (! validDay(nextDate, bh))
      {
        if (freUnit == 'Days'){
          nextDate = nextDate.addDays((Integer)freq);
        }else{
          nextDate = nextDate.addDays(1);
        }       
      }
    }
    
    return nextDate;
  }
  
  public static Boolean validDay(Date currDate, BusinessHours bh)
  {
    String dayOfWeek = Datetime.newInstance(currDate, Time.newInstance(0,0,0,0)).format('EEEE');
    system.debug('YTI#####start: '+bh.get(dayOfWeek + 'StartTime'));
      system.debug('YTI#####end: '+bh.get(dayOfWeek + 'EndTime'));
    
    if (bh.get(dayOfWeek + 'StartTime') == null || bh.get(dayOfWeek + 'EndTime') == null)
    {
      return false;
    }
    
    return true;
  }
  
  public static List<SObject> getCustomObjects(String objectName, String whereClause, Set<Id> idSet)
    {
        String query = 'SELECT ';
        Date todayDate = system.today();
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();

        // Grab the fields from the describe method and append them to the queryString one by one.
        Integer count = 0;
        for(String s : objectFields.keySet()) {
            if (count++ > 0)
            {
                query += ' ,';
            }
           query += s;
        }

        // Add FROM statement
        query += ' FROM ' + objectName;

        // Add on a WHERE/ORDER/LIMIT statement as needed
        query += ' ' + whereClause;
  
  
        return database.query(query);
    }
    
}