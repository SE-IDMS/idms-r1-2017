/*****************************************************************************************
    Author : Shruti Karn
    Description : For October 2013 Release to enable Partners to submit Assesssment 
                                       
*****************************************************************************************/
public class VFC_PartnerAssessment
{
    public PartnerAssessment__c prtAssessment{get;set;}
    public map<String,String> mapFieldValue{get;set;}
    public list<String> lstQueryFieldValue{get;set;}
    public String retURL{get;set;}
    public Boolean readOnly{get;set;}
    public VFC_PartnerAssessment(ApexPages.StandardController controller) {
    
        String orfId;
        List<String> fieldList = new List<String>();
        readOnly = false;
        mapFieldValue = new map<String,String>();
                
        fieldList.add('AssessmentScore__c');
        fieldList.add('Assessment__c');
        fieldList.add('Assessment__r.Name');
        fieldList.add('Assessment__r.PartnerPRogram__c');
        fieldList.add('Assessment__r.ProgramLevel__c');
        fieldList.add('OpportunityRegistrationForm__c');
        fieldList.add('OpportunityRegistrationForm__r.Name');
        fieldList.add('OpportunityRegistrationForm__r.PartnerProgram__c');
        fieldList.add('OpportunityRegistrationForm__r.ProgramLevel__c');
        fieldList.add('OpportunityRegistrationForm__r.PartnerProgram__r.Name');
        fieldList.add('OpportunityRegistrationForm__r.ProgramLevel__r.Name');
        //Added for EUS# 041707, on 10-Jul-2014, By Renjith Jose
        fieldList.add('OpportunityRegistrationForm__r.Tech_ORFOwner__r.Contact.AccountID'); 
        
        if(!test.isRunningTest())
            controller.addFields(fieldList);
        prtAssessment = (PartnerAssessment__c)controller.getRecord();
        orfID = ApexPages.currentPage().getParameters().get('orfID');
        system.debug('orfID :'+orfID );
        if(ApexPages.currentPage().getParameters().get('retURL')  != null)
            retURL = ApexPages.currentPage().getParameters().get('retURL');
        else
        {
           retURL = '/'+prtAssessment.OpportunityRegistrationForm__c;
           readOnly = true;
        }
        if(ApexPages.currentPage().getParameters().get('readOnly')  != null)
        {
            readOnly = Boolean.valueOF(ApexPages.currentPage().getParameters().get('readOnly'));
        }
        system.debug('retURL :'+retURL );
        if(prtAssessment.Id == null)
        {
            if(orfID != null && orfID.trim() != '')
            {
                prtAssessment.OpportunityRegistrationForm__c = orfID;
                prtAssessment.recordtypeid = Label.CLOCT13PRM01;
                OpportunityRegistrationForm__c orf = new OpportunityRegistrationForm__c();
                orf = [Select id,Name,PartnerProgram__C,PartnerProgram__r.Name,ProgramLevel__c,ProgramLevel__r.Name, Tech_ORFOwner__r.Contact.AccountID from 
                                                            OpportunityRegistrationForm__c where id = :orfID limit 1];
                  
                list<ACC_PartnerProgram__c> accountProgram = new list<ACC_PartnerProgram__c>();
                accountProgram = [Select id from ACC_PartnerProgram__c where partnerprogram__c = :orf.PartnerProgram__c 
                                                                and programlevel__c=:orf.ProgramLevel__c and account__c =:orf.Tech_ORFOwner__r.Contact.AccountID and active__c=true limit 1];
                
                prtAssessment.account__c = orf.Tech_ORFOwner__r.Contact.AccountID;
                if(accountProgram != null && !(accountProgram.isEmpty()))
                    prtAssessment.AccountAssignedProgram__c = accountProgram[0].Id;
                mapFieldValue.put('Opportunity Registration Form' , orf.Name);
                mapFieldValue.put('Partner Program' , orf.PartnerProgram__r.Name);
                mapFieldValue.put('Program Level' , orf.ProgramLevel__r.Name);
                lstQueryFieldValue = new list<String>();
                lstQueryFieldValue.add('PartnerProgram__c:'+orf.PartnerProgram__c);
                lstQueryFieldValue.add('ProgramLevel__c:'+orf.ProgramLevel__c);
            }
            
        }
        else if(prtAssessment.OpportunityRegistrationForm__c != null)
        {
            prtAssessment.account__c = prtAssessment.OpportunityRegistrationForm__r.Tech_ORFOwner__r.Contact.AccountID;
            mapFieldValue.put('Opportunity Registration Form' , prtAssessment.OpportunityRegistrationForm__r.Name);
            mapFieldValue.put('Partner Program' , prtAssessment.OpportunityRegistrationForm__r.PartnerProgram__r.Name);
            mapFieldValue.put('Program Level' , prtAssessment.OpportunityRegistrationForm__r.ProgramLevel__r.Name);
            lstQueryFieldValue = new list<String>();
            lstQueryFieldValue.add('PartnerProgram__c:'+prtAssessment.OpportunityRegistrationForm__r.PartnerProgram__c);
            lstQueryFieldValue.add('ProgramLevel__c:'+prtAssessment.OpportunityRegistrationForm__r.ProgramLevel__c);
            //retURL = prtAssessment.OpportunityRegistrationForm__c;
        }
        
        
    }


}