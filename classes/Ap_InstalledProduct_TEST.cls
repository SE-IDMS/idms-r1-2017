@isTest
private class Ap_InstalledProduct_TEST {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account accountSobj = Utils_TestMethods.createAccount();
        insert accountSobj;
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c();
        sc.SVMXC__Company__c = accountSobj.Id;
        insert sc;
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = accountSobj.id;
        site1.PrimaryLocation__c = true;
        insert site1;
        SVMXC__Site__c site2 = new SVMXC__Site__c();
        site2.Name = 'Test Location 2';
        site2.SVMXC__Street__c  = 'Test Street ';
        site2.SVMXC__Account__c = accountSobj.id;
        site2.PrimaryLocation__c = false;
        site2.SVMXC__Parent__c = site1.id;        
        insert site2;
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = accountSobj.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId1';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        insert ip1;
        SVMXC__Installed_Product__c ip3 = new SVMXC__Installed_Product__c();
        ip3.SVMXC__Company__c = accountSobj.id;
        ip3.Name = 'Test Intalled Product ';
        ip3.SVMXC__Status__c= 'new';
        ip3.GoldenAssetId__c = 'GoledenAssertId3';
        ip3.BrandToCreate__c ='Test Brand';
        
        SVMXC__Service_Contract_Products__c cp = new SVMXC__Service_Contract_Products__c();
        cp.SVMXC__Installed_Product__c = ip1.id;
        cp.SVMXC__Service_Contract__c= sc.id;
       // insert cp;
        
        try{
            SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
            ip2.SVMXC__Company__c = accountSobj.id;
            ip2.Name = 'Test Intalled Product ';
            ip2.SVMXC__Status__c= 'new';
            ip2.GoldenAssetId__c = 'GoledenAssertId1';
            ip2.BrandToCreate__c ='Test Brand';
            insert ip2;
            
        }
        catch(exception ex)
        {   
        }
        try{
            
            ip3.GoldenAssetId__c = 'GoledenAssertId1';
            
            update ip3;
            
        }
        catch(exception ex)
        {   
        }
        try{
        ip1.SVMXC__Site__c = site2.id;
        update ip1;
        }
        catch(Exception ex)
        {
            
        }
        
        
    }
}