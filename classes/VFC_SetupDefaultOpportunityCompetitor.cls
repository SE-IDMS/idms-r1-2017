/*
  BR-9556 - Default Opportunity Competitor functionality.
  Controller for VFP_DefaultOpportunityCompetitor
*/
public class VFC_SetupDefaultOpportunityCompetitor{

  //Declare variables
  public list<UserParameter__c> existingUserparameterlist{ get; set; }{existingUserparameterlist=new list<UserParameter__c>();}
  public Id UserId{get;set;}
  public Boolean hasCompetitor{ get; set; }{hasCompetitor=False;}
  public UserParameter__c userparam{ get; set; }{userparam=new UserParameter__c();}

  //Constructor
    public VFC_SetupDefaultOpportunityCompetitor(ApexPages.StandardController controller) {
        UserId=(ID)System.currentPageReference().getParameters().get( 'UserId' );
        // only one record can exist for one 
        existingUserparameterlist =[Select User__c,Competitor__c,Competitor2__c,Competitor3__c,Competitor4__c,Competitor5__c,Competitor6__c,Competitor7__c,Competitor8__c,Competitor9__c,Competitor10__c from UserParameter__c where  User__c=:UserId]; 
        if(existingUserparameterlist.size()>0 && existingUserparameterlist!=null) {
          userparam=existingUserparameterlist[0];
          hasCompetitor=TRUE;
        }
        else
          userparam.User__c=UserId;
    }

    //upsert Default Competitors in the User Parameter Object
    public PageReference upserttUserParameters(){
      List<Competitor__C> compList=new List<Competitor__C>();
      try{

         //"OTHER" Competitor cannot be added to the default list
        compList=[select Id,name from Competitor__C where Name=:Label.CLJUN16SLS03];
        if(userparam!=null && compList.size()>0 && (userparam.Competitor10__c==compList[0].Id || userparam.Competitor9__c==compList[0].Id || userparam.Competitor8__c==compList[0].Id || userparam.Competitor7__c==compList[0].Id || userparam.Competitor6__c==compList[0].Id || userparam.Competitor5__c==compList[0].Id || userparam.Competitor4__c==compList[0].Id || userparam.Competitor3__c==compList[0].Id || userparam.Competitor2__c==compList[0].Id || userparam.Competitor__c==compList[0].Id)){
          ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLJUN16SLS04);
          ApexPages.addMessage(myMsg);
          return null;
        }

        //remove duplicate competitors
        if(userparam.Competitor10__c!=null && ((userparam.Competitor9__c!=null && userparam.Competitor10__c==userparam.Competitor9__c) || (userparam.Competitor8__c!=null && userparam.Competitor10__c==userparam.Competitor8__c) || (userparam.Competitor7__c!=null && userparam.Competitor10__c==userparam.Competitor7__c) || (userparam.Competitor6__c!=null && userparam.Competitor10__c==userparam.Competitor6__c) || (userparam.Competitor5__c!=null && userparam.Competitor10__c==userparam.Competitor5__c) || (userparam.Competitor4__c!=null && userparam.Competitor10__c==userparam.Competitor4__c) || (userparam.Competitor3__c!=null && userparam.Competitor10__c==userparam.Competitor3__c) || (userparam.Competitor2__c!=null && userparam.Competitor10__c==userparam.Competitor2__c) || (userparam.Competitor__c!=null && userparam.Competitor10__c==userparam.Competitor__c)))
          userparam.Competitor10__c=null;
        if(userparam.Competitor9__c!=null  && ((userparam.Competitor8__c!=null && userparam.Competitor9__c==userparam.Competitor8__c) || (userparam.Competitor7__c!=null && userparam.Competitor9__c==userparam.Competitor7__c) || (userparam.Competitor6__c!=null && userparam.Competitor9__c==userparam.Competitor6__c) || (userparam.Competitor5__c!=null && userparam.Competitor9__c==userparam.Competitor5__c) || (userparam.Competitor4__c!=null && userparam.Competitor9__c==userparam.Competitor4__c) || (userparam.Competitor3__c!=null && userparam.Competitor9__c==userparam.Competitor3__c) || (userparam.Competitor2__c!=null && userparam.Competitor9__c==userparam.Competitor2__c) || (userparam.Competitor__c!=null && userparam.Competitor9__c==userparam.Competitor__c)))
          userparam.Competitor9__c=null;
        if(userparam.Competitor8__c!=null  && ((userparam.Competitor7__c!=null && userparam.Competitor8__c==userparam.Competitor7__c) || (userparam.Competitor6__c!=null && userparam.Competitor8__c==userparam.Competitor6__c) || (userparam.Competitor5__c!=null && userparam.Competitor8__c==userparam.Competitor5__c) || (userparam.Competitor4__c!=null && userparam.Competitor8__c==userparam.Competitor4__c) || (userparam.Competitor3__c!=null && userparam.Competitor8__c==userparam.Competitor3__c) || (userparam.Competitor2__c!=null && userparam.Competitor8__c==userparam.Competitor2__c) || (userparam.Competitor__c!=null && userparam.Competitor8__c==userparam.Competitor__c)))
          userparam.Competitor8__c=null;
        if(userparam.Competitor7__c!=null  && ((userparam.Competitor6__c!=null && userparam.Competitor7__c==userparam.Competitor6__c) || (userparam.Competitor5__c!=null && userparam.Competitor7__c==userparam.Competitor5__c) || (userparam.Competitor4__c!=null && userparam.Competitor7__c==userparam.Competitor4__c) || (userparam.Competitor3__c!=null && userparam.Competitor7__c==userparam.Competitor3__c) || (userparam.Competitor2__c!=null && userparam.Competitor7__c==userparam.Competitor2__c) || (userparam.Competitor__c!=null && userparam.Competitor7__c==userparam.Competitor__c)))
          userparam.Competitor7__c=null;
        if(userparam.Competitor6__c!=null  && ((userparam.Competitor5__c!=null && userparam.Competitor6__c==userparam.Competitor5__c) || (userparam.Competitor4__c!=null && userparam.Competitor6__c==userparam.Competitor4__c) || (userparam.Competitor3__c!=null && userparam.Competitor6__c==userparam.Competitor3__c) || (userparam.Competitor2__c!=null && userparam.Competitor6__c==userparam.Competitor2__c) || (userparam.Competitor__c!=null && userparam.Competitor6__c==userparam.Competitor__c)))
          userparam.Competitor6__c=null;
        if(userparam.Competitor5__c!=null  && ((userparam.Competitor4__c!=null && userparam.Competitor5__c==userparam.Competitor4__c) || (userparam.Competitor3__c!=null && userparam.Competitor5__c==userparam.Competitor3__c) || (userparam.Competitor2__c!=null && userparam.Competitor5__c==userparam.Competitor2__c) || (userparam.Competitor__c!=null  && userparam.Competitor5__c==userparam.Competitor__c)))
          userparam.Competitor5__c=null;
        if(userparam.Competitor4__c!=null  && ((userparam.Competitor3__c!=null && userparam.Competitor4__c==userparam.Competitor3__c) || (userparam.Competitor2__c!=null && userparam.Competitor4__c==userparam.Competitor2__c) || (userparam.Competitor__c!=null && userparam.Competitor4__c==userparam.Competitor__c)))
          userparam.Competitor4__c=null;
        if(userparam.Competitor3__c!=null  && ((userparam.Competitor2__c!=null && userparam.Competitor3__c==userparam.Competitor2__c) || (userparam.Competitor__c!=null && userparam.Competitor3__c==userparam.Competitor__c)))
          userparam.Competitor3__c=null;
        if(userparam.Competitor2__c!=null  && userparam.Competitor__c!=null && userparam.Competitor2__c==userparam.Competitor__c)
          userparam.Competitor2__c=null;

        
        if(hasCompetitor)
          update userparam; //If userparameter already exists
        else
          insert userparam; //if userparameter doesnt exists
      }
      catch(Exception ex){
          ApexPages.addMessages(ex);
          return null;
      }

      PageReference pr = new PageReference(Site.getPathPrefix()+'/'+UserId+'?noredirect=1&isUserEntityOverride=1');
      pr.setredirect(true); //setRedirect(true);  
      return pr;
    }

    //Cancel method
    public PageReference Cancel(){
      PageReference pr = new PageReference(Site.getPathPrefix()+'/'+UserId+'?noredirect=1&isUserEntityOverride=1');
        pr.setredirect(true); 
        return pr;
    }
}