@isTest(SeeAllData=true)
private class Handler_QuoteAfterUpdate_TEST {

    @isTest static void createQuote() {
        List<Country__c> countryList = [SELECT Id from Country__c WHERE Name = 'France'];
        
        Id countryId = countryList.get(0).Id;

        Account acc = new Account(Name='Test Acc', Country__c = countryId, SEAccountID__c='123', Tech_CountryCode__c='FR', City__c='Paris', Street__c = 'street', ZipCode__c='123', Z_ProfileDiscount__c='1234');
        insert acc;

        Contact con = new Contact(AccountId=acc.Id, Country__c = countryId, FirstName='Test', LastName='Contact', Email='test@test.com' );
        insert con;

        Contact con2 = new Contact(AccountId=acc.Id, Country__c = countryId, FirstName='Test', LastName='Contact', Email='test@test.com');
        insert con2;

        Zuora__CustomerAccount__c billAcc = new Zuora__CustomerAccount__c(Zuora__BillToId__c = '12345',Zuora__Account__c = acc.id, ReferenceName__c = 'Test Customer Account');
        insert billAcc;

        List<Zuora__CustomerAccount__c> billAccToUpdate = new List<Zuora__CustomerAccount__c>();

        Date today;
        DateTime dT = System.now();
        today = date.newinstance(dT.year(), dT.month(), dT.day());

        zqu__Quote__c quote = new zqu__Quote__c();
        quote.zqu__Service_Activation_Date__c = today;
        quote.zqu__BillToContact__c = con.id;
        quote.zqu__SoldToContact__c = con.id;
        quote.zqu__ZuoraSubscriptionID__c = '12345678901234567890123456789012';
        quote.zqu__StartDate__c = today;
        quote.zqu__Status__c = 'New';
        quote.Z_ProvisionDate__c = today;
        quote.zqu__Account__c = acc.Id;
        quote.Name = 'Test SFDC Quote';
        quote.zqu__SubscriptionType__c = 'New Subscription';
        quote.Bill2Contact__c ='Bill to Contact';
        quote.CurrencyIsoCode = 'EUR';
        quote.Entity__c = 'FIO';
       // quote.Installer__c = con.Id;
        //quote.Installer_Email__c = 'test@test.com';
        quote.Sold2Contact__c = 'Sold to Contact';
        quote.Type__c = 'Regular';
        quote.Trial__c = 'N';
        quote.zqu__InvoiceOwnerId__c = '2c92c0f954a999f20154a9a8ff010bf8';
        quote.zqu__ZuoraAccountID__c = '2c92c0f854a988870154aa1267a0789e';
        quote.zqu__InvoiceOwnerName__c = 'Testing A';
       // quote.zqu__GenerateInvoice__c = true;
       // quote.zqu__InvoiceID__c = '12345678901234567890123456789012';
        quote.ZuoraContactId__c = '';
        insert quote;
        system.debug('Quote = ' + quote);

     /*   zqu__QuoteAmendment__c qa = new zqu__QuoteAmendment__c(zqu__Quote__c = quote.id, 
                                                                zqu__AutoRenew__c = 'true',
                                                                zqu__InitialTerm__c = 12,
                                                                zqu__RenewalTerm__c = 12,
                                                                zqu__TermStartDate__c = today,
                                                                zqu__TermType__c = 'Termed',
                                                                zqu__Type__c = 'NewSubscription',
                                                                Name ='toto');
        insert qa;*/

        zqu__Quote__c quote2 = new zqu__Quote__c();
        quote2.zqu__Service_Activation_Date__c = today;
        quote2.zqu__BillToContact__c = con.id;
        quote2.zqu__SoldToContact__c = con.id;
        quote2.zqu__ZuoraSubscriptionID__c = '12345678901234567890123456789022';
        quote2.zqu__Status__c = 'New';
        quote2.Z_ProvisionDate__c = today;
        quote2.zqu__Account__c = acc.Id;
        quote2.Name = 'Test SFDC Quote';
        quote2.zqu__SubscriptionType__c = 'New Subscription';
        quote2.Bill2Contact__c ='Bill to Contact';
        quote2.CurrencyIsoCode = 'EUR';
        quote2.Entity__c = 'FIO';
        //quote2.Installer__c = con.Id;
        //quote2.Installer_Email__c = 'test@test.com';
        quote2.Sold2Contact__c = 'Sold to Contact';
        quote2.Type__c = 'Regular';
        quote2.Trial__c = 'N';
        quote2.zqu__InvoiceOwnerId__c = '2c92c0f954a999f20154a9a8ff010bf2';
        quote2.zqu__ZuoraAccountID__c = '2c92c0f854a988870154aa1267a0782e';
        quote2.zqu__InvoiceOwnerName__c = 'Testing A';
        //quote2.zqu__GenerateInvoice__c = true;
        //quote2.zqu__InvoiceID__c = '12345678901234567890123456789022';
        quote2.ZuoraContactId__c = '';
        insert quote2;
/*
        zqu__ZProduct__c prod = new zqu__ZProduct__c(zqu__SKU__c = 'TestSKU', zqu__ZuoraId__c = '12345678901234567890123456789012', Type__c = 'Primary', Z_ImmediateInvoice__c = 'No');
        insert prod;
        zqu__ProductRatePlan__c pRP = new zqu__ProductRatePlan__c(zqu__ZuoraId__c ='12345678901234567890123456789012' ,zqu__ZProduct__c = prod.Id);
        insert pRP;
        zqu__QuoteRatePlan__c qRP = new zqu__QuoteRatePlan__c(zqu__QuoteAmendment__c = qa.Id, zqu__ProductRatePlan__c = pRP.Id);
        insert qRP;
        Zuora__CustomerAccount__c ca = new Zuora__CustomerAccount__c(Zuora__Account__c = acc.id, ReferenceName__c = 'Test Name',ImmediateInvoiceAccount__c='No', Zuora__Zuora_Id__c=quote.zqu__ZuoraAccountID__c);
        insert ca;
        List<Zuora__CustomerAccount__c> listZuoraAccount = new List<Zuora__CustomerAccount__c>();
        listZuoraAccount.add(ca);
*/
        Set<String> zQuoteIdSet = new Set<String> ();
        zQuoteIdSet.add(quote.id);

        Set<String> zquotsBilltoIds = new Set<String> ();
        zquotsBilltoIds.add(quote.id);

        Set<String> zquotsSoldtoIds = new Set<String> ();
        zquotsSoldtoIds.add(quote.id);

        Set<String> zquotsBilltoIds2 = new Set<String> ();
        zquotsBilltoIds2.add(quote2.id);

        Set<String> zquotsSoldtoIds2 = new Set<String> ();
        zquotsSoldtoIds2.add(quote2.id);

       // Map<Id,zqu__Quote__c> oldZquoteMap = new Map<Id,zqu__Quote__c>();
        //oldZquoteMap.put(qRP.Id, quote);

       // Map<Id,zqu__Quote__c> newZquoteMap = new Map<Id,zqu__Quote__c>();
       // newZquoteMap.put(qRP.Id, quote);

        Set<String> zquotsSubsActivationDateIds = new Set<String> ();
        zquotsSubsActivationDateIds.add(quote.id);

        Set<String> zquoteSubsProvisionDateIds = new Set<String> ();
        zquoteSubsProvisionDateIds.add(quote.id);

        Test.startTest();
        Handler_QuoteAfterUpdate.updateZSubscriptionFields(zQuoteIdSet);
        Handler_QuoteAfterUpdate.updateBilltoSoldtoCon(zquotsBilltoIds,zquotsSoldtoIds);
        Handler_QuoteAfterUpdate.updateBilltoSoldtoCon(zquotsBilltoIds2,zquotsSoldtoIds);
        Handler_QuoteAfterUpdate.updateBilltoSoldtoCon(zquotsBilltoIds,zquotsSoldtoIds2);
        //Handler_QuoteAfterUpdate.UpdateActivationDateByCondition(oldZquoteMap,newZquoteMap);
        Handler_QuoteAfterUpdate.updateSubsDate(zquotsSubsActivationDateIds, zquoteSubsProvisionDateIds);
        Handler_QuoteAfterUpdate.BeginZuoraQuoteFlow(zquotsBilltoIds, zquotsSoldtoIds, zquotsSubsActivationDateIds, zquoteSubsProvisionDateIds);
        Handler_QuoteAfterUpdate.faketest();
        Test.stopTest();
    }
}