//DESCRIPTION: BATCH CLASS TO MIGRATE SALES CONTRIBUTOR RECORDS TO OPPORTUNITY OVERLAY SPLIT
global class BATCH_SalesContributorDataMigration implements Database.Batchable<sObject>
{
    global Id overlaySplitId,revenueSplitId;
    global string query;
    global BATCH_SalesContributorDataMigration(String query)
    {
         //QUERY ID OF SPLIT TYPE
         List<OpportunitySplitType> splitTypes = [select Id,DeveloperName from OpportunitySplitType where DeveloperName =: Label.CLAPR15SLS70 OR DeveloperName =:Label.CLAPR15SLS85];
         for(OpportunitySplitType os: splitTypes)
         {
             if(os.DeveloperName == Label.CLAPR15SLS70)
                 overlaySplitId = os.Id;
             if(os.DeveloperName == Label.CLAPR15SLS85)
                 revenueSplitId = os.Id;                 
         }
         this.query = query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> opportunities)
    {
        Map<Integer, OPP_SalesContributor__c> insRecs= new Map<Integer, OPP_SalesContributor__c>();
        Integer noOfRecs= 0;
        Set<OPP_SalesContributor__c> updSalesContrs = new Set<OPP_SalesContributor__c>();
        List<OPP_SalesContributor__c> updSalesContrslst = new List<OPP_SalesContributor__c>();         
        Map<Id, List<OpportunitySplit>> opptyIdSplits = new Map<Id, List<OpportunitySplit>>();
        List<OpportunitySplit> splitsToUpsert = new List<OpportunitySplit>();
        Set<Id> splitIds = new Set<Id>();
        List<OpportunitySplit> splitsToUpsertSet = new List<OpportunitySplit>();            
        Map<String, Opp_SalesContributor__c> salesContIdRecord = new Map<String, Opp_SalesContributor__c>();
        
        Set<Id> opptySplitIds = new Set<Id>();
        //ITERATES THROUGH OPPORTUNITY RECORDS
        for(Sobject s: opportunities)
        {
            OpportunitySplit op;  
            List<OPP_SalesContributor__c> slsConts = new List<OPP_SalesContributor__c>();         
            Boolean isOwnerPresent = false;
            Set<Id> slsUserIds= new Set<Id>();
            Opportunity o = (Opportunity)s;               
            Set<String> splitOpptyIds = new Set<String>();
            Map<String, OPP_SalesContributor__c> opptySlsCont = new Map<String, OPP_SalesContributor__c>();                        
            
            map<String, String> userOpptySlsName = new Map<String, String>();
            //TAKES THE REVENUE SPLIT RECORD OF OPPORTUNITY OWNER
            for(OpportunitySplit oss: o.OpportunitySplits)
            {
                if(oss.SplitOwnerId == o.OwnerId && oss.SplitTypeId == revenueSplitId)
                    op = oss;
            }
            
            //IF THERE ARE MULTIPLE SALES CONTRIBUTORS WITH SAME USER & SAME/DIFFERENT CONTRIBUTION %, SUMS THE SLS RECORDS
            for(OPP_SalesContributor__c slsCont: o.SalesContributors__r)
            {
                 if(slsCont.TECH_IsMigrated__c==false && slsCont.DM_UserActive__c == true)
                 {
                    salesContIdRecord.put(slsCont.Name ,slsCont);
                    if(opptySlsCont.containsKey(o.Id+' '+slsCont.User__c))   
                    {
                        system.debug(slsCont.Contribution__c+'Innnnnnnnnnnside FFFFFFF Loop'+opptySlsCont.get(o.Id+' '+slsCont.User__c));
                        OPP_SalesContributor__c sc = new OPP_SalesContributor__c();
                        sc = opptySlsCont.get(o.Id+' '+slsCont.User__c).clone();
                        sc.Contribution__c = opptySlsCont.get(o.Id+' '+slsCont.User__c).Contribution__c + slsCont.Contribution__c;
                        opptySlsCont.put(o.Id+' '+slsCont.User__c, sc);
                    }
                    else
                    {                
                        OPP_SalesContributor__c sc = new OPP_SalesContributor__c();
                        sc = slsCont.Clone();
                        opptySlsCont.put(o.Id+' '+slsCont.User__c, sc);
                    }   
                    userOpptySlsName.put(o.Id+' '+slsCont.User__c,slsCont.Name);                 
                }                                        
            }
            system.debug('**** Sum of Sales contributor % ******' + opptySlsCont.values());
            //ITERATES THROUGH SALES CONTRIBUTORS
            for(OPP_SalesContributor__c salesCont: opptySlsCont.values())
            {
                    system.debug(salesCont+'&&&&&&&&&&&&'+op);
                    List<OpportunitySplit> splits = new List<OpportunitySplit>();
                    if(opptyIdSplits.containsKey(o.Id))
                    {
                        splits.addAll(opptyIdSplits.get(o.Id));
                    }
                    //CREATES SPLIT RECORDS FOR DIFFERENT SCENARIOS
                    if(o.OwnerId == salesCont.User__c && salesCont.Contribution__c>0)
                    {
                        isOwnerPresent = true;
                        if(salesCont.Contribution__c < 100)
                        {
                            if(o.TECH_TotalContributionPercentage__c == 100)
                            {                      
                                        splits.add(new OpportunitySplit(Id = op.Id, Role__c = op.Role__c,
                                                    SplitPercentage = salesCont.Contribution__c,
                                                    SplitTypeId = revenueSplitId,
                                                    OpportunityId = op.OpportunityId,
                                                    TECH_SalesContributorId__c = userOpptySlsName.get(o.Id+' '+salesCont.User__c),
                                                    SplitOwnerId = op.SplitOwnerId,
                                                    Approved__c= op.Approved__c,
                                                    TECH_IsCloned__c= op.TECH_IsCloned__c));                                                               
                                        if(!(opptySplitIds.contains(op.Id)))
                                        {
                                            opptyIdSplits.put(o.Id, splits);  
                                            opptySplitIds.add(op.Id);
                                        }
                                        system.debug(opptyIdSplits+ '***Scenario 1******');                                                                 
                            }
                            else if(o.TECH_TotalContributionPercentage__c > 100)
                            {
                                    splits.add(new OpportunitySplit(Role__c = salesCont.Role__c,
                                                SplitPercentage = salesCont.Contribution__c,
                                                SplitTypeId = overlaySplitId,
                                                OpportunityId = salesCont.Opportunity__c,
                                                TECH_SalesContributorId__c = userOpptySlsName.get(o.Id+' '+salesCont.User__c),                                                
                                                SplitOwnerId = salesCont.User__c,
                                                Approved__c= salesCont.Approved__c,
                                                TECH_IsCloned__c= salesCont.TECH_IsCloned__c));                                                               

                                    if(!(splitOpptyIds.contains(op.Id+''+salesCont.User__c+''+overlaySplitId)))
                                    {
                                            opptyIdSplits.put(o.Id, splits);  
                                            splitOpptyIds.add(op.Id+''+salesCont.User__c+''+overlaySplitId);
                                    }          

                                        system.debug(opptyIdSplits+ '****Scenario 2*****');                                                                                                 
                                       
                            }
                            else if(o.TECH_TotalContributionPercentage__c < 100 && salesCont.Contribution__c != o.TECH_TotalContributionPercentage__c)
                            {
                                    splits.add(new OpportunitySplit(Role__c = salesCont.Role__c,
                                                SplitPercentage = salesCont.Contribution__c,
                                                SplitTypeId = overlaySplitId,
                                                OpportunityId = salesCont.Opportunity__c,
                                                TECH_SalesContributorId__c = userOpptySlsName.get(o.Id+' '+salesCont.User__c),                                                
                                                SplitOwnerId = salesCont.User__c,
                                                Approved__c= salesCont.Approved__c,
                                                TECH_IsCloned__c= salesCont.TECH_IsCloned__c));                                                                                                                             
                                    if(!(splitOpptyIds.contains(op.Id+''+salesCont.User__c+''+overlaySplitId)))
                                    {
                                            opptyIdSplits.put(o.Id, splits);  
                                            splitOpptyIds.add(op.Id+''+salesCont.User__c+''+overlaySplitId);
                                    }
                                        system.debug(opptyIdSplits+ '****Scenario 3*****');                                                                                         }                        
                        }
                        else if(salesCont.Contribution__c>100)
                        {
                                    splits.add(new OpportunitySplit(Role__c = salesCont.Role__c,
                                                SplitPercentage = salesCont.Contribution__c-100,
                                                SplitTypeId = overlaySplitId,
                                                OpportunityId = salesCont.Opportunity__c,
                                                TECH_SalesContributorId__c = userOpptySlsName.get(o.Id+' '+salesCont.User__c),                                                
                                                SplitOwnerId = salesCont.User__c,
                                                Approved__c= salesCont.Approved__c,
                                                TECH_IsCloned__c= salesCont.TECH_IsCloned__c));                                                               
                                    if(!(splitOpptyIds.contains(op.Id+''+salesCont.User__c+''+overlaySplitId)))
                                    {
                                            opptyIdSplits.put(o.Id, splits);  
                                            splitOpptyIds.add(op.Id+''+salesCont.User__c+''+overlaySplitId);
                                    }
                                        system.debug(opptyIdSplits+ '****Scenario 4*****');                                                                                                 
                        }
                        else if(salesCont.Contribution__c==100)
                        {
                                     splits.add(new OpportunitySplit(Id = op.Id, Role__c = op.Role__c,
                                                    SplitPercentage = op.SplitPercentage,
                                                    SplitTypeId = op.SplitTypeId,
                                                    OpportunityId = op.OpportunityId,
                                                    TECH_SalesContributorId__c = userOpptySlsName.get(o.Id+' '+salesCont.User__c),                                                    
                                                    SplitOwnerId = op.SplitOwnerId,
                                                    Approved__c= op.Approved__c,
                                                    TECH_IsCloned__c= op.TECH_IsCloned__c));                                                               
                                        if(!(opptySplitIds.contains(op.Id)))
                                        {
                                            opptyIdSplits.put(o.Id, splits);  
                                            opptySplitIds.add(op.Id);
                                        }
                                        system.debug(opptyIdSplits+ '***Scenario 1a******');  
                        }
                    }
                    else if(o.OwnerId != salesCont.User__c && salesCont.Contribution__c>0)
                    {
                         if(o.TECH_TotalContributionPercentage__c == 100)   
                         {
                                    splits.add(new OpportunitySplit(Role__c = salesCont.Role__c,
                                                SplitPercentage = salesCont.Contribution__c,
                                                SplitTypeId = revenueSplitId,
                                                OpportunityId = salesCont.Opportunity__c,
                                                TECH_SalesContributorId__c = userOpptySlsName.get(o.Id+' '+salesCont.User__c),                                                
                                                SplitOwnerId = salesCont.User__c,
                                                Approved__c= salesCont.Approved__c,
                                                TECH_IsCloned__c= salesCont.TECH_IsCloned__c));                                                               
                                    if(!(splitOpptyIds.contains(op.Id+''+salesCont.User__c+''+revenueSplitId)))
                                    {
                                            opptyIdSplits.put(o.Id, splits);  
                                            splitOpptyIds.add(op.Id+''+salesCont.User__c+''+revenueSplitId);
                                    }
                                        system.debug(opptyIdSplits+ '****Scenario 5*****');                                                                                                 
                         }
                         else
                         {
                                    splits.add(new OpportunitySplit(Role__c = salesCont.Role__c,
                                                SplitPercentage = salesCont.Contribution__c,
                                                SplitTypeId = overlaySplitId,
                                                OpportunityId = salesCont.Opportunity__c,
                                                TECH_SalesContributorId__c = userOpptySlsName.get(o.Id+' '+salesCont.User__c),                                                
                                                SplitOwnerId = salesCont.User__c,
                                                Approved__c= salesCont.Approved__c,
                                                TECH_IsCloned__c= salesCont.TECH_IsCloned__c));                                                                                               
                                    if(!(splitOpptyIds.contains(op.Id+''+salesCont.User__c+''+overlaySplitId)))
                                    {
                                            opptyIdSplits.put(o.Id, splits);  
                                            splitOpptyIds.add(op.Id+''+salesCont.User__c+''+overlaySplitId);
                                    } 
                                        system.debug(opptyIdSplits+ '****Scenario 6*****');                                                                                                                            
                         }
                    }                                                        
            }
            
            //THE SCENARIO EXECUTES IF OWNER RECORD IS NOT IN SALES CONTRIBUTORS
            if(isOwnerPresent == false && o.TECH_TotalContributionPercentage__c == 100)
            {
                List<OpportunitySplit> splitsList = new List<OpportunitySplit>();
                if(opptyIdSplits.get(o.Id)!=null)
                {
                    for(OpportunitySplit op1: opptyIdSplits.get(o.Id))
                    {
                        OpportunitySplit opptySplit = new OpportunitySplit();
                        opptySplit  = op1;
                        opptySplit.SplitTypeId = overlaySplitId;
                        splitsList.add(opptySplit);
                    }
                }
                if(splitsList.size()>0)
                {
                    opptyIdSplits.put(o.Id, splitsList);
                }
            }
            //IF THERE ARE INACTIVE USERS AND TOTAL CONTRIBUTION PERCENT EQUALS 100
            //INSERTS OWNER TO REVENUE SPLITS
            if(o.TECH_TotalContributionPercentage__c == 100)
            {
                Decimal totalContPerc = 0;
                List<OpportunitySplit> overlaySplits = new List<OpportunitySplit>();
                List<OpportunitySplit> revenueSplits = new List<OpportunitySplit>();
                List<OpportunitySplit> splitsList = new List<OpportunitySplit>();
                if(opptyIdSplits.get(o.Id)!=null)
                {
                    for(OpportunitySplit op1: opptyIdSplits.get(o.Id))
                    {
                        if(op1.SplitTypeId == revenueSplitId)
                        {
                            totalContPerc = totalContPerc+op1.SplitPercentage;
                            revenueSplits.add(op1);   
                        }
                        else
                        {
                            overlaySplits.add(op1);
                        }
                    }
                    if(totalContPerc!=100)
                    {
                        for(OpportunitySplit op1: revenueSplits)
                        {
                            OpportunitySplit opptySplit = new OpportunitySplit();
                            if(op1.SplitOwnerId == o.OwnerId)
                            {
                                opptySplit  = op1.clone();                                
                            }
                            else
                            {
                                opptySplit  = op1;
                            }
                            opptySplit.SplitTypeId = overlaySplitId;
                            splitsList.add(opptySplit);
                        }
                        if(overlaySplits.size()>0)
                        { 
                            splitsList.addall(overlaySplits);
                        }
                        if(splitsList.size()>0)
                            opptyIdSplits.put(o.Id, splitsList);
                    }   
                }
            }
                if(opptyIdSplits.containsKey(o.Id))
                    splitsToUpsertSet.addAll(opptyIdSplits.get(o.Id));
            }
            
            splitsToUpsert = new List<OpportunitySplit>();
            system.debug(splitsToUpsertSet + '****Upsert Data ******');
            splitsToUpsert.addAll(splitsToUpsertSet);
            system.debug(splitsToUpsert + '****Final Upsert Data******');

            //UPSERTS SPLIT RECORDS
            Database.upsertResult[] srList = Database.upsert(splitsToUpsert,false);     
            Integer recs = 0;            
            for (Database.upsertResult sr : srList) 
            {
                if((sr.isSuccess()))
                {
                    OPP_SalesContributor__c sls = new OPP_SalesContributor__c();
                    system.debug(recs + 'bbbbbbbbbbbb');
                        if(salesContIdRecord.containsKey(splitsToUpsert[recs].TECH_SalesContributorId__c))
                        {
                            sls = salesContIdRecord.get(splitsToUpsert[recs].TECH_SalesContributorId__c);                            
                            if(!(updSalesContrs.contains(sls)))
                            {
                                sls.TECH_IsMigrated__c = true;
                                sls.TECH_Error__c = '';
                                updSalesContrs.add(sls);                            
                            }
                        } 
                  }
                  else                
                  {
                    for(Database.Error err : sr.getErrors()) 
                    {
                        //UPDATES THE CORRESPONDING SALES CONTRIBUTOR RECORD IF NOT MIGRATED
                        OPP_SalesContributor__c sls = new OPP_SalesContributor__c();
                        if(salesContIdRecord.containsKey(splitsToUpsert[recs].TECH_SalesContributorId__c))
                        {
                            sls = salesContIdRecord.get(splitsToUpsert[recs].TECH_SalesContributorId__c);                            
                            system.debug(sls.Id + 'Opportunity Id '+ sls.Opportunity__c + 'Errrrrrros '+ err.getMessage()+ err.getFields());
                            if(!(updSalesContrs.contains(sls)))
                            {
                                sls.TECH_IsMigrated__c = false;
                                sls.TECH_Error__c = err.getMessage();
                                updSalesContrs.add(sls);         
                            }
                        }                        
                    }                    
                  }
                  recs = recs+1;
            }
            
            //UPDATES ERROR SALES CONTRIBUTOR RECORD WITH THE CORRESPONDING ERROR MESSAGE
            if(updSalesContrs.size()>0)
            {
                updSalesContrslst.addall(updSalesContrs);
                update updSalesContrslst;
            }         
    }
    
    //FINISH METHOD
    global void finish(Database.BatchableContext BC)
    {
        String email = 'bhuvana.subramaniyan@schneider-electric.com';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo('bhuvana.subramaniyan@schneider-electric.com');
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody(' Batch Process Completed ' );        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    
    }         
}