/*
    Author:Pooja Bhute
    Date:11/02/2011.
    Description:This class tests the AP04_AgreementTrigger class.    
*/

@isTest
private class AP04_AgreementTrigger_Test{     
static List<OppAgreement__c> aggList = new List<OppAgreement__c>();
static ProductLineForAgreement__c[] objProdLine = new List<ProductLineForAgreement__c>();
static User user,newUser,newUser2,newUser3 ;
static Country__c newCountry;
static OppAgreement__c objAgree;
static Account acc;
static Opportunity objOpp;
static OppAgreement__c objAgree2;
         
    static
    {
        newUser = Utils_TestMethods.createStandardUser('TestUser');         
         Database.SaveResult UserInsertResult = Database.insert(newUser, false);         
           
         if(!UserInsertResult.isSuccess())
             Database.Error err = UserInsertResult.getErrors()[0];
         
         newUser2 = Utils_TestMethods.createStandardUser('TestUse2');         
         Database.SaveResult SecondUserInsertResult = Database.insert(newUser2, false);         
           
         newUser3 = Utils_TestMethods.createStandardUser('TestUse3');         
         newUser3.IsActive = false;
         Database.SaveResult ThirdUserInsertResult = Database.insert(newUser3, false);
           
         if(!SecondUserInsertResult.isSuccess())
             Database.Error err = SecondUserInsertResult.getErrors()[0];        
         newCountry = Utils_TestMethods.createCountry();
         Database.SaveResult CountryInsertResult = Database.insert(newCountry, false);
           
         if(!CountryInsertResult.isSuccess())
             Database.Error err = CountryInsertResult.getErrors()[0];
         objAgree = Utils_TestMethods.createFrameAgreement(newUser.Id);         
        objAgree.PhaseSalesStage__c = '3 - Identify & Qualify';   
            objAgree.AgreementType__c = 'Frame Agreement';
         insert objAgree;
          
         
         aggList.add(objAgree); 
         
         // 2. Create the Frame Opportunity
         acc = Utils_TestMethods.createAccount();
         Database.SaveResult AccountInsertResult = Database.insert(acc, false);
           
         if(!AccountInsertResult.isSuccess())
             Database.Error err = AccountInsertResult.getErrors()[0];
         objOpp = Utils_TestMethods.createFrameOpporutnity(acc.Id, objAgree.Id, newCountry.Id);
         Database.SaveResult OpportunityInsertResult = Database.insert(objOpp, false);
         
         objAgree2 = Utils_TestMethods.createFrameAgreementWithAccount(newUser.Id,acc.Id );
         objAgree2.PhaseSalesStage__c = '3 - Identify & Qualify';   
            objAgree2.AgreementType__c = 'Frame Agreement';         
             insert objAgree2;
    }
     static TestMethod void testGrantAccessInsertUpdate() 
     {    
         System.Debug('****** Testing the Method of Granting access to the user ****');  
             
         Test.startTest();       
         //December Release - Creating the OEM Agreement for the method updateOEMAgreementOpp()
        Competitor__c compt1 = Utils_TestMethods.createCompetitor();
        Database.SaveResult comp1 = Database.insert(compt1,false);
        
        Competitor__c compt2 = Utils_TestMethods.createCompetitor();
        Database.SaveResult comp2 = Database.insert(compt2,false);
        
         OppAgreement__c objOEMAgreement = Utils_TestMethods.createOEMAgreementWithAccount(newUser.Id,acc.Id);
            objOEMAgreement.Opportunity_Type__c='Standard';
            objOEMAgreement.PhaseSalesStage__c = '3 - Identify & Qualify';   
            objOEMAgreement.WinningCompetitor__c=compt1.Id;
            Database.SaveResult AgreementInsertResult3 = Database.insert(objOEMAgreement, false);
            
         Opportunity objOEMOpp = Utils_TestMethods.createFrameOpporutnity(acc.Id, objOEMAgreement.Id, newCountry.Id);
         Database.SaveResult OEMOpportunityInsertResult = Database.insert(objOEMOpp, false);   

         OPP_OpportunityCompetitor__c oppComp = Utils_TestMethods.createOppCompetitor(compt1.Id,objOEMOpp.Id);
         Database.SaveResult oppttyAgr1 = Database.insert(oppComp, false);

         objOEMAgreement.PhaseSalesStage__c='4 - Influence & Develop';
         objOEMAgreement.WinningCompetitor__c=compt2.Id;         
         Database.SaveResult AgreementInsertResult4 = Database.update(objOEMAgreement, false);

         // 3. Create the Product Line 
         ProductLineForAgreement__c objPLAGR = Utils_TestMethods.createProductLineAgr(objAgree.Id,800000);         
         Database.SaveResult ProductLineInsertResult = Database.insert(objPLAGR, false);
             
          if(!ProductLineInsertResult.isSuccess())
             Database.Error err = ProductLineInsertResult.getErrors()[0];
                
             OppAgreement__c objAgree1 = Utils_TestMethods.createFrameAgreement(newUser.Id);         
             Database.SaveResult AgreementInsertResult1 = Database.insert(objAgree1, false);
             aggList.add(objAgree1);
              
             //AP04_AgreementTrigger.ValidateAgrrementStatusUpdation(aggList);  
             //aggList.add(objAgree1);  
             
             objAgree.PhaseSalesStage__c = Label.CL00220;
             objAgree.AgreementType__c = Label.CL00239;
             objAgree.AgreementManager__c = newUser2.Id;
             update objAgree;
              ProductLineForAgreement__c objPLAGR1 = Utils_TestMethods.createLeadingProductLineAgr(objAgree1.Id,0);
             
             Database.SaveResult ProductLineInsertResult1 = Database.insert(objPLAGR1, false);   
             
             objPLAGR.LeadingProductLine__c = False;
             update objPLAGR;
             objPLAGR.LeadingProductLine__c = True;
             update objPLAGR;
          
         Test.stopTest();                                                     
    }
}