public class AP_UpdateCCCActionAge{
    
    public static void updateCCCActionAgeonInsert(list<CCCAction__c> lstNewCCCAction){
        //*********************************************************************************
        // Method Name      : updateCCCActionAgeonInsert
        // Purpose          : To populate TECH_StatusEnterDate__c from CCC Action on Insert 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 18th June 2012
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Sep - 11 Release
        ///********************************************************************************/
        
        System.Debug('****AP_UpdateCCCActionAge  updateCCCActionAgeonInsert  Started****');
        for(integer i=0;i<lstNewCCCAction.size();i++){
            lstNewCCCAction[i].TECH_StatusEnterDate__c  = System.now();
        }
        System.Debug('****AP_UpdateCCCActionAge  updateCCCActionAgeonInsert  Finished****');
    }
    
    public static void updateTaskAgeOnUpdate(map<Id,CCCAction__c> mapNewCCCAction , map<Id,CCCAction__c> mapOldAction){
        //*********************************************************************************
        // Method Name      : updateTaskAgeOnUpdate
        // Purpose          : To populate TECH_StatusEnterDate__c from CCC Action on Update 
        // Created by       : Vimal Karunakaran - Global Delivery Team
        // Date created     : 18th June 2012
        // Modified by      :
        // Date Modified    :
        // Remarks          : For Sep - 11 Release
        ///********************************************************************************/
        
        
        System.Debug('****AP_UpdateCCCActionAge  updateTaskAgeOnUpdate  Started****');
        for(ID actionID : mapNewCCCAction.keySet()){
            if(mapNewCCCAction.get(actionID).Status__c != mapOldAction.get(actionID).Status__c){
                Double dblDateDiff = (Double)((System.now().getTime() - mapNewCCCAction.get(actionID).TECH_StatusEnterDate__c.gettime()))/3600000;
                
                //When Status = 'In Progress'
                if(mapOldAction.get(actionID).Status__c == Label.CLSEP12CCC06){
                    if(mapNewCCCAction.get(actionID).TECH_InProgressAge__c != null){
                        mapNewCCCAction.get(actionID).TECH_InProgressAge__c = mapNewCCCAction.get(actionID).TECH_InProgressAge__c + dblDateDiff;
                    }
                    else{
                        mapNewCCCAction.get(actionID).TECH_InProgressAge__c = dblDateDiff;
                    }
                }
                //When Status = 'Completed'
                if(mapOldAction.get(actionID).Status__c == Label.CLSEP12CCC07){
                    if(mapNewCCCAction.get(actionID).TECH_ClosedAge__c != null){
                        mapNewCCCAction.get(actionID).TECH_ClosedAge__c = mapNewCCCAction.get(actionID).TECH_ClosedAge__c + dblDateDiff;
                    }
                        
                    else{
                        mapNewCCCAction.get(actionID).TECH_ClosedAge__c = dblDateDiff;
                    }
                }
                
                //When Status = 'Open'
                if(mapOldAction.get(actionID).Status__c == Label.CLSEP12CCC08){
                    if(mapNewCCCAction.get(actionID).TECH_OpenAge__c != null){
                        mapNewCCCAction.get(actionID).TECH_OpenAge__c = mapNewCCCAction.get(actionID).TECH_OpenAge__c + dblDateDiff;
                    }
                    else{
                        mapNewCCCAction.get(actionID).TECH_OpenAge__c = dblDateDiff;
                        system.debug('test6:'+mapNewCCCAction.get(actionID).TECH_OpenAge__c);
                    }
                }  
                mapNewCCCAction.get(actionID).TECH_StatusEnterDate__c = System.now();
            }
        }
        System.Debug('****AP_UpdateCCCActionAge  updateTaskAgeOnUpdate  Finished****');
    }
}