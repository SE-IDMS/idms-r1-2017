/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | AP_EUS                                                                             |
|                       |                                                                                    |
|     - Object(s)       | End User Support                                                                   |
|     - Description     |   - Insert or Update Escalation History for KPIs calculation                       |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | 21-oct-2012                                                                        |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
public class AP_EUS
{
   public static void UpdateEscalationHistory(List<EUS_EndUserSupport__c> lstEUS)
   {
      System.Debug('## >>> BEGIN: AP_EUS.UpdateEscalationHistory(List<EUS_EndUserSupport__c>) <<<');
      System.Debug('## >>>        lstEUS = ' + lstEUS);

      //Store all EUS Ids in a set
      set<Id> setEUSID = new set<Id>();
      for (EUS_EndUserSupport__c varEUS : lstEUS)
      {
         setEUSID.add(varEUS.Id);
      }

      //Query for finding last open Escalation History record per EUS
      List<EndUserSupportEscalationHistory__c> lstEscalation = [SELECT Id,
                                                                       EndUserSupport__c,
                                                                       From__c,
                                                                       FromDate__c,
                                                                       Status__c,
                                                                       To__c,
                                                                       ToDate__c,
                                                                       TrackingField__c
                                                                FROM   EndUserSupportEscalationHistory__c
                                                                WHERE  TrackingField__c = :System.Label.CLNOV13EUS04  //Escalation Level
                                                                AND    EndUserSupport__c in :setEUSID
                                                                ORDER BY EndUserSupport__c Asc, FromDate__c Asc];
      System.Debug('## >>>        lstEscalation = ' + lstEscalation);

      //Create a Map of Escalations - 'Last' Escalation record is stored since records are ordered by From Date Asc (older to newer)
      Map<Id, EndUserSupportEscalationHistory__c> mapEscalation = new Map<Id, EndUserSupportEscalationHistory__c>();
      for (EndUserSupportEscalationHistory__c varEscalation : lstEscalation)
      {
         mapEscalation.put(varEscalation.EndUserSupport__c, varEscalation);
      }

      //Define actions
      List<EndUserSupportEscalationHistory__c> lstNEWEscalation = new List<EndUserSupportEscalationHistory__c>();
      List<EndUserSupportEscalationHistory__c> lstUPDEscalation = new List<EndUserSupportEscalationHistory__c>();
      for (EUS_EndUserSupport__c varEUS : lstEUS)
      {
         EndUserSupportEscalationHistory__c tmpEscalation = mapEscalation.get(varEUS.Id);
         System.Debug('## >>> Escalations for EUS[' + varEUS.Id + '] are: ' + tmpEscalation);

         //1st Case: tmpEscalation is null when a new EUS is created or cloned
         if (tmpEscalation == null)
         {
            System.Debug('## >>>>>> New EUS: ' + varEUS.Id + ' ==> Add 1st Escalation record');
            //In the following conditions:
            //  - System.Label.CLNOV13EUS05 = Closed
            //  - System.Label.CLNOV13EUS06 = Closed - Duplicate
            //  - System.Label.CLNOV13EUS07 = Under Champion's Validation
            if(varEUS.Status__c != System.Label.CLNOV13EUS05 && varEUS.Status__c != System.Label.CLNOV13EUS06 && varEUS.Status__c != System.Label.CLNOV13EUS07)
            {
               System.Debug('## >>>>>> EUS not closed');
               lstNEWEscalation.add(new EndUserSupportEscalationHistory__c(EndUserSupport__c = varEUS.Id,
                                                                           From__c           = varEUS.EscalationLevel__c,
                                                                           FromDate__c       = varEUS.LastModifiedDate,
                                                                           Status__c         = varEUS.Status__c,
                                                                           TrackingField__c  = System.Label.CLNOV13EUS04  //Escalation Level
                                                                         ));
            }
            else
            {
               System.Debug('## >>>>>> EUS closed');
               lstNEWEscalation.add(new EndUserSupportEscalationHistory__c(EndUserSupport__c = varEUS.Id,
                                                                           From__c           = varEUS.EscalationLevel__c,
                                                                           FromDate__c       = varEUS.LastModifiedDate,
                                                                           Status__c         = varEUS.Status__c,
                                                                           To__c             = varEUS.EscalationLevel__c,
                                                                           ToDate__c         = varEUS.LastModifiedDate,
                                                                           TrackingField__c  = System.Label.CLNOV13EUS04  //Escalation Level
                                                                         ));
            }
         }
         else
         {
            //2nd Case: tmpEscalation is not null when a new EUS is updated
            System.Debug('## >>>>>> Not a new EUS: ' + varEUS.Id);
            if (varEUS.Status__c == tmpEscalation.Status__c && varEUS.EscalationLevel__c == tmpEscalation.From__c)
            {
               System.Debug('## >>>>>> No change in Status and Escalation Level => no change in History');
            }
            if (varEUS.Status__c == tmpEscalation.Status__c && varEUS.EscalationLevel__c != tmpEscalation.From__c)
            {
               System.Debug('## >>>>>> No change in Status but Escalation Level has been changed');
               if(varEUS.Status__c == System.Label.CLNOV13EUS05 || varEUS.Status__c == System.Label.CLNOV13EUS06 || varEUS.Status__c == System.Label.CLNOV13EUS07)
               {
                  System.Debug('## >>>>>> Status = ' + varEUS.Status__c + ' => only update To');
                  lstUPDEscalation.add(new EndUserSupportEscalationHistory__c(Id                = tmpEscalation.Id,
                                                                              To__c             = varEUS.EscalationLevel__c
                                                                            ));
               }
               else
               {
                  System.Debug('## >>>>>> Status = ' + varEUS.Status__c + ' => update in History and new record');
                  lstUPDEscalation.add(new EndUserSupportEscalationHistory__c(Id                = tmpEscalation.Id,
                                                                              To__c             = varEUS.EscalationLevel__c,
                                                                              ToDate__c         = varEUS.LastModifiedDate
                                                                            ));
                  lstNEWEscalation.add(new EndUserSupportEscalationHistory__c(EndUserSupport__c = varEUS.Id,
                                                                              From__c           = varEUS.EscalationLevel__c,
                                                                              FromDate__c       = varEUS.LastModifiedDate,
                                                                              Status__c         = varEUS.Status__c,
                                                                              TrackingField__c  = System.Label.CLNOV13EUS04  //Escalation Level
                                                                            ));
               }
            }
            if (varEUS.Status__c != tmpEscalation.Status__c && varEUS.EscalationLevel__c == tmpEscalation.From__c)
            {
               System.Debug('## >>>>>> Status has been changed and no change in Escalation Level');
               if(   (varEUS.Status__c == System.Label.CLNOV13EUS05 || varEUS.Status__c == System.Label.CLNOV13EUS06 || varEUS.Status__c == System.Label.CLNOV13EUS07)
                  && (tmpEscalation.Status__c != System.Label.CLNOV13EUS05 && tmpEscalation.Status__c != System.Label.CLNOV13EUS06 && tmpEscalation.Status__c != System.Label.CLNOV13EUS07))
               {
                  System.Debug('## >>>>>> Status = ' + varEUS.Status__c + ' Open EUS to closed EUS => update Status and To fields on History');
                  lstUPDEscalation.add(new EndUserSupportEscalationHistory__c(Id             = tmpEscalation.Id,
                                                                              Status__c      = varEUS.Status__c,
                                                                              To__c          = varEUS.EscalationLevel__c,
                                                                              ToDate__c      = varEUS.LastModifiedDate
                                                                            ));
               }
               if(   (varEUS.Status__c == System.Label.CLNOV13EUS05 || varEUS.Status__c == System.Label.CLNOV13EUS06 || varEUS.Status__c == System.Label.CLNOV13EUS07)
                  && (tmpEscalation.Status__c == System.Label.CLNOV13EUS05 || tmpEscalation.Status__c == System.Label.CLNOV13EUS06 || tmpEscalation.Status__c == System.Label.CLNOV13EUS07))
               {
                  System.Debug('## >>>>>> Status = ' + varEUS.Status__c + ' Closed EUS to closed EUS => update Status field on History');
                  lstUPDEscalation.add(new EndUserSupportEscalationHistory__c(Id             = tmpEscalation.Id,
                                                                              Status__c      = varEUS.Status__c
                                                                            ));
               }
               if(   (varEUS.Status__c != System.Label.CLNOV13EUS05 && varEUS.Status__c != System.Label.CLNOV13EUS06 && varEUS.Status__c != System.Label.CLNOV13EUS07)
                  && (tmpEscalation.Status__c != System.Label.CLNOV13EUS05 && tmpEscalation.Status__c != System.Label.CLNOV13EUS06 && tmpEscalation.Status__c != System.Label.CLNOV13EUS07))
               {
                  System.Debug('## >>>>>> Status = ' + varEUS.Status__c + ' Open EUS to open EUS => update Status field on History');
                  lstUPDEscalation.add(new EndUserSupportEscalationHistory__c(Id             = tmpEscalation.Id,
                                                                              Status__c      = varEUS.Status__c
                                                                            ));
               }
               if(   (varEUS.Status__c != System.Label.CLNOV13EUS05 && varEUS.Status__c != System.Label.CLNOV13EUS06 && varEUS.Status__c != System.Label.CLNOV13EUS07)
                  && (tmpEscalation.Status__c == System.Label.CLNOV13EUS05 || tmpEscalation.Status__c == System.Label.CLNOV13EUS06 || tmpEscalation.Status__c == System.Label.CLNOV13EUS07))
               {
                  System.Debug('## >>>>>> Status = ' + varEUS.Status__c + ' Closed EUS to open EUS => New Escalation record');
                  lstNEWEscalation.add(new EndUserSupportEscalationHistory__c(EndUserSupport__c = varEUS.Id,
                                                                              From__c           = varEUS.EscalationLevel__c,
                                                                              FromDate__c       = varEUS.LastModifiedDate,
                                                                              Status__c         = varEUS.Status__c,
                                                                              TrackingField__c  = System.Label.CLNOV13EUS04  //Escalation Level
                                                                            ));
               }
            }
            if (varEUS.Status__c != tmpEscalation.Status__c && varEUS.EscalationLevel__c != tmpEscalation.From__c)
            {
               System.Debug('## >>>>>> Status and Escalation Level have been changed');
               System.Debug('## >>>>>> Status = ' + varEUS.Status__c + ' => update Status and To fields on History');
               lstUPDEscalation.add(new EndUserSupportEscalationHistory__c(Id                = tmpEscalation.Id,
                                                                           Status__c         = varEUS.Status__c,
                                                                           To__c             = varEUS.EscalationLevel__c,
                                                                           ToDate__c         = varEUS.LastModifiedDate
                                                                         ));
               if(varEUS.Status__c != System.Label.CLNOV13EUS05 && varEUS.Status__c != System.Label.CLNOV13EUS06 && varEUS.Status__c != System.Label.CLNOV13EUS07)
               {
                  System.Debug('## >>>>>> and create new Escalation record - EUS not closed');
                  lstNEWEscalation.add(new EndUserSupportEscalationHistory__c(EndUserSupport__c = varEUS.Id,
                                                                              From__c           = varEUS.EscalationLevel__c,
                                                                              FromDate__c       = varEUS.LastModifiedDate,
                                                                              Status__c         = varEUS.Status__c,
                                                                              TrackingField__c  = System.Label.CLNOV13EUS04  //Escalation Level
                                                                            ));
               }
               else
               {
                  System.Debug('## >>>>>> and create new Escalation record - EUS closed');
                  lstNEWEscalation.add(new EndUserSupportEscalationHistory__c(EndUserSupport__c = varEUS.Id,
                                                                              From__c           = varEUS.EscalationLevel__c,
                                                                              FromDate__c       = varEUS.LastModifiedDate,
                                                                              Status__c         = varEUS.Status__c,
                                                                              To__c             = varEUS.EscalationLevel__c,
                                                                              ToDate__c         = varEUS.LastModifiedDate,
                                                                              TrackingField__c  = System.Label.CLNOV13EUS04  //Escalation Level
                                                                            ));
               }
            }
         }
      }

      //Perform actions in the database
      if (lstNEWEscalation != null && !lstNEWEscalation.isEmpty())
      {
         insert(lstNEWEscalation);
      }

      if (lstUPDEscalation != null && !lstUPDEscalation.isEmpty())
      {
         update(lstUPDEscalation);
      }

      System.Debug('## >>> END:   AP_EUS.UpdateEscalationHistory(List<EUS_EndUserSupport__c>) <<<');
   }
}