/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 03/08/2012
    Description     : Class for triggers FE_FundingEntityBeforeInsert and FE_FundingEntityBeforeUpdate.
                      Creates a list of funding entity records to be processed by the FE_FundingEntityUtils class
*/

public with sharing class FE_FundingEntityTriggers
{
    public static void fundingentityBeforeUpdate(Map<Id,DMTFundingEntity__c> newMap,Map<Id,DMTFundingEntity__c> oldMap)
    {
        System.Debug('****** fundingentityBeforeUpdate Start ****'); 
        List<DMTFundingEntity__c> fundingentityListToProcessCONDITION1 = new List<DMTFundingEntity__c>();                
        
        for (ID feId:newMap.keyset())
        {
            DMTFundingEntity__c newFE = newMap.get(feId);
            DMTFundingEntity__c oldFE = oldMap.get(feId);
            
            if((newFE.ActiveForecast__c == true && oldFE.ActiveForecast__c == false) ||(newFE.HFMCode__c!=oldFE.HFMCode__c))
            {
                fundingentityListToProcessCONDITION1.add(newFE);                
            }
            SYstem.debug('-----------Fe'+newFE);
                                    
        }
               
        if (fundingentityListToProcessCONDITION1.size()>0) 
            FE_FundingEntityUtils.deactivateActiveFundingEntity(fundingentityListToProcessCONDITION1,true);

        // September Release Change by Srikant Joshi START  
        FE_FundingEntityUtils.updateProjectRequest(newMap.values());
        // December Release Change by Srikant Joshi START 
        //Commented P & L logic in triggers for July 2013 Release by Priyanka Shetty
        //FE_FundingEntityUtils.updatePLFields(newMap,oldMap);

        System.Debug('****** fundingentityBeforeUpdate End ****');             

    }   
    /*
    public static void fundingentityAfterUpdate(List<DMTFundingEntity__c> newFundingEntities){
        System.Debug('****** fundingentityAfterUpdate Start ****'); 
     
        FE_FundingEntityUtils.updateProjectRequest(newFundingEntities);

        System.Debug('****** fundingentityAfterUpdate End ****');             

    }
    */
    // September Release Change by Srikant Joshi END
    public static void fundingentityBeforeDelete(List<DMTFundingEntity__c> newFundingEntities){
        System.Debug('****** fundingentityAfterUpdate Start ****'); 
     
        FE_FundingEntityUtils.updateProjectRequest(newFundingEntities);

        System.Debug('****** fundingentityAfterUpdate End ****');             

    }


    public static void fundingentityBeforeInsert(List<DMTFundingEntity__c> newFundingEntities)
    {
        System.Debug('****** fundingentityBeforeInsert Start ****'); 
        List<DMTFundingEntity__c> fundingentityListToProcessCONDITION1 = new List<DMTFundingEntity__c>();
        //List<DMTFundingEntity__c> fundingentityListToProcessCONDITION2 = new List<DMTFundingEntity__c>();
        
        for (DMTFundingEntity__c newFE:newFundingEntities)
        {                                  
            if(newFE.ActiveForecast__c == true)
            {
                fundingentityListToProcessCONDITION1.add(newFE);
                //fundingentityListToProcessCONDITION2.add(newFE);                
            }

        }
        
        
        if (fundingentityListToProcessCONDITION1.size()>0) 
            FE_FundingEntityUtils.deactivateActiveFundingEntity(fundingentityListToProcessCONDITION1,false);

        if (newFundingEntities.size()>0) 
            FE_FundingEntityUtils.checkFundingEntityExists(newFundingEntities);
            
        if(newFundingEntities!= null && newFundingEntities.size()>0)
            FE_FundingEntityUtils.fundentityBeforeInsert(newFundingEntities);
            
        System.Debug('****** fundingentityBeforeInsert End ****');
    }   
    
}