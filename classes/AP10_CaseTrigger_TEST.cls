/*
Author          : Abhishek (ACN) 
Date Created    : 19/05/2011
Description     : Test class for the apex class AP10_CaseTrigger.
*/


@isTest
private class AP10_CaseTrigger_TEST{
    static testMethod void caseTriggerTestMethod(){
        Account objTestAccount = Utils_TestMethods.createAccount();
        Database.insert(objTestAccount);        
 
        Contact objTestContact = Utils_TestMethods.createContact(objTestAccount.Id,'Test Contact For Cases');
        Database.insert(objTestContact);     
        //To Cover the AP_TaskHandler
        Task taskObj = Utils_TestMethods.createTask(Null,Null,'Not Started');
        taskobj.subject='Email:';
        taskobj.description='Additional To:';
        taskObj.CallType='Inbound';
        insert taskObj;
        
        List<Case> lstCases = new List<Case>();
        
        Case caseWithActivity = Utils_TestMethods.createCase(objTestAccount.id,objTestContact.id,'In Progress');
        lstCases.add(caseWithActivity);
        
        
        Database.insert(lstCases);
        /*----------Test Starts---------------------*/
        Test.startTest();   
        
        Task objTaskForCase = Utils_TestMethods.createTask(caseWithActivity.id,objTestContact.id,'In Progress');
        Database.insert(objTaskForCase);

        Event objEventForCase = Utils_TestMethods.createEvent(caseWithActivity.id,objTestContact.id,'Planned');
        Database.insert(objEventForCase);  
       
        
        caseWithActivity.status='Closed';
        caseWithActivity.Spam__c=true;
        caseWithActivity.ClientReference__c = 'ClientRef';
        
        try{
            Database.SaveResult[] lstCaseSaveResult = Database.update(lstCases,false);
            for(Database.SaveResult CaseSaveResult:lstCaseSaveResult){
                if(!CaseSaveResult.isSuccess()){
                    Database.Error err = CaseSaveResult.getErrors()[0];              
                }
            }            
        }
        catch (DMLException e){
            System.assert(e.getMessage().contains(System.Label.CL00324));                        
        }
        
        Test.stopTest();
    }
    static testMethod void vipIconTestMethod(){ 
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
                
        Account testAccount1 = Utils_TestMethods.createAccount();
        testAccount1.Account_VIPFlag__c='VIP1';
        insert testAccount1;
        
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id,'test contact');
        testcontact.email='abc@ymail.com';
        insert testcontact;
        Contact testcontact22 = Utils_TestMethods.createContact(testAccount.Id,'test contact22');
        testcontact22.email='abc1@ymail.com';
        testcontact22.WorkPhone__c='12332111';
        insert testcontact22;
        
         //To Cover the AP_TaskHandler
        Task taskObj = Utils_TestMethods.createTask(testAccount.id,Null,'Not Started');
        insert taskObj;
        
        Case testCase = Utils_TestMethods.createCase(testAccount.Id,testcontact.Id,'Open');
        testCase.AssetOwner__c=testAccount1.id;
        testcase.SuppliedEmail='abc@ymail.com';
        testcase.Origin='web';
        insert testCase;
        testcase.ContactID=testcontact22.id;
        update testcase;
        
    }
    
    static testMethod void caseContractTestMethod(){    
        //Added by Shruti Karn for September Release
        Account testAccount = Utils_TestMethods.createAccount();
        testAccount.Account_VIPFlag__c='VIP1';
        insert testAccount;        

        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id,'test contact');
        insert testcontact;

        Contract testContract = Utils_TestMethods.createContract(testAccount.Id , null);
        insert testContract;

        CTR_ValueChainPlayers__c CVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        CVCP.Contact__c = testContact.ID;
        CVCP.ContactRole__c = System.Label.CLSEP12CCC23;
        insert CVCP;    
        /*----------Test Starts---------------------*/
        Test.startTest(); 

        Case testCase = Utils_TestMethods.createCase(testAccount.Id,testcontact.Id,'Open');
        testCase.CTRValueChainPlayer__c = CVCP.Id;
        testCase.Origin = 'Email';
        insert testCase;
        testCase.CTRValueChainPlayer__c = null;
        update testCase;

        PackageTemplate__c PKGTemplate = Utils_TestMethods.createPkgTemplate();
        PKGTemplate.FreeOfCost__c = true;
        insert PKGTemplate;

        ContractProductInformation__c product = new ContractProductInformation__c();
        product.ProductFamily__c = 'Test';
        product.packagetemplate__c = PKGTemplate.Id;
        insert product;

        Package__c Pkg = Utils_TestMethods.createPackage(testContract.Id , PKGTemplate.Id);
        Pkg.status__c = 'Draft';
        insert Pkg;

        testCase.ProductFamily__c = 'Test';
        testCase.CTRValueChainPlayer__c = CVCP.Id;
        testCase.DebitPoints__c = true;
        //update testCase;
        list<case> lstCase = new list<case>();
        lstCase.add(testCase);
        AP10_CaseTrigger.checkFreeProduct(lstCase);
        Test.stopTest();

/*-----------Test Ends--------------------------*/        
    }
    
    static testMethod void caseExternalReferenceTestMethod(){    
        //Added by Vimal K 
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;        

        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id,'test contact');
        insert testcontact;

        
        Test.startTest(); 
        Case testCase = Utils_TestMethods.createCase(testAccount.Id,testcontact.Id,'Open');
        testCase.ExternalReference__c = Label.CL00686;
        insert testCase;
        
        Test.stopTest();
        

/*-----------Test Ends--------------------------*/        
    }
    
    static testMethod void updateCaseStatus(){
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;  
        Contact conObj1 = Utils_TestMethods.createContact(testAccount.id, ' Contact 1' );
        conObj1.firstname = 'Test';
        conObj1.WorkPhone__c = '123456';
        conObj1.accountid = testAccount.id;
        insert conObj1;
        
        
        GCSSite__c gcssite = new GCSSite__c();
        gcssite.Account__c=testAccount.Id;
        gcssite.Name='TestSite'; 
        gcssite.Active__c=true;
        insert gcssite;
        
        Case cseObj = Utils_TestMethods.createCase(testAccount.Id, conObj1.Id, 'Open');
        cseObj.Site__c=gcssite.id;
        insert cseObj;
        
        
        Test.startTest(); 
       list<case> lstCase = new list<case>();
        lstCase.add(cseObj);
        AP10_CaseTrigger.legacySiteIdPopulation(lstCase);
       Test.stopTest();
    }
}