Public Class AP_CoveredProduct
    {

        public static void InstalledProdcutUnderContract(List<SVMXC__Service_Contract_Products__c> coverdprodList)
        {  
            System.debug('***************************** InstalledProdcutUnderContract ********************');
            Set<id> ipidSet = new Set<id>();
            Map<id,List<SVMXC__Service_Contract_Products__c>> ipidcpListmap = new Map<id,List<SVMXC__Service_Contract_Products__c>>();
            Set<id> ipidtoupdatedSet = new Set<id>();
            if(coverdprodList != null && coverdprodList.size()>0)
            {            
               for(SVMXC__Service_Contract_Products__c covObj :coverdprodList ){
                   
                   if(covObj.SVMXC__Installed_Product__c!= null)
                   {
                       ipidSet.add(covObj.SVMXC__Installed_Product__c);
                   }
               }
                
            }
            if(ipidSet != null && ipidSet.size()>0)
            {
               for(SVMXC__Service_Contract_Products__c cpObj  : [select id,SVMXC__Installed_Product__c from SVMXC__Service_Contract_Products__c where SVMXC__Installed_Product__c  in :ipidSet ]){
                   if(ipidcpListmap.containskey(cpObj.SVMXC__Installed_Product__c)){
                       ipidcpListmap.get(cpObj.SVMXC__Installed_Product__c).add(cpObj);
                   }
                   else{
                       ipidcpListmap.put(cpObj.SVMXC__Installed_Product__c, new List<SVMXC__Service_Contract_Products__c>());
                       ipidcpListmap.get(cpObj.SVMXC__Installed_Product__c).add(cpObj);
                   }
               }
            }
            if(coverdprodList != null && coverdprodList.size()>0)
            {            
               for(SVMXC__Service_Contract_Products__c covObj :coverdprodList ){
                   
                   if(ipidcpListmap.containskey(covObj.SVMXC__Installed_Product__c))
                   {
                       if(ipidcpListmap.get(covObj.SVMXC__Installed_Product__c).size() > 1)
                       {
                           
                       }
                       else{
                           // do nothing 
                            System.debug('\n CLog :'+covObj.SVMXC__Installed_Product__c);
                           if(ipidcpListmap.get(covObj.SVMXC__Installed_Product__c)[0].id == covObj.id)
                             ipidtoupdatedSet.add(covObj.SVMXC__Installed_Product__c);
                       }
                   }
               }
                
            }
            if(ipidtoupdatedSet != null && ipidtoupdatedSet.size()>0)
            {
                List<SVMXC__Installed_Product__c> ipObjList =[Select Id,UnderContract__c From SVMXC__Installed_Product__c Where Id in :ipidtoupdatedSet ];
                for(SVMXC__Installed_Product__c ipObj :ipObjList)
                {
                    ipObj.UnderContract__c = false;
                }
                System.debug('\n CLog :'+ipObjList);
                update ipObjList;
                System.debug('\n CLog :'+ipObjList);
            }
            
        }
        //added by suhail for jun2016 release start
public static void PmcoverageCreate(){
    SVMXC.INTF_WebServicesDef.INTF_TargetRecord request = new SVMXC.INTF_WebServicesDef.INTF_TargetRecord();
    SVMXC.INTF_WebServicesDef.INTF_TargetRecordObject headerRecord = new SVMXC.INTF_WebServicesDef.INTF_TargetRecordObject();
    SVMXC.INTF_WebServicesDef.INTF_Record record = new SVMXC.INTF_WebServicesDef.INTF_Record();
    List<SVMXC.INTF_WebServicesDef.INTF_Record> records = new List<SVMXC.INTF_WebServicesDef.INTF_Record>();
    List<SVMXC.INTF_WebServicesDef.INTF_StringMap> targetRecordAsKeyValue = new List<SVMXC.INTF_WebServicesDef.INTF_StringMap>();
    
    
    for(SVMXC__PM_Plan__c eachPM: [Select Id From SVMXC__PM_Plan__c]) {
    targetRecordAsKeyValue.add(new SVMXC.INTF_WebServicesDef.INTF_StringMap('Id', eachPM.Id));
    }
        record.targetRecordAsKeyValue = targetRecordAsKeyValue;
        records.add(record);
        headerRecord.records = records;
        request.headerRecord = headerRecord;
        //system.debug('Result: ' + SVMXC.PREV_SFMEventUtils.INTF_CalculatePMPlanDetails_SCON_WS2(request));
}
        
        //added by suhail for jun2016 release end
        
        
      Public Static void Slatermload(set<id> spset,list<SVMXC__Service_Contract_Products__c> cplist)
     {
        list<SVMXC__Service_Contract__c> sclist = new list<SVMXC__Service_Contract__c>();
         sclist=[select id , name,SVMXC__Service_Level__c from SVMXC__Service_Contract__c where id in:spset];
         for(SVMXC__Service_Contract__c sc: sclist){
            
            for(SVMXC__Service_Contract_Products__c  cp:cplist){
                
                cp.SVMXC__SLA_Terms__c = sc.SVMXC__Service_Level__c;

            }
        }
    }

        
        
        /*
        Added By Deepak
        For April 2015
        To populate the value of Installed Product Account fields
        */
        public static void PopulatingAccountFields(List<SVMXC__Service_Contract_Products__c> coverdprodList)
        {
            List<SVMXC__Installed_Product__c> iplist = New list<SVMXC__Installed_Product__c>();
            List<Account> acclist = New List<Account>();
            set<Id> Ipid = New set<Id>();
            Set<Id> AccId = New Set<Id>();
            if(coverdprodList.Size()>0){
                for(SVMXC__Service_Contract_Products__c cp :coverdprodList){
                    if(cp.SVMXC__Installed_Product__c !=null){
                        Ipid.add(cp.SVMXC__Installed_Product__c);
                    }
                }
            }
            if(Ipid !=null){
                iplist=[Select Id,SVMXC__Company__c From SVMXC__Installed_Product__c Where Id in :Ipid];
            }
            if(iplist.size()>0){
                for(SVMXC__Installed_Product__c ip :iplist){
                    if(ip.SVMXC__Company__c!=null){
                        AccId.add(ip.SVMXC__Company__c);
                    }
                }
            }
            if(AccId !=null){
                acclist =[Select Id,SDHGoldenVersion__c,Name,AccountLocalName__c,Street__c,StreetLocalLang__c,
                AdditionalAddress__c,LocalAdditionalAddress__c,ZipCode__c,Tech_CountryCode__c,City__c,
                StateProvince__r.CountryCode__c From Account Where Id in :AccId];
            }
            if(acclist.size()>0){
                for(SVMXC__Service_Contract_Products__c cp :coverdprodList){
                    for(Account acc :acclist){
                        cp.installedAtAddAddressInfo__c = acc.AdditionalAddress__c;
                        cp.installedAtAddAddressInfoLocal__c=acc.LocalAdditionalAddress__c;
                        cp.installedAtCity__c=acc.City__c;
                        cp.installedAtCountryCode__c=acc.Tech_CountryCode__c;
                        cp.InstalledAtGoldenID__c=acc.SDHGoldenVersion__c;
                        //cp.installedAtLocalCity__c=acc.
                        cp.InstalledAtName__c=acc.Name;
                        cp.installedAtNameLocal__c=acc.AccountLocalName__c;
                        cp.installedAtStateProvinceCode__c=acc.StateProvince__r.CountryCode__c;
                        cp.installedAtStreet__c=acc.Street__c;
                        cp.installedAtStreetLocal__c=acc.StreetLocalLang__c;
                        cp.installedAtZipCode__c=acc.ZipCode__c;
                    }
                } 
            }
        }
    }