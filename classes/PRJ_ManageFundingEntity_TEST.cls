/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 03/15/2012
    Description     : Test class for the class PRJ_ManageFundingEntity
*/


@isTest
private class PRJ_ManageFundingEntity_TEST
{
    static testMethod void myUnitTest()
    {
        //User runAsUser = Utils_TestMethods.createStandardDMTUser('tst2');
        
        //insert runAsUser;
        //System.runAs(runAsUser)
        //{
            DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
            insert testDMTA1; 
            
            PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
            testProj.ParentFamily__c = 'Business';
            testProj.Parent__c = 'Buildings';
            testProj.BusGeographicZoneIPO__c = 'Global';
            testProj.BusGeographicalIPOOrganization__c = 'CIS';
            testProj.GeographicZoneSE__c = 'Global';
            testProj.Tiering__c = 'Tier 1';
            testProj.ProjectRequestDate__c = system.today();
            testProj.Description__c = 'Test';
            testProj.GlobalProcess__c = 'Customer Care';
            testProj.ProjectClassification__c = 'Infrastructure';
            testProj.BusinessTechnicalDomain__c = 'Supply Chain';
            insert testProj;
            
            DMTFundingEntity__c testFE = Utils_TestMethods_DMT.createDMTFundingEntity(testProj,'2012');
            insert testFE;
            
            PRJ_ManageFundingEntity pageFE = new PRJ_ManageFundingEntity(new ApexPages.StandardController(testFE));
            pageFE.updateFundingEntity();
            //pageFE.backToFundingEntity();                                                            
        //}
        //Ramakrishna Added Code For June Release Start
         
        PRJ_ProjectReq__c  ProjReq = new PRJ_ProjectReq__c();
        ProjReq.ParentFamily__c = 'Business';
         testProj.BusGeographicZoneIPO__c = 'Global';
        ProjReq.Parent__c = 'Buildings';
        ProjReq.GeographicZoneSE__c = 'EMEAS';
        ProjReq.GeographicalSEOrganization__c = 'All Europe';
        insert ProjReq;
        DMTFundingEntityCostItemsFields__c crfe = new DMTFundingEntityCostItemsFields__c();
        crfe.Name ='Testfe 1';
        crfe.CostItemFieldAPIName__c = 'CAPEXY0__c';
        crfe.Type__c = 'IT FUNDING CAPEX';
        crfe.TypeOrder__c = 'Type 1';
        insert crfe;    
        system.debug('@@@@@@@@@@@@@@'+  ProjReq);
       
        
        Budget__c oBudget = new Budget__c();
        oBudget.CurrencyIsoCode = 'EUR';
        oBudget.ProjectReq__c = ProjReq.id;
        oBudget.TECHTotalPlannedITCosts__c = 10;
        //oBudget.TotalPlannedCost__c = 100;
        oBudget.Active__c = true;
        insert oBudget;
        system.debug('@@@@@@@@@@@@@@'+  oBudget);
        test.startTest();
        PRJ_BudgetLine__c bgLines = new PRJ_BudgetLine__c();
        bgLines.Budget__c = oBudget.id;
        bgLines.BudgetYear__c = System.today();
        bgLines.Type__c = 'IT Cashout CAPEX';
        bgLines.Year__c = '2012';
        bgLines.Amount__c = 100;
        insert bgLines;
        
         DMTFundingEntity__c ferec = new DMTFundingEntity__c();
        ferec.ParentFamily__c = 'Business';
        ferec.Parent__c = 'Buildings';
        ferec.GeographicZoneSE__c = 'EMEAS';
        ferec.GeographicalSEOrganization__c = 'All Europe';
        ferec.CurrencyISOCode = 'EUR';
        ferec.ProjectReq__c = ProjReq.id;
        ferec.ActiveForecast__c = true;
        ferec.BudgetYear__c = '2012';
        ferec.ForecastQuarter__c = 'Q1';
        ferec.ForecastYear__c = '2012';
        insert ferec;
        PRJ_ManageFundingEntity pageFE2 = new PRJ_ManageFundingEntity(new ApexPages.StandardController(ferec));
        
            pageFE2.updateFundingEntity();
         //Priyanka added/commented Code For May2013 Release Start        
           // pageFE2.backToFundingEntity(); 
              pageFE2.UpdateSelectionInBudgetEnvelop();
              pageFE2.back(); 
       //Priyanka  Added Code For May2013 Release End
        //Ramakrishna Added Code For June Release End
        test.stopTest();
                            
    }
}