/********************************************************************
* Company: Fielo
* Created Date: 27/03/2016
* Description:
********************************************************************/
@isTest
public with sharing class FieloPRM_RewardsPSetImplementsTest{
  
    public static testMethod void testUnit(){
             
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        User u = new User();
                
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c prmProgram = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
            
            prmProgram.FieloEE__SiteProfile__c = 'SE - Channel Partner (Community)';
            
            update prmProgram;

            FieloEE__Member__c mem = FieloEE.MockUpFactory.createMember('Test member', '1234', 'DNI');
            
            mem.FieloEE__Program__c = prmProgram.Id;
            
            update mem;
            
            Contact con = [SELECT Id, PRMUIMSId__c FROM Contact WHERE FieloEE__Member__c =: mem.Id];
            
            con.PRMUIMSId__c = '111';
            
            update con;
        
            Set<String> setIds = new Set<String>();
            
            setIds.add('111');
        
            FieloPRM_RewardsPSetImplements rewardsPSetImplements = new FieloPRM_RewardsPSetImplements();
            
            rewardsPSetImplements.promote(setIds);
            rewardsPSetImplements.demote(setIds);
        
        }
    }
}