/*
    Author          : Bhuvana Subramaniyan
    Description     : Test class for VFC_RiskAssessmentNavigations.
*/


@isTest
private class VFC_RiskAssessmentNavigations_TEST 
{
    static testMethod void testRiskAssessment()
    {
        CFWRiskAssessment__c cfwr = new CFWRiskAssessment__c();
        cfwr.ProjectName__c = 'testProjectName';
        cfwr.SubmittedforApproval__c = true;
        insert cfwr;
        
        test.startTest();
        PageReference pageRef = Page.VFP_CFWDetailPage;
        Test.setCurrentPage(pageRef);
        List<RecordType> recordTypes = [select Id From RecordType where sobjecttype = 'CFWRiskAssessment__c'];
        ApexPages.StandardController sc1 = new ApexPages.StandardController(cfwr);
        VFC_RiskAssessmentNavigations rskAssment = new VFC_RiskAssessmentNavigations(sc1);
        if(recordTypes[0]!=null)
        rskAssment.urlValue = recordTypes[0].Id;
        rskAssment.urlPageRedirect();       
        
        rskAssment.urlValue = 'VFP_CertificationChecklist';
        rskAssment.urlPageRedirect();  
        
        rskAssment.urlValue = 'VFP_CFWGuidelinesPolicies';
        rskAssment.urlPageRedirect();  
        
        rskAssment.urlValue = 'VFP_RiskAssessment';
        rskAssment.urlPageRedirect();       
        
        CertificationChecklist__c cc = new CertificationChecklist__c();
        cc.CertificationRiskAssessment__c = cfwr.Id;
        insert cc;
        
        rskAssment.urlValue = 'VFP_CertificationChecklist';
        rskAssment.urlPageRedirect(); 
        
        CertificationGuidelines__c certGuideline= new CertificationGuidelines__c();
        certGuideline.CertificationRiskAssessment__c = cfwr.Id;
        insert certGuideline;
       
        rskAssment.urlValue = 'VFP_CFWGuidelinesPolicies';
        rskAssment.urlPageRedirect();  
        if(recordTypes[1]!=null)
        {
            cfwr.recordTypeId = recordTypes[1].Id;
            update cfwr;
            sc1 = new ApexPages.StandardController(cfwr);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1);
        }
        if(recordTypes[2]!=null)
        {
            cfwr.recordTypeId = recordTypes[2].Id;
            update cfwr;
            sc1 = new ApexPages.StandardController(cfwr);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1);
        }
        if(recordTypes[3]!=null)
        {
            cfwr.recordTypeId = recordTypes[3].Id;
            update cfwr;
            sc1 = new ApexPages.StandardController(cfwr);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1);
        }
        if(recordTypes[4]!=null)
        {
            cfwr.recordTypeId = recordTypes[4].Id;
            update cfwr;
            sc1 = new ApexPages.StandardController(cfwr);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1);
        }
        if(recordTypes[5]!=null)
        {
            cfwr.recordTypeId = recordTypes[5].Id;
            update cfwr;
            sc1 = new ApexPages.StandardController(cfwr);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1); 
        }
        if(recordTypes[0]!=null)
        {
            ApexPages.currentPage().getParameters().put('RecordType',recordTypes[0].Id);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1); 
        }
        if(recordTypes[1]!=null)
        {
            ApexPages.currentPage().getParameters().put('RecordType',recordTypes[1].Id);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1); 
        }
        if(recordTypes[2]!=null)
        {
            ApexPages.currentPage().getParameters().put('RecordType',recordTypes[2].Id);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1); 
        }
        if(recordTypes[3]!=null)
        {
            ApexPages.currentPage().getParameters().put('RecordType',recordTypes[3].Id);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1); 
        }
        if(recordTypes[4]!=null)
        {
            ApexPages.currentPage().getParameters().put('RecordType',recordTypes[4].Id);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1); 
        }
        if(recordTypes[5]!=null)
        {
            ApexPages.currentPage().getParameters().put('RecordType',recordTypes[5].Id);
            rskAssment = new VFC_RiskAssessmentNavigations(sc1); 
        }
        
        test.stopTest();
    }
}