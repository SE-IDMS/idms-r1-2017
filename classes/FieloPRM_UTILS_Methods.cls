/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 24/02/2015
* Description: Utilities methods for main the PRM Project
********************************************************************/

public with sharing class FieloPRM_UTILS_Methods {

    public FieloPRM_UTILS_Methods(){}
    
    /**
     * [generateMapByField generates a map with a key as a determined field of the object. It doesn't check for repeated values]
     * @method          generateMapByField
     * @Pre-conditions  the field was queried
     * @Post-conditions 
     * @param           {[type]}           String        fieldKey      [The API NAME of the field of the object which we want to use as the map key]
     * @param           {[type]}           List<sObject> records       [the records to put on the map]
     * @return          {[type]}                         [a map with a key as the field mentioned in the parameter and value as the record]
     */
    public static map<String,sobject> generateMapByField(String fieldKey,List<sObject> records){
        Map<String,sObject> toReturnMap = new Map<String,sObject>();

        for(sObject record : records){
            toReturnMap.put(String.valueOf(record.get(fieldKey)),record);
        }
        return toReturnMap;
    }

    /**
     * [addError add a page message error]
     * @method          addError
     * @param           {[type]}   String message [message to show on page]
     */
    public static void addError(String message){
        FieloPRM_UTILS_Methods.debugError('VF PAGE MESSAGE(error) : ' + message);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, message));
    }

    /**
     * [addWarn add a page message warning]
     * @method          addWarn
     * @param           {[type]}   String message [message to show on page]
     */
    public static void addWarn(String message){     
        FieloPRM_UTILS_Methods.debugError('VF PAGE MESSAGE(warn) : ' + message);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING,  message));
    }

    /**
     * [addInfo add a page message info]
     * @method          addInfo
     * @param           {[type]}   String message [message to show on page]
     */
    public static void addInfo(String message){
        FieloPRM_UTILS_Methods.debugError('VF PAGE MESSAGE(info) : ' + message);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO,  message));
    }

    /**
     * [debug add a system.debug with a standard format]
     * @method          addInfo
     * @param           {[type]}   String message [message to show ]
     */
    public static void debug(String message){
        System.debug('####INFO ' + message);
    }

    /**
     * [debug add a system.debug with level error with a standard format]
     * @method          addInfo
     * @param           {[type]}   String message [message to show ]
     */
    public static void debugError(String message){
        System.debug(loggingLevel.Error, '########ERROR ' + message);
    }

        
}