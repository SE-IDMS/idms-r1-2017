/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 09/11/2010
    Description     : Test class for batch apex BATCH01_OpptyCockpit
*/
@isTest
private class BATCH01_OpptyCockpit_TEST {

    static testMethod void myUnitTest() {
        //Create 3 x Users to be Account Owners
        List<User> users = new List<User>();
        User testUser1 = Utils_TestMethods.createStandardUser('tst1');
        User testUser2 = Utils_TestMethods.createStandardUser('tst2');
        User testUser3 = Utils_TestMethods.createStandardUser('tst3');
        User testUser4 = Utils_TestMethods.createStandardUser('tst4');
        users.add(testUser1);
        users.add(testUser2);
        users.add(testUser3);
        users.add(testUser4);
        insert users;
        
        List<Account> accounts = new List<Account>();     
        Account a1 = Utils_TestMethods.createAccount(users[0].Id);
        Account a2 = Utils_TestMethods.createAccount(users[1].Id);
        Account a3 = Utils_TestMethods.createAccount(users[2].Id);
        Account a4 = Utils_TestMethods.createAccount(users[3].Id);
        accounts.add(a1);
        accounts.add(a2);
        accounts.add(a3);
        accounts.add(a4);
        insert accounts;
        
        List<Opportunity> opps = new List<Opportunity>();
        
        //Create 4 x Opportunities in TREE 1
          Opportunity o1 = Utils_TestMethods.createOpportunity(accounts[0].Id);      // Top of tree
          o1.StageName = '5 - Prepare & Bid';
      Opportunity o2 = Utils_TestMethods.createOpportunity(accounts[0].Id);      // Child
      o2.StageName = '5 - Prepare & Bid';
      Opportunity o3 = Utils_TestMethods.createOpportunity(accounts[0].Id);      // Child
      Opportunity o4 = Utils_TestMethods.createOpportunity(accounts[1].Id);      // Grandchild
      Opportunity o5 = Utils_TestMethods.createOpportunity(accounts[1].Id);      // Grandchild
    
        //Create 2 x Opportunities in TREE 2
          Opportunity o6 = Utils_TestMethods.createOpportunity(accounts[0].Id);      //Top of tree
      Opportunity o7 = Utils_TestMethods.createOpportunity(accounts[1].Id);      //Child
      o7.StageName = '5 - Prepare & Bid';
    
    //Create 2 x Opportunity in TREE 3
      Opportunity o8 = Utils_TestMethods.createOpportunity(accounts[2].Id);      //Top of tree
      Opportunity o9 = Utils_TestMethods.createOpportunity(accounts[2].Id);      //Child
    
    //Create Opportunity in TREE 4
      Opportunity o10 = Utils_TestMethods.createOpportunity(accounts[3].Id);      //Top of tree
      
    opps.add(o1);
    opps.add(o2);
    opps.add(o3);
    opps.add(o4);
    opps.add(o5);
    opps.add(o6);
    opps.add(o7);
    opps.add(o8);
    opps.add(o9);
    opps.add(o10);
    insert opps;

    //Set the relationships - i.e. top of tree, child, grandchild
    opps[1].ParentOpportunity__c = opps[0].Id;                      //TREE 1
    opps[2].ParentOpportunity__c = opps[0].Id;                      //TREE 1
    opps[3].ParentOpportunity__c = opps[1].Id;                      //TREE 1
    opps[4].ParentOpportunity__c = opps[1].Id;                      //TREE 1
    opps[6].ParentOpportunity__c = opps[5].Id;                      //TREE 2
    opps[8].ParentOpportunity__c = opps[7].Id;                      //TREE 3
    update opps;
    
    //Simulate that an update has occured to a tracked field - FEED
    List<FeedItem> fieldHistories = new List<FeedItem>();
    FeedItem ofh1 = new FeedItem( ParentId = opps[0].Id, LinkUrl = 'http://www.google.com');      //Related to TREE 1
    FeedItem ofh2 = new FeedItem( ParentId = opps[3].Id, LinkUrl = 'http://www.google.com');      //Related to TREE 1
    FeedItem ofh3 = new FeedItem( ParentId = opps[6].Id, LinkUrl = 'http://www.google.com');      //Related to TREE 2
    FeedItem ofh4 = new FeedItem( ParentId = opps[9].Id, LinkUrl = 'http://www.google.com');      //Related to TREE 4
    fieldHistories.add(ofh1);
    fieldHistories.add(ofh2);
    fieldHistories.add(ofh3);
    fieldHistories.add(ofh4);
    insert fieldHistories;
    
    //Construct query for batch
    /*String query = 'Select o.OpportunityId, o.Opportunity.ParentOpportunity__c, o.Opportunity.ParentOpportunity__r.ParentOpportunity__c, o.Opportunity.ParentOpportunity__r.ParentOpportunity__r.ParentOpportunity__c From OpportunityFieldHistory o Where ';*/
    String query = 'Select o.ParentId, o.Parent.ParentOpportunity__c, o.Parent.ParentOpportunity__r.ParentOpportunity__c, o.Parent.ParentOpportunity__r.ParentOpportunity__r.ParentOpportunity__c From OpportunityFeed o Where ';
    for(Integer i = 0; i < opps.size(); i++){
      if(i == opps.size()- 1)
        query += 'o.ParentId = \''+ opps[i].Id +'\'';
      else
        query += 'o.ParentId = \''+ opps[i].Id +'\' or ';
    }

    Test.startTest();  
    BATCH01_OpptyCockpit b = new BATCH01_OpptyCockpit(query);
    database.executebatch(b, 50);
    Test.stopTest();

      //Coverage only    
      BATCH01_OpptyCockpit b1 = new BATCH01_OpptyCockpit();
    }
}