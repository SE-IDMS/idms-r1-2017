/*
Author          : Rakhi Kumar 
Date Created    : 13/03/2013
Description     : Controller class utilised by VFP_MyCallsTodayRedirect.
                  CTI Integration module for May 2013 Release
-----------------------------------------------------------------
                          MODIFICATION HISTORY
-----------------------------------------------------------------
Modified By     : <>
Modified Date   : <>
Description     : <>
-----------------------------------------------------------------
*/
public class VFC_MyCallsTodayRedirect {
    //This method will redirect user to the My Calls Today report
    public PageReference myCallsTodayRedirect(){  
        //Redirect to My Calls Today report
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        PageReference referenceURL = new PageReference(baseURL + '/' + System.Label.CLMAY13CCC22);//Define the My Calls Today Report Id in Custom Label
        referenceURL .setRedirect(true);
        return referenceURL ; 
    }
}