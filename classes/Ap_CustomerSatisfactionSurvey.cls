public class Ap_CustomerSatisfactionSurvey extends TriggerBase{

     private map<Id,CustomerSatisfactionSurvey__c> newMap;
    private map<Id, CustomerSatisfactionSurvey__c> oldMap;
    private List<CustomerSatisfactionSurvey__c> newRecList;
    Set<String> ExistGoldenIdSet = new Set<String>();
        
     public Ap_CustomerSatisfactionSurvey(boolean isBefore, boolean isAfter, boolean isUpdate, boolean isInsert, boolean isDelete,
        List<CustomerSatisfactionSurvey__c> newList,
        map<Id, CustomerSatisfactionSurvey__c> oldMap, map<Id, CustomerSatisfactionSurvey__c> newMap) {
        super(isBefore, isAfter, isUpdate, isInsert, isDelete);

        this.newMap = newMap;
        this.oldMap = oldMap;
        this.newRecList = newList;
        
    }
    public override void onBeforeInsert() {
        
        Set<String> GoldenIdSet = new Set<String>();        
        
        for(CustomerSatisfactionSurvey__c ipObj :newRecList){
            
            if(ipObj.GoldenCustomerSatisfactionSurveyId__c != null)
            GoldenIdSet.add(ipObj.GoldenCustomerSatisfactionSurveyId__c);         
        }
        if(GoldenIdSet != null && GoldenIdSet.size()>0)
        {
            for(CustomerSatisfactionSurvey__c obj:[Select id,GoldenCustomerSatisfactionSurveyId__c from CustomerSatisfactionSurvey__c where GoldenCustomerSatisfactionSurveyId__c in :GoldenIdSet]){
                ExistGoldenIdSet.add(obj.GoldenCustomerSatisfactionSurveyId__c);
            }
        }
        sdhGoldenUniqeCheck();
        
        
        
    }
    public  void sdhGoldenUniqeCheck() {
        for(CustomerSatisfactionSurvey__c ipObj :newRecList){
            
            if(ipObj.GoldenCustomerSatisfactionSurveyId__c!= null)
            {
                if(ExistGoldenIdSet.contains(ipObj.GoldenCustomerSatisfactionSurveyId__c))
                ipObj.addError('Golden Customer Satisfation Survey already Exist');
                
            }
        }
        
    }
}