@isTest
public class VFC_AddDefaultOpportunityCompetitor_TEST{
    public static testMethod void runPositiveTestCases() {
        Competitor__c c1=new Competitor__c();
        c1.name='xyz';
        c1.Active__c=true;
        insert c1;
        
        Competitor__c c2=new Competitor__c();
        c2.name='abc';
        c2.Active__c=true;
        insert c2;
        
        UserParameter__c up2=new UserParameter__c();
        up2.User__c=userinfo.getUserId();
        up2.Competitor__c=c1.id;
        up2.Competitor2__c=c2.id;
        up2.Competitor3__c=c1.id;
        up2.Competitor4__c=c2.id;
        up2.Competitor5__c=c1.id;
        up2.Competitor6__c=c2.id;
        up2.Competitor7__c=c1.id;
        up2.Competitor8__c=c2.id;
        up2.Competitor9__c=c1.id;
        up2.Competitor10__c=c2.id;
        insert up2;
        
        Account a2 = Utils_TestMethods.createAccount();
        INSERT a2;
        Opportunity Opp = Utils_TestMethods.createOpportunity(a2.Id);
        INSERT Opp; 
        
        OPP_OpportunityCompetitor__c OppComp = Utils_TestMethods.createOppCompetitor(C1.ID, Opp.id);
        
        Pagereference pg = new pagereference('/apex/VFP_AddDefaultOpportunityCompetitor?oppID='+Opp.id); 
        Test.setCurrentPageReference(pg);
        ApexPages.currentPage().getParameters().put('sourcePage', 'OpptyLost');  
        VFC_AddDefaultOpportunityCompetitor  adoc = new VFC_AddDefaultOpportunityCompetitor(new ApexPages.StandardController(OppComp));  
        VFC_AddDefaultOpportunityCompetitor.wrapOpportunityCompetitor wrapcomp= new VFC_AddDefaultOpportunityCompetitor.wrapOpportunityCompetitor(OppComp);
        wrapcomp.selected=TRUE;
        adoc.defaultOpptyCompetitorList.add(wrapcomp);
        adoc.opptyStatus='status';
        adoc.opptyReason='reason';
        adoc.opptyAmount='100';
        adoc.doConfirm();
        adoc.addDefaultCompetitor();
        adoc.Cancel(); 
        Test.StartTest();
            ApexPages.currentPage().getParameters().put('sourcePage', 'blah');  
            adoc.addDefaultCompetitor();
            VFC_AddDefaultOpportunityCompetitor  adoc2 = new VFC_AddDefaultOpportunityCompetitor(new ApexPages.StandardController(OppComp));  
            adoc2.addDefaultCompetitor();
        Test.StopTest();
    }
}