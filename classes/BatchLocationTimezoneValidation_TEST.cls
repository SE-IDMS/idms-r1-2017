@isTest
public with sharing class BatchLocationTimezoneValidation_TEST{

    static testMethod void BatchLocationTimezoneValidation_TEST()
    { 
      List<ID> siteIds = new List<ID>();
      CS_CountryTimeZone__c countryTimeZone = new CS_CountryTimeZone__c(Name = 'THA', TimeZoneCode__c= 'Asia/Bangkok');
      insert countryTimeZone ;

      Country__c c1 = Utils_TestMethods.createCountry(); 
      c1.countrycode__c ='FR';
      insert c1;
      
      Country__c c2 = Utils_TestMethods.createCountry(); 
      c2.countrycode__c ='THA';
      insert c2;
      
      Account acc1 = Utils_TestMethods.createAccount();
      acc1 .Street__c = '13 rue du colonel moutarde';
      acc1 .City__c = 'Paris';
      acc1 .ZipCode__c = '75012';
      acc1 .Country__c = c1.Id;
      acc1 .GeoLocation__Latitude__s = 48.816957;
      acc1 .GeoLocation__Longitude__s = 2.364460;
      insert acc1 ;
      
      Account acc2 = Utils_TestMethods.createAccount();
      acc2 .Street__c = '13 rue du colonel moutarde';
      acc2 .City__c = 'Bangkok';
      //acc.StateProvince__c;
      acc2 .ZipCode__c = 'XXXX';
      acc2 .Country__c = c2.Id;
      insert acc2 ;
      
      
      SVMXC__Site__c site1 = new SVMXC__Site__c(
         Name = 'Location Test1 Timezone - Site',
         PrimaryLocation__c = true,
         SVMXC__Account__c = acc1.Id
         );
         
      SVMXC__Site__c site2 = new SVMXC__Site__c(
         Name = 'Location Test2 Timezone - Site',
         PrimaryLocation__c = true,
         SVMXC__Account__c = acc2.Id
         );
         
      insert site1;
      siteIds.add(site1.id); 
      System.debug('## Location inserted: '+site1 );
      
      insert site2;
      siteIds.add(site2.id);
      System.debug('## Location inserted: '+site2 );
      
      Test.startTest();
      BatchLocationTimezoneValidation batch = new BatchLocationTimezoneValidation();
      batch.query = 'SELECT Id, Name, SVMXC__Longitude__c, SVMXC__Latitude__c, TimeZone__c, SVMXC__Account__r.id, LocationCountry__r.CountryCode__c FROM SVMXC__Site__c';
      Database.executeBatch(batch);
      Test.stopTest();
      
      site1 = [SELECT TimeZone__c FROM SVMXC__Site__c WHERE Id = :site1.Id LIMIT 1];
      System.debug('## Timezone updated: '+site1 );
      //System.assertEquals(site1.TimeZone__c, 'Asia/Bangkok');
      
      site2 = [SELECT TimeZone__c FROM SVMXC__Site__c WHERE Id = :site2.Id LIMIT 1];
      System.debug('## Timezone updated: '+site2 );
      System.assertEquals(site2.TimeZone__c, 'Asia/Bangkok');
        
        string CORN_EXP = '0 0 0 1 4 ?';
        
        BatchLocationTimezoneValidation calculateLocationTimezone = new BatchLocationTimezoneValidation ();
        string jobid = system.schedule('my batch job', CORN_EXP, new BatchLocationTimezoneValidation());
        CronTrigger ct = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
        //Database.executeBatch(calculateLocationTimezone, batchSize );
    }
}