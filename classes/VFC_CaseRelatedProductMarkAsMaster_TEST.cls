@isTest
private class VFC_CaseRelatedProductMarkAsMaster_TEST{
    static testMethod void CaseRelatedProductMarkAsMasterFamily_TestMethod() {
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
               
        List<OPP_Product__c> lstProduct = new List<OPP_Product__c>();
        
        OPP_Product__c  objProduct1 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU','TEST_PL','TEST_PF','TEST_FLMY');
        objProduct1.CCCRelevant__c=true;
        OPP_Product__c objProduct2 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU2','TEST_PL2','TEST_PF2','TEST_FLMY2');
        objProduct2.CCCRelevant__c=true;
        lstProduct.add(objProduct1);
        lstProduct.add(objProduct2);
        
        Database.insert(lstProduct);
        
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        objOpenCase.Family_lk__c = objProduct1.Id;
        Database.insert(objOpenCase);
        
        Test.startTest();
        
        objOpenCase.Family_lk__c = objProduct2.Id;
        Database.update(objOpenCase);
        List<CSE_RelatedProduct__c> lstCaseRelatedProduct = new List<CSE_RelatedProduct__c>([Select Id,MasterProduct__c From CSE_RelatedProduct__c where Case__c =: objOpenCase.Id AND MasterProduct__c != true ]);
        if(lstCaseRelatedProduct.size()>0){
            ApexPages.StandardController CaseRelatedProductStandardController = new ApexPages.StandardController(lstCaseRelatedProduct[0]);   
            VFC_CaseRelatedProductMarkAsMaster MarkAsMasterPage = new VFC_CaseRelatedProductMarkAsMaster(CaseRelatedProductStandardController);
        
            PageReference pageRef = Page.VFP_CaseRelatedProductMarkAsMaster;
            Test.setCurrentPage(pageRef);
            MarkAsMasterPage.performAction();
            MarkAsMasterPage.returnToCase();
        }
        Test.stopTest();
    }
    static testMethod void CaseRelatedProductMarkAsMasterCR_TestMethod() {
        Account objAccount = Utils_TestMethods.createAccount();
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        Database.insert(objContact);
               
        List<OPP_Product__c> lstProduct = new List<OPP_Product__c>();
        
        OPP_Product__c  objProduct1 = Utils_TestMethods.createProductHierarchyAtFamily('TEST BU','TEST_PL','TEST_PF','TEST_FLMY');
        Database.insert(objProduct1);
        
        Product2 objCommercialReference = Utils_TestMethods.createCommercialReference('VFCPROD','MaterialDescription For VFCPROD','BU-VFCPROD', 'PL-VFCPROD', 'PF-VFCPROD', 'F-VFCPROD',false, false, false);
        Database.insert(objCommercialReference);
        
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        objOpenCase.CommercialReference_lk__c = objCommercialReference.Id;
        Database.insert(objOpenCase);
        
        Test.startTest();
        objOpenCase.CommercialReference_lk__c = null;
        objOpenCase.Family_lk__c = objProduct1.Id;
        Database.update(objOpenCase);
        List<CSE_RelatedProduct__c> lstCaseRelatedProduct = new List<CSE_RelatedProduct__c>([Select Id,MasterProduct__c From CSE_RelatedProduct__c where Case__c =: objOpenCase.Id AND MasterProduct__c != true ]);
        if(lstCaseRelatedProduct.size()>0){
            ApexPages.StandardController CaseRelatedProductStandardController = new ApexPages.StandardController(lstCaseRelatedProduct[0]);   
            VFC_CaseRelatedProductMarkAsMaster MarkAsMasterPage = new VFC_CaseRelatedProductMarkAsMaster(CaseRelatedProductStandardController);
        
            PageReference pageRef = Page.VFP_CaseRelatedProductMarkAsMaster;
            Test.setCurrentPage(pageRef);
            MarkAsMasterPage.performAction();
            MarkAsMasterPage.returnToCase();
        }
        Test.stopTest();
    }
}