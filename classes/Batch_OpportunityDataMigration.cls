global class Batch_OpportunityDataMigration implements 
    Database.Batchable<sObject>{

     global final Map<String,OPP_Product__c> productMap=new Map<String,OPP_Product__c>();
     global final Map<String,String> revenuelinetoproductlinefieldsmap=new Map<String,String>();   
     global final Set<String> openStages=new Set<String>{'1 - Understand Business Context','2 - Define Opportunity Portfolio','3 - Identify & Qualify','4 - Influence & Develop','5 - Prepare & Bid','6 - Negotiate & Win'};
     global final String wonStage='7 - Deliver & Validate';
     global final String lostStage='0 - Closed';
     global Integer limitnumber;
     global final Map<String,String> concatenatedProductMap=new Map<String,String>();

   global List<String> tobeProcessedStageNames=new List<String>();

   global Batch_OpportunityDataMigration(){}
   global Batch_OpportunityDataMigration(List<String> stagenameslst,Integer lmtnumber)
   {
    tobeProcessedStageNames=stagenameslst;
    if(lmtnumber==0 || lmtnumber==null)
        limitnumber=2000000;
    limitnumber=lmtnumber;
    prepareFinalMaps();    
   }


  private void prepareFinalMaps()
  {
    prepareProductMap();    
    prepareRevenueLinetoProductMap();    
    prepareConcatenatedMap();
  }

  private void prepareProductMap()
  {
    List<OPP_Product__c> lstProducts=[SELECT BusinessUnit__c,Id,Name FROM OPP_Product__c WHERE TECH_PM0CodeInGMR__c LIKE '____' AND IsActive__c = true];
   for(OPP_Product__c product: lstProducts)
      {
          if(product.Name.left(5)=='ITSL2'){
              productmap.put(product.Name.left(5),product);
              productmap.put(product.Name.left(5)+'-2',product);    
          }
          else
              productmap.put(product.Name.left(5),product);
          productmap.put(product.Name,product);
      }
  } 

  private void prepareConcatenatedMap()
  {
    MAP<String,DataMigration_ProductTable__c> tempMap=DataMigration_ProductTable__c.getAll();
    for(String productId:tempMap.keySet())
    concatenatedProductMap.put(tempMap.get(productId).ConcatenatedText__c.toLowerCase(),productId);
  }  

  private void prepareRevenueLinetoProductMap()
  {
    revenuelinetoproductlinefieldsmap.put('TECH_CommercialReference__c','TECH_CommercialReference__c');
    revenuelinetoproductlinefieldsmap.put('Commission__c','Commission__c');
    revenuelinetoproductlinefieldsmap.put('Configuration_Id__c','ConfigurationID__c');
    revenuelinetoproductlinefieldsmap.put('ExpectedShippeddate__c','DeliveryDate__c');
    revenuelinetoproductlinefieldsmap.put('Family__c','Family__c');
    revenuelinetoproductlinefieldsmap.put('IncludedInForecast__c','IncludedInForecast__c');
    revenuelinetoproductlinefieldsmap.put('Include_in_Quote__c','IncludeinQuote__c');
    revenuelinetoproductlinefieldsmap.put('Legacy_Line_Id__c','LegacyLineId__c');
    revenuelinetoproductlinefieldsmap.put('ProductDescription__c','ProductDescription__c');
    revenuelinetoproductlinefieldsmap.put('Opportunity__c','Opportunity__c');
    revenuelinetoproductlinefieldsmap.put('OverlayRevenue__c','OverlayRevenueITB__c');
    revenuelinetoproductlinefieldsmap.put('Probability__c','Probability__c');
    revenuelinetoproductlinefieldsmap.put('ProductBU__c','ProductBU__c');
    revenuelinetoproductlinefieldsmap.put('ProductFamily__c','ProductFamily__c');
    revenuelinetoproductlinefieldsmap.put('ProductLine__c','ProductLine__c');
    revenuelinetoproductlinefieldsmap.put('PurchaseOrderNumber__c','PurchaseOrderNumber__c');
    revenuelinetoproductlinefieldsmap.put('Quantity__c','Quantity__c');
    revenuelinetoproductlinefieldsmap.put('RevenueOverride__c','RevenueOverride__c');
    revenuelinetoproductlinefieldsmap.put('Revenuetype__c','LineStatus__c');
    revenuelinetoproductlinefieldsmap.put('RLContact__c','PLContact__c');
    revenuelinetoproductlinefieldsmap.put('RLDate__c','LineClosedate__c');
    revenuelinetoproductlinefieldsmap.put('RL_Designation_Comments__c','Designation__c');
    revenuelinetoproductlinefieldsmap.put('SalesOrderNumber__c','SalesOrderNumber__c');
    revenuelinetoproductlinefieldsmap.put('TECH_IntouchQuoteLineRowId__c','TECH_IntouchQuoteLineRowId__c');
    revenuelinetoproductlinefieldsmap.put('TECH_IntouchQuoteRowId__c','TECH_IntouchQuoteRowId__c');
    revenuelinetoproductlinefieldsmap.put('Unit_Price__c','Amount__c');
    revenuelinetoproductlinefieldsmap.put('Id','ITBRevenueLineIdOld__c');
    revenuelinetoproductlinefieldsmap.put('Name','ITBRevenueLineNoOld__c');
  }


   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator([select Name,StageName,Status__c,CloseDate,DataMigration__c,Amount,OpportunityScope__c,LeadingBusiness__c,TECH_AmountEUR__c,CurrencyIsoCode,IncludedInForecast__c,Probability,DataMigration_CaseNumber__c,(select TECH_CommercialReference__c,Commission__c,ConfigurationID__c,DeliveryDate__c,Family__c,IncludedInForecast__c,IncludeinQuote__c,LegacyLineId__c,ProductDescription__c,Opportunity__c,OverlayRevenueITB__c,Probability__c,ProductBU__c,ProductFamily__c,ProductLine__c,PurchaseOrderNumber__c,Quantity__c,RevenueOverride__c,LineStatus__c,PLContact__c,LineClosedate__c,LineType__c,Designation__c,SalesOrderNumber__c,TECH_IntouchQuoteLineRowId__c,TECH_IntouchQuoteRowId__c,Amount__c,CurrencyIsoCode from Product_Line_2__r),(select TECH_CommercialReference__c,Commission__c,Configuration_Id__c,ExpectedShippeddate__c,Family__c,IncludedInForecast__c,Include_in_Quote__c,Legacy_Line_Id__c,ProductDescription__c,Opportunity__c,OverlayRevenue__c,Probability__c,ProductBU__c,ProductFamily__c,ProductLine__c,PurchaseOrderNumber__c,Quantity__c,RevenueOverride__c,Revenuetype__c,RLContact__c,RLDate__c,RL_Designation_Comments__c,SalesOrderNumber__c,TECH_IntouchQuoteLineRowId__c,TECH_IntouchQuoteRowId__c,Unit_Price__c,RL_Amount__c,CurrencyIsoCode,Name from ITB_Revenue_Lines__r) from Opportunity where StageName in :tobeProcessedStageNames and DataMigration__c=False and OpportunityScope__c!=null and OpportunityScope__c in :productmap.keySet() order by CreatedDate DESC limit :limitnumber]);
   }
   
   global void execute(
                Database.BatchableContext BC, 
                List<sObject> scope){      
      Map<Id,Opportunity> opportunitymap=new Map<Id,Opportunity>();    
      for(sObject s : scope){
          Opportunity opprecord=(Opportunity)s;   
          opportunitymap.put(opprecord.id,opprecord);
      }
      //pass by reference      
      Map<String,Double> opportunityLinesAmountMap=new Map<String,Double>();
      Map<String,Double> revenueLinesAmountMap=new Map<String,Double>();
      Set<Id> revenueLineswithinconsistentcurrency=new Set<Id>();    
      Set<Id> opportunityLineswithinconsistentcurrency=new Set<Id>();    

      prepareinBatchMaps(opportunitymap,opportunityLinesAmountMap,revenueLinesAmountMap,revenueLineswithinconsistentcurrency,opportunityLineswithinconsistentcurrency);

      processCaseByCase(opportunitymap,opportunityLinesAmountMap,revenueLinesAmountMap,revenueLineswithinconsistentcurrency,opportunityLineswithinconsistentcurrency);
   }

   private void prepareinBatchMaps(Map<Id,Opportunity> opportunitymap,Map<String,Double> opportunityLinesAmountMap,Map<String,Double> revenueLinesAmountMap,Set<Id> revenueLineswithinconsistentcurrency,Set<Id> opportunityLineswithinconsistentcurrency)
   {        
    //for(Opportunity opportunityrecord:[select id,CurrencyIsoCode,(select Amount__c,CurrencyIsoCode from  Product_Line_2__r) from Opportunity where id in :opportunitymap.keyset()])
    List<Opportunity> lstOpportunitesforOppLines=[select id,CurrencyIsoCode,(select Amount__c,CurrencyIsoCode from  Product_Line_2__r) from Opportunity where id in :opportunitymap.keyset()];
    for(Opportunity opportunityrecord:lstOpportunitesforOppLines)
    {
        Double prodsum=0;
        for(OPP_ProductLine__c opportunitylinerecord:opportunityrecord.Product_Line_2__r)
        {
            if(opportunitylinerecord.CurrencyISOCode==opportunityrecord.CurrencyISOCode){
                if(opportunitylinerecord.Amount__c!=null)
                  prodsum+=opportunitylinerecord.Amount__c;                
            }
            else
                opportunityLineswithinconsistentcurrency.add(opportunitylinerecord.id);
            
        }
        opportunityLinesAmountMap.put(opportunityrecord.id,prodsum);
    }         
    //prepare revenueLinesAmountMap
    //for(Opportunity opportunityrecord:[select id,Amount,CurrencyIsoCode, (select RL_Amount__c,CurrencyIsoCode from  ITB_Revenue_Lines__r) from Opportunity where id in :opportunitymap.keyset()])
    List<Opportunity> lstOpportunitesforrevLines=[select id,Amount,CurrencyIsoCode, (select RL_Amount__c,CurrencyIsoCode from  ITB_Revenue_Lines__r) from Opportunity where id in :opportunitymap.keyset()];
    for(Opportunity opportunityrecord:lstOpportunitesforrevLines)
    {
        Double revsum=0;
        for(Revenue_Line__c revenuelinerecord:opportunityrecord.ITB_Revenue_Lines__r){
            if(opportunityrecord.CurrencyIsoCode==revenuelinerecord.CurrencyIsoCode){
                if(revenuelinerecord.RL_Amount__c!=null)
                    revsum+=revenuelinerecord.RL_Amount__c;
            }
            else
                revenueLineswithinconsistentcurrency.add(revenuelinerecord.id);
        }        
        revenueLinesAmountMap.put(opportunityrecord.id,revsum);
    } 


   }

   global void processCaseByCase(Map<Id,Opportunity> opportunitymap,Map<String,Double> opportunityLinesAmountMap,Map<String,Double> revenueLinesAmountMap,Set<Id> revenueLineswithinconsistentcurrency,Set<Id> opportunityLineswithinconsistentcurrency)
   {
      List<OPP_ProductLine__c> opportunitylinestobecreated=new List<OPP_ProductLine__c>();
    List<OPP_ProductLine__c> opportunitylinestobeupdated=new List<OPP_ProductLine__c>();
    List<Opportunity> opportunitiestobeupdated=new List<Opportunity>();

    for(Opportunity opprecord:opportunitymap.values())
    {
        if(opprecord.Amount==null)
        opprecord.Amount=0;
        //case 1 - no product lines and no revenue lines
        if(opprecord.Product_Line_2__r.size()==0 && opprecord.ITB_Revenue_Lines__r.size()==0){
            OPP_ProductLine__c tempOppLineRecord=populateBasicInfo(opprecord);
            tempOppLineRecord.Amount__c=opprecord.Amount;                        
            calculateLineStatus(tempOppLineRecord,opprecord);                
            opportunitylinestobecreated.add(tempOppLineRecord);
            opprecord.DataMigration_CaseNumber__c='1';
            System.debug('is in case 1');
        }
        //case 2
        else if(opprecord.Product_Line_2__r.size()>0 && opprecord.ITB_Revenue_Lines__r.size()==0)
        {
            //calculate the sum,since the product line and opp will have same currency am not writing any rules for amount conversion            
            if(opportunityLinesAmountMap.containsKey(opprecord.id))
            {
                //case 2.1.1
                if(opportunityLinesAmountMap.get(opprecord.id)>opprecord.Amount && opportunityLinesAmountMap.get(opprecord.id)<(1.03*opprecord.Amount))
                {                    
                    for(OPP_ProductLine__c existingline:opprecord.Product_Line_2__r){
                      if(opportunityLineswithinconsistentcurrency.contains(existingline.id)){
                        existingline.LineStatus__c='To be deleted';
                        existingline.IncludeinQuote__c=false;                        
                      }                      
                      else
                        calculateLineStatus(existingline,opprecord);

                        populateExistingLineInfo(existingline,opprecord);
                        if(existingline.Amount__c!=null)
                          existingline.Amount__c=existingline.Amount__c/existingline.Quantity__c;
                        else
                          existingline.Amount__c=0;                      
                        energyCleanUp(existingline);
                        opportunitylinestobeupdated.add(existingline);
                    }    
                    OPP_ProductLine__c tempOppLineRecord=populateBasicInfo(opprecord);
                    tempOppLineRecord.Amount__c=opprecord.Amount;                                       
                    tempOppLineRecord.LineStatus__c='To be deleted';
                    tempOppLineRecord.IncludeinQuote__c=false;
                    opportunitylinestobecreated.add(tempOppLineRecord);                    
                    opprecord.DataMigration_CaseNumber__c='2.1.1';
                    System.debug('is in case 2.1.1');
                }
                //case 2.1.2
                else if(opportunityLinesAmountMap.get(opprecord.id)>(1.03*opprecord.Amount))
                {
                    for(OPP_ProductLine__c existingline:opprecord.Product_Line_2__r){                      
                        existingline.LineStatus__c='To be deleted';
                        existingline.IncludeinQuote__c=false;
                        populateExistingLineInfo(existingline,opprecord);
                        if(existingline.Amount__c!=null)
                            existingline.Amount__c=existingline.Amount__c/existingline.Quantity__c;
                        else
                            existingline.Amount__c=0;                          
                        energyCleanUp(existingline);
                        opportunitylinestobeupdated.add(existingline);

                    } 

                    OPP_ProductLine__c tempOppLineRecord=populateBasicInfo(opprecord);
                    tempOppLineRecord.Amount__c=opprecord.Amount;                                       
                    calculateLineStatus(tempOppLineRecord,opprecord);
                    opportunitylinestobecreated.add(tempOppLineRecord);                    
                    opprecord.DataMigration_CaseNumber__c='2.1.2';
                    System.debug('is in case 2.1.2');
                }
                //case 2.2
                else if(opportunityLinesAmountMap.get(opprecord.id)<(1.03*opprecord.Amount))
                {                    
                    for(OPP_ProductLine__c existingline:opprecord.Product_Line_2__r){
                      if(opportunityLineswithinconsistentcurrency.contains(existingline.id)){
                        existingline.LineStatus__c='To be deleted';
                        existingline.IncludeinQuote__c=false;
                      }                      
                      else
                        calculateLineStatus(existingline,opprecord);

                        populateExistingLineInfo(existingline,opprecord);
                        if(existingline.Amount__c!=null)
                            existingline.Amount__c=existingline.Amount__c/existingline.Quantity__c;
                        else
                            existingline.Amount__c=0;                      
                        energyCleanUp(existingline);
                        opportunitylinestobeupdated.add(existingline);
                    }                                           
                    OPP_ProductLine__c tempOppLineRecord=populateBasicInfo(opprecord);
                    tempOppLineRecord.Amount__c=opprecord.Amount-opportunityLinesAmountMap.get(opprecord.id);
                    calculateLineStatus(tempOppLineRecord,opprecord);
                    opportunitylinestobecreated.add(tempOppLineRecord);
                    opprecord.DataMigration_CaseNumber__c='2.2';
                    System.debug('is in case 2.2');
                }                
            }
            
        }
        //case 3
        else if(opprecord.Product_Line_2__r.size()==0 && opprecord.ITB_Revenue_Lines__r.size()>0)
        {
            if(revenueLinesAmountMap.containsKey(opprecord.id))
            {                            
                OPP_ProductLine__c tempOppLineRecord=populateBasicInfo(opprecord);
                tempOppLineRecord.Amount__c=opprecord.Amount;                
                //case 3.1
                if(revenueLinesAmountMap.get(opprecord.id)!=0){
                tempOppLineRecord.LineStatus__c='To be deleted';                
                tempOppLineRecord.IncludeinQuote__c=false;
                opprecord.DataMigration_CaseNumber__c='3.1';
                System.debug('is in case 3.1');
                }
                //case 3.2
                else{                  
                calculateLineStatus(tempOppLineRecord,opprecord);                
                opprecord.DataMigration_CaseNumber__c='3.2';
                System.debug('is in case 3.2');
                }
                opportunitylinestobecreated.add(tempOppLineRecord);

                //copy the revenue lines to opportunity lines
                for(Revenue_Line__c revenuelinerecord:opprecord.ITB_Revenue_Lines__r)
                {
                    OPP_ProductLine__c tempOppLineRecord_toCopy=new OPP_ProductLine__c();
                    fieldtofieldMapping(revenuelinerecord,tempOppLineRecord_toCopy);
                    populateProductInformationforRLTOOLlines(tempOppLineRecord_toCopy);
                    if(revenueLineswithinconsistentcurrency.contains(revenuelinerecord.id))
                        tempOppLineRecord_toCopy.Amount__c=0;
                    tempOppLineRecord_toCopy.CurrencyIsoCode=opprecord.CurrencyIsoCode;                       
                    opportunitylinestobecreated.add(tempOppLineRecord_toCopy);
                }                
            }
        }

        //case 4
        else if(opprecord.Product_Line_2__r.size()>0 && opprecord.ITB_Revenue_Lines__r.size()>0 && opprecord.LeadingBusiness__c=='IT')
        {
            if(revenueLinesAmountMap.containsKey(opprecord.id))
            {                            
                OPP_ProductLine__c tempOppLineRecord=populateBasicInfo(opprecord);
                tempOppLineRecord.Amount__c=opprecord.Amount;                                

                //case 4.1
                if(revenueLinesAmountMap.get(opprecord.id)!=0){
                tempOppLineRecord.LineStatus__c='To be deleted';
                tempOppLineRecord.IncludeinQuote__c=false;
                opprecord.DataMigration_CaseNumber__c='4.1';
                System.debug('is in case 4.1');
                }
                //case 4.2
                else{
                calculateLineStatus(tempOppLineRecord,opprecord);                
                opprecord.DataMigration_CaseNumber__c='4.2';
                System.debug('is in case 4.2');
                }
                opportunitylinestobecreated.add(tempOppLineRecord);

                //copy the revenue lines to opportunity lines
                for(Revenue_Line__c revenuelinerecord:opprecord.ITB_Revenue_Lines__r)
                {
                    OPP_ProductLine__c tempOppLineRecord_toCopy=new OPP_ProductLine__c();
                    fieldtofieldMapping(revenuelinerecord,tempOppLineRecord_toCopy);
                    populateProductInformationforRLTOOLlines(tempOppLineRecord_toCopy);
                    if(revenueLineswithinconsistentcurrency.contains(revenuelinerecord.id))
                        tempOppLineRecord_toCopy.Amount__c=0;
                    tempOppLineRecord_toCopy.CurrencyIsoCode=opprecord.CurrencyIsoCode;                       
                    opportunitylinestobecreated.add(tempOppLineRecord_toCopy);
                }  

                //existing opportunity lines should be marked as to be deleted
                for(OPP_ProductLine__c existingline:opprecord.Product_Line_2__r){
                        existingline.LineStatus__c='To be deleted';                         
                        existingline.IncludeinQuote__c=false;
                        populateExistingLineInfo(existingline,opprecord);
                        if(existingline.Amount__c!=null)
                            existingline.Amount__c=existingline.Amount__c/existingline.Quantity__c;
                        else
                            existingline.Amount__c=0;  
                        energyCleanUp(existingline);
                        opportunitylinestobeupdated.add(existingline);
                }     
            }
        }

         //case 5
        else if(opprecord.Product_Line_2__r.size()>0 && opprecord.ITB_Revenue_Lines__r.size()>0 && opprecord.LeadingBusiness__c!='IT')
        {
            if(revenueLinesAmountMap.containsKey(opprecord.id))
            {                                            
                OPP_ProductLine__c tempOppLineRecord=populateBasicInfo(opprecord);                
                tempOppLineRecord.Amount__c=opprecord.Amount;                
                calculateLineStatus(tempOppLineRecord,opprecord);
                opportunitylinestobecreated.add(tempOppLineRecord);

                //copy the revenue lines to opportunity lines
                for(Revenue_Line__c revenuelinerecord:opprecord.ITB_Revenue_Lines__r)
                {
                    OPP_ProductLine__c tempOppLineRecord_toCopy=new OPP_ProductLine__c();
                    fieldtofieldMapping(revenuelinerecord,tempOppLineRecord_toCopy);
                    populateProductInformationforRLTOOLlines(tempOppLineRecord_toCopy);
                    if(revenueLineswithinconsistentcurrency.contains(revenuelinerecord.id))
                        tempOppLineRecord_toCopy.Amount__c=0;
                    tempOppLineRecord_toCopy.LineStatus__c='To be deleted';
                    // tempOppLineRecord_toCopy.IncludeinQuote__c=false;
                    tempOppLineRecord_toCopy.CurrencyIsoCode=opprecord.CurrencyIsoCode;                       
                    opportunitylinestobecreated.add(tempOppLineRecord_toCopy);
                }  

                //existing opportunity lines should be marked as to be deleted
                for(OPP_ProductLine__c existingline:opprecord.Product_Line_2__r){
                        existingline.LineStatus__c='To be deleted';  
                        existingline.IncludeinQuote__c=false;                      
                        populateExistingLineInfo(existingline,opprecord);
                        if(existingline.Amount__c!=null)
                            existingline.Amount__c=existingline.Amount__c/existingline.Quantity__c;
                        else
                            existingline.Amount__c=0;  
                        energyCleanUp(existingline);
                        opportunitylinestobeupdated.add(existingline);
                }     
            }
            opprecord.DataMigration_CaseNumber__c='5';
            System.debug('is in case 5');
        }        
        opprecord.DataMigration__c=True;
        opportunitiestobeupdated.add(new Opportunity(Id=opprecord.id,DataMigration__c=true,DataMigration_CaseNumber__c=opprecord.DataMigration_CaseNumber__c));
    }
    Savepoint sp = Database.setSavepoint();
    try{
        Database.update(opportunitiestobeupdated);          
        Database.insert(opportunitylinestobecreated);
        Database.update(opportunitylinestobeupdated);                    
    }    
    catch(Exception ex)
    {
        Database.rollback(sp);
    } 
}

global void finish(Database.BatchableContext BC){
   }
   
private void fieldtofieldMapping(Revenue_Line__c revenuelinerecord,OPP_ProductLine__c productlinerecord)
{
    for(String revenuelinefield:revenuelinetoproductlinefieldsmap.keyset())
    {
        productlinerecord.put(revenuelinetoproductlinefieldsmap.get(revenuelinefield),revenuelinerecord.get(revenuelinefield));
    }
}    

private OPP_ProductLine__c populateBasicInfo(Opportunity opportunityrecord)
{
    OPP_ProductLine__c productlinerecord=new OPP_ProductLine__c();
    System.debug('opportunityrecord.OpportunityScope__c'+opportunityrecord.OpportunityScope__c);
    OPP_Product__c productrecord=productmap.get(opportunityrecord.OpportunityScope__c);
    productlinerecord.opportunity__c=opportunityrecord.id;
    productlinerecord.Product__c=productrecord.id;
    productlinerecord.ProductBU__c=productrecord.BusinessUnit__c;
    productlinerecord.ProductLine__c=productrecord.Name;  
    productlinerecord.Quantity__c=1;
    productlinerecord.CurrencyISOCode=opportunityrecord.CurrencyISOCode;
    productlinerecord.RecordTypeId=System.Label.CLOCT13SLS22;    
    productlinerecord.LineClosedate__c=opportunityrecord.closeDate;
    productlinerecord.LineType__c='Parent Line';
    productlinerecord.IncludedInForecast__c=opportunityrecord.IncludedInForecast__c;
    productlinerecord.Probability__c=opportunityrecord.Probability;
    productlinerecord.IncludeinQuote__c=false;
    return productlinerecord;  
}

private void populateExistingLineInfo(OPP_ProductLine__c productlinerecord,Opportunity opportunityrecord)
{    
    productlinerecord.RecordTypeId=System.Label.CLOCT13SLS22;    
    productlinerecord.LineClosedate__c=opportunityrecord.closeDate;
    productlinerecord.LineType__c='Parent Line';
    productlinerecord.IncludedInForecast__c=opportunityrecord.IncludedInForecast__c;
    productlinerecord.Probability__c=opportunityrecord.Probability;  
    if(productlinerecord.Quantity__c==null || productlinerecord.Quantity__c==0)
    productlinerecord.Quantity__c=1;  
}

private void calculateLineStatus(OPP_ProductLine__c productlinerecord,Opportunity opportunityrecord)
{
  //stage Name 
  if(openStages.contains(opportunityrecord.StageName))
  {
    productlinerecord.LineStatus__c='Pending';
  }
  else if(wonStage==opportunityrecord.StageName)
  {
   productlinerecord.LineStatus__c='Won'; 
  }
  else if(lostStage==opportunityrecord.StageName)
  {      
    if(opportunityrecord.Status__c!=null && opportunityrecord.Status__c.startsWithIgnoreCase('Cancelled'))
      productlinerecord.LineStatus__c='Cancelled';
    else
      productlinerecord.LineStatus__c='Lost';  
  }
}  
private void energyCleanUp(OPP_ProductLine__c productlinerecord)
{
  if(productlinerecord.ProductBU__c!=null && productlinerecord.ProductBU__c.startsWithIgnoreCase('ENERGY'))
    productlinerecord.ProductBU__c='INFRASTRUCTURE';
  if(productlinerecord.ProductLine__c!=null && productlinerecord.ProductLine__c.startsWithIgnoreCase('EN'))
    productlinerecord.ProductLine__c='IN'+productlinerecord.ProductLine__c.subString(2,productlinerecord.ProductLine__c.length());

}

private void populateProductInformationforRLTOOLlines(OPP_ProductLine__c existingline)
{
    String keyString=existingline.ProductBU__c+':'+existingline.ProductLine__c+':'+existingline.ProductFamily__c+':'+existingline.Family__c;
    keyString=keyString.toLowerCase();
    if(concatenatedProductMap.containsKey(keyString))
    existingline.Product__c=concatenatedProductMap.get(keyString);
}


}