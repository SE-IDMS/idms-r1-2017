/*******
Release:Q2 2016 release 
Comment:Test class for VFC_ELLAEmailUnsubscribe main class


********/

@istest
public class VFC_ELLAEmailUnsubscribe_TEST {
    static Testmethod  void Test_VFC_ELLAEmailUnsubscribe() { 
        Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
        List<LaunchReportStakeholders__c> newLReportSholder= new List<LaunchReportStakeholders__c>();
        User newUser = Utils_TestMethods.createStandardUser('ELLAMQ2');  
        User newUser1 = Utils_TestMethods.createStandardUser('ElQ2Sh');  
        PageReference pageRef= Page.VFP_ELLAEmailUnsubscribe; 
        //
        Blob beforeblobUsr = Blob.valueOf(newUser1.email);
        string strUserEmailEncode = EncodingUtil.base64Encode(beforeblobUsr);
        pageRef.getParameters().put(label.CLJUN16ELLA01,strUserEmailEncode );
        
        Test.setCurrentPageReference(pageRef);
        Database.SaveResult UserInsertResult = Database.insert(newUser, false);
        newUser1.BypassVR__c = TRUE;
        
        newUser1.UserRoleID = rol_ID ;
        newUser1.BypassTriggers__c='AP_Offer1;AP_Offer3';
        //Database.SaveResult UserInsertResult1 = Database.insert(newUser1, false);
        insert newUser1; 
        system.runas(newUser1) {
            LaunchReportStakeholders__c LaunchReport = new LaunchReportStakeholders__c();
            LaunchReport.BUReport__c=true;
            LaunchReport.User__c=newUser1.id;
            LaunchReport.IsActive__c=true;
            newLReportSholder.add(LaunchReport);
            
            insert newLReportSholder; 
            
            VFC_ELLAEmailUnsubscribe  vfellUnsub= new VFC_ELLAEmailUnsubscribe();
            vfellUnsub.verify();
        }
        Test.startTest();
        Blob beforeblobUsr1 = Blob.valueOf('');
        string strUserEmailEncode1 = EncodingUtil.base64Encode(beforeblobUsr1);
        pageRef.getParameters().put(label.CLJUN16ELLA01,strUserEmailEncode1 );
        
        Test.setCurrentPageReference(pageRef);
        system.runas(newUser1) {
              List<LaunchReportStakeholders__c> newLReportSholder1= new List<LaunchReportStakeholders__c>();
            LaunchReportStakeholders__c LaunchReport1 = new LaunchReportStakeholders__c();
            LaunchReport1.BUReport__c=true;
            LaunchReport1.User__c=newUser.id;
            LaunchReport1.IsActive__c=true;
            newLReportSholder1.add(LaunchReport1);
            
            insert newLReportSholder1; 
            
            VFC_ELLAEmailUnsubscribe  vfellUnsub1= new VFC_ELLAEmailUnsubscribe();
            vfellUnsub1.verify();
        }
        
        Test.stopTest();
            
    }        
}