global class AP29_SaveAdvancedPA
{
    global class AdvancedPA
    {
        WebService String ListPrice;
        WebService DateTime PriceDate;
        WebService integer Quantity;
        WebService String CatalogNumber;
    }
    
     global class returnDataBean
     {
        WebService String returnCode;
        WebService String returnMessage;
     }     
     
    WebService static returnDataBean SaveAdvancedPA(AdvancedPA PAarg, String CaseID, String bFOAccountID)
    {
        CSE_ExternalReferences__c ExternalReference = new CSE_ExternalReferences__c();
        ExternalReference.Case__c = CaseID;  
        ExternalReference.Type__c = 'P&A';
      
        String delimiter = ': ';
        String newline = '\n';
        
        ExternalReference.Description__c ='';
        ExternalReference.Description__c += 'Price Date' + delimiter + PAarg.PriceDate;
        ExternalReference.Description__c += newline + 'List Price' + delimiter + PAarg.ListPrice;     
        ExternalReference.Description__c += newline + 'Quantity' + delimiter + PAarg.Quantity;
        ExternalReference.Description__c += newline + 'Part No.' + delimiter + PAarg.CatalogNumber;    
        
        ExternalReference.Description__c = Replace(ExternalReference.Description__c,'TRY','YTL');
        ExternalReference.Description__c = Replace(ExternalReference.Description__c,'EUR','&#8364;');
        ExternalReference.Description__c = Replace(ExternalReference.Description__c,'USD','&#36;');
        
        returnDataBean returnBean = new returnDataBean();
        returnBean.returnCode = 'E';
        
        try
        {    
            DataBase.SaveResult InsertedExternalReference = DataBase.insert(ExternalReference); 
            System.Debug('---- DML Errors: ' + InsertedExternalReference .getErrors() ); 
            
            if(InsertedExternalReference.issuccess())
                returnBean.returnCode = 'S';
            else
            {             
                returnBean.returnMessage='Save P&A has failed';
            }
        }
        catch(Exception e)
        {
            returnBean.returnMessage=e.getmessage();     
        }
                                     
        return returnBean;
    }
    
    public static String Replace(String StringToHandle, String ReplacingValue, String ValueToBeReplaced)
    {
           List<String> ExtRefSplit =  StringToHandle.split(ValueToBeReplaced,-1);
           if(ExtRefSplit.size()>1)
           {
                StringToHandle = ExtRefSplit[0];
                StringToHandle += ReplacingValue;
                StringToHandle += ExtRefSplit[1];
           } 
           
           return StringToHandle;
    }
}