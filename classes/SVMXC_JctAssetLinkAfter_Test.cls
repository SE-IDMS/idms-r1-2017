@isTest(seealldata=true)
public class SVMXC_JctAssetLinkAfter_Test {
    
    static testMethod void assestlinktest() {
        
        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(name='testip');
        insert ip;
        
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(name='testip2');
        insert ip2;
        
        JCtAssetLink__c assestlink= new JCtAssetLink__c(Note__c='test note',LinkToAsset__c=ip.id,LinkType__c = 'AS_LINK_1',UpdateRelatedIPonInsert__c=true,InstalledProduct__c=ip2.id);
        insert assestlink;
        
    }

}