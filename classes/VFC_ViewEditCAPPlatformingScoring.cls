/*
 Weights are not taken from formula fields.
 Dynamically fetch weights as trigger will update weights in backend tech field. 
 Ensure to show the approprite weights fetching at run time. 
 We check for CAP weights, if it is not defined,
 then we will check for country classification based on CL1 and KMT and then preference
 on Global weight and Default weight.
*/

public class VFC_ViewEditCAPPlatformingScoring{
    public Id psID{get;set;}
    public SFE_PlatformingScoring__c ps{get;set;}{ps=new SFE_PlatformingScoring__c();}
    public List<CAPGlobalMasterData__c> capQuesAnsList=new List<CAPGlobalMasterData__c>();
    public List<CAPGlobalMasterData__c> capQuesAnsGlobalList=new List<CAPGlobalMasterData__c>();
    public List<wrapscoring> wrapscoringList{get;set;} {wrapscoringList=new List<wrapscoring>();}
    
    public VFC_ViewEditCAPPlatformingScoring(ApexPages.StandardController controller) {
        psID=(ID)System.currentPageReference().getParameters().get( 'id' );
        if(psID!=null){
            ps=[Select Name,AssignedTo__c,AccountOwner__c,PlatformedAccount__c,PlatformedAccount__r.country__c,PlatformedAccount__r.ClassLevel1__c,PlatformedAccount__r.MarketSegment__c,IndivCAP__c,IndivCAP__r.CriterWeightQu1__c,IndivCAP__r.CriterWeightQu2__c,IndivCAP__r.CriterWeightQu3__c,IndivCAP__r.CriterWeightQu4__c,IndivCAP__r.CriterWeightQu5__c,IndivCAP__r.CriterWeightQu6__c,IndivCAP__r.CriterWeightQu7__c,IndivCAP__r.CriterWeightQu8__c,Stateprovince__c,EndDate__c,ClassificationLvl1__c,CustomerProfile__c,Score__c,MktShare__c,PAMPlaforming__c,DirectSalesPlatforming__c,IndirectSalesPlatforming__c,Q1Rating__c,Q2Rating__c,Q3Rating__c,Q4Rating__c,Q5Rating__c,Q6Rating__c,Q7Rating__c,Q8Rating__c,Q1CriteriaWeight__c,Q2CriteriaWeight__c,Q3CriteriaWeight__c,Q4CriteriaWeight__c,Q5CriteriaWeight__c,Q6CriteriaWeight__c,Q7CriteriaWeight__c,Q8CriteriaWeight__c From SFE_PlatformingScoring__c  where Id=:psID];
            System.debug('>>>>>'+ps);
            
            //Question sequence
            for(Integer i=0;i<8;i++){
                wrapscoring ws=new wrapscoring();
                ws.qseq='Q'+(i+1);
                wrapscoringList.add(ws);
            }
            //Rating
            wrapscoringList[0].rating=ps.Q1Rating__c ;
            wrapscoringList[1].rating=ps.Q2Rating__c ;
            wrapscoringList[2].rating=ps.Q3Rating__c ;
            wrapscoringList[3].rating=ps.Q4Rating__c ;
            wrapscoringList[4].rating=ps.Q5Rating__c ;
            wrapscoringList[5].rating=ps.Q6Rating__c ;
            wrapscoringList[6].rating=ps.Q7Rating__c ;
            wrapscoringList[7].rating=ps.Q8Rating__c ; 
            //Weight          
            wrapscoringList[0].wgt=ps.Q1CriteriaWeight__c ;
            wrapscoringList[1].wgt=ps.Q2CriteriaWeight__c ;
            wrapscoringList[2].wgt=ps.Q3CriteriaWeight__c ;
            wrapscoringList[3].wgt=ps.Q4CriteriaWeight__c ;
            wrapscoringList[4].wgt=ps.Q5CriteriaWeight__c ;
            wrapscoringList[5].wgt=ps.Q6CriteriaWeight__c ;
            wrapscoringList[6].wgt=ps.Q7CriteriaWeight__c ;
            wrapscoringList[7].wgt=ps.Q8CriteriaWeight__c ; 
           
            List<CAPGlobalMasterData__c> cwgList=new List<CAPGlobalMasterData__c>();
            cwgList=[SELECT QuestionInterpretation__c,AnswerInterpretation1__c,AnswerInterpretation2__c,AnswerInterpretation3__c,AnswerInterpretation4__c,ClassificationLevel1__c,Weight__c,QuestionSequence__c from CAPGlobalMasterData__c where ClassificationLevel1__c=:ps.PlatformedAccount__r.ClassLevel1__c OR ClassificationLevel1__c='Default' ORDER BY QuestionSequence__c ASC];
            for(CAPGlobalMasterData__c cwg:cwgList){
                if(cwg.ClassificationLevel1__c=='Default')
                    capQuesAnsGlobalList.add(cwg);
                 else
                    capQuesAnsList.add(cwg);
             }
                
          //Question Interpretation and Answer Interpretation as per custom classification L1 from CAPGlobalMasterData
          if(capQuesAnsList.size()==8){
            for(Integer i=0;i<8;i++)
              wrapscoringList[i].ques=capQuesAnsList[i].QuestionInterpretation__c;

           for(Integer i=0;i<8;i++){
              wrapscoringList[i].ans1=capQuesAnsList[i].AnswerInterpretation1__c;
              wrapscoringList[i].ans2=capQuesAnsList[i].AnswerInterpretation2__c;
              wrapscoringList[i].ans3=capQuesAnsList[i].AnswerInterpretation3__c;
              wrapscoringList[i].ans4=capQuesAnsList[i].AnswerInterpretation4__c;
           }
       }
      
          //Fetch the Default Question and Answer Interpretation from CAPGlobalMasterData if its not available for the respective classification
          else if(capQuesAnsGlobalList.size()==8){
           for(Integer i=0;i<8;i++)
              wrapscoringList[i].ques=capQuesAnsGlobalList[i].QuestionInterpretation__c;

            for(Integer i=0;i<8;i++){
              wrapscoringList[i].ans1=capQuesAnsGlobalList[i].AnswerInterpretation1__c;
              wrapscoringList[i].ans2=capQuesAnsGlobalList[i].AnswerInterpretation2__c;
              wrapscoringList[i].ans3=capQuesAnsGlobalList[i].AnswerInterpretation3__c;
              wrapscoringList[i].ans4=capQuesAnsGlobalList[i].AnswerInterpretation4__c;
               }
           }
        }
    }
    public class wrapscoring{
        public String qseq{get; set;}
        public String ques{get; set;}
        public String rating{get; set;}
        public String ans1{get; set;}
        public String ans2{get; set;}
        public String ans3{get; set;}
        public String ans4{get; set;}
        public Decimal wgt{get; set;}
        public wrapscoring(){}
     }
    public PageReference EditPS(){
        PageReference curPage = new PageReference(Site.getPathPrefix()+'/apex/VFP_EditCAPPlatformingScoring');
        curPage.getParameters().put('id',psID);
        curPage.setRedirect(true);
        return curPage;
    }
       public PageReference Save(){
            ps.Q1Rating__c=wrapscoringList[0].rating;
            ps.Q2Rating__c=wrapscoringList[1].rating;
            ps.Q3Rating__c=wrapscoringList[2].rating;
            ps.Q4Rating__c=wrapscoringList[3].rating;
            ps.Q5Rating__c=wrapscoringList[4].rating;
            ps.Q6Rating__c=wrapscoringList[5].rating;
            ps.Q7Rating__c=wrapscoringList[6].rating;
            ps.Q8Rating__c=wrapscoringList[7].rating;
            try{
                update ps;
                PageReference curPage = new PageReference(Site.getPathPrefix()+'/apex/VFP_ViewCAPPlatformingScoring');
                curPage.getParameters().put('id',psID);
                curPage.setRedirect(true);
                return curPage;
           }
           Catch(Exception ex){
           ApexPages.addMessages(ex);        
          } 
          return null;
    }
    public PageReference Deleteps(){
        try{
                delete ps;
                PageReference curPage = new PageReference(Site.getPathPrefix()+'/'+ps.IndivCAP__c);
                curPage.setRedirect(true);
                return curPage;
           }
           Catch(Exception ex){
           ApexPages.addMessages(ex);        
          } 
          return null; 
    }
     public PageReference backToCAP(){
       PageReference pr = new PageReference(Site.getPathPrefix()+'/'+ps.IndivCAP__c);
       pr.setredirect(true); 
       return pr;
   }
}