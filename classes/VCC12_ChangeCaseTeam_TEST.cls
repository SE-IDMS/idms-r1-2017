/* 
    Author: Nicolas PALITZYNE (Accenture)
    Created date: 26/02/2012
    Description: Test Class for VCC12_ChangeCaseTeam Apex Controller
*/  

@istest
private class VCC12_ChangeCaseTeam_TEST
{
    private static testMethod void testVCC12() { 
        Country__c CCCountry = new Country__c(Name='TestCountry',CountryCode__c='TC');
        insert CCCountry; 
        
        Account anAccount = Utils_TestMethods.createAccount();
        anAccount.Country__c=CCCountry.id;
        insert anAccount;
        
        
        Contact aContact = Utils_TestMethods.createContact(anAccount.Id,'aTestContact');
        insert aContact;       
                
        user testUser =  Utils_TestMethods.createStandardUser('testcls');
        insert testUser;
        
        CustomerCareTeam__c sampleCCTeam = new CustomerCareTeam__c(CCCountry__c=CCCountry.ID, Name='Sample CC Team',LevelOfSupport__c='Primary', CaseOrigin__c='Web',
                                                                       CustomerClassifications__c='EU', SupportClassification__c='DAC');
        insert sampleCCTeam;
       
        CaseClassification__c sampleClassification = new CaseClassification__c(category__c='6-General', Reason__c='medium', SubReason__c='Complaint');
        insert sampleClassification;
        
        TeamCaseJunction__c sampleSupportedCaseCls = new TeamCaseJunction__c(CaseClassification__c = sampleClassification.ID, CCTeam__c=sampleCCTeam.ID);
        insert sampleSupportedCaseCls;
        
        TeamAgentMapping__c teamMember = new TeamAgentMapping__c(CCAgent__c = testUser.ID, CCTeam__c = sampleCCTeam.ID, defaultTeam__c=true);
        insert teamMember;
        /* BR-1825 Commented By Vimal K (GD India)
        Case testCase = Utils_TestMethods.createCase(anAccount.Id, aContact.Id, 'Open');
        */
        Case testCase = Utils_TestMethods.createCase(anAccount.Id, aContact.Id, 'In Progress');
        /* BR-4310 Commented By Vimal K (GD India)
        testCase.supportCategory__c = '6-General';
        testCase.Symptom__c = 'medium';
        testCase.subSymptom__c = 'Complaint';
        */
        testCase.supportCategory__c = 'Others*';
        testCase.Symptom__c = 'Training Request';
        
        testCase.ProductFamily__c = 'PF_TEST';
        testCase.CCCountry__c = CCCountry.ID;
        insert testCase;

        //OPP_Product__c sampleProduct = new OPP_Product__c(Name='PF_TEST', BusinessUnit__c='EN', ProductLine__c='PL_TEST');
        //insert sampleProduct;
        OPP_Product__c sampleProduct = new OPP_Product__c();
        sampleProduct.Name = 'TEST BU';
        sampleProduct.BusinessUnit__c = 'TEST BU';
        sampleProduct.ProductLine__c='TEST_PL';
        sampleProduct.ProductFamily__c='TEST_PF';
        sampleProduct.Family__c='TEST_FLMY';
        sampleProduct.TECH_PM0CodeInGMR__c = '11223344';
        sampleProduct.HierarchyType__c = 'PM0-FAMILY';
        sampleProduct.IsActive__c = true;
        Database.insert(sampleProduct);
        
        
        TeamProductJunction__c sampleProductCaseCls = new TeamProductJunction__c(CCTeam__c=sampleCCTeam.ID, productFamily__c = sampleProduct.ID);
        insert sampleProductCaseCls;  
                
        PageReference pageRef = Page.VFP88_SingleCaseTeamTransfer;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('retURL',testCase.ID);
        ApexPages.currentPage().getParameters().put('ID', testCase.ID);
       
        VCC12_ChangeCaseTeam changeCCTeamController = new VCC12_ChangeCaseTeam();
        changeCCTeamController.getDoInitialization();
        
        changeCCTeamController.searchCriteria = 'Sample';
        changeCCTeamController.displayCase.productBU__c= 'ENERGY';
        changeCCTeamController.displayCase.ProductLine__c= 'Product Line';
        changeCCTeamController.displayCase.ProductFamily__c= 'PF';
        changeCCTeamController.caseOrigin = 'Web';
        changeCCTeamController.CustomerClassification = 'EU';
        changeCCTeamController.ContactSCT = 'DAC';
        
        changeCCTeamController.search();
        System.debug('changeCCTeamController.resultList: '+ changeCCTeamController.resultList);
        if(changeCCTeamController.resultList != null && !changeCCTeamController.resultList.isEmpty()){
            System.debug('Vimal-1');
            DataTemplate__c firstResult = changeCCTeamController.resultList[0];
            pagereference pageResult = changeCCTeamController.performAction(firstResult,changeCCTeamController);
        }

        changeCCTeamController.cancel();
        
        changeCCTeamController.clearFilters();
        changeCCTeamController.displayError('Message 1','Message 2','top');
        changeCCTeamController.displayError('Message 1','Message 2','middle');

        List<Case> caseList = new List<Case>();
        caseList.add(testCase);
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(caseList);
        standardSetController.setSelected(caseList);
        VFC89_MassCaseTeamTransfer massCaseTransferController = new VFC89_MassCaseTeamTransfer(standardSetController );
        
        PageReference massTransferPage = page.VFP89_MassCaseTeamTransfer;   
        Test.setCurrentPage(massTransferPage);   
        ApexPages.currentPage().getParameters().put('retURL',testCase.ID);

        system.currentpagereference().getParameters().put('ID',null);
        
         
        VCC12_ChangeCaseTeam changeCCTeamControllerMulti = new VCC12_ChangeCaseTeam();
        
        changeCCTeamControllerMulti.CaseIDList_att = new List<ID>{testCase.ID};
        System.debug('VCC12.TEST - INFO - Case List ID for VCC12 = ' + changeCCTeamControllerMulti.CaseIDList_att);
        changeCCTeamControllerMulti.getDoInitialization();  
        changeCCTeamControllerMulti.search();
        if(changeCCTeamControllerMulti.resultList != null && !changeCCTeamControllerMulti.resultList.isEmpty()){
            DataTemplate__c firstResult = changeCCTeamControllerMulti.resultList[0];
            pagereference pageResult = changeCCTeamControllerMulti.performAction(firstResult,changeCCTeamControllerMulti);
        }
        
        
        VCC12_ChangeCaseTeam changeCCTeamController_SearchTab = new VCC12_ChangeCaseTeam();
        changeCCTeamController_SearchTab.searchTab = true;
        changeCCTeamController_SearchTab.getDoInitialization();
        changeCCTeamController_SearchTab.search();
        changeCCTeamController_SearchTab.actionNumber=1;
        if(changeCCTeamController_SearchTab.resultList != null && !changeCCTeamController_SearchTab.resultList.isEmpty()){
            DataTemplate__c firstResult = changeCCTeamController_SearchTab.resultList[0];
            pagereference pageResult = changeCCTeamController_SearchTab.performAction(firstResult,changeCCTeamController_SearchTab);
        }
        
        VCC12_ChangeCaseTeam changeCCTeamController_SearchTabAction2 = new VCC12_ChangeCaseTeam();
        changeCCTeamController_SearchTabAction2.searchTab = true;
        changeCCTeamController_SearchTabAction2.getDoInitialization();
        changeCCTeamController_SearchTabAction2.search();
        changeCCTeamController_SearchTabAction2.actionNumber=2;
        if(changeCCTeamController_SearchTabAction2.resultList != null && !changeCCTeamController_SearchTabAction2.resultList.isEmpty()){
            DataTemplate__c firstResult = changeCCTeamController_SearchTabAction2.resultList[0];
            pagereference pageResult = changeCCTeamController_SearchTabAction2.performAction(firstResult,changeCCTeamController_SearchTabAction2);
        }
        VCC12_ChangeCaseTeam changeCCTeamController_NewTeam = new VCC12_ChangeCaseTeam();      
        pagereference pageNewTeamResult = changeCCTeamController_NewTeam.newTeam();
    }
}