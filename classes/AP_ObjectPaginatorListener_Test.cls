global class AP_ObjectPaginatorListener_Test implements AP_ObjectPaginatorListener{
    global Boolean handlePageChangeInvoked {get;set;}
    
    global AP_ObjectPaginatorListener_Test(){
        handlePageChangeInvoked = false;
    }
    
    global void handlePageChange(List<Object> newPage){
        handlePageChangeInvoked = true;
    }
    
}