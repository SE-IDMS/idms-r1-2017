/**
*   Created By: akhilesh bagwari
*   Created Date: 01/06/2016
*   Modified by:  06/12/2016  
*   This is global REST API class to reset the user password (email or Mobile) via IFW.
**/

@RestResource(urlMapping='/IDMSPasswordRecovery/*')
global with sharing class IDMSPasswordRecoveryRest{
    
    //POST method will be called by API which will reset password of user.
    @HttpPost
    global static IDMSResponseWrapper doPost(User UserRecord){
        system.debug('UserRecord in reset pwd input: '+ UserRecord);        
        IDMSPasswordService pwdService  = new IDMSPasswordService();             
        IDMSResponseWrapper Response    = pwdService.resetPassworduser(UserRecord);
        RestResponse res                = RestContext.response;
        
        if(Response.Status.equalsignorecase('Error')){
            res.statuscode = integer.valueOf(Label.CLDEC16IDMS001);
            if(Response.Message.startsWith('Error in')){
                res.statuscode = integer.valueOf(Label.CLDEC16IDMS001);
            }
            if(Response.Message.startsWith('User not')){
                res.statuscode = integer.valueOf(Label.CLDEC16IDMS002);
            }
        }
        return Response;
    }
    
    //Put method will be called by API to update password using fed id. 
    @HttpPut
    global static IDMSResponseWrapper doPut(String NewPwd, String FederationIdentifier,String IDMS_Profile_update_source){
        system.debug('NewPwd: '+ NewPwd);
        system.debug('FederationIdentifier: '+ FederationIdentifier);
        system.debug('IDMS_Profile_update_source: '+ IDMS_Profile_update_source);       
        IDMSPasswordService pwdService  = new IDMSPasswordService();             
        IDMSResponseWrapper Response    = pwdService.updatePasswordFederation(NewPwd,FederationIdentifier,IDMS_Profile_update_source);
        RestResponse res                = RestContext.response;
        if(Response.Status.equalsignorecase('Error')){
            res.statuscode              = integer.valueOf(Label.CLDEC16IDMS001);
            if(Response.Message.startsWith('Error in')){
                res.statuscode          = integer.valueOf(Label.CLDEC16IDMS001);
            }
            if(Response.Message.startsWith('User not')){
                res.statuscode          = integer.valueOf(Label.CLDEC16IDMS002);
            }
        }
        return Response;
    } 
    
}