/*
    Description: Test Class for VFC_OpportunitySearch
*/
@isTest
private class VFC_OpportunitySearch_TEST
{
    static testMethod void testOpportunitySearch()
    {
        ApexPages.StandardController controller;
        VFC_OpportunitySearch oppSrch;        
        Account acc = Utils_TestMethods.createAccount();
        Account acc1 = Utils_TestMethods.createAccount();
        Opportunity opp2,opp3, opp4;
        Country__c newCountry;
        User u;
        OPP_Project__c prj;      
 
        OpportunityRegistrationform__c ORF;
        PartnerProgram__c countryPRG ;
        pageReference pr;
        
        //system.runas(new User(Id = UserInfo.getUserId()))
        //{
            acc.AccShortName__c = 'testShortName';
            insert acc;        
            insert acc1;
            
            newCountry = Utils_TestMethods.createCountry();
            newCountry.Name = 'FRANCE';
            insert newCountry; 
            
             prj =Utils_TestMethods.createMasterProject(acc.Id,newCountry.Id);
            insert prj;
            opp2 = Utils_TestMethods.createOpportunity(acc.Id);
            opp2.TECH_IsOpportunityUpdate__c  = true;
            opp2.Reason__c = 'test';
            opp2.OpptyType__c = 'Standard';    
            opp2.Name = 'TEst for Oppty more than 40 charsTEst for O';
            opp2.AccountId = acc.Id;
            opp2.EndUserCustomer__c= acc.Id;    
            insert opp2;
            pr = new PageReference('/006/e');
            pr.getParameters().put('OpptyType', Label.CLAPR14SLS25);
            pr.getParameters().put('prjId',prj.Id);
            Test.setCurrentPage(pr);
            controller = new ApexPages.StandardController(opp2);
            oppSrch = new VFC_OpportunitySearch(controller);
            
            opp3 = Utils_TestMethods.createOpportunity(acc1.Id);
            opp3.Name = 'ttttttt';
            opp3.TECH_IsOpportunityUpdate__c  = true;
            opp3.Reason__c = 'test';
            opp3.OpptyType__c = 'Solutions';        
            opp3.COuntryOfDestination__c = null;
            opp3.Location__c = 'testLocation';
            opp3.AccountId = acc1.Id;
            opp3.EndUserCustomer__c= acc.Id;    
            insert opp3;
            
            opp4 = Utils_TestMethods.createOpportunity(acc1.Id);
            opp4.TECH_IsOpportunityUpdate__c  = true;
            opp4.Reason__c = 'test';
            opp4.OpptyType__c = 'Solutions';        
            opp4.COuntryOfDestination__c = null;
            opp4.Location__c = 'testLocation';
            insert opp4;
                           
           /* u = Utils_TestMethods.createStandardUserWithNoCountry('ORF');
            u.BypassVR__c = true;
            u.country__c = newCountry.countrycode__c;
            u.UserRoleId=System.Label.CLDEC13SLS40;
            update u;*/
        //}

        //system.runAs(u)
        //{
            PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
            globalProgram.TECH_CountriesId__c = newCountry.id;
            globalPRogram.recordtypeid = Label.CLMAY13PRM15;
            globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
            insert globalProgram;
            
            ApexPages.currentPage().getParameters().put('GlobalPrgId',globalProgram.Id);
            ApexPages.currentPage().getParameters().put('type',Label.CLJUN13PRM07);
            ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(globalProgram);
            VFC_NewCountryProgram myPageCon1 = new VFC_NewCountryProgram(sdtCon1);
            myPageCon1.goToCountryPRGEditPage();
            
            countryPRG = [Select id,recordtypeid from PartnerProgram__c where Id= :globalProgram.Id limit 1];
            countryPRG.ProgramStatus__c = Label.CLMAY13PRM47;
            countryPRG.recordtypeId=Label.CLMAY13PRM05;
            update countryPRG;
            ORF = Utils_TestMethods.CreateORF(newCountry.Id);
            ORF.ProductType__c = 'test';
            ORF.PArtnerProgram__C  = countryPRG.Id;
            insert ORF;
       // }
        
        oppSrch.sortExpression = 'Name';
        oppSrch.sortDirection = 'ASC';
        oppSrch.openOpptys = true;
        oppSrch.search();        
        oppSrch.continueCreation();
        oppSrch.createOpportunity();  
        
        pr = new PageReference('/006/e');
        pr.getParameters().put('OpptyType', Label.CLAPR14SLS25);
        Test.setCurrentPage(pr);
        controller = new ApexPages.StandardController(opp3);
        oppSrch = new VFC_OpportunitySearch(controller);
        oppSrch.sortExpression = 'Name';
        oppSrch.sortDirection = 'DESC';
        oppSrch.search();
        oppSrch.checkNAMWizardRedirection();                 
                          
        pr = new PageReference('/006/e');
        pr.getParameters().put('OpptyType', Label.CLAPR14SLS25);
        Test.setCurrentPage(pr);
        controller = new ApexPages.StandardController(opp4);
        oppSrch = new VFC_OpportunitySearch(controller);
        oppSrch.sortExpression = 'Name';
        oppSrch.sortDirection = 'DESC';
        oppSrch.search();
                  
        pr = new PageReference('/006/e');
        pr.getParameters().put('oppRegFormId',ORF.Id);
        pr.getParameters().put('save_new','test');
        pr.getParameters().put('OpptyType', Label.CLAPR14SLS25);
        Test.setCurrentPage(pr);
        //system.runas(new User(Id = UserInfo.getUserId()))
        //{
            controller = new ApexPages.StandardController(opp3);        
            oppSrch = new VFC_OpportunitySearch(controller);
            oppSrch.selectedOpportunityId = opp3.Id;
            oppSrch.sortExpression = 'Name';
            oppSrch.sortDirection = 'DESC';
            oppSrch.search();
            oppSrch.continueCreation();                                      
            oppSrch.createOpportunity();         
            oppSrch.selectOpportunityForORF();
        //}             
    }
}