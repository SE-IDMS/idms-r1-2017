@isTest
private class VFC_OpenCCCActions_Test{
    public static testMethod void testOpenCCCAction(){
        System.debug('#### VFC_OpenCCCActions_Test.testOpenCCCAction Started ####');
        
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCaseForAction;
        
        CCCAction__c objAction = Utils_TestMethods.createCCCAction(objCaseForAction.Id);
        insert objAction;
        
        PageReference pageRef = Page.VFP_OpenCCCActions;
        Test.setCurrentPage(pageRef);
        VFC_OpenCCCActions controller = new VFC_OpenCCCActions();
        controller.getCCCActions();
        System.debug('#### VFC_OpenCCCActions_Test.testOpenCCCAction Ends ####');
    }
}