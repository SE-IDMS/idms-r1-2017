/*************************************************************************************************************

    DATE          CREATED BY      PURPOSE..
    20/Nov/13     Deepak          For The Purpose of Creating IB Coverege Automatically in Service Contract..
    
    
************************************************************************************************************ */

public with sharing class VFC_CreateCoveredProduct
{
   // public VFC_CreateCoveredProduct(ApexPages.StandardController controller) {}

    set<Id> scId = New Set<Id>();
    List<SVMXC__Service_Contract__c> sclist =New List<SVMXC__Service_Contract__c>();
    
    boolean isSuccess = false;
    public  boolean getisSuccess(){return isSuccess;}
    
    boolean closeWindow = false;
    public  boolean getcloseWindow(){return closeWindow;}
    
    
    
    string message;
    public string getmessage(){return message;} 
    
    string displayImage = '';
    public string getdisplayImage() {return displayImage;}
    
    boolean isValidated = false;
    public boolean getisValidated (){return this.isValidated;}
            
    public void doProcess()
    {
      displayImage = 'none';
      isValidated = true;
      
        try
        {
             System.debug('******************************************************************'); 
             String st =   ApexPages.currentPage().getParameters().get('ID');
             
             sclist= [select id, SVMXC__Start_Date__c,SVMXC__End_Date__c from SVMXC__Service_Contract__c 
                      where id = :st];
             
      List<SVMXC__Service_Contract_Services__c> IsList = New List<SVMXC__Service_Contract_Services__c>();
      List<SVMXC__Service_Contract_Products__c> scplist = New List<SVMXC__Service_Contract_Products__c>();
      List<AssociatedInstalledProduct__c> AIpList = New List<AssociatedInstalledProduct__c>();
      
      IsList=[SELECT Id FROM SVMXC__Service_Contract_Services__c WHERE SVMXC__Service_Contract__c =:st ];
      List<SVMXC__Service_Contract_Products__c> scpToUpdate = New List<SVMXC__Service_Contract_Products__c>();
      
      
      AIpList = [Select InstalledProduct__r.Id,Name From AssociatedInstalledProduct__c Where ServiceMaintenanceContract__c =:st];
            
          set<string> s = New set<string>();
  
          for(SVMXC__Service_Contract_Services__c is :IsList)
          {   
              for(AssociatedInstalledProduct__c aip :AIpList)
              {   
                  for(SVMXC__Service_Contract__c sc :sclist)  
                  {  
                      s.add(String.valueOf(sc.Id).substring(0,15)+'-'+String.valueOf(is.Id).substring(0,15)+'-'+String.valueOf(aip.InstalledProduct__r.Id).substring(0,15));
                      system.debug('1s:'+s);
                  }
  
              }
          }
          scpToUpdate=[Select Id,Name,Tech_SetOfId__c,SVMXC__Installed_Product__c,IncludedService__c,SVMXC__Service_Contract__c From SVMXC__Service_Contract_Products__c Where Tech_SetOfId__c in :s];
        
          set<String> extStr = new Set<String>();
          if(scpToUpdate.size()>0)
          {
              for(SVMXC__Service_Contract_Products__c cp :scpToUpdate)
              {
                  extStr.add(cp.Tech_SetOfId__c);
              }
          }
          if(IsList.size()>0 && AIpList.size()>0 && sclist.size()>0)
          {
              for(SVMXC__Service_Contract_Services__c is :IsList)
              {   
                  for(AssociatedInstalledProduct__c aip :AIpList)
                  {   
                      for(SVMXC__Service_Contract__c sc :sclist)  
                      {             
                          SVMXC__Service_Contract_Products__c scp = New SVMXC__Service_Contract_Products__c();
                          scp.IncludedService__c=is.Id;
                          scp.SVMXC__Installed_Product__c=aip.InstalledProduct__r.Id;
                          scp.SVMXC__Service_Contract__c=sc.Id;
                          scp.SVMXC__Start_Date__c =sc.SVMXC__Start_Date__c;
                          scp.SVMXC__End_Date__c = sc.SVMXC__End_Date__c;
                          
                          if(!extStr.contains(String.valueOf(scp.SVMXC__Service_Contract__c).substring(0,15)+'-'+String.valueOf(scp.IncludedService__c).substring(0,15)+'-'+String.valueOf(scp.SVMXC__Installed_Product__c).substring(0,15)))
                          scplist.add(scp);
                        }
                  }
              }
          }
            if(scplist.size()>0)
              insert scplist;
                message = 'IB Coverages have been created successfully';
                isSuccess = true;
        }
        catch (exception ex)
     {
       isSuccess = false;
       message = 'Fail';
       apexpages.addmessage(new apexpages.message(ApexPages.severity.ERROR, ex.getMessage() ));
     }
    }
    
    
    public pageReference doclose()
    {
    closeWindow = true;
        return null;
    }
}