/**
     Author: Pooja Gupta
     Date: 02-May-2013
     Description: Test class for VFC_MultiselectController
     Release: May 2013 
 */
 
@isTest
private class VFC_MultiselectController_TEST
{
    static testMethod void testMultiselectExampleController()
    {
           List<Country__c> Countrylst = new list<Country__c>();
            For(integer c=0; c<10 ; c++)
            {
                Country__c country = new country__c(Name = 'TestCountry'+c,CountryCode__c ='XY'+c);
                Countrylst.add(country);
            }
            Database.SaveResult[] CountryListInsert= Database.insert(Countrylst, true);
             
             
             User User = Utils_TestMethods.createStandardUserWithNoCountry('PPRG2');
             User.BypassVR__c = true;
             User.country__c = Countrylst[0].countrycode__c;
             user.BypassWF__c = false;
             //User.PartnerORFConverter__c = true;
             //User.ProgramOwner__c = true;
             //User.ProgramAdministrator__c = true;
             //User.ProgramApprover__c = true;
             //User.ProgramManager__c = true;
             //Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
             //Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName(); 
             
             insert User;
                             
             system.runas(user)
             {
                List<Contact> l = new List<Contact>();
                for (Integer i = 0; i < 10; i++)
                {
                    Contact c = new Contact(firstName = 'User'+i, lastName = 'Name'+i,Email='test@test.com');
                    l.add(c);
                }
                insert l;
                
               
                Account Acc = Utils_TestMethods.createAccount();
                insert Acc;    
            
                PartnerProgram__c prog1 = Utils_TestMethods.createPartnerProgram();
                prog1.TECH_CountriesId__c = Countrylst[0].id;
                prog1.TargetCountriesTxt__c = 'Testtext';
       
                insert prog1;
                
                List<PartnerProgramCountries__c> progCountries = New list<PartnerProgramCountries__c>();
                For(integer i=0; i< 10; i++)
                {
                   PartnerProgramCountries__c ptrcountry = new PartnerProgramCountries__c(PartnerProgram__c = prog1.id);
                   
                   if(math.mod(i,2)!=0)
                       ptrcountry.Country__c = Countrylst[0].id;
                   else
                       ptrcountry.Country__c = Countrylst[1].id;
                   
                   progCountries.add(ptrcountry);
                }
                
                Database.SaveResult[] ptrcountriesInsert= Database.insert(progCountries, true);
                
                Apexpages.Standardcontroller controller1 = new Apexpages.Standardcontroller(prog1);
                VFC_MultiselectController vfcon = new VFC_MultiselectController(controller1);
                Pagereference pg1 = Page.VFP_MultiselectController;
                Test.setCurrentPage(pg1);
               
                List<SelectOption> i = new list<SelectOption>();
               
                    vfcon.selPartnerProgs = vfcon.allPartnerProgs;
                    vfcon.save();
            }
     }
}