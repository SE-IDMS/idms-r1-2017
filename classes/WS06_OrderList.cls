//Generated by wsdl2apex

public class WS06_OrderList {
    public class ServiceExpectedException extends Exception{
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class orderStatusList {
        public WS06_OrderList.orderStatusListSearchDataBean arg0;
        private String[] arg0_type_info = new String[]{'arg0','http://bridgefrontoffice.schneider-electric.com','orderStatusListSearchDataBean','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'arg0'};
    }
    public class genericBean {
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class orderStatusListOrderDataBean {
        public String column;
        public String orderBy;
        private String[] column_type_info = new String[]{'column','http://bridgefrontoffice.schneider-electric.com','orderStatusColumn','0','1','false'};
        private String[] orderBy_type_info = new String[]{'orderBy','http://bridgefrontoffice.schneider-electric.com','orderBy','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'column','orderBy'};
    }
    public class orderStatusListResponse {
        public WS06_OrderList.orderStatusResult return_x;
        private String[] return_x_type_info = new String[]{'return','http://bridgefrontoffice.schneider-electric.com','orderStatusResult','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class OrderStatusListPort {
        public String endpoint_x = 'http://bemint.dev.schneider-electric.com/myseremoteservice/OrderStatusListService';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com', 'WS06_OrderList'};
        public WS06_OrderList.orderStatusResult orderStatusList(WS06_OrderList.orderStatusListSearchDataBean arg0) {
            WS06_OrderList.orderStatusList request_x = new WS06_OrderList.orderStatusList();
            WS06_OrderList.orderStatusListResponse response_x;
            request_x.arg0 = arg0;
            Map<String, WS06_OrderList.orderStatusListResponse> response_map_x = new Map<String, WS06_OrderList.orderStatusListResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://bridgefrontoffice.schneider-electric.com',
              'orderStatusList',
              'http://bridgefrontoffice.schneider-electric.com',
              'orderStatusListResponse',
              'WS06_OrderList.orderStatusListResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
    }
    public class orderStatusResult {
        public WS06_OrderList.orderStatusDataBean[] orderList;
        public Long rangeEnd;
        public Long rangeStart;
        public WS06_OrderList.returnDataBean returnInfo;
        public Long totalNumberOfPojos;
        private String[] orderList_type_info = new String[]{'orderList','http://bridgefrontoffice.schneider-electric.com','orderStatusDataBean','0','-1','true'};
        private String[] rangeEnd_type_info = new String[]{'rangeEnd','http://www.w3.org/2001/XMLSchema','long','0','1','false'};
        private String[] rangeStart_type_info = new String[]{'rangeStart','http://www.w3.org/2001/XMLSchema','long','0','1','false'};
        private String[] returnInfo_type_info = new String[]{'returnInfo','http://bridgefrontoffice.schneider-electric.com','returnDataBean','0','1','false'};
        private String[] totalNumberOfPojos_type_info = new String[]{'totalNumberOfPojos','http://www.w3.org/2001/XMLSchema','long','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'orderList','rangeEnd','rangeStart','returnInfo','totalNumberOfPojos'};
    }
    public class ServiceFatalException extends Exception{
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class orderStatusListSearchDataBean {
        public String bfoAccountId;
        public String catalogNumber;
        public WS06_OrderList.orderStatusListOrderDataBean[] orderByList;
        public String partialOrderNumber;
        public String partialPONumber;
        public Long rangeEnd;
        public Long rangeStart;
        public DateTime releaseDateFrom;
        public DateTime releaseDateTo;
        public String[] sdhLegacyPSName;
        private String[] bfoAccountId_type_info = new String[]{'bfoAccountId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] catalogNumber_type_info = new String[]{'catalogNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] orderByList_type_info = new String[]{'orderByList','http://bridgefrontoffice.schneider-electric.com','orderStatusListOrderDataBean','0','-1','true'};
        private String[] partialOrderNumber_type_info = new String[]{'partialOrderNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] partialPONumber_type_info = new String[]{'partialPONumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] rangeEnd_type_info = new String[]{'rangeEnd','http://www.w3.org/2001/XMLSchema','long','0','1','false'};
        private String[] rangeStart_type_info = new String[]{'rangeStart','http://www.w3.org/2001/XMLSchema','long','0','1','false'};
        private String[] releaseDateFrom_type_info = new String[]{'releaseDateFrom','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] releaseDateTo_type_info = new String[]{'releaseDateTo','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] sdhLegacyPSName_type_info = new String[]{'sdhLegacyPSName','http://www.w3.org/2001/XMLSchema','string','0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'bfoAccountId','catalogNumber','orderByList','partialOrderNumber','partialPONumber','rangeEnd','rangeStart','releaseDateFrom','releaseDateTo','sdhLegacyPSName'};
    }
    public class returnDataBean {
        public String returnCode;
        public String returnMessage;
        private String[] returnCode_type_info = new String[]{'returnCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] returnMessage_type_info = new String[]{'returnMessage','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'returnCode','returnMessage'};
    }
    public class orderStatusDataBean {
        public String currencyCode;
        public String enteringUserName;
        public Boolean expressDelivery;
        public String legacyName;
        public String mySEOrderDetailUrl;
        public String mySEStatus;
        public String orderNumber;
        public String orderPlacedThrough;
        public String poNumber;
        public DateTime releaseDate;
        public DateTime requestedDeliveryDate;
        public String shipToName;
        public String soldToName;
        public Double totalAmount;
        private String[] currencyCode_type_info = new String[]{'currencyCode','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] enteringUserName_type_info = new String[]{'enteringUserName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] expressDelivery_type_info = new String[]{'expressDelivery','http://www.w3.org/2001/XMLSchema','boolean','0','1','false'};
        private String[] legacyName_type_info = new String[]{'legacyName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] mySEOrderDetailUrl_type_info = new String[]{'mySEOrderDetailUrl','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] mySEStatus_type_info = new String[]{'mySEStatus','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] orderNumber_type_info = new String[]{'orderNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] orderPlacedThrough_type_info = new String[]{'orderPlacedThrough','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] poNumber_type_info = new String[]{'poNumber','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] releaseDate_type_info = new String[]{'releaseDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] requestedDeliveryDate_type_info = new String[]{'requestedDeliveryDate','http://www.w3.org/2001/XMLSchema','dateTime','0','1','false'};
        private String[] shipToName_type_info = new String[]{'shipToName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] soldToName_type_info = new String[]{'soldToName','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] totalAmount_type_info = new String[]{'totalAmount','http://www.w3.org/2001/XMLSchema','double','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://bridgefrontoffice.schneider-electric.com','false','false'};
        private String[] field_order_type_info = new String[]{'currencyCode','enteringUserName','expressDelivery','legacyName','mySEOrderDetailUrl','mySEStatus','orderNumber','orderPlacedThrough','poNumber','releaseDate','requestedDeliveryDate','shipToName','soldToName','totalAmount'};
    }
}