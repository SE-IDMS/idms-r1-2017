/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 02/11/2010
    Description     : Test class for Controller VFC08_OASTemplate.
*/

@isTest
private class VFC08_OPQTemplate_TEST {
    private static final String FEASIBILITY = 'Feasibility';
    private static final String ATTRACTIVENESS = 'Attractiveness';
    private static final String PC = 'Project Category';
    private static final String GATE = 'S0';
    private static final String GATES1 = 'S1';
    private static final String GATEP0 = 'P0';
    private static final String DecisionCriteria = 'DC'; 
    private static final String ProjectCategoryTool = 'PCT'; 
    
    static testMethod void testOpportunityScoring(){ 
    OPQ_AssessmentSection__c feasibility_section = new OPQ_AssessmentSection__c(Gate__c = GATE, Axis__c = FEASIBILITY);
        insert feasibility_section;
    OPQ_AssessmentSection__c feasibility_section_1 = new OPQ_AssessmentSection__c(Gate__c = GATE, Axis__c = FEASIBILITY);
        insert feasibility_section_1;
    List<OPQ_AssessmentQuestion__c> feasibility_questions = new List<OPQ_AssessmentQuestion__c>();
        for(Integer i = 0; i < 5; i++){
            OPQ_AssessmentQuestion__c q;
                q = new OPQ_AssessmentQuestion__c(Assessment__c = feasibility_section.Id,Active__c=True);
            feasibility_questions.add(q);
        }
        insert feasibility_questions;
    
    OPQ_AssessmentSection__c attractiveness_section = new OPQ_AssessmentSection__c(Gate__c = GATE, Axis__c = ATTRACTIVENESS);
        insert attractiveness_section;
    OPQ_AssessmentQuestion__c q = new OPQ_AssessmentQuestion__c(Assessment__c = attractiveness_section.Id);
        insert q;
    
    
    
    List<OPQ_AssessmentSection__c> section_S0 = new List<OPQ_AssessmentSection__c>();
        section_S0.add(new OPQ_AssessmentSection__c(Name = 'Project', Axis__c = PC, Gate__c = 'S0'));
        section_S0.add(new OPQ_AssessmentSection__c(Name = 'Value Chain', Axis__c = PC, Gate__c = 'S0'));
        section_S0.add(new OPQ_AssessmentSection__c(Name = 'Design', Axis__c = PC, Gate__c = 'S0'));
        insert section_S0;
        
    List<OPQ_AssessmentSection__c> section_S1 = new List<OPQ_AssessmentSection__c>();
        section_S1.add(new OPQ_AssessmentSection__c(Name = 'Project', Axis__c = PC, Gate__c = 'S1'));
        section_S1.add(new OPQ_AssessmentSection__c(Name = 'Risk', Axis__c = PC, Gate__c = 'S1'));
        section_S1.add(new OPQ_AssessmentSection__c(Name = 'Value Chain', Axis__c = PC, Gate__c = 'S1'));
        section_S1.add(new OPQ_AssessmentSection__c(Name = 'Design', Axis__c = PC, Gate__c = 'S1'));
        insert section_S1;
        
    List<OPQ_AssessmentSection__c> section_P0 = new List<OPQ_AssessmentSection__c>();
        section_P0.add(new OPQ_AssessmentSection__c(Name = 'Project', Axis__c = PC, Gate__c = 'P0'));
        section_P0.add(new OPQ_AssessmentSection__c(Name = 'Risk', Axis__c = PC, Gate__c = 'P0'));
        section_P0.add(new OPQ_AssessmentSection__c(Name = 'Value Chain', Axis__c = PC, Gate__c = 'P0'));
        section_P0.add(new OPQ_AssessmentSection__c(Name = 'Design', Axis__c = PC, Gate__c = 'P0'));
        insert section_P0;
    
    
    List<OPQ_AssessmentQuestion__c> ques_S0 = new List<OPQ_AssessmentQuestion__c>();
        ques_S0.add(new OPQ_AssessmentQuestion__c(Name = 'Value', Assessment__c = section_S0.get(0).Id, Active__c=True));
        ques_S0.add(new OPQ_AssessmentQuestion__c(Name = 'Solution', Assessment__c = section_S0.get(0).Id, Active__c=True));
        ques_S0.add(new OPQ_AssessmentQuestion__c(Name = 'Customer', Assessment__c = section_S0.get(1).Id, Active__c=True));
        ques_S0.add(new OPQ_AssessmentQuestion__c(Name = 'Technology', Assessment__c = section_S0.get(2).Id, Active__c=True));
        ques_S0.add(new OPQ_AssessmentQuestion__c(Name = 'Content', Assessment__c = section_S0.get(2).Id, Active__c=True));
        ques_S0.add(new OPQ_AssessmentQuestion__c(Name = 'Business Model', Assessment__c = section_S0.get(2).Id, Active__c=True));
        insert ques_S0;

    List<OPQ_AssessmentQuestion__c> ques_S1 = new List<OPQ_AssessmentQuestion__c>();
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Value', Assessment__c = section_S1.get(0).Id, Active__c=True));
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Duration', Assessment__c = section_S1.get(0).Id, Active__c=True));
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Billing', Assessment__c = section_S1.get(0).Id, Active__c=True));
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Solution', Assessment__c = section_S1.get(0).Id, Active__c=True));
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Contract', Assessment__c = section_S1.get(1).Id, Active__c=True));
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Schedule', Assessment__c = section_S1.get(1).Id, Active__c=True));
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Customer', Assessment__c = section_S1.get(2).Id, Active__c=True));
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Suppliers', Assessment__c = section_S1.get(2).Id, Active__c=True));
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Technology', Assessment__c = section_S1.get(3).Id, Active__c=True));
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Content', Assessment__c = section_S1.get(3).Id, Active__c=True));
        ques_S1.add(new OPQ_AssessmentQuestion__c(Name = 'Business Model', Assessment__c = section_S1.get(3).Id, Active__c=True));
        insert ques_S1;
    
    List<OPQ_AssessmentQuestion__c> ques_P0 = new List<OPQ_AssessmentQuestion__c>();
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Value', Assessment__c = section_P0.get(0).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Duration', Assessment__c = section_P0.get(0).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Billing', Assessment__c = section_P0.get(0).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Solution', Assessment__c = section_P0.get(0).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Contract', Assessment__c = section_P0.get(1).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Financial', Assessment__c = section_P0.get(1).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Schedule', Assessment__c = section_P0.get(2).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Customer', Assessment__c = section_P0.get(2).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Suppliers', Assessment__c = section_P0.get(2).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Technology', Assessment__c = section_P0.get(3).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Content', Assessment__c = section_P0.get(3).Id, Active__c=True));
        ques_P0.add(new OPQ_AssessmentQuestion__c(Name = 'Business Model', Assessment__c = section_P0.get(3).Id, Active__c=True));
        insert ques_P0;
        
        Account a = Utils_TestMethods.createAccount();
        insert a;
        Opportunity o = Utils_TestMethods.createOpportunity(a.Id);
        o.StageName = '3 - Identify & Qualify';                         //ensure TECH_Gate__c formula field is equal to S0
        
        insert o;
        
        o = [select Id, TECH_Gate__c, TECH_PCTGate__c, ProjectCategoryS0__c, ProjectCategoryS1__c, ProjectCategoryP0__c, OpptyType__c  from Opportunity where Id =: o.Id];
        
        
        /*
        *   Initialise controller - DECISION CRITERIA (FEASIBILITY) AXIS
        */
        System.debug('****---------DECISION CRITERIA AXIS---------*****');
        PageReference pageRef = Page.VFP08_OPQTemplate;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', o.Id);
        ApexPages.currentPage().getParameters().put('Axis', DecisionCriteria);
        ApexPages.StandardController sc = new ApexPages.StandardController(o);
        VFC08_OPQTemplate c = new VFC08_OPQTemplate(sc);
        
        //Pre-checks
        List<OPQ_OpportunityQuestion__c> questions = [select id from OPQ_OpportunityQuestion__c where Opportunity__c = :o.Id 
                                                        and Active__c=:Label.CL00074  
                                                        and AssessmentQuestion__r.Assessment__r.TECH_Axis__c = :DecisionCriteria
                                                        and AssessmentQuestion__r.Assessment__r.Gate__c = :GATE];
        //System.assertEquals(0, questions.size()); 
        
        //Simulate page entry view
        c.createQuestions();
               
        //Check results
        questions = [select id,Active__c from OPQ_OpportunityQuestion__c where Opportunity__c = :o.Id 
                            and Active__c = :Label.CL00074  
                            and AssessmentQuestion__r.Assessment__r.TECH_Axis__c = :DecisionCriteria
                            and AssessmentQuestion__r.Assessment__r.Gate__c = :GATE];
    
        //System.assert(questions.size() > 0);
        //Additional method calls for coverage
        c.save(); 
        
        c.getScores();
        OPQ_OpportunityQuestion__c oppQues1 = new OPQ_OpportunityQuestion__c( Opportunity__c = o.Id,Gate__c=GATE,Active__c=Label.CL00074 ,AssessmentQuestion__c=feasibility_questions[1].Id); 
        OPQ_OpportunityQuestion__c oppQues2 = new OPQ_OpportunityQuestion__c( Opportunity__c = o.Id,Gate__c=GATE,Active__c=Label.CL00074 ,AssessmentQuestion__c=feasibility_questions[2].Id);
        OPQ_OpportunityQuestion__c oppQues3 = new OPQ_OpportunityQuestion__c( Opportunity__c = o.Id,Gate__c=GATE,Active__c=Label.CL00074 ,AssessmentQuestion__c=feasibility_questions[3].Id);
        List<OPQ_OpportunityQuestion__c> quest = new OPQ_OpportunityQuestion__c[]{oppQues1,oppQues2,oppQues3};
        Database.insert(quest); 
        
        /*
        *   Initialise controller - DECISION CRITERIA (ATTRACTIVENESS) AXIS
        */
        
       // ApexPages.currentPage().getParameters().put('Axis', ATTRACTIVENESS);
        sc = new ApexPages.StandardController(o);
        c = new VFC08_OPQTemplate(sc);
          
        questions = [select id from OPQ_OpportunityQuestion__c where Opportunity__c = :o.Id 
                            and Active__c=:Label.CL00074  
                            and AssessmentQuestion__r.Assessment__r.TECH_Axis__c = :DecisionCriteria
                            and AssessmentQuestion__r.Assessment__r.Gate__c = :GATE];
                            
      // System.assertEquals(0, questions.size());
        
        //Simulate page entry view
        c.createQuestions();
        
        questions = [select id from OPQ_OpportunityQuestion__c where Opportunity__c = :o.Id 
                            and Active__c=:Label.CL00074   
                            and AssessmentQuestion__r.Assessment__r.TECH_Axis__c = :DecisionCriteria
                            and AssessmentQuestion__r.Assessment__r.Gate__c = :GATE];
        
        //System.assert(questions.size() > 0);
            
        //Additional method calls for coverage
        c.bTestFlag = true;                         //flag used to simulate error on update/insert to enforce complete coverage
        c.save(); 
        
        //Ensure coverage in simulated error condition on creating questions
        delete questions;

        
/*
        *   Initialise controller - PROJECT CATEGORY TOOL AXIS
        */
        System.debug('****---------PROJECT CATEGORY TOOL AXIS---------*****');
        PageReference pageRef1 = Page.VFP08_OPQTemplate;
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('id', o.Id);  
        ApexPages.currentPage().getParameters().put('Axis', ProjectCategoryTool);
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(o);
        VFC08_OPQTemplate c1 = new VFC08_OPQTemplate(sc1);
        
        //Pre-checks
        List<OPQ_OpportunityQuestion__c> questionsPCTS0 = [select id from OPQ_OpportunityQuestion__c where Opportunity__c = :o.Id 
                                                        and Active__c=:Label.CL00074  
                                                        and AssessmentQuestion__r.Assessment__r.TECH_Axis__c = :ProjectCategoryTool
                                                        and AssessmentQuestion__r.Assessment__r.Gate__c = :GATE];
        //System.assertEquals(0, questionsPCTS0.size()); 
        
        //Simulate page entry view
        c1.createQuestions();
               
        //Check results
        questionsPCTS0 = [select id,Active__c,Score__c,AssessmentQuestion__r.Name from OPQ_OpportunityQuestion__c where Opportunity__c = :o.Id 
                            and Active__c = :Label.CL00074  
                            and AssessmentQuestion__r.Assessment__r.TECH_Axis__c = :ProjectCategoryTool
                            and AssessmentQuestion__r.Assessment__r.Gate__c = :GATE];
                
       //System.assert(questionsPCTS0.size() > 0);
       //System.debug('--------->>>>>questionsPCTS0----'+questionsPCTS0[0].AssessmentQuestion__r.Name+'----'+questionsPCTS0[1].AssessmentQuestion__r.Name+'----'+questionsPCTS0[2].AssessmentQuestion__r.Name+'----'+questionsPCTS0[3].AssessmentQuestion__r.Name+'----'+questionsPCTS0[4].AssessmentQuestion__r.Name+'----'+questionsPCTS0[5].AssessmentQuestion__r.Name);
        
               
        //Additional method calls for coverage
        c1.save(); 
        c1.getScores();
       if(questionsPCTS0.size() > 0){
        System.debug('***For ProjectCategoryS0__c = B***');
        questionsPCTS0[0].Score__c='1';
        //System.debug('--------->>>>>questionsPCTS0[0]'+questionsPCTS0[0].AssessmentQuestion__r.Name);
        update(questionsPCTS0);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        c1.save(); 
        //System.debug('------->>>>>>>>>ProjectCategoryS0__c'+o.ProjectCategoryS0__c);    
        
        System.debug('***For ProjectCategoryS0__c = A***');
        questionsPCTS0[1].Score__c='2';
        //System.debug('--------->>>>>questionsPCTS0[1]'+questionsPCTS0[1].AssessmentQuestion__r.Name);
        questionsPCTS0[5].Score__c='4';
       // System.debug('--------->>>>>questionsPCTS0[5]'+questionsPCTS0[5].AssessmentQuestion__r.Name);
        update(questionsPCTS0);
        //System.debug('--------->>>>>questionsPCTS0'+questionsPCTS0);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        
    //  c1.bTestFlag = true;                         //flag used to simulate error on update/insert to enforce complete coverage
        c1.save(); 
        //System.debug('------->>>>>>>>>ProjectCategoryS0__c'+o.ProjectCategoryS0__c);
        
        System.debug('***For ProjectCategoryS0__c = C***');
        questionsPCTS0[1].Score__c='1';
        //System.debug('--------->>>>>questionsPCTS0[1]'+questionsPCTS0[1].AssessmentQuestion__r.Name);
        questionsPCTS0[2].Score__c='2';
       // System.debug('--------->>>>>questionsPCTS0[2]'+questionsPCTS0[2].AssessmentQuestion__r.Name);        
        questionsPCTS0[3].Score__c='1';
        //System.debug('--------->>>>>questionsPCTS0[3]'+questionsPCTS0[3].AssessmentQuestion__r.Name);
        questionsPCTS0[4].Score__c='2';
        //System.debug('--------->>>>>questionsPCTS0[4]'+questionsPCTS0[4].AssessmentQuestion__r.Name);
        questionsPCTS0[5].Score__c='1';
       // System.debug('--------->>>>>questionsPCTS0[5]'+questionsPCTS0[5].AssessmentQuestion__r.Name);
        update(questionsPCTS0);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        c1.save(); 
        //System.debug('------->>>>>>>>>ProjectCategoryS0__c'+o.ProjectCategoryS0__c);
        
        delete(questionsPCTS0);
    } 
        // For GATE S1
        
        o.StageName = '4 - Influence & Develop';                         //ensure TECH_Gate__c formula field is equal to S1 
        update o;
        o = [select Id, TECH_Gate__c, TECH_PCTGate__c, ProjectCategoryS0__c, ProjectCategoryS1__c, ProjectCategoryP0__c, OpptyType__c  from Opportunity where Id =: o.Id];
        //System.debug('----->>>>>>>Tech_PCTGate__c'+o.TECH_PCTGate__c);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        
        List<OPQ_OpportunityQuestion__c> questionsPCTS1 = [select id from OPQ_OpportunityQuestion__c where Opportunity__c = :o.Id 
                                                        and Active__c=:Label.CL00074  
                                                        and AssessmentQuestion__r.Assessment__r.TECH_Axis__c = :ProjectCategoryTool
                                                        and AssessmentQuestion__r.Assessment__r.Gate__c = :GATES1];
        //System.assertEquals(0, questionsPCTS1.size()); 
        
        //Simulate page entry view
        c1.createQuestions();
               
        //Check results
        questionsPCTS1 = [select id,Active__c,Score__c,AssessmentQuestion__r.Name from OPQ_OpportunityQuestion__c where Opportunity__c = :o.Id 
                            and Active__c = :Label.CL00074  
                            and AssessmentQuestion__r.Assessment__r.TECH_Axis__c = :ProjectCategoryTool
                            and AssessmentQuestion__r.Assessment__r.Gate__c = :GATES1];
                
       //System.assert(questionsPCTS1.size() > 0);
       //System.debug('--------->>>>>questionsPCTS1----'+questionsPCTS1[0].AssessmentQuestion__r.Name+'----'+questionsPCTS1[1].AssessmentQuestion__r.Name+'----'+questionsPCTS1[2].AssessmentQuestion__r.Name+'----'+questionsPCTS1[3].AssessmentQuestion__r.Name+'----'+questionsPCTS1[4].AssessmentQuestion__r.Name+'----'+questionsPCTS1[5].AssessmentQuestion__r.Name+'----'+questionsPCTS1[6].AssessmentQuestion__r.Name+'----'+questionsPCTS1[7].AssessmentQuestion__r.Name+'----'+questionsPCTS1[8].AssessmentQuestion__r.Name+'----'+questionsPCTS1[9].AssessmentQuestion__r.Name+'----'+questionsPCTS1[10].AssessmentQuestion__r.Name);
        if(questionsPCTS1.size() > 0){
               
        //Additional method calls for coverage
        c1.save(); 
       
        System.debug('***For ProjectCategoryS1__c = B***');
        questionsPCTS1[2].Score__c='1';
        //System.debug('--------->>>>>questionsPCTS1[2]'+questionsPCTS1[2].AssessmentQuestion__r.Name);
        update(questionsPCTS1);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        c1.save(); 
        //System.debug('------->>>>>>>>>ProjectCategoryS1__c'+o.ProjectCategoryS1__c);    
                
        System.debug('***For ProjectCategoryS1__c = C***');
        questionsPCTS1[3].Score__c='1'; 
        //System.debug('--------->>>>>questionsPCTS1[3]'+questionsPCTS1[3].AssessmentQuestion__r.Name);
        questionsPCTS1[1].Score__c='1';
       // System.debug('--------->>>>>questionsPCTS1[1]'+questionsPCTS1[1].AssessmentQuestion__r.Name);   
        questionsPCTS1[5].Score__c='1';
       // System.debug('--------->>>>>questionsPCTS1[5]'+questionsPCTS1[5].AssessmentQuestion__r.Name); 
        questionsPCTS1[4].Score__c='1';
        //System.debug('--------->>>>>questionsPCTS1[4]'+questionsPCTS1[4].AssessmentQuestion__r.Name);
        questionsPCTS1[6].Score__c='1';
        //System.debug('--------->>>>>questionsPCTS1[6]'+questionsPCTS1[6].AssessmentQuestion__r.Name);
        questionsPCTS1[7].Score__c='1';
       // System.debug('--------->>>>>questionsPCTS1[7]'+questionsPCTS1[7].AssessmentQuestion__r.Name);
        questionsPCTS1[10].Score__c='1';
        //System.debug('--------->>>>>questionsPCTS1[10]'+questionsPCTS1[10].AssessmentQuestion__r.Name);
        questionsPCTS1[9].Score__c='1';
       // System.debug('--------->>>>>questionsPCTS1[9]'+questionsPCTS1[9].AssessmentQuestion__r.Name);
        questionsPCTS1[8].Score__c='1';
       // System.debug('--------->>>>>questionsPCTS1[8]'+questionsPCTS1[8].AssessmentQuestion__r.Name);
        update(questionsPCTS1);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        c1.save();
        //System.debug('------->>>>>>>>>ProjectCategoryS1__c'+o.ProjectCategoryS1__c);
        
        System.debug('***For ProjectCategoryS1__c = A***');
        questionsPCTS1[3].Score__c='2'; 
       // System.debug('--------->>>>>questionsPCTS1[3]'+questionsPCTS1[3].AssessmentQuestion__r.Name);
        questionsPCTS1[10].Score__c='3';
        //System.debug('--------->>>>>questionsPCTS1[10]'+questionsPCTS1[10].AssessmentQuestion__r.Name);
        update(questionsPCTS1);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        c1.save();
        //System.debug('------->>>>>>>>>ProjectCategoryS1__c'+o.ProjectCategoryS1__c);
        
        delete(questionsPCTS1);
    }
        // For GATE P0
        
        o.StageName = '6 - Negotiate & Win';                         //ensure TECH_Gate__c formula field is equal to P0 
        update o;
        o = [select Id, TECH_Gate__c, TECH_PCTGate__c, ProjectCategoryS0__c, ProjectCategoryS1__c, ProjectCategoryP0__c, OpptyType__c from Opportunity where Id =: o.Id];
        //System.debug('----->>>>>>>Tech_PCTGate__c'+o.TECH_PCTGate__c);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
               
        //Simulate page entry view
        c1.createQuestions();
               
        //Check results
        List<OPQ_OpportunityQuestion__c> questionsPCTP0 = [select id,Active__c,Score__c,AssessmentQuestion__r.Name from OPQ_OpportunityQuestion__c where Opportunity__c = :o.Id 
                            and Active__c = :Label.CL00074  
                            and AssessmentQuestion__r.Assessment__r.TECH_Axis__c = :ProjectCategoryTool
                            and AssessmentQuestion__r.Assessment__r.Gate__c = :GATEP0];
                
       //System.assert(questionsPCTP0.size() > 0);
       //System.debug('--------->>>>>questionsPCTP0----'+questionsPCTP0[0].AssessmentQuestion__r.Name+'----'+questionsPCTP0[1].AssessmentQuestion__r.Name+'----'+questionsPCTP0[2].AssessmentQuestion__r.Name+'----'+questionsPCTP0[3].AssessmentQuestion__r.Name+'----'+questionsPCTP0[4].AssessmentQuestion__r.Name+'----'+questionsPCTP0[5].AssessmentQuestion__r.Name+'----'+questionsPCTP0[6].AssessmentQuestion__r.Name+'----'+questionsPCTP0[7].AssessmentQuestion__r.Name+'----'+questionsPCTP0[8].AssessmentQuestion__r.Name+'----'+questionsPCTP0[9].AssessmentQuestion__r.Name+'----'+questionsPCTP0[10].AssessmentQuestion__r.Name+'----'+questionsPCTP0[11].AssessmentQuestion__r.Name);
    if(questionsPCTP0.size() > 0){    
               
        //Additional method calls for coverage
        c1.save(); 
       
        System.debug('***For ProjectCategoryP0__c = B***');
        questionsPCTP0[2].Score__c='1';
        //System.debug('--------->>>>>questionsPCTP0[2]'+questionsPCTP0[2].AssessmentQuestion__r.Name);
        update(questionsPCTP0);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        c1.save(); 
        //System.debug('------->>>>>>>>>ProjectCategoryP0__c'+o.ProjectCategoryP0__c);    
        
        System.debug('***For ProjectCategoryP0__c = A***');
        questionsPCTP0[3].Score__c='2'; 
        //System.debug('--------->>>>>questionsPCTP0[3]'+questionsPCTP0[3].AssessmentQuestion__r.Name);
        questionsPCTP0[11].Score__c='3';
       // System.debug('--------->>>>>questionsPCTP0[11]'+questionsPCTP0[11].AssessmentQuestion__r.Name);
        update(questionsPCTP0);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        c1.save(); 
        //System.debug('------->>>>>>>>>ProjectCategoryP0__c'+o.ProjectCategoryP0__c);
        
        System.debug('***For ProjectCategoryP0__c = C***');
        questionsPCTP0[3].Score__c='1'; 
       // System.debug('--------->>>>>questionsPCTP0[3]'+questionsPCTP0[3].AssessmentQuestion__r.Name);
        questionsPCTP0[1].Score__c='1';
        //System.debug('--------->>>>>questionsPCTP0[1]'+questionsPCTP0[1].AssessmentQuestion__r.Name);
        questionsPCTP0[5].Score__c='1';
       // System.debug('--------->>>>>questionsPCTP0[5]'+questionsPCTP0[5].AssessmentQuestion__r.Name);   
        questionsPCTP0[4].Score__c='1';
       // System.debug('--------->>>>>questionsPCTP0[4]'+questionsPCTP0[4].AssessmentQuestion__r.Name);
        questionsPCTP0[8].Score__c='1';
       // System.debug('--------->>>>>questionsPCTP0[8]'+questionsPCTP0[8].AssessmentQuestion__r.Name);
        questionsPCTP0[7].Score__c='1';
        // System.debug('--------->>>>>questionsPCTP0[7]'+questionsPCTP0[7].AssessmentQuestion__r.Name);
        questionsPCTP0[6].Score__c='1';
       // System.debug('--------->>>>>questionsPCTP0[6]'+questionsPCTP0[6].AssessmentQuestion__r.Name); 
        questionsPCTP0[11].Score__c='1';
      //  System.debug('--------->>>>>questionsPCTP0[11]'+questionsPCTP0[11].AssessmentQuestion__r.Name);
        questionsPCTP0[10].Score__c='1';
       // System.debug('--------->>>>>questionsPCTP0[10]'+questionsPCTP0[10].AssessmentQuestion__r.Name);
        questionsPCTP0[9].Score__c='1';
      //  System.debug('--------->>>>>questionsPCTP0[9]'+questionsPCTP0[9].AssessmentQuestion__r.Name);
        update(questionsPCTP0);
        
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        c1.save(); 
      //  System.debug('------->>>>>>>>>ProjectCategoryP0__c'+o.ProjectCategoryP0__c);
        
        //Additional method calls for coverage        
        c1.bTestFlag = true;                         //flag used to simulate error on update/insert to enforce complete coverage
        c1.save(); 
        //Ensure coverage in simulated error condition on creating questions
        delete(questionsPCTP0);
    }    
        sc1 = new ApexPages.StandardController(o);
        c1 = new VFC08_OPQTemplate(sc1);
        c1.bTestFlag = true;
        c1.createQuestions();
        
    }
}