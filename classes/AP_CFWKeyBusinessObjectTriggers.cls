Public class AP_CFWKeyBusinessObjectTriggers
{
    /** This method is used for Create the User Accessing Application to be displayed in the Data View of the Certification & Risk Assessment Tool*/
    public Static void CreateUserAccessingApp (map<Id,CFWKeyBusinessObjects__c> mapNewKBO)
    {
     List<id> CFWId = new List<id>();
     
     
     For(Id key:mapNewKBO.KeySet()){
     CFWId.add(mapNewKBO.get(key).CertificationRiskAssessment__c);    
     }
     
     Map<id,CFWApplicationUsers__c> MapCFWAppUser = new Map<id,CFWApplicationUsers__c>();
     
     List<CFWApplicationUsers__c> AppUsr = [Select id,UserRole__c,CertificationRiskAssessment__c from CFWApplicationUsers__c where CertificationRiskAssessment__c in :CFWid];
     
     List<CFWUserDataAccess__c> InsCFWDa = new List<CFWUserDataAccess__c>();
     
     if(!AppUsr.isEmpty()){ 
     For(Id key:mapNewKBO.KeySet()){
       For(CFWApplicationUsers__c U:AppUsr){
         CFWUserDataAccess__c CFWDa = new CFWUserDataAccess__c();
         CFWDa.ApplicationUsers__c = U.id;
        // CFWDa.UserRole__c = U.UserRole__c;
         CFWDa.CertificationRiskAssessment__c = U.CertificationRiskAssessment__c;
         CFWDa.BusinessObjects__c = key;
        // CFWDa.BusinessObject__c = mapNewKBO.get(key).BusinessObject__c;
         InsCFWDa.add(CFWDa);
         }
        } 
         Insert InsCFWDa;    
    }
   }
  }