/********************************************************************
* Company: Fielo
* Created Date: 09/07/2016
* Description:
********************************************************************/
@isTest
public with sharing class FieloPRM_Queueable_ResetLevelTest {
    

    static testmethod void test1() {
        User us = new user(id = userinfo.getuserId());

        us.BypassVR__c = true;
        update us;
        FieloEE.MockUpFactory.setCustomProperties(false);

         FieloEE__Member__c member = FieloEE.MockUpFactory.createMember('Test member', '1234', 'DNI');
        member = [select FieloEE__User__c from fieloee__member__c where id=:member.Id]; 
        FieloEE__Member__c member2 = FieloEE.MockUpFactory.createMember('Test2 member2', '56789', 'DNI');

        FieloCH__Challenge__c ch = FieloPRM_UTILS_MockUpFactory.createChallenge('testCH', 'Competition','Objective');
        FieloCH__Mission__c mission1 = FieloPRM_UTILS_MockUpFactory.createMissionWithObjective('Mission 1', 'FieloEE__BadgeMember__c', 'count', 'name', 'Equals or greater than', 1);
        FieloCH__MissionCriteria__c missionCriteria = FieloPRM_UTILS_MockUpFactory.createCriteria('Text', 'F_PRM_BadgeName__c', 'equals', 'Test', null, null, false, mission1.Id);

        FieloCH__MissionChallenge__c missionChallenge1  = FieloPRM_UTILS_MockUpFactory.createMissionChallenge(ch.id , mission1.id);
        FieloEE__Badge__c badge = new FieloEE__Badge__c (name='test', F_PRM_Type__c = 'Program Level', F_PRM_BadgeAPIName__c = '1');
        FieloEE__Badge__c badge2 = new FieloEE__Badge__c (name='test2', F_PRM_Type__c = 'Program Level', F_PRM_BadgeAPIName__c = '2');

        List<FieloEE__Badge__c> badges = new List<FieloEE__Badge__c>{badge, badge2};
        insert badges; 
        FieloCH__ChallengeReward__c chr1 = new FieloCH__ChallengeReward__c(F_PRM_ExpirationType__c = 'Months', F_PRM_ExpirationNumber__c = 1,FieloCH__LogicalExpression__c = '(1)', FieloCH__Challenge__c = ch.Id, FieloCH__Badge__c= badges[0].id);
        FieloCH__ChallengeReward__c chr2 = new FieloCH__ChallengeReward__c(F_PRM_ExpirationType__c = 'Months', F_PRM_ExpirationNumber__c = 1,FieloCH__LogicalExpression__c = '(1)',FieloCH__Challenge__c = ch.Id, FieloCH__Badge__c= badges[1].id);

        List<FieloCH__ChallengeReward__c> chRewards = new List<FieloCH__ChallengeReward__c>{chr1,chr2};
        insert chRewards;
        ch.FieloCH__isActive__c = true; 
        update ch; 

        list<FieloEE__BadgeMember__c> listBadgeMemberToPRocess = new list<FieloEE__BadgeMember__c>();
        FieloEE__BadgeMember__c badgemem = new FieloEE__BadgeMember__c();
        badgemem.FieloEE__Member2__c = member.id;
        badgemem.FieloEE__Badge2__c = badge.id;
        listBadgeMemberToPRocess.add(badgemem);

        System.runas(us){
            test.StartTEst();
            
            ID jobID = System.enqueueJob(new FieloPRM_Queueable_ResetLevel(listBadgeMemberToPRocess));

            test.StopTEst();
        }
    }
}