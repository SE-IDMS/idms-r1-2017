/********************************************************************
 * ZuoraUtilitiesTest
 *
 * Author: Kevin Fabregue - Zuora Europe
 ********************************************************************/

@isTest(seeAllData = true)
public with sharing class ZuoraUtilitiesTest {
    @isTest static void testAllMethods(){
        Test.startTest();
        Zuora.zApi zApi = ZuoraUtilities.zuoraApiAccess();

        ZuoraUtilities.queryToZuora(zApi, 'SELECT Id FROM Subscription');
        ZuoraUtilities.queryToZuora(zApi, 'SELECT Id FROM InvoiceItem');
        ZuoraUtilities.queryToZuora(zApi, 'SELECT TargetDate FROM Invoice');
        ZuoraUtilities.queryToZuora(zApi, 'SELECT Id FROM Invoice');
        ZuoraUtilities.queryToZuora(zApi, 'SELECT Id FROM InvoicePayment');
        ZuoraUtilities.queryToZuora(zApi, 'SELECT Id FROM Payment');
        ZuoraUtilities.queryToZuora(zApi, 'SELECT Id FROM Contact');

        List<Zuora.zObject> zuoraObjectsToCreateList = new List<Zuora.zObject>();
        List<Zuora.zApi.SaveResult> saveResultsList = ZuoraUtilities.createZuoraObjects(zApi, zuoraObjectsToCreateList);
        
        List<Zuora.zObject> zuoraObjectsToUpdateList = new List<Zuora.zObject>();
        List<Zuora.zApi.SaveResult> saveResultsList2 = ZuoraUtilities.updateZuoraObjects(zApi, zuoraObjectsToUpdateList);

        ZuoraUtilities.analyzeSaveResult(saveResultsList.get(0));

        ZuoraUtilities.dateZuoraFormat(Date.today());
        List<Account> accs = new List<Account>();
        Country__c ct = [select Id from Country__c limit 1];
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
        insert acc1;
        accs.add(acc1);
        ZuoraUtilities.updateVatIdInZuora(accs);
        Test.stopTest();
    }
}