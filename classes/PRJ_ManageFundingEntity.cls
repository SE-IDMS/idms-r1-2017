/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 03/15/2012
    Description     : Controller class for the page PRJ_FundingEntity                      
                      
*/

public with sharing class PRJ_ManageFundingEntity
{
    public ApexPages.StandardController controller;
    
    public DMTFundingEntity__c oFE {get;set;}
    
    public List<Integer> yearsInt {get;set;}
    
    public Map<Integer,string> yearsMap {get;set;}
    
    public Map<string,string> alphaTypes {get;set;}
    
    public Map<Integer,List<string>> keysforyear {get;set;}
    
    public String mapkey{get;set;}{mapkey='';}
    
    public List<string> lstTypeOrder {get;set;}
    
    public Set<string> setTypeOrder {get;set;}
    
    Map<string,DMTFundingEntityCostItemsFields__c> mapCS {get;set;}
     
    public List<string> lstType {get;set;}
    
    Boolean boolActive = false;
    public Map<string,string> keyMap {get;set;}
    {
        keyMap = new Map<string,string>();
    }
    
    public Map<string,string> fieldnameMap {get;set;}
    {
        fieldnameMap = new Map<string,string>();
        system.debug(DMTFundingEntityCostItemsFields__c.getall().values());
        Map<String,DMTFundingEntityCostItemsFields__c> csMap = DMTFundingEntityCostItemsFields__c.getAll();
        System.Debug('csMap size: ' + csMap.keySet().size());
        for(String s:csMap.keySet())
        {
            string strValue = csMap.get(s).CostItemFieldAPIName__c;
            System.Debug('strValue: ' + strValue);
            fieldnameMap.put(s,strValue);
            mapkey += s + ',';
            keyMap.put(strValue,s);
            
        }
        System.Debug('Mapkey: ' + mapkey);
    }
    
    public string startYear {get;set;}
    
    public PRJ_ManageFundingEntity(ApexPages.StandardController controller)
    {
        this.controller = controller;
        if(!Test.isRunningTest())
        {
            controller.addFields(fieldnameMap.values());
        }                
        oFE = (DMTFundingEntity__c) controller.getRecord();
        mainInit();
 
    }
    
    private void mainInit()
    {
        yearsInt = new List<Integer>();
        yearsMap = new Map<Integer,string>();
        alphaTypes = new Map<string,string>();
        keysforyear = new Map<Integer,List<string>>();
        lstTypeOrder = new List<string>();
        setTypeOrder = new Set<string>();
        mapCS = DMTFundingEntityCostItemsFields__c.getAll();
        lstType = new List<string>();
        
        for(DMTFundingEntityCostItemsFields__c cs1:mapCS.values())
        {
            if(!alphaTypes.containsKey(cs1.Type__c))
                alphaTypes.put(cs1.Type__c,cs1.Type__c);        
            if(!setTypeOrder.contains(cs1.TypeOrder__c))
                setTypeOrder.add(cs1.TypeOrder__c);
        }
        
        for(string to:setTypeOrder)
        {
            lstTypeOrder.add(to);
        }
        
        lstTypeOrder.sort();
        
        
        for(integer i=0;i<lstTypeOrder.size();i++)
        {
            for(DMTFundingEntityCostItemsFields__c cs1:mapCS.values())
            {
                if(cs1.TypeOrder__c == lstTypeOrder.get(i))
                {
                    /*
                    string strType = cs1.Type__c;
                    boolean isDuplicate = false;
                    for(integer j=0;j<lstType.size();j++)
                    {
                        if(lstType.get(j) == strType)
                        {
                            isDuplicate = true;
                            break;
                        }
                    }
                    if(!isDuplicate)
                        lstType.add(cs1.Type__c);
                    break;
                    */
                    lstType.add(cs1.Type__c);
                    break;
                }
            }
        }               
       // Modified by Ramakrishna Singara(Schneider Electric) for December Release
       // Included new fields in query ie PLFundEstimatedY0__c,PLFundEstimatedY1__c,PLFundEstimatedY2__c,PLFundEstimatedY3__c
       oFE = [select Name,ProjectReq__c,BudgetYear__c,CAPEXY0__c,CAPEXY1__c,CAPEXY2__c,CAPEXY3__c,OPEXOTCY0__c,OPEXOTCY1__c,OPEXOTCY2__c,OPEXOTCY3__c,SmallEnhancementsY0__c,SmallEnhancementsY1__c,SmallEnhancementsY2__c,SmallEnhancementsY3__c,RunningCostsImpactY0__c,RunningCostsImpactY1__c,RunningCostsImpactY2__c,RunningCostsImpactY3__c,PLFundEstimatedY0__c,PLFundEstimatedY1__c,PLFundEstimatedY2__c,PLFundEstimatedY3__c,TotalCAPEX__c,TotalOPEXOTC__c,TotalSmallEnhancements__c,TotalRunningCostsImpact__c,TotalFY0__c,TotalFY1__c,TotalFY2__c,TotalFY3__c,TotalCostFY0FY1FY2__c from DMTFundingEntity__c where Id = :oFE.Id];
        
        startYear = oFE.BudgetYear__c;
        
        generateKeysBasedOnStartYear();
             
    }
    
    
    private void generateKeysBasedOnStartYear()
    {
        List<String> concat = new List<String>();
        Integer iYear = Integer.valueOf(startYear.trim());
        Integer calcYear = 0;
        
        //for (integer i = 0;i<PRJ_Config__c.getInstance().NumberOfYearsForBudget__c;i++) 
        for (integer i = 0;i<4;i++) 
        {
            concat = new List<String>();
            calcYear = iYear + i;         
            yearsInt.add(calcYear);
            yearsMap.put(calcYear,'Y' + i);
            for (String subkey : alphaTypes.values())
            {
                string strVal = yearsMap.get(calcYear);
                concat.add(subkey+strVal);                              
            }
            keysforyear.put(calcYear, concat);
        }        
    }
    // Calculate P & L button added for July 2013 release -- Priyanka Shetty (Schneider Electric)--START
   public pagereference updatePLFields(){
    
            Try{        
                    for(DMT_FundingEntiity_BudgetLines__c csInstance : DMT_FundingEntiity_BudgetLines__c.getAll().values() ){
                        if(oFE.get(csInstance.name) == null)
                            oFE.put(csInstance.name,0);
                    }
                         oFE.PLFundEstimatedY0__c = oFE.OPEXOTCY0__c + oFE.RunningCostsImpactY0__c + oFE.SmallEnhancementsY0__c;
                         oFE.PLFundEstimatedY1__c = oFE.OPEXOTCY1__c + oFE.RunningCostsImpactY1__c + oFE.SmallEnhancementsY1__c + oFE.CAPEXY0__c/3;
                         oFE.PLFundEstimatedY2__c = oFE.OPEXOTCY2__c + oFE.RunningCostsImpactY2__c + oFE.SmallEnhancementsY2__c + oFE.CAPEXY0__c/3 + oFE.CAPEXY1__c/3;
                         oFE.PLFundEstimatedY3__c = oFE.OPEXOTCY3__c + oFE.RunningCostsImpactY3__c + oFE.SmallEnhancementsY3__c + + oFE.CAPEXY0__c/3 + oFE.CAPEXY1__c/3 + oFE.CAPEXY2__c/3;
                        //update oFE;
                        updateFundingEntity();
            }
            Catch(Exception e){
                System.debug('------'+e);
            }   
             return null;
    }
    //END
    public pagereference updateFundingEntity()
    {        
        try
        {
            update oFE;
            
            //Ramakrishna added code For June Release Start
            
            

            Map<string, list<DMTFundingEntity__c>> EntityFelistMap = new Map<string, list<DMTFundingEntity__c>>();
            DMTFundingEntity__c  fe=[select id ,CurrencyISOCode,ProjectReq__c,BudgetYear__c  from DMTFundingEntity__c   where id =:oFE.id];
            list<Budget__c>  oBudget=[select id,CurrencyISOCode,TotalPlannedCost__c,TotalCashoutCAPEX__c,TotalCashoutOPEX__c,TotalInternalCAPEX__c,TotalInternalOPEX__c,TotalSmallEnhancements__c, NewTotalCashoutCAPEX__c, NewTotalCashoutOPEX__c, NewTotalInternalCAPEX__c, NewTotalInternalOPEX__c, NewTotalSmallEnhancements__c,PastBudgetCashoutCAPEXCumulative__c,PastBudgetCashoutOPEXCumulative__c,PastBudgetInternalCAPEXCumulative__c,PastBudgetInternalOPEXCumulative__c,PastBudgetSmallEnhancementsCumulativ__c,PastBudgetGDCostsCumulative__c, Active__c from Budget__c where ProjectReq__c = :fe.ProjectReq__c and Active__c = true];
            
            if(oBudget!=null && oBudget.size()>0){
                list< DMTFundingEntity__c> ferec =[select ParentFamily__c, Parent__c, GeographicZoneSE__c, GeographicalSEOrganization__c,CurrencyISOCode, TotalCostFY0FY1FY2__c , BudgetYear__c, id, ActiveForecast__c, SelectedinEnvelop__c, ProjectReq__c,  ForecastYear__c, ForecastQuarter__c from  DMTFundingEntity__c where ProjectReq__c =:oFE.ProjectReq__c and BudgetYear__c=:oFE.BudgetYear__c and ActiveForecast__c = true];
                List<DMTFundingEntity__c> fetomakeTotal= new List<DMTFundingEntity__c>();
                Map<String , integer> intQuaterMap = new Map<String,integer>();
                intQuaterMap.put('Q1',1);
                intQuaterMap.put('Q2',2);
                intQuaterMap.put('Q3',3);
                intQuaterMap.put('Q4',4);
                Map<String , DMTFundingEntity__c> quaterFEMap = new Map<String,DMTFundingEntity__c>();
                for(DMTFundingEntity__c dmtfe : ferec){
                    
    
                    String mapKey='';
                    mapkey=dmtfe.ParentFamily__c+dmtfe.Parent__c+dmtfe.GeographicZoneSE__c+dmtfe.GeographicalSEOrganization__c;
                    if(!EntityFelistMap.containsKey(mapkey)){
                        
    
                        EntityFelistMap.put(mapkey,new List<DMTFundingEntity__c>());
                        EntityFelistMap.get(mapKey).add(dmtfe);
                        
                    }
                    else{
                        EntityFelistMap.get(mapKey).add(dmtfe);
                    }
                    
                }
                if(EntityFelistMap!= null && EntityFelistMap.size()>0){
                    
                    for(String ks: EntityFelistMap.keySet()){
                        
                        List<DMTFundingEntity__c> felist=new List<DMTFundingEntity__c>();
                        felist = EntityFelistMap.get(ks);
                        if(felist.size()>1){
                            DMTFundingEntity__c temp=new DMTFundingEntity__c();
                            
                          
                            for(Integer i=0;i<felist.size();i++){
                                
                                  if(i== 0){
                                    temp =felist[0];
                                  }
                                  else{
                                    Integer tFryr=Integer.valueOf(temp.ForecastYear__c);
                                    Integer nFryr=Integer.valueOf(felist[i].ForecastYear__c);
                                    if(tFryr == nFryr){
                                        if(intQuaterMap.get(temp.ForecastQuarter__c )> intQuaterMap.get(felist[i].ForecastQuarter__c)){
                                        
                                        }
                                        else{
                                            temp =felist[i];
                                        }
                                    }
                                    else if(nFryr > tFryr){
                                        temp= felist[i];
                                    }
                                    
                                  }
                                
                                
                                
                            }
                            fetomakeTotal.add(temp);
                            
                        }
                        else if(felist.size() == 1){
                            fetomakeTotal.add(felist[0]);
                        }
                        
                    }
                    
                }
                system.debug('***********Funding Entity size***********:  '+fetomakeTotal.size());
                Decimal totalCost=0;
                List<DatedConversionRate> dtCRList = new List<DatedConversionRate>();
                Map<String, Decimal> isoCodeConvMap = new Map<String, Decimal>();
                Set<String> isoCodeSet =new Set<String>();
                if(fetomakeTotal!= null && fetomakeTotal.size()>0){
                    for(DMTFundingEntity__c fund: fetomakeTotal){
                        isoCodeSet.add(fund.CurrencyISOCode);
                        system.debug('***********Funding Entity IsoCode List***********:  '+isoCodeSet);
    
                    }
                }
                
                if(oBudget[0]!= null){
                    isoCodeSet.add(oBudget[0].CurrencyISOCode);
                    system.debug('***********Budget IsoCode***********:  '+isoCodeSet);
                }
                
                if(isoCodeSet!= null && isoCodeSet.size()>0){
                    dtCRList= [SELECT ConversionRate,IsoCode FROM DatedConversionRate where  IsoCode in :isoCodeSet and startDate<=:System.Today().toStartOfMonth() and NextStartDate>=:System.Today()];
                    system.debug('***********Date Convertion List***********:  '+dtCRList.size());
                }
                
                if(dtCRList!= null && dtCRList.size()>0){
                    for(DatedConversionRate dcr : dtCRList){
                        isoCodeConvMap.put(dcr.IsoCode,dcr.ConversionRate);
    
    
                    }
    
                }
                
                if(fetomakeTotal!= null && fetomakeTotal.size()>0){
                    for(DMTFundingEntity__c fundEnt: fetomakeTotal){
                        System.debug('******* '+isoCodeConvMap.get(oBudget[0].CurrencyISOCode));
                         System.debug('******* '+isoCodeConvMap.get(fundEnt.CurrencyISOCode));
                        totalCost += ((fundEnt.TotalCostFY0FY1FY2__c)*isoCodeConvMap.get(oBudget[0].CurrencyISOCode))/isoCodeConvMap.get(fundEnt.CurrencyISOCode);
                        system.debug('***********totalCost***********:  '+totalCost);
                    }
                }
                
                Decimal tpc;
                totalCost = totalCost.setScale(0);
                System.debug('asdf'+oBudget[0].TotalPlannedCost__c);
                if(oBudget[0].TotalPlannedCost__c!= null){
                    tpc=oBudget[0].TotalPlannedCost__c.setScale(0);
                }
                System.debug('!!!!!!!!!'+totalCost );
                string st;
                Decimal TotalDC;
                Decimal tdc;
                integer td;
                if(totalCost !=tpc ){
                    system.debug('***********totalCost***********:  '+totalCost      +oBudget[0].TotalPlannedCost__c);
                    TotalDC =totalCost - tpc;
                    tdc = (TotalDC)*isoCodeConvMap.get(fe.CurrencyISOCode)/isoCodeConvMap.get(oBudget[0].CurrencyISOCode);
                    td = math.abs(integer.valueof(tdc.setScale(0)));
                    st = string.valueOf(td);
                    //tdc = math.abs(integer.valueof(totalCost - tpc));
                    //st = string.valueOf(tdc);
                    ApexPages.addMessage(new ApexPages.Message ( ApexPages.Severity.WARNING,Label.DMT_MessageFundingNotEqualToCostsForecast + '  '+fe.CurrencyISOCode + ' '+st));
                    //ApexPages.addMessage(new ApexPages.Message ( ApexPages.Severity.WARNING,Label.DMT_MessageFundingNotEqualToCostsForecast ));
                    
                }   
                /*if(totalCost !=tpc ){
                    system.debug('***********totalCost***********:  '+totalCost      +oBudget[0].TotalPlannedCost__c);
                    tdc = math.abs(integer.valueof(totalCost - tpc));
                    st = string.valueOf(tdc);
                    //ApexPages.addMessage(new ApexPages.Message ( ApexPages.Severity.WARNING,Label.DMT_MessageFundingNotEqualToCostsForecast + '  '+fe.CurrencyISOCode + ' '+st));
                    ApexPages.addMessage(new ApexPages.Message ( ApexPages.Severity.WARNING,Label.DMT_MessageFundingNotEqualToCostsForecast));
                }*/   
                
            }
            


           // Ramakrishna added code For June Release End
          mainInit();
        }        
        catch(DmlException dmlexp)
        {
            for(integer i = 0;i<dmlexp.getNumDml();i++)
                ApexPages.addMessages(new noMessageException(dmlexp.getDmlMessage(i)));
                      
        }                
        return null;
    }
    
    
  public Pagereference UpdateSelectionInBudgetEnvelop()
    {
        String newPageUrl = '/apex/FE_UpdateSelectionInBudgetEnvelop?id='+oFE.Id;
        PageReference pageRef = new PageReference(newPageUrl);
        pageRef.setRedirect(true);
        return pageRef;
    }

    /**
    * Method managing the back button to the related/parent budget
    **/      
    public pagereference back(){
        PageReference pageRef = new PageReference('/'+oFE.ProjectReq__c);
        pageRef.setRedirect(true);
        return pageRef;
    }
    /*
    public Pagereference backToFundingEntity()
    {
        PageReference pageRef = new PageReference('/'+ oFE.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
  */
     public class noMessageException extends Exception{}
    
}