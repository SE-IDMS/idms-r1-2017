public class IdmsUimsReconcCompanyMapping {
    //Mapping of IDMS - UIMS User. 
    public static IdmsUsersResyncflowServiceIms.accountBean mapIdmsCompanyUims(User usr){
        IdmsUsersResyncflowServiceIms.accountBean uimsAccountBean = new IdmsUsersResyncflowServiceIms.accountBean();
        uimsAccountBean.NAME = (usr.CompanyName !=null && !((string)usr.CompanyName).equalsignorecase('null'))?usr.CompanyName:null;
        //ToCheck: uimsAccountBean.goldenId = (usr.IDMSCompanyGoldenId__c !=null && !((string)usr.IDMSCompanyGoldenId__c).equalsignorecase('null'))?usr.IDMSCompanyGoldenId__c:null;
        uimsAccountBean.COUNTRY_CODE = (usr.Company_Country__c !=null && !((string)usr.Company_Country__c).equalsignorecase('null'))?usr.Company_Country__c:null;
        uimsAccountBean.CURRENCY_CODE = (usr.DefaultCurrencyIsoCode !=null && !((string)usr.DefaultCurrencyIsoCode).equalsignorecase('null'))?usr.DefaultCurrencyIsoCode:null;
        uimsAccountBean.CUSTOMER_CLASS = (usr.IDMSClassLevel2__c !=null && !((string)usr.IDMSClassLevel2__c).equalsignorecase('null'))?usr.IDMSClassLevel2__c:null;
        uimsAccountBean.CITY = (usr.Company_City__c !=null && !((string)usr.Company_City__c).equalsignorecase('null'))?usr.Company_City__c:null;
        uimsAccountBean.ZIP_CODE  = (usr.Company_Postal_Code__c !=null && !((string)usr.Company_Postal_Code__c).equalsignorecase('null'))?usr.Company_Postal_Code__c:null;
        uimsAccountBean.POBOX = (usr.IDMSCompanyPoBox__c !=null && !((string)usr.IDMSCompanyPoBox__c).equalsignorecase('null'))?usr.IDMSCompanyPoBox__c:null;
        uimsAccountBean.STATE_PROVINCE_CODE = (usr.Company_State__c !=null && !((string)usr.Company_State__c).equalsignorecase('null'))?usr.Company_State__c:null;
        uimsAccountBean.STREET = (usr.Company_Address1__c !=null && !((string)usr.Company_Address1__c).equalsignorecase('null'))?usr.Company_Address1__c:null;    
        uimsAccountBean.ADD_INFO_ADDRESS = (usr.Company_Address2__c !=null && !((string)usr.Company_Address2__c).equalsignorecase('null'))?usr.Company_Address2__c:null; 
        if(usr.IDMSCompanyHeadquarters__c){
            uimsAccountBean.HEADQUARTER = TRUE;
        }else{
            uimsAccountBean.HEADQUARTER = FALSE;
        }
        uimsAccountBean.COUNTY = (usr.IDMSCompanyCounty__c !=null && !((string)usr.IDMSCompanyCounty__c).equalsignorecase('null'))?usr.IDMSCompanyCounty__c:null; 
        uimsAccountBean.WEB_SITE = (usr.Company_Website__c !=null && !((string)usr.Company_Website__c).equalsignorecase('null'))?usr.Company_Website__c:null; 
         If(!String.isBlank(usr.IDMSMarketSubSegment__c)){
            uimsAccountBean.MARKET_SEGMENT = usr.IDMSMarketSubSegment__c;
        }else{
            uimsAccountBean.MARKET_SEGMENT = usr.IDMSMarketSegment__c;
        }  
        uimsAccountBean.MARKET_SERVED = (usr.IDMSCompanyMarketServed__c !=null && !((string)usr.IDMSCompanyMarketServed__c).equalsignorecase('null'))?usr.IDMSCompanyMarketServed__c:null; 
        uimsAccountBean.EMPLOYEE_SIZE = (usr.IDMSCompanyNbrEmployees__c !=null && !((string)usr.IDMSCompanyNbrEmployees__c).equalsignorecase('null'))?usr.IDMSCompanyNbrEmployees__c:null; 
        //Not mapped: uimsAccountBean.BUSINESS_UNIT_CODE = (usr.IDMSClassLevel1__c !=null && !((string)usr.IDMSClassLevel1__c).equalsignorecase('null'))?usr.IDMSClassLevel1__c:null;
        uimsAccountBean.TAX_IDENTIFICATION_NUMBER = (usr.IDMSTaxIdentificationNumber__c !=null && !((string)usr.IDMSTaxIdentificationNumber__c).equalsignorecase('null'))?usr.IDMSTaxIdentificationNumber__c:null;
        return uimsAccountBean;
        
    }
}