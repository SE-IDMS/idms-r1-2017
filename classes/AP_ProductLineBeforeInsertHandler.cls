/*
    Author: Bhuvana S(GD Solutions)
    Purpose: Merged different functionalities of Product Line After insert to a single class
*/
public with sharing class AP_ProductLineBeforeInsertHandler 
{    
    public void OnBeforeInsert(List<OPP_ProductLine__c> newProductLines,Map<Id,OPP_ProductLine__c> newProductLinesMap)
    {
        // EXECUTE AFTER INSERT LOGIC       
        updProductLine(newProductLines);
    }
    //The method updates the Line Amount Converted EUR field with the dated Exchange Rate
    /*For PRM oct 13 Release:
        Update Product line picklist if value is entered for Product Line text
        Update Prodct Line text if product line picklist is selected.*/
    private void updProductLine(List<OPP_ProductLine__c> newProductLines)
    {
        for(OPP_ProductLine__c pl: newProductLines)
        {
            //Checks if the Opportunity is closed
            if(pl.ProductLine__c != null && pl.ProductLine__c.length()>=5)
                pl.PartnerProductLine__c = pl.ProductLine__c.substring(0,5);
            else if(pl.PartnerProductLine__c != null)
            {
                
                Map<String, CS_ProductLineBusinessUnit__c> mapProductLineBU = CS_ProductLineBusinessUnit__c.getAll();
                if(mapProductLineBU.containsKey(pl.PartnerProductLine__c))
                {
                    pl.ProductLine__c = mapProductLineBU.get(pl.PartnerProductLine__c).ProductLine__c;
                    pl.ProductBU__c = mapProductLineBU.get(pl.PartnerProductLine__c).ProductBU__c;
                }
            }
        }
    }
}