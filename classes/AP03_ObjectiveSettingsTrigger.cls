/*
    
    Author:Pooja Bhute
    Date: 31/01/2011.
    Description:This class includes the bussiness logic for the Triggers on the Objective Settings Object. 
*/

public with sharing class AP03_ObjectiveSettingsTrigger {
    
    public static Boolean AP03TestClassFlag = True;
    public static List<User> userList = new List<User>();
    public static List<String> userIds = new List<String>();
    public static void UpdateObjectiveLeaderOnObjectives(List<ObjectivesSettings__c> objectivesSettings) {    
        System.Debug('****AP03_ObjectiveSettings methodInsertUpdate Started ****');             
        for(ObjectivesSettings__c OC : objectivesSettings) 
        {  
            OC.ObjectiveLeader__c = OC.OWNERID;    
        }  
        System.Debug('****AP03_ObjectiveSettings methodInsertUpdate Ended****');        
    }    
    public static void UpdateObjectiveLeaderApprover(List<ObjectivesSettings__c> objectivesSettings)
    {
        System.Debug('****AP03_ObjectiveSettings Update Approver Started ****');  
        for(ObjectivesSettings__c OC : objectivesSettings) 
        {
            userIds.add(OC.OwnerID);
        }
        userList = [SELECT ManagerID,ObjectivesApprover__c FROM User WHERE ID IN:userIds];
        System.Debug('****** User size:' + userList.size());
        
        for(User leader : userList)
        {
            if(leader.ObjectivesApprover__c == null)
            {
                System.Debug('****** Approver:' + leader.ObjectivesApprover__c);
                System.Debug('****** Manager:' + leader.ManagerID);
                if(leader.ManagerId !=null)
                {
                    leader.ObjectivesApprover__c = leader.ManagerID;
                }
            }
        }
        if(userList.Size() > 0){
                if(userList.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                    System.Debug('######## AP03 Error Inserting : '+ Label.CL00264);
                }
                else{
                    Database.SaveResult[] updateObjectiveApprovers = Database.update(userList,false);
                    
                    for(Database.SaveResult sr: updateObjectiveApprovers ){
                        if(!sr.isSuccess()){
                            Database.Error err = sr.getErrors()[0];
                            System.Debug('######## AP03 Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                        }    
                    }
                      
                }
        }
        
        
        
        System.Debug('****AP03_ObjectiveSettings Update Approver Ended ****');          
        
    
    }     
    public static void recordUpdationAfterApproval(ObjectivesSettings__c[] objectivesSettings) {    
        System.Debug('****AP03_ObjectiveSettings recordUpdationAfterApproval Started ****');
        List<ProcessInstance> processInstanceList = new List<ProcessInstance>();
        List<String> objectiveSettingIds = new  List<String> ();
           
        for(ObjectivesSettings__c objs : objectivesSettings) {    
            objectiveSettingIds.add(objs.Id);
        }
        
        if(objectiveSettingIds.Size() > 0){
            processInstanceList = [Select Id, LastModifiedDate, Status, TargetObjectId from ProcessInstance where TargetObjectId in: objectiveSettingIds and Status =: Label.CL00242];
            System.Debug('****Process Instances: '+processInstanceList);
                                            
            for(ObjectivesSettings__c objSet: objectivesSettings) {       
                if(objSet.Tech_ReportIsApproved__c == Label.CL00074.toUpperCase() && objSet.ObjectivesModifiedAfterApproval__c == False && objSet.TECH_UpdatedByApprovalProcess__c == False){ 
                    for(ProcessInstance prosInst: processInstanceList) {
                        System.Debug('****Obj Settings Last Modified Date: '+objSet.LastModifiedDate);
                        System.Debug('****Process Instance Last Modified Date: '+prosInst.LastModifiedDate);
                        if((objSet.Id == prosInst.TargetObjectId) && (objSet.LastModifiedDate > prosInst.LastModifiedDate)){
                            objSet.ObjectivesModifiedAfterApproval__c = True;                                     
                        }
                    }
                }
                else if(objSet.Tech_ReportIsApproved__c == Label.CL00074.toUpperCase() && objSet.ObjectivesModifiedAfterApproval__c == False && objSet.TECH_UpdatedByApprovalProcess__c == True){
                    objSet.TECH_UpdatedByApprovalProcess__c = False;
                }
            }
        }
       System.Debug('****AP03_ObjectiveSettings recordUpdationAfterApproval Ended ****'); 
    }
    
    public static void recordUpdationAfterApprovalForObjDetail(ObjectivesDetail__c[] objectiveDetails) {
        System.Debug('****AP03_ObjectiveSettings recordUpdationAfterApprovalForObjDetail Started ****');
        
        List<String> objectiveSettingIds = new List<String>();
        List<ObjectivesSettings__c> objectiveSettingsToUpdate = new List<ObjectivesSettings__c>();
        Set<String> ObjectivesSet = new Set<String>();
        List<ObjectivesSettings__c> objectivesSettingDetails = new List<ObjectivesSettings__c>();
        
        for(ObjectivesDetail__c objDet : objectiveDetails) {   
            if(!ObjectivesSet.contains(objDet.ParentObjectiveSetting__c)){
                objectiveSettingIds.add(objDet.ParentObjectiveSetting__c);
                ObjectivesSet.add(objDet.ParentObjectiveSetting__c);
            }
        }         
        
        if(objectiveSettingIds.Size() > 0){
            objectivesSettingDetails = [SELECT Tech_ReportIsApproved__c, TECH_UpdatedByApprovalProcess__c, ObjectivesModifiedAfterApproval__c FROM ObjectivesSettings__c WHERE Id in: objectiveSettingIds] ;
                         
            for(ObjectivesSettings__c objSet: objectivesSettingDetails){
                if(objSet.Tech_ReportIsApproved__c == Label.CL00074.toUpperCase() && objSet.ObjectivesModifiedAfterApproval__c == False){ // Removed '&& objSet.TechnUpdatedByApprovalProcess__c == false' till further clarification              
                    objSet.ObjectivesModifiedAfterApproval__c = true;                   
                    objectiveSettingsToUpdate.add(objSet);
                }                
            }
            System.Debug('****Obj Settings to be Updated: '+objectiveSettingsToUpdate);
            
            if(objectiveSettingsToUpdate.Size() > 0){
                if(objectiveSettingsToUpdate.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                    System.Debug('######## AP03 Error updating: '+ Label.CL00264);
                }
                else{
                     Database.SaveResult[] objectiveSettingsUpdateResult = Database.update(objectiveSettingsToUpdate, true);
                        
                    for(Database.SaveResult sr: objectiveSettingsUpdateResult){
                        if(!sr.isSuccess()){
                            Database.Error err = sr.getErrors()[0];
                            System.Debug('######## AP03 Error Updating: '+err.getStatusCode()+' '+err.getMessage());
                        }    
                    }
                }
            }
        }
    
        System.Debug('****AP03_ObjectiveSettings recordUpdationAfterApprovalForObjDetail Ended ****'); 
    } 
    
      
    public static void recordUpdationAfterApprovalForSubObjective(List<SubObjective__c> subObjectives){   
        System.Debug('****AP03_ObjectiveSettings recordUpdationAfterApprovalForSubObjective Started ****'); 
        List<String> subObjectiveId = new List<String>();
        List<String> parentObjectiveId = new List<String>();
        List<ObjectivesSettings__c> objectiveSettingsToUpdate = new List<ObjectivesSettings__c>();
        Set<String> subObjectiveSet = new Set<String>();
        Set<String> parentObjectiveSet = new Set<String>();
        List<ObjectivesDetail__c> objectivesDetails = new List<ObjectivesDetail__c>();
        List<ObjectivesSettings__c> objectivesSettings = new List<ObjectivesSettings__C>();
    
        for(SubObjective__c objSub: subObjectives) {
            if(!subObjectiveSet.contains(objSub.ParentObjective__c)){
                subObjectiveId.add(objSub.ParentObjective__c);
                subObjectiveSet.add(objSub.ParentObjective__c);
            }
        }
        
        if(subObjectiveId.Size() > 0)
        {
            objectivesDetails = [SELECT ID, LASTMODIFIEDDATE, ParentObjectiveSetting__c FROM ObjectivesDetail__c WHERE Id IN : subObjectiveId];
                     
            for(ObjectivesDetail__c od: objectivesDetails) {    
                if(!parentObjectiveSet.contains(od.ParentObjectiveSetting__c)){
                    parentObjectiveId.add(od.ParentObjectiveSetting__c);
                    parentObjectiveSet.add(od.ParentObjectiveSetting__c);
                }
            }
            
            if(parentObjectiveId.Size() > 0)
            {
                objectivesSettings = [SELECT ID, ObjectivesModifiedAfterApproval__c, TECH_UpdatedByApprovalProcess__c, Tech_ReportIsApproved__c FROM ObjectivesSettings__c WHERE ID IN: parentObjectiveId];
                        
                for(ObjectivesSettings__c objSet: objectivesSettings){
                    if(objSet.Tech_ReportIsApproved__c == Label.CL00074.toUpperCase() && objSet.ObjectivesModifiedAfterApproval__c == False){ // Removed '&& objSet.TechnUpdatedByApprovalProcess__c == false' till further clarification               
                        objSet.ObjectivesModifiedAfterApproval__c = true;                   
                        objectiveSettingsToUpdate.add(objSet);
                    }                    
                }
                
                if(objectiveSettingsToUpdate.Size() > 0){
                    if(objectiveSettingsToUpdate.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                        System.Debug('######## AP03 Error updating: '+ Label.CL00264);
                    }
                    else{
                         Database.SaveResult[] objectiveSettingsUpdateResult = Database.update(objectiveSettingsToUpdate, true);
                            
                        for(Database.SaveResult sr: objectiveSettingsUpdateResult){
                            if(!sr.isSuccess()){
                                Database.Error err = sr.getErrors()[0];
                                System.Debug('######## AP03 Error Updating: '+err.getStatusCode()+' '+err.getMessage());
                            }    
                        }
                    }
                }
            }
        }
        System.Debug('****AP03_ObjectiveSettings recordUpdationAfterApprovalForSubObjective Ended ****');    
   } 
   
   public static void updateTechFieldsofSubObjective(List<SubObjective__c> subobjectives)
   {
        System.Debug('**** AP03_ObjectiveSettings updateTechFieldsofSubObjective method Started ****'); 
        List<String> subObjectiveIds = new List<String>();
        List<SubObjective__c> subObjectivesToBeUpdated = new List<SubObjective__c>();
                    
        for(SubObjective__c objectives : subobjectives) 
        {  
            subObjectiveIds.add(objectives.Id);    
        }  
        
        SubObjective__c[] subObjectiveDetails = [Select Id, RecordType.Name, toLabel(CustomerClassificationLevel1__c), toLabel(CustomerClassificationLevel2__c), toLabel(MarketSegment__c), toLabel(MarketSubSegment__c), Tech_CustomerClassificationLevel1__c, Tech_CustomerClassificationLevel2__c, Tech_MarketSegment__c, Tech_MarketSubSegment__c, TECH_RecordTypeName__c from SubObjective__c where Id in: subObjectiveIds];
        
        for(SubObjective__c subObjectiveDetail: subObjectiveDetails)
        {            
            subObjectiveDetail.Tech_CustomerClassificationLevel1__c = subObjectiveDetail.CustomerClassificationLevel1__c;
            subObjectiveDetail.Tech_CustomerClassificationLevel2__c = subObjectiveDetail.CustomerClassificationLevel2__c;
            subObjectiveDetail.Tech_MarketSegment__c = subObjectiveDetail.MarketSegment__c;
            subObjectiveDetail.Tech_MarketSubSegment__c = subObjectiveDetail.MarketSubSegment__c;
            subObjectiveDetail.TECH_RecordTypeName__c = subObjectiveDetail.RecordType.Name;     

            subObjectivesToBeUpdated.add(subObjectiveDetail);           
        }         
       
            
        if(subObjectivesToBeUpdated.Size() > 0){
                        
            if(subObjectivesToBeUpdated.size() + Limits.getDMLRows() > Limits.getLimitDMLRows()){
                System.Debug('######## AP03 Error updating: '+ Label.CL00264);
            }
            else{
                Database.SaveResult[] subObjectivesUpdateResult = Database.update(subObjectivesToBeUpdated, true);
                   
                for(Database.SaveResult sr: subObjectivesUpdateResult){
                    if(!sr.isSuccess()){
                        Database.Error err = sr.getErrors()[0];
                        System.Debug('######## AP03 Error Updating: '+err.getStatusCode()+' '+err.getMessage());
                    }    
                }
            }
        } 
           
        System.Debug('**** AP03_ObjectiveSettings updateTechFieldsofSubObjective Ended ****');   
    }   
         
}