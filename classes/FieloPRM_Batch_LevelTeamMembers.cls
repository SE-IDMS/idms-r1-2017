/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: 
********************************************************************/

global class FieloPRM_Batch_LevelTeamMembers implements Database.Batchable<sObject>, Schedulable{
    
    private static final CS_PRM_ApexJobSettings__c config = CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Batch_LevelTeamMembers');    
    
    //BATCH METHODS
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query= 'SELECT Id, FieloCH__Team2__c, FieloCH__Member2__c FROM FieloCH__TeamMember__c WHERE F_PRM_Status__c = \'PENDING\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<FieloCH__TeamMember__c> scope) {
        FieloPRM_Utils_ChTeam.membersLeveling(scope);
        for (FieloCH__TeamMember__c tMemb: scope){
            tMemb.F_PRM_Status__c = 'PROCESSED';
        }
        update scope;
    } 
    
    global void finish(Database.BatchableContext BC) {}
    
    //SCHEDULE METHODS
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new FieloPRM_Batch_LevelTeamMembers(), Integer.valueOf(config.F_PRM_BatchSize__c));
    }   
    
    public static void scheduleNow(){
        DateTime nowTime = datetime.now().addSeconds(65);
        String Seconds      = '0';
        String Minutes      = nowTime.minute() < 10 ? '0' + nowTime.minute() : String.valueOf(nowTime.minute());
        String Hours        = nowTime.hour()   < 10 ? '0' + nowTime.hour()   : String.valueOf(nowTime.hour());
        String DayOfMonth   = String.valueOf(nowTime.day());
        String Month        = String.ValueOf(nowTime.month());
        String DayOfweek    = '?';
        String optionalYear = String.valueOf(nowTime.year());
        String CronExpression = Seconds+' '+Minutes+' '+Hours+' '+DayOfMonth+' '+Month+' '+DayOfweek+' '+optionalYear;
        System.schedule('FieloPRM_Batch_LevelTeamMembers '+system.now() + ':' + system.now().millisecond(), CronExpression, new FieloPRM_Batch_LevelTeamMembers());
    }  
}