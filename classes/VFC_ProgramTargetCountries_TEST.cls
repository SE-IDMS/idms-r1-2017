/*
    Author: Pooja Gupta
    Date: 03-May-13
    Description: Test Class for VFC_ProgramTargetCountries
    Release: May 2013

*/

@istest

private class VFC_ProgramTargetCountries_TEST
{
    static testMethod void myunittest()
    {
        
        List<Country__c> Countries = new list<Country__c>();
        For(integer c=0; c<10 ; c++)
        {
            Country__c country = new country__c(Name = 'TestCountry'+c,CountryCode__c ='XY'+c);
            Countries.add(country);
        }
        Database.SaveResult[] CountryListInsert= Database.insert(Countries, true);
        
        PartnerProgram__c prog1 = Utils_TestMethods.createPartnerProgram();
        prog1.TECH_CountriesId__c = Countries[0].id;
        prog1.TargetCountriesTxt__c = 'Testtext';
        insert prog1;
        
        List<PartnerProgramCountries__c> progCountries = New list<PartnerProgramCountries__c>();
        For(integer i=0; i< 10; i++)
        {
            PartnerProgramCountries__c ptrcountry = new PartnerProgramCountries__c(PartnerProgram__c = prog1.id);
        
            if(math.mod(i,2)!=0)
                ptrcountry.Country__c = Countries[0].id;
            else
                ptrcountry.Country__c = Countries[1].id;
            
            progCountries.add(ptrcountry);
        }
        
        Database.SaveResult[] ptrcountriesInsert= Database.insert(progCountries, true);   
        
        Apexpages.Standardcontroller controller1 = new Apexpages.Standardcontroller(progCountries[1]);
        VFC_ProgramTargetCountries vfcon = new VFC_ProgramTargetCountries(controller1);
        Pagereference pg1 = Page.VFP_ProgramTargetCountries;
        Test.setCurrentPage(pg1);
    }
}