@isTest
public class PRM_FeatureUtils_Test{

    public static testMethod void testMethodOne() {
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];         
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test.com',
                         isActive = True,                       
                         BypassVR__c = True);
                         
        insert u;

        PermissionSet tpermissionSet = [Select id,name from PermissionSet limit 1];
        
        if(tpermissionSet != null)
            PermissionSetAssignment pa = PRM_FeatureUtils.getPermissionSetAssignementByName(tpermissionSet.Name,u.id);
        
    
    }
    
    
}