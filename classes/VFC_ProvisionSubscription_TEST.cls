@isTest
public class VFC_ProvisionSubscription_TEST{
    @isTest
    static void runTest() {
        Zuora__Subscription__c a = new Zuora__Subscription__c();
        a.Name='A-S001111';
        insert a;
        PageReference pageRef = Page.VFP_ReprovisionSubscription;
            Test.setCurrentPage(pageRef); 
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MktoAccessTokenHttpCalloutMock());
        ApexPages.StandardController stanPage = new ApexPages.StandardController(a); 
            VFC_ProvisionSubscription controller = new  VFC_ProvisionSubscription(stanPage);
        Test.stopTest();        
    }
    @isTest
    static void runTest1() {
        Country__c country= Utils_TestMethods.createCountry();
                    insert country; 
        Account ac= Utils_TestMethods.createAccount();
        ac.Country__c=country.id;
        ac.MarketSegment__c = 'BDZ';
        insert ac;
        Contact con1 = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=ac.id, email = 'TestPartCrebfo@test.com', Country__c = country.id);
        insert con1;
        con1.PRMUIMSID__c = '798976978775446jg77';
        con1.JobFunc__c = 'Z016';
        con1.JobTitle__c = 'ZC';
        update con1;
        Test.startTest();
        VFC_ContactsToUIMS.ResponseResult responseresult1 = new VFC_ContactsToUIMS.ResponseResult();
        responseresult1.statuscode = '200';
        responseresult1.description = '';
        responseresult1.responseMessage = 'OK';
        responseresult1.responseCode = '200';
        responseresult1.federatedId = con1.PRMUIMSID__c; 
        
        PageReference pageRef = Page.VFP_ContacttoUIMS;
            Test.setCurrentPage(pageRef); 
        
        Test.setMock(HttpCalloutMock.class, new ContactIFWMock());
        
       // ContactIFWMock fakeresponse = new ContactIFWMock(500,'ok','{\"Fault\": {\"faultcode\": \"ns0:Server\",\"faultstring\": \"The message could not be sent to the service group IFW_UAT_BPM_ValOn_SG. Check whether the corresponding service container is running.\",\"faultactor\": \"com.eibus.web.soap.Gateway.wcp\",\"detail\": {\"FaultDetails\": {\"LocalizableMessage\": {\"MessageCode\": \"Cordys.WebGateway.Messages.WG_SOAPTransaction_ReceiverDetailsInvalid\",\"Insertion\": \"IFW_UAT_BPM_ValOn_SG\"}},\"FaultRelatedException\": \"fail\"}}}');
        ApexPages.currentPage().getParameters().put('id',con1.Id);
        ApexPages.StandardController stanPage = new ApexPages.StandardController(con1); 
        VFC_ContactsToUIMS controller = new  VFC_ContactsToUIMS(stanPage);
        controller.UpdateContact();
        controller.back();
        //controller.ResponseResult();
        Test.stopTest();        
    }
    @isTest
    static void runTest11() {
        Country__c country1= Utils_TestMethods.createCountry();
                    insert country1; 
        Account ac1= Utils_TestMethods.createAccount();
        ac1.Country__c=country1.id;
        ac1.MarketSegment__c = 'BDZ';
        insert ac1;
        Contact con11 = new Contact(LastName ='TestCOnt1',firstName = 'TestFirst1', AccountId=ac1.id, email = 'TestPartCrebfo1@test.com', Country__c = country1.id);
        insert con11;
        con11.PRMUIMSID__c = '798976978775446jg78';
        con11.JobFunc__c = 'Z016';
        con11.JobTitle__c = 'ZC';
        update con11;
        Test.startTest();
        VFC_ContactsToUIMS.ResponseResult responseresult11 = new VFC_ContactsToUIMS.ResponseResult();
        responseresult11.statuscode = '200';
        responseresult11.description = '';
        responseresult11.responseMessage = 'OK';
        responseresult11.responseCode = '200';
        responseresult11.federatedId = con11.PRMUIMSID__c; 
        
        PageReference pageRef = Page.VFP_ContacttoUIMS;
            Test.setCurrentPage(pageRef); 
        
        Test.setMock(HttpCalloutMock.class, new ContactSynchIFWMock ());
        
       // ContactIFWMock fakeresponse = new ContactIFWMock(500,'ok','{\"Fault\": {\"faultcode\": \"ns0:Server\",\"faultstring\": \"The message could not be sent to the service group IFW_UAT_BPM_ValOn_SG. Check whether the corresponding service container is running.\",\"faultactor\": \"com.eibus.web.soap.Gateway.wcp\",\"detail\": {\"FaultDetails\": {\"LocalizableMessage\": {\"MessageCode\": \"Cordys.WebGateway.Messages.WG_SOAPTransaction_ReceiverDetailsInvalid\",\"Insertion\": \"IFW_UAT_BPM_ValOn_SG\"}},\"FaultRelatedException\": \"fail\"}}}');
        ApexPages.currentPage().getParameters().put('id',con11.Id);
        ApexPages.StandardController stanPage1 = new ApexPages.StandardController(con11); 
        VFC_ContactsToUIMS controller1 = new  VFC_ContactsToUIMS(stanPage1);
        controller1.UpdateContact();
        controller1.back();
        //controller.ResponseResult();
        Test.stopTest();        
    }
    @isTest
    static void runTest12() {
        Country__c country2= Utils_TestMethods.createCountry();
                    insert country2; 
        Account ac2= Utils_TestMethods.createAccount();
        ac2.Country__c=country2.id;
        ac2.MarketSegment__c = 'BDZ';
        insert ac2;
        Contact con12 = new Contact(LastName ='TestCOnt12',firstName = 'TestFirst12', AccountId=ac2.id, email = 'TestPartCrebfo2@test.com', Country__c = country2.id);
        insert con12;
        con12.PRMUIMSID__c = '798976978775446jg78';
        con12.JobFunc__c = 'Z016';
        con12.JobTitle__c = 'ZC';
        update con12;
        Test.startTest();
        VFC_ContactsToUIMS.ResponseResult responseresult2 = new VFC_ContactsToUIMS.ResponseResult();
        responseresult2.statuscode = '200';
        responseresult2.description = '';
        responseresult2.responseMessage = 'OK';
        responseresult2.responseCode = '200';
        responseresult2.federatedId = con12.PRMUIMSID__c; 
        
        PageReference pageRef = Page.VFP_ContacttoUIMS;
            Test.setCurrentPage(pageRef); 
        
        Test.setMock(HttpCalloutMock.class, new IFWContactSynchMock ());
        
       // ContactIFWMock fakeresponse = new ContactIFWMock(500,'ok','{\"Fault\": {\"faultcode\": \"ns0:Server\",\"faultstring\": \"The message could not be sent to the service group IFW_UAT_BPM_ValOn_SG. Check whether the corresponding service container is running.\",\"faultactor\": \"com.eibus.web.soap.Gateway.wcp\",\"detail\": {\"FaultDetails\": {\"LocalizableMessage\": {\"MessageCode\": \"Cordys.WebGateway.Messages.WG_SOAPTransaction_ReceiverDetailsInvalid\",\"Insertion\": \"IFW_UAT_BPM_ValOn_SG\"}},\"FaultRelatedException\": \"fail\"}}}');
        ApexPages.currentPage().getParameters().put('id',con12.Id);
        ApexPages.StandardController stanPage12 = new ApexPages.StandardController(con12); 
        VFC_ContactsToUIMS controller12 = new  VFC_ContactsToUIMS(stanPage12);
        controller12.UpdateContact();
        controller12.back();
        //controller.ResponseResult();
        Test.stopTest();        
    }
}