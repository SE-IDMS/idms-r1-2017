global class CaseArchiving_BatchSchedule implements Schedulable {
    global void execute(SchedulableContext SC) {
        Integer size = Integer.Valueof(System.Label.CLJUL14CCC08);
        CaseArchiving_Batch  pBatch = new CaseArchiving_Batch(); 
        if(!Test.isRunningTest())
        Database.executeBatch(pBatch,size);
    }
}