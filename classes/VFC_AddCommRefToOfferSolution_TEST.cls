/****
Last Modified By:Hanamanth as per Q1 release 
******/

@isTest
private class VFC_AddCommRefToOfferSolution_TEST
{
    static testMethod void testVFC_AddCommRefToOfferSolution() 
    {        
        Offer_Lifecycle__c offcycle = new Offer_Lifecycle__c (Name='test',Solution_Name__c='Test5555#',Description__c='test',Segment_Marketing_VP__c=UserInfo.getUserId(),Leading_Business_BU__c='Energy',Launch_Owner__c=UserInfo.getUserId(),Process_Type__c='PMP');
        insert offcycle;
          
        ApexPages.StandardController controller = new ApexPages.StandardController(offcycle);   
        VFC_AddCommRefToOfferSolution GMRSearchPage = new VFC_AddCommRefToOfferSolution(controller);
        PageReference pageRef = Page.VFP_AddCommRefToOfferSolution;
        String jsonSelectString = '[{"subFamily_type_info":["subFamily","http://bridgefrontoffice.schneider-electric.com","labelValue","0","1","false"],"subFamily":{"value_type_info":["value","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"value":"77","label_type_info":["label","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"label":"Test Sub Family","field_order_type_info":["label","value"],"apex_schema_type_info":["http://bridgefrontoffice.schneider-electric.com","false","false"]},"strategicProductFamily_type_info":["strategicProductFamily","http://bridgefrontoffice.schneider-electric.com","labelValue","0","1","false"],"strategicProductFamily":{"value_type_info":["value","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"value":"33","label_type_info":["label","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"label":"Test Product Family","field_order_type_info":["label","value"],"apex_schema_type_info":["http://bridgefrontoffice.schneider-electric.com","false","false"]},"productSuccession_type_info":["productSuccession","http://bridgefrontoffice.schneider-electric.com","labelValue","0","1","false"],"productSuccession":{"value_type_info":["value","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"value":"66","label_type_info":["label","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"label":"Test Product Succession","field_order_type_info":["label","value"],"apex_schema_type_info":["http://bridgefrontoffice.schneider-electric.com","false","false"]},"productLine_type_info":["productLine","http://bridgefrontoffice.schneider-electric.com","labelValue","0","1","false"],"productLine":{"value_type_info":["value","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"value":"22","label_type_info":["label","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"label":"Test Product Line","field_order_type_info":["label","value"],"apex_schema_type_info":["http://bridgefrontoffice.schneider-electric.com","false","false"]},"productGroup_type_info":["productGroup","http://bridgefrontoffice.schneider-electric.com","labelValue","0","1","false"],"productGroup":{"value_type_info":["value","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"value":"55","label_type_info":["label","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"label":"Test Product Group","field_order_type_info":["label","value"],"apex_schema_type_info":["http://bridgefrontoffice.schneider-electric.com","false","false"]},"field_order_type_info":["businessLine","commercialReference","description","family","productGroup","productLine","productSuccession","strategicProductFamily","subFamily"],"family_type_info":["family","http://bridgefrontoffice.schneider-electric.com","labelValue","0","1","false"],"family":{"value_type_info":["value","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"value":"44","label_type_info":["label","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"label":"Test Family","field_order_type_info":["label","value"],"apex_schema_type_info":["http://bridgefrontoffice.schneider-electric.com","false","false"]},"description_type_info":["description","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"description":"Test Description","commercialReference_type_info":["commercialReference","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"commercialReference":"Test Commercial Reference","businessLine_type_info":["businessLine","http://bridgefrontoffice.schneider-electric.com","labelValue","0","1","false"],"businessLine":{"value_type_info":["value","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"value":"11","label_type_info":["label","http://www.w3.org/2001/XMLSchema","string","0","1","false"],"label":"Test Business Line","field_order_type_info":["label","value"],"apex_schema_type_info":["http://bridgefrontoffice.schneider-electric.com","false","false"]},"apex_schema_type_info":["http://bridgefrontoffice.schneider-electric.com","false","false"]}]';        
        pageRef.getParameters().put('jsonSelectString',jsonSelectString);
        pageRef.getParameters().put('Id',offcycle.Id);
        System.debug('$$$$$'+pageRef);
        Test.setCurrentPage(pageRef);
        GMRSearchPage.jsonSelectString=jsonSelectString;
        VFC_AddCommRefToOfferSolution.remoteSearch('test','02CABFCW',FALSE,FALSE,FALSE,FALSE,FALSE);
        VFC_AddCommRefToOfferSolution.remoteSearch('test','02CABF',FALSE,FALSE,FALSE,FALSE,FALSE);
        VFC_AddCommRefToOfferSolution.remoteSearch('test','02CA',FALSE,FALSE,FALSE,FALSE,FALSE);
        VFC_AddCommRefToOfferSolution.remoteSearch('test','02',FALSE,FALSE,FALSE,FALSE,FALSE);
        VFC_AddCommRefToOfferSolution.getOthers('02CABFCW');
        Test.startTest(); 
        GMRSearchPage.performCheckbox();
        GMRSearchPage.pageCancelFunction();
        Test.stopTest(); 
   }
}