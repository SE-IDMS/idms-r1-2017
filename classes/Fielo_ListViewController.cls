/************************************************************
* Developer: Ramiro Ichazo                                  *
* Type: VF Controller                                       *
* VisualForce: Fielo_ListView                               *
* Created Date: 08.09.2014                                  *
* Description:                                              *
************************************************************/

public class Fielo_ListViewController {
    
    public string debug {get;set;}
    
    //Variables PUBLICAS
    public List<SelectOption> listViews {get;set;}
    public Fielo_ListView__c currentLV {get;set;}
    public Fielo_GeneralPager pager {get;set;}
    public String listViewSelected {get;set;}
    public List<string> listFields {get;set;}
    public String searchVal{get;set;}
    public Integer pagenum {get;set;}
    
    //Variables PRIVADAS
    private map<Id, Fielo_ListView__c> relatedViews;
    private Fielo_CRUDObjectSettings__c crudOS;
    
    //Variables para usar desde el Where Condition
    private Id userId = UserInfo.getUserId();
    private Id recordId;
    private Id memberId;
    
    //Variables usadas en el ordenamiento por Columnas.
    public String fieldToOrder {get;set;}
    public String orderAscOrDesc {get;set;}
    private String defaultOrderFields;
    private String whereClause;
    private String limitClause;
    private String orderClause;
    
    //-----------------------------------------------------//
    //******************CONSTRUCTOR************************//
    //-----------------------------------------------------//
    public Fielo_ListViewController(){
        
        memberId = FieloEE.memberUtil.getmemberId();
        
        //Query al Fielo Component
        FieloEE__Component__c fComp = [SELECT F_CRUDObjectSettings__c FROM FieloEE__Component__c WHERE Id =: ApexPages.currentPage().getParameters().get('fComponentId')];
        
        //Query al CRUS SObject Settings
        String allFields = '';
        for(Schema.SObjectField nameAPIfield : Schema.SObjectType.Fielo_ListView__c.fields.getMap().values()){
            allFields += allFields == '' ? String.ValueOf(nameAPIfield): ', '+String.ValueOf(nameAPIfield);
        }
        crudOS = Database.query('SELECT Id, F_ObjectApiName__c, (SELECT '+allFields+' FROM F_ListViews__r) FROM Fielo_CRUDObjectSettings__c WHERE Id = \''+fComp.F_CRUDObjectSettings__c+'\'');
        
        if(!crudOS.F_ListViews__r.isEmpty()){
            //Busco el Default y armo el picklist de vistas
            listViews = new List<SelectOption>();
            for(Fielo_ListView__c listView: crudOS.F_ListViews__r){
                listViews.add(new SelectOption(listView.Id, listView.Name));
                if(listView.F_isDefault__c){
                    currentLV = listView;
                }
            }
            
            //Si no existe el default agarro el primero
            if(currentLV == null){
                currentLV = crudOS.F_ListViews__r.get(0);
            }
            
            //Seteo el current List View
            listViewSelected = currentLV.Id;
            
            settingListView();
            //Genero el paginado
            generatePager();
            pagenum = pager.getPageNumber();
        }else{
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, Label.Fielo_ListViewControllerNoListView));
        }
    }
        
    //-----------------------------------------------------//
    //*************METODOS DE VISTAS***********************//
    //----------------------------------------------------//
    public PageReference doChangeView(){
        currentLV = relatedViews.get(listViewSelected);
        settingListView();
        searchVal = '';
        generatePager();
        return null;
    }
    
    private void settingListView(){
        //Cargo la lista de campos a mostrar
        listFields = currentLV.F_FieldSet__c.split(',');
        
        //Cargo los "Clause" del query y la columna que se ordeno
        if (currentLV.F_whereCondition__c != null && currentLV.F_whereCondition__c.contains('ORDER BY')){
            //Con ORDER BY
            whereClause = currentLV.F_whereCondition__c.split('ORDER BY').get(0);
            orderClause = currentLV.F_whereCondition__c.split('ORDER BY').get(1).split('LIMIT').get(0);
            limitClause = currentLV.F_whereCondition__c.split('ORDER BY').get(1).split('LIMIT').get(0).split('LIMIT').size() > 1 ? currentLV.F_whereCondition__c.split('ORDER BY').get(1).split('LIMIT').get(0).split('LIMIT').get(1) : '';
            //Seteo el campo a ordenar y el modo
            fieldToOrder = (orderClause.split(',').get(0)).trim().split(' ').get(0);
            orderAscOrDesc = orderClause.split(',').get(0).containsIgnoreCase('DESC') ? 'DESC' : 'ASC';
        }else{
            //Sin ORDER BY
            whereClause = currentLV.F_whereCondition__c != null ? currentLV.F_whereCondition__c.split('LIMIT').get(0) : '';
            orderClause = '';
            limitClause = currentLV.F_whereCondition__c != null && currentLV.F_whereCondition__c.contains('LIMIT') ? currentLV.F_whereCondition__c.split('LIMIT').get(1) : '';
            //Seteo como default el campo a ordenar "CreatedDate" y el modo DESC
            fieldToOrder = 'CreatedDate';
            orderAscOrDesc = 'DESC';
        }
    }
    
    //---------------------------------------------------//
    //****************METODOS DE PAGINADO****************//
    //---------------------------------------------------//
    public void generatePager(){
        //ARMO EL QUERY
        
        //Seteo el SELECT
        string query = 'SELECT Id, ';
        query += currentLV.F_FieldSet__c;
        
        //Seteo el FROM
        query += ' FROM ' + currentLV.F_ObjectAPIName__c;
        
        //Seteo el WHERE
        query += ' ' + whereClause;
        
        //Armo y seteo el ORDER BY
        String orderClauseAux;
        if(orderClause == ''){
            debug += ' no tiene default order fields ';
            orderClauseAux =' ORDER BY ' +fieldToOrder + ' ' + orderAscOrDesc + ' NULLS LAST';
        }else if (orderClause.contains(fieldToOrder)){
            debug += ' tiene default order fields y contiene columna que queremos ordenar ';
            String orderFields = '';
            for (String field : orderClause.split(',')){
                if (!field.contains(fieldToOrder.trim())){
                    orderFields += ', ' + field;
                }
            }
            orderClauseAux = ' ORDER BY ' + fieldToOrder +' ' + orderAscOrDesc + ' NULLS LAST' + orderFields;
        }else{
            debug += ' tiene default order fields  ';
            orderClauseAux = ' ORDER BY ' + fieldToOrder +' ' + orderAscOrDesc + ' NULLS LAST, ' + orderClause;
        }
        query += orderClauseAux;
        
        //seteo el LIMIT
        query += limitClause != NULL && limitClause != '' ? ' LIMIT ' + limitClause : '';
        
        //Seteo el Search
        if(searchVal != null && searchVal != '' && currentLV.F_SearchFields__c != null){
            String searchCond = '';
            for (String field : currentLV.F_SearchFields__c.split(',')){
                searchCond += searchCond == '' ? field.trim() + ' LIKE \'%'+searchVal+'%\'' : ' OR '+field.trim() + ' LIKE \'%'+searchVal+'%\'';
            }
            searchCond = 'WHERE ' + '('+searchCond+')';
            query = query.contains('WHERE') ? query.replace('WHERE ', searchCond+' AND ') : query.replace('ORDER BY', searchCond + ' ORDER BY');
        }
        
        //Genero el paginador
        debug += query;
        System.debug (debug);
        System.debug(query);
        try{
            pager = new Fielo_GeneralPager(Database.getQueryLocator(query), currentLV.F_PageSize__c != null ? Integer.ValueOf(currentLV.F_PageSize__c) : 10);
        }catch (Exception e){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }
    
    public PageReference doSetPageNumber(){
        if(pager != null){
            if(pageNum < 1 || pageNum > pager.getTotalPages()){
                pageNum = 1;
            }
            pager.setPageNumber(pageNum);
            pagenum = pager.getPageNumber();
        }
        return null;
    }
    
    public PageReference doNext(){
        pager.Next();
        pagenum = pager.getPageNumber();
        return null;
    }
    
    public PageReference doPrevious(){
        pager.Previous();
        pagenum = pager.getPageNumber();
        return null;
    }
    
    public void doOrderColumn(){
        String columnToOrder = Apexpages.currentPage().getParameters().get('columnToOrder');
        if (columnToOrder == fieldToOrder){
            orderAscOrDesc = orderAscOrDesc == 'DESC' ? 'ASC' : 'DESC';
        }else{
            orderAscOrDesc = 'DESC';
            fieldToOrder = columnToOrder;
        }
        generatePager();
    }
    
    //-------------------------------------------------------------------//
    //***************METODOS DE REDIRECCIONAMIENTO***********************//
    //------------------------------------------------------------------//
    public PageReference doCreate(){
        PageReference pageRet = Fielo_Utilities.getMenuReferenceById(currentLV.F_RedirectNew__c);
        pageRet.getParameters().put('mode', 'c');
        pageRet.setRedirect(true);
        return pageRet;
    }
    
    public PageReference doRead(){
        string recordId = system.currentPageReference().getParameters().get('recordId');
        PageReference pageRet = Fielo_Utilities.getMenuReferenceById(currentLV.F_RedirectDetails__c);
        pageRet.getParameters().put('recordId', recordId);
        pageRet.getParameters().put('mode', 'r');
        pageRet.setRedirect(true);
        return pageRet;
    }
    
    public PageReference doUpdate(){
        string recordId = system.currentPageReference().getParameters().get('recordId');
        PageReference pageRet = Fielo_Utilities.getMenuReferenceById(currentLV.F_RedirectEdit__c);
        pageRet.getParameters().put('recordId', recordId);
        pageRet.getParameters().put('mode', 'u');
        pageRet.setRedirect(true);
        return pageRet;
    }

    public PageReference doDelete(){
        string recordId = system.currentPageReference().getParameters().get('recordId');
        PageReference pageRet = Fielo_Utilities.getMenuReferenceById(currentLV.F_RedirectDelete__c);
        pageRet.getParameters().put('recordId', recordId);
        pageRet.getParameters().put('mode', 'd');
        pageRet.setRedirect(true);
        return pageRet;
    }
}