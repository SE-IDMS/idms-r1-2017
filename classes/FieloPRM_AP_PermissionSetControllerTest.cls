/**************************************************************************************
    Author: Fielo Team
    Date: 30/06/2015
    Description: 
    Related Components: <Component1> / <Componente2>...
***************************************************************************************/
@isTest
private class FieloPRM_AP_PermissionSetControllerTest{
    public static testMethod void testUnit1(){
        PermissionSet ps = new PermissionSet(name='testpermission', label='myps');
        insert ps;

        FieloPRM_AP_PermissionSetController controller = new FieloPRM_AP_PermissionSetController();
        controller.doRetrieve();
        controller.doDeploy();
        controller.doExport();
        controller.doImport();
        controller.permissionSetId = ps.id;
        controller.fileName = 'test';
        controller.documentBody = blob.valueOf('test');
        controller.getItems();
        controller.doRetrieve();
        controller.doDeploy();
        controller.doExport();
        controller.doImport();
    }
}