global class AP_PRMGroupMemberEmailHandler implements Messaging.InboundEmailHandler {
      
      global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
          Messaging.InboundEmailResult result = updateContactUser(email);
          return result;
      }
      
      private Messaging.InboundEmailResult updateContactUser(Messaging.InboundEmail email){
          
          Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
          
          try {
          
              System.debug('**Email**'+email);
              
              String conId = email.Subject.substringAfter('ContactId: ');
              System.debug('**Contact Id**'+conId);
              
              Contact newCon = [SELECT Id,Name, LastModifiedDate FROM Contact WHERE Id =:conId];
              System.debug('**Contact **'+newCon);
              Map<String,Object> params = new Map<String,Object>();
              
              if(newCon != null){
                  params.put('secondaryContact',newCon);
                  //params.put('contactId',newCon.Id);
              }
              
              Flow.Interview.UpdateUserOnDecliningPRMRegistration PRMPrimaryEmailFlow = new Flow.Interview.UpdateUserOnDecliningPRMRegistration(params);
              PRMPrimaryEmailFlow.start();
              
              result.success = true;
              
          }
          catch(exception e) {
            result.success = false;
            result.message = e.getMessage();
            System.debug('**** An exception ****'+e.getMessage());
        }
        return result;
      }
  }