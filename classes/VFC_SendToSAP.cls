public class VFC_SendToSAP
{
    Public ID WorkOrderNotificationId {get; set;}
    public ApexPages.StandardController standardController;
    private WorkOrderNotification__c currentWON = new WorkOrderNotification__c();
    public boolean isITBWON{get;set;}
    public boolean isError{get;set;}
    public boolean closeWindow{get;set;}{closeWindow=false;}
    
    public VFC_SendToSAP(ApexPages.StandardController controller) 
    {
       standardController = controller;
       currentWON = (WorkOrderNotification__c)controller.getRecord();
       WorkOrderNotificationId = controller.getId();
       isITBWON=false;
       isError=false;          
    }
    public pageReference CallSAP(){
        PageReference pageResult;
        String sessionId = UserInfo.getSessionId();
        String sfURL =System.URL.getSalesforceBaseURL().getHost();
        ID userID=UserInfo.getUserId();
        WorkOrderNotification__c WonObj=[SELECT Case__r.account.SEAccountId__c FROM WorkOrderNotification__c where id =:WorkOrderNotificationId limit 1];
        if(WonObj.Case__r.account.SEAccountId__c!=null){
            String AccountId = WonObj.Case__r.account.SEAccountId__c;
        }
        //pageResult=new PageReference('https://zcewotc01.eur.gad.schneider-electric.com:444/sch.bfoint.bFOIntHandler.wcp?appctx=WON.SYNC&boid=ISYS_SAP&pid=BFO&WONid='+WorkOrderNotificationId+'&sid='+sessionId+'&surl='+sfURL+'&uid='+userID+'&aid='+WonObj.Case__r.account.SEAccountId__c);   
        pageResult=new PageReference(System.Label.CLNOV15CCC03+'sch.bfoint.bFOIntHandler.wcp?appctx=WON.SYNC&boid=ISYS_SAP&pid=BFO&WONid='+WorkOrderNotificationId+'&sid='+sessionId+'&surl='+sfURL+'&uid='+userID+'&aid='+WonObj.Case__r.account.SEAccountId__c);   
        
        return pageResult;
    }
  
}