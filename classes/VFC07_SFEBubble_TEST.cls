/*
    Author          : Grégory GACKIERE (SFDC) 
    Date Created    : 12/11/2010
    Description     : Test class for Controller SFE Bubble.  Verifies that Platform Scoring records are well retrieved 
                        in order to be used for the Flash file initialization
*/
@isTest
private class VFC07_SFEBubble_TEST {

    static testMethod void testGetBubblesController() { 
        //Setup test data
        User testUser = Utils_TestMethods.createStandardUser('tst');
        testUser.CAPRelevant__c = true;
        testUser.BypassVR__c = true;
        testUser.BypassWF__c = true;
        insert testUser;
        System.RunAs(testUser) {//You can't create/edit a platforming scoring record you're not in charge of        

        SFE_IndivCAP__c indivCAP = Utils_TestMethods.createIndividualActionPlan(testUser.Id);
        indivCAP.AssignedTo__c = testUser.Id;
        insert indivCAP;
        
        List<Account> accList = new List<Account>();
        for(Integer i = 0; i < 5; i ++){  
            accList.add(Utils_TestMethods.createAccount(testUser.Id));
        }
        insert accList;
        String result;
                
        //Initialise controller etc.
        PageReference pageRef = Page.VFP07_SFEBubble;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', indivCAP.Id);
        VFC07_SFEBubble c = new VFC07_SFEBubble(new ApexPages.StandardController(indivCAP));
        //Pre-checks
        List<SFE_PlatformingScoring__c> platformRecords = [select id from SFE_PlatformingScoring__c where IndivCAP__c = :indivCAP.Id];
        System.assertEquals(0, platformRecords.size()); 
        
        Map<Id, SFE_PlatformingScoring__c> mapPlatformRecords = new Map<Id, SFE_PlatformingScoring__c>();
        Integer count = 0;
        for(Account a : accList){
            count++;
            System.debug('count : ' + count);
            mapPlatformRecords.put(a.Id, new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, PlatformedAccount__c = a.Id, Score__c = (count*10), MktShare__c = (count*10) ));
        }
        
        insert(mapPlatformRecords.values());
        
        //Check prerequisites
        List<SFE_PlatformingScoring__c> records = [select id,Score__c,MktShare__c from SFE_PlatformingScoring__c where IndivCAP__c = :indivCAP.Id];
        System.assertEquals(5, records.size());    //i.e. 5 accounts the user owners
        System.debug('### record : ' + (records.get(0)).Score__c);
        //Simulate page entry view
        Test.startTest();
        //Test one with 5 "good" platform scroring
        result = c.getBubbles();
        System.assertNotEquals(null, result);
        result = c.getFlashVars();
        System.assertNotEquals(null, result);
        System.assertEquals(null, c.refreshSFEBubbleCtrl());
        //Test.stopTest();
        System.debug('### result : ' + result);
        //Return property bNoErrors for coverage
        Boolean bTest = c.bNoErrors;
        System.assert(true, bTest);
        //to ensure good coverage
        c.bTestFlag = true;
        result = c.getBubbles();
        //Test two with 5 "good" platform scroring & a bad one
        //create a new account
        Account lastAcc1 = Utils_TestMethods.createAccount(testUser.Id);
        insert(lastAcc1);
        SFE_PlatformingScoring__c lastPlatScor1 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, PlatformedAccount__c = lastAcc1.Id,Score__c = null, MktShare__c = 1); 
        lastPlatScor1.DirectSalesPlatforming__c=20;
        lastPlatScor1.IndirectSalesPlatforming__c=5;
        lastPlatScor1.PAMPlaforming__c=30;   
        insert(lastPlatScor1);
        result = c.getFlashVars();
        System.assertNotEquals(null,c.sfeIndivCAP.Id);
        bTest = c.bNoErrors;
        System.assert(!bTest);
      //  System.assertEquals(null, c.getBubbles());
        //Test two with 5 "good" platform scroring & a bad one
        //create a new account
        Account lastAcc2 = Utils_TestMethods.createAccount(testUser.Id);
        insert(lastAcc2);
        SFE_PlatformingScoring__c lastPlatScor2 = new SFE_PlatformingScoring__c(IndivCAP__c = indivCAP.Id, PlatformedAccount__c = lastAcc2.Id,Score__c = 1, MktShare__c = 1); 
        lastPlatScor2.DirectSalesPlatforming__c=20;
        lastPlatScor2.IndirectSalesPlatforming__c=5;
        lastPlatScor2.PAMPlaforming__c=30;
        insert(lastPlatScor2);
        result = c.getFlashVars();
        bTest = c.bNoErrors;
        System.assert(!bTest);
       // System.assertEquals(null, c.getBubbles());
        c.bTestFlag = true; 
        result = c.getFlashVars();
       // System.assertEquals(null, c.getBubbles());
        System.assert(!bTest);
        c.setSfeIndivCAP(indivCAP);
        c.getSfeIndivCAP();
        c.display();       
        Test.stopTest(); 
        }
    }
}