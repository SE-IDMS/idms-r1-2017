/********************************************************************
* Company: Fielo
* Created Date: 09/07/2016
* Description: 
********************************************************************/
public without sharing class FieloPRM_MemberTrigger{ 
    
    public static Boolean isAlreadyRun = false;
    
    /********************************************************************
    METHOD: syncrhonizeContactAndAccount
    MODE: after insert
    DML: UPDATE FieloEE__Member__c
    COMMENTS:
    ********************************************************************/
    public static void syncrhonizeContactAndAccount(List<FieloEE__Member__c> triggernew){
        //Query All Fields Member
        String memberquery = 'SELECT FieloEE__User__r.ContactId, ';
        for(String fieldName: Schema.getGlobalDescribe().get('FieloEE__Member__c').getDescribe().fields.getMap().keySet()){
           memberquery += fieldName + ', ';
        }
        memberquery = memberquery.left(memberquery.lastIndexOf(','));
        memberquery += ' FROM ' + ' FieloEE__Member__c ' + ' WHERE Id IN: triggernew';
        List<FieloEE__Member__c> listMember = (List<FieloEE__Member__c>)Database.query(memberquery);
        
        set<id> setIdContact = new set<id>();
        set<id> setIdAccount = new set<id>();
        for(FieloEE__Member__c member: listMember){
            setIdContact.add(member.FieloEE__User__r.ContactId);
            setIdAccount.add(member.F_Account__c);
        }
        
        //Query All Fields Contact
        String queryContact = 'SELECT ';
        for(String fieldName : Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet()){
           queryContact += fieldName + ', ';
        }
        queryContact = queryContact.left(queryContact.lastIndexOf(','));
        queryContact += ' FROM Contact WHERE Id IN: setIdContact';
        List<Contact> listContact = (List<Contact>)Database.query(queryContact);
        
        //Query All Fields Account
        String queryAccount = 'SELECT ';
        for(String fieldName : Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keySet()) {
           queryAccount += fieldName + ', ';
        }
        queryAccount = queryAccount.left(queryAccount.lastIndexOf(','));
        queryAccount += ' FROM Account WHERE Id In: setIdAccount';
        List<Account> listAccount = (List<Account>)Database.query(queryAccount);
        
        //Fielo Syncrhonize Contact
        if(!test.isRunningTest()){
            if(listContact != null && listMember != null){
                SyncObjects instance2 = new SyncObjects(listContact);
                instance2.toSincronize = listMember ;
                boolean allornone2 = false;
                //DML: UPDATE FieloEE__Member__c
                SyncObjectsResult result2 = instance2.syncronize(allornone2);
            }
        }
        
        //Fielo Syncrhonize Account
        if(!test.isRunningTest()){
            if(listAccount != null && listMember != null){
                SyncObjects instance = new SyncObjects(listAccount);
                instance.toSincronize = listMember;
                boolean allornone = false;
                //DML: UPDATE FieloEE__Member__c
                SyncObjectsResult result = instance.syncronize(allornone);
            }   
        }
    }
    
    /********************************************************************
    METHOD: searchMemberProperties
    MODE: after insert
    DML: UPDATE MemberExternalProperties__c
    COMMENTS:
    ********************************************************************/
    public static void searchMemberProperties(list<FieloEE__Member__c> triggernew){
        map<string, FieloEE__Member__C> mapUIMSidMember = new map<string, FieloEE__Member__C>();
        
        for(FieloEE__Member__c member: triggernew){
            if(member.PRMUIMSId__c != null){
                mapUIMSidMember.put(member.PRMUIMSId__c,member);
            }
        }
        
        if(!mapUIMSidMember.isEmpty()){
            set<String> setMemberid = mapUIMSidMember.keySet();
            set<string> setFieldsMemberProp =  Schema.SObjectType.MemberExternalProperties__c.fields.getMap().keySet();
            list<string> listFieldsMemberProp = new list<string>();
            listFieldsMemberProp.addAll(setFieldsMemberProp);
            String soqlMemberProperties = 'SELECT ' + String.join(listFieldsMemberProp, ',') + ' FROM ' + ' MemberExternalProperties__c ' + ' WHERE PRMUIMSId__c in : setMemberid ';
            List<MemberExternalProperties__c> listMemberProperties = (List<MemberExternalProperties__c>) Database.query(soqlMemberProperties);
            
            list<MemberExternalProperties__c> listMemberPropertiesToUpdate = new list<MemberExternalProperties__c>();
            for(MemberExternalProperties__c memberProperties: listMemberProperties){
                if(mapUIMSidMember.containskey(memberProperties.PRMUIMSId__c)){
                    memberProperties.Member__c = mapUIMSidMember.get(memberProperties.PRMUIMSId__c).id;
                    listMemberPropertiesToUpdate.add(memberProperties);
                }
            }
            if(listMemberPropertiesToUpdate.size() > 0){
                //DML: UPDATE MemberExternalProperties__c
                update listMemberPropertiesToUpdate;
            }
        }
    }
    
    /********************************************************************
    METHOD: createMemberHistory
    MODE: after update
    DML: INSERT FieloPRM_MemberUpdateHistory__c
    COMMENTS:
    ********************************************************************/
    public static void createMemberHistory(map<id, FieloEE__Member__c> triggernew, map<id,FieloEE__Member__c> triggerold){
        list<FieloPRM_MemberUpdateHistory__c> listMemberHistory = new list<FieloPRM_MemberUpdateHistory__c>();
        for(FieloEE__Member__c member : triggernew.values()){
            if(triggerold.containskey(member.id) && member.F_PRM_ProgramLevels__c != triggerold.get(member.id).F_PRM_ProgramLevels__c ){
                FieloPRM_MemberUpdateHistory__c memberhistory = new FieloPRM_MemberUpdateHistory__c();
                memberhistory.F_PRM_Member__c = member.id;
                memberhistory.F_PRM_Type__c = 'Program Level';
                listMemberHistory.add(memberhistory);
            }
        }
        //DML: INSERT FieloPRM_MemberUpdateHistory__c
        insert listMemberHistory;
    }
    
    /********************************************************************
    METHOD: isActiveAccount
    MODE: after update
    DML: UPDATE Account
    COMMENTS:
    ********************************************************************/
    public static void isActiveAccount(list<FieloEE__Member__c> triggernew, map<id,FieloEE__Member__c> triggeroldmap){
        set<id> setIdAccountActive = new set<id>();
        set<id> setIdAccountInactive = new set<id>();
        for(FieloEE__Member__c member: triggernew){
            if(triggeroldmap.containskey(member.id) &&  triggeroldmap.get(member.id).F_PRM_IsActivePRMContact__c !=  member.F_PRM_IsActivePRMContact__c && member.F_PRM_IsActivePRMContact__c && member.F_Account__c != null){
                setIdAccountActive.add(member.F_Account__c);
            }
            if(triggeroldmap.containskey(member.id) &&  triggeroldmap.get(member.id).F_PRM_IsActivePRMContact__c !=  member.F_PRM_IsActivePRMContact__c && !member.F_PRM_IsActivePRMContact__c && member.F_Account__c != null){
                setIdAccountInactive.add(member.F_Account__c);
            }
        }
       
        list<Account> listAccountUpdate = new list<Account>();
        if(!setIdAccountActive.isEmpty()){
            list<Account> listAccountActive = [SELECT id, PRMIsActiveAccount__c FROM Account WHERE PRMIsActiveAccount__c = false AND id in: setIdAccountActive];
            for(Account acc : listAccountActive){
                acc.PRMIsActiveAccount__c = true;
                listAccountUpdate.add(acc);
            }
        }
        if(!setIdAccountInactive.isEmpty()){
            list<Account> listAccountInactive = [SELECT id, PRMIsActiveAccount__c, (SELECT ID FROM Members__r WHERE F_PRM_IsActivePRMContact__c = true limit 1) FROM Account WHERE PRMIsActiveAccount__c = true AND id in: setIdAccountInactive];
            
            for(Account acc : listAccountInactive){
                if(acc.Members__r == null || acc.Members__r.size() ==0){
                    acc.PRMIsActiveAccount__c = false;
                    listAccountUpdate.add(acc);
                }
            }
        }
        if(!listAccountUpdate.isEmpty()){
            //DML: UPDATE Account
            update listAccountUpdate;
        }
    }
    
    /********************************************************************
    METHOD: hybridSegment
    MODE: after update
    DML: INSERT FieloEE__MemberSegment__c, DELETE FieloEE__MemberSegment__c
    COMMENTS:
    ********************************************************************/
    public static void hybridSegment(map<id,FieloEE__Member__c> triggernewmap, map<id,FieloEE__Member__c> triggeroldmap){
       
        set<String> setCountryCodes = new set<String>{'WW'};
        set<String> setRegionCode = new set<String>();
        for(FieloEE__Member__c m :triggernewmap.values()){
            setCountryCodes.add(m.F_PRM_CountryCodeInjectQuery__c);
            setRegionCode.add(m.F_PRM_RegionInjectQuery__c);
        }
        
        map<id,FieloEE__RedemptionRule__c> mapSegments = new map<id,FieloEE__RedemptionRule__c>([SELECT Id, RecordType.DeveloperName, RecordType.Name, CountryCode__c, F_PRM_Country__r.F_PRM_IsRegion__c, F_PRM_Country__r.F_PRM_ITBRegion__c, 
                                                                                                 (SELECT Id, Name, FieloEE__BooleanValue__c, FieloEE__DateValue__c, FieloEE__FieldName__c, FieloEE__FieldType__c, FieloEE__Operator__c, FieloEE__Values__c, FieloEE__Name__c, FieloEE__NumberValue__c
                                                                                                 FROM FieloEE__Redemption_Criterias__r)
                                                                                                 FROM FieloEE__RedemptionRule__c
                                                                                                 WHERE
                                                                                                 RecordType.DeveloperName  = 'Manual' AND 
                                                                                                 F_PRM_Type__c = 'Hybrid' AND
                                                                                                 FieloEE__isActive__c = true AND
                                                                                                 (CountryCode__c IN: setCountryCodes OR (F_PRM_Country__r.F_PRM_IsRegion__c = TRUE AND F_PRM_Country__r.F_PRM_ITBRegion__c =: setRegionCode))
                                                                                                ]);
          
        if(!mapSegments.values().isEmpty()){
            set<string> setFieldsMember = new set<String>();
            set<id> setMemberid = triggernewmap.keySet();
            Boolean isCrossfield = false;
            
            for(Schema.FieldSetMember f: SObjectType.FieloEE__Member__c.FieldSets.F_PRM_HybridSegment.getFields()){
                setFieldsMember.add(f.getFieldPath());
            }
            
            for(FieloEE__RedemptionRule__c segment: mapSegments.values()){
                for(FieloEE__RedemptionRuleCriteria__c criteria: segment.FieloEE__Redemption_Criterias__r){
                    //TODO Fielo TEAM Change this implementation it's workarount
                    String fieldName = criteria.FieloEE__FieldName__c;
                    if(fieldName != 'PRMUIMSId__c'){
                        if(fieldName.left(6) != 'F_PRM_' && fieldName.left(2) != 'F_' && fieldName.left(9) != 'FieloEE__'){
                            fieldName = 'FieloEE__' + fieldName;
                        }
                    }
                    setFieldsMember.add(fieldName);
                    if(criteria.FieloEE__FieldName__c.contains('.')){
                        isCrossfield = true;
                    }
                }
            }
            
            List<FieloEE__Member__c> listMembers = new List<FieloEE__Member__c>();
            if(isCrossfield){
                list<string> listFieldsMember = new list<string>();
                listFieldsMember.addAll(setFieldsMember);
                String soqlMember = 'SELECT ' + String.join(listFieldsMember, ',') + ' FROM ' + ' FieloEE__Member__c ' + ' WHERE Id IN: setMemberid';
                listMembers = (List<FieloEE__Member__c>)Database.query(soqlMember);
            }else{
                listMembers.addAll(triggernewmap.values());
            }
            
            List<FieloEE__Member__c> listMembersToprocess = new List<FieloEE__Member__c>();
            if((FieloEE__Triggers__c.getInstance(UserInfo.getUserId()).Id != null && FieloEE__Triggers__c.getInstance(UserInfo.getUserId()).F_PRM_ForceHybridSegment__c) ){
                listMembersToprocess.addAll(listMembers);
            }else{
                if(triggeroldmap != null){
                    for(FieloEE__Member__c mNew: listMembers){
                        if(triggeroldmap.containskey(mNew.id)){
                            FieloEE__Member__c mOld = triggeroldmap.get(mNew.id);
                            for(String field: setFieldsMember){
                                field = field.split('\\.')[0].toLowerCase();
                                field = (field.endsWith('__r')) ? (field.removeEnd('__r') + '__c') : (field);
                                
                                Boolean fieldDif = false;
                                if(mNew.get(field) != null && mOld.get(field) != null){
                                    if(!String.ValueOf(mNew.get(field)).equals(String.ValueOf(mOld.get(field)))){
                                        fieldDif = true;
                                    }
                                }else if(mNew.get(field) != mOld.get(field)){
                                    fieldDif = true;
                                }
                                if(fieldDif){
                                    listMembersToprocess.add(mNew);
                                    break;
                                }
                                
                            }
                        }
                    }
                }else{
                    listMembersToprocess.addAll(listMembers);
                }
            }

            if(!listMembersToprocess.isEmpty()){
                map<id, set<id>> mapMemberSegmentsNew = FieloPRM_UTILS_SegmentHybrid.segmentByMember(listMembersToprocess, mapSegments.values());

                map<id, set<id>> mapMemberSegmentsOld = new map<id, set<id>>();
                for(FieloEE__MemberSegment__c memberseg: [SELECT id, FieloEE__Member2__c, FieloEE__Segment2__c FROM FieloEE__MemberSegment__c WHERE FieloEE__Member2__c IN: setMemberid]){
                    if(mapMemberSegmentsOld.containskey(memberseg.FieloEE__Member2__c)){
                        mapMemberSegmentsOld.get(memberseg.FieloEE__Member2__c).add(memberseg.FieloEE__Segment2__c);
                    }else{
                        mapMemberSegmentsOld.put(memberseg.FieloEE__Member2__c, new set<Id>{memberseg.FieloEE__Segment2__c});
                    }
                }

                list<FieloEE__MemberSegment__c> listMemberSegmentInsert = new list<FieloEE__MemberSegment__c>();
                set<string> setMemberSegmentDelete = new set<string>();
                
                for(FieloEE__Member__c mem: triggernewmap.values()){

                    set<id> setIdsNew = new set<id>();
                    set<id> setIdsOld = new set<id>();
                    if(mapMemberSegmentsNew.containskey(mem.id)){
                        setIdsNew = mapMemberSegmentsNew.get(mem.id);
                    }
                    if(mapMemberSegmentsOld.containskey(mem.id)){
                        setIdsOld = mapMemberSegmentsOld.get(mem.id);
                    }

                    for(Id idsegment: setIdsNew){
                        if(!setIdsOld.contains(idsegment)){
                            FieloEE__MemberSegment__c newMemberSegment = new FieloEE__MemberSegment__c(
                                FieloEE__Member2__c = mem.id,
                                FieloEE__Segment2__c = idsegment
                            );
                            listMemberSegmentInsert.add(newMemberSegment);
                        }
                    }
                    
                    for(Id idsegment: setIdsOld){
                        if(!setIdsNew.contains(idsegment) && mapSegments.containskey(idsegment)){
                            setMemberSegmentDelete.add(String.valueof(mem.id) + String.valueof(idsegment));
                        }
                    }
                }
                
                if(!listMemberSegmentInsert.isEmpty()){
                    //DML: INSERT FieloEE__MemberSegment__c
                    insert listMemberSegmentInsert;
                } 
                if(!setMemberSegmentDelete.isEmpty()){
                    //DML: DELETE FieloEE__MemberSegment__c
                    delete [SELECT id FROM FieloEE__MemberSegment__c WHERE FieloEE__MemberSegmentCombination__c LIKE: setMemberSegmentDelete];
                }
            }
        }
    }
    
    /********************************************************************
    METHOD: addMemberToTeam
    MODE: after update
    DML: UPDATE FieloCH__TeamMember__c, UPDATE FieloCH__ChallengeMember__c, INSERT FieloEE__BadgeMember__c
    COMMENTS: When a member is created, if a team with his account exists, add to that team
    ********************************************************************/
    public static void addMemberToTeam(List<FieloEE__Member__c> membersNew, Map<Id, FieloEE__Member__c> membersOldMap){
        Set<Id> membersIdsUpdateTeam = new Set<Id>();
        for(FieloEE__Member__c member : membersNew){
            if(member.F_PRM_ContactRegistrationStatus__c == 'Validated' && membersOldMap.get(member.Id).F_PRM_ContactRegistrationStatus__c != 'Validated'){
                membersIdsUpdateTeam.add(member.Id);
            }
        }
        if(!membersIdsUpdateTeam.isEmpty()){
            Savepoint sp = Database.setSavepoint();
            List<FieloCH__TeamMember__c> newTeamMembers;
            try{
                //DML: UPDATE FieloCH__TeamMember__c
                newTeamMembers = FieloPRM_Utils_ChTeam.addTeamMember(membersIdsUpdateTeam);
            }catch(Exception e){
                Database.rollback(sp);
                return;
            }
            try{
                if(!newTeamMembers.isEmpty()){
                    //DML: UPDATE FieloCH__ChallengeMember__c
                    //DML: INSERT FieloEE__BadgeMember__c
                    FieloPRM_Utils_ChTeam.membersLeveling(newTeamMembers);
                }
            }catch(Exception e){
                Database.rollback(sp);
            }
        }
    }
    
}