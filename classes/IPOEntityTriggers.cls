public with sharing class IPOEntityTriggers
{


    Public Static void IPOEntityBeforeInsertUpdate_ValidateEntityRecord(List<IPO_Strategic_Entity_Progress__c> CEntity){
    
    
    
  list<IPO_Strategic_Entity_Progress__c> Eprogress=new list<IPO_Strategic_Entity_Progress__c>();
    
         Eprogress=[select Scope__c,Entity__c, IPO_Strategic_Program__c,Year__c from IPO_Strategic_Entity_Progress__c];
            for(IPO_Strategic_Entity_Progress__c CEn:CEntity){
                CEn.ValidateEntityInsertUpdate__c=false;
                for(IPO_Strategic_Entity_Progress__c EP:Eprogress){
                    if(EP.IPO_Strategic_Program__c == CEn.IPO_Strategic_Program__c && EP.Year__c == CEn.Year__c){
                        if(CEn.Scope__c == EP.Scope__c && CEn.Entity__c == EP.Entity__c){
                    
                           CEn.ValidateEntityInsertUpdate__c=true;
                        }
                                         
                       }               
                    }
                }
            }
    
 
    Public Static void IPOEntityBeforeInsertUpdate_ValidateScopeEntity(List<IPO_Strategic_Entity_Progress__c> CEntity)
             {
             
      
                System.Debug('****** ConnectEntityBeforeInsertUpdate Start ****');
              list<IPO_Strategic_Program__c> Program = new List<IPO_Strategic_Program__c>(); 
              
              
              
              integer PW;
              integer GB;
              integer GSC;
              integer PAR;
              integer GF;
              
              Program = [select Global_Functions__c, Global_Business__c, Operation__c,  Name,Year__c from IPO_Strategic_Program__c];
              for(IPO_Strategic_Entity_Progress__c CE:CEntity){
              
              
                     PW = 0; 
                     GB = 0;
                     GSC = 0;
                     GF = 0;
                     PAR = 0;
                     CE.ValidateScopeInsertUpdate__c = false;
                     for(IPO_Strategic_Program__c P:Program){
                     
                     // Check if the Scope in Null in Program or Entity
                     
                      if(P.Name == CE.IPO_Strategic_Program_Name__c && P.Year__c == CE.Year__c) {
                      if(P.Global_Functions__c != null){
                                  if((CE.Scope__c == Label.Connect_Global_Functions) && !(P.Global_Functions__c.Contains(CE.Entity__c)))
                                  GF = GF + 1;
                                  }
                                  else if((P.Global_Functions__c == null)&& (CE.Scope__c == Label.Connect_Global_Functions))
                            GF = GF + 1;
                          if(P.Global_Business__c != null){
                                  if((CE.Scope__c == Label.Connect_Global_Business) && !(P.Global_Business__c.Contains(CE.Entity__c)))
                                  GB = GB + 1;
                                  }
                                  else if((P.Global_Business__c == null)&& (CE.Scope__c == Label.Connect_Global_Business))
                            GB = GB + 1;
                          if(P.Operation__c != null){
                                  if((CE.Scope__c == Label.Connect_Power_Region) && !(P.Operation__c.Contains(CE.Entity__c)))
                                  PW = PW + 1;
                                  }
                                  else if((P.Operation__c == null)&& (CE.Scope__c == Label.Connect_Power_Region))
                            PW = PW + 1;
                            
                            
                               } // if Ends
                               
                               if((GF > 0) ||  (GB > 0) ||  (PW > 0))
                                   CE.ValidateScopeInsertUpdate__c = True;
                               
                              } // For Ends
                             } //For Ends

                  }   
            
}