/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: This class provides methods that allow to update user email address 
**/  
public class vfc_updateEmailAddress{
    // Method to update user email address and if success redirect to userprofile page.
    public PageReference updateEmail(){
        string userid =ApexPages.currentPage().getParameters().get('id');
        try{
            user u = [select idmsnewemail__c,email,IDMS_User_Context__c,contactid,IDMS_Profile_update_source__c from user where id=:userid limit 1];
            If(u != null) {
                string personContactId  =u.ContactId;
                u.email                 =u.idmsnewemail__c;
                u.id                    =userid;
                u.username              =u.idmsnewemail__c+Label.CLJUN16IDMS71;
                update u;
                IDMSUIMSWebServiceCalls.requestEmailChangeUIMS(u.idmsnewemail__c,userid,u.IDMS_Profile_update_source__c);
                
                if(u.IDMS_User_Context__c.equalsignorecase('Home') || u.IDMS_User_Context__c.equalsignorecase('@Home')){
                    if(personContactId!=null){
                        updateAccountPerson(JSON.serialize(u),personContactId);
                    }        
                }
                PageReference pg = new PageReference('/UserLogin');
                IDMSApplicationMapping__c appMap1 = new IDMSApplicationMapping__c();
                appMap1 = IDMSApplicationMapping__c.getInstance(u.IDMS_Profile_update_source__c);
                if(appMap1!=null){
                String appIDpass          = (String)appMap1.get('appid__c');
                pg.getParameters().put('app', appIDpass);
                pg.setredirect(true);
                }
                else{
                pg.getParameters().put('app', u.IDMS_Profile_update_source__c);
                pg.setredirect(true);
                }
                return pg;
            }
            return null;
        }
        catch(exception e){
            return null;
        }
    }
    // This method is used to update user value in account object
    @future
    public static void updateAccountPerson(string userecord,string personContactId){
        user userPerAcc1    = (user)JSON.deserialize(userecord,user.class);
        account userAcc     = [select PersonEmail,id from account where PersonContactId=:personContactId];
        userAcc.PersonEmail = userPerAcc1.email;
         if (Schema.sObjectType.Account.fields.IDMS_Contact_UserExternalId__pc.isUpdateable() && Schema.sObjectType.Account.fields.IDMS_Account_UserExternalId__c.isUpdateable()){
            userAcc.IDMS_Contact_UserExternalId__pc = userPerAcc1.email+Label.CLJUN16IDMS71;
            userAcc.IDMS_Account_UserExternalId__c  = userPerAcc1.email+Label.CLJUN16IDMS71;
            update userAcc;
        }
    }
}