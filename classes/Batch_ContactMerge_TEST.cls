//Description: Test Class for Batch_ContactMerge

@isTest
private class Batch_ContactMerge_TEST {
    static TestMethod void testMergeContact(){
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name =: 'SE - Channel Partner (Community)' limit 1][0].Id;
        
        Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
        insert c;
        
        Account acc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',
                    recordtypeId = businessAccRTId, Country__c = c.Id);
        insert acc;
        
        Contact c1 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',SEContactId__c = '123456',PRMOnly__c = true, PRMUIMSID__c = '123',
                email='test.test@test.com');
        insert c1;
        
        Contact c2 = new Contact(FirstName='Test',LastName = 'TestName',AccountId=acc.Id,JobTitle__c='Z3',
                CorrespLang__c='EN',WorkPhone__c='1234567890',PRMOnly__c = false, PRMUIMSSEContactId__c = '123456',
                email='test.test@test.com');
        insert c2;
        
        User u1 = new User( email='test.test@test.com', contactid = c1.id, profileid = ProfileFieloId, UserName='test-user@fakeemail.com', alias='tuser1', CommunityNickName='tuser1',
                TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1',
                LanguageLocaleKey='en_US', FirstName = 'Testy', LastName = 'User' );
        insert u1;
        
        User u2 = new User( email='test.test@test.com', contactid = c2.id, profileid = ProfileFieloId, UserName='test-user2@fakeemail.com', alias='tuser2', CommunityNickName='tuser2',
                TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1',
                LanguageLocaleKey='en_US', FirstName = 'Testter', LastName = 'User' );
        insert u2;
        
        Batch_ContactMerge batchCont = new Batch_ContactMerge();
        database.executeBatch(batchCont);
    }
}