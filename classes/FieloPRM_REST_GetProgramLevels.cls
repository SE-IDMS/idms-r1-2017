/********************************************************************************************************
    Author: Fielo Team
    Date: 11/03/2015
    Description: 
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestGetProgramLevels/*')
global class FieloPRM_REST_GetProgramLevels{   

    /**
    * [getProgramLevels returns all program levels (badge records type "program level")]
    * @method   getProgramLevels
    * @Pre-conditions  
    * @Post-conditions 
    * @return   List<String>    [list of program level external id (badge records type "program level")]
    */
    @HttpGet
    global static map<String,String> getProgramLevels(){    
        
        return FieloPRM_UTILS_Badge.getProgramLevels();
    
    }   
}