public with sharing class ZuoraQuotePreviewController {
    
    public String QuoteId{get;set;}
    public String SendToZBillingUrl{get;set;}
    public Boolean warning{get;set;}
    public Boolean block{get;set;}
    public boolean isSubmitApproval{get;set;}

    public pagereference ZuoraQuotePreviewCheck(){
      //Variables
      isSubmitApproval = false;
      QuoteId = ApexPages.currentPage().getParameters().get('QuoteId');
      List<ZuoraSendTozBillingCheck__c> customSettings = ZuoraSendTozBillingCheck__c.getall().values();
      warning = false;
      block = false;
      Map<String, Map<String, Schema.SObjectField>> fieldMaps = new Map<String, Map<String, Schema.SObjectField>>();
      Set<String> setSObjects = new Set<String>();
      Map<Id,zqu__Quote__c> mapIdQuote = new Map<Id,zqu__Quote__c>();
      String responses = '';
      Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
      Set<String> setApiNames = new Set<String>();
      
      //LIST OF FIELDS FOR QUERY ON ZQUOTE
      for(ZuoraSendTozBillingCheck__c cs:customSettings){
        setApiNames.add(cs.API_Name__c); 
      }

      //LIST OF FIELDS FOR QUERY ON ZQUOTE
      if(!setApiNames.contains('Id')){
        setApiNames.add('Id'); 
      }
      if(!setApiNames.contains('Entity__c')){
        setApiNames.add('Entity__c'); 
      }

      //DYNAMIC QUERY
      String query = 'Select ';
      Boolean first = true;
      
      for(String str:setApiNames){
        
        if(first){
          query = query+str;
        }else{
          query = query+','+str;
        }
        
        first = false;
      }
      
      query = query+' from zqu__Quote__c where Id = :QuoteId';
      
      list<zqu__Quote__c> listZQ = Database.query(query);
      if(listZQ != null && listZQ.size()>0){
        for(zqu__Quote__c zq:listZQ){
          mapIdQuote.put(zq.Id,zq);
        }
      }
      ////////////////////////////
      
      if(mapIdQuote.size()>0){
        
        // RETRIEVE LIST OF SOBJECT TO PROCESS
        for(ZuoraSendTozBillingCheck__c cs:customSettings){
          
          if(mapIdQuote.get(QuoteId).Entity__c == cs.Entity__c){
          
            setSObjects.add(cs.sObject__c);
          }
        }

        // RETRIEVE FIELD DESCRIBE
        for(String sObj:setSObjects){
          
          Schema.SObjectType sObjSchema = schemaMap.get(sObj);
          
          Map<String, Schema.SObjectField> fieldMap = sObjSchema.getDescribe().fields.getMap();
          
          fieldMaps.put(sObj,fieldMap);
        }

        // PROCESS
        for(ZuoraSendTozBillingCheck__c cs:customSettings){

          if(mapIdQuote.get(QuoteId).Entity__c == cs.Entity__c){
            
            String fieldName;
            
            List<String> listFieldRelations = new List<String>();

            // USE CASE : RELATED OBJECT FIELDS
            listFieldRelations = cs.API_Name__c.split('\\.'); 

            Boolean EmptyField = false;
            if(listFieldRelations.size()>1){

              sObject obj = mapIdQuote.get(QuoteId);

              for(Integer i=0;i<listFieldRelations.size();i++){
                
                if(i != listFieldRelations.size()  && !EmptyField){
                  
                  if(obj != null){
                    obj = obj.getSobject(listFieldRelations.get(i));
                    
                  }else{
                    EmptyField = true;
                    fieldName = fieldMaps.get(cs.sObject__c).get(listFieldRelations.get(i)).getDescribe().getLabel();
                  }

                }else{
                  
                    fieldName = fieldMaps.get(cs.sObject__c).get(listFieldRelations.get(i)).getDescribe().getLabel();
                }
              }
            }else{
              if(mapIdQuote.get(QuoteId).get(cs.API_Name__c) == null){
                EmptyField = true;
              }
              fieldName = fieldMaps.get(cs.sObject__c).get(cs.API_Name__c).getDescribe().getLabel();  
            }
            
            if(fieldName != null){  

              if(cs.Condition__c == 'Empty' && EmptyField){
                
                if(cs.Action__c == 'Block'){
                  block = true;
                  String response = ' The field '+fieldName+' must be filled';
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,response));
                  
                }else if(cs.Action__c == 'Warning'){
                  warning = true;
                  String response = ' The field '+fieldName+' must be filled';
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,response));
                  
                }

              }else if (cs.Condition__c == 'Filled' && !EmptyField){
                
                if(cs.Action__c == 'Block'){
                  block = true;
                  String response = ' The field '+fieldName+' must be Empty';
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,response));
                  
                }else if(cs.Action__c == 'Warning'){
                  warning = true;
                  String response = ' The field '+fieldName+' must be Empty';
                  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,response));
                  
                }
              }
            }
          }    
        }    
      }

      //PAGE REFERENCE
      String hostVal  = ApexPages.currentPage().getHeaders().get('Host');
      String urlVal = '/apex/ZuoraQuoteSubmit?scontrolCaching=11&id='+QuoteId;
      String urll = 'https://' + hostVal+urlVal;
      
      if (urll.contains('//c.')) {
        urll = urll.replace('//c.','//zqu.');
      }else {
        if (urll.contains('--c.')) {
            urll = urll.replace('--c.','--zqu.');
        }
      }

      SendToZBillingUrl = urll;

      System.debug(block);
      if(!block && !warning){

        PageReference redirecturl = new PageReference(urll);

        redirecturl.setRedirect(true);
        
        String action = ApexPages.currentPage().getParameters().get('Action');
        //If submit for approval, redirect to approval page on VF Page
        if(action != null && action == 'SubmitApproval'){
          isSubmitApproval = true;
          return null;
        }
        //Send To zBilling Case
        else{
          return redirecturl;
        }
      }
        
        
      return null;
        
    }

    public PageReference continueToSend(){
      System.debug('## SendToZBillingUrl : '+SendToZBillingUrl);
    String hostVal  = ApexPages.currentPage().getHeaders().get('Host');
      String urlVal = '/apex/ZuoraQuoteSubmit?scontrolCaching=11&id='+QuoteId;
      String urll = 'https://' + hostVal+urlVal;

      if (urll.contains('//c.')) {
            urll = urll.replace('//c.','//zqu.');
          }else {
              if (urll.contains('--c.')) {
                  urll = urll.replace('--c.','--zqu.');
              }
          }

      //CHECK
      SendToZBillingUrl = urll;
        
      PageReference redirecturl = new PageReference(SendToZBillingUrl);

          redirecturl.setRedirect(true);
          
          return redirecturl;
    }

    public PageReference returnToQuote(){
      PageReference redirecturl = new PageReference('/'+QuoteId);

      redirecturl.setRedirect(true);
      
      return redirecturl;
    }

}