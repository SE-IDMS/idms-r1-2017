/********************************************************************
* Company: Fielo
* Created Date: 31/07/2015
* Description: Test Class of class FieloPRM_AP_MemberFeatureTriggers
********************************************************************/
@isTest
private class FieloPRM_MemberFeatureTest {
    
    @isTest static void test_method_one() {
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
                 
            insert member;

            Contact con = [Select id FROM contact WHERE FieloEE__Member__c = : member.id ];

            con.PRMUIMSId__c = '123TEST';

            update con;

            list<FieloPRM_Feature__c> listFeatureinsert = new list<FieloPRM_Feature__c>();
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.name = 'feature test';
            feature.F_PRM_FeatureCode__c= 'feature api name';
            feature.F_PRM_CustomLogicClass__c = 'FieloPRM_CustomLogicImplementTest';
            
            listFeatureinsert.add(feature);

            FieloPRM_Feature__c feature2 = new FieloPRM_Feature__c();
            feature2.name = 'feature test2';
            feature2.F_PRM_FeatureCode__c = 'feature api name2';
            listFeatureinsert.add(feature2);

            insert listFeatureinsert;

            String idRecordTypeSegment = [Select id,developerName, sobjecttype  from RecordType WHERE sobjecttype   = 'FieloEE__RedemptionRule__c' and developerName = 'Manual'].id;

            List<RecordType> listRecordType = [Select id,DeveloperName,Name,SobjectType FRom RecordType WHERE developerNAme = 'Manual'];
            
            map<string, RecordType> mapRecordType = new map<string, RecordType>();
        
            for(RecordType rt: listRecordType){
                mapRecordType.put(rt.SobjectType + rt.DeveloperName,rt);
            }
            
            test.startTest(); 

            list<FieloPRM_MemberFeature__c> listMemberFeature = new list<FieloPRM_MemberFeature__c>();
            list<FieloPRM_MemberFeature__c> listMemberFeatureDelete = new list<FieloPRM_MemberFeature__c>();
            

            FieloPRM_MemberFeature__c memberFeature = new FieloPRM_MemberFeature__c();
            memberFeature.F_PRM_Feature__c =  feature.id;
            memberFeature.F_PRM_Member__c =  member.id;
            memberFeature.F_PRM_IsActive__c  =  true;
            memberFeature.PRMUIMSId__c = '123TEST';
            memberFeature.RecordTypeID = mapRecordType.get( 'FieloPRM_MemberFeature__c' +'Manual').id;
            //insert memberFeature;
            listMemberFeature.add(memberFeature);

            FieloPRM_MemberFeature__c memberfeature1 = new FieloPRM_MemberFeature__c();
            memberfeature1.F_PRM_Feature__c =  feature2.id;
            memberfeature1.F_PRM_Member__c =  member.id;
            memberFeature1.F_PRM_IsActive__c  =  true;
            memberFeature1.PRMUIMSId__c = '123TEST';
            listMemberFeature.add(memberfeature1);
            listMemberFeatureDelete.add(memberfeature1);

            CS_PRM_ApexJobSettings__c PRMSettings = new CS_PRM_ApexJobSettings__c(
                Name = 'FieloPRM_Batch_MemberFeatProcess',
                InitialSync__c = true
            );
            insert PRMSettings;

            insert listMemberFeature;
            listMemberFeature[0].F_PRM_IsActive__c  = false;
            listMemberFeature[1].F_PRM_IsActive__c  = false;
            update listMemberFeature;

            listMemberFeature[0].F_PRM_IsActive__c  = true;
            listMemberFeature[1].F_PRM_IsActive__c  = true;
            update listMemberFeature;

            delete listMemberFeatureDelete;
            test.stopTest();
        }
    }
    
    
    static testmethod void Test_FeatureTest() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);

                
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo= new Account();
            accTo.Name = 'test acc to';
            insert accTo;

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'USUARIO UNIT TEST 1';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST 1';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
            
                 
            insert member;
            
            Contact con = [Select id FROM contact WHERE FieloEE__Member__c = : member.id ];

            con.PRMUIMSId__c = '123TEST';

            update con;

            list<FieloPRM_Feature__c> listFeatureinsert = new list<FieloPRM_Feature__c>();
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.name = 'test1';
            feature.F_PRM_FeatureCode__c= 'feature test1';
            feature.F_PRM_CustomLogicClass__c = 'FieloPRM_CustomLogicImplementTest';

            listFeatureinsert.add(feature);

            FieloPRM_Feature__c feature2 = new FieloPRM_Feature__c();
            feature2.name = 'test2';
            feature2.F_PRM_FeatureCode__c = 'feature test2';
            listFeatureinsert.add(feature2);

            insert listFeatureinsert;
            
            //test.startTest(); 

            system.debug('debug Line 137');
            list<FieloPRM_MemberFeature__c> listMemberFeature = new list<FieloPRM_MemberFeature__c>();
            list<FieloPRM_MemberFeature__c> listMemberFeatureDelete = new list<FieloPRM_MemberFeature__c>();
            

            FieloPRM_MemberFeature__c memberFeature = new FieloPRM_MemberFeature__c();
            memberFeature.F_PRM_Feature__c =  feature.id;
            memberFeature.F_PRM_Member__c =  member.id;
            memberFeature.F_PRM_IsActive__c  =  true;
            memberFeature.PRMUIMSId__c = '123TEST';
            //insert memberFeature;
            listMemberFeature.add(memberFeature);
            system.debug('debug Line 148');
            FieloPRM_MemberFeature__c memberfeature1 = new FieloPRM_MemberFeature__c();
            memberfeature1.F_PRM_Feature__c =  feature2.id;
            memberfeature1.F_PRM_Member__c =  member.id;
            memberFeature1.F_PRM_IsActive__c  =  true;
            memberFeature1.PRMUIMSId__c = '123TEST';
            listMemberFeature.add(memberfeature1);
            listMemberFeatureDelete.add(memberfeature1);
            system.debug('debug Line 155');

            CS_PRM_ApexJobSettings__c PRMSettings = new CS_PRM_ApexJobSettings__c(
                Name = 'FieloPRM_Batch_MemberFeatProcess',
                InitialSync__c = true
            );
            insert PRMSettings;            
            
            insert listMemberFeature;
            system.debug('debug Line 157');
            listMemberFeature[0].F_PRM_IsActive__c  = false;
            listMemberFeature[1].F_PRM_IsActive__c  = false;
            update listMemberFeature;
            system.debug('debug Line 161');
            test.startTest();
                listMemberFeature[0].F_PRM_IsActive__c  = true;
                listMemberFeature[1].F_PRM_IsActive__c  = true;
                update listMemberFeature;
                system.debug('debug Line 162');

                

                delete listMemberFeatureDelete;
                system.debug('debug Line 170');
                

            test.stopTest();
            system.debug('debug Line 174');
        }
    }
}