/*
        Auhtor: Amitava Dutta
        Date: 03/03/11
        Description: Test class for testing the Create Series Apex Controller class
        
    */
    @isTest
    private with sharing Class VFC20_CreateSeries_Test
    {
        static List<OppAgreement__c> aggList = new List<OppAgreement__c>();
    static ProductLineForAgreement__c[] objProdLine = new List<ProductLineForAgreement__c>();
    static User user,newUser,newUser2 ;
    static Country__c newCountry;
    static OppAgreement__c objAgree,objAgree4;
    static Account acc;
    static Opportunity objOpp,opp2,opp3,opp4;
    static OppAgreement__c objAgree2,objAgree3;
    static List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();   
    static List<OppAgreement__c> oa = new List<OppAgreement__c>();
    static OPP_Product__c  prod = new OPP_Product__c();
    static
    {
        VFC61_OpptyClone.isOpptyClone = true;
        acc = Utils_TestMethods.createAccount();
         insert acc;
        
        objAgree2 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree2.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree2.AgreementValidFrom__c = Date.today();         
        objAgree2.FrameOpportunityInterval__c = 'Monthly';
        objAgree2.SignatureDate__c = Date.today();
        objAgree2.Opportunity_Type__c = 'Solutions';        
        objAgree2.Agreement_Duration_in_month__c = '3';
        objAgree2.AgreementType__c = Label.CL00622;
        insert objAgree2;
        
        prod.ProductLine__c = null;
        prod.BusinessUnit__c = 'EcoBuilding';
        insert prod;
        opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.StageName = '3 - Identify & Qualify';
        opp2.AgreementReference__c = objAgree2.Id;
        insert opp2;        
        
        //Inserts the Product Lines        
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            pl1.LineType__c = Label.CLOCT13SLS06;
            pl1.LineStatus__c = 'Pending';
            prodLines.add(pl1);
        }
        
        Insert prodLines; 
        
        
        objAgree3 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree3.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree3.AgreementValidFrom__c = Date.today();         
        objAgree3.FrameOpportunityInterval__c = Label.CL00221;
        objAgree3.SignatureDate__c = Date.today();
        objAgree3.Agreement_Duration_in_month__c = '2';
        objAgree3.Opportunity_Type__c = 'Solutions';  
        objAgree3.AgreementType__c = Label.CL00622;              
        objAgree3.TECH_IsCreateByBatch__c = true;
        insert objAgree3;        
        
        objAgree4 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree4.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree4.AgreementValidFrom__c = Date.today();         
        objAgree4.FrameOpportunityInterval__c = 'Monthly';
        objAgree4.SignatureDate__c = Date.today();
        objAgree4.Opportunity_Type__c = 'Solutions';
        objAgree4.Agreement_Duration_in_month__c = '61';
        objAgree4.AgreementType__c = Label.CL00658;                      
        insert objAgree4;   
        opp4 = Utils_TestMethods.createOpportunity(acc.Id);
        opp4.StageName = '3 - Identify & Qualify';
        opp4.AgreementReference__c = objAgree4.Id;
        insert opp4;  
        prodLines.clear();
        
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp4.Id);
            pl1.Quantity__c = 10;
            pl1.LineType__c = Label.CLOCT13SLS06;
            pl1.LineStatus__c = 'Pending';   
            OPP_ProductLine__c pl2 = Utils_TestMethods.createProductLine(opp4.Id);
            pl2.Quantity__c = 0;
            pl2.LineType__c = Label.CLOCT13SLS06;
            pl2.LineStatus__c = 'Pending';              
            prodLines.add(pl2);
        
        
        Insert prodLines;     
    }
    
    static testmethod void testMassUpdate1()
    {   
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(objAgree2);
        ApexPages.currentPage().getParameters().put('agreementId',objAgree2.Id);         
        VFC20_CreateSeries createSeries = new VFC20_CreateSeries(controller);
        createSeries.iscreateseries = true;
        createSeries.confirmmessage  = 'IC'; 
        createSeries.confirmCreateSeries();
        //createSeries.createSeries();
        
        List<OppAgreement__c> agreements = new List<OppAgreement__c>();
        agreements.add(objAgree2);
        createSeries.GetNonUpdatedAgreementFieldForSeries(agreements);      
        createSeries.UpdateCloseDateForMasterOpportunity(Date.today(), Date.today(), Label.CL00222);
        createSeries.UpdateCloseDateForMasterOpportunity(Date.today(), Date.today(), Label.CL00224);
        createSeries.UpdateCloseDateForMasterOpportunity(Date.today(), Date.today(), Label.CL00223);
        createSeries.UpdateCloseDateForMasterOpportunity(Date.today(), Date.today(), Label.CL00221);
        createSeries.GetCloseDate(Date.today(),Label.CL00222,2);
        createSeries.GetCloseDate(Date.today(),Label.CL00225,2);
        createSeries.GetCloseDate(Date.today(),Label.CL00224,2);
        createSeries.GetCloseDate(Date.today(),Label.CL00223,2);      
        createSeries.UpdateCloseDateForMasterOpportunity(Date.today(), Date.today().addmonths(2), Label.CL00225);                              
        createSeries.UpdateCloseDateForMasterOpportunity(Date.today(), Date.today().addmonths(8), Label.CL00224);                                      
        createSeries.UpdateCloseDateForMasterOpportunity(Date.today(), Date.today().addmonths(8), Label.CL00223);                                              
        createSeries.UpdateCloseDateForMasterOpportunity(Date.today(), Date.today().addmonths(8), Label.CL00222);                                                      
        createSeries.GetCloseDate(Date.today(),Label.CL00225,1);   
        createSeries.GetCloseDate(Date.today(),Label.CL00224,1);  
        createSeries.GetCloseDate(Date.today(),Label.CL00223,1);    
        createSeries.GetCloseDate(Date.today(),Label.CL00221,1);                  
        createSeries.GetCloseDate(Date.today(),Label.CL00222,1); 
        List<OppAgreement__c> agsLst = new List<OppAgreement__c>();
        OppAgreement__c objAgreetemp = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgreetemp.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgreetemp.AgreementValidFrom__c = Date.today();         
        objAgreetemp.FrameOpportunityInterval__c = null;
        objAgreetemp.SignatureDate__c = Date.today();
        objAgreetemp.Opportunity_Type__c = 'Solutions';        
        objAgreetemp.Agreement_Duration_in_month__c = '3';
        objAgreetemp.AgreementType__c = Label.CL00622; 
        agsLst.add(objAgreetemp);
        OppAgreement__c objAgreetemp1 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgreetemp1.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgreetemp1.AgreementValidFrom__c = Date.today();         
        objAgreetemp1.FrameOpportunityInterval__c = 'Monthly';
        objAgreetemp1.SignatureDate__c = null;
        objAgreetemp1.Opportunity_Type__c = 'Solutions';        
        objAgreetemp1.Agreement_Duration_in_month__c = '3';
        objAgreetemp1.AgreementType__c = Label.CL00622; 
        agsLst.add(objAgreetemp1);
        createseries.GetNonUpdatedAgreementFieldForSeries(agsLst);                
        createseries.CheckAgreementCriteriaForSeries(agsLst);
        Test.stopTest();
    }    
    static testmethod void testMassUpdate2()
    {   
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(objAgree3);
        ApexPages.currentPage().getParameters().put('agreementId',objAgree3.Id);         
        VFC20_CreateSeries createSeries = new VFC20_CreateSeries(controller);
        createSeries.confirmCreateSeries();
        createSeries.createSeries();
        createSeries.unLoadMe();
        Test.stopTest();
    }
    
    static testmethod void testMassUpdate4()
    {   
        Test.startTest();        
        opp3 = Utils_TestMethods.createOpportunity(acc.Id);
        opp3.StageName = '3 - Identify & Qualify';
        opp3.AgreementReference__c = objAgree2.Id;
        insert opp3; 
        ApexPages.StandardController controller = new ApexPages.StandardController(objAgree2);
        ApexPages.currentPage().getParameters().put('agreementId',objAgree2.Id);         
        VFC20_CreateSeries createSeries = new VFC20_CreateSeries(controller);
        createSeries.confirmCreateSeries();
        //createSeries.createSeries();
        Test.stopTest();        
    }
    static testmethod void testMassUpdate3()
    {   
        Test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(objAgree4);
        ApexPages.currentPage().getParameters().put('agreementId',objAgree4.Id);         
        VFC20_CreateSeries createSeries = new VFC20_CreateSeries(controller);
        createSeries.confirmCreateSeries();
        createSeries.createSeries();
        Test.stopTest();        
        
    }    
    }