@isTest
private class AP_CCCAction_Handler_TEST {
	public static testMethod void testAnswerNotification(){
        System.debug('#### AP_CCCAction_Handler_TEST.testAnswerNotification Started ####');
        
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestContactAction');
        insert objContact;
        
        List<Case> lstCase = new List<Case>();
        List<Case> lstCaseUpdate = new List<Case>();
        Case objCaseActionInternal = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open/ Waiting For Details - Internal');
        
        lstCase.add(objCaseActionInternal);
        
        Database.SaveResult[] dbInsertResult = Database.Insert(lstCase, false);
        
        CCCAction__c objAction = Utils_TestMethods.createAction(objCaseActionInternal.Id,UserInfo.getUserId());
        objAction.SendNotificationonResponse__c=true;
        
        Database.SaveResult dbCCActionInsertResult = Database.insert(objAction, false);
        
        objAction.TECH_AnswerNotificationRequired__c =true;
        objAction.Status__c = Label.CLOCT15CCC01;
        objAction.Answer__c = 'Internal Commnunication from other Department';
        Database.SaveResult dbCCActionUpdateResult = Database.update(objAction, false);

        System.debug('#### AP_CCCAction_Handler_TEST.testAnswerNotification Started Ends ####');
    }
    
}