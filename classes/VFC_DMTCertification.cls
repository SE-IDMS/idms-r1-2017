public with sharing class VFC_DMTCertification {

    // Declarations 
    public string pardmtid;
    //public PRJ_ProjectReq__c Prj;
    public string prjName;
    public string prjDomain;
    public string prjcode;
    

    public VFC_DMTCertification(ApexPages.StandardController controller) {
    pardmtid = ApexPages.currentPage().getParameters().get('id');
    prjname = ApexPages.currentPage().getParameters().get('name');
    prjDomain = ApexPages.currentPage().getParameters().get('domain');
    prjcode = ApexPages.currentPage().getParameters().get('prcode');

     
    system.debug('^^^ ID + ^^^ Name ' + pardmtid  );
    }
    
    public pagereference RedirectLink(){
    pagereference pg;
    List<CFWRiskAssessment__c> Cert = new List<CFWRiskAssessment__c>();
    
    Cert = [Select id from CFWRiskAssessment__c where DMTProjectRequestID__c = :pardmtid];
    
    System.debug('#####Cert ID' +  pardmtid);
    
    if(Cert.size() > 0)
     pg = new pagereference('/' + Cert[0].id);
    else
      pg = new pagereference(Label.CLOCT15CFW80 + Label.CLOCT15CFW75 + '=' + prjname + '&'+ Label.CLOCT15CFW76 + '=' + prjDomain + '&' + Label.CLOCT15CFW77 + '=' + prjcode + '&' + Label.CLOCT15CFW78 + '=' + pardmtid + '&'+ Label.CLOCT15CFW79 + '='  + prjname);
    
    return pg;
    }
      
      
  
}