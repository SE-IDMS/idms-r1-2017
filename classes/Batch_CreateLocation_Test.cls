@isTest
public class Batch_CreateLocation_Test{
    
    public static testMethod void TestOne() {
         // Creating Data 
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.RecordTypeid = Label.CLOCT13ACC08;
        insert objAccount;
                
        Country__c country= Utils_TestMethods.createCountry(); 
        country.CountryCode__c= 'hkh';   
        insert country;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        objContact.Country__c= country.id;
        insert objContact;
        
          
       
        List<SVMXC__Site__c> LocList = new List<SVMXC__Site__c>();
        
        for(integer i=0;i<10;i++){
        
            SVMXC__Site__c site1 = new SVMXC__Site__c();
            site1.Name = 'Test Location'+i;
            site1.SVMXC__Street__c  = 'Test Street';
            site1.SVMXC__Account__c = objAccount .id;
            site1.PrimaryLocation__c = true;
            site1.RecordTypeId = '012A0000000ncnY';
            site1.LocationCountry__c=country.Id;
            site1.SVMXC__Partner_Contact__c=objContact.Id;
            LocList.add(site1);
        
        }
         insert LocList ;  
         
        Batch_CreateLocation ipbatch  = new Batch_CreateLocation();
        ID batchprocessid = Database.executeBatch(ipbatch);
        
    }

}