public with sharing class IPOStrategicMilestoneTriggers
{
    Public Static void IPOStrategicMilestoneBeforeInsertUpdate(List<IPO_Strategic_Milestone__c> IPOMile)
      {
      
          System.Debug('****** ConnectMilestoneBeforeInsertUpdate Start ****');
          list<IPO_Strategic_Program__c> IPOProgram = new List<IPO_Strategic_Program__c>(); 
          
          // Check the Scope Selected for Power Regions
          
          integer PW;
          integer GBs;
          integer GFn;
          IPOProgram = [select Global_Functions__c, Global_Business__c, Operation__c,Name from IPO_Strategic_Program__c];
          for(IPO_Strategic_Milestone__c M:IPOMile){
          
          
                 PW = 0; 
                 GBs = 0;                 
                 GFn = 0;
                 
                 for(IPO_Strategic_Program__c P:IPOProgram){
                 
                 // Check if the Scope in Null in Program or Milestone
                 
                  if((P.Name == M.Program__c) && (M.Operation_Regions__c != null) && (P.Operation__c == null))
                      M.Validate_insert_update__c = True; 
                  else if((P.Name == M.Program__c) && (M.Global_Business__c != null) && (P.Global_Business__c == null))
                      M.Validate_Insert_Update__c = True; 
                  else if((P.Name == M.Program__c) && (M.Global_Functions__c != null) && (P.Global_Functions__c == null))
                       M.Validate_Insert_Update__c = True;
                                     
                // Check the Scope Selected for Power Regions
                   if((P.Name == M.Program__c) && (M.Operation_Regions__c != null)&& (P.Operation__c !=null)){                        
                     for(String PR: M.Operation_Regions__c.Split(';')){    
                      if(!(P.Operation__c.Contains(PR)))
                           PW = PW + 1;                      
                     } // For Loop Ends
                    } //if Ends
                  // Check the Scope Selected for Global Business
                   if((P.Name == M.Program__c) && (M.Global_Business__c != null) && (P.Global_Business__c !=null)){                        
                     for(String GB: M.Global_Business__c.Split(';')){    
                      if(!(P.Global_Business__c.Contains(GB)))
                           GBs = GBs + 1;                      
                     } // For Loop Ends
                    } // If Ends
                    
                  // Check the Scope Selected for Global Function
                   if((P.Name == M.Program__c) && (M.Global_Functions__c != null)&& (P.Global_Functions__c !=null)){                        
                     for(String GF: M.Global_Functions__c.Split(';')){    
                      if(!(P.Global_Functions__c.Contains(GF)))
                           GFn = GFn + 1;                      
                     } // For Loop Ends
                    } // If Ends           
                    
                      if ((PW > 0 ) || (GBs > 0) || (GFn > 0))
                       M.Validate_Insert_Update__c = True;  
                    } // For Loop Ends
                 
                              
                 }// For Loop Ends
                 
             System.Debug('****** IPOMilestoneBeforeInsertUpdate End ****');
            
            }   
            
             
      // Check if Child Cascading Milestones Exists before updating / removing scope from parent milestone
                
   Public Static void IPOStrategicMilestoneBeforeUpdate_CheckCascadingMilestoneScope(List<IPO_Strategic_Milestone__c> IPOMilenew,List<IPO_Strategic_Milestone__c> IPOMileold )
      {
      
          System.Debug('****** IPOGlobalMilestoneBeforeUpdate_CheckCascadingMilestoneScope Start ****');
          
          List<IPO_Strategic_Cascading_Milestone__c> IPOCMile = New List<IPO_Strategic_Cascading_Milestone__c>();
          
          IPOCMile = [Select Name, Scope__c, Entity__c,IPO_Strategic_Milestone_Name__c from IPO_Strategic_Cascading_Milestone__c];
          
          for(IPO_Strategic_Milestone__c M : IPOMilenew){
              for(IPO_Strategic_Cascading_Milestone__c CM: IPOCMile){    
                if(M.IPO_Strategic_Milestone_Name__c == CM.IPO_Strategic_Milestone_Name__c) { 
                  //System.Debug('Milestone Updation ' + M.ValidateScopeonUpdate__c);
                   // Check if Entiy in the Milestone is Null for any Scope (All Entities Removed)
                
                if(((M.Global_Functions__c == null)&& (CM.Scope__c == Label.Connect_Global_Functions)) || ((M.Global_Business__c == null)&& (CM.Scope__c == Label.Connect_Global_Business)) || ((M.Operation_Regions__c == null)&& (CM.Scope__c == Label.Connect_Power_Region))){
                    M.ValidateScopeonUpdate__c = True;
                    System.Debug('Milestone Updation ');
                     }
                     
                       // Check for Every Scope
                         
                    
             if((M.Global_Functions__c != null) && (CM.Scope__c == Label.Connect_Global_Functions) && (!(M.Global_Functions__c.Contains(CM.Entity__c)))){
                         // System.Debug('Cascading Milestone for GF ' + M.Global_Functions__c + '......' + CM.Entity__c);
                          M.ValidateScopeonUpdate__c = True;
                          }
                      else if((M.Global_Business__c != null)&& (CM.Scope__c == Label.Connect_Global_Business) && (!(M.Global_Business__c.Contains(CM.Entity__c))))
                          M.ValidateScopeonUpdate__c = True;
                      else if((M.Operation_Regions__c != null) && (CM.Scope__c == Label.Connect_Power_Region) && (!(M.Operation_Regions__c.Contains(CM.Entity__c)))){
                          //System.Debug('Milestone Updation ' + CM.Entity__c);
                          M.ValidateScopeonUpdate__c = True;
                          }
              
                    } // If Ends
                  } // For Ends
                   //System.Debug('Milestone Updation ' + M.ValidateScopeonUpdate__c);
                 }// For Ends
                 
                // System.Debug('****** ConnectGlobalMilestoneBeforeUpdate_CheckCascadingMilestoneScope Stop ****');
                }     
     Public Static void IPOStrategicMilestoneBeforeInsert(List<IPO_Strategic_Milestone__c> IPOMile)
      {
      
          System.Debug('****** ConnectMilestoneBeforeInsert Start ****');
          if (IPOMile[0].Progress_Summary_Q4__c != null){
              IPOMile[0].Current_Status__c = IPOMile[0].Progress_Summary_Q4__c;
              IPOMile[0].Current_Progress_in_Quarter__c = IPOMile[0].Status_in_Quarter_Q4__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q4__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q4__c;
              }
          else if (IPOMile[0].Progress_Summary_Q3__c != null){
              IPOMile[0].Current_Status__c = IPOMile[0].Progress_Summary_Q3__c;
              IPOMile[0].Current_Progress_in_Quarter__c = IPOMile[0].Status_in_Quarter_Q3__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q3__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q3__c;
              }
           else if (IPOMile[0].Progress_Summary_Q2__c != null){
              IPOMile[0].Current_Status__c = IPOMile[0].Progress_Summary_Q2__c;
              IPOMile[0].Current_Progress_in_Quarter__c = IPOMile[0].Status_in_Quarter_Q2__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q2__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q2__c;
              }
           else if (IPOMile[0].Progress_Summary_Q1__c != null){
              IPOMile[0].Current_Status__c = IPOMile[0].Progress_Summary_Q1__c;
              IPOMile[0].Current_Progress_in_Quarter__c = IPOMile[0].Status_in_Quarter_Q1__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q1__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q1__c;
              }
              
           
          }
          
 Public Static void IPOStrategicMilestoneBeforeUpdate(List<IPO_Strategic_Milestone__c> IPOMile, List<IPO_Strategic_Milestone__c> IPOMileold)
      {
      
          System.Debug('****** ConnectMilestoneBeforeInsert Start ****');
              IPOMile[0].Current_Status__c = IPOMile[0].Progress_Summary_Q1__c;
              IPOMile[0].Current_Progress_in_Quarter__c = IPOMile[0].Status_in_Quarter_Q1__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q1__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q1__c;
              
           if ((IPOMile[0].Progress_Summary_Q4__c != 'No Data') && (IPOMile[0].Progress_Summary_Q4__c != null))           
           {
              IPOMile[0].Current_Status__c = IPOMile[0].Progress_Summary_Q4__c;
              IPOMile[0].Current_Progress_in_Quarter__c = IPOMile[0].Status_in_Quarter_Q4__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q4__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q4__c;
              }
          else if ((IPOMile[0].Progress_Summary_Q3__c != null) && (IPOMile[0].Progress_Summary_Q3__c != 'No Data'))
             {
              IPOMile[0].Current_Status__c = IPOMile[0].Progress_Summary_Q3__c;
              IPOMile[0].Current_Progress_in_Quarter__c = IPOMile[0].Status_in_Quarter_Q3__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q3__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q3__c;
              }
          else if ((IPOMile[0].Progress_Summary_Q2__c != null) && (IPOMile[0].Progress_Summary_Q2__c != 'No Data'))
              {
              IPOMile[0].Current_Status__c = IPOMile[0].Progress_Summary_Q2__c;
              IPOMile[0].Current_Progress_in_Quarter__c = IPOMile[0].Status_in_Quarter_Q2__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q2__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q2__c;
              }
          else if ((IPOMile[0].Progress_Summary_Q1__c != null) && (IPOMile[0].Progress_Summary_Q1__c != 'No Data'))
              {
              IPOMile[0].Current_Status__c = IPOMile[0].Progress_Summary_Q1__c;
              IPOMile[0].Current_Progress_in_Quarter__c = IPOMile[0].Status_in_Quarter_Q1__c;
              IPOMile[0].Current_Roadblocks__c = IPOMile[0].Roadblocks_Q1__c;
              IPOMile[0].Current_Decision_Review_Outcomes__c = IPOMile[0].Decision_Review_Outcomes_Q1__c;
              }
             }
      
      //auto updation of Milestone status when the respective cascading milestones are all completed.
      
      public static void IPOMilestoneUpdate_StatusUpdate(List<IPO_Strategic_Milestone__c> Miles){
          List<IPO_Strategic_Cascading_Milestone__c> CMiles = new List<IPO_Strategic_Cascading_Milestone__c>();
          integer csize = 0;
          integer flag = 0;
          for(IPO_Strategic_Milestone__c Mil : Miles){
              CMiles = [SELECT Id, Name, IPO_Strategic_Milestone__c, Progress_Summary_Deployment_Leader_Q1__c, Progress_Summary_Deployment_Leader_Q2__c, 
                        Progress_Summary_Deployment_Leader_Q3__c, Progress_Summary_Deployment_Leader_Q4__c, CurrentProgressSummary_Deployment_Leader__c
                        FROM IPO_Strategic_Cascading_Milestone__c WHERE IPO_Strategic_Milestone__c = :Mil.Id ];  
                
          
          csize = CMiles.size();
          if(CMiles.size() == 0){
              if(Mil.Progress_Summary_Q1__c != null && Mil.Progress_Summary_Q1__c == 'Completed'){
                      Mil.Progress_Summary_Q2__c = Mil.Progress_Summary_Q1__c;
                      Mil.Progress_Summary_Q3__c = Mil.Progress_Summary_Q1__c;
                      Mil.Progress_Summary_Q4__c = Mil.Progress_Summary_Q1__c;
                  } 
                  if(Mil.Progress_Summary_Q2__c != null && Mil.Progress_Summary_Q2__c == 'Completed'){
                      Mil.Progress_Summary_Q3__c = Mil.Progress_Summary_Q2__c;
                      Mil.Progress_Summary_Q4__c = Mil.Progress_Summary_Q2__c;
                  }
                  if(Mil.Progress_Summary_Q3__c != null && Mil.Progress_Summary_Q3__c == 'Completed'){
                      Mil.Progress_Summary_Q4__c = Mil.Progress_Summary_Q3__c;
                  }   
          }
          else{
              for(IPO_Strategic_Cascading_Milestone__c CM : CMiles){
                  if(CM.CurrentProgressSummary_Deployment_Leader__c == 'Completed' || CM.Progress_Summary_Deployment_Leader_Q1__c == 'Completed' ||
                     CM.Progress_Summary_Deployment_Leader_Q2__c == 'Completed' || CM.Progress_Summary_Deployment_Leader_Q3__c == 'Completed' ||
                     CM.Progress_Summary_Deployment_Leader_Q4__c == 'Completed')
                      flag = flag + 1;
                  
                  else
                      flag = 0;
                     
              }
          
              if(flag == csize){
                  
                  if(Mil.Progress_Summary_Q1__c != null && Mil.Progress_Summary_Q1__c == 'Completed'){
                      Mil.Progress_Summary_Q2__c = Mil.Progress_Summary_Q1__c;
                      Mil.Progress_Summary_Q3__c = Mil.Progress_Summary_Q1__c;
                      Mil.Progress_Summary_Q4__c = Mil.Progress_Summary_Q1__c;
                  } 
                  if(Mil.Progress_Summary_Q2__c != null && Mil.Progress_Summary_Q2__c == 'Completed'){
                      Mil.Progress_Summary_Q3__c = Mil.Progress_Summary_Q2__c;
                      Mil.Progress_Summary_Q4__c = Mil.Progress_Summary_Q2__c;
                  }
                  if(Mil.Progress_Summary_Q3__c != null && Mil.Progress_Summary_Q3__c == 'Completed'){
                      Mil.Progress_Summary_Q4__c = Mil.Progress_Summary_Q3__c;
                  } 
                  /*
                  Mil.Progress_Summary_Q1__c = 'Completed';
                  Mil.Progress_Summary_Q2__c = 'Completed';
                  Mil.Progress_Summary_Q3__c = 'Completed';
                  Mil.Progress_Summary_Q4__c = 'Completed';
                  */  
              }
          }
       }    
      }
      
           
    }