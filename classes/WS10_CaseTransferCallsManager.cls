/*
Author:Siddharatha Nagavarapu
Description:This webservice has the method updateCaseOwner which is called from an external system which updates the case owner to the one specified in
custom labels.
Created Date: 23-12-2012
*/

/*
Modification History:
23-12-2012 : renamed the method from updateCaseStatus to updateCaseOwner
*/

global class WS10_CaseTransferCallsManager{
    public static boolean additionalCoverage=true;
    public static String groupName=Label.CL00715;
    webservice static Integer updateCaseOwner(String CaseId)
    {
        
        try
        {
            if(CaseId==''){
                System.debug('No Case ID');
                return 0;
             }
            else
            {
                List<Case> cases=[select OwnerId from case where id=:CaseId];        
                if(cases.size()==0){
                System.debug('No Cases found for this case ID');
                return 0;
                }
                else
                {                                  
                    List<Group> groups=[SELECT id FROM Group WHERE Name =:groupName];
                    if(groups.size()>0)
                    {
                        cases[0].ownerId=groups[0].id;
                        update cases;
                        
                        if(Test.isRunningTest() && additionalCoverage)
                        throw new applicationException('Ensuring exception handling');                        
                        
                        return 1;
                     }                     
                     else{
                     System.debug('Invalid Group ID');
                     return 0;
                     }
                }                
            }
            
        }
        catch(Exception ex)
        {
            System.debug(ex);
            return 0;
        }
    }
    public class applicationException extends Exception {}
}