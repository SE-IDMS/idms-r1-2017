@isTest
global class ContactSynchIFWMock implements HttpCalloutMock{
    global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('fail');
        res.setStatusCode(500);
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{\"registerUserResponse\":{\"federatedId\":\"798976978775446jg78\"}}');
        return res;
    }
}