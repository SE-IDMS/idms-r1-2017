@isTest
private class Batch_ReferentialCaseDataMigration_TEST {
    static testMethod void testReferentialCaseDataMigration() {
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        insert objContact;
        
        Product2 objCommercialReference = Utils_TestMethods.createCommercialReference('CMREF123','MaterialDescription For CMREF123','BU-CMREF123', 'PL-CMREF123', 'PF-CMREF123', 'F-CMREF123',false, false, false);
        Database.insert(objCommercialReference);
        
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        objOpenCase.CommercialReference_lk__c = objCommercialReference.Id;
        Database.insert(objOpenCase);
        
        Test.startTest();
        
        String strQuery = 'Select Id,ProductBU__c,ProductLine__c,ProductFamily__c,Family__c, Family_lk__c, SubFamily__c,ProductGroup__c,ProductSuccession__c, CommercialReference__c,CommercialReference_lk__c, TECH_GMRCode__c,TECH_2015ReferentialUpdated__c from Case where Status in (\'New\',\'In Progress\') AND CommercialReference_lk__c != null  AND TECH_2015ReferentialUpdated__c =false';
        Batch_ReferentialCaseDataMigration objBatchJob = new  Batch_ReferentialCaseDataMigration(strQuery,1);
        Database.executeBatch(objBatchJob);
        Test.stopTest();
    }
}