/****************************************************************************************************************************

    Author       : Shruti Karn
    Created Date : 06 March 2013
    Description  : 1. updateCountryProgram()
                       (a) Update all country partner programs when the parent partner program is updated
    Updated By : Shruti Karn
                 For June 2013 Release
                 1. To update the classification level2 when classification level1 is updated.
                 2. To update the market sub-segment when market segment is updated.                  
***************************************************************************************************************************/
public class  AP_PartnerPRG_CountryProgramMethods
{
     
    public static void updateCountryProgram(map<Id,PartnerProgram__c> mapNewGlobalPRG , map<Id,PartnerProgram__c> mapOldGlobalPRG)
    {
        
        Schema.SObjectType objType = PartnerProgram__c.getSObjectType();
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.PartnerProgram__c.fields.getMap();
        list<String> lstFields = new list<String>();
        list<PartnerProgram__c> lstCountryPartnerPRG = new list<PartnerProgram__c>();
        
        map<Id,list<PartnerProgram__c>> mapGlobalCountryPRG = new map<Id,list<PartnerProgram__c>>();
        set<Id> setCountryProgramID = new set<ID>();
        String strAllFields = '';
        setCountryProgramID.addALL(mapNewGlobalPRG.keySet());
        Map<String, Schema.SObjectField> mapAllFields = Schema.SObjectType.PartnerProgram__c.fields.getMap();
                       
        if (mapAllFields != null)
        {
            for (Schema.SObjectField ft : mapAllFields.values())// loop through all field tokens (ft)
            { 
                strAllFields += ft.getDescribe().getName()+',';
              
            }
        }
        
        if (strAllFields.endsWith(','))
        {
            strAllFields = strAllFields.substring(0,strAllFields.lastIndexOf(','));
        }
     
        String countryPRGQuery = 'Select '+ strAllFields+ ' FROM PartnerProgram__c WHERE globalpartnerprogram__c in :setCountryProgramID limit 10000';
        lstCountryPartnerPRG = database.query(countryPRGQuery);
        if(!lstCountryPartnerPRG.isEmpty())
        {
            for(PartnerProgram__c countryPRG :lstCountryPartnerPRG)
            {
                if(!mapGlobalCountryPRG.containsKey(countryPRG.GlobalPartnerProgram__c))
                    mapGlobalCountryPRG.put(countryPRG.GlobalPartnerProgram__c, new list<PartnerProgram__c> {(countryPRG)});
                else
                    mapGlobalCountryPRG.get(countryPRG.GlobalPartnerProgram__c).add(countryPRG);        
            }
                
        }
        
        //map<String,CS_GlobalPRGtoCountryPRGFields__c> mapGPtoCPFields = CS_GlobalPRGtoCountryPRGFields__c.getAll();

        list<PartnerProgram__c> lstCountryPRG = new list<PartnerProgram__c>();            
        if(!mapNewGlobalPRG.isEmpty())
        {
            for(Id globalPRGID : mapNewGlobalPRG.keySet())
            {   
                if(mapGlobalCountryPRG.get(globalPRGID) != null)
                {
                    for(PartnerProgram__c countryPRG : mapGlobalCountryPRG.get(globalPRGID))
                    {
                        Boolean Changed = false;
                        for (String str : mapFields.keyset()) 
                        {
                            if(mapNewGlobalPRG.get(globalPRGID).recordtypeid == Label.CLMAY13PRM15 &&  mapFields.get(str).getDescribe().isUpdateable() && mapNewGlobalPRG.get(globalPRGID).get(str) != mapOldGlobalPRG.get(globalPRGID).get(str))
                            {
                                //if field can be synced with child country programs
                                //if(mapGPtoCPFields.get(str) == null || (mapGPtoCPFields.get(str).DoNotSyncThisField__C == false) )
                                if(CS_GlobalPRGtoCountryPRGFields__c.getValues(str.toLowerCase()) != null)
                                {    
                                    countryPRG.put(str, mapNewGlobalPRG.get(globalPRGID).get(str));
                                    Changed = true;
                                }         
                            }
                        }
                        if(Changed)
                            lstCountryPRG.add(countryPRG);
                    }
                }
                
            }
            try
            { 
               if(!lstCountryPRG.isEmpty())
                update lstCountryPRG; 
            }
            catch(Exception e)
            {
                for (Integer i = 0; i < mapNewGlobalPRG.size(); i++) 
                { 
                    mapNewGlobalPRG.values().get(i).addError(e.getMessage()); 
                    System.debug(e.getMessage()); 
                }   
            }
        }
          
    }

/*************************************************************************************************************************
    For PRM June 13 Release
    To update Classification Level 2 when Classification Level 1 is updated.
*************************************************************************************************************************/
    public static void delClassificationLevel2(map<Id,PartnerProgram__c> mapPartnerProgram, set<String> setClassificationLvl1 )
    {

       /* Integer INTLIMIT = Limits.getLimitDMLRows() - Limits.getDMLRows();
        list<PartnerProgramClassification__c> lstPartnerClassification = new list<PartnerProgramClassification__c>();
        map<Id,list<PartnerProgramClassification__c>> mapProgramClassfication = new map<Id,list<PartnerProgramClassification__c>>();
        list<PartnerProgramClassification__c> lstLeveltobeDel  = new list<PartnerProgramClassification__c>();
        lstPartnerClassification = [Select id,partnerprogram__c,classificationlevel__r.classificationlevelname__c from PartnerProgramClassification__c where PartnerProgram__c in : mapPartnerProgram.keySet() and ClassificationLevel__r.ParentClassificationLevel__r.Name in : setClassificationLvl1 limit :INTLIMIT];
        for(PartnerProgramClassification__c prgcl : lstPartnerClassification)
        {
            if(!mapProgramClassfication.containsKey(prgcl.partnerprogram__c))
                mapProgramClassfication.put(prgcl.partnerprogram__c , new list<PartnerProgramClassification__c> {(prgcl)});
            else
                mapProgramClassfication.get(prgcl.partnerprogram__c).add(prgcl);
        }
        
        for(Id prgID  : mapPartnerProgram.keySet())
            if(mapProgramClassfication.containsKey(prgID))
            {
                lstLeveltobeDel.addALL(mapProgramClassfication.get(prgID));
                for(PartnerProgramClassification__c prgcl2 :  mapProgramClassfication.get(prgID))
                {
                    if(mapPartnerProgram.get(prgID).Classificationlevel2__c.contains(prgcl2.classificationlevel__r.classificationlevelname__c))
                    {    
                        if(mapPartnerProgram.get(prgID).Classificationlevel2__c.startsWith(prgcl2.classificationlevel__r.classificationlevelname__c+','))
                            mapPartnerProgram.get(prgID).Classificationlevel2__c=mapPartnerProgram.get(prgID).Classificationlevel2__c.remove(prgcl2.classificationlevel__r.classificationlevelname__c+',');
                        else if(mapPartnerProgram.get(prgID).Classificationlevel2__c.startsWith(prgcl2.classificationlevel__r.classificationlevelname__c))
                            mapPartnerProgram.get(prgID).Classificationlevel2__c=mapPartnerProgram.get(prgID).Classificationlevel2__c.remove(prgcl2.classificationlevel__r.classificationlevelname__c);
                        else
                            mapPartnerProgram.get(prgID).Classificationlevel2__c=mapPartnerProgram.get(prgID).Classificationlevel2__c.remove(','+prgcl2.classificationlevel__r.classificationlevelname__c);    
                    }
                    if(mapPartnerProgram.get(prgID).Classificationlevel2__c.endsWith(','))
                        mapPartnerProgram.get(prgID).Classificationlevel2__c=mapPartnerProgram.get(prgID).Classificationlevel2__c.removeEnd(',');
                }
            }
        delete lstLeveltobeDel;*/
      
    }
/*************************************************************************************************************************
    For PRM June 13 Release
    To update Market Sub Segment when Market Segment is updated.
*************************************************************************************************************************/
    public static void delMarketSubSegment(map<Id, PartnerProgram__c> mapPartnerProgram,set<String> setMarketSeg)
    {

     /*   Integer INTLIMIT = Limits.getLimitDMLRows() - Limits.getDMLRows();
        list<PartnerProgramMarket__c> lstPartnerMarket = new list<PartnerProgramMarket__c>();
        map<Id,list<PartnerProgramMarket__c>> mapProgramMarket = new map<Id,list<PartnerProgramMarket__c>>();
        list<PartnerProgramMarket__c> lstMarkettoDel  = new list<PartnerProgramMarket__c>();
        lstPartnerMarket = [Select id,partnerprogram__c,marketsegment__r.marketsegmentname__c from PartnerProgramMarket__c where PartnerProgram__c in : mapPartnerProgram.keySet() and MarketSegment__r.ParentMarketSegment__r.Name in : setMarketSeg limit :INTLIMIT];
        for(PartnerProgramMarket__c prgms : lstPartnerMarket)
        {
            if(!mapProgramMarket.containsKey(prgms.partnerprogram__c))
                mapProgramMarket.put(prgms.partnerprogram__c , new list<PartnerProgramMarket__c> {(prgms)});
            else
                mapProgramMarket.get(prgms.partnerprogram__c).add(prgms);
        }
        
        for(Id prgID  : mapPartnerProgram.keySet())
            if(mapProgramMarket.containsKey(prgID))
            {
                lstMarkettoDel.addALL(mapProgramMarket.get(prgID));
                for(PartnerProgramMarket__c prgmss :  mapProgramMarket.get(prgID))
                {
                    if(mapPartnerProgram.get(prgID).MarketSubSegment__c.contains(prgmss.marketsegment__r.marketsegmentname__c))
                    {    
                        if(mapPartnerProgram.get(prgID).MarketSubSegment__c.startsWith(prgmss.marketsegment__r.marketsegmentname__c+','))
                            mapPartnerProgram.get(prgID).MarketSubSegment__c=mapPartnerProgram.get(prgID).MarketSubSegment__c.remove(prgmss.marketsegment__r.marketsegmentname__c+',');
                        else if(mapPartnerProgram.get(prgID).MarketSubSegment__c.startsWith(prgmss.marketsegment__r.marketsegmentname__c))
                            mapPartnerProgram.get(prgID).MarketSubSegment__c=mapPartnerProgram.get(prgID).MarketSubSegment__c.remove(prgmss.marketsegment__r.marketsegmentname__c);
                        else
                            mapPartnerProgram.get(prgID).MarketSubSegment__c=mapPartnerProgram.get(prgID).MarketSubSegment__c.remove(','+prgmss.marketsegment__r.marketsegmentname__c);    
                    }
                    if(mapPartnerProgram.get(prgID).MarketSubSegment__c.endsWith(','))
                        mapPartnerProgram.get(prgID).MarketSubSegment__c=mapPartnerProgram.get(prgID).MarketSubSegment__c.removeEnd(',');
                }
            }
        delete lstMarkettoDel;*/
    }
    
/******************************************************************************************************************************************************************************************
 Release     : APR14
 BR          : BR-4703
 Description : Deactivate Program Levels, Program Features if the Programs are decommissioned
*******************************************************************************************************************************************************************************************/
public static void deactivateProgramLevelsFeatures(List<PartnerProgram__c> lstPrgs)
 {
    
    System.Debug('***** deactivateProgramLevelsFeatures ******');
    List<PartnerProgram__c> lstDecommPrgs = new List<PartnerProgram__c>();
    List<ProgramLevel__c> lstPrgLevel = new List<ProgramLevel__c>();
    List<ProgramFeature__c> lstPrgFeatures = new List<ProgramFeature__c>();
    List<ProgramRequirement__c> lstPrgRequirements = new List<ProgramRequirement__c>();
    
    for(PartnerProgram__c aPrg :lstPrgs)
    {
        if(aPrg.ProgramStatus__c == System.label.CLMAY13PRM34) //Decommissioned
            lstDecommPrgs.add(aPrg);    
    }
    
    lstPrgLevel = [Select id,LevelStatus__c from ProgramLevel__c Where PartnerProgram__c = :lstDecommPrgs and LevelStatus__c != :System.label.CLMAY13PRM21 Limit 1000 ];
    
    if(lstPrgLevel.size() > 0)
    {
        //Deactivating Program Features  
        lstPrgFeatures = [Select id,FeatureStatus__c from ProgramFeature__c where PartnerProgram__c = :lstDecommPrgs and FeatureStatus__c != :System.label.CLMAY13PRM07 Limit 1000 ];
        if(lstPrgFeatures.size() >0)
        {
            for(ProgramFeature__c aPrgFeature :lstPrgFeatures)
                aPrgFeature.FeatureStatus__c = System.label.CLMAY13PRM07; //'Inactive';
                
            try{    
                 Update  lstPrgFeatures;
                 System.Debug('********* '+lstPrgFeatures.size() + ' Program Features are Deactivated ****');
            }
            catch(Exception e)
            {
                System.debug(e.getMessage());  
            }
                
        } 
        
        
        //Deactivating Program requirements
        lstPrgRequirements = [Select id,Active__c from ProgramRequirement__c where PartnerProgram__c = :lstDecommPrgs and Active__c = true Limit 1000];
        if(lstPrgRequirements.size() > 0)
        {
            for(ProgramRequirement__c aPrgReq :lstPrgRequirements)
                aPrgReq.Active__c = false;
            try{    
                Update lstPrgRequirements;  
                System.Debug('********* '+lstPrgRequirements.size() + ' Program Requirements are Deactivated ****');
            }
            catch(Exception e)
            {
                System.debug(e.getMessage());  
            }
        
        }
        
        
        //Deactivating Program Levels
        for(ProgramLevel__c aPrgLvl :lstPrgLevel)
            aPrgLvl.LevelStatus__c = System.label.CLMAY13PRM21; //'Inactive';
        
        try{
            Update lstPrgLevel;
            System.Debug('********* '+lstPrgLevel.size() + ' Program Levels are Deactivated ****');
        }
        catch(Exception e)
        {
            System.debug(e.getMessage());  
        }
    
    }


 }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

public static void createGlobalProgramLevel(List<PartnerProgram__c> lstPrgs)
 {
    //Program Level create for each global Program
    
        List<ProgramLevel__c> lstProgLevel = new List<ProgramLevel__c>();
        
    try{
        for(PartnerProgram__c aPrg :lstPrgs)
        {
            if(aPrg.recordtypeid == Label.CLMAY13PRM15) //Global Program
            {
                
                //START: OCT14 Release
                //Modified as per OCT14 Release
                
                    ProgramLevel__c newProgLevel = new ProgramLevel__c();
                    newProgLevel.Name = aPrg.BaseLevelName__c;
                    newProgLevel.PartnerProgram__c = aPrg.id;
                    newProgLevel.Description__c = aPrg.BaseLevelDescription__c;
                    newProgLevel.Hierarchy__c = System.label.CLAPR14PRM27;
                    newProgLevel.ModifiablebyCountry__c = aPrg.ModifiablebyCountry__c;
                    newProgLevel.LevelStatus__c = aPrg.BaseLevelStatus__c;
                    newProgLevel.recordtypeid = System.label.CLMAY13PRM18;
                
                    lstProgLevel.add(newProgLevel); 
                    System.debug('*** Added new Program Level ');
                    
               //END: OCT14 Release     
            
            }       
        }
        
        if(lstProgLevel.size() > 0)
            insert lstProgLevel;    
    }   
    catch(System.Exception  e)
    {
       System.debug(e.getMessage());  
    }    
 
 }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
    @future
    public static void deactivateAccountAssignedPrograms(Set<ID> setInactivePrgLevelIDs)
    {
        system.debug('******** Deactivate Account Assigned Programs *********');
        try{
            List<ACC_PartnerProgram__c> lstAccPrograms = [Select id,Active__c from ACC_PartnerProgram__c where ProgramLevel__c in :setInactivePrgLevelIDs and Active__c = true Limit 1000];
            if(lstAccPrograms.size() > 0)
            {
        
                for(ACC_PartnerProgram__c accPrg :lstAccPrograms)
                    accPrg.Active__c = false;            
        
                System.debug('****** Deactivated Account Assigned Programs: '+lstAccPrograms.size());
                Update lstAccPrograms;
                
            }   
            
                    
            //Deactivating Program Assessment 26-Mar-14
            
            /*List<Assessment__c> lstAssessments = [Select id,Active__c from Assessment__c where ProgramLevel__c in :setInactivePrgLevelIDs and Active__c = true Limit 1000];
            if(lstAssessments.size() > 0)
            {
                for(Assessment__c aPrgAssessment :lstAssessments)
                    aPrgAssessment.Active__c = false;
            
                System.debug('****** Deactivated Program Assessments: '+lstAssessments.size());
                Update lstAssessments;  
            }*/

            //Added by Sreenivas Sake for BR-4915
            List<AssessementProgram__c> lstAssessments = [Select Assessment__c, Active__c from AssessementProgram__c where ProgramLevel__c in :setInactivePrgLevelIDs and Active__c = true Limit 1000];
            if(lstAssessments.size() > 0)
            {
                for(AssessementProgram__c aPrgAssessment :lstAssessments)
                    aPrgAssessment.Active__c = false;

                System.debug('****** Deactivated Program Assessments: '+lstAssessments.size());
                Update lstAssessments;  
            
            }
            //-----------------------------
            
                
        }
        catch(Exception e){
            System.debug(e.getMessage());  
        }
        
                
    }

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

   
}