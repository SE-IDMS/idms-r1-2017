@isTest
Private class AP_InvolvedOrganization_Test{

  static TestMethod void testingupdateInvolvedOrgonBRE(){
       Id rol_ID = [Select ID from UserRole where name = 'CEO' limit 1].ID;
        
         User TestUserRep = Utils_TestMethods.createStandardUser('UserRep');  
            TestUserRep.BypassVR__c =TRUE;
            TestUserRep.UserRoleID = rol_ID ;
            insert TestUserRep;      
        
system.runAs(TestUserRep) {
    
     Account acct = Utils_TestMethods.createAccount();
              acct.LeadingBusiness__c = Label.CL00318;
              acct.Account_VIPFlag__c = 'VIP1';
              insert acct;  
              Country__c newCountry = Utils_TestMethods.createCountry();
              newCountry.Region__c=Label.CL00320;
             insert newCountry;
    

    list<InvolvedOrganization__c> iOrgListIOrg = new list<InvolvedOrganization__c>();
        User newUser = Utils_TestMethods.createStandardUser('TestUser');         
         Database.SaveResult UserInsertResult = Database.insert(newUser, false);   
         User newUser1 = Utils_TestMethods.createStandardUser('TestUser1');         
         Database.SaveResult UserInsertResult1 = Database.insert(newUser1, false); 
        User newUser2 = Utils_TestMethods.createStandardUser('TestUser2');         
         Database.SaveResult UserInsertResult2 = Database.insert(newUser2, false);           
      User newUser3 = Utils_TestMethods.createStandardUser('TestUser3');         
         Database.SaveResult UserInsertResult3 = Database.insert(newUser3, false);  
    
     system.debug('**repuser**'+TestUserRep);
      BusinessRiskEscalationEntity__c bREE0= new BusinessRiskEscalationEntity__c(); 
          bREE0.Entity__c = Label.CL00318;
          insert bREE0;
          EntityStakeholder__c stakeholder0 = Utils_TestMethods.createEntityStakeholder(bREE0.Id,newUser.Id,Label.CL00309);
          insert stakeholder0;
          
          BusinessRiskEscalationEntity__c bREE1= new BusinessRiskEscalationEntity__c(); 
          bREE1.Entity__c = Label.CL00318;
          bREE1.SubEntity__c= Label.CL00319;            
          bREE1.Location_Type__c='Plant';
          insert bREE1;
           BusinessRiskEscalationEntity__c bREE13= new BusinessRiskEscalationEntity__c(); 
          bREE13.Entity__c = Label.CL00318;
          bREE13.SubEntity__c= Label.CL00319+'12';          
          bREE13.Location_Type__c='Plant';
          insert bREE13;
          BusinessRiskEscalationEntity__c bREE112= new BusinessRiskEscalationEntity__c(); 
          bREE112.Entity__c = Label.CL00318;
          bREE112.SubEntity__c= Label.CL00319+'SubEntity';
          //bREE112.Location__c='Loc2';
          bREE112.Location_Type__c='Plant';
          insert bREE112;
          
          
          EntityStakeholder__c stakeholder4 = Utils_TestMethods.createEntityStakeholder(bREE1.Id,newUser.Id,'BRE Resolution Leader');
          insert stakeholder4; 
           
          BusinessRiskEscalationEntity__c bREE21= new BusinessRiskEscalationEntity__c(Entity__c ='Test', Location_Type__c='Country Front Office (verticalized)');
          insert bREE21;
          BusinessRiskEscalationEntity__c bREE22= new BusinessRiskEscalationEntity__c(Entity__c ='Test', SubEntity__c= 'Argentina', Location_Type__c='Country Front Office (verticalized)');
          insert bREE22;
          BusinessRiskEscalationEntity__c bREE2= new BusinessRiskEscalationEntity__c(); 
          bREE2.Entity__c ='Test'; 
          bREE2.SubEntity__c= 'Argentina';
          bREE2.Location__c='Loc2';
          bREE2.Location_Type__c='Country Front Office (verticalized)';
          insert bREE2;
                   
          EntityStakeholder__c stakeholder2 = Utils_TestMethods.createEntityStakeholder(bREE2.Id,newUser2.Id,Label.CL10036);
          insert stakeholder2;
           BusinessRiskEscalationEntity__c bREE31= new BusinessRiskEscalationEntity__c(Entity__c = 'Power Global');
          insert bREE31;
          BusinessRiskEscalationEntity__c bREE3= new BusinessRiskEscalationEntity__c(); 
          bREE3.Entity__c = 'Power Global';
          bREE3.SubEntity__c='Low Voltage';
       
          insert bREE3;
          EntityStakeholder__c stakeholder6 = Utils_TestMethods.createEntityStakeholder(bREE13.Id,newUser.Id,'BRE Resolution Leader');
          insert stakeholder6; 
           BusinessRiskEscalationEntity__c bREE41= new BusinessRiskEscalationEntity__c(Entity__c = 'Power Global1'); 
          insert bREE41;
          BusinessRiskEscalationEntity__c bREE4= new BusinessRiskEscalationEntity__c(); 
          bREE4.Entity__c = 'Power Global1';
          bREE4.SubEntity__c='Low Voltage1';
       
          insert bREE4;
          EntityStakeholder__c stakeholder01 = Utils_TestMethods.createEntityStakeholder(bREE4.Id,newUser.Id,Label.CL00309);
          insert stakeholder01;
          EntityStakeholder__c stakeholder02 = Utils_TestMethods.createEntityStakeholder(bREE4.Id,newUser.Id,Label.CL00315);
          insert stakeholder02;
  /*        
         EntityStakeholder__c stakeholder3 = Utils_TestMethods.createEntityStakeholder(bREE3.Id,newUser1.Id,Label.CL10036);
         insert stakeholder3; 
          
           
       EntityStakeholder__c stakeholder11 = Utils_TestMethods.createEntityStakeholder(bREE1.Id,newUser.Id,Label.CL00309);
          insert stakeholder11;  
          */
             
       BusinessRiskEscalations__c brEscalation1 = Utils_TestMethods.createBusinessRiskEscalation(acct.Id,newCountry.Id,newUser.Id);
       brEscalation1.OriginatingOrganisation__c   =bREE2.id;
       brEscalation1.ResolutionOrganisation__c    =bREE3.id;
       brEscalation1.ResolutionLeader__c=newUser.id; 
       brEscalation1.TECH_InvolvedOrganization__c = 'TECH_InvolvedOrganization__c';
       brEscalation1.BusinessRiskSponsor__c = newUser.id; 
       insert brEscalation1;
        Utils_SDF_Methodology.removeFromRunOnce('AP_IO_AI');
       InvolvedOrganization__c invorg5=new InvolvedOrganization__c();
                    invorg5.BusinessRiskEscalation__c=brEscalation1.id;
                    invorg5.Representative__c=UserInfo.getUserId();
                    invorg5.Organization__c =bREE1.Id;
                    invorg5.TECH_FromBRE__c = false;
                    insert invorg5;
 Utils_SDF_Methodology.removeFromRunOnce('AP_IO_AI');
       InvolvedOrganization__c invorg=new InvolvedOrganization__c();
                    invorg.BusinessRiskEscalation__c=brEscalation1.id;
                    invorg.Representative__c=UserInfo.getUserId();
                    invorg.Organization__c =bREE13.Id;
                    invorg.TECH_FromBRE__c = false;
                    insert invorg;
                     Utils_SDF_Methodology.removeFromRunOnce('AP_IO_AI');
                    iOrgListIOrg.add(invorg);
                    iOrgListIOrg.add(invorg5);
                    system.debug('*******iOrgListIOrg***'+iOrgListIOrg);
                    if(iOrgListIOrg.size()>0){
                   // AP_InvolvedOrganization.updateInvolvedOrgonBRE(iOrgListIOrg,'No');
                   }
        InvolvedOrganization__c invorg2=new InvolvedOrganization__c(BusinessRiskEscalation__c=brEscalation1.id, Representative__c=UserInfo.getUserId(), Organization__c =bREE112.Id, TECH_FromBRE__c = false);
        insert invorg2;
        Utils_SDF_Methodology.removeFromRunOnce('AP_IO_AU');
         // invorg.Representative__c=null;
          update invorg;
          Utils_SDF_Methodology.removeFromRunOnce('AP_IO_AU');
          invorg.Representative__c=TestUserRep.Id;
          invorg.Organization__c =bREE4.Id;
    //invorg.Organization__c =bREE1.Id;
        update invorg;
       
      
        iOrgListIOrg.add(invorg);
        iOrgListIOrg.add(invorg2);
         AP_InvolvedOrganization.updateIORepresentative(iOrgListIOrg);
          AP_InvolvedOrganization.populateRepresentative(iOrgListIOrg); 
         map<id,set<id>>  maps = AP_InvolvedOrganization.getInvOrganizationIds(iOrgListIOrg);
         set<id> idsset = AP_InvolvedOrganization.getOrganizationIdsSet(maps);
         Map<id, set<EntityStakeholder__c>> maps123 = AP_InvolvedOrganization.getOrganizationStakeHolders(idsset);
         Map<id,set<string>> map567 = AP_InvolvedOrganization.getfilteredStakeHolders(maps,maps123);
         //AP_InvolvedOrganization.insertInvovledOrgStakeholders(map567,'No');
           string tempTechInvOrg=brEscalation1.TECH_InvolvedOrganization__c; 
           Map<id,set<string>> MapofBreInvOrgNames= New Map<id,set<string>>();
           Set<String> set12 = new Set<String>();
           set12.add(string.valueof(bREE1.id));
           set12.add(string.valueof(bREE112.id));
           MapofBreInvOrgNames.put(brEscalation1.id,set12);
           }
        // AP_InvolvedOrganization.updateInvolvedOrgonBRE(iOrgListIOrg,'No');  
         //AP_InvolvedOrganization.insertBREStakeholders(map567,maps,);
}
}