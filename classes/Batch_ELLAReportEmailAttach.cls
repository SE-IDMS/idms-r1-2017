/****************
Class Name :  Schedule_ELLAOfferProjectReport 

Remarks : For March(Q1) 16 Release   send notifcation to user 
Remarks : For June(Q2) 16 Release update
*****************/

global class Batch_ELLAReportEmailAttach implements Database.Batchable<sObject>,Schedulable{
    
    set<ID> setBREIdsToProcess = new set<ID>();
    
    List<ReportStakeholder__c> inrtlistReport= new List<ReportStakeholder__c>(); 
    ReportStakeholder__c reportstk;
    
    global Batch_ELLAReportEmailAttach() {

    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
            return Database.getQueryLocator([Select id,Email__c,BUReport__c,IsActive__c,IsUnsubscribe__c,CountryReport__c,User__c from LaunchReportStakeholders__c where (IsUnsubscribe__c=false AND IsActive__c=true) ]);
        
    }
    
    global void execute(Database.BatchableContext BC, List<LaunchReportStakeholders__c> scope) {
    
        for(LaunchReportStakeholders__c lRS : scope){
            reportstk = new  ReportStakeholder__c(); 
            if(lRS.Email__c!=null) {
                reportstk.Email__c=lRS.Email__c;
            }
            
            if(lRS.user__c!=null) {
                
                reportstk.Recipient__c=lRS.user__c;
                reportstk.LaunchReportStakeholder__c=lRS.id; 
            } 
            if(lRS.BUReport__c) {
                reportstk.BUReport__c=true; 
                
            } 
            if(lRS.CountryReport__c) {
                reportstk.CountryReport__c=true;        
                
            }
            
            inrtlistReport.add(reportstk);
            
        }
        
        if(inrtlistReport.size() > 0) {
            
            
            database.saveresult[] srlist = database.insert(inrtlistReport, false); 
        }
        
       
    } 
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
  
    global void execute(SchedulableContext sc) {
        Batch_ELLAReportEmailAttach erea = new Batch_ELLAReportEmailAttach();
        
        // label : CLMAR16ELLAbFO12 = 200
        ID batchprocessid = Database.executeBatch(erea,Integer.valueOf(System.Label.CLMAR16ELLAbFO12));           
    }
}