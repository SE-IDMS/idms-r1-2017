public class VFC_PAInvensysCaseEmailAddress{
    
    public String StrCaseEmailaddress{get;set;}
    public String StrAccountEmailaddress{get;set;}
     
     
    //Constructor
    public VFC_PAInvensysCaseEmailAddress(){}
    
    public class emailAddressCase{
        public string strCaseEmail{get;set;}
        public string StrAccountEmailaddress  {get;set;}
        public emailAddressCase(){}
    
    }
    
    public void emailCaseAccount(){
        String strCaseEmailId = ApexPages.currentPage().getParameters().get('Id');
        String strAccountEmailId = ApexPages.currentPage().getParameters().get('AccountId');
       list<case> lstCase= [select id,Accountid from case where id =:strCaseEmailId ];
      system.debug('lstCase+++++++'+lstCase);
       String strQuery;
       if(lstCase.size()>0){
           string strAccId =lstCase[0].Accountid;
           strQuery='select id, Email__c,Case__r.AccountId,Case__c,Account__c from Subscriber__c where (Case__c =\''+strCaseEmailId +'\' OR Account__c =\''+strAccId+'\') AND User__r.isActive = true';
       
       }
       else{
       
           strQuery='select id, Email__c,Case__r.AccountId,Case__c,Account__c from Subscriber__c where Case__c =\''+strCaseEmailId+'\' AND User__r.isActive = true' ;
       }
        system.debug('database.query(strQuery)+++++++'+strQuery);
        for(Subscriber__c s: database.query(strQuery)){
               system.debug('s----'+s);              
            if(!string.isBlank(s.Email__c) && !string.isBlank(s.Case__c) && strCaseEmailId == s.Case__c){
                    if(string.isBlank(StrCaseEmailaddress)){
                        StrCaseEmailaddress =s.Email__c;
                        
                    }
                    else{
                        StrCaseEmailaddress=StrCaseEmailaddress+';'+s.Email__c;
                        
                    }
                    continue;
            }
            if(!string.isBlank(s.Email__c) && !string.isBlank(s.Account__c)){
                    if(string.isBlank(StrAccountEmailaddress)){
                        StrAccountEmailaddress =s.Email__c;
                        
                    }
                    else{
                        StrAccountEmailaddress  =StrAccountEmailaddress +';'+s.Email__c;
                        
                    }
            }
            
        }
        system.debug('StrCaseEmailaddress----'+StrCaseEmailaddress);
    }

}