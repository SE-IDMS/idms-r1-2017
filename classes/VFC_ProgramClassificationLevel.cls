public class VFC_ProgramClassificationLevel
{

    public list<PartnerProgramClassification__c> lstClassificationLvl1 {get;set;}
    public list<String> lstClassificationLvl {set;}
    String tmp;
    public VFC_ProgramClassificationLevel(ApexPages.StandardController controller) {
        lstClassificationLvl1  = [Select classificationlevelCatalog__r.Name, classificationlevelCatalog__r.parentclassificationlevel__c , 
                                   ClassificationLevelCatalog__r.ClassificationLevelName__c 
                       from PartnerProgramClassification__c where PartnerProgram__c=:controller.getId()
                      order by ClassificationLevelCatalog__r.Name limit 1000]; 

    }

    public List<String> getlstClassificationLvl()
    {
        List<String> lstClassificationLvl2 = new List<String>();
        map<String,List<String>> mapClassification = new map<String,List<String>>();
        String ClassLevel1;
        List<String> lstClassLevel1Name = new List<String>();
        
        for(PartnerProgramClassification__c var :lstClassificationLvl1)
        {           
            if(var.classificationlevelCatalog__r.parentclassificationlevel__c == null)
            {
                ClassLevel1 = var.ClassificationLevelCatalog__r.ClassificationLevelName__c;
                List<String> level2 = new List<String>();
                mapClassification.put(ClassLevel1,level2);
                lstClassLevel1Name.add(ClassLevel1);
                
            }   
            else
                mapClassification.get(ClassLevel1).add(var.ClassificationLevelCatalog__r.ClassificationLevelName__c);
        }
        
        lstClassLevel1Name.sort();
        for(String aClass :lstClassLevel1Name)
        {
            lstClassificationLvl2.add(aClass);
            if(mapClassification.get(aClass).size() > 0)
            {
               List<String> lstSortedValue = mapClassification.get(aClass);
               lstSortedValue.sort();
               lstClassificationLvl2.add(' - '+String.join(lstSortedValue,','));     
            }   
            
        }
        
        
       // if(classLv2 != '')
         //   lstClassificationLvl2.add(classLv2);

        return lstClassificationLvl2;
    }

}