//test class for uimsv22ImplServiceImsSchneiderCom
@isTest
public class uimsv22ImplServiceImsSchneiderComTest { 
    static testmethod void testSetPassword(){
        try{       
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();
            test.setPassword('callerFid','authentificationToken','password');
        }
        catch (exception e){}        
    }
    static testmethod void testRequestPhoneId(){
        try{
            uimsv22ServiceImsSchneiderCom.accessElement access = new uimsv22ServiceImsSchneiderCom.accessElement();        
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();
            test.requestPhoneIdChange('callerFid','authentificationToken',access ,'newPhoneId');
        }
        catch (exception e){}        
    }
    static testmethod void testUpdateUser(){
        try {
            uimsv22ServiceImsSchneiderCom.userV6  user6 = new uimsv22ServiceImsSchneiderCom.userV6() ;
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();
            test.updateUser('callerFid','samlAssertion',user6); 
        }catch(exception e){}
    }
    
    static testmethod void testsetPasswordWithSms(){
        try {
            
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();
            test.setPasswordWithSms('callerFid','phoneId','smsToken','tokenType','password');
        }catch(exception e){}
    }
    static testmethod void testgetPrograms(){
        try{
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();
            test.getPrograms('callerFid','samlAssertion');
        }
        catch (exception e){}
    }
    static testmethod void testrequestEmailChange(){
        try{
            uimsv22ServiceImsSchneiderCom.accessElement access = new uimsv22ServiceImsSchneiderCom.accessElement(); 
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();  
            test.requestEmailChange('callerFid','samlAssertion',access,'newEmail');
        }catch(exception e){}
    }
    static testmethod void testacceptUserMerge(){
        try{
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();  
            test.acceptUserMerge('callerFid','authentificationToken');
        }catch(exception e){}
    }
    static testmethod void testrejectUserMerge(){
        try{
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();     
            test.rejectUserMerge('callerFid','authentificationToken');
        }catch(exception e){}
    }
    static testmethod void testupdatePassword(){
        try{
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();     
            test.updatePassword('callerFid','samlAssertion','oldPassword','newPassword');
        }catch(exception e){}
    }
    static testmethod void testgetUser(){
        try{
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();     
            test.getUser('callerFid','samlAssertionOrToken');
        }catch(exception e) {}
    }
    static testmethod void testupdateEmail(){
        try{
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();     
            test.updateEmail('callerFid','authentificationToke');
        }catch(exception e) {}
    }
    static testmethod void testgetAccessControl(){
        try{
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();     
            test.getAccessControl('callerFid','samlAssertion');
        }catch(exception e) {}
    }
    static testmethod void testupdatePhoneId(){
        try{
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();     
            test.updatePhoneId('callerFid','phoneId','smsToken');
        }catch(exception e) {}
        
    }
    static testmethod void testactivateIdentity(){
        try{
            uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort test = new uimsv22ImplServiceImsSchneiderCom.UserManager_UIMSV22_ImplPort();     
            test.activateIdentity('callerFid','password','authentificationToken');
        }catch(exception e) {}
        
    }
    // test metods for multiple cases
    static testmethod void testAllService(){
        uimsv22ServiceImsSchneiderCom.userV6 userV6 = new uimsv22ServiceImsSchneiderCom.userV6();
        uimsv22ServiceImsSchneiderCom.ImsMailerException ImsMailerException = new uimsv22ServiceImsSchneiderCom.ImsMailerException ();
        uimsv22ServiceImsSchneiderCom.userV5 userV5 = new uimsv22ServiceImsSchneiderCom.userV5 ();
        uimsv22ServiceImsSchneiderCom.setPassword setPassword = new uimsv22ServiceImsSchneiderCom.setPassword ();
        uimsv22ServiceImsSchneiderCom.updatePasswordResponse updatePasswordResponse = new uimsv22ServiceImsSchneiderCom.updatePasswordResponse ();
        uimsv22ServiceImsSchneiderCom.getAccessControlResponse getAccessControlResponse = new uimsv22ServiceImsSchneiderCom.getAccessControlResponse ();
        uimsv22ServiceImsSchneiderCom.entry_element entry_element = new uimsv22ServiceImsSchneiderCom.entry_element ();
        uimsv22ServiceImsSchneiderCom.program program = new uimsv22ServiceImsSchneiderCom.program ();
        uimsv22ServiceImsSchneiderCom.rejectUserMergeResponse rejectUserMergeResponse = new uimsv22ServiceImsSchneiderCom.rejectUserMergeResponse ();
        uimsv22ServiceImsSchneiderCom.officialsIds officialsIds = new uimsv22ServiceImsSchneiderCom.officialsIds ();
        uimsv22ServiceImsSchneiderCom.LdapTemplateNotReadyException  LdapTemplateNotReadyException  = new uimsv22ServiceImsSchneiderCom.LdapTemplateNotReadyException ();
        uimsv22ServiceImsSchneiderCom.updateUserResponse updateUserResponse = new uimsv22ServiceImsSchneiderCom.updateUserResponse ();
        uimsv22ServiceImsSchneiderCom.activateIdentity activateIdentity = new uimsv22ServiceImsSchneiderCom.activateIdentity ();
        uimsv22ServiceImsSchneiderCom.rejectUserMerge rejectUserMerge = new uimsv22ServiceImsSchneiderCom.rejectUserMerge ();
        uimsv22ServiceImsSchneiderCom.UnexpectedLdapResponseException  UnexpectedLdapResponseException  = new uimsv22ServiceImsSchneiderCom.UnexpectedLdapResponseException ();
        uimsv22ServiceImsSchneiderCom.setPasswordWithSms setPasswordWithSms = new uimsv22ServiceImsSchneiderCom.setPasswordWithSms ();
        uimsv22ServiceImsSchneiderCom.childs_element childs_element = new uimsv22ServiceImsSchneiderCom.childs_element ();
        uimsv22ServiceImsSchneiderCom.acceptUserMerge acceptUserMerge = new uimsv22ServiceImsSchneiderCom.acceptUserMerge ();
        uimsv22ServiceImsSchneiderCom.programLevel programLevel = new uimsv22ServiceImsSchneiderCom.programLevel ();
        uimsv22ServiceImsSchneiderCom.accessList_element accessList_element = new uimsv22ServiceImsSchneiderCom.accessList_element ();
        uimsv22ServiceImsSchneiderCom.updateEmail updateEmail = new uimsv22ServiceImsSchneiderCom.updateEmail ();
        uimsv22ServiceImsSchneiderCom.IMSServiceSecurityCallNotAllowedException  IMSServiceSecurityCallNotAllowedException  = new uimsv22ServiceImsSchneiderCom.IMSServiceSecurityCallNotAllowedException  ();
        uimsv22ServiceImsSchneiderCom.acceptUserMergeResponse acceptUserMergeResponse = new uimsv22ServiceImsSchneiderCom.acceptUserMergeResponse ();
        uimsv22ServiceImsSchneiderCom.accessElement accessElement = new uimsv22ServiceImsSchneiderCom.accessElement ();
        uimsv22ServiceImsSchneiderCom.requestPhoneIdChangeResponse requestPhoneIdChangeResponse = new uimsv22ServiceImsSchneiderCom.requestPhoneIdChangeResponse ();
        uimsv22ServiceImsSchneiderCom.getUserResponse getUserResponse = new uimsv22ServiceImsSchneiderCom.getUserResponse ();
        uimsv22ServiceImsSchneiderCom.getUser getUser = new uimsv22ServiceImsSchneiderCom.getUser ();
        uimsv22ServiceImsSchneiderCom.activateIdentityResponse activateIdentityResponse = new uimsv22ServiceImsSchneiderCom.activateIdentityResponse ();
        uimsv22ServiceImsSchneiderCom.requestPhoneIdChange requestPhoneIdChange = new uimsv22ServiceImsSchneiderCom.requestPhoneIdChange ();
        uimsv22ServiceImsSchneiderCom.getPrograms getPrograms = new uimsv22ServiceImsSchneiderCom.getPrograms ();
        uimsv22ServiceImsSchneiderCom.InvalidImsServiceMethodArgumentException  InvalidImsServiceMethodArgumentException  = new uimsv22ServiceImsSchneiderCom.InvalidImsServiceMethodArgumentException  ();
        uimsv22ServiceImsSchneiderCom.accessTree accessTree = new uimsv22ServiceImsSchneiderCom.accessTree ();
        uimsv22ServiceImsSchneiderCom.requestEmailChange requestEmailChange = new uimsv22ServiceImsSchneiderCom.requestEmailChange ();
        uimsv22ServiceImsSchneiderCom.RequestedInternalUserException  RequestedInternalUserException  = new uimsv22ServiceImsSchneiderCom.RequestedInternalUserException  ();
        uimsv22ServiceImsSchneiderCom.getAccessControl getAccessControl = new uimsv22ServiceImsSchneiderCom.getAccessControl ();
        uimsv22ServiceImsSchneiderCom.identity identity = new uimsv22ServiceImsSchneiderCom.identity ();
        uimsv22ServiceImsSchneiderCom.updatePhoneIdResponse updatePhoneIdResponse = new uimsv22ServiceImsSchneiderCom.updatePhoneIdResponse ();
        uimsv22ServiceImsSchneiderCom.ids_element ids_element = new uimsv22ServiceImsSchneiderCom.ids_element ();
        uimsv22ServiceImsSchneiderCom.updateEmailResponse updateEmailResponse = new uimsv22ServiceImsSchneiderCom.updateEmailResponse ();
        uimsv22ServiceImsSchneiderCom.getProgramsResponse getProgramsResponse = new uimsv22ServiceImsSchneiderCom.getProgramsResponse ();
        uimsv22ServiceImsSchneiderCom.SecuredImsException  SecuredImsException  = new uimsv22ServiceImsSchneiderCom.SecuredImsException  ();
        uimsv22ServiceImsSchneiderCom.updatePassword updatePassword = new uimsv22ServiceImsSchneiderCom.updatePassword ();
        uimsv22ServiceImsSchneiderCom.RequestedEntryNotExistsException  RequestedEntryNotExistsException  = new uimsv22ServiceImsSchneiderCom.RequestedEntryNotExistsException  ();
        uimsv22ServiceImsSchneiderCom.updateUser updateUser = new uimsv22ServiceImsSchneiderCom.updateUser ();
        uimsv22ServiceImsSchneiderCom.activateIdentityNoPassword activateIdentityNoPassword = new uimsv22ServiceImsSchneiderCom.activateIdentityNoPassword ();
        uimsv22ServiceImsSchneiderCom.IOException  IOException  = new uimsv22ServiceImsSchneiderCom.IOException  ();
        uimsv22ServiceImsSchneiderCom.setPasswordWithSmsResponse setPasswordWithSmsResponse = new uimsv22ServiceImsSchneiderCom.setPasswordWithSmsResponse ();
        uimsv22ServiceImsSchneiderCom.activateIdentityNoPasswordResponse activateIdentityNoPasswordResponse = new uimsv22ServiceImsSchneiderCom.activateIdentityNoPasswordResponse ();
        uimsv22ServiceImsSchneiderCom.updatePhoneId updatePhoneId = new uimsv22ServiceImsSchneiderCom.updatePhoneId ();
        uimsv22ServiceImsSchneiderCom.InactiveUserImsException  InactiveUserImsException  = new uimsv22ServiceImsSchneiderCom.InactiveUserImsException  ();
        uimsv22ServiceImsSchneiderCom.setPasswordResponse setPasswordResponse = new uimsv22ServiceImsSchneiderCom.setPasswordResponse ();
        uimsv22ServiceImsSchneiderCom.requestEmailChangeResponse requestEmailChangeResponse = new uimsv22ServiceImsSchneiderCom.requestEmailChangeResponse ();
    }
}