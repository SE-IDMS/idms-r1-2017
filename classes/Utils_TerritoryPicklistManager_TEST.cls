/*
    Author          : Accenture Team 
    Date Created    : 29/02/2012
    Description     : Test Class for Territory Picklist Manager Utility class
*/
@isTest
private class  Utils_TerritoryPicklistManager_TEST
{
       static testMethod void   competitorPicklists_TestMethod()
       {
           Utils_PicklistManager Picklists = Utils_Factory.getPickListManager('TERR');
           
           
           // create test data
           Country__c country = Utils_TestMethods.createCountry();
           insert country;
               
           StateProvince__c stateprovince = Utils_TestMethods.createStateProvince(country.id);
           insert stateprovince;
    
           SE_Territory__c territory =  Utils_TestMethods.createTerritory(country.id, stateprovince.id);
           insert territory;
           
           Picklists.getPickListLabels();
           Picklists.getPicklistValues(1, '');  
           Picklists.getPicklistValues(2, country.id);
           
       }   
}