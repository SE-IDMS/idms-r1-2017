@isTest
private class VFC_SPAContractSync_TEST
{

    static testMethod void SPAContractSync_1() 
    {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        
        User l2user=Utils_TestMethods.createStandardUser('l2');
        insert l2user;
        
        User l1user=Utils_TestMethods.createStandardUser('l1');        
        l1user.ManagerId=l2user.id;
        insert l1user;
        
        User runasthisuser=Utils_TestMethods.createStandardUser('sp');
        runasthisuser.ManagerId=l1user.Id;
        insert runasthisuser;        
        
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,runasthisuser.id);
        insert param;
        
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        
        List<SPAQuotaQuarter__c> quotaquarters=Utils_SPA.createQuotaQuarters(quota.Id);
        insert quotaquarters;
        
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
        
         //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.salesperson__c=runasthisuser.id;
        insert sparequest;      

        
        ApexPages.StandardController controller=new ApexPages.StandardController(sparequest);
        VFC_SPAContractSync controllerinstance=new VFC_SPAContractSync(controller); 

        try
        {
            PageReference pg1 = controllerinstance.sync();
            PageReference pg2 = controllerinstance.validateSync();
            
            
        }
        catch (Exception e)
        {
            System.debug('....error.....'+e);
        }
    }
    static testMethod void SPAContractSync_2() 
    {
        //starting with the flow
        SPASalesGroup__c salesgroup=Utils_SPA.createSPASalesGroup('mysalesgroup');
        insert salesgroup;
        
        User l2user=Utils_TestMethods.createStandardUser('l2');
        insert l2user;
        
        User l1user=Utils_TestMethods.createStandardUser('l1');        
        l1user.ManagerId=l2user.id;
        insert l1user;
        
        User runasthisuser=Utils_TestMethods.createStandardUser('sp');
        runasthisuser.ManagerId=l1user.Id;
        insert runasthisuser;        
        
        UserParameter__c param=Utils_SPA.createUserParameter(salesgroup.id,runasthisuser.id);
        insert param;
        
        SPAQuota__c quota=Utils_SPA.createQuota('HK_SAP',salesgroup.id);
        insert quota;
        
        List<SPAQuotaQuarter__c> quotaquarters=Utils_SPA.createQuotaQuarters(quota.Id);
        insert quotaquarters;
        
        //SPA Account with Legacy Account
        Account acc=Utils_SPA.createAccount();
        
        //SPA Opportunity
        Opportunity opp=Utils_SPA.createOpportunity(acc.id);
        insert opp;
        
        //SPA Threshold Amount
        SPAThresholdAmount__c thresholdamount=Utils_SPA.createThresholdAmount('HK_SAP','Other');
        insert thresholdamount;
        
         //SPA Threshold Discount
        SPAThresholdDiscount__c thresholddiscount=Utils_SPA.createThresholdDiscount('HK_SAP','Additional Discount');
        insert thresholddiscount;
        
        SPARequest__c sparequest=Utils_SPA.createSPARequest('Discount',opp.id);
        sparequest.salesperson__c=runasthisuser.id;
        sparequest.BackOfficeContractNumber__c='12345';
        insert sparequest;      

        
        ApexPages.StandardController controller=new ApexPages.StandardController(sparequest);
        VFC_SPAContractSync controllerinstance=new VFC_SPAContractSync(controller); 

        try
        {           
            PageReference pg1 = controllerinstance.validateSync();          
            
        }
        catch (Exception e)
        {
            System.debug('....error.....'+e);
        }
    }
    
}