public class VFC_Fsearch
{

    public PageReference getSize() {
        return null;
    }

    public boolean displayPopup {get; set;}
    public String searchOrderText {get; set;}

    public boolean consoleScript {get; set;}
    public boolean errorMessage{get;set;}
    public Case myCase{get;set;}
  
    public VFC_Fsearch() {
     displayPopup = false;
     searchOrderText = 'Search Order';
     consoleScript = false;
     String caseId = ApexPages.currentPage().getParameters().get('caseid');
     if (caseId != null) {
         Case myCase = [select Id, AnswerToCustomer__c, Subject from Case where id=:caseId];
         ApexPages.currentPage().getParameters().put('caseSubject', myCase.Subject);
      }
      
     
    }
    
    public void searchOrderPop()
    {
     displayPopup = !displayPopup ;
     if (displayPopup != true) searchOrderText = 'Search Order';
     else searchOrderText = 'Close Search Order';
    }

    public String searchOrderText() {
        return searchOrderText;
    }
    public void closePopup()
    {
     displayPopup = false;     
    }
    
    public PageReference backToCase() {
       try {
        return new PageReference('/'+updateCase());
        } catch (Exception e) {
          ApexPages.addMessages(e);
          errorMessage = true;
          return ApexPages.currentPage();
        } 
    } 
    
    public PageReference updateAndClose() {
    try {
         String caseId = updateCase();
        consoleScript = true;
        
        } catch (Exception e) {
          errorMessage = true;
          ApexPages.addMessages(e);
        } 
       return ApexPages.currentPage();
    
    }
    public String updateCase() {
        String caseId = ApexPages.currentPage().getParameters().get('caseid');
        Case myCase = [select Id, AnswerToCustomer__c from Case where id=:caseId];
        
          AP_CaseUpdateWithFedSearchResults.executeUpdate(myCase);
          
      
        return caseid;
    }
    public PageReference continueToCase() {
        return new PageReference('/'+ApexPages.currentPage().getParameters().get('caseid'));
    }
    
    public String getUserCCC(){
        GroupMember[] memberships = [select Group.Name from GroupMember where Group.Name like 'Pilot - FSearch%' and UserOrGroupId = : UserInfo.getUserId() limit 1];
        return memberships.size()==0?null:memberships[0].Group.Name.replace('Pilot - FSearch - ', '');
    }
        
}