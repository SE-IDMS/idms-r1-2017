@isTest
public class VFC_AccountMasterProfile_TEST{
    public static testMethod void Testrun1() {
        Account a = Utils_TestMethods.createAccount();
        a.ClassLevel1__c='Financier';
        insert a;
        Country__c c = Utils_TestMethods.createCountry(); 
        insert c;
        a.country__c=c.id;
        a.MarketSegment__c='BD1';
        update a;
        AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
        insert amp;
        
        amp.Q4Rating__c='4';
        update amp;
        OPP_Product__c op=new OPP_Product__c(BusinessUnit__c = 'businessUnit',Family__c = 'family',ProductFamily__c = 'productFamily',ProductLine__c = 'productLine',HierarchyType__c = 'PM0-BUSINESS-UNIT',IsActive__c = true);
        insert op;
        List<CAPGlobalMasterData__c> CAPGlobalMasterDataCLList = new List<CAPGlobalMasterData__c>();
        List<CAPGlobalMasterData__c> CAPGlobalMasterDefaultDataList = new List<CAPGlobalMasterData__c>();
        List<CAPCountryWeight__c> countryweightsList=new List<CAPCountryWeight__c>();
        for(integer i=0;i<8;i++)
        {
            CAPCountryWeight__c capweight=new CAPCountryWeight__c(ClassificationLevel1__c='FI',Country__c=c.id,CountryWeight__c=3,MarketSegment__c='BD1');
            capweight.QuestionSequence__c='Q'+(i+1);
            countryweightsList.add(capweight);
        }
        insert countryweightsList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmd =new CAPGlobalMasterData__c(QuestionInterpretation__c='Quetion', AnswerInterpretation1__c='ans1',AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='FI');               capgmd.QuestionSequence__c='Q'+(i+1);                                       
            CAPGlobalMasterDataCLList.add(capgmd);                                                          
        }
        insert CAPGlobalMasterDataCLList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmdDefault =new CAPGlobalMasterData__c(QuestionInterpretation__c='Question',AnswerInterpretation1__c='ans1',AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='Default');
            capgmdDefault.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDefaultDataList.add(capgmdDefault);                                                           
        }
        insert CAPGlobalMasterDefaultDataList;
        AccountMasterProfilePAM__c aPAM=new AccountMasterProfilePAM__c(GMRBusinessUnit__c='ECOBUILDINGS',AccountMasterProfile__c=amp.id,PAM__c=10,DirectSales__c=2,IndirectSales__c=4);
        aPAM.AccountMasterProfile__c=amp.id;
        
        String name='ECOBUILDINGS';
        Pagereference pg = new pagereference('/apex/VFP_createEditAccountMasterProfile?accId='+a.id+'&ampID='+amp.ID); 
        Test.setCurrentPageReference(pg);
        VFC_AccountMasterProfile  ampc = new VFC_AccountMasterProfile(new ApexPages.StandardController(amp));  
        VFC_AccountMasterProfile.wrapPAMDetails wrppamdetails= new VFC_AccountMasterProfile.wrapPAMDetails(aPAM,op.name);
        VFC_AccountMasterProfile.wrapscoring wrpScoring= new VFC_AccountMasterProfile.wrapscoring();
        
        ampc.saveAMP();
        ampc.editAMP();
        ampc.Cancel();
        ampc.backToAccount();
    }
    
    public static testMethod void Testrun2() {
        Account a = Utils_TestMethods.createAccount();
        a.ClassLevel1__c='FI';
        insert a;
        Country__c c = Utils_TestMethods.createCountry(); 
        insert c;
        a.country__c=c.id;
        update a;
        AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
        insert amp;
        AccountMasterProfilePAM__c aPAM=new AccountMasterProfilePAM__c(GMRBusinessUnit__c='ECOBUILDINGS',AccountMasterProfile__c=amp.id);
        String name='ECOBUILDINGS';
        List<CAPGlobalMasterData__c> CAPGlobalMasterDataCLList = new List<CAPGlobalMasterData__c>();
        List<CAPGlobalMasterData__c> CAPGlobalMasterDefaultDataList = new List<CAPGlobalMasterData__c>();
        List<CAPCountryWeight__c> countryweightsList=new List<CAPCountryWeight__c>();
        for(integer i=0;i<8;i++)
        {
            CAPCountryWeight__c capweight=new CAPCountryWeight__c(ClassificationLevel1__c='FI',Country__c=c.id,CountryWeight__c=3);
            capweight.QuestionSequence__c='Q'+(i+1);
            countryweightsList.add(capweight);
        }
        insert countryweightsList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmd =new CAPGlobalMasterData__c(QuestionInterpretation__c='Quetion', AnswerInterpretation1__c='ans1',AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='FI');                capgmd.QuestionSequence__c='Q'+(i+1);                                      
            CAPGlobalMasterDataCLList.add(capgmd);                                                          
        }
        insert CAPGlobalMasterDataCLList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmdDefault =new CAPGlobalMasterData__c(QuestionInterpretation__c='Question',AnswerInterpretation1__c='ans1',AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='Default');
            capgmdDefault.QuestionSequence__c='Q'+(i+1);
            CAPGlobalMasterDefaultDataList.add(capgmdDefault);                                                           
        }
        insert CAPGlobalMasterDefaultDataList;
        Pagereference pg = new pagereference('/apex/VFP_createEditAccountMasterProfile?accId='+a.id+'&ampID='+null); 
        Test.setCurrentPageReference(pg);
        VFC_AccountMasterProfile  ampc = new VFC_AccountMasterProfile(new ApexPages.StandardController(amp));  
        VFC_AccountMasterProfile.wrapPAMDetails wrppamdetails= new VFC_AccountMasterProfile.wrapPAMDetails(aPAM,name);
        VFC_AccountMasterProfile.wrapscoring wrpScoring= new VFC_AccountMasterProfile.wrapscoring();
        ampc.saveAMP();
        ampc.editAMP();
        ampc.Cancel();
        ampc.backToAccount();
    }
    public static testMethod void Testrun3() {
        Account a = Utils_TestMethods.createAccount();
        insert a;
        AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
        insert amp;
        Boolean hasNoError=FALSE;
        Boolean hasEditAccess=True;
        Boolean hasError=True;
        AccountMasterProfilePAM__c aPAM=new AccountMasterProfilePAM__c(GMRBusinessUnit__c='ECOBUILDINGS',AccountMasterProfile__c=amp.id);
        String name='ECOBUILDINGS';
        Pagereference pg = new pagereference('/apex/VFP_ViewAccountMasterProfile?accId='+a.id+'&ampID='+amp.id); 
        Test.setCurrentPageReference(pg);
        VFC_AccountMasterProfile  ampc = new VFC_AccountMasterProfile(new ApexPages.StandardController(amp));  
        VFC_AccountMasterProfile.wrapPAMDetails wrppamdetails= new VFC_AccountMasterProfile.wrapPAMDetails(aPAM,name);
        VFC_AccountMasterProfile.wrapscoring wrpScoring= new VFC_AccountMasterProfile.wrapscoring();
        ampc.saveAMP();
        ampc.editAMP();
        ampc.Cancel();
        ampc.backToAccount();
    }
    public static testMethod void Testrun4() {
        Account a = Utils_TestMethods.createAccount();
        a.ClassLevel1__c='Default';
        insert a;
        Country__c c = Utils_TestMethods.createCountry(); 
        insert c;
        a.country__c=c.id;
        update a;
        AccountMasterProfile__c amp=new AccountMasterProfile__c(Account__c=a.id,Q1Rating__c='1',Q2Rating__c='2',Q3Rating__c='2',Q4Rating__c='4',Q5Rating__c='3',Q6Rating__c='2',Q7Rating__c='4',Q8Rating__c='2',DirectSales__c=200,IndirectSales__c=300,totalPam__c=600);
        insert amp;
        amp.Q4Rating__c='4';
        update amp;
        
        List<CAPGlobalMasterData__c> CAPGlobalMasterDataCLList = new List<CAPGlobalMasterData__c>();
        List<CAPGlobalMasterData__c> CAPGlobalMasterDefaultDataList = new List<CAPGlobalMasterData__c>();
        List<CAPCountryWeight__c> countryweightsList=new List<CAPCountryWeight__c>();
        for(integer i=0;i<8;i++)
        {
            CAPCountryWeight__c capweight=new CAPCountryWeight__c(ClassificationLevel1__c='FI',Country__c=c.id,CountryWeight__c=3);
            capweight.QuestionSequence__c='Q'+(i+1);
            countryweightsList.add(capweight);
        }
        insert countryweightsList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmd =new CAPGlobalMasterData__c(QuestionInterpretation__c='Quetion', AnswerInterpretation1__c='ans1',AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='FI');               capgmd.QuestionSequence__c='Q'+(i+1);                                        
            CAPGlobalMasterDataCLList.add(capgmd);                                                          
        }
        insert CAPGlobalMasterDataCLList;
        for(integer i=0;i<8;i++)
        {
            CAPGlobalMasterData__c capgmdDefault =new CAPGlobalMasterData__c(QuestionInterpretation__c='Question',AnswerInterpretation1__c='ans1',AnswerInterpretation2__c='ans2',AnswerInterpretation3__c='ans3',AnswerInterpretation4__c='ans4',ClassificationLevel1__c='Default');
            capgmdDefault.QuestionSequence__c='Q'+(i+1); 
            CAPGlobalMasterDefaultDataList.add(capgmdDefault);                                                           
        }
        insert CAPGlobalMasterDefaultDataList;
        AccountMasterProfilePAM__c aPAM=new AccountMasterProfilePAM__c(GMRBusinessUnit__c='ECOBUILDINGS',AccountMasterProfile__c=amp.id);
        String name='ECOBUILDINGS';
        Pagereference pg = new pagereference('/apex/VFP_createEditAccountMasterProfile?accId='+a.id+'&ampID='+amp.ID); 
        Test.setCurrentPageReference(pg);
        VFC_AccountMasterProfile  ampc = new VFC_AccountMasterProfile(new ApexPages.StandardController(amp));  
        VFC_AccountMasterProfile.wrapPAMDetails wrppamdetails= new VFC_AccountMasterProfile.wrapPAMDetails(aPAM,name);
        VFC_AccountMasterProfile.wrapscoring wrpScoring= new VFC_AccountMasterProfile.wrapscoring();
       ampc.saveAMP();
        ampc.editAMP();
        ampc.Cancel();
        ampc.backToAccount();
    }
}