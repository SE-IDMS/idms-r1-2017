/**************************************************************************************
    Author: Fielo Team
    Date: 13/04/2016
    Description: 
    Related Components: 
***************************************************************************************/

@isTest
public with sharing class FieloPRM_AP_ComponentTriggersTest {
  
    public static testMethod void testUnit_topSectionCheckChangeLayout(){
        Id IdRTComponentTopSection = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'FieloPRM_C_TopSection'].Id;
        Id IdRTComponentTopSection1 = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'FieloPRM_C_PartnerLocatorForm'].Id;

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c();
        FieloEE__Component__c comp;
        FieloEE__Component__c comp1
        ;
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);

            menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Test Menu'
            );
            insert menu;
            
            comp = new FieloEE__Component__c(
                Name = 'Top Section',
                FieloEE__Menu__c = menu.Id, 
                RecordTypeId = IdRTComponentTopSection,
                FieloEE__Layout__c = 'Grid'
            );
            insert comp;
            
            comp1 = new FieloEE__Component__c(
                Name = 'Top Section',
                FieloEE__Menu__c = menu.Id, 
                RecordTypeId = IdRTComponentTopSection1,
                FieloEE__Layout__c = 'Grid'
            );
            insert comp1;
            
            FieloEE__News__c news = new FieloEE__News__c(
                FieloEE__Component__c = comp.Id,
                FieloEE__IsActive__c = true,
                FieloEE__HasSegments__c = true,
                F_PRM_Style__c = 'Side Image',
                F_PRM_PartnerNameWelcomeTitle__c = true,
                FieloEE__AttachmentId__c = 'XXXXXXXXXXXXXXX',
                F_PRM_BackgroundColor__c = 'White',
                F_PRM_ContentFilter__c = 'P-WW',
                FieloEE__Title__c = 'Test',
                FieloEE__ExtractText__c = 'Test',
                F_PRM_LinkLabel__c = 'Test',
                F_PRM_GoToLink__c = 'www.fielo.com'
            );
            insert news;

            FieloEE__PublicSettings__c ps = new FieloEE__PublicSettings__c(SetupOwnerId=UserInfo.getOrganizationId(), CacheEnabled__c = true);
            insert ps;
            
            comp.FieloEE__Layout__c = 'Slider';
            try{
                update comp;
            }catch(Exception e){}

            delete comp1;
        }
        
    }
    
}