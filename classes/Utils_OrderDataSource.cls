public class Utils_OrderDataSource
{
    public Boolean test;
    
    public List<SelectOption> getColumns()
    {
        List<SelectOption> columns = new List<SelectOption>();
        columns.add(new SelectOption('SEOrderReference__c', sObjectType.OPP_OrderLink__c.fields.SEOrderReference__c.getLabel()));
        columns.add(new SelectOption('PONumber__c', sObjectType.OPP_OrderLink__c.fields.PONumber__c.getLabel()));
        columns.add(new SelectOption('OrderCreationDate__c', sObjectType.OPP_OrderLink__c.fields.OrderCreationDate__c.getLabel()));
        columns.add(new SelectOption('Status__c', sObjectType.OPP_OrderLink__c.fields.Status__c.getLabel()));
        columns.add(new SelectOption('Express__c', sObjectType.OPP_OrderLink__c.fields.Express__c.getLabel()));
        columns.add(new SelectOption('OrderPlacedThrough__c', sObjectType.OPP_OrderLink__c.fields.OrderPlacedThrough__c.getLabel()));
        columns.add(new SelectOption('OrderEnteredBy__c', sObjectType.OPP_OrderLink__c.fields.OrderEnteredBy__c.getLabel()));
        columns.add(new SelectOption('BackOffice__c', sObjectType.OPP_OrderLink__c.fields.BackOffice__c.getLabel()));    
        return columns;
    }
   
    public List<SelectOption> getDetailsColumns()
    {
        List<SelectOption> columns = new List<SelectOption>();
        columns.add(new SelectOption('ItemNumber__c', sObjectType.OPP_DeliveryDetails__c.fields.ItemNumber__c.getLabel()));
        columns.add(new SelectOption('PartNumber__c', sObjectType.OPP_DeliveryDetails__c.fields.PartNumber__c.getLabel()));
        columns.add(new SelectOption('invoiceDate__c', sObjectType.OPP_DeliveryDetails__c.fields.invoiceDate__c.getLabel()));
        columns.add(new SelectOption('invoiceNumber__c', sObjectType.OPP_DeliveryDetails__c.fields.invoiceNumber__c.getLabel()));   
        columns.add(new SelectOption('TotalQuantityOrdered__c', sObjectType.OPP_DeliveryDetails__c.fields.TotalQuantityOrdered__c.getLabel()));
        columns.add(new SelectOption('ExpressDelivery__c', sObjectType.OPP_DeliveryDetails__c.fields.ExpressDelivery__c.getLabel()));
        columns.add(new SelectOption('DeliveryDate__c',sObjectType.OPP_DeliveryDetails__c.fields.DeliveryDate__c.getLabel()));
        columns.add(new SelectOption('RescheduledDate__c',sObjectType.OPP_DeliveryDetails__c.fields.RescheduledDate__c.getLabel()));         
        columns.add(new SelectOption('RequestedDeliveryDate__c',sObjectType.OPP_DeliveryDetails__c.fields.RequestedDeliveryDate__c.getLabel()));
        columns.add(new SelectOption('ItemHasShipped__c',sObjectType.OPP_DeliveryDetails__c.fields.ItemHasShipped__c.getLabel()));          
        return columns;
    }   
   

   public Utils_DataSource.Result SearchOrder(VCC07_OrderConnectorController OrderController)
   {
        // --- PRE TREATMENT ----
   
        WS06_OrderList.orderStatusListOrderDataBean ListOrder = new WS06_OrderList.orderStatusListOrderDataBean();
        
        /*
        Enum values for Order Status List:
        OrderBy  : DESCENDING,ASCENDING
        OrderStatusColumn : BUSINESS_UNIT,PURCHASER_ID,ORDER_NUMBER,PO_NUMBER,RELEASE_DATE,ORDER_PLACED_THROUGH,MYSE_STATUS,REQUESTED_DELIVERY_DATE,EXPRESS_DELIVERY
        */
        
        ListOrder.column = Label.CL00587; // RELEASE_DATE
        ListOrder.OrderBy = Label.CL00588;  // DESCENDING
        
        List<WS06_OrderList.orderStatusListOrderDataBean> OrderByList = new List<WS06_OrderList.orderStatusListOrderDataBean>();
        OrderByList.add(ListOrder);
        
        WS06_OrderList.orderStatusListSearchDataBean OrderSearchCriteria = new WS06_OrderList.orderStatusListSearchDataBean();
         
        OrderSearchCriteria.bfoAccountId = OrderController.DateCapturer.Account__c; 
        if(OrderController.PartNo != null && OrderController.PartNo != '') 
            OrderSearchCriteria.catalogNumber = OrderController.PartNo.toUpperCase();
            
        if(OrderController.OrderNumber != null && OrderController.OrderNumber != '')    
            OrderSearchCriteria.partialOrderNumber = OrderController.OrderNumber.trim();
            
        if(OrderController.PONumber != null && OrderController.PONumber != '')              
            OrderSearchCriteria.partialPONumber = OrderController.PONumber;
            
        OrderSearchCriteria.rangeEnd = 50; 
        OrderSearchCriteria.rangeStart = 0; 
        
        if(OrderController.DisplayAdvancedCriteria==true)
        {
            OrderSearchCriteria.releaseDateFrom = OrderController.DateCapturer.DateField1__c;
            OrderSearchCriteria.releaseDateTo = OrderController.DateCapturer.DateField2__c;
        }
        
        if(OrderController.BackOffice != Label.CL00501 && OrderController.BackOffice != Label.CL00355)
        {
            OrderSearchCriteria.sdhLegacyPSName = new List<String>();
            OrderSearchCriteria.sdhLegacyPSName.add(OrderController.BackOffice);
        }
        
        OrderSearchCriteria.orderByList = OrderByList;
 
        // ---- WEB SERVICE CALLOUT -----
        WS06_OrderList.OrderStatusListPort OrderConnector = new WS06_OrderList.OrderStatusListPort();
        OrderConnector.endpoint_x = Label.CL00564;
        OrderConnector.ClientCertName_x = Label.CL00616;

        
        WS06_OrderList.orderStatusResult WebServiceResults = new WS06_OrderList.orderStatusResult();
        
        if(test==true) // If test mode is selected, dummy values are returned
            WebServiceResults = Utils_Stubs.OrderStub();  
        else
            WebServiceResults = OrderConnector.orderStatusList(OrderSearchCriteria);
         
        String bFOAccountName = [SELECT Id, Name FROM Account WHERE ID=:OrderSearchCriteria.bfoAccountId].Name; 
                           
        // ---- POST TREATMENT -----
        Utils_DataSource.Result Results = new Utils_DataSource.Result();
        List<OPP_OrderLink__c> OrderLinkList = new List<OPP_OrderLink__c>();
        
        If(WebServiceResults.orderList != null)
        {
            For(WS06_OrderList.orderStatusDataBean  Order_WS:WebServiceResults.orderList)
            {
                OPP_OrderLink__c Order_sObject = new OPP_OrderLink__c();
                
                Order_sObject.CurrencyIsoCode = Order_WS.currencyCode; 
                
                Order_sObject.OrderEnteredBy__c = Order_WS.enteringUserName;
                
                if(Order_WS.expressDelivery==true)
                    Order_sObject.Express__c = Label.CL00583;
                else 
                    Order_sObject.Express__c = Label.CL00584;    
                
                Order_sObject.OrderAccountName__c =  bFOAccountName;    
                Order_sObject.Status__c = Order_WS.mySEStatus;
                Order_sObject.SEOrderReference__c = Order_WS.orderNumber;
                Order_sObject.PONumber__c = Order_WS.poNumber;
                Order_sObject.OrderPlacedThrough__c = Order_WS.orderPlacedThrough;
                Order_sObject.OrderCreationDate__c = Order_WS.releaseDate;
                
                System.debug('#### Order_WS.releaseDate: ' + Order_WS.releaseDate);
                System.debug('#### Order_sObject.OrderCreationDate__c: ' + Order_sObject.OrderCreationDate__c);
                System.debug('#### Order_sObject.OrderCreationDate__c.date(): ' + Order_sObject.OrderCreationDate__c.date());
                
                Order_sObject.OrderDeliveryDate__c = Order_WS.requestedDeliveryDate;
                Order_sObject.ShipTo__c = Order_WS.shipToName;
                Order_sObject.soldTo__c = Order_WS.soldToName;
                Order_sObject.Amount__c = Order_WS.totalAmount;
                Order_sObject.BackOffice__c = Order_WS.legacyName;
                
                OrderLinkList.add(Order_sObject);
            }
        }
        
        Results.returnCode = WebServiceResults.returnInfo.ReturnCode;
        Results.returnMessage = WebServiceResults.returnInfo.ReturnMessage;
        Results.RecordList = OrderLinkList;
        Results.numberOfRecord = WebServiceResults.totalNumberOfPojos;
        
        return Results;                   
   }
   
   public Utils_DataSource.Result SearchOrderDetails(VCC07_OrderConnectorController OrderController)
   {
         // --- PRE TREATMENT ----   
         WS07_OrderDetails.orderDetailSearchDataBean OrderDetailSearchCriteria = new WS07_OrderDetails.orderDetailSearchDataBean();
         OrderDetailSearchCriteria.bfoAccountId = OrderController.DateCapturer.Account__c;
         OrderDetailSearchCriteria.orderNumber = OrderController.SelectedOrder.SEOrderReference__c;
         OrderDetailSearchCriteria.sdhLegacyPSName = OrderController.SelectedOrder.BackOffice__c;
                  
         // ---- WEB SERVICE CALLOUT -----         
         WS07_OrderDetails.OrderStatusDetailPort OrderDetailConnector = new WS07_OrderDetails.OrderStatusDetailPort();
         WS07_OrderDetails.orderStatusDetailResult OrderDetailsResults = new WS07_OrderDetails.orderStatusDetailResult();
         OrderDetailConnector.Endpoint_x = Label.CL00565;
         OrderDetailConnector.ClientCertName_x = Label.CL00616;
        
         if(test==true) // If test mode is selected, dummy values are returned
            OrderDetailsResults = Utils_Stubs.OrderDetailStub();  
         else
            OrderDetailsResults = OrderDetailConnector.orderDetail(OrderDetailSearchCriteria );         
   
         // ---- POST TREATMENT -----         
         Utils_DataSource.Result Results = new Utils_DataSource.Result();
         
         List<OPP_DeliveryDetails__c> OrderDetailsList = new List<OPP_DeliveryDetails__c>();
         

         if(OrderDetailsResults.orderDetail.lines != null)
         {
             For(WS07_OrderDetails.orderStatusLineDataBean  OrderDetails_WS:OrderDetailsResults.orderDetail.lines)
             {
                 OPP_DeliveryDetails__c OrderDetails_sObject = new OPP_DeliveryDetails__c();
             
                 OrderDetails_sObject.PartNumber__c = OrderDetails_WS.catalogNumber;
                 
                 if(OrderDetails_WS.expressDelivery != null)
                 {   
                     if(OrderDetails_WS.expressDelivery)
                         OrderDetails_sObject.expressDelivery__c = Label.CL00583;
                     else
                     OrderDetails_sObject.expressDelivery__c = Label.CL00584; 
                 }    

                 OrderDetails_sObject.ExtendedProfilePrice__c = OrderDetails_WS.extendedProfilePrice;
                 OrderDetails_sObject.invoiceDate__c = OrderDetails_WS.invoiceDate;
                 OrderDetails_sObject.invoiceNumber__c = OrderDetails_WS.invoiceNumber;

                 if(OrderDetails_WS.itemHasShipped!=null)   
                 {                 
                     if(OrderDetails_WS.itemHasShipped)   
                         OrderDetails_sObject.ItemHasShipped__c = Label.CL00583;
                     else
                         OrderDetails_sObject.ItemHasShipped__c = Label.CL00584;     
                 }
                                 
                     
                 OrderDetails_sObject.ItemNumber__c = OrderDetails_WS.itemNumber;
                 OrderDetails_sObject.Status__c = OrderDetails_WS.mySEItemStatus;
                 OrderDetails_sObject.RequestedDeliveryDate__c = OrderDetails_WS.requestedDeliveryDate; 
                 OrderDetails_sObject.RescheduledDate__c = OrderDetails_WS.rescheduleDate;
                 OrderDetails_sObject.TotalQuantityOrdered__c = OrderDetails_WS.totalQuantityOrdered;
                 OrderDetails_sObject.mySEShippingUrl__c = OrderDetails_WS.mySEOrderStatusShippingScheduleUrl;
                 OrderDetails_sObject.mySEShippingUrl__c += '&bfoSessionId=' + UserInfo.getSessionId() + '&bfoServerUrl=' + Label.CL00597 + '&bfoAccountId=' + OrderController.DateCapturer.Account__c + '&sdhLegacyPsName=' + OrderController.SelectedOrder.BackOffice__c;
                 
                 OrderDetailsList.add(OrderDetails_sObject);
             }
         }
        
        // Sort by item number
    
        Boolean Sorted=false;            
        while(!Sorted)
        {
             sorted = true;
             For(integer i=0; i<OrderDetailsList.size()-1; i++)
             {
                 if(OrderDetailsList[i].ItemNumber__c >OrderDetailsList[i+1].ItemNumber__c)
                 {
                     OPP_DeliveryDetails__c Temp = OrderDetailsList[i];
                     OrderDetailsList[i] = OrderDetailsList[i+1];
                     OrderDetailsList[i+1] = temp;
                     sorted = false;
                 }
             }
         }
         
         Results.RecordList = OrderDetailsList;
        
         return Results;    
     }  
}