@isTest
private class ConnectEntityTriggers_Test {
    static testMethod void testConnectEntityTriggers() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('con10');
                System.runAs(runAsUser){
                        Initiatives__c inti=Utils_TestMethods_Connect.createInitiative();
                        insert inti;
                        Project_NCP__c cp=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division;'+Label.Connect_Smart_Cities,GSC_Regions__c='APAC',Power_Region__c='NA',Partner_Region__c='Power',Smart_Cities_Division__c='test');
            insert cp;
            
            //+Ve
            Test.startTest();

            Connect_Entity_Progress__c CEp1=new Connect_Entity_Progress__c(Global_Program__c=cp.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM');
            insert CEp1;
            Connect_Entity_Progress__c CEp2=new Connect_Entity_Progress__c(Global_Program__c=cp.id,Scope__c=Label.Connect_Global_Business,Entity__c='ITB');
            insert CEp2;
            Connect_Entity_Progress__c CEp3=new Connect_Entity_Progress__c(Global_Program__c=cp.id,Scope__c=Label.Connect_Global_Business,Entity__c='Partner-Division',Partner_Region__c='Power');
            insert CEp3;
            Connect_Entity_Progress__c CEp4=new Connect_Entity_Progress__c(Global_Program__c=cp.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GSC-Regional',GSC_Region__c='APAC');
            insert CEp4;
            Connect_Entity_Progress__c CEp13=new Connect_Entity_Progress__c(Global_Program__c=cp.id,Scope__c=Label.Connect_Global_Business,Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test');
            insert CEp13;

            //-Ve

            Connect_Entity_Progress__c CEp5=new Connect_Entity_Progress__c(Global_Program__c=cp.id,Scope__c=Label.Connect_Global_Business,Entity__c='ITB');
             try{
            insert CEp5;
            }catch(DmlException e){
            e.getMessage();
            }
            Connect_Entity_Progress__c CEp6=new Connect_Entity_Progress__c(Global_Program__c=cp.id,Scope__c=Label.Connect_Global_Business,Entity__c='Partner-Division',Partner_Region__c='Power');
            try{
            insert CEp6;
            }catch(DmlException m){
            m.getMessage();
            }
            Connect_Entity_Progress__c CEp7=new Connect_Entity_Progress__c(Global_Program__c=cp.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GSC-Regional',GSC_Region__c='APAC');
            try{           
            insert CEp7;            
            }catch(DmlException l){
            l.getMessage();
            }

            CEp1.Entity__c='test';
            try{
            update CEp1;
            }catch(DmlException d)
            {
            d.getMessage();
            }
            CEp3.Partner_Region__c='test';
            try{            
            update CEp3;
            }catch(DmlException c)
            {
            c.getMessage();
            }
            CEp4.GSC_Region__c='test';

            try{
            update CEp4;
            }catch(DmlException p)
            {
            p.getMessage();
            }                      
            CEp2.Entity__c='test';
            try{                
            update CEp2;
            }catch(DmlException x)
            {
            x.getMessage();
            }

            Test.stopTest();
            
            
            Project_NCP__c cp1=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp1',Global_Functions__c=null,Global_Business__c=null,Power_Region__c=null);
            insert cp1;
            Connect_Entity_Progress__c CEp8=new Connect_Entity_Progress__c(Global_Program__c=cp1.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM');
            try{
            insert CEp8;
            }catch(DmlException dm)
            {
            dm.getMessage();
            }
            Connect_Entity_Progress__c CEp9=new Connect_Entity_Progress__c(Global_Program__c=cp1.id,Scope__c=Label.Connect_Global_Business,Entity__c='ITB');
            try{
            insert CEp9;
            }catch(DmlException ex)
            {
            ex.getMessage();
            }
            Connect_Entity_Progress__c CEp10=new Connect_Entity_Progress__c(Global_Program__c=cp1.id,Scope__c=Label.Connect_Power_Region,Entity__c='test');
            try{
            insert CEp10;
            }catch(DmlException ep)
            {
            ep.getMessage();
            }
            

            }
        }
}