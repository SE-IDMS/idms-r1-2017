@isTest
private class VFC_NewTEX_TEST
{
    static testMethod void testNewTEX() 
    {
        test.startTest();
        Country__c accountCountry = new Country__c();
        accountCountry.CountryCode__c = 'ZZ';
        insert accountCountry;
        
        StateProvince__c accountStateProvince = new StateProvince__c();
        accountStateProvince.Country__c = accountCountry.ID;
        accountStateProvince.CountryCode__c = 'ZZ';
        accountStateProvince.StateProvinceCode__c = 'WW';
        insert accountStateProvince;
        
        Account account1 = Utils_TestMethods.createAccount();
        account1.StateProvince__c = accountStateProvince.ID;
        account1.Country__c = accountCountry.ID;
        account1.AdditionalAddress__c = 'AA';
        account1.City__c = 'CITY';
        account1.ZipCode__c = '12345';
        account1.County__c = 'DD';
        account1.POBoxZip__c = '678';
        account1.StreetLocalLang__c = 'LOCAL STREET';
        account1.LocalAdditionalAddress__c = 'RR';
        account1.LocalCity__c = 'LOCAL CITY';
        account1.LocalCounty__c = 'LOCAL COUNTY';
        account1.Type='GSA';

        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;
        
        List<Case> CaseList = new List<Case>();
        
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');        
        case1.SupportCategory__c = '4 - Post-Sales Tech Support';
        case1.Symptom__c =  'Installation/ Setup';
        case1.SubSymptom__c = 'Hardware';    
        case1.Quantity__c = 4;
        case1.Family__c = 'ADVANCED PANEL';
        case1.CommercialReference__c='xbtg5230';
        case1.ProductFamily__c='HMI SCHNEIDER BRAND';
        case1.TECH_GMRCode__c='02BF6DRG16NBX07632';
        CaseList.add(case1); 
        
        Case case2 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');        
        case2.SupportCategory__c = '4 - Post-Sales Tech Support';
        case2.Symptom__c =  'Installation/ Setup';
        case2.SubSymptom__c = 'Hardware';    
        case2.Quantity__c = 4;
        case2.Family__c = 'ADVANCED PANEL';
        case2.CommercialReference__c='xbtg5230';
        case2.ProductFamily__c='HMI SCHNEIDER BRAND';
        case2.TECH_GMRCode__c='02BF6DRG16NBX07632';
        CaseList.add(case2); 
        
        Case case3 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');        
        case3.SupportCategory__c = '4 - Post-Sales Tech Support';
        case3.Symptom__c =  'Installation/ Setup';
        case3.SubSymptom__c = 'Hardware';    
        case3.Quantity__c = 4;
        case3.Family__c = 'ADVANCED PANEL';
        case3.ProductFamily__c='HMI SCHNEIDER BRAND';
        case3.TECH_GMRCode__c='02BF6DRG16NBX07632';
        case3.Status = Label.CLSEP14I2P03;
        case3.AnswerToCustomer__c = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        case3.CustomerRequest__c = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        CaseList.add(case3);
        
         Case case4 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');        
        case4.SupportCategory__c = '4 - Post-Sales Tech Support';
        case4.Symptom__c =  'Installation/ Setup';
        case4.SubSymptom__c = 'Hardware';    
        case4.Family__c = 'ADVANCED PANEL';
        case4.ProductFamily__c='HMI SCHNEIDER BRAND';
        case4.TECH_GMRCode__c='02BF6DRG16NBX07632';
        case4.Status = Label.CLSEP14I2P03;
        case4.AnswerToCustomer__c = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        case4.CustomerRequest__c = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        CaseList.add(case4);
        
        INSERT CaseList;
       
        User TestUser = Utils_TestMethods.createStandardUser('RRuser');
        Insert TestUser ;
        
        BusinessRiskEscalationEntity__c AccOrg= new BusinessRiskEscalationEntity__c(Name='RC TEST', Entity__c='RC Entity-1', ProblemToPreventionDeployed__c = True, Street__c = 'RC Street', RCPOBox__c = '123456', RCCountry__c = accountCountry.Id, RCCity__c = 'RC City', RCZipCode__c = '500005');
     Insert AccOrg; 
         BusinessRiskEscalationEntity__c AccOrg2= new BusinessRiskEscalationEntity__c(Name='RC TEST', Entity__c='RC Entity-1', SubEntity__c='RC Sub-Entity',ProblemToPreventionDeployed__c = True, Street__c = 'RC Street', RCPOBox__c = '123456', RCCountry__c = accountCountry.Id, RCCity__c = 'RC City', RCZipCode__c = '500005');
     Insert AccOrg2;  
     BusinessRiskEscalationEntity__c AccOrg3= new BusinessRiskEscalationEntity__c(Name='RC TEST', Entity__c='RC Entity-1', SubEntity__c='RC Sub-Entity', Location__c='RC Location', ProblemToPreventionDeployed__c = True, Street__c = 'RC Street', RCPOBox__c = '123456', RCCountry__c = accountCountry.Id, RCCity__c = 'RC City', RCZipCode__c = '500005');
     Insert AccOrg3; 
        CustomerSafetyIssue__c CSI = new CustomerSafetyIssue__c(     
                                             RelatedOrganization__c=AccOrg.id,  
                                             AffectedCustomer__c = account1.id,              
                                             ReportedDateofOccurrence__c = system.today(),
                                             AddressOfOccurrence__c = 'Test Address',
                                             CountryOfOccurrence__c = accountCountry.id,
                                             TypeofSite__c = 'Industrial facility',
                                             BodilyInjury__c ='Risk Reported',
                                             LeaderEmailAddress__c = 'test@schneider-electric.com',
                                             LeaderPhoneNumber__c = '1111111111',
                                             //PropertyDamage__c = 'Risk Reported',
                                             CurrencyIsoCode = 'INR',
                                             TypeOfInjury__c = 'Back injury',
                                             CauseOfInjury__c='Contact with harmful material/substance',
                                             InjuredPartyAffiliation__c = 'Customer',
                                             //TypeOfPropertyDamage__c = 'Fire damage to building',
                                             IssueInvestigationLeader__c = TestUser.id,
                                             RelatedbFOCase__c = CaseList[0].id
                                             );
        Insert CSI;
        
        TEX__c newTEX = new TEX__c();
        newTEX.case__c = CaseList[0].ID;
        
        TEX__c newTEX1 = new TEX__c();
        newTEX1.case__c = null;
        
        TEX__c newTEX2 = new TEX__c();
        newTEX2.case__c = CaseList[2].ID;
        
        TEX__c newTEX3 = new TEX__c();
        newTEX3.case__c = CaseList[3].ID;
        
        test.stoptest();
        PageReference ref = new PageReference('/apex/VFC_NewTEX?returl=I'+CaseList[0].ID);
        Test.setCurrentPage(ref); 
        
        ApexPages.StandardController TEXStandardController = new ApexPages.StandardController(newTEX);
        VFC_NewTEX TEXController = new VFC_NewTEX(TEXStandardController );
        TEXController.relatedCase = New Case();
        TEXController.doCancel();
        TEXController.goToEditPage();
        TEXController.doCancel();
        
        PageReference ref2 = new PageReference('/apex/VFC_NewTEX?returl=I'+CaseList[1].ID);
        Test.setCurrentPage(ref2); 
        
        ApexPages.StandardController TEXStandardController2 = new ApexPages.StandardController(newTEX);
        VFC_NewTEX TEXController2 = new VFC_NewTEX(TEXStandardController2);
        TEXController2.goToEditPage();
        TEXController2.doCancel();
        
      
        PageReference ref1 = new PageReference('/apex/VFC_NewTEX?returl=I');
        Test.setCurrentPage(ref1); 
        
        ApexPages.StandardController TEXStandardController1 = new ApexPages.StandardController(newTEX);
        VFC_NewTEX TEXController1 = new VFC_NewTEX(TEXStandardController1);
        TEXController1.goToEditPage();
        TEXController1.doCancel();
        
        PageReference ref3 = new PageReference('/apex/VFC_NewTEX?returl=I'+null);
        Test.setCurrentPage(ref3); 
        
        ApexPages.StandardController TEXStandardController3 = new ApexPages.StandardController(newTEX1);
        VFC_NewTEX TEXController3 = new VFC_NewTEX(TEXStandardController3);
        TEXController3.goToEditPage();
        TEXController3.doCancel();
        
        PageReference ref4 = new PageReference('/apex/VFC_NewTEX?returl=I'+CaseList[2].ID);
        Test.setCurrentPage(ref4); 
        
        ApexPages.StandardController TEXStandardController4 = new ApexPages.StandardController(newTEX2);
        VFC_NewTEX TEXController4 = new VFC_NewTEX(TEXStandardController4);
        TEXController4.goToEditPage();
        TEXController4.doCancel();
        
        PageReference ref5 = new PageReference('/apex/VFC_NewTEX?returl=I'+CaseList[3].ID);
        Test.setCurrentPage(ref5); 
        
        ApexPages.StandardController TEXStandardController5 = new ApexPages.StandardController(newTEX3);
        VFC_NewTEX TEXController5 = new VFC_NewTEX(TEXStandardController5);
        TEXController5.goToEditPage();
        TEXController5.doCancel();
        
        }
    static testMethod void myUnitTest2() 
    {
        test.startTest();
        Country__c accountCountry = new Country__c();
        accountCountry.CountryCode__c = 'ZZ';
        insert accountCountry;
        
        StateProvince__c accountStateProvince = new StateProvince__c();
        accountStateProvince.Country__c = accountCountry.ID;
        accountStateProvince.CountryCode__c = 'ZZ';
        accountStateProvince.StateProvinceCode__c = 'WW';
        insert accountStateProvince;
        
        Account account1 = Utils_TestMethods.createAccount();
        account1.StateProvince__c = accountStateProvince.ID;
        account1.Country__c = accountCountry.ID;
        account1.AdditionalAddress__c = 'AA';
        account1.City__c = 'CITY';
        account1.ZipCode__c = '12345';
        account1.County__c = 'DD';
        account1.POBoxZip__c = '678';
        account1.StreetLocalLang__c = 'LOCAL STREET';
        account1.LocalAdditionalAddress__c = 'RR';
        account1.LocalCity__c = 'LOCAL CITY';
        account1.LocalCounty__c = 'LOCAL COUNTY';
        account1.Type='GSA';

        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact1');
        insert contact1;
        
        List<Case> CaseList = new List<Case>();
        
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');        
        case1.SupportCategory__c = '4 - Post-Sales Tech Support';
        case1.Symptom__c =  'Installation/ Setup';
        case1.SubSymptom__c = 'Hardware';    
       // case1.Quantity__c = 4;
        case1.Family__c = 'ADVANCED PANEL';
        case1.CommercialReference__c='xbtg5230';
        case1.ProductFamily__c='HMI SCHNEIDER BRAND';
        case1.TECH_GMRCode__c='02BF6DRG1234';
        CaseList.add(case1); 
        
        INSERT CaseList;
       
        User TestUser = Utils_TestMethods.createStandardUser('RRuserb7');
        Insert TestUser ;
        
        BusinessRiskEscalationEntity__c AccOrg= new BusinessRiskEscalationEntity__c(Name='RC TEST', Entity__c='RC Entity-1', ProblemToPreventionDeployed__c = True, Street__c = 'RC Street', RCPOBox__c = '123456', RCCountry__c = accountCountry.Id, RCCity__c = 'RC City', RCZipCode__c = '500005');
     Insert AccOrg; 
         BusinessRiskEscalationEntity__c AccOrg2= new BusinessRiskEscalationEntity__c(Name='RC TEST', Entity__c='RC Entity-1', SubEntity__c='RC Sub-Entity',ProblemToPreventionDeployed__c = True, Street__c = 'RC Street', RCPOBox__c = '123456', RCCountry__c = accountCountry.Id, RCCity__c = 'RC City', RCZipCode__c = '500005');
     Insert AccOrg2;  
     BusinessRiskEscalationEntity__c AccOrg3= new BusinessRiskEscalationEntity__c(Name='RC TEST', Entity__c='RC Entity-1', SubEntity__c='RC Sub-Entity', Location__c='RC Location', ProblemToPreventionDeployed__c = True, Street__c = 'RC Street', RCPOBox__c = '123456', RCCountry__c = accountCountry.Id, RCCity__c = 'RC City', RCZipCode__c = '500005');
     Insert AccOrg3; 
        CustomerSafetyIssue__c CSI = new CustomerSafetyIssue__c(     
                                             RelatedOrganization__c=AccOrg3.id,  
                                             AffectedCustomer__c = account1.id,              
                                             ReportedDateofOccurrence__c = system.today(),
                                             AddressOfOccurrence__c = 'Test Address',
                                             CountryOfOccurrence__c = accountCountry.id,
                                             TypeofSite__c = 'Industrial facility',
                                             BodilyInjury__c ='Risk Reported',
                                             LeaderEmailAddress__c = 'test@schneider-electric.com',
                                             LeaderPhoneNumber__c = '1111111111',
                                             //PropertyDamage__c = 'Risk Reported',
                                             CurrencyIsoCode = 'INR',
                                             TypeOfInjury__c = 'Back injury',
                                             CauseOfInjury__c='Contact with harmful material/substance',
                                             InjuredPartyAffiliation__c = 'Customer',
                                             //TypeOfPropertyDamage__c = 'Fire damage to building',
                                             //IssueInvestigationLeader__c = TestUser.id,
                                             RelatedbFOCase__c = CaseList[0].id
                                             );
        Insert CSI;
     
        TEX__c temptech=AP_TechnicalExpertAssessment_Test.createTEX(CaseList[0].ID,TestUser.id);
        temptech.Material_Status__c='Delivered to Return Center';
        temptech.ShippedFromCustomer__c = null;
        temptech.ExpertAssessmentStarted__c = system.today();
        temptech.case__c = CaseList[0].ID;
        insert temptech;  
        
        //PageReference ref = new PageReference('/apex/VFC_NewTEX?returl=I'+CaseList[0].ID);
        PageReference pageRef = Page.VFP_NewTEX;

       Test.setCurrentPage(pageRef);

       pageRef.getParameters().put(Label.CL00330, CaseList[0].ID);
       Test.setCurrentPage(pageRef); 
        system.debug('TEST-------->'+CaseList[0]);
        ApexPages.StandardController TEXStandardController = new ApexPages.StandardController(temptech);
        VFC_NewTEX TEXController = new VFC_NewTEX(TEXStandardController );
        TEXController.initializeTEX(temptech);
        TEXController.goToEditPage();
        TEXController.doCancel();
        
        test.stoptest();
    }
}