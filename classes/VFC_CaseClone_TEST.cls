@isTest
public class VFC_CaseClone_TEST {
static testMethod void testcaseclone() {
    Contact con = new Contact();
        Country__c country = Utils_TestMethods.createCountry();
        country.InternationalPhoneCode__c='1340';
        insert country;        
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(country.Id);
        insert stateProv;
        Account a = new Account();
        a.Name = 'TEST VFC_ContactSearch';
        a.AccountLocalName__c = 'Account Local Name';
        a.Street__c = 'Street';
        a.ZipCode__c = 'zipcode';
        a.City__c = 'city';
        a.StateProvince__c = stateProv.Id;
        a.Country__c = country.Id;
        a.POBox__c = 'pobox';
        a.POBoxZip__c = 'poboxzip';
        a.StreetLocalLang__c = 'streetlocal';
        a.ToBeDeleted__c = false;
        a.CorporateHeadquarters__c = true;
        a.LeadingBusiness__c = 'BD';
        a.MarketSegment__c = 'BDZ';        
        insert a;
        
        con.FirstName='conFirstName';
        con.LastName='conLastName';
        con.LocalFirstName__c='conLocalFirstName';
        con.LocalLastName__c='conLocalLastName';
        con.Street__c='Street';
        con.StreetLocalLang__c='StreetLocalLang';
        con.Country__c = country.Id;
        con.ZipCode__c='ZipCode';
        con.City__c='City';
        con.LocalCity__c='LocalCity';
        con.StateProv__c=stateProv.Id;
        con.POBox__c='POBox';
        con.email='abc@gmail.com';
        con.WorkPhone__c='+91123456';
        con.WorkPhoneExt__c='123456';
        con.MobilePhone='123456';
        con.accountid=a.id;
        insert con;

        Case CaseObj= Utils_TestMethods.createCase(a.Id,Con.Id,'Open');
        CaseObj.SupportCategory__c = '4 - Post-Sales Tech Support';
        CaseObj.Symptom__c =  'Installation/ Setup';
        CaseObj.SubSymptom__c = 'Hardware';
        CaseObj.OtherSymptom__c = 'Other';    
        CaseObj.Quantity__c = 4;
        CaseObj.Family__c = 'ADVANCED PANEL';
        CaseObj.CommercialReference__c='xbtg5230';
        CaseObj.CaseSymptom__c='test CaseSymptom';
        CaseObj.CaseSubSymptom__c='test CaseSubSymptom';
        CaseObj.CaseOtherSymptom__c='test CaseOtherSymptom';
        CaseObj.ProductDescription__c='test description';
        Insert CaseObj;
        
        ApexPages.currentPage().getParameters().put('CaseId',CaseObj.id);
        ApexPages.currentPage().getParameters().put('RelatedCaseNumber',CaseObj.CaseNumber);
        ApexPages.currentPage().getParameters().put('isdtp','isdtp');
        VFC_CaseClone vfCtrl = new VFC_CaseClone(new ApexPages.StandardController(CaseObj));
        vfCtrl.CaseCloneLink();
       }
  }