@isTest
private class AP_AccPlanConfidential_RecordCount_TEST 
{
    private static Account acc;
    private static Opportunity opp;
    
    static
    {
        // create test data
        acc = Utils_TestMethods.createAccount();
        insert acc;
        Country__c ct = Utils_TestMethods.createCountry();
        ct.name = 'France';
        insert ct;
        opp = Utils_TestMethods.createOpportunity(acc.Id);
        insert opp;
    }
    static testMethod void testAccPlanConfInsert()  
    {
        SFE_AccPlan__c objAccPlan=new SFE_AccPlan__c(name='ip account plan',Account__c=acc.id,AccountTurnover__c=100);
        insert objAccPlan;
        AccountPlanConfidential__c objAPC=new AccountPlanConfidential__c(AccountPlan__c=objAccPlan.id,Strengths__c='strength');
        insert objAPC;   
    }
    static testMethod void testAccPlanConfInsert1()  
    {
        SFE_AccPlan__c objAccPlan1=new SFE_AccPlan__c(name='ip account plan',Account__c=acc.id,AccountTurnover__c=100);
        insert objAccPlan1;
        AccountPlanConfidential__c objAPC1=new AccountPlanConfidential__c(AccountPlan__c=objAccPlan1.id,Strengths__c='strength');
        insert objAPC1;   
        AccountPlanConfidential__c objAPC11=new AccountPlanConfidential__c(AccountPlan__c=objAccPlan1.id,Strengths__c='strength');
        try
        {
            insert objAPC11;
        }
        catch(Exception e) 
        {
            system.debug('....Error...'+e);
        }
    }
    
    
}