public class VFC_SupportNotificationEmail {
    public Id Country {get;set;}
    public String email{
    get{
        if(Country != null){
            List<PRMCountry__c> pcountry = new List<PRMCountry__c>([SELECT Id, CountrySupportEmail__c FROM PRMCountry__c WHERE (MemberCountry1__c = :Country OR MemberCountry2__c = :Country OR
                                  MemberCountry3__c = :Country OR MemberCountry4__c = :Country OR MemberCountry5__c = :Country OR     
                                  Country__c = :Country) AND CountrySupportEmail__c != null]);
            if(!pcountry.isEmpty()) email = pcountry[0].CountrySupportEmail__c;
            else email = System.label.CLOCT15PRM024;    
        }
        return email;
    }
    set;}
}