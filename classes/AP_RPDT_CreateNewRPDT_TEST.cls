@isTest
private class AP_RPDT_CreateNewRPDT_TEST {
static testMethod void myUnitTest() {

        Country__c CountryObj = Utils_TestMethods.createCountry();
    Insert CountryObj;

      BusinessRiskEscalationEntity__c RCorg = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST',
                                            Entity__c='RC Entity-1',
                                            SubEntity__c='RC Sub-Entity',
                                            Location__c='RC Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg;  
    
    BusinessRiskEscalationEntity__c RCorg1 = new BusinessRiskEscalationEntity__c(
                                            Name='RC TEST2',
                                            Entity__c='RC Entity-2',
                                            SubEntity__c='RC Sub-Entity2',
                                            Location__c='RC Location2',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'RC Street2',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObj.Id,
                                            RCCity__c = 'RC City2',
                                            RCZipCode__c = '500005'
                                            );
    Insert RCorg1;  
    
  
    RMAShippingToAdmin__c RPDT = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Repair defective product',
                                 CommercialReference__c='xbtg5230',
                                 TECH_ProductFamily__c='HMI SCHNEIDER BRAND',
                                 ReturnCenter__c = RCorg.Id,  
                                 Ruleactive__c = True,
                                 TECH_GMRCode__c='02BF6D'
                                 );
    Insert RPDT;
    
    
    RMAShippingToAdmin__c RPDT1 = new RMAShippingToAdmin__c (
                                 Country__c = CountryObj.Id,
                                 Capability__c = 'Receive defective product',                                
                                 Ruleactive__c = True,
                                 CommercialReference__c='xbtg5230',
                                 TECH_ProductFamily__c='HMI SCHNEIDER BRAND',
                                 ReturnCenter__c = RCorg.Id,
                                 TECH_GMRCode__c='02BF6D'
                                 );
    Insert RPDT1;
   
   RPDT1.CommercialReference__c='xbtg5230';
   RPDT1.TECH_GMRCode__c='02BF6D';
   RPDT1.TECH_ProductFamily__c='HMI SCHNEIDER BRAND';
   Update RPDT1;
   
}
}