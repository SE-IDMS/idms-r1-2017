/*
Created By: Akram GARGOURI
Release: PRM: Inactivate inactive Fielo Member
*/
global class Batch_PRMMemberAdoption implements Database.Batchable<sObject>,Schedulable  {
    
    public String query;
    public Integer batchSize = 200;
    
    global Batch_PRMMemberAdoption() {
        
    }

    global Batch_PRMMemberAdoption(Integer batchSize) {
        this.batchSize = batchSize;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
    
        if(query== null){
            DateTime dt = DateTime.now();
            query = 'Select id from FieloEE__Member__c where F_PRM_DesactivateDate__c < :dt AND F_PRM_IsActivePRMContact__c = TRUE'; 
        }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<FieloEE__Member__c> scope) {
        if(scope != null && scope.size() >0){
            List<FieloEE__Member__c> membersList = new List<FieloEE__Member__c>();
            for(FieloEE__Member__c mem : scope){
                mem.F_PRM_IsActivePRMContact__c = false;
                membersList.add(mem);
            }
            update membersList;
        }else{
            //Nothing
        }
    } 
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
    //Schedule method
    global void execute(SchedulableContext sc) 
    {
        Batch_PRMMemberAdoption batchMemAdoption = new Batch_PRMMemberAdoption(); 
        Database.executebatch(batchMemAdoption,batchSize);
    }

}