public class VFC_ManageAccountExternalFacet {

    public Boolean IsError {get;set;}
    public Boolean IsSucess {get;set;}
    public Account Acc {get;set;} 
    public Account OrgAcc;
    public List<String> accFields;
    public Boolean clickEdit {get;set;}
    public List<SelectOption> ContactsSelectList {get; set;}
    public Id NewPrimaryContactId {get;set;}
    public Boolean PrimaryChangeIsSucess {get;set;}
    public Account detailAccount {get;set;}
    public List<SelectOption> PRMReasonForDecline {get;set;}
    public String NewStatus {get;set;} 
    public Boolean isEdit{get;set;}
    public String retUrl{get;set;}
    
    
    public List<Contact> accContactList;
    public Contact PrimaryContact;
    private String[] accountFields = new String[] {'Name','Street__c','AdditionalAddress__c','ClassLevel1__c','ClassLevel2__c','City__c','Phone','Country__c','StateProvince__c','ZipCode__c','CurrencyIsoCode',
                'CorporateHeadquarters__c','MarketServed__c','AccountNumber','AnnualSales__c','EmployeeSize__c','VATNumber__c','SEAccountID__c',
                'PRMCompanyName__c','PRMAccount__c','PRMCompanyPhone__c','PRMCorporateHeadquarters__c','PRMStreet__c',
                'PRMIncludeCompanyInfoInSearchResults__c','PRMAdditionalAddress__c','PRMBusinessType__c','PRMCity__c','PRMAreaOfFocus__c',          
                'PRMCountry__c','PRMMarketServed__c','PRMStateProvince__c','PRMTaxId__c','PRMZipCode__c','PRMEmployeeSize__c','PRMSEAccountNumber2__c',
                'PRMCurrencyIsoCode__c','PRMPreferredDistributor1__c','PRMAccountRegistrationStatus__c','PRMPreferredDistributor2__c',
                'PRMReasonForDecline__c','PRMPreferredDistributor3__c','PRMAdditionalComments__c','PRMPreferredDistributor4__c',
                'PRMRegistrationActivationDate__c','PRMUIMSID__c','PRMOrigin__c','PRMOriginSource__c','PRMOriginSourceInfo__c','PRMUIMSSEAccountId__c','PRMTnC__c',
                'PRMIsActiveAccount__c','PRMIsEnrolledAccount__c','PRMAccountCompletedProfile__c','PRMExcludeFromReports__c','PRMAnnualSales__c','PRMRegistrationReviewedRejectedDate__c',
                'PLCompanyName__c','PLMainContactFirstName__c','PLMainContactLastName__c','PLMainContactPhone__c','PLMainContactEmail__c','PLWebsite__c','PLCompanyDescription__c',
                'PLStreet__c','PLAdditionalAddress__c','PLStateProvince__c','PLCity__c','PLCountry__c','PLZipCode__c','PLShowInPartnerLocator__c','PLBusinessType__c',
                'PLAreaOfFocus__c','PLMarketServed__c','PLPartnerLocatorFeatureGranted__c','PLShowInPartnerLocatorForced__c','PRMDomainsOfExpertise__c'
                };


    public VFC_ManageAccountExternalFacet(ApexPages.StandardController stdController) {
        
        String qStrng = ApexPages.currentPage().getParameters().get('isEdit');
        System.debug('** isEdit **'+isEdit);
        retUrl = ApexPages.currentPage().getParameters().get('retUrl');
        System.debug('** retUrl **'+retUrl);
        
        if(String.isNotBlank(qStrng) && qStrng != Null){
            clickEdit = True;
            isEdit =True;
        }
        System.debug('** Param **'+ApexPages.currentPage().getParameters().get('isEdit'));
        
        accFields = new List<String> (accountFields);
        NewPrimaryContactId = null;
        if(!test.isRunningTest()){
            stdController.addFields(accFields);
        }
        Acc = (Account)stdController.getRecord();
        
        System.debug('** Account **'+Acc);
        
        if(qStrng != Null && isEdit) {
            Acc.PRMOrigin__c = 'Internal Registration';
            Acc.PRMAccount__c = True;
            
            List<CS_PRM_AccountFieldsMapping__c> accFieldsMappingLst = CS_PRM_AccountFieldsMapping__c.getall().values();
            
            for (CS_PRM_AccountFieldsMapping__c accFM : accFieldsMappingLst){
            
                System.debug('**Field Name && Value **'+accFM.Name+' '+Acc.get(accFM.Name));
                if(Acc.get(accFM.Name) == Null){
                    
                    if(accFM.AccountSourceField__c == 'AnnualSales__c'){
                        double tAnnualSales = double.valueOf(Acc.get(accFM.AccountSourceField__c));
                        Schema.DescribeFieldResult fieldResult1 = Account.PRMAnnualSales__c.getDescribe();
                        List<Schema.PicklistEntry> plentry = fieldResult1.getPicklistValues();
                        String maxvalue;
                        for( Schema.PicklistEntry f : plentry){
                            maxvalue = f.getValue();
                            if(tAnnualSales <= double.valueOf(maxvalue)){
                                Acc.put(accFM.Name,f.getValue());
                                break;
                            }   
                        } 

                        if(tAnnualSales > double.valueOf(maxvalue) )
                            Acc.put(accFM.Name,maxvalue);    
                    }
                    else {
                        System.debug('**Source Field Name && Value **'+accFM.AccountSourceField__c+' '+Acc.get(accFM.AccountSourceField__c));
                        Acc.put(accFM.Name, Acc.get(accFM.AccountSourceField__c));
                    }
                
                }
                
                System.debug('Account ::'+Acc);
            }
        }
        
        
        detailAccount = Acc;
        IsError = False;
        IsSucess = False;
        
        accContactList = [SELECT Id,PRMUIMSID__c,AccountID,PRMPrimaryContact__c,PRMFirstName__c,PRMLastName__c,PRMEmail__c,PRMContact__c,
                          PRMIsActiveContact__c FROM Contact WHERE AccountId = :Acc.Id AND PRMContactRegistrationStatus__c = 'Validated' 
                           ORDER BY PRMFirstName__c ASC];
        ContactsSelectList = New List<SelectOption>();

        for(Contact con:accContactList){
            if(con.PRMPrimaryContact__c){
                PrimaryContact = con;
                NewPrimaryContactId = con.Id;
            }
            ContactsSelectList.add(new SelectOption(con.Id,con.PRMFirstName__c+' '+con.PRMLastName__c));
        }
        PRMReasonForDecline = New List<SelectOption>();
        PRMReasonForDecline.add(new SelectOption('','--None--'));
        Schema.DescribeFieldResult dfr = Account.PRMReasonForDecline__c.getDescribe();
        for(Schema.PicklistEntry pLEntry: dfr.getPicklistValues()){
            if(pLEntry.isActive())
            PRMReasonForDecline.add(new SelectOption(pLEntry.getValue(), pLEntry.getLabel()));
        }
    }
    
    public void primaryConChangeReg(){
        Boolean isUpdatedinUIMS;
        System.debug('NewPrimaryContactId:' + NewPrimaryContactId);
        System.debug('PrimaryContact:' + PrimaryContact);
        try{
            list<Contact> newPrimaryContact = ([SELECT Id,PRMPrimaryContact__c,PRMUIMSID__c FROM Contact WHERE Id = :NewPrimaryContactId]);
            contact toPrimaryCon = New Contact();
            List<Contact> toUpdateList = new List<Contact>();
            if(NewPrimaryContactId != PrimaryContact.Id){
                toPrimaryCon  = newPrimaryContact [0];
                toPrimaryCon.PRMPrimaryContact__c = true;
                PrimaryContact.PRMPrimaryContact__c = false;
                toUpdateList.add(toPrimaryCon);
                toUpdateList.add(PrimaryContact);
                
                if(!toUpdateList.isEmpty()){
                    system.debug('**** the size is ***'+toUpdateList.Size());
                    isUpdatedinUIMS = AP_PRMUtils.UpdatePrimaryContact_Admin(PrimaryContact,toPrimaryCon);
                    if(isUpdatedinUIMS){
                        update toUpdateList;
                        PrimaryContact = toPrimaryCon;
                        NewPrimaryContactId = toPrimaryCon.Id;
                        PrimaryChangeIsSucess = true;
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Primary Contact for this Account changed Sucessfully'));
                    }
                }
            }
            else
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select a Non Primary Contact'));
        }
        catch(exception e){
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,e.getMessage()));
        }
    }
    public PageReference UpdateAccountInfoInUIMS(){
        try {
            if(isAccountChanged()){
                if(Acc.PRMAccountRegistrationStatus__c == 'Declined' && String.isBlank(acc.PRMReasonForDecline__c))
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Reason for Declining Registration is Mandatory for this status'));
                if(acc.PRMReasonForDecline__c == 'Other' && (String.isBlank(acc.PRMAdditionalComments__c) || acc.PRMAdditionalComments__c == null))
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Additional Comments is Mandatory for this Reason for Declining Registration'));
                
                if(ApexPages.getMessages().Size() == 0){
                    //Do the UIMS Update once the class is created.
                    AP_UserRegModel upUIMSCompany = New AP_UserRegModel();
                    upUIMSCompany.accountId = Acc.Id;
                    upUIMSCompany.companyFederatedId = Acc.PRMUIMSID__c;
                    upUIMSCompany.companyName = Acc.PRMCompanyName__c;
                    upUIMSCompany.businessType = Acc.PRMBusinessType__c;
                    upUIMSCompany.areaOfFocus = Acc.PRMAreaOfFocus__c;
                    upUIMSCompany.marketServed = Acc.PRMMarketServed__c;
                    upUIMSCompany.employeeSize = Acc.PRMEmployeeSize__c;
                    upUIMSCompany.companyAddress1 = Acc.PRMStreet__c;
                    upUIMSCompany.companyCity = Acc.PRMCity__c;
                    upUIMSCompany.companyZipcode = Acc.PRMZipCode__c;
                    upUIMSCompany.companyCurrencyCode = Acc.PRMCurrencyIsoCode__c;
                    upUIMSCompany.includeCompanyInfoInPublicSearch = Acc.PRMIncludeCompanyInfoInSearchResults__c;
                    upUIMSCompany.companyCountry = Acc.PRMCountry__c;
                    upUIMSCompany.companyStateProvince = Acc.PRMStateProvince__c;
                    
                    upUIMSCompany.companyPhone   = Acc.PRMCompanyPhone__c;
                    upUIMSCompany.companyWebsite = Acc.PRMWebsite__c;
                    upUIMSCompany.taxId          = Acc.PRMTaxId__c;
                    upUIMSCompany.annualSales    = Acc.PRMAnnualSales__c;
                    upUIMSCompany.companyHeaderQuarters    =Acc.PRMCorporateHeadquarters__c;
                    upUIMSCompany.companyAddress2 = Acc.PRMAdditionalAddress__c;
                    
                    
                    Boolean accountUpdated = AP_PRMUtils.UpdateAccountInUIMS_Admin(upUIMSCompany);
                    
                    //if(accountUpdated){
                        update acc;
                        
                        if(isEdit != Null && isEdit){
                            system.debug('* retUrl *'+retUrl);
                            return new PageReference('/apex/VFP_NewPRMRegistration?sfdc.tabName=01r12000000KAPd&contId='+retUrl);
                        
                        }    
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, System.Label.CLJUL15PRM005)); //Your changes were saved successfully
                        isSucess = True;
                        clickEdit = false;
                    //}
                    //else
                    //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.CLJUL15PRM006)); //Unable to save changes, error received while updating information in UIMS
                }
                return null;
            }
            else{
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.CLJUL15PRM007)); //No changes were detected, save once the changes were made to the data
                return null;
            }    
        }
        catch(Exception ex){
            System.debug(' ****There is an exception on updating the Account info in UIMS****' +ex.getMessage() + ex.getStackTraceString());
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));
            return null;
        }
    }
    public pageReference editPage() {
        clickEdit = true;
        return null;
    }
    public pageReference detailPage() {
        clickEdit = false;
        
        if(isEdit != Null && isEdit)
            return new PageReference('/apex/VFP_NewPRMRegistration?sfdc.tabName=01r12000000KAPd&contId='+retUrl);
        else 
            return null;
    }
    
    
    private Boolean IsAccountChanged() {
        Boolean haveValuesChanged = false;
        String accid = Acc.Id;
        String qCols = 'Select Id,'+String.join(accountFields, ',');
        if(qCols.endsWithIgnoreCase(',')) qCols = qCols.removeEndIgnoreCase(',');
        qCols = qCols+' FROM Account WHERE ID = :accid';
        System.debug('The query Value is' +qCols);
        OrgAcc = Database.Query(qCols);

        for(String s:accFields){
            System.debug('**What are the values we are getting'+Acc.get(s)+'OldValue'+OrgAcc.get(s));
            
            if(!haveValuesChanged && Acc.get(s) != OrgAcc.get(s)){
                System.debug('**What are the values we are getting'+Acc.get(s)+'OldValue'+OrgAcc.get(s));
                haveValuesChanged = True;
            }
        }
        return haveValuesChanged;
    }

    public pagereference MoveToDetailPage(){
        return new pagereference('/'+acc.Id);
    }
    public pagereference goToChangePrimaryContactPage(){
        return new pagereference('/apex/VFP_PRMChangePrimaryContactOld?id='+acc.Id);
    }
    public pagereference goToManageExternalFacetPage(){
        clickEdit = false;
        PrimaryChangeIsSucess = false;
        return new pagereference('/apex/VFP_ManageAccountExternalFacet?id='+acc.Id);
    }
    public pagereference updateShowLocatorStatus(){
        Account updtAcc = new account(id = acc.id,PLShowInPartnerLocator__c = NewStatus );
        Update updtAcc;
        PageReference redirect = new pagereference('/apex/VFP_ManageAccountExternalFacet?id='+acc.Id);
        redirect.setRedirect(true); 
        return redirect;
    }   
}