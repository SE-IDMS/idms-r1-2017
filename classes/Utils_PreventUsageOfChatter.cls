/*
Author:Siddharatha Nagavarapu (GD Solutions India)
Class:Utils_PreventUsageOfChatter
Release:bFO December 2012 Major release
Component No:
Purpose: This class provides methods to prevent posts to the selected objects and groups in SpiceGroupData__c and SpiceObjectData__c(will come as part of april 2012 release)
Methods:
    public:
        populateAllowedListForPosts
        preventPosts
        preventComments
    private:
        populateAllowedListForPosts
        populateAllowedListForComments  
*/
public class Utils_PreventUsageOfChatter
{
    //  variables declaration start 

    public static Map<String,String> notallowedObjectsList=null; //get the list of objects for which the posts and comments are not allowed
    // public static Set<String> notallowedGroupsFeedsList=null; //get the list of Groups for which the posts and comments are not allowed    
    // public static Set<String> notallowedGroupsCommentsList=null; //get the list of Comments for which the posts and comments are not allowed    
    public static Set<Id> notallowedGroupsList=null;
    // variables declaration end

/*
Author:Siddharatha Nagavarapu (GD Solutions India)
Method:populateAllowedListForPosts
Release:bFO December 2012 Major release
Purpose: Query the information from SpiceGroupData and populate the notallowedGroupsList list.
*/
private static void populateAllowedListForPosts()
{
    notallowedObjectsList=new Map<String,String>(); //Marked for future release       
    for(SpiceObjectData__c spd:[select ObjectAPIName__c, ErrorMessage__c from SpiceObjectData__c where PostsAllowedinChatter__c=false])        
        notallowedObjectsList.put(spd.ObjectAPIName__c, spd.ErrorMessage__c);
        
    notallowedGroupsList=new Set<Id>();
    if (!notallowedObjectsList.containsKey(System.Label.CLDEC12SED03))
    {
        for(SpiceGroupData__c sgd:[select GroupId__c from SpiceGroupData__c where PostsAllowedinChatter__c=false])
        {
            // notallowedGroupsFeedsList.add((sgd.GroupId__c+'').subString(0,14).toUpperCase());
            notallowedGroupsList.add(sgd.GroupId__c);
        }
    }
}

/*
Author:Siddharatha Nagavarapu (GD Solutions India)
Method:populateAllowedListForComments
Release:bFO December 2012 Major release
Purpose: Query the information from SpiceGroupData and populate the notallowedGroupsList list.
*/

private static void populateAllowedListForComments()
{  
    notallowedObjectsList=new Map<String,String>(); //Marked for future release       
    for(SpiceObjectData__c spd:[select ObjectAPIName__c, ErrorMessage__c from SpiceObjectData__c where CommentsAllowedinChatter__c=false])        
        notallowedObjectsList.put(spd.ObjectAPIName__c, spd.ErrorMessage__c);
        
    notallowedGroupsList=new Set<Id>();
    if (!notallowedObjectsList.containsKey(System.Label.CLDEC12SED03)) // i.e. If the Chatter group object is not frozen
    {
        for(SpiceGroupData__c sgd:[select GroupId__c from SpiceGroupData__c where CommentsAllowedinChatter__c=false])
        {
            // notallowedGroupsFeedsList.add((sgd.GroupId__c+'').subString(0,14).toUpperCase());
            notallowedGroupsList.add(sgd.GroupId__c);
        }
    }       
}

/*
Author:Siddharatha Nagavarapu (GD Solutions India)
Method:preventPosts
Release:bFO December 2012 Major release
Purpose: Based on the Id get the get the object type and check in the list of notallowedGroups and display the message in the label
*/

public static void preventPosts(List<FeedItem> feedItems) 
{
    populateAllowedListForPosts();
    
    for(FeedItem feedItem:feedItems)
    {
        String objectType=(feedItem.parentId.getSobjectType()+''); //getSobjectType method is part of API 26 in apex.
        if(notallowedObjectsList.containsKey(objectType))
        {
            if(notallowedObjectsList.get(objectType) != null && notallowedObjectsList.get(objectType).length()>0)
            {
                if(!Test.isRunningTest())
                    feedItem.addError(notallowedObjectsList.get(objectType));  
            }
            else
            {
               if(!Test.isRunningTest())
                   feedItem.addError(System.Label.CLDEC12SED01);  
            }   
        }
        else if(objectType.equalsIgnoreCase(System.Label.CLDEC12SED03)) // i.e. if the post is linked to a group
        {
            if (notallowedGroupsList.contains(feedItem.ParentId)) // i.e. if the related Chatter group is frozen
            {
                if(!Test.isRunningTest())
                    feedItem.addError(System.Label.CLDEC12SED02);     
            }
        }
    }
}

/*
Author:Siddharatha Nagavarapu (GD Solutions India)
Method:preventComments
Release:bFO December 2012 Major release
Purpose: Based on the Id get the get the object type and check in the list of notallowedGroups and display the message in the label
*/
public static void preventComments(List<FeedComment> feedComments) 
{
    populateAllowedListForComments();
    
    for(FeedComment feedComment:feedComments)
    {
        String objectType=(feedComment.parentId.getSobjectType()+''); //getSobjectType method is part of API 26 in apex.
        if(notallowedObjectsList.containsKey(objectType))
        {
            if(notallowedObjectsList.get(objectType) != null && notallowedObjectsList.get(objectType).length() > 0)
            {
                if(!Test.isRunningTest())
                    feedComment.addError(notallowedObjectsList.get(objectType));  
            }
            else
            {
               if(!Test.isRunningTest())
                   feedComment.addError(System.Label.CLDEC12SED01);  
            }   
        }
        else if(objectType.equalsIgnoreCase(System.Label.CLDEC12SED03)) // i.e. if the comment is linked to a group
        {
            if (notallowedGroupsList.contains(feedComment.ParentId)) // i.e. if the related Chatter group is frozen
            {
                if(!Test.isRunningTest())
                    feedComment.addError(System.Label.CLDEC12SED02);     
            }
        }
    }
}      

}