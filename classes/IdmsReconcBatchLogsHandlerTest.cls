@isTest
public class IdmsReconcBatchLogsHandlerTest {
    static testmethod void createUpdateUserBatch(){
     IdmsReconcBatchLogsHandler batchHandler = new IdmsReconcBatchLogsHandler(); 
     IdmsUsersResyncflowServiceIms.createContactReturnBean createbean = new IdmsUsersResyncflowServiceIms.createContactReturnBean();
     IdmsUsersResyncflowServiceIms.contactUpdateReturnBean updatebean= new IdmsUsersResyncflowServiceIms.contactUpdateReturnBean();
     
     String env43 = System.Label.CLQ316IDMS043 ; 
     String env44 = System.Label.CLQ316IDMS044 ; 
     
     User userbatch = new User(alias = 'userbat', email='userbat' + '@accenture.com', 
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                    BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                    timezonesidkey='Europe/London', username='userbat' + env44 + env43 ,Company_Postal_Code__c='12345',
                                    Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                    IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                    IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
            
     insert userbatch;
     IdmsReconcBatchLogsHandler.batchLogForCreateUser(userbatch, createbean ,null);
     IdmsReconcBatchLogsHandler.batchLogsForUpdateUser(userbatch, updatebean,null);
    }
    
    static testmethod void createUpdateCompanyBatch(){
     IdmsReconcBatchLogsHandler batchHandler = new IdmsReconcBatchLogsHandler();  
     String env43 = System.Label.CLQ316IDMS043 ; 
     String env44 = System.Label.CLQ316IDMS044 ; 
     
     User userbatch1= new User(alias = 'userbat1', email='userbat1' + '@accenture.com', 
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                    BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                    timezonesidkey='Europe/London', username='userbat1' + env44 + env43 ,Company_Postal_Code__c='12345',
                                    Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                    IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                    IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
            
     insert userbatch1;
     IdmsUsersResyncflowServiceIms.createReturnBean createReturn = new IdmsUsersResyncflowServiceIms.createReturnBean();
     IdmsUsersResyncflowServiceIms.accountUpdateReturnBean accountUpdate = new IdmsUsersResyncflowServiceIms.accountUpdateReturnBean();
     IdmsReconcBatchLogsHandler.batchLogForCreateCompany(userbatch1,createReturn, null );
     IdmsReconcBatchLogsHandler.batchLogsForUpdateCompany(userbatch1,accountUpdate,null );
     
    }
    static testmethod void createUpdateCompanyFailedBatch(){
     IdmsReconcBatchLogsHandler batchHandler = new IdmsReconcBatchLogsHandler();  
     String env43 = System.Label.CLQ316IDMS043 ; 
     String env44 = System.Label.CLQ316IDMS044 ; 
     
     User userbatch2= new User(alias = 'userbat2', email='userbat2' + '@accenture.com', 
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                    BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                    timezonesidkey='Europe/London', username='userbat2' + env44 + env43 ,Company_Postal_Code__c='12345',
                                    Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                    IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                    IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
            
     insert userbatch2;
     IDMSCompanyReconciliationBatchHandler__c companyHandler = new IDMSCompanyReconciliationBatchHandler__c();
     IdmsReconcBatchLogsHandler.batchLogForFailedCreateCompany(null,companyHandler,null);
    }
    
}