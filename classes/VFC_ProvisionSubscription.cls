global class VFC_ProvisionSubscription {
   // global String accessToken {get;set;}
  //  global String msg {get;set;}
    String sname{get;set;}
    public VFC_ProvisionSubscription(ApexPages.StandardController controller){
       string name = ApexPages.currentPage().getParameters().get('name');
       string sid = ApexPages.currentPage().getParameters().get('id');
       sname = sid;
       string msg1 = provisioningSubscripition(name);
       ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, msg1);
       ApexPages.addmessage(myMsg);
    }
    
    webservice static string GenerateIFWAccessToken() {
        //generating the access token to IFW
        Http h = new Http();
        String ClientId = System.Label.CLOCT15FIO003;
        String ClientSecret = System.Label.CLOCT15FIO004;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(System.Label.CLOCT15FIO005);
        req.setHeader('Authorization',System.Label.CLOCT15FIO006);
        req.setHeader('Content-Type',System.Label.CLOCT15FIO007);
        req.setBody(System.Label.CLOCT15FIO008);
        req.setMethod('POST');
        HttpResponse res = h.send(req);
        Map<String,Object> responseMap =   (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
        System.debug('** IFW AUTH RESPONSE:' + responseMap);
        string accessToken = (string)responseMap.get('access_token');
        System.debug('** IFW AUTH TOKEN:' + accessToken);
        
        return accessToken;
    }
    
    public string provisioningSubscripition(string subName) {
        String msg='';
        system.debug('&&&&'+subName);
        String token = GenerateIFWAccessToken();
        string session = userinfo.getsessionid();
        String bearer = 'Bearer '+ token;
       // String body = '<?xml version="1.0" encoding="UTF-8"?><fulfillProvisioning><event><category>SubscriptionCreated</category></event><subscription><subscriptionName>' + sub.Name + '</subscriptionName></subscription></fulfillProvisioning>';
        Http h1 = new Http();
        HttpRequest req1 = new HttpRequest();
        req1.setEndpoint(System.Label.CLOCT15FIO009);
        req1.setHeader('Authorization','Bearer ' + token);
        req1.setHeader('Accept',System.Label.CLOCT15FIO012);
        req1.setHeader(System.Label.CLOCT15FIO050,System.Label.CLOCT15FIO051);
        req1.setBody(System.Label.CLOCT15FIO010 + subName + System.Label.CLOCT15FIO011);
        req1.setHeader('requestId',System.Label.CLOCT15FIO014);
        req1.setHeader(System.Label.CLOCT15FIO052,System.Label.CLOCT15FIO053);
        req1.setHeader(System.Label.CLOCT15FIO054,System.Label.CLOCT15FIO055);
        req1.setHeader('Content-Type',System.Label.CLOCT15FIO013);
        req1.setMethod('POST');
        System.debug('*******'+req1);
        HttpResponse res1 = h1.send(req1);
        Integer code = res1.getStatusCode();
        System.debug('**&&'+code);
        System.debug('****'+res1);
        if(code == 200)
            msg = System.Label.CLOCT15FIO015;
        else
            msg = System.Label.CLOCT15FIO016;
      /*  Dom.Document doc = res1.getBodyDocument();
        for(dom.XmlNode node : doc.getRootElement().getChildElements()) {
          if(node.getName()=='message') {
             System.debug('*********'+node.getText());
             msg = node.getText();
           }
        } */
        return msg;
    }
    
    public pageReference back() {
        PageReference pageRef = new PageReference('/'+sname);
        return pageRef;
    }
}