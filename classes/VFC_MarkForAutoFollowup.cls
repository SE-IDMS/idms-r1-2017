public class VFC_MarkForAutoFollowup {
    public Case currentCase;
    private static final  String strCustomer  = 'Customer';
    private static final  String strStatusInProgress  = 'In Progress';
    private static final  String strStatusAnswerProvidedToCustomer  = 'Answer Provided to Customer';
    private static final  String strStatusNew  = 'New';
    
    public VFC_MarkForAutoFollowup(ApexPages.StandardController controller) {

        currentCase     =  (Case)controller.getRecord();
        currentCase      = [Select Id, OwnerId,Status, ActionNeededFrom__c, AutoFollowupwithCustomer__c from Case where Id=:currentCase.Id limit 1];
    }
    public PageReference returnToCase() {
        PageReference returnPage = new PageReference('/' + currentCase.Id);
        return returnPage;
    }
    public PageReference performAction() {
        PageReference ResultPage=null;
        currentCase.Status = strStatusAnswerProvidedToCustomer;
        currentCase.ActionNeededFrom__c = strCustomer;
        currentCase.AutoFollowupwithCustomer__c = true;
        //currentCase.OwnerId = UserInfo.getUserId();
        if(String.valueOf(currentCase.OwnerId).startsWithIgnoreCase('00G')){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLOCT14CCC58));
            return null;
        }
        else{
            Database.SaveResult caseUpdateResult = Database.update(currentCase,false);
            if(!caseUpdateResult.isSuccess()){
                Database.Error DMLerror = caseUpdateResult.getErrors()[0];
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLerror.getMessage()));
                return null;     
            }
            else{
                ResultPage = new PageReference('/' + currentCase.Id);
            }
        }
        return ResultPage;
    }
}