@isTest
private class ConnectTargetDashboardCont_Test{
    static testMethod void EntityInititativeHealthTest() {
    
       User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('conneEED');
       User runAsUser2 = Utils_TestMethods_Connect.createStandardConnectUser('conEEED');
       User runAsUser3 = Utils_TestMethods_Connect.createStandardConnectUser('conEEEED');
        System.runAs(runAsUser){
          test.startTest();
          Initiatives__c inti=new Initiatives__c(Name='initiative', Transformation__c = Label.ConnectTransPeople);
         insert inti;
          Initiatives__c inti1=new Initiatives__c(Name='initiative', Transformation__c = Label.ConnectTransCustomer);
         insert inti1;
          Initiatives__c inti2=new Initiatives__c(Name='initiative', Transformation__c = Label.ConnectTransEfficiency);
         insert inti2;
         Initiatives__c inti3=new Initiatives__c(Name='initiative', Transformation__c = Label.ConnectTransEverywhere);
         insert inti3;
         
           Project_NCP__c cp=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',User__c = runasuser.id, Program_Leader_2__c = runAsUser2.id,Program_Leader_3__c = runAsUser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp;          
         Project_NCP__c cp1=new Project_NCP__c(Program__c=inti1.id,Program_Name__c='cp2',User__c = runasuser2.id, Program_Leader_2__c = runasuser.id,Program_Leader_3__c = runAsUser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp1;
         Project_NCP__c cp2=new Project_NCP__c(Program__c=inti2.id,Program_Name__c='cp3',User__c = runasuser2.id, Program_Leader_2__c = runasuser.id,Program_Leader_3__c = runAsUser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp2;
         Project_NCP__c cp3=new Project_NCP__c(Program__c=inti3.id,Program_Name__c='cp4',User__c = runasuser2.id, Program_Leader_2__c = runasuser.id,Program_Leader_3__c = runAsUser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp3;
                
         Global_KPI_Targets__c GKPIT=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp.id, Transformation__c = Label.ConnectTransPeople, Global_Business__c = 'Industry', KPI_Type__c = Label.ConnectGlobalKPI,KPI_Acronym__c ='test',Year__c=Label.ConnectYear,Global_KPI_Target_Q1__c = '100',Global_KPI_Actual_Q1__c='90');
         insert GKPIT; 
         Cascading_KPI_Target__c EKPI = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,OverallKPIOrder__c = 0,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'Industry' );
         insert EKPI;
         
         Global_KPI_Targets__c GKPIT1=new Global_KPI_Targets__c(KPI_Name__c='testKPI1',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp1.id, Transformation__c = Label.ConnectTransCustomer,Global_Business__c = 'Industry',KPI_Type__c = Label.ConnectGlobalKPI,KPI_Acronym__c ='test',Year__c=Label.ConnectYear,Global_KPI_Target_Q2__c = '100',Global_KPI_Actual_Q2__c='90');
         insert GKPIT1;
         Cascading_KPI_Target__c EKPI1 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT1.id,OverallKPIOrder__c = 0,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'Industry' );
         insert EKPI1;
         Global_KPI_Targets__c GKPIT2=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp2.id, Transformation__c = Label.ConnectTransEfficiency,Global_Business__c = 'Industry',KPI_Type__c = Label.ConnectGlobalKPI,KPI_Acronym__c ='test',Year__c=Label.ConnectYear,Global_KPI_Target_Q1__c = '100',Global_KPI_Actual_Q1__c='90');
         insert GKPIT2;
         Cascading_KPI_Target__c EKPI2 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT2.id,OverallKPIOrder__c = 0,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'Industry' );
         insert EKPI2;
         Global_KPI_Targets__c GKPIT3=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp3.id, Transformation__c = Label.ConnectTransEverywhere,Global_Business__c = 'Industry',KPI_Type__c = Label.ConnectGlobalKPI,KPI_Acronym__c ='test',Year__c=Label.ConnectYear);
         insert GKPIT3;         
         Cascading_KPI_Target__c EKPI3 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT3.id,OverallKPIOrder__c = 0,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'Industry' );
         insert EKPI3;
       //   Global_KPI_Targets__c GKPIT4=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Global_Business__c = 'Industry');
       //  insert GKPIT4;
      //    Global_KPI_Targets__c GKPIT5=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Global_Business__c = 'Industry',OverallKPIOrder__c=0);
      //   insert GKPIT5;
         Global_KPI_Targets__c GKPIT6=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp2.id, Transformation__c = Label.ConnectTransEfficiency,Global_Business__c = 'Industry',OverallKPIOrder__c=0,KPI_Type__c = Label.ConnectGlobalKPI);
         insert GKPIT6;
         Cascading_KPI_Target__c EKPI6 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT6.id,OverallKPIOrder__c = 0,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'Industry' );
         insert EKPI6;
         
         Global_KPI_Targets__c GKPIT7=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email, Transformation__c = Label.ConnectTransNone,Global_Business__c = 'Industry;test',OverallKPIOrder__c=0,KPI_Type__c = Label.ConnectGlobalKPI);
         insert GKPIT7;
         Cascading_KPI_Target__c EKPI7 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT7.id,OverallKPIOrder__c = 0,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'Industry' );
         insert EKPI7;
         
         Global_KPI_Targets__c GKPIT8=new Global_KPI_Targets__c(KPI_Name__c='testKPI2',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email, Transformation__c = Label.ConnectTransNone,Global_Business__c = 'Industry',OverallKPIOrder__c=1,KPI_Type__c = Label.ConnectGlobalKPI);
         insert GKPIT8;
         Cascading_KPI_Target__c EKPI8 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT7.id,OverallKPIOrder__c = 0,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'test' );
         insert EKPI8;
         
           PageReference pageRef = Page.ConnectTargetToolReport;
           Test.setCurrentPage(pageRef);
           ConnectTargetDashboardController controller = new ConnectTargetDashboardController ();
       
       
    controller.Entity = Label.ConnectCorporateKPIGlobal;       
    controller.getFilterList();
    controller.FilterChange();
    controller.getFilterYear();
    controller.getFilterQuarter();
    
    controller.RenderPDF();
    
    controller.getConnectEverywhere();
    controller.getConnectToCustomer();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    
        
    controller.Entity = 'Industry';
     
    controller.getFilterList();
    controller.RenderPDF();
     controller.getFilterYear();
    controller.getFilterQuarter();
    
    controller.FilterChange();
     
    controller.getConnectEverywhere();
    controller.getConnectToCustomer();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    
    controller.getOverall();
    
    controller.Fin = 'True';
    controller.getFin();
    
    controller.Quarter = 'Q4';
    controller.Entity = Label.ConnectCorporateKPIGlobal;       
    controller.getFilterList();
    controller.FilterChange();
  
    controller.getConnectEverywhere();
    controller.getConnectToCustomer();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    
    controller.Entity = 'Industry';
    controller.Fin = 'True';
    controller.getFin();
     
    controller.getFilterList();
    controller.RenderPDF();
     controller.getFilterYear();
    controller.getFilterQuarter();
    
    controller.FilterChange();
     
    controller.getConnectEverywhere();
    controller.getConnectToCustomer();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    
   
    
    controller.Quarter = 'Q3';
    controller.Entity = Label.ConnectCorporateKPIGlobal;       
    controller.getFilterList();
    controller.FilterChange();
    test.stopTest();
    controller.getConnectEverywhere();
    controller.getConnectToCustomer();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    
    controller.Entity = 'Industry';
    controller.Fin = 'True';
    controller.getFin();
     
    controller.getFilterList();
    controller.RenderPDF();
     controller.getFilterYear();
    controller.getFilterQuarter();
    
    controller.FilterChange();
     
    controller.getConnectEverywhere();
    controller.getConnectToCustomer();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    
     
     
    controller.Quarter = 'Q2';
    controller.Entity = Label.ConnectCorporateKPIGlobal;       
    controller.getFilterList();
    controller.FilterChange();
  
    controller.getConnectEverywhere();
    controller.getConnectToCustomer();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    
    controller.Entity = 'Industry';
    controller.Fin = 'True';
    controller.getFin();
     
    controller.getFilterList();
    controller.RenderPDF();
     controller.getFilterYear();
    controller.getFilterQuarter();
    
    controller.FilterChange();
     
    controller.getConnectEverywhere();
    controller.getConnectToCustomer();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    
   
    controller.Quarter = 'Q1';
    controller.Entity = Label.ConnectCorporateKPIGlobal;       
    controller.getFilterList();
    controller.FilterChange();
  
    controller.getConnectEverywhere();
    controller.getConnectToCustomer();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    
    controller.Yearchange();
    controller.QuarterChange();
    Controller.getyears();
    
    controller.Entity = 'Industry';
    controller.Fin = 'True';
    controller.getFin();
     
    controller.getFilterList();
    controller.RenderPDF();
     controller.getFilterYear();
    controller.getFilterQuarter();
    
    controller.FilterChange();
     
    controller.getConnectEverywhere();
    controller.getConnectToCustomer();   
    controller.getConnectPeople();
    controller.getConnectforEfficiency();
    
    
   
    }
    }
    }