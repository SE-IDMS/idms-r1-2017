/****************************************************
Class       : VFC_SRV_MYFSWOReportsController
Author      : Raghavender Katta
Description : This controller fetches the intervention reports based on applied filters.
****************************************************/ 
Public without sharing Class VFC_SRV_MYFSWOReportsController{
    Public Map<string,string> selectoptionMap{get;set;}
    Public String SlectedValue{get;set;}
    Public String UltimateParentAccount {get;set;}
    Public String SelectedCountry {get;set;}
    Public Static String TempParentAccount{get;set;}
    Public Static String TempCoutnry{get;set;}
    Public list<SVMXC__Service_Order__c> woList{get;set;}
    Public String Site{get;set;}
    Public String WOStatus{get;set;}
    Public String FromDate{get;set;}
    Public string ToDate{get;set;}
    public Integer size {get; set;}    
    Public Integer woAttachmentListSize {get;set;}
    Public Static String ultParentAccount {get;set;}
    Public Static String locationCountry {get;set;}
    Public list<Attachment> attachmentList{get;set;}
    Public Map<String,SVMXC__Service_Order__c> woMap{get;set;}
    Public Map<String,List<Attachment>> woAttachmentMap{get;set;}
    Public List<String> woIdList{get;set;}
    Public Integer attachmentListSize {get;set;}
    Public Map<String,List<Attachment>> woAttachmentSubMap{get;set;}
    Public List<String> tempWOIdList{get;set;}
    Public String currentUserLanguage{get;set;}
    Public map<id,string> locaccountmap{get;set;}
    Public Boolean isMoreSites{get;set;}
    Public Set<Id> siteIdSet{get;set;}
    Public String UserId{get;set;}
    
    
    Public VFC_SRV_MYFSWOReportsController(){
        
        selectoptionMap= new map<string,string>();
        locaccountmap = new map<id,string>();
        selectoptionMap.put(System.label.CLSRVMYFS_ViewAll,System.label.CLSRVMYFS_ViewAll);
        UltimateParentAccount = Apexpages.currentpage().getparameters().get('Params');
        SelectedCountry = Apexpages.currentpage().getparameters().get('country');
        currentUserLanguage = UserInfo.getLanguage();
        System.debug('currentUserLanguage' + currentUserLanguage);
        ultParentAccount = UltimateParentAccount;
        locationCountry = SelectedCountry;
        SlectedValue = Apexpages.currentpage().getparameters().get('Site');
        TempParentAccount = UltimateParentAccount;
        TempCoutnry = SelectedCountry;
        
        UserId = Apexpages.currentpage().getparameters().get('UId');
        if(userId==null || userId ==''){
         userId = UserInfo.getUserId();
         
        }
        
        /* list<SVMXC__Site__c> lstSites = [Select id,name,SVMXC__Account__c,SVMXC__Account__r.Country__r.name,SVMXC__Account__r.UltiParentAcc__c from SVMXC__Site__c where
(SVMXC__Account__r.UltiParentAcc__c =:UltimateParentAccount OR SVMXC__Account__C =:UltimateParentAccount ) and PrimaryLocation__c = True and SVMXC__Account__r.Country__r.CountryCode__c=:SelectedCountry ORDER BY name Asc]; */
        List<SVMXC__Site__c> lstSites = AP_SRV_myFSUtils.getAuthorizedSitesForUser(userId, SelectedCountry, UltimateParentAccount);
        for(SVMXC__Site__c Sites :lstSites){
            selectoptionMap.put(sites.Id,sites.name);
            locaccountmap.put(sites.Id,sites.SVMXC__Account__c);
            System.debug('locaccountmap'+locaccountmap);
        }
        if(selectoptionMap != null && selectoptionMap.size() > 2 ){
            isMoreSites = true;
        }else{
            selectoptionMap.remove(System.label.CLSRVMYFS_ViewAll);
        }
        size=20; 
        Site = Apexpages.currentpage().getparameters().get('Site'); 
        System.debug('test'+Site);
        woAttachmentListSize = 0;
        if(site == null){
            site = System.label.CLSRVMYFS_ViewAll;
        }
    }
    //Called on chnage of location picklist and retrives WO
    Public Void getInterventionReports(){
        DateTime todayDateTime = system.now();
         Set<Id> SitesIdSet = new Set<Id>();
         DateTime fromdateFormat;
         DateTime todateFormat;
        if(site==System.label.CLSRVMYFS_ViewAll){
         
      WOList =  AP_SRV_myFSUtils.getAuthorizedInterventionReportsforUser(userId, SelectedCountry, UltimateParentAccount,todayDateTime,fromdateFormat,todateFormat,siteIdSet);
            System.debug('constructor'+woList);
            processWOInterventionReports(woList);          
            
            
        }
        else{
         
           WOList =AP_SRV_myFSUtils.getAuthorizedInterventionReportsforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDateTime,fromdateFormat,todateFormat,siteIdSet); 
            processWOInterventionReports(woList);
            
            
        }
    }
    
    public void next(){
        
        this.size+=20;
        if(woAttachmentMap != null && !woAttachmentMap.isEmpty()){
            List<attachment> subAttachmentList = new List<attachment>();
            woIdList = new List<String>();
            if(tempWOIdList != null && !tempWOIdList.isEmpty()){
                for(String woId : tempWOIdList){
                    List<Attachment> subListOfAttachment = new List<Attachment>();
                    if(woAttachmentMap.get(woId) != null && !woAttachmentMap.get(woId).isEmpty()){
                        for(Attachment eachAttachment : woAttachmentMap.get(woId)){
                            subAttachmentList.add(eachAttachment);
                            subListOfAttachment.add(eachAttachment);
                            if(subAttachmentList != null && subAttachmentList.size() == size){
                                break;
                            }
                        }
                        
                    }
                    woIdList.add(woId);
                    woAttachmentSubMap.put(woId,subListOfAttachment);
                    if(subAttachmentList != null && subAttachmentList.size() == size){
                        break;
                    }
                }
                
            }
        }
        
    }
    
    Public void fromDateSelector(){
        set<string> completedStatus = new set<String>();   
        
        DateTime fromdateFormat;
        DateTime todateFormat;
        DateTime todayDateTime = system.now();
        if(fromdate!=''){
            fromdateFormat= date.valueof(fromdate);
        }
        if(todate!=''){
            
            todateFormat= date.valueof(todate).adddays(+1);  
        }  
        
        if(site==System.label.CLSRVMYFS_ViewAll){
              
                WOList =  AP_SRV_myFSUtils.getAuthorizedInterventionReportsforUser(userId, SelectedCountry, UltimateParentAccount,todayDateTime,fromdateFormat,todateFormat,siteIdSet);
   
               processWOInterventionReports(woList);
                
            
                    
              
        }
        else{       
                
               
             WOList =AP_SRV_myFSUtils.getAuthorizedInterventionReportsforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDateTime,fromdateFormat,todateFormat,siteIdSet); 
                processWOInterventionReports(woList);                           
                                   
                
        }
    }
    
    Public void processWOInterventionReports(list<SVMXC__Service_Order__c> woList){
        attachmentList = new List<attachment>();
        DateTime TodayDate = system.now();
        woMap = new Map<String,SVMXC__Service_Order__c>(); 
        woAttachmentMap = new Map<String, List<Attachment>>(); 
        woIdList = new List<String>();
        tempWOIdList = new List<String>();
        woAttachmentSubMap = new Map<String, List<Attachment>>();  
        if(woList != null && !woList.isempty()){
            if(Test.isRunningTest()){
            }else{
                AP_SRV_myFSUtils.WorkorderSharing(WOList,userId);
            } 
            for(SVMXC__Service_Order__c eachWO : woList){
                 if(eachWO.TECH_CustomerScheduledDateTimeECH__c == null && eachWO.CustomerTimeZone__c != null && eachWO.SVMXC__Scheduled_Date_Time__c != null){
                    eachWO.TECH_CustomerScheduledDateTimeECH__c= eachWO.SVMXC__Scheduled_Date_Time__c.format('dd MMM yyyy HH:mm:ss', eachWO.CustomerTimeZone__c);
                  }   
                List<Attachment> listOfAttachment = new List<Attachment>();
                if(eachWO.Attachments != null && !(eachWO.Attachments).isEmpty()){
                    tempWOIdList.add(eachWO.Id);
                    woMap.put(eachWO.Id,eachWO);
                    for(Attachment eachAttachment :eachWO.Attachments){
                        attachmentList.add(eachAttachment);
                        listOfAttachment.add(eachAttachment);                         
                    }
                    woAttachmentMap.put(eachWO.Id,listOfAttachment);
                    
                    
                }
                
            }
            woAttachmentListSize = attachmentList.size();
            if(woAttachmentMap != null && !woAttachmentMap.isEmpty()){
                List<attachment> subAttachmentList = new List<attachment>();
                if(tempWOIdList != null && !tempWOIdList.isEmpty()){
                    for(String woId : tempWOIdList){
                        List<Attachment> subListOfAttachment = new List<Attachment>();
                        if(woAttachmentMap.get(woId) != null && !woAttachmentMap.get(woId).isEmpty()){
                            for(Attachment eachAttachment : woAttachmentMap.get(woId)){
                                subAttachmentList.add(eachAttachment);
                                subListOfAttachment.add(eachAttachment);
                                if(subAttachmentList != null && subAttachmentList.size() == size){
                                    break;
                                }
                            }
                            
                        }
                        woIdList.add(woId);
                        woAttachmentSubMap.put(woId,subListOfAttachment);
                        if(subAttachmentList != null && subAttachmentList.size() == size){
                            break;
                        }
                    }
                    
                }
            }
        }
        If(attachmentList!=null && attachmentList.IsEmpty()){
            woAttachmentListSize = 0;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, System.label.CLSRVMYFS_NO_INTERVENTION_REPORTS_TO_DISPLAY));
        }
    }
    
    Public void toDateSelector(){
        set<string> completedStatus = new set<String>();   
        
        DateTime fromdateFormat;
        DateTime todateFormat;
        DateTime todayDateTime = system.now();
        if(fromdate!=''){
            fromdateFormat= date.valueof(fromdate);
        }
        
        if(todate!=''){
            todateFormat= date.valueof(todate).adddays(+1);  
        }  
        
        
        
        if(site == System.label.CLSRVMYFS_ViewAll){
            
             
              WOList =  AP_SRV_myFSUtils.getAuthorizedInterventionReportsforUser(userId, SelectedCountry, UltimateParentAccount,todayDateTime,fromdateFormat,todateFormat,siteIdSet);
                processWOInterventionReports(woList);             
                                
               
        }
        else{    
            
                
                
                 WOList =AP_SRV_myFSUtils.getAuthorizedInterventionReportsforAccount(userId, SelectedCountry, UltimateParentAccount,locaccountmap.get(site),todayDateTime,fromdateFormat,todateFormat,siteIdSet); 
                processWOInterventionReports(woList);
                
           
                 
        }
    }
    
}