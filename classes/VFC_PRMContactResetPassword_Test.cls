@isTest
public class VFC_PRMContactResetPassword_Test {
    
    static TestMethod void pRMContactResetPassword() {
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        System.runAs(u) {
        Country__c testCountry = new Country__c();
            testCountry.Name   = 'India';
            testCountry.CountryCode__c = 'IN';
            testCountry.InternationalPhoneCode__c = '91';
            testCountry.Region__c = 'APAC';
        INSERT testCountry;
        StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
        INSERT testStateProvince;
                
        Account newAcc = new Account();
            newAcc.RecordTypeId = System.Label.CLAPR15PRM025;   
            newAcc.Name  =  'NewAccount';
            newAcc.PRMCompanyName__c = 'NewCompanyName';
            newAcc.PRMStreet__c       = 'Elnath Street ';
            newAcc.PRMAdditionalAddress__c = 'Marathalli';
            newAcc.PRMCity__c              = 'Bangalore';
            newAcc.PRMStateProvince__c     = testStateProvince.Id;
            //newAcc.PRMStateProvince__r.Name
            //newAcc.PRMCountry__r.Name
            newAcc.PRMCountry__c         = testCountry.Id;
            newAcc.PRMCompanyPhone__c    = '987654321';
            newAcc.PRMWebsite__c         = 'Https://schneider-electric.com';
            newAcc.PRMZipCode__c         = '560103';
            newAcc.PRMCorporateHeadquarters__c = true;
            newAcc.PRMUIMSID__c = '123456';
            newAcc.PRMBusinessType__c = 'FI';
            newAcc.PRMAreaOfFocus__c  = 'FI3';
        INSERT newAcc;
        
        
        Contact newCon = new Contact();
        newCon.LastName = 'lastNameContact';
        newCon.PRMUIMSID__c = '123456';
        newCon.AccountId = newAcc.Id;
        newCon.PRMContact__c = True;
        newCon.FirstName = 'FirstNameContact' ;
        INSERT newCon;
        
        
        
            PageReference pageRef = Page.VFP_PRMContactResetPassword;
            Test.setCurrentPage(pageRef);
            Apexpages.Standardcontroller thisController = new Apexpages.Standardcontroller(newCon);
            ApexPages.currentPage().getParameters().put('Id',newCon.id);
            VFC_PRMContactResetPassword thisContactResetPassword = new VFC_PRMContactResetPassword(thisController);
            thisContactResetPassword.ResetPassword();
        }
    }
}