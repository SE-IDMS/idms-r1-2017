@isTest
public class WS_UIMSAdminService_GetModel_Test {
    
    public static testMethod void testModels_1() {
        WS_UIMSAdminService_GetModel.getPrimaryContact g1 = new WS_UIMSAdminService_GetModel.getPrimaryContact();
        WS_UIMSAdminService_GetModel.accountBean a1 = new WS_UIMSAdminService_GetModel.accountBean();
        WS_UIMSAdminService_GetModel.RequestedEntryNotExistsException re1 = new WS_UIMSAdminService_GetModel.RequestedEntryNotExistsException();
        WS_UIMSAdminService_GetModel.primaryContactRequest pc1 = new WS_UIMSAdminService_GetModel.primaryContactRequest();
    WS_UIMSAdminService_GetModel.programLevelBean pl1 = new WS_UIMSAdminService_GetModel.programLevelBean();
    WS_UIMSAdminService_GetModel.LdapTemplateNotReadyException l1 = new WS_UIMSAdminService_GetModel.LdapTemplateNotReadyException();
        WS_UIMSAdminService_GetModel.getFeature gf1 = new WS_UIMSAdminService_GetModel.getFeature();
    WS_UIMSAdminService_GetModel.ImsServiceException ims1 = new WS_UIMSAdminService_GetModel.ImsServiceException();
    WS_UIMSAdminService_GetModel.contactBean cb1 = new WS_UIMSAdminService_GetModel.contactBean();
    WS_UIMSAdminService_GetModel.primaryContactResponse pcr1 = new WS_UIMSAdminService_GetModel.primaryContactResponse();
    WS_UIMSAdminService_GetModel.genericSDHBean sb1 = new WS_UIMSAdminService_GetModel.genericSDHBean();
    WS_UIMSAdminService_GetModel.InactiveUserImsException imse1 = new WS_UIMSAdminService_GetModel.InactiveUserImsException();
    WS_UIMSAdminService_GetModel.simpleResponseBean srb1 = new WS_UIMSAdminService_GetModel.simpleResponseBean();
        WS_UIMSAdminService_GetModel.getContactResponse gcr1 = new WS_UIMSAdminService_GetModel.getContactResponse();
    WS_UIMSAdminService_GetModel.childs_element ce1 = new WS_UIMSAdminService_GetModel.childs_element();
    WS_UIMSAdminService_GetModel.IMSServiceSecurityCallNotAllowedException sca1 = new WS_UIMSAdminService_GetModel.IMSServiceSecurityCallNotAllowedException();
    WS_UIMSAdminService_GetModel.getAccessControlResponse acr1 = new WS_UIMSAdminService_GetModel.getAccessControlResponse();
    WS_UIMSAdminService_GetModel.getPrimaryContactResponse pcr2 = new WS_UIMSAdminService_GetModel.getPrimaryContactResponse();
    WS_UIMSAdminService_GetModel.accessElement ae2 = new WS_UIMSAdminService_GetModel.accessElement();
    WS_UIMSAdminService_GetModel.featureBean fb1 = new WS_UIMSAdminService_GetModel.featureBean();
    WS_UIMSAdminService_GetModel.InvalidImsServiceMethodArgumentException sma1 = new WS_UIMSAdminService_GetModel.InvalidImsServiceMethodArgumentException();
    WS_UIMSAdminService_GetModel.accessTree at1 = new WS_UIMSAdminService_GetModel.accessTree();
    WS_UIMSAdminService_GetModel.genericProgBean pb1 = new WS_UIMSAdminService_GetModel.genericProgBean();
    WS_UIMSAdminService_GetModel.getAccessControl gac2 = new WS_UIMSAdminService_GetModel.getAccessControl();
    WS_UIMSAdminService_GetModel.getAccount ga1 = new WS_UIMSAdminService_GetModel.getAccount();
    WS_UIMSAdminService_GetModel.accessList_element ale1 = new WS_UIMSAdminService_GetModel.accessList_element();
    WS_UIMSAdminService_GetModel.getContact gc2 = new WS_UIMSAdminService_GetModel.getContact();
    WS_UIMSAdminService_GetModel.UnexpectedLdapResponseException ldap1 = new WS_UIMSAdminService_GetModel.UnexpectedLdapResponseException();
    WS_UIMSAdminService_GetModel.SecuredImsException sms1 = new WS_UIMSAdminService_GetModel.SecuredImsException();
    WS_UIMSAdminService_GetModel.getProgram gp2 = new WS_UIMSAdminService_GetModel.getProgram();
    WS_UIMSAdminService_GetModel.getAccountResponse gacr2 = new WS_UIMSAdminService_GetModel.getAccountResponse();
    }
    
    public static testMethod void testGetContact() {
        Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
        WS_UIMSAdminService_Get.BackOutputServiceImplPort port = new WS_UIMSAdminService_Get.BackOutputServiceImplPort();
        port.getContact('bFO_PRMAdmin', '873598df-918a-413d-8087-2d02e881ff1z');
    }
}