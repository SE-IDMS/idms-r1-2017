/*
    Author          : Ankit Badhani (ACN) 
    Date Created    : 23/05/2011
    Description     : Class utilised by trigger SMETeamAfterInsert.
*/

public class AP17_SharingAccessToSMETeam
{
    /* Method to grant Access to the SME
    */
    public static void insertSMETeamMember(Map<Id, RIT_SMETeam__C> newSMETeamMap)
    {
        System.Debug('******insertSMETeamMember method in SharingAccessToSMETeam Class Start****');
        List<OPP_QuoteLink__Share> quoteLinkShare= new List<OPP_QuoteLink__Share>();
        Set<ID> quoteID =new Set<ID>();
        for(RIT_SMETeam__C sme:newSMETeamMap.values())
        { 
        quoteID.add(sme.QuoteLink__c);
        }
        List<OPP_QuoteLink__Share> oppQuoteLinkShare = [SELECT parentid,userOrGroupid  FROM OPP_QuoteLink__Share WHERE parentId IN : quoteID  AND  RowCause='GrantAccessToSME__c'];  
        Map<String,RIT_SMETeam__c> smeTeam = new Map<String,RIT_SMETeam__c>();
        
        for(RIT_SMETeam__c  SMETeamMember  : newSMETeamMap.values())        
        {
            smeTeam.put(String.valueOf(SMETeamMember.QuoteLink__c)+String.valueOf(SMETeamMember.id),SMETeamMember);         
        }
    
      for(OPP_QuoteLink__Share  oppQuoteShare : oppQuoteLinkShare)
        {
                SmeTeam.keyset().remove(string.valueOf(oppQuoteShare.parentID)+string.valueOf(oppQuoteShare.userOrGroupid));         
        }
        set<String> smeUsers= new set<String>();
        for(RIT_SMETeam__c  SMETeamObj  :  SmeTeam.values())
        {
            smeUsers.add(newSmeTeamMap.get(SMETeamObj.Id).TeamMember__C);
        }
        Set<String> smeSalesUsers= new Set<String>();
        list<user> users=[select id,ProfileId,Permission_sets_assigned__c from user where id in:smeUsers and (ProfileId=:Label.CLJUL13SLS01 or profileID =:label.CL00419)];// and profileID =:label.CL00419];
        users=Utils_Methods.filterUsers(users,new Set<String>{System.Label.CLJUL13SLS02},Label.CLJUL13SLS01,Label.CL00419);
        for(user user:users)
        {
            smeSalesUsers.add(user.id);       
        }   
        for(RIT_SMETeam__c  SMETeamObj  : SmeTeam.values())
        {
            OPP_QuoteLink__Share  oppQLKSSMETeam  = new OPP_QuoteLink__Share();
            oppQLKSSMETeam.UserOrGroupId  =  newSmeTeamMap.get(SMETeamObj.Id).TeamMember__C;
           oppQLKSSMETeam.ParentID  =  newSmeTeamMap.get(SMETeamObj.ID).Quotelink__c;
            if(smeSalesUsers.contains(newSmeTeamMap.get(SMETeamObj.Id).TeamMember__C))
            {
                oppQLKSSMETeam.AccessLevel=label.CL00418;
            }
            else
            {
                oppQLKSSMETeam.AccessLevel=label.CL00203;
            }
            oppQLKSSMETeam.RowCause=Schema.OPP_QuoteLink__Share.RowCause.GrantAccessToSME__c;
          quoteLinkShare.add(oppQLKSSMETeam);
            system.debug('QuoteLinkShare'+quoteLinkShare);
        }
        if(quoteLinkShare.Size() > 0)
        {
            if(quoteLinkShare.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## AP16 Error updating : '+ Label.CL00264);
            }
            else
            {
                Database.SaveResult[] lsr = database.insert(quoteLinkShare,false);
                for( Database.SaveResult triggersr : lsr) 
                {
                    if(triggersr.isSuccess())
                    {  
                        system.debug('success Event share'); 
                    }
                    else
                    {  
                        Database.Error err = triggersr.getErrors()[0];               
                        if(err.getStatusCode() == StatusCode.FIELD_INTEGRITY_EXCEPTION  &&
                        err.getMessage().contains('AccessLevel')){ 
                        system.debug('Success *Trivial Access*');
                    } 
                    else
                    {
                        system.debug('failed * Event share*'); 
                    } 
                    } 
                }
            }
        }               
        System.Debug('*** insertSMETeamMember method in AP17_SharingAccessToSMETeam Class End***');  
    }
    /* Method to revoke Access from the SME
    */
    public static void deleteExistingSMETeamMember(Map<Id,RIT_SMETeam__C> deleteSMETeamMap)
    {        
        System.Debug('******deleteExistingSMETeamMember method in SharingAccessToSMETeam Class Start****');
        Map<String,id> smeMap = new  Map<String,id>();
        Set<String> existingSmeSet = new  Set<String>();
        set<Id> userId = new set<id>();
        set<Id> parentId = new set<Id>();
        for(RIT_SMETeam__c  smeTeamMember :  deleteSMETeamMap.values())
        {        
            smeMap.put(String.valueOf(smeTeamMember.QuoteLink__c)+String.valueOf(smeTeamMember.TeamMember__c),smeTeamMember.QuoteLink__c); 
            userId.add(smeTeamMember.TeamMember__c);
        } 
        List<RIT_SMETeam__c> existingSMEList= new List<RIT_SMETeam__c>([Select Id,name,QuoteLink__c,TeamMember__c from RIT_SMETeam__c where QuoteLink__c IN:smeMap.values() AND TeamMember__c IN: userId ]);     
        

            for(RIT_SMETeam__c existingSME : existingSMEList){
                 existingSmeSet.add(String.valueOf(existingSME.QuoteLink__c)+String.valueOf(existingSME.TeamMember__c)); 
            }
        
           for(String duplicate: existingSmeSet){
              smeMap.remove(duplicate);
           }
        List<OPP_QuoteLink__Share> newQLSSMEDelete = new List<OPP_QuoteLink__Share>();
        
        List<OPP_QuoteLink__Share> deleteQLSSMEList = [SELECT UserOrGroupId,ParentId,RowCause FROM OPP_QuoteLink__Share
                                                    WHERE RowCause = 'GrantAccessToSME__c' and ParentId IN: smeMap.values()];

        for(OPP_QuoteLink__Share  oppQLKShareObj: deleteQLSSMEList)
        {
            if(smeMap.keyset().contains(string.valueOf(oppQLKShareObj.parentID)+string.valueOf(oppQLKShareObj.userOrGroupid)))
            {
                newQLSSMEDelete.add(oppQLKShareObj);
            }
        }           
        system.debug('****'+ newQLSSMEDelete);
        if(newQLSSMEDelete.Size() > 0)
        {
            if(newQLSSMEDelete.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## AP16 Error updating : '+ Label.CL00264);
            }
            else
            {
                Database.DeleteResult[] lsr = database.delete(newQLSSMEDelete,false);
                for( Database.DeleteResult triggersr : lsr) 
                {
                    if(triggersr.isSuccess())
                    {  
                        system.debug('success Event share'); 
                    }
                    else
                    {  
                        Database.Error err = triggersr.getErrors()[0];               
                        if(err.getStatusCode() == StatusCode.FIELD_INTEGRITY_EXCEPTION  &&
                        err.getMessage().contains('AccessLevel')){ 
                        system.debug('Success *Trivial Access*');
                    } 
                    else
                    {
                        system.debug('failed * Event share*'); 
                    } 
                    } 
                }
            }
        }         
        System.Debug('***deleteExistingSMETeamMember method in AP17_SharingAccessToSMETeam Class End***');  
    }


    public static void updateSMETeamMember(Map<Id,RIT_SMETeam__c> newSMETeam,Map<Id,RIT_SMETeam__c> oldSMETeam)    
    {
    System.Debug('***updateSMETeamMember method in AP17_SharingAccessToSMETeam Class Start***');  
        deleteExistingSMETeamMember(oldSMETeam);
        insertSMETeamMember(newSMETeam);       
    System.Debug('***updateSMETeamMember method in AP17_SharingAccessToSMETeam Class End***');  
    }
}