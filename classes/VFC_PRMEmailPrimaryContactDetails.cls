global class VFC_PRMEmailPrimaryContactDetails{
    global Id currConAccountId {get;set;}
    global Contact PrimaryContact {
        get {
            if(currConAccountId != Null){
                List<Contact> primaryContactList = [Select Id,PRMFirstName__c,PRMEmail__C,PRMMobilePhone__c,Name,PRMLastName__c,PRMWorkPhone__c 
                                            From Contact Where AccountId =:currConAccountId AND PRMPrimaryContact__c = True];
                if(primaryContactList.Size() > 0)
                PrimaryContact = primaryContactList[0];
            }
            return PrimaryContact;
        }
        set;
    }  
}