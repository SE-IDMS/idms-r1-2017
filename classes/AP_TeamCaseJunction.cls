public class AP_TeamCaseJunction{

//*********************************************************************************
// Method Name      : DefaultSupportedCaseClassifications
// Purpose          : Ensure there is only one default choice on Creation of SupportedCaseClassifications
// Created by       : Hari Krishna - Global Delivery Team
// Date created     : 
// Modified by      :
// Date Modified    :
// Remarks          : For May Global CCC - 13 Release , BR-2197 
///********************************************************************************/    

    public static void DefaultSupportedCaseClassifications(Map<ID,TeamCaseJunction__c> newMap){
        
        Set<id> defaltTCJSet = new Set<id>();
        Set<id> CCTeamSet = new Set<id>();
        
        List <TeamCaseJunction__c> DefaultsList = new List <TeamCaseJunction__c>();
        List <TeamCaseJunction__c> toUpdatelist = new List <TeamCaseJunction__c>();
        
        for(TeamCaseJunction__c tcjnew :newMap.values()){
            if(tcjnew.Default__c != null && tcjnew.Default__c == true ){
                defaltTCJSet.add(tcjnew.id);
                CCTeamSet.add(tcjnew.CCTeam__c);
            }
        }       
        
        if(defaltTCJSet != null && defaltTCJSet.size()>0 && CCTeamSet != null && CCTeamSet.size()>0 )
          toUpdatelist  =   DefaultSCCUpdateList(defaltTCJSet, CCTeamSet);
        
        if(toUpdatelist!= null && toUpdatelist.size()>0){
            update toUpdatelist;
        }
        
    }

//*********************************************************************************
// Method Name      : DefaultSupportedCaseClassifications
// Purpose          : Ensure there is only one default choice on update of SupportedCaseClassifications
// Created by       : Hari Krishna - Global Delivery Team
// Date created     : 
// Modified by      :
// Date Modified    :
// Remarks          : For May Global CCC - 13 Release , BR-2197 
///********************************************************************************/   


    public static void DefaultSupportedCaseClassifications(Map<ID,TeamCaseJunction__c> newMap,Map<ID,TeamCaseJunction__c> oldMap){
        
        Set<id> defaltTCJSet = new Set<id>();
        Set<id> CCTeamSet = new Set<id>();
        
        List <TeamCaseJunction__c> DefaultsList = new List <TeamCaseJunction__c>();
        List <TeamCaseJunction__c> toUpdatelist = new List <TeamCaseJunction__c>();
        
        for(TeamCaseJunction__c tcjnew :newMap.values()){
            System.debug('************************************************ update ');
            System.debug('************************************************ new value :'+tcjnew.Default__c +'old Value :'+oldMap.get(tcjnew.id).Default__c);
            if(tcjnew.Default__c == true && oldMap.get(tcjnew.id).Default__c == false){
                defaltTCJSet.add(tcjnew.id);
                CCTeamSet.add(tcjnew.CCTeam__c);
            }
        }
        if(defaltTCJSet != null && defaltTCJSet.size()>0 && CCTeamSet != null && CCTeamSet.size()>0 )
        {
          toUpdatelist  =   DefaultSCCUpdateList(defaltTCJSet, CCTeamSet);
          System.debug('************************************************ update '+toUpdatelist);
        }
        
        
        if(toUpdatelist!= null && toUpdatelist.size()>0){
            update toUpdatelist;
        }
        
    }
    
    public static List<TeamCaseJunction__c> DefaultSCCUpdateList(Set<ID> defaltTCJSet,Set<id> CCTeamSet){
    
        List <TeamCaseJunction__c> toUpdatelist = new List <TeamCaseJunction__c>();
        List <TeamCaseJunction__c> DefaultsList = new List <TeamCaseJunction__c>();
        if(CCTeamSet!= null && CCTeamSet.size()>0){
            DefaultsList=[select Id, CCTeam__c, Default__c from TeamCaseJunction__c where CCTeam__c in :CCTeamSet and Default__c = true];
            if(DefaultsList!= null && DefaultsList.size()>0){
                for(TeamCaseJunction__c tch:DefaultsList){
                    if(!defaltTCJSet.contains(tch.id))
                    {
                        tch.Default__c = false;
                        toUpdatelist.add(tch);
                    }
                }
            }
        }
        return toUpdatelist;
        
    }

}