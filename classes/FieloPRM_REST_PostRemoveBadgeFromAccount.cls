/********************************************************************************************************
    Author: Fielo Team
    Date: 27/03/2015
    Description: 
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestPostRemoveBadgeFromAccount/*') 
global class FieloPRM_REST_PostRemoveBadgeFromAccount{  

    /**
    * [postRemoveBadgeFromAccount]
    * @method   postRemoveBadgeFromAccount
    * @Pre-conditions  
    * @Post-conditions 
    * @param    []    []
    * @return   []    []
    */
    @HttpPost
    global static String postRemoveBadgeFromAccount(String type, Map<String ,List<String>> accToBadges){    
        
        return FieloPRM_UTILS_BadgeAccount.removeBadgeFromAccount(type, accToBadges);        
    }  
   
}