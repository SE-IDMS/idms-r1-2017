/* 
    Author: Yannick TISSERAND (Accenture)
    Created date: 02/04/2012
    Description: Apex class containing method for PRM opportunity re-assignment to Agent/Partner
    5-Mar-2013  Srinivas Nallapati  PRM Mar13   BR-2910    
*/   
public without sharing class AP48_OpportunityPartnerAssignment 
{
  public static Map<ID, Contact> createContactMap(){
    Map<ID, Contact> contactMap = new Map<ID, Contact>();
    String currentUserId = userInfo.getUserId();
    
    // Retrieve all the accounts for which the current user is the owner
    List<Account> accounts = [select id from Account where isPartner = true and ownerId =:currentUserId];
    // Retrieve all the accounts for which the current user is part of the account team with read/Write access
    List<AccountTeamMember> accountTeams = [select id, accountId from AccountTeamMember where isDeleted = false and userId =:currentUserId];
       
    // Consolidating the lit of Accounts    
    if(accountTeams.size() > 0){
      for(AccountTeamMember accountTeam:accountTeams){
          System.Debug('**Account=' + accountTeam.accountId);
        Account account = new Account(id=accountTeam.accountId);
        accounts.add(account);
      }
    }
    
    // Retrieve all the contacts related to these accounts   
    List<Contact> contacts  = new List<COntact>();
    if(!(accounts.isEmpty()))
    contacts = [select id from Contact where AccountId in :accounts];
    
    // Building the contact's map
    for(Contact contact:contacts){
      if(!contactMap.containsKey(contact.id)){
        System.Debug('**Contact=' + contact.id);
        contactMap.put(contact.id, contact);
      }
    }
    
    return contactMap;
  }
  //Srinivas PRM Mar13  BR-2910 Added LoggedInUser  to query
  public static Map<ID, User> createUserMap(List<Opportunity> Opportunities){
    Set<ID> ownerIds = new Set<ID>();
    Map<ID, User> userMap = new Map<ID, User>();
    
    for(Opportunity opp:Opportunities){
    
      if(!ownerIds.contains(opp.ownerId)){
        ownerIds.add(opp.ownerId);
      }
    }
    
    List<User> users  = [select id, IsPortalEnabled, contactId, profileId, PartnerORFConverter__c from User where id in :ownerIds or id=:UserInfo.getUserId()];
        
    for(User user:users){
    
      if(!userMap.containsKey(user.id)){
        System.Debug('**User=' + user.id);
        userMap.put(user.id, user);
      }
    }
    
    return userMap;
    
  }
}