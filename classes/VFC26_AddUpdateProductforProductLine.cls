/*
    Author          : Accenture Team
    Date Created    : 18/05/2011 
    Description     : Controller extensions for VFP26_AddUpdateProductforProductLine.This class fetches the search 
                      parameters from the component controller, performs the search by calling factory methods  
*/
public with sharing class  VFC26_AddUpdateProductforProductLine extends VFC_ControllerBase
{
    //Iniatialize variables
    private OPP_ProductLine__c currentProductLine = new OPP_ProductLine__c();
    private Opportunity opp = new Opportunity();  
    private Map<OPP_ProductLine__c, DataTemplate__c> updatePL = new Map<OPP_ProductLine__c, DataTemplate__c>();  
    private Map<Opportunity, DataTemplate__c> insertPL = new Map<Opportunity, DataTemplate__c>();          
    private List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();    
    public List<String> filters{get; set;}    
    public Utils_DataSource dataSource;
    public List<SelectOption> columns{get;set;}
    public List<DataTemplate__c> fetchedRecords{get;set;}
    public String interfaceType{get;set;}
    public String searchKeyword{get; set;}
    public List<String> keywords;
    public List<String> picklistsValues{get;set;}
    public Boolean DisplayResults{get;set;}
    private List<OPP_Product__c> products = new List<OPP_Product__c>();
    private OPP_Product__c product = new OPP_Product__c();
    public Utils_DataSource.Result searchResult ;
    public VCC05_DisplaySearchFields searchController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC05_DisplaySearchFields displaySearchFields;
                displaySearchFields = (VCC05_DisplaySearchFields)getcomponentControllerMap().get('searchComponent');
                system.debug('--------displaySearchFields-------'+displaySearchFields );
                if(displaySearchFields!= null)
                return displaySearchFields;
            } 
            return new VCC05_DisplaySearchFields();
        }
    } 
    public VCC06_DisplaySearchResults resultsController 
    {
        set;
        get
        {
            if(getcomponentControllerMap()!=null)
            {
                VCC06_DisplaySearchResults displaySearchResults;
                displaySearchResults = (VCC06_DisplaySearchResults)getcomponentControllerMap().get('resultComponent');
                system.debug('--------displaySearchResults -------'+displaySearchResults );                
                if(displaySearchResults!= null)
                return displaySearchResults;
            }  
            return new VCC06_DisplaySearchResults();
        }
    }
     //Constructor
    public VFC26_AddUpdateProductforProductLine(ApexPages.StandardController controller) 
    {
        System.Debug('****** Initializing the Properties and Variables Begins for VFC26_AddUpdateProductforProductLine******');            
        currentProductLine = (OPP_ProductLine__c)controller.getRecord();
        keywords = new List<String>();
        filters = new List<String>();
        columns = new List<SelectOption>();
        interfaceType = 'GMR';

        if(currentProductLine.Id!=null)
        {  
          prodLines = [Select Id, TECH_CommercialReference__c, Opportunity__c,ProductBU__c, ProductLine__c, ProductFamily__c, Family__c from OPP_ProductLine__c where Id=:currentProductLine.Id LIMIT 1];
          OPP_ProductLine__c prodLine = [Select Id, TECH_CommercialReference__c, Opportunity__c,ProductBU__c, ProductLine__c, ProductFamily__c, Family__c from OPP_ProductLine__c where Id=:currentProductLine.Id LIMIT 1];
        
          if(prodLine !=null)
          {
                filters.add(prodLine.ProductBU__c);
                filters.add(prodLine.ProductLine__c);
                filters.add(prodLine.ProductFamily__c);
                filters.add(prodLine.Family__c); 
                
                if(prodLine.TECH_CommercialReference__c!=null)
                    searchKeyword = prodLine.TECH_CommercialReference__c;
                if(searchKeyword != null)  
                    keywords = searchKeyword.split(' ');                                               
           }
        }
        
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
        
        dataSource = Utils_Factory.getDataSource(interfaceType );
        // columns = dataSource.getColumns();
        Columns = new List<SelectOption>();
        Columns.add(new SelectOption('Field8__c',Label.CL00374)); // First Column
        Columns.add(new SelectOption('Field9__c',Label.CL00375));
        Columns.add(new SelectOption('Field4__c',Label.CL00354));
        Columns.add(new SelectOption('Field3__c',Label.CL00353));
        Columns.add(new SelectOption('Field2__c',Label.CL00352));
        Columns.add(new SelectOption('Field1__c',Label.CL00351)); // Last Column
        fetchedRecords = new List<DataTemplate__c>();
        System.Debug('****** Initializing the Properties and Variables Ends for VFC26_AddUpdateProductforProductLine******');
    }
    
    public PageReference  search() 
    {     
    /* This method will be called on click of "Search" Button.This method calls the factory methods
     * and fetches the search results
     */
       System.Debug('****** Searching Begins  for VFC25_AddUpdateProductforCase******');
        if(resultsController.searchResults!=null)
            resultsController.searchResults.clear();

        fetchedRecords.clear();
        keywords = new List<String>();
        filters = new List<String>();
        if(searchController.searchText!=null)
            searchKeyword = searchController.searchText.trim();
        if(searchKeyword != null)
            keywords = searchKeyword.split('\\s');
            if(searchController.level1!=null)            
        filters.add(searchController.level1);
            if(searchController.level2!=null)
        filters.add(searchController.level2);
            if(searchController.level2!=null && searchController.level3!=null)
        filters.add(searchController.level3);
            if(searchController.level2!=null && searchController.level3!=null && searchController.level4!=null)
        filters.add(searchController.level4); 
        system.debug('----filters-----'+filters);
        system.debug('----keywords -----'+keywords );  
        
            if(((!filters.ISEmpty()) && filters[0] != Label.CL00355) || ((!keywords.ISEmpty())&& keywords[0].length()>0))
            {
                try
                {
                    System.debug('#### Filters : ' + filters);
                    //Makes the webservice call for search                    
                    searchResult = dataSource.Search(keywords,filters);
                    fetchedRecords = searchResult.recordList;
                    System.debug('Data source Result'+fetchedRecords);
                       
                    DisplayResults = true;
                     if(searchResult.NumberOfRecord>=Utils_GMRDataSource.GMR_RECORD_PER_PAGE && fetchedRecords.size()>1)
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CL00349));
                }
                catch(Exception exc)
                {
                    system.debug('Exception while calling GMR WS '+ exc.getMessage() );
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.error,Label.CL00398));
                }                
            }
            else if(filters[0] == Label.CL00355 && keywords[0].length()==0)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CL00347));

            }             
        
        System.Debug('****** Searching Ends  for VFC25_AddUpdateProductforCase******');        
        return null;          
    }
    /* This method will be called on click of "Clear" Button.This method clears the value in the search text box
     * and the values in picklists
     */
    public PageReference clear() 
    {
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Begins  for VFC26_AddUpdateProductforProductLine******');     
        fetchedRecords = new List<DataTemplate__c>();
        searchKeyword = null;
        keywords = new List<String>();
        filters = new List<String>();        
        searchController.init();
        DisplayResults = false;
        System.Debug('****** clearing the value in the search text box,picklists & Search Text Ends for VFC26_AddUpdateProductforProductLine******');     
        return null;
    }
    /* This method will be called from the component controller on click of "Select" link.This method correspondingly inserts/updates
     * the product line.
     */
    public override pagereference PerformAction(sObject obj, VFC_ControllerBase controllerBase)
    {
        System.Debug('****** Insertion/Updation of Product Line Begins******');
        pagereference pg;
        Id recordId;
        Boolean flag;
        VFC26_AddUpdateProductforProductLine thisController = (VFC26_AddUpdateProductforProductLine)controllerBase;
        system.debug('currentprodlineID: '+currentProductLine.Id);
        if(currentProductLine.Id ==null)
        {
            if(system.currentpagereference().getParameters().get(System.Label.CL00334)!=null)
            {
                opp = [select Id, CurrencyISOCode from Opportunity where Id=:system.currentpagereference().getParameters().get(System.Label.CL00334)];
                system.debug('------oppId-----'+opp);
                
                //added to display proper error message to user instead of stack trace
                Savepoint sp = Database.setSavepoint();
                Database.saveResult svr = Database.update(opp,false);
                if(!svr.isSuccess())
                {       
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,svr.getErrors()[0].getMessage()));                 
                        return null; 
                }
                Database.rollback(sp);   
                //till here
                
                insertPL.put(opp,(dataTemplate__c)obj);
            }
            else{
                system.debug('currentprodlineID: '+currentProductLine.Id);
                //Modif JFA - Save&New
                String parentOppID = '';
                String parentObjID = system.currentpagereference().getParameters().get('retURL'); //fetch the related object id from the URL
                system.debug('#### parentObjID: ' + parentObjID );
                if (parentObjID != null)
                    parentObjID = system.currentpagereference().getParameters().get('retURL').replace('/',''); // delete the "/"
                List<OPP_ProductLine__c> PLList = [SELECT ID, Opportunity__c FROM OPP_ProductLine__c WHERE ID=: parentObjID LIMIT 1];
                if(PLList.size()==1){ // if related object is an order, fetch its related opportunity
                    parentOppID = PLList[0].Opportunity__c;
                }
                else{
                    List<Opportunity> OppList = [SELECT ID, OwnerID FROM Opportunity WHERE ID=:parentObjID LIMIT 1];
                    if(OppList.size()==1){    // if related object is an Opportunity assign it directly
                        parentOppID = OppList[0].ID;
                    } 
                }
                system.debug('#### parentOppID : ' + parentOppID );
                if(parentOppID != ''){
                    opp = [select Id, CurrencyISOCode from Opportunity where Id=: parentOppID];
                    
                    //added to display proper error message to user instead of stack trace
                    Savepoint sp = Database.setSavepoint();
                    Database.saveResult svr = Database.update(opp,false);
                    if(!svr.isSuccess())
                    {       
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,svr.getErrors()[0].getMessage()));                 
                        return null; 
                    }
                    Database.rollback(sp);   
                    //till here
                    
                    insertPL.put(opp,(dataTemplate__c)obj); 
                }            
            }
            List<String> prodLineIds ;
            //Inserts Product Line
            if(insertPL.size()>0)
            {
                try
                {                                        
                    prodLineIds = AP15_ProductLineIntegrationMethods.InsertProductLine(insertPL);        
                }
                catch(DMLException e)
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00414));
                    pg = null;
                }
                system.debug('------prodLineIds-----'+prodLineIds );
                if(prodLineIds!=null && prodLineIds.size()==1)
                recordId = prodLineIds[0];
                if(recordId!=null)
                pg = new pagereference('/'+recordId+'/e?retURL=%2F'+recordId) ; 
            }
        }
        else
        {
            if(thisController.prodLines.size() == 1)
                updatePL.put(thisController.prodLines[0], (dataTemplate__c)obj);
            //Updates Product Line
            if(updatePL.size()>0)
            {
                system.debug('-----In Update -----');
                try
                {
                    AP15_ProductLineIntegrationMethods.UpdatePL(updatePL);   
                }
                catch(DMLException e)
                {
                    flag = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00414));
                    pg = null;
                }
            }
            if(thisController.prodLines.size() == 1)
                recordId = thisController.prodLines[0].Id;
            if(recordId!=null && flag != true)
                pg = new pagereference('/'+recordId+'/e?retURL=%2F'+recordId) ;                    
        }
        System.Debug('****** Insertion/Updation of Product Line Ends******');
        return pg;
    }
    public pagereference cancel()
    {
        System.Debug('****** Cancel Method Begins  for  VFC26_AddUpdateProductforProductLine******');
        pagereference pg;
        if(currentProductLine.Id !=null)
        pg = new pagereference('/'+currentProductLine.Id);
        else if(system.currentpagereference().getParameters().get(System.Label.CL00334)!=null)
        pg = new pagereference('/'+system.currentpagereference().getParameters().get(System.Label.CL00334));
        else if (system.currentpagereference().getParameters().get('retURL')!=null)
        pg = new pagereference(system.currentpagereference().getParameters().get('retURL'));
        system.debug('------Page reference -----'+pg);
        System.Debug('****** Cancel Method Ends for  VFC26_AddUpdateProductforProductLine******');
        if(pg!=null)
            return pg;
        else
            return null;
    }           
}