/* DESCRIPTION: UPDATES OPEN OPPORTUNITY LINES OF THE SERIES */
public with sharing class VFC_MassUpdateOLsAgr
{
    public List<OPP_ProductLine__c> prodLineslst= new List<OPP_ProductLine__c>();     
    public OPP_ProductLine__c selectedoppline {get;set;} 
    public string agreementId;
    public Id oppId;
    public List<OPP_Product__c> products = new List<OPP_Product__c>();
    public List<Opportunity> opptyList = new List<Opportunity>();
    string toBeDel = Label.CL00139, lost = Label.CLSEP12SLS14 , cancelled = Label.CL00326;    
    PageReference currPage = new PageReference('/apex/VFP_MassUpdateOLsAgr');
    private static Savepoint sp;                
    public string prodLine{get;set;}
    public string selProdBU{get;set;}
    public string selStatus{get;set;}
    public string pgMsg{get;set;}
    public List<ProductLine> productLines {get;set;}
    public boolean editLine{get;set;}
    public string severity{get;set;}
    public OPPAgreement__c agreement{get;set;}
    public boolean renderAddLine{get;set;}
    public List<Opp_ProductLine__c> newProductLines{get;set;}
    List<Opportunity> opportunityList= new List<Opportunity>();
    public boolean displayAddError{get;set;}
    //CONSTRUCTOR TO QUERY AGREEMENT, OPPORUTNITY & PRODUCT
    public VFC_MassUpdateOLsAgr(ApexPages.StandardSetController controller)
    {
        agreementId = system.currentPagereference().getparameters().get('id');   
        productLines= new List<ProductLine>();          
        agreement = [select Id, Name from OPPAgreement__c where Id=:agreementId];
        opportunityList = [SELECT ID,StageName,SolutionCtr__c,CloseDate FROM Opportunity WHERE AgreementReference__c =: agreementId ORDER BY CreatedDate];

        if(opportunityList.size()>0)
        {
            for(Opportunity opp: opportunityList)
            {
                if(opp.StageName!=Label.CL00272 && oppId==null)
                    oppId = opp.Id;                
            }
            if(system.currentPagereference().getparameters().get('selectedopplne')!=null)
                selectedoppline = [SELECT ID,Name,LineStatus__c,Amount__c,Quantity__c,LineClosedate__c,Lowestlevelvalue__c,TECH_OriginalId__c, ProductBU__c,ProductLine__c,ProductFamily__c,Family__c,TECH_CommercialReference__c, Opportunity__c FROM OPP_ProductLine__c WHERE Id=: system.currentPagereference().getparameters().get('selectedopplne')];                
            
            //EXECUTED FOR ADD PRODUCT
            if(system.currentPagereference().getparameters().get('addProduct')!=null)
            {
                renderAddLine = true;
                newProductLines = new List<Opp_ProductLine__c>();
                if(system.currentPagereference().getparameters().get('addProduct')!=null)
                {
                    if(system.currentPagereference().getparameters().get('selectedopp')!=null)
                        newProductLines = [SELECT ID,Name,ParentLine__c,Product__c,TECH_IsCreatedFromAgr__c,LineStatus__c,Amount__c,Quantity__c,LineClosedate__c,Lowestlevelvalue__c,TECH_OriginalId__c, ProductBU__c,ProductLine__c,ProductFamily__c,Family__c,TECH_CommercialReference__c, Opportunity__c FROM OPP_ProductLine__c WHERE Opportunity__c=: system.currentPagereference().getparameters().get('selectedopp') and TECH_IsCreatedFromAgr__c=:true and CreatedById=:UserInfo.getUserId() ];
                }                
            }   
                            
        } 
        else
        {
            pgMsg = Label.CLAPR15SLS62;
            severity = 'Error';            
        }
        
    }
    
    //ADDS NEW PRODUCT LINES TO ALL OPPORTUNITIES
    public void addProductLines()
    {
            displayAddError = false;
            List<OPP_ProductLine__c> prodLinestoUpdate = new List<OPP_ProductLine__c>();
            List<OPP_ProductLine__c> errProdLines= new List<OPP_ProductLine__c>();
            Savepoint savepoint = database.setsavepoint();               
            for(OPP_ProductLine__c op: newProductLines)
            {
                Opp_ProductLine__c newPL = op;
                newPL.TECH_IsCreatedFromAgr__c = false;
                
                for(Opportunity opp:opportunityList)
                {
                    if(opp.StageName!=Label.CL00272)
                    {
                        if(system.currentPagereference().getparameters().get('selectedopp')!=opp.Id)
                        {
                            OPP_ProductLine__c newLine = new OPP_ProductLine__c();
                            newLine = newPL.clone();                                
                            newLine.Opportunity__c = opp.Id;
                            newLine.LineCloseDate__c = opp.CloseDate;                
                            prodLinestoUpdate.add(newLine);                      
                        }
                    }
                }
                prodLinestoUpdate.add(newPL);                  
            }        
            Database.upsertResult[] prodLineSaveRes = Database.upsert(prodLinestoUpdate, false);
            for(Database.upsertResult sr: prodLineSaveRes)
            {
                if(!sr.isSuccess())
                {
                    Database.Error err = sr.getErrors()[0];
                    pgMsg = Label.CLOCT15SLS71 + err.getMessage();
                    severity = 'error';
                    displayAddError = true;
                    System.Debug('######## VFC_MassUpdateOLsAgr Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                    //Rolls back in case of any error
                    database.rollback(savepoint);

                    //Updates Opportunity Line status to "To be deleted" as Opportunity amount needs to be deleted            
                    for(OPP_ProductLine__c op: newProductLines)
                    {
                        Opp_ProductLine__c newPL = op;
                        newPL.LineStatus__c = Label.CL00139;
                        errProdLines.add(newPL);
                    }
                    //updates the Opportunity line & delete Opportunity line
                    update errProdLines;
                    delete errProdLines;
                }    
            }
                                
    }
    
    //UPDATES THE RELATED OPPORTUNITY LINES WITH THE UPDATED CHANGED PRODUCT
    public void updateRelatedOpptyLines()
    {
        if(system.currentPagereference().getparameters().get('changeProduct')!=null)
        {
            if(system.currentPagereference().getparameters().get('selectedopplne')!=null)
                selectedoppline = [SELECT ID,Name,Product__c,LineStatus__c,Amount__c,Quantity__c,LineClosedate__c,Lowestlevelvalue__c,TECH_OriginalId__c, ProductBU__c,ProductLine__c,ProductFamily__c,Family__c,TECH_CommercialReference__c, Opportunity__c FROM OPP_ProductLine__c WHERE Id=: system.currentPagereference().getparameters().get('selectedopplne')];
            if(selectedoppline.Id!=null)
            {
                String selectedopplineId1 = String.valueOf(selectedoppline.Id).substring(0,(String.valueOf(selectedoppline.Id).length()-3));
        
                String querystring = 'SELECT ID,Name,LineStatus__c,Amount__c,Product__c,Quantity__c,LineClosedate__c,Lowestlevelvalue__c,TECH_OriginalId__c, ProductBU__c,ProductLine__c,ProductFamily__c,Family__c,TECH_CommercialReference__c, Opportunity__c,Opportunity__r.AgreementReference__c FROM OPP_ProductLine__c WHERE TECH_OriginalId__c=: selectedopplineId1 and Opportunity__r.AgreementReference__c =: agreementId';
            
                List<OPP_ProductLine__c> prodLinestoUpdate = Database.query(querystring);
                List<OPP_ProductLine__c> prodLinesUpdates = new List<OPP_ProductLine__c>();
        
                for(OPP_ProductLine__c ol: prodLinestoUpdate)
                {
                    OPP_ProductLine__c newLine = new OPP_ProductLine__c();
                    newLine.ProductBU__c=selectedoppline.ProductBU__c;
                    newLine.ProductLine__c=selectedoppline.ProductLine__c;        
                    newLine.ProductFamily__c=selectedoppline.ProductFamily__c;
                    newLine.Family__c=selectedoppline.Family__c;  
                    newLine.Product__c=selectedoppline.Product__c;
                    newLine.Id = ol.Id;
                    prodLinesUpdates.add(newLine);            
                }
                
                Database.SaveResult[] prodLineSaveRes = Database.update(prodLinesUpdates, false);
                for(Database.SaveResult sr: prodLineSaveRes)
                {
                    if(!sr.isSuccess())
                    {
                        Database.Error err = sr.getErrors()[0];
                        pgMsg = err.getMessage();
                        System.Debug('######## AP04 Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                    }    
                }            
            }
            if(pgMsg!=null)
            {
                ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Info,pgMsg);        
                ApexPages.addMessage(myMsg2);
                severity='error';
                
            }
        }
    }
    //WRAPPER CLASS TO DISPLAY OPPORTUNITY LINE 
    public class ProductLine
    {
        public OPP_ProductLine__c prodLine{get;set;}
        public boolean selected{get;set;}        
        public ProductLine(OPP_ProductLine__c prdLine) 
        {
            prodLine= prdLine;
            selected = false;
        }
    }
    //DISPLAYS LIST OF PRODUCT BUS
    public List<SelectOption> getprodBUS() 
    {   
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(Label.CLAPR15SLS52,Label.CLAPR15SLS52));
        set<String> prodLines = new Set<String>();
        for(OPP_Product__c pb: [SELECT Id, BusinessUnit__c FROM OPP_Product__c WHERE ProductLine__c = NULL AND BusinessUnit__c != NULL ORDER BY BusinessUnit__c])
        {
            if(!(prodLines.contains((pb.BusinessUnit__c))))
            {
                options.add(new SelectOption(pb.BusinessUnit__c,pb.BusinessUnit__c));
                 prodLines.add(pb.BusinessUnit__c);
            }
        }        
        return options;
    }
    
    //DISPLAYS LIST OF LINE STATUS
    public List<SelectOption> getLineStatus() 
    {   
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(Label.CLAPR15SLS52,Label.CLAPR15SLS52));
        Schema.DescribeFieldResult fieldRes = OPP_ProductLine__c.LineStatus__c.getDescribe();
        List<Schema.PicklistEntry> pickEntry = fieldRes.getPicklistValues();  
        for(Schema.PicklistEntry pe: pickEntry)    
        {
            options.add(new SelectOption(pe.getLabel(), pe.getLabel()));
        }
        return options;
    }
    
    //DISPLAYS THE DETAILS OF SELECTED OPPORTUNITY LINE
    public void NextPage() 
    {
        boolean checkSel = false;
        pageMessages();
        pgMsg = null;
        for(ProductLine pl : productLines) 
        {
            if(pl.selected == true) 
            {
                if(checksel == false)
                {
                    checksel = true;
                    selectedoppline = (pl.prodLine);                                 
                }
                else
                {
                    pgMsg = Label.CLAPR15SLS59;
                    severity = 'Error';
                    selectedoppline = null;                    
                }
            } 
        }
        if(selectedoppline == null && pgMsg==null)
        {
            pgMsg = Label.CLAPR15SLS69;
            severity = 'Error';
        }
    }
    //DISPLAYS PAGE MESSAGES
    public void pageMessages()
    {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info,Label.CLAPR15SLS57);
        ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Info,Label.CLAPR15SLS56);        
        ApexPages.Message myMsg3 = new ApexPages.Message(ApexPages.Severity.Info,Label.CLOCT15SLS03);
        ApexPages.addMessage(myMsg);
        ApexPages.addMessage(myMsg2); 
        ApexPages.addMessage(myMsg3); 
    }
    //SEARCHES FOR THE OPPORTUNITY LINE WITH THE CORRESPONDING FILTER CRITERIA
    public pagereference search()
    {        
        if(system.currentPagereference().getparameters().get('addProduct')==null)
        {
        //UPDATE THE OPPORTUNITY LINES IF THE PRODUCT IS CHANGED
        updateRelatedOpptyLines();
        
        string queryString,whereclause;   
        productLines= new List<ProductLine>();
        pgMsg = null;  
        pageMessages();   
        if(oppId!=null)
        {
            querystring = 'SELECT ID,Name,LineStatus__c,Amount__c,Quantity__c,LineClosedate__c,Lowestlevelvalue__c,ProductBU__c,ProductLine__c,ProductFamily__c,Family__c,TECH_CommercialReference__c, Opportunity__c FROM OPP_ProductLine__c ';
    
            whereclause = ' WHERE TECH_OriginalId__c !=null'; 
            
            
            if((selProdBU == 'No Filter' || selProdBU == null) && (prodLine == null || prodLine == '')) 
            {
                System.debug('$$$$$ both null');
                whereclause = whereclause + ' AND Opportunity__r.Id =: oppId';    
            }
            else if(selProdBU != 'No Filter' && (prodLine == null || prodLine == '')) 
            {
                System.debug('$$$$$ filter');
                whereclause = whereclause +' AND Opportunity__r.Id =: oppId AND ProductBU__c =: selProdBU ';            
            }
            else if(selProdBU == 'No Filter' && (prodLine != null || prodLine != '')) 
            {
                System.debug('$$$$$ prod line');
                whereclause = whereclause + ' AND Opportunity__r.Id =: oppId AND ProductLine__c LIKE \'%'+Utils_Methods.escapeForWhere(prodLine)+'%\'';     
            }
            else if(selProdBU != 'No Filter' && (prodLine != null || prodLine != '')) 
            {
                System.debug('$$$$$ filter and prod line');
                whereclause = whereclause + ' AND Opportunity__r.Id =: oppId AND ProductLine__c LIKE \'%'+Utils_Methods.escapeForWhere(prodLine)+'%\' AND ProductBU__c =: selProdBU ';
            } 
            if(selStatus!=null && selStatus!='No Filter')
                whereclause = whereclause + ' AND LineStatus__c =: selStatus';
         
            whereclause = whereclause + ' limit '+Label.CLAPR15SLS65;
            querystring = querystring + whereclause ; 
            system.debug(querystring +'$$$$ Query String');
            prodLineslst = Database.query(querystring);
            for(OPP_ProductLine__c singleOL : prodLineslst)
            {
                productLines.add(new ProductLine(singleOL)); 
            }
            if(productLines.size() == 0)
            {
                pgMsg = Label.CL00013;
                severity = 'info';
            }
        }
        else
        {
            pgMsg = Label.CL00013;
            severity = 'info';
        }
        }
        else
        {
            addProductLines();
        }
        if(pgMsg==null && system.currentPagereference().getparameters().get('addProduct')!=null)
            return new pagereference('/'+ agreementId);
        else
            return null; 
        
        
    }
    //CLEARS THE SEARCH CRITERIA
    public pagereference clearSearch()
    {
        currPage.getParameters().put('id',agreementId);
        currPage.setRedirect(true);
        return currPage;
    }
    //UPDATES THE OPEN OPPORTUNITY LINES RELATED TO THE AGREEMENT
    public pagereference save()
    {
        String querystring, whereclause,selectedopplineId;
        pgMsg = null;
        pageMessages();
        if(selectedoppline.Id!=null)
        {
            selectedopplineId = String.valueOf(selectedoppline.Id).substring(0,(String.valueOf(selectedoppline.Id).length()-3));
    
            querystring = 'SELECT ID,Name,LineStatus__c,Amount__c,Quantity__c,LineClosedate__c,Lowestlevelvalue__c,TECH_OriginalId__c, ProductBU__c,ProductLine__c,ProductFamily__c,Family__c,TECH_CommercialReference__c, Opportunity__c,Opportunity__r.AgreementReference__c FROM OPP_ProductLine__c WHERE TECH_OriginalId__c=: selectedopplineId and Opportunity__r.AgreementReference__c =: agreementId';
        
            List<OPP_ProductLine__c> prodLinestoUpdate = Database.query(querystring);
            List<OPP_ProductLine__c> prodLinesUpdates = new List<OPP_ProductLine__c>();
    
            for(OPP_ProductLine__c ol: prodLinestoUpdate)
            {
                OPP_ProductLine__c newLine = new OPP_ProductLine__c();
                newLine.Id = ol.Id;
                newLine.LineStatus__c = selectedoppline.LineStatus__c;
                newLine.Amount__c = selectedoppline.Amount__c;
                newLine.Quantity__c = selectedoppline.Quantity__c;
                newLine.LineClosedate__c = selectedoppline.LineClosedate__c;
                prodLinesUpdates.add(newLine);            
            }
            sp = Database.setSavepoint(); 
            Database.SaveResult[] prodLineSaveRes = Database.update(prodLinesUpdates, false);
            for(Database.SaveResult sr: prodLineSaveRes)
            {
                if(!sr.isSuccess())
                {
                    Database.Error err = sr.getErrors()[0];
                    pgMsg = err.getMessage();
                    System.Debug('######## AP04 Error Inserting: '+err.getStatusCode()+' '+err.getMessage());
                }    
            }            
        }
        if(pgMsg!=null)
        {
            ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.Info,pgMsg);        
            ApexPages.addMessage(myMsg2);
            severity='error';
            Database.rollback(sp);
            return null;            
        }
        else
        return new pagereference('/'+ agreementId);
    }
    //REDIRECTS BACK TO THE AGREEMENT PGAE
    public pagereference cancel()
    {
        return new pagereference('/'+agreementId);
    }
    //ALLOWS TO EDIT THE SELECTED OPPORUTNITY LINE
    public pagereference editProductLine()
    {
        editLine = true;
        pageMessages();
        return null;
    }
    //REDIRECTS TO GMR SEARCH PAGE TO CHANGE THE PRODUCT    
    public pagereference ChangeProduct()
    {
        PageReference changeProduct= new PageReference('/apex/VFP_AddUpdateProductsForOpportunity?mode=change&sourceType=agreement&id='+selectedoppline.Id+'&agreementId='+agreementId);
        changeProduct.getParameters().put('AgreementID', agreementID);
        return changeProduct;
    }
    //RECIRECTS TO GMR SEARCH PAGE TO ADD THE PRODUCT
    public pagereference AddProduct()
    { 
        PageReference addProduct= new PageReference('/apex/VFP_AddUpdateProductsForOpportunity?mode=addProdFromAgreement&sourceType=agreement&agreementId='+agreementId+'&id='+oppId);
        return addProduct;
    }
    
    
}