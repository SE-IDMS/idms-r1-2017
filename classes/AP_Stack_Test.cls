/**
Ap_stack
****/

@isTest
Public class AP_Stack_Test {

    static Testmethod void  complaintRequest() {

         // Create Country
        Country__c CountryObjLCR = Utils_TestMethods.createCountry();
       
        Insert CountryObjLCR;
         // Create User
        User LCRTestUser = Utils_TestMethods.createStandardUser('LCROwner');
        Insert LCRTestUser ;

        BusinessRiskEscalationEntity__c lCROrganzObj01 = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1');
        insert lCROrganzObj01; 
        BusinessRiskEscalationEntity__c lCROrganzObj02 = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1', SubEntity__c='LCR Sub-Entity');
        insert lCROrganzObj02; 
        BusinessRiskEscalationEntity__c lCROrganzObj03 = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1', SubEntity__c='LCR Sub-Entity1');
        insert lCROrganzObj03; 
         // Create Organization (Complaint request)
         List<BusinessRiskEscalationEntity__c> breeList=new List<BusinessRiskEscalationEntity__c>();
         BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(Name='LCR TEST',Entity__c='LCR Entity-1', SubEntity__c='LCR Sub-Entity',Location__c='LCR Location',Location_Type__c= 'Return Center',Street__c = 'LCR Street',RCPOBox__c = '123456',RCCountry__c = CountryObjLCR.Id,RCCity__c = 'LCR City',RCZipCode__c = '500055',ContactEmail__c ='SCH@schneider.com');
        breeList.add(lCROrganzObj); 
         BusinessRiskEscalationEntity__c lCROrganzObj1 = new BusinessRiskEscalationEntity__c(Name='LCR TEST Repair', Entity__c='LCR Entity-1', SubEntity__c='LCR Sub-Entity1', Location__c='LCR Location 1', Location_Type__c= 'LCR', Street__c = 'LCR Street1', RCPOBox__c = '123456', RCCountry__c = CountryObjLCR.Id, RCCity__c = 'LCR City1', RCZipCode__c = '500225', ContactEmail__c ='SCH1@schneider.com');
        breeList.add(lCROrganzObj1); 
        insert breeList; 
        
        List<EntityStakeholder__c> lcrshList=new List<EntityStakeholder__c>();
        EntityStakeholder__c  LCROrgStakeHold= new EntityStakeholder__c(
                                User__c=LCRTestUser.id,
                                AdditionalInformation__c='TestLCR',
                                Department__c ='TestLCR',
                                BusinessRiskEscalationEntity__c =lCROrganzObj.id,
                                Role__c ='Complaint/Request Owner'
                                
                                ) ;
        lcrshList.add(LCROrgStakeHold); 
         EntityStakeholder__c  LCROrgStakeHold1= new EntityStakeholder__c(
                                User__c=LCRTestUser.id,
                                AdditionalInformation__c='TestLCR',
                                Department__c ='TestLCR',
                                BusinessRiskEscalationEntity__c =lCROrganzObj.id,
                                Role__c ='CS&Q Manager'
                                
                                ) ;
        lcrshList.add(LCROrgStakeHold1); 
        insert lcrshList;
        
        List<CS_ComplaintsRequestsTrackFields__c> custSet =  new list<CS_ComplaintsRequestsTrackFields__c>();
            CS_ComplaintsRequestsTrackFields__c cust1 = new CS_ComplaintsRequestsTrackFields__c(customfieldname__c='Accountable Organization', name='Accountable Organization', customfieldapi__c='AccountableOrganization__c');
            custSet.add(cust1);
            insert custSet;     
            
        system.runas(LCRTestUser) {
        
        List<ComplaintRequest__c> CompRequestObjList=new List<ComplaintRequest__c>();
            ComplaintRequest__c  CompRequestObj1 = new ComplaintRequest__c(
                    Status__c='Open',
                   // Case__c  =CaseObj.id,
                    Category__c ='1 - Pre-Sales Support',
                    reason__c='Marketing Activity Response',
                    AccountableOrganization__c =lCROrganzObj02 .id, 
                    ReportingEntity__c =lCROrganzObj.id
                    );
            CompRequestObjList.add(CompRequestObj1);
            boolean isKeep=true;
              insert CompRequestObjList; 
              Answertostack__c InsertStackNw = new Answertostack__c();

            InsertStackNw.ComplaintRequest__c=CompRequestObjList[0].id;
            InsertStackNw.KeepInvolved__c =true;
            InsertStackNw.Department__c ='strDepartment';
            //InsertStackNw.IssueOwner__c =cRIsueUsrId;
            InsertStackNw.ContactEmailAccountableOrganization__c='h@schneider-electric.com';
            InsertStackNw.Organization__c=lCROrganzObj03.id;
            InsertStackNw.TECH_StackCounter__c=Integer.ValueOf(label.CLOCT15I2P74);
           
            insert InsertStackNw; 
             AP_Stack.keepInvolved=true;
             AP_Stack.addtheOrgToStack(isKeep,CompRequestObjList[0].AccountableOrganization__c,CompRequestObjList[0].ReportingEntity__c,'Ne', new set<id>{CompRequestObjList[0].id},'oldDepart','h@schneider-electric.com',LCRTestUser.id, null);
           // AP_Stack.getStackPosition_1(lCROrganzObj03.id,0);
             AP_Stack.clearStackFromPosition(0);
           
              Utils_SDF_Methodology.log('','');Utils_SDF_Methodology.log('',1);Utils_SDF_Methodology.log(1,'');Utils_SDF_Methodology.log(1,1);Utils_SDF_Methodology.log(1);Utils_SDF_Methodology.log('');Utils_SDF_Methodology.startTimer();Utils_SDF_Methodology.stopTimer();
        Utils_SDF_Methodology.limits();
        Utils_SDF_Methodology myPAD=new Utils_SDF_Methodology();myPAD.getPAD_logs();
        Utils_SDF_Methodology.canTrigger('');
            
            
  }   

 }  
}