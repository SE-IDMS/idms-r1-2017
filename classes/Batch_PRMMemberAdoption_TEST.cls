@isTest
public class Batch_PRMMemberAdoption_TEST{
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	
	static testMethod void testMemberAdoption(){

		FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        
        
                
        FieloEE__Member__c mem = new FieloEE__Member__c(
            FieloEE__FirstName__c = 'test 1',
            FieloEE__LastName__c= 'test 1',
            F_PRM_DesactivateDate__c = Datetime.now().addDays(-10),
            F_PRM_IsActivePRMContact__c=true
        );
        insert mem;

        Test.startTest();
        Batch_PRMMemberAdoption batchMemAdoption = new Batch_PRMMemberAdoption(); 
		batchMemAdoption = new Batch_PRMMemberAdoption(50); 
        Database.executebatch(batchMemAdoption);
        Test.stopTest();

       // Verify member desactivated
       FieloEE__Member__c member = [select id,F_PRM_IsActivePRMContact__c from FieloEE__Member__c where id=:mem.Id];
       System.assertEquals(false,member.F_PRM_IsActivePRMContact__c);

	}
}