/**
Description: This covers VFC_IdmsMyProfileControllerHomeWork class
Release: Sept 2016
**/

@isTest
class VFC_IdmsMyProfileControllerHomeWorkTest{
    //test method for VFC_IdmsMyProfileControllerHomeWork covering success cases
    static testmethod void testMyProfileControllerHomeWork(){
        User userObj1 = new User(alias = 'user', email='user1234123' + '@cognizant.com', 
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London', username='user1234123' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='Test1Pras-Kuld1234',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = '@Work',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',
                                 IDMS_POBox__c = '1111',IDMSIdentityType__c='Email',mobilephone='998765432');
        
        insert userObj1;
        System.assert(userObj1!=null);
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='test123',AppName__c='Idms1');
        insert iDMSApplicationMapping;
        System.assert(iDMSApplicationMapping!=null);
        
        ApexPages.currentPage().getParameters().put('app',iDMSApplicationMapping.Name);
        
        test.Starttest();
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController=new VFC_IdmsMyProfileControllerHomeWork();
            
            vFCController.mobilePhone='sdf';
            
            vFCController.mobileCountryCode='sddad';
            
            vFCController.UPhone='sd';
            
            vFCController.UPhoneCountryCode='sdjn';
            
            vFCController.setpwdFlag=true;
            
            vFCController.pwdresetSuccess=true;
            
            vFCController.isIdentityTypeMobile=true;
            
            
            vFCController.changeEmailMobilePage();
            vFCController.changePassword();
            vFCController.clone();
            vFCController.editEmail();
            vFCController.getUser();
            vFCController.emailMobileValues='123123123';
            vFCController.save();
            vFCController.saveEmail();
            vFCController.updateEmail();
            vFCController.oldpassword='Welcome@1';
            vFCController.newPassword='Welcome@2';
            vFCController.verifyNewPassword='Welcome@2';
            vFCController.updatePassword();
            
            vFCController.emailMobileVal='123@gmai.com';
            vFCController.testClassVar=true;
            vFCController.saveEmail();
            
            vFCController.testClassVar=false;
            vFCController.saveEmail();    
        }
        
        
        
        userObj1.IDMS_User_Context__c='@Home';
        userObj1.IDMSCompanyHeadquarters__c=true;
        userObj1.IDMS_Email_opt_in__c='u';
        userObj1.IDMSAnnualRevenue__c=0;
        
        update userObj1;
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController=new VFC_IdmsMyProfileControllerHomeWork();
            vFCController.emailMobileValues='123123123';
            vFCController.save();
        }
        

        test.Stoptest();
    }
    
    //test method for VFC_IdmsMyProfileControllerHomeWork covering multiple cases
    static testmethod void testMyProfileControllerHomeWork2(){
        User userObj1 = new User(alias = 'user',email='user12341231' + '@cognizant.com', 
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London', Company_Postal_Code__c='12345',username='user12341231' + '@bridge-fo.com',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='Test4Pras-Kuld1234',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = '@Work',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',IDMS_PreferredLanguage__c='None',
                                 IDMS_POBox__c = '1111',IDMSIdentityType__c='Mobile',mobilephone='998765432123456',firstName='Test123412');
        
        insert userObj1;
        System.assert(userObj1!=null);
        
        test.Starttest();   
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController3=new VFC_IdmsMyProfileControllerHomeWork();
            vFCController3.save();
        }
        userObj1.Phone='12345678';
        update userObj1;
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController4=new VFC_IdmsMyProfileControllerHomeWork();
            vFCController4.save();
        }
        userObj1.IDMSWorkPhone__c='12345678987';
        update userObj1;
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController5=new VFC_IdmsMyProfileControllerHomeWork();
            vFCController5.save();
        }
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController2=new VFC_IdmsMyProfileControllerHomeWork();
            
            vFCController2.mobilePhone='sdf';
            
            vFCController2.mobileCountryCode='sddad';
            
            vFCController2.UPhone='sd';
            
            vFCController2.UPhoneCountryCode='sdjn';
            
            vFCController2.testStringval='Success';
            vFCController2.save();
            vFCController2.testStringval='error';
            vFCController2.save();
            vFCController2.testClassVar=true;
            vFCController2.updateEmail();
            vFCController2.oldpassword='';
            vFCController2.updatePassword();
            vFCController2.oldpassword='Welcome@123';
            vFCController2.newPassword='abc';
            vFCController2.updatePassword();
            vFCController2.oldpassword='Welcome@12';
            vFCController2.newPassword='Welcome@123';
            vFCController2.verifyNewPassword='Welcome@1234';
            vFCController2.updatePassword();
        }
        
        userObj1.MobilePhone='@#$%rtyy456';
        update userObj1;
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController6=new VFC_IdmsMyProfileControllerHomeWork();
            vFCController6.save();
        }
        
        userObj1.MobilePhone='98451169161';
        userObj1.countrycode='IN';
        userObj1.statecode='10';
        
        update userObj1;
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController7=new VFC_IdmsMyProfileControllerHomeWork();
            vFCController7.checkedEmailOpt=false;
            vFCController7.save();
        }
        
        userObj1.MobilePhone='98451169161';
        userObj1.countrycode='IN';
        userObj1.statecode='';
        
        update userObj1;
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController7=new VFC_IdmsMyProfileControllerHomeWork();
            vFCController7.save();
        }
        
        userObj1.MobilePhone='98451169161';
        userObj1.countrycode='IN';
        userObj1.statecode='10';
        update userObj1;
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController7=new VFC_IdmsMyProfileControllerHomeWork();
            vFCController7.emailMobileVal='12376786877';
            vFCController7.testClassVar=true;
            vFCController7.saveEmail();
        }
        
        test.Stoptest();
    }
    
    // test method for VFC_IdmsMyProfileControllerHomeWork covering cancel button method
    static testmethod void testMyProfileControllerHomeWork3(){
        
        User userObj1 = new User(alias = 'user',email='user12341231' + '@cognizant.com', 
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true,
                                 timezonesidkey='Europe/London', Company_Postal_Code__c='12345',username='user12341231' + '@bridge-fo.com',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='Test4Pras-Kuld1234',IDMS_Registration_Source__c = 'Test',
                                 IDMS_User_Context__c = '@Work',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',IDMS_PreferredLanguage__c='None',
                                 IDMS_POBox__c = '1111',IDMSIdentityType__c='Mobile',mobilephone='998765432123456',firstName='Test123412');
        
        insert userObj1;
        System.Runas(userObj1){
            VFC_IdmsMyProfileControllerHomeWork vFCController7=new VFC_IdmsMyProfileControllerHomeWork();
            vFCController7.cancelButton();
        }
        
    }
    
    
    
}