@isTest
private class VFC38_AccOrderController_TEST
{    
    static TestMethod void AccOrderControllerTest() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;    

        Opportunity opp1 = Utils_TestMethods.createOpportunity (account1.Id);
        insert opp1;

        OPP_OrderLink__c OrdLk =  Utils_TestMethods.createOrderLink(opp1.Id);
        insert OrdLk;                             

        ApexPages.StandardController AccStandardController = new ApexPages.StandardController(Account1);
        VFC38_AccOrderController AccOrderController = new VFC38_AccOrderController(AccStandardController);
            
        VCC07_OrderConnectorController OrderController = new VCC07_OrderConnectorController();
        OrderController.OrderMethods = AccOrderController.AccountImplementation ;
        
        /* TESTS */         
          
        OrderController.RelatedRecordAttribute = Account1; 
        OrderController.StandardObjectId = Account1.ID;
          
        OrderController.getInit();
        OrderController .SelectedOrder = OrdLk;
        OrderController.SearchOrder();
        if(OrderController.FetchedRecords.size()>0){
            OrderController.PerformAction(OrderController.FetchedRecords[0],OrderController);
            OrderController.ActionNumber=2;
            OrderController.PerformAction(OrderController.FetchedRecords[0],OrderController);               
            OrderController.Action();
        }
        OrderController.SelectedOrder = OrdLk;
        OrderController.Action();        
        OrderController.Cancel();
        OrderController.Clear();
        
        // Profile profile = [select id from profile where name='SE - Salesperson'];        
        // User user = new User(alias = 'user1', email='user' + '@accenture.com', emailencodingkey='UTF-8', 
        // lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = profile.Id, 
        // BypassWF__c = false, timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
        
        Profile profile;
        User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;
        User user;
        System.runAs(sysadmin2){
            UserandPermissionSets useruserandpermissionsetrecord=new UserandPermissionSets('user','SE - Salesperson');
           user = useruserandpermissionsetrecord.userrecord;
            Database.insert(user);

            List<PermissionSetAssignment> permissionsetassignmentlstforuser=new List<PermissionSetAssignment>();    
            for(Id permissionsetId:useruserandpermissionsetrecord.permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforuser.add(new PermissionSetAssignment(AssigneeId=user.id,PermissionSetId=permissionsetId));
            if(permissionsetassignmentlstforuser.size()>0)
            Database.insert(permissionsetassignmentlstforuser);//Database.insert(permissionsetassignmentlstfordelegatedapprover);  
        }

        //AccountTeamMember AccTeamMember = Utils_TestMethods.createAccTeamMember(account1.ID, user.Id);
        system.runAs(user){
            Utils_TestAccessRights.TestRightsOnAccount(account1);
            OrderController.getInit();
            OrderController.SearchOrder();
            OrderController.SelectedOrder = OrdLk;
            OrderController.Action();        
            OrderController.Cancel();
            OrderController.Clear();
        }
        
        Account account2 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', OwnerID = user.id);
        system.runAs(user){
            Utils_TestAccessRights.TestRightsOnAccount(account2);
        }
        
        profile = [select id from profile where name='System Administrator'];        
        user = new User(alias = 'user2', email='user' + '@accenture.com', emailencodingkey='UTF-8', 
        lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = profile.Id, 
        BypassWF__c = false, timezonesidkey='Europe/London', username='user2' + '@bridge-fo.com');
        system.runAs(user){
            Utils_TestAccessRights.TestRightsOnAccount(account1);
        }
    }
}