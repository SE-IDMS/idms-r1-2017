/**
16-Sep-2013
Shruti Karn  
PRM Oct13 Release    
Initial Creation: test class for VFC_PartnerAssessment
 */
@isTest
private class VFC_PartnerAssessment_TEST {
     
     static testMethod void myUnitTest() {
     
          Country__c TestCountry= Utils_TestMethods.createCountry();
          insert TestCountry;
          PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
          globalProgram.TECH_CountriesId__c = TestCountry.id;
          globalPRogram.recordtypeid = Label.CLMAY13PRM15;
          globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
          insert globalProgram;
          
          PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, TestCountry.Id);
          insert countryProgram; 
          list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
          countryPartnerProgram = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
         
          if(!countryPartnerProgram.isEmpty())
          {
            countryPartnerProgram[0].ProgramStatus__C = Label.CLMAY13PRM47;
            update countryPartnerProgram;
          }
          OpportunityRegistrationForm__c ORF1 = new OpportunityRegistrationForm__c( Status__c='Pending', 
                                                AccountName__c= 'Test',
                                                //AccountCountry__c = accountObj.Country__c,
                                                Amount__c = 100000,
                                                ContactEmail__c='Test@test.com',
                                                OpportunityType__c='Standard',
                                                OpportunityScope__c='ENMVP - SECONDARY MV PRODUCTS',
                                                ExpectedOrderDate__c=System.Today(),
                                                ContactFirstName__c='Test',
                                                ContactLastName__c='User',
                                                ProjectName__c='Test Project',
                                                ProductType__c='BDIBS',
                                                PartnerProgram__c=countryPartnerProgram[0].id,
                                                //ownerid=u1.id,
                                                Tech_CheckORFStatusOnSave__c='True');
          Insert ORF1;
           
          PartnerAssessment__c assessment = new PartnerAssessment__c();
          assessment__c assmnt = new assessment__C();
          assmnt.Name = 'Test Assessment';
          insert assmnt;
          ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(assessment);
          ApexPages.currentPage().getParameters().put('orfID',ORF1.Id);
          
          VFC_PartnerAssessment myPageCon1 = new VFC_PartnerAssessment(sdtCon1);
          assessment.assessment__C = assmnt.id;
          insert assessment;
          ApexPages.StandardController sdtCon = new ApexPages.StandardController(assessment);
          ApexPages.currentPage().getParameters().put('retURL',ORF1.Id);
          VFC_PartnerAssessment myPageCon = new VFC_PartnerAssessment(sdtCon);
     }
}