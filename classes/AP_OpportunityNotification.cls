/**
* @author: Nabil ZEGHACHE
* Date Created (YYYY-MM-DD): 2016-08-25
* Test class: 
* Description: This class manages the Business logic on Opportunity Notification object
* **********
* Scenarios:
* **********
* 1.  Set the amount and currency of the interest based on the relevant Interest Amounts records when the country of Destination of the Opportunity Notification is changed
* 
* -----------------------------------------------------------------
*                     MODIFICATION HISTORY
* -----------------------------------------------------------------
* Modified By: Authors Name
* Modified Date: Date
* Description: Brief Description of Change + BR/Hotfix
* -----------------------------------------------------------------
*/

public class AP_OpportunityNotification {
    
    /**
    * @author: Nabil ZEGHACHHE
    * Date Created: 2016-06-16
    * Description: this method receives a list of Opportunity Notifications, retrieves the corresponding Interests and call the method to (re)calculate the interest amounts.
    * @param aListOfOpptyNotif A list of Interest records to be processed.
    * -----------------------------------------------------------------
    *                    MODIFICATION HISTORY
    * -----------------------------------------------------------------
    * Modified By: Authors Name
    * Modified Date: Date
    * 
    * Description: Brief Description of Change + BR/Hotfix
    * -----------------------------------------------------------------
    */
    public static void updateInterestAmountsForOpptyNotifs(List<OpportunityNotification__c> aListOfOpptyNotif) {
        if (aListOfOpptyNotif != null) {
            // Construct the map to store Opportunity Notifications with their IDs. Will be used later on while updating the Interest amounts.
            Map<Id, OpportunityNotification__c> oppNotifMap = new Map<Id, OpportunityNotification__c>();
            for (OpportunityNotification__c currentOppNotif : aListOfOpptyNotif) {
                oppNotifMap.put(currentOppNotif.Id, currentOppNotif);
            }
            
            // Retrieve the list of related Interest for the current list of Opportunity Notifications
            List<InterestOnOpportunityNotif__c> aListOfInterests = [
                SELECT Amount__c,Comments__c,CurrencyIsoCode,Id,Interest__c,Leading_Business_BU__c,Name,OpportunityNotification__c,Type__c 
                FROM InterestOnOpportunityNotif__c 
                WHERE OpportunityNotification__c IN :oppNotifMap.keySet() 
                LIMIT 50000];
            
            // Call the method that will update the Interest amounts for the corresponding Interest records.
            AP_InterestOnOpportunityNotif.updateInterestAmount(aListOfInterests, oppNotifMap);
            update aListOfInterests;
        }
    }
    
    
}