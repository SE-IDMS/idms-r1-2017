/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        : 28-Feb-2012
    Modification Log    : Partner-Region Added on 11-May-2012, Added Action Column on 11-July-2012
    Description         : Controller to display Program Network 
*/

public with sharing class ConnectProgramScopeListController {

    public ConnectProgramScopeListController(ApexPages.StandardController controller) {

    }

public string getPageid(){
return Pageid;
}

public string getprogram(){
return program;
}

// Declarations 

public List<Team_Members__c> Scope = New List<Team_Members__c>();
public string Pageid;
 
public List<ProgramNetwork> prgNetwork = new List<ProgramNetwork>();

public string program;
public string year;
public List<ProgramNetwork> getProgramNetwork(){

  Pageid = ApexPages.currentPage().getParameters().get('Id');
    List<Project_NCP__c> prg = [Select id, Name, Year__c,Global_Functions__c, Global_Business__c, Power_Region__c, GSC_Regions__c, Partner_Region__c, Smart_Cities_Division__c from Project_NCP__c where id = :pageid];
    program = prg[0].Name;
    year = prg[0].Year__c;
    Scope = [SELECT Id, Team_Name__c,Deployment_Leaders__c, Champion_Name__c, Entity__c, GSC_Region__c,Partner_Region__c,Smart_Cities_Division__c from Team_Members__c where Program_Name__c like :program and year__c = :year];
    
    
    // Loop to Check the Scope and the respective Deployment Leaders
    
    // Power Regions Scope
      
    Integer PW;  
    try{  
    if(!(Prg[0].Power_Region__c == null)){  
        for(String PR: Prg[0].Power_Region__c.Split(';')){
           PW = 0;
           for(Team_Members__c Team:Scope){
            if(PR == Team.Entity__c && Team.Team_Name__c == Label.Connect_Power_Region && !(Team.Deployment_Leaders__c == null)) {
                prgNetwork.add(new ProgramNetwork(Team.Id,Team.Team_Name__c,Team.Entity__c,Team.GSC_Region__c, Team.Partner_Region__c, Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                PW = 1;
                } // if Ends 
             } // For Loop Ends
                 If(PW == 0){
                     prgNetwork.add(new ProgramNetwork('',Label.Connect_Power_Region,PR,'','','','',''));
                     } // If Ends
             } // For Loop Ends
            } // If Ends
           } // Try Ends
           catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
           
               
          // Global Functions Scope with atleast one GSC Region
            integer GFn;
          try{
            if((Prg[0].Global_Functions__c.Contains(Label.Connect_GSC)) && (Prg[0].GSC_Regions__c != null)){
            for(String GF: Prg[0].Global_Functions__c.Split(';')){                      
                           
             for(String GSC: Prg[0].GSC_Regions__c.Split(';')){ 
               GFn = 0; 
                 for(Team_Members__c Team:Scope){
                  //for(String T_GSC: Team.GSC_Region__c.Split(';')){
                   if(GF == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Functions  && !(Team.Deployment_Leaders__c == null) && (Team.GSC_Region__c == GSC)) {
                    prgNetwork.add(new ProgramNetwork(Team.Id,Team.Team_Name__c,Team.Entity__c,Team.GSC_Region__c, Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    GFn = 1;
                  } // If Ends
                 }  // For Loop Ends  
               // } // For Loop Ends   
                         
               If(GFn == 0 && GF == Label.Connect_GSC){
                   prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Functions,GF,GSC,'','','',''));
                    } // If Ends
                             
             } // For Loop Ends
             
             if(GFn == 0 && GF != Label.Connect_GSC){
              for(Team_Members__c Team:Scope){
                 if(GF == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Functions  && !(Team.Deployment_Leaders__c == null)) {
                    prgNetwork.add(new ProgramNetwork(Team.Id,Team.Team_Name__c,Team.Entity__c,'','', Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    GFn = 1;
                  } // If Ends
                 } // For Ends
                if (GFn == 0)
                  prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Functions,GF,'','','','',''));
                 } // If Ends 
              
             } //for Loop Ends
             } // If Ends
            }//Try Ends
            catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
      
       // Global Functions Scope with No GSC Region
           
          try{
            if(!(Prg[0].Global_Functions__c.Contains(Label.Connect_GSC))){          
                
              for(String GF: Prg[0].Global_Functions__c.Split(';')){
              GFn = 0;
                 for(Team_Members__c Team:Scope){
                if(GF == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Functions  && !(Team.Deployment_Leaders__c == null) ) {
                    prgNetwork.add(new ProgramNetwork(Team.Id,Team.Team_Name__c,Team.Entity__c,Team.GSC_Region__c,Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    GFn = 1;
                 } // If Ends
                }  // For Loop Ends
              
              If(GFn == 0 && GF == Label.Connect_GSC){
                    prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Functions,GF,'','','','',''));
                    } // If Ends
                 else if(GFn == 0 && GF != Label.Connect_GSC){
                     prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Functions,GF,'','','','',''));
                     } // If Ends
                
             
             } //for Loop Ends
             } // If Ends
            }//Try Ends
            catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
                   
              
          
          // // Global Business Scope with No Partner Region
          
          integer GBs; 
          try{           
          if(!(Prg[0].Global_Business__c == null) && (Prg[0].Global_Business__c != Label.Connect_Partner) && (Prg[0].Global_Business__c != Label.Connect_Smart_Cities)){ 
              for(String GB: Prg[0].Global_Business__c.Split(';')){
               GBs = 0;
               for(Team_Members__c Team:Scope){
                if(GB == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Business && !(Team.Deployment_Leaders__c == null) && (Team.Partner_Region__c == null)&& (Team.Smart_Cities_Division__c == null)) {
                    prgNetwork.add(new ProgramNetwork(Team.Id,Team.Team_Name__c,Team.Entity__c,Prg[0].GSC_Regions__c,Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    GBs = 1;
                } // If Ends
               } // For Loop Ends
                 if((GBs == 0) && (GB != Label.Connect_Partner) && (GB != Label.Connect_Smart_Cities)){
                    prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Business,GB,'','','','',''));
                     } // If Ends
              } // For Loop Ends
             } // If Ends
            }// Try Ends
             catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
              
     // Global Business With Partner Region
     integer PRn;
     Integer SC;
     try{
            if((Prg[0].Global_Business__c.Contains(Label.Connect_Partner)) && (Prg[0].Partner_Region__c != null)){
            for(String GB: Prg[0].Global_Business__c.Split(';')){                      
                           
             for(String PAR: Prg[0].Partner_Region__c.Split(';')){ 
               PRn = 0; 
                 for(Team_Members__c Team:Scope){
                  //for(String T_PAR: Team.Partner_Region__c.Split(';')){
                  if(GB == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Business  && !(Team.Deployment_Leaders__c == null) && (Team.Partner_Region__c == PAR)) {
                    prgNetwork.add(new ProgramNetwork(Team.Id,Team.Team_Name__c,Team.Entity__c,'',Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    PRn = 1;
                  } // If Ends
                 }  // For Loop Ends     
                // } //For Loop Ends      
               If(PRn == 0 && GB == Label.Connect_Partner){
                 prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Business,GB,'',PAR,'','',''));
                    } // If Ends
                             
             } // For Loop Ends
            }// For Ends
           }//IF Ends
           
           // Global Business with Smart Cities
           
           if((Prg[0].Global_Business__c.Contains(Label.Connect_Smart_Cities)) && (Prg[0].Smart_Cities_Division__c != null)){
            for(String GB: Prg[0].Global_Business__c.Split(';')){                          
             
              for(String SMC: Prg[0].Smart_Cities_Division__c.Split(';')){ 
               SC = 0; 
                 for(Team_Members__c Team:Scope){
                  //for(String T_PAR: Team.Partner_Region__c.Split(';')){
                  if(GB == Team.Entity__c && Team.Team_Name__c == Label.Connect_Global_Business  && !(Team.Deployment_Leaders__c == null) && (Team.Smart_Cities_Division__c == SMC)) {
                    prgNetwork.add(new ProgramNetwork(Team.Id,Team.Team_Name__c,Team.Entity__c,'',Team.Partner_Region__c,Team.Deployment_Leaders__c, Team.Champion_Name__c,Team.Smart_Cities_Division__c));
                    SC = 1;
                  } // If Ends
                 }  // For Loop Ends     
                // } //For Loop Ends      
               If(SC == 0 && GB == Label.Connect_Smart_Cities){
                 prgNetwork.add(new ProgramNetwork('',Label.Connect_Global_Business,GB,'','','','',SMC));
                    } // If Ends
                             
             } // For Loop Ends
             
             } //for Loop Ends
             } // If Ends
            }//Try Ends
            catch(Exception e){
              System.Debug('Error:Scope Selection ' + e);
              }// Catch Ends
           
    return prgNetwork;
  }


 
    public class ProgramNetwork{
    public String id{get;set;}
    public String Scope {get; set;}
    public String Entity {get; set;}
    public String GSC_Region {get;set;}
    public String Partner_Region {get;set;}
    public String Deployment_Leader {get;set;}
    public String Champions{get;set;}
    public String SmartCity{get;set;}

    public ProgramNetwork(string id, string s,string e, string gsc, string par, string d, string c, string sc){
        this.id = id;
        this.Scope=s;
        this.Entity=e;
        this.GSC_Region=gsc;
        this.Partner_Region=par;
        this.Deployment_Leader=d;
        this.Champions=c;
        this.SmartCity=sc;

    }
   }
}