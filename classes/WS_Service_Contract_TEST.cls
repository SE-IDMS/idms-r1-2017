@isTest(seealldata=true)
private class WS_Service_Contract_TEST{
    
    
        
     static testMethod void CreatPositiveTest() {      

                TestData td = new TestData();
                td.CreateTestData();
                User user = td.userObj;                
                Product2 p2 = td.product;
                Country__c countrySobj = td.country;          
                SVMXC__Service_Order__c workorder = td.workorderobj;
                Account accountSobj = td.account;
                Contact contactSobj = td.contact;
                
                SVMXC__Installed_Product__c ip = td.ipobj;
               
            system.runAs(user){
                WS_Service_Contract.ServiceContract scw = new  WS_Service_Contract.ServiceContract();
                scw.contractNumberHeader = 'Test one';
                scw.description = 'Test Desc';
                scw.soldToAccount= accountSobj.id;
                scw.installedAtAccount = accountSobj.id;
                scw.billToAccount =  accountSobj.id;
                scw.contractStartDate = system.today();
                scw.contractEndDate = system.today()+30;
                scw.contractPrice = 100.00;
                scw.currencyCode = 'INR';
                scw.isEvergreen = true;
                scw.leadingBU ='IT';
                scw.sendCustomerRenewalNotification = true;
                scw.ownerSESA = 'SESA000000';
                scw.salesRepSESA = 'SESA000000';
                scw.status ='IPE';
                scw.PODate = system.today();
                scw.PONumber ='PONumber-100';
                WS_Service_Contract.Contact conunit = new WS_Service_Contract.Contact();
                conunit.GoldenID = contactSobj.SEContactID__c;
                //conunit.localMiddleInitial = contactSobj.LocalMidInit__c;
                scw.soldToContact = conunit;
                scw.billToContact = conunit;
                scw.mainInstalledAtContact = conunit;
                scw.countryOfBO ='US';
                scw.BOSystem ='CA_SAP';
                scw.BOReferenceHeader  = 'SC-1000-0001';
                WS_Service_Contract.ServiceLine slunit = new WS_Service_Contract.ServiceLine();
                slunit.contractNumberLine ='SLCN-001-0001';
                slunit.BOReferenceLine = 'SL-1000-0001';
                slunit.parentContractBORef = scw.BOReferenceHeader;
                slunit.contractPrice= 100.00;
                slunit.currencyCode ='INR';
                slunit.description =  'description Test mod ';
                slunit.entitlement = 'entitlement';
                slunit.lineEndDate = system.today()+30;
                slunit.lineStartDate = system.today();
                slunit.mainShipToAccount = accountSobj.id;
                slunit.SKUComRef = p2.id;
                slunit.quantity = 100;
                WS_Service_Contract.CoveredProduct cpunit = new WS_Service_Contract.CoveredProduct();
                cpunit.BOReferenceCoveredProduct ='CP-BORef-0001';
                cpunit.BOReferenceLine = slunit.BOReferenceLine;
                cpunit.InstalledProductEndDate = system.today()+30;
                cpunit.InstalledProductStartDate = system.today();
                cpunit.InstalledProductbFOID = ip.id;
                cpunit.InstalledProductGoldenID = ip.GoldenAssetId__c ;
                slunit.CoveredProducts = new List<WS_Service_Contract.CoveredProduct>{cpunit};
                scw.serviceLines = new List<WS_Service_Contract.serviceLine>{slunit}; 
              
                Test.startTest();
                WS_Service_Contract.ServiceContractResult result =  WS_Service_Contract.bulkCreateServiceContracts(scw);
                WS_Service_Contract.getServiceContractDetails(new list<id>{scw.bFOcontractID},'');
                WS_Service_Contract.updateServiceContractSyncDetails(scw.bFOcontractID,'test',true,'');
                WS_Service_Contract.updateServiceContractSyncDetails(scw.bFOcontractID,'test',false,'test');
                WS_Service_Contract.Contact wContact = new WS_Service_Contract.Contact();
                wContact.firstName ='fname';
                wContact.localFirstName='lfname';
                wContact.middleInitial='mi';
                wContact.lastName ='lname';
                wContact.localLastName ='llname';
                wContact.emailAddress ='email';
                wContact.workPhone ='111111';
                wContact.mobilePhone ='222222';
                wContact.GoldenID ='goldenid';
                
                scw.soldToContact = wContact;
                scw.billToContact = wContact;
                scw.mainInstalledAtContact = wContact;
                Sobject consobj = (Sobject)contactSobj;
                WS_Service_Contract.ContactInfo(scw,new SVMXC__Service_Contract__c(),new List<Sobject>{consobj});
                WS_Service_Contract.getContactFields(wContact);
                Test.stopTest();
                
            
        }
        
    
    }
    static testMethod void CreatNegatveHFail() { 
            TestData td = new TestData();
                td.CreateTestData();
                User user = td.userObj;                
                Product2 p2 = td.product;
                Country__c countrySobj = td.country;          
                SVMXC__Service_Order__c workorder = td.workorderobj;
                Account accountSobj = td.account;
                Contact contactSobj = td.contact;
                
                SVMXC__Installed_Product__c ip = td.ipobj;
                system.runAs(user){
                WS_Service_Contract.ServiceContract scw = new  WS_Service_Contract.ServiceContract();
                scw.contractNumberHeader = 'Test one';
                scw.description = 'Test Desc';
                scw.soldToAccount= accountSobj.id;
                scw.installedAtAccount = accountSobj.id;
                scw.billToAccount =  accountSobj.id;
                scw.contractStartDate = system.today();
                scw.contractEndDate = system.today()+30;
                scw.contractPrice = 100.00;
                scw.currencyCode = 'INR';
                scw.isEvergreen = true;
                scw.leadingBU ='IT';
                scw.sendCustomerRenewalNotification = true;
                scw.ownerSESA = 'SESA000000';
                scw.salesRepSESA = 'SESA000000';
                scw.status ='IPE';
                scw.PODate = system.today();
                scw.PONumber ='PONumber-100';
                WS_Service_Contract.Contact conunit = new WS_Service_Contract.Contact();
                conunit.GoldenID = contactSobj.SEContactID__c;
                //conunit.localMiddleInitial = contactSobj.LocalMidInit__c;
                scw.soldToContact = conunit;
                scw.billToContact = conunit;
                scw.mainInstalledAtContact = conunit;
                scw.countryOfBO ='US';
                scw.BOSystem ='CA_SAP';
                scw.BOReferenceHeader  = 'SC-1000-0001';
                 
              
                Test.startTest();
                 String boref='-';
                
                boref ='Test-----------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------';
                   scw.BOReferenceHeader = boref;
                WS_Service_Contract.ServiceContractResult result =  WS_Service_Contract.bulkCreateServiceContracts(scw);
                
                Test.stopTest();
                }
                
            
        }
        static testMethod void CreatNegatveSLFail() { 
            TestData td = new TestData();
                td.CreateTestData();
                User user = td.userObj;                
                Product2 p2 = td.product;
                Country__c countrySobj = td.country;          
                SVMXC__Service_Order__c workorder = td.workorderobj;
                Account accountSobj = td.account;
                Contact contactSobj = td.contact;
                
                SVMXC__Installed_Product__c ip = td.ipobj;
                system.runAs(user){
                WS_Service_Contract.ServiceContract scw = new  WS_Service_Contract.ServiceContract();
                scw.contractNumberHeader = 'Test one';
                scw.description = 'Test Desc';
                scw.soldToAccount= accountSobj.id;
                scw.installedAtAccount = accountSobj.id;
                scw.billToAccount =  accountSobj.id;
                scw.contractStartDate = system.today();
                scw.contractEndDate = system.today()+30;
                scw.contractPrice = 100.00;
                scw.currencyCode = 'INR';
                scw.isEvergreen = true;
                scw.leadingBU ='IT';
                scw.sendCustomerRenewalNotification = true;
                scw.ownerSESA = 'SESA000000';
                scw.salesRepSESA = 'SESA000000';
                scw.status ='IPE';
                scw.PODate = system.today();
                scw.PONumber ='PONumber-100';
                WS_Service_Contract.Contact conunit = new WS_Service_Contract.Contact();
                conunit.GoldenID = contactSobj.SEContactID__c;
                //conunit.localMiddleInitial = contactSobj.LocalMidInit__c;
                scw.soldToContact = conunit;
                scw.billToContact = conunit;
                scw.mainInstalledAtContact = conunit;
                scw.countryOfBO ='US';
                scw.BOSystem ='CA_SAP';
                scw.BOReferenceHeader  = 'SC-1000-0001';
                 WS_Service_Contract.ServiceLine slunit = new WS_Service_Contract.ServiceLine();
                slunit.contractNumberLine ='SLCN-001-0001';
                slunit.BOReferenceLine = 'Test-----------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------';
                slunit.parentContractBORef = scw.BOReferenceHeader;
                slunit.contractPrice= 100.00;
                slunit.currencyCode ='INR';
                slunit.description =  'description Test mod ';
                slunit.entitlement = 'entitlement';
                slunit.lineEndDate = system.today()+30;
                slunit.lineStartDate = system.today();
                slunit.mainShipToAccount = accountSobj.id;
                slunit.SKUComRef = p2.id;
                slunit.quantity = 100;
                 
              
                Test.startTest();
                 String boref='-';
                scw.serviceLines = new List<WS_Service_Contract.serviceLine>{slunit}; 
                
                WS_Service_Contract.ServiceContractResult result =  WS_Service_Contract.bulkCreateServiceContracts(scw);
                
                Test.stopTest();
                }
                
            
        }
        static testMethod void CreatNegatveCPFail() { 
            TestData td = new TestData();
                td.CreateTestData();
                User user = td.userObj;                
                Product2 p2 = td.product;
                Country__c countrySobj = td.country;          
                SVMXC__Service_Order__c workorder = td.workorderobj;
                Account accountSobj = td.account;
                Contact contactSobj = td.contact;
                
                SVMXC__Installed_Product__c ip = td.ipobj;
                system.runAs(user){
                WS_Service_Contract.ServiceContract scw = new  WS_Service_Contract.ServiceContract();
                scw.contractNumberHeader = 'Test one';
                scw.description = 'Test Desc';
                scw.soldToAccount= accountSobj.id;
                scw.installedAtAccount = accountSobj.id;
                scw.billToAccount =  accountSobj.id;
                scw.contractStartDate = system.today();
                scw.contractEndDate = system.today()+30;
                scw.contractPrice = 100.00;
                scw.currencyCode = 'INR';
                scw.isEvergreen = true;
                scw.leadingBU ='IT';
                scw.sendCustomerRenewalNotification = true;
                scw.ownerSESA = 'SESA000000';
                scw.salesRepSESA = 'SESA000000';
                scw.status ='IPE';
                scw.PODate = system.today();
                scw.PONumber ='PONumber-100';
                WS_Service_Contract.Contact conunit = new WS_Service_Contract.Contact();
                conunit.GoldenID = contactSobj.SEContactID__c;
                //conunit.localMiddleInitial = contactSobj.LocalMidInit__c;
                scw.soldToContact = conunit;
                scw.billToContact = conunit;
                scw.mainInstalledAtContact = conunit;
                scw.countryOfBO ='US';
                scw.BOSystem ='CA_SAP';
                scw.BOReferenceHeader  = 'SC-1000-0001';
                 WS_Service_Contract.ServiceLine slunit = new WS_Service_Contract.ServiceLine();
                slunit.contractNumberLine ='SLCN-001-0001';
                slunit.BOReferenceLine = 'SL-1000-0001';
                slunit.parentContractBORef = scw.BOReferenceHeader;
                slunit.contractPrice= 100.00;
                slunit.currencyCode ='INR';
                slunit.description =  'description Test mod ';
                slunit.entitlement = 'entitlement';
                slunit.lineEndDate = system.today()+30;
                slunit.lineStartDate = system.today();
                slunit.mainShipToAccount = accountSobj.id;
                slunit.SKUComRef = p2.id;
                slunit.quantity = 100;
                WS_Service_Contract.CoveredProduct cpunit = new WS_Service_Contract.CoveredProduct();
                
                cpunit.BOReferenceCoveredProduct = 'Test-----------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------'+
                        '--------------------------------------------------------------------------------------------------------------';
                cpunit.InstalledProductEndDate = system.today()+30;
                cpunit.InstalledProductStartDate = system.today();
                cpunit.InstalledProductbFOID = ip.id;
                cpunit.InstalledProductGoldenID = ip.GoldenAssetId__c ;
                slunit.CoveredProducts = new List<WS_Service_Contract.CoveredProduct>{cpunit};
                scw.serviceLines = new List<WS_Service_Contract.serviceLine>{slunit}; 
                 
              
                Test.startTest();
           
                WS_Service_Contract.ServiceContractResult result =  WS_Service_Contract.bulkCreateServiceContracts(scw);
                
                Test.stopTest();
                }
                
            
        }
    static testMethod void ServiceContractDetails() {
            User user = new User(alias = 'user', email='user' + '@Schneider-electric.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = System.label.CLAPR15SRV37, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'SVMX10;SVMX_InstalledProduct2;SVMX22;SRV_APR_001;SVMX18;AP_WorkOrderTechnicianNotification;SRV_APR_001;SVMX16;SVMX14;SVMX19',
                timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = 'SESA000000',SESAExternalID__c ='SESA000000');
                insert user;
        system.runAs(user){
                
                Product2 p2 = Utils_TestMethods.createCommercialReference('One','Two','Three','Four','Five','Six',true,true,true);
                insert p2;
               
                //Country__c countrySobj =Utils_TestMethods.createCountry();
                country__c countrySobj = [Select name,id,countrycode__C from country__C where countrycode__c =: 'US'];
                //insert countrySobj ;    
                
                SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();
                workorder.BackOfficeReference__c='test work order';
                insert workorder;
                
                Account accountSobj = Utils_TestMethods.createAccount();
                accountSobj.SEAccountID__c ='SEAccountID';
                accountSobj.RecordTypeId  = System.Label.CLOCT13ACC08;
                accountSobj.Country__c= countrySobj.id;
                insert accountSobj;
                
                Contact contactSobj = Utils_TestMethods.createContact(accountSobj.id, 'contact LastName');
                contactSobj.Country__c = countrySobj.id;
                contactSobj.SEContactID__c ='SEContactID1';
                contactSobj.FirstName = 'CFirstName';
                contactSobj.LastName= 'CLastName';
                contactSobj.LocalFirstName__c= 'CLFirstName';
                contactSobj.MidInit__c ='TMid';
                contactSobj.LocalMidInit__c='TLMid';
                contactSobj.LocalLastName__c='CLLastName';
                contactSobj.Email='test@test.com';
                contactSobj.mobilePhone ='9999999999';
                contactSobj.WorkPhone__c='9999999999';
                insert contactSobj;
                
                
                SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
                ip.SVMXC__Company__c = accountSobj.id;
                ip.Name = 'Test Intalled Product';
                ip.SVMXC__Status__c= 'new';
                ip.GoldenAssetId__c = 'GoledenAssertId10001';
                ip.BrandToCreate__c ='Test Brand';
                ip.CustomerSerialNumber__c = 'SerialNumber';
                insert ip;
                               
                
                Test.startTest();
               
                SVMXC__Service_Contract__c testContract = new SVMXC__Service_Contract__c();
                testContract.SVMXC__Company__c = accountSobj.Id;
                testContract.SoldtoAccount__c=accountSobj.Id;
                testContract.Name = 'Test Contract0';  
                testContract.SVMXC__Contact__c = contactSobj.Id  ;    
                testContract.SVMXC__Start_Date__c = system.today();
                testContract.SVMXC__End_Date__c= system.today() + 10;
                testContract.SVMXC__Renewed_From__c = null;
                testContract.LeadingBusinessBU__c ='BD';
                testContract.DefaultInstalledAtAccount__c=accountSobj.Id;
                testContract.Tech_ContractCancel__c = true; 
                testContract.SVMXC__Active__c   = false;
                insert testContract;
        
                SVMXC__Service_Contract__c serviceLine = new SVMXC__Service_Contract__c();
                serviceLine.SVMXC__Company__c = accountSobj.Id;
                serviceLine.Name = 'Test Contract1'; 
                serviceLine.ParentContract__c = testContract.id;
                //serviceLine.SVMXC__Contact__c = testContract.Id ;
                insert serviceLine;
            
                SVMXC__Service_Contract_Products__c scp2=  new SVMXC__Service_Contract_Products__c();
                scp2.SVMXC__Service_Contract__c = serviceLine.id;
                scp2.SVMXC__Installed_Product__c = ip.id;
                insert scp2;
            
                WS_Service_Contract.getServiceContractDetails(new list<id>{testContract.id},'');
                WS_Service_Contract.updateServiceContractSyncDetails(testContract.id,'test',true,'');
                WS_Service_Contract.updateServiceContractSyncDetails(testContract.id,'test',false,'test');
           
        
        }
    }
    static testMethod void UpdateTest() {
                User user = new User(alias = 'user', email='user' + '@Schneider-electric.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = System.label.CLAPR15SRV37, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'SVMX10;SVMX_InstalledProduct2;SVMX22;SRV_APR_001;SVMX18;AP_WorkOrderTechnicianNotification;SRV_APR_001;SVMX16;SVMX14;SVMX19',
                timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = 'SESA000000',SESAExternalID__c ='SESA000000');
                insert user;
        
                 
                
                
                system.runAs(user){
                
                Product2 p2 = Utils_TestMethods.createCommercialReference('One','Two','Three','Four','Five','Six',true,true,true);
                insert p2;
               
                //Country__c countrySobj =Utils_TestMethods.createCountry();
                country__c countrySobj = [Select name,id,countrycode__C from country__C where countrycode__c =: 'US'];
                //insert countrySobj ;    
                
                SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();
                workorder.BackOfficeReference__c='test work order';
                insert workorder;
                
                Account accountSobj = Utils_TestMethods.createAccount();
                accountSobj.SEAccountID__c ='SEAccountID';
                accountSobj.RecordTypeId  = System.Label.CLOCT13ACC08;
                accountSobj.Country__c= countrySobj.id;
                insert accountSobj;
                
                Contact contactSobj = Utils_TestMethods.createContact(accountSobj.id, 'contact LastName');
                contactSobj.Country__c = countrySobj.id;
                contactSobj.SEContactID__c ='SEContactID1';
                contactSobj.FirstName = 'CFirstName';
                contactSobj.LastName= 'CLastName';
                contactSobj.LocalFirstName__c= 'CLFirstName';
                contactSobj.MidInit__c ='TMid';
                contactSobj.LocalMidInit__c='TLMid';
                contactSobj.LocalLastName__c='CLLastName';
                contactSobj.Email='test@test.com';
                contactSobj.mobilePhone ='9999999999';
                contactSobj.WorkPhone__c='9999999999';
                insert contactSobj;
                
                
                SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
                ip.SVMXC__Company__c = accountSobj.id;
                ip.Name = 'Test Intalled Product';
                ip.SVMXC__Status__c= 'new';
                ip.GoldenAssetId__c = 'GoledenAssertId10001';
                ip.BrandToCreate__c ='Test Brand';
                ip.CustomerSerialNumber__c = 'SerialNumber';
                insert ip;
                               
                
                
               
                SVMXC__Service_Contract__c testContract = new SVMXC__Service_Contract__c();
                testContract.SVMXC__Company__c = accountSobj.Id;
                testContract.SoldtoAccount__c=accountSobj.Id;
                testContract.Name = 'Test Contract0';  
                testContract.SVMXC__Contact__c = contactSobj.Id  ;    
                testContract.SVMXC__Start_Date__c = system.today();
                testContract.SVMXC__End_Date__c= system.today() + 10;
                testContract.SVMXC__Renewed_From__c = null;
                testContract.LeadingBusinessBU__c ='BD';
                testContract.DefaultInstalledAtAccount__c=accountSobj.Id;
                testContract.Tech_ContractCancel__c = true; 
                testContract.SVMXC__Active__c   = false;
                insert testContract;
        
                SVMXC__Service_Contract__c serviceLine = new SVMXC__Service_Contract__c();
                serviceLine.SVMXC__Company__c = accountSobj.Id;
                serviceLine.Name = 'Test Contract1';  
                //serviceLine.SVMXC__Contact__c = testContract.Id ;
                insert serviceLine;
        
                SVMXC__Service_Contract_Products__c scp1=  new SVMXC__Service_Contract_Products__c();
                scp1.SVMXC__Service_Contract__c = serviceLine.id;
                scp1.SVMXC__Installed_Product__c = ip.id;
                insert scp1;
                
                 
                    
                
                try{
                WS_Service_Contract.ServiceContract scw = new  WS_Service_Contract.ServiceContract();
                scw.bFOcontractID = testContract.id;
                scw.contractNumberHeader = 'Test one';
                scw.description = 'Test Desc';
                scw.soldToAccount= accountSobj.id;
                scw.installedAtAccount = accountSobj.id;
                scw.billToAccount =  accountSobj.id;
                scw.contractStartDate = system.today();
                scw.contractEndDate = system.today()+30;
                scw.contractPrice = 100.00;
                scw.currencyCode = 'INR';
                scw.isEvergreen = true;
                scw.leadingBU ='IT';
                scw.sendCustomerRenewalNotification = true;
                scw.ownerSESA = 'SESA000000';
                scw.salesRepSESA = 'SESA000000';
                scw.status ='IPE';
                scw.PODate = system.today();
                scw.PONumber ='PONumber-100';
                WS_Service_Contract.Contact conunit = new WS_Service_Contract.Contact();
                conunit.GoldenID = contactSobj.SEContactID__c;
                scw.soldToContact = conunit;
                scw.billToContact = conunit;
                scw.mainInstalledAtContact = conunit;
                scw.countryOfBO ='US';
                scw.BOSystem ='CA_SAP';
                scw.BOReferenceHeader  = 'SC-1000-0001';
                WS_Service_Contract.ServiceLine slunit = new WS_Service_Contract.ServiceLine();
                slunit.contractNumberLine ='SLCN-001-0001';
                slunit.BOReferenceLine = 'SL-1000-0001';
                slunit.parentContractBORef = scw.BOReferenceHeader;
                slunit.contractPrice= 100.00;
                slunit.currencyCode ='INR';
                slunit.description =  'description Test mod ';
                slunit.entitlement = 'entitlement';
                slunit.lineEndDate = system.today()+30;
                slunit.lineStartDate = system.today();
                slunit.mainShipToAccount = accountSobj.id;
                slunit.SKUComRef = p2.id;
                slunit.quantity = 100;
                WS_Service_Contract.ServiceLine slunit1= slunit.clone();
                slunit1.bFOContractLineID = serviceLine.id;
                WS_Service_Contract.CoveredProduct cpunit = new WS_Service_Contract.CoveredProduct();
                cpunit.BOReferenceCoveredProduct ='CP-BORef-0001';
                cpunit.BOReferenceLine = slunit.BOReferenceLine;
                cpunit.InstalledProductEndDate = system.today()+30;
                cpunit.InstalledProductStartDate = system.today();
                cpunit.InstalledProductbFOID = ip.id;
                cpunit.InstalledProductGoldenID = ip.GoldenAssetId__c ;
                    
                 
                WS_Service_Contract.ServiceLine slunit2 = slunit.clone();
                slunit2.BOReferenceLine  = 'SL-1000-0002';
                WS_Service_Contract.CoveredProduct cpunit2 = cpunit.clone();
                    cpunit2.BOReferenceCoveredProduct ='CP-BORef-0002';
                //cpunit2.InstalledProductbFOID = ip1.id;
                cpunit2.BOReferenceCoveredProduct = 'CP-BORef-0002';
                slunit.CoveredProducts = new List<WS_Service_Contract.CoveredProduct>{cpunit};
                slunit2.CoveredProducts = new List<WS_Service_Contract.CoveredProduct>{cpunit2};
                scw.serviceLines = new List<WS_Service_Contract.serviceLine>{slunit,slunit1}; 
                                
                     Test.startTest();               
                 
                WS_Service_Contract.bulkUpdateServiceContracts(scw);
                
                
                              
                Test.stopTest();
               
                /*
                slunit2.CoveredProducts.add(cpunit2);
                scw.serviceLines.add(slunit2);
                System.debug('hari log :'+scw);
                */ 
                
               
                
                
             }       
                
            catch (exception e){}
        }
        
    }
    static testMethod void getMatchedContact(){
        
                Account accountSobj = Utils_TestMethods.createAccount();
                accountSobj.SEAccountID__c ='SEAccountID';
                accountSobj.RecordTypeId  = System.Label.CLOCT13ACC08;
                //accountSobj.Country__c= countrySobj.id;
                insert accountSobj;
        
                Contact contactSobj = Utils_TestMethods.createContact(accountSobj.id, 'contact LastName');
                //contactSobj.Country__c = countrySobj.id;
                contactSobj.SEContactID__c ='SEContactID1';
                contactSobj.FirstName = 'CFirstName';
                contactSobj.LastName= 'CLastName';
                contactSobj.LocalFirstName__c= 'CLFirstName';
                contactSobj.MidInit__c ='TMid';
                contactSobj.LocalMidInit__c='TLMid';
                contactSobj.LocalLastName__c='CLLastName';
                contactSobj.Email='test@test.com';
                contactSobj.mobilePhone ='9999999999';
                contactSobj.WorkPhone__c='9999999999';
                insert contactSobj;
                Sobject csobj = (Sobject)contactSobj;
                List<Sobject> cobjList = new List<Sobject>();
                cobjList.add(csobj);
                WS_Service_Contract.Contact conunit = new WS_Service_Contract.Contact();
                conunit.GoldenID = 'SEContactID1';
                WS_Service_Contract.getMatchedContact(conunit,cobjList,accountSobj.id);
                WS_Service_Contract.getMatchedContact(conunit,cobjList,null);
                conunit.GoldenID = 'SEContactID2';
                conunit.emailAddress = 'test@test.com';
                WS_Service_Contract.getMatchedContact(conunit,cobjList,accountSobj.id);
                WS_Service_Contract.getMatchedContact(conunit,cobjList,null);
                conunit.GoldenID = 'SEContactID2';
                conunit.emailAddress = 'test1@test.com';
                conunit.mobilePhone = '9999999999';
                WS_Service_Contract.getMatchedContact(conunit,cobjList,accountSobj.id);
                WS_Service_Contract.getMatchedContact(conunit,cobjList,null);
                conunit.GoldenID = 'SEContactID2';
                conunit.emailAddress = 'test1@test.com';
                conunit.mobilePhone = '19999999999';
                conunit.firstName = 'CFirstName';
                WS_Service_Contract.getMatchedContact(conunit,cobjList,accountSobj.id);
                WS_Service_Contract.getMatchedContact(conunit,cobjList,null);
                conunit.GoldenID = 'SEContactID2';
                conunit.emailAddress = 'test1@test.com';
                conunit.mobilePhone = '19999999999';
                conunit.firstName = 'CFirstName1';
                conunit.lastName = 'CLastName';
                WS_Service_Contract.getMatchedContact(conunit,cobjList,accountSobj.id);
                WS_Service_Contract.getMatchedContact(conunit,cobjList,null);
                conunit.GoldenID = 'SEContactID2';
                conunit.emailAddress = 'test1@test.com';
                conunit.mobilePhone = '19999999999';
                conunit.firstName = 'CFirstName1';
                conunit.lastName = 'CLastName1';
                conunit.localFirstName = 'CLFirstName1';
                conunit.localMiddleInitial='TLMid';
                WS_Service_Contract.getMatchedContact(conunit,cobjList,accountSobj.id);
                WS_Service_Contract.getMatchedContact(conunit,cobjList,null);
                WS_Service_Contract.ServiceContract wssc = new WS_Service_Contract.ServiceContract();
                WS_Service_Contract.ServiceLine wssl = new WS_Service_Contract.ServiceLine();
                wssc.serviceLines = new List<WS_Service_Contract.ServiceLine>();            
        
                WS_Service_Contract.mandatoryCheck(wssc);
                //WS_Service_Contract.getMatchedContact()               
                
        
    }
        
    
    public class TestData{
        
            User userObj ;
            
            Country__c country ;
            Product2 product;
            Account account;
            Contact contact;
            SVMXC__Service_Order__c workorderobj;
            SVMXC__Installed_Product__c ipobj;
            
            
            public void CreateTestData(){
        
                User user = new User(alias = 'user', email='user' + '@Schneider-electric.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = System.label.CLAPR15SRV37, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'SVMX10;SVMX_InstalledProduct2;SVMX22;SRV_APR_001;SVMX18;AP_WorkOrderTechnicianNotification;SRV_APR_001;SVMX16;SVMX14;SVMX19',
                timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = 'SESA000000',SESAExternalID__c ='SESA000000');
                insert user;
                userObj = user;
               
                Product2 p2 = Utils_TestMethods.createCommercialReference('One','Two','Three','Four','Five','Six',true,true,true);
                insert p2;
                product  = p2;
                //Country__c countrySobj =Utils_TestMethods.createCountry();
                country__c countrySobj = [Select name,id,countrycode__C from country__C where countrycode__c =: 'US'];
                //insert countrySobj ;    
                country = countrySobj;
                SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c();
                workorder.BackOfficeReference__c='test work order';
                insert workorder;
                workorderobj = workorder;
                Account accountSobj = Utils_TestMethods.createAccount();
                accountSobj.SEAccountID__c ='SEAccountID';
                accountSobj.RecordTypeId  = System.Label.CLOCT13ACC08;
                accountSobj.Country__c= countrySobj.id;
                insert accountSobj;
                account = accountSobj;
                Contact contactSobj = Utils_TestMethods.createContact(accountSobj.id, 'contact LastName');
                contactSobj.Country__c = countrySobj.id;
                contactSobj.SEContactID__c ='SEContactID1';
                contactSobj.FirstName = 'CFirstName';
                contactSobj.LastName= 'CLastName';
                contactSobj.LocalFirstName__c= 'CLFirstName';
                contactSobj.MidInit__c ='TMid';
                contactSobj.LocalMidInit__c='TLMid';
                contactSobj.LocalLastName__c='CLLastName';
                contactSobj.Email='test@test.com';
                contactSobj.mobilePhone ='9999999999';
                contactSobj.WorkPhone__c='9999999999';
                insert contactSobj;
                contact = contactSobj;
                
                SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c();
                ip.SVMXC__Company__c = accountSobj.id;
                ip.Name = 'Test Intalled Product';
                ip.SVMXC__Status__c= 'new';
                ip.GoldenAssetId__c = 'GoledenAssertId10001';
                ip.BrandToCreate__c ='Test Brand';
                ip.CustomerSerialNumber__c = 'SerialNumber';
                insert ip;
                ipobj = ip;
                
                }
        
    
    
    }
    
    
  
}