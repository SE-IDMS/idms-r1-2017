//*********************************************************************************
// Class Name       : VFC1000_ProblemSearch_Test
// Purpose          : Test Class for VFC1000_ProblemSearch
// Created by       : Global Delivery Team
// Date created     : 26th Augest 2011
// Modified by      :
// Date Modified    :
// Remarks          : For Oct - 11 Release
///********************************************************************************/


@isTest
Private with sharing class VFC1000_ProblemSearch_Test {
      
        static testMethod void VFC1000_ProblemSearchTest1() {
            // to test Simple search problem
            //creation of objects
            
            Country__c country1= Utils_TestMethods.createCountry();
            insert country1;
            BusinessRiskEscalationEntity__c BRE1=Utils_TestMethods.createBRE('sample entity','sample subentity','sample location','Plant');
            BRE1.Country__c = country1.id;
            BRE1.ProblemToPreventionDeployed__c =  true;
            insert BRE1;
            
            Problem__c problem1 = Utils_TestMethods.createProblem(BRE1.id);
            insert problem1;
            List<Problem__c> problemlist=new List<Problem__c>();
            for(Integer i=0;i<100 ;i++){
                Problem__c p=Utils_TestMethods.createProblem(BRE1.id);
                p.Title__c ='Test '+i;
                problemlist.add(p);
                
                
            }
            
            
                insert problemlist;
            
            CommercialReference__c comref =  new CommercialReference__c();
            
            comref.Problem__c=problem1.id;
            comref.CommercialReference__c='Test Commercial Ref';
            comref.ProductLine__c = 'Test ProductLine';
            insert comref;
            
            Problem__c p=new Problem__c();
            Test.startTest();
            PageReference pageRef= Page.VFP1000_ProblemSearch; 
            Test.setCurrentPage(pageRef);            
            ApexPages.StandardController probSContoller = new ApexPages.StandardController(p);
            VFC1000_ProblemSearch  problemSearchController=new VFC1000_ProblemSearch(probSContoller);
            VCC05_DisplaySearchFields componentController = new VCC05_DisplaySearchFields();
            componentController.pickListType = 'PM0_GMR';
            componentController.SearchFilter = new String[4];
            componentController.SearchFilter[0] = '00';
            componentController.SearchFilter[1] = Label.CL00355;
            componentcontroller.key = 'searchComponent';
            componentController.PageController = problemSearchController;
            componentController.init();
            problemSearchController.title='Test';
            problemSearchController.problem.AccountableOrganization__c = BRE1.id;
            problemSearchController.CommercialReference ='Test Commercial Ref';
            problemSearchController.doSearchProblem();
            problemSearchController.title='';
            problemSearchController.CommercialReference ='';
            problemSearchController.PartNumber ='test';
            problemSearchController.doSearchProblem();
            problemSearchController.continueCreation();
            
            Test.stopTest();
            
            
        }
        static testMethod void VFC1000_ProblemSearchTest2() {
            
             Country__c country1= Utils_TestMethods.createCountry();
            insert country1;
            
            
            BusinessRiskEscalationEntity__c BRE2=Utils_TestMethods.createBRE('sample entity2','sample subentity2','sample location2','Plant2');         
            
            
            insert BRE2;
            Problem__c problem1 = Utils_TestMethods.createProblem(BRE2.id);
            insert problem1;
            CommercialReference__c comref =  new CommercialReference__c();
            
            comref.Problem__c=problem1.id;
            comref.CommercialReference__c='Test Commercial Ref';
            comref.ProductLine__c = 'Test ProductLine';
            insert comref;
            
            Problem__c p=new Problem__c();
            Test.startTest();
             PageReference pageRef2= Page.VFP1000_ProblemSearch; 
            Test.setCurrentPage(pageRef2);            
            ApexPages.StandardController probSContoller2 = new ApexPages.StandardController(p);
            VFC1000_ProblemSearch  problemSearchController2=new VFC1000_ProblemSearch(probSContoller2);
            VCC05_DisplaySearchFields componentController2 = new VCC05_DisplaySearchFields();
            componentController2.pickListType = 'PM0_GMR';
            componentController2.SearchFilter = new String[4];
            componentController2.SearchFilter[0] =  Label.CL00355;
            componentController2.SearchFilter[1] = Label.CL00355;
            componentController2.SearchFilter[2] = Label.CL00355;
            componentController2.SearchFilter[3] = Label.CL00355;
            componentController2.key = 'searchComponent';
            componentController2.PageController = problemSearchController2;
            componentController2.init();    
            problemSearchController2.problem.AccountableOrganization__c = BRE2.id;              
            problemSearchController2.doSearchProblem(); 
            Test.stopTest();    
            
        }
        static testMethod void VFC1000_ProblemSearchTest3() {
            
             Country__c country=new  Country__c(Name='TestCountry', CountryCode__c='TTT');
            insert country;
            Id idSupplierAcctRecType=   [SELECT Id FROM RecordType WHERE 
                                 SObjectType = 'Account' AND Name = 'Supplier'].id; 
       
       // Create Partner Account
        Account supplier = new Account();
        supplier.RecordTypeId = idSupplierAcctRecType;
        supplier.Name = 'Test Supplier'; 
        supplier.ProblemtoPreventionDeployed__c=  true;
            
            BusinessRiskEscalationEntity__c AccountableOrganization=new  BusinessRiskEscalationEntity__c(Entity__c = 'PowerTEST',Country__c= country.id, SubEntity__c= 'Low Voltage');
            AccountableOrganization.ProblemToPreventionDeployed__c =true;
            insert AccountableOrganization;
            List<Problem__c> problemlist=new List<Problem__c>();
            
            for(Integer i=0;i<100 ;i++){
                Problem__c p=new Problem__c();
                p.Title__c ='Test '+i;
                problemlist.add(p);
                p.RecordTypeId= System.Label.CLI2PAPR120014;
                p.AccountableOrganization__c=AccountableOrganization.id;
                p.PartnerAccount__c=supplier.id;
                p.PartNumber__c ='Test Part Number';
                p.WhereWasTheProblemDetected__c='TEST';
                
            }
            
            
                insert problemlist;
                Problem__c p=new Problem__c();
                Test.startTest();
                 PageReference pageRef= Page.VFP1000_ProblemSearch; 
                 Test.setCurrentPage(pageRef);
                ApexPages.StandardController prob1 = new ApexPages.StandardController(p);
                ApexPages.currentPage().getParameters().put('RecordType', System.Label.CLI2PAPR120010);
                 VFC_ControllerBase basecontroller = new VFC_ControllerBase();                
                 
                 VFC1000_ProblemSearch  problemsearch=new VFC1000_ProblemSearch(prob1);      
                  
                 
                 VCC05_DisplaySearchFields componentcontroller = new VCC05_DisplaySearchFields();
                 componentcontroller.pickListType = 'PM0_GMR';
                 //componentcontroller.level1='Energy';
                 componentcontroller.SearchFilter = new String[4];
                componentcontroller.SearchFilter[0] = '00';
                componentcontroller.SearchFilter[1] = Label.CL00355;
                 componentcontroller.key = 'searchComponent';
                 componentcontroller.PageController = problemsearch;
                 componentcontroller.init();
                
                problemsearch.searchController=componentcontroller;
                
                 problemsearch.title='Test';
                 problemsearch.doSearchProblem();
                 //problemsearch.Supplier = 'Test';
                 //problemsearch.PartNumber ='Test';
                 //problemsearch.CommercialReference='Test';
                 problemsearch.Supplier = 'Test';
                 problemsearch.doSearchProblem();
                 problemsearch.title='';
                  
                 problemsearch.Supplier = 'Test';
                 problemsearch.doSearchProblem();
                 problemsearch.continueCreation();
                 Test.stopTest();
            
        }
        static testMethod void VFC1000_ProblemSearchTest4() {
            
            
            Problem__c p=new Problem__c();
                Test.startTest();
                 PageReference pageRef= Page.VFP1000_ProblemSearch; 
                 Test.setCurrentPage(pageRef);
                ApexPages.StandardController prob1 = new ApexPages.StandardController(p);
                ApexPages.currentPage().getParameters().put('RecordType', System.Label.CLI2PAPR120010);
                 VFC_ControllerBase basecontroller = new VFC_ControllerBase();                
                 
                 VFC1000_ProblemSearch  problemsearch=new VFC1000_ProblemSearch(prob1);      
                  
                 
                 VCC05_DisplaySearchFields componentcontroller = new VCC05_DisplaySearchFields();
                 componentcontroller.pickListType = 'PM0_GMR';
                 //componentcontroller.level1='Energy';
                 componentcontroller.SearchFilter = new String[4];
                componentcontroller.SearchFilter[0] = '00';
                componentcontroller.SearchFilter[1] = Label.CL00355;
                 componentcontroller.key = 'searchComponent';
                 componentcontroller.PageController = problemsearch;
                 componentcontroller.init();
                
                problemsearch.searchController=componentcontroller;
                problemsearch.doSearchProblem();
            
            Test.stopTest();
        }
       

}