/*
Batch to ensure Account Master Profile values populated into the Account, if not populated from the queuable job.
*/

global class Batch_UpdateAccountFromAMP implements Database.Batchable<sObject>,Schedulable {
    public String query;
    Public  Map<String,Decimal> currencyMap = new Map<String,Decimal>();
    
    global Batch_UpdateAccountFromAMP()
    {        
         for(CurrencyType ct:[SELECT ConversionRate,DecimalPlaces,IsActive,IsoCode FROM CurrencyType WHERE IsActive = true])  
        {      
            currencyMap.put(ct.IsoCode,ct.ConversionRate);
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       query='select id,AccountMasterProfile__c,TotalPAM__c,lastmodifieddate,CurrencyIsocode,Account__r.Tech_CL1Updated__c,Account__r.AccountMasterProfileLastUpdated__c, Account__r.AccountMasterProfile__c,Account__r.CurrencyIsoCode, Account__r.TotalPAM__c from AccountMasterProfile__c where lastmodifieddate >= Yesterday';        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<AccountMasterProfile__c> scope) {
        List<Account> accList=new List<Account>();
        String accCurrency;
        for(AccountMasterProfile__c s:scope){
            accCurrency=null;  
            if(currencyMap.containsKey(s.Account__r.CurrencyIsoCode))
                accCurrency=s.Account__r.CurrencyIsoCode;
            else
                accCurrency='EUR';// for inactive currency
            System.debug('s.Account__r.AccountMasterProfileLastUpdated__c'+s.Account__r.AccountMasterProfileLastUpdated__c);
            System.debug('entered loop>>>');
            if((s.Account__r.AccountMasterProfileLastUpdated__c != s.lastmodifieddate.date()) || (s.Account__r.AccountMasterProfile__c!=s.AccountMasterProfile__c) || (s.Account__r.Tech_CL1Updated__c==true) || (s.TotalPAM__c!=null && (s.Account__r.TotalPAM__c!= ((s.TotalPAM__c * currencyMap.get(accCurrency))/currencyMap.get(s.CurrencyIsoCode)))))
            {
                System.debug('entered loop>>>');
                s.Account__r.AccountMasterProfileLastUpdated__c=s.lastmodifieddate.Date();
                s.Account__r.AccountMasterProfile__c=s.AccountMasterProfile__c;
                if(s.TotalPAM__c!=null)
                   s.Account__r.TotalPAM__c= (s.TotalPAM__c * currencyMap.get(accCurrency))/currencyMap.get(s.CurrencyIsoCode); 
                 //Commented below line if user updates AMP first and then changes CL1 of Account, AMP will be holding  weights of old CL1 but batch will uncheck thinking user has updated AMP after he changed CL1
                //s.Account__r.Tech_CL1Updated__c=False;
                if(s.Account__r.CurrencyIsoCode!=accCurrency)
                    s.Account__r.CurrencyIsoCode=accCurrency;
                acclist.add(s.Account__r);
            }
        }

         Database.update(accList,false);
    } 
    
    global void finish(Database.BatchableContext BC) {
        
        
    }
    
    global void execute(SchedulableContext sc) {
        Batch_UpdateAccountFromAMP ampBatch = new Batch_UpdateAccountFromAMP();
        Database.executeBatch(ampBatch,Integer.ValueOf(Label.CLQ316SLS009));
    } 
}