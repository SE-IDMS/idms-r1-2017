/*    Author          : Accenture Team    
      Date Created    : 24/10/2011     
      Description     : Controller extensions for VFP65_NewEditSMETeamMember. 
*/
                        
public without sharing class VFC65_NewEditSMETeamMember 
{
    public String detailedComm{get;set;}
    public String pgeMsg {get;set;}
    public boolean renderMembAct{get;set;}
    public boolean isAdmin{get;set;}
    public boolean hasEditAccess{get;set;}
    public boolean isSME{get;set;}
    public boolean isDraft{get;set;}
    public boolean isNew{get;set;}
    
    public RIT_SMETeam__c smeTeam = new RIT_SMETeam__c();
    private String query;
    private List<Sobject> sobjs;
    private OPP_QuoteLInk__Share quteLinkShare = new OPP_QuoteLInk__Share();
    private List<OPP_QuoteLInk__Share> quteLinkShares = new List<OPP_QuoteLInk__Share>();
    private Set<Id> userIds = new Set<Id>();
    private boolean checkFullAccess = false;
    private boolean checkEditAccess =false;    
    public id smeTeamMemberId;
    private OPP_QuoteLink__c quoteLink;
    private String quteOwnerId;
    private String quteStatus;
    public String quteLookupId;
    public String quteLookupName;
    
    //Constructor
    public VFC65_NewEditSMETeamMember(ApexPages.StandardController controller) 
    {
        smeTeam = (RIT_SMETeam__c)controller.getRecord();
        //Getting the Quote Id for a New SME Team Member
        quteLookupId = System.currentPageReference().getParameters().get(CS007_StaticValues__c.getValues('CS007_1').Values__c);         //Quote Link Field Id of SME Team Member       
        System.debug('#####quteLookupId ###'+quteLookupId );
    }
    public void smeDetails(){
       system.debug('--------smeTeam------'+smeTeam+ '------detailedComm-----'+detailedComm);
       //Concatenates the Detailed Comments Text Area with the SME's Detailed Comments
       if(detailedComm!=null && detailedComm.length()>0 && detailedComm!='&nbsp;')
       {
           if(smeTeam.DetailedComments__c == null)
               smeTeam.DetailedComments__c = UserInfo.getName()+' , '+System.Now() +': <BR/>'+ detailedComm;
           else
               smeTeam.DetailedComments__c = UserInfo.getName()+' , '+System.Now() +': <BR/>'+ detailedComm + '<BR/> <BR/>'+smeTeam.DetailedComments__c;     //Appending with the existing comments
       }
        
        //Activates the SME Team Member
        if(renderMembAct == true)
            smeTeam.MemberActivated__c = true;

       system.debug('--------smeTeam------'+smeTeam);        
            
        //Inserts or Updates the SME Team Member Record            
        Database.upsertResult recIns= Database.Upsert(smeTeam, false);
        if(!recIns.isSuccess())
            pgeMsg = recIns.getErrors()[0].getMessage();
        else
            smeTeamMemberId = recIns.getId();
    }
    
    /* This method inserts/updates the corresponding SME Team Member Record */
    public pagereference save()
    {
            smeDetails();
            if(smeTeamMemberId!=null)
                return new ApexPages.StandardController(smeTeam).view();       
            else        
                return null; 
    } 

    //Added saveAndNew for BR-4488 April 2015 release
    public pagereference saveAndNew()
    {
        smeDetails();
        if(smeTeamMemberId!=null){
           System.debug('>>>>>#####Save and new loop');
           Pagereference pageRef = new PageReference(Label.CLAPR15SLS72+quteLookupName+Label.CLAPR15SLS73+quteLookupId+Label.CLAPR15SLS74+quteLookupId+Label.CLAPR15SLS75) ;   
           System.debug('>>>>>#####>>>>>'+pageRef );            
           pageRef.setRedirect(true);
           return pageRef;     
          }else        
                return null; 
    }

    /* This method is called on the page load of VFP65. Checks if the 
       logged in user has appropriate permissions to create/edit the
       SME Team Member.     
    */
    public pagereference checkQuoteLinkStatus()
    {   
        //Query the Owner Id & Quote Status for a new SME Team Member
        //For New record creation
        if(smeTeam.Id==null)
        {
             if(quteLookupId!=null)
             {
                 //Gets the Quote Link Query from the corresponding Custom Settings    
                 query = CS005_Queries__c.getValues('CS005_5').StaticQuery__c +' where Id=\''+quteLookupId+'\'' ;
                 quoteLink = Database.Query(query);
                 system.debug('-------quoteLink-----'+quoteLink );
                 if(quoteLink!=null)
                 {
                     quteLookupName=quoteLink.Name;
                     quteOwnerId = quoteLink.OwnerId;
                     quteStatus = quoteLink.QuoteStatus__c;                    
                 }
             }            
         }
         else
         {
             //Assign the Owner Id and Quote Status for existing SME Team Member
             quteOwnerId = smeTeam.QuoteLink__r.OwnerId;
             quteStatus = smeTeam.QuoteLink__r.QuoteStatus__c;
             quteLookupId = smeTeam.QuoteLink__c;
             //quteLookupName= smeTeam.QuoteLink__r.Name;
         }
         system.debug('------quteOwnerId------'+quteOwnerId);
         system.debug('------quteStatus------'+quteStatus); 
        //isDraft added for April 2015 release 
        if(quteStatus=='Draft')
            isDraft=True;
        //Checks whether the quote link is approved
        if(quteStatus == Label.CL00242)
        {
            pgeMsg = Label.CL00659;            
        }
        else
        {             
            UserRecordAccess recordAccess=[select HasAllAccess,HasEditAccess,RecordId from UserRecordAccess where RecordId=:quteLookupId and UserId=:UserInfo.getUserId()];                                    
                checkFullAccess = recordAccess.HasAllAccess;
                checkEditAccess =recordAccess.HasEditAccess;            
            system.debug('------checkFullAccess------'+checkFullAccess);
            system.debug('------checkEditAccess------'+checkEditAccess); 
                       
            //Checks if its a new SME Team Member
            if(smeTeam.Id==null)
            {
                isNew=true;
                //Set isAdmin if current user is System Admin/Delegated Admin/Delegated Admin+Panda Profile user
                if((UserInfo.getProfileId()==Label.CL00110)|| (UserInfo.getProfileId()==Label.CLOCT13SLS24) || (UserInfo.getProfileId()==Label.CLOCT13SLS25))
                    isAdmin=True;
                if(checkEditAccess || checkFullAccess)
                    hasEditAccess = true;
                else
                    pgeMsg = Label.CL00563;    
                System.debug('>>>>>&&&&>>>isAdmin'+isAdmin+'>>>>>hasEditAccess >>>>'+hasEditAccess );         
            }
            else if(smeTeam.Id!=null && smeTeam.MemberActivated__c == true)
            {
                //Checks if SME Team Member record is edited with Member Activated checkbox checked. 
                //Check if the current user is the SME team memeber                               
                if(smeTeam.TeamMember__c == UserInfo.getUserId())
                    isSME= true;
                //if(Utils_Methods.checkFullAccess(smeTeam.TeamMember__c))
                //Set isAdmin if current user is System Admin/Delegated Admin/Delegated Admin+Panda Profile user
                if((UserInfo.getProfileId()==Label.CL00110)|| (UserInfo.getProfileId()==Label.CLOCT13SLS24) || (UserInfo.getProfileId()==Label.CLOCT13SLS25))
                    isAdmin=True;
                //check if current user is Quote link owner
                if(quteOwnerId == UserInfo.getUserId())
                    hasEditAccess=True;
                if(isSME!=true && isAdmin!= true && hasEditAccess!=true)
                    pgeMsg = Label.CL00563;   
                    
                System.debug('>>>>>>####isAdmin###>>>'+ isAdmin+'>>>>>hasEditAccess>>>'+hasEditAccess+'>>>>>isSME>>>>>');          
            }
            else if(smeTeam.Id!=null && smeTeam.MemberActivated__c ==false)
            {
                //Checks if SME Team Member record is edited with Member Activated checkbox unchecked.                
                if(checkEditAccess || checkFullAccess)
                {
                    pgeMsg = Label.CL00662;
                    renderMembAct = true;
                }  
                else
                    pgeMsg = Label.CL00563;              
            }
        }
        return null;
    }  
}