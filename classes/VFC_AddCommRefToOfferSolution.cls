global with sharing class VFC_AddCommRefToOfferSolution{ 
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public Offer_Lifecycle__c offerRecord{get;set;}
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    Public List<OfferComponents__c> lstOCs {get; set;}
    Public List<Offer_Lifecycle__c > offerList {get; set;}
    public boolean blnIsAccs{get;set;}
    
    public VFC_AddCommRefToOfferSolution(ApexPages.StandardController controller){ 
        blnIsAccs=false;
        set<Id> userAccSet = new set<Id>(); 
        id profileId = [Select Id,Name from Profile where name = 'System Administrator'].id;
        if(ApexPages.currentPage().getParameters().get('oid') != null ){
        offerList = [Select id,ownerid,Segment_Marketing_VP__c,Launch_Owner__c,Launch_Area_Manager__c,Marketing_Director__c,Withdrawal_Owner__c, name from Offer_Lifecycle__c where id =:ApexPages.currentPage().getParameters().get('oid')];
        system.debug('** offerlist size'+ offerList.size());
        if(offerList.size()>0)
        offerRecord = offerList[0];
          userAccSet = new set<Id>{offerRecord.ownerid,offerRecord.Segment_Marketing_VP__c,offerRecord.Launch_Area_Manager__c,offerRecord.Marketing_Director__c,offerRecord.Withdrawal_Owner__c};
        }
        else{   
        offerRecord = (Offer_Lifecycle__c)controller.getRecord();
        }
        lstOCs = new List<OfferComponents__c>(); 
        if(!userAccSet.contains(Userinfo.getuserid()) && userinfo.getProfileid()!=profileId) {
            blnIsAccs=true;
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You dont have access to create Offer Components'));
        }   
    }
    
   public VFC_AddCommRefToOfferSolution(ApexPages.StandardSetController controller) {
        //offerRecord=(Offer_Lifecycle__c)controller.getRecord(); 
 
    }
    
    public String scriteria{get;set;}{    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
    }
    
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
    


        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
            }
        }
        if(!Test.isRunningTest()){    
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
    
 
    
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        system.debug('&&& business'+ business);
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        system.debug('*** @Remote - query'+ query);
        return Database.query(query);       
    }
    
    Public pagereference performCheckbox(){ 
    
       system.debug('*** checkbox - enter');
       if(jsonSelectString=='')    
            jsonSelectString=pageParameters.get('jsonSelectString');   
       
        system.debug('***jsonSelectString'+jsonSelectString);
        List<WS_GMRSearch.gmrDataBean> DBL= (List<WS_GMRSearch.gmrDataBean>) JSON.deserialize(jsonSelectString, List<WS_GMRSearch.gmrDataBean>.class);
        
        for(WS_GMRSearch.gmrDataBean DBL1: DBL)
        {
           
            OfferComponents__c oc = new OfferComponents__c();
            oc.OfferLifeCycle__c = offerRecord.id;
            oc.Business__c = DBL1.businessLine.label;
            oc.ProductLine__c = DBL1.productLine.label;
            oc.ProductFamily__c = DBL1.strategicProductFamily.label;
            oc.Family__c = DBL1.family.label;
            oc.SubFamily__c = DBL1.subFamily.label;
            oc.ProductDescription__c = DBL1.description;
            oc.ProductGroup__c = DBL1.productGroup.label;
            oc.ProductSuccession__c = DBL1.productSuccession.label;
            oc.ComponentsReference__c = DBL1.commercialReference;
            // GMR Code
            if(DBL1.businessLine.value != null)
            oc.GMRCode__c = DBL1.businessLine.value;
            if(DBL1.productLine.value != null)
              oc.GMRCode__c += DBL1.productLine.value;
            if(DBL1.strategicProductFamily.label != null)
              oc.GMRCode__c += DBL1.strategicProductFamily.value;
            if(DBL1.family.label != null)
               oc.GMRCode__c += DBL1.family.value;
            if(DBL1.subFamily.label != null)
               oc.GMRCode__c += DBL1.subFamily.value;
            if(DBL1.productSuccession.label != null)
               oc.GMRCode__c += DBL1.productSuccession.value;
            if(DBL1.productGroup.label != null)
               oc.GMRCode__c += DBL1.productGroup.value;
            // add to the list
            lstOCs.add(oc);
        }
        system.debug('*** lstOCs'+lstOCs);
        try{
            
            insert lstOCs;
            PageReference offerPage;            
            offerPage=new PageReference('/'+ offerRecord.id);                        
            return offerPage;   
                   
        }
        catch(DmlException dmlexception){
            for(integer i = 0;i<dmlexception.getNumDml();i++)
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,dmlexception.getDmlMessage(i)));    
            return null;                  
        }  
    }      
    
 public PageReference pageCancelFunction(){
        PageReference pageResult;
        if(offerRecord.id!=null){
            pageResult = new PageReference('/'+ offerRecord.id);
             
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
}