@isTest
private class VFC_AutoPopulateGMRForValidCR_TEST {
    static testMethod void AutoPopulateGMRTestMethod(){
        Country__c objCountry = Utils_TestMethods.createCountry();
        insert objCountry;

        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.country__c=objCountry.id;
        insert objAccount;

        Contact objContact =Utils_TestMethods.createContact(objAccount.Id,'TestContact');
        objContact.country__c=objCountry.id;
        objContact.CorrespLang__c='french';
        database.insert(objContact);


        test.startTest();
        // BR-1825 Commented By Vimal K (GD India)
        //Case objOpenCase =Utils_TestMethods.createCase(objAccount.id,objContact.id,'Open');
        Case objOpenCase =Utils_TestMethods.createCase(objAccount.id,objContact.id,'In Progress');
        /* BR-4310 Commented By Vimal K (GD India)
        objOpenCase.SupportCategory__c = Label.CL00408;
        objOpenCase.Symptom__c = Label.CL00737;
        objOpenCase.SubSymptom__c = 'sub symptom';
        
        objOpenCase.SupportCategory__c = Label.CL00737;
        objOpenCase.CommercialReference__c = 'Commercial Reference XF-4 5';
        objOpenCase.ProductBU__c = 'Power';
        */
        Database.insert(objOpenCase);
        
        ApexPages.StandardController caseStandardController= new ApexPages.StandardController(objOpenCase);
        VFC_AutoPopulateGMRForValidCR  testGMRAutoPopulate = new  VFC_AutoPopulateGMRForValidCR(caseStandardController);
        string strnoofunrademails='5';
        string strcaseid=objOpenCase.id;
        boolean blnresult;
        blnresult=VFC_AutoPopulateGMRForValidCR.remoteUnReadEmailUpdateOnCase(strcaseid,strnoofunrademails);
        testGMRAutoPopulate.CheckReload();
        testGMRAutoPopulate.checkConsoleValue();        
        
        EmailMessage em = new EmailMessage();
        em.ParentId = objOpenCase.Id;
        em.Status = '0';
        insert em;
        
        testGMRAutoPopulate = new  VFC_AutoPopulateGMRForValidCR(caseStandardController);
        testGMRAutoPopulate.CheckReload();
        //testGMRAutoPopulate.Check();
    }
}