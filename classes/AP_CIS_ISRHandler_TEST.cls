//@Author :- Ashish Sharma
//@Description :- Test class to test the CIS ISR trigger function.
//@ Date 10/09/2014
@isTest
public class AP_CIS_ISRHandler_TEST{
    //Country Members Declaration
    public static List<Country__c> countryList;
    public static Country__c countryInstance1;
    public static Country__c countryInstance2;
    public static Country__c countryInstance3;
    
    //CIS Team Member Declaration
    public static List<CIS_CorrespondentsTeam__c> correspondentTeamList;
    public static CIS_CorrespondentsTeam__c cisCorrespondentTeamInstance1;
    public static CIS_CorrespondentsTeam__c cisCorrespondentTeamInstance2;
    public static CIS_CorrespondentsTeam__c cisCorrespondentTeamInstance3;
    
    //ISR Member Declaration
    public static List<CIS_ISR__c> isrList1;
    public static List<CIS_ISR__c> isrList2;
    public static List<CIS_ISR__c> isrList3;
    
    //Contact Member declaration
    public static Contact con;
    //Queue And Public Group Declaration
    public static Group testGroup1;
    public static Group testGroup2;
    public static Group testGroup3;
    
    public static List<QueueSObject> queuesObjectList;
    //Method to initilize countries records.
    public static void initCountry(){
        countryList = new  List<Country__c>();
        
        countryInstance1 = new Country__c();
        countryInstance1.Name = 'India';
        countryInstance1.CountryCode__c ='IN';
        countryList.add(countryInstance1);
        
        countryInstance2 = new Country__c();
        countryInstance2.Name = 'France';
        countryInstance2.CountryCode__c ='FR';
        countryList.add(countryInstance2);
        
        countryInstance3 = new Country__c();
        countryInstance3.Name = 'Italy';
        countryInstance3.CountryCode__c ='IT';
        countryList.add(countryInstance3);
       
        Database.insert(countryList);
    }
    
    //Method to initilize Contact 
    public static void initContact(){ 
        /*Country__c country = Utils_TestMethods.createCountry();
        country.Name='TestCountry';
        country.CountryCode__c='IN');
        country.InternationalPhoneCode__c='1340';
        insert country;*/        
        StateProvince__c stateProv = Utils_TestMethods.createStateProvince(countryInstance1.Id);
        insert stateProv;
        con= new Contact();
        con.FirstName='conFirstName';
        con.LastName='conLastName';
        con.LocalFirstName__c='conLocalFirstName';
        con.LocalLastName__c='conLocalLastName';
        con.Street__c='Street';
        con.StreetLocalLang__c='StreetLocalLang';
        con.Country__c = countryInstance1.Id;
        con.ZipCode__c='ZipCode';
        con.City__c='City';
        con.LocalCity__c='LocalCity';
        con.StateProv__c=stateProv.Id;
        con.POBox__c='POBox';
        con.email='abc@gmail.com';
        con.WorkPhone__c='+91123456';
        con.WorkPhoneExt__c='123456';
        con.MobilePhone='123456';
        insert con;
    }
    //Method to initilize Queue And Public Groups
    public static void initCISQueuesAndPublicGroups(){
        //User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        testGroup1 = new Group(Name='CIS Team 1',Email='testEmail1@schneider-electric.com', type='Queue');
        insert testGroup1;
        testGroup2 = new Group(Name='CIS Team 2',Email='testEmail2@schneider-electric.com', type='Queue');
        insert testGroup2;
        testGroup3 = new Group(Name='CIS Team 3',Email='testEmail3@schneider-electric.com', type='Queue');
        insert testGroup3;      
        /*GroupMember GM = new GroupMember();
            GM.GroupId = testGroup1.id;
            GM.UserOrGroupId = testGroup2.id;
         insert GM;
        GroupMember GM1 = new GroupMember();
            GM1.GroupId = testGroup2.id;
            GM1.UserOrGroupId = testGroup1.id;
         insert GM1;*/
            
        queuesObjectList = new  List<QueueSObject>{
                new QueueSObject(QueueID = testGroup1.id, SObjectType = 'CIS_CorrespondentsTeam__c'),
                new QueueSObject(QueueID = testGroup1.id, SObjectType = 'CIS_ISR__c'),
                new QueueSObject(QueueID = testGroup1.id, SObjectType = 'CIS_CountryListPrice__c'),
                new QueueSObject(QueueID = testGroup1.id, SObjectType = 'CIS_FieldServiceRepresentative__c'),
                new QueueSObject(QueueID = testGroup1.id, SObjectType = 'CIS_ServiceCenter__c'),
                new QueueSObject(QueueID = testGroup2.id, SObjectType = 'CIS_CorrespondentsTeam__c'),
                new QueueSObject(QueueID = testGroup2.id, SObjectType = 'CIS_ISR__c'),
                new QueueSObject(QueueID = testGroup2.id, SObjectType = 'CIS_CountryListPrice__c'),
                new QueueSObject(QueueID = testGroup2.id, SObjectType = 'CIS_FieldServiceRepresentative__c'),
                new QueueSObject(QueueID = testGroup2.id, SObjectType = 'CIS_ServiceCenter__c'),
                new QueueSObject(QueueID = testGroup3.id, SObjectType = 'CIS_CorrespondentsTeam__c'),
                new QueueSObject(QueueID = testGroup3.id, SObjectType = 'CIS_ISR__c'),
                new QueueSObject(QueueID = testGroup3.id, SObjectType = 'CIS_CountryListPrice__c'),
                new QueueSObject(QueueID = testGroup3.id, SObjectType = 'CIS_FieldServiceRepresentative__c'),
                new QueueSObject(QueueID = testGroup3.id, SObjectType = 'CIS_ServiceCenter__c')};
        System.runAs (new User(Id = UserInfo.getUserId())) {
            insert queuesObjectList;
        } 
    }
    //Method to initilize Correspondent Team Data
    public static void initCISTeam(){
        initCISQueuesAndPublicGroups();
         //Initilizing Correspondent Team Data
        correspondentTeamList = new List<CIS_CorrespondentsTeam__c>();
        
        cisCorrespondentTeamInstance1 = new CIS_CorrespondentsTeam__c();
        cisCorrespondentTeamInstance1.Name ='CIS Team 1';
        cisCorrespondentTeamInstance1.OwnerId = testGroup1.Id; 
        cisCorrespondentTeamInstance1.CIS_FieldsOfActivity__c='ED: Low Voltage';
        cisCorrespondentTeamInstance1.CIS_ScopeOfCountries__c='France';
        correspondentTeamList.add(cisCorrespondentTeamInstance1);
        
        cisCorrespondentTeamInstance2 = new CIS_CorrespondentsTeam__c();
        cisCorrespondentTeamInstance2.Name ='CIS Team 2';
        cisCorrespondentTeamInstance2.OwnerId = testGroup2.Id; 
        cisCorrespondentTeamInstance2.CIS_FieldsOfActivity__c='ED: Medium Voltage';
        cisCorrespondentTeamInstance2.CIS_ScopeOfCountries__c='India';
        correspondentTeamList.add(cisCorrespondentTeamInstance2);
        
        cisCorrespondentTeamInstance3 = new CIS_CorrespondentsTeam__c();
        cisCorrespondentTeamInstance3.Name ='CIS Team 3';
        cisCorrespondentTeamInstance3.OwnerId = testGroup3.Id; 
        cisCorrespondentTeamInstance3.CIS_FieldsOfActivity__c='ED: Medium Voltage';
        cisCorrespondentTeamInstance3.CIS_ScopeOfCountries__c='Italy';
        correspondentTeamList.add(cisCorrespondentTeamInstance3);
        
        Database.insert(correspondentTeamList);   
    }
    
   //Method to initilize ISR Records With FieldofActivity ,EU Country and OEM CIS Team populated
    public static void initISRWithFieldOfActivityEUCountryANDOEMCISTEam(){
       isrList1 = new List<CIS_ISR__c>();
        
        for(Integer i =0;i<201;i++){
            CIS_ISR__c isrInstance = new CIS_ISR__c();
            isrInstance.Name = 'Test ISR'+String.valueOf(i);
            isrInstance.CIS_FieldOfActivity__c='ED: Low Voltage'; 
            isrInstance.CIS_EndUserCountry__c= countryInstance1.Id;
            isrInstance.CIS_OEMCISCorrespondentsTeam__c = cisCorrespondentTeamInstance2.Id;
            isrList1.add(isrInstance);
        }
        for(Integer i =0;i<201;i++){
            CIS_ISR__c isrInstance = new CIS_ISR__c();
            isrInstance.Name = 'Test ISR'+String.valueOf(i);
            isrInstance.CIS_OEMQueueMembersEmails__c ='testEmail1@schneider-electric.com';
            isrInstance.CIS_EUQueueMemberEmail__c ='testEmail2@schneider-electric.com';
            isrInstance.CIS_FieldOfActivity__c='ED: Medium Voltage'; 
            isrInstance.CIS_EndUserCountry__c= countryInstance2.Id;
            isrInstance.CIS_OEMCISCorrespondentsTeam__c = cisCorrespondentTeamInstance1.Id;
            isrList1.add(isrInstance);
        }
        
        Database.insert(isrList1);  
        CIS_ISRComment__c newISRComment = new CIS_ISRComment__c();
        newISRComment.CIS_ISR__c = isrList1[0].Id;
        newISRComment.CIS_TeamToNotify__c ='OEM Correspondent team;End User Correspondent team';
        insert newISRComment;
        
        Database.update([select id from CIS_ISRComment__c where id=:newISRComment.Id]);
    }
    //Method to initilize ISR Records Without FieldofActivity ,EU Country and OEM CIS Team populated
    public static void initISRWithoutFieldOfActivityEUCountryANDOEMCISTEam(){
       isrList2 = new List<CIS_ISR__c>();
        
        for(Integer i =0;i<201;i++){
            CIS_ISR__c isrInstance = new CIS_ISR__c();
            isrInstance.Name = 'Test ISR'+String.valueOf(i);
            //isrInstance.CIS_FieldOfActivity__c='ED: Low Voltage'; 
            isrInstance.CIS_EndUserCountry__c= countryInstance1.Id;
            //isrInstance.CIS_OEMCISCorrespondentsTeam__c = cisCorrespondentTeamInstance2.Id;
            isrList2.add(isrInstance);
        }
        for(Integer i =0;i<201;i++){
            CIS_ISR__c isrInstance = new CIS_ISR__c();
            isrInstance.Name = 'Test ISR'+String.valueOf(i);
            //isrInstance.CIS_FieldOfActivity__c='ED: Medium Voltage'; 
            isrInstance.CIS_EndUserCountry__c= countryInstance2.Id;
            //isrInstance.CIS_OEMCISCorrespondentsTeam__c = cisCorrespondentTeamInstance1.Id;
            isrList2.add(isrInstance);
        }
        
        Database.insert(isrList2);     
    }
    //Method to update ISR records to provide End User Country to ISR. 
    public static void updateISR(){
        List<CIS_ISR__c> isrToUpdate = new List<CIS_ISR__c>([select id,CIS_EndUserCountry__c from CIS_ISR__c]);
        for(CIS_ISR__c isrInstance:isrToUpdate){
            isrInstance.CIS_FieldOfActivity__c = 'ED: Medium Voltage';    
        }
        Database.update(isrToUpdate);
    }
    //Method to initilize ISR Records Without FieldofActivity ,EU Country and OEM CIS Team populated
    public static void initISRWithEUCISTeam(){
       isrList3 = new List<CIS_ISR__c>();
        
        for(Integer i =0;i<201;i++){
            CIS_ISR__c isrInstance = new CIS_ISR__c();
            isrInstance.Name = 'Test ISR'+String.valueOf(i);
            //isrInstance.CIS_FieldOfActivity__c='ED: Low Voltage'; 
            isrInstance.CIS_EndUserCountry__c= countryInstance1.Id;
            isrInstance.CIS_EUCISCorrespondentsTeam__c = cisCorrespondentTeamInstance2.Id;
            isrList3.add(isrInstance);
        }
        for(Integer i =0;i<201;i++){
            CIS_ISR__c isrInstance = new CIS_ISR__c();
            isrInstance.Name = 'Test ISR'+String.valueOf(i);
            //isrInstance.CIS_FieldOfActivity__c='ED: Medium Voltage'; 
            isrInstance.CIS_EndUserCountry__c= countryInstance2.Id;
            isrInstance.CIS_EUCISCorrespondentsTeam__c = cisCorrespondentTeamInstance1.Id;
            
            isrList3.add(isrInstance);
        }
        
        Database.insert(isrList3);     
    }
     //Method to Update ISR Records Country
    public static void updateISRCountry(){
        isrList1 = new List<CIS_ISR__c>();
        
       
            CIS_ISR__c isrInstance1 = new CIS_ISR__c();
            isrInstance1.Name = 'Test ISR'+String.valueOf(1);
            isrInstance1.CIS_FieldOfActivity__c='ED: Low Voltage'; 
            isrInstance1.CIS_EndUserCountry__c= countryInstance1.Id;
            isrInstance1.CIS_OEMQueueMembersEmails__c ='testEmail1@schneider-electric.com;testEmail2@schneider-electric.com';
            isrInstance1.CIS_EUQueueMemberEmail__c ='testEmail2@schneider-electric.com;testEmail1@schneider-electric.com';
            isrInstance1.CIS_OEMCISCorrespondentsTeam__c = cisCorrespondentTeamInstance2.Id;
            isrInstance1.CIS_EUCISCorrespondentsTeam__c = cisCorrespondentTeamInstance2.Id;
            isrInstance1.CIS_EndUserContact__c =con.Id;
            isrList1.add(isrInstance1);
        
            CIS_ISR__c isrInstance2 = new CIS_ISR__c();
            isrInstance2.Name = 'Test ISR'+String.valueOf(2);
            isrInstance2.CIS_FieldOfActivity__c='ED: Medium Voltage'; 
            isrInstance2.CIS_EndUserCountry__c= countryInstance2.Id;
            isrInstance2.CIS_EndUserContact__c =con.Id;
            isrInstance2.CIS_OEMCISCorrespondentsTeam__c = cisCorrespondentTeamInstance1.Id;
            isrInstance2.CIS_EUCISCorrespondentsTeam__c = cisCorrespondentTeamInstance1.Id;
            isrList1.add(isrInstance2);
            
             
        
       Database.insert(isrList1);     
       Set<Id> isrIds =new Set<Id>();
       for(CIS_ISR__c isrInstance:isrList1){
        isrIds.add(isrInstance.id);      
       }
       
       CIS_ISRComment__c newISRComment = new CIS_ISRComment__c();
        newISRComment.CIS_ISR__c = isrList1[0].Id;
        newISRComment.CIS_TeamToNotify__c ='OEM Correspondent team;End User Correspondent team';
        insert newISRComment;
        System.debug('newISRComment:'+newISRComment);
        Database.update([select id from CIS_ISRComment__c where id=:newISRComment.Id]);
        
       
       /*
       List<CIS_ISR__c> isrListToUpdate = new  List<CIS_ISR__c>([select id,CIS_EndUserCountry__c from CIS_ISR__c where id in:isrIds]);
       for(CIS_ISR__c isrInstance:isrListToUpdate){
           if(isrInstance.CIS_EndUserCountry__c ==countryInstance2.Id){
                isrInstance.CIS_EndUserCountry__c=countryInstance1.Id;    
           }
           else{
                isrInstance.CIS_EndUserCountry__c=countryInstance2.Id;         
           }
       }
       Database.update(isrListToUpdate);  
       */
       List<CIS_ISR__c> isrToUpdateToAck = new List<CIS_ISR__c>([select id,CIS_EUContactDate__c,CIS_StInterventionDate__c,CIS_Status__c,CIS_EndUserCountry__c,CIS_SEProductSUsage__c,CIS_UnderWarranty__c,CIS_Liability__c,CIS_Defect__c ,CIS_SEEngineer__c,CIS_EndUserRepresentative__c ,CIS_EnfOfInterventionDate__c ,CIS_NumberOfVisits__c ,CIS_CommissioningDate__c from CIS_ISR__c where id in:isrIds]);
       for(CIS_ISR__c isrInstance:isrToUpdateToAck){
           isrInstance.CIS_EUContactDate__c = system.today();
           isrInstance.CIS_StInterventionDate__c = system.today();
           isrInstance.CIS_Status__c ='Acknowledged';
           isrInstance.CIS_SEProductSUsage__c ='Normal';
            isrInstance.CIS_UnderWarranty__c ='Yes';
            isrInstance.CIS_Liability__c ='SE';
            isrInstance.CIS_Defect__c ='Permanent';
            isrInstance.CIS_SEEngineer__c ='Test SE Engineer';
            isrInstance.CIS_EndUserRepresentative__c='Test End User Rep';
            isrInstance.CIS_EnfOfInterventionDate__c=System.today();
            isrInstance.CIS_NumberOfVisits__c=4;
            isrInstance.CIS_CommissioningDate__c=System.today();
       }
       Database.update(isrToUpdateToAck);
       System.debug('After Ack'+isrToUpdateToAck);
       // Part of April-15
       List<CIS_ISR__c> isrToUpdateCISTeam = new List<CIS_ISR__c>([select id,CIS_Status__c,CIS_EndUserCountry__c,CIS_EUCISCorrespondentsTeam__c from CIS_ISR__c where id in:isrIds]);
       for(CIS_ISR__c isrInstance:isrToUpdateCISTeam){
           isrInstance.CIS_EUCISCorrespondentsTeam__c = cisCorrespondentTeamInstance3.Id;
       }
       Database.update(isrToUpdateCISTeam);
       System.debug('After Ack1'+isrToUpdateCISTeam);
       /*
       List<CIS_ISR__c> isrToUpdateBackToSent = new List<CIS_ISR__c>([select id,CIS_EUContactDate__c,CIS_StInterventionDate__c,CIS_Status__c,CIS_EndUserCountry__c,CIS_SEProductSUsage__c,CIS_UnderWarranty__c,CIS_Liability__c,CIS_Defect__c ,CIS_SEEngineer__c,CIS_EndUserRepresentative__c ,CIS_EnfOfInterventionDate__c ,CIS_NumberOfVisits__c ,CIS_CommissioningDate__c from CIS_ISR__c where id in:isrIds]);
       for(CIS_ISR__c isrInstance:isrToUpdateBackToSent){
           isrInstance.CIS_Status__c ='Sent';
       }
       Database.update(isrToUpdateBackToSent);
       */
       
       List<CIS_ISR__c> isrToUpdateToReported = new List<CIS_ISR__c>([select id,CIS_Status__c,CIS_OEMSatisfactionInterventionLeadtime__c,CIS_OEMSatisfactinQualityOfTheReport__c,CIS_OEMSatisfactonCustomerSatisfaction__c,CIS_ReportValidatedDate__c,CIS_OEMSatisfactionAdditionalComments__c from CIS_ISR__c where id in:isrIds]);
       for(CIS_ISR__c isrInstance:isrToUpdateToReported){
            isrInstance.CIS_Status__c ='Reported';
            isrInstance.CIS_OEMSatisfactionInterventionLeadtime__c='3';
            isrInstance.CIS_OEMSatisfactinQualityOfTheReport__c='4';
            isrInstance.CIS_OEMSatisfactonCustomerSatisfaction__c='Good';
            isrInstance.CIS_ReportValidatedDate__c=System.today();
            isrInstance.CIS_OEMSatisfactionAdditionalComments__c='Test Comments';
       }
       Database.update(isrToUpdateToReported);
       System.debug('isrToUpdateToReported'+isrToUpdateToReported);
       List<CIS_ISR__c> isrToUpdateToReportValidated = new List<CIS_ISR__c>([select id,CIS_DateOfInvoicing__c,CIS_EUSatisfactionAdditionalComments__c,CIS_EUSatisfactionCustomerSatisfaction__c,CIS_EUSatisfactinEnoughInfoForInterv__c,CIS_Status__c,CIS_EUSatisfactionFaultDescription__c from CIS_ISR__c where id in:isrIds]);
        for(CIS_ISR__c isrInstance:isrToUpdateToReportValidated){
            isrInstance.CIS_Status__c='Report validated';
            isrInstance.CIS_EUSatisfactionFaultDescription__c ='Good';
            isrInstance.CIS_EUSatisfactinEnoughInfoForInterv__c='3';
            isrInstance.CIS_EUSatisfactionCustomerSatisfaction__c='Good';
            isrInstance.CIS_EUSatisfactionAdditionalComments__c='Test Additional Cmments';
            isrInstance.CIS_DateOfInvoicing__c=System.today();
        }
        Database.update(isrToUpdateToReportValidated);
       System.debug('isrToUpdateToReportValidated'+isrToUpdateToReportValidated);
        List<CIS_ISR__c> isrToUpdateToBeClosed= new List<CIS_ISR__c>([select id,CIS_Status__c from CIS_ISR__c where id in:isrIds]);
        for(CIS_ISR__c isrInstance:isrToUpdateToBeClosed){
            isrInstance.CIS_Status__c='To be closed';       
        }
        Database.update(isrToUpdateToBeClosed);
        System.debug('isrToUpdateToBeClosed'+isrToUpdateToBeClosed);
        
        List<CIS_ISR__c> isrToUpdateToClosed= new List<CIS_ISR__c>([select id,CIS_Status__c from CIS_ISR__c where id in:isrIds]);
        for(CIS_ISR__c isrInstance:isrToUpdateToClosed){
            isrInstance.CIS_Status__c='Closed';       
        }
        Database.update(isrToUpdateToClosed);
        System.debug('isrToUpdateToClosed'+isrToUpdateToClosed);
    }
    
    public static void updateFlags(){
        isrList1 = new List<CIS_ISR__c>();
        
       
            CIS_ISR__c isrInstance1 = new CIS_ISR__c();
            isrInstance1.Name = 'Test ISR'+String.valueOf(1);
            isrInstance1.CIS_FieldOfActivity__c='ED: Low Voltage'; 
            isrInstance1.CIS_EndUserCountry__c= countryInstance1.Id;
            isrInstance1.CIS_OEMCISCorrespondentsTeam__c = cisCorrespondentTeamInstance2.Id;
            isrInstance1.CIS_EndUserContact__c =con.Id;
            isrList1.add(isrInstance1);
        
            CIS_ISR__c isrInstance2 = new CIS_ISR__c();
            isrInstance2.Name = 'Test ISR'+String.valueOf(2);
            isrInstance2.CIS_FieldOfActivity__c='ED: Medium Voltage'; 
            isrInstance2.CIS_EndUserCountry__c= countryInstance2.Id;
            isrInstance2.CIS_EndUserContact__c =con.Id;
            isrInstance2.CIS_OEMCISCorrespondentsTeam__c = cisCorrespondentTeamInstance1.Id;
            isrList1.add(isrInstance2);
                        
           Database.insert(isrList1);     
           Set<Id> isrIds =new Set<Id>();
           for(CIS_ISR__c isrInstance:isrList1){
            isrIds.add(isrInstance.id);      
           }
            
        
        List<CIS_ISR__c> isrToUpdateFlags= new List<CIS_ISR__c>([select id,CIS_Status__c,Sentmorethan2days__c,ReportValidatedmorethan7days__c,Acknowledgedmorethan7days__c from CIS_ISR__c where id in:isrIds]);
        for(CIS_ISR__c isrInstance:isrToUpdateFlags){
            isrInstance.Sentmorethan2days__c=True;
            isrInstance.ReportValidatedmorethan7days__c=True;
            isrInstance.Acknowledgedmorethan7days__c=True;          
        }
        Database.update(isrToUpdateFlags);
    }
    
    //ISR Unit Test 1
   /* @isTest
    public static void muUnitTest1(){
        Test.startTest();
            initCountry(); 
            initCISTeam();
            initISRWithFieldOfActivityEUCountryANDOEMCISTEam();
        Test.stopTest();
    }*/
    @isTest
    public static void muUnitTest2(){
        
            User currentUser = [select id,CIS_Bypass_Validation_Rules__c from User where id=:UserInfo.getUserId() limit 1];
            currentUser.CIS_Bypass_Validation_Rules__c = true;
            Update currentUser;
            initCountry(); 
            initCISTeam();
            initContact();
        Test.startTest();
            updateISRCountry();
        Test.stopTest();
    }
    @isTest
    public static void muUnitTest5(){
        Test.startTest();
            User currentUser = [select id,CIS_Bypass_Validation_Rules__c from User where id=:UserInfo.getUserId() limit 1];
            currentUser.CIS_Bypass_Validation_Rules__c = true;
            Update currentUser;
            initCountry(); 
            initCISTeam();
            initContact();
            updateFlags();
        Test.stopTest();
    }
    
    
    @isTest
    public static void myunitTest3(){
        Test.startTest();
            initCountry(); 
            initCISTeam();
            initISRWithoutFieldOfActivityEUCountryANDOEMCISTEam();
        Test.stopTest();    
    }
    
     /*
    
    @isTest
    public static void myunitTest4(){
        Test.startTest();
            initCountry(); 
            initCISTeam();
            initISRWithEUCISTeam();
        Test.stopTest();    
    }
    */
}