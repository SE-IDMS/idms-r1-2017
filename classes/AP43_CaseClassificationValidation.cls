public class AP43_CaseClassificationValidation
{
    // Static variables are used in order to use only one getDescribe by transacation; Governor limit = 10
    static public Set<String> categoryPicklistEntries; // Picklist entries for case category
    static public Set<String> reasonPicklistEntries; // Picklist entries for case reason
    static public Set<String> subreasonPicklistEntries; // Picklist entries for case sub reason

    // Main method to validate a case classification against case's picklist value
    static public String validate(CaseClassification__c aCaseClass)
    {
       String errorMessage; // Resulting error message, null if valid
       Boolean isClassificationValid=false;
       
       System.debug('AP43.validate.INFO - Method called.');
       
       // verify that the category is valid, the reason is valid and the sub reason is valid (if not null)
       // Level above another one which is not null, cannot be null e.g. Category is null but reason filled is not valid
       if(aCaseClass.Category__c !=null) { 
           if(validateCagegory(aCaseClass)){
               if(aCaseClass.Reason__c == null){
                   if(aCaseClass.SubReason__c == null){
                       System.debug('AP43.validate.SUCCESS - Category is valid; Reason is null; Sub reason is null.');
                       isClassificationValid = true;
                   }
                   else {
                       System.debug('AP43.validate.ERROR - Category is valid; Reason is null; Sub reason is not null.');
                       errorMessage = 'sub reason cannot be defined for a null reason.';
                   }
               }
               else if(validateReason(aCaseClass)) {
                   if(aCaseClass.SubReason__c == null) {
                       System.debug('AP43.validate.SUCCESS - Category is valid; Reason is valid; Sub reason is null.');
                       isClassificationValid = true;
                   }
                   else if(validateSubReason(aCaseClass)){
                       System.debug('AP43.validate.SUCCESS - Category is valid; Reason is valid; Sub reason is valid.');
                       isClassificationValid = true;
                   }
                   else {
                       System.debug('AP43.validate.ERROR - Category is valid; Reason is valid; Sub reason is invalid.');
                       errorMessage = 'sub reason value is not valid.';
                   }
               }
               else {
                   System.debug('AP43.validate.ERROR - Category is valid; Reason is invalid;');
                   errorMessage = 'reason value is not valid.';
               }
           }
           else {
               System.debug('AP43.validate.ERROR - Category is invalid;');
               errorMessage = 'category value is not valid.';
           }
       }   
       else {
           System.debug('AP43.validate.SUCCESS - Category is null; Reason is null; Sub reason is not null.');
           isClassificationValid=true;
       }
       
       if(isClassificationValid ) {
           System.debug('AP43.validate.SUCCESS - Case classification is valid.');
           errorMessage = null;
       }
       else {
           System.debug('AP43.validate.ERROR - Case classification is invalid.');
       }
       
       System.debug('AP43.validate.INFO - Error message: ' + errorMessage); 
       System.debug('AP43.validate.INFO - End of method.'); 
       
       return errorMessage;
    }

    // Validates that value of the category is valid according to the case's related field
    static public Boolean validateCagegory(CaseClassification__c aCaseClass)
    {
        Boolean isCategoryValid=false;
    
        System.debug('AP43.validateCagegory.INFO - Method called.');
               
        try {
            // If empty, fill the set with all picklist values
            if(categoryPicklistEntries == null){
                categoryPicklistEntries = new Set<String>();
            
                for(Schema.PicklistEntry picklistEntry:Case.SupportCategory__c.getDescribe().getPicklistValues()){
                    categoryPicklistEntries.add(picklistEntry.getValue());
                }
            }
            
            System.debug('AP43.validateCagegory.INFO - List of valid categories =' + categoryPicklistEntries);
            
            // Check if the category is contained in the set of valid values
            if(categoryPicklistEntries.contains(aCaseClass.category__c)){
                System.debug('AP43.validateCagegory.SUCCESS - Category is valid ' );
                isCategoryValid=true;
            }
            else {
                System.debug('AP43.validateCagegory.ERROR - Category is invalid ' );
            }
        }
        catch(Exception e){
            System.debug('AP43.validateCategory.'+e.getTypeName()+'- '+ e.getMessage());
            System.debug('AP43.validateCategory.'+e.getTypeName()+'- Line = '+ e.getLineNumber());
            System.debug('AP43.validateCategory.'+e.getTypeName()+'- Stack trace = '+ e.getStackTraceString());
        }
        
        System.debug('AP43.validateCagegory.INFO - End of method.');
        return isCategoryValid;
    }
    
    // Validates that value of the reason is valid according to the case's related field
    static public Boolean validateReason(CaseClassification__c aCaseClass)
    {
        Boolean isReasonValid=false;
    
        System.debug('AP43.validateReason.INFO - Method called.');
        try {
            // If empty, fill the set with all picklist values
            if(ReasonPicklistEntries == null){
                ReasonPicklistEntries = new Set<String>();
            
                for(Schema.PicklistEntry picklistEntry:Case.Symptom__c.getDescribe().getPicklistValues()){
                    ReasonPicklistEntries.add(picklistEntry.getValue());
                }
            }
            
            System.debug('AP43.validateCagegory.INFO - List of valid reasons =' + ReasonPicklistEntries);
            
            // Check if the Reason is contained in the set of valid values
            if(ReasonPicklistEntries.contains(aCaseClass.Reason__c)){
                System.debug('AP43.validateReason.SUCCESS - Reason is valid ' );
                isReasonValid=true;
            }
            else {
                System.debug('AP43.validateReason.ERROR - Reason is invalid.' );
            }
        }
        catch(Exception e){
            System.debug('AP43.validateReason.'+e.getTypeName()+'- '+ e.getMessage());
            System.debug('AP43.validateReason.'+e.getTypeName()+'- Line = '+ e.getLineNumber());
            System.debug('AP43.validateReason.'+e.getTypeName()+'- Stack trace = '+ e.getStackTraceString());
        }
        
        System.debug('AP43.validateReason.INFO - End of method.');
        return isReasonValid;
    }
    
        // Validates that value of the SubReason is valid according to the case's related field
    static public Boolean validateSubReason(CaseClassification__c aCaseClass)
    {
        Boolean isSubReasonValid=false;
    
        System.debug('AP43.validateSubReason.INFO - Method called.');
        try {
            // If empty, fill the set with all picklist values
            if(SubReasonPicklistEntries == null){
                SubReasonPicklistEntries = new Set<String>();
            
                for(Schema.PicklistEntry picklistEntry:Case.SubSymptom__c.getDescribe().getPicklistValues()){
                    SubReasonPicklistEntries.add(picklistEntry.getValue());
                }
            }
            
            System.debug('AP43.validateCagegory.INFO - List of valid sub reasons =' + SubReasonPicklistEntries);
            
            // Check if the SubReason is contained in the set of valid values
            if(SubReasonPicklistEntries.contains(aCaseClass.SubReason__c)){
                System.debug('AP43.validateSubReason.SUCCESS - Sub reason is valid for case classification' );
                isSubReasonValid =true;
            }
            else {
                System.debug('AP43.validateSubReason.ERROR - Sub reason is invalid for case classification' );
            }
        }
        catch(Exception e){
            System.debug('AP43.validateSubReason.'+e.getTypeName()+'- '+ e.getMessage());
            System.debug('AP43.validateSubReason.'+e.getTypeName()+'- Line = '+ e.getLineNumber());
            System.debug('AP43.validateSubReason.'+e.getTypeName()+'- Stack trace = '+ e.getStackTraceString());
        }
        
        System.debug('AP43.validateSubReason.INFO - End of method.');
        return isSubReasonValid;
    }
}