/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 18/10/2010
    Description     : Controller takes an 'Individual Commercial Action Plan' record id and creates a new 'Platform Scoring' 
                        record for each account that the user defined in the assignedTo__c field on the Individual Commercial 
                        Action Plan record either owns or belongs to the Account Team.
*/

/*
Modified History:
-----------------
Date: Nov 2nd
Author: Siddharatha Nagavarapu
Purpose: For the UAT imporvement DEF -116

Date: May 27nd
Author: Kaushal Kishore
Purpose: For July 2015 minor release BR-7606

Date: 30/01/2015
Purpose: Removed filter Account Relationship Status not “Disengaged” and added Primary Coverage Model as “Sales Rep” or “Inside Sales”. 

Date: Q3 2016 release
Purpose: Defaulting PAM,Direct Sales, Indirect Sales and Rating for the platforming scoring while generating.
- If Assigned to is Account Owner or External Sales Responsible (in case of Italy it is Owner field) then Copy the PAM,Direct Sales, Indirect Sales and Rating from Account Master Profile.
- If Assigned to is team member then
   - Copy PAM,Direct Sales and Indirect Sales from the user BU split of AMP based on the mapping in CS_UserBUGMRBusinessUnitMapping
   - Copy Total PAM,Direc Sales and Indirect Sales from AMP if the user BU is not mapped in CS_UserBUGMRBusinessUnitMapping
   -  Copy rating from latest record of PS having CAP startdate in last 18 months. If he doesnt have any record  ,
    then copy rating from another user Platforming scoring record having same BU as Assigned To for the same account using Tech_AccAssignedToBUMap__c

*/
public with sharing class VFC04_SFETargetting {
    public boolean displayerrorMessage{get;set;}{displayerrorMessage=true;}
    private SFE_IndivCAP__c platformingCampaign{get;set;}
    //private final Integer ACCOUNT_LIMIT = 12000;             // we insert a max of 1000 Platforming scoring records, so it is unnecessary to return more than 1000 Accounts
    //private final Integer ACCOUNT_TEAM_LIMIT = 1000;        // we insert a max of 1000 Platforming scoring records, so it is unnecessary to return more than 1000 Account Team records
    //private Integer NEW_RECORD_LIMIT = 10000;                // governor limit of 1000 records insert exists, we don't make final so when running Test we can alter this number for coverage purposes.
    public boolean flag{get;set;}
    Public string buttonLabel{get;set;}
    public boolean check1,check2;
    public list<SFE_IndivCAP__c> pc;
    public static Map<Integer,List<SFE_PlatformingScoring__c>> BatchUpdateMap{get;set;}{BatchUpdateMap=new Map<Integer,List<SFE_PlatformingScoring__c>>();}
    public Integer processhere=100;
    public Integer BatchSize=3000;

    public VFC04_SFETargetting(){
        pc = [select id, AssignedTo__c,TECH_SendEmail__c,OwnerId  from SFE_IndivCAP__c where id = :ApexPages.currentPage().getParameters().get('id') ];
        //and OwnerId = :UserInfo.getUserId()];
        //Dec -11 release addition (start)
        String profileName=[select Name from profile where Id=:UserInfo.getProfileId()].Name;//the current users profile is compared with the prefix text in the custom label
        List<String> profilePrefixList=new List<String>();
        profilePrefixList=System.Label.CL00632.split(',');
        for(String profilePrefix : profilePrefixList)
        {
            if(pc.size()>0 && (UserInfo.getUserId().equals(pc.get(0).OwnerId) || profileName.startsWith(profilePrefix)))
            {//the existing conditions are retained
                flag=true;
                check1=false;
                check2=false;
                buttonLabel=Label.CL00005;
                displayerrorMessage=false;              
                platformingCampaign=pc.get(0);
            }
            else
            {
                buttonLabel=Label.CL00267;
            }
        }
        //dec -11 additon (end)
    }
    
    public PageReference createPlatformScorings(){
       // if(Test.isRunningTest())
         //   NEW_RECORD_LIMIT = 10;
            
        PageReference pageRef = null;
        Database.SaveResult[] lsr = new List<Database.SaveResult>();
        try{
            if(platformingCampaign != null){                
                if(platformingCampaign.AssignedTo__c != null){
                    Recordtype capRecordType = [Select id from RecordType where developername =  'CAPAfterGeneratingAccounts' limit 1];
                    SFE_IndivCAP__c cap=new SFE_IndivCAP__c(Id=platformingCampaign.Id,recordtypeID=capRecordType.Id);
                    update cap; //cant change weights at CAP level
                    List<AccountMasterProfile__c> amplist=new List<AccountMasterProfile__c>();
                    List<AccountMasterProfilePAM__c> amppamlist=new List<AccountMasterProfilePAM__c>(); 
                    Set<Id> ownerAccountIdLsit=new Set<Id>();
                    Set<Id> ownerAccTeamMemIdLsit=new Set<Id>();
                    Set<Id> acctLsit=new Set<Id>();
                    Map<Id, SFE_PlatformingScoring__c> mapPlatformRecords = new Map<Id, SFE_PlatformingScoring__c>();
                    String GMRBU;
                    Map<Id,AccountMasterProfile__c> ampMap=new Map<Id,AccountMasterProfile__c>();
                    Map<Id,AccountMasterProfilePAM__c> ampPamMap=new Map<Id,AccountMasterProfilePAM__c>();
                    Map<String,Decimal> currencyMap = new Map<String,Decimal>();
                    //Fetch currecy conversion rate
                    for(CurrencyType ct:[SELECT ConversionRate,DecimalPlaces,IsActive,IsoCode FROM CurrencyType WHERE IsActive = true])            
                        currencyMap.put(ct.IsoCode,ct.ConversionRate);  
                    User u=[select UserBusinessUnit__c,CurrencyIsoCode,DefaultCurrencyIsoCode  from user where Id=:platformingCampaign.AssignedTo__c]; //fetch Assigned To user BU
                    //User curuser=[select CurrencyIsoCode,Name,DefaultCurrencyIsoCode from user where Id=:userinfo.getUserId()]; //fetch current user currency
                    System.debug('>>>>> current user'+u);
                    // Fetch the GMR BU Mapping from the custom setting 
                    CS_UserBUGMRBusinessUnitMapping__c uBUGMRBUMap= CS_UserBUGMRBusinessUnitMapping__c.getValues(u.UserBusinessUnit__c);
                    if(uBUGMRBUMap!=null)
                        GMRBU= uBUGMRBUMap.GMRBusinessUnit__c;

                    // retrieve all accounts that AssignedTo user owns
                    for(Account a : [select id from Account where ((OwnerId = :platformingCampaign.AssignedTo__c and (ExternalSalesResponsible__c=null OR ExternalSalesResponsible__c='')) OR (ExternalSalesResponsible__c!=null and ExternalSalesResponsible__c=:platformingCampaign.AssignedTo__c)) and ToBeDeleted__c=false and Inactive__c=false and recordtypeId!=:Label.CLDEC12ACC01 and (PrimaryRelationshipLeader__c=:Label.CLDEC14SLS02 OR PrimaryRelationshipLeader__c=:Label.CLDEC14SLS03 OR PrimaryRelationshipLeader__c=:Label.CLQ316SLS032) limit :System.limits.getLimitQueryRows()]){
                    //    if(mapPlatformRecords.size() == NEW_RECORD_LIMIT)
                      //      break;
                        mapPlatformRecords.put(a.Id, new SFE_PlatformingScoring__c(IndivCAP__c = platformingCampaign.Id, PlatformedAccount__c = a.Id,CurrencyIsocode=u.DefaultCurrencyIsoCode));
                        ownerAccountIdLsit.add(a.Id);
                    }
                    
                    // return all accounts were the AssignedTo user belongs to the account team
                    for(AccountTeamMember atm : [select id, accountId from AccountTeamMember where UserId = :platformingCampaign.AssignedTo__c and Account.OwnerId != :platformingCampaign.AssignedTo__c and Account.ExternalSalesResponsible__c!= :platformingCampaign.AssignedTo__c and Account.ToBeDeleted__c=false and Account.Inactive__c=false and (Account.PrimaryRelationshipLeader__c=:Label.CLDEC14SLS02 OR Account.PrimaryRelationshipLeader__c=:Label.CLDEC14SLS03 OR Account.PrimaryRelationshipLeader__c=:Label.CLQ316SLS032 ) limit :System.limits.getLimitQueryRows()]){
                       // if(mapPlatformRecords.size() == NEW_RECORD_LIMIT)
                         //   break;
                        mapPlatformRecords.put(atm.accountId, new SFE_PlatformingScoring__c(IndivCAP__c = platformingCampaign.Id, PlatformedAccount__c = atm.accountId,CurrencyIsocode=u.DefaultCurrencyIsoCode));
                        ownerAccTeamMemIdLsit.add(atm.accountId);
                    }
                    
                    acctLsit.addAll(ownerAccountIdLsit);
                    acctLsit.addAll(ownerAccTeamMemIdLsit);
                    
                    // fetch AMP and AMPPAM data
                    amplist=[select CurrencyIsoCode,TotalPAM__c,Directsales__c,Indirectsales__c, Q1Rating__c, Q2Rating__c, Q3Rating__c, Q4Rating__c, Q5Rating__c, Q6Rating__c, Q7Rating__c, Q8Rating__c, Account__c from AccountMasterProfile__c where Account__c in :acctLsit limit :System.limits.getLimitQueryRows()];
                    if(GMRBU!=null){
                        amppamlist=[select AccountMasterProfile__c,AccountMasterProfile__r.Account__c,DirectSales__c, IndirectSales__c, PAM__c, GMRBusinessUnit__c from AccountMasterProfilePAM__c where AccountMasterProfile__c in :amplist and GMRBusinessUnit__c=:GMRBU and Active__c=TRUE limit :System.limits.getLimitQueryRows()];
                        for(AccountMasterProfilePAM__c amppam:amppamlist)
                            ampPamMap.put(amppam.AccountMasterProfile__r.Account__c,amppam);
                    }
                    
                    List<SFE_PlatformingScoring__c> lastyearPSlist=new List<SFE_PlatformingScoring__c>(); // fetch Platforming scoring records for the team members
                    lastyearPSlist=[select CurrencyIsoCode,createddate,PlatformedAccount__c,AssignedTo__c,IndivCAP__r.AssignedTo__c,IndivCAP__r.EndDate__c,IndivCAP__r.StartDate__c,Tech_AccAssignedToBUMap__c, PAMPlaforming__c, DirectSalesPlatforming__c, IndirectSalesPlatforming__c,Q1Rating__c, Q2Rating__c, Q3Rating__c,Q4Rating__c,Q5Rating__c,Q6Rating__c,Q7Rating__c,Q8Rating__c from SFE_PlatformingScoring__c where (IndivCAP__r.Status__c=:Label.CL00444 OR IndivCAP__r.Status__c=:Label.CLQ316SLS038) and  IndivCAP__r.StartDate__c=LAST_N_DAYS:548 and PlatformedAccount__c in :acctLsit limit :System.limits.getLimitQueryRows()];
                    Map<Id,SFE_PlatformingScoring__c> lastyearMap=new Map<Id,SFE_PlatformingScoring__c>();
                    Map<String,SFE_PlatformingScoring__c> latestRecAMPMap= new Map<String,SFE_PlatformingScoring__c>();

                    for(AccountMasterProfile__c amp:amplist)
                        ampMap.put(amp.Account__c,amp);

                    for(SFE_PlatformingScoring__c ps:lastyearPSlist){
                        if(ps.IndivCAP__r.AssignedTo__c==platformingCampaign.AssignedTo__c){
                            if(lastyearMap.containsKey(ps.PlatformedAccount__c)){
                                if(lastyearMap.get(ps.PlatformedAccount__c).IndivCAP__r.StartDate__c < ps.IndivCAP__r.StartDate__c) //take latest  record
                                   lastyearMap.put(ps.PlatformedAccount__c,ps); 
                            }
                            else
                              lastyearMap.put(ps.PlatformedAccount__c,ps) ;
                       }
                       if(latestRecAMPMap.containsKey(ps.Tech_AccAssignedToBUMap__c)){
                           if(latestRecAMPMap.get(ps.Tech_AccAssignedToBUMap__c).IndivCAP__r.StartDate__c< ps.IndivCAP__r.StartDate__c) //take latest record based on Tech_AccAssignedToBUMap__c
                               latestRecAMPMap.put(ps.Tech_AccAssignedToBUMap__c,ps); 
                       }
                       else
                           latestRecAMPMap.put(ps.Tech_AccAssignedToBUMap__c,ps) ;
                    }
                    
                    for(Id acc:acctLsit){
                        System.debug('>>>>>><<<<'+ampmap.containsKey(acc));
                        if(ownerAccountIdLsit.contains(acc) && mapPlatformRecords.containsKey(acc) && ampmap.containsKey(acc)){
                            if(ampmap.get(acc).TotalPAM__c!=null)
                                mapPlatformRecords.get(acc).PAMPlaforming__c= ampmap.get(acc).TotalPAM__c * currencyMap.get(u.DefaultCurrencyIsoCode)/currencyMap.get(ampmap.get(acc).CurrencyIsoCode);
                            else
                               mapPlatformRecords.get(acc).PAMPlaforming__c=null;
                            if(ampmap.get(acc).DirectSales__c!=null) 
                                mapPlatformRecords.get(acc).DirectSalesPlatforming__c =ampmap.get(acc).DirectSales__c * currencyMap.get(u.DefaultCurrencyIsoCode)/currencyMap.get(ampmap.get(acc).CurrencyIsoCode);
                            else
                                mapPlatformRecords.get(acc).DirectSalesPlatforming__c =null;
                            if(ampmap.get(acc).IndirectSales__c!=null)
                                mapPlatformRecords.get(acc).IndirectSalesPlatforming__c=ampmap.get(acc).IndirectSales__c * currencyMap.get(u.DefaultCurrencyIsoCode)/currencyMap.get(ampmap.get(acc).CurrencyIsoCode);
                            else
                               mapPlatformRecords.get(acc).IndirectSalesPlatforming__c=null; 
                            mapPlatformRecords.get(acc).Q1Rating__c=ampmap.get(acc).Q1Rating__c;
                            mapPlatformRecords.get(acc).Q2Rating__c=ampmap.get(acc).Q2Rating__c;
                            mapPlatformRecords.get(acc).Q3Rating__c=ampmap.get(acc).Q3Rating__c;
                            mapPlatformRecords.get(acc).Q4Rating__c=ampmap.get(acc).Q4Rating__c;
                            mapPlatformRecords.get(acc).Q5Rating__c=ampmap.get(acc).Q5Rating__c;
                            mapPlatformRecords.get(acc).Q6Rating__c=ampmap.get(acc).Q6Rating__c;
                            mapPlatformRecords.get(acc).Q7Rating__c=ampmap.get(acc).Q7Rating__c;
                            mapPlatformRecords.get(acc).Q8Rating__c=ampmap.get(acc).Q8Rating__c;
                            mapPlatformRecords.get(acc).Tech_ReferenceRecord__c='OwnerMapping_'+ampmap.get(acc).Id;
                        }
                       
                        else if(ownerAccTeamMemIdLsit.contains(acc)){
                            if(mapPlatformRecords.containsKey(acc)){
                                mapPlatformRecords.get(acc).Tech_ReferenceRecord__c='TeamMemberMapping';
                                System.debug('>>>>>>>GMRBU '+GMRBU);
                                if(GMRBU!=null && ampPamMap.Containskey(acc)){
                                     // copy PAM and sales values from the respective user BU
                                     if(ampPamMap.get(acc).PAM__c!=null)
                                         mapPlatformRecords.get(acc).PAMPlaforming__c=ampPamMap.get(acc).PAM__c* currencyMap.get(u.DefaultCurrencyIsoCode)/currencyMap.get(ampPamMap.get(acc).CurrencyIsoCode);
                                     else
                                        mapPlatformRecords.get(acc).PAMPlaforming__c=null; 
                                     if(ampPamMap.get(acc).DirectSales__c!=null)
                                         mapPlatformRecords.get(acc).DirectSalesPlatforming__c =ampPamMap.get(acc).DirectSales__c * currencyMap.get(u.DefaultCurrencyIsoCode)/currencyMap.get(ampPamMap.get(acc).CurrencyIsoCode);
                                     else
                                        mapPlatformRecords.get(acc).DirectSalesPlatforming__c =null;
                                     if(ampPamMap.get(acc).IndirectSales__c!=null)
                                         mapPlatformRecords.get(acc).IndirectSalesPlatforming__c=ampPamMap.get(acc).IndirectSales__c * currencyMap.get(u.DefaultCurrencyIsoCode)/currencyMap.get(ampPamMap.get(acc).CurrencyIsoCode);
                                     else
                                         mapPlatformRecords.get(acc).IndirectSalesPlatforming__c=null;
                                     mapPlatformRecords.get(acc).Tech_ReferenceRecord__c=mapPlatformRecords.get(acc).Tech_ReferenceRecord__c+'_'+GMRBU+'_PAMrec='+ampPamMap.get(acc).Id;
                                }
                                else if(GMRBU==null && ampmap.containsKey(acc)){ // if the user BU doesnt have mapping wih GMR BU then default PAM and sales from AMP Total PAM and Sales
                                     if(ampmap.get(acc).TotalPAM__c!=null)
                                         mapPlatformRecords.get(acc).PAMPlaforming__c=ampmap.get(acc).TotalPAM__c * currencyMap.get(u.DefaultCurrencyIsoCode )/currencyMap.get(ampmap.get(acc).CurrencyIsoCode); 
                                     else
                                         mapPlatformRecords.get(acc).PAMPlaforming__c=null;
                                     if(ampmap.get(acc).DirectSales__c!=null)
                                         mapPlatformRecords.get(acc).DirectSalesPlatforming__c =ampmap.get(acc).DirectSales__c *  currencyMap.get(u.DefaultCurrencyIsoCode )/currencyMap.get(ampmap.get(acc).CurrencyIsoCode);
                                     else
                                         mapPlatformRecords.get(acc).DirectSalesPlatforming__c=null;
                                     if(ampmap.get(acc).IndirectSales__c!=null)
                                         mapPlatformRecords.get(acc).IndirectSalesPlatforming__c=ampmap.get(acc).IndirectSales__c * currencyMap.get(u.DefaultCurrencyIsoCode )/currencyMap.get(ampmap.get(acc).CurrencyIsoCode);
                                     else
                                         mapPlatformRecords.get(acc).IndirectSalesPlatforming__c=null;
                                     mapPlatformRecords.get(acc).Tech_ReferenceRecord__c=mapPlatformRecords.get(acc).Tech_ReferenceRecord__c+'_PAMrec='+ampmap.get(acc).Id;
                                }
                                if(lastyearMap.ContainsKey(acc)){
                                    mapPlatformRecords.get(acc).Q1Rating__c=lastyearMap.get(acc).Q1Rating__c;
                                    mapPlatformRecords.get(acc).Q2Rating__c=lastyearMap.get(acc).Q2Rating__c;
                                    mapPlatformRecords.get(acc).Q3Rating__c=lastyearMap.get(acc).Q3Rating__c;
                                    mapPlatformRecords.get(acc).Q4Rating__c=lastyearMap.get(acc).Q4Rating__c;
                                    mapPlatformRecords.get(acc).Q5Rating__c=lastyearMap.get(acc).Q5Rating__c;
                                    mapPlatformRecords.get(acc).Q6Rating__c=lastyearMap.get(acc).Q6Rating__c;
                                    mapPlatformRecords.get(acc).Q7Rating__c=lastyearMap.get(acc).Q7Rating__c;
                                    mapPlatformRecords.get(acc).Q8Rating__c=lastyearMap.get(acc).Q8Rating__c;
                                    mapPlatformRecords.get(acc).Tech_ReferenceRecord__c=mapPlatformRecords.get(acc).Tech_ReferenceRecord__c+'_Ratingrec='+lastyearMap.get(acc).Id;
                                }
                                else if(latestRecAMPMap.ContainsKey(acc+'_'+u.UserBusinessUnit__c)){
                                    mapPlatformRecords.get(acc).Q1Rating__c=latestRecAMPMap.get(acc+'_'+u.UserBusinessUnit__c).Q1Rating__c;
                                    mapPlatformRecords.get(acc).Q2Rating__c=latestRecAMPMap.get(acc+'_'+u.UserBusinessUnit__c).Q2Rating__c;
                                    mapPlatformRecords.get(acc).Q3Rating__c=latestRecAMPMap.get(acc+'_'+u.UserBusinessUnit__c).Q3Rating__c;
                                    mapPlatformRecords.get(acc).Q4Rating__c=latestRecAMPMap.get(acc+'_'+u.UserBusinessUnit__c).Q4Rating__c;
                                    mapPlatformRecords.get(acc).Q5Rating__c=latestRecAMPMap.get(acc+'_'+u.UserBusinessUnit__c).Q5Rating__c;
                                    mapPlatformRecords.get(acc).Q6Rating__c=latestRecAMPMap.get(acc+'_'+u.UserBusinessUnit__c).Q6Rating__c;
                                    mapPlatformRecords.get(acc).Q7Rating__c=latestRecAMPMap.get(acc+'_'+u.UserBusinessUnit__c).Q7Rating__c;
                                    mapPlatformRecords.get(acc).Q8Rating__c=latestRecAMPMap.get(acc+'_'+u.UserBusinessUnit__c).Q8Rating__c;
                                    mapPlatformRecords.get(acc).Tech_ReferenceRecord__c=mapPlatformRecords.get(acc).Tech_ReferenceRecord__c+'_Ratingrec='+latestRecAMPMap.get(acc+'_'+u.UserBusinessUnit__c).Id;
                                }
                            }
                        }
                    }
            
                    // create the Platform Scoring records
                    if(!mapPlatformRecords.isEmpty()){
                            //Modified By Abhishek for SLS_45 and SLS_46
                            flag=false;
                            pc[0].TECH_SendEmail__c=true;       
                            //END
                            //added for sept release
                            
                            List<SFE_PlatformingScoring__c> tobeprocessedhere=new List<SFE_PlatformingScoring__c>();
                            List<SFE_PlatformingScoring__c> pstobeprocessedinfuturemethod=new List<SFE_PlatformingScoring__c>();

                            Integer counter=0;
                            
                            for(Id accountId :mapPlatformRecords.keySet()){
                            if(counter<=processhere){
                                tobeprocessedhere.add(mapPlatformRecords.get(accountId));
                                counter++;
                            }
                            else
                                pstobeprocessedinfuturemethod.add(mapPlatformRecords.get(accountId)) ;                          
                            }
                            
                          lsr = Database.insert(tobeprocessedhere, false);
                         
                        // process any errors
                        for(Database.SaveResult sr : lsr){
                            if(!sr.isSuccess()){
                                if(sr.getErrors()[0].getStatusCode() == StatusCode.DUPLICATE_VALUE){
                                    check1=true;
                                    //Modified By Abhishek for SLS_45 and SLS_46                                                                   
                                    //END
                                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, sr.getErrors()[0].getMessage()));
                                }                                
                             }
                               else{
                                   check2=true;
                               } 
                        }
                        //populate the reamaining in future methods
                        if(pstobeprocessedinfuturemethod.size()>0)
                        callFutureMethods(pstobeprocessedinfuturemethod);
                        
                        if(check1 && !check2){
                            pc[0].TECH_SendEmail__c=false;  
                            buttonLabel=Label.CL00267;                                    
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.CL00007));    
                        }
                    }  
                }
                if(!ApexPages.hasMessages()){
                    // re-direct to the Individual Commercial Action Plan record detail page if there have been no errors
                    pageRef = new PageReference('/' + platformingCampaign.Id);
                    pageRef.setRedirect(true);
                }
            }else{
                pageRef = new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
                pageRef.setRedirect(true);
            }
            if(Test.isRunningTest())
                throw new TestException ();     // used during tests only to achieve coverage
        }catch (Exception e){
            ApexPages.addMessages(e);
        }
         database.update(pc);   
        return pageRef;
    }
    
    public void createPlatformScoringsFromClone(List<Id> existingAccountIds,SFE_IndivCAP__c caprecord){       
        try{            
            Map<Id, SFE_PlatformingScoring__c> mapPlatformRecords = new Map<Id, SFE_PlatformingScoring__c>();
            // retrieve all accounts that AssignedTo user owns
            for(Account a : [select id from Account where OwnerId = :caprecord.AssignedTo__c and (PrimaryRelationshipLeader__c=:Label.CLDEC14SLS02 OR PrimaryRelationshipLeader__c=:Label.CLDEC14SLS03) and ToBeDeleted__c=false and Inactive__c=false and id not in :existingAccountIds]){
                //    if(mapPlatformRecords.size() == NEW_RECORD_LIMIT)
                //      break;
                mapPlatformRecords.put(a.Id, new SFE_PlatformingScoring__c(IndivCAP__c = caprecord.Id, PlatformedAccount__c = a.Id));
            }
            // return all accounts were the AssignedTo user belongs to the account team
            Integer theLimit=Limits.getLimitQueryRows()-Limits.getQueryRows();
            for(AccountTeamMember atm : [select id, accountId from AccountTeamMember where UserId = :platformingCampaign.AssignedTo__c and Account.OwnerId != :caprecord.AssignedTo__c and Account.ToBeDeleted__c=false and Account.Inactive__c=false and (Account.PrimaryRelationshipLeader__c=:Label.CLDEC14SLS02 OR Account.PrimaryRelationshipLeader__c=:Label.CLDEC14SLS03) and Id not in :existingAccountIds limit :theLimit]){
                // if(mapPlatformRecords.size() == NEW_RECORD_LIMIT)
                //   break;
                mapPlatformRecords.put(atm.accountId, new SFE_PlatformingScoring__c(IndivCAP__c = caprecord.Id, PlatformedAccount__c = atm.accountId));
            }
            // create the Platform Scoring records
            if(!mapPlatformRecords.isEmpty()){
                List<SFE_PlatformingScoring__c> tobeprocessedhere=new List<SFE_PlatformingScoring__c>();
                List<Id> tobeprocessedinfuturemethod=new List<Id>();
                Integer counter=0;
                for(Id accountId :mapPlatformRecords.keySet()){
                    if(counter<=processhere){
                        tobeprocessedhere.add(mapPlatformRecords.get(accountId));
                        counter++;
                    }
                    else
                        tobeprocessedinfuturemethod.add(accountId);                            
                    }
                    Database.insert(tobeprocessedhere, false);
                    system.debug('test'+mapPlatformRecords.values());                        
                    //populate the reamaining in future methods
                    callFutureMethods(tobeprocessedinfuturemethod,caprecord.id);
                }  
            }    
            catch(Exception ex){}           
    }
    
    /**Modified by abhishek as per Req SLS_45 and SLS_46
    */
    public pagereference cancel()
    {    
        return new Pagereference('/'+ApexPages.currentPage().getparameters().get('id'));
    }
    public class TestException extends Exception {}
    
    public void callFutureMethods(List<Id> accountIdList,Id capId)
    {
        Integer batchcounter=0;  
        Set<Id> accountIds=new Set<Id>();  
        for(Id accountId: accountIdList){            
            accountIds.add(accountId);
            batchcounter++;
            if(batchcounter==BatchSize)
            {
                batchcounter=0;
                VFC04_SFETargetting.performInsertForPlaftormAccounts(accountIds,capId);
                accountIds.clear();                
            }
        }
        if(accountIds.size()>0){
            VFC04_SFETargetting.performInsertForPlaftormAccounts(accountIds,capId);
            accountIds.clear();     
        }           
    }   
    
    @future
    static void performInsertForPlaftormAccounts(Set<Id> accountIds,Id Capid){
        String errorLog='Errors in creating Platforming Accounts';
        Integer errorcount=0;        
        List<SFE_PlatformingScoring__c> tobeinserted=new  List<SFE_PlatformingScoring__c>();
        for(Id accountid : accountIds){
            tobeinserted.add(new SFE_PlatformingScoring__c(IndivCAP__c=Capid,PlatformedAccount__c =accountid));
        }
        List<Database.SaveResult> lsr=Database.insert(tobeinserted,false);
        for(Database.SaveResult sr:lsr)
        {            
           if(!sr.isSuccess())
           {
              if(sr.getErrors()[0].getStatusCode() != StatusCode.DUPLICATE_VALUE)
              {
                  errorLog+=sr.getId()+'-'+sr.getErrors()[0].getMessage();
                  errorcount++;
              }
           }   
        }
        if(errorcount>0)
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> mails=new List<String>();
            mails.add('ram.iyer@schneider-electric.com');
            mail.setToAddresses(mails);
            mail.setSubject('Error Log while creating Platforming accounts for a commercial action plan record '+capId);
            mail.setPlainTextBody(errorLog);
            
            if(!Test.isRunningTest())                          // ensure we never actually send an email during //a Test Run
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       }
        
    }

    public void callFutureMethods(List<SFE_PlatformingScoring__c> pslist)
    {
    Integer batchcounter=0;  
    List<SFE_PlatformingScoring__c> pslst=new List<SFE_PlatformingScoring__c>();
        for(SFE_PlatformingScoring__c ps: pslist)
        {            
            pslst.add(ps);
            batchcounter++;
            if(batchcounter==BatchSize)
            {
                batchcounter=0;
                 try {
                    Integer currentQueuedJobsInTranstn = Limits.getQueueableJobs();
                    Integer maxQueuedJobsInTranstn = Limits.getLimitQueueableJobs();
                    System.debug('getQueueableJobs ***' + Limits.getQueueableJobs() +'**getLimitQueueableJobs **'+Limits.getLimitQueueableJobs());
                    
                    if(currentQueuedJobsInTranstn < maxQueuedJobsInTranstn) {
                        ID jobID = System.enqueueJob(new AP_PerformInsertForPlaftormAccounts (pslst));
                        System.debug('* job ID***' + jobID);
                    } else {
                        System.debug('>>>>Maximum queueable limit reached');
                        
                    }
                } catch (LimitException ltEx) {
                   System.debug('*** Exception Enqueue-ing AP_PerformInsertForPlaftormAccounts  ***'+ltEx.getMessage()+' '+ltEx.getstacktracestring()); 
                }
                pslst.clear();                
            }
        }
        if(pslst.size()>0){
                 try {
                    Integer currentQueuedJobsInTranstn = Limits.getQueueableJobs();
                    Integer maxQueuedJobsInTranstn = Limits.getLimitQueueableJobs();
                    System.debug('getQueueableJobs ***' + Limits.getQueueableJobs() +'**getLimitQueueableJobs **'+Limits.getLimitQueueableJobs());
                    
                    if(currentQueuedJobsInTranstn < maxQueuedJobsInTranstn) {
                        ID jobID = System.enqueueJob (new AP_PerformInsertForPlaftormAccounts (pslst));
                        System.debug('* job ID***' + jobID);
                    } else {
                        System.debug('>>>>Maximum queueable limit reached');
                        
                    }
                } catch (LimitException ltEx) {
                   System.debug('*** Exception Enqueue-ing AP_PerformInsertForPlaftormAccounts  ***'+ltEx.getMessage()+' '+ltEx.getstacktracestring()); 
                }
            pslst.clear();     
        }           
    }   
}