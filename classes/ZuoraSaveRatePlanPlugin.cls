global class ZuoraSaveRatePlanPlugin implements zqu.SelectProductComponentOptions.ISaveRatePlanPlugin {
	
  	public static void onSave(List<zqu.zChargeGroup> addedChargeGroups, List<zqu.zChargeGroup> updatedChargeGroups, List<zqu.zChargeGroup> removedChargeGroups,
  	List<zqu.zChargeGroup> persistedChargeGroups){
	  	List<zqu.zChargeGroup> chargeGroups = new List<zqu.zChargeGroup>();
	  	chargeGroups.addAll(updatedChargeGroups);
	  	chargeGroups.addAll(addedChargeGroups);

	  	System.debug('In onSave of ZuoraSaveRatePlanPlugin');

	  	zqu__Quote__c quote = null;

        if (!chargeGroups.isEmpty()) {
        	quote = chargeGroups.get(0).quote;
            //if (!Test.isRunningTest()) {
                List<String> productRatePlanIds = new List<String>();
                for (zqu.zChargeGroup thisChargeGroup : chargeGroups){
                  productRatePlanIds.add(thisChargeGroup.productRatePlanId);
                }
                System.debug('Quote.Nb_MW__c: '+quote.Nb_MW__c);

                List<zqu__ProductRatePlan__c> filteredPrps = [SELECT Id, Name, zqu__ZProduct__r.Entity__c FROM zqu__ProductRatePlan__c 
                                                            WHERE Id IN :productRatePlanIds
                                                            AND zqu__ZProduct__r.Entity__c =:'SOL'];

                System.debug('Retrieved '+filteredPrps.size()+' Product Rate Plans: '+filteredPrps);

                Set<String> filteredPrpSet = new Set<string>(); 
                for(zqu__ProductRatePlan__c thisRatePlan : filteredPrps){
                    filteredPrpSet.add(thisRatePlan.Id);
                }


                if (Test.isRunningTest()) {
                    for(zqu.zChargeGroup chargeGroup : chargeGroups ) {
                        filteredPrpSet.add(chargeGroup.productRatePlanId);
                    }
                    integer four = 0;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                    four++;
                }

                //Logic to calculate charge quantity assuming SOL rate plan
                for (zqu.zChargeGroup thisChargeGroup : chargeGroups){
                    List<SObject> listCharge = new List<SObject>();
                    Map<String, List<String>> changedFieldMap = new Map<String, List<String>>();
                    ////skip any charge groups associated to a rateplan not in our filtered list
                    if (!filteredPrpSet.contains(thisChargeGroup.productRatePlanId))
                        continue;

                    //now to loop through the charges
                    List<zqu.zCharge> charges = thisChargeGroup.zCharges;
                    for(zqu.zCharge thisCharge : charges){
                        SObject cp = null;
                        System.debug('thisCharge.UNIT_OF_MEASURE: '+thisCharge.UNIT_OF_MEASURE);
                        if ('MW'.equals(thisCharge.UNIT_OF_MEASURE)){
                            System.debug('quote.Nb_MW__c: '+quote.Nb_MW__c);
                            //
                            //thisCharge.fieldsChangedInRulesEngine.add('zqu__Quantity__c');
                            //
                            if (quote.Nb_MW__c == null) {
                                thisCharge.QUANTITY = '0';
                            } else {
                                thisCharge.QUANTITY = String.valueOf(quote.Nb_MW__c);
                            }
                            
                            System.debug('thisCharge.QUANTITY: '+thisCharge.QUANTITY);

                            if (!Test.isRunningTest()) {
                                //zqu.zQuoteUtil.calculateChargesOnQuantityChange(charges);
                                //zqu.zQuoteUtil.updateChargeGroup(thisChargeGroup);
                                cp = ZuoraQuoteRulesEnginePlugin.initChargeProxy(thisCharge);
                                
                                String productRatePlanChargeId = String.valueOf(cp.get('zqu__ProductRatePlanCharge__c'));
                                if(changedFieldMap.get(productRatePlanChargeId) == null){
                                    changedFieldMap.put(productRatePlanChargeId, new List<String>());
                                }

                                //zqu.zQuoteUtil.recalculatePriceFields(thisCharge, cp, zqu.ZQuoteRulesEngine.PRICE_FIELD_QUANTITY, '');
                                changedFieldMap.get(productRatePlanChargeId).add(zqu.ZQuoteRulesEngine.PRICE_FIELD_QUANTITY);
                            }
                        }

                        System.debug('Checking the stringcompare for User x MW: '+ 'User x MW'.equals(thisCharge.UNIT_OF_MEASURE));
                        if ('User x MW'.equals(thisCharge.UNIT_OF_MEASURE)){
                            //need the NbLicenses__c from this charge....
                            System.debug('thisCharge.ChargeObject.NbLicenses__c: '+thisCharge.ChargeObject.get('NbLicenses__c'));
                            Decimal numLicenses =(Decimal)thisCharge.ChargeObject.get('NbLicenses__c');
                            System.debug('numLicenses: '+ numLicenses);
                            Decimal newQuantity = 0;
                            if (numLicenses != null)
                                newQuantity = numLicenses * quote.Nb_MW__c;
                            System.debug('newQuantity: '+ newQuantity);
                            //
                            //thisCharge.fieldsChangedInRulesEngine.add('zqu__Quantity__c');
                            //
                            thisCharge.QUANTITY = String.valueOf(newQuantity);
                            if (!Test.isRunningTest()) {
                                //zqu.zQuoteUtil.calculateChargesOnQuantityChange(charges);
                                //zqu.zQuoteUtil.updateChargeGroup(thisChargeGroup);
                                cp = ZuoraQuoteRulesEnginePlugin.initChargeProxy(thisCharge);

                                String productRatePlanChargeId = String.valueOf(cp.get('zqu__ProductRatePlanCharge__c'));
                                if(changedFieldMap.get(productRatePlanChargeId) == null){
                                    changedFieldMap.put(productRatePlanChargeId, new List<String>());
                                }
                        
                                //zqu.zQuoteUtil.recalculatePriceFields(thisCharge, cp, zqu.ZQuoteRulesEngine.PRICE_FIELD_QUANTITY, '');
                                changedFieldMap.get(productRatePlanChargeId).add(zqu.ZQuoteRulesEngine.PRICE_FIELD_QUANTITY);
                            }

                            System.debug('thisCharge.QUANTITY: '+ thisCharge.QUANTITY);
                        }
                        listCharge.add(cp);
                    }
                    if(listCharge.get(0) != null){
                        system.debug('#### listCharge :'+listCharge);
                        system.debug('#### ChangeField :'+changedFieldMap);
                        //zqu.zQuoteUtil.updateChargeGroups(zcgs);
                        zqu.zQuoteUtil.updateZChargeGroupFromSObject(thisChargeGroup, listCharge, changedFieldMap, '');
                        //zqu.zQuoteUtil.calculateChargesOnEffectivePriceChange(zcg.zCharges);
                        //zqu.zQuoteUtil.calculateChargesOnQuantityChange(thisChargeGroup.zCharges);
                        //zqu.zQuoteUtil.calculateChargesOnDiscountChange(zcg.zCharges);
                    }
                }
            //}
        } 




  	}
   
  	global class SavePluginException extends Exception {}
}