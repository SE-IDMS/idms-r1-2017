/********************************************************************
* Company: Fielo
* Developer: Pablo Cassinerio
* Created Date: 23/08/2016
* Description: 
********************************************************************/
@isTest
global class FieloPRM_Schedule_MemberFeatureTest{
    
    static testmethod void test1() {
        User us = new user(id = userinfo.getuserId());

        us.BypassVR__c = true;
        update us;
        FieloEE.MockUpFactory.setCustomProperties(false);

        CS_PRM_ApexJobSettings__c conf = new CS_PRM_ApexJobSettings__c(Name ='FieloPRM_Batch_MemberFeatProcess', F_PRM_TryCount__c = 5, F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10);
            insert conf;

        FieloEE__Member__c member = FieloEE.MockUpFactory.createMember('Test member', '56789', 'DNI');

        FieloPRM_Feature__c feature = new FieloPRM_Feature__c(
            Name = 'TestRamiro',
            F_PRM_FeatureAPIName__c = 'TestRamiroApiName',
            F_PRM_FeatureCode__c = 'Fecodetest',
            F_PRM_CustomLogicClass__c = 'FieloPRM_RewardsPSetImplements');
        insert feature;

        FieloPRM_MemberFeature__c mf = new FieloPRM_MemberFeature__c(
            F_PRM_Member__c = member.Id, 
            F_PRM_Feature__c = feature.Id,
            F_PRM_TryCount__c = 0);
        insert mf;

        List<FieloPRM_MemberFeature__c> mfs = new List<FieloPRM_MemberFeature__c>();
        mfs.add(mf);
        
        System.runAs(us){
            test.StartTest();
            try{
                FieloPRM_Schedule_MemberFeature.scheduleNow(true, mfs);    
            }catch(Exception e){
                
            }
            

            test.StopTest();
        }
     }
}