public Class Fielo_BreadCrumbController{
    
    public FieloEE__Menu__c homeMenu {get; set;}
    public FieloEE__Menu__c parentMenu {get; set;}
    public FieloEE__Menu__c actualMenu {get; set;}
    
    public Fielo_BreadCrumbController(){
        List<FieloEE__Menu__c> homeMenus = [SELECT Id, Name, FieloEE__CustomURL__c, (SELECT Id FROM FieloEE__Components__r), (SELECT Id, FieloEE__CustomURL__c FROM FieloEE__Menu__r ORDER BY FieloEE__Order__c) FROM FieloEE__Menu__c WHERE FieloEE__Home__c = true AND FieloEE__Program__c =: FieloEE.ProgramUtil.getProgramByDomain().Id limit 1];
        if(!homeMenus.isEmpty()){
            homeMenu = homeMenus[0];
        }
        
        String idMenu = ApexPages.currentPage().getParameters().get('idMenu');
        List<FieloEE__Menu__c> actualMenu = [SELECT Id, Name, FieloEE__Menu__c, FieloEE__CustomURL__c, (SELECT Id FROM FieloEE__Components__r), (SELECT Id, FieloEE__CustomURL__c FROM FieloEE__Menu__r ORDER BY FieloEE__Order__c) FROM FieloEE__Menu__c WHERE Id =: idMenu limit 1];
        if(!actualMenu.isEmpty()){
            this.actualMenu = actualMenu[0];
            if(actualMenu[0].FieloEE__Menu__c != null){
                parentMenu = [SELECT Id, Name, FieloEE__CustomURL__c, (SELECT Id FROM FieloEE__Components__r), (SELECT Id, FieloEE__CustomURL__c FROM FieloEE__Menu__r ORDER BY FieloEE__Order__c) FROM FieloEE__Menu__c WHERE Id =: actualMenu[0].FieloEE__Menu__c limit 1];
            }
        }
        
    }
    
}