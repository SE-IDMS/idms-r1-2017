public class VFC_ManageContactExternalFacet {
    public Boolean UpdateContactIsError {get;set;}
    public Boolean ResetPasswordIsError {get;set;}
    public Boolean UpdateContactIsSucess {get;set;}
    public Boolean ResetPasswordIsSucess {get;set;}
    public Boolean clickEdit {get;set;}
    public Contact Con {get;set;}
    public Contact OrgCon;
    public List<String> conFields;
    public List<SelectOption> IMSApplications{get; set;}
    public String IMSApplicationID {get;set;}
    private String[] contactFields = new String[] {
                'PRMFirstName__c','PRMLastName__c','PRMEmail__c','PRMJobTitle__c','PRMJobFunc__c','PRMWorkPhone__c','PRMMobilePhone__c',
                'PRMCountry__c','PRMCustomerClassificationLevel1__c','PRMCustomerClassificationLevel2__c','PRMOrigin__c',
                'PRMLastLoginDate__c','PRMTaxId__c','PRMContact__c','PRMPrimaryContact__c','PRMUser__c','IsMasterSalesContact__c',
                'PRMContactRegistrationStatus__c','PRMReasonForDecline__c','PRMRegistrationActivationDate__c',
                'PRMSecondary1ClassificationLevel1__c','PRMSecondary2ClassificationLevel1__c','PRMSecondary3ClassificationLevel1__c',
                'PRMSecondary1ClassificationLevel2__c','PRMSecondary2ClassificationLevel2__c','PRMSecondary3ClassificationLevel2__c',
                'PRMExcludeFromReports__c','PRMIsActiveContact__c','PRMUIMSID__c','AccountId','PRMRegistrationReviewedRejectedDate__c',
                'PRMApplicationRequested__c'
            }; 
    
    public VFC_ManageContactExternalFacet(ApexPages.StandardController stdController) {
        
        String resetPassword = ApexPages.currentPage().getParameters().get('resetpassword');
        String qStrng = ApexPages.currentPage().getParameters().get('isEdit');

        List<AP_PRMUtils.ApplicationType> applicationLst = new List<AP_PRMUtils.ApplicationType>();
        conFields = new List<String> (contactFields);
        if(!test.isRunningTest()){
            stdController.addFields(ConFields);
        }

        if(String.isNotBlank(qStrng) && qStrng != Null){
            clickEdit = True;
        }

        Con = (Contact)stdController.getRecord();
        ResetPasswordIsError = False;
        updateContactIsSucess = False;
        ResetPasswordIsSucess = False;
        IMSApplications = New List<SelectOption>();
        IMSApplications.add(new SelectOption('','--None--'));
        //IMSApplications.add(new SelectOption(System.label.CLAPR15PRM022,'PRM v2'));
        if(!con.PRMContact__c && !clickEdit){
            updateContactIsError = true;
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.CLAPR15PRM420));
        }
        if(resetPassword == 'True'){
            if(String.isNotBlank(con.PRMUIMSID__c)){
                applicationLst = AP_PRMUtils.getApplications_Admin(con.PRMUIMSID__c);
            }
            if(applicationLst != Null && applicationLst.size() > 0)
                for(AP_PRMUtils.ApplicationType applicationType : applicationLst)
                    IMSApplications.add(new SelectOption(applicationType.ApplicationId,applicationType.DisplayName));
        }   
    }
    public Void updateContactInfoInUIMS(){
        
        try {
            Account acc = [SELECT PRMUIMSID__c, Id FROM Account WHERE Id = :Con.AccountId];
            if(IsContactChanged()){
                if(Con.PRMContactRegistrationStatus__c == 'Declined' && String.isBlank(Con.PRMReasonForDecline__c))
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Reason for Declining Registration is Mandatory for this status'));
                if(ApexPages.getMessages().Size() == 0){
                    //Do the UIMS Update once the class is created.
                    AP_UserRegModel updateUIMSContact = new AP_UserRegModel();
                    updateUIMSContact.contactID = Con.Id;
                    updateUIMSContact.uimsCompanyId = acc.PRMUIMSID__c;
                    updateUIMSContact.federationId = Con.PRMUIMSID__c;
                    updateUIMSContact.firstName = Con.PRMFirstName__c;
                    updateUIMSContact.lastName = Con.PRMLastName__c;
                    updateUIMSContact.email = Con.PRMEmail__c;
                    updateUIMSContact.jobTitle = Con.PRMJobTitle__c;
                    updateUIMSContact.jobFunction = Con.PRMJobFunc__c;
                    if(String.isNotBlank(Con.PRMWorkPhone__c) && Con.PRMWorkPhone__c != null) {
                        updateUIMSContact.phoneNumber = Con.PRMWorkPhone__c;
                        updateUIMSContact.phoneType = 'Work';
                    }
                    else if(String.isNotBlank(Con.PRMMobilePhone__c) && Con.PRMMobilePhone__c != null) {
                        updateUIMSContact.phoneNumber = Con.PRMMobilePhone__c;
                        updateUIMSContact.phoneType = 'Mobile';
                    }
                    updateUIMSContact.companyCountry = Con.PRMCountry__c;
                    updateUIMSContact.businessType = Con.PRMCustomerClassificationLevel1__c;
                    updateUIMSContact.areaOfFocus = Con.PRMCustomerClassificationLevel2__c;
                    updateUIMSContact.prmOrigin = Con.PRMOrigin__c;
                    updateUIMSContact.taxId = Con.PRMTaxId__c;
                    updateUIMSContact.isPrimaryContact = Con.PRMPrimaryContact__c;
                    Boolean contactUpdated = AP_PRMUtils.UpdateContactInUIMS_Admin(updateUIMSContact);
                    // if(contactUpdated){
                        update Con;
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, System.Label.CLJUL15PRM005)); //Your changes were saved successfully
                        updateContactIsSucess = True;
                        clickEdit = false;
                    // }
                    // else
                    // ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.CLJUL15PRM006));  //Unable to save changes, error received while updating information in UIMS
                }
            }
            else
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, System.Label.CLJUL15PRM007)); //No changes were detected, save once the changes were made to the data
        }
        catch(Exception ex){
            System.debug(' ****There is an exception on updating the Account info in UIMS****' +ex.getMessage() + ex.getStackTraceString());
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, ex.getMessage()));
        }
    }
    
    public pageReference editPage() {
        clickEdit = true;
        return null;
    }
    public pageReference detailPage() {
        clickEdit = false;
        return null;
    }
    
    
    private Boolean IsContactChanged() {
        Boolean haveValuesChanged = false;
        String conId = Con.Id;
        String qCols = 'Select Id,'+String.join(contactFields, ',');
        if(qCols.endsWithIgnoreCase(',')) qCols = qCols.removeEndIgnoreCase(',');
        qCols = qCols+' FROM Contact WHERE ID = :conId';
        System.debug('The query Value is' +qCols);
        OrgCon = Database.Query(qCols);

        for(String s:conFields){
            System.debug('**What are the values we are getting'+Con.get(s)+'OldValue'+OrgCon.get(s));
            
            if(!haveValuesChanged && Con.get(s) != OrgCon.get(s)){
                System.debug('**What are the values we are getting'+Con.get(s)+'OldValue'+OrgCon.get(s));
                haveValuesChanged = True;
            }
        }
        return haveValuesChanged;
    }
    
    public Void ResetPassword() {
        try {
            string federatedId = '';
            List<User> lUsr = new List<User>([SELECT Id, federationIdentifier, email, Contact.PRMContact__c FROM USER WHERE IsActive = true AND federationIdentifier = :Con.PRMUIMSID__c]);
            if (!lUsr.isEmpty()) {
                User usr = lUsr[0];
                federatedId = (usr != null ? usr.FederationIdentifier : '');
            }
            if(String.isBlank(federatedId)){
                uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort uimsPort = new uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
                uimsPort.timeout_x = 120000;
                uimsPort.clientCertName_x = System.Label.CLAPR15PRM018;
                uimsPort.endpoint_x = System.Label.CLAPR15PRM017;
                uimsv2ServiceImsSchneiderComAUM.userFederatedIdAndType srchUsrResult = uimsPort.searchUser(System.Label.CLAPR15PRM016, con.PRMEmail__c);
                System.debug('Search Result: ' + srchUsrResult);
                if (srchUsrResult != null ) federatedId = (string.isNotBlank(srchUsrResult.federatedId) ? srchUsrResult.federatedId : '');
            }
            if(string.isNotBlank(federatedId)){
                uimsv2ServiceImsSchneiderComAUM.accessElement application = new uimsv2ServiceImsSchneiderComAUM.accessElement();
                application.type_x = System.label.CLAPR15PRM020;//'APPLICATION';
                application.id = IMSApplicationID;    //System.label.CLAPR15PRM022;'prm';
                uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort abc = new uimsv2ImplServiceImsSchneiderComAUM.AuthenticatedUserManager_UIMSV2_ImplPort();
                abc.endpoint_x = System.label.CLAPR15PRM017; //'https://ims-sqe.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/2WAuthenticated-UserManager' (https://ims-sqe.btsec.dev.schneider-electric.com/IMS-UserManager/UIMSV2/2WAuthenticated-UserManager%27);
                abc.timeout_x = Integer.valueOf(System.label.CLAPR15PRM021);//120000; 
                abc.clientCertName_x = System.label.CLAPR15PRM018;//'Certificate_IMS';
                String resetStatus = abc.resetPassword(System.label.CLAPR15PRM019, federatedId, application);
                System.debug('*** Password reset status:' + resetStatus);
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, System.Label.CLAPR15PRM419));
                ResetPasswordIsSucess = True;
            }
            else ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.CLJUL15PRM019)); //'There is no Partner user associated for this contact OR user is not active.' 
            //}
            //else ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.CLJUL15PRM018)); //'No email exists with this emailaddress in SE network.'
        } 
        catch (Exception e) {
            System.debug('Error:' + e.getMessage() + e.getStackTraceString());
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            ResetPasswordIsError = True;
        }
    }
    
    public pagereference goToManConExtFacetPage(){
        ResetPasswordIsError = false;
        ResetPasswordIsSucess = false;
        clickEdit = false;
        return new pagereference('/apex/VFP_ManageContactExternalFacet?id='+Con.Id);
    }
    
    public pagereference MoveToDetailPage(){
        return new pagereference('/'+Con.Id);
    }
    
    public pagereference goToResetPasswordPage(){
        
        return new pagereference('/apex/VFP_PRMContactResetPasswordOld?id='+Con.Id);
    }
}