/********************************************************************
* Company: Fielo
* Created Date: 26/04/2015
* Description: Test class for class FieloPRM_UTILS_MenuMethods
********************************************************************/
@isTest
public without sharing class FieloPRM_UTILS_MenuMethodsTest {
    
     @testSetup static void testSetupMethodPRM() {

        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            Country__c testCountry = new Country__c(
                Name   = 'Global',
                CountryCode__c = 'WW'
            );
            insert testCountry;
           
            PRMCountry__c testPRmcountry = new PRMCountry__c(
                Name   = 'Global',
                Country__c = testCountry.id,
                PLDatapool__c = '123123test',
                CountryPortalEnabled__c = true       
            );
            insert testPRmcountry;
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.PRMCountry__c = testCountry.Id;
            insert acc;
            
            Contact con = new Contact(
                AccountId = acc.Id,
                FirstName = 'test contact Fn 2',
                LastName  = 'test contact Ln',
                PRMCompanyInfoSkippedAtReg__c = false,
                PRMCountry__c = testCountry.Id,
                PRMFirstName__c = 'PRM test contact Fn',
                PRMLastName__c  = 'PRM test contact Fn',
                PRMLanguage__c = 'en'
            );
    
            insert con;
    
            List<Profile> profiles = [select id from profile where name='SE - Channel Partner (Community)'];
            if(profiles.size()>0){
                UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
                
                User userTest = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
                userTest.ContactId = con.Id;
                userTest.PRMRegistrationCompleted__c = false;
                userTest.BypassWF__c = false;
                userTest.BypassVR__c = true;
                userTest.FirstName = 'Test User First';
    
                insert userTest;
            }
        }

    }
    
    public static testMethod void testUnit(){

        User us = new user(id = userinfo.getuserId());
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Program__c prog = [SELECT Id FROM FieloEE__Program__c LIMIT 1];
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c= 'Polo',
                FieloEE__FirstName__c = 'Marco',
                FieloEE__Street__c = 'test',
                FieloEE__Program__c = prog.Id
            );
            insert member;
            
            Contact cont = new Contact(
                LastName = 'TEST',
                FieloEE__Member__c = member.Id
            );
            insert cont;
                       
            Country__c testCountry = [SELECT Id FROM Country__c LIMIT 1];

            ClassificationLevelCatalog__c classLevel1 = new ClassificationLevelCatalog__c(
                Name  = 'EU',
                ClassificationLevelName__c = 'End User or Consumer'
            );
            insert classLevel1;

            ClassificationLevelCatalog__c classLevel2 = new ClassificationLevelCatalog__c(
                Name = 'EUB',
                ClassificationLevelName__c = 'Large Corporation',
                ParentClassificationLevel__c = classLevel1.Id
            );
            insert classLevel2;

            CountryChannels__c cntryChannels = new CountryChannels__c(
                Channel__c = classLevel1.Id,
                SubChannel__c = classLevel2.Id,
                Active__c = True,
                Country__c = testCountry.Id,
                AllowPrimaryToManage__c = True
            );
            insert cntryChannels;

            FieloEE__Menu__c thisMenu = new FieloEE__Menu__c(
                Name = 'Test',
                FieloEE__Title__c = 'Test',
                FieloEE__ExternalName__c = 'P_US_TestMenu2',
                FieloEE__Visibility__c = 'Both',
                FieloEE__Program__c = prog.Id,
                F_PRM_Status__c = 'Active',
                FieloEE__Placement__c = 'Main'
            );
            insert thisMenu;

            FieloEE__Menu__c thisMenuChild = new FieloEE__Menu__c(
                Name = 'Test',
                FieloEE__Title__c = 'Test',
                FieloEE__ExternalName__c = 'P_US_TestMenu3',
                FieloEE__Visibility__c = 'Both',
                FieloEE__Program__c = prog.Id,
                F_PRM_Status__c = 'Active',
                FieloEE__Placement__c = 'Main',
                FieloEE__Menu__c = thisMenu.Id
            );
            insert thisMenuChild;
            
            FieloEE__Section__c section = new FieloEE__Section__c(
                FieloEE__Menu__c = thisMenu.Id
            );
            insert section;
            
            test.startTest();
            
            FieloPRM_UTILS_MenuMethods.MenusContainer container1 = FieloPRM_UTILS_MenuMethods.getMenus('test', 'test'); 
            FieloPRM_UTILS_MenuMethods.MenusContainer container2 = FieloPRM_UTILS_MenuMethods.getMenus('Main', cont.Id);  
            FieloPRM_UTILS_MenuMethods.MenusContainer container3 = FieloPRM_UTILS_MenuMethods.getMenus('TOP', cont.Id);          
            FieloPRM_UTILS_MenuMethods.validateAndReplaceDuplicateExternalNames(new List<String>{'P-US-TestMenu','P-US-TestMenu','P_US_TestMenu2','P-US-TestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongName','P-US-TestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongNameTestMenuLongName'});           
            FieloPRM_UTILS_MenuMethods.createEntitySubscriptions(new Set<String>{thisMenu.Id});          
            FieloPRM_UTILS_MenuMethods.getDateTimeString();
            FieloPRM_UTILS_MenuMethods.resetRedirectCookie();
            FieloPRM_UTILS_MenuMethods.setACookie('test','test');
            FieloPRM_UTILS_MenuMethods.getRegistrationStringMenu();
            FieloPRM_UTILS_MenuMethods.setMyProgramsMenuCookie(member);
            FieloPRM_UTILS_MenuMethods.setMyPartnershipMenuCookie(cntryChannels);
            FieloPRM_UTILS_MenuMethods.setHomeMenuCookie(member,cntryChannels);
            FieloPRM_UTILS_MenuMethods.setCountryMenuByParentCookie(member,thisMenu.FieloEE__ExternalName__c,'test');
            test.stopTest();
        }
    }
    
}