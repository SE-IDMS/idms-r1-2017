@isTest
private class VFC_POMPLogin_Test {
    static testMethod void testPOMP() {
        PageReference pageRef = new PageReference('/apex/VFP_POMPLogin');
        system.Test.setCurrentPage(pageRef);
        
        Country__c testCountry = new Country__c();
         testCountry.Name   = 'USA';
         testCountry.CountryCode__c = 'US';
         testCountry.InternationalPhoneCode__c = '91';
         testCountry.Region__c = 'APAC';
         INSERT testCountry;
            
         Account acc = Utils_TestMethods.createAccount();
         acc.RecordTypeId = System.label.CLAPR15PRM025;
         INSERT acc;
         
         Contact cont = Utils_TestMethods.createContact(acc.Id,'TestContact');
         cont.PRMContact__c = TRUE;
         cont.PRMCustomerClassificationLevel1__c= 'LC';
         cont.PRMCustomerClassificationLevel2__c= 'Lc1';
         cont.PRMCountry__c = testCountry.Id;
         cont.NewPOMPCommunity__c = true;
         INSERT cont;
         
         User usr = Utils_TestMethods.createStandardUser('TestUser');
         usr.contactId = cont.Id;
         usr.ProfileId = System.label.CLAPR15PRM026;
         INSERT usr;    
       System.runAs(usr) { 
       
        VFC_POMPLogin POMPLogin = new VFC_POMPLogin();
        POMPLogin.redirectpage();
        POMPLogin.redirectNewPompPage();
      }
    }
    static testMethod void testPOMP1() {
        PageReference pageRef = new PageReference('/apex/VFP_POMPLogin');
        system.Test.setCurrentPage(pageRef);
        
        Country__c testCountry = new Country__c();
         testCountry.Name   = 'USA';
         testCountry.CountryCode__c = 'US';
         testCountry.InternationalPhoneCode__c = '91';
         testCountry.Region__c = 'APAC';
         INSERT testCountry;
            
         Account acc = Utils_TestMethods.createAccount();
         acc.RecordTypeId = System.label.CLAPR15PRM025;
         INSERT acc;
         
         Contact cont = Utils_TestMethods.createContact(acc.Id,'TestContact');
         cont.PRMContact__c = TRUE;
         cont.PRMCustomerClassificationLevel1__c= 'LC';
         cont.PRMCustomerClassificationLevel2__c= 'Lc1';
         cont.PRMCountry__c = testCountry.Id;
         //cont.NewPOMPCommunity__c = true;
         INSERT cont;
         
         User usr = Utils_TestMethods.createStandardUser('TestUser');
         usr.contactId = cont.Id;
         usr.ProfileId = System.label.CLAPR15PRM026;
         INSERT usr;    
       System.runAs(usr) { 
       
        VFC_POMPLogin POMPLogin = new VFC_POMPLogin();
        POMPLogin.redirectpage();
        POMPLogin.redirectNewPompPage();
      }
    }
}