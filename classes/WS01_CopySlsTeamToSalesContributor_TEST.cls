/* 
    Global Class Name: WS01_CopySalesTeamToSalesContributor_Test
    Author: Amitava Dutta
    Date created:25/01/2011.
    Purpose: Basically a global class which is used to copy the Sales Team associated 
    with Opportunity as Sales Contributor List
    REF: SRO+ Release

*/

@isTest
private class WS01_CopySlsTeamToSalesContributor_TEST
{   
  //Region Defining the Class Variables
    
    public static ApexPages.StandardController objController;
    static integer mintTestResult;
    static string mstrTestOpportunityID;
    static List<OPP_SalesContributor__c> lstTestContributor;
  //End of Defining the Class Variables
    
  //Region Test Methods
       //-------Check the Copy Sales Team to Sales contributor ---
        static TestMethod void TestCopySalesTeamToSalesContributor()
        {   

            Test.startTest();   
            
                Account accountNew=Utils_TestMethods.createAccount();
                insert accountNew;
                
                Opportunity newOpportunity = Utils_TestMethods.createOpportunity(accountNew.id);
                insert newOpportunity;
                User objUser =  Utils_TestMethods.createStandardUser('amitava');
                insert objUser;
                OpportunityTeamMember otm = Utils_TestMethods.createOppTeamMember(newOpportunity.Id,objUser.Id);
                insert otm;
                
                /*List<OpportunityTeamMember> lstOpps = [SELECT OpportunityID,UserID FROM OpportunityTeamMember 
                WHERE OpportunityID IN (Select Id from Opportunity)ORDER BY CREATEDDATE DESC LIMIT 1];*/
        
                WS01_CopySalesTeamToSalesContributor.CopySalesTeamToSalesContributor(newOpportunity.Id);
                
                /*
                if(lstOpps.size() !=0)
                {
                    lstTestContributor = [SELECT User__C,Opportunity__c FROM 
                    OPP_SalesContributor__c WHERE User__c !=: lstOpps[0].UserID 
                    AND Opportunity__c=:lstOpps[0].OpportunityID];
                    
                    if(lstTestContributor.size() !=0)
                    {
                        mstrTestOpportunityID = lstTestContributor[0].Opportunity__c;
                       WS01_CopySalesTeamToSalesContributor.CopySalesTeamToSalesContributor(mstrTestOpportunityID);
                    }    
                }*/
           Test.StopTest();
        }
  //Region End of Test Methods
}