public with sharing class VFC74_PlatformingVisitsExecutionKPI {
    
    public Double counttargetQ1{get;set;}{counttargetQ1=0;}
    public Double counttargetQ2{get;set;}{counttargetQ2=0;}
    public Double counttargetQ3{get;set;}{counttargetQ3=0;}
    public Double counttargetS1{get;set;}{counttargetS1=0;}
    public Double counttargetS2{get;set;}{counttargetS2=0;}
    public Double counttargetS3{get;set;}{counttargetS3=0;}
    public Double counttargetG1{get;set;}{counttargetG1=0;}
    public Double counttargetG2{get;set;}{counttargetG2=0;}
    public Double counttargetG3{get;set;}{counttargetG3=0;}
    public Double counttargetother{get;set;}{counttargetother=0;}   
    public Double totaltarget{get;set;}{totaltarget=0;}
    
    public Double countresultQ1{get;set;}{countresultQ1=0;}
    public Double countresultQ2{get;set;}{countresultQ2=0;}
    public Double countresultQ3{get;set;}{countresultQ3=0;}
    public Double countresultS1{get;set;}{countresultS1=0;}
    public Double countresultS2{get;set;}{countresultS2=0;}
    public Double countresultS3{get;set;}{countresultS3=0;}
    public Double countresultG1{get;set;}{countresultG1=0;}
    public Double countresultG2{get;set;}{countresultG2=0;}
    public Double countresultG3{get;set;}{countresultG3=0;}
    public Double countresultother{get;set;}{countresultother=0;}
    public Double totalresult{get;set;}{totalresult=0;}
    
    public decimal Q1targetpercent{get;set;}
    public decimal Q2targetpercent{get;set;}
    public decimal Q3targetpercent{get;set;}
    public decimal S1targetpercent{get;set;}
    public decimal S2targetpercent{get;set;}
    public decimal S3targetpercent{get;set;}
    public decimal G1targetpercent{get;set;}
    public decimal G2targetpercent{get;set;}
    public decimal G3targetpercent{get;set;}
    public decimal othertargetpercent{get;set;}
    
    public decimal Q1resultpercent{get;set;}
    public decimal Q2resultpercent{get;set;}
    public decimal Q3resultpercent{get;set;}
    public decimal S1resultpercent{get;set;}
    public decimal S2resultpercent{get;set;}
    public decimal S3resultpercent{get;set;}
    public decimal G1resultpercent{get;set;}
    public decimal G2resultpercent{get;set;}
    public decimal G3resultpercent{get;set;}
    public decimal otherresultpercent{get;set;}
    public decimal totalresulpercent{get;set;}
    
    public decimal Q1deviation {get;set;}
    public decimal Q2deviation {get;set;}
    public decimal Q3deviation {get;set;}
    public decimal S1deviation {get;set;}
    public decimal S2deviation {get;set;}
    public decimal S3deviation {get;set;}
    public decimal G1deviation {get;set;}
    public decimal G2deviation {get;set;}
    public decimal G3deviation {get;set;}
    public decimal otherdeviation {get;set;}
    public decimal totaldeviation {get;set;}

    
    public VFC74_PlatformingVisitsExecutionKPI(){
        Datetime dateTimetemp = System.now();
        Date dateInf = Date.newInstance(dateTimetemp.year(),1,1);       
        
        list<Event> myEventtargeted1  = [SELECT id,Platforming_Customer_Profile__c, Status__c FROM Event WHERE OwnerId in (select Id from User where user.manager.id=:UserInfo.getuserid()) AND Platforming_Customer_Profile__c!=NULL AND  StartDateTime >=:dateInf AND StartDateTime <= today];
        list<Event> myEventtargeted2  = [SELECT id,Platforming_Customer_Profile__c, Status__c FROM Event WHERE OwnerId=:UserInfo.getuserid() AND Platforming_Customer_Profile__c!=NULL AND  StartDateTime >=:dateInf AND StartDateTime <= today];
        list<Event> myEventnottargeted1  = [SELECT id,Platforming_Customer_Profile__c, Status__c FROM Event WHERE OwnerId in (select Id from User where user.manager.id=:UserInfo.getuserid()) AND Platforming_Customer_Profile__c=NULL AND  StartDateTime >=:dateInf AND StartDateTime <= today];
        list<Event> myEventnottargeted2  = [SELECT id,Platforming_Customer_Profile__c, Status__c FROM Event WHERE OwnerId=:UserInfo.getuserid() AND Platforming_Customer_Profile__c=NULL AND  StartDateTime >=:dateInf AND StartDateTime <= today];
        
        List<Event> myEventtargeted = new List<Event>();
        myEventtargeted.addall(myEventtargeted1);
        myEventtargeted.addall(myEventtargeted2);
        
        List<Event> myEventnottargeted = new List<Event>();
        myEventnottargeted.addall(myEventnottargeted1);
        myEventnottargeted.addall(myEventnottargeted2);
        
        counttargetother=myEventnottargeted.size();
        for (Event evtnt:myEventnottargeted)
        {
            if(evtnt.Status__c =='Closed'){countresultother=countresultother+1;}    
        }
        
        for (Event evt:myEventtargeted)
        {
            if(evt.Platforming_Customer_Profile__c=='Q1'){
                counttargetQ1=counttargetQ1+1;
                if(evt.Status__c =='Closed'){countresultQ1=countresultQ1+1;}
            }
            if(evt.Platforming_Customer_Profile__c=='Q2'){
                counttargetQ2=counttargetQ2+1;
                totaltarget=totaltarget+counttargetQ2;
                if(evt.Status__c =='Closed'){countresultQ2=countresultQ2+1;}
            }
            if(evt.Platforming_Customer_Profile__c=='Q3'){
                counttargetQ3=counttargetQ3+1;
                if(evt.Status__c =='Closed'){countresultQ3=countresultQ3+1;}
            }
            if(evt.Platforming_Customer_Profile__c=='S1'){
                counttargetS1=counttargetS1+1;
                if(evt.Status__c =='Closed'){countresultS1=countresultS1+1;}
            }
            if(evt.Platforming_Customer_Profile__c=='S2'){
                counttargetS2=counttargetS2+1;
                totaltarget=totaltarget+counttargetS2;
                if(evt.Status__c =='Closed'){countresultS2=countresultS2+1;}
            }
            if(evt.Platforming_Customer_Profile__c=='S3'){
                counttargetS3=counttargetS3+1;
                if(evt.Status__c =='Closed'){countresultS3=countresultS3+1;}
            }
            if(evt.Platforming_Customer_Profile__c=='G1'){
                counttargetG1=counttargetG1+1;
                if(evt.Status__c =='Closed'){countresultG1=countresultG1+1;}
            }
            if(evt.Platforming_Customer_Profile__c=='G2'){
                counttargetG2=counttargetG2+1;
                if(evt.Status__c =='Closed'){countresultG2=countresultG2+1;}
            }
            if(evt.Platforming_Customer_Profile__c=='G3'){
                counttargetG3=counttargetG3+1;
                if(evt.Status__c =='Closed'){countresultG3=countresultG3+1;}
            }
            
        }
        totaltarget=counttargetQ1+counttargetQ2+counttargetQ3+counttargetS1+counttargetS2+counttargetS3+counttargetG1+counttargetG2+counttargetG3+counttargetother;
        totalresult=countresultQ1+countresultQ2+countresultQ3+countresultS1+countresultS2+countresultS3+countresultG1+countresultG2+countresultG3+countresultother;
        
        if (totaltarget<>0) Q1targetpercent=(100*counttargetQ1/totaltarget) ; else Q1targetpercent=0;
        if (totaltarget<>0) Q2targetpercent=(100*counttargetQ2/totaltarget) ; else Q2targetpercent=0;
        if (totaltarget<>0) Q3targetpercent=(100*counttargetQ3/totaltarget) ; else Q3targetpercent=0;
        if (totaltarget<>0) S1targetpercent=(100*counttargetS1/totaltarget) ; else S1targetpercent=0;
        if (totaltarget<>0) S2targetpercent=(100*counttargetS2/totaltarget) ; else S2targetpercent=0;
        if (totaltarget<>0) S3targetpercent=(100*counttargetS3/totaltarget) ; else S3targetpercent=0;
        if (totaltarget<>0) G1targetpercent=(100*counttargetG1/totaltarget) ; else G1targetpercent=0;
        if (totaltarget<>0) G2targetpercent=(100*counttargetG2/totaltarget) ; else G2targetpercent=0;
        if (totaltarget<>0) G3targetpercent=(100*counttargetG3/totaltarget) ; else G3targetpercent=0;
        if (totaltarget<>0) othertargetpercent=(100*counttargetother/totaltarget) ; else othertargetpercent=0;
        
        system.debug('################################## Q2targetpercent : '+Q2targetpercent);
        
        if (totalresult<>0) Q1resultpercent=(100*countresultQ1/totalresult) ; else Q1resultpercent=0;
        if (totalresult<>0) Q2resultpercent=(100*countresultQ2/totalresult) ; else Q2resultpercent=0;
        if (totalresult<>0) Q3resultpercent=(100*countresultQ3/totalresult) ; else Q3resultpercent=0;
        if (totalresult<>0) S1resultpercent=(100*countresultS1/totalresult) ; else S1resultpercent=0;
        if (totalresult<>0) S2resultpercent=(100*countresultS2/totalresult) ; else S2resultpercent=0;
        if (totalresult<>0) S3resultpercent=(100*countresultS3/totalresult) ; else S3resultpercent=0;
        if (totalresult<>0) G1resultpercent=(100*countresultG1/totalresult) ; else G1resultpercent=0;
        if (totalresult<>0) G2resultpercent=(100*countresultG2/totalresult) ; else G2resultpercent=0;
        if (totalresult<>0) G3resultpercent=(100*countresultG3/totalresult) ; else G3resultpercent=0;
        if (totalresult<>0) otherresultpercent=(100*countresultother/totalresult) ; else otherresultpercent=0;
        
        if (totaltarget<>0) totalresulpercent=(100*totalresult/totaltarget) ; else totalresulpercent=0;
        
        Q1deviation = math.abs(Q1targetpercent-Q1resultpercent);
        Q2deviation = math.abs(Q2targetpercent-Q2resultpercent);
        Q3deviation = math.abs(Q3targetpercent-Q3resultpercent);
        S1deviation = math.abs(S1targetpercent-S1resultpercent);
        S2deviation = math.abs(S2targetpercent-S2resultpercent);
        S3deviation = math.abs(S3targetpercent-S3resultpercent);
        G1deviation = math.abs(G1targetpercent-G1resultpercent);
        G2deviation = math.abs(G2targetpercent-G2resultpercent);
        G3deviation = math.abs(G3targetpercent-G3resultpercent);
        otherdeviation = math.abs(othertargetpercent-otherresultpercent);
        totaldeviation = Q1deviation + Q2deviation +Q3deviation +S1deviation + S2deviation +S3deviation +G1deviation + G2deviation +G3deviation + otherdeviation ;
        }
        
}