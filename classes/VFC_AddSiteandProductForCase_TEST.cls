@isTest
    Public Class VFC_AddSiteandProductForCase_TEST{
     Static TestMethod Void UnitTest1(){
        country__c samplecountry=null;
        List<Country__c> countries =[select id,Name,CountryCode__c from country__c limit 1];
        if(countries.size()==1)
            samplecountry=countries[0];
        else{               
            samplecountry=new country__c(Name='TestCountry',CountryCode__c='TCC');
            insert samplecountry;    
        }
               
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.Country__c = samplecountry.Id;
        Database.insert(objAccount);
        
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact1');
        objContact.Country__c = samplecountry.Id;
        Database.insert(objContact);
               
        Case objOpenCase = Utils_TestMethods.createCase(objAccount.Id, objContact.Id, 'In Progress');
        Database.insert(objOpenCase);
        
        // Create several Products
        List<Product2> productToInsert = new List<Product2>();
        Integer usecase = 0;
        for(Integer i=0 ; i <= 15 ; i++){ //(String commercialReference,String strMaterialDescription, String businessUnit, String productLine, String productFamily, String family,
            if(usecase == 1){
                Product2 objCommercialReference = Utils_TestMethods.createCommercialReference('Test Commercial Reference','MaterialDescription For VFCPROD'+i,'BU-VFCPROD'+i, 'PL-VFCPROD'+i, 'PF-VFCPROD'+i, 'F-VFCPROD1',false, false, false);
                   
                productToInsert.add(objCommercialReference);
                usecase = 0;
            }else{
                Product2 objCommercialReference = Utils_TestMethods.createCommercialReference('Test Commercial Reference','MaterialDescription For VFCPROD'+i,'BU-VFCPROD'+i, 'PL-VFCPROD'+i, 'PF-VFCPROD'+i, 'F-VFCPROD0',false, false, false);
                productToInsert.add(objCommercialReference);
                usecase = 1;
            }
        }
        Product2 objCommercialReference1 = Utils_TestMethods.createCommercialReference('VFCPROD22','MaterialDescription For VFCPROD','BU-VFCPROD', 'PL-VFCPROD', 'PF-VFCPROD', 'F-VFCPROD3',false, false, false);
        productToInsert.add(objCommercialReference1);
        Product2 objCommercialReference2 = Utils_TestMethods.createCommercialReference('VFCPROD33','MaterialDescription For VFCPROD','BU-VFCPROD', 'PL-VFCPROD', 'PF-VFCPROD', 'F-VFCPROD4',false, false, false);
        productToInsert.add(objCommercialReference2);
        Database.insert(productToInsert);
        
        List<Product2> searchresults = [Select id,Name,ProductFamilyId__c,ProductGDP__r.HierarchyType__c,ProductGDP__r.HierarchyTypeoftheParent__c from Product2];
        String debug = '';
        for(Product2 prod : searchResults){
            debug += prod.ProductFamilyId__c +','+prod.ProductGDP__r.HierarchyType__c+','+prod.ProductGDP__r.HierarchyTypeoftheParent__c+' || ';
        }
        system.debug('######## searchresults debug '+debug);
        // Create 2 Case Related Product
       
     ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objOpenCase);   
     VFC_AddSiteandProductForCase AddSites = new VFC_AddSiteandProductForCase(CaseStandardController );
     PageReference pageRef = Page.VFP_AddSiteandProductForCase ;
     system.currentpagereference().getParameters().put('commercialReference','tEST');
        
        //GMRSearchPage.SiteRadioWrapper();
        Account accounts = Utils_TestMethods.createAccount();
      //accounts.OwnerId = Usr.Id;
      accounts.SEAccountID__c ='12345689';
      Database.insert(accounts);
      
      Country__c Contry = New Country__c ();
      Contry.Name='France';Contry .CountryCode__c='Fr';
      insert Contry;
      
      contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
      
      Database.insert(con); 
      Con.Country__c = Contry.Id;
      update Con; 
      
      User userObj=Utils_TestMethods.createStandardUser('Test');
      insert userObj;
       
       list<GCSSite__c> lstgcs= new list<GCSSite__c>(); 
      for(integer i=0;i<13;i++){
        GCSSite__c  siteObj = new GCSSite__c();
       siteObj.Account__c=accounts.id;
       siteObj.LegacyIBSiteID__c='TestIBSiteID1'+i;
       siteObj.Name='Testsitess';
       siteObj.Active__c=true;
       siteObj.LegacySiteId__c='TestLegacySiteID'+i;
       lstgcs.add(siteObj); 
       }
       insert lstgcs;
       
       AddSites.idselected =lstgcs[0].Id; 
       AddSites.SiteSearch = 'TEST';
       ApexPages.currentPage().getParameters().put('commercialReference','Test Commercial Reference');
       String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString();  
        String jsonSelectFamily = Utils_WSDummyTestData.createDummyJSONString();
        ApexPages.currentPage().getParameters().put('jsonSelectString','['+jsonSelectString+']');
        ApexPages.currentPage().getParameters().put('jsonSelectFamily','['+jsonSelectFamily+']');
        
        AddSites.jsonSelectString='['+jsonSelectString+']';
        //AddSites.jsonSelectFamily='['+jsonSelectFamily+']';
        AddSites.performCheckbox();
        AddSites.pageCancelFunction();
        
        AddSites.SearchSites();
        AddSites.ActionFucntion1();
        AddSites.performCheckbox1();
        
        VFC_AddSiteandProductForCase.getOthers('ANYBUSINESS');
       
     }
     static testMethod void testGMRSearch2()
    {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new WS_MOCK_GMRSearch_TEST());
        VFC_AddSiteandProductForCase.remoteSearch('12345','abcd1234',True,True,True,True,True);
        Test.stopTest();
    }
    }