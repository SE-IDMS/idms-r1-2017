@IsTest
public class VFC_Fsearch_TEST {

 @IsTest
      
    static void testSearchOrderPop()
    {
        Account Acc = Utils_TestMethods.createAccount();
        Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'Jean Dupont');
        
        insert Acc;
        insert Ctct;
        
        Case Cse1 = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
        Insert Cse1;
        Cse1.AnswertoCustomer__c = 'Answer';
        Update Cse1;
        
                
        PageReference pageRef = Page.VFP_Fsearch;
        Test.setCurrentPage(pageRef);

        VFC_Fsearch controller = new VFC_Fsearch();
        
        String searchOrderText = controller.searchOrderText();
        controller.searchOrderPop();
        String searchOrderText2 = controller.searchOrderText();
        
        System.assert(!searchOrderText.equals(searchOrderText2));
        controller.searchOrderPop();
        String searchOrderText3 = controller.searchOrderText();
     
        System.assert(searchOrderText.equals(searchOrderText3));
        
    }
    
@IsTest

    static void testBackToCase() {
     
         Account Acc = Utils_TestMethods.createAccount();
        Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'Jean Dupont');
        
        insert Acc;
        insert Ctct;
        
        Case Cse1 = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'Open');
        Insert Cse1;
        Cse1.AnswertoCustomer__c = 'Answer';
        Update Cse1;
        
        PageReference pageRef = new PageReference('/apex/VFP_Fsearch?caseid='+ Cse1.Id);

        //pageRef.getParameters().put('caseid', Cse1.Id);
        Test.setCurrentPage(pageRef);
     
         VFC_Fsearch controller = new VFC_Fsearch();
         controller.updateCase();
         PageReference page = controller.backToCase();
          System.assertNotEquals(null,page );
          page = controller.continueToCase();
          System.assertNotEquals(null,page );
          page = controller.updateAndClose();
          System.assertNotEquals(null,page );
          page = controller.getSize();
          System.assertEquals(null,page );
          
         VFC_Fsearch controller2 = new VFC_Fsearch();
         PageReference page2 = new PageReference('/apex/VFP_Fsearch?caseid=123456');
         Test.setCurrentPage(page2 );
         controller2.backToCase();
         
         page2 = controller2.updateAndClose();
          
     }
     
     @IsTest
     static void testClosePopup() {
         VFC_Fsearch controller = new VFC_Fsearch();
         boolean open = controller.displayPopup;
         controller.closePopup();
         boolean closed = controller.displayPopup;
         System.assert(!closed);
         
     }
    
    @IsTest
    static void testGetUserCCC(){
        VFC_Fsearch controller = new VFC_Fsearch();
        String userCCC = controller.getUserCCC();
        //System.assertNotEquals(null,userCCC );
    }


    /*@IsTest
    static void testUpdateCaseWithException() {
    

    
    }*/
}