/*
Author:Siddharth
Purpose:Test Methods for the class VFC74_PlatformingVisitsExecutionKPI
Methods:    Constructor,            
            

*/
@isTest
private class VFC74_PlatformingVisitsExecutionKPI_TEST
{
    static testMethod void runpositivetests() {
    Event e=new Event(Platforming_Customer_Profile__c='Q3',Status__c='Planned',OwnerId=UserInfo.getUserId(),StartDateTime=System.Today()-1,DurationInMinutes=600);
    Event e1=new Event(Platforming_Customer_Profile__c='Q2',Status__c='Planned',OwnerId=UserInfo.getUserId(),StartDateTime=System.Today()-1,DurationInMinutes=600);
    Event e2=new Event(Platforming_Customer_Profile__c='Q1',Status__c='Planned',OwnerId=UserInfo.getUserId(),StartDateTime=System.Today()-1,DurationInMinutes=600);
    
    Event e3=new Event(Platforming_Customer_Profile__c='S1',Status__c='Closed',OwnerId=UserInfo.getUserId(),StartDateTime=System.Today()-1,DurationInMinutes=600);
    Event e4=new Event(Platforming_Customer_Profile__c='S2',Status__c='Closed',OwnerId=UserInfo.getUserId(),StartDateTime=System.Today()-1,DurationInMinutes=600);
    Event e5=new Event(Platforming_Customer_Profile__c='S3',Status__c='Closed',OwnerId=UserInfo.getUserId(),StartDateTime=System.Today()-1,DurationInMinutes=600);
    
    Event e6=new Event(Platforming_Customer_Profile__c='G1',Status__c='Closed',OwnerId=UserInfo.getUserId(),StartDateTime=System.Today()-1,DurationInMinutes=600);
    Event e7=new Event(Platforming_Customer_Profile__c='G2',Status__c='Closed',OwnerId=UserInfo.getUserId(),StartDateTime=System.Today()-1,DurationInMinutes=600);
    Event e8=new Event(Platforming_Customer_Profile__c='G3',Status__c='Closed',OwnerId=UserInfo.getUserId(),StartDateTime=System.Today()-1,DurationInMinutes=600);
    
    Event e9=new Event(Platforming_Customer_Profile__c=null,Status__c='Closed',OwnerId=UserInfo.getUserId(),StartDateTime=System.Today()-1,DurationInMinutes=600);
    List<Event> events=new List<Event>{e,e1,e2,e3,e4,e5,e6,e7,e8,e9};

    insert events;
        VFC74_PlatformingVisitsExecutionKPI kpi=new VFC74_PlatformingVisitsExecutionKPI();
    }
}