global class UserandPermissionSets{
    public PermissionSetandProfileGroup permissionsetandprofilegrouprecord;
    public User userrecord;
    public static Map<String,PermissionSetandProfileGroup> jobFunctionPermissionSetMap=new Map<String,PermissionSetandProfileGroup>();

    static
    {
        jobFunctionPermissionSetMap.put('SE - Salesperson',new PermissionSetandProfileGroup('00eA0000000b2jp',new List<String>{'0PSA0000000L6sW'}));
        jobFunctionPermissionSetMap.put('SE - Sales Manager',new PermissionSetandProfileGroup('00eA0000000b2ju',new List<String>{'0PSA0000000L6sV','0PSA0000000LCUK'}));
        jobFunctionPermissionSetMap.put('SE - Customer Care Agent - Primary',new PermissionSetandProfileGroup('00eA0000000b2hoIAA',new List<String>{'0PSA0000000L8FaOAK','0PSA0000000L8JmOAK','0PSA0000000L8K1OAK','0PSA0000000L98VOAS','0PSA0000000L9ZlOAK','0PSA0000000LBbUOAW','0PSA0000000LBbeOAG','0PSA0000000LBbyOAG','0PSA0000000LBcSOAW','0PSA0000000LBedOAG'}));
        jobFunctionPermissionSetMap.put('SE - Non-sales RITE Approver',new PermissionSetandProfileGroup('00eA0000000b2jp',new List<String>{'0PSA0000000L6sW','0PSA0000000L6sV'}));
        jobFunctionPermissionSetMap.put('SE - Sales Standard User',new PermissionSetandProfileGroup('00eA0000000b2jp',new List<String>{'0PSA0000000LF4t'}));
    }

public UserandPermissionSets(String alias,String jobfunction){
    if(jobfunction!=null && jobFunctionPermissionSetMap.containsKey(jobfunction)){
        this.permissionsetandprofilegrouprecord=jobFunctionPermissionSetMap.get(jobfunction);
        this.userrecord = new User(alias = alias,
            email=alias + '@schneider-electric.com',
            emailencodingkey='UTF-8',
            lastname='Testing',
            languagelocalekey='en_US',
            localesidkey='en_US',
            BypassWF__c = true,
            profileid = this.permissionsetandprofilegrouprecord.profileId,
            timezonesidkey='Europe/London',
            username=alias + '@bridge-fo.com',
            CAPRelevant__c=true,
            VisitObjDay__c=1,
            AvTimeVisit__c=3,
            WorkDaysYear__c=200,
            BusinessHours__c=8,
            UserBusinessUnit__c='Cross Business',
            Country__c='France',
            UserRoleId=System.Label.CLDEC13SLS40,
            Permission_sets_assigned__c=System.Label.CLJUN13SLS02);
    }
}   

public class PermissionSetandProfileGroup{
    public Id profileId;
    public List<Id> permissionsetIds;       
    public PermissionSetandProfileGroup(String strprofileid,List<String> strpermissionsetIds)
    {
        permissionsetIds=new List<Id>();
        profileid=Id.valueOf(strprofileid);
        for(String tempStr:strpermissionsetIds)
            permissionsetIds.add(Id.valueOf(tempStr));
    }
}
    public static void justInsert(List<Sobject> sobjectslist){
       Database.insert(sobjectslist);
    }
}