@IsTest
public with sharing class Batch_PRM_UserMigration_TEST {
	
	public static Map<Id,PRMPartnerMigration__c> prepareScenario(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        Country__c france = new Country__c(CountryCode__c='FR',Name='FRANCE');
        insert france;
        Account acc = Utils_TestMethods.createAccount();
        acc.PRMUIMSId__c ='theAccExtId_1';    
        acc.Country__c=france.Id;
        acc.Street__c='my Street';
        insert acc;
        
        Contact con = Utils_TestMethods.createContact(acc.Id,'myContactName');
        con.PRMUIMSID__c =  'theContExtId_1';
        con.email='prm.user@account-site.com';
        insert con;
		
        FieloEE__Member__c mem = new FieloEE__Member__c(
            FieloEE__FirstName__c = 'test',
            FieloEE__LastName__c= 'test'
        );
        insert mem;
        
        Contact con2 = Utils_TestMethods.createContact(acc.Id,'myContactName 2');
        con2.PRMUIMSID__c =  'theContExtId_2';
        con2.PRMEmail__c ='prm.user2@account-site.com';
        con2.email='prm.user2@account-site.com';
        con2.FieloEE__Member__c = mem.Id;
        con2.PRMCountry__c = france.Id;
        insert con2;
        
        Contact con3 = Utils_TestMethods.createContact(acc.Id,'myContactName 33');
        con3.PRMUIMSID__c =  'theContExtId_312ca';
        con3.PRMEmail__c ='prm.use3r2@account3-site.com';
        con3.email='prm.user333@account-s3ite.com';
        con3.FieloEE__Member__c = mem.Id;
        con3.PRMCountry__c = france.Id;
        insert con3;

        Id ProfileFieloId = [SELECT Id FROM Profile WHERE Name = 'SE - Channel Partner (Community)' limit 1][0].Id;
        User u1 = new User(FirstName = con2.FirstName, ProfileId = ProfileFieloId, FederationIdentifier=con2.PRMUIMSId__c,LastName = con2.LastName, Email = con2.PRMEmail__c, Username = con2.PRMEmail__c + '.bfo.com',Alias ='teOo8_', CommunityNickname = con2.Name+'8_9', ContactId = con2.Id, CurrencyIsoCode = 'USD',TimeZoneSidKey = 'GMT', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',UserPermissionsMobileUser = false);
        insert u1;


        PRM_UserMigration__c cusSetting = new PRM_UserMigration__c(); 
        cusSetting.LanguageLocaleKey__c = 'en_US' ;
        cusSetting.LocaleSidKey__c = 'fr_FR' ;
        cusSetting.TimeZoneSidKey__c = 'GMT';
        cusSetting.CurrencyIsoCode__c = 'EUR';
        cusSetting.name = 'FR';
        insert cusSetting;
        
        List<PRMPartnerMigration__c> myMigList = new List<PRMPartnerMigration__c>{new PRMPartnerMigration__c(Contact__c=con.Id),new PRMPartnerMigration__c(Contact__c=con2.Id,MemberId__c=mem.Id),new PRMPartnerMigration__c(Contact__c=con3.Id,MemberId__c=mem.Id)};
        insert myMigList;
        return new Map<Id,PRMPartnerMigration__c>{con.Id=>myMigList[0],con2.Id=>myMigList[1],con3.Id=>myMigList[2]};
    }

	public static testMethod void testStep0(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			prepareScenario();
       		Test.startTest();
	        Batch_PRM_UserMigration batchPRMUserMigration = new Batch_PRM_UserMigration(true,true,true,true,true,'',0,50,1); 
			Id jobId = Database.executebatch(batchPRMUserMigration);
			Test.stopTest();
		}
    }

    public static testMethod void testStep1(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			prepareScenario();
			Test.startTest();
	        Batch_PRM_UserMigration batchPRMUserMigration = new Batch_PRM_UserMigration(true,true,true,true,true,'',1,50,1); 
			Id jobId = Database.executebatch(batchPRMUserMigration);
			Test.stopTest();
		}
    }

    public static testMethod void testStep2(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			prepareScenario();
			Test.startTest();
	        Batch_PRM_UserMigration batchPRMUserMigration = new Batch_PRM_UserMigration(true,true,true,true,true,'',2,50,1); 
			Id jobId = Database.executebatch(batchPRMUserMigration);
			Test.stopTest();
		}
    }

    public static testMethod void testStep3(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			prepareScenario();
			Test.startTest();
	        Batch_PRM_UserMigration batchPRMUserMigration = new Batch_PRM_UserMigration(true,true,true,true,true,'',3,50,1); 
			Id jobId = Database.executebatch(batchPRMUserMigration);
			Test.stopTest();
		}
    }

    public static testMethod void testStep4(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			prepareScenario();
			Test.startTest();
	        Batch_PRM_UserMigration batchPRMUserMigration = new Batch_PRM_UserMigration(true,true,true,true,true,'',4,50,1); 
			Id jobId = Database.executebatch(batchPRMUserMigration);
			Test.stopTest();
		}
    }

    public static testMethod void testStep5(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			prepareScenario();
			Test.startTest();
	        Batch_PRM_UserMigration batchPRMUserMigration = new Batch_PRM_UserMigration(true,true,true,true,true,'',1,50,3); 
			Id jobId = Database.executebatch(batchPRMUserMigration);
			Test.stopTest();
		}
    }

    public static testMethod void testStep6(){    
        Id UserRoleID1 = [select UserRoleID from user where profile.Name = 'System Administrator'][0].UserRoleID;
        User sysAdmin=Utils_TestMethods.createStandardUser('sys1');
        sysAdmin.UserRoleID = UserRoleID1;
        sysAdmin.isActive=true;
        sysAdmin.BypassVR__c = true;
		System.runAs ( sysAdmin ) {
			prepareScenario();
			Test.startTest();
	        Batch_PRM_UserMigration batchPRMUserMigration = new Batch_PRM_UserMigration(true,true,true,true,true,'',6,50,1); 
			Id jobId = Database.executebatch(batchPRMUserMigration);
			Test.stopTest();
		}
    }
}