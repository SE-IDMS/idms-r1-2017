@isTest
private class BATCH_CreateSeries_TEST
{     
    static List<OppAgreement__c> aggList = new List<OppAgreement__c>();
    static ProductLineForAgreement__c[] objProdLine = new List<ProductLineForAgreement__c>();
    static User user,newUser,newUser2 ;
    static Country__c newCountry;
    static OppAgreement__c objAgree;
    static Account acc;
    static Opportunity objOpp,opp2,opp3,opp4;
    static OppAgreement__c objAgree2,objAgree3;
    static List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();   
    static List<OppAgreement__c> oa = new List<OppAgreement__c>();
    static OPP_Product__c  prod = new OPP_Product__c();
    static
    {
        VFC61_OpptyClone.isOpptyClone = true;
        acc = Utils_TestMethods.createAccount();
         insert acc;
        
        opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.StageName = '3 - Identify & Qualify';
        insert opp2; 
        objAgree2 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree2.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree2.AgreementValidFrom__c = Date.today();         
        objAgree2.FrameOpportunityInterval__c = 'Monthly';
        objAgree2.SignatureDate__c = Date.today();
        objAgree2.Agreement_Duration_in_month__c = '3';
        objAgree2.TECH_IsCreateByBatch__c = true;
        insert objAgree2;
        
        prod.ProductLine__c = null;
        prod.BusinessUnit__c = 'EcoBuilding';
        insert prod;
        opp2.AgreementReference__c = objAgree2.Id;
        update opp2;        
        opp3 = Utils_TestMethods.createOpportunity(acc.Id);        
        opp3.AgreementReference__c = objAgree2.Id;        
        insert opp3; 
        opp4 = Utils_TestMethods.createOpportunity(acc.Id);        
        opp4.AgreementReference__c = objAgree2.Id;        
        insert opp4;
        //Inserts the Product Lines        
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            prodLines.add(pl1);
        }
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp3.Id);
            pl1.Quantity__c = 10;
            prodLines.add(pl1);
        }
        Insert prodLines; 
        objAgree3 = Utils_TestMethods.createFrameAgreementWithAccount(UserInfo.getUserId(),acc.Id );         
        objAgree3.PhaseSalesStage__c = '3 - Identify & Qualify';   
        objAgree3.AgreementValidFrom__c = Date.today();         
        objAgree3.FrameOpportunityInterval__c = Label.CL00221;
        objAgree3.SignatureDate__c = Date.today();
        objAgree3.Agreement_Duration_in_month__c = '2';
        insert objAgree3;        
    }     

    static testmethod void testCreateSeries()
    {  
        batch_CreateSEries creSers = new Batch_CreateSeries(objAgree2.Id); 
        Test.startTest();
        database.executeBatch(creSers,10);
        Test.stopTest();
    }
}