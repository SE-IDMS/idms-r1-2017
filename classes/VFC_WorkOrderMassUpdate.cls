public with sharing class VFC_WorkOrderMassUpdate {

	public List<SVMXC__Service_Order__c> woList = new List<SVMXC__Service_Order__c>();
	public SVMXC__Service_Order__c wo{get;set;}
	public Boolean isRescheduled{get;set;}

	public Boolean IsSelected{get;set;}
	public Boolean saveSuccess{get;set;}

	public String isSendCustomerConfirmationEmail{    
        get {
            if (isSendCustomerConfirmationEmail==null) {
                isSendCustomerConfirmationEmail='';
            }
            return isSendCustomerConfirmationEmail;
        }
        set;
    }
	public List<SelectOption> optionsList{get;set;}

	Set<id> woidset = new Set<id>();

	public VFC_WorkOrderMassUpdate(ApexPages.StandardSetController OwStdListController) {
		IsSelected = false;
		saveSuccess = false;
		woList = (List<SVMXC__Service_Order__c>) OwStdListController.getSelected();
		isRescheduled =  false;

		wo = new SVMXC__Service_Order__c();
		for(SVMXC__Service_Order__c w:this.woList){
			woidset.add(w.id);
		}
		if(woidset!= null && woidset.size()>0){
			IsSelected =true;
            optionsList = new List<SelectOption>();
            optionsList.add(new SelectOption('', '--None--'));
            optionsList.add(new SelectOption('True', 'Yes'));
			optionsList.add(new SelectOption('False', 'No'));           
		} 
		else{
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please select at least one work order to update'));
		}  
	}
	public void doSave(){
		try{
			saveSuccess = true; // we are going to save. By default, we consider the save to be a success. While reviewing the save result, if we have at least one error, we will set saveSuccess to false.
			woList = [SELECT id, Name, SVMXC__Order_Status__c, RescheduleReason__c, SVMXC__Company__c, SVMXC__Site__c, SVMXC__Contact__c, CustomerRequestedDate__c, CountryOfBackOffice__c, BackOfficeSystem__c, SVMXC__Order_Type__c, WorkOrderSubType__c, IsBillable__c, SVMXC__Billing_Type__c, SendConfirmationEmail__c, SVMXC__Primary_Territory__c, CustomerTimeZone__c, PlannerContact__c, SVMXC__Special_Instructions__c FROM  SVMXC__Service_Order__c  WHERE id in :woidset];

			for(SVMXC__Service_Order__c worder : this.woList){
				// Manage the status if the user has selected a status
				if (wo.SVMXC__Order_Status__c != null) {
					worder.SVMXC__Order_Status__c =  wo.SVMXC__Order_Status__c;
					if(isRescheduled) {
						worder.RescheduleReason__c = wo.RescheduleReason__c   ;
					}
				}

				// Manage the Installed At Account if the user has selected one
				if (wo.SVMXC__Company__c != null) {
					worder.SVMXC__Company__c =  wo.SVMXC__Company__c;
				}
				// Manage the Location if the user has selected one
				if (wo.SVMXC__Site__c != null) {
					worder.SVMXC__Site__c =  wo.SVMXC__Site__c;
				}
				// Manage the Contact if the user has selected one
				if (wo.SVMXC__Contact__c != null) {
					worder.SVMXC__Contact__c =  wo.SVMXC__Contact__c;
				}
                
				// Manage the Customer Requested Date if the user has selected one
				if (wo.CustomerRequestedDate__c != null) {
					worder.CustomerRequestedDate__c =  wo.CustomerRequestedDate__c;
				}  
				
				// Manage the Country of Back Office if the user has selected one
				if (wo.CountryOfBackOffice__c != null) {
					worder.CountryOfBackOffice__c =  wo.CountryOfBackOffice__c;
				} 
				
				// Manage the Back Office System if the user has selected one
				if (wo.BackOfficeSystem__c != null) {
					worder.BackOfficeSystem__c =  wo.BackOfficeSystem__c;
				}
                
				// Manage the Work Order Type if the user has selected one
				if (wo.SVMXC__Order_Type__c != null) {
					worder.SVMXC__Order_Type__c =  wo.SVMXC__Order_Type__c;
				}
                
				// Manage the Work Order Sub-Type__c if the user has selected one
				if (wo.WorkOrderSubType__c != null) {
					worder.WorkOrderSubType__c =  wo.WorkOrderSubType__c;
				}
                
				// Manage the non Billable flag if the user has selected one
				if (wo.IsBillable__c != null) {
					worder.IsBillable__c =  wo.IsBillable__c;
				}
                
				// Manage the non billable reason if the user has selected one
				if (wo.SVMXC__Billing_Type__c != null) {
					worder.SVMXC__Billing_Type__c =  wo.SVMXC__Billing_Type__c;
				}
                
				// Manage the Work Order Sub-Type__c if the user has selected one
				if (isSendCustomerConfirmationEmail != null && isSendCustomerConfirmationEmail != '') {
					worder.SendConfirmationEmail__c =  Boolean.valueOf(isSendCustomerConfirmationEmail);
				}
                
				// Manage the non billable reason if the user has selected one
				if (wo.SVMXC__Primary_Territory__c != null) {
					worder.SVMXC__Primary_Territory__c =  wo.SVMXC__Primary_Territory__c;
				}
                
				// Manage the non billable reason if the user has selected one
				if (wo.CustomerTimeZone__c != null) {
					worder.CustomerTimeZone__c =  wo.CustomerTimeZone__c;
				}
                
				// Manage the non billable reason if the user has selected one
				if (wo.PlannerContact__c != null) {
					worder.PlannerContact__c =  wo.PlannerContact__c;
				}
                
				// Manage the non billable reason if the user has selected one
				if (wo.SVMXC__Special_Instructions__c != null) {
					worder.SVMXC__Special_Instructions__c =  wo.SVMXC__Special_Instructions__c;
				}
                
                System.debug('#### isSendCustomerConfirmationEmail: '+isSendCustomerConfirmationEmail);
			}
			
			try {
				Database.saveResult []svr = Database.update(woList, false); // Updating the work orders.
				Integer i = 0;
				String woNumber;
				List<SVMXC__Service_Order__c> successList = new List<SVMXC__Service_Order__c>();
				List<SVMXC__Service_Order__c> failureList = new List<SVMXC__Service_Order__c>();
				for(Database.SaveResult svrt : svr)
				{
					if (woList.get(i) != null) {
						woNumber = woList.get(i).name;
					}
					if(!svrt.isSuccess()){
						saveSuccess = false;
						System.Debug('########  Error Update: '+i+'-'+': '+svrt.getErrors()[0].getStatusCode()+' '+svrt.getErrors()[0].getMessage());
						ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,woNumber+': '+svrt.getErrors()[0].getMessage()));
					} 
					else{
						ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,woNumber+': successfully updated'));
					}
					i++;
				}
			}
			catch (Exception e) {
				saveSuccess = false;
				System.Debug(logginglevel.WARN, '########  Error while updating WOs: '+ e.getMessage());
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Error while updating WOs'+e.getMessage()));
			}

			//return doRedirectAfterSave();
			
			/*
			if(isSuccess )
			{
				return doRedirect();
			}
			else{
				return null;
			}
			*/
		}
		catch(Exception ex){
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
			//return null ;
		}
	}

	public PageReference  doCancel(){
		return doRedirect();
	}
	
	public PageReference doRedirect(){
		System.Debug(logginglevel.INFO, '######## doRedirect');
		String SOPrefix = SObjectType.SVMXC__Service_Order__c.getKeyPrefix(); 
		String url='/'+SOPrefix;
		PageReference pref = new PageReference(url);
		pref.setRedirect(true);
		return pref ;
	}
	
	public PageReference call(){
        if(wo.SVMXC__Order_Status__c == System.label.CLMAY13SRV42) {
            isRescheduled = true;
        }
        else{
            isRescheduled = false;
        }
        return null;
	}

}