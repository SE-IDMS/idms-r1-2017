/*
    Author          : Rajeev (ACN) 
    Date Created    : 10/02/2011
    Description     : Test class for the apex class AP08_CaseStatusUpdate.
*/


@isTest

private class AP08_CaseStatusUpdate_TEST {
    
/* @Method <This method caseStatusTestMethod is used test the AP01_WorkOrderNotification class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Not throwing any Exception>
*/
    static testMethod void updateCaseStatusMethod(){
        
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
        
        Contact contact1 = Utils_TestMethods.createContact(account1.Id,'TestContact');
        insert contact1;
        
        Case case1 = Utils_TestMethods.createCase(account1.Id, contact1.Id, 'Open');
        insert case1;
        
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        CustomerLocation__c customerLocation = Utils_TestMethods.createCustomerLocation(account1.id, country.id);
        Database.insert(customerLocation);
        
        Test.startTest();
        
        WorkOrderNotification__c workOrderNotification1 = Utils_TestMethods.createWorkOrderNotification(Case1.Id, customerLocation.id);
        workOrderNotification1.OnSiteContactSameasCaseContact__c = 'Yes';
        workOrderNotification1.OnSiteContact__c = contact1.id;
        insert workOrderNotification1;
        
       // Case case2 = [Select Id,AnswerToCustomer__c from case where Id=:case1.Id];
        case1.AnswerToCustomer__c='Refresh the Page.';
        update case1;
        
        WorkOrderNotification__c workOrderNotification2 = Utils_TestMethods.createWorkOrderNotification(case1.Id, customerLocation.id);
        workOrderNotification2.OnSiteContactSameasCaseContact__c = 'Yes';
        workOrderNotification2.OnSiteContact__c = contact1.id;
        insert workOrderNotification2;
        
       // Case caseRecord = [Select Id,status,AnswerToCustomer__c from case where id=:case1.Id];
       // System.debug('caseRecord '+caseRecord);
        //System.assertEquals(caseRecord.Status,Label.CL00230);
       // System.assertEquals(caseRecord.AnswerToCustomer__c,'Refresh the Page. '+Label.CL00266);
        
        //Close case if all WON are closed
        workOrderNotification1.Work_Order_Status__c = 'Closed';
        Update workOrderNotification1;
        
        workOrderNotification2.Work_Order_Status__c = 'Closed';
        Update workOrderNotification2;
        
        Test.stopTest();
    }
}