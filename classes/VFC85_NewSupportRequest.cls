public with sharing class VFC85_NewSupportRequest
{
    public OPP_SupportRequest__c oppSupReq;    
    public VFC85_NewSupportRequest(ApexPages.StandardController controller) 
    {        
        Utils_SDF_Methodology.startTimer();
        oppSupReq=(OPP_SupportRequest__c)controller.getRecord();                
        oppSupReq.Opportunity__c=ApexPages.currentPage().getParameters().get('oppid'); 
        
        if(ApexPages.currentPage().getParameters().get('RecordType')==Label.CL10092 || ApexPages.currentPage().getParameters().get('RecordType')==Label.CL10094) //selling||transfer
            oppSupReq.SellingCenter__c=ApexPages.currentPage().getParameters().get('scid');        
        else if(ApexPages.currentPage().getParameters().get('RecordType')==Label.CL10093) //solution
            oppSupReq.SolutionCenter__c=ApexPages.currentPage().getParameters().get('scid');        
       Utils_SDF_Methodology.stopTimer();
       Utils_SDF_Methodology.Limits();
    }
    public String getSolCenName()
    {
        return ApexPages.currentPage().getParameters().get('scname');
    }
    public PageReference Save()
    {        
         Opportunity oppt=[select id from Opportunity where id =:oppSupReq.Opportunity__c];         
         Savepoint sp = Database.setSavepoint(); 
         Database.saveResult svr = Database.update(oppt,false);
        if(!svr.isSuccess())        {
            oppSupReq.addError(svr.getErrors()[0].getMessage());      
            return null; 
                }
        Database.rollback(sp);   
        Database.SaveResult sr=Database.insert(oppSupReq,false);        
        if(!sr.isSuccess())
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,sr.getErrors()[0].getMessage()));
            return null;
        }        
        PageReference pg = new ApexPages.StandardController(oppSupReq).view();
        return pg;                          
    }
    
    public PageReference Cancel()
    {
        PageReference pg = new PageReference('/'+ApexPages.currentPage().getParameters().get('oppid'));
        pg.setRedirect(true);
        return pg;
    }
    public PageReference SaveAndNew()
    {
        Opportunity oppt=[select id from Opportunity where id =:oppSupReq.Opportunity__c];         
        
        Savepoint sp = Database.setSavepoint(); 
        Database.saveResult svr = Database.update(oppt,false);
        if(!svr.isSuccess()){
            oppSupReq.addError(svr.getErrors()[0].getMessage());      
            return null; 
        }
        Database.rollback(sp);   
        
        Database.SaveResult sr=Database.insert(oppSupReq,false);        
        if(!sr.isSuccess())
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,sr.getErrors()[0].getMessage()));
            return null;
        }        
        String oppid;
        if(oppSupReq.Opportunity__c!=null)
        oppid=oppSupReq.Opportunity__c;
        else
        oppid=ApexPages.currentPage().getParameters().get('oppid');                  
        PageReference pg=new PageReference('/setup/ui/recordtypeselect.jsp?ent='+ApexPages.currentPage().getParameters().get('ent')+'&retURL=/'+oppid+'&save_new_url=/a0T/e?CF00NA000000559Lc='+[select name from Opportunity where id=:oppid].name+'&CF00NA000000559Lc_lkid='+oppid+'&scontrolCaching=1&retURL=%2F'+oppid);      
        return pg;                
    }
    public String getRerenderCenterTypeDisplay()
    {    
       if(ApexPages.currentPage().getParameters().get('RecordType')==Label.CL10092 || ApexPages.currentPage().getParameters().get('RecordType')==Label.CL10094) //selling||transfer
            return 'sel';
       else if(ApexPages.currentPage().getParameters().get('RecordType')==Label.CL10093) //solution
           return 'sol';
       return 'null';
    }
    
    
  public class noMessageException extends Exception{}
}