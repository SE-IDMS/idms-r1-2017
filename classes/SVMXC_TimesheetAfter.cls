public class SVMXC_TimesheetAfter {

	public static void submitTimesheetForApproval (List<SVMXC_Timesheet__c> triggerNew, Map<Id, SVMXC_Timesheet__c> triggerOld, Boolean isInsert, Boolean isUpdate) { 
	 	/*
            Method to submit a timesheet to the approval process (Written Mar 3 2015)
        */
		
		 if (isUpdate) {
    
    		 // obtain running user mgr field
			User runningUser = [SELECT ManagerId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
    
            for (SVMXC_Timesheet__c ts1: triggerNew) {
             
                if (ts1.Submit_for_Approval__c) {       
                
                	if (runningUser.ManagerId != null) {
                		
	                    // create the new approval request to submit
	                    Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
	                    
	                    if (ts1.Submission_Notes__c != null)
	                    	req.setComments(ts1.Submission_Notes__c);
	                    else
	                    	req.setComments('Submitted for approval via Submit for Approval button.');
	                    	
	                    req.setObjectId(ts1.Id);
	                    // submit the approval request for processing
	                    Approval.ProcessResult result = Approval.process(req);
	                    // display if the reqeust was successful
	                    System.debug('Submitted for approval successfully: '+result.isSuccess());
	                    
	                    // if it was successfully submitted for approval, update the WDL Line Status
	                    if (result.isSuccess()) {
	                        // do anything else?
	                    } else {
	                        ts1.addError('There was an issue submitting this record to the approval process');
	                    }
                		
                	} else {
                		// submitter is missing the manager field on their user record
                		ts1.addError('This Timesheet can not be submitted for approval because your user record does not contain a manager.  Please contact your administrator to get one established.');
                	}
                }    
            }
        } 	
	}
}