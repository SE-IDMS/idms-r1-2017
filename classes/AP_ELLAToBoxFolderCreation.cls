/*****
  
Future method invoke the Box API Calls
******/

//Region/country Box folder creation 
Public class AP_ELLAToBoxFolderCreation {   
    
   
    @future (callout=true)
     public static void OfferFolderCreationInBox(set<String> offerId ){ 
        LIST<Offer_Lifecycle__c> listOfferUpdate = new LIST<Offer_Lifecycle__c>();
        try {
        
            boxResponseObject boxRes=new boxResponseObject();
            string folder_id;
            LIST<Offer_Lifecycle__c> offerList =[select Id, BoxUserEmailId__c,Launch_Owner__c,Launch_Owner__r.email,Name,BoxSharedLink__c,BoxFolderId__c from Offer_Lifecycle__c where Id=: offerId];
            if(offerList.size() > 0)  {
                for(Offer_Lifecycle__c offerObj:offerList) {
          
                    if(offerObj.BoxFolderId__c==null && offerObj.BoxUserEmailId__c!=null ) {
                    
                        /*****New folder creation IN BOX ******/
                        folder_id=AP_ELLAToBoxConnect.folderStructureCreation(offerObj.Name,null,offerObj.BoxUserEmailId__c); 
                        if(folder_id != null) {
                            boxRes = (boxResponseObject)JSON.deserialize(folder_id, boxResponseObject.class);
                           
                        }
                        
                        if(boxRes.folderId!=null || boxRes.folderId!='' ) {    
                            offerObj.BoxFolderId__c=boxRes.folderId;
                            offerObj.BoxSharedLink__c =boxRes.folderShareLink;
                            offerObj.BoxCollaboration__c=boxRes.CollaborationId;
                            listOfferUpdate.add(offerObj);
                        
                        }               
                                                
                        system.debug('HHHHddddH'+folder_id);    
                    }
                }
                if(listOfferUpdate.size() > 0 && folder_id !=null ) {
                    database.saveresult[] srlist = database.update(listOfferUpdate, false);                    
                }
            }
        }catch (exception exp) {
            system.debug('TEST'+exp);
        
        }
    
    }
    
     @future (callout=true)
     public static void httpProjectCountryFolderInBox(Set<string> strProjectId ){ 
     
        try {
        LIST<Milestone1_Project__c> listProject = new LIST<Milestone1_Project__c>();       
        string folder_id;        
        boxResponseObject boxProjectRes=new boxResponseObject();
         Set<string>  setOfferId = new Set<string>();
        Set<string>  setCountryId = new Set<string>();
        Set<string>  setZoneId = new Set<string>();
         List<Milestone1_Project__c> projectCountry = new  List<Milestone1_Project__c> ();
        //LIST<Milestone1_Project__c> projectList = new LIST<Milestone1_Project__c>();
        LIST<Milestone1_Project__c> projectListBoxID = new LIST<Milestone1_Project__c>();
        
        LIST<Milestone1_Project__c> projectList =[select Id,Name, BoxUserEmailId__c,Zone__c,Country_Launch_Leader__c,Country_Launch_Leader__r.email,Offer_Launch__c,Country__c,Country__r.name,Offer_Launch__r.BoxFolderId__c,BoxSharedLink__c,BoxFolderId__c from Milestone1_Project__c where Id=: strProjectId];
      
        for(Milestone1_Project__c proOffObj:projectList) {
            setOfferId.add(proOffObj.Offer_Launch__c);
            setCountryId.add(proOffObj.Country__r.name);
            if(proOffObj.Country__r.name!=null) {
                    setCountryId.add(proOffObj.Country__r.name);
            }
            
            if(proOffObj.Zone__c!=null) {
            setZoneId.add(proOffObj.Zone__c);
            }
        }
        if(setCountryId.size() > 0) {
            projectCountry=[select Id,Name, BoxUserEmailId__c,Country_Launch_Leader__c,Zone__c,Country_Launch_Leader__r.email,Offer_Launch__c,Country__c,Country__r.name,Offer_Launch__r.BoxFolderId__c,BoxSharedLink__c,BoxFolderId__c from Milestone1_Project__c where Offer_Launch__c=:setOfferId and Country__r.name=:setCountryId ];
        }
        if(setZoneId.size() > 0) {
            projectCountry=[select Id,Name, BoxUserEmailId__c,Country_Launch_Leader__c,Zone__c,Country_Launch_Leader__r.email,Offer_Launch__c,Country__c,Country__r.name,Offer_Launch__r.BoxFolderId__c,BoxSharedLink__c,BoxFolderId__c from Milestone1_Project__c where Offer_Launch__c=:setOfferId and Zone__c=:setZoneId ];
        }
        
        for(Milestone1_Project__c proObj1:projectCountry) {
            if(proObj1.BoxFolderId__c!=null) {
                projectListBoxID.add(proObj1);
            }
        }   
      
        if(projectList.size() > 0 && projectListBoxID.size() ==0) {
        
            /*****New folder and file creation IN BOX ******/
            for(Milestone1_Project__c proObj:projectList) {            
                if(proObj.Offer_Launch__r.BoxFolderId__c!=null && proObj.BoxFolderId__c==null && proObj.Country__r.name!=null && proObj.BoxUserEmailId__c!=null)  {  
                    
                    folder_id=AP_ELLAToBoxConnect.folderStructureCreation (proObj.Country__r.name,proObj.Offer_Launch__r.BoxFolderId__c,proObj.BoxUserEmailId__c); 

                                       
                } 
                if(proObj.Offer_Launch__r.BoxFolderId__c!=null && proObj.BoxFolderId__c==null && proObj.Zone__c!=null && proObj.BoxUserEmailId__c!=null) {
                    folder_id=AP_ELLAToBoxConnect.folderStructureCreation (proObj.Zone__c,proObj.Offer_Launch__r.BoxFolderId__c,proObj.BoxUserEmailId__c); 

                }

                if(folder_id != null && folder_id!='') {
                        boxProjectRes = (boxResponseObject)JSON.deserialize(folder_id, boxResponseObject.class);
                        if(boxProjectRes.folderId!=null) {
                            proObj.BoxFolderId__c=boxProjectRes.folderId;
                            proObj.BoxSharedLink__c =boxProjectRes.folderShareLink;
                            proObj.BoxCollaboration__c=boxProjectRes.CollaborationId;
                        }
                        listProject.add(proObj);

                }   
            }
            
            if(listProject.size() > 0 && folder_id !=null ) {
            
                database.saveresult[] srlist = database.update(listProject, false);
                       
            }
            system.debug('HHHHddddH'+folder_id);    
        }//for update if user created 2 country for same offer.
        else if(projectList.size() > 0 && projectListBoxID.size()>0) {
            system.debug('Test-->'+projectCountry);
                    Map<string,Milestone1_Project__c> mapProjt= new Map<string,Milestone1_Project__c>();
                    list<Milestone1_Project__c>  updateMProject = new list<Milestone1_Project__c>();
                    List<Milestone1_Project__c> lisIndroduction = new List<Milestone1_Project__c>();
                    for(Milestone1_Project__c mpObj:projectCountry) {
                    
                            if(mpObj.BoxFolderId__c!=null && mpObj.Country__r.name!=null) {
                               
                                    mapProjt.put(mpObj.Country__r.name,mpObj);
                               
                            }
                    }
                    
                    for(Milestone1_Project__c mpObj1:projectCountry) {
                        
                        if(mpObj1.BoxFolderId__c==null && mpObj1.Country__r.name!=null) {
                            if(mapProjt.containsKey(mpObj1.Country__r.name)) {
                                mpObj1.BoxFolderId__c=mapProjt.get(mpObj1.Country__r.name).BoxFolderId__c;
                                mpObj1.BoxSharedLink__c  =mapProjt.get(mpObj1.Country__r.name).BoxSharedLink__c ;
                                
                                updateMProject.add(mpObj1);
                            }
                           

                        }
                    
                    }
                    for(Milestone1_Project__c mpObj2:projectCountry) {
                         if(mpObj2.BoxUserEmailId__c!=null && mpObj2.BoxFolderId__c==null ) {
                            system.debug('Test-->'+mpObj2.BoxUserEmailId__c);
                            system.debug('Test-->1'+mapProjt);
                                if(mpObj2.BoxUserEmailId__c !=mapProjt.get(mpObj2.Country__r.name).BoxUserEmailId__c) {
                                    lisIndroduction.add(mpObj2);
                                }   
                            }
                    }
                    system.debug('Test-->'+updateMProject);
                    if(updateMProject.size() > 0) {
                        update updateMProject;
                        
                        if(lisIndroduction.size() > 0) {
                            for(Milestone1_Project__c mProjectObj:lisIndroduction) {
                                string collabora=AP_ELLAToBoxConnect.box_collaboraFolder(mProjectObj.Offer_Launch__r.BoxFolderId__c,mProjectObj.BoxUserEmailId__c);

                            }   
                        }
                        
                        //serviceResult =label.CLAPR15ELLAbFO11;//'Folder is created successfully ';
                    }              
        
        }
        }catch (exception exp) {

        }   
    
    }
    
    @future (callout=true)
     public static void OfferCollbrCreationInBox(set<String> offerIdSet ){ 
        LIST<Offer_Lifecycle__c> listOfferUpdate = new LIST<Offer_Lifecycle__c>();
        try {

            boxResponseObject boxRes=new boxResponseObject();
            string Collabora_id;
            LIST<Offer_Lifecycle__c> offerCalbList =[select Id, BoxCollaboration__c,BoxUserEmailId__c,Launch_Owner__c,Launch_Owner__r.email,Name,BoxSharedLink__c,BoxFolderId__c from Offer_Lifecycle__c where Id=: offerIdSet and BoxFolderId__c!=null];

            if(offerCalbList.size() > 0)  {

                 for(Offer_Lifecycle__c offerObj:offerCalbList) {
          
                    if(offerObj.BoxFolderId__c!=null && offerObj.BoxUserEmailId__c!=null ) {
                    
                        /*****New folder creation IN BOX ******/
                        Collabora_id=AP_ELLAToBoxConnect.addBoxCollaboraFolder(offerObj.BoxFolderId__c,offerObj.BoxUserEmailId__c); 
                        if(Collabora_id != null) {
                            boxRes = (boxResponseObject)JSON.deserialize(Collabora_id, boxResponseObject.class);
                           
                        }
                        
                        if(boxRes.CollaborationId!=null || boxRes.CollaborationId!='' ) {    
                            offerObj.BoxCollaboration__c=boxRes.CollaborationId;
                            //offerObj.BoxSharedLink__c =boxRes.folderShareLink;
                            listOfferUpdate.add(offerObj);
                        
                        }               
                                                
                        system.debug('HHHHddddH'+Collabora_id);    
                    }
                }
                
                if(listOfferUpdate.size() > 0 ) {
                    database.saveresult[] srlist = database.update(listOfferUpdate, false);                    
                }
            }
            
        
        
        }catch (exception exp) {
        
        }
     
     }
     
    @future (callout=true)
    public static void projectAddCollbrCreationInBox(set<String> strCalbProjectId ) {

        try{
        
            LIST<Milestone1_Project__c> listColbProject = new LIST<Milestone1_Project__c>(); 
            string projectCollabora_id;        
            boxResponseObject boxProjectRes=new boxResponseObject();
            list<Milestone1_Project__c>  updateMProject = new list<Milestone1_Project__c>();
            LIST<Milestone1_Project__c> projectList =[select Id,Name, BoxUserEmailId__c,Zone__c,Country_Launch_Leader__c,Country_Launch_Leader__r.email,Offer_Launch__c,Country__c,Country__r.name,Offer_Launch__r.BoxFolderId__c,BoxSharedLink__c,BoxFolderId__c from Milestone1_Project__c where Id=: strCalbProjectId and BoxFolderId__c!=null];
        
            if(projectList.size() > 0) {
                 
                for(Milestone1_Project__c proObj:projectList) {
                    if(proObj.Offer_Launch__r.BoxFolderId__c!=null && proObj.BoxFolderId__c!=null && (proObj.Country__r.name!=null || proObj.Zone__c!=null) && proObj.BoxUserEmailId__c!=null)  {  
                        projectCollabora_id=AP_ELLAToBoxConnect.addBoxCollaboraFolder(proObj.BoxFolderId__c,proObj.BoxUserEmailId__c); 
                        if(projectCollabora_id != null) {
                            boxProjectRes= (boxResponseObject)JSON.deserialize(projectCollabora_id, boxResponseObject.class);
                           
                        }
                        
                         if(boxProjectRes.CollaborationId!=null || boxProjectRes.CollaborationId!='' ) {    
                            proObj.BoxCollaboration__c=boxProjectRes.CollaborationId;
                            //offerObj.BoxSharedLink__c =boxRes.folderShareLink;
                            updateMProject.add(proObj);
                        
                        }
                    }

                }
                 if(updateMProject.size() > 0 ) {
                    database.saveresult[] srlist = database.update(updateMProject, false);                    
                }

            }
        
        }catch (exception exp) {
        
        }

    }
    public class boxResponseObject {
        string folderId{get;set;}
        string folderShareLink{get;set;}  
        string CollaborationId{get;set;}      
       
    }     
}