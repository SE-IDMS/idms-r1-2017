//*********************************************************************************
//Apex class Name :  Utils_EmailNotificationMap
// Purpose : To maintain the levels of notification for EntityStakeholders.
// Created by : Ramesh Rajasekaran - Global Delivery Team
// Date created : 15th Sep 2011
// Modified by :
// Date Modified :
// Remarks : For July - 11 release 
///********************************************************************************/ 
public class Utils_EmailNotificationMap {

        public static Map<Integer, Map<String, List<RoleLevelWrapper>> > getEmailNotificationMap(){

                Map<Integer, Map<String, List<RoleLevelWrapper>> > OrgLevelAndscenarioRoleandOrgLevelMap = new Map<Integer, Map<String, List<RoleLevelWrapper>> >();            
                Map<String, List<RoleLevelWrapper>> scenarioRoleandOrgLevelMap=new Map<String, List<RoleLevelWrapper>>(); 
                Map<String, List<RoleLevelWrapper>> scenarioRoleandOrgLevelMapLevel2=new Map<String, List<RoleLevelWrapper>>();
                Map<String, List<RoleLevelWrapper>> scenarioRoleandOrgLevelMapLevel3=new Map<String, List<RoleLevelWrapper>>();
                
                // Custom Label for the corresponding Roles
                // CS&Q Manager - Label.CL10056 
                // Organization Manager - Label.CL10046 
                // I2P Process Advocate - Label.CL10039 
                // Local Safety Committee - Label.CL10040 
                // GSA CS&Q Manager - Label.CL10038 
                // Bre Resolution Leader - Label.CL10033
                
                //*********** ******* for orgLevel 1 *************************
                
                scenarioRoleandOrgLevelMap.put('SC1',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10039,1)});    
                
                scenarioRoleandOrgLevelMap.put('SC3',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10039,1)});    
                
                scenarioRoleandOrgLevelMap.put('SC5',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10040,1)});
                
                scenarioRoleandOrgLevelMap.put('SC36',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1)
                //,
                //new RoleLevelWrapper(Label.CL10033,1)
                });
                
                scenarioRoleandOrgLevelMap.put('SC36_1',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10033,1)
                });
                
                scenarioRoleandOrgLevelMap.put('SC7',
                new List<RoleLevelWrapper>{
                //new RoleLevelWrapper(Label.CL10056,1),
                //new RoleLevelWrapper(Label.CL10046,1)
                });

                scenarioRoleandOrgLevelMap.put('SC8',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10040,1)});
                
                scenarioRoleandOrgLevelMap.put('SC9',
                new List<RoleLevelWrapper>{new RoleLevelWrapper(Label.CL10038,1)});

                scenarioRoleandOrgLevelMap.put('SC12',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10038,1),
                new RoleLevelWrapper(Label.CL10040,3)});
                
                scenarioRoleandOrgLevelMap.put('SC14',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10038,1),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10033,1)});
                
                scenarioRoleandOrgLevelMap.put('SC15',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1)});
                
                scenarioRoleandOrgLevelMap.put('SC16',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10040,1)});
                
                scenarioRoleandOrgLevelMap.put('SC17',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10038,1),
                new RoleLevelWrapper(Label.CL10033,1)});
                
                scenarioRoleandOrgLevelMap.put('SC18',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),              
                new RoleLevelWrapper(Label.CL10039,1)});

                scenarioRoleandOrgLevelMap.put('SC19',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10040,1)});
                
                scenarioRoleandOrgLevelMap.put('SC20',
                new List<RoleLevelWrapper>{
                //new RoleLevelWrapper(Label.CL10038,1),
                new RoleLevelWrapper(Label.CL10033,1),
                new RoleLevelWrapper(Label.CL10038,1)});
                
                scenarioRoleandOrgLevelMap.put('SC30',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10039,1)});
                
                scenarioRoleandOrgLevelMap.put('SC32',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10039,1)});
                
                scenarioRoleandOrgLevelMap.put('SC34',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10040,1)});
                
                scenarioRoleandOrgLevelMap.put('SC22',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10040,1)});

                scenarioRoleandOrgLevelMap.put('SC23',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),              
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10039,3),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});                
                
                scenarioRoleandOrgLevelMap.put('SC25',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10040,1)});

                scenarioRoleandOrgLevelMap.put('SC27',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10040,1)});

                scenarioRoleandOrgLevelMap.put('SC29',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10040,1)});
                
                OrgLevelAndscenarioRoleandOrgLevelMap.put(1,scenarioRoleandOrgLevelMap);    
                
                // *************** For Level 2 ************************
                
                scenarioRoleandOrgLevelMapLevel2.put('SC1',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10039,2)});    
                
                scenarioRoleandOrgLevelMapLevel2.put('SC3',
                new List<RoleLevelWrapper>{
                //new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                //new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2)});    
                
                
                scenarioRoleandOrgLevelMapLevel2.put('SC5',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC6',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC36',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2)
                //,
                //new RoleLevelWrapper(Label.CL10033,2)
                });
                
                scenarioRoleandOrgLevelMap.put('SC36_1',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10033,2)
                });
                

                scenarioRoleandOrgLevelMapLevel2.put('SC7',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2)
                //new RoleLevelWrapper(Label.CL10046,2)
                });

                scenarioRoleandOrgLevelMapLevel2.put('SC8',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                //new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC9',
                new List<RoleLevelWrapper>{new RoleLevelWrapper(Label.CL10038,2)});

                scenarioRoleandOrgLevelMapLevel2.put('SC12',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10038,1),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC14',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10038,2),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10033,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC15',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC16',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC17',
                new List<RoleLevelWrapper>{new RoleLevelWrapper(Label.CL10038,2),
                new RoleLevelWrapper(Label.CL10033,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC18',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2),              
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2)});

                scenarioRoleandOrgLevelMapLevel2.put('SC19',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC20',
                new List<RoleLevelWrapper>{
                //new RoleLevelWrapper(Label.CL10038,2),
                new RoleLevelWrapper(Label.CL10033,2),
                new RoleLevelWrapper(Label.CL10038,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC30',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10039,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC32',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC34',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC22',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});

                scenarioRoleandOrgLevelMapLevel2.put('SC23',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),                          
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),              
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});                
                
                scenarioRoleandOrgLevelMapLevel2.put('SC25',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),                          
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),              
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});        

                scenarioRoleandOrgLevelMapLevel2.put('SC27',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),                          
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),              
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});        

                scenarioRoleandOrgLevelMapLevel2.put('SC29',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),                          
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),              
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2)});
                
                scenarioRoleandOrgLevelMapLevel2.put('SC37',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10046 ,2)});       
                
                OrgLevelAndscenarioRoleandOrgLevelMap.put(2,scenarioRoleandOrgLevelMapLevel2);  
                
                // *************** For Level 3 ************************
                
                scenarioRoleandOrgLevelMapLevel3.put('SC1',
                new List<RoleLevelWrapper>{             
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10039,3)});    
                
                scenarioRoleandOrgLevelMapLevel3.put('SC2',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC3',
                new List<RoleLevelWrapper>{
                //new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10039,3)});    
                
                scenarioRoleandOrgLevelMapLevel3.put('SC4',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC5',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10039,3),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC6',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC36',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3)
                //,
                //new RoleLevelWrapper(Label.CL10033,3)
                });
                
                scenarioRoleandOrgLevelMap.put('SC36_1',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10033,3)
                });
                
                scenarioRoleandOrgLevelMapLevel3.put('SC7',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),
                new RoleLevelWrapper(Label.CL10046,3)});

                scenarioRoleandOrgLevelMapLevel3.put('SC8',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC9',
                new List<RoleLevelWrapper>{new RoleLevelWrapper(Label.CL10038,3)});

                scenarioRoleandOrgLevelMapLevel3.put('SC12',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10039,3),
                new RoleLevelWrapper(Label.CL10038,1),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC14',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10038,3),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10039,3),
                new RoleLevelWrapper(Label.CL10033,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC15',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC16',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC17',
                new List<RoleLevelWrapper>{new RoleLevelWrapper(Label.CL10038,3),
                new RoleLevelWrapper(Label.CL10033,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC18',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,2),      
                new RoleLevelWrapper(Label.CL10056,3),                  
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10039,3)});

                scenarioRoleandOrgLevelMapLevel3.put('SC19',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC20',
                new List<RoleLevelWrapper>{
                //new RoleLevelWrapper(Label.CL10038,3),
                new RoleLevelWrapper(Label.CL10033,3),
                new RoleLevelWrapper(Label.CL10038,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC30',
                new List<RoleLevelWrapper>{             
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10039,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC31',
                new List<RoleLevelWrapper>{             
                new RoleLevelWrapper(Label.CL10056,1)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC32',
                new List<RoleLevelWrapper>{             
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10039,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC33',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC34',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10039,3),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});
                
                scenarioRoleandOrgLevelMapLevel3.put('SC22',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),
                new RoleLevelWrapper(Label.CL10056,3),
                new RoleLevelWrapper(Label.CL10046,1),
                new RoleLevelWrapper(Label.CL10046,2),
                new RoleLevelWrapper(Label.CL10046,3),
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),
                new RoleLevelWrapper(Label.CL10039,3),
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});

                scenarioRoleandOrgLevelMapLevel3.put('SC23',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),  
                new RoleLevelWrapper(Label.CL10056,3),  
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),      
                new RoleLevelWrapper(Label.CL10039,3),                  
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});                
                
                scenarioRoleandOrgLevelMapLevel3.put('SC25',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),  
                new RoleLevelWrapper(Label.CL10056,3),  
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),      
                new RoleLevelWrapper(Label.CL10039,3),                  
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});    

                scenarioRoleandOrgLevelMapLevel3.put('SC27',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),  
                new RoleLevelWrapper(Label.CL10056,3),  
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),      
                new RoleLevelWrapper(Label.CL10039,3),                  
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});    

                scenarioRoleandOrgLevelMapLevel3.put('SC29',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10056,1),
                new RoleLevelWrapper(Label.CL10056,2),  
                new RoleLevelWrapper(Label.CL10056,3),  
                new RoleLevelWrapper(Label.CL10039,1),
                new RoleLevelWrapper(Label.CL10039,2),      
                new RoleLevelWrapper(Label.CL10039,3),                  
                new RoleLevelWrapper(Label.CL10040,1),
                new RoleLevelWrapper(Label.CL10040,2),
                new RoleLevelWrapper(Label.CL10040,3)});    
                
                scenarioRoleandOrgLevelMapLevel3.put('SC37',
                new List<RoleLevelWrapper>{
                new RoleLevelWrapper(Label.CL10046 ,2)});
                
                OrgLevelAndscenarioRoleandOrgLevelMap.put(3,scenarioRoleandOrgLevelMapLevel3);          
                    
                
                return OrgLevelAndscenarioRoleandOrgLevelMap;

        }



        public class RoleLevelWrapper{
            public String RoleName;
            public Integer RoleLevel;       

            public RoleLevelWrapper(String RoleName,Integer Level){
                this.RoleName=RoleName;
                this.RoleLevel=Level;           
            }
        }


}