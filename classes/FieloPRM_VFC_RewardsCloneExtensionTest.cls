/***********************************************************
    Author: Fielo Team
************************************************************/
@isTest

public class FieloPRM_VFC_RewardsCloneExtensionTest{

    public static testMethod void test1(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){
            
            FieloEE.MockUpFactory.setCustomProperties(false);
           
            FieloEE__Program__c program = new FieloEE__Program__c(
                Name = 'PRM Is On',
                FieloEE__SiteURL__c = 'http://www.google.com',
                FieloEE__RecentRewardsDays__c = 18
            );
            insert program;

            Country__c testCountryGlobal = new Country__c(
                Name   = 'Global Country Test',
                CountryCode__c = 'WW',
                InternationalPhoneCode__c = '00',
                Region__c = 'TESTG'
            );
            insert testCountryGlobal;

            Country__c testCountryLocal = new Country__c(
                Name   = 'Local Country Test',
                CountryCode__c = 'US',
                InternationalPhoneCode__c = '01',
                Region__c = 'TESTL'
            );
            insert testCountryLocal;
                        
            PRMCountry__c prmCountryGlobal = new PRMCountry__c(
                Name = 'Global Country Global Test',
                CountryPortalEnabled__c = true,
                Country__c = testCountryGlobal.Id,
                PLDatapool__c = 'pagecontrol1',
                TECH_MigrationUniqueID__c = 'TestGlobal'
            );
            insert prmCountryGlobal;

            PRMCountry__c prmCountryLocal = new PRMCountry__c(
                Name = 'Local Country Local Test',
                CountryPortalEnabled__c = true,
                Country__c = testCountryLocal.Id,
                PLDatapool__c = 'pagecontrol2',
                TECH_MigrationUniqueID__c = 'TestLocal'
            );
            insert prmCountryLocal;
            
            prmCountryLocal = [SELECT Id, F_PRM_RewardsMenu__c, CountryCode__c FROM PRMCountry__c WHERE Id =: prmCountryLocal.Id];
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Test Template Parent Menu',
                FieloEE__Placement__c = 'Template',
                F_PRM_Type__c = 'Template',
                FieloEE__Program__c = program.id,
                F_PRM_MenuFilter__c = 'P-WW-TestParentMenu',
                FieloEE__ExternalName__c = 'P_WW_TestParentMenu',
                F_PRM_Scope__c = 'Global',
                Name = 'TMPL-P-XX-Rewards Parent Menu',
                F_PRM_Country__c = prmCountryGlobal.Id
            );
            insert parentMenu;
            
            FieloEE__Menu__c childMenu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Test Template Child Menu',
                FieloEE__Placement__c = 'Template',
                F_PRM_Type__c = 'Template',
                FieloEE__Program__c = program.id,
                F_PRM_MenuFilter__c = 'P-WW-TestParentMenu',
                FieloEE__ExternalName__c = 'P_WW_TestChildMenu',
                F_PRM_Scope__c = 'Global',
                Name = 'TMPL-P-XX-Rewards Child Menu',
                F_PRM_Country__c = prmCountryGlobal.Id,
                FieloEE__Menu__c = parentMenu.Id
            );
            insert childMenu;

            RecordType rtMenu = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'Menu'];
            RecordType rtCategories = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'Categories'];
            RecordType rtCatalog = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'Catalog'];

            List<FieloEE__Component__c> listComp = new List<FieloEE__Component__c>();

            FieloEE__Component__c componentMenuParent = new FieloEE__Component__c(
                FieloEE__Menu__c = parentMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentMenuParent',
                RecordTypeId = rtMenu.Id
            );
            listComp.add(componentMenuParent);

            FieloEE__Component__c componentMenuChild = new FieloEE__Component__c(
                FieloEE__Menu__c = childMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentMenuChild',
                RecordTypeId = rtMenu.Id
            );
            listComp.add(componentMenuChild);

            FieloEE__Category__c newCat = new FieloEE__Category__c(
                Name = 'XX - Test Category',
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_CategoryFilter__c = 'P-WW-TestCategory',
                FieloEE__Order__c = 1
            );
            insert newCat;

            FieloEE__Component__c componentCategoryParent = new FieloEE__Component__c(
                FieloEE__Menu__c = parentMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentCategoryParent',
                RecordTypeId = rtCategories.Id,
                FieloEE__Category__c = newCat.Id
            );
            listComp.add(componentCategoryParent);
            
            FieloEE__Component__c componentCategoryChild = new FieloEE__Component__c(
                FieloEE__Menu__c = childMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentCategoryChild',
                RecordTypeId = rtCategories.Id,
                FieloEE__Category__c = newCat.Id
            );
            listComp.add(componentCategoryChild);

            FieloEE__Tag__c newTag = new FieloEE__Tag__c(
                Name = 'XX - Test Tag',
                F_PRM_TagFilter__c = 'P-WW-TestTag',
                FieloEE__Order__c = 1
            );
            insert newTag;

            FieloEE__Component__c componentTagParent = new FieloEE__Component__c(
                FieloEE__Menu__c = parentMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentTagParent',
                RecordTypeId = rtCatalog.Id,
                FieloEE__Tag__c = newTag.Id
            );
            listComp.add(componentTagParent);
            
            FieloEE__Component__c componentTagChild = new FieloEE__Component__c(
                FieloEE__Menu__c = childMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentTagChild',
                RecordTypeId = rtCatalog.Id,
                FieloEE__Tag__c = newTag.Id
            );
            listComp.add(componentTagChild);
            
            insert listComp;
                                             
            test.startTest();

            ApexPages.StandardController stdController = new ApexPages.StandardController(prmCountryLocal);
            FieloPRM_VFC_RewardsCloneExtension ext = new FieloPRM_VFC_RewardsCloneExtension(stdController);
            ext.selectedMenu = parentMenu.Id;
            ext.nextStep();
            ext.redirect();
           
            test.stopTest();
        }
    }

    public static testMethod void test2(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){
            
            FieloEE.MockUpFactory.setCustomProperties(false);
           
            FieloEE__Program__c program = new FieloEE__Program__c(
                Name = 'PRM Is On',
                FieloEE__SiteURL__c = 'http://www.google.com',
                FieloEE__RecentRewardsDays__c = 18
            );
            insert program;

            Country__c testCountryGlobal = new Country__c(
                Name   = 'Global Country Test',
                CountryCode__c = 'WW',
                InternationalPhoneCode__c = '00',
                Region__c = 'TESTG'
            );
            insert testCountryGlobal;

            Country__c testCountryLocal = new Country__c(
                Name   = 'Local Country Test',
                CountryCode__c = 'US',
                InternationalPhoneCode__c = '01',
                Region__c = 'TESTL'
            );
            insert testCountryLocal;
                        
            PRMCountry__c prmCountryGlobal = new PRMCountry__c(
                Name = 'Global Country Global Test',
                CountryPortalEnabled__c = true,
                Country__c = testCountryGlobal.Id,
                PLDatapool__c = 'pagecontrol1',
                TECH_MigrationUniqueID__c = 'TestGlobal'
            );
            insert prmCountryGlobal;

            PRMCountry__c prmCountryLocal = new PRMCountry__c(
                Name = 'Local Country Local Test',
                CountryPortalEnabled__c = true,
                Country__c = testCountryLocal.Id,
                PLDatapool__c = 'pagecontrol2',
                TECH_MigrationUniqueID__c = 'TestLocal'
            );
            insert prmCountryLocal;
            
            prmCountryLocal = [SELECT Id, F_PRM_RewardsMenu__c, CountryCode__c FROM PRMCountry__c WHERE Id =: prmCountryLocal.Id];
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Test Template Parent Menu',
                FieloEE__Placement__c = 'Template',
                F_PRM_Type__c = 'Template',
                FieloEE__Program__c = program.id,
                F_PRM_MenuFilter__c = 'P-WW-TestParentMenu',
                FieloEE__ExternalName__c = 'P_WW_TestParentMenu1',
                F_PRM_Scope__c = 'Global',
                Name = 'TMPL-P-XX-Rewards Parent Menu',
                F_PRM_Country__c = prmCountryGlobal.Id
            );
            insert parentMenu;
            
            FieloEE__Menu__c childMenu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Test Template Child Menu',
                FieloEE__Placement__c = 'Template',
                F_PRM_Type__c = 'Template',
                FieloEE__Program__c = program.id,
                F_PRM_MenuFilter__c = 'P-WW-TestParentMenu',
                FieloEE__ExternalName__c = 'P_WW_TestChildMenu1',
                F_PRM_Scope__c = 'Global',
                Name = 'TMPL-P-XX-Rewards Child Menu',
                F_PRM_Country__c = prmCountryGlobal.Id,
                FieloEE__Menu__c = parentMenu.Id
            );
            insert childMenu;

            RecordType rtMenu = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'Menu'];
            RecordType rtCategories = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'Categories'];
            RecordType rtCatalog = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'Catalog'];

            List<FieloEE__Component__c> listComp = new List<FieloEE__Component__c>();

            FieloEE__Component__c componentMenuParent = new FieloEE__Component__c(
                FieloEE__Menu__c = parentMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentMenuParent',
                RecordTypeId = rtMenu.Id
            );
            listComp.add(componentMenuParent);

            FieloEE__Component__c componentMenuChild = new FieloEE__Component__c(
                FieloEE__Menu__c = childMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentMenuChild',
                RecordTypeId = rtMenu.Id
            );
            listComp.add(componentMenuChild);
           
            insert listComp;
            
            FieloEE__Banner__c banner = new FieloEE__Banner__c(
                Name = 'testBanner', 
                FieloEE__Component__c = componentMenuChild.Id,
                F_PRM_ContentFilter__c = 'P-WW-Banner',
                F_PRM_Country__c = prmCountryGlobal.Id,
                FieloEE__Program__c = program.id
            );
            insert banner;            
            
            FieloEE__News__c contentFeed = new FieloEE__News__c(
                FieloEE__Component__c = componentMenuChild.Id,
                F_PRM_ContentFilter__c = 'P-WW-ContentFeed',
                F_PRM_Country__c = prmCountryGlobal.Id,
                FieloEE__Program__c = program.id
            );
            insert contentFeed; 
                                             
            test.startTest();

            ApexPages.StandardController stdController = new ApexPages.StandardController(prmCountryLocal);
            FieloPRM_VFC_RewardsCloneExtension ext = new FieloPRM_VFC_RewardsCloneExtension(stdController);
            ext.selectedMenu = parentMenu.Id;
            ext.nextStep();
            ext.redirect();
           
            test.stopTest();
        }
    }

    public static testMethod void test3(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        System.RunAs(us){
            
            FieloEE.MockUpFactory.setCustomProperties(false);
           
            FieloEE__Program__c program = new FieloEE__Program__c(
                Name = 'PRM Is On',
                FieloEE__SiteURL__c = 'http://www.google.com',
                FieloEE__RecentRewardsDays__c = 18
            );
            insert program;

            Country__c testCountryGlobal = new Country__c(
                Name   = 'Global Country Test',
                CountryCode__c = 'WW',
                InternationalPhoneCode__c = '00',
                Region__c = 'TESTG'
            );
            insert testCountryGlobal;

            Country__c testCountryLocal = new Country__c(
                Name   = 'Local Country Test',
                CountryCode__c = 'US',
                InternationalPhoneCode__c = '01',
                Region__c = 'TESTL'
            );
            insert testCountryLocal;
                        
            PRMCountry__c prmCountryGlobal = new PRMCountry__c(
                Name = 'Global Country Global Test',
                CountryPortalEnabled__c = true,
                Country__c = testCountryGlobal.Id,
                PLDatapool__c = 'pagecontrol1',
                TECH_MigrationUniqueID__c = 'TestGlobal'
            );
            insert prmCountryGlobal;

            PRMCountry__c prmCountryLocal = new PRMCountry__c(
                Name = 'Local Country Local Test',
                CountryPortalEnabled__c = true,
                Country__c = testCountryLocal.Id,
                PLDatapool__c = 'pagecontrol2',
                TECH_MigrationUniqueID__c = 'TestLocal'
            );
            insert prmCountryLocal;
            
            prmCountryLocal = [SELECT Id, F_PRM_RewardsMenu__c, CountryCode__c FROM PRMCountry__c WHERE Id =: prmCountryLocal.Id];
            
            FieloEE__Menu__c parentMenu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Test Template Parent Menu',
                FieloEE__Placement__c = 'Template',
                F_PRM_Type__c = 'Template',
                FieloEE__Program__c = program.id,
                F_PRM_MenuFilter__c = 'P-WW-TestParentMenu',
                FieloEE__ExternalName__c = 'P_WW_TestParentMenu2',
                F_PRM_Scope__c = 'Global',
                Name = 'TMPL-P-XX-Rewards Parent Menu',
                F_PRM_Country__c = prmCountryGlobal.Id
            );
            insert parentMenu;
            
            FieloEE__Menu__c childMenu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Test Template Child Menu',
                FieloEE__Placement__c = 'Template',
                F_PRM_Type__c = 'Template',
                FieloEE__Program__c = program.id,
                F_PRM_MenuFilter__c = 'P-WW-TestParentMenu',
                FieloEE__ExternalName__c = 'P_WW_TestChildMenu2',
                F_PRM_Scope__c = 'Global',
                Name = 'TMPL-P-XX-Rewards Child Menu',
                F_PRM_Country__c = prmCountryGlobal.Id,
                FieloEE__Menu__c = parentMenu.Id
            );
            insert childMenu;

            RecordType rtMenu = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'Menu'];
            RecordType rtCategories = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'Categories'];
            RecordType rtCatalog = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'Catalog'];
            RecordType rtListView = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'ListView'];
            RecordType rtCRUD = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloEE__Component__c' AND DeveloperName = 'CRUD'];
            RecordType rtpageLayoutForm = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloCRUD__PageLayout__c' AND DeveloperName = 'Form'];
            RecordType rtListViewLV = [SELECT Id FROM RecordType WHERE sObjectType = 'FieloCRUD__ListView__c' AND DeveloperName = 'ListView'];

            FieloCRUD__ObjectSettings__c objectSetting = new FieloCRUD__ObjectSettings__c(
                FieloCRUD__ObjectAPIName__c = 'FieloPRM_Invoice__c',
                Name = 'PRM Invoice',
                FieloCRUD__FieldMember__c = 'F_PRM_Member__c',
                FieloCRUD__FieldToDefinePageLayout__c = 'Name'
            );
            insert objectSetting;

            FieloCRUD__ListView__c listView1 = new FieloCRUD__ListView__c(
                Name = 'XX - Test',
                FieloCRUD__ObjectSettings__c = objectSetting.Id,
                F_PRM_Scope__c = 'Global',
                RecordTypeId = rtListViewLV.Id
            );
            insert listView1;

            FieloCRUD__ListView__c listView2 = new FieloCRUD__ListView__c(
                Name = 'XX - Test',
                FieloCRUD__ObjectSettings__c = objectSetting.Id,
                F_PRM_Scope__c = 'Local',
                RecordTypeId = rtListViewLV.Id
            );
            insert listView2;

            FieloCRUD__PageLayout__c pageLayout = new FieloCRUD__PageLayout__c(
                FieloCRUD__ObjectSettings__c = objectSetting.Id,
                Name = 'XX - Name',
                FieloCRUD__ValueToDefinePageLayout__c = 'XX',
                RecordTypeId = rtpageLayoutForm.Id
            );
            insert pageLayout;

            List<FieloEE__Component__c> listComp = new List<FieloEE__Component__c>();

            FieloEE__Component__c componentCRUD = new FieloEE__Component__c(
                FieloEE__Menu__c = parentMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentCRUD',
                RecordTypeId = rtCRUD.Id,
                FieloCRUD__PageLayout__c = pageLayout.Id,
                FieloCRUD__ObjectSettings__c = objectSetting.Id
            );
            listComp.add(componentCRUD);

            FieloEE__Component__c componentListView1 = new FieloEE__Component__c(
                FieloEE__Menu__c = childMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentLisView',
                RecordTypeId = rtListView.Id,
                FieloCRUD__ListView__c = listView1.Id
            );
            listComp.add(componentListView1);

            FieloEE__Component__c componentListView2 = new FieloEE__Component__c(
                FieloEE__Menu__c = parentMenu.id,
                F_PRM_Country__c = prmCountryGlobal.Id,
                F_PRM_ComponentFilter__c = 'P-WW-TestComponentLisView',
                RecordTypeId = rtListView.Id,
                FieloCRUD__ListView__c = listView2.Id
            );
            listComp.add(componentListView2);
            
            insert listComp;
                                             
            test.startTest();

            ApexPages.StandardController stdController = new ApexPages.StandardController(prmCountryLocal);
            FieloPRM_VFC_RewardsCloneExtension ext = new FieloPRM_VFC_RewardsCloneExtension(stdController);
            ext.selectedMenu = parentMenu.Id;
            ext.nextStep();
            ext.redirect();
           
            test.stopTest();
        }
    }

 
}