@isTest
private class VFC_ManageNotificationCentre_TEST{

     static testMethod void testNotification() {
     
         Id profilesId = [select id from profile where name='SE - Channel Partner (Community)'].Id;
           
            Country__c Country = new Country__c (Name='TCY', CountryCode__c='TCY'); 
            insert country;
            String countryid = country.id;
            System.debug('****** Country ID:'+country.id);

            Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345');
            insert PartnerAcc;
            
            Contact PartnerCon = new Contact(
                    FirstName='Test',
                    LastName='lastname',
                    AccountId=PartnerAcc.Id,
                    JobTitle__c='Z3',
                    CorrespLang__c='EN', 
                    Country__c = Country.id,    
                    WorkPhone__c='1234567890'
                    );
                    System.debug('****** Country ID:'+countryid);
                    PartnerCon.Country__c = countryid ; 
                    
                    Insert PartnerCon;
            
           //PartnerProgram__c prog = new PartnerProgram__c();
           //prog.name = 'Test Prog';   
           //insert prog;
   
   
            PermissionSet orfConvPermissionSet1 = [Select ID,UserLicenseId from PermissionSet where Name='PRMRegularORF' Limit 1];
      
            User   u= new User(Username = 'testUserOne@schneider-electric.com', LastName = 'User11', alias = 'tuser1',
                    CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                    Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US' ,IsActive =true, ProfileID = profilesId, ContactID = PartnerCon.Id, UserPermissionsSFContentUser=true );
            insert u; 

     
        
        // 
        // User u = [select Id,Contact.Id from User WHERE Profile.Name='SE - Channel Partner (Community)' AND IsActive = TRUE LIMIT 1];
         RecordType recordType1 = [Select Id, developerName from RecordType WHERE developerName = 'UserNotification'];
         //Country__c c = Utils_TestMethods.createCountry();
         //insert c;
         Country__c c = Country;
         
         PRMCountry__c cluster = new PRMCountry__c();
         cluster.Name = 'Test Cluster';
         cluster.Country__c = c.Id;
         cluster.PLDatapool__c = 'test';
         cluster.CountrySupportEmail__c = 'test@abc.com';
         cluster.DefaultLandingPage__c = '/test';
         insert cluster;
        
         SetupAlerts__c Notification1 = new SetupAlerts__c();
         Notification1.Name = 'Test Notification - 01';
         Notification1.Content__c = 'This is a test alert';
         Notification1.PRMCountryClusterSetup__c = cluster.Id;
         Notification1.Status__c= 'Published';
         Notification1.RecordTypeId = recordType1.Id;
         Notification1.Contact__c = u.Contact.Id;
         insert Notification1;
         
         TrackSetupAlets__c trackAlert = new TrackSetupAlets__c();
         trackAlert.Deleted__c = false;
         trackAlert.Read__c = false;
         trackAlert.SetupAlerts__c = Notification1.Id;
         trackAlert.User__c = u.Id;
         insert trackAlert;
         
         System.runAs(u){
             VFC_ManageNotificationCentre.searchNotificationAlerts();
             VFC_ManageNotificationCentre.modifyNotification('read',trackAlert.Id);
         }
    }
}