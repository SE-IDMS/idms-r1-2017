/*
    Author          : Accenture Team
    Date Created    : 18/07/2011 
    Description     : Controller extensions for VFP41_LogACallRedirect & VFP42_SchACallRedirect.This class creates a completed task
                      for a Log a call & redirects the user to Task Edit page with the appropriate fields prepopulated
                      for a Schedule a callback
   12-June-2012     Srinivas Nallapati  June Mkt 2012 release       BR-1771 A lead qualifier should have the ability to send an email to the prospect from the lead page. 
                                                                     This will create a specific activity on the lead.                   
   29-Oct-2012      Srinivas Nallapati  Dec Mkt 2012 release        BT-2272 : prevent user from Logging a Call & Schedule a Callback for Closed Leads
   
*/
public class VFC41_CallRedirect
{
    //Iniatialize variables
    public Lead lead{get;private set;}
    public String errorMsg{get;set;}    
    private pagereference pg;
    private boolean validateMandFields = false;
    private boolean validatePriority= false;

    //Constructor
    public VFC41_CallRedirect(ApexPages.StandardController controller) 
    {
        lead= (Lead)controller.getRecord();
        lead = [select Status, Callbacklimit__c,FirstName,CallAttemptsRemaining__c, TECH_IsActivityAsc__c,CallAttempts__c,Recordtypeid  from lead where Id=:lead.Id limit 1];
        

        validateMandFields = validateMandatoryFields();
        validatePriority= validatePriority();
        
        if(validateMandFields)
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00581));  
        if(validatePriority)
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00509));
                                             
    }
    
    /* This method is called on the load of VFP41_LogACallRedirect page. This method validates the Lead's Priority.
       If the Lead's priority is valid, creates a new Task and redirects the user to the Lead Detail page.
       Else displays an error message.
     */  
    
    public pagereference logACallRedirect()
    {
        System.Debug('****** Start of Log a call Redirect Method ******');           
        List<task> tasks = new List<task>();
        Boolean errDuringIns = false;
                
        //BR-2272
        if(lead.Status != Label.CL00554 && lead.Status != Label.CL00447 && lead.Recordtypeid != System.Label.CLJUL14MKT01)
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLDEC12MKT08)); //End BR2272   
        //Srinivas - MKT May13 Change
        else if(!( VFC_ConvertLead.hasEditOnLead(Userinfo.getUserId(), lead.Id)) )
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLMAY13MKT006));
        }
        //ENd of May13 
        else if(validateMandFields == false && validatePriority==false)
        {
                if(lead!=null)
                {
                    Task task = new Task();
                    task.whoId = lead.Id;
                    task.status=Label.CL00423;
                    task.subject = Label.CL00545;
                    task.ActivityDate = Date.Today();
                    //Change for BR-1771
                    task.TECH_TaskType__c = Label.CLSEP12MKT01; 
                    //End of change for Br-1771
                    tasks.add(task);
                    
                    errDuringIns = insertTasks(tasks);
        
                    if(errDuringIns==false)
                        if(Site.getPrefix() == '/partners')
                            pg= new pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/partners/'+lead.Id);
                        else if(Site.getPrefix() == '/pomp')
                            pg= new pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/pomp/'+lead.Id);
                        else if(Site.getPrefix() == '/agents')
                            pg= new pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/agents/'+lead.Id);   
                        else
                            pg= new pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+lead.Id);                                            
                }    
        }
        System.Debug('****** End of Log a call Redirect Method ******');   
        if(pg!=null)
            return pg;
        else
            return null;
    }
    /* This method is called on the load of VFC42_SchACallRedirect page. This method redirects the user to the 
     * New Task page with the Task Status, Lead Name, Subjectfields pre-populated.
     */  
    public pagereference schACallRedirect()
    { 
        System.Debug('****** Start of Schedule a call Redirect Method ******');
        //Change for BR-1771
        Integer CallTaskCount = [select count() from Task where (TECH_TaskType__c = :Label.CLSEP12MKT01 or  TECH_TaskType__c = null)and WhoId =:lead.id];      
        
        if(!( VFC_ConvertLead.hasEditOnLead(Userinfo.getUserId(), lead.Id)) )
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLMAY13MKT007));
        }
        //ENd of May13          
        //if(!lead.TECH_ISActivityAsc__c)
        else if(CallTaskCount == 0) 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00533)); 
        //BR-2272
        else if(lead.Status != Label.CL00554 && lead.Status != Label.CL00447 && lead.Recordtypeid != System.Label.CLJUL14MKT01)
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLDEC12MKT09)); //End BR2272
        //Srinivas - MKT May13 Change
        else if(validateMandFields==false && (CallTaskCount > 0) && validatePriority == false)//End of change for Br-1771
        {
                if(lead!=null)
                {           
                    if(Site.getPrefix() == '/partners')
                            pg= new pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/partners/'+SObjectType.Task.keyPrefix
                               +'/e?who_id='+lead.Id+'&tsk5='+Label.CL00545+'&retURL=%2F'+Lead.Id);
                    else if(Site.getPrefix() == '/pomp')
                            pg= new pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/pomp/'+SObjectType.Task.keyPrefix
                               +'/e?who_id='+lead.Id+'&tsk5='+Label.CL00545+'&retURL=%2F'+Lead.Id);
                    else if(Site.getPrefix() == '/agents')
                            pg= new pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/agents/'+SObjectType.Task.keyPrefix
                               +'/e?who_id='+lead.Id+'&tsk5='+Label.CL00545+'&retURL=%2F'+Lead.Id);
                    else
                      pg= new pagereference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+SObjectType.Task.keyPrefix
                               +'/e?who_id='+lead.Id+'&tsk5='+Label.CL00545+'&retURL=%2F'+Lead.Id);
                }
        }
        System.Debug('****** End of Schedule a call Redirect Method ******');        
        if(pg!=null)
            return pg;
        else
            return null;
    }
    
    /* This method checks if the Callback limit is null and returns the boolean value accordingly. 
       If null, displays an error message */
    
    public boolean validatePriority()
    {
        if(lead.CallBackLimit__c==null)
        {
            errorMsg = Label.CL00509;
            return true;
        }
        else
            return false;           
    }
    
    
    /*  This method is called from logACallRedirect method of VFC41_CallRedirect 
     *  This method inserts the tasks and returns if any error occurred during insert.
     */
    public boolean insertTasks(List<Task> tasks)
    {
        boolean isError = false;
        if(tasks.Size() > 0)
        {
            if(tasks.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## VFC41 Error Inserting : '+ Label.CL00264);
            }
            else
            {
                Database.SaveResult[] tasksUpdate= Database.insert(tasks, false);
                for(Database.SaveResult sr: tasksUpdate)
                {
                    system.debug(sr.isSuccess());
                    if(!sr.isSuccess())
                    {
                        isError = true;
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00398));  
                        System.Debug('######## VFC41 Error Inserting: '+sr.getErrors()[0].getStatusCode()+' '+sr.getErrors()[0].getMessage());
                    }    
                }
            }                         
        }   
        return isError;
    }
    
    public boolean validateMandatoryFields()
    {
        if(lead.FirstName == null)
        {
           return true;
        }
        else
            return false;
    }
}