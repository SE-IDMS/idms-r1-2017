global class Batch_OpportunityLineDataMigration implements Database.Batchable<sObject>{
    global Integer intLimitNumber;
    global List<String> tobeProcessedStageNames=new List<String>();

    global Batch_OpportunityLineDataMigration(){}
    global Batch_OpportunityLineDataMigration(List<String> lstStageNames,Integer intLmtNumber){
        tobeProcessedStageNames=lstStageNames;
        if(intLmtNumber==0 || intLmtNumber==null)
            intLimitNumber=2000000;
        intLimitNumber=intLmtNumber;
    }


    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id,ProductBU__c,ProductLine__c,ProductFamily__c,Family__c,Product__c,TECH_CommercialReference__c,TECH_2014ReferentialUpdated__c FROM OPP_ProductLine__c where Opportunity__r.StageName IN :tobeProcessedStageNames and TECH_2014ReferentialUpdated__c=false Order By CreatedDate DESC LIMIT :intLimitNumber]);
    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){      
        Map<Id,OPP_ProductLine__c> mapOpportunityLine =new Map<Id,OPP_ProductLine__c>();    
        for(sObject objScope : scope){
            OPP_ProductLine__c objOpportunityLine =(OPP_ProductLine__c)objScope;   
            mapOpportunityLine.put(objOpportunityLine.id,objOpportunityLine);
        }
        updateReferntialDataOnOpportunity(mapOpportunityLine);
    }

    global void updateReferntialDataOnOpportunity(Map<Id,OPP_ProductLine__c> mapOpportunityLine){
        List<OPP_ProductLine__c> lstOpportunityLinesToBeUpdate =new List<OPP_ProductLine__c>();
        Map<Id,OPP_ProductLine__c>mapOppLinesWithCR = new Map<Id,OPP_ProductLine__c>();
        Map<Id,OPP_ProductLine__c>mapOppLinesWithProductRef = new Map<Id,OPP_ProductLine__c>();
        Map<Id,OPP_ProductLine__c>mapOppLinesWithOutRef = new Map<Id,OPP_ProductLine__c>();
        Map<Id,String>mapOppLinesUniqueRef = new Map<Id,String>();
        String strErrorId='';
        String strErrorMessage='';
        String strErrorType='';
        String strErrorStackTrace='';
        
        Set<String> CommRefSet = new Set<String>();
        Map<String,TECH_CommRefAllLevel__c> mapCommRefAllLevel = new Map<String,TECH_CommRefAllLevel__c>();
        Map<Id,TECH_GMRMapping__c> mapGMRMapping = new Map<Id,TECH_GMRMapping__c>();
        Set<Id> OldProductIdSet = new Set<Id>();
        Set<Id> UniqueAllLevelsSet = new Set<Id>();
        String strMapKey;
        for(OPP_ProductLine__c ObjOppLine: mapOpportunityLine.values()){
            if(ObjOppLine.TECH_CommercialReference__c !=null && ObjOppLine.TECH_CommercialReference__c.trim() !=''){
            // Reference to Commercial Reference Table
            mapOppLinesWithCR.put(ObjOppLine.Id,ObjOppLine);
                CommRefSet.add(ObjOppLine.TECH_CommercialReference__c);
            }
            /*
            else if(ObjOppLine.Product__c !=null ){
                // Reference to Opp Product Table
                mapOppLinesWithProductRef.put(ObjOppLine.Id,ObjOppLine);
                OldProductIdSet.add(ObjOppLine.Product__c);  
            }
            else{
                if(ObjOppLine.Product__c == null && (ObjOppLine.TECH_CommercialReference__c == null || ObjOppLine.TECH_CommercialReference__c =='') && (ObjOppLine.ProductBU__c !=null && ObjOppLine.ProductBU__c.trim() !='')){
                //Data Migration Issue
                    if(ObjOppLine.ProductBU__c.trim() !=''){
                        strMapKey = ObjOppLine.ProductBU__c.toUpperCase();
                    }
                    if(ObjOppLine.ProductLine__c.trim() !=''){
                        strMapKey += ',' + ObjOppLine.ProductLine__c.toUpperCase();
                    }
                    if(ObjOppLine.ProductFamily__c.trim() !=''){
                        strMapKey += ',' + ObjOppLine.ProductFamily__c.toUpperCase();
                    }
                    if(ObjOppLine.Family__c.trim() !=''){
                        strMapKey += ',' + ObjOppLine.Family__c.toUpperCase();
                    }
                    UniqueAllLevelsSet.add(strMapKey);
                    mapOppLinesUniqueRef.put(ObjOppLine.Id,strMapKey);
                    mapOppLinesWithOutRef.put(ObjOppLine.Id,ObjOppLine);
                }
            }
            */
        }
        if(mapOppLinesWithCR.size()>0){
            List<TECH_CommRefAllLevel__c> lstCommRefAllLevel = new List<TECH_CommRefAllLevel__c>([Select Id, Name, BusinessUnit__c, ProductLine__c, ProductFamily__c, Family__c, SubFamily__c, ProductSuccession__c, ProductGroup__c, NewGMRCode__c from TECH_CommRefAllLevel__c where Name IN:CommRefSet]);
            if(lstCommRefAllLevel.size()>0){
                for(TECH_CommRefAllLevel__c objCommRef: lstCommRefAllLevel ){
                    mapCommRefAllLevel.put(objCommRef.Name, objCommRef);
                }
            }
            if(mapCommRefAllLevel.size()>0){
                for(OPP_ProductLine__c ObjOppLine: mapOppLinesWithCR.values()){
                    if(mapCommRefAllLevel.containsKey(ObjOppLine.TECH_CommercialReference__c)){
                        ObjOppLine.ProductBU__c = mapCommRefAllLevel.get(ObjOppLine.TECH_CommercialReference__c).BusinessUnit__c;
                        ObjOppLine.ProductLine__c = mapCommRefAllLevel.get(ObjOppLine.TECH_CommercialReference__c).ProductLine__c;
                        ObjOppLine.ProductFamily__c = mapCommRefAllLevel.get(ObjOppLine.TECH_CommercialReference__c).ProductFamily__c;
                        ObjOppLine.Family__c = mapCommRefAllLevel.get(ObjOppLine.TECH_CommercialReference__c).Family__c;
                        ObjOppLine.TECH_2014ReferentialUpdated__c=true;
                        lstOpportunityLinesToBeUpdate.add(ObjOppLine);
                    }
                }
            }
        }
        /*
        if(mapOppLinesWithProductRef.size()>0){
            List<TECH_GMRMapping__c> lstGMRMapping = new List<TECH_GMRMapping__c>([Select Id, Name, OldProductID__c, NewBusinessUnit__c, NewProductLine__c, NewProductFamily__c, NewFamily__c, NewGMRCode__c from TECH_GMRMapping__c where OldProductID__c IN:OldProductIdSet ]);
            if(lstGMRMapping.size()>0){
                for(TECH_GMRMapping__c objGMRMapping: lstGMRMapping){
                    mapGMRMapping.put(objGMRMapping.OldProductID__c,objGMRMapping);
                }             
            }
            if(mapGMRMapping.size()>0){
                for(OPP_ProductLine__c ObjOppLine: mapOppLinesWithProductRef.values()){
                    if(mapGMRMapping.containsKey(ObjOppLine.Product__c)){
                        ObjOppLine.ProductBU__c = mapGMRMapping.get(ObjOppLine.Product__c).NewBusinessUnit__c;
                        ObjOppLine.ProductLine__c = mapGMRMapping.get(ObjOppLine.Product__c).NewProductLine__c;
                        ObjOppLine.ProductFamily__c = mapGMRMapping.get(ObjOppLine.Product__c).NewProductFamily__c;
                        ObjOppLine.Family__c = mapGMRMapping.get(ObjOppLine.Product__c).NewFamily__c;
                        ObjOppLine.TECH_2014ReferentialUpdated__c=true;
                        lstOpportunityLinesToBeUpdate.add(ObjOppLine);                      
                    }
                }
            }
        }
        
        if(mapOppLinesWithOutRef.size()>0){
            List<TECH_GMRMapping__c> lstGMRMapping = new List<TECH_GMRMapping__c>([Select Id, Name, OldProductID__c, NewBusinessUnit__c, NewProductLine__c, NewProductFamily__c, NewFamily__c, NewGMRCode__c from TECH_GMRMapping__c where  UniqueAllLevels__c IN:UniqueAllLevelsSet ]);
            mapGMRMapping=null;
            mapGMRMapping = new Map<Id,TECH_GMRMapping__c>();
            if(lstGMRMapping.size()>0){
                for(TECH_GMRMapping__c objGMRMapping: lstGMRMapping){
                    mapGMRMapping.put(objGMRMapping.UniqueAllLevels__c,objGMRMapping);
                }             
            }
            if(mapGMRMapping.size()>0){
                for(OPP_ProductLine__c ObjOppLine: mapOppLinesWithOutRef.values()){
                    if(mapOppLinesUniqueRef.containsKey(ObjOppLine.Id)){
                        if(mapGMRMapping.containsKey(mapOppLinesUniqueRef.get(ObjOppLine.Id))){
                            ObjOppLine.ProductBU__c = mapGMRMapping.get(mapOppLinesUniqueRef.get(ObjOppLine.Id)).NewBusinessUnit__c;
                            ObjOppLine.ProductLine__c = mapGMRMapping.get(mapOppLinesUniqueRef.get(ObjOppLine.Id)).NewProductLine__c;
                            ObjOppLine.ProductFamily__c = mapGMRMapping.get(mapOppLinesUniqueRef.get(ObjOppLine.Id)).NewProductFamily__c;
                            ObjOppLine.Family__c = mapGMRMapping.get(mapOppLinesUniqueRef.get(ObjOppLine.Id)).NewFamily__c;
                            ObjOppLine.Product__c = mapGMRMapping.get(mapOppLinesUniqueRef.get(ObjOppLine.Id)).OldProductID__c;
                            ObjOppLine.TECH_2014ReferentialUpdated__c=true;
                            lstOpportunityLinesToBeUpdate.add(ObjOppLine);
                        }
                    }
                }
            }
        }
        */
        
        try{
            Database.SaveResult[] OppLineUpdateResult = Database.update(lstOpportunityLinesToBeUpdate, false);
            for(Database.SaveResult objSaveResult: OppLineUpdateResult){
                if(!objSaveResult.isSuccess()){
                    Database.Error err = objSaveResult.getErrors()[0];
                    strErrorId = objSaveResult.getId() + ':' + '\n';
                    strErrorMessage = err.getStatusCode()+' '+err.getMessage()+ '\n';
                }
            }
        }    
        catch(Exception ex){
            strErrorMessage += ex.getTypeName()+'- '+ ex.getMessage() + '\n';
            strErrorType += ex.getTypeName()+'- '+ ex.getLineNumber() + '\n';
            strErrorStackTrace += ex.getTypeName()+'- '+ ex.getStackTraceString() + '\n';
        }
        finally{
            if(strErrorId.trim().length()>0 || Test.isRunningTest()){
                try{
                    DebugLog__c objDebugLog = new DebugLog__c(ExceptionType__c=strErrorType,Description__c=strErrorMessage,StackTrace__c=strErrorStackTrace, DebugLog__c= strErrorId );
                    Database.DMLOptions DMLoption = new Database.DMLOptions(); 
                    DMLoption.optAllOrNone = false;
                    DMLoption.AllowFieldTruncation = true;
                    DataBase.SaveResult debugLog_SR = Database.insert(objDebugLog, DMLoption);
                }
                catch(Exception ex){
                }
            }
        }
    }
    global void finish(Database.BatchableContext BC){
    }
   
}