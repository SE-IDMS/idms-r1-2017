@IsTest
public class PRM_VFC_MigrationFromV1toV2_TEST{
    public static testMethod void testController(){
    	

    	Country__c country = Utils_TestMethods.createCountry();
        country.countrycode__c ='ASM';
        insert country;
        
        Country__c country1 = Utils_TestMethods.createCountry();
        country1.countrycode__c ='ABC';
        insert country1;

        Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
        Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
        Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
        CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
        newRecTypeMap.name = recCatalogActivityReq.Id;
        newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
        newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;

        insert newRecTypeMap;
        
        //creates a global partner program
        PartnerPRogram__c gp1 = new PartnerPRogram__c();
        gp1 = Utils_TestMethods.createPartnerProgram();
        gp1.TECH_CountriesId__c = country.id;
        gp1.recordtypeid = Label.CLMAY13PRM15;
        gp1.ProgramStatus__c = Label.CLMAY13PRM47;
        insert gp1;

        //creates a program level for the global program
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;

        //creates a country partner program
        PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
        insert cp1;
        cp1.ProgramStatus__c = 'active';
        update cp1;

        //creates a program level for the country partner program
        ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
        insert prg2;

        ExternalPropertiesCatalogType__c epcExternalSystemType = new ExternalPropertiesCatalogType__c(Source__c='BFO',Type__c='MigrationToProgramV2',TypeOfSource__c='BFO',PropertiesAndCatalogRecordType__c='External Properties');
        insert epcExternalSystemType;
        

        PageReference pageRef = Page.VFP_MigrationFromProgramV1ToV2;
		pageRef.getParameters().put('partnerProgramId',cp1.Id);
		Test.setCurrentPage(pageRef);
    	PRM_VFC_MigrationFromV1toV2 myCtrl = new PRM_VFC_MigrationFromV1toV2();
    	System.assertNotEquals(null,PRM_VFC_MigrationFromV1toV2.EXTERNAL_SYSTEM_MIGRATION_FUNCTIONNALKEY);
    	System.assertNotEquals(null,PRM_VFC_MigrationFromV1toV2.AUTOMATIC_EVENT_TYPE);
    	System.assertNotEquals(null,myCtrl.programLevelList);
    	System.assertEquals(1,myCtrl.programLevelList.size());
    	myCtrl.programLevelList[0].isChecked = true;
    	myCtrl.generateAllV2MigrationObjects();
    }
}