@istest
private class Batch_AssignProgramChannelFeature_TEST {
   static testMethod void myUnitTest_manual() {
    
   
    Test.startTest();
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        
        PartnerPRogram__c globalProgram = Utils_TestMethods.createPartnerProgram();
        globalPRogram.recordtypeid = Label.CLMAY13PRM15;
        globalProgram.ProgramStatus__C = Label.CLMAY13PRM47;
        globalProgram.ProgramType__c = 'Channel Feature';
        insert globalProgram;
    
        ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(globalProgram.id);
        insert prg1;
        prg1.levelstatus__c = 'active';
        update prg1;   
    
    
        PartnerProgram__c countryProgram = Utils_TestMethods.createCountryPartnerProgram(globalProgram.Id, null);
        countryProgram.ProgramStatus__C = Label.CLMAY13PRM47;
        insert countryProgram; 
        list<PartnerProgram__c> countryPartnerProgram = new list<PartnerProgram__c>();
        countryPartnerProgram = [Select id,programstatus__c,country__c,recordtypeid from PartnerProgram__c where Id = :countryProgram.Id limit 1];
        
        CS_PRM_ApexJobSettings__c  cJobSetting = new CS_PRM_ApexJobSettings__c();
        cJobSetting.LastRun__c = Date.today();
        cJobSetting.InitialSync__c =false;
        cJobSetting.Name='AssignChannelFeatureByProgram';
        insert cJobSetting;
        
               
        Batch_AssignProgramChannelFeature batch_ppsa1 = new Batch_AssignProgramChannelFeature(); 
        if([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 5)
            Database.executebatch(batch_ppsa1,2);
        
    Test.stopTest();
 }
 
 
  static testMethod void myUnitTest_schedule() {
        Test.startTest();
        String CRON_EXP = '0 0 0 1 2 ? 2025';  
        String jobId = System.schedule('AssignProgramChannelFeature', CRON_EXP, new Batch_AssignProgramChannelFeature() );
        Test.stopTest();
    }

}