/*******
Author:Hanamanth 
Comments:To Unsubscribe the Report user
Release :Q2 release 


********/


public class VFC_ELLAEmailUnsubscribe {   

    
    public Boolean verified { get; private set; }
    public string userEmaiiId;
    public string userEmaiiIdEncode;    
    public Boolean verifiedError { get; set; }    
    public Boolean successMesge { get; set; }
    public VFC_ELLAEmailUnsubscribe() {
        
        verifiedError=false;
        this.verified = false;
        successMesge=false;
        userEmaiiId=ApexPages.currentPage().getParameters().get(label.CLJUN16ELLA01);
        
        try {
            if(userEmaiiId!=null && userEmaiiId!='') {

                Blob afterblob = EncodingUtil.base64Decode(userEmaiiId);
                userEmaiiIdEncode=afterblob.tostring();

            
            }else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLJUN16ELLA04));
                verifiedError=true;
            }            
        
        }catch(Exception exp) {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,exp.getmessage()));
            verifiedError=true;
        }
    }
    
    public PageReference verify() {
        
             
        List<LaunchReportStakeholders__c>   listlReportSholdr = new List<LaunchReportStakeholders__c> ();

        List<LaunchReportStakeholders__c>   listlReportupdate = new List<LaunchReportStakeholders__c> ();

        if(userEmaiiIdEncode!=null && userEmaiiIdEncode!='') {
            listlReportSholdr = [SELECT id,Email__c,IsUnsubscribe__c,IsActive__c,User__c from LaunchReportStakeholders__c where IsUnsubscribe__c=false and(Email__c=:userEmaiiIdEncode OR User__r.email=:userEmaiiIdEncode)];
        } 
        if(listlReportSholdr.size() > 0) {

            for(LaunchReportStakeholders__c  lrSObj:listlReportSholdr) {

                lrSObj.IsUnsubscribe__c=true;
                listlReportupdate.add(lrSObj);

            }
        }
        if(listlReportupdate.size() > 0) {

            Database.SaveResult[] results = Database.update(listlReportupdate,false);

            for (Database.SaveResult result : results) {
                if (result.isSuccess()) {
                    this.verified=true;
                }
            }

        }
        
        if(this.verified) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLJUN16ELLA03));
            successMesge=true;
            return null;
        }
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,label.CLJUN16ELLA04));
            verifiedError=true;            
            return null; 
        }
    }

  
   
    
    
    
}