@isTest(SeeAllData=true)
private class SVMXC_Timesheet_ReviewTest {

	private static SVMXC__Service_Group_Members__c tech = null;
	
	public static DateTime inputTestDate = System.today().addDays(-1);
    
	public static string testyr = string.valueOf(inputTestDate.year());	
    public static string testmo = string.valueOf(inputTestDate.month()) ;
    public static string testday = string.valueOf(inputTestDate.day());
    public static string hour = '23'; 
    public static string minute = '00'; 
    public static string second = '00'; 
    public static string stringDate = testyr + '-' + testmo + '-' + testday + ' ' + hour + ':' + minute + ':' + second; 
    public static Datetime testdate = Datetime.valueOf(stringDate);
		
	static testMethod void TimesheetReviewTest() {
	
		setupWeekPeriod();
        
        SVMXC_Timesheet__c ts = new SVMXC_Timesheet__c(
            Technician__c = tech.Id,
            Start_Date__c = system.today().addDays(-7).toStartOfWeek() + 1,
            End_Date__c = system.today().toStartOfWeek() - 2
        );
        
        insert ts;
        
        Account acc = Utils_TestMethods.createAccount();
       	insert acc;
       
        SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(Name = 'Test', SVMXC__Company__c = acc.Id);
        insert contract;
        
        SVMXC__Service_Order__c wo =  Utils_TestMethods.createWorkOrder(acc.id);
        wo.SVMXC__Order_Type__c='Maintenance';
        wo.CustomerRequestedDate__c = Date.today();
        wo.BackOfficeReference__c = '111111';
        wo.CustomerTimeZone__c = 'Europe/Paris';
        insert wo;
        
        List<SVMXC_Timesheet__c> tsList = [SELECT Id FROM SVMXC_Timesheet__c WHERE Technician__c = :tech.Id AND Start_Date__c <= :system.today()];
        system.assertEquals(1, tsList.size());
        
       	//RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType = 'SVMXC__Service_Order_Line__c' AND Name = 'Labor' LIMIT 1]; 
        Id RecordTypeId = ServiceMaxTimesheetUtils.getRecordTypeMap('SVMXC__Service_Order_Line__c').get(ServiceMaxTimesheetUtils.recordTypeWDTimeName);
        
        SVMXC__Service_Order_Line__c detail = new SVMXC__Service_Order_Line__c(
           RecordTypeId = RecordTypeId,
           SVMXC__Service_Order__c = wo.Id,
           SVMXC__Line_Type__c = ServiceMaxTimesheetUtils.lineTypeTime,
           SVMXC__Group_Member__c = tech.Id,
           SVMXC__Start_Date_and_Time__c = testdate.addHours(-5),
           SVMXC__End_Date_and_Time__c = testdate.addHours(-4));
        
        insert detail;
        
        List<SVMXC_Time_Entry__c> tes = [SELECT Work_Details__c, Timesheet__c FROM SVMXC_Time_Entry__c WHERE Work_Details__c =: detail.Id];      	
         
        List<SVMXC_Time_Entry__c> teInserts = new List<SVMXC_Time_Entry__c>();

        DateTime startTime = DateTime.newInstance(ts.Start_Date__c.year(), ts.Start_Date__c.month(), ts.Start_Date__c.day());
       
       	SVMXC_Time_Entry__c te1a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addHours(8), End_Date_Time__c = startTime.addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te1a); 
        
        SVMXC_Time_Entry__c te2a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(1).addHours(8), End_Date_Time__c = startTime.addDays(1).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te2a); 
        
        SVMXC_Time_Entry__c te3a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(2).addHours(8), End_Date_Time__c = startTime.addDays(2).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te3a); 
        
        SVMXC_Time_Entry__c te4a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(3).addHours(8), End_Date_Time__c = startTime.addDays(3).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te4a); 
        
        SVMXC_Time_Entry__c te5a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(4).addHours(8), End_Date_Time__c = startTime.addDays(4).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te5a);
        
        SVMXC_Time_Entry__c te6a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(5).addHours(8), End_Date_Time__c = startTime.addDays(5).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te6a); 
        
        SVMXC_Time_Entry__c te7a = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(6).addHours(8), End_Date_Time__c = startTime.addDays(6).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te7a);
        
        SVMXC_Time_Entry__c te1b = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addHours(8), End_Date_Time__c = startTime.addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te1b); 
        
        SVMXC_Time_Entry__c te2b = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(1).addHours(8), End_Date_Time__c = startTime.addDays(1).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te2b); 
        
        SVMXC_Time_Entry__c te3b = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(2).addHours(8), End_Date_Time__c = startTime.addDays(2).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te3b); 
        
        SVMXC_Time_Entry__c te4b = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(3).addHours(8), End_Date_Time__c = startTime.addDays(3).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te4b); 
        
        SVMXC_Time_Entry__c te5b = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(4).addHours(8), End_Date_Time__c = startTime.addDays(4).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te5b);
        
        SVMXC_Time_Entry__c te6b = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(5).addHours(8), End_Date_Time__c = startTime.addDays(5).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te6b); 
        
        SVMXC_Time_Entry__c te7b = new SVMXC_Time_Entry__c (Start_Date_Time__c = startTime.addDays(6).addHours(8), End_Date_Time__c = startTime.addDays(6).addHours(10),
       										Timesheet__c = tes[0].Timesheet__c, Technician__c = tech.Id);      
        teInserts.add(te7b);
        
        insert teInserts;
    	
    	SVMXC_Timesheet__c workingTS = [SELECT Id FROM SVMXC_Timesheet__c WHERE Id =: tes[0].Timesheet__c LIMIT 1];
    
        ApexPages.StandardController controller = new ApexPages.StandardController(workingTS);       
        
        Test.startTest();
        
        SVMXC_Timesheet_ReviewExtension tre = new SVMXC_Timesheet_ReviewExtension(controller);
        
        Test.setCurrentPage(new PageReference('/apex/SVMXC_Timesheet_Review'));
        
        tre.init();

        Test.stopTest();
        	
	}
	
	private static void setupWeekPeriod()  {
        SVMXC__Service_Group__c team = new SVMXC__Service_Group__c(
            Name = 'Test team'
        );
        
        insert team;
        
        user userObj = Utils_TestMethods.createStandardUser('test');
        insert userObj;
        
        tech = new SVMXC__Service_Group_Members__c(
            SVMXC__Service_Group__c = team.Id,
            SVMXC__Salesforce_User__c = userObj.Id);
        
        insert tech;
        
        Map<String, String> settingsMap = new Map<String,String>();
        settingsMap.put('TIMESHEET002', '1');
        settingsMap.put('TIMESHEET003', 'false');
        settingsMap.put('PROriginationDate', '2013-01-06');
        
        ServiceMaxTimesheetUtils.appSettings = settingsMap;
    }
}