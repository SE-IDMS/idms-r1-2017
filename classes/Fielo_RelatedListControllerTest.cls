@isTest

public with sharing class Fielo_RelatedListControllerTest {
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        
        Account acc= new Account(
            Name = 'test',
            Street__c = 'Some Street',
            ZipCode__c = '012345'
        );
        insert acc;
                
        FieloEE__Member__c mem = new FieloEE__Member__c(
            FieloEE__FirstName__c = 'test',
            FieloEE__LastName__c= 'test'
        );
        insert mem;
    
        FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
        insert memb;
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c(
            FieloEE__Title__c = 'test'
        );
        insert menu;

        Fielo_CRUDObjectSettings__c testCrud = new Fielo_CRUDObjectSettings__c(
            F_ObjectAPIName__c = 'Account'
        );
        insert testCrud;
    
        FieloEE__Component__c component = new FieloEE__Component__c(
            FieloEE__Menu__c = Menu.Id,
            F_CRUDObjectSettings__c = testCrud.Id
        );
        insert component;
        
        Fielo_Listview__c listview = new Fielo_ListView__c(
            F_ObjectSettings__c = testCrud.Id,
            F_FieldSet__c = 'Name',
            F_RedirectDelete__c = menu.Id,
            F_RedirectDetails__c = menu.Id,
            F_RedirectEdit__c = menu.Id,
            F_RedirectNew__c = menu.Id,
            F_RelatedListObjectAPIName__c = 'Account'
        );
        insert listview;
        
        ApexPages.currentPage().getParameters().put('recordId', acc.Id);
        Fielo_RelatedListController test = new Fielo_RelatedListController();
        test.setcurrentLV(listview);
        test.setfRecordId(acc.Id);
        
        test.doCreate();
        test.doRead();
        test.doUpdate();
        test.doDelete();
    }
}