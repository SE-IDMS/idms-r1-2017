/*
BR-7383
OCT 15 RELEASE
Divya M
to get the no of Related Symptoms Associated with the respective Problems. and Update on the Update on the Problem
*/

public class AP_RelatedSymptom{
    public static void CountRelSymptoms(list<RelatedSymptom__c> lstSymp){
        set<id> PrblmIds = new set<id>();
        for(RelatedSymptom__c sym : lstSymp){
            PrblmIds.add(sym.Problem__c);
        }
        list<Problem__c> prblmstoUpd = new list<Problem__c>();
        list<RelatedSymptom__c> relSympLst = [Select id, Name, Problem__c, Symptom__c, SubSymptom__c from RelatedSymptom__c where Problem__c in: PrblmIds];
        if(relSympLst.size()>0){
            integer count;
            for(Problem__c prblm : [Select id, Name, CountOfRelatedSymptoms__c from Problem__c where id in: PrblmIds]){
                count = 0;
                for(RelatedSymptom__c relSym : relSympLst){
                    if(relSym.Problem__c == prblm.id){
                        Count = Count + 1;
                    }
                }
                prblm.CountOfRelatedSymptoms__c = Count;
                prblmstoUpd.add(prblm);
            }
            if(prblmstoUpd.size()>0){
                Update prblmstoUpd;
            }
        }       
    }
}