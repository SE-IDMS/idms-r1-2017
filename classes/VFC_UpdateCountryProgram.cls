/*****************************************************************************************
    Author : Shruti Karn
    Description : For May 2013 Release to update Partner Program Status to 'Decommissioned'
                                                         
*****************************************************************************************/

public with sharing class VFC_UpdateCountryProgram
{
    //public String ProgramID{get;set;}
    public PartnerProgram__c program{get;set;}
    public User LoggedInUser {get;set;} {LoggedInUser = [select id,Name, ProfileId, UserRoleId, ProgramAdministrator__c, ProgramApprover__c,   ProgramManager__c,ProgramOwner__c,  PartnerORFConverter__c from User WHERE id =:UserInfo.getUserId()];}
    public String DecomError ; //Apr14 Release
    
  public VFC_UpdateCountryProgram.VFC_UpdateCountryProgram(ApexPages.StandardController controller)
    {
        program = (PartnerProgram__c)controller.getRecord();
    }
    
    public pagereference updateCountryStatus()
    {
        if(LoggedInUser.ProgramAdministrator__c == false)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,System.Label.CLMAY13PRM51));
            return null;
        }  
        //String ProgramID = ApexPages.currentPage().getParameters().get('PrgId');
        if(program != null)
        {
            list<PartnerProgram__c> lstPartnerPRG = new list<PartnerProgram__c>();
            lstPartnerPRG = [Select id,programstatus__c from PartnerProgram__c where id = :program.Id limit 1];
            if(!lstPartnerPRG.isEmpty())
                lstPartnerPRG[0].programstatus__c = Label.CLMAY13PRM34;
            if(!lstPartnerPRG.isEmpty())
            {
                /*Database.SaveResult[] SaveResult1 = database.update(lstPartnerPRG,false);
                for(Integer i=0;i<SaveResult1.size();i++ )
                {
                    Database.SaveResult sr =SaveResult1[i];
                    if(!sr.isSuccess())
                    {
                        Database.Error err = sr.getErrors()[0];
                        //lstPartnerPRG[0].addError( err.getMessage());  
                        system.debug('Error:'+err.getMessage());
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,Label.CLMAY13PRM35);
                        ApexPages.addmessage(msg); 
                        return null;
                    }
                    else
                    {
                        PageReference PartnerPRGPage = new PageReference('/'+Program.ID);
                        return PartnerPRGPage;
                    }
                }*/
                try
                {
                    update lstPartnerPRG;
                    PageReference PartnerPRGPage = new PageReference('/'+Program.ID);
                    
                    if(isTestException == true)
                        throw new TestException();
                    
                    return PartnerPRGPage;
                }
                catch(Exception e)
                {
                   // for (Integer i = 0; i < e.getNumDml(); i++) 
                    //{ 
                        //if(e.getDmlMessage(i).toUpperCase().contains(Label.CLMAY13PRM43))
                            //ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, Label.CLMAY13PRM35,'')); 
                       // else    
                           // ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i).toUpperCase(),'')); System.debug(e.getDmlMessage(i)); 
                   // } 
                    
                     ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage().toUpperCase(),'')); System.debug(e.getMessage()); 
                    return null;
                    
                }
            }
        }
        return null;
          
    }
    
   /*****************************************************************************************************************************************************
     Created By  : Renjith Jose
     Release     : BR-4702, APR14 Release.
     Description : Approval Process for Decommissioning of a Partner Program 
    *****************************************************************************************************************************************************/
   public pagereference DecommissionProgram()
    {
    
        //Validate the Program is in Active Status
        //Program Owner only can submit the Program for decommission
        //Initiating Decommission Approval Request
        
        list<PartnerProgram__c> lstPartnerPRG = new list<PartnerProgram__c>();
        lstPartnerPRG = [Select id,programstatus__c,OwnerID,TECH_DecommissionRequester__c from PartnerProgram__c where id = :program.Id limit 1];

        if(lstPartnerPRG.size()>0) {
            lstPartnerPRG[0].TECH_DecommissionRequester__c = LoggedInUser.id;
            //if(LoggedInUser.ProgramAdministrator__c == false) { 
                //Validate the Program is in Active Status
                if(lstPartnerPRG[0].ProgramStatus__c != System.label.CLMAY13PRM47) //Active 
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.label.CLAPR14PRM01)); 
                
                //Program Owner only can submit the Program for decommission
              /*  if(String.valueOf(UserInfo.getUserId()) != String.valueOf(lstPartnerPRG[0].OwnerID))
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.label.CLMAY13PRM51)); */
            
                if(ApexPages.hasMessages())
                    return null;
        
                //Changing status to pending decommission approval
                //lstPartnerPRG[0].ProgramStatus__c = System.label.CLAPR14PRM03;
                try {   
                    System.debug('******** Updating Program');
                    Update lstPartnerPRG[0]; 
                    
                    System.debug('******** Approval Process - start');
                    // Create an approval request
                    Approval.ProcessSubmitRequest decomReq = new Approval.ProcessSubmitRequest();
                    decomReq.setComments(System.label.CLAPR14PRM02); //Submit request for Decommission Approval.
                    decomReq.setObjectId(Program.id);       
            
            
                    Approval.ProcessResult result = Approval.process(decomReq);
        
                    // Verify the result
                    System.debug('******->'+result.isSuccess());
                    if(!result.isSuccess()) {
                        ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, result.getErrors()[0].getMessage())); 
                        System.debug('*** Result Error '+result.getErrors()[0].getMessage());
                        return null;        
                    }
                }
                catch(Exception e) {
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage().toUpperCase(),'')); System.debug(e.getMessage()); 
                    return null;                    
                }
            // }
            // else //Decommissioned by Administrator 
            // {
            //     try
            //     {
            //         lstPartnerPRG[0].programstatus__c = Label.CLMAY13PRM34;
            //         Update lstPartnerPRG[0];
            //     }
            //     catch(Exception e)
            //     {
            //         ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage().toUpperCase(),'')); System.debug(e.getMessage()); 
            //         return null;                    
            //     }
                
            // }
            
        } //end of if(lstPartnerPRG.size()>0)
             
         PageReference PartnerPRGPage = new PageReference('/'+Program.ID);
         return PartnerPRGPage;
    }
    
        
    //For test 
    public static Boolean isTestException=false;
    // Inner class
    class TestException extends Exception {}
}//End of class