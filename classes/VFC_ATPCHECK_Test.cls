@isTest(seealldata=true)
global class VFC_ATPCHECK_Test {
        static testMethod void myUnitTest() {               
        PageReference pageRef = Page.VFP_ATPCHECK;
        Test.setCurrentPage(pageRef);
        Account acc = Utils_TestMethods.createAccount();
            acc.name='test account';
                insert acc;
              SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c(BackOfficeSystem__c='Part',CountryOfBackOffice__c='Ind');
                    insert workorder;   
             SVMXC__RMA_Shipment_Order__c partsorder= new SVMXC__RMA_Shipment_Order__c(Ship_to__c=acc.id,SVMXC__Service_Order__c=workorder.id,SVMXC__Order_Status__c='Open');                   
                     insert partsorder;         
              SVMXC__RMA_Shipment_Line__c partsorderline1 = new SVMXC__RMA_Shipment_Line__c();
                 partsorderline1.SVMXC__Service_Order__c=workorder.id;
                 partsorderline1.recordtypeid='012A0000000nphY';
                 partsorderline1.SVMXC__RMA_Shipment_Order__c = partsorder.id;
                 partsorderline1.Ship__c = acc.id;       
                   insert partsorderline1; 
                               
           VFC_ATPCHECK VFController = new VFC_ATPCHECK(new ApexPages.StandardController(workorder)); 
           //Test.setMock(WebServiceMock.class, new VFC_ATPCHECK_Test.WS_MOCK_ATPCHECK_TEST());         
             VFC_ATPCHECK.PartsOrderLine pline = new  VFC_ATPCHECK.PartsOrderLine();
                 pline.checked=true;
                 pline.isshow=false;
                 pline.isactive=false;
                 pline.cstyle ='mystyle';
                 pline.Message='testmsg';
                 pline.CalculatedplantName='calcplant';
                 pline.recordtypeid='012A0000000nphY';
                 pline.plid=partsorderline1.id;
                 pline.pid=partsorderline1.SVMXC__RMA_Shipment_Order__c;
                 pline.pline = partsorderline1;
                 pline.pline.recordtypeid='012A0000000nphY';
                VFController.linemap.put(partsorderline1.id,partsorderline1);
                VFController.plist= new list<VFC_ATPCHECK.PartsOrderLine>();
              VFController.plist.add(pline);
              VFController.checkBackOffice();
              VFController.save();
              VFController.cancel();
                Plant__c plant=new Plant__c(name='test plant');
            insert plant;                
            VFController.getPlantIDFromName('test plant');
             VFController.getURL(partsorderline1);                
             VFController.getIsOracleSystem();       
    
     }
     static testMethod void myUnitTest2() {
       Test.starttest();
        PageReference pageRef = Page.VFP_ATPCHECK;
        Test.setCurrentPage(pageRef);
        Account acc = Utils_TestMethods.createAccount();
            acc.name='test account';
                insert acc;
              SVMXC__Service_Order__c workorder = new SVMXC__Service_Order__c(BackOfficeSystem__c='Part',CountryOfBackOffice__c='Ind');
                    insert workorder;   
             SVMXC__RMA_Shipment_Order__c partsorder= new SVMXC__RMA_Shipment_Order__c(Ship_to__c=acc.id,SVMXC__Service_Order__c=workorder.id,SVMXC__Order_Status__c='Open');                   
                     insert partsorder;         
              SVMXC__RMA_Shipment_Line__c partsorderline1 = new SVMXC__RMA_Shipment_Line__c();
                     partsorderline1.SVMXC__Service_Order__c=workorder.id;
                     partsorderline1.recordtypeid='012A0000000nphZ';
                     partsorderline1.SVMXC__RMA_Shipment_Order__c = partsorder.id;
                     partsorderline1.Ship__c = acc.id;       
                       insert partsorderline1;                                    
           VFC_ATPCHECK VFController = new VFC_ATPCHECK(new ApexPages.StandardController(workorder));
           Test.stoptest();
    }
}