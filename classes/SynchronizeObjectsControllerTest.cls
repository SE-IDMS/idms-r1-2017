@isTest
private class SynchronizeObjectsControllerTest {
    
    /**
    * @author Tomas Garcia E.
    * @date 11/12/14
    * @description SynchronizeObjectsController test.
    */

  /*Tests from Contact to Account*/
    static testMethod void testSingle() {
        Account accTest = new Account(Name='Account test 1');
        insert accTest;
        
        Contact conTest = new Contact(firstName = 'Contact', lastName = 'Test 1', accountId = accTest.Id);
        conTest.Fax = '1233';
        conTest.HomePhone = '9999';
        conTest.Title = 'Test title';
        insert conTest;
      
        ObjectSync__c config = new ObjectSync__c();
        config.FromIdentifier__c = 'AccountId';
        config.FromObject_API_Name__c = 'Contact';
        config.ToIdentifier__c = 'Id';
        config.ToObject_API_Name__c = 'Account';
        config.Condition__c = '';
        config.MappingFields_JSON__c = '[{"fromApiField":"Title","toApiField":"Description","Nillable":false}]';
        config.RoundTrip__c = false;
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(config);
        SynchronizeObjectsController sync = new SynchronizeObjectsController(sc);
        sync.objSync.FromIdentifier__c = 'Account' ;
        sync.objSync.ToIdentifier__c = 'Account';
        sync.getObjectsNames();
        sync.getDeFieldsNames();
        sync.getParaFieldsNames();
        sync.generateMappingFields();
        sync.Save();
        
        ObjectSync__c config2 = new ObjectSync__c();
        config2.FromIdentifier__c = 'AccountId';
        config2.FromObject_API_Name__c = 'Contact';
        config2.ToIdentifier__c = 'Id';
        config2.ToObject_API_Name__c = 'Account';
        config2.Condition__c = '';
        config2.MappingFields_JSON__c = '[{"fromApiField":"Title","toApiField":"Description","Nillable":false}]';
        config2.RoundTrip__c = false;
        insert config2;       
            
        ApexPages.StandardController sc2 = new ApexPages.StandardController(config2);
        SynchronizeObjectsController sync2 = new SynchronizeObjectsController(sc2);
        sync2.Save();
        sync2.getObjectsNames();
        sync2.getDeFieldsNames();
        sync2.getParaFieldsNames();
        sync2.generateMappingFields();
        sync2.Save();
  
    }
    
     static testMethod void testSingle2() {
         
        ObjectSync__c config2 = new ObjectSync__c();
        config2.FromIdentifier__c = 'AccountId';
        config2.FromObject_API_Name__c = 'Contact';
        config2.ToIdentifier__c = 'Id';
        config2.ToObject_API_Name__c = 'Account';
        config2.Condition__c = '';
        config2.MappingFields_JSON__c = '[{"fromApiField":"Title","toApiField":"Description","Nillable":false}]';
        config2.RoundTrip__c = false;
        insert config2;       
        config2.RoundTrip__c = false;
        upsert config2;  
            
        ApexPages.StandardController sc2 = new ApexPages.StandardController(config2);
        SynchronizeObjectsController sync2 = new SynchronizeObjectsController(sc2);
        config2.RoundTrip__c = true;
        sync2.Save();
        sync2.getObjectsNames();
        sync2.getDeFieldsNames();
        sync2.getParaFieldsNames();
        sync2.generateMappingFields();
       // sync2.Save();
  
    }
}