public with sharing class Utils_SolutionCenterPicklistManager implements Utils_PicklistManager{
  // Return name of the levels
    Public list<String> getPickListLabels()
    {
        List<String> Labels = new List<String>();
        Labels.add(Label.CL10077);
        Labels.add(Label.CL10206);
        Labels.add(Label.CLSLSFEB1401);
        return Labels;
    } 

    // Return All Picklist Values 
    public List<SelectOption> getPicklistValues(Integer i, String S)
    {
       List<SelectOption> options = new  List<SelectOption>();
       
       options.add(new SelectOption(Label.CL00355,Label.CL00355)); 
 
       // Populate Business fields and Country when Page Loads
       if(i==1)
       {
            // Populate Business Picklist Values
            Schema.DescribeFieldResult business = Schema.sObjectType.SolutionCenter__c.fields.BusinessesServed__c;
            for (Schema.PickListEntry PickVal : business.getPicklistValues())
            {
                options.add(new SelectOption(PickVal.getValue(),PickVal.getLabel()));
            }
       }
       if(i==2)
       {
            // Populate Country Picklist Values
            for(Country__c countries: [Select Id, Name from Country__c order by Name asc])
            {
                options.add(new SelectOption(countries.Id,countries.Name));
            }
       }
        if(i==3)
       {
            // Populate Tier Category Picklist Values
            Schema.DescribeFieldResult tier = Schema.sObjectType.SolutionCenter__c.fields.TierCategory__c;
            for (Schema.PickListEntry PickVal : tier.getPicklistValues())
            {
                options.add(new SelectOption(PickVal.getValue(),PickVal.getLabel()));
            }
       }

       return  options;  
    }
}