@isTest
Public Class VFC_CISCompetencyDelete_TEST {
    Static testMethod void competencyDelTest(){
          
        List<Country__c> countryList = new  List<Country__c>();
        
        Country__c countryInstance1 = new Country__c();
        countryInstance1.Name = 'India';
        countryInstance1.CountryCode__c ='IN';
        countryList.add(countryInstance1);  
        
        Country__c countryInstance2 = new Country__c();
        countryInstance2.Name = 'France';
        countryInstance2.CountryCode__c ='FR';
        countryList.add(countryInstance2);
       
        Database.insert(countryList);
        
        
        CIS_ServiceCenter__c serviceCenterObj = new CIS_ServiceCenter__c();
        serviceCenterObj.CIS_Country__c=countryInstance1.id;
        serviceCenterObj.CIS_ScopeOfCountries__c='India';
        insert serviceCenterObj;
        
        CIS_FieldServiceRepresentative__c fSRObj = new CIS_FieldServiceRepresentative__c();
        fSRObj.CIS_ServiceCenter__c=serviceCenterObj.id; 
        fSRObj.CIS_ScopeOfCountries__c='India';
        insert fSRObj;
        
        CIS_Competency__c competenceObj = new CIS_Competency__c();
        competenceObj.CIS_FieldServiceRepresentative__c=fSRObj.id;
        competenceObj.CIS_ServiceCenter__c=serviceCenterObj.id;
        insert competenceObj;
        
        ApexPages.currentPage().getParameters().put('id',competenceObj.id);
        ApexPages.currentPage().getParameters().put('retURL', '/home/home.jsp');
        ApexPAges.StandardController sc = new ApexPages.StandardController(competenceObj);
        VFC_CISCompetencyDelete testController = new VFC_CISCompetencyDelete(sc);
        testController.gobacktoHome();
        testController.checkOwnerandUser();
        
    }


}