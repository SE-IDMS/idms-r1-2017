@isTest
private class ScorecardDMLQuestionnaireCont_test{
    static testMethod void ScorecardDML() {
    
       User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('Entity5');
        System.runAs(runAsUser){
        
         Global_KPI_Targets__c GKPIT=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Year__c = '2013',Global_KPI_Owner__c=label.Connect_Email,Transformation__c = Label.ConnectTransPeople,KPI_Type__c = 'Country KPI',CountryPickList__c = 'test' );
         insert GKPIT;
         Cascading_KPI_Target__c EKPI = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email,CountryPickList__c = 'test' );
         insert EKPI;
         DML_Questionnaire__c DMLQ1 = new DML_Questionnaire__c(Question__c = 'test1', Year__c = '2013');
         insert DMLQ1;
         DML_Questionnaire__c DMLQ2 = new DML_Questionnaire__c(Question__c = 'test2', Year__c = '2013');
         insert DMLQ2;
         DML_Questionnaire__c DMLQ3 = new DML_Questionnaire__c(Question__c = 'test3', Year__c = '2013');
         insert DMLQ3;
         DML_Questionnaire__c DMLQ4 = new DML_Questionnaire__c(Question__c = 'test4', Year__c = '2013');
         insert DMLQ4;
         DML_Answers__c DMLAns1 = new DML_Answers__c(Question_ID__c = DMLQ1.Name, Entity_Target__c = EKPI.id,Entity__c = 'test',Answerpicklist__c = 'Yes', DML_Questionnaire__c = DMLQ1.Id);
         insert DMLAns1;
         DML_Answers__c DMLAns2 = new DML_Answers__c(Question_ID__c = DMLQ2.Name,Entity_Target__c = EKPI.id,Entity__c = 'test',Answerpicklist__c = 'Yes',DML_Questionnaire__c = DMLQ2.Id);
         insert DMLAns2;
         DML_Answers__c DMLAns3 = new DML_Answers__c(Question_ID__c = DMLQ3.Name,Entity_Target__c = EKPI.id,Entity__c = 'test',Answerpicklist__c = 'Yes',DML_Questionnaire__c = DMLQ3.Id);
         insert DMLAns3;
         DML_Answers__c DMLAns4 = new DML_Answers__c(Question_ID__c = DMLQ4.Name,Entity_Target__c = EKPI.id,Entity__c = 'test',Answerpicklist__c = 'Yes',DML_Questionnaire__c = DMLQ4.Id);
         insert DMLAns4;
         
         PageReference pageRef = Page.DML_Questionnaire;
       
       
       Test.setCurrentPage(pageRef);
     
       
       ApexPages.StandardController stanPage= new ApexPages.StandardController(EKPI);
       ScorecardDMLQuestionnaireCont controller = new ScorecardDMLQuestionnaireCont(stanPage);

      
       ApexPages.currentPage().getParameters().put('Id', EKPI.id); 
       
       
       controller.getDMLAnswers(); 
       controller.getItems(); 
       controller.change(); 
       controller.cancel(); 
       controller.save(); 
       controller.getQuarter();
       }
       }
       }