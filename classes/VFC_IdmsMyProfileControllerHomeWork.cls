/**
•   Created By: Sudhanshu
•   Created Date: 01/06/2016
•   Modified by:  06/12/2016
•   Description: class used as controller for vf page user profile for updating user profile
**/ 
    public with sharing class VFC_IdmsMyProfileControllerHomeWork{
    public boolean isEditEmail{get; set;}
    public boolean redirectToApp{get; set;}
    public String preflang{get; set;}
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}  
    public Boolean setpwdFlag{get; set;}  
    public Boolean resetpwdFlag{get; set;}    
    public Boolean pwdresetSuccess{get; set;}
    public Boolean testClassVar{get; set;} 
    public String testStringval{get; set;} 
    public String appid{get; set;} 
    public List<SelectOption> statusOptionsPrefLang{get;set;}
    private User user;
    private User userEmail;
    public User userCountryState{get; set;}
    public String appUrl{get; set;} 
    public string authToken=ApexPages.currentPage().getParameters().get('token');
    public boolean headQ{get;set;}    
    public string registrationSource{get;set;}    
    public string userContext='';    
    public boolean isContextWork{get;set;}    
    public boolean isIdentityTypeMobile{get;set;}    
    public string changeEmailMobile{get;set;}    
    public string updateEmailMobile{get;set;}    
    public string emailMobileVal{get;set;}    
    public string buttonText{get;set;}    
    public boolean checkedEmailOpt{get;set;}    
    public string emailMobileText{get;set;}    
    public string emailMobileValues {get;set;}    
    public string mobilePhone{get;set;}    
    public string mobileCountryCode{get;set;}    
    public string UPhone{get;set;}    
    public string UPhoneCountryCode{get;set;}    
    public boolean isSocialUser{get;set;}    
    public string userEmailVal{get;set;}    
    public string userMobileVal{get;set;}    
    public string mobileHidden{get;set;}    
    public string changeIdentityLink{get;set;}    
    public string changePasswordLink{get;set;}    
    public string userProfileLink{get;set;}    
    public string language{get;set;}    
    public string errorMsg{get;set;}    
    public string appIDpass{get;set;}    
    public boolean hideAllFields{get;set;}
    public string userworkphone{get;set;}
    Public string appTabName{get;set;}
    public string appTabLogo{get; set;}
    public string appandSE{get;set;}
    public boolean isLogoPresent{get;set;}
    public string currentPageURL=ApexPages.currentPage().getUrl();
    public string selectedVal{get;set;}
    public string langCode{get;set;}
        
    //get the user data from IdmsMyProfilePage
    public User getUser() {
        return user;
    }
    
    //constructor used for redirections and fetching values from custom setting
    public VFC_IdmsMyProfileControllerHomeWork() {
    
        String langFromURL = ApexPages.currentPage().getParameters().get('lang');  
        
        if(string.isnotblank(langfromURL)){
        
        IDMSAvailableLanguages__c appMap = new IDMSAvailableLanguages__c();
        
        appMap = IDMSAvailableLanguages__c.getInstance(langFromURL);
        
        if(appMap!=null){
        
        language=langFromURL;
        
        }
        
        else
        {
        
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible','IE=10');
        
        string langFromBrowser=ApexPages.currentPage().getHeaders().get('Accept-Language');
        
        if(langFromBrowser.contains(',')){
        integer indexofComma=langFromBrowser.indexof(',');
        
        system.debug(indexofComma);
        
        language=langFromBrowser.substring(0,indexofComma);
        }
        else{
        language=langFromBrowser;
        }
        
        }
        
        }      
         
        else{ 
         
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible','IE=10');
        
        string langFromBrowser=ApexPages.currentPage().getHeaders().get('Accept-Language');
        
        if(langFromBrowser.contains(',')){
        integer indexofComma=langFromBrowser.indexof(',');
        
        system.debug(indexofComma);
        
        language=langFromBrowser.substring(0,indexofComma);
        }
        else{
        language=langFromBrowser;
        }
        
        }
        
        isIdentityTypeMobile        = false;        
        isSocialUser                = false;        
        appid                       = ApexPages.currentPage().getParameters().get('app');
        registrationSource          = appid;        
        isContextWork               = false;        
        Schema.FieldSet companyFieldSet             = Schema.SObjectType.User.fieldSets.getMap().get('IDMSCompanyFields');        
        List<Schema.FieldSetMember> CompanyFieldObj = companyFieldSet.getFields();        
        string queryFS = '';
        
        for(Schema.FieldSetMember CompanyFieldObjItr:CompanyFieldObj){
            queryFS=queryFS+ ',' +CompanyFieldObjItr.getFieldPath();             
            system.debug('---SOQLquery ---'+CompanyFieldObjItr.getFieldPath());
        }
        
        isEditEmail   = false;
        redirectToApp = false;
        string id     = +UserInfo.getUserId();
        string query  = 'select id,IDMS_Registration_Source__c,idmsapplanguage__c,IDMSMobileEmail__c,usertype, email,IDMS_Email_opt_in__c,IDMSIdentityType__c,'+
                        'IDMS_User_Context__c,statecode,countrycode,Country,state,username, MobilePhone, firstname,IDMSMiddleName__c,IDMSSalutation__c,'+
                        'IDMSSuffix__c, lastname,IDMS_PreferredLanguage__c, DefaultCurrencyIsoCode, street,City, PostalCode,  IDMS_POBox__c, Phone, '+
                        'localesidkey, extension,LanguageLocaleKey,TimeZoneSidKey,IDMS_County__c,IDMS_AdditionalAddress__c,federationIdentifier,idmsprimarycontact__c'+queryFS+' FROM User where id =:id';
        system.debug(query);
        user = database.query(query);
        system.debug(user);
        
        userworkphone=user.phone;
                
        if(string.isnotblank(user.IDMSIdentityType__c)){
            if(user.IDMSIdentityType__c.equals('Mobile')){
                system.debug(user);
                isIdentityTypeMobile = true;
                changeEmailMobile    = label.CLNOV16IDMS238;
                updateEmailMobile    = label.CLNOV16IDMS195;
                appTabName           = Label.CLNOV16IDMS128;
                appTabLogo           = Label.CLFEB17IDMS004;
                emailMobileVal       = user.mobilephone;
                buttonText           = label.CLNOV16IDMS238;
                emailMobileText      = 'Email';
                emailMobileValues    = user.IDMSMobileEmail__c;
            }
            if(user.IDMSIdentityType__c.equals('Email')){
                isIdentityTypeMobile = false;
                system.debug(user);
                changeEmailMobile    = label.CLNOV16IDMS239;
                updateEmailMobile    = label.CLNOV16IDMS240;
                emailMobileVal       = user.email;
                buttonText           = label.CLNOV16IDMS239;
                emailMobileText      = 'Mobile';
                emailMobileValues    = user.email;
                userMobileVal        = user.mobilephone;
            }
        }
        userCountryState             = new User();
        userCountryState.countryCode = user.company_country__c;
        userCountryState.stateCode   = user.company_state__c;
        userMobileVal                = user.mobilephone;
        system.debug('company_country= '+userCountryState.countryCode+'   ;   company_state = '+userCountryState.stateCode);
        userContext                  = user.IDMS_User_Context__c;
        if(string.isnotblank(userContext)){
            if(user.IDMS_User_Context__c.equalsignorecase('Work') || user.IDMS_User_Context__c.equalsignorecase('@Work')){
                isContextWork=true;
                system.debug(user);
            }else{
                isContextWork = false;
                system.debug(user);
            }
        }
        if(user.IDMSCompanyHeadquarters__c){
            headQ = true;
        }
        if(user.IDMS_Email_opt_in__c.equals('Y')){
            checkedEmailOpt=true;
        }else{
            checkedEmailOpt=false;
        }        
        if(user.IDMSAnnualRevenue__c == Integer.valueOf('0')){
            user.IDMSAnnualRevenue__c = null;
        }
        if(string.isnotblank(user.IDMS_Registration_Source__c)){
            if(label.CLNOV16IDMS003.contains(user.IDMS_Registration_Source__c)){
                isSocialUser=true;
            }
        }
        
        //code to load pref language picklist
        Schema.DescribeFieldResult statusFieldPrefLang = Schema.User.IDMS_PreferredLanguage__c.getDescribe();
        statusOptionsPrefLang = new list<SelectOption>();
        
        //country pick list
        if(user.IDMS_PreferredLanguage__c == null || user.IDMS_PreferredLanguage__c.contains('None')){
            for (Schema.Picklistentry picklistPrefLang : statusFieldPrefLang.getPicklistValues()){
                if(picklistPrefLang.getValue().contains('None')){
                    system.debug(user);
                    continue;
                }else{
                    system.debug(user);
                    statusOptionsPrefLang.add(new SelectOption(picklistPrefLang.getValue(),picklistPrefLang.getLabel()));
                }
            }
        }else{
            for (Schema.Picklistentry picklistPrefLangNew : statusFieldPrefLang.getPicklistValues())
            {
                if(picklistPrefLangNew.getValue() == user.IDMS_PreferredLanguage__c){
                    statusOptionsPrefLang.add(new SelectOption(picklistPrefLangNew.getValue(),picklistPrefLangNew.getLabel()));
                    system.debug(user);
                }
            }
            for (Schema.Picklistentry picklistPrefLang : statusFieldPrefLang.getPicklistValues()){
                if(picklistPrefLang.getValue() == user.IDMS_PreferredLanguage__c){
                    continue;
                }else{
                    statusOptionsPrefLang.add(new SelectOption(picklistPrefLang.getValue(),picklistPrefLang.getLabel()));
                }
            }
        }
        
        //get ap url on the basis of app name
        //this condition will run in case of test class only
        if(Test.isRunningTest()){
            registrationSource = 'idms';
        }
        //end test condition   
        
        
        
        IDMSApplicationMapping__c appMap1 = new IDMSApplicationMapping__c();
        hideAllFields = false;
        if(string.isblank(appid)){
            hideAllFields=true;
        }else{
            hideAllFields=false;
        }
        appAndSE                        = Label.CLR117IDMS004;
        appMap1 = IDMSApplicationMapping__c.getInstance(appid);
        if(appMap1 != null){
            appIDpass          = (String)appMap1.get('appid__c');
            registrationSource = (String)appMap1.get('Name');
            system.debug(registrationSource);
            if(string.isnotblank((string)appMap1.UrlRedirectAfterProfileUpdate__c)){
                appurl = (string)appMap1.UrlRedirectAfterProfileUpdate__c;
            }
            else{
                appurl = '/identity/idp/login?app='+appMap1.appid__c;
            }
            
            
            appAndSE            = (String)appMap1.get('AppBrandOrLogo__c');
            
            
            // added to show application specific TabName as in BFOID-523
                    appTabName = (String)appMap1.get('AppTabName__c');
                    if(String.isBlank(AppTabName)){
                        AppTabName     = Label.CLNOV16IDMS128;
                    }
       // added to show application specific TabLogo as in BFOID-523
                    appTabLogo         = (String)appMap1.get('AppTabLogo__c');
                    if(String.isBlank(AppTabLogo)){
                        AppTabLogo     = Label.CLFEB17IDMS004;
                    } 
                    
                    if(string.isnotblank(appAndSe)){
        isLogoPresent=true;
        }
        else{
        appAndSE                        = Label.CLR117IDMS004;
        }
                        
        }else{
            hideAllFields=true;
        }
        
        
        
        // guest users should never be able to access this page
        if (user.usertype.equalsignorecase('Guest')) {
            system.debug('user without session');
            throw new NoAccessException();
        }        
        
        PageReference pgChangeEmail = Page.changeEmailMobile;
        pgChangeEmail.getParameters().put('app', ApexPages.currentPage().getParameters().get('app'));
        changeIdentityLink = pgChangeEmail.getUrl();
        
        PageReference pgPasswordUpdate   = Page.IDMSPasswordUpdate;
        pgPasswordUpdate.getParameters().put('app', ApexPages.currentPage().getParameters().get('app'));
        changePasswordLink=pgPasswordUpdate.getUrl();
        
        PageReference pgUserProfile = Page.userprofile;
        pgUserProfile.getParameters().put('app', ApexPages.currentPage().getParameters().get('app'));
        userProfileLink = pgUserProfile.getUrl();
    }
    
    //used for rendering on vf page
    public void editEmail() {
        isEditEmail=true;
    }
    // method used for callind idmsupdate user class for updating user profile
    public PageReference save() {
        try {
        
            user.phone=userworkphone;
            system.debug('hidden value-'+mobileHidden);
            //validation for firstname and lastname
            if (user.FirstName == null||user.FirstName == '') {
                errorMsg = 'firstname.error';
                return null;
            }
            if (user.LastName == null || user.LastName == ''){
                errorMsg='lastname.error';              
                return null;
            }
            //need to uncomment for standard country and state changes
            if(user.IDMSIdentityType__c.equals('Mobile')){
                system.debug('value of mobileEmail:'+emailMobileValues);
                system.debug('value of mobile:'+userMobileVal);
                user.IDMSMobileEmail__c = emailMobileValues;
                user.email              = user.IDMSMobileEmail__c;
            }
            if(user.IDMSIdentityType__c.equals('Email')){
                system.debug('value of mobileEmail:'+emailMobileValues);
                system.debug('value of mobile:'+userMobileVal);
                user.mobilephone = userMobileVal;
            }
            if (user.Phone != null && user.Phone != ''){
                Boolean result    = false;
                String PhValid    = Label.CLJUN16IDMS31;
                Pattern mypattern = Pattern.compile(PhValid);
                Matcher myMatcher = mypattern.matcher(user.Phone);
                result            = myMatcher.matches();
                If(result == false){
                    errorMsg = 'mobile.error';
                    return null;
                }
            }
            if (user.MobilePhone != null && user.MobilePhone != ''){
                If(!Pattern.matches(Label.CLJUN16IDMS31,user.MobilePhone)){
                    errorMsg = 'mobile.error';                   
                    return null;
                }
            }
            
            if(user.Countrycode == '' || user.Countrycode == null || user.Countrycode.contains('--None--')){
                ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, 'Country should not be blank.');
                ApexPages.addMessage(msgareaOfF);
                return null;
            }
            
            //this condition will run in case of test class only
            if(Test.isRunningTest()){
                registrationSource='idms';
            }
            
            //need to uncomment for standard country and state changes
            user.country = user.countrycode;
            if(user.statecode == null){
                user.statecode = '';
            }else{
                user.state = user.statecode;  
            }  
            user.company_country__c            = userCountryState.countryCode;
            user.company_state__c              = userCountryState.stateCode;
            user.IDMS_Profile_update_source__c = registrationSource; 
            user.IDMS_PreferredLanguage__c     = preflang;
            
            if(user.IDMS_User_Context__c.equalsignorecase('Work') || user.IDMS_User_Context__c.equalsignorecase('@Work')){
                user.IDMSCompanyHeadquarters__c = headQ;
            }
            if(checkedEmailOpt){
                user.IDMS_Email_opt_in__c = 'y';
            }else{
                user.IDMS_Email_opt_in__c = 'u';
            }
            try{
                user.IDMS_User_Context__c = userContext;
                IDMSResponseWrapper data;
                IDMSUpdateUser obj        = new IDMSUpdateUser();
                data = obj.updateUser(user);
                //this condition will run in case of test class only
                if(Test.isRunningTest()){
                    data.status = testStringval;
                }
                //end test condition  
                if(data.status.equalsIgnoreCase('error')){
                    errorMsg                    = 'update.fail';
                    ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, data.message);
                    ApexPages.addMessage(msgntChkd );
                    
                    return null;
                }
                else{
                    //redirectToApp = true;
                    errorMsg = 'update.success';
                    return null;
                }
            }
            catch(exception e){
                errorMsg = 'update.fail';                
                return null;
            }
        } catch(DmlException e) {
            errorMsg = 'update.fail';
            return null;
        }
        return null;
    }
    
    public PageReference saveEmail() {
        //validation for Email to check correct format
        boolean isIdentypeMobile = false;
        //validation check if email already exist
        if(user.IDMSIdentityType__c == 'Email'){
            isIdentypeMobile=false;
        }else{
            isIdentypeMobile = true;
        }
        if(string.isblank(emailMobileVal)){
            errorMsg = 'input.error';
            return null;            
        }
        IDMSUpdateUser objUpdateUser = new IDMSUpdateUser();
        List<User> existingContacts = [SELECT id, email FROM User WHERE email = :emailMobileVal or mobilephone =:emailMobileVal];
        if(existingContacts.size()>0)
        {
            if(!isIdentypeMobile){
                errorMsg='email.error';
                return null;
            }else
            {
                errorMsg = 'mobile.error';
                return null;
            }
        }        
        IDMSResponseWrapper res=new IDMSResponseWrapper();
        //calling the request email change method method
        try{
            String id      = (String)user.id;
            boolean status = false;
            string idType  = '';
            
            if(user.IDMSIdentityType__c == 'Email'){
                user.email        = emailMobileVal;
                idType            = 'Your Email';
                String PhValid    = Label.CLJUN16IDMS30;
                Pattern mypattern = Pattern.compile(PhValid);
                Matcher myMatcher = mypattern.matcher(emailMobileVal);
                boolean result    = myMatcher.matches();
                If(result == false){
                    errorMsg = 'email.invalid';
                    return null;
                }                
            }
            if(user.IDMSIdentityType__c == 'Mobile'){
                user.mobilephone  = emailMobileVal;
                idType            = 'Your Phone';
                isIdentypeMobile  = true;
                String PhValid    = Label.CLJUN16IDMS31;
                Pattern mypattern = Pattern.compile(PhValid);
                Matcher myMatcher = mypattern.matcher(emailMobileVal);
                boolean result    = myMatcher.matches();
                If(result == false){
                    errorMsg = 'mobile.invalid';
                    return null;
                }
            }
            user.IDMS_Profile_update_source__c = registrationSource;
            user.IDMS_User_Context__c          = userContext;
            user.idmsapplanguage__c= language;
            res            = objUpdateUser.updateUser(user);
            system.debug(res);
            if(res.status.equalsignorecase('success')){
                status = true;
            }
            
            //this condition will run in case of test class only
            if(Test.isRunningTest()){
                status = testClassVar;
            }
            //end test condition   
            if(status == true){
                errorMsg = 'update.success';
                if(isIdentypeMobile){
                    PageReference pg = Page.IDMSConfirmPin;
                    pg.getParameters().put('app', ApexPages.currentPage().getParameters().get('app'));
                    pg.getParameters().put('id', user.id);
                    pg.getParameters().put('key', Label.CLQ316IDMS005);
                    pg.setRedirect(true);
                    system.debug(pg);
                    return pg;
                }else{
                    //redirectToApp = true;
                }
                system.debug(redirectToApp+'   '+appUrl);
                return null;
            }
            else{
                errorMsg = 'update.fail';  
                
                if(res.Message.contains('New username already present')){
                
                errorMsg = 'emailupdate.fail';
                }
                              
                return null;
            }
        }
        catch(exception e){
            errorMsg = 'update.fail';  
                 
            return null;
        }
        return null;
    }
    
    // used for setting change password link
    public PageReference changePassword() {
        PageReference pgPasswordUpdate = Page.IDMSPasswordUpdate;
        pgPasswordUpdate.getParameters().put('app', ApexPages.currentPage().getParameters().get('app'));
        pgPasswordUpdate.setRedirect(true);
        system.debug(pgPasswordUpdate);
        return pgPasswordUpdate;
    }
    //used for setting change identity link
    public PageReference changeEmailMobilePage(){
        PageReference objChangeEmail = new PageReference(Page.ChangeEmailMobile+'?app='+registrationSource);        
        changeIdentityLink = objChangeEmail.getUrl();
        return Page.ChangeEmailMobile;
    }
    
    //used for setting cancel button link
    public PageReference cancelButton(){   
        system.debug('In cancel button');
        IDMSApplicationMapping__c appMap1 = new IDMSApplicationMapping__c();
        appMap1=IDMSApplicationMapping__c.getInstance(appid);
        string appid1;
        if(appMap1 != null){
            appid1 = (string)appMap1.get('appid__c');
            system.debug('In cancel button'+appid1);
        }
        PageReference pg = new PageReference('/identity/idp/login');
        pg.getParameters().put('app', appid1);
        pg.setRedirect(true);
        system.debug(pg);
        return pg;
    }
    
    //used for update password
    public PageReference updatePassword() {
        if(oldPassword == '')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS51)); 
            return null;
        }
        if(!Pattern.matches(Label.CLJUN16IDMS48, newPassword ))
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS49)); 
            return null;
        }
        if(verifyNewPassword!=newPassword )
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, Label.CLJUN16IDMS50)); 
            return null;
        }
        try{
            resetpwdFlag = true;
            //this condition will run in case of test class only
            if(Test.isRunningTest()){
                String sa = 'testrun';
            }
            //end test condition  
            String sa = [select  Assertion__c from USerSAMLAssertion__c where UserId__c=:user.id].Assertion__c;
            //need to uncomment once got samlAssertion
            IDMSUIMSWebServiceCalls idmsUimsApiCalls = new IDMSUIMSWebServiceCalls();
            boolean res = idmsUimsApiCalls.updatePassword(oldPassword,newPassword,sa);
            //this condition will run in case of test class only
            if(Test.isRunningTest()){
                res=testClassVar;
            }
            //end test condition   
            if(res == true){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Your password has been successfully updated.'));
                redirectToApp = true;
                return null;
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Password update failed, please enter correct Existing Password.')); 
                return null;
            }
        }
        catch(exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Password update failed, please try after some time.')); 
            return null;
        }
    }     
    
    //method to update email
    public PageReference updateEmail() {
        IDMSResponseWrapper data;
        IDMSUIMSWebServiceCalls idmsUimsApiCalls = new IDMSUIMSWebServiceCalls();
        boolean isupdated = idmsUimsApiCalls.uimsUpdateEmail(Label.CLJUN16IDMS121,authToken);
        //this condition will run in case of test class only
        if(Test.isRunningTest()){
            isupdated=testClassVar;
        }
        //end test condition    
        if(isupdated==true){
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Your Email id has been successfully updated.');
            ApexPages.addMessage(msgntChkd );
            redirectToApp=true;
            return null;
        }else{
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Email update failed due to some error, please try after some time');
            ApexPages.addMessage(msgntChkd );
            return null;
        }
    }
    
    public pagereference refreshPage(){
    
    system.debug(selectedVal);
        
        langCode=[select name from IDMSAvailableLanguages__c where language__c=:selectedVal].name;
        
        system.debug(langCode);
        system.debug(currentPageURL);
        
        pagereference pg=new pagereference(currentPageURL);
        pg.getParameters().put('lang',langCode);
        pg.setRedirect(true);
        system.debug(pg);
        return pg;
    
    }
    
    public List<SelectOption> getoptns(){
        
        List<IDMSAvailableLanguages__c> appmap= [select language__c,name from IDMSAvailableLanguages__c];
        list<string> list1=new list<string>();
        list<string> list2=new list<string>();
        list1.add('Select Language');
        list2.add('Select Language');
        for (integer i=0;i<appmap.size();i++){
            IDMSAvailableLanguages__c obj= appmap.get(i);
            list1.add(obj.language__c);
            list2.add(obj.name);
        }
        
        List<SelectOption> optns = new List<Selectoption>();
        for(integer i=0;i<list1.size();i++){
        optns.add(new SelectOption(list2.get(i),list1.get(i)));
        
            }
        return optns;
        }
    
}