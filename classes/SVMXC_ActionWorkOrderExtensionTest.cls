@isTest(SeeAllData=true)
private class SVMXC_ActionWorkOrderExtensionTest { 

    static testMethod void testAcceptOrRejectWebAccept() {
    
        //profile used
        Profile profile = [select id from profile where name like: Label.CLCCCMAR120001];  
        
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
        insert user;
    
        //Create Country
        //Country__c Ctry = Utils_TestMethods.createCountry(); 
        //insert Ctry;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.Type ='SA';
        insert acc;
        
        SVMXC__Site__c site = new SVMXC__Site__c(name= 'site1');
        site.SVMXC__Account__c = acc.Id;
        insert site;
        
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c(name='testsc');
        insert sc;

        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm1;
                
        RecordType rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c' and DeveloperName ='Technician'];
        RecordType rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c' and DeveloperName = 'Technican'];  
 
        st = new SVMXC__Service_Group__c(RecordTypeId =rts.Id, SVMXC__Active__c = true,Name = 'Test Service Team');
        insert st;
         
        sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =rtssg.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = user.Id,
                                                            Send_Notification__c = 'Email',
                                                            SVMXC__Email__c = 'sgm1.test@company.com',
                                                            SVMXC__Phone__c = '(506) 878-2887'
                                                            );
        insert sgm1;
    
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
        workOrder.Work_Order_Category__c='On-site';   
        workOrder.SVMXC__Order_Type__c='Maintenance';
        workOrder.WorkOrderSubType__c='3-Day Workshop'; 
        workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
        workOrder.SVMXC__Order_Status__c = 'UnScheduled';
        workOrder.CustomerRequestedDate__c = Date.today();
        workOrder.Service_Business_Unit__c = 'Energy';
        workOrder.CustomerTimeZone__c='Asia/Baghdad';
        workOrder.SVMXC__Priority__c = 'Normal-Medium';
        workOrder.SendAlertNotification__c = true;
        workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder.SVMXC__Site__c = site.id;
        workOrder.SVMXC__Company__c= acc.id;
        workOrder.SVMXC__Service_Contract__c = sc.id;
        insert workOrder;
        
        workOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
        workOrder.BackOfficeReference__c = '123';
        
        Test.startTest();
        update workOrder;
           
        AssignedToolsTechnicians__c technician1 = Utils_TestMethods.CreateAssignedTools_Technician(workOrder.id, sgm1.id);
        technician1.Status__c ='Assigned';
        
        insert technician1;
        
        
        AssignedToolsTechnicians__c att = [SELECT Sites_Access_Token__c FROM AssignedToolsTechnicians__c 
                                           WHERE Id =: technician1.Id];
        
        ApexPages.StandardController controller = new ApexPages.StandardController(att);       
        SVMXC_ActionWorkOrderExtension awoe = new SVMXC_ActionWorkOrderExtension(controller);
  
        List<SelectOption> selOpts = awoe.getRejectionReasons();
        //List<SelectOption> selOpts2 = awoe.getActions();
        
        awoe.throwExceptionMsg('test', 'warning');
        awoe.throwExceptionMsg('test', 'info');
        awoe.throwExceptionMsg('test', 'error');
        
        try{
        	awoe.init();
        }catch(Exception ex) {
        	System.debug('### Expected error : ' + ex);
        }
        ApexPages.currentPage().getParameters().put('token', att.Sites_Access_Token__c);
        awoe.initRunAlready = false; 
        awoe.init();
        
        for (AssignedToolsTechnicians__c att1 : awoe.attRecords) {
			att1.ReasonForRejection__c = 'Planner Error'; 
        }
        
        try{
        	awoe.save();
        }catch(Exception ex) {
        	System.debug('### Expected error : ' + ex);
        }
        
        for (AssignedToolsTechnicians__c att1 : awoe.attRecords) {
			att1.Status__c = 'Accepted'; 
        }
        awoe.save();
        
        //awoe.actionValue = 'Accepted';
        //awoe.changedRadio();
        
        
                
        Test.stopTest();   
    }

/*    
    static testMethod void testBadToken() {
        
        //profile used
        Profile profile = [select id from profile where name like: Label.CLCCCMAR120001];  
        
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
        insert user;
    
        //Create Country
        //Country__c Ctry = Utils_TestMethods.createCountry(); 
        //insert Ctry;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.Type ='SA';
        insert acc;
        
        SVMXC__Site__c site = new SVMXC__Site__c(name= 'site1');
        site.SVMXC__Account__c = acc.Id;
        insert site;
        
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c(name='testsc');
        insert sc;

        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm1;
                
        RecordType rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c' and DeveloperName ='Technician'];
        RecordType rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c' and DeveloperName = 'Technican'];  
 
        st = new SVMXC__Service_Group__c(RecordTypeId =rts.Id, SVMXC__Active__c = true,Name = 'Test Service Team');
        insert st;
         
        sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =rtssg.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = user.Id,
                                                            Send_Notification__c = 'Email',
                                                            SVMXC__Email__c = 'sgm1.test@company.com',
                                                            SVMXC__Phone__c = '(506) 878-2887'
                                                            );
        insert sgm1;
    
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
        workOrder.Work_Order_Category__c='On-site';   
        workOrder.SVMXC__Order_Type__c='Maintenance';
        workOrder.WorkOrderSubType__c='3-Day Workshop'; 
        workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
        workOrder.SVMXC__Order_Status__c = 'UnScheduled';
        workOrder.CustomerRequestedDate__c = Date.today();
        workOrder.Service_Business_Unit__c = 'Energy';
        workOrder.CustomerTimeZone__c='Asia/Baghdad';
        workOrder.SVMXC__Priority__c = 'Normal-Medium';
        workOrder.SendAlertNotification__c = true;
        workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder.SVMXC__Site__c = site.id;
        workOrder.SVMXC__Company__c= acc.id;
        workOrder.SVMXC__Service_Contract__c = sc.id;
        insert workOrder;

        workOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
        workOrder.BackOfficeReference__c = '123';
        Test.startTest();
        update workOrder;
           
        AssignedToolsTechnicians__c technician1 = Utils_TestMethods.CreateAssignedTools_Technician(workOrder.id, sgm1.id);
        technician1.Status__c ='Assigned';
        
        insert technician1;
        
        
        
        AssignedToolsTechnicians__c att = [SELECT Sites_Access_Token__c FROM AssignedToolsTechnicians__c 
                                           WHERE Id =: technician1.Id];
        
        ApexPages.StandardController controller = new ApexPages.StandardController(att);       
        SVMXC_ActionWorkOrderExtension awoe = new SVMXC_ActionWorkOrderExtension(controller);
 
        awoe.init();
        
        ApexPages.currentPage().getParameters().put('token', 'bad token');
        awoe.init();
        
        ApexPages.currentPage().getParameters().put('token', att.Sites_Access_Token__c);
        awoe.init();
     
        Test.stopTest();                 
    }
    
    static testMethod void testAcceptOrRejectWebReject() {
    
        //profile used
        Profile profile = [select id from profile where name like: Label.CLCCCMAR120001];  
        
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com');
        insert user;
    
        //Create Country
        //Country__c Ctry = Utils_TestMethods.createCountry(); 
        //insert Ctry;
        
        Account acc = Utils_TestMethods.createAccount();
        acc.Type ='SA';
        insert acc;
        
        SVMXC__Site__c site = new SVMXC__Site__c(name= 'site1');
        site.SVMXC__Account__c = acc.Id;
        insert site;
        
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c(name='testsc');
        insert sc;

        SVMXC__Service_Group__c st;
        SVMXC__Service_Group_Members__c sgm1;
                
        RecordType rts = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group__c' and DeveloperName ='Technician'];
        RecordType rtssg = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = 'SVMXC__Service_Group_Members__c' and DeveloperName = 'Technican'];  
 
        st = new SVMXC__Service_Group__c(RecordTypeId =rts.Id, SVMXC__Active__c = true,Name = 'Test Service Team');
        insert st;
         
        sgm1 = new SVMXC__Service_Group_Members__c(RecordTypeId =rtssg.Id,SVMXC__Service_Group__c =st.id,
                                                            SVMXC__Role__c = 'Schneider Employee',
                                                            SVMXC__Salesforce_User__c = user.Id,
                                                            Send_Notification__c = 'Email',
                                                            SVMXC__Email__c = 'sgm1.test@company.com',
                                                            SVMXC__Phone__c = '(506) 878-2887'
                                                            );
        insert sgm1;
    
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
        workOrder.Work_Order_Category__c='On-site';   
        workOrder.SVMXC__Order_Type__c='Maintenance';
        workOrder.WorkOrderSubType__c='3-Day Workshop'; 
        workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
        workOrder.SVMXC__Order_Status__c = 'UnScheduled';
        workOrder.CustomerRequestedDate__c = Date.today();
        workOrder.Service_Business_Unit__c = 'Energy';
        workOrder.CustomerTimeZone__c='Asia/Baghdad';
        workOrder.SVMXC__Priority__c = 'Normal-Medium';
        workOrder.SendAlertNotification__c = true;
        workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
        workOrder.SVMXC__Site__c = site.id;
        workOrder.SVMXC__Company__c= acc.id;
        workOrder.SVMXC__Service_Contract__c = sc.id;
        insert workOrder;

        workOrder.SVMXC__Order_Status__c = 'Customer Confirmed';
        workOrder.BackOfficeReference__c = '123';
        Test.startTest();
        update workOrder;
           
        AssignedToolsTechnicians__c technician1 = Utils_TestMethods.CreateAssignedTools_Technician(workOrder.id, sgm1.id);
        technician1.Status__c ='Assigned';
        
        insert technician1;
        
        
        
        AssignedToolsTechnicians__c att = [SELECT Sites_Access_Token__c FROM AssignedToolsTechnicians__c 
                                           WHERE Id =: technician1.Id];
        
        ApexPages.StandardController controller = new ApexPages.StandardController(att);       
        SVMXC_ActionWorkOrderExtension awoe = new SVMXC_ActionWorkOrderExtension(controller);
 
        ApexPages.currentPage().getParameters().put('token', att.Sites_Access_Token__c);
        awoe.init();
        
        awoe.actionValue = 'Rejected';
        awoe.fsrResponse = 'Wont be able to make it because of illness';
        awoe.changedRadio();
        awoe.save(); 
        
        awoe.rejectionReasonValue = 'Illness';
        awoe.save();
                
        Test.stopTest();        
    }
*/    
}