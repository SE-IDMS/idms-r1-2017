@isTest
private class VFC_MySEUserIdentification_TEST{
static testMethod void testVFC_MySEUserIdentification_TestMethod() {
    
    Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.City__c ='New City';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.Email = 'test@test.com';
        insert testcontact;
        

    Case objCase = Utils_TestMethods.createCase(testAccount.Id,testcontact.Id,'Open');
    insert objCase;
    
    DigitalToolsUserInformation__c mySE = new DigitalToolsUserInformation__c();
    mySE.Contact__c=testcontact.Id;
    mySE.CasePromoted__c=objCase.Id;
    mySE.isActive__c= True;
    insert mySE;
    
    VFC_MySEUserIdentification.wrapperclass wrap = new VFC_MySEUserIdentification.wrapperclass(mySE);
    wrap.checkbox=True;
    
    objCase.LastAgentPromoted__c =UserInfo.getUserId();
    objCase.CasePromoted__c=  True;
    update objCase;
    
    PageReference pref = Page.VFP_MySEUserIdentification;      
    pref.getParameters().put('id',objCase.id);
    Test.setCurrentPage(pref);

    Test.startTest();
    ApexPages.StandardController CaseStandardController = new ApexPages.StandardController(objCase);
    VFC_MySEUserIdentification MySEuserInfo = new VFC_MySEUserIdentification(CaseStandardController);
    MySEuserInfo.save();
    Test.stopTest();
   
        
    }
}