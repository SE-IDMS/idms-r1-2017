/**************************************************************************************
    Author: Fielo Team 
    Date: 09/04/2015
    Description:  
    Related Components: -
***************************************************************************************/
public with sharing class FieloPRM_AP_PermissionSetController {
    
    public transient blob documentBody {get; set;}
    public string permissionSetId {get;set;}
    public Boolean exportToTXT {get;set;}
    public string jsonString {get;set;}
    public string fileName {get; set;}
    public string fileTXTName {get;set;}
    
    public Boolean importObject {get;set;}
    public Boolean importField {get;set;}
    public Boolean importEntity {get;set;}

    private string psetName;
    
    public FieloPRM_AP_PermissionSetController(){
        exportToTXT = false;
        importObject = true;
        importField = true;
        importEntity= true;
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        for (PermissionSet ps : [SELECT Id, Name, Label FROM PermissionSet ORDER BY Label]) {
            options.add(new SelectOption(ps.Id, ps.Label));
        }
        return options;
    }
    
    public void doRetrieve() {
        if(permissionSetId != null){
            PermissionSet pset = [SELECT Id, Description, IsOwnedByProfile, Label, Name, NamespacePrefix, ProfileId, UserLicenseId,
                                 (SELECT Id, ParentId, PermissionsCreate, PermissionsDelete, PermissionsEdit, PermissionsModifyAllRecords, PermissionsRead, PermissionsViewAllRecords, SobjectType from ObjectPerms),
                                 (SELECT Id, Field, ParentId, PermissionsEdit, PermissionsRead, SobjectType FROM FieldPerms),
                                 (SELECT Id, ParentId, SetupEntityId, SetupEntityType FROM SetupEntityAccessItems)
                                  FROM PermissionSet WHERE Id =: permissionSetId];
            
            PermissionSetWrapper ps = new PermissionSetWrapper();
            ps.perSet = pset;
            
            set<String> setIds = new set<String>();
            for(SetupEntityAccess sea: pset.SetupEntityAccessItems){ setIds.add(sea.SetupEntityId); }
            
            ps.mapIdName = new map<String, String>();
            for(ApexClass ac: [SELECT Id, Name FROM ApexClass WHERE Id IN: setIds]){ ps.mapIdName.put(ac.id, ac.Name); }
            for(ApexPage ap: [SELECT Id, Name FROM ApexPage WHERE Id IN: setIds]){ ps.mapIdName.put(ap.id, ap.Name); }
            
            jsonString = JSON.serializePretty(ps);
            psetName = pset.Name;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Permission Set: \"' + pset.Name + '\", ObjectPerms: ' + pset.ObjectPerms.size() + ', FieldPerms: ' + pset.FieldPerms.size() + ', SetupEntityAccessItems: ' + pset.SetupEntityAccessItems.size()));
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Plese select Permission Set'));
        }
    }
    
    public void doDeploy() {
        
        if(jsonString != null && jsonString != ''){
        
            //Deserialize del Permission Set con todos los objetos relacionados
            PermissionSetWrapper ps = (PermissionSetWrapper)JSON.deserializeStrict(jsonString, PermissionSetWrapper.class);
            
            PermissionSet pSetToUpdate = ps.perSet;
            map<String, String> mapNameId = new map<String, String>();
            if(importEntity){
                for(ApexClass ac: [SELECT Id, Name FROM ApexClass WHERE Name IN: ps.mapIdName.values()]){ mapNameId.put('ApexClass' +  ac.Name, ac.Id); }
                for(ApexPage ap: [SELECT Id, Name FROM ApexPage WHERE Name IN: ps.mapIdName.values()]){ mapNameId.put('ApexPage' +  ap.Name, ap.Id); }
            }

            //Permission Set en la ORG
            List<PermissionSet> currentPSetList = [SELECT Id, Description, IsOwnedByProfile, Label, Name, NamespacePrefix, ProfileId, UserLicenseId, 
                (SELECT Id FROM ObjectPerms), 
                (SELECT Id FROM FieldPerms), (SELECT Id FROM SetupEntityAccessItems) 
                FROM PermissionSet WHERE Name =: pSetToUpdate.Name];
            
            //Mapeo del Permission Set
            PermissionSet psToUpsert;

            psToUpsert = !currentPSetList.isEmpty() ? currentPSetList[0] : new PermissionSet();
            /*if(!currentPSetList.isEmpty()){
                psToUpsert = currentPSetList[0];
            }else{
                psToUpsert = new PermissionSet();
            }*/
            
            psToUpsert.Description = pSetToUpdate.Description;
            psToUpsert.Label = pSetToUpdate.Label;
            psToUpsert.Name = pSetToUpdate.Name;
            
            Savepoint sp = Database.setSavePoint();
            
            try{
                //Actualizacion del Permission Set
                upsert psToUpsert;
                system.debug('psToUpsert: ' + psToUpsert);  
                //Delete de los FieldPerms existentes
                system.debug('psToUpsert.FieldPerms: ' + psToUpsert.FieldPerms);
                //if(!psToUpsert.FieldPerms.isEmpty()){delete psToUpsert.FieldPerms;}
                
                map<String,FieldPermissions > mapCodeFieldPermission = new map<String,FieldPermissions >();
                
                if(importField){
                    if(!psToUpsert.FieldPerms.isEmpty()){
                        list<FieldPermissions> listFieldPermissions = [SELECT Id, Field, ParentId, PermissionsEdit, PermissionsRead, SobjectType FROM FieldPermissions WHERE ParentId = : psToUpsert.id ];
                        system.debug('1' + listFieldPermissions[0].ParentId + listFieldPermissions[0].Field +  listFieldPermissions[0].SobjectType);
                        for(FieldPermissions fieldPer : listFieldPermissions){
                            mapCodeFieldPermission.put(fieldPer.ParentId + fieldPer.Field +  fieldPer.SobjectType,fieldPer);
                        }
                    }
                }
                
                //Delete de los SetupEntityAccessItems existentes
                //if(!psToUpsert.SetupEntityAccessItems.isEmpty()){delete psToUpsert.SetupEntityAccessItems;}
                
                //Delete de los ObjectPerms existentes
                if (!psToUpsert.ObjectPerms.isEmpty() && importObject) {
                    List<DataBase.DeleteResult> deleteResult = DataBase.Delete(psToUpsert.ObjectPerms, false);
                    //Elimino los dependientes
                    List<ObjectPermissions> listObjPermissionsToDelete2 = new List<ObjectPermissions>();
                    for(integer i = 0; i < deleteResult.size(); i++) {
                        if (!deleteResult[i].isSuccess()) {listObjPermissionsToDelete2.add(psToUpsert.ObjectPerms[i]);}
                    }
                    if (!listObjPermissionsToDelete2.isEmpty()) {delete listObjPermissionsToDelete2;}
                }
                
                //Mapeo de los FieldPermissions
                map<string, FieldPermissions> listNewFieldPermissions = new map<string, FieldPermissions>();
                
                if(importField){
                    for(FieldPermissions fps: pSetToUpdate.FieldPerms){

                        FieldPermissions fpsAux = new FieldPermissions();
                        if(mapCodeFieldPermission.containsKey(psToUpsert.Id + fps.Field +  fps.SobjectType)){
                            fpsAux = mapCodeFieldPermission.get(psToUpsert.Id + fps.Field +  fps.SobjectType);
                            fpsAux.PermissionsEdit = fps.PermissionsEdit;
                            fpsAux.PermissionsRead = fps.PermissionsRead;
                            //listNewFieldPermissions.add(fpsAux);
                        }else{
                            fpsAux = new FieldPermissions( ParentId = psToUpsert.Id, Field = fps.Field, PermissionsEdit = fps.PermissionsEdit, PermissionsRead = fps.PermissionsRead , SobjectType = fps.SobjectType );
                        }
                        mapCodeFieldPermission.put(fpsAux.ParentId + fpsAux.Field +  fpsAux.SobjectType,fpsAux);
                        listNewFieldPermissions.put(fpsAux.ParentId + fpsAux.Field +  fpsAux.SobjectType,fpsAux);
                        
                    }
                }
                
                if(importEntity){
                    //system.debug('query: ' + [SELECT id FROM SetupEntityAccess WHERE ParentId = :psToUpsert.Id ]);
                    list<SetupEntityAccess> listSetepEntity = [SELECT id, ParentId, SetupEntityId FROM SetupEntityAccess WHERE ParentId = :psToUpsert.Id ];
                    set<String> setParentId = new set<String>();
                    if(listSetepEntity.size() > 0){
                        for(SetupEntityAccess setupentity : listSetepEntity){
                            string keyAux = String.valueOf(setupentity.ParentId) + String.valueOf(setupentity.SetupEntityId) ; 
                            setParentId.add(keyAux);
                        }
                    }
                    //Mapeo de los SetupEntityAccess
                    List<SetupEntityAccess> listNewSetupEntityAccess = new List<SetupEntityAccess>();
                    for(SetupEntityAccess sas: pSetToUpdate.SetupEntityAccessItems){
                        if(ps.mapIdName.containsKey(sas.SetupEntityType + sas.SetupEntityId) && mapNameId.containsKey(ps.mapIdName.get(sas.SetupEntityType + sas.SetupEntityId))){
                            string keyAux = String.valueOf(psToUpsert.Id) + String.valueOf(mapNameId.get(ps.mapIdName.get(sas.SetupEntityType + sas.SetupEntityId))) ; 
                            if(!setParentId.contains(keyAux)){
                                listNewSetupEntityAccess.add(new SetupEntityAccess( ParentId = psToUpsert.Id, SetupEntityId = mapNameId.get(ps.mapIdName.get(sas.SetupEntityId)) ));    
                            }
                            
                        }
                    }
                    if(listNewSetupEntityAccess.size() > 0){
                        insert listNewSetupEntityAccess;
                    }
                }

                if(importObject){
                    //Mapeo de los ObjectPermissions
                    List<ObjectPermissions> listNewObjectPermissions = new List<ObjectPermissions>();
                    for(ObjectPermissions ops: pSetToUpdate.ObjectPerms){
                        listNewObjectPermissions.add(new ObjectPermissions( ParentId = psToUpsert.Id, PermissionsCreate = ops.PermissionsCreate, PermissionsDelete = ops.PermissionsDelete, PermissionsEdit = ops.PermissionsEdit, PermissionsModifyAllRecords = ops.PermissionsModifyAllRecords, PermissionsRead = ops.PermissionsRead, PermissionsViewAllRecords = ops.PermissionsViewAllRecords, SobjectType = ops.SobjectType ));
                    }
                    
                    //Insert de los ObjectPermissions
                    List<DataBase.SaveResult> listNewObjectPermissionsResult = DataBase.insert(listNewObjectPermissions, false);
                    List<ObjectPermissions> listNewObjectPermissions2 = new List<ObjectPermissions>();
                    for(integer i = 0; i < listNewObjectPermissionsResult.size(); i++) {
                        if (!listNewObjectPermissionsResult[i].isSuccess()) { listNewObjectPermissions2.add(listNewObjectPermissions[i].clone(false)); }
                    }
                    if (!listNewObjectPermissions2.isEmpty()) {insert listNewObjectPermissions2;}
                }
                
                //Insert de los SetupEntityAccess
                
                
                
                //Insert de los FieldPermissions
                /*system.debug('TOKEN listNewFieldPermissions: ' + listNewFieldPermissions.values().size());
                system.debug('TOKEN listNewFieldPermissions.values()[0]: ' + listNewFieldPermissions.values()[0]);
                system.debug('TOKEN listNewFieldPermissions.values()[1]: ' + listNewFieldPermissions.values()[1]);*/
                
                /*integer i = 0;
                list<FieldPermissions> listauxinsert =new list<FieldPermissions>();
                for(FieldPermissions aux :listNewFieldPermissions.values()){
                    i++;
                    if(aux.id != null && aux.id != ''){
                        system.debug('id nº' + i + ' : ' + aux.id);
                        system.debug('aux nº' + i + ' : ' + aux);
                    }
                    if(i<11){
                        system.debug('id nº' + i + ' : ' + aux.id);
                        system.debug('aux nº' + i + ' : ' + aux);
                        listauxinsert.add(aux);
                    }
                }*/
                upsert listNewFieldPermissions.values();
                //upsert listauxinsert;
                system.debug('after insert: ' );
                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Deployed!'));
            }catch(Exception e){
                //Database.rollback(sp);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getStackTraceString()));
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'JSON Empty'));
        }
    }
    
    public void doExport(){
        if(jsonString != null && jsonString != ''){
            exportToTXT = true;
            fileTXTName = 'text/plain/#'+ psetName + ' - ' + Datetime.now() +'.txt';
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'JSON Empty'));
        }
    }
    
    public void doImport(){
        if(documentBody != null){
            try{jsonString = documentBody.toString();}catch(Exception e){}
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Plese select a file'));
        }
        documentBody = null;
    }
    
    public class PermissionSetWrapper {
        public PermissionSet perSet { get; set; }
        public map<String, String> mapIdName { get; set; }
    }
    
}