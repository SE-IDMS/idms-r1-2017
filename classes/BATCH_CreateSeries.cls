/*
    Description: Batch class to create Opportunity Series if the Opportunity count > 25
*/
global class BATCH_CreateSeries implements Database.Batchable<sObject>,Database.Stateful
{
    //Variables Declaration
    String agreementId;

    global BATCH_CreateSeries(String agrId)
    {
        agreementId = agrId;
    }
    //Start method
    global Database.querylocator start(Database.BatchableContext BC)
    {
        //QUERIES THE AGREEMENT
        return Database.getQueryLocator([SELECT PhaseSalesStage__c, TECH_IsCreateByBatch__c,SignatureDate__c, FrameOpportunityInterval__c,CurrencyIsoCode,AgreementTimeSpan__c,AgreementValidfrom__c,AgreementValidUntil__c,Amount__c,AgreementType__c FROM OppAgreement__c WHERE ID =:agreementId]);
    }
    
    global void execute(Database.BatchableContext BC, List<OppAgreement__c> agreement)
    {
        system.debug('**** In Execute Method ****' + agreement );
        //Iterates through the Agreement, Calls the createSeries class and creates series
        for(OppAgreement__c op: agreement)
        {
            if(agreement[0].TECH_IsCreateByBatch__c == true)
            {
                VFC20_CreateSeries creSer = new VFC20_CreateSeries(new ApexPages.StandardController(op)); 
                creser.isSeriesFromBatch= true;
                creSer.createSeries();       
            }
        }
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
        system.debug('**** In Finish Method ****' );
    }
}