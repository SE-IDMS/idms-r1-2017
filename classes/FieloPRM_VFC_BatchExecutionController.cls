/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 08/08/2016
* Description: 
********************************************************************/

public with sharing class FieloPRM_VFC_BatchExecutionController {

    //PUBLIC ATTRIBUTES
    public list<BatchJobWrapper> listBatchs {get;set;}
    public string batchToexecute {get;set;}
    public BatchJobWrapper batchToRun {get;set;}
    public List<FieloPRM_BatchManagementConfiguration__c> batchs {get;set;}
    public Integer batchSize {get; set;}
    
    public FieloPRM_VFC_BatchExecutionController(ApexPages.StandardController controller){
        batchSize = 200;
        FieloPRM_BatchManagementView__c batchView = (FieloPRM_BatchManagementView__c)controller.getRecord();
        batchs = [SELECT id, Name,  F_PRM_BatchName__c, F_PRM_Prefix__c, F_PRM_Description__c FROM FieloPRM_BatchManagementConfiguration__c WHERE F_PRM_BatchManagementView__c =: batchView.id ORDER BY F_PRM_Order__c];
        getJobs();
    }
    
    public void getJobs(){
        listBatchs = new list<BatchJobWrapper>();
        for(FieloPRM_BatchManagementConfiguration__c b : batchs){
            List<AsyncApexJob> BatchsJobs = [SELECT TotalJobItems, Status, NumberOfErrors, ExtendedStatus, JobType, JobItemsProcessed, CompletedDate FROM AsyncApexJob WHERE ApexClass.Name = :b.F_PRM_BatchName__c AND JobType = 'BatchApex' ORDER BY CompletedDate DESC LIMIT 1];
            if(test.isrunningTest()){
                AsyncApexJob asyncApex = new AsyncApexJob();
                BatchsJobs.add(asyncApex);
            }
            loadWrapper(BatchsJobs, b);
        }
    }
    
    private void loadWrapper(List<AsyncApexJob> batchJobs, FieloPRM_BatchManagementConfiguration__c b){
        BatchJobWrapper bjw =  new BatchJobWrapper();
        bjw.name = b.name;
        if(!String.isEmpty(b.F_PRM_Prefix__c)){
            bjw.batchName = b.F_PRM_Prefix__c + '.' + b.F_PRM_BatchName__c;
        }else{
            bjw.batchName = b.F_PRM_BatchName__c;  
        }
        
        bjw.description = b.F_PRM_Description__c;
        if(!batchJobs.isEmpty()){
            AsyncApexJob job = batchJobs.get(0);
            Double totalItems = job.TotalJobItems;
            Double itemsProcessed = job.JobItemsProcessed;
            bjw.job = job;

            if(job.Status == 'Queued' || job.Status == 'Preparing'){
                bjw.percentComplete = 0;
            }else if(totalItems != null){
                if(totalItems == 0){
                    //A little check here as we don't want to divide by 0.
                    bjw.percentComplete = 100;
                }else{
                    bjw.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
                }
            }
        }
        listBatchs.add(bjw);
    }
    
    public void executeb(){
        if(batchToexecute != null){
            Type t = Type.forName(batchToexecute);
            if(batchSize < 0 || batchSize > 200){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Batch size can be from 1 to 200'));
                return;
            }
            if(t != null){
                Id batchJobId = Database.executeBatch((Database.Batchable<sObject>) t.newInstance(),batchSize);
                getJobs();
            }
        }
    }
    
    //This is the wrapper class the includes the job itself and a value for the percent complete
    public Class BatchJobWrapper{
        public String name {get;set;}
        public String batchName {get;set;}
        public String description {get;set;}
        public AsyncApexJob job {get;set;}
        public Integer percentComplete {get;set;}
    }
    
}