@isTest
private class VFC_PRMHome_TEST
{
    static testMethod void myUnitTest()
    {
        
        Country__c Country = new Country__c (Name='USA', CountryCode__c='US'); 
        insert country;
            
        PRMCountry__c prmCountry = new PRMCountry__c(CountryPortalEnabled__c = true,
                                          CountrySupportEmail__c = 'abc@xyz.com',
                                          PLDatapool__c = 'rn_US',
                                          Country__c = country.id
                                          );
        insert prmCountry;
        
        CS_PRM_ApexJobSettings__c CS_ApexJob = new CS_PRM_ApexJobSettings__c(Name = 'BatchProcessNotifications', InitialSync__c = true, LastRun__c = System.Now());
        insert CS_ApexJob;
        
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
        insert cLChild;
        
        
        CountryChannels__c thisNewCountryChannel = new CountryChannels__c();
        thisNewCountryChannel.Channel__c = cLC.Id;
        thisNewCountryChannel.SubChannel__c = cLChild.id;
        thisNewCountryChannel.Country__c = country.Id;
        thisNewCountryChannel.Active__c = True;
        thisNewCountryChannel.PRMCountry__c = prmCountry.Id;
        insert thisNewCountryChannel;
        
        
        Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345',PRMCountry__c =country.id );
        insert PartnerAcc;
    
        Contact PartnerCon = new Contact(
            FirstName='Test',
            LastName='lastname',
            AccountId=PartnerAcc.Id,
            JobTitle__c='Z3',
            CorrespLang__c='EN',            
            WorkPhone__c='1234567890',
            PRMCountry__c = country.id
            );            
            
            
            Insert PartnerCon;
            
        Id profilesId = System.label.CLMAR13PRM03;  
        User   u1= new User(Username = 'testUserOne@schneider-electric.com', LastName = 'User11', alias = 'tuser1',
                    CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                    Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId, ContactID = PartnerCon.Id, UserPermissionsSFContentUser=true );
        insert u1; 
        
        
        system.runas(u1){
            PageReference pageRefWithretURL = Page.VFP_updateTargetAndSegmentation;
            pageRefWithretURL.getParameters().put(Label.CLOCT14PRM11, '');
            Test.setCurrentPage(pageRefWithretURL);
            //ApexPages.StandardController sdtCon = new ApexPages.StandardController();
            ApexPages.StandardController sdtCon1 = new ApexPages.StandardController(u1);
            //VFC_PRMHome homePage = new VFC_PRMHome(sdtCon1);
            VFC_PRMHome homePage1 = new VFC_PRMHome();
            homePage1.UpdatePartnerRedirectURL();
            homePage1.getRedirectURL();      
             
        }    
        
        //User usr = Utils_TestMethods.createStandardUser('tUsVFC92');
        //Insert usr;
        
    }

    
}