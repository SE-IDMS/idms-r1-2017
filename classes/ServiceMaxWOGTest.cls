@isTest(seeAllData=true)
public class ServiceMaxWOGTest
{
    static testMethod void WOGTest()
    {   
       
       Account acc = Utils_TestMethods.createAccount();
       insert acc;
       
        SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c(Name = 'Test', SVMXC__Company__c = acc.Id);
        insert contract;
        
        SVMXC__Service_Order__c wor =  Utils_TestMethods.createWorkOrder(acc.id);
         wor.SVMXC__Order_Type__c='Maintenance';
         wor.CustomerRequestedDate__c = Date.today();
         wor.BackOfficeReference__c = '111111';
         insert wor;
        
         SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
         workOrder.Work_Order_Category__c='On-site'; 
         workOrder.SoldToAccount__c =    acc.id;
         workOrder.SVMXC__Order_Type__c='Maintenance';
         workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
         workOrder.SVMXC__Order_Status__c = 'UnScheduled';
         workOrder.CustomerRequestedDate__c = Date.today();
         workOrder.Service_Business_Unit__c = 'Energy';
         workOrder.SVMXC__Priority__c = 'Normal-Medium';
         workOrder.SendAlertNotification__c = true;
         workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         workOrder.BackOfficeReference__c = '111111';
         workOrder.SVMXC__Service_Contract__c =contract.Id;
         workOrder.Parent_Work_Order__c =wor.Id;
         insert workOrder;
         
          List<SVMXC__Service_Order__c> wolst = [select Id, Parent_Work_Order__c, SVMXC__Company__c, SoldToAccount__c, SVMXC__Service_Contract__c, BackOfficeReference__c, SVMXC__Order_Status__c from SVMXC__Service_Order__c limit 2];
         
           
        for(SVMXC__Service_Order__c so:wolst)
        {
            so.Parent_Work_Order__c = null;
        }
        ServiceMaxWOG.CreateWOG(wolst);
        WorkOrderGroup__c wog = new WorkOrderGroup__c();
        wog.Account__c = wolst[0].SVMXC__Company__c;
        wog.SoldTo__c = wolst[0].SoldToAccount__c;
        wog.ServiceMaintenanceContract__c = wolst[0].SVMXC__Service_Contract__c;
        wog.BackOfficeReference__c = wolst[0].BackOfficeReference__c;
        wog.WOGName__c = 'Test';
        insert wog;
        
        List<SVMXC__Service_Order__c> solst = new List<SVMXC__Service_Order__c>();
        for(SVMXC__Service_Order__c so:wolst)
        {
            SVMXC__Service_Order__c wo = so.clone();
            wo.WorkOrderGroup__c = wog.id;
            wo.SVMXC__Order_Status__c = 'Cancelled';
            solst.add(wo);
        }
        if(solst.size()>0) insert solst;
        ServiceMaxWOG.CreateWOG(solst);    
        solst.clear();
        for(SVMXC__Service_Order__c so:solst)
        {
            SVMXC__Service_Order__c wo = so.clone();
            wo.WorkOrderGroup__c = null;
            wo.Parent_Work_Order__c = so.Id;
            wo.SVMXC__Order_Status__c ='Service Delivered';
            solst.add(wo);
        }
        if(solst.size()>0) insert solst;
        ServiceMaxWOG.CreateWOG(solst);  
        
        SVMXC__Service_Contract__c contract2 = new SVMXC__Service_Contract__c(Name = 'Test', SVMXC__Company__c = acc.Id);
            insert contract2 ;
        SVMXC__PM_Plan__c pm = new SVMXC__PM_Plan__c(Name = 'test', SVMXC__Service_Contract__c = contract2.Id, SVMXC__Work_Order_Assign_To_User__c = UserInfo.getUserId());
        insert pm;
        SVMXC__Service_Order__c workOrder2 =  Utils_TestMethods.createWorkOrder(acc.id);
         workOrder2.Work_Order_Category__c='On-site'; 
         workOrder2.SoldToAccount__c =    acc.id;
         workOrder2.SVMXC__Order_Type__c='Maintenance';
         workOrder2.SVMXC__Problem_Description__c = 'BLALBLALA';
         workOrder2.SVMXC__Order_Status__c = 'UnScheduled';
         workOrder2.CustomerRequestedDate__c = Date.today();
         workOrder2.Service_Business_Unit__c = 'Energy';
         workOrder2.SVMXC__Priority__c = 'Normal-Medium';
         workOrder2.SendAlertNotification__c = true;
         workOrder2.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         workOrder2.BackOfficeReference__c = '2222222';
         workOrder2.SVMXC__Service_Contract__c =contract.Id;
         workOrder2.Parent_Work_Order__c =wor.Id;
           workOrder2.WorkOrderGroup__c = null;
         //insert workOrder2;
        ServiceMaxWOG.UpdateWOGPMPlan(new Set<id>{pm.id},new List<SVMXC__Service_Order__c>{workOrder2});
        
        
    }
}