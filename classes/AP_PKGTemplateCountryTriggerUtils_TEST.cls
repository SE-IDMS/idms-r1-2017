/**
31-July-2012    
Shruti Karn  
Sept CCC Release    
Initial Creation: test class for PKGTemplateCountryAfterInsert ,  PKGTemplateCountryAfterDelete and AP_PKGTemplateCountryTriggerUtils
 */
@isTest
private class AP_PKGTemplateCountryTriggerUtils_TEST {

    static testMethod void myUnitTest() {
        
        PackageTemplate__c PKGTemplate = Utils_TestMethods.createPkgTemplate();
        insert PKGTemplate;
        
        Country__c country =  Utils_TestMethods.createCountry();
        insert country;
        
        Country__c country2 =  Utils_TestMethods.createCountry();
        country2.CountryCode__c = 'ABC';
        insert country2;
        
        PackageTemplateCountry__c PKGCountry = Utils_TestMethods.createPKGTempltCountry(PKGTemplate.Id , country.Id);
        insert PKGCountry;
        
        PackageTemplateCountry__c PKGCountry2 = Utils_TestMethods.createPKGTempltCountry(PKGTemplate.Id , country2.Id);
        insert PKGCountry2;
        
        delete PKGCountry ;
      
    }
}