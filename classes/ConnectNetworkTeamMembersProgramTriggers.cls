/*
    Author           : Gayathri Shivakumar (Schneider Electric)
    Date Created     :  03-Feb-2012
    Modification Log : Partner Region Added on 10-May-2012
    Description      : Class for Connect Deployment Network Triggers, Validate Deployment Network
*/

public with sharing class ConnectNetworkTeamMembersProgramTriggers
{


Public Static void ConnectNetworkTeamMembersBeforeInsert(List<Team_Members__c> TM)
{

System.Debug('****** ConnectNetworkTeamMembersBeforeInsert Start ****'); 

// Lists

list<Team_Members__c> Team = [select Team_Member__r.FirstName, Team_Member__r.LastName, Team_Name__c, Entity__c, Program_Name__c, GSC_Region__c, Partner_Region__c, Smart_Cities_Division__c from Team_Members__c] ;
list <Project_NCP__c> Program = [Select Name, Program_Name__c, Global_Functions__c, Global_Business__c, Power_Region__c, GSC_Regions__c, Partner_Region__c,Smart_Cities_Division__c from Project_NCP__c ];
List<String> GlobalFunctions = new List<string>();
List<String> GlobalBusiness = new List<string>();
List<String> PowerRegion = new List<string>();
List<String>  GSC_Region = new List<string>();
List<String> Partner_Region = new List<string>();
List<String> Smart_Cities = new List<string>();

// Declarations 

String GF_Team;
Integer I;
Boolean GF_Network= false;
Boolean GF_No_Network= false;

String GB_Team;
Boolean GB_Network = false;
Boolean GB_No_Network = false;


String PR_Team;
Boolean PR_Network = false;
Boolean PR_No_Network = false;

String GSC_Team;
Boolean GSC_Network = false;
Boolean GSC_No_Network = false;

String Partner_Team;
Boolean Partner_Network = false;
Boolean Partner_No_Network = false;

String SC_Team;
Boolean SC_Network = false;
Boolean SC_No_Network = false;

GF_Team = '';
GB_Team = '';
PR_Team = '';
GSC_Team = '';
Partner_Team = '';
SC_Team = '';


For(Team_Members__c T : TM){



  For(Team_Members__c TMs : Team){
  
  // Concatenate the Scope of Selected Team Members for Global Functions
  
    if(TMs.Team_Name__c == System.Label.Connect_Global_Functions && TMs.Program_Name__c == T.Program_Name__c){
        GF_Team = GF_Team + Tms.Entity__c + ';';
        System.Debug('GFTeam ' + GF_Team);
         }        
   
    
  // Concatenate the Scope of Selected Team Members for Global Business


    if(TMs.Team_Name__c == System.Label.Connect_Global_Business && TMs.Program_Name__c == T.Program_Name__c){
        GB_Team = GB_Team + Tms.Entity__c + ';';
         }
   
       
  // Concatenate the Scope of Selected Team Members for Power Region


    if(TMs.Team_Name__c == System.Label.Connect_Power_Region && TMs.Program_Name__c == T.Program_Name__c){
        PR_Team = PR_Team + Tms.Entity__c + ';';
         }
  
       
  // Concatenate the Scope of Selected Team Members for GSC 

    if(TMs.Team_Name__c == System.Label.Connect_Global_Functions && TMs.Entity__c == System.Label.Connect_GSC && TMs.Program_Name__c == T.Program_Name__c){
        GSC_Team = GSC_Team + Tms.GSC_Region__c + ';';
         }
         
  // Concatenate the Scope of Selected Team Members for Partner-Region

    if(TMs.Team_Name__c == System.Label.Connect_Global_Business && TMs.Entity__c == System.Label.Connect_Partner && TMs.Program_Name__c == T.Program_Name__c){
        Partner_Team = Partner_Team + Tms.Partner_Region__c + ';';
         }
         
 // Concatenate the Scope of Selected Team Members for Smart Cities

    if(TMs.Team_Name__c == System.Label.Connect_Global_Business && TMs.Entity__c == System.Label.Connect_Smart_Cities && TMs.Program_Name__c == T.Program_Name__c){
        SC_Team = SC_Team + Tms.Smart_Cities_Division__c + ';';
         }
         
      
    }//Team For Loop End
    
    // Adding The Entity of the Record updated/inserted
    
    if(T.Team_Name__c == System.Label.Connect_Global_Functions)
     GF_Team = GF_Team + T.Entity__c + ';';
     
     if(T.Team_Name__c == System.Label.Connect_Global_Business)
       GB_Team = GB_Team + T.Entity__c + ';';
       
     if(T.Team_Name__c == System.Label.Connect_Power_Region)
       PR_Team = PR_Team + T.Entity__c + ';';
       
     if(T.Team_Name__c == System.Label.Connect_Global_Functions && T.Entity__c == System.Label.Connect_GSC)
        GSC_Team = GSC_Team + T.GSC_Region__c + ';';
        
     if(T.Team_Name__c == System.Label.Connect_Global_Business && T.Entity__c == System.Label.Connect_Partner)
        Partner_Team = Partner_Team + T.Partner_Region__c + ';';
    
    System.Debug('GF-Team ' + GF_Team);
    
    // Split the Entities in Program for Global Functions

    For(Project_NCP__c Prg : Program){

        if(Prg.Name == T.Program_Name__c && Prg.Global_Functions__c!= null){
            for(String GF: Prg.Global_Functions__c.Split(';')){
                
                 GlobalFunctions.add(GF);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
            
            System.Debug('Updated Teams ' + GF_Team +  ' ' + GSC_Team + ' ' + GB_Team + ' ' + PR_Team);
            
     // Split the Entities in Program for Global Business

    For(Project_NCP__c Prg : Program){
       
        System.Debug('Global Business ' + Prg.Global_Business__c);   

        if(Prg.Name == T.Program_Name__c && Prg.Global_Business__c != null){
        
             System.Debug('Global Business ' + Prg.Global_Business__c);   
              for(String GB: Prg.Global_Business__c.Split(';')){
                System.Debug('GB ' + GB);
                 GlobalBusiness.add(GB);
                      } // For String Ends
             } // If Ends
             
       
            } // Program Loop Ends
            
    // Split the Entities in Program for Power Region

    For(Project_NCP__c Prg : Program){
    
        
        if(Prg.Name == T.Program_Name__c && Prg.Power_Region__c != null){
            for(String PR: Prg.Power_Region__c.Split(';')){
                
                 PowerRegion.add(PR);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
      // Split the Entities in Program for GSC

    For(Project_NCP__c Prg : Program){

        if(Prg.Name == T.Program_Name__c && Prg.GSC_Regions__c!= null){
            for(String GSC_R: Prg.GSC_Regions__c.Split(';')){
                            
                 GSC_Region.add(GSC_R);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
            
    // Split the Entities in Program for Partner-Region

    For(Project_NCP__c Prg : Program){

        if(Prg.Name == T.Program_Name__c && Prg.Partner_Region__c!= null){
            for(String PAR: Prg.Partner_Region__c.Split(';')){
                            
                 Partner_Region.add(PAR);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
     
     
     // Split the Entities in Program for Smart Cities

    For(Project_NCP__c Prg : Program){

        if(Prg.Name == T.Program_Name__c && Prg.Smart_Cities_Division__c!= null){
            for(String SC: Prg.Smart_Cities_Division__c.Split(';')){
                            
                 Smart_Cities.add(SC);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends       
           
            
            
  // Compare Team Entities for Global Functions 
  
  if((GF_Team == '') && (GlobalFunctions.size() > 0))
      GF_No_Network = True; 
  
  else if(GF_Team != ''){

  for(I=0; I < GlobalFunctions.size(); I++){
    //System.Debug('No Network ');

    if(GF_Team.Contains(GlobalFunctions[I])){
     GF_Network = True; 
     System.Debug('Network GF --- ' + GF_Network);
     }
     else{
     GF_No_Network = True; 
     System.Debug('No Network  GF --' + GF_No_Network);
      }
     }// GF For Loops Ends
    } // GF IF Ends
 
 // Compare Team Entities for Global Business
 
   if((GB_Team == '') && (GlobalBusiness.size() > 0))
      GB_No_Network = True; 
   
   else  if(GB_Team != ''){
 
   for(I=0; I < GlobalBusiness.size(); I++){
    System.Debug( ' GB Team ' + GB_Team  + 'GB ' + GlobalBusiness[I]);
  
    if(GB_Team.Contains(GlobalBusiness[I])){
     GB_Network = True; 
     //System.Debug('No Network ' + GB_Network);
     }
     else{
     GB_No_Network = True; 
     //System.Debug('No Network ' + GB_No_Network);
     }
    }// GB For Loops Ends
   } // GB IF Ends
   
   
   // Compare Team Entities for Power Regions
   
 if((PR_Team == '') && (PowerRegion.size() > 0))
      PR_No_Network = True; 
 else    if(PR_Team != ''){
  for(I=0; I < PowerRegion.size(); I++){
    //System.Debug('No Network ');

    if(PR_Team.Contains(PowerRegion[I])){
     PR_Network = True; 
     //System.Debug('No Network ' + PR_Network);
     }
     else{
     PR_No_Network = True; 
     //System.Debug('No Network ' + PR_No_Network);
     }
    }// PR For Loops Ends
   } // PR IF Ends
   
   
    // Compare Team Entities for GSC
    
 if((GSC_Team == '') && (GSC_Region.size() > 0))
      GSC_No_Network = True; 
      
 else  if(GSC_Team != ''){
 
  for(I=0; I < GSC_Region.size(); I++){
 
    if(GSC_Team.Contains(GSC_Region[I])){
     GSC_Network = True;      
     }
     else{
     GSC_No_Network = True; 
     
     }
    }// GSC For Loops Ends
   } // GSC IF Ends
 

 // Compare Team Entities for Partner-Region
    
 if((Partner_Team == '') && (Partner_Region.size() > 0))
      Partner_No_Network = True; 
      
 else  if(Partner_Team != ''){
 
  for(I=0; I < Partner_Region.size(); I++){
 
    if(Partner_Team.Contains(Partner_Region[I])){
     Partner_Network = True;      
     }
     else{
     Partner_No_Network = True; 
     
     }
    }// Partner-Region For Loops Ends
   } // Partner-Region IF Ends
   
   
   // Compare Team Entities for Smart Cities
    
 if((SC_Team == '') && (Smart_Cities.size() > 0))
      SC_No_Network = True; 
      
 else  if(SC_Team != ''){
 
  for(I=0; I < Smart_Cities.size(); I++){
 
    if(SC_Team.Contains(Smart_Cities[I])){
     SC_Network = True;      
     }
     else{
     SC_No_Network = True; 
     
     }
    }// Smart Cities For Loops Ends
   } // Smart Cities IF Ends
   
System.Debug('No Network ' + 'GF' + GF_No_Network + 'GB' + GB_No_Network + 'PR' + PR_No_Network + 'GSC' + GSC_No_Network);

    if((GF_No_Network == True) || (GB_No_Network == True)|| (PR_No_Network == True) || (GSC_No_Network == True) || (Partner_No_Network == True) || (SC_No_Network == True))
        T.Program_Network__c = False;
    else 
        T.Program_Network__c = True;
  
  
  //System.Debug( 'Program_Network ' + T.Program_Network__c);  
     } // Record Loop Ends
     
   
   
  System.Debug('****** ConnectNetworkTeamMembersBeforeInsert Stop ****'); 

 } // Static Class Ends
 
 // Class to Validate Deployment Network on Update
 
 Public Static void ConnectNetworkTeamMembersBeforeUpdate(List<Team_Members__c> TM, List<Team_Members__c> TMold)
{

System.Debug('****** ConnectNetworkTeamMembersBeforeUpdate Start ****'); 

// Lists

list<Team_Members__c> Team = [select Team_Member__r.FirstName, Team_Member__r.LastName, Team_Name__c, Entity__c, Program_Name__c, GSC_Region__c,Partner_Region__c, Smart_Cities_Division__c from Team_Members__c  ] ;
list <Project_NCP__c> Program = [Select Name, Program_Name__c, Global_Functions__c, Global_Business__c, Power_Region__c, GSC_Regions__c,Partner_Region__c,Smart_Cities_Division__c from Project_NCP__c];
List<String> GlobalFunctions = new List<string>();
List<String> GlobalBusiness = new List<string>();
List<String> PowerRegion = new List<string>();
List<String>  GSC_Region = new List<string>();
List<String> Partner_Region = new List<string>();
List<String> Smart_Cities = new List<string>();

// Declarations 

String GF_Team;
Integer I;
Boolean GF_Network;
Boolean GF_No_Network;

String GB_Team;
Boolean GB_Network;
Boolean GB_No_Network;


String PR_Team;
Boolean PR_Network;
Boolean PR_No_Network;

String GSC_Team;
Boolean GSC_Network;
Boolean GSC_No_Network;

String Partner_Team;
Boolean Partner_Network = false;
Boolean Partner_No_Network = false;

String SC_Team;
Boolean SC_Network = false;
Boolean SC_No_Network = false;

GF_Team = '';
GB_Team = '';
PR_Team = '';
GSC_Team = '';
Partner_Team = '';
SC_Team = '';


For(Team_Members__c T : TM){


  For(Team_Members__c TMs : Team){
  
  // Concatenate the Scope of Selected Team Members for Global Functions

    if(TMs.Team_Name__c == System.Label.Connect_Global_Functions && TMs.Program_Name__c == T.Program_Name__c && (TMold[0].Team_Name__c == System.Label.Connect_Global_Functions && TMold[0].Program_Name__c == T.Program_Name__c && TMold[0].Entity__c != T.Entity__c)){
        GF_Team = GF_Team + Tms.Entity__c + ';';
         }
              
  // Concatenate the Scope of Selected Team Members for Global Business

     if(TMs.Team_Name__c == System.Label.Connect_Global_Business && TMs.Program_Name__c == T.Program_Name__c && (TMold[0].Team_Name__c == System.Label.Connect_Global_Business && TMold[0].Program_Name__c == T.Program_Name__c && TMold[0].Entity__c != T.Entity__c )){
        GB_Team = GB_Team + Tms.Entity__c + ';';
         }
   
  // Concatenate the Scope of Selected Team Members for Power Region

    if(TMs.Team_Name__c == System.Label.Connect_Power_Region && TMs.Program_Name__c == T.Program_Name__c && (TMold[0].Team_Name__c == System.Label.Connect_Power_Region && TMold[0].Program_Name__c == T.Program_Name__c && TMold[0].Entity__c != T.Entity__c )){
        PR_Team = PR_Team + Tms.Entity__c + ';';
         }
  
       System.Debug('New Change Operation Region ' + PR_Team);
  // Concatenate the Scope of Selected Team Members for GSC

    if(TMs.Team_Name__c == System.Label.Connect_Global_Functions && TMs.Program_Name__c == T.Program_Name__c &&  T.Entity__c == System.Label.Connect_GSC && (TMold[0].Team_Name__c == System.Label.Connect_Global_Functions && TMold[0].Program_Name__c == T.Program_Name__c && TMold[0].GSC_Region__c != T.GSC_Region__c )){
        GSC_Team = GSC_Team + Tms.GSC_Region__c + ';';
         }
         
     // Concatenate the Scope of Selected Team Members for Partner-Region

    if(TMs.Team_Name__c == System.Label.Connect_Global_Business && TMs.Program_Name__c == T.Program_Name__c && T.Entity__c == System.Label.Connect_Partner && (TMold[0].Team_Name__c == System.Label.Connect_Global_Business && TMold[0].Program_Name__c == T.Program_Name__c && TMold[0].Partner_Region__c != T.Partner_Region__c )){
        Partner_Team = Partner_Team + Tms.Partner_Region__c + ';';
         }
         
    // Concatenate the Scope of Selected Team Members for Smart Cities

    if(TMs.Team_Name__c == System.Label.Connect_Global_Business && TMs.Program_Name__c == T.Program_Name__c && T.Entity__c == System.Label.Connect_Smart_Cities && (TMold[0].Team_Name__c == System.Label.Connect_Global_Business && TMold[0].Program_Name__c == T.Program_Name__c && TMold[0].Smart_Cities_Division__c != T.Smart_Cities_Division__c )){
        SC_Team = SC_Team + Tms.Smart_Cities_Division__c + ';';
         }
         
     } // Team For Loop Ends
     
      // Adding The Entity of the Record updated/inserted
      
   if(T.Team_Name__c == System.Label.Connect_Global_Functions )
     GF_Team = GF_Team + T.Entity__c + ';';
     
   if(T.Team_Name__c == System.Label.Connect_Global_Business)
       GB_Team = GB_Team + T.Entity__c + ';';
       
   if(T.Team_Name__c == System.Label.Connect_Power_Region)
       PR_Team = PR_Team + T.Entity__c + ';';
      
   if(T.Team_Name__c == System.Label.Connect_Global_Functions  && T.Entity__c == System.Label.Connect_GSC)
    GSC_Team = GSC_Team + T.GSC_Region__c + ';';
    
    if(T.Team_Name__c == System.Label.Connect_Global_Business && T.Entity__c == System.Label.Connect_Partner )
     Partner_Team = Partner_Team + T.Partner_Region__c + ';';
     
    if(T.Team_Name__c == System.Label.Connect_Global_Business && T.Entity__c == System.Label.Connect_Smart_Cities )
     SC_Team = SC_Team + T.Smart_Cities_Division__c + ';';
    
    // Split the Entities in Program for Global Functions

    For(Project_NCP__c Prg : Program){

        if(Prg.Name == T.Program_Name__c && Prg.Global_Functions__c!= null){
            for(String GF: Prg.Global_Functions__c.Split(';')){
                
                 GlobalFunctions.add(GF);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
            
            System.Debug('Updated Teams ' + GF_Team +  ' ' + GSC_Team + ' ' + GB_Team);
            
     // Split the Entities in Program for Global Business

    For(Project_NCP__c Prg : Program){
       
        System.Debug('Global Business ' + Prg.Global_Business__c);   

        if(Prg.Name == T.Program_Name__c && Prg.Global_Business__c != null){
        
             System.Debug('Global Business ' + Prg.Global_Business__c);   
              for(String GB: Prg.Global_Business__c.Split(';')){
                System.Debug('GB ' + GB);
                 GlobalBusiness.add(GB);
                      } // For String Ends
             } // If Ends
             
       
            } // Program Loop Ends
            
    // Split the Entities in Program for Power Region

    For(Project_NCP__c Prg : Program){
    
        
        if(Prg.Name == T.Program_Name__c && Prg.Power_Region__c != null){
            for(String PR: Prg.Power_Region__c.Split(';')){
                
                 PowerRegion.add(PR);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
      // Split the Entities in Program for GSC

    For(Project_NCP__c Prg : Program){

        if(Prg.Name == T.Program_Name__c && Prg.GSC_Regions__c!= null){
            for(String GSC_R: Prg.GSC_Regions__c.Split(';')){
                            
                 GSC_Region.add(GSC_R);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
            
    // Split the Entities in Program for Partner-Region

    For(Project_NCP__c Prg : Program){

        if(Prg.Name == T.Program_Name__c && Prg.Partner_Region__c!= null){
            for(String PAR: Prg.Partner_Region__c.Split(';')){
                            
                 Partner_Region.add(PAR);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
   
       // Split the Entities in Program for Smart Cities

    For(Project_NCP__c Prg : Program){

        if(Prg.Name == T.Program_Name__c && Prg.Smart_Cities_Division__c!= null){
            for(String SC: Prg.Smart_Cities_Division__c.Split(';')){
                            
                 Smart_Cities.add(SC);
                      } // For String Ends
             } // If Ends
            } // Program Loop Ends
            
  // Compare Team Entities for Global Functions 
  
  if((GF_Team == '') && (GlobalFunctions.size() > 0))
      GF_No_Network = True; 
  
  else if(GF_Team != ''){

  for(I=0; I < GlobalFunctions.size(); I++){
    //System.Debug('No Network ');
   
    if(GF_Team.Contains(GlobalFunctions[I])){
     GF_Network = True; 
     System.Debug('No Network GF' + GF_Network);
     }
     else{
     GF_No_Network = True; 
     System.Debug('No Network GF ' + GF_No_Network);
      }
     }// GF For Loops Ends
    } // GF IF Ends
 
 // Compare Team Entities for Global Business
 
 if((GB_Team == '') && (GlobalBusiness.size() > 0))
      GB_No_Network = True; 
      
 else if(GB_Team != ''){
    
   for(I=0; I < GlobalBusiness.size(); I++){
    System.Debug( ' GB Team ' + GB_Team  + 'GB ' + GlobalBusiness[I]);
   
    if(GB_Team.Contains(GlobalBusiness[I])){
     GB_Network = True; 
     System.Debug('No Network GB ' + GB_Network);
     }
     else{
     GB_No_Network = True; 
     System.Debug('No Network GB ' + GB_No_Network);
     }
    } // GB For Loops Ends
   }// GB IF Ends
   
   
   // Compare Team Entities for Power Regions
   
 if((PR_Team == '') && (PowerRegion.size() > 0))
     PR_No_Network = True; 
     
 else  if(PR_Team != ''){
 
  for(I=0; I < PowerRegion.size(); I++){
    //System.Debug('No Network ');
  
    if(PR_Team.Contains(PowerRegion[I])){
     PR_Network = True; 
     System.Debug('Network PR' + PR_Network);
     }
     else{
     PR_No_Network = True; 
     System.Debug('No Network PR' + PR_No_Network);
     }
    }// PR For Loops Ends
   } // PR IF Ends
   
   
    // Compare Team Entities for GSC
    
if((GSC_Team == '') && (GSC_Region.size() > 0))
     GSC_No_Network = True;
      
else  if(GSC_Team != ''){

  for(I=0; I < GSC_Region.size(); I++){
  
    if(GSC_Team.Contains(GSC_Region[I])){
     GSC_Network = True;      
     }
     else{
     GSC_No_Network = True; 
     
     }
    } // GSC For Loops Ends
   } // GSC IF Ends
   
 // Compare Team Entities for Partner-Region
    
if((Partner_Team == '') && (Partner_Region.size() > 0))
     Partner_No_Network = True;
      
else  if(Partner_Team != ''){

  for(I=0; I < Partner_Region.size(); I++){
  
    if(Partner_Team.Contains(Partner_Region[I])){
     Partner_Network = True;      
     }
     else{
     Partner_No_Network = True; 
     
     }
    } // Partner-Region For Loops Ends
   } // Partner-Region IF Ends
   
   
  // Compare Team Entities for Smart Cities
    
if((SC_Team == '') && (Smart_Cities.size() > 0))
     SC_No_Network = True;
      
else  if(SC_Team != ''){

  for(I=0; I < Smart_Cities.size(); I++){
  
    if(Partner_Team.Contains(Smart_Cities[I])){
     SC_Network = True;      
     }
     else{
     SC_No_Network = True; 
     
     }
    } // Smart Cities For Loops Ends
   } // Smart Cities IF Ends
 

System.Debug('No Network ' + 'GF' + GF_No_Network + 'GB' + GB_No_Network + 'PR' + PR_No_Network + 'GSC' + GSC_No_Network);

    if((GF_No_Network == True) || (GB_No_Network == True)|| (PR_No_Network == True) || (GSC_No_Network == True) || (Partner_No_Network == True) || (SC_No_Network == True))
        T.Program_Network__c = False;
      else
        T.Program_Network__c = True;
  
  
  //System.Debug( 'Program_Network ' + T.Program_Network__c);  
     } // Team Loop Ends
     
   
   
  System.Debug('****** ConnectNetworkTeamMembersBeforeUpdate Stop ****'); 

 } // Static Class Ends
 
 

}