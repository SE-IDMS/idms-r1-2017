public class VFC_SPAContractSync {
public SPARequest__c request{get;set;}
 
    public VFC_SPAContractSync(ApexPages.StandardController controller) {       
        request=(SPARequest__c)controller.getRecord();                
    }
    public PageReference validateSync()
    {
    boolean hasError=false;
        if(request.BackOfficeContractNumber__c!=null){
        hasError=true;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error,System.Label.CLSEP12SLS37));
        }
        if(request.ApprovalStatus__c!='Approved'){
        hasError=true;
        ApexPages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.Error,System.Label.CLSEP12SLS38));
        }
        if(hasError)
        return null;
        else
        return sync();        
    }    
    public PageReference sync()
    {
        return new PageReference(System.Label.CLSEP12SLS32+'?'+System.Label.CLSEP12SLS33+'='+request.id+'&'+System.Label.CLSEP12SLS34+'='+request.BackofficeSystemId__c+'&'+System.Label.CLSEP12SLS35+'='+UserInfo.getSessionId()+'&'+System.Label.CLSEP12SLS36+'='+System.Label.CL00597);
    }

}