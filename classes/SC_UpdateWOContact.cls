global class SC_UpdateWOContact implements Schedulable {
  
   global void execute(SchedulableContext ctx) {
      
        try
        {
            list<Contact> lstExistingContact = [Select id, TECH_Downloadable__c from Contact where TECH_Downloadable__c = TRUE limit 10000];
            for(Contact contact : lstExistingContact)
            {
                contact.TECH_Downloadable__c = false;
            }
            update lstExistingContact;
            list<SVMXC__Service_Order__c> lstWorkOrder = new list<SVMXC__Service_Order__c>();
            lstWorkOrder = [Select Customer_Location__c, SVMXC__Company__c from SVMXC__Service_Order__c where  
                                SVMXC__Order_Status__c = : Label.CLDEC12SRV12 or 
                                
                                SVMXC__Order_Status__c = : Label.CLDEC12SRV10 or 
                                SVMXC__Order_Status__c = : Label.CLDEC12SRV48 or 
                                SVMXC__Order_Status__c = : Label.CLDEC12SRV49  limit 50000];
            set<Id> setAccountId = new set<Id>();
            set<Id> setCustLocationId = new set<Id>();
            
            for(SVMXC__Service_Order__c WO : lstWorkOrder)
            {
                setAccountId.add(WO.SVMXC__Company__c);
                setCustLocationId.add(WO.Customer_Location__c);
            }
            
            list<FSM_Rel_cont_to_CL__c> lstCustLocationContact = [Select id, Contact__c from FSM_Rel_cont_to_CL__c where Customer_Location__c in :setCustLocationId limit 10000];
            set<Id> setContactId = new set<Id>();
            for(FSM_Rel_cont_to_CL__c cont : lstCustLocationContact)
                setContactId.add(cont.Contact__c);
            list<Contact> lstContact = [Select id, TECH_Downloadable__c from Contact where AccountID in :setAccountId or id in :setContactId limit 10000];
            for(Contact contact : lstContact)
            {
                if(contact.TECH_Downloadable__c== false)
                    contact.TECH_Downloadable__c = true;
            }
            update lstContact;
        }
        catch(Exception e)
        {
              System.debug('####### Following exception occured '+e.getMessage());
        }
   }   
}