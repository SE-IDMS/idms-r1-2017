@isTest
public class IDMSPingSamlJitHandlerTest {
   
   static testmethod void createUserTest() { 
       IDMSPingSamlJitHandler saml = new IDMSPingSamlJitHandler() ; 
       saml.createUser(null,null,null,null,null,null);
   }
   static testmethod void updateUserTest() {
     
     User usersaml = new User(alias = 'usersaml', email='usersaml' + '@accenture.com', isActive = false, 
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='usersaml' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMSToken__c='1234');
        
     insert usersaml;
     
     Map<String,String> samlAttributes = new Map<String,String>() ; 
     samlAttributes.put('country','us');
     samlAttributes.put('division','us');
     samlAttributes.put('phone','9986995000');
     samlAttributes.put('email','usersaml@accenture.com');
     samlAttributes.put('FirstName','user');
     samlAttributes.put('LastName','saml');
     samlAttributes.put('City','us');
     samlAttributes.put('FederatedId','308-Test123');
     
     
     IDMSPingSamlJitHandler saml = new IDMSPingSamlJitHandler() ; 
     saml.updateUser(usersaml.id,null,null,null,usersaml.FederationIdentifier ,samlAttributes ,null);
   }
   
   static testmethod void updateUserTest2() {
     
     User usersaml2 = new User(alias = 'usersam2', email='usersam2' + '@accenture.com', isActive = true,
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='usersam2' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMSToken__c='1234');
        
     insert usersaml2;
     
     Map<String,String> samlAttributes = new Map<String,String>() ; 
     samlAttributes.put('country','us');
     samlAttributes.put('division','us');
     samlAttributes.put('phone','9986995000');
     samlAttributes.put('email','usersaml@accenture.com');
     samlAttributes.put('FirstName','user');
     samlAttributes.put('LastName','saml');
     samlAttributes.put('City','us');
     samlAttributes.put('FederatedId','308-Test123');
     
     
     IDMSPingSamlJitHandler saml = new IDMSPingSamlJitHandler() ; 
     saml.updateUser(null,null,null,null,usersaml2.FederationIdentifier ,samlAttributes ,null);
     saml.updateUser(usersaml2.id,null,null,null,usersaml2.FederationIdentifier ,samlAttributes ,null);
   }
   static testmethod void mapJitIdmsUser(){
   
      User usersaml3 = new User(alias = 'usersam3', email='usersam3' + '@accenture.com', isActive = false ,
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='usersam3' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMSToken__c='1234');
        
     insert usersaml3;
    
  
     Map<String,String> samlAttributes = new Map<String,String>() ; 
     samlAttributes.put('country','us');
     samlAttributes.put('division','us');
     samlAttributes.put('phone','9986995000');
     samlAttributes.put('email','usersaml@accenture.com');
     samlAttributes.put('FirstName','user');
     samlAttributes.put('LastName','saml');
     samlAttributes.put('City','us');
     samlAttributes.put('FederatedId','308-Test123');
      
     IDMSPingSamlJitHandler saml = new IDMSPingSamlJitHandler() ; 
     String jsonValue = saml.mapPingIdmsUser(usersaml3, samlAttributes, true ) ; 
     //IDMSPingSamlJitHandler.updateAsync(jsonValue); 
   }
   Static testMethod void updateAsyncTest(){
       User usersaml4 = new User(alias = 'usersam4', email='usersam4' + '@accenture.com', isActive = false ,
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='usersam4' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMSToken__c='1234');
        
     insert usersaml4;
     IDMSPingSamlJitHandler.updateAsync(JSON.Serialize(usersaml4)); 
   }
}