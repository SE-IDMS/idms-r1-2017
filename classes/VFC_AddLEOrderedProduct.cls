global with sharing class VFC_AddLEOrderedProduct  {    
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public ComplaintRequest__c crRecord{get;set;}
    public Boolean changeProduct{get;set;}
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    public String jsonLevel{get;set;}{jsonLevel='';} 
    public VFC_AddLEOrderedProduct (ApexPages.StandardController controller){
        crRecord=(ComplaintRequest__c)controller.getRecord();   

        String Mode = system.currentpagereference().getParameters().get('Mode');
                
/*        if(Mode=='ordered')
        {
            ChangeProduct = true;
        }
        else if (Mode=='received')
        {
            ChangeProduct = false;
        }    
*/      
//k:START
        if(Mode=='ordered')
        {
            ChangeProduct = true;
            if(crRecord.id!=null){
                crRecord=[select id, CommercialReferenceOrdered__c,CommercialReferenceOrdered_lk__c,TECH_CROrderBusiness__c ,FamilyOrdered__c,FamilyOrdered_lk__c ,TECH_CROrderProductDescription__c ,TECH_CROrderProductFamily__c ,TECH_CROrderProductGroup__c, TECH_CROrderProductLine__c, TECH_CROrderProductSuccession__c, TECH_CROrderSubFamily__c  from ComplaintRequest__c where id=:crRecord.id];
                if(crRecord.TECH_CROrderBusiness__c!=null)
                    pageParameters.put('businessLine',crRecord.TECH_CROrderBusiness__c);            
                if(crRecord.TECH_CROrderProductLine__c!=null)
                    pageParameters.put('productLine',crRecord.TECH_CROrderProductLine__c);            
                if(crRecord.TECH_CROrderProductFamily__c!=null)
                    pageParameters.put('strategicProductFamily',crRecord.TECH_CROrderProductFamily__c);            
                if(crRecord.FamilyOrdered__c!=null)
                    pageParameters.put('family',crRecord.FamilyOrdered__c);  
               // if(crRecord.FamilyOrdered_lk__c!=null)
                   // pageParameters.put('family',crRecord.FamilyOrdered_lk__c);              
                if(crRecord.CommercialReferenceOrdered__c!=null)
                    pageParameters.put('commercialReference',crRecord.CommercialReferenceOrdered__c);
               // if(crRecord.CommercialReferenceOrdered_lk__c!=null)
                  //  pageParameters.put('commercialReference',crRecord.CommercialReferenceOrdered_lk__c);
                String URLCR='';
                if(pageParameters.containsKey('commercialReference'))
                    URLCR =pageParameters.get('commercialReference');          
            }                     
        }
        else if (Mode=='received')
        {
            ChangeProduct = false;
            if(crRecord.id!=null){
                crRecord=[select id, CommercialReferenceReceived__c,CommercialReferenceReceived_lk__c,TECH_CRReceivedBusiness__c ,FamilyReceived__c ,FamilyReceived_lk__c,TECH_CRReceivedProductDescription__c ,TECH_CRReceivedProductFamily__c ,TECH_CRReceivedProductGroup__c, TECH_CRReceivedProductLine__c, TECH_CRReceivedProductSuccession__c, TECH_CRReceivedSubFamily__c  from ComplaintRequest__c where id=:crRecord.id];
                if(crRecord.TECH_CRReceivedBusiness__c!=null)
                    pageParameters.put('businessLine',crRecord.TECH_CRReceivedBusiness__c);            
                if(crRecord.TECH_CRReceivedProductLine__c!=null)
                    pageParameters.put('productLine',crRecord.TECH_CRReceivedProductLine__c);            
                if(crRecord.TECH_CRReceivedProductFamily__c!=null)
                    pageParameters.put('strategicProductFamily',crRecord.TECH_CRReceivedProductFamily__c);            
                if(crRecord.FamilyReceived__c!=null)
                    pageParameters.put('family',crRecord.FamilyReceived__c); 
               // if(crRecord.FamilyReceived_lk__c!=null)
                 //   pageParameters.put('family',crRecord.FamilyReceived_lk__c);                 
                if(crRecord.CommercialReferenceReceived__c!=null)
                    pageParameters.put('commercialReference',crRecord.CommercialReferenceReceived__c);
               // if(crRecord.CommercialReferenceReceived_lk__c!=null)
                  //  pageParameters.put('commercialReference',crRecord.CommercialReferenceReceived_lk__c);
                String URLCR='';
                if(pageParameters.containsKey('commercialReference'))
                    URLCR =pageParameters.get('commercialReference');          
            }
        }    

//k:END        
              
    }
    
    public String scriteria{get;set;}{    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
    }
    
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
    


        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
            }
        }
        if(!Test.isRunningTest()){    
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
    

    
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        return Database.query(query);       
    }
    public PageReference performSelect(){      
        String jsonSelectString = Apexpages.currentPage().getParameters().get('jsonSelectString');
        System.debug('jsonSelectString Select = ' + jsonSelectString);
        
        WS_GMRSearch.gmrFilteredDataBean DBL = (WS_GMRSearch.gmrFilteredDataBean) JSON.deserialize(jsonSelectString, WS_GMRSearch.gmrFilteredDataBean.class);   
        
        
        if(crRecord.id != null){         
      
          if(ChangeProduct)
          {
            if(DBL.commercialReference != null)
            crRecord.CommercialReferenceOrdered__c= DBL.commercialReference.UnEscapeXML();
            if(DBL.ProductIdbFO != null)
                crRecord.CommercialReferenceOrdered_lk__c= DBL.ProductIdbFO.UnEscapeXML();
            if(DBL.family.label != null)
            crRecord.Family__c = DBL.family.label.UnEscapeXML();
            //crRecord.TECH_CROrderGMRCode__c=GMRProduct.field11__c;
            if(DBL.businessLine.label != null)
            //crRecord.TECH_CROrderBusiness__c =DBL.productLine.label;
            crRecord.TECH_CROrderBusiness__c =DBL.businessLine.label.UnEscapeXML(); //k:
            if(DBL.family.label != null)
            crRecord.TECH_CROrderFamily__c =DBL.family.label.UnEscapeXML();
            crRecord.FamilyOrdered__c=DBL.family.label.UnEscapeXML();
                if(!Test.isRunningTest())
                    if(DBL.ProductFamilybFO!=null)
            crRecord.FamilyOrdered_lk__c=DBL.ProductFamilybFO.UnEscapeXML();
            if(DBL.description != null)         
            crRecord.TECH_CROrderProductDescription__c =DBL.description.UnEscapeXML();
            if(DBL.strategicProductFamily.label != null)
            crRecord.TECH_CROrderProductFamily__c =DBL.strategicProductFamily.label.UnEscapeXML();
            if(DBL.productGroup.label!=null)
            crRecord.TECH_CROrderProductGroup__c =DBL.productGroup.label.UnEscapeXML();
            if(DBL.productLine.label != null)
            crRecord.TECH_CROrderProductLine__c =DBL.productLine.label.UnEscapeXML();
            if(DBL.productSuccession.label!=null)
            crRecord.TECH_CROrderProductSuccession__c =DBL.productSuccession.label.UnEscapeXML();
            if(DBL.subFamily.label!=null)
            crRecord.TECH_CROrderSubFamily__c =DBL.subFamily.label.UnEscapeXML();
            
            
                //for all 7 level's GMR Code
            if(DBL.businessLine.value != null)
                crRecord.TECH_CROrderGMRCode__c = DBL.businessLine.value;
            if(DBL.productLine.value != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.productLine.value;
            if(DBL.strategicProductFamily.label != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.strategicProductFamily.value;
            if(DBL.family.label != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.family.value;
            if(DBL.subFamily.label != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.subFamily.value;
            if(DBL.productSuccession.label != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.productSuccession.value;
            if(DBL.productGroup.label != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.productGroup.value;
         } 
         
          else
          {
            if(DBL.commercialReference != null)
                crRecord.CommercialReferenceReceived__c= DBL.commercialReference.UnEscapeXML();
            if(DBL.ProductIdbFO != null)
                crRecord.CommercialReferenceReceived_lk__c= DBL.ProductIdbFO.UnEscapeXML();         
            if(DBL.family.label != null)
                crRecord.FamilyReceived__c =  DBL.family.label.UnEscapeXML(); 
            if(DBL.family.label!= null)
                if(!Test.isRunningTest())
                    if(DBL.ProductFamilybFO!=null)
                crRecord.FamilyReceived_lk__c =  DBL.ProductFamilybFO.UnEscapeXML();
            //crRecord.TECH_CROrderGMRCode__c=GMRProduct.field11__c;
            if(DBL.businessLine.label != null)
                crRecord.TECH_CRReceivedBusiness__c =DBL.businessLine.label.UnEscapeXML();
            if(DBL.family.label != null)
                crRecord.TECH_CRReceivedFamily__c =DBL.family.label.UnEscapeXML();
            if(DBL.description != null)
                crRecord.TECH_CRReceivedProductDescription__c =DBL.description.UnEscapeXML();
            if(DBL.strategicProductFamily.label != null)
                crRecord.TECH_CRReceivedProductFamily__c =DBL.strategicProductFamily.label.UnEscapeXML();
            if(DBL.productGroup.label!=null)
                crRecord.TECH_CRReceivedProductGroup__c =DBL.productGroup.label.UnEscapeXML();
            if(DBL.productLine.label != null)
                crRecord.TECH_CRReceivedProductLine__c =DBL.productLine.label.UnEscapeXML();
            if(DBL.productSuccession.label!=null)
                crRecord.TECH_CRReceivedProductSuccession__c =DBL.productSuccession.label.UnEscapeXML();
            if(DBL.subFamily.label!=null)
                crRecord.TECH_CRReceivedSubFamily__c =DBL.subFamily.label.UnEscapeXML();
                
                
                //for all 7 level's GMR Code
            if(DBL.businessLine.value != null)
                crRecord.TECH_CRReceivedGMRCode__c = DBL.businessLine.value;
            if(DBL.productLine.value != null)
                crRecord.TECH_CRReceivedGMRCode__c += DBL.productLine.value;
            if(DBL.strategicProductFamily.label != null)
                crRecord.TECH_CRReceivedGMRCode__c += DBL.strategicProductFamily.value;
            if(DBL.family.label != null)
                crRecord.TECH_CRReceivedGMRCode__c += DBL.family.value;
            if(DBL.subFamily.label != null)
                crRecord.TECH_CRReceivedGMRCode__c += DBL.subFamily.value;
            if(DBL.productSuccession.label != null)
                crRecord.TECH_CRReceivedGMRCode__c += DBL.productSuccession.value;
            if(DBL.productGroup.label != null)
                crRecord.TECH_CRReceivedGMRCode__c += DBL.productGroup.value;
          }
                 
        }                   
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Complaint Request ID cannot be blank'));
            return null;
        }       

        try{
            upsert crRecord;
            PageReference casePage;            
            casePage=new PageReference('/'+crRecord.id);                        
            return casePage;
        }
        catch(DmlException dmlexception){
            for(integer i = 0;i<dmlexception.getNumDml();i++)
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,dmlexception.getDmlMessage(i)));    
            return null;                  
        }  
    
    }
    public PageReference performSelectFamily(){
    
    
        if(jsonSelectString=='')    
           // jsonSelectString=pageParameters.get('jsonSelectString');
        String jsonSelectString = Apexpages.currentPage().getParameters().get('jsonSelectString');
        System.debug('jsonSelectString Family = ' + jsonSelectString);
      //  WS04_GMR.gmrDataBean DBL= (WS04_GMR.gmrDataBean) JSON.deserialize(jsonSelectString, WS04_GMR.gmrDataBean.class);   
    WS_GMRSearch.gmrFilteredDataBean DBL= (WS_GMRSearch.gmrFilteredDataBean) JSON.deserialize(jsonSelectString, WS_GMRSearch.gmrFilteredDataBean.class);
        if(crRecord.id!=null){  
           if(ChangeProduct)
           {
            if(DBL.businessLine.label != null)
            crRecord.TECH_CROrderBusiness__c =DBL.businessLine.label.UnEscapeXML();
            if(DBL.productLine.label != null)
            crRecord.TECH_CROrderProductLine__c =DBL.productLine.label.UnEscapeXML();
            if(DBL.strategicProductFamily.label != null)
            crRecord.TECH_CROrderProductFamily__c =DBL.strategicProductFamily.label.UnEscapeXML();            
            if(DBL.family.label != null)
            crRecord.TECH_CROrderFamily__c =DBL.family.label.UnEscapeXML();
            crRecord.FamilyOrdered__c=DBL.family.label.UnEscapeXML();
            if(!Test.isRunningTest())
                if(DBL.ProductFamilybFO != null)
                    crRecord.FamilyOrdered_lk__c=DBL.ProductFamilybFO.UnEscapeXML();
            if(DBL.family.label != null)
            crRecord.Family__c = DBL.family.label.UnEscapeXML();
            
            crRecord.CommercialReferenceOrdered__c= null;
            crRecord.TECH_CROrderSubFamily__c =null;
            crRecord.TECH_CROrderProductDescription__c =null;
            crRecord.TECH_CROrderProductGroup__c =null;
             crRecord.TECH_CROrderProductSuccession__c =null;       
            crRecord.CommercialReferenceOrdered_lk__c=null;
            
                //for all 7 level's GMR Code
            if(DBL.businessLine.value != null)
                crRecord.TECH_CROrderGMRCode__c = DBL.businessLine.value;
            if(DBL.productLine.value != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.productLine.value;
            if(DBL.strategicProductFamily.label != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.strategicProductFamily.value;
            if(DBL.family.label != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.family.value;
            if(DBL.subFamily.label != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.subFamily.value;
            if(DBL.productSuccession.label != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.productSuccession.value;
            if(DBL.productGroup.label != null)
                crRecord.TECH_CROrderGMRCode__c += DBL.productGroup.value;
           }
           else
           {
           
            if(DBL.businessLine.label != null)
                crRecord.TECH_CRReceivedBusiness__c =DBL.businessLine.label.UnEscapeXML();
            if(DBL.productLine.label != null)
                crRecord.TECH_CRReceivedProductLine__c =DBL.productLine.label.UnEscapeXML();  
            if(DBL.strategicProductFamily.label != null)
                crRecord.TECH_CRReceivedProductFamily__c =DBL.strategicProductFamily.label.UnEscapeXML(); 
            if(DBL.family.label != null)
                crRecord.FamilyReceived__c =  DBL.family.label.UnEscapeXML(); 
            if(DBL.family.label != null)
                if(!Test.isRunningTest())
                    if(DBL.ProductFamilybFO != null)
                        crRecord.FamilyReceived_lk__c =  DBL.ProductFamilybFO.UnEscapeXML();
            if(DBL.family.label != null)
                crRecord.TECH_CRReceivedFamily__c =DBL.family.label.UnEscapeXML();
                

                crRecord.TECH_CRReceivedProductDescription__c =null;
                crRecord.TECH_CRReceivedProductGroup__c =null;
                crRecord.TECH_CRReceivedProductSuccession__c =null;
                crRecord.TECH_CRReceivedSubFamily__c =null;
                crRecord.CommercialReferenceReceived__c=null;
                crRecord.CommercialReferenceReceived_lk__c=null;
                
                //for all 7 level's GMR Code
            if(DBL.businessLine.value != null)
                crRecord.TECH_CRReceivedGMRCode__c = DBL.businessLine.value;
            if(DBL.productLine.value != null)
                crRecord.TECH_CRReceivedGMRCode__c += DBL.productLine.value;
            if(DBL.strategicProductFamily.label != null)
                crRecord.TECH_CRReceivedGMRCode__c += DBL.strategicProductFamily.value;
            if(DBL.family.label != null)
                crRecord.TECH_CRReceivedGMRCode__c += DBL.family.value;
           }
            
        }
        else{
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Complaint Request Id cannot be blank'));
             return null;
        }       

        try{
            upsert crRecord;
            PageReference casePage;            
            casePage=new PageReference('/'+crRecord.id);                        
            return casePage;
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
            return null;
        }          
        return null;
    }
    
    public PageReference pageCancelFunction(){
        PageReference pageResult;
    
        if(crRecord.id!=null){
            pageResult = new PageReference('/'+ crRecord.id);  
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
    
    
}