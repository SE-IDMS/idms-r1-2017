/************************************************************
* Developer: Denis Aranda                                   *
* Type: Test Class                                          *
* Created Date: 15.04.2014                                  *
* Tested Class: FieloPRM_AP_NewsDetailComController         *
************************************************************/

@isTest
public without sharing class FieloPRM_AP_NewsDetailComControllerTest {
  
    public static testMethod void testUnit(){
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__News__c news = new FieloEE__News__c ();
        Insert news;
        FieloPRM_AP_NewsDetailComController controller = new FieloPRM_AP_NewsDetailComController();
        String ft2 = controller.fieldTitle;        
        controller.newsRecordId = news.id;
        String ft = controller.fieldTitle;
    }
}