/*
    Description: Test class for VFC_ReferentialDataUpdates and Batch_ReferentialDataUpdates
*/
@isTest
public class VFC_ReferentialDataUpdates_TEST{
    static testMethod void testReferentialDataUpdates(){
        Account acc = Utils_TestMethods.createAccount();
        acc.Marketsegment__c='TestSegment';
        acc.Marketsubsegment__c='Testsubsegment';
        acc.ToBeDeleted__c=true;
        insert acc;
        VFC_ReferentialDataUpdates controllerinstance=new VFC_ReferentialDataUpdates();
        controllerinstance.obj='Account';
        controllerinstance.wherecond='Marketsubsegment__c=\'Testsubsegment\'';
        controllerinstance.updateField='ToBeDeleted__c';
        controllerinstance.updateValue='true';
        controllerinstance.queryGenerate();
        Integer numBatchJobs = [SELECT count() FROM AsyncApexJob WHERE Status = 'Queued' OR Status = 'Processing'];
        if(numBatchJobs<5){
            controllerinstance.executeBatch();
            controllerinstance.checkStatus();  
        }  
        controllerinstance.queryLimit='1';
        controllerinstance.batchLimit='1';
        controllerinstance.updateField='Marketsubsegment__c';
        controllerinstance.updateValue='Testsubsegment';
        Test.startTest();
        controllerinstance.queryGenerate();
        if(numBatchJobs<5)
            controllerinstance.executeBatch();
        controllerinstance.reset();
        Test.stopTest();
    }
}