@isTest
global class AP_BoxConnectImplNew_TEST implements HttpCalloutMock {
 
    global boolean isfolderCreation;
    
    global boolean isfolderAccess;

    global AP_BoxConnectImplNew_TEST (boolean Isfolder,boolean isfldrAccess) {
     isfolderCreation=Isfolder;
     isfolderAccess=isfldrAccess;
    }
    global HTTPResponse respond(HTTPRequest req) {
       
       
       if(isfolderCreation) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/json');
           res.setBody('{"access_token": "schneiderelectricelllaa","expires_in": 3600,"restricted_to": [],"token_type": "bearer","refresh_token": "TESThjhkjhjhgkjhkjh"}');
       
        //res.setBody('{"Id":"1234","folderShareLink":"www.google.com","url":"www.google.com","expires_in":30}');
        //res.setStatus(label.CLAPR15ELLAbFO32);
        res.setStatusCode(200);
        res.setStatus(label.CLAPR15ELLAbFO32);
        return res;
        
        
    }
    
     if(isfolderAccess) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        
        res.setHeader('Content-Type', 'application/json');
         res.setBody('{"type": "collaboration","id": "123456","created_by": {"type": "user","id": "17738362","name": "ELLA HA","login": "hELLA@schneider-electric.com"},"created_at": "2016-12-12T10:54:37-08:00","modified_at": "2016-12-12T11:30:43-08:00","expires_at": null,"status": "accepted","accessible_by": {"type": "user","id": "23456","name": "sean","login": "sean+test@box.com"},"role": "editor","acknowledged_at": "2016-12-12T11:30:43-08:00","item": {"type": "folder","id": "33333333333","sequence_id": "0","etag": "0","name": "ELLA user"}}');
        //res.setBody('{"Id":"1234","folderShareLink":"www.google.com","url":"www.google.com","expires_in":30}');
        //res.setStatus(label.CLAPR15ELLAbFO32);
        res.setStatusCode(200);
        res.setStatus(label.CLAPR15ELLAbFO32);
        return res;
        
        
    }
    return null;
    }
}