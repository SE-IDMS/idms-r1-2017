@isTest
public class VFC_SRV_MyFsLoginPage_Test {
    
    @testSetup
    static void setupUserLogin() {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='testprmuserlogin.login@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='testprmuserlogin.login@schneider-electric.com' + Label.CLMAR13PRM05,
                         UserRoleId = r.Id,
                        FederationIdentifier = '188b80b1-c622-4f20-8b1d-3ae86af11c4a',
                         isActive = True,                       
                         BypassVR__c = True);
        INSERT u;       
        ApexPages.currentPage().getHeaders().put('country', 'US');
        
    }
    
    public static testMethod void testUserLoginInit_1() {
        ApexPages.currentPage().getParameters().put('SunQueryParamsString', 'IMSQueryParam');
        ApexPages.currentPage().getParameters().put('goto', 'IMSGotoParam');
        //ApexPages.currentPage().getHeaders().put('Referer', 'https://ims-sqe.btsec.dev.schneider-electric.com/opensso/UI/Login?errorMessage=auth.failed&Login.Submit=Login&goto=http%3A%2F%2Fims-sqe.btsec.dev.schneider-electric.com%2Fopensso%2Fidpssoinit%3FmetaAlias%3D%252Fidp%26spEntityID%3Dhttps%3A%2F%2Fse--devAPR.cs22.my.salesforce.com&gx_charset=UTF-8&spEntityID=https%3A%2F%2Fse--devAPR.cs22.my.salesforce.com&errorMessage=auth.failed');
        
        VFC_SRV_MyFsLoginPage loginCtrlr = new VFC_SRV_MyFsLoginPage();         
        String params = loginCtrlr.getIsSSOParamSet();
        
        loginCtrlr.redirectToSSOPage();
    }

    public static testMethod void testUserLoginInit_2() {
        ApexPages.currentPage().getParameters().put('SunQueryParamsString', '');
        ApexPages.currentPage().getParameters().put('goto', '');
        ApexPages.currentPage().getHeaders().put('Referer', System.Label.CLAPR15PRM114);
        ApexPages.currentPage().getParameters().put('country', 'US');
        VFC_SRV_MyFsLoginPage loginCtrlr = new VFC_SRV_MyFsLoginPage(); 
        String params = loginCtrlr.getIsSSOParamSet();
    }

    public static testMethod void testUserLoginInit_3() {
        ApexPages.currentPage().getParameters().put('SunQueryParamsString', '');
        ApexPages.currentPage().getParameters().put('goto', '');
        ApexPages.currentPage().getHeaders().put('Referer', '');
        
        VFC_SRV_MyFsLoginPage loginCtrlr = new VFC_SRV_MyFsLoginPage(); 
        String params = loginCtrlr.getIsSSOParamSet();
    }
    
    public static testMethod void testUserLoginInit_Error() {
        ApexPages.currentPage().getParameters().put('SunQueryParamsString', '');
        ApexPages.currentPage().getParameters().put('goto', '');
        ApexPages.currentPage().getHeaders().put('Referer', 'https://ims-sqe.btsec.dev.schneider-electric.com/opensso/UI/Login?errorMessage=auth.failed&Login.Submit=Login&goto=http%3A%2F%2Fims-sqe.btsec.dev.schneider-electric.com%2Fopensso%2Fidpssoinit%3FmetaAlias%3D%252Fidp%26spEntityID%3Dhttps%3A%2F%2Fse--devAPR.cs22.my.salesforce.com&gx_charset=UTF-8&spEntityID=https%3A%2F%2Fse--devAPR.cs22.my.salesforce.com&errorMessage=auth.failed');
        
        VFC_SRV_MyFsLoginPage loginCtrlr = new VFC_SRV_MyFsLoginPage(); 
        String params = loginCtrlr.getIsSSOParamSet();        
    }

    public static testMethod void testResetPassword_1() {
        Test.startTest();
        Test.setMock(WebServiceMock.class, new WS_MockClassPRM_Test());
        VFC_SRV_MyFsLoginPage.ResetPasswordResult resetResult = VFC_SRV_MyFsLoginPage.resetPasswordRemoting('testprmuserlogin.login@schneider-electric.com');
        System.debug('** Reset Pwd Result:' + resetResult);        
    }
}