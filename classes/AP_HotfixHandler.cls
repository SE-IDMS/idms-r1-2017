public class AP_HotfixHandler {
    
    public static void processHotFixForApproved(){
        toggleUser();
    }

    
    public static void processHotFixForMigrated(){
        toggleUser();
    }

    @future
    private  static void toggleUser()
    {
        User x=[select isactive from user where id=:Label.CLFEB13EUS01];        
        x.isactive=!x.isactive;   
        if(!Test.isRunningTest())  
        update x;
    }
}