public class AP_IMSSSOInitParams {

    public class SSOInitParams{
        public String GotoParam { get; set; }
        public String QueryParam { get; set; }
    }
    
    /*
    public static SSOInitParams getImsSsoInitParams(String EndPointUrl){
        List<map<string,object>> results = new List<map<string,object>>();
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        SSOInitParams initParams = new SSOInitParams();
        
        req.setEndpoint(EndPointUrl);
        req.setMethod('GET');
        res = http.send(req);
        System.debug('--Response1 Body--' + res.getbody());
        
        // redirection checking
        boolean redirect = false;
        if(res.getStatusCode() >=300 && res.getStatusCode() <= 307 && res.getStatusCode() != 306) {
            do {
                //System.debug('--Response Status b/w 300 to 307 except 306--');
                redirect = false; // reset the value each time
                
                String loc = res.getHeader('Location'); // get location of the redirect
                // System.debug('--Location--' + loc);
                if(loc == null) {
                    redirect = false;
                    continue;
                }
                // sending request with new endpoint.
                req = new HttpRequest();
                req.setEndpoint(loc);
                req.setMethod('GET');
                res = http.send(req);
                
                if(res.getStatusCode() != 500) { // 500 = fail
                    //System.debug('--ResponseStatusCode--'+res.getStatusCode());
                    if(res.getStatusCode() >=300 && res.getStatusCode() <= 307 && res.getStatusCode() != 306) {
                        //System.debug('--Response Status b/w 300 to 307 except 306--:' + res.getStatusCode());
                        redirect= true;
                    }
                }
            } while (redirect && Limits.getCallouts() != Limits.getLimitCallouts());
        }
        
        //System.debug('--Response--' + res);
		String responseBody = res.getBody();
        Integer gotoStartIndex = responseBody.indexOf('type="hidden" name="goto"');
        Integer gotoEndIndex = responseBody.indexOf('/>', gotoStartIndex);
        Integer queryParamStartIndex = responseBody.indexOf('type="hidden" name="SunQueryParamsString"');
        Integer queryParamEndIndex = responseBody.indexOf('/>', queryParamStartIndex);
        
        String gotoInput = responseBody.substring(gotoStartIndex, gotoEndIndex);
        String queryParamInput = responseBody.substring(queryParamStartIndex, queryParamEndIndex);
        String gotoValue = '';
        String queryParamValue = '';
        if (String.isNotBlank(gotoInput) && gotoInput.indexOf('value=') > 0) {
        	gotoValue = gotoInput.substring(gotoInput.indexOf('value=') + 7);
            if (String.isNotBlank(gotoValue)) {
                System.debug('** Ending with quotes');
                gotoValue = gotoValue.trim().substring(0, gotoValue.trim().length() - 1);
            }
        }
        if (String.isNotBlank(queryParamInput) && queryParamInput.indexOf('value=') > 0) {
        	queryParamValue = queryParamInput.substring(queryParamInput.indexOf('value=') + 7);
            if (String.isNotBlank(queryParamValue)) {
                System.debug('** Ending with quotes');
                queryParamValue = queryParamValue.trim().substring(0, queryParamValue.trim().length() - 1);
            }
        }
        
        System.debug('Goto value: ' + gotoValue);
        System.debug('Query Param value:' + queryParamValue);
        initParams.GotoParam = gotoValue;
        initParams.QueryParam = queryParamValue;
        
		return initParams;
    }
    */
}