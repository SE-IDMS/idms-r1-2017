/*
    Author          : Abhishek (ACN) 
    Date Created    : 01/08/2011
    Description     : Test class for the apex class VFC53_PlatformingScoringsCompleted .
*/
@isTest
private class VFC53_PlatformingScoringsCompleted_TEST {
    
/* @Method <This method deletePlatformingsTestMethod is used test the VFC53_PlatformingScoringsCompleted class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Throwing an Error>
*/
    static testMethod void checkAsigneeTestMethod() {

        Profile profile = [select id from profile where name='System Administrator'];
    
        User user = Utils_TestMethods.createStandardUser(profile.id,'TestScor');
        user.BypassVR__c=True;
        user.CAPRelevant__c=True;
        Database.insert(user);   
            
        Account accounts = Utils_TestMethods.createAccount();
        accounts.ownerId=user.id;
        Database.insert(accounts); 
        
        list<SFE_IndivCAP__c> Cap= new list<SFE_IndivCAP__c>();
    
        SFE_IndivCAP__c caps = Utils_TestMethods.createIndividualActionPlan(user.id);       
        database.insert(caps);
    
        test.startTest();
        ApexPages.StandardController sc1= new ApexPages.StandardController(caps);
        Pagereference p = page.VFP53_PlatformingScoringsCompleted;
        Test.setCurrentPageReference(p); 
        VFC53_PlatformingScoringsCompleted scoring = new VFC53_PlatformingScoringsCompleted(sc1);
        scoring.checkAsignee();
    
        system.runAs(user) {
            ApexPages.StandardController sc2= new ApexPages.StandardController(caps); 
            VFC53_PlatformingScoringsCompleted score = new VFC53_PlatformingScoringsCompleted(sc2);
            try {
                score.checkAsignee();
                score.updateCAP();
            }
            catch (DMLException e) {
                system.assert(e.getMessage().contains(Label.CL00451));                        
            }              
        }
        test.stopTest();    
    }

    static testMethod void checkAsigneeAgentsTestMethod() {

        Profile profile = [select id from profile where name='System Administrator'];
        Id agentProfileId = [SELECT Id FROM Profile WHERE name='SE - Agent (US)' LIMIT 1].Id;

        User user = Utils_TestMethods.createStandardUser(profile.id,'TestScor');
        user.BypassVR__c=True;
        user.CAPRelevant__c=True;
        Database.insert(user);   
        
        Country__c cntry = Utils_TestMethods.createCountry();
        insert cntry;
        
        Account agentAccount = Utils_TestMethods.createAccount();
        agentAccount.Country__c = cntry.Id;
        insert agentAccount;

        agentAccount.IsPartner = True;
        update agentAccount;

        List<Contact> lstAgentContacts = new List<Contact>();
        Contact agentContact = new Contact(
                  FirstName='Test',
                  LastName='lastname',
                  AccountId=agentAccount.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  WorkPhone__c='1234567890',
                  Country__c=cntry.Id
        );   
        lstAgentContacts.add(agentContact);

        Contact agentContact2 = new Contact(
                  FirstName='Test',
                  LastName='lastname',
                  AccountId=agentAccount.Id,
                  JobTitle__c='Z3',
                  CorrespLang__c='EN',
                  WorkPhone__c='1234567890',
                  Country__c=cntry.Id
        );   
        lstAgentContacts.add(agentContact2);
        Insert lstAgentContacts;

        List<User> lstAgentUsers = new List<User>(); 
        User   u1= new User(Username = 'agents.testUserOne@bfo-portal.com', LastName = 'User11', alias = 'tuser1',
                          CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                          Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = agentProfileId,
                          ContactID = agentContact.Id, UserPermissionsSFContentUser=true );        
        lstAgentUsers.add(u1);

        User   u2 = new User(Username = 'agents.testUserTwo@bfo-portal.com', LastName = 'User11', alias = 'agtusr1',
                          CommunityNickName = 'atestUser12', TimeZoneSidKey = 'America/Chicago', 
                          Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                          LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = agentProfileId,
                          ContactID = agentContact2.Id, UserPermissionsSFContentUser=true );        
        lstAgentUsers.add(u2);
        Insert lstAgentUsers;

        Account accounts = Utils_TestMethods.createAccount();
        accounts.ownerId=u1.id;
        Database.insert(accounts); 

        list<SFE_IndivCAP__c> Cap= new list<SFE_IndivCAP__c>();
    
        SFE_IndivCAP__c caps = Utils_TestMethods.createIndividualActionPlan(u1.id);       
        database.insert(caps);
    
        test.startTest();
        ApexPages.StandardController sc1= new ApexPages.StandardController(caps);
        Pagereference p = page.VFP53_PlatformingScoringsCompleted;
        Test.setCurrentPageReference(p); 
        VFC53_PlatformingScoringsCompleted scoring = new VFC53_PlatformingScoringsCompleted(sc1);
        scoring.checkAsignee();
    
        system.runAs(user) {
            caps.AssignedTo__c = u2.Id;  
            update caps;

            ApexPages.StandardController sc2= new ApexPages.StandardController(caps); 
            VFC53_PlatformingScoringsCompleted score = new VFC53_PlatformingScoringsCompleted(sc2);
            try {
                score.checkAsignee();
                score.updateCAP();
            }
            catch (DMLException e) {
                system.assert(e.getMessage().contains(Label.CL00451));                        
            }              
        }
        test.stopTest();    
    }
}