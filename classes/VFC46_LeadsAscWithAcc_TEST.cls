/*
    Author          : ACN 
    Date Created    : 20/07/2011
    Description     : Test class for VFC46_LeadsAscWithAcc class 
*/
@isTest
private class VFC46_LeadsAscWithAcc_TEST 
{
    
    @testSetup static void testDataForLead(){
        Country__c country= new Country__c(Name = 'USA', CountryCode__c = 'US');
        insert country;
          
        StateProvince__c StateProvince = new StateProvince__c(Name = 'Alaska', StateProvinceCode__c = 'AK', Country__c = country.Id, CountryCode__c = 'US');
        insert StateProvince;
    }
    static testMethod void testdisplayLeads() 
    {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
         User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC43'); 
         user.bypassWF__c = false;            
         system.runas(user)
         {
            test.startTest();
           // Country__c country= Utils_TestMethods.createCountry();
           // insert country;
          
            Country__c country = [select id from Country__c WHERE Name = 'USA'];
            StateProvince__c StateProvince = [Select Id from StateProvince__c WHERE Country__c =:country.Id Limit 1];
            
            Lead lead2 = Utils_TestMethods.createLead(null, country.Id,100);
            lead2.StateProvince__c = StateProvince.Id;
            insert lead2;

            lead2.ClassificationLevel1__c='LC';
            lead2.LeadingBusiness__c='BD';
            lead2.SuspectedValue__c=100;
            lead2.ZipCode__c='123789';
            lead2.OpportunityFinder__c ='Lead Agent';
            lead2.City__c='US';
            lead2.Street__c = 'testStreet';
            update lead2;
            
            Database.LeadConvert leadConv = new database.LeadConvert();
            leadConv.setLeadId(lead2.id);
            leadConv.setConvertedStatus('3 - Closed - Opportunity Created');
            
            Database.LeadConvertResult lcr = Database.convertLead(leadConv);
                        
            lead2 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,convertedAccountId,Account__c,TECH_OpportunityOwner__c,ConvertedContactId,opportunity__c from lead where Id =: lead2.Id limit 1];            
            
            System.assertEquals('3 - Closed - Opportunity Created',lead2.Status);
            System.assertEquals('Opportunity Created',lead2.SubStatus__c);            
            System.assertEquals(lead2.Account__c,lead2.ConvertedAccountId);            
            Account acc = [select Id from Account where Id=:lead2.ConvertedAccountId limit 1];
            
            Lead lead1 = Utils_TestMethods.createLeadWithAcc(acc.Id, country.Id,100);
            insert lead1;            
            lead1 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Account__c,Contact__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            System.assertEquals(acc.Id,lead1.Account__c);

            ApexPages.StandardController sc = new ApexPages.StandardController(acc);
            VFC46_LeadsAscWithAcc leadsAscWithAcc= new VFC46_LeadsAscWithAcc(sc);
            leadsAscWithAcc.displayLeads();
            leadsAscWithAcc.getgotoNewLead();
            test.stopTest();
          }
        }
    }
    static testMethod void testdisplayLeads2() 
    {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
         User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC43'); 
         user.bypassWF__c = false;            
         system.runas(user)
         {
            test.startTest();
           // Country__c country= Utils_TestMethods.createCountry();
           // insert country;
           
          // Country__c country= new Country__c(Name = 'Canada', CountryCode__c = 'CA');
          // insert country;
          
          Country__c country = [select id from Country__c WHERE Name = 'USA'];
          StateProvince__c StateProvince = [Select Id from StateProvince__c WHERE Country__c =:country.Id Limit 1];
            
           Lead lead2 = new Lead(FirstName = 'test', LastName  ='testLead', Street__c='testStreet', Country__c = country.Id, Priority__c = 100, Email = 'testLead@accenture.com',  OpportunityType__c = 'Standard',OpportunityScope__c = 'BDPRD--Building Management Products',POBOx__c = 'xxx');
           lead2.StateProvince__c = StateProvince.Id;
           insert lead2;

            //lead2.ClassificationLevel1__c='LC';
            lead2.LeadingBusiness__c='BD';
            lead2.SuspectedValue__c=100;
            lead2.ZipCode__c='123789';
            lead2.OpportunityFinder__c ='Lead Agent';
            lead2.City__c='US';
            lead2.Street__c = 'testStreet';
            update lead2;
            
            Database.LeadConvert leadConv = new database.LeadConvert();
            leadConv.setLeadId(lead2.id);
            leadConv.setConvertedStatus('3 - Closed - Opportunity Created');
            
            Database.LeadConvertResult lcr = Database.convertLead(leadConv);
                        
            lead2 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,convertedAccountId,Account__c,TECH_OpportunityOwner__c,ConvertedContactId,opportunity__c from lead where Id =: lead2.Id limit 1];            
            
            //System.assertEquals('3 - Closed - Opportunity Created',lead2.Status);
            //System.assertEquals('Opportunity Created',lead2.SubStatus__c);            
            //System.assertEquals(lead2.Account__c,lead2.ConvertedAccountId);            
            Account acc = [select Id from Account where Id=:lead2.ConvertedAccountId limit 1];
            
            Lead lead1 = Utils_TestMethods.createLeadWithAcc(acc.Id, country.Id,100);
            lead1.Company = null;
            insert lead1;            
            lead1 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Account__c,Contact__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            System.assertEquals(acc.Id,lead1.Account__c);

            ApexPages.StandardController sc = new ApexPages.StandardController(acc);
            VFC46_LeadsAscWithAcc leadsAscWithAcc= new VFC46_LeadsAscWithAcc(sc);
            leadsAscWithAcc.displayLeads();
            leadsAscWithAcc.getgotoNewLead();
            test.stopTest();
          }
        }
    }
}