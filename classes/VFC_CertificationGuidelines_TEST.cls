/*
    Author          : Bhuvana Subramaniyan
    Description     : Test class for VFC_CertificationGuidelines.
*/


@isTest
private class VFC_CertificationGuidelines_TEST 
{
    static testMethod void testcertificationGuidelines()
    {
        test.startTest();
        CFWRiskAssessment__c cfwr = new CFWRiskAssessment__c();
        cfwr.ProjectName__c = 'testProjectName';
        cfwr.SubmittedforApproval__c = true;
        insert cfwr;
        
        CertificationGuidelines__c certGuideline= new CertificationGuidelines__c();
        certGuideline.CertificationRiskAssessment__c = cfwr.Id;
        insert certGuideline;
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(certGuideline);
        VFC_CertificationGuidelines certGuidelines = new  VFC_CertificationGuidelines(sc1);
        
        certGuidelines.urlValue = Label.CLOCT15CFW01;
        certGuidelines.urlPageRedirect();       
        
        certGuidelines.urlValue = 'VFP_CertificationChecklist';
        certGuidelines.urlPageRedirect();  
        
        certGuidelines.urlValue = 'VFP_CFWGuidelinesPolicies';
        certGuidelines.urlPageRedirect();  
        
        certGuidelines.urlValue = 'VFP_RiskAssessment';
        certGuidelines.urlPageRedirect();       
        
        CertificationChecklist__c cc = new CertificationChecklist__c();
        cc.CertificationRiskAssessment__c = cfwr.Id;
        insert cc;
        
        certGuidelines.urlValue = 'VFP_CertificationChecklist';
        certGuidelines.urlPageRedirect(); 
        
        certGuidelines.urlValue = 'VFP_CFWGuidelinesPolicies';
        certGuidelines.urlPageRedirect();  
        certGuidelines.SaveGuidelines();
        certGuidelines.EditGuidelines();
        certGuidelines.CancelPage();
        test.stopTest();
    }
}