/**
Description: This covers the UIMSUserAfterInsert class
**/
@IsTest
public class IDMS_UIMSUserAfterInsertTest{
    
    public static testMethod void idmsUimsTest(){
        UIMS_User__c uimsUser1 = new UIMS_User__c();
        uimsUser1.email__c = 'toto@yopmail.com';
        insert uimsUser1;
        
        UIMS_User__c uimsUser2 = new UIMS_User__c();
        uimsUser2.phoneId__c= '0033673467263';
        insert uimsUser2;
        
    }
}