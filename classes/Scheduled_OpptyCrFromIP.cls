/*
    Author              : Deepak Kumar (Schneider Electric)
    Date Created        :  01-April-2015
    Description         : Scheduler class to Schedule Batch_ObsolenceOpportunityCreation and Batch_EndWarrOpportunityCreation
*/

global class Scheduled_OpptyCrFromIP implements Schedulable{
     global void execute(SchedulableContext sc) {
        Batch_EndWarrOpportunityCreation b = new Batch_EndWarrOpportunityCreation();
        database.executebatch(b);
        Batch_ObsolenceOpportunityCreation  c = new Batch_ObsolenceOpportunityCreation();
        database.executebatch(c);
  }
}