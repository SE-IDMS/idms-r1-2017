global without sharing class SEInsideSales360_Services {
    @AuraEnabled
    global static Account getAccountById(String accountId) {
        List<Account> acountList = [SELECT Id, MarketSegment__c, AccountMasterProfile__c, Account.Owner.Name, Fax, Website, toLabel(LeadingBusiness__c), toLabel(ClassLevel1__c),  Name, Phone, BillingCountry, BillingState, BillingCity, BillingStateCode, BillingStreet, BillingPostalCode, 
            (SELECT Id, Score__c, MarketShare__c, TotalPAM__c, DirectSales__c, IndirectSales__c FROM Account_Master_Profiles__r),
            (SELECT Id, Status__c, EventType__c, SalesEventType__c, Subject, ActivityDate, StartDateTime, What.Name, What.Id, Description, Status, Who.Name, Who.Id, Priority, IsTask FROM OpenActivities WHERE (IsTask=TRUE AND ActivityDate <= NEXT_N_DAYS:90 AND ActivityDate >= LAST_N_DAYS:90) OR (IsTask=FALSE AND StartDateTime <= NEXT_N_DAYS:90 AND StartDateTime >= LAST_N_DAYS:90)),
            (SELECT Id, Status__c, EventType__c, SalesEventType__c, Subject, ActivityDate, StartDateTime, What.Name, What.Id, Description, Status, Who.Name, Who.Id, Priority, IsTask FROM ActivityHistories WHERE (IsTask=TRUE AND ActivityDate <= NEXT_N_DAYS:90 AND ActivityDate >= LAST_N_DAYS:90) OR (IsTask=FALSE AND StartDateTime <= NEXT_N_DAYS:90 AND StartDateTime >= LAST_N_DAYS:90)),
            (SELECT Id, Contact__r.Name, Name, NonMarcommCampaign__r.Name FROM Leads__r),
            (SELECT Id, CaseNumber, Contact.Name, Subject, Priority, CreatedDate, Status, Owner.Name FROM Cases ORDER BY CreatedDate DESC),
            (SELECT Id, Name, Email, JobDescr__c, toLabel(JobFunc__c), toLabel(JobTitle__c), WorkPhone__c, WorkPhoneExt__c, MobilePhone, PRMMobilePhone__c  FROM Contacts WHERE Contact.AccountId =:accountId ORDER BY Name ASC),
            (SELECT id, Name, Amount, StageName, CloseDate, Account.Name, SourceReference__c, OpptyType__c, Probability, Owner.Name FROM Opportunities WHERE Account.Id=:accountId ORDER BY CloseDate DESC)
            FROM Account WHERE Id=:accountId];
  
        if (acountList.size() > 0) {
            return acountList[0];
        } 
        return null;
    }    
    
    @AuraEnabled
    global static Account getAccountByContact(String contactId) {
        List<Contact> contactList = [SELECT Account.Id FROM Contact WHERE Id=:contactId];
        String accountId = null;
        if (contactList.size() > 0) {
            accountId = contactList[0].Account.Id;
            return getAccountById(accountId);
        }
        
        return null;
    }    
    
    @AuraEnabled
    global static String getCurrentUserId() {
        return UserInfo.getUserId();
    }

    @AuraEnabled
    global static String getEventRecordTypeId(String developerName) {
        List<RecordType> rtList = [SELECT Id,DeveloperName FROM RecordType WHERE SobjectType='Event' AND DeveloperName=:developerName];
        if (rtList.size() > 0) {
            return rtList[0].Id;
        }
        return null;
    }    
  
}