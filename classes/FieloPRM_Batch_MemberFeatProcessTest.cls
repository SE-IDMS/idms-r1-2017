/********************************************************************
* Company: Fielo
* Created Date: 27/07/2016
* Description: Test class for batch FieloPRM_Batch_MemberFeatProcess
********************************************************************/
@isTest
public with sharing class FieloPRM_Batch_MemberFeatProcessTest{

    public testMethod static void testUnit2(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);
     
            Account acc = new Account();
            acc.Name = 'test acc';
            insert acc;
            
            Account accTo = new Account();
            accTo.Name = 'test acc to';
            insert accTo;

            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__FirstName__c = 'USUARIO UNIT TEST ';
            member.FieloEE__Street__c = 'test';
            member.F_Account__c = acc.id;
                 
            insert member;

            Contact con = [Select id FROM contact WHERE FieloEE__Member__c = : member.id ];

            con.PRMUIMSId__c = '123TEST';

            update con;

            list<FieloPRM_Feature__c> listFeatureinsert = new list<FieloPRM_Feature__c>();
            FieloPRM_Feature__c feature = new FieloPRM_Feature__c();
            feature.name = 'feature test';
            feature.F_PRM_FeatureCode__c = 'feature api name';
            feature.F_PRM_CustomLogicClass__c = 'FieloPRM_CustomLogicImplementTest';
            
            listFeatureinsert.add(feature);

            FieloPRM_Feature__c feature2 = new FieloPRM_Feature__c();
            feature2.name = 'feature test2';
            feature2.F_PRM_FeatureCode__c = 'feature api name2';
            listFeatureinsert.add(feature2);
            
            FieloPRM_Feature__c feature3 = new FieloPRM_Feature__c();
            feature3.name = 'feature test3';
            feature3.F_PRM_FeatureCode__c = 'feature api name3';
            listFeatureinsert.add(feature3);

            insert listFeatureinsert;

            list<FieloPRM_MemberFeature__c> listMemberFeature = new list<FieloPRM_MemberFeature__c>();

            FieloPRM_MemberFeature__c memberFeature = new FieloPRM_MemberFeature__c();
            memberFeature.F_PRM_Feature__c =  feature.id;
            memberFeature.F_PRM_Member__c =  member.id;
            memberFeature.F_PRM_IsActive__c  =  true;
            memberFeature.PRMUIMSId__c = '123TEST';
            //insert memberFeature;
            listMemberFeature.add(memberFeature);

            FieloPRM_MemberFeature__c memberfeature1 = new FieloPRM_MemberFeature__c();
            memberfeature1.F_PRM_Feature__c =  feature2.id;
            memberfeature1.F_PRM_Member__c =  member.id;
            memberFeature1.F_PRM_IsActive__c  =  true;
            memberFeature1.PRMUIMSId__c = '123TEST';
            listMemberFeature.add(memberfeature1);
            
            FieloPRM_MemberFeature__c memberfeature2 = new FieloPRM_MemberFeature__c();
            memberfeature2.F_PRM_Feature__c =  feature3.id;
            memberfeature2.F_PRM_Member__c =  member.id;
            memberFeature2.F_PRM_IsActive__c  =  true;
            memberFeature2.PRMUIMSId__c = '123TEST';
            listMemberFeature.add(memberfeature2);

            CS_PRM_ApexJobSettings__c PRMSettings = new CS_PRM_ApexJobSettings__c(
                Name = 'FieloPRM_Batch_MemberFeatProcess',
                InitialSync__c = true,
                F_PRM_BatchSize__c = 200
            );
            insert PRMSettings;

            insert listMemberFeature;
            listMemberFeature[0].F_PRM_Status__c  = 'TOPROCESSACTIVE';
            listMemberFeature[1].F_PRM_Status__c  = 'TOPROCESSINACTIVE';
            listMemberFeature[2].F_PRM_Status__c  = 'TOREPROCESSACTIVE';
            update listMemberFeature;  

            Id batchJobId = Database.executeBatch(new FieloPRM_Batch_MemberFeatProcess());
                        
            FieloPRM_Batch_MemberFeatProcess bat = new FieloPRM_Batch_MemberFeatProcess();
            bat.execute(null);
            
            FieloPRM_Batch_MemberFeatProcess.runSync(listMemberFeature);
            FieloPRM_Batch_MemberFeatProcess.process(listMemberFeature);
            FieloPRM_Batch_MemberFeatProcess.scheduleNow();     
            FieloPRM_Batch_MemberFeatProcess.schedule(null);
        }
    }     
               
}