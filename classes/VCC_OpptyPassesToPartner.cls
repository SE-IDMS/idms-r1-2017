public class VCC_OpptyPassesToPartner{
    
    public Opportunity opty {get;set;}
    public User usr {get;set;}
    public Contact con {get;set;}
    public string urlVal{get;set;}
    
    public String RecptId ;
    public String getRecptId(){
        return RecptId;
    }
    public void setRecptId(String Value)
    {
        RecptId = Value;
        usr = [select id, name, Email from User where id=:RecptId];
    }

    private String OpptyId;
    public String getOpptyId()
    {
        return OpptyId;
    } 
    public void setOpptyId(String Value){
        OpptyId = Value;
        urlVal = URL.getSalesforceBaseUrl().toExternalForm()+'/'+OpptyId;
        opty  = [select id, Name,SEReference__c, Account.name, Account.Phone, AccountId from Opportunity where id=:OpptyId ];
        
        List<OPP_ValueChainPlayers__c> lsOVCP = [select Id, Name, Contact__c, ContactRole__C from OPP_ValueChainPlayers__c WHERE OpportunityName__c=:opty.id AND Contact__c != null AND ContactRole__c = :System.Label.CLSEP12PRM16];
        if(lsOVCP.size() > 0)
        {
            Con = [select id, name, Phone, Email,HomePhone,AssistantPhone,MobilePhone, OtherPhone,Aphone__c, WorkPhone__c from Contact where id=:lsOVCP[0].contact__c];   
        }
    }
    
    public VCC_OpptyPassesToPartner()
    {/*
        opty  = [select id, Name, Account.name, Account.Phone, AccountId from Opportunity where id=:OpptyId ];
        usr = [select id, name, Email from User where id=:RecptId];
        List<OPP_ValueChainPlayers__c> lsOVCP = [select Id, Name, Contact__c, ContactRole__C from OPP_ValueChainPlayers__c where OpportunityName__c=:op.id and Contact__c != null];
        if(lsOVCP.size() > 0)
        {
            Con = [select id, name, Phone, Email from Contact where id=:lsOVCP[0].contact__c];   
        }
     */       
    }
    
   public String NewLine {
        get { return '\r\n'; }
        set;
   }
   

    
}