/**************************
    Author: Fielo Team
***************************/
@isTest
public with sharing class FieloPRM_VFC_InvoiceStatusUpdateContTest{
  
    public static testMethod void testUnit1(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        User u = new User();
         
        FieloEE__Component__c comp;
        
        FieloEE__Member__c mem = new FieloEE__Member__c();
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            mem.FieloEE__FirstName__c = 'test'; 
            mem.FieloEE__LastName__c = 'test';
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc = new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;

            FieloPRM_Invoice__c invoice = new FieloPRM_Invoice__c(
                Name = 'test',
                F_PRM_InvoiceDate__c = date.today(),
                F_PRM_Member__c = mem.Id,
                F_PRM_RetailerAccount__c = acc.Id
            );
            
            insert invoice;
            
            invoice = [SELECT Id, Name, F_PRM_InvoiceDate__c, F_PRM_Member__c, F_PRM_Status__c, F_PRM_RetailerAccount__c FROM FieloPRM_Invoice__c WHERE Id =: invoice.Id];

            ApexPages.StandardController stdController = new ApexPages.StandardController(invoice);
            FieloPRM_VFC_InvoiceStatusUpdateCont ext = new FieloPRM_VFC_InvoiceStatusUpdateCont(stdController);
            ext.doUpdateStatus();
            ext.doGoToInv();
        
        }

    }
    
    public static testMethod void testUnit2(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        User u = new User();
         
        FieloEE__Component__c comp;
        
        FieloEE__Member__c mem = new FieloEE__Member__c();
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            mem.FieloEE__FirstName__c = 'test'; 
            mem.FieloEE__LastName__c = 'test';
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc = new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;

            FieloPRM_Invoice__c invoice = new FieloPRM_Invoice__c(
                Name = 'test',
                F_PRM_InvoiceDate__c = date.today(),
                F_PRM_Member__c = mem.Id,
                F_PRM_RetailerAccount__c = acc.Id
            );
            
            insert invoice;
            
            FieloPRM_InvoiceDetail__c invoiceDetail = new FieloPRM_InvoiceDetail__c(
                F_PRM_Invoice__c = invoice.Id,
                F_PRM_UnitPrice__c = 100,
                F_PRM_Volume__c = 1
            );
            
            insert invoiceDetail;
            
            invoice = [SELECT Id, Name, F_PRM_InvoiceDate__c, F_PRM_Member__c, F_PRM_Status__c, F_PRM_RetailerAccount__c FROM FieloPRM_Invoice__c WHERE Id =: invoice.Id];

            ApexPages.StandardController stdController = new ApexPages.StandardController(invoice);
            FieloPRM_VFC_InvoiceStatusUpdateCont ext = new FieloPRM_VFC_InvoiceStatusUpdateCont(stdController);
            ext.doUpdateStatus();
            ext.doGoToInv();
        
        }

    }    

    public static testMethod void testUnit3(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        User u = new User();
         
        FieloEE__Component__c comp;
        
        FieloEE__Member__c mem = new FieloEE__Member__c();
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            mem.FieloEE__FirstName__c = 'test'; 
            mem.FieloEE__LastName__c = 'test';
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc = new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;

            FieloPRM_Invoice__c invoice = new FieloPRM_Invoice__c(
                Name = 'test',
                F_PRM_InvoiceDate__c = date.today(),
                F_PRM_Member__c = mem.Id,
                F_PRM_RetailerAccount__c = acc.Id
            );
            
            insert invoice;
            
            invoice.F_PRM_Status__c = 'Rejected';
            
            update invoice;
            
            invoice = [SELECT Id, Name, F_PRM_InvoiceDate__c, F_PRM_Member__c, F_PRM_Status__c, F_PRM_RetailerAccount__c FROM FieloPRM_Invoice__c WHERE Id =: invoice.Id];

            ApexPages.StandardController stdController = new ApexPages.StandardController(invoice);
            FieloPRM_VFC_InvoiceStatusUpdateCont ext = new FieloPRM_VFC_InvoiceStatusUpdateCont(stdController);
            ext.doUpdateStatus();
            ext.doGoToInv();
        
        }

    }

    public static testMethod void testUnit4(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        User u = new User();
         
        FieloEE__Component__c comp;
        
        FieloEE__Member__c mem = new FieloEE__Member__c();
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            mem.FieloEE__FirstName__c = 'test'; 
            mem.FieloEE__LastName__c = 'test';
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc = new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;

            FieloPRM_Invoice__c invoice = new FieloPRM_Invoice__c(
                Name = 'test',
                F_PRM_InvoiceDate__c = date.today(),
                F_PRM_Member__c = mem.Id,
                F_PRM_RetailerAccount__c = acc.Id
            );
            
            insert invoice;
            
            invoice.F_PRM_Status__c = 'Approved';
            
            update invoice;
            
            invoice = [SELECT Id, Name, F_PRM_InvoiceDate__c, F_PRM_Member__c, F_PRM_Status__c, F_PRM_RetailerAccount__c FROM FieloPRM_Invoice__c WHERE Id =: invoice.Id];

            ApexPages.StandardController stdController = new ApexPages.StandardController(invoice);
            FieloPRM_VFC_InvoiceStatusUpdateCont ext = new FieloPRM_VFC_InvoiceStatusUpdateCont(stdController);
            ext.doUpdateStatus();
            ext.doGoToInv();
        
        }

    }

    public static testMethod void testUnit5(){
        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        User u = new User();
         
        FieloEE__Component__c comp;
        
        FieloEE__Member__c mem = new FieloEE__Member__c();
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            mem.FieloEE__FirstName__c = 'test'; 
            mem.FieloEE__LastName__c = 'test';
            Insert mem;
            
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            Insert deactivate;
            
            Account acc = new Account(
                Name = 'test', 
                Street__c = 'Some Street', 
                ZipCode__c = '012345'
            );
            Insert acc;

            FieloPRM_Invoice__c invoice = new FieloPRM_Invoice__c(
                Name = 'test',
                F_PRM_InvoiceDate__c = date.today(),
                F_PRM_Member__c = mem.Id
            );
            
            insert invoice;
            
            invoice.F_PRM_Status__c = 'Approved';
            
            update invoice;
            
            invoice = [SELECT Id, Name, F_PRM_InvoiceDate__c, F_PRM_Member__c, F_PRM_Status__c, F_PRM_RetailerAccount__c FROM FieloPRM_Invoice__c WHERE Id =: invoice.Id];

            ApexPages.StandardController stdController = new ApexPages.StandardController(invoice);
            FieloPRM_VFC_InvoiceStatusUpdateCont ext = new FieloPRM_VFC_InvoiceStatusUpdateCont(stdController);
            ext.doUpdateStatus();
            ext.doGoToInv();
        
        }

    }
    
}