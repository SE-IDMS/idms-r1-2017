@isTest private class TRG_COIN_IntroProjectAfterInsertTEST{

static testMethod void IntroductionProjectAfterInsertTestMethod() {    
Milestone1_Project__c  offer1 = new Milestone1_Project__c (Name = 'Offert Test01', RecordTypeId = Label.COIN038_TAG038);
Milestone1_Project__c  offer2 = new Milestone1_Project__c (Name = 'Offert Test02', RecordTypeId = Label.COIN038_TAG038);
Milestone1_Project__c  offer3 = new Milestone1_Project__c (Name = 'Offert Test03', RecordTypeId = Label.COIN038_TAG038);
Milestone1_Project__c  offer4 = new Milestone1_Project__c (Name = 'Offert Test04', RecordTypeId = Label.COIN038_TAG038);
Milestone1_Project__c  offer5 = new Milestone1_Project__c (Name = 'Offert Test05', RecordTypeId = Label.COIN038_TAG038);


Test.startTest();

insert offer1;
insert offer2;
insert offer3;
insert offer4;
insert offer5;

List<Milestone1_Milestone__c> lstProject = [select id, name from Milestone1_Milestone__c where id=:offer1.id];
//System.assertEquals(1, lstProject.size()); 

Test.stopTest();


    }
    
}