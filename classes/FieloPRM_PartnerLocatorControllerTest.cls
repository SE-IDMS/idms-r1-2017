@Istest
private with sharing class FieloPRM_PartnerLocatorControllerTest{

	static testMethod void testPLComponentController(){
		
		FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;
        
        Account acc= new Account(
            Name = 'test',
            Street__c = 'Some Street',
            ZipCode__c = '012345',
            PLDataPool__c ='en_US2'
        );
        insert acc;
                
        FieloEE__Member__c mem = new FieloEE__Member__c(
            FieloEE__FirstName__c = 'test',
            FieloEE__LastName__c= 'test'
        );
        insert mem;
    
        FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
        insert memb;
        
        FieloEE.MemberUtil.setmemberId(memb.Id);
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c(
            FieloEE__Title__c = 'test'
        );
        insert menu;

        Fielo_CRUDObjectSettings__c testCrud = new Fielo_CRUDObjectSettings__c(
            F_ObjectAPIName__c = 'Account'
        );
        insert testCrud;
    
        FieloEE__Component__c component = new FieloEE__Component__c(
            FieloEE__Menu__c = Menu.Id,
            F_CRUDObjectSettings__c = testCrud.Id
        );
        insert component;

        ComponentParameter__c compParameters = new  ComponentParameter__c(Component__c=component.Id,BottomLabel__c='',BottomLink__c='http://www.google.com', BusinessTypeValues__c='', ShowBusinessTypeAndAreaOfFocus__c=false, SubmitURL__c='http://www.google.com', Subtitle__c='subTitle',Title__c='Title',BusinessTypeDefaultValue__c='WD',DistanceUnitValues__c='##DEF##km\r\nml',DistanceValues__c='##DEF##50\r\n100',AreaOfFocusExcludeValues__c='impossibleValue\r\nimpossibleValue2');
        insert compParameters;

        FieloPRM_VFP_PartnerLocatorController myController = new FieloPRM_VFP_PartnerLocatorController();
		myController.currentComponentId = component.Id;
		FieloPRM_VFP_PartnerLocatorController.PartnerLocatorComponentWrapper myWrapper =  myController.currentComponentWithParameter;
		System.assertEquals('Title',myWrapper.title);
		System.assertEquals('subTitle',myWrapper.subTitle);
		System.assertEquals('km',myWrapper.defaultDistanceUnit);
		System.assertEquals(2,myWrapper.areaOfFocusExcludeValues.size());
		

		Schema.DescribeFieldResult fieldResult = Account.PLBusinessType__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        System.assertEquals(ple.size(),myController.getPLBusinessType().size());


        fieldResult = Account.PLAreaOfFocus__c.getDescribe();
        ple = fieldResult.getPicklistValues();
        System.assertEquals(ple.size(),myController.getPLAreaOfFocus().size());
        System.assertEquals(2,myController.getDistanceUnitValues().size());
        System.assertEquals(2,myController.getDistanceValues().size());
        
     }

}