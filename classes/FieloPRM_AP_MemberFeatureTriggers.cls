/********************************************************************
* Company: Fielo
* Developer: Waldemar Mayo
* Created Date: 31/07/2015
* Description: 
********************************************************************/
public without sharing class FieloPRM_AP_MemberFeatureTriggers {

    public static Boolean isRunning = false;
    public static Boolean isDelete = false;
    
    public static void executeCustomLogic(List<FieloPRM_MemberFeature__c> newFeats){
        FieloPRM_Schedule_MemberFeature.scheduleNow(true, newFeats);
    }

    public static void executeDemoteCustomLogic(List<FieloPRM_MemberFeature__c> newFeats){
        FieloPRM_Schedule_MemberFeature.scheduleNow(false, newFeats);
    }
    
    public static void detailCreation(){
        
        List<FieloPRM_MemberFeature__c> triggerNew = trigger.new;
        list<RecordType> listRecordType = [Select id,DeveloperName,Name,SobjectType FRom RecordType WHERE developerNAme = 'Manual'];
        map<string, RecordType> mapRecordType = new map<string, RecordType>();
        
        for(RecordType rt: listRecordType){
            mapRecordType.put(rt.SobjectType + rt.DeveloperName,rt);
        }
        
        list<FieloPRM_MemberFeatureDetail__c> listMemberFeatureDetailInsert = new list<FieloPRM_MemberFeatureDetail__c>();
        for(FieloPRM_MemberFeature__c memberFeature : triggerNew){
            if(memberFeature.RecordTypeID ==  mapRecordType.get( 'FieloPRM_MemberFeature__c' +'Manual').id){
                FieloPRM_MemberFeatureDetail__c newMemberFeatureDetail = new FieloPRM_MemberFeatureDetail__c();
                newMemberFeatureDetail.F_PRM_User__c = userinfo.getuserid();
                newMemberFeatureDetail.F_PRM_isActive__c = true;
                newMemberFeatureDetail.RecordTypeID = mapRecordType.get('FieloPRM_MemberFeatureDetail__c' + 'Manual').id;
                newMemberFeatureDetail.F_PRM_MemberFeature__c = memberfeature.id;
                listMemberFeatureDetailInsert.add(newMemberFeatureDetail);
            }
        }
        insert listMemberFeatureDetailInsert;
    }
    
    public static void setMemberFeatureSync(){
        Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Automatic' AND SobjectType = 'FieloPRM_MemberFeature__c'].Id;
        for(FieloPRM_MemberFeature__c memberFeature : (List<FieloPRM_MemberFeature__c>)Trigger.new){
            Boolean isAuto = memberFeature.RecordTypeID == rtId;
            if(isAuto){
                memberFeature.F_PRM_IsSynchronic__c = memberFeature.F_PRM_IsParentSync__c;
            }else if(memberFeature.F_PRM_IsParentSync__c){
                memberFeature.F_PRM_IsSynchronic__c = true;
            }
        }
    }
    
    public static void checkMemberFeatureStatus(List<FieloPRM_MemberFeature__c> triggerNew, Map<Id,SObject> triggerOldMap){
        
        CS_PRM_ApexJobSettings__c config = CS_PRM_ApexJobSettings__c.getInstance('FieloPRM_Batch_MemberFeatProcess');
        if(config == null){
            config = new CS_PRM_ApexJobSettings__c(Name = 'FieloPRM_Batch_MemberFeatProcess', F_PRM_BatchSize__c = 50, F_PRM_SyncAmount__c = 10, F_PRM_TryCount__c = 5);
            insert config;
        }

        for(FieloPRM_MemberFeature__c memberFeature : triggerNew){
            FieloPRM_MemberFeature__c oldMF;
            if(triggerOldMap != null){
                oldMF = (FieloPRM_MemberFeature__c)triggerOldMap.get(memberFeature.Id);
            }
            if(Trigger.isDelete){
                memberFeature.F_PRM_Status__c = 'TOPROCESSINACTIVE';
                memberFeature.F_PRM_CustomLogicOK__c = false;
                memberFeature.F_PRM_SerializeOK__c = false;
            }else if(oldMF != null && memberFeature.F_PRM_IsActive__c != oldMF.F_PRM_IsActive__c){
                if(memberFeature.F_PRM_IsActive__c && ( oldMF == null || !oldMF.F_PRM_IsActive__c)){
                    memberFeature.F_PRM_Status__c = 'TOPROCESSACTIVE';
                    memberFeature.F_PRM_CustomLogicOK__c = false;
                    memberFeature.F_PRM_SerializeOK__c = false;
                } else if(!memberFeature.F_PRM_IsActive__c && (oldMF == null || oldMF.F_PRM_IsActive__c)){
                    memberFeature.F_PRM_Status__c = 'TOPROCESSINACTIVE';
                    memberFeature.F_PRM_CustomLogicOK__c = false;
                    memberFeature.F_PRM_SerializeOK__c = false;
                }
            }else if(memberFeature.F_PRM_TryCount__c >= Integer.valueOf(config.F_PRM_TryCount__c)){
                    memberFeature.F_PRM_Status__c = 'FAILED';
            }else if(memberFeature.F_PRM_Status__c != null && memberFeature.F_PRM_Status__c.equals('PROCESSED')){
                memberFeature.F_PRM_CustomLogicOK__c = true;
                memberFeature.F_PRM_SerializeOK__c = true;
            }else if(memberFeature.F_PRM_CustomLogicOK__c && memberFeature.F_PRM_SerializeOK__c){
                memberFeature.F_PRM_Status__c = 'PROCESSED';  
            }else if(Trigger.isBefore &&Trigger.isInsert){
                memberFeature.F_PRM_Status__c = 'TOPROCESSACTIVE';
            }
        }

        isRunning = true;
        try{
            if(System.isBatch()){
                update triggerNew;
            }
        }catch(Exception e){
            /*List<FieloPRM_MemberFeature__c> newList = new List<FieloPRM_MemberFeature__c>();
            for(FieloPRM_MemberFeature__c feat: triggerNew){
                newList.add(feat);
            }
            update newList;*/
        }
        isRunning = false;
    }
}