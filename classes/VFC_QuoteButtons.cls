/*
+-----------------------+------------------------------------------------------------------------------------+
| Author                | Levasseur Jean-Philippe                                                            |
+-----------------------+------------------------------------------------------------------------------------+
| Description           |                                                                                    |
|                       |                                                                                    |
|     - Component       | VFC_QuoteButtons                                                                   |
|                       |                                                                                    |
|     - Object(s)       | zqu__Quote__c                                                                      |
|     - Description     |   - Manage code behind ZQuote custom buttons to avoid 'Invalid Session Id'         |
|                       |   - Managed buttons are:                                                           |
|                       |       > Publish_to_GoDigital_2  => method: Publish2GoDigital()                     |
|                       |       > Ok                      => method: back()                                  |
|                       |       > Generate_Word_2         => method: GenerateDocument()                      |
|                       |       > Generate_PDF_2          => method: GenerateDocument()                      |
|                       |   - GenerateDocument() encapsulates the ZQGeneratePdfController() Zuora methods    |
+-----------------------+------------------------------------------------------------------------------------+
| Delivery Date         | October, 26th 2016                                                                 |
+-----------------------+------------------------------------------------------------------------------------+
| Modifications         |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
| Governor informations |                                                                                    |
+-----------------------+------------------------------------------------------------------------------------+
*/
public class VFC_QuoteButtons {
   public String quoteId;
   public List<zqu__Quote__c> zQuote = new List<zqu__Quote__c>();
   public List<Zuora_External_Approval__c> entities = new List<Zuora_External_Approval__c>();
   public List<Zuora_External_Approval__c> zCS_External_Approval = new List<Zuora_External_Approval__c>();
   
   public VFC_QuoteButtons(ApexPages.StandardController controller) {
      quoteId = ApexPages.currentPage().getParameters().get('id');
      zQuote = [SELECT Id, zqu__Status__c, ApprovalStatus__c, Entity__c FROM zqu__Quote__c WHERE Id = :quoteId];
   }

   public PageReference Publish2GoDigital() {
      PageReference pageRef;
      Boolean found = false;

      if (zQuote.size() > 0) {
         entities = [SELECT Id, Name FROM Zuora_External_Approval__c];
         zCS_External_Approval = [SELECT Id, Name, ApprovalStatus__c FROM Zuora_External_Approval__c WHERE Name = :zQuote[0].Entity__c and ApprovalStatus__c = :zQuote[0].ApprovalStatus__c];
      }
      
      if(!(entities.size() > 0)) {
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLNOV16FIO003));
         pageRef = null;
      }
      else
      {
         for(Zuora_External_Approval__c entity : entities)
         {
            if(zQuote[0].Entity__c == entity.Name) {
               found = true;
            }
         }
      
         if(!found) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLNOV16FIO004));
            pageRef = null;
         }
         else
         {
            if (!(zCS_External_Approval.size() > 0)) {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLNOV16FIO005));
               pageRef = null;
            }
            else
            {
               zQuote[0].zqu__Status__c = 'Created';
               zQuote[0].ApprovalStatus__c = 'Externally Submitted';
               update(zQuote);
               pageRef = new PageReference('/'+quoteId);
            }
         }
      }
      return pageRef;
   }

   public pageReference back(){
      pageReference pageRef = new PageReference('/'+quoteId);
      return pageRef;
   } 

   public PageReference GenerateDocument() {
      String sessionID = UserInfo.getSessionId();
      String format = ApexPages.currentPage().getParameters().get('format');

      //API Partner Server 10.0 URL should be something like: https://cs21.salesforce.com/services/Soap/u/10.0/00Dq00000000SiM
      //                           but we get something like: https://c.cs21.visual.force.com/services/Soap/u/10.0/00Dq00000000SiM
      //but it looks like working !!!
      String PartnerServerURL = System.URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/10.0/' + UserInfo.getOrganizationId();
      
      String EncodedPartnerServerURL = EncodingUtil.urlEncode(PartnerServerURL, 'UTF-8');

      pageReference pageRef = new PageReference('/apex/zqu__zqgeneratepdf?SID='+sessionID+'&SURL='+EncodedPartnerServerURL+'&QID='+quoteId+'&format='+format);
      return pageRef;
   }
}