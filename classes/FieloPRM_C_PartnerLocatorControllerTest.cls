@Istest
private with sharing class FieloPRM_C_PartnerLocatorControllerTest{

    static testMethod void testPLComponentController(){
        
        FieloEE.MockUpFactory.setCustomProperties(false);
        FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
            FieloEE__Member__c = false
        );
        insert deactivate;

        Country__c c = new Country__c(Name = 'France', CountryCode__c = 'FR');
        insert c;

        PRMCountry__c prmCountry = new PRMCountry__c(Country__c=c.Id,PLDataPool__c='en_US2',CountryPortalEnabled__c=true,PartnerLocatorSubmitURL__c='http://www.google.{countryCode}');
        insert prmCountry;
        
        Account acc= new Account(
            Name = 'test',
            Street__c = 'Some Street',
            ZipCode__c = '012345',
            PLDataPool__c ='en_US2'
        );
        insert acc;
                
        FieloEE__Member__c mem = new FieloEE__Member__c(
            FieloEE__FirstName__c = 'test',
            FieloEE__LastName__c= 'test',
            F_Country__c = c.Id
        );
        insert mem;
    
        FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
        insert memb;
        
        FieloEE.MemberUtil.setmemberId(mem.Id);
        
        FieloEE__Menu__c menu = new FieloEE__Menu__c(
            FieloEE__Title__c = 'test'
        );
        insert menu;

        Fielo_CRUDObjectSettings__c testCrud = new Fielo_CRUDObjectSettings__c(
            F_ObjectAPIName__c = 'Account'
        );
        insert testCrud;
    
        FieloEE__Component__c component = new FieloEE__Component__c(
            FieloEE__Menu__c = Menu.Id,
            F_CRUDObjectSettings__c = testCrud.Id
        );
        insert component;
        
        FieloPRM_PartnerLocatorConfiguration__c compParameters = new  FieloPRM_PartnerLocatorConfiguration__c(F_PRM_CountryCluster__c=prmCountry.Id,F_PRM_BottomLabel__c='',F_PRM_ShowBusinessType__c=false, F_PRM_Subtitle__c='subTitle',F_PRM_Title__c='Title',F_PRM_BusinessTypeDefaultValue__c='WD',F_PRM_DistanceUnitValues__c='##DEF##km\r\nml',F_PRM_DistanceValues__c='##DEF##50\r\n100',F_PRM_AreaOfFocusExcludeValues__c='impossibleValue\r\nimpossibleValue2');
        insert compParameters;

        FieloPRM_C_PartnerLocatorFormController myController = new FieloPRM_C_PartnerLocatorFormController();
        myController.currentComponentId = component.Id;
        FieloPRM_C_PartnerLocatorFormController.PartnerLocatorComponentWrapper myWrapper =  myController.currentComponentWithParameter;
        System.assertEquals('Title',myWrapper.title);
        System.assertEquals('subTitle',myWrapper.subTitle);
        System.assertEquals('km',myWrapper.defaultDistanceUnit);
        System.assertEquals(2,myWrapper.areaOfFocusExcludeValues.size());
        System.assertEquals('http://www.google.fr',myWrapper.url);
        
        

        Schema.DescribeFieldResult fieldResult = Account.PLBusinessType__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        //System.assertEquals(ple.size(),myController.getPLBusinessType().size());


        fieldResult = Account.PLAreaOfFocus__c.getDescribe();
        ple = fieldResult.getPicklistValues();
        //System.assertEquals(ple.size(),myController.getPLAreaOfFocus().size());
        //System.assertEquals(2,myController.getDistanceUnitValues().size());
        //System.assertEquals(2,myController.getDistanceValues().size());

        myController.switchSecondLocator();
        myController.getSwitchableLocator();
        FieloPRM_C_PartnerLocatorFormController.PartnerLocatorComponentWrapper error = new FieloPRM_C_PartnerLocatorFormController.PartnerLocatorComponentWrapper(true,'error');
        
     }

}