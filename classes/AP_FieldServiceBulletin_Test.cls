@isTest
private class AP_FieldServiceBulletin_Test {
    
    static testMethod void Method1() {
        
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        runAsUser.BypassWF__c = True;
        runAsUser.BypassVR__c = True;
        insert runAsUser;
        
        System.runAs(runAsUser) {
        
            OPP_Product__c OPPprod = Utils_TestMethods.createProduct('businessUnit','productLine','','');
            insert OPPprod;
            
            FieldServiceBulletin__c FSB = new FieldServiceBulletin__c();
            FSB.Name = 'Test'; 
            FSB.Description__c = 'Test Description';
            FSB.ProductLine__c = OPPprod.Id;
            insert FSB;
                            
            BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                    accOrg1.Name='Test';
                    accOrg1.Entity__c='Test Entity-2'; 
                    accOrg1.Operations__c = True;
                    insert accOrg1;       
            
            EntityStakeholder__c OrgSH2 = Utils_TestMethods.createEntityStakeholder(accOrg1.Id,runAsUser.Id,'CS&Q Manager');
            insert OrgSH2;  
            
            LocalizedFSB__c locFSB = new LocalizedFSB__c();
            locFSB.Organization__c = accOrg1.Id;
            locFSB.FieldServiceBulletin__c = FSB.Id;
            locFSB.Status__c = 'Executing';
            insert locFSB;
            
            FSB.Status__c = '6. Completed';
            update FSB;
        }
    }   
    
    static testMethod void Method2() {
        
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        runAsUser.BypassWF__c = True;
        runAsUser.BypassVR__c = True;
        insert runAsUser;
        
        System.runAs(runAsUser) {
        
            BusinessRiskEscalationEntity__c accOrg1 = new BusinessRiskEscalationEntity__c();
                    accOrg1.Name='Test';
                    accOrg1.Entity__c='Test Entity-2'; 
                    insert  accOrg1;
            BusinessRiskEscalationEntity__c accOrg2 = new BusinessRiskEscalationEntity__c();
                    accOrg2.Name='Test';
                    accOrg2.Entity__c='Test Entity-2'; 
                    accOrg2.SubEntity__c='Test Sub-Entity 2';
                    insert  accOrg2;
            BusinessRiskEscalationEntity__c accOrg = new BusinessRiskEscalationEntity__c();
                    accOrg.Name='Test';
                    accOrg.Entity__c='Test Entity-2';  
                    accOrg.SubEntity__c='Test Sub-Entity 2';
                    accOrg.Location__c='Test Location 2';
                    accOrg.Location_Type__c='Design Center';
                    insert  accOrg;
            
            Problem__c prob = Utils_TestMethods.createProblem(accOrg.id);
            prob.Severity__c = 'Safety Related';
            prob.RecordTypeid = Label.CLI2PAPR120014;
            insert prob;
            
            OPP_Product__c OPPprod = Utils_TestMethods.createProduct('businessUnit','productLine','','');
            insert OPPprod;
            
            ProductLineQualityContactMapping__c plis = new ProductLineQualityContactMapping__c();
            plis.Product__c = OPPprod.Id;
            plis.QualityContact__c = runAsUser.Id;
            plis.AccountableOrganization__c = accOrg.Id;
            insert plis;
            
            FieldServiceBulletin__c FSB = new FieldServiceBulletin__c();
            FSB.Name = 'Test'; 
            FSB.Status__c = '1. New';
            FSB.Description__c = 'Test Description';
            FSB.Problem__c = prob.Id;
            FSB.TypeofDocumenttoGenerate__c = 'FSB';
            FSB.ProductLine__c = OPPprod.Id;
            insert FSB; 
            
            ContainmentAction__c testConAction1 = Utils_TestMethods.createContainmentAction(prob.Id, accOrg.Id);
            testConAction1.FieldServiceBulletin__c = FSB.Id;
            testConAction1.Status__c = 'Executing';
            insert testConAction1;  
            
            FSB.Status__c = '6. Completed';
            update FSB;
        }        
    } 
}