/* 
    Author: Nicolas PALITZYNE (Accenture)
    Created date: 19/03/2012
    Description: Test Class for VFC89_MassCaseTeamTransfer Apex Controller
*/  

@istest
private class VFC89_MassCaseTeamTransfer_TEST
{
    private static testMethod void testVCC12() { 
    
        Account anAccount = Utils_TestMethods.createAccount();
        insert anAccount;
        
        Contact aContact = Utils_TestMethods.createContact(anAccount.Id,'aTestContact');
        insert aContact;
        
        Case testCase = Utils_TestMethods.createCase(anAccount.Id, aContact.Id, 'Open');
        insert testCase;

        List<Case> caseList = new List<Case>();
        caseList.add(testCase);
        ApexPages.StandardSetController standardSetController = new ApexPages.StandardSetController(caseList);
        standardSetController.setSelected(caseList);
        VFC89_MassCaseTeamTransfer massCaseTransferController = new VFC89_MassCaseTeamTransfer(standardSetController );
    }
}