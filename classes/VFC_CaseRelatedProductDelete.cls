global with sharing class VFC_CaseRelatedProductDelete {
    public boolean errorFlag{get;set;}
    public Id caseId;
    public CSE_RelatedProduct__c caseProductRecord;
    public Case cse; 
    public VFC_CaseRelatedProductDelete(ApexPages.StandardController controller){
        caseProductRecord=(CSE_RelatedProduct__c)controller.getRecord();
        errorFlag=false;
        if(caseProductRecord.id!=null){
            caseProductRecord=[select Id, MasterProduct__c,Case__c from CSE_RelatedProduct__c where id=:caseProductRecord.id];
            if(caseProductRecord!=null && caseProductRecord.Case__c !=null)
                caseId = caseProductRecord.Case__c;
        }
    }
    public PageReference pageCancelFunction(){
        PageReference pageResult;
        if(caseId != null){
            pageResult = new PageReference('/'+ caseId);
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
    public PageReference deleteCaseRelatedProduct(){
        PageReference pageResult;
        boolean deleteRecord  = false;
        
        /* MODIFIED FOR BR-6634 - Allow master product to be removed from the related list by using the "Del" hyperlink, if there is one product associated with the case */
        if(caseProductRecord!=null)
        {
            List<CSE_RelatedProduct__c> caseRecords = [select Id, MasterProduct__c,Case__c from CSE_RelatedProduct__c where Case__c=:caseId];
            if(caseRecords!=null)
            {
                if(caseRecords.size()!=1)
                {
                    if(caseProductRecord.MasterProduct__c)
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+'You cannot delete a Master Product'));
                        errorFlag=true;
                        return null;
                    }
                    else
                        deleteRecord = true;
                } 
                else
                {
                    //CLEARS PRODUCT RELATED DETAILS IN CASE 
                    cse = [select Id,ProductBU__c, ProductLine__c, ProductFamily__c, Family__c, SubFamily__c,ProductSuccession__c, ProductGroup__c, ProductDescription__c, CommercialReference__c, TECH_GMRCode__c,Family_lk__c,CommercialReference_lk__c,CaseSymptom__c,CaseSubSymptom__c, 
                        CaseOtherSymptom__c from Case where Id=:caseId];
                        cse.Family_lk__c = null;
                        cse.CommercialReference_lk__c  = null;                        
                }
                if(cse!=null)
                {
                    DataBase.saveResult updateCase = Database.update(cse,false);
                    if(!(updateCase.isSuccess()))
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+updateCase.getErrors()[0]));
                        errorFlag=true;
                        System.debug('DML_EXCEPTION - Error When updating Case ' + updateCase.getErrors()[0]);
                    }
                } 
                if(deleteRecord == true)
                {
                    DataBase.DeleteResult deleteCaseProduct = Database.delete(caseProductRecord,false);
                    if(!(deleteCaseProduct.isSuccess()))
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+deleteCaseProduct.getErrors()[0]));
                        errorFlag=true;
                        System.debug('DML_EXCEPTION - Error When deleting Case Product' + deleteCaseProduct.getErrors()[0]);
                    }
                         
                }               
                /* END OF MODIFICATION BR-6634 */
            }
        }
        
        if(caseId != null){
            pageResult = new PageReference('/'+ caseId);
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
}