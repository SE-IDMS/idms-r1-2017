global class BatchSRVWorkOrderIPAssignment  implements Database.Batchable<sObject>, Schedulable {
    public Integer rescheduleInterval = Integer.ValueOf(Label.CLAPR14SRV13);
    
    public String query = 'select id, name, Shipped_Serial_Number__c, SVMXC__Product__c, SVMXC__Component__c from  SVMXC__Service_Order__c where ((Shipped_Serial_Number__c != null AND SVMXC__Product__c != null AND SVMXC__Component__c = null) OR (Attached_Work_Details__c != 0 AND SVMXC__Component__c != null)) limit '+Label.CLAPR14SRV14;
    
    public List<String> errorStatusesList = new List<String> {'BLOCKED', 'ERROR', 'PAUSED', 'PAUSED_BLOCKED', 'DELETED'};
        
        private static Boolean isDebugEnabled = true;
    static {
        try {
            isDebugEnabled = Boolean.valueOf(Label.CLAPR14SRV20);
        }
        catch(Exception e) {
            System.debug('#### Error getting debug enabled status from Custom Label CLAPR14SRV20: '+e.getMessage());
        }        
    }
    
    /*private static void myDebug(String aMsg) {
        if (isDebugEnabled) {
            System.debug('#### '+aMsg);
        }
    }*/
    global Database.Querylocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(query);    
    }
    
    global void execute(Database.BatchableContext BC, List <sObject> scope)
    {
        
        if(scope!=null){
            AP_IPAssociation_Handler.attachIPForWorkOrders((List<SVMXC__Service_Order__c>) scope);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
                /*Boolean canReschedule = True;
        List<CronJobDetail> scheduledJobDetailList = [SELECT Id, Name, JobType FROM CronJobDetail where Name = :scheduledJobName limit 100];
        myDebug('scheduledJobDetailList: '+scheduledJobDetailList);
        
        if (scheduledJobDetailList != null && scheduledJobDetailList.size()>0) {
        // There is at least one scheduled job already for this name
        // Looking if one of scheduled job with the correct name is in an 'Active' state
        List<CronTrigger> scheduledJobList = [SELECT Id, State, TimesTriggered, NextFireTime FROM CronTrigger WHERE CronJobDetailId = :scheduledJobDetailList[0].Id and State not in :errorStatusesList limit 1];
        myDebug('scheduledJobList: '+scheduledJobList);
        if (scheduledJobList != null && scheduledJobList.size()>0) {
        canReschedule = False;
        }
        }
        if (canReschedule && !Test.isRunningTest() ) {
        System.scheduleBatch(new BatchSRVWorkOrderIPAssignment(), scheduledJobName, rescheduleInterval);
        }*/
    }
    
    global void execute(SchedulableContext sc) {
        
        BatchSRVWorkOrderIPAssignment attachIPForWorkOrders = new BatchSRVWorkOrderIPAssignment();
        Database.executeBatch(attachIPForWorkOrders);
    }   
    
    
}