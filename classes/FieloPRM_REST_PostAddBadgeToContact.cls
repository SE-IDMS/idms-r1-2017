/********************************************************************************************************
    Author: Fielo Team
    Date: 27/03/2015
    Description: 
    Related Components:
*********************************************************************************************************/
@RestResource(urlMapping='/RestPostAddBadgeToContact/*') 
global class FieloPRM_REST_PostAddBadgeToContact{  

    /**
    * [postAddBadgeToContact]
    * @method   postAddBadgeToContact
    * @Pre-conditions  
    * @Post-conditions 
    * @param    []    []
    * @return   []    []
    */
    @HttpPost
    global static String postAddBadgeToContact(String type, Map<String ,List<String>> conToBadges){    
        
        return FieloPRM_UTILS_BadgeMember.addBadgeToContact(type,conToBadges);        
    }  
   
}