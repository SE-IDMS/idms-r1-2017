/*
06-Dec-2012    Srinivas Nallapati    Dec12 A&C release    DEF-1131
*/
public class VFC_AccountRecordTypeSelect {
    public Schema.DescribeSObjectResult AccResult = Account.SObjectType.getDescribe();
    
    public PageReference redirect() {
        PageReference pg ;
        
        if(AccResult.isCreateable())
            pg = new PageReference(System.Label.CLDEC12AC11);
        else
            pg = Page.vfp02_accountsearch;
        return pg;
    }

}