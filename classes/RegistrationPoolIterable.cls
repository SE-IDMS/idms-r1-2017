/**
* @author Anil Sistla
* @date 05/11/2016
* @description Iterator class encapuslating the functionality for registration pool
*               used as custom iterator in batch. Implements Iterable interface to
*               be used in the batch start to return locator
*/
public class RegistrationPoolIterable 
        implements Iterable<AP_PRMRegistrationPoolingService.RegistrationPoolInfo> {

    private AP_PRMAppConstants.ProcessRegistrationPool typeToProcess;
    
    public RegistrationPoolIterable () {
    }

    public RegistrationPoolIterable(AP_PRMAppConstants.ProcessRegistrationPool typeToProcess) {
        this.typeToProcess = typeToProcess;
    }
    
    /**
    * @author Anil Sistla
    * @date 05/11/2016
    * @description Iterable interface iterator method returns an instance of RegistrationPoolIterator
    * @return Iterator
    */
    public Iterator<AP_PRMRegistrationPoolingService.RegistrationPoolInfo> Iterator(){
        if(this.typeToProcess != null)
            return new RegistrationPoolIterator(typeToProcess);
        else
            return new RegistrationPoolIterator();
    }
}