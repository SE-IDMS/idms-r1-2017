/* 
    Author: Nicolas PALITZYNE (Accenture)
    Created date: 26/02/2012
    Description: Apex class containing method to check if a case classification is not already covered
*/   

public class AP46_CheckTeamClassification
{
    public Map<ID,CaseClassification__c> relatedCaseClassMap; // Map of case classifications with their IDs
    public Map<Id, List<TeamCaseJunction__c>> newCCTeamMap; // Map of CC Team Map ID with their Team junction for new junctions
    public Map<Id, List<TeamCaseJunction__c>> existingCCTeamMap; // Map of CC Team Map ID with their Team junction for old junctions
    public List<TeamCaseJunction__c> TeamCaseJunctionList; // List of related supported case classification records
    
    // Class constructor, build the map of Case classifications with their IDs
    public AP46_CheckTeamClassification(List<TeamCaseJunction__c> aTeamCaseJunctionList) 
    {
        try {
            System.Debug('AP46.checkCoverage.constructor - Constructor is called.');
            
            TeamCaseJunctionList = aTeamCaseJunctionList;
            relatedCaseClassMap = new Map<ID,CaseClassification__c>();
            
            for(CaseClassification__c caseClassItem:[SELECT Id, Name, Category__c, Reason__c, SubReason__c FROM CaseClassification__c LIMIT 100]) {
            relatedCaseClassMap.put(caseClassItem.Id,caseClassItem);
            }

                 
            // Build the map of new team-case classification junctions for new items
            newCCTeamMap = new Map<Id, List<TeamCaseJunction__c>> ();        
            
            for(TeamCaseJunction__c teamJunctionItem:aTeamCaseJunctionList) {
            
                // Check if the ID is already recorded in the map, add it if necessary
                if(!newCCTeamMap.keyset().contains(teamJunctionItem.CCTeam__c)) {
                newCCTeamMap.put(teamJunctionItem.CCTeam__c,new List<TeamCaseJunction__c>());
                }
                
                // Add a new junction object in the list mapped with the team
                newCCTeamMap.get(teamJunctionItem.CCTeam__c).add(teamJunctionItem);
            }
        
            System.Debug('AP46.checkCoverage.INFO - Map ID/List of new Supported Classification='+newCCTeamMap );
            
            // Retrieve the list of existing team-case classification junctions for the CC Team in the set of IDs
            existingCCTeamMap = new Map<Id, List<TeamCaseJunction__c>> ();
            
            for(CustomerCareTeam__c CCTeamItem:[SELECT ID, Name, (SELECT ID, Name, CaseClassification__r.Category__c, CaseClassification__r.Reason__c,
                                                CaseClassification__r.SubReason__c, CaseClassification__c,  CCTeam__c
                                                FROM TeamCaseJunction__r) FROM CustomerCareTeam__c WHERE ID IN :newCCTeamMap.keyset()]) {
            
            existingCCTeamMap.put(CCTeamItem.ID,CCTeamItem.TeamCaseJunction__r);
            
            }
        }
        catch(Exception e) {
            System.Debug('AP46.checkExistingCoverage.'+e.getTypeName()+'- '+ e.getMessage());
            System.Debug('AP46.checkExistingCoverage.'+e.getTypeName()+'- Line = '+ e.getLineNumber());
            System.Debug('AP46.checkExistingCoverage.'+e.getTypeName()+'- Stack trace = '+ e.getStackTraceString());
            
            debugLog__c debugLogRecord = new debugLog__c(ExceptionType__c=e.getTypeName(),Description__c=e.getMessage(),StackTrace__c=e.getStackTraceString());
            DataBase.SaveResult newCase_SR = database.insert(debugLogRecord ,false);
        }
          
        System.Debug('AP46.checkCoverage.INFO - Map ID/List of existing Supported Classification  ='+existingCCTeamMap);
        System.Debug('AP46.checkCoverage.constructor - End of constructor.');     
    }

    // Add error on inserted object when the related classification is already covered
    public void checkCoverage()
    {
        /*
            For every junction object Case Classification / Team inserted or updated
                verify that there is no record in database covering the classification
                verify that there is no record in the list of new records covering the classification
                
            A case classification is considered as covered if
                The related category is covered
                The related reason is covered
                The related sub reason is covered
        */

        System.Debug('AP46.checkCoverage.INFO - Method is called.');

        try {    
            
            System.Debug('AP46.checkCoverage.INFO - Map ID/List of new Supported Classification='+newCCTeamMap );
            System.Debug('AP46.checkCoverage.INFO - Map ID/List of existing Supported Classification  ='+existingCCTeamMap);
            
            if(existingCCTeamMap != null && newCCTeamMap != null) {                                         
                for(TeamCaseJunction__c teamCaseItem:TeamCaseJunctionList ) {
                    
                    System.Debug('AP46.checkCoverage.INFO - Item checked = ' + teamCaseItem);
                    
                    // Check in Case classifications covered existing in the database
                    for(TeamCaseJunction__c existingTeamCaseItem:existingCCTeamMap.get(teamCaseItem.CCTeam__c)) {               
                        checkClassificationCoverage(existingTeamCaseItem,teamCaseItem); 
                    }

                    // Check in existing Case classifications just added
                    for(TeamCaseJunction__c newTeamCaseItem:newCCTeamMap.get(teamCaseItem.CCTeam__c)) {
                        checkClassificationCoverage(newTeamCaseItem,teamCaseItem); 
                    }
                }
            }
            
            System.Debug('AP46.checkCoverage.INFO - End of Method.');
        }
        catch(Exception e) {
            System.Debug('AP46.checkExistingCoverage.'+e.getTypeName()+'- '+ e.getMessage());
            System.Debug('AP46.checkExistingCoverage.'+e.getTypeName()+'- Line = '+ e.getLineNumber());
            System.Debug('AP46.checkExistingCoverage.'+e.getTypeName()+'- Stack trace = '+ e.getStackTraceString());
            
            debugLog__c debugLogRecord = new debugLog__c(ExceptionType__c=e.getTypeName(),Description__c=e.getMessage(),StackTrace__c=e.getStackTraceString());
            DataBase.SaveResult newCase_SR = database.insert(debugLogRecord,false);
        }
    }
    
    // Checks if the classification of argTeamCase is already covered by anExistingCase
    public Boolean checkClassificationCoverage(TeamCaseJunction__c anExistingTeamCase, TeamCaseJunction__c argTeamCase)
    {
        System.Debug('AP46.checkClassificationCoverage.INFO - Method is called.');
        System.Debug('AP46.checkClassificationCoverage.INFO - Existing Junction ='+ anExistingTeamCase);
        System.Debug('AP46.checkClassificationCoverage.INFO - New Junction ='+ argTeamCase);
    
        Boolean isCovered=false;
        
        // Existing junction classification values
        String eCategory = anExistingTeamCase.CaseClassification__r.Category__c;
        String eReason = anExistingTeamCase.CaseClassification__r.Reason__c;
        String eSubReason = anExistingTeamCase.CaseClassification__r.SubReason__c;
        
        // New junction classification values
        String nCategory = relatedCaseClassMap.get(argTeamCase.CaseClassification__c).Category__c;
        String nReason = relatedCaseClassMap.get(argTeamCase.CaseClassification__c).Reason__c;
        String nSubReason = relatedCaseClassMap.get(argTeamCase.CaseClassification__c).SubReason__c;
        
        System.Debug('AP46.checkClassificationCoverage.INFO - Existing Category  = '+ eCategory );
        System.Debug('AP46.checkClassificationCoverage.INFO - New Category = '+ nCategory );
        System.Debug('AP46.checkClassificationCoverage.INFO - Existing Reason = '+ eReason );
        System.Debug('AP46.checkClassificationCoverage.INFO - New Reason = '+ nReason);
        System.Debug('AP46.checkClassificationCoverage.INFO - Existing Sub Reason = '+ eSubReason );
        System.Debug('AP46.checkClassificationCoverage.INFO - New Sub Reason = '+ nSubReason );        
 
        if(anExistingTeamCase != null && argTeamCase != null) {
        
            // Check if whole category is covered in the existing
            if(eCategory != null && nCategory != null) {
                
                if(eReason == null && eCategory == nCategory) {
                    argTeamCase.addError(Label.CL00776+nCategory+Label.CL00777+anExistingTeamCase.ID+Label.CL00778+anExistingTeamCase.Name+Label.CL00779);
                    isCovered = true;
                }
            }
            
            System.Debug('AP46.checkClassificationCoverage.INFO - Is Whole category covered? '+ isCovered );
            
            // Check if the Reason is covered
            if(!isCovered && eReason != null && nReason != null) {
                
                if(eSubReason == null && eReason == nReason) {
                    argTeamCase.addError(Label.CL00780+nReason +Label.CL00777+anExistingTeamCase.ID+Label.CL00778+anExistingTeamCase.Name+Label.CL00779);
                    isCovered = true;
                }
            }
            
            System.Debug('AP46.checkClassificationCoverage.INFO - Is Whole reason covered? '+ isCovered );
                        
            // Check Sub Reason
            if(!isCovered && eSubReason != null && nSubReason != null) {
            
                if(eSubReason == nSubReason) {
                    argTeamCase.addError(Label.CL00781+nSubReason +Label.CL00777+anExistingTeamCase.ID+Label.CL00778+anExistingTeamCase.Name+Label.CL00779);
                    isCovered = true;
                }               
            }   
            
            System.Debug('AP46.checkClassificationCoverage.INFO - Is sub reason covered? '+ isCovered );            
        }
        
        System.Debug('AP46.checkClassificationCoverage.INFO - Is new team covered ='+isCovered);
        System.Debug('AP46.checkClassificationCoverage.INFO - End of method.');
        
        return isCovered;
    }
}