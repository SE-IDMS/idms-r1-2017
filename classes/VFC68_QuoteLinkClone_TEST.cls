/*
    Author          : ACN 
    Date Created    : 3/11/2011
    Description     : Test class for VFC68_QuoteLinkClone class 
*/
@isTest
private class VFC68_QuoteLinkClone_TEST 
{
    static testMethod void testQuoteLinkClone() 
    {
        User salesManager1,salesManager2,systemAdmin,delegatedSysAdmin;
        OPP_QuoteLink__c quoteLink,quoteLink1,quoteLink2,quoteLink3;

        List<Sobject> sobjs= new List<Sobject>();
        List<User> users = new List<User>();
        List<User> lstUsers = new List<User>();
        List<User> lstUser_1=new List<User>();
        // List<Profile> profiles = [select id,Name from profile where name='SE - Sales Manager' OR name like '%System Administrator%'];        
        List<Profile> profiles = [select id,Name from profile where name like '%System Administrator%'];                
        for(Profile p: profiles)
        {
            // if(p.name == 'SE - Sales Manager')
            // {
            //     salesManager1 = Utils_TestMethods.createStandardUser(p.Id,'tU1VFCXX');
            //     salesManager2 = Utils_TestMethods.createStandardUser(p.Id,'tU4VFCYY');
            //     lstUsers.add(salesManager1);
            //     lstUsers.add(salesManager2);
            // }
            // else
             if(p.name.contains('SE - Delegated System Administrator')){
                delegatedSysAdmin= Utils_TestMethods.createStandardUser(p.Id,'tU2VFCZZ');
                //lstUsers.add(delegatedSysAdmin);
            }
            else if(p.name=='System Administrator')
                systemAdmin= Utils_TestMethods.createStandardUserByPassVR(p.Id,'tU3VFCWW');
            
                
         }

          User sysadmin2=Utils_TestMethods.createStandardUser('sys2');
        sysadmin2.isActive=true;        
        System.runAs(sysadmin2){
         Map<String,User> aliasnametousermap=new Map<String,User>();   
         Map<String,UserandPermissionSets> aliasnametouserpermissionsetrecordmap=new Map<String,UserandPermissionSets>();   
         for(integer i=0;i<2;i++)
         {
            UserandPermissionSets newuserandpermissionsetrecord=new UserandPermissionSets('Users'+i,'SE - Sales Manager');
            user newuser = newuserandpermissionsetrecord.userrecord;           
            aliasnametouserpermissionsetrecordmap.put('Users'+i,newuserandpermissionsetrecord);
            aliasnametousermap.put('Users'+i,newuser);
        }
        Database.insert(aliasnametousermap.values());     
        lstUser_1.addAll(aliasnametousermap.values());
        List<PermissionSetAssignment> permissionsetassignmentlstforusers=new List<PermissionSetAssignment>();    
        for(String useralias:aliasnametouserpermissionsetrecordmap.keySet())
        {            
            for(Id permissionsetId:aliasnametouserpermissionsetrecordmap.get(useralias).permissionsetandprofilegrouprecord.permissionsetIds)
                permissionsetassignmentlstforusers.add(new PermissionSetAssignment(AssigneeId=aliasnametousermap.get(useralias).id,PermissionSetId=permissionsetId));    
        } 
        if(permissionsetassignmentlstforusers.size()>0)
            Database.insert(permissionsetassignmentlstforusers); 
        }
        salesManager1=lstUser_1[0];
        salesManager2=lstUser_1[1];        
        lstUsers.add(salesManager1);
        lstUsers.add(salesManager2);


         //inserts User
        // if(salesManager2!=null)           
          //   insert salesManager2;
         // insert lstUsers;
             
         //inserts Account
         Account acc = RIT_Utils_TestMethods.createAccount();
         acc.ZipCode__c = '100001';
         insert acc;
         
         //inserts Opportunity
         Opportunity opp = RIT_Utils_TestMethods.createOpportunity(acc.Id);
         insert opp;
 
         list<AccountShare> lstAccountShare = new list<AccountShare>();
         for(integer i=0;i<lstUSers.size();i++)
         {
            AccountShare aShare = new AccountShare();
            aShare.accountId= acc.Id;
            aShare.UserOrGroupId = lstUSers[i].Id;
            aShare.accountAccessLevel = 'edit';
            aShare.OpportunityAccessLevel = 'read';
            aShare.CaseAccessLevel = 'edit';
            //insert aShare;  
            lstAccountShare.add(aShare);
         }                  
         insert lstAccountShare;  
 
         system.runas(salesManager1)
         {      
             //inserts QuoteLink
             quoteLink = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
             quoteLink.IssuedQuoteDateToCustomer__c = Date.Today();
             quoteLink.Margin__c = 10;
             sobjs.add((Sobject)quoteLink);
                        
             //inserts QuoteLink
             quoteLink1 = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
             sobjs.add((Sobject)quoteLink1);
             
             //inserts QuoteLink
             quoteLink2 = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
             sobjs.add((Sobject)quoteLink2);
             
             insert sobjs;                     
             
             sobjs.clear();
             
             //inserts Project Information
             RIT_ProjectInformation__c projInfo1= RIT_Utils_TestMethods.createProjInfowithConsort(quoteLink.Id);
             sobjs.add((Sobject)projInfo1);
                
             //inserts Project Information
             RIT_ProjectInformation__c projInfo= RIT_Utils_TestMethods.createProjInfo(quoteLink.Id);
             sobjs.add((Sobject)projInfo);
                
             //inserts Financial Environment record for quoteLink 
             RIT_FinancialEnv__c finEnv = RIT_Utils_TestMethods.createFinEnv(quoteLink.Id);
             sobjs.add((Sobject)finEnv);
                
             //inserts Contractual Environment
             RIT_ContractualEnv__c contEnv = RIT_Utils_TestMethods.createConEnv(quoteLink.Id);
             sobjs.add((Sobject)contEnv);
        
             //inserts Budget
             RIT_Budget__c budget= RIT_Utils_TestMethods.createBudget(quoteLink.Id);
             sobjs.add((Sobject)budget);
        
             //inserts Project Management
             RIT_ProjectManagement__c projMgnt = RIT_Utils_TestMethods.createProjMgnt(quoteLink.Id);
             sobjs.add((Sobject)projMgnt);
                
             //inserts Techical & Manufacturing
             RIT_TechManScope__c techManf= RIT_Utils_TestMethods.createTechManf(quoteLink.Id);                
             sobjs.add((Sobject)techManf);
              
             //inserts Commercial & Competitive Environment
             RIT_CommCompEnv__c commComp= RIT_Utils_TestMethods.createCommComp(quoteLink.Id);
             sobjs.add((Sobject)commComp);
             
             //inserts Sub Contracters & OG Suppliers
             RIT_SubContractOGSupp__c subContract= RIT_Utils_TestMethods.createSubContr(quoteLink.Id);
             sobjs.add((Sobject)subContract);
             insert sobjs;
             
             sobjs.clear();             
             //inserts Terms and Means of Payment for Financial Environment
             RIT_TermsAndMeansOfPayment__c termsAndMeans = RIT_Utils_TestMethods.createTermsAndMeans(finEnv.Id);
             sobjs.add((Sobject)termsAndMeans);
                
             //inserts Risk for Project Management
             RIT_Risks__c risk =  RIT_Utils_TestMethods.createProjMgntRisks(quoteLink.Id,projMgnt.Id);
             sobjs.add((Sobject)risk);

             //insert Note
             Note note= RIT_Utils_TestMethods.createNotes(finEnv.Id);
             sobjs.add((Sobject)note);
             insert sobjs;
             
             OPP_QuoteLInk__Share quteShare = new OPP_QuoteLInk__Share();
             quteShare.AccessLevel = 'Read';
             quteShare.UserOrGroupId= salesManager2.Id;
             quteShare.parentId = quoteLink2.Id;
             insert quteShare;
          }
          system.runas(systemAdmin)
          {               
             //inserts QuoteLink
             quoteLink3 = RIT_Utils_TestMethods.createQuoteLink(opp.Id);
             insert quoteLink3;        
             quoteLink3 = [select id,CurrencyISOCode,QuoteStatus__c from OPP_QuoteLink__c where Id=:quoteLink3.Id];
             
             //inserts Project Information
             RIT_ProjectInformation__c projInfo= RIT_Utils_TestMethods.createProjInfo(quoteLink1.Id);
             insert projInfo;             
          }
          test.startTest();
          system.runas(salesManager1)
          {    
             Test.setCurrentPageReference(new PageReference('Page.VFP68_QuoteLinkClone'));
            System.currentPageReference().getParameters().put('mode', 'revise');
             ApexPages.StandardController sc = new ApexPages.StandardController(quoteLink);
             VFC68_QuoteLinkClone cloneQuteLink = new VFC68_QuoteLinkClone(sc);
             cloneQuteLink.cloneQuoteLink();
             cloneQuteLink.continueClone();   
          }
          system.runas(salesManager2)
          {
             Test.setCurrentPageReference(new PageReference('Page.VFP68_QuoteLinkClone'));
            System.currentPageReference().getParameters().put('mode', 'blah');
             ApexPages.StandardController sc = new ApexPages.StandardController(quoteLink2);
             VFC68_QuoteLinkClone cloneQuteLink = new VFC68_QuoteLinkClone(sc);
             cloneQuteLink.cloneQuoteLink();     
             cloneQuteLink.continueClone();                      
          }
          
          //Following conditions are commented because the too many statements are performed and the governor limits are reached.
          //No additional class have been created because the class code coverage is not impacted by this change.
          //Please let me know for any remark.
          //Jules Faligot (jules.faligot@accenture.com)
          
          system.runas(delegatedSysAdmin)
          {               
             ApexPages.StandardController sc = new ApexPages.StandardController(quoteLink);
             VFC68_QuoteLinkClone cloneQuteLink = new VFC68_QuoteLinkClone(sc);
             cloneQuteLink.cloneQuoteLink();
             cloneQuteLink.continueClone();             
          }
          system.runas(systemAdmin)
          {               
             ApexPages.StandardController sc = new ApexPages.StandardController(quoteLink3);
             VFC68_QuoteLinkClone cloneQuteLink = new VFC68_QuoteLinkClone(sc);
             cloneQuteLink.cloneQuoteLink();
             cloneQuteLink.continueClone();             
          }        
          test.stopTest();
    }
}