public class psswdregex_prashanth_Cont {
    public string strpwd {get; set;}
    public string strFN {get; set;}
    public string strLN {get; set;}
    public boolean policyFlag {get; set;}
    
    public psswdregex_prashanth_Cont(){
        policyFlag=true;
        strFN='John1';
        strLN='Doe2';
        strpwd='Ambitdoe@829';
        
        /*String pwdPattern = '^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d$!@#$&-_=+{}()~*^\\s]{8,20}$';
        
        Boolean fn1=false;
        Boolean ln1=false;
        
        fn1=Pattern.matches('^(?!'+strFN+'$).*', strpwd);
        policyFlag=fn1;*/
        
        //if(!strpwd.containsIgnoreCase(strFN) && !strpwd.containsIgnoreCase(strLN)) policyFlag=true;
        //if(strpwd.length()>8) policyFlag=true;
        //if(Pattern.matches('[0-9]{2}-[0-9]{2}', strpwd)) policyFlag=true; //Label.tstregex_pk1 (\\d)\\1+ 
        
        
        
    }
    public void checkpsswdpolicy() {
        policyFlag=true;
        //Pattern matching
        //For Repeated and Sequential
        Pattern repeatedPattern = Pattern.compile('.*(\\d)\\1{1}.*'); // repeated 
        Pattern sequentialPattern = pattern.compile('.*(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)|9(?=0)){1}\\d.*'); 
        
        matcher repeatedMatcher = repeatedPattern.matcher(strpwd);
        matcher sequentialMatcher = sequentialPattern.matcher(strpwd);
        
        //For 3 out of 4
        pattern myPattern = pattern.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$'); //ULN
        pattern myPattern2 = pattern.compile('^(?=.*[A-Z])(?=.*\\d)(?=.*(_|[^\\w])).+$'); //UNS
        pattern myPattern3 = pattern.compile('^(?=.*[a-z])(?=.*\\d)(?=.*(_|[^\\w])).+$'); //LNS
        pattern myPattern4 = pattern.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*(_|[^\\w])).+$');  //ULS
        
        matcher myMatcher1 = myPattern.matcher(strpwd);
        matcher mymatcher2 = myPattern2.matcher(strpwd);
        matcher myMatcher3 = myPattern3.matcher(strpwd);
        matcher myMatcher4 = myPattern4.matcher(strpwd);
                
        system.debug('match1--> ' + myMatcher1.matches() ) ; 
        system.debug('match2--> ' + myMatcher2.matches() ) ; 
        system.debug('match3--> ' + myMatcher3.matches() ) ; 
        system.debug('match4--> ' + myMatcher4.matches() ) ;
        
        //New implementation
        if(strpwd.containsIgnoreCase(strFN) || strpwd.containsIgnoreCase(strLN)){ 
            policyFlag=false;
            system.debug('~1');
        }
        else if(strpwd.length()<8){
            policyFlag=false;
            system.debug('~2');
        }
        else if(( String.valueOf(repeatedMatcher.matches()) == 'true' ) || ( String.valueOf(sequentialMatcher.matches()) == 'true')){
           policyFlag=false;
           system.debug('~3');
        }
        else if(!(( String.valueOf(myMatcher1.matches()) == 'true' ) || ( String.valueOf(myMatcher2.matches()) == 'true' ) || ( String.valueOf(myMatcher3.matches()) == 'true' ) || ( String.valueOf(myMatcher4.matches()) == 'true' ))){
          policyFlag=false;
          system.debug('~4');
        }
        }
    
}