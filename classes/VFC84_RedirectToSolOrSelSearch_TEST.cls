@isTest
private class VFC84_RedirectToSolOrSelSearch_TEST 
{
    static testMethod void VFC84_RedirectToSolOrSelSearch() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
            
        Opportunity opp = Utils_TestMethods.createOpenOpportunity(account1.id);
        insert opp;         
        
        OPP_SellingCenter__c sc=new OPP_SellingCenter__c(Name='sctest',Email__c='abc@xyz.com');
        insert sc;
        
        OPP_SupportRequest__c newSuppReq = new OPP_SupportRequest__c(
                                                                    Opportunity__c = opp.id,
                                                                    Comments__c='comments',
                                                                    SupportRequestedBy__c=system.today(),
                                                                    SupportType__c='Solutions and Quotations Support Requests',
                                                                    RecordTypeId=Label.CL10092,
                                                                    SellingCenter__c=sc.id
                                                                    )   ;
        PageReference pageRef = Page.VFP85_NewSupportRequest;
        Test.setCurrentPage(pageRef);                                                            
        ApexPages.currentPage().getParameters().put('oppid', opp.id);
        ApexPages.currentPage().getParameters().put('scid', sc.id);
        ApexPages.currentPage().getParameters().put('RecordType', Label.CL10092);
 
        ApexPages.StandardController con = new ApexPages.StandardController(newSuppReq);
        VFC84_RedirectToSolOrSelSearch ext = new VFC84_RedirectToSolOrSelSearch(con);
        
        ext.doRedirect();
        //delete newSuppReq;
        
        OPP_SupportRequest__c newSuppReq1 = new OPP_SupportRequest__c(
                                                                    Opportunity__c = opp.id,
                                                                    Comments__c='comments',
                                                                    SupportRequestedBy__c=system.today(),
                                                                    SupportType__c='Solutions and Quotations Support Requests',
                                                                    RecordTypeId=Label.CL10093,
                                                                    SellingCenter__c=sc.id
                                                                    )   ;
       
        
        con = new ApexPages.StandardController(newSuppReq1);
        ext = new VFC84_RedirectToSolOrSelSearch(con);        
        ext.doRedirect();
        
        PageReference pageRef2 = Page.VFP85_NewSupportRequest;
        Test.setCurrentPage(pageRef2);                                                            
        /*ApexPages.currentPage().getParameters().put('oppid', opp.id);
        ApexPages.currentPage().getParameters().put('scid', sc.id);
        ApexPages.currentPage().getParameters().put('RecordType', Label.CL10093);
        ApexPages.StandardController con1 = new ApexPages.StandardController(newSuppReq1);
        VFC84_RedirectToSolOrSelSearch ext1 = new VFC84_RedirectToSolOrSelSearch(con1);
        //ext.doRedirect();
        
        
         PageReference pageRef3= Page.VFP85_NewSupportRequest;
        Test.setCurrentPage(pageRef3);    
        */      
        /*ApexPages.currentPage().getParameters().put('oppid', opp.id);
        ApexPages.currentPage().getParameters().put('scid', sc.id);
        ApexPages.currentPage().getParameters().put('RecordType', Label.CL10093);
        ApexPages.StandardController con2 = new ApexPages.StandardController(newSuppReq1);
        */
        
        
        

        
    }
}