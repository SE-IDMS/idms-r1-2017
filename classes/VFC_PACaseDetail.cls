public class VFC_PACaseDetail {

    //public String strCaseId {get;set;}
    public string comment{get;set;}
    public VFC_PACaseDetail(){
      Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=edge');
        //strCaseId=ApexPages.currentPage().getParameters().get('id');
    }
    
    @RemoteAction  
    public static CaseWrapperClass getCaseDetails(String caseId){
        CaseWrapperClass caseWrapper = new CaseWrapperClass();
        List<Case> lstCases=[SELECT CaseNumber,CreatedDate,Account.name,Account.Phone,Account.Parent.Name,Account.Fax,
                    Account.Website,Account.Street__c,Account.StreetLocalLang__c,Account.City__c,Account.LocalCity__c,
                    Account.ZipCode__c,Account.County__c,Account.LocalCounty__c,Account.AdditionalAddress__c,Account.LocalAdditionalAddress__c,
                    Account.StateProvince__r.StateProvinceCode__c, Account.TECH_SDH_CountryCode__c,Account.BillingCity, Account.BillingState, Account.BillingCountry,
                    Subject,Priority,Contact.name,Contact.LocalName__c,Contact.CorrespLang__c,Contact.CommPref__c,Contact.WorkPhone__c,
                    Contact.Email,Contact.WorkPhoneExt__c,Contact.AEmail__c,Contact.Aphone__c,Contact.Fax,Contact.WorkFaxExt__c,Description,AnswerToCustomer__c,
                    CustomerRequestedDate__c,Site__r.name,PACaseType__c, Alarm_date_and_time__c,SubFamily__c,CustomerRequest__c,
                    CommercialReference_lk__r.Name,Family_lk__r.Name,Owner.name, Severity__c,
                    CustomerMajorIssue__c,PAGCS_Web_Status__c,PAPlatform__c,PACustomer_internal_case_number__c FROM Case where Id= :caseId];
        if(lstCases.size() > 0) {
            caseWrapper.caseObj = lstCases[0];
            if(lstCases[0].Alarm_date_and_time__c != null) {
                caseWrapper.strDateOccured = PA_UtilityClass.userLocaleDateTimeConversion(lstCases[0].Alarm_date_and_time__c);
            }
            caseWrapper.strDateOpened = PA_UtilityClass.userLocaleDateTimeConversion(lstCases[0].CreatedDate);
        }
        return caseWrapper;
    } 
    
    /*@RemoteAction  
    public static DateTimeWrapperClass getStringDateTime(String caseId){
        DateTimeWrapperClass strDateTime = new DateTimeWrapperClass();
        List<Case> lstCases=[SELECT Alarm_date_and_time__c, CreatedDate FROM Case where Id= :caseId];
        if(lstCases.size() > 0) {
            if(lstCases[0].Alarm_date_and_time__c != null) {
                strDateTime.strDateOccured = PA_UtilityClass.userLocaleDateTimeConversion(lstCases[0].Alarm_date_and_time__c);
            }
            strDateTime.strDateOpened = PA_UtilityClass.userLocaleDateTimeConversion(lstCases[0].CreatedDate);
        }
        return strDateTime;
    }*/ 
    
    @RemoteAction  
    public static List<CaseComment> getCaseComments(String caseId){
        List<CaseComment> lstCaseComments;
        lstCaseComments=[SELECT CommentBody,CreatedBy.Name,CreatedDate FROM CaseComment where ParentId= :caseId ORDER BY CreatedDate DESC];
        return lstCaseComments;
    } 
    
    @RemoteAction  
    public static List<Attachment> getCaseAttachments(String caseId){
        List<Attachment> lstCaseAttachments;
        lstCaseAttachments=[SELECT BodyLength,CreatedBy.Name,Id,LastModifiedDate,Name,ParentId FROM Attachment where ParentId= :caseId ORDER BY CreatedDate DESC];
        return lstCaseAttachments;
    } 
    @RemoteAction
    public static List<CaseComment> saveCaseComment(String comment, String strCaseId) {
        system.debug('comment==='+comment);
        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerUserEmail = true;
        CaseComment caseComment=new CaseComment();
        caseComment.CommentBody=comment;
        caseComment.ParentId=strCaseId;
        caseComment.IsPublished=true;
        database.insert(caseComment, dlo);
        List<CaseComment> lstCaseComments=[SELECT CommentBody,CreatedBy.Name,CreatedDate FROM CaseComment where ParentId= :strCaseId ORDER BY CreatedDate DESC];
        return lstCaseComments;
    }
    @RemoteAction
    public static List<CaseComment> submitForCaseClosure(String strCaseId) {
        CaseComment caseComment=new CaseComment();
        caseComment.CommentBody='Requesting to close the case : Please close the case';
        caseComment.ParentId=strCaseId;
        caseComment.IsPublished=true;
        insert caseComment;
        List<CaseComment> lstCaseComments=[SELECT CommentBody,CreatedBy.Name,CreatedDate FROM CaseComment where ParentId= :strCaseId ORDER BY CreatedDate DESC];
        return lstCaseComments;
    }

    @RemoteAction
    public static Boolean userSubscribeCaseStatus(String strCaseId) {
        List<Subscriber__c> lstSuscriber = [select id from Subscriber__c where Case__c =: strCaseId AND User__c =: userinfo.getuserId()];
        if(lstSuscriber.size() > 0) {
            return true;
        }
        return false;
    }

    @RemoteAction
    public static Boolean subscribeUnsubscribeCase(String strCaseId) {
        List<Subscriber__c> lstSuscriber = [select id from Subscriber__c where Case__c =: strCaseId AND User__c =: userinfo.getuserId()];
        if(lstSuscriber.size() > 0) {
            delete lstSuscriber;
            return false;
        }else
        {
            User userObj= [Select id,ContactId,AccountId, Contact.Email, Contact.AccountId, Email from User where id=:userinfo.getUserId() limit 1];
            Subscriber__c objSubscribe = new Subscriber__c();
            objSubscribe.Case__c = strCaseId;
            objSubscribe.User__c = userinfo.getUserId();
            if(String.isBlank(userObj.ContactId)){
                objSubscribe.Email__c = userObj.Email;
            } else
            {
                objSubscribe.Email__c = userObj.Contact.Email;
                objSubscribe.Account__c = userObj.Contact.AccountId;
            }
            insert objSubscribe;
            return true;
        }
        return null;
    }
    
    Public class CaseWrapperClass {
        Public Case caseObj;
        Public string strDateOccured;
        Public string strDateOpened;
    }
    
}