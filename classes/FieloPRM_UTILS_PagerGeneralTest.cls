// *******************************************
// Test Method
// *******************************************
@isTest

private class FieloPRM_UTILS_PagerGeneralTest{

    static testmethod void test(){
        
        AggregateResult[] results;
        
        
        Integer totalUsers;
    
        if(Test.isRunningtest()){
            totalUsers = 100;
        }else{
            results = [Select count(id) total from User];
            totalUsers = (Integer)results.get(0).get('total');
        }
       
        FieloPRM_UTILS_PagerGeneral paginatedList;
        
        if(Test.isRunningtest()){
            paginatedList = new FieloPRM_UTILS_PagerGeneral('Select id from User Limit 100',1);
        }else{
            paginatedList = new FieloPRM_UTILS_PagerGeneral('Select id from User',1);
        }
        
         
        paginatedList.getRecords();
        paginatedList.getItemsDisplaying();
        paginatedList.setPageNumber(1);
        paginatedList.getTotalPages();
        paginatedList.getPageSize();
        system.assertEquals(totalUsers,paginatedList.getResultSize());
        system.assertEquals(1, paginatedList.getPageNumber());
        system.assertEquals(false,paginatedList.hasPrevious);
        if(totalUsers>1){
            system.assertEquals(true,paginatedList.hasNext);
            paginatedList.next();
            system.assertEquals(true,paginatedList.hasPrevious);
            system.assertEquals(2, paginatedList.getPageNumber());
            paginatedList.previous();
            system.assertEquals(1, paginatedList.getPageNumber());
            paginatedList.last();
            system.assertEquals(totalUsers,paginatedList.getPageNumber());
            system.assertEquals(false,paginatedList.hasNext);
            paginatedList.first();
            system.assertEquals(1, paginatedList.getPageNumber());
             
        }else{
            system.assertEquals(false, paginatedList.hasNext);
        }
    }
}