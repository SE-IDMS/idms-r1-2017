/**************************
    Author: Fielo Team
***************************/
public without sharing class FieloPRM_AP_RewardPointsController{

    public Integer totalPoints {get;set;}
    public static FieloEE__Member__c member {get;set;}
    
    private String languageMember = FieloEE.OrganizationUtil.getLanguage();
    private static PointsAndLink pointsAndLinkRecord;
    private Id memId;
    
    public FieloPRM_AP_RewardPointsController(){
        memId = FieloEE.MemberUtil.getMemberId();
        member = [SELECT FieloEE__Points__c, F_PRM_CountryCode__c FROM FieloEE__Member__c WHERE Id =: memId] ;
    }   
    
    public PageReference goToRewardsOverview(){
        pointsAndLinkRecord = getPointsAndLink(memId);
        PageReference linkRewardsOverview = new PageReference(pointsAndLinkRecord.overviewLink);
        linkRewardsOverview.setRedirect(true);
        system.debug('###linkRewardsOverview: ' + linkRewardsOverview);
        return linkRewardsOverview;
    } 

    public static PointsAndLink getPointsAndLink(String memOrContactId){
        PointsAndLink pointsAndLinkTemp = new PointsAndLink();
        if(member == null && memOrContactId != null){
            member = [SELECT FieloEE__Points__c, F_PRM_CountryCode__c FROM FieloEE__Member__c WHERE Id =: memOrContactId OR FieloEE__User__r.ContactId =: memOrContactId] ;
        }
        if(member != null){            
            pointsAndLinkTemp.overviewLink = '';       
            if(member.F_PRM_CountryCode__c != null){
                pointsAndLinkTemp.overviewLink = '/partners/Menu/' + 'P_' + member.F_PRM_CountryCode__c.toUpperCase() + '_Rewards_Overview';
            }
            pointsAndLinkTemp.points = member.FieloEE__Points__c.intValue();           
        }
        return pointsAndLinkTemp;
    }
    
    public class PointsAndLink{
        public String overviewLink {get; set;}
        public Integer points {get; set;}
    }
    
}