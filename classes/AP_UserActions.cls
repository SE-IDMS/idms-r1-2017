public class AP_UserActions
{
    public static void populateLastActivationDateforInsert(List<User> users)
    {
        for(User u:users)
        {
            if(u.isActive){
            u.LastActivationDate__c=System.Today();
            u.LastActivationUser__c=UserInfo.getUserId();
            }
        }
    }
    
    public static void populateLastActivationDateforUpdate(Map<Id,User> oldUserMap,Map<Id,User> newUserMap)
    {
        for(Id userId:newUserMap.keySet())
        {
            if(oldUserMap.get(userId).isActive!=newUserMap.get(userId).isActive){
            newUserMap.get(userId).LastActivationDate__c=System.Today();
            newUserMap.get(userId).LastActivationUser__c=UserInfo.getUserId();
            }
        }
    }
}