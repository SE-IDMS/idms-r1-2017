@isTest

private class AP_UpdateMatchingCase_Test {

    public static testMethod void problemCase1() {
        
       
        String commRefTOTest='TEST_XBTGT5430';
        String gmrCodeToTest='02BF6DRG1234'; 
        
        Country__c country1=new Country__c(CountryCode__c='US', Name='United States');  
        insert country1;
        User runAsUser = Utils_TestMethods.createStandardUser('test');
        runAsUser.Country__c = country1.Id;
        insert runAsUser;
        
        System.runAs(runAsUser){
            Test.startTest();
            string prefix='aa';
            User  sampleUser=Utils_TestMethods.createStandardUser('sample'+prefix);
            Database.insert(sampleUser);
            
            Country__c objCountry = Utils_TestMethods.createCountry();
            objCountry.Name='TCY'; 
            objCountry.CountryCode__c='IN';
            Database.insert(objCountry);
            
            Account objAccount = Utils_TestMethods.createAccount();
            objAccount.Country__c = objCountry.Id;
            Database.insert(objAccount);
            
            Contact objContact=Utils_TestMethods.createContact(objAccount.Id,'TestContact');
            Database.insert(objContact);
                      
                          
            BusinessRiskEscalationEntity__c breEntity = Utils_TestMethods.createBusinessRiskEscalationEntity();
            Database.insert(breEntity);
            BusinessRiskEscalationEntity__c breSubEntity = Utils_TestMethods.createBusinessRiskEscalationEntityWithSubEntity();
            Database.insert(breSubEntity);
            BusinessRiskEscalationEntity__c breLocationType = Utils_TestMethods.createBusinessRiskEscalationEntityWithLocation ();
            Database.insert(breLocationType);
            
            List<CS_Severity__c> lstCS = new List<CS_Severity__c>();
            CS_Severity__c CS1 = new CS_Severity__c(Name = 'Minor', Value__c = 1);
            lstCS.add(CS1);
            CS_Severity__c CS2 = new CS_Severity__c(Name = 'Safety related', Value__c = 10);
            lstCS.add(CS2);
            Database.insert(lstCS);
             
            Problem__c prob = Utils_TestMethods.createProblem(breLocationType.Id,12);
            prob.ProblemLeader__c = sampleUser.id;
            prob.ProductQualityProblem__c = false;
            prob.TECH_Symptoms__c = 'Symptom - SubSymptom'; 
            prob.RecordTypeid = Label.CLI2PAPR120014;
            prob.Sensitivity__c = 'Public';
            prob.Severity__c = 'Safety related';
            Database.insert(prob);
            
            Problem__c prob1 = Utils_TestMethods.createProblem(breLocationType.Id,12);
            prob1.ProblemLeader__c = sampleUser.id;
            prob1.ProductQualityProblem__c = false;
            prob1.TECH_Symptoms__c = 'Symptom - SubSymptom'; 
            prob1.RecordTypeid = Label.CLI2PAPR120014;
            prob1.Sensitivity__c = 'Public';
            prob1.Severity__c = 'Minor';
            Database.insert(prob1); 
            
            Set<Id> probIds = new Set<Id>();
            probIds.add(prob.Id);
            probIds.add(prob1.Id);
            
            CommercialReference__c crs = new CommercialReference__c(Problem__c = prob.Id, Business__c = 'Business', 
                                                                    CommercialReference__c = commRefTOTest,Family__c = 'Family', 
                                                                    GMRCode__c = gmrCodeToTest,ProductLine__c = 'ProductLine', 
                                                                    ProductFamily__c ='ProductFamily');
            Database.insert(crs);
            CommercialReference__c crs1 = new CommercialReference__c(Problem__c = prob1.Id, Business__c = 'Business', 
                                                                    CommercialReference__c = '',Family__c = 'Family', 
                                                                    GMRCode__c = gmrCodeToTest.substring(8),ProductLine__c = 'ProductLine', 
                                                                    ProductFamily__c ='ProductFamily');
            Database.insert(crs1);
            
            
            Case objCase = Utils_TestMethods.createCase(objAccount.Id,objContact.Id,'In Progress');
            objCase.CaseSymptom__c= 'Symptom';
            objCase.CaseSubSymptom__c= 'SubSymptom';
            objCase.CommercialReference__c = commRefTOTest;
            objCase.TECH_GMRCode__c = gmrCodeToTest;
            objCase.ProductBU__c = 'Business';
            objCase.ProductLine__c = 'ProductLine';
            objCase.ProductFamily__c ='ProductFamily';
            objCase.Family__c ='Family';
            Database.insert(objCase);
            
            ProblemCaseUnifiedLink__c pcul = new ProblemCaseUnifiedLink__c();
            pcul.Case__c = objCase.Id;
            pcul.problem__c = prob1.Id;
            pcul.ProblemSeverity__c = 'OSA';
            insert pcul; 
            
            Case objCase1 = Utils_TestMethods.createCase(objAccount.Id,objContact.Id,'In Progress');
            objCase1.CaseSymptom__c= 'Symptom';
            objCase1.CaseSubSymptom__c= 'SubSymptom';
            objCase1.CommercialReference__c = '';
            objCase1.TECH_GMRCode__c = gmrCodeToTest.substring(8);
            objCase1.ProductBU__c = 'Business';
            objCase1.ProductLine__c = 'ProductLine';
            objCase1.ProductFamily__c ='ProductFamily';
            objCase1.Family__c ='Family';
            Database.insert(objCase1);
            
            Set<Id> caseIds = new Set<Id>();
            caseIds.add(objCase.Id);
            caseIds.add(objCase1.Id); 
                        
            List<problem__c> problemList=new List<problem__c>();
            for (Integer i=0; i<10;i++) {
                problem__c pr1=new problem__c(Sensitivity__c = 'Public', Severity__c=Label.CLOCT14I2P22, RecordTypeid = Label.CLI2PAPR120014,Title__c='Test1 '+i, TECH_Symptoms__c = 'Symptom - SubSymptom');
                problemList.add(pr1);
            }
            for (Integer i=0; i<Integer.valueOf(Label.CLOCT14I2P21)+10;i++) {
                problem__c pr1=new problem__c(Sensitivity__c = 'Public', Severity__c='Significant / High', RecordTypeid = Label.CLI2PAPR120014,Title__c='Test2 '+i, TECH_Symptoms__c = 'Symptom - SubSymptom');
                problemList.add(pr1);
            }
            
            insert problemList;
            
            List<CommercialReference__c> commRefList= new List<CommercialReference__c>();
            for (Problem__c prb:problemList) {
                CommercialReference__c cr1=new CommercialReference__c(gmrcode__c=gmrCodeToTest, ProductLine__c = 'ProductLine', commercialreference__c=commRefTOTest, problem__c=prb.id);
                commRefList.add(cr1);
            }
            insert commRefList;
            
            Map<Id,Case> mapCase = new Map<Id,Case>();
            mapCase.put(objCase.Id,objCase);
            mapCase.put(objCase1.Id,objCase1);
            
            AP_UpdateMatchingCase.performMatching(probIds);
            AP_UpdateMatchingCase.CaseWithOpenProblems(caseIds);
            AP_UpdateMatchingCase.updateCaseStakeholders(mapCase);
            AP_UpdateMatchingCase.checkPMI_CMI(mapCase, mapCase);
        }    
    }
    /*
    public static testMethod void problemCase2() { 
      
      Account Acc = Utils_TestMethods.createAccount();       
        Contact Ctct = Utils_TestMethods.createContact(Acc.Id,'TEST');              
        insert Acc;       
        insert Ctct;
        
        Case Cse = Utils_TestMethods.createCase(Acc.Id,Ctct.Id,'In Progress');
        Cse.Severity__c = 'Potential Safety Issue';
        insert Cse;
        Cse.CustomerMajorIssue__c = 'Yes';
        update Cse; 
    }
	*/
	
}