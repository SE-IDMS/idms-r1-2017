/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        : 11-Jun-2013
    Modification Log    : 11-Oct-2013 -- added intiative filter and filter option ALL
    Description         : Controller to IPO Program Progress Report
*/


public with sharing class IPOProgramProgressReportController {

    public IPOProgramProgressReportController(ApexPages.StandardController controller) {
      
      Quarter = 'Q1';
      Initiative = 'ALL';
      ProgOwner = 'ALL';
      Year = '2013';
      
     }
  
 public string Quarter{get;set;}
    
      public List<SelectOption> getFilterQuarter() {
      List<SelectOption> options = new List<SelectOption>();
       options.add(new SelectOption('Q1','Q1'));  
       options.add(new SelectOption('Q2','Q2'));     
       options.add(new SelectOption('Q3','Q3'));  
       options.add(new SelectOption('Q4','Q4'));    
       
       return options;
  }
  public string Year{get;set;}
  
        public List<SelectOption> getFilterYear() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('2013','2013'));
        options.add(new SelectOption('2014','2014'));
        options.add(new SelectOption('2015','2015'));
        
        return options;
        }
  
  public string ProgOwner{get;set;}
  public string Initiative{get;set;}
  
  // Program Owner Selection
  
  public List<SelectOption> getFilterProgramOwnerList() 
    {
    Set<String> SetProgramOwner = new Set<String>();
    List<SelectOption> options = new List<SelectOption>();
    List<IPO_Strategic_Program__c> lstIPOPrg = new List<IPO_Strategic_Program__c>();
    lstIPOPrg = [Select id, Name, Program_Owner__r.FirstName, Program_Owner__r.LastName from IPO_Strategic_Program__c];
    //options.add(new SelectOption('','<Select a Program Owner>'));  
    options.add(new SelectOption('ALL','All Program Owners'));  
    for(IPO_Strategic_Program__c PO : lstIPOPrg)
     {
       if(PO.Program_Owner__r.FirstName != null)
         SetProgramOwner.add(PO.Program_Owner__r.FirstName + ' ' + PO.Program_Owner__r.LastName);
        
      }
    for(string pown : SetProgramOwner)
    {
       options.add(new SelectOption(pown,pown)); 
     }
     options.sort();  
    return(options);
    }
    
    
  // Initiative List
  
    public List<SelectOption> getFilterInitiativeList() 
    {
    Set<String> SetInitiative = new Set<String>();
    List<SelectOption> options = new List<SelectOption>();
    List<IPO_Stategic__c> lstIni = new List<IPO_Stategic__c>();
    lstIni = [Select id, Name from IPO_Stategic__c];
    //options.add(new SelectOption('','<Select a Initiative>'));  
    options.add(new SelectOption('ALL','All Initiatives'));  
    for(IPO_Stategic__c Ini : lstIni)
     {
      
         SetInitiative.add(Ini.Name);
        
      }
    for(string init : SetInitiative)
    {
       options.add(new SelectOption(init,init)); 
     }
     options.sort();  
    return(options);
    }
   // Progress Snapshot
   
   public List<IPOProgress> getIPOPrgProgress()
   {
     List<IPOProgress> IPOPrg = new List<IPOProgress>();
     List<Program_Progress_Snapshot__c> PP = new List<Program_Progress_Snapshot__c>();
  if (Initiative == 'ALL' && ProgOwner == 'ALL')
      PP = [Select id,IPO_Program__r.name, Q1_Program_Health_Indicator__c,Q2_Program_Health_Indicator__c,Q3_Program_Health_Indicator__c,Q4_Program_Health_Indicator__c,Status_in_Quarter_Q1__c,Status_in_Quarter_Q2__c,Status_in_Quarter_Q3__c,Status_in_Quarter_Q4__c,Roadblocks_Q1__c,Roadblocks_Q2__c,Roadblocks_Q3__c,Roadblocks_Q4__c,Decision_Review_Outcomes_Q1__c,Decision_Review_Outcomes_Q2__c,Decision_Review_Outcomes_Q3__c,Decision_Review_Outcomes_Q4__c  from Program_Progress_Snapshot__c  where Current_Year__c = :Year order by IPO_Program__r.name];
  else 
      if(Initiative == 'ALL' && ProgOwner != 'ALL')
        PP = [Select id,IPO_Program__r.name, Q1_Program_Health_Indicator__c,Q2_Program_Health_Indicator__c,Q3_Program_Health_Indicator__c,Q4_Program_Health_Indicator__c,Status_in_Quarter_Q1__c,Status_in_Quarter_Q2__c,Status_in_Quarter_Q3__c,Status_in_Quarter_Q4__c,Roadblocks_Q1__c,Roadblocks_Q2__c,Roadblocks_Q3__c,Roadblocks_Q4__c,Decision_Review_Outcomes_Q1__c,Decision_Review_Outcomes_Q2__c,Decision_Review_Outcomes_Q3__c,Decision_Review_Outcomes_Q4__c  from Program_Progress_Snapshot__c where Program_Owner__c = :ProgOwner and Current_Year__c = :Year order by IPO_Program__r.name];
 else 
      if(Initiative != 'ALL' && ProgOwner == 'ALL')
        PP = [Select id,IPO_Program__r.name, Q1_Program_Health_Indicator__c,Q2_Program_Health_Indicator__c,Q3_Program_Health_Indicator__c,Q4_Program_Health_Indicator__c,Status_in_Quarter_Q1__c,Status_in_Quarter_Q2__c,Status_in_Quarter_Q3__c,Status_in_Quarter_Q4__c,Roadblocks_Q1__c,Roadblocks_Q2__c,Roadblocks_Q3__c,Roadblocks_Q4__c,Decision_Review_Outcomes_Q1__c,Decision_Review_Outcomes_Q2__c,Decision_Review_Outcomes_Q3__c,Decision_Review_Outcomes_Q4__c  from Program_Progress_Snapshot__c where initiative_Name__c = :Initiative and Current_Year__c = :Year order by IPO_Program__r.name];      
 else 
      if(Initiative != 'ALL' && ProgOwner != 'ALL')     
       PP = [Select id,IPO_Program__r.name, Q1_Program_Health_Indicator__c,Q2_Program_Health_Indicator__c,Q3_Program_Health_Indicator__c,Q4_Program_Health_Indicator__c,Status_in_Quarter_Q1__c,Status_in_Quarter_Q2__c,Status_in_Quarter_Q3__c,Status_in_Quarter_Q4__c,Roadblocks_Q1__c,Roadblocks_Q2__c,Roadblocks_Q3__c,Roadblocks_Q4__c,Decision_Review_Outcomes_Q1__c,Decision_Review_Outcomes_Q2__c,Decision_Review_Outcomes_Q3__c,Decision_Review_Outcomes_Q4__c  from Program_Progress_Snapshot__c where Program_Owner__c = :ProgOwner and Current_Year__c = :Year and initiative_Name__c = :Initiative order by IPO_Program__r.name];
     
  for(Program_Progress_Snapshot__c Pgs: PP)
       {
       if(Quarter == 'Q1')
           IPOPrg.add(new IPOProgress(Pgs.id, Pgs.IPO_Program__r.name,Pgs.Q1_Program_Health_Indicator__c,Pgs.Q2_Program_Health_Indicator__c, Pgs.Q3_Program_Health_Indicator__c, Pgs.Q4_Program_Health_Indicator__c,Pgs.Status_in_Quarter_Q1__c,Pgs.Roadblocks_Q1__c,Pgs.Decision_Review_Outcomes_Q1__c));
       else if(Quarter == 'Q2')  
            IPOPrg.add(new IPOProgress(Pgs.id, Pgs.IPO_Program__r.name,Pgs.Q1_Program_Health_Indicator__c,Pgs.Q2_Program_Health_Indicator__c, Pgs.Q3_Program_Health_Indicator__c, Pgs.Q4_Program_Health_Indicator__c,Pgs.Status_in_Quarter_Q2__c,Pgs.Roadblocks_Q2__c,Pgs.Decision_Review_Outcomes_Q2__c));  
       else if(Quarter == 'Q3')     
            IPOPrg.add(new IPOProgress(Pgs.id, Pgs.IPO_Program__r.name,Pgs.Q1_Program_Health_Indicator__c,Pgs.Q2_Program_Health_Indicator__c, Pgs.Q3_Program_Health_Indicator__c, Pgs.Q4_Program_Health_Indicator__c,Pgs.Status_in_Quarter_Q3__c,Pgs.Roadblocks_Q3__c,Pgs.Decision_Review_Outcomes_Q3__c));     
       else if(Quarter == 'Q4')     
            IPOPrg.add(new IPOProgress(Pgs.id, Pgs.IPO_Program__r.name,Pgs.Q1_Program_Health_Indicator__c,Pgs.Q2_Program_Health_Indicator__c, Pgs.Q3_Program_Health_Indicator__c, Pgs.Q4_Program_Health_Indicator__c,Pgs.Status_in_Quarter_Q4__c,Pgs.Roadblocks_Q4__c,Pgs.Decision_Review_Outcomes_Q4__c));         
      }
     return(IPOPrg);
   }
        
   // IPO Milestones Q1
   public List<IPOMile> getMilestoneDue()
   {
     List<IPO_Strategic_Milestone__c> MileDue = new List<IPO_Strategic_Milestone__c>();
     
     List<IPOMile> lstMileQ1 = new List<IPOMile>();
     if (Initiative == 'ALL' && ProgOwner != 'ALL')
       MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q1__c, Status_in_Quarter_Q1__c, Roadblocks_Q1__c,Decision_Review_Outcomes_Q1__c,Due_Date__c from IPO_Strategic_Milestone__c where Program_Owner__c = :ProgOwner  and Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q1_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q1_End) and Calendar_Year(Due_Date__c)= :Integer.valueof(Year)  order by Program__c ];
     else  if (Initiative != 'ALL' && ProgOwner != 'ALL')
      MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q1__c, Status_in_Quarter_Q1__c, Roadblocks_Q1__c,Decision_Review_Outcomes_Q1__c,Due_Date__c from IPO_Strategic_Milestone__c where Program_Owner__c = :ProgOwner  and Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q1_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q1_End)and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) and initiative_Name__c = :Initiative order by Program__c ];
     else  if (Initiative != 'ALL' && ProgOwner == 'ALL')
      MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q1__c, Status_in_Quarter_Q1__c, Roadblocks_Q1__c,Decision_Review_Outcomes_Q1__c,Due_Date__c from IPO_Strategic_Milestone__c where  Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q1_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q1_End)  and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) and initiative_Name__c = :Initiative order by Program__c]; 
      else  if (Initiative == 'ALL' && ProgOwner == 'ALL')
      MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q1__c, Status_in_Quarter_Q1__c, Roadblocks_Q1__c,Decision_Review_Outcomes_Q1__c,Due_Date__c from IPO_Strategic_Milestone__c where  Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q1_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q1_End) and Calendar_Year(Due_Date__c)= :Integer.valueof(Year)   order by Program__c ];
       System.Debug('********** Milestone Data start **********Milestone DUe Date');
     System.Debug('The value in MileDue is : '+MileDue);
     For(IPO_Strategic_Milestone__c MQ1: MileDue)
      {
       IPOMile MileQ1 = new IPOMile();
       MileQ1.id = MQ1.id;
       MileQ1.Name = MQ1.Name;
       MileQ1.Program = MQ1.Program__c;
       MileQ1.Milestone = MQ1.IPO_Strategic_Milestone_Name__c;
       MileQ1.Status = MQ1.Progress_Summary_Q1__c;
       MileQ1.Progress = MQ1.Status_in_Quarter_Q1__c; 
       MileQ1.Roadblock = MQ1.Roadblocks_Q1__c;
       MileQ1.Decision = MQ1.Decision_Review_Outcomes_Q1__c;
       lstMileQ1.add(MileQ1);
       }
       
     return(lstMileQ1);
    }
    // IPO Milestones Q2

    public List<IPOMile> getMilestoneDueQ2()
   {
     List<IPO_Strategic_Milestone__c> MileDue = new List<IPO_Strategic_Milestone__c>();
     
     List<IPOMile> lstMileQ2 = new List<IPOMile>();
     if (Initiative == 'ALL' && ProgOwner != 'ALL' )
     MileDue = [Select id,Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q1__c,Progress_Summary_Q2__c, Status_in_Quarter_Q1__c,Status_in_Quarter_Q2__c,Roadblocks_Q1__c, Roadblocks_Q2__c,Decision_Review_Outcomes_Q1__c,Decision_Review_Outcomes_Q2__c from IPO_Strategic_Milestone__c where Program_Owner__c = :ProgOwner  and Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q2_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q2_End) and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) order by Program__c ];
     else
     if (Initiative != 'ALL' && ProgOwner != 'ALL' )
     MileDue = [Select id,Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q1__c,Progress_Summary_Q2__c, Status_in_Quarter_Q1__c,Status_in_Quarter_Q2__c,Roadblocks_Q1__c, Roadblocks_Q2__c,Decision_Review_Outcomes_Q1__c,Decision_Review_Outcomes_Q2__c from IPO_Strategic_Milestone__c where Program_Owner__c = :ProgOwner  and Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q2_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q2_End)and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) and initiative_Name__c = :Initiative  order by Program__c ];
     else
     if (Initiative != 'ALL' && ProgOwner == 'ALL' )
     MileDue = [Select id,Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q1__c,Progress_Summary_Q2__c, Status_in_Quarter_Q1__c,Status_in_Quarter_Q2__c,Roadblocks_Q1__c, Roadblocks_Q2__c,Decision_Review_Outcomes_Q1__c,Decision_Review_Outcomes_Q2__c from IPO_Strategic_Milestone__c where Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q2_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q2_End) and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) and initiative_Name__c = :Initiative  order by Program__c ];
     else
     if (Initiative == 'ALL' && ProgOwner == 'ALL' )
     MileDue = [Select id,Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q1__c,Progress_Summary_Q2__c, Status_in_Quarter_Q1__c,Status_in_Quarter_Q2__c,Roadblocks_Q1__c, Roadblocks_Q2__c,Decision_Review_Outcomes_Q1__c,Decision_Review_Outcomes_Q2__c from IPO_Strategic_Milestone__c where Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q2_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q2_End) and Calendar_Year(Due_Date__c)= :Integer.valueof(Year)  order by Program__c ];
     
     For(IPO_Strategic_Milestone__c MQ2: MileDue)
      {
       IPOMile MileQ2 = new IPOMile();
        MileQ2.Program = MQ2.Program__c;
        MileQ2.id = MQ2.id;
        MileQ2.Name = MQ2.Name;
       MileQ2.Milestone = MQ2.IPO_Strategic_Milestone_Name__c;
       MileQ2.Status = MQ2.Progress_Summary_Q2__c;
       MileQ2.PStatus = MQ2.Progress_Summary_Q1__c;
       MileQ2.Progress = MQ2.Status_in_Quarter_Q2__c; 
       MileQ2.PProgress = MQ2.Status_in_Quarter_Q1__c;
       MileQ2.Roadblock = MQ2.Roadblocks_Q2__c;
       MileQ2.PRoadblock = MQ2.Roadblocks_Q1__c;
       MileQ2.Decision = MQ2.Decision_Review_Outcomes_Q2__c;
       MileQ2.PDecision = MQ2.Decision_Review_Outcomes_Q1__c;
       lstMileQ2.add(MileQ2);
       }
       
     return(lstMileQ2);
    }
    
    // IPO Milestone Q3
    public List<IPOMile> getMilestoneDueQ3()
   {
     List<IPO_Strategic_Milestone__c> MileDue = new List<IPO_Strategic_Milestone__c>();
     List<IPOMile> lstMileQ3 = new List<IPOMile>();
     if (Initiative == 'ALL' &&  ProgOwner == 'ALL')
      MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q2__c,Progress_Summary_Q3__c,Status_in_Quarter_Q2__c, Status_in_Quarter_Q3__c, Roadblocks_Q2__c,Roadblocks_Q3__c,
      Decision_Review_Outcomes_Q2__c,Decision_Review_Outcomes_Q3__c from IPO_Strategic_Milestone__c where Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q3_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q3_End)  and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) order by Program__c ];
     else  if (Initiative != 'ALL' &&  ProgOwner != 'ALL')
      MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q2__c,Progress_Summary_Q3__c,Status_in_Quarter_Q2__c, Status_in_Quarter_Q3__c, Roadblocks_Q2__c,Roadblocks_Q3__c,
      Decision_Review_Outcomes_Q2__c,Decision_Review_Outcomes_Q3__c from IPO_Strategic_Milestone__c where Program_Owner__c = :ProgOwner  and Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q3_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q3_End)and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) and initiative_Name__c = :Initiative  order by Program__c ];
    else  if (Initiative == 'ALL' &&  ProgOwner != 'ALL')
      MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q2__c,Progress_Summary_Q3__c,Status_in_Quarter_Q2__c, Status_in_Quarter_Q3__c, Roadblocks_Q2__c,Roadblocks_Q3__c,
      Decision_Review_Outcomes_Q2__c,Decision_Review_Outcomes_Q3__c from IPO_Strategic_Milestone__c where Program_Owner__c = :ProgOwner  and Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q3_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q3_End) and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) order by Program__c ];
    else  if (Initiative != 'ALL' &&  ProgOwner == 'ALL')
      MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q2__c,Progress_Summary_Q3__c,Status_in_Quarter_Q2__c, Status_in_Quarter_Q3__c, Roadblocks_Q2__c,Roadblocks_Q3__c,
      Decision_Review_Outcomes_Q2__c,Decision_Review_Outcomes_Q3__c from IPO_Strategic_Milestone__c where  Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q3_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q3_End) and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) and initiative_Name__c = :Initiative  order by Program__c ]; 
      
     For(IPO_Strategic_Milestone__c MQ3: MileDue)
      {
       IPOMile MileQ3 = new IPOMile();
        MileQ3.Program = MQ3.Program__c;
         MileQ3.id = MQ3.id;
          MileQ3.Name = MQ3.Name;
       MileQ3.Milestone = MQ3.IPO_Strategic_Milestone_Name__c;
       MileQ3.Status = MQ3.Progress_Summary_Q3__c;
       MileQ3.PStatus = MQ3.Progress_Summary_Q2__c;
       MileQ3.Progress = MQ3.Status_in_Quarter_Q3__c; 
       MileQ3.PProgress = MQ3.Status_in_Quarter_Q2__c; 
       MileQ3.Roadblock = MQ3.Roadblocks_Q3__c;
       MileQ3.PRoadblock = MQ3.Roadblocks_Q2__c;
       MileQ3.Decision = MQ3.Decision_Review_Outcomes_Q3__c;
       MileQ3.PDecision = MQ3.Decision_Review_Outcomes_Q2__c;
       lstMileQ3.add(MileQ3);
       }
       
     return(lstMileQ3);
    }
    
     // IPO Milestone Q4
    public List<IPOMile> getMilestoneDueQ4()
   {
     List<IPO_Strategic_Milestone__c> MileDue = new List<IPO_Strategic_Milestone__c>();
     List<IPOMile> lstMileQ4 = new List<IPOMile>();
     if (Initiative == 'ALL' && ProgOwner != 'ALL')
     MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q3__c,Progress_Summary_Q4__c,Status_in_Quarter_Q3__c, Status_in_Quarter_Q4__c,Roadblocks_Q3__c, Roadblocks_Q4__c,
     Decision_Review_Outcomes_Q3__c,Decision_Review_Outcomes_Q4__c from IPO_Strategic_Milestone__c where Program_Owner__c = :ProgOwner   and Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q4_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q4_End) and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) order by Program__c ];
     
     else if (Initiative != 'ALL' && ProgOwner != 'ALL')
     MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q3__c,Progress_Summary_Q4__c,Status_in_Quarter_Q3__c, Status_in_Quarter_Q4__c,Roadblocks_Q3__c, Roadblocks_Q4__c,
     Decision_Review_Outcomes_Q3__c,Decision_Review_Outcomes_Q4__c from IPO_Strategic_Milestone__c where Program_Owner__c = :ProgOwner   and Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q4_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q4_End) and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) and initiative_Name__c = :Initiative order by Program__c ];
     
     else if (Initiative != 'ALL' && ProgOwner == 'ALL')
     MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q3__c,Progress_Summary_Q4__c,Status_in_Quarter_Q3__c, Status_in_Quarter_Q4__c,Roadblocks_Q3__c, Roadblocks_Q4__c,
     Decision_Review_Outcomes_Q3__c,Decision_Review_Outcomes_Q4__c from IPO_Strategic_Milestone__c where  Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q4_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q4_End)  and Calendar_Year(Due_Date__c)= :Integer.valueof(Year) and initiative_Name__c = :Initiative order by Program__c ];
     
     else if (Initiative == 'ALL' && ProgOwner == 'ALL')
     MileDue = [Select id, Name, Program__c,IPO_Strategic_Milestone_Name__c,Progress_Summary_Q3__c,Progress_Summary_Q4__c,Status_in_Quarter_Q3__c, Status_in_Quarter_Q4__c,Roadblocks_Q3__c, Roadblocks_Q4__c,
     Decision_Review_Outcomes_Q3__c,Decision_Review_Outcomes_Q4__c from IPO_Strategic_Milestone__c where  Due_Date__c >= :Date.Valueof(Year + Label.IPO_Q4_Start) and Due_Date__c <= :Date.Valueof(Year + Label.IPO_Q4_End) and Calendar_Year(Due_Date__c)= :Integer.valueof(Year)  order by Program__c ];
     
     For(IPO_Strategic_Milestone__c MQ4: MileDue)
      {
       IPOMile MileQ4 = new IPOMile();
        MileQ4.Program = MQ4.Program__c;
        MileQ4.Name = MQ4.Name;
         MileQ4.id = MQ4.id;
       MileQ4.Milestone = MQ4.IPO_Strategic_Milestone_Name__c;
       MileQ4.Status = MQ4.Progress_Summary_Q4__c;
       MileQ4.PStatus = MQ4.Progress_Summary_Q3__c;
       MileQ4.Progress = MQ4.Status_in_Quarter_Q4__c; 
       MileQ4.PProgress = MQ4.Status_in_Quarter_Q3__c;
       MileQ4.Roadblock = MQ4.Roadblocks_Q4__c;
       MileQ4.PRoadblock = MQ4.Roadblocks_Q3__c;
       MileQ4.Decision = MQ4.Decision_Review_Outcomes_Q4__c;
       MileQ4.PDecision = MQ4.Decision_Review_Outcomes_Q3__c;
       lstMileQ4.add(MileQ4);
       }
       
     return(lstMileQ4);
    }
    
    // Milestone class
    
    public class IPOMile{
    public String id{get;set;}
    public String Program {get; set;}
    public String Milestone {get; set;}
    public String Status {get; set;}
    public String PStatus {get; set;}
    public String Progress{get;set;}   
    public String PProgress{get;set;} 
    public String Roadblock {get;set;}
    public String PRoadblock {get;set;}
    public String Decision {get;set;}
    public String PDecision {get;set;}
    public String Name{get;set;}
   
    public IPOMile(string id, string p,string m, string s, string pp, string rb, string d,string n,string ps,string pp1,string pd,string prb){
        this.id = id;
        this.Program=p;
        this.Milestone=m;
        this.Status=s;
        this.PStatus=ps;
        this.Progress=pp;
        this.PProgress=pp1;
        this.Roadblock=rb;
        this.PRoadblock=prb;
        this.Decision=d; 
        this.PDecision=pd;
        this.Name = n;      

    }
    public ipoMile()
      {
       }      
  
  }
 
 public class IPOProgress{
    public String id{get;set;}
    public String Program {get; set;}
    public String Q1Status {get; set;}
    public String Q2Status {get; set;}
    public String Q3Status {get; set;}
    public String Q4Status {get; set;}
    public String Progress{get;set;}   
    public String Roadblock {get;set;}
    public String Decision {get;set;} 
    
    public IPOProgress(string id, string prg,string q1,string q2,string q3,string q4, string pgs, string rb, string d){ 
    
    this.id = id;
    this.Program = prg;
    this.Q1Status = q1;
    this.Q2Status = q2;
    this.Q3Status = q3;
    this.Q4Status = q4;
    this.Progress = pgs;
    this.Roadblock = rb;
    this.Decision = d;
    
    }
  
 }  
}