@isTest

public class AP54_Contact_TEST {
    
    static testMethod void copyAddrFromAcc() {
        
        Country__c c = Utils_Testmethods.createCountry();
        INSERT c;
            
        Account acc = Utils_TestMethods.createAccount();
        acc.Country__c = c.Id;
        INSERT acc;
               
        Contact con1 = Utils_TestMethods.createContact(acc.Id, 'Contact');
        con1.Country__c = c.Id;
        INSERT con1;
        
        Contact con2 = Utils_TestMethods.createContact(acc.Id, 'Contact');
        con2.Country__c = c.Id;
        con2.UpdateAccountAddress__c=false;
        INSERT con2;
    }
}