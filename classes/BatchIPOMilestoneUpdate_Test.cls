/*
    Author              : Gayathri Shivakumar(Schneider Electric)
    Date Created        :  26-Jul-2013
    Description         : Test Class for BatchIPOMilestoneUpdate and BatchIPOCascadingMilestoneUpdate
*/

@isTest 
private class BatchIPOMilestoneUpdate_Test{
    static testMethod void classTest(){
         User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPO9');
        System.runAs(runAsUser){
        IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiativeTest');
        insert IPOinti;
        IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation__c='NA;APAC');
        insert IPOp;
        IPO_Strategic_Milestone__c IPOmile1=new IPO_Strategic_Milestone__c(IPO_Strategic_Program_Name__c=IPOp.id,IPO_Strategic_Milestone_Name__c='mile1',Global_Functions__c='GM',Global_Business__c='ITB',Operation_Regions__c='NA');
        insert IPOmile1;
        IPO_Strategic_Cascading_Milestone__c IPOcas1= new IPO_Strategic_Cascading_Milestone__c(IPO_Strategic_Milestone__c=IPOmile1.id,IPO_Strategic_Cascading_Milestone_Name__c='IPOcasmile1',Scope__c='Global Funtions',Entity__c='GM');
        insert IPOcas1;
        BatchIPOMilestoneUpdate b = new BatchIPOMilestoneUpdate();
        BatchIPOCascadingMilestoneUpdate c = new BatchIPOCascadingMilestoneUpdate();
        test.startTest();
        database.executebatch(b);
        database.executebatch(c);
        test.stopTest();
       }
    }
}