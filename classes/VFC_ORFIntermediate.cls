public class VFC_ORFIntermediate {
    //public Map<String,String> MapSelPrg= new Map<String,String>();
    public List<SelectOption> PrgLevelList{set;}
    public Map<id,String> mapPartnerPrograms = new Map<id,String>();  
    public String PrgLevel{
               get;
               set;
           }
    /*public String getPrgLevel() {
    System.debug('************************* Constructor3');
        if(MapSelPrg.size== 1)
            PrgLevel = MapSelPrg.ketset[0];
            
        return PrgLevel;    
    }*/
    
    public VFC_ORFIntermediate(ApexPages.StandardController controller){
    }
         
    public List<SelectOption> getPrgLevelList(){
        System.debug('************************* Constructor2');
        List<SelectOption> options = new List<SelectOption>();
        
        //added for test class      
        //User loggerInUser = [Select name, ContactId, Contact.AccountId from User where id= :UserInfo.getUserId()];
        User loggerInUser;
        if (System.Test.isRunningTest()) {
            String userid;
            if(ApexPages.currentPage().getParameters().containsKey('userid')) {
                userid  =ApexPages.currentPage().getParameters().get('userid');     
                loggerInUser = [Select name, ContactId, Contact.AccountId,contact.FieloEE__Member__c from User where id= :userid];
            }   
        }       
        else
            loggerInUser = [Select name, ContactId, Contact.AccountId, Contact.FieloEE__Member__c from User where id= :UserInfo.getUserId()];   
        
        //---------------------------
        Boolean hasValue = false;
        SET<String> SetPrg = new SET<String>(); 
        Map<String,String> MapSelPrg= new Map<String,String>();
        Map<id,String> mapAccPartnerPrograms = new Map<id,String>();  
        
        if(loggerInUser.ContactId != null) {
            // Account Partner Program
            //Map<id,String> mapAccPartnerPrograms = new Map<id,String>();       
           
                     
            List<ACC_PartnerProgram__c> lstAccPartnerPrograms = [SELECT PartnerProgram__r.Name, PartnerProgram__c, Account__c,ProgramLevel__c,ProgramLevel__r.Name 
                                                            FROM ACC_PartnerProgram__c 
                                                            WHERE Active__c = true AND Account__c = :loggerInUser.Contact.AccountId 
                                                            AND (PartnerProgram__r.ProgramStatus__c = :System.Label.CLMAY13PRM09 OR PartnerProgram__r.ProgramStatus__c = :System.Label.CLAPR14PRM03)];
        
            for(ACC_PartnerProgram__c apm : lstAccPartnerPrograms){
                mapAccPartnerPrograms.put(apm.ProgramLevel__c,apm.PartnerProgram__r.Name);
                mapPartnerPrograms.put(apm.ProgramLevel__c,apm.PartnerProgram__c);
            }   
                         
            if(!String.isBlank(loggerInUser.Contact.FieloEE__Member__c)){
                       
                List<FieloEE__BadgeMember__c> lstBadgeMember = [Select id,FieloEE__Badge2__c,FieloEE__Badge2__r.F_PRM_ProgramLevel__c,FieloEE__Badge2__r.F_PRM_ProgramLevel__r.PartnerProgram__r.Name,
                                                                FieloEE__Badge2__r.F_PRM_ProgramLevel__r.PartnerProgram__c
                                                                from FieloEE__BadgeMember__c where FieloEE__Member2__c = :loggerInUser.Contact.FieloEE__Member__c AND 
                                                                FieloEE__Badge2__r.F_PRM_ProgramLevel__c != null];
                                                        
                if(lstBadgeMember.size() > 0){
                    for(FieloEE__BadgeMember__c aBadge :lstBadgeMember){
                        if(!mapAccPartnerPrograms.containsKey(aBadge.FieloEE__Badge2__r.F_PRM_ProgramLevel__c))
                            mapAccPartnerPrograms.put(aBadge.FieloEE__Badge2__r.F_PRM_ProgramLevel__c,aBadge.FieloEE__Badge2__r.F_PRM_ProgramLevel__r.PartnerProgram__r.Name);
                        
                        if(!mapPartnerPrograms.containsKey(aBadge.FieloEE__Badge2__r.F_PRM_ProgramLevel__c))
                            mapPartnerPrograms.put(aBadge.FieloEE__Badge2__r.F_PRM_ProgramLevel__c,aBadge.FieloEE__Badge2__r.F_PRM_ProgramLevel__r.PartnerProgram__c);
                    }
                }               
            }                                              
                
            List <ProgramFeature__c> lstAccFeature = new List<ProgramFeature__c>();
            if(mapAccPartnerPrograms.size() > 0){
                lstAccFeature = [Select FeatureCatalog__r.Category__c,ProgramLevel__c,ProgramLevel__r.Name from ProgramFeature__c 
                                  Where ProgramLevel__c = :mapAccPartnerPrograms.keySet()];
            }
            
            if(lstAccFeature.size() >0){
            
                for(ProgramFeature__c accftr :lstAccFeature){
           
                 List<String> lstAccPartnerFeature = accftr.FeatureCatalog__r.Category__c.split(';');
                   system.debug('******* +++++:'+lstAccPartnerFeature );
                   for(String apgl :lstAccPartnerFeature)
                   {
                    // if(apgl == 'Opportunity Registration' || apgl == 'Project'){
                     if(System.label.CLFEB14PRM18.contains(apgl))
                     {
                         //SetPrg.add(mapAccPartnerPrograms.get(accftr.ProgramLevel__c)+' - '+accftr.ProgramLevel__r.Name+' ('+apgl+')'); 
                        // MapSelPrg.put(accftr.ProgramLevel__c+'-'+apgl,mapAccPartnerPrograms.get(accftr.ProgramLevel__c)+' - '+accftr.ProgramLevel__r.Name+' ('+apgl+')');
                         MapSelPrg.put(accftr.ProgramLevel__c+'-'+apgl,mapAccPartnerPrograms.get(accftr.ProgramLevel__c)+' - '+accftr.ProgramLevel__r.Name);
                     }
                    }     
                }                       
                  
           
            }
           
           //------------------------------------------  
           
                
             
        } 
        

        if(MapSelPrg.size()==1)
        {
            for(String ch :MapSelPrg.keyset()) {
                options.add(new SelectOption(ch,MapSelPrg.get(ch)));
                PrgLevel = ch;
            } 
                                
        }
        else if(MapSelPrg.size() > 1){
        
            for(String ch :MapSelPrg.keyset()) {
                options.add(new SelectOption(ch,MapSelPrg.get(ch)));
            }   
        
        }
             
        return options;
        
    }
    
    public pagereference doCancel(){ 

        PageReference orfPage = new PageReference('/');
        return orfPage  ;
        
    }
    
    public pagereference doProceed() {
        
        //012K0000000D811
        PageReference pg1;
        system.debug('*******'+PrgLevel);
        if(PrgLevel != null) {
             
            Integer pos = PrgLevel.indexof('-');
            system.debug('**********'+PrgLevel.substring(pos+1).trim());
            system.debug('*****Id*****'+PrgLevel.substring(0,pos).trim());
            String projlevelid = PrgLevel.substring(0,pos).trim();
            String programID = mapPartnerPrograms.get(projlevelid);
            // if(pos == -1 || PrgLevel.substring(pos+1).trim() != 'Project')
            if(pos == -1 || PrgLevel.substring(pos+1).trim() != System.label.CLFEB14PRM19)
                pg1 = New PageReference('/' +'apex/VFP_EnhancedOpportunityRegistration?projname='+projlevelid+'&recordtype='+system.label.CLFEB14PRM05+'&orfType=2'+'&programid='+programID);
            else
                pg1 = New PageReference('/' +'apex/VFP_ProjectOpportunityRegistration?projname='+projlevelid+'&recordtype='+system.label.CLFEB14PRM04+'&orfType=1'+'&programid='+programID);
                
            return pg1; 
        }
        else
            return null;
    }
    //------------------------------------------------------
    
    public pagereference CheckProceed()         
     {
        User loggerInUser = [Select name, ContactId, Contact.AccountId from User where id= :UserInfo.getUserId()];
        if( loggerInUser.ContactId == null) {
           PageReference pg;
           pg = New PageReference('/' +'apex/VFP_EnhancedOpportunityRegistration?&recordtype=' + System.Label.CLFEB14PRM05 + '&orfType=2');
           return pg;      
        }

        PageReference pg2;  
        if(getPrgLevelList().size() ==1){
            pg2 = doProceed();
            pg2.setRedirect(true);
            return pg2;
        }   
        else
            return null;
        
    
    }   
    
    //--------------------------------------------------------
   
}