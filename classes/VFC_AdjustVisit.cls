/* Controller for VFP_AdjustVisit*/

public class VFC_AdjustVisit{

    //Constructor
    public Id AssignedToId{get;set;}
    public Id capid{get;set;}
    public String manager{get;set;}
    public String assignedto{get;set;}
    public List<SFE_PlatformingScoring__c> pslist{get;set;}
    public SFE_PlatformingScoring__c ps1{get;set;}{ps1=new SFE_PlatformingScoring__c();}
    public SFE_PlatformingScoring__c ps2{get;set;}{ps2=new SFE_PlatformingScoring__c();}
    public SFE_PlatformingScoring__c ps3{get;set;}{ps3=new SFE_PlatformingScoring__c();}
    public SFE_PlatformingScoring__c ps4{get;set;}{ps4=new SFE_PlatformingScoring__c();}
    public SFE_PlatformingScoring__c ps5{get;set;}{ps5=new SFE_PlatformingScoring__c();}
    public SFE_PlatformingScoring__c acc1{get;set;}{acc1=new SFE_PlatformingScoring__c();}
    public SFE_PlatformingScoring__c acc2{get;set;}{acc2=new SFE_PlatformingScoring__c();}
    public SFE_PlatformingScoring__c acc3{get;set;}{acc3=new SFE_PlatformingScoring__c();}
    public SFE_PlatformingScoring__c acc4{get;set;}{acc4=new SFE_PlatformingScoring__c();}
    public SFE_PlatformingScoring__c acc5{get;set;}{acc5=new SFE_PlatformingScoring__c();}
    public VFC_AdjustVisit() {
        AssignedToId=(ID)System.currentPageReference().getParameters().get( 'AssignedToId' );
        capid=(ID)System.currentPageReference().getParameters().get( 'capID' );
        pslist=new List<SFE_PlatformingScoring__c>();
        for(Integer i=0;i<5;i++){
            SFE_PlatformingScoring__c ps=new SFE_PlatformingScoring__c();
            pslist.add(ps);
        }
        List<SFE_IndivCAP__c> cap=[select OwnerId,AssignedTo__c,AssignedTo__r.Name,AssignedTo__r.Manager.Name from SFE_IndivCAP__c where Id=:capid];
        assignedto=cap[0].AssignedTo__r.Name;
        manager=cap[0].AssignedTo__r.Manager.Name;
        System.debug('>>>>>'+AssignedToId+'>>>>'+assignedto+'>>>>>'+manager);
   }
   
    public class eventtWrapper {
        public Id accID{get;set;}
        public String Name{get;set;}
        public String city{get;set;}
        public String AccountMasterProfile{get;set;}
        public String CAPProfile{get;set;}
        //Event Information
        public Integer EventClosedFY{get;set;}   
        public Integer EventFromCAP{get;set;}  
        public Integer AllEvent{get;set;} 
        public Integer AllEventOriginal{get;set;} 
        public Integer ClosedEvents{get;set;} 
        public Integer OpenEvents{get;set;}
        public Integer OpenEventsOriginal{get;set;} 
        public Integer Adjustment{get;set;}
        public Integer AdjustmentHidden{get;set;}
        public Boolean showOppty ;
        public opptytWrap oppty{get;set;} // Opportunity information
  }

    public class wrapAddAccounts{
        public List<eventtWrapper> evedatawrap{get;set;}
        public String errormsg{get;set;}  
    } 
       
     public class eventtWrap{
        public Id Id{get;set;}
        public Integer countEve{get;set;}   
        public String Name{get;set;}
        public opptytWrap oppty;
    }
    public class opptytWrap{
        public Decimal wonOppty{get;set;}
        public Decimal openOppty{get;set;}
        public Decimal newlyOppty{get;set;}
        public String userCurrencyCode{get;set;}
        public String decimalseperator{get;set;} 
    }
     
    @RemoteAction
    public static Boolean isManager(Id CAPID){
       List<SFE_IndivCAP__c> cap=[select OwnerId,AssignedTo__c,AssignedTo__r.Name,AssignedTo__r.ManagerId from SFE_IndivCAP__c where Id=:CAPID];
       if(cap.size()>0){
           if(UserInfo.getUserId()==cap[0].OwnerId || UserInfo.getUserId()==cap[0].AssignedTo__r.ManagerId)
               return true;
            else
                return false;
        }
        return false;
    }  
    
    @RemoteAction  
    public static opptytWrap getOpptyData(Id accId){
        Map<String,Decimal> currencyMap = new Map<String,Decimal>();     
        for(CurrencyType ct:[SELECT ConversionRate,DecimalPlaces,IsActive,IsoCode FROM CurrencyType WHERE IsActive = true])            
            currencyMap.put(ct.IsoCode,ct.ConversionRate);
        User currentUser=[select CurrencyIsoCode,DefaultCurrencyIsoCode from user where Id=:UserInfo.getUserId()];
        opptytWrap oppty=new opptytWrap();
        oppty.userCurrencyCode=currentUser.DefaultCurrencyIsoCode ;
        oppty.decimalseperator=CS_CurrencyDecimalSeperator__c.getValues(UserInfo.getLocale()).DecimalSeperator__c;
        System.debug('Conversionrate>>>>>> '+currencyMap.get(currentUser.DefaultCurrencyIsoCode));
        List<AggregateResult> wonOppty=[select sum(Amount)amt from Opportunity where (AccountId=:accId OR EndUserCustomer__c=:accId OR FinalCustSiteAcc__c=:accId) and CloseDate= LAST_N_DAYS:365 and StageName=:Label.CLMAR16SLS05];
        if(wonOppty[0].get('amt')!=null){
            oppty.wonOppty=(Decimal)wonOppty[0].get('amt')*currencyMap.get(currentUser.DefaultCurrencyIsoCode);
            System.debug('>>>>wonOppty '+wonOppty[0].get('amt'));
       } else
           oppty.wonOppty=0;
        List<AggregateResult> openOppty=[select sum(Amount)amt  from Opportunity where (AccountId=:accId OR EndUserCustomer__c=:accId OR FinalCustSiteAcc__c=:accId) and StageName!=:Label.CLMAR16SLS05 and StageName!=:Label.CLOCT15SLS77];
        if(openOppty[0].get('amt')!=null){
            oppty.openOppty=(Decimal)openOppty[0].get('amt')*currencyMap.get(currentUser.DefaultCurrencyIsoCode);
            System.debug('>>>>openOppty '+openOppty[0].get('amt'));
         }else
            oppty.openOppty=0;
        List<AggregateResult> newlyOppty=[select sum(Amount)amt from Opportunity where (AccountId=:accId OR EndUserCustomer__c=:accId OR FinalCustSiteAcc__c=:accId) and CreatedDate = THIS_YEAR];
        if(newlyOppty[0].get('amt')!=null){
            oppty.newlyOppty=(Decimal)newlyOppty[0].get('amt')*currencyMap.get(currentUser.DefaultCurrencyIsoCode);
            System.debug('>>>>newlyOppty '+newlyOppty[0].get('amt'));
         }else
            oppty.newlyOppty=0;   
        return oppty;
    }
 
  
   @RemoteAction  
    public static Map<Id,AggregateResult> getsalesEventClosedLastFY(Id AssignedToId){
        Map<Id,AggregateResult> salesEventClosedLastFY=new Map<Id,AggregateResult>([select AccountId Id,count(Id) countEve from Event where OwnerId=:AssignedToId and Activitydate=LAST_YEAR and What.Type = 'Account' and Status__c='Closed' and  RecordTypeId =:Label.CLMAY13SLS01 group by accountId]);
        return salesEventClosedLastFY;
    }
    
    @RemoteAction  
    public static Map<Id,AggregateResult> getsalesEventFromCAP(Id AssignedToId){
        Map<Id,AggregateResult> salesEventFromCAP=new Map<Id,AggregateResult>([select AccountId Id,count(Id) countEve from Event where OwnerId=:AssignedToId and Activitydate=THIS_YEAR and What.Type = 'Account' and TECH_PlatformingScoringID__c!=null group by accountId]);
        return salesEventFromCAP;
    }
    
    @RemoteAction  
    public static Map<Id,AggregateResult> getALLevent(Id AssignedToId){
        Map<Id,AggregateResult> salesEventALL=new Map<Id,AggregateResult>([select AccountId Id,count(Id) countEve from Event where OwnerId=:AssignedToId and Activitydate=THIS_YEAR and What.Type = 'Account' and  RecordTypeId =:Label.CLMAY13SLS01 and  Status__c!='Cancelled' group by accountId]);
        return salesEventALL;
    }
    
    @RemoteAction  
    public static Map<Id,AggregateResult> getsalesEventClosed(Id AssignedToId){
        Map<Id,AggregateResult> salesEventClosed=new Map<Id,AggregateResult>([select AccountId Id,count(Id) countEve from Event where OwnerId=:AssignedToId and Activitydate=THIS_YEAR and What.Type = 'Account' and  RecordTypeId =:Label.CLMAY13SLS01  and Status__c='Closed' group by accountId]);
        return salesEventClosed;
    }
        @RemoteAction  
    public static Map<Id,AggregateResult> getsalesEventOpen(Id AssignedToId){
        Map<Id,AggregateResult> salesEventOpen=new Map<Id,AggregateResult>([select AccountId Id,count(Id) countEve from Event where OwnerId=:AssignedToId and Activitydate=THIS_YEAR and What.Type = 'Account' and  RecordTypeId =:Label.CLMAY13SLS01  and Status__c!='Closed'  and  Status__c!='Cancelled' group by accountId]);
        return salesEventOpen;
    }

     @RemoteAction  
       public static List<eventtWrapper> geteventData(Map<Id,eventtWrap> salesEventClosedLastFY,Map<Id,eventtWrap> salesEventFromCAP,Map<Id,eventtWrap> salesEventALL,Map<Id,eventtWrap> salesEventClosed,Map<Id,eventtWrap> salesEventOpen,Id capID){
        List<eventtWrapper> eventWrapList=new List<eventtWrapper>();
        Map<Id,Account> accMap=new Map<Id,Account>([select name,city__c,AccountMasterProfile__c from Account where id in:salesEventALL.keyset() or id in:salesEventFromCAP.keyset()]);
        List<SFE_PlatformingScoring__c> sfeList=[select PlatformedAccount__c,CustomerProfile__c from SFE_PlatformingScoring__c where IndivCAP__c=:capID];
        Map<Id,String> psMap=new Map<Id,String>();
        for(SFE_PlatformingScoring__c ps:sfeList)
            psMap.put(ps.PlatformedAccount__c,ps.CustomerProfile__c);
       
        for(Id ar:salesEventALL.keyset()){
            eventtWrapper ew=new eventtWrapper();
            ew.accID=(Id)ar;
            ew.Name=accMap.get(ar).name;
            ew.city=accMap.get(ar).city__c;
            ew.AccountMasterProfile=accMap.get(ar).AccountMasterProfile__c;
            ew.CAPProfile=psMap.get(ar);
            if(salesEventClosedLastFY.get(ar)!=null)
                ew.EventClosedFY=salesEventClosedLastFY.get(ar).countEve;
            else
                ew.EventClosedFY=0;
            if(salesEventFromCAP.get(ar)!=null) 
                ew.EventFromCAP=salesEventFromCAP.get(ar).countEve;
            else
                ew.EventFromCAP=0;
            if(salesEventALL.get(ar)!=null) 
                ew.AllEvent=salesEventALL.get(ar).countEve;
            else
                ew.AllEvent=0;
            ew.AllEventOriginal=ew.AllEvent;
            if(salesEventClosed.get(ar)!=null) 
                ew.ClosedEvents=salesEventClosed.get(ar).countEve;
            else
               ew.ClosedEvents=0; 
            if(salesEventOpen.get(ar)!=null) 
                ew.OpenEvents=salesEventOpen.get(ar).countEve;  
            else
               ew.OpenEvents=0;  
            ew.OpenEventsOriginal= ew.OpenEvents;
            eventWrapList.add(ew);
        }
        for(Id ar:salesEventFromCAP.keyset()){
            if(!salesEventALL.ContainsKey(ar)){
                eventtWrapper ew=new eventtWrapper();
                ew.accID=(Id)ar;
                ew.Name=accMap.get(ar).name;
                ew.city=accMap.get(ar).city__c;
                ew.AccountMasterProfile=accMap.get(ar).AccountMasterProfile__c;
                ew.CAPProfile=psMap.get(ar);
                if(salesEventClosedLastFY.get(ar)!=null)
                    ew.EventClosedFY=salesEventClosedLastFY.get(ar).countEve;
                else
                    ew.EventClosedFY=0;
                if(salesEventFromCAP.get(ar)!=null) 
                    ew.EventFromCAP=salesEventFromCAP.get(ar).countEve;
                else
                    ew.EventFromCAP=0;
                if(salesEventALL.get(ar)!=null) 
                    ew.AllEvent=salesEventALL.get(ar).countEve;
                else
                    ew.AllEvent=0;
                ew.AllEventOriginal=ew.AllEvent;
                if(salesEventClosed.get(ar)!=null) 
                    ew.ClosedEvents=salesEventClosed.get(ar).countEve;
                else
                   ew.ClosedEvents=0; 
                if(salesEventOpen.get(ar)!=null) 
                    ew.OpenEvents=salesEventOpen.get(ar).countEve;  
                else
                   ew.OpenEvents=0;  
                ew.OpenEventsOriginal= ew.OpenEvents;
                eventWrapList.add(ew);
            }
        }
        
        System.debug('>>>>> eventWrapList >>> '+eventWrapList);   
        return eventWrapList;
   } 
   
   @RemoteAction  
    public static List<SFE_PlatformingScoring__c> addAccounts(){
            List<SFE_PlatformingScoring__c> pslist1=new List<SFE_PlatformingScoring__c>();
               for(Integer i=0;i<5;I++){
               SFE_PlatformingScoring__c ps=new SFE_PlatformingScoring__c();
               pslist1.add(ps);
           }
           return pslist1;
    } 
    
     @RemoteAction  
    public static wrapAddAccounts submitAccounts(List<Id> accList,List<eventtWrapper> data,Id AssignedToId){
    //User will be able to add new accounts where he doesnt have activities
           System.debug('>>>>>submitaccList>> '+accList);
           wrapAddAccounts wrapacc=new wrapAddAccounts(); 
           wrapacc.evedatawrap=data;
           wrapacc.errormsg=null;
         if(accList.size()>0){
           Map<Id,Account> acctMap=new Map<Id,Account>([select Id,name,Inactive__c,ToBeDeleted__c,city__c,AccountMasterProfile__c from Account where id in:accList]);
           Set<Id> newaccset = new Set<Id>();
           Set<Id> accset = new Set<Id>();
           for(Integer i=0;i<data.size();i++)
                accset.add(data[i].accID);
            for(Id acc:accList){
                if(accset.contains(acc)){
                    if(wrapacc.errormsg!=null)
                        wrapacc.errormsg+=acctMap.get(acc).name + Label.CLQ316SLS029;
                    else
                        wrapacc.errormsg=acctMap.get(acc).name + Label.CLQ316SLS029;
                }
                if(newaccset.contains(acc)){
                    if(wrapacc.errormsg!=null)
                        wrapacc.errormsg+=acctMap.get(acc).name + Label.CLQ316SLS030;
                    else
                        wrapacc.errormsg=acctMap.get(acc).name + Label.CLQ316SLS030;
                }
                else
                    newaccset.add(acc);
                if(acctMap.get(acc).Inactive__c || acctMap.get(acc).ToBeDeleted__c ){
                    if(wrapacc.errormsg!=null)
                        wrapacc.errormsg+=acctMap.get(acc).name + Label.CLQ316SLS031;
                    else
                        wrapacc.errormsg=acctMap.get(acc).name + Label.CLQ316SLS031;
                }
                
           }
           if(newaccset.size()>0 && wrapacc.errormsg==null){
               for(Id acc:accList){
                    eventtWrapper ew=new eventtWrapper();
                    ew.accID=acc;
                    ew.Name=acctMap.get(acc).name;
                    ew.city=acctMap.get(acc).city__c;
                    ew.EventClosedFY=0;
                    ew.EventFromCAP=0;
                    ew.AllEvent=0;
                    ew.AllEventOriginal=0;
                    ew.ClosedEvents=0; 
                    ew.OpenEvents=0;
                    ew.OpenEventsOriginal=0;
                    System.debug('>>>>>>submit ew ******'+ew);
                    wrapacc.evedatawrap.add(ew);
                } 
            }
          
          }            
           else{
               if(wrapacc.errormsg!=null)
                        wrapacc.errormsg+='Please select Account from the lookup icon <br/>';
                    else
                        wrapacc.errormsg='Please select Account from the lookup icon<br/>';
           }

           return wrapacc;
      
    } 
   
   @RemoteAction  
    public static String adjustvisits(List<eventtWrapper> data,Id capid,Id AssignedToId){
        System.debug('****adjustvisits>>>>*****');
        System.debug('****adjudata>> '+data);
        String messageAdjustVisits;
        Map<Id,Integer> adjneg=new Map<Id,Integer>();
        Map<Id,Integer> adjpos=new Map<Id,Integer>();
        for(eventtWrapper ar:data){
            eventtWrapper ew=new eventtWrapper();
            ew=ar;
            if(ar.Adjustment!=null && ar.Adjustment!=0){
                if(ew.AdjustmentHidden!=null)
                    ew.AdjustmentHidden+=ar.Adjustment;
                else
                  ew.AdjustmentHidden=ar.Adjustment;
            } 
          if(ar.AdjustmentHidden!=null && ar.AdjustmentHidden!=0 && ar.AdjustmentHidden<=100 && ar.OpenEventsOriginal+ew.AdjustmentHidden>=0){
            if(ew.AdjustmentHidden<0)
                adjneg.put(ar.accID,ew.AdjustmentHidden);
            else
                adjpos.put(ar.accID,ew.AdjustmentHidden);   
            }
        }
        //SFE_IndivCAP__c cap=[select Id,EndDate__c from SFE_IndivCAP__c where id=:capid limit 1];
        Date endDate = Date.newInstance(date.Today().year(), 12, 31);
        if(adjneg.size()>0 || adjpos.size()>0){
            try {
                Integer currentQueuedJobsInTranstn = Limits.getQueueableJobs();
                Integer maxQueuedJobsInTranstn = Limits.getLimitQueueableJobs();
                System.debug('getQueueableJobs ***' + Limits.getQueueableJobs() +'**getLimitQueueableJobs **'+Limits.getLimitQueueableJobs());
                if(currentQueuedJobsInTranstn < maxQueuedJobsInTranstn) {
                    ID jobID = System.enqueueJob (new AP_CAPEventGeneration(adjneg,adjpos,endDate,AssignedToId));
                    System.debug('* job ID***' + jobID);
                    messageAdjustVisits= Label.CLQ316SLS027+Label.CLQ316SLS028;
                    } else {
                        System.debug('>>>>Maximum queueable limit reached');
                        messageAdjustVisits=Label.CLQ316SLS026;
                    }
                } catch (Exception ex) {
                   System.debug('*** Exception Enqueue-ing AP_CAPEventGeneration***'+ex.getMessage()+' '+ex.getstacktracestring()); 
                    messageAdjustVisits=Label.CLQ316SLS025+ex.getMessage();
                }
        }
        else
            messageAdjustVisits=Label.CLQ316SLS024;
        return messageAdjustVisits;
    }
    
    @RemoteAction  
    public static List<eventtWrapper> simulateImpacts(List<eventtWrapper> data){
        System.debug('Adjust>>>> '+data);
        List<eventtWrapper> eventWrapList=new List<eventtWrapper>();
        for(eventtWrapper ar:data){
            eventtWrapper ew=new eventtWrapper();
            ew=ar;
            if(ar.Adjustment!=null && ar.Adjustment!=0){
                ew.AllEvent=ew.AllEvent+ar.Adjustment;
                ew.OpenEvents=ew.OpenEvents+ar.Adjustment;
               if(ew.AdjustmentHidden!=null)
                    ew.AdjustmentHidden+=ar.Adjustment;
                else
                  ew.AdjustmentHidden=ar.Adjustment; 
            } 
             ew.Adjustment=null;   
            eventWrapList.add(ew);
            
        }
        return eventWrapList;
    }   
}