global class VFC_CassiniAccess {
     //global String strContractdays {get;set;}
  //  global String msg {get;set;}
  public static List<Package__c> lstPackage ;
  public static List<Contract > lstContracts;
  //public String StrcheckContract  {get;set;}
    public Static String strerror{get;set;}
    @RemoteAction
    public static String Contractdays(){
       lstContracts = new  List<Contract >();
      Integer contractDays =0;
      lstPackage  = new List<Package__c>();
        List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){
            List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){
                  lstContracts=[SELECT Id,Points__c,EndDate,Status FROM Contract Where Id=:lstCVCPs[0].Contract__c and WebAccess__c =: 'Cassini' LIMIT 1];
               if(!lstContracts.isEmpty()){
                    //points=integer.valueOf(lstContracts[0].Points__c);
                    if(!Test.isRunningTest()) contractDays=System.Today().daysbetween(lstContracts[0].EndDate);

                     lstPackage=[SELECT PackageTemplate__c, 
                                PackageTemplate__r.PortalAccess__c, 
                                PackageTemplate__r.PointOfContact__c,   
                                PackageTemplate__r.PointOfContact__r.Priority1Routing__c, 
                                PackageTemplate__r.PointOfContact__r.DefaultRoutingTeam__c,
                                PackageTemplate__r.PointOfContact__r.Organization__c   
                                FROM Package__c WHERE Contract__c = :lstContracts[0].Id 
                                AND PackageTemplate__r.PortalAccess__c = TRUE
                                
                                AND PackageTemplate__r.PointOfContact__r.ChannelType__c ='Portal' 
                                ORDER BY LastModifiedDate DESC LIMIT 1];
                    
                
                }
            }
        }
        //system.debug('Check:'+lstContracts[0].EndDate.addDays(30));
        //system.debug(system.now()<lstContracts[0].EndDate.addDays(30));
        //system.debug(lstContracts);
        //system.debug(lstPackage);
        if(lstContracts.isEmpty()){
            return 'no_access';
        }
        else if(!lstPackage.isEmpty()  &&( lstContracts[0].Status =='Grace' ||(lstContracts[0].Status =='Activated' &&  system.now() < lstContracts[0].EndDate.addDays(30)))){
            //system.debug('1st condition');
            return 'unableFaq_casecreate';
        }
        else if(lstPackage.isEmpty()  &&( lstContracts[0].Status =='Grace' ||(lstContracts[0].Status =='Activated' &&  system.now()< lstContracts[0].EndDate.addDays(30)))){
              //system.debug('2 nd condition');
            return 'unablefaq';
        }
        else{
          return 'no_access';  
        }
    }
    public string  getStrcheckContract(){
        lstContracts = new  List<Contract >();
      Integer contractDays =0;
      lstPackage  = new List<Package__c>();
        List<User> lstContactId=[SELECT ContactId FROM User WHERE Id = :Userinfo.GetuserId()];
        if(lstContactId.size()>0){
            List <CTR_ValueChainPlayers__c> lstCVCPs=[SELECT Contact__c,Id,Name,Contract__c,LastModifiedDate FROM CTR_ValueChainPlayers__c WHERE Contact__c =:lstContactId[0].ContactId ORDER BY LastModifiedDate DESC LIMIT 1];
            if(lstCVCPs.size()>0){
                  lstContracts=[SELECT Id,Points__c,EndDate,Status FROM Contract Where Id=:lstCVCPs[0].Contract__c and WebAccess__c =: 'Cassini' LIMIT 1];
               if(!lstContracts.isEmpty()){
                    //points=integer.valueOf(lstContracts[0].Points__c);
                    if(!Test.isRunningTest()) contractDays=System.Today().daysbetween(lstContracts[0].EndDate);

                     lstPackage=[SELECT PackageTemplate__c, 
                                PackageTemplate__r.PortalAccess__c, 
                                PackageTemplate__r.PointOfContact__c,   
                                PackageTemplate__r.PointOfContact__r.Priority1Routing__c, 
                                PackageTemplate__r.PointOfContact__r.DefaultRoutingTeam__c,
                                PackageTemplate__r.PointOfContact__r.Organization__c   
                                FROM Package__c WHERE Contract__c = :lstContracts[0].Id 
                                AND PackageTemplate__r.PortalAccess__c = TRUE
                                
                                AND PackageTemplate__r.PointOfContact__r.ChannelType__c ='Portal' 
                                ORDER BY LastModifiedDate DESC LIMIT 1];
                    
                
                }
            }
        }
        if(lstContracts.isEmpty()){
            strError ='no_access';
            return strError ;
        }
        else if(!lstPackage.isEmpty()  &&( lstContracts[0].Status =='Grace' ||(lstContracts[0].Status =='Activated' &&  system.now() < lstContracts[0].EndDate.addDays(30)))){
            //system.debug('1st condition');
            strError ='unableFaq_casecreate';
            return strError ;
        }
        else if(lstPackage.isEmpty()  &&( lstContracts[0].Status =='Grace' ||(lstContracts[0].Status =='Activated' &&  system.now()< lstContracts[0].EndDate.addDays(30)))){
              //system.debug('2 nd condition');
              strError = 'unablefaq';
             return strError ;
        }
        else {
           strError ='no_access';
         }
        return strError ;
        //return 'no_access';  
        
    }
 }