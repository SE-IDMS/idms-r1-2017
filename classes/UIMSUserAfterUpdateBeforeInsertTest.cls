// test class for UIMSUserAfterUpdateBeforeInsert trigger
@isTest
public class UIMSUserAfterUpdateBeforeInsertTest{
    // test method for success case1
    static testmethod void testMethodOne() { 
        
        
        UIMS_User__c uimsUser = new UIMS_User__c() ; 
        uimsUser.isConvertedToIdmsUser__c = FALSE ; 
        
        uimsUser.FederatedID__c = '123456';
        uimsUser.EMail__c = 'testaccenture@accenture.com';
        insert uimsUser ;
        
        UIMS_User__c uimsSelUser = [Select isConvertedToIdmsUser__c from UIMS_User__c where id =: uimsUser.id];
        uimsSelUser.isConvertedToIdmsUser__c = FALSE ;
        update uimsSelUser; 
        
    }
    // test method for success case2
    static testmethod void testmethodtwo(){
        UIMS_User__c uimsUser = new UIMS_User__c() ; 
        uimsUser.isConvertedToIdmsUser__c = FALSE ; 
        uimsUser.EMail__c = 'testaccent1@accenture.com';
        uimsUser.FederatedID__c = '12345634';
        uimsUser.PhoneId__c  = '9000090000';
        insert uimsUser ;
        
        UIMS_User__c uimsSelUser = [Select isConvertedToIdmsUser__c from UIMS_User__c where id =: uimsUser.id];
        uimsSelUser.isConvertedToIdmsUser__c = FALSE ;
        update uimsSelUser; 
        
    }
}