/**
* This is test class for IDMSRequestPasswordReset class. 
**/

@isTest
public class IDMSPasswordResetTest{ 
    //This method tests reset password for email identity users. 
    static testMethod void resetPasswordEmail(){
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Heroku',AppName__c='Heroku',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        
        String env43 = System.Label.CLQ316IDMS043 ; 
        String env44 = System.Label.CLQ316IDMS044 ;
        
        User UserObjEmail = new User(alias = 'userpass', email='userpass' + '@accenture.com', 
                                     emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                     localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                     BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                     timezonesidkey='Europe/London', username='userpass' + env44 + env43 ,Company_Postal_Code__c='12345',
                                     Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                     IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                     IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert UserObjEmail;
        PageReference pRef = Page.IDMSRequestPasswordReset;
        pRef.getParameters().put('app', 'Heroku');
        Test.setCurrentPage(pRef);       
        
        VFC_IDMSRequestPasswordReset resetPassword = new VFC_IDMSRequestPasswordReset();
        resetPassword.showCaptcha= false ; 
        resetPassword.emailAddress = '';
        resetPassword.phoneNumber = '' ;
        resetPassword.pwdresetSuccess = true; 
        resetPassword.hideAllFields = true; 
        resetPassword.passwordReset();
        resetPassword.redirectToApp = true; 
        resetPassword.suffixurl = 'sample';
        resetPassword.successPhoneMsg= true; 
        resetPassword.emailOrPhone = 'userpass@bridge-fo.com';
        resetPassword.passwordReset();
        resetPassword.loginRedirect();
        
        try{
            VFC_IDMSRequestPasswordReset.resetPasswordForCreatedUser(UserObjEmail.username,'idms');}
        catch(Exception e){}
        
        resetPassword.emailAddress = '';
        resetPassword.phoneNumber = '' ;
        resetPassword.pwdresetSuccess = true; 
        resetPassword.passwordReset();
        resetPassword.redirectToApp = true; 
        resetPassword.suffixurl = 'sample';
        resetPassword.emailOrPhone = 'userpas1@bridge-fo.com';
        resetPassword.showCaptcha = false ; 
        resetPassword.passwordReset();
        resetPassword.loginRedirect();
    } 
    
    
    //This method tests reset password for Phone identity users. 
    static testMethod void resetPasswordPhone(){
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Heroku',AppName__c='Heroku',context__c='work',
                                                                                       AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        String env43 = System.Label.CLQ316IDMS043 ; 
        String env44 = System.Label.CLQ316IDMS044 ;
        String sep2 = System.Label.CLSEP16IDMS2; 
        
        system.debug('moiblephone --> ' + env43  + '--' + env44 ) ;  
        
        User UserObjPhone = new User(alias = 'userpass', email='userpass' + '@accenture.com', 
                                     emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                     localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                     BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                     timezonesidkey='Europe/London', username='9986995000'+ sep2 ,Company_Postal_Code__c='12345',
                                     Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                     IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                     IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert UserObjPhone;
        
        
        system.debug('username--> ' + UserObjPhone.username ) ;
        PageReference pRef = Page.IDMSRequestPasswordReset;
        pRef.getParameters().put('app', 'Heroku');
        Test.setCurrentPage(pRef);       
        
        VFC_IDMSRequestPasswordReset resetPassword = new VFC_IDMSRequestPasswordReset(); 
        resetPassword.hideAllFields = true; 
        resetPassword.emailAddress = '';
        resetPassword.phoneNumber = '' ;
        resetPassword.pwdresetSuccess = true; 
        resetPassword.passwordReset();
        resetPassword.showCaptcha = false ; 
        resetPassword.emailOrPhone = '9986995000' ; 
        resetPassword.passwordReset();
        
        
    } 
    
    //This method tests set password for user. 
    static testmethod void setPassword() {
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Heroku',AppName__c='Heroku',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        
        User UserObj2 = new User(alias = 'userset', email='userset' + '@accenture.com', isActive = true,
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                 BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='userset' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995003',FederationIdentifier ='308-Test123',
                                 IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                 IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999' , 
                                 IDMSSignatureGenerationDate__c=system.now().date(),IDMSToken__c='test2367');
        
        insert UserObj2;  
        Profile p = [SELECT id, Name FROM Profile where name = 'System Administrator' ].get(0);
        User UserObjAdm = new User(alias = 'useradm', email='userAd' + '@accenture.com', isActive = true,
                                   emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                   localesidkey='en_US', profileid = p.id, BypassWF__c = true,BypassVR__c = true, 
                                   BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                   timezonesidkey='Europe/London', username='userAd' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                   Company_Phone_Number__c='9986995003', IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                   IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999' , 
                                   IDMSSignatureGenerationDate__c=system.now().date(),IDMSToken__c='test2367');
        
        insert UserObjAdm;  
        Test.startTest();
        PageReference pRef = Page.IDMSSetPassword ;
        pRef.getParameters().put('id',UserObj2.id);
        pRef.getParameters().put('action','SetUserPwd');
        pRef.getParameters().put('sig','test2367');
        pRef.getParameters().put('App','Heroku');
        Test.setCurrentPage(pRef); 
        System.runAs(UserObjAdm){
            try{ 
                system.setpassword(UserObj2.id,'Welcome01');    
                VFC_IDMSSetPassword setPassword1 = new VFC_IDMSSetPassword() ; 
                setPassword1.redirectToApp = false ;
                setPassword1.appUrl= '' ; 
                setPassword1.setpwdFlag = true ;
                setPassword1.resetpwdFlag = true; 
                setPassword1.idmsOperation = 'idms' ;
                setPassword1.errorMessage = false ;
                setPassword1.ErrorMsgCode  = '';
                setPassword1.PageErrorMsg = '';
                setPassword1.confirmPassword= 'Welcome02';
                setPassword1.password = 'Welcome02';
                setPassword1.setPassword() ; 
            }catch(Exception e){}}
        
        
        User UserObj3 = [select id ,name from user where id =: UserObj2.id] ;
        
        System.runAs(UserObjAdm){
            try{
                PageReference pRef1 = Page.IDMSSetPassword ;
                pRef1.getParameters().put('id',UserObj3.id);
                pRef1.getParameters().put('action','SetUserPwd');
                pRef1.getParameters().put('sig','test2367');
                pRef1.getParameters().put('App','Heroku');
                Test.setCurrentPage(pRef1); 
                VFC_IDMSSetPassword setPassword2 = new VFC_IDMSSetPassword() ; 
                setPassword2.password = 'Welcome#1234';
                setPassword2.confirmPassword = 'Welcome#1234';
                setPassword2.setPassword();
            }catch(exception e){}
        }
        Test.stopTest();
        
        System.runAs(UserObjAdm){
            try{
                //system.setpassword(UserObj2.id,'Welcome#123');  
                VFC_IDMSSetPassword setPassword5 = new VFC_IDMSSetPassword() ; 
                setPassword5.Password = 'Welcome01';
                setPassword5.confirmPassword = 'Welcome01'; 
                setPassword5.setPassword() ;   
                
            }catch(exception e){}
        }
        
        System.runAs(UserObjAdm){
            try{
                VFC_IDMSSetPassword setPassword2 = new VFC_IDMSSetPassword() ; 
                setPassword2.redirectToApp = false ;
                setPassword2.appUrl= '' ; 
                setPassword2.setpwdFlag = true ;
                setPassword2.resetpwdFlag = true; 
                setPassword2.idmsOperation = 'idms' ;
                setPassword2.setPassword() ; 
            }catch(Exception e){}
        }
        System.runAs(UserObjAdm){
            try{
                VFC_IDMSSetPassword setPassword3 = new VFC_IDMSSetPassword() ; 
                setPassword3.Password = 'Welcome#1234';
                setPassword3.confirmPassword = 'Welcome#456';
                setPassword3.registrationSource = 'idms' ; 
                setPassword3.signature = IDMSEmailEncryption.EmailEncryption(UserObj2) ; 
                setPassword3.setPassword() ; 
            }catch(Exception e){}
        }
        System.runAs(UserObjAdm){
            try{
                
                VFC_IDMSSetPassword setPassword4 = new VFC_IDMSSetPassword() ; 
                setPassword4.Password = 'Welcome#1234';
                setPassword4.confirmPassword = '';
                setPassword4.registrationSource = 'idms' ; 
                setPassword4.signature = IDMSEmailEncryption.EmailEncryption(UserObj2) ; 
                setPassword4.setPassword() ; 
            }catch(Exception e){}
        }
        System.runAs(UserObjAdm){
            try{
                IDMSApplicationMapping__c iDMSApplicationMapping12= new IDMSApplicationMapping__c(Name='Heroku1',AppName__c='Heroku1',context__c='work',
                                                                                                  UrlRedirectAfterProfileUpdate__c='',AppStartURL__c='http://www.google.com');
                insert iDMSApplicationMapping12;
                PageReference pRef2 = Page.IDMSSetPassword ;
                pRef2.getParameters().put('id',UserObj3.id);
                pRef2.getParameters().put('action','SetUserPwd');
                pRef2.getParameters().put('sig','test2367');
                pRef2.getParameters().put('App','Heroku1');
                Test.setCurrentPage(pRef2); 
                VFC_IDMSSetPassword setPassword5 = new VFC_IDMSSetPassword() ; 
                setPassword5.Password = 'Welcome002';
                setPassword5.confirmPassword = 'Welcome002';
                setPassword5.setPassword() ; 
            }catch(Exception e){}
        }
        
        VFC_IDMSSetPassword setPassword6 = new VFC_IDMSSetPassword() ;  
        setPassword6.loginRedirect() ;
        
        
        
    } 
    //This method tests set password for user.
    static testMethod void setPassword2(){
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Heroku',AppName__c='Heroku',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        
        User UserObj22 = new User(alias = 'userset2', email='userset2' + '@accenture.com', isActive = true,
                                  emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                  localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                  BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                  timezonesidkey='Europe/London', username='userset2' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                  Company_Phone_Number__c='9986995003',FederationIdentifier ='308-Test123',
                                  IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                  IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMSSignatureGenerationDate__c=system.today(),IDMSToken__c='test2367'
                                 );
        
        insert UserObj22;   
        try{
            System.runAs(UserObj22){
                PageReference pRef = Page.IDMSSetPassword ;
                pRef.getParameters().put('id',UserObj22.id);
                pRef.getParameters().put('action','SetUserPwd');
                pRef.getParameters().put('sig','test2367');
                pRef.getParameters().put('App','Heroku');
                
                VFC_IDMSSetPassword setPasswordOne = new VFC_IDMSSetPassword(); 
                setPasswordOne.password = 'Welcome01';
                setPasswordOne.confirmPassword = 'Welcome01';
                setPasswordOne.setPassword();
                
            }}catch(exception e){}
        
        
    }
    static testMethod void setPassword3(){
        try{
            
            VFC_IDMSSetPassword setPasswordOne = new VFC_IDMSSetPassword(); 
            setPasswordOne.setPassword();
            
        }catch(exception e){}
    }
    
    //This method tests confirm pin received by user.
    static testMethod void confirmPin(){
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Heroku',AppName__c='Heroku',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        
        User UserObj3 = new User(alias = 'usercofn', email='usercofn' + '@accenture.com', 
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                 BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='usercofn' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                 IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                 IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMSToken__c='1234');
        
        insert UserObj3;
        
        PageReference pRef = Page.IDMSConfirmPin ;
        pRef.getParameters().put('app', 'Heroku');
        pRef.getParameters().put('id',userObj3.id);
        pRef.getParameters().put('key','SetUserPwd');
        Test.setCurrentPage(pRef);       
        
        Test.startTest();
        VFC_ConfirmPin confirmPinVer = new VFC_ConfirmPin() ; 
        confirmPinVer.confirmPin = null ;  
        confirmPinVer.incorrectAppId = false ; 
        confirmPinVer.redirectToApp = true; 
        confirmPinVer.confirmPin ='1234567';
        confirmPinVer.errorPageMessage = '' ;
        confirmPinVer.confirmKey() ;
        
        confirmPinVer.confirmPin = null ; 
        confirmPinVer.confirmKey() ; 
        
        confirmPinVer.idmsOperation = 'SetUserPwd';
        confirmPinVer.password = true; 
        confirmPinVer.idmsPassword = '' ;
        confirmPinVer.cnfpassword = '' ;
        confirmPinVer.confirmPin = '123456' ;
        confirmPinVer.confirmKey() ;
        system.debug('User Verification Code1 --> ' + UserObj3.IDMSPinVerification__c) ;
        
        
        confirmPinVer.idmsOperation = 'SetUserPwd';
        confirmPinVer.password = true; 
        confirmPinVer.idmsPassword = 'Welcom#123' ;
        confirmPinVer.cnfpassword = 'Welcome#123' ;
        confirmPinVer.confirmPin = '123456' ;
        confirmPinVer.confirmKey() ;
        system.debug('User Verification Code1 --> ' + UserObj3.IDMSPinVerification__c) ; 
        
        confirmPinVer.idmsOperation = 'SetUserPwd';
        confirmPinVer.password = true; 
        confirmPinVer.idmsPassword = 'Welcome#123' ;
        confirmPinVer.cnfpassword = 'Welcome#123' ;
        confirmPinVer.confirmPin = '123456' ;
        confirmPinVer.confirmKey() ;
        system.debug('User Verification Code1 --> ' + UserObj3.IDMSPinVerification__c) ; 
        
        
        confirmPinVer.idmsOperation = 'UpdateUserRecord';
        confirmPinVer.password = true; 
        confirmPinVer.idmsPassword = 'Welcome#123' ;
        confirmPinVer.cnfpassword = 'Welcome#123' ;
        confirmPinVer.confirmPin = '123456' ;
        confirmPinVer.confirmKey() ;
        system.debug('User Verification Code1 --> ' + UserObj3.IDMSPinVerification__c) ; 
        
        
        confirmPinVer.idmsOperation = 'ActiveUser';
        confirmPinVer.password = true; 
        confirmPinVer.idmsPassword = 'Welcom#123' ;
        confirmPinVer.confirmPin = '123456' ;
        confirmPinVer.confirmKey() ;
        system.debug('User Verification Code2 --> ' + UserObj3.IDMSPinVerification__c) ;         
        
        confirmPinVer.resendPin();
        Test.stopTest();
    } 
    
    //This method tests confirm pin received by user. 
    static testmethod void confirmPin1(){
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Heroku',AppName__c='Heroku',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        User UserObjConfirm = new User(alias = 'usercf1', email='usercf1' + '@accenture.com', 
                                       emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                       localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                       BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                       timezonesidkey='Europe/London', username='usercf1' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                       Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                       IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                       IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMSToken__c='1234');
        
        insert UserObjConfirm ;
        system.debug('verification code--> ' + UserObjConfirm.IDMSPinVerification__c); 
        
        PageReference pRef = Page.IDMSConfirmPin ;
        pRef.getParameters().put('app', 'Heroku');
        pRef.getParameters().put('id',UserObjConfirm.id);
        pRef.getParameters().put('key','UpdateUserRecord');
        Test.setCurrentPage(pRef);       
        
        Test.startTest();
        VFC_ConfirmPin confirmPinVer = new VFC_ConfirmPin() ; 
        confirmPinVer.confirmPin= '123456';
        confirmPinVer.idmsPassword = 'Welcome#123';
        confirmPinVer.confirmKey();
        Test.stopTest(); 
        
    }
    //This method tests update password for user.
    static testmethod void updatePassword1(){
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Heroku',AppName__c='Heroku',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        
        User UserObj4 = new User(alias = 'userup', email='userup' + '@accenture.com', isActive = true , 
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                 BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='userup' + '@bridge-fo.com',Company_Postal_Code__c='12345',
                                 Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                 IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                 IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMSToken__c='1234');
        
        insert UserObj4;
        System.setPassword(userObj4.id,'Welcome#123') ;
        Test.startTest();
        
        PageReference pRef = Page.IDMSPasswordUpdate ;
        pRef.getParameters().put('app', 'Heroku');
        Test.setCurrentPage(pRef);       
        
        try{
            System.runAs(UserObj4){
                VFC_IdmsPasswordUpdate updatepwd = new VFC_IdmsPasswordUpdate(); 
                updatepwd.redirectToApp = true; 
                updatepwd.oldPassword = 'Welcome#123' ;
                updatepwd.newPassword = 'Welcome#123';
                updatepwd.verifyNewPassword = 'Welcome#123' ;
                updatepwd.registrationSource = 'idms' ;
                updatepwd.updatePassword() ; 
            }
            
            System.runAs(UserObj4){
                VFC_IdmsPasswordUpdate updatepwd1 = new VFC_IdmsPasswordUpdate(); 
                updatepwd1.redirectToApp = true; 
                updatepwd1.oldPassword = 'Welcome#123' ;
                updatepwd1.newPassword = 'Welcome#1234';
                updatepwd1.verifyNewPassword = 'Welcome#1234' ;
                updatepwd1.registrationSource = 'UIMS' ;
                updatepwd1.updatePassword() ; 
                updatepwd1.profileRedirect();
            }
            
        }catch(exception e){} 
        Test.stopTest();
    }
    //This method tests update password for user. 
    static testmethod void updatepassword2(){
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Heroku',AppName__c='Heroku',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        
        VFC_IdmsPasswordUpdate updatepwd2 = new VFC_IdmsPasswordUpdate(); 
        updatepwd2.redirectToApp = true; 
        updatepwd2.oldPassword = '' ;
        updatepwd2.newPassword = '';
        updatepwd2.verifyNewPassword = '' ;
        updatepwd2.registrationSource = '' ;
        updatepwd2.profileRedirect(); 
        updatepwd2.updatePassword() ; 
        updatepwd2.getAppUrl();
        
        
    }
    
    //This method tests update password for user with required fields. 
    static testmethod void updatepassword33(){
        
        IDMSApplicationMapping__c iDMSApplicationMapping= new IDMSApplicationMapping__c(Name='Heroku',AppName__c='Heroku',context__c='work',
                                                                                        AppStartURL__c='http://www.google.com');
        insert iDMSApplicationMapping;
        
        VFC_IdmsPasswordUpdate updatepwd2 = new VFC_IdmsPasswordUpdate(); 
        Test.starttest(); 
        updatepwd2.oldPassword='Welcome#123';
        updatepwd2.newPassword = '';
        updatepwd2.appUrl= '';
        updatepwd2.redirectionId= '';
        updatepwd2.sessionURL= '';
        updatepwd2.updatePassword() ;
        
        updatepwd2.oldPassword='Welcome#123';
        updatepwd2.newPassword = 'Welcome#123';
        updatepwd2.updatePassword();
        
        updatepwd2.oldPassword='Welcome#123';
        updatepwd2.newPassword = 'Welcome#1235';
        updatepwd2.verifyNewPassword= '';
        updatepwd2.updatePassword() ; 
        
        updatepwd2.oldPassword='Welcome#123';
        updatepwd2.newPassword = 'Welcome#1235';
        updatepwd2.verifyNewPassword= 'Welcome#12352';
        updatepwd2.registrationSource='';
        updatepwd2.updatePassword() ;
        
        updatepwd2.oldPassword='';
        updatepwd2.newPassword = 'Welcome#1235';
        updatepwd2.verifyNewPassword= 'Welcome#12352';
        updatepwd2.registrationSource='';
        updatepwd2.updatePassword() ;
        
        updatepwd2.oldPassword='Welcome#123';
        updatepwd2.newPassword = 'Welcome#1235';
        updatepwd2.verifyNewPassword= 'Welcome#123';
        updatepwd2.registrationSource='';
        updatepwd2.updatePassword() ;
        
        updatepwd2.oldPassword='Welcome#123';
        updatepwd2.newPassword = 'Welcome#1235';
        updatepwd2.verifyNewPassword= 'Welcome#1235';
        updatepwd2.registrationSource='Welcome#12351';
        updatepwd2.updatePassword() ;
        updatepwd2.getAppUrl();
        updatepwd2.profileRedirect(); 
        Test.stoptest();
        
    } 
    
}