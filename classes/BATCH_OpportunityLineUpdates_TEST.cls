/*
    Description: Test Class for BATCH_OpportunityLineUpdates 
*/
@isTest
private class BATCH_OpportunityLineUpdates_TEST
{
    static testMethod void testOppLineUpdates()
    {
        List<OPP_ProductLine__c> prodLines = new List<OPP_ProductLine__c>();
        
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        
        Opportunity opp2 = Utils_TestMethods.createOpportunity(acc.Id);
        opp2.TECH_IsOpportunityUpdate__c  = true;
        opp2.Reason__c = 'test';
        insert opp2;

        List<CS_ProductLineBU__c> prodLinesCS = new List<CS_ProductLineBU__c>();
        CS_ProductLineBU__c prodl = new CS_ProductLineBU__c();
        prodl.Name = 'Test1';
        prodl.ProductBU__c = 'T1';
        prodLinesCS.add(prodl);        
        
        CS_ProductLineBU__c prod2 = new CS_ProductLineBU__c();
        prod2.Name = 'Test2';
        prod2.ProductBU__c = 'T2';
        prodLinesCS.add(prod2);
        
        insert prodLinesCS;
       
        //Inserts Opportunity Lines
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            pl1.ProductBU__c = 'Test1';
            pl1.ProductLine__c = 'IDOL2';
            prodLines.add(pl1);
        }
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            pl1.ProductBU__c = 'Test2';
            pl1.ProductLine__c = 'TestL2';
            prodLines.add(pl1);
        }
        for(Integer i=0;i<2;i++)
        {
            OPP_ProductLine__c pl1 = Utils_TestMethods.createProductLine(opp2.Id);
            pl1.Quantity__c = 10;
            pl1.ProductBU__c = 'Test2';
            pl1.ProductLine__c = 'IDOL3';
            prodLines.add(pl1);
        }
        Database.SaveResult[] lsr = Database.Insert(prodLines);
        BATCH_OpportunityLineUpdates oppLne = new BATCH_OpportunityLineUpdates(); 
        database.executebatch(oppLne,10000);
    }
}