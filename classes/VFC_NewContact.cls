// Modified : June 16 Release - Prepopulation of Correspondence Language field of Contact based on the country of its associated account
// Modified : For Project Back2Standard - Added standard address fields instead of Custom address field prepopulation

public class VFC_NewContact {
    public Id acc{get; set;}
    public Account a{get; set;} {a=new Account();}
    public VFC_NewContact(ApexPages.StandardController controller) {
        acc = ApexPages.currentPage().getParameters().get('accid');
        if(acc!=null)
            a=[Select Street__c,City__c,StreetLocalLang__c,POBox__c,POBoxZip__c,ZipCode__c,Country__c,Country__r.Name,StateProvince__r.Name,LocalCity__c,County__c,LocalCounty__c,StateProvince__r.Id,Name,Id,Pagent__r.ID,Pagent__r.Name,PBAAgent__r.Id,PBAAgent__r.Name, BillingStreet, BillingCity, BillingPostalCode, BillingStateCode, BillingCountryCode  from Account where id=:acc];
    }
    
    public Pagereference redirectToContact()
    {
        Pagereference pageRef;
        if(acc!=null)
        {
            String accStreet=''; 
            String accCity='';
            String accStateProvince='';
            String accStreetLocal='';
            String accPOBox='';
            String accPOBoxZip='';  
            String accZipCode='';
            String accLocalCity='';
            String accCounty='';
            String accLocalCounty='';
            String accName='';
            String accPagent='';
            String accPBAAgent='';
            String conCorrespLang='';
            String accCountry='';
            
            /*if(a.Street__c!=null && a.Street__c!='' )
                    accStreet=EncodingUtil.urlEncode(a.Street__c,'UTF-8');
            if(a.City__c!=null && a.City__c!='' )
                    accCity=EncodingUtil.urlEncode(a.City__c,'UTF-8');
            if(a.StateProvince__r.Name!=null && a.StateProvince__r.Name!='' )
                    accStateProvince=EncodingUtil.urlEncode(a.StateProvince__r.Name,'UTF-8');
            if(a.ZipCode__c!=null && a.ZipCode__c!='' )
                    accZipCode=EncodingUtil.urlEncode(a.ZipCode__c,'UTF-8');*/
                    
            if(a.BillingStreet!=null && a.BillingStreet!='' )
                    accStreet=EncodingUtil.urlEncode(a.BillingStreet,'UTF-8');
            if(a.BillingCity!=null && a.BillingCity!='' )
                    accCity=EncodingUtil.urlEncode(a.BillingCity,'UTF-8');
            if(a.BillingStateCode!=null && a.BillingStateCode!='' )
                    accStateProvince=EncodingUtil.urlEncode(a.BillingStateCode,'UTF-8');
            if(a.BillingPostalCode!=null && a.BillingPostalCode!='' )
                    accZipCode=EncodingUtil.urlEncode(a.BillingPostalCode,'UTF-8');
            if(a.BillingCountryCode!=null && a.BillingCountryCode!='' ){
                    accCountry=EncodingUtil.urlEncode(a.BillingCountryCode,'UTF-8');
                    conCorrespLang=EncodingUtil.urlEncode([SELECT CorrespLang__c FROM Country__c WHERE CountryCode__c=:a.BillingCountryCode].CorrespLang__c,'UTF-8');                    
            }
            
                    
            if(a.StreetLocalLang__c!=null && a.StreetLocalLang__c!='' )
                    accStreetLocal=EncodingUtil.urlEncode(a.StreetLocalLang__c,'UTF-8');
            if(a.POBox__c!=null && a.POBox__c!='' )
                    accPOBox=EncodingUtil.urlEncode(a.POBox__c,'UTF-8');
            if(a.POBoxZip__c!=null && a.POBoxZip__c!='' )
                    accPOBoxZip=EncodingUtil.urlEncode(a.POBoxZip__c,'UTF-8');
            if(a.LocalCity__c!=null && a.LocalCity__c!='' )
                    accLocalCity=EncodingUtil.urlEncode(a.LocalCity__c,'UTF-8');
            if(a.County__c!=null && a.County__c!='' )
                    accCounty=EncodingUtil.urlEncode(a.County__c,'UTF-8');
            if(a.LocalCounty__c!=null && a.LocalCounty__c!='' )
                    accLocalCounty=EncodingUtil.urlEncode(a.LocalCounty__c,'UTF-8');
            if(a.Name!=null && a.Name!='' )
                    accName=EncodingUtil.urlEncode(a.Name,'UTF-8');
            if(a.Pagent__r.Name!=null && a.Pagent__r.Name!='' )
                    accPagent=EncodingUtil.urlEncode(a.Pagent__r.Name,'UTF-8');
            if(a.PBAAgent__r.Name!=null && a.PBAAgent__r.Name!='' )
                    accPBAAgent=EncodingUtil.urlEncode(a.PBAAgent__r.Name,'UTF-8');                    
        
            //pageRef = new Pagereference('/003/e?nooverride=1&00NA00000054koT='+accStreet+'&00NA00000054koC='+accCity+'&00NA00000054koS='+accStreetLocal+'&00NA00000054koP='+accPOBox+'&00NA00000054koO='+accPOBoxZip+'&00NA00000054koY='+accZipCode+'&CF00NA00000054koG='+a.Country__r.Name+'&CF00NA00000054koG_lkid='+a.Country__c+'&CF00NA00000054koR='+accStateProvince+'&00NA0000005mnKF='+accLocalCity+'&00NA0000005mnac='+accCounty+'&00NA0000005mnah='+accLocalCounty+'&con4='+accName+'&con4_lkid='+a.Id+'&CF00NA0000008za9I='+accPagent+'&CF00NA0000008za9H='+accPBAAgent+'&00NA00000054koF='+conCorrespLang);
            pageRef = new Pagereference('/003/e?nooverride=1&con18street='+accStreet+'&con18city='+accCity+'&00NA00000054koS='+accStreetLocal+'&00NA00000054koP='+accPOBox+'&00NA00000054koO='+accPOBoxZip+'&con18zip='+accZipCode+'&con18country='+accCountry+'&con18state='+accStateProvince+'&00NA0000005mnKF='+accLocalCity+'&00NA0000005mnac='+accCounty+'&00NA0000005mnah='+accLocalCounty+'&con4='+accName+'&con4_lkid='+a.Id+'&CF00NA0000008za9I='+accPagent+'&CF00NA0000008za9H='+accPBAAgent+'&00NA00000054koF='+conCorrespLang);
        }
        else if(acc==null)
        {
            pageRef = new Pagereference('/003/e?nooverride=1&retURL=%2F003%2Fo');
        }
        return pageRef;
    }

}