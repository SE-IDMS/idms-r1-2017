global class Batch_ReferentialUpdateFromVFP implements Database.Batchable<sObject>{

   public Boolean assignNull=false;
    
   global Database.QueryLocator start(Database.BatchableContext BC){
      System.debug('start Called');
        System.debug('queryLocatorQuery:'+queryLocatorQuery);
        return Database.getQueryLocator( queryLocatorQuery );
   }
   
   public class BatchException extends Exception {}
   
   Boolean reRunErrorRecordsOnly;
   String sObjectName;
   String queryLocatorQuery;
   
   public Batch_ReferentialUpdateFromVFP(Boolean reRunErrorRecordsOnly, String sObjectName) {
    //System.debug('Constructor Called');
    this.reRunErrorRecordsOnly=reRunErrorRecordsOnly;
    this.sObjectName=sObjectName;
    queryLocatorQuery=getQueryFor(reRunErrorRecordsOnly, sObjectName);
        if (queryLocatorQuery=='No Records') {
            throw new BatchException(Label.CLDEC14REF05);
        }
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<sObject> currList=new List<sObject>();
        Schema.SObjectType currObjType=null;
        for (sObject currObj:scope){
            if (currObj!=null){
                if (currObjType==null) {
                    currObjType=currObj.getSObjectType();
                }
                if (currObj.getSObjectType()==currObjType) {
                    currList.add(currObj);
                } else {
                    doUpdateFor(currList, currObjType.getDescribe().getName());
                    currList=new List<sObject>();
                    currList.add(currObj);
                    currObjType=currObj.getSObjectType();
                }
            }
        }
            if (currList!=null && currList.size()>0){
                doUpdateFor(currList, currObjType.getDescribe().getName());
            }
    }
    
    public void doUpdateFor(List<sObject> toRecs, String toObj){ //toObjName is Object API Name taken from records.
    
        List<String> toWhereFields=new List<String>();
        /*
        System.debug('refMap:'+refMap);
        System.debug('toObj:'+toObj);
        System.debug('mapContainer.refMap.get(toObj):'+refMap.get(toObj));
        System.debug('mapContainer.refMap.get(toObj).keySet():'+refMap.get(toObj).keySet());
        */
        
        for (String fromObj:refMap.get(toObj).keySet()){
            List<ReferentialDataMapping__c> refDataMapList=refMap.get(toObj).get(fromObj);
                    for (ReferentialDataMapping__c currRefDataRec: refDataMapList){
                        String whereTo=currRefDataRec.WhereClauseToField__c;
                        toWhereFields.add(whereTo);
                    }
        }
        
        Map<String, String> toRecINstrMap=new Map<String, String>();
        Map<String,Set<String>> alreadyDone=new Map<String,Set<String>>();
        for (sObject currToObj: toRecs) {
            for (String currWhereField: toWhereFields){
                if ( !toRecINstrMap.keySet().contains(currWhereField) ){
                    toRecINstrMap.put(currWhereField, '');
                    alreadyDone.put(currWhereField, new Set<String>());
                }
                if ( !alreadyDone.get(currWhereField).contains( String.valueOf(currToObj.get(currWhereField)) ) ){
                    toRecINstrMap.put(currWhereField, toRecINstrMap.get(currWhereField) + '\''+ String.escapeSingleQuotes( String.valueOf(currToObj.get(currWhereField)) )  +'\', ');
                    alreadyDone.get(currWhereField).add(String.valueOf(currToObj.get(currWhereField)));
                }
            }
        }
        
        
        Map<id, sObject> finalUpdates= new Map<id, sObject>();
        //To Object -> From Objects ->  List of Records of Referential Data Mapping
        for (String fromObj:refMap.get(toObj).keySet()){
                List<ReferentialDataMapping__c> currRefDataListOfToFromObj = refMap.get(toObj).get(fromObj);

                String currentQuery='select ';
                Set<String> fieldSelectSet=new Set<String>(); //To Avoid duplicates.

                Set<String> fieldWhereSet=new Set<String>(); //To Avoid duplicates.
                String currentWhere=' where ';

                for (ReferentialDataMapping__c currRefRec: currRefDataListOfToFromObj){
                    String currField=currRefRec.FromField__c;
                    if ( !fieldSelectSet.contains(currField) ){
                        currentQuery=currentQuery+currField+', ';
                        fieldSelectSet.add(currField);
                    }
                    String currWhereFrom=currRefRec.WhereClauseFromField__c;
                    if ( !fieldSelectSet.contains(currWhereFrom) ){
                        currentQuery=currentQuery+currWhereFrom+', ';
                        fieldSelectSet.add(currWhereFrom);
                    }
                    if ( !fieldWhereSet.contains(currWhereFrom) ){
                            String currWhereTo=currRefRec.WhereClauseToField__c;
                            currentWhere=currentWhere+currWhereFrom+' IN (' +toRecINstrMap.get(currWhereTo).substring(0, toRecINstrMap.get(currWhereTo).length()-2)+') OR '; //'!=NULL AND '; 
                        fieldWhereSet.add(currWhereFrom);
                    }
//String.escapeSingleQuotes( 
//.replace('\\','\\\\')
                }
                
                if (currentQuery.length()>2){
                    currentQuery=currentQuery.substring(0, currentQuery.length()-', '.length()); //-2
                    currentQuery=currentQuery+' FROM '+fromObj;
                    if (currentWhere.length()>5){
                        currentWhere=currentWhere.substring(0, currentWhere.length()-' OR '.length()); //-4
                        currentQuery=currentQuery+currentWhere;
                    }
                }
                
                System.debug('currentQuery: '+currentQuery);
            List<sobject> fromRecs=database.query(currentQuery);
            
            //Code to iterate and update records:
            for (sObject currToObj: toRecs) {
                for (sObject currFromObj:fromRecs) {
                    for (ReferentialDataMapping__c currRefRec: currRefDataListOfToFromObj){
                                String whereFrom=currRefRec.WhereClauseFromField__c;
                                String whereTo=currRefRec.WhereClauseToField__c;
                                sObject whereFromObj;
                                Object whereFromVal;
                                if (whereFrom.contains('.')){
                                    whereFromObj=getLastSobj(currFromObj, whereFrom);
                                    if (whereFromObj!=null){
                                        whereFromVal=whereFromObj.get( getFieldOf(whereFrom) );
                                    }
                                } else {
                                    whereFromObj=currFromObj;
                                    whereFromVal=currFromObj.get(whereFrom);
                                }
                                
                                sObject whereToObj;
                                Object whereToVal;
                                if (whereTo.contains('.')){
                                    whereToObj=getLastSobj(currToObj, whereTo);
                                    if (whereToObj!=null){
                                        whereToVal=whereToObj.get( getFieldOf(whereTo) );
                                    }
                                } else {
                                    whereToObj=currToObj;
                                    whereToVal=currToObj.get(whereTo);
                                }

                                if ( whereFromVal!=null && whereFromObj!=null && whereFromVal==whereToVal ){
                                    
                                        String fromField=currRefRec.FromField__c;
                                        String toField=currRefRec.ToField__c;
                                        String toFieldToUpdate;
                                            sObject toObjToUpdate;
                                            if (toField.contains('.')){
                                                toObjToUpdate = getLastSobj(currToObj, toField);
                                                if (toObjToUpdate!=null){
                                                    toFieldToUpdate=getFieldOf(toField);
                                                    //toObjToUpdateVal = toObjToUpdate.get(toFieldToUpdate); //will NOT be used as we are as we are not concerned about what value we are going to replace
                                                }
                                            } else {
                                                toObjToUpdate=currToObj;
                                                toFieldToUpdate=toField;
                                                //toObjToUpdateVal=toObjToUpdate.get(toFieldToUpdate); //will NOT be used as we are as we are not concerned about what value we are going to replace
                                            }
                                            
                                            if ( toObjToUpdate!=null){
                                                if ( finalUpdates.keySet().contains(toObjToUpdate.id) ){
                                                    toObjToUpdate=finalUpdates.get(toObjToUpdate.id);
                                                }
                                                    //sObject fromObject=getLastSobj(currFromObj, fromField);
                                                    sObject fromObject;
                                                    Object fromObjectVal;
                                                    if (fromField.contains('.')){
                                                        fromObject = getLastSobj(currFromObj, fromField);
                                                        if (fromObject!=null){
                                                            fromObjectVal = fromObject.get( getFieldOf(fromField) );
                                                        }
                                                    } else {
                                                        fromObject=currFromObj;
                                                        fromObjectVal=fromObject.get(fromField);
                                                    }

                                                    if (fromObject!=null && fromObjectVal!=null){
                                                        toObjToUpdate.put( toFieldToUpdate, fromObjectVal ); //currToObj.toField=currFromObj.fromField
                                                        finalUpdates.put(toObjToUpdate.id, toObjToUpdate);
                                                    } else if (assignNull){
                                                        toObjToUpdate.put( toFieldToUpdate, null ); //currToObj.toField=currFromObj.fromField
                                                        finalUpdates.put(toObjToUpdate.id, toObjToUpdate);
                                                        //currToObjUpdated=true;
                                                    }
                                                    /*
                                                    System.debug('whereTo:'+whereTo);
                                                    System.debug('whereFrom:'+whereFrom);
                                                    System.debug('To Field:'+toField);
                                                    System.debug('From Field:'+fromField);
                                                    System.debug('Going to add:'+currToObj);
                                                    */
                                            }
                                }
                    }
                }
            }
        }

        List<sObject> toUpdateList = finalUpdates.values();
        Database.SaveResult[] saveRes = database.update(toUpdateList, false);
        
        List<ReferentialDataError__c> toInsertErrors=new List<ReferentialDataError__c>();
        for (Integer i=0; i<saveRes.size(); i++){
            Database.SaveResult sr=saveRes.get(i);
            if ( !sr.isSuccess() ) {
                for(Database.Error err : sr.getErrors()) {
                    ReferentialDataError__c currErr=new ReferentialDataError__c();
                    currErr.StatusCode__c=err.getStatusCode().name();
                    currErr.IDofToObject__c=String.valueOf(toUpdateList.get(i).get('id'));
                    currErr.ErrorMessage__c=err.getMessage();
                    currErr.ErrorInFields__c=String.valueOf(err.getFields()); //Check if this is converting correct.
                    
                    //To be changed
                    currErr.ObjectName__c=toUpdateList.get(i).getSObjectType().getDescribe().getName(); 
                    
                    toInsertErrors.add(currErr);
                    /*
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                    */
                }           
            }
        }
        
        database.insert(toInsertErrors,false);
        
    }
    
    public static sObject getLastSobj(sObject sObj, String field){
        String[] fieldParsed=field.split('\\.');
        for (Integer i=0; i<(fieldParsed.size()-1); i++) {
            String currPart=fieldParsed[i];
            sObj=sObj.getSObject(currPart);
            if (sObj==null) return null;
        }
        return sObj;
    
    }
    
    public static String getFieldOf(String field){
        //System.debug('field to Parse:'+field);
        String[] fieldParsed=field.split('\\.');
        //System.debug('fieldParsed:'+fieldParsed);
        return fieldParsed[fieldParsed.size()-1];
    }
    
    
   global void finish(Database.BatchableContext BC){
   }
   
   
    public Map< String, Map<String, List<ReferentialDataMapping__c>> > refMap;
   
    public String getQueryFor(Boolean reRunErrorRecordsOnly, String toSObjectName) {
        
        refMap=new Map< String, Map<String, List<ReferentialDataMapping__c>> >();
        
        List<ReferentialDataMapping__c> refDataRecs = [select id, Active__c, FromObject__c, ToObject__c, ToField__c, FromField__c, WhereClauseFromField__c, WhereClauseToField__c from ReferentialDataMapping__c where Active__c=true AND ToObject__c=:toSObjectName];
        
        Map<String, Set<String>> toObjectFields= new Map<String, Set<String>>();
        Map<String, Set<String>> toObjectWhere= new Map<String, Set<String>>();
        
        for (ReferentialDataMapping__c refDataRec: refDataRecs){
                if (refDataRec.toObject__c!=null && refDataRec.fromObject__c!=null){
                    //For To Object
                    if( !refMap.keySet().contains(refDataRec.toObject__c) ){
                        refMap.put(refDataRec.toObject__c, new Map<String, List<ReferentialDataMapping__c>>());
                    }
                    //For From Object
                    if ( !refMap.get(refDataRec.toObject__c).keySet().contains(refDataRec.fromObject__c) ){
                            refMap.get(refDataRec.toObject__c).put(refDataRec.fromObject__c, new List<ReferentialDataMapping__c>());
                    }
                    //ReferentialDataMappingWrapper
                    List<ReferentialDataMapping__c> refDataMappingForToFromObject=refMap.get(refDataRec.toObject__c).get(refDataRec.fromObject__c);
                    refDataMappingForToFromObject.add(refDataRec);
                    //System.debug('refMap In getQueryFor():'+refMap);
                    if ( !toObjectFields.keySet().contains(refDataRec.toObject__c) ){
                        toObjectFields.put(refDataRec.toObject__c, new Set<String>());
                        toObjectWhere.put(refDataRec.toObject__c, new Set<String>());
                    }
                    toObjectFields.get(refDataRec.toObject__c).add(refDataRec.ToField__c);
                    if (refDataRec.WhereClauseToField__c!=null){
                        toObjectWhere.get(refDataRec.toObject__c).add(refDataRec.WhereClauseToField__c);
                    }
                }
        }
        
        //Get All Error Records and Store it in Map to Group By To Object Name
        
        Map<String, List<ReferentialDataError__c>> objectNameToErrorRecsMap=new Map<String, List<ReferentialDataError__c>>();
        if (reRunErrorRecordsOnly) {
            List<ReferentialDataError__c> prevErrorRecs=[select id, name, objectName__c, IDofToObject__c from ReferentialDataError__c where ReRun__c=true AND objectName__c=:toSObjectName];
            if (prevErrorRecs.isEmpty()) {
                return 'No Records';
            }
            for (ReferentialDataError__c currRec: prevErrorRecs) {
                if(!objectNameToErrorRecsMap.keySet().contains(currRec.objectName__c)){
                    objectNameToErrorRecsMap.put(currRec.objectName__c, new List<ReferentialDataError__c>());
                }
                objectNameToErrorRecsMap.get(currRec.objectName__c).add(currRec);
            }
        }
        
        String ObjName=toSObjectName; //Value will be same as currRec.objectName__c.
            String currentQuery='select id, ';
            
            Set<String> fieldSelectSet=new Set<String>();
            for (String currField: toObjectFields.get(ObjName)){
                    if ( !fieldSelectSet.contains(currField) ){
                        currentQuery=currentQuery+currField+', ';
                        fieldSelectSet.add(currField);
                    }
            }
            String currentWhere=' where ';
            for ( String currWhere: toObjectWhere.get(ObjName) ){
                if ( !fieldSelectSet.contains(currWhere) ){
                    currentQuery=currentQuery+currWhere+', ';
                    fieldSelectSet.add(currWhere);
                }
                currentWhere=currentWhere+currWhere+'!=NULL AND ';
            }
            if (currentQuery.length()>9){
                currentQuery=currentQuery.substring(0, currentQuery.length()-', '.length()); //-2
                currentQuery=currentQuery+' FROM '+ObjName;
                if (currentWhere.length()>7){
                    currentWhere=currentWhere.substring(0, currentWhere.length()-' AND '.length()); //-5
                    //In case of reRun error records only:
                    if (reRunErrorRecordsOnly) {
                        
                        for ( String currWhere: toObjectWhere.get(ObjName) ){
                            currentWhere=currentWhere+' AND id IN (';
                            for (ReferentialDataError__c currRec: objectNameToErrorRecsMap.get(ObjName)) {
                                currentWhere=currentWhere+'\''+currRec.IDofToObject__c+'\',';
                            }
                            currentWhere=currentWhere.substring(0, currentWhere.length()-1)+')';
                        }
                    }

                    currentQuery=currentQuery+currentWhere;
                }
                
            }
            return currentQuery;
   }

}