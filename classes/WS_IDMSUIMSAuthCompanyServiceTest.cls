/**
@Author: Prashanth GS
Description: This covers WS_IDMSUIMSAuthCompanyService and WS_IDMSUIMSAuthCompanyServiceImpl class
Release: Sept 2016
**/ 

@isTest
class WS_IDMSUIMSAuthCompanyServiceTest{
    //Test1 : Calling removePrimaryContact method from WS_IDMSUIMSAuthCompanyServiceImpl
    static testmethod void testremovePrimaryContact(){
        try{ 
            test.Starttest();
            WS_IDMSUIMSAuthCompanyServiceImpl wS_IDMSUIMSAuthCompanyServiceImpl=new WS_IDMSUIMSAuthCompanyServiceImpl();
            WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort companyManager_UIMSV2_ImplPort=new WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort();
            Boolean bolResponse=companyManager_UIMSV2_ImplPort.removePrimaryContact('Test123','Test1234');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test2 : Calling getCompany method from WS_IDMSUIMSAuthCompanyServiceImpl
    static testmethod void testgetCompany(){
        try{
            test.Starttest();
            WS_IDMSUIMSAuthCompanyServiceImpl wS_IDMSUIMSAuthCompanyServiceImpl=new WS_IDMSUIMSAuthCompanyServiceImpl();
            WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort companyManager_UIMSV2_ImplPort=new WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort();
            WS_IDMSUIMSAuthCompanyService.companyV3 resWS_IDMSUIMSAuthCompanyService=companyManager_UIMSV2_ImplPort.getCompany('Test123','Test1234','Test1235');
            
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test3 : Calling rejectCompanyMerge method from WS_IDMSUIMSAuthCompanyServiceImpl
    static testmethod void testrejectCompanyMerge(){
        try{
            test.Starttest();
            WS_IDMSUIMSAuthCompanyServiceImpl wS_IDMSUIMSAuthCompanyServiceImpl=new WS_IDMSUIMSAuthCompanyServiceImpl();
            WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort companyManager_UIMSV2_ImplPort=new WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort();
            Boolean bolResponse1=companyManager_UIMSV2_ImplPort.rejectCompanyMerge('Test123','Test1234','Test1235');
            
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test4 : Calling updateCompany method from WS_IDMSUIMSAuthCompanyServiceImpl
    static testmethod void testupdateCompany(){
        try{
            test.Starttest();
            WS_IDMSUIMSAuthCompanyServiceImpl wS_IDMSUIMSAuthCompanyServiceImpl=new WS_IDMSUIMSAuthCompanyServiceImpl();
            WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort companyManager_UIMSV2_ImplPort=new WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort();
            Boolean bolResponse2=companyManager_UIMSV2_ImplPort.updateCompany('Test123','Test1234','Test12345',new WS_IDMSUIMSAuthCompanyService.companyV3());
            
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test5 : Calling acceptCompanyMerge method from WS_IDMSUIMSAuthCompanyServiceImpl
    static testmethod void testacceptCompanyMerge(){
        try{
            test.Starttest();
            WS_IDMSUIMSAuthCompanyServiceImpl wS_IDMSUIMSAuthCompanyServiceImpl=new WS_IDMSUIMSAuthCompanyServiceImpl();
            WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort companyManager_UIMSV2_ImplPort=new WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort();
            Boolean bolResponse3=companyManager_UIMSV2_ImplPort.acceptCompanyMerge('Test123','Test1234','Test1235');
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test6 : Calling searchCompany method from WS_IDMSUIMSAuthCompanyServiceImpl
    static testmethod void testsearchCompany(){
        try{
            test.Starttest();
            WS_IDMSUIMSAuthCompanyServiceImpl wS_IDMSUIMSAuthCompanyServiceImpl=new WS_IDMSUIMSAuthCompanyServiceImpl();
            WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort companyManager_UIMSV2_ImplPort=new WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort();
            WS_IDMSUIMSAuthCompanyService.companyV3[] responnse1=companyManager_UIMSV2_ImplPort.searchCompany('Test123','Test1234',new WS_IDMSUIMSAuthCompanyService.companyV3());
            
            test.Stoptest();
        }
        catch(Exception e){}     
    }
    
    //Test7: Calling CreateCompany method from WS_IDMSUIMSAuthCompanyServiceImpl
    static testmethod void testcreateCompany(){
        try{
            test.Starttest();
            WS_IDMSUIMSAuthCompanyServiceImpl wS_IDMSUIMSAuthCompanyServiceImpl=new WS_IDMSUIMSAuthCompanyServiceImpl();
            WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort companyManager_UIMSV2_ImplPort=new WS_IDMSUIMSAuthCompanyServiceImpl.CompanyManager_UIMSV2_ImplPort();
            String strResponse2=companyManager_UIMSV2_ImplPort.createCompany('Test123','Test1234',new WS_IDMSUIMSAuthCompanyService.companyV3());
            
            test.Stoptest();
        }
        catch(Exception e){}
    }
    
    //Test8 : Calling All Subclass of WS_IDMSUIMSAuthCompanyService
    static testmethod void testWS_IDMSUIMSAuthCompanyService(){
        WS_IDMSUIMSAuthCompanyService wS_IDMSUIMSAuthCompanyService=new WS_IDMSUIMSAuthCompanyService();
        WS_IDMSUIMSAuthCompanyService.acceptCompanyMerge clsacceptCompanyMerge=new WS_IDMSUIMSAuthCompanyService.acceptCompanyMerge();
        WS_IDMSUIMSAuthCompanyService.companyV3 clscompanyV3=new WS_IDMSUIMSAuthCompanyService.companyV3();
        WS_IDMSUIMSAuthCompanyService.updateCompany clsupdateCompany=new WS_IDMSUIMSAuthCompanyService.updateCompany();
        WS_IDMSUIMSAuthCompanyService.InvalidImsServiceMethodArgumentException clsInvalidImsServiceMethodArgumentException=new WS_IDMSUIMSAuthCompanyService.InvalidImsServiceMethodArgumentException();
        WS_IDMSUIMSAuthCompanyService.searchCompany clssearchCompany=new WS_IDMSUIMSAuthCompanyService.searchCompany();
        WS_IDMSUIMSAuthCompanyService.RequestedEntryNotExistsException clsRequestedEntryNotExistsException=new WS_IDMSUIMSAuthCompanyService.RequestedEntryNotExistsException();
        WS_IDMSUIMSAuthCompanyService.rejectCompanyMergeResponse clsrejectCompanyMergeResponse=new WS_IDMSUIMSAuthCompanyService.rejectCompanyMergeResponse();
        WS_IDMSUIMSAuthCompanyService.getCompanyResponse clsgetCompanyResponse=new WS_IDMSUIMSAuthCompanyService.getCompanyResponse();
        WS_IDMSUIMSAuthCompanyService.UnexpectedLdapResponseException clsUnexpectedLdapResponseException=new WS_IDMSUIMSAuthCompanyService.UnexpectedLdapResponseException();
        WS_IDMSUIMSAuthCompanyService.LdapTemplateNotReadyException clsLdapTemplateNotReadyException=new WS_IDMSUIMSAuthCompanyService.LdapTemplateNotReadyException();
        WS_IDMSUIMSAuthCompanyService.updateCompanyResponse clsupdateCompanyResponse=new WS_IDMSUIMSAuthCompanyService.updateCompanyResponse();
        WS_IDMSUIMSAuthCompanyService.rejectCompanyMerge clsrejectCompanyMerge=new WS_IDMSUIMSAuthCompanyService.rejectCompanyMerge();
        WS_IDMSUIMSAuthCompanyService.RequestedInternalUserException clsRequestedInternalUserException=new WS_IDMSUIMSAuthCompanyService.RequestedInternalUserException();
        WS_IDMSUIMSAuthCompanyService.SecuredImsException clsSecuredImsException=new WS_IDMSUIMSAuthCompanyService.SecuredImsException();
        WS_IDMSUIMSAuthCompanyService.getCompany clsgetCompany=new WS_IDMSUIMSAuthCompanyService.getCompany();                                
        WS_IDMSUIMSAuthCompanyService.removePrimaryContact clsremovePrimaryContact=new WS_IDMSUIMSAuthCompanyService.removePrimaryContact();
        WS_IDMSUIMSAuthCompanyService.removePrimaryContactResponse clsremovePrimaryContactResponse=new WS_IDMSUIMSAuthCompanyService.removePrimaryContactResponse();                                
        WS_IDMSUIMSAuthCompanyService.createCompanyResponse clscreateCompanyResponse=new WS_IDMSUIMSAuthCompanyService.createCompanyResponse();
        WS_IDMSUIMSAuthCompanyService.createCompany clscreateCompany=new WS_IDMSUIMSAuthCompanyService.createCompany();                                
        WS_IDMSUIMSAuthCompanyService.acceptCompanyMergeResponse clsacceptCompanyMergeResponse=new WS_IDMSUIMSAuthCompanyService.acceptCompanyMergeResponse();
        WS_IDMSUIMSAuthCompanyService.IMSServiceSecurityCallNotAllowedException clsIMSServiceSecurityCallNotAllowedException=new WS_IDMSUIMSAuthCompanyService.IMSServiceSecurityCallNotAllowedException();                                
        WS_IDMSUIMSAuthCompanyService.searchCompanyResponse clssearchCompanyResponse=new WS_IDMSUIMSAuthCompanyService.searchCompanyResponse();                               
        
    }
}