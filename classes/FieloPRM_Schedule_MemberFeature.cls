/********************************************************************
* Company: Fielo
* Developer: Pablo Cassinerio
* Created Date: 23/08/2016
* Description: 
********************************************************************/
global class FieloPRM_Schedule_MemberFeature implements Schedulable{

    global Boolean isPromote = true;
    global Boolean isDelete = false;
    global List<FieloPRM_MemberFeature__c> newFeats = new List<FieloPRM_MemberFeature__c>();

    global void execute(SchedulableContext sc){    
        if(newFeats == null) newFeats = new List<FieloPRM_MemberFeature__c>();
        if(isDelete == null) isDelete = false;
        Set<id> setFeature = new Set<id>();
        List<FieloPRM_MemberFeature__c> toSerialize = new List<FieloPRM_MemberFeature__c>();
        Map<id,Set<String>> mapPRMUIMSIdFeature = new Map<id,Set<String>>();
        for(FieloPRM_MemberFeature__c memberFeature : newFeats){
            System.debug('memberFeature: ' + memberFeature.PRMUIMSId__c);
            if(isDelete || !memberFeature.F_PRM_CustomLogicOK__c){
                setFeature.add(memberFeature.F_PRM_Feature__c);
                Set<String> setPRMUIMSIdAux = new Set<String>();
                if(mapPRMUIMSIdFeature.containskey(memberFeature.F_PRM_Feature__c)){
                    setPRMUIMSIdAux = mapPRMUIMSIdFeature.get(memberFeature.F_PRM_Feature__c);
                }
                setPRMUIMSIdAux.add(memberFeature.PRMUIMSId__c);
                mapPRMUIMSIdFeature.put(memberFeature.F_PRM_Feature__c , setPRMUIMSIdAux);
            }else{
                toSerialize.add(memberFeature);
            }
        }
        if(!toSerialize.isEmpty()){
            FieloPRM_Schedule_MemberFeatureSerialize.scheduleNow(isPromote, 'OK', toSerialize, '');
        }

        String segmentQuery = 'SELECT ';
        for(String fieldName : Schema.getGlobalDescribe().get('FieloPRM_Feature__c').getDescribe().fields.getMap().keySet()) {
           segmentQuery += fieldName + ', ';
        }
        segmentQuery = segmentQuery.left(segmentQuery.lastIndexOf(','));
        segmentQuery += ' FROM ' + 'FieloPRM_Feature__c' + ' WHERE Id IN: setFeature    ';
        list<FieloPRM_Feature__c> listFeatures = (list<FieloPRM_Feature__c>)Database.query(segmentQuery) ;
        for(FieloPRM_Feature__c feature : listFeatures ){
            if(feature.F_PRM_CustomLogicClass__c != null && feature.F_PRM_CustomLogicClass__c != ''){
                toSerialize = new List<FieloPRM_MemberFeature__c>();
                for(FieloPRM_MemberFeature__c feat: newFeats){
                    if(feat.F_PRM_Feature__c == feature.Id){
                        toSerialize.add(feat);
                    }
                }
                String customLogic = feature.F_PRM_CustomLogicClass__c;
                Type t = Type.forName('',customLogic);
                if(t == null){
                    t = Type.forName(customLogic);
                }
                if(t==null){
                    FieloPRM_Schedule_MemberFeatureSerialize.scheduleNow(isPromote, 'ERROR', toSerialize, 'Problem Instantiating the customClass: '+customLogic+'. Please check the feature: '+feature.Id); 
                    continue;

                }
                FieloPRM_FeatureCustomLogic deliveryReward = (FieloPRM_FeatureCustomLogic)t.newInstance();

                if(mapPRMUIMSIdFeature.containskey(feature.id)){
                    try{  
                        System.debug(mapPRMUIMSIdFeature.get(feature.id));
                        if(isPromote){
                            deliveryReward.promote(mapPRMUIMSIdFeature.get(feature.id));
                        }else{
                            deliveryReward.demote(mapPRMUIMSIdFeature.get(feature.id));
                        }
                        if(!toSerialize.isEmpty()){
                            FieloPRM_Schedule_MemberFeatureSerialize.scheduleNow(isPromote, 'OK', toSerialize, '');   
                        }
                    }catch(Exception e){
                        if(!toSerialize.isEmpty()){
                            FieloPRM_Schedule_MemberFeatureSerialize.scheduleNow(isPromote, 'ERROR', toSerialize, e.getMessage());    
                        }
                    }
                }
            }
        }
    }

    public static void scheduleNow(Boolean isPro, List<FieloPRM_MemberFeature__c> newFeats){
        DateTime nowTime = datetime.now().addSeconds(65);
        String Seconds      = '0';
        Integer minutesInt = nowTime.minute() + (isPro ? 1 : 0);
        String Minutes      = minutesInt < 10 ? '0' + minutesInt : String.valueOf(minutesInt);
        String Hours        = nowTime.hour()   < 10 ? '0' + nowTime.hour()   : String.valueOf(nowTime.hour());
        String DayOfMonth   = String.valueOf(nowTime.day());
        String Month        = String.ValueOf(nowTime.month());
        String DayOfweek    = '?';
        String optionalYear = String.valueOf(nowTime.year());
        String CronExpression = Seconds+' '+Minutes+' '+Hours+' '+DayOfMonth+' '+Month+' '+DayOfweek+' '+optionalYear;
        FieloPRM_Schedule_MemberFeature cl = new FieloPRM_Schedule_MemberFeature();
        cl.isPromote = isPro;
        cl.isDelete = Trigger.isDelete;
        cl.newFeats = newFeats;
        try{
            if(!isRunning()){
                System.schedule('FieloPRM_Schedule_MemberFeature '+system.now() + isPro, CronExpression, cl);    
            } 
        }catch(Exception e){

        }catch(System.UnexpectedException e){

        }
    }

    global static Boolean isRunning(){
        CronTrigger[] triggers = [SELECT Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger WHERE CronJobDetail.Name LIKE 'FieloPRM_Schedule_MemberFeature%'AND NextFireTime != NULL LIMIT 1];
        return triggers.size() > 0;
    }
}