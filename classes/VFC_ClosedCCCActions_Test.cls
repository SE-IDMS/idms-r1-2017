@isTest
private class VFC_ClosedCCCActions_Test{
    public static testMethod void testclosedCCCAction(){
        System.debug('#### VFC_ClosedCCCActions_Test.testclosedCCCAction Started ####');
        
        User actionOwner = Utils_TestMethods.createSEStandardUser('clseActn');
        insert actionOwner;
        
        Account objAccount = Utils_TestMethods.createAccount();
        insert objAccount;

        Contact objContact = Utils_TestMethods.createContact(objAccount.Id, 'TestCCCContact');
        insert objContact;
        
        Case objCaseForAction = Utils_TestMethods.createCase(objAccount.id, objContact.id, 'Open');
        insert objCaseForAction;
        
        CCCAction__c objAction = Utils_TestMethods.createCCCAction(objCaseForAction.Id);
        insert objAction;
        
        objAction.ActionOwner__c = actionOwner.Id;
        objAction.Status__c='Closed';
        update objAction;
    
      
        PageReference pageRef = Page.VFP_ClosedCCCActions;
        Test.setCurrentPage(pageRef);
        VFC_ClosedCCCActions controller = new VFC_ClosedCCCActions();
        //controller.userName();
        controller.getCCCActions();
        System.debug('#### VFC_ClosedCCCActions_Test.testclosedCCCAction Ends ####');
    }
}