/*****************************************************************************************
    Author : Shruti Karn
    Description : For September 2012 Release to override 'New Beneficiary Package' button so that:
                  1. Only the Packages related to the Parent Contract are displayed for selection
                                       
*****************************************************************************************/
public class VFC_NewEditBeneficaryPKG
{
    public BeneficiaryPackage__c CVCPPkg{get;set;}
    public list<Package__c> lstPackages {get;set;}
    public string searchValue{get;set;}
    Id contractId;
    public string pkgId {get;set;}
    public String pkgName{get;set;}
    map<Id,package__c> mapIdPackage;
    ApexPages.StandardController controller;
    String queryString;
    
/******************************************************************************************************************
    Constructor
******************************************************************************************************************/ 
    public VFC_NewEditBeneficaryPKG(ApexPages.StandardController controller)
    {
        PageReference thisPage = ApexPages.currentPage();
        List<String> url = new list<String>();
        url = thisPage.getUrl().split('\\?');
        if(url.size() > 1)
            queryString = url[1];
        this.controller = controller;
        CVCPPkg = new BeneficiaryPackage__c();
        CVCPPkg = (BeneficiaryPackage__c)controller.getRecord();
        if(CVCPPkg.package__c != null)
        {
            Package__c pkg = [Select id, name from PAckage__c where id =: CVCPPkg.package__c limit 1];
            pkgName = pkg.Name;
        }
        if(CVCPPkg.Id != null)
        {
            CVCPPkg = [Select id,name,package__c,ContractValueChainPlayer__c from BeneficiaryPackage__c where id =: CVCPPkg.Id limit 1];
            
        }
        
        String cvcpID = ApexPages.currentPage().getParameters().get('cvcpId');
        if(cvcpId != null)
        {
            CTR_ValueChainPlayers__c cvcp = new CTR_ValueChainPlayers__c();
            cvcp  = [Select Contract__c from    CTR_ValueChainPlayers__c where id =: cvcpId limit 1];
            contractId = cvcp.contract__c;
            if(contractId != null)
            {
                lstPackages = new list<Package__c>();
                mapIdPackage = new map<Id,Package__c>([Select id, Name , Contract__c, contract__r.Name, PackageTemplate__c, packagetemplate__r.Name, status__c, startdate__C, enddate__c from Package__c where contract__c  =: contractId limit 10000]);
                for(Id pkgID : mapIdPackage.keySet())
                     lstPackages.add(mapIdPackage.get(pkgID));              
            }
        }
    }

/******************************************************************************************************************
    To get the list of PAckages related to the Parent Contract
******************************************************************************************************************/ 
    public void search()
    {
        String query = 'Select id, Name , Contract__c, contract__r.Name ,  PackageTemplate__c, packagetemplate__r.Name, status__c, startdate__C, enddate__c from Package__c where contract__c  =: contractId and name like ' +'\'%'+ searchvalue + '%\''+' limit 1000';
        lstPackages = database.query(query); 
    }
/******************************************************************************************************************
    To assign the selected PAckage to the Beneficiary Package
******************************************************************************************************************/ 
    public PageReference  SelectPKG()
    {
       if(mapIdPackage.containsKey(pkgId))
       {
           pkgName =  mapIdPackage.get(pkgId).Name;
           CVCPPkg.package__c = pkgId ;
       }
       return null;
    }
    
/******************************************************************************************************************
    To save the Beneficiary Package record
******************************************************************************************************************/ 
    public PageReference save()
    {
        if(pkgId != null && pkgId.trim()!= '')
            CVCPPkg.package__c = pkgId ;
        if(CVCPPkg.package__c == null)
        {        
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select a Package')); 
            return null;
        }
        try
        {
            upsert CVCPPkg;
            PageReference pageRef = new PageReference('/'+CVCPPkg.Id);
            pageRef.setRedirect(true);
            return pageRef;
        }
        catch(Exception e)
        {
            
            for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),'')); System.debug(e.getDmlMessage(i)); 
            }      
            return null;
        }
        
    }
/******************************************************************************************************************
    Save and New functionality the Beneficiary Package record
******************************************************************************************************************/ 
    public PageReference saveandnew()
    {
        if(pkgId != null && pkgId.trim()!= '')
            CVCPPkg.package__c = pkgId ;
        if(CVCPPkg.package__c == null)
        {        
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select a Package')); 
            return null;
        }
        try
        {
            upsert CVCPPkg;
            Schema.DescribeSObjectResult describeResult = Controller.getRecord().getSObjectType().getDescribe();
            PageReference pageRef= new PageReference('/' + describeResult.getKeyPrefix() + '/e?' + queryString);
            pageRef.setRedirect(true);
            return pageRef;
        }
        catch(DmlException e)
        {
          
            for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),'')); System.debug(e.getDmlMessage(i)); 
            } 
            return null;
        }
    }
    
    
    
}