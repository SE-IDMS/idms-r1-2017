@isTest
private class AP38_createSolutionCenterCountry_Test {

    static testMethod void createSolutionCenterCountryTest() {
    	
    	Country__c country1 = Utils_TestMethods.createCountry();
		insert country1;
		Account account1 = Utils_TestMethods.createAccount();
		insert account1;
		      
		Opportunity oppty = Utils_TestMethods.createOpenOpportunity(account1.id);
		oppty.OwnerId = UserInfo.getUserId();      
		insert oppty;  
		    
		SolutionCenter__c solcenter= Utils_TestMethods.createSolutionCenter();
		solcenter.Name = 'Test for VFP79_SearchSolutionCenter';
		solcenter.CountryOfLocation__c =country1.id;
		solcenter.BusinessesServed__c ='BD';
		insert  solcenter;
       
    }
}