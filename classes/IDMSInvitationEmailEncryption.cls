//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This class generates random token for invitation emails signature.
//                  This token expires in 7 days.
//
//*******************************************************************************************


public class IDMSInvitationEmailEncryption {
    
    //Returns part of the URL to be sent in the email
    public Static String EmailEncryption(IDMS_Send_Invitation__c invitation){
        invitation.IDMSToken__c = generateRandomString(20);
        invitation.IDMSSignatureGenerationDate__c = system.today();
        String signature = invitation.IDMSToken__c;
        update invitation;
        system.debug('signature: '+ signature);
        return signature;
    }
    
    //Returns part of the URL to be sent in the email
    public Static Boolean EmailDecryption(String Signature, String InvitationId){
        IDMS_Send_Invitation__c invite = [SELECT Id,IDMSToken__c,IDMSSignatureGenerationDate__c from IDMS_Send_Invitation__c where Id = :InvitationId limit 1][0];
        if(invite.IDMSSignatureGenerationDate__c != null && system.today() <= (invite.IDMSSignatureGenerationDate__c).addDays(7)){
            if (invite.IDMSToken__c == Signature){
                invite.IDMSToken__c = '';
                invite.IDMSSignatureGenerationDate__c = null;
                update invite;                
                return TRUE;
            } else{
                return FALSE;
            }
        }
        return false;
    }
    //This method generate random toekn 
    public static String generateRandomString(Integer len) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    } 
}