@isTest(SeeAllData=true)
private class AP_WorkOrderHandler_TEST {

    
    public static testMethod void myUnitTest() {
        // TO DO: implement unit test
      
      String footerA = 'France address footer';
      String footerB = 'Thailand address footer';
        
        //fill custom setting values   Create Country1 and Country2 names A&B
        
        FS_ServiceInterventionReportCountry__c csCountry1 = FS_ServiceInterventionReportCountry__c.getInstance('FR');
        if (csCountry1 == null) {
            csCountry1 = new FS_ServiceInterventionReportCountry__c();
            
            csCountry1.Name='FR';
            csCountry1.Country_del__c='France';
            csCountry1.Service_Intervention_Report_Footer__c= footerA;
            
            insert csCountry1;
        }
        else {
            footerA = csCountry1.Service_Intervention_Report_Footer__c;
        }
        
        FS_ServiceInterventionReportCountry__c csCountry2 = FS_ServiceInterventionReportCountry__c.getInstance('TH');
        if (csCountry1 == null) {
            csCountry2 = new FS_ServiceInterventionReportCountry__c();
            csCountry2.Name='TH';
            csCountry2.Country_del__c='Thailand';
            csCountry2.Service_Intervention_Report_Footer__c= footerB;
            
            insert csCountry2;
        }
        else {
            footerB = csCountry2.Service_Intervention_Report_Footer__c;
        }
        
        //fill countries object   Create Countries A&B&C
        
        Country__c CountryA = new Country__c();
        CountryA.Name = 'France';
        CountryA.CountryCode__c = 'FR';
        
        upsert CountryA CountryCode__c;
        
        Country__c CountryB = new Country__c();
        CountryB.Name = 'Thailand';
        CountryB.CountryCode__c = 'TH';
        
        upsert CountryB CountryCode__c;
        
        Country__c CountryC = new Country__c();
        CountryC.Name = 'Netherlands';
        CountryC.CountryCode__c = 'NL';
        
        upsert CountryC CountryCode__c;
        
        //create 3 accounts
        
        Account AccountA = new Account();
        AccountA.Name='TestAccountA'; 
        AccountA.Country__c=CountryA.id;
        AccountA.Street__c='New StreetA';
        AccountA.POBox__c='XXXA';
        AccountA.ZipCode__c = '12345';
        
        insert AccountA;
        
        Account AccountB = new Account();
        AccountB.Name='TestAccountB'; 
        AccountB.Country__c=CountryB.id;
        AccountB.Street__c='New StreetB';
        AccountB.POBox__c='XXXB';
        AccountB.ZipCode__c = '12345';
        
        insert AccountB;
        
        Account AccountC = new Account();
        AccountC.Name='TestAccountC'; 
        AccountC.Country__c=CountryC.id;
        AccountC.Street__c='New StreetC';
        AccountC.POBox__c='XXXC';
        AccountC.ZipCode__c= '12345';
        
        insert AccountC;
        
        
         List<SVMXC__Service_Order__c> workOrderList = new List<SVMXC__Service_Order__c>();

        SVMXC__Service_Order__c workOrder;
        workOrder = new SVMXC__Service_Order__c(SVMXC__Company__c = AccountA.Id);
        workOrderList .add(workOrder);
        workOrder = new SVMXC__Service_Order__c(SVMXC__Company__c = AccountB.Id);
        workOrderList .add(workOrder);
        workOrder = new SVMXC__Service_Order__c(SVMXC__Company__c = AccountC.Id);
        workOrderList .add(workOrder);
/*         
        workOrder =  Utils_TestMethods.createWorkOrder(AccountA.id);
                 workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
                 workOrder.SVMXC__Order_Status__c = 'UnScheduled';
                 workOrder.CustomerRequestedDate__c = Date.today();
                 workOrder.Service_Business_Unit__c = 'Energy';
                 workOrder.SVMXC__Priority__c = 'Normal-Medium';
                 workOrder.CustomerTimeZone__c= 'on leave';
                 workOrder.SendAlertNotification__c = true;
                 workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
                 workOrder.BackOfficeReference__c = '111111';
                 workOrder.Comments_to_Planner__c='testing';
*/
        insert workOrderList;
         
         
         AP_WorkOrderHandler.fillInFooter(workOrderList);
         
         System.debug('##### List of footers:');
         for (SVMXC__Service_Order__c aWorkorder : workOrderList) {
             System.debug('##### WO: '+aWorkorder + ' - Footer: '+aWorkorder.Tech_WOFooterPerCountry__c);
         }
         
        //test Logs
        List<SVMXC__Service_Order__c> workOrderListAfterInsert = [select Id, name, SVMXC__Company__c, Tech_WOFooterPerCountry__c from SVMXC__Service_Order__c where Id in :workOrderList];
        System.assertEquals(footerA, workOrderListAfterInsert[0].Tech_WOFooterPerCountry__c);
        System.assertEquals(footerB, workOrderListAfterInsert[1].Tech_WOFooterPerCountry__c);
        System.assertEquals(null, workOrderListAfterInsert[2].Tech_WOFooterPerCountry__c);
         
         
        }
 }