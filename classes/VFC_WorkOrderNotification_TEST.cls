/*
    Author          : Shruti Karn
    Date Created    : 26/11/2012
    Description     : Test class for VFC_WorkOrderNotification_Prepopulate class 
*/
@isTest(SeeAllData=true)
private class VFC_WorkOrderNotification_TEST {
    static testMethod void testWorkOrderNotificationPrepopulate(){
        
        Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
        
        Account testAccount1 = Utils_TestMethods.createAccount();
        insert testAccount1;
        
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        
        CustomerLocation__c custLocation = Utils_TestMethods.createCustomerLocation(testAccount.Id, testCountry.Id);
        insert custLocation;
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'Test Location';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = testAccount.id;
        site.PrimaryLocation__c = true;
        //site.PreferredFSE__c = sgm.Id;
        insert site;
        
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
        site1.SVMXC__Account__c = testAccount.id;
        site1.PrimaryLocation__c = false;
        //site1.PreferredFSE__c = sgm.Id;
        insert site1;
        
        
        
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id,'testContact');
        insert testContact;
        
        Case testCase = Utils_TestMethods.createCase(testAccount.Id,testContact.Id,'Open');
        //testCase.SVMXC__Site__c = site.Id;
        testCase.AssetOwner__c = testAccount.Id;
        insert testCase;
        
        Case testCase1 = Utils_TestMethods.createCase(testAccount.Id,testContact.Id,'Open');
        testCase1.SVMXC__Site__c = site1.Id;
        //testCase1.AssetOwner__c = testAccount.Id;
        insert testCase1;
        
        WorkOrderNotification__c testWON = Utils_TestMethods.createWorkOrderNotification(testCase.Id, custLocation.Id);
        insert testWON;
        
        SVMXC__Service_Order__c workOrder = Utils_TestMethods.createWorkOrder(testAccount.Id);
        insert workOrder;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(testWON);
        VFC_WorkOrderNotification_Prepopulate newObj = new VFC_WorkOrderNotification_Prepopulate(controller);
        
        PageReference pageRef = Page.VFP_WorkOrderNotification_PrePopulate ;
        //pageRef.getParameters().put(Label.CLDEC12SRV07, custLocation.Id);
        pageRef.getParameters().put(Label.CLDEC12SRV45, testCase.Id);
        Test.setCurrentPage(pageRef);
        newObj.gotoeditpage();
        Test.startTest();
        VFC_WorkOrderNotification_Prepopulate newObj2 = new VFC_WorkOrderNotification_Prepopulate(controller);        
        PageReference pageRef2 = Page.VFP_WorkOrderNotification_PrePopulate ;
        pageRef2.getParameters().put(Label.CLDEC12SRV45, testCase1.Id);
        Test.setCurrentPage(pageRef2);
        newObj2.gotoeditpage();
        
        VFC_WorkOrderNotification_Prepopulate newObj1 = new VFC_WorkOrderNotification_Prepopulate(controller);        
        PageReference pageRef1 = Page.VFP_WorkOrderNotification_PrePopulate ;
        pageRef1.getParameters().put(Label.CLMAY13SRV12, site.Id);
        Test.setCurrentPage(pageRef1);
        newObj1.gotoeditpage();
        
        VFC_WorkOrderNotification_Prepopulate newObj3 = new VFC_WorkOrderNotification_Prepopulate(controller);        
        PageReference pageRef3 = Page.VFP_WorkOrderNotification_PrePopulate ;
        pageRef3.getParameters().put(Label.CLMAY13SRV12, site1.Id);
        Test.setCurrentPage(pageRef3);
        newObj3.gotoeditpage();
        
         Test.stopTest();   
    }
    static testMethod void testWorkOrderNotificationPrepopulate1(){
    
     Account testAccount = Utils_TestMethods.createAccount();
        insert testAccount;
    Contact testContact = Utils_TestMethods.createContact(testAccount.Id,'testContact');
        insert testContact;
    Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;

    CustomerLocation__c custLocation = Utils_TestMethods.createCustomerLocation(testAccount.Id, testCountry.Id);
        insert custLocation;

    SVMXC__Site__c site = new SVMXC__Site__c();
    site.Name = 'Test Location';
    site.SVMXC__Street__c  = 'Test Street';
    site.SVMXC__Account__c = testAccount.id;
    site.PrimaryLocation__c = false;
    //site.PreferredFSE__c = sgm.Id;
        insert site;
    
    Case testCase = Utils_TestMethods.createCase(testAccount.Id,testContact.Id,'Open');
    //testCase.SVMXC__Site__c = site.Id;
    testCase.AssetOwner__c = testAccount.Id;
        insert testCase;
    Test.startTest();
    
    WorkOrderNotification__c testWON = Utils_TestMethods.createWorkOrderNotification(testCase.Id, custLocation.Id);
        insert testWON;
    
    ApexPages.StandardController controller = new ApexPages.StandardController(testWON);
    VFC_WorkOrderNotification_Prepopulate newObj = new VFC_WorkOrderNotification_Prepopulate(controller);
    PageReference pageRef = Page.VFP_WorkOrderNotification_PrePopulate ;
    pageRef.getParameters().put(Label.CLDEC12SRV45, testCase.Id);
    Test.setCurrentPage(pageRef);
    newObj.gotoeditpage();
    
    VFC_WorkOrderNotification_Prepopulate newObj1 = new VFC_WorkOrderNotification_Prepopulate(controller);        
    PageReference pageRef1 = Page.VFP_WorkOrderNotification_PrePopulate ;
    pageRef1.getParameters().put(Label.CLMAY13SRV12, site.Id);
    Test.setCurrentPage(pageRef1);
    newObj1.gotoeditpage();
     Test.stopTest();   
    
    }
}