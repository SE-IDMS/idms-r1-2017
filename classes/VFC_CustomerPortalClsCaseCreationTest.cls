@isTest
public class VFC_CustomerPortalClsCaseCreationTest {

public static testMethod void VFC_CustomerPortalClsCaseCreation() {
 
    //Set up all conditions for testing.
    PageReference pageRef = Page.VFP_CustomerPortalClsCaseCreation;
    Test.setCurrentPage(pageRef);
        
    VFC_CustomerPortalClsCaseCreation controller = new VFC_CustomerPortalClsCaseCreation(); 
        
    //PageReference nextPage = controller.getCaseDetailView();
    
    // Verify that page fails without parameters
      // System.assertEquals(null, nextPage);         
        
    account a = new account(name = 'name');
    Contact c = new Contact(AccountId = a.Id, Workphone__c = '-12345');
    
    
    
    case case1 = new case(ContactId=c.id, subject='case1', status='New');    
    //insert(case1);
    case case2 = new case(ContactId=c.id, subject='case2', status='New');    
    //insert(case2);
    Case case3 = new Case(ContactId=c.id, subject='case3', status='New');
    //insert(case3);
                         
    
    String selectedProductNone = 'None';
    String selectedProductSfw = 'SFW';
    String selectedProductHdw = 'HDW';
    
    String caseSubject1 = 'My Subject 1';
    String caseSubject2 = 'My Subject 2';
    
    String productfamilySfw = 'SWF'; 
    String productfamilyHdw = 'HDW';
    
    // Testing Begin Here    
    Test.starttest(); 
    controller.getUserCOntact();
    controller.getUserr();
    controller.getUserAcc();
    controller.getUserContact();
    
    controller.getProductTypes();
    
    controller.setselectedProductType('None');
    controller.readToWriteField();
    
    controller.setselectedProductType('HDW');
    controller.readToWriteField();

    controller.setselectedProductType('SFW');
    controller.getselectedProductType();
    controller.readToWriteField();
    
    controller.setOperatingSystem('WINDOWS 7');
    controller.getOperatingSystem();
    
    controller.setSoftwareVersion('SP 2');
    controller.getSoftwareVersion();
    
    controller.getCheckOperatingSystem();
    controller.getCheckSoftwareVersion();
    
    
    controller.setCaseSubject('My CAse 1 Subject');
    controller.getCaseSubject();
    
    controller.setCaseDescription('My Case 1 Description');
    controller.getCaseDescription();
    
    
    controller.setContactPhone('+33 100000000');
    controller.getContactPhone();
    
    controller.setAccountName('SCHNEIDER-ELECTRIC FRANCE');
    controller.getAccountName();        
    
    controller.submitCase();
    
    System.assertEquals(controller.submitCase(), null);

    
    Test.stoptest(); 
    
}


}