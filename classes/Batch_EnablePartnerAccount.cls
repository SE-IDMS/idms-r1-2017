global class Batch_EnablePartnerAccount implements Database.Batchable<AggregateResult>, Database.stateful, Schedulable{

    global DateTime startTime;
    global DateTime endTime;
    global List<PRM_Technical_Partner_Record__c> listRecords = new List<PRM_Technical_Partner_Record__c>();
    
    global Iterable<AggregateResult> start(Database.BatchableContext BC) {
        return new AggregateResultMergeAccIterable();
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        startTime = datetime.now();
        Map<string,integer> elligibleAccMap = new Map<string,integer>();
        //Process all AggregateResult and store them in a Map with the count of each group
        for (Sobject so : scope)  {
            AggregateResult ar = (AggregateResult)so;
            elligibleAccMap.put(string.valueof(ar.get('SEAccountID__c')),Integer.valueOf(ar.get('expr0')));
        }
        System.debug('##########################elligibleAccMap = ' + elligibleAccMap);
        
        Id businessAccRTId = [SELECT Id from RecordType where SobjectType='Account' and developerName='Customer' limit 1][0].Id;
        
        //Elligible business account must be enable as partner account (isPArtner = true)
        List<Account> listAccToUpdate = new List<Account>();
        for(Account a : [SELECT Id, isPartner, RecordtypeId, PRMUIMSID__c, SEAccountID__c
        FROM Account WHERE SEAccountID__c IN: elligibleAccMap.keySet()]){
            if(a.RecordtypeId == businessAccRTId && a.PRMUIMSID__c == null && elligibleAccMap.get(a.SEAccountId__c) == 1 && a.isPartner == false){
                PRM_Technical_Partner_Record__c p = new PRM_Technical_Partner_Record__c();
                try{
                    a.isPartner = true;
                    listAccToUpdate.add(a);
                    p.Name = a.SEAccountID__c;
                    p.PRMUIMS__c = a.PRMUIMSID__c;
                    p.Record_Id__c = a.Id;
                    p.Object__c = 'Account';
                    p.Success__c = true;
                    listRecords.add(p);
                }
                 catch (Exception e){
                    p.Name = a.SEAccountID__c;
                    p.PRMUIMS__c = a.PRMUIMSID__c;
                    p.Record_Id__c = a.Id;
                    p.Object__c = 'Account';
                    p.Error__c = true;
                    p.Error_Message__c = e.getMessage();
                    listRecords.add(p);
                }
            }
        }
        
        update listAccToUpdate;
        endTime = datetime.now();
    }
    
    global void finish(Database.BatchableContext BC) {
        System.debug('##########################Batch_Start_Time__c = ' + startTime );
        System.debug('##########################Batch_End_Time__c = ' + endTime);
        PRMTechnicalMergeHistory__c accountMergeHistory = new PRMTechnicalMergeHistory__c(Name = BC.getJobId(),
        Batch_Start_Time__c = startTime, Batch_End_Time__c = endTime);
        insert accountMergeHistory;
        
        for(PRM_Technical_Partner_Record__c r: listRecords){
            r.Batch_Id__c = accountMergeHistory.Id;
        }
        insert listRecords;
    }
    
    //Schedule method
    global void execute(SchedulableContext sc) {
        Batch_EnablePartnerAccount batchAcc = new Batch_EnablePartnerAccount();
        database.executeBatch(batchAcc);
    }
}