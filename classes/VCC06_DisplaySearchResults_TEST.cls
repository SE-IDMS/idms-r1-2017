/**
 
 */
@isTest
private class VCC06_DisplaySearchResults_TEST {

    static testMethod void testDisplaySearchResultsComponent() {
        // TO DO: implement unit test
        
     Test.startTest();
       VCC06_DisplaySearchResults  controller = new VCC06_DisplaySearchResults();      
        VFC_ControllerBase basePageController = new VFC_ControllerBase();
        VCC_ControllerBase basecomponentcontroller = new VCC_ControllerBase();

        basePageController.getThis();
        basePageController.getComponentControllerMap();        
        basePageController.setComponentControllerMap('resultsComponent',controller );             
        basecomponentcontroller.pageController = basePageController ;
        basecomponentcontroller.key = 'resultsComponent';
        basecomponentcontroller.pageController = basePageController ;
        
        List<String> keywords = new List<String>();
        Utils_DummyDataForTest dummydata = new Utils_DummyDataForTest();
        controller.columnList = dummydata.getColumns();
        
        Schema.sObjectType objType;
        Schema.DescribeSObjectResult objDescribe;
        Map<String, Schema.SObjectField> objFields;
        Map<String, Schema.DisplayType> ObjFieldTypes = new Map<String, Schema.DisplayType>();
        Schema.SObjectField field;
        Schema.DescribeFieldResult fieldDescribe;
        Schema.DisplayType fieldType;
        String[] dataType;
        List<String> Filters;    
        for(integer i=0;i < 4;i++){
        controller.columnList.add(new SelectOption('Field8__c','commercial Reference'));            
        }

        //Utils_DataSource.Result=
        Utils_DataSource.Result  tempresult = new Utils_DataSource.Result();
        tempresult = dummydata.Search(keywords,keywords);
        List<DataTemplate__c> dataTemplates = (List<DataTemplate__c>)tempresult.recordList;
        Set<Object> exisRec = new Set<Object>();
        for(DataTemplate__c dt: dataTemplates )
        {
            exisRec.add(dt.Field1__c);
        }
        controller.existingRecords = exisRec;

            objType= DataTemplate__c.getSObjectType();
            objDescribe =objType.getdescribe();
            objFields =objDescribe.fields.getMap();
            for(SelectOption selectValue:controller.columnList)
            {
                field = objFields.get(selectValue.getvalue());
                fieldDescribe = field.getdescribe();
                fieldType = fieldDescribe.getType(); 
                objFieldTypes.put(selectValue.getvalue(),FieldType);          
            }
            
            //dataType = controller.getDataType(objFields,controller.columnList);
            
        
        PageReference pageRef= Page.VFP25_AddUpdateProductforCase;
        pageRef.getParameters().put('sortColumn','4');

        controller.setpageNumber(2);
        controller.recordsPerPage = 4;
        controller.getComponentController();

       controller.displayRecordList=tempresult.recordList; 

       controller.pageNumber = 5; 
       controller.numberOfPages = 2;
       controller.getpageNumber();
       
       controller.pageNumber = 0; 
       controller.getpageNumber();

        List<String> FieldForKeyList = new List<String>();

        VCC06_DisplaySearchResults.DataDisplayWrapper[] wrapperclass = new VCC06_DisplaySearchResults.DataDisplayWrapper[]{};
        VCC06_DisplaySearchResults.DataDisplayWrapper wrapperclass1 = new VCC06_DisplaySearchResults.DataDisplayWrapper(tempresult.recordList[1],1,1,controller.columnList,basePageController,objFields,FieldForKeyList ,controller.existingRecords,objFieldTypes);
        wrapperclass = controller.getDisplaySearchResults();
        
        wrapperclass1.Action1();
     //   wrapperclass1.checkRecord();
        
        
        Test.setCurrentPageReference(pageRef);
        system.debug('current pagereff'+system.currentPageReference().getparameters());
        controller.getrecordsDisplay();
        controller.first();
        controller.last();
        controller.previous();
        controller.next();
        controller.sortColumn();
       // boolean hasNext = controller.hasNext;
       // boolean hasPrevious = controller.hasPrevious;
        //boolean hasLast = controller.hasFirst;
        //boolean hasFirst = controller.hasLast;
        controller.getpageNumber();
        controller.pageNumberRenderer();
     Test.stopTest();
        
        
       
    }
}