@isTest
private class VFC_QuickANIUpdate_TEST {
    static testMethod void testVFC_QuickANIUpdate() {
            
        ApexPages.currentPage().getParameters().put('ContactId', '');
        ApexPages.currentPage().getParameters().put('ANINumber', '');
        ApexPages.currentPage().getParameters().put('AccountID', '');
         Country__c countryObj=new Country__c();
         countryObj.Name='India122';
         countryObj.CountryCode__c='91';
        // countryObj.Currency__c='INR';
         insert countryObj; 
        Account objAccount = Utils_TestMethods.createAccount();
        objAccount.RecordTypeId = Label.CLOCT13ACC08;
        insert objAccount;
        Contact objContact = Utils_TestMethods.createContact(objAccount.Id,'TestContact99');
        objContact.phone = '+918376483';
        objcontact.country__c=countryObj.id;
        insert objContact;     
        ApexPages.currentPage().getParameters().put('AccountID', objAccount.ID);
       //ApexPages.currentPage().getParameters().put('ContactId', objContact.Id);
        ApexPages.currentPage().getParameters().put('ANINumber', '');
        VFC_QuickANIUpdate quickaniobj1 = new VFC_QuickANIUpdate();   
        quickaniobj1.isAccountPhone=true;
        quickaniobj1.isani=false;      
        quickaniobj1.Pageredirection();
        
        ApexPages.currentPage().getParameters().put('ContactId', objContact.Id);
        ApexPages.currentPage().getParameters().put('ANINumber', '+918376483');
        VFC_QuickANIUpdate quickaniobj = new VFC_QuickANIUpdate();
        quickaniobj.isAccountPhone=false;
        quickaniobj.isani=false;
        quickaniobj.Pageredirection();
        
        ApexPages.currentPage().getParameters().put('AccountID', objAccount.ID);
        ApexPages.currentPage().getParameters().put('ANINumber', '+918376483');
        VFC_QuickANIUpdate quickaniobj2 = new VFC_QuickANIUpdate();
        quickaniobj2.isAccountPhone=true;
        quickaniobj2.isani=false;
        quickaniobj2.Pageredirection();
        
        ApexPages.currentPage().getParameters().put('AccountID',objAccount.ID);
        ApexPages.currentPage().getParameters().put('ANINumber', '+918376483');
        VFC_QuickANIUpdate quickaniobj4 = new VFC_QuickANIUpdate();
        quickaniobj4.isani=true;
        quickaniobj4.Pageredirection(); 
        
        ApexPages.currentPage().getParameters().put('AccountID','');
        ApexPages.currentPage().getParameters().put('ANINumber', '+918376483');
        VFC_QuickANIUpdate quickaniobj3 = new VFC_QuickANIUpdate();
        quickaniobj3.isani=true;
        quickaniobj3.Pageredirection(); 
                  
        }
   }