/*
Author          : Global Delivery Team - Bangalore
Date Created    : 
Description     : Test class for VFP_CaseFieldsPrepopulatFromCTI class.
*/
@isTest 
private class VFC_CaseFieldsPrepopulatFromCTI_Test {

    static testMethod void TestCaseFieldsPrepopulatFromCTI() { 
        // creating Test Data
        Account  acc= Utils_TestMethods.createAccount(userInfo.getUserId()); 
        insert acc;
        Contact   con= Utils_TestMethods.createContact(acc.id,'TestContact');
        insert con;
        
        CaseClassification__c  cc = Utils_TestMethods.createCaseClassification();
        insert cc;
        Country__c country = Utils_TestMethods.createCountry();
        insert country;
        CustomerCareTeam__c  cct=Utils_TestMethods.CustomerCareTeam(country.id);
        cct.CTI_Skill__c ='Test_VF';
        insert cct;
        TeamCaseJunction__c tcj = new TeamCaseJunction__c();
        tcj.CaseClassification__c = cc.id;
        tcj.CCTeam__c = cct.id;
        tcj.Default__c=false;
        insert tcj;
        tcj.Default__c=true;
        update tcj;
        
        
        PageReference pageRef = Page.VFP_CaseFieldsPrepopulatFromCTI;
        Test.setCurrentPage(pageRef);
        Case c = new Case();
        VFC_CaseFieldsPrepopulatFromCTI controller = new VFC_CaseFieldsPrepopulatFromCTI(new ApexPages.StandardController(c));
        ApexPages.currentPage().getParameters().put('CtiSkill', 'Test_VF');
        ApexPages.currentPage().getParameters().put('ContactId', con.id);
        ApexPages.currentPage().getParameters().put('ANI', '+333333333');
        ApexPages.currentPage().getParameters().put('CountryCode', country.CountryCode__c);
        controller.init();
        
        
    
    }
    
}