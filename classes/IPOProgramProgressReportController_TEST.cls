@isTest
public class IPOProgramProgressReportController_TEST 
{

    static testMethod void IPOProgramProgressReportTest()
    {

        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPOCC');
        System.runAs(runAsUser)
        {
            PageReference pageRef = Page.IPO_Program_Progress_Report;
            Test.setCurrentPage(pageRef);
            
            String Year = '2013';
                       
            IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
            
            IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Program_Owner__c = runasuser.id,Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp;
            
            Program_Progress_Snapshot__c PPS=new Program_Progress_Snapshot__c(IPO_Program__c=IPOp.id,IPO_Program_Health_Q1__c='On Time Delivered');
            PPS.Current_Year__c = Year;
            insert PPS;
           
            IPO_Strategic_Milestone__c IPOM=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation_Regions__c='NA;APAC',Due_Date__c=Date.ValueOf(Year + label.IPO_Q4_Start));
            Insert IPOM;
             
            IPO_Strategic_Milestone__c IPOM2=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile2',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(Year + label.IPO_Q2_Start),Progress_Summary_Q1__c ='In Progress',Status_in_Quarter_Q1__c='Test',Roadblocks_Q1__c='Test1',Decision_Review_Outcomes_Q1__c = 'test');
            Insert IPOM2; 

            IPO_Strategic_Milestone__c IPOM3=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile3',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(Year + label.IPO_Q1_Start),Progress_Summary_Q2__c ='In Progress',Status_in_Quarter_Q2__c='TestQ2',Roadblocks_Q2__c='Test1Q2',Decision_Review_Outcomes_Q2__c = 'testQ2');
            Insert IPOM3; 

            IPO_Strategic_Milestone__c IPOM4=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile4',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(Year + label.IPO_Q3_Start),Progress_Summary_Q3__c ='In Progress',Status_in_Quarter_Q3__c='TestQ3',Roadblocks_Q3__c='Test1Q3',Decision_Review_Outcomes_Q3__c = 'testQ3');
            Insert IPOM4;

            IPO_Strategic_Milestone__c IPOM5=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile5',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(Year + label.IPO_Q4_Start),Progress_Summary_Q4__c ='In Progress',Status_in_Quarter_Q4__c='TestQ4',Roadblocks_Q4__c='Test1Q4',Decision_Review_Outcomes_Q4__c = 'testQ4');
            Insert IPOM5;

            ApexPages.currentPage().getparameters().put('Id',IPOp.id);
            ApexPages.StandardController stanPage= new ApexPages.StandardController(IPOp);
            IPOProgramProgressReportController controller = new IPOProgramProgressReportController (stanPage);
           //  IPOProgramProgressReportController controller = new IPOProgramProgressReportController(new ApexPages.StandardController(IPOp));
           
            Controller.getFilterQuarter() ;
            Controller.Quarter = 'Q2';
            Controller.getFilterProgramOwnerList() ;
            Controller.getFilterInitiativeList();
            Controller.ProgOwner = Label.Connect_Email;
            Controller.getFilterYear();
            Controller.getIPOPrgProgress();
            Controller.getMilestoneDue() ;
            Controller.getMilestoneDueQ2();
            Controller.getMilestoneDueQ3();
            Controller.getMilestoneDueQ4() ;

            Controller.Quarter = 'Q1';
            Controller.getFilterQuarter() ;
            Controller.getFilterProgramOwnerList() ;
            Controller.getFilterInitiativeList();
            Controller.getFilterYear();   
            Controller.ProgOwner = runasuser.Firstname + ' ' + runasuser.Lastname;
            Controller.getIPOPrgProgress();           
            Controller.getMilestoneDue() ;
            Controller.getMilestoneDueQ2();
            Controller.getMilestoneDueQ3();
            Controller.getMilestoneDueQ4() ;
           
            
            Controller.Quarter = 'Q3';
            Controller.getFilterQuarter() ;
            Controller.getFilterProgramOwnerList() ;
            Controller.getFilterInitiativeList();
            Controller.getFilterYear();            
            Controller.ProgOwner = Label.Connect_Email;
            Controller.getIPOPrgProgress();            
            Controller.getMilestoneDue() ;
            Controller.getMilestoneDueQ2();
            Controller.getMilestoneDueQ3();
            Controller.getMilestoneDueQ4() ;
            
            
            Controller.Quarter = 'Q4';
            Controller.getFilterQuarter() ;
            Controller.getFilterProgramOwnerList() ;
            Controller.getFilterInitiativeList();
            Controller.getFilterYear();            
            Controller.ProgOwner = Label.Connect_Email;
            Controller.getIPOPrgProgress();
            Controller.getMilestoneDue() ;
            Controller.getMilestoneDueQ2();
            Controller.getMilestoneDueQ3();
            Controller.getMilestoneDueQ4() ;
        }
    }
    
    static testMethod void IPOProgramProgressReportTest1()
    {

        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('IPOCC');
        System.runAs(runAsUser)
        {
            PageReference pageRef = Page.IPO_Program_Progress_Report;
            Test.setCurrentPage(pageRef);
            
            IPO_Stategic__c IPOinti=new IPO_Stategic__c(Name='IPOinitiative');
            insert IPOinti;
            
            IPO_Strategic_Program__c IPOp=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Program_Owner__c = runasuser.id,Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp;
            
            IPO_Strategic_Program__c IPOp1=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Program_Owner__c = Label.Connect_Email,Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp1;
            
            IPO_Strategic_Program__c IPOp2=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Program_Owner__c = Label.Connect_Email,Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp2;
            
            IPO_Strategic_Program__c IPOp3=new IPO_Strategic_Program__c(IPO_Strategic_Initiative_Name__c=IPOinti.id,Name='IPO1',IPO_Strategic_Program_Leader_1__c = runasuser.id, IPO_Strategic_Program_Leader_2__c = runasuser.id, Global_Functions__c='GM;S&I;HR',Program_Owner__c = Label.Connect_Email,Global_Business__c='ITB;power',Operation__c='NA;APAC');
            insert IPOp3;
            
            Program_Progress_Snapshot__c PPS=new Program_Progress_Snapshot__c(IPO_Program__c=IPOp.id,IPO_Program_Health_Q2__c='On Time Delivered');
            insert PPS;
            
            Program_Progress_Snapshot__c PPS1=new Program_Progress_Snapshot__c(IPO_Program__c=IPOp1.id,IPO_Program_Health_Q2__c='On Time Delivered');
            insert PPS1;
            
            Program_Progress_Snapshot__c PPS2=new Program_Progress_Snapshot__c(IPO_Program__c=IPOp2.id,IPO_Program_Health_Q2__c='On Time Delivered');
            insert PPS2;
            
            Program_Progress_Snapshot__c PPS3=new Program_Progress_Snapshot__c(IPO_Program__c=IPOp3.id,IPO_Program_Health_Q2__c='On Time Delivered');
            insert PPS3;
            
            IPO_Strategic_Milestone__c IPOM=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM;S&I;HR',Global_Business__c='ITB;power',Operation_Regions__c='NA;APAC',Due_Date__c=Date.ValueOf(2014 + label.IPO_Q4_Start));
            Insert IPOM;
             
            IPO_Strategic_Milestone__c IPOM2=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile2',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(2014 + label.IPO_Q2_Start),Progress_Summary_Q1__c ='In Progress',Status_in_Quarter_Q1__c='Test',Roadblocks_Q1__c='Test1',Decision_Review_Outcomes_Q1__c = 'test');
            Insert IPOM2; 

            IPO_Strategic_Milestone__c IPOM3=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile3',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(2014 + label.IPO_Q1_Start),Progress_Summary_Q2__c ='In Progress',Status_in_Quarter_Q2__c='TestQ2',Roadblocks_Q2__c='Test1Q2',Decision_Review_Outcomes_Q2__c = 'testQ2');
            Insert IPOM3; 

            IPO_Strategic_Milestone__c IPOM4=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile4',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(2014 + label.IPO_Q3_Start),Progress_Summary_Q3__c ='In Progress',Status_in_Quarter_Q3__c='TestQ3',Roadblocks_Q3__c='Test1Q3',Decision_Review_Outcomes_Q3__c = 'testQ3');
            Insert IPOM4;

            IPO_Strategic_Milestone__c IPOM5=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile5',IPO_Strategic_Program_Name__c=IPOp.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(2014 + label.IPO_Q4_Start),Progress_Summary_Q4__c ='In Progress',Status_in_Quarter_Q4__c='TestQ4',Roadblocks_Q4__c='Test1Q4',Decision_Review_Outcomes_Q4__c = 'testQ4');
            Insert IPOM5;
            
            IPO_Strategic_Milestone__c IPOM6=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile6',IPO_Strategic_Program_Name__c=IPOp1.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(2014 + label.IPO_Q4_Start),Progress_Summary_Q4__c ='In Progress',Status_in_Quarter_Q4__c='TestQ4',Roadblocks_Q4__c='Test1Q4',Decision_Review_Outcomes_Q4__c = 'testQ4');
            Insert IPOM6;
            
            IPO_Strategic_Milestone__c IPOM7=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile7',IPO_Strategic_Program_Name__c=IPOp2.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(2014 + label.IPO_Q4_Start),Progress_Summary_Q4__c ='In Progress',Status_in_Quarter_Q4__c='TestQ4',Roadblocks_Q4__c='Test1Q4',Decision_Review_Outcomes_Q4__c = 'testQ4');
            Insert IPOM7;
            
             IPO_Strategic_Milestone__c IPOM8=new IPO_Strategic_Milestone__c(IPO_Strategic_Milestone_Name__c='testIPOMile8',IPO_Strategic_Program_Name__c=IPOp3.id,Global_Functions__c='GM',Global_Business__c='ITB;power',Operation_Regions__c='APAC',Due_Date__c=Date.ValueOf(2014 + label.IPO_Q4_Start),Progress_Summary_Q4__c ='In Progress',Status_in_Quarter_Q4__c='TestQ4',Roadblocks_Q4__c='Test1Q4',Decision_Review_Outcomes_Q4__c = 'testQ4');
            Insert IPOM8;
            
            ApexPages.currentPage().getparameters().put('Id',IPOp.id);
            ApexPages.StandardController stanPage= new ApexPages.StandardController(IPOp);
            IPOProgramProgressReportController controller = new IPOProgramProgressReportController (stanPage);
           //  IPOProgramProgressReportController controller = new IPOProgramProgressReportController(new ApexPages.StandardController(IPOp));
           
            Controller.getFilterQuarter() ;
            Controller.Quarter = 'Q2';
            Controller.getFilterProgramOwnerList() ;
            Controller.getFilterInitiativeList();
            Controller.getFilterYear();
            Controller.Initiative = 'Abc';
            Controller.ProgOwner = Label.Connect_Email;
            Controller.getIPOPrgProgress();
            Controller.getMilestoneDue() ;
            Controller.getMilestoneDueQ2();
            Controller.getMilestoneDueQ3();
            Controller.getMilestoneDueQ4() ;

            Controller.Quarter = 'Q1';
            Controller.getFilterQuarter() ;
            Controller.getFilterProgramOwnerList() ;
            Controller.getFilterInitiativeList();
            Controller.getFilterYear();            
            Controller.Initiative = 'Abc';
            Controller.ProgOwner = runasuser.Firstname + ' ' + runasuser.Lastname;
            Controller.getIPOPrgProgress();           
            Controller.getMilestoneDue() ;
            Controller.getMilestoneDueQ2();
            Controller.getMilestoneDueQ3();
            Controller.getMilestoneDueQ4() ;
           
            
            Controller.Quarter = 'Q3';
            Controller.getFilterQuarter() ;
            Controller.getFilterProgramOwnerList() ;
            Controller.getFilterInitiativeList();
            Controller.getFilterYear();            
            Controller.ProgOwner = Label.Connect_Email;
            Controller.getIPOPrgProgress();            
            Controller.getMilestoneDue() ;
            Controller.getMilestoneDueQ2();
            Controller.getMilestoneDueQ3();
            Controller.getMilestoneDueQ4() ;
            
            
            Controller.Quarter = 'Q4';
            Controller.getFilterQuarter() ;
            Controller.getFilterProgramOwnerList() ;
            Controller.getFilterInitiativeList();
            Controller.getFilterYear();            
            Controller.ProgOwner = Label.Connect_Email;
            Controller.getIPOPrgProgress();
            Controller.getMilestoneDue() ;
            Controller.getMilestoneDueQ2();
            Controller.getMilestoneDueQ3();
            Controller.getMilestoneDueQ4() ;
            
            ApexPages.currentPage().getparameters().put('Id',IPOp1.id);
            ApexPages.StandardController stanPage1= new ApexPages.StandardController(IPOp1);
            IPOProgramProgressReportController controller1 = new IPOProgramProgressReportController (stanPage1);
            
            Controller1.Quarter = 'Q4';
            Controller1.getFilterQuarter() ;
            Controller1.getFilterProgramOwnerList() ;
            Controller1.getFilterInitiativeList();
            Controller1.getFilterYear();            
            //Controller1.ProgOwner = runasuser.Firstname + ' ' + runasuser.Lastname;
            Controller1.getIPOPrgProgress();
            Controller1.getMilestoneDue() ;
            Controller1.getMilestoneDueQ2();
            Controller1.getMilestoneDueQ3();
            Controller1.getMilestoneDueQ4() ;
            
            Controller1.Quarter = 'Q2';
            Controller1.getFilterQuarter() ;
            Controller1.getFilterProgramOwnerList() ;
            Controller1.getFilterInitiativeList();
            Controller1.getFilterYear();
            Controller1.Initiative = 'Abcd';
            //Controller1.ProgOwner = runasuser.Firstname + ' ' + runasuser.Lastname;
            Controller1.getIPOPrgProgress();
            Controller1.getMilestoneDue() ;
            Controller1.getMilestoneDueQ2();
            Controller1.getMilestoneDueQ3();
            Controller1.getMilestoneDueQ4() ;
            
             ApexPages.currentPage().getparameters().put('Id',IPOp2.id);
            ApexPages.StandardController stanPage2= new ApexPages.StandardController(IPOp2);
            IPOProgramProgressReportController controller2 = new IPOProgramProgressReportController (stanPage2);
            
            Controller2.Quarter = 'Q3';
            Controller2.getFilterQuarter() ;
            Controller2.getFilterProgramOwnerList() ;
            Controller2.getFilterInitiativeList();
            Controller2.getFilterYear();            
            //Controller2.ProgOwner = runasuser.Firstname + ' ' + runasuser.Lastname;
            Controller2.getIPOPrgProgress();
            Controller2.getMilestoneDue() ;
            Controller2.getMilestoneDueQ2();
            Controller2.getMilestoneDueQ3();
            Controller2.getMilestoneDueQ4() ;
            
            
            
             ApexPages.currentPage().getparameters().put('Id',IPOp3.id);
            ApexPages.StandardController stanPage3= new ApexPages.StandardController(IPOp3);
            IPOProgramProgressReportController controller3 = new IPOProgramProgressReportController (stanPage3);
            
            Controller3.Quarter = 'Q2';
            Controller3.getFilterQuarter() ;
            Controller3.getFilterProgramOwnerList() ;
            Controller3.getFilterInitiativeList();
            Controller3.getFilterYear();
            //Controller3.ProgOwner = runasuser.Firstname + ' ' + runasuser.Lastname;
            Controller3.getIPOPrgProgress();
            Controller3.getMilestoneDue() ;
            Controller3.getMilestoneDueQ2();
            Controller3.getMilestoneDueQ3();
            Controller3.getMilestoneDueQ4() ;
        }
    }
}