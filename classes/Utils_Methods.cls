/*
    Author          : Adrian Modolea ~ adrian.modolea@axoa.fr 
    Date Created    : 11/10/2010
    Description     : Utility class for commonly used methods 
*/
public without sharing class Utils_Methods 
{
    public static Map<String,Decimal> currencyMap=null;
    public static Map<String,Decimal> datedCurrencyMap=null;
    
    // escaping SOSL special characters & | ! ( ) { } [ ] ^ “ ~ * ? : \ ' for the FIND expression
    public static String escapeForSOSL(String str) {
        Pattern patt = Pattern.compile('([\\\'\\?\\&\\|\\!\\{\\}\\[\\]\\(\\)\\^\\~\\*\\:\\"\\+\\-\\\\])');
        //Pattern patt = Pattern.compile('([\\\'\\|\\!\\{\\}\\[\\]\\(\\)\\^\\~\\*\\:\\"\\+\\-\\\\])');
        return patt.matcher(str).replaceAll('\\\\$1');  
    }

    // escaping special characters \ ' for the WHERE condition
    public static String escapeForWhere(String str) {
        return STRING.escapeSingleQuotes(str.replace('\\', '\\\\'));
    }
    
    //Formats the date according to the user's locale and returns the corresponding string
     public static string convertToString(Map<String, Schema.SObjectField> mapField, Map<String,Schema.DisplayType> mapType, SObject obj, String fieldAPIName)
    {
        System.debug('#### convertToString');
        String dateAsString;
        Schema.DisplayType fieldType = mapType.get(fieldAPIName);
        Map<String, String> mappedValues = new Map<String, String>(); //map for holding locale to datetime format
        mappedValues = mapLocaleValues(); //populate the map with all the locale specific datetime formats
        String user_locale = UserInfo.getLocale(); //grab the locale of the user
        system.debug('userlocale'+user_locale);
        String datetime_format = 'dd/MM/yyyy'; //variable for the datetime format defaulted to the France format
        
        System.debug('#### FieldType.name(): '+FieldType.name());
 
        if(FieldType.name() == 'DATE' || fieldType.name() == 'DATETIME')
        {
            System.debug('#### FieldType is Date or DateTime');
            DateTime objDate = (DateTime)obj.get(mapField.get(fieldAPIName));
            if (mappedValues.containsKey(user_locale)) 
            { //if the map contains the correct datetime format                    
                datetime_format = mappedValues.get(user_locale); //grab the datetime format for the locale
                System.debug('#### datetime_format: '+datetime_format);
            }
            if(objDate != null && datetime_format!=null)
                dateAsString = objDate.format(datetime_format);
            return dateAsString;
        }        
        else if(fieldType.name() == 'anytype')
        {
            System.debug('#### FieldType is anytype');
            String valueAsString;
            if(obj.get(mapField.get(fieldAPIName)) != null)
               valueAsString = String.valueof(obj.get(mapField.get(fieldAPIName)));
            return valueAsString;
        }
        else
        {
            System.debug('#### FieldType is else');
            String value = String.valueof(obj.get(mapField.get(fieldAPIName)));
            return value;
        }
    }
 
    //Map of User locale and Date Format
    public static Map<String, String> mapLocaleValues() 
    {
            Map<String, String> locale_map = new Map<String, String>(); //holds the locale to timedate formats
            locale_map.put('ar', 'dd/MM/yyyy');
            locale_map.put('ar_AE', 'dd/MM/yyyy');
            locale_map.put('ar_BH', 'dd/MM/yyyy');
            locale_map.put('ar_JO', 'dd/MM/yyyy');
            locale_map.put('ar_KW', 'dd/MM/yyyy');
            locale_map.put('ar_LB', 'dd/MM/yyyy');
            locale_map.put('ar_SA', 'dd/MM/yyyy');
            locale_map.put('bg_BG', 'yyyy-M-d');
            locale_map.put('ca', 'dd/MM/yyyy');
            locale_map.put('ca_ES', 'dd/MM/yyyy');
            locale_map.put('ca_ES_EURO', 'dd/MM/yyyy');
            locale_map.put('cs', 'd.M.yyyy');
            locale_map.put('cs_CZ', 'd.M.yyyy');
            locale_map.put('da', 'dd-MM-yyyy');
            locale_map.put('da_DK', 'dd-MM-yyyy');
            locale_map.put('de', 'dd.MM.yyyy');
            locale_map.put('de_AT', 'dd.MM.yyyy');

            locale_map.put('de_AT_EURO', 'dd.MM.yyyy');
            locale_map.put('de_CH', 'dd.MM.yyyy');
            locale_map.put('de_DE', 'dd.MM.yyyy');
            locale_map.put('de_DE_EURO', 'dd.MM.yyyy');
            locale_map.put('de_LU', 'dd.MM.yyyy');
            locale_map.put('de_LU_EURO', 'dd.MM.yyyy');
            locale_map.put('el_GR', 'd/M/yyyy');
            locale_map.put('en_AU', 'd/MM/yyyy');
            locale_map.put('en_B', 'M/d/yyyy');
            locale_map.put('en_BM', 'M/d/yyyy');
            locale_map.put('en_CA', 'dd/MM/yyyy');
            locale_map.put('en_GB', 'dd/MM/yyyy');
            locale_map.put('en_GH', 'M/d/yyyy');
            locale_map.put('en_ID', 'M/d/yyyy');
            locale_map.put('en_IE', 'dd/MM/yyyy');
            locale_map.put('en_IE_EURO', 'dd/MM/yyyy');
            locale_map.put('en_NZ', 'd/MM/yyyy');
            locale_map.put('en_SG', 'M/d/yyyy');
            locale_map.put('en_US', 'M/d/yyyy hh:mm:ss Z');
            locale_map.put('en_ZA', 'yyyy/MM/dd');
            locale_map.put('es', 'd/MM/yyyy');
            locale_map.put('es_AR', 'dd/MM/yyyy');
            locale_map.put('es_BO', 'dd-MM-yyyy');
            locale_map.put('es_CL', 'dd-MM-yyyy');
            locale_map.put('es_CO', 'd/MM/yyyy');
            locale_map.put('es_CR', 'dd/MM/yyyy');
            locale_map.put('es_EC', 'dd/MM/yyyy');
            locale_map.put('es_ES', 'd/MM/yyyy');
            locale_map.put('es_ES_EURO', 'd/MM/yyyy');
            locale_map.put('es_GT', 'd/MM/yyyy');
            locale_map.put('es_HN', 'MM-dd-yyyy');
            locale_map.put('es_MX', 'd/MM/yyyy');
            locale_map.put('es_PE', 'dd/MM/yyyy');
            locale_map.put('es_PR', 'MM-dd-yyyy');
            locale_map.put('es_PY', 'dd/MM/yyyy');
            locale_map.put('es_SV', 'MM-dd-yyyy');
            locale_map.put('es_UY', 'dd/MM/yyyy');
            locale_map.put('es_VE', 'dd/MM/yyyy');
            locale_map.put('et_EE', 'd.MM.yyyy');
            locale_map.put('fi', 'd.M.yyyy');
            locale_map.put('fi_FI', 'd.M.yyyy');
            locale_map.put('fi_FI_EURO', 'd.M.yyyy');
            locale_map.put('fr', 'dd/MM/yyyy hh:mm:ss Z');
            locale_map.put('fr_BE', 'd/MM/yyyy');
            locale_map.put('fr_CA', 'yyyy-MM-dd');
            locale_map.put('fr_CH', 'dd.MM.yyyy');
            locale_map.put('fr_FR', 'dd/MM/yyyy hh:mm:ss Z');
            locale_map.put('fr_FR_EURO', 'dd/MM/yyyy');
            locale_map.put('fr_LU', 'dd/MM/yyyy');
            locale_map.put('fr_MC', 'dd/MM/yyyy');
            locale_map.put('hr_HR', 'yyyy.MM.dd');
            locale_map.put('hu', 'yyyy.MM.dd');
            locale_map.put('hy_AM', 'M/d/yyyy');
            locale_map.put('is_IS', 'd.M.yyyy');
            locale_map.put('it', 'dd/MM/yyyy');
            locale_map.put('it_CH', 'dd.MM.yyyy');
            locale_map.put('it_IT', 'dd/MM/yyyy');
            locale_map.put('iw', ' dd/MM/yyyy');
            locale_map.put('iw_IL', ' dd/MM/yyyy');
            locale_map.put('ja', 'yyyy/MM/dd');
            locale_map.put('ja_JP', 'yyyy/MM/dd');
            locale_map.put('kk_KZ', 'M/d/yyyy');
            locale_map.put('km_KH', 'M/d/yyyy');
            locale_map.put('ko', 'yyyy. M. d');
            locale_map.put('ko_KR', 'yyyy. M. d');
            locale_map.put('lt_LT', 'yyyy.M.d');
            locale_map.put('lv_LV', 'yyyy.d.M');
            locale_map.put('ms_MY', 'dd/MM/yyyy');
            locale_map.put('nl', 'd-M-yyyy');
            locale_map.put('nl_BE', 'd/MM/yyyy');
            locale_map.put('nl_NL', 'd-M-yyyy');
            locale_map.put('nl_SR', 'd-M-yyyy');
            locale_map.put('no', 'dd.MM.yyyy');
            locale_map.put('no_NO', 'dd.MM.yyyy');
            locale_map.put('pl', 'yyyy-MM-dd');
            locale_map.put('pt', 'dd-MM-yyyy');
            locale_map.put('pt_AO', 'dd-MM-yyyy');
            locale_map.put('pt_BR', 'dd/MM/yyyy');
            locale_map.put('pt_PT', 'dd-MM-yyyy');
            locale_map.put('ro_RO', 'dd.MM.yyyy');
            locale_map.put('ru', 'dd.MM.yyyy');
            locale_map.put('sk_SK', 'd.M.yyyy');
            locale_map.put('sl_SI', 'd.M.y');
            locale_map.put('sv', 'yyyy-MM-dd');
            locale_map.put('sv_SE', 'yyyy-MM-dd');
            locale_map.put('th', 'M/d/yyyy');
            locale_map.put('th_TH', 'd/M/yyyy');
            locale_map.put('tr', 'dd.MM.yyyy hh:mm:ss Z');
            locale_map.put('ur_PK', 'M/d/yyyy');
            locale_map.put('vi_VN', ' dd/MM/yyyy');
            locale_map.put('zh', 'yyyy-M-d');
            locale_map.put('zh_CN', 'yyyy-M-d');
            locale_map.put('zh_HK', 'yyyy-M-d');
            locale_map.put('zh_TW', 'yyyy/M/d');
            return locale_map; //return the map
    }
    
    //This method returns the String format of logged in user's timezone
    public static String userTimeZoneFormat()
    {
        String datetime_format = 'dd/MM/yyyy';
        Map<String, String> mappedValues = new Map<String, String>(); //map for holding locale to datetime format
        mappedValues = mapLocaleValues(); 
        String user_locale = UserInfo.getLocale();
        if (mappedValues.containsKey(user_locale)) 
        { //if the map contains the correct datetime format                    
                datetime_format = mappedValues.get(user_locale); //grab the datetime format for the locale
        }
        return datetime_format;
    }
    
    /*    Modified By ACCENTURE
          Modifed for December Release Business Requirement BR-929
          Modified Date: 31/10/2011
    */
    
    /* This method checks if the logged in User is System Admin/Delegated System Admin/Record Owner
       and returns boolean value accordingly.
    */ 
    public static boolean checkFullAccess(Id recordOwnerId)
    {
        String filterText = '%'+Label.CL00498+'%';
        
        //Queries the profile where profile name contains System Admin
        String query =  CS005_Queries__c.getValues('CS005_2').StaticQuery__c +' where name like \''+filterText+'\'' ;       
        
        //Queries the records
        List<Sobject> sobjs = Database.Query(query);
        List<Profile> profiles = new List<Profile>();
        if(sobjs!=null && !sobjs.isEmpty())
        {
            profiles = (List<Profile>)sobjs;
        }
        Set<String> profId = new Set<String>();
        
        //Adds the profile Ids to the List
        for(Profile profile: profiles)
        {
            profId.add(profile.Id);
        }        
        system.debug('-----profId-----'+profId);
        if((profId.contains(UserInfo.getProfileId())) || (recordOwnerId!=null && (UserInfo.getUserId() == recordOwnerId)))
            return true;
        else
            return false;
    }
    
    /*  This method creates SOQL for child sObjects based on the Source Record Id, Target Record Id and Child Objects
        Clones the child object records and returns the cloned records.   
    */
    public static Map<Sobject, SObject> cloneChildRecords(String sourceRecordId, String targetRecordId, List<Schema.ChildRelationship> childRecords)
    {
        Map<SObject, SObject> srcRecIdClondRec = new Map<SObject, sObject>();        
        for(ChildRelationship child : childRecords) 
        {
            if(sourceRecordId!=null && targetRecordId!=null)
            {
                //Generates query for child object by calling generateSelectAllQuery method
                String query = generateSelectAllQuery(child.getChildSObject().newSObject());
            
                //Adds the Filter Criteria to the returned Query based on Source Record
                query += ' where ' + child.getField() + '= \'' + sourceRecordId+ '\'';
                
                //Query child records on child sObject
                List<SObject> currentRecords = Database.query(query);      
                
                for(Sobject sobj: currentRecords)
                {
                    Sobject clonRec = sobj.clone(false);
                    clonRec.put(child.getField(), targetRecordId);                
                    srcRecIdClondRec.put(sobj, clonRec);
                }
            }
        }
        system.debug('--------cloneChildRecords------'+srcRecIdClondRec);
        return srcRecIdClondRec;         
    }
    
    /* Generate query based for the passed sObject. Verify that fields are accessible, 
       createable, not unique, not auto number, not calculated, not formula
     */
    public static string generateSelectAllQuery(Sobject sobj)
    {
        String query;
        if(sobj!=null)
        {
            query   = 'select Id';
            //Gets the details of the sobject
            Schema.DescribeSObjectResult sobjResult = sobj.getsObjectType().getDescribe();
            
            //Add field names to query
            for(Schema.SObjectField field : sobjResult.fields.getMap().values()) 
            {
                if(field.getDescribe().isAccessible() && field.getDescribe().isCreateable() && !field.getDescribe().isUnique() &&
                !field.getDescribe().isAutoNumber() && !field.getDescribe().isCalculated()) 
                {
                    query += ',' + field.getDescribe().getName();
                }
            }        
            query+= ' from '+ sobjResult.getName();
            system.debug('-------Query-----'+query);
        }
        return query;
     }
     
     /* This method inserts the Sobject records to the database and returns the inserted
       record Ids.
    */
    public static List<Id> insertRecords(List<Sobject> recToBeIns)
    {
        List<Id> insRecIds = new List<Id>();
        if(recToBeIns!=null && !recToBeIns.isEmpty())
        {
            if(recToBeIns.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
            {
                System.Debug('######## Utils_Methods Error Inserting : '+ Label.CL00264);
            }
            else
            {
                Database.SaveResult[] recIns= Database.Insert(recToBeIns, true);
                for(Database.SaveResult sr: recIns)
                {
                    if(!sr.isSuccess())
                        System.Debug('######## Utils_Methods Error Inserting: '+sr.getErrors()[0].getStatusCode()+' '+sr.getErrors()[0].getMessage());                    
                    else
                        insRecIds.add(sr.getId());
                }
            }                         
        }   
        system.debug('------insRecIds------'+insRecIds);
        if(!insRecIds.isEmpty())
            return insRecIds;
        else
            return null; 
    }
/***************************************************************************************************
    Converts an Amount to Euro based on the ISO code of the currecny
***************************************************************************************************/
    public static Double convertCurrencyToEuro(String sourceISOCode , Decimal Amount)
    {
        populateCurrencyMap();
       // DatedConversionRate convAmt = [SELECT ConversionRate,IsoCode,NextStartDate,StartDate FROM DatedConversionRate WHERE IsoCode=:sourceISOCode and startDate<=:startDate and nextStartDate>:startDate];
        Double resultAmount = (Double)(Amount / currencyMap.get(sourceISOCode));
        return resultAmount;
    }
    
/***************************************************************************************************
    Converts an Amount From Euro based on the ISO code of the currecny
***************************************************************************************************/
    public static Double convertCurrencyFromEuro(String resultISOCode , Decimal Amount)
    {
        populateCurrencyMap();
      //  DatedConversionRate convAmt = [SELECT ConversionRate,IsoCode,NextStartDate,StartDate FROM DatedConversionRate WHERE IsoCode=:resultISOCode and startDate<=:startDate and nextStartDate>:startDate];
        Double resultAmount = (Double)(Amount * currencyMap.get(resultISOCode));
        return resultAmount;
    } 
    
    public static void populateCurrencyMap()
    {
        if(currencyMap==null)
        {
            currencyMap=new Map<String,Decimal>();
            for(CurrencyType ct:[SELECT ConversionRate,DecimalPlaces,IsActive,IsoCode FROM CurrencyType WHERE IsActive = true])            
                currencyMap.put(ct.IsoCode,ct.ConversionRate);            
        }
    }
    
    public static Double convertCurrencyToEuroforDatedCurrency(String sourceISOCode , Decimal Amount)
    {
        populateDatedCurrencyMap();
       // DatedConversionRate convAmt = [SELECT ConversionRate,IsoCode,NextStartDate,StartDate FROM DatedConversionRate WHERE IsoCode=:sourceISOCode and startDate<=:startDate and nextStartDate>:startDate];
        Double resultAmount = (Double)(Amount / datedCurrencyMap.get(sourceISOCode));
        return resultAmount;
    }
    
    public static void populateDatedCurrencyMap()
    {
       if(datedCurrencyMap==null)
        {
            datedCurrencyMap=new Map<String,Decimal>();
            for(DatedConversionRate ct:[SELECT ConversionRate,IsoCode FROM DatedConversionRate WHERE startDate<=:System.Today() and nextStartDate>:System.Today()])            
                datedCurrencyMap.put(ct.IsoCode,ct.ConversionRate);            
        }
    }
    
    public static List<User> filterUsers(List<User> usersList,Set<String> permissionsetnames,String currentprofileIdString,String oldProfileIdString)
    {
        Id currentProfileId=Id.valueOf(currentprofileIdString);   
        Id oldProfileId=Id.valueOf(oldProfileIdString);
        List<User> newUsersList=new List<User>();
        permissionsetnames.removeAll(new List<String>{null});
        for(User tmpUser:usersList)
        {
            if(tmpUser.ProfileId==oldProfileId)
                newUsersList.add(tmpUser);
            else if(tmpUser.ProfileId==currentProfileId)
            {
                Set<String> tempSet=new Set<String>();
                tempSet.addAll(tmpUser.Permission_sets_assigned__c.split(','));
                if(tempSet.containsAll(permissionsetnames))
                    newUsersList.add(tmpUser);
            }
        }
        return  newUsersList;
    }
        public static String getConvertDateTimeFormat(string strDT){
        //Accepts input as "EEE, d MMM yyyy HH:mm:ss Z" and returns as “yyyy-MM-dd HH:mm:ss”
        Map<string,integer> MapMonthList = new Map<string,integer>();
        MapMonthList.put('Jan',1);
        MapMonthList.put('Feb',2);
        MapMonthList.put('Mar',3);
        MapMonthList.put('Apr',4);
        MapMonthList.put('May',5);
        MapMonthList.put('Jun',6);
        MapMonthList.put('Jul',7);
        MapMonthList.put('Aug',8);
        MapMonthList.put('Sep',9);
        MapMonthList.put('Oct',10);
        MapMonthList.put('Nov',11);
        MapMonthList.put('Dec',12);

        try{    
        //strDT = strDT.replace(' +','+');
        String[] strDTDivided = strDT.split(' ',0);
        
        string month = String.ValueOf(MapMonthList.get(strDTDivided.get(2)));
        string day = strDTDivided.get(1);
        string year = strDTDivided.get(3);
        if(year.length()==2) // Changed as per BR-4029 (EUSR-027324)
            year = '20'+ year;
        //string strTime = strDTDivided.get(4).replace('+',' +');
        string strTime = strDTDivided.get(4);
        string strTimeZone = strDTDivided.get(5);
        //“yyyy-MM-dd HH:mm:ss”
        //string stringDate = year + '-' + month + '-' + day + ' ' + strTime;
        string stringDate = year + '-' + month + '-' + day + ' ' + strTime + ' ' + strTimeZone;
        return stringDate;
        }
        catch(Exception e){
            return null;
        }       
    }
    //Method to add Feed Post to a Record for User.   
    // Added By Vimal K (GD India) For April 2014 Release
    
    public static void postFeed(Id recordId, Id userId, String strFeedItem){
        String strCommunityId = null;
        //ConnectApi.FeedType objFeedType = ConnectApi.FeedType.UserProfile;
        ConnectApi.FeedType objFeedType                 = ConnectApi.FeedType.Record;
        
        ConnectApi.FeedItemInput objFeedItemInput       = new ConnectApi.FeedItemInput();
        ConnectApi.MessageBodyInput objMessageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput objTextSegmentInput;
        ConnectApi.MentionSegmentInput objMentionSegmentInput = new ConnectApi.MentionSegmentInput();

        objMessageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        objTextSegmentInput = new ConnectApi.TextSegmentInput();
        objTextSegmentInput.text = strFeedItem;
        objMessageBodyInput.messageSegments.add(objTextSegmentInput);

        objMentionSegmentInput.id = userId;
        objMessageBodyInput.messageSegments.add(objMentionSegmentInput);
        
        /*
        objTextSegmentInput = new ConnectApi.TextSegmentInput();
        objTextSegmentInput.text = strFeedItem;
        objMessageBodyInput.messageSegments.add(objTextSegmentInput);
        */
        
        objFeedItemInput.body = objMessageBodyInput;
        if(!Test.IsRunningTest())
        ConnectApi.FeedItem objFeedItem = ConnectApi.ChatterFeeds.postFeedItem(strCommunityId, objFeedType, recordId, objFeedItemInput, null);      
    }
}