@isTest
public class ConnectProgramScopeListController_TEST {

    static testMethod void ConnectProgramScopeTest() {
        
         User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connect7');
        System.runAs(runAsUser){
            Initiatives__c inti=Utils_TestMethods_Connect.createInitiative();
            insert inti;
            Project_NCP__c cp1=new Project_NCP__c(Program__c=inti.id,Name='cp',Global_Business__c='ITB;Partner-Division;'+Label.Connect_Smart_Cities,Global_Functions__c='GSC-Regional;GF',Power_Region__c='EAS;PR',Smart_Cities_Division__c='test',GSC_Regions__c='GSC',Partner_Region__c='test');
            insert cp1;
            Champions__c Champ=new Champions__c(Scope__c='Global Functions',Entity__c='GM');
            insert champ;
            Champions__c Champ1=new Champions__c(Scope__c='Global Business',Entity__c='ITB');
            insert champ1;
            Team_Members__c Teammembers=new Team_Members__c(Program__c=cp1.id,Champion_Name__c=champ.id,Team_Name__c='Global Functions', Entity__c='GF',GSC_Region__c='GSC',Team_Member__c=Label.Connect_Email);
            insert Teammembers;
            Team_Members__c Teammembers1=new Team_Members__c(Program__c=cp1.id,Champion_Name__c=champ1.id,Team_Name__c='Global Business', Entity__c='ITB',Team_Member__c=Label.Connect_Email);
            insert Teammembers1;
            Team_Members__c Teammembers4=new Team_Members__c(Program__c=cp1.id,Champion_Name__c=champ1.id,Team_Name__c='Global Business', Entity__c='Partner-Division',Partner_Region__c='test',Team_Member__c=Label.Connect_Email);
            insert Teammembers4;
            Team_Members__c Teammembers5=new Team_Members__c(Program__c=cp1.id,Champion_Name__c=champ1.id,Team_Name__c='Global Business', Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test',Team_Member__c=Label.Connect_Email);
            insert Teammembers5;
            Team_Members__c Teammembers2=new Team_Members__c(Program__c=cp1.id,Champion_Name__c=champ1.id,Team_Name__c='Power Region', Entity__c='PR',Team_Member__c=Label.Connect_Email);
            insert Teammembers2;
            Team_Members__c Teammembers3=new Team_Members__c(Program__c=cp1.id,Champion_Name__c=champ1.id,Team_Name__c='Global Businesss', Entity__c='ITB',Team_Member__c=Label.Connect_Email);
            insert Teammembers3;
    
            //Create the Instance of the Page and Set that Page as the page in current operation.
            PageReference vfPage = Page.Connect_Program_Network;
            Test.setCurrentPage(vfPage);
            
            //Add parameters to page URL
            ApexPages.currentPage().getParameters().put('Id',cp1.id);
     
            //Create the Instance of the Controller and call its methods.
            ConnectProgramScopeListController PN = new ConnectProgramScopeListController(new ApexPages.StandardController(cp1));
            Test.startTest();
            
            //Call the Controller methods with the newly created Global Program.
            PN.getPageid();       
            PN.getProgramNetwork();
            PN.getprogram();
            
            //Update the created Global Program to meet alternate condition.
            cp1.Global_Functions__c='GM';
            cp1.GSC_Regions__c='';
            delete Teammembers;
            update cp1;
            
            //Call the Controller methods with the updated Global Program.
            PN.getPageid();       
            PN.getProgramNetwork(); 
            
            //Create a New Global Program to meet alternate condition.
            Project_NCP__c cp2=new Project_NCP__c(Program__c=inti.id,Name='cp',Global_Business__c='ITB',Global_Functions__c='GSC-regional;GF',Power_Region__c='EAS;PR',GSC_Regions__c='GSC');
            insert cp2;
            
            //Add parameters to page URL
            ApexPages.currentPage().getParameters().put('Id',cp2.id);
            
            //Call the Controller methods with the newly created Global Program.
            PN.getPageid();       
            PN.getProgramNetwork(); 
              
            Test.stopTest();
        }
   }
}