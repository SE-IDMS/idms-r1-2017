@isTest //(SeeAllData=true)
public with sharing class ZuoraSubscriptionTriggerTest{

    private static testMethod void testOpportunityAmountUpdateTrigggerMethod(){
        
        Zuora__Subscription__c sub = new Zuora__Subscription__c(Asset_SKU__c = '123', Asset_SN__c='AAA123');
        insert sub;

        sub.Zuora__ServiceActivationDate__c =  Date.today();
        update sub;
        
        Zuora__SubscriptionProductCharge__c spc2 = new Zuora__SubscriptionProductCharge__c(Zuora__Subscription__c = sub.id);
        insert spc2;
        
    }
}