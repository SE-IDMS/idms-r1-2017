public with sharing class ConnectInitiativeHealthByEntity_Cont {

    public ConnectInitiativeHealthByEntity_Cont(ApexPages.StandardController controller) {
    
    Quarter = Label.Connect_Quarter;
    RenPDF = ''; 
    if (EntityList == null)
        EntityList = 'Global'; 
        Year = ApexPages.CurrentPage().getParameters().get('Year');
    if(Year == null)
        Year = Label.ConnectYear;
    RetriveHealthStatus();
      
       }
       
 public string getYear(){
   return Year ;
 }

public pagereference YearChange(){
     if(Year != '2012'){
     PageReference pageRef = new PageReference('/apex/Connect_Initiative_Health_By_Entity2013?Year=Year');
     return (pageRef);
     }
     else{
       PageReference pageRef2012 = new PageReference('/apex/Connect_Initiative_Health_By_Entity?Year=2012');
       return (pageRef2012); 
       }  
    }

 public pagereference QuarterChange(){
     
     return null;
    
    }
    
public string getQuarter(){
 return Quarter ;
 }
 
 public string getEntityList(){
 return EntityList ;
 }

    
public pagereference EntityChange(){
     EntityHealthStatus();
     return null;
    
    }
    
 // Declaration Section
 
 List<ProgramLst> PE = new List<ProgramLst>();
 List<ProgramLst> EW = new List<ProgramLst>();
 List<ProgramLst> EL = new List<ProgramLst>();
 List<ProgramLst> GC = new List<ProgramLst>();
 List<ProgramLst> GCI = new List<ProgramLst>();
 List<ProgramLst> GE = new List<ProgramLst>();
 List<ProgramLst> OE = new List<ProgramLst>();
 List<ProgramLst> OM = new List<ProgramLst>();
 List<ProgramLst> RE = new List<ProgramLst>();
 List<ProgramLst> SE = new List<ProgramLst>();
 List<ProgramLst> TSC = new List<ProgramLst>();
 List<ProgramLst> EI = new List<ProgramLst>();

 public string Quarter {get; set;}
 public string EntityList{get;set;}
 public string RenPDF{get; set;}
 public string Year {get; set;}

 
 //List<Connect_Entity_Progress__c> Entity = [Select Entity__c,Q1_Entity_Health_Status__c,Q2_Entity_Health_Status__c,Q3_Entity_Health_Status__c,Q4_Entity_Health_Status__c,Initiative_Name__c from Connect_Entity_Progress__c where Entity__c = :EntityList];
 
 
 public List<SelectOption> getFilterQuarter() {
      List<SelectOption> options = new List<SelectOption>();
       options.add(new SelectOption('Q1','Q1'));  
       options.add(new SelectOption('Q2','Q2'));     
       options.add(new SelectOption('Q3','Q3'));  
       options.add(new SelectOption('Q4','Q4'));       
       return options;
  }
  
public List<SelectOption> getFilterList() {
       List<SelectOption> options = new List<SelectOption>();
       options.add(new SelectOption('2012','2012'));  
       options.add(new SelectOption('2013','2013')); 
       options.add(new SelectOption('2014','2014')); 
             
       return options;
   }
   
public List<SelectOption> getEntityFilter() {
      List<SelectOption> options = new List<SelectOption>();
       options.add(new SelectOption('Global','Global'));
       options.add(new SelectOption('Building','Building'));
       options.add(new SelectOption('China','China Operations'));
       options.add(new SelectOption('Finance','Finance'));   
       options.add(new SelectOption('GM','GM'));     
       options.add(new SelectOption('GSC-Global','GSC-Global')); 
       options.add(new SelectOption('Global Operations','Global Operations'));
       options.add(new SelectOption('HR','HR')); 
       options.add(new SelectOption('Industry','Industry'));     
       options.add(new SelectOption('Infrastructure','Infrastructure'));  
       options.add(new SelectOption('ITB','ITB'));
       options.add(new SelectOption('IPO','IPO'));   
       options.add(new SelectOption('LSBE','Partner Sub-Entity LSBE'));   
       options.add(new SelectOption('LSBA','Partner Sub-Entity LSBA'));
       options.add(new SelectOption('North America','North America Operations'));      
       options.add(new SelectOption('Power','Partner Sub-Entity Power'));        
       options.add(new SelectOption('S&I','S&I'));
        
       
       
       // 2013 options
       
        List<SelectOption> options2013 = new List<SelectOption>();
       options2013.add(new SelectOption('Global','Global'));
       options2013.add(new SelectOption('China','China Operations')); 
       options2013.add(new SelectOption('Buildings','Cities - Buildings'));   
       options2013.add(new SelectOption('Energy','Cities - Energy'));  
       options2013.add(new SelectOption('Professional Services','Cities - Professional Services')); 
       options2013.add(new SelectOption('Smart Infrastructure','Cities - Smart Infrastructure')); 
       options2013.add(new SelectOption('Finance','Finance')); 
       options2013.add(new SelectOption('GM','GM'));
       options2013.add(new SelectOption('Global Operations','Global Operations'));     
       options2013.add(new SelectOption('GSC-Global','GSC-Global'));  
       //options2013.add(new SelectOption('GSC China','GSC-Regional China'));    
       //options2013.add(new SelectOption('EAJP','GSC-Regional EAJP'));  
       //options2013.add(new SelectOption('EMEA Products','GSC-Regional EMEA Products'));  
       //options2013.add(new SelectOption('EMEA Lifespace','GSC-Regional EMEA Lifespace'));  
       //options2013.add(new SelectOption('EMEA Equipment','GSC-Regional EMEA Equipment'));     
       //options2013.add(new SelectOption('India','GSC-Regional India'));  
       //options2013.add(new SelectOption('GSC North America','GSC-Regional North America'));  
       //options2013.add(new SelectOption('South America','GSC-Regional South America'));        
       options2013.add(new SelectOption('HR','HR')); 
       options2013.add(new SelectOption('IPO','IPO')); 
       options2013.add(new SelectOption('Industry','Industry'));     
       options2013.add(new SelectOption('IT','IT'));  
       options2013.add(new SelectOption('North America','North America Operations'));      
       options2013.add(new SelectOption('Partner','Partner'));  
       //options2013.add(new SelectOption('Eco Business','Partner Division Eco Business'));
       //options2013.add(new SelectOption('Partner Projects','Partner Division Partner Projects'));
       //options2013.add(new SelectOption('Retail','Partner Division Retail'));
        options2013.add(new SelectOption('Strategy','Strategy'));
       
      // 2014 options
         
        List<SelectOption> options2014 = new List<SelectOption>();
       options2014.add(new SelectOption('Global','Global'));
       options2014.add(new SelectOption('China','China Operations')); 
       options2014.add(new SelectOption('Energy','Energy')); 
       options2014.add(new SelectOption('Finance','Finance')); 
       options2014.add(new SelectOption('Global Operations','Global Operations'));   
       options2014.add(new SelectOption('Global Solutions','Global Solutions'));   
       options2014.add(new SelectOption('GM','GM'));        
       options2014.add(new SelectOption('GSC-Global','GSC-Global'));  
       //options2014.add(new SelectOption('GSC China','GSC-Regional China'));    
       //options2014.add(new SelectOption('EAJP','GSC-Regional EAJP'));  
       //options2014.add(new SelectOption('EMEA Products','GSC-Regional EMEA Products'));  
       //options2014.add(new SelectOption('EMEA Lifespace','GSC-Regional EMEA Lifespace'));  
       //options2014.add(new SelectOption('EMEA Equipment','GSC-Regional EMEA Equipment'));     
       //options2014.add(new SelectOption('India','GSC-Regional India'));  
       //options2014.add(new SelectOption('GSC North America','GSC-Regional North America'));  
       //options2014.add(new SelectOption('South America','GSC-Regional South America'));        
       options2014.add(new SelectOption('HR','HR')); 
       options2014.add(new SelectOption('IPO','IPO')); 
       options2014.add(new SelectOption('Industry','Industry'));     
       options2014.add(new SelectOption('IT','IT'));  
       options2014.add(new SelectOption('North America','North America Operations'));      
       options2014.add(new SelectOption('Partner','Partner'));  
      // options2014.add(new SelectOption('Eco Business','Partner Division Eco Business'));
      // options2014.add(new SelectOption('Partner Projects','Partner Division Partner Projects'));
      // options2014.add(new SelectOption('Retail','Partner Division Retail'));
      // options2014.add(new SelectOption('Building Division','Partner Division Building'));
       options2014.add(new SelectOption('Strategy','Strategy'));
       
       if(Year == '2012')
        return options;
       else  if(Year == '2013')
         return options2013;
       else
         return options2014;
  }
  // Get Program Health Status for All Entities
  
Public void RetriveHealthStatus(){
List<Project_NCP__c> Prg = [Select id, Name, Program__r.Name,Program_Health_Status_by_Sponsor__c,Program_Health_Status_by_Sponsor_Q2__c,Program_Health_Status_by_Sponsor_Q3__c,Program_Health_Status_by_Sponsor_Q4__c from Project_NCP__c where Year__c = :Year and Visible__c = :'True'];
 PE = new List<ProgramLst>();
 EW = new List<ProgramLst>();
 EL = new List<ProgramLst>();
 GC = new List<ProgramLst>();
 GCI = new List<ProgramLst>();
 GE = new List<ProgramLst>();
 OE = new List<ProgramLst>();
 OM = new List<ProgramLst>();
 RE = new List<ProgramLst>();
 SE = new List<ProgramLst>();
 TSC = new List<ProgramLst>();
 EI = new List<ProgramLst>();

string TrafficLight;

For(Project_NCP__c p: Prg){
ProgramLst ini1 = new ProgramLst();
ProgramLst ini2 = new ProgramLst();
ProgramLst ini3 = new ProgramLst();
ProgramLst ini4 = new ProgramLst();
ProgramLst ini5 = new ProgramLst();
ProgramLst ini6 = new ProgramLst();
ProgramLst ini7 = new ProgramLst();
ProgramLst ini8 = new ProgramLst();
ProgramLst ini9 = new ProgramLst();
ProgramLst ini10 = new ProgramLst();
ProgramLst ini11 = new ProgramLst();
ProgramLst ini12 = new ProgramLst();


// Traffic Light for Q1

if(Quarter == 'Q1'){
 if(p.Program_Health_Status_by_Sponsor__c == Label.ConnectStatusOnTimeDelivered)
   TrafficLight = '/resource/Connect_Green';
 else 
  if(p.Program_Health_Status_by_Sponsor__c == Label.ConnectStatusSlightDelay)
    TrafficLight = '/resource/Connect_Yellow';
 else 
  if(p.Program_Health_Status_by_Sponsor__c == Label.ConnectStatusNoData)
    TrafficLight = '/resource/Connect_Grey';
    else 
  if(p.Program_Health_Status_by_Sponsor__c == Label.ConnectStatusNoProgressorDelayedover1Month)
    TrafficLight = '/resource/Connect_Red';
    }
    
// Traffic Light for Q2

if(Quarter == 'Q2'){
 if(p.Program_Health_Status_by_Sponsor_Q2__c == Label.ConnectStatusOnTimeDelivered)
   TrafficLight = '/resource/Connect_Green';
 else 
  if(p.Program_Health_Status_by_Sponsor_Q2__c == Label.ConnectStatusSlightDelay)
    TrafficLight = '/resource/Connect_Yellow';
 else 
  if(p.Program_Health_Status_by_Sponsor_Q2__c == Label.ConnectStatusNoData)
    TrafficLight = '/resource/Connect_Grey';
     else 
  if(p.Program_Health_Status_by_Sponsor_Q2__c == Label.ConnectStatusNoProgressorDelayedover1Month)
    TrafficLight = '/resource/Connect_Red';
 
   }

 // Traffic Light for Q3
 
 if(Quarter == 'Q3'){
 if(p.Program_Health_Status_by_Sponsor_Q3__c == Label.ConnectStatusOnTimeDelivered)
   TrafficLight = '/resource/Connect_Green';
 else 
  if(p.Program_Health_Status_by_Sponsor_Q3__c == Label.ConnectStatusSlightDelay)
    TrafficLight = '/resource/Connect_Yellow';
 else 
  if(p.Program_Health_Status_by_Sponsor_Q3__c == Label.ConnectStatusNoData)
    TrafficLight = '/resource/Connect_Grey';
  else
    if(p.Program_Health_Status_by_Sponsor_Q3__c == Label.ConnectStatusNoProgressorDelayedover1Month)
    TrafficLight = '/resource/Connect_Red';
    }

if(Quarter == 'Q4'){
 if(p.Program_Health_Status_by_Sponsor_Q4__c == Label.ConnectStatusOnTimeDelivered)
   TrafficLight = '/resource/Connect_Green';
 else 
  if(p.Program_Health_Status_by_Sponsor_Q4__c == Label.ConnectStatusSlightDelay)
    TrafficLight = '/resource/Connect_Yellow';
 else 
  if(p.Program_Health_Status_by_Sponsor_Q4__c == Label.ConnectStatusNoData)
    TrafficLight = '/resource/Connect_Grey';
     else
    if(p.Program_Health_Status_by_Sponsor_Q4__c == Label.ConnectStatusNoProgressorDelayedover1Month)
    TrafficLight = '/resource/Connect_Red';
 
   }


  if (p.Program__r.Name == Label.ConnectInitPE){
     system.debug(' Program ******** ' + p.Name);
     ini1.Initname = p.Program__r.Name;
     ini1.prgname = p.Name;
     ini1.prghealth = TrafficLight;
     ini1.prgid = p.id;
      PE.add(ini1);
     } // If Ends
     
     if (p.Program__r.Name == Label.ConnectinitEW){
     system.debug(' Program ******** ' + p.Name);
     ini2.Initname = p.Program__r.Name;
     ini2.prgname = p.Name;
     ini2.prghealth = TrafficLight;
     ini2.prgid = p.id;
      EW.add(ini2);
     } // If Ends
     
      if (p.Program__r.Name == Label.ConnectInitEI){
     system.debug(' Program ******** ' + p.Name);
     ini3.Initname = p.Program__r.Name;
     ini3.prgname = p.Name;
     ini3.prghealth = TrafficLight;
     ini3.prgid = p.id;
      EI.add(ini3);
     } // If Ends
     
       
     if (p.Program__r.Name == Label.ConnectInitEL){
     system.debug(' Program ******** ' + p.Name);
     ini4.Initname = p.Program__r.Name;
     ini4.prgname = p.Name;
     ini4.prghealth = TrafficLight;
     ini4.prgid = p.id;
      EL.add(ini4);
     } // If Ends
     
     if (p.Program__r.Name == Label.ConnectInitGC){
     system.debug(' Program ******** ' + p.Name);
     ini5.Initname = p.Program__r.Name;
     ini5.prgname = p.Name;
     ini5.prghealth = TrafficLight;
     ini5.prgid = p.id;
      GC.add(ini5);
     } // If Ends
     
     if (p.Program__r.Name == Label.ConnectInitGCI){
     system.debug(' Program ******** ' + p.Name);
     ini6.Initname = p.Program__r.Name;
     ini6.prgname = p.Name;
     ini6.prghealth = TrafficLight;
     ini6.prgid = p.id;
      GCI.add(ini6);
     } // If Ends
     
     if (p.Program__r.Name == Label.ConnectInitGE){
     system.debug(' Program ******** ' + p.Name);
     ini7.Initname = p.Program__r.Name;
     ini7.prgname = p.Name;
     ini7.prghealth = TrafficLight;
     ini7.prgid = p.id;
      GE.add(ini7);
     } // If Ends
     
     if (p.Program__r.Name == Label.ConnectInitOE){
     system.debug(' Program ******** ' + p.Name);
     ini8.Initname = p.Program__r.Name;
     ini8.prgname = p.Name;
     ini8.prghealth = TrafficLight;
     ini8.prgid = p.id;
      OE.add(ini8);
     } // If Ends
     
     if (p.Program__r.Name == Label.ConnectInitOM){
     system.debug(' Program ******** ' + p.Name);
     ini9.Initname = p.Program__r.Name;
     ini9.prgname = p.Name;
     ini9.prghealth = TrafficLight;
     ini9.prgid = p.id;
      OM.add(ini9);
     } // If Ends
     
     if (p.Program__r.Name == Label.ConnectInitRE){
     system.debug(' Program ******** ' + p.Name);
     ini10.Initname = p.Program__r.Name;
     ini10.prgname = p.Name;
     ini10.prghealth = TrafficLight;
     ini10.prgid = p.id;
      RE.add(ini10);
     } // If Ends
     
     if (p.Program__r.Name == Label.ConnectInitSE){
     system.debug(' Program ******** ' + p.Name);
     ini11.Initname = p.Program__r.Name;
     ini11.prgname = p.Name;
     ini11.prghealth = TrafficLight;
     ini11.prgid = p.id;
      SE.add(ini11);
     } // If Ends
     
      if (p.Program__r.Name == Label.ConnectInitTSC){
     system.debug(' Program ******** ' + p.Name);
     ini12.Initname = p.Program__r.Name;
     ini12.prgname = p.Name;
     ini12.prghealth = TrafficLight;
     ini12.prgid = p.id;
      TSC.add(ini12);
     } // If Ends
       
   } // for Ends  

}

// Initiative Health Status for Entities

Public void EntityHealthStatus(){

string TrafficLight;

 PE = new List<ProgramLst>();
 EW = new List<ProgramLst>();
 EL = new List<ProgramLst>();
 GC = new List<ProgramLst>();
 GCI = new List<ProgramLst>();
 GE = new List<ProgramLst>();
 OE = new List<ProgramLst>();
 OM = new List<ProgramLst>();
 RE = new List<ProgramLst>();
 SE = new List<ProgramLst>();
 TSC = new List<ProgramLst>();
 EI = new List<ProgramLst>();

List<Connect_Entity_Progress__c> Ent = new List<Connect_Entity_Progress__c>();


if (EntityList.Contains('LSBE') || EntityList.Contains('LSBA') || EntityList.Contains('Power') )
    
    Ent = [Select Entity__c,Q1_Entity_Health_Status__c,Q2_Entity_Health_Status__c,Q3_Entity_Health_Status__c,Q4_Entity_Health_Status__c,Initiative_Name__c,Program_Name__c from Connect_Entity_Progress__c where Partner_Region__c = :EntityList and Report_Year__c = :Year and Visible__c = :'True'];
 
else if (EntityList.Contains('Eco Business') || EntityList.Contains('Retail') || EntityList.Contains('Partner Projects'))
    
    Ent = [Select Entity__c,Q1_Entity_Health_Status__c,Q2_Entity_Health_Status__c,Q3_Entity_Health_Status__c,Q4_Entity_Health_Status__c,Initiative_Name__c,Program_Name__c from Connect_Entity_Progress__c where Partner_Region__c = :EntityList and Report_Year__c = :Year and Visible__c = :'True'];
 
else if (EntityList.Contains('EAJP') || EntityList.Contains('India') || EntityList.Contains('South America')|| EntityList.Contains('EMEA Products')|| EntityList.Contains('EMEA Lifespace')|| EntityList.Contains('EMEA Equipment') )
    Ent = [Select Entity__c,Q1_Entity_Health_Status__c,Q2_Entity_Health_Status__c,Q3_Entity_Health_Status__c,Q4_Entity_Health_Status__c,Initiative_Name__c,Program_Name__c from Connect_Entity_Progress__c where GSC_Region__c = :EntityList and Report_Year__c = :Year and Visible__c = :'True'];
    
else if (EntityList.Contains('Buildings') || EntityList.Contains('Smart Infrastructure') || EntityList.Contains('Professional Services')|| EntityList.Contains('Energy') && Year == '2013')
   
    Ent = [Select Entity__c,Q1_Entity_Health_Status__c,Q2_Entity_Health_Status__c,Q3_Entity_Health_Status__c,Q4_Entity_Health_Status__c,Initiative_Name__c,Program_Name__c from Connect_Entity_Progress__c where Smart_Cities_Division__c = :EntityList and Report_Year__c = :Year and Visible__c = :'True'];


else if (EntityList.Contains('GSC North America'))
    Ent = [Select Entity__c,Q1_Entity_Health_Status__c,Q2_Entity_Health_Status__c,Q3_Entity_Health_Status__c,Q4_Entity_Health_Status__c,Initiative_Name__c,Program_Name__c from Connect_Entity_Progress__c where GSC_Region__c = :'North America' and Report_Year__c = :Year and Visible__c = :'True'];

else if (EntityList.Contains('GSC China'))
    Ent = [Select Entity__c,Q1_Entity_Health_Status__c,Q2_Entity_Health_Status__c,Q3_Entity_Health_Status__c,Q4_Entity_Health_Status__c,Initiative_Name__c,Program_Name__c from Connect_Entity_Progress__c where GSC_Region__c = :'China' and Report_Year__c = :Year and Visible__c = :'True'];
    
else  
    Ent = [Select Entity__c,Q1_Entity_Health_Status__c,Q2_Entity_Health_Status__c,Q3_Entity_Health_Status__c,Q4_Entity_Health_Status__c,Initiative_Name__c,Program_Name__c from Connect_Entity_Progress__c where Entity__c = :EntityList and Report_Year__c = :Year and Visible__c = :'True'];
   

For(Connect_Entity_Progress__c e: Ent)
{
ProgramLst ini1 = new ProgramLst();
ProgramLst ini2 = new ProgramLst();
ProgramLst ini3 = new ProgramLst();
ProgramLst ini4 = new ProgramLst();
ProgramLst ini5 = new ProgramLst();
ProgramLst ini6 = new ProgramLst();
ProgramLst ini7 = new ProgramLst();
ProgramLst ini8 = new ProgramLst();
ProgramLst ini9 = new ProgramLst();
ProgramLst ini10 = new ProgramLst();
ProgramLst ini11 = new ProgramLst();
ProgramLst ini12 = new ProgramLst();

system.debug('Entity Size ...' + Ent.size());
// Traffic Light for Q1

if(Quarter == 'Q1'){
 if(e.Q1_Entity_Health_Status__c == Label.ConnectStatusOnTimeDelivered)
   TrafficLight = '/resource/Connect_Green';
 else 
  if(e.Q1_Entity_Health_Status__c == Label.ConnectStatusSlightDelay)
    TrafficLight = '/resource/Connect_Yellow';
 else 
  if(e.Q1_Entity_Health_Status__c == Label.ConnectStatusNoData)
    TrafficLight = '/resource/Connect_Grey';
    else 
  if(e.Q1_Entity_Health_Status__c == Label.ConnectStatusNoProgressorDelayedover1Month)
    TrafficLight = '/resource/Connect_Red';
    }
    
// Traffic Light for Q2

if(Quarter == 'Q2'){
 if(e.Q2_Entity_Health_Status__c == Label.ConnectStatusOnTimeDelivered)
   TrafficLight = '/resource/Connect_Green';
 else 
  if(e.Q2_Entity_Health_Status__c == Label.ConnectStatusSlightDelay)
    TrafficLight = '/resource/Connect_Yellow';
 else 
  if(e.Q2_Entity_Health_Status__c == Label.ConnectStatusNoData)
    TrafficLight = '/resource/Connect_Grey';
     else 
  if(e.Q2_Entity_Health_Status__c == Label.ConnectStatusNoProgressorDelayedover1Month)
    TrafficLight = '/resource/Connect_Red';
 
   }

 // Traffic Light for Q3
 
 if(Quarter == 'Q3'){
 if(e.Q3_Entity_Health_Status__c == Label.ConnectStatusOnTimeDelivered)
   TrafficLight = '/resource/Connect_Green';
 else 
  if(e.Q3_Entity_Health_Status__c == Label.ConnectStatusSlightDelay)
    TrafficLight = '/resource/Connect_Yellow';
 else 
  if(e.Q3_Entity_Health_Status__c == Label.ConnectStatusNoData)
    TrafficLight = '/resource/Connect_Grey';
    else
    if(e.Q3_Entity_Health_Status__c == Label.ConnectStatusNoProgressorDelayedover1Month)
    TrafficLight = '/resource/Connect_Red';
    }

if(Quarter == 'Q4'){
 if(e.Q4_Entity_Health_Status__c == Label.ConnectStatusOnTimeDelivered)
   TrafficLight = '/resource/Connect_Green';
 else 
  if(e.Q4_Entity_Health_Status__c == Label.ConnectStatusSlightDelay)
    TrafficLight = '/resource/Connect_Yellow';
 else 
  if(e.Q4_Entity_Health_Status__c == Label.ConnectStatusNoData)
    TrafficLight = '/resource/Connect_Grey';
     else
    if(e.Q4_Entity_Health_Status__c == Label.ConnectStatusNoProgressorDelayedover1Month)
    TrafficLight = '/resource/Connect_Red';
 
   }


  if (e.Initiative_Name__c == Label.ConnectInitPE){
     system.debug(' Program ******** ' + e.Program_Name__c);
     ini1.Initname = e.Initiative_Name__c;
     ini1.prgname = e.Program_Name__c;
     ini1.prghealth = TrafficLight;
      PE.add(ini1);
     } // If Ends
     
     if (e.Initiative_Name__c == Label.ConnectinitEW){
     system.debug(' Program ******** ' + e.Program_Name__c);
     ini2.Initname = e.Initiative_Name__c;
     ini2.prgname = e.Program_Name__c;
     ini2.prghealth = TrafficLight;
     system.debug(' Traffic Light ------' + TrafficLight);
      EW.add(ini2);
     } // If Ends
     
      if (e.Initiative_Name__c == Label.ConnectInitEI){
     system.debug(' Program ******** ' + e.Initiative_Name__c);
     ini3.Initname = e.Initiative_Name__c;
     ini3.prgname = e.Program_Name__c;
     ini3.prghealth = TrafficLight;
      EI.add(ini3);
     } // If Ends
     
       
     if (e.Initiative_Name__c == Label.ConnectInitEL){
     system.debug(' Program ******** ' + e.Initiative_Name__c);
     ini4.Initname = e.Initiative_Name__c;
     ini4.prgname = e.Program_Name__c;
     ini4.prghealth = TrafficLight;
      EL.add(ini4);
     } // If Ends
     
     if (e.Initiative_Name__c == Label.ConnectInitGC){
     system.debug(' Program ******** ' + e.Initiative_Name__c);
     ini5.Initname = e.Initiative_Name__c;
     ini5.prgname = e.Program_Name__c;
     ini5.prghealth = TrafficLight;
      GC.add(ini5);
     } // If Ends
     
     if (e.Initiative_Name__c == Label.ConnectInitGCI){
     system.debug(' Program ******** ' + e.Initiative_Name__c);
     ini6.Initname = e.Initiative_Name__c;
     ini6.prgname = e.Program_Name__c;
     ini6.prghealth = TrafficLight;
      GCI.add(ini6);
     } // If Ends
     
     if (e.Initiative_Name__c == Label.ConnectInitGE){
     system.debug(' Program ******** ' + e.Initiative_Name__c);
     ini7.Initname = e.Initiative_Name__c;
     ini7.prgname = e.Program_Name__c;
     ini7.prghealth = TrafficLight;
      GE.add(ini7);
     } // If Ends
     
     if (e.Initiative_Name__c == Label.ConnectInitOE){
     system.debug(' Program ******** ' + e.Initiative_Name__c);
     ini8.Initname = e.Initiative_Name__c;
     ini8.prgname = e.Program_Name__c;
     ini8.prghealth = TrafficLight;
      OE.add(ini8);
     } // If Ends
     
     if (e.Initiative_Name__c == Label.ConnectInitOM){
     system.debug(' Program ******** ' + e.Initiative_Name__c);
     ini9.Initname = e.Initiative_Name__c;
     ini9.prgname = e.Program_Name__c;
     ini9.prghealth = TrafficLight;
      OM.add(ini9);
     } // If Ends
     
     if (e.Initiative_Name__c == Label.ConnectInitRE){
     system.debug(' Program ******** ' + e.Initiative_Name__c);
     ini10.Initname = e.Initiative_Name__c;
     ini10.prgname = e.Program_Name__c;
     ini10.prghealth = TrafficLight;
      RE.add(ini10);
     } // If Ends
     
     if (e.Initiative_Name__c == Label.ConnectInitSE){
     system.debug(' Program ******** ' + e.Initiative_Name__c);
     ini11.Initname = e.Initiative_Name__c;
     ini11.prgname = e.Program_Name__c;
     ini11.prghealth = TrafficLight;
      SE.add(ini11);
     } // If Ends
     
      if (e.Initiative_Name__c == Label.ConnectInitTSC){
     system.debug(' Program ******** ' + e.Initiative_Name__c);
     ini12.Initname = e.Initiative_Name__c;
     ini12.prgname = e.Program_Name__c;
     ini12.prghealth = TrafficLight;
      TSC.add(ini12);
     } // If Ends
       
   } // for Ends  

}

 
Public Class ProgramLst{
 public string Initname{get;set;}
 public string prgname{get;set;}
 public string prghealth{get;set;}
 public string prgid{get;set;}
 }
 
 public List<ProgramLst> getPE(){
   if(EntityList == 'Global')
      RetriveHealthStatus();
   else
      EntityHealthStatus();
    
   return PE;
   }
   
 public List<ProgramLst> getEW(){
 if(EntityList == 'Global')
      RetriveHealthStatus();
   else
      EntityHealthStatus();
    
   return EW;
   }
   public List<ProgramLst> getEI(){
   return EI;
   }
   public List<ProgramLst> getEL(){
   return EL;
   }
   public List<ProgramLst> getGC(){
   return GC;
   }
   public List<ProgramLst> getGCI(){
   return GCI;
   }
   public List<ProgramLst> getGE(){
   return GE;
   }
   public List<ProgramLst> getOE(){
   return OE;
   }
   public List<ProgramLst> getTSC(){
   return TSC;
   }
   public List<ProgramLst> getOM(){
   return OM;
   }
   public List<ProgramLst> getRE(){
   return RE;
   }
   public List<ProgramLst> getSE(){
   if(EntityList == 'Global')
      RetriveHealthStatus();
   else
      EntityHealthStatus();
    
   return SE;
   }
   
public pagereference RenderPDF(){
 renPDF = 'PDF';
 return null ;
 }
}