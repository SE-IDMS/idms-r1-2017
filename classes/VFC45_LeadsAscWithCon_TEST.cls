/*
    Author          : ACN 
    Date Created    : 20/07/2011
    Description     : Test class for VFC45_LeadsAscWithCon class 
*/
@isTest
private class VFC45_LeadsAscWithCon_TEST 
{
    @testSetup static void testDataForLead(){
        Country__c country= new Country__c(Name = 'USA', CountryCode__c = 'US');
        insert country;
          
        StateProvince__c StateProvince = new StateProvince__c(Name = 'Alaska', StateProvinceCode__c = 'AK', Country__c = country.Id, CountryCode__c = 'US');
        insert StateProvince;
    }
    
    static testMethod void testdisplayLeads() 
    {
        Database.DMLOptions objDMLOptions = new Database.DMLOptions();
        objDMLOptions.DuplicateRuleHeader.AllowSave = true;
        
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        
        User sysAdmin = Utils_TestMethods.createStandardUser('sysAdmin');
        insert sysAdmin;
        
        if(profiles.size()>0)
        {
         User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC43');
         user.bypassWF__c = false; 
                      
         System.RunAs(user)
         {
            test.startTest();
            //Account acc = Utils_TestMethods.createAccount();
            //Database.SaveResult accountSaveResult = Database.insert(Acc,objDMLOptions); 
            
            //Country__c country= Utils_TestMethods.createCountry();
            //insert country;
            Country__c country = [select id from Country__c WHERE Name = 'USA'];
            
            Lead lead2 = Utils_TestMethods.createLead(null, country.Id,100);
            insert lead2;

            lead2.ClassificationLevel1__c='LC';
            lead2.LeadingBusiness__c='BD';
            lead2.SuspectedValue__c=100;
            lead2.ZipCode__c='123789';
            lead2.OpportunityFinder__c ='Lead Agent';
            lead2.City__c='US';
            lead2.Street__c = 'testStreet';
            update lead2;
            
            Database.LeadConvert leadConv = new database.LeadConvert();
            leadConv.setLeadId(lead2.id);
            leadConv.setConvertedStatus('3 - Closed - Opportunity Created');
            
            List<Account> acct = [Select Name, Country__c, City__c from Account];
            System.debug('----- Accounts created : '+acct);
            System.RunAs(sysAdmin)
            {
                Database.LeadConvertResult lcr = Database.convertLead(leadConv);
            }
                    
            lead2 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,Contact__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,ConvertedContactId,opportunity__c from lead where Id =: lead2.Id limit 1];            
            
            System.assertEquals('3 - Closed - Opportunity Created',lead2.Status);
            //System.assertEquals('Opportunity Created',lead2.SubStatus__c);            
            System.assertEquals(lead2.Contact__c,lead2.ConvertedContactId);            
            Contact con = [select Id,Account.id from contact where Id=:lead2.ConvertedContactId limit 1];
            
            
            Lead lead1 = Utils_TestMethods.createLeadWithCon(con.Id, country.Id,100);
            lead1.Account__c= con.Account.id;
            insert lead1;            
            lead1 = [select Status, convertedOpportunityId,CallAttempts__c,LastCallAttemptDate__c,ScheduledCallbackDate__c,subStatus__c,CallBackLimit__c,TECH_Country__c,TECH_OpportunityOwner__c,Contact__c,opportunity__c from lead where Id =: lead1.Id limit 1];            
            System.assertEquals(con.Id,lead1.Contact__c);

            ApexPages.StandardController sc = new ApexPages.StandardController(con);
            VFC45_LeadsAscWithCon leadsAscWithCon= new VFC45_LeadsAscWithCon(sc);
            leadsAscWithCon.displayLeads();
            leadsAscWithCon.gotoNewLead();
            leadsAscWithCon.getGotoNewLeadUrl();
            test.stopTest();
          }
        }
    }
}