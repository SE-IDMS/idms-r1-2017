/********************************************************************
* Company: Fielo
* Developer: Elena J. Schwarzböck
* Created Date: 05/09/2016
* Description: 
********************************************************************/
public without sharing class FieloPRM_VFC_AsyncProcessController{
    
    public FieloCH__AsynchProcess__c record {get; set;}
    public CSWrapper wrapRecord {get;set;}

    public FieloCH__AsynchProcess__c recordNew {get; set;}
    public CSWrapper wrapRecordNew {get;set;}

    public FieloEE__Member__c memberProxy{get; set;}

    public List<FieloCH__AsynchProcess__c> listExisting {get; set;}
    public Boolean isRead {get;set;}
    public Boolean showNewForm {get;set;}

    public FieloPRM_VFC_AsyncProcessController (){
        memberProxy = new FieloEE__Member__c();
        getRecords();
        recordNew = new FieloCH__AsynchProcess__c();
        wrapRecordNew = new CSWrapper(recordNew);
        isRead = true;
        showNewForm = false;
    }

    public void getRecords(){
        record = FieloCH__AsynchProcess__c.getOrgDefaults();
        wrapRecord = new CSWrapper(record);
        listExisting = [SELECT Id, Name, CreatedDate, SetupOwnerId, FieloCH__BadgeMemberTimeUnit__c, FieloCH__EventTimeUnit__c, FieloCH__RewardTimeUnit__c, FieloCH__SubscriptionTimeUnit__c, FieloCH__Prefilter__c FROM FieloCH__AsynchProcess__c WHERE SetupOwnerId !=: UserInfo.getOrganizationId() ORDER BY CreatedDate];
    }

    public PageReference doEdit() {
        isRead = false;
        return null;
    }

    public PageReference doSave() {
        try{
            if(wrapRecord.BadgeMemberTimeUnit != null && wrapRecord.BadgeMemberTimeUnit != ''){
                record.FieloCH__BadgeMemberTimeUnit__c = Decimal.ValueOf(wrapRecord.BadgeMemberTimeUnit);
            }else{
                record.FieloCH__BadgeMemberTimeUnit__c = null;
            }
            if(wrapRecord.EventTimeUnit != null && wrapRecord.EventTimeUnit != ''){
                record.FieloCH__EventTimeUnit__c = Decimal.ValueOf(wrapRecord.EventTimeUnit);
            }else{
                record.FieloCH__EventTimeUnit__c = null;
            }
            if(wrapRecord.RewardTimeUnit != null && wrapRecord.RewardTimeUnit != ''){
                record.FieloCH__RewardTimeUnit__c = Decimal.ValueOf(wrapRecord.RewardTimeUnit);
            }else{
                record.FieloCH__RewardTimeUnit__c = null;
            }
            if(wrapRecord.SubscriptionTimeUnit != null && wrapRecord.SubscriptionTimeUnit != ''){
                record.FieloCH__SubscriptionTimeUnit__c = Decimal.ValueOf(wrapRecord.SubscriptionTimeUnit);
            }else{
                record.FieloCH__SubscriptionTimeUnit__c = null;
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: ' + e.getMessage()));
        }
        record.FieloCH__Prefilter__c = Boolean.ValueOf(wrapRecord.Prefilter);
        upsert record FieloCH__AsynchProcess__c.Id;
        isRead = true;
        return null;
    }

    public PageReference doNew() {
        showNewForm = true;
        return null;
    }

    public PageReference doSaveNew() {
        recordNew.SetupOwnerId = memberProxy.FieloEE__User__c;
        if(recordNew.SetupOwnerId == null){
            recordNew.SetupOwnerId.addError('You must enter a value.');
            return null;
        }
        try{
            if(wrapRecordNew.BadgeMemberTimeUnit != null && wrapRecordNew.BadgeMemberTimeUnit != ''){
                recordNew.FieloCH__BadgeMemberTimeUnit__c = Decimal.ValueOf(wrapRecordNew.BadgeMemberTimeUnit);
            }else{
                recordNew.FieloCH__BadgeMemberTimeUnit__c = null;
            }
            if(wrapRecordNew.EventTimeUnit != null && wrapRecordNew.EventTimeUnit != ''){
                recordNew.FieloCH__EventTimeUnit__c = Decimal.ValueOf(wrapRecordNew.EventTimeUnit);
            }else{
                recordNew.FieloCH__EventTimeUnit__c = null;   
            }
            if(wrapRecordNew.RewardTimeUnit != null && wrapRecordNew.RewardTimeUnit != ''){
                recordNew.FieloCH__RewardTimeUnit__c = Decimal.ValueOf(wrapRecordNew.RewardTimeUnit);
            }else{
                recordNew.FieloCH__RewardTimeUnit__c = null;
            }
            if(wrapRecordNew.SubscriptionTimeUnit != null && wrapRecordNew.SubscriptionTimeUnit != ''){
                recordNew.FieloCH__SubscriptionTimeUnit__c = Decimal.ValueOf(wrapRecordNew.SubscriptionTimeUnit);
            }else{
                recordNew.FieloCH__SubscriptionTimeUnit__c = null;
            }
            recordNew.FieloCH__Prefilter__c = Boolean.ValueOf(wrapRecordNew.Prefilter);
            upsert recordNew;
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: ' + e.getMessage()));
        }
        showNewForm = false;
        recordNew = new FieloCH__AsynchProcess__c();
        wrapRecordNew = new CSWrapper();
        memberProxy = new FieloEE__Member__c();
        getRecords();
        return null;
    }

    public PageReference doDelete() {
        Savepoint sp = Database.setSavepoint();
        try{
            String recordId = System.currentPageReference().getParameters().get('recordId');
            FieloCH__AsynchProcess__c recordDelete = FieloCH__AsynchProcess__c.getInstance(recordId);
            try{
                delete recordDelete;
            }catch(DMLException e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: ' + e.getMessage()));
            }
            recordNew = new FieloCH__AsynchProcess__c();
            wrapRecordNew = new CSWrapper();
            memberProxy = new FieloEE__Member__c();
            getRecords();
        }catch(Exception e){
            Database.rollback(sp);

        }
        return null;
    }

    public PageReference doUpdate() {
        String recordId = System.currentPageReference().getParameters().get('recordId');
        recordNew = [SELECT Id, Name, CreatedDate, SetupOwnerId, FieloCH__BadgeMemberTimeUnit__c, FieloCH__EventTimeUnit__c, FieloCH__RewardTimeUnit__c, FieloCH__SubscriptionTimeUnit__c, FieloCH__Prefilter__c FROM FieloCH__AsynchProcess__c WHERE SetupOwnerId =: recordId];
        wrapRecordNew = new CSWrapper(recordNew);
        memberProxy.FieloEE__User__c = Id.valueOf(recordId);
        showNewForm = true;
        getRecords();
        return null;
    }

    public PageReference doCancel() {
        isRead = true;
        return null;
    }

    public PageReference doCancelNew() {
        recordNew = new FieloCH__AsynchProcess__c();
        wrapRecordNew = new CSWrapper();
        memberProxy = new FieloEE__Member__c();
        getRecords();
        showNewForm = false;
        return null;
    }
    
    public class CSWrapper{
        public String SetupOwnerId {get;set;}
        public String BadgeMemberTimeUnit {get;set;}
        public String EventTimeUnit {get;set;}
        public String RewardTimeUnit {get;set;}
        public String SubscriptionTimeUnit {get;set;}
        public Boolean Prefilter {get;set;}
        public CSWrapper(FieloCH__AsynchProcess__c cs){
            SetupOwnerId = cs.SetupOwnerId;
            BadgeMemberTimeUnit = String.valueOf(cs.FieloCH__BadgeMemberTimeUnit__c);
            EventTimeUnit = String.valueOf(cs.FieloCH__EventTimeUnit__c);
            RewardTimeUnit = String.valueOf(cs.FieloCH__RewardTimeUnit__c);
            SubscriptionTimeUnit = String.valueOf(cs.FieloCH__SubscriptionTimeUnit__c);
            Prefilter = cs.FieloCH__Prefilter__c;
        }
        public CSWrapper(){
            BadgeMemberTimeUnit = '';
            EventTimeUnit = '';
            RewardTimeUnit = '';
            SubscriptionTimeUnit = '';
            Prefilter = false;   
        }
    }

}