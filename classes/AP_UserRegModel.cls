// DO NOT CHANGE THE METHOD CASING . CHANGING WILL HAVE BREAKING IMPACTS WITH UI

global Class AP_UserRegModel{

    // Contact Information
    global String email { get; set; }
    global boolean emailRequired;
    global String emailDefault;
    global boolean emailHidden;
    global boolean emailHiddenOnProfile;
    global boolean emailRequiredOnProfile;

    global String confirmEmail { get; set; }
    global boolean confirmEmailRequired;
    
    global String firstName { get; set; }
    global boolean firstNameRequired;
    global String firstNameDefault;
    global boolean firstNameHidden;
    global boolean firstNameHiddenOnProfile;
    global String firstNameTransliterated;
    global boolean firstNameRequiredOnProfile;

    global String lastName {get; set; }
    global boolean lastNameRequired;
    global String lastNameDefault;
    global boolean lastNameHidden;
    global boolean lastNameHiddenOnProfile;
    global String lastNameTransliterated;
    global boolean lastNameRequiredOnProfile;

    global String phoneNumber { get; set; }
    global String phoneNumberDefault;
    global boolean phoneNumberRequired;
    global boolean phoneNumberHidden;
    global boolean phoneNumberHiddenOnProfile;
    global boolean phoneNumberRequiredOnProfile;

    global String phoneType { get; set; }
     
    global String jobFunction { get; set; }
    global boolean jobFunctionRequired;
    global boolean jobFunctionHidden;
    global String jobFunctionDefault;
    global boolean jobFunctionHiddenOnProfile;
    global boolean jobFunctionRequiredOnProfile;

    global String jobTitle { get; set; }
    global boolean jobTitleRequired;
    global boolean jobTitleHidden;
    global String jobTitleDefault;
    global boolean jobTitleHiddenOnProfile;
    global boolean jobTitleRequiredOnProfile;

    global String businessType { get; set; }
    global boolean businessTypeRequired;
    global String businessTypeDefault;
    global boolean businessTypeHidden;
    global boolean businessTypeHiddenOnProfile;
    global boolean businessTypeRequiredOnProfile;

    global String areaOfFocus { get; set; }
    global boolean areaOfFocusRequired;
    global String areaOfFocusDefault;
    global boolean areaOfFocusHidden;
    global boolean areaOfFocusHiddenOnProfile;
    global boolean areaOfFocusRequiredOnProfile;

    global String accountId {get; set; }
    global String accOwnerID;
    global Boolean isPrimaryContact;
    global String userLanguage {get; set;}
    global String bFOLanguageLocaleKey;
    
    // Company Information
    global String companyName {get; set; }
    global boolean companyNameRequired;
    global boolean companyNameHidden;
    global String companyNameDefault;
    global boolean companyNameHiddenOnProfile;
    global String companyNameTransliterated;
    global boolean companyNameRequiredOnProfile;

    global String companyFederatedId;
    global String uimsCompanyId;
    
    global String companyPhone { get; set; }
    global boolean companyPhoneRequired;
    global boolean companyPhoneHidden;
    global String companyPhoneDefault;
    global boolean companyPhoneHiddenOnProfile;
    global boolean companyPhoneRequiredOnProfile;

    global String companyAddress1 { get; set; }
    global boolean companyAddress1Required;
    global boolean companyAddress1Hidden;
    global String companyAddress1Default;
    global boolean companyAddress1HiddenOnProfile;
    global String companyAddress1Transliterated;
    global boolean companyAddress1RequiredOnProfile;

    global String companyAddress2 { get; set; }
    global boolean companyAddress2Required;
    global boolean companyAddress2Hidden;
    global String companyAddress2Default;
    global boolean companyAddress2HiddenOnProfile;
    global String companyAddress2Transliterated;
    global boolean companyAddress2RequiredOnProfile;

    global String companyCity { get; set; }  
    global boolean companyCityRequired;
    global boolean companyCityHidden;
    global String companyCityDefault;
    global boolean companyCityHiddenOnProfile;
    global String companyCityTransliterated;
    global boolean companyCityRequiredOnProfile;

    global String companyCountryIsoCode;
    global String companyStateProvinceIsoCode;
    
    global String companyCountry { get; set; }
    global boolean companyCountryRequired;
    global boolean companyCountryHidden;
    global String companyCountryDefault;
    global boolean companyCountryHiddenOnProfile;
    global boolean companyCountryRequiredOnProfile;

    global String companyStateProvince {get; set; }
    global boolean companyStateProvinceRequired;
    global boolean companyStateProvinceHidden;
    global String companyStateProvinceDefault;
    global boolean companyStateProvinceHiddenOnProfile;
    global boolean companyStateProvinceRequiredOnProfile;

    global String companyZipcode {get; set; }
    global boolean companyZipcodeRequired;
    global boolean companyZipcodeHidden;
    global String companyZipcodeDefault;
    global boolean companyZipcodeHiddenOnProfile;
    global boolean companyZipcodeRequiredOnProfile;

    global boolean companyHeaderQuarters {get; set; }
    global boolean companyHeadQuartersRequired;
    global boolean companyHeadQuartersHidden;
    global boolean companyHeaderQuartersDefault;
    global boolean companyHeadQuartersHiddenOnProfile;
    global boolean companyHeadQuartersRequiredOnProfile;

    global String companyWebsite {get; set; }
    global boolean companyWebsiteRequired;
    global boolean companyWebsiteHidden;
    global String companyWebsiteDefault;
    global boolean companyWebsiteHiddenOnProfile;
    global boolean companyWebsiteRequiredOnProfile;

    global String taxId {get; set; }
    global boolean taxIdRequired;
    global boolean taxIdHidden;
    global String taxIdDefault;
    global boolean taxIdHiddenOnProfile; 
    global boolean taxIdRequiredOnProfile;

    global String companyClassificationLevel1;
    global boolean termsAndConditions;
    
    global boolean includeCompanyInfoInPublicSearch { get; set; }
    global boolean includeCompanyInfoInPublicSearchHidden;
    global boolean includeCompanyInfoInPublicSearchRequired;
    global String  includeCompanyInfoInPublicSearchDefault;
    global boolean includeCompanyInfoInPublicSearchHiddenOnProfile;
    global boolean includeCompanyInfoInPublicSearchRequiredOnProfile;

    global boolean doNotHaveCompany {get; set; }    
    global boolean doNotHaveCompanyHidden;
    global boolean doNotHaveCompanyRequired;
    global String  doNotHaveCompanyDefault;
    global boolean doNotHaveCompanyHiddenOnProfile;
    global boolean doNotHaveCompanyRequiredOnProfile;

    global boolean skippedCompanyStep { get; set; }
    
    global string companyBusinessType;

    global string fieldActivity;
    
    global string prefDistributor1;
    global boolean prefDistributor1Required;
    global boolean prefDistributor1Hidden;
    global string prefDistributor1Default;
    global boolean prefDistributor1HiddenOnProfile;
    global boolean prefDistributor1RequiredOnProfile;

    global string prefDistributor2;
    global boolean prefDistributor2Required;
    global boolean prefDistributor2Hidden;
    global string prefDistributor2Default;
    global boolean prefDistributor2HiddenOnProfile;
    global boolean prefDistributor2RequiredOnProfile;

    global string prefDistributor3;
    global string prefDistributor3Required;
    global string prefDistributor3Hidden;
    global string prefDistributor3Default;
    global string prefDistributor3HiddenOnProfile;
    global string prefDistributor3RequiredOnProfile;

    global string prefDistributor4;
    global boolean prefDistributor4Required;
    global boolean prefDistributor4Hidden;
    global string prefDistributor4Default;
    global boolean prefDistributor4HiddenOnProfile;
    global boolean prefDistributor4RequiredOnProfile;

    global string marketServed;
    global string marketServedRequired;
    global string marketServedHidden;
    global string marketServedDefault;
    global string marketServedHiddenOnProfile;
    global string marketServedRequiredOnProfile;

    global String  companySpeciality;
    global boolean companySpecialityRequired;
    global String  companySpecialityDefault;
    global boolean companySpecialityHidden;
    global boolean companySpecialityHiddenOnProfile;    
    global boolean companySpecialityRequiredOnProfile;

    global string employeeSize;
    global boolean employeeSizeRequired;
    global boolean employeeSizeHidden;
    global string employeeSizeDefault;
    global boolean employeeSizeHiddenOnProfile;
    global boolean employeeSizeRequiredOnProfile;

    global string companyCurrency;
    global boolean companyCurrencyRequired;
    global boolean companyCurrencyHidden;
    global string companyCurrencyDefault;
    global boolean companyCurrencyHiddenOnProfile;
    global boolean companyCurrencyRequiredOnProfile;

    global string annualSales;
    global boolean annualSalesRequired;
    global boolean annualSalesHidden;
    global string annualSalesDefault;
    global boolean annualSalesHiddenOnProfile;
    global boolean annualSalesRequiredOnProfile;

    global string seAccountNumbers;
    global boolean seAccountNumbersRequired;
    global boolean seAccountNumbersHidden;
    global string seAccountNumbersDefault;
    global boolean seAccountNumbersHiddenOnProfile;
    global boolean seAccountNumbersRequiredOnProfile;

    global boolean primayContact;
    global string contactID;
    global string jobTitleCode;
    global string jobFunctionCode;
    global string companyBusinessTypeCode;
    global string employeeSizeCode;
    global string companyAreaOfFocus;
    global string companyAreaOfFocusCode;
    global string marketServedCode;
    global string businessType1;
    global String areaOfFocus1;
    global string businessType2;
    global String areaOfFocus2;
    global string businessType3;
    global String areaOfFocus3;
    global string businessType1Code;
    global string areaOfFocus1Code;
    
    global string businessType2Code;
    global string areaOfFocus2Code;
    
    global string businessType3Code;
    global string areaOfFocus3Code;
    
    global string companyCurrencyCode;
    global string federationId;
    
    //all the PL fields as variables
    global String plCompanyName;
    global String plCompanyCountry;
    
    global String plCompanyAddress1;
    global String plCompanyAddress2;
    global String plCompanyCity;
    global String plCompanyStateProvince;
    global String plCompanyZipcode;
    global String plmarketServed;
    global String plCompanyPhone;
    global String plBusinessType;
    global String plMainContactEmail;
    global String plMainContactFirstName;
    global String plMainContactLastName;
    global String plMainContactphone;
    global String plWebsite;
    global String plCompanyDescription;
    global String plDatapool;
    global String plDomainsOfExpertise;
    global String plFax;
    global boolean plShowInPartnerLocator;
    global Boolean plShowInPartnerLocatorForced;
    global String plAreaOfFocus;
    global String plCompanyCountryCode;
    global String plCompanyStateProvinceCode;
    global string plmarketServedCode; 
    global string companyCountryCode;
    global string companyStateProvinceCode;
    global boolean isInternalUser;
    global boolean skipStep5;
    global boolean skipStep6;
    global string annualSalesCode;
    global string contRegStatus;
    
    global string  cTaxId;
    global boolean cTaxIdRequired;
    global boolean cTaxIdHidden;
    global string  cTaxIdDefault;
    global boolean cTaxIdHiddenOnProfile; 
    global boolean cTaxIdRequiredOnProfile;

    global String prmOrigin {get; set; }
    global String prmOriginSource {get; set; }
    global String prmOriginSourceInfo {get; set; }
    
    global boolean prmUserRegistrationCompleted;
    global string contCountry;
    global boolean isAccountIdSet;
    
    global string leadId;
    global string leadSource;
    global String marketoAssetLink;
    global String mktoLeadPartitionName;
    global String targetApplicationRequested {get; set; } 
    global String securityToken {get; set;}  

    global String batchImportId { get; set; } 
    global String companyChange {get; set; }
    global boolean privateRegCompleted {get; set;}
    global String socialMedia {get; set;}
    global boolean existingAccount{get;set;}
    global boolean existingContact{get;set;}
    global String wechatId { get; set;}
    global String phoneId { get; set;}
    global AP_PRMAppConstants.ProcessRegistrationPool bFOPool {get; set;}
    global AP_UserRegModel(){
        emailRequired = confirmEmailRequired = lastNameRequired = firstNameRequired = businessTypeRequired = areaOfFocusRequired = true;
        includeCompanyInfoInPublicSearch = true;
        skippedCompanyStep = false;
        doNotHaveCompany = existingAccount = existingContact = false;
        companyHeaderQuarters = false;
        cTaxIdHidden = true;
        phoneType = 'PhoneType';
        businessType = '';
        areaOfFocus = '';
        jobTitle = '';
        jobFunction = '';
        companyChange = '';
        socialMedia = '';
        wechatId = phoneId = '';
        skipStep5 = false;
        skipStep6 = false;
        plShowInPartnerLocator = false;
        securityToken = '';
        primayContact = isPrimaryContact = true;
        isAccountIdSet = false;
        termsAndConditions = false;
        privateRegCompleted = false;
        targetApplicationRequested = System.label.CLMAR16PRM011;
    }
}