public with sharing class VFC_AccountUpdateRequestMessage
{
    public boolean displayErrorMessage{get;set;}{displayErrorMessage=false;}
    private AccountUpdateRequest__c accountUpdateRequestrecord=null;
    
    public VFC_AccountUpdateRequestMessage(ApexPages.StandardController controller) 
    {
        accountUpdateRequestrecord=(AccountUpdateRequest__c)controller.getRecord(); 
        
        //Check if AUR marked for deletion & reason <> Duplicate               
        if(accountUpdateRequestrecord.ToBeDeleted__c && accountUpdateRequestrecord.ReasonForDeletion__c!=Label.CL00521)
        {
            for(Account acc: [Select id, (select id from Leads__r),(select id from Opportunities), (select id from Cases) From Account Where id=:accountUpdateRequestrecord.Account__c])
            {
                if(acc.Leads__r.size()>0 || acc.Opportunities.size()>0 || acc.Cases.size()>0)
                    displayErrorMessage=true;
                else
                    displayErrorMessage=false;
            }                
        }

    }
}