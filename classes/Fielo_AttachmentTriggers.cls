/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Created Date: 19/08/2014                                  *
************************************************************/
public with sharing class Fielo_AttachmentTriggers {
    public static void AttIdForParent (list<Attachment> triggerNew){
        List<Id> ListParentId = new List<Id>();
        map<Id, Attachment> mapAccountIdAttach = new map<Id, Attachment>();
        
        for(Attachment attach: triggerNew){
            ListParentId.add(attach.ParentId);
            mapAccountIdAttach.put(attach.ParentId, attach);
        }
        
        List<Fielo_LoyaltyEligibleProduct__c> LoyaltyProduct = [SELECT id, Name, F_AttachmentId__c FROM Fielo_LoyaltyEligibleProduct__c WHERE Id IN: ListParentId];
        if(!LoyaltyProduct.isEmpty()){            
            for(Fielo_LoyaltyEligibleProduct__c prod: LoyaltyProduct){
                prod.F_AttachmentId__c = mapAccountIdAttach.get(prod.Id).Id;
                prod.F_AttachmentId__c = prod.F_AttachmentId__c.substring(0,prod.F_AttachmentId__c.length()-3);
            }
            update LoyaltyProduct;
        }
    }
}