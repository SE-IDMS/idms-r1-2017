//*******************************************************************************************
//Created by      : Onkar
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : This class is used to update user AIL records.
//
//*******************************************************************************************

public without sharing class IDMSUpdateUserAIL {
    public Boolean idmsUserMatched = false;
    //This method is used to update the user AIL record
    public IDMSResponseWrapperAIL updateUserAIL(IDMSUserAIL__c UserAILRecord)   {
        //Step 1:check mandatory fields
        if(!mandatoryFieldsMissing(UserAILRecord)){
            //Step 2: Search existing User in 
            String ident = SearchExistingUser(UserAILRecord);
            if (ident != null){
                system.debug('Entered in User found');
                If (UserAILRecord.IDMS_Profile_update_source__c == UserAILRecord.IDMSAcl__c ||UserAILRecord.IDMS_Profile_update_source__c == Label.CLQ316IDMS202 ){
                    UserAILRecord.IDMSTECHUniqueAIL__c = ident + UserAILRecord.IDMSAclType__c + UserAILRecord.IDMSAcl__c;
                    system.debug('unique value:'+UserAILRecord.IDMSTECHUniqueAIL__c);
                    //Check for existing userAIL in IDMS
                    List<IDMSUserAIL__c> UserAilList = [select Id from IDMSUserAIL__c where  IDMSTECHUniqueAIL__c =: UserAILRecord.IDMSTECHUniqueAIL__c Limit 1];
                    IDMSUserAIL__c userAIL;
                    boolean updateRequired = false;
                    
                    if(userAilList.size() > 0){ 
                        // Update on existing record
                        userAIL = UserAilList[0];
                        updateRequired = true;
                    }else{
                        // insert of new record
                        userAIL = UserAILRecord;
                    }
                    
                    if(UserAILRecord.IDMSOperation__c == Label.CLQ316IDMS050 || UserAILRecord.IDMSOperation__c == Label.CLQ316IDMS051)   {
                        userAIL.IDMSOperation__c = UserAILRecord.IDMSOperation__c;
                    }else{
                        //Operation not valid
                        return new IDMSResponseWrapperAil(null,Label.CLJUN16IDMS76,Label.CLQ316IDMS053);  
                        
                    }
                    if(UserAILRecord.IDMSAclType__c == Label.CLQ316IDMS203 || UserAILRecord.IDMSAclType__c== Label.CLQ316IDMS204 || UserAILRecord.IDMSAclType__c== Label.CLQ316IDMS205)   {
                        userAIL.IDMSAclType__c = UserAILRecord.IDMSAclType__c; 
                        //ACL Type is not valid
                    } else {
                        return new IDMSResponseWrapperAil(null,Label.CLJUN16IDMS76,Label.CLQ316IDMS180);  
                    }
                    
                    if(idmsUserMatched && ident != null){
                        userAIL.IDMSUser__c = ident ;
                    }
                    
                    if(updateRequired){
                        update userAIL;
                        userAIL = [select ID, IDMSAcl__c, IDMSAclType__c, IDMSFederationIdentifier__c ,IDMSIsRevokedOperation__c, 
                                   IDMSOperation__c,  IDMS_Profile_update_source__c, IDMSUser__c, IDMSUser__r.FederationIdentifier  
                                   from IDMSUserAIL__c 
                                   where ID =:userAIL.Id Limit 1];
                        
                        // Call asynchrone to UIMS to update user
                        syncAilUIMS(userAIL );
                        
                        return new IDMSResponseWrapperAil(userAIL,Label.CLQ316IDMS052,Label.CLQ316IDMS054);                     
                    }else{
                        insert userAIL;
                        userAIL = [select ID, IDMSAcl__c, IDMSAclType__c, IDMSFederationIdentifier__c ,IDMSIsRevokedOperation__c, 
                                   IDMSOperation__c,  IDMS_Profile_update_source__c, IDMSUser__c, IDMSUser__r.FederationIdentifier  
                                   from IDMSUserAIL__c 
                                   where ID =:userAIL.Id];
                        // Call asynchrone to UIMS to update user
                        syncAilUIMS(userAIL );
                        system.debug(userAIL.id);
                        return new IDMSResponseWrapperAil(userAIL,Label.CLQ316IDMS052,Label.CLQ316IDMS055);      
                    }
                } else {
                    // Error source should be = ACL
                    system.debug('Update source and ACL value are different: ' + UserAILRecord);
                    return new IDMSResponseWrapperAil(null,Label.CLJUN16IDMS76,Label.CLQ316IDMS056);
                }
            } else {
                UserAILRecord.IDMSTECHUniqueAIL__c = UserAILRecord.IDMSFederationIdentifier__c  + UserAILRecord.IDMSAclType__c + UserAILRecord.IDMSAcl__c;
                List<IDMSUserAIL__c> UserAilUpdateList = [select ID, IDMSFederationIdentifier__c, IDMSTECHUniqueAIL__c, IDMSAcl__c, IDMSAclType__c, IDMSOperation__c,
                                                          IDMS_Profile_update_source__c, IDMSUser__c   
                                                          from IDMSUserAIL__c 
                                                          where IDMSFederationIdentifier__c =: UserAILRecord.IDMSFederationIdentifier__c and IDMSTECHUniqueAIL__c =: UserAILRecord.IDMSTECHUniqueAIL__c];
                system.debug('AIL in the list:'+UserAilUpdateList);
                IDMSUserAIL__c UserAILToUpdate;
                if(UserAilUpdateList.size()> 0){
                    system.debug('Size is greater than zero:'+UserAilUpdateList);
                    UserAILToUpdate = UserAilUpdateList[0];
                    if(UserAILRecord.IDMSOperation__c == Label.CLQ316IDMS050 || UserAILRecord.IDMSOperation__c == Label.CLQ316IDMS051)   {
                        UserAILToUpdate.IDMSOperation__c = UserAILRecord.IDMSOperation__c;
                    }else{
                        //Operation not valid
                        return new IDMSResponseWrapperAil(null,Label.CLJUN16IDMS76,Label.CLQ316IDMS053);  
                        
                    }
                    update UserAILToUpdate;
                    system.debug('after update');
                    UserAILToUpdate = [select ID, IDMSAcl__c, IDMSAclType__c, IDMSFederationIdentifier__c,IDMSIsRevokedOperation__c, 
                                       IDMSOperation__c,  IDMS_Profile_update_source__c 
                                       from IDMSUserAIL__c 
                                       where ID =:UserAILToUpdate.Id];
                    return new IDMSResponseWrapperAil(UserAILToUpdate,Label.CLQ316IDMS052,Label.CLQ316IDMS054); 
                }else {
                    
                    // User doesn't exist
                    system.debug('User doesnt exist: ' + UserAILRecord);
                    return new IDMSResponseWrapperAil(null,Label.CLJUN16IDMS76,Label.CLQ316IDMS057);                                    
                }
            }
        }else{        
            //Missing mandatory 
            return new IDMSResponseWrapperAil(null,Label.CLJUN16IDMS76,Label.CLQ316IDMS058);                                                
        }
        return null;
    }
    //This method is used to search existing user record
    public String SearchExistingUser(IDMSUserAIL__c UserAILRecord){
        
        if(UserAILRecord.IDMSUser__c != Null){
            List<User> usersId = [select ID, FederationIdentifier from User where Id =: UserAILRecord.IDMSUser__c];
            if(usersId.size()>0){
                idmsUserMatched = true;
                return usersId[0].Id;
            }
        }else if(UserAILRecord.IDMSFederationIdentifier__c != Null){
            List<UIMS_User__c> uimsUsersFed = [select ID, FederatedId__c from UIMS_User__c where FederatedId__c=: UserAILRecord.IDMSFederationIdentifier__c];
            if(uimsUsersFed.size()>0){
                idmsUserMatched = FALSE;
                return uimsUsersFed[0].FederatedId__c ;
            } 
        }
        
        return null;
    }
    
    //This method is used to check if mandatory field is missing in the user record
    public Boolean mandatoryFieldsMissing(IDMSUserAIL__c UserAILRecord){
        if(UserAILRecord.IDMSUser__c == Null && UserAILRecord.IDMSFederationIdentifier__c == Null){
            return TRUE;
        }
        if(UserAILRecord.IDMSAclType__c == Null || UserAILRecord.IDMSAcl__c == Null ||UserAILRecord.IDMSOperation__c == Null ||UserAILRecord.IDMS_Profile_update_source__c == NULL){
            return TRUE;
        }
        return FALSE;
    }
    //This method is used to sync with UIMS
    public void syncAilUIMS(IDMSUserAIL__c userAIL){
        if(userAIL.IDMS_Profile_update_source__c!= Label.CLQ316IDMS202){ 
            String callerFid = Label.CLJUN16IDMS104;
            String UserFedId;
            if(userAIL.IDMSUser__r.FederationIdentifier != null){
                UserFedId = userAIL.IDMSUser__r.FederationIdentifier;
            }else {
                UserFedId = userAIL.IDMSFederationIdentifier__c;
            } 
            String UimsAccess = userAIL.IDMSAclType__c +'-'+ userAIL.IDMSAcl__c;    
            system.debug('Update User AIL has been sent to UIMS'+userAIL.IDMSUser__r.FederationIdentifier);
            if(userAIL.IDMSOperation__c == Label.CLQ316IDMS050){
                IDMSUIMSWebServiceCalls.uimsUpdateUserAILGrant(callerFid, UserFedId, UimsAccess); 
            }else {
                IDMSUIMSWebServiceCalls.uimsUpdateUserAILRevoke(callerFid, UserFedId, UimsAccess);
            }
        }         
    }
    
}