//*******************************************************************************************
//Created by      : Ranjith
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : class used as controller for reset password vf page for reset user password
//
//*******************************************************************************************

    public with sharing class VFC_IDMSRequestPasswordReset{
    public String emailAddress {get;set;}
    public String phoneNumber {get;set;}
    public Boolean pwdresetSuccess{get;set;}
    public String emailOrPhone {get;set;} 
    public String appid{get;set;} 
    public String context ; 
    public Boolean redirectToApp{get;set;}
    public Boolean redirectToAppLogin{get;set;} 
    public String suffixurl{get;set;} 
    public Boolean appBool ; 
    public Boolean hideAllFields{get;set;}
    public string appURL{get;set;}
    public Boolean errorMessage{get;set;}
    public String ErrorMsgCode {get;set;}
    public Boolean showCaptcha{get;set;}
    public String cookievalueURL{get;set;} 
    
    //app variables to display at page
    public string appName{get; set;}
    public string appNameToDisplay{get; set;}
    public string appDes{get; set;}
    public string appDesFooter{get; set;}
    public string appDesFooter1{get; set;}
    public string appLogo{get; set;}
    public string appAndSE{get; set;}
    public string appBgrdImg{get; set;}
    public string appTabName{get;set;}
    public string appTabLogo{get; set;}
    public string appMobileFooterImg{get; set;}
    public string applicationId{get; set;}
    public string language{get; set;}
    public boolean successPhoneMsg {get;set;} 
    
    //Get application langauge from url
    public string appLanguage = ApexPages.currentPage().getParameters().get('lang');
        
    //constructor used for redirections and fetching values from custom setting
    public VFC_IDMSRequestPasswordReset(){
        appid=ApexPages.currentPage().getParameters().get('app');
        system.debug('appId after entityEncode: '+ appid);
        
        if(String.isBlank(appid)){
            hideAllFields=true;
        }
        else if(appid != null){
            IDMSApplicationMapping__c appMap;
            appMap = IDMSApplicationMapping__c.getInstance(appid);
            if(appMap != null){
                context         = (String)appMap.get('context__c');
                appName         = (String)appMap.get('Name'); //for functional use
                appNameToDisplay= (String)appMap.get('AppName__c'); //To Display on the page
                //application details to display at page
                appDes              = (String)appMap.get('AppDescription__c');
                appDesFooter        = (String)appMap.AppTextAndUrl__c;
                appLogo             = (String)appMap.get('AppLogo__c');
                appAndSE            = (String)appMap.AppBrandOrLogo__c;
                appBgrdImg          = (String)appMap.AppBackgroundVisual__c;
                // added to show application specific TabName as in BFOID-523
                appTabName          = Label.CLNOV16IDMS128;
                appTabName = (String)appMap.get('AppTabName__c');
                    if(String.isBlank(AppTabName)){
                        AppTabName     = Label.CLNOV16IDMS128;
                    }
              // added to show application specific TabLogo as in BFOID-523
              appTabLogo            = Label.CLFEB17IDMS004;
              appTabLogo         = (String)appMap.get('AppTabLogo__c');
                    if(String.isBlank(AppTabLogo)){
                        AppTabLogo     = Label.CLFEB17IDMS004;
                    } 
                appMobileFooterImg  = (String)appMap.get('AppMobileFooterImage__c');
                appDesFooter1       = (String)appMap.get('AppDescriptionFooter1__c');
                applicationId       = (String)appMap.get('AppId__c');
               // language            = (String)appMap.get('Language__c');
                if(string.isnotblank((string)appMap.get('UrlRedirectAfterPwReset__c'))){
                    appURL          = (string)appMap.get('UrlRedirectAfterPwReset__c');
                }
                else{
                    appurl          ='/identity/idp/login?app='+(string)appMap.get('appid__c');
                }
                
            }else{
                hideAllFields       = true;
            }
            
        }
         //get Login URL from cookie
         Cookie urlCookie= ApexPages.currentPage().getCookies().get('LoginURLCookie'); 
         cookievalueURL=String.valueOf(urlCookie.getValue());
         if(String.isBlank(cookievalueURL)){
         cookievalueURL=Label.CLNOV16IDMS057+applicationId;
         }
        
        String toShowCaptcha    = Label.CLJUL16IDMS6;
        if(toShowCaptcha.equalsIgnoreCase('true')){
            showCaptcha         = true;
        } 
        else{
            showCaptcha         = false;
        } 
    }
    
    //method used for verifying captcha and password set
    public PageReference passwordReset(){
        //new I am not a robot captcha verification
        if(showCaptcha == true){
            String captcha_response = ApexPages.currentPage().getParameters().get('g-recaptcha-response');
            captcha_response = AP_IDMSEntityEncode.encode(captcha_response);
            system.debug('captcha_response after entity encode: '+ captcha_response);
            if(String.isBlank(captcha_response) || !IDMSValidateReCaptcha.validateReCaptcha(captcha_response)){
                ErrorMsgCode='captcha.error';
                return null;
            }
        }
        
        system.debug('appid--> ' + appid ) ; 
        system.debug('email address-> ' + emailOrPhone ) ; 
        redirectToAppLogin = false ; 
        IDMSPasswordService passwordService = new IDMSPasswordService() ; 
        IDMSResponseWrapper  idmsResponse   = new IDMSResponseWrapper() ; 
        PageReference successPg             = new PageReference('/apex/IDMSRequestPasswordResetSuccess'); 
        successPg.getParameters().put('app',appid) ; 
        
        if(String.isBlank(emailOrPhone)){
             errorMessage    = false;
             ErrorMsgCode    = 'resetphone.null';
             return null;
        }
        
        if(String.isNotBlank(emailOrPhone)) {            
            Pattern p = Pattern.compile('.+@.+\\.[a-z]+');
            Matcher m = p.matcher(emailOrPhone);
            system.debug('Matches --> ' + m.matches()) ; 
            if(m.matches()){ // Email 
                String usernamesuffix = emailOrPhone +Label.CLJUN16IDMS71;
                system.debug('username--> ' + usernamesuffix) ; 
                List<User> users = [select id, username, email ,MobilePhone , IDMSIdentityType__c , 
                                    IDMS_Profile_update_source__c,IDMS_Registration_Source__c
                                    from User
                                    where username =: usernamesuffix limit 1];    
                
                system.debug('usernamelist--> ' + users.size() + users) ; 
                VFC_IDMS_Consumer_SocialLogin res   = new VFC_IDMS_Consumer_SocialLogin();
                Boolean iscreated                   = false;
                User usr                            = new User();
                if(users.size()== 0 && res.resetPasswordCreateUser(emailOrPhone,applicationId,context)){
                    users = [select id, username, email ,MobilePhone , IDMSIdentityType__c,
                             IDMS_Profile_update_source__c,IDMS_Registration_Source__c
                             from User
                             where username =: usernamesuffix limit 1];
                    iscreated = true;
                }
                if(users.size() == 0){
                    errorMessage    = false;
                    ErrorMsgCode    = 'reset.failed';
                }else{
                    system.debug('user --> ' + appName + ' ' + users[0] ) ;
                    usr.id                              = users[0].id;
                    usr.email                           = users[0].email;
                    usr.IDMS_Profile_update_source__c   = appName; 
                    
                    if(!iscreated){
                        system.debug('idmsresponse--> ' + idmsResponse.Message) ; 
                        idmsResponse                    = passwordService.resetPassworduser(usr,appid,appLanguage);
                    } 
                    else{
                        system.debug('idmsresponse--> ' + idmsResponse.Message) ; 
                        resetPasswordForCreatedUser(usernamesuffix,appName,appLanguage);
                        return successPg ;
                    }
                    if(idmsResponse != null) {    
                        system.debug('idmsresponse--> ' + idmsResponse.Message) ;
                        if(idmsResponse.Status.equalsignorecase('Success')){
                            return successPg;
                        }
                        else 
                            return null; 
                    }                    
                }
            }else { 
                String phonenumbersuffix ;
                System.debug('emailorphone--> ' + emailOrPhone); 
                if(emailOrphone != null ) {
                    emailOrphone = emailOrPhone.trim(); 
                }
                // HotFix Jira 443 
                if((emailOrPhone !=null || emailOrPhone !='') && Pattern.matches(Label.CLJUN16IDMS31, emailOrPhone ) ){
                    system.debug('entering phone--> ') ; 
                    if(emailOrPhone.startsWith('+')){
                        emailOrPhone = emailOrPhone.replace('+', '00');
                    }
                    else if(!emailOrPhone.startsWith('00')){
                        emailOrPhone = '00' + emailOrPhone;
                    }
                    phonenumbersuffix = emailOrPhone +Label.CLQ316IDMS102;
                }
                
                List<User> users = [select id, username, email ,MobilePhone , IDMSIdentityType__c , 
                                    IDMS_Registration_Source__c,IDMS_Profile_update_source__c from User where username =: phonenumbersuffix limit 1];    
                system.debug('phonenumberlist--> ' + users.size() + users) ; 
                if(users.size() == 0 ){
                    errorMessage=false;
                    ErrorMsgCode='reset.failed';
                    return null;
                }else{
                    User usr                            = new User();
                    usr.id                              = users[0].id;
                    usr.MobilePhone                     = users[0].MobilePhone;
                    usr.IDMS_Profile_update_source__c   = appName; 
                    idmsResponse                        = passwordService.resetPassworduser(usr,appid,appLanguage);
                    system.debug('idmsresponse --> ' + idmsResponse.Message); 
                    if(idmsResponse != null) {
                        if(idmsResponse.Status.equalsignorecase('Success')){
                            suffixurl                   = '?app='+appName+'&id='+users[0].id+'&key='+Label.CLQ316IDMS004;
                            suffixurl                   = Label.CLQ316IDMS112+suffixurl;
                            system.debug('suffix url--> ' + suffixurl ) ;
                            redirectToApp               = true; 
                            successPhoneMsg             = true ; 
                            return successPg ;   
                        }     
                    }
                }          
            }
        }      
        
        else {            
            errorMessage=false;
            ErrorMsgCode='resetphone.failed';
            return null;
        }
        
        return null;
    }
    
    //used to redirect  user to login page
    public PageReference loginRedirect() {
        PageReference redirectpg = new PageReference('/apex/UserLogin');
        redirectpg.getParameters().put('app',applicationId) ; 
        return redirectpg ;  
    }
    
    //called only in case of usercreated on the fly  
    @future
    public Static void resetPasswordForCreatedUser(String usernamesuffix,String appName, String appLanguage){
        User u=[select id,email from user where username=:usernamesuffix limit 1];
        user usr=new user();
        usr.id=u.id;
        usr.email=u.email;
        usr.IDMS_Profile_update_source__c=appName; 
        IDMSPasswordService passwordresetcall = new IDMSPasswordService() ; 
        passwordresetcall.resetPassworduser(usr,appName, appLanguage);
    }
    
    
    
}