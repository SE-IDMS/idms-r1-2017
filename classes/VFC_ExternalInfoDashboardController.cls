public virtual class VFC_ExternalInfoDashboardController {

    Private String accQryFields;
    Private String cntQryFields;
    public List<SelectOption> BusinessTypeList { get; set; }
    public List<SelectOption> AreaOfFocusList { get; set; }
    public String SelectedBusinessType { get; set; }
    public String SelectedAreaOfFocus { get; set; }
    public String TabName { get; set; }
    public String RecordId { get; set; }
    public String Mode { get; set; }
    public String SaveStatus { get; set; }
    public List<String> ValidationMessages { get; private set; }

    public List<EligiblePrimaryContact> PPCEligible { get; set; }
    public Contact PrimaryPartnerContact { get; set; }

    public String AccountFields { 
        get { return this.accQryFields; }
        set { this.accQryFields = value; } 
    }
    public String ContactFields { 
        get { return this.cntQryFields; }
        set { this.cntQryFields = value; } 
    }
    public Integer PageNumber { get; set; }
    public List<Integer> TotalPages { get;set; }
    public Integer PaginationSize { get;set; }
    public DashboardDetails DashboardInfo { get; set; }
    public Map<Id,Boolean> ConNotHasFullPortalAccess { get;set; }
    public String cntryId { get;set; }
    
    public VFC_ExternalInfoDashboardController() {
        String query;
        Integer prmContactCount = 0;
        Integer nonPRMCount = 0;
        PageNumber = 0;
        TotalPages = new List<Integer>();
        ConNotHasFullPortalAccess = new Map<Id,Boolean>();
        TabName = ApexPages.currentPage().getParameters().get('tab');
        RecordId = ApexPages.currentPage().getParameters().get('id');
        SaveStatus = ApexPages.currentPage().getParameters().get('status');
        String tMode = ApexPages.currentPage().getParameters().get('mode');

        if (String.isNotBlank(tMode))
            Mode = tMode;

        System.debug('*** Query Tab:' + ApexPages.currentPage().getParameters().get('tab'));
        System.debug('*** Mode:' + ApexPages.currentPage().getParameters().get('mode'));

        System.debug('*** Mode: ' + Mode);
        
        String accountQueryFields = '', contactQueryFields = '';

        for (Integer i = 0; i < 15; i++) {
            CS_PRM_ApexJobSettings__c qryFld = CS_PRM_ApexJobSettings__c.getInstance('PRMAccountFieldList' + String.valueOf(i));
            CS_PRM_ApexJobSettings__c cQryFld = CS_PRM_ApexJobSettings__c.getInstance('PRMContactFieldList' + String.valueOf(i));

            accountQueryFields = accountQueryFields + (qryFld != null ? qryFld.QueryFields__c : '');
            contactQueryFields = contactQueryFields + (cQryFld != null ? cQryFld.QueryFields__c : '');
        }
        this.AccountFields = accountQueryFields;
        this.ContactFields = contactQueryFields;

        this.ValidationMessages = new List<String>();
        this.PPCEligible = new List<EligiblePrimaryContact>();

        DashboardInfo = new DashboardDetails();
        PaginationSize = Integer.valueof(CS_PRM_ApexJobSettings__c.getValues('PAGESIZE_INTERNALUSER').QueryFields__c);
        query = 'SELECT ';
        DashboardInfo.ProgramMembershipCount = 0;
        System.debug('RecordId123 :'+RecordId);
        if(RecordId.Startswith('001')) {
            query = query + accountQueryFields + ' FROM Account WHERE Id = :RecordId';
            System.debug('*** Account Query:' + query);
            DashboardInfo.AccountInfo = database.Query(query);
            
            string cListQry = 'SELECT ' + contactQueryFields + ' FROM Contact WHERE AccountId = :RecordId';
            System.debug('*** Contact List Query:' + cListQry);
            DashboardInfo.ContactList = database.Query(cListQry);
            // Could not run aggregate queries on checkbox field
            // hence iterating 
            for (Contact c :  DashboardInfo.ContactList ) {
                if (c.PRMContact__c != null && c.PRMContact__c == true)
                    prmContactCount ++;
                else
                    nonPRMCount ++;
                
                if (String.isNotBlank(c.PRMContactRegistrationStatus__c) && c.PRMContactRegistrationStatus__c == 'Validated' && c.PRMPrimaryContact__c != true) {
                    EligiblePrimaryContact tPC = new  EligiblePrimaryContact();
                    tPC.Id = c.Id;
                    tPC.Name = c.PRMFirstName__c + ' ' + c.PRMLastName__c;
                    this.PPCEligible.Add( tPC );
                }

                if (c.PRMPrimaryContact__c == true)
                    this.PrimaryPartnerContact = c;
            }
            DashboardInfo.PRMContactCount = prmContactCount;
            DashboardInfo.NonPRMContactCount = nonPRMCount;
            if(!DashboardInfo.ContactList.isEmpty()){
                GetPaginationContacts();
                Integer pageCount = DashboardInfo.ContactList.Size()/PaginationSize;
                pageCount = Math.mod(DashboardInfo.ContactList.Size(), PaginationSize) == 0 ?  pageCount: ++pageCount;
                for(Integer i=1; i<=pageCount; i++){
                    TotalPages.add(i);
                }
            }
            List<AggregateResult> lPrgMembrsip = new List<AggregateResult>([
                                SELECT COUNT_DISTINCT(FieloCH__Challenge__c) ProgramCount
                                FROM FieloCH__TeamChallenge__c
                                WHERE FieloCH__Challenge__r.PartnerProgram__c = null AND 
                                      FieloCH__Team__c IN (SELECT F_PRM_ProgramTeam__c FROM Account WHERE id = :RecordId)]);

            if (lPrgMembrsip != null && !lPrgMembrsip.isEmpty()) {
                AggregateResult tAR = lPrgMembrsip[0];
                DashboardInfo.ProgramMembershipCount = Integer.valueOf(tAR.get('ProgramCount'));
            }
            
            //Adding account Assigned Program
            List<AggregateResult> lPrgAssigned = new List<AggregateResult>([
                                SELECT COUNT_DISTINCT(PartnerProgram__c) AssignedPrgCount
                                FROM ACC_PartnerProgram__c
                                WHERE Active__c = true AND Account__c = :RecordId]);
                                
                                
            if (lPrgAssigned != null && !lPrgAssigned.isEmpty()) {
                AggregateResult tPCount = lPrgAssigned[0];
                DashboardInfo.ProgramMembershipCount = DashboardInfo.ProgramMembershipCount + Integer.valueOf(tPCount.get('AssignedPrgCount'));
            }
            
            
            
        }
        
        if(RecordId.Startswith('003')){
        System.debug('RecordId456 :'+RecordId);
            query = query + contactQueryFields + ' FROM Contact WHERE Id =:RecordId';
            System.debug('Query123 :'+query);
            
            DashboardInfo.ContactInfo = database.Query(query);
            if(!String.isBlank(DashboardInfo.ContactInfo.FieloEE__Member__c))
                DashboardInfo.MemberInfo = [SELECT CreatedById,CreatedDate,CurrencyIsoCode,FieloEE__Age__c,FieloEE__AgreementReadOnly__c,FieloEE__Agreement__c,FieloEE__ApprovedReadOnly__c,FieloEE__Approved__c,FieloEE__Birthdate__c,FieloEE__Blocked__c,FieloEE__City__c,FieloEE__Country__c,FieloEE__DaysLastTransaction__c,FieloEE__Email__c,FieloEE__EnabledStatusReadOnly__c,FieloEE__Enabled__c,FieloEE__FirstName__c,FieloEE__Gender__c,FieloEE__IdentificationCode__c,FieloEE__IdentificationType__c,FieloEE__Identification__c,FieloEE__imageId__c,FieloEE__IsActive__c,FieloEE__LastName__c,FieloEE__LastTransactionDateReadOnly__c,FieloEE__LastTransactionDate__c,FieloEE__LevelReadOnly__c,FieloEE__Level__c,FieloEE__LoyaltyAge__c,FieloEE__MediaPoints__c,FieloEE__MemberCodeReadOnly__c,FieloEE__MemberCode__c,FieloEE__MobilePhone__c,FieloEE__Name__c,FieloEE__Neighborhood__c,FieloEE__NetPoints__c,FieloEE__OperatorCode__c,FieloEE__OperatorPassword__c,FieloEE__Phone__c,FieloEE__PointsReadOnly__c,FieloEE__Points__c,FieloEE__Preference__c,FieloEE__ProfileCompletenessReadOnly__c,FieloEE__ProfileCompleteness__c,FieloEE__Program__c,FieloEE__RedeemPointsPercent__c,FieloEE__ReferalMemberReadOnly__c,FieloEE__ReferalMember__c,FieloEE__RegisteredReadOnly__c,FieloEE__Registered__c,FieloEE__StartDateReadOnly__c,FieloEE__StartDate__c,FieloEE__State__c,FieloEE__Status__c,FieloEE__Street__c,FieloEE__TotalRedeemPoints__c,FieloEE__TotalRedeemValue__c,FieloEE__TotalRedeem__c,FieloEE__TotalTransactions__c,FieloEE__TotalValue__c,FieloEE__Type__c,FieloEE__UserReadOnly__c,FieloEE__User__c,FieloEE__ZipCode__c,F_Account__c,F_Address1__c,F_AreaOfFocus__c,F_BusinessType__c,F_City__c,F_CompanyName__c,F_CompanyPhoneNumber__c,F_CompanyType__c,F_CompanyWebsiteURL__c,F_CorporateHeadquarters__c,F_CountryName__c,F_Country__c,F_JobFunction__c,F_JobTitle__c,F_LastLoginDateTime__c,F_NearLandmark__c,F_PhoneExtension__c,F_POBox__c,F_PRM_AccountProfileComplete__c,F_PRM_AccountRegistrationStatus__c,F_PRM_Channels__c,F_PRM_ContactRegistrationStatus__c,F_PRM_CountryCode__c,F_PRM_Country__c,F_PRM_DesactivateDate__c,F_PRM_ExcludeFromReports__c,F_PRM_IsActivePRMContact__c,F_PRM_IsEnrolledMember__c,F_PRM_Language__c,F_PRM_LastLoginDateTime__c,F_PRM_PrimaryChannel__c,F_PRM_PrimaryContactIncompletedProfile__c,F_PRM_PrimaryContact__c,F_PRM_ProgramLevels__c,F_PRM_ReasonForDecline__c,F_PRM_SecondaryChannel1__c,F_PRM_SecondaryChannel2__c,F_PRM_SecondaryChannel3__c,F_PRM_SecondaryChannelsList__c,F_PRM_SegmentationAttribute__c,F_PurchaseSEProductsBefore__c,F_SEProductsSelling__c,F_StateProvince__c,F_Title__c,Id,IsDeleted,IsLocked,LastActivityDate,LastModifiedById,LastModifiedDate,MayEdit,Name,OwnerId,PRMUIMSId__c,SystemModstamp FROM FieloEE__Member__c WHERE ID =: DashboardInfo.ContactInfo.FieloEE__Member__c];
            System.debug('*** Member name '+DashboardInfo.ContactInfo.FieloEE__Member__r.Name);
            List<AggregateResult> lPrgMembrsip = new List<AggregateResult>([
                                SELECT COUNT_DISTINCT(FieloCH__Challenge__c) ProgramCount
                                 FROM FieloCH__TeamChallenge__c]);
                                //WHERE FieloCH__Team__c IN (SELECT Id FROM FieloCH__Team__c WHERE F_PRM_Account__c = :RecordId)]);

            if (lPrgMembrsip != null && !lPrgMembrsip.isEmpty()) {
                AggregateResult tAR = lPrgMembrsip[0];
                DashboardInfo.ProgramMembershipCount = Integer.valueOf(tAR.get('ProgramCount'));
            }
            FieloEE__Program__c Program = FieloPRM_UTILS_ProgramMethods.getProgramByContact(RecordId);
            if(Program != null)
                DashboardInfo.ConNotHasFullPortalAccess = false;
            else
                DashboardInfo.ConNotHasFullPortalAccess = true;
        }
        System.Debug('*** Tab Name:' + TabName);
    }
    
    public virtual void PopulateAvailableChannels () {
        cntryId = this.DashboardInfo.AccountInfo.PRMCountry__c;
        Set<String> tChannels = new Set<String>();
        List<PRMCountry__c> lCntry = new List<PRMCountry__c>([SELECT Id,SupportedLanguage1__c,SupportedLanguage2__c,SupportedLanguage3__c,Country__c,MemberCountry1__c,MemberCountry2__c,MemberCountry3__c,MemberCountry4__c,MemberCountry5__c,
                                                        (SELECT Id, ChannelCode__c,Channel_Name__c, SubChannelCode__c,Sub_Channel__c, Country__c FROM CountryChannels__r WHERE Active__c = true)
                                                        FROM PRMCountry__c WHERE CountryPortalEnabled__c = True 
                                                        AND (Country__c = :cntryId OR MemberCountry1__c = :cntryId OR MemberCountry2__c = :cntryId OR MemberCountry3__c = :cntryId OR MemberCountry4__c = :cntryId OR MemberCountry5__c = :cntryId)]);

        System.debug('**Country Channel List**' + lCntry);
        this.BusinessTypeList = new List<SelectOption>();
        this.BusinessTypeList.add(new SelectOption('', '--None--'));

        if (lCntry != null && !lCntry.isEmpty()) { 
            // Map<String, String> mBT = new Map<String, String>();
            PRMCountry__c tCntry = lCntry[0];
            List<CountryChannels__c> lChannels = tCntry.CountryChannels__r;
            if (lChannels != null && !lChannels.isEmpty()) {
                for (CountryChannels__c tChnl : lChannels) {
                    if (!tChannels.contains(tChnl.ChannelCode__c))
                        tChannels.add(tChnl.ChannelCode__c);
                }
            }
            Schema.DescribeFieldResult btResult = PRMLovs__c.BT__c.getDescribe();
            List<Schema.PicklistEntry> btPicklistValues = btResult.getPicklistValues();

            for( Schema.PicklistEntry pValue : btPicklistValues){
                if (tChannels.Contains(pValue.getValue()))
                    this.BusinessTypeList.add(new SelectOption(pValue.getValue(), pValue.getLabel())); 
            }
        }
    }
    
    public virtual PageReference PopulateAvailableSubChannels () {
        cntryId = this.DashboardInfo.AccountInfo.PRMCountry__c;
        System.debug('*** Account Country: ' + cntryId);
        System.debug('*** Selected Business Type: ' + this.SelectedBusinessType);

        List<PRMCountry__c> lCntry = new List<PRMCountry__c>([SELECT Id,SupportedLanguage1__c,SupportedLanguage2__c,SupportedLanguage3__c,Country__c,MemberCountry1__c,MemberCountry2__c,MemberCountry3__c,MemberCountry4__c,MemberCountry5__c,
                                                                    (SELECT Id, ChannelCode__c,Channel_Name__c, SubChannelCode__c,Sub_Channel__c, Country__c FROM CountryChannels__r WHERE  ChannelCode__c = :this.SelectedBusinessType AND Active__c = true)
                                                                FROM PRMCountry__c WHERE CountryPortalEnabled__c=True 
                                                                AND (Country__c = :cntryId OR MemberCountry1__c = :cntryId OR MemberCountry2__c = :cntryId OR MemberCountry3__c = :cntryId OR MemberCountry4__c = :cntryId OR MemberCountry5__c = :cntryId)]);

        this.AreaOfFocusList.Clear();
        this.AreaOfFocusList.add(new SelectOption('','--None--'));
        
        Map<String, String> mAF = new Map<String, String>();
        Schema.DescribeFieldResult afResult = PRMLovs__c.AF__c.getDescribe();
        List<Schema.PicklistEntry> afPicklistValues = afResult.getPicklistValues();

        for( Schema.PicklistEntry pValue : afPicklistValues){
            mAF.put(pValue.getValue(), pValue.getLabel()); 
        }

        PRMCountry__c tCntry = lCntry[0];
        List<CountryChannels__c> lChannels = tCntry.CountryChannels__r;
        System.debug('*** Sub Channels:' + lChannels);

        Set<String> sSubChannels = new Set<String>();
        if (lChannels != null && !lChannels.isEmpty()) {
            for (CountryChannels__c tChnl : lChannels) {
                if (!sSubChannels.contains(tChnl.SubChannelCode__c))
                    sSubChannels.add(tChnl.SubChannelCode__c);
            }
            
            for (String s : sSubChannels) {
                if (mAF.containsKey(s))
                    this.AreaOfFocusList.add(new SelectOption(s, mAF.get(s)));
            }
        }
        System.debug('*** Sub Channel Options:' + this.AreaOfFocusList);
        return null;
    }
    
    public  PageReference GetPaginationContacts(){
        try{
            ValidationMessages.clear();
            Integer offset = PageNumber * PaginationSize;
            string cListQry = 'SELECT ' + ContactFields + ' FROM Contact WHERE AccountId = :RecordId ORDER BY PRMPrimaryContact__c DESC, PRMContact__c DESC Limit '+PaginationSize+' OFFSET '+offset;
            DashboardInfo.ContactOffsetList = database.Query(cListQry);
            for(Contact con: DashboardInfo.ContactOffsetList){
                if(con.PRMContact__c){
                    FieloEE__Program__c Program = FieloPRM_UTILS_ProgramMethods.getProgramByContact(con.Id);
                    if(Program != null)
                        ConNotHasFullPortalAccess.put(con.Id,false);
                    else
                        ConNotHasFullPortalAccess.put(con.Id,true);
                }
                else
                    ConNotHasFullPortalAccess.put(con.Id,false);
                    
                if(String.isBlank(con.PRMFirstName__c))
                    con.PRMFirstName__c = con.FirstName;
                if(String.isBlank(con.PRMLastName__c))
                    con.PRMLastName__c = con.LastName;
                if(String.isBlank(con.PRMEmail__c))
                    con.PRMEmail__c = con.Email;
            }
        }
        catch(exception ex){
            System.debug('*** '+ex);
            SaveStatus = 'valErrors';
            integer errorPage = PageNumber+1;
            // ValidationMessages.add( 'Error while getting page '+errorPage+'list of contacts: ' + ex.getMessage() + ex.getStackTraceString());
        }
        return null;
    }
    
    public PageReference DoNewRegistration () {
        PageReference redirect = new PageReference('/apex/VFP_NewPRMRegistration'); 
        
        // pass the selected asset ID to the new page
        redirect.getParameters().put('accid', DashboardInfo.AccountInfo.Id); 
        redirect.setRedirect(true); 
        return redirect;
    }
    
    // This method will be overwritten to return if the respective record is dirty or not
    protected virtual Boolean IsDirty () {
        return false;
    }

    // This method will be overwritten to return list of error messages to be displated to the user
    protected virtual void ValidateInputData() {
    } 

    public virtual PageReference Save () {
        return null;
    }

    public class EligiblePrimaryContact {
        public String Id { get; set; }
        public String Name { get; set; }
    }

    public class DashboardDetails {
        public Account AccountInfo { get; private set; }
        public Contact ContactInfo { get; private set; }
        public FieloEE__Member__c MemberInfo { get; private set; }
        public List<Contact> ContactList { get; set; }
        public List<Contact> ContactOffsetList { get;set; }
        public Integer PRMContactCount { get; set; }
        public Integer NonPRMContactCount { get; set; }
        public Integer ProgramMembershipCount { get; set; }
        public List<ProgramMembership> ProgramMembershipList { get;set; }
        public Integer RegProgramsCount { get;set; }
        public Boolean ConNotHasFullPortalAccess { get;set; }
        
        public Boolean ProfileCompleted { 
            get {
                return (this.AccountInfo != null) ? this.AccountInfo.PRMAccountCompletedProfile__c : false;
            }
        private set; }
        public DashboardDetails () {
            ProgramMembershipList = new List<ProgramMembership>();
            ContactOffsetList = new List<Contact>();
        }
    }
    public class ProgramMembership {
        public FieloPRM_BadgeAccount__c BadgeAccount { get;set; }
        public String ProgramOrBadge { get;set; }
        public ACC_PartnerProgram__c AccPartProgram { get;set; }
        
        public ProgramMembership(FieloPRM_BadgeAccount__c BadgeAccount, String ProgramOrBadge, ACC_PartnerProgram__c AccPartProgram){
            this.BadgeAccount = BadgeAccount;
            this.ProgramOrBadge = ProgramOrBadge;
            this.AccPartProgram = AccPartProgram;
        }
    }
    public PageReference DoCancelMember() {
        PageReference pg = new PageReference('/apex/VFP_ManageMemberExternalInfo?tab=member&id='+RecordId+'&mode='+Mode);
        pg.setredirect(true);
        return pg;
    }
}