public with sharing class VFC_I2PSearchController{

    public String searchText {get;set;}
    public List<Problem__c> problist {get;set;}
    public Boolean ShowMsgs {get;set;}
    public String username{get; set;}
    public String password {get; set;}
    public Boolean isEdit {get;set;}

    public VFC_I2PSearchController(){
        problist = new List<Problem__c>();
        ShowMsgs = false;
        isEdit = false;
    }

    public pageReference doLogin(){  
        return Site.login(username,password,'/ExtranetI2P/home/home.jsp');
    }

    public PageReference forgotPasswordI2PCommunity() {
        boolean success = Site.forgotPassword(username);
        PageReference pr = Page.VFP_I2PCommunitiesLoginPage;
        pr.setRedirect(true);
        if (success) {  
            isEdit = true;
            return pr;
        }
        return null;
    }
    public PageReference searchproblems(){
        problist = new List<Problem__c>();
        ShowMsgs = false;
        if(searchText == null || searchText == '' || searchText.length() < Integer.valueof(Label.CLI2PSupplier001) ){
            ShowMsgs = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CLMAY15I2P01)); //Please enter a search string at least 2 characters long.
            return null;
        }
        else{
            String soql = 'select id,Name';
            for(Schema.FieldSetMember fld :SObjectType.Problem__c.FieldSets.problemFieldSet.getFields()) {
            soql += ', ' + fld.getFieldPath();
            }
            soql += ' from Problem__C where';
            String whereCondition = '';
            if (searchText != null && searchText != '') {
                searchText = searchText.replaceAll('\\*','%');
                whereCondition = whereCondition +' Name LIKE \'' + '%'+searchText+'%\' OR Title__c LIKE \'' + '%'+searchText+'%\'';
            }
            soql = soql + whereCondition +' limit '+Label.CLI2PSupplier002;
            System.debug('$$$$'+soql);
            try{
                problist = database.query(soql);
                if(problist.size() > 0){
                    ShowMsgs = true;
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.CLMAY15I2P03)); // No match found
                }
            }
            catch(Exception e){
                System.debug('Exception'+e.getMessage());
            }
            return null;  
        }
    }
}