global class AssessmentResult
{
    // WS error codes & error message
    WebService Integer returnCode;
    WebService String message;
    WebService Integer totalNoOfAssessments; 
    WebService Assessment[] assessments { get; set; }
}