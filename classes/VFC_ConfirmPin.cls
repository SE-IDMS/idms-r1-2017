//*******************************************************************************************
//Created by      : Ranjith
//Created Date    : 2016/06/01 
//Modified by     : Ranjith                
//Modified Date   : 2016/12/07         Version : 1.0         Ticket#              Purpose : 
//Overall Purpose : class used as controller for confirm pin vf page
//*******************************************************************************************

public without sharing class VFC_ConfirmPin{
    public String confirmPin{get;set;}
    public Id userId {get;set;} 
    public String idmsOperation{get;set;}
    public String idmsPassword{get;set;}
    public boolean password{get;set;}
    public String cnfpassword{get;set;}
    public boolean redirectToApp{get;set;}
    public string appid{get;set;}
    public string context , appName; 
    public boolean incorrectAppId{get;set;}
    public string appurl{get;set;}
    public string appDes{get; set;}
    public string appDesFooter{get; set;}
    public string appDesFooter1{get; set;}
    public string language{get; set;}
    public string applicationId{get; set;}
    public string appLogo{get; set;}
    public string appAndSE{get; set;}
    public string appBgrdImg{get; set;}
    public string appTabName{get;set;}
    public string appTabLogo{get; set;}
    public string appMobileFooterImg{get; set;}
    public string errorPageMessage {get;set;}
    public Boolean errorMessage{get;set;}
    public String ErrorMsgCode {get;set;}
    
    //constructor used for redirections and fetching values from custom setting
    public VFC_ConfirmPin(){
        incorrectAppId = false;
        appid = ApexPages.currentPage().getParameters().get('app');
        system.debug('appid--'+appid);
        String userIDNullCheck ='ID';
        try{
            userId = ApexPages.currentPage().getParameters().get('id'); 
            system.debug('userId '+userId );
        }catch(exception e){
            system.debug('inside catch--');
            userIDNullCheck = '';
        }
        idmsOperation = ApexPages.currentPage().getParameters().get('key');
        if(idmsOperation == 'SetUserPwd') 
            this.password = true ; 
        IDMSApplicationMapping__c appMap;
        appMap = IDMSApplicationMapping__c.getInstance(appid);
        
        if(appMap != null){
            context            = (String)appMap.get('context__c');
            appName            = (String)appMap.get('name');
            appDes             = (String)appMap.get('AppDescription__c');
            appDesFooter       = (String)appMap.get('AppDescriptionFooter__c');
            appLogo            = (String)appMap.get('AppLogo__c');
            appAndSE           = (String)appMap.get('AppAndSELogo__c');
            appBgrdImg         = (String)appMap.get('AppBackgroundImg__c');
            appMobileFooterImg = (String)appMap.get('AppMobileFooterImage__c');
            appDesFooter1      = (String)appMap.get('AppDescriptionFooter1__c');
            applicationId      = (String)appMap.get('AppId__c');
            language           = (String)appMap.get('Language__c');
            // added to show application specific TabName as in BFOID-523
            appTabName         = Label.CLNOV16IDMS128;
            appTabName = (String)appMap.get('AppTabName__c');
                    if(String.isBlank(AppTabName)){
                        AppTabName     = Label.CLNOV16IDMS128;
                    }
           // added to show application specific TabLogo as in BFOID-523
           appTabLogo         = Label.CLFEB17IDMS004;
           appTabLogo         = (String)appMap.get('AppTabLogo__c');
                    if(String.isBlank(AppTabLogo)){
                        AppTabLogo     = Label.CLFEB17IDMS004;
                    } 
            
            if(string.isnotblank((string)appmap.get('UrlRedirectAfterReg__c'))){
                appurl = (string)appmap.get('UrlRedirectAfterReg__c');
            }else
            {
                appurl='/identity/idp/login?app='+applicationId;
            }
        }else{
            system.debug('inside else');
            incorrectAppId=true;
            system.debug('in else--'+incorrectAppId);
        }
        if(String.isBlank(appid) || String.isBlank(idmsOperation) || string.isBlank(userIDNullCheck)){
            incorrectAppId=true;
        }
    }
    
    // method used for confirming pin entered ny user
    public Pagereference confirmKey() {
        IDMSConfirmPin idmsPin              = new IDMSConfirmPin() ; 
        IDMSConfirmPinResponse idmsResponse = new IDMSConfirmPinResponse() ;   
        PageReference successPg             = new PageReference('/identity/IDMSConfirmSuccess');
        successPg.getParameters().put('app',appid);
        
        if(String.isBlank(confirmPin)){
            errorMessage = false;
            ErrorMsgCode = 'error.pin';
            return null;
            
        }   
        else if(String.isNotBlank(confirmPin) && confirmPin.length() != 6 ) {
            errorMessage = false;
            ErrorMsgCode = 'error.incorrectpin';
            return null;
            
        }
        else if(idmsOperation == 'SetUserPwd' ) {
            if ( String.isBlank(idmsPassword ) || String.isBlank(cnfpassword)) {
                errorMessage = false;
                ErrorMsgCode = 'error.password';
                return null;
            }
           if(!Pattern.matches(Label.CLNOV16IDMS129, idmsPassword ) && String.isNotBlank(idmsPassword) ){
               errorMessage = false;
                ErrorMsgCode = 'error.passworderror';
                return null;
           }
            if(!Pattern.matches(Label.CLNOV16IDMS129, cnfpassword) && String.isNotBlank(cnfPassword) ){
               errorMessage = false;
                ErrorMsgCode = 'error.cnfpassworderror';
                return null;
           }
            if(!idmsPassword.equalsIgnoreCase(cnfpassword)){
                errorMessage = false;
                ErrorMsgCode = 'error.confirmpwd';
                return null;
            }
            idmsResponse = idmsPin.ConfirmPin(userId,null,'idms',confirmPin,idmsOperation,idmsPassword);
            if(idmsResponse != null){ 
                if(idmsResponse.Status.equalsignorecase('Success')){
                    return successPg; 
                } 
                if(idmsResponse.Message != null && (idmsResponse.Message.contains(System.label.CLQ316IDMS015)||idmsResponse.Message.contains(System.label.CLJUN16IDMS52))){
                    errorPageMessage  = idmsResponse.Message ; 
                    errorMessage = false;
                    ErrorMsgCode = 'error.rspMsg';
                    redirectToApp = false; 
                    return null;
                    
                }else {
                    errorPageMessage  = idmsResponse.Message ; 
                    errorMessage = false;
                    ErrorMsgCode = 'error.rspMsg';
                    return null;
                }
            }    
            else {
                errorPageMessage  = idmsResponse.Message ; 
                errorMessage = false;
                ErrorMsgCode = 'error.rspMsg';
                return null;
            }
        } else {
            idmsResponse = idmsPin.ConfirmPin(userId,null,'idms',confirmPin,idmsOperation,idmsPassword);
            if(idmsResponse != null) {
                if(idmsResponse.Status.equalsignorecase('Success')){
                    return successPg;
                }
                if(idmsResponse.Message != null && (idmsResponse.Message.contains(System.label.CLQ316IDMS015))){
                    errorPageMessage  = idmsResponse.Message ; 
                    errorMessage = false;
                    ErrorMsgCode = 'error.rspMsg';
                    return null;
                    redirectToApp = false; 
                }
            }else{
                errorPageMessage  = idmsResponse.Message ; 
                errorMessage=false;
                ErrorMsgCode='error.rspMsg';
                return null;
            }
        }
        return null ; 
    }
    
    //method used for implementing resend pin functionality
    public Pagereference resendPin(){
        List<User> u = [select ID, mobilephone, FederationIdentifier from User where ID=: userId ];
        system.debug('user coming from url:'+u);
        User usertosend;
        if(u.size()> 0){
            usertoSend = u[0];
        }
        usertoSend.IDMSPinVerification__c = String.valueof(100000+ Math.round(Math.random()*100000));
        update usertoSend;
        IDMSSendCommunication.sendSMS(usertoSend);
        return null;
    }
}