/*
Monalisa - March 2016 release
Copied from SPA Mass Upload functinality with modifications according to the Opportunity Lines.
Controller for the VFP_OpportunityLinesMassCreate
*/

public with sharing class VFC_OpportunityLinesMassCreate{
    public String type{get;set;}{type=null;}
    public boolean isTableReady{get;set;}{isTableReady=false;}   
    public StringListWrapper csvList{get;set;}{csvList=new StringListWrapper();} 
    public Map<String,CS_OpportunityLineItemCSVMap__c> csvNameAPINameMap = new Map<String,CS_OpportunityLineItemCSVMap__c>();
    public boolean hasErrors{get;set;}{hasErrors=false;}
    public Map<String,boolean> isRequiredMap{get;set;}{isRequiredMap=new Map<String,boolean>();}
    public Map<String,String> headerAPItoCSVMap{get;set;}{headerAPItoCSVMap=new Map<String,String>();}
    public Opportunity opprtunityRecord{get;set;}{opprtunityRecord=new Opportunity();}
    public boolean flag{get;set;}
    public Set<String> justRequiredMap=new Set<String>();
    public List<LineItemWrapper> lineItemWrapperRecords{get;set;}{lineItemWrapperRecords=new List<LineItemWrapper>();}
    public boolean disableEverything{get;set;}{disableEverything=false;}
    public boolean displayReadOnlyFields{get;set;}{displayReadOnlyFields=false;}
    public boolean hasNoLineItemErrors{get;set;}{hasNoLineItemErrors=true;}
      String decimalSeperator=null;
    String groupingSeperator=null;
    Integer multiplicationFactor=1;
    public Integer rowNum{get;set;}
    
     public VFC_OpportunityLinesMassCreate(ApexPages.StandardSetController controller) {
         if(ApexPages.currentPage().getParameters().containsKey('id'))        
            for(Opportunity requestrecord:[select id,SEreference__c,CurrencyISOcode,CloseDate from Opportunity where id =:ApexPages.currentPage().getParameters().get('id')])
                opprtunityRecord=requestrecord;
     
     }
     
      public void typeSelect()
     {
          System.debug('>>>>>>Type is>>>>'+type);
          this.type = type;
          if(type==null || type=='')
              opprtunityRecord.addError(System.Label.CLOCT14SLS34);
              
     }
     
     public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(System.Label.CLMAR16SLS16,System.Label.CLMAR16SLS16)); 
        options.add(new SelectOption(System.Label.CLMAR16SLS17,System.Label.CLMAR16SLS17)); 
         return options; 
        }
        
     public void preapareLineItemList()
        {
            Integer rownumber=0;
            try{               
                System.debug('>>>>> Method type'+type);
                System.debug('>>>>> Method csv size>>>'+csvList.stringList.size());
                if (type=='PM0 Level'){
                   for(CS_OpportunityLineItemCSVMap__c cs: [select Name,ApiName__c,FieldType__c,isRequired__c,MassCreationType__c from CS_OpportunityLineItemCSVMap__c where MassCreationType__c ='PM0 Level' OR MassCreationType__c=:System.Label.CLOCT14SLS62])
                        csvNameAPINameMap.put(cs.Name,cs); 
                      System.debug('>>>>>csvNameAPINameMap'+csvNameAPINameMap);
                  }
                  
                 else if (type=='Commercial Reference'){
                   for(CS_OpportunityLineItemCSVMap__c cs: [select Name,ApiName__c,FieldType__c,isRequired__c,MassCreationType__c  from CS_OpportunityLineItemCSVMap__c where MassCreationType__c ='Commercial Reference' OR MassCreationType__c=:System.Label.CLOCT14SLS62])
                        csvNameAPINameMap.put(cs.Name,cs); 
                        System.debug('>>>>>csvNameAPINameMap'+csvNameAPINameMap);
                  }
                
                  if(csvList.stringList.size()>0)
                  {
                      try{
                            decimalSeperator=CS_CurrencyDecimalSeperator__c.getValues(UserInfo.getLocale()).DecimalSeperator__c;
                            groupingSeperator=CS_CurrencyDecimalSeperator__c.getValues(UserInfo.getLocale()).GroupingSeperator__c;
                            if(decimalSeperator==null)
                                decimalSeperator='';
                            if(groupingSeperator==null)
                                groupingSeperator='';
                        }
                        catch( Exception ex ){
                            System.debug('exception'); 
                            decimalSeperator=CS_CurrencyDecimalSeperator__c.getValues('en_US').DecimalSeperator__c;
                            groupingSeperator=CS_CurrencyDecimalSeperator__c.getValues('en_US').GroupingSeperator__c;                
                        }
                    System.debug('>>>>decimalSeperator '+decimalSeperator+' >>>>groupingSeperator>>> '+groupingSeperator);
                    List<String> headerRow=new List<String>();                      
                    headerRow=csvList.stringList[0];            
                    populateRequiredMap();
                    if(validateHeaders(headerRow)){
                        rownumber=0;                        
                        for(Integer rowcount=1;rowcount<csvList.stringList.size();rowcount++)
                        {
                            rownumber=rowcount+1;
                            List<String> cells=csvList.stringList[rowcount];                    
                            OPP_ProductLine__c tempLineItem=new OPP_ProductLine__c(Opportunity__c=opprtunityRecord.id,CurrencyISOcode=opprtunityRecord.CurrencyISOcode,LineClosedate__c=opprtunityRecord.CloseDate,LineType__c='Parent Line', recordTypeId=System.Label.CLOCT13SLS21);
                            for(Integer cellcount=0;cellcount<cells.size();cellcount++){
                            //the name is retreived from headerRow[cellcount] and the corresponding value is retreived from cells[cellcount]
                                String fieldType=csvNameAPINameMap.get(headerRow[cellcount]).FieldType__c;  
                                 System.debug('><><><><><'+fieldType+'><><><><'+cells[cellcount].trim());                            
                                if(cells[cellcount].trim()!=null && cells[cellcount].trim()!=''){
                                   if(fieldType == 'currency' || fieldType == 'integer' || fieldType == 'percent' || fieldType == 'decimal' || fieldType == 'double' )
                                    {
                                         String amt=cells[cellcount].trim();
                                         System.debug('amtbefore>>>>>'+amt);
                                         if(decimalSeperator==','){
                                            System.debug('Comma Seperator>>>>>>>');
                                                amt=amt.replace(groupingSeperator,'');
                                                amt=amt.replace('.','..');//to throw error for dot if its not grouping seperator
                                                amt=amt.replace(',','.');
                                        }
                                        else if (decimalSeperator=='.'){
                                            System.debug('Dot Seperator>>>>');
                                            amt=amt.replace(groupingSeperator,'');
                                        }
                                        System.debug('amtafter>>>>>'+amt);
                                        multiplicationFactor=1;
                                        if(amt.contains('K')||amt.contains('k'))
                                        {
                                            amt=amt.replace('K','').replace('k','');
                                            multiplicationFactor=1000;
                                        }
                                        else if(amt.contains('M')||amt.contains('m'))
                                        {
                                            amt=amt.replace('M','').replace('m','');
                                            multiplicationFactor=1000000;
                                        }
                                        else if(amt.contains('B')||amt.contains('b'))
                                        {
                                            amt=amt.replace('B','').replace('b','');
                                            multiplicationFactor=1000000000;
                                        }
                                        amt=amt.remove('"').remove('"').trim();//remove quote if any exists
                                        System.debug('>>>>>fieldType '+fieldType );
                                        System.debug('@@@@@@>>>>>>>>>'+amt);
                                        
                                        
                                        if(fieldType == 'integer'){  
                                            tempLineItem.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,(Integer.valueOf(amt)*multiplicationFactor)); 
                                       }
                                        else{
                                          decimal decamt=Decimal.valueOf((String)amt.trim());
                                         tempLineItem.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,(decamt*multiplicationFactor).setScale(2,RoundingMode.HALF_UP)); 
                                       }
                                    }
                                    else if(fieldType == 'boolean')
                                    {
                                        tempLineItem.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,Boolean.valueOf(cells[cellcount].trim())); 
                                    }                   
                                    else if(fieldType == 'date')
                                    {
                                        tempLineItem.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,date.valueOf(cells[cellcount].trim())); 
                                    }                
                                    else
                                    {
                                        //remove quotes at the start and end of the String which got appended if the value has commas
                                        tempLineItem.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,cells[cellcount].trim().removeStart('"').removeEnd('"'));
                                    }                                                                                   
                                    if(headerAPItoCSVMap.keySet().size()!=headerRow.size())
                                        headerAPItoCSVMap.put(csvNameAPINameMap.get(headerRow[cellcount]).APIName__c,headerRow[cellcount].trim());
                                }    
                            } 
                            if(tempLineItem.Amount__c==null || tempLineItem.Amount__c==0  ||tempLineItem.Quantity__c==null || tempLineItem.Quantity__c==0 )   {             
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CLMAR16SLS24+' '+rownumber));
                                hasErrors=true;
                            }
                            
                            lineItemWrapperRecords.add(new LineItemWrapper(tempLineItem,System.Label.CLMAR16SLS22));
                            if(lineItemWrapperRecords.size()> Integer.ValueOf(System.Label.CLMAR16SLS19)){
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CLMAR16SLS18));
                                hasErrors=true;
                            }
                        }                                                           
                    } 
                    else
                    {                                            
                        String errorMessage=System.Label.CLMAR16SLS23;
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage));
                        hasErrors=true;
                    }                   
                }           
            }
            catch(Exception ex)     
            {
                System.debug('error>>>>>>'+ex.getmessage());
                if(rownumber!=0)
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.Label.CLMAR16SLS24+' '+rownumber));
                 else
                     ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getmessage()));
                hasErrors=true;
            }
            finally{
                isTableReady=true;                
            } 
        }
        
       public void populateRequiredMap(){
            for(CS_OpportunityLineItemCSVMap__c csvmaprecord :csvNameAPINameMap.values()){                
                isRequiredMap.put(csvmaprecord.ApiName__c,csvmaprecord.isRequired__c);  
                if(csvmaprecord.isRequired__c)              
                justRequiredMap.add(csvmaprecord.Name);
            }
        }
        
       public boolean validateHeaders(List<String> headerRow)
        {   
            Set<String> headerRowset= new Set<String>(headerRow);
            System.debug('csvNameAPINameMap.keySet()>>>>>'+new Set<String>(csvNameAPINameMap.keySet()));
            System.debug('headerRow>>>>>>'+headerRow);
            System.debug('!csvNameAPINameMap.keySet().containsAll(headerRow)>>>>>>'+csvNameAPINameMap.keySet().containsAll(headerRow));         
            System.debug('headerRowset.containsAll(justRequiredMap)>>>>>>'+headerRowset.containsAll(justRequiredMap));
            
            if(!csvNameAPINameMap.keySet().containsAll(headerRowset) || !headerRowset.containsAll(justRequiredMap) )            
                return false;            
            else
                return true;
        }
        
        public PageReference create()
        {
           displayReadOnlyFields=false; 
                hasNoLineItemErrors=true;
           if(type=='PM0 Level'){
              System.debug('>>>>>> PM0 level loop >>>>');
              List<OPP_Product__c> oppProdList=[select Id,BusinessUnit__c,ProductLine__c,ProductFamily__c,Family__c from OPP_Product__c where isactive__c=true and SubFamily__c=null and series__c=null and GDP__c=null and ProductLine__c!=null and BusinessUnit__c!=null and TECH_PM0CodeInGMR__c!=null];
              Map<String,OPP_Product__c> prodMapId =new Map<String,OPP_Product__c>();
              String plstr=''; // for all Product table data with out modification
              String plstrg='';// for 5 digit Product Line with Business Unit,Product Line and Product Family
              String plstring='';// for 5 digit Product line with out Business Unit,Product Line and Product Family
              String plstrng='';// for full data ProductLine with out Business Unit,Product Line and Product Family
              for(OPP_Product__c pl:oppProdList){
              //ContainsKey function of Map is case Sensitive if Key is String, and hence converting key to Uppercase
                   plstr='';
                   plstrg='';
                   if(pl.BusinessUnit__c!=null && pl.BusinessUnit__c!=''){
                     plstr=pl.BusinessUnit__c.toUpperCase();
                     plstrg=pl.BusinessUnit__c.toUpperCase();
                    }
                   if(pl.ProductLine__c!=null && pl.ProductLine__c!='') {
                     plstr+= '_'+pl.ProductLine__c.toUpperCase();
                     plstrg+= '_'+pl.ProductLine__c.Left(5).toUpperCase();
                    }
                   if(pl.ProductFamily__c!=null && pl.ProductFamily__c!='') {
                    plstr+='_'+pl.ProductFamily__c.toUpperCase();
                    plstrg+='_'+pl.ProductFamily__c.toUpperCase();
                   }
                  if(pl.Family__c!=null && pl.Family__c!=''){
                    plstr+='_'+pl.Family__c.toUpperCase();
                    plstrg+='_'+pl.Family__c.toUpperCase();
                  }
                  // To take Product Line with out Business Unit,ProductFamily and Family
                  if(pl.ProductLine__c!=null && pl.ProductLine__c!='' && (pl.ProductFamily__c==null || pl.ProductFamily__c=='') && (pl.Family__c==null || pl.Family__c=='')){
                       plstring='';// for 5 digit product Line
                       plstrng='';// for complete Product Line
                       plstring='_'+pl.ProductLine__c.Left(5).toUpperCase();
                       plstrng='_'+pl.ProductLine__c.toUpperCase();
                      if(plstring!='' && !(prodMapId.containsKey(plstring)))
                       prodMapId.put(plstring,pl);
                      if(plstrng!='' && !(prodMapId.containsKey(plstrng)))
                       prodMapId.put(plstrng,pl);
                   }
                   system.debug('######>>>>><<<<<<'+plstr+'>>>><<<<<<'+plstrg);
                  if(plstrg!='' && !(prodMapId.containsKey(plstrg)))
                       prodMapId.put(plstrg,pl);
                  if(plstr!='' && !(prodMapId.containsKey(plstr)))
                      prodMapId.put(plstr,pl);
              }
              system.debug('##########'+prodMapId);
              String linestr;
              for(LineItemWrapper lineItemWrapperRecord :lineItemWrapperRecords){
                  linestr='';
                  lineItemWrapperRecord.message=null;
                   //ContainsKey function of Map is case Sensitive if Key is String and hence converting key to Uppercase
                  if(lineItemWrapperRecord.opptyLine.ProductBU__c!=null && lineItemWrapperRecord.opptyLine.ProductBU__c!='')
                      linestr=lineItemWrapperRecord.opptyLine.ProductBU__c.toUpperCase();
                  if(lineItemWrapperRecord.opptyLine.ProductLine__c!=null && lineItemWrapperRecord.opptyLine.ProductLine__c!='')  
                      linestr+='_'+lineItemWrapperRecord.opptyLine.ProductLine__c.toUpperCase();
                   if(lineItemWrapperRecord.opptyLine.ProductFamily__c!=null && lineItemWrapperRecord.opptyLine.ProductFamily__c!='')   
                     linestr+= '_'+lineItemWrapperRecord.opptyLine.ProductFamily__c.toUpperCase();
                   if(lineItemWrapperRecord.opptyLine.Family__c!=null && lineItemWrapperRecord.opptyLine.Family__c!='')    
                     linestr+='_'+lineItemWrapperRecord.opptyLine.Family__c.toUpperCase();
                   linestr=linestr.trim();
                   system.debug('>>>>linestr>>>>>'+linestr);
                   if(lineItemWrapperRecord.opptyLine.ProductLine__c==null){
                       lineItemWrapperRecord.message=System.Label.CLMAR16SLS20;
                        hasNoLineItemErrors=false; 
                   }    
                     else if(prodMapId.containsKey(linestr)){
                     //Assign the values fetched from the table and what user has given excel. So that case will be same as loaded in the OPP_Product__c table
                          lineItemWrapperRecord.opptyLine.Product__c=prodMapId.get(linestr).Id; 
                          lineItemWrapperRecord.opptyLine.ProductBU__c=prodMapId.get(linestr).BusinessUnit__c; 
                          lineItemWrapperRecord.opptyLine.ProductLine__c=prodMapId.get(linestr).ProductLine__c; 
                          lineItemWrapperRecord.opptyLine.ProductFamily__c=prodMapId.get(linestr).ProductFamily__c; 
                          lineItemWrapperRecord.opptyLine.Family__c=prodMapId.get(linestr).Family__c;                    
                      }
                      else{
                       lineItemWrapperRecord.message=System.Label.CLMAR16SLS25;
                        hasNoLineItemErrors=false;                        
                      } 
              }
                displayReadOnlyFields=true;
          }  
          else{
              System.debug('>>>>>> Commercial reference loop >>>>');
              List<String> CRlist = new List<String>();
              for(LineItemWrapper lineItemWrapperRecord :lineItemWrapperRecords){
                      System.debug('!!!!!!!!!!!!!>>>>>'+lineItemWrapperRecord.opptyLine.TECH_CommercialReference__c);
                      CRlist.add(lineItemWrapperRecord.opptyLine.TECH_CommercialReference__c.trim());
              }
             List<Product2> prod2list = [select Id,Name,ProductCode,FamilyName__c,Description,ProductFamilyName__c,ProductLine__c,BusinessUnit__c,ProductFamilyId__c,Cache_BusinessUnit__c,Cache_ProductLine__c,Cache_ProductFamily__c,Cache_Family__c,ProductGDP__r.BusinessUnit__c,ProductGDP__r.ProductLine__c,ProductGDP__r.ProductFamily__c,ProductGDP__r.Family__c from Product2 where name in :CRlist and ProductGDP__c!=null];
              Map<String,Product2> prodMap = new Map<String,Product2>();
              Map<String,Product2> duplicateProdMap = new Map<String,Product2>();
              for(product2 pr:prod2list){
                      if(prodMap.containsKey(pr.NAme.toUpperCase()))
                          duplicateProdMap.put(pr.NAme.toUpperCase(),pr);
                     prodMap.put(pr.NAme.toUpperCase(),pr);
               }
             System.debug('>>>>>>>>>>prodMap'+prodMap);
             //ContainsKey function of Map is case Sensitive if Key is String and hence converting key to Uppercase
             for(LineItemWrapper lineItemWrapperRecord :lineItemWrapperRecords){
                 lineItemWrapperRecord.message=null;
                 Product2 prd=prodMap.get(lineItemWrapperRecord.opptyLine.TECH_CommercialReference__c.toUpperCase().trim());
                 if(prd==null){
                    lineItemWrapperRecord.message=System.Label.CLMAR16SLS25;
                    hasNoLineItemErrors=false; 
                 }
                 else if(duplicateProdMap.containsKey(prd.NAme.toUpperCase())){
                     lineItemWrapperRecord.message=System.Label.CLMAR16SLS21;
                    hasNoLineItemErrors=false;  
                                  
                 }
                 else{
                 //Avoiding dependency on cache field - Change done in Mar 2016 release
                     lineItemWrapperRecord.opptyLine.TECH_CommercialReference__c=prd.name;
                     /*lineItemWrapperRecord.opptyLine.ProductBU__c=prd.Cache_BusinessUnit__c;
                     lineItemWrapperRecord.opptyLine.ProductLine__c=prd.Cache_ProductLine__c;
                     lineItemWrapperRecord.opptyLine.ProductFamily__c=prd.Cache_ProductFamily__c;
                     lineItemWrapperRecord.opptyLine.Family__c=prd.Cache_Family__c ;*/
                     lineItemWrapperRecord.opptyLine.ProductBU__c=prd.ProductGDP__r.BusinessUnit__c;
                     lineItemWrapperRecord.opptyLine.ProductLine__c=prd.ProductGDP__r.ProductLine__c;
                     lineItemWrapperRecord.opptyLine.ProductFamily__c=prd.ProductGDP__r.ProductFamily__c;
                     lineItemWrapperRecord.opptyLine.Family__c=prd.ProductGDP__r.Family__c;
                     lineItemWrapperRecord.opptyLine.Product__c=prd.ProductFamilyId__c;
                     //lineItemWrapperRecord.opptyLine.ProductDescription__c=prd.Description;
                     lineItemWrapperRecord.opptyLine.ProductDescription__c=prd.ProductCode;
                }
             }
               displayReadOnlyFields=true; 
          }      
          
          return null;
        }
        
        public void delRow()
        {
            rowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('index'));
            lineItemWrapperRecords.remove(rowNum);   
        }   
        
        class LineItemWrapper
        {
            public OPP_ProductLine__c opptyLine{get;set;}
            public String status{get;set;}          
            public String message{get;set;}
            LineItemWrapper(OPP_ProductLine__c opptyLine,String status){
                this.opptyLine=opptyLine;
                this.status=status;
            }           
        }
        
        public PageReference proceedWithInsert(){
        List<OPP_ProductLine__c> tobeInsertedLineItems=new List<OPP_ProductLine__c>();
        for(LineItemWrapper liwrecord:lineItemWrapperRecords)
        {
            if(liwrecord.message!=null)
                    liwrecord.message=null;
                tobeInsertedLineItems.add(liwrecord.opptyLine);
        }

        if(tobeInsertedLineItems.size()>0)
        {
            List<Database.SaveResult> lstSaveresults=Database.insert(tobeInsertedLineItems,false);
            for(Integer i=0;i<lstSaveresults.size();i++)
            {
                if(lstSaveresults[i].isSuccess())
                {
                  lineItemWrapperRecords[i].status=System.Label.CLMAR16SLS27;
                  lineItemWrapperRecords[i].message='<a class="link" href="/'+lstSaveresults[i].getId()+'">'+System.Label.CLMAR16SLS26+'</a>';
                }
                else
                {
                    lineItemWrapperRecords[i].status=System.Label.CLMAR16SLS28;
                    lineItemWrapperRecords[i].message=lstSaveresults[i].getErrors()[0].getMessage();
                }
            }           
        }
        disableEverything=true;        
        return null;
        }
}