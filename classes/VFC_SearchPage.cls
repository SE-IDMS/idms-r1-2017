public with sharing class VFC_SearchPage 
{
    public string searchText{get;set;}
    public string selMetComp{get;set;}
    public List<ResultSet> searchedRecords{get;set;}
    public String pgMsg{get;set;}
    public String severity{get;set;} 
    public String sObjectName{get;set;} 
    public List<String> selCustLabel{get;set;}  
    Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();    
    List<String> accessibleFields = new List<String>(); 
    Map<String, CS_CustomLabels__c> customLabelslst= CS_CustomLabels__c.getAll();
    List<SelectOption> sObjectOptions;
    //Displays the Metadata types for search
    public List<SelectOption> getmetadataComponents() 
    {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('VFCClass','VFC Classes'));
            options.add(new SelectOption('APClass','AP Classes'));            
            options.add(new SelectOption('TestClass','Test Classes'));
            options.add(new SelectOption('BatchClass','Batch Classes'));
            options.add(new SelectOption('OtherClass','Other Classes'));            
            options.add(new SelectOption('Page','Visualforce Pages'));
            options.add(new SelectOption('EmailTemplate','Email Templates'));
            options.add(new SelectOption('Component','Components'));
            options.add(new SelectOption('Trigger','Triggers'));
            options.add(new SelectOption('Objects','Objects & Fields'));
            options.add(new SelectOption('CustomLabel','Custom Labels'));
            options.add(new SelectOption('ValidationRule','Validation Rules'));
            options.add(new SelectOption('WorkflowRule','Workflow Rules'));
            options.add(new SelectOption('WorkflowFieldUpdate','Workflow Field Updates'));
            
            return options;
    }
    //Wrapper Class to display the Result Set
    public class ResultSet
    {
        public String id{get;set;} 
        public String name{get;set;}
        public String compType{get;set;}
        public ResultSet(String idV, String nameV, String compTypeV)
        {
            this.id = idV;
            this.name = nameV+'';          
            this.compType = compTypeV;
        }       
    }
    public void search()
    {
        searchedRecords = new List<ResultSet>();
        if(selMetComp!=null)
        {
        if(selMetComp.containsIgnoreCase('Class') || selMetComp == 'Page' || selMetComp == 'EmailTemplate' || selMetComp == 'Trigger' || selMetComp == 'Component')
            queryRecords(selMetComp);
        if(sObjectName!=null && selMetComp == 'Objects')
            discoverAccessibleFields(gd.get(sObjectName)); 
        if((sObjectName!=null &&  selMetComp != 'Objects') || selMetComp == Label.CLAPR15SLS26) 
            queryMetadata(selMetComp,sObjectName);
        }
        system.debug('$$$$$$$$$$$ SearchedRecords $$$$$$$$$4' + searchedRecords);
        if(searchedRecords.size()==0)
        {
            pgMsg = 'No Records returned';
            severity = 'info';
        }
        else
            pgMsg = null;
    }
    
    public List<SelectOption> getObjectName()
    {
        if(sObjectOptions==null)
        {
            if(selMetComp == 'Objects' || selMetComp == Label.CLAPR15SLS23 || selMetComp == Label.CLAPR15SLS24 || selMetComp == Label.CLAPR15SLS25)
            {
                sObjectOptions = new List<SelectOption>();        
                for(Schema.SObjectType f : gd.values())
                {
                   Schema.DescribeSObjectResult d = f.getDescribe();
                   if (!d.isCustomSetting() && d.getRecordTypeInfos().size() > 0 && d.isCreateable() && !d.getname().containsignorecase('history') && !d.getname().containsignorecase('tag') && !d.getname().containsignorecase('share') && !d.getname().containsignorecase('feed')) 
                   {
                      if(sObjectOptions.size()<1000)
                          sObjectOptions.add(new SelectOption(d.getName(), d.getLabel()));
                   }
                }
                sObjectOptions.sort();            
            }
        }        
        return sObjectOptions;            
    }
    
    //Searches in Custom Fields for the selected Sobject
     private void discoverAccessibleFields(Schema.SobjectType newObj) 
     {
        this.accessibleFields = new List<String>();
        Set<String> fieldNames = new Set<String>();
        Map<String, Schema.SobjectField> fields = newObj.getDescribe().fields.getMap();

        for (String s : fields.keySet()) 
        {
            Schema.DescribeFieldResult fieldRes = fields.get(s).getDescribe();
                //Check if the field label/Formula/Default value contains the search text
                if((fieldRes.getCalculatedFormula()!=null && (fieldRes.getCalculatedFormula().containsIgnoreCase(searchtext.toLowerCase()))) ||  (fieldRes.getDefaultValueFormula()!=null && (fieldRes.getDefaultValueFormula().containsIgnoreCase(searchtext.toLowerCase())))  || fieldRes.getLabel().containsIgnoreCase(searchtext.toLowerCase()) || fieldRes.getName().containsIgnoreCase(searchtext.toLowerCase()))
                {
                    if((!(fieldNames.contains(newObj+'.'+fieldRes.getName()))) && searchedRecords.size()<1000)
                    {
                        searchedRecords.add(new VFC_SearchPage.ResultSet('', newObj+'.'+fieldRes.getName(),'Fields'));
                        fieldNames.add(newObj+'.'+fieldRes.getName());
                    }
                }
                //Checks if the picklist value of the field contains the search text
                for(Schema.PicklistEntry pe: fieldRes.getPicklistValues()) 
                {
                    if((pe.getValue().containsIgnoreCase(searchtext.toLowerCase()) || pe.getLabel().containsIgnoreCase(searchtext.toLowerCase())) && (!(fieldNames.contains(newObj+'.'+fieldRes.getName())))  && searchedRecords.size()<1000)
                    {
                        fieldNames.add(newObj+'.'+fieldRes.getName());
                        searchedRecords.add(new VFC_SearchPage.ResultSet('', newObj+'.'+fieldRes.getName(),'Fields'));
                    }
                }
        }
    }
   
    public void queryRecords(String searchObj)
    {
            if(searchObj.containsIgnoreCase('Class'))
            {
                List<ApexClass> classes;
                if(!Test.IsRunningTest())
                {
                    if(searchObj == 'VFCClass')
                        classes = Database.query(Label.CLAPR15SLS17 +  ' and (Name LIKE \'VFC_%\')');  
                    if(searchObj == 'APClass')
                        classes = Database.query(Label.CLAPR15SLS17 +  ' and (Name LIKE \'AP_%\')');  
                    if(searchObj == 'TESTClass')
                        classes = Database.query(Label.CLAPR15SLS17 +  ' and (Name LIKE \'%_TEST\')');      
                    if(searchObj == 'BATCHClass')
                        classes = Database.query(Label.CLAPR15SLS17 +  ' and (Name LIKE \'BATCH_%\')');      
                    if(searchObj == 'OtherClass')
                        classes = Database.query(Label.CLAPR15SLS17 +  ' and (NOT Name LIKE \'BATCH_%\')' +  ' and (NOT Name LIKE \'VFC_%\')' +  ' and (NOT Name LIKE \'AP_%\')'+  ' and (NOT Name LIKE \'%_TEST\')');          
                }
                else
                    classes = new WS_MOCK_SEARCHPAGE_TEST('apexClass').apexClasses();
                for(ApexClass ac: classes)
                {
                    if(searchtext!=null && ac.body.containsIgnoreCase(searchtext.toLowerCase()))
                    {
                        if(searchedRecords.size()<Integer.ValueOf(Label.CLAPR15SLS15))
                        searchedRecords.add(new VFC_SearchPage.ResultSet(ac.Id, ac.Name,Label.CLAPR15SLS42));
                    }
                }                
            }
            if(searchObj == 'EmailTemplate')
            {
                List<EmailTemplate> emailTemps;
                if(!Test.IsRunningTest())
                {
                    emailTemps = Database.query(Label.CLAPR15SLS18);  
                }
                else
                    emailTemps = new WS_MOCK_SEARCHPAGE_TEST('emailTemps').emailTemps();
                for(EmailTemplate em: emailTemps)
                {
                    if(searchtext!=null && ((em.body!=null && em.body.containsIgnoreCase(searchtext.toLowerCase())) || (em.subject!=null && em.subject.containsIgnoreCase(searchtext.toLowerCase()))))
                    {
                        if(searchedRecords.size()<Integer.ValueOf(Label.CLAPR15SLS15))
                        searchedRecords.add(new VFC_SearchPage.ResultSet(em.Id, em.Name,Label.CLAPR15SLS41));
                    }
                }
            }
            if(searchObj == 'Trigger')
            {
                List<ApexTrigger> triggers;
                if(!Test.IsRunningTest())
                {
                    triggers = Database.query(Label.CLAPR15SLS19); 
                }
                else
                    triggers = new WS_MOCK_SEARCHPAGE_TEST('triggers').triggers(); 
                for(ApexTrigger ac: triggers)
                {
                    if(searchtext!=null && ac.body.containsIgnoreCase(searchtext.toLowerCase()))
                    {
                        if(searchedRecords.size()<Integer.ValueOf(Label.CLAPR15SLS15))
                        searchedRecords.add(new VFC_SearchPage.ResultSet(ac.Id, ac.Name,Label.CLAPR15SLS43));
                    }
                }
            }
            if(searchObj == 'Page')
            {
                List<ApexPage> pages;
                if(!Test.IsRunningTest())
                {
                    pages = database.query(Label.CLAPR15SLS20);
                }
                else
                    pages= new WS_MOCK_SEARCHPAGE_TEST('apexPage').pages();
                for(ApexPage ap: pages)
                {
                    if(searchtext!=null && ap.markup.containsIgnoreCase(searchtext.toLowerCase()))
                    {
                        if(searchedRecords.size()<Integer.ValueOf(Label.CLAPR15SLS15))
                        searchedRecords.add(new VFC_SearchPage.ResultSet(aP.Id, ap.Name,Label.CLAPR15SLS44));
                    }
                }
            }
            if(searchObj == 'Component')
            {
                List<ApexComponent> components;
                if(!Test.IsRunningTest())
                {
                    components = database.query(Label.CLAPR15SLS21);
                }
                else
                    components= new WS_MOCK_SEARCHPAGE_TEST('components').components();
                for(ApexComponent acomp: components)
                {
                    if(searchtext!=null && acomp.markup.containsIgnoreCase(searchtext.toLowerCase()))
                    {
                        if(searchedRecords.size()<Integer.ValueOf(Label.CLAPR15SLS15))
                        searchedRecords.add(new VFC_SearchPage.ResultSet(acomp.Id, acomp.Name,Label.CLAPR15SLS45));
                    }
                }
            }
            system.debug(searchedRecords + '$$$$$$$$Searched Records $$$$$$$$$4');                           
    }
    // Create the Metadata API service stub and authenticate
    private static MetadataService.MetadataPort createService()
    { 
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        service.timeout_x = Integer.ValueOf(Label.CLAPR15SLS16);
        return service;     
    }
    //Displays the list of custom labels to search for
    public List<SelectOption> getcustLabels() 
    {
            List<SelectOption> options;
            if(selMetComp == 'CustomLabel')
            {
                options = new List<SelectOption>();
                for(String custLab : customLabelslst.keyset())
                {
                    options.add(new SelectOption(custLab,custLab));
                }
            }            
            return options;
    }
    //The method queries the metadata for the search string
    public void queryMetadata(String metadataType,String selsObjectValue)
    {
         if(metadataType == Label.CLAPR15SLS23 || metadataType==Label.CLAPR15SLS24 || metadatatype ==Label.CLAPR15SLS25 || metadatatype ==Label.CLAPR15SLS26)
         {
            MetadataService.MetadataPort service = createService();             
            List<MetadataService.ListMetadataQuery> queries = new List<MetadataService.ListMetadataQuery>();        
            MetadataService.ListMetadataQuery queryLayout = new MetadataService.ListMetadataQuery();
            queryLayout.type_x = metadataType;
            queries.add(queryLayout); 
            MetadataService.FileProperties[] fileProperties;

            if(Limits.getHeapSize()<Limits.getLimitHeapSize() && Limits.getCallouts()<Limits.getLimitCallOuts())  
            {                
                if(metadatatype !=Label.CLAPR15SLS26)
                    fileProperties = service.listMetadata(queries, 30);
                
                Map<String,string> fullNames = new Map<String,String>();
                if(fileProperties!=null || metadatatype ==Label.CLAPR15SLS26)
                {
                    Set<String> tempList = new Set<String>();
                    if(metadatatype !=Label.CLAPR15SLS26)
                    {
                        for(MetadataService.FileProperties fp: fileProperties)
                        {
                            if(fp.FullName!=null)
                                fullNames.put(fp.FullName,fp.ManageableState);
                        }
                    }
                    system.debug(selCustLabel+'$$$$ selCustLabel $$$$$');
                    if(selCustLabel!=null)
                    {
                        for(String custLab: selCustLabel)
                        {
                            for(Integer i=1;i<Integer.valueOf(Label.CLAPR15SLS65);i++)
                            {
                                if(i<10)
                                    fullNames.put(custLab+'0'+String.valueOf(i), 'unmanaged');
                                else
                                    fullNames.put(custLab+String.valueOf(i), 'unmanaged');
                            }
                        }
                    }
                    
                    system.debug(fullNames+'$$$$ fullNames $$$$$');
                    Boolean maxValue = false;
                    for(String s: fullNames.Keyset())
                    {
                           if(Limits.getCpuTime()<Limits.getLimitCpuTime())
                           {
                                if(tempList!=null && tempList.size()<Integer.valueOf(Label.CLAPR15SLS28))
                                {
                                        if(fullNames.get(s) == 'unmanaged')
                                        {                   
                                            if(selsObjectValue!=null)
                                            {                                            
                                                    if(s.IndexOf('.')!=-1 && selsObjectValue==s.substring(0,s.IndexOf('.')))
                                                    {
                                                        tempList.add(s);
                                                    }                                            
                                            }    
                                            else
                                            {
                                                tempList.add(s);
                                            }                                        
                                        } 
                                        if(tempList.size() == Integer.valueOf(Label.CLAPR15SLS28)-1)                          
                                            maxValue = true;
                           }
                           if(tempList.size()>=Integer.valueOf(Label.CLAPR15SLS28)  || maxValue==false)
                           {
                                system.debug(tempList+'<-------------Temp List ----------->');
                                MetadataService.IReadResult resultValue;
                                List<String> tempListFinal = new List<String>();
                                
                                if(Limits.getCallouts()<Limits.getLimitCallOuts() && (!(tempList.isEmpty())))
                                {
                                    tempListFinal.addAll(tempList);
                                    if(!Test.isRunningTest())
                                    resultValue = service.readMetadata(metadataType,tempListFinal); 
                                    else
                                    resultValue  = new WS_MOCK_SEARCHPAGE_TEST('ReadMetadata').valRuleReadResult();                              
                                    system.debug(resultValue+'$$$$$$$ ResultValue $$$$$$$$$4');
                                }                                
                                tempList.clear();
                                if(resultValue!=null)
                                {
                                    for(MetadataService.Metadata md: resultValue.getRecords())
                                    {                                        
                                        //Searches in the custom label
                                        if(metadataType == Label.CLAPR15SLS26)
                                        {
                                            MetadataService.CustomLabel cl = new MetadataService.CustomLabel();
                                            if(cl!=null)
                                            {
                                                cl = (MetadataService.CustomLabel)md; 
                                                if(cl.fullName!=null && cl.value!=null && cl.value.containsIgnoreCase(searchtext.toLowerCase()))
                                                searchedRecords.add(new VFC_SearchPage.ResultSet('', cl.fullName,Label.CLAPR15SLS46));
                                            }
                                        }

                                        //Searches for the search text in validation rule
                                        if(metadataType == Label.CLAPR15SLS23)
                                        {
                                            MetadataService.ValidationRule vr = new MetadataService.ValidationRule();
                                            if(vr!=null)
                                            {
                                                vr = (MetadataService.ValidationRule)md;                         
                                                if(vr.errorConditionFormula!=null && vr.errorConditionFormula.containsIgnoreCase(searchtext.toLowerCase()))
                                                {    
                                                    searchedRecords.add(new VFC_SearchPage.ResultSet('', vr.fullName,label.CLAPR15SLS47));
                                                }
                                            }
                                        }
                                        //Searches for the search text in workflow rule
                                        if(metadataType == Label.CLAPR15SLS24)
                                        {
                                            MetadataService.WorkflowRule wr = new MetadataService.WorkflowRule();
                                            if(wr!=null)
                                            {
                                                wr = (MetadataService.WorkflowRule)md;
                                                if(wr.formula!=null && wr.formula.containsIgnoreCase(searchtext.toLowerCase()))
                                                    searchedRecords.add(new VFC_SearchPage.ResultSet('', wr.fullName,Label.CLAPR15SLS48));                                                 
                                            }
                                        }
                                        //Searches for the search text in workflow field update                                         
                                        if(metadataType == Label.CLAPR15SLS25)
                                        {
                                            MetadataService.WorkflowFieldUpdate wfU  = new MetadataService.WorkflowFieldUpdate();
                                            if(wfU!=null)
                                            {
                                                wfU = (MetadataService.WorkflowFieldUpdate)md;
                                                if((wfU.formula!=null && wfU.formula.containsIgnoreCase(searchtext.toLowerCase())) || (wfU.literalValue!=null && wfU.literalValue.containsIgnoreCase(searchtext.toLowerCase())))
                                                {    
                                                    searchedRecords.add(new VFC_SearchPage.ResultSet('', wfU.fullName,Label.CLAPR15SLS49));
                                                }
                                            }
                                        }
                                    }
                                }
                                system.debug('$$$$$$$$$$$ SearchedRecords $$$$$$$$$4' + searchedRecords);
                            }
                            }                          
                        }          
                    } 
             }
         }
    }
}