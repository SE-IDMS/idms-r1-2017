/*    Author          : Accenture Team
      Date Created    : 24/10/2011
      Description     : Controller extensions for VFC61_OpptyClone. */

public without sharing class VFC61_OpptyClone extends VFC_ControllerBase
{
    public Opportunity oppor{get;set;}{oppor=new Opportunity();}
    public List<String> filters{get;set;}
    public static boolean isOpptyClone = false;
    public Boolean flag{get;set;}
    public String pgeMsg{get;set;}
    public Boolean display{get;set;}
    public Boolean selected{get;set;}
    public Boolean interim{get;set;}
    private static Savepoint sp;
    private static Savepoint spoppty;
    public list<ChildRecordSelection> childRecords{get;set;}
    private Set<String> selectedRecords;
    public Map<String,String> records;
    private static list<OpportunityTeamMember> opportunityTeamMembers = new list<OpportunityTeamMember>();
    private set<string> users = new set<string>();
    private Boolean rolledBack;
    private boolean oppTeamMembers;
    private Id overlaySplitId,revenueSplitId;
    private Id oldOwnerId;
    public Opportunity oppty=new Opportunity();
    public VFC61_OpptyClone(ApexPages.StandardController controller)
    {
        oppor = (Opportunity)controller.getRecord();
        oppor.RecordTypeId=System.Label.CLOCT13SLS16;
      //  oppor.OpportunitySource__c=System.Label.CLOCT13SLS04;             
        system.debug('opportunity'+oppor);
        filters = new List<String>();
        display=true;
        interim=false;
        oldOwnerId = oppor.OwnerId;
        List<OpportunitySplitType> splitTypes = [select Id,DeveloperName from OpportunitySplitType where DeveloperName =: Label.CLAPR15SLS70 OR DeveloperName =:Label.CLAPR15SLS85];
        for(OpportunitySplitType os: splitTypes)
        {
             if(os.DeveloperName == Label.CLAPR15SLS70)
                 overlaySplitId = os.Id;
             if(os.DeveloperName == Label.CLAPR15SLS85)
                 revenueSplitId = os.Id;                 
        }
        if(oppor.OpptyType__c!=null)
        {
            filters.add(oppor.OpptyType__c);
        }
        oppty=[select Project__r.ProjectStatus__c from Opportunity where Id=:oppor.Id];
            System.debug('>>>>oppty'+oppty );
    }
    //Establish component controller communication
    // public VCC05_DisplaySearchFields searchController
    // {
    //     set;
    //     get
    //     {
    //         if(getcomponentControllerMap()!=null)
    //         {
    //             VCC05_DisplaySearchFields displaySearchFields;
    //             displaySearchFields = (VCC05_DisplaySearchFields)getcomponentControllerMap().get('searchComponent');
    //             system.debug('--------displaySearchFields-------'+displaySearchFields );
    //             if(displaySearchFields!= null)
    //             return displaySearchFields;
    //         }
    //         return new VCC05_DisplaySearchFields();
    //     }
    // }
    //checks if the logged in user has access to clone the record
    public pageReference cloneOpportunity()
    {
         flag = true; // Boolean to flag if the current user can create an opportunity
         System.debug('>>>>>cloneOpportunity got called');
         // Retrieve the Describe sObject Result for Opportunity
         Schema.DescribeSObjectResult opportunitySobject = Opportunity.SObjectType.getDescribe();
         
         // If the current user cannot create opportunities
         if(!opportunitySobject.isCreateable())
         {
              flag = false;
              /* upadted by Accenture IDC FOR EUS */
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,label.CL00563));
             //End Of Update
         }
         //Map<String, Schema.SObjectField> M = Schema.SObjectType.Opportunity.fields.getMap();
         
         // Map<String, Schema.SObjectField> M = Schema.SObjectType.Opportunity.fields.getMap();        
        if(flag)
        {
            //To ensure Close Date VR dont fire when record is getting cloned.
            sp = Database.setSavepoint(); //created a savepoint for roll back
            String stagename=oppor.Stagename;
            if(oppor.CloseDate<system.today()){
            	oppor.CloseDate = system.today();            
                oppor.Stagename= Label.CLMAR16SLS01;
            }            
            Database.saveResult sv = Database.update(oppor,false); //updating the existing opportunity
            oppor.Stagename=stagename;
            Database.rollback(sp); //rolling back 
            if(!sv.issuccess())
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,sv.getErrors()[0].getMessage()));
                flag= false;
            }
        }
         System.debug('cloneOpportunity Method Ends');
         return null;
    }
    //queries the share Object for Opprtunity Team members
    public list<OpportunityTeamMember> oppShare()
    {
        system.debug('oppShare Method Begins');
        String opportunityTeamQuery = CS005_Queries__c.getValues('CS005_12').StaticQuery__c+' '+ 'where opportunityID=\''+oppor.id+'\''; //Querying OpportunityTeam fields.
        list<OpportunityTeamMember> oppteam = Database.Query(opportunityTeamQuery);
        List<Id> userIds = new List<Id>();
        List<OpportunityTeamMember> activeTeamMembers = new List<OpportunityTeamMember>();
        
        if(oppteam!=null && !oppteam.isEmpty())
        {
           /* Modifed for BR-5617 */
           //Add only the active team members to the Opportunity Team while cloning
           for(OpportunityTeamMember otm: oppteam)
           {
               userIds.add(otm.UserId);
           }
           //Queries for active users
           Map<Id, User> activeUsers = new Map<Id,User>([select Id, IsActive from User where Id in: userIds and IsActive=:true]);
           Boolean hasInactiveUser = false;
           for(OpportunityTeamMember otm: oppteam)
           {
               if(activeUsers.get(otm.UserId)!=null)
                   activeTeamMembers.add(otm);
               else
                   hasInactiveUser = true;                   
           }
           if(hasInactiveUser == true)
               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,Label.CLOCT14SLS08));
           
           if(activeTeamMembers!=null && !activeTeamMembers.isEmpty())
               return activeTeamMembers;
           else
               return new list<OpportunityTeamMember>();
           
           /* End of Modification for BR-5617 */
        }
        else
           return new list<OpportunityTeamMember>();
    }
    //checks if the user has selected an appropriate stage and type.
    Public pagereference proceedChildSelection()
    {
        system.debug('proceedChildSelection Method Begins');
            // if((searchController.level1.equalsIgnoreCase(Label.CL00355))||(searchController.level2.equalsIgnoreCase(Label.CL00355)))
            // {
            //    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.CL00650));
            // }
            // else
            // {
                display=false;
                getchildRecords();
            // }
        System.debug('proceedChildSelection Method Ends');
        return null;
    }
    //selection of child records to clone
    public pageReference proceedClone()
    {
        system.debug('proceedClone Method Begins');
        interim=true;
        //Added for March 2016 Release - DEF-9452
        if(oppty.Project__r.ProjectStatus__c==Label.CLMAR16SLS53)
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CLMAR16SLS54));
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00648));
        selectedRecords = new Set<String>();
        if(childRecords!=null && (!(childRecords.isEmpty())))
        {
            for(ChildRecordSelection childSelected:childRecords)
            {
                if(childSelected.selected==true)
                {
                    selectedRecords.add(childSelected.records);
                }
            }
        }
        /* Modified for BR-5617 */
        if(selectedRecords!=null && (!(selectedRecords.isEmpty())) && selectedRecords.contains(Label.CL00661))
            opportunityTeamMembers = oppShare();
        
        /* End of Modification for BR-5617 */
        System.debug('proceedClone Method Ends');
        return null;
    }
    //cloning the opportunity record with selected child records.
    public pageReference continueClone()
    {
        spoppty = Database.setSavepoint();
        rolledBack=false;
        system.debug('continueClone Method Begins');                    
        String query1 = Utils_Methods.generateSelectAllQuery(oppor);
        String query2 = ' '+'where id =\''+oppor.id+'\'';
        String query = query1 + query2;
        List<opportunity> currentRecords = Database.Query(query);
        Opportunity clonedOpportunity = new Opportunity();
        list<Id> insertRecordId = new List<Id>();
        Map<STring, string> stageForecastCategory = new Map<String, String>();
        if(currentRecords!=null && !currentRecords.isEmpty())
        {
            for(OpportunityStage oppstage:[SELECT DefaultProbability,ForecastCategoryName,MasterLabel FROM OpportunityStage WHERE IsActive = true])
            {
                stageForecastCategory.put(oppstage.MasterLabel,oppstage.ForecastCategoryName);           
            }
            //JFA: test modif ".clone"
            clonedOpportunity = currentRecords[0].clone();
            //clonedOpportunity = currentRecords[0];
            // End modif
            clonedOpportunity.TECH_ChiTuOpportunityID__c=NULL; //For BR-9740
            clonedOpportunity.OpptyType__c=oppor.OpptyType__c;
            clonedOpportunity.StageName=oppor.StageName;
            clonedOpportunity.ForecastCategoryName = stageForecastCategory.get(oppor.StageName);
            clonedOpportunity.OpportunitySource__c=oppor.OpportunitySource__c;
            clonedOpportunity.TECH_TriggerVR__c = false;
            clonedOpportunity.OpportunityScope__c= NULL;                    
            clonedOpportunity.ActualQualification__c=NULL;
            clonedOpportunity.ActualQuoteInvestDecision__c=NULL;
            clonedOpportunity.ActualQuoteSubmitDecision__c=NULL;
            clonedOpportunity.PlannedQualification__c=NULL;
            clonedOpportunity.PlannedQuoteInvestDecision__c=NULL;
            clonedOpportunity.PlannedQuoteSubmitDecision__c=NULL;
            //PromisedDeliveryDateForQuotation__C nullifying has been added for BR-9761 - June 2016 release
            clonedOpportunity.PromisedDeliveryDateForQuotation__c=NULL;
            clonedOpportunity.QuoteIssuanceDateToCustomer__c=NULL;
            clonedOpportunity.QuoteSubmissionTime__c=NULL;
            clonedOpportunity.QuoteSubmissionDate__c=NULL;
            clonedOpportunity.Status__c = NULL;
            clonedOpportunity.Reason__c = NULL;                    
            //clonedOpportunity.BusinessMix__c=NULL;
            clonedOpportunity.CloseDate = system.today();
            clonedOpportunity.TECH_CreatedFromLead__c = False;
            system.debug('#### NewOpp Old OwnnerID '+clonedOpportunity.OwnerId);
            clonedOpportunity.OwnerId = UserInfo.getUserId();
            system.debug('#### NewOpp New OwnnerID '+clonedOpportunity.OwnerId);
            clonedOpportunity.OpportunityDetector__c = UserInfo.getUserId();
            clonedOpportunity.TECH_CrossProcessConversionAmount__c= null;
            clonedOpportunity.Amount=0;
            clonedOpportunity.ProjectCategoryP0__c = NULL;
            clonedOpportunity.ProjectCategoryS0__c = NULL;
            clonedOpportunity.ProjectCategoryS1__c = NULL;
            //RFQReceivedOn__c nullifying has been added for DEF-9383 - March 2016 release
            clonedOpportunity.RFQReceivedOn__c= NULL;
            //Added for March 2016 release -DEF-9452
            if(oppty.Project__r.ProjectStatus__c==Label.CLMAR16SLS53)
                clonedOpportunity.project__c= NULL;
             //Added for March 2016 release - DEF-9447
             if(clonedOpportunity.OpportunityCategory__c==Label.CLMAR16SLS55)
                 clonedOpportunity.OpportunityCategory__c=Label.CLMAR16SLS56;
            //modif JFA proba
            clonedOpportunity.probability = null;
            //end of modif    
            if(((currentRecords[0].name).length()+label.CL00651.length())<120)
                clonedOpportunity.name=currentRecords[0].name+label.CL00651;
            else
            {
                clonedOpportunity.name=currentRecords[0].name.substring(0,((currentRecords[0].name).length()-(((currentRecords[0].name).length()+label.CL00651.length())-120)))+label.CL00651;
            }
            /* Modified By ACCENTURE IDC
            Modifed for Defect - DEF-0386
            Modified Date: 06/12/2011
            */
            if(oppor.OpptyType__c!=clonedOpportunity.OpptyType__c)
                clonedOpportunity.OpportunityScope__c=null;            //Blank out the Opportunity Scope if the user selected Opportunity type(during cloning) is not same as the Master Opportunity Type 
                    //End of Modification
            clonedOpportunity.TECH_SendEmailOnLeadConv__c=false;
        }
        if(clonedOpportunity!=null)
        {
            System.debug('#### Inserting new Opp');
            isOpptyClone = true;
            Database.saveResult sv = Database.insert(clonedOpportunity,false); //inserting the cloned opportunity
            System.debug('#### New Opp inserted: '+clonedOpportunity.id);
            if(!sv.issuccess())
            {
                System.debug('#### Error');
                rolledBack=true;
                pgeMsg = sv.getErrors()[0].getMessage();
                System.debug('### Error:'+ pgeMsg);                
            }                    
        }
        if(!rolledBack)
        {
            System.debug('#### Continue - next is SalesContributor');
            System.debug('#### Selected records: '+selectedRecords);
            // SalesContributor was not checked by the user, so adding the current user to Sales Contributor with 100% contribution
            //splitTypeId = [select Id from OpportunitySplitType where DeveloperName=: Label.CLAPR15SLS70].Id;
            if(selectedRecords.isEmpty() || !selectedRecords.contains(sObjectType.OpportunitySplit.label))
            {
                System.debug('#### SalesContributor was not checked by the user, so adding the current user to Sales Contributor with 100% contribution');
                /*OpportunitySplit newSplit = new OpportunitySplit();
                newSplit.OpportunityId = clonedOpportunity.id;
                newSplit.SplitPercentage = 100;
                newSplit.SplitOwnerId =clonedOpportunity.ownerId;
                newSplit.SplitTypeId = splitTypeId;
                System.debug('#### Sales contributor user: '+newSplit.SplitOwnerId);
                System.debug('#### Original opp owner id: '+currentRecords[0].ownerId);
                System.debug('#### Cloned oppty owner id: '+clonedOpportunity.ownerId);
                newSplit.TECH_IsCloned__c=true;
                newSplit.Role__c=Label.CL00265;
                System.debug('#### Going to insert Sales contributor: '+newSplit);
                Database.saveResult svr = database.insert(newSplit,false);
                System.debug('#### Sales contributor inserted');
                if(!svr.isSuccess())
                {
                    System.debug('#### Error - failed to insert salescontributor: '+svr.getErrors()[0].getMessage());
                    rolledBack=true;
                    pgeMsg = svr.getErrors()[0].getMessage();
                }*/
            }
            System.debug('#### Selected records: '+selectedRecords);
            //MODIFIED FOR BR-5772
            if(!rolledBack && selectedRecords!=null && !selectedRecords.isEmpty() && selectedRecords.contains(sObjectType.OpportunitySplit.label))
            {                
                System.debug('#### SalesContributor was checked by the user, so adding the Sales Contributors list as on the original opportunity' );
                String querySales1 = Utils_Methods.generateSelectAllQuery(new OpportunitySplit());
                String querySales2 = ' '+'where OpportunityId =\''+oppor.id+'\'';
                String querySales = querySales1 + querySales2;
                System.debug('#### SalesContributor query: '+querySales);             
                list<OpportunitySplit> splitRecords =  new list<OpportunitySplit>();             
                List<OpportunitySplit> currentsplitUsers = Database.Query(querySales);
                for(OpportunitySplit contributors : currentsplitUsers)
                {
                    if(currentsplitUsers.size()>1)
                    {                        
                        if(oldOwnerId == clonedOpportunity.OwnerId)
                        {
                            OpportunitySplit contribut = new  OpportunitySplit();
                            contribut = contributors.clone(false);
                            contribut.OpportunityId = clonedOpportunity.id;
                            contribut.splitTypeId = contributors.splitTypeId;
                            contribut.TECH_IsCloned__c=true;
                            splitRecords.add(contribut);
                        }
                        else if(oldOwnerId!= clonedOpportunity.OwnerId)
                        {
                            if(contributors.SplitOwnerId!= oldOwnerId)
                            {
                                    OpportunitySplit contribut = new  OpportunitySplit();
                                    contribut = contributors.clone(false);
                                    contribut.OpportunityId = clonedOpportunity.id;
                                    contribut.splitTypeId = contributors.splitTypeId;
                                    contribut.TECH_IsCloned__c=true;
                                    splitRecords.add(contribut);
                            }
                        }
                    }
                }
                if(splitRecords!=null && !splitRecords.isEmpty())
                {
                    try
                    {
                        system.debug(splitRecords+ '$$$$$$$$$ Split Records');
                        Database.saveResult[] srList = Database.insert(splitRecords,false);

                        for (Database.saveResult sr : srList) 
                        {
                            if(!(sr.isSuccess()))
                            {
                                for(Database.Error err : sr.getErrors()) 
                                {
                                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage());
                                    ApexPages.addMessage(myMsg);
                                }
                            }
                        }                        
                        selectedRecords.remove(sObjectType.OpportunitySplit.label);
                    }
                    catch(Exception e)
                    {
                        rolledBack= true;
                        pgeMsg=e.getMessage();
                    }
                }
            }
            System.debug('#### Selected records: '+selectedRecords);
            if(!rolledBack && selectedRecords!=null && !selectedRecords.isEmpty())
            {
                //getting the child relationships
                List<Schema.ChildRelationship> allchildRecords = oppor.getSobjectType().getDescribe().getChildRelationships();
                List<Schema.ChildRelationship> childRecordsToClone = new List<Schema.ChildRelationship>();
                list<OpportunityShare> clonedOpportunityShare = new list<OpportunityShare>();
                list<OpportunityTeamMember> clonedOppTeamMembers= new list<OpportunityTeamMember>();
                Map<string,Schema.ChildRelationship> relations = new Map<string,Schema.ChildRelationship>();
                if(!allchildRecords.isEmpty() && allchildRecords!=null)
                {
                    for(Schema.ChildRelationship child : allchildRecords)
                    {
                        relations.put(String.ValueOf(child.getChildSObject()),child);
                    }
                    System.debug('#### relations: '+relations);
                    Boolean oppLineSel  = false;
                    Boolean vcpSel  = false;
                    for(string selected:selectedRecords)
                    {
                        if(!((selected == sObjectType.OPP_ProductLine__c.label) || (selected == sObjectType.OPP_ValueChainPlayers__c.label) || (sObjectType.OpportunityTeamMember.label.contains(selected)) || (sObjectType.OpportunitySplit.label.contains(selected))))
                        {
                            childRecordsToClone.add(relations.get(records.get(selected)));
                        }
                        if(selected == sObjectType.OPP_ProductLine__c.label)
                            oppLineSel = true;
                        if(selected == sObjectType.OPP_ValueChainPlayers__c.label)
                            vcpSel = true;
                        if(sObjectType.OpportunityTeamMember.label.contains(selected))
                            oppTeamMembers = true;
                    }
                    if(vcpSel == true)
                    {
                        system.debug('Cloning of Value chain player begins');
                        OPP_ValueChainPlayers__c vcp=new OPP_ValueChainPlayers__c(); 
                        List<OPP_ValueChainPlayers__c> newParentVCPlist = new List<OPP_ValueChainPlayers__c>();
                        String vcpQuery1 = Utils_Methods.generateSelectAllQuery(vcp);
                        String vcpquery2 = ' '+'where OpportunityName__c =\''+oppor.id+'\' AND ParentValueChainPlayer__c = '+NULL+' AND Tobedeleted__c = '+FALSE;
                        String vcpquery = vcpQuery1 + vcpquery2;
                        System.debug('VCPLinequery>>>>'+vcpquery);
                        List<OPP_ValueChainPlayers__c> existingVCPs = Database.Query(vcpquery);
                        for(OPP_ValueChainPlayers__c oldVCP: existingVCPs)
                        {
                            vcp = oldVCP.clone(false, true);
                            vcp.OpportunityName__c = clonedOpportunity.Id;
                            newParentVCPlist.add(vcp);
                        }
                        try{
                            Database.insert(newParentVCPlist,true);
                       }
                       Catch(Exception e){
                           rolledBack= true;
                           pgeMsg=e.getMessage();
                       }
                    }
                    System.debug('#### childRecordsToClone: '+childRecordsToClone);
                    if(oppLineSel ==true)
                    {
                        system.debug('Cloning of Product Line Begins');
                        Map<Id, Id> oldNewProdLines = new Map<Id, Id>();
                        List<OPP_ProductLine__c> oldParentLines = new List<OPP_ProductLine__c>();
                        List<OPP_ProductLine__c> newParentLines = new List<OPP_ProductLine__c>();
                        Map<Id, Id> childParentLne = new Map<Id, Id>(); 
                        OPP_ProductLine__c pl=new OPP_ProductLine__c(); 
                        String productLineQuery1 = Utils_Methods.generateSelectAllQuery(pl);
                        String productLinequery2 = ' '+'where Opportunity__c =\''+oppor.id+'\''+' AND LineStatus__c!=\'To be deleted\' AND LineType__c=\'Parent Line\'';
                        String productLinequery = productLineQuery1 + productLinequery2;
                        System.debug('productLinequery>>>>'+productLinequery);
                        List<OPP_ProductLine__c> oldProdLines = Database.Query(productLinequery);
                        
                        /* List<OPP_ProductLine__c> oldProdLines = [select Id, IsDeleted, Name, CurrencyIsoCode, RecordTypeId, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, MayEdit, IsLocked, Opportunity__c, Amount__c, DeliveryDate__c, Designation__c, Product__c, Quantity__c, TECH_AmountInNumber__c, Type__c, UnitForQuantity__c, BusbarArrangement__c, BusbarCurrent__c, ControlMonitoring__c, HighestVoltageLevel__c, PLContact__c, ShortRatedCurrent__c, SupplyingPlant__c, TotalMVA__c, UnitaryPowerEachTransformerMVA__c, VoltageLevel__c, CheckedbyGMR__c, CommercialReference__c, Family__c, ProductBU__c, ProductDescription__c, ProductFamily__c, ProductLine__c, TECH_CommercialReference__c, Specified__c, IsASynergy__c, SolutionCenter__c, Commission__c, ConcatenatedID__c, ConfigurationID__c,  CrossSellingLineAmount__c, CrossSellingLineWeighted__c, ITBRevenueLineIdOld__c, ITBRevenueLineNoOld__c, IncludedinForecast__c, IncludeinQuote__c, IsACrossSellingOpptyLine__c, IsALevel2__c, IsAdvancedServices__c, IsaLevel3memberopptyLine__c, LegacyLineId__c,  LineAmount__c, LineClosedate__c, LineStatus__c, LineType__c,  LowestLevelInPyramid__c, Lowestlevelvalue__c, OpportunityLeader__c, OpportunitySEReferenceNumber__c,
                         OverlayRevenueITB__c, ParentLine__c, Probability__c, PurchaseOrderNumber__c, RevenueOverride__c, SalesOrderNumber__c, SmartInfraType__c, TECH_IntouchQuoteLineRowId__c, TECH_IntouchQuoteRowId__c, TECH_IsACrossSellingOpptyLine__c, TECH_LegacyLastUpdateDate__c, TECH_OpportunityClosedDate__c, WeightedAmount__c from OPP_ProductLine__c where Opportunity__c =:oppor.Id ];

                        for(OPP_ProductLine__c prodLine: oldProdLines)
                        {
                            //Inserts all the Parent Lines
                            if(prodLine.ParentLine__c == null)
                            {
                                OPP_ProductLine__c pl = prodLine.clone(false, true);
                                pl.Opportunity__c = clonedOpportunity.Id;
                                insert pl;                           
                                oldNewProdLines.put(prodLine.Id, pl.Id);                                                         
                            }
                            //Collects the Child & Parent Lines
                            if(prodLine.ParentLine__c!=null)
                            {
                                childParentLne.put(prodLine.Id, prodLine.ParentLine__c);
                            }
                                                         
                        }
                        for(OPP_ProductLine__c prodLine: oldProdLines)
                        {
                            //Inserts the Child Line
                            if(prodLine.ParentLine__c != null)
                            {
                                OPP_ProductLine__c pl = prodLine.clone(false, true);
                                pl.ParentLine__c = oldNewProdLines.get(childParentLne.get(prodLine.Id));
                                pl.Opportunity__c = clonedOpportunity.Id;                               
                                insert pl;
                                oldNewProdLines.put(prodLine.Id, pl.Id);                                                         
                            }
                        }*/
                        for(OPP_ProductLine__c prodLine: oldProdLines)
                        {
                            pl = prodLine.clone(false, true);
                            pl.TECT_ChiTuOpportunityLineID__c = NULL; // For BR - 9740
                            pl.Opportunity__c = clonedOpportunity.Id;
                            pl.Designation__c = NULL;
                            pl.LineStatus__c = System.Label.CLOCT13PRM20;//Pending Status
                            pl.SolutionCenter__c = NULL;
                            pl.DeliveryDate__c = NULL;
                            pl.LineClosedate__c = clonedOpportunity.CloseDate;
                            pl.PLContact__c = NULL;
                            pl.IsASynergy__c = FALSE;
                            pl.NumberofPoints__c = NULL;
                            pl.PurchaseOrderNumber__c = NULL;
                            pl.SalesOrderNumber__c = NULL;
                            pl.RevenueOverride__c = NULL;
                            pl.Commission__c = NULL;
                            pl.Probability__c = NULL;
                            //pl.IncludeinQuote__c = FALSE; 
                            pl.IncludedinForecast__c =  System.Label.CL00274; //Value "No" is set for cloned Line item 
                            pl.ITBRevenueLineNoOld__c =  NULL;
                            pl.TECH_IntouchQuoteLineRowId__c = NULL;
                            pl.SmartInfraType__c = NULL;
                            pl.OverlayRevenueITB__c = 0;
                            pl.LegacyLineId__c = NULL;
                            pl.TECH_IntouchQuoteRowId__c = NULL;
                            pl.ITBRevenueLineIdOld__c = NULL;
                            pl.TECH_LegacyLastUpdateDate__c = NULL;
                            pl.TECH_2014ReferentialUpdated__c = FALSE;
                            newParentLines.add(pl);
                        }
                        try{
                            insert newParentLines;
                       }
                       Catch(Exception e){
                           Database.rollback(spoppty);//Rollback to the Savepoint
                           rolledBack= true;
                           pgeMsg=e.getMessage();
                       }
                    }
                            if(!rolledBack && selectedRecords.contains(Label.CL00661))
                            {
                                System.debug('#### Processing opp share' +opportunityTeamMembers);
                                for(OpportunityTeamMember share: oppShare())
                                {
                                    /* Modified for BR-5617 */
                                    if(oppTeamMembers==true)
                                    {
                                       OpportunityTeamMember otm = new OpportunityTeamMember();
                                       otm.UserId = share.UserId;
                                       otm.TeamMemberRole = share.TeamMemberRole;
                                       otm.OpportunityId = clonedOpportunity.id;
                                       otm.TECH_PartnerInvolvedDate__c = share.TECH_PartnerInvolvedDate__c;
                                       clonedOppTeamMembers.add(otm);
                                    }
                                    //Modified the criteria for Opportunity Share
                                    if (share.userId != clonedOpportunity.ownerId && (share.OpportunityAccessLevel ==label.CL00203 || share.UserId == oppor.ownerID)) 
                                    {
                                        OpportunityShare oppShare = new OpportunityShare();                                     
                                        oppShare.opportunityId = clonedOpportunity.id;
                                        oppShare.userOrGroupId = share.userId;
                                        oppShare.OpportunityAccessLevel = label.CL00203;
                                        clonedOpportunityShare.add(oppShare);
                                        
                                    }
                                    else 
                                    {
                                         System.debug('#### Do not add the new opp owner . He has been added previously');
                                    }
                                    
                                }
                                if(clonedOppTeamMembers!=null && !clonedOppTeamMembers.isEmpty())
                                {
                                     Utils_Methods.insertRecords(clonedOppTeamMembers);
                                }
                                /* End of Modifcation for BR-5617 */
                                if(clonedOpportunityShare!=null && !clonedOpportunityShare.isEmpty())
                                {
                                    //inserting records in OpportunityShare
                                    try
                                    {
                                        System.debug('#### Going to insert opp share (clonedOpportunityShare): '+clonedOpportunityShare);
                                        Utils_Methods.insertRecords(clonedOpportunityShare);
                                        System.debug('#### Opp share inserted');
                                    }
                                    catch(Exception e)
                                    {
                                        rolledBack= true;
                                        pgeMsg=e.getMessage();
                                        System.debug('#### Error - Child object insertion failed: '+e.getMessage());
                                        System.debug('#### Error - Child object insertion failed: '+e);
                                    }
                                }
                            }
                    if(!childRecordsToClone.isEmpty() && childRecordsToClone!=null)
                    {
                        //inserting child records
                        Map<Sobject, SObject> childmap = new Map<Sobject, SObject>();
                        list<sObject> childobj = new list<sObject>();
                        childmap =  Utils_Methods.cloneChildRecords(oppor.id,clonedOpportunity.id,childRecordsToClone);
                        System.debug('#### childmap: '+childmap);
                        if(childmap.size()>0)
                        {
                            for(sObject obj : childmap.values())
                            {
                                if(obj!=null)
                                {
                                     childobj.add(obj);
                                }
                            }
                        }
                        System.debug('#### childobj: '+childobj);
                        if(childobj!=null && !childobj.isEmpty())
                        {
                            if(insertRecordId!=null)
                            insertRecordId.clear();
                            try
                            {
                                System.debug('#### Going to insert child objects: '+childobj);
                                insertRecordId = Utils_Methods.insertRecords(childobj);
                                System.debug('#### Child objects inserted: '+childobj);
                            }
                            catch(Exception e)
                            {
                                rolledBack= true;
                                pgeMsg=e.getMessage();
                                System.debug('#### Error - Child object insertion failed: '+e.getMessage());
                                System.debug('#### Error - Child object insertion failed: '+e);
                            }                            
                        }
                    }
                }
            }
        }
        System.debug('continueClone Method Ends');
        if(rolledBack)
        {                         
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,pgeMsg));
            flag = false;
            return null;
        }
        else
        {                
            //clonedOpportunity.TECH_TriggerVR__c = true;
            Opportunity newClonedOpportunity=new Opportunity(Id=clonedOpportunity.Id,TECH_TriggerVR__c=true);                    
            Database.saveResult sv = Database.update(newClonedOpportunity,false); //updating the cloned opportunity
            if(!sv.issuccess())
            {                        
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,sv.getErrors()[0].getMessage()));
                flag= false;
                return null;
            }
        }                
        return new pagereference('/'+clonedOpportunity.id+'/e?'+Label.CL00330+'=%2F'+clonedOpportunity.id);
    }

    public list<ChildRecordSelection> getchildRecords()
    {
        system.debug('getchildRecords Method Begins');
        records =new Map<String,String>();
        records.put(Label.CL00661,'OpportunityTeamMember');
        records.put(sObjectType.OpportunitySplit.label,'OpportunitySplit');
        records.put(sObjectType.OPP_ProductLine__c.label,'OPP_ProductLine__c');
        records.put(sObjectType.OPP_ValueChainPlayers__c.label,'OPP_ValueChainPlayers__c');
        records.put(sObjectType.OPP_OpportunityCompetitor__c.label,'OPP_OpportunityCompetitor__c');
        childRecords = new list<ChildRecordSelection>();
        for(String rec:records.Keyset())
        {
            childRecords.add(new ChildRecordSelection(rec));
        }
        System.debug('getchildRecords Method Ends');
        return childRecords;
    }

    Public class ChildRecordSelection
    {
        public string records{get;set;}
        public Boolean selected{get; set;}
        public ChildRecordSelection(String rec)
        {
            system.debug('ChildRecordSelection Class Begins');
            records = rec;
            if(records.equalsIgnoreCase(Label.CL00661)||records.equalsIgnoreCase(sObjectType.OPP_ValueChainPlayers__c.label)||records.equalsIgnoreCase(sObjectType.OPP_ProductLine__c.label))
                 selected = true;
            else
                 selected = false;
            System.debug('ChildRecordSelection Class Ends');
        }
    }
}