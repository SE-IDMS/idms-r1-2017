public class VFC_RunSpecifiedTests 
{   
public List<String> CompListID{get;set;}
    public List<Component__c> NewCompList = new List<Component__c>();
    public String compXMLResult {get;set;}   
    Map<string,string> CompMap = new Map<String,String>();
    Map<String, List<String>> packageComponentMap = new Map<String, List<String>>();
    Map<Id, APApexTestClass__c> apxTestCls = new Map<Id, APApexTestClass__c>([Select id,Name,TestClass__c,Type__c FROM APApexTestClass__c]);
        
    public VFC_RunSpecifiedTests(ApexPages.StandardSetController controller) 
    {        
        /*------------------MAP OF COMPONENTS----------------------------*/
        CompMap.put('Apex Class','ApexClass');     
        CompMap.put('Apex Trigger','ApexTrigger');   
        /*----------END OF MAP---------------------*/
          
        CompListID = new List<String>(); 
        List<Component__c> components = (List<Component__c>)controller.getSelected();
        
        for(Component__C compItem:components) {  
            CompListID.add(compItem.ID);
        }
        
       If(CompListID != null && CompListID.size()>0){
           NewCompList = [Select FieldName__c,ComponentType__c, IsMigrate__c from Component__c Where Id IN:CompListID AND Ismigrate__c = TRUE];
       }
       
       If(NewCompList != null && NewCompList.size()>0){
       List<String> lstFieldName = new List<String>();
       Set<String> setFieldName = new Set<String>();
                        
       //--Identify test classes in Custom Setting for the classes added in component list
           for(APApexTestClass__c objCS: apxTestCls.values()){    
                //System.debug('##########' + objCS.ClassTrigger_Name__c + '###########');
                if(CompMap.containsKey(objCS.Type__c)){ 
                    for(Component__c objCompList: NewCompList ){
                        if(objCompList.IsMigrate__c == true){
                            if(objCS.Name == objCompList.FieldName__c){
                                if(!setFieldName.contains(objCS.TestClass__c)){
                                        lstFieldName.add(objCS.TestClass__c);
                                        setFieldName.add(objCS.TestClass__c); 
                                    }
                               }
                           }
                       }
                   }
                   packageComponentMap.put(CompMap.get('Apex Class'),lstFieldName);
            }                    
           if(packageComponentMap.size()>0){            
              compXMLResult = RunSpecifiedTestXML(packageComponentMap);   
              //System.debug(compXMLResult); 
           }            
       }
       else
       {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No Records found!, Check Migrate checkbox for the components..');
        ApexPages.addmessage(myMsg);
       }
    }
    
    public string RunSpecifiedTestXML(Map<String, List<String>> pckComponentMap)
    {
        XmlStreamWriter w = new XmlStreamWriter();  
           for(String objComponent: pckComponentMap.Keyset())
           {   
               for(String valuefromlst :pckComponentMap.get(objComponent))
               {
                  if(valuefromlst!='' && valuefromlst!=null){
                      w.writeStartElement(null, 'runTest', null);
                      w.writeCharacters(valuefromlst);
                      w.writeEndElement(); // End Members
                  }            
               }  
          } 
            w.writeCharacters('\r\n');
            w.writeCharacters('*****************************************************************************************************************');
            w.writeCharacters('\r\n');
          for(String objComponent: pckComponentMap.Keyset())
           {   
               for(String valuefromlst :pckComponentMap.get(objComponent))
               {
                  if(valuefromlst!='' && valuefromlst!=null){
                      w.writeCharacters(valuefromlst);
                      w.writeCharacters(',\r\n');
                  }            
               }  
          }                    
          String xmlOutput = w.getXmlString();         
          w.close();        
          return xmlOutput;
          } 
}