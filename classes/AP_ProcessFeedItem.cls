//*********************************************************************************
//Class Name :  AP_ProcessFeedItem
// Purpose : triggers FeedItemAfterInsert and FeedItemAfterUpdate use this class 
// Created by : Shivdeep Gupta - Global Delivery Team
// Date created :  March 02, 2015
// Modified by :
// Date Modified :
// Remarks : For April 15 Release  
// ********************************************************************************

public class AP_ProcessFeedItem{

    FeedItem oFeedItem = new FeedItem();
    List<FeedItem> listFeedItem = new List<FeedItem>();
    /*BusinessRiskEscalations__c oBRE;
    List<BusinessRiskEscalations__c> listBRE = new List<BusinessRiskEscalations__c>();
    List<ID> bREIDs = new List<ID>();
    

    
    public void updateBREActualUserUdate(List<FeedItem> listFeedItem){
        Map<String,Schema.SobjectType> globalDescribe = Schema.getGlobalDescribe();
        // Label  CLAPR15I2P11 = BusinessRiskEscalations__c
        string bREObjectPrefixChar = globalDescribe.get(System.Label.CLAPR15I2P11).getDescribe().getkeyprefix();
        system.debug( 'Test 1'+ bREObjectPrefixChar  );
        
        for(FeedItem oFI : listFeedItem){
            if(oFI.ParentID != null){
                if(string.Valueof(oFI.ParentID).startsWith(bREObjectPrefixChar)){
                    bREIDs.add(oFI.ParentID);
                }   
            }
        }
        
        if(bREIDs.size()>0){
            for(ID oID : bREIDs){
                oBRE = new BusinessRiskEscalations__c(ID = oID);
                oBRE.Actual_User_Lastmodifiedby__c = UserInfo.getUserId();
                oBRE.Actual_User_LastmodifiedDate__c = System.now();
                listBRE.add(oBRE);
            }
            
        }
        
        if(listBRE.size() >0){
            update listBRE;
        }
    }
    
    // OCT 2015 Release Added by Uttara - Last Updated and Last Updated by fields population on Containment Action
    
    FeedItem oFeedItem1 = new FeedItem();
    List<FeedItem> listFeedItem1 = new List<FeedItem>();
    ContainmentAction__c oXA;
    List<ContainmentAction__c> listXA = new List<ContainmentAction__c>();
    List<ID> XAIDs = new List<ID>();
    
    public void updateXA_LastUpdatedActualUserAndDate(List<FeedItem> listFeedItem1){
         updateLastUpdatedby(listFeedItem1);
        Map<String,Schema.SobjectType> globalDescribe = Schema.getGlobalDescribe();
        // Label  CLOCT15I2P15 = ContainmentAction__c
        string XAObjectPrefixChar = globalDescribe.get(System.Label.CLOCT15I2P15).getDescribe().getkeyprefix();
        system.debug( 'Test 1'+ XAObjectPrefixChar  );
        
        for(FeedItem oFI : listFeedItem1){
            if(oFI.ParentID != null){
                if(string.Valueof(oFI.ParentID).startsWith(XAObjectPrefixChar))
                    XAIDs.add(oFI.ParentID); 
            }
        }
        
        if(XAIDs.size()>0){
            for(ID oID : XAIDs){
                oXA = new ContainmentAction__c(ID = oID);
                oXA.ActualUserLastmodifiedby__c= UserInfo.getUserId();
                oXA.ActualUserLastmodifiedDate__c= System.now();
                listXA.add(oXA);
            }
        }
        
        if(listXA.size() >0)
            update listXA;
    }*/
    public void updateLastUpdatedby(List<FeedItem> listFeedItem){
        String str = System.Label.CLJUN16I2P01;
        for(string key : str.split(','))
        {     system.debug('****key**'+key);  
            //sObject=key;
            sObject oXA;
            List<sObject> listXA = new List<sObject>();
            List<ID> XAIDs = new List<ID>();
            Map<String,Schema.SobjectType> globalDescribe = Schema.getGlobalDescribe();
            
            string XAObjectPrefixChar = globalDescribe.get(key).getDescribe().getkeyprefix();
            system.debug( 'Test 23'+ XAObjectPrefixChar  );
        
            for(FeedItem oFI : listFeedItem){
                if(oFI.ParentID != null){
                    if(string.Valueof(oFI.ParentID).startsWith(XAObjectPrefixChar))
                        XAIDs.add(oFI.ParentID); 
                }
            }
        
            if(XAIDs.size()>0){
                for(ID oID : XAIDs){
                    oXA = (sObject )createNewObject(key);//(ID = oID);
                    oXA.put('ID',oID);
                    //put('UserOrGroupId',usrId);
                    if(key=='Problem__c'|| key=='ContainmentAction__c'){
                        oXA.put('ActualUserLastmodifiedby__c', UserInfo.getUserId());
                        oXA.put('ActualUserLastmodifiedDate__c', System.now());
                    }else if(key=='BusinessRiskEscalations__c'){
                        oXA.put('Actual_User_Lastmodifiedby__c', UserInfo.getUserId());
                        oXA.put('Actual_User_LastmodifiedDate__c', System.now());
                    }else{
                        oXA.put('LastUpdatedby__c', UserInfo.getUserId());
                        oXA.put('LastUpdated__c', System.now());
                    }
                    listXA.add(oXA);
                }
            }
        
            if(listXA.size() >0)
                update listXA;
            }
    }
    public static sObject createNewObject(String strObjtName) {
       
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(strObjtName);        
        
        return targetType.newSObject(); 
    }
}