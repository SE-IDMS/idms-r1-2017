/*
    Author              : Gayathri Shivakumar (Schneider Electric)
    Date Created        :11-Oct-2012
    Modification Log    : 
    Description         : Test Method for Initiative Health by Entity Report
    Edited              : 12th Jun 2013 
 */

@isTest
private class ConnectInitiativeHealthbyEntity_Test{
    static testMethod void EntityInititativeHealthTest() {
    
       User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connectE');
        System.runAs(runAsUser){
            
       
        initiatives__c inti1=Utils_TestMethods_Connect.createInitiative();
        inti1.Name = Label.ConnectInitEL;
        inti1.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti1.Initiative_Health_Status_by_Sponsors__c =Label.ConnectStatusOnTimeDelivered;
        inti1.Year__c = Label.ConnectYear;
        inti1.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti1.Q4_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        insert inti1;
        
        Project_NCP__c cp1=new Project_NCP__c(Program__c=inti1.id,Program_Name__c='cp1',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test', year__c = Label.ConnectYear, Visible__c = 'True', Program_Health_Status_by_Sponsor__c = Label.ConnectStatusOnTimeDelivered,Program_Health_Status_by_Sponsor_Q2__c = Label.ConnectStatusOnTimeDelivered);
        insert cp1;
        
        Connect_Entity_Progress__c CEp1=new Connect_Entity_Progress__c(Global_Program__c=cp1.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q1_Entity_Health_Status__c = Label.ConnectStatusOnTimeDelivered,Q2_Entity_Health_Status__c = Label.ConnectStatusOnTimeDelivered,Q3_Entity_Health_Status__c = Label.ConnectStatusOnTimeDelivered,Q4_Entity_Health_Status__c = Label.ConnectStatusOnTimeDelivered);            
        insert CEp1;
    
        initiatives__c inti2=Utils_TestMethods_Connect.createInitiative();
        inti2.Name = Label.ConnectInitEI;
        inti2.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti2.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti2.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti2.Q4_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti2.Year__c = Label.ConnectYear;
        insert inti2;
        Project_NCP__c cp2=new Project_NCP__c(Program__c=inti2.id,Program_Name__c='cp2',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True', Program_Health_Status_by_Sponsor__c = Label.ConnectStatusSlightDelay,Program_Health_Status_by_Sponsor_Q2__c = Label.ConnectStatusSlightDelay);
        insert cp2;
        Connect_Entity_Progress__c CEp2=new Connect_Entity_Progress__c(Global_Program__c=cp2.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q1_Entity_Health_Status__c = Label.ConnectStatusSlightDelay,Q2_Entity_Health_Status__c = Label.ConnectStatusSlightDelay,Q3_Entity_Health_Status__c = Label.ConnectStatusSlightDelay,Q4_Entity_Health_Status__c = Label.ConnectStatusSlightDelay);            
        insert CEp2;

      
        initiatives__c inti3=Utils_TestMethods_Connect.createInitiative();
        inti3.Name = Label.ConnectInitEW;
        inti3.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti3.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti3.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti3.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti3.Year__c = Label.ConnectYear;
        insert inti3;
        Project_NCP__c cp3=new Project_NCP__c(Program__c=inti3.id,Program_Name__c='cp2',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True', Program_Health_Status_by_Sponsor__c = Label.ConnectStatusNoProgressorDelayedover1Month,Program_Health_Status_by_Sponsor_Q2__c = Label.ConnectStatusNoProgressorDelayedover1Month);
        insert cp3;
        Connect_Entity_Progress__c CEp3=new Connect_Entity_Progress__c(Global_Program__c=cp3.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q1_Entity_Health_Status__c = Label.ConnectStatusNoData,Q2_Entity_Health_Status__c = Label.ConnectStatusNoData,Q3_Entity_Health_Status__c = Label.ConnectStatusNoData);            
        insert CEp3;

        
        initiatives__c inti4=Utils_TestMethods_Connect.createInitiative();
        inti4.Name = Label.ConnectInitGCI;
        inti4.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti4.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti4.Q3_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti4.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti4.Year__c = Label.ConnectYear;
        insert inti4;
        Project_NCP__c cp4=new Project_NCP__c(Program__c=inti4.id,Program_Name__c='cp2',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True', Program_Health_Status_by_Sponsor_Q3__c = Label.ConnectStatusOnTimeDelivered);
        insert cp4;
        Connect_Entity_Progress__c CEp4=new Connect_Entity_Progress__c(Global_Program__c=cp4.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q1_Entity_Health_Status__c = Label.ConnectStatusNoProgressorDelayedover1Month,Q2_Entity_Health_Status__c = Label.ConnectStatusNoProgressorDelayedover1Month,Q3_Entity_Health_Status__c = Label.ConnectStatusNoProgressorDelayedover1Month,Q4_Entity_Health_Status__c = Label.ConnectStatusNoProgressorDelayedover1Month);            
        insert CEp4;

        
        initiatives__c inti5=Utils_TestMethods_Connect.createInitiative();
        inti5.Name = Label.ConnectInitGC;
        inti5.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti5.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti5.Q3_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti5.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti5.Year__c = Label.ConnectYear;
        insert inti5;
        Project_NCP__c cp5=new Project_NCP__c(Program__c=inti5.id,Program_Name__c='cp5',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True', Program_Health_Status_by_Sponsor_Q4__c = Label.ConnectStatusNoProgressorDelayedover1Month,Program_Health_Status_by_Sponsor_Q3__c = Label.ConnectStatusNoProgressorDelayedover1Month);
        insert cp5;
        Connect_Entity_Progress__c CEp5=new Connect_Entity_Progress__c(Global_Program__c=cp5.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM');            
        insert CEp5;

        
        initiatives__c inti6=Utils_TestMethods_Connect.createInitiative();
        inti6.Name = Label.ConnectInitGE;
        inti6.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti6.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti6.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti6.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti6.Year__c = Label.ConnectYear;
        insert inti6;
        Project_NCP__c cp6=new Project_NCP__c(Program__c=inti6.id,Program_Name__c='cp2',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True');
        insert cp6;
        Connect_Entity_Progress__c CEp6=new Connect_Entity_Progress__c(Global_Program__c=cp6.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q2_Entity_Health_Status__c= Label.ConnectStatusSlightDelay );            
        insert CEp6;


        
        initiatives__c inti7=Utils_TestMethods_Connect.createInitiative();
        inti7.Name = Label.ConnectInitOE;
        inti7.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti7.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti7.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti7.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti7.Year__c = Label.ConnectYear;
        insert inti7;
        Project_NCP__c cp7=new Project_NCP__c(Program__c=inti7.id,Program_Name__c='cp2',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True');
        insert cp7;
        Connect_Entity_Progress__c CEp7=new Connect_Entity_Progress__c(Global_Program__c=cp7.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q2_Entity_Health_Status__c= Label.ConnectStatusSlightDelay);            
        insert CEp7;


    
        initiatives__c inti8=Utils_TestMethods_Connect.createInitiative();
        inti8.Name = Label.ConnectInitOM;
        inti8.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti8.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti8.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti8.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti8.Year__c = Label.ConnectYear;
        insert inti8;
        Project_NCP__c cp8=new Project_NCP__c(Program__c=inti8.id,Program_Name__c='cp2',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True');
        insert cp8;
       Connect_Entity_Progress__c CEp8=new Connect_Entity_Progress__c(Global_Program__c=cp8.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q2_Entity_Health_Status__c= Label.ConnectStatusSlightDelay);            
        insert CEp8;



      
        initiatives__c inti9=Utils_TestMethods_Connect.createInitiative();
        inti9.Name =Label.ConnectInitPE;
        inti9.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti9.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti9.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti9.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti9.Year__c = Label.ConnectYear;
        insert inti9;
        Project_NCP__c cp9=new Project_NCP__c(Program__c=inti9.id,Program_Name__c='cp2',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True', Program_Health_Status_by_Sponsor_Q4__c = Label.ConnectStatusSlightDelay);
        insert cp9;
        Connect_Entity_Progress__c CEp9=new Connect_Entity_Progress__c(Global_Program__c=cp9.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q2_Entity_Health_Status__c= Label.ConnectStatusSlightDelay);            
        insert CEp9;


        
        initiatives__c inti10=Utils_TestMethods_Connect.createInitiative();
        inti10.Name = Label.ConnectInitRE;
        inti10.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti10.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti10.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti10.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti10.Year__c = Label.ConnectYear;
        insert inti10;
        Project_NCP__c cp10=new Project_NCP__c(Program__c=inti10.id,Program_Name__c='cp2',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True', Program_Health_Status_by_Sponsor_Q3__c = Label.ConnectStatusSlightDelay,Program_Health_Status_by_Sponsor_Q4__c = Label.ConnectStatusOnTimeDelivered);
        insert cp10;
        Connect_Entity_Progress__c CEp10=new Connect_Entity_Progress__c(Global_Program__c=cp10.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q2_Entity_Health_Status__c= Label.ConnectStatusSlightDelay);            
        insert CEp10;


        
        initiatives__c inti11=Utils_TestMethods_Connect.createInitiative();
        inti11.Name = Label.ConnectInitSE;
        inti11.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti11.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti11.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti11.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti11.Year__c = Label.ConnectYear;
        insert inti11;
        Project_NCP__c cp11=new Project_NCP__c(Program__c=inti11.id,Program_Name__c='cp2',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True');
        insert cp11;
        Connect_Entity_Progress__c CEp11=new Connect_Entity_Progress__c(Global_Program__c=cp11.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q2_Entity_Health_Status__c= Label.ConnectStatusSlightDelay);            
        insert CEp11;


        
        initiatives__c inti12=Utils_TestMethods_Connect.createInitiative();
        inti12.Name = Label.ConnectInitTSC;
        inti12.Q2_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti12.Initiative_Health_Status_by_Sponsors__c = Label.ConnectStatusOnTimeDelivered;
        inti12.Q3_Initiative_Health_Status__c = Label.ConnectStatusOnTimeDelivered;
        inti12.Q4_Initiative_Health_Status__c =  Label.ConnectStatusOnTimeDelivered;
        inti12.Year__c = Label.ConnectYear;
        insert inti12;
        Project_NCP__c cp12=new Project_NCP__c(Program__c=inti12.id,Program_Name__c='cp12',Global_Functions__c='GM;GSC-Regional',Global_Business__c='ITB;Partner-Division',GSC_Regions__c='test1',Power_Region__c='NA',Partner_Region__c='test',year__c = Label.ConnectYear, Visible__c = 'True');
        insert cp12;
        Connect_Entity_Progress__c CEp12=new Connect_Entity_Progress__c(Global_Program__c=cp12.id,Scope__c=Label.Connect_Global_Functions,Entity__c='GM',Q2_Entity_Health_Status__c= Label.ConnectStatusSlightDelay);            
        insert CEp12;


        
      
       //Use the PageReference Apex class to instantiate 
        PageReference pageRef = Page.Connect_Initiative_Health_By_Entity;
       
       //In this case, the Visualforce page named 'Connect_Initiative_Health_Status_Report' is the starting point of this Test method. 
        Test.setCurrentPage(pageRef);

 //Instantiate and construct the controller class.   
    ApexPages.StandardController stanPage= new ApexPages.StandardController(inti1);
       ConnectInitiativeHealthByEntity_Cont controller = new ConnectInitiativeHealthByEntity_Cont (stanPage);

 
    controller.Quarter = 'Q2';
    controller.EntityList = 'Global';
    
     //Call the controller method
    controller.getEntityFilter();
    controller.getFilterQuarter();
    controller.getYear();
    controller.YearChange();   
    controller.getQuarter();
    controller.RenderPDF();
    controller.QuarterChange();
    controller.getEL();   
    controller.getEI();
    controller.getEW();
    controller.getGCI();
    controller.getGC();
    controller.getGE();
    controller.getOE();
    controller.getOM();
    controller.getPE();
    controller.getRE();
    controller.getSE();
    controller.getTSC();
    controller.getEntityList();
    
      
    controller.Quarter = 'Q1';
    
    //Call the controller method
    controller.getEntityFilter();
    controller.getFilterQuarter();
    controller.getYear();
    controller.YearChange();       
    controller.getQuarter();
    controller.RenderPDF();
    controller.QuarterChange();
    controller.getEL();   
    controller.getEI();
    controller.getEW();
    controller.getGCI();
    controller.getGC();
    controller.getGE();
    controller.getOE();
    controller.getOM();
    controller.getPE();
    controller.getRE();
    controller.getSE();
    controller.getTSC();
    controller.getEntityList();
    controller.EntityChange();


    controller.Quarter = 'Q3';
    
    //Call the controller method
    controller.getEntityFilter();
    controller.getFilterQuarter();
    controller.getYear();
    controller.YearChange();       
    controller.getQuarter();
    controller.RenderPDF();
    controller.QuarterChange();
    controller.getEL();   
    controller.getEI();
    controller.getEW();
    controller.getGCI();
    controller.getGC();
    controller.getGE();
    controller.getOE();
    controller.getOM();
    controller.getPE();
    controller.getRE();
    controller.getSE();
    controller.getTSC();
    controller.EntityChange();



    controller.Quarter = 'Q4';
    
    //Call the controller method
    controller.getEntityFilter();
    controller.getFilterQuarter();
    controller.getYear();
    controller.YearChange();       
    controller.getQuarter();
    controller.RenderPDF();
    controller.QuarterChange();
    controller.getEL();   
    controller.getEI();
    controller.getEW();
    controller.getGCI();
    controller.getGC();
    controller.getGE();
    controller.getOE();
    controller.getOM();
    controller.getPE();
    controller.getRE();
    controller.getSE();
    controller.getTSC();
    controller.EntityList = 'GM';
    controller.getEntityList();
    controller.EntityChange();

  controller.getFilterList();
    
    
    }
    
   }
}