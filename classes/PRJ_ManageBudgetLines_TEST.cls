/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PRJ_ManageBudgetLines_TEST {

    static testMethod void myUnitTest() {
        Account acc = Utils_TestMethods.createAccount();
        insert acc;
        //Contact con = Utils_TestMethods.createContact('alias', 'alias',acc.Id);
        DMTStakeholder__c con = Utils_TestMethods.createDMTStakeholder('alias', 'alias');
        insert con;
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
        insert testDMTA1; 
        PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
        testProj.ParentFamily__c = 'Business';
        testProj.Parent__c = 'Buildings';
        testProj.BusGeographicZoneIPO__c = 'Global';
        testProj.BusGeographicalIPOOrganization__c = 'CIS';
        testProj.GeographicZoneSE__c = 'Global';
        testProj.Tiering__c = 'Tier 1';
        testProj.ProjectRequestDate__c = system.today();
        testProj.Description__c = 'Test';
        testProj.GlobalProcess__c = 'Customer Care';
        testProj.ProjectClassification__c = 'Infrastructure';
        insert testProj;
        Budget__c budget = Utils_TestMethods.createBudget(testProj);
        insert budget;
        

        PRJ_BudgetLine__c bl = new PRJ_BudgetLine__c(Budget__c=budget.Id,Amount__c=12,Year__c='2011',Type__c='Cashout CAPEX');
        insert bl;
        PRJ_BudgetLine__c b2 = new PRJ_BudgetLine__c(Budget__c=budget.Id,Amount__c=12,Year__c='2010',Type__c='Cashout CAPEX');
        insert b2;
        PRJ_BudgetLine__c b3 = new PRJ_BudgetLine__c(Budget__c=budget.Id,Year__c='2013',Type__c='IT P & L Cost Impact2013');
        insert b3;
        PRJ_BudgetLine__c b4 = new PRJ_BudgetLine__c(Budget__c=budget.Id,Year__c='2013',Type__c='Cashout CAPEX');
        insert b4;
        PRJ_BudgetLine__c b5 = new PRJ_BudgetLine__c(Budget__c=budget.Id,Year__c='2013',Type__c='IPO Internal Regions APAC2013');
        insert b5;
        PRJ_BudgetLine__c b6 = new PRJ_BudgetLine__c(Budget__c=budget.Id,Year__c='2013',Type__c='IPO Internal Regions NAM2013');
        insert b6;
        PRJ_BudgetLine__c b7 = new PRJ_BudgetLine__c(Budget__c=budget.Id,Year__c='2014',Type__c='IPO Internal Regions NAM2014');
        insert b7;
        PRJ_BudgetLine__c b8 = new PRJ_BudgetLine__c(Budget__c=budget.Id,Year__c='2013',Type__c='IPO Internal Regions EUROPE2014');
        insert b8;
        PRJ_BudgetLine__c b9 = new PRJ_BudgetLine__c(Budget__c=budget.Id,Year__c='2013',Type__c='IPO Internal Regions NAM2015');
        insert b9;
        PRJ_BudgetLine__c b10 = new PRJ_BudgetLine__c(Budget__c=budget.Id,Year__c='2013',Type__c='IPO Internal Regions2015');
        insert b10;
        
        Map<integer,List<PRJ_BudgetLine__c>> years = new Map<integer,List<PRJ_BudgetLine__c>>{};

        
        PRJ_ManageBudgetLines c = new PRJ_ManageBudgetLines(new ApexPages.StandardController(budget));
        Test.startTest(); 
        c.updateBudgetLines();
        c.back();
        c.updatePLfields();
        c.updateRegionFields();
        Test.stopTest();
        
        
    }
}