/*
    Author          : Sreedevi Surendran (Schneider Electric)
    Date Created    : 03/15/2012
    Description     : Utility class for the test methods of DMT
*/
@isTest // Added by Srikant Feb 2013 Release
public with sharing class Utils_TestMethods_DMT
{
    public static DMTFundingEntity__c createDMTFundingEntity(PRJ_ProjectReq__c pr,string budgetYear)
    {
        DMTFundingEntity__c oFE = new DMTFundingEntity__c(ProjectReq__c = pr.Id, BudgetYear__c = budgetYear);
        return oFE;
    }
    
    public static DMTFundingEntity__c createDMTFundingEntity(PRJ_ProjectReq__c pr,string budgetYear,boolean isActive,string pfamily, string borg, string sel1, string sel2, string fquarter, string fyear)
    {
        DMTFundingEntity__c oFE = new DMTFundingEntity__c(ProjectReq__c = pr.Id, BudgetYear__c = budgetYear, ActiveForecast__c = isActive, ParentFamily__c = pfamily, Parent__c = borg, GeographicZoneSE__c = sel1, GeographicalSEOrganization__c = sel2, ForecastQuarter__c = fquarter, ForecastYear__c = fyear);
        return oFE;        
    }

    public static DMTFundingEntity__c createDMTFundingEntity(PRJ_ProjectReq__c pr,string budgetYear,boolean isActive,string pfamily, string borg, string sel1, string sel2)
    {
        DMTFundingEntity__c oFE = new DMTFundingEntity__c(ProjectReq__c = pr.Id, BudgetYear__c = budgetYear, ActiveForecast__c = isActive, ParentFamily__c = pfamily, Parent__c = borg, GeographicZoneSE__c = sel1, GeographicalSEOrganization__c = sel2);
        return oFE;        
    }
    
    public static DMTFundingEntity__c createDMTFundingEntity(PRJ_ProjectReq__c pr,string budgetYear,boolean isActive, boolean isSelected, string strHFM)
    {
        DMTFundingEntity__c oFE = new DMTFundingEntity__c(ProjectReq__c = pr.Id, BudgetYear__c = budgetYear, ActiveForecast__c = isActive, SelectedinEnvelop__c = isSelected, HFMCode__c = strHFM);
        return oFE;
    }
    
    public static DMTAuthorizationMasterData__c createDMTAuthorizationMasterData(Id authUser, string strStatus, string strParentFamily, string strBusinessOrganization, string strIPORegionalOrganization)
    {
        DMTAuthorizationMasterData__c oDMTA = new DMTAuthorizationMasterData__c(AuthorizedUser1__c = authUser, NextStep__c = strStatus, ParentFamily__c = strParentFamily, Parent__c = strBusinessOrganization, BusGeographicZoneIPO__c = strIPORegionalOrganization);
        return oDMTA;
    }
    
    public static DMTAuthorizationMasterData__c createDMTAuthorizationMasterData(Id authUser, string strStatus, string strBusinessOrganization, string strIPORegionalOrganization)
    {
        DMTAuthorizationMasterData__c oDMTA = new DMTAuthorizationMasterData__c(AuthorizedUser1__c = authUser, NextStep__c = strStatus, Parent__c = strBusinessOrganization, BusGeographicZoneIPO__c = strIPORegionalOrganization);
        return oDMTA;
    }
    
    public static DMTAuthorizationMasterData__c createDMTAuthorizationMasterData(Id authUser, string strStatus, string strBusinesstechnicalDomain)
    {
        DMTAuthorizationMasterData__c oDMTA = new DMTAuthorizationMasterData__c(AuthorizedUser1__c = authUser, NextStep__c = strStatus, BusinessTechnicalDomain__c = strBusinesstechnicalDomain);
        return oDMTA;
    }
    
    public static PRJ_Stakeholder__c createDMTPRJStakeholder(PRJ_ProjectReq__c pr)
    {
        PRJ_Stakeholder__c oDMTA = new PRJ_Stakeholder__c(ActorFirstName__c='Test',ActorLastName__c='Test',Exclude__c=true,ProjectReq__c=pr.id,Role__c='Architecture Responsible');
        return oDMTA;
    }
    
    
    
}