@isTest
private class VCC05_DisplaySearchFields_TEST 
{
    static testMethod void testSearchFieldComponent() 
    {
        VCC05_DisplaySearchFields controller = new VCC05_DisplaySearchFields();
        controller.pickListType = 'PM0_GMR';
        controller.SearchFilter = new String[4];
        controller.SearchFilter[0] = '00';
        controller.SearchFilter[1] = Label.CL00355;      
        PageReference pageRef= Page.VFP25_AddUpdateProductforCase;  
        test.startTest();     
        controller.init();
        For(integer index=1; index<VCC05_DisplaySearchFields.MAX_NUMBER_OF_PL; index++)
        {
            pageRef.getParameters().put('level',String.valueof(index));
            if(index==6)
                controller.levels[5] = Label.CL00355;
            Test.setCurrentPageReference(pageRef);
            controller.picklistrefresher(); 
        }
        test.stopTest();     
    }
}