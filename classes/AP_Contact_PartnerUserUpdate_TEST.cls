/*
    Srinivas Nallapati    18-Mar-2013    PRM Mar13  Test class for 'AP_Contact_PartnerUserUpdate'  
*/
@isTest
private class AP_Contact_PartnerUserUpdate_TEST
{
    static testMethod void testPartnerConCreation() 
    {
            List<Profile> profiles = [select id from profile where name='System Administrator'];
            if(profiles.size()>0)
            {
                User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
                usr.BypassWF__c = false;
                usr.ByPassVR__c = true;
                usr.userRoleId = '00EA0000000MiDK';
                Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
                Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName();        
        
                system.runas(usr) 
                { 
                                 
                    Country__c country= Utils_TestMethods.createCountry();
                    insert country; 
                    
                    Account ac= Utils_TestMethods.createAccount();
                    //ac.Country__c=country.id;
                    insert ac;
                    
                    ac.isPartner = true;
                    update ac;
                            
                    List<COntact> lsCOn = new List<Contact>();
                    for(Integer i=0;i<15;i++)
                    {
                        Contact con1 = new Contact(LastName ='TestCOnt'+i,firstName = 'TestFirst'+i, AccountId=ac.id, email = 'TestPartCrebfo'+i+'@test.com');
                        if(Math.mod(i,2) ==0)
                            con1.PRMUser__c = true;
                        lsCOn.add(con1); 
                    }
                    
                   
                    insert lsCOn;
                    set<Id> setContactID = new set<Id>();
                    for(COntact c : lsCOn)
                    {
                      c.PRMUser__c = true;
                      setContactID.add(c.ID);
                    }
                    lsCOn[0].email = '';
                    lsCOn[0].MobilePhone ='1234567890';
                    lsCOn[0].correspLang__c ='';
                    
                    update  lsCOn; 
                    
                   // AP_Contact_PartnerUserUpdate.disablePartnerContact(setContactID);
                     for(COntact c : lsCOn)
                     {
                       
                       c.PRMUser__c = false;
                     }
                     update  lsCOn;
                     for(COntact c : lsCOn)
                     
                       c.PRMUser__c = true;
                    
                    update  lsCOn; 
                    
                }
            //toggle user Test.
            Id p = [select id from profile where name='Partner Community User'].id;
            system.runAs(usr){
            Country__c country1= new Country__c(Name='TestCountry1', CountryCode__c='US');
            insert country1;
            Account ac = Utils_TestMethods.createAccount();
            //ac.Country__c = country1.Id;
            insert ac;
            Contact con = new Contact(LastName ='TestCOnt',firstName = 'TestFirst', AccountId=ac.id, email = '1TestPartCrebfo@test.com');
            insert con;
            User user = new User(alias = 'test123', email='test123@noemail.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p,IsActive =true,
            ContactId = con.Id,
            timezonesidkey='America/Los_Angeles', username='tester.partcreate@noemail.com');
            Test.startTest();
            insert user;
            
            con.PRMContactRegistrationStatus__c = System.Label.CLAPR15PRM066;
            update con;
            //AP_Contact_PartnerUserUpdate.disablePartnerContact(setContactID);
            Test.stopTest();
            Boolean userActive = [Select Id,isActive From User where id = :user.id].isActive; 
            
            
            }
    }//End of if  

            
    }//End of testMethod
    static testMethod void testPartnerConCreation1() 
    {
            List<Profile> profiles = [select id from profile where name='System Administrator'];
            if(profiles.size()>0)
            {
                User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
                usr.BypassWF__c = false;
                usr.ByPassVR__c = true;
                usr.userRoleId = '00EA0000000MiDK';
                usr.IsActive = true;
                Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
                Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName();        
        
                system.runas(usr) 
                { 
                                 
                    Country__c country= Utils_TestMethods.createCountry();
                    insert country; 
                    
                    Account ac= Utils_TestMethods.createAccount();
                    //ac.Country__c=country.id;
                    insert ac;
                    
                    ac.isPartner = true;
                    update ac;
                    
                    List<COntact> lsCOn = new List<Contact>();
                    for(Integer i=0;i<15;i++)
                    {
                        Contact con1 = new Contact(LastName ='TestCOnt'+i,firstName = 'TestFirst'+i, AccountId=ac.id, email = 'TestPartCrebfo'+i+'@test.com');
                        if(Math.mod(i,2) ==0)
                            con1.PRMUser__c = true;
                        lsCOn.add(con1); 
                    }
                    insert lsCOn;
                    
                    
                    
                    set<Id> setContactID = new set<Id>();
                    for(COntact c : lsCOn)
                    {
                      c.PRMUser__c = true;
                      setContactID.add(c.ID);
                    }
                    lsCOn[0].email = '';
                    lsCOn[0].MobilePhone ='1234567890';
                    lsCOn[0].correspLang__c ='';
                    
                    update  lsCOn; 
                    
                     for(COntact c : lsCOn)
                     {
                       
                       c.PRMUser__c = false;
                     }
                     update  lsCOn;
                     for(COntact c : lsCOn)
                     
                       c.PRMUser__c = true;
                    
                    update  lsCOn; 
                    test.startTest();
                    AP_Contact_PartnerUserUpdate.toggleUser(setContactID);
                    test.stopTest();
                }
            //toggle user Test.
                       
            }
    }//End of if  
    static testMethod void testPartnerConCreation2() 
    {
            List<Profile> profiles = [select id from profile where name='System Administrator'];
            if(profiles.size()>0)
            {
                User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
                usr.BypassWF__c = false;
                usr.ByPassVR__c = true;
                usr.userRoleId = '00EA0000000MiDK';
                usr.IsActive = true;
                Schema.DescribeSObjectResult LEDDescribe= Lead.sObjectType.getDescribe();
                Map<string, Schema.RecordTypeInfo> mapRecordType = LEDDescribe.getRecordTypeInfosByName();        
        
                system.runas(usr) 
                { 
                                 
                    Country__c country= Utils_TestMethods.createCountry();
                    insert country; 
                    
                    Account ac= Utils_TestMethods.createAccount();
                    //ac.Country__c=country.id;
                    insert ac;
                    
                    ac.isPartner = true;
                    update ac;
                    
                    List<COntact> lsCOn = new List<Contact>();
                    for(Integer i=0;i<15;i++)
                    {
                        Contact con1 = new Contact(LastName ='TestCOnt'+i,firstName = 'TestFirst'+i, AccountId=ac.id, email = 'TestPartCrebfo'+i+'@test.com');
                        if(Math.mod(i,2) ==0)
                            con1.PRMUser__c = true;
                        lsCOn.add(con1); 
                    }
                    insert lsCOn;
                    
                    
                    
                    set<Id> setContactID = new set<Id>();
                    for(COntact c : lsCOn)
                    {
                      c.PRMUser__c = true;
                      
                    }
                    
                    update  lsCOn;
                    
                    for(Contact c : lsCOn) 
                        setContactID.add(c.ID);
                        
                                        
                    test.startTest();
                    AP_Contact_PartnerUserUpdate.disablePartnerContact(setContactID);
                    test.stopTest();
                }
            //toggle user Test.
                       
            }
    }  
            
    //End of testMethod
}