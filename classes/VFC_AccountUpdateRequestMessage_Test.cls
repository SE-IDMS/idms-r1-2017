@isTest
private class VFC_AccountUpdateRequestMessage_Test {
	
		@isTest static void testAccountUpdateRequestPositive() {
		// Implement test code
			Country__c ct = new Country__c();
            ct.Name='TestCountry1';
            ct.CountryCode__c='IT';
            insert ct;
            
            Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
            insert acc1;
            
            AccountUpdateRequest__c aur = new AccountUpdateRequest__c(Account__c = acc1.id,
                                                             AccountName__c = acc1.name,
                                                             City__c='Test City',
                                                             Street__c='Test Street',
                                                             Country__c=ct.id,
                                                             ZipCode__c='012345',
                                                             ToBeDeleted__c=true);
                                                             //Approver__c=UserInfo.getUserId());
            insert aur;
            
            ApexPages.StandardController controller=new ApexPages.StandardController(aur);
            new VFC_AccountUpdateRequestMessage(controller);
	}
	@isTest static void testAccountUpdateRequestNegative() {
		// Implement test code
        Country__c ct = new Country__c();
        ct.Name='TestCountry1';
        ct.CountryCode__c='IT';
        insert ct;
        
        Account acc1 = new Account(Name='Acc1', ClassLevel1__c='FI', Street__c='New Street', City__c='city', POBox__c='XXX', ZipCode__c='12345', Country__c=ct.id,Type='GMO',SEAccountID__c='ABC1234');      
        insert acc1;
        
        //Utils_TestMethods.createLeadWithAcc(Id accId, Id countryId, Decimal priority)
		Lead myLead=Utils_TestMethods.createLeadWithAcc(acc1.id,ct.id,1);
		insert myLead;
        
		acc1=[Select id,Name,ToBeDeleted__c, (select id from Leads__r),(select id from Opportunities), (select id from Cases) From Account Where id=:acc1.id];
		AccountUpdateRequest__c aur = new AccountUpdateRequest__c(Account__c = acc1.id,
                                                             AccountName__c = acc1.name,
                                                             City__c='Test City',
                                                             Street__c='Test Street',
                                                             Country__c=ct.id,
                                                             ZipCode__c='012345',
                                                             ToBeDeleted__c=true);
                                                             //Approver__c=UserInfo.getUserId());
        insert aur;
        
        ApexPages.StandardController controller=new ApexPages.StandardController(aur);
        new VFC_AccountUpdateRequestMessage(controller);
	}
	
}