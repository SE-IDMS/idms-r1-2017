public class IDMSCreateUserAsync implements Queueable,Database.AllowsCallouts{
    
    User UserRecord;
    String password;
    
    public IDMSCreateUserAsync(User UserRecord, String password){      
        this.UserRecord = UserRecord;
        this.password = password;
        system.debug('in constructor:' +'password = '+ password + 'userRecord = '+ UserRecord);        
    }
    
    public void execute(QueueableContext context) {        
        IdmsUserRest.doPost(UserRecord,password);
    }
}