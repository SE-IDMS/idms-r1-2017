public class AP_ContactBeforeUpdateHandler
{    
    public void onBeforeUpdate(List<Contact> newContacts)
    {
        // EXECUTE BEFORE UPDATE LOGIC
        /* These following lines checks if there are open cases, leads, contract value chain players, service records, Value Chain Players and Activities (customer visits, task, events etc). 
               If yes, the contact can’t be deleted.    
        */
         
        Map<Id, Id> conCasesIds = new Map<Id, Id>();
        Map<Id, Id> conLeadsIds= new Map<Id, Id>();        
        Map<Id, Id> conContractIds= new Map<Id, Id>();
        Map<Id, Id> conVCPIds= new Map<Id, Id>();
        Map<Id, Id> conTasksIds= new Map<Id, Id>();
        Map<Id, Id> conEventsIds= new Map<Id, Id>();
        Map<Id, Id> conCustSurIds= new Map<Id, Id>();
        List<Case> cases = new List<Case>();
        List<Lead> leads = new List<Lead>();
        List<CTR_ValueChainPlayers__c> contractVCP = new List<CTR_ValueChainPlayers__c>();
        List<OPP_ValueChainPlayers__c> oppVCP = new List<OPP_ValueChainPlayers__c>();
        List<Task> tasks = new List<Task>();
        List<Event> events = new List<Event>();
        List<CustomerSatisfactionSurvey__c> custSur = new List<CustomerSatisfactionSurvey__c>();
       
            //Queries the cases for the corresponding Contact
            cases  = [select Id,ContactId,CreatedDate,Status from Case where ContactId in: newContacts and ((Status!=:Label.CLDEC13CCC04 and Status!=:Label.CL00326) or CreatedDate>:Date.today().addmonths(-6))];
            
            for(Case c: cases)
            {
                conCasesIds.put(c.ContactId,c.Id);
            }
            
            //Queries the Leads for the corresponding Contact
            leads = [select Id, Contact__c from Lead where Contact__c in: newContacts];
            for(Lead l: leads)
            {
                conLeadsIds.put(l.Contact__c, l.Id);
            }

            //Queries Contract Value Chain Player for the corresponding Contact
            contractVCP = [select Id,Contact__c from CTR_ValueChainPlayers__c where Contact__c in: newContacts];
            for(CTR_ValueChainPlayers__c cntrct: contractVCP)
            {
                conContractIds.put(cntrct.Contact__c, cntrct.Id);
            }

            //Queries Value Chain Player for the corresponding Contact
            oppVCP = [select Id,Contact__c from OPP_ValueChainPlayers__c where Contact__c in: newContacts];
            for(OPP_ValueChainPlayers__c vcp: oppVCP)
            {
                conVCPIds.put(vcp.Contact__c, vcp.Id);
            }

            //Queries Tasks for the corresponding Contact
            tasks = [select Id, WhoId from Task where WhoId in: newContacts];
            for(Task t: tasks)
            {
                conTasksIds.put(t.WhoId, t.Id);
            }
            
            //Queries Events for the corresponding Contact
            events = [select Id, WhoId from Event where WhoId in: newContacts];
            for(Event eve: events)
            {
                conEventsIds.put(eve.WhoId, eve.Id);
            }
            
            //Queries the Customer Experience Feedback records for the corresponding Account
            custSur = [select Id, bFOContactId__c from CustomerSatisfactionSurvey__c where bFOContactId__c in: newContacts];
            for(CustomerSatisfactionSurvey__c csat: custSur)
            {
                conCustSurIds.put(csat.bFOContactId__c, csat.Id);
            }


        //Displays an error if there are open cases, leads, contract value chain players, value chain players, activities, work order, service roles, service contract or opportunity notification . 
        for(Contact con: newContacts)
        {
                if((con.ReasonForDeletion__c!=Label.CL00521) && ((conCasesIds.containsKey(con.Id)) || (conLeadsIds.containsKey(con.Id)) || (conCustSurIds.containsKey(con.Id)) || (conContractIds.containsKey(con.Id)) || (conVCPIds.containsKey(con.Id)) || (conTasksIds.containsKey(con.Id)) || (conEventsIds.containsKey(con.Id)) || (con.TECH_IsSVMXRecordPresentForContact__c==true)))
                {    
                    con.addError(Label.CLAPR14ACC01);
                }
        }
    }
    
    public void updateCustomAddrFields(List<Contact> updateContactCountryOrState, Set<String> countryCodeLst, Set<String> stateCodeLst)
    {
        Map<String,Country__c> countryMap = new Map<String,Country__c>();
        Map<String,StateProvince__c> stateMap = new Map<String,StateProvince__c>();
        
        for(Country__c ct : [SELECT ID, CountryCode__c, Name FROM Country__c WHERE CountryCode__c IN :countryCodeLst])
        {
            countryMap.put(ct.CountryCode__c,ct);
        }
        
        for(StateProvince__c st: [SELECT ID, Name, StateProvinceCode__c, CountryCode__c FROM StateProvince__c WHERE CountryCode__c IN :countryCodeLst AND StateProvinceCode__c IN :stateCodeLst])
        {
            String ctyStateKey = st.CountryCode__c + st.StateProvinceCode__c;
            stateMap.put(ctyStateKey,st);
        }
        
        for(Contact con: updateContactCountryOrState)
        {
            if(String.isNotBlank(con.OtherCountryCode))
                con.Country__c = countryMap.get(con.OtherCountryCode).Id;
            if(String.isNotBlank(con.OtherStateCode))    
                con.StateProv__c = stateMap.get(con.OtherCountryCode+con.OtherStateCode).Id;
        }
    }
    
    //BR-10632 Nov16 Release: This method will update the sync with marketo field
    public void updateMarketoContact (List<Contact> contList) {
        System.debug('Inside marketo method-->'+ contList);
        Set<ID> accId = new Set<ID>();
        for(Contact cont: contList) {
            if(cont.Country__c == Label.CLNOV16ACC003 && cont.IsPersonAccount == false && cont.PRMContact__c == false && cont.TECH_AcoountClassLevel1__c == Label.CLNOV16ACC004) {
                System.debug('----This is China Country---');
                cont.SynchronizeWithMarketo__c = Integer.Valueof(System.Label.CLNOV16ACC001); //CLNOV16ACC001 = 1;
                accId.add(cont.AccountId);
            }
            else if(cont.Country__c != Label.CLNOV16ACC003) {
                System.debug('Inside else method');
                cont.SynchronizeWithMarketo__c = Integer.Valueof(System.Label.CLNOV16ACC001); //CLNOV16ACC001 = 1;
                accId.add(cont.AccountId);
            }
            
        }
        System.debug('Potential Accounts to be updated'+accId);
        if(accId.size() > 0)
            AP30_UpdateAccountOwner.updateSynchwithmarketo(accId);
    }
}