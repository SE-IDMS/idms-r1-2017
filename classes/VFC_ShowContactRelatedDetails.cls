/*
    Author          : Shruti Karn
    Date Created    : 02/05/2012
    Description     : Controller searches the Cases related to the Contact.
    
*/
public class VFC_ShowContactRelatedDetails
{
    public list<Case> lstCases {get;set;}
    public Contact cont {get;set;}
    public Boolean showMoreCases {get;set;}
    public VFC_ShowContactRelatedDetails(ApexPages.StandardController standardController)
    {
        cont = (Contact)standardController.getRecord();
        showMoreCases = false;
        if(cont.Id == null)
        {
            ApexPages.Message msg1 = new ApexPages.Message(ApexPages.Severity.WARNING,'No related records');
            ApexPages.addmessage(msg1); 
        
        }
        else
        {
            lstCases = new list<Case>();
            String queryCases = 'Select casenumber , status, subject , contactid from Case where contactid ='+'\'' + cont.id+'\'' +' order by createddate desc limit '+ System.Label.CLSEP12CCC20 ;
            lstCases = database.query(queryCases);
            if(lstCases.size() == Integer.valueOf(System.Label.CLSEP12CCC20))
            {
                lstCases.remove(Integer.valueOf(System.Label.CLSEP12CCC20) -1);
                showMoreCases = true;
            }
        }
    }
}