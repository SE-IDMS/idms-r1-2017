/*
    Author          : Abhishek (ACN) 
    Date Created    : 01/08/2011
    Description     : Test class for the apex class VFC48_MassDelete .
*/
@isTest
private class VFC48_MassDelete_TEST {
    
/* @Method <This method deletePlatformingsTestMethod is used test the VFC48_MassDelete class>
   @param <Not taking any paramters>
   @return <void> - <Not Returning anything>
   @throws exception - <Throwing an Error>
*/
    static testMethod void deletePlatformingsTestMethod()
    {
    Account accounts = Utils_TestMethods.createAccount();
    Database.insert(accounts); 
    
    Profile profile = [select id from profile where name='System Administrator'];
    
    User user = Utils_TestMethods.createStandardUser(profile.id,'TUser1');
    user.BypassVR__c=True;
    user.CAPRelevant__c=True;
    Database.insert(user);    
    
    User user1 = Utils_TestMethods.createStandardUser(profile.id,'RunUser');
    Database.insert(user1);   
    
    list<SFE_IndivCAP__c> Cap= new list<SFE_IndivCAP__c>();
    
    for(integer i=0;i<2;i++)
    {     
    SFE_IndivCAP__c caps = Utils_TestMethods.createIndividualActionPlan(user.id);   
    caps.ownerId= user.id;
    cap.add(caps);
    }        
    database.insert(cap);
    
    list<SFE_PlatformingScoring__c> scorings = new list<SFE_PlatformingScoring__c>();
    
    for(integer i=0;i<2;i++)
    {
        scorings.add(Utils_TestMethods.createPlatformScoring(cap[i].id,accounts.id,15,'1'));
    }
    database.insert(scorings);
    test.startTest();

    system.runAs(user1)
    {
        Pagereference pg = new pagereference('/apex/VFC48_MassDelete?retURL=%2F'+cap[0].id); 
        Test.setCurrentPageReference(pg);         
        ApexPages.StandardSetController sc3 = new ApexPages.StandardSetController(scorings);
        sc3.setSelected(scorings);      
        VFC48_MassDelete deleteScore = new VFC48_MassDelete(sc3);
            // deleteScore.deletePlatformings();  
       try
       {
 
            deleteScore.deletePlatformings();                
       }
        catch (DMLException e)
        {
           system.assert(e.getMessage().contains(Label.CL00440));                        
        }       
    }     
    system.runAs(user)
    {
    Pagereference p = new pagereference('/apex/VFC48_MassDelete?retURL=%2F'+cap[0].id); 
    Test.setCurrentPageReference(p);         
    ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(scorings);
    sc1.setSelected(scorings);      
    VFC48_MassDelete deleteScoring = new VFC48_MassDelete(sc1);
    try
    {
        deleteScoring.deletePlatformings();                
    }
    catch (DMLException e)
    {
                       
    }      
    list<SFE_IndivCAP__c> indiCap = new list<SFE_IndivCAP__c>();
    for(SFE_IndivCAP__c caps:cap)
    {
        caps.status__c='Released';
        indiCap.add(caps);
    }
    database.update(indiCap);                  
    ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(scorings);
    sc2.setSelected(scorings);      
    VFC48_MassDelete deleteScorings = new VFC48_MassDelete(sc2);
    try
    {
        deleteScorings.deletePlatformings();                
    }
    catch (DMLException e)
    {
       system.assert(e.getMessage().contains(Label.CL00441));                        
    }          
    ApexPages.StandardSetController sc4 = new ApexPages.StandardSetController(scorings);    
    VFC48_MassDelete deleteScorin = new VFC48_MassDelete(sc4);
    try
    {
        deleteScorin.deletePlatformings();                  
    }
    catch (DMLException e)
    {
       system.assert(e.getMessage().contains(Label.CL00442));                        
    } 
    }    
    test.stopTest();
    }
}