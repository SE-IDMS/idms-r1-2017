/*
Srinivas Nallapati	20-Mar-2013		May13 PRM Release
*/
public class AP_PRMChangeRequest
{
  	  
	public static void updateRequestApprover(List<ProgramChangeRequest__c> lstPrmRqs)
	{

		set<id> ProgIds = new set<id>();
		for(ProgramChangeRequest__c pcr : lstPrmRqs)
		{
			ProgIds.add(pcr.PartnerProgram__C);
		}

		Map<id,PartnerProgram__C> mapProgs = new map<id,PartnerProgram__C>([select ownerId, name, ProgramAdministrator__c, ProgramApprover__c, GlobalProgramOwner__c, RecordTypeId, RecordType.name from PartnerProgram__C where id in :ProgIds]);

		for(ProgramChangeRequest__c pcr : lstPrmRqs)
		{
			if(pcr.PartnerProgram__C != null && mapProgs.containsKey(pcr.PartnerProgram__C))
			{	
				pcr.Tech_ProgramOwner__c = mapProgs.get(pcr.PartnerProgram__C).OwnerId;
				pcr.Tech_ProgramAdministrator__c = mapProgs.get(pcr.PartnerProgram__C).ProgramAdministrator__c;
			}	
		}
	}

	
}