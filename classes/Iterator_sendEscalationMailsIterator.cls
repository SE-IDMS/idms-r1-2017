global class Iterator_sendEscalationMailsIterator
   implements Iterator<sObject>, iterable<sObject>{ 
   
   
   global Iterator<sObject> Iterator(){
      return new Iterator_sendEscalationMailsIterator();
   }
   List<ComplaintRequest__c> list1 {get; set;} 
   List<ContainmentAction__c> list2 {get; set;} 
   List<CorrectiveAction__c> list3 {get; set;} 
   List<PreventiveAction__c> list4 {get; set;} 
   //List<sObject> currentList {get; set;}
   List<List<sObject>> listOfLists{get; set;}
   Integer currentListi {get;set;}
   Integer i {get; set;} 

   public Iterator_sendEscalationMailsIterator(){
    /*
    Label.CLOCT13RR57 - Cancelled
    Label.CLOCT14I2P18--Completed
    Label.CLOCT14I2P19--Canceled
    Label.CLOCT14I2P20--Closed
    Label.CLOCT14I2P38 - Duplicate
    */
       List<String> notIn=new List<String>();
       notIn.add(Label.CLOCT13RR57);
       notIn.add(Label.CLOCT14I2P18);
       notIn.add(Label.CLOCT14I2P19);
       notIn.add(Label.CLOCT14I2P20);
       notIn.add(Label.CLOCT14I2P38);
       
       //As part of April 2015 release added 'ContactEmailAccountableOrganization__c' field on Complaint Request Query the is BR-7133. added by Ram Chilukuri
       list1 = [select id, IssueOwner__c, TECH_DueDate__c, AccountableOrganization__c,Status__c,LastNotificationDay__c,ContactEmailAccountableOrganization__c,TECH_ReminderDate__c,TECH_DueDateForNotifications__c from ComplaintRequest__c where status__c NOT IN :notIn And TECH_DueDate__c!=Null]; 
       list2 = [select id, Owner__c, DueDate__c, AccountableOrganization__c,Status__c,LastNotificationDay__c from ContainmentAction__c where status__c NOT IN :notIn]; 
       list3 = [select id, Owner__c, DueDate__c, AccountableOrganization__c,Status__c,LastNotificationDay__c from CorrectiveAction__c where status__c NOT IN :notIn]; 
       list4 = [select id, Owner__c, DueDate__c, AccountableOrganization__c,Status__c,LastNotificationDay__c from PreventiveAction__c where status__c NOT IN :notIn]; 
       listOfLists=new List<List<sObject>>();
       listOfLists.add(list1);
       listOfLists.add(list2);
       listOfLists.add(list3);
       listOfLists.add(list4);
       currentListi=0;
       i = 0; 
   }   

   global boolean hasNext(){ 
   
       //System.debug('hasNext Called');
       
       if(currentListi >= listOfLists.size()) 
       {
           //System.debug('Falied At currentListi='+currentListi +',i='+i);
           return false; 
       }

       if (listOfLists[currentListi]==null || i>=listOfLists[currentListi].size()) {
            currentListi++;
            i=0;
            return hasNext();
       }
       
       if (listOfLists[currentListi]==null || listOfLists[currentListi].size()==0) {
                currentListi++;
                return hasNext();
       }


       
           return true; 
       
   }    

   global sObject next(){ 
       // 8 is an arbitrary 
       // constant in this example
       // that represents the 
       // maximum size of the list.
       //if(i == 8){return null;}
       sObject currentSObj=listOfLists[currentListi][i];
       i++;     
/*       if (i>=listOfLists[currentListi].size()) {
            currentListi++;
            i=0;
       }
       */
       //System.debug('currentSObj:'+currentSObj);
       return currentSObj; 
   } 
}