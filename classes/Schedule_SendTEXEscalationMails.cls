/* 
BR-7353 - October 15 Release 
Divya M 
*/ 

global class Schedule_SendTEXEscalationMails implements Database.Batchable<sObject>,Schedulable{
    
    List<TEX__c> setBREIdsToProcess= new List<TEX__c>();
    
     global Schedule_SendTEXEscalationMails() {
        
    }
    global final String Query = 'select id, Name, TECH_NotificationDate__c, ReminderDate__c,Status__c, EscalationDate__c, LastNotificationDay__c, OfferExpert__c,ExpertAssessmentCompleted__c, ExpertCenter__c, ExpertCenter__r.Entity__c,ExpertCenter__r.SubEntity__c, OfferExpert__r.email, ExpertCenter__r.ContactEmail__c, ExpertCenter__r.id, FrontOfficeTEXManager__c, FrontOfficeTEXManager__r.Email,ExpertCenter__r.Name,FrontOfficeOrganization__c,FrontOfficeOrganization__r.id, FrontOfficeOrganization__r.Name, FrontOfficeOrganization__r.Entity__c, FrontOfficeOrganization__r.SubEntity__c,LineofBusiness__c, LineofBusiness__r.Entity__c, LineofBusiness__r.SubEntity__c, LineofBusiness__r.id, LineofBusiness__r.Name from TEX__c where (Status__c != \'Closed\' AND Status__c != \'Cancelled\' AND ExpertAssessmentCompleted__c = null)';
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
       return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<TEX__c> scope) {
     List<TEX__c> currList=new List<TEX__c>();
        //Schema.SObjectType currObjType=null;
        for (TEX__c tex:scope){
            if(tex.ReminderDate__c != null  || tex.EscalationDate__c != null ){ //|| tex.ReminderDate__c!= '' || tex.EscalationDate__c != ''
                currList.add(tex);
            }
        }
            if (currList!=null && currList.size()>0){
                    AP_sendTEXEscalationMails.createTEXStakeholder(currList);
            }
        } 
    
    global void finish(Database.BatchableContext BC) {
        
    }
    global void execute(SchedulableContext sc) {
        Schedule_SendTEXEscalationMails b1 = new Schedule_SendTEXEscalationMails();
        
        // label : CLAPR15I2P13 = 200
        ID batchprocessid = Database.executeBatch(b1,Integer.valueOf(System.Label.CLAPR15I2P13));           
    }
}