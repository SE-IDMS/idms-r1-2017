/*
		MODIFIED BY : Adrian MODOLEA (adrian.modolea@servicemax.com), July 28, 2016
			- 2016 Q3 MAJ : BR-10095 : Would like to be able to accept and reject individual events in the work order. 
*/
public class SVMXC_ActionWorkOrderExtension 
{
    private String sitesToken = null;
    private final AssignedToolsTechnicians__c att;
    public List<AssignedToolsTechnicians__c> attRecords {get; set;}
    private Boolean allRejected = true;
    private Boolean allAccepted = true;
    private Set<String> validOrderStatus = new Set<String>{'Customer Confirmed','Acknowledge FSE','WIP'};
    public Boolean initRunAlready = false;
    public String messageSuccess {get; set;}
    
    public String woNumber {get; set;}
    
    public Boolean showActionArea {get; set;}
    
    public List<SelectOption> rejectionReasons;
    
    public List<SelectOption> getRejectionReasons() {
    
        if (rejectionReasons == null) {
            rejectionReasons = new List<SelectOption>();
            
            Schema.DescribeFieldResult fieldResult = AssignedToolsTechnicians__c.ReasonforRejection__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            rejectionReasons.add(new SelectOption('', 'Please Choose a Rejection Reason'));
        
            for(Schema.PicklistEntry f : ple) {
                rejectionReasons.add(new SelectOption(f.getLabel(), f.getValue()));
            }        
            
        }
        return rejectionReasons;
    }
    
    public SVMXC_ActionWorkOrderExtension (ApexPages.StandardController stdController) 
    {
        att = (AssignedToolsTechnicians__c)stdController.getRecord();
    }
    
    public PageReference save () 
    {
        System.debug('##### Invoked save method()');
        
        // passed validations so we'll update the Assigned Lines with the selection made. 
        for (AssignedToolsTechnicians__c att1 : attRecords) {
			if (att1.Status__c == 'Rejected') {
				if(att1.ReasonForRejection__c == null) {
	            	throwExceptionMsg('You have selected to reject one or several lines so you must specify a Rejection Reason.', 'warning');
	            	return null;  
	        	}
	        	//att1.PrimaryUser__c = false;
			} else
				att1.ReasonForRejection__c = null;	
        }
		        
        try {
            update attRecords;
        } catch (Exception e) {
            throwExceptionMsg('An unhandled exception occured recording your Response, please contact your Administrator ' + e , 'error');
            return null;  
        }
        
        showActionArea = false;
        //throwExceptionMsg('Your response has successfully been recorded.', 'confirm');
        messageSuccess = 'Your response has successfully been recorded.';
          
        return null;
    }
    
    
    public PageReference init () {
       
        if (initRunAlready == false) {
             
            initRunAlready = true;
            if (sitesToken == null)
                sitesToken = ApexPages.currentPage().getParameters().get('token');
            
            if (sitesToken == null || sitesToken == '') {
                throwExceptionMsg('The token from your email is invalid, contact your Adminstator or Accept/Reject the WO from your mobile device instead.', 'error');
                showActionArea = false;
                return null;
            }
             
            // use token to search for Assigned Tools Tech matching record
            List<AssignedToolsTechnicians__c> attRecord = [SELECT Status__c, Id, WorkOrder__r.SVMXC__Order_Status__c, WorkOrder__r.Name,
                                                            WorkOrder__r.SVMXC__Company__r.Name, PrimaryUser__c, TechnicianEquipment__c, WorkOrder__c
                                                            FROM AssignedToolsTechnicians__c 
                                                            WHERE Sites_Access_Token__c =: sitesToken
                                                            AND Status__c<>'Cancelled' AND Status__c<>'Completed'];    
            
            if (attRecord.size() == 0) {
                throwExceptionMsg('The Work Order could not be found, contact your Adminstator or Accept/Reject the WO from your mobile device instead.', 'error');
                showActionArea = false;
                return null;
            } else {
            
                // check order status
                if (!validOrderStatus.contains(attRecord[0].WorkOrder__r.SVMXC__Order_Status__c)) {
                    throwExceptionMsg('The current Work Order Status of ' + attRecord[0].WorkOrder__r.SVMXC__Order_Status__c + ' does not allow you to Accept or Reject the Work Order.', 'error');
                    showActionArea = false;
                    return null;
                }
            }
            
            // record was found, now query all possible assignment records that need to be actioned.
            attRecords = [SELECT Status__c, Id, WorkOrder__r.SVMXC__Order_Status__c, WorkOrder__r.Name, EventID__c, 
                                                            WorkOrder__r.SVMXC__Company__r.Name, PrimaryUser__c, TechnicianEquipment__c, WorkOrder__c,
                                                            StartDateTime__c, EndDateTime__c, ReasonforRejection__c, FSR_Response__c,
                                                            TECH_FSRStartDateTime__c, TECH_FSREndDateTime__c
                                                            FROM AssignedToolsTechnicians__c 
                                                            WHERE TechnicianEquipment__c =: attRecord[0].TechnicianEquipment__c
                                                            AND WorkOrder__c =: attRecord[0].WorkOrder__c
                                                            AND Status__c != 'Cancelled' // DEF-10087 (9thJune2016) AMO
                                                            AND Status__c != 'Rejected']; // DEF-10787 (7thSept2016) AMO                  
            showActionArea = true;
            woNumber = attRecord[0].WorkOrder__r.Name;
            
            return null;
        }
        return null;
    }
    
    public void throwExceptionMsg (String message, String severity) {
    
        if (severity.equalsIgnoreCase('warning')) {
            System.debug('Throwing this warning message : ' + message);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, message, message);
            ApexPages.addMessage(msg);
        } else if (severity.equalsIgnoreCase('confirm')) {
            System.debug('Throwing this info message : ' + message);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.CONFIRM, message, message);
            ApexPages.addMessage(msg);
        } else { //assumes error
            System.debug('Throwing this error message : ' + message);
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, message, message);
            ApexPages.addMessage(msg);
        }
    }
    
    
}