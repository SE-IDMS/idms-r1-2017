@isTest
private class Batch_PRMMigrationFromV1toV2_TEST
{
    static testMethod void createMemberProperties()
    {
        User user1 = Utils_TestMethods.createStandardUserWithNoCountry('ASM');
        user1.ProgramAdministrator__c = true;
        user1.ByPassVR__c=true;
        System.runas(user1){
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c country = Utils_TestMethods.createCountry();
            country.countrycode__c ='ASM';
            insert country;
            
            Country__c country1 = Utils_TestMethods.createCountry();
            country1.countrycode__c ='ABC';
            insert country1;

            Recordtype acctActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'AccountAssignedRequirement__c' limit 1];
            Recordtype contActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'ContactAssignedRequirement__c' limit 1];
            Recordtype recCatalogActivityReq = [Select id from RecordType where developername = 'ActivityRequirement' and SobjectType = 'RequirementCatalog__c' limit 1];
            CS_RequirementRecordType__c newRecTypeMap = new CS_RequirementRecordType__c();
            newRecTypeMap.name = recCatalogActivityReq.Id;
            newRecTypeMap.AARRecordTypeID__c = acctActivityReq.Id;
            newRecTypeMap.CARRecordTypeID__c = contActivityReq.Id;

            insert newRecTypeMap;
            //creates a global partner program
            PartnerPRogram__c gp1 = new PartnerPRogram__c();
            gp1 = Utils_TestMethods.createPartnerProgram();
            gp1.TECH_CountriesId__c = country.id;
            gp1.recordtypeid = Label.CLMAY13PRM15;
            gp1.ProgramStatus__c = Label.CLMAY13PRM47;
            insert gp1;

            //creates a program level for the global program
            ProgramLevel__c prg1 = Utils_TestMethods.createProgramLevel(gp1.id);
            insert prg1;
            prg1.levelstatus__c = 'active';
            update prg1;

            //creates a country partner program
            PartnerProgram__c cp1 = Utils_TestMethods.createCountryPartnerProgram(gp1.id, country.id);
            insert cp1;
            cp1.ProgramStatus__c = 'active';
            update cp1;

            //creates a program level for the country partner program
            ProgramLevel__c prg2 = Utils_TestMethods.createCountryProgramLevel(cp1.id);
            prg2.levelstatus__c = 'active';
            insert prg2;

            //Account & Contact
            Account acc = Utils_TestMethods.createAccount();
            //acc.country__c = country.id;
            //acc.isPartner = true;
            acc.PRMAccount__c = True;
            acc.ClassLevel1__c = 'LC';
            acc.ClassLevel2__c = 'LC2';
            acc.PRMAccount__c = TRUE;
            acc.PRMUIMSId__c='AKRAM-TST';
            Insert acc;

            
            acc.IsPartner = TRUE;
            update acc;

            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test',
                FieloEE__LastName__c= 'test'
            );
            insert mem;
            
            Contact contact = Utils_TestMethods.createContact(acc.Id, 'Test Contact');
            contact.PRMContact__c = true;
            contact.FieloEE__Member__c = mem.Id;
            contact.PRMUIMSId__c='AKRAM-TST';
            Insert contact;

             ACC_PartnerProgram__c accPgm = Utils_TestMethods.createAccountProgram(prg2.id, cp1.id, acc.id);
            //acc.TECH_UniqueAssignedId__c = 
            //insert accPgm;
            accPgm.active__c = true;
            insert accPgm;

            ContactAssignedProgram__c contactAssignedProgram = new ContactAssignedProgram__c();
            contactAssignedProgram.AccountAssignedProgram__c = accPgm.Id;
            contactAssignedProgram.Active__c = True;
            contactAssignedProgram.Contact__c = contact.Id;
            contactAssignedProgram.PartnerProgram__c = cp1.id;
            contactAssignedProgram.ProgramLevel__c = prg2.id;
            insert contactAssignedProgram;

            ExternalPropertiesCatalogType__c epcExternalSystemType = new ExternalPropertiesCatalogType__c(Source__c='BFO',Type__c='MigrationToProgramV2',TypeOfSource__c='BFO',PropertiesAndCatalogRecordType__c='External Properties');
            insert epcExternalSystemType;
            

            ExternalPropertiesCatalog__c epc = new ExternalPropertiesCatalog__c(RecordTypeId=(Id)System.label.CLJUN16PRM064,ExternalPropertiesCatalogType__r=new ExternalPropertiesCatalogType__c(  FunctionalKey__c=PRM_VFC_MigrationFromV1toV2.EXTERNAL_SYSTEM_MIGRATION_FUNCTIONNALKEY),PropertyName__c='AKRAM',Subtype__c=prg2.Id);
            insert epc;

            List<Id> prgList = new List<Id>{prg1.Id,prg2.Id};
            Test.startTest();
            Batch_PRMMigrationFromV1toV2 prmMigrationBatch = new Batch_PRMMigrationFromV1toV2(prgList);
            Database.executebatch(prmMigrationBatch);
            Test.stopTest();

            List<MemberExternalProperties__c> memProp = [select Id from MemberExternalProperties__c where member__c=:mem.Id and ExternalPropertiesCatalog__c=:epc.Id];
            //System.assertEquals(1,memProp.size());
        }

    }
}