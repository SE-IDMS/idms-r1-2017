/*
    Author          : Ramakrishna Singara (Schneider Electric)
    Date Created    : 19/7/2012
    Description     : Test class for the class PROG_CostForecastUtils
*/

@isTest
private class PROG_CostForecastUtils_TEST{
    static testMethod void PROGCostForecast(){
        
        DMTBudgetValueToProjectRequest__c test= new DMTBudgetValueToProjectRequest__c(name='test',CostItemFieldAPIName__c='BusinessCashoutCAPEX__c',ProjectRequestFieldAPIName__c='BusinessCashoutCAPEX20');
        insert test;
        DMTBudgetValueToProgram__c test1= new DMTBudgetValueToProgram__c(name='test1',CostItemFieldAPIName__c='CashoutCapex__c',ProgramFieldAPIName__c='ITCashoutCAPEX20');
        insert test1;
        //creating program
        DMTProgram__c dmtProg = new DMTProgram__c();
        dmtProg.BusinessConnectProgram__c = 'Global Customer Intimacy - Best in class key account management';
        dmtProg.Name = 'B2B eCommerce';
        dmtProg.BusinessOrgFamily__c = 'Business';
        dmtProg.Business_Organization__c = 'Buildings';
        dmtProg.SEOrganizationL1__c = 'India';
        dmtProg.SEOrganizationL2__c = '';
        insert dmtProg;
        //creating program forecost1
        ProgramCostsForecasts__c progCostForecast = new ProgramCostsForecasts__c();
        progCostForecast.DMTProgramId__c = dmtProg.id;
        progCostForecast.Active__c = True;
        progCostForecast.LastActiveofBudgetYear__c = False;
        progCostForecast.Description__c = 'Test data';
        insert progCostForecast;
        //creating program forecost2
        ProgramCostsForecasts__c progCostForecast2 = new ProgramCostsForecasts__c();
        progCostForecast2.DMTProgramId__c = dmtProg.id;
        progCostForecast2.Active__c = True;
        progCostForecast2.LastActiveofBudgetYear__c = False;
        progCostForecast2.Description__c = 'Test data';
        insert progCostForecast2;
        progCostForecast.Description__c = 'Test data 2';
        update progCostForecast;
        delete progCostForecast;
                
    }
}