/*
    Author          : Bhuvana S
    Description     : Test class for AP_CFWUDABeforeUpdateHandler
*/
@isTest
private class AP_CFWUDABeforeUpdateHandler_TEST 
{
    static testMethod void testudaBeforeUpdate()
    {
        test.startTest();
            
        CFWRiskAssessment__c cfwr = new CFWRiskAssessment__c();
        cfwr.ProjectName__c = 'testProjectName';
        insert cfwr;
        
        CFWKeyBusinessObjects__c cbo = new CFWKeyBusinessObjects__c();
        cbo.CertificationRiskAssessment__c = cfwr.Id;
        insert cbo;
        
        List<RecordType> recordTypes = [select Id From RecordType where sobjecttype = 'CFWRiskAssessment__c'];
        CFWApplicationUsers__c cau = new CFWApplicationUsers__c();
        cau.CertificationRiskAssessment__c = cfwr.Id;
        insert cau;
        if(recordTypes[1]!=null)
        {
            cfwr.RecordTypeId = recordTypes[1].Id;
            update cfwr;
        }
        CFWUserDataAccess__c cuda = new CFWUserDataAccess__c();
        cuda.BusinessObjects__c = cbo.Id;
        cuda.CertificationRiskAssessment__c = cfwr.Id;
        cuda.ApplicationUsers__c = cau.Id;
        insert cuda;
        
        cuda.DataAccess__c = 'Read';
        update cuda;
        
        test.stoptest();
    }
}