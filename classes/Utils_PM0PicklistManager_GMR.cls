public class Utils_PM0PicklistManager_GMR implements Utils_PicklistManager
{
    Map<String,String> ProductLineMap; // Map between Values and Labels for the product Line Picklist
    Map<String,String> ProductFamilyMap; // Map between Values and Labels for the Product Family Picklist
    Map<String,String> BusinessMap;  // Map between Values and Labels for the Business Picklist

    Public list<String> getPickListLabels()
    {
        List<String> Labels = new List<String>();
        Labels.add(Label.CL00351);
        Labels.add(Label.CL00352);
        Labels.add(Label.CL00353);
        Labels.add(Label.CL00354);
        return Labels;
    } 

    public List<SelectOption> getPicklistValues(Integer level, String InputString)
    {  
        String SearchString;
     
        if(BusinessMap == null)
        {
            BusinessMap = new Map<String,String>(); 
            Schema.DescribeFieldResult Business = Schema.sObjectType.OPP_Product__c.fields.BusinessUnit__c;
            for (Schema.PickListEntry PickVal : Business.getPicklistValues())
            {
                BusinessMap.put(PickVal.getValue(),PickVal.getLabel());
            }
        }
        
        if(ProductLineMap == null)
        {
            ProductLineMap = new Map<String,String>(); 
            Schema.DescribeFieldResult pickList = Schema.sObjectType.OPP_Product__c.fields.ProductLine__c;
            
            for (Schema.PickListEntry PickVal : pickList.getPicklistValues())
            {
                ProductLineMap.put(PickVal.getValue(),PickVal.getLabel());
            }
        }
              
        if(ProductFamilyMap == null)
        {
            ProductFamilyMap = new Map<String,String>(); 
            Schema.DescribeFieldResult pickList = Schema.sObjectType.OPP_Product__c.fields.ProductFamily__c;
            
            for (Schema.PickListEntry PickVal : pickList.getPicklistValues())
            {
                ProductFamilyMap.put(PickVal.getValue(),PickVal.getLabel());
            }
        }
    
        
       If(InputString == Label.CL00355 || InputString=='')
           return null;
       
       List<SelectOption> options = new  List<SelectOption>();
       options.add(new SelectOption(Label.CL00355,Label.CL00355)); 
      
       if(level == 1)     
           SearchString = '__';
       else
           SearchString = InputString + '__';   
       //List<OPP_Product__c> Products = [SELECT id, Name, BusinessUnit__c, Family__c, ProductFamily__c, ProductLine__c, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE TECH_PM0CodeInGMR__c LIKE :SearchString ORDER BY TECH_PM0CodeInGMR__c ASC];
       List<OPP_Product__c> Products = [SELECT id, Name, BusinessUnit__c, Family__c, ProductFamily__c, ProductLine__c, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =: TRUE AND TECH_PM0CodeInGMR__c LIKE :SearchString ORDER BY TECH_PM0CodeInGMR__c ASC];
       For(OPP_Product__c Product:Products)
       {
           if(level == 1)
           {
               options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,Product.Name));
               /*
               if(BusinessMap.get(Product.BusinessUnit__c) != null)
                    options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,BusinessMap.get(Product.BusinessUnit__c))); 
               else            
                    options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,Product.BusinessUnit__c)); 
               */       
           }
            
           if(level == 2)
           {
               options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,Product.Name));
               
               /*
               if(ProductLineMap.get(Product.ProductLine__c) != null)
                    options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,ProductLineMap.get(Product.ProductLine__c))); 
               else            
                    options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,Product.ProductLine__c));   
               */     
           }  
           
           if(level == 3)
           {
                options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,Product.Name));
                
               /*
               if(ProductFamilyMap.get(Product.ProductFamily__c) != null)
                    options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,ProductFamilyMap.get(Product.ProductFamily__c))); 
               else            
                    options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,Product.ProductFamily__c));  
                */ 
           }  
           
           if(level == 4)
           {
                options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,Product.Name));
              // options.add(new SelectOption(Product.TECH_PM0CodeInGMR__c,Product.Family__c));   
           }  
       } 
        
       return options;
    }
}