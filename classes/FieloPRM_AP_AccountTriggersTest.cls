@isTest

public class FieloPRM_AP_AccountTriggersTest{
    
    static testMethod void testUnitTeam() {
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Program__c program = [SELECT Id, Name FROM FieloEE__Program__c limit 1][0];
            program.Name = FieloPRM_Utils_ChTeam.PRM_PROGRAM_NAME;
            update program;

            Account acc = new Account(Name = 'acc');
            insert acc;
            Account acc2 = new Account(Name = 'acc2');
            insert acc2;

            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc.Id});
            //run again, so checks that it exists a team with this account and return
            FieloPRM_Utils_ChTeam.createGamifyAccountTeams(new Set<Id>{acc.Id});

            FieloEE__Member__c member = new FieloEE__Member__c(FieloEE__LastName__c= 'Polo', FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()), FieloEE__Street__c = 'test', F_Account__c = acc.Id);
            insert member;

            member.F_Account__c = acc2.Id;
            update member;

            delete member;

            member = new FieloEE__Member__c(FieloEE__Program__c = program.id, FieloEE__LastName__c= 'Polo', FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()), FieloEE__Street__c = 'test', F_Account__c = acc.Id);
            insert member;
            
            test.starttest();
                                        
            Contact c = new contact();
            c.firstname = 'test';
            c.lastname = 'test2';
            c.accountId = acc.Id;
            c.FieloEE__Member__c = member.id; 
            insert c;
            
            FieloCH__Team__c team = new FieloCH__Team__c(Name = FieloPRM_Utils_ChTeam.TEAM_NAME_PREFIX + acc.Name , FieloCH__Program__c = Program.Id); //, F_PRM_Account__c = acc.Id
            insert team;
            
            acc.F_PRM_ProgramTeam__c = team.Id;
            
            update acc;
            test.stoptest();
            
        }
    }

}