@isTest
private class VFC_ReferentialUpdate_TEST{

    static testMethod void testForCommercialRef(){

    //Inserting ReferentialDataMapping__c Records
    List<ReferentialDataMapping__c> ReferentialDataMappingList = new List<ReferentialDataMapping__c>();
            ReferentialDataMappingList.add(new ReferentialDataMapping__c(fromfield__c='BusinessUnit__c', fromobject__c='OPP_Product__c', tofield__c='TECH_CROrderBusiness__c', active__c=true, whereclausefromfield__c='name', 
            whereclausetofield__c='CommercialReferenceOrdered__c', toobject__c='ComplaintRequest__c'));

            ReferentialDataMappingList.add(new ReferentialDataMapping__c(fromfield__c='parenthierarchy__r.BusinessUnit__c', fromobject__c='OPP_Product__c', tofield__c='TECH_CROrderBusiness__c', active__c=true, whereclausefromfield__c='name', 
            whereclausetofield__c='CommercialReferenceOrdered__c', toobject__c='ComplaintRequest__c'));

            ReferentialDataMappingList.add(new ReferentialDataMapping__c(fromfield__c='parenthierarchy__r.BusinessUnit__c', fromobject__c='OPP_Product__c', tofield__c='TECH_CROrderBusiness__c', active__c=true, whereclausefromfield__c='name', 
            whereclausetofield__c='CommercialReferenceOrdered__c', toobject__c='ComplaintRequest__c'));

/*          ReferentialDataMappingList.add(new ReferentialDataMapping__c(fromfield__c='parenthierarchy__r.BusinessUnit__c', fromobject__c='OPP_Product__c', tofield__c='Status__c', active__c=true, whereclausefromfield__c='name', 
            whereclausetofield__c='CommercialReferenceOrdered__c', toobject__c='ComplaintRequest__c'));
*/
            ReferentialDataMappingList.add(new ReferentialDataMapping__c(fromfield__c='BusinessUnit__c', fromobject__c='OPP_Product__c', tofield__c='Status__c', active__c=true, whereclausefromfield__c='name', 
            whereclausetofield__c='CommercialReferenceOrdered__c', toobject__c='ComplaintRequest__c'));

            //parenthierarchy__c
        /*
        ReferentialDataMappingList.add(new ReferentialDataMapping__c(fromfield__c='BusinessUnit__c', fromobject__c='OPP_Product__c', tofield__c='ForceManualSelection__c', active__c=true, whereclausefromfield__c='name', name='BusinessUnit__c', 
        whereclausetofield__c='CommercialReferenceOrdered__c', toobject__c='ComplaintRequest__c'));
        */
        
        /*
        ReferentialDataMappingList.add(new ReferentialDataMapping__c(fromfield__c='Name', fromobject__c='OPP_Product__c', tofield__c='ParentField__c', active__c=false, whereclausefromfield__c='Name', name='Record 2', 
        whereclausetofield__c='Name', toobject__c='Test_Product_2__c'));
        */
        
        /*
        ReferentialDataMappingList.add(new ReferentialDataMapping__c(fromfield__c='BusinessUnit__c', fromobject__c='OPP_Product__c', tofield__c='ForceManualSelection__c', active__c=false, whereclausefromfield__c='name', name='ProdToCR - Error', 
        whereclausetofield__c='CommercialReferenceOrdered__c', toobject__c='ComplaintRequest__c'));
        */
        insert ReferentialDataMappingList;

        
        //Inserting From Object Records
        OPP_Product__c OPP_ProductObj = new OPP_Product__c(
        name='REPARATIONS Error',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='02BQ8SZ2------0DZH', 
        businessunit__c='Open', /*parenthierarchy__c=OPP_ProductObj.id,*/   /* For OPP_Product__c id=a0PK0000008A7R7MAK  */isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false
        );
        insert OPP_ProductObj;
        
        List<OPP_Product__c> productList=new List<OPP_Product__c>();
        OPP_Product__c OPP_ProductObj1 = new OPP_Product__c(
        name='REPARATIONS I H M',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY', 
        businessunit__c='Open', parenthierarchy__c=OPP_ProductObj.id,   /* For OPP_Product__c id=a0PK0000008A7R7MAK  */isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false
        );
        productList.add(OPP_ProductObj1);
        
        OPP_Product__c OPP_ProductObj2 = new OPP_Product__c(
        name='REPARATIONS I H M',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY', 
        businessunit__c='INDUSTRIAL AUTOMATION', parenthierarchy__c=OPP_ProductObj.id,   /* For OPP_Product__c id=a0PK0000008A7R7MAK  */isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false
        );
        productList.add(OPP_ProductObj2);
        
        OPP_Product__c OPP_ProductObj3 = new OPP_Product__c(
        name='REPARATIONS I H M',
        productline__c='IDIBS - INDUSTRY SERVICES', productfamily__c='EXCHANGE/REPAIR', hierarchytypeoftheparent__c='PM0-FAMILY', family__c='HMI/SCADA', issparepart__c=false, tech_pm0codeingmr__c='DUMMY', 
        businessunit__c='INDUSTRIAL AUTOMATION', parenthierarchy__c=OPP_ProductObj.id,   /* For OPP_Product__c id=a0PK0000008A7R7MAK  */isactive__c=true, sdhid__c='300DZH', isdocumentation__c=false, hierarchytype__c='PM0GDP', 
        isold__c=false
        );
        productList.add(OPP_ProductObj3);
        
        insert productList;
        
        //Inserting To Object Records
        Country__c CountryObjLCR = Utils_TestMethods.createCountry();
        Insert CountryObjLCR;

        BusinessRiskEscalationEntity__c lCROrganzObj = new BusinessRiskEscalationEntity__c(
                                            Name='LCR TEST',
                                            Entity__c='LCR Entity-1',
                                            SubEntity__c='LCR Sub-Entity',
                                            Location__c='LCR Location',
                                            Location_Type__c= 'Return Center',
                                            Street__c = 'LCR Street',
                                            RCPOBox__c = '123456',
                                            RCCountry__c = CountryObjLCR.Id,
                                            RCCity__c = 'LCR City',
                                            RCZipCode__c = '500055',
                                            ContactEmail__c ='SCH@schneider.com'
                                            );
        Insert lCROrganzObj; 

        List<ComplaintRequest__c> crList=new List<ComplaintRequest__c>();
        ComplaintRequest__c ComplaintRequestObj1 = new ComplaintRequest__c(
        //name='REPARATIONS I H M',
        CommercialReferenceOrdered__c='REPARATIONS I H M',
        letterofreserve__c=false, forcemanualselection__c=false, infotransport__c='No', infolac__c='No', tech_answer__c=false, category__c='Product Selection & Services', 
        leadstoproductqualityissue__c='No', status__c='Open', tech_symptomsavailable__c=false, tech_forward__c=false, needtolaunchtechnicalcomplaint__c='No', reportingentity__c=lCROrganzObj.id,  /* For BusinessRiskEscalationEntity__c id=a1HK0000002p3V4MAI  */
        tech_isblankcomment__c=true, doescustomerreturnproduct__c='No',  accountableorganization__c=lCROrganzObj.id,   /* For BusinessRiskEscalationEntity__c id=a1HK0000002p3V4MAI  */accountableorganizationworking__c=false, infoquality__c='No', tech_external1__c=false, 
        reason__c='Catalog Selection', tech_accountable_org__c=false, qualitycheck__c=false, stockcheckrequired__c=false, priority__c='Normal', countforccl__c=false, 
        tech_ischeck__c=false, tech_isexternal__c='No', infoccc__c='No'
        );
        crList.add(ComplaintRequestObj1);
        
        ComplaintRequest__c ComplaintRequestObj2 = new ComplaintRequest__c(
        //name='REPARATIONS I H M',
        CommercialReferenceOrdered__c='REPARATIONS Error',
        letterofreserve__c=false, forcemanualselection__c=false, infotransport__c='No', infolac__c='No', tech_answer__c=false, category__c='Product Selection & Services', 
        leadstoproductqualityissue__c='No', status__c='Closed', tech_symptomsavailable__c=false, tech_forward__c=false, needtolaunchtechnicalcomplaint__c='No', reportingentity__c=lCROrganzObj.id,  /* For BusinessRiskEscalationEntity__c id=a1HK0000002p3V4MAI  */
        tech_isblankcomment__c=true, doescustomerreturnproduct__c='No',  accountableorganization__c=lCROrganzObj.id,   /* For BusinessRiskEscalationEntity__c id=a1HK0000002p3V4MAI  */accountableorganizationworking__c=false, infoquality__c='No', tech_external1__c=false, 
        reason__c='Catalog Selection', tech_accountable_org__c=false, qualitycheck__c=false, stockcheckrequired__c=false, priority__c='Normal', countforccl__c=false, 
        tech_ischeck__c=false, tech_isexternal__c='No', infoccc__c='No', SatisfactionOnQualityofAnswer__c='ABCDEFGHIJKLMNOPQRSTUVWXYZ', FinalResolution__c='ABCD', Final_Resolution__c='EFGH', SatisfactionOnSpeedofAnswer__c='ABCD'
        );
        //Final Resolution, 
        crList.add(ComplaintRequestObj2);
        insert crList;
        
        VFC_ReferentialUpdate refUpdate = new VFC_ReferentialUpdate();
        refUpdate.toObjSelected='ComplaintRequest__c';
        refUpdate.reRunErrorRecordsOnlyMap.put('ComplaintRequest__c', 'false');
        //Later 1: refUpdate.reRunErrorRecordsOnlyMap.put('ComplaintRequest__c', 'true');
        //Later 2: refUpdate.reRunErrorRecordsOnlyMap.put('ComplaintRequest__c', null);
        Test.startTest();
        refUpdate.executeBatch();
        
        refUpdate.reRunErrorRecordsOnlyMap.put('ComplaintRequest__c', 'true');
        refUpdate.executeBatch();

        refUpdate.reRunErrorRecordsOnlyMap.put('ComplaintRequest__c', null);
        refUpdate.executeBatch();
        Test.stopTest();
    }

}