/*
    Author          : Accenture Team
    Date Created    : 12/04/2012 
    Description     : Controller for Revenue Line Mass Edit page  
*/
public with sharing class  VFC92_RLMassEditController extends VFC_ControllerBase
{
    public class RLContainer{
        public Revenue_Line__c RL {get; set;}
        public String UpdateStatus {get; set;}
        public String Link {get; set;}
        public String ErrorMessage {get; set;}
        
        public RLContainer(Revenue_Line__c rl){
            this.RL = rl;
            this.UpdateStatus = '';
            this.ErrorMessage = '';
            this.Link ='/'+this.RL.id;  
        }    
    }
    
    public List<Revenue_Line__c> RLList {get; set;}
    public boolean displayPage {get;set;}
    
    public List<Revenue_Line__c> RetrieveRLList(List<Revenue_Line__c> RLList){
        List<ID> RLIdList = new List<ID>();
        List<Revenue_Line__c> RetrievedRLList = new List<Revenue_Line__c>();
        if(RLList.size() > 200){
            this.DisplayPage = false;
        }
        else{
            this.DisplayPage = true;
            for(Revenue_Line__c rl : RLList){
                RLIdList.add(rl.id);
            } 
            RetrievedRLList = [SELECT name, Opportunity__c, Revenuetype__c, PurchaseOrderNumber__c, SalesOrderNumber__c, 
            RevenueOverride__c, Probability__c, Configuration_Id__c, Unit_Price__c, Include_in_Quote__c, RLDate__c, 
            IncludedInForecast__c, Quantity__c, ExpectedShippeddate__c, TECH_CommercialReference__c, RL_Amount__c
            FROM Revenue_Line__c WHERE ID IN: RLIdList];
            
        }
        return RetrievedRLList;
    }
    
    public Map<Id, RLContainer> RLId_RLWrap {get; set;}
    public List<RLContainer> RLWrapList {get; set;}
    
    public VFC92_RLMassEditController (ApexPages.StandardSetController RLStandardSetController){
        this.RLList = (List<Revenue_Line__c>) RLStandardSetController.getSelected();
        this.RLWrapList = new List<RLContainer>();
        this.RLId_RLWrap = new Map<Id, RLContainer>();
        this.DisplayPage = false;

        this.RLList = RetrieveRLList(this.RLList);
        for (Revenue_Line__c rl : this.RLList){
            RLContainer RLWrap = new RLContainer(rl);
            this.RLWrapList.add(RLWrap);
            this.RLId_RLWrap.put(rl.id, RLWrap);
        }        
    }
    
    public void CustomSave(){
        List<Revenue_Line__c> RLListTmp = new List<Revenue_Line__c>();
        for (RLContainer RLWrap : this.RLWrapList ){
            RLWrap.UpdateStatus = '';
            RLListTmp.add(RLWrap.RL);
        }
        List<Database.SaveResult> SRList = database.update(RLListTmp , false);
        this.RLList = RetrieveRLList(this.RLList);
        integer i = 0; // element # in the result list = RL # in the RLWrapList
        while(i<SRList.size()){
            RLContainer RLWrap = this.RLId_RLWrap.get(this.RLList[i].Id);
            if (SRList[i].getErrors().size()>0){
                for(Database.Error err: SRList[i].getErrors())
                { 
                    RLWrap.ErrorMessage = err.getMessage();
                    system.debug('########## error: ' + err);
                    RLWrap.UpdateStatus = 'Not Ok';
                    RLWrap.rl = this.RLList[i];
                } 
            }
            else{
                RLWrap.UpdateStatus = 'Ok';
                RLWrap.ErrorMessage = 'Record successfully updated';
                RLWrap.rl = this.RLList[i];
            }         
            i++;
        }
    }    
}