/********************************************************************
* Company: Fielo
* Created Date: 26/04/2015
* Description: Utils class for test classes
********************************************************************/
@isTest
public with sharing class FieloPRM_UTILS_MockUpFactory {

    private static String idRecordTypeSegment;
    private static String idRecordTypeCat;
    private static PRMCountry__c prmCountry;

    public static FieloEE__RedemptionRule__c createSegmentManual(){
            
        if(idRecordTypeSegment == null){
            idRecordTypeSegment = [Select id,developerName, sobjecttype  from RecordType WHERE sobjecttype = 'FieloEE__RedemptionRule__c' and developerName = 'Manual'].id;
        }

        FieloEE__RedemptionRule__c segment = new FieloEE__RedemptionRule__c();
        segment.RecordTypeid = idRecordTypeSegment;
        segment.name = 'Segment ';
        segment.FieloEE__Description__c = 'Segment';
        segment.F_PRM_SegmentFilter__c = 'SE' + datetime.now();       
        segment.FieloEE__isActive__c = true;
        insert segment;
        return segment;
    }

    public static FieloEE__Category__c createCategory(id menuid){
            
        if(idRecordTypeCat == null){
            
            idRecordTypeCat = [Select id, developerName, sobjecttype from RecordType WHERE sobjecttype = 'FieloEE__Category__c' and developerName = 'CategoryItem'].id;
        }

        if(prmcountry == null){
            
            Country__c countrycat = new Country__c();
            countrycat.Name   = 'USA';
            countrycat.CountryCode__c = 'US';
            countrycat.InternationalPhoneCode__c = '1';
            countrycat.Region__c = 'APAC';
            INSERT countrycat;   

             prmCountry = new PRMCountry__c();
            prmCountry.Name = 'Test';
            prmCountry.CountryPortalEnabled__c = true;
            prmCountry.Country__c = countrycat.Id;
            prmCountry.PLDatapool__c = 'abcd';
            INSERT prmcountry;
        }
        
        FieloEE__Category__c cat = new FieloEE__Category__c(

            
            Name = 'News', 
            recordtypeId = idRecordTypeCat, 
            F_PRM_Menu__c = menuid,
            F_PRM_Country__c = prmCountry.id,
            F_PRM_CategoryFilter__c = 'P-US-' + datetime.now()
        );
        Insert cat;
        return cat;
    }
    public static FieloEE__Category__c createCategory(){
            
        if(idRecordTypeCat == null){
            
            idRecordTypeCat = [Select id, developerName, sobjecttype from RecordType WHERE sobjecttype = 'FieloEE__Category__c' and developerName = 'CategoryItem'].id;
        }

        if(prmcountry == null){
            Country__c countrycat = new Country__c();
            countrycat.Name   = 'USA';
            countrycat.CountryCode__c = 'US';
            countrycat.InternationalPhoneCode__c = '1';
            countrycat.Region__c = 'APAC';
            INSERT countrycat;   
               
             prmCountry = new PRMCountry__c();
            prmCountry.Name = 'Test';
            prmCountry.CountryPortalEnabled__c = true;
            prmCountry.Country__c = countrycat.Id;
            prmCountry.PLDatapool__c = 'abcd';
            INSERT prmcountry;    
        }

        FieloEE__Category__c cat = new FieloEE__Category__c(
            Name = 'News', 
            recordtypeId = idRecordTypeCat,
            F_PRM_Country__c = prmcountry.id,
            F_PRM_CategoryFilter__c = 'cat' + datetime.now()
        );
        Insert cat;
        return cat;
    }
    public Static FieloEE__Program__c createProgram(){
        FieloEE__Program__c program = new FieloEE__Program__c(Name = 'PRM Is On', FieloEE__SiteURL__c ='https://secommunities.force.com/partners/', FieloEE__RecentRewardsDays__c = 1);
        insert program;
        return program;
    }
    /**
    * @author Diego Amarante
    * @date 12/08/2013
    * @description Creates and inserts a challenge and an attachment
    * @param String name 
    * @param String mode 
    * @param String competitionType
    * @return FieloCH__Challenge__c 
    */ 
    public static FieloCH__Challenge__c createChallenge(String name, String mode, String competitionType){
        FieloEE__Program__c program = createProgram();
        Date fecha = system.today();
        FieloCH__Challenge__c challenge = new FieloCH__Challenge__c(Name = name, FieloCH__Mode__c = mode, FieloCH__CompetitionType__c = competitionType, 
            FieloCH__Subscription__c = 'Global', FieloCH__InitialDate__c = fecha.addDays(-5), FieloCH__FinalDate__c = fecha.addDays(5), 
            FieloCH__SubscribeFrom__c = null, FieloCH__SubscribeTo__c = null, F_PRM_ChallengeFilter__c = 'Ch' + datetime.now() , FieloCH__Program__c = program.Id);
        insert challenge;
        Attachment a = new Attachment(name = 'test', Body = Blob.valueOf('test'), parentId = challenge.Id);
        insert a;
        return challenge;
    }
    
    /**
    * @author Diego Amarante
    * @date 12/08/2013
    * @description Activates and updates challenge
    * @param FieloCH__Challenge__c challenge
    * @return FieloCH__Challenge__c 
    */ 
    public static FieloCH__Challenge__c activateChallenge(FieloCH__Challenge__c challenge){
        challenge.FieloCH__IsActive__c = true;
        update challenge;
        return challenge;
    }
    
    /**
    * @author Diego Amarante
    * @date 12/08/2013
    * @description Creates and inserts a mission with objective
    * @param String name 
    * @param String selectedObject 
    * @param String objetiveType 
    * @param String fieldName 
    * @param String operator 
    * @param Decimal objetiveValue
    * @return  FieloCH__Mission__c
    */ 
    public static FieloCH__Mission__c createMissionWithObjective(String name, String selectedObject, String objetiveType, String fieldName, String operator, Decimal objetiveValue){
        RecordType recordType = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'WithObjective' limit 1];
        FieloCH__Mission__c mission = new FieloCH__Mission__c(Name = name, FieloCH__Object__c = selectedObject, FieloCH__ObjectiveType__c = objetiveType, 
            FieloCH__FieldName__c = fieldName, FieloCH__Operator__c = operator, FieloCH__ObjectiveValue__c = objetiveValue, RecordTypeId = recordType.Id, F_PRM_MissionFilter__c  = 'mi' + datetime.now());
        insert mission;
        return mission;
    }
    
    /**
    * @author Diego Amarante
    * @date 12/08/2013
    * @description Creates and inserts a mission without objective
    * @param String name 
    * @param String selectedObject 
    * @param String objetiveType
    * @return  FieloCH__Mission__c
    */ 
    public static FieloCH__Mission__c createMissionWithoutObjective(String name, String selectedObject, String objetiveType){
        RecordType recordType = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'WithoutObjective' limit 1];
        FieloCH__Mission__c mission = new FieloCH__Mission__c(Name = name, FieloCH__Object__c = selectedObject, FieloCH__ObjectiveType__c = objetiveType, 
            RecordTypeId = recordType.Id, F_PRM_MissionFilter__c  = 'mi' + datetime.now());
        insert mission;
        return mission;
    }
    
    /**
    * @author Diego Amarante
    * @date 12/08/2013
    * @description Creates and inserts a challenge mission
    * @param Id IdChal 
    * @param Id IdMis
    * @return FieloCH__MissionChallenge__c  
    */ 
    public static FieloCH__MissionChallenge__c createMissionChallenge(Id IdChal , Id IdMis ){
        FieloCH__MissionChallenge__c mch = new FieloCH__MissionChallenge__c(FieloCH__Challenge__c = IdChal, FieloCH__Mission__c = IdMis);
        insert mch;
        return mch;
    }
    
    /**
    * @author Diego Amarante
    * @date 12/08/2013
    * @description Creates and inserts a challenge member
    * @param FieloCH__Challenge__c challenge 
    * @param FieloEE__Member__c member
    * @return FieloCH__ChallengeMember__c  
    */ 
    public static FieloCH__ChallengeMember__c createChallengeMember(FieloCH__Challenge__c challenge, FieloEE__Member__c member){
        FieloCH__ChallengeMember__c chm = new FieloCH__ChallengeMember__c(FieloCH__Challenge2__c = challenge.Id, FieloCH__Member__c = member.Id);
        insert chm;
        return chm;
    }

    /**
    * @author Diego Amarante
    * @date 12/08/2013
    * @description Creates and inserts a mission criteria
    * @param String fieldType 
    * @param String field 
    * @param String operator 
    * @param String textValue 
    * @param Decimal numberValue 
    * @param Date dateValue 
    * @param Boolean booleanValue 
    * @param Id missionId
    * @return  FieloCH__MissionCriteria__c
    */    
    public static FieloCH__MissionCriteria__c createCriteria(String fieldType, String field, String operator, String textValue, Decimal numberValue, Date dateValue, Boolean booleanValue, Id missionId ){
        FieloCH__MissionCriteria__c result = new FieloCH__MissionCriteria__c();
        
        if(fieldType == 'Text' || fieldType == 'List' ){
            result.FieloCH__Values__c = textValue;
        }
        if(fieldType == 'Number'){
            result.FieloCH__NumberValue__c = numberValue;
        }
        if(fieldType == 'Date'){
            result.FieloCH__DateValue__c = dateValue;
        }
        if(fieldType == 'Boolean'){
            result.FieloCH__BooleanValue__c = booleanValue;
        }
        
        result.FieloCH__FieldType__c = fieldType;
        result.FieloCH__Operator__c = operator;
        result.FieloCH__FieldName__c = field;
        result.FieloCH__Mission__c = missionId;
        insert result;
        return result;
    }
    
    /**
    * @author Diego Amarante
    * @date 12/08/2013
    * @description Creates and inserts a mission criteria
    * @param FieloCH__Mission__c mission
    * @return  FieloCH__MissionCriteria__c
    */    
    public static FieloCH__MissionCriteria__c createCriteria(FieloCH__Mission__c mission){
        FieloCH__MissionCriteria__c criteria = new FieloCH__MissionCriteria__c(FieloCH__FieldName__c = 'Name' , FieloCH__Mission__c = mission.Id);
        insert criteria;
        return criteria;
    }
    
    /**
    * @author Diego Amarante
    * @date 12/08/2013
    * @description Creates and inserts a ChallengeReward
    * @param FieloCH__Challenge__c challenge
    * @return  FieloCH__ChallengeReward__c
    */    
    public static FieloCH__ChallengeReward__c createChallengeReward(FieloCH__Challenge__c challenge){
        FieloCH__ChallengeReward__c reward = new FieloCH__ChallengeReward__c(FieloCH__Challenge__c = challenge.Id);
        insert reward;
        return reward;
    }
    
    /**
    * @author Diego Amarante
    * @date 12/08/2013
    * @description Sets challenge rewards values. If specified (by last boolean value ),updates it
    * @param FieloCH__ChallengeReward__c chal 
    * @param Decimal points 
    * @param String fieldType 
    * @param String field 
    * @param String tValue 
    * @param Date dValue 
    * @param Decimal nValue 
    * @param Boolean bValue 
    * @paramId badgeId 
    * @param Id rewardId 
    * @param Boolean doUpdate
    * @return  void
    */ 
    public static void setChallengeReward(FieloCH__ChallengeReward__c chal, Decimal points, String fieldType, String field, String tValue, Date dValue, Decimal nValue, Boolean bValue,Id badgeId,Id rewardId, Boolean doUpdate){

        if(field != null && field !='' && fieldType != null && fieldType !='' ){
            if(fieldType == 'Text'){
                chal.FieloCH__Value__c = tValue;
            }
            if(fieldType == 'Date'){
                chal.FieloCH__DateValue__c = dValue;
            }
            if(fieldType == 'Numeric'){
                chal.FieloCH__NumberValue__c = nValue;
            }
            if(fieldType == 'Boolean'){
                chal.FieloCH__BooleanValue__c = bValue;
            }
    
            chal.FieloCH__FieldType__c = fieldType;
            chal.FieloCH__FieldToUpdate__c = field;
        }
        
        if(points != null){
            chal.FieloCH__TransactionPoint__c = points;
            chal.FieloCH__TransactionType__c = 'Adjust';
        }
        
        if(rewardId != null){
            chal.FieloCH__Reward__c = rewardId;
        }
        
        if(badgeId != null){
            chal.FieloCH__Badge__c = badgeId;
        }
        
        if(doUpdate){
            update chal;
        }
    } 

}