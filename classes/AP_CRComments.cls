/*
    Author        : Swapnil Saurav
    Date Created  : 12/04/2016
    Description   : C/R Comments event handler.
    ---------------------------------------------------
*/

Public class AP_CRComments
{
    
     /* **********************************************************************************
    // APRIL2016 RELEASE - SWAPNIL SAURAV - 15April2016
    // Method Name : updateCommentsOnCR
    //The below method will take the new comments record created to Comments field on the associated Complaint/Request record
    // ********************************************************************************** */
    
    Public Static void updateCommentsOnCR(List<CR_Comments__c> listCRComments) {
    //Declare variables
        Map<Id,ComplaintRequest__c> listCR = new Map<Id,ComplaintRequest__c>();
        List<ComplaintRequest__c> CRList = new List<ComplaintRequest__c>();
        List<ComplaintRequest__c> CRUpdateList = new List<ComplaintRequest__c>();
        ComplaintRequest__c singleCR = new ComplaintRequest__c();
        //listCR = [SELECT ID,Comments__c from ComplaintRequest__c WHERE ID in listCRComments.Complaint_Request__c];
        for(CR_Comments__c crComment: listCRComments) {
            singleCR.id = crComment.Complaint_Request__c;
            singleCR.Comments__c = '<b>' + crComment.TECH_CreatorName__c + ', ' +crComment.CreatedDate + ' GMT</b> - ' + crComment.Comment__c ;
            System.debug('*****This is a single comment******'+singleCR);
            listCR.put(singleCR.id,singleCR);
        }
        System.debug('List CR with all CR records'+listCR);
        CRList = [SELECT ID,Comments__c from ComplaintRequest__c WHERE ID IN: listCR.keyset()];
        for(ComplaintRequest__c cr: CRList) {
            if(listCR.containsKey(cr.id)) {
                if(cr.Comments__c != null) {
                    cr.Comments__c = listCR.get(cr.Id).Comments__c +'<br/>' + cr.Comments__c;
                }
                else {
                    cr.Comments__c = listCR.get(cr.Id).Comments__c +'<br/>';    
                }
                CRUpdateList.add(cr);   
            }
        }  
        System.debug('****Final CR list to update****'+CRUpdateList);
        if(CRUpdateList.size() > 0) {
        	update(CRUpdateList);    
        }
    }
}