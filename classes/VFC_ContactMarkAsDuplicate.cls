/****************************************************************************/
//Author         :    Pooja Suresh
//Description    :    Controller for marking duplicate contacts
//Release        :    May 2013
/****************************************************************************/
public with sharing class VFC_ContactMarkAsDuplicate { 


  public Map<String,Contact> mapDuplicates=new Map<String,Contact>(); 
  public ID selMasterID {get; set;}
  public Boolean checked = TRUE;

  public VFC_ContactMarkAsDuplicate(ApexPages.StandardSetController controller) {
    if(!Test.isRunningTest())
      controller.addFields(new List<String>{'ToBeDeleted__c'});  
    for(Contact contactRecord:(List<Contact>)controller.getSelected())  
      mapDuplicates.put(contactRecord.id,contactRecord);  
  }


  public PageReference selectAsMaster()
  {
   try{
    if(selMasterID!=null)
    {      
      if (mapDuplicates.get(selMasterID).ToBeDeleted__c){
        ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,label.CLMAY13ACC05));
        return null;
      }
    }
    for(Contact contactRecord:mapDuplicates.values())
    {
      if((contactRecord.id+'')!=selMasterID)
      {
        contactRecord.ToBeDeleted__c = checked;
        contactRecord.ReasonForDeletion__c = 'Duplicate';
        contactRecord.DuplicateWith__c = Id.valueOf(selMasterID);  
      }    
    }
    mapDuplicates.remove(selMasterID);
    update mapDuplicates.values();

    

    return new PageReference('/' + ApexPages.CurrentPage().getparameters().get('id'));
 }
 catch(DMLException e){
  ApexPages.addMessages(e);
  return null;
}  
}

}