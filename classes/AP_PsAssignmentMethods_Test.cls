@isTest
public class AP_PsAssignmentMethods_Test {

     static testMethod void testActivateUser() {
        
        Profile profile = [SELECT Id from profile WHERE Name='System Administrator'];  
        User adminUser  = [SELECT Id FROM User WHERE ProfileId = :profile.Id AND isActive = true LIMIT 1];

        System.runAs (adminUser) {
        
            Test.startTest();

            Integer max = 5;
            String randomString = EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(1,max).toUpperCase();

            User inactiveUser = new User(alias = randomString, email= randomString+'@schneider-electric.com', IsActive = false,
                                         emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                         localesidkey='en_US', profileid = profile.Id, BypassWF__c = true,
                                         timezonesidkey='Europe/London', username= randomString+ '@bridge-fo.com', CAPRelevant__c=true, VisitObjDay__c=1,
                                         AvTimeVisit__c=3,WorkDaysYear__c=200,BusinessHours__c=8,UserBusinessUnit__c='Cross Business',Country__c='FR',
                                         BypassVR__c=true);
            
            insert inactiveUser;
            
            List<PermissionSetGroup__c> permissionSetGroupList =  permissionTestMethods.createPermissionSetGroups();        
            ApexPages.StandardController userController = new ApexPages.StandardController(inactiveUser);
            
            VFC_PermissionSetAssignment viewGroupPermissionController = new VFC_PermissionSetAssignment(userController);        
                        
            // Search by Name
            viewGroupPermissionController.searchName            = permissionSetGroupList.get(0).Name;
                        
            viewGroupPermissionController.search();
            
            ApexPages.currentPage().getParameters().put('selectId', permissionSetGroupList.get(0).Id);
            
            viewGroupPermissionController.selectGroup();

            viewGroupPermissionController.saveAssignment();
            
            UserRole testUserRole = new UserRole(Name = 'TestRole');
            insert testUserRole;
            
            inactiveUser.IsActive = true;
            inactiveUser.UserRoleId = testUserRole.Id;
            update inactiveUser;             

            Test.stopTest();
        }
    }

     static testMethod void testDeleteAssignment() {
        
        Profile profile = [SELECT Id from profile WHERE Name='System Administrator'];  
        User adminUser  = [SELECT Id FROM User WHERE ProfileId = :profile.Id AND isActive = true LIMIT 1];

        System.runAs (adminUser) {
        
            Test.startTest();

            Integer max = 5;
            String randomString = EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(1,max).toUpperCase();

            List<PermissionSetGroup__c> permissionSetGroupList =  permissionTestMethods.createPermissionSetGroups();        
            ApexPages.StandardController userController = new ApexPages.StandardController(adminUser);
            
            VFC_PermissionSetAssignment viewGroupPermissionController = new VFC_PermissionSetAssignment(userController);        
                        
            // Search by Name
            viewGroupPermissionController.searchName            = permissionSetGroupList.get(0).Name;
                        
            viewGroupPermissionController.search();
            
            ApexPages.currentPage().getParameters().put('selectId', permissionSetGroupList.get(0).Id);
            
            viewGroupPermissionController.selectGroup();

            viewGroupPermissionController.saveAssignment();

            viewGroupPermissionController = new VFC_PermissionSetAssignment(userController);        
            ApexPages.currentPage().getParameters().put('removeId', permissionSetGroupList.get(0).Id);
            viewGroupPermissionController.removeGroup();
            viewGroupPermissionController.saveAssignment();
                                    
            Test.stopTest();
        }
    } 
}