public with sharing class RecordServices {
    
    @AuraEnabled    
    public static List<Step__c> getSteps(String appId){
        return [SELECT Id,Name,app__c,ownerselectionforthisstep__c ,predefineduser__c,stepnumber__c,entrycondition__c,RecordType.Name, statusvalueonentrytothisstep__c, statusvalueonrejectonthisstep__c,actionvalueforreject__c, rejectbuttonlabel__c, approvebuttonlabel__c, actionvalueforapprove__c,predefinedgroup__c,predefinedgroup__r.Name,sendtoprevioussteplabel__c   FROM Step__c where app__c=:appId order by stepnumber__c ASC];
    }
    
    @AuraEnabled
    public static Map<Integer,Step__c> getStepsByNumber(String appId){
        Map<Integer,Step__c> steps = new Map<Integer,Step__c>();
        for(Step__c step: [SELECT Id,Name,app__c,stepnumber__c,entrycondition__c,RecordType.Name,ownerselectionforthisstep__c ,predefineduser__c,predefineduser__r.Name, statusvalueonentrytothisstep__c, statusvalueonrejectonthisstep__c,actionvalueforreject__c, rejectbuttonlabel__c, approvebuttonlabel__c, actionvalueforapprove__c,predefinedgroup__c,predefinedgroup__r.Name,sendtoprevioussteplabel__c  FROM Step__c where app__c=:appId])
            steps.put(Integer.valueOf(step.stepnumber__c),step);
        return steps;
    }
    
    
    @AuraEnabled
    public static List<Field__c> getFields(String stepId)
    {
        return [SELECT Id,Name,appdatafieldmap__c,displayname__c, fieldtype__c, section__c, step__c, targetobject__c, app__c, referencelookup__c, isreadonly__c, updateparentonchange__c, layoutposition__c, lookuplistviewid__c, lookuplistviewname__c, lookupsoqlquery__c, position__c, required__c, updatablethroughactiononly__c,controllingfield__c,isdependent__c,fields__c,referencelookupfield__c, (SELECT id,Name,isdefault__c,controllingfieldvalue__r.Name,controllingfieldvalue__r.Field__c from FieldValues__r) FROM Field__c where step__c=:stepId];
    }
    
    @AuraEnabled
    public static List<Field__c> getFieldsByStepNumber(String appId,Integer stepNumber)
    {
        return [SELECT Id,Name,appdatafieldmap__c,displayname__c, fieldtype__c, section__c, step__c,step__r.Name,step__r.RecordType.Name, targetobject__c, app__c,app__r.Name,app__r.wikilink__c, referencelookup__c, isreadonly__c, updateparentonchange__c, layoutposition__c, lookuplistviewid__c, lookuplistviewname__c, lookupsoqlquery__c, position__c, required__c, updatablethroughactiononly__c,helptext__c ,controllingfield__c,isdependent__c,fields__c,referencelookupfield__c,app__r.ObjectShortcut__c,(SELECT id,Name,isdefault__c,controllingfieldvalue__r.Name,controllingfieldvalue__r.Field__c from FieldValues__r) FROM Field__c where step__r.stepnumber__c=:stepNumber and step__r.app__c=:appId];
    }
    
    @AuraEnabled
    public static String getEntryStatusByStepNumber(String appId,Integer stepNumber){
        return [SELECT statusvalueonentrytothisstep__c from Step__c where App__c=:appId and stepnumber__c=:stepNumber].statusvalueonentrytothisstep__c;
    }
    
    @AuraEnabled
    public static String getEntryStatusByStepId(String stepId){
        return [SELECT statusvalueonentrytothisstep__c from Step__c where Id=:stepId].statusvalueonentrytothisstep__c;
    }
    
    @AuraEnabled
    public static String getApproveActionByStepNumber(String appId,Integer stepNumber){
        return [SELECT actionvalueforapprove__c from Step__c where App__c=:appId and stepnumber__c=:stepNumber].actionvalueforapprove__c;
    }
    
    @AuraEnabled
    public static String getApproveActionByStepId(String stepId){
        return [SELECT actionvalueforapprove__c from Step__c where Id=:stepId].actionvalueforapprove__c;
    }
    
    @AuraEnabled
    public static String getApproveActionByRecordId(Id recordId){
        return [SELECT step__r.actionvalueforapprove__c from Record__c where Id=:recordId].step__r.actionvalueforapprove__c;
    }
    
    @AuraEnabled
    public static String getRejectStatusByRecordId(String recordId){
        return [SELECT Step__r.statusvalueonrejectonthisstep__c from Record__c where Id=:recordId].Step__r.statusvalueonrejectonthisstep__c;
    }
    
    @AuraEnabled
    public static String getRejectActionByRecordId(String recordId){
        return [SELECT Step__r.actionvalueforreject__c from Record__c where Id=:recordId].Step__r.actionvalueforreject__c;
    }
    
    @AuraEnabled
    public static String getRecordCurrentStepId(String recordId){
        return [SELECT Step__c from Record__c where Id=:recordId].Step__c;
    }
    
    @AuraEnabled
    public static String getStepTypeByStepId(String stepId){
        return [SELECT RecordType.Name from Step__c where Id=:stepId].RecordType.Name;
    }
    
    @AuraEnabled
    public static Integer getRecordCountByAppId(String appId){
        return [SELECT count() from Record__c where app__c=:appId];
    }
    
    @AuraEnabled
    public static String getRecordStepHistory(String recordId){
        return [SELECT steporder__c from Record__c where Id=:recordId].steporder__c;
    }
    
    @AuraEnabled
    public static String saveRecord(Record__c appDataRecord){        
        System.debug(appDataRecord);
        appDataRecord.stepowner__c=UserInfo.getUserId();
        appDataRecord.approverlist__c = '[{"attributes":{"type":"User","url":"/services/data/v37.0/sobjects/User/'+UserInfo.getUserId()+'"}},{"Name":"'+UserInfo.getFirstName()+' '+UserInfo.getLastName()+'","Id":"'+UserInfo.getUserId()+'","Email":"'+UserInfo.getUserEmail()+'"}]';
        appDataRecord.approveridlist__c = UserInfo.getUserId();
        appDataRecord.status__c=getEntryStatusByStepNumber(appDataRecord.app__c,1);
        appDataRecord.Name =appDataRecord.Name + '-'+ (getRecordCountByAppId(appDataRecord.app__c)+1).format().leftPad(6).replaceAll(' ','0'); 
        appDataRecord.steporder__c=appDataRecord.step__c;
        //appDataRecord.action__c=getApproveActionByStepNumber(appDataRecord.app__c,1);
        Database.SaveResult sr = Database.insert(appDataRecord,false);        
        transactionhistory__c transactionhistory = new transactionhistory__c(Record__c=appDataRecord.Id);        
        transactionhistory.step__c=[select step__c from Record__c where id=:appDataRecord.Id].step__c;        
        transactionhistory.description__c='created the record';
        transactionhistory.status__c=appDataRecord.status__c;
        transactionhistory.isstatuschange__c=true;
        Database.insert(transactionhistory,true);
        return JSON.serialize(sr);
    }
    
    @AuraEnabled
    public static String updateRecord(Record__c appDataRecord,String changes){        
        RecordUpdates recordupdates = new RecordUpdates();
        return recordupdates.updateRecordGodMode(appDataRecord,changes);        
    }
    
    @AuraEnabled
    public static List<Object> getQueryData(String query){
        return Database.query(query+' LIMIT 500');
    }
    
    @AuraEnabled
    public static String getRecordInfo(String recordId){
        //determine the current step and appid from the recordId
        Record__c record = [select Id,Name,step__c,app__c,step__r.stepnumber__c,step__r.RecordType.Name,stepowner__c,status__c,message__c,isRejected__c,stepowner__r.Name,OwnerId,Owner.Name,iscurrentowner__c,managerid__c,managername__c,approverlist__c,CreatedById,CreatedBy.Name,steporder__c from Record__c where Id=:recordId ];
        Map<Integer,List<Field__c>> stepNumberFieldsMap = New Map<Integer,List<Field__c>>();        
        for(Integer i=1;i<=record.step__r.stepnumber__c;i++){
            stepNumberFieldsMap.put(i,getFieldsByStepNumber(record.app__c,i));
        }
     //   System.debug(stepNumberFieldsMap);
        List<String> fieldsinquery = new List<String>();
        Map<Integer,Map<String,Object>> returnresult = new Map<Integer,Map<String,Object>>();
        for(Integer stepnumber : stepNumberFieldsMap.keySet()){
            for(Field__c field: stepNumberFieldsMap.get(stepnumber))
                fieldsinquery.add(field.appdatafieldmap__c);            
        }
        System.debug(fieldsinquery);
        String querytobeused = 'select Id,Name,status__c,action__c,message__c,isRejected__c,stepowner__c,stepowner__r.Name,OwnerId,Owner.Name,iscurrentowner__c,managerid__c,managername__c,CreatedById,CreatedBy.Name,steporder__c,'+String.join(fieldsinquery,',')+' from Record__c where id=\''+recordId+'\'';
        System.debug(querytobeused);
        Record__c completedatarecord = Database.query(querytobeused);
        Map<String,Field__c> fieldinfomap = new Map<String,Field__c>();
        for(Integer stepnumber : stepNumberFieldsMap.keySet()){
            Map<String,Object> retres = new Map<String,Object>();
            for(Field__c field: stepNumberFieldsMap.get(stepnumber)){
                retres.put(field.displayname__c,completedatarecord.get(field.appdatafieldmap__c));
                fieldinfomap.put(field.displayname__c,field);
            }
            returnresult.put(stepnumber,retres);
        }
        
        
        RecordInfo rf  = new RecordInfo();
        rf.data = returnresult;
        rf.name = record.Name;
        rf.stepnumber = Integer.valueOf(record.step__r.stepnumber__c);
        rf.steptype = record.step__r.RecordType.Name;
        rf.steps = getStepsByNumber(record.app__c);
        rf.ownerId=record.createdById;
        rf.ownerName=record.CreatedBy.Name;
        rf.managerId=record.managerid__c;
        rf.managerName=record.managername__c;
        rf.stepownerId=record.stepowner__c;
        rf.stepownerName=record.stepowner__r.Name; 
        if(record.approverlist__c!=null)
            rf.approverlist = JSON.deserializeUntyped(record.approverlist__c);
        rf.currentuserid = UserInfo.getUserId();
        //  rf.isApprovalRecord = true;        
        //  rf.isNewApproval = RecordServices.getApprovalHistory(recordId) == null ? true :false ;
        rf.record = completedatarecord;
        rf.iscurrentOwner = false;
        if(record.isRejected__c)
            rf.seemovetonextstepbutton=false;
        else
            rf.seemovetonextstepbutton = (record.stepowner__c==UserInfo.getUserId());		        
        rf.fieldinfomap = fieldinfomap;        
        rf.history = [select CreatedDate,CreatedBy.Name,description__c,step__r.Name,step__c,status__c,isstatuschange__c,action__c,isactionchange__c from transactionhistory__c where Record__c=:completedatarecord.Id order by CreatedDate DESC];
        return JSON.serialize(rf);        
    }    
    
    public class RecordInfo{
        public Map<Integer,Map<String,Object>> data;
        public Map<Integer,Step__c> steps;
        public Map<String,Field__c> fieldinfomap;
        public String name;
        //   public Boolean isApprovalRecord;
        //   public Boolean isNewApproval;
        public String steptype;
        public Integer stepnumber;
        public Record__c record;
        public Boolean seemovetonextstepbutton;
        public List<transactionhistory__c> history; 
        public String ownerId;
        public String ownerName;
        public String stepownerId;
        public String stepownerName;
        public String managerId;
        public String managerName;
        public Boolean iscurrentOwner;
        public Object approverlist;
        public Id currentuserid;
    }
    
    @AuraEnabled
    public static String updateStep(Id stepId,Id OwnerId,String comments,Id recordId,List<User> approverlist,List<User> cclist,Id prevstepId,Id groupId,Boolean cleanhistory){		
        //first step change yourself to owner
        
        RecordUpdates recordupdates = new RecordUpdates();
        recordupdates.updateRecordOwner(recordId);
        
        if(approverlist==null)
            approverlist = new List<User>();
		if(cclist == null)
            cclist = new List<User>();
        
        List<Id> approveridslist = new List<Id>();
        String userstring=null;
        if(groupId!=null){
            userstring='[';
            List<String> usrarray = new List<String>();            
            for(groupappuserjunction__c ur:[SELECT Id, User__c,User__r.Name,User__r.Email FROM groupappuserjunction__c where Group__c=:groupId])
            {
                usrarray.add('{"attributes":{"type":"User","url":"/services/data/v37.0/sobjects/User/'+ur.User__c+'"},"Name":"'+ur.User__r.Name+'","Id":"'+ur.User__c+'","Email":"'+ur.User__r.Email+'"}');
               // approverlist.add(new User(Id=ur.User__c,Name=ur.User__r.Name,Email=ur.User__r.Email));
            }            
            userstring+=String.join(usrarray, ',')+']';
            approverlist = (List<User>)JSON.deserialize(userstring, List<User>.class);

        }
        if(OwnerId!=null){
            approverlist.add([select Id,Name,Email from User where Id = :OwnerId limit 1]);            
        }
        
        System.debug(userstring);
        System.debug(approverlist);
        if(OwnerId!=null || approverlist!=null){
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            
            
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            if(OwnerId!=null){
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                mentionSegmentInput.id = OwnerId;
                messageBodyInput.messageSegments.add(mentionSegmentInput);
            }
            else{
                for(User u: approverlist){
                    approveridslist.add(u.id);
                    ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                    mentionSegmentInput.id = u.id;
                    messageBodyInput.messageSegments.add(mentionSegmentInput);
                }   
            }
            
            
            if(comments!=null){               
                textSegmentInput.text = comments;            
                messageBodyInput.messageSegments.add(textSegmentInput);
            }
            if(cclist!=null && cclist.size()>0){ 
                ConnectApi.TextSegmentInput textSegmentInputfyi = new ConnectApi.TextSegmentInput();
                textSegmentInputfyi.text='\n FYI : ';
                messageBodyInput.messageSegments.add(textSegmentInputfyi);
            }
            for(User u: cclist){
                ConnectApi.MentionSegmentInput mentionSegmentInputfyi = new ConnectApi.MentionSegmentInput();
                mentionSegmentInputfyi.id = u.id;
                messageBodyInput.messageSegments.add(mentionSegmentInputfyi);
            }    
            
            //add the links
            ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
            feedElementCapabilitiesInput.link = new ConnectApi.LinkCapabilityInput();
            feedElementCapabilitiesInput.link.url ='/apex/VFP_AppLauncher_LightningOut?recordId=aIYP0000000CaVdOAK&mode=view';
            feedElementCapabilitiesInput.link.urlName = 'If you are on SF1 please use this link to access the record';
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = recordId;
            feedItemInput.capabilities = feedElementCapabilitiesInput;
            
            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput, null);
        }
        Record__c record = new Record__c(Id=recordId);
        if(Ownerid!=null){
            record.stepowner__c=OwnerId;
            record.approveridlist__c = OwnerId;
        }
        if(approveridslist!=null && approveridslist.size()>0)
        record.approveridlist__c = String.join(approveridslist, ',');
        record.step__c=stepId;
        record.prevstep__c=prevstepId;
        record.message__c=comments;
        record.status__c=getEntryStatusByStepId(stepId);
        if(!cleanhistory)
        record.action__c=getApproveActionByRecordId(recordId);
        record.finished__c=(getStepTypeByStepId(stepId)=='Final Step');
        record.cclist__c=JSON.serialize(cclist);
        record.approverlist__c=JSON.serialize(approverlist);
        String steporder = getRecordStepHistory(recordId);
        if(cleanhistory){
            List<String> historysteps = new List<String>();
            historysteps = steporder.split(',');
            clearDataForStep(record,historysteps[historysteps.size()-1]);            
            historysteps.remove(historysteps.size()-1);
            steporder=String.join(historysteps,',');			
	        record.steporder__c=steporder;            
        }
        else
            record.steporder__c=steporder!=null?steporder+','+stepId:stepId;

        
        
        transactionhistory__c transactionhistory_action = new transactionhistory__c(Record__c =recordId);
        if(!cleanhistory)
        transactionhistory_action.step__c=getRecordCurrentStepId(recordId); 
        else
          transactionhistory_action.step__c=stepId;  
        transactionhistory_action.description__c=comments;
        if(cleanhistory){
            if(comments!=null)
                transactionhistory_action.description__c+='    Move back to previous step ';
            else
            	transactionhistory_action.description__c='Move back to previous step ';
        }
        transactionhistory_action.isactionchange__c=true;
        transactionhistory_action.action__c=record.action__c;
        
        
        List<Database.SaveResult> srs = new List<Database.SaveResult>();
        System.debug(record);
        srs.add(Database.update(record,true));
        List<Record__Share> recordshr = new List<Record__Share>();
        if(OwnerId!=null && OwnerId!=UserInfo.getUserId()){
            Record__Share recordshrrec  = new Record__Share();      
            recordshrrec.ParentId = recordId;        
            recordshrrec.UserOrGroupId = OwnerId;        
            recordshrrec.AccessLevel = 'Edit';       
            recordshrrec.RowCause = Schema.Record__Share.RowCause.Manual;        
            recordshr.add(recordshrrec);
        }
        for(User u:approverlist){
            Record__Share recordshrrec  = new Record__Share();      
            recordshrrec.ParentId = recordId;        
            recordshrrec.UserOrGroupId = u.id;        
            recordshrrec.AccessLevel = 'Edit';       
            recordshrrec.RowCause = Schema.Record__Share.RowCause.Manual;        
            recordshr.add(recordshrrec);
        }
        for(User u:cclist){
            if(u.id != UserInfo.getUserId()){
            Record__Share recordshrrec  = new Record__Share();      
            recordshrrec.ParentId = recordId;        
            recordshrrec.UserOrGroupId = u.id;        
            recordshrrec.AccessLevel = 'Read';       
            recordshrrec.RowCause = Schema.Record__Share.RowCause.Manual;        
            recordshr.add(recordshrrec);
            }
        }
        if(recordshr.size()>0)
        srs.addAll(Database.insert(recordshr,false));
        
        transactionhistory__c transactionhistory = new transactionhistory__c(Record__c =recordId);
        transactionhistory.step__c=stepId;
        if(comments!=null && comments.length()>200)
            comments = comments.substring(0,195)+' ...';
        transactionhistory.description__c=UserInfo.getName()+'  : '+ comments; 
        transactionhistory.status__c=record.status__c;
        transactionhistory.isstatuschange__c=true;        
        List<transactionhistory__c> transactionlist = new List<transactionhistory__c>();
        transactionlist.add(transactionhistory);
        transactionlist.add(transactionhistory_action);        
        srs.addAll(Database.insert(transactionlist,true));
        return JSON.serialize(srs);
    }    
    
    public static Record__c clearDataForStep(Record__c record,Id stepId){
        List<Field__c> fields = [select Id,appdatafieldmap__c from Field__c where step__c=:stepId];
        for(Field__c field:fields){
            record.put(field.appdatafieldmap__c,null);
        }
		return record;        
    }
    
    @AuraEnabled
    public static String rejectTheRecord(Id stepId,Id recordId,String comments,List<User> cclist){
		if(cclist!=null){
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();            
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
                        
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            
            if(comments!=null){               
                textSegmentInput.text = comments;            
                messageBodyInput.messageSegments.add(textSegmentInput);
            }
            
            if(cclist!=null && cclist.size()>0){ 
                ConnectApi.TextSegmentInput textSegmentInputfyi = new ConnectApi.TextSegmentInput();
                textSegmentInputfyi.text='\n FYI : ';
                messageBodyInput.messageSegments.add(textSegmentInputfyi);
            }
            
            for(User u: cclist){
                ConnectApi.MentionSegmentInput mentionSegmentInputfyi = new ConnectApi.MentionSegmentInput();
                mentionSegmentInputfyi.id = u.id;
                messageBodyInput.messageSegments.add(mentionSegmentInputfyi);
            }    
            
            //add the links
            ConnectApi.FeedElementCapabilitiesInput feedElementCapabilitiesInput = new ConnectApi.FeedElementCapabilitiesInput();
            feedElementCapabilitiesInput.link = new ConnectApi.LinkCapabilityInput();
            feedElementCapabilitiesInput.link.url ='/apex/VFP_AppLauncher_LightningOut?recordId=aIYP0000000CaVdOAK&mode=view';
            feedElementCapabilitiesInput.link.urlName = 'If you are on SF1 please use this link to access the record';
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = recordId;
            feedItemInput.capabilities = feedElementCapabilitiesInput;
            
            ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput, null);
        }

        
        Record__c record = new Record__c(Id=recordId);
        record.message__c=comments;
        record.status__c=getRejectStatusByRecordId(recordId);
        record.action__c=getRejectActionByRecordId(recordId);
        record.isRejected__c=true;
        if(cclist!=null && cclist.size()>0)
        record.cclist__c= JSON.serialize(cclist);
        Database.SaveResult sr = Database.update(record,true);
        
        
        transactionhistory__c transactionhistory = new transactionhistory__c(Record__c =recordId);
        transactionhistory.step__c=stepId;
        if(comments!=null && comments.length()>200)
            comments = comments.substring(0,195)+' ...';
        transactionhistory.description__c=UserInfo.getName()+'  : '+ comments;
        transactionhistory.status__c=record.status__c;
        transactionhistory.isstatuschange__c=true;
        
 		transactionhistory__c transactionhistory_action = new transactionhistory__c(Record__c =recordId);
        transactionhistory_action.step__c=stepId;
        transactionhistory_action.description__c=comments;
        transactionhistory_action.isactionchange__c=true;
        transactionhistory_action.action__c=record.action__c;
        
        List<transactionhistory__c> transactionlist = new List<transactionhistory__c>();
        transactionlist.add(transactionhistory);
        transactionlist.add(transactionhistory_action); 
        
        Database.insert(transactionlist,true);
        return JSON.serialize(sr);
    }
    
    @AuraEnabled
    public static Attachment getAttachmentInfo(Id attachmentId){
        if(attachmentId!=null)
            return [SELECT Id, Name, ContentType, BodyLength, CreatedDate FROM Attachment where Id=:attachmentId];
        else
            return null;
    } 
    
    @AuraEnabled
    public static List<Record__c> getApproveRecords(String limitrecords){
        Integer limitrecord = Integer.valueOf(limitrecords);
        String useridstring = '%'+UserInfo.getUserId()+'%';
        return [select Id,Name,App__r.Name,App__c,Step__r.Name,action__c,status__c,LastModifiedDate,iscurrentowner__c,step__r.RecordType.Name,CreatedBy.Name,CreatedById,LastModifiedBy.Name,LastModifiedById,CreatedDate from Record__c where approveridlist__c like :useridstring and Step__r.RecordType.Name!='FirstStep' and finished__c=false and isRejected__c=false order by LastModifiedDate DESC  limit :limitrecord ];         
    }
    
    @AuraEnabled
    public static List<Record__c> getMyRecords(Integer limitrecords){
        Integer limitrecord = Integer.valueOf(limitrecords);
//        Map<Id,RecentlyViewed> recentrecords = new Map<Id,RecentlyViewed>([SELECT Id,Name FROM RecentlyViewed  WHERE Type ='Record__c' ORDER BY LastViewedDate DESC limit :limitrecord]);        
        return [select Id,Name,App__r.Name,App__c,Step__r.Name,LastModifiedDate,status__c,action__c,iscurrentowner__c,finished__c,step__r.RecordType.Name,LastModifiedBy.Name from Record__c where CreatedById=:UserInfo.getUserId() order by LastModifiedDate DESC limit :limitrecords ];         
    } 
    
    @AuraEnabled
    public static String deleteRecord(Id recordId){
        Database.DeleteResult sr = Database.delete(recordId);       
        return JSON.serialize(sr);
    }
    
    @AuraEnabled
    public static String undeleteRecord(Id recordId){
        Database.UndeleteResult sr = Database.undelete(recordId);       
        return JSON.serialize(sr);
    }
    
    @AuraEnabled
    public static List<Record__c> getMyRecycleBin(){
        List<Record__c> records = [select Id,Name,app__r.Name,action__c,status__c,LastModifiedDate from Record__c where LastModifiedById =:UserInfo.getUserId() and isDeleted=true order by LastModifiedDate desc ALL ROWS];
        return records;
    }
    
    @AuraEnabled
    public static List<Record__c> getAllRecycleBin(){
        List<Record__c> records = [select Id,Name from Record__c where isDeleted=true ALL ROWS];
        return records;
    }
    
    without sharing class RecordUpdates{
        public void updateRecordOwner(Id recordId){
            Record__c record_base = new Record__c(Id=recordId);
            record_base.OwnerId = UserInfo.getUserId();
            update record_base;
        }
        
        public String updateRecordGodMode(Record__c appDataRecord,String changes){
            Database.SaveResult sr = Database.update(appDataRecord,false);
            transactionhistory__c transactionhistory = new transactionhistory__c(Record__c=appDataRecord.Id);        
            transactionhistory.step__c=[select step__c from Record__c where id=:appDataRecord.Id].step__c;        
            transactionhistory.description__c=changes;
            Database.insert(transactionhistory,true);
            return JSON.serialize(sr);
        }
    }
}