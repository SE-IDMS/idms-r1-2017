@isTest
private class VFC_UnifiedSearch_TEST{

    static testMethod void DisplayCRDetailsTestMethod() {
        String jsonSelectString = Utils_WSDummyTestData.createDummyJSONString(); //This will provide the test data
        String gmrCodeToTest='02BQ8SZ2';
         
        Test.startTest();
        PageReference pageRef1 = Page.VFP_UnifiedSearch;
        pageRef1.getParameters().put('commercialReference','XBTG5230');  
        pageRef1.getParameters().put('jsonSelectString',jsonSelectString);  

        Test.setCurrentPage(pageRef1);
        
        VFC_UnifiedSearch controller1 = new VFC_UnifiedSearch();
        controller1.performSelect();
        
        controller1.jsonSelectString='';
        controller1.performSelectFamily();
        controller1.pageCancelFunction();

        VFC_UnifiedSearch.getOthers('ANYBUSINESS');
        VFC_UnifiedSearch.remoteSearch('XBTG5230', gmrCodeToTest.substring(0,2), true, false, true, true, true);
        VFC_UnifiedSearch.remoteSearch('XBTG5230', gmrCodeToTest.substring(0,4), true, false, true, true, true);
        VFC_UnifiedSearch.remoteSearch('XBTG5230', gmrCodeToTest.substring(0,6), true, false, true, true, true);
        VFC_UnifiedSearch.remoteSearch('XBTG5230', gmrCodeToTest.substring(0,8), true, false, true, true, true);

        Test.stopTest();
    }
}