/*
20-Nov-2012     Srinivas Nallapati  Initial Creation: A&C  Dec12 release. 
*/
public with sharing class VFC_AccountSearchRedirect {

    public VFC_AccountSearchRedirect(ApexPages.StandardController controller) {

    }
    
    //Redirects the Users to Account search page besed on the user profile
    public PageReference redirect()
    {
        User usr = [select name, ProfileId, Profile.Name from user where id= :UserInfo.getUserId() limit 1];
        
        String PRMprofiles = System.Label.CLDEC12AC03;
        
        List<String> listPRMProfiles = PRMprofiles.split(',');
        set<String> setPRMProfiles = new set<String>();
        for(String str : listPRMProfiles )
            setPRMProfiles.add(str);

        PageReference AcSearch;
        
        
        
        if(setPRMProfiles.contains(usr.Profile.Name))
        {
            AcSearch = page.VFP_AccountSearchByPartner;
        }
        else
        {
            AcSearch = page.VFP02_AccountSearch;
        }    
        
        if(ApexPages.CurrentPage().getParameters().get('RecordType') != null) 
        {
            AcSearch.getParameters().put('RecordType', ApexPages.CurrentPage().getParameters().get('RecordType')); 
        }
        
        return AcSearch;
    }
}