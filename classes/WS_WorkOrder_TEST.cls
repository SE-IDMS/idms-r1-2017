@isTest
public class WS_WorkOrder_TEST{
    @testSetup 
    static void testSetupMethodcountrycreation(){
            Country__c Ctry = new Country__c();
                Ctry.CountryCode__c = 'US';
                Ctry.Name = 'US';
                insert Ctry;
                
                 StateProvince__c teststateAZ = Utils_TestMethods.createStateProvince(Ctry.id);
            teststateAZ.Name = 'Arizona';
            teststateAZ.StateProvinceCode__c = 'AZ';
            teststateAZ.Country__c = Ctry.id;
            insert teststateAZ;
                Account testAccountUSNJ = new Account();
            testAccountUSNJ.Name='TestAccountUSNJ';
            testAccountUSNJ.Street__c='New Street';
            testAccountUSNJ.POBox__c='XXX';
            testAccountUSNJ.OwnerId = '00512000005c05G';//us.id;
            testAccountUSNJ.MarketSubSegment__c='B43';       
            testAccountUSNJ.Country__c=Ctry.id;
            testAccountUSNJ.StateProvince__c=teststateAZ.id;
            testAccountUSNJ.LeadingBusiness__c = 'EN';
            testAccountUSNJ.type = 'SA';
            testAccountUSNJ.SEAccountID__c = '1111';
            testAccountUSNJ.PrimaryRelationshipLeader__c = 'Field Service';
            insert testAccountUSNJ;
            
            }
       static testMethod void myUnitTest() {
      
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassTriggers__c = 'AP_WorkOrderTechnicianNotification',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;


        
        system.runAs(user){   

        Country__c testCountryUS =[select id,name from Country__c where CountryCode__c=:'US'];
        Account acc=[select id,name,SEAccountID__c from account where name=:'TestAccountUSNJ'];
          
        SVMXC__Territory__c territory = new  SVMXC__Territory__c(Name = 'territory 1', SVMXC__Territory_Code__c = '1');
        insert territory;
     
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'Test Location';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = acc.id;
        site.PrimaryLocation__c = true;
        insert site;

     
        
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);
         //workOrder.SVMXC__Contact__c = contact1.id;
         workOrder.Work_Order_Category__c='On-site';                 
         workOrder.SVMXC__Order_Type__c='Maintenance';
         workOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
         workOrder.SVMXC__Order_Status__c = 'UnScheduled';
         workOrder.CustomerRequestedDate__c = Date.today();
         workOrder.Service_Business_Unit__c = 'Energy';
         workOrder.SVMXC__Priority__c = 'Normal-Medium';
        // workOrder.Customer_Location__c = Cust_Location.Id;
         workOrder.SVMXC__Site__c = site.Id;
         workOrder.SendAlertNotification__c = true;
         workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         workOrder.BackOfficeReference__c = 'XXX';
         workOrder.SVMXC__Order_Status__c = 'New';
         workOrder.SVMXC__Primary_Territory__c = territory.id;
         workOrder.BackOfficeSystem__c =Label.CLDEC12SRV58 ; // 'CA_SAP';
         workOrder.TECHAccountDetail__c = acc.id;
         //workOrder.TECHPMsesaId__c = 'techid2';
         workOrder.CustomerTimeZone__c='Pacific/Kiritimati';
         insert workOrder;
         WO_ValueChainPlayer__c vcp1= new WO_ValueChainPlayer__c();
            vcp1.WorkOrder__c = workOrder.id;
            //vcp1.User__c = userinfo.getUserId();
            vcp1.Account__c = acc.id;
           // vcp1.TECHAccountDetail__c ='tAccount';
            insert vcp1;
            
         
          SVMXC__Service_Order__c oneWorkOrderToClose =  Utils_TestMethods.createWorkOrder(acc.id);
         //workOrder.SVMXC__Contact__c = contact1.id;
         oneWorkOrderToClose.Work_Order_Category__c='On-site';                 
         oneWorkOrderToClose.SVMXC__Order_Type__c='Maintenance';
         oneWorkOrderToClose.SVMXC__Problem_Description__c = 'BLALBLALA';
         oneWorkOrderToClose.SVMXC__Order_Status__c = 'UnScheduled';
         oneWorkOrderToClose.CustomerRequestedDate__c = Date.today();
         oneWorkOrderToClose.Service_Business_Unit__c = 'Energy';
         oneWorkOrderToClose.SVMXC__Priority__c = 'Normal-Medium';
        // oneWorkOrderToClose.Customer_Location__c = Cust_Location.Id;
          oneWorkOrderToClose.SVMXC__Site__c = site.Id;
         oneWorkOrderToClose.SendAlertNotification__c = true;
         oneWorkOrderToClose.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         oneWorkOrderToClose.BackOfficeReference__c = 'XXX';
         oneWorkOrderToClose.SVMXC__Order_Status__c = 'New';
         oneWorkOrderToClose.SVMXC__Primary_Territory__c = territory.id;
         oneWorkOrderToClose.BackOfficeSystem__c = 'CA_SAP';
         oneWorkOrderToClose.TECHAccountDetail__c = acc.id;
         oneWorkOrderToClose.TECHPMsesaId__c = 'techid';
         oneWorkOrderToClose.CustomerTimeZone__c='Pacific/Kiritimati';
         insert oneWorkOrderToClose;
         
         WO_ValueChainPlayer__c vcp = new WO_ValueChainPlayer__c(WorkOrder__c = workOrder.id);
         insert vcp;
         test.startTest();
         SVMXC__Service_Order__c closedWorkOrder =  Utils_TestMethods.createWorkOrder(acc.id);
         closedWorkOrder.Work_Order_Category__c='On-site';                 
         closedWorkOrder.SVMXC__Order_Type__c='Maintenance';
         closedWorkOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
         closedWorkOrder.SVMXC__Order_Status__c = 'Closed';
         closedWorkOrder.CustomerRequestedDate__c = Date.today();
         closedWorkOrder.Service_Business_Unit__c = 'Energy';
         closedWorkOrder.SVMXC__Priority__c = 'Normal-Medium';
         //closedWorkOrder.Customer_Location__c = Cust_Location.Id;
          closedWorkOrder.SVMXC__Site__c = site.Id;
         closedWorkOrder.SendAlertNotification__c = true;
         closedWorkOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         closedWorkOrder.BackOfficeReference__c = 'XXX';
         closedWorkOrder.SVMXC__Order_Status__c = 'New';
         closedWorkOrder.SVMXC__Primary_Territory__c = territory.id;
         closedWorkOrder.BackOfficeSystem__c = 'CA_SAP';
         closedWorkOrder.CustomerTimeZone__c='Pacific/Kiritimati';
         insert closedWorkOrder;
         
         
         SVMXC__Service_Order__c techClosedWorkOrder =  Utils_TestMethods.createWorkOrder(acc.id);
         techClosedWorkOrder.Work_Order_Category__c='On-site';                 
         techClosedWorkOrder.SVMXC__Order_Type__c='Maintenance';
         techClosedWorkOrder.SVMXC__Problem_Description__c = 'BLALBLALA';
         techClosedWorkOrder.SVMXC__Order_Status__c = 'Service Validated';
         techClosedWorkOrder.CustomerRequestedDate__c = Date.today();
         techClosedWorkOrder.Service_Business_Unit__c = 'Energy';
         techClosedWorkOrder.SVMXC__Priority__c = 'Normal-Medium';
        // techClosedWorkOrder.Customer_Location__c = Cust_Location.Id;
         techClosedWorkOrder.SVMXC__Site__c = site.Id;
         techClosedWorkOrder.SendAlertNotification__c = true;
         techClosedWorkOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         techClosedWorkOrder.BackOfficeReference__c = 'XXX1';
         techClosedWorkOrder.SVMXC__Order_Status__c = 'New';
         techClosedWorkOrder.SVMXC__Primary_Territory__c = territory.id ;
         techClosedWorkOrder.BackOfficeSystem__c = 'CA_SAP';
         techClosedWorkOrder.CustomerTimeZone__c='Pacific/Kiritimati';
            
         insert techClosedWorkOrder;
          
        WS_WorkOrder.getListWOItemResult output = new WS_WorkOrder.getListWOItemResult();
        
        
       
        
        list <WO_ValueChainPlayer__c> v = new list<WO_ValueChainPlayer__c>();
        list<SVMXC__Service_Order__c> lwo = new list<SVMXC__Service_Order__c>();
        lwo.add(workOrder);
        v = WS_WorkOrder.getListOfWOVCPToCreate(lwo);
        
        workOrder.BackOfficeSystem__c = 'ES_SAP';
        update workOrder;
        
               
        
        SVMXC__Service_Order_Line__c workDetail = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c= workOrder.id,SVMXC__End_Date_and_Time__c = date.today());
        insert workDetail;
        
        
        
        output = WS_WorkOrder.getTechCloseWorkOrders('CA_SAP','');
        
        List<Id> workOrderIds = new List<Id>();
        workOrderIds.add(workOrder.id);
        
        WS_WorkOrder.getWorkOrderDetails(workOrder.id);
        WS_WorkOrder.updateWorkOrderDetails(workOrder.id,WS_WorkOrder.UpdateEvent.SYNC,'00001', 'ES_SAP', 'OK','OperationName');
        WS_WorkOrder.updateWorkOrderDetails(workOrder.id, WS_WorkOrder.UpdateEvent.SYNC, '00001', 'ES_SAP', 'OK','OperationName');
        WS_WorkOrder.updateWorkOrderDetails(workOrder.id, WS_WorkOrder.UpdateEvent.TECO, '00001', 'ES_SAP', 'OK','OperationName');
        WS_WorkOrder.updateWorkOrderDetails(workOrder.id, WS_WorkOrder.UpdateEvent.CLOSE, '00001', 'ES_SAP', 'OK','OperationName');
        WS_WorkOrder.updateWorkOrderDetails(workOrder.id, WS_WorkOrder.UpdateEvent.UPD, '00001', 'CA_SAP', 'OK','OperationName');
        WS_WorkOrder.getWorkDetails(workOrder.id);  
        WS_WorkOrder.bulkCloseWorkOrders(workOrderIds, 'ES_SAP'); 
        //WS_WorkOrder.updateWorkDetails(workDetail.id, 'ES_SAP');  
        // Hari krishna update
        WS_WorkOrder.updateWorkDetails(workDetail.id, 'ES_SAP');
        WS_WorkOrder.updateWorkDetails(workDetail.id, 'ES_SAP'); 
        WS_WorkOrder.WorkOrder newWorkOrder = new WS_WorkOrder.WorkOrder();
        WS_WorkOrder.createWorkOrderDetails(newWorkOrder , 'ES_SAP');
      
      
      
        newWorkOrder = new WS_WorkOrder.WorkOrder();
        newWorkOrder.accountName = '1111' ;
        newWorkOrder.backOfficeSystem = 'CA_SAP';
        newWorkOrder.workOrderStatus = 'New';
        newWorkOrder.workOrderReason = 'blabla';
        newWorkOrder.workOrderType = 'Ecofit';
        newWorkOrder.customerReferenceNumber = 'TOTOT';
        newWorkOrder.customerRequestedDate = date.today();
        newWorkOrder.estimatedDuration = 5;
        newWorkOrder.serviceCenterName = '1';
        newWorkOrder.estimatedExpenseCost = 44;
        newWorkOrder.estimatedMaterialCost = 44;
        newWorkOrder.priority = 'Normal-Medium';
        newWorkOrder.ProjectManager = user.FederationIdentifier;
        newWorkOrder.plannerContactUsername = user.FederationIdentifier;
        newWorkOrder.remainingValue = 44;
        newWorkOrder.serviceBusinessUnit = 'BD';
        newWorkOrder.ServiceOrderNumber = '999999';
        newWorkOrder.workOrderCategory = 'Onsite';
        newWorkOrder.AccountDetails = user.FederationIdentifier;
        
        WS_WorkOrder.WorkOrder BadWorkOrder = new WS_WorkOrder.WorkOrder();
        BadWorkOrder.accountName = 'invalid' ;
        BadWorkOrder.backOfficeSystem = 'CA_SAP';
        BadWorkOrder.workOrderStatus = 'New';
        BadWorkOrder.workOrderReason = 'blabla';
        BadWorkOrder.workOrderType = 'Ecofit';
        BadWorkOrder.customerReferenceNumber = 'TOTOT';
        BadWorkOrder.customerRequestedDate = date.today();
        BadWorkOrder.estimatedDuration = 5;
        BadWorkOrder.serviceCenterName = 'inva-lid';
        BadWorkOrder.estimatedExpenseCost = 44;
        BadWorkOrder.estimatedMaterialCost = 44;
        BadWorkOrder.priority = 'Normal-Medium';
        BadWorkOrder.ProjectManager = 'inval';
        BadWorkOrder.plannerContactUsername = user.FederationIdentifier;
        BadWorkOrder.remainingValue = 44;
        BadWorkOrder.serviceBusinessUnit = 'BD';
        BadWorkOrder.ServiceOrderNumber = '999999';
        BadWorkOrder.workOrderCategory = 'Onsite';
        BadWorkOrder.AccountDetails = 'inval';
        
        //Work Order to Update
        WS_WorkOrder.WorkOrder WorkOrderToUpdate = new WS_WorkOrder.WorkOrder();
        WorkOrderToUpdate.btfWorkOrderId = workOrder.Id;
        //WorkOrderToUpdate.accountName = '12345';
        WorkOrderToUpdate.backOfficeSystem = 'CA_SAP';
        WorkOrderToUpdate.workOrderStatus = 'Service Delivered';
        WorkOrderToUpdate.workOrderReason = 'blabla';
        WorkOrderToUpdate.workOrderType = 'Ecofit';
        WorkOrderToUpdate.customerReferenceNumber = 'TOTOT';
        WorkOrderToUpdate.customerRequestedDate = date.today();
        WorkOrderToUpdate.estimatedDuration = 5;
        WorkOrderToUpdate.serviceCenterName = '1';
        WorkOrderToUpdate.estimatedExpenseCost = 44;
        WorkOrderToUpdate.estimatedMaterialCost = 44;
        WorkOrderToUpdate.priority = 'Normal-Medium';
        //WorkOrderToUpdate.ProjectManager = 'SESA141224';
        WorkOrderToUpdate.remainingValue = 44;
        WorkOrderToUpdate.serviceBusinessUnit = 'BD';
        WorkOrderToUpdate.ServiceOrderNumber = '999999';
        WorkOrderToUpdate.workOrderCategory = 'Onsite';
        WorkOrderToUpdate.AccountDetails = user.FederationIdentifier;
        
        
        WS_WorkOrder.WorkOrder WorkOrderToClose = new WS_WorkOrder.WorkOrder();
        WorkOrderToClose.btfWorkOrderId = oneWorkOrderToClose.Id;
        WorkOrderToClose.workOrderStatus = 'Closed';
        WorkOrderToClose.serviceCenterName = 'N/A';
        
        
        WS_WorkOrder.WorkOrder newWorkOrderMissingFields = new WS_WorkOrder.WorkOrder();     

        
        
        
        
        
        list<WS_WorkOrder.workOrderItemResult> result = new list<WS_WorkOrder.workOrderItemResult>();
        list<WS_WorkOrder.WorkOrder> input = new list<WS_WorkOrder.WorkOrder>();
        
        list<id> wOIds  = new list<id>();
        WS_WorkOrder.workOrderItemResult oneRes = new WS_WorkOrder.workOrderItemResult();

        input.add(newWorkOrder);
        input.add(WorkOrderToUpdate);
        input.add(WorkOrderToClose);
        input.add(newWorkOrderMissingFields);
        input.add(BadWorkOrder);
        
       
       list<string> userSesaIds = new list<string> ();
       list<string> seaAccountIds = new list<string> ();
       list<string> territoryCodes = new list<string>();
       
       userSesaIds.add(user.FederationIdentifier);
       territoryCodes.add(territory.SVMXC__Territory_Code__c);
       seaAccountIds.add(acc.SEAccountID__c);
       
       WS_WorkOrder.userMap(userSesaIds);
       WS_WorkOrder.accountMap(seaAccountIds);
       WS_WorkOrder.territoryMap(territoryCodes);
       
       
       
        list<SVMXC__Service_Order__c> lwOrder = new list<SVMXC__Service_Order__c>();
        list<WO_ValueChainPlayer__c> listVchain = new list <WO_ValueChainPlayer__c>();
        list<id> idwo = new list<id>();
        result = WS_WorkOrder.bulkCreateUpdateWorkOrders(input);
        input.clear();
        input.add(WorkOrderToUpdate);
        
        result = WS_WorkOrder.bulkCreateUpdateWorkOrders(input);
        idwo.add(workOrder.id);
        listVchain = WS_WorkOrder.getListOfWOVCPToUpdate(idwo);
        
        list<WS_WorkOrder.WorkOrder> inputcreate = new list<WS_WorkOrder.WorkOrder>();
        inputcreate.add(newWorkOrder);
        lwOrder = WS_WorkOrder.getWOToBeCreated(inputcreate);
        WS_WorkOrder.BulkCreateWo(inputcreate);
        list<WS_WorkOrder.WorkOrder> inputupdate = new list<WS_WorkOrder.WorkOrder>();
        inputupdate.add(WorkOrderToUpdate);
        inputupdate.add(WorkOrderToClose);
        lwOrder = WS_WorkOrder.getWOToBeCreated(inputupdate);
        //commented on 17 oct 2013
       // WS_WorkOrder.BulkUpdateWo(inputupdate);
        
              
       
            
        result = WS_WorkOrder.updateTechCloseWorkOrders(wOIds,'CA_SAP');
        wOIds.add(techClosedWorkOrder.id);
        result = WS_WorkOrder.updateTechCloseWorkOrders(wOIds,'CA_SAP');
            

        
        
        
        
        
        WS_WorkOrder.BulkUpdateWorkDetailResult BUWDR = new WS_WorkOrder.BulkUpdateWorkDetailResult();
        BUWDR.success = true;
        BUWDR.errorMessage = 'toto';

      
        WS_WorkOrder.getWOItemResult gwr = new WS_WorkOrder.getWOItemResult();
        gwr.ServiceOrderNumberResult = 'test';
        gwr.workOrderIdResult = workOrder.id;
        gwr.workOrderNumberResult = '111111';
        gwr.serviceCenterNameResult = 'teritory';
        
        WS_WorkOrder.mandatoryFieldError('123',workOrder.Name,'field');
        WS_WorkOrder.GenericError( '123', 'test' ,workOrder.id, 'error message', 'created');
        WS_WorkOrder.GenericError( '123', 'test' ,workOrder.id, 'error message', 'closed');
         WS_WorkOrder.GenericError( '123', 'test' ,workOrder.id, 'error message', 'updated');
        
           

        
        list<id> idclosed = new list <id>();
        idclosed.add(workOrder.id);
        idclosed.add(closedWorkOrder.id);
        idclosed.add(techClosedWorkOrder.id);
        WS_WorkOrder.CloseWorkOrderResult resultclosed = new WS_WorkOrder.CloseWorkOrderResult();
        WS_WorkOrder.BulkUpdateWorkDetailResult resultupd = new WS_WorkOrder.BulkUpdateWorkDetailResult();
        
        //commented on 17 oct 2013
        resultupd = WS_WorkOrder.bulkUpdateWorkDetails(idclosed,'CA_SAP');
        resultclosed = WS_WorkOrder.bulkCloseWorkOrders(idclosed,'CA_SAP');
        boolean valid;
        valid = WS_WorkOrder.isValidId('sFSQFQSF');
        valid = WS_WorkOrder.isValidId(workOrder.id);
        
        list<string> bor = new list <string>();
        bor.add('borNull');
        bor.add('XXX');
        list<WS_WorkOrder.bfoIDResults> idsres = new list<WS_WorkOrder.bfoIDResults>();
        idsres = WS_WorkOrder.getBFOIds(bor, 'CA_SAP');
        
        test.stopTest();
        
    }
    
    }
    
         static testMethod void myUnitTest4() {
        
           User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassTriggers__c = 'AP_WorkOrderTechnicianNotification',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
        system.runAs(user){   
        Test.startTest();
        Country__c testCountryUS =[select id,name from Country__c where CountryCode__c=:'US'];
        Account acc=[select id,name,SEAccountID__c from account where name=:'TestAccountUSNJ'];
          
        SVMXC__Territory__c territory = new  SVMXC__Territory__c(Name = 'territory 1', SVMXC__Territory_Code__c = '1');
        insert territory;
     
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'Test Location';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = acc.id;
        site.PrimaryLocation__c = true;
        insert site;
          SVMXC__Service_Order__c oneWorkOrderToClose =  Utils_TestMethods.createWorkOrder(acc.id);
         oneWorkOrderToClose.SVMXC__Order_Status__c = 'UnScheduled';
          oneWorkOrderToClose.SVMXC__Site__c = site.Id;
         oneWorkOrderToClose.SendAlertNotification__c = true;
         oneWorkOrderToClose.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         oneWorkOrderToClose.BackOfficeReference__c = 'XXX';
         oneWorkOrderToClose.SVMXC__Primary_Territory__c = territory.id;
         oneWorkOrderToClose.BackOfficeSystem__c = 'CA_SAP';
         oneWorkOrderToClose.TECHAccountDetail__c = acc.id;
         oneWorkOrderToClose.TECHPMsesaId__c = 'techid';
         oneWorkOrderToClose.CustomerTimeZone__c='Pacific/Kiritimati';
         insert oneWorkOrderToClose;
         
         try{
         WS_WorkOrder.getTechCloseWorkOrders('BOffSys1','query');
          }catch (exception e){}
         try{
         WS_WorkOrder.getTechCloseWorkOrders(null,'query');
         }catch (exception e){}
         

            
            
        }
    
    }
    
     static testMethod void myUnitTest5() {
        
           User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassTriggers__c = 'AP_WorkOrderTechnicianNotification',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
        system.runAs(user){   
        Test.startTest();
        Country__c testCountryUS =[select id,name from Country__c where CountryCode__c=:'US'];
        Account acc=[select id,name,SEAccountID__c from account where name=:'TestAccountUSNJ'];
          
        SVMXC__Territory__c territory = new  SVMXC__Territory__c(Name = 'territory 1', SVMXC__Territory_Code__c = '1');
        insert territory;
     
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'Test Location';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = acc.id;
        site.PrimaryLocation__c = true;
        insert site;
          SVMXC__Service_Order__c oneWorkOrderToClose =  Utils_TestMethods.createWorkOrder(acc.id);
         oneWorkOrderToClose.SVMXC__Order_Status__c = 'UnScheduled';
          oneWorkOrderToClose.SVMXC__Site__c = site.Id;
         oneWorkOrderToClose.SendAlertNotification__c = true;
         oneWorkOrderToClose.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         oneWorkOrderToClose.BackOfficeReference__c = 'XXX';
         oneWorkOrderToClose.SVMXC__Primary_Territory__c = territory.id;
         oneWorkOrderToClose.BackOfficeSystem__c = 'CA_SAP';
         oneWorkOrderToClose.TECHAccountDetail__c = acc.id;
         oneWorkOrderToClose.TECHPMsesaId__c = 'techid';
         oneWorkOrderToClose.CustomerTimeZone__c='Pacific/Kiritimati';
         insert oneWorkOrderToClose;
         
         SVMXC__Service_Order_Line__c workDetail = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c= oneWorkOrderToClose.id,SVMXC__End_Date_and_Time__c = date.today());
        insert workDetail;
         
         WS_WorkOrder.bulkUpdateWorkDetails(new list<ID>{workDetail.id},'BOffSys1');
         Test.stoptest();

            
            
        }
    
    }
    
        static testMethod void myUnitTest8() {
        
           User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassTriggers__c = 'AP_WorkOrderTechnicianNotification',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
        system.runAs(user){   
        Test.startTest();
        Country__c testCountryUS =[select id,name from Country__c where CountryCode__c=:'US'];
        Account acc=[select id,name,SEAccountID__c from account where name=:'TestAccountUSNJ'];
          
        SVMXC__Territory__c territory = new  SVMXC__Territory__c(Name = 'territory 1', SVMXC__Territory_Code__c = '1');
        insert territory;
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'Test Location';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = acc.id;
        site.PrimaryLocation__c = true;
        insert site;
          SVMXC__Service_Order__c oneWorkOrderToClose =  Utils_TestMethods.createWorkOrder(acc.id);
         oneWorkOrderToClose.SVMXC__Order_Status__c = 'UnScheduled';
          oneWorkOrderToClose.SVMXC__Site__c = site.Id;
         oneWorkOrderToClose.SendAlertNotification__c = true;
         oneWorkOrderToClose.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         oneWorkOrderToClose.BackOfficeReference__c = 'XXX';
         oneWorkOrderToClose.SVMXC__Primary_Territory__c = territory.id;
         oneWorkOrderToClose.BackOfficeSystem__c = 'CA_SAP';
         oneWorkOrderToClose.TECHAccountDetail__c = acc.id;
         oneWorkOrderToClose.TECHPMsesaId__c = 'techid';
         oneWorkOrderToClose.CustomerTimeZone__c='Pacific/Kiritimati';
         oneWorkOrderToClose.BackOfficeSystem__c =Label.CLDEC12SRV58;
         insert oneWorkOrderToClose;
         WS_WorkOrder.WorkOrder wss= new WS_WorkOrder.WorkOrder();
         wss.SendCustomerConfirmationEmail=true;
         wss.OperationName='test234';
         wss.ReportRequired=true;
         wss.ProjectManager=string.valueof(userinfo.getuserid());
         Test.stoptest();
        }
    }
    
    
        static testMethod void myUnitTest7() {
        
           User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassTriggers__c = 'AP_WorkOrderTechnicianNotification',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
        system.runAs(user){   
        Test.startTest();
        Country__c testCountryUS =[select id,name from Country__c where CountryCode__c=:'US'];
        Account acc=[select id,name,SEAccountID__c from account where name=:'TestAccountUSNJ'];
          
        SVMXC__Territory__c territory = new  SVMXC__Territory__c(Name = 'territory 1', SVMXC__Territory_Code__c = '1');
        insert territory;
     
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'Test Location';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = acc.id;
        site.PrimaryLocation__c = true;
        insert site;
          SVMXC__Service_Order__c oneWorkOrderToClose =  Utils_TestMethods.createWorkOrder(acc.id);
         oneWorkOrderToClose.SVMXC__Order_Status__c = 'UnScheduled';
          oneWorkOrderToClose.SVMXC__Site__c = site.Id;
         oneWorkOrderToClose.SendAlertNotification__c = true;
         oneWorkOrderToClose.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         oneWorkOrderToClose.BackOfficeReference__c = 'XXX';
         oneWorkOrderToClose.SVMXC__Primary_Territory__c = territory.id;
         oneWorkOrderToClose.BackOfficeSystem__c = 'CA_SAP';
         oneWorkOrderToClose.TECHAccountDetail__c = acc.id;
         oneWorkOrderToClose.TECHPMsesaId__c = 'techid';
         oneWorkOrderToClose.CustomerTimeZone__c='Pacific/Kiritimati';
         oneWorkOrderToClose.BackOfficeSystem__c =Label.CLDEC12SRV58;
         insert oneWorkOrderToClose;

            WO_ValueChainPlayer__c vcp1= new WO_ValueChainPlayer__c();
            vcp1.WorkOrder__c = oneWorkOrderToClose.id;
            vcp1.User__c=userinfo.getuserid();
            insert vcp1;
            try{
         WS_WorkOrder.getListOfWOVCPToUpdate(new list<ID>{oneWorkOrderToClose.id});
         }catch (exception e){}
         Test.stoptest();
        }
    }
    
    static testMethod void myUnitTest6() {
        
           User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassTriggers__c = 'AP_WorkOrderTechnicianNotification',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
        system.runAs(user){   
        Test.startTest();
        Country__c testCountryUS =[select id,name from Country__c where CountryCode__c=:'US'];
        Account acc=[select id,name,SEAccountID__c from account where name=:'TestAccountUSNJ'];
          
        SVMXC__Territory__c territory = new  SVMXC__Territory__c(Name = 'territory 1', SVMXC__Territory_Code__c = '1');
        insert territory;
     
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'Test Location';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = acc.id;
        site.PrimaryLocation__c = true;
        insert site;
          SVMXC__Service_Order__c oneWorkOrderToClose =  Utils_TestMethods.createWorkOrder(acc.id);
         oneWorkOrderToClose.SVMXC__Order_Status__c = 'UnScheduled';
          oneWorkOrderToClose.SVMXC__Site__c = site.Id;
         oneWorkOrderToClose.SendAlertNotification__c = true;
         oneWorkOrderToClose.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         oneWorkOrderToClose.BackOfficeReference__c = 'XXX';
         oneWorkOrderToClose.SVMXC__Primary_Territory__c = territory.id;
         oneWorkOrderToClose.BackOfficeSystem__c = 'CA_SAP';
         oneWorkOrderToClose.TECHAccountDetail__c = acc.id;
         oneWorkOrderToClose.TECHPMsesaId__c = 'techid';
         oneWorkOrderToClose.CustomerTimeZone__c='Pacific/Kiritimati';
         oneWorkOrderToClose.BackOfficeSystem__c =Label.CLDEC12SRV58;
         insert oneWorkOrderToClose;

            WO_ValueChainPlayer__c vcp1= new WO_ValueChainPlayer__c();
            vcp1.WorkOrder__c = oneWorkOrderToClose.id;
            vcp1.AccountDetails__c='text123';
            vcp1.Account__c = acc.id;
            insert vcp1;
            try{
         WS_WorkOrder.getListOfWOVCPToUpdate(new list<ID>{oneWorkOrderToClose.id});
         }catch (exception e){}
         Test.stoptest();
        }
    }
    static testMethod void myUnitTest3() {
        
           User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = system.label.CLAPR15PRM307, BypassWF__c = true,BypassTriggers__c = 'AP_WorkOrderTechnicianNotification',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        insert user;
        system.runAs(user){   
        Test.startTest();
        Country__c testCountryUS =[select id,name from Country__c where CountryCode__c=:'US'];
        Account acc=[select id,name,SEAccountID__c from account where name=:'TestAccountUSNJ'];
          
        SVMXC__Territory__c territory = new  SVMXC__Territory__c(Name = 'territory 1', SVMXC__Territory_Code__c = '1');
        insert territory;
     
        
        SVMXC__Site__c site = new SVMXC__Site__c();
        site.Name = 'Test Location';
        site.SVMXC__Street__c  = 'Test Street';
        site.SVMXC__Account__c = acc.id;
        site.PrimaryLocation__c = true;
        insert site;
          SVMXC__Service_Order__c oneWorkOrderToClose =  Utils_TestMethods.createWorkOrder(acc.id);
         oneWorkOrderToClose.SVMXC__Order_Status__c = 'UnScheduled';
          oneWorkOrderToClose.SVMXC__Site__c = site.Id;
         oneWorkOrderToClose.SendAlertNotification__c = true;
         oneWorkOrderToClose.SVMXC__Scheduled_Date_Time__c = Datetime.now();
         oneWorkOrderToClose.BackOfficeReference__c = 'XXX';
         oneWorkOrderToClose.SVMXC__Primary_Territory__c = territory.id;
         oneWorkOrderToClose.BackOfficeSystem__c = 'CA_SAP';
         oneWorkOrderToClose.TECHAccountDetail__c = acc.id;
         oneWorkOrderToClose.TECHPMsesaId__c = 'techid';
         oneWorkOrderToClose.CustomerTimeZone__c='Pacific/Kiritimati';
         insert oneWorkOrderToClose;
         
          WO_ValueChainPlayer__c vcp1= new WO_ValueChainPlayer__c();
            vcp1.WorkOrder__c = oneWorkOrderToClose.id;
            vcp1.Account__c = acc.id;
            insert vcp1;
            try{
            WS_WorkOrder.getListOfWOVCPToCreate(new list<SVMXC__Service_Order__c>{oneWorkOrderToClose});
            }catch (exception e){}
            Test.stoptest();
            
            WS_WorkOrder.CreateWorkOrderResult res= new WS_WorkOrder.CreateWorkOrderResult();
            res.errorMessage='testanand';
            WS_WorkOrder.UpdateWorkDetailResult rres= new WS_WorkOrder.UpdateWorkDetailResult();
            rres.errorMessage='testaa';
            WS_WorkOrder.WorkOrderResult rwed= new WS_WorkOrder.WorkOrderResult();
            rwed.workOrderId=oneWorkOrderToClose.id;
            
            
        }
    
    }
    
    static testMethod void myUnitTest2() {
     
        /*Account acc = Utils_TestMethods.createAccount();
        acc.SEAccountID__c = '1111';
        insert acc;*/
        Account acc=[select id,name,SEAccountID__c from account where name=:'TestAccountUSNJ'];
        Contact con = Utils_TestMethods.createContact(acc.id, 'Test Contact');
        insert con;
        // Positive Case
        WS_WorkOrder.PartOrder wpowithoutpoline = new WS_WorkOrder.PartOrder();
        wpowithoutpoline.Accountbfoid = acc.id;
        wpowithoutpoline.BackOfficeReference = 'TestBoffRef';
        wpowithoutpoline.Contactbfoid = con.id;
        wpowithoutpoline.OperationBONumber = 'TestOperationBoNumaber1';
        wpowithoutpoline.OrderStatus = 'OrderStatus';
        wpowithoutpoline.ShipmentType = 'ShipmentType';
        List<WS_WorkOrder.PartOrderResult>  bcporderplinews1 = WS_WorkOrder.bulkCreatePartOrder_PartItems(new List<WS_WorkOrder.PartOrder>{wpowithoutpoline},'Test Backoff System');
        WS_WorkOrder.PartOrderLine porl1 = new WS_WorkOrder.PartOrderLine();
        porl1.CommercialReference = 'CommercialReference 1';
        porl1.ExpectedQty = 10;
        porl1.RequestedDate = Date.today();
        porl1.ShipmentType = 'ShipmentType';
        porl1.UnitofMeasure = 'KG';
        WS_WorkOrder.PartOrderLine porl2 = new WS_WorkOrder.PartOrderLine();
        porl2.CommercialReference = 'CommercialReference 2';
        porl2.ExpectedQty = 10;
        porl2.RequestedDate = Date.today();
        porl2.ShipmentType = 'ShipmentType';
        porl2.UnitofMeasure = 'KG'; 
            
        WS_WorkOrder.PartOrder wpowithpoline = new WS_WorkOrder.PartOrder();
        wpowithpoline.Accountbfoid = acc.id;
        wpowithpoline.BackOfficeReference = 'TestBoffRef';
        wpowithpoline.Contactbfoid = con.id;
        wpowithpoline.OperationBONumber = 'TestOperationBoNumaber2';
        wpowithpoline.OrderStatus = 'OrderStatus';
        wpowithpoline.ShipmentType = 'ShipmentType';
        wpowithpoline.PartOrderLine = new List<WS_WorkOrder.PartOrderLine>{porl1,porl2};
        List<WS_WorkOrder.PartOrderResult>  bcporderplinews2 = WS_WorkOrder.bulkCreatePartOrder_PartItems(new List<WS_WorkOrder.PartOrder>{wpowithpoline},'Test Backoff System');
        
        // Mandatroy Fields Check
        wpowithpoline.Accountbfoid = null;
        wpowithpoline.BackOfficeReference = null;
        wpowithpoline.Contactbfoid = null;
        wpowithpoline.OperationBONumber = null;
        wpowithpoline.OrderStatus = null;
        wpowithpoline.ShipmentType = null;
        wpowithpoline.PartOrderLine = new List<WS_WorkOrder.PartOrderLine>{porl1};
        
        porl2.CommercialReference =  null;
        porl2.ExpectedQty =  null;
        porl2.RequestedDate =  null;
        porl2.ShipmentType =  null;
        porl2.UnitofMeasure =  null;
        
        wpowithoutpoline.PartOrderLine =  new List<WS_WorkOrder.PartOrderLine>{porl2};
        
        List<WS_WorkOrder.PartOrderResult>  bcporderplinews3 = WS_WorkOrder.bulkCreatePartOrder_PartItems(new List<WS_WorkOrder.PartOrder>{wpowithpoline,wpowithoutpoline},'Test Backoff System1');
        
        wpowithoutpoline.Accountbfoid = acc.id;
        wpowithoutpoline.BackOfficeReference = 'TestBoffRef';
        wpowithoutpoline.Contactbfoid = con.id;
        wpowithoutpoline.OperationBONumber = 'TestOperationBoNumaber111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111';
        wpowithoutpoline.OrderStatus = 'OrderStatus';
        wpowithoutpoline.ShipmentType = 'ShipmentType';
        
        porl2.CommercialReference = 'CommercialReference 21111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111';
        porl2.ExpectedQty = 10;
        porl2.RequestedDate = Date.today();
        porl2.ShipmentType = 'ShipmentType';
        porl2.UnitofMeasure = 'KG'; 
        
        wpowithpoline.Accountbfoid = acc.id;
        wpowithpoline.BackOfficeReference = 'TestBoffRef';
        wpowithpoline.Contactbfoid = con.id;
        wpowithpoline.OperationBONumber = 'TestOperationBoNumaber2';
        wpowithpoline.OrderStatus = 'OrderStatus';
        wpowithpoline.ShipmentType = 'ShipmentType';
        wpowithpoline.PartOrderLine = new List<WS_WorkOrder.PartOrderLine>{porl1,porl2};
        
        List<WS_WorkOrder.PartOrderResult>  bcporderplinews4 = WS_WorkOrder.bulkCreatePartOrder_PartItems(new List<WS_WorkOrder.PartOrder>{wpowithpoline,wpowithoutpoline},'Test Backoff System2');
         
        WorkOrderGroup__c wg = new WorkOrderGroup__c();
        wg.BackOfficeSystem__c ='BOffSys1';
        wg.ServiceOrderTechnicallyClosed__c =false;
        wg.Status__c = Label.CLOCT13SRV33;
        insert wg;
        WorkOrderGroup__c wg2 = new WorkOrderGroup__c();
        wg2.BackOfficeSystem__c ='BOffSys1';
        wg2.ServiceOrderTechnicallyClosed__c =false;
        wg2.Status__c = 'New';
        insert wg2;
        
        SVMXC__Service_Order__c workOrder =  Utils_TestMethods.createWorkOrder(acc.id);         
         workOrder.Work_Order_Category__c='On-site';             
         workOrder.SendAlertNotification__c = true;
         workOrder.SVMXC__Scheduled_Date_Time__c = Datetime.now();      
         insert workOrder;
         SVMXC__RMA_Shipment_Order__c rma= new SVMXC__RMA_Shipment_Order__c(SVMXC__Service_Order__c=workOrder.id);
         try{
         insert rma;
         }catch (exception e){}
        
        SVMXC__RMA_Shipment_Line__c poline= new SVMXC__RMA_Shipment_Line__c(SVMXC__RMA_Shipment_Order__c=rma.id);
        try{
         insert poline;
         }catch (exception e){}
        
        
        WS_WorkOrder.PartOrderLine pos= new WS_WorkOrder.PartOrderLine();
        WS_WorkOrder.PartOrder po= new WS_WorkOrder.PartOrder();
        //list<WS_WorkOrder.PartOrderLine> polos= new list<WS_WorkOrder.PartOrderLine>();
        //polos.add(pos);
        //po.PartOrderLine =polos;
        WS_WorkOrder.PartOrderLineResult por= new WS_WorkOrder.PartOrderLineResult();
        por.success=true;
        por.type='test';
        por.errorMessage='test123';
        WS_WorkOrder.coveragemethod();
        WS_WorkOrder.bulkUpdateSOReleasedWorkOrderGroups(new List<id>{wg.id,acc.id},'BOffSys1');
        WS_WorkOrder.bulkGetTechCloseWorkOrderGroups('BOffSys1');
        WS_WorkOrder.bulkUpdateTechCloseWorkOrderGroups(new List<id>{wg.id,wg2.id},'BOffSys1');
        WS_WorkOrder.bulkUpdateWorkDetails(new List<id>{wg.id,wg2.id},'BOffSys1');
        WS_WorkOrder.getBFOIds(new List<string>{workOrder.name},'BOffSys1');
        WS_WorkOrder.ServiceContractMap(new set<string>{'BOffSys1'},new set<string>{'BOffSys1'});
        WS_WorkOrder.IncludedServiceMap(new set<string>{'BOffSys1'},new set<string>{'BOffSys1'},new set<string>{'BOffSys1'});
        try{
        WS_WorkOrder.bulkCreatePartsOrderLine(new list<WS_WorkOrder.PartOrder>{po},'BOffSys1');
        }catch (exception e){}
        try{
        WS_WorkOrder.getCreatedPartsOrders(new set<id>{rma.id});
        WS_WorkOrder.getCreatedPartsOrderLines(new set<id>{poline.id});
        WS_WorkOrder.getPartsOrderLines(new list<WS_WorkOrder.PartOrderLine>{pos},rma.id,'BOffSys1');
        }catch (exception e){}
        
        
    }
}