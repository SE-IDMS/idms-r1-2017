global class WS_AOS_Work_Order{


    global class WorkOrder{
        webservice date CustomerRequestDate;
        webservice String Priority;
        webservice String WorkOrderReason;
        webservice String CommentsToPlanner;
        webservice String Status;
        webservice String Category;
        webservice String Type;
        webservice String SubType; 
        webservice String IsBillable;
        webservice String InstalledAtAccount;
        webservice String Location;
        webservice String ServiceLine;
        webservice String ContractHeader;
        webservice String ServiceBU;
        webservice String workOrderNumber;
        webservice InstalledProduct InstalledProduct;
        webservice Contact Contact;
        webservice String RemoteSystemWorkOrderID;
       

    }
    global class InstalledProduct{
     
        webservice String GoldenID;
       
     }
     global class Contact{
        
       
        webservice String FirstName;
        webservice String LastName;
        webservice String Email;
        webservice String Phone;
        //webservice String Address;
        //webservice Id Id;
        //webservice String contactInformation;
       
        
     }
    global class WorkOrderResult{
        webservice Boolean success;
        webservice String type;
        webservice String errorMessage;
        webservice Id workOrderbFOID;
        webservice String workOrderNumber;       
        webservice Id workOrderGroupbFOID;
       
    }
    
    // Cordys will call CreatAOSWorkOrders to create WorkOrder in BFO
    webservice static  List<WorkOrderResult> CreatAOSWorkOrders(List<WorkOrder> request){
        
        return WS_AOS_Work_Order_Handler.CreateServiceOrder(request);
        
    }
    
    
}