@isTest

private class FieloPRM_REST_UpProgLevelOnContactsTest{

    static testMethod void unitTest(){
        

        
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
        
            FieloEE.MockUpFactory.setCustomProperties(false);

        
            FieloEE__Member__c member = new FieloEE__Member__c();
            member.FieloEE__LastName__c= 'TestLaastNo';
            member.FieloEE__FirstName__c = 'TestFiirstNo';
            member.FieloEE__Street__c = 'test2o';
                 
            insert member;
        
            FieloPRM_MemberUpdateHistory__c history = new FieloPRM_MemberUpdateHistory__c ();
            history.F_PRM_Type__c = 'Program Level';
            history.F_PRM_Member__c = member.id;
            insert history;
        }
        
        List<String> method1 = FieloPRM_UTILS_BadgeMember.getUpdatedProgramLevelOnContacts('19900202020202020','19900202020202020');
        
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.addParameter('fromTime', '19900202020202020');
        req.addParameter('toTime', '19900202020220202');
        
        req.requestURI = '/services/apexrest/RestUpdatedProgramLevelOnContacts';  
        req.httpMethod = 'GET';//HTTP Request Type
        RestContext.request = req;
        RestContext.response= res;
        
        FieloPRM_REST_UpProgLevelOnContacts callClass = new FieloPRM_REST_UpProgLevelOnContacts();
        FieloPRM_REST_UpProgLevelOnContacts.getUpdatedProgramLevelOnContacts();
        
    }

    
}