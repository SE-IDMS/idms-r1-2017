@isTest
 Private Class VFC_CustomerPortalClsCaseView_TEST{
     
     static testMethod void closeCaseView(){
        Account accounts = Utils_TestMethods.createAccount();
        insert accounts;
             
        contact con=Utils_TestMethods.createContact(accounts.Id,'testContact');
        Database.insert(con);     
    
        Case caseobj=Utils_TestMethods.createCase(accounts.id,con.id,'closed');
        Database.insert(caseobj);
     
        ApexPages.currentPage().getParameters().Put('Id',caseobj.id);
        VFC_CustomerPortalClsCaseView caseViewObj = new VFC_CustomerPortalClsCaseView();
        caseViewObj.getCaseDetail();
        caseViewObj.getCaseComments();
        caseViewObj.getCaseDetailView();
        caseViewObj.getAttachments();
        caseViewObj.getAccountName();
        caseViewObj.getContactPhone();
        caseViewObj.setAccountName('Test Account');
        caseViewObj.setContactPhone('9389383839');
        caseViewObj.getIsAuthenticated();
        caseViewObj.getUserContact();
        caseViewObj.getUserr();
        caseViewObj.getUserAcc();
        caseViewObj.getCaseId();   
     }
 
 
 }