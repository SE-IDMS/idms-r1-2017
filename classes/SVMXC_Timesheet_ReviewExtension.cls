public class SVMXC_Timesheet_ReviewExtension {
    /*
            Visualforce Extension to consolidate time entries into a single table like page for online manager review.
            (Written March 23, 2015)
    */
    
    public String fullURL {get; set;}
    public String userLink {get; set;}
    public String mgrfullURL {get; set;}
    public String mgrLink {get; set;}
    
    private final SVMXC_Timesheet__c ts;
        
    public SVMXC_Timesheet__c currentTS {get; set;}

    public ApexPages.standardController controller {get; set;}
            
    public SVMXC_Timesheet_ReviewExtension(ApexPages.StandardController stdcontroller) {
        controller = stdcontroller;
        this.ts = (SVMXC_Timesheet__c)controller.getRecord();
    }
    
    public String styleSatSun {get; set;}
    public String styleSatSunHeader {get; set;}
    public String noteNum2 {get; set;}
    
    public timeentry totals {get; set;}
    
    public List<timeentry> timeentriesList {get; set;}
    public class timeentry {
        public String Activity {get; set;}
        public String ActivityStyle {get; set;}
        public String ActivityLink {get; set;}
        public String Tooltip {get; set;}
        public String ActivityTooltip {get; set;}
        public Double MonReg {get; set;}      
        public Double TueReg {get; set;}      
        public Double WedReg {get; set;}     
        public Double ThuReg {get; set;}   
        public Double FriReg {get; set;}    
        public Double SatReg {get; set;}
        public Double SunReg {get; set;}
        public Double HorizontalTotal {get; set;}
    }
    
    private List<timeentry> BuildTimeEntries (List<SVMXC_Time_Entry__c> incomingTEs) {
    
        List<timeentry> timeEntries = new List<timeentry>(); 
        
        Map<Id, timeentry> workingMap = new Map<Id, timeentry>();
        
        for (SVMXC_Time_Entry__c incomingTE: incomingTEs) {
    
            // build timeentry class based on incoming Time Entry record
            timeentry te = new timeentry();
            te.HorizontalTotal = 0;
            
            // check if this is related to a WO already processed
            if (incomingTE.Work_Details__r.SVMXC__Service_Order__c != null && workingMap.containsKey(incomingTE.Work_Details__r.SVMXC__Service_Order__c)) {
                System.debug('Existing found and used');
                te = workingMap.get(incomingTE.Work_Details__r.SVMXC__Service_Order__c);        
            }
            
            if (incomingTE.Work_Details__r.SVMXC__Service_Order__c != null) {
                
                te.Activity = incomingTE.Work_Details__r.SVMXC__Service_Order__r.Name;               
                
                String tooltip = '';
                tooltip = 'Work Order # : ' + incomingTE.Work_Order_Number__c;
                tooltip = tooltip + ' Account : ' + incomingTE.Work_Details__r.SVMXC__Service_Order__r.SVMXC__Company__r.Name;
                
                te.Tooltip = tooltip;                   
                te.ActivityLink = '/' + incomingTE.Work_Details__r.SVMXC__Service_Order__c;
                
                if (incomingTE.Work_Details__r.SVMXC__Service_Order__r.SVMXC__Order_Status__c == 'Closed')                   
                    te.ActivityStyle = 'color:blue;font-size:14px;';
                else
                    te.ActivityStyle = 'color:green;font-size:14px;';
            } else {
                // unrelated to Work Order
                te.Activity = incomingTE.Activity__c;
                te.ActivityLink = '/' + incomingTE.Id;
                te.ActivityStyle = 'color:black;font-size:14px;';
                
                String tooltipActivity = '';
                
                tooltipActivity = 'Start Date : ' + incomingTE.Start_Date_Time__c.format('MMMM d,  yyyy hh:mm:ss a');
                tooltipActivity = tooltipActivity + ' End Date : ' + incomingTE.End_Date_Time__c.format('MMMM d,  yyyy hh:mm:ss a');
                te.Tooltip = tooltipActivity;
            }
            
            if (incomingTE.Comments__c != null)
                te.ActivityTooltip = incomingTE.Comments__c;
                
            if (incomingTE.Day_of_Week__c != null && incomingTE.Day_of_Week__c == 'Monday' && incomingTE.Total_Time__c != null) {
                
                if (te.MonReg == null)
                    te.MonReg = incomingTE.Total_Time__c;
                else
                    te.MonReg = te.MonReg + incomingTE.Total_Time__c;
                    
                te.HorizontalTotal = te.HorizontalTotal + incomingTE.Total_Time__c;
               
                if (totals.MonReg == null)
                    totals.MonReg = incomingTE.Total_Time__c;
                else
                    totals.MonReg = totals.MonReg + incomingTE.Total_Time__c;
            }  
            
            if (incomingTE.Day_of_Week__c != null && incomingTE.Day_of_Week__c == 'Tuesday' && incomingTE.Total_Time__c != null) {
                
                 if (te.TueReg == null)
                    te.TueReg = incomingTE.Total_Time__c;
                else
                    te.TueReg = te.TueReg + incomingTE.Total_Time__c;
                    
                te.HorizontalTotal = te.HorizontalTotal + incomingTE.Total_Time__c;
               
                if (totals.TueReg == null)
                    totals.TueReg = incomingTE.Total_Time__c;
                else
                    totals.TueReg = totals.TueReg + incomingTE.Total_Time__c;
            } 
            
            if (incomingTE.Day_of_Week__c != null && incomingTE.Day_of_Week__c == 'Wednesday' && incomingTE.Total_Time__c != null) {
                
                if (te.WedReg == null)
                    te.WedReg = incomingTE.Total_Time__c;
                else
                    te.WedReg = te.WedReg + incomingTE.Total_Time__c;
                
                te.HorizontalTotal = te.HorizontalTotal + incomingTE.Total_Time__c;
               
                if (totals.WedReg == null)
                    totals.WedReg = incomingTE.Total_Time__c;
                else
                    totals.WedReg = totals.WedReg + incomingTE.Total_Time__c;
            }  
            
             if (incomingTE.Day_of_Week__c != null && incomingTE.Day_of_Week__c == 'Thursday' && incomingTE.Total_Time__c != null) {
                
                if (te.ThuReg == null)
                    te.ThuReg = incomingTE.Total_Time__c;
                else
                    te.ThuReg = te.ThuReg + incomingTE.Total_Time__c;
                
                te.HorizontalTotal = te.HorizontalTotal + incomingTE.Total_Time__c;
               
                if (totals.ThuReg == null)
                    totals.ThuReg = incomingTE.Total_Time__c;
                else
                    totals.ThuReg = totals.ThuReg + incomingTE.Total_Time__c;
            }  
            
             if (incomingTE.Day_of_Week__c != null && incomingTE.Day_of_Week__c == 'Friday' && incomingTE.Total_Time__c != null) {
                
                if (te.FriReg == null)
                    te.FriReg = incomingTE.Total_Time__c;
                else
                    te.FriReg = te.FriReg + incomingTE.Total_Time__c;
                
                te.HorizontalTotal = te.HorizontalTotal + incomingTE.Total_Time__c;
               
                if (totals.FriReg == null)
                    totals.FriReg = incomingTE.Total_Time__c;
                else
                    totals.FriReg = totals.FriReg + incomingTE.Total_Time__c;
            }  
            
            if (incomingTE.Day_of_Week__c != null && incomingTE.Day_of_Week__c == 'Saturday' && incomingTE.Total_Time__c != null) {
                
                styleSatSun = '';
                styleSatSunHeader = 'text-align:center';
                
                if (te.SatReg == null)
                    te.SatReg = incomingTE.Total_Time__c;
                else
                    te.SatReg = te.SatReg + incomingTE.Total_Time__c;
                
                te.HorizontalTotal = te.HorizontalTotal + incomingTE.Total_Time__c;
               
                if (totals.SatReg == null)
                    totals.SatReg = incomingTE.Total_Time__c;
                else
                    totals.SatReg = totals.SatReg + incomingTE.Total_Time__c;
            }  
            
            if (incomingTE.Day_of_Week__c != null && incomingTE.Day_of_Week__c == 'Sunday' && incomingTE.Total_Time__c != null) {
                
                styleSatSun = '';
                styleSatSunHeader = 'text-align:center';
                
                if (te.SunReg == null)
                    te.SunReg = incomingTE.Total_Time__c;
                else
                    te.SunReg = te.SunReg + incomingTE.Total_Time__c;
                
                te.HorizontalTotal = te.HorizontalTotal + incomingTE.Total_Time__c;
               
                if (totals.SunReg == null)
                    totals.SunReg = incomingTE.Total_Time__c;
                else
                    totals.SunReg = totals.SunReg + incomingTE.Total_Time__c;
            }  
            
            if (incomingTE.Work_Details__c == null) {
                
                // time entry is not related to a work detail and will remain on its own display line for report
                workingMap.put(incomingTE.Id, te);
                
            } else {
                
                // time entry is related to a work detail, this will need to be consolidated with others related to the same WO (if they exist) for display
                workingMap.put(incomingTE.Work_Details__r.SVMXC__Service_Order__c, te);
            }       
        }
        
        // extract all time entries from Map and put into list  
        for (timeentry t : workingMap.values()) {
            timeEntries.add(t);
        }
        
        return timeEntries; 
    }
    
    public PageReference gotoTimesheet () {
        
        PageReference scPage = new PageReference('/' + ts.Id);
        scPage.setRedirect(true);
        return scPage; 
    }
    
    public PageReference gotoApprovalPage () {
        
        PageReference scPage = new PageReference('/apex/SVMXC_Approvals');
        scPage.setRedirect(true);
        return scPage; 
    }
    
    public PageReference init() {
        
        currentTS = [SELECT Utilization__c, Technician__r.Technician_Manager__c, Technician__r.SVMXC__Salesforce_User__c, Submission_Notes__c,
                        Technician__r.Name, Technician__c, Start_Date__c, Region__c, Name, End_Date__c, Status__c, Total_Hours__c, Total_Billable_Hours__c,
                        Total_Nonbillable_Hours__c
                        FROM SVMXC_Timesheet__c WHERE Id =: ts.Id LIMIT 1];         
        
        User u = new User();
        
        if (currentTS.Technician__r.SVMXC__Salesforce_User__c != null)
            u = [SELECT ManagerId, FullPhotoURL FROM User WHERE Id =: currentTS.Technician__r.SVMXC__Salesforce_User__c LIMIT 1];
        else 
            u = [SELECT FullPhotoURL, ManagerId FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
            
        userLink = '/' + u.Id;  
            
        // pull Manager picture
        User u2 = new User();
        if (u.ManagerId != null) {
            u2 = [SELECT FullPhotoURL FROM USER WHERE Id =: u.ManagerId LIMIT 1];
            mgrfullURL = u2.FullPhotoUrl;
            mgrLink = '/' + u2.Id;
        }
        
        fullURL = u.FullPhotoURL;
        
        // setup time entries       
        List<SVMXC_Time_Entry__c> TEs = [SELECT Id, 
                                            Activity__c, Work_Order_Number__c, Work_Details__c, Work_Details__r.SVMXC__Service_Order__r.SVMXC__Order_Status__c, 
                                            Work_Details__r.SVMXC__Service_Order__r.SVMXC__Company__r.Name, Comments__c, Total_Time__c, Start_Date_Time__c, End_Date_Time__c,
                                            Work_Details__r.SVMXC__Service_Order__r.SVMXC__Order_Type__c, Day_of_Week__c, Work_Details__r.SVMXC__Service_Order__r.Name
                                            FROM SVMXC_Time_Entry__c WHERE Timesheet__c =: ts.Id ORDER BY Work_Details__c DESC];

        System.debug('####### Number of Time Entries : ' + TEs.size());

        totals = new timeentry();
        totals.Activity = 'Hour Totals';

        styleSatSun = 'display:none !important;';
        styleSatSunHeader = 'display:none !important;';

        timeentriesList = new List <timeentry>();
        timeentriesList = BuildTimeEntries(TEs);
        
        if (styleSatSun == 'display:none !important;')
            noteNum2 = '** Saturday/Sunday columns are not shown as there is no data for this timesheet for those weekend days.';
        
        return null;
    }
    
}