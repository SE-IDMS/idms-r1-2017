@isTest
private class VFC85_NewSupportRequest_TEST 
{
    static testMethod void VFC85_NewSupportRequest() 
    {
        Account account1 = Utils_TestMethods.createAccount();
        insert account1;
            
        Opportunity opp = Utils_TestMethods.createOpenOpportunity(account1.id);
        insert opp;         
        
        OPP_SellingCenter__c sc=new OPP_SellingCenter__c(Name='sctest',Email__c='abc@xyz.com');
        insert sc;
        
        OPP_SupportRequest__c newSuppReq = new OPP_SupportRequest__c(
                                                                    Opportunity__c = opp.id,
                                                                    Comments__c='comments',
                                                                    SupportRequestedBy__c=system.today(),
                                                                    SupportType__c='Solutions and Quotations Support Requests',
                                                                    RecordTypeId=Label.CL10092,
                                                                    SellingCenter__c=sc.id
                                                                    )   ;
        PageReference pageRef = Page.VFP85_NewSupportRequest;
        Test.setCurrentPage(pageRef);                                                            
        ApexPages.currentPage().getParameters().put('oppid', opp.id);
        ApexPages.currentPage().getParameters().put('scid', sc.id);
        ApexPages.currentPage().getParameters().put('RecordType', Label.CL10092);
 
        ApexPages.StandardController con = new ApexPages.StandardController(newSuppReq);
        VFC85_NewSupportRequest ext = new VFC85_NewSupportRequest(con);
        
        ext.getSolCenName();
        ext.getRerenderCenterTypeDisplay();
        ext.Save();
        ext.Cancel();
        //delete newSuppReq;
        
        OPP_SupportRequest__c newSuppReq1 = new OPP_SupportRequest__c(
                                                                    Opportunity__c = opp.id,
                                                                    Comments__c='comments',
                                                                    SupportRequestedBy__c=system.today(),
                                                                    SupportType__c='Solutions and Quotations Support Requests',
                                                                    RecordTypeId=Label.CL10092,
                                                                    SellingCenter__c=sc.id
                                                                    )   ;
       
         PageReference pageRef2 = Page.VFP85_NewSupportRequest;
        Test.setCurrentPage(pageRef2);                                                            
        ApexPages.currentPage().getParameters().put('oppid', opp.id);
        ApexPages.currentPage().getParameters().put('scid', sc.id);
        ApexPages.currentPage().getParameters().put('RecordType', Label.CL10092);
        ApexPages.StandardController con1 = new ApexPages.StandardController(newSuppReq1);
        VFC85_NewSupportRequest ext1 = new VFC85_NewSupportRequest(con1);
        ext1.SaveAndNew();
        
        
         PageReference pageRef3= Page.VFP85_NewSupportRequest;
        Test.setCurrentPage(pageRef3);                                                            
        ApexPages.currentPage().getParameters().put('oppid', opp.id);
        ApexPages.currentPage().getParameters().put('scid', sc.id);
        ApexPages.currentPage().getParameters().put('RecordType', Label.CL10093);
        ApexPages.StandardController con2 = new ApexPages.StandardController(newSuppReq1);
        VFC85_NewSupportRequest ext2 = new VFC85_NewSupportRequest(con2);
        ext2.getSolCenName();
        ext2.getRerenderCenterTypeDisplay();
        ext2.Save();
        //ext.Cancel();
        
        
        

        
    }
}