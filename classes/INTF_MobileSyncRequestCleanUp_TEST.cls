@isTest
private class INTF_MobileSyncRequestCleanUp_TEST 
{
    static testMethod void mobilesynccleanup()
    {
    SVMXC__Sync_Request__c syncReq = new SVMXC__Sync_Request__c();
        syncReq.SVMXC__Client_Request_Id__c = 'ABCDEFGHIJKL MNOPQRS TUVWXYZ';
        insert  syncReq;
        
        Test.startTest();
         INTF_MobileSyncRequestCleanUp bac = new INTF_MobileSyncRequestCleanUp();
         bac.execute( (Database.BatchableContext) null, new List<SVMXC__Sync_Request__c>{ syncReq } );         
         INTF_MobileSyncRequestCleanUp SRVbatch= new INTF_MobileSyncRequestCleanUp();
         ID batchSRVid=Database.executeBatch(SRVbatch);        
         string CORN_EXP = '0 30 0 1 4 ?';               
         string jobid4 = system.schedule('clean up from test class', CORN_EXP, new INTF_MobileSyncRequestCleanUp());
         CronTrigger ct4 = [select id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobid4];     
     Test.stopTest();
    }
    
}