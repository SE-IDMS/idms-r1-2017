/**
    Author          : Nitin Khunal
    Date Created    : 14/10/2016
    Description     : Test Class for VFC_PAHeaderTemplate apex class
*/
@isTest
public class VFC_PAHeaderTemplate_TEST {
    @testSetup static void testData() {
        Country__c testCountry = Utils_TestMethods.createCountry();
        insert testCountry;
        Account testAccount = Utils_TestMethods.createAccount(userinfo.getuserid(), testCountry.Id);
        testAccount.SEAccountID__c = '12345678';
        testAccount.City__c = 'Bangalore';
        insert testAccount;
        Contact testcontact = Utils_TestMethods.createContact(testAccount.Id, 'Contact');
        testcontact.SEContactID__c = '1234567';
        testcontact.Email = 'test@test.com';
        insert testcontact;
        
        Contract testContract = Utils_TestMethods.createContract(testAccount.Id, testcontact.Id);
        insert testContract;
        CTR_ValueChainPlayers__c testCVCP = Utils_TestMethods.createCntrctValuChnPlyr(testContract.Id);
        testCVCP.legacypin__c ='1234';
        testCVCP.Contact__c = testcontact.Id;
        insert testCVCP;
    }
    
    @isTest static void testMethod1() {
        Account acc = [select id from Account limit 1];
        WS_PAPortletUserManagement.UserInformation uInfo = new WS_PAPortletUserManagement.UserInformation();
        uInfo.strContactGoldenId='12345'; 
        uInfo.strAccountGoldenId='12345678';
        uInfo.strCustomerFirstId='1234';
        uInfo.strWebUserId='';
        uInfo.strWebUserName='testInv'; 
        WS_PAPortletUserManagement.SiteResult userresult = WS_PAPortletUserManagement.createUser(uInfo);
        WS_PAPortletUserManagement.ManualSharingInformation manualShareInfo = new WS_PAPortletUserManagement.ManualSharingInformation();
        manualShareInfo.strAccountGoldenId = '12345678';
        manualShareInfo.strSharingAccessLevel = 'Edit';
        manualShareInfo.strWebUserName = 'testInv';
        WS_PAPortletUserManagement.SiteResult userresult1 = WS_PAPortletUserManagement.accountManualSharing(manualShareInfo);
        system.runAs(new user(Id = userresult.userId)) {
            VFC_PAHeaderTemplate controller = new VFC_PAHeaderTemplate();
        }
    }
}