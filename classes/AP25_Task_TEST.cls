/*
    Author          : ACN 
    Date Created    : 06/08/2011
    Description     : Test class for AP25_Task
*/
@isTest
private class AP25_Task_TEST 
{
    static testMethod void testTasks() 
    {
        List<Profile> profiles = [select id from profile where name='System Administrator'];
        if(profiles.size()>0)
        {
            User user = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC43');                
            system.runas(user)
            {
                
                MarketingCallBackLimit__c markCallBack = Utils_TestMethods.createMarkCallBack(100,100);
                insert markCallBack;
    
                Country__c country= Utils_TestMethods.createCountry();
                insert country;            
                            
                Lead lead1 = Utils_TestMethods.createLead(null, country.Id,100);
                insert lead1;

                Account acc = Utils_TestMethods.createAccount();
                Database.DMLOptions objDMLOptions = new Database.DMLOptions();
                objDMLOptions.DuplicateRuleHeader.AllowSave = true;
                Database.SaveResult accountSaveResult = Database.insert(acc,objDMLOptions); 
                //insert acc;

                Opportunity opp = Utils_TestMethods.createOpenOpportunity(acc.id);
                insert opp;
                
                Opportunity opp1 = Utils_TestMethods.createOpenOpportunity(acc.id);
                insert opp1;
                
                test.startTest();
                List<Task> tasks= new List<Task>();
                for(Integer i=0;i<10;i++)
                {
                    Task task = Utils_TestMethods.createOpenActivity(null,lead1.Id,  'Not Started', Date.Today().addDays(2));
                    tasks.add(task);
                }
                Database.SaveResult[] tasksInsert= Database.insert(tasks, false);
                
                 List<Id> tasksId = new List<Id>();
                 for(Database.SaveResult sr: tasksInsert)
                 {
                     if(sr.isSuccess())
                     {
                        tasksId.add(sr.Id);
                     }    
                 }
                Task task = [select ActivityDate, Status from Task where Id in:tasksId limit 1];
                System.assertEquals(task.Status, 'Not Started');
                System.assertEquals(task.ActivityDate, Date.Today().addDays(2));            
                
                tasks.clear();
                for(Integer i=0;i<10;i++)
                {
                    task = Utils_TestMethods.createOpenActivity(null,lead1.Id,  'Completed', Date.Today());
                    tasks.add(task);
                }
                tasksInsert= Database.insert(tasks, false);
                    
                task = Utils_TestMethods.createTask(null,lead1.Id,  'Not Started');
                tasks.clear();
                tasks.add(task);
                tasksInsert= Database.insert(tasks, false);
                
                task = [select ActivityDate, Status from Task where Id in:tasksId limit 1];
                
                task.ActivityDate = task.ActivityDate.addDays(2);
                update task;
                task = [select ActivityDate, Status from Task where Id =:task.Id limit 1];
                System.assertEquals(task.ActivityDate, Date.Today().addDays(4));                        
                
                task.Status= Label.CL00327;
                update task;
                task = [select ActivityDate, Status from Task where Id =:task.Id limit 1];
                System.assertEquals(task.Status, Label.CL00327);
                
                tasks.clear();
                task = [select ActivityDate, Status from Task where Id =:task.Id limit 1];            
                task.ActivityDate = null;
                tasks.add(task);
                tasksInsert= Database.update(tasks, false);
                
                System.debug('------ First Activity Task After Insert Starts ------');
                Task opptyTask= Utils_TestMethods.createTask(opp.Id,null,'Not Started');
                Database.insert(opptyTask, false);
                
                List<Opportunity> opptyInsert= [SELECT Tech_FirstActivity__c FROM Opportunity WHERE Id =:opp.id limit 1];
                System.debug('-------- oppty '+opptyInsert);
                System.assertEquals(opptyInsert[0].Tech_FirstActivity__c, Date.Today());
                
                System.debug('------ First Activity Task After Update Starts ------');
                opptyTask.WhatId=opp1.Id;
                Database.update(opptyTask, false);
                
                List<Opportunity> opptyUpdate= [SELECT Tech_FirstActivity__c FROM Opportunity WHERE Id =:opp.id limit 1];
                System.debug('-------- oppty '+opptyUpdate);
                System.assertEquals(opptyUpdate[0].Tech_FirstActivity__c, Date.Today());
                
                test.stopTest();
            }
        }
    }
}