/**
* @author Julio Enrique
* @date 24/08/2015
* @description utils of challenge reward
*/
public with sharing class FieloPRM_UTILS_ChallengeReward {
    public static final String EXPIRATION_TYPE_DAYS = 'Days';
    public static final String EXPIRATION_TYPE_MONTHS = 'Months';
    public static final String EXPIRATION_TYPE_YEARS = 'Years';
    public static final String EXPIRATION_TYPE_FIX_DATE = 'Fix Date';
    public static final String EXPIRATION_TYPE_NO_EXPIRATION = 'None';

    /**
    * @author Julio Enrique
    * @date 24/08/2015
    * @description calculate expiration date of a reward, based on the type and number. If its no expiration, returns null
    */
    public static Date calculateExpirationDate(FieloCH__CHallengeReward__c chr , Integer expirationNumber){
        String expirationType = chr.F_PRM_ExpirationType__c;
        Date returnDate = Date.today();

        if(EXPIRATION_TYPE_DAYS == expirationType){
            returnDate = returnDate.addDays(expirationNumber);
        }else if(EXPIRATION_TYPE_MONTHS == expirationType){
            returnDate = returnDate.addMonths(expirationNumber);
        }else if(EXPIRATION_TYPE_YEARS == expirationType){
            returnDate = returnDate.addYears(expirationNumber);
        }else if(EXPIRATION_TYPE_FIX_DATE == expirationType){
            Date fixDate = Date.newInstance(returnDate.Year(), Integer.valueof(chr.F_PRM_ExpirationMonth__c), Integer.valueof(chr.F_PRM_ExpirationDay__c));
            returnDate = fixDate;
            if(Date.Today() > fixDate){
                returnDate = returnDate.addYears(1);
            }
        }else{
            returnDate = null;
        }
        return returnDate;
    }

}