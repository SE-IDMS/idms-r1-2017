public with sharing class VFC_AddRemoveSpecClassification {
    
    Specialization__c  spec;
    list<SpecializationClassification__c> lstExistingClasification = new list<SpecializationClassification__c>();
    list<SpecializationClassification__c> lstExistingLvl2 = new list<SpecializationClassification__c>();
    
    Set<Id> selOpts1 = new Set<Id>();
    public Boolean hasError{get;set;}
    public SelectOption[] selClassificationLevel { get; set; }
    public SelectOption[] allClassificationLevel{ get; set; }
    public String type {get;set;}
    
    public VFC_AddRemoveSpecClassification(ApexPages.StandardController controller) {

        selClassificationLevel = new List<SelectOption>();  
        allClassificationLevel = new List<SelectOption>();
        set<String> setCL1 = new set<String>();
        spec = (Specialization__c)controller.getRecord();
        type = ApexPages.currentPage().getParameters().get('type');
        lstExistingClasification = [Select classificationlevelCatalog__r.Name, ClassificationLevelCatalog__c,classificationlevelCatalog__r.classificationlevelname__c from SpecializationClassification__c where Specialization__c = : spec.Id and classificationlevelCatalog__r.parentclassificationlevel__c = null order by classificationlevelCatalog__r.classificationlevelname__c limit 1000];
        
        if(type == Label.CLJUN13PRM03)
        {
            if(lstExistingClasification != null && !lstExistingClasification.isEmpty())
            {
                for (SpecializationClassification__c s : lstExistingClasification ) 
                {

                    selClassificationLevel.add(new SelectOption(s.ClassificationLevelCatalog__c,s.classificationlevelCatalog__r.classificationlevelname__c));
                    selOpts1.add(s.ClassificationLevelCatalog__c);
                    //setCL1.add(s.classificationlevelCatalog__r.Name);
                }  
            }
            
            list<ClassificationLevelCatalog__c> lstAllClassificationLevel1 = new list<ClassificationLevelCatalog__c>();
            lstAllClassificationLevel1 = [SELECT id,name,classificationlevelname__c FROM ClassificationLevelCatalog__c WHERE ParentClassificationLevel__c = null and active__c = true order by classificationlevelname__c limit 1000];        
            for(ClassificationLevelCatalog__c classificationLevel : lstAllClassificationLevel1) 
            {
                if(!(selOpts1.contains(classificationLevel.Id)))
                    allClassificationLevel.add(new SelectOption(classificationLevel.Id, classificationLevel.classificationlevelname__c ));
            }
        }
        else if(type == Label.CLOCT13PRM80)
        {
            list<ClassificationLevelCatalog__c> lstClassificationLevel2 = new list<ClassificationLevelCatalog__c>();
            if(lstExistingClasification != null && !lstExistingClasification.isEmpty())
            {
                for (SpecializationClassification__c s : lstExistingClasification ) 
                {
                    setCL1.add(s.classificationlevelCatalog__r.Name);
                }
                lstClassificationLevel2 = [SELECT id,name,classificationlevelname__c FROM ClassificationLevelCatalog__c WHERE ParentClassificationLevel__r.name in:setCL1 and active__c = true order by classificationlevelname__c limit 1000];
            }
            else
            {
                hasError = true; 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select Classfication Level 1'));
            }
            lstExistingLvl2 = [Select id,ClassificationLevelCatalog__c,classificationlevelCatalog__r.classificationlevelname__c from SpecializationClassification__c WHERE Specialization__c = : spec.Id and classificationlevelCatalog__r.parentclassificationlevel__c != null order by classificationlevelCatalog__r.classificationlevelname__c limit 5000];
            for(SpecializationClassification__c existingLvl2 : lstExistingLvl2)
            {
                selClassificationLevel.add(new SelectOption(existingLvl2.ClassificationLevelCatalog__c,existingLvl2.classificationlevelCatalog__r.classificationlevelname__c));
                selOpts1.add(existingLvl2.ClassificationLevelCatalog__c);
            }
            for(ClassificationLevelCatalog__c allLvl2 : lstClassificationLevel2 )
            {
                if(!(selOpts1.contains(allLvl2.Id)))
                    allClassificationLevel.add(new SelectOption(allLvl2.Id, allLvl2.classificationlevelname__c ));
            }
            
        }
    }//End ofConstructor
    
    public PageReference updateSpecializationClassification() 
    {
        Savepoint sp =  Database.setSavepoint();
        try
        {            
            set<Id> setSelIds = new set<ID>();
            list<SpecializationClassification__c> lstNewSpecClassification = new list<SpecializationClassification__c>();
            for(SelectOption so : selClassificationLevel) 
            {
                SpecializationClassification__c specClasification = new SpecializationClassification__c();
                specClasification.Specialization__c= spec.Id;
                specClasification.ClassificationLevelCatalog__c = so.getValue();
                lstNewSpecClassification.add(specClasification);
                setSelIds.add(so.getValue());
            }
            if(type == Label.CLJUN13PRM03 && lstExistingClasification != null && !lstExistingClasification.isEmpty())
            {
                selOpts1.removeAll(setSelIds);
                lstExistingLvl2 = [Select id,ClassificationLevelCatalog__c,classificationlevelCatalog__r.classificationlevelname__c from SpecializationClassification__c WHERE Specialization__c= : spec.Id and classificationlevelCatalog__r.parentclassificationlevel__c in : selOpts1 limit 5000];
                delete lstExistingClasification;
                delete lstExistingLvl2;
                
            }
            else if(type == Label.CLJUN13PRM04 && lstExistingLvl2 != null && !lstExistingLvl2.isEmpty())
            {
                delete lstExistingLvl2;
            }
            lstExistingClasification.clear();
            
            if(lstNewSpecClassification != null && !lstNewSpecClassification.isEmpty())
                insert lstNewSpecClassification;
        }
        catch(DMLException e)
        {
            Database.rollback(sp);
            hasError = true;    
            for (Integer i = 0; i < e.getNumDml(); i++) 
            { 
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i),'')); System.debug(e.getDmlMessage(i)); 
            } 
                       
            return null;
        }
        
        return new pagereference('/'+spec.Id);    
       return null;
    }

}//End of class