/*    
      Author          : Srikant (Schneider Electric)    
      Date Created    : 03/21/2012    
      Description     : Controller class for the visualforce page PRJ_UpdatePriority                      
                        Updates the value for the field 'Priority' after checking whether the logged-in user has the authority to do so.
*/                      

public with sharing class PRJ_UpdatePriorityController
{
    public PRJ_ProjectReq__c oPR{get;set;}
    
    public ApexPages.StandardController controller;
    
    public PRJ_UpdatePriorityController(ApexPages.StandardController controller)
    {
        this.controller = controller;
        oPR = (PRJ_ProjectReq__c) controller.getRecord();  
        oPR = [Select Id,Parent__c,NextStep__c,OwnerId,ScoreResultCalculated__c,BusGeographicZoneIPO__c,BusinessTechnicalDomain__c,ParentFamily__c,AppServicesDecision__c,AppGlobalOpservdecisiondate__c,AuthorizeFunding__c,BCINumber__c,WorkType__c,RecordType.DeveloperName from PRJ_ProjectReq__c where Id = :oPR.Id];
         
    }
    
    public PageReference updateProjectRequest()
    {
        List<DMTAuthorizationMasterData__c> objAuth = new List<DMTAuthorizationMasterData__c>();
        Set<string> setUser = new Set<string>();
        try
        {   objAuth = [SELECT AuthorizedUser1__c,AuthorizedUser2__c,AuthorizedUser3__c,AuthorizedUser4__c,AuthorizedUser5__c,AuthorizedUser6__c,AuthorizedUser7__c,AuthorizedUser8__c,AuthorizedUser9__c,AuthorizedUser10__c,BusGeographicZoneIPO__c,BusinessTechnicalDomain__c,Id,NextStep__c,ParentFamily__c,Parent__c FROM DMTAuthorizationMasterData__c where NextStep__c =:System.Label.DMT_StatusValid AND BusinessTechnicalDomain__c =:oPR.BusinessTechnicalDomain__c limit 1];                    
            if(objAuth.size() > 0)
            { 
                System.Debug('Data present in table'+objAuth);
                SObject sobjectAuth =(SObject)objAuth[0];
                for(Integer i=1;i<11;i++)
                {
                    if(sobjectAuth.get('AuthorizedUser'+i+'__c') !=null)
                        setUser.add(sobjectAuth.get('AuthorizedUser'+i+'__c')+'');
                }                            
            }
            if(setUser.contains(UserInfo.getUserId()))
                {
                    System.debug('Authorized User');         
                    PRJ_UpdateStatusControllerVerify.updatePriority(oPR,null);
                }    
            else
                {
                    System.debug('UnAuthorized User');
                    if(oPR.ScoreResultCalculated__c == '4 (Mandatory)')
                        ApexPages.addMessages(new noMessageException('Only Domain Owner has the ability to change the Priority Value to 4'));
                    else
                        update oPR;
                }  
            
        }
        catch(DmlException dmlexp)
        {
            for(integer i = 0;i<dmlexp.getNumDml();i++){
                if(dmlexp.getDmlMessage(i).contains('insufficient access rights on object id')){
                    ApexPages.addMessages(new noMessageException('You do not have sufficient privilages to change the Priority Value'));
                }else
                ApexPages.addMessages(new noMessageException(dmlexp.getDmlMessage(i)));
             }
                      
        }
        catch(Exception exp)
        {
            ApexPages.addMessages(new noMessageException(exp.getMessage()));
        }
        
        return null;
    }
    
     public PageReference backToProjectRequest()
    {
        PageReference pageRef = new PageReference('/'+ oPR.Id);
        pageRef.setRedirect(true);
        return pageRef; 
    }
    
    public class noMessageException extends Exception{}
    
}