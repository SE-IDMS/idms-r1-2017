// Test Class for VFC_GrantFullPRMPortalAccess class

@isTest
private class VFC_GrantFullPRMPortalAccess_Test{
// TestSetup 
    @testSetup static void inserts(){
    Country__c TestCountry = new Country__c();
            TestCountry.Name   = 'India';
            TestCountry.CountryCode__c = 'IN';
            TestCountry.InternationalPhoneCode__c = '91';
            TestCountry.Region__c = 'APAC';
            INSERT TestCountry;
    Country__c testCountry3 = new Country__c();
            testCountry3.Name   = 'Pakistan';
            testCountry3.CountryCode__c = 'PK';
            testCountry3.InternationalPhoneCode__c = '91';
            testCountry3.Region__c = 'APAC';
            
         INSERT testCountry3;
    Country__c testCountry4 = new Country__c();
            testCountry4.Name   = 'Srilanka';
            testCountry4.CountryCode__c = 'SL';
            testCountry4.InternationalPhoneCode__c = '91';
            testCountry4.Region__c = 'APAC';
            
         INSERT testCountry4;
    Country__c testCountry5 = new Country__c();
            testCountry5.Name   = 'Nepal';
            testCountry5.CountryCode__c = 'NP';
            testCountry5.InternationalPhoneCode__c = '91';
            testCountry5.Region__c = 'APAC';
            
         INSERT testCountry5;

    StateProvince__c testStateProvince = new StateProvince__c();
            testStateProvince.Name = 'Karnataka';
            testStateProvince.Country__c = testCountry.Id;
            testStateProvince.CountryCode__c = testCountry.CountryCode__c;
            testStateProvince.StateProvinceCode__c = '10';
            INSERT testStateProvince;
            
        Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', Country__c=TestCountry.id, PRMCompanyName__c='TestAccount',PRMBusinessType__c='FI',
                                    PRMAreaOfFocus__c='FI1',PLShowInPartnerLocatorForced__c = true, StateProvince__c = testStateProvince.id,City__c='Bangalore');
            insert PartnerAcc;
        Contact PartnerCon = new Contact(FirstName='Test',LastName='lastname',AccountId=PartnerAcc.Id, JobTitle__c='Z3',CorrespLang__c='EN',
                  WorkPhone__c='999121212',
                  Country__c=TestCountry.Id,
                  PRMFirstName__c = 'Test',
                  PRMLastName__c = 'lastname',
                  PRMEmail__c = 'test.lastName@yopmail.com',
                  PRMWorkPhone__c = '999121212',
                  PRMPrimaryContact__c = true
                  );
                  Insert PartnerCon;
    
    }
 // Method 1
    static testMethod void positiveCases(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) {        
            VFC_GrantFullPRMPortalAccess gfpAccess=new VFC_GrantFullPRMPortalAccess(); 
            gfpAccess.SaveStatus='Failure';       
            Contact con = [Select FirstName,LastName,JobTitle__c,CorrespLang__c,AccountId,WorkPhone__c,Country__c,PRMFirstName__c,PRMLastName__c,PRMEmail__c,PRMWorkPhone__c,PRMPrimaryContact__c from Contact where WorkPhone__c='999121212'];
            Test.SetCurrentPageReference(New PageReference('Page.VFC_GrantFullPRMPortalAccess'));
            String contactID= System.CurrentPageReference().getParameters().put('id',con.Id);
            gfpAccess.GrantFullPortalAccess();  
        }
            
    }
  
    static testMethod void negativeCases(){
     Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) {           
            VFC_GrantFullPRMPortalAccess gfpAccess=new VFC_GrantFullPRMPortalAccess(); 
            gfpAccess.SaveStatus='Failure';        
            Contact con = [Select FirstName,LastName,JobTitle__c,CorrespLang__c,AccountId,WorkPhone__c,Country__c,PRMFirstName__c,PRMLastName__c,PRMEmail__c,PRMWorkPhone__c,PRMPrimaryContact__c from Contact where WorkPhone__c='999121212'];
            Test.SetCurrentPageReference(New PageReference('Page.VFC_GrantFullPRMPortalAccess'));
            String contactID= System.CurrentPageReference().getParameters().put('id',con.Id);
            gfpAccess.GrantFullPortalAccess();
            }           
    }
    static testMethod void someContactId(){
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) { 
        VFC_GrantFullPRMPortalAccess gfpAccess=new VFC_GrantFullPRMPortalAccess(); 
        gfpAccess.SaveStatus='Failure'; 
        Test.SetCurrentPageReference(New PageReference('Page.VFC_GrantFullPRMPortalAccess'));
        String contactID= System.CurrentPageReference().getParameters().put('id','0987654321');
        gfpAccess.GrantFullPortalAccess();
        }    
    }
    static testMethod void noContactId(){
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) {      
                VFC_GrantFullPRMPortalAccess gfpAccess=new VFC_GrantFullPRMPortalAccess(); 
                gfpAccess.SaveStatus='Failure';                 
                gfpAccess.GrantFullPortalAccess();  
            }
    }    
        static testMethod void noValidMessages(){
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        UserRole r = [SELECT Id FROM UserRole WHERE Name='CEO']; 
        User u = new User(Alias = 'standt', 
                         Email='test@schneider-electric.com', 
                         EmailEncodingKey='UTF-8',
                         FirstName ='Test First',      
                         LastName='Testing',
                         CommunityNickname='CommunityName123',      
                         LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', 
                         ProfileId = p.Id,
                         TimeZoneSidKey='America/Los_Angeles', 
                         UserName='subhashish@test1.com',
                         userroleid = r.Id,
                         isActive = True,                       
                         BypassVR__c = True);
        
        
       System.runAs(u) { 
        Country__c country = [select Id,Name,CountryCode__c,InternationalPhoneCode__c,Region__c FROM Country__c Where Name='India'];     
        StateProvince__c stateProv = [select id,name,Country__c,CountryCode__c,StateProvinceCode__c from StateProvince__c  where name='Karnataka'];
        Country__c country3=[select id,Name,CountryCode__c,InternationalPhoneCode__c,Region__c from Country__c  where name = 'Pakistan'];
        Country__c country4=[select id,Name,CountryCode__c,InternationalPhoneCode__c,Region__c from Country__c  where name = 'Srilanka'];
        Country__c country5=[select id,Name,CountryCode__c,InternationalPhoneCode__c,Region__c from Country__c  where name = 'Nepal'];
        Account PartnerAcc1 = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345', Country__c=country.id, PRMCompanyName__c='TestAccount',PRMCountry__c=country.id,PRMBusinessType__c='FI',
                                    PRMAreaOfFocus__c='FI1',PLShowInPartnerLocatorForced__c = true,StateProvince__c = stateProv.id,City__c='Bangalore');
        insert PartnerAcc1;
        Contact PartnerCon1 = new Contact(FirstName='Test',LastName='lastname',AccountId=PartnerAcc1.Id, JobTitle__c='Z3',CorrespLang__c='EN',
                  WorkPhone__c='999121212',
                  Country__c=country.Id,
                  PRMFirstName__c = 'Test',
                  PRMLastName__c = 'lastname',
                  PRMEmail__c = 'test.lastName@yopmail.com',
                  PRMWorkPhone__c = '999121212',
                  PRMPrimaryContact__c = true
                  );
                  Insert PartnerCon1;
        
        ClassificationLevelCatalog__c cLC = new ClassificationLevelCatalog__c(Name = 'FI', ClassificationLevelName__c = 'Financier', Active__c = true);
        insert cLC;
        ClassificationLevelCatalog__c cLChild = new ClassificationLevelCatalog__c(Name = 'FI1', ClassificationLevelName__c = 'Multi Lateral Financial Institution', Active__c = true, ParentClassificationLevel__c = cLC.id);
        insert cLChild; 
         PRMCountry__c newPRMCntry = new PRMCountry__c(Name='EMEA - FR1',Country__c = PartnerAcc1.PRMCountry__c, PLDatapool__c = 'test112',CountryPortalEnabled__c =true,
                                    SupportedLanguage1__c='EN',SupportedLanguage2__c='ES',MemberCountry1__c=country3.Id,MemberCountry2__c=country4.Id,
                                    MemberCountry3__c=country5.Id);
                Insert newPRMCntry;
        
        CountryChannels__c cChannels = new CountryChannels__c();
        cChannels.PRMCountry__c=newPRMCntry.Id;
        cChannels.Active__c = true;
        cChannels.AllowPrimaryToManage__c = True; 
        cChannels.Channel__c = cLC.Id; 
        cChannels.SubChannel__c = cLChild.id;
        cChannels.Country__c = country.id; 
        cChannels.CompleteUserRegistrationonFirstLogin__c = true;
       // cChannels.ChannelCode__c= PartnerAcc.PRMBusinessType__c;
       // cChannels.SubChannelCode__c= PartnerAcc.PRMAreaOfFocus__c;      
        insert cChannels;            
            
        VFC_GrantFullPRMPortalAccess gfpAccess=new VFC_GrantFullPRMPortalAccess(); 
        gfpAccess.SaveStatus='Failure'; 
        Test.SetCurrentPageReference(New PageReference('Page.VFC_GrantFullPRMPortalAccess'));
        String contactID= System.CurrentPageReference().getParameters().put('id',PartnerCon1.id);  
        Test.SetCurrentPageReference(New PageReference('Page.VFC_GrantFullPRMPortalAccess'));
        String cntID= System.CurrentPageReference().getParameters().put('id',PartnerCon1.id);     
        gfpAccess.GrantFullPortalAccess();  
        }
        
    }
    
}