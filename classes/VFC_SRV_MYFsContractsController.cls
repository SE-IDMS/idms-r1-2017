Public Without sharing Class VFC_SRV_MYFsContractsController{
 
 Public SVMXC__Service_Contract__c HeaderContract{get;set;}
 Public Date Mydate{get;set;}
 Public list<SVMXC__Site__c> Locationlst{get;set;}
 Public map<Id,list<SVMXC__Service_Contract__c>> LocationContractLineMap{get;set;}
 Public List<SVMXC__Service_Contract__c> ChildResult{get;set;}
 Public map<Id,SVMXC__Site__c> LocationIdLst{get;set;}
 Public map<Id,SVMXC__Site__c> Locationmap{get;set;}
 Public String site{get;set;}
 Public Id ParentContractId{get;set;}
 Public String UltimateParentAccount{get;set;}
 Public String SelectedCountry{get;set;}
 Public Integer StartDate{get;set;}
 Public String EndDate {get;set;}
 Public String UserId{get;set;}
  
 Public VFC_SRV_MYFsContractsController(){
    ParentContractId = Apexpages.currentpage().getparameters().get('Id');
    UltimateParentAccount = Apexpages.currentpage().getparameters().get('Params');
    SelectedCountry= Apexpages.currentpage().getparameters().get('country');
    Site= Apexpages.currentpage().getparameters().get('site');
    system.debug('+++USERID+++++'+UserId);
    UserId = Apexpages.currentpage().getparameters().get('UId');
    if(userId==null || userId ==''){
     userId = UserInfo.getUserId();
     
    }
    Datetime  today = date.today();
    mydate= date.valueof(today).adddays(+120);
    system.debug('###Paretnt Contract###'+ParentContractId);
    system.debug('###Ultimate Account###'+UltimateParentAccount );
    system.debug('###SelectedCountry###'+SelectedCountry);
    system.debug('###Site###'+Site);
    GetContracts();
    }
    Public Void GetContracts(){
    Locationlst = new list<SVMXC__Site__c>();
    Locationmap = new map<Id,SVMXC__Site__c>();
    if(ParentContractId!=null){ 
      HeaderContract = [SELECT Id, Name, Status__c, SVMXC__Start_Date__c,Nb_of_years__c, SVMXC__End_Date__c, SVMXC__Company__c, SVMXC__Company__r.Name, DefaultInstalledAtAccount__c, DefaultInstalledAtAccount__r.Name, SoldtoAccount__c, SoldtoAccount__r.Name FROM SVMXC__Service_Contract__c WHERE ID =:ParentContractId  ORDER BY SVMXC__End_Date__c];
      Integer StartMonth= HeaderContract.SVMXC__Start_Date__c.Month();
      Integer StartYear = HeaderContract.SVMXC__Start_Date__c.Year();
      Integer EndMonth= HeaderContract.SVMXC__End_Date__c.Month();
      Integer EndYear = HeaderContract.SVMXC__End_Date__c.Year();
      StartDate=EndYear - StartYear ;
      Integer EndDate1 = StartMonth-EndMonth;
      if(EndDate1<0){
        EndDate = string.valueof(-EndDate1);
      }
      else{
        EndDate  = string.valueof(EndDate1);
      }
     
      
      //system.debug('###HeaderContract###'+HeaderContract );
    }
    set<id> LocIds = new set<id>();
    //list<SVMXC__Site__c> Siteslst = new list<SVMXC__Site__c>();
    Set<Id> anAccountSet = new Set<Id>();
     if(Site==System.label.CLSRVMYFS_ViewAll){
          if(!Test.isRunningTest()){
             Locationlst = AP_SRV_myFSUtils.getAuthorizedSitesForUser( userId, SelectedCountry, UltimateParentAccount );
          }
          else{
           Locationlst =[SELECT id,name,SVMXC__Account__c,SVMXC__Account__r.Country__r.name,SVMXC__Account__r.UltiParentAcc__c FROM SVMXC__Site__c];
          }
        
    }
    else{
        Locationlst  =[SELECT id,name,SVMXC__Account__c,SVMXC__Account__r.Country__r.name,SVMXC__Account__r.UltiParentAcc__c 
                       FROM SVMXC__Site__c 
                       WHERE Id=:Site LIMIT 1000];
              
    }
    
    for(SVMXC__Site__c Loc :Locationlst  ){
             LocIds.add(Loc.Id);
             Locationmap.put(Loc.id,loc);
    } 
   
    map<Id,Id> LocationAccountMap = new map<Id,Id>();
    for(SVMXC__Site__c Loc : Locationlst ){
      anAccountSet.add(Loc.SVMXC__Account__c);
      LocationAccountMap.put(Loc.SVMXC__Account__r.Id,loc.Id);
      system.debug('###LocationAccountMap###'+LocationAccountMap);
    }  
    LocationContractLineMap = new map<Id,list<SVMXC__Service_Contract__c>>();
    system.debug('###anAccountSet###'+anAccountSet); 
     list<SVMXC__Service_Contract_Products__c> coveredProdclist = [select id,SVMXC__Service_Contract__r.Id from SVMXC__Service_Contract_Products__c where 
                                                              SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c IN :anAccountSet ];
        Set<id> contractIds = new Set<id>();
        if(coveredProdclist!=null && !coveredProdclist.isEmpty()){
        for(SVMXC__Service_Contract_Products__c coveredProd : coveredProdclist){
           contractIds.add(coveredProd.SVMXC__Service_Contract__r.Id);
        }
          
        }
        System.debug('contractIds '+contractIds);
        childResult = new List<SVMXC__Service_Contract__c>();
        childResult = [SELECT Id, Name, Status__c,SVMXC__Service_Contract_Notes__c, SVMXC__Start_Date__c,SVMXC__Company__r.Id, SVMXC__End_Date__c, ParentContract__c, ParentContract__r.status__c,
                   SVMXC__Company__c, 
                   SVMXC__Company__r.Name, ParentContract__r.SVMXC__Company__c, ParentContract__r.SVMXC__Company__r.Name, ParentContract__r.DefaultInstalledAtAccount__c, 
                   ParentContract__r.DefaultInstalledAtAccount__r.Name, ParentContract__r.SoldtoAccount__c, ParentContract__r.SoldtoAccount__r.Name,
                   (select id,name,SVMXC__Installed_Product__c,SVMXC__Installed_Product__r.SVMXC__Site__c,SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c,
                      SVMXC__Installed_Product__r.id,Serial_Number__c,SVMXC__Installed_Product__r.name from R00N70000001hzdaEAA where 
                      SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c IN :anAccountSet
                   ) 
                   ,(select id,name,SVMXC__Service_Name__c from R00N70000001hzdmEAA) FROM SVMXC__Service_Contract__c 
                    WHERE
                    ( ID IN :contractIds OR SVMXC__Company__c IN :anAccountSet)  
                     AND ParentContract__c =:ParentContractId ];
           
        system.debug('###childResult ###'+childResult );
          
        list<SVMXC__Service_Contract__c> ServicelInes = new list<SVMXC__Service_Contract__c >();
        LocationIdLst = new map<Id,SVMXC__Site__c>();
        Map<Id,Id> ServiceLineAccountLocationMap = new  Map<Id,Id>();
        for(SVMXC__Service_Contract__c Lines : childResult ){
            Map<id,id> ContractCompany = new Map<id,id>(); 
            
            if(Lines.SVMXC__Company__c!=null){
              ContractCompany.put(Lines.SVMXC__Company__c,Lines.SVMXC__Company__c);
              ServiceLineAccountLocationMap.put(Lines.SVMXC__Company__c,LocationAccountMap.get(Lines.SVMXC__Company__c));
            }
            else{
                if(Lines.R00N70000001hzdaEAA!=null){
                    for(SVMXC__Service_Contract_Products__c CoveredProduct : Lines.R00N70000001hzdaEAA){
                      if(CoveredProduct.SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c!=null){
                          if(anAccountSet.contains(CoveredProduct.SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c)){
                           ContractCompany.put(CoveredProduct.SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c,CoveredProduct.SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c);
                          }
                       }
                    }
                    list<Id> ContractCompanyIds = new list<Id>();
                    ContractCompanyIds = ContractCompany.values();
                    Id Company = ContractCompanyIds[0];
                    ServiceLineAccountLocationMap.put(Company ,LocationAccountMap.get(Company));
                }
            }
            
            
            
        }
        for(SVMXC__Service_Contract__c Lines : childResult ){
            
            Map<id,id> ContractCompany = new Map<id,id>(); 
            
           if(Lines.SVMXC__Company__c!=null){
              ContractCompany.put(Lines.SVMXC__Company__c,Lines.SVMXC__Company__c);
            }
            else{
                if(Lines.R00N70000001hzdaEAA!=null){
                    for(SVMXC__Service_Contract_Products__c CoveredProduct : Lines.R00N70000001hzdaEAA){
                       if(CoveredProduct.SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c!=null){
                            if(anAccountSet.contains(CoveredProduct.SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c)){
                               ContractCompany.put(CoveredProduct.SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c,CoveredProduct.SVMXC__Installed_Product__r.SVMXC__Site__r.SVMXC__Account__c);
                            }
                        }
                    
                    }
                }
            }
            list<Id> ContractCompanyIds = new list<Id>();
            ContractCompanyIds = ContractCompany.values();
            Id Company = ContractCompanyIds[0];
             system.debug('Locationmap'+Locationmap);
             system.debug('LocationAccountMap'+LocationAccountMap);
             system.debug('LocationIdLst'+LocationIdLst);
             system.debug('Company'+Company );
             if(Lines.SVMXC__Company__c!=null){
                 if(!LocationContractLineMap.containskey(LocationAccountMap.get(Lines.SVMXC__Company__c)))
                 {
                     if(ServiceLineAccountLocationMap.get(Lines.SVMXC__Company__c)==LocationAccountMap.get(Lines.SVMXC__Company__c))
                     {
                         ServicelInes = new list<SVMXC__Service_Contract__c >();
                         ServicelInes.add(Lines);
                         System.Debug('###LocationAccountMap'+LocationAccountMap);
                         system.debug('###ServiceLines'+ServicelInes);
                         System.debug('###LinesCompany'+Lines.SVMXC__Company__c);
                         System.debug('###LocationofLines'+LocationAccountMap.get(Lines.SVMXC__Company__c));
                         if(anAccountSet.contains(Lines.SVMXC__Company__c)){
                             LocationContractLineMap.put(LocationAccountMap.get(Lines.SVMXC__Company__c),ServicelInes);
                         }
                     }
                     System.debug('###LocationContractLineMap'+LocationContractLineMap); 
                }
                else{
                     ServicelInes.add(Lines);
                     LocationContractLineMap.put(LocationAccountMap.get(Lines.SVMXC__Company__c),ServicelInes);
                     System.debug('###LocationContractLineMap'+LocationContractLineMap); 
                 }
             }
             else{
                 system.debug('Company'+Company);
                 system.debug('LocationAccountMap'+LocationAccountMap);
                 system.debug('ServiceLineAccountLocationMap'+ServiceLineAccountLocationMap);
                 if(!LocationContractLineMap.containskey(LocationAccountMap.get(Company))){
                   if(ServiceLineAccountLocationMap.get(Company)==LocationAccountMap.get(Company)){
                        system.debug('success');
                        ServicelInes = new list<SVMXC__Service_Contract__c >();
                        ServicelInes.add(Lines);
                        system.debug('###ServiceLines'+ServicelInes);
                        LocationContractLineMap.put(LocationAccountMap.get(Company),ServicelInes);
                        System.debug('###LocationContractLineMap'+LocationContractLineMap); 
                    }
                }
                else{
                     ServicelInes.add(Lines);
                     LocationContractLineMap.put(LocationAccountMap.get(Company ),ServicelInes);
                     System.debug('###LocationContractLineMap'+LocationContractLineMap);
                 }
                 
             }
             
            system.debug('###CoveredProductCompany'+company);
            system.debug('###ServiceLinesCompany'+Lines.SVMXC__Company__c);
            system.debug('###LocationAccountMap'+LocationAccountMap);
            system.debug('###Company'+Locationmap);
            system.debug('###Company'+LocationIdLst); 
            
            if(Lines.SVMXC__Company__c!=null){
                if(anAccountSet.contains(Lines.SVMXC__Company__c)){
                if(LocationIdLst!=null  && !LocationIdLst.containsKey(Locationmap.get((LocationAccountMap.get(Lines.SVMXC__Company__c))).Id)){
                    LocationIdLst.put(Locationmap.get(LocationAccountMap.get(Lines.SVMXC__Company__c)).id,Locationmap.get(LocationAccountMap.get(Lines.SVMXC__Company__c)));
                    system.debug('###Locations'+LocationIdLst); 
                } 
                              
               }
               else{
                  if(anAccountSet.contains(Company)){
                     if(LocationIdLst!=null &&  !LocationIdLst.containsKey(Locationmap.get((LocationAccountMap.get(Company))).Id)){
                          LocationIdLst.put(Locationmap.get(LocationAccountMap.get(Company)).id,Locationmap.get(LocationAccountMap.get(Company)));
                          system.debug('###Locations'+LocationIdLst); 
                      }
                   }
                }
            }
            else{
            
               if(anAccountSet.contains(Company)){
                if(LocationIdLst!=null  && !LocationIdLst.containsKey(Locationmap.get((LocationAccountMap.get(Company))).Id)){
                    LocationIdLst.put(Locationmap.get(LocationAccountMap.get(Company)).id,Locationmap.get(LocationAccountMap.get(Company)));
                    system.debug('###Locations'+LocationIdLst); 
                } 
                              
               }
           }           
                
            
        }
      system.debug('LocationContractLineMap'+LocationContractLineMap);
      system.debug('LocationIdLst'+LocationIdLst);
    }  
}