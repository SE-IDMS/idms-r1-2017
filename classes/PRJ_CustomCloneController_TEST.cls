@isTest
private class PRJ_CustomCloneController_TEST
{
    static testMethod void test_PRJ_CustomCloneController() 
    {
        DMTAuthorizationMasterData__c testDMTA1 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Created','Buildings','Global');
        insert testDMTA1; 
        PRJ_ProjectReq__c testProj = new PRJ_ProjectReq__c();
        testProj.ParentFamily__c = 'Business';
        testProj.Parent__c = 'Buildings';
        testProj.BusGeographicZoneIPO__c = 'Global';
        testProj.BusGeographicalIPOOrganization__c = 'CIS';
        testProj.GeographicZoneSE__c = 'Global';
        testProj.Tiering__c = 'Tier 1';
        testProj.ProjectRequestDate__c = system.today();
        testProj.Description__c = 'Test';
        testProj.GlobalProcess__c = 'Customer Care';
        testProj.ProjectClassification__c = 'Infrastructure';
        insert testProj;
            
        Budget__c testBud = Utils_TestMethods.createBudget(testProj,true);
        insert testBud;   
        
        DMTAuthorizationMasterData__c testDMTA2 = Utils_TestMethods_DMT.createDMTAuthorizationMasterData(UserInfo.getUserId(),'Valid','Supply Chain');
        insert testDMTA2;         
           
        testProj.NextStep__c='Created';
        testProj.BusinessTechnicalDomain__c = 'Supply Chain';
        update testProj;
        
        PRJ_CustomCloneController vfCtrl = new PRJ_CustomCloneController(new ApexPages.StandardController(testProj));
        vfCtrl.callCustomClone();
    }
    
}