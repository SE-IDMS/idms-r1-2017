/*
    Author          : Nicolas Palitzyne ~ nicolas.palitzyne@accenture.com 
    Date Created    : 11/05/2011
    Description     : This class contains methods to update case fields with values coming from integrated systems
    Date Updated    : 24/11/2012
    Updated By      : Vimal Karunakaran (Global Delivery)
    Description     : Draft FAQ Method Changes
    
    Modified By     :Nikhil Agarwal
    Date            :08-05-2014(May 2014 Release)
    BR              :BR-4759,4800
    Description     :Draft FAQ file Attachment and new field Business Entity
*/

public class AP12_CaseIntegrationMethods
{
    public static Boolean bypassDML = false;

    /** Convention for Product as DataTamplate__c :
        Field1__c : Business 
        Field2__c : Product Line
        Field3__c : Product Family
        Field4__c : Family 
        Field5__c : Sub Family
        Field6__c : Product Succession
        Field7__c : Product Group
        Field8__c : Commercial Reference
        Field9__c : Product Description
        Field10__c : GMR Code
    **/
   
   // Update fields related to PM0 in a case
    public static void UpdateCasePM0(Case Cse, DataTemplate__c Obj)
    {
        DataTemplate__c Product = (DataTemplate__c)Obj;

        if(Product.field1__c != null)
            Cse.ProductBU__c = Product.field1__c;
            
        if(Product.field2__c != null)
            Cse.ProductLine__c = Product.field2__c;  
            
        if(Product.field3__c != null)
            Cse.ProductFamily__c = Product.field3__c;  
            
        if(Product.field4__c != null)
            Cse.Family__c = Product.field4__c;
        
        if(Product.field5__c != null)
            Cse.SubFamily__c = Product.field5__c;
            
        if(Product.field6__c != null)
            Cse.ProductSuccession__c = Product.field6__c;
        
        if(Product.field7__c != null)
            Cse.ProductGroup__c = Product.field7__c;       
        
        if(Product.field8__c != null)
            Cse.CommercialReference__c = Product.field8__c;  

        if(Product.field9__c != null)
            Cse.ProductDescription__c = Product.field9__c;      
        
        if(Product.field11__c != null)
            Cse.TECH_GMRCode__c = Product.field11__c;      
                   
        Cse.CheckedbyGMR__c = true;
        Cse.TECH_LaunchGMR__c = false;
        
        if(!bypassDML) {
            DataBase.SaveResult currentCase_SR = DataBase.Update(Cse,false);
            
            if(!currentCase_SR.isSuccess()) {
                DataBase.Error DMLerror = currentCase_SR.getErrors()[0];
                System.debug('AP12.UpdateCasePM0.DML_EXCEPTION - Error when updating the case.');
                System.debug('AP12.UpdateCasePM0.DML_EXCEPTION - Error Message: ' + DMLerror.getMessage());
            }
            else {
                System.debug('AP12.UpdateCasePM0.SUCCESS - Case updated successfully. ');
            } 
        }                               
    }
 
    // Update fields related to PM0 in a case, when User selects Select Family
    public static void UpdateCaseFamily(Case Cse, DataTemplate__c Obj)
    {
        //*********************************************************************************
        // Method Name : UpdateCaseFamily
        // Purpose : To populate upto Product Family for the Case, when Select Family is selected in GMR Search
        // Created by : Vimal Karunakaran - Global Delivery Team
        // Date created : 24th February 2012
        // Modified by :
        // Date Modified :
        // Remarks : For Apr - 12 Release
        ///*********************************************************************************/
        
        System.Debug('****AP12_CaseIntegrationMethods UpdateCaseFamily Started****');

        DataTemplate__c Product = (DataTemplate__c)Obj;

        if(Product.field1__c != null)
            Cse.ProductBU__c = Product.field1__c;
            
        if(Product.field2__c != null)
            Cse.ProductLine__c = Product.field2__c;  
            
        if(Product.field3__c != null)
            Cse.ProductFamily__c = Product.field3__c;  
            
        if(Product.field4__c != null)
            Cse.Family__c = Product.field4__c;
        
        Cse.SubFamily__c = null;
        Cse.ProductSuccession__c = null;
        Cse.ProductGroup__c = null;       
        Cse.CommercialReference__c = null;  
        Cse.ProductDescription__c = null;      
        
        if(Product.field10__c != null)
            Cse.TECH_GMRCode__c = Product.field10__c;      
            
        
        Cse.CheckedbyGMR__c = true;
        Cse.TECH_LaunchGMR__c = false;
        
        Database.SaveResult CaseUpdate = Database.Update(Cse);          
        if(!CaseUpdate.isSuccess()){
            Database.Error err = CaseUpdate.getErrors()[0];
            System.Debug('######## AP12 Error Updating: '+err.getStatusCode()+' '+err.getMessage());
        }
        System.Debug('****AP12_CaseIntegrationMethods UpdateCaseFamily Finished****');
    }

    // Attach Inquira FAQs to a case
    public static void AttachInquiraFAQ(Case Cse, List<InquiraFAQ__c> FAQList)
    {
        List<InquiraFAQ__c> TobeInsertedFAQs = new List<InquiraFAQ__c>();
        Set<String> ExistingFAQIDs = CheckExistingFAQ(Cse);
        
        For(InquiraFAQ__c FAQ:FAQList)
        {
            if(!ExistingFAQIDs.contains(FAQ.Name))
            {
                FAQ.Case__c = Cse.Id; 
                TobeInsertedFAQs.add(FAQ);
            }
        }
                     
        List<DataBase.SaveResult> AP13_SR = Database.Insert(TobeInsertedFAQs);  
        List<Database.Error> DMLError = new List<Database.Error>();
        For(DataBase.SaveResult SaveResult:AP13_SR)
        {
            System.Debug('#### DML Errors : ' + SaveResult.getErrors() ); 
        }      
    }
    
    // Create a first version of an Inquira FAQ 
    public static list<string> DraftInquiraFAQ(Case Cse,String selectedLanguage,String Country,String Target,String Title,String Answer,list<Attachment> attachmentid)
    {
        WS_Inquira.localeBean Local = new WS_Inquira.localeBean();
        WS_Inquira.ArrayofFaqDataHandler objattachment=new WS_Inquira.ArrayofFaqDataHandler();
        List<WS_Inquira.FaqDataHandler> lstFaqDataHandler = new List<WS_Inquira.FaqDataHandler>();
        WS_Inquira.FaqDataHandler objFaqDataHandler = new WS_Inquira.FaqDataHandler();
        WS_Inquira.FaqCKMServiceImplPort InquiraConnection = new WS_Inquira.FaqCKMServiceImplPort();
        WS_Inquira.faqCKMCreateResponse objfaqCKMCreateResponse;
        String strFAQId='';
        String strmsg='';
        list<string> lstfaqidandmessage = new list<string>();
        final String INQUIRA_ENDPOINT = Label.CLMAY14CCC25;
        final String INQUIRA_CERTIFICATE = Label.CLMAY14CCC23;
        InquiraConnection.endpoint_x = INQUIRA_ENDPOINT;
        InquiraConnection.ClientCertName_x = INQUIRA_CERTIFICATE;
        InquiraConnection.timeout_x = Integer.valueOf(System.Label.CLMAY13CCC39);
        list <attachment> lstattachmenttobesend =new list<attachment>();
        
        if(Country!= null)
            Local.isoCountry = Country.toUpperCase();
        if(selectedLanguage!= null)      
            Local.isoLanguage = selectedLanguage.toLowerCase();
            
            if(attachmentid.size()>0){
                lstattachmenttobesend=[select id,body,name from attachment where id=:attachmentid[0].id];
                objFaqDataHandler.name=lstattachmenttobesend[0].name;
                objFaqDataHandler.file=EncodingUtil.base64Encode(lstattachmenttobesend[0].body);
                lstFaqDataHandler.add(objFaqDataHandler);
                objattachment.attachment = lstFaqDataHandler;
            }
            else
            {
               lstFaqDataHandler = new List<WS_Inquira.FaqDataHandler>();
            }
            
        String strAuthor = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        String strProductBU=Label.CLDEC12CCC12;
        String CustomerRequest  = (Title == null ? '' : Title.replaceAll('<[^>]+>',' '));
        //String AnswerToCustomer = (Answer == null ? '' : Answer.replaceAll('<[^>]+>',' '));
        String AnswerToCustomer =Answer;
        
        if(!Test.isRunningTest())        
        objfaqCKMCreateResponse = InquiraConnection.createFaq(CustomerRequest,AnswerToCustomer,Target,local, Cse.Subject, strAuthor, Cse.ProductFamily__c, strProductBU,objattachment); 
        else
        objfaqCKMCreateResponse =  Utils_DummyDataForTest.createDraftFaq(false,false);             
        if(objfaqCKMCreateResponse!=null){
            strFAQId = objfaqCKMCreateResponse.faqIdCreated;
            lstfaqidandmessage.add(strFAQId);
            strmsg = objfaqCKMCreateResponse.message;
            lstfaqidandmessage.add(strmsg);
        }
        if(attachmentid.size()>0){
            lstfaqidandmessage.add(''+attachmentid[0]);
        }
        return lstfaqidandmessage;
    }
    
    // Check in the SFDC Database what are the related FAQs
    public static set<String> CheckExistingFAQ(Case Cse)
    {
         set<string> ExistingFAQIDs = new set<String>();

         For(InquiraFAQ__c  ExistingFAQ:[SELECT id,Name,Date__c,Excerpt__c,Title__c,URL__c FROM InquiraFAQ__c WHERE Case__c=:Cse.Id])
         {
             ExistingFAQIDs.add(ExistingFAQ.Name);
         }

         return ExistingFAQIDs;
     }
     
        // Attach Inquira FAQs to the case field "Answer To Customer" field.
    public static void UpdateCaseWithInquiraFaQ(Case Cse)
    {
        //*********************************************************************************
        // Method Name : UpdateCaseWithInquiraFaQ
        // Purpose : To populate upto Answer to Customer for the Case
        // Created by : Ramesh Rajasekaran - Global Delivery Team
        // Date created : 24th February 2012
        // Modified by : Vimal K (GD India
        // Date Modified : 13 May 2014
        // Remarks : For Apr - 12 Release (Created)
        // Remarks : For Apr - 12 Release (Modified) To exclude Non Public FAQ 
        // while adding to Case
        ///*********************************************************************************/
        
        System.Debug('****AP12_CaseIntegrationMethods UpdateCaseWithInquiraFaQ Started****');
        
        String InquiraFaQContent = '';
        List<InquiraFAQ__c> TobeInsertedFAQs = new List<InquiraFAQ__c>();
        
        if(Cse.Id != null)
        //TobeInsertedFAQs = [Select Name,Excerpt__c,URL__c, Title__c from InquiraFAQ__c where Case__c =: Cse.Id]; 
        TobeInsertedFAQs = [Select Name,Excerpt__c,URL__c, Title__c from InquiraFAQ__c where Case__c =: Cse.Id and Visibility__c =: 'Public']; 
              
               
        if(Cse.AnswerToCustomer__c != null)
        InquiraFaQContent  = Cse.AnswerToCustomer__c+'<br />';
       
               
        if(TobeInsertedFAQs != null && TobeInsertedFAQs.size()>0)
        {        
            for(InquiraFAQ__c inqobj:TobeInsertedFAQs)
            {               
                if(InquiraFaQContent != null || InquiraFaQContent != '')
                 {  
                     if(!InquiraFaQContent.contains(inqobj.Name)) 
                     {                                      
                         InquiraFaQContent +=  'ID => '+inqobj.Name+ '<br />';   
                         InquiraFaQContent +=  'Title => '+inqobj.Title__c+ '<br />';   
                         //InquiraFaQContent +=  'Excerpt => '+inqobj.Excerpt__c+ '<br />';
                         InquiraFaQContent +=  'URL => '+(inqobj.URL__c).replace('&','&amp;')+ '<br /><br />';
                     }
                 }
            }
        }        
       
       
        If(InquiraFaQContent != '' && InquiraFaQContent.length() <= 32000)
        {
            Cse.AnswerToCustomer__c = InquiraFaQContent;  
            Database.SaveResult CasetoUpdate = Database.Update(Cse);         
            List<Database.Error> DMLError = CasetoUpdate.getErrors(); 
            System.Debug('#### DML Errors : ' + DMLError );                 
        }       
        else
        {
            Cse.addError(System.Label.CL00720);// Warning Message - Warning: The FAQ data could not be populated in the Case object due to insufficient data limit
        }
        System.Debug('****AP12_CaseIntegrationMethods UpdateCaseWithInquiraFaQ Finished ****');
    }      
}