/*
Controller for the VFP_ProjectTeamNewEdit to check the access of the current user
*/

public class VFC_ProjectTeamNewEditDelete {
    public boolean hasError{get;set;}
    public boolean hasAllAccess{get;set;}
    public OPP_ProjectTeam__c prjTeam{get;set;}{prjTeam=new OPP_ProjectTeam__c ();}
    public Id custId{get;set;}
    public Id prjTeamId{get;set;}{prjTeamId=null;}
    public String pgeMsg {get;set;}
    
     public VFC_ProjectTeamNewEditDelete (ApexPages.StandardController controller) {
         hasError=False;
         hasAllAccess=False;
         custId=System.currentPageReference().getParameters().get('custId');
         prjTeamId=System.currentPageReference().getParameters().get('Id');
         prjTeam=(OPP_ProjectTeam__c)controller.getRecord();
         if(custId!=null)
            prjTeam.Project__c=custId;
        if(prjTeamId!=null)
          prjTeam=[select TeamRole__c,User__c,Name,Project__c from OPP_ProjectTeam__c where Id=:prjTeamId];
     }
     
      public pageReference checkAccess(){
          System.debug('>>>>>>>>custId>>>>'+prjTeam.Project__c);
          System.debug('>>>>>>>>UserId>>>>'+UserInfo.getUserId() );
          List<UserRecordAccess> usac=[select HasAllAccess,RecordId from  UserRecordAccess where UserId=:UserInfo.getUserId() and RecordId=:prjTeam.Project__c];
          //UserAccessRecord for each user on Project you will find only one record
          if(usac.size()>0){
              if(usac[0].HasAllAccess==TRUE){
                  hasAllAccess=TRUE;
              }   
           else{
               hasError=True;
               ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.CLOCT15SLS70)); 
          }
        }
        else {
              hasError=True;
               ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,System.Label.CLOCT15SLS70)); 
         }     
         return null;
      }
      
      public pageReference deletePrjTeam(){
       checkAccess();
       if(hasError!=True){
            Database.DeleteResult recIns= Database.delete(prjTeam, false);
            if(!recIns.isSuccess()){
                hasError=True;
                pgeMsg = recIns.getErrors()[0].getMessage();
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,pgeMsg)); 
                
            }   
            else{
                 hasError=False;
                Pagereference pageRef = new PageReference('/'+prjTeam.Project__c) ;   
                System.debug('>>>>>#####>>>>>'+pageRef );            
                pageRef.setRedirect(true);
                 return pageRef;             
               }
        }
        return null;
      }
      
      public pageReference Save(){
        Database.upsertResult recIns= Database.Upsert(prjTeam, false);
        if(!recIns.isSuccess()){
            hasError=True;
            pgeMsg = recIns.getErrors()[0].getMessage();
            //Below code is written since the Visualforce Page is not displaying error message same as in standard layout for duplicate record error.
            if(pgeMsg.Contains('duplicate value found:')){
                String ProjectId = [Select Name from OPP_ProjectTeam__c where Id=:pgeMsg.right(15) limit 1].Name;
                pgeMsg=System.Label.CLOCT15SLS69+'<b><a href='+Site.getPathPrefix()+'/'+pgeMsg.right(15)+'>'+ProjectId +'</a></b>';
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,pgeMsg)); 
           }
            else
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.FATAL,pgeMsg)); 
            
        }   
        else{
             hasError=False;
              return new ApexPages.StandardController(prjTeam).view();
           }
        return null;
      }

      public pagereference saveAndNew()
    {
        Save();
        if(hasError!=True){
            System.debug('>>>>>#####Save and new loop');
            Pagereference pageRef = new PageReference(Site.getPathPrefix()+'/apex/VFP_ProjectTeamNewEdit?custId='+prjTeam.Project__c+'&retURL=%2F'+prjTeam.Project__c) ;   
            System.debug('>>>>>#####>>>>>'+pageRef );            
            pageRef.setRedirect(true);
             return pageRef; 
         }
        return null;
    }
}