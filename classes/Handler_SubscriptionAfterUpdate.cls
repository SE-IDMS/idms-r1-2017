public class Handler_SubscriptionAfterUpdate {
    public class MyException extends Exception{}
    public static Zuora_Request_Soap_Handler.zuoraHandler zh;
    public static String sessionId;
    
    @future(callout=true)
    public static void BeginZuoraSubscriptionFlow(Set<String> subsIds) {
        if (subsIds.size()>0){
            zh = new Zuora_Request_Soap_Handler.zuoraHandler(null,null,null);
            zh = Zuora_Request_Soap_Handler.loginToZuora();
            try{
                if (zh.id==null)
                    throw new MyException(zh.message);
            }catch (MyException ex){
                Helper_SendingErrorEmail.sendNotificationEmail('Subscription', ex.getMessage());
            }
            sessionId = zh.id;
            updateSubsDate(subsIds); 
        }                
    }
    
    public static void updateSubsDate(Set<String> SubsIds){
        list<Zuora__Subscription__c> subsList = [SELECT ID, Zuora__ServiceActivationDate__c, Zuora__Zuora_Id__c FROM Zuora__Subscription__c WHERE ID IN:subsIds];
        for (Zuora__Subscription__c sub :subsList){
            String current_time = String.valueOf(datetime.now().time()).substring(0,8);
            //String Service_Activation_Date = String.valueof(sub.Zuora__ServiceActivationDate__c)+'T'+current_time+'-08:00';
            String Service_Activation_Date = String.valueOf(datetime.newInstance(sub.Zuora__ServiceActivationDate__c, time.newInstance(0,0,0,0)).format('yyyy-MM-dd'));
            zh = Zuora_Request_Soap_Handler.updateZuoraSubscription(sessionId, sub.Zuora__Zuora_Id__c, Service_Activation_Date,'ServiceActivationDate');
            try{
                if (zh.id==null)
                    throw new MyException(zh.message);
            }catch (MyException ex){
                Helper_SendingErrorEmail.sendNotificationEmail('Subscription', ex.getMessage());
            }
             
        }
    }
    /**
     * @param newSubscriptionToUpdate [list of new subscriptions that was inserted.]
     * [update quote subscription field after insert new subscriptions  .]
     * @createdBy   Ester Mendelson, 13/07/2015         
     * @lastModifiedBy  Ester Mendelson, 13/07/2015 
     */
     public static void updateZquoteSubscription(List<Zuora__Subscription__c> newSubscriptionToUpdate){
        system.debug('Kerensen in updateZquoteSubscription'+newSubscriptionToUpdate);
        Map<String,Zuora__Subscription__c> subByIdMap = new Map<String,Zuora__Subscription__c> ();
        for(Zuora__Subscription__c sub : newSubscriptionToUpdate){
            subByIdMap.put(sub.Zuora__Zuora_Id__c,sub);
        }
        List<zqu__Quote__c> quoteListToUpdateSubscription=[SELECT ID, Subscription__c ,Zqu__ZuoraSubscriptionID__c FROM zqu__Quote__c WHERE zqu__Status__c = 'Sent to Z-Billing' AND zqu__ZuoraSubscriptionID__c IN :subByIdMap.keySet()];
        for(zqu__Quote__c quote :quoteListToUpdateSubscription){
     
            if(subByIdMap.get(quote.zqu__ZuoraSubscriptionID__c).Id != Null){
                quote.Subscription__c = subByIdMap.get(quote.zqu__ZuoraSubscriptionID__c).Id;
            }
        }
        if(quoteListToUpdateSubscription.size() >0){
            update quoteListToUpdateSubscription;
        }
    }
    /**
     * @param newSubscriptionToUpdate [list of  subscriptions that was update.]
     * [update quote subscription field after update  subscriptions  .]
     * @createdBy   Ester Mendelson, 27/08/2015         
     * @lastModifiedBy  Ester Mendelson, 27/08/2015 
     */
    public static void updateZquoteExistingSubscription(List<Zuora__Subscription__c> newSubscriptionToUpdate){
        system.debug('Kerensen in updateZquoteSubscription'+newSubscriptionToUpdate);
        Map<String,Zuora__Subscription__c> subByNameMap = new Map<String,Zuora__Subscription__c> ();
        for(Zuora__Subscription__c sub : newSubscriptionToUpdate){
             subByNameMap.put(sub.Name,sub);
        }
        List<zqu__Quote__c> quoteListToUpdateSubscription =[SELECT ID, Subscription__c ,Zqu__ZuoraSubscriptionID__c, Zqu__ExistSubscriptionID__c, zqu__Subscription_Name__c FROM zqu__Quote__c WHERE zqu__Status__c = 'Sent to Z-Billing' AND  zqu__Subscription_Name__c IN :subByNameMap.keySet() AND Subscription__c =: Null];
        for(zqu__Quote__c quote :quoteListToUpdateSubscription){
                quote.Subscription__c =  subByNameMap.get(quote.zqu__Subscription_Name__c).Id;
        }
        if(quoteListToUpdateSubscription.size() >0){
            update quoteListToUpdateSubscription;
        }
    }
/**
     * @param newSubscriptionToUpdate with ASSET []
     * [update subscription field Asset after update  subscriptions  .]
     * @createdBy       JP Levasseur, 08/02/2016
     * @lastModifiedBy  JP Levasseur, 11/04/2016
     */
    public static void updateSubscriptionAsset(Set<String> subsIds){
        system.debug('updateSubscriptionAsset : ' + subsIds);
        
        //1 - Query on Subscriptions
        List<Zuora__Subscription__c> ListSub =[SELECT Id, Name, Asset__c, Asset_SKU__c, Asset_SN__c
                                               FROM   Zuora__Subscription__c
                                               WHERE  Id IN :subsIds];
        system.debug('***** >> ListSub = ' + ListSub);
        
        //2 - Query on Installed Products (Assets), based on Asset_SKUs and Asset_SNs
        Set<String> Asset_SKUs = new Set<String>();
        Set<String> Asset_SNs  = new Set<String>();
        
        for (Zuora__Subscription__c aSub : ListSub) {
           Asset_SKUs.add(aSub.Asset_SKU__c);

           //April 2016 Release : Asset_SN__c should contain Serial Number + SKU when received from Zuora
           //                     Need to remove the SKU at the end of this string
           //                     If SKU is not found, Asset_SN__c is returned (managed by the removeEnd() function)
           Asset_SNs.add(aSub.Asset_SN__c.removeEnd(aSub.Asset_SKU__c));
        }
        system.debug('***** >>Asset_SKUs = ' + Asset_SKUs);
        system.debug('***** >>Asset_SNs = ' + Asset_SNs);
        
        List<SVMXC__Installed_Product__c> ListAssets =[SELECT Id, SVMXC__Product__r.Name, SVMXC__Serial_Lot_Number__c
                                                       FROM   SVMXC__Installed_Product__c
                                                       WHERE  SVMXC__Product__r.Name IN :Asset_SKUs
                                                       AND    SVMXC__Serial_Lot_Number__c IN :Asset_SNs];
        system.debug('***** >> ListAssets = ' + ListAssets);

        //3 - Create a map of Assets
        Map<String,Id> mapAssets = new Map<String,Id>();

        for(SVMXC__Installed_Product__c anAsset : ListAssets)
        {
           mapAssets.put(anAsset.SVMXC__Serial_Lot_Number__c + anAsset.SVMXC__Product__r.Name,anAsset.Id);
        }
        system.debug('***** >> mapAssets = ' + mapAssets);

        //4 - Update Subscriptions
        for (Zuora__Subscription__c aSub : ListSub) {
           system.debug('***** >> aSub before = ' + aSub);
           
           //April 2016 Release : Asset_SN__c should already contain Serial Number + SKU when received from Zuora
           //                     No need to add the SKU at the end of this string
           if(mapAssets.containsKey(aSub.Asset_SN__c)) {
              aSub.Asset__c = mapAssets.get(aSub.Asset_SN__c);
              system.debug('***** >> aSub after = ' + aSub);
           }
        }
        
        if(ListSub.size() >0){
            update ListSub;
        }
    }
}