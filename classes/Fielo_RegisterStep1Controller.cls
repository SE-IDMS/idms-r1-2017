/************************************************************
* Developer: Elena J. Schwarzböck                           *
* Type: VF Page Controller                                  *
* Page: Fielo_RegisterStep1                                 *
* Created Date: 14/08/2014                                  *
************************************************************/
global with sharing class Fielo_RegisterStep1Controller{
    
    public PageReference termsConditionsPage {get; set;}
    public Boolean acceptTermsConditions {get;set;}
    public List<String> listMessages {get;set;}
    public String selectedCountry {get;set;}
    public Boolean isSuccessful {get;set;}
    
    private Id idReferral;
    
    //CONSTRUCTOR
    public Fielo_RegisterStep1Controller(){
        termsConditionsPage = Fielo_Utilities.getMenuReference2('Terms & Conditions');
    }
    
    private FieloEE.RegisterStep1Controller controller;
    public Fielo_RegisterStep1Controller(FieloEE.RegisterStep1Controller controller){
        this.controller = controller;
        
        acceptTermsConditions = false;
        idReferral = ApexPages.currentPage().getParameters().get('rid');
        
        try{
            controller.member.F_Country__c = [SELECT Id, Name FROM Country__c WHERE Name = 'United Arab Emirates'].Id;
            selectedCountry = controller.member.F_Country__c;
            controller.member.F_PhoneExtension__c = '+971';
            controller.member.F_City__c = 'Dubai';
            controller.member.F_CorporateHeadquarters__c = true;
        }catch(Exception e){}
        
        listMessages = new List<String>();
        termsConditionsPage = Fielo_Utilities.getMenuReference2('Terms & Conditions');
    }
    
    public PageReference doToMenu(){
        PageReference pageRet;
        if(!ApexPages.currentPage().getParameters().containsKey('idMenu')){
            pageRet = Fielo_Utilities.getMenuReference('Register_Now');
            pageRet.getParameters().put('rid', ApexPages.currentPage().getParameters().get('rid'));
        }
        return pageRet;
    }
    
    public List<SelectOption> getCountryValues(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));     
        for(Country__c countryAdded: [SELECT Id, Name FROM Country__c ORDER BY Name]){
            options.add(new SelectOption(countryAdded.Id, countryAdded.Name));
        }         
        return options;
    }
    
    public pageReference doSubmit(){
        
        listMessages.clear();
        
        //se valida por aca que se ingrese el country porque no funciona con el required=true desde la pagina y es un campo obligatorio
        if(selectedCountry == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Fielo_RegisterStep1ControllerEnterACountry));
            loadlistMessages();
            return null;        
        }
        
        if(controller.member.F_BusinessType__c == 'SC' && controller.member.F_PurchaseSEProductsBefore__c == null){
            controller.member.F_PurchaseSEProductsBefore__c.addError('Please let us know if you have previously bought our products');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please let us know if you have previously bought our products'));
            loadlistMessages();
            return null;
        }
        
        //se valida que una misma compania no pueda tener mas de un member/usuario
        List<FieloEE__Member__c> companyMember = [SELECT Id, F_CompanyName__c FROM FieloEE__Member__c WHERE F_CompanyName__c =: controller.member.F_CompanyName__c LIMIT 1];
        if(!companyMember.isEmpty()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Fielo_RegisterStep1ControllerCompRegistered));
            loadlistMessages();
            return null;
        }
        
        if(!acceptTermsConditions){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Fielo_RegisterStep1ControllerAcceptTermsAndCond));
            loadlistMessages();
            return null;                       
        }else{
            //si se selecciono como pais "United Arab Emirates" es obligatorio cargar una City
            if(selectedCountry == 'United Arab Emirates' && controller.member.F_City__c == null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, Label.Fielo_RegisterStep1ControllerEnteraCity));
                loadlistMessages();
                return null;
            }
            
            //este paso es provisorio para evitar la validation rule sobre ACC_VR10_StateProvinceMandatory que se dispara
            //al sacar este campo del formulario. Falta definir como se va a solucionar.                
            List<StateProvince__c> stateProvice = [SELECT Id, Name FROM StateProvince__c WHERE Country__c =: selectedCountry LIMIT 1];              
            if(!stateProvice.isEmpty()){controller.member.F_StateProvince__c = stateProvice[0].Id;}
            controller.member.F_Country__c = selectedCountry;
            
            //si por parametro llega el id del member que lo recomendo se completa el campo FieloEE__ReferalMember__c
            if(idReferral != null){controller.member.FieloEE__ReferalMember__c = idReferral;}
            
            FieloEE__Program__c prog = FieloEE.ProgramUtil.getProgramByDomain();
            if(prog != null){
                controller.member.FieloEE__Program__c = prog.Id;
            }
            
            try{
                insert controller.member;
                controller.registered = true;
            }catch(DmlException dmle){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, dmle.getMessage()));
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
            }
            
            if(controller.registered == true){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, Label.Fielo_RegisterStep1ControllerEmailSent));
                isSuccessful = true;
            }
            
            loadlistMessages();
        }
        return null;    
    }
    
    private void loadlistMessages(){
        for(ApexPages.Message mes: ApexPages.getMessages()){
            listMessages.add(mes.getSummary());
        }
    }
    
    @RemoteAction
    global static String doAssignPhoneExtension(String countryId){
        String exten = '';
        String countryName = [SELECT Id, Name FROM Country__c WHERE Id =: countryId].Name;
        
        if(countryName == 'United Arab Emirates'){
            exten = '+971';
        }else if(countryName == 'Bahrain'){
            exten = '+973';   
        }else if(countryName == 'Kuwait'){
            exten = '+965'; 
        }else if(countryName == 'Oman'){
            exten = '+968'; 
        }else if(countryName == 'Qatar'){
            exten = '+974';
        }
        return exten;
    }
    
    @RemoteAction
    global static String doBusinessType(String bType){
        string isInstaller = 'false';
        if(bType == 'Installer'){
            isInstaller = 'true';
        }
        return isInstaller;
    }
    
}