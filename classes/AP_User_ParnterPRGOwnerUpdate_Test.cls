/*
    Author          : Srinivas Nallapati  
    Date Created    : 02-May-2013
    Description     : Test class for AP_User_ParnterPRGOwnerUpdate
*/

@isTest
private class AP_User_ParnterPRGOwnerUpdate_Test
{
    static testMethod void testPRMAdminUsers() 
    {
        List<Profile> profiles = [select id from profile where name='SE - Marcom Lead Admin'];
        if(profiles.size()>0)
        {
            User usr = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsrAP19');
            usr.ProgramOwner__c = true;
            usr.ProgramApprover__c  = true;
            insert usr;
            
            usr.ProgramOwner__c = false;
            usr.ProgramApprover__c  = false; 
            update usr;
            
            usr.ProgramOwner__c = true;
            usr.ProgramApprover__c  = true; 
            update usr;
            
            
            
        }      
        
    }

}