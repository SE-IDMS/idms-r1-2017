/*
    Author          : Accenture IDC Team 
    Date Created    : 21/07/2011
    Description     : Class utilised by Mass Delete Button.
*/
public class VFC48_MassDelete {
    public Boolean flag{get;set;}
    public string url;
    public List<SFE_PlatformingScoring__c> scoring = new List<SFE_PlatformingScoring__c>();
    public SFE_IndivCAP__c cap= new SFE_IndivCAP__c();

        public VFC48_MassDelete(ApexPages.StandardSetController controller) {    
        scoring=controller.getSelected();   
        url=ApexPages.currentPage().getParameters().get(system.label.CL00330); 
        }
        
     //Method performs mass deletion of selected Platforming records
              
        public pagereference deletePlatformings()
        {  
        system.debug('DeletePlatforming Method Begins');         
        string capId=url.substring(1,url.length()); 
        if(capID!=null)
        {
            cap=[select AssignedTo__c,ownerID,Status__c from SFE_IndivCAP__c where id =:capId];            
        }
            if(scoring.size()>0)
            {   
                  if(!cap.status__c.equalsIgnoreCase(Label.CL00444))
                  {           
                    if((UserInfo.getUserId()!=cap.ownerID)&&(UserInfo.getUserId()!=cap.AssignedTo__c) &&(UserInfo.getProfileId()!=Label.CL00110) &&(UserInfo.getProfileId()!=Label.CLOCT13SLS24)&&(UserInfo.getProfileId()!=Label.CLOCT13SLS25))
                    {                
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00440)); 
                        flag=true;
                        return null;
                    } 
                    else
                    {          
                         if(scoring.size() + Limits.getDMLRows() > Limits.getLimitDMLRows())
                            {
                                    System.Debug('######## VFC48 Error Insert: '+ Label.CL00264);
                                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00264)); 
                            }
                            else
                            {                     
                                Database.deleteResult[] VFC48_MassDelete = Database.delete(scoring); 
                                for(Database.deleteResult dr: VFC48_MassDelete)
                                {
                                    if(!dr.isSuccess())
                                    {
                                    Database.Error err = dr.getErrors()[0];                    
                                    }
                    
                                 }
                             }
                     return new pagereference(url);                           
                    }      
                }  
                else
                {            
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00441)); 
                    flag=true;
                    return null;
                }     
            }        
            else
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Warning,Label.CL00442));
                flag=true;
                return null;
            }
        }
           
}