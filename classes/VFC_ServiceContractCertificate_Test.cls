@isTest(seealldata=true)
private class VFC_ServiceContractCertificate_Test 
{ 
    public static testmethod void test()
    {
        try{
         
            SVMXC__Service_Contract__c sc= new SVMXC__Service_Contract__c();
            sc.name='test';
            insert sc;
            system.assert(sc != null);

            PageReference pageRef1 = Page.VFP_ServiceContractCertificate; //('page.VFP_ServiceContractCertificate?id='+'&DispTYP=pdf'+sc.Id);
            Test.setCurrentPage(pageRef1);
            
            system.currentPageReference().getParameters().put('SId',sc.id);
           
             
            VFC_ServiceContractCertificate controller2 = new VFC_ServiceContractCertificate();
  
            controller2.getisInsert();
            controller2.setstrDispTyp(null);
            controller2.getstrDispTyp();
            controller2.setstrDispTyp('pdf');
            controller2.setstrDispTyp('doc');
             controller2.getstrDispTyp();
            controller2.setstrDispTyp('xls');
            controller2.getstrDispTyp();
            controller2.getstrReportType();
            controller2.getAttachFile();
            controller2.setAttachFile(true);
            controller2.getSId();
             controller2.getList();
            controller2.getMMDetail();
            controller2.getPageName();
            controller2.setPageName('VFP_ServiceContractCertificate');
             controller2.AttachFile = true;
            controller2.strReportType = 'SingleRecord';
           
            Map<String, SObjectField> fieldMap = Schema.SObjectType.SVMXC__Service_Contract__c.fields.getMap();
            VFC_ServiceContractCertificate.getFieldList(fieldMap, false);
           	

            controller2.ObjName = 'SVMXC__Service_Contract__c';
            controller2.getItems();
      
        }
        catch(exception ex){Apexpages.addMessage(new ApexPages.Message ( ApexPages.Severity.ERROR,ex.getmessage()));}
        
    }
    public static testmethod void test1()
    {
        
         
            SVMXC__Service_Contract__c sc1= new SVMXC__Service_Contract__c();
            sc1.name='test';
            insert sc1;
            system.assert(sc1 != null);

            PageReference pageRef2 = Page.VFP_ServiceContractCertificate; //('page.VFP_ServiceContractCertificate?id='+'&DispTYP=pdf'+sc.Id);
            Test.setCurrentPage(pageRef2);
            
            VFC_ServiceContractCertificate controller3 = new VFC_ServiceContractCertificate();
  
            controller3.Cancel();
        } 
  public static testmethod void test2()
    {
     
            SVMXC__Service_Contract__c sc4= new SVMXC__Service_Contract__c();
            sc4.name='test';
            insert sc4;
            system.assert(sc4 != null);

            PageReference pageRef5 = Page.VFP_ServiceContractCertificate; //('page.VFP_ServiceContractCertificate?id='+'&DispTYP=pdf'+sc.Id);
            Test.setCurrentPage(pageRef5);
            
            VFC_ServiceContractCertificate controller8 = new VFC_ServiceContractCertificate();
  
            controller8.Generate();
        }    

 public static testmethod void test3()
    {
     
            SVMXC__Service_Contract__c sc9= new SVMXC__Service_Contract__c();
            sc9.name='test';
            insert sc9;
            system.assert(sc9!= null);

            PageReference pageRef7 = Page.VFP_ServiceContractCertificate; //('page.VFP_ServiceContractCertificate?id='+'&DispTYP=pdf'+sc.Id);
            Test.setCurrentPage(pageRef7);
            
            VFC_ServiceContractCertificate controller7 = new VFC_ServiceContractCertificate();
  
            controller7.getREPTAG001();
        	controller7.getREPTAG002();
        	controller7.getREPTAG003();
        	controller7.getREPTAG004();
        	controller7.getREPTAG005();
        	controller7.getREPTAG006();
        	//controller7.getREPTAG007();
        	//controller7.getREPTAG008();
        
        
        }        
                
            
}