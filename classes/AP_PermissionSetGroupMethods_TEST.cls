@isTest
public class AP_PermissionSetGroupMethods_TEST {

    static testMethod void testGroupMethods() {
        
        Profile profile = [SELECT Id from profile WHERE Name='System Administrator'];  
        User adminUser  = [SELECT Id FROM User WHERE ProfileId = :profile.Id AND isActive = true LIMIT 1];

        System.runAs (adminUser) {
        
            Test.startTest();

            Integer max = 5;
            String randomString = EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(1,max).toUpperCase();

            List<PermissionSetGroup__c> permissionSetGroupList =  permissionTestMethods.createPermissionSetGroups();        
            permissionSetGroupList.get(0).License__c = 'SVMX';
            update permissionSetGroupList.get(0);
            
            ApexPages.StandardController userController = new ApexPages.StandardController(adminUser);
            
            VFC_PermissionSetAssignment viewGroupPermissionController = new VFC_PermissionSetAssignment(userController);        
                        
            viewGroupPermissionController.searchName            = permissionSetGroupList.get(0).Name;
                        
            viewGroupPermissionController.search();
            
            ApexPages.currentPage().getParameters().put('selectId', permissionSetGroupList.get(0).Id);
            
            viewGroupPermissionController.selectGroup();

            viewGroupPermissionController.saveAssignment();

            viewGroupPermissionController = new VFC_PermissionSetAssignment(userController);        
            ApexPages.currentPage().getParameters().put('removeId', permissionSetGroupList.get(0).Id);
            viewGroupPermissionController.removeGroup();
            viewGroupPermissionController.saveAssignment();
                                    
            Test.stopTest();
        }
    } 
}