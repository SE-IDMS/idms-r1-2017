/*
Test Class for Milestone1_Month
April 2015 Release
Divya M 
*/
@isTest
public class Milestone1_Month_Test {
    static testMethod void testMe() {
        Milestone1_Project__c testProject = Milestone1_Test_Utility.sampleProject('UNIT TEST PROJECT NAME ABC123XYZ UNIQUE' + System.now());
        testProject.Status__c = 'Active';
        testProject.Deadline__c = Date.today();
        insert testProject;
        
        Milestone1_Milestone__c testMilestone = Milestone1_Test_Utility.sampleMilestone(testProject.Id,null,'UNIT TEST MILESTONE NAME ACB123XYZ UNIQUE' + System.now());
        testMilestone.Deadline__c = Date.today();
        testMilestone.Complete__c = false;
        insert testMilestone;

        Milestone1_Task__c testTask = Milestone1_Test_Utility.sampleTask(testMilestone.Id);
        testTask.Complete__c = false;
        testTask.Start_Date__c = Date.today().addDays(-1);
        testTask.Due_Date__c = Date.today().addDays(-1);
        
        insert testTask;
        
        Milestone1_Month m = new Milestone1_Month( Date.today());
        m.getValidDateRange();
        m.clearEvents();
        m.getCurrentWeek(Date.today());
        //integer int1 = m.getWeekNumber();
        //Date dt = m.getStartingDate();
        
        //system.assert(m!=null); 
        list<Milestone1_Month.Week> l = m.getWeeks(); 
        for(Milestone1_Month.Week w : l)
        {
            List<Milestone1_Month.Day> days = w.getDays();
            for(Milestone1_Month.Day d : days)
            {
                system.debug(d.getDayOfYear());
                system.debug(d.getDayOfMonth());
                system.debug(d.getDayNumber());
                system.debug(d.getCSSName());
            }
        }
        system.debug(m.getFirstDate());
        system.debug(m.getWeekdayNames());
        system.debug(m.getYearName());
        system.debug(m.getMonthName());
       // system.assert(m.getWeeks() != null);
       // system.assert(m.getWeeks().size() > 3);
        Milestone1_Month.Day mmday = new Milestone1_Month.Day(Date.today(),4);
        date dt1 = mmday.getDate();
        string str = mmday.getDayOfMonth2();
        List<Milestone1_Calendar_Item> lst = mmday.getDayAgenda();
        string str2 = mmday.getFormatedDate();
        m.setEvents(lst);
        List<Milestone1_Calendar_Item>  lst2 = mmday.getEventsToday();
    } 
}