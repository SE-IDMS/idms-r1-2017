/**************************
 Created By-Deepak
 Created Date- 07-04-2015
 **************************/
@isTest(SeeAlldata=true)
private class AP_OpportunityCreationFromIB_TEST {
 
    public static testMethod void myUnitTest() {
        String Amendment = System.Label.CLAPR15SRV26;
        String ServiceLineRT = System.Label.CLAPR15SRV02;
         User us = CreateUser();
         system.runAs(us)
         {
        //A country 
        //Country__c testCountry = [select id from Country__c where CountryCode__c =: 'FR'];
        //Country__c testCountry = Utils_TestMethods.createCountry(); // commented by suhail 
        //Routing Data created for Country, State and Zip
        
        OptyOwnerRoutingRule__c RuleUSAZZip = new OptyOwnerRoutingRule__c();
        RuleUSAZZip.Name = 'USAZZip';
        RuleUSAZZip.Business_Unit__c = 'IT';
        RuleUSAZZip.Country__c = 'US';
        RuleUSAZZip.StateProvince__c = 'AZ';
        RuleUSAZZip.RoutingRule__c = 'Postalcode';
        RuleUSAZZip.Person__c = us.id;
        RuleUSAZZip.OpportunityType__c = 'EOW';
        //RuleUSAZZip.CurrencyIsoCode = 'INR - Indian Rupee';
        RuleUSAZZip.ZipCodeMin__c = '100';
        RuleUSAZZip.ZipCodeMax__c = '200';
        insert RuleUSAZZip;
        
        country__c countryUS = [Select name,id,countrycode__C from country__C where countrycode__c =: 'US'];
        StateProvince__c stateUSAZ = [Select name,id,countrycode__C,Country__c,StateProvinceCode__c from StateProvince__c where StateProvinceCode__c =: 'AZ' and Country__c = :countryUS.id];
        
        Account accountUSAZ = Utils_TestMethods.createAccount();
        accountUSAZ.RecordTypeId  = System.Label.CLOCT13ACC08;
        accountUSAZ.Country__c= countryUS.id;
        accountUSAZ.StateProvince__c = stateUSAZ.id;
        accountUSAZ.LeadingBusiness__c = 'IT';
        accountUSAZ.ZipCode__c = '105';
        accountUSAZ.OwnerId = us.id;
        accountUSAZ.Street__c='New Street';
        accountUSAZ.Name='TestAccountUSAZ with Zip';
        insert accountUSAZ;
        
        /*Country__c testcountry = new Country__c(); // added by suhail 
        testCountry.name = 'testcounrtysuhail1'; // added by suhail 
        testCountry.CountryCode__c = 'X1' ;  // added by suhail */
        
        
        //insert testCountry;
      //User 
        
       
        /*User us = new User(alias = 'user', email='user' + '@accenture.com', 
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                             localesidkey='en_US', profileid = System.label.CLDEC14SRV01, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX05;AP54;AP_Contact_PartnerUserUpdate',
                             timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345',Company_Postal_Code__c='12345',Company_Phone_Number__c='9986995000');
        UserRole uRole = [Select Id,Name,DeveloperName From UserRole where DeveloperName = 'CEO'];
        us.UserRoleId = uRole.Id;
        insert us;*/
        //An Account     
        country__c testCountry = [Select name,id,countrycode__C from country__C where countrycode__c =: 'IE'];
        Account testAccount = Utils_TestMethods.createAccount(us.Id,testCountry.id);
    
        testAccount.MarketSegment__c='ID5';
        testAccount.MarketSubSegment__c='B43';
        //testAccount.BillingCountryCode='IN'; // changed from IN to TE
        //testAccount.BillingCountry='India' ;
        //testAccount.BillingState='Kerala';
        insert testAccount;
        
        Account testAccount1 = Utils_TestMethods.createAccount(us.Id,testCountry.id);
    
        testAccount1.MarketSegment__c='ID5';
        testAccount1.MarketSubSegment__c='B43';
        testAccount1.type = 'SA';
        testAccount1.PrimaryRelationshipLeader__c = 'Field Service';
        //testAccount.BillingCountryCode='IN'; // changed from IN to TE
        //testAccount.BillingCountry='India' ;
        //testAccount.BillingState='Kerala';
        insert testAccount1;
         /*      
        //A Contact              
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id , 'TestContact');
        testContact.Country__c = testCountry.Id;
        insert testContact;*/
        AccountTeamMember ATM1 = new AccountTeamMember(AccountId = testAccount1.id,UserId = us.id,TeamMemberRole = 'V1SR');
        insert ATM1;
      
        SVMXC__Site__c site1 = new SVMXC__Site__c();
        site1.Name = 'Test Location';
        site1.SVMXC__Street__c  = 'Test Street';
      
        site1.SVMXC__Account__c = testAccount.id;
        site1.PrimaryLocation__c = true;
        insert site1;
        
        SVMXC__Site__c site2 = new SVMXC__Site__c();
        site2.Name = 'Test Location';
        site2.SVMXC__Street__c  = 'Test Street';
      
        site2.SVMXC__Account__c = testAccount1.id;
        site2.PrimaryLocation__c = true;
        insert site2;
        
        SVMXC__Site__c site3 = new SVMXC__Site__c();
        site3.Name = 'Test Location1';
        site3.SVMXC__Street__c  = 'Test Street1';
      
        site3.SVMXC__Account__c = accountUSAZ.id;
        site3.PrimaryLocation__c = true;
        insert site3;
        
        //OPP_Product__c Pro_GDP = [select id from OPP_Product__c where BusinessUnit__c = 'INFOR TECHNO. BUSINESS' limit 1];
        OPP_Product__c Pro_GDP = new OPP_Product__c();                                             
       
        Pro_GDP.Family__c = 'family';
        Pro_GDP.ProductFamily__c = 'productFamily';
        Pro_GDP.ProductLine__c = 'productLine';
        Pro_GDP.HierarchyType__c = 'PM0-FAMILY';
        Pro_GDP.IsActive__c = true;
        //Pro_GDP.BusinessUnit__c = 'EnergyX1'; //added by suhail 
        Pro_GDP.BusinessUnit__c = 'INFOR TECHNO. BUSINESS'; 
        insert Pro_GDP;
        
        
        Product2 newprod = new Product2();                                                
        newprod.Name                        = 'commercialReference1';
        newprod.ProductCode                 = 'strMaterialDescription_xxxx';
        newprod.CommercialStatus__c         = '08';
        newprod.CommercialReference__c      = 'commercialReferencePPP';     
        newprod.ProductGDP__c               = Pro_GDP.Id;
        newprod.IsActive                    = true;
        newprod.RenewableService__c         =true; 
        insert newprod;
        
        //Custom Setting
        
        CS_BUCountry__c csobj = new CS_BUCountry__c();
        csobj.BatchType__c='EOW';
        csobj.BusinessUnit__c='ENERGY'; //changed from it to Energy by suhail
        csobj.CountryCode__c='IN';  // uncommented by suhail
        csobj.Name='XTY';

        insert csobj;
        
        CS_BUCountry__c csobj2 = [select BatchType__c,BusinessUnit__c,CountryCode__c,Name from CS_BUCountry__c where CountryCode__c = 'US' and BusinessUnit__c = 'INFOR TECHNO. BUSINESS' limit 1];
        /*  CS_BUCountry__c csobj2 = new CS_BUCountry__c();
        csobj2.BatchType__c='EOW';
        csobj2.BusinessUnit__c='Energy'; //changed from it to Energy by suhail
        csobj2.CountryCode__c='X1';  // uncommented by suhail
        csobj2.Name='XTY2';

        insert csobj2;*/
        
        CS_ProductLineBusinessUnit__c plbu = new CS_ProductLineBusinessUnit__c();
        plbu.BUCode__c = 'EN';
        plbu.ProductBU__c='EnergyX1'; //changed from INFOR TECHNO. BUSINESS to EnergyIN by suhail 
        plbu.ProductLine__c='IT3PH - 3-PHASE POWER EQUIPMEN';
        plbu.Name='Test1';
        insert plbu;
        
        List<SVMXC__Installed_Product__c> applist = New List<SVMXC__Installed_Product__c>();
        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = testAccount.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId1';
        ip1.BrandToCreate__c ='Test Brand';
        ip1.SVMXC__Site__c = site1.id;
        ip1.DeviceTypeToCreate__c = 'device1';
        ip1.LifeCycleStatusOfTheInstalledProduct__c = system.label.CLAPR14SRV05;
        ip1.DecomissioningDate__c = date.today();
        //ip1.AssetCategory2__c =system.label.CLAPR14SRV01;
        ip1.SVMXC__Serial_Lot_Number__c = '1234';
        ip1.SVMXC__Product__c=newprod.Id;
        ip1.SVMXC__Warranty_Start_Date__c= date.Today()-121;
        ip1.SVMXC__Warranty_End_Date__c= date.Today()+130;
        ip1.ObsolescenceOpportunityGenerated__c =false;
        ip1.EndOfWarrantyOpportunityGenerated__c =false;
        ip1.AssetCategory2__c='Category 2' ;
        ip1.UnderContract__c =false;
      
        
        
        ip1.Tech_IpAssociatedToSC__c = false;
        
        
        insert ip1;
        system.debug('UnderContract__c\n i1:'+ip1.UnderContract__c);
        system.debug('AssetCategory2__c \n i2:'+ip1.AssetCategory2__c);
        system.debug('EndOfWarrantyOpportunityGenerated__c\n i3:'+ip1.EndOfWarrantyOpportunityGenerated__c);
        system.debug('Tech_IpAssociatedToSC__c \n i4:'+ip1.Tech_IpAssociatedToSC__c );
        system.debug('SVMXC__Product__c\n i5:'+ip1.SVMXC__Product__c);
        
        System.debug('Tech_CheckForEndOfWarranty__c :'+ip1.Tech_CheckForEndOfWarranty__c );
        System.debug('EndOfWarrantyOpportunityTriggered__c:'+ip1.EndOfWarrantyOpportunityTriggered__c);
        
        SVMXC__Installed_Product__c testrec=[select EndOfWarrantyOpportunityTriggered__c,SVMXC__Warranty_End_Date__c,Tech_CheckForEndOfWarranty__c  from SVMXC__Installed_Product__c where ID=:ip1.Id];
        System.debug('>>>>>>>>testrec>>>'+testrec.EndOfWarrantyOpportunityTriggered__c+'>>>>>>><<<<<'+testrec.SVMXC__Warranty_End_Date__c+'>><<>><<'+testrec.Tech_CheckForEndOfWarranty__c  );
        
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c();
        ip2.SVMXC__Company__c = testAccount1.id;
        ip2.Name = 'Test Intalled Product ';
        ip2.SVMXC__Status__c= 'new';
        ip2.GoldenAssetId__c = 'GoledenAssertId2';
        ip2.BrandToCreate__c ='Test Brand';
        ip2.SVMXC__Site__c = site2.id;
        ip2.DeviceTypeToCreate__c = 'device1';
        ip2.LifeCycleStatusOfTheInstalledProduct__c = system.label.CLAPR14SRV05;
        ip2.DecomissioningDate__c = date.today();
        //ip2.AssetCategory2__c =system.label.CLAPR14SRV01;
        ip2.SVMXC__Serial_Lot_Number__c = '1234vis';
        ip2.SVMXC__Product__c=newprod.Id;
        ip2.SVMXC__Warranty_Start_Date__c= date.Today()-121;
        ip2.SVMXC__Warranty_End_Date__c= date.Today()+130;
        ip2.ObsolescenceOpportunityGenerated__c =false;
        ip2.EndOfWarrantyOpportunityGenerated__c =false;
        ip2.AssetCategory2__c='Category 2' ;
        ip2.UnderContract__c =false;
      
        
        
        ip2.Tech_IpAssociatedToSC__c = false;
        
        
        insert ip2;
        
        SVMXC__Installed_Product__c ip3 = new SVMXC__Installed_Product__c();
        ip3.SVMXC__Company__c = accountUSAZ.id;
        ip3.Name = 'Test Intalled Product Zip ';
        ip3.SVMXC__Status__c= 'new';
        ip3.GoldenAssetId__c = 'GoledenAssertId3';
        ip3.BrandToCreate__c ='Test Brand';
        ip3.SVMXC__Site__c = site3.id;
        ip3.DeviceTypeToCreate__c = 'device1';
        ip3.LifeCycleStatusOfTheInstalledProduct__c = system.label.CLAPR14SRV05;
        ip3.DecomissioningDate__c = date.today();
        //ip3.AssetCategory2__c =system.label.CLAPR14SRV01;
        ip3.SVMXC__Serial_Lot_Number__c = '1234vis1';
        ip3.SVMXC__Product__c=newprod.Id;
        ip3.SVMXC__Warranty_Start_Date__c= date.Today()-121;
        ip3.SVMXC__Warranty_End_Date__c= date.Today()+130;
        ip3.ObsolescenceOpportunityGenerated__c =false;
        ip3.EndOfWarrantyOpportunityGenerated__c =false;
        ip3.AssetCategory2__c='Category 2' ;
        ip3.UnderContract__c =false;
      
        
        
        ip3.Tech_IpAssociatedToSC__c = false;
        
        
        insert ip3;
        
        
        
        set<Id> ips = new set<Id>();
        ips.add(ip1.id);
        ips.add(ip2.id);
        ips.add(ip3.id);
        
        List<SVMXC__Installed_Product__c> Iplist = [select id,EndOfWarrantyOpportunityGenerated__c,ObsolescenceOpportunityGenerated__c , SVMXC__Product__r.RenewableService__c ,Tech_CheckForEndOfWarranty__c,EndOfWarrantyOpportunityTriggered__c,ObsolescenceOpportunityTriggered__c,SVMXC__Product__r.Serviceability__c,
                                              Tech_IpAssociatedToSC__c,AssetCategory2__c,UnderContract__c,SVMXC__Company__c,SVMXC__Company__r.Name,
                                              SVMXC__Product__r.BusinessUnit__c, SVMXC__Company__r.MarketSegment__c,SVMXC__Warranty_End_Date__c,
                                              SVMXC__Company__r.LeadingBusiness__c,   SVMXC__Company__r.AccType__c,SVMXC__Product__r.ProductGDP__c, ProductBuCountry__c,
                                              SVMXC__Company__r.Country__c,SKUToCreate__c,CommissioningDateInstallDate__c,Tech_IPGrouping__c  from SVMXC__Installed_Product__c Where Id in :ips];
                                              
                                              
        //System.debug('prolist:'+prolist[0].RenewableService__c);
        System.debug('11Iplist:'+Iplist);
        applist.add(ip1);
        Test.starttest();
        if(Iplist!=null && Iplist.size()>0) {       
            AP_OpportunityCreationFromIB.ProcessIPforObsolence(Iplist);
            AP_OpportunityCreationFromIB.ProcessInstalledProduct(Iplist);
        }
        Test.stoptest();
    }
    }
    public Static User CreateUser(){
        User user = new User(alias = 'user', email='user' + '@accenture.com', 
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = System.label.CLDEC14SRV01, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP30;AP_WorkOrderTechnicianNotification;SVMX05;AP54;AP_Contact_PartnerUserUpdate',
        timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',FederationIdentifier = '12345');
        UserRole uRole = [Select Id,Name,DeveloperName From UserRole where DeveloperName = 'CEO'];
        user.UserRoleId = uRole.Id;
        insert user;
        return user;
    
    }
   
}