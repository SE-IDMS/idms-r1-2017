/*
    Controller for the VFP_ProjectBarcode and VFP_ProjectBarcodeDetailView
*/

public with sharing class VFC_ProjectBarcode{
    public VFC_ProjectBarcode(ApexPages.StandardController controller) {}
    
   //Wrapper class 
    public class oppWrapper {
        List<Opportunity> opplist{get;set;} 
        OPP_Project__c prj{get;set;}   
        User currentUserDetails{get;set;} 
        String decimalseperator{get;set;}  
    }
      
     //referred in the VFP_ProjectBarcodeDetailView to display the Opportunities 
     @RemoteAction  
     public static oppWrapper getOppData(Id prjID){
        List<Opportunity> opplstlevel1= new List<Opportunity>();
        List<Opportunity> opplstlevel2= new List<Opportunity>();
        List<Opportunity> opplstlevel3= new List<Opportunity>();
        List<Opportunity> opplist= new List<Opportunity>();
        OPP_Project__c project=new OPP_Project__c();
        project=[Select convertCurrency(Tech_SumOfOpenOpportunities__c),convertCurrency(Tech_SumOfWonOpportunities__c),convertCurrency(Tech_SumOfLostOpportunities__c),convertCurrency(Tech_SumOfCancelledOpportunities__c) from OPP_Project__C where Id=:prjID];
        
        //first level Opportunities which are directly linked to the master Project
        opplstlevel1=[Select Id,Owner.Name,Name,AccountId,Account.Name,toLabel(Account.ClassLevel1__c),StageName,CurrencyIsocode,convertCurrency(Amount),QuoteSubmissionDate__c,CloseDate,ForecastCategoryName,IncludedInForecast__c,Probability,OpptyType__c,Status__c,OpportunityCategory__c,ToBeDeleted__c from Opportunity where Project__c=:prjID];
         
        //Second level Opportunities      
        opplstlevel2=[select Id,Owner.Name,Name,AccountId,Account.Name,toLabel(Account.ClassLevel1__c),StageName,CurrencyIsocode,convertCurrency(Amount),QuoteSubmissionDate__c,CloseDate,ForecastCategoryName,IncludedInForecast__c,Probability,OpptyType__c,Status__c,OpportunityCategory__c,ToBeDeleted__c from Opportunity where ParentOpportunity__c in :opplstlevel1];
            
        //Third level Opportunities      
        opplstlevel3=[select Id,Owner.Name,Name,AccountId,Account.Name,toLabel(Account.ClassLevel1__c),StageName,CurrencyIsocode,convertCurrency(Amount),QuoteSubmissionDate__c,CloseDate,ForecastCategoryName,IncludedInForecast__c,Probability,OpptyType__c,Status__c,OpportunityCategory__c,ToBeDeleted__c from Opportunity where ParentOpportunity__c in :opplstlevel2 and IncludedInForecast__c=:System.Label.CLOCT15SLS07 and ToBeDeleted__c=False];
              
        if(opplstlevel1.size()>0){
            for(Opportunity opp:opplstlevel1){
                if(opp.IncludedInForecast__c==System.Label.CLOCT15SLS07 && opp.ToBeDeleted__c==False)
                    opplist.add(opp);
            }
        }
        
        if(opplstlevel2.size()>0){
            for(Opportunity opp:opplstlevel2){
                if(opp.IncludedInForecast__c==System.Label.CLOCT15SLS07 && opp.ToBeDeleted__c==False)
                    opplist.add(opp);
            }
        }
        
        if(opplstlevel3.size()>0)
            opplist.addAll(opplstlevel3);
        // fetch the currency details of the current user who is viewing
        User currentUser=[select CurrencyIsoCode,DefaultCurrencyIsoCode from user where Id=:UserInfo.getUserId()];
        oppWrapper owrap= new oppWrapper();
        owrap.prj=project;
        owrap.opplist=opplist;
        owrap.currentUserDetails=currentUser;
        owrap.decimalseperator=CS_CurrencyDecimalSeperator__c.getValues(UserInfo.getLocale()).DecimalSeperator__c;
        return owrap;
    }
    
   //referred in the VFP_ProjectBarcode 
   @RemoteAction  
    public static oppWrapper getProjectData(Id prjID){
        //convertCurrency functiona is used to display amount in the current user currency
        OPP_Project__c project=[Select convertCurrency(Tech_SumOfOpenOpportunities__c),convertCurrency(Tech_SumOfWonOpportunities__c),convertCurrency(Tech_SumOfLostOpportunities__c),convertCurrency(Tech_SumOfCancelledOpportunities__c) from OPP_Project__C where Id=:prjID];
        // fetch the currency details of the current user who is viewing
        User currentUser=[select CurrencyIsoCode,DefaultCurrencyIsoCode from user where Id=:UserInfo.getUserId()];
        oppWrapper owrap= new oppWrapper();
        owrap.prj=project;
        owrap.currentUserDetails=currentUser;
        owrap.decimalseperator=CS_CurrencyDecimalSeperator__c.getValues(UserInfo.getLocale()).DecimalSeperator__c;
        return owrap;
    }
}