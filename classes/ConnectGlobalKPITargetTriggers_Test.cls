/*
    Author          : Kiran Kareddy (Schneider Electric)
    Date Created    : 18-oct-2012
    Description     : Test Class for ConnectGlobalKpiTargetTriggers
*/

@isTest
private class ConnectGlobalKPITargetTriggers_Test {
    static testMethod void testConnectGlobalKPITargetTriggers() {
        User runAsUser = Utils_TestMethods_Connect.createStandardConnectUser('connts21');
        User runAsUser2 = Utils_TestMethods_Connect.createStandardConnectUser('connts23');
        User runAsUser3 = Utils_TestMethods_Connect.createStandardConnectUser('connts24');
        System.runAs(runAsUser){
        test.startTest();
         Initiatives__c inti=new Initiatives__c(Name='initiative', Transformation__c = Label.ConnectTransPeople);
         insert inti;
         
         Project_NCP__c cp=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp',User__c = runasuser.id, Program_Leader_2__c = runasuser2.id,Program_Leader_3__c = runasuser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power;'+Label.Connect_Smart_Cities,Power_Region__c='NA;APAC',Smart_Cities_Division__c='test');
         insert cp; 
         
         Project_NCP__c cp2=new Project_NCP__c(Program__c=inti.id,Program_Name__c='cp2',User__c = runasuser2.id, Program_Leader_2__c = runasuser.id,Program_Leader_3__c = runasuser3.id, Global_Functions__c='GM;S&I;HR;GSC-Regional',GSC_Regions__c='NA',Global_Business__c='ITB;power',Power_Region__c='NA;APAC');
         insert cp2;
         
         Champions__c C3=new Champions__c(Scope__c='Global Business',Entity__c=Label.Connect_Smart_Cities,Smart_Cities_Division__c='test',Year__c = '2013');
         insert c3;
          Champions__c C4=new Champions__c(Scope__c='Global Business',Entity__c='Industry',Year__c = '2013');
         insert c4;
         Champions__c C5=new Champions__c(Scope__c='Global Functions',Entity__c='GSC-Regional',GSC_Regions__c='NA',Year__c = '2013');
         insert c5;
          Champions__c C6=new Champions__c(Scope__c='Global Business',Entity__c='Partner-Division',Partner_Division__c='test',Year__c = '2013');
         insert c6;
                
         Global_KPI_Targets__c GKPIT=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp.id, Transformation__c = Label.ConnectTransPeople, Global_Business__c = 'Industry;Partner-Division;'+Label.Connect_Smart_Cities,Global_Functions__c='GM;GSC-Regional',Operations_Region__c='NA',GSC_Regions__c='NA',Smart_Cities_Division__c='test',Partner_Sub_Entity__c='test',Year__c='2013');
         insert GKPIT;
         
          Global_KPI_Targets__c GKPIT2=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,Connect_Global_Program__c = cp2.id, Transformation__c = Label.ConnectTransPeople,Global_Business__c = 'Industry',Global_Functions__c='GM',Operations_Region__c='NA');
         insert GKPIT2;
         
         Cascading_KPI_Target__c EKPI = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target_Q4__c = '10', Scope__c = 'Global Business', Entity__c = 'Industry' );
         insert EKPI;
         Cascading_KPI_Target__c EKPI1 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Functions', Entity__c = 'GSC-Regional',GSC_Region__c='NA' );
         insert EKPI1;
         Cascading_KPI_Target__c EKPI2 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = Label.Connect_Smart_Cities,Smart_Cities_Division__c='test' );
         insert EKPI2;
         Cascading_KPI_Target__c EKPI3 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT.id,Entity_KPI_Target_Q4__c = '10',Entity_Data_Reporter__c=label.Connect_Email, Scope__c = 'Global Business', Entity__c = 'Partner-Division',Partner_Sub_Entity__c='test' );
         insert EKPI3;
         
         Global_KPI_Targets__c GKPITC=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,CountryPickList__c = 'IN-INDIA',Year__c='2013');
         insert GKPITC;
         
         //Cascading_KPI_Target__c EKPIC = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPITC.id);
        // insert EKPIC;
         
         c3.Champion_Name__c = label.Connect_Email;
         c4.Champion_Name__c = label.Connect_Email;
         c5.Champion_Name__c = label.Connect_Email;
         c6.Champion_Name__c = label.Connect_Email;
         update c3;
         update c4;
         update c5;
         update c6;
         
          test.stopTest();  
         
         Global_KPI_Targets__c GKPIT4=new Global_KPI_Targets__c(KPI_Name__c='testKPI',Global_Data_Reporter__c=label.Connect_Email,Global_KPI_Owner__c=label.Connect_Email,KPI_Type__c = 'Country KPI', CountryPickList__c='NA-Canada;CN-China;NA-Mexico;IN-India');
         GKPIT4.Year__c = '2013';
         insert GKPIT4;
         Cascading_KPI_Target__c EKPI4 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT4.id,Entity_KPI_Target_Q4__c = '10',CountryPickList__c ='NA-Canada' );
         insert EKPI4;
         Cascading_KPI_Target__c EKPI5 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT4.id,Entity_KPI_Target_Q4__c = '10',CountryPickList__c ='CN-China' );
         insert EKPI5;
         
         
         
          EKPI.Entity_Data_Reporter__c = label.Connect_Email;
          update EKPI;
          EKPI4.CountryPickList__c = 'NA-Mexico';
         System.Debug('The year is:'+EKPI4.Year__c);
          update EKPI4;
         
         Cascading_KPI_Target__c EKPI6 = new Cascading_KPI_Target__c(Global_KPI_Target__c = GKPIT4.id,Entity_KPI_Target_Q4__c = '10',CountryPickList__c ='IN-India' );
         insert EKPI6;
         System.Debug('The year 6 is:'+EKPI6.Year__c);
         
         cp.Program_Leader_2__c = Utils_TestMethods_Connect.createStandardConnectUser('connectUv').id;
         update cp;
         update GKPIT;
         
         cp2.User__c = Utils_TestMethods_Connect.createStandardConnectUser('connectUX1').id;
         update cp2;
         update GKPIT2;     
    
               
         GKPIT.Global_Data_Reporter__c = Utils_TestMethods_Connect.createStandardConnectUser('connectUt').id;
         GKPIT.Global_KPI_Owner__c = runasuser.id;
         GKPIT.Comments__c = 'Comments for testing';
         GKPIT.KPI_Description__c = 'Description for testing';
         update GKPIT;
         
         GKPIT.Connect_Global_Program__c = cp2.id;
         update GKPIT;         
         
         GKPIT.Connect_Global_Program__c  = null;
         GKPIT.Comments__c = 'Comments updated for testing';
         GKPIT.KPI_Description__c = 'Description updated for testing';
         update GKPIT;
         
  }
        }
        }