@isTest
public class AP_CassiniPOCCaseAssingment_TEST {
    public static testMethod void TestCassiniPOCCaseAssingment() { 
         Map<String,ID> profiles = new Map<String,ID>();
         List<Profile> ps = [select id, name from Profile where name = 'System Administrator' Limit 1];
         for(Profile p : ps)
          {
             profiles.put(p.name, p.id);
          }
          
          
         user admin =  [SELECT Id FROM user WHERE profileid =:profiles.get('System Administrator') and isActive=true and UserRoleID!=null and BypassVR__c= True limit 1];
  
  
        system.runas(admin){
        Id profilesId = System.label.CLMAR13PRM03;
               
        Country__c ObjCountry=null;
        List<Country__c> countries =[select id,Name,CountryCode__c from country__c limit 1];
        if(countries.size()==1)
            ObjCountry=countries[0];
        else{               
            ObjCountry=new country__c(Name='India',CountryCode__c='IN');
            insert ObjCountry;    
        }
        
        String countryid = ObjCountry.id;
    
        Account PartnerAcc = new Account(Name='TestAccount', Street__c='New Street', POBox__c='XXX', ZipCode__c='12345');
        insert PartnerAcc;
        
        Contact PartnerCon = new Contact(
                FirstName='Test',
                LastName='lastname',
                AccountId=PartnerAcc.Id,
                JobTitle__c='Z3',
                CorrespLang__c='EN',            
                WorkPhone__c='1234567890'
                );
                System.debug('****** Country ID:'+countryid);
                PartnerCon.Country__c = countryid ; 
                
                Insert PartnerCon;
        
       
       
       PermissionSet orfConvPermissionSet1 = [Select ID,UserLicenseId from PermissionSet where Name='PRMRegularORFCommunity' Limit 1];
          
       User   u1= new User(Username = 'testUserOne@schneider-electric.com', LastName = 'User11', alias = 'tuser1',
                        CommunityNickName = 'testUser1', TimeZoneSidKey = 'America/Chicago', 
                        Email = 'testUser@schneider-electric.com', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8',
                        LanguageLocaleKey = 'en_US' ,BypassVR__c= True, ProfileID = profilesId, ContactID = PartnerCon.Id, UserPermissionsSFContentUser=true );
       insert u1; 
       
        Contract contract=new Contract();
        contract.StartDate=System.now().Date();
        contract.Points__c=109;
        contract.WebAccess__c='Cassini';
        contract.AccountId=PartnerAcc.Id;
        contract.ContactName__c=PartnerCon.Id;
        insert contract;
        System.assert(contract!=null);
        
        CTR_ValueChainPlayers__c ctrv=new CTR_ValueChainPlayers__c();
        ctrv.Contact__c=PartnerCon.Id;
        ctrv.Contract__c=contract.Id;
        insert ctrv;
        System.assert(ctrv!=null);
        
        Case objCase=new Case(Description='Test Desc12341',CustomerRequest__c='Test CustReq12341',
                              RelatedContract__c=contract.Id,ContactID=PartnerCon.Id);
        insert objCase;
        System.assert(objCase!=null);
        
        BusinessRiskEscalationEntity__c org=new BusinessRiskEscalationEntity__c(Name='TestOrg123451',CurrencyIsoCode='EUR',Entity__c='Test Entity',Location_Type__c='Customer Care Center',Location__c='Primary CCC Philippines (Cavite)',SubEntity__c='Test SubEntity');
        insert org;
        
        PointOfContact__c poc=new PointOfContact__c(ChannelType__c='Portal',Name='Test123451',Organization__c=org.Id,Priority1Routing__c='Product Family Support Team',SupportedCountry__c=countryid);
        insert poc;
        
        PackageTemplate__c packageTemplate=new PackageTemplate__c(Name='Test Package Template123451',PointOfContact__c=poc.Id,PortalAccess__c=TRUE);
        insert packageTemplate;
        
        Package__c package1=new Package__c(Contract__c=contract.Id,CurrencyIsoCode='EUR',EndDate__c=System.now().addDays(100).Date(),PackageTemplate__c=packageTemplate.Id,StartDate__c=System.now().Date());
        insert package1;
        
        CustomerCareTeam__c cccTeam1=new CustomerCareTeam__c(Name='Test Cassini Team123451',ManagingOrganization__c=org.Id);
        insert cccTeam1;
        
        CustomerCareTeam__c cccTeam2=new CustomerCareTeam__c(Name='Test Cassini Team1234512',ManagingOrganization__c=org.Id);
        insert cccTeam2;
        
        OPP_Product__c family=new OPP_Product__c(Name='Test Family123451',ProductFamily__c='Test family123451');
        insert family;
        
        TeamProductJunction__c teamProduction1=new TeamProductJunction__c(CCTeam__c=cccTeam1.Id,ProductFamily__c=family.Id);
        insert teamProduction1;
        
        TeamProductJunction__c teamProduction2=new TeamProductJunction__c(CCTeam__c=cccTeam2.Id,ProductFamily__c=family.Id,DefaultSupportedProductFamily__c=true);
        insert teamProduction2;
        
        
       Test.startTest();

       PartnerAcc.TECH_AccountOwner__c=u1.Id;
       update PartnerAcc;
      
       system.runas(u1)
       {
            AP_CassiniPOCCaseAssingment caseAssignment=new AP_CassiniPOCCaseAssingment ();
            AP_CassiniPOCCaseAssingment.assingCase('Test family123451');
       }
       Test.stopTest();
       }
    }



}