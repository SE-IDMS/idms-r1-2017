/*
Author: Sid (bFO Solution) & Polyspot team
description: this class is a test class for the controller class VFC_DK_ContactSeach  , this controller provides a way to make
a webservice callout to polyspot engine and it will return back with the json response.

*/

@isTest

global class VFC_DK_ContactSearch_TEST {
     @isTest static void testCallout() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        VFC_DK_ContactSearch.getContent('{searchstring:eric}');
    }
}