/**
* @author Alejandro Spinelli
* @date   04/12/2012
* @description  controller for the news view page
*/
global with sharing class Fielo_NewsViewExtensions {

    global FieloEE__Member__c member {get;set;}
    global Boolean likeIt {get;set;}
    global String parentId {get;set;}
    global ApexPages.StandardController controller {get;set;}
    
    public FieloEE__Menu__c menu {get;set;}
    public List<String> viewFieldSet {get; set;}
    public List<String> listFieldSet {get; set;}
    private Id compId {get; set;}
    
    global Fielo_NewsViewExtensions(ApexPages.StandardController controller) {
         this.controller = controller;
         member = FieloEE.MemberUtil.getAllFieldsLoggedMember();
         likeIt = false;
         parentId = (controller.getId() != null) ? controller.getId().substring(0,15) : '';

         if(parentId != null && member != null){            
            List<FieloEE__Like__c> newsLike =  [SELECT Id, FieloEE__Member__c, FieloEE__ParentId__c FROM FieloEE__Like__c WHERE FieloEE__Member__c =: member.id AND FieloEE__ParentId__c =: parentId];
            if(newsLike.size() > 0){
                likeIt = true;
            }
        }
        
        String allFields = '';
        for(Schema.SObjectField nameAPIfield : Schema.SObjectType.FieloEE__Menu__c.fields.getMap().values()){
            allFields += allFields==''?String.ValueOf(nameAPIfield):', '+String.ValueOf(nameAPIfield);
        }
        menu = Database.query('SELECT '+allFields+' FROM FieloEE__Menu__c WHERE Id = \''+Apexpages.currentPage().getParameters().get('idMenu')+'\'');
        
        compId = Apexpages.currentPage().getParameters().get('compId');
        viewFieldSet = new List<String>();
        listFieldSet = new List<String>();
        FieloEE__Component__c component;
        if(compId != null){            
            component = [SELECT FieloEE__FieldSet__c, FieloEE__FieldSet2__c FROM FieloEE__Component__c WHERE id =: compId LIMIT 1];
            //add to the set the custom fieldset of the record
            if(component.FieloEE__FieldSet__c != null && component.FieloEE__FieldSet__c != ''){                
                for(String field : component.FieloEE__FieldSet__c.split(',')){
                    viewFieldSet.add('FieloEE__'+field);             
                }
            }

            if(component.FieloEE__FieldSet2__c != null && component.FieloEE__FieldSet2__c != ''){
                for(String field : component.FieloEE__FieldSet2__c.split(',')){
                    listFieldSet.add('FieloEE__'+field);
                }
            }
        }
        if(!System.Test.isRunningTest()){
            this.controller.addFields(viewFieldSet);    
        }
        
    }
         
    /**
    * @author Matias Trionfetti
    * @date 24/07/2012
    * @description method that quit a reward from the wish list
    * @return Pagereference 
    */
    global Pagereference refresh(){
        try{
            PageReference pageRef = Page.Fielo_NewsView;
            pageRef.getParameters().put('id',controller.getId());
            pageRef.getParameters().put('returnURL', Apexpages.currentPage().getParameters().get('returnURL'));
            pageRef.setRedirect(true);
            return pageRef;
        }catch(Exception e){}
        
        return null;
    }
   
}