//Batch Job for Processing the Records
global class IDMSCreateCompanyReconciliationBatch implements Database.Batchable<SObject>,Database.AllowsCallouts{
    
    //Start Method
    global Database.Querylocator start (Database.BatchableContext BC) {
        //Query which will be determine the scope of Records fetching the same
        Integer attemptsMaxLmt = integer.valueof(label.CLNOV16IDMS022);
        Integer lmtRecords = integer.valueof(label.CLNOV16IDMS023);        
        return Database.getQueryLocator([Select Id, AtCreate__c,AtUpdate__c, HttpMessage__c, IdmsUser__c, NbrAttempts__c from IDMSCompanyReconciliationBatchHandler__c where NbrAttempts__c <= :attemptsMaxLmt limit :lmtRecords]);
    }
    
    //Execute method
    global void execute (Database.BatchableContext BC, List<SObject> scope) {
        //Unique Batch Id 
        String BatchId = BC.getJobId();        
        List<IDMSCompanyReconciliationBatchHandler__c> recCompaniesToCreate = new List<IDMSCompanyReconciliationBatchHandler__c>();
        for (SObject userConcScope: scope){ 
            IDMSCompanyReconciliationBatchHandler__c usr = (IDMSCompanyReconciliationBatchHandler__c)userConcScope;
            if(usr.AtCreate__c == true){
                recCompaniesToCreate.add(usr);
            }
        }
        if(recCompaniesToCreate.size() > 0){
            //ToDo: do API call to create users in UIMS
            IdmsUimsReconcCreateCompanyBulk.createUimsCompanies(recCompaniesToCreate, BatchId);
            System.debug('Idms users to Create Reconciliate: '+recCompaniesToCreate);
        }
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC)
    {
        //Below code will fetch the job Id.
        AsyncApexJob a = [Select a.TotalJobItems, a.Status, a.NumberOfErrors, a.JobType, a.JobItemsProcessed, a.ExtendedStatus, a.CreatedById, a.CompletedDate From AsyncApexJob a WHERE id = :BC.getJobId()];//get the job Id
        //Schedule update company batch.
        
        system.debug('*****Inside third batch run******');
        IDMSUpdateCompanyReconciliationBatch updateCompanyBatch = new IDMSUpdateCompanyReconciliationBatch();
        ID batchprocessid = Database.executeBatch(updateCompanyBatch ,100);        
    }
}