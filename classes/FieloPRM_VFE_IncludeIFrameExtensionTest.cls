/********************************************************************
* Company: Fielo
* Created Date: 05/08/2016
* Description: Test class for class FieloPRM_VFE_IncludeIFrameExtension
********************************************************************/
@isTest
public without sharing class FieloPRM_VFE_IncludeIFrameExtensionTest {
  
    @testSetup static void testSetupMethodPRM(){
    
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
    
            Country__c testCountry = new Country__c(
                Name   = 'Global',
                CountryCode__c = 'WW'
            );
            insert testCountry;
           
            PRMCountry__c testPRmcountry = new PRMCountry__c(
                Name   = 'Global',
                Country__c = testCountry.id,
                PLDatapool__c = '123123test',
                CountryPortalEnabled__c = true       
            );
            insert testPRmcountry;
            
            Account acc = new Account();
            acc.Name = 'test acc';
            acc.PRMCountry__c = testCountry.Id;
            insert acc;
            
            Contact con = new Contact(
                AccountId = acc.Id,
                FirstName = 'test contact Fn 2',
                LastName  = 'test contact Ln',
                PRMCompanyInfoSkippedAtReg__c = false,
                PRMCountry__c = testCountry.Id,
                PRMFirstName__c = 'PRM test contact Fn',
                PRMLastName__c  = 'PRM test contact Fn',
                PRMLanguage__c = 'en'
            );
            insert con;
    
            List<Profile> profiles = [select id from profile where name='SE - Channel Partner (Community)'];
            if(profiles.size()>0){
                UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
                
                User userTest = Utils_TestMethods.createStandardUser(profiles[0].Id,'tUsVFC92');
                userTest.ContactId = con.Id;
                userTest.PRMRegistrationCompleted__c = false;
                userTest.BypassWF__c = false;
                userTest.BypassVR__c = true;
                userTest.FirstName = 'Test User First';
    
                insert userTest;
            }
        }

    }
    
    public static testMethod void testUnit(){
        
        User us = new user(id = userinfo.getuserId());
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Menu__c menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Menu'
            );
            insert menu;
            
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            
            // test without member
            FieloPRM_VFE_IncludeIFrameExtension C1 = new FieloPRM_VFE_IncludeIFrameExtension(new FieloEE.PageController());
            C1.redirectPageLogin();
            C1.idPrimaryMenu = 'TST';
            
            FieloPRM_VFE_IncludeIFrameExtension.sendEmailToCase('Description', 'url');
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo',
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'Calle Falsa',
                F_PRM_PrimaryChannel__c = 'TST'
            );
            insert member;
            
            FieloEE.MemberUtil.setMemberId(member.Id);
            
            FieloPRM_VFE_IncludeIFrameExtension C2 = new FieloPRM_VFE_IncludeIFrameExtension(new FieloEE.PageController());
            C2.redirectPageLogin();
            String fieldON = C2.fieldOptionalName;
            String fieldMT = C2.fieldMenuTitle;
            Object cw  = new FieloPRM_VFE_IncludeIFrameExtension.ContactWrapper();
            Object cw2 = new FieloPRM_VFE_IncludeIFrameExtension.ContactWrapper('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
            
        }
    }
    
    public static testMethod void testUnittwo(){

        User us = new user(id = userinfo.getuserId());

        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Menu__c menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Menu'
            );
            //menu.FieloEE__ExternalName__c = 'ContinueRegistration';
            menu.FieloEE__visibility__c   = 'Public';
            insert menu;
            
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            
            // test without member
            FieloPRM_VFE_IncludeIFrameExtension C1 = new FieloPRM_VFE_IncludeIFrameExtension(new FieloEE.PageController());
            C1.redirectPageLogin();
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo',
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'Calle Falsa'
            );
            insert member;
            
            FieloEE.MemberUtil.setMemberId(member.Id);
            
            FieloPRM_VFE_IncludeIFrameExtension C2 = new FieloPRM_VFE_IncludeIFrameExtension(new FieloEE.PageController());
            C2.redirectPageLogin();
            Object cw  = new FieloPRM_VFE_IncludeIFrameExtension.ContactWrapper();
            Object cw2 = new FieloPRM_VFE_IncludeIFrameExtension.ContactWrapper('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
            
        }
    }
    
    public static testMethod void testUnitthree(){

        User us = new user(id = userinfo.getuserId());

        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Menu__c menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Menu'
            );
            menu.FieloEE__ExternalName__c = 'ContinueRegistration';
            menu.FieloEE__visibility__c   = 'Private';
            menu.F_PRM_AvailableGuestUsers__c = false;
            insert menu;
            
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            
            // test without member
            FieloPRM_VFE_IncludeIFrameExtension C1 = new FieloPRM_VFE_IncludeIFrameExtension(new FieloEE.PageController());
            C1.redirectPageLogin();
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo',
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'Calle Falsa'
            );
            insert member;
            
            FieloEE.MemberUtil.setMemberId(member.Id);
            
            FieloPRM_VFE_IncludeIFrameExtension C2 = new FieloPRM_VFE_IncludeIFrameExtension(new FieloEE.PageController());
            C2.redirectPageLogin();
            Object cw  = new FieloPRM_VFE_IncludeIFrameExtension.ContactWrapper();
            Object cw2 = new FieloPRM_VFE_IncludeIFrameExtension.ContactWrapper('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
            
        }
    }
    public static testMethod void testUnitfour(){

        User us = new user(id = userinfo.getuserId());
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Menu__c menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Menu'
            );
            menu.FieloEE__ExternalName__c = 'ContinueRegistration';
            menu.FieloEE__visibility__c   = 'Public';
            insert menu;
            
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            
            // test without member
            FieloPRM_VFE_IncludeIFrameExtension C1 = new FieloPRM_VFE_IncludeIFrameExtension(new FieloEE.PageController());
            C1.redirectPageLogin();
            
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo',
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'Calle Falsa'
            );
            insert member;
            
            FieloEE.MemberUtil.setMemberId(member.Id);
            
            FieloPRM_VFE_IncludeIFrameExtension C2 = new FieloPRM_VFE_IncludeIFrameExtension(new FieloEE.PageController());
            C2.redirectPageLogin();
            Object cw  = new FieloPRM_VFE_IncludeIFrameExtension.ContactWrapper();
            Object cw2 = new FieloPRM_VFE_IncludeIFrameExtension.ContactWrapper('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
            
        }
    }
    
    public static testMethod void testUnitfive(){
    
        User us = new user(id = userinfo.getuserId());
        us.LanguageLocaleKey = 'th';
        update us;
        
        System.RunAs(us){

            FieloEE.MockUpFactory.setCustomProperties(false);
            
            FieloEE__Menu__c menu = new FieloEE__Menu__c(
                FieloEE__Title__c = 'Menu'
            );
            menu.FieloEE__ExternalName__c = 'ContinueRegistration';
            menu.FieloEE__visibility__c   = 'Public';
            insert menu;
            
            String idRecordTypeCat = [Select id, developerName, sobjecttype from RecordType WHERE sobjecttype = 'FieloEE__Category__c' and developerName = 'CategoryItem'].id;

            FieloEE__Category__c cat = FieloPRM_UTILS_MockUpFactory.createCategory(menu.Id);
                           
            ApexPages.currentPage().getParameters().put('idMenu', menu.id);
            
            // test without member
            FieloPRM_VFE_IncludeIFrameExtension C1 = new FieloPRM_VFE_IncludeIFrameExtension(new FieloEE.PageController());
            C1.redirectPageLogin();

            Country__c country = new Country__c(
                Name = 'Test',
                CountryCode__c = 'TST'            
            );
            insert country;
            
            FieloEE__Member__c member = new FieloEE__Member__c(
                FieloEE__LastName__c = 'Polo',
                FieloEE__FirstName__c = 'Marco' + String.ValueOf(DateTime.now().getTime()),
                FieloEE__Street__c = 'Calle Falsa',
                F_Country__c = country.Id,
                F_PRM_PrimaryChannel__c = 'TST'               
            );
            insert member;

            ClassificationLevelCatalog__c channel = new ClassificationLevelCatalog__c(
                Active__c = true,
                ClassificationLevelName__c = 'TST'
            );
            insert channel;

            ClassificationLevelCatalog__c subChannel = new ClassificationLevelCatalog__c(
                Active__c = true,
                ClassificationLevelName__c = 'TST',
                ParentClassificationLevel__c = channel.Id
            );
            insert subChannel;
            
            CountryChannels__c countryChannel = new CountryChannels__c(
                Active__c = true,
                Country__c = country.Id,
                Channel__c = channel.Id,
                SubChannel__c = subChannel.Id,
                F_PRM_Menu__c = menu.Id   
            );
            insert countryChannel;
            
            FieloEE.MemberUtil.setMemberId(member.Id);
            
            FieloPRM_VFE_IncludeIFrameExtension C2 = new FieloPRM_VFE_IncludeIFrameExtension(new FieloEE.PageController());
            C2.redirectPageLogin();
            Object cw  = new FieloPRM_VFE_IncludeIFrameExtension.ContactWrapper();
            Object cw2 = new FieloPRM_VFE_IncludeIFrameExtension.ContactWrapper('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
            
        }
    }
}