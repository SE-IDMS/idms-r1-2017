public class AP_TeamProductMappingMethods
{
    //Makes sure only one product family is marked as the default one for every CCC team
    public static void uncheckOtherDefaultProductFamilies(List<TeamProductJunction__c> supportedProductFamilies)
    {
        System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultProductFamilies.INFO - Method is called.');
        Integer defaultProductFamilyCount = 0;
        Set<ID> TeamIds = new Set<ID>();
        if(supportedProductFamilies != null && supportedProductFamilies.size() != null) {
            
            try {     
                for(Integer i=0; i < supportedProductFamilies.size(); i++) {
                    if(supportedProductFamilies[i].DefaultSupportedProductFamily__c){
                        defaultProductFamilyCount++;
                        TeamIds.add(supportedProductFamilies[i].CCTeam__c);
                    }
                    if(defaultProductFamilyCount > 1){
                        supportedProductFamilies[i].DefaultSupportedProductFamily__c.addError('You cannot define multiple default Supported Product Families for a team.');
                    }
                }
                
                if(defaultProductFamilyCount == 1){
                    List<TeamProductJunction__c> existingDefaultProductFamiliesList = new List<TeamProductJunction__c>();
                    //existingDefaultProductFamiliesList = [SELECT ID, DefaultSupportedProductFamily__c FROM TeamProductJunction__c WHERE DefaultSupportedProductFamily__c = TRUE];
                    existingDefaultProductFamiliesList = [SELECT ID, DefaultSupportedProductFamily__c FROM TeamProductJunction__c WHERE DefaultSupportedProductFamily__c = TRUE and CCTeam__c=:TeamIds];
                
                    if(existingDefaultProductFamiliesList.size() == 0) {
                        System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultProductFamilies.SUCCESS - No existing Supported Product Family marked as default found.');
                    }
                    else {
                        System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultProductFamilies.INFO - List of product families marked as default =' + existingDefaultProductFamiliesList);
                        for(TeamProductJunction__c existingAssignment : existingDefaultProductFamiliesList) {
                            existingAssignment.DefaultSupportedProductFamily__c = false;
                        }
                        // Update records
                        List<Database.SaveResult> existingDefaultProductFamiliesListUncheck = Database.Update(existingDefaultProductFamiliesList, false);

                        for(Database.SaveResult dbSaveResult : existingDefaultProductFamiliesListUncheck){
                            if(!dbSaveResult.isSuccess()) {
                                DataBase.Error DMLerror = dbSaveResult.getErrors()[0];
                                System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultProductFamilies.DML_EXCEPTION - Error When updating team-assignment.');
                                System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultProductFamilies.DML_EXCEPTION - Error Message: ' + DMLerror.getMessage());
                            }
                            else {
                                System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultProductFamilies.SUCCESS - Default Assignment updated successfully. ');
                            }
                        }
                    }
                }
            }
            catch(Exception e) { 
                System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultTeam.'+e.getTypeName() + '- ' + e.getMessage());
                System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultTeam.'+e.getTypeName() + '- Line = ' + e.getLineNumber());
                System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultTeam.'+e.getTypeName() + '- Stack trace = ' + e.getStackTraceString());
            }
        }   
        else {
            System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultProductFamilies.ERROR - Entry map null or empty.'); 
        }
        
        System.debug('AP_TeamProductMappingMethods.uncheckOtherDefaultProductFamilies.INFO - End of method.');
    }
    
}