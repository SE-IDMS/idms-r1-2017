/**
* This is test class for IDMSConfirmPin Apex class. 
**/
@IsTest 
public class IDMSConfirmPinTest{
    //method tests end point url and http moethod with all required parameters for confirm PIN API. 
    static testmethod void testdoPut1(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/ConfirmPIN';  
        req.httpMethod = 'Post';
        RestContext.request= req;
        RestContext.response= res;
        User UserObj = IDMSTestClassUtility.createUser('@Home','UserCreationtest3Aug@accenture.com',
                                                       'testfirstname','testlastname1','EN','TestClass');        
        IDMSConfirmPinResponse response = IdmsConfirmpinRest.doPost(UserObj.id,UserObj.FederationIdentifier,'UIMS','123456','TTTTTTT','WELCOME123'); 
        IDMSConfirmPinResponse response1 = IdmsConfirmpinRest.doPost(UserObj.id,UserObj.FederationIdentifier,'','123456','TTTTTTT','WELCOME123');
        IDMSConfirmPinResponse response2 = IdmsConfirmpinRest.doPost(UserObj.id,UserObj.FederationIdentifier,'','','SetUserPwd',''); 
        IDMSConfirmPinResponse response3 = IdmsConfirmpinRest.doPost('','','','','','');   
    }
    //method to tests end point url and http moethod return status codes with few parameters for confirm PIN API.
    static testmethod void testdoPut2(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/ConfirmPin';  
        req.httpMethod = 'Put';
        RestContext.request= req;
        RestContext.response= res;
        User UserObj2 = new User(alias = 'user', email='user' + '@accenture.com', 
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                 localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                 timezonesidkey='Europe/London', username='user' + '@bridge-fo.com',Company_Postal_Code__c='12345',Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999');
        
        insert UserObj2;
        
        IDMSConfirmPin obj = new IDMSConfirmPin();
        obj.ConfirmPin(UserObj2.id,UserObj2.FederationIdentifier,'uims','123456','SetUserPwd','welcome123');
        obj.ConfirmPin('77777777777777777',UserObj2.FederationIdentifier,'uims','123456','SetUserPwd','welcome123');
        obj.ConfirmPin('','222222222222222222','uims','123456','SetUserPwd','welcome123');
        UserObj2.IDMSPinVerification__c='123456';
        update UserObj2;
        obj.ConfirmPin(UserObj2.id,UserObj2.FederationIdentifier,'uims','123456','UpdateUserRecord','welcome123');
        UserObj2.IDMSPinVerification__c='123456';
        update UserObj2;
        obj.ConfirmPin(UserObj2.id,UserObj2.FederationIdentifier,'uims','123456','ActivateUser','welcome123');
        UserObj2.IDMSPinVerification__c='123456';
        update UserObj2;
        obj.ConfirmPin(UserObj2.id,UserObj2.FederationIdentifier,'uims','123456','InitPwd','welcome123');
        system.setpassword(UserObj2.id,'Welcome#7890');
        obj.ConfirmPin(UserObj2.id,UserObj2.FederationIdentifier,'uims','123456','','welcome100');
        IDMSConfirmPin.updatePassword(UserObj2.id,'Welcome#1234');
    }
    //method to test activate user using pin sent by API.
    static testmethod void confirmPinIdIsActive() {
        IDMSConfirmPin confirm = new IDMSConfirmPin();
        User usercon = new User(alias = 'usercon', email='usercon' + '@accenture.com', isActive=true,
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                                BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                                timezonesidkey='Europe/London', username='usercon' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                                Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                                IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                                IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMS_VU_NEW__c='1');
        
        insert usercon ;
        usercon.IDMSPinVerification__c = '123456';
        update usercon; 
        Test.startTest();
        System.runAs(usercon){
            confirm.ConfirmPin(usercon.id,'','idms','123456','SetUserPwd','');
            confirm.ConfirmPin(usercon.id,'','idms','','SetUserPwd','');
            confirm.ConfirmPin(usercon.id,'','idms','123456','SetUserPwd','Welcome#');
            confirm.ConfirmPin(usercon.id,'','idms','123456','SetUserPwd','Welcome#123');
            confirm.ConfirmPin(usercon.id,'','UIMS','123456','SetUserPwd','Welcome#123');
        }
        User userfed = [select IDMSPinVerification__c,FederationIdentifier  from user where id =: usercon.id ];
        userfed.IDMSPinVerification__c = '123456';
        update userfed; 
        System.runAs(userfed){
            confirm.ConfirmPin('',userfed.FederationIdentifier  ,'idms','','SetUserPwd','');
            confirm.ConfirmPin('',userfed.FederationIdentifier  ,'idms','123456','SetUserPwd','');
            confirm.ConfirmPin('',userfed.FederationIdentifier  ,'idms','123456','SetUserPwd','Welcome#');
            confirm.ConfirmPin('',userfed.FederationIdentifier  ,'idms','123456','SetUserPwd','Welcome#123');
            confirm.ConfirmPin('',userfed.FederationIdentifier  ,'UIMS','123456','SetUserPwd','Welcome#123');
        }
        
    }
    //method to test not activate user using pin sent by API.
    static testmethod void confirmPinIdIsNoActive() {
        IDMSConfirmPin confirm = new IDMSConfirmPin();
        User userno = new User(alias = 'userno', email='userno' + '@accenture.com',
                               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                               localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                               BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                               timezonesidkey='Europe/London', username='userno' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                               Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                               IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                               IDMS_Email_opt_in__c = 'Y',IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMS_VU_NEW__c='1');
        
        insert userno ;
        userno.IDMSPinVerification__c = '123456';
        update userno; 
        Test.startTest();
        confirm.ConfirmPin(userno.id,'','idms','123456','SetUserPwd','');
        confirm.ConfirmPin(userno.id,'','idms','123456','SetUserPwd','Welcome#');
        confirm.ConfirmPin(userno.id,'','idms','123456','SetUserPwd','Welcome#123');
        confirm.ConfirmPin(userno.id,'','UIMS','123456','SetUserPwd','Welcome#123');
        Test.stopTest(); 
    }
    //method to test activate user using pin sent by API.
    static testmethod void confirmPinIdIsINActive() {
        IDMSConfirmPin confirm = new IDMSConfirmPin();
        User userno = new User(alias = 'userno', email='userno' + '@accenture.com',
                               emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                               localesidkey='en_US', profileid = System.label.CLFEB14SRV02, BypassWF__c = true,BypassVR__c = true, 
                               BypassTriggers__c = 'AP_WorkOrderTechnicianNotification;SVMX07;SVMX23;SVMX05;SVMX06;SVMX07;SRV04;SRV05;SRV06;AP54;AP_Contact_PartnerUserUpdate',
                               timezonesidkey='Europe/London', username='userno' + '@bridge-fo.com.devmajq3a',Company_Postal_Code__c='12345',
                               Company_Phone_Number__c='9986995000',FederationIdentifier ='308-Test123',
                               IDMS_Registration_Source__c = 'Test',IDMS_User_Context__c = '@Home',IDMS_PreferredLanguage__c= 'EN',IDMS_county__c = 'test',
                               IDMS_Email_opt_in__c = 'Y',isActive =false,IDMS_POBox__c = '1111',IDMSPinVerification__c='123456',IDMSNewPhone__c='99999999',IDMS_VU_NEW__c='1');
        
        insert userno ;
        userno.IDMSPinVerification__c = '123456';
        update userno; 
        Test.startTest();
        confirm.ConfirmPin(userno.id,'','idms','123456','SetUserPwd','');
        confirm.ConfirmPin(userno.id,'','idms','123456','SetUserPwd','Welcome#');
        confirm.ConfirmPin(userno.id,'','idms','123456','SetUserPwd','Welcome#123');
        confirm.ConfirmPin(userno.id,'','UIMS','123456','SetUserPwd','Welcome#123');
        Test.stopTest(); 
    }
}