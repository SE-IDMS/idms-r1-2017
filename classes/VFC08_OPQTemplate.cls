/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 02/11/2010
    Description     : Controller extension takes an 'Opportunity' record id and Axis and creates questions relevant to the Axis and Opportunity gate 
                        - if not previously existing.  Enables users to apply a score for each question upon submission.
                                    
*/
public with sharing class VFC08_OPQTemplate {
    //Global section
    private final Opportunity opp;
    private final String DecisionCriteria = 'DC';   //  DC = Decision Criteria (Attractiveness and Feasibility feature)
    private final String ProjectCategoryTool = 'PCT';  
    private final String AXIS;
    private final String GATE;
    private final Integer QUERY_LIMIT = 1000;       //There would never be this many records, but we set limit to ensure there are never any governor limits exceeded       
    private Integer DML_LIMIT = 1000;               //There would never be this many records to insert/update, but we set limit to ensure there are never any governor/performance implications
    public Boolean bTestFlag = false;               //Used in Test method only, used to simulate error thrown and this ensure complete code coverage
    public Boolean showWeight {get; set;}    {showWeight = false;}        //Used to render the VFP to show the section weight, question weight and also the appropriate header for Decision Criteria assessment
    
    
    //Constructor
    public VFC08_OPQTemplate(ApexPages.StandardController stdController) {
        if(Test.isRunningTest())
            DML_LIMIT = 15;             //Enforce coverage in test methods
            
        opp = (Opportunity)stdController.getRecord();
               
        if(opp != null && opp.OpptyType__c != System.Label.CL00074){
            
            if(ApexPages.currentPage().getParameters().get('Axis') != null){
                if(ApexPages.currentPage().getParameters().get('Axis') == DecisionCriteria){
                    this.AXIS = DecisionCriteria;
                    this.GATE = opp.TECH_Gate__c;
                    this.showWeight = true;
                }
                else if(ApexPages.currentPage().getParameters().get('Axis') == ProjectCategoryTool){
                    this.AXIS = ProjectCategoryTool;
                    this.GATE = opp.TECH_PCTGate__c;
                    this.showWeight = false;
                }
                this.sectionwrappers = getSections();   //initialise sectionwrappers collection with questions for this opp and axis - if exists   
            }
        }
    }
    
    //Method section
    public PageReference createQuestions(){
        PageReference pageref = new ApexPages.Standardcontroller(opp).view();
        if( opp.OpptyType__c != System.Label.CL00074)        // OpptyType <> Standard
        {                            
            List<OPQ_OpportunityQuestion__c> loq_to_create = new List<OPQ_OpportunityQuestion__c>();
            Database.SaveResult[] lsr = new List<Database.SaveResult>();
            try{
                if(AXIS != null && GATE != null && sectionwrappers.isEmpty()){
                    //only create questions if none already exist for this opportunity and axis
                    for(OPQ_AssessmentQuestion__c oaq : [select o.Id From OPQ_AssessmentQuestion__c o where o.Active__c=True and o.Assessment__r.TECH_Axis__c = :AXIS and o.Assessment__r.Gate__c = :GATE limit :QUERY_LIMIT]){
                        loq_to_create.add(new OPQ_OpportunityQuestion__c(Opportunity__c = opp.Id, AssessmentQuestion__c = oaq.Id));
                        if(loq_to_create.size() == DML_LIMIT)
                            break;
                    }
                    if(!loq_to_create.isEmpty()){
                        lsr = Database.insert(loq_to_create, false);
                        for(Database.SaveResult sr : lsr){
                            if(!sr.isSuccess() || (Test.isRunningTest() && bTestFlag)){
                                //add any errors to apex pages message for display on the associated Visualforce page
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, sr.getErrors()[0].getMessage()));
                            }
                        }
                        //update sectionwrapper property with updated values
                        this.sectionwrappers = getSections();
                    }
                }   
                if(!sectionwrappers.isEmpty())
                    pageref = null;                                 //ensure we do not redirect back to opp if we have questions
            }catch(Exception e){
                ApexPages.addMessages(e);
            }
        }
        else{
            pageref = null;                                        //ensure we do not redirect back to opp and display error msg for Std oppties
        }
        return pageref;
    }
    
    public List<VFC08_OPQTemplateWrapper> getSections(){
        List<VFC08_OPQTemplateWrapper> list_sections = new List<VFC08_OPQTemplateWrapper>();
        Integer i = 0;
        String section_id;
        // Below if statement executed for "Score Attractiveness/Feasibility" Button
        if(AXIS != null && GATE != null){
            //Returns the sections and question ordered by section position and question position
            for(OPQ_OpportunityQuestion__c oq :[select o.Id, o.Score__c,o.Active__c, o.AssessmentQuestion__r.Position__c, o.AssessmentQuestion__r.Name, o.AssessmentQuestion__r.Guideline__c, o.AssessmentQuestion__r.QuestionWeight__c, o.AssessmentQuestion__r.Assessment__c, o.AssessmentQuestion__r.Assessment__r.Id, o.AssessmentQuestion__r.Assessment__r.Name, o.AssessmentQuestion__r.Assessment__r.SectionWeight__c, o.AssessmentQuestion__r.Assessment__r.Position__c From OPQ_OpportunityQuestion__c o where o.Opportunity__c =: opp.Id and o.AssessmentQuestion__r.Assessment__r.TECH_Axis__c = :AXIS and o.Active__c =:Label.CL00074 and o.AssessmentQuestion__r.Assessment__r.Gate__c = :GATE order by o.AssessmentQuestion__r.Assessment__r.Position__c, o.AssessmentQuestion__r.Position__c limit :QUERY_LIMIT]){
                //The soql orders the sections, we populate into list in this same order for eventual display on VF page
                if(section_id == null || section_id != oq.AssessmentQuestion__r.Assessment__r.Id){
                    if(section_id != null && section_id != oq.AssessmentQuestion__r.Assessment__r.Id)
                        i++;
                    section_id = oq.AssessmentQuestion__r.Assessment__r.Id;
                    list_sections.add(new VFC08_OPQTemplateWrapper(oq));
                }else if(section_id == oq.AssessmentQuestion__r.Assessment__r.Id){
                    list_sections.get(i).questions.add(oq);
                }
            }
        }
        
        return list_sections;
    }
    
    public PageReference save(){
        List<OPQ_OpportunityQuestion__c> opp_questions_update = new List<OPQ_OpportunityQuestion__c>();
        Map<String,OPQ_OpportunityQuestion__c> mapForCategoryCheck = new Map<String,OPQ_OpportunityQuestion__c>();
        Database.SaveResult[] lsr = new List<Database.SaveResult>();
        PageReference pageref;
        try{
            for(VFC08_OPQTemplateWrapper sw : sectionwrappers){
                for(OPQ_OpportunityQuestion__c qw : sw.questions){
                    opp_questions_update.add(new OPQ_OpportunityQuestion__c(Id = qw.Id, Score__c = qw.Score__c));
                    mapForCategoryCheck.put(qw.AssessmentQuestion__r.Name,qw);
                    if(opp_questions_update.size() == DML_LIMIT)
                        break;
                }
            }
            
         //   system.debug('--->>>>>>>'+mapForCategoryCheck.keyset()+'--------'+mapForCategoryCheck.values()+'---------'+mapForCategoryCheck.get('Value').Score__c);
         //   system.debug('');
            
            if(AXIS == ProjectCategoryTool)
                calculateCategory(mapForCategoryCheck,opp);                   // Calculate Project category based on Gate, only for Project Category Axis
            
            if(!opp_questions_update.isEmpty()){
                lsr = Database.update (opp_questions_update, false);
                for(Database.SaveResult sr : lsr){
                    if(!sr.isSuccess() || (Test.isRunningTest() && bTestFlag)){
                        //add any errors to apex pages message for display on the associated Visualforce page
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, sr.getErrors()[0].getMessage()));
                    }
                }
            }
            
            
            
            if(!ApexPages.hasMessages())
            {
                pageref = new ApexPages.Standardcontroller(opp).view();         //navigate back to the opportunity detail page if no errors to display
                update opp;
            }
            
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
        
        return pageref;
    }
    
    public List<SelectOption> getScores() {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = OPQ_OpportunityQuestion__c.Score__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        if(AXIS == ProjectCategoryTool){
            ple.remove(ple.size()-1);           //This is done to explicitly remove the picklist value '5' from field Score__c for Axis=Project Category Tool as it requires score values from 1 to 4 only.
        }
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
   /* 
   public String getFeasibiltyConstant(){
        return this.FEASIBILITY;
    }
    
    public String getAttractivenessConstant(){
        return this.ATTRACTIVENESS;
    }
    
    public String getAxisConstant(){
        return this.AXIS;
    }*/
    
    
    public void calculateCategory(Map<String,OPQ_OpportunityQuestion__c> categoryCheck,Opportunity Opp){
    
          // Below is the formula to calculate “Project category S0” field of Opportunity, depending on the answers to the questions posed at Gate S0.
            if(AXIS == ProjectCategoryTool && GATE == 'S0'){
                
               if( 
                   (
                       (categoryCheck.get('Value').Score__c != null && Integer.valueof(categoryCheck.get('Value').Score__c) >= 3) && ( (categoryCheck.get('Customer').Score__c != null && Integer.valueof(categoryCheck.get('Customer').Score__c) == 4) || (categoryCheck.get('Technology').Score__c != null && Integer.valueof(categoryCheck.get('Technology').Score__c) >= 3) || (categoryCheck.get('Content').Score__c != null && Integer.valueof(categoryCheck.get('Content').Score__c) == 4) || (categoryCheck.get('Business Model').Score__c != null && Integer.valueof(categoryCheck.get('Business Model').Score__c) >= 3) )   
                   )
                    ||
                   (
                       (categoryCheck.get('Value').Score__c != null && Integer.valueof(categoryCheck.get('Value').Score__c) == 2) && (categoryCheck.get('Technology').Score__c != null && Integer.valueof(categoryCheck.get('Technology').Score__c) >= 3) 
                   )
                )
                opp.ProjectCategoryS0__c= 'A';
                else if(
                    (categoryCheck.get('Value').Score__c != null && Integer.valueof(categoryCheck.get('Value').Score__c) >= 1) && (categoryCheck.get('Customer').Score__c != null && Integer.valueof(categoryCheck.get('Customer').Score__c) <= 2) && (categoryCheck.get('Technology').Score__c != null && Integer.valueof(categoryCheck.get('Technology').Score__c) == 1) && (categoryCheck.get('Content').Score__c != null && Integer.valueof(categoryCheck.get('Content').Score__c) <= 2) && (categoryCheck.get('Business Model').Score__c != null && Integer.valueof(categoryCheck.get('Business Model').Score__c) == 1) 
                )
                opp.ProjectCategoryS0__c= 'C';
                else if ( 
                       categoryCheck.get('Value').Score__c != null || 
                       categoryCheck.get('Customer').Score__c != null ||
                       categoryCheck.get('Technology').Score__c != null ||
                       categoryCheck.get('Content').Score__c != null ||
                       categoryCheck.get('Business Model').Score__c != null ||
                       categoryCheck.get('Solution').Score__c != null
                )
                opp.ProjectCategoryS0__c= 'B';
                
              //system.debug('----->>>>>>>>>>>'+ opp.ProjectCategoryS0__c); 
          }
              
          // Below is the formula to calculate “Project category S1” field of Opportunity, depending on the answers to the questions posed at Gate S1. 
             if(AXIS == ProjectCategoryTool && GATE == 'S1'){
                
               if( 
                   (
                       (categoryCheck.get('Value').Score__c != null && Integer.valueof(categoryCheck.get('Value').Score__c) >= 3) && ( (categoryCheck.get('Billing').Score__c != null && Integer.valueof(categoryCheck.get('Billing').Score__c) == 4) || (categoryCheck.get('Contract').Score__c != null && Integer.valueof(categoryCheck.get('Contract').Score__c) == 4) || (categoryCheck.get('Schedule').Score__c != null && Integer.valueof(categoryCheck.get('Schedule').Score__c) == 4) ||  (categoryCheck.get('Customer').Score__c != null && Integer.valueof(categoryCheck.get('Customer').Score__c) == 4) || (categoryCheck.get('Suppliers').Score__c != null && Integer.valueof(categoryCheck.get('Suppliers').Score__c) == 4) || (categoryCheck.get('Technology').Score__c != null && Integer.valueof(categoryCheck.get('Technology').Score__c) >= 3) || (categoryCheck.get('Content').Score__c != null && Integer.valueof(categoryCheck.get('Content').Score__c) == 4) || (categoryCheck.get('Business Model').Score__c != null && Integer.valueof(categoryCheck.get('Business Model').Score__c) >= 3) )   
                   )
                    ||
                   (
                       (categoryCheck.get('Value').Score__c != null && Integer.valueof(categoryCheck.get('Value').Score__c) == 2) && (categoryCheck.get('Technology').Score__c != null && Integer.valueof(categoryCheck.get('Technology').Score__c) >= 3) 
                   )
                )
                opp.ProjectCategoryS1__c= 'A';
                else if(
                    (categoryCheck.get('Value').Score__c != null && Integer.valueof(categoryCheck.get('Value').Score__c) >= 1) && (categoryCheck.get('Billing').Score__c != null && Integer.valueof(categoryCheck.get('Billing').Score__c) <= 3) && (categoryCheck.get('Contract').Score__c != null && Integer.valueof(categoryCheck.get('Contract').Score__c) == 1) && (categoryCheck.get('Schedule').Score__c != null && Integer.valueof(categoryCheck.get('Schedule').Score__c) <= 2) &&  (categoryCheck.get('Customer').Score__c != null && Integer.valueof(categoryCheck.get('Customer').Score__c) <= 2) && (categoryCheck.get('Suppliers').Score__c != null && Integer.valueof(categoryCheck.get('Suppliers').Score__c) == 1) && (categoryCheck.get('Technology').Score__c != null && Integer.valueof(categoryCheck.get('Technology').Score__c) == 1) && (categoryCheck.get('Content').Score__c != null && Integer.valueof(categoryCheck.get('Content').Score__c) <= 2) && (categoryCheck.get('Business Model').Score__c != null && Integer.valueof(categoryCheck.get('Business Model').Score__c) == 1)
                )
                opp.ProjectCategoryS1__c= 'C';
                else if ( categoryCheck.get('Value').Score__c != null ||
                          categoryCheck.get('Duration').Score__c != null ||
                          categoryCheck.get('Billing').Score__c != null ||
                          categoryCheck.get('Solution').Score__c != null ||
                          categoryCheck.get('Contract').Score__c != null ||
                          categoryCheck.get('Schedule').Score__c != null || 
                          categoryCheck.get('Customer').Score__c != null ||
                          categoryCheck.get('Suppliers').Score__c != null ||
                          categoryCheck.get('Technology').Score__c != null ||
                          categoryCheck.get('Content').Score__c != null ||
                          categoryCheck.get('Business Model').Score__c != null
                )
                opp.ProjectCategoryS1__c= 'B';
                
          //    system.debug('----->>>>>>>>>>>'+ opp.ProjectCategoryS1__c);                 
          }
            
            // Below is the formula to calculate “Project category P0” field of Opportunity, depending on the answers to the questions posed at Gate P0. 
             if(AXIS == ProjectCategoryTool && GATE == 'P0'){
                
               if( 
                   (
                       (categoryCheck.get('Value').Score__c != null && Integer.valueof(categoryCheck.get('Value').Score__c) >= 3) && ( (categoryCheck.get('Billing').Score__c != null && Integer.valueof(categoryCheck.get('Billing').Score__c) == 4) || (categoryCheck.get('Contract').Score__c != null && Integer.valueof(categoryCheck.get('Contract').Score__c) == 4) || (categoryCheck.get('Financial').Score__c != null && Integer.valueof(categoryCheck.get('Financial').Score__c) == 4) || (categoryCheck.get('Schedule').Score__c != null && Integer.valueof(categoryCheck.get('Schedule').Score__c) == 4) ||  (categoryCheck.get('Customer').Score__c != null && Integer.valueof(categoryCheck.get('Customer').Score__c) == 4) || (categoryCheck.get('Suppliers').Score__c != null && Integer.valueof(categoryCheck.get('Suppliers').Score__c) == 4) || (categoryCheck.get('Technology').Score__c != null && Integer.valueof(categoryCheck.get('Technology').Score__c) >= 3) || (categoryCheck.get('Content').Score__c != null && Integer.valueof(categoryCheck.get('Content').Score__c) == 4) || (categoryCheck.get('Business Model').Score__c != null && Integer.valueof(categoryCheck.get('Business Model').Score__c) >= 3) )   
                   )
                    ||
                   (
                       (categoryCheck.get('Value').Score__c != null && Integer.valueof(categoryCheck.get('Value').Score__c) == 2) && (categoryCheck.get('Technology').Score__c != null && Integer.valueof(categoryCheck.get('Technology').Score__c) >= 3) 
                   )
                )
                opp.ProjectCategoryP0__c= 'A';
                else if(
                    (categoryCheck.get('Value').Score__c != null && Integer.valueof(categoryCheck.get('Value').Score__c) >= 1) && (categoryCheck.get('Billing').Score__c != null && Integer.valueof(categoryCheck.get('Billing').Score__c) <= 3) && (categoryCheck.get('Contract').Score__c != null && Integer.valueof(categoryCheck.get('Contract').Score__c) == 1) && (categoryCheck.get('Financial').Score__c != null && Integer.valueof(categoryCheck.get('Financial').Score__c) <= 2) && (categoryCheck.get('Schedule').Score__c != null && Integer.valueof(categoryCheck.get('Schedule').Score__c) <= 2) && (categoryCheck.get('Customer').Score__c != null && Integer.valueof(categoryCheck.get('Customer').Score__c) <= 2) && (categoryCheck.get('Suppliers').Score__c != null && Integer.valueof(categoryCheck.get('Suppliers').Score__c) == 1) && (categoryCheck.get('Technology').Score__c != null && Integer.valueof(categoryCheck.get('Technology').Score__c) == 1) && (categoryCheck.get('Content').Score__c != null && Integer.valueof(categoryCheck.get('Content').Score__c) <= 2) && (categoryCheck.get('Business Model').Score__c != null && Integer.valueof(categoryCheck.get('Business Model').Score__c) == 1) 
                )
                opp.ProjectCategoryP0__c= 'C';
                else if ( categoryCheck.get('Value').Score__c != null || 
                          categoryCheck.get('Duration').Score__c != null ||
                          categoryCheck.get('Billing').Score__c != null ||
                          categoryCheck.get('Solution').Score__c != null || 
                          categoryCheck.get('Contract').Score__c != null ||
                          categoryCheck.get('Financial').Score__c != null ||
                          categoryCheck.get('Schedule').Score__c != null ||
                          categoryCheck.get('Customer').Score__c != null ||
                          categoryCheck.get('Suppliers').Score__c != null ||
                          categoryCheck.get('Technology').Score__c != null ||
                          categoryCheck.get('Content').Score__c != null ||
                          categoryCheck.get('Business Model').Score__c != null
                )
                opp.ProjectCategoryP0__c= 'B';
                
          //    system.debug('----->>>>>>>>>>>'+ opp.ProjectCategoryP0__c);                  
          }
          
          return;
    }

    //Properties section   
    public List<VFC08_OPQTemplateWrapper> sectionwrappers {get; set;}
       
    //Inner Class - Wrapper class for encapsulating section and question data for display on Visualforce page VFP08_OASTemplate.
    private class VFC08_OPQTemplateWrapper {
        public String sectionName {get; set;}
        public Decimal sectionWeight {get; set;}
        public Decimal sectionPosition {get; set;}
        public List<OPQ_OpportunityQuestion__c> questions {get; set;}
        
        public VFC08_OPQTemplateWrapper(OPQ_OpportunityQuestion__c question){
            this.sectionName = question.AssessmentQuestion__r.Assessment__r.Name;
            this.sectionWeight = question.AssessmentQuestion__r.Assessment__r.SectionWeight__c;
            this.sectionPosition = question.AssessmentQuestion__r.Assessment__r.Position__c;
            this.questions = new List <OPQ_OpportunityQuestion__c>();
            this.questions.add(question);
        }
    }
}