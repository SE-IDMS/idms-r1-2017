/**************************
    Author: Fielo Team
***************************/
public class FieloPRM_AP_HeaderController{
    
    public Boolean displayPoints {get; set;}
    public String partnersName {get;set;}
    
    public static FieloEE__Member__c member = null;
     
    public FieloPRM_AP_HeaderController(){
        displayPoints = false;
        Id memId = FieloEE.MemberUtil.getMemberId();
        partnersName = getPartnerName(memId);
        displayPoints = IsRewardsPartner(memId);
    }

    public static string getPartnerName(String memOrContactId){
        String name = '';
        if(member == null){
            if(memOrContactId != null){
                member = getMemberByMemberOrContactId(memOrContactId); 
            }else{
                return name;
            }
        }
        name = member.FieloEE__FirstName__c + ' ' + member.FieloEE__LastName__c;
        if(name.length() > 20){
            name = member.FieloEE__FirstName__c;
            if(name.length() > 20){
                name = member.FieloEE__FirstName__c.abbreviate(20);
            }
        }
        return name;
    }

    public static boolean IsRewardsPartner(String memOrContactId){
        if(member == null){
            if(memOrContactId != null){    
                member = getMemberByMemberOrContactId(memOrContactId); 
            }else{
                return false;
            }
        }
        if(member.F_PRM_MemberFeatures__c != null && member.F_PRM_MemberFeatures__c.contains(label.CLPRMMAR16009)){
            system.debug('### Partner has Rewards Feature');
            return true;
        }
        return false;    
    }

    public static FieloEE__Member__c getMemberByMemberOrContactId(String memOrContactId){
        if(memOrContactId != null){
            return [SELECT Id, FieloEE__FirstName__c, FieloEE__LastName__c, F_PRM_MemberFeatures__c FROM FieloEE__Member__c WHERE Id =: memOrContactId OR FieloEE__User__r.ContactId =: memOrContactId];
        }else{
            return null;
        }   
    }

}