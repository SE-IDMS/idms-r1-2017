/*     
@Author: Deepak
Created Date: 19-12-2013
Description: Test class for AP_IsSVMXRelatedContact .
**********
*/
@isTest(SeeAllData=true)
Public Class AP_IsSVMXRelatedContact_test
{
    static testMethod void Test() 
    {
        Country__c testCountry = [Select name,id, countrycode__c from country__c where countrycode__C =: 'FR'];
  
        
        Account testAccount = Utils_TestMethods.createAccount();
        testAccount.Country__c = testCountry.Id;
        insert testAccount;
        
        Contact testContact = Utils_TestMethods.createContact(testAccount.Id,'testContact');
        testContact.Country__c = testCountry.Id;
        insert testContact;
        
            SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c();
        ip1.SVMXC__Company__c = testAccount.id;
        ip1.Name = 'Test Intalled Product ';
        ip1.SVMXC__Status__c= 'new';
        ip1.GoldenAssetId__c = 'GoledenAssertId1';
        ip1.BrandToCreate__c ='Test Brand';
       // ip1.SVMXC__Site__c = site1.id;
        insert ip1;
        
        
        
        Role__c srvrole = New Role__c();
        //srvrole.Name = 'test';
        srvrole.Account__c = testAccount.Id;
        srvrole.Contact__c = testContact.Id;
        srvrole.InstalledProduct__c = ip1.Id;
        insert srvrole ;
        
        list<Role__c> rolList = New List<Role__c>();
        rolList.add(srvrole);
        
        srvrole.Notes__c = 'abcdefgh';
        update srvrole;
        
        String Event;
        
        set<Id> conid = New set<Id>();
        conid.add(testContact.Id);
        
        AP_IsSVMXRelatedContact.UpdateContact(conid);
        if(rolList.size()>0)
        AP_IsSVMXRelatedContact.CheckDuplicateRole(rolList,Event);
        
    }
}