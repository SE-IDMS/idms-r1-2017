//****************************************************************************************************************** 
// Overall Purpose : Class used to check the new password policy UTILS_
// Created by      : Prashanth 
// Created Date    : 16/01/2017
// Modified by     : Prashanth| Date - 16/01/2017 | Version - 1.0    |  BR - BFOID-456  | Purpose - To check new Password policy       
//******************************************************************************************************************

Public class IdmsPasswordPolicy{

    public Boolean checkPolicyMetOrNot(String Password,String Id,String FedId){
        List<User> objUser=new List<User>();
        if(Id!=null && Id!=''){
            objUser=[Select Firstname,Lastname from User where Id=:Id Limit 1];
        }
        else if(FedId!=null && FedId!=''){
            objUser=[Select Firstname,Lastname from User where FederationIdentifier=:FedId Limit 1];
        }
        
        return checkPolicy(Password,objUser[0].FirstName,objUser[0].LastName);
    }
    
    public Boolean checkPolicyMetOrNotWithFNLN(String Password,String FirstName,String LastName){
        
        return checkPolicy(Password,FirstName,LastName);
    }
    
    public Boolean checkPolicy(String Password,String Firstname,String Lastname){
        Boolean bolPolicyFlag=true;
        
        Pattern repeatedPattern = Pattern.compile('.*(\\d)\\1{1}.*'); // repeated 
        Pattern sequentialPattern = pattern.compile('.*(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)|9(?=0)){1}\\d.*'); 
        
        matcher repeatedMatcher = repeatedPattern.matcher(Password);
        matcher sequentialMatcher = sequentialPattern.matcher(Password);
        
        //For 3 out of 4
        pattern myPattern = pattern.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$'); //ULN
        pattern myPattern2 = pattern.compile('^(?=.*[A-Z])(?=.*\\d)(?=.*(_|[^\\w])).+$'); //UNS
        pattern myPattern3 = pattern.compile('^(?=.*[a-z])(?=.*\\d)(?=.*(_|[^\\w])).+$'); //LNS
        pattern myPattern4 = pattern.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*(_|[^\\w])).+$');  //ULS
        
        matcher myMatcher1 = myPattern.matcher(Password);
        matcher mymatcher2 = myPattern2.matcher(Password);
        matcher myMatcher3 = myPattern3.matcher(Password);
        matcher myMatcher4 = myPattern4.matcher(Password);
        
        //New implementation
            if(Password.containsIgnoreCase(FirstName) || Password.containsIgnoreCase(LastName)){ 
                bolPolicyFlag=false;
            }
            else if(Password.length()<8){
                bolPolicyFlag=false;
            }
            else if(( String.valueOf(repeatedMatcher.matches()) == 'true' ) || ( String.valueOf(sequentialMatcher.matches()) == 'true')){
               bolPolicyFlag=false;
            }
            else if(!(( String.valueOf(myMatcher1.matches()) == 'true' ) || ( String.valueOf(myMatcher2.matches()) == 'true' ) || ( String.valueOf(myMatcher3.matches()) == 'true' ) || ( String.valueOf(myMatcher4.matches()) == 'true' ))){
              bolPolicyFlag=false;
            }
        
        return bolPolicyFlag;
    }
    
}