global with sharing class VFC1001_AddCommercialReferencesToProblem{ 
   
    public Map<String,String> pageParameters=system.currentpagereference().getParameters();
    public String jsonSelectString{get;set;}{jsonSelectString='';}
    public String jsonSelectFamily{get;set;}{jsonSelectFamily='';}
    Public Set<CommercialReference__c> lstOCs {get; set;}
    public Problem__c relatedProblem{get;set;}
    
    public VFC1001_AddCommercialReferencesToProblem(ApexPages.StandardController controller){
        
        if(relatedProblem==null && ApexPages.currentPage().getParameters().containsKey(Label.CL10053) && ApexPages.currentPage().getParameters().get(Label.CL10053)!=null)
        {
            relatedProblem=new Problem__c(id=ApexPages.currentPage().getParameters().get(Label.CL10053));
            //ProblemName=ApexPages.currentPage().getParameters().get(Label.CL10054);
        }  
        lstOCs = new Set<CommercialReference__c>();
    }
    
    public String scriteria{get;set;}{    
        MAP<String,String> pageParameters=ApexPages.currentPage().getParameters();
            if(pageParameters.containsKey('commercialReference') && pageParameters.get('commercialReference')!=null)
                scriteria=pageParameters.get('commercialReference');
    }
    
    public String gmrcode{get;set;}        
    
    @RemoteAction
    global static List<Object> remoteSearch(String searchString,String gmrcode,Boolean searchincommercialrefs,Boolean exactSearch,Boolean excludeGDP,Boolean excludeDocs,Boolean excludeSpares){
        WS_GMRSearch.GMRSearchPort GMRConnection = new WS_GMRSearch.GMRSearchPort();        
        WS_GMRSearch.filtersInDataBean filters=new WS_GMRSearch.filtersInDataBean();        
        WS_GMRSearch.paginationInDataBean Pagination=new WS_GMRSearch.paginationInDataBean();

        GMRConnection.endpoint_x=Label.CL00376;
        GMRConnection.timeout_x=40000;
        GMRConnection.ClientCertName_x=Label.CL00616;

        Pagination.maxLimitePerPage=99;
        Pagination.pageNumber=1;
        
        filters.exactSearch=exactSearch;
        filters.onlyCatalogNumber=searchincommercialrefs;
        
        filters.excludeOld=!(excludeGDP);
        filters.excludeMarketingDocumentation= !(excludeDocs);
        filters.excludeSpareParts=!(excludeSpares);  
    


        filters.keyWordList=searchString.split(' '); //prepare the search string
        if(gmrcode != null){
            if(gmrcode.length()==2){
                filters.businessLine = gmrcode;            
            }
            else if(gmrcode.length()==4){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine =  gmrcode.substring(2,4);            
            }
            else if(gmrcode.length()==6){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6);
            }
            else if(gmrcode.length()==8){
                filters.businessLine = gmrcode.substring(0,2);            
                filters.productLine = gmrcode.substring(2,4);
                filters.strategicProductFamily = gmrcode.substring(4,6); 
                filters.Family = gmrcode.substring(6,8);
            }
        }
        if(!Test.isRunningTest()){    
            WS_GMRSearch.resultFilteredDataBean results=GMRConnection.globalFilteredSearch(filters,Pagination);  
            return results.gmrFilteredDataBeanList;    
        }
        else{
            return null;
        }
    }
    
 
    
    @RemoteAction
    global static List<OPP_Product__c> getOthers(String business) {
        system.debug('&&& business'+ business);
        String query='SELECT  Name, TECH_PM0CodeInGMR__c  FROM OPP_Product__c WHERE IsActive__c =TRUE and TECH_PM0CodeInGMR__c like \''+business+'__\' ORDER BY Name ASC';
        system.debug('*** @Remote - query'+ query);
        return Database.query(query);       
    }
    
    Public pagereference performCheckbox(){ 
     
        system.debug('*** checkbox - enter');
       
        // Retrieve the selected parameters on the page
        jsonSelectString = Apexpages.currentPage().getParameters().get('jsonSelectString');
        jsonSelectFamily= Apexpages.currentPage().getParameters().get('jsonSelectFamily');
        lstOCs = new Set<CommercialReference__c>();
        System.debug('*** jsonSelectString' + jsonSelectString);
        System.debug('*** jsonSelectFamily' + jsonSelectFamily);
        
        if(jsonSelectString != '' && jsonSelectString !='[]'  && jsonSelectString != null) {
        
            List<WS_GMRSearch.gmrDataBean> DBL = (List<WS_GMRSearch.gmrDataBean>) JSON.deserialize(jsonSelectString, List<WS_GMRSearch.gmrDataBean>.class);
        
        
            System.debug('*** Select - enter');
            // Get the list of selected related products as GMRDataBean
           
                for(WS_GMRSearch.gmrDataBean DBL1 : DBL) {
                    // Create a Problem Related Record with information of selected Product
                        
                    CommercialReference__c oc1 = new CommercialReference__c();
              
                    oc1.Problem__c = relatedProblem.id;
                    oc1.Business__c = DBL1.businessline.label.UnEscapeXML();
                    oc1.ProductLine__c = DBL1.productLine.label.UnEscapeXML();
                    oc1.ProductFamily__c = DBL1.strategicProductFamily.label.UnEscapeXML();
                    oc1.Family__c = DBL1.family.label.UnEscapeXML();                          
                    oc1.SubFamily__c = DBL1.subFamily.label.UnEscapeXML();
                    oc1.ProductSuccession__c = DBL1.productSuccession.label.UnEscapeXML();
                    oc1.ProductGroup__c = DBL1.productGroup.label.UnEscapeXML();
                    oc1.ProductDescription__c = DBL1.description.UnEscapeXML();
                    oc1.CommercialReference__c = DBL1.commercialReference.UnEscapeXML();
                    
                    // GMR Code
                    if(DBL1.businessLine.value != null)
                    oc1.GMRCode__c = DBL1.businessLine.value;
                    if(DBL1.productLine.value != null)
                      oc1.GMRCode__c += DBL1.productLine.value;
                    if(DBL1.strategicProductFamily.label != null)
                      oc1.GMRCode__c += DBL1.strategicProductFamily.value;
                    if(DBL1.family.label != null)
                       oc1.GMRCode__c += DBL1.family.value;
                    if(DBL1.subFamily.label != null)
                       oc1.GMRCode__c += DBL1.subFamily.value;
                    if(DBL1.productSuccession.label != null)
                       oc1.GMRCode__c += DBL1.productSuccession.value;
                    if(DBL1.productGroup.label != null)
                       oc1.GMRCode__c += DBL1.productGroup.value;
                    // add to the list
                    lstOCs.add(oc1);
                    System.debug('*** lstOCs'+lstOCs);
                 }
    }    
        
    if( jsonSelectFamily != null && jsonSelectFamily!='[]' &&  jsonSelectFamily != '' ) {
        
            List<WS_GMRSearch.gmrDataBean> DBL = (List<WS_GMRSearch.gmrDataBean>) JSON.deserialize(jsonSelectFamily, List<WS_GMRSearch.gmrDataBean>.class);   
    
            System.debug('*** Family - enter');
            // Get the list of selected families as GMRDataBean
                             
              for(WS_GMRSearch.gmrDataBean DBL2 : DBL) {
                        // Create a Problem Related Record with information of selected Product
                        
                        CommercialReference__c oc2 = new CommercialReference__c();
               
                        oc2.Problem__c = relatedProblem.id;
                        oc2.Business__c = DBL2.businessLine.label.UnEscapeXML();
                        oc2.ProductLine__c = DBL2.productLine.label.UnEscapeXML();
                        oc2.ProductFamily__c = DBL2.strategicProductFamily.label.UnEscapeXML();
                        oc2.Family__c = DBL2.family.label.UnEscapeXML();
                        oc2.SubFamily__c = '';
                        oc2.ProductSuccession__c = '';
                        oc2.ProductGroup__c = '';
                        oc2.ProductDescription__c = '';
                        oc2.CommercialReference__c = '';
                        
                        // GMR Code
                        if(DBL2.businessLine.value != null)
                        oc2.GMRCode__c = DBL2.businessLine.value;
                        if(DBL2.productLine.value != null)
                          oc2.GMRCode__c += DBL2.productLine.value;
                        if(DBL2.strategicProductFamily.label != null)
                          oc2.GMRCode__c += DBL2.strategicProductFamily.value;
                        if(DBL2.family.label != null)
                           oc2.GMRCode__c += DBL2.family.value;
                           
                        // add to the list
                        lstOCs.add(oc2);
                        System.debug('*** lstOCs'+lstOCs);   
                    }
        }   
        
        System.debug('*** lstOCs'+lstOCs);
        
        try {
        Database.SaveResult[] CRInsertResult = Database.insert(new List<CommercialReference__c>(lstOCs), true);
                                
        PageReference CRPage;            
        CRPage=new PageReference('/'+ relatedProblem.id);                        
        return CRPage;
        }
        catch(Exception e) {
            System.Debug('*** Catch - Error Inserting: ' + e.getMessage());
            ApexPages.Message insertErrorMessage = new ApexPages.message(ApexPages.severity.ERROR, e.getMessage() + ' ' + 'Please contact System Admin.');
            ApexPages.addMessage(insertErrorMessage);
            return null;
        }
    }   
    
    public PageReference pageCancelFunction() {
        PageReference pageResult;
        if(relatedProblem.id!=null){
            pageResult = new PageReference('/'+ relatedProblem.id);
             
        }
        else{
           return new PageReference('/home.jsp');
        }
        return pageResult;
    }
}