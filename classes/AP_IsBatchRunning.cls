public class AP_IsBatchRunning {
    
    
    @InvocableMethod(label='Is it a Batch Run' description='This method is called from flow on User')
    public static List<Boolean> checkBatch(){
    
        system.debug('**IsBatch Running method entry**--->System.isBatch()::'+System.isBatch());
        List<Boolean> booleanLst = new List<Boolean>{System.isBatch()};
        System.debug('**returned**'+booleanLst);
        
        return booleanLst;
    }
}