@IsTest
public class VFC_PRMLogoutTest{
    static testMethod void testUrl(){
        User us = new user(id = userinfo.getuserId());
        us.BypassVR__c = true;
        update us;
        
        System.RunAs(us){
            FieloEE.MockUpFactory.setCustomProperties(false);
            FieloEE__Triggers__c deactivate = new FieloEE__Triggers__c(
                FieloEE__Member__c = false
            );
            insert deactivate;

            Country__c c = new Country__c(Name = 'USA', CountryCode__c = 'US');
            insert c;

            PRMCountry__c prmCountry = new PRMCountry__c(Country__c=c.Id,PLDataPool__c='en_US2',DefaultLandingPage__c='http://www.partners.us',CountryPortalEnabled__c=true);
            insert prmCountry;

             Country__c cww = new Country__c(Name = 'Global', CountryCode__c = 'WW');
            insert cww;
            PRMCountry__c prmCountryWW = new PRMCountry__c(Country__c=cww.Id,PLDataPool__c='ww_WW2',DefaultLandingPage__c='http://www.global.com',CountryPortalEnabled__c=true);
            insert prmCountryWW;
            
             Account acc = Utils_TestMethods.createAccount();
             insert acc;
                    
            FieloEE__Member__c mem = new FieloEE__Member__c(
                FieloEE__FirstName__c = 'test',
                FieloEE__LastName__c= 'test',
                F_Country__c = c.Id
            );
            insert mem;
            
            
            PageReference pageRef = Page.VFP_PartnersLogout;
            Test.setCurrentPage(pageRef);

            Test.startTest();
            VFC_PRMLogout prmLogout = new VFC_PRMLogout();
            String url = prmLogout.getRedirectURL();
            // System.assertEquals('http://www.global.com',url);

            FieloEE__FrontEndSessionData__c memb = new FieloEE__FrontEndSessionData__c(FieloEE__MemberId__c= mem.Id);
            insert memb;
            FieloEE.MemberUtil.setmemberId(mem.Id);

            prmLogout = new VFC_PRMLogout();
            url = prmLogout.getRedirectURL();
            // System.assertEquals('http://www.partners.us',url);


            // System.assertEquals(0,ApexPages.currentPage().getCookies().size());

            String cookiesCommaSeparated = System.label.CLPRMMAR16008;
            for(String cookieName: cookiesCommaSeparated.split(',')){
                if(ApexPages.currentPage().getCookies().containsKey(cookieName)){
                    Cookie aux = new Cookie('key', 'value', null, 31536000, false);
                    ApexPages.currentPage().setCookies(new Cookie[]{aux});
                }
            }
            //Number of cookies in page must
            // System.assertNotEquals(cookiesCommaSeparated.countMatches(',')+1,ApexPages.currentPage().getCookies().size());
            prmLogout.getFlushCookies();
            // System.assertEquals(0,ApexPages.currentPage().getCookies().size());
            Test.stopTest();
        }
        
    }
}