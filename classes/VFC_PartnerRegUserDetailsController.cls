/**
*   Created By: akhilesh bagwari
*   Created Date: 27/03/2016
*   Modified by:  02/10/2016 
•   Description: This class is used to create IDMS I2P users. 
**/
public class VFC_PartnerRegUserDetailsController {
    
    // These four member variables maintain the state of the wizard.
    // When users enter data into the wizard, their input is stored
    // in these variables. 
    User user;
    public User u{get;set;}
    public String email {get; set;}
    // public String birthdate{get; set;}
    public String cnfemail {get; set;}
    public String errorMesg = 'Email id already exists';
    public String country{get; set;}
    public String language{get; set;}
    public Boolean showFirstname{get;set;}
    public Boolean hidefields{get;set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String firstName{get; set;}
    public String lastname{get; set;}
    public String businessType{get; set;}
    public String areOfFocus{get; set;}
    public String phomenum{get; set;}
    public String phoneType{get; set;}
    public String jobFun{get; set;}
    public String jobTitle{get; set;}
    public String taxId{get; set;}
    public User usr{get; set;}
    public Boolean hidefieldsEmail{get;set;}
    public Boolean notvalid{get;set;}
    public Boolean checked{get;set;}
    public String gridOut{get; set;}
    public Boolean popUPrender{get;set;}
    public Boolean displayPopup{get;set;}
    
    public List<SelectOption> statusOptions{get;set;}
    public List<SelectOption> statusOptionsLang{get;set;}
    public List<SelectOption> statusOptionsPhoneType{get;set;}
    public List<SelectOption> statusOptionsJobType{get;set;}
    public List<SelectOption> statusOptionsJobFunction{get;set;}
    
    public List<SelectOption> options_country {get; set;}
    public List<SelectOption> statusOptionsBus{get;set;}
    public List<SelectOption> mylist2{get;set;}
    public Map<String,List<String>> objResults1{get;set;}
    public List<SelectOption> statusOptionsAf{get;set;}
    public List<String> value1{get;set;}
    public String reg1Check{get; set;}
    
    public void autoRun()
    {
    }
    
    
    public void getFieldsPartner()
    {   
        
    }
    
    TStringUtils s=new TStringUtils();
    TStringUtils s1=new TStringUtils();
    
    public void getAf(){
    }
    
    //added for email exist redirect
    public PageReference redirectPopup()
    {
        displayPopup 	= false;
        String url		=Label.IDMS_RedirectLogin;
        showFirstname	=false;
        PageReference pg = new PageReference(url);
        pg.setRedirect(true);        
        return pg;
    }
    
    public VFC_PartnerRegUserDetailsController (){
        u = new User();
        //added to fix default country issue
        usr = new User();
        if(usr.Country__c == null){
            usr.Country__c = 'US';
        }
        if(usr.LanguageLocaleKey == null){
            usr.LanguageLocaleKey = 'en_US';
        }
        
        showFirstname = false;
        
        String url = '<a href='+ Label.IDMS_Term_Condition + ' target=_blank>Terms & Conditions</a>';
        gridOut='color:white;background-color:green;background-image:none;;float:left;';
        
        //insert c;
        reg1Check='I have read, understood, and agree to the '+url+ ' including those relating to the associated communications.';     
    }
    
    //method for email validation
    private boolean isValidEmail() {
        return email == cnfemail;
    }
    
    public  PageReference gotoPage()
    {
        if (!isValidEmail()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Email and Confirm Email does not match');
            showFirstname=false;
            popUPrender=false;
            ApexPages.addMessage(msg);
            return null;
        }
        
        if(!Pattern.matches('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$', email))
        {
            notvalid	= true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter a valid email address')); 
            popUPrender	= false;
        }
        
        else{notvalid=false;}
        List<User> existingContacts = [SELECT id, email FROM User WHERE email = :email];
        
        if(existingContacts.size() > 0)
        {
            displayPopup = true;
            popUPrender= true;
            showFirstname=false;
        }
        
        else{
            popUPrender=false;
            if((existingContacts.size() == 0) && (Pattern.matches('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$', email))){
                showFirstname= true;
                hidefieldsEmail=true;
                
                gridOut='color:white;background-color:grey;background-image:none;;float:left;';
            }
            
        }
        
        
        return null;
        
    }
    
    // The next four methods return one of each of the four member
    // variables. If this is the first time the method is called,
    // it creates an empty record for the variable.
    // The next three methods control navigation through
    // the wizard. Each returns a PageReference for one of the three pages
    // in the wizard. Note that the redirect attribute does not need to
    // be set on the PageReference because the URL does not need to change
    // when users move from page to page.
    
    
    public PageReference step2() {
        return Page.VFP_PartnerRegistrationUserDetails ;
    }
    
    public PageReference step3() {
        
        if (firstName=='null'||firstName==null||firstName==''||lastName=='null'||lastName==null||lastName=='') {
            ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, 'First Name and Last Name should not be blank.');
            ApexPages.addMessage(msgareaOfF);
            showFirstname= true;
            return null;
        }
        
        //phone no. format check
        if (phomenum!=''){
            Boolean result;
            String PhValid= '^([0-9\\(\\)\\/\\+ \\-]*)$';
            Pattern mypattern=Pattern.compile(PhValid);
            Matcher myMatcher=mypattern.matcher(phomenum);
            result=myMatcher.matches();
            If(result==false){
                ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a valid phone number. Do not include letters in your phone number. Acceptable special characters include: + - ( ) or a space.');
                ApexPages.addMessage(msgareaOfF);
                showFirstname= true;
                return null;
            }
            else {result = true;}}
        
        if (checked==false) {
            ApexPages.Message msgntChkd = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please accept the terms and conditions.');
            
            ApexPages.addMessage(msgntChkd );
            return null;
        }
        
        
        
        if (phoneType==null || phoneType.contains('Select')||phoneType=='null' ) {
            phoneType='';
            showFirstname= true;
        }    
        return Page.VFP_PartnerRegistrationCompanyDetails;
    }
    
    public PageReference step4() {
        return Page.emailVerify;
    }
    
    public PageReference backToFirstPage() {
        return Page.VFP_PartnerRegistrationUserDetails;
    }
    
    //password validation
    private boolean isValidPassword() {
        return password == confirmPassword;
    }
    
    // This method performs the final save for all four objects, and
    // then navigates the user to the detail page for the new
    // opportunity.
    public PageReference save() {    
        //validate company phone no. format
        if (acc_phone!=''){
            Boolean result;
            String PhValid= '^([0-9\\(\\)\\/\\+ \\-]*)$';
            Pattern mypattern=Pattern.compile(PhValid);
            Matcher myMatcher=mypattern.matcher(acc_phone);
            result=myMatcher.matches();
            If(result==false){
                ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a valid company phone number. Do not include letters in your company phone number. Acceptable special characters include: + - ( ) or a space.');
                ApexPages.addMessage(msgareaOfF);
                //showFirstname= true;
                return null;
            }
            else {result = true;}}
        
        //validate postal code
        if (acc_postalCode!=''){
            Boolean resultZip;
            String zipValid= '^[0-9]{5}(?:-[0-9]{4})?$';
            Pattern mypatternZip=Pattern.compile(zipValid);
            Matcher myMatcherZip=mypatternZip.matcher(acc_postalCode);
            resultZip=myMatcherZip.matches();
            If(resultZip==false){
                ApexPages.Message msgareaOfF = new ApexPages.Message(ApexPages.Severity.ERROR, 'Zip code should be in XXXXX or XXXXX-XXXX format');
                ApexPages.addMessage(msgareaOfF);
                //showFirstname= true;
                return null;
            }
            else {resultZip= true;}}
        
        //duplicate nickname check
        String nicknameCheck=firstName+'.'+lastname;
        List<User> nicknameList=[Select CommunityNickname from User where CommunityNickname=:nicknameCheck];
        If(nicknameList.size()>0){
            Integer nickSize= nicknameList.size();
            Integer add=nickSize+1;
            nicknameCheck=nicknameCheck+add;    
        }
        
        // it's okay if password is null - we'll send the user a random password in that case
        list<profile> profiles=[select id from profile where name=:Label.IDMS_partner_profile_registration];        
        String profileId='';
        for(profile p:profiles){
            profileId=p.id;    
        }
        
        
        User u1 = new User();
        u1.Username = email;
        u1.Email = email;
        u1.CommunityNickname = email;
        u1.Country__c=usr.Country__c;
        u1.LanguageLocaleKey=usr.LanguageLocaleKey;
        u1.firstName=firstName;
        u1.lastName=lastname;
        u1.Business_Type__c=usr.Business_Type__c;
        u1.Area_of_focus__c=usr.Area_of_focus__c;
        u1.Phone=phomenum;
        u1.Phone_Type__c=usr.Phone_Type__c;
        u1.Job_Function__c=usr.Job_Function__c;
        u1.Job_Title__c=usr.Job_Title__c;
        u1.Tax_ID__c=taxId;
        u1.CompanyName=acc_CompanyName;
        u1.Company_Address1__c=acc_address1;
        u1.Company_Address2__c=acc_address2;
        u1.Company_Country__c=usr.Company_Country__c;
        u1.Company_Phone_Number__c=acc_phone;
        u1.Company_Postal_Code__c=acc_postalCode;
        u1.Company_State__c=acc_state;
        u1.Company_Website__c=acc_website;
        u1.Company_City__c=acc_city;
        list<account> accId=[select id from account where name=:Label.CL_Default_Account];
        String defaultAccountID='';
        
        for (account a: accId){    
            defaultAccountID=a.id;
        }
        
        String userId = Site.createExternalUser(u1, defaultAccountID, password);
        try{ VFC_PartnerRegUserDetailsController.setPermission(userId); }
        Catch(Exception ex){
            system.debug('---Exception---'+ex.getmessage());
        }
        if (userId != null) { 
            if (password != null && password.length() > 1) {
                return Site.login(u1.Username, password, null);
            }
            else {
                PageReference page = System.Page.emailVerify;
                page.setRedirect(true);
                return page;
            }
            PageReference page = System.Page.emailVerify;
            page.setRedirect(true);
            return page;
        }
        return null;
    }
    
    @future
    public static void setPermission(String usrID){
        String PSNames = Label.CL_PermissionSetNames;
        List<string> ListPSNames = PSNames.split(';');
        List<PermissionSet> LstPSRecords = [select id,name from PermissionSet where Name in :ListPSNames limit 1000];
        List<PermissionSetAssignment> ListPSAssignment = new List<PermissionSetAssignment>();
        PermissionSetAssignment PSA;
        if(LstPSRecords != null && LstPSRecords.size() > 0){
            for(PermissionSet PS:LstPSRecords){
                PSA = new PermissionSetAssignment();
                PSA.PermissionSetId  = PS.id;
                PSA.AssigneeId = usrID;
                ListPSAssignment.add(PSA);
                
            }
            
        }
        if(ListPSAssignment != null && ListPSAssignment.size() >0){
            Database.insert(ListPSAssignment,false);
        }
        
    }
    
    
    
    //company code sud
    public boolean isPublicCompany { get; set; }
    public Account account;
    public boolean companyFound{ get; set; }
    public string msg {get; set;}
    public string acc_AccountName{get; set;}
    public string acc_CompanyName {get; set;}
    public string acc_phone {get; set;}
    public string acc_address1 {get; set;}
    public string acc_address2 {get; set;}
    public string acc_city {get; set;}
    public String acc_state {get; set;}
    public String acc_country {get; set;}
    public String acc_country1 {get; set;}
    public string acc_postalCode {get; set;}
    public string acc_website {get; set;}
    public list<Account> account_details{get; set;}
    public String invitationCode{ get; set; }
    public string errorMsg {get; set;}
    
}