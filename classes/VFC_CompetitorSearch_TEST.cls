@isTEST
public class VFC_CompetitorSearch_TEST {
    
    static testMethod void Searchtestmethod() {
        
        Account A = Utils_TestMethods.createAccount();
        INSERT A;
        
        Opportunity O = Utils_TestMethods.createOpportunity(A.Id);
        INSERT O; 
        
        Competitor__c C = Utils_TestMethods.createCompetitor();
        INSERT C;
        
        OPP_OpportunityCompetitor__c OC = Utils_TestMethods.createOppCompetitor(C.ID, O.id);
        //INSERT OC;
        
        //for VFP_CompetitorSearch page and VFC_CompetitorSearch controller
        PageReference pageRef = Page.VFP_CompetitorSearch;
        Test.setCurrentPage(pageRef);
        
        String url = pageRef.getUrl();
        pageRef.getParameters().put('String',url); //passing the URL of the page to the constructor 
        
        ApexPages.StandardController sc = new ApexPages.StandardController(OC);
        VFC_CompetitorSearch CS = new VFC_CompetitorSearch(sc);
        
        String searchText = C.Name;
        VFC_CompetitorSearch.getRecords(searchText);
        CS.SaveandNew();   
        //CS.Save(); 
        
        //for sourcePage=OpptyLost
        PageReference pageRef1 = Page.VFP_CompetitorSearch;
        Test.setCurrentPage(pageRef1);
        
        String url1 = pageRef1.getUrl();
        pageRef1.getParameters().put('String',url1); //passing the URL of the page to the constructor 
        pageRef1.getParameters().put('sourcePage','OpptyLost');
        pageRef1.getParameters().put(Label.CL00452,O.id);
        pageRef1.getParameters().put('status','Lost');
        pageRef1.getParameters().put('reason','Price');
        pageRef1.getParameters().put('amount','1000.00');
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(OC);
        VFC_CompetitorSearch CS1 = new VFC_CompetitorSearch(sc);
        
        String searchText1 = C.Name;
        VFC_CompetitorSearch.getRecords(searchText1);
        CS1.SaveandNew();   
        //CS1.Save();
    }
    
    static testMethod void Searchtestmethod2() {
        
        Account A = Utils_TestMethods.createAccount();
        INSERT A;
        
        Opportunity O = Utils_TestMethods.createOpportunity(A.Id);
        INSERT O; 
        
        Competitor__c C = Utils_TestMethods.createCompetitor();
        INSERT C;
        
        OPP_OpportunityCompetitor__c OC = Utils_TestMethods.createOppCompetitor(C.ID, O.id);
        //INSERT OC;
        
        //for VFP_CompetitorSearch page and VFC_CompetitorSearch controller
        PageReference pageRef = Page.VFP_CompetitorSearch;
        Test.setCurrentPage(pageRef);
        
        String url = pageRef.getUrl();
        pageRef.getParameters().put('String',url); //passing the URL of the page to the constructor 
        
        ApexPages.StandardController sc = new ApexPages.StandardController(OC);
        VFC_CompetitorSearch CS = new VFC_CompetitorSearch(sc);
        
        String searchText = C.Name;
        VFC_CompetitorSearch.getRecords(searchText);
        //CS.SaveandNew();   
        //CS.Save(); 
        
        //for sourcePage=OpptyLost
        PageReference pageRef1 = Page.VFP_CompetitorSearch;
        Test.setCurrentPage(pageRef1);
        
        String url1 = pageRef1.getUrl();
        pageRef1.getParameters().put('String',url1); //passing the URL of the page to the constructor 
        pageRef1.getParameters().put('sourcePage','OpptyLost');
        pageRef1.getParameters().put(Label.CL00452,O.id);
        pageRef1.getParameters().put('status','Lost');
        pageRef1.getParameters().put('reason','Price');
        pageRef1.getParameters().put('amount','1000.00');
        
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(OC);
        VFC_CompetitorSearch CS1 = new VFC_CompetitorSearch(sc);
        
        String searchText1 = C.Name;
        VFC_CompetitorSearch.getRecords(searchText1);
        //CS1.SaveandNew();   
        CS1.Save();
    }
    
    static testMethod void Searchtestmethod3() {
        
        Account A = Utils_TestMethods.createAccount();
        INSERT A;
        
        Opportunity O = Utils_TestMethods.createOpportunity(A.Id);
        INSERT O; 
        
        Competitor__c C = Utils_TestMethods.createCompetitor();
        INSERT C;
        
        OPP_OpportunityCompetitor__c OC = Utils_TestMethods.createOppCompetitor(C.ID, O.id);
        INSERT OC;
        
        //for VFP_CompetitorSearch page and VFC_CompetitorSearch controller
        PageReference pageRef = Page.VFP_CompetitorSearch;
        Test.setCurrentPage(pageRef);
        
        String url = pageRef.getUrl();
        pageRef.getParameters().put('String',url); //passing the URL of the page to the constructor 
        
        ApexPages.StandardController sc = new ApexPages.StandardController(OC);
        VFC_CompetitorSearch CS = new VFC_CompetitorSearch(sc);
        
        String searchText = C.Name;
        VFC_CompetitorSearch.getRecords(searchText);
        CS.SaveandNew();   
        CS.Save(); 
        
    }
    
    /*
    static testMethod void Exceptiontestmethod() {
        
        Account A = Utils_TestMethods.createAccount();
        INSERT A;
        
        Opportunity O = Utils_TestMethods.createOpportunity(A.Id);
        INSERT O; 
        
        Competitor__c C = Utils_TestMethods.createCompetitor();
        C.Active__c = FALSE;
        INSERT C;        
        
        OPP_OpportunityCompetitor__c OC = Utils_TestMethods.createOppCompetitor(C.ID, O.id);
        //INSERT OC;       
        
        //for VFP_CompetitorSearch page and VFC_CompetitorSearch controller
        PageReference pageRef = Page.VFP_CompetitorSearch;
        Test.setCurrentPage(pageRef);
        
        String url = pageRef.getUrl();
        pageRef.getParameters().put('Id',O.id); //passing the URL of the page to the constructor 
        //Other parameters?
        
        ApexPages.StandardController sc = new ApexPages.StandardController(OC);
        VFC_CompetitorSearch CS = new VFC_CompetitorSearch(sc);
       
        //PageReference pr = new pageReference('');
        //CS.queryString = null;
        cs.SaveAndNew();
    }*/
}