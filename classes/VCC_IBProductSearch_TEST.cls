@isTest
private class VCC_IBProductSearch_TEST{

    static testMethod void VCC_IBProductSearchTest(){
        //Start of Test Data preparation

        Account account1 = Utils_TestMethods.createAccount();
        insert account1;

        SVMXC__Site__c location1 = new SVMXC__Site__c();
        location1.Name = 'Test Location';
        location1.SVMXC__Location_Type__c = 'SITE';
        location1.SVMXC__Account__c = account1.id;
        location1.TimeZone__c = 'Europe/Paris';
        insert location1;
        
        SVMXC__Installed_Product__c asset1 = new SVMXC__Installed_Product__c();
        asset1.Name = 'Test Asset';
        asset1.SVMXC__Company__c = account1.id;
        asset1.SVMXC__Site__c = location1.id;
        asset1.SVMXC__Status__c = 'Installed';
        asset1.BrandToCreate__c = 'Schneider Electric';
        asset1.DeviceTypeToCreate__c = 'Circuit Breaker';
        insert asset1;

        string [] searchfilterList= new string[]{};

        for(Integer token=0;token<17;token++){
            searchfilterList.add('a');          
        }

        List<String> keywords = new List<String>();
        Utils_DummyDataForTest dummydata = new Utils_DummyDataForTest();
        Utils_DataSource.Result  tempresult = new Utils_DataSource.Result();
        tempresult = dummydata.Search(keywords,keywords);

        sObject tempsObject = tempresult.recordList[0];
        //End of Test Data preparation.

        Test.startTest();
        ApexPages.StandardController page1 = new ApexPages.StandardController(asset1);
        PageReference pageRef= Page.VFP_SearchProductCatalog; 
        PageReference pageRef1= Page.VFP_SearchProductCatalog; 


        VFC_ControllerBase basecontroller = new VFC_ControllerBase();
        VFC_SearchProductCatalog ProductCaseController = new VFC_SearchProductCatalog(page1);

        System.debug('~~~~~~~~~~Start of Test~~~~~~~~~~');
        VCC_IBProductSearch ProductforCase = new VCC_IBProductSearch();
        //VCC_IBProductSearch ProductforCase1 = new VCC_IBProductSearch(asset1);
        //VCC06_DisplaySearchResults componentcontroller1 =ProductforCase1.resultsController; 

        VCC05_DisplaySearchFields componentcontroller = new VCC05_DisplaySearchFields(); 

        componentcontroller.pickListType = 'IB_PRODUCT';
        componentcontroller.SearchFilter = new String[4];
        componentcontroller.SearchFilter[0] = 'Schneider Electric';  
        
        componentcontroller.searchText = 'search\\stest';
        componentcontroller.key = 'searchComponent';
        //componentcontroller.PageController = ProductforCase1;
        componentcontroller.init();
        System.debug('#### COMPONENT CONTROLLER : ' + componentcontroller);
        //ProductforCase1.searchController = componentcontroller; 
        //System.debug('#### PAGE COMPONENT CONTROLLER : ' + ProductforCase1.searchController);    
        pageRef.getParameters().put('key','searchComponent');
        test.setCurrentPageReference(pageRef);  
        componentcontroller.searchText = 'search\\stest';
        componentcontroller.SearchFilter[0] = 'Schneider Electric';  

      
        Test.stopTest();
    }
}