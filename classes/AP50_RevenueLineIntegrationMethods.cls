/*
    Author          : Mathieu Gaudenzi
    Date Created    : 12/04/2012
    Description     : Update a Revenue Line in the DataBase
*/

public class AP50_RevenueLineIntegrationMethods 
{
    /** Convention for Product as DataTamplate__c :
        Field1__c : Business Unit
        Field2__c : Product Line
        Field3__c : Product Family
        Field4__c : Family 
        Field5__c : Sub Family
        Field6__c : Product Succession
        Field7__c : Product Group
        Field8__c : Commercial Reference
        Field9__c : Product Description
    **/

     public static List<String> InsertRevenueLine(Map<Opportunity, DataTemplate__c> ObjMap)
    {
        
        system.debug('#################################### InsertRevenueLine '+ObjMap);
        List<Revenue_Line__c> Products = new List<Revenue_Line__c>();
    
        For(Opportunity Obj:ObjMap.keyset())
        {
            DataTemplate__c InputProduct = (DataTemplate__c)ObjMap.get(Obj);
            Revenue_Line__c Product = new Revenue_Line__c();
    
            Product.Opportunity__c = Obj.Id;
        
            if(InputProduct.field1__c != null)
                Product.ProductBU__c = InputProduct.field1__c;
                
            if(InputProduct.field2__c != null)
                Product.ProductLine__c = InputProduct.field2__c;  
                
            if(InputProduct.field3__c != null)
                Product.ProductFamily__c = InputProduct.field3__c;  
                
            if(InputProduct.field4__c != null)
                Product.Family__c = InputProduct.field4__c;   
            
            if(InputProduct.field8__c != null)
                Product.TECH_CommercialReference__c = InputProduct.field8__c;  
    
            if(InputProduct.field9__c != null)
                Product.ProductDescription__c = InputProduct.field9__c;   
                
            
            
            Product.CurrencyIsoCode = Obj.CurrencyIsoCode;
            
            Products.add(Product);    
       }
       
       List<Database.SaveResult> RevenueLineInsert = Database.Insert(Products);
       
       List<String> ProductIDs = new List<String>();
       For(Database.SaveResult DSR:RevenueLineInsert)
       {
           ProductIDs.add(DSR.getID());
       }
       
       return ProductIDs;
    } 
    
    
    // Return dependent picklist value 
    public static List<String> UpdateRL(Map<Revenue_Line__c, DataTemplate__c> ObjMap)
    {
        List<Revenue_Line__c> Products = new List<Revenue_Line__c>();
    
        For(Revenue_Line__c Product:ObjMap.keyset())
        {
            DataTemplate__c InputProduct = (DataTemplate__c)ObjMap.get(Product);
    
            if(InputProduct.field1__c != null)
                Product.ProductBU__c = InputProduct.field1__c;
                
            if(InputProduct.field2__c != null)
                Product.ProductLine__c = InputProduct.field2__c;  
                
            if(InputProduct.field3__c != null)
                Product.ProductFamily__c = InputProduct.field3__c;  
                
            if(InputProduct.field4__c != null)
                Product.Family__c = InputProduct.field4__c;   
            
            if(InputProduct.field8__c != null)
               Product.TECH_CommercialReference__c = InputProduct.field8__c;  
    
            if(InputProduct.field9__c != null)
               Product.ProductDescription__c = InputProduct.field9__c; 
            
                 
            
            Products.add(Product);    
       }
       
       List<Database.SaveResult> RevenueLineInsert = Database.Update(Products);
       
       List<String> ProductIDs = new List<String>();
       For(Database.SaveResult DSR:RevenueLineInsert)
       {
           ProductIDs.add(DSR.getID());
       }
       
       return ProductIDs;
    }
    
    
}